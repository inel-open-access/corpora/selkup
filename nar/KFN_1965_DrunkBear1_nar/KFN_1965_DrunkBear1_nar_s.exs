<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KFN_1965_DrunkBear_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KFN_1965_DrunkBear1_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">81</ud-information>
            <ud-information attribute-name="# HIAT:w">61</ud-information>
            <ud-information attribute-name="# e">61</ud-information>
            <ud-information attribute-name="# HIAT:u">12</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KFN">
            <abbreviation>KFN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KFN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T61" id="Seg_0" n="sc" s="T0">
               <ts e="T8" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">uɣot</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">tɨlʼ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">pelʼkaqɨt</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">qorɣa</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">töːmba</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">čʼoːlond</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">mʼedam</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">awešpɨgu</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">tabɨt</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">koštɨmbat</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_38" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">parütɨm</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">meːmbɨtɨt</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">nača</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">parütɨm</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">čatčimbat</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_56" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">qorɣa</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">parütɨm</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">ütɨmbat</ts>
                  <nts id="Seg_65" n="HIAT:ip">,</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">tab</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">šeːrbat</ts>
                  <nts id="Seg_72" n="HIAT:ip">.</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_75" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">ilʼlʼe</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">aːlʼčimba</ts>
                  <nts id="Seg_81" n="HIAT:ip">,</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">qondalba</ts>
                  <nts id="Seg_85" n="HIAT:ip">.</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_88" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_90" n="HIAT:w" s="T23">tabɨt</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_93" n="HIAT:w" s="T24">šitə</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_96" n="HIAT:w" s="T25">čündəp</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_99" n="HIAT:w" s="T26">arelbat</ts>
                  <nts id="Seg_100" n="HIAT:ip">,</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">telʼegaɣe</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">qwänbat</ts>
                  <nts id="Seg_107" n="HIAT:ip">.</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_110" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_112" n="HIAT:w" s="T29">šeːrbɨlʼ</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">qorqɨp</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_118" n="HIAT:w" s="T31">čelʼegant</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_121" n="HIAT:w" s="T32">pannbat</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">moqonä</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_127" n="HIAT:w" s="T34">qwändɨgu</ts>
                  <nts id="Seg_128" n="HIAT:ip">.</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_131" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">moqonä</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_136" n="HIAT:w" s="T36">üːbečimbat</ts>
                  <nts id="Seg_137" n="HIAT:ip">.</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_140" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_142" n="HIAT:w" s="T37">qorɣ</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_145" n="HIAT:w" s="T38">qälʼčimba</ts>
                  <nts id="Seg_146" n="HIAT:ip">,</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_149" n="HIAT:w" s="T39">parkʼeːlɨmba</ts>
                  <nts id="Seg_150" n="HIAT:ip">.</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_153" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_155" n="HIAT:w" s="T40">čündet</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_158" n="HIAT:w" s="T41">kočiwatpat</ts>
                  <nts id="Seg_159" n="HIAT:ip">,</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_162" n="HIAT:w" s="T42">qoptə</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_165" n="HIAT:w" s="T43">torčaqɨlbat</ts>
                  <nts id="Seg_166" n="HIAT:ip">.</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_169" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_171" n="HIAT:w" s="T44">telʼega</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_174" n="HIAT:w" s="T45">wes</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_177" n="HIAT:w" s="T46">taq</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_180" n="HIAT:w" s="T47">panalbat</ts>
                  <nts id="Seg_181" n="HIAT:ip">,</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_184" n="HIAT:w" s="T48">i</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_187" n="HIAT:w" s="T49">qorqɣu</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_190" n="HIAT:w" s="T50">näj</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_193" n="HIAT:w" s="T51">qwänba</ts>
                  <nts id="Seg_194" n="HIAT:ip">.</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_197" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_199" n="HIAT:w" s="T52">tabet</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_202" n="HIAT:w" s="T53">čwesse</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_205" n="HIAT:w" s="T54">töːat</ts>
                  <nts id="Seg_206" n="HIAT:ip">,</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_209" n="HIAT:w" s="T55">telʼega</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_212" n="HIAT:w" s="T56">näj</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_215" n="HIAT:w" s="T57">čangwa</ts>
                  <nts id="Seg_216" n="HIAT:ip">,</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_219" n="HIAT:w" s="T58">i</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_222" n="HIAT:w" s="T59">qorqunäj</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_225" n="HIAT:w" s="T60">netua</ts>
                  <nts id="Seg_226" n="HIAT:ip">.</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T61" id="Seg_228" n="sc" s="T0">
               <ts e="T1" id="Seg_230" n="e" s="T0">uɣot </ts>
               <ts e="T2" id="Seg_232" n="e" s="T1">tɨlʼ </ts>
               <ts e="T3" id="Seg_234" n="e" s="T2">pelʼkaqɨt </ts>
               <ts e="T4" id="Seg_236" n="e" s="T3">qorɣa </ts>
               <ts e="T5" id="Seg_238" n="e" s="T4">töːmba </ts>
               <ts e="T6" id="Seg_240" n="e" s="T5">čʼoːlond </ts>
               <ts e="T7" id="Seg_242" n="e" s="T6">mʼedam </ts>
               <ts e="T8" id="Seg_244" n="e" s="T7">awešpɨgu. </ts>
               <ts e="T9" id="Seg_246" n="e" s="T8">tabɨt </ts>
               <ts e="T10" id="Seg_248" n="e" s="T9">koštɨmbat. </ts>
               <ts e="T11" id="Seg_250" n="e" s="T10">parütɨm </ts>
               <ts e="T12" id="Seg_252" n="e" s="T11">meːmbɨtɨt </ts>
               <ts e="T13" id="Seg_254" n="e" s="T12">nača </ts>
               <ts e="T14" id="Seg_256" n="e" s="T13">parütɨm </ts>
               <ts e="T15" id="Seg_258" n="e" s="T14">čatčimbat. </ts>
               <ts e="T16" id="Seg_260" n="e" s="T15">qorɣa </ts>
               <ts e="T17" id="Seg_262" n="e" s="T16">parütɨm </ts>
               <ts e="T18" id="Seg_264" n="e" s="T17">ütɨmbat, </ts>
               <ts e="T19" id="Seg_266" n="e" s="T18">tab </ts>
               <ts e="T20" id="Seg_268" n="e" s="T19">šeːrbat. </ts>
               <ts e="T21" id="Seg_270" n="e" s="T20">ilʼlʼe </ts>
               <ts e="T22" id="Seg_272" n="e" s="T21">aːlʼčimba, </ts>
               <ts e="T23" id="Seg_274" n="e" s="T22">qondalba. </ts>
               <ts e="T24" id="Seg_276" n="e" s="T23">tabɨt </ts>
               <ts e="T25" id="Seg_278" n="e" s="T24">šitə </ts>
               <ts e="T26" id="Seg_280" n="e" s="T25">čündəp </ts>
               <ts e="T27" id="Seg_282" n="e" s="T26">arelbat, </ts>
               <ts e="T28" id="Seg_284" n="e" s="T27">telʼegaɣe </ts>
               <ts e="T29" id="Seg_286" n="e" s="T28">qwänbat. </ts>
               <ts e="T30" id="Seg_288" n="e" s="T29">šeːrbɨlʼ </ts>
               <ts e="T31" id="Seg_290" n="e" s="T30">qorqɨp </ts>
               <ts e="T32" id="Seg_292" n="e" s="T31">čelʼegant </ts>
               <ts e="T33" id="Seg_294" n="e" s="T32">pannbat </ts>
               <ts e="T34" id="Seg_296" n="e" s="T33">moqonä </ts>
               <ts e="T35" id="Seg_298" n="e" s="T34">qwändɨgu. </ts>
               <ts e="T36" id="Seg_300" n="e" s="T35">moqonä </ts>
               <ts e="T37" id="Seg_302" n="e" s="T36">üːbečimbat. </ts>
               <ts e="T38" id="Seg_304" n="e" s="T37">qorɣ </ts>
               <ts e="T39" id="Seg_306" n="e" s="T38">qälʼčimba, </ts>
               <ts e="T40" id="Seg_308" n="e" s="T39">parkʼeːlɨmba. </ts>
               <ts e="T41" id="Seg_310" n="e" s="T40">čündet </ts>
               <ts e="T42" id="Seg_312" n="e" s="T41">kočiwatpat, </ts>
               <ts e="T43" id="Seg_314" n="e" s="T42">qoptə </ts>
               <ts e="T44" id="Seg_316" n="e" s="T43">torčaqɨlbat. </ts>
               <ts e="T45" id="Seg_318" n="e" s="T44">telʼega </ts>
               <ts e="T46" id="Seg_320" n="e" s="T45">wes </ts>
               <ts e="T47" id="Seg_322" n="e" s="T46">taq </ts>
               <ts e="T48" id="Seg_324" n="e" s="T47">panalbat, </ts>
               <ts e="T49" id="Seg_326" n="e" s="T48">i </ts>
               <ts e="T50" id="Seg_328" n="e" s="T49">qorqɣu </ts>
               <ts e="T51" id="Seg_330" n="e" s="T50">näj </ts>
               <ts e="T52" id="Seg_332" n="e" s="T51">qwänba. </ts>
               <ts e="T53" id="Seg_334" n="e" s="T52">tabet </ts>
               <ts e="T54" id="Seg_336" n="e" s="T53">čwesse </ts>
               <ts e="T55" id="Seg_338" n="e" s="T54">töːat, </ts>
               <ts e="T56" id="Seg_340" n="e" s="T55">telʼega </ts>
               <ts e="T57" id="Seg_342" n="e" s="T56">näj </ts>
               <ts e="T58" id="Seg_344" n="e" s="T57">čangwa, </ts>
               <ts e="T59" id="Seg_346" n="e" s="T58">i </ts>
               <ts e="T60" id="Seg_348" n="e" s="T59">qorqunäj </ts>
               <ts e="T61" id="Seg_350" n="e" s="T60">netua. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T8" id="Seg_351" s="T0">KFN_1965_DrunkBear1_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_352" s="T8">KFN_1965_DrunkBear1_nar.002 (001.002)</ta>
            <ta e="T15" id="Seg_353" s="T10">KFN_1965_DrunkBear1_nar.003 (001.003)</ta>
            <ta e="T20" id="Seg_354" s="T15">KFN_1965_DrunkBear1_nar.004 (001.004)</ta>
            <ta e="T23" id="Seg_355" s="T20">KFN_1965_DrunkBear1_nar.005 (001.005)</ta>
            <ta e="T29" id="Seg_356" s="T23">KFN_1965_DrunkBear1_nar.006 (001.006)</ta>
            <ta e="T35" id="Seg_357" s="T29">KFN_1965_DrunkBear1_nar.007 (001.007)</ta>
            <ta e="T37" id="Seg_358" s="T35">KFN_1965_DrunkBear1_nar.008 (001.008)</ta>
            <ta e="T40" id="Seg_359" s="T37">KFN_1965_DrunkBear1_nar.009 (001.009)</ta>
            <ta e="T44" id="Seg_360" s="T40">KFN_1965_DrunkBear1_nar.010 (001.010)</ta>
            <ta e="T52" id="Seg_361" s="T44">KFN_1965_DrunkBear1_nar.011 (001.011)</ta>
            <ta e="T61" id="Seg_362" s="T52">KFN_1965_DrunkBear1_nar.012 (001.012)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T8" id="Seg_363" s="T0">уɣот ′тыlпеlкаɣыт kорɣа тӧ̄мба ′чʼо̄лонд мʼедам а′вешпыгу.</ta>
            <ta e="T10" id="Seg_364" s="T8">табыт kошты‵мбат.</ta>
            <ta e="T15" id="Seg_365" s="T10">па′рӱтым ′ме̄мбытыт нача па′рӱтым ‵чатчим′бат.</ta>
            <ta e="T20" id="Seg_366" s="T15">′kорɣа пар′ӱтым ′ӱтымбат, таб ′ше̨̄рбат.</ta>
            <ta e="T23" id="Seg_367" s="T20">и′llе ′а̄lчимба, kон′далба.</ta>
            <ta e="T29" id="Seg_368" s="T23">′табыт шитъ ′чӱндъп а′релбат, те′lегаɣе kwӓнбат.</ta>
            <ta e="T35" id="Seg_369" s="T29">′ше̄рбыl kорɣып тʼе′lегант ‵панн′бат моɣо′нӓ ′kwӓндыгу.</ta>
            <ta e="T37" id="Seg_370" s="T35">моɣо′нӓ ′ӱ̄бечимбат.</ta>
            <ta e="T40" id="Seg_371" s="T37">kорɣ ′kӓlчимба, пар кʼе̄лымба.</ta>
            <ta e="T44" id="Seg_372" s="T40">чӱндет кочиватпат, ′kоптъ тор′чаɣылбат.</ta>
            <ta e="T52" id="Seg_373" s="T44">теlега вес таk па′налбат, и kорɣу нӓй kwӓнба.</ta>
            <ta e="T61" id="Seg_374" s="T52">табет чвессе тӧ̄ат, те′lега нӓй ′чангва, и kорɣунӓй ′нетуа.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T8" id="Seg_375" s="T0">uɣot tɨlʼpelʼkaqɨt korɣa töːmba čʼoːlond mʼedam awešpɨgu.</ta>
            <ta e="T10" id="Seg_376" s="T8">tabɨt koštɨmbat.</ta>
            <ta e="T15" id="Seg_377" s="T10">parütɨm meːmbɨtɨt nača parütɨm čatčimbat.</ta>
            <ta e="T20" id="Seg_378" s="T15">korɣa parütɨm ütɨmbat, tab šeːrbat.</ta>
            <ta e="T23" id="Seg_379" s="T20">ilʼlʼe aːlʼčimba, kondalba.</ta>
            <ta e="T29" id="Seg_380" s="T23">tabɨt šitə čündəp arelbat, telʼegaɣe qwänbat.</ta>
            <ta e="T35" id="Seg_381" s="T29">šeːrbɨlʼ qorqɨp čelʼegant pannbat moqonä kwändɨgu.</ta>
            <ta e="T37" id="Seg_382" s="T35">moqonä üːbečimbat.</ta>
            <ta e="T40" id="Seg_383" s="T37">qorɣ qälʼčimba, par kʼeːlɨmba.</ta>
            <ta e="T44" id="Seg_384" s="T40">čündet kočivatpat, qoptə torčaqɨlbat.</ta>
            <ta e="T52" id="Seg_385" s="T44">telʼega ves taq panalbat, i qorqɣu näj qwänba.</ta>
            <ta e="T61" id="Seg_386" s="T52">tabet čvesse töːat, telʼega näj čangva, i qorqunäj netua.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T8" id="Seg_387" s="T0">uɣot tɨlʼ pelʼkaqɨt qorɣa töːmba čʼoːlond mʼedam awešpɨgu. </ta>
            <ta e="T10" id="Seg_388" s="T8">tabɨt koštɨmbat. </ta>
            <ta e="T15" id="Seg_389" s="T10">parütɨm meːmbɨtɨt nača parütɨm čatčimbat. </ta>
            <ta e="T20" id="Seg_390" s="T15">qorɣa parütɨm ütɨmbat, tab šeːrbat. </ta>
            <ta e="T23" id="Seg_391" s="T20">ilʼlʼe aːlʼčimba, qondalba. </ta>
            <ta e="T29" id="Seg_392" s="T23">tabɨt šitə čündəp arelbat, telʼegaɣe qwänbat. </ta>
            <ta e="T35" id="Seg_393" s="T29">šeːrbɨlʼ qorqɨp čelʼegant pannbat moqonä qwändɨgu. </ta>
            <ta e="T37" id="Seg_394" s="T35">moqonä üːbečimbat. </ta>
            <ta e="T40" id="Seg_395" s="T37">qorɣ qälʼčimba, parkʼeːlɨmba. </ta>
            <ta e="T44" id="Seg_396" s="T40">čündet kočiwatpat, qoptə torčaqɨlbat. </ta>
            <ta e="T52" id="Seg_397" s="T44">telʼega wes taq panalbat, i qorqɣu näj qwänba. </ta>
            <ta e="T61" id="Seg_398" s="T52">tabet čwesse töːat, telʼega näj čangwa, i qorqunäj netua. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_399" s="T0">uɣot</ta>
            <ta e="T2" id="Seg_400" s="T1">tɨ-lʼ</ta>
            <ta e="T3" id="Seg_401" s="T2">pelʼka-qɨt</ta>
            <ta e="T4" id="Seg_402" s="T3">qorɣa</ta>
            <ta e="T5" id="Seg_403" s="T4">töː-mba</ta>
            <ta e="T6" id="Seg_404" s="T5">čʼoːlo-nd</ta>
            <ta e="T7" id="Seg_405" s="T6">mʼod-a-m</ta>
            <ta e="T8" id="Seg_406" s="T7">aw-e-špɨ-gu</ta>
            <ta e="T9" id="Seg_407" s="T8">tab-ǝ-t</ta>
            <ta e="T10" id="Seg_408" s="T9">koštɨ-mba-t</ta>
            <ta e="T11" id="Seg_409" s="T10">parütɨ-m</ta>
            <ta e="T12" id="Seg_410" s="T11">meː-mbɨ-tɨt</ta>
            <ta e="T13" id="Seg_411" s="T12">nača</ta>
            <ta e="T14" id="Seg_412" s="T13">parütɨ-m</ta>
            <ta e="T15" id="Seg_413" s="T14">čatči-mba-t</ta>
            <ta e="T16" id="Seg_414" s="T15">qorɣa</ta>
            <ta e="T17" id="Seg_415" s="T16">parütɨ-m</ta>
            <ta e="T18" id="Seg_416" s="T17">ütɨ-mba-t</ta>
            <ta e="T19" id="Seg_417" s="T18">tab</ta>
            <ta e="T20" id="Seg_418" s="T19">šeːr-ba-t</ta>
            <ta e="T21" id="Seg_419" s="T20">ilʼlʼe</ta>
            <ta e="T22" id="Seg_420" s="T21">aːlʼči-mba</ta>
            <ta e="T23" id="Seg_421" s="T22">qonda-lə-ba</ta>
            <ta e="T24" id="Seg_422" s="T23">tab-ǝ-t</ta>
            <ta e="T25" id="Seg_423" s="T24">šitə</ta>
            <ta e="T26" id="Seg_424" s="T25">čünd-ə-p</ta>
            <ta e="T27" id="Seg_425" s="T26">arel-ba-t</ta>
            <ta e="T28" id="Seg_426" s="T27">telʼega-ɣe</ta>
            <ta e="T29" id="Seg_427" s="T28">qwän-ba-t</ta>
            <ta e="T30" id="Seg_428" s="T29">šeːr-bɨlʼ</ta>
            <ta e="T31" id="Seg_429" s="T30">qorqɨ-p</ta>
            <ta e="T32" id="Seg_430" s="T31">čelʼega-nt</ta>
            <ta e="T33" id="Seg_431" s="T32">pan-nba-t</ta>
            <ta e="T34" id="Seg_432" s="T33">moqonä</ta>
            <ta e="T35" id="Seg_433" s="T34">qwän-dɨ-gu</ta>
            <ta e="T36" id="Seg_434" s="T35">moqonä</ta>
            <ta e="T37" id="Seg_435" s="T36">üːbe-či-mba-t</ta>
            <ta e="T38" id="Seg_436" s="T37">qorɣ</ta>
            <ta e="T39" id="Seg_437" s="T38">qälʼči-mba</ta>
            <ta e="T40" id="Seg_438" s="T39">parkʼeː-lɨ-mba</ta>
            <ta e="T41" id="Seg_439" s="T40">čünde-t</ta>
            <ta e="T42" id="Seg_440" s="T41">kočiwat-pa-t</ta>
            <ta e="T43" id="Seg_441" s="T42">qoptə</ta>
            <ta e="T44" id="Seg_442" s="T43">torča-qɨl-ba-t</ta>
            <ta e="T45" id="Seg_443" s="T44">telʼega</ta>
            <ta e="T46" id="Seg_444" s="T45">wes</ta>
            <ta e="T47" id="Seg_445" s="T46">taq</ta>
            <ta e="T48" id="Seg_446" s="T47">panal-ba-t</ta>
            <ta e="T49" id="Seg_447" s="T48">i</ta>
            <ta e="T50" id="Seg_448" s="T49">qorqɣu</ta>
            <ta e="T51" id="Seg_449" s="T50">näj</ta>
            <ta e="T52" id="Seg_450" s="T51">qwän-ba</ta>
            <ta e="T53" id="Seg_451" s="T52">tab-e-t</ta>
            <ta e="T54" id="Seg_452" s="T53">čwesse</ta>
            <ta e="T55" id="Seg_453" s="T54">töː-a-t</ta>
            <ta e="T56" id="Seg_454" s="T55">telʼega</ta>
            <ta e="T57" id="Seg_455" s="T56">näj</ta>
            <ta e="T58" id="Seg_456" s="T57">čang-wa</ta>
            <ta e="T59" id="Seg_457" s="T58">i</ta>
            <ta e="T60" id="Seg_458" s="T59">qorqu-näj</ta>
            <ta e="T61" id="Seg_459" s="T60">netu-a</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_460" s="T0">ugon</ta>
            <ta e="T2" id="Seg_461" s="T1">tɨ-lʼ</ta>
            <ta e="T3" id="Seg_462" s="T2">pelka-qɨt</ta>
            <ta e="T4" id="Seg_463" s="T3">qorqɨ</ta>
            <ta e="T5" id="Seg_464" s="T4">töː-mbɨ</ta>
            <ta e="T6" id="Seg_465" s="T5">čʼoːlo-nde</ta>
            <ta e="T7" id="Seg_466" s="T6">mʼod-ɨ-m</ta>
            <ta e="T8" id="Seg_467" s="T7">am-ɨ-špɨ-gu</ta>
            <ta e="T9" id="Seg_468" s="T8">tab-ɨ-t</ta>
            <ta e="T10" id="Seg_469" s="T9">koštɨ-mbɨ-dət</ta>
            <ta e="T11" id="Seg_470" s="T10">parüdi-m</ta>
            <ta e="T12" id="Seg_471" s="T11">me-mbɨ-dət</ta>
            <ta e="T13" id="Seg_472" s="T12">nača</ta>
            <ta e="T14" id="Seg_473" s="T13">parüdi-m</ta>
            <ta e="T15" id="Seg_474" s="T14">čatči-mbɨ-dət</ta>
            <ta e="T16" id="Seg_475" s="T15">qorqɨ</ta>
            <ta e="T17" id="Seg_476" s="T16">parüdi-m</ta>
            <ta e="T18" id="Seg_477" s="T17">üdɨ-mbɨ-tɨ</ta>
            <ta e="T19" id="Seg_478" s="T18">tab</ta>
            <ta e="T20" id="Seg_479" s="T19">əːšeːr-mbɨ-tɨ</ta>
            <ta e="T21" id="Seg_480" s="T20">illä</ta>
            <ta e="T22" id="Seg_481" s="T21">alʼči-mbɨ</ta>
            <ta e="T23" id="Seg_482" s="T22">qontɨ-lə-mbɨ</ta>
            <ta e="T24" id="Seg_483" s="T23">tab-ɨ-t</ta>
            <ta e="T25" id="Seg_484" s="T24">šitə</ta>
            <ta e="T26" id="Seg_485" s="T25">čünd-ɨ-p</ta>
            <ta e="T27" id="Seg_486" s="T26">arel-mbɨ-dət</ta>
            <ta e="T28" id="Seg_487" s="T27">čelʼega-se</ta>
            <ta e="T29" id="Seg_488" s="T28">qwän-mbɨ-dət</ta>
            <ta e="T30" id="Seg_489" s="T29">əːšeːr-mbɨlʼe</ta>
            <ta e="T31" id="Seg_490" s="T30">qorqɨ-p</ta>
            <ta e="T32" id="Seg_491" s="T31">čelʼega-nde</ta>
            <ta e="T33" id="Seg_492" s="T32">pan-mbɨ-dət</ta>
            <ta e="T34" id="Seg_493" s="T33">moqne</ta>
            <ta e="T35" id="Seg_494" s="T34">qwän-dɨ-gu</ta>
            <ta e="T36" id="Seg_495" s="T35">moqne</ta>
            <ta e="T37" id="Seg_496" s="T36">üːppɨ-če-mbɨ-dət</ta>
            <ta e="T38" id="Seg_497" s="T37">qorqɨ</ta>
            <ta e="T39" id="Seg_498" s="T38">qelʼčǝ-mbɨ</ta>
            <ta e="T40" id="Seg_499" s="T39">parka-lɨ-mbɨ</ta>
            <ta e="T41" id="Seg_500" s="T40">čünd-t</ta>
            <ta e="T42" id="Seg_501" s="T41">kačwat-mbɨ-dət</ta>
            <ta e="T43" id="Seg_502" s="T42">qoptə</ta>
            <ta e="T44" id="Seg_503" s="T43">torčaː-qəl-mbɨ-dət</ta>
            <ta e="T45" id="Seg_504" s="T44">čelʼega</ta>
            <ta e="T46" id="Seg_505" s="T45">wesʼ</ta>
            <ta e="T47" id="Seg_506" s="T46">tak</ta>
            <ta e="T48" id="Seg_507" s="T47">panal-mbɨ-tɨ</ta>
            <ta e="T49" id="Seg_508" s="T48">i</ta>
            <ta e="T50" id="Seg_509" s="T49">qorqɨ</ta>
            <ta e="T51" id="Seg_510" s="T50">naj</ta>
            <ta e="T52" id="Seg_511" s="T51">qwän-mbɨ</ta>
            <ta e="T53" id="Seg_512" s="T52">tab-ɨ-t</ta>
            <ta e="T54" id="Seg_513" s="T53">čwesse</ta>
            <ta e="T55" id="Seg_514" s="T54">töː-ɨ-dət</ta>
            <ta e="T56" id="Seg_515" s="T55">čelʼega</ta>
            <ta e="T57" id="Seg_516" s="T56">naj</ta>
            <ta e="T58" id="Seg_517" s="T57">čaŋgɨ-ŋɨ</ta>
            <ta e="T59" id="Seg_518" s="T58">i</ta>
            <ta e="T60" id="Seg_519" s="T59">qorqɨ-naj</ta>
            <ta e="T61" id="Seg_520" s="T60">nʼetu-ŋɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_521" s="T0">earlier</ta>
            <ta e="T2" id="Seg_522" s="T1">this-ADJZ</ta>
            <ta e="T3" id="Seg_523" s="T2">side-LOC</ta>
            <ta e="T4" id="Seg_524" s="T3">bear.[NOM]</ta>
            <ta e="T5" id="Seg_525" s="T4">come-PST.NAR.[3SG.S]</ta>
            <ta e="T6" id="Seg_526" s="T5">bee-ILL</ta>
            <ta e="T7" id="Seg_527" s="T6">honey-EP-ACC</ta>
            <ta e="T8" id="Seg_528" s="T7">eat-EP-IPFV2-INF</ta>
            <ta e="T9" id="Seg_529" s="T8">(s)he-EP.[NOM]-PL</ta>
            <ta e="T10" id="Seg_530" s="T9">find.out-PST.NAR-3PL</ta>
            <ta e="T11" id="Seg_531" s="T10">vodka-ACC</ta>
            <ta e="T12" id="Seg_532" s="T11">do-DUR-3PL</ta>
            <ta e="T13" id="Seg_533" s="T12">there</ta>
            <ta e="T14" id="Seg_534" s="T13">vodka-ACC</ta>
            <ta e="T15" id="Seg_535" s="T14">put-PST.NAR-3PL</ta>
            <ta e="T16" id="Seg_536" s="T15">bear.[NOM]</ta>
            <ta e="T17" id="Seg_537" s="T16">vodka-ACC</ta>
            <ta e="T18" id="Seg_538" s="T17">drink-PST.NAR-3SG.O</ta>
            <ta e="T19" id="Seg_539" s="T18">(s)he.[NOM]</ta>
            <ta e="T20" id="Seg_540" s="T19">get.drunk-PST.NAR-3SG.O</ta>
            <ta e="T21" id="Seg_541" s="T20">down</ta>
            <ta e="T22" id="Seg_542" s="T21">fall-PST.NAR.[3SG.S]</ta>
            <ta e="T23" id="Seg_543" s="T22">sleep-INCH-PST.NAR.[3SG.S]</ta>
            <ta e="T24" id="Seg_544" s="T23">(s)he-EP.[NOM]-PL</ta>
            <ta e="T25" id="Seg_545" s="T24">two</ta>
            <ta e="T26" id="Seg_546" s="T25">horse-EP-ACC</ta>
            <ta e="T27" id="Seg_547" s="T26">tie-PST.NAR-3PL</ta>
            <ta e="T28" id="Seg_548" s="T27">cart-INSTR</ta>
            <ta e="T29" id="Seg_549" s="T28">go.away-PST.NAR-3PL</ta>
            <ta e="T30" id="Seg_550" s="T29">get.drunk-PTCP.PST</ta>
            <ta e="T31" id="Seg_551" s="T30">bear-ACC</ta>
            <ta e="T32" id="Seg_552" s="T31">cart-ILL</ta>
            <ta e="T33" id="Seg_553" s="T32">put-PST.NAR-3PL</ta>
            <ta e="T34" id="Seg_554" s="T33">home</ta>
            <ta e="T35" id="Seg_555" s="T34">go.away-TR-INF</ta>
            <ta e="T36" id="Seg_556" s="T35">home</ta>
            <ta e="T37" id="Seg_557" s="T36">leave-DRV-PST.NAR-3PL</ta>
            <ta e="T38" id="Seg_558" s="T37">bear.[NOM]</ta>
            <ta e="T39" id="Seg_559" s="T38">wake.up-PST.NAR.[3SG.S]</ta>
            <ta e="T40" id="Seg_560" s="T39">shout-RES-PST.NAR.[3SG.S]</ta>
            <ta e="T41" id="Seg_561" s="T40">horse-PL.[3SG.S]</ta>
            <ta e="T42" id="Seg_562" s="T41">get.angry-PST.NAR-3PL</ta>
            <ta e="T43" id="Seg_563" s="T42">%%</ta>
            <ta e="T44" id="Seg_564" s="T43">run-MULS-PST.NAR-3PL</ta>
            <ta e="T45" id="Seg_565" s="T44">cart.[NOM]</ta>
            <ta e="T46" id="Seg_566" s="T45">all</ta>
            <ta e="T47" id="Seg_567" s="T46">away</ta>
            <ta e="T48" id="Seg_568" s="T47">break-PST.NAR-3SG.O</ta>
            <ta e="T49" id="Seg_569" s="T48">and</ta>
            <ta e="T50" id="Seg_570" s="T49">bear.[3SG.S]</ta>
            <ta e="T51" id="Seg_571" s="T50">also</ta>
            <ta e="T52" id="Seg_572" s="T51">go.away-PST.NAR.[3SG.S]</ta>
            <ta e="T53" id="Seg_573" s="T52">(s)he-EP-PL.[NOM]</ta>
            <ta e="T54" id="Seg_574" s="T53">backward</ta>
            <ta e="T55" id="Seg_575" s="T54">come-EP-3PL</ta>
            <ta e="T56" id="Seg_576" s="T55">cart.[NOM]</ta>
            <ta e="T57" id="Seg_577" s="T56">also</ta>
            <ta e="T58" id="Seg_578" s="T57">NEG.EX-CO.[3SG.S]</ta>
            <ta e="T59" id="Seg_579" s="T58">and</ta>
            <ta e="T60" id="Seg_580" s="T59">bear-EMPH</ta>
            <ta e="T61" id="Seg_581" s="T60">NEG.EX-CO.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_582" s="T0">раньше</ta>
            <ta e="T2" id="Seg_583" s="T1">этот-ADJZ</ta>
            <ta e="T3" id="Seg_584" s="T2">сторона-LOC</ta>
            <ta e="T4" id="Seg_585" s="T3">медведь.[NOM]</ta>
            <ta e="T5" id="Seg_586" s="T4">прийти-PST.NAR.[3SG.S]</ta>
            <ta e="T6" id="Seg_587" s="T5">пчела-ILL</ta>
            <ta e="T7" id="Seg_588" s="T6">мёд-EP-ACC</ta>
            <ta e="T8" id="Seg_589" s="T7">есть-EP-IPFV2-INF</ta>
            <ta e="T9" id="Seg_590" s="T8">он(а)-EP.[NOM]-PL</ta>
            <ta e="T10" id="Seg_591" s="T9">узнать;-PST.NAR-3PL</ta>
            <ta e="T11" id="Seg_592" s="T10">водка-ACC</ta>
            <ta e="T12" id="Seg_593" s="T11">делать-DUR-3PL</ta>
            <ta e="T13" id="Seg_594" s="T12">туда</ta>
            <ta e="T14" id="Seg_595" s="T13">водка-ACC</ta>
            <ta e="T15" id="Seg_596" s="T14">поставить-PST.NAR-3PL</ta>
            <ta e="T16" id="Seg_597" s="T15">медведь.[NOM]</ta>
            <ta e="T17" id="Seg_598" s="T16">водка-ACC</ta>
            <ta e="T18" id="Seg_599" s="T17">пить-PST.NAR-3SG.O</ta>
            <ta e="T19" id="Seg_600" s="T18">он(а).[NOM]</ta>
            <ta e="T20" id="Seg_601" s="T19">опьянеть-PST.NAR-3SG.O</ta>
            <ta e="T21" id="Seg_602" s="T20">вниз</ta>
            <ta e="T22" id="Seg_603" s="T21">упасть-PST.NAR.[3SG.S]</ta>
            <ta e="T23" id="Seg_604" s="T22">спять-INCH-PST.NAR.[3SG.S]</ta>
            <ta e="T24" id="Seg_605" s="T23">он(а)-EP.[NOM]-PL</ta>
            <ta e="T25" id="Seg_606" s="T24">два</ta>
            <ta e="T26" id="Seg_607" s="T25">лошадь-EP-ACC</ta>
            <ta e="T27" id="Seg_608" s="T26">связать-PST.NAR-3PL</ta>
            <ta e="T28" id="Seg_609" s="T27">телега-INSTR</ta>
            <ta e="T29" id="Seg_610" s="T28">пойти-PST.NAR-3PL</ta>
            <ta e="T30" id="Seg_611" s="T29">опьянеть-PTCP.PST</ta>
            <ta e="T31" id="Seg_612" s="T30">медведь-ACC</ta>
            <ta e="T32" id="Seg_613" s="T31">телега-ILL</ta>
            <ta e="T33" id="Seg_614" s="T32">положить-PST.NAR-3PL</ta>
            <ta e="T34" id="Seg_615" s="T33">домой</ta>
            <ta e="T35" id="Seg_616" s="T34">пойти-TR-INF</ta>
            <ta e="T36" id="Seg_617" s="T35">домой</ta>
            <ta e="T37" id="Seg_618" s="T36">пойти-DRV-PST.NAR-3PL</ta>
            <ta e="T38" id="Seg_619" s="T37">медведь.[NOM]</ta>
            <ta e="T39" id="Seg_620" s="T38">проснуться-PST.NAR.[3SG.S]</ta>
            <ta e="T40" id="Seg_621" s="T39">кричать-RES-PST.NAR.[3SG.S]</ta>
            <ta e="T41" id="Seg_622" s="T40">лошадь-PL.[3SG.S]</ta>
            <ta e="T42" id="Seg_623" s="T41">испугаться-PST.NAR-3PL</ta>
            <ta e="T43" id="Seg_624" s="T42">%%</ta>
            <ta e="T44" id="Seg_625" s="T43">бегать-MULS-PST.NAR-3PL</ta>
            <ta e="T45" id="Seg_626" s="T44">телега.[NOM]</ta>
            <ta e="T46" id="Seg_627" s="T45">весь</ta>
            <ta e="T47" id="Seg_628" s="T46">прочь</ta>
            <ta e="T48" id="Seg_629" s="T47">сломать-PST.NAR-3SG.O</ta>
            <ta e="T49" id="Seg_630" s="T48">и</ta>
            <ta e="T50" id="Seg_631" s="T49">медведь.[3SG.S]</ta>
            <ta e="T51" id="Seg_632" s="T50">тоже</ta>
            <ta e="T52" id="Seg_633" s="T51">пойти-PST.NAR.[3SG.S]</ta>
            <ta e="T53" id="Seg_634" s="T52">он(а)-EP-PL.[NOM]</ta>
            <ta e="T54" id="Seg_635" s="T53">назад</ta>
            <ta e="T55" id="Seg_636" s="T54">прийти-EP-3PL</ta>
            <ta e="T56" id="Seg_637" s="T55">телега.[NOM]</ta>
            <ta e="T57" id="Seg_638" s="T56">тоже</ta>
            <ta e="T58" id="Seg_639" s="T57">NEG.EX-CO.[3SG.S]</ta>
            <ta e="T59" id="Seg_640" s="T58">и</ta>
            <ta e="T60" id="Seg_641" s="T59">медведь-EMPH</ta>
            <ta e="T61" id="Seg_642" s="T60">NEG.EX-CO.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_643" s="T0">adv</ta>
            <ta e="T2" id="Seg_644" s="T1">dem-n&gt;adj</ta>
            <ta e="T3" id="Seg_645" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_646" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_647" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_648" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_649" s="T6">n-n:ins-n:case</ta>
            <ta e="T8" id="Seg_650" s="T7">v-n:ins-v&gt;v-v:inf</ta>
            <ta e="T9" id="Seg_651" s="T8">pers-n:ins-n:case-n:num</ta>
            <ta e="T10" id="Seg_652" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_653" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_654" s="T11">v-v&gt;v-v:pn</ta>
            <ta e="T13" id="Seg_655" s="T12">adv</ta>
            <ta e="T14" id="Seg_656" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_657" s="T14">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_658" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_659" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_660" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_661" s="T18">pers-n:case</ta>
            <ta e="T20" id="Seg_662" s="T19">v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_663" s="T20">preverb</ta>
            <ta e="T22" id="Seg_664" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_665" s="T22">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_666" s="T23">pers-n:ins-n:case-n:num</ta>
            <ta e="T25" id="Seg_667" s="T24">num</ta>
            <ta e="T26" id="Seg_668" s="T25">n-n:ins-n:case</ta>
            <ta e="T27" id="Seg_669" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_670" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_671" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_672" s="T29">v-v&gt;ptcp</ta>
            <ta e="T31" id="Seg_673" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_674" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_675" s="T32">v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_676" s="T33">adv</ta>
            <ta e="T35" id="Seg_677" s="T34">v-v&gt;v-v:inf</ta>
            <ta e="T36" id="Seg_678" s="T35">adv</ta>
            <ta e="T37" id="Seg_679" s="T36">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_680" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_681" s="T38">v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_682" s="T39">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_683" s="T40">n-n:num-v:pn</ta>
            <ta e="T42" id="Seg_684" s="T41">v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_685" s="T42">adv</ta>
            <ta e="T44" id="Seg_686" s="T43">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_687" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_688" s="T45">quant</ta>
            <ta e="T47" id="Seg_689" s="T46">preverb</ta>
            <ta e="T48" id="Seg_690" s="T47">v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_691" s="T48">conj</ta>
            <ta e="T50" id="Seg_692" s="T49">n-v:pn</ta>
            <ta e="T51" id="Seg_693" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_694" s="T51">v-v:tense-v:pn</ta>
            <ta e="T53" id="Seg_695" s="T52">pers-n:ins-n:num-n:case</ta>
            <ta e="T54" id="Seg_696" s="T53">adv</ta>
            <ta e="T55" id="Seg_697" s="T54">v-n:ins-v:pn</ta>
            <ta e="T56" id="Seg_698" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_699" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_700" s="T57">v-v:ins-v:pn</ta>
            <ta e="T59" id="Seg_701" s="T58">conj</ta>
            <ta e="T60" id="Seg_702" s="T59">n-clit</ta>
            <ta e="T61" id="Seg_703" s="T60">v-n:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_704" s="T0">adv</ta>
            <ta e="T2" id="Seg_705" s="T1">adj</ta>
            <ta e="T3" id="Seg_706" s="T2">n</ta>
            <ta e="T4" id="Seg_707" s="T3">n</ta>
            <ta e="T5" id="Seg_708" s="T4">v</ta>
            <ta e="T6" id="Seg_709" s="T5">n</ta>
            <ta e="T7" id="Seg_710" s="T6">n</ta>
            <ta e="T8" id="Seg_711" s="T7">v</ta>
            <ta e="T9" id="Seg_712" s="T8">pers</ta>
            <ta e="T10" id="Seg_713" s="T9">v</ta>
            <ta e="T11" id="Seg_714" s="T10">n</ta>
            <ta e="T12" id="Seg_715" s="T11">v</ta>
            <ta e="T13" id="Seg_716" s="T12">adv</ta>
            <ta e="T14" id="Seg_717" s="T13">n</ta>
            <ta e="T15" id="Seg_718" s="T14">v</ta>
            <ta e="T16" id="Seg_719" s="T15">n</ta>
            <ta e="T17" id="Seg_720" s="T16">n</ta>
            <ta e="T18" id="Seg_721" s="T17">v</ta>
            <ta e="T19" id="Seg_722" s="T18">pers</ta>
            <ta e="T20" id="Seg_723" s="T19">v</ta>
            <ta e="T21" id="Seg_724" s="T20">preverb</ta>
            <ta e="T22" id="Seg_725" s="T21">v</ta>
            <ta e="T23" id="Seg_726" s="T22">v</ta>
            <ta e="T24" id="Seg_727" s="T23">pers</ta>
            <ta e="T25" id="Seg_728" s="T24">num</ta>
            <ta e="T26" id="Seg_729" s="T25">n</ta>
            <ta e="T27" id="Seg_730" s="T26">v</ta>
            <ta e="T28" id="Seg_731" s="T27">n</ta>
            <ta e="T29" id="Seg_732" s="T28">v</ta>
            <ta e="T30" id="Seg_733" s="T29">adj</ta>
            <ta e="T31" id="Seg_734" s="T30">n</ta>
            <ta e="T32" id="Seg_735" s="T31">n</ta>
            <ta e="T33" id="Seg_736" s="T32">v</ta>
            <ta e="T34" id="Seg_737" s="T33">adv</ta>
            <ta e="T35" id="Seg_738" s="T34">v</ta>
            <ta e="T36" id="Seg_739" s="T35">adv</ta>
            <ta e="T37" id="Seg_740" s="T36">v</ta>
            <ta e="T38" id="Seg_741" s="T37">n</ta>
            <ta e="T39" id="Seg_742" s="T38">v</ta>
            <ta e="T40" id="Seg_743" s="T39">v</ta>
            <ta e="T41" id="Seg_744" s="T40">n</ta>
            <ta e="T42" id="Seg_745" s="T41">v</ta>
            <ta e="T43" id="Seg_746" s="T42">adv</ta>
            <ta e="T44" id="Seg_747" s="T43">v</ta>
            <ta e="T45" id="Seg_748" s="T44">n</ta>
            <ta e="T46" id="Seg_749" s="T45">quant</ta>
            <ta e="T47" id="Seg_750" s="T46">preverb</ta>
            <ta e="T48" id="Seg_751" s="T47">v</ta>
            <ta e="T49" id="Seg_752" s="T48">conj</ta>
            <ta e="T50" id="Seg_753" s="T49">n</ta>
            <ta e="T51" id="Seg_754" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_755" s="T51">v</ta>
            <ta e="T53" id="Seg_756" s="T52">n</ta>
            <ta e="T54" id="Seg_757" s="T53">adv</ta>
            <ta e="T55" id="Seg_758" s="T54">v</ta>
            <ta e="T56" id="Seg_759" s="T55">n</ta>
            <ta e="T57" id="Seg_760" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_761" s="T57">v</ta>
            <ta e="T59" id="Seg_762" s="T58">conj</ta>
            <ta e="T60" id="Seg_763" s="T59">n</ta>
            <ta e="T61" id="Seg_764" s="T60">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_765" s="T3">np:S</ta>
            <ta e="T5" id="Seg_766" s="T4">v:pred</ta>
            <ta e="T8" id="Seg_767" s="T6">s:purp</ta>
            <ta e="T9" id="Seg_768" s="T8">pro.h:S</ta>
            <ta e="T10" id="Seg_769" s="T9">v:pred</ta>
            <ta e="T11" id="Seg_770" s="T10">np:O</ta>
            <ta e="T12" id="Seg_771" s="T11">0.3.h:S v:pred</ta>
            <ta e="T14" id="Seg_772" s="T13">np:O</ta>
            <ta e="T15" id="Seg_773" s="T14">0.3.h:S v:pred</ta>
            <ta e="T16" id="Seg_774" s="T15">np:S</ta>
            <ta e="T17" id="Seg_775" s="T16">np:O</ta>
            <ta e="T18" id="Seg_776" s="T17">v:pred</ta>
            <ta e="T19" id="Seg_777" s="T18">pro:S</ta>
            <ta e="T20" id="Seg_778" s="T19">v:pred</ta>
            <ta e="T22" id="Seg_779" s="T21">0.3:S v:pred</ta>
            <ta e="T23" id="Seg_780" s="T22">0.3:S v:pred</ta>
            <ta e="T24" id="Seg_781" s="T23">pro.h:S</ta>
            <ta e="T26" id="Seg_782" s="T25">np:O</ta>
            <ta e="T27" id="Seg_783" s="T26">v:pred</ta>
            <ta e="T29" id="Seg_784" s="T28">0.3.h:S v:pred</ta>
            <ta e="T31" id="Seg_785" s="T30">np:O</ta>
            <ta e="T33" id="Seg_786" s="T32">0.3.h:S v:pred</ta>
            <ta e="T35" id="Seg_787" s="T33">s:purp</ta>
            <ta e="T37" id="Seg_788" s="T36">0.3.h:S v:pred</ta>
            <ta e="T38" id="Seg_789" s="T37">np:S</ta>
            <ta e="T39" id="Seg_790" s="T38">v:pred</ta>
            <ta e="T40" id="Seg_791" s="T39">0.3:S v:pred</ta>
            <ta e="T41" id="Seg_792" s="T40">np:S</ta>
            <ta e="T42" id="Seg_793" s="T41">v:pred</ta>
            <ta e="T44" id="Seg_794" s="T43">0.3:S v:pred</ta>
            <ta e="T45" id="Seg_795" s="T44">np:O</ta>
            <ta e="T48" id="Seg_796" s="T47">0.3:S v:pred</ta>
            <ta e="T50" id="Seg_797" s="T49">np:S</ta>
            <ta e="T52" id="Seg_798" s="T51">v:pred</ta>
            <ta e="T53" id="Seg_799" s="T52">pro.h:S</ta>
            <ta e="T55" id="Seg_800" s="T54">v:pred</ta>
            <ta e="T56" id="Seg_801" s="T55">np:S</ta>
            <ta e="T58" id="Seg_802" s="T57">v:pred</ta>
            <ta e="T60" id="Seg_803" s="T59">np:S</ta>
            <ta e="T61" id="Seg_804" s="T60">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_805" s="T0">adv:Time</ta>
            <ta e="T3" id="Seg_806" s="T2">np:So</ta>
            <ta e="T4" id="Seg_807" s="T3">np:A</ta>
            <ta e="T6" id="Seg_808" s="T5">np:G</ta>
            <ta e="T7" id="Seg_809" s="T6">np:P</ta>
            <ta e="T9" id="Seg_810" s="T8">pro.h:A</ta>
            <ta e="T11" id="Seg_811" s="T10">np:P</ta>
            <ta e="T12" id="Seg_812" s="T11">0.3.h:A</ta>
            <ta e="T13" id="Seg_813" s="T12">adv:G</ta>
            <ta e="T14" id="Seg_814" s="T13">np:Th</ta>
            <ta e="T15" id="Seg_815" s="T14">0.3.h:A</ta>
            <ta e="T16" id="Seg_816" s="T15">np:A</ta>
            <ta e="T17" id="Seg_817" s="T16">np:P</ta>
            <ta e="T19" id="Seg_818" s="T18">pro:E</ta>
            <ta e="T22" id="Seg_819" s="T21">0.3:P</ta>
            <ta e="T23" id="Seg_820" s="T22">0.3:P</ta>
            <ta e="T24" id="Seg_821" s="T23">pro.h:A</ta>
            <ta e="T26" id="Seg_822" s="T25">np:Th</ta>
            <ta e="T28" id="Seg_823" s="T27">np:Ins</ta>
            <ta e="T29" id="Seg_824" s="T28">0.3.h:A</ta>
            <ta e="T31" id="Seg_825" s="T30">np:Th</ta>
            <ta e="T32" id="Seg_826" s="T31">np:G</ta>
            <ta e="T33" id="Seg_827" s="T32">0.3.h:A</ta>
            <ta e="T34" id="Seg_828" s="T33">adv:G</ta>
            <ta e="T36" id="Seg_829" s="T35">adv:G</ta>
            <ta e="T37" id="Seg_830" s="T36">0.3.h:A</ta>
            <ta e="T38" id="Seg_831" s="T37">np:E</ta>
            <ta e="T40" id="Seg_832" s="T39">0.3:A</ta>
            <ta e="T41" id="Seg_833" s="T40">np:E</ta>
            <ta e="T44" id="Seg_834" s="T43">0.3:A</ta>
            <ta e="T45" id="Seg_835" s="T44">np:P</ta>
            <ta e="T48" id="Seg_836" s="T47">0.3:A</ta>
            <ta e="T50" id="Seg_837" s="T49">np:A</ta>
            <ta e="T53" id="Seg_838" s="T52">pro.h:A</ta>
            <ta e="T56" id="Seg_839" s="T55">np:Th</ta>
            <ta e="T60" id="Seg_840" s="T59">np:Th</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T6" id="Seg_841" s="T5">RUS:cult</ta>
            <ta e="T7" id="Seg_842" s="T6">RUS:cult</ta>
            <ta e="T28" id="Seg_843" s="T27">RUS:cult</ta>
            <ta e="T32" id="Seg_844" s="T31">RUS:cult</ta>
            <ta e="T45" id="Seg_845" s="T44">RUS:cult</ta>
            <ta e="T46" id="Seg_846" s="T45">RUS:core</ta>
            <ta e="T49" id="Seg_847" s="T48">RUS:gram</ta>
            <ta e="T56" id="Seg_848" s="T55">RUS:cult</ta>
            <ta e="T59" id="Seg_849" s="T58">RUS:gram</ta>
            <ta e="T61" id="Seg_850" s="T60">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T6" id="Seg_851" s="T5">Vsub</ta>
            <ta e="T32" id="Seg_852" s="T31">Csub</ta>
            <ta e="T61" id="Seg_853" s="T60">Csub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T6" id="Seg_854" s="T5">dir:infl</ta>
            <ta e="T7" id="Seg_855" s="T6">dir:infl</ta>
            <ta e="T32" id="Seg_856" s="T31">dir:infl</ta>
            <ta e="T45" id="Seg_857" s="T44">dir:bare</ta>
            <ta e="T56" id="Seg_858" s="T55">dir:bare</ta>
            <ta e="T61" id="Seg_859" s="T60">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T8" id="Seg_860" s="T0">Когда-то с Тарской стороны медведь пришёл к пчёлам мёд есть.</ta>
            <ta e="T10" id="Seg_861" s="T8">Они узнали про это.</ta>
            <ta e="T15" id="Seg_862" s="T10">Водку сделали и туда водку поставили.</ta>
            <ta e="T20" id="Seg_863" s="T15">Медведь водку выпил и пьяным стал.</ta>
            <ta e="T23" id="Seg_864" s="T20">Он упал и заснул.</ta>
            <ta e="T29" id="Seg_865" s="T23">Они двух лошадей запрягли и на телеге приехали.</ta>
            <ta e="T35" id="Seg_866" s="T29">Пьяного медведя на телегу положили, чтобы домой увезти.</ta>
            <ta e="T37" id="Seg_867" s="T35">Домой отправились.</ta>
            <ta e="T40" id="Seg_868" s="T37">Медведь проснулся, кричать стал.</ta>
            <ta e="T44" id="Seg_869" s="T40">Лошади испугались, поскакали(?).</ta>
            <ta e="T52" id="Seg_870" s="T44">Он сломал всю телегу, и медведь тоже ушёл.</ta>
            <ta e="T61" id="Seg_871" s="T52">Они домой пришли: телеги нет и медведя нет.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T8" id="Seg_872" s="T0">Once a bear came from that area to the bees to eat honey.</ta>
            <ta e="T10" id="Seg_873" s="T8">They found it out.</ta>
            <ta e="T15" id="Seg_874" s="T10">They made vodka and put it there.</ta>
            <ta e="T20" id="Seg_875" s="T15">The bear drank vodka and got drunk.</ta>
            <ta e="T23" id="Seg_876" s="T20">He fell down and fell asleep.</ta>
            <ta e="T29" id="Seg_877" s="T23">They harnessed two horses and left with a cart.</ta>
            <ta e="T35" id="Seg_878" s="T29">They put the drunk bear onto the cart to carry it home.</ta>
            <ta e="T37" id="Seg_879" s="T35">They went home.</ta>
            <ta e="T40" id="Seg_880" s="T37">The bear woke up and started to shout.</ta>
            <ta e="T44" id="Seg_881" s="T40">The horses got frightened and gallopped off.</ta>
            <ta e="T52" id="Seg_882" s="T44">He has broken the entire cart, and the bear was gone, too.</ta>
            <ta e="T61" id="Seg_883" s="T52">They came home: there is no cart and there is no bear as well.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T8" id="Seg_884" s="T0">Einmal kam ein Bär aus dieser Gegend zu den Bienen um Honig zu essen.</ta>
            <ta e="T10" id="Seg_885" s="T8">Sie fanden es heraus.</ta>
            <ta e="T15" id="Seg_886" s="T10">Sie machten Wodka und taten ihn dorthin.</ta>
            <ta e="T20" id="Seg_887" s="T15">Der Bär trank Wodka und wurde betrunken.</ta>
            <ta e="T23" id="Seg_888" s="T20">Er fiel zu Boden und schlief ein.</ta>
            <ta e="T29" id="Seg_889" s="T23">Sie spannten zwei Pferde an und fuhren mit dem Wagen weg.</ta>
            <ta e="T35" id="Seg_890" s="T29">Sie legten den betrunkenen Bären auf den Wagen um ihn nach Hause zu bringen.</ta>
            <ta e="T37" id="Seg_891" s="T35">Sie fuhren nach Hause.</ta>
            <ta e="T40" id="Seg_892" s="T37">Der Bär wachte auf und begann zu schreien.</ta>
            <ta e="T44" id="Seg_893" s="T40">Die Pferde bekamen Angst und rannten davon.</ta>
            <ta e="T52" id="Seg_894" s="T44">Er hat den Wagen ganz beschädigt, der Bär war auch weggelaufen.</ta>
            <ta e="T61" id="Seg_895" s="T52">Sie kamen nach Hause: es gibt keinen Wagen und auch keinen Bären (mehr).</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T8" id="Seg_896" s="T0">раньше с Тарской стороны медведь пришёл к пчёлам мёд есть</ta>
            <ta e="T10" id="Seg_897" s="T8">они узнали</ta>
            <ta e="T15" id="Seg_898" s="T10">водку (самогон) сделали туда водку поставили</ta>
            <ta e="T20" id="Seg_899" s="T15">медведь водку выпил он пьяный стал</ta>
            <ta e="T23" id="Seg_900" s="T20">упал заснул</ta>
            <ta e="T29" id="Seg_901" s="T23">они двух лошадей запрягли на телеге приехали</ta>
            <ta e="T35" id="Seg_902" s="T29">пьяного медведя на телегу положили домой увезти</ta>
            <ta e="T37" id="Seg_903" s="T35">домой отправились</ta>
            <ta e="T40" id="Seg_904" s="T37">медведь проснулся [разбудился] кричать стал</ta>
            <ta e="T44" id="Seg_905" s="T40">лошади испугались скоком побежали</ta>
            <ta e="T52" id="Seg_906" s="T44">телега вся сломалась и медведь тоже ушёл</ta>
            <ta e="T61" id="Seg_907" s="T52">они домой пришли телеги нет и медведя нет</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T20" id="Seg_908" s="T15">[WNB:] Why is it in objective conjugation?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
