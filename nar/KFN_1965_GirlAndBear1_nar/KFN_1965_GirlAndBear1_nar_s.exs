<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KFN_1965_GirlAndBear_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KFN_1965_GirlAndBear1_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">174</ud-information>
            <ud-information attribute-name="# HIAT:w">138</ud-information>
            <ud-information attribute-name="# e">138</ud-information>
            <ud-information attribute-name="# HIAT:u">28</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KFN">
            <abbreviation>KFN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T138" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KFN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T137" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">nadäk</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">qorqup</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">čaǯimbad</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">qandəqət</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">nälʼgut</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">mattə</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">qaːlɨmbat</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">aratə</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">maǯʼondə</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">qwanbat</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_41" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">aː</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">qor</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">həroqɨt</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">qorga</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">tömba</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">mat</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_62" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">nälʼqut</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">šoqort</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">nʼaj</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">omdelʼǯimbat</ts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_77" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">na</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">šoqor</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">poneqɨlʼ</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">šoqor</ts>
                  <nts id="Seg_89" n="HIAT:ip">.</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_92" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">qorg</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_97" n="HIAT:w" s="T25">na</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_100" n="HIAT:w" s="T26">šoqort</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">panalʼbat</ts>
                  <nts id="Seg_104" n="HIAT:ip">.</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_107" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_109" n="HIAT:w" s="T28">nʼaip</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_112" n="HIAT:w" s="T29">wes</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">ambat</ts>
                  <nts id="Seg_116" n="HIAT:ip">,</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_119" n="HIAT:w" s="T31">nʼalʼalʼ</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_122" n="HIAT:w" s="T32">nʼaj</ts>
                  <nts id="Seg_123" n="HIAT:ip">.</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_126" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_128" n="HIAT:w" s="T33">okɨr</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_131" n="HIAT:w" s="T34">kanak</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_134" n="HIAT:w" s="T35">artpətɨlʼ</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_137" n="HIAT:w" s="T36">eppa</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_141" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_143" n="HIAT:w" s="T37">nam</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_146" n="HIAT:w" s="T38">näj</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_149" n="HIAT:w" s="T39">qorq</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_152" n="HIAT:w" s="T40">ambat</ts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_156" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_158" n="HIAT:w" s="T41">kananando</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_161" n="HIAT:w" s="T42">okɨr</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_164" n="HIAT:w" s="T43">tälʼǯʼida</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_167" n="HIAT:w" s="T44">qaːlɨmba</ts>
                  <nts id="Seg_168" n="HIAT:ip">.</nts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_171" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_173" n="HIAT:w" s="T45">košünent</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_176" n="HIAT:w" s="T46">šeːrba</ts>
                  <nts id="Seg_177" n="HIAT:ip">.</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_180" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_182" n="HIAT:w" s="T47">moqalʼ</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_185" n="HIAT:w" s="T48">qoǯap</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_188" n="HIAT:w" s="T49">taq</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_191" n="HIAT:w" s="T50">wes</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_194" n="HIAT:w" s="T51">niškɨlʼbat</ts>
                  <nts id="Seg_195" n="HIAT:ip">.</nts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_198" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_200" n="HIAT:w" s="T52">wes</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_203" n="HIAT:w" s="T53">moqa</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_206" n="HIAT:w" s="T54">lʼaqkalʼǯʼimbat</ts>
                  <nts id="Seg_207" n="HIAT:ip">,</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_210" n="HIAT:w" s="T55">onǯʼe</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_213" n="HIAT:w" s="T56">moqeː</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_216" n="HIAT:w" s="T57">čaqə</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_219" n="HIAT:w" s="T58">eǯʼimba</ts>
                  <nts id="Seg_220" n="HIAT:ip">.</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_223" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_225" n="HIAT:w" s="T59">nälʼqulat</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_228" n="HIAT:w" s="T60">popolʼǯ</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_231" n="HIAT:w" s="T61">aːtelʼǯimbat</ts>
                  <nts id="Seg_232" n="HIAT:ip">,</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_235" n="HIAT:w" s="T62">okur</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_238" n="HIAT:w" s="T63">nädek</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_241" n="HIAT:w" s="T64">olɨm</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_244" n="HIAT:w" s="T65">mešpelɨmbat</ts>
                  <nts id="Seg_245" n="HIAT:ip">.</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_248" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_250" n="HIAT:w" s="T66">olʼɨm</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_253" n="HIAT:w" s="T67">meːmbat</ts>
                  <nts id="Seg_254" n="HIAT:ip">,</nts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_257" n="HIAT:w" s="T68">tülʼdep</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_260" n="HIAT:w" s="T69">aːbetɨmbat</ts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_264" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_266" n="HIAT:w" s="T70">qorq</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_269" n="HIAT:w" s="T71">akoškandɨ</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_272" n="HIAT:w" s="T72">nʼulʼeǯʼemba</ts>
                  <nts id="Seg_273" n="HIAT:ip">,</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_276" n="HIAT:w" s="T73">maːtɨm</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_279" n="HIAT:w" s="T74">mannɨmbɨgu</ts>
                  <nts id="Seg_280" n="HIAT:ip">.</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_283" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_285" n="HIAT:w" s="T75">tap</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_288" n="HIAT:w" s="T76">na</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_291" n="HIAT:w" s="T77">nädek</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_294" n="HIAT:w" s="T78">qɨːnolʼ</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_297" n="HIAT:w" s="T138">ǯʼak</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_300" n="HIAT:w" s="T79">čaǯembek</ts>
                  <nts id="Seg_301" n="HIAT:ip">.</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_304" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_306" n="HIAT:w" s="T80">qorq</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_309" n="HIAT:w" s="T81">ilʼlʼe</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_312" n="HIAT:w" s="T82">aːlʼčimba</ts>
                  <nts id="Seg_313" n="HIAT:ip">.</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_316" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_318" n="HIAT:w" s="T83">patom</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_321" n="HIAT:w" s="T84">qorqɨp</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_324" n="HIAT:w" s="T85">taqkɨrgu</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_327" n="HIAT:w" s="T86">okkɨr</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_330" n="HIAT:w" s="T87">nalʼgut</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_333" n="HIAT:w" s="T88">üdembat</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_336" n="HIAT:w" s="T89">šidaːro</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_339" n="HIAT:w" s="T90">werstand</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_342" n="HIAT:w" s="T91">täbɨlʼ</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_345" n="HIAT:w" s="T92">qup</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_348" n="HIAT:w" s="T93">qwärgu</ts>
                  <nts id="Seg_349" n="HIAT:ip">.</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_352" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_354" n="HIAT:w" s="T94">täbɨlqut</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_357" n="HIAT:w" s="T95">načaqɨndo</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_360" n="HIAT:w" s="T96">töːmbat</ts>
                  <nts id="Seg_361" n="HIAT:ip">.</nts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_364" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_366" n="HIAT:w" s="T97">qorqɨp</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_369" n="HIAT:w" s="T98">taq</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_372" n="HIAT:w" s="T99">kɨrɨmbat</ts>
                  <nts id="Seg_373" n="HIAT:ip">,</nts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_376" n="HIAT:w" s="T100">mužurumbat</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_379" n="HIAT:w" s="T101">i</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_382" n="HIAT:w" s="T102">ambadet</ts>
                  <nts id="Seg_383" n="HIAT:ip">.</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_386" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_388" n="HIAT:w" s="T103">a</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_391" n="HIAT:w" s="T104">qobound</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_394" n="HIAT:w" s="T105">čondɨš</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_397" n="HIAT:w" s="T106">meːmbad</ts>
                  <nts id="Seg_398" n="HIAT:ip">.</nts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_401" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_403" n="HIAT:w" s="T107">na</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_406" n="HIAT:w" s="T108">nädeq</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_409" n="HIAT:w" s="T109">qorqɨp</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_412" n="HIAT:w" s="T110">čaǯebɨlʼ</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_415" n="HIAT:w" s="T111">sečas</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_418" n="HIAT:w" s="T112">näj</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_421" n="HIAT:w" s="T113">tar</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_424" n="HIAT:w" s="T114">eːlʼa</ts>
                  <nts id="Seg_425" n="HIAT:ip">.</nts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_428" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_430" n="HIAT:w" s="T115">qwärat</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_433" n="HIAT:w" s="T116">tabɨt</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_436" n="HIAT:w" s="T117">Lʼukerkatko</ts>
                  <nts id="Seg_437" n="HIAT:ip">.</nts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_440" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_442" n="HIAT:w" s="T118">tabɨnan</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_445" n="HIAT:w" s="T119">čuʒitɨlʼ</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_448" n="HIAT:w" s="T120">pudɨlʼǯe</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_451" n="HIAT:w" s="T121">warqə</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_454" n="HIAT:w" s="T122">nenʼäd</ts>
                  <nts id="Seg_455" n="HIAT:ip">,</nts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_458" n="HIAT:w" s="T123">tabə</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_461" n="HIAT:w" s="T124">ütqombadet</ts>
                  <nts id="Seg_462" n="HIAT:ip">,</nts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_465" n="HIAT:w" s="T125">šidaːro</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_468" n="HIAT:w" s="T126">werstand</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_471" n="HIAT:w" s="T127">kajamba</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_474" n="HIAT:w" s="T128">tabequt</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_477" n="HIAT:w" s="T129">qwärgu</ts>
                  <nts id="Seg_478" n="HIAT:ip">.</nts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_481" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_483" n="HIAT:w" s="T130">tabə</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_486" n="HIAT:w" s="T131">qwärɨmbat</ts>
                  <nts id="Seg_487" n="HIAT:ip">.</nts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_490" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_492" n="HIAT:w" s="T132">toʒe</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_495" n="HIAT:w" s="T133">sečas</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_498" n="HIAT:w" s="T134">näj</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_501" n="HIAT:w" s="T135">tar</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_504" n="HIAT:w" s="T136">eːlʼa</ts>
                  <nts id="Seg_505" n="HIAT:ip">.</nts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T137" id="Seg_507" n="sc" s="T0">
               <ts e="T1" id="Seg_509" n="e" s="T0">nadäk </ts>
               <ts e="T2" id="Seg_511" n="e" s="T1">qorqup </ts>
               <ts e="T3" id="Seg_513" n="e" s="T2">čaǯimbad. </ts>
               <ts e="T4" id="Seg_515" n="e" s="T3">qandəqət </ts>
               <ts e="T5" id="Seg_517" n="e" s="T4">nälʼgut </ts>
               <ts e="T6" id="Seg_519" n="e" s="T5">mattə </ts>
               <ts e="T7" id="Seg_521" n="e" s="T6">qaːlɨmbat. </ts>
               <ts e="T8" id="Seg_523" n="e" s="T7">aratə </ts>
               <ts e="T9" id="Seg_525" n="e" s="T8">maǯʼondə </ts>
               <ts e="T10" id="Seg_527" n="e" s="T9">qwanbat. </ts>
               <ts e="T11" id="Seg_529" n="e" s="T10">aː </ts>
               <ts e="T12" id="Seg_531" n="e" s="T11">qor </ts>
               <ts e="T13" id="Seg_533" n="e" s="T12">həroqɨt </ts>
               <ts e="T14" id="Seg_535" n="e" s="T13">qorga </ts>
               <ts e="T15" id="Seg_537" n="e" s="T14">tömba </ts>
               <ts e="T16" id="Seg_539" n="e" s="T15">mat. </ts>
               <ts e="T17" id="Seg_541" n="e" s="T16">nälʼqut </ts>
               <ts e="T18" id="Seg_543" n="e" s="T17">šoqort </ts>
               <ts e="T19" id="Seg_545" n="e" s="T18">nʼaj </ts>
               <ts e="T20" id="Seg_547" n="e" s="T19">omdelʼǯimbat. </ts>
               <ts e="T21" id="Seg_549" n="e" s="T20">na </ts>
               <ts e="T22" id="Seg_551" n="e" s="T21">šoqor </ts>
               <ts e="T23" id="Seg_553" n="e" s="T22">poneqɨlʼ </ts>
               <ts e="T24" id="Seg_555" n="e" s="T23">šoqor. </ts>
               <ts e="T25" id="Seg_557" n="e" s="T24">qorg </ts>
               <ts e="T26" id="Seg_559" n="e" s="T25">na </ts>
               <ts e="T27" id="Seg_561" n="e" s="T26">šoqort </ts>
               <ts e="T28" id="Seg_563" n="e" s="T27">panalʼbat. </ts>
               <ts e="T29" id="Seg_565" n="e" s="T28">nʼaip </ts>
               <ts e="T30" id="Seg_567" n="e" s="T29">wes </ts>
               <ts e="T31" id="Seg_569" n="e" s="T30">ambat, </ts>
               <ts e="T32" id="Seg_571" n="e" s="T31">nʼalʼalʼ </ts>
               <ts e="T33" id="Seg_573" n="e" s="T32">nʼaj. </ts>
               <ts e="T34" id="Seg_575" n="e" s="T33">okɨr </ts>
               <ts e="T35" id="Seg_577" n="e" s="T34">kanak </ts>
               <ts e="T36" id="Seg_579" n="e" s="T35">artpətɨlʼ </ts>
               <ts e="T37" id="Seg_581" n="e" s="T36">eppa. </ts>
               <ts e="T38" id="Seg_583" n="e" s="T37">nam </ts>
               <ts e="T39" id="Seg_585" n="e" s="T38">näj </ts>
               <ts e="T40" id="Seg_587" n="e" s="T39">qorq </ts>
               <ts e="T41" id="Seg_589" n="e" s="T40">ambat. </ts>
               <ts e="T42" id="Seg_591" n="e" s="T41">kananando </ts>
               <ts e="T43" id="Seg_593" n="e" s="T42">okɨr </ts>
               <ts e="T44" id="Seg_595" n="e" s="T43">tälʼǯʼida </ts>
               <ts e="T45" id="Seg_597" n="e" s="T44">qaːlɨmba. </ts>
               <ts e="T46" id="Seg_599" n="e" s="T45">košünent </ts>
               <ts e="T47" id="Seg_601" n="e" s="T46">šeːrba. </ts>
               <ts e="T48" id="Seg_603" n="e" s="T47">moqalʼ </ts>
               <ts e="T49" id="Seg_605" n="e" s="T48">qoǯap </ts>
               <ts e="T50" id="Seg_607" n="e" s="T49">taq </ts>
               <ts e="T51" id="Seg_609" n="e" s="T50">wes </ts>
               <ts e="T52" id="Seg_611" n="e" s="T51">niškɨlʼbat. </ts>
               <ts e="T53" id="Seg_613" n="e" s="T52">wes </ts>
               <ts e="T54" id="Seg_615" n="e" s="T53">moqa </ts>
               <ts e="T55" id="Seg_617" n="e" s="T54">lʼaqkalʼǯʼimbat, </ts>
               <ts e="T56" id="Seg_619" n="e" s="T55">onǯʼe </ts>
               <ts e="T57" id="Seg_621" n="e" s="T56">moqeː </ts>
               <ts e="T58" id="Seg_623" n="e" s="T57">čaqə </ts>
               <ts e="T59" id="Seg_625" n="e" s="T58">eǯʼimba. </ts>
               <ts e="T60" id="Seg_627" n="e" s="T59">nälʼqulat </ts>
               <ts e="T61" id="Seg_629" n="e" s="T60">popolʼǯ </ts>
               <ts e="T62" id="Seg_631" n="e" s="T61">aːtelʼǯimbat, </ts>
               <ts e="T63" id="Seg_633" n="e" s="T62">okur </ts>
               <ts e="T64" id="Seg_635" n="e" s="T63">nädek </ts>
               <ts e="T65" id="Seg_637" n="e" s="T64">olɨm </ts>
               <ts e="T66" id="Seg_639" n="e" s="T65">mešpelɨmbat. </ts>
               <ts e="T67" id="Seg_641" n="e" s="T66">olʼɨm </ts>
               <ts e="T68" id="Seg_643" n="e" s="T67">meːmbat, </ts>
               <ts e="T69" id="Seg_645" n="e" s="T68">tülʼdep </ts>
               <ts e="T70" id="Seg_647" n="e" s="T69">aːbetɨmbat. </ts>
               <ts e="T71" id="Seg_649" n="e" s="T70">qorq </ts>
               <ts e="T72" id="Seg_651" n="e" s="T71">akoškandɨ </ts>
               <ts e="T73" id="Seg_653" n="e" s="T72">nʼulʼeǯʼemba, </ts>
               <ts e="T74" id="Seg_655" n="e" s="T73">maːtɨm </ts>
               <ts e="T75" id="Seg_657" n="e" s="T74">mannɨmbɨgu. </ts>
               <ts e="T76" id="Seg_659" n="e" s="T75">tap </ts>
               <ts e="T77" id="Seg_661" n="e" s="T76">na </ts>
               <ts e="T78" id="Seg_663" n="e" s="T77">nädek </ts>
               <ts e="T138" id="Seg_665" n="e" s="T78">qɨːnolʼ </ts>
               <ts e="T79" id="Seg_667" n="e" s="T138">ǯʼak </ts>
               <ts e="T80" id="Seg_669" n="e" s="T79">čaǯembek. </ts>
               <ts e="T81" id="Seg_671" n="e" s="T80">qorq </ts>
               <ts e="T82" id="Seg_673" n="e" s="T81">ilʼlʼe </ts>
               <ts e="T83" id="Seg_675" n="e" s="T82">aːlʼčimba. </ts>
               <ts e="T84" id="Seg_677" n="e" s="T83">patom </ts>
               <ts e="T85" id="Seg_679" n="e" s="T84">qorqɨp </ts>
               <ts e="T86" id="Seg_681" n="e" s="T85">taqkɨrgu </ts>
               <ts e="T87" id="Seg_683" n="e" s="T86">okkɨr </ts>
               <ts e="T88" id="Seg_685" n="e" s="T87">nalʼgut </ts>
               <ts e="T89" id="Seg_687" n="e" s="T88">üdembat </ts>
               <ts e="T90" id="Seg_689" n="e" s="T89">šidaːro </ts>
               <ts e="T91" id="Seg_691" n="e" s="T90">werstand </ts>
               <ts e="T92" id="Seg_693" n="e" s="T91">täbɨlʼ </ts>
               <ts e="T93" id="Seg_695" n="e" s="T92">qup </ts>
               <ts e="T94" id="Seg_697" n="e" s="T93">qwärgu. </ts>
               <ts e="T95" id="Seg_699" n="e" s="T94">täbɨlqut </ts>
               <ts e="T96" id="Seg_701" n="e" s="T95">načaqɨndo </ts>
               <ts e="T97" id="Seg_703" n="e" s="T96">töːmbat. </ts>
               <ts e="T98" id="Seg_705" n="e" s="T97">qorqɨp </ts>
               <ts e="T99" id="Seg_707" n="e" s="T98">taq </ts>
               <ts e="T100" id="Seg_709" n="e" s="T99">kɨrɨmbat, </ts>
               <ts e="T101" id="Seg_711" n="e" s="T100">mužurumbat </ts>
               <ts e="T102" id="Seg_713" n="e" s="T101">i </ts>
               <ts e="T103" id="Seg_715" n="e" s="T102">ambadet. </ts>
               <ts e="T104" id="Seg_717" n="e" s="T103">a </ts>
               <ts e="T105" id="Seg_719" n="e" s="T104">qobound </ts>
               <ts e="T106" id="Seg_721" n="e" s="T105">čondɨš </ts>
               <ts e="T107" id="Seg_723" n="e" s="T106">meːmbad. </ts>
               <ts e="T108" id="Seg_725" n="e" s="T107">na </ts>
               <ts e="T109" id="Seg_727" n="e" s="T108">nädeq </ts>
               <ts e="T110" id="Seg_729" n="e" s="T109">qorqɨp </ts>
               <ts e="T111" id="Seg_731" n="e" s="T110">čaǯebɨlʼ </ts>
               <ts e="T112" id="Seg_733" n="e" s="T111">sečas </ts>
               <ts e="T113" id="Seg_735" n="e" s="T112">näj </ts>
               <ts e="T114" id="Seg_737" n="e" s="T113">tar </ts>
               <ts e="T115" id="Seg_739" n="e" s="T114">eːlʼa. </ts>
               <ts e="T116" id="Seg_741" n="e" s="T115">qwärat </ts>
               <ts e="T117" id="Seg_743" n="e" s="T116">tabɨt </ts>
               <ts e="T118" id="Seg_745" n="e" s="T117">Lʼukerkatko. </ts>
               <ts e="T119" id="Seg_747" n="e" s="T118">tabɨnan </ts>
               <ts e="T120" id="Seg_749" n="e" s="T119">čuʒitɨlʼ </ts>
               <ts e="T121" id="Seg_751" n="e" s="T120">pudɨlʼǯe </ts>
               <ts e="T122" id="Seg_753" n="e" s="T121">warqə </ts>
               <ts e="T123" id="Seg_755" n="e" s="T122">nenʼäd, </ts>
               <ts e="T124" id="Seg_757" n="e" s="T123">tabə </ts>
               <ts e="T125" id="Seg_759" n="e" s="T124">ütqombadet, </ts>
               <ts e="T126" id="Seg_761" n="e" s="T125">šidaːro </ts>
               <ts e="T127" id="Seg_763" n="e" s="T126">werstand </ts>
               <ts e="T128" id="Seg_765" n="e" s="T127">kajamba </ts>
               <ts e="T129" id="Seg_767" n="e" s="T128">tabequt </ts>
               <ts e="T130" id="Seg_769" n="e" s="T129">qwärgu. </ts>
               <ts e="T131" id="Seg_771" n="e" s="T130">tabə </ts>
               <ts e="T132" id="Seg_773" n="e" s="T131">qwärɨmbat. </ts>
               <ts e="T133" id="Seg_775" n="e" s="T132">toʒe </ts>
               <ts e="T134" id="Seg_777" n="e" s="T133">sečas </ts>
               <ts e="T135" id="Seg_779" n="e" s="T134">näj </ts>
               <ts e="T136" id="Seg_781" n="e" s="T135">tar </ts>
               <ts e="T137" id="Seg_783" n="e" s="T136">eːlʼa. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_784" s="T0">KFN_1965_GirlAndBear1_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_785" s="T3">KFN_1965_GirlAndBear1_nar.002 (001.002)</ta>
            <ta e="T10" id="Seg_786" s="T7">KFN_1965_GirlAndBear1_nar.003 (001.003)</ta>
            <ta e="T16" id="Seg_787" s="T10">KFN_1965_GirlAndBear1_nar.004 (001.004)</ta>
            <ta e="T20" id="Seg_788" s="T16">KFN_1965_GirlAndBear1_nar.005 (001.005)</ta>
            <ta e="T24" id="Seg_789" s="T20">KFN_1965_GirlAndBear1_nar.006 (001.006)</ta>
            <ta e="T28" id="Seg_790" s="T24">KFN_1965_GirlAndBear1_nar.007 (001.007)</ta>
            <ta e="T33" id="Seg_791" s="T28">KFN_1965_GirlAndBear1_nar.008 (001.008)</ta>
            <ta e="T37" id="Seg_792" s="T33">KFN_1965_GirlAndBear1_nar.009 (001.009)</ta>
            <ta e="T41" id="Seg_793" s="T37">KFN_1965_GirlAndBear1_nar.010 (001.010)</ta>
            <ta e="T45" id="Seg_794" s="T41">KFN_1965_GirlAndBear1_nar.011 (001.011)</ta>
            <ta e="T47" id="Seg_795" s="T45">KFN_1965_GirlAndBear1_nar.012 (001.012)</ta>
            <ta e="T52" id="Seg_796" s="T47">KFN_1965_GirlAndBear1_nar.013 (001.013)</ta>
            <ta e="T59" id="Seg_797" s="T52">KFN_1965_GirlAndBear1_nar.014 (001.014)</ta>
            <ta e="T66" id="Seg_798" s="T59">KFN_1965_GirlAndBear1_nar.015 (001.015)</ta>
            <ta e="T70" id="Seg_799" s="T66">KFN_1965_GirlAndBear1_nar.016 (001.016)</ta>
            <ta e="T75" id="Seg_800" s="T70">KFN_1965_GirlAndBear1_nar.017 (001.017)</ta>
            <ta e="T80" id="Seg_801" s="T75">KFN_1965_GirlAndBear1_nar.018 (001.018)</ta>
            <ta e="T83" id="Seg_802" s="T80">KFN_1965_GirlAndBear1_nar.019 (001.019)</ta>
            <ta e="T94" id="Seg_803" s="T83">KFN_1965_GirlAndBear1_nar.020 (001.020)</ta>
            <ta e="T97" id="Seg_804" s="T94">KFN_1965_GirlAndBear1_nar.021 (001.021)</ta>
            <ta e="T103" id="Seg_805" s="T97">KFN_1965_GirlAndBear1_nar.022 (001.022)</ta>
            <ta e="T107" id="Seg_806" s="T103">KFN_1965_GirlAndBear1_nar.023 (001.023)</ta>
            <ta e="T115" id="Seg_807" s="T107">KFN_1965_GirlAndBear1_nar.024 (001.024)</ta>
            <ta e="T118" id="Seg_808" s="T115">KFN_1965_GirlAndBear1_nar.025 (001.025)</ta>
            <ta e="T130" id="Seg_809" s="T118">KFN_1965_GirlAndBear1_nar.026 (001.026)</ta>
            <ta e="T132" id="Seg_810" s="T130">KFN_1965_GirlAndBear1_nar.027 (001.027)</ta>
            <ta e="T137" id="Seg_811" s="T132">KFN_1965_GirlAndBear1_nar.028 (001.028)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_812" s="T0">на′дӓк kорɣуп чаджимбад.</ta>
            <ta e="T7" id="Seg_813" s="T3">′kандъɣът нӓlгут маттъ kа̄лымбат.</ta>
            <ta e="T10" id="Seg_814" s="T7">′аратъ ма′дʼжʼондъ kwан′бат.</ta>
            <ta e="T16" id="Seg_815" s="T10">а̄ k[ɣ]ор hъ′роɣыт ′kорга тӧмба мат.</ta>
            <ta e="T20" id="Seg_816" s="T16">′нӓlkут шо′ɣорт ′нʼай ‵омдеlджи′мбат.</ta>
            <ta e="T24" id="Seg_817" s="T20">на шо′ɣор ′понеɣыl шо′ɣор.</ta>
            <ta e="T28" id="Seg_818" s="T24">kорг на шо′ɣ[k]орт па′наlбат.</ta>
            <ta e="T33" id="Seg_819" s="T28">′нʼаип вес ′амбат, ′нʼаlаl нʼай.</ta>
            <ta e="T37" id="Seg_820" s="T33">окыр ка′нак артпътыl е′ппа.</ta>
            <ta e="T41" id="Seg_821" s="T37">нам нӓй kорɣ ам′бат.</ta>
            <ta e="T45" id="Seg_822" s="T41">ка′нанандо окыр тӓlдʼжʼида kа̄лымба.</ta>
            <ta e="T47" id="Seg_823" s="T45">кошӱнент ше̄рба.</ta>
            <ta e="T52" id="Seg_824" s="T47">мо′ɣаl kо′джап таk вес нишкыl[л]′бат.</ta>
            <ta e="T59" id="Seg_825" s="T52">вес мо′ɣа lаkкаlдʼжʼимбат, ондʼжʼе мо′ɣе̄ ′чаɣъ едʼжʼимба.</ta>
            <ta e="T66" id="Seg_826" s="T59">нӓlkулат по′поlдж а̄′теlджимбат, окур нӓ′дек ′олым ′мешпелымбат.</ta>
            <ta e="T70" id="Seg_827" s="T66">оlым ′ме̄мбат, тӱlдеп а̄бетым′бат.</ta>
            <ta e="T75" id="Seg_828" s="T70">kорɣ акошканды ′нʼуlедʼжʼемба, ма̄тым маннымбыгу.</ta>
            <ta e="T80" id="Seg_829" s="T75">тап ′на нӓ′дек kы̄ноl′дʼжʼак ′чаджембек.</ta>
            <ta e="T83" id="Seg_830" s="T80">kорɣ и′llе а̄lчимба.</ta>
            <ta e="T94" id="Seg_831" s="T83">патом kорɣып таk′кыр′гу оккыр наl′гут ′ӱдембат ши′да̄ро вер′станд ′тӓбыl kуп ′kвӓргу.</ta>
            <ta e="T97" id="Seg_832" s="T94">тӓбылkут на′чаɣындо тӧ̄мбат.</ta>
            <ta e="T103" id="Seg_833" s="T97">kорɣып таk кырымбат, ′мужурумбат и ′амбадет.</ta>
            <ta e="T107" id="Seg_834" s="T103">а kобоунд чондыш ′ме̄мбад.</ta>
            <ta e="T115" id="Seg_835" s="T107">на нӓ′деk kорɣып чаджебыl сечас нӓй тар е̄lа.</ta>
            <ta e="T118" id="Seg_836" s="T115">kwӓрат табыт lу′керкатко.</ta>
            <ta e="T130" id="Seg_837" s="T118">табынан чужитыl ′пудыlдже варɣъ не′нʼӓд, табъ ӱтkомбадет, шида̄ро верстанд каjамба табеkут ′kwӓргу.</ta>
            <ta e="T132" id="Seg_838" s="T130">табъ ′kwӓрымбат.</ta>
            <ta e="T137" id="Seg_839" s="T132">тоже сечас нӓй тар е̄′lа.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_840" s="T0">nadäk qorqup čaǯimbad.</ta>
            <ta e="T7" id="Seg_841" s="T3">qandəqət nälʼgut mattə qaːlɨmbat.</ta>
            <ta e="T10" id="Seg_842" s="T7">aratə maǯʼondə qwanbat.</ta>
            <ta e="T16" id="Seg_843" s="T10">aː q[q]or həroqɨt qorga tömba mat.</ta>
            <ta e="T20" id="Seg_844" s="T16">nälʼqut šoqort nʼaj omdelʼǯimbat.</ta>
            <ta e="T24" id="Seg_845" s="T20">na šoqor poneqɨlʼ šoqor.</ta>
            <ta e="T28" id="Seg_846" s="T24">qorg na šoq[q]ort panalʼbat.</ta>
            <ta e="T33" id="Seg_847" s="T28">nʼaip ves ambat, nʼalʼalʼ nʼaj.</ta>
            <ta e="T37" id="Seg_848" s="T33">okɨr kanak artpətɨlʼ eppa.</ta>
            <ta e="T41" id="Seg_849" s="T37">nam näj qorq ambat.</ta>
            <ta e="T45" id="Seg_850" s="T41">kananando okɨr tälʼdʼžʼida qaːlɨmba.</ta>
            <ta e="T47" id="Seg_851" s="T45">košünent šeːrba.</ta>
            <ta e="T52" id="Seg_852" s="T47">moqalʼ qodžap taq ves niškɨlʼ[l]bat.</ta>
            <ta e="T59" id="Seg_853" s="T52">ves moqa lʼaqkalʼdʼžʼimbat, ondʼžʼe moqeː čaqə edʼžʼimba.</ta>
            <ta e="T66" id="Seg_854" s="T59">nälʼqulat popolʼdž aːtelʼdžimbat, okur nädek olɨm mešpelɨmbat.</ta>
            <ta e="T70" id="Seg_855" s="T66">olʼɨm meːmbat, tülʼdep aːbetɨmbat.</ta>
            <ta e="T75" id="Seg_856" s="T70">qorq akoškandɨ nʼulʼedʼžʼemba, maːtɨm mannɨmbɨgu.</ta>
            <ta e="T80" id="Seg_857" s="T75">tap na nädek qɨːnolʼdʼžʼak čadžembek.</ta>
            <ta e="T83" id="Seg_858" s="T80">qorq ilʼlʼe aːlʼčimba.</ta>
            <ta e="T94" id="Seg_859" s="T83">patom qorqɨp taqkɨrgu okkɨr nalʼgut üdembat šidaːro verstand täbɨlʼ qup qvärgu.</ta>
            <ta e="T97" id="Seg_860" s="T94">täbɨlqut načaqɨndo töːmbat.</ta>
            <ta e="T103" id="Seg_861" s="T97">qorqɨp taq kɨrɨmbat, mužurumbat i ambadet.</ta>
            <ta e="T107" id="Seg_862" s="T103">a qobound čondɨš meːmbad.</ta>
            <ta e="T115" id="Seg_863" s="T107">na nädeq qorqɨp čadžebɨlʼ sečas näj tar eːlʼa.</ta>
            <ta e="T118" id="Seg_864" s="T115">qwärat tabɨt lʼukerkatko.</ta>
            <ta e="T130" id="Seg_865" s="T118">tabɨnan čužitɨlʼ pudɨlʼdže varqə nenʼäd, tabə ütqombadet, šidaːro verstand kajamba tabequt qwärgu.</ta>
            <ta e="T132" id="Seg_866" s="T130">tabə qwärɨmbat.</ta>
            <ta e="T137" id="Seg_867" s="T132">tože sečas näj tar eːlʼa.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_868" s="T0">nadäk qorqup čaǯimbad. </ta>
            <ta e="T7" id="Seg_869" s="T3">qandəqət nälʼgut mattə qaːlɨmbat. </ta>
            <ta e="T10" id="Seg_870" s="T7">aratə maǯʼondə qwanbat. </ta>
            <ta e="T16" id="Seg_871" s="T10">aː qor həroqɨt qorga tömba mat. </ta>
            <ta e="T20" id="Seg_872" s="T16">nälʼqut šoqort nʼaj omdelʼǯimbat. </ta>
            <ta e="T24" id="Seg_873" s="T20">na šoqor poneqɨlʼ šoqor. </ta>
            <ta e="T28" id="Seg_874" s="T24">qorg na šoqort panalʼbat. </ta>
            <ta e="T33" id="Seg_875" s="T28">nʼaip wes ambat, nʼalʼalʼ nʼaj. </ta>
            <ta e="T37" id="Seg_876" s="T33">okɨr kanak artpətɨlʼ eppa. </ta>
            <ta e="T41" id="Seg_877" s="T37">nam näj qorq ambat. </ta>
            <ta e="T45" id="Seg_878" s="T41">kananando okɨr tälʼǯʼida qaːlɨmba. </ta>
            <ta e="T47" id="Seg_879" s="T45">košünent šeːrba. </ta>
            <ta e="T52" id="Seg_880" s="T47">moqalʼ qoǯap taq wes niškɨlʼbat. </ta>
            <ta e="T59" id="Seg_881" s="T52">wes moqa lʼaqkalʼǯʼimbat, onǯʼe moqeː čaqə eǯʼimba. </ta>
            <ta e="T66" id="Seg_882" s="T59">nälʼqulat popolʼǯ aːtelʼǯimbat, okur nädek olɨm mešpelɨmbat. </ta>
            <ta e="T70" id="Seg_883" s="T66">olʼɨm meːmbat, tülʼdep aːbetɨmbat. </ta>
            <ta e="T75" id="Seg_884" s="T70">qorq akoškandɨ nʼulʼeǯʼemba, maːtɨm mannɨmbɨgu. </ta>
            <ta e="T80" id="Seg_885" s="T75">tap na nädek qɨːnolʼ ǯʼak čaǯembek. </ta>
            <ta e="T83" id="Seg_886" s="T80">qorq ilʼlʼe aːlʼčimba. </ta>
            <ta e="T94" id="Seg_887" s="T83">patom qorqɨp taqkɨrgu okkɨr nalʼgut üdembat šidaːro werstand täbɨlʼ qup qwärgu. </ta>
            <ta e="T97" id="Seg_888" s="T94">täbɨlqut načaqɨndo töːmbat. </ta>
            <ta e="T103" id="Seg_889" s="T97">qorqɨp taq kɨrɨmbat, mužurumbat i ambadet. </ta>
            <ta e="T107" id="Seg_890" s="T103">a qobound čondɨš meːmbad. </ta>
            <ta e="T115" id="Seg_891" s="T107">na nädeq qorqɨp čaǯebɨlʼ sečas näj tar eːlʼa. </ta>
            <ta e="T118" id="Seg_892" s="T115">qwärat tabɨt Lʼukerkatko. </ta>
            <ta e="T130" id="Seg_893" s="T118">tabɨnan čuʒitɨlʼ pudɨlʼǯe warqə nenʼäd, tabə ütqombadet, šidaːro werstand kajamba tabequt qwärgu. </ta>
            <ta e="T132" id="Seg_894" s="T130">tabə qwärɨmbat. </ta>
            <ta e="T137" id="Seg_895" s="T132">toʒe sečas näj tar eːlʼa. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_896" s="T0">nadäk</ta>
            <ta e="T2" id="Seg_897" s="T1">qorqu-p</ta>
            <ta e="T3" id="Seg_898" s="T2">čaǯi-mba-d</ta>
            <ta e="T4" id="Seg_899" s="T3">qandə-qət</ta>
            <ta e="T5" id="Seg_900" s="T4">nä-lʼ-gu-t</ta>
            <ta e="T6" id="Seg_901" s="T5">mat-tə</ta>
            <ta e="T7" id="Seg_902" s="T6">qaːlɨ-mba-t</ta>
            <ta e="T8" id="Seg_903" s="T7">ara-tə</ta>
            <ta e="T9" id="Seg_904" s="T8">maǯʼo-ndə</ta>
            <ta e="T10" id="Seg_905" s="T9">qwan-ba-t</ta>
            <ta e="T11" id="Seg_906" s="T10">aː</ta>
            <ta e="T12" id="Seg_907" s="T11">qor</ta>
            <ta e="T13" id="Seg_908" s="T12">hər-o-qɨt</ta>
            <ta e="T14" id="Seg_909" s="T13">qorga</ta>
            <ta e="T15" id="Seg_910" s="T14">tö-mba</ta>
            <ta e="T16" id="Seg_911" s="T15">mat</ta>
            <ta e="T17" id="Seg_912" s="T16">nä-lʼ-qu-t</ta>
            <ta e="T18" id="Seg_913" s="T17">šoqor-t</ta>
            <ta e="T19" id="Seg_914" s="T18">nʼaj</ta>
            <ta e="T20" id="Seg_915" s="T19">omde-lʼǯi-mba-t</ta>
            <ta e="T21" id="Seg_916" s="T20">na</ta>
            <ta e="T22" id="Seg_917" s="T21">šoqor</ta>
            <ta e="T23" id="Seg_918" s="T22">pone-qɨ-lʼ</ta>
            <ta e="T24" id="Seg_919" s="T23">šoqor</ta>
            <ta e="T25" id="Seg_920" s="T24">qorg</ta>
            <ta e="T26" id="Seg_921" s="T25">na</ta>
            <ta e="T27" id="Seg_922" s="T26">šoqor-t</ta>
            <ta e="T28" id="Seg_923" s="T27">panalʼ-ba-t</ta>
            <ta e="T29" id="Seg_924" s="T28">nʼai-p</ta>
            <ta e="T30" id="Seg_925" s="T29">wes</ta>
            <ta e="T31" id="Seg_926" s="T30">am-ba-t</ta>
            <ta e="T32" id="Seg_927" s="T31">nʼalʼalʼ</ta>
            <ta e="T33" id="Seg_928" s="T32">nʼaj</ta>
            <ta e="T34" id="Seg_929" s="T33">okɨr</ta>
            <ta e="T35" id="Seg_930" s="T34">kanak</ta>
            <ta e="T36" id="Seg_931" s="T35">artpə-tɨlʼ</ta>
            <ta e="T37" id="Seg_932" s="T36">e-ppa</ta>
            <ta e="T38" id="Seg_933" s="T37">na-m</ta>
            <ta e="T39" id="Seg_934" s="T38">näj</ta>
            <ta e="T40" id="Seg_935" s="T39">qorq</ta>
            <ta e="T41" id="Seg_936" s="T40">am-ba-t</ta>
            <ta e="T42" id="Seg_937" s="T41">kana-nando</ta>
            <ta e="T43" id="Seg_938" s="T42">okɨr</ta>
            <ta e="T44" id="Seg_939" s="T43">tälʼǯʼi-da</ta>
            <ta e="T45" id="Seg_940" s="T44">qaːlɨ-mba</ta>
            <ta e="T46" id="Seg_941" s="T45">košüne-nt</ta>
            <ta e="T47" id="Seg_942" s="T46">šeːr-ba</ta>
            <ta e="T48" id="Seg_943" s="T47">moqa-lʼ</ta>
            <ta e="T49" id="Seg_944" s="T48">qoǯa-p</ta>
            <ta e="T50" id="Seg_945" s="T49">taq</ta>
            <ta e="T51" id="Seg_946" s="T50">wes</ta>
            <ta e="T52" id="Seg_947" s="T51">niškɨlʼ-ba-t</ta>
            <ta e="T53" id="Seg_948" s="T52">wes</ta>
            <ta e="T54" id="Seg_949" s="T53">moqa</ta>
            <ta e="T55" id="Seg_950" s="T54">lʼaqkalʼǯʼi-mba-t</ta>
            <ta e="T56" id="Seg_951" s="T55">onǯʼe</ta>
            <ta e="T57" id="Seg_952" s="T56">moqeː</ta>
            <ta e="T58" id="Seg_953" s="T57">čaqə</ta>
            <ta e="T59" id="Seg_954" s="T58">eǯʼi-mba</ta>
            <ta e="T60" id="Seg_955" s="T59">nä-lʼ-qu-la-t</ta>
            <ta e="T61" id="Seg_956" s="T60">popolʼ-ǯ</ta>
            <ta e="T62" id="Seg_957" s="T61">aːte-lʼǯi-mba-t</ta>
            <ta e="T63" id="Seg_958" s="T62">okur</ta>
            <ta e="T64" id="Seg_959" s="T63">nädek</ta>
            <ta e="T65" id="Seg_960" s="T64">olɨ-m</ta>
            <ta e="T66" id="Seg_961" s="T65">me-špe-lɨ-mba-t</ta>
            <ta e="T67" id="Seg_962" s="T66">olʼɨ-m</ta>
            <ta e="T68" id="Seg_963" s="T67">meː-mba-t</ta>
            <ta e="T69" id="Seg_964" s="T68">tülʼde-p</ta>
            <ta e="T70" id="Seg_965" s="T69">aːb-e-tɨ-mba-t</ta>
            <ta e="T71" id="Seg_966" s="T70">qorq</ta>
            <ta e="T72" id="Seg_967" s="T71">akoška-ndɨ</ta>
            <ta e="T73" id="Seg_968" s="T72">nʼulʼeǯʼe-mba</ta>
            <ta e="T74" id="Seg_969" s="T73">maːt-ɨ-m</ta>
            <ta e="T75" id="Seg_970" s="T74">mannɨ-mbɨ-gu</ta>
            <ta e="T76" id="Seg_971" s="T75">tap</ta>
            <ta e="T77" id="Seg_972" s="T76">na</ta>
            <ta e="T78" id="Seg_973" s="T77">nädek</ta>
            <ta e="T138" id="Seg_974" s="T78">qɨːnolʼ</ta>
            <ta e="T79" id="Seg_975" s="T138">ǯʼak</ta>
            <ta e="T80" id="Seg_976" s="T79">čaǯe-mbe-k</ta>
            <ta e="T81" id="Seg_977" s="T80">qorq</ta>
            <ta e="T82" id="Seg_978" s="T81">ilʼlʼe</ta>
            <ta e="T83" id="Seg_979" s="T82">aːlʼči-mba</ta>
            <ta e="T84" id="Seg_980" s="T83">patom</ta>
            <ta e="T85" id="Seg_981" s="T84">qorqɨ-p</ta>
            <ta e="T86" id="Seg_982" s="T85">taqkɨr-gu</ta>
            <ta e="T87" id="Seg_983" s="T86">okkɨr</ta>
            <ta e="T88" id="Seg_984" s="T87">na-lʼ-gu-t</ta>
            <ta e="T89" id="Seg_985" s="T88">üde-mba-t</ta>
            <ta e="T90" id="Seg_986" s="T89">šidaːro</ta>
            <ta e="T91" id="Seg_987" s="T90">wersta-nd</ta>
            <ta e="T92" id="Seg_988" s="T91">täbɨ-lʼ</ta>
            <ta e="T93" id="Seg_989" s="T92">qup</ta>
            <ta e="T94" id="Seg_990" s="T93">qwär-gu</ta>
            <ta e="T95" id="Seg_991" s="T94">täbɨ-l-qu-t</ta>
            <ta e="T96" id="Seg_992" s="T95">nača-qɨndo</ta>
            <ta e="T97" id="Seg_993" s="T96">töː-mba-t</ta>
            <ta e="T98" id="Seg_994" s="T97">qorqɨ-p</ta>
            <ta e="T99" id="Seg_995" s="T98">taq</ta>
            <ta e="T100" id="Seg_996" s="T99">kɨrɨ-mba-t</ta>
            <ta e="T101" id="Seg_997" s="T100">mužuru-mba-t</ta>
            <ta e="T102" id="Seg_998" s="T101">i</ta>
            <ta e="T103" id="Seg_999" s="T102">am-ba-det</ta>
            <ta e="T104" id="Seg_1000" s="T103">a</ta>
            <ta e="T105" id="Seg_1001" s="T104">qobo-un-d</ta>
            <ta e="T106" id="Seg_1002" s="T105">čondɨš</ta>
            <ta e="T107" id="Seg_1003" s="T106">meː-mba-d</ta>
            <ta e="T108" id="Seg_1004" s="T107">na</ta>
            <ta e="T109" id="Seg_1005" s="T108">nädeq</ta>
            <ta e="T110" id="Seg_1006" s="T109">qorqɨ-p</ta>
            <ta e="T111" id="Seg_1007" s="T110">čaǯe-bɨlʼ</ta>
            <ta e="T112" id="Seg_1008" s="T111">sečas</ta>
            <ta e="T113" id="Seg_1009" s="T112">näj</ta>
            <ta e="T114" id="Seg_1010" s="T113">tar</ta>
            <ta e="T115" id="Seg_1011" s="T114">eːlʼa</ta>
            <ta e="T116" id="Seg_1012" s="T115">qwär-a-t</ta>
            <ta e="T117" id="Seg_1013" s="T116">tab-ǝ-t</ta>
            <ta e="T118" id="Seg_1014" s="T117">Lʼuker-ka-tko</ta>
            <ta e="T119" id="Seg_1015" s="T118">tab-ɨ-nan</ta>
            <ta e="T120" id="Seg_1016" s="T119">čuʒi-tɨlʼ</ta>
            <ta e="T121" id="Seg_1017" s="T120">pudɨlʼ-ǯe</ta>
            <ta e="T122" id="Seg_1018" s="T121">warqə</ta>
            <ta e="T123" id="Seg_1019" s="T122">nenʼä-d</ta>
            <ta e="T124" id="Seg_1020" s="T123">tabə</ta>
            <ta e="T125" id="Seg_1021" s="T124">üt-qo-mba-det</ta>
            <ta e="T126" id="Seg_1022" s="T125">šidaːro</ta>
            <ta e="T127" id="Seg_1023" s="T126">wersta-nd</ta>
            <ta e="T128" id="Seg_1024" s="T127">kaja-mba</ta>
            <ta e="T129" id="Seg_1025" s="T128">tabe-qu-t</ta>
            <ta e="T130" id="Seg_1026" s="T129">qwär-gu</ta>
            <ta e="T131" id="Seg_1027" s="T130">tabə</ta>
            <ta e="T132" id="Seg_1028" s="T131">qwär-ɨ-mba-t</ta>
            <ta e="T133" id="Seg_1029" s="T132">toʒe</ta>
            <ta e="T134" id="Seg_1030" s="T133">sečas</ta>
            <ta e="T135" id="Seg_1031" s="T134">näj</ta>
            <ta e="T136" id="Seg_1032" s="T135">tar</ta>
            <ta e="T137" id="Seg_1033" s="T136">eːlʼa</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1034" s="T0">nadek</ta>
            <ta e="T2" id="Seg_1035" s="T1">qorqɨ-p</ta>
            <ta e="T3" id="Seg_1036" s="T2">čačɨ-mbɨ-t</ta>
            <ta e="T4" id="Seg_1037" s="T3">qande-qɨn</ta>
            <ta e="T5" id="Seg_1038" s="T4">neː-lʼ-qum-t</ta>
            <ta e="T6" id="Seg_1039" s="T5">maːt-nde</ta>
            <ta e="T7" id="Seg_1040" s="T6">qalɨ-mbɨ-dət</ta>
            <ta e="T8" id="Seg_1041" s="T7">ara-t</ta>
            <ta e="T9" id="Seg_1042" s="T8">maǯʼo-nde</ta>
            <ta e="T10" id="Seg_1043" s="T9">qwän-mbɨ-dət</ta>
            <ta e="T11" id="Seg_1044" s="T10">aː</ta>
            <ta e="T12" id="Seg_1045" s="T11">qor</ta>
            <ta e="T13" id="Seg_1046" s="T12">xər-ɨ-qɨt</ta>
            <ta e="T14" id="Seg_1047" s="T13">qorqɨ</ta>
            <ta e="T15" id="Seg_1048" s="T14">töː-mbɨ</ta>
            <ta e="T16" id="Seg_1049" s="T15">maːt</ta>
            <ta e="T17" id="Seg_1050" s="T16">neː-lʼ-qum-t</ta>
            <ta e="T18" id="Seg_1051" s="T17">šoqor-nde</ta>
            <ta e="T19" id="Seg_1052" s="T18">nʼaj</ta>
            <ta e="T20" id="Seg_1053" s="T19">omde-lʼčǝ-mbɨ-dət</ta>
            <ta e="T21" id="Seg_1054" s="T20">na</ta>
            <ta e="T22" id="Seg_1055" s="T21">šoqor</ta>
            <ta e="T23" id="Seg_1056" s="T22">poːne-qɨn-lʼ</ta>
            <ta e="T24" id="Seg_1057" s="T23">šoqor</ta>
            <ta e="T25" id="Seg_1058" s="T24">qorqɨ</ta>
            <ta e="T26" id="Seg_1059" s="T25">na</ta>
            <ta e="T27" id="Seg_1060" s="T26">šoqor-t</ta>
            <ta e="T28" id="Seg_1061" s="T27">panal-mbɨ-t</ta>
            <ta e="T29" id="Seg_1062" s="T28">nʼaj-p</ta>
            <ta e="T30" id="Seg_1063" s="T29">wesʼ</ta>
            <ta e="T31" id="Seg_1064" s="T30">am-mbɨ-t</ta>
            <ta e="T32" id="Seg_1065" s="T31">nʼalʼalʼ</ta>
            <ta e="T33" id="Seg_1066" s="T32">nʼaj</ta>
            <ta e="T34" id="Seg_1067" s="T33">okkər</ta>
            <ta e="T35" id="Seg_1068" s="T34">kanak</ta>
            <ta e="T36" id="Seg_1069" s="T35">artpɨ-dɨlʼ</ta>
            <ta e="T37" id="Seg_1070" s="T36">e-mbɨ</ta>
            <ta e="T38" id="Seg_1071" s="T37">na-m</ta>
            <ta e="T39" id="Seg_1072" s="T38">naj</ta>
            <ta e="T40" id="Seg_1073" s="T39">qorqɨ</ta>
            <ta e="T41" id="Seg_1074" s="T40">am-mbɨ-t</ta>
            <ta e="T42" id="Seg_1075" s="T41">kanak-nando</ta>
            <ta e="T43" id="Seg_1076" s="T42">okkər</ta>
            <ta e="T44" id="Seg_1077" s="T43">tälʼǯʼi-da</ta>
            <ta e="T45" id="Seg_1078" s="T44">qalɨ-mbɨ</ta>
            <ta e="T46" id="Seg_1079" s="T45">košüne-nde</ta>
            <ta e="T47" id="Seg_1080" s="T46">šeːr-mbɨ</ta>
            <ta e="T48" id="Seg_1081" s="T47">muga-lʼ</ta>
            <ta e="T49" id="Seg_1082" s="T48">qoča-p</ta>
            <ta e="T50" id="Seg_1083" s="T49">tak</ta>
            <ta e="T51" id="Seg_1084" s="T50">wesʼ</ta>
            <ta e="T52" id="Seg_1085" s="T51">niškɨlʼ-mbɨ-t</ta>
            <ta e="T53" id="Seg_1086" s="T52">wesʼ</ta>
            <ta e="T54" id="Seg_1087" s="T53">muga</ta>
            <ta e="T55" id="Seg_1088" s="T54">lʼaqkalʼǯʼi-mbɨ-t</ta>
            <ta e="T56" id="Seg_1089" s="T55">onǯe</ta>
            <ta e="T57" id="Seg_1090" s="T56">muga</ta>
            <ta e="T58" id="Seg_1091" s="T57">čeq</ta>
            <ta e="T59" id="Seg_1092" s="T58">eǯe-mbɨ</ta>
            <ta e="T60" id="Seg_1093" s="T59">neː-lʼ-qum-la-t</ta>
            <ta e="T61" id="Seg_1094" s="T60">potpol-nǯ</ta>
            <ta e="T62" id="Seg_1095" s="T61">attɛ-lʼčǝ-mbɨ-dət</ta>
            <ta e="T63" id="Seg_1096" s="T62">okkər</ta>
            <ta e="T64" id="Seg_1097" s="T63">nadek</ta>
            <ta e="T65" id="Seg_1098" s="T64">olo-m</ta>
            <ta e="T66" id="Seg_1099" s="T65">me-špɨ-lɨ-mbɨ-t</ta>
            <ta e="T67" id="Seg_1100" s="T66">olo-m</ta>
            <ta e="T68" id="Seg_1101" s="T67">me-mbɨ-t</ta>
            <ta e="T69" id="Seg_1102" s="T68">tüːlʼde-m</ta>
            <ta e="T70" id="Seg_1103" s="T69">am-ɨ-dɨ-mbɨ-t</ta>
            <ta e="T71" id="Seg_1104" s="T70">qorqɨ</ta>
            <ta e="T72" id="Seg_1105" s="T71">akoška-nde</ta>
            <ta e="T73" id="Seg_1106" s="T72">nʼulʼeǯʼe-mbɨ</ta>
            <ta e="T74" id="Seg_1107" s="T73">maːt-ɨ-m</ta>
            <ta e="T75" id="Seg_1108" s="T74">mantɨ-mbɨ-gu</ta>
            <ta e="T76" id="Seg_1109" s="T75">taw</ta>
            <ta e="T77" id="Seg_1110" s="T76">na</ta>
            <ta e="T78" id="Seg_1111" s="T77">nadek</ta>
            <ta e="T138" id="Seg_1112" s="T78">kɨlnolʼ</ta>
            <ta e="T79" id="Seg_1113" s="T138">ǯʼak</ta>
            <ta e="T80" id="Seg_1114" s="T79">čačɨ-mbɨ-k</ta>
            <ta e="T81" id="Seg_1115" s="T80">qorqɨ</ta>
            <ta e="T82" id="Seg_1116" s="T81">illä</ta>
            <ta e="T83" id="Seg_1117" s="T82">alʼči-mbɨ</ta>
            <ta e="T84" id="Seg_1118" s="T83">patom</ta>
            <ta e="T85" id="Seg_1119" s="T84">qorqɨ-p</ta>
            <ta e="T86" id="Seg_1120" s="T85">taqkɨr-gu</ta>
            <ta e="T87" id="Seg_1121" s="T86">okkər</ta>
            <ta e="T88" id="Seg_1122" s="T87">neː-lʼ-qum-t</ta>
            <ta e="T89" id="Seg_1123" s="T88">üdɨ-mbɨ-dət</ta>
            <ta e="T90" id="Seg_1124" s="T89">šidaːro</ta>
            <ta e="T91" id="Seg_1125" s="T90">wersta-nde</ta>
            <ta e="T92" id="Seg_1126" s="T91">tebe-lʼ</ta>
            <ta e="T93" id="Seg_1127" s="T92">qum</ta>
            <ta e="T94" id="Seg_1128" s="T93">qwär-gu</ta>
            <ta e="T95" id="Seg_1129" s="T94">tebe-lʼ-qum-t</ta>
            <ta e="T96" id="Seg_1130" s="T95">nača-ɣɨndo</ta>
            <ta e="T97" id="Seg_1131" s="T96">töː-mbɨ-dət</ta>
            <ta e="T98" id="Seg_1132" s="T97">qorqɨ-p</ta>
            <ta e="T99" id="Seg_1133" s="T98">tak</ta>
            <ta e="T100" id="Seg_1134" s="T99">kɨrɨ-mbɨ-dət</ta>
            <ta e="T101" id="Seg_1135" s="T100">mužurə-mbɨ-t</ta>
            <ta e="T102" id="Seg_1136" s="T101">i</ta>
            <ta e="T103" id="Seg_1137" s="T102">am-mbɨ-dət</ta>
            <ta e="T104" id="Seg_1138" s="T103">a</ta>
            <ta e="T105" id="Seg_1139" s="T104">kobɨ-un-t</ta>
            <ta e="T106" id="Seg_1140" s="T105">čondɨš</ta>
            <ta e="T107" id="Seg_1141" s="T106">me-mbɨ-dət</ta>
            <ta e="T108" id="Seg_1142" s="T107">na</ta>
            <ta e="T109" id="Seg_1143" s="T108">nadek</ta>
            <ta e="T110" id="Seg_1144" s="T109">qorqɨ-p</ta>
            <ta e="T111" id="Seg_1145" s="T110">čačɨ-mbɨlʼe</ta>
            <ta e="T112" id="Seg_1146" s="T111">sečas</ta>
            <ta e="T113" id="Seg_1147" s="T112">naj</ta>
            <ta e="T114" id="Seg_1148" s="T113">tar</ta>
            <ta e="T115" id="Seg_1149" s="T114">ele</ta>
            <ta e="T116" id="Seg_1150" s="T115">qwär-ɨ-dət</ta>
            <ta e="T117" id="Seg_1151" s="T116">tab-ɨ-t</ta>
            <ta e="T118" id="Seg_1152" s="T117">Lʼuker-ka-tqo</ta>
            <ta e="T119" id="Seg_1153" s="T118">tab-ɨ-nan</ta>
            <ta e="T120" id="Seg_1154" s="T119">čuʒi-dɨlʼ</ta>
            <ta e="T121" id="Seg_1155" s="T120">pudol-se</ta>
            <ta e="T122" id="Seg_1156" s="T121">wargɨ</ta>
            <ta e="T123" id="Seg_1157" s="T122">nʼenʼnʼa-t</ta>
            <ta e="T124" id="Seg_1158" s="T123">tab</ta>
            <ta e="T125" id="Seg_1159" s="T124">üdɨ-ku-mbɨ-dət</ta>
            <ta e="T126" id="Seg_1160" s="T125">šidaːro</ta>
            <ta e="T127" id="Seg_1161" s="T126">wersta-nde</ta>
            <ta e="T128" id="Seg_1162" s="T127">koja-mbɨ</ta>
            <ta e="T129" id="Seg_1163" s="T128">tebe-qum-t</ta>
            <ta e="T130" id="Seg_1164" s="T129">qwär-gu</ta>
            <ta e="T131" id="Seg_1165" s="T130">tab</ta>
            <ta e="T132" id="Seg_1166" s="T131">qwär-ɨ-mbɨ-t</ta>
            <ta e="T133" id="Seg_1167" s="T132">toʒe</ta>
            <ta e="T134" id="Seg_1168" s="T133">sečas</ta>
            <ta e="T135" id="Seg_1169" s="T134">naj</ta>
            <ta e="T136" id="Seg_1170" s="T135">tar</ta>
            <ta e="T137" id="Seg_1171" s="T136">ele</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1172" s="T0">girl.[NOM]</ta>
            <ta e="T2" id="Seg_1173" s="T1">bear-ACC</ta>
            <ta e="T3" id="Seg_1174" s="T2">shoot-PST.NAR-3SG.O</ta>
            <ta e="T4" id="Seg_1175" s="T3">autumn-LOC</ta>
            <ta e="T5" id="Seg_1176" s="T4">woman-ADJZ-human.being-PL.[NOM]</ta>
            <ta e="T6" id="Seg_1177" s="T5">house-ILL</ta>
            <ta e="T7" id="Seg_1178" s="T6">stay-PST.NAR-3PL</ta>
            <ta e="T8" id="Seg_1179" s="T7">old.man-PL</ta>
            <ta e="T9" id="Seg_1180" s="T8">taiga-ILL</ta>
            <ta e="T10" id="Seg_1181" s="T9">go.away-PST.NAR-3PL</ta>
            <ta e="T11" id="Seg_1182" s="T10">NEG</ta>
            <ta e="T12" id="Seg_1183" s="T11">%%</ta>
            <ta e="T13" id="Seg_1184" s="T12">snow-EP-LOC</ta>
            <ta e="T14" id="Seg_1185" s="T13">bear.[NOM]</ta>
            <ta e="T15" id="Seg_1186" s="T14">come-PST.NAR.[3SG.S]</ta>
            <ta e="T16" id="Seg_1187" s="T15">house.[NOM]</ta>
            <ta e="T17" id="Seg_1188" s="T16">woman-ADJZ-human.being-PL.[NOM]</ta>
            <ta e="T18" id="Seg_1189" s="T17">stove-ILL</ta>
            <ta e="T19" id="Seg_1190" s="T18">bread.[NOM]</ta>
            <ta e="T20" id="Seg_1191" s="T19">sit.down-PFV-PST.NAR-3PL</ta>
            <ta e="T21" id="Seg_1192" s="T20">this</ta>
            <ta e="T22" id="Seg_1193" s="T21">stove.[NOM]</ta>
            <ta e="T23" id="Seg_1194" s="T22">outward(s)-LOC-ADJZ</ta>
            <ta e="T24" id="Seg_1195" s="T23">stove.[NOM]</ta>
            <ta e="T25" id="Seg_1196" s="T24">bear.[NOM]</ta>
            <ta e="T26" id="Seg_1197" s="T25">this</ta>
            <ta e="T27" id="Seg_1198" s="T26">stove.[NOM]-3SG</ta>
            <ta e="T28" id="Seg_1199" s="T27">break-PST.NAR-3SG.O</ta>
            <ta e="T29" id="Seg_1200" s="T28">bread-ACC</ta>
            <ta e="T30" id="Seg_1201" s="T29">all</ta>
            <ta e="T31" id="Seg_1202" s="T30">eat-PST.NAR-3SG.O</ta>
            <ta e="T32" id="Seg_1203" s="T31">raw</ta>
            <ta e="T33" id="Seg_1204" s="T32">bread.[NOM]</ta>
            <ta e="T34" id="Seg_1205" s="T33">one</ta>
            <ta e="T35" id="Seg_1206" s="T34">dog.[NOM]</ta>
            <ta e="T36" id="Seg_1207" s="T35">be.tied-PTCP.PRS</ta>
            <ta e="T37" id="Seg_1208" s="T36">be-DUR.[3SG.S]</ta>
            <ta e="T38" id="Seg_1209" s="T37">this-ACC</ta>
            <ta e="T39" id="Seg_1210" s="T38">also</ta>
            <ta e="T40" id="Seg_1211" s="T39">bear.[3SG.S]</ta>
            <ta e="T41" id="Seg_1212" s="T40">eat-PST.NAR-3SG.O</ta>
            <ta e="T42" id="Seg_1213" s="T41">dog-ABL</ta>
            <ta e="T43" id="Seg_1214" s="T42">one</ta>
            <ta e="T44" id="Seg_1215" s="T43">tail-INDEF</ta>
            <ta e="T45" id="Seg_1216" s="T44">stay-PST.NAR.[3SG.S]</ta>
            <ta e="T46" id="Seg_1217" s="T45">porch-ILL</ta>
            <ta e="T47" id="Seg_1218" s="T46">enter-PST.NAR.[3SG.S]</ta>
            <ta e="T48" id="Seg_1219" s="T47">flour-ADJZ</ta>
            <ta e="T49" id="Seg_1220" s="T48">bag-ACC</ta>
            <ta e="T50" id="Seg_1221" s="T49">away</ta>
            <ta e="T51" id="Seg_1222" s="T50">all</ta>
            <ta e="T52" id="Seg_1223" s="T51">%%-PST.NAR-3SG.O</ta>
            <ta e="T53" id="Seg_1224" s="T52">all</ta>
            <ta e="T54" id="Seg_1225" s="T53">flour.[NOM]</ta>
            <ta e="T55" id="Seg_1226" s="T54">shake.out-PST.NAR-3SG.O</ta>
            <ta e="T56" id="Seg_1227" s="T55">oneself.3SG.[NOM]</ta>
            <ta e="T57" id="Seg_1228" s="T56">flour.[NOM]</ta>
            <ta e="T58" id="Seg_1229" s="T57">white</ta>
            <ta e="T59" id="Seg_1230" s="T58">become-PST.NAR.[3SG.S]</ta>
            <ta e="T60" id="Seg_1231" s="T59">daughter-ADJZ-human.being-PL.[NOM]-3SG</ta>
            <ta e="T61" id="Seg_1232" s="T60">underground-ILL2</ta>
            <ta e="T62" id="Seg_1233" s="T61">hide-PFV-PST.NAR-3PL</ta>
            <ta e="T63" id="Seg_1234" s="T62">one</ta>
            <ta e="T64" id="Seg_1235" s="T63">girl.[NOM]</ta>
            <ta e="T65" id="Seg_1236" s="T64">bullet-ACC</ta>
            <ta e="T66" id="Seg_1237" s="T65">do-IPFV2-RES-PST.NAR-3SG.O</ta>
            <ta e="T67" id="Seg_1238" s="T66">bullet-ACC</ta>
            <ta e="T68" id="Seg_1239" s="T67">do-PST.NAR-3SG.O</ta>
            <ta e="T69" id="Seg_1240" s="T68">rifle-ACC</ta>
            <ta e="T70" id="Seg_1241" s="T69">eat-EP-TR-PST.NAR-3SG.O</ta>
            <ta e="T71" id="Seg_1242" s="T70">bear.[NOM]</ta>
            <ta e="T72" id="Seg_1243" s="T71">window-ILL</ta>
            <ta e="T73" id="Seg_1244" s="T72">get.up.fast-PST.NAR.[3SG.S]</ta>
            <ta e="T74" id="Seg_1245" s="T73">house-EP-ACC</ta>
            <ta e="T75" id="Seg_1246" s="T74">look-DUR-INF</ta>
            <ta e="T76" id="Seg_1247" s="T75">this</ta>
            <ta e="T77" id="Seg_1248" s="T76">this</ta>
            <ta e="T78" id="Seg_1249" s="T77">girl.[NOM]</ta>
            <ta e="T138" id="Seg_1250" s="T78">chest</ta>
            <ta e="T79" id="Seg_1251" s="T138">%%</ta>
            <ta e="T80" id="Seg_1252" s="T79">shoot-DUR-3SG.S</ta>
            <ta e="T81" id="Seg_1253" s="T80">bear.[NOM]</ta>
            <ta e="T82" id="Seg_1254" s="T81">down</ta>
            <ta e="T83" id="Seg_1255" s="T82">fall-PST.NAR.[3SG.S]</ta>
            <ta e="T84" id="Seg_1256" s="T83">then</ta>
            <ta e="T85" id="Seg_1257" s="T84">bear-ACC</ta>
            <ta e="T86" id="Seg_1258" s="T85">%%-INF</ta>
            <ta e="T87" id="Seg_1259" s="T86">one</ta>
            <ta e="T88" id="Seg_1260" s="T87">woman-ADJZ-human.being-3SG</ta>
            <ta e="T89" id="Seg_1261" s="T88">send-PST.NAR-3PL</ta>
            <ta e="T90" id="Seg_1262" s="T89">twenty</ta>
            <ta e="T91" id="Seg_1263" s="T90">verst-ILL</ta>
            <ta e="T92" id="Seg_1264" s="T91">man-ADJZ</ta>
            <ta e="T93" id="Seg_1265" s="T92">human.being.[NOM]</ta>
            <ta e="T94" id="Seg_1266" s="T93">call-INF</ta>
            <ta e="T95" id="Seg_1267" s="T94">man-ADJZ-human.being-PL</ta>
            <ta e="T96" id="Seg_1268" s="T95">there-EL.3SG</ta>
            <ta e="T97" id="Seg_1269" s="T96">come-PST.NAR-3PL</ta>
            <ta e="T98" id="Seg_1270" s="T97">bear-ACC</ta>
            <ta e="T99" id="Seg_1271" s="T98">away</ta>
            <ta e="T100" id="Seg_1272" s="T99">pull.off-PST.NAR-3PL</ta>
            <ta e="T101" id="Seg_1273" s="T100">cook-PST.NAR-3SG.O</ta>
            <ta e="T102" id="Seg_1274" s="T101">and</ta>
            <ta e="T103" id="Seg_1275" s="T102">eat-PST.NAR-3PL</ta>
            <ta e="T104" id="Seg_1276" s="T103">but</ta>
            <ta e="T105" id="Seg_1277" s="T104">skin-PROL-3SG</ta>
            <ta e="T106" id="Seg_1278" s="T105">sleeping.bag.[NOM]</ta>
            <ta e="T107" id="Seg_1279" s="T106">do-PST.NAR-3PL</ta>
            <ta e="T108" id="Seg_1280" s="T107">this</ta>
            <ta e="T109" id="Seg_1281" s="T108">girl.[NOM]</ta>
            <ta e="T110" id="Seg_1282" s="T109">bear-ACC</ta>
            <ta e="T111" id="Seg_1283" s="T110">shoot-PTCP.PST</ta>
            <ta e="T112" id="Seg_1284" s="T111">now</ta>
            <ta e="T113" id="Seg_1285" s="T112">also</ta>
            <ta e="T114" id="Seg_1286" s="T113">still</ta>
            <ta e="T115" id="Seg_1287" s="T114">live.[3SG.S]</ta>
            <ta e="T116" id="Seg_1288" s="T115">call-EP-3PL</ta>
            <ta e="T117" id="Seg_1289" s="T116">(s)he-EP-PL</ta>
            <ta e="T118" id="Seg_1290" s="T117">Lukerya-DIM-TRL</ta>
            <ta e="T119" id="Seg_1291" s="T118">(s)he-EP-ADES</ta>
            <ta e="T120" id="Seg_1292" s="T119">be.unconscious-PTCP.PRS</ta>
            <ta e="T121" id="Seg_1293" s="T120">cheek-COM</ta>
            <ta e="T122" id="Seg_1294" s="T121">big</ta>
            <ta e="T123" id="Seg_1295" s="T122">sister.[NOM]-3SG</ta>
            <ta e="T124" id="Seg_1296" s="T123">(s)he.[NOM]</ta>
            <ta e="T125" id="Seg_1297" s="T124">send-HAB-PST.NAR-3PL</ta>
            <ta e="T126" id="Seg_1298" s="T125">twenty</ta>
            <ta e="T127" id="Seg_1299" s="T126">verst-ILL</ta>
            <ta e="T128" id="Seg_1300" s="T127">go-PST.NAR.[3SG.S]</ta>
            <ta e="T129" id="Seg_1301" s="T128">man-human.being-PL</ta>
            <ta e="T130" id="Seg_1302" s="T129">call-INF</ta>
            <ta e="T131" id="Seg_1303" s="T130">(s)he.[NOM]</ta>
            <ta e="T132" id="Seg_1304" s="T131">call-EP-PST.NAR-3SG.O</ta>
            <ta e="T133" id="Seg_1305" s="T132">also</ta>
            <ta e="T134" id="Seg_1306" s="T133">now</ta>
            <ta e="T135" id="Seg_1307" s="T134">also</ta>
            <ta e="T136" id="Seg_1308" s="T135">still</ta>
            <ta e="T137" id="Seg_1309" s="T136">live.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1310" s="T0">девушка.[NOM]</ta>
            <ta e="T2" id="Seg_1311" s="T1">медведь-ACC</ta>
            <ta e="T3" id="Seg_1312" s="T2">стрелять-PST.NAR-3SG.O</ta>
            <ta e="T4" id="Seg_1313" s="T3">осень-LOC</ta>
            <ta e="T5" id="Seg_1314" s="T4">женщина-ADJZ-человек-PL.[NOM]</ta>
            <ta e="T6" id="Seg_1315" s="T5">дом-ILL</ta>
            <ta e="T7" id="Seg_1316" s="T6">остаться-PST.NAR-3PL</ta>
            <ta e="T8" id="Seg_1317" s="T7">старик-PL</ta>
            <ta e="T9" id="Seg_1318" s="T8">тайга-ILL</ta>
            <ta e="T10" id="Seg_1319" s="T9">пойти-PST.NAR-3PL</ta>
            <ta e="T11" id="Seg_1320" s="T10">NEG</ta>
            <ta e="T12" id="Seg_1321" s="T11">%%</ta>
            <ta e="T13" id="Seg_1322" s="T12">снег-EP-LOC</ta>
            <ta e="T14" id="Seg_1323" s="T13">медведь.[NOM]</ta>
            <ta e="T15" id="Seg_1324" s="T14">прийти-PST.NAR.[3SG.S]</ta>
            <ta e="T16" id="Seg_1325" s="T15">дом.[NOM]</ta>
            <ta e="T17" id="Seg_1326" s="T16">женщина-ADJZ-человек-PL.[NOM]</ta>
            <ta e="T18" id="Seg_1327" s="T17">печка-ILL</ta>
            <ta e="T19" id="Seg_1328" s="T18">хлеб.[NOM]</ta>
            <ta e="T20" id="Seg_1329" s="T19">сесть-PFV-PST.NAR-3PL</ta>
            <ta e="T21" id="Seg_1330" s="T20">этот</ta>
            <ta e="T22" id="Seg_1331" s="T21">печка.[NOM]</ta>
            <ta e="T23" id="Seg_1332" s="T22">наружу-LOC-ADJZ</ta>
            <ta e="T24" id="Seg_1333" s="T23">печка.[NOM]</ta>
            <ta e="T25" id="Seg_1334" s="T24">медведь.[NOM]</ta>
            <ta e="T26" id="Seg_1335" s="T25">этот</ta>
            <ta e="T27" id="Seg_1336" s="T26">печка.[NOM]-3SG</ta>
            <ta e="T28" id="Seg_1337" s="T27">сломать-PST.NAR-3SG.O</ta>
            <ta e="T29" id="Seg_1338" s="T28">хлеб-ACC</ta>
            <ta e="T30" id="Seg_1339" s="T29">весь</ta>
            <ta e="T31" id="Seg_1340" s="T30">есть-PST.NAR-3SG.O</ta>
            <ta e="T32" id="Seg_1341" s="T31">сырой</ta>
            <ta e="T33" id="Seg_1342" s="T32">хлеб.[NOM]</ta>
            <ta e="T34" id="Seg_1343" s="T33">один</ta>
            <ta e="T35" id="Seg_1344" s="T34">собака.[NOM]</ta>
            <ta e="T36" id="Seg_1345" s="T35">быть.привязанным-PTCP.PRS</ta>
            <ta e="T37" id="Seg_1346" s="T36">быть-DUR.[3SG.S]</ta>
            <ta e="T38" id="Seg_1347" s="T37">этот-ACC</ta>
            <ta e="T39" id="Seg_1348" s="T38">тоже</ta>
            <ta e="T40" id="Seg_1349" s="T39">медведь.[3SG.S]</ta>
            <ta e="T41" id="Seg_1350" s="T40">есть-PST.NAR-3SG.O</ta>
            <ta e="T42" id="Seg_1351" s="T41">собака-ABL</ta>
            <ta e="T43" id="Seg_1352" s="T42">один</ta>
            <ta e="T44" id="Seg_1353" s="T43">хвост-INDEF</ta>
            <ta e="T45" id="Seg_1354" s="T44">остаться-PST.NAR.[3SG.S]</ta>
            <ta e="T46" id="Seg_1355" s="T45">сени-ILL</ta>
            <ta e="T47" id="Seg_1356" s="T46">зайти-PST.NAR.[3SG.S]</ta>
            <ta e="T48" id="Seg_1357" s="T47">мука-ADJZ</ta>
            <ta e="T49" id="Seg_1358" s="T48">мешок-ACC</ta>
            <ta e="T50" id="Seg_1359" s="T49">прочь</ta>
            <ta e="T51" id="Seg_1360" s="T50">весь</ta>
            <ta e="T52" id="Seg_1361" s="T51">%%-PST.NAR-3SG.O</ta>
            <ta e="T53" id="Seg_1362" s="T52">весь</ta>
            <ta e="T54" id="Seg_1363" s="T53">мука.[NOM]</ta>
            <ta e="T55" id="Seg_1364" s="T54">вытряхнуть-PST.NAR-3SG.O</ta>
            <ta e="T56" id="Seg_1365" s="T55">сам.3SG.[NOM]</ta>
            <ta e="T57" id="Seg_1366" s="T56">мука.[NOM]</ta>
            <ta e="T58" id="Seg_1367" s="T57">белый</ta>
            <ta e="T59" id="Seg_1368" s="T58">стать-PST.NAR.[3SG.S]</ta>
            <ta e="T60" id="Seg_1369" s="T59">дочь-ADJZ-человек-PL.[NOM]-3SG</ta>
            <ta e="T61" id="Seg_1370" s="T60">подпол-ILL2</ta>
            <ta e="T62" id="Seg_1371" s="T61">спрятаться-PFV-PST.NAR-3PL</ta>
            <ta e="T63" id="Seg_1372" s="T62">один</ta>
            <ta e="T64" id="Seg_1373" s="T63">девушка.[NOM]</ta>
            <ta e="T65" id="Seg_1374" s="T64">пуля-ACC</ta>
            <ta e="T66" id="Seg_1375" s="T65">делать-IPFV2-RES-PST.NAR-3SG.O</ta>
            <ta e="T67" id="Seg_1376" s="T66">пуля-ACC</ta>
            <ta e="T68" id="Seg_1377" s="T67">делать-PST.NAR-3SG.O</ta>
            <ta e="T69" id="Seg_1378" s="T68">ружье-ACC</ta>
            <ta e="T70" id="Seg_1379" s="T69">есть-EP-TR-PST.NAR-3SG.O</ta>
            <ta e="T71" id="Seg_1380" s="T70">медведь.[NOM]</ta>
            <ta e="T72" id="Seg_1381" s="T71">окно-ILL</ta>
            <ta e="T73" id="Seg_1382" s="T72">встать.быстро-PST.NAR.[3SG.S]</ta>
            <ta e="T74" id="Seg_1383" s="T73">дом-EP-ACC</ta>
            <ta e="T75" id="Seg_1384" s="T74">смотреть-DUR-INF</ta>
            <ta e="T76" id="Seg_1385" s="T75">этот</ta>
            <ta e="T77" id="Seg_1386" s="T76">этот</ta>
            <ta e="T78" id="Seg_1387" s="T77">девушка.[NOM]</ta>
            <ta e="T138" id="Seg_1388" s="T78">грудь</ta>
            <ta e="T79" id="Seg_1389" s="T138">%%</ta>
            <ta e="T80" id="Seg_1390" s="T79">стрелять-DUR-3SG.S</ta>
            <ta e="T81" id="Seg_1391" s="T80">медведь.[NOM]</ta>
            <ta e="T82" id="Seg_1392" s="T81">вниз</ta>
            <ta e="T83" id="Seg_1393" s="T82">упасть-PST.NAR.[3SG.S]</ta>
            <ta e="T84" id="Seg_1394" s="T83">потом</ta>
            <ta e="T85" id="Seg_1395" s="T84">медведь-ACC</ta>
            <ta e="T86" id="Seg_1396" s="T85">%%-INF</ta>
            <ta e="T87" id="Seg_1397" s="T86">один</ta>
            <ta e="T88" id="Seg_1398" s="T87">женщина-ADJZ-человек-3SG</ta>
            <ta e="T89" id="Seg_1399" s="T88">посылать-PST.NAR-3PL</ta>
            <ta e="T90" id="Seg_1400" s="T89">двадцать</ta>
            <ta e="T91" id="Seg_1401" s="T90">верста-ILL</ta>
            <ta e="T92" id="Seg_1402" s="T91">мужчина-ADJZ</ta>
            <ta e="T93" id="Seg_1403" s="T92">человек.[NOM]</ta>
            <ta e="T94" id="Seg_1404" s="T93">позвать-INF</ta>
            <ta e="T95" id="Seg_1405" s="T94">мужчина-ADJZ-человек-PL</ta>
            <ta e="T96" id="Seg_1406" s="T95">там-EL.3SG</ta>
            <ta e="T97" id="Seg_1407" s="T96">прийти-PST.NAR-3PL</ta>
            <ta e="T98" id="Seg_1408" s="T97">медведь-ACC</ta>
            <ta e="T99" id="Seg_1409" s="T98">прочь</ta>
            <ta e="T100" id="Seg_1410" s="T99">ободрать-PST.NAR-3PL</ta>
            <ta e="T101" id="Seg_1411" s="T100">сварить-PST.NAR-3SG.O</ta>
            <ta e="T102" id="Seg_1412" s="T101">и</ta>
            <ta e="T103" id="Seg_1413" s="T102">есть-PST.NAR-3PL</ta>
            <ta e="T104" id="Seg_1414" s="T103">а</ta>
            <ta e="T105" id="Seg_1415" s="T104">шкура-PROL-3SG</ta>
            <ta e="T106" id="Seg_1416" s="T105">спальный.мешок.[NOM]</ta>
            <ta e="T107" id="Seg_1417" s="T106">делать-PST.NAR-3PL</ta>
            <ta e="T108" id="Seg_1418" s="T107">этот</ta>
            <ta e="T109" id="Seg_1419" s="T108">девушка.[NOM]</ta>
            <ta e="T110" id="Seg_1420" s="T109">медведь-ACC</ta>
            <ta e="T111" id="Seg_1421" s="T110">стрелять-PTCP.PST</ta>
            <ta e="T112" id="Seg_1422" s="T111">сейчас</ta>
            <ta e="T113" id="Seg_1423" s="T112">тоже</ta>
            <ta e="T114" id="Seg_1424" s="T113">ещё</ta>
            <ta e="T115" id="Seg_1425" s="T114">жить.[3SG.S]</ta>
            <ta e="T116" id="Seg_1426" s="T115">позвать-EP-3PL</ta>
            <ta e="T117" id="Seg_1427" s="T116">он(а)-EP-PL</ta>
            <ta e="T118" id="Seg_1428" s="T117">Лукерья-DIM-TRL</ta>
            <ta e="T119" id="Seg_1429" s="T118">он(а)-EP-ADES</ta>
            <ta e="T120" id="Seg_1430" s="T119">быть.без.сознания-PTCP.PRS</ta>
            <ta e="T121" id="Seg_1431" s="T120">щека-COM</ta>
            <ta e="T122" id="Seg_1432" s="T121">большой</ta>
            <ta e="T123" id="Seg_1433" s="T122">сестра.[NOM]-3SG</ta>
            <ta e="T124" id="Seg_1434" s="T123">он(а).[NOM]</ta>
            <ta e="T125" id="Seg_1435" s="T124">посылать-HAB-PST.NAR-3PL</ta>
            <ta e="T126" id="Seg_1436" s="T125">двадцать</ta>
            <ta e="T127" id="Seg_1437" s="T126">верста-ILL</ta>
            <ta e="T128" id="Seg_1438" s="T127">ходить-PST.NAR.[3SG.S]</ta>
            <ta e="T129" id="Seg_1439" s="T128">мужчина-человек-PL</ta>
            <ta e="T130" id="Seg_1440" s="T129">позвать-INF</ta>
            <ta e="T131" id="Seg_1441" s="T130">он(а).[NOM]</ta>
            <ta e="T132" id="Seg_1442" s="T131">позвать-EP-PST.NAR-3SG.O</ta>
            <ta e="T133" id="Seg_1443" s="T132">тоже</ta>
            <ta e="T134" id="Seg_1444" s="T133">сейчас</ta>
            <ta e="T135" id="Seg_1445" s="T134">тоже</ta>
            <ta e="T136" id="Seg_1446" s="T135">ещё</ta>
            <ta e="T137" id="Seg_1447" s="T136">жить.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1448" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_1449" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_1450" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_1451" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_1452" s="T4">n-n&gt;adj-n-n:num-n:case</ta>
            <ta e="T6" id="Seg_1453" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_1454" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_1455" s="T7">n-n:num</ta>
            <ta e="T9" id="Seg_1456" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_1457" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_1458" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_1459" s="T11">quant</ta>
            <ta e="T13" id="Seg_1460" s="T12">n-n:ins-n:case</ta>
            <ta e="T14" id="Seg_1461" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_1462" s="T14">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_1463" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_1464" s="T16">n-n&gt;adj-n-n:num-n:case</ta>
            <ta e="T18" id="Seg_1465" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_1466" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_1467" s="T19">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_1468" s="T20">dem</ta>
            <ta e="T22" id="Seg_1469" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_1470" s="T22">adv-n:case-n&gt;adj</ta>
            <ta e="T24" id="Seg_1471" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_1472" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_1473" s="T25">dem</ta>
            <ta e="T27" id="Seg_1474" s="T26">n-n:case-n:poss</ta>
            <ta e="T28" id="Seg_1475" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_1476" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_1477" s="T29">quant</ta>
            <ta e="T31" id="Seg_1478" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_1479" s="T31">adj</ta>
            <ta e="T33" id="Seg_1480" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_1481" s="T33">num</ta>
            <ta e="T35" id="Seg_1482" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_1483" s="T35">v-v&gt;ptcp</ta>
            <ta e="T37" id="Seg_1484" s="T36">v-v&gt;v-v:pn</ta>
            <ta e="T38" id="Seg_1485" s="T37">dem-n:case</ta>
            <ta e="T39" id="Seg_1486" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_1487" s="T39">n-v:pn</ta>
            <ta e="T41" id="Seg_1488" s="T40">v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_1489" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_1490" s="T42">num</ta>
            <ta e="T44" id="Seg_1491" s="T43">n-clit</ta>
            <ta e="T45" id="Seg_1492" s="T44">v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_1493" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_1494" s="T46">v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_1495" s="T47">n-n&gt;adj</ta>
            <ta e="T49" id="Seg_1496" s="T48">n-n:case</ta>
            <ta e="T50" id="Seg_1497" s="T49">preverb</ta>
            <ta e="T51" id="Seg_1498" s="T50">quant</ta>
            <ta e="T52" id="Seg_1499" s="T51">v-v:tense-v:pn</ta>
            <ta e="T53" id="Seg_1500" s="T52">quant</ta>
            <ta e="T54" id="Seg_1501" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_1502" s="T54">v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_1503" s="T55">emphpro-n:case</ta>
            <ta e="T57" id="Seg_1504" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_1505" s="T57">adj</ta>
            <ta e="T59" id="Seg_1506" s="T58">v-v:tense-v:pn</ta>
            <ta e="T60" id="Seg_1507" s="T59">n-n&gt;adj-n-n:num-v:pn-n:poss</ta>
            <ta e="T61" id="Seg_1508" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_1509" s="T61">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_1510" s="T62">num</ta>
            <ta e="T64" id="Seg_1511" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_1512" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_1513" s="T65">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_1514" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_1515" s="T67">v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_1516" s="T68">n-n:case</ta>
            <ta e="T70" id="Seg_1517" s="T69">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_1518" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_1519" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_1520" s="T72">v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_1521" s="T73">n-n:ins-n:case</ta>
            <ta e="T75" id="Seg_1522" s="T74">v-v&gt;v-v:inf</ta>
            <ta e="T76" id="Seg_1523" s="T75">dem</ta>
            <ta e="T77" id="Seg_1524" s="T76">dem</ta>
            <ta e="T78" id="Seg_1525" s="T77">n-n:case</ta>
            <ta e="T138" id="Seg_1526" s="T78">n</ta>
            <ta e="T79" id="Seg_1527" s="T138">%%</ta>
            <ta e="T80" id="Seg_1528" s="T79">v-v&gt;v-v:pn</ta>
            <ta e="T81" id="Seg_1529" s="T80">n-n:case</ta>
            <ta e="T82" id="Seg_1530" s="T81">preverb</ta>
            <ta e="T83" id="Seg_1531" s="T82">v-v:tense-v:pn</ta>
            <ta e="T84" id="Seg_1532" s="T83">adv</ta>
            <ta e="T85" id="Seg_1533" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_1534" s="T85">v-v:inf</ta>
            <ta e="T87" id="Seg_1535" s="T86">num</ta>
            <ta e="T88" id="Seg_1536" s="T87">n-n&gt;adj-n-n:poss</ta>
            <ta e="T89" id="Seg_1537" s="T88">v-v:tense-v:pn</ta>
            <ta e="T90" id="Seg_1538" s="T89">num</ta>
            <ta e="T91" id="Seg_1539" s="T90">n-n:case</ta>
            <ta e="T92" id="Seg_1540" s="T91">n-n&gt;adj</ta>
            <ta e="T93" id="Seg_1541" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_1542" s="T93">v-v:inf</ta>
            <ta e="T95" id="Seg_1543" s="T94">n-n&gt;adj-n-n:num</ta>
            <ta e="T96" id="Seg_1544" s="T95">adv-n:case.poss</ta>
            <ta e="T97" id="Seg_1545" s="T96">v-v:tense-v:pn</ta>
            <ta e="T98" id="Seg_1546" s="T97">n-n:case</ta>
            <ta e="T99" id="Seg_1547" s="T98">preverb</ta>
            <ta e="T100" id="Seg_1548" s="T99">v-v:tense-v:pn</ta>
            <ta e="T101" id="Seg_1549" s="T100">v-v:tense-v:pn</ta>
            <ta e="T102" id="Seg_1550" s="T101">conj</ta>
            <ta e="T103" id="Seg_1551" s="T102">v-v:tense-v:pn</ta>
            <ta e="T104" id="Seg_1552" s="T103">conj</ta>
            <ta e="T105" id="Seg_1553" s="T104">n-n:case-n:poss</ta>
            <ta e="T106" id="Seg_1554" s="T105">n-n:case</ta>
            <ta e="T107" id="Seg_1555" s="T106">v-v:tense-v:pn</ta>
            <ta e="T108" id="Seg_1556" s="T107">dem</ta>
            <ta e="T109" id="Seg_1557" s="T108">n-n:case</ta>
            <ta e="T110" id="Seg_1558" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_1559" s="T110">v-v&gt;ptcp</ta>
            <ta e="T112" id="Seg_1560" s="T111">adv</ta>
            <ta e="T113" id="Seg_1561" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_1562" s="T113">adv</ta>
            <ta e="T115" id="Seg_1563" s="T114">v-v:pn</ta>
            <ta e="T116" id="Seg_1564" s="T115">v-v:ins-v:pn</ta>
            <ta e="T117" id="Seg_1565" s="T116">pers-n:ins-n:num</ta>
            <ta e="T118" id="Seg_1566" s="T117">nprop-n&gt;n-n:case</ta>
            <ta e="T119" id="Seg_1567" s="T118">pers-n:ins-n:case</ta>
            <ta e="T120" id="Seg_1568" s="T119">v-v&gt;ptcp</ta>
            <ta e="T121" id="Seg_1569" s="T120">n-n:case</ta>
            <ta e="T122" id="Seg_1570" s="T121">adj</ta>
            <ta e="T123" id="Seg_1571" s="T122">n-n:case-n:poss</ta>
            <ta e="T124" id="Seg_1572" s="T123">pers-n:case</ta>
            <ta e="T125" id="Seg_1573" s="T124">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T126" id="Seg_1574" s="T125">num</ta>
            <ta e="T127" id="Seg_1575" s="T126">n-n:case</ta>
            <ta e="T128" id="Seg_1576" s="T127">v-v:tense-v:pn</ta>
            <ta e="T129" id="Seg_1577" s="T128">n-n-n:num</ta>
            <ta e="T130" id="Seg_1578" s="T129">v-v:inf</ta>
            <ta e="T131" id="Seg_1579" s="T130">pers-n:case</ta>
            <ta e="T132" id="Seg_1580" s="T131">v-v:ins-v:tense-v:pn</ta>
            <ta e="T133" id="Seg_1581" s="T132">ptcl</ta>
            <ta e="T134" id="Seg_1582" s="T133">adv</ta>
            <ta e="T135" id="Seg_1583" s="T134">ptcl</ta>
            <ta e="T136" id="Seg_1584" s="T135">adv</ta>
            <ta e="T137" id="Seg_1585" s="T136">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1586" s="T0">n</ta>
            <ta e="T2" id="Seg_1587" s="T1">n</ta>
            <ta e="T3" id="Seg_1588" s="T2">v</ta>
            <ta e="T4" id="Seg_1589" s="T3">n</ta>
            <ta e="T5" id="Seg_1590" s="T4">n</ta>
            <ta e="T6" id="Seg_1591" s="T5">n</ta>
            <ta e="T7" id="Seg_1592" s="T6">v</ta>
            <ta e="T8" id="Seg_1593" s="T7">n</ta>
            <ta e="T9" id="Seg_1594" s="T8">n</ta>
            <ta e="T10" id="Seg_1595" s="T9">v</ta>
            <ta e="T11" id="Seg_1596" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_1597" s="T11">n</ta>
            <ta e="T13" id="Seg_1598" s="T12">n</ta>
            <ta e="T14" id="Seg_1599" s="T13">n</ta>
            <ta e="T15" id="Seg_1600" s="T14">v</ta>
            <ta e="T16" id="Seg_1601" s="T15">n</ta>
            <ta e="T17" id="Seg_1602" s="T16">n</ta>
            <ta e="T18" id="Seg_1603" s="T17">n</ta>
            <ta e="T19" id="Seg_1604" s="T18">n</ta>
            <ta e="T20" id="Seg_1605" s="T19">v</ta>
            <ta e="T21" id="Seg_1606" s="T20">pro</ta>
            <ta e="T22" id="Seg_1607" s="T21">n</ta>
            <ta e="T23" id="Seg_1608" s="T22">adj</ta>
            <ta e="T24" id="Seg_1609" s="T23">n</ta>
            <ta e="T25" id="Seg_1610" s="T24">n</ta>
            <ta e="T26" id="Seg_1611" s="T25">pro</ta>
            <ta e="T27" id="Seg_1612" s="T26">n</ta>
            <ta e="T28" id="Seg_1613" s="T27">v</ta>
            <ta e="T29" id="Seg_1614" s="T28">n</ta>
            <ta e="T30" id="Seg_1615" s="T29">quant</ta>
            <ta e="T31" id="Seg_1616" s="T30">v</ta>
            <ta e="T32" id="Seg_1617" s="T31">adj</ta>
            <ta e="T33" id="Seg_1618" s="T32">n</ta>
            <ta e="T34" id="Seg_1619" s="T33">num</ta>
            <ta e="T35" id="Seg_1620" s="T34">n</ta>
            <ta e="T36" id="Seg_1621" s="T35">adj</ta>
            <ta e="T37" id="Seg_1622" s="T36">v</ta>
            <ta e="T38" id="Seg_1623" s="T37">dem</ta>
            <ta e="T39" id="Seg_1624" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_1625" s="T39">n</ta>
            <ta e="T41" id="Seg_1626" s="T40">v</ta>
            <ta e="T42" id="Seg_1627" s="T41">n</ta>
            <ta e="T43" id="Seg_1628" s="T42">num</ta>
            <ta e="T44" id="Seg_1629" s="T43">n</ta>
            <ta e="T45" id="Seg_1630" s="T44">v</ta>
            <ta e="T46" id="Seg_1631" s="T45">n</ta>
            <ta e="T47" id="Seg_1632" s="T46">v</ta>
            <ta e="T48" id="Seg_1633" s="T47">adj</ta>
            <ta e="T49" id="Seg_1634" s="T48">n</ta>
            <ta e="T50" id="Seg_1635" s="T49">preverb</ta>
            <ta e="T51" id="Seg_1636" s="T50">quant</ta>
            <ta e="T52" id="Seg_1637" s="T51">v</ta>
            <ta e="T53" id="Seg_1638" s="T52">quant</ta>
            <ta e="T54" id="Seg_1639" s="T53">n</ta>
            <ta e="T55" id="Seg_1640" s="T54">v</ta>
            <ta e="T56" id="Seg_1641" s="T55">emphpro</ta>
            <ta e="T57" id="Seg_1642" s="T56">n</ta>
            <ta e="T58" id="Seg_1643" s="T57">adj</ta>
            <ta e="T59" id="Seg_1644" s="T58">v</ta>
            <ta e="T60" id="Seg_1645" s="T59">n</ta>
            <ta e="T61" id="Seg_1646" s="T60">n</ta>
            <ta e="T62" id="Seg_1647" s="T61">v</ta>
            <ta e="T63" id="Seg_1648" s="T62">num</ta>
            <ta e="T64" id="Seg_1649" s="T63">n</ta>
            <ta e="T65" id="Seg_1650" s="T64">n</ta>
            <ta e="T66" id="Seg_1651" s="T65">v</ta>
            <ta e="T67" id="Seg_1652" s="T66">n</ta>
            <ta e="T68" id="Seg_1653" s="T67">v</ta>
            <ta e="T69" id="Seg_1654" s="T68">n</ta>
            <ta e="T70" id="Seg_1655" s="T69">v</ta>
            <ta e="T71" id="Seg_1656" s="T70">n</ta>
            <ta e="T72" id="Seg_1657" s="T71">n</ta>
            <ta e="T73" id="Seg_1658" s="T72">v</ta>
            <ta e="T74" id="Seg_1659" s="T73">n</ta>
            <ta e="T75" id="Seg_1660" s="T74">v</ta>
            <ta e="T76" id="Seg_1661" s="T75">dem</ta>
            <ta e="T77" id="Seg_1662" s="T76">pro</ta>
            <ta e="T78" id="Seg_1663" s="T77">n</ta>
            <ta e="T138" id="Seg_1664" s="T78">n</ta>
            <ta e="T79" id="Seg_1665" s="T138">%%</ta>
            <ta e="T80" id="Seg_1666" s="T79">v</ta>
            <ta e="T81" id="Seg_1667" s="T80">n</ta>
            <ta e="T82" id="Seg_1668" s="T81">preverb</ta>
            <ta e="T83" id="Seg_1669" s="T82">v</ta>
            <ta e="T84" id="Seg_1670" s="T83">adv</ta>
            <ta e="T85" id="Seg_1671" s="T84">n</ta>
            <ta e="T86" id="Seg_1672" s="T85">v</ta>
            <ta e="T87" id="Seg_1673" s="T86">num</ta>
            <ta e="T88" id="Seg_1674" s="T87">n</ta>
            <ta e="T89" id="Seg_1675" s="T88">v</ta>
            <ta e="T90" id="Seg_1676" s="T89">num</ta>
            <ta e="T91" id="Seg_1677" s="T90">n</ta>
            <ta e="T92" id="Seg_1678" s="T91">adj</ta>
            <ta e="T93" id="Seg_1679" s="T92">n</ta>
            <ta e="T94" id="Seg_1680" s="T93">v</ta>
            <ta e="T95" id="Seg_1681" s="T94">n</ta>
            <ta e="T96" id="Seg_1682" s="T95">adv</ta>
            <ta e="T97" id="Seg_1683" s="T96">v</ta>
            <ta e="T98" id="Seg_1684" s="T97">n</ta>
            <ta e="T99" id="Seg_1685" s="T98">preverb</ta>
            <ta e="T100" id="Seg_1686" s="T99">v</ta>
            <ta e="T101" id="Seg_1687" s="T100">v</ta>
            <ta e="T102" id="Seg_1688" s="T101">conj</ta>
            <ta e="T103" id="Seg_1689" s="T102">v</ta>
            <ta e="T104" id="Seg_1690" s="T103">conj</ta>
            <ta e="T105" id="Seg_1691" s="T104">n</ta>
            <ta e="T106" id="Seg_1692" s="T105">n</ta>
            <ta e="T107" id="Seg_1693" s="T106">v</ta>
            <ta e="T108" id="Seg_1694" s="T107">pro</ta>
            <ta e="T109" id="Seg_1695" s="T108">n</ta>
            <ta e="T110" id="Seg_1696" s="T109">n</ta>
            <ta e="T111" id="Seg_1697" s="T110">v</ta>
            <ta e="T112" id="Seg_1698" s="T111">adv</ta>
            <ta e="T113" id="Seg_1699" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_1700" s="T113">n</ta>
            <ta e="T115" id="Seg_1701" s="T114">v</ta>
            <ta e="T116" id="Seg_1702" s="T115">v</ta>
            <ta e="T117" id="Seg_1703" s="T116">pers</ta>
            <ta e="T118" id="Seg_1704" s="T117">n</ta>
            <ta e="T119" id="Seg_1705" s="T118">pers</ta>
            <ta e="T120" id="Seg_1706" s="T119">adj</ta>
            <ta e="T121" id="Seg_1707" s="T120">n</ta>
            <ta e="T122" id="Seg_1708" s="T121">adj</ta>
            <ta e="T123" id="Seg_1709" s="T122">n</ta>
            <ta e="T124" id="Seg_1710" s="T123">pers</ta>
            <ta e="T125" id="Seg_1711" s="T124">v</ta>
            <ta e="T126" id="Seg_1712" s="T125">num</ta>
            <ta e="T127" id="Seg_1713" s="T126">n</ta>
            <ta e="T128" id="Seg_1714" s="T127">v</ta>
            <ta e="T129" id="Seg_1715" s="T128">n</ta>
            <ta e="T130" id="Seg_1716" s="T129">v</ta>
            <ta e="T131" id="Seg_1717" s="T130">pers</ta>
            <ta e="T132" id="Seg_1718" s="T131">v</ta>
            <ta e="T133" id="Seg_1719" s="T132">ptcl</ta>
            <ta e="T134" id="Seg_1720" s="T133">adv</ta>
            <ta e="T135" id="Seg_1721" s="T134">ptcl</ta>
            <ta e="T136" id="Seg_1722" s="T135">n</ta>
            <ta e="T137" id="Seg_1723" s="T136">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_1724" s="T0">np.h:S</ta>
            <ta e="T2" id="Seg_1725" s="T1">np:O</ta>
            <ta e="T3" id="Seg_1726" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_1727" s="T4">np.h:S</ta>
            <ta e="T7" id="Seg_1728" s="T6">v:pred</ta>
            <ta e="T8" id="Seg_1729" s="T7">np.h:S</ta>
            <ta e="T10" id="Seg_1730" s="T9">v:pred</ta>
            <ta e="T14" id="Seg_1731" s="T13">np:S</ta>
            <ta e="T15" id="Seg_1732" s="T14">v:pred</ta>
            <ta e="T17" id="Seg_1733" s="T16">np.h:S</ta>
            <ta e="T19" id="Seg_1734" s="T18">np:O</ta>
            <ta e="T20" id="Seg_1735" s="T19">v:pred</ta>
            <ta e="T22" id="Seg_1736" s="T21">np:S</ta>
            <ta e="T24" id="Seg_1737" s="T23">n:pred</ta>
            <ta e="T25" id="Seg_1738" s="T24">np:S</ta>
            <ta e="T27" id="Seg_1739" s="T26">np:O</ta>
            <ta e="T28" id="Seg_1740" s="T27">v:pred</ta>
            <ta e="T29" id="Seg_1741" s="T28">np:O</ta>
            <ta e="T31" id="Seg_1742" s="T30">0.3:S v:pred</ta>
            <ta e="T33" id="Seg_1743" s="T32">np:O</ta>
            <ta e="T35" id="Seg_1744" s="T34">np:S</ta>
            <ta e="T36" id="Seg_1745" s="T35">adj:pred</ta>
            <ta e="T37" id="Seg_1746" s="T36">cop</ta>
            <ta e="T38" id="Seg_1747" s="T37">pro:O</ta>
            <ta e="T40" id="Seg_1748" s="T39">np:S</ta>
            <ta e="T41" id="Seg_1749" s="T40">v:pred</ta>
            <ta e="T44" id="Seg_1750" s="T43">np:S</ta>
            <ta e="T45" id="Seg_1751" s="T44">v:pred</ta>
            <ta e="T47" id="Seg_1752" s="T46">0.3:S v:pred</ta>
            <ta e="T49" id="Seg_1753" s="T48">np:O</ta>
            <ta e="T52" id="Seg_1754" s="T51">0.3:S v:pred</ta>
            <ta e="T54" id="Seg_1755" s="T53">np:O</ta>
            <ta e="T55" id="Seg_1756" s="T54">0.3:S v:pred</ta>
            <ta e="T56" id="Seg_1757" s="T55">pro:S</ta>
            <ta e="T58" id="Seg_1758" s="T57">adj:pred</ta>
            <ta e="T59" id="Seg_1759" s="T58">cop</ta>
            <ta e="T60" id="Seg_1760" s="T59">np.h:S</ta>
            <ta e="T62" id="Seg_1761" s="T61">v:pred</ta>
            <ta e="T64" id="Seg_1762" s="T63">np.h:S</ta>
            <ta e="T65" id="Seg_1763" s="T64">np:O</ta>
            <ta e="T66" id="Seg_1764" s="T65">v:pred</ta>
            <ta e="T67" id="Seg_1765" s="T66">np:O</ta>
            <ta e="T68" id="Seg_1766" s="T67">0.3.h:S v:pred</ta>
            <ta e="T69" id="Seg_1767" s="T68">np:O</ta>
            <ta e="T70" id="Seg_1768" s="T69">0.3.h:S v:pred</ta>
            <ta e="T71" id="Seg_1769" s="T70">np:S</ta>
            <ta e="T73" id="Seg_1770" s="T72">v:pred</ta>
            <ta e="T75" id="Seg_1771" s="T73">s:purp</ta>
            <ta e="T78" id="Seg_1772" s="T77">np.h:S</ta>
            <ta e="T80" id="Seg_1773" s="T79">v:pred</ta>
            <ta e="T81" id="Seg_1774" s="T80">np:S</ta>
            <ta e="T83" id="Seg_1775" s="T82">v:pred</ta>
            <ta e="T86" id="Seg_1776" s="T84">s:purp</ta>
            <ta e="T88" id="Seg_1777" s="T87">np.h:O</ta>
            <ta e="T89" id="Seg_1778" s="T88">0.3.h:S v:pred</ta>
            <ta e="T94" id="Seg_1779" s="T91">s:purp</ta>
            <ta e="T95" id="Seg_1780" s="T94">np.h:S</ta>
            <ta e="T97" id="Seg_1781" s="T96">v:pred</ta>
            <ta e="T98" id="Seg_1782" s="T97">np:O</ta>
            <ta e="T100" id="Seg_1783" s="T99">0.3.h:S v:pred</ta>
            <ta e="T101" id="Seg_1784" s="T100">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T103" id="Seg_1785" s="T102">0.3.h:S v:pred</ta>
            <ta e="T106" id="Seg_1786" s="T105">np:O</ta>
            <ta e="T107" id="Seg_1787" s="T106">0.3.h:S v:pred</ta>
            <ta e="T109" id="Seg_1788" s="T108">np.h:S</ta>
            <ta e="T111" id="Seg_1789" s="T109">s:rel</ta>
            <ta e="T115" id="Seg_1790" s="T114">v:pred</ta>
            <ta e="T116" id="Seg_1791" s="T115">v:pred</ta>
            <ta e="T117" id="Seg_1792" s="T116">pro.h:S</ta>
            <ta e="T123" id="Seg_1793" s="T122">np.h:S</ta>
            <ta e="T124" id="Seg_1794" s="T123">pro.h:O</ta>
            <ta e="T125" id="Seg_1795" s="T124">0.3.h:S v:pred</ta>
            <ta e="T128" id="Seg_1796" s="T127">0.3.h:S v:pred</ta>
            <ta e="T130" id="Seg_1797" s="T128">s:purp</ta>
            <ta e="T131" id="Seg_1798" s="T130">pro.h:S</ta>
            <ta e="T132" id="Seg_1799" s="T131">v:pred 0.3.h:O</ta>
            <ta e="T137" id="Seg_1800" s="T136">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_1801" s="T0">np.h:A</ta>
            <ta e="T2" id="Seg_1802" s="T1">np:P</ta>
            <ta e="T4" id="Seg_1803" s="T3">np:Time</ta>
            <ta e="T5" id="Seg_1804" s="T4">np.h:Th</ta>
            <ta e="T6" id="Seg_1805" s="T5">np:L</ta>
            <ta e="T8" id="Seg_1806" s="T7">np.h:A</ta>
            <ta e="T9" id="Seg_1807" s="T8">np:G</ta>
            <ta e="T13" id="Seg_1808" s="T12">np:Path</ta>
            <ta e="T14" id="Seg_1809" s="T13">np:A</ta>
            <ta e="T16" id="Seg_1810" s="T15">np:G</ta>
            <ta e="T17" id="Seg_1811" s="T16">np.h:A</ta>
            <ta e="T18" id="Seg_1812" s="T17">np:G</ta>
            <ta e="T19" id="Seg_1813" s="T18">np:Th</ta>
            <ta e="T22" id="Seg_1814" s="T21">np:Th</ta>
            <ta e="T25" id="Seg_1815" s="T24">np:A</ta>
            <ta e="T27" id="Seg_1816" s="T26">np:P</ta>
            <ta e="T29" id="Seg_1817" s="T28">np:P</ta>
            <ta e="T31" id="Seg_1818" s="T30">0.3:A</ta>
            <ta e="T33" id="Seg_1819" s="T32">np:P</ta>
            <ta e="T35" id="Seg_1820" s="T34">np:Th</ta>
            <ta e="T38" id="Seg_1821" s="T37">pro:P</ta>
            <ta e="T40" id="Seg_1822" s="T39">np:A</ta>
            <ta e="T42" id="Seg_1823" s="T41">np:So</ta>
            <ta e="T44" id="Seg_1824" s="T43">np:Th</ta>
            <ta e="T46" id="Seg_1825" s="T45">np:G</ta>
            <ta e="T47" id="Seg_1826" s="T46">0.3:A</ta>
            <ta e="T49" id="Seg_1827" s="T48">np:P</ta>
            <ta e="T52" id="Seg_1828" s="T51">0.3:A</ta>
            <ta e="T54" id="Seg_1829" s="T53">np:Th</ta>
            <ta e="T55" id="Seg_1830" s="T54">0.3:A</ta>
            <ta e="T56" id="Seg_1831" s="T55">pro:Th</ta>
            <ta e="T60" id="Seg_1832" s="T59">np.h:A</ta>
            <ta e="T61" id="Seg_1833" s="T60">np:G</ta>
            <ta e="T64" id="Seg_1834" s="T63">np.h:A</ta>
            <ta e="T65" id="Seg_1835" s="T64">np:P</ta>
            <ta e="T67" id="Seg_1836" s="T66">np:P</ta>
            <ta e="T68" id="Seg_1837" s="T67">0.3.h:A</ta>
            <ta e="T69" id="Seg_1838" s="T68">np:Th</ta>
            <ta e="T70" id="Seg_1839" s="T69">0.3.h:A</ta>
            <ta e="T71" id="Seg_1840" s="T70">np:A</ta>
            <ta e="T72" id="Seg_1841" s="T71">np:G</ta>
            <ta e="T74" id="Seg_1842" s="T73">np:Th</ta>
            <ta e="T75" id="Seg_1843" s="T74">0.3:A</ta>
            <ta e="T78" id="Seg_1844" s="T77">np.h:A</ta>
            <ta e="T138" id="Seg_1845" s="T78">np:G</ta>
            <ta e="T81" id="Seg_1846" s="T80">np:P</ta>
            <ta e="T84" id="Seg_1847" s="T83">adv:Time</ta>
            <ta e="T85" id="Seg_1848" s="T84">np:P</ta>
            <ta e="T88" id="Seg_1849" s="T87">np.h:Th</ta>
            <ta e="T89" id="Seg_1850" s="T88">0.3.h:A</ta>
            <ta e="T91" id="Seg_1851" s="T90">np:G</ta>
            <ta e="T93" id="Seg_1852" s="T92">np.h:Th</ta>
            <ta e="T94" id="Seg_1853" s="T93">0.3.h:A</ta>
            <ta e="T95" id="Seg_1854" s="T94">np.h:A</ta>
            <ta e="T96" id="Seg_1855" s="T95">adv:So</ta>
            <ta e="T98" id="Seg_1856" s="T97">np:P</ta>
            <ta e="T100" id="Seg_1857" s="T99">0.3.h:A</ta>
            <ta e="T101" id="Seg_1858" s="T100">0.3.h:A 0.3:P</ta>
            <ta e="T103" id="Seg_1859" s="T102">0.3.h:A</ta>
            <ta e="T105" id="Seg_1860" s="T104">np:So 0.3:Poss</ta>
            <ta e="T106" id="Seg_1861" s="T105">np:P</ta>
            <ta e="T107" id="Seg_1862" s="T106">0.3.h:A</ta>
            <ta e="T109" id="Seg_1863" s="T108">np.h:Th</ta>
            <ta e="T110" id="Seg_1864" s="T109">np:P</ta>
            <ta e="T111" id="Seg_1865" s="T110">0.3.h:A</ta>
            <ta e="T112" id="Seg_1866" s="T111">adv:Time</ta>
            <ta e="T117" id="Seg_1867" s="T116">pro.h:A</ta>
            <ta e="T119" id="Seg_1868" s="T118">pro.h:Poss</ta>
            <ta e="T123" id="Seg_1869" s="T122">np.h:Th </ta>
            <ta e="T124" id="Seg_1870" s="T123">pro.h:Th</ta>
            <ta e="T125" id="Seg_1871" s="T124">0.3.h:A</ta>
            <ta e="T127" id="Seg_1872" s="T126">np:G</ta>
            <ta e="T128" id="Seg_1873" s="T127">0.3.h:A</ta>
            <ta e="T129" id="Seg_1874" s="T128">np.h:Th</ta>
            <ta e="T130" id="Seg_1875" s="T129">0.3.h:A</ta>
            <ta e="T131" id="Seg_1876" s="T130">pro.h:A</ta>
            <ta e="T132" id="Seg_1877" s="T131">0.3.h:Th</ta>
            <ta e="T134" id="Seg_1878" s="T133">adv:Time</ta>
            <ta e="T137" id="Seg_1879" s="T136">0.3.h:Th</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T30" id="Seg_1880" s="T29">RUS:core</ta>
            <ta e="T48" id="Seg_1881" s="T47">RUS:cult</ta>
            <ta e="T51" id="Seg_1882" s="T50">RUS:core</ta>
            <ta e="T53" id="Seg_1883" s="T52">RUS:core</ta>
            <ta e="T54" id="Seg_1884" s="T53">RUS:cult</ta>
            <ta e="T57" id="Seg_1885" s="T56">RUS:cult</ta>
            <ta e="T61" id="Seg_1886" s="T60">RUS:cult</ta>
            <ta e="T72" id="Seg_1887" s="T71">RUS:cult</ta>
            <ta e="T84" id="Seg_1888" s="T83">RUS:core</ta>
            <ta e="T91" id="Seg_1889" s="T90">RUS:cult</ta>
            <ta e="T102" id="Seg_1890" s="T101">RUS:gram</ta>
            <ta e="T104" id="Seg_1891" s="T103">RUS:gram</ta>
            <ta e="T112" id="Seg_1892" s="T111">RUS:core</ta>
            <ta e="T127" id="Seg_1893" s="T126">RUS:cult</ta>
            <ta e="T133" id="Seg_1894" s="T132">RUS:core</ta>
            <ta e="T134" id="Seg_1895" s="T133">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T48" id="Seg_1896" s="T47">Vsub</ta>
            <ta e="T54" id="Seg_1897" s="T53">Vsub</ta>
            <ta e="T57" id="Seg_1898" s="T56">Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T48" id="Seg_1899" s="T47">dir:infl</ta>
            <ta e="T54" id="Seg_1900" s="T53">dir:bare</ta>
            <ta e="T57" id="Seg_1901" s="T56">dir:bare</ta>
            <ta e="T61" id="Seg_1902" s="T60">dir:infl</ta>
            <ta e="T72" id="Seg_1903" s="T71">dir:infl</ta>
            <ta e="T91" id="Seg_1904" s="T90">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_1905" s="T0">Девушка убила медведя.</ta>
            <ta e="T7" id="Seg_1906" s="T3">Осенью женщины дома остались.</ta>
            <ta e="T10" id="Seg_1907" s="T7">Мужики в тайгу пошли.</ta>
            <ta e="T16" id="Seg_1908" s="T10">По маленькому снегу медведь пришел к дому.</ta>
            <ta e="T20" id="Seg_1909" s="T16">Женщины в печку хлеб посадили.</ta>
            <ta e="T24" id="Seg_1910" s="T20">Эта печка уличная.</ta>
            <ta e="T28" id="Seg_1911" s="T24">Медведь эту печь сломал.</ta>
            <ta e="T33" id="Seg_1912" s="T28">Хлеб весь съел, сырой хлеб.</ta>
            <ta e="T37" id="Seg_1913" s="T33">Одна собака привязана была.</ta>
            <ta e="T41" id="Seg_1914" s="T37">Ее медведь тоже съел.</ta>
            <ta e="T45" id="Seg_1915" s="T41">От собаки остался один хвостик.</ta>
            <ta e="T47" id="Seg_1916" s="T45">[Медведь] в сени зашел.</ta>
            <ta e="T52" id="Seg_1917" s="T47">Мешок с мукой весь порвал.</ta>
            <ta e="T59" id="Seg_1918" s="T52">Всю муку вытряхнул, он сам стал белый от муки.</ta>
            <ta e="T66" id="Seg_1919" s="T59">Женщины в подпол спрятались, одна девушка пулю приготовила.</ta>
            <ta e="T70" id="Seg_1920" s="T66">Пулю сделала, ружье зарядила.</ta>
            <ta e="T75" id="Seg_1921" s="T70">Медведь на окно встал, чтобы в избу посмотреть.</ta>
            <ta e="T80" id="Seg_1922" s="T75">Эта девушка в грудь ему выстрелила.</ta>
            <ta e="T83" id="Seg_1923" s="T80">Медведь упал.</ta>
            <ta e="T94" id="Seg_1924" s="T83">Потом, чтобы медведя ободрать, одну женщину отправили за двадцать километров, мужиков позвать.</ta>
            <ta e="T97" id="Seg_1925" s="T94">Мужики оттуда пришли.</ta>
            <ta e="T103" id="Seg_1926" s="T97">Медведя ободрали, сварили и съели.</ta>
            <ta e="T107" id="Seg_1927" s="T103">А из шкуры сделали спальник.</ta>
            <ta e="T115" id="Seg_1928" s="T107">Эта девушка, которая медведя застрелила, сейчас тоже еще жива.</ta>
            <ta e="T118" id="Seg_1929" s="T115">Зовут ее Лукерьей.</ta>
            <ta e="T130" id="Seg_1930" s="T118">У неё старшая сестра с больной щекой, её посылали за двадцать верст мужиков звать.</ta>
            <ta e="T132" id="Seg_1931" s="T130">Она позвала.</ta>
            <ta e="T137" id="Seg_1932" s="T132">Тоже сейчас еще жива.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_1933" s="T0">A girl killed a bear.</ta>
            <ta e="T7" id="Seg_1934" s="T3">In autumn, women stayed at home.</ta>
            <ta e="T10" id="Seg_1935" s="T7">The men went to the taiga.</ta>
            <ta e="T16" id="Seg_1936" s="T10">A bear came to the house by thin snow.</ta>
            <ta e="T20" id="Seg_1937" s="T16">The women put bread into the stove.</ta>
            <ta e="T24" id="Seg_1938" s="T20">This is an outdoor stove.</ta>
            <ta e="T28" id="Seg_1939" s="T24">The bear broke the stove.</ta>
            <ta e="T33" id="Seg_1940" s="T28">It ate all the bread, raw bread.</ta>
            <ta e="T37" id="Seg_1941" s="T33">There was one dog tied there.</ta>
            <ta e="T41" id="Seg_1942" s="T37">The bear ate it, too.</ta>
            <ta e="T45" id="Seg_1943" s="T41">Only a tail remained from the dog.</ta>
            <ta e="T47" id="Seg_1944" s="T45">[The bear] went into the porch.</ta>
            <ta e="T52" id="Seg_1945" s="T47">It tore the flour sack entirely.</ta>
            <ta e="T59" id="Seg_1946" s="T52">It shook out all the flour, itself it became white with flour.</ta>
            <ta e="T66" id="Seg_1947" s="T59">The women hid in the cellar, one girl prepared a bullet.</ta>
            <ta e="T70" id="Seg_1948" s="T66">She made a bullet and loaded the rifle.</ta>
            <ta e="T75" id="Seg_1949" s="T70">The bear got up at the window to look inside the house.</ta>
            <ta e="T80" id="Seg_1950" s="T75">This girl shot [it] into the chest.</ta>
            <ta e="T83" id="Seg_1951" s="T80">The bear fell down.</ta>
            <ta e="T94" id="Seg_1952" s="T83">Then, to skin the bear, they sent one woman twenty kilometers away, to call the men.</ta>
            <ta e="T97" id="Seg_1953" s="T94">The men came from there.</ta>
            <ta e="T103" id="Seg_1954" s="T97">They skinned the bear, cooked it and ate.</ta>
            <ta e="T107" id="Seg_1955" s="T103">And with the skin, they made a sleeping bag.</ta>
            <ta e="T115" id="Seg_1956" s="T107">This girl who shot the bear, she is still alive now.</ta>
            <ta e="T118" id="Seg_1957" s="T115">She is called Lukerya.</ta>
            <ta e="T130" id="Seg_1958" s="T118">She has an elder sister with a sore cheek, [it is her whom] they sent twenty miles away to call the men.</ta>
            <ta e="T132" id="Seg_1959" s="T130">She did call them.</ta>
            <ta e="T137" id="Seg_1960" s="T132">She is also still alive now.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_1961" s="T0">Ein Mädchen erschoss einen Bären.</ta>
            <ta e="T7" id="Seg_1962" s="T3">Im Herbst blieben die Frauen zu Hause.</ta>
            <ta e="T10" id="Seg_1963" s="T7">Die Männer gingen in die Taiga.</ta>
            <ta e="T16" id="Seg_1964" s="T10">Auf dem wenigen Schnee kam der Bär zum Haus.</ta>
            <ta e="T20" id="Seg_1965" s="T16">Die Frauen haben das Brot in den Ofen getan.</ta>
            <ta e="T24" id="Seg_1966" s="T20">Das ist ein Ofen, der draußen steht.</ta>
            <ta e="T28" id="Seg_1967" s="T24">Der Bär hat diesen Ofen zerstört.</ta>
            <ta e="T33" id="Seg_1968" s="T28">Er hat das ganze rohe Brot gefressen.</ta>
            <ta e="T37" id="Seg_1969" s="T33">Ein Hund war am Ofen angebunden.</ta>
            <ta e="T41" id="Seg_1970" s="T37">Der Bär fraß auch ihn.</ta>
            <ta e="T45" id="Seg_1971" s="T41">Vom Hund blieb nur sein Schwanz übrig.</ta>
            <ta e="T47" id="Seg_1972" s="T45">[Der Bär] ging in den Windfang.</ta>
            <ta e="T52" id="Seg_1973" s="T47">Er zeriss die Mehlsäcke.</ta>
            <ta e="T59" id="Seg_1974" s="T52">Er verschüttete das ganze Mehl, er selbst wurde weiß vom Mehl.</ta>
            <ta e="T66" id="Seg_1975" s="T59">Die Frauen versteckten sich im Keller und ein Mädchen bereitete eine Kugel vor.</ta>
            <ta e="T70" id="Seg_1976" s="T66">Sie machte die Kugel und lud das Gewehr.</ta>
            <ta e="T75" id="Seg_1977" s="T70">Der Bär stellte sich ans Fenster, um ins Haus hinein zu schauen.</ta>
            <ta e="T80" id="Seg_1978" s="T75">Das Mädchen schoss ihm in die Brust.</ta>
            <ta e="T83" id="Seg_1979" s="T80">Der Bär fiel um.</ta>
            <ta e="T94" id="Seg_1980" s="T83">Dann, um den Bär zu häuten, schickten sie eine Frau zwanzig Kilometer weiter, um die Männer zu rufen.</ta>
            <ta e="T97" id="Seg_1981" s="T94">Die Männer kamen von dort.</ta>
            <ta e="T103" id="Seg_1982" s="T97">Sie häuteten den Bär, kochten und aßen ihn.</ta>
            <ta e="T107" id="Seg_1983" s="T103">Und aus der Haut machten sie einen Schlafsack.</ta>
            <ta e="T115" id="Seg_1984" s="T107">Dieses Mädchen, das den Bären erschossen hat, lebt immer noch.</ta>
            <ta e="T118" id="Seg_1985" s="T115">Sie heißt Lukerja.</ta>
            <ta e="T130" id="Seg_1986" s="T118">Sie hat eine ältere Schwester mit einer kranken Wange, die wurde zwanzig Kilometer weiter geschickt, um die Männer zu rufen.</ta>
            <ta e="T132" id="Seg_1987" s="T130">Sie rief sie.</ta>
            <ta e="T137" id="Seg_1988" s="T132">Sie lebt auch immer noch.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_1989" s="T0">девка убила медведя</ta>
            <ta e="T7" id="Seg_1990" s="T3">осенью девушки [бабы] дома остались</ta>
            <ta e="T10" id="Seg_1991" s="T7">мужики в тайгу пошли</ta>
            <ta e="T16" id="Seg_1992" s="T10">по маленькому снегу медведь пришел к дому</ta>
            <ta e="T20" id="Seg_1993" s="T16">бабы в печку хлеб посадили</ta>
            <ta e="T24" id="Seg_1994" s="T20">эта печка уличная печка</ta>
            <ta e="T28" id="Seg_1995" s="T24">медведь эту печь сломал</ta>
            <ta e="T33" id="Seg_1996" s="T28">хлеб весь съел сырой хлеб</ta>
            <ta e="T37" id="Seg_1997" s="T33">одна собака привязана была</ta>
            <ta e="T41" id="Seg_1998" s="T37">ее тоже медведь съел</ta>
            <ta e="T45" id="Seg_1999" s="T41">от собаки один хвостик остался</ta>
            <ta e="T47" id="Seg_2000" s="T45">в кладовку зашел</ta>
            <ta e="T52" id="Seg_2001" s="T47">мучной мешок [мешок с мукой] весь прорвал</ta>
            <ta e="T59" id="Seg_2002" s="T52">всю муку вытряхнул сам белый от муки стал</ta>
            <ta e="T66" id="Seg_2003" s="T59">женщины в подпол спрятались одна девка пулю делала</ta>
            <ta e="T70" id="Seg_2004" s="T66">пулю сделала, ружье зарядила</ta>
            <ta e="T75" id="Seg_2005" s="T70">медведь на окно встал в избу смотреть</ta>
            <ta e="T80" id="Seg_2006" s="T75">эта девка в грудь стреляла</ta>
            <ta e="T83" id="Seg_2007" s="T80">медведь упал</ta>
            <ta e="T94" id="Seg_2008" s="T83">потом медведя ободрать одну женщину отправили за двадцать километров</ta>
            <ta e="T97" id="Seg_2009" s="T94">мужики оттуда пришли</ta>
            <ta e="T103" id="Seg_2010" s="T97">медведя ободрали сварили и съели</ta>
            <ta e="T107" id="Seg_2011" s="T103">а из шкуры сделали спальник (спальный мешок)</ta>
            <ta e="T115" id="Seg_2012" s="T107">эта девка которая медведя застрелила сейчас тоже еще жива</ta>
            <ta e="T118" id="Seg_2013" s="T115">зовут ее Лукерьей</ta>
            <ta e="T130" id="Seg_2014" s="T118">у нее с больной щекой старшая сестра ее посылали за 20 верст мужиков звать</ta>
            <ta e="T132" id="Seg_2015" s="T130">она позвала</ta>
            <ta e="T137" id="Seg_2016" s="T132">тоже сейчас еще жива</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto">
            <ta e="T115" id="Seg_2017" s="T107"> тар - еще.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T138" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
