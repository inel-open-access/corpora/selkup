<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KFN_1965_HowWeMakeBaskets_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KFN_1965_HowWeMakeBaskets_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">37</ud-information>
            <ud-information attribute-name="# HIAT:w">31</ud-information>
            <ud-information attribute-name="# e">31</ud-information>
            <ud-information attribute-name="# HIAT:u">4</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KFN">
            <abbreviation>KFN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KFN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T31" id="Seg_0" n="sc" s="T0">
               <ts e="T11" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">mi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">pajaneː</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">meːšpaj</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">čiːdep</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">kaʒna</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">pot</ts>
                  <nts id="Seg_20" n="HIAT:ip">,</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">okur</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">čiːd</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">nagur</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">salʼkwa</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_35" n="HIAT:w" s="T10">stoit</ts>
                  <nts id="Seg_36" n="HIAT:ip">.</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_39" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">okur</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">čiːdko</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">nagur</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">saːlʼkwa</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">merǯikwatɨ</ts>
                  <nts id="Seg_54" n="HIAT:ip">.</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_57" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">šedə</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">čidə</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">čaːr</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">meːkwaj</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_71" n="HIAT:w" s="T20">čeːlʼ</ts>
                  <nts id="Seg_72" n="HIAT:ip">,</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_75" n="HIAT:w" s="T21">šedəqud</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_78" n="HIAT:w" s="T22">tädə</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_81" n="HIAT:w" s="T23">čid</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_84" n="HIAT:w" s="T24">čeːlʼ</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_87" n="HIAT:w" s="T25">meqwat</ts>
                  <nts id="Seg_88" n="HIAT:ip">.</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_91" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_93" n="HIAT:w" s="T26">täːte</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_96" n="HIAT:w" s="T27">čidqo</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_99" n="HIAT:w" s="T28">šedəqwelʼčöt</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_102" n="HIAT:w" s="T29">salʼkwa</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_105" n="HIAT:w" s="T30">iːlʼaqe</ts>
                  <nts id="Seg_106" n="HIAT:ip">.</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T31" id="Seg_108" n="sc" s="T0">
               <ts e="T1" id="Seg_110" n="e" s="T0">mi </ts>
               <ts e="T2" id="Seg_112" n="e" s="T1">pajaneː </ts>
               <ts e="T3" id="Seg_114" n="e" s="T2">meːšpaj </ts>
               <ts e="T4" id="Seg_116" n="e" s="T3">čiːdep </ts>
               <ts e="T5" id="Seg_118" n="e" s="T4">kaʒna </ts>
               <ts e="T6" id="Seg_120" n="e" s="T5">pot, </ts>
               <ts e="T7" id="Seg_122" n="e" s="T6">okur </ts>
               <ts e="T8" id="Seg_124" n="e" s="T7">čiːd </ts>
               <ts e="T9" id="Seg_126" n="e" s="T8">nagur </ts>
               <ts e="T10" id="Seg_128" n="e" s="T9">salʼkwa </ts>
               <ts e="T11" id="Seg_130" n="e" s="T10">stoit. </ts>
               <ts e="T12" id="Seg_132" n="e" s="T11">okur </ts>
               <ts e="T13" id="Seg_134" n="e" s="T12">čiːdko </ts>
               <ts e="T14" id="Seg_136" n="e" s="T13">nagur </ts>
               <ts e="T15" id="Seg_138" n="e" s="T14">saːlʼkwa </ts>
               <ts e="T16" id="Seg_140" n="e" s="T15">merǯikwatɨ. </ts>
               <ts e="T17" id="Seg_142" n="e" s="T16">šedə </ts>
               <ts e="T18" id="Seg_144" n="e" s="T17">čidə </ts>
               <ts e="T19" id="Seg_146" n="e" s="T18">čaːr </ts>
               <ts e="T20" id="Seg_148" n="e" s="T19">meːkwaj </ts>
               <ts e="T21" id="Seg_150" n="e" s="T20">čeːlʼ, </ts>
               <ts e="T22" id="Seg_152" n="e" s="T21">šedəqud </ts>
               <ts e="T23" id="Seg_154" n="e" s="T22">tädə </ts>
               <ts e="T24" id="Seg_156" n="e" s="T23">čid </ts>
               <ts e="T25" id="Seg_158" n="e" s="T24">čeːlʼ </ts>
               <ts e="T26" id="Seg_160" n="e" s="T25">meqwat. </ts>
               <ts e="T27" id="Seg_162" n="e" s="T26">täːte </ts>
               <ts e="T28" id="Seg_164" n="e" s="T27">čidqo </ts>
               <ts e="T29" id="Seg_166" n="e" s="T28">šedəqwelʼčöt </ts>
               <ts e="T30" id="Seg_168" n="e" s="T29">salʼkwa </ts>
               <ts e="T31" id="Seg_170" n="e" s="T30">iːlʼaqe. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T11" id="Seg_171" s="T0">KFN_1965_HowWeMakeBaskets_nar.001 (001.001)</ta>
            <ta e="T16" id="Seg_172" s="T11">KFN_1965_HowWeMakeBaskets_nar.002 (001.002)</ta>
            <ta e="T26" id="Seg_173" s="T16">KFN_1965_HowWeMakeBaskets_nar.003 (001.003)</ta>
            <ta e="T31" id="Seg_174" s="T26">KFN_1965_HowWeMakeBaskets_nar.004 (001.004)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T11" id="Seg_175" s="T0">ми па′jане̄ ′ме̄шпай чӣдеп кажна пот, окур чӣд нагур саlква стоит.</ta>
            <ta e="T16" id="Seg_176" s="T11">окур чӣдко нагур са̄lква мерджикваты.</ta>
            <ta e="T26" id="Seg_177" s="T16">шедъ чидъ ча̄р ме̄квай че̄лʼ, шедъkуд тӓдъ чид че̄лʼ меkват.</ta>
            <ta e="T31" id="Seg_178" s="T26">тӓ̄те чидkо шедъkwеlчӧт саlква ӣlаɣе.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T11" id="Seg_179" s="T0">mi pajaneː meːšpaj čiːdep kaʒna pot, okur čiːd nagur salʼkwa stoit.</ta>
            <ta e="T16" id="Seg_180" s="T11">okur čiːdko nagur saːlʼkva merdžikvatɨ.</ta>
            <ta e="T26" id="Seg_181" s="T16">šedə čidə čaːr meːkvaj čeːlʼ, šedəqud tädə čid čeːlʼ meqvat.</ta>
            <ta e="T31" id="Seg_182" s="T26">täːte čidqo šedəqwelʼčöt salʼkva iːlʼaqe.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T11" id="Seg_183" s="T0">mi pajaneː meːšpaj čiːdep kaʒna pot, okur čiːd nagur salʼkwa stoit. </ta>
            <ta e="T16" id="Seg_184" s="T11">okur čiːdko nagur saːlʼkwa merǯikwatɨ. </ta>
            <ta e="T26" id="Seg_185" s="T16">šedə čidə čaːr meːkwaj čeːlʼ, šedəqud tädə čid čeːlʼ meqwat. </ta>
            <ta e="T31" id="Seg_186" s="T26">täːte čidqo šedəqwelʼčöt salʼkwa iːlʼaqe. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_187" s="T0">mi</ta>
            <ta e="T2" id="Seg_188" s="T1">paja-n-eː</ta>
            <ta e="T3" id="Seg_189" s="T2">meː-špa-j</ta>
            <ta e="T4" id="Seg_190" s="T3">čiːd-e-p</ta>
            <ta e="T5" id="Seg_191" s="T4">kaʒna</ta>
            <ta e="T6" id="Seg_192" s="T5">po-t</ta>
            <ta e="T7" id="Seg_193" s="T6">okur</ta>
            <ta e="T8" id="Seg_194" s="T7">čiːd</ta>
            <ta e="T9" id="Seg_195" s="T8">nagur</ta>
            <ta e="T10" id="Seg_196" s="T9">salʼkwa</ta>
            <ta e="T11" id="Seg_197" s="T10">stoit</ta>
            <ta e="T12" id="Seg_198" s="T11">okur</ta>
            <ta e="T13" id="Seg_199" s="T12">čiːd-ko</ta>
            <ta e="T14" id="Seg_200" s="T13">nagur</ta>
            <ta e="T15" id="Seg_201" s="T14">saːlʼkwa</ta>
            <ta e="T16" id="Seg_202" s="T15">merǯi-k-wa-tɨ</ta>
            <ta e="T17" id="Seg_203" s="T16">šedə</ta>
            <ta e="T18" id="Seg_204" s="T17">čid-ə</ta>
            <ta e="T19" id="Seg_205" s="T18">čaːr</ta>
            <ta e="T20" id="Seg_206" s="T19">meː-k-wa-j</ta>
            <ta e="T21" id="Seg_207" s="T20">čeːlʼ</ta>
            <ta e="T22" id="Seg_208" s="T21">šedə-qud</ta>
            <ta e="T23" id="Seg_209" s="T22">tädə</ta>
            <ta e="T24" id="Seg_210" s="T23">čid</ta>
            <ta e="T25" id="Seg_211" s="T24">čeːlʼ</ta>
            <ta e="T26" id="Seg_212" s="T25">me-q-wa-t</ta>
            <ta e="T27" id="Seg_213" s="T26">täːte</ta>
            <ta e="T28" id="Seg_214" s="T27">čid-qo</ta>
            <ta e="T29" id="Seg_215" s="T28">šedəqwelʼčöt</ta>
            <ta e="T30" id="Seg_216" s="T29">salʼkwa</ta>
            <ta e="T31" id="Seg_217" s="T30">iː-lʼa-qe</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_218" s="T0">mi</ta>
            <ta e="T2" id="Seg_219" s="T1">paja-n-se</ta>
            <ta e="T3" id="Seg_220" s="T2">me-špɨ-j</ta>
            <ta e="T4" id="Seg_221" s="T3">čiːd-ɨ-p</ta>
            <ta e="T5" id="Seg_222" s="T4">kaʒnɨj</ta>
            <ta e="T6" id="Seg_223" s="T5">po-n</ta>
            <ta e="T7" id="Seg_224" s="T6">okkər</ta>
            <ta e="T8" id="Seg_225" s="T7">čiːd</ta>
            <ta e="T9" id="Seg_226" s="T8">nagur</ta>
            <ta e="T10" id="Seg_227" s="T9">salkwa</ta>
            <ta e="T11" id="Seg_228" s="T10">stoit</ta>
            <ta e="T12" id="Seg_229" s="T11">okkər</ta>
            <ta e="T13" id="Seg_230" s="T12">čiːd-tqo</ta>
            <ta e="T14" id="Seg_231" s="T13">nagur</ta>
            <ta e="T15" id="Seg_232" s="T14">salkwa</ta>
            <ta e="T16" id="Seg_233" s="T15">merǯi-ku-wa-dət</ta>
            <ta e="T17" id="Seg_234" s="T16">šitə</ta>
            <ta e="T18" id="Seg_235" s="T17">čiːd-ɨ</ta>
            <ta e="T19" id="Seg_236" s="T18">čaːr</ta>
            <ta e="T20" id="Seg_237" s="T19">me-ku-wa-j</ta>
            <ta e="T21" id="Seg_238" s="T20">čeːl</ta>
            <ta e="T22" id="Seg_239" s="T21">šitə-qɨn</ta>
            <ta e="T23" id="Seg_240" s="T22">tettɨ</ta>
            <ta e="T24" id="Seg_241" s="T23">čiːd</ta>
            <ta e="T25" id="Seg_242" s="T24">čeːl</ta>
            <ta e="T26" id="Seg_243" s="T25">me-ku-wa-tɨ</ta>
            <ta e="T27" id="Seg_244" s="T26">tettɨ</ta>
            <ta e="T28" id="Seg_245" s="T27">čiːd-tqo</ta>
            <ta e="T29" id="Seg_246" s="T28">šedəqwelʼčöt</ta>
            <ta e="T30" id="Seg_247" s="T29">salkwa</ta>
            <ta e="T31" id="Seg_248" s="T30">iː-la-qe</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_249" s="T0">we.DU.NOM</ta>
            <ta e="T2" id="Seg_250" s="T1">wife-GEN-COM</ta>
            <ta e="T3" id="Seg_251" s="T2">do-IPFV2-1DU</ta>
            <ta e="T4" id="Seg_252" s="T3">basket-EP-ACC</ta>
            <ta e="T5" id="Seg_253" s="T4">every</ta>
            <ta e="T6" id="Seg_254" s="T5">year-ADV.LOC</ta>
            <ta e="T7" id="Seg_255" s="T6">one</ta>
            <ta e="T8" id="Seg_256" s="T7">basket.[NOM]</ta>
            <ta e="T9" id="Seg_257" s="T8">three</ta>
            <ta e="T10" id="Seg_258" s="T9">ruble.[NOM]</ta>
            <ta e="T11" id="Seg_259" s="T10">cost.3SG</ta>
            <ta e="T12" id="Seg_260" s="T11">one</ta>
            <ta e="T13" id="Seg_261" s="T12">basket-TRL</ta>
            <ta e="T14" id="Seg_262" s="T13">three</ta>
            <ta e="T15" id="Seg_263" s="T14">ruble</ta>
            <ta e="T16" id="Seg_264" s="T15">pay-HAB-CO-3PL</ta>
            <ta e="T17" id="Seg_265" s="T16">two</ta>
            <ta e="T18" id="Seg_266" s="T17">basket-EP.[NOM]</ta>
            <ta e="T19" id="Seg_267" s="T18">by</ta>
            <ta e="T20" id="Seg_268" s="T19">do-HAB-CO-1DU</ta>
            <ta e="T21" id="Seg_269" s="T20">day.[NOM]</ta>
            <ta e="T22" id="Seg_270" s="T21">two-LOC</ta>
            <ta e="T23" id="Seg_271" s="T22">four</ta>
            <ta e="T24" id="Seg_272" s="T23">basket.[NOM]</ta>
            <ta e="T25" id="Seg_273" s="T24">day.[NOM]</ta>
            <ta e="T26" id="Seg_274" s="T25">do-HAB-CO-3PL</ta>
            <ta e="T27" id="Seg_275" s="T26">four.[NOM]</ta>
            <ta e="T28" id="Seg_276" s="T27">basket-TRL</ta>
            <ta e="T29" id="Seg_277" s="T28">twelve</ta>
            <ta e="T30" id="Seg_278" s="T29">ruble.[NOM]</ta>
            <ta e="T31" id="Seg_279" s="T30">take-FUT-%%</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_280" s="T0">мы.DU.NOM</ta>
            <ta e="T2" id="Seg_281" s="T1">жена-GEN-COM</ta>
            <ta e="T3" id="Seg_282" s="T2">делать-IPFV2-1DU</ta>
            <ta e="T4" id="Seg_283" s="T3">кузов-EP-ACC</ta>
            <ta e="T5" id="Seg_284" s="T4">каждый</ta>
            <ta e="T6" id="Seg_285" s="T5">год-ADV.LOC</ta>
            <ta e="T7" id="Seg_286" s="T6">один</ta>
            <ta e="T8" id="Seg_287" s="T7">кузов.[NOM]</ta>
            <ta e="T9" id="Seg_288" s="T8">три</ta>
            <ta e="T10" id="Seg_289" s="T9">рубль.[NOM]</ta>
            <ta e="T11" id="Seg_290" s="T10">стоить.3SG</ta>
            <ta e="T12" id="Seg_291" s="T11">один</ta>
            <ta e="T13" id="Seg_292" s="T12">кузов-TRL</ta>
            <ta e="T14" id="Seg_293" s="T13">три</ta>
            <ta e="T15" id="Seg_294" s="T14">рубль</ta>
            <ta e="T16" id="Seg_295" s="T15">платить-HAB-CO-3PL</ta>
            <ta e="T17" id="Seg_296" s="T16">два</ta>
            <ta e="T18" id="Seg_297" s="T17">кузов-EP.[NOM]</ta>
            <ta e="T19" id="Seg_298" s="T18">по</ta>
            <ta e="T20" id="Seg_299" s="T19">делать-HAB-CO-1DU</ta>
            <ta e="T21" id="Seg_300" s="T20">день.[NOM]</ta>
            <ta e="T22" id="Seg_301" s="T21">два-LOC</ta>
            <ta e="T23" id="Seg_302" s="T22">четыре</ta>
            <ta e="T24" id="Seg_303" s="T23">кузов.[NOM]</ta>
            <ta e="T25" id="Seg_304" s="T24">день.[NOM]</ta>
            <ta e="T26" id="Seg_305" s="T25">делать-HAB-CO-3PL</ta>
            <ta e="T27" id="Seg_306" s="T26">четыре.[NOM]</ta>
            <ta e="T28" id="Seg_307" s="T27">кузов-TRL</ta>
            <ta e="T29" id="Seg_308" s="T28">двенадцать</ta>
            <ta e="T30" id="Seg_309" s="T29">рубль.[NOM]</ta>
            <ta e="T31" id="Seg_310" s="T30">взять-FUT-%%</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_311" s="T0">pers</ta>
            <ta e="T2" id="Seg_312" s="T1">n-n:case-n:case</ta>
            <ta e="T3" id="Seg_313" s="T2">v-v&gt;v-v:pn</ta>
            <ta e="T4" id="Seg_314" s="T3">n-n:ins-n:case</ta>
            <ta e="T5" id="Seg_315" s="T4">adj</ta>
            <ta e="T6" id="Seg_316" s="T5">n-adv:case</ta>
            <ta e="T7" id="Seg_317" s="T6">num</ta>
            <ta e="T8" id="Seg_318" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_319" s="T8">num</ta>
            <ta e="T10" id="Seg_320" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_321" s="T10">v</ta>
            <ta e="T12" id="Seg_322" s="T11">num</ta>
            <ta e="T13" id="Seg_323" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_324" s="T13">num</ta>
            <ta e="T15" id="Seg_325" s="T14">n</ta>
            <ta e="T16" id="Seg_326" s="T15">v-v:pn-v:ins-v:pn</ta>
            <ta e="T17" id="Seg_327" s="T16">num</ta>
            <ta e="T18" id="Seg_328" s="T17">n-n:ins-n:case</ta>
            <ta e="T19" id="Seg_329" s="T18">pp</ta>
            <ta e="T20" id="Seg_330" s="T19">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T21" id="Seg_331" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_332" s="T21">num-n:case</ta>
            <ta e="T23" id="Seg_333" s="T22">num</ta>
            <ta e="T24" id="Seg_334" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_335" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_336" s="T25">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T27" id="Seg_337" s="T26">num-n:case</ta>
            <ta e="T28" id="Seg_338" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_339" s="T28">num</ta>
            <ta e="T30" id="Seg_340" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_341" s="T30">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_342" s="T0">pers</ta>
            <ta e="T2" id="Seg_343" s="T1">n</ta>
            <ta e="T3" id="Seg_344" s="T2">v</ta>
            <ta e="T4" id="Seg_345" s="T3">n</ta>
            <ta e="T5" id="Seg_346" s="T4">adj</ta>
            <ta e="T6" id="Seg_347" s="T5">n</ta>
            <ta e="T7" id="Seg_348" s="T6">num</ta>
            <ta e="T8" id="Seg_349" s="T7">n</ta>
            <ta e="T9" id="Seg_350" s="T8">num</ta>
            <ta e="T10" id="Seg_351" s="T9">n</ta>
            <ta e="T11" id="Seg_352" s="T10">v</ta>
            <ta e="T12" id="Seg_353" s="T11">num</ta>
            <ta e="T13" id="Seg_354" s="T12">n</ta>
            <ta e="T14" id="Seg_355" s="T13">num</ta>
            <ta e="T15" id="Seg_356" s="T14">n</ta>
            <ta e="T16" id="Seg_357" s="T15">v</ta>
            <ta e="T17" id="Seg_358" s="T16">num</ta>
            <ta e="T18" id="Seg_359" s="T17">n</ta>
            <ta e="T19" id="Seg_360" s="T18">pp</ta>
            <ta e="T20" id="Seg_361" s="T19">v</ta>
            <ta e="T21" id="Seg_362" s="T20">n</ta>
            <ta e="T22" id="Seg_363" s="T21">num</ta>
            <ta e="T23" id="Seg_364" s="T22">num</ta>
            <ta e="T24" id="Seg_365" s="T23">n</ta>
            <ta e="T25" id="Seg_366" s="T24">n</ta>
            <ta e="T26" id="Seg_367" s="T25">v</ta>
            <ta e="T27" id="Seg_368" s="T26">num</ta>
            <ta e="T28" id="Seg_369" s="T27">n</ta>
            <ta e="T29" id="Seg_370" s="T28">num</ta>
            <ta e="T30" id="Seg_371" s="T29">n</ta>
            <ta e="T31" id="Seg_372" s="T30">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_373" s="T0">pro.h:S</ta>
            <ta e="T3" id="Seg_374" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_375" s="T3">np:O</ta>
            <ta e="T8" id="Seg_376" s="T7">np:S</ta>
            <ta e="T11" id="Seg_377" s="T10">v:pred</ta>
            <ta e="T15" id="Seg_378" s="T14">np:O</ta>
            <ta e="T16" id="Seg_379" s="T15">0.3.h:S v:pred</ta>
            <ta e="T18" id="Seg_380" s="T17">np:O</ta>
            <ta e="T20" id="Seg_381" s="T19">0.1.h:S v:pred</ta>
            <ta e="T26" id="Seg_382" s="T25">v:pred</ta>
            <ta e="T31" id="Seg_383" s="T30">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_384" s="T0">pro.h:A</ta>
            <ta e="T2" id="Seg_385" s="T1">np:Com</ta>
            <ta e="T4" id="Seg_386" s="T3">np:P</ta>
            <ta e="T6" id="Seg_387" s="T5">np:Time</ta>
            <ta e="T8" id="Seg_388" s="T7">np:Th</ta>
            <ta e="T15" id="Seg_389" s="T14">np:Th</ta>
            <ta e="T16" id="Seg_390" s="T15">0.3.h:A</ta>
            <ta e="T18" id="Seg_391" s="T17">np:P</ta>
            <ta e="T20" id="Seg_392" s="T19">0.1.h:A</ta>
            <ta e="T21" id="Seg_393" s="T20">np:Time</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_394" s="T4">RUS:core</ta>
            <ta e="T11" id="Seg_395" s="T10">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T11" id="Seg_396" s="T0">Мы с женой делаем кузова каждый год, один кузов стоит три рубля.</ta>
            <ta e="T16" id="Seg_397" s="T11">За один кузов платят три рубля.</ta>
            <ta e="T26" id="Seg_398" s="T16">По два кузова делаем в день, двое четыре кузова за день делают.</ta>
            <ta e="T31" id="Seg_399" s="T26">За четыре кузова получим двенадцать рублей</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T11" id="Seg_400" s="T0">Together with my wife every year we make baskets, one basket costs three rubles.</ta>
            <ta e="T16" id="Seg_401" s="T11">For one basket they pay three rubles.</ta>
            <ta e="T26" id="Seg_402" s="T16">We make two baskets a day, two people make four baskets a day.</ta>
            <ta e="T31" id="Seg_403" s="T26">For four baskets we get twelve rubles.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T11" id="Seg_404" s="T0">Meine Frau und ich machen Körbe, ein Korb kostet drei Rubel.</ta>
            <ta e="T16" id="Seg_405" s="T11">Man zahlt für einen Korb drei Rubel. </ta>
            <ta e="T26" id="Seg_406" s="T16">Jeweils zwei Körbe machen wir in einem Tag, zu zweit machen wir vier Körbe.</ta>
            <ta e="T31" id="Seg_407" s="T26">Für vier Körbe bekommen wir zwölf Rubel.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T11" id="Seg_408" s="T0">мы с женой делаем кузовы один кузов три рубля</ta>
            <ta e="T16" id="Seg_409" s="T11">за один кузов три рубля платят</ta>
            <ta e="T26" id="Seg_410" s="T16">по два кузова делаем в день, двое четыре кузова за день делаем</ta>
            <ta e="T31" id="Seg_411" s="T26">за четыре кузова получим двенадцать рублей</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
