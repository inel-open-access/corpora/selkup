<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KFN_1967_Mammoth_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KFN_1965_Mammoth_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">71</ud-information>
            <ud-information attribute-name="# HIAT:w">56</ud-information>
            <ud-information attribute-name="# e">56</ud-information>
            <ud-information attribute-name="# HIAT:u">9</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KFN">
            <abbreviation>KFN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KFN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T56" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">miːnan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">taper</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">qoʒar</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">tʼäŋgwa</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">A</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">kutə</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">tap</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">tanut</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_32" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">Moʒət</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">eja</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">ötqöt</ts>
                  <nts id="Seg_41" n="HIAT:ip">!</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_44" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">Ugot</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">kupec</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">eːk</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">Köŋeɣɨt</ts>
                  <nts id="Seg_56" n="HIAT:ip">,</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">tabe</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">tawešpɨmbat</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">qoʒarn</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">aːmdɨp</ts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_72" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">na</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">kupeck</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">taumbat</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">qoʒarn</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">amdɨp</ts>
                  <nts id="Seg_87" n="HIAT:ip">,</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_90" n="HIAT:w" s="T24">kudə</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_93" n="HIAT:w" s="T25">qokut</ts>
                  <nts id="Seg_94" n="HIAT:ip">,</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">tabe</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_100" n="HIAT:w" s="T27">tawešpɨmbat</ts>
                  <nts id="Seg_101" n="HIAT:ip">.</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_104" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">Köŋ</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_109" n="HIAT:w" s="T29">aː</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_112" n="HIAT:w" s="T30">warɣə</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_115" n="HIAT:w" s="T31">kɨː</ts>
                  <nts id="Seg_116" n="HIAT:ip">,</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">qoʒart</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">eːkumbat</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">načat</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">ugot</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_132" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_134" n="HIAT:w" s="T36">Köŋ</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_137" n="HIAT:w" s="T37">Tɨmnan</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_140" n="HIAT:w" s="T38">ütčuga</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_143" n="HIAT:w" s="T39">eːk</ts>
                  <nts id="Seg_144" n="HIAT:ip">.</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_147" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_149" n="HIAT:w" s="T40">Köŋeɣɨt</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_152" n="HIAT:w" s="T41">okɨr</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_155" n="HIAT:w" s="T42">ara</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_158" n="HIAT:w" s="T43">qombat</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_161" n="HIAT:w" s="T44">qoʒarn</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_164" n="HIAT:w" s="T45">ol</ts>
                  <nts id="Seg_165" n="HIAT:ip">,</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_168" n="HIAT:w" s="T46">aːmnɨndeː</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_171" n="HIAT:w" s="T47">qombade</ts>
                  <nts id="Seg_172" n="HIAT:ip">,</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_175" n="HIAT:w" s="T48">mat</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_178" n="HIAT:w" s="T49">qonǯirhap</ts>
                  <nts id="Seg_179" n="HIAT:ip">.</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_182" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_184" n="HIAT:w" s="T50">Na</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_187" n="HIAT:w" s="T51">ara</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_190" n="HIAT:w" s="T52">andeː</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_193" n="HIAT:w" s="T53">taːderhɨt</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_196" n="HIAT:w" s="T54">qoʒarn</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_199" n="HIAT:w" s="T55">ollagap</ts>
                  <nts id="Seg_200" n="HIAT:ip">.</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T56" id="Seg_202" n="sc" s="T0">
               <ts e="T1" id="Seg_204" n="e" s="T0">miːnan </ts>
               <ts e="T2" id="Seg_206" n="e" s="T1">taper </ts>
               <ts e="T3" id="Seg_208" n="e" s="T2">qoʒar </ts>
               <ts e="T4" id="Seg_210" n="e" s="T3">tʼäŋgwa. </ts>
               <ts e="T5" id="Seg_212" n="e" s="T4">A </ts>
               <ts e="T6" id="Seg_214" n="e" s="T5">kutə </ts>
               <ts e="T7" id="Seg_216" n="e" s="T6">tap </ts>
               <ts e="T8" id="Seg_218" n="e" s="T7">tanut. </ts>
               <ts e="T9" id="Seg_220" n="e" s="T8">Moʒət </ts>
               <ts e="T10" id="Seg_222" n="e" s="T9">eja </ts>
               <ts e="T11" id="Seg_224" n="e" s="T10">ötqöt! </ts>
               <ts e="T12" id="Seg_226" n="e" s="T11">Ugot </ts>
               <ts e="T13" id="Seg_228" n="e" s="T12">kupec </ts>
               <ts e="T14" id="Seg_230" n="e" s="T13">eːk </ts>
               <ts e="T15" id="Seg_232" n="e" s="T14">Köŋeɣɨt, </ts>
               <ts e="T16" id="Seg_234" n="e" s="T15">tabe </ts>
               <ts e="T17" id="Seg_236" n="e" s="T16">tawešpɨmbat </ts>
               <ts e="T18" id="Seg_238" n="e" s="T17">qoʒarn </ts>
               <ts e="T19" id="Seg_240" n="e" s="T18">aːmdɨp. </ts>
               <ts e="T20" id="Seg_242" n="e" s="T19">na </ts>
               <ts e="T21" id="Seg_244" n="e" s="T20">kupeck </ts>
               <ts e="T22" id="Seg_246" n="e" s="T21">taumbat </ts>
               <ts e="T23" id="Seg_248" n="e" s="T22">qoʒarn </ts>
               <ts e="T24" id="Seg_250" n="e" s="T23">amdɨp, </ts>
               <ts e="T25" id="Seg_252" n="e" s="T24">kudə </ts>
               <ts e="T26" id="Seg_254" n="e" s="T25">qokut, </ts>
               <ts e="T27" id="Seg_256" n="e" s="T26">tabe </ts>
               <ts e="T28" id="Seg_258" n="e" s="T27">tawešpɨmbat. </ts>
               <ts e="T29" id="Seg_260" n="e" s="T28">Köŋ </ts>
               <ts e="T30" id="Seg_262" n="e" s="T29">aː </ts>
               <ts e="T31" id="Seg_264" n="e" s="T30">warɣə </ts>
               <ts e="T32" id="Seg_266" n="e" s="T31">kɨː, </ts>
               <ts e="T33" id="Seg_268" n="e" s="T32">qoʒart </ts>
               <ts e="T34" id="Seg_270" n="e" s="T33">eːkumbat </ts>
               <ts e="T35" id="Seg_272" n="e" s="T34">načat </ts>
               <ts e="T36" id="Seg_274" n="e" s="T35">ugot. </ts>
               <ts e="T37" id="Seg_276" n="e" s="T36">Köŋ </ts>
               <ts e="T38" id="Seg_278" n="e" s="T37">Tɨmnan </ts>
               <ts e="T39" id="Seg_280" n="e" s="T38">ütčuga </ts>
               <ts e="T40" id="Seg_282" n="e" s="T39">eːk. </ts>
               <ts e="T41" id="Seg_284" n="e" s="T40">Köŋeɣɨt </ts>
               <ts e="T42" id="Seg_286" n="e" s="T41">okɨr </ts>
               <ts e="T43" id="Seg_288" n="e" s="T42">ara </ts>
               <ts e="T44" id="Seg_290" n="e" s="T43">qombat </ts>
               <ts e="T45" id="Seg_292" n="e" s="T44">qoʒarn </ts>
               <ts e="T46" id="Seg_294" n="e" s="T45">ol, </ts>
               <ts e="T47" id="Seg_296" n="e" s="T46">aːmnɨndeː </ts>
               <ts e="T48" id="Seg_298" n="e" s="T47">qombade, </ts>
               <ts e="T49" id="Seg_300" n="e" s="T48">mat </ts>
               <ts e="T50" id="Seg_302" n="e" s="T49">qonǯirhap. </ts>
               <ts e="T51" id="Seg_304" n="e" s="T50">Na </ts>
               <ts e="T52" id="Seg_306" n="e" s="T51">ara </ts>
               <ts e="T53" id="Seg_308" n="e" s="T52">andeː </ts>
               <ts e="T54" id="Seg_310" n="e" s="T53">taːderhɨt </ts>
               <ts e="T55" id="Seg_312" n="e" s="T54">qoʒarn </ts>
               <ts e="T56" id="Seg_314" n="e" s="T55">ollagap. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_315" s="T0">KFN_1965_Mammoth_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_316" s="T4">KFN_1965_Mammoth_nar.002 (001.002)</ta>
            <ta e="T11" id="Seg_317" s="T8">KFN_1965_Mammoth_nar.003 (001.003)</ta>
            <ta e="T19" id="Seg_318" s="T11">KFN_1965_Mammoth_nar.004 (001.004)</ta>
            <ta e="T28" id="Seg_319" s="T19">KFN_1965_Mammoth_nar.005 (001.005)</ta>
            <ta e="T36" id="Seg_320" s="T28">KFN_1965_Mammoth_nar.006 (001.006)</ta>
            <ta e="T40" id="Seg_321" s="T36">KFN_1965_Mammoth_nar.007 (001.007)</ta>
            <ta e="T50" id="Seg_322" s="T40">KFN_1965_Mammoth_nar.008 (001.008)</ta>
            <ta e="T56" id="Seg_323" s="T50">KFN_1965_Mammoth_nar.009 (001.009)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_324" s="T0">′мӣнан та′пер ко′жар тʼäңва</ta>
            <ta e="T8" id="Seg_325" s="T4">а кутъ тап та′нут </ta>
            <ta e="T11" id="Seg_326" s="T8">может еjа öтköт</ta>
            <ta e="T19" id="Seg_327" s="T11">у′гот купецʼ ēк кö′ңеɣыт, табъ та′вешпымбат kожарн āмдып</ta>
            <ta e="T28" id="Seg_328" s="T19">на купецк ′таумбат kо′жарн ′амдып, кудъ kо′кут, табъ та′вешпымбат</ta>
            <ta e="T36" id="Seg_329" s="T28">köң ā варɣъ кы̄, kо′жарт ēкумбат на′чат у′гот</ta>
            <ta e="T40" id="Seg_330" s="T36">köң тымнан üтчуга ēк</ta>
            <ta e="T50" id="Seg_331" s="T40">′köңеɣыт окыр ара kомбат kо′жарн ол, āмнындē ′kомбаде, мат kонджирhап</ta>
            <ta e="T56" id="Seg_332" s="T50">на а′ра ан′дē тāдер(ɣ)hыт kо′жарн оллагап</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_333" s="T0">miːnan taper qoʒar tʼäŋgwa.</ta>
            <ta e="T8" id="Seg_334" s="T4">a kutə tap tanut</ta>
            <ta e="T11" id="Seg_335" s="T8">moʒət eja ötqöt </ta>
            <ta e="T19" id="Seg_336" s="T11">ugot kupecʼ eːk köŋeɣɨt, tabe tawešpɨmbat qoʒarn amdɨp. </ta>
            <ta e="T28" id="Seg_337" s="T19">na kupеck taumbat qoʒarn amdɨp, kudə qokut, tabe tawešpɨmbat. </ta>
            <ta e="T36" id="Seg_338" s="T28">köŋ aː warɣə kɨː, qoʒart eːkumbat načat ugot. </ta>
            <ta e="T40" id="Seg_339" s="T36">Köŋ Tɨmnan ütčuga eːk</ta>
            <ta e="T50" id="Seg_340" s="T40">Köŋeɣɨt ‎‎okɨr ara qombat qoʒarn ol, aːmnɨndeː qombade, mat qonǯirhap</ta>
            <ta e="T56" id="Seg_341" s="T50">na ara andeː taːder(ɣ)hɨt qoʒarn ollagap. </ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_342" s="T0">miːnan taper qoʒar tʼäŋgwa. </ta>
            <ta e="T8" id="Seg_343" s="T4">A kutə tap tanut. </ta>
            <ta e="T11" id="Seg_344" s="T8">Moʒət eja ötqöt! </ta>
            <ta e="T19" id="Seg_345" s="T11">Ugot kupec eːk Köŋeɣɨt, tabe tawešpɨmbat qoʒarn aːmdɨp. </ta>
            <ta e="T28" id="Seg_346" s="T19">na kupeck taumbat qoʒarn amdɨp, kudə qokut, tabe tawešpɨmbat. </ta>
            <ta e="T36" id="Seg_347" s="T28">Köŋ aː warɣə kɨː, qoʒart eːkumbat načat ugot. </ta>
            <ta e="T40" id="Seg_348" s="T36">Köŋ Tɨmnan ütčuga eːk. </ta>
            <ta e="T50" id="Seg_349" s="T40">Köŋeɣɨt okɨr ara qombat qoʒarn ol, aːmnɨndeː qombade, mat qonǯirhap. </ta>
            <ta e="T56" id="Seg_350" s="T50">Na ara andeː taːderhɨt qoʒarn ollagap. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_351" s="T0">miː-nan</ta>
            <ta e="T2" id="Seg_352" s="T1">taper</ta>
            <ta e="T3" id="Seg_353" s="T2">qoʒar</ta>
            <ta e="T4" id="Seg_354" s="T3">tʼäŋg-wa</ta>
            <ta e="T5" id="Seg_355" s="T4">a</ta>
            <ta e="T6" id="Seg_356" s="T5">kutə</ta>
            <ta e="T7" id="Seg_357" s="T6">tap</ta>
            <ta e="T8" id="Seg_358" s="T7">tanu-t</ta>
            <ta e="T9" id="Seg_359" s="T8">moʒət</ta>
            <ta e="T10" id="Seg_360" s="T9">e-ja</ta>
            <ta e="T11" id="Seg_361" s="T10">öt-qöt</ta>
            <ta e="T12" id="Seg_362" s="T11">ugot</ta>
            <ta e="T13" id="Seg_363" s="T12">kupec</ta>
            <ta e="T14" id="Seg_364" s="T13">eː-k</ta>
            <ta e="T15" id="Seg_365" s="T14">Köŋ-e-ɣɨt</ta>
            <ta e="T16" id="Seg_366" s="T15">tabe</ta>
            <ta e="T17" id="Seg_367" s="T16">taw-e-špɨ-mba-t</ta>
            <ta e="T18" id="Seg_368" s="T17">qoʒar-n</ta>
            <ta e="T19" id="Seg_369" s="T18">aːmdɨ-p</ta>
            <ta e="T20" id="Seg_370" s="T19">na</ta>
            <ta e="T21" id="Seg_371" s="T20">kupeck</ta>
            <ta e="T22" id="Seg_372" s="T21">tau-mba-t</ta>
            <ta e="T23" id="Seg_373" s="T22">qoʒar-n</ta>
            <ta e="T24" id="Seg_374" s="T23">amdɨ-p</ta>
            <ta e="T25" id="Seg_375" s="T24">kudə</ta>
            <ta e="T26" id="Seg_376" s="T25">qo-ku-t</ta>
            <ta e="T27" id="Seg_377" s="T26">tabe</ta>
            <ta e="T28" id="Seg_378" s="T27">taw-e-špɨ-mba-t</ta>
            <ta e="T29" id="Seg_379" s="T28">Köŋ</ta>
            <ta e="T30" id="Seg_380" s="T29">aː</ta>
            <ta e="T31" id="Seg_381" s="T30">warɣə</ta>
            <ta e="T32" id="Seg_382" s="T31">kɨː</ta>
            <ta e="T33" id="Seg_383" s="T32">qoʒar-t</ta>
            <ta e="T34" id="Seg_384" s="T33">eː-ku-mba-t</ta>
            <ta e="T35" id="Seg_385" s="T34">nača-t</ta>
            <ta e="T36" id="Seg_386" s="T35">ugot</ta>
            <ta e="T37" id="Seg_387" s="T36">Köŋ</ta>
            <ta e="T38" id="Seg_388" s="T37">Tɨm-nan</ta>
            <ta e="T39" id="Seg_389" s="T38">ütčuga</ta>
            <ta e="T40" id="Seg_390" s="T39">eː-k</ta>
            <ta e="T41" id="Seg_391" s="T40">Köŋ-e-ɣɨt</ta>
            <ta e="T42" id="Seg_392" s="T41">okɨr</ta>
            <ta e="T43" id="Seg_393" s="T42">ara</ta>
            <ta e="T44" id="Seg_394" s="T43">qo-mba-t</ta>
            <ta e="T45" id="Seg_395" s="T44">qoʒar-n</ta>
            <ta e="T46" id="Seg_396" s="T45">ol</ta>
            <ta e="T47" id="Seg_397" s="T46">aːmnɨ-n-deː</ta>
            <ta e="T48" id="Seg_398" s="T47">qo-mba-de</ta>
            <ta e="T49" id="Seg_399" s="T48">mat</ta>
            <ta e="T50" id="Seg_400" s="T49">qo-nǯi-r-ha-p</ta>
            <ta e="T51" id="Seg_401" s="T50">na</ta>
            <ta e="T52" id="Seg_402" s="T51">ara</ta>
            <ta e="T53" id="Seg_403" s="T52">andeː</ta>
            <ta e="T54" id="Seg_404" s="T53">taːde-r-hɨ-t</ta>
            <ta e="T55" id="Seg_405" s="T54">qoʒar-n</ta>
            <ta e="T56" id="Seg_406" s="T55">ol-laga-p</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_407" s="T0">mi-nan</ta>
            <ta e="T2" id="Seg_408" s="T1">taper</ta>
            <ta e="T3" id="Seg_409" s="T2">qoʒar</ta>
            <ta e="T4" id="Seg_410" s="T3">čaŋgɨ-wa</ta>
            <ta e="T5" id="Seg_411" s="T4">a</ta>
            <ta e="T6" id="Seg_412" s="T5">kutɨ</ta>
            <ta e="T7" id="Seg_413" s="T6">tap</ta>
            <ta e="T8" id="Seg_414" s="T7">tanu-tɨ</ta>
            <ta e="T9" id="Seg_415" s="T8">moʒət</ta>
            <ta e="T10" id="Seg_416" s="T9">e-ŋɨ</ta>
            <ta e="T11" id="Seg_417" s="T10">üt-qɨn</ta>
            <ta e="T12" id="Seg_418" s="T11">ugon</ta>
            <ta e="T13" id="Seg_419" s="T12">kupeːs</ta>
            <ta e="T14" id="Seg_420" s="T13">e-k</ta>
            <ta e="T15" id="Seg_421" s="T14">Köŋ-ɨ-qɨt</ta>
            <ta e="T16" id="Seg_422" s="T15">tab</ta>
            <ta e="T17" id="Seg_423" s="T16">taw-ɨ-špɨ-mbɨ-tɨ</ta>
            <ta e="T18" id="Seg_424" s="T17">qoʒar-n</ta>
            <ta e="T19" id="Seg_425" s="T18">amdɨ-p</ta>
            <ta e="T20" id="Seg_426" s="T19">na</ta>
            <ta e="T21" id="Seg_427" s="T20">kupeːs</ta>
            <ta e="T22" id="Seg_428" s="T21">taw-mbɨ-tɨ</ta>
            <ta e="T23" id="Seg_429" s="T22">qoʒar-n</ta>
            <ta e="T24" id="Seg_430" s="T23">amdɨ-p</ta>
            <ta e="T25" id="Seg_431" s="T24">kutɨ</ta>
            <ta e="T26" id="Seg_432" s="T25">qo-ku-tɨ</ta>
            <ta e="T27" id="Seg_433" s="T26">tab</ta>
            <ta e="T28" id="Seg_434" s="T27">taw-ɨ-špɨ-mbɨ-tɨ</ta>
            <ta e="T29" id="Seg_435" s="T28">Köŋ</ta>
            <ta e="T30" id="Seg_436" s="T29">aː</ta>
            <ta e="T31" id="Seg_437" s="T30">wargɨ</ta>
            <ta e="T32" id="Seg_438" s="T31">kɨ</ta>
            <ta e="T33" id="Seg_439" s="T32">qoʒar-t</ta>
            <ta e="T34" id="Seg_440" s="T33">e-ku-mbɨ-dət</ta>
            <ta e="T35" id="Seg_441" s="T34">nača-tɨ</ta>
            <ta e="T36" id="Seg_442" s="T35">ugon</ta>
            <ta e="T37" id="Seg_443" s="T36">Köŋ</ta>
            <ta e="T38" id="Seg_444" s="T37">Tɨm-nan</ta>
            <ta e="T39" id="Seg_445" s="T38">üčega</ta>
            <ta e="T40" id="Seg_446" s="T39">e-k</ta>
            <ta e="T41" id="Seg_447" s="T40">Köŋ-ɨ-qɨt</ta>
            <ta e="T42" id="Seg_448" s="T41">okkər</ta>
            <ta e="T43" id="Seg_449" s="T42">ara</ta>
            <ta e="T44" id="Seg_450" s="T43">qo-mbɨ-tɨ</ta>
            <ta e="T45" id="Seg_451" s="T44">qoʒar-n</ta>
            <ta e="T46" id="Seg_452" s="T45">olo</ta>
            <ta e="T47" id="Seg_453" s="T46">amdɨ-t-tɨ</ta>
            <ta e="T48" id="Seg_454" s="T47">qo-mbɨ-tɨ</ta>
            <ta e="T49" id="Seg_455" s="T48">man</ta>
            <ta e="T50" id="Seg_456" s="T49">qo-nǯe-r-sɨ-m</ta>
            <ta e="T51" id="Seg_457" s="T50">na</ta>
            <ta e="T52" id="Seg_458" s="T51">ara</ta>
            <ta e="T53" id="Seg_459" s="T52">ande</ta>
            <ta e="T54" id="Seg_460" s="T53">tade-r-sɨ-tɨ</ta>
            <ta e="T55" id="Seg_461" s="T54">qoʒar-n</ta>
            <ta e="T56" id="Seg_462" s="T55">olo-laga-p</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_463" s="T0">we.DU.NOM-ADES</ta>
            <ta e="T2" id="Seg_464" s="T1">now</ta>
            <ta e="T3" id="Seg_465" s="T2">mammoth.[NOM]</ta>
            <ta e="T4" id="Seg_466" s="T3">NEG.EX-CO.[3SG.S]</ta>
            <ta e="T5" id="Seg_467" s="T4">but</ta>
            <ta e="T6" id="Seg_468" s="T5">who</ta>
            <ta e="T7" id="Seg_469" s="T6">still</ta>
            <ta e="T8" id="Seg_470" s="T7">know-3SG.O</ta>
            <ta e="T9" id="Seg_471" s="T8">maybe</ta>
            <ta e="T10" id="Seg_472" s="T9">be-CO.[3SG.S]</ta>
            <ta e="T11" id="Seg_473" s="T10">water-LOC</ta>
            <ta e="T12" id="Seg_474" s="T11">earlier</ta>
            <ta e="T13" id="Seg_475" s="T12">merchant.[NOM]</ta>
            <ta e="T14" id="Seg_476" s="T13">be-3SG.S</ta>
            <ta e="T15" id="Seg_477" s="T14">Kenga-EP-LOC</ta>
            <ta e="T16" id="Seg_478" s="T15">(s)he.[NOM]</ta>
            <ta e="T17" id="Seg_479" s="T16">buy-EP-IPFV2-PST.NAR-3SG.O</ta>
            <ta e="T18" id="Seg_480" s="T17">mammoth-GEN</ta>
            <ta e="T19" id="Seg_481" s="T18">horn-ACC</ta>
            <ta e="T20" id="Seg_482" s="T19">this</ta>
            <ta e="T21" id="Seg_483" s="T20">merchant</ta>
            <ta e="T22" id="Seg_484" s="T21">buy-PST.NAR-3SG.O</ta>
            <ta e="T23" id="Seg_485" s="T22">mammoth-GEN</ta>
            <ta e="T24" id="Seg_486" s="T23">horn-ACC</ta>
            <ta e="T25" id="Seg_487" s="T24">who.[NOM]</ta>
            <ta e="T26" id="Seg_488" s="T25">find-HAB-3SG.O</ta>
            <ta e="T27" id="Seg_489" s="T26">(s)he.[NOM]</ta>
            <ta e="T28" id="Seg_490" s="T27">buy-EP-IPFV2-PST.NAR-3SG.O</ta>
            <ta e="T29" id="Seg_491" s="T28">Kenga.[NOM]</ta>
            <ta e="T30" id="Seg_492" s="T29">NEG</ta>
            <ta e="T31" id="Seg_493" s="T30">big.[NOM]</ta>
            <ta e="T32" id="Seg_494" s="T31">river.[NOM]</ta>
            <ta e="T33" id="Seg_495" s="T32">mammoth-PL.[NOM]</ta>
            <ta e="T34" id="Seg_496" s="T33">be-HAB-PST.NAR-3PL</ta>
            <ta e="T35" id="Seg_497" s="T34">there-ADV.LOC</ta>
            <ta e="T36" id="Seg_498" s="T35">earlier</ta>
            <ta e="T37" id="Seg_499" s="T36">Kenga.[NOM]</ta>
            <ta e="T38" id="Seg_500" s="T37">river.Tym-ADES</ta>
            <ta e="T39" id="Seg_501" s="T38">small</ta>
            <ta e="T40" id="Seg_502" s="T39">be-3SG.S</ta>
            <ta e="T41" id="Seg_503" s="T40">Kenga-EP-LOC</ta>
            <ta e="T42" id="Seg_504" s="T41">one</ta>
            <ta e="T43" id="Seg_505" s="T42">old.man.[NOM]</ta>
            <ta e="T44" id="Seg_506" s="T43">find-PST.NAR-3SG.O</ta>
            <ta e="T45" id="Seg_507" s="T44">mammoth-GEN</ta>
            <ta e="T46" id="Seg_508" s="T45">head.[NOM]</ta>
            <ta e="T47" id="Seg_509" s="T46">horn-PL-3SG</ta>
            <ta e="T48" id="Seg_510" s="T47">find-PST.NAR-3SG.O</ta>
            <ta e="T49" id="Seg_511" s="T48">I.NOM</ta>
            <ta e="T50" id="Seg_512" s="T49">see-IPFV3-FRQ-PST-1SG.O</ta>
            <ta e="T51" id="Seg_513" s="T50">this</ta>
            <ta e="T52" id="Seg_514" s="T51">old.man.[NOM]</ta>
            <ta e="T53" id="Seg_515" s="T52">boat.[NOM]</ta>
            <ta e="T54" id="Seg_516" s="T53">bring-FRQ-PST-3SG.O</ta>
            <ta e="T55" id="Seg_517" s="T54">mammoth-GEN</ta>
            <ta e="T56" id="Seg_518" s="T55">head-SNGL-ACC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_519" s="T0">мы.DU.NOM-ADES</ta>
            <ta e="T2" id="Seg_520" s="T1">теперь</ta>
            <ta e="T3" id="Seg_521" s="T2">мамонт.[NOM]</ta>
            <ta e="T4" id="Seg_522" s="T3">NEG.EX-CO.[3SG.S]</ta>
            <ta e="T5" id="Seg_523" s="T4">а</ta>
            <ta e="T6" id="Seg_524" s="T5">кто</ta>
            <ta e="T7" id="Seg_525" s="T6">ещё</ta>
            <ta e="T8" id="Seg_526" s="T7">знать-3SG.O</ta>
            <ta e="T9" id="Seg_527" s="T8">может.быть</ta>
            <ta e="T10" id="Seg_528" s="T9">быть-CO.[3SG.S]</ta>
            <ta e="T11" id="Seg_529" s="T10">вода-LOC</ta>
            <ta e="T12" id="Seg_530" s="T11">раньше</ta>
            <ta e="T13" id="Seg_531" s="T12">купец.[NOM]</ta>
            <ta e="T14" id="Seg_532" s="T13">быть-3SG.S</ta>
            <ta e="T15" id="Seg_533" s="T14">Кенга-EP-LOC</ta>
            <ta e="T16" id="Seg_534" s="T15">он(а).[NOM]</ta>
            <ta e="T17" id="Seg_535" s="T16">купить-EP-IPFV2-PST.NAR-3SG.O</ta>
            <ta e="T18" id="Seg_536" s="T17">мамонт-GEN</ta>
            <ta e="T19" id="Seg_537" s="T18">рог-ACC</ta>
            <ta e="T20" id="Seg_538" s="T19">этот</ta>
            <ta e="T21" id="Seg_539" s="T20">купец</ta>
            <ta e="T22" id="Seg_540" s="T21">купить-PST.NAR-3SG.O</ta>
            <ta e="T23" id="Seg_541" s="T22">мамонт-GEN</ta>
            <ta e="T24" id="Seg_542" s="T23">рог-ACC</ta>
            <ta e="T25" id="Seg_543" s="T24">кто.[NOM]</ta>
            <ta e="T26" id="Seg_544" s="T25">найти-HAB-3SG.O</ta>
            <ta e="T27" id="Seg_545" s="T26">он(а).[NOM]</ta>
            <ta e="T28" id="Seg_546" s="T27">купить-EP-IPFV2-PST.NAR-3SG.O</ta>
            <ta e="T29" id="Seg_547" s="T28">Кенга.[NOM]</ta>
            <ta e="T30" id="Seg_548" s="T29">NEG</ta>
            <ta e="T31" id="Seg_549" s="T30">большой.[NOM]</ta>
            <ta e="T32" id="Seg_550" s="T31">река.[NOM]</ta>
            <ta e="T33" id="Seg_551" s="T32">мамонт-PL.[NOM]</ta>
            <ta e="T34" id="Seg_552" s="T33">быть-HAB-PST.NAR-3PL</ta>
            <ta e="T35" id="Seg_553" s="T34">там-ADV.LOC</ta>
            <ta e="T36" id="Seg_554" s="T35">раньше</ta>
            <ta e="T37" id="Seg_555" s="T36">Кенга.[NOM]</ta>
            <ta e="T38" id="Seg_556" s="T37">река.Тым-ADES</ta>
            <ta e="T39" id="Seg_557" s="T38">маленький</ta>
            <ta e="T40" id="Seg_558" s="T39">быть-3SG.S</ta>
            <ta e="T41" id="Seg_559" s="T40">Кенга-EP-LOC</ta>
            <ta e="T42" id="Seg_560" s="T41">один</ta>
            <ta e="T43" id="Seg_561" s="T42">старик.[NOM]</ta>
            <ta e="T44" id="Seg_562" s="T43">найти-PST.NAR-3SG.O</ta>
            <ta e="T45" id="Seg_563" s="T44">мамонт-GEN</ta>
            <ta e="T46" id="Seg_564" s="T45">голова.[NOM]</ta>
            <ta e="T47" id="Seg_565" s="T46">рог-PL-3SG</ta>
            <ta e="T48" id="Seg_566" s="T47">найти-PST.NAR-3SG.O</ta>
            <ta e="T49" id="Seg_567" s="T48">я.NOM</ta>
            <ta e="T50" id="Seg_568" s="T49">увидеть-IPFV3-FRQ-PST-1SG.O</ta>
            <ta e="T51" id="Seg_569" s="T50">этот</ta>
            <ta e="T52" id="Seg_570" s="T51">старик.[NOM]</ta>
            <ta e="T53" id="Seg_571" s="T52">обласок.[NOM]</ta>
            <ta e="T54" id="Seg_572" s="T53">принести-FRQ-PST-3SG.O</ta>
            <ta e="T55" id="Seg_573" s="T54">мамонт-GEN</ta>
            <ta e="T56" id="Seg_574" s="T55">голова-SNGL-ACC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_575" s="T0">pers-n:case</ta>
            <ta e="T2" id="Seg_576" s="T1">adv</ta>
            <ta e="T3" id="Seg_577" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_578" s="T3">v-v:ins-v:pn</ta>
            <ta e="T5" id="Seg_579" s="T4">conj</ta>
            <ta e="T6" id="Seg_580" s="T5">interrog</ta>
            <ta e="T7" id="Seg_581" s="T6">adv</ta>
            <ta e="T8" id="Seg_582" s="T7">v-v:pn</ta>
            <ta e="T9" id="Seg_583" s="T8">v</ta>
            <ta e="T10" id="Seg_584" s="T9">v-v:ins-v:pn</ta>
            <ta e="T11" id="Seg_585" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_586" s="T11">adv</ta>
            <ta e="T13" id="Seg_587" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_588" s="T13">v-v:pn</ta>
            <ta e="T15" id="Seg_589" s="T14">nprop-n:ins-n:case</ta>
            <ta e="T16" id="Seg_590" s="T15">pers-n:case</ta>
            <ta e="T17" id="Seg_591" s="T16">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_592" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_593" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_594" s="T19">dem</ta>
            <ta e="T21" id="Seg_595" s="T20">n</ta>
            <ta e="T22" id="Seg_596" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_597" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_598" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_599" s="T24">interrog-n:case</ta>
            <ta e="T26" id="Seg_600" s="T25">v-v&gt;v-v:pn</ta>
            <ta e="T27" id="Seg_601" s="T26">pers-n:case</ta>
            <ta e="T28" id="Seg_602" s="T27">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_603" s="T28">nprop-n:case</ta>
            <ta e="T30" id="Seg_604" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_605" s="T30">adj-n:case</ta>
            <ta e="T32" id="Seg_606" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_607" s="T32">n-n:num-n:case</ta>
            <ta e="T34" id="Seg_608" s="T33">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_609" s="T34">adv-adv:case</ta>
            <ta e="T36" id="Seg_610" s="T35">adv</ta>
            <ta e="T37" id="Seg_611" s="T36">nprop-n:case</ta>
            <ta e="T38" id="Seg_612" s="T37">nprop-n:case</ta>
            <ta e="T39" id="Seg_613" s="T38">adj</ta>
            <ta e="T40" id="Seg_614" s="T39">v-v:pn</ta>
            <ta e="T41" id="Seg_615" s="T40">nprop-n:ins-n:case</ta>
            <ta e="T42" id="Seg_616" s="T41">num</ta>
            <ta e="T43" id="Seg_617" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_618" s="T43">v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_619" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_620" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_621" s="T46">n-n:num-n:poss</ta>
            <ta e="T48" id="Seg_622" s="T47">v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_623" s="T48">pers</ta>
            <ta e="T50" id="Seg_624" s="T49">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T51" id="Seg_625" s="T50">dem</ta>
            <ta e="T52" id="Seg_626" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_627" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_628" s="T53">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T55" id="Seg_629" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_630" s="T55">n-n&gt;n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_631" s="T0">pers</ta>
            <ta e="T2" id="Seg_632" s="T1">adv</ta>
            <ta e="T3" id="Seg_633" s="T2">n</ta>
            <ta e="T4" id="Seg_634" s="T3">v</ta>
            <ta e="T5" id="Seg_635" s="T4">conj</ta>
            <ta e="T6" id="Seg_636" s="T5">interrog</ta>
            <ta e="T7" id="Seg_637" s="T6">adv</ta>
            <ta e="T8" id="Seg_638" s="T7">v</ta>
            <ta e="T9" id="Seg_639" s="T8">v</ta>
            <ta e="T10" id="Seg_640" s="T9">v</ta>
            <ta e="T11" id="Seg_641" s="T10">n</ta>
            <ta e="T12" id="Seg_642" s="T11">adv</ta>
            <ta e="T13" id="Seg_643" s="T12">n</ta>
            <ta e="T14" id="Seg_644" s="T13">v</ta>
            <ta e="T15" id="Seg_645" s="T14">nprop</ta>
            <ta e="T16" id="Seg_646" s="T15">pers</ta>
            <ta e="T17" id="Seg_647" s="T16">v</ta>
            <ta e="T18" id="Seg_648" s="T17">n</ta>
            <ta e="T19" id="Seg_649" s="T18">n</ta>
            <ta e="T20" id="Seg_650" s="T19">pro</ta>
            <ta e="T21" id="Seg_651" s="T20">n</ta>
            <ta e="T22" id="Seg_652" s="T21">v</ta>
            <ta e="T23" id="Seg_653" s="T22">n</ta>
            <ta e="T24" id="Seg_654" s="T23">n</ta>
            <ta e="T25" id="Seg_655" s="T24">interrog</ta>
            <ta e="T26" id="Seg_656" s="T25">v</ta>
            <ta e="T27" id="Seg_657" s="T26">pers</ta>
            <ta e="T28" id="Seg_658" s="T27">v</ta>
            <ta e="T29" id="Seg_659" s="T28">nprop</ta>
            <ta e="T30" id="Seg_660" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_661" s="T30">adj</ta>
            <ta e="T32" id="Seg_662" s="T31">n</ta>
            <ta e="T33" id="Seg_663" s="T32">n</ta>
            <ta e="T34" id="Seg_664" s="T33">v</ta>
            <ta e="T35" id="Seg_665" s="T34">adv</ta>
            <ta e="T36" id="Seg_666" s="T35">adv</ta>
            <ta e="T37" id="Seg_667" s="T36">nprop</ta>
            <ta e="T38" id="Seg_668" s="T37">nprop</ta>
            <ta e="T39" id="Seg_669" s="T38">adj</ta>
            <ta e="T40" id="Seg_670" s="T39">v</ta>
            <ta e="T41" id="Seg_671" s="T40">nprop</ta>
            <ta e="T42" id="Seg_672" s="T41">num</ta>
            <ta e="T43" id="Seg_673" s="T42">n</ta>
            <ta e="T44" id="Seg_674" s="T43">v</ta>
            <ta e="T45" id="Seg_675" s="T44">n</ta>
            <ta e="T46" id="Seg_676" s="T45">n</ta>
            <ta e="T47" id="Seg_677" s="T46">n</ta>
            <ta e="T48" id="Seg_678" s="T47">v</ta>
            <ta e="T49" id="Seg_679" s="T48">pers</ta>
            <ta e="T50" id="Seg_680" s="T49">v</ta>
            <ta e="T51" id="Seg_681" s="T50">pro</ta>
            <ta e="T52" id="Seg_682" s="T51">n</ta>
            <ta e="T53" id="Seg_683" s="T52">n</ta>
            <ta e="T54" id="Seg_684" s="T53">v</ta>
            <ta e="T55" id="Seg_685" s="T54">n</ta>
            <ta e="T56" id="Seg_686" s="T55">n</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_687" s="T2">np:S</ta>
            <ta e="T4" id="Seg_688" s="T3">v:pred</ta>
            <ta e="T6" id="Seg_689" s="T5">pro.h:S</ta>
            <ta e="T8" id="Seg_690" s="T7">v:pred</ta>
            <ta e="T10" id="Seg_691" s="T9">0.3:S v:pred</ta>
            <ta e="T13" id="Seg_692" s="T12">np.h:S</ta>
            <ta e="T14" id="Seg_693" s="T13">v:pred</ta>
            <ta e="T16" id="Seg_694" s="T15">pro.h:S</ta>
            <ta e="T17" id="Seg_695" s="T16">v:pred</ta>
            <ta e="T19" id="Seg_696" s="T18">np:O</ta>
            <ta e="T21" id="Seg_697" s="T20">np.h:S</ta>
            <ta e="T22" id="Seg_698" s="T21">np.h:S</ta>
            <ta e="T24" id="Seg_699" s="T23">np:O</ta>
            <ta e="T25" id="Seg_700" s="T24">pro.h:S</ta>
            <ta e="T26" id="Seg_701" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_702" s="T26">pro.h:S</ta>
            <ta e="T28" id="Seg_703" s="T27">v:pred</ta>
            <ta e="T29" id="Seg_704" s="T28">np:S</ta>
            <ta e="T32" id="Seg_705" s="T31">n:pred</ta>
            <ta e="T33" id="Seg_706" s="T32">np:S</ta>
            <ta e="T34" id="Seg_707" s="T33">v:pred</ta>
            <ta e="T37" id="Seg_708" s="T36">np:S</ta>
            <ta e="T39" id="Seg_709" s="T38">adj:pred</ta>
            <ta e="T40" id="Seg_710" s="T39">cop</ta>
            <ta e="T43" id="Seg_711" s="T42">np.h:S</ta>
            <ta e="T44" id="Seg_712" s="T43">v:pred</ta>
            <ta e="T46" id="Seg_713" s="T45">np:O</ta>
            <ta e="T47" id="Seg_714" s="T46">np:O</ta>
            <ta e="T48" id="Seg_715" s="T47">0.3.h:S v:pred</ta>
            <ta e="T49" id="Seg_716" s="T48">pro.h:S</ta>
            <ta e="T50" id="Seg_717" s="T49">v:pred</ta>
            <ta e="T52" id="Seg_718" s="T51">np.h:S</ta>
            <ta e="T54" id="Seg_719" s="T53">v:pred</ta>
            <ta e="T56" id="Seg_720" s="T55">np:O</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_721" s="T0">pro:L</ta>
            <ta e="T2" id="Seg_722" s="T1">adv:Time</ta>
            <ta e="T3" id="Seg_723" s="T2">np:Th</ta>
            <ta e="T6" id="Seg_724" s="T5">pro.h:E</ta>
            <ta e="T10" id="Seg_725" s="T9">0.3:Th</ta>
            <ta e="T11" id="Seg_726" s="T10">np:L</ta>
            <ta e="T12" id="Seg_727" s="T11">adv:Time</ta>
            <ta e="T13" id="Seg_728" s="T12">np.h:Th</ta>
            <ta e="T15" id="Seg_729" s="T14">np:L</ta>
            <ta e="T16" id="Seg_730" s="T15">pro.h:A</ta>
            <ta e="T18" id="Seg_731" s="T17">np:Poss</ta>
            <ta e="T19" id="Seg_732" s="T18">np:Th</ta>
            <ta e="T21" id="Seg_733" s="T20">np.h:A</ta>
            <ta e="T23" id="Seg_734" s="T22">np:Poss</ta>
            <ta e="T24" id="Seg_735" s="T23">np:Th</ta>
            <ta e="T25" id="Seg_736" s="T24">pro.h:A</ta>
            <ta e="T27" id="Seg_737" s="T26">pro.h:A</ta>
            <ta e="T29" id="Seg_738" s="T28">np:Th</ta>
            <ta e="T33" id="Seg_739" s="T32">np:Th</ta>
            <ta e="T35" id="Seg_740" s="T34">adv:L</ta>
            <ta e="T36" id="Seg_741" s="T35">adv:Time</ta>
            <ta e="T37" id="Seg_742" s="T36">np:Th</ta>
            <ta e="T41" id="Seg_743" s="T40">np:L</ta>
            <ta e="T43" id="Seg_744" s="T42">np.h:E</ta>
            <ta e="T45" id="Seg_745" s="T44">np:Poss</ta>
            <ta e="T46" id="Seg_746" s="T45">np:Th</ta>
            <ta e="T47" id="Seg_747" s="T46">np:Th</ta>
            <ta e="T48" id="Seg_748" s="T47">0.3.h:E</ta>
            <ta e="T49" id="Seg_749" s="T48">pro.h:E</ta>
            <ta e="T52" id="Seg_750" s="T51">np.h:A</ta>
            <ta e="T53" id="Seg_751" s="T52">np:L</ta>
            <ta e="T55" id="Seg_752" s="T54">np:Poss</ta>
            <ta e="T56" id="Seg_753" s="T55">np:Th</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_754" s="T1">RUS:core</ta>
            <ta e="T5" id="Seg_755" s="T4">RUS:gram</ta>
            <ta e="T9" id="Seg_756" s="T8">RUS:core</ta>
            <ta e="T13" id="Seg_757" s="T12">RUS:cult</ta>
            <ta e="T21" id="Seg_758" s="T20">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_759" s="T0">У нас мамонтов теперь нет. </ta>
            <ta e="T8" id="Seg_760" s="T4">А кто знает! </ta>
            <ta e="T11" id="Seg_761" s="T8">Может быть, под водой есть!</ta>
            <ta e="T19" id="Seg_762" s="T11">Раньше был купец на Кёнге, он покупал рога мамонта. </ta>
            <ta e="T28" id="Seg_763" s="T19">Этот купец покупал рога мамонта, кто найдёт, он покупал. </ta>
            <ta e="T36" id="Seg_764" s="T28">Кёнга — небольшая река, а мамонты раньше там были. </ta>
            <ta e="T40" id="Seg_765" s="T36">Кёнга меньше, чем Тым. </ta>
            <ta e="T50" id="Seg_766" s="T40">Один старик нашел на реке Кёнге голову мамонта с рогами, я видел. </ta>
            <ta e="T56" id="Seg_767" s="T50">Этот старик вёз на лодке голову мамонта.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_768" s="T0">There are no mammoths here.</ta>
            <ta e="T8" id="Seg_769" s="T4">But who knows yet!</ta>
            <ta e="T11" id="Seg_770" s="T8">Maybe they are under the water.</ta>
            <ta e="T19" id="Seg_771" s="T11">Earlier there was a merchant at the Kjonga, he bought mammoth horn.</ta>
            <ta e="T28" id="Seg_772" s="T19">That mercant bought mammoth horn, who finds, he buys.</ta>
            <ta e="T36" id="Seg_773" s="T28">The Kjonga is not a big river, but earlier mammoths have been there.</ta>
            <ta e="T40" id="Seg_774" s="T36">The Kjonga is smaller than the Tym.</ta>
            <ta e="T50" id="Seg_775" s="T40">An old man found a mammoth head with horns in the Kyonga, I have seen that. </ta>
            <ta e="T56" id="Seg_776" s="T50">The old man was bringing a mammoth head in his boat.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_777" s="T0">Bei uns gibt es keine Mammuts.</ta>
            <ta e="T8" id="Seg_778" s="T4">Aber wer weiß das noch! </ta>
            <ta e="T11" id="Seg_779" s="T8">Vielleicht sind sie unter Wasser!</ta>
            <ta e="T19" id="Seg_780" s="T11">Früher war der Kaufmann an der Kjonga, er kaufte Mammuthorn. </ta>
            <ta e="T28" id="Seg_781" s="T19">Dieser Kaufmann kaufte Mammonthorn, wer findet, er kauft.</ta>
            <ta e="T36" id="Seg_782" s="T28">Die Kjonga ist kein großer Fluss, aber früher waren die Mammuts dort.</ta>
            <ta e="T40" id="Seg_783" s="T36">Die Kjonga ist kleiner als der Tym. </ta>
            <ta e="T50" id="Seg_784" s="T40">Ein alter Mann hat in der Kjonga einen Mammutkopf mit Hörnern gefunden, ich habe es gesehen. </ta>
            <ta e="T56" id="Seg_785" s="T50">Dieser alte Mann bringt im Boot einen Mammutkopf.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_786" s="T0">у нас мамонтов теперь нет. </ta>
            <ta e="T8" id="Seg_787" s="T4">а кто его знает</ta>
            <ta e="T11" id="Seg_788" s="T8">может быть под водой </ta>
            <ta e="T19" id="Seg_789" s="T11">раньше был купец на Кёнге он покупал мамонтовы рога</ta>
            <ta e="T28" id="Seg_790" s="T19">этот купец покупал мамонта рога, кто найдет, он покупал</ta>
            <ta e="T36" id="Seg_791" s="T28">Кёнга - небольщая река, а мамонты были там раньше</ta>
            <ta e="T40" id="Seg_792" s="T36">Кöнга меньше, чем р. Тым</ta>
            <ta e="T50" id="Seg_793" s="T40">один старик по р. Кöнга нашел мамонтову голову с рогами, я видел</ta>
            <ta e="T56" id="Seg_794" s="T50">этот старик вез на лодке голову мамонта</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T40" id="Seg_795" s="T36">[WNB:] Tɨm-nan: it must be in ablative, not in adessive.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
