<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KFN_1965_MyWifesDream_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KFN_1965_MyWifesDream_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">36</ud-information>
            <ud-information attribute-name="# HIAT:w">29</ud-information>
            <ud-information attribute-name="# e">29</ud-information>
            <ud-information attribute-name="# HIAT:u">4</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KFN">
            <abbreviation>KFN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KFN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T29" id="Seg_0" n="sc" s="T0">
               <ts e="T11" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">mat</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">pajat</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">tap</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">pet</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">ködetämba</ts>
                  <nts id="Seg_17" n="HIAT:ip">,</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">qajda</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">tömba</ts>
                  <nts id="Seg_24" n="HIAT:ip">,</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_27" n="HIAT:w" s="T7">ilʼlʼe</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">čelʼkəmpat</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_33" n="HIAT:w" s="T9">atčep</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_36" n="HIAT:w" s="T10">qajda</ts>
                  <nts id="Seg_37" n="HIAT:ip">.</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_40" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">mat</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">üntelʼǯap</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">qajto</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_51" n="HIAT:w" s="T14">tap</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_54" n="HIAT:w" s="T15">parka</ts>
                  <nts id="Seg_55" n="HIAT:ip">.</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_58" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">mat</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_63" n="HIAT:w" s="T17">nʼa</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_66" n="HIAT:w" s="T18">ibɨǯʼimbɨlʼe</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_69" n="HIAT:w" s="T19">innä</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_72" n="HIAT:w" s="T20">šödap</ts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_76" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">tab</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">qonǯerbat</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_84" n="HIAT:w" s="T23">kak</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_87" n="HIAT:w" s="T24">kwälɨn</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_90" n="HIAT:w" s="T25">olʼ</ts>
                  <nts id="Seg_91" n="HIAT:ip">,</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_94" n="HIAT:w" s="T26">na</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_97" n="HIAT:w" s="T27">tabɨp</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_100" n="HIAT:w" s="T28">čelkɨlbat</ts>
                  <nts id="Seg_101" n="HIAT:ip">.</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T29" id="Seg_103" n="sc" s="T0">
               <ts e="T1" id="Seg_105" n="e" s="T0">mat </ts>
               <ts e="T2" id="Seg_107" n="e" s="T1">pajat </ts>
               <ts e="T3" id="Seg_109" n="e" s="T2">tap </ts>
               <ts e="T4" id="Seg_111" n="e" s="T3">pet </ts>
               <ts e="T5" id="Seg_113" n="e" s="T4">ködetämba, </ts>
               <ts e="T6" id="Seg_115" n="e" s="T5">qajda </ts>
               <ts e="T7" id="Seg_117" n="e" s="T6">tömba, </ts>
               <ts e="T8" id="Seg_119" n="e" s="T7">ilʼlʼe </ts>
               <ts e="T9" id="Seg_121" n="e" s="T8">čelʼkəmpat </ts>
               <ts e="T10" id="Seg_123" n="e" s="T9">atčep </ts>
               <ts e="T11" id="Seg_125" n="e" s="T10">qajda. </ts>
               <ts e="T12" id="Seg_127" n="e" s="T11">mat </ts>
               <ts e="T13" id="Seg_129" n="e" s="T12">üntelʼǯap </ts>
               <ts e="T14" id="Seg_131" n="e" s="T13">qajto </ts>
               <ts e="T15" id="Seg_133" n="e" s="T14">tap </ts>
               <ts e="T16" id="Seg_135" n="e" s="T15">parka. </ts>
               <ts e="T17" id="Seg_137" n="e" s="T16">mat </ts>
               <ts e="T18" id="Seg_139" n="e" s="T17">nʼa </ts>
               <ts e="T19" id="Seg_141" n="e" s="T18">ibɨǯʼimbɨlʼe </ts>
               <ts e="T20" id="Seg_143" n="e" s="T19">innä </ts>
               <ts e="T21" id="Seg_145" n="e" s="T20">šödap. </ts>
               <ts e="T22" id="Seg_147" n="e" s="T21">tab </ts>
               <ts e="T23" id="Seg_149" n="e" s="T22">qonǯerbat </ts>
               <ts e="T24" id="Seg_151" n="e" s="T23">kak </ts>
               <ts e="T25" id="Seg_153" n="e" s="T24">kwälɨn </ts>
               <ts e="T26" id="Seg_155" n="e" s="T25">olʼ, </ts>
               <ts e="T27" id="Seg_157" n="e" s="T26">na </ts>
               <ts e="T28" id="Seg_159" n="e" s="T27">tabɨp </ts>
               <ts e="T29" id="Seg_161" n="e" s="T28">čelkɨlbat. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T11" id="Seg_162" s="T0">KFN_1965_MyWifesDream_nar.001 (001.001)</ta>
            <ta e="T16" id="Seg_163" s="T11">KFN_1965_MyWifesDream_nar.002 (001.002)</ta>
            <ta e="T21" id="Seg_164" s="T16">KFN_1965_MyWifesDream_nar.003 (001.003)</ta>
            <ta e="T29" id="Seg_165" s="T21">KFN_1965_MyWifesDream_nar.004 (001.004)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T11" id="Seg_166" s="T0">мат па′jат тап′пет ′кӧдетӓмба, kайда тӧмба, и′lle чеlкъмпат атчеп kайда.</ta>
            <ta e="T16" id="Seg_167" s="T11">мат ӱнтеlджап kай′то тап пар′ка.</ta>
            <ta e="T21" id="Seg_168" s="T16">мат нʼа ибыдʼжʼимбылʼе и′ннӓ шӧ′дап.</ta>
            <ta e="T29" id="Seg_169" s="T21">таб kонджер′бат как квӓлын ′оl, на таб̂ып челкыл′п[б]ат.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T11" id="Seg_170" s="T0">mat pajat tappet ködetämba, qajda tömba, ilʼlʼe čelʼkəmpat atčep qajda.</ta>
            <ta e="T16" id="Seg_171" s="T11">mat üntelʼǯap qajto tap parka.</ta>
            <ta e="T21" id="Seg_172" s="T16">mat nʼa ibɨǯʼimbɨlʼe innä šödap.</ta>
            <ta e="T29" id="Seg_173" s="T21">tab qonǯerbat kak kvälɨn olʼ, na tab̂ɨp čelkɨlp[b]at.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T11" id="Seg_174" s="T0">mat pajat tap pet ködetämba, qajda tömba, ilʼlʼe čelʼkəmpat atčep qajda. </ta>
            <ta e="T16" id="Seg_175" s="T11">mat üntelʼǯap qajto tap parka. </ta>
            <ta e="T21" id="Seg_176" s="T16">mat nʼa ibɨǯʼimbɨlʼe innä šödap. </ta>
            <ta e="T29" id="Seg_177" s="T21">tab qonǯerbat kak kwälɨn olʼ, na tabɨp čelkɨlbat. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_178" s="T0">mat</ta>
            <ta e="T2" id="Seg_179" s="T1">paja-t</ta>
            <ta e="T3" id="Seg_180" s="T2">tap</ta>
            <ta e="T4" id="Seg_181" s="T3">pe-t</ta>
            <ta e="T5" id="Seg_182" s="T4">ködetä-mba</ta>
            <ta e="T6" id="Seg_183" s="T5">qaj-da</ta>
            <ta e="T7" id="Seg_184" s="T6">tö-mba</ta>
            <ta e="T8" id="Seg_185" s="T7">ilʼlʼe</ta>
            <ta e="T9" id="Seg_186" s="T8">čelʼkə-mpa-t</ta>
            <ta e="T10" id="Seg_187" s="T9">atčep</ta>
            <ta e="T11" id="Seg_188" s="T10">qaj-da</ta>
            <ta e="T12" id="Seg_189" s="T11">mat</ta>
            <ta e="T13" id="Seg_190" s="T12">ünte-lʼǯa-p</ta>
            <ta e="T14" id="Seg_191" s="T13">qaj-to</ta>
            <ta e="T15" id="Seg_192" s="T14">tap</ta>
            <ta e="T16" id="Seg_193" s="T15">parka</ta>
            <ta e="T17" id="Seg_194" s="T16">mat</ta>
            <ta e="T18" id="Seg_195" s="T17">nʼa</ta>
            <ta e="T19" id="Seg_196" s="T18">ibɨǯʼi-mbɨ-lʼe</ta>
            <ta e="T20" id="Seg_197" s="T19">innä</ta>
            <ta e="T21" id="Seg_198" s="T20">šöd-a-p</ta>
            <ta e="T22" id="Seg_199" s="T21">tab</ta>
            <ta e="T23" id="Seg_200" s="T22">qonǯer-ba-t</ta>
            <ta e="T24" id="Seg_201" s="T23">kak</ta>
            <ta e="T25" id="Seg_202" s="T24">kwäl-ɨ-n</ta>
            <ta e="T26" id="Seg_203" s="T25">olʼ</ta>
            <ta e="T27" id="Seg_204" s="T26">na</ta>
            <ta e="T28" id="Seg_205" s="T27">tab-ɨ-p</ta>
            <ta e="T29" id="Seg_206" s="T28">čelkɨ-l-ba-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_207" s="T0">man</ta>
            <ta e="T2" id="Seg_208" s="T1">paja-tɨ</ta>
            <ta e="T3" id="Seg_209" s="T2">taw</ta>
            <ta e="T4" id="Seg_210" s="T3">pe-n</ta>
            <ta e="T5" id="Seg_211" s="T4">kodɛta-mbɨ</ta>
            <ta e="T6" id="Seg_212" s="T5">qaj-da</ta>
            <ta e="T7" id="Seg_213" s="T6">töː-mbɨ</ta>
            <ta e="T8" id="Seg_214" s="T7">illä</ta>
            <ta e="T9" id="Seg_215" s="T8">čelkɨ-mbɨ-tɨ</ta>
            <ta e="T10" id="Seg_216" s="T9">ačem</ta>
            <ta e="T11" id="Seg_217" s="T10">qaj-da</ta>
            <ta e="T12" id="Seg_218" s="T11">man</ta>
            <ta e="T13" id="Seg_219" s="T12">undɛ-lʼčǝ-m</ta>
            <ta e="T14" id="Seg_220" s="T13">qaj-da</ta>
            <ta e="T15" id="Seg_221" s="T14">tab</ta>
            <ta e="T16" id="Seg_222" s="T15">parka</ta>
            <ta e="T17" id="Seg_223" s="T16">man</ta>
            <ta e="T18" id="Seg_224" s="T17">neː</ta>
            <ta e="T19" id="Seg_225" s="T18">ibɨǯʼi-mbɨ-le</ta>
            <ta e="T20" id="Seg_226" s="T19">inne</ta>
            <ta e="T21" id="Seg_227" s="T20">šid-ɨ-m</ta>
            <ta e="T22" id="Seg_228" s="T21">tab</ta>
            <ta e="T23" id="Seg_229" s="T22">qonǯer-mbɨ-tɨ</ta>
            <ta e="T24" id="Seg_230" s="T23">kak</ta>
            <ta e="T25" id="Seg_231" s="T24">qwel-ɨ-n</ta>
            <ta e="T26" id="Seg_232" s="T25">olo</ta>
            <ta e="T27" id="Seg_233" s="T26">na</ta>
            <ta e="T28" id="Seg_234" s="T27">tab-ɨ-m</ta>
            <ta e="T29" id="Seg_235" s="T28">čelkɨ-lɨ-mbɨ-tɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_236" s="T0">I.GEN</ta>
            <ta e="T2" id="Seg_237" s="T1">old.woman.[NOM]-3SG</ta>
            <ta e="T3" id="Seg_238" s="T2">this</ta>
            <ta e="T4" id="Seg_239" s="T3">night-ADV.LOC</ta>
            <ta e="T5" id="Seg_240" s="T4">dream-PST.NAR.[3SG.S]</ta>
            <ta e="T6" id="Seg_241" s="T5">what-INDEF</ta>
            <ta e="T7" id="Seg_242" s="T6">come-PST.NAR.[3SG.S]</ta>
            <ta e="T8" id="Seg_243" s="T7">down</ta>
            <ta e="T9" id="Seg_244" s="T8">crush.down-PST.NAR-3SG.O</ta>
            <ta e="T10" id="Seg_245" s="T9">heavy</ta>
            <ta e="T11" id="Seg_246" s="T10">what-INDEF.[NOM]</ta>
            <ta e="T12" id="Seg_247" s="T11">I.NOM</ta>
            <ta e="T13" id="Seg_248" s="T12">hear-PFV-1SG.O</ta>
            <ta e="T14" id="Seg_249" s="T13">what-INDEF.[NOM]</ta>
            <ta e="T15" id="Seg_250" s="T14">(s)he.[NOM]</ta>
            <ta e="T16" id="Seg_251" s="T15">shout.[3SG.S]</ta>
            <ta e="T17" id="Seg_252" s="T16">I.NOM</ta>
            <ta e="T18" id="Seg_253" s="T17">woman.[NOM]</ta>
            <ta e="T19" id="Seg_254" s="T18">%%-DUR-CVB</ta>
            <ta e="T20" id="Seg_255" s="T19">up</ta>
            <ta e="T21" id="Seg_256" s="T20">wake.up-EP-1SG.O</ta>
            <ta e="T22" id="Seg_257" s="T21">(s)he.[NOM]</ta>
            <ta e="T23" id="Seg_258" s="T22">see-PST.NAR-3SG.O</ta>
            <ta e="T24" id="Seg_259" s="T23">how</ta>
            <ta e="T25" id="Seg_260" s="T24">fish-EP-GEN</ta>
            <ta e="T26" id="Seg_261" s="T25">head.[NOM]</ta>
            <ta e="T27" id="Seg_262" s="T26">this</ta>
            <ta e="T28" id="Seg_263" s="T27">(s)he-EP-ACC</ta>
            <ta e="T29" id="Seg_264" s="T28">crush.down-RES-PST.NAR-3SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_265" s="T0">я.GEN</ta>
            <ta e="T2" id="Seg_266" s="T1">старуха.[NOM]-3SG</ta>
            <ta e="T3" id="Seg_267" s="T2">этот</ta>
            <ta e="T4" id="Seg_268" s="T3">ночь-ADV.LOC</ta>
            <ta e="T5" id="Seg_269" s="T4">мечтать-PST.NAR.[3SG.S]</ta>
            <ta e="T6" id="Seg_270" s="T5">что-INDEF</ta>
            <ta e="T7" id="Seg_271" s="T6">прийти-PST.NAR.[3SG.S]</ta>
            <ta e="T8" id="Seg_272" s="T7">вниз</ta>
            <ta e="T9" id="Seg_273" s="T8">придавить-PST.NAR-3SG.O</ta>
            <ta e="T10" id="Seg_274" s="T9">тяжёлый</ta>
            <ta e="T11" id="Seg_275" s="T10">что-INDEF.[NOM]</ta>
            <ta e="T12" id="Seg_276" s="T11">я.NOM</ta>
            <ta e="T13" id="Seg_277" s="T12">слышать-PFV-1SG.O</ta>
            <ta e="T14" id="Seg_278" s="T13">что-INDEF.[NOM]</ta>
            <ta e="T15" id="Seg_279" s="T14">он(а).[NOM]</ta>
            <ta e="T16" id="Seg_280" s="T15">кричать.[3SG.S]</ta>
            <ta e="T17" id="Seg_281" s="T16">я.NOM</ta>
            <ta e="T18" id="Seg_282" s="T17">женщина.[NOM]</ta>
            <ta e="T19" id="Seg_283" s="T18">%%-DUR-CVB</ta>
            <ta e="T20" id="Seg_284" s="T19">вверх</ta>
            <ta e="T21" id="Seg_285" s="T20">разбудить-EP-1SG.O</ta>
            <ta e="T22" id="Seg_286" s="T21">он(а).[NOM]</ta>
            <ta e="T23" id="Seg_287" s="T22">видеть-PST.NAR-3SG.O</ta>
            <ta e="T24" id="Seg_288" s="T23">как</ta>
            <ta e="T25" id="Seg_289" s="T24">рыба-EP-GEN</ta>
            <ta e="T26" id="Seg_290" s="T25">голова.[NOM]</ta>
            <ta e="T27" id="Seg_291" s="T26">этот</ta>
            <ta e="T28" id="Seg_292" s="T27">он(а)-EP-ACC</ta>
            <ta e="T29" id="Seg_293" s="T28">придавить-RES-PST.NAR-3SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_294" s="T0">pers</ta>
            <ta e="T2" id="Seg_295" s="T1">n-n:case-n:poss</ta>
            <ta e="T3" id="Seg_296" s="T2">dem</ta>
            <ta e="T4" id="Seg_297" s="T3">n-adv:case</ta>
            <ta e="T5" id="Seg_298" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_299" s="T5">interrog-clit</ta>
            <ta e="T7" id="Seg_300" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_301" s="T7">preverb</ta>
            <ta e="T9" id="Seg_302" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_303" s="T9">adj</ta>
            <ta e="T11" id="Seg_304" s="T10">interrog-clit-n:case</ta>
            <ta e="T12" id="Seg_305" s="T11">pers</ta>
            <ta e="T13" id="Seg_306" s="T12">v-v&gt;v-v:pn</ta>
            <ta e="T14" id="Seg_307" s="T13">interrog-clit-n:case</ta>
            <ta e="T15" id="Seg_308" s="T14">pers-n:case</ta>
            <ta e="T16" id="Seg_309" s="T15">v-v:pn</ta>
            <ta e="T17" id="Seg_310" s="T16">pers</ta>
            <ta e="T18" id="Seg_311" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_312" s="T18">v-v&gt;v-v&gt;adv</ta>
            <ta e="T20" id="Seg_313" s="T19">preverb</ta>
            <ta e="T21" id="Seg_314" s="T20">v-n:ins-v:pn</ta>
            <ta e="T22" id="Seg_315" s="T21">pers-n:case</ta>
            <ta e="T23" id="Seg_316" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_317" s="T23">interrog</ta>
            <ta e="T25" id="Seg_318" s="T24">n-n:ins-n:case</ta>
            <ta e="T26" id="Seg_319" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_320" s="T26">dem</ta>
            <ta e="T28" id="Seg_321" s="T27">pers-n:ins-n:case</ta>
            <ta e="T29" id="Seg_322" s="T28">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_323" s="T0">pers</ta>
            <ta e="T2" id="Seg_324" s="T1">n</ta>
            <ta e="T3" id="Seg_325" s="T2">dem</ta>
            <ta e="T4" id="Seg_326" s="T3">adv</ta>
            <ta e="T5" id="Seg_327" s="T4">v</ta>
            <ta e="T6" id="Seg_328" s="T5">pro</ta>
            <ta e="T7" id="Seg_329" s="T6">v</ta>
            <ta e="T8" id="Seg_330" s="T7">preverb</ta>
            <ta e="T9" id="Seg_331" s="T8">v</ta>
            <ta e="T10" id="Seg_332" s="T9">adj</ta>
            <ta e="T11" id="Seg_333" s="T10">pro</ta>
            <ta e="T12" id="Seg_334" s="T11">pers</ta>
            <ta e="T13" id="Seg_335" s="T12">v</ta>
            <ta e="T14" id="Seg_336" s="T13">pro</ta>
            <ta e="T15" id="Seg_337" s="T14">pers</ta>
            <ta e="T16" id="Seg_338" s="T15">v</ta>
            <ta e="T17" id="Seg_339" s="T16">pers</ta>
            <ta e="T18" id="Seg_340" s="T17">n</ta>
            <ta e="T19" id="Seg_341" s="T18">v</ta>
            <ta e="T20" id="Seg_342" s="T19">preverb</ta>
            <ta e="T21" id="Seg_343" s="T20">v</ta>
            <ta e="T22" id="Seg_344" s="T21">pers</ta>
            <ta e="T23" id="Seg_345" s="T22">v</ta>
            <ta e="T24" id="Seg_346" s="T23">interrog</ta>
            <ta e="T25" id="Seg_347" s="T24">n</ta>
            <ta e="T26" id="Seg_348" s="T25">n</ta>
            <ta e="T27" id="Seg_349" s="T26">pro</ta>
            <ta e="T28" id="Seg_350" s="T27">pers</ta>
            <ta e="T29" id="Seg_351" s="T28">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_352" s="T1">np.h:S</ta>
            <ta e="T5" id="Seg_353" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_354" s="T5">pro.h:S</ta>
            <ta e="T7" id="Seg_355" s="T6">v:pred</ta>
            <ta e="T9" id="Seg_356" s="T8">v:pred 0.3.h:O</ta>
            <ta e="T11" id="Seg_357" s="T10">pro:S</ta>
            <ta e="T12" id="Seg_358" s="T11">pro.h:S</ta>
            <ta e="T13" id="Seg_359" s="T12">v:pred</ta>
            <ta e="T14" id="Seg_360" s="T13">pro:O</ta>
            <ta e="T15" id="Seg_361" s="T14">pro.h:S</ta>
            <ta e="T16" id="Seg_362" s="T15">v:pred</ta>
            <ta e="T17" id="Seg_363" s="T16">pro.h:S</ta>
            <ta e="T18" id="Seg_364" s="T17">np.h:O</ta>
            <ta e="T19" id="Seg_365" s="T18">s:temp</ta>
            <ta e="T21" id="Seg_366" s="T20">v:pred</ta>
            <ta e="T22" id="Seg_367" s="T21">pro.h:S</ta>
            <ta e="T23" id="Seg_368" s="T22">v:pred</ta>
            <ta e="T29" id="Seg_369" s="T23">s:compl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_370" s="T0">pro.h:Poss</ta>
            <ta e="T2" id="Seg_371" s="T1">np.h:E</ta>
            <ta e="T4" id="Seg_372" s="T3">adv:Time</ta>
            <ta e="T6" id="Seg_373" s="T5">pro.h:A</ta>
            <ta e="T9" id="Seg_374" s="T8">0.3.h:P</ta>
            <ta e="T11" id="Seg_375" s="T10">pro:A</ta>
            <ta e="T12" id="Seg_376" s="T11">pro.h:E</ta>
            <ta e="T14" id="Seg_377" s="T13">pro:Th</ta>
            <ta e="T15" id="Seg_378" s="T14">pro.h:A</ta>
            <ta e="T17" id="Seg_379" s="T16">pro.h:A</ta>
            <ta e="T18" id="Seg_380" s="T17">np.h:P</ta>
            <ta e="T22" id="Seg_381" s="T21">pro.h:E</ta>
            <ta e="T25" id="Seg_382" s="T24">pro:Poss</ta>
            <ta e="T26" id="Seg_383" s="T25">np:A</ta>
            <ta e="T28" id="Seg_384" s="T27">pro.h:P</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T24" id="Seg_385" s="T23">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T11" id="Seg_386" s="T0">Моя жена сегодня ночью сон видела, кто-то пришел, ее придавило что-то тяжелое.</ta>
            <ta e="T16" id="Seg_387" s="T11">Я услышал, чего-то она кричит.</ta>
            <ta e="T21" id="Seg_388" s="T16">Я ее толкал(?), разбудил.</ta>
            <ta e="T29" id="Seg_389" s="T21">Она (во сне) видела, как рыбья голова ее придавила.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T11" id="Seg_390" s="T0">My wife saw a dream this night: somebody came, she was pressed by something heavy. </ta>
            <ta e="T16" id="Seg_391" s="T11">I heard, she was shouting something.</ta>
            <ta e="T21" id="Seg_392" s="T16">I pushed(?) her, I woke her up.</ta>
            <ta e="T29" id="Seg_393" s="T21">She saw a fish head having pressed her down.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T11" id="Seg_394" s="T0">Meine Frau träumte diese Nacht: Jemand kam, sie wurde von etwas Schwerem niedergedrückt.</ta>
            <ta e="T16" id="Seg_395" s="T11">Ich hörte, sie rief etwas.</ta>
            <ta e="T21" id="Seg_396" s="T16">Ich stieß (?) sie, ich weckte sie auf.</ta>
            <ta e="T29" id="Seg_397" s="T21">Sie sah, wie ein Fischkopf sie niederdrückte.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T11" id="Seg_398" s="T0">моя жена сегодня ночью сон видела, кто-то пришел, ее придавило что-то тяжелое</ta>
            <ta e="T16" id="Seg_399" s="T11">я услышал чего-то она кричит</ta>
            <ta e="T21" id="Seg_400" s="T16">я ее толкал разбудил</ta>
            <ta e="T29" id="Seg_401" s="T21">она во сне видела как рыбья голова этот самый ее придавил</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T11" id="Seg_402" s="T0">[WNB:] the possessive suffix is wrong</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
