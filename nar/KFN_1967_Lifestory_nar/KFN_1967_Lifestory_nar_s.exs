<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name />
         <referenced-file url="KFN_1967_Lifestory_nar.wav" />
         <referenced-file url="KFN_1967_Lifestory_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KFN_1967_Lifestory_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">244</ud-information>
            <ud-information attribute-name="# HIAT:w">156</ud-information>
            <ud-information attribute-name="# e">158</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">2</ud-information>
            <ud-information attribute-name="# HIAT:u">40</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SPK">
            <abbreviation>KFN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="6.9" type="appl" />
         <tli id="T1" time="7.88" type="appl" />
         <tli id="T2" time="8.86" type="appl" />
         <tli id="T3" time="9.84" type="appl" />
         <tli id="T4" time="11.057" type="appl" />
         <tli id="T5" time="11.563" type="appl" />
         <tli id="T6" time="12.07" type="appl" />
         <tli id="T7" time="13.35" type="appl" />
         <tli id="T8" time="14.14" type="appl" />
         <tli id="T9" time="14.93" type="appl" />
         <tli id="T10" time="15.72" type="appl" />
         <tli id="T11" time="17.65" type="appl" />
         <tli id="T12" time="18.68" type="appl" />
         <tli id="T13" time="20.2" type="appl" />
         <tli id="T14" time="20.87" type="appl" />
         <tli id="T15" time="21.54" type="appl" />
         <tli id="T16" time="22.817" type="appl" />
         <tli id="T17" time="23.203" type="appl" />
         <tli id="T18" time="23.59" type="appl" />
         <tli id="T19" time="24.235" type="appl" />
         <tli id="T20" time="24.87" type="appl" />
         <tli id="T21" time="28.498" type="appl" />
         <tli id="T22" time="29.655" type="appl" />
         <tli id="T23" time="30.812000789302946" />
         <tli id="T24" time="31.97" type="appl" />
         <tli id="T25" time="32.872" type="appl" />
         <tli id="T26" time="33.525" type="appl" />
         <tli id="T27" time="34.177" type="appl" />
         <tli id="T28" time="34.83" type="appl" />
         <tli id="T158" time="34.881" type="intp" />
         <tli id="T29" time="37.76" type="appl" />
         <tli id="T30" time="38.57" type="appl" />
         <tli id="T31" time="39.38" type="appl" />
         <tli id="T32" time="40.392" type="appl" />
         <tli id="T33" time="41.315" type="appl" />
         <tli id="T34" time="42.238" type="appl" />
         <tli id="T35" time="43.16" type="appl" />
         <tli id="T36" time="44.656" type="appl" />
         <tli id="T37" time="45.521" type="appl" />
         <tli id="T38" time="46.387" type="appl" />
         <tli id="T39" time="48.548" type="appl" />
         <tli id="T40" time="49.681" type="appl" />
         <tli id="T41" time="50.813" type="appl" />
         <tli id="T42" time="51.945" type="appl" />
         <tli id="T43" time="53.077" type="appl" />
         <tli id="T44" time="54.21" type="appl" />
         <tli id="T45" time="55.342" type="appl" />
         <tli id="T46" time="57.357" type="appl" />
         <tli id="T47" time="58.163" type="appl" />
         <tli id="T48" time="58.969" type="appl" />
         <tli id="T148" time="59.02" type="intp" />
         <tli id="T49" time="61.421" type="appl" />
         <tli id="T50" time="62.934" type="appl" />
         <tli id="T51" time="64.446" type="appl" />
         <tli id="T52" time="66.549" type="appl" />
         <tli id="T53" time="67.397" type="appl" />
         <tli id="T54" time="68.245" type="appl" />
         <tli id="T55" time="69.094" type="appl" />
         <tli id="T149" time="69.145" type="intp" />
         <tli id="T56" time="69.942" type="appl" />
         <tli id="T57" time="70.79" type="appl" />
         <tli id="T58" time="72.928" type="appl" />
         <tli id="T59" time="74.14" type="appl" />
         <tli id="T60" time="75.351" type="appl" />
         <tli id="T61" time="76.563" type="appl" />
         <tli id="T62" time="77.775" type="appl" />
         <tli id="T63" time="79.256" type="appl" />
         <tli id="T64" time="80.214" type="appl" />
         <tli id="T65" time="81.173" type="appl" />
         <tli id="T66" time="82.131" type="appl" />
         <tli id="T67" time="83.08233276531482" />
         <tli id="T68" time="84.693" type="appl" />
         <tli id="T69" time="85.402" type="appl" />
         <tli id="T70" time="86.111" type="appl" />
         <tli id="T71" time="86.82" type="appl" />
         <tli id="T72" time="87.529" type="appl" />
         <tli id="T73" time="88.238" type="appl" />
         <tli id="T74" time="89.395" type="appl" />
         <tli id="T75" time="89.91" type="appl" />
         <tli id="T76" time="90.425" type="appl" />
         <tli id="T77" time="90.94" type="appl" />
         <tli id="T78" time="91.455" type="appl" />
         <tli id="T79" time="91.97" type="appl" />
         <tli id="T80" time="93.527" type="appl" />
         <tli id="T81" time="94.1" type="appl" />
         <tli id="T82" time="94.674" type="appl" />
         <tli id="T83" time="95.247" type="appl" />
         <tli id="T84" time="95.82" type="appl" />
         <tli id="T85" time="97.174" type="appl" />
         <tli id="T86" time="97.87" type="appl" />
         <tli id="T87" time="98.567" type="appl" />
         <tli id="T88" time="101.074" type="appl" />
         <tli id="T89" time="102.268" type="appl" />
         <tli id="T90" time="104.18997214042375" />
         <tli id="T91" time="106.10324375116669" />
         <tli id="T92" time="106.492" type="appl" />
         <tli id="T93" time="107.716" type="appl" />
         <tli id="T94" time="108.94" type="appl" />
         <tli id="T95" time="110.074" type="appl" />
         <tli id="T96" time="111.059" type="appl" />
         <tli id="T97" time="112.044" type="appl" />
         <tli id="T98" time="113.462" type="appl" />
         <tli id="T159" time="113.88" type="intp" />
         <tli id="T99" time="114.298" type="appl" />
         <tli id="T100" time="115.133" type="appl" />
         <tli id="T101" time="115.969" type="appl" />
         <tli id="T102" time="118.30285018900506" />
         <tli id="T103" time="119.246" type="appl" />
         <tli id="T104" time="120.372" type="appl" />
         <tli id="T105" time="121.499" type="appl" />
         <tli id="T106" time="122.626" type="appl" />
         <tli id="T107" time="123.516" type="appl" />
         <tli id="T108" time="124.167" type="appl" />
         <tli id="T109" time="124.817" type="appl" />
         <tli id="T110" time="125.468" type="appl" />
         <tli id="T111" time="126.119" type="appl" />
         <tli id="T112" time="127.524" type="appl" />
         <tli id="T113" time="128.302" type="appl" />
         <tli id="T114" time="129.081" type="appl" />
         <tli id="T115" time="129.86" type="appl" />
         <tli id="T116" time="130.638" type="appl" />
         <tli id="T117" time="132.40906178598095" />
         <tli id="T118" time="133.208" type="appl" />
         <tli id="T119" time="134.104" type="appl" />
         <tli id="T120" time="136.37560049117977" />
         <tli id="T121" time="137.19" type="appl" />
         <tli id="T122" time="138.024" type="appl" />
         <tli id="T123" time="138.857" type="appl" />
         <tli id="T124" time="139.69" type="appl" />
         <tli id="T125" time="140.524" type="appl" />
         <tli id="T126" time="142.30207596835916" />
         <tli id="T127" time="142.885" type="appl" />
         <tli id="T128" time="143.576" type="appl" />
         <tli id="T129" time="145.00865532014188" />
         <tli id="T130" time="146.161" type="appl" />
         <tli id="T131" time="147.397" type="appl" />
         <tli id="T132" time="148.634" type="appl" />
         <tli id="T133" time="149.871" type="appl" />
         <tli id="T134" time="151.108" type="appl" />
         <tli id="T135" time="152.344" type="appl" />
         <tli id="T136" time="154.64167788991037" />
         <tli id="T137" time="155.701" type="appl" />
         <tli id="T138" time="157.522" type="appl" />
         <tli id="T139" time="158.295" type="appl" />
         <tli id="T140" time="159.068" type="appl" />
         <tli id="T141" time="159.841" type="appl" />
         <tli id="T142" time="160.614" type="appl" />
         <tli id="T143" time="162.57475530030803" />
         <tli id="T144" time="162.933" type="appl" />
         <tli id="T145" time="163.799" type="appl" />
         <tli id="T146" time="165.70132110322942" />
         <tli id="T147" time="166.057" type="appl" />
         <tli id="T150" time="168.334569487353" />
         <tli id="T151" time="169.284" type="appl" />
         <tli id="T152" time="171.76779206412172" />
         <tli id="T153" time="172.67" type="appl" />
         <tli id="T154" time="173.656" type="appl" />
         <tli id="T155" time="174.641" type="appl" />
         <tli id="T156" time="175.626" type="appl" />
         <tli id="T157" time="178.56" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SPK"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T156" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Ugot</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">mat</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">nädak</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Qwa</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">pajam</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">eːxɨ</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_26" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">Helʼǯʼ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">minan</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">kɨbamar</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">nʼeːɣat</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_41" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">Wes</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">šiǯʼauat</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_50" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_52" n="HIAT:w" s="T12">Napas</ts>
                  <nts id="Seg_53" n="HIAT:ip">,</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_56" n="HIAT:w" s="T13">tɨka</ts>
                  <nts id="Seg_57" n="HIAT:ip">,</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_60" n="HIAT:w" s="T14">töwaj</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_64" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_66" n="HIAT:w" s="T15">Uqqər</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_69" n="HIAT:w" s="T16">neü</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_72" n="HIAT:w" s="T17">eːɣən</ts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_76" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_78" n="HIAT:w" s="T18">Naj</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_81" n="HIAT:w" s="T19">šiǯʼaua</ts>
                  <nts id="Seg_82" n="HIAT:ip">.</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_85" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_87" n="HIAT:w" s="T20">Me</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_90" n="HIAT:w" s="T21">qwennaj</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_93" n="HIAT:w" s="T22">šɨtequt</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_96" n="HIAT:w" s="T23">tapeččegu</ts>
                  <nts id="Seg_97" n="HIAT:ip">.</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_100" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_102" n="HIAT:w" s="T24">Mat</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_105" n="HIAT:w" s="T25">pajam</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_108" n="HIAT:w" s="T26">maːttə</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_111" n="HIAT:w" s="T27">qweǯäp</ts>
                  <nts id="Seg_112" n="HIAT:ip">.</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_115" n="HIAT:u" s="T28">
                  <nts id="Seg_116" n="HIAT:ip">(</nts>
                  <ts e="T158" id="Seg_118" n="HIAT:w" s="T28">Ma-</ts>
                  <nts id="Seg_119" n="HIAT:ip">)</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_122" n="HIAT:w" s="T158">Tapeččaj</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_125" n="HIAT:w" s="T29">Sanʼkahe</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_128" n="HIAT:w" s="T30">šɨtequt</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_132" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_134" n="HIAT:w" s="T31">Tabnan</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_137" n="HIAT:w" s="T32">kanaktə</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_140" n="HIAT:w" s="T33">maːttə</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_143" n="HIAT:w" s="T34">kuranna</ts>
                  <nts id="Seg_144" n="HIAT:ip">.</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_147" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_149" n="HIAT:w" s="T35">Tab</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_152" n="HIAT:w" s="T36">mogne</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_155" n="HIAT:w" s="T37">qwenna</ts>
                  <nts id="Seg_156" n="HIAT:ip">.</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_159" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_161" n="HIAT:w" s="T38">Töwa</ts>
                  <nts id="Seg_162" n="HIAT:ip">,</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_165" n="HIAT:w" s="T39">čenča</ts>
                  <nts id="Seg_166" n="HIAT:ip">:</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_169" n="HIAT:w" s="T40">«Tan</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_172" n="HIAT:w" s="T41">alʼǯʼigal</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_175" n="HIAT:w" s="T42">qödemba</ts>
                  <nts id="Seg_176" n="HIAT:ip">,</nts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_179" n="HIAT:w" s="T43">qogolemba</ts>
                  <nts id="Seg_180" n="HIAT:ip">,</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_183" n="HIAT:w" s="T44">hajgelemba»</ts>
                  <nts id="Seg_184" n="HIAT:ip">.</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_187" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_189" n="HIAT:w" s="T45">Mat</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_192" n="HIAT:w" s="T46">čʼwesse</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_195" n="HIAT:w" s="T47">parannak</ts>
                  <nts id="Seg_196" n="HIAT:ip">.</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_199" n="HIAT:u" s="T48">
                  <ts e="T148" id="Seg_201" n="HIAT:w" s="T48">Töwak</ts>
                  <nts id="Seg_202" n="HIAT:ip">,</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_205" n="HIAT:w" s="T148">sabɨlʼ</ts>
                  <nts id="Seg_206" n="HIAT:ip">,</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_209" n="HIAT:w" s="T49">qogolemba</ts>
                  <nts id="Seg_210" n="HIAT:ip">,</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_213" n="HIAT:w" s="T50">hajgelemba</ts>
                  <nts id="Seg_214" n="HIAT:ip">.</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_217" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_219" n="HIAT:w" s="T51">Patom</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_222" n="HIAT:w" s="T52">mat</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_225" n="HIAT:w" s="T53">matqen</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_228" n="HIAT:w" s="T54">wargelak</ts>
                  <nts id="Seg_229" n="HIAT:ip">,</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_232" n="HIAT:w" s="T55">tap</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_234" n="HIAT:ip">(</nts>
                  <ts e="T56" id="Seg_236" n="HIAT:w" s="T149">wutenʼot</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_239" n="HIAT:w" s="T56">telʼǯimbukwap</ts>
                  <nts id="Seg_240" n="HIAT:ip">)</nts>
                  <nts id="Seg_241" n="HIAT:ip">.</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_244" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_246" n="HIAT:w" s="T57">Tawɨt</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_249" n="HIAT:w" s="T58">qwennak</ts>
                  <nts id="Seg_250" n="HIAT:ip">,</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_253" n="HIAT:w" s="T59">tam</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_256" n="HIAT:w" s="T60">wargak</ts>
                  <nts id="Seg_257" n="HIAT:ip">,</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_260" n="HIAT:w" s="T61">qwelʼčak</ts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_264" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_266" n="HIAT:w" s="T62">Patom</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_269" n="HIAT:w" s="T63">qelʼčak</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_272" n="HIAT:w" s="T64">pet</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_274" n="HIAT:ip">—</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_277" n="HIAT:w" s="T65">tapə</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_280" n="HIAT:w" s="T66">čʼaŋgwa</ts>
                  <nts id="Seg_281" n="HIAT:ip">.</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_284" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_286" n="HIAT:w" s="T67">Kare</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_289" n="HIAT:w" s="T68">kuralbak</ts>
                  <nts id="Seg_290" n="HIAT:ip">,</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_293" n="HIAT:w" s="T69">konne</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_296" n="HIAT:w" s="T70">kuralbak</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_298" n="HIAT:ip">—</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_301" n="HIAT:w" s="T71">kajgannaj</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_304" n="HIAT:w" s="T72">čʼaŋgwa</ts>
                  <nts id="Seg_305" n="HIAT:ip">.</nts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_308" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_310" n="HIAT:w" s="T73">Potom</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_313" n="HIAT:w" s="T74">ündeǯäp</ts>
                  <nts id="Seg_314" n="HIAT:ip">:</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_317" n="HIAT:w" s="T75">tam</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_320" n="HIAT:w" s="T76">maǯʼe</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_323" n="HIAT:w" s="T77">puǯoɣen</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_326" n="HIAT:w" s="T78">eːjä</ts>
                  <nts id="Seg_327" n="HIAT:ip">.</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_330" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_332" n="HIAT:w" s="T79">Mat</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_335" n="HIAT:w" s="T80">kwenak</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_338" n="HIAT:w" s="T81">načʼid</ts>
                  <nts id="Seg_339" n="HIAT:ip">,</nts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_342" n="HIAT:w" s="T82">maǯʼe</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_345" n="HIAT:w" s="T83">puǯoɣend</ts>
                  <nts id="Seg_346" n="HIAT:ip">.</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_349" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_351" n="HIAT:w" s="T84">Qandark</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_354" n="HIAT:w" s="T85">qorende</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_357" n="HIAT:w" s="T86">tadap</ts>
                  <nts id="Seg_358" n="HIAT:ip">.</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_361" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_363" n="HIAT:w" s="T87">Taben</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_366" n="HIAT:w" s="T88">tändə</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_369" n="HIAT:w" s="T89">ürǯemba</ts>
                  <nts id="Seg_370" n="HIAT:ip">.</nts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_373" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_375" n="HIAT:w" s="T90">Qajčugu</ts>
                  <nts id="Seg_376" n="HIAT:ip">?</nts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_379" n="HIAT:u" s="T91">
                  <nts id="Seg_380" n="HIAT:ip">(</nts>
                  <ts e="T92" id="Seg_382" n="HIAT:w" s="T91">Pekundə</ts>
                  <nts id="Seg_383" n="HIAT:ip">)</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_386" n="HIAT:w" s="T92">amdaŋ</ts>
                  <nts id="Seg_387" n="HIAT:ip">,</nts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_390" n="HIAT:w" s="T93">ačamnə</ts>
                  <nts id="Seg_391" n="HIAT:ip">.</nts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_394" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_396" n="HIAT:w" s="T94">Potom</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_398" n="HIAT:ip">(</nts>
                  <nts id="Seg_399" n="HIAT:ip">(</nts>
                  <ats e="T96" id="Seg_400" n="HIAT:non-pho" s="T95">…</ats>
                  <nts id="Seg_401" n="HIAT:ip">)</nts>
                  <nts id="Seg_402" n="HIAT:ip">)</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_405" n="HIAT:w" s="T96">qondanna</ts>
                  <nts id="Seg_406" n="HIAT:ip">.</nts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_409" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_411" n="HIAT:w" s="T97">Qelʼča</ts>
                  <nts id="Seg_412" n="HIAT:ip">,</nts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_415" n="HIAT:w" s="T98">potom</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_418" n="HIAT:w" s="T159">man</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_421" n="HIAT:w" s="T99">mogne</ts>
                  <nts id="Seg_422" n="HIAT:ip">,</nts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_425" n="HIAT:w" s="T100">omdle</ts>
                  <nts id="Seg_426" n="HIAT:ip">,</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_429" n="HIAT:w" s="T101">qwennak</ts>
                  <nts id="Seg_430" n="HIAT:ip">.</nts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_433" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_435" n="HIAT:w" s="T102">Kɨgak</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_438" n="HIAT:w" s="T103">tap</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_441" n="HIAT:w" s="T104">üːdegu</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_444" n="HIAT:w" s="T105">samalʼose</ts>
                  <nts id="Seg_445" n="HIAT:ip">.</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_448" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_450" n="HIAT:w" s="T106">Wračen</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_453" n="HIAT:w" s="T107">tenčak</ts>
                  <nts id="Seg_454" n="HIAT:ip">:</nts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_457" n="HIAT:w" s="T108">«Samolʼot</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_460" n="HIAT:w" s="T109">aː</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_463" n="HIAT:w" s="T110">töwa»</ts>
                  <nts id="Seg_464" n="HIAT:ip">.</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_467" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_469" n="HIAT:w" s="T111">Bolʼnicand</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_472" n="HIAT:w" s="T112">qondaj</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_475" n="HIAT:w" s="T113">šɨtequt</ts>
                  <nts id="Seg_476" n="HIAT:ip">,</nts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_479" n="HIAT:w" s="T114">matgalŋ</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_482" n="HIAT:w" s="T115">aː</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_485" n="HIAT:w" s="T116">inǯat</ts>
                  <nts id="Seg_486" n="HIAT:ip">.</nts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_489" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_491" n="HIAT:w" s="T117">Šɨtə</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_494" n="HIAT:w" s="T118">äreɣend</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_497" n="HIAT:w" s="T119">ippaj</ts>
                  <nts id="Seg_498" n="HIAT:ip">.</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_501" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_503" n="HIAT:w" s="T120">Potom</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_506" n="HIAT:w" s="T121">kwačoɣel</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_509" n="HIAT:w" s="T122">wračon</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_512" n="HIAT:w" s="T123">tenčak</ts>
                  <nts id="Seg_513" n="HIAT:ip">:</nts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_516" n="HIAT:w" s="T124">Kuʒat</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_519" n="HIAT:w" s="T125">qwendəle</ts>
                  <nts id="Seg_520" n="HIAT:ip">?</nts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_523" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_525" n="HIAT:w" s="T126">Tabə</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_528" n="HIAT:w" s="T127">čenča</ts>
                  <nts id="Seg_529" n="HIAT:ip">:</nts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_532" n="HIAT:w" s="T128">«Patom»</ts>
                  <nts id="Seg_533" n="HIAT:ip">.</nts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_536" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_538" n="HIAT:w" s="T129">Patom</ts>
                  <nts id="Seg_539" n="HIAT:ip">,</nts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_542" n="HIAT:w" s="T130">nadi</ts>
                  <nts id="Seg_543" n="HIAT:ip">,</nts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_546" n="HIAT:w" s="T131">töwa</ts>
                  <nts id="Seg_547" n="HIAT:ip">,</nts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_550" n="HIAT:w" s="T132">samolʼose</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_553" n="HIAT:w" s="T133">qwendat</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_556" n="HIAT:w" s="T134">tap</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_559" n="HIAT:w" s="T135">kwačond</ts>
                  <nts id="Seg_560" n="HIAT:ip">.</nts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_563" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_565" n="HIAT:w" s="T136">Kwačoɣet</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_568" n="HIAT:w" s="T137">lečimbat</ts>
                  <nts id="Seg_569" n="HIAT:ip">.</nts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_572" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_574" n="HIAT:w" s="T138">Patom</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_577" n="HIAT:w" s="T139">karenan</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_580" n="HIAT:w" s="T140">maːt</ts>
                  <nts id="Seg_581" n="HIAT:ip">,</nts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_584" n="HIAT:w" s="T141">nawerno</ts>
                  <nts id="Seg_585" n="HIAT:ip">,</nts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_588" n="HIAT:w" s="T142">qwenembat</ts>
                  <nts id="Seg_589" n="HIAT:ip">.</nts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_592" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_594" n="HIAT:w" s="T143">Mat</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_597" n="HIAT:w" s="T144">pelgal</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_600" n="HIAT:w" s="T145">wargak</ts>
                  <nts id="Seg_601" n="HIAT:ip">.</nts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_604" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_606" n="HIAT:w" s="T146">Pelgal</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_608" n="HIAT:ip">(</nts>
                  <nts id="Seg_609" n="HIAT:ip">(</nts>
                  <ats e="T150" id="Seg_610" n="HIAT:non-pho" s="T147">warqu</ats>
                  <nts id="Seg_611" n="HIAT:ip">)</nts>
                  <nts id="Seg_612" n="HIAT:ip">)</nts>
                  <nts id="Seg_613" n="HIAT:ip">…</nts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_616" n="HIAT:u" s="T150">
                  <ts e="T151" id="Seg_618" n="HIAT:w" s="T150">Patom</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_621" n="HIAT:w" s="T151">nädak</ts>
                  <nts id="Seg_622" n="HIAT:ip">.</nts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_625" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_627" n="HIAT:w" s="T152">Sejčas</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_630" n="HIAT:w" s="T153">tar</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_633" n="HIAT:w" s="T154">naj</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_636" n="HIAT:w" s="T155">wargešpak</ts>
                  <nts id="Seg_637" n="HIAT:ip">.</nts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T156" id="Seg_639" n="sc" s="T0">
               <ts e="T1" id="Seg_641" n="e" s="T0">Ugot </ts>
               <ts e="T2" id="Seg_643" n="e" s="T1">mat </ts>
               <ts e="T3" id="Seg_645" n="e" s="T2">nädak. </ts>
               <ts e="T4" id="Seg_647" n="e" s="T3">Qwa </ts>
               <ts e="T5" id="Seg_649" n="e" s="T4">pajam </ts>
               <ts e="T6" id="Seg_651" n="e" s="T5">eːxɨ. </ts>
               <ts e="T7" id="Seg_653" n="e" s="T6">Helʼǯʼ </ts>
               <ts e="T8" id="Seg_655" n="e" s="T7">minan </ts>
               <ts e="T9" id="Seg_657" n="e" s="T8">kɨbamar </ts>
               <ts e="T10" id="Seg_659" n="e" s="T9">nʼeːɣat. </ts>
               <ts e="T11" id="Seg_661" n="e" s="T10">Wes </ts>
               <ts e="T12" id="Seg_663" n="e" s="T11">šiǯʼauat. </ts>
               <ts e="T13" id="Seg_665" n="e" s="T12">Napas, </ts>
               <ts e="T14" id="Seg_667" n="e" s="T13">tɨka, </ts>
               <ts e="T15" id="Seg_669" n="e" s="T14">töwaj. </ts>
               <ts e="T16" id="Seg_671" n="e" s="T15">Uqqər </ts>
               <ts e="T17" id="Seg_673" n="e" s="T16">neü </ts>
               <ts e="T18" id="Seg_675" n="e" s="T17">eːɣən. </ts>
               <ts e="T19" id="Seg_677" n="e" s="T18">Naj </ts>
               <ts e="T20" id="Seg_679" n="e" s="T19">šiǯʼaua. </ts>
               <ts e="T21" id="Seg_681" n="e" s="T20">Me </ts>
               <ts e="T22" id="Seg_683" n="e" s="T21">qwennaj </ts>
               <ts e="T23" id="Seg_685" n="e" s="T22">šɨtequt </ts>
               <ts e="T24" id="Seg_687" n="e" s="T23">tapeččegu. </ts>
               <ts e="T25" id="Seg_689" n="e" s="T24">Mat </ts>
               <ts e="T26" id="Seg_691" n="e" s="T25">pajam </ts>
               <ts e="T27" id="Seg_693" n="e" s="T26">maːttə </ts>
               <ts e="T28" id="Seg_695" n="e" s="T27">qweǯäp. </ts>
               <ts e="T158" id="Seg_697" n="e" s="T28">(Ma-) </ts>
               <ts e="T29" id="Seg_699" n="e" s="T158">Tapeččaj </ts>
               <ts e="T30" id="Seg_701" n="e" s="T29">Sanʼkahe </ts>
               <ts e="T31" id="Seg_703" n="e" s="T30">šɨtequt. </ts>
               <ts e="T32" id="Seg_705" n="e" s="T31">Tabnan </ts>
               <ts e="T33" id="Seg_707" n="e" s="T32">kanaktə </ts>
               <ts e="T34" id="Seg_709" n="e" s="T33">maːttə </ts>
               <ts e="T35" id="Seg_711" n="e" s="T34">kuranna. </ts>
               <ts e="T36" id="Seg_713" n="e" s="T35">Tab </ts>
               <ts e="T37" id="Seg_715" n="e" s="T36">mogne </ts>
               <ts e="T38" id="Seg_717" n="e" s="T37">qwenna. </ts>
               <ts e="T39" id="Seg_719" n="e" s="T38">Töwa, </ts>
               <ts e="T40" id="Seg_721" n="e" s="T39">čenča: </ts>
               <ts e="T41" id="Seg_723" n="e" s="T40">«Tan </ts>
               <ts e="T42" id="Seg_725" n="e" s="T41">alʼǯʼigal </ts>
               <ts e="T43" id="Seg_727" n="e" s="T42">qödemba, </ts>
               <ts e="T44" id="Seg_729" n="e" s="T43">qogolemba, </ts>
               <ts e="T45" id="Seg_731" n="e" s="T44">hajgelemba». </ts>
               <ts e="T46" id="Seg_733" n="e" s="T45">Mat </ts>
               <ts e="T47" id="Seg_735" n="e" s="T46">čʼwesse </ts>
               <ts e="T48" id="Seg_737" n="e" s="T47">parannak. </ts>
               <ts e="T148" id="Seg_739" n="e" s="T48">Töwak, </ts>
               <ts e="T49" id="Seg_741" n="e" s="T148">sabɨlʼ, </ts>
               <ts e="T50" id="Seg_743" n="e" s="T49">qogolemba, </ts>
               <ts e="T51" id="Seg_745" n="e" s="T50">hajgelemba. </ts>
               <ts e="T52" id="Seg_747" n="e" s="T51">Patom </ts>
               <ts e="T53" id="Seg_749" n="e" s="T52">mat </ts>
               <ts e="T54" id="Seg_751" n="e" s="T53">matqen </ts>
               <ts e="T55" id="Seg_753" n="e" s="T54">wargelak, </ts>
               <ts e="T149" id="Seg_755" n="e" s="T55">tap </ts>
               <ts e="T56" id="Seg_757" n="e" s="T149">(wutenʼot </ts>
               <ts e="T57" id="Seg_759" n="e" s="T56">telʼǯimbukwap). </ts>
               <ts e="T58" id="Seg_761" n="e" s="T57">Tawɨt </ts>
               <ts e="T59" id="Seg_763" n="e" s="T58">qwennak, </ts>
               <ts e="T60" id="Seg_765" n="e" s="T59">tam </ts>
               <ts e="T61" id="Seg_767" n="e" s="T60">wargak, </ts>
               <ts e="T62" id="Seg_769" n="e" s="T61">qwelʼčak. </ts>
               <ts e="T63" id="Seg_771" n="e" s="T62">Patom </ts>
               <ts e="T64" id="Seg_773" n="e" s="T63">qelʼčak </ts>
               <ts e="T65" id="Seg_775" n="e" s="T64">pet — </ts>
               <ts e="T66" id="Seg_777" n="e" s="T65">tapə </ts>
               <ts e="T67" id="Seg_779" n="e" s="T66">čʼaŋgwa. </ts>
               <ts e="T68" id="Seg_781" n="e" s="T67">Kare </ts>
               <ts e="T69" id="Seg_783" n="e" s="T68">kuralbak, </ts>
               <ts e="T70" id="Seg_785" n="e" s="T69">konne </ts>
               <ts e="T71" id="Seg_787" n="e" s="T70">kuralbak — </ts>
               <ts e="T72" id="Seg_789" n="e" s="T71">kajgannaj </ts>
               <ts e="T73" id="Seg_791" n="e" s="T72">čʼaŋgwa. </ts>
               <ts e="T74" id="Seg_793" n="e" s="T73">Potom </ts>
               <ts e="T75" id="Seg_795" n="e" s="T74">ündeǯäp: </ts>
               <ts e="T76" id="Seg_797" n="e" s="T75">tam </ts>
               <ts e="T77" id="Seg_799" n="e" s="T76">maǯʼe </ts>
               <ts e="T78" id="Seg_801" n="e" s="T77">puǯoɣen </ts>
               <ts e="T79" id="Seg_803" n="e" s="T78">eːjä. </ts>
               <ts e="T80" id="Seg_805" n="e" s="T79">Mat </ts>
               <ts e="T81" id="Seg_807" n="e" s="T80">kwenak </ts>
               <ts e="T82" id="Seg_809" n="e" s="T81">načʼid, </ts>
               <ts e="T83" id="Seg_811" n="e" s="T82">maǯʼe </ts>
               <ts e="T84" id="Seg_813" n="e" s="T83">puǯoɣend. </ts>
               <ts e="T85" id="Seg_815" n="e" s="T84">Qandark </ts>
               <ts e="T86" id="Seg_817" n="e" s="T85">qorende </ts>
               <ts e="T87" id="Seg_819" n="e" s="T86">tadap. </ts>
               <ts e="T88" id="Seg_821" n="e" s="T87">Taben </ts>
               <ts e="T89" id="Seg_823" n="e" s="T88">tändə </ts>
               <ts e="T90" id="Seg_825" n="e" s="T89">ürǯemba. </ts>
               <ts e="T91" id="Seg_827" n="e" s="T90">Qajčugu? </ts>
               <ts e="T92" id="Seg_829" n="e" s="T91">(Pekundə) </ts>
               <ts e="T93" id="Seg_831" n="e" s="T92">amdaŋ, </ts>
               <ts e="T94" id="Seg_833" n="e" s="T93">ačamnə. </ts>
               <ts e="T95" id="Seg_835" n="e" s="T94">Potom </ts>
               <ts e="T96" id="Seg_837" n="e" s="T95">((…)) </ts>
               <ts e="T97" id="Seg_839" n="e" s="T96">qondanna. </ts>
               <ts e="T98" id="Seg_841" n="e" s="T97">Qelʼča, </ts>
               <ts e="T159" id="Seg_843" n="e" s="T98">potom </ts>
               <ts e="T99" id="Seg_845" n="e" s="T159">man </ts>
               <ts e="T100" id="Seg_847" n="e" s="T99">mogne, </ts>
               <ts e="T101" id="Seg_849" n="e" s="T100">omdle, </ts>
               <ts e="T102" id="Seg_851" n="e" s="T101">qwennak. </ts>
               <ts e="T103" id="Seg_853" n="e" s="T102">Kɨgak </ts>
               <ts e="T104" id="Seg_855" n="e" s="T103">tap </ts>
               <ts e="T105" id="Seg_857" n="e" s="T104">üːdegu </ts>
               <ts e="T106" id="Seg_859" n="e" s="T105">samalʼose. </ts>
               <ts e="T107" id="Seg_861" n="e" s="T106">Wračen </ts>
               <ts e="T108" id="Seg_863" n="e" s="T107">tenčak: </ts>
               <ts e="T109" id="Seg_865" n="e" s="T108">«Samolʼot </ts>
               <ts e="T110" id="Seg_867" n="e" s="T109">aː </ts>
               <ts e="T111" id="Seg_869" n="e" s="T110">töwa». </ts>
               <ts e="T112" id="Seg_871" n="e" s="T111">Bolʼnicand </ts>
               <ts e="T113" id="Seg_873" n="e" s="T112">qondaj </ts>
               <ts e="T114" id="Seg_875" n="e" s="T113">šɨtequt, </ts>
               <ts e="T115" id="Seg_877" n="e" s="T114">matgalŋ </ts>
               <ts e="T116" id="Seg_879" n="e" s="T115">aː </ts>
               <ts e="T117" id="Seg_881" n="e" s="T116">inǯat. </ts>
               <ts e="T118" id="Seg_883" n="e" s="T117">Šɨtə </ts>
               <ts e="T119" id="Seg_885" n="e" s="T118">äreɣend </ts>
               <ts e="T120" id="Seg_887" n="e" s="T119">ippaj. </ts>
               <ts e="T121" id="Seg_889" n="e" s="T120">Potom </ts>
               <ts e="T122" id="Seg_891" n="e" s="T121">kwačoɣel </ts>
               <ts e="T123" id="Seg_893" n="e" s="T122">wračon </ts>
               <ts e="T124" id="Seg_895" n="e" s="T123">tenčak: </ts>
               <ts e="T125" id="Seg_897" n="e" s="T124">Kuʒat </ts>
               <ts e="T126" id="Seg_899" n="e" s="T125">qwendəle? </ts>
               <ts e="T127" id="Seg_901" n="e" s="T126">Tabə </ts>
               <ts e="T128" id="Seg_903" n="e" s="T127">čenča: </ts>
               <ts e="T129" id="Seg_905" n="e" s="T128">«Patom». </ts>
               <ts e="T130" id="Seg_907" n="e" s="T129">Patom, </ts>
               <ts e="T131" id="Seg_909" n="e" s="T130">nadi, </ts>
               <ts e="T132" id="Seg_911" n="e" s="T131">töwa, </ts>
               <ts e="T133" id="Seg_913" n="e" s="T132">samolʼose </ts>
               <ts e="T134" id="Seg_915" n="e" s="T133">qwendat </ts>
               <ts e="T135" id="Seg_917" n="e" s="T134">tap </ts>
               <ts e="T136" id="Seg_919" n="e" s="T135">kwačond. </ts>
               <ts e="T137" id="Seg_921" n="e" s="T136">Kwačoɣet </ts>
               <ts e="T138" id="Seg_923" n="e" s="T137">lečimbat. </ts>
               <ts e="T139" id="Seg_925" n="e" s="T138">Patom </ts>
               <ts e="T140" id="Seg_927" n="e" s="T139">karenan </ts>
               <ts e="T141" id="Seg_929" n="e" s="T140">maːt, </ts>
               <ts e="T142" id="Seg_931" n="e" s="T141">nawerno, </ts>
               <ts e="T143" id="Seg_933" n="e" s="T142">qwenembat. </ts>
               <ts e="T144" id="Seg_935" n="e" s="T143">Mat </ts>
               <ts e="T145" id="Seg_937" n="e" s="T144">pelgal </ts>
               <ts e="T146" id="Seg_939" n="e" s="T145">wargak. </ts>
               <ts e="T147" id="Seg_941" n="e" s="T146">Pelgal </ts>
               <ts e="T150" id="Seg_943" n="e" s="T147">((warqu))… </ts>
               <ts e="T151" id="Seg_945" n="e" s="T150">Patom </ts>
               <ts e="T152" id="Seg_947" n="e" s="T151">nädak. </ts>
               <ts e="T153" id="Seg_949" n="e" s="T152">Sejčas </ts>
               <ts e="T154" id="Seg_951" n="e" s="T153">tar </ts>
               <ts e="T155" id="Seg_953" n="e" s="T154">naj </ts>
               <ts e="T156" id="Seg_955" n="e" s="T155">wargešpak. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_956" s="T0">KFN_1967_Lifestory_nar.001 (001)</ta>
            <ta e="T6" id="Seg_957" s="T3">KFN_1967_Lifestory_nar.002 (002)</ta>
            <ta e="T10" id="Seg_958" s="T6">KFN_1967_Lifestory_nar.003 (003)</ta>
            <ta e="T12" id="Seg_959" s="T10">KFN_1967_Lifestory_nar.004 (004)</ta>
            <ta e="T15" id="Seg_960" s="T12">KFN_1967_Lifestory_nar.005 (005)</ta>
            <ta e="T18" id="Seg_961" s="T15">KFN_1967_Lifestory_nar.006 (006)</ta>
            <ta e="T20" id="Seg_962" s="T18">KFN_1967_Lifestory_nar.007 (007)</ta>
            <ta e="T24" id="Seg_963" s="T20">KFN_1967_Lifestory_nar.008 (008)</ta>
            <ta e="T28" id="Seg_964" s="T24">KFN_1967_Lifestory_nar.009 (009)</ta>
            <ta e="T31" id="Seg_965" s="T28">KFN_1967_Lifestory_nar.010 (010)</ta>
            <ta e="T35" id="Seg_966" s="T31">KFN_1967_Lifestory_nar.011 (011)</ta>
            <ta e="T38" id="Seg_967" s="T35">KFN_1967_Lifestory_nar.012 (012)</ta>
            <ta e="T45" id="Seg_968" s="T38">KFN_1967_Lifestory_nar.013 (013)</ta>
            <ta e="T48" id="Seg_969" s="T45">KFN_1967_Lifestory_nar.014 (014)</ta>
            <ta e="T51" id="Seg_970" s="T48">KFN_1967_Lifestory_nar.015 (015)</ta>
            <ta e="T57" id="Seg_971" s="T51">KFN_1967_Lifestory_nar.016 (016)</ta>
            <ta e="T62" id="Seg_972" s="T57">KFN_1967_Lifestory_nar.017 (017)</ta>
            <ta e="T67" id="Seg_973" s="T62">KFN_1967_Lifestory_nar.018 (018)</ta>
            <ta e="T73" id="Seg_974" s="T67">KFN_1967_Lifestory_nar.019 (019)</ta>
            <ta e="T79" id="Seg_975" s="T73">KFN_1967_Lifestory_nar.020 (020)</ta>
            <ta e="T84" id="Seg_976" s="T79">KFN_1967_Lifestory_nar.021 (021)</ta>
            <ta e="T87" id="Seg_977" s="T84">KFN_1967_Lifestory_nar.022 (022)</ta>
            <ta e="T90" id="Seg_978" s="T87">KFN_1967_Lifestory_nar.023 (023)</ta>
            <ta e="T91" id="Seg_979" s="T90">KFN_1967_Lifestory_nar.024 (024)</ta>
            <ta e="T94" id="Seg_980" s="T91">KFN_1967_Lifestory_nar.025 (024)</ta>
            <ta e="T97" id="Seg_981" s="T94">KFN_1967_Lifestory_nar.026 (025)</ta>
            <ta e="T102" id="Seg_982" s="T97">KFN_1967_Lifestory_nar.027 (026)</ta>
            <ta e="T106" id="Seg_983" s="T102">KFN_1967_Lifestory_nar.028 (027)</ta>
            <ta e="T111" id="Seg_984" s="T106">KFN_1967_Lifestory_nar.029 (028)</ta>
            <ta e="T117" id="Seg_985" s="T111">KFN_1967_Lifestory_nar.030 (029)</ta>
            <ta e="T120" id="Seg_986" s="T117">KFN_1967_Lifestory_nar.031 (030)</ta>
            <ta e="T126" id="Seg_987" s="T120">KFN_1967_Lifestory_nar.032 (031)</ta>
            <ta e="T129" id="Seg_988" s="T126">KFN_1967_Lifestory_nar.033 (032)</ta>
            <ta e="T136" id="Seg_989" s="T129">KFN_1967_Lifestory_nar.034 (033)</ta>
            <ta e="T138" id="Seg_990" s="T136">KFN_1967_Lifestory_nar.035 (034)</ta>
            <ta e="T143" id="Seg_991" s="T138">KFN_1967_Lifestory_nar.036 (035)</ta>
            <ta e="T146" id="Seg_992" s="T143">KFN_1967_Lifestory_nar.037 (036)</ta>
            <ta e="T150" id="Seg_993" s="T146">KFN_1967_Lifestory_nar.038 (037)</ta>
            <ta e="T152" id="Seg_994" s="T150">KFN_1967_Lifestory_nar.039 (038)</ta>
            <ta e="T156" id="Seg_995" s="T152">KFN_1967_Lifestory_nar.040 (039)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_996" s="T0">Уго́т мат нӓда́к.</ta>
            <ta e="T6" id="Seg_997" s="T3">Манна́н пая́м э̄ҳы.</ta>
            <ta e="T10" id="Seg_998" s="T6">Хельджь мiнан кыбама́р э̄ҳа.</ta>
            <ta e="T12" id="Seg_999" s="T10">Вес шиджӓумбат.</ta>
            <ta e="T15" id="Seg_1000" s="T12">Напа́с, тыка́, тӧай.</ta>
            <ta e="T18" id="Seg_1001" s="T15">О́ӄӄэр нэ э̄ҳа.</ta>
            <ta e="T20" id="Seg_1002" s="T18">Най шиджӓумба.</ta>
            <ta e="T24" id="Seg_1003" s="T20">Мы, квэ́най шэдэӷу́т табе́ҷэгу.</ta>
            <ta e="T28" id="Seg_1004" s="T24">Мат пайӓм ма̄т квэ́джӓм.</ta>
            <ta e="T31" id="Seg_1005" s="T28">Табе́ҷай Са́нькаҳе шэдэӷу́т.</ta>
            <ta e="T35" id="Seg_1006" s="T31">Табна́н кана́кт ма̄т кура́нна.</ta>
            <ta e="T38" id="Seg_1007" s="T35">Таб могнэ́ квэ́нба.</ta>
            <ta e="T45" id="Seg_1008" s="T38">Тӧа, ҷэ́нҷа: «Тан альджига́л кӧдэмба, о́голэмба, ха́йгэлэмба».</ta>
            <ta e="T48" id="Seg_1009" s="T45">Мат чвэ́ссе па́раннак.</ta>
            <ta e="T51" id="Seg_1010" s="T48">Са́быль, о́голэмба, ха́йгэлэмба.</ta>
            <ta e="T57" id="Seg_1011" s="T51">Потом мат ма́тӄэн варге́лак, ӱтэп ӱтэ́льджикумбак.</ta>
            <ta e="T62" id="Seg_1012" s="T57">Та́эт квэ́нак, там варга́к, квэ́льҷак.</ta>
            <ta e="T67" id="Seg_1013" s="T62">Потом кэ́льчак пет — таб ча́ңгва.</ta>
            <ta e="T73" id="Seg_1014" s="T67">Каре́ кура́лбак, коннэ́ кура́лбак — ка́йганнай ча́ңгва.</ta>
            <ta e="T79" id="Seg_1015" s="T73">Потом ӱндэджӓм: таб маджь пу́джоӷэн э̄йӓ.</ta>
            <ta e="T84" id="Seg_1016" s="T79">Мат квэ́нак начи́д, маджь пу́джоӷэнд.</ta>
            <ta e="T87" id="Seg_1017" s="T84">Ка́ндарк коре́нд тада́м.</ta>
            <ta e="T90" id="Seg_1018" s="T87">Та́бэн тӓнд ӱрджэмба́.</ta>
            <ta e="T94" id="Seg_1019" s="T91">Альджига́ кунд а́мдаң, а́ҷыт.</ta>
            <ta e="T97" id="Seg_1020" s="T94">Пото́м нынд конда́нна.</ta>
            <ta e="T102" id="Seg_1021" s="T97">Кэ́льча, пото́м могнэ́, омдле́, квэнаӷ.</ta>
            <ta e="T106" id="Seg_1022" s="T102">Кыга́к та́бэп ӱ̄дэгу самолёссэ.</ta>
            <ta e="T111" id="Seg_1023" s="T106">Вра́чен ҷэ́нҷак: «Самолёт а̄ тӧа».</ta>
            <ta e="T117" id="Seg_1024" s="T111">Больни́цанд квэ́най шэдэгу́т, матга́лк а̄ и́нджат.</ta>
            <ta e="T120" id="Seg_1025" s="T117">Шэд аре́ӷэнд иппа́й.</ta>
            <ta e="T126" id="Seg_1026" s="T120">Потом кваҷо́ӷэл вра́чен ҷэ́нҷак: «Кужа́ӄэт квэ́ндле?»</ta>
            <ta e="T129" id="Seg_1027" s="T126">Таб ҷэ́нҷа: «Потом».</ta>
            <ta e="T136" id="Seg_1028" s="T129">Потом, надi, тӧа, самолёссе квэ́ндат та́бэп кваҷо́нд.</ta>
            <ta e="T138" id="Seg_1029" s="T136">Кваҷо́ӷэт лечи́мбат.</ta>
            <ta e="T143" id="Seg_1030" s="T138">Потом каре́нан ма̄т, наверно, квэ́нэмбат.</ta>
            <ta e="T146" id="Seg_1031" s="T143">Мат пялга́лк варга́к.</ta>
            <ta e="T150" id="Seg_1032" s="T146">Пялга́лк варгэгу́ кошка́лк э̄йӓ.</ta>
            <ta e="T152" id="Seg_1033" s="T150">Потом нӓда́к.</ta>
            <ta e="T156" id="Seg_1034" s="T152">Сейчас тар най варге́шпак.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_1035" s="T0">Ugot mat nädak.</ta>
            <ta e="T6" id="Seg_1036" s="T3">Mannan pajam eːxɨ.</ta>
            <ta e="T10" id="Seg_1037" s="T6">Helʼǯʼ minan kɨbamar eːxa.</ta>
            <ta e="T12" id="Seg_1038" s="T10">Ves šiǯäumbat.</ta>
            <ta e="T15" id="Seg_1039" s="T12">Napas, tɨka, töaj.</ta>
            <ta e="T18" id="Seg_1040" s="T15">Oqqer ne eːxa.</ta>
            <ta e="T20" id="Seg_1041" s="T18">Naj šiǯäumba.</ta>
            <ta e="T24" id="Seg_1042" s="T20">Mɨ, kvenaj šedeɣut tabečegu.</ta>
            <ta e="T28" id="Seg_1043" s="T24">Mat pajäm maːt kveǯäm.</ta>
            <ta e="T31" id="Seg_1044" s="T28">Tabečaj Sanʼkaxe šedeɣut.</ta>
            <ta e="T35" id="Seg_1045" s="T31">Tabnan kanakt maːt kuranna.</ta>
            <ta e="T38" id="Seg_1046" s="T35">Tab mogne kvenba.</ta>
            <ta e="T45" id="Seg_1047" s="T38">Töa, čenča: «Tan alʼǯigal ködemba, ogolemba, xajgelemba».</ta>
            <ta e="T48" id="Seg_1048" s="T45">Mat čʼvesse parannak.</ta>
            <ta e="T51" id="Seg_1049" s="T48">Sabɨlʼ, ogolemba, xajgelemba.</ta>
            <ta e="T57" id="Seg_1050" s="T51">Potom mat matqen vargelak, ütep ütelʼǯikumbak.</ta>
            <ta e="T62" id="Seg_1051" s="T57">Taet kvenak, tam vargak, kvelʼčʼak.</ta>
            <ta e="T67" id="Seg_1052" s="T62">Potom kelʼčak pet — tab čʼaŋgva.</ta>
            <ta e="T73" id="Seg_1053" s="T67">Kare kuralbak, konne kuralbak — kajgannaj čʼaŋgva.</ta>
            <ta e="T79" id="Seg_1054" s="T73">Potom ündeǯäm: tab maǯʼ puǯoɣen eːjä.</ta>
            <ta e="T84" id="Seg_1055" s="T79">Mat kvenak načʼid, maǯʼ puǯoɣend.</ta>
            <ta e="T87" id="Seg_1056" s="T84">Kandark korend tadam.</ta>
            <ta e="T90" id="Seg_1057" s="T87">Taben tänd ürǯemba.</ta>
            <ta e="T94" id="Seg_1058" s="T91">Alʼǯiga kund amdaŋ, ačɨt.</ta>
            <ta e="T97" id="Seg_1059" s="T94">Potom nɨnd kondanna.</ta>
            <ta e="T102" id="Seg_1060" s="T97">Kelʼča, potom mogne, omdle, kvenaɣ.</ta>
            <ta e="T106" id="Seg_1061" s="T102">Kɨgak tabep üːdegu samolёsse.</ta>
            <ta e="T111" id="Seg_1062" s="T106">Vračʼen čenčak: «Samolёt aː töa».</ta>
            <ta e="T117" id="Seg_1063" s="T111">Bolʼnicand kvenaj šedegut, matgalk aː inǯat.</ta>
            <ta e="T120" id="Seg_1064" s="T117">Šed areɣend ippaj.</ta>
            <ta e="T126" id="Seg_1065" s="T120">Potom kvačoɣel vračen čenčak: «Kuʒaqet kvendle?»</ta>
            <ta e="T129" id="Seg_1066" s="T126">Tab čenča: «Potom».</ta>
            <ta e="T136" id="Seg_1067" s="T129">Potom, nadi, töa, samolёsse kvendat tabep kvačond.</ta>
            <ta e="T138" id="Seg_1068" s="T136">Kvačoɣet lečimbat.</ta>
            <ta e="T143" id="Seg_1069" s="T138">Potom karenan maːt, naverno, kvenembat.</ta>
            <ta e="T146" id="Seg_1070" s="T143">Mat pʼalgalk vargak.</ta>
            <ta e="T150" id="Seg_1071" s="T146">Pʼalgalk vargegu koškalk eːjä.</ta>
            <ta e="T152" id="Seg_1072" s="T150">Potom nädak.</ta>
            <ta e="T156" id="Seg_1073" s="T152">Sejčas tar naj vargešpak.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_1074" s="T0">Ugot mat nädak. </ta>
            <ta e="T6" id="Seg_1075" s="T3">Qwa pajam eːxɨ. </ta>
            <ta e="T10" id="Seg_1076" s="T6">Helʼǯʼ minan kɨbamar nʼeːɣat. </ta>
            <ta e="T12" id="Seg_1077" s="T10">Wes šiǯʼauat. </ta>
            <ta e="T15" id="Seg_1078" s="T12">Napas, tɨka, töwaj. </ta>
            <ta e="T18" id="Seg_1079" s="T15">Uqqər neü eːɣən. </ta>
            <ta e="T20" id="Seg_1080" s="T18">Naj šiǯʼaua. </ta>
            <ta e="T24" id="Seg_1081" s="T20">Me qwennaj šɨtequt tapeččegu. </ta>
            <ta e="T28" id="Seg_1082" s="T24">Mat pajam maːttə qweǯäp. </ta>
            <ta e="T31" id="Seg_1083" s="T28">(Ma-) Tapeččaj Sanʼkahe šɨtequt. </ta>
            <ta e="T35" id="Seg_1084" s="T31">Tabnan kanaktə maːttə kuranna. </ta>
            <ta e="T38" id="Seg_1085" s="T35">Tab mogne qwenna. </ta>
            <ta e="T45" id="Seg_1086" s="T38">Töwa, čenča: «Tan alʼǯʼigal qödemba, qogolemba, hajgelemba». </ta>
            <ta e="T48" id="Seg_1087" s="T45">Mat čʼwesse parannak. </ta>
            <ta e="T51" id="Seg_1088" s="T48">Töwak, sabɨlʼ, qogolemba, hajgelemba. </ta>
            <ta e="T57" id="Seg_1089" s="T51">Patom mat matqen wargelak, tap (wutenʼot telʼǯimbukwap). </ta>
            <ta e="T62" id="Seg_1090" s="T57">Tawɨt qwennak, tam wargak, qwelʼčak. </ta>
            <ta e="T67" id="Seg_1091" s="T62">Patom qelʼčak pet — tapə čʼaŋgwa. </ta>
            <ta e="T73" id="Seg_1092" s="T67">Kare kuralbak, konne kuralbak — kajgannaj čʼaŋgwa. </ta>
            <ta e="T79" id="Seg_1093" s="T73">Potom ündeǯäp: tam maǯʼe puǯoɣen eːjä. </ta>
            <ta e="T84" id="Seg_1094" s="T79">Mat kwenak načʼid, maǯʼe puǯoɣend. </ta>
            <ta e="T87" id="Seg_1095" s="T84">Qandark qorende tadap. </ta>
            <ta e="T90" id="Seg_1096" s="T87">Taben tändə ürǯemba. </ta>
            <ta e="T91" id="Seg_1097" s="T90">Qajčugu?</ta>
            <ta e="T94" id="Seg_1098" s="T91">(Pekundə) amdaŋ, ačamnə. </ta>
            <ta e="T97" id="Seg_1099" s="T94">Potom ((…)) qondanna. </ta>
            <ta e="T102" id="Seg_1100" s="T97">Qelʼča, potom man mogne, omdle, qwennak. </ta>
            <ta e="T106" id="Seg_1101" s="T102">Kɨgak tap üːdegu samalʼose. </ta>
            <ta e="T111" id="Seg_1102" s="T106">Wračen tenčak: «Samolʼot aː töwa». </ta>
            <ta e="T117" id="Seg_1103" s="T111">Bolʼnicand qondaj šɨtequt, matgalŋ aː inǯat. </ta>
            <ta e="T120" id="Seg_1104" s="T117">Šɨtə äreɣend ippaj.</ta>
            <ta e="T126" id="Seg_1105" s="T120">Potom kwačoɣel wračon tenčak: «Kuʒat qwendəle?» </ta>
            <ta e="T129" id="Seg_1106" s="T126">Tabə čenča: «Patom». </ta>
            <ta e="T136" id="Seg_1107" s="T129">Patom, nadi, töwa, samolʼose qwendat tap kwačond. </ta>
            <ta e="T138" id="Seg_1108" s="T136">Kwačoɣet lečimbat. </ta>
            <ta e="T143" id="Seg_1109" s="T138">Patom karenan maːt, nawerno, qwenembat. </ta>
            <ta e="T146" id="Seg_1110" s="T143">Mat pelgal wargak. </ta>
            <ta e="T150" id="Seg_1111" s="T146">Pelgal ((warqu…)). </ta>
            <ta e="T152" id="Seg_1112" s="T150">Patom nädak. </ta>
            <ta e="T156" id="Seg_1113" s="T152">Sejčas tar naj wargešpak. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1114" s="T0">ugot</ta>
            <ta e="T2" id="Seg_1115" s="T1">mat</ta>
            <ta e="T3" id="Seg_1116" s="T2">näda-k</ta>
            <ta e="T4" id="Seg_1117" s="T3">qwa</ta>
            <ta e="T5" id="Seg_1118" s="T4">paja-m</ta>
            <ta e="T6" id="Seg_1119" s="T5">eː-xɨ</ta>
            <ta e="T7" id="Seg_1120" s="T6">helʼǯʼ</ta>
            <ta e="T8" id="Seg_1121" s="T7">mi-nan</ta>
            <ta e="T9" id="Seg_1122" s="T8">kɨbamar</ta>
            <ta e="T10" id="Seg_1123" s="T9">nʼeːɣa-t</ta>
            <ta e="T11" id="Seg_1124" s="T10">wes</ta>
            <ta e="T12" id="Seg_1125" s="T11">šiǯʼau-a-t</ta>
            <ta e="T13" id="Seg_1126" s="T12">Napas</ta>
            <ta e="T14" id="Seg_1127" s="T13">tɨka</ta>
            <ta e="T15" id="Seg_1128" s="T14">tö-wa-j</ta>
            <ta e="T16" id="Seg_1129" s="T15">uqqər</ta>
            <ta e="T17" id="Seg_1130" s="T16">ne-ü</ta>
            <ta e="T18" id="Seg_1131" s="T17">eː-ɣə-n</ta>
            <ta e="T19" id="Seg_1132" s="T18">naj</ta>
            <ta e="T20" id="Seg_1133" s="T19">šiǯʼau-a</ta>
            <ta e="T21" id="Seg_1134" s="T20">me</ta>
            <ta e="T22" id="Seg_1135" s="T21">qwen-na-j</ta>
            <ta e="T23" id="Seg_1136" s="T22">šɨte-qut</ta>
            <ta e="T24" id="Seg_1137" s="T23">tapeč-če-gu</ta>
            <ta e="T25" id="Seg_1138" s="T24">mat</ta>
            <ta e="T26" id="Seg_1139" s="T25">paja-m</ta>
            <ta e="T27" id="Seg_1140" s="T26">maːt-tə</ta>
            <ta e="T28" id="Seg_1141" s="T27">qweǯä-p</ta>
            <ta e="T29" id="Seg_1142" s="T158">tapeč-ča-j</ta>
            <ta e="T30" id="Seg_1143" s="T29">Sanʼka-he</ta>
            <ta e="T31" id="Seg_1144" s="T30">šɨte-qut</ta>
            <ta e="T32" id="Seg_1145" s="T31">tab-nan</ta>
            <ta e="T33" id="Seg_1146" s="T32">kanak-tə</ta>
            <ta e="T34" id="Seg_1147" s="T33">maːt-tə</ta>
            <ta e="T35" id="Seg_1148" s="T34">kur-ɨ-nna</ta>
            <ta e="T36" id="Seg_1149" s="T35">tab</ta>
            <ta e="T37" id="Seg_1150" s="T36">mogne</ta>
            <ta e="T38" id="Seg_1151" s="T37">qwen-na</ta>
            <ta e="T39" id="Seg_1152" s="T38">tö-wa</ta>
            <ta e="T40" id="Seg_1153" s="T39">čenča</ta>
            <ta e="T41" id="Seg_1154" s="T40">tan</ta>
            <ta e="T42" id="Seg_1155" s="T41">alʼǯʼiga-l</ta>
            <ta e="T43" id="Seg_1156" s="T42">qöde-mba</ta>
            <ta e="T44" id="Seg_1157" s="T43">qo-gole-m-ba</ta>
            <ta e="T45" id="Seg_1158" s="T44">haj-gele-m-ba</ta>
            <ta e="T46" id="Seg_1159" s="T45">mat</ta>
            <ta e="T47" id="Seg_1160" s="T46">čʼwesse</ta>
            <ta e="T48" id="Seg_1161" s="T47">para-nna-k</ta>
            <ta e="T148" id="Seg_1162" s="T48">tö-wa-k</ta>
            <ta e="T49" id="Seg_1163" s="T148">sabɨlʼ</ta>
            <ta e="T50" id="Seg_1164" s="T49">qo-gole-m-ba</ta>
            <ta e="T51" id="Seg_1165" s="T50">haj-gele-m-ba</ta>
            <ta e="T52" id="Seg_1166" s="T51">patom</ta>
            <ta e="T53" id="Seg_1167" s="T52">mat</ta>
            <ta e="T54" id="Seg_1168" s="T53">mat-qen</ta>
            <ta e="T55" id="Seg_1169" s="T54">warge-la-k</ta>
            <ta e="T149" id="Seg_1170" s="T55">tap</ta>
            <ta e="T56" id="Seg_1171" s="T149">%%</ta>
            <ta e="T57" id="Seg_1172" s="T56">%%-lʼǯi-mbu-k-wa-p</ta>
            <ta e="T58" id="Seg_1173" s="T57">taw-ɨ-t</ta>
            <ta e="T59" id="Seg_1174" s="T58">qwen-na-k</ta>
            <ta e="T60" id="Seg_1175" s="T59">tam</ta>
            <ta e="T61" id="Seg_1176" s="T60">warga-k</ta>
            <ta e="T62" id="Seg_1177" s="T61">qwelʼ-ča-k</ta>
            <ta e="T63" id="Seg_1178" s="T62">patom</ta>
            <ta e="T64" id="Seg_1179" s="T63">qelʼča-k</ta>
            <ta e="T65" id="Seg_1180" s="T64">pe-t</ta>
            <ta e="T66" id="Seg_1181" s="T65">tapə</ta>
            <ta e="T67" id="Seg_1182" s="T66">čʼaŋg-wa</ta>
            <ta e="T68" id="Seg_1183" s="T67">kare</ta>
            <ta e="T69" id="Seg_1184" s="T68">kur-a-l-ba-k</ta>
            <ta e="T70" id="Seg_1185" s="T69">konne</ta>
            <ta e="T71" id="Seg_1186" s="T70">kur-a-l-ba-k</ta>
            <ta e="T72" id="Seg_1187" s="T71">kaj-gan-naj</ta>
            <ta e="T73" id="Seg_1188" s="T72">čʼaŋg-wa</ta>
            <ta e="T74" id="Seg_1189" s="T73">potom</ta>
            <ta e="T75" id="Seg_1190" s="T74">ünde-ǯä-p</ta>
            <ta e="T76" id="Seg_1191" s="T75">tam</ta>
            <ta e="T77" id="Seg_1192" s="T76">maǯʼe</ta>
            <ta e="T78" id="Seg_1193" s="T77">puǯo-ɣen</ta>
            <ta e="T79" id="Seg_1194" s="T78">eː-jä</ta>
            <ta e="T80" id="Seg_1195" s="T79">mat</ta>
            <ta e="T81" id="Seg_1196" s="T80">kwe-na-k</ta>
            <ta e="T82" id="Seg_1197" s="T81">načʼi-d</ta>
            <ta e="T83" id="Seg_1198" s="T82">maǯʼe</ta>
            <ta e="T84" id="Seg_1199" s="T83">puǯo-ɣend</ta>
            <ta e="T85" id="Seg_1200" s="T84">qandark</ta>
            <ta e="T86" id="Seg_1201" s="T85">qore-nde</ta>
            <ta e="T87" id="Seg_1202" s="T86">tada-p</ta>
            <ta e="T88" id="Seg_1203" s="T87">tab-e-n</ta>
            <ta e="T89" id="Seg_1204" s="T88">tän-də</ta>
            <ta e="T90" id="Seg_1205" s="T89">ür-ǯe-mba</ta>
            <ta e="T91" id="Seg_1206" s="T90">qajču-gu</ta>
            <ta e="T92" id="Seg_1207" s="T91">pe-kundə</ta>
            <ta e="T93" id="Seg_1208" s="T92">amda-ŋ</ta>
            <ta e="T94" id="Seg_1209" s="T93">ača-m-nə</ta>
            <ta e="T95" id="Seg_1210" s="T94">potom</ta>
            <ta e="T97" id="Seg_1211" s="T96">qonda-nna</ta>
            <ta e="T98" id="Seg_1212" s="T97">qelʼča</ta>
            <ta e="T159" id="Seg_1213" s="T98">potom</ta>
            <ta e="T99" id="Seg_1214" s="T159">man</ta>
            <ta e="T100" id="Seg_1215" s="T99">mogne</ta>
            <ta e="T101" id="Seg_1216" s="T100">omd-le</ta>
            <ta e="T102" id="Seg_1217" s="T101">qwen-na-k</ta>
            <ta e="T103" id="Seg_1218" s="T102">kɨga-k</ta>
            <ta e="T104" id="Seg_1219" s="T103">tap</ta>
            <ta e="T105" id="Seg_1220" s="T104">üːde-gu</ta>
            <ta e="T106" id="Seg_1221" s="T105">samalʼo-se</ta>
            <ta e="T107" id="Seg_1222" s="T106">wrač-e-n</ta>
            <ta e="T108" id="Seg_1223" s="T107">tenča-k</ta>
            <ta e="T109" id="Seg_1224" s="T108">samolʼot</ta>
            <ta e="T110" id="Seg_1225" s="T109">aː</ta>
            <ta e="T111" id="Seg_1226" s="T110">tö-wa</ta>
            <ta e="T112" id="Seg_1227" s="T111">bolʼnica-nd</ta>
            <ta e="T113" id="Seg_1228" s="T112">qonda-j</ta>
            <ta e="T114" id="Seg_1229" s="T113">šɨte-qut</ta>
            <ta e="T115" id="Seg_1230" s="T114">mat-gal-ŋ</ta>
            <ta e="T116" id="Seg_1231" s="T115">aː</ta>
            <ta e="T117" id="Seg_1232" s="T116">i-nǯa-t</ta>
            <ta e="T118" id="Seg_1233" s="T117">šɨtə</ta>
            <ta e="T119" id="Seg_1234" s="T118">äre-ɣend</ta>
            <ta e="T120" id="Seg_1235" s="T119">ippa-j</ta>
            <ta e="T121" id="Seg_1236" s="T120">potom</ta>
            <ta e="T122" id="Seg_1237" s="T121">kwačo-ɣe-l</ta>
            <ta e="T123" id="Seg_1238" s="T122">wrač-o-n</ta>
            <ta e="T124" id="Seg_1239" s="T123">tenča-k</ta>
            <ta e="T125" id="Seg_1240" s="T124">kuʒa-t</ta>
            <ta e="T126" id="Seg_1241" s="T125">qwen-də-le</ta>
            <ta e="T127" id="Seg_1242" s="T126">tabə</ta>
            <ta e="T128" id="Seg_1243" s="T127">čenča</ta>
            <ta e="T129" id="Seg_1244" s="T128">patom</ta>
            <ta e="T130" id="Seg_1245" s="T129">patom</ta>
            <ta e="T131" id="Seg_1246" s="T130">nadi</ta>
            <ta e="T132" id="Seg_1247" s="T131">tö-wa</ta>
            <ta e="T133" id="Seg_1248" s="T132">samolʼo-se</ta>
            <ta e="T134" id="Seg_1249" s="T133">qwen-da-t</ta>
            <ta e="T135" id="Seg_1250" s="T134">tap</ta>
            <ta e="T136" id="Seg_1251" s="T135">kwačo-nd</ta>
            <ta e="T137" id="Seg_1252" s="T136">kwačo-ɣet</ta>
            <ta e="T138" id="Seg_1253" s="T137">leči-mba-t</ta>
            <ta e="T139" id="Seg_1254" s="T138">patom</ta>
            <ta e="T140" id="Seg_1255" s="T139">kare-nan</ta>
            <ta e="T141" id="Seg_1256" s="T140">maːt</ta>
            <ta e="T142" id="Seg_1257" s="T141">nawerno</ta>
            <ta e="T143" id="Seg_1258" s="T142">qwen-e-mba-t</ta>
            <ta e="T144" id="Seg_1259" s="T143">mat</ta>
            <ta e="T145" id="Seg_1260" s="T144">pelga-l</ta>
            <ta e="T146" id="Seg_1261" s="T145">warga-k</ta>
            <ta e="T147" id="Seg_1262" s="T146">pelga-l</ta>
            <ta e="T151" id="Seg_1263" s="T150">patom</ta>
            <ta e="T152" id="Seg_1264" s="T151">näda-k</ta>
            <ta e="T153" id="Seg_1265" s="T152">sejčas</ta>
            <ta e="T154" id="Seg_1266" s="T153">tar</ta>
            <ta e="T155" id="Seg_1267" s="T154">naj</ta>
            <ta e="T156" id="Seg_1268" s="T155">warge-špa-k</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1269" s="T0">ugon</ta>
            <ta e="T2" id="Seg_1270" s="T1">man</ta>
            <ta e="T3" id="Seg_1271" s="T2">nadɨ-k</ta>
            <ta e="T4" id="Seg_1272" s="T3">qwaj</ta>
            <ta e="T5" id="Seg_1273" s="T4">paja-mɨ</ta>
            <ta e="T6" id="Seg_1274" s="T5">e-ŋɨ</ta>
            <ta e="T7" id="Seg_1275" s="T6">helʼǯʼ</ta>
            <ta e="T8" id="Seg_1276" s="T7">mi-nan</ta>
            <ta e="T9" id="Seg_1277" s="T8">kɨbamar</ta>
            <ta e="T10" id="Seg_1278" s="T9">nʼeːga-t</ta>
            <ta e="T11" id="Seg_1279" s="T10">wesʼ</ta>
            <ta e="T12" id="Seg_1280" s="T11">šiǯʼau-wa-t</ta>
            <ta e="T13" id="Seg_1281" s="T12">Napas</ta>
            <ta e="T14" id="Seg_1282" s="T13">tɨka</ta>
            <ta e="T15" id="Seg_1283" s="T14">töː-wa-j</ta>
            <ta e="T16" id="Seg_1284" s="T15">okkər</ta>
            <ta e="T17" id="Seg_1285" s="T16">neː-w</ta>
            <ta e="T18" id="Seg_1286" s="T17">e-ŋɨ-n</ta>
            <ta e="T19" id="Seg_1287" s="T18">naj</ta>
            <ta e="T20" id="Seg_1288" s="T19">šiǯʼau-wa</ta>
            <ta e="T21" id="Seg_1289" s="T20">me</ta>
            <ta e="T22" id="Seg_1290" s="T21">qwän-ŋɨ-j</ta>
            <ta e="T23" id="Seg_1291" s="T22">šitə-qɨn</ta>
            <ta e="T24" id="Seg_1292" s="T23">tabek-ču-gu</ta>
            <ta e="T25" id="Seg_1293" s="T24">man</ta>
            <ta e="T26" id="Seg_1294" s="T25">paja-mɨ</ta>
            <ta e="T27" id="Seg_1295" s="T26">maːt-nde</ta>
            <ta e="T28" id="Seg_1296" s="T27">kwešɨ-m</ta>
            <ta e="T29" id="Seg_1297" s="T158">tabek-ču-j</ta>
            <ta e="T30" id="Seg_1298" s="T29">Sanʼka-se</ta>
            <ta e="T31" id="Seg_1299" s="T30">šitə-qɨn</ta>
            <ta e="T32" id="Seg_1300" s="T31">tab-nan</ta>
            <ta e="T33" id="Seg_1301" s="T32">kanak-tɨ</ta>
            <ta e="T34" id="Seg_1302" s="T33">maːt-nde</ta>
            <ta e="T35" id="Seg_1303" s="T34">kur-ɨ-ŋɨ</ta>
            <ta e="T36" id="Seg_1304" s="T35">tab</ta>
            <ta e="T37" id="Seg_1305" s="T36">moqne</ta>
            <ta e="T38" id="Seg_1306" s="T37">qwän-ŋɨ</ta>
            <ta e="T39" id="Seg_1307" s="T38">töː-wa</ta>
            <ta e="T40" id="Seg_1308" s="T39">čenčɨ</ta>
            <ta e="T41" id="Seg_1309" s="T40">tan</ta>
            <ta e="T42" id="Seg_1310" s="T41">alʼǯiga-lə</ta>
            <ta e="T43" id="Seg_1311" s="T42">qöːdə-mbɨ</ta>
            <ta e="T44" id="Seg_1312" s="T43">qoː-galɨ-m-mbɨ</ta>
            <ta e="T45" id="Seg_1313" s="T44">saj-galɨ-m-mbɨ</ta>
            <ta e="T46" id="Seg_1314" s="T45">man</ta>
            <ta e="T47" id="Seg_1315" s="T46">čwesse</ta>
            <ta e="T48" id="Seg_1316" s="T47">para-ŋɨ-k</ta>
            <ta e="T148" id="Seg_1317" s="T48">töː-wa-k</ta>
            <ta e="T49" id="Seg_1318" s="T148">sabɨlʼ</ta>
            <ta e="T50" id="Seg_1319" s="T49">qoː-galɨ-m-mbɨ</ta>
            <ta e="T51" id="Seg_1320" s="T50">saj-galɨ-m-mbɨ</ta>
            <ta e="T52" id="Seg_1321" s="T51">patom</ta>
            <ta e="T53" id="Seg_1322" s="T52">man</ta>
            <ta e="T54" id="Seg_1323" s="T53">maːt-qɨn</ta>
            <ta e="T55" id="Seg_1324" s="T54">wargɨ-lɨ-k</ta>
            <ta e="T149" id="Seg_1325" s="T55">tab</ta>
            <ta e="T56" id="Seg_1326" s="T149">%%</ta>
            <ta e="T57" id="Seg_1327" s="T56">%%-lǯe-mbɨ-ku-wa-p</ta>
            <ta e="T58" id="Seg_1328" s="T57">taɣ-ɨ-n</ta>
            <ta e="T59" id="Seg_1329" s="T58">qwän-ŋɨ-k</ta>
            <ta e="T60" id="Seg_1330" s="T59">tam</ta>
            <ta e="T61" id="Seg_1331" s="T60">wargɨ-k</ta>
            <ta e="T62" id="Seg_1332" s="T61">qwel-ču-k</ta>
            <ta e="T63" id="Seg_1333" s="T62">patom</ta>
            <ta e="T64" id="Seg_1334" s="T63">qelʼčǝ-k</ta>
            <ta e="T65" id="Seg_1335" s="T64">pe-n</ta>
            <ta e="T66" id="Seg_1336" s="T65">tab</ta>
            <ta e="T67" id="Seg_1337" s="T66">čaŋgɨ-wa</ta>
            <ta e="T68" id="Seg_1338" s="T67">kare</ta>
            <ta e="T69" id="Seg_1339" s="T68">kur-ɨ-lɨ-mbɨ-k</ta>
            <ta e="T70" id="Seg_1340" s="T69">konne</ta>
            <ta e="T71" id="Seg_1341" s="T70">kur-ɨ-lɨ-mbɨ-k</ta>
            <ta e="T72" id="Seg_1342" s="T71">qaj-qɨn-naj</ta>
            <ta e="T73" id="Seg_1343" s="T72">čaŋgɨ-wa</ta>
            <ta e="T74" id="Seg_1344" s="T73">patom</ta>
            <ta e="T75" id="Seg_1345" s="T74">undɛ-nǯe-m</ta>
            <ta e="T76" id="Seg_1346" s="T75">tab</ta>
            <ta e="T77" id="Seg_1347" s="T76">maǯʼo</ta>
            <ta e="T78" id="Seg_1348" s="T77">puǯo-qɨn</ta>
            <ta e="T79" id="Seg_1349" s="T78">e-ja</ta>
            <ta e="T80" id="Seg_1350" s="T79">man</ta>
            <ta e="T81" id="Seg_1351" s="T80">qwän-ŋɨ-k</ta>
            <ta e="T82" id="Seg_1352" s="T81">nača-nt</ta>
            <ta e="T83" id="Seg_1353" s="T82">maǯʼo</ta>
            <ta e="T84" id="Seg_1354" s="T83">puǯo-qɨnt</ta>
            <ta e="T85" id="Seg_1355" s="T84">qanduk</ta>
            <ta e="T86" id="Seg_1356" s="T85">qore-nde</ta>
            <ta e="T87" id="Seg_1357" s="T86">tade-p</ta>
            <ta e="T88" id="Seg_1358" s="T87">tab-ɨ-n</ta>
            <ta e="T89" id="Seg_1359" s="T88">tʼane-tɨ</ta>
            <ta e="T90" id="Seg_1360" s="T89">ür-nǯe-mbɨ</ta>
            <ta e="T91" id="Seg_1361" s="T90">qajču-gu</ta>
            <ta e="T92" id="Seg_1362" s="T91">pe-kundɨ</ta>
            <ta e="T93" id="Seg_1363" s="T92">omde-k</ta>
            <ta e="T94" id="Seg_1364" s="T93">aːčɨ-m-ŋɨ</ta>
            <ta e="T95" id="Seg_1365" s="T94">patom</ta>
            <ta e="T97" id="Seg_1366" s="T96">qontɨ-ŋɨ</ta>
            <ta e="T98" id="Seg_1367" s="T97">qelʼčǝ</ta>
            <ta e="T159" id="Seg_1368" s="T98">patom</ta>
            <ta e="T99" id="Seg_1369" s="T159">man</ta>
            <ta e="T100" id="Seg_1370" s="T99">moqne</ta>
            <ta e="T101" id="Seg_1371" s="T100">omde-le</ta>
            <ta e="T102" id="Seg_1372" s="T101">qwän-ŋɨ-k</ta>
            <ta e="T103" id="Seg_1373" s="T102">kɨge-k</ta>
            <ta e="T104" id="Seg_1374" s="T103">tab</ta>
            <ta e="T105" id="Seg_1375" s="T104">üdɨ-gu</ta>
            <ta e="T106" id="Seg_1376" s="T105">samolʼot-se</ta>
            <ta e="T107" id="Seg_1377" s="T106">wrač-ɨ-ni</ta>
            <ta e="T108" id="Seg_1378" s="T107">čenčɨ-k</ta>
            <ta e="T109" id="Seg_1379" s="T108">samolʼot</ta>
            <ta e="T110" id="Seg_1380" s="T109">aː</ta>
            <ta e="T111" id="Seg_1381" s="T110">töː-wa</ta>
            <ta e="T112" id="Seg_1382" s="T111">palnisa-nde</ta>
            <ta e="T113" id="Seg_1383" s="T112">qontɨ-j</ta>
            <ta e="T114" id="Seg_1384" s="T113">šitə-qɨn</ta>
            <ta e="T115" id="Seg_1385" s="T114">maːt-galɨ-ŋ</ta>
            <ta e="T116" id="Seg_1386" s="T115">aː</ta>
            <ta e="T117" id="Seg_1387" s="T116">iː-nǯe-dət</ta>
            <ta e="T118" id="Seg_1388" s="T117">šitə</ta>
            <ta e="T119" id="Seg_1389" s="T118">ärä-qɨnt</ta>
            <ta e="T120" id="Seg_1390" s="T119">eppɨ-j</ta>
            <ta e="T121" id="Seg_1391" s="T120">patom</ta>
            <ta e="T122" id="Seg_1392" s="T121">kwače-qɨn-lʼ</ta>
            <ta e="T123" id="Seg_1393" s="T122">wrač-ɨ-ni</ta>
            <ta e="T124" id="Seg_1394" s="T123">čenčɨ-k</ta>
            <ta e="T125" id="Seg_1395" s="T124">kuča-n</ta>
            <ta e="T126" id="Seg_1396" s="T125">qwän-dɨ-l</ta>
            <ta e="T127" id="Seg_1397" s="T126">tab</ta>
            <ta e="T128" id="Seg_1398" s="T127">čenčɨ</ta>
            <ta e="T129" id="Seg_1399" s="T128">patom</ta>
            <ta e="T130" id="Seg_1400" s="T129">patom</ta>
            <ta e="T131" id="Seg_1401" s="T130">%%</ta>
            <ta e="T132" id="Seg_1402" s="T131">töː-wa</ta>
            <ta e="T133" id="Seg_1403" s="T132">samolʼot-se</ta>
            <ta e="T134" id="Seg_1404" s="T133">qwän-dɨ-dət</ta>
            <ta e="T135" id="Seg_1405" s="T134">tab</ta>
            <ta e="T136" id="Seg_1406" s="T135">kwače-nde</ta>
            <ta e="T137" id="Seg_1407" s="T136">kwače-qɨn</ta>
            <ta e="T138" id="Seg_1408" s="T137">lʼeči-mbɨ-dət</ta>
            <ta e="T139" id="Seg_1409" s="T138">patom</ta>
            <ta e="T140" id="Seg_1410" s="T139">kare-nan</ta>
            <ta e="T141" id="Seg_1411" s="T140">maːt</ta>
            <ta e="T142" id="Seg_1412" s="T141">nawernə</ta>
            <ta e="T143" id="Seg_1413" s="T142">qwän-ɨ-mbɨ-dət</ta>
            <ta e="T144" id="Seg_1414" s="T143">man</ta>
            <ta e="T145" id="Seg_1415" s="T144">pelga-lʼ</ta>
            <ta e="T146" id="Seg_1416" s="T145">wargɨ-k</ta>
            <ta e="T147" id="Seg_1417" s="T146">pelga-lʼ</ta>
            <ta e="T151" id="Seg_1418" s="T150">patom</ta>
            <ta e="T152" id="Seg_1419" s="T151">nadɨ-k</ta>
            <ta e="T153" id="Seg_1420" s="T152">sečas</ta>
            <ta e="T154" id="Seg_1421" s="T153">tar</ta>
            <ta e="T155" id="Seg_1422" s="T154">naj</ta>
            <ta e="T156" id="Seg_1423" s="T155">wargɨ-špɨ-k</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1424" s="T0">earlier</ta>
            <ta e="T2" id="Seg_1425" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_1426" s="T2">get.married-1SG.S</ta>
            <ta e="T4" id="Seg_1427" s="T3">beautiful</ta>
            <ta e="T5" id="Seg_1428" s="T4">wife-1SG</ta>
            <ta e="T6" id="Seg_1429" s="T5">be-CO.[3SG.S]</ta>
            <ta e="T7" id="Seg_1430" s="T6">seven</ta>
            <ta e="T8" id="Seg_1431" s="T7">we.DU.GEN-ADES</ta>
            <ta e="T9" id="Seg_1432" s="T8">child.[NOM]</ta>
            <ta e="T10" id="Seg_1433" s="T9">daughter-PL</ta>
            <ta e="T11" id="Seg_1434" s="T10">all</ta>
            <ta e="T12" id="Seg_1435" s="T11">die-CO-3PL</ta>
            <ta e="T13" id="Seg_1436" s="T12">Napas.[NOM]</ta>
            <ta e="T14" id="Seg_1437" s="T13">here</ta>
            <ta e="T15" id="Seg_1438" s="T14">come-CO-1DU</ta>
            <ta e="T16" id="Seg_1439" s="T15">one</ta>
            <ta e="T17" id="Seg_1440" s="T16">daughter.[NOM]-1SG</ta>
            <ta e="T18" id="Seg_1441" s="T17">be-CO-3SG.S</ta>
            <ta e="T19" id="Seg_1442" s="T18">also</ta>
            <ta e="T20" id="Seg_1443" s="T19">die-CO.[3SG.S]</ta>
            <ta e="T21" id="Seg_1444" s="T20">we.DU.[NOM]</ta>
            <ta e="T22" id="Seg_1445" s="T21">go.away-CO-1DU</ta>
            <ta e="T23" id="Seg_1446" s="T22">two-LOC</ta>
            <ta e="T24" id="Seg_1447" s="T23">squirrel-CAP-INF</ta>
            <ta e="T25" id="Seg_1448" s="T24">I.NOM</ta>
            <ta e="T26" id="Seg_1449" s="T25">wife-1SG</ta>
            <ta e="T27" id="Seg_1450" s="T26">house-ILL</ta>
            <ta e="T28" id="Seg_1451" s="T27">leave-1SG.O</ta>
            <ta e="T29" id="Seg_1452" s="T158">squirrel-CAP-1DU</ta>
            <ta e="T30" id="Seg_1453" s="T29">Sanka-COM</ta>
            <ta e="T31" id="Seg_1454" s="T30">two-LOC</ta>
            <ta e="T32" id="Seg_1455" s="T31">(s)he-ADES</ta>
            <ta e="T33" id="Seg_1456" s="T32">dog-3SG</ta>
            <ta e="T34" id="Seg_1457" s="T33">house-ILL</ta>
            <ta e="T35" id="Seg_1458" s="T34">run-EP-CO.[3SG.S]</ta>
            <ta e="T36" id="Seg_1459" s="T35">(s)he.[NOM]</ta>
            <ta e="T37" id="Seg_1460" s="T36">home</ta>
            <ta e="T38" id="Seg_1461" s="T37">go.away-CO.[3SG.S]</ta>
            <ta e="T39" id="Seg_1462" s="T38">come-CO.[3SG.S]</ta>
            <ta e="T40" id="Seg_1463" s="T39">say.[3SG.S]</ta>
            <ta e="T41" id="Seg_1464" s="T40">you.SG.GEN</ta>
            <ta e="T42" id="Seg_1465" s="T41">grandmother.[NOM]-2SG</ta>
            <ta e="T43" id="Seg_1466" s="T42">be.ill-PST.NAR.[3SG.S]</ta>
            <ta e="T44" id="Seg_1467" s="T43">ear-CAR-TRL-PST.NAR.[3SG.S]</ta>
            <ta e="T45" id="Seg_1468" s="T44">eye-CAR-TRL-PST.NAR.[3SG.S]</ta>
            <ta e="T46" id="Seg_1469" s="T45">I.NOM</ta>
            <ta e="T47" id="Seg_1470" s="T46">backward</ta>
            <ta e="T48" id="Seg_1471" s="T47">return-CO-1SG.S</ta>
            <ta e="T148" id="Seg_1472" s="T48">come-CO-1SG.S</ta>
            <ta e="T49" id="Seg_1473" s="T148">indeed</ta>
            <ta e="T50" id="Seg_1474" s="T49">ear-CAR-TRL-PST.NAR.[3SG.S]</ta>
            <ta e="T51" id="Seg_1475" s="T50">eye-CAR-TRL-PST.NAR.[3SG.S]</ta>
            <ta e="T52" id="Seg_1476" s="T51">then</ta>
            <ta e="T53" id="Seg_1477" s="T52">I.NOM</ta>
            <ta e="T54" id="Seg_1478" s="T53">house-LOC</ta>
            <ta e="T55" id="Seg_1479" s="T54">live-RES-1SG.S</ta>
            <ta e="T149" id="Seg_1480" s="T55">(s)he.[NOM]</ta>
            <ta e="T56" id="Seg_1481" s="T149">%%</ta>
            <ta e="T57" id="Seg_1482" s="T56">%%-VBLZ-DUR-HAB-CO-1SG.S</ta>
            <ta e="T58" id="Seg_1483" s="T57">summer-EP-ADV.LOC</ta>
            <ta e="T59" id="Seg_1484" s="T58">go.away-CO-1SG.S</ta>
            <ta e="T60" id="Seg_1485" s="T59">there</ta>
            <ta e="T61" id="Seg_1486" s="T60">live-1SG.S</ta>
            <ta e="T62" id="Seg_1487" s="T61">fish-CAP-1SG.S</ta>
            <ta e="T63" id="Seg_1488" s="T62">then</ta>
            <ta e="T64" id="Seg_1489" s="T63">wake.up-1SG.S</ta>
            <ta e="T65" id="Seg_1490" s="T64">night-ADV.LOC</ta>
            <ta e="T66" id="Seg_1491" s="T65">(s)he.[NOM]</ta>
            <ta e="T67" id="Seg_1492" s="T66">NEG.EX-CO.[3SG.S]</ta>
            <ta e="T68" id="Seg_1493" s="T67">to.the.river</ta>
            <ta e="T69" id="Seg_1494" s="T68">run-EP-RES-PST.NAR-1SG.S</ta>
            <ta e="T70" id="Seg_1495" s="T69">upwards</ta>
            <ta e="T71" id="Seg_1496" s="T70">run-EP-RES-PST.NAR-1SG.S</ta>
            <ta e="T72" id="Seg_1497" s="T71">what-LOC-EMPH</ta>
            <ta e="T73" id="Seg_1498" s="T72">NEG.EX-CO.[3SG.S]</ta>
            <ta e="T74" id="Seg_1499" s="T73">then</ta>
            <ta e="T75" id="Seg_1500" s="T74">hear-IPFV3-1SG.O</ta>
            <ta e="T76" id="Seg_1501" s="T75">there</ta>
            <ta e="T77" id="Seg_1502" s="T76">taiga.[NOM]</ta>
            <ta e="T78" id="Seg_1503" s="T77">inside-LOC</ta>
            <ta e="T79" id="Seg_1504" s="T78">be-CO.[3SG.S]</ta>
            <ta e="T80" id="Seg_1505" s="T79">I.NOM</ta>
            <ta e="T81" id="Seg_1506" s="T80">go.away-CO-1SG.S</ta>
            <ta e="T82" id="Seg_1507" s="T81">there-ADV.ILL</ta>
            <ta e="T83" id="Seg_1508" s="T82">taiga.[NOM]</ta>
            <ta e="T84" id="Seg_1509" s="T83">inside-ILL.3SG</ta>
            <ta e="T85" id="Seg_1510" s="T84">how</ta>
            <ta e="T86" id="Seg_1511" s="T85">hut-ILL</ta>
            <ta e="T87" id="Seg_1512" s="T86">bring-1SG.O</ta>
            <ta e="T88" id="Seg_1513" s="T87">(s)he-EP-GEN</ta>
            <ta e="T89" id="Seg_1514" s="T88">mind-3SG</ta>
            <ta e="T90" id="Seg_1515" s="T89">lose-IPFV3-PST.NAR.[3SG.S]</ta>
            <ta e="T91" id="Seg_1516" s="T90">do.what-INF</ta>
            <ta e="T92" id="Seg_1517" s="T91">night-during</ta>
            <ta e="T93" id="Seg_1518" s="T92">sit-3SG.S</ta>
            <ta e="T94" id="Seg_1519" s="T93">wait-TRL-CO.[3SG.S]</ta>
            <ta e="T95" id="Seg_1520" s="T94">then</ta>
            <ta e="T97" id="Seg_1521" s="T96">sleep-CO.[3SG.S]</ta>
            <ta e="T98" id="Seg_1522" s="T97">wake.up.[3SG.S]</ta>
            <ta e="T159" id="Seg_1523" s="T98">then</ta>
            <ta e="T99" id="Seg_1524" s="T159">I.NOM</ta>
            <ta e="T100" id="Seg_1525" s="T99">home</ta>
            <ta e="T101" id="Seg_1526" s="T100">sit-CVB</ta>
            <ta e="T102" id="Seg_1527" s="T101">go.away-CO-1SG.S</ta>
            <ta e="T103" id="Seg_1528" s="T102">want-1SG.S</ta>
            <ta e="T104" id="Seg_1529" s="T103">(s)he.[NOM]</ta>
            <ta e="T105" id="Seg_1530" s="T104">send-INF</ta>
            <ta e="T106" id="Seg_1531" s="T105">airplane-INSTR</ta>
            <ta e="T107" id="Seg_1532" s="T106">doctor-EP-ALL</ta>
            <ta e="T108" id="Seg_1533" s="T107">say-1SG.S</ta>
            <ta e="T109" id="Seg_1534" s="T108">airplane.[NOM]</ta>
            <ta e="T110" id="Seg_1535" s="T109">NEG</ta>
            <ta e="T111" id="Seg_1536" s="T110">come-CO.[3SG.S]</ta>
            <ta e="T112" id="Seg_1537" s="T111">hospital-ILL</ta>
            <ta e="T113" id="Seg_1538" s="T112">lie.down-1DU</ta>
            <ta e="T114" id="Seg_1539" s="T113">two-LOC</ta>
            <ta e="T115" id="Seg_1540" s="T114">I-CAR-ADVZ</ta>
            <ta e="T116" id="Seg_1541" s="T115">NEG</ta>
            <ta e="T117" id="Seg_1542" s="T116">take-IPFV3-3PL</ta>
            <ta e="T118" id="Seg_1543" s="T117">two</ta>
            <ta e="T119" id="Seg_1544" s="T118">month-LOC.3SG</ta>
            <ta e="T120" id="Seg_1545" s="T119">lie-1DU</ta>
            <ta e="T121" id="Seg_1546" s="T120">then</ta>
            <ta e="T122" id="Seg_1547" s="T121">town-LOC-ADJZ</ta>
            <ta e="T123" id="Seg_1548" s="T122">doctor-EP-ALL</ta>
            <ta e="T124" id="Seg_1549" s="T123">say-1SG.S</ta>
            <ta e="T125" id="Seg_1550" s="T124">when-ADV.LOC</ta>
            <ta e="T126" id="Seg_1551" s="T125">go.away-TR-2SG.O</ta>
            <ta e="T127" id="Seg_1552" s="T126">(s)he.[NOM]</ta>
            <ta e="T128" id="Seg_1553" s="T127">say.[3SG.S]</ta>
            <ta e="T129" id="Seg_1554" s="T128">then</ta>
            <ta e="T130" id="Seg_1555" s="T129">then</ta>
            <ta e="T131" id="Seg_1556" s="T130">%%</ta>
            <ta e="T132" id="Seg_1557" s="T131">come-CO.[3SG.S]</ta>
            <ta e="T133" id="Seg_1558" s="T132">airplane-INSTR</ta>
            <ta e="T134" id="Seg_1559" s="T133">go.away-TR-3PL</ta>
            <ta e="T135" id="Seg_1560" s="T134">(s)he.[NOM]</ta>
            <ta e="T136" id="Seg_1561" s="T135">town-ILL</ta>
            <ta e="T137" id="Seg_1562" s="T136">town-LOC</ta>
            <ta e="T138" id="Seg_1563" s="T137">cure-PST.NAR-3PL</ta>
            <ta e="T139" id="Seg_1564" s="T138">then</ta>
            <ta e="T140" id="Seg_1565" s="T139">to.the.river-ADES</ta>
            <ta e="T141" id="Seg_1566" s="T140">house.[NOM]</ta>
            <ta e="T142" id="Seg_1567" s="T141">probably</ta>
            <ta e="T143" id="Seg_1568" s="T142">go.away-EP-PST.NAR-3PL</ta>
            <ta e="T144" id="Seg_1569" s="T143">I.NOM</ta>
            <ta e="T145" id="Seg_1570" s="T144">half-ADJZ</ta>
            <ta e="T146" id="Seg_1571" s="T145">live-1SG.S</ta>
            <ta e="T147" id="Seg_1572" s="T146">half-ADJZ</ta>
            <ta e="T151" id="Seg_1573" s="T150">then</ta>
            <ta e="T152" id="Seg_1574" s="T151">get.married-1SG.S</ta>
            <ta e="T153" id="Seg_1575" s="T152">now</ta>
            <ta e="T154" id="Seg_1576" s="T153">still</ta>
            <ta e="T155" id="Seg_1577" s="T154">also</ta>
            <ta e="T156" id="Seg_1578" s="T155">live-IPFV2-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1579" s="T0">раньше</ta>
            <ta e="T2" id="Seg_1580" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_1581" s="T2">жениться-1SG.S</ta>
            <ta e="T4" id="Seg_1582" s="T3">красивый</ta>
            <ta e="T5" id="Seg_1583" s="T4">жена-1SG</ta>
            <ta e="T6" id="Seg_1584" s="T5">быть-CO.[3SG.S]</ta>
            <ta e="T7" id="Seg_1585" s="T6">семь</ta>
            <ta e="T8" id="Seg_1586" s="T7">мы.DU.GEN-ADES</ta>
            <ta e="T9" id="Seg_1587" s="T8">ребёнок.[NOM]</ta>
            <ta e="T10" id="Seg_1588" s="T9">дочь-PL</ta>
            <ta e="T11" id="Seg_1589" s="T10">весь</ta>
            <ta e="T12" id="Seg_1590" s="T11">умереть-CO-3PL</ta>
            <ta e="T13" id="Seg_1591" s="T12">Напас.[NOM]</ta>
            <ta e="T14" id="Seg_1592" s="T13">сюда</ta>
            <ta e="T15" id="Seg_1593" s="T14">прийти-CO-1DU</ta>
            <ta e="T16" id="Seg_1594" s="T15">один</ta>
            <ta e="T17" id="Seg_1595" s="T16">дочь.[NOM]-1SG</ta>
            <ta e="T18" id="Seg_1596" s="T17">быть-CO-3SG.S</ta>
            <ta e="T19" id="Seg_1597" s="T18">тоже</ta>
            <ta e="T20" id="Seg_1598" s="T19">умереть-CO.[3SG.S]</ta>
            <ta e="T21" id="Seg_1599" s="T20">мы.DU.[NOM]</ta>
            <ta e="T22" id="Seg_1600" s="T21">пойти-CO-1DU</ta>
            <ta e="T23" id="Seg_1601" s="T22">два-LOC</ta>
            <ta e="T24" id="Seg_1602" s="T23">белка-CAP-INF</ta>
            <ta e="T25" id="Seg_1603" s="T24">я.NOM</ta>
            <ta e="T26" id="Seg_1604" s="T25">жена-1SG</ta>
            <ta e="T27" id="Seg_1605" s="T26">дом-ILL</ta>
            <ta e="T28" id="Seg_1606" s="T27">оставить-1SG.O</ta>
            <ta e="T29" id="Seg_1607" s="T158">белка-CAP-1DU</ta>
            <ta e="T30" id="Seg_1608" s="T29">Санька-COM</ta>
            <ta e="T31" id="Seg_1609" s="T30">два-LOC</ta>
            <ta e="T32" id="Seg_1610" s="T31">он(а)-ADES</ta>
            <ta e="T33" id="Seg_1611" s="T32">собака-3SG</ta>
            <ta e="T34" id="Seg_1612" s="T33">дом-ILL</ta>
            <ta e="T35" id="Seg_1613" s="T34">бегать-EP-CO.[3SG.S]</ta>
            <ta e="T36" id="Seg_1614" s="T35">он(а).[NOM]</ta>
            <ta e="T37" id="Seg_1615" s="T36">домой</ta>
            <ta e="T38" id="Seg_1616" s="T37">пойти-CO.[3SG.S]</ta>
            <ta e="T39" id="Seg_1617" s="T38">прийти-CO.[3SG.S]</ta>
            <ta e="T40" id="Seg_1618" s="T39">сказать.[3SG.S]</ta>
            <ta e="T41" id="Seg_1619" s="T40">ты.GEN</ta>
            <ta e="T42" id="Seg_1620" s="T41">бабушка.[NOM]-2SG</ta>
            <ta e="T43" id="Seg_1621" s="T42">болеть-PST.NAR.[3SG.S]</ta>
            <ta e="T44" id="Seg_1622" s="T43">ухо-CAR-TRL-PST.NAR.[3SG.S]</ta>
            <ta e="T45" id="Seg_1623" s="T44">глаз-CAR-TRL-PST.NAR.[3SG.S]</ta>
            <ta e="T46" id="Seg_1624" s="T45">я.NOM</ta>
            <ta e="T47" id="Seg_1625" s="T46">назад</ta>
            <ta e="T48" id="Seg_1626" s="T47">вернуться-CO-1SG.S</ta>
            <ta e="T148" id="Seg_1627" s="T48">прийти-CO-1SG.S</ta>
            <ta e="T49" id="Seg_1628" s="T148">в.самом.деле</ta>
            <ta e="T50" id="Seg_1629" s="T49">ухо-CAR-TRL-PST.NAR.[3SG.S]</ta>
            <ta e="T51" id="Seg_1630" s="T50">глаз-CAR-TRL-PST.NAR.[3SG.S]</ta>
            <ta e="T52" id="Seg_1631" s="T51">потом</ta>
            <ta e="T53" id="Seg_1632" s="T52">я.NOM</ta>
            <ta e="T54" id="Seg_1633" s="T53">дом-LOC</ta>
            <ta e="T55" id="Seg_1634" s="T54">жить-RES-1SG.S</ta>
            <ta e="T149" id="Seg_1635" s="T55">он(а).[NOM]</ta>
            <ta e="T56" id="Seg_1636" s="T149">%%</ta>
            <ta e="T57" id="Seg_1637" s="T56">%%-VBLZ-DUR-HAB-CO-1SG.S</ta>
            <ta e="T58" id="Seg_1638" s="T57">лето-EP-ADV.LOC</ta>
            <ta e="T59" id="Seg_1639" s="T58">пойти-CO-1SG.S</ta>
            <ta e="T60" id="Seg_1640" s="T59">там</ta>
            <ta e="T61" id="Seg_1641" s="T60">жить-1SG.S</ta>
            <ta e="T62" id="Seg_1642" s="T61">рыба-CAP-1SG.S</ta>
            <ta e="T63" id="Seg_1643" s="T62">потом</ta>
            <ta e="T64" id="Seg_1644" s="T63">проснуться-1SG.S</ta>
            <ta e="T65" id="Seg_1645" s="T64">ночь-ADV.LOC</ta>
            <ta e="T66" id="Seg_1646" s="T65">он(а).[NOM]</ta>
            <ta e="T67" id="Seg_1647" s="T66">NEG.EX-CO.[3SG.S]</ta>
            <ta e="T68" id="Seg_1648" s="T67">к.реке</ta>
            <ta e="T69" id="Seg_1649" s="T68">бегать-EP-RES-PST.NAR-1SG.S</ta>
            <ta e="T70" id="Seg_1650" s="T69">вверх</ta>
            <ta e="T71" id="Seg_1651" s="T70">бегать-EP-RES-PST.NAR-1SG.S</ta>
            <ta e="T72" id="Seg_1652" s="T71">что-LOC-EMPH</ta>
            <ta e="T73" id="Seg_1653" s="T72">NEG.EX-CO.[3SG.S]</ta>
            <ta e="T74" id="Seg_1654" s="T73">потом</ta>
            <ta e="T75" id="Seg_1655" s="T74">слышать-IPFV3-1SG.O</ta>
            <ta e="T76" id="Seg_1656" s="T75">там</ta>
            <ta e="T77" id="Seg_1657" s="T76">тайга.[NOM]</ta>
            <ta e="T78" id="Seg_1658" s="T77">внутренность-LOC</ta>
            <ta e="T79" id="Seg_1659" s="T78">быть-CO.[3SG.S]</ta>
            <ta e="T80" id="Seg_1660" s="T79">я.NOM</ta>
            <ta e="T81" id="Seg_1661" s="T80">пойти-CO-1SG.S</ta>
            <ta e="T82" id="Seg_1662" s="T81">туда-ADV.ILL</ta>
            <ta e="T83" id="Seg_1663" s="T82">тайга.[NOM]</ta>
            <ta e="T84" id="Seg_1664" s="T83">внутренность-ILL.3SG</ta>
            <ta e="T85" id="Seg_1665" s="T84">как</ta>
            <ta e="T86" id="Seg_1666" s="T85">шалаш-ILL</ta>
            <ta e="T87" id="Seg_1667" s="T86">принести-1SG.O</ta>
            <ta e="T88" id="Seg_1668" s="T87">он(а)-EP-GEN</ta>
            <ta e="T89" id="Seg_1669" s="T88">ум-3SG</ta>
            <ta e="T90" id="Seg_1670" s="T89">потеряться-IPFV3-PST.NAR.[3SG.S]</ta>
            <ta e="T91" id="Seg_1671" s="T90">что.делать-INF</ta>
            <ta e="T92" id="Seg_1672" s="T91">ночь-в.течение</ta>
            <ta e="T93" id="Seg_1673" s="T92">сидеть-3SG.S</ta>
            <ta e="T94" id="Seg_1674" s="T93">ждать-TRL-CO.[3SG.S]</ta>
            <ta e="T95" id="Seg_1675" s="T94">потом</ta>
            <ta e="T97" id="Seg_1676" s="T96">спать-CO.[3SG.S]</ta>
            <ta e="T98" id="Seg_1677" s="T97">проснуться.[3SG.S]</ta>
            <ta e="T159" id="Seg_1678" s="T98">потом</ta>
            <ta e="T99" id="Seg_1679" s="T159">я.NOM</ta>
            <ta e="T100" id="Seg_1680" s="T99">домой</ta>
            <ta e="T101" id="Seg_1681" s="T100">сидеть-CVB</ta>
            <ta e="T102" id="Seg_1682" s="T101">пойти-CO-1SG.S</ta>
            <ta e="T103" id="Seg_1683" s="T102">хотеть-1SG.S</ta>
            <ta e="T104" id="Seg_1684" s="T103">он(а).[NOM]</ta>
            <ta e="T105" id="Seg_1685" s="T104">посылать-INF</ta>
            <ta e="T106" id="Seg_1686" s="T105">самолёт-INSTR</ta>
            <ta e="T107" id="Seg_1687" s="T106">врач-EP-ALL</ta>
            <ta e="T108" id="Seg_1688" s="T107">сказать-1SG.S</ta>
            <ta e="T109" id="Seg_1689" s="T108">самолёт.[NOM]</ta>
            <ta e="T110" id="Seg_1690" s="T109">NEG</ta>
            <ta e="T111" id="Seg_1691" s="T110">прийти-CO.[3SG.S]</ta>
            <ta e="T112" id="Seg_1692" s="T111">больница-ILL</ta>
            <ta e="T113" id="Seg_1693" s="T112">лечь-1DU</ta>
            <ta e="T114" id="Seg_1694" s="T113">два-LOC</ta>
            <ta e="T115" id="Seg_1695" s="T114">я-CAR-ADVZ</ta>
            <ta e="T116" id="Seg_1696" s="T115">NEG</ta>
            <ta e="T117" id="Seg_1697" s="T116">взять-IPFV3-3PL</ta>
            <ta e="T118" id="Seg_1698" s="T117">два</ta>
            <ta e="T119" id="Seg_1699" s="T118">месяц-LOC.3SG</ta>
            <ta e="T120" id="Seg_1700" s="T119">лежать-1DU</ta>
            <ta e="T121" id="Seg_1701" s="T120">потом</ta>
            <ta e="T122" id="Seg_1702" s="T121">город-LOC-ADJZ</ta>
            <ta e="T123" id="Seg_1703" s="T122">врач-EP-ALL</ta>
            <ta e="T124" id="Seg_1704" s="T123">сказать-1SG.S</ta>
            <ta e="T125" id="Seg_1705" s="T124">когда-ADV.LOC</ta>
            <ta e="T126" id="Seg_1706" s="T125">пойти-TR-2SG.O</ta>
            <ta e="T127" id="Seg_1707" s="T126">он(а).[NOM]</ta>
            <ta e="T128" id="Seg_1708" s="T127">сказать.[3SG.S]</ta>
            <ta e="T129" id="Seg_1709" s="T128">потом</ta>
            <ta e="T130" id="Seg_1710" s="T129">потом</ta>
            <ta e="T131" id="Seg_1711" s="T130">%%</ta>
            <ta e="T132" id="Seg_1712" s="T131">прийти-CO.[3SG.S]</ta>
            <ta e="T133" id="Seg_1713" s="T132">самолёт-INSTR</ta>
            <ta e="T134" id="Seg_1714" s="T133">пойти-TR-3PL</ta>
            <ta e="T135" id="Seg_1715" s="T134">он(а).[NOM]</ta>
            <ta e="T136" id="Seg_1716" s="T135">город-ILL</ta>
            <ta e="T137" id="Seg_1717" s="T136">город-LOC</ta>
            <ta e="T138" id="Seg_1718" s="T137">лечить-PST.NAR-3PL</ta>
            <ta e="T139" id="Seg_1719" s="T138">потом</ta>
            <ta e="T140" id="Seg_1720" s="T139">к.реке-ADES</ta>
            <ta e="T141" id="Seg_1721" s="T140">дом.[NOM]</ta>
            <ta e="T142" id="Seg_1722" s="T141">наверно</ta>
            <ta e="T143" id="Seg_1723" s="T142">пойти-EP-PST.NAR-3PL</ta>
            <ta e="T144" id="Seg_1724" s="T143">я.NOM</ta>
            <ta e="T145" id="Seg_1725" s="T144">половина-ADJZ</ta>
            <ta e="T146" id="Seg_1726" s="T145">жить-1SG.S</ta>
            <ta e="T147" id="Seg_1727" s="T146">половина-ADJZ</ta>
            <ta e="T151" id="Seg_1728" s="T150">потом</ta>
            <ta e="T152" id="Seg_1729" s="T151">жениться-1SG.S</ta>
            <ta e="T153" id="Seg_1730" s="T152">сейчас</ta>
            <ta e="T154" id="Seg_1731" s="T153">ещё</ta>
            <ta e="T155" id="Seg_1732" s="T154">тоже</ta>
            <ta e="T156" id="Seg_1733" s="T155">жить-IPFV2-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1734" s="T0">adv</ta>
            <ta e="T2" id="Seg_1735" s="T1">pers</ta>
            <ta e="T3" id="Seg_1736" s="T2">v-v:pn</ta>
            <ta e="T4" id="Seg_1737" s="T3">adj</ta>
            <ta e="T5" id="Seg_1738" s="T4">n-n:poss</ta>
            <ta e="T6" id="Seg_1739" s="T5">v-v:ins-v:pn</ta>
            <ta e="T7" id="Seg_1740" s="T6">num</ta>
            <ta e="T8" id="Seg_1741" s="T7">pers-n:case</ta>
            <ta e="T9" id="Seg_1742" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_1743" s="T9">n-n:num</ta>
            <ta e="T11" id="Seg_1744" s="T10">quant</ta>
            <ta e="T12" id="Seg_1745" s="T11">v-v:ins-v:pn</ta>
            <ta e="T13" id="Seg_1746" s="T12">nprop-n:case</ta>
            <ta e="T14" id="Seg_1747" s="T13">adv</ta>
            <ta e="T15" id="Seg_1748" s="T14">v-v:ins-v:pn</ta>
            <ta e="T16" id="Seg_1749" s="T15">num</ta>
            <ta e="T17" id="Seg_1750" s="T16">n-n:case-n:poss</ta>
            <ta e="T18" id="Seg_1751" s="T17">v-v:ins-v:pn</ta>
            <ta e="T19" id="Seg_1752" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_1753" s="T19">v-v:ins-v:pn</ta>
            <ta e="T21" id="Seg_1754" s="T20">pers</ta>
            <ta e="T22" id="Seg_1755" s="T21">v-v:ins-v:pn</ta>
            <ta e="T23" id="Seg_1756" s="T22">num-n:case</ta>
            <ta e="T24" id="Seg_1757" s="T23">n-n&gt;v-v:inf</ta>
            <ta e="T25" id="Seg_1758" s="T24">pers</ta>
            <ta e="T26" id="Seg_1759" s="T25">n-n:poss</ta>
            <ta e="T27" id="Seg_1760" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_1761" s="T27">v-v:pn</ta>
            <ta e="T29" id="Seg_1762" s="T158">n-n&gt;v-v:pn</ta>
            <ta e="T30" id="Seg_1763" s="T29">nprop-n:case</ta>
            <ta e="T31" id="Seg_1764" s="T30">num-n:case</ta>
            <ta e="T32" id="Seg_1765" s="T31">pers-n:case</ta>
            <ta e="T33" id="Seg_1766" s="T32">n-n:poss</ta>
            <ta e="T34" id="Seg_1767" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_1768" s="T34">v-v:ins-v:ins-v:pn</ta>
            <ta e="T36" id="Seg_1769" s="T35">pers-n:case</ta>
            <ta e="T37" id="Seg_1770" s="T36">adv</ta>
            <ta e="T38" id="Seg_1771" s="T37">v-v:ins-v:pn</ta>
            <ta e="T39" id="Seg_1772" s="T38">v-v:ins-v:pn</ta>
            <ta e="T40" id="Seg_1773" s="T39">v-v:pn</ta>
            <ta e="T41" id="Seg_1774" s="T40">pers</ta>
            <ta e="T42" id="Seg_1775" s="T41">n-n:case-n:poss</ta>
            <ta e="T43" id="Seg_1776" s="T42">v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_1777" s="T43">n-n&gt;adj-n&gt;v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_1778" s="T44">n-n&gt;adj-n&gt;v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_1779" s="T45">pers</ta>
            <ta e="T47" id="Seg_1780" s="T46">adv</ta>
            <ta e="T48" id="Seg_1781" s="T47">v-v:ins-v:pn</ta>
            <ta e="T148" id="Seg_1782" s="T48">v-v:ins-v:pn</ta>
            <ta e="T49" id="Seg_1783" s="T148">adv</ta>
            <ta e="T50" id="Seg_1784" s="T49">n-n&gt;adj-n&gt;v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_1785" s="T50">n-n&gt;adj-n&gt;v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_1786" s="T51">adv</ta>
            <ta e="T53" id="Seg_1787" s="T52">pers</ta>
            <ta e="T54" id="Seg_1788" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_1789" s="T54">v-v&gt;v-v:pn</ta>
            <ta e="T149" id="Seg_1790" s="T55">pers-n:case</ta>
            <ta e="T56" id="Seg_1791" s="T149">%%</ta>
            <ta e="T57" id="Seg_1792" s="T56">%%-n&gt;v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T58" id="Seg_1793" s="T57">n-n:ins-adv:case</ta>
            <ta e="T59" id="Seg_1794" s="T58">v-v:ins-v:pn</ta>
            <ta e="T60" id="Seg_1795" s="T59">adv</ta>
            <ta e="T61" id="Seg_1796" s="T60">v-v:pn</ta>
            <ta e="T62" id="Seg_1797" s="T61">n-n&gt;v-v:pn</ta>
            <ta e="T63" id="Seg_1798" s="T62">adv</ta>
            <ta e="T64" id="Seg_1799" s="T63">v-v:pn</ta>
            <ta e="T65" id="Seg_1800" s="T64">n-adv:case</ta>
            <ta e="T66" id="Seg_1801" s="T65">pers-n:case</ta>
            <ta e="T67" id="Seg_1802" s="T66">v-v:ins-v:pn</ta>
            <ta e="T68" id="Seg_1803" s="T67">adv</ta>
            <ta e="T69" id="Seg_1804" s="T68">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_1805" s="T69">adv</ta>
            <ta e="T71" id="Seg_1806" s="T70">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_1807" s="T71">interrog-n:case-clit</ta>
            <ta e="T73" id="Seg_1808" s="T72">v-v:ins-v:pn</ta>
            <ta e="T74" id="Seg_1809" s="T73">adv</ta>
            <ta e="T75" id="Seg_1810" s="T74">v-v&gt;v-v:pn</ta>
            <ta e="T76" id="Seg_1811" s="T75">adv</ta>
            <ta e="T77" id="Seg_1812" s="T76">n-n:case</ta>
            <ta e="T78" id="Seg_1813" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_1814" s="T78">v-v:ins-v:pn</ta>
            <ta e="T80" id="Seg_1815" s="T79">pers</ta>
            <ta e="T81" id="Seg_1816" s="T80">v-v:ins-v:pn</ta>
            <ta e="T82" id="Seg_1817" s="T81">adv-adv:case</ta>
            <ta e="T83" id="Seg_1818" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_1819" s="T83">n-n:case.poss</ta>
            <ta e="T85" id="Seg_1820" s="T84">interrog</ta>
            <ta e="T86" id="Seg_1821" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_1822" s="T86">v-v:pn</ta>
            <ta e="T88" id="Seg_1823" s="T87">pers-n:ins-n:case</ta>
            <ta e="T89" id="Seg_1824" s="T88">n-n:poss</ta>
            <ta e="T90" id="Seg_1825" s="T89">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_1826" s="T90">v-v:inf</ta>
            <ta e="T92" id="Seg_1827" s="T91">n-pp</ta>
            <ta e="T93" id="Seg_1828" s="T92">v-v:pn</ta>
            <ta e="T94" id="Seg_1829" s="T93">v-v&gt;v-v:pn</ta>
            <ta e="T95" id="Seg_1830" s="T94">adv</ta>
            <ta e="T97" id="Seg_1831" s="T96">v-v:ins-v:pn</ta>
            <ta e="T98" id="Seg_1832" s="T97">v-v:pn</ta>
            <ta e="T159" id="Seg_1833" s="T98">adv</ta>
            <ta e="T99" id="Seg_1834" s="T159">pers</ta>
            <ta e="T100" id="Seg_1835" s="T99">adv</ta>
            <ta e="T101" id="Seg_1836" s="T100">v-v&gt;adv</ta>
            <ta e="T102" id="Seg_1837" s="T101">v-v:ins-v:pn</ta>
            <ta e="T103" id="Seg_1838" s="T102">v-v:pn</ta>
            <ta e="T104" id="Seg_1839" s="T103">pers-n:case</ta>
            <ta e="T105" id="Seg_1840" s="T104">v-v:inf</ta>
            <ta e="T106" id="Seg_1841" s="T105">n-n:case</ta>
            <ta e="T107" id="Seg_1842" s="T106">n-n:ins-n:case</ta>
            <ta e="T108" id="Seg_1843" s="T107">v-v:pn</ta>
            <ta e="T109" id="Seg_1844" s="T108">n-n:case</ta>
            <ta e="T110" id="Seg_1845" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_1846" s="T110">v-v:ins-v:pn</ta>
            <ta e="T112" id="Seg_1847" s="T111">n-n:case</ta>
            <ta e="T113" id="Seg_1848" s="T112">v-v:pn</ta>
            <ta e="T114" id="Seg_1849" s="T113">num-n:case</ta>
            <ta e="T115" id="Seg_1850" s="T114">pers-n&gt;adj-adj&gt;adv</ta>
            <ta e="T116" id="Seg_1851" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_1852" s="T116">v-v&gt;v-v:pn</ta>
            <ta e="T118" id="Seg_1853" s="T117">num</ta>
            <ta e="T119" id="Seg_1854" s="T118">n-n:case.poss</ta>
            <ta e="T120" id="Seg_1855" s="T119">v-v:pn</ta>
            <ta e="T121" id="Seg_1856" s="T120">adv</ta>
            <ta e="T122" id="Seg_1857" s="T121">n-n:case-n&gt;adj</ta>
            <ta e="T123" id="Seg_1858" s="T122">n-n:ins-n:case</ta>
            <ta e="T124" id="Seg_1859" s="T123">v-v:pn</ta>
            <ta e="T125" id="Seg_1860" s="T124">interrog-adv:case</ta>
            <ta e="T126" id="Seg_1861" s="T125">v-v&gt;v-v:pn</ta>
            <ta e="T127" id="Seg_1862" s="T126">pers-n:case</ta>
            <ta e="T128" id="Seg_1863" s="T127">v-v:pn</ta>
            <ta e="T129" id="Seg_1864" s="T128">adv</ta>
            <ta e="T130" id="Seg_1865" s="T129">adv</ta>
            <ta e="T131" id="Seg_1866" s="T130">adv</ta>
            <ta e="T132" id="Seg_1867" s="T131">v-v:ins-v:pn</ta>
            <ta e="T133" id="Seg_1868" s="T132">n-n:case</ta>
            <ta e="T134" id="Seg_1869" s="T133">v-v&gt;v-v:pn</ta>
            <ta e="T135" id="Seg_1870" s="T134">pers-n:case</ta>
            <ta e="T136" id="Seg_1871" s="T135">n-n:case</ta>
            <ta e="T137" id="Seg_1872" s="T136">n-n:case</ta>
            <ta e="T138" id="Seg_1873" s="T137">v-v:tense-v:pn</ta>
            <ta e="T139" id="Seg_1874" s="T138">adv</ta>
            <ta e="T140" id="Seg_1875" s="T139">adv-n:case</ta>
            <ta e="T141" id="Seg_1876" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_1877" s="T141">adv</ta>
            <ta e="T143" id="Seg_1878" s="T142">v-n:ins-v:tense-v:pn</ta>
            <ta e="T144" id="Seg_1879" s="T143">pers</ta>
            <ta e="T145" id="Seg_1880" s="T144">n-n&gt;adj</ta>
            <ta e="T146" id="Seg_1881" s="T145">v-v:pn</ta>
            <ta e="T147" id="Seg_1882" s="T146">n-n&gt;adj</ta>
            <ta e="T151" id="Seg_1883" s="T150">adv</ta>
            <ta e="T152" id="Seg_1884" s="T151">v-v:pn</ta>
            <ta e="T153" id="Seg_1885" s="T152">adv</ta>
            <ta e="T154" id="Seg_1886" s="T153">adv</ta>
            <ta e="T155" id="Seg_1887" s="T154">ptcl</ta>
            <ta e="T156" id="Seg_1888" s="T155">v-v&gt;v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1889" s="T0">adv</ta>
            <ta e="T2" id="Seg_1890" s="T1">pers</ta>
            <ta e="T3" id="Seg_1891" s="T2">v</ta>
            <ta e="T4" id="Seg_1892" s="T3">adj</ta>
            <ta e="T5" id="Seg_1893" s="T4">n</ta>
            <ta e="T6" id="Seg_1894" s="T5">v</ta>
            <ta e="T7" id="Seg_1895" s="T6">num</ta>
            <ta e="T8" id="Seg_1896" s="T7">pers</ta>
            <ta e="T9" id="Seg_1897" s="T8">n</ta>
            <ta e="T10" id="Seg_1898" s="T9">n</ta>
            <ta e="T11" id="Seg_1899" s="T10">quant</ta>
            <ta e="T12" id="Seg_1900" s="T11">v</ta>
            <ta e="T13" id="Seg_1901" s="T12">nprop</ta>
            <ta e="T14" id="Seg_1902" s="T13">adv</ta>
            <ta e="T15" id="Seg_1903" s="T14">v</ta>
            <ta e="T16" id="Seg_1904" s="T15">num</ta>
            <ta e="T17" id="Seg_1905" s="T16">n</ta>
            <ta e="T18" id="Seg_1906" s="T17">v</ta>
            <ta e="T19" id="Seg_1907" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_1908" s="T19">v</ta>
            <ta e="T21" id="Seg_1909" s="T20">pers</ta>
            <ta e="T22" id="Seg_1910" s="T21">v</ta>
            <ta e="T23" id="Seg_1911" s="T22">num</ta>
            <ta e="T24" id="Seg_1912" s="T23">v</ta>
            <ta e="T25" id="Seg_1913" s="T24">pers</ta>
            <ta e="T26" id="Seg_1914" s="T25">n</ta>
            <ta e="T27" id="Seg_1915" s="T26">n</ta>
            <ta e="T28" id="Seg_1916" s="T27">v</ta>
            <ta e="T29" id="Seg_1917" s="T158">v</ta>
            <ta e="T30" id="Seg_1918" s="T29">nprop</ta>
            <ta e="T31" id="Seg_1919" s="T30">num</ta>
            <ta e="T32" id="Seg_1920" s="T31">pers</ta>
            <ta e="T33" id="Seg_1921" s="T32">n</ta>
            <ta e="T34" id="Seg_1922" s="T33">pers</ta>
            <ta e="T35" id="Seg_1923" s="T34">v</ta>
            <ta e="T36" id="Seg_1924" s="T35">pers</ta>
            <ta e="T37" id="Seg_1925" s="T36">adv</ta>
            <ta e="T38" id="Seg_1926" s="T37">v</ta>
            <ta e="T39" id="Seg_1927" s="T38">v</ta>
            <ta e="T40" id="Seg_1928" s="T39">v</ta>
            <ta e="T41" id="Seg_1929" s="T40">pers</ta>
            <ta e="T42" id="Seg_1930" s="T41">n</ta>
            <ta e="T43" id="Seg_1931" s="T42">v</ta>
            <ta e="T44" id="Seg_1932" s="T43">v</ta>
            <ta e="T45" id="Seg_1933" s="T44">v</ta>
            <ta e="T46" id="Seg_1934" s="T45">pers</ta>
            <ta e="T47" id="Seg_1935" s="T46">adv</ta>
            <ta e="T48" id="Seg_1936" s="T47">v</ta>
            <ta e="T148" id="Seg_1937" s="T48">v</ta>
            <ta e="T49" id="Seg_1938" s="T148">adv</ta>
            <ta e="T50" id="Seg_1939" s="T49">v</ta>
            <ta e="T51" id="Seg_1940" s="T50">n</ta>
            <ta e="T52" id="Seg_1941" s="T51">adv</ta>
            <ta e="T53" id="Seg_1942" s="T52">pers</ta>
            <ta e="T54" id="Seg_1943" s="T53">n</ta>
            <ta e="T55" id="Seg_1944" s="T54">v</ta>
            <ta e="T149" id="Seg_1945" s="T55">pers</ta>
            <ta e="T56" id="Seg_1946" s="T149">%%</ta>
            <ta e="T57" id="Seg_1947" s="T56">v</ta>
            <ta e="T58" id="Seg_1948" s="T57">n</ta>
            <ta e="T59" id="Seg_1949" s="T58">v</ta>
            <ta e="T60" id="Seg_1950" s="T59">adv</ta>
            <ta e="T61" id="Seg_1951" s="T60">v</ta>
            <ta e="T62" id="Seg_1952" s="T61">v</ta>
            <ta e="T63" id="Seg_1953" s="T62">adv</ta>
            <ta e="T64" id="Seg_1954" s="T63">v</ta>
            <ta e="T65" id="Seg_1955" s="T64">adv</ta>
            <ta e="T66" id="Seg_1956" s="T65">pers</ta>
            <ta e="T67" id="Seg_1957" s="T66">v</ta>
            <ta e="T68" id="Seg_1958" s="T67">adv</ta>
            <ta e="T69" id="Seg_1959" s="T68">v</ta>
            <ta e="T70" id="Seg_1960" s="T69">adv</ta>
            <ta e="T71" id="Seg_1961" s="T70">v</ta>
            <ta e="T72" id="Seg_1962" s="T71">pro</ta>
            <ta e="T73" id="Seg_1963" s="T72">v</ta>
            <ta e="T74" id="Seg_1964" s="T73">adv</ta>
            <ta e="T75" id="Seg_1965" s="T74">v</ta>
            <ta e="T76" id="Seg_1966" s="T75">adv</ta>
            <ta e="T77" id="Seg_1967" s="T76">n</ta>
            <ta e="T78" id="Seg_1968" s="T77">n</ta>
            <ta e="T79" id="Seg_1969" s="T78">v</ta>
            <ta e="T80" id="Seg_1970" s="T79">pers</ta>
            <ta e="T81" id="Seg_1971" s="T80">v</ta>
            <ta e="T82" id="Seg_1972" s="T81">adv</ta>
            <ta e="T83" id="Seg_1973" s="T82">n</ta>
            <ta e="T84" id="Seg_1974" s="T83">n</ta>
            <ta e="T85" id="Seg_1975" s="T84">interrog</ta>
            <ta e="T86" id="Seg_1976" s="T85">n</ta>
            <ta e="T87" id="Seg_1977" s="T86">v</ta>
            <ta e="T88" id="Seg_1978" s="T87">n</ta>
            <ta e="T89" id="Seg_1979" s="T88">n</ta>
            <ta e="T90" id="Seg_1980" s="T89">v</ta>
            <ta e="T91" id="Seg_1981" s="T90">v</ta>
            <ta e="T92" id="Seg_1982" s="T91">pp</ta>
            <ta e="T93" id="Seg_1983" s="T92">v</ta>
            <ta e="T94" id="Seg_1984" s="T93">v</ta>
            <ta e="T95" id="Seg_1985" s="T94">adv</ta>
            <ta e="T97" id="Seg_1986" s="T96">v</ta>
            <ta e="T98" id="Seg_1987" s="T97">v</ta>
            <ta e="T159" id="Seg_1988" s="T98">adv</ta>
            <ta e="T99" id="Seg_1989" s="T159">pers</ta>
            <ta e="T100" id="Seg_1990" s="T99">adv</ta>
            <ta e="T101" id="Seg_1991" s="T100">adv</ta>
            <ta e="T102" id="Seg_1992" s="T101">v</ta>
            <ta e="T103" id="Seg_1993" s="T102">v</ta>
            <ta e="T104" id="Seg_1994" s="T103">pers</ta>
            <ta e="T105" id="Seg_1995" s="T104">v</ta>
            <ta e="T106" id="Seg_1996" s="T105">n</ta>
            <ta e="T107" id="Seg_1997" s="T106">n</ta>
            <ta e="T108" id="Seg_1998" s="T107">v</ta>
            <ta e="T109" id="Seg_1999" s="T108">n</ta>
            <ta e="T110" id="Seg_2000" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_2001" s="T110">v</ta>
            <ta e="T112" id="Seg_2002" s="T111">v</ta>
            <ta e="T113" id="Seg_2003" s="T112">v</ta>
            <ta e="T114" id="Seg_2004" s="T113">num</ta>
            <ta e="T115" id="Seg_2005" s="T114">adv</ta>
            <ta e="T116" id="Seg_2006" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_2007" s="T116">v</ta>
            <ta e="T118" id="Seg_2008" s="T117">num</ta>
            <ta e="T119" id="Seg_2009" s="T118">n</ta>
            <ta e="T120" id="Seg_2010" s="T119">v</ta>
            <ta e="T121" id="Seg_2011" s="T120">adv</ta>
            <ta e="T122" id="Seg_2012" s="T121">n</ta>
            <ta e="T123" id="Seg_2013" s="T122">n</ta>
            <ta e="T124" id="Seg_2014" s="T123">v</ta>
            <ta e="T125" id="Seg_2015" s="T124">interrog</ta>
            <ta e="T126" id="Seg_2016" s="T125">v</ta>
            <ta e="T127" id="Seg_2017" s="T126">pers</ta>
            <ta e="T128" id="Seg_2018" s="T127">v</ta>
            <ta e="T129" id="Seg_2019" s="T128">adv</ta>
            <ta e="T130" id="Seg_2020" s="T129">adv</ta>
            <ta e="T131" id="Seg_2021" s="T130">adv</ta>
            <ta e="T132" id="Seg_2022" s="T131">v</ta>
            <ta e="T133" id="Seg_2023" s="T132">n</ta>
            <ta e="T134" id="Seg_2024" s="T133">v</ta>
            <ta e="T135" id="Seg_2025" s="T134">pers</ta>
            <ta e="T136" id="Seg_2026" s="T135">n</ta>
            <ta e="T137" id="Seg_2027" s="T136">n</ta>
            <ta e="T138" id="Seg_2028" s="T137">v</ta>
            <ta e="T139" id="Seg_2029" s="T138">adv</ta>
            <ta e="T140" id="Seg_2030" s="T139">adj</ta>
            <ta e="T141" id="Seg_2031" s="T140">pers</ta>
            <ta e="T142" id="Seg_2032" s="T141">adv</ta>
            <ta e="T143" id="Seg_2033" s="T142">v</ta>
            <ta e="T144" id="Seg_2034" s="T143">pers</ta>
            <ta e="T145" id="Seg_2035" s="T144">adj</ta>
            <ta e="T146" id="Seg_2036" s="T145">v</ta>
            <ta e="T147" id="Seg_2037" s="T146">adj</ta>
            <ta e="T151" id="Seg_2038" s="T150">adv</ta>
            <ta e="T152" id="Seg_2039" s="T151">v</ta>
            <ta e="T153" id="Seg_2040" s="T152">adv</ta>
            <ta e="T154" id="Seg_2041" s="T153">n</ta>
            <ta e="T155" id="Seg_2042" s="T154">ptcl</ta>
            <ta e="T156" id="Seg_2043" s="T155">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_2044" s="T0">adv:Time</ta>
            <ta e="T2" id="Seg_2045" s="T1">pro.h:A</ta>
            <ta e="T5" id="Seg_2046" s="T4">np.h:Th 0.1.h:Poss</ta>
            <ta e="T8" id="Seg_2047" s="T7">np.h:Poss</ta>
            <ta e="T10" id="Seg_2048" s="T9">np.h:Th</ta>
            <ta e="T11" id="Seg_2049" s="T10">np.h:P</ta>
            <ta e="T14" id="Seg_2050" s="T13">adv:G</ta>
            <ta e="T15" id="Seg_2051" s="T14">0.1.h:A</ta>
            <ta e="T17" id="Seg_2052" s="T16">np.h:Th</ta>
            <ta e="T20" id="Seg_2053" s="T19">0.3.h:P</ta>
            <ta e="T21" id="Seg_2054" s="T20">0.1.h:A</ta>
            <ta e="T25" id="Seg_2055" s="T24">pro.h:A</ta>
            <ta e="T26" id="Seg_2056" s="T25">np.h:Th 0.1.h:Poss</ta>
            <ta e="T27" id="Seg_2057" s="T26">np:L</ta>
            <ta e="T29" id="Seg_2058" s="T158">0.1.h:A</ta>
            <ta e="T30" id="Seg_2059" s="T29">np:Com</ta>
            <ta e="T32" id="Seg_2060" s="T31">pro.h:Poss</ta>
            <ta e="T33" id="Seg_2061" s="T32">np:A</ta>
            <ta e="T34" id="Seg_2062" s="T33">np:G</ta>
            <ta e="T36" id="Seg_2063" s="T35">pro.h:A</ta>
            <ta e="T37" id="Seg_2064" s="T36">adv:G</ta>
            <ta e="T39" id="Seg_2065" s="T38">0.3.h:A</ta>
            <ta e="T40" id="Seg_2066" s="T39">0.3.h:A</ta>
            <ta e="T41" id="Seg_2067" s="T40">pro.h:Poss</ta>
            <ta e="T42" id="Seg_2068" s="T41">np.h:Th</ta>
            <ta e="T44" id="Seg_2069" s="T43">0.3.h:P</ta>
            <ta e="T45" id="Seg_2070" s="T44">0.3.h:P</ta>
            <ta e="T46" id="Seg_2071" s="T45">pro.h:A</ta>
            <ta e="T47" id="Seg_2072" s="T46">adv:G</ta>
            <ta e="T148" id="Seg_2073" s="T48">0.1.h:A</ta>
            <ta e="T50" id="Seg_2074" s="T49">0.3.h:P</ta>
            <ta e="T51" id="Seg_2075" s="T50">0.3.h:P</ta>
            <ta e="T52" id="Seg_2076" s="T51">adv:Time</ta>
            <ta e="T53" id="Seg_2077" s="T52">pro.h:Th</ta>
            <ta e="T54" id="Seg_2078" s="T53">np:L</ta>
            <ta e="T58" id="Seg_2079" s="T57">adv:Time</ta>
            <ta e="T59" id="Seg_2080" s="T58">0.1.h:A</ta>
            <ta e="T60" id="Seg_2081" s="T59">adv:L</ta>
            <ta e="T61" id="Seg_2082" s="T60">0.1.h:Th</ta>
            <ta e="T62" id="Seg_2083" s="T61">0.1.h:A</ta>
            <ta e="T63" id="Seg_2084" s="T62">adv:Time</ta>
            <ta e="T64" id="Seg_2085" s="T63">0.1.h:P</ta>
            <ta e="T65" id="Seg_2086" s="T64">adv:Time</ta>
            <ta e="T66" id="Seg_2087" s="T65">pro.h:Th</ta>
            <ta e="T68" id="Seg_2088" s="T67">adv:G</ta>
            <ta e="T69" id="Seg_2089" s="T68">0.1.h:A</ta>
            <ta e="T70" id="Seg_2090" s="T69">adv:G</ta>
            <ta e="T71" id="Seg_2091" s="T70">0.1.h:A</ta>
            <ta e="T72" id="Seg_2092" s="T71">pro:L</ta>
            <ta e="T73" id="Seg_2093" s="T72">0.3.h:Th</ta>
            <ta e="T74" id="Seg_2094" s="T73">adv:Time</ta>
            <ta e="T75" id="Seg_2095" s="T74">0.1.h:E</ta>
            <ta e="T76" id="Seg_2096" s="T75">adv:L</ta>
            <ta e="T78" id="Seg_2097" s="T77">np:L</ta>
            <ta e="T79" id="Seg_2098" s="T78">0.3.h:Th</ta>
            <ta e="T80" id="Seg_2099" s="T79">pro.h:A</ta>
            <ta e="T82" id="Seg_2100" s="T81">adv:G</ta>
            <ta e="T84" id="Seg_2101" s="T83">np:G</ta>
            <ta e="T86" id="Seg_2102" s="T85">np:G</ta>
            <ta e="T87" id="Seg_2103" s="T86">0.1.h:A 0.3.h:Th</ta>
            <ta e="T88" id="Seg_2104" s="T87">pro.h:Poss</ta>
            <ta e="T89" id="Seg_2105" s="T88">np:Th </ta>
            <ta e="T90" id="Seg_2106" s="T89">0.3.h:B</ta>
            <ta e="T91" id="Seg_2107" s="T90">0.3.h:A</ta>
            <ta e="T92" id="Seg_2108" s="T91">pp:Time</ta>
            <ta e="T94" id="Seg_2109" s="T93">0.3.h:A</ta>
            <ta e="T95" id="Seg_2110" s="T94">adv:Time</ta>
            <ta e="T97" id="Seg_2111" s="T96">0.3.h:P</ta>
            <ta e="T98" id="Seg_2112" s="T97">0.3.h:P</ta>
            <ta e="T159" id="Seg_2113" s="T98">adv:Time</ta>
            <ta e="T99" id="Seg_2114" s="T159">pro.h:A</ta>
            <ta e="T100" id="Seg_2115" s="T99">adv:G</ta>
            <ta e="T101" id="Seg_2116" s="T100">0.3.h:Th</ta>
            <ta e="T102" id="Seg_2117" s="T101">0.3.h:A</ta>
            <ta e="T103" id="Seg_2118" s="T102">0.1.h:E</ta>
            <ta e="T104" id="Seg_2119" s="T103">pro.h:Th</ta>
            <ta e="T105" id="Seg_2120" s="T104">v:Th</ta>
            <ta e="T106" id="Seg_2121" s="T105">np:Ins</ta>
            <ta e="T107" id="Seg_2122" s="T106">np.h:R</ta>
            <ta e="T108" id="Seg_2123" s="T107">0.1.h:A</ta>
            <ta e="T109" id="Seg_2124" s="T108">np:A</ta>
            <ta e="T112" id="Seg_2125" s="T111">np:G</ta>
            <ta e="T113" id="Seg_2126" s="T112">0.1.h:A</ta>
            <ta e="T117" id="Seg_2127" s="T116">0.3.h:A 0.3.h:Th</ta>
            <ta e="T119" id="Seg_2128" s="T118">np:Time</ta>
            <ta e="T120" id="Seg_2129" s="T119">0.1.h:Th</ta>
            <ta e="T121" id="Seg_2130" s="T120">adv:Time</ta>
            <ta e="T123" id="Seg_2131" s="T122">np.h:R</ta>
            <ta e="T124" id="Seg_2132" s="T123">0.1.h:A</ta>
            <ta e="T125" id="Seg_2133" s="T124">pro:Time</ta>
            <ta e="T126" id="Seg_2134" s="T125">0.2.h:A 0.3.h:Th</ta>
            <ta e="T127" id="Seg_2135" s="T126">pro.h:A</ta>
            <ta e="T129" id="Seg_2136" s="T128">adv:Time</ta>
            <ta e="T130" id="Seg_2137" s="T129">adv:Time</ta>
            <ta e="T132" id="Seg_2138" s="T131">0.3.h:A</ta>
            <ta e="T133" id="Seg_2139" s="T132">np:Ins</ta>
            <ta e="T134" id="Seg_2140" s="T133">0.3.h:A</ta>
            <ta e="T135" id="Seg_2141" s="T134">pro.h:Th</ta>
            <ta e="T136" id="Seg_2142" s="T135">np:G</ta>
            <ta e="T137" id="Seg_2143" s="T136">np:L</ta>
            <ta e="T138" id="Seg_2144" s="T137">0.3.h:A 0.3.h:P</ta>
            <ta e="T139" id="Seg_2145" s="T138">adv:Time</ta>
            <ta e="T140" id="Seg_2146" s="T139">np:G</ta>
            <ta e="T143" id="Seg_2147" s="T142">0.3.h:A</ta>
            <ta e="T144" id="Seg_2148" s="T143">pro.h:Th</ta>
            <ta e="T151" id="Seg_2149" s="T150">adv:Time</ta>
            <ta e="T152" id="Seg_2150" s="T151">0.1.h:A</ta>
            <ta e="T153" id="Seg_2151" s="T152">adv:Time</ta>
            <ta e="T156" id="Seg_2152" s="T155">0.1.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_2153" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_2154" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_2155" s="T4">np.h:S</ta>
            <ta e="T6" id="Seg_2156" s="T5">v:pred</ta>
            <ta e="T10" id="Seg_2157" s="T9">np.h:S</ta>
            <ta e="T11" id="Seg_2158" s="T10">np.h:S</ta>
            <ta e="T12" id="Seg_2159" s="T11">v:pred</ta>
            <ta e="T15" id="Seg_2160" s="T14">0.1.h:S v:pred</ta>
            <ta e="T17" id="Seg_2161" s="T16">np.h:S</ta>
            <ta e="T18" id="Seg_2162" s="T17">v:pred</ta>
            <ta e="T20" id="Seg_2163" s="T19">0.3.h:S v:pred</ta>
            <ta e="T21" id="Seg_2164" s="T20">0.1.h:S</ta>
            <ta e="T22" id="Seg_2165" s="T21">v:pred</ta>
            <ta e="T24" id="Seg_2166" s="T23">s:purp</ta>
            <ta e="T25" id="Seg_2167" s="T24">pro.h:S</ta>
            <ta e="T26" id="Seg_2168" s="T25">np.h:O</ta>
            <ta e="T28" id="Seg_2169" s="T27">v:pred</ta>
            <ta e="T29" id="Seg_2170" s="T158">0.1.h:S v:pred</ta>
            <ta e="T33" id="Seg_2171" s="T32">np:S</ta>
            <ta e="T35" id="Seg_2172" s="T34">v:pred</ta>
            <ta e="T36" id="Seg_2173" s="T35">pro.h:S</ta>
            <ta e="T38" id="Seg_2174" s="T37">v:pred</ta>
            <ta e="T39" id="Seg_2175" s="T38">0.3.h:S v:pred</ta>
            <ta e="T40" id="Seg_2176" s="T39">0.3.h:S v:pred</ta>
            <ta e="T42" id="Seg_2177" s="T41">np.h:S</ta>
            <ta e="T43" id="Seg_2178" s="T42">v:pred</ta>
            <ta e="T44" id="Seg_2179" s="T43">0.3.h:S v:pred</ta>
            <ta e="T45" id="Seg_2180" s="T44">0.3.h:S v:pred</ta>
            <ta e="T46" id="Seg_2181" s="T45">pro.h:S</ta>
            <ta e="T48" id="Seg_2182" s="T47">v:pred</ta>
            <ta e="T148" id="Seg_2183" s="T48">0.1.h:S v:pred</ta>
            <ta e="T50" id="Seg_2184" s="T49">0.3.h:S v:pred</ta>
            <ta e="T51" id="Seg_2185" s="T50">0.3.h:S v:pred</ta>
            <ta e="T53" id="Seg_2186" s="T52">pro.h:S</ta>
            <ta e="T55" id="Seg_2187" s="T54">v:pred</ta>
            <ta e="T57" id="Seg_2188" s="T56">0.1.h:S v:pred</ta>
            <ta e="T59" id="Seg_2189" s="T58">0.1.h:S v:pred</ta>
            <ta e="T61" id="Seg_2190" s="T60">0.1.h:S v:pred</ta>
            <ta e="T62" id="Seg_2191" s="T61">0.1.h:S v:pred</ta>
            <ta e="T64" id="Seg_2192" s="T63">0.1.h:S v:pred</ta>
            <ta e="T66" id="Seg_2193" s="T65">pro.h:S</ta>
            <ta e="T67" id="Seg_2194" s="T66">v:pred</ta>
            <ta e="T69" id="Seg_2195" s="T68">0.1.h:S v:pred</ta>
            <ta e="T71" id="Seg_2196" s="T70">0.1.h:S v:pred</ta>
            <ta e="T73" id="Seg_2197" s="T72">0.3.h:S v:pred</ta>
            <ta e="T75" id="Seg_2198" s="T74">0.1.h:S v:pred</ta>
            <ta e="T79" id="Seg_2199" s="T78">0.3.h:S v:pred</ta>
            <ta e="T80" id="Seg_2200" s="T79">pro.h:S</ta>
            <ta e="T81" id="Seg_2201" s="T80">v:pred</ta>
            <ta e="T87" id="Seg_2202" s="T86">0.1.h:S v:pred 0.3.h:O</ta>
            <ta e="T89" id="Seg_2203" s="T88">np:O</ta>
            <ta e="T90" id="Seg_2204" s="T89">0.3.h:S v:pred</ta>
            <ta e="T91" id="Seg_2205" s="T90">0.3.h:S v:pred</ta>
            <ta e="T93" id="Seg_2206" s="T92">v:pred</ta>
            <ta e="T94" id="Seg_2207" s="T93">0.3.h:S v:pred</ta>
            <ta e="T97" id="Seg_2208" s="T96">0.3.h:S v:pred</ta>
            <ta e="T98" id="Seg_2209" s="T97">0.3.h:S v:pred</ta>
            <ta e="T99" id="Seg_2210" s="T159">pro.h:S</ta>
            <ta e="T101" id="Seg_2211" s="T100">s:temp</ta>
            <ta e="T102" id="Seg_2212" s="T101">0.3.h:S v:pred</ta>
            <ta e="T103" id="Seg_2213" s="T102">0.1.h:S v:pred</ta>
            <ta e="T105" id="Seg_2214" s="T104">v:O</ta>
            <ta e="T108" id="Seg_2215" s="T107">0.1.h:S v:pred</ta>
            <ta e="T109" id="Seg_2216" s="T108">np:S</ta>
            <ta e="T111" id="Seg_2217" s="T110">v:pred</ta>
            <ta e="T113" id="Seg_2218" s="T112">0.1.h:S v:pred</ta>
            <ta e="T117" id="Seg_2219" s="T116">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T120" id="Seg_2220" s="T119">0.1.h:S v:pred</ta>
            <ta e="T124" id="Seg_2221" s="T123">0.1.h:S v:pred</ta>
            <ta e="T126" id="Seg_2222" s="T125">0.2.h:S v:pred 0.3.h:O</ta>
            <ta e="T127" id="Seg_2223" s="T126">pro.h:S</ta>
            <ta e="T128" id="Seg_2224" s="T127">v:pred</ta>
            <ta e="T132" id="Seg_2225" s="T131">0.3.h:S v:pred</ta>
            <ta e="T134" id="Seg_2226" s="T133">0.3.h:S v:pred</ta>
            <ta e="T135" id="Seg_2227" s="T134">pro.h:O</ta>
            <ta e="T138" id="Seg_2228" s="T137">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T143" id="Seg_2229" s="T142">0.3.h:S v:pred</ta>
            <ta e="T144" id="Seg_2230" s="T143">pro.h:S</ta>
            <ta e="T146" id="Seg_2231" s="T145">v:pred</ta>
            <ta e="T152" id="Seg_2232" s="T151">0.1.h:S v:pred</ta>
            <ta e="T156" id="Seg_2233" s="T155">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T11" id="Seg_2234" s="T10">RUS:core</ta>
            <ta e="T52" id="Seg_2235" s="T51">RUS:core</ta>
            <ta e="T60" id="Seg_2236" s="T59">RUS:core</ta>
            <ta e="T63" id="Seg_2237" s="T62">RUS:core</ta>
            <ta e="T74" id="Seg_2238" s="T73">RUS:core</ta>
            <ta e="T76" id="Seg_2239" s="T75">RUS:core</ta>
            <ta e="T95" id="Seg_2240" s="T94">RUS:core</ta>
            <ta e="T159" id="Seg_2241" s="T98">RUS:core</ta>
            <ta e="T106" id="Seg_2242" s="T105">RUS:cult</ta>
            <ta e="T107" id="Seg_2243" s="T106">RUS:cult</ta>
            <ta e="T109" id="Seg_2244" s="T108">RUS:cult</ta>
            <ta e="T112" id="Seg_2245" s="T111">RUS:cult</ta>
            <ta e="T121" id="Seg_2246" s="T120">RUS:core</ta>
            <ta e="T123" id="Seg_2247" s="T122">RUS:cult</ta>
            <ta e="T129" id="Seg_2248" s="T128">RUS:core</ta>
            <ta e="T130" id="Seg_2249" s="T129">RUS:core</ta>
            <ta e="T133" id="Seg_2250" s="T132">RUS:cult</ta>
            <ta e="T138" id="Seg_2251" s="T137">RUS:cult</ta>
            <ta e="T139" id="Seg_2252" s="T138">RUS:core</ta>
            <ta e="T142" id="Seg_2253" s="T141">RUS:disc</ta>
            <ta e="T151" id="Seg_2254" s="T150">RUS:core</ta>
            <ta e="T153" id="Seg_2255" s="T152">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T113" id="Seg_2256" s="T111">RUS:calq</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_2257" s="T0">Когда-то я женился.</ta>
            <ta e="T6" id="Seg_2258" s="T3">У меня жена красивая была.</ta>
            <ta e="T10" id="Seg_2259" s="T6">Семь дочерей у нас было.</ta>
            <ta e="T12" id="Seg_2260" s="T10">Все умерли.</ta>
            <ta e="T15" id="Seg_2261" s="T12">В Напас, сюда, приехали.</ta>
            <ta e="T18" id="Seg_2262" s="T15">Одна дочь была.</ta>
            <ta e="T20" id="Seg_2263" s="T18">Тоже умерла.</ta>
            <ta e="T24" id="Seg_2264" s="T20">Мы поехали вдвоём белковать.</ta>
            <ta e="T28" id="Seg_2265" s="T24">Я жену дома оставил.</ta>
            <ta e="T31" id="Seg_2266" s="T28">Охотимся на белок с Санькой вдвоём.</ta>
            <ta e="T35" id="Seg_2267" s="T31">У него собака домой убежала.</ta>
            <ta e="T38" id="Seg_2268" s="T35">Он домой отправился.</ta>
            <ta e="T45" id="Seg_2269" s="T38">Пришёл, говорит: «Твоя баба заболела, оглохла, ослепла».</ta>
            <ta e="T48" id="Seg_2270" s="T45">Я обратно вернулся.</ta>
            <ta e="T51" id="Seg_2271" s="T48">Я пришёл, правда, оглохла, ослепла.</ta>
            <ta e="T57" id="Seg_2272" s="T51">Потом я дома пожил, (?).</ta>
            <ta e="T62" id="Seg_2273" s="T57">Летом поехал, там живу, рыбачу.</ta>
            <ta e="T67" id="Seg_2274" s="T62">Потом проснулся ночью — её нет.</ta>
            <ta e="T73" id="Seg_2275" s="T67">К реке побежал, на гору побежал — нигде нет.</ta>
            <ta e="T79" id="Seg_2276" s="T73">Потом слышу: она в тайге очутилась.</ta>
            <ta e="T84" id="Seg_2277" s="T79">Я пошёл туда, в чащу леса.</ta>
            <ta e="T87" id="Seg_2278" s="T84">Кое-как в шалаш её притащил.</ta>
            <ta e="T90" id="Seg_2279" s="T87">У неё память потерялась.</ta>
            <ta e="T91" id="Seg_2280" s="T90">Что делать? </ta>
            <ta e="T94" id="Seg_2281" s="T91">(Целую ночь?) [она] сидела, (стала ждать?).</ta>
            <ta e="T97" id="Seg_2282" s="T94">Потом (?) уснула.</ta>
            <ta e="T102" id="Seg_2283" s="T97">Проснулась, потом я домой, посидев, отправился.</ta>
            <ta e="T106" id="Seg_2284" s="T102">Хочу её отправить самолётом.</ta>
            <ta e="T111" id="Seg_2285" s="T106">Врачу говорю: «Самолёт не прилетел».</ta>
            <ta e="T117" id="Seg_2286" s="T111">В больницу легли вдвоём, без меня не берут.</ta>
            <ta e="T120" id="Seg_2287" s="T117">Два месяца лежим.</ta>
            <ta e="T126" id="Seg_2288" s="T120">Потом городскому врачу говорю: «Когда увезёшь её?»</ta>
            <ta e="T129" id="Seg_2289" s="T126">Он говорит: «Потом».</ta>
            <ta e="T136" id="Seg_2290" s="T129">Потом (?) приехал, самолётом её увезли в город</ta>
            <ta e="T138" id="Seg_2291" s="T136">В городе лечили.</ta>
            <ta e="T143" id="Seg_2292" s="T138">Потом в заречный дом, наверно, увезли.</ta>
            <ta e="T146" id="Seg_2293" s="T143">Я (один?) живу.</ta>
            <ta e="T150" id="Seg_2294" s="T146">[?]</ta>
            <ta e="T152" id="Seg_2295" s="T150">Потом женился.</ta>
            <ta e="T156" id="Seg_2296" s="T152">Сейчас ещё так живу.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_2297" s="T0">I got married earlier.</ta>
            <ta e="T6" id="Seg_2298" s="T3">I had a beautiful wife.</ta>
            <ta e="T10" id="Seg_2299" s="T6">We had seven daughters.</ta>
            <ta e="T12" id="Seg_2300" s="T10">They all died.</ta>
            <ta e="T15" id="Seg_2301" s="T12">We came here, to Napas.</ta>
            <ta e="T18" id="Seg_2302" s="T15">I had one daughter.</ta>
            <ta e="T20" id="Seg_2303" s="T18">She also died.</ta>
            <ta e="T24" id="Seg_2304" s="T20">Now, two of us went to hunt for squirrels.</ta>
            <ta e="T28" id="Seg_2305" s="T24">I left my wife back home.</ta>
            <ta e="T31" id="Seg_2306" s="T28">Sanja and I hunted squirrels together.</ta>
            <ta e="T35" id="Seg_2307" s="T31">His dog ran away, ran home.</ta>
            <ta e="T38" id="Seg_2308" s="T35">He headed home.</ta>
            <ta e="T45" id="Seg_2309" s="T38">He came and said: “Your old woman was ill, got deaf, got blind.”</ta>
            <ta e="T48" id="Seg_2310" s="T45">I returned.</ta>
            <ta e="T51" id="Seg_2311" s="T48">I came, indeed, she got deaf, she got blind.</ta>
            <ta e="T57" id="Seg_2312" s="T51">Then I lived at home, (?).</ta>
            <ta e="T62" id="Seg_2313" s="T57">In the summer I went and lived there, fished.</ta>
            <ta e="T67" id="Seg_2314" s="T62">Then I woke up at night – and she is gone.</ta>
            <ta e="T73" id="Seg_2315" s="T67">I ran to the river, I ran to the hill – she is nowhere.</ta>
            <ta e="T79" id="Seg_2316" s="T73">Then I heard: she turned up in the taiga.</ta>
            <ta e="T84" id="Seg_2317" s="T79">I went there, to the deepest part of the forest.</ta>
            <ta e="T87" id="Seg_2318" s="T84">I was barely able to bring her to the hut.</ta>
            <ta e="T90" id="Seg_2319" s="T87">She lost her memory.</ta>
            <ta e="T91" id="Seg_2320" s="T90">What to do? </ta>
            <ta e="T94" id="Seg_2321" s="T91">(All night long?) [she] sat, (began to wait?).</ta>
            <ta e="T97" id="Seg_2322" s="T94">Then she fell asleep (?).</ta>
            <ta e="T102" id="Seg_2323" s="T97">She woke up, then I sat for a while and went home.</ta>
            <ta e="T106" id="Seg_2324" s="T102">I want to send her with an airplane.</ta>
            <ta e="T111" id="Seg_2325" s="T106">I told the doctor: “The plane didn’t come.”</ta>
            <ta e="T117" id="Seg_2326" s="T111">We stayed at the hospital together, they didn’t take her without me.</ta>
            <ta e="T120" id="Seg_2327" s="T117">We have been here for two months.</ta>
            <ta e="T126" id="Seg_2328" s="T120">Then I said to the town doctor: “When will you take her away?”</ta>
            <ta e="T129" id="Seg_2329" s="T126">He said: “Later”.</ta>
            <ta e="T136" id="Seg_2330" s="T129">Then (?) he came, and she was taken to the city by plane.</ta>
            <ta e="T138" id="Seg_2331" s="T136">They treated her in the city.</ta>
            <ta e="T143" id="Seg_2332" s="T138">Then she was probably taken to the house beyond the river.</ta>
            <ta e="T146" id="Seg_2333" s="T143">I live (alone?).</ta>
            <ta e="T150" id="Seg_2334" s="T146">[?]</ta>
            <ta e="T152" id="Seg_2335" s="T150">Then I got married.</ta>
            <ta e="T156" id="Seg_2336" s="T152">I still live like this.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_2337" s="T0">Früher habe ich geheiratet.</ta>
            <ta e="T6" id="Seg_2338" s="T3">Ich hatte eine schöne Frau.</ta>
            <ta e="T10" id="Seg_2339" s="T6">Wir hatten sieben Töchter.</ta>
            <ta e="T12" id="Seg_2340" s="T10">Sie starben alle.</ta>
            <ta e="T15" id="Seg_2341" s="T12">Wir kamen hierher, nach Napas.</ta>
            <ta e="T18" id="Seg_2342" s="T15">Ich hatte eine Tochter.</ta>
            <ta e="T20" id="Seg_2343" s="T18">Sie starb auch.</ta>
            <ta e="T24" id="Seg_2344" s="T20">Nun, zwei von uns gingen Eichhörnchen jagen.</ta>
            <ta e="T28" id="Seg_2345" s="T24">Ich ließ meine Frau zu Hause zurück.</ta>
            <ta e="T31" id="Seg_2346" s="T28">Sanja und ich jagten zu zweit.</ta>
            <ta e="T35" id="Seg_2347" s="T31">Sein Hund lief weg, lief nach Hause.</ta>
            <ta e="T38" id="Seg_2348" s="T35">Er machte sich auf dem Weg nach Hause.</ta>
            <ta e="T45" id="Seg_2349" s="T38">Er kam und sagte: „Deine alte Frau war krank, wurde taub, erblindete.“</ta>
            <ta e="T48" id="Seg_2350" s="T45">Ich ging zurück.</ta>
            <ta e="T51" id="Seg_2351" s="T48">Ich kam, wirklich, sie wurde taub und erblindete.</ta>
            <ta e="T57" id="Seg_2352" s="T51">Dann lebte ich zu Hause, (?).</ta>
            <ta e="T62" id="Seg_2353" s="T57">Im Sommer ging ich und wohnte dort, fischte.</ta>
            <ta e="T67" id="Seg_2354" s="T62">Dann wachte ich auf nachts – und sie ist weg.</ta>
            <ta e="T73" id="Seg_2355" s="T67">Ich lief zum Fluss, ich lief zum Hügel – sie ist nirgendwo.</ta>
            <ta e="T79" id="Seg_2356" s="T73">Dann hörte ich: sie ist auf der Taiga aufgetaucht.</ta>
            <ta e="T84" id="Seg_2357" s="T79">Ich ging dorthin, in den tiefsten Wald.</ta>
            <ta e="T87" id="Seg_2358" s="T84">Ich konnte sie gerade noch zur Hütte bringen.</ta>
            <ta e="T90" id="Seg_2359" s="T87">Sie verlor ihr Gedächtnis.</ta>
            <ta e="T91" id="Seg_2360" s="T90">Was tun? </ta>
            <ta e="T94" id="Seg_2361" s="T91">(Die ganze Nacht?) saß [sie], (wartete?).</ta>
            <ta e="T97" id="Seg_2362" s="T94">Dann schlief sie ein (?).</ta>
            <ta e="T102" id="Seg_2363" s="T97">Sie wachte auf, dann saß ich ein bisschen und ging nach Hause.</ta>
            <ta e="T106" id="Seg_2364" s="T102">Ich will sie mit einem Flugzeug schicken.</ta>
            <ta e="T111" id="Seg_2365" s="T106">Ich erzählte dem Arzt: „Das Flugzeug kam nicht.“</ta>
            <ta e="T117" id="Seg_2366" s="T111">Wir blieben im Krankenhaus zusammen, sie nahmen sie ohne mich nicht.</ta>
            <ta e="T120" id="Seg_2367" s="T117">Wir sind seit zwei Monaten hier.</ta>
            <ta e="T126" id="Seg_2368" s="T120">Dann sagte ich zum Stadtarzt: „Wann nimmst du sie?“</ta>
            <ta e="T129" id="Seg_2369" s="T126">Er sagte: „Später.“</ta>
            <ta e="T136" id="Seg_2370" s="T129">Dann (?) kam er, und sie wurde mit dem Flugzeug zur Stadt gebracht.</ta>
            <ta e="T138" id="Seg_2371" s="T136">Sie behandelten sie in der Stadt.</ta>
            <ta e="T143" id="Seg_2372" s="T138">Dann wurde sie wahrscheinlich zum Haus hinter dem Fluss gebracht.</ta>
            <ta e="T146" id="Seg_2373" s="T143">Ich lebe (alleine?).</ta>
            <ta e="T150" id="Seg_2374" s="T146">[?]</ta>
            <ta e="T152" id="Seg_2375" s="T150">Dann habe ich geheiratet.</ta>
            <ta e="T156" id="Seg_2376" s="T152">Ich lebe immer noch so.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_2377" s="T0">Раньше я женился.</ta>
            <ta e="T6" id="Seg_2378" s="T3">У меня жена была.</ta>
            <ta e="T10" id="Seg_2379" s="T6">Семь детей у меня было.</ta>
            <ta e="T12" id="Seg_2380" s="T10">Все умерли.</ta>
            <ta e="T15" id="Seg_2381" s="T12">В Напас, сюда, приехали.</ta>
            <ta e="T18" id="Seg_2382" s="T15">Одна дочь была.</ta>
            <ta e="T20" id="Seg_2383" s="T18">Тоже умерла.</ta>
            <ta e="T24" id="Seg_2384" s="T20">Это, мы поехали вдвоём белковать.</ta>
            <ta e="T28" id="Seg_2385" s="T24">Я жену дома оставил.</ta>
            <ta e="T31" id="Seg_2386" s="T28">Охотимся с Санькой вдвоём.</ta>
            <ta e="T35" id="Seg_2387" s="T31">У него собака домой убежала.</ta>
            <ta e="T38" id="Seg_2388" s="T35">Он домой отправился.</ta>
            <ta e="T45" id="Seg_2389" s="T38">Пришёл, говорит: «Твоя баба заболела, привыкла, ослепла».</ta>
            <ta e="T48" id="Seg_2390" s="T45">Я обратно вернулся.</ta>
            <ta e="T51" id="Seg_2391" s="T48">Правда, привыкла, ослепла.</ta>
            <ta e="T57" id="Seg_2392" s="T51">Потом я дома пожил, водку попил.</ta>
            <ta e="T62" id="Seg_2393" s="T57">Летом поехал, там живу, рыбачу.</ta>
            <ta e="T67" id="Seg_2394" s="T62">Потом проснулся ночью — её нет.</ta>
            <ta e="T73" id="Seg_2395" s="T67">К реке побежал, на гору побежал — нигде нет.</ta>
            <ta e="T79" id="Seg_2396" s="T73">Потом слышу: она в тайге очутилась.</ta>
            <ta e="T84" id="Seg_2397" s="T79">Я пошёл туда, в чащу леса.</ta>
            <ta e="T87" id="Seg_2398" s="T84">Кое-как в шалаш притащил.</ta>
            <ta e="T90" id="Seg_2399" s="T87">У неё память потерялась.</ta>
            <ta e="T94" id="Seg_2400" s="T91">Старуха долго сидела, ждала.</ta>
            <ta e="T97" id="Seg_2401" s="T94">Потом здесь уснула.</ta>
            <ta e="T102" id="Seg_2402" s="T97">Проснулась, потом домой, посидев, отправились.</ta>
            <ta e="T106" id="Seg_2403" s="T102">Хочу её отправить самолётом.</ta>
            <ta e="T111" id="Seg_2404" s="T106">Врачу говорю: «Самолёт не прилетел».</ta>
            <ta e="T117" id="Seg_2405" s="T111">В больницу отправились вдвоём, без меня не берут.</ta>
            <ta e="T120" id="Seg_2406" s="T117">Два месяца лежим.</ta>
            <ta e="T126" id="Seg_2407" s="T120">Потом городскому врачу говорю: «Когда увезёте?»</ta>
            <ta e="T129" id="Seg_2408" s="T126">Он говорит: «Потом».</ta>
            <ta e="T136" id="Seg_2409" s="T129">Потом, наконец, приехал, самолётом её увезли в город</ta>
            <ta e="T138" id="Seg_2410" s="T136">В городе лечили.</ta>
            <ta e="T143" id="Seg_2411" s="T138">Потом в заречный дом, наверно, увезли.</ta>
            <ta e="T146" id="Seg_2412" s="T143">Я один живу.</ta>
            <ta e="T150" id="Seg_2413" s="T146">Одному жить плохо.</ta>
            <ta e="T152" id="Seg_2414" s="T150">Потом женился.</ta>
            <ta e="T156" id="Seg_2415" s="T152">Сейчас ещё так живу.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T31" id="Seg_2416" s="T28">[AAV:] Kanʼkahe ?</ta>
            <ta e="T73" id="Seg_2417" s="T67">[AAV:] kuralgwak?</ta>
            <ta e="T79" id="Seg_2418" s="T73">[AAV:] bunǯoɣən?</ta>
            <ta e="T94" id="Seg_2419" s="T91">[AAV:] Tentative transcription and analysis.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T158" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T148" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T149" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T159" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
