<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KFN_196X_BearHunting2_nar</transcription-name>
         <referenced-file url="KFN_196X_BearHunting2_nar.wav" />
         <referenced-file url="KFN_196X_BearHunting2_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KFN_196X_BearHunting2_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">92</ud-information>
            <ud-information attribute-name="# HIAT:w">67</ud-information>
            <ud-information attribute-name="# e">67</ud-information>
            <ud-information attribute-name="# HIAT:u">18</ud-information>
            <ud-information attribute-name="# sc">22</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KFN">
            <abbreviation>KFN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="5.1330077471124875" />
         <tli id="T2" time="6.484588683121652" />
         <tli id="T3" time="7.836169619130817" />
         <tli id="T4" time="9.187750555139981" />
         <tli id="T5" time="10.539331491149145" />
         <tli id="T6" time="11.609263625449865" />
         <tli id="T7" time="12.679195759750584" />
         <tli id="T8" time="13.749127894051306" />
         <tli id="T9" time="14.819060028352025" />
         <tli id="T10" time="15.5" />
         <tli id="T11" time="16.345629864830933" />
         <tli id="T12" time="16.852264395714766" />
         <tli id="T13" time="17.50555629080181" />
         <tli id="T14" time="18.338836769229157" />
         <tli id="T15" time="19.372104562479077" />
         <tli id="T16" time="19.825409142743556" />
         <tli id="T17" time="20.285379966835453" />
         <tli id="T18" time="20.81201322920154" />
         <tli id="T19" time="23.00520744842233" />
         <tli id="T20" time="23.585170661407766" />
         <tli id="T21" time="24.23846255649481" />
         <tli id="T22" time="24.551776016383492" />
         <tli id="T23" time="25.078409278749582" />
         <tli id="T24" time="25.665038735562437" />
         <tli id="T25" time="26.271666923857552" />
         <tli id="T26" time="26.964956281909107" />
         <tli id="T27" time="27.49825578810261" />
         <tli id="T28" time="28.031555294296115" />
         <tli id="T29" time="28.524857337525106" />
         <tli id="T30" time="29.378136547434714" />
         <tli id="T31" time="30.15808707524272" />
         <tli id="T32" time="30.758049019710413" />
         <tli id="T33" time="31.224686087629728" />
         <tli id="T34" time="32.264620124707065" />
         <tli id="T35" time="32.657928510524776" />
         <tli id="T36" time="34.05783971428273" />
         <tli id="T37" time="35.157769945806834" />
         <tli id="T38" time="35.67773696434549" />
         <tli id="T39" time="36.810998415006694" />
         <tli id="T40" time="37.604281430469534" />
         <tli id="T41" time="37.89759615887597" />
         <tli id="T42" time="38.41089693358721" />
         <tli id="T43" time="38.85086902619686" />
         <tli id="T44" time="39.510827165111316" />
         <tli id="T45" time="40.17078530402578" />
         <tli id="T46" time="40.557427446016064" />
         <tli id="T47" time="41.11739192751924" />
         <tli id="T48" time="41.604027726920826" />
         <tli id="T49" time="41.96400489360144" />
         <tli id="T50" time="42.483971912140106" />
         <tli id="T51" time="43.117265075744896" />
         <tli id="T52" time="43.65723082576582" />
         <tli id="T53" time="44.183864088131905" />
         <tli id="T54" time="45.11713822397054" />
         <tli id="T55" time="45.5637765604076" />
         <tli id="T56" time="45.95708494622531" />
         <tli id="T57" time="46.977020251820385" />
         <tli id="T58" time="47.39699361294778" />
         <tli id="T59" time="47.59698092777034" />
         <tli id="T60" time="47.86363068086709" />
         <tli id="T61" time="48.610249989538" />
         <tli id="T62" time="49.01689086301055" />
         <tli id="T63" time="49.443530467965346" />
         <tli id="T64" time="50.050158656260464" />
         <tli id="T65" time="50.350139628494304" />
         <tli id="T66" time="50.736781770484605" />
         <tli id="T67" time="51.66338966249582" />
         <tli id="T68" time="51.98336936621192" />
         <tli id="T69" time="52.29668282610061" />
         <tli id="T70" time="53.023303403289255" />
         <tli id="T71" time="53.36994808231503" />
         <tli id="T72" time="54.056571196539174" />
         <tli id="T73" time="54.80985674903749" />
         <tli id="T74" time="55.463148644124544" />
         <tli id="T75" time="55.74979712870355" />
         <tli id="T76" time="56.663072533059925" />
         <tli id="T77" time="57.23636950221795" />
         <tli id="T78" time="58.722941875732346" />
         <tli id="T0" time="59.736" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KFN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T9" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">čaǯeqɨɣɨt</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">nälgut</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">maǯöɣət</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">kwajambat</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">kana</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">muːdelba</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">korqot</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">pedɛnd</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T14" id="Seg_31" n="sc" s="T10">
               <ts e="T14" id="Seg_33" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_35" n="HIAT:w" s="T10">čwesse</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_38" n="HIAT:w" s="T11">töwat</ts>
                  <nts id="Seg_39" n="HIAT:ip">,</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_42" n="HIAT:w" s="T12">mašək</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_45" n="HIAT:w" s="T13">qwerat</ts>
                  <nts id="Seg_46" n="HIAT:ip">.</nts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T21" id="Seg_48" n="sc" s="T15">
               <ts e="T21" id="Seg_50" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_52" n="HIAT:w" s="T15">mi</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_55" n="HIAT:w" s="T16">towarɨšej</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_58" n="HIAT:w" s="T17">načʼid</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_61" n="HIAT:w" s="T18">kwɛnnaj</ts>
                  <nts id="Seg_62" n="HIAT:ip">,</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_65" n="HIAT:w" s="T19">korɣot</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_68" n="HIAT:w" s="T20">pedend</ts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T26" id="Seg_71" n="sc" s="T22">
               <ts e="T26" id="Seg_73" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_75" n="HIAT:w" s="T22">korqot</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_78" n="HIAT:w" s="T23">ped</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_81" n="HIAT:w" s="T24">kenalǯ</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_84" n="HIAT:w" s="T25">meːmba</ts>
                  <nts id="Seg_85" n="HIAT:ip">.</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T30" id="Seg_87" n="sc" s="T27">
               <ts e="T30" id="Seg_89" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_91" n="HIAT:w" s="T27">peden</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_94" n="HIAT:w" s="T28">mogout</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_97" n="HIAT:w" s="T29">tölaj</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T39" id="Seg_100" n="sc" s="T31">
               <ts e="T34" id="Seg_102" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_104" n="HIAT:w" s="T31">qorqə</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_107" n="HIAT:w" s="T32">ponɛː</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_110" n="HIAT:w" s="T33">paxtə</ts>
                  <nts id="Seg_111" n="HIAT:ip">.</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_114" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_116" n="HIAT:w" s="T34">mat</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_119" n="HIAT:w" s="T35">čʼaǯap</ts>
                  <nts id="Seg_120" n="HIAT:ip">,</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_123" n="HIAT:w" s="T36">ranʼnʼenap</ts>
                  <nts id="Seg_124" n="HIAT:ip">,</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_127" n="HIAT:w" s="T37">kaʒek</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_130" n="HIAT:w" s="T38">torčaɣənna</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T44" id="Seg_133" n="sc" s="T40">
               <ts e="T44" id="Seg_135" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_137" n="HIAT:w" s="T40">pir</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_140" n="HIAT:w" s="T41">tawarišə</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_143" n="HIAT:w" s="T42">šɨtəqut</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_146" n="HIAT:w" s="T43">nʼodaj</ts>
                  <nts id="Seg_147" n="HIAT:ip">.</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T51" id="Seg_149" n="sc" s="T45">
               <ts e="T51" id="Seg_151" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_153" n="HIAT:w" s="T45">tapə</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_156" n="HIAT:w" s="T46">čʼelɨtɨ</ts>
                  <nts id="Seg_157" n="HIAT:ip">,</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_160" n="HIAT:w" s="T47">čʼelɨtɨ</ts>
                  <nts id="Seg_161" n="HIAT:ip">,</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_164" n="HIAT:w" s="T48">vesʼ</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_167" n="HIAT:w" s="T49">mimak</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_170" n="HIAT:w" s="T50">čʼaǯɨt</ts>
                  <nts id="Seg_171" n="HIAT:ip">.</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T72" id="Seg_173" n="sc" s="T52">
               <ts e="T54" id="Seg_175" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_177" n="HIAT:w" s="T52">potom</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_180" n="HIAT:w" s="T53">kaʒanna</ts>
                  <nts id="Seg_181" n="HIAT:ip">.</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_184" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_186" n="HIAT:w" s="T54">tap</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_189" n="HIAT:w" s="T55">qorqo</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_192" n="HIAT:w" s="T56">nʼoːtɨt</ts>
                  <nts id="Seg_193" n="HIAT:ip">.</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_196" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_198" n="HIAT:w" s="T57">man</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_201" n="HIAT:w" s="T58">naj</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_204" n="HIAT:w" s="T59">načʼidə</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_207" n="HIAT:w" s="T60">kaʒennak</ts>
                  <nts id="Seg_208" n="HIAT:ip">.</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_211" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_213" n="HIAT:w" s="T61">qorqo</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_216" n="HIAT:w" s="T62">qön</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_219" n="HIAT:w" s="T63">kurdak</ts>
                  <nts id="Seg_220" n="HIAT:ip">.</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_223" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_225" n="HIAT:w" s="T64">qorqə</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_228" n="HIAT:w" s="T65">meka</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_231" n="HIAT:w" s="T66">manǯa</ts>
                  <nts id="Seg_232" n="HIAT:ip">.</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_235" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_237" n="HIAT:w" s="T67">mat</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_240" n="HIAT:w" s="T68">tap</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_243" n="HIAT:w" s="T69">čʼaǯap</ts>
                  <nts id="Seg_244" n="HIAT:ip">,</nts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_247" n="HIAT:w" s="T70">elʼlʼe</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_250" n="HIAT:w" s="T71">alʼčʼa</ts>
                  <nts id="Seg_251" n="HIAT:ip">.</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T76" id="Seg_253" n="sc" s="T73">
               <ts e="T76" id="Seg_255" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_257" n="HIAT:w" s="T73">kɨbana</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_260" n="HIAT:w" s="T74">tap</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_263" n="HIAT:w" s="T75">orannɨt</ts>
                  <nts id="Seg_264" n="HIAT:ip">.</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T78" id="Seg_266" n="sc" s="T77">
               <ts e="T78" id="Seg_268" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_270" n="HIAT:w" s="T77">Всё</ts>
                  <nts id="Seg_271" n="HIAT:ip">.</nts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T9" id="Seg_273" n="sc" s="T1">
               <ts e="T2" id="Seg_275" n="e" s="T1">čaǯeqɨɣɨt </ts>
               <ts e="T3" id="Seg_277" n="e" s="T2">nälgut </ts>
               <ts e="T4" id="Seg_279" n="e" s="T3">maǯöɣət </ts>
               <ts e="T5" id="Seg_281" n="e" s="T4">kwajambat. </ts>
               <ts e="T6" id="Seg_283" n="e" s="T5">kana </ts>
               <ts e="T7" id="Seg_285" n="e" s="T6">muːdelba </ts>
               <ts e="T8" id="Seg_287" n="e" s="T7">korqot </ts>
               <ts e="T9" id="Seg_289" n="e" s="T8">pedɛnd. </ts>
            </ts>
            <ts e="T14" id="Seg_290" n="sc" s="T10">
               <ts e="T11" id="Seg_292" n="e" s="T10">čwesse </ts>
               <ts e="T12" id="Seg_294" n="e" s="T11">töwat, </ts>
               <ts e="T13" id="Seg_296" n="e" s="T12">mašək </ts>
               <ts e="T14" id="Seg_298" n="e" s="T13">qwerat. </ts>
            </ts>
            <ts e="T21" id="Seg_299" n="sc" s="T15">
               <ts e="T16" id="Seg_301" n="e" s="T15">mi </ts>
               <ts e="T17" id="Seg_303" n="e" s="T16">towarɨšej </ts>
               <ts e="T18" id="Seg_305" n="e" s="T17">načʼid </ts>
               <ts e="T19" id="Seg_307" n="e" s="T18">kwɛnnaj, </ts>
               <ts e="T20" id="Seg_309" n="e" s="T19">korɣot </ts>
               <ts e="T21" id="Seg_311" n="e" s="T20">pedend. </ts>
            </ts>
            <ts e="T26" id="Seg_312" n="sc" s="T22">
               <ts e="T23" id="Seg_314" n="e" s="T22">korqot </ts>
               <ts e="T24" id="Seg_316" n="e" s="T23">ped </ts>
               <ts e="T25" id="Seg_318" n="e" s="T24">kenalǯ </ts>
               <ts e="T26" id="Seg_320" n="e" s="T25">meːmba. </ts>
            </ts>
            <ts e="T30" id="Seg_321" n="sc" s="T27">
               <ts e="T28" id="Seg_323" n="e" s="T27">peden </ts>
               <ts e="T29" id="Seg_325" n="e" s="T28">mogout </ts>
               <ts e="T30" id="Seg_327" n="e" s="T29">tölaj. </ts>
            </ts>
            <ts e="T39" id="Seg_328" n="sc" s="T31">
               <ts e="T32" id="Seg_330" n="e" s="T31">qorqə </ts>
               <ts e="T33" id="Seg_332" n="e" s="T32">ponɛː </ts>
               <ts e="T34" id="Seg_334" n="e" s="T33">paxtə. </ts>
               <ts e="T35" id="Seg_336" n="e" s="T34">mat </ts>
               <ts e="T36" id="Seg_338" n="e" s="T35">čʼaǯap, </ts>
               <ts e="T37" id="Seg_340" n="e" s="T36">ranʼnʼenap, </ts>
               <ts e="T38" id="Seg_342" n="e" s="T37">kaʒek </ts>
               <ts e="T39" id="Seg_344" n="e" s="T38">torčaɣənna. </ts>
            </ts>
            <ts e="T44" id="Seg_345" n="sc" s="T40">
               <ts e="T41" id="Seg_347" n="e" s="T40">pir </ts>
               <ts e="T42" id="Seg_349" n="e" s="T41">tawarišə </ts>
               <ts e="T43" id="Seg_351" n="e" s="T42">šɨtəqut </ts>
               <ts e="T44" id="Seg_353" n="e" s="T43">nʼodaj. </ts>
            </ts>
            <ts e="T51" id="Seg_354" n="sc" s="T45">
               <ts e="T46" id="Seg_356" n="e" s="T45">tapə </ts>
               <ts e="T47" id="Seg_358" n="e" s="T46">čʼelɨtɨ, </ts>
               <ts e="T48" id="Seg_360" n="e" s="T47">čʼelɨtɨ, </ts>
               <ts e="T49" id="Seg_362" n="e" s="T48">vesʼ </ts>
               <ts e="T50" id="Seg_364" n="e" s="T49">mimak </ts>
               <ts e="T51" id="Seg_366" n="e" s="T50">čʼaǯɨt. </ts>
            </ts>
            <ts e="T72" id="Seg_367" n="sc" s="T52">
               <ts e="T53" id="Seg_369" n="e" s="T52">potom </ts>
               <ts e="T54" id="Seg_371" n="e" s="T53">kaʒanna. </ts>
               <ts e="T55" id="Seg_373" n="e" s="T54">tap </ts>
               <ts e="T56" id="Seg_375" n="e" s="T55">qorqo </ts>
               <ts e="T57" id="Seg_377" n="e" s="T56">nʼoːtɨt. </ts>
               <ts e="T58" id="Seg_379" n="e" s="T57">man </ts>
               <ts e="T59" id="Seg_381" n="e" s="T58">naj </ts>
               <ts e="T60" id="Seg_383" n="e" s="T59">načʼidə </ts>
               <ts e="T61" id="Seg_385" n="e" s="T60">kaʒennak. </ts>
               <ts e="T62" id="Seg_387" n="e" s="T61">qorqo </ts>
               <ts e="T63" id="Seg_389" n="e" s="T62">qön </ts>
               <ts e="T64" id="Seg_391" n="e" s="T63">kurdak. </ts>
               <ts e="T65" id="Seg_393" n="e" s="T64">qorqə </ts>
               <ts e="T66" id="Seg_395" n="e" s="T65">meka </ts>
               <ts e="T67" id="Seg_397" n="e" s="T66">manǯa. </ts>
               <ts e="T68" id="Seg_399" n="e" s="T67">mat </ts>
               <ts e="T69" id="Seg_401" n="e" s="T68">tap </ts>
               <ts e="T70" id="Seg_403" n="e" s="T69">čʼaǯap, </ts>
               <ts e="T71" id="Seg_405" n="e" s="T70">elʼlʼe </ts>
               <ts e="T72" id="Seg_407" n="e" s="T71">alʼčʼa. </ts>
            </ts>
            <ts e="T76" id="Seg_408" n="sc" s="T73">
               <ts e="T74" id="Seg_410" n="e" s="T73">kɨbana </ts>
               <ts e="T75" id="Seg_412" n="e" s="T74">tap </ts>
               <ts e="T76" id="Seg_414" n="e" s="T75">orannɨt. </ts>
            </ts>
            <ts e="T78" id="Seg_415" n="sc" s="T77">
               <ts e="T78" id="Seg_417" n="e" s="T77">Всё. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_418" s="T1">KFN_196X_BearHunting2_nar.001 (001)</ta>
            <ta e="T9" id="Seg_419" s="T5">KFN_196X_BearHunting2_nar.002 (002)</ta>
            <ta e="T14" id="Seg_420" s="T10">KFN_196X_BearHunting2_nar.003 (003)</ta>
            <ta e="T21" id="Seg_421" s="T15">KFN_196X_BearHunting2_nar.004 (004)</ta>
            <ta e="T26" id="Seg_422" s="T22">KFN_196X_BearHunting2_nar.005 (005)</ta>
            <ta e="T30" id="Seg_423" s="T27">KFN_196X_BearHunting2_nar.006 (006)</ta>
            <ta e="T34" id="Seg_424" s="T31">KFN_196X_BearHunting2_nar.007 (007)</ta>
            <ta e="T39" id="Seg_425" s="T34">KFN_196X_BearHunting2_nar.008 (008)</ta>
            <ta e="T44" id="Seg_426" s="T40">KFN_196X_BearHunting2_nar.009 (009)</ta>
            <ta e="T51" id="Seg_427" s="T45">KFN_196X_BearHunting2_nar.010 (010)</ta>
            <ta e="T54" id="Seg_428" s="T52">KFN_196X_BearHunting2_nar.011 (011)</ta>
            <ta e="T57" id="Seg_429" s="T54">KFN_196X_BearHunting2_nar.012 (012)</ta>
            <ta e="T61" id="Seg_430" s="T57">KFN_196X_BearHunting2_nar.013 (013)</ta>
            <ta e="T64" id="Seg_431" s="T61">KFN_196X_BearHunting2_nar.014 (014)</ta>
            <ta e="T67" id="Seg_432" s="T64">KFN_196X_BearHunting2_nar.015 (015)</ta>
            <ta e="T72" id="Seg_433" s="T67">KFN_196X_BearHunting2_nar.016 (016)</ta>
            <ta e="T76" id="Seg_434" s="T73">KFN_196X_BearHunting2_nar.017 (017)</ta>
            <ta e="T78" id="Seg_435" s="T77">KFN_196X_BearHunting2_nar.018 (018)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_436" s="T1">Чаджэқыӷыт нӓлгут маджӧӷэт қваямбат.</ta>
            <ta e="T9" id="Seg_437" s="T5">Канак мудэмба корӷэт педэнд.</ta>
            <ta e="T14" id="Seg_438" s="T10">Чвэссе тӧат, машэк қвэрат.</ta>
            <ta e="T21" id="Seg_439" s="T15">Ми товаришей начид квэннай, коргот педэнд.</ta>
            <ta e="T26" id="Seg_440" s="T22">Коргот пед кэналдж мемба.</ta>
            <ta e="T30" id="Seg_441" s="T27">Педэн могоут тӧлай.</ta>
            <ta e="T34" id="Seg_442" s="T31">Корг понэ пахта.</ta>
            <ta e="T39" id="Seg_443" s="T34">Мат чажап. Раненнап. Кажэк торчагэнна.</ta>
            <ta e="T44" id="Seg_444" s="T40">Пир товарищем шэдэгут нӧлай.</ta>
            <ta e="T51" id="Seg_445" s="T45">Таб челыт - челыт, весь мимок чажыт.</ta>
            <ta e="T54" id="Seg_446" s="T52">Потом кажэнна.</ta>
            <ta e="T57" id="Seg_447" s="T54">Таб коргоп нӧдыт.</ta>
            <ta e="T61" id="Seg_448" s="T57">Ман най начид кажэннак.</ta>
            <ta e="T64" id="Seg_449" s="T61">Коргон кӧн курдак.</ta>
            <ta e="T67" id="Seg_450" s="T64">Корг мека манджа.</ta>
            <ta e="T72" id="Seg_451" s="T67">Мат табэп чажап. Элле альча.</ta>
            <ta e="T76" id="Seg_452" s="T73">Кыбанатап оранныт.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_453" s="T1">Čʼaǯeqɨɣɨt nälgut maǯöɣet qvajambat.</ta>
            <ta e="T9" id="Seg_454" s="T5">Kanak mudemba korɣet pedend.</ta>
            <ta e="T14" id="Seg_455" s="T10">Čʼvesse töat, mašek qverat.</ta>
            <ta e="T21" id="Seg_456" s="T15">Mi tovarišej načʼid kvennaj, korgot pedend.</ta>
            <ta e="T26" id="Seg_457" s="T22">Korgot ped kenalǯ memba.</ta>
            <ta e="T30" id="Seg_458" s="T27">Peden mogout tölaj.</ta>
            <ta e="T34" id="Seg_459" s="T31">Korg pone paxta.</ta>
            <ta e="T39" id="Seg_460" s="T34">Mat čaʒap. Ranennap. Kaʒek torčagenna.</ta>
            <ta e="T44" id="Seg_461" s="T40">Pir tovarišem šedegut nölaj.</ta>
            <ta e="T51" id="Seg_462" s="T45">Tab čʼelɨt - čʼelɨt, vesʼ mimok čʼaʒɨt.</ta>
            <ta e="T54" id="Seg_463" s="T52">Potom kaʒenna.</ta>
            <ta e="T57" id="Seg_464" s="T54">Tab korgop nödɨt.</ta>
            <ta e="T61" id="Seg_465" s="T57">Man naj načʼid kaʒennak.</ta>
            <ta e="T64" id="Seg_466" s="T61">Korgon kön kurdak.</ta>
            <ta e="T67" id="Seg_467" s="T64">Korg meka manǯa.</ta>
            <ta e="T72" id="Seg_468" s="T67">Mat tabep čʼaʒap. Elle alʼčʼa.</ta>
            <ta e="T76" id="Seg_469" s="T73">Kɨbanatap orannɨt.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_470" s="T1">čaǯeqɨɣɨt nälgut maǯöɣət kwajambat. </ta>
            <ta e="T9" id="Seg_471" s="T5">kana muːdelba korqot pedɛnd. </ta>
            <ta e="T14" id="Seg_472" s="T10">čwesse töwat, mašək qwerat. </ta>
            <ta e="T21" id="Seg_473" s="T15">mi towarɨšej načʼid kwɛnnaj, korɣot pedend. </ta>
            <ta e="T26" id="Seg_474" s="T22">korqot ped kenalǯ meːmba. </ta>
            <ta e="T30" id="Seg_475" s="T27">peden mogout tölaj. </ta>
            <ta e="T34" id="Seg_476" s="T31">qorqə ponɛː paxtə. </ta>
            <ta e="T39" id="Seg_477" s="T34">mat čʼaǯap, ranʼnʼenap, kaʒek torčaɣənna. </ta>
            <ta e="T44" id="Seg_478" s="T40">pir tawarišə šɨtəqut nʼodaj. </ta>
            <ta e="T51" id="Seg_479" s="T45">tapə čʼelɨtɨ, čʼelɨtɨ, vesʼ mimak čʼaǯɨt. </ta>
            <ta e="T54" id="Seg_480" s="T52">potom kaʒanna. </ta>
            <ta e="T57" id="Seg_481" s="T54">tap qorqo nʼoːtɨt. </ta>
            <ta e="T61" id="Seg_482" s="T57">man naj načʼidə kaʒennak. </ta>
            <ta e="T64" id="Seg_483" s="T61">qorqo qön kurdak. </ta>
            <ta e="T67" id="Seg_484" s="T64">qorqə meka manǯa. </ta>
            <ta e="T72" id="Seg_485" s="T67">mat tap čʼaǯap, elʼlʼe alʼčʼa. </ta>
            <ta e="T76" id="Seg_486" s="T73">kɨbana tap orannɨt. </ta>
            <ta e="T78" id="Seg_487" s="T77">Всё. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_488" s="T1">čaǯeqɨ-ɣɨt</ta>
            <ta e="T3" id="Seg_489" s="T2">nä-l-gu-t</ta>
            <ta e="T4" id="Seg_490" s="T3">maǯö-ɣət</ta>
            <ta e="T5" id="Seg_491" s="T4">kwaja-mba-t</ta>
            <ta e="T6" id="Seg_492" s="T5">kana</ta>
            <ta e="T7" id="Seg_493" s="T6">muːd-e-l-ba</ta>
            <ta e="T8" id="Seg_494" s="T7">korqo-t</ta>
            <ta e="T9" id="Seg_495" s="T8">ped-ɛ-nd</ta>
            <ta e="T11" id="Seg_496" s="T10">čwesse</ta>
            <ta e="T12" id="Seg_497" s="T11">tö-wa-t</ta>
            <ta e="T13" id="Seg_498" s="T12">mašək</ta>
            <ta e="T14" id="Seg_499" s="T13">qwer-a-t</ta>
            <ta e="T16" id="Seg_500" s="T15">mi</ta>
            <ta e="T17" id="Seg_501" s="T16">towarɨšej</ta>
            <ta e="T18" id="Seg_502" s="T17">načʼid</ta>
            <ta e="T19" id="Seg_503" s="T18">kwɛn-na-j</ta>
            <ta e="T20" id="Seg_504" s="T19">korɣo-t</ta>
            <ta e="T21" id="Seg_505" s="T20">ped-e-nd</ta>
            <ta e="T23" id="Seg_506" s="T22">korqo-t</ta>
            <ta e="T24" id="Seg_507" s="T23">ped</ta>
            <ta e="T25" id="Seg_508" s="T24">ken-a-l-ǯ</ta>
            <ta e="T26" id="Seg_509" s="T25">meː-mba</ta>
            <ta e="T28" id="Seg_510" s="T27">ped-e-n</ta>
            <ta e="T29" id="Seg_511" s="T28">mogo-ut</ta>
            <ta e="T30" id="Seg_512" s="T29">tö-la-j</ta>
            <ta e="T32" id="Seg_513" s="T31">qorqə</ta>
            <ta e="T33" id="Seg_514" s="T32">ponɛː</ta>
            <ta e="T34" id="Seg_515" s="T33">paxtə</ta>
            <ta e="T35" id="Seg_516" s="T34">mat</ta>
            <ta e="T36" id="Seg_517" s="T35">čʼaǯa-p</ta>
            <ta e="T37" id="Seg_518" s="T36">ranʼnʼe-na-p</ta>
            <ta e="T38" id="Seg_519" s="T37">kaʒek</ta>
            <ta e="T39" id="Seg_520" s="T38">torčaɣənna</ta>
            <ta e="T41" id="Seg_521" s="T40">pir</ta>
            <ta e="T42" id="Seg_522" s="T41">tawarišə</ta>
            <ta e="T43" id="Seg_523" s="T42">šɨtə-qut</ta>
            <ta e="T44" id="Seg_524" s="T43">nʼo-da-j</ta>
            <ta e="T46" id="Seg_525" s="T45">tapə</ta>
            <ta e="T47" id="Seg_526" s="T46">čʼelɨ-tɨ</ta>
            <ta e="T48" id="Seg_527" s="T47">čʼelɨ-tɨ</ta>
            <ta e="T49" id="Seg_528" s="T48">vesʼ</ta>
            <ta e="T50" id="Seg_529" s="T49">mima-k</ta>
            <ta e="T51" id="Seg_530" s="T50">čʼaǯɨ-t</ta>
            <ta e="T53" id="Seg_531" s="T52">potom</ta>
            <ta e="T54" id="Seg_532" s="T53">kaʒan-na</ta>
            <ta e="T55" id="Seg_533" s="T54">tap</ta>
            <ta e="T56" id="Seg_534" s="T55">qorqo</ta>
            <ta e="T57" id="Seg_535" s="T56">nʼoː-tɨ-t</ta>
            <ta e="T58" id="Seg_536" s="T57">man</ta>
            <ta e="T59" id="Seg_537" s="T58">naj</ta>
            <ta e="T60" id="Seg_538" s="T59">načʼi-də</ta>
            <ta e="T61" id="Seg_539" s="T60">kaʒen-na-k</ta>
            <ta e="T62" id="Seg_540" s="T61">qorqo</ta>
            <ta e="T63" id="Seg_541" s="T62">qö-n</ta>
            <ta e="T64" id="Seg_542" s="T63">kur-da-k</ta>
            <ta e="T65" id="Seg_543" s="T64">qorqə</ta>
            <ta e="T66" id="Seg_544" s="T65">meka</ta>
            <ta e="T67" id="Seg_545" s="T66">manǯa</ta>
            <ta e="T68" id="Seg_546" s="T67">mat</ta>
            <ta e="T69" id="Seg_547" s="T68">tap</ta>
            <ta e="T70" id="Seg_548" s="T69">čʼaǯa-p</ta>
            <ta e="T71" id="Seg_549" s="T70">elʼlʼe</ta>
            <ta e="T72" id="Seg_550" s="T71">alʼčʼa</ta>
            <ta e="T74" id="Seg_551" s="T73">kɨba-na</ta>
            <ta e="T75" id="Seg_552" s="T74">tap</ta>
            <ta e="T76" id="Seg_553" s="T75">oran-nɨ-t</ta>
            <ta e="T78" id="Seg_554" s="T77">всё</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_555" s="T1">Čaǯeqɨ-qɨn</ta>
            <ta e="T3" id="Seg_556" s="T2">neː-lʼ-qum-t</ta>
            <ta e="T4" id="Seg_557" s="T3">maǯʼo-qɨt</ta>
            <ta e="T5" id="Seg_558" s="T4">qwaja-mbɨ-dət</ta>
            <ta e="T6" id="Seg_559" s="T5">kanak</ta>
            <ta e="T7" id="Seg_560" s="T6">muːd-ɨ-lɨ-mbɨ</ta>
            <ta e="T8" id="Seg_561" s="T7">qorqɨ-n</ta>
            <ta e="T9" id="Seg_562" s="T8">ped-ɨ-nde</ta>
            <ta e="T11" id="Seg_563" s="T10">čwesse</ta>
            <ta e="T12" id="Seg_564" s="T11">töː-wa-dət</ta>
            <ta e="T13" id="Seg_565" s="T12">maᴣik</ta>
            <ta e="T14" id="Seg_566" s="T13">qwär-ɨ-dət</ta>
            <ta e="T16" id="Seg_567" s="T15">mi</ta>
            <ta e="T17" id="Seg_568" s="T16">towarɨšej</ta>
            <ta e="T18" id="Seg_569" s="T17">nača</ta>
            <ta e="T19" id="Seg_570" s="T18">qwän-ŋɨ-j</ta>
            <ta e="T20" id="Seg_571" s="T19">qorqɨ-n</ta>
            <ta e="T21" id="Seg_572" s="T20">ped-ɨ-nde</ta>
            <ta e="T23" id="Seg_573" s="T22">qorqɨ-n</ta>
            <ta e="T24" id="Seg_574" s="T23">ped</ta>
            <ta e="T25" id="Seg_575" s="T24">ken-ɨ-lʼ-nǯ</ta>
            <ta e="T26" id="Seg_576" s="T25">me-mbɨ</ta>
            <ta e="T28" id="Seg_577" s="T27">ped-ɨ-n</ta>
            <ta e="T29" id="Seg_578" s="T28">moqo-mun</ta>
            <ta e="T30" id="Seg_579" s="T29">töː-lɨ-j</ta>
            <ta e="T32" id="Seg_580" s="T31">qorqɨ</ta>
            <ta e="T33" id="Seg_581" s="T32">ponɛ</ta>
            <ta e="T34" id="Seg_582" s="T33">paktɨ</ta>
            <ta e="T35" id="Seg_583" s="T34">man</ta>
            <ta e="T36" id="Seg_584" s="T35">čačɨ-m</ta>
            <ta e="T37" id="Seg_585" s="T36">ranʼnʼe-ŋɨ-m</ta>
            <ta e="T38" id="Seg_586" s="T37">kaʒɨk</ta>
            <ta e="T39" id="Seg_587" s="T38">%%</ta>
            <ta e="T41" id="Seg_588" s="T40">pir</ta>
            <ta e="T42" id="Seg_589" s="T41">towariš</ta>
            <ta e="T43" id="Seg_590" s="T42">šitə-qɨn</ta>
            <ta e="T44" id="Seg_591" s="T43">nö-ntɨ-j</ta>
            <ta e="T46" id="Seg_592" s="T45">tab</ta>
            <ta e="T47" id="Seg_593" s="T46">čelɨ-tɨ</ta>
            <ta e="T48" id="Seg_594" s="T47">čelɨ-tɨ</ta>
            <ta e="T49" id="Seg_595" s="T48">wesʼ</ta>
            <ta e="T50" id="Seg_596" s="T49">mimo-k</ta>
            <ta e="T51" id="Seg_597" s="T50">čačɨ-tɨ</ta>
            <ta e="T53" id="Seg_598" s="T52">patom</ta>
            <ta e="T54" id="Seg_599" s="T53">kaʒan-ŋɨ</ta>
            <ta e="T55" id="Seg_600" s="T54">tab</ta>
            <ta e="T56" id="Seg_601" s="T55">qorqɨ</ta>
            <ta e="T57" id="Seg_602" s="T56">nöː-da-tɨ</ta>
            <ta e="T58" id="Seg_603" s="T57">man</ta>
            <ta e="T59" id="Seg_604" s="T58">naj</ta>
            <ta e="T60" id="Seg_605" s="T59">nača-nt</ta>
            <ta e="T61" id="Seg_606" s="T60">kaʒan-ŋɨ-k</ta>
            <ta e="T62" id="Seg_607" s="T61">qorqɨ</ta>
            <ta e="T63" id="Seg_608" s="T62">qö-n</ta>
            <ta e="T64" id="Seg_609" s="T63">kur-ntɨ-k</ta>
            <ta e="T65" id="Seg_610" s="T64">qorqɨ</ta>
            <ta e="T66" id="Seg_611" s="T65">mäkkä</ta>
            <ta e="T67" id="Seg_612" s="T66">manǯu</ta>
            <ta e="T68" id="Seg_613" s="T67">man</ta>
            <ta e="T69" id="Seg_614" s="T68">tab</ta>
            <ta e="T70" id="Seg_615" s="T69">čačɨ-m</ta>
            <ta e="T71" id="Seg_616" s="T70">illä</ta>
            <ta e="T72" id="Seg_617" s="T71">alʼči</ta>
            <ta e="T74" id="Seg_618" s="T73">kɨba-n</ta>
            <ta e="T75" id="Seg_619" s="T74">tab</ta>
            <ta e="T76" id="Seg_620" s="T75">oral-ŋɨ-tɨ</ta>
            <ta e="T78" id="Seg_621" s="T77">всё</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_622" s="T1">Chizhapka-LOC</ta>
            <ta e="T3" id="Seg_623" s="T2">daughter-ADJZ-human.being-PL</ta>
            <ta e="T4" id="Seg_624" s="T3">taiga-LOC</ta>
            <ta e="T5" id="Seg_625" s="T4">go-PST.NAR-3PL</ta>
            <ta e="T6" id="Seg_626" s="T5">dog.[NOM]</ta>
            <ta e="T7" id="Seg_627" s="T6">bark-EP-RES-PST.NAR.[3SG.S]</ta>
            <ta e="T8" id="Seg_628" s="T7">bear-GEN</ta>
            <ta e="T9" id="Seg_629" s="T8">cave-EP-ILL</ta>
            <ta e="T11" id="Seg_630" s="T10">backward</ta>
            <ta e="T12" id="Seg_631" s="T11">come-CO-3PL</ta>
            <ta e="T13" id="Seg_632" s="T12">I.ACC</ta>
            <ta e="T14" id="Seg_633" s="T13">call-EP-3PL</ta>
            <ta e="T16" id="Seg_634" s="T15">we.DU.NOM</ta>
            <ta e="T17" id="Seg_635" s="T16">companion.COM</ta>
            <ta e="T18" id="Seg_636" s="T17">there</ta>
            <ta e="T19" id="Seg_637" s="T18">go.away-CO-1DU</ta>
            <ta e="T20" id="Seg_638" s="T19">bear-GEN</ta>
            <ta e="T21" id="Seg_639" s="T20">cave-EP-ILL</ta>
            <ta e="T23" id="Seg_640" s="T22">bear-GEN</ta>
            <ta e="T24" id="Seg_641" s="T23">cave.[NOM]</ta>
            <ta e="T25" id="Seg_642" s="T24">winter-EP-ADJZ-ILL2</ta>
            <ta e="T26" id="Seg_643" s="T25">do-PST.NAR.[3SG.S]</ta>
            <ta e="T28" id="Seg_644" s="T27">cave-EP-GEN</ta>
            <ta e="T29" id="Seg_645" s="T28">back-PROL</ta>
            <ta e="T30" id="Seg_646" s="T29">come-RES-1DU</ta>
            <ta e="T32" id="Seg_647" s="T31">bear.[NOM]</ta>
            <ta e="T33" id="Seg_648" s="T32">outwards</ta>
            <ta e="T34" id="Seg_649" s="T33">jump.[3SG.S]</ta>
            <ta e="T35" id="Seg_650" s="T34">I.NOM</ta>
            <ta e="T36" id="Seg_651" s="T35">shoot-1SG.O</ta>
            <ta e="T37" id="Seg_652" s="T36">wound-CO-1SG.O</ta>
            <ta e="T38" id="Seg_653" s="T37">at.a.run</ta>
            <ta e="T39" id="Seg_654" s="T38">%%</ta>
            <ta e="T41" id="Seg_655" s="T40">oneself.[NOM]</ta>
            <ta e="T42" id="Seg_656" s="T41">companion.[NOM]</ta>
            <ta e="T43" id="Seg_657" s="T42">two-LOC</ta>
            <ta e="T44" id="Seg_658" s="T43">drive-IPFV-1DU</ta>
            <ta e="T46" id="Seg_659" s="T45">(s)he.[NOM]</ta>
            <ta e="T47" id="Seg_660" s="T46">aim-3SG.O</ta>
            <ta e="T48" id="Seg_661" s="T47">aim-3SG.O</ta>
            <ta e="T49" id="Seg_662" s="T48">all</ta>
            <ta e="T50" id="Seg_663" s="T49">beside-ADVZ</ta>
            <ta e="T51" id="Seg_664" s="T50">shoot-3SG.O</ta>
            <ta e="T53" id="Seg_665" s="T52">then</ta>
            <ta e="T54" id="Seg_666" s="T53">run-CO.[3SG.S]</ta>
            <ta e="T55" id="Seg_667" s="T54">(s)he.[3SG.S]</ta>
            <ta e="T56" id="Seg_668" s="T55">bear.[NOM]</ta>
            <ta e="T57" id="Seg_669" s="T56">drive-IPFV-3SG.O</ta>
            <ta e="T58" id="Seg_670" s="T57">I.NOM</ta>
            <ta e="T59" id="Seg_671" s="T58">also</ta>
            <ta e="T60" id="Seg_672" s="T59">there-ADV.ILL</ta>
            <ta e="T61" id="Seg_673" s="T60">run-CO-1SG.S</ta>
            <ta e="T62" id="Seg_674" s="T61">bear.[NOM]</ta>
            <ta e="T63" id="Seg_675" s="T62">side-ADV.LOC</ta>
            <ta e="T64" id="Seg_676" s="T63">run-IPFV-1SG.S</ta>
            <ta e="T65" id="Seg_677" s="T64">bear.[NOM]</ta>
            <ta e="T66" id="Seg_678" s="T65">I.ALL</ta>
            <ta e="T67" id="Seg_679" s="T66">look.[3SG.S]</ta>
            <ta e="T68" id="Seg_680" s="T67">I.NOM</ta>
            <ta e="T69" id="Seg_681" s="T68">(s)he.[NOM]</ta>
            <ta e="T70" id="Seg_682" s="T69">shoot-1SG.O</ta>
            <ta e="T71" id="Seg_683" s="T70">down</ta>
            <ta e="T72" id="Seg_684" s="T71">fall.[3SG.S]</ta>
            <ta e="T74" id="Seg_685" s="T73">small-ADV.LOC</ta>
            <ta e="T75" id="Seg_686" s="T74">(s)he.[NOM]</ta>
            <ta e="T76" id="Seg_687" s="T75">catch-CO-3SG.O</ta>
            <ta e="T78" id="Seg_688" s="T77">all</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_689" s="T1">Чижапка-LOC</ta>
            <ta e="T3" id="Seg_690" s="T2">дочь-ADJZ-человек-PL</ta>
            <ta e="T4" id="Seg_691" s="T3">тайга-LOC</ta>
            <ta e="T5" id="Seg_692" s="T4">идти-PST.NAR-3PL</ta>
            <ta e="T6" id="Seg_693" s="T5">собака.[NOM]</ta>
            <ta e="T7" id="Seg_694" s="T6">лаять-EP-RES-PST.NAR.[3SG.S]</ta>
            <ta e="T8" id="Seg_695" s="T7">медведь-GEN</ta>
            <ta e="T9" id="Seg_696" s="T8">берлога-EP-ILL</ta>
            <ta e="T11" id="Seg_697" s="T10">назад</ta>
            <ta e="T12" id="Seg_698" s="T11">прийти-CO-3PL</ta>
            <ta e="T13" id="Seg_699" s="T12">я.ACC</ta>
            <ta e="T14" id="Seg_700" s="T13">позвать-EP-3PL</ta>
            <ta e="T16" id="Seg_701" s="T15">мы.DU.NOM</ta>
            <ta e="T17" id="Seg_702" s="T16">товарищ.COM</ta>
            <ta e="T18" id="Seg_703" s="T17">туда</ta>
            <ta e="T19" id="Seg_704" s="T18">пойти-CO-1DU</ta>
            <ta e="T20" id="Seg_705" s="T19">медведь-GEN</ta>
            <ta e="T21" id="Seg_706" s="T20">берлога-EP-ILL</ta>
            <ta e="T23" id="Seg_707" s="T22">медведь-GEN</ta>
            <ta e="T24" id="Seg_708" s="T23">берлога.[NOM]</ta>
            <ta e="T25" id="Seg_709" s="T24">зима-EP-ADJZ-ILL2</ta>
            <ta e="T26" id="Seg_710" s="T25">делать-PST.NAR.[3SG.S]</ta>
            <ta e="T28" id="Seg_711" s="T27">берлога-EP-GEN</ta>
            <ta e="T29" id="Seg_712" s="T28">спина-PROL</ta>
            <ta e="T30" id="Seg_713" s="T29">прийти-RES-1DU</ta>
            <ta e="T32" id="Seg_714" s="T31">медведь.[NOM]</ta>
            <ta e="T33" id="Seg_715" s="T32">наружу</ta>
            <ta e="T34" id="Seg_716" s="T33">прыгнуть.[3SG.S]</ta>
            <ta e="T35" id="Seg_717" s="T34">я.NOM</ta>
            <ta e="T36" id="Seg_718" s="T35">стрелять-1SG.O</ta>
            <ta e="T37" id="Seg_719" s="T36">ранить-CO-1SG.O</ta>
            <ta e="T38" id="Seg_720" s="T37">бегом</ta>
            <ta e="T39" id="Seg_721" s="T38">%%</ta>
            <ta e="T41" id="Seg_722" s="T40">сам.[NOM]</ta>
            <ta e="T42" id="Seg_723" s="T41">товарищ.[NOM]</ta>
            <ta e="T43" id="Seg_724" s="T42">два-LOC</ta>
            <ta e="T44" id="Seg_725" s="T43">гнать-IPFV-1DU</ta>
            <ta e="T46" id="Seg_726" s="T45">он(а).[NOM]</ta>
            <ta e="T47" id="Seg_727" s="T46">целиться-3SG.O</ta>
            <ta e="T48" id="Seg_728" s="T47">целиться-3SG.O</ta>
            <ta e="T49" id="Seg_729" s="T48">весь</ta>
            <ta e="T50" id="Seg_730" s="T49">мимо-ADVZ</ta>
            <ta e="T51" id="Seg_731" s="T50">стрелять-3SG.O</ta>
            <ta e="T53" id="Seg_732" s="T52">потом</ta>
            <ta e="T54" id="Seg_733" s="T53">бежать-CO.[3SG.S]</ta>
            <ta e="T55" id="Seg_734" s="T54">он(а).[3SG.S]</ta>
            <ta e="T56" id="Seg_735" s="T55">медведь.[NOM]</ta>
            <ta e="T57" id="Seg_736" s="T56">гнать-IPFV-3SG.O</ta>
            <ta e="T58" id="Seg_737" s="T57">я.NOM</ta>
            <ta e="T59" id="Seg_738" s="T58">тоже</ta>
            <ta e="T60" id="Seg_739" s="T59">туда-ADV.ILL</ta>
            <ta e="T61" id="Seg_740" s="T60">бежать-CO-1SG.S</ta>
            <ta e="T62" id="Seg_741" s="T61">медведь.[NOM]</ta>
            <ta e="T63" id="Seg_742" s="T62">сторона-ADV.LOC</ta>
            <ta e="T64" id="Seg_743" s="T63">бегать-IPFV-1SG.S</ta>
            <ta e="T65" id="Seg_744" s="T64">медведь.[NOM]</ta>
            <ta e="T66" id="Seg_745" s="T65">я.ALL</ta>
            <ta e="T67" id="Seg_746" s="T66">смотреть.[3SG.S]</ta>
            <ta e="T68" id="Seg_747" s="T67">я.NOM</ta>
            <ta e="T69" id="Seg_748" s="T68">он(а).[NOM]</ta>
            <ta e="T70" id="Seg_749" s="T69">стрелять-1SG.O</ta>
            <ta e="T71" id="Seg_750" s="T70">вниз</ta>
            <ta e="T72" id="Seg_751" s="T71">упасть.[3SG.S]</ta>
            <ta e="T74" id="Seg_752" s="T73">маленький-ADV.LOC</ta>
            <ta e="T75" id="Seg_753" s="T74">он(а).[NOM]</ta>
            <ta e="T76" id="Seg_754" s="T75">поймать-CO-3SG.O</ta>
            <ta e="T78" id="Seg_755" s="T77">всё</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_756" s="T1">nprop-n:case</ta>
            <ta e="T3" id="Seg_757" s="T2">n-n&gt;adj-n-n:num</ta>
            <ta e="T4" id="Seg_758" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_759" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_760" s="T5">n.[n:case]</ta>
            <ta e="T7" id="Seg_761" s="T6">v-n:ins-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T8" id="Seg_762" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_763" s="T8">n-n:ins-n:case</ta>
            <ta e="T11" id="Seg_764" s="T10">adv</ta>
            <ta e="T12" id="Seg_765" s="T11">v-v:ins-v:pn</ta>
            <ta e="T13" id="Seg_766" s="T12">pers</ta>
            <ta e="T14" id="Seg_767" s="T13">v-n:ins-v:pn</ta>
            <ta e="T16" id="Seg_768" s="T15">pers</ta>
            <ta e="T17" id="Seg_769" s="T16">n</ta>
            <ta e="T18" id="Seg_770" s="T17">adv</ta>
            <ta e="T19" id="Seg_771" s="T18">v-v:ins-v:pn</ta>
            <ta e="T20" id="Seg_772" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_773" s="T20">n-n:ins-n:case</ta>
            <ta e="T23" id="Seg_774" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_775" s="T23">n.[n:case]</ta>
            <ta e="T25" id="Seg_776" s="T24">n-n:ins-n&gt;adj-n:case</ta>
            <ta e="T26" id="Seg_777" s="T25">v-v:tense.[v:pn]</ta>
            <ta e="T28" id="Seg_778" s="T27">n-n:ins-n:case</ta>
            <ta e="T29" id="Seg_779" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_780" s="T29">v-v&gt;v-v:pn</ta>
            <ta e="T32" id="Seg_781" s="T31">n.[n:case]</ta>
            <ta e="T33" id="Seg_782" s="T32">adv</ta>
            <ta e="T34" id="Seg_783" s="T33">v.[v:pn]</ta>
            <ta e="T35" id="Seg_784" s="T34">pers</ta>
            <ta e="T36" id="Seg_785" s="T35">v-v:pn</ta>
            <ta e="T37" id="Seg_786" s="T36">v-v:ins-v:pn</ta>
            <ta e="T38" id="Seg_787" s="T37">adv</ta>
            <ta e="T39" id="Seg_788" s="T38">%%</ta>
            <ta e="T41" id="Seg_789" s="T40">emphpro.[n:case]</ta>
            <ta e="T42" id="Seg_790" s="T41">n</ta>
            <ta e="T43" id="Seg_791" s="T42">num-n:case</ta>
            <ta e="T44" id="Seg_792" s="T43">v-v&gt;v-v:pn</ta>
            <ta e="T46" id="Seg_793" s="T45">pers.[n:case]</ta>
            <ta e="T47" id="Seg_794" s="T46">v-v:pn</ta>
            <ta e="T48" id="Seg_795" s="T47">v-v:pn</ta>
            <ta e="T49" id="Seg_796" s="T48">quant</ta>
            <ta e="T50" id="Seg_797" s="T49">adv-adj&gt;adv</ta>
            <ta e="T51" id="Seg_798" s="T50">v-v:pn</ta>
            <ta e="T53" id="Seg_799" s="T52">adv</ta>
            <ta e="T54" id="Seg_800" s="T53">v-v:ins.[v:pn]</ta>
            <ta e="T55" id="Seg_801" s="T54">pers.[v:pn]</ta>
            <ta e="T56" id="Seg_802" s="T55">n.[n:case]</ta>
            <ta e="T57" id="Seg_803" s="T56">v-v&gt;v-v:pn</ta>
            <ta e="T58" id="Seg_804" s="T57">pers</ta>
            <ta e="T59" id="Seg_805" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_806" s="T59">adv-adv:case</ta>
            <ta e="T61" id="Seg_807" s="T60">v-v:ins-v:pn</ta>
            <ta e="T62" id="Seg_808" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_809" s="T62">n-adv:case</ta>
            <ta e="T64" id="Seg_810" s="T63">v-v&gt;v-v:pn</ta>
            <ta e="T65" id="Seg_811" s="T64">n.[n:case]</ta>
            <ta e="T66" id="Seg_812" s="T65">pers</ta>
            <ta e="T67" id="Seg_813" s="T66">v.[v:pn]</ta>
            <ta e="T68" id="Seg_814" s="T67">pers</ta>
            <ta e="T69" id="Seg_815" s="T68">pers.[n:case]</ta>
            <ta e="T70" id="Seg_816" s="T69">v-v:pn</ta>
            <ta e="T71" id="Seg_817" s="T70">preverb</ta>
            <ta e="T72" id="Seg_818" s="T71">v.[v:pn]</ta>
            <ta e="T74" id="Seg_819" s="T73">adj-adv:case</ta>
            <ta e="T75" id="Seg_820" s="T74">pers.[n:case]</ta>
            <ta e="T76" id="Seg_821" s="T75">v-v:ins-v:pn</ta>
            <ta e="T78" id="Seg_822" s="T77">pro</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_823" s="T1">nprop</ta>
            <ta e="T3" id="Seg_824" s="T2">n</ta>
            <ta e="T4" id="Seg_825" s="T3">n</ta>
            <ta e="T5" id="Seg_826" s="T4">v</ta>
            <ta e="T6" id="Seg_827" s="T5">n</ta>
            <ta e="T7" id="Seg_828" s="T6">v</ta>
            <ta e="T8" id="Seg_829" s="T7">n</ta>
            <ta e="T9" id="Seg_830" s="T8">n</ta>
            <ta e="T11" id="Seg_831" s="T10">adv</ta>
            <ta e="T12" id="Seg_832" s="T11">v</ta>
            <ta e="T13" id="Seg_833" s="T12">pers</ta>
            <ta e="T14" id="Seg_834" s="T13">v</ta>
            <ta e="T16" id="Seg_835" s="T15">pers</ta>
            <ta e="T17" id="Seg_836" s="T16">n</ta>
            <ta e="T18" id="Seg_837" s="T17">adv</ta>
            <ta e="T19" id="Seg_838" s="T18">v</ta>
            <ta e="T20" id="Seg_839" s="T19">n</ta>
            <ta e="T21" id="Seg_840" s="T20">n</ta>
            <ta e="T23" id="Seg_841" s="T22">n</ta>
            <ta e="T24" id="Seg_842" s="T23">n</ta>
            <ta e="T25" id="Seg_843" s="T24">n</ta>
            <ta e="T26" id="Seg_844" s="T25">v</ta>
            <ta e="T28" id="Seg_845" s="T27">n</ta>
            <ta e="T29" id="Seg_846" s="T28">n</ta>
            <ta e="T30" id="Seg_847" s="T29">v</ta>
            <ta e="T32" id="Seg_848" s="T31">n</ta>
            <ta e="T33" id="Seg_849" s="T32">adv</ta>
            <ta e="T34" id="Seg_850" s="T33">v</ta>
            <ta e="T35" id="Seg_851" s="T34">pers</ta>
            <ta e="T36" id="Seg_852" s="T35">v</ta>
            <ta e="T37" id="Seg_853" s="T36">v</ta>
            <ta e="T38" id="Seg_854" s="T37">adv</ta>
            <ta e="T39" id="Seg_855" s="T38">%%</ta>
            <ta e="T41" id="Seg_856" s="T40">emphpro</ta>
            <ta e="T42" id="Seg_857" s="T41">n</ta>
            <ta e="T43" id="Seg_858" s="T42">num</ta>
            <ta e="T44" id="Seg_859" s="T43">v</ta>
            <ta e="T46" id="Seg_860" s="T45">pers</ta>
            <ta e="T47" id="Seg_861" s="T46">v</ta>
            <ta e="T48" id="Seg_862" s="T47">v</ta>
            <ta e="T49" id="Seg_863" s="T48">quant</ta>
            <ta e="T50" id="Seg_864" s="T49">adv</ta>
            <ta e="T51" id="Seg_865" s="T50">v</ta>
            <ta e="T53" id="Seg_866" s="T52">adv</ta>
            <ta e="T54" id="Seg_867" s="T53">v</ta>
            <ta e="T55" id="Seg_868" s="T54">pers</ta>
            <ta e="T56" id="Seg_869" s="T55">n</ta>
            <ta e="T57" id="Seg_870" s="T56">v</ta>
            <ta e="T58" id="Seg_871" s="T57">pers</ta>
            <ta e="T59" id="Seg_872" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_873" s="T59">adv</ta>
            <ta e="T61" id="Seg_874" s="T60">v</ta>
            <ta e="T62" id="Seg_875" s="T61">n</ta>
            <ta e="T63" id="Seg_876" s="T62">n</ta>
            <ta e="T64" id="Seg_877" s="T63">v</ta>
            <ta e="T65" id="Seg_878" s="T64">n</ta>
            <ta e="T66" id="Seg_879" s="T65">pers</ta>
            <ta e="T67" id="Seg_880" s="T66">v</ta>
            <ta e="T68" id="Seg_881" s="T67">pers</ta>
            <ta e="T69" id="Seg_882" s="T68">pers</ta>
            <ta e="T70" id="Seg_883" s="T69">v</ta>
            <ta e="T71" id="Seg_884" s="T70">preverb</ta>
            <ta e="T72" id="Seg_885" s="T71">v</ta>
            <ta e="T74" id="Seg_886" s="T73">adv</ta>
            <ta e="T75" id="Seg_887" s="T74">pers</ta>
            <ta e="T76" id="Seg_888" s="T75">v</ta>
            <ta e="T78" id="Seg_889" s="T77">pro</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_890" s="T1">np:L</ta>
            <ta e="T3" id="Seg_891" s="T2">np.h:A</ta>
            <ta e="T4" id="Seg_892" s="T3">np:L</ta>
            <ta e="T6" id="Seg_893" s="T5">np:A</ta>
            <ta e="T8" id="Seg_894" s="T7">np:Poss</ta>
            <ta e="T9" id="Seg_895" s="T8">np:G</ta>
            <ta e="T11" id="Seg_896" s="T10">adv:G</ta>
            <ta e="T12" id="Seg_897" s="T11">0.3.h:A</ta>
            <ta e="T13" id="Seg_898" s="T12">pro.h:Th</ta>
            <ta e="T14" id="Seg_899" s="T13">0.3.h:A</ta>
            <ta e="T16" id="Seg_900" s="T15">pro.h:A</ta>
            <ta e="T17" id="Seg_901" s="T16">np:Com</ta>
            <ta e="T18" id="Seg_902" s="T17">adv:G</ta>
            <ta e="T20" id="Seg_903" s="T19">np:Poss</ta>
            <ta e="T21" id="Seg_904" s="T20">np:G</ta>
            <ta e="T23" id="Seg_905" s="T22">np:Poss</ta>
            <ta e="T24" id="Seg_906" s="T23">np:Th</ta>
            <ta e="T28" id="Seg_907" s="T27">np:Poss</ta>
            <ta e="T29" id="Seg_908" s="T28">np:Path</ta>
            <ta e="T30" id="Seg_909" s="T29">0.1.h:A</ta>
            <ta e="T32" id="Seg_910" s="T31">np:A</ta>
            <ta e="T33" id="Seg_911" s="T32">adv:G</ta>
            <ta e="T35" id="Seg_912" s="T34">pro.h:A</ta>
            <ta e="T36" id="Seg_913" s="T35">0.3:P</ta>
            <ta e="T37" id="Seg_914" s="T36">0.1.h:A 0.3:P</ta>
            <ta e="T44" id="Seg_915" s="T43">0.1.h:A</ta>
            <ta e="T46" id="Seg_916" s="T45">pro.h:A</ta>
            <ta e="T47" id="Seg_917" s="T46">0.3:Th</ta>
            <ta e="T48" id="Seg_918" s="T47">0.3.h:A 0.3:Th</ta>
            <ta e="T51" id="Seg_919" s="T50">0.3.h:A</ta>
            <ta e="T53" id="Seg_920" s="T52">adv:Time</ta>
            <ta e="T54" id="Seg_921" s="T53">0.3:A</ta>
            <ta e="T55" id="Seg_922" s="T54">pro.h:A</ta>
            <ta e="T56" id="Seg_923" s="T55">np:Th</ta>
            <ta e="T58" id="Seg_924" s="T57">pro.h:A</ta>
            <ta e="T60" id="Seg_925" s="T59">adv:G</ta>
            <ta e="T62" id="Seg_926" s="T61">np:Poss</ta>
            <ta e="T63" id="Seg_927" s="T62">adv:G</ta>
            <ta e="T64" id="Seg_928" s="T63">0.1.h:A</ta>
            <ta e="T65" id="Seg_929" s="T64">np:A</ta>
            <ta e="T66" id="Seg_930" s="T65">pro.h:Th</ta>
            <ta e="T68" id="Seg_931" s="T67">pro.h:A</ta>
            <ta e="T69" id="Seg_932" s="T68">pro:P</ta>
            <ta e="T72" id="Seg_933" s="T71">0.3:P</ta>
            <ta e="T75" id="Seg_934" s="T74">pro.h:A</ta>
            <ta e="T76" id="Seg_935" s="T75">0.3:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_936" s="T2">np.h:S</ta>
            <ta e="T5" id="Seg_937" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_938" s="T5">np:S</ta>
            <ta e="T7" id="Seg_939" s="T6">v:pred</ta>
            <ta e="T12" id="Seg_940" s="T11">0.3.h:S v:pred</ta>
            <ta e="T13" id="Seg_941" s="T12">pro.h:O</ta>
            <ta e="T14" id="Seg_942" s="T13">0.3.h:S v:pred</ta>
            <ta e="T16" id="Seg_943" s="T15">pro.h:S</ta>
            <ta e="T19" id="Seg_944" s="T18">v:pred</ta>
            <ta e="T24" id="Seg_945" s="T23">np:S</ta>
            <ta e="T26" id="Seg_946" s="T25">v:pred</ta>
            <ta e="T30" id="Seg_947" s="T29">0.1.h:S v:pred</ta>
            <ta e="T32" id="Seg_948" s="T31">np:S</ta>
            <ta e="T34" id="Seg_949" s="T33">v:pred</ta>
            <ta e="T35" id="Seg_950" s="T34">pro.h:S</ta>
            <ta e="T36" id="Seg_951" s="T35">v:pred 0.3:O</ta>
            <ta e="T37" id="Seg_952" s="T36">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T44" id="Seg_953" s="T43">0.1.h:S v:pred</ta>
            <ta e="T46" id="Seg_954" s="T45">pro.h:S</ta>
            <ta e="T47" id="Seg_955" s="T46">v:pred 0.3:O</ta>
            <ta e="T48" id="Seg_956" s="T47">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T51" id="Seg_957" s="T50">0.3.h:S v:pred</ta>
            <ta e="T54" id="Seg_958" s="T53">0.3:S v:pred</ta>
            <ta e="T55" id="Seg_959" s="T54">pro.h:S</ta>
            <ta e="T56" id="Seg_960" s="T55">np:O</ta>
            <ta e="T57" id="Seg_961" s="T56">v:pred</ta>
            <ta e="T58" id="Seg_962" s="T57">pro.h:S</ta>
            <ta e="T61" id="Seg_963" s="T60">v:pred</ta>
            <ta e="T64" id="Seg_964" s="T63">0.1.h:S v:pred</ta>
            <ta e="T65" id="Seg_965" s="T64">np:S</ta>
            <ta e="T67" id="Seg_966" s="T66">v:pred</ta>
            <ta e="T68" id="Seg_967" s="T67">pro.h:S</ta>
            <ta e="T69" id="Seg_968" s="T68">pro:O</ta>
            <ta e="T70" id="Seg_969" s="T69">v:pred</ta>
            <ta e="T72" id="Seg_970" s="T71">0.3:S v:pred</ta>
            <ta e="T75" id="Seg_971" s="T74">pro.h:S</ta>
            <ta e="T76" id="Seg_972" s="T75">v:pred 0.3:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T17" id="Seg_973" s="T16">RUS:cult</ta>
            <ta e="T37" id="Seg_974" s="T36">RUS:core</ta>
            <ta e="T42" id="Seg_975" s="T41">RUS:cult</ta>
            <ta e="T47" id="Seg_976" s="T46">RUS:cult</ta>
            <ta e="T48" id="Seg_977" s="T47">RUS:cult</ta>
            <ta e="T49" id="Seg_978" s="T48">RUS:core</ta>
            <ta e="T53" id="Seg_979" s="T52">RUS:core</ta>
            <ta e="T78" id="Seg_980" s="T77">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_981" s="T1">In Chizhapka women were wandering in the forest.</ta>
            <ta e="T9" id="Seg_982" s="T5">A dog started to bark at a bear’s den.</ta>
            <ta e="T14" id="Seg_983" s="T10">They came back and called me.</ta>
            <ta e="T21" id="Seg_984" s="T15">A friend and I went there, to the bear’s den.</ta>
            <ta e="T26" id="Seg_985" s="T22">The bear’s den was made (for the winter?).</ta>
            <ta e="T30" id="Seg_986" s="T27">We came to the den from behind.</ta>
            <ta e="T34" id="Seg_987" s="T31">The bear ran outside.</ta>
            <ta e="T39" id="Seg_988" s="T34">I shot and wounded it, It ran away.</ta>
            <ta e="T44" id="Seg_989" s="T40">My friend and I, the two of us ran after it.</ta>
            <ta e="T51" id="Seg_990" s="T45">My friend fired and fired, and missed.</ta>
            <ta e="T54" id="Seg_991" s="T52">Then [it?] ran.</ta>
            <ta e="T57" id="Seg_992" s="T54">He was trying to catch up with the bear.</ta>
            <ta e="T61" id="Seg_993" s="T57">I also ran that way.</ta>
            <ta e="T64" id="Seg_994" s="T61">I ran up to the bear.</ta>
            <ta e="T67" id="Seg_995" s="T64">The bear stares at me.</ta>
            <ta e="T72" id="Seg_996" s="T67">I shot at it and [it?] fell. </ta>
            <ta e="T76" id="Seg_997" s="T73">Barely caught it.</ta>
            <ta e="T78" id="Seg_998" s="T77">That's all.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_999" s="T1">In Chizhapka wanderten Frauen im Wald herum.</ta>
            <ta e="T9" id="Seg_1000" s="T5">Der Hund fing an, vor der Bärenhöhle zu bellen.</ta>
            <ta e="T14" id="Seg_1001" s="T10">Sie kamen zurück und riefen mich.</ta>
            <ta e="T21" id="Seg_1002" s="T15">Ein Freund und ich gingen hin, zur Bärenhöhle.</ta>
            <ta e="T26" id="Seg_1003" s="T22">Die Bärenhöhle war (%).</ta>
            <ta e="T30" id="Seg_1004" s="T27">Wir gingen hin, von hinter der Bärenhöhle.</ta>
            <ta e="T34" id="Seg_1005" s="T31">Der Bär rannte nach draußen.</ta>
            <ta e="T39" id="Seg_1006" s="T34">Ich schoss nach ihm und verletzte ihn, er lief weg. </ta>
            <ta e="T44" id="Seg_1007" s="T40">Mein Freund und ich, wir zwei liefen ihm nach.</ta>
            <ta e="T51" id="Seg_1008" s="T45">Mein Freund schoss und schoss – jedes Mal daneben. (%)</ta>
            <ta e="T54" id="Seg_1009" s="T52">Dann lief [der Bär?].</ta>
            <ta e="T57" id="Seg_1010" s="T54">Er wollte den Bär einholen.</ta>
            <ta e="T61" id="Seg_1011" s="T57">Ich rannte ihm auch nach.</ta>
            <ta e="T64" id="Seg_1012" s="T61">Ich rannte auf den Bär zu.</ta>
            <ta e="T67" id="Seg_1013" s="T64">Der Bär schaut mich an.</ta>
            <ta e="T72" id="Seg_1014" s="T67">Ich schoss nach ihm, [er?] fiel.</ta>
            <ta e="T76" id="Seg_1015" s="T73">Er hat ihn(??) beinahe erwischt. </ta>
            <ta e="T78" id="Seg_1016" s="T77">Das war's.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_1017" s="T1">В Чижапке женщины по лесу ходили.</ta>
            <ta e="T9" id="Seg_1018" s="T5">Собака залаяла на медвежью берлогу.</ta>
            <ta e="T14" id="Seg_1019" s="T10">Назад пришли, меня зовут.</ta>
            <ta e="T21" id="Seg_1020" s="T15">Мы с товарищем туда пошли, к медвежьей берлоге.</ta>
            <ta e="T26" id="Seg_1021" s="T22">Медвежья берлога (на зиму?) сделана.</ta>
            <ta e="T30" id="Seg_1022" s="T27">Cзади берлоги подошли.</ta>
            <ta e="T34" id="Seg_1023" s="T31">Медведь наружу выскочил.</ta>
            <ta e="T39" id="Seg_1024" s="T34">Я выстрелил, ранил его, он убежал.</ta>
            <ta e="T44" id="Seg_1025" s="T40">С товарищем вдвоём догоняем.</ta>
            <ta e="T51" id="Seg_1026" s="T45">Он стрелял, стрелял, мимо выстрелил.</ta>
            <ta e="T54" id="Seg_1027" s="T52">Потом [медведь?] побежал. </ta>
            <ta e="T57" id="Seg_1028" s="T54">Он медведя догоняет.</ta>
            <ta e="T61" id="Seg_1029" s="T57">Я тоже туда побежал.</ta>
            <ta e="T64" id="Seg_1030" s="T61">К медведю прибежал.</ta>
            <ta e="T67" id="Seg_1031" s="T64">Медведь на меня смотрит.</ta>
            <ta e="T72" id="Seg_1032" s="T67">Я в него выстрелил, [он?] упал.</ta>
            <ta e="T76" id="Seg_1033" s="T73">Чуть его не поймал.</ta>
            <ta e="T78" id="Seg_1034" s="T77">Всё.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T57" id="Seg_1035" s="T54">[AAV] qorɣoj ? However ADJZ should be -l.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
