<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KFN_196X_DrunkBear2_nar</transcription-name>
         <referenced-file url="KFN_196X_DrunkBear2_nar.wav" />
         <referenced-file url="KFN_196X_DrunkBear2_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KFN_196X_DrunkBear2_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">90</ud-information>
            <ud-information attribute-name="# HIAT:w">64</ud-information>
            <ud-information attribute-name="# e">64</ud-information>
            <ud-information attribute-name="# HIAT:u">20</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KFN">
            <abbreviation>KFN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="10.16" type="appl" />
         <tli id="T1" time="11.612" type="appl" />
         <tli id="T2" time="13.065" type="appl" />
         <tli id="T3" time="14.518" type="appl" />
         <tli id="T4" time="16.925658343651616" />
         <tli id="T5" time="17.863" type="appl" />
         <tli id="T6" time="18.851" type="appl" />
         <tli id="T7" time="19.839" type="appl" />
         <tli id="T8" time="20.827" type="appl" />
         <tli id="T9" time="22.818640610602397" />
         <tli id="T10" time="23.66" type="appl" />
         <tli id="T11" time="24.725" type="appl" />
         <tli id="T12" time="25.79" type="appl" />
         <tli id="T13" time="26.855" type="appl" />
         <tli id="T14" time="28.985" type="appl" />
         <tli id="T15" time="30.085" type="appl" />
         <tli id="T16" time="31.42" type="appl" />
         <tli id="T17" time="32.175" type="appl" />
         <tli id="T18" time="32.93" type="appl" />
         <tli id="T19" time="33.685" type="appl" />
         <tli id="T20" time="34.44" type="appl" />
         <tli id="T21" time="35.195" type="appl" />
         <tli id="T22" time="36.695" type="appl" />
         <tli id="T23" time="37.49" type="appl" />
         <tli id="T24" time="38.758" type="appl" />
         <tli id="T25" time="39.672" type="appl" />
         <tli id="T26" time="41.25754213818821" />
         <tli id="T27" time="42.065" type="appl" />
         <tli id="T28" time="42.975" type="appl" />
         <tli id="T29" time="43.885" type="appl" />
         <tli id="T30" time="44.795" type="appl" />
         <tli id="T31" time="46.34390579167627" />
         <tli id="T32" time="47.17" type="appl" />
         <tli id="T33" time="49.02374614384166" />
         <tli id="T34" time="50.255" type="appl" />
         <tli id="T35" time="51.75" type="appl" />
         <tli id="T36" time="53.245" type="appl" />
         <tli id="T37" time="55.983331536032395" />
         <tli id="T38" time="56.75" type="appl" />
         <tli id="T39" time="58.07654016931583" />
         <tli id="T40" time="59.265" type="appl" />
         <tli id="T41" time="61.64966063886969" />
         <tli id="T42" time="62.708" type="appl" />
         <tli id="T43" time="63.892" type="appl" />
         <tli id="T44" time="65.59609220225754" />
         <tli id="T45" time="68.4959194490037" />
         <tli id="T46" time="69.475" type="appl" />
         <tli id="T47" time="70.485" type="appl" />
         <tli id="T48" time="71.725" type="appl" />
         <tli id="T49" time="73.43562517277125" />
         <tli id="T50" time="74.09" type="appl" />
         <tli id="T51" time="74.865" type="appl" />
         <tli id="T52" time="75.64" type="appl" />
         <tli id="T53" time="76.415" type="appl" />
         <tli id="T54" time="77.55" type="appl" />
         <tli id="T55" time="78.375" type="appl" />
         <tli id="T56" time="79.893" type="appl" />
         <tli id="T57" time="80.677" type="appl" />
         <tli id="T58" time="81.46" type="appl" />
         <tli id="T59" time="83.235" type="appl" />
         <tli id="T60" time="83.865" type="appl" />
         <tli id="T61" time="84.495" type="appl" />
         <tli id="T62" time="85.125" type="appl" />
         <tli id="T63" time="85.755" type="appl" />
         <tli id="T64" time="86.385" type="appl" />
         <tli id="T65" time="86.814" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KFN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T64" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Ugot</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">Telʼpekaqət</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">čʼol</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">warembad</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Načʼat</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">qorqə</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">tömba</ts>
                  <nts id="Seg_26" n="HIAT:ip">,</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">mʼodom</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">awešpat</ts>
                  <nts id="Seg_33" n="HIAT:ip">.</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_36" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">Tapet</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">qoštembat</ts>
                  <nts id="Seg_42" n="HIAT:ip">,</nts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_45" n="HIAT:w" s="T11">mʼodom</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">awešpat</ts>
                  <nts id="Seg_49" n="HIAT:ip">.</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_52" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_54" n="HIAT:w" s="T13">Parət</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_57" n="HIAT:w" s="T14">mʼeːmɨndat</ts>
                  <nts id="Seg_58" n="HIAT:ip">.</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_61" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_63" n="HIAT:w" s="T15">Načʼat</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_66" n="HIAT:w" s="T16">čočembat</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_69" n="HIAT:w" s="T17">kočʼek</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_72" n="HIAT:w" s="T18">paridep</ts>
                  <nts id="Seg_73" n="HIAT:ip">,</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_76" n="HIAT:w" s="T19">qorqop</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">šerbat</ts>
                  <nts id="Seg_80" n="HIAT:ip">.</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_83" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_85" n="HIAT:w" s="T21">qorqə</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_88" n="HIAT:w" s="T22">šerbat</ts>
                  <nts id="Seg_89" n="HIAT:ip">.</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_92" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_94" n="HIAT:w" s="T23">Elʼlʼe</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_97" n="HIAT:w" s="T24">alʼčimba</ts>
                  <nts id="Seg_98" n="HIAT:ip">,</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_101" n="HIAT:w" s="T25">qondalba</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_105" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_107" n="HIAT:w" s="T26">Tapət</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_110" n="HIAT:w" s="T27">šɨdə</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_113" n="HIAT:w" s="T28">čʼundəm</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_116" n="HIAT:w" s="T29">arelʼbat</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_119" n="HIAT:w" s="T30">čʼelʼegand</ts>
                  <nts id="Seg_120" n="HIAT:ip">.</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_123" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_125" n="HIAT:w" s="T31">Načʼat</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_128" n="HIAT:w" s="T32">qwenbat</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_132" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_134" n="HIAT:w" s="T33">čʼelʼegand</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_137" n="HIAT:w" s="T34">tälʼǯembat</ts>
                  <nts id="Seg_138" n="HIAT:ip">,</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_141" n="HIAT:w" s="T35">qorqo</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_144" n="HIAT:w" s="T36">qondalbəl</ts>
                  <nts id="Seg_145" n="HIAT:ip">.</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_148" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_150" n="HIAT:w" s="T37">Mogunä</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_153" n="HIAT:w" s="T38">qwendegu</ts>
                  <nts id="Seg_154" n="HIAT:ip">.</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_157" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_159" n="HIAT:w" s="T39">Mogunä</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_162" n="HIAT:w" s="T40">üːgorembat</ts>
                  <nts id="Seg_163" n="HIAT:ip">.</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_166" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_168" n="HIAT:w" s="T41">Qorqo</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_171" n="HIAT:w" s="T42">watot</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_174" n="HIAT:w" s="T43">qelʼčʼimba</ts>
                  <nts id="Seg_175" n="HIAT:ip">.</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_178" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_180" n="HIAT:w" s="T44">Parkeːlɨmba</ts>
                  <nts id="Seg_181" n="HIAT:ip">.</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_184" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_186" n="HIAT:w" s="T45">Čʼundət</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_189" n="HIAT:w" s="T46">qačʼiwatpat</ts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_193" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_195" n="HIAT:w" s="T47">Qoteq</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_198" n="HIAT:w" s="T48">torčaːɣəlbat</ts>
                  <nts id="Seg_199" n="HIAT:ip">.</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_202" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_204" n="HIAT:w" s="T49">Wesʼ</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_207" n="HIAT:w" s="T50">čʼelʼegap</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_210" n="HIAT:w" s="T51">taq</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_213" n="HIAT:w" s="T52">panalbat</ts>
                  <nts id="Seg_214" n="HIAT:ip">.</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_217" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_219" n="HIAT:w" s="T53">Qorqo</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_222" n="HIAT:w" s="T54">qwenba</ts>
                  <nts id="Seg_223" n="HIAT:ip">.</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_226" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_228" n="HIAT:w" s="T55">Tab-a-t</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_231" n="HIAT:w" s="T56">čʼwesse</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_234" n="HIAT:w" s="T57">töːwat</ts>
                  <nts id="Seg_235" n="HIAT:ip">.</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_238" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_240" n="HIAT:w" s="T58">Čʼelʼega</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_243" n="HIAT:w" s="T59">naj</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_246" n="HIAT:w" s="T60">čʼaŋgwa</ts>
                  <nts id="Seg_247" n="HIAT:ip">,</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_250" n="HIAT:w" s="T61">qorqo</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_253" n="HIAT:w" s="T62">naj</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_256" n="HIAT:w" s="T63">čʼaŋgwa</ts>
                  <nts id="Seg_257" n="HIAT:ip">.</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T64" id="Seg_259" n="sc" s="T0">
               <ts e="T1" id="Seg_261" n="e" s="T0">Ugot </ts>
               <ts e="T2" id="Seg_263" n="e" s="T1">Telʼpekaqət </ts>
               <ts e="T3" id="Seg_265" n="e" s="T2">čʼol </ts>
               <ts e="T4" id="Seg_267" n="e" s="T3">warembad. </ts>
               <ts e="T5" id="Seg_269" n="e" s="T4">Načʼat </ts>
               <ts e="T6" id="Seg_271" n="e" s="T5">qorqə </ts>
               <ts e="T7" id="Seg_273" n="e" s="T6">tömba, </ts>
               <ts e="T8" id="Seg_275" n="e" s="T7">mʼodom </ts>
               <ts e="T9" id="Seg_277" n="e" s="T8">awešpat. </ts>
               <ts e="T10" id="Seg_279" n="e" s="T9">Tapet </ts>
               <ts e="T11" id="Seg_281" n="e" s="T10">qoštembat, </ts>
               <ts e="T12" id="Seg_283" n="e" s="T11">mʼodom </ts>
               <ts e="T13" id="Seg_285" n="e" s="T12">awešpat. </ts>
               <ts e="T14" id="Seg_287" n="e" s="T13">Parət </ts>
               <ts e="T15" id="Seg_289" n="e" s="T14">mʼeːmɨndat. </ts>
               <ts e="T16" id="Seg_291" n="e" s="T15">Načʼat </ts>
               <ts e="T17" id="Seg_293" n="e" s="T16">čočembat </ts>
               <ts e="T18" id="Seg_295" n="e" s="T17">kočʼek </ts>
               <ts e="T19" id="Seg_297" n="e" s="T18">paridep, </ts>
               <ts e="T20" id="Seg_299" n="e" s="T19">qorqop </ts>
               <ts e="T21" id="Seg_301" n="e" s="T20">šerbat. </ts>
               <ts e="T22" id="Seg_303" n="e" s="T21">qorqə </ts>
               <ts e="T23" id="Seg_305" n="e" s="T22">šerbat. </ts>
               <ts e="T24" id="Seg_307" n="e" s="T23">Elʼlʼe </ts>
               <ts e="T25" id="Seg_309" n="e" s="T24">alʼčimba, </ts>
               <ts e="T26" id="Seg_311" n="e" s="T25">qondalba. </ts>
               <ts e="T27" id="Seg_313" n="e" s="T26">Tapət </ts>
               <ts e="T28" id="Seg_315" n="e" s="T27">šɨdə </ts>
               <ts e="T29" id="Seg_317" n="e" s="T28">čʼundəm </ts>
               <ts e="T30" id="Seg_319" n="e" s="T29">arelʼbat </ts>
               <ts e="T31" id="Seg_321" n="e" s="T30">čʼelʼegand. </ts>
               <ts e="T32" id="Seg_323" n="e" s="T31">Načʼat </ts>
               <ts e="T33" id="Seg_325" n="e" s="T32">qwenbat. </ts>
               <ts e="T34" id="Seg_327" n="e" s="T33">čʼelʼegand </ts>
               <ts e="T35" id="Seg_329" n="e" s="T34">tälʼǯembat, </ts>
               <ts e="T36" id="Seg_331" n="e" s="T35">qorqo </ts>
               <ts e="T37" id="Seg_333" n="e" s="T36">qondalbəl. </ts>
               <ts e="T38" id="Seg_335" n="e" s="T37">Mogunä </ts>
               <ts e="T39" id="Seg_337" n="e" s="T38">qwendegu. </ts>
               <ts e="T40" id="Seg_339" n="e" s="T39">Mogunä </ts>
               <ts e="T41" id="Seg_341" n="e" s="T40">üːgorembat. </ts>
               <ts e="T42" id="Seg_343" n="e" s="T41">Qorqo </ts>
               <ts e="T43" id="Seg_345" n="e" s="T42">watot </ts>
               <ts e="T44" id="Seg_347" n="e" s="T43">qelʼčʼimba. </ts>
               <ts e="T45" id="Seg_349" n="e" s="T44">Parkeːlɨmba. </ts>
               <ts e="T46" id="Seg_351" n="e" s="T45">Čʼundət </ts>
               <ts e="T47" id="Seg_353" n="e" s="T46">qačʼiwatpat. </ts>
               <ts e="T48" id="Seg_355" n="e" s="T47">Qoteq </ts>
               <ts e="T49" id="Seg_357" n="e" s="T48">torčaːɣəlbat. </ts>
               <ts e="T50" id="Seg_359" n="e" s="T49">Wesʼ </ts>
               <ts e="T51" id="Seg_361" n="e" s="T50">čʼelʼegap </ts>
               <ts e="T52" id="Seg_363" n="e" s="T51">taq </ts>
               <ts e="T53" id="Seg_365" n="e" s="T52">panalbat. </ts>
               <ts e="T54" id="Seg_367" n="e" s="T53">Qorqo </ts>
               <ts e="T55" id="Seg_369" n="e" s="T54">qwenba. </ts>
               <ts e="T56" id="Seg_371" n="e" s="T55">Tab-a-t </ts>
               <ts e="T57" id="Seg_373" n="e" s="T56">čʼwesse </ts>
               <ts e="T58" id="Seg_375" n="e" s="T57">töːwat. </ts>
               <ts e="T59" id="Seg_377" n="e" s="T58">Čʼelʼega </ts>
               <ts e="T60" id="Seg_379" n="e" s="T59">naj </ts>
               <ts e="T61" id="Seg_381" n="e" s="T60">čʼaŋgwa, </ts>
               <ts e="T62" id="Seg_383" n="e" s="T61">qorqo </ts>
               <ts e="T63" id="Seg_385" n="e" s="T62">naj </ts>
               <ts e="T64" id="Seg_387" n="e" s="T63">čʼaŋgwa. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_388" s="T0">KFN_196X_DrunkBear2_nar.001 (001)</ta>
            <ta e="T9" id="Seg_389" s="T4">KFN_196X_DrunkBear2_nar.002 (002)</ta>
            <ta e="T13" id="Seg_390" s="T9">KFN_196X_DrunkBear2_nar.003 (003)</ta>
            <ta e="T15" id="Seg_391" s="T13">KFN_196X_DrunkBear2_nar.004 (004)</ta>
            <ta e="T21" id="Seg_392" s="T15">KFN_196X_DrunkBear2_nar.005 (005)</ta>
            <ta e="T23" id="Seg_393" s="T21">KFN_196X_DrunkBear2_nar.006 (006)</ta>
            <ta e="T26" id="Seg_394" s="T23">KFN_196X_DrunkBear2_nar.007 (007)</ta>
            <ta e="T31" id="Seg_395" s="T26">KFN_196X_DrunkBear2_nar.008 (008)</ta>
            <ta e="T33" id="Seg_396" s="T31">KFN_196X_DrunkBear2_nar.009 (009)</ta>
            <ta e="T37" id="Seg_397" s="T33">KFN_196X_DrunkBear2_nar.010 (010)</ta>
            <ta e="T39" id="Seg_398" s="T37">KFN_196X_DrunkBear2_nar.011 (011)</ta>
            <ta e="T41" id="Seg_399" s="T39">KFN_196X_DrunkBear2_nar.012 (012)</ta>
            <ta e="T44" id="Seg_400" s="T41">KFN_196X_DrunkBear2_nar.013 (013)</ta>
            <ta e="T45" id="Seg_401" s="T44">KFN_196X_DrunkBear2_nar.014 (014)</ta>
            <ta e="T47" id="Seg_402" s="T45">KFN_196X_DrunkBear2_nar.015 (015)</ta>
            <ta e="T49" id="Seg_403" s="T47">KFN_196X_DrunkBear2_nar.016 (016)</ta>
            <ta e="T53" id="Seg_404" s="T49">KFN_196X_DrunkBear2_nar.017 (017)</ta>
            <ta e="T55" id="Seg_405" s="T53">KFN_196X_DrunkBear2_nar.018 (018)</ta>
            <ta e="T58" id="Seg_406" s="T55">KFN_196X_DrunkBear2_nar.019 (019)</ta>
            <ta e="T64" id="Seg_407" s="T58">KFN_196X_DrunkBear2_nar.020 (020)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_408" s="T0">Угот Тельпекагэт пчёл варэмбадэт.</ta>
            <ta e="T9" id="Seg_409" s="T4">Тучат корг тӧмба, мӧдэм авешпат.</ta>
            <ta e="T13" id="Seg_410" s="T9">Табэт коштэмбат, мёдом авешпат.</ta>
            <ta e="T15" id="Seg_411" s="T13">Парот мемындат.</ta>
            <ta e="T21" id="Seg_412" s="T15">Начат чочэмбат кочек парыдэп, коргоп шэрбат.</ta>
            <ta e="T23" id="Seg_413" s="T21">коргоп шэрбат.</ta>
            <ta e="T26" id="Seg_414" s="T23">Элле альчемба, қондалба.</ta>
            <ta e="T31" id="Seg_415" s="T26">Тапет шэд чундэм арельбат челеганд. </ta>
            <ta e="T33" id="Seg_416" s="T31">Начат квэнбат.</ta>
            <ta e="T37" id="Seg_417" s="T33">Телеганд тӓльджэмбат коргоп қондалбэл.</ta>
            <ta e="T39" id="Seg_418" s="T37">Могунэ квэндэгу.</ta>
            <ta e="T41" id="Seg_419" s="T39">Могунэ ӱгорэмбат.</ta>
            <ta e="T44" id="Seg_420" s="T41">Корӷo ватот қэльчемба.</ta>
            <ta e="T45" id="Seg_421" s="T44">Паркелымба.</ta>
            <ta e="T47" id="Seg_422" s="T45">Чундэт қычватпат.</ta>
            <ta e="T49" id="Seg_423" s="T47">қотэқ парчаӷэлбат.</ta>
            <ta e="T53" id="Seg_424" s="T49">Вес телегап тақ паналбат.</ta>
            <ta e="T55" id="Seg_425" s="T53">қорӷo қвэнба.</ta>
            <ta e="T58" id="Seg_426" s="T55">Табат чвэссе тӧат.</ta>
            <ta e="T64" id="Seg_427" s="T58">Телега най чаңгва, қорӷo най чаңгва.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_428" s="T0">Ugot Telʼpekaget pčʼёl varembadet.</ta>
            <ta e="T9" id="Seg_429" s="T4">Tučʼat korg tömba, mödem avešpat.</ta>
            <ta e="T13" id="Seg_430" s="T9">Tabet koštembat, mёdom avešpat.</ta>
            <ta e="T15" id="Seg_431" s="T13">Parot memɨndat.</ta>
            <ta e="T21" id="Seg_432" s="T15">Načat čʼočembat kočʼek parɨdep, korgop šerbat.</ta>
            <ta e="T23" id="Seg_433" s="T21">korgop šerbat.</ta>
            <ta e="T26" id="Seg_434" s="T23">Elle alʼčemba, qondalba.</ta>
            <ta e="T31" id="Seg_435" s="T26">Tapet šed čundem arelʼbat čelegand. </ta>
            <ta e="T33" id="Seg_436" s="T31">Načat kwenbat.</ta>
            <ta e="T37" id="Seg_437" s="T33">Čelegand tälʼǯembat korgop qondalbel.</ta>
            <ta e="T39" id="Seg_438" s="T37">Mogune kwendegu.</ta>
            <ta e="T41" id="Seg_439" s="T39">Mogune ügorembat.</ta>
            <ta e="T44" id="Seg_440" s="T41">Korɣo vatot qelʼčʼemba.</ta>
            <ta e="T45" id="Seg_441" s="T44">Parkelɨmba.</ta>
            <ta e="T47" id="Seg_442" s="T45">Čundet qɨčwatpat.</ta>
            <ta e="T49" id="Seg_443" s="T47">qoteq parčaɣelbat.</ta>
            <ta e="T53" id="Seg_444" s="T49">Wes čelegap taq panalbat.</ta>
            <ta e="T55" id="Seg_445" s="T53">qorɣo qwenba.</ta>
            <ta e="T58" id="Seg_446" s="T55">Tabat čwesse töat.</ta>
            <ta e="T64" id="Seg_447" s="T58">Čelega naj čaŋgwa, qorɣo naj čaŋgva.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_448" s="T0">Ugot Telʼpekaqət čʼol warembad. </ta>
            <ta e="T9" id="Seg_449" s="T4">Načʼat qorqə tömba, mʼodom awešpat. </ta>
            <ta e="T13" id="Seg_450" s="T9">Tapet qoštembat, mʼodom awešpat. </ta>
            <ta e="T15" id="Seg_451" s="T13">Parət mʼeːmɨndat. </ta>
            <ta e="T21" id="Seg_452" s="T15">Načʼat čočembat kočʼek paridep, qorqop šerbat. </ta>
            <ta e="T23" id="Seg_453" s="T21">korgə šerbat. </ta>
            <ta e="T26" id="Seg_454" s="T23">Elʼlʼe alʼčimba, qondalba. </ta>
            <ta e="T31" id="Seg_455" s="T26">Tapət šɨdə čʼundəm arelʼbat čʼelʼegand. </ta>
            <ta e="T33" id="Seg_456" s="T31">Načʼat qwɛnbat. </ta>
            <ta e="T37" id="Seg_457" s="T33">Čʼelʼegand tälʼǯembat, qorqo qondalbəl. </ta>
            <ta e="T39" id="Seg_458" s="T37">Mogunä qwendegu. </ta>
            <ta e="T41" id="Seg_459" s="T39">Mogunä üːgorembat. </ta>
            <ta e="T44" id="Seg_460" s="T41">Qorqo watot qelʼčʼimba. </ta>
            <ta e="T45" id="Seg_461" s="T44">Parkeːlɨmba. </ta>
            <ta e="T47" id="Seg_462" s="T45">Čʼundət qačʼiwatpat. </ta>
            <ta e="T49" id="Seg_463" s="T47">Qoteq torčaːɣəlbat. </ta>
            <ta e="T53" id="Seg_464" s="T49">Wesʼ čʼelʼegap taq panalbat. </ta>
            <ta e="T55" id="Seg_465" s="T53">Qorqo qwenba. </ta>
            <ta e="T58" id="Seg_466" s="T55">Tabat čʼwesse töːwat. </ta>
            <ta e="T64" id="Seg_467" s="T58">Čʼelʼega naj čʼaŋgwa, qorqə naj čʼaŋgwa. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_468" s="T0">ugot</ta>
            <ta e="T2" id="Seg_469" s="T1">Telʼpeka-qət</ta>
            <ta e="T3" id="Seg_470" s="T2">čʼol</ta>
            <ta e="T4" id="Seg_471" s="T3">ware-mba-d</ta>
            <ta e="T5" id="Seg_472" s="T4">načʼat</ta>
            <ta e="T6" id="Seg_473" s="T5">qorqə</ta>
            <ta e="T7" id="Seg_474" s="T6">tö-mba</ta>
            <ta e="T8" id="Seg_475" s="T7">mʼod-o-m</ta>
            <ta e="T9" id="Seg_476" s="T8">aw-e-špa-t</ta>
            <ta e="T10" id="Seg_477" s="T9">tap-e-t</ta>
            <ta e="T11" id="Seg_478" s="T10">qošte-mba-t</ta>
            <ta e="T12" id="Seg_479" s="T11">mʼod-o-m</ta>
            <ta e="T13" id="Seg_480" s="T12">aw-e-špa-t</ta>
            <ta e="T14" id="Seg_481" s="T13">parət</ta>
            <ta e="T15" id="Seg_482" s="T14">mʼeː-mɨ-nda-t</ta>
            <ta e="T16" id="Seg_483" s="T15">načʼat</ta>
            <ta e="T17" id="Seg_484" s="T16">čoče-mba-t</ta>
            <ta e="T18" id="Seg_485" s="T17">kočʼek</ta>
            <ta e="T19" id="Seg_486" s="T18">paride-p</ta>
            <ta e="T20" id="Seg_487" s="T19">qorqo-p</ta>
            <ta e="T21" id="Seg_488" s="T20">šer-ba-t</ta>
            <ta e="T22" id="Seg_489" s="T21">qorqə</ta>
            <ta e="T23" id="Seg_490" s="T22">šer-ba-t</ta>
            <ta e="T24" id="Seg_491" s="T23">elʼlʼe</ta>
            <ta e="T25" id="Seg_492" s="T24">alʼči-mba</ta>
            <ta e="T26" id="Seg_493" s="T25">qonda-l-ba</ta>
            <ta e="T27" id="Seg_494" s="T26">tap-ə-t</ta>
            <ta e="T28" id="Seg_495" s="T27">šɨdə</ta>
            <ta e="T29" id="Seg_496" s="T28">čʼund-ə-m</ta>
            <ta e="T30" id="Seg_497" s="T29">arelʼ-ba-t</ta>
            <ta e="T31" id="Seg_498" s="T30">čʼelʼega-nd</ta>
            <ta e="T32" id="Seg_499" s="T31">načʼa-t</ta>
            <ta e="T33" id="Seg_500" s="T32">qwen-ba-t</ta>
            <ta e="T34" id="Seg_501" s="T33">čʼelʼega-nd</ta>
            <ta e="T35" id="Seg_502" s="T34">tälʼǯe-mba-t</ta>
            <ta e="T36" id="Seg_503" s="T35">qorqo</ta>
            <ta e="T37" id="Seg_504" s="T36">qonda-l-bəl</ta>
            <ta e="T38" id="Seg_505" s="T37">mogunä</ta>
            <ta e="T39" id="Seg_506" s="T38">qwen-de-gu</ta>
            <ta e="T40" id="Seg_507" s="T39">mogunä</ta>
            <ta e="T41" id="Seg_508" s="T40">üː-go-r-e-mba-t</ta>
            <ta e="T42" id="Seg_509" s="T41">qorqo</ta>
            <ta e="T43" id="Seg_510" s="T42">wat-ot</ta>
            <ta e="T44" id="Seg_511" s="T43">qelʼčʼi-mba</ta>
            <ta e="T45" id="Seg_512" s="T44">parkeː-lɨ-mba</ta>
            <ta e="T46" id="Seg_513" s="T45">čʼund-ə-t</ta>
            <ta e="T47" id="Seg_514" s="T46">qačʼiwat-pa-t</ta>
            <ta e="T48" id="Seg_515" s="T47">qoteq</ta>
            <ta e="T49" id="Seg_516" s="T48">torčaː-ɣəl-ba-t</ta>
            <ta e="T50" id="Seg_517" s="T49">wesʼ</ta>
            <ta e="T51" id="Seg_518" s="T50">čʼelʼega-p</ta>
            <ta e="T52" id="Seg_519" s="T51">taq</ta>
            <ta e="T53" id="Seg_520" s="T52">panal-ba-t</ta>
            <ta e="T54" id="Seg_521" s="T53">qorqo</ta>
            <ta e="T55" id="Seg_522" s="T54">qwen-ba</ta>
            <ta e="T56" id="Seg_523" s="T55">tab-a-t</ta>
            <ta e="T57" id="Seg_524" s="T56">čʼwesse</ta>
            <ta e="T58" id="Seg_525" s="T57">töː-wa-t</ta>
            <ta e="T59" id="Seg_526" s="T58">čʼelʼega</ta>
            <ta e="T60" id="Seg_527" s="T59">naj</ta>
            <ta e="T61" id="Seg_528" s="T60">čʼaŋg-wa</ta>
            <ta e="T62" id="Seg_529" s="T61">qorqo</ta>
            <ta e="T63" id="Seg_530" s="T62">naj</ta>
            <ta e="T64" id="Seg_531" s="T63">čʼaŋg-wa</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_532" s="T0">ugon</ta>
            <ta e="T2" id="Seg_533" s="T1">Telʼpeka-qɨn</ta>
            <ta e="T3" id="Seg_534" s="T2">čʼoːlo</ta>
            <ta e="T4" id="Seg_535" s="T3">warɨ-mbɨ-dət</ta>
            <ta e="T5" id="Seg_536" s="T4">nača</ta>
            <ta e="T6" id="Seg_537" s="T5">qorqɨ</ta>
            <ta e="T7" id="Seg_538" s="T6">töː-mbɨ</ta>
            <ta e="T8" id="Seg_539" s="T7">mʼod-ɨ-m</ta>
            <ta e="T9" id="Seg_540" s="T8">am-ɨ-špɨ-tɨ</ta>
            <ta e="T10" id="Seg_541" s="T9">tab-ɨ-t</ta>
            <ta e="T11" id="Seg_542" s="T10">koštɨ-mbɨ-dət</ta>
            <ta e="T12" id="Seg_543" s="T11">mʼod-ɨ-m</ta>
            <ta e="T13" id="Seg_544" s="T12">am-ɨ-špɨ-tɨ</ta>
            <ta e="T14" id="Seg_545" s="T13">parüdi</ta>
            <ta e="T15" id="Seg_546" s="T14">me-mbɨ-ndɨ-dət</ta>
            <ta e="T16" id="Seg_547" s="T15">nača</ta>
            <ta e="T17" id="Seg_548" s="T16">čatči-mbɨ-dət</ta>
            <ta e="T18" id="Seg_549" s="T17">koček</ta>
            <ta e="T19" id="Seg_550" s="T18">parüdi-p</ta>
            <ta e="T20" id="Seg_551" s="T19">qorqɨ-m</ta>
            <ta e="T21" id="Seg_552" s="T20">əːšeːr-mbɨ-dət</ta>
            <ta e="T22" id="Seg_553" s="T21">qorqɨ</ta>
            <ta e="T23" id="Seg_554" s="T22">əːšeːr-mbɨ-dət</ta>
            <ta e="T24" id="Seg_555" s="T23">illä</ta>
            <ta e="T25" id="Seg_556" s="T24">alʼči-mbɨ</ta>
            <ta e="T26" id="Seg_557" s="T25">qontɨ-lɨ-mbɨ</ta>
            <ta e="T27" id="Seg_558" s="T26">tab-ɨ-t</ta>
            <ta e="T28" id="Seg_559" s="T27">šitə</ta>
            <ta e="T29" id="Seg_560" s="T28">čünd-ɨ-m</ta>
            <ta e="T30" id="Seg_561" s="T29">arel-mbɨ-dət</ta>
            <ta e="T31" id="Seg_562" s="T30">čelʼega-nde</ta>
            <ta e="T32" id="Seg_563" s="T31">nača-t</ta>
            <ta e="T33" id="Seg_564" s="T32">qwän-mbɨ-dət</ta>
            <ta e="T34" id="Seg_565" s="T33">čelʼega-nde</ta>
            <ta e="T35" id="Seg_566" s="T34">tälʼǯe-mbɨ-dət</ta>
            <ta e="T36" id="Seg_567" s="T35">qorqɨ</ta>
            <ta e="T37" id="Seg_568" s="T36">qontɨ-lɨ-mbɨlʼe</ta>
            <ta e="T38" id="Seg_569" s="T37">moqne</ta>
            <ta e="T39" id="Seg_570" s="T38">qwän-dɨ-gu</ta>
            <ta e="T40" id="Seg_571" s="T39">moqne</ta>
            <ta e="T41" id="Seg_572" s="T40">ü-ku-r-ɨ-mbɨ-dət</ta>
            <ta e="T42" id="Seg_573" s="T41">qorqɨ</ta>
            <ta e="T43" id="Seg_574" s="T42">wattə-ut</ta>
            <ta e="T44" id="Seg_575" s="T43">qelʼčǝ-mbɨ</ta>
            <ta e="T45" id="Seg_576" s="T44">parka-lɨ-mbɨ</ta>
            <ta e="T46" id="Seg_577" s="T45">čünd-ɨ-t</ta>
            <ta e="T47" id="Seg_578" s="T46">kačwat-mbɨ-dət</ta>
            <ta e="T48" id="Seg_579" s="T47">qoteq</ta>
            <ta e="T49" id="Seg_580" s="T48">torčɛ-qəl-mbɨ-dət</ta>
            <ta e="T50" id="Seg_581" s="T49">wesʼ</ta>
            <ta e="T51" id="Seg_582" s="T50">čelʼega-p</ta>
            <ta e="T52" id="Seg_583" s="T51">tak</ta>
            <ta e="T53" id="Seg_584" s="T52">panal-mbɨ-tɨ</ta>
            <ta e="T54" id="Seg_585" s="T53">qorqɨ</ta>
            <ta e="T55" id="Seg_586" s="T54">qwän-mbɨ</ta>
            <ta e="T56" id="Seg_587" s="T55">tab-ɨ-t</ta>
            <ta e="T57" id="Seg_588" s="T56">čwesse</ta>
            <ta e="T58" id="Seg_589" s="T57">töː-wa-dət</ta>
            <ta e="T59" id="Seg_590" s="T58">čelʼega</ta>
            <ta e="T60" id="Seg_591" s="T59">naj</ta>
            <ta e="T61" id="Seg_592" s="T60">čaŋgɨ-wa</ta>
            <ta e="T62" id="Seg_593" s="T61">qorqɨ</ta>
            <ta e="T63" id="Seg_594" s="T62">naj</ta>
            <ta e="T64" id="Seg_595" s="T63">čaŋgɨ-wa</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_596" s="T0">earlier</ta>
            <ta e="T2" id="Seg_597" s="T1">Telpeka-LOC</ta>
            <ta e="T3" id="Seg_598" s="T2">bee.[NOM]</ta>
            <ta e="T4" id="Seg_599" s="T3">hold-PST.NAR-3PL</ta>
            <ta e="T5" id="Seg_600" s="T4">there</ta>
            <ta e="T6" id="Seg_601" s="T5">bear.[NOM]</ta>
            <ta e="T7" id="Seg_602" s="T6">come-PST.NAR.[3SG.S]</ta>
            <ta e="T8" id="Seg_603" s="T7">honey-EP-ACC</ta>
            <ta e="T9" id="Seg_604" s="T8">eat-EP-IPFV2-3SG.O</ta>
            <ta e="T10" id="Seg_605" s="T9">(s)he-EP-PL</ta>
            <ta e="T11" id="Seg_606" s="T10">find.out-PST.NAR-3PL</ta>
            <ta e="T12" id="Seg_607" s="T11">honey-EP-ACC</ta>
            <ta e="T13" id="Seg_608" s="T12">eat-EP-IPFV2-3SG.O</ta>
            <ta e="T14" id="Seg_609" s="T13">vodka.[NOM]</ta>
            <ta e="T15" id="Seg_610" s="T14">do-PST.NAR-INFER-3PL</ta>
            <ta e="T16" id="Seg_611" s="T15">there</ta>
            <ta e="T17" id="Seg_612" s="T16">put-PST.NAR-3PL</ta>
            <ta e="T18" id="Seg_613" s="T17">much</ta>
            <ta e="T19" id="Seg_614" s="T18">vodka-ACC</ta>
            <ta e="T20" id="Seg_615" s="T19">bear-ACC</ta>
            <ta e="T21" id="Seg_616" s="T20">get.drunk-PST.NAR-3PL</ta>
            <ta e="T22" id="Seg_617" s="T21">bear.[NOM]</ta>
            <ta e="T23" id="Seg_618" s="T22">get.drunk-PST.NAR-3PL</ta>
            <ta e="T24" id="Seg_619" s="T23">down</ta>
            <ta e="T25" id="Seg_620" s="T24">fall-PST.NAR.[3SG.S]</ta>
            <ta e="T26" id="Seg_621" s="T25">sleep-RES-PST.NAR.[3SG.S]</ta>
            <ta e="T27" id="Seg_622" s="T26">(s)he-EP-PL</ta>
            <ta e="T28" id="Seg_623" s="T27">two</ta>
            <ta e="T29" id="Seg_624" s="T28">horse-EP-ACC</ta>
            <ta e="T30" id="Seg_625" s="T29">tie-PST.NAR-3PL</ta>
            <ta e="T31" id="Seg_626" s="T30">cart-ILL</ta>
            <ta e="T32" id="Seg_627" s="T31">there-%%</ta>
            <ta e="T33" id="Seg_628" s="T32">go.away-PST.NAR-3PL</ta>
            <ta e="T34" id="Seg_629" s="T33">telega-ILL</ta>
            <ta e="T35" id="Seg_630" s="T34">load-PST.NAR-3PL</ta>
            <ta e="T36" id="Seg_631" s="T35">bear.[NOM]</ta>
            <ta e="T37" id="Seg_632" s="T36">sleep-RES-PTCP.PST</ta>
            <ta e="T38" id="Seg_633" s="T37">home</ta>
            <ta e="T39" id="Seg_634" s="T38">go.away-TR-INF</ta>
            <ta e="T40" id="Seg_635" s="T39">home</ta>
            <ta e="T41" id="Seg_636" s="T40">pull-HAB-FRQ-EP-PST.NAR-3PL</ta>
            <ta e="T42" id="Seg_637" s="T41">bear.[NOM]</ta>
            <ta e="T43" id="Seg_638" s="T42">road-PROL</ta>
            <ta e="T44" id="Seg_639" s="T43">wake.up-PST.NAR.[3SG.S]</ta>
            <ta e="T45" id="Seg_640" s="T44">shout-RES-PST.NAR.[3SG.S]</ta>
            <ta e="T46" id="Seg_641" s="T45">horse-EP-PL</ta>
            <ta e="T47" id="Seg_642" s="T46">frighten-PST.NAR-3PL</ta>
            <ta e="T48" id="Seg_643" s="T47">completely</ta>
            <ta e="T49" id="Seg_644" s="T48">run-MULS-PST.NAR-3PL</ta>
            <ta e="T50" id="Seg_645" s="T49">all</ta>
            <ta e="T51" id="Seg_646" s="T50">telega-ACC</ta>
            <ta e="T52" id="Seg_647" s="T51">away</ta>
            <ta e="T53" id="Seg_648" s="T52">break-PST.NAR-3SG.O</ta>
            <ta e="T54" id="Seg_649" s="T53">bear.[NOM]</ta>
            <ta e="T55" id="Seg_650" s="T54">go.away-PST.NAR.[3SG.S]</ta>
            <ta e="T56" id="Seg_651" s="T55">(s)he-EP-PL</ta>
            <ta e="T57" id="Seg_652" s="T56">backward</ta>
            <ta e="T58" id="Seg_653" s="T57">come-CO-3PL</ta>
            <ta e="T59" id="Seg_654" s="T58">cart.[NOM]</ta>
            <ta e="T60" id="Seg_655" s="T59">also</ta>
            <ta e="T61" id="Seg_656" s="T60">NEG.EX-CO.[3SG.S]</ta>
            <ta e="T62" id="Seg_657" s="T61">bear.[NOM]</ta>
            <ta e="T63" id="Seg_658" s="T62">also</ta>
            <ta e="T64" id="Seg_659" s="T63">NEG.EX-CO.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_660" s="T0">раньше</ta>
            <ta e="T2" id="Seg_661" s="T1">Тельпека-LOC</ta>
            <ta e="T3" id="Seg_662" s="T2">пчела.[NOM]</ta>
            <ta e="T4" id="Seg_663" s="T3">держать-PST.NAR-3PL</ta>
            <ta e="T5" id="Seg_664" s="T4">туда</ta>
            <ta e="T6" id="Seg_665" s="T5">медведь.[NOM]</ta>
            <ta e="T7" id="Seg_666" s="T6">прийти-PST.NAR.[3SG.S]</ta>
            <ta e="T8" id="Seg_667" s="T7">мёд-EP-ACC</ta>
            <ta e="T9" id="Seg_668" s="T8">есть-EP-IPFV2-3SG.O</ta>
            <ta e="T10" id="Seg_669" s="T9">он(а)-EP-PL</ta>
            <ta e="T11" id="Seg_670" s="T10">узнать-PST.NAR-3PL</ta>
            <ta e="T12" id="Seg_671" s="T11">мёд-EP-ACC</ta>
            <ta e="T13" id="Seg_672" s="T12">есть-EP-IPFV2-3SG.O</ta>
            <ta e="T14" id="Seg_673" s="T13">водка.[NOM]</ta>
            <ta e="T15" id="Seg_674" s="T14">делать-PST.NAR-INFER-3PL</ta>
            <ta e="T16" id="Seg_675" s="T15">туда</ta>
            <ta e="T17" id="Seg_676" s="T16">поставить-PST.NAR-3PL</ta>
            <ta e="T18" id="Seg_677" s="T17">много</ta>
            <ta e="T19" id="Seg_678" s="T18">водка-ACC</ta>
            <ta e="T20" id="Seg_679" s="T19">медведь-ACC</ta>
            <ta e="T21" id="Seg_680" s="T20">опьянеть-PST.NAR-3PL</ta>
            <ta e="T22" id="Seg_681" s="T21">медведь.[NOM]</ta>
            <ta e="T23" id="Seg_682" s="T22">опьянеть-PST.NAR-3PL</ta>
            <ta e="T24" id="Seg_683" s="T23">вниз</ta>
            <ta e="T25" id="Seg_684" s="T24">упасть-PST.NAR.[3SG.S]</ta>
            <ta e="T26" id="Seg_685" s="T25">спять-RES-PST.NAR.[3SG.S]</ta>
            <ta e="T27" id="Seg_686" s="T26">он(а)-EP-PL</ta>
            <ta e="T28" id="Seg_687" s="T27">два</ta>
            <ta e="T29" id="Seg_688" s="T28">лошадь-EP-ACC</ta>
            <ta e="T30" id="Seg_689" s="T29">связать-PST.NAR-3PL</ta>
            <ta e="T31" id="Seg_690" s="T30">телега-ILL</ta>
            <ta e="T32" id="Seg_691" s="T31">туда-%%</ta>
            <ta e="T33" id="Seg_692" s="T32">пойти-PST.NAR-3PL</ta>
            <ta e="T34" id="Seg_693" s="T33">cart-ILL</ta>
            <ta e="T35" id="Seg_694" s="T34">погрузить-PST.NAR-3PL</ta>
            <ta e="T36" id="Seg_695" s="T35">медведь.[NOM]</ta>
            <ta e="T37" id="Seg_696" s="T36">спять-RES-PTCP.PST</ta>
            <ta e="T38" id="Seg_697" s="T37">домой</ta>
            <ta e="T39" id="Seg_698" s="T38">пойти-TR-INF</ta>
            <ta e="T40" id="Seg_699" s="T39">домой</ta>
            <ta e="T41" id="Seg_700" s="T40">тащить-HAB-FRQ-EP-PST.NAR-3PL</ta>
            <ta e="T42" id="Seg_701" s="T41">медведь.[NOM]</ta>
            <ta e="T43" id="Seg_702" s="T42">путь-PROL</ta>
            <ta e="T44" id="Seg_703" s="T43">проснуться-PST.NAR.[3SG.S]</ta>
            <ta e="T45" id="Seg_704" s="T44">кричать-RES-PST.NAR.[3SG.S]</ta>
            <ta e="T46" id="Seg_705" s="T45">лошадь-EP-PL</ta>
            <ta e="T47" id="Seg_706" s="T46">испугаться-PST.NAR-3PL</ta>
            <ta e="T48" id="Seg_707" s="T47">совсем</ta>
            <ta e="T49" id="Seg_708" s="T48">бегать-MULS-PST.NAR-3PL</ta>
            <ta e="T50" id="Seg_709" s="T49">весь</ta>
            <ta e="T51" id="Seg_710" s="T50">cart-ACC</ta>
            <ta e="T52" id="Seg_711" s="T51">прочь</ta>
            <ta e="T53" id="Seg_712" s="T52">сломать-PST.NAR-3SG.O</ta>
            <ta e="T54" id="Seg_713" s="T53">медведь.[NOM]</ta>
            <ta e="T55" id="Seg_714" s="T54">пойти-PST.NAR.[3SG.S]</ta>
            <ta e="T56" id="Seg_715" s="T55">он(а)-EP-PL</ta>
            <ta e="T57" id="Seg_716" s="T56">назад</ta>
            <ta e="T58" id="Seg_717" s="T57">прийти-CO-3PL</ta>
            <ta e="T59" id="Seg_718" s="T58">телега.[NOM]</ta>
            <ta e="T60" id="Seg_719" s="T59">тоже</ta>
            <ta e="T61" id="Seg_720" s="T60">NEG.EX-CO.[3SG.S]</ta>
            <ta e="T62" id="Seg_721" s="T61">медведь.[NOM]</ta>
            <ta e="T63" id="Seg_722" s="T62">тоже</ta>
            <ta e="T64" id="Seg_723" s="T63">NEG.EX-CO.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_724" s="T0">adv</ta>
            <ta e="T2" id="Seg_725" s="T1">nprop-n:case</ta>
            <ta e="T3" id="Seg_726" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_727" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_728" s="T4">adv</ta>
            <ta e="T6" id="Seg_729" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_730" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_731" s="T7">n-n:ins-n:case</ta>
            <ta e="T9" id="Seg_732" s="T8">v-n:ins-v&gt;v-v:pn</ta>
            <ta e="T10" id="Seg_733" s="T9">pers-n:ins-n:num</ta>
            <ta e="T11" id="Seg_734" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_735" s="T11">n-n:ins-n:case</ta>
            <ta e="T13" id="Seg_736" s="T12">v-n:ins-v&gt;v-v:pn</ta>
            <ta e="T14" id="Seg_737" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_738" s="T14">v-v:tense-v:mood-v:pn</ta>
            <ta e="T16" id="Seg_739" s="T15">adv</ta>
            <ta e="T17" id="Seg_740" s="T16">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_741" s="T17">quant</ta>
            <ta e="T19" id="Seg_742" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_743" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_744" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_745" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_746" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_747" s="T23">preverb</ta>
            <ta e="T25" id="Seg_748" s="T24">v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_749" s="T25">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_750" s="T26">pers-n:ins-n:num</ta>
            <ta e="T28" id="Seg_751" s="T27">num</ta>
            <ta e="T29" id="Seg_752" s="T28">n-n:ins-n:case</ta>
            <ta e="T30" id="Seg_753" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_754" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_755" s="T31">adv</ta>
            <ta e="T33" id="Seg_756" s="T32">v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_757" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_758" s="T34">v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_759" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_760" s="T36">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T38" id="Seg_761" s="T37">adv</ta>
            <ta e="T39" id="Seg_762" s="T38">v-v&gt;v-v:inf</ta>
            <ta e="T40" id="Seg_763" s="T39">adv</ta>
            <ta e="T41" id="Seg_764" s="T40">v-v&gt;v-v&gt;v-n:ins-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_765" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_766" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_767" s="T43">v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_768" s="T44">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_769" s="T45">n-n:ins-n:num</ta>
            <ta e="T47" id="Seg_770" s="T46">v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_771" s="T47">adv</ta>
            <ta e="T49" id="Seg_772" s="T48">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_773" s="T49">quant</ta>
            <ta e="T51" id="Seg_774" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_775" s="T51">preverb</ta>
            <ta e="T53" id="Seg_776" s="T52">v-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_777" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_778" s="T54">v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_779" s="T55">pers-n:ins-n:case</ta>
            <ta e="T57" id="Seg_780" s="T56">adv</ta>
            <ta e="T58" id="Seg_781" s="T57">v-v:ins-v:pn</ta>
            <ta e="T59" id="Seg_782" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_783" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_784" s="T60">v-v:ins-v:pn</ta>
            <ta e="T62" id="Seg_785" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_786" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_787" s="T63">v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_788" s="T0">adv</ta>
            <ta e="T2" id="Seg_789" s="T1">nprop</ta>
            <ta e="T3" id="Seg_790" s="T2">n</ta>
            <ta e="T4" id="Seg_791" s="T3">v</ta>
            <ta e="T5" id="Seg_792" s="T4">adv</ta>
            <ta e="T6" id="Seg_793" s="T5">n</ta>
            <ta e="T7" id="Seg_794" s="T6">v</ta>
            <ta e="T8" id="Seg_795" s="T7">n</ta>
            <ta e="T9" id="Seg_796" s="T8">v</ta>
            <ta e="T10" id="Seg_797" s="T9">pers</ta>
            <ta e="T11" id="Seg_798" s="T10">v</ta>
            <ta e="T12" id="Seg_799" s="T11">n</ta>
            <ta e="T13" id="Seg_800" s="T12">v</ta>
            <ta e="T14" id="Seg_801" s="T13">n</ta>
            <ta e="T15" id="Seg_802" s="T14">v</ta>
            <ta e="T16" id="Seg_803" s="T15">adv</ta>
            <ta e="T17" id="Seg_804" s="T16">v</ta>
            <ta e="T18" id="Seg_805" s="T17">quant</ta>
            <ta e="T19" id="Seg_806" s="T18">adj</ta>
            <ta e="T20" id="Seg_807" s="T19">n</ta>
            <ta e="T21" id="Seg_808" s="T20">v</ta>
            <ta e="T22" id="Seg_809" s="T21">n</ta>
            <ta e="T23" id="Seg_810" s="T22">v</ta>
            <ta e="T24" id="Seg_811" s="T23">preverb</ta>
            <ta e="T25" id="Seg_812" s="T24">v</ta>
            <ta e="T26" id="Seg_813" s="T25">v</ta>
            <ta e="T27" id="Seg_814" s="T26">pers</ta>
            <ta e="T28" id="Seg_815" s="T27">num</ta>
            <ta e="T29" id="Seg_816" s="T28">n</ta>
            <ta e="T30" id="Seg_817" s="T29">v</ta>
            <ta e="T31" id="Seg_818" s="T30">n</ta>
            <ta e="T32" id="Seg_819" s="T31">adv</ta>
            <ta e="T33" id="Seg_820" s="T32">v</ta>
            <ta e="T34" id="Seg_821" s="T33">n</ta>
            <ta e="T35" id="Seg_822" s="T34">v</ta>
            <ta e="T36" id="Seg_823" s="T35">n</ta>
            <ta e="T37" id="Seg_824" s="T36">v</ta>
            <ta e="T38" id="Seg_825" s="T37">adv</ta>
            <ta e="T39" id="Seg_826" s="T38">v</ta>
            <ta e="T40" id="Seg_827" s="T39">adv</ta>
            <ta e="T41" id="Seg_828" s="T40">v</ta>
            <ta e="T42" id="Seg_829" s="T41">n</ta>
            <ta e="T43" id="Seg_830" s="T42">n</ta>
            <ta e="T44" id="Seg_831" s="T43">v</ta>
            <ta e="T45" id="Seg_832" s="T44">v</ta>
            <ta e="T46" id="Seg_833" s="T45">n</ta>
            <ta e="T47" id="Seg_834" s="T46">v</ta>
            <ta e="T48" id="Seg_835" s="T47">adv</ta>
            <ta e="T49" id="Seg_836" s="T48">v</ta>
            <ta e="T50" id="Seg_837" s="T49">quant</ta>
            <ta e="T51" id="Seg_838" s="T50">n</ta>
            <ta e="T52" id="Seg_839" s="T51">preverb</ta>
            <ta e="T53" id="Seg_840" s="T52">v</ta>
            <ta e="T54" id="Seg_841" s="T53">n</ta>
            <ta e="T55" id="Seg_842" s="T54">v</ta>
            <ta e="T56" id="Seg_843" s="T55">pers</ta>
            <ta e="T57" id="Seg_844" s="T56">adv</ta>
            <ta e="T58" id="Seg_845" s="T57">v</ta>
            <ta e="T59" id="Seg_846" s="T58">n</ta>
            <ta e="T60" id="Seg_847" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_848" s="T60">v</ta>
            <ta e="T62" id="Seg_849" s="T61">n</ta>
            <ta e="T63" id="Seg_850" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_851" s="T63">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_852" s="T0">adv:Time</ta>
            <ta e="T2" id="Seg_853" s="T1">np:L</ta>
            <ta e="T3" id="Seg_854" s="T2">np:Th</ta>
            <ta e="T4" id="Seg_855" s="T3">0.3.h:A </ta>
            <ta e="T5" id="Seg_856" s="T4">adv:G</ta>
            <ta e="T6" id="Seg_857" s="T5">np:A</ta>
            <ta e="T8" id="Seg_858" s="T7">np:P</ta>
            <ta e="T9" id="Seg_859" s="T8">0.3:A</ta>
            <ta e="T10" id="Seg_860" s="T9">pro.h:E</ta>
            <ta e="T12" id="Seg_861" s="T11">np:P</ta>
            <ta e="T13" id="Seg_862" s="T12">0.3:A</ta>
            <ta e="T14" id="Seg_863" s="T13">np:P</ta>
            <ta e="T15" id="Seg_864" s="T14">0.3.h:A </ta>
            <ta e="T16" id="Seg_865" s="T15">adv:G</ta>
            <ta e="T17" id="Seg_866" s="T16">0.3.h:A </ta>
            <ta e="T19" id="Seg_867" s="T18">np:Th</ta>
            <ta e="T20" id="Seg_868" s="T19">np:P</ta>
            <ta e="T21" id="Seg_869" s="T20">0.3.h:A</ta>
            <ta e="T22" id="Seg_870" s="T21">np:P</ta>
            <ta e="T23" id="Seg_871" s="T22">0.3.h:A</ta>
            <ta e="T25" id="Seg_872" s="T24">0.3:P</ta>
            <ta e="T26" id="Seg_873" s="T25">0.3:P</ta>
            <ta e="T27" id="Seg_874" s="T26">pro.h:A</ta>
            <ta e="T29" id="Seg_875" s="T28">np:Th</ta>
            <ta e="T31" id="Seg_876" s="T30">np:G</ta>
            <ta e="T32" id="Seg_877" s="T31">adv:G</ta>
            <ta e="T33" id="Seg_878" s="T32">0.3.h:A</ta>
            <ta e="T34" id="Seg_879" s="T33">np:G</ta>
            <ta e="T35" id="Seg_880" s="T34">0.3.h:A</ta>
            <ta e="T36" id="Seg_881" s="T35">np:Th</ta>
            <ta e="T38" id="Seg_882" s="T37">adv:G</ta>
            <ta e="T40" id="Seg_883" s="T39">adv:G</ta>
            <ta e="T41" id="Seg_884" s="T40">0.3.h:A</ta>
            <ta e="T42" id="Seg_885" s="T41">np:P</ta>
            <ta e="T43" id="Seg_886" s="T42">np:Path</ta>
            <ta e="T45" id="Seg_887" s="T44">0.3:A</ta>
            <ta e="T46" id="Seg_888" s="T45">np:E</ta>
            <ta e="T49" id="Seg_889" s="T48">0.3.h:A</ta>
            <ta e="T51" id="Seg_890" s="T50">np:P</ta>
            <ta e="T53" id="Seg_891" s="T52">0.3:A</ta>
            <ta e="T54" id="Seg_892" s="T53">np:A</ta>
            <ta e="T56" id="Seg_893" s="T55">pro.h:A</ta>
            <ta e="T57" id="Seg_894" s="T56">adv:G</ta>
            <ta e="T59" id="Seg_895" s="T58">np:Th</ta>
            <ta e="T62" id="Seg_896" s="T61">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_897" s="T3">0.3.h:S v:pred</ta>
            <ta e="T6" id="Seg_898" s="T5">np:S</ta>
            <ta e="T7" id="Seg_899" s="T6">v:pred</ta>
            <ta e="T8" id="Seg_900" s="T7">np:O</ta>
            <ta e="T9" id="Seg_901" s="T8">0.3:S v:pred</ta>
            <ta e="T10" id="Seg_902" s="T9">pro.h:S</ta>
            <ta e="T11" id="Seg_903" s="T10">v:pred</ta>
            <ta e="T12" id="Seg_904" s="T11">np:O</ta>
            <ta e="T13" id="Seg_905" s="T12">0.3:S v:pred</ta>
            <ta e="T14" id="Seg_906" s="T13">np:O</ta>
            <ta e="T15" id="Seg_907" s="T14">0.3.h:S v:pred</ta>
            <ta e="T17" id="Seg_908" s="T16">0.3.h:S v:pred</ta>
            <ta e="T19" id="Seg_909" s="T18">np:O</ta>
            <ta e="T20" id="Seg_910" s="T19">np:O</ta>
            <ta e="T21" id="Seg_911" s="T20">0.3.h:S v:pred</ta>
            <ta e="T22" id="Seg_912" s="T21">np:O</ta>
            <ta e="T23" id="Seg_913" s="T22">0.3.h:S v:pred</ta>
            <ta e="T25" id="Seg_914" s="T24">0.3:S v:pred</ta>
            <ta e="T26" id="Seg_915" s="T25">0.3:S v:pred</ta>
            <ta e="T27" id="Seg_916" s="T26">pro.h:S</ta>
            <ta e="T29" id="Seg_917" s="T28">np:O</ta>
            <ta e="T30" id="Seg_918" s="T29">v:pred</ta>
            <ta e="T33" id="Seg_919" s="T32">0.3.h:S v:pred</ta>
            <ta e="T35" id="Seg_920" s="T34">0.3.h:S v:pred</ta>
            <ta e="T36" id="Seg_921" s="T35">np:S</ta>
            <ta e="T37" id="Seg_922" s="T36">adj:pred</ta>
            <ta e="T39" id="Seg_923" s="T37">s:purp</ta>
            <ta e="T41" id="Seg_924" s="T40">0.3.h:S v:pred</ta>
            <ta e="T42" id="Seg_925" s="T41">np:S</ta>
            <ta e="T44" id="Seg_926" s="T43">v:pred</ta>
            <ta e="T45" id="Seg_927" s="T44">0.3:S v:pred</ta>
            <ta e="T46" id="Seg_928" s="T45">np:S</ta>
            <ta e="T47" id="Seg_929" s="T46">v:pred</ta>
            <ta e="T49" id="Seg_930" s="T48">0.3.h:S v:pred</ta>
            <ta e="T51" id="Seg_931" s="T50">np:O</ta>
            <ta e="T53" id="Seg_932" s="T52">0.3:S v:pred</ta>
            <ta e="T54" id="Seg_933" s="T53">np:S</ta>
            <ta e="T55" id="Seg_934" s="T54">v:pred</ta>
            <ta e="T56" id="Seg_935" s="T55">pro.h:S</ta>
            <ta e="T58" id="Seg_936" s="T57">v:pred</ta>
            <ta e="T59" id="Seg_937" s="T58">np:S</ta>
            <ta e="T61" id="Seg_938" s="T60">v:pred</ta>
            <ta e="T62" id="Seg_939" s="T61">np:S</ta>
            <ta e="T64" id="Seg_940" s="T63">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_941" s="T2">RUS:cult</ta>
            <ta e="T8" id="Seg_942" s="T7">RUS:cult</ta>
            <ta e="T12" id="Seg_943" s="T11">RUS:cult</ta>
            <ta e="T31" id="Seg_944" s="T30">RUS:cult</ta>
            <ta e="T34" id="Seg_945" s="T33">RUS:cult</ta>
            <ta e="T50" id="Seg_946" s="T49">RUS:core</ta>
            <ta e="T51" id="Seg_947" s="T50">RUS:cult</ta>
            <ta e="T59" id="Seg_948" s="T58">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T3" id="Seg_949" s="T2">inCdel</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_950" s="T0">Раньше в Тельпеке пчел держали.</ta>
            <ta e="T9" id="Seg_951" s="T4">Туда медведь пришел, мед ест.</ta>
            <ta e="T13" id="Seg_952" s="T9">Они увидели, как он мед ест.</ta>
            <ta e="T15" id="Seg_953" s="T13">Водку сделали.</ta>
            <ta e="T21" id="Seg_954" s="T15">Туда поставили много водки, медведя напоили.</ta>
            <ta e="T23" id="Seg_955" s="T21">Медведя напоили.</ta>
            <ta e="T26" id="Seg_956" s="T23">Вниз упал, уснул.</ta>
            <ta e="T31" id="Seg_957" s="T26">Они двух лошадей в телегу запрягли.</ta>
            <ta e="T33" id="Seg_958" s="T31">Туда поехали.</ta>
            <ta e="T37" id="Seg_959" s="T33">В телегу погрузили [медведя], медведь спит.</ta>
            <ta e="T39" id="Seg_960" s="T37">Домой (назад) везти.</ta>
            <ta e="T41" id="Seg_961" s="T39">Домой тащат. </ta>
            <ta e="T44" id="Seg_962" s="T41">Медведь по дороге проснулся.</ta>
            <ta e="T45" id="Seg_963" s="T44">Закричал.</ta>
            <ta e="T47" id="Seg_964" s="T45">Лошади испугались.</ta>
            <ta e="T49" id="Seg_965" s="T47">Сильно побежали (?).</ta>
            <ta e="T53" id="Seg_966" s="T49">Всю телегу сломал.</ta>
            <ta e="T55" id="Seg_967" s="T53">Медведь ушёл.</ta>
            <ta e="T58" id="Seg_968" s="T55">Они назад пришли.</ta>
            <ta e="T64" id="Seg_969" s="T58">Телеги тоже нету, медведя тоже нету.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_970" s="T0">Earlier in Telpeka people had bees.</ta>
            <ta e="T9" id="Seg_971" s="T4">A bear was coming there, he was eating honey.</ta>
            <ta e="T13" id="Seg_972" s="T9">They saw him eating honey.</ta>
            <ta e="T15" id="Seg_973" s="T13">They made vodka.</ta>
            <ta e="T21" id="Seg_974" s="T15">They put much vodka there and made the bear drunk.</ta>
            <ta e="T23" id="Seg_975" s="T21">They made the bear drunk.</ta>
            <ta e="T26" id="Seg_976" s="T23">He fell down and fell asleep.</ta>
            <ta e="T31" id="Seg_977" s="T26">They harnessed two horses into a cart.</ta>
            <ta e="T33" id="Seg_978" s="T31">They went there.</ta>
            <ta e="T37" id="Seg_979" s="T33">They loaded [the bear] onto the cart, the sleeping bear.</ta>
            <ta e="T39" id="Seg_980" s="T37">To bring it home.</ta>
            <ta e="T41" id="Seg_981" s="T39">They pull it home.</ta>
            <ta e="T44" id="Seg_982" s="T41">Along the way the bear woke up.</ta>
            <ta e="T45" id="Seg_983" s="T44">He shouted.</ta>
            <ta e="T47" id="Seg_984" s="T45">The horses got frightened.</ta>
            <ta e="T49" id="Seg_985" s="T47">They gallopped off.</ta>
            <ta e="T53" id="Seg_986" s="T49">He broke the entire cart.</ta>
            <ta e="T55" id="Seg_987" s="T53">The bear went away.</ta>
            <ta e="T58" id="Seg_988" s="T55">They came back.</ta>
            <ta e="T64" id="Seg_989" s="T58">There is neither the cart, nor the bear.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_990" s="T0">Früher hielten die Leute in Telpeka Bienen.</ta>
            <ta e="T9" id="Seg_991" s="T4">Ein Bär kam dorthin, er aß Honig.</ta>
            <ta e="T13" id="Seg_992" s="T9">Sie sahen ihn Honig essen.</ta>
            <ta e="T15" id="Seg_993" s="T13">Sie machten Vodka.</ta>
            <ta e="T21" id="Seg_994" s="T15">Sie taten viel Vodka dorthin und machten den Bär betrunken.</ta>
            <ta e="T23" id="Seg_995" s="T21">Sie machten den Bären betrunken.</ta>
            <ta e="T26" id="Seg_996" s="T23">Er fiel zu Boden und schlief ein.</ta>
            <ta e="T31" id="Seg_997" s="T26">Sie spannten zwei Pferde vor einen Wagen.</ta>
            <ta e="T33" id="Seg_998" s="T31">Sie fuhren dorthin.</ta>
            <ta e="T37" id="Seg_999" s="T33">Auf den Wagen luden sie [ihn], der schlafende Bär.</ta>
            <ta e="T39" id="Seg_1000" s="T37">Um ihn nach Hause zu bringen.</ta>
            <ta e="T41" id="Seg_1001" s="T39">Sie zogen ihn nach Hause.</ta>
            <ta e="T44" id="Seg_1002" s="T41">Der Bär wachte auf dem Weg auf.</ta>
            <ta e="T45" id="Seg_1003" s="T44">Er schrie.</ta>
            <ta e="T47" id="Seg_1004" s="T45">Die Pferde bekamen Angst.</ta>
            <ta e="T49" id="Seg_1005" s="T47">Sie liefen davon.</ta>
            <ta e="T53" id="Seg_1006" s="T49">Er zerschlug den ganzen Wagen.</ta>
            <ta e="T55" id="Seg_1007" s="T53">Der Bär lief weg.</ta>
            <ta e="T58" id="Seg_1008" s="T55">Sie kamen zurück.</ta>
            <ta e="T64" id="Seg_1009" s="T58">Es gibt weder den Wagen noch den Bären.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_1010" s="T0">Раньше в Тельпекаге пчел держали.</ta>
            <ta e="T9" id="Seg_1011" s="T4">Туда медведь пришел, мед ест.</ta>
            <ta e="T13" id="Seg_1012" s="T9">Его увидел, мед ест.</ta>
            <ta e="T15" id="Seg_1013" s="T13">Поверхность сделали.</ta>
            <ta e="T21" id="Seg_1014" s="T15">Там поставил много водки, медведь опьянел.</ta>
            <ta e="T23" id="Seg_1015" s="T21">медведь опьянел.</ta>
            <ta e="T26" id="Seg_1016" s="T23">Вниз упал, уснул.</ta>
            <ta e="T31" id="Seg_1017" s="T26">Он двух лошадей в телегу запряг.</ta>
            <ta e="T33" id="Seg_1018" s="T31">Туда повезли.</ta>
            <ta e="T37" id="Seg_1019" s="T33">В телегу затащили (погрузили) медведя спящего.</ta>
            <ta e="T39" id="Seg_1020" s="T37">Домой (назад) везти.</ta>
            <ta e="T41" id="Seg_1021" s="T39">Домой тащат. </ta>
            <ta e="T44" id="Seg_1022" s="T41">Медведь по дороге проснулся.</ta>
            <ta e="T45" id="Seg_1023" s="T44">Закричал.</ta>
            <ta e="T47" id="Seg_1024" s="T45">Лошади испугались.</ta>
            <ta e="T49" id="Seg_1025" s="T47">Сильно побежали.</ta>
            <ta e="T53" id="Seg_1026" s="T49">Всю телегу (прочь) сломал.</ta>
            <ta e="T55" id="Seg_1027" s="T53">Медведь ушёл.</ta>
            <ta e="T58" id="Seg_1028" s="T55">Они назад пришли.</ta>
            <ta e="T64" id="Seg_1029" s="T58">Телеги тоже нету, медведя тоже нету.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T21" id="Seg_1030" s="T15">[WNB:] usually the verb is intransitive.</ta>
            <ta e="T23" id="Seg_1031" s="T21">[WNB:] Why is the verb in the objective conjugation? [AAV:] šerbat: palat. rʲ</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
