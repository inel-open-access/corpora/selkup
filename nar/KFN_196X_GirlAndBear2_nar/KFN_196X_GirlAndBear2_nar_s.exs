<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KFN_196X_GirlAndBear2_nar</transcription-name>
         <referenced-file url="KFN_196X_GirlAndBear2_nar.wav" />
         <referenced-file url="KFN_196X_GirlAndBear2_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KFN_196X_GirlAndBear2_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">113</ud-information>
            <ud-information attribute-name="# HIAT:w">86</ud-information>
            <ud-information attribute-name="# e">86</ud-information>
            <ud-information attribute-name="# HIAT:u">20</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KFN">
            <abbreviation>KFN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="2.82" type="appl" />
         <tli id="T1" time="3.855" type="appl" />
         <tli id="T2" time="4.89" type="appl" />
         <tli id="T3" time="5.925" type="appl" />
         <tli id="T4" time="8.112882615924239" />
         <tli id="T5" time="8.853" type="appl" />
         <tli id="T6" time="9.757" type="appl" />
         <tli id="T7" time="12.279317813091577" />
         <tli id="T8" time="12.913" type="appl" />
         <tli id="T9" time="13.688" type="appl" />
         <tli id="T10" time="14.463" type="appl" />
         <tli id="T11" time="15.238" type="appl" />
         <tli id="T12" time="16.013" type="appl" />
         <tli id="T13" time="18.07899560754851" />
         <tli id="T16" time="18.72562635014888" />
         <tli id="T14" time="19.425587463272993" />
         <tli id="T15" time="20.0855507985043" />
         <tli id="T17" time="21.385478580020507" />
         <tli id="T18" time="21.965446359466203" />
         <tli id="T19" time="22.726" type="appl" />
         <tli id="T20" time="24.198655625147893" />
         <tli id="T21" time="24.778" type="appl" />
         <tli id="T22" time="25.508" type="appl" />
         <tli id="T23" time="26.57185711345441" />
         <tli id="T24" time="27.097" type="appl" />
         <tli id="T25" time="27.671" type="appl" />
         <tli id="T26" time="28.245" type="appl" />
         <tli id="T27" time="28.819" type="appl" />
         <tli id="T28" time="30.6449641622052" />
         <tli id="T29" time="31.388" type="appl" />
         <tli id="T30" time="32.268" type="appl" />
         <tli id="T31" time="34.33142602465886" />
         <tli id="T32" time="34.88" type="appl" />
         <tli id="T33" time="35.713" type="appl" />
         <tli id="T34" time="36.546" type="appl" />
         <tli id="T35" time="37.378" type="appl" />
         <tli id="T36" time="38.626" type="appl" />
         <tli id="T37" time="39.308" type="appl" />
         <tli id="T38" time="39.99" type="appl" />
         <tli id="T39" time="40.673" type="appl" />
         <tli id="T40" time="42.488" type="appl" />
         <tli id="T41" time="43.398" type="appl" />
         <tli id="T42" time="44.308" type="appl" />
         <tli id="T43" time="45.453" type="appl" />
         <tli id="T44" time="46.303" type="appl" />
         <tli id="T45" time="47.153" type="appl" />
         <tli id="T46" time="48.670629399229966" />
         <tli id="T47" time="49.31" type="appl" />
         <tli id="T48" time="50.148" type="appl" />
         <tli id="T49" time="50.986" type="appl" />
         <tli id="T50" time="52.27042940958255" />
         <tli id="T51" time="53.006" type="appl" />
         <tli id="T52" time="53.703" type="appl" />
         <tli id="T53" time="54.4" type="appl" />
         <tli id="T54" time="55.92355979045887" />
         <tli id="T55" time="56.633" type="appl" />
         <tli id="T56" time="57.393" type="appl" />
         <tli id="T57" time="58.153" type="appl" />
         <tli id="T58" time="58.913" type="appl" />
         <tli id="T59" time="59.673" type="appl" />
         <tli id="T60" time="60.616" type="appl" />
         <tli id="T61" time="61.288" type="appl" />
         <tli id="T62" time="61.96" type="appl" />
         <tli id="T63" time="63.91644907270469" />
         <tli id="T64" time="64.502" type="appl" />
         <tli id="T65" time="65.196" type="appl" />
         <tli id="T66" time="65.89" type="appl" />
         <tli id="T67" time="66.584" type="appl" />
         <tli id="T68" time="67.52958167568819" />
         <tli id="T69" time="68.348" type="appl" />
         <tli id="T70" time="69.213" type="appl" />
         <tli id="T71" time="70.078" type="appl" />
         <tli id="T72" time="70.943" type="appl" />
         <tli id="T73" time="71.808" type="appl" />
         <tli id="T74" time="73.07594021015737" />
         <tli id="T75" time="73.968" type="appl" />
         <tli id="T76" time="74.938" type="appl" />
         <tli id="T77" time="76.16910170053438" />
         <tli id="T78" time="77.243" type="appl" />
         <tli id="T79" time="79.31559356143516" />
         <tli id="T80" time="79.722" type="appl" />
         <tli id="T81" time="80.31" type="appl" />
         <tli id="T82" time="80.899" type="appl" />
         <tli id="T83" time="81.487" type="appl" />
         <tli id="T84" time="82.076" type="appl" />
         <tli id="T85" time="82.664" type="appl" />
         <tli id="T86" time="83.253" type="appl" />
         <tli id="T87" time="84.515" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KFN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T86" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Qandəqət</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">qut</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">maǯʼondə</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">qwenbat</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Nalqut</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">maːtə</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">qalembat</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">Qandəqət</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">aː</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">wargə</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">xəroːɣət</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">qorqət</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">tömba</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_50" n="HIAT:u" s="T13">
                  <ts e="T16" id="Seg_52" n="HIAT:w" s="T13">Nalgut</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_54" n="HIAT:ip">(</nts>
                  <ts e="T14" id="Seg_56" n="HIAT:w" s="T16">š-</ts>
                  <nts id="Seg_57" n="HIAT:ip">)</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_60" n="HIAT:w" s="T14">šoqort</ts>
                  <nts id="Seg_61" n="HIAT:ip">,</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T15">ponəl</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">šoqort</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">nʼajem</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">omdəlǯəmbat</ts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_77" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">Qorqə</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">tak</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">panalbat</ts>
                  <nts id="Seg_86" n="HIAT:ip">.</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_89" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_91" n="HIAT:w" s="T23">Na</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">nʼalal</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_97" n="HIAT:w" s="T25">nʼaj</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_100" n="HIAT:w" s="T26">ves</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">ambat</ts>
                  <nts id="Seg_104" n="HIAT:ip">.</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_107" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_109" n="HIAT:w" s="T28">Potom</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_112" n="HIAT:w" s="T29">qošünenə</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">šerba</ts>
                  <nts id="Seg_116" n="HIAT:ip">.</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_119" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_121" n="HIAT:w" s="T31">Mugal</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_124" n="HIAT:w" s="T32">koǯap</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">taŋ</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">nʼiškɨlbat</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_134" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_136" n="HIAT:w" s="T35">Onǯə</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_139" n="HIAT:w" s="T36">pirətə</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_142" n="HIAT:w" s="T37">vesʼ</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_145" n="HIAT:w" s="T38">čʼaːqeǯʼəmba</ts>
                  <nts id="Seg_146" n="HIAT:ip">.</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_149" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_151" n="HIAT:w" s="T39">Patom</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_154" n="HIAT:w" s="T40">maːtə</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_157" n="HIAT:w" s="T41">kɨgelɨmban</ts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_161" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_163" n="HIAT:w" s="T42">Nalʼgut</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_166" n="HIAT:w" s="T43">katora</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_169" n="HIAT:w" s="T44">podpolǯə</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_172" n="HIAT:w" s="T45">ateːlʼǯembat</ts>
                  <nts id="Seg_173" n="HIAT:ip">.</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_176" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_178" n="HIAT:w" s="T46">Okkur</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_181" n="HIAT:w" s="T47">nadek</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_184" n="HIAT:w" s="T48">olɨm</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_187" n="HIAT:w" s="T49">meːšpat</ts>
                  <nts id="Seg_188" n="HIAT:ip">.</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_191" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_193" n="HIAT:w" s="T50">Olɨm</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_196" n="HIAT:w" s="T51">meːmbat</ts>
                  <nts id="Seg_197" n="HIAT:ip">,</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_200" n="HIAT:w" s="T52">tülʼdem</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_203" n="HIAT:w" s="T53">abɛdɛmbat</ts>
                  <nts id="Seg_204" n="HIAT:ip">.</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_207" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_209" n="HIAT:w" s="T54">Qorqə</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_212" n="HIAT:w" s="T55">akoškan</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_215" n="HIAT:w" s="T56">nülʼeǯemba</ts>
                  <nts id="Seg_216" n="HIAT:ip">,</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_219" n="HIAT:w" s="T57">maːttə</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_222" n="HIAT:w" s="T58">manämbugu</ts>
                  <nts id="Seg_223" n="HIAT:ip">.</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_226" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_228" n="HIAT:w" s="T59">Tabə</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_231" n="HIAT:w" s="T60">kɨlnol</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_234" n="HIAT:w" s="T61">ǯʼaɣ</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_237" n="HIAT:w" s="T62">čaǯembat</ts>
                  <nts id="Seg_238" n="HIAT:ip">.</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_241" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_243" n="HIAT:w" s="T63">Patom</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_246" n="HIAT:w" s="T64">qajčugu</ts>
                  <nts id="Seg_247" n="HIAT:ip">,</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_250" n="HIAT:w" s="T65">taqqergu</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_253" n="HIAT:w" s="T66">aː</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_256" n="HIAT:w" s="T67">tänuwat</ts>
                  <nts id="Seg_257" n="HIAT:ip">.</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_260" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_262" n="HIAT:w" s="T68">Okkɨr</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_265" n="HIAT:w" s="T69">qup</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_268" n="HIAT:w" s="T70">üdɨmbat</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_271" n="HIAT:w" s="T71">šɨdaro</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_274" n="HIAT:w" s="T72">kulumerterte</ts>
                  <nts id="Seg_275" n="HIAT:ip">,</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_278" n="HIAT:w" s="T73">verstanderi</ts>
                  <nts id="Seg_279" n="HIAT:ip">.</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_282" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_284" n="HIAT:w" s="T74">Načagəndə</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_287" n="HIAT:w" s="T75">tebequp</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_290" n="HIAT:w" s="T76">tömbat</ts>
                  <nts id="Seg_291" n="HIAT:ip">.</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_294" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_296" n="HIAT:w" s="T77">Qorqəp</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_299" n="HIAT:w" s="T78">taqqɨrɨmbat</ts>
                  <nts id="Seg_300" n="HIAT:ip">.</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_303" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_305" n="HIAT:w" s="T79">Taqqɨrlʼe</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_308" n="HIAT:w" s="T80">puːlʼe</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_311" n="HIAT:w" s="T81">na</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_314" n="HIAT:w" s="T82">qorqən</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_317" n="HIAT:w" s="T83">waǯʼep</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_320" n="HIAT:w" s="T84">vesʼ</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_323" n="HIAT:w" s="T85">ambat</ts>
                  <nts id="Seg_324" n="HIAT:ip">.</nts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T86" id="Seg_326" n="sc" s="T0">
               <ts e="T1" id="Seg_328" n="e" s="T0">Qandəqət </ts>
               <ts e="T2" id="Seg_330" n="e" s="T1">qut </ts>
               <ts e="T3" id="Seg_332" n="e" s="T2">maǯʼondə </ts>
               <ts e="T4" id="Seg_334" n="e" s="T3">qwenbat. </ts>
               <ts e="T5" id="Seg_336" n="e" s="T4">Nalqut </ts>
               <ts e="T6" id="Seg_338" n="e" s="T5">maːtə </ts>
               <ts e="T7" id="Seg_340" n="e" s="T6">qalembat. </ts>
               <ts e="T8" id="Seg_342" n="e" s="T7">Qandəqət </ts>
               <ts e="T9" id="Seg_344" n="e" s="T8">aː </ts>
               <ts e="T10" id="Seg_346" n="e" s="T9">wargə </ts>
               <ts e="T11" id="Seg_348" n="e" s="T10">xəroːɣət </ts>
               <ts e="T12" id="Seg_350" n="e" s="T11">qorqət </ts>
               <ts e="T13" id="Seg_352" n="e" s="T12">tömba. </ts>
               <ts e="T16" id="Seg_354" n="e" s="T13">Nalgut </ts>
               <ts e="T14" id="Seg_356" n="e" s="T16">(š-) </ts>
               <ts e="T15" id="Seg_358" n="e" s="T14">šoqort, </ts>
               <ts e="T17" id="Seg_360" n="e" s="T15">ponəl </ts>
               <ts e="T18" id="Seg_362" n="e" s="T17">šoqort </ts>
               <ts e="T19" id="Seg_364" n="e" s="T18">nʼajem </ts>
               <ts e="T20" id="Seg_366" n="e" s="T19">omdəlǯəmbat. </ts>
               <ts e="T21" id="Seg_368" n="e" s="T20">Qorqə </ts>
               <ts e="T22" id="Seg_370" n="e" s="T21">tak </ts>
               <ts e="T23" id="Seg_372" n="e" s="T22">panalbat. </ts>
               <ts e="T24" id="Seg_374" n="e" s="T23">Na </ts>
               <ts e="T25" id="Seg_376" n="e" s="T24">nʼalal </ts>
               <ts e="T26" id="Seg_378" n="e" s="T25">nʼaj </ts>
               <ts e="T27" id="Seg_380" n="e" s="T26">ves </ts>
               <ts e="T28" id="Seg_382" n="e" s="T27">ambat. </ts>
               <ts e="T29" id="Seg_384" n="e" s="T28">Potom </ts>
               <ts e="T30" id="Seg_386" n="e" s="T29">qošünenə </ts>
               <ts e="T31" id="Seg_388" n="e" s="T30">šerba. </ts>
               <ts e="T32" id="Seg_390" n="e" s="T31">Mugal </ts>
               <ts e="T33" id="Seg_392" n="e" s="T32">koǯap </ts>
               <ts e="T34" id="Seg_394" n="e" s="T33">taŋ </ts>
               <ts e="T35" id="Seg_396" n="e" s="T34">nʼiškɨlbat. </ts>
               <ts e="T36" id="Seg_398" n="e" s="T35">Onǯə </ts>
               <ts e="T37" id="Seg_400" n="e" s="T36">pirətə </ts>
               <ts e="T38" id="Seg_402" n="e" s="T37">vesʼ </ts>
               <ts e="T39" id="Seg_404" n="e" s="T38">čʼaːqeǯʼəmba. </ts>
               <ts e="T40" id="Seg_406" n="e" s="T39">Patom </ts>
               <ts e="T41" id="Seg_408" n="e" s="T40">maːtə </ts>
               <ts e="T42" id="Seg_410" n="e" s="T41">kɨgelɨmban. </ts>
               <ts e="T43" id="Seg_412" n="e" s="T42">Nalʼgut </ts>
               <ts e="T44" id="Seg_414" n="e" s="T43">katora </ts>
               <ts e="T45" id="Seg_416" n="e" s="T44">podpolǯə </ts>
               <ts e="T46" id="Seg_418" n="e" s="T45">ateːlʼǯembat. </ts>
               <ts e="T47" id="Seg_420" n="e" s="T46">Okkur </ts>
               <ts e="T48" id="Seg_422" n="e" s="T47">nadek </ts>
               <ts e="T49" id="Seg_424" n="e" s="T48">olɨm </ts>
               <ts e="T50" id="Seg_426" n="e" s="T49">meːšpat. </ts>
               <ts e="T51" id="Seg_428" n="e" s="T50">Olɨm </ts>
               <ts e="T52" id="Seg_430" n="e" s="T51">meːmbat, </ts>
               <ts e="T53" id="Seg_432" n="e" s="T52">tülʼdem </ts>
               <ts e="T54" id="Seg_434" n="e" s="T53">abɛdɛmbat. </ts>
               <ts e="T55" id="Seg_436" n="e" s="T54">Qorqə </ts>
               <ts e="T56" id="Seg_438" n="e" s="T55">akoškan </ts>
               <ts e="T57" id="Seg_440" n="e" s="T56">nülʼeǯemba, </ts>
               <ts e="T58" id="Seg_442" n="e" s="T57">maːttə </ts>
               <ts e="T59" id="Seg_444" n="e" s="T58">manämbugu. </ts>
               <ts e="T60" id="Seg_446" n="e" s="T59">Tabə </ts>
               <ts e="T61" id="Seg_448" n="e" s="T60">kɨlnol </ts>
               <ts e="T62" id="Seg_450" n="e" s="T61">ǯʼaɣ </ts>
               <ts e="T63" id="Seg_452" n="e" s="T62">čaǯembat. </ts>
               <ts e="T64" id="Seg_454" n="e" s="T63">Patom </ts>
               <ts e="T65" id="Seg_456" n="e" s="T64">qajčugu, </ts>
               <ts e="T66" id="Seg_458" n="e" s="T65">taqqergu </ts>
               <ts e="T67" id="Seg_460" n="e" s="T66">aː </ts>
               <ts e="T68" id="Seg_462" n="e" s="T67">tänuwat. </ts>
               <ts e="T69" id="Seg_464" n="e" s="T68">Okkɨr </ts>
               <ts e="T70" id="Seg_466" n="e" s="T69">qup </ts>
               <ts e="T71" id="Seg_468" n="e" s="T70">üdɨmbat </ts>
               <ts e="T72" id="Seg_470" n="e" s="T71">šɨdaro </ts>
               <ts e="T73" id="Seg_472" n="e" s="T72">kulumerterte, </ts>
               <ts e="T74" id="Seg_474" n="e" s="T73">verstanderi. </ts>
               <ts e="T75" id="Seg_476" n="e" s="T74">Načagəndə </ts>
               <ts e="T76" id="Seg_478" n="e" s="T75">tebequp </ts>
               <ts e="T77" id="Seg_480" n="e" s="T76">tömbat. </ts>
               <ts e="T78" id="Seg_482" n="e" s="T77">Qorqəp </ts>
               <ts e="T79" id="Seg_484" n="e" s="T78">taqqɨrɨmbat. </ts>
               <ts e="T80" id="Seg_486" n="e" s="T79">Taqqɨrlʼe </ts>
               <ts e="T81" id="Seg_488" n="e" s="T80">puːlʼe </ts>
               <ts e="T82" id="Seg_490" n="e" s="T81">na </ts>
               <ts e="T83" id="Seg_492" n="e" s="T82">qorqən </ts>
               <ts e="T84" id="Seg_494" n="e" s="T83">waǯʼep </ts>
               <ts e="T85" id="Seg_496" n="e" s="T84">vesʼ </ts>
               <ts e="T86" id="Seg_498" n="e" s="T85">ambat. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_499" s="T0">KFN_196X_GirlAndBear2_nar.001 (001 )</ta>
            <ta e="T7" id="Seg_500" s="T4">KFN_196X_GirlAndBear2_nar.002 (002)</ta>
            <ta e="T13" id="Seg_501" s="T7">KFN_196X_GirlAndBear2_nar.003 (003)</ta>
            <ta e="T20" id="Seg_502" s="T13">KFN_196X_GirlAndBear2_nar.004 (004)</ta>
            <ta e="T23" id="Seg_503" s="T20">KFN_196X_GirlAndBear2_nar.005 (005)</ta>
            <ta e="T28" id="Seg_504" s="T23">KFN_196X_GirlAndBear2_nar.006 (006)</ta>
            <ta e="T31" id="Seg_505" s="T28">KFN_196X_GirlAndBear2_nar.007 (007)</ta>
            <ta e="T35" id="Seg_506" s="T31">KFN_196X_GirlAndBear2_nar.008 (008)</ta>
            <ta e="T39" id="Seg_507" s="T35">KFN_196X_GirlAndBear2_nar.009 (009)</ta>
            <ta e="T42" id="Seg_508" s="T39">KFN_196X_GirlAndBear2_nar.010 (010)</ta>
            <ta e="T46" id="Seg_509" s="T42">KFN_196X_GirlAndBear2_nar.011 (011)</ta>
            <ta e="T50" id="Seg_510" s="T46">KFN_196X_GirlAndBear2_nar.012 (012)</ta>
            <ta e="T54" id="Seg_511" s="T50">KFN_196X_GirlAndBear2_nar.013 (013)</ta>
            <ta e="T59" id="Seg_512" s="T54">KFN_196X_GirlAndBear2_nar.014 (014)</ta>
            <ta e="T63" id="Seg_513" s="T59">KFN_196X_GirlAndBear2_nar.015 (015)</ta>
            <ta e="T68" id="Seg_514" s="T63">KFN_196X_GirlAndBear2_nar.016 (016)</ta>
            <ta e="T74" id="Seg_515" s="T68">KFN_196X_GirlAndBear2_nar.017 (017)</ta>
            <ta e="T77" id="Seg_516" s="T74">KFN_196X_GirlAndBear2_nar.018 (018)</ta>
            <ta e="T79" id="Seg_517" s="T77">KFN_196X_GirlAndBear2_nar.019 (019)</ta>
            <ta e="T86" id="Seg_518" s="T79">KFN_196X_GirlAndBear2_nar.020 (020)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_519" s="T0">Кандэӷэт кут маджӧнд қвэнбат.</ta>
            <ta e="T7" id="Seg_520" s="T4">Нӓлгут ма̄тэ қалэмбат.</ta>
            <ta e="T13" id="Seg_521" s="T7">Кандэӷэт а̄варг хэроӷэт қорӷ тӧмба.</ta>
            <ta e="T20" id="Seg_522" s="T13">Нӓлгут шогорт … понэл шогорт нӓйем омдэлджэмбат.</ta>
            <ta e="T23" id="Seg_523" s="T20">Корг так паналбат.</ta>
            <ta e="T28" id="Seg_524" s="T23">На нӓйм вес амбат.</ta>
            <ta e="T31" id="Seg_525" s="T28">Потом кошонендж шэрба.</ta>
            <ta e="T35" id="Seg_526" s="T31">Мугал коджап так нишкылбат.</ta>
            <ta e="T39" id="Seg_527" s="T35">Ондж пирэт вес чагоджемба.</ta>
            <ta e="T42" id="Seg_528" s="T39">Потом ма̄т кыгелымбан.</ta>
            <ta e="T46" id="Seg_529" s="T42">Нӓлгут катора подполджэ атэльджембат.</ta>
            <ta e="T50" id="Seg_530" s="T46">Оккор надэк олым мешпат.</ta>
            <ta e="T54" id="Seg_531" s="T50">Олом мембат, тӱльдэм абэдэмбат.</ta>
            <ta e="T59" id="Seg_532" s="T54">Корг акошканд ныледжемба, ма̄т манэмбэгу.</ta>
            <ta e="T63" id="Seg_533" s="T59">Таб кыль нольджӓӷ чаджембат.</ta>
            <ta e="T68" id="Seg_534" s="T63">Потом кальчегу таққэргу а̄ тэнват.</ta>
            <ta e="T74" id="Seg_535" s="T68">Оққэр қуп ӱдомбат шэдаро конэртэртэ верстандэли.</ta>
            <ta e="T77" id="Seg_536" s="T74">Начагындо тэбэлгуп тӧмбат.</ta>
            <ta e="T79" id="Seg_537" s="T77">Коргоп таққырымбат.</ta>
            <ta e="T86" id="Seg_538" s="T79">Таққырле пуле на коргон ваджеп вес амбат.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_539" s="T0">Kandeɣet kut maǯönd qvenbat.</ta>
            <ta e="T7" id="Seg_540" s="T4">Nälgut maːte qalembat.</ta>
            <ta e="T13" id="Seg_541" s="T7">Kandeɣet aːvarg xeroɣet qorɣ tömba.</ta>
            <ta e="T20" id="Seg_542" s="T13">Nälgut šogort … ponel šogort näjem omdelǯembat.</ta>
            <ta e="T23" id="Seg_543" s="T20">Korg tak panalbat.</ta>
            <ta e="T28" id="Seg_544" s="T23">Na näjm ves ambat.</ta>
            <ta e="T31" id="Seg_545" s="T28">Potom košonenǯ šerba.</ta>
            <ta e="T35" id="Seg_546" s="T31">Mugal koǯap tak niškɨlbat.</ta>
            <ta e="T39" id="Seg_547" s="T35">Onǯ pirət ves čagoǯemba.</ta>
            <ta e="T42" id="Seg_548" s="T39">Potom maːt kɨgelɨmban.</ta>
            <ta e="T46" id="Seg_549" s="T42">Nälgut katora podpolǯe atelʼǯembat.</ta>
            <ta e="T50" id="Seg_550" s="T46">Okkor nadek olɨm mešpat.</ta>
            <ta e="T54" id="Seg_551" s="T50">Olom membat, tülʼdem abedembat.</ta>
            <ta e="T59" id="Seg_552" s="T54">Korg akoškand nɨleǯemba, maːt manembegu.</ta>
            <ta e="T63" id="Seg_553" s="T59">Tab kɨlʼ nolʼǯäɣ čʼaǯembat.</ta>
            <ta e="T68" id="Seg_554" s="T63">Potom kalʼčʼegu taqqergu aː tenvat.</ta>
            <ta e="T74" id="Seg_555" s="T68">Oqqer qup üdombat šedaro konerterte verstandeli.</ta>
            <ta e="T77" id="Seg_556" s="T74">Načʼagɨndo tebelgup tömbat.</ta>
            <ta e="T79" id="Seg_557" s="T77">Korgop taqqɨrɨmbat.</ta>
            <ta e="T86" id="Seg_558" s="T79">Taqqɨrle pule na korgon vaǯep ves ambat.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_559" s="T0">Qandəqət qut maǯʼondə qwenbat. </ta>
            <ta e="T7" id="Seg_560" s="T4">Nalqut maːtə qalembat. </ta>
            <ta e="T13" id="Seg_561" s="T7">Qandəqət aː wargə xəroːɣət qorqət tömba. </ta>
            <ta e="T20" id="Seg_562" s="T13">Nalgut (š-) šoqort, ponəl šoqort nʼajem omdəlǯəmbat. </ta>
            <ta e="T23" id="Seg_563" s="T20">Qorqə tak panalbat. </ta>
            <ta e="T28" id="Seg_564" s="T23">Na nʼalal nʼaj ves ambat. </ta>
            <ta e="T31" id="Seg_565" s="T28">Potom qošünenə šerba. </ta>
            <ta e="T35" id="Seg_566" s="T31">Mugal koǯap taŋ nʼiškɨlbat. </ta>
            <ta e="T39" id="Seg_567" s="T35">Onǯə pirətə vesʼ čʼaːqeǯʼəmba.</ta>
            <ta e="T42" id="Seg_568" s="T39">Patom maːtə kɨgelɨmban. </ta>
            <ta e="T46" id="Seg_569" s="T42">Nalʼgut katora podpolǯə ateːlʼǯembat. </ta>
            <ta e="T50" id="Seg_570" s="T46">Okkur nadek olɨm meːšpat. </ta>
            <ta e="T54" id="Seg_571" s="T50">Olɨm meːmbat, tülʼdem abɛdɛmbat. </ta>
            <ta e="T59" id="Seg_572" s="T54">Qorqə akoškan nülʼeǯemba, maːttə manämbugu. </ta>
            <ta e="T63" id="Seg_573" s="T59">Tabə kɨlnol ǯʼaɣ čaǯembat. </ta>
            <ta e="T68" id="Seg_574" s="T63">Patom qajčugu, taqqergu aː tänuwat. </ta>
            <ta e="T74" id="Seg_575" s="T68">Okkɨr qup üdɨmbat šɨdaro kulumerterte, verstanderi. </ta>
            <ta e="T77" id="Seg_576" s="T74">Načagəndə tebequp tömbat. </ta>
            <ta e="T79" id="Seg_577" s="T77">Qorqəp taqqɨrɨmbat. </ta>
            <ta e="T86" id="Seg_578" s="T79">Taqqɨrlʼe puːlʼe na qorqən waǯʼep vesʼ ambat. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_579" s="T0">qandə-qət</ta>
            <ta e="T2" id="Seg_580" s="T1">qu-t</ta>
            <ta e="T3" id="Seg_581" s="T2">maǯʼo-ndə</ta>
            <ta e="T4" id="Seg_582" s="T3">qwen-ba-t</ta>
            <ta e="T5" id="Seg_583" s="T4">na-l-qu-t</ta>
            <ta e="T6" id="Seg_584" s="T5">maː-tə</ta>
            <ta e="T7" id="Seg_585" s="T6">qale-mba-t</ta>
            <ta e="T8" id="Seg_586" s="T7">qandə-qət</ta>
            <ta e="T9" id="Seg_587" s="T8">aː</ta>
            <ta e="T10" id="Seg_588" s="T9">wargə</ta>
            <ta e="T11" id="Seg_589" s="T10">xər-oː-ɣət</ta>
            <ta e="T12" id="Seg_590" s="T11">qorqə-t</ta>
            <ta e="T13" id="Seg_591" s="T12">tö-mba</ta>
            <ta e="T16" id="Seg_592" s="T13">na-l-gu-t</ta>
            <ta e="T15" id="Seg_593" s="T14">šoqor-t</ta>
            <ta e="T17" id="Seg_594" s="T15">ponə-l</ta>
            <ta e="T18" id="Seg_595" s="T17">šoqor-t</ta>
            <ta e="T19" id="Seg_596" s="T18">nʼaj-e-m</ta>
            <ta e="T20" id="Seg_597" s="T19">omdə-lǯə-mba-t</ta>
            <ta e="T21" id="Seg_598" s="T20">qorqə</ta>
            <ta e="T22" id="Seg_599" s="T21">tak</ta>
            <ta e="T23" id="Seg_600" s="T22">panal-ba-t</ta>
            <ta e="T24" id="Seg_601" s="T23">na</ta>
            <ta e="T25" id="Seg_602" s="T24">nʼalal</ta>
            <ta e="T26" id="Seg_603" s="T25">nʼaj</ta>
            <ta e="T27" id="Seg_604" s="T26">ves</ta>
            <ta e="T28" id="Seg_605" s="T27">am-ba-t</ta>
            <ta e="T29" id="Seg_606" s="T28">potom</ta>
            <ta e="T30" id="Seg_607" s="T29">qošünenə</ta>
            <ta e="T31" id="Seg_608" s="T30">šer-ba</ta>
            <ta e="T32" id="Seg_609" s="T31">muga-l</ta>
            <ta e="T33" id="Seg_610" s="T32">koǯa-p</ta>
            <ta e="T34" id="Seg_611" s="T33">taŋ</ta>
            <ta e="T35" id="Seg_612" s="T34">nʼiškɨ-l-ba-t</ta>
            <ta e="T36" id="Seg_613" s="T35">onǯə</ta>
            <ta e="T37" id="Seg_614" s="T36">pirə-tə</ta>
            <ta e="T38" id="Seg_615" s="T37">vesʼ</ta>
            <ta e="T39" id="Seg_616" s="T38">čʼaːq-eǯʼə-mba</ta>
            <ta e="T40" id="Seg_617" s="T39">patom</ta>
            <ta e="T41" id="Seg_618" s="T40">maːtə</ta>
            <ta e="T42" id="Seg_619" s="T41">kɨge-lɨ-mba-n</ta>
            <ta e="T43" id="Seg_620" s="T42">na-lʼ-gu-t</ta>
            <ta e="T44" id="Seg_621" s="T43">katora</ta>
            <ta e="T45" id="Seg_622" s="T44">podpol-ǯə</ta>
            <ta e="T46" id="Seg_623" s="T45">ateː-lʼǯe-mba-t</ta>
            <ta e="T47" id="Seg_624" s="T46">okkur</ta>
            <ta e="T48" id="Seg_625" s="T47">nadek</ta>
            <ta e="T49" id="Seg_626" s="T48">olɨ-m</ta>
            <ta e="T50" id="Seg_627" s="T49">meː-špa-t</ta>
            <ta e="T51" id="Seg_628" s="T50">olɨ-m</ta>
            <ta e="T52" id="Seg_629" s="T51">meː-mba-t</ta>
            <ta e="T53" id="Seg_630" s="T52">tülʼde-m</ta>
            <ta e="T54" id="Seg_631" s="T53">ab-ɛ-dɛ-mba-t</ta>
            <ta e="T55" id="Seg_632" s="T54">qorqə</ta>
            <ta e="T56" id="Seg_633" s="T55">akoška-n</ta>
            <ta e="T57" id="Seg_634" s="T56">nülʼeǯe-mba</ta>
            <ta e="T58" id="Seg_635" s="T57">maːt-tə</ta>
            <ta e="T59" id="Seg_636" s="T58">manä-mbu-gu</ta>
            <ta e="T60" id="Seg_637" s="T59">tabə</ta>
            <ta e="T61" id="Seg_638" s="T60">kɨlnol</ta>
            <ta e="T62" id="Seg_639" s="T61">ǯʼaɣ</ta>
            <ta e="T63" id="Seg_640" s="T62">čaǯe-mba-t</ta>
            <ta e="T64" id="Seg_641" s="T63">patom</ta>
            <ta e="T65" id="Seg_642" s="T64">qajču-gu</ta>
            <ta e="T66" id="Seg_643" s="T65">taqqer-gu</ta>
            <ta e="T67" id="Seg_644" s="T66">aː</ta>
            <ta e="T68" id="Seg_645" s="T67">tänu-wa-t</ta>
            <ta e="T69" id="Seg_646" s="T68">okkɨr</ta>
            <ta e="T70" id="Seg_647" s="T69">qup</ta>
            <ta e="T71" id="Seg_648" s="T70">üdɨ-mba-t</ta>
            <ta e="T72" id="Seg_649" s="T71">šɨdaro</ta>
            <ta e="T73" id="Seg_650" s="T72">kulumerter-te</ta>
            <ta e="T74" id="Seg_651" s="T73">versta-nde-ri</ta>
            <ta e="T75" id="Seg_652" s="T74">načagəndə</ta>
            <ta e="T76" id="Seg_653" s="T75">tebe-qup</ta>
            <ta e="T77" id="Seg_654" s="T76">tö-mba-t</ta>
            <ta e="T78" id="Seg_655" s="T77">qorqə-p</ta>
            <ta e="T79" id="Seg_656" s="T78">taqqɨr-ɨ-mba-t</ta>
            <ta e="T80" id="Seg_657" s="T79">taqqɨr-lʼe</ta>
            <ta e="T81" id="Seg_658" s="T80">puːlʼe</ta>
            <ta e="T82" id="Seg_659" s="T81">na</ta>
            <ta e="T83" id="Seg_660" s="T82">qorqə-n</ta>
            <ta e="T84" id="Seg_661" s="T83">waǯʼe-p</ta>
            <ta e="T85" id="Seg_662" s="T84">vesʼ</ta>
            <ta e="T86" id="Seg_663" s="T85">am-ba-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_664" s="T0">qande-qɨt</ta>
            <ta e="T2" id="Seg_665" s="T1">qum-t</ta>
            <ta e="T3" id="Seg_666" s="T2">maǯʼo-nde</ta>
            <ta e="T4" id="Seg_667" s="T3">qwän-mbɨ-dət</ta>
            <ta e="T5" id="Seg_668" s="T4">neː-lʼ-qum-t</ta>
            <ta e="T6" id="Seg_669" s="T5">maːt-nde</ta>
            <ta e="T7" id="Seg_670" s="T6">qalɨ-mbɨ-dət</ta>
            <ta e="T8" id="Seg_671" s="T7">qande-qɨn</ta>
            <ta e="T9" id="Seg_672" s="T8">aː</ta>
            <ta e="T10" id="Seg_673" s="T9">wargɨ</ta>
            <ta e="T11" id="Seg_674" s="T10">xər-ɨ-qɨt</ta>
            <ta e="T12" id="Seg_675" s="T11">qorqɨ-t</ta>
            <ta e="T13" id="Seg_676" s="T12">töː-mbɨ</ta>
            <ta e="T16" id="Seg_677" s="T13">neː-lʼ-qum-t</ta>
            <ta e="T15" id="Seg_678" s="T14">šoqor-nde</ta>
            <ta e="T17" id="Seg_679" s="T15">ponɛ-lʼ</ta>
            <ta e="T18" id="Seg_680" s="T17">šoqor-nde</ta>
            <ta e="T19" id="Seg_681" s="T18">nʼaj-ɨ-m</ta>
            <ta e="T20" id="Seg_682" s="T19">omde-lʼčǝ-mbɨ-dət</ta>
            <ta e="T21" id="Seg_683" s="T20">qorqɨ</ta>
            <ta e="T22" id="Seg_684" s="T21">tak</ta>
            <ta e="T23" id="Seg_685" s="T22">panal-mbɨ-tɨ</ta>
            <ta e="T24" id="Seg_686" s="T23">na</ta>
            <ta e="T25" id="Seg_687" s="T24">nʼalʼalʼ</ta>
            <ta e="T26" id="Seg_688" s="T25">nʼaj</ta>
            <ta e="T27" id="Seg_689" s="T26">wesʼ</ta>
            <ta e="T28" id="Seg_690" s="T27">am-mbɨ-tɨ</ta>
            <ta e="T29" id="Seg_691" s="T28">patom</ta>
            <ta e="T30" id="Seg_692" s="T29">qošüntʼ</ta>
            <ta e="T31" id="Seg_693" s="T30">šeːr-mbɨ</ta>
            <ta e="T32" id="Seg_694" s="T31">muga-lʼ</ta>
            <ta e="T33" id="Seg_695" s="T32">qoča-p</ta>
            <ta e="T34" id="Seg_696" s="T33">tak</ta>
            <ta e="T35" id="Seg_697" s="T34">nɨškɨ-lɨ-mbɨ-tɨ</ta>
            <ta e="T36" id="Seg_698" s="T35">onǯe</ta>
            <ta e="T37" id="Seg_699" s="T36">pirɨ-tɨ</ta>
            <ta e="T38" id="Seg_700" s="T37">wesʼ</ta>
            <ta e="T39" id="Seg_701" s="T38">čeq-eǯe-mbɨ</ta>
            <ta e="T40" id="Seg_702" s="T39">patom</ta>
            <ta e="T41" id="Seg_703" s="T40">maːt</ta>
            <ta e="T42" id="Seg_704" s="T41">kɨge-lɨ-mbɨ-n</ta>
            <ta e="T43" id="Seg_705" s="T42">neː-lʼ-qum-t</ta>
            <ta e="T44" id="Seg_706" s="T43">katora</ta>
            <ta e="T45" id="Seg_707" s="T44">potpol-nǯ</ta>
            <ta e="T46" id="Seg_708" s="T45">attɛ-lʼčǝ-mbɨ-dət</ta>
            <ta e="T47" id="Seg_709" s="T46">okkər</ta>
            <ta e="T48" id="Seg_710" s="T47">nadek</ta>
            <ta e="T49" id="Seg_711" s="T48">olo-m</ta>
            <ta e="T50" id="Seg_712" s="T49">me-špɨ-tɨ</ta>
            <ta e="T51" id="Seg_713" s="T50">olo-m</ta>
            <ta e="T52" id="Seg_714" s="T51">me-mbɨ-tɨ</ta>
            <ta e="T53" id="Seg_715" s="T52">tüːlʼde-m</ta>
            <ta e="T54" id="Seg_716" s="T53">am-ɨ-dɨ-mbɨ-tɨ</ta>
            <ta e="T55" id="Seg_717" s="T54">qorqɨ</ta>
            <ta e="T56" id="Seg_718" s="T55">akoška-ni</ta>
            <ta e="T57" id="Seg_719" s="T56">nʼulʼeǯʼe-mbɨ</ta>
            <ta e="T58" id="Seg_720" s="T57">maːt-nde</ta>
            <ta e="T59" id="Seg_721" s="T58">mantɨ-mbɨ-gu</ta>
            <ta e="T60" id="Seg_722" s="T59">tab</ta>
            <ta e="T61" id="Seg_723" s="T60">kɨlnol</ta>
            <ta e="T62" id="Seg_724" s="T61">ǯʼaɣ</ta>
            <ta e="T63" id="Seg_725" s="T62">čačɨ-mbɨ-tɨ</ta>
            <ta e="T64" id="Seg_726" s="T63">patom</ta>
            <ta e="T65" id="Seg_727" s="T64">qajču-gu</ta>
            <ta e="T66" id="Seg_728" s="T65">taqqer-gu</ta>
            <ta e="T67" id="Seg_729" s="T66">aː</ta>
            <ta e="T68" id="Seg_730" s="T67">tanu-wa-dət</ta>
            <ta e="T69" id="Seg_731" s="T68">okkər</ta>
            <ta e="T70" id="Seg_732" s="T69">qum</ta>
            <ta e="T71" id="Seg_733" s="T70">üdɨ-mbɨ-tɨ</ta>
            <ta e="T72" id="Seg_734" s="T71">šidaːro</ta>
            <ta e="T73" id="Seg_735" s="T72">kilometr-nde</ta>
            <ta e="T74" id="Seg_736" s="T73">wersta-nde-%%</ta>
            <ta e="T75" id="Seg_737" s="T74">načautə</ta>
            <ta e="T76" id="Seg_738" s="T75">tebe-qum</ta>
            <ta e="T77" id="Seg_739" s="T76">töː-mbɨ-dət</ta>
            <ta e="T78" id="Seg_740" s="T77">qorqɨ-m</ta>
            <ta e="T79" id="Seg_741" s="T78">taqqer-ɨ-mbɨ-dət</ta>
            <ta e="T80" id="Seg_742" s="T79">taqqer-le</ta>
            <ta e="T81" id="Seg_743" s="T80">puːle</ta>
            <ta e="T82" id="Seg_744" s="T81">na</ta>
            <ta e="T83" id="Seg_745" s="T82">qorqɨ-n</ta>
            <ta e="T84" id="Seg_746" s="T83">waǯʼe-m</ta>
            <ta e="T85" id="Seg_747" s="T84">wesʼ</ta>
            <ta e="T86" id="Seg_748" s="T85">am-mbɨ-dət</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_749" s="T0">autumn-LOC</ta>
            <ta e="T2" id="Seg_750" s="T1">human.being-PL</ta>
            <ta e="T3" id="Seg_751" s="T2">taiga-ILL</ta>
            <ta e="T4" id="Seg_752" s="T3">go.away-PST.NAR-3PL</ta>
            <ta e="T5" id="Seg_753" s="T4">daughter-ADJZ-human.being-PL</ta>
            <ta e="T6" id="Seg_754" s="T5">house-ILL</ta>
            <ta e="T7" id="Seg_755" s="T6">stay-PST.NAR-3PL</ta>
            <ta e="T8" id="Seg_756" s="T7">autumn-LOC</ta>
            <ta e="T9" id="Seg_757" s="T8">NEG</ta>
            <ta e="T10" id="Seg_758" s="T9">big</ta>
            <ta e="T11" id="Seg_759" s="T10">snow-EP-LOC</ta>
            <ta e="T12" id="Seg_760" s="T11">bear.[NOM]-3SG</ta>
            <ta e="T13" id="Seg_761" s="T12">come-PST.NAR.[3SG.S]</ta>
            <ta e="T16" id="Seg_762" s="T13">daughter-ADJZ-human.being-PL.[NOM]</ta>
            <ta e="T15" id="Seg_763" s="T14">oven-ILL</ta>
            <ta e="T17" id="Seg_764" s="T15">outwards-ADJZ</ta>
            <ta e="T18" id="Seg_765" s="T17">oven-ILL</ta>
            <ta e="T19" id="Seg_766" s="T18">bread-EP-ACC</ta>
            <ta e="T20" id="Seg_767" s="T19">sit.down-PFV-PST.NAR-3PL</ta>
            <ta e="T21" id="Seg_768" s="T20">bear.[NOM]</ta>
            <ta e="T22" id="Seg_769" s="T21">away</ta>
            <ta e="T23" id="Seg_770" s="T22">break-PST.NAR-3SG.O</ta>
            <ta e="T24" id="Seg_771" s="T23">this</ta>
            <ta e="T25" id="Seg_772" s="T24">raw</ta>
            <ta e="T26" id="Seg_773" s="T25">bread.[NOM]</ta>
            <ta e="T27" id="Seg_774" s="T26">all</ta>
            <ta e="T28" id="Seg_775" s="T27">eat-PST.NAR-3SG.O</ta>
            <ta e="T29" id="Seg_776" s="T28">then</ta>
            <ta e="T30" id="Seg_777" s="T29">porch.[NOM]</ta>
            <ta e="T31" id="Seg_778" s="T30">enter-PST.NAR.[3SG.S]</ta>
            <ta e="T32" id="Seg_779" s="T31">flour-ADJZ</ta>
            <ta e="T33" id="Seg_780" s="T32">bag-ACC</ta>
            <ta e="T34" id="Seg_781" s="T33">away</ta>
            <ta e="T35" id="Seg_782" s="T34">tear-RES-PST.NAR-3SG.O</ta>
            <ta e="T36" id="Seg_783" s="T35">oneself.3SG</ta>
            <ta e="T37" id="Seg_784" s="T36">stature.[NOM]-3SG</ta>
            <ta e="T38" id="Seg_785" s="T37">all</ta>
            <ta e="T39" id="Seg_786" s="T38">white-become-PST.NAR.[3SG.S]</ta>
            <ta e="T40" id="Seg_787" s="T39">then</ta>
            <ta e="T41" id="Seg_788" s="T40">house.[NOM]</ta>
            <ta e="T42" id="Seg_789" s="T41">want-RES-PST.NAR-3SG.S</ta>
            <ta e="T43" id="Seg_790" s="T42">woman-ADJZ-human.being-PL.[NOM]</ta>
            <ta e="T44" id="Seg_791" s="T43">%which</ta>
            <ta e="T45" id="Seg_792" s="T44">cellar-ILL2</ta>
            <ta e="T46" id="Seg_793" s="T45">hide-PFV-PST.NAR-3PL</ta>
            <ta e="T47" id="Seg_794" s="T46">one</ta>
            <ta e="T48" id="Seg_795" s="T47">girl.[NOM]</ta>
            <ta e="T49" id="Seg_796" s="T48">bullet-ACC</ta>
            <ta e="T50" id="Seg_797" s="T49">do-IPFV2-3SG.O</ta>
            <ta e="T51" id="Seg_798" s="T50">bullet-ACC</ta>
            <ta e="T52" id="Seg_799" s="T51">do-PST.NAR-3SG.O</ta>
            <ta e="T53" id="Seg_800" s="T52">rifle-ACC</ta>
            <ta e="T54" id="Seg_801" s="T53">eat-EP-TR-PST.NAR-3SG.O</ta>
            <ta e="T55" id="Seg_802" s="T54">bear.[NOM]</ta>
            <ta e="T56" id="Seg_803" s="T55">window-ALL</ta>
            <ta e="T57" id="Seg_804" s="T56">get.up.fast-PST.NAR.[3SG.S]</ta>
            <ta e="T58" id="Seg_805" s="T57">house-ILL</ta>
            <ta e="T59" id="Seg_806" s="T58">look-DUR-INF</ta>
            <ta e="T60" id="Seg_807" s="T59">(s)he.[NOM]</ta>
            <ta e="T61" id="Seg_808" s="T60">chest.[NOM]</ta>
            <ta e="T62" id="Seg_809" s="T61">%%</ta>
            <ta e="T63" id="Seg_810" s="T62">shoot-PST.NAR-3SG.O</ta>
            <ta e="T64" id="Seg_811" s="T63">then</ta>
            <ta e="T65" id="Seg_812" s="T64">do.what-INF</ta>
            <ta e="T66" id="Seg_813" s="T65">pull.off-INF</ta>
            <ta e="T67" id="Seg_814" s="T66">NEG</ta>
            <ta e="T68" id="Seg_815" s="T67">know-CO-3PL</ta>
            <ta e="T69" id="Seg_816" s="T68">one</ta>
            <ta e="T70" id="Seg_817" s="T69">human.being.[NOM]</ta>
            <ta e="T71" id="Seg_818" s="T70">send-PST.NAR-3SG.O</ta>
            <ta e="T72" id="Seg_819" s="T71">twenty</ta>
            <ta e="T73" id="Seg_820" s="T72">kilometer-ILL</ta>
            <ta e="T74" id="Seg_821" s="T73">verst-ILL-%%</ta>
            <ta e="T75" id="Seg_822" s="T74">from.there</ta>
            <ta e="T76" id="Seg_823" s="T75">man-human.being.[NOM]</ta>
            <ta e="T77" id="Seg_824" s="T76">come-PST.NAR-3PL</ta>
            <ta e="T78" id="Seg_825" s="T77">bear-ACC</ta>
            <ta e="T79" id="Seg_826" s="T78">pull.off-EP-PST.NAR-3PL</ta>
            <ta e="T80" id="Seg_827" s="T79">pull.off-CVB</ta>
            <ta e="T81" id="Seg_828" s="T80">after</ta>
            <ta e="T82" id="Seg_829" s="T81">this</ta>
            <ta e="T83" id="Seg_830" s="T82">bear-GEN</ta>
            <ta e="T84" id="Seg_831" s="T83">meat-ACC</ta>
            <ta e="T85" id="Seg_832" s="T84">all</ta>
            <ta e="T86" id="Seg_833" s="T85">eat-PST.NAR-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_834" s="T0">осень-LOC</ta>
            <ta e="T2" id="Seg_835" s="T1">человек-PL</ta>
            <ta e="T3" id="Seg_836" s="T2">тайга-ILL</ta>
            <ta e="T4" id="Seg_837" s="T3">пойти-PST.NAR-3PL</ta>
            <ta e="T5" id="Seg_838" s="T4">дочь-ADJZ-человек-PL</ta>
            <ta e="T6" id="Seg_839" s="T5">дом-ILL</ta>
            <ta e="T7" id="Seg_840" s="T6">остаться-PST.NAR-3PL</ta>
            <ta e="T8" id="Seg_841" s="T7">осень-LOC</ta>
            <ta e="T9" id="Seg_842" s="T8">NEG</ta>
            <ta e="T10" id="Seg_843" s="T9">большой</ta>
            <ta e="T11" id="Seg_844" s="T10">снег-EP-LOC</ta>
            <ta e="T12" id="Seg_845" s="T11">медведь.[NOM]-3SG</ta>
            <ta e="T13" id="Seg_846" s="T12">прийти-PST.NAR.[3SG.S]</ta>
            <ta e="T16" id="Seg_847" s="T13">дочь-ADJZ-человек-PL.[NOM]</ta>
            <ta e="T15" id="Seg_848" s="T14">печь-ILL</ta>
            <ta e="T17" id="Seg_849" s="T15">наружу-ADJZ</ta>
            <ta e="T18" id="Seg_850" s="T17">печь-ILL</ta>
            <ta e="T19" id="Seg_851" s="T18">хлеб-EP-ACC</ta>
            <ta e="T20" id="Seg_852" s="T19">сесть-PFV-PST.NAR-3PL</ta>
            <ta e="T21" id="Seg_853" s="T20">медведь.[NOM]</ta>
            <ta e="T22" id="Seg_854" s="T21">прочь</ta>
            <ta e="T23" id="Seg_855" s="T22">сломать-PST.NAR-3SG.O</ta>
            <ta e="T24" id="Seg_856" s="T23">этот</ta>
            <ta e="T25" id="Seg_857" s="T24">сырой</ta>
            <ta e="T26" id="Seg_858" s="T25">хлеб.[NOM]</ta>
            <ta e="T27" id="Seg_859" s="T26">весь</ta>
            <ta e="T28" id="Seg_860" s="T27">есть-PST.NAR-3SG.O</ta>
            <ta e="T29" id="Seg_861" s="T28">потом</ta>
            <ta e="T30" id="Seg_862" s="T29">сени.[NOM]</ta>
            <ta e="T31" id="Seg_863" s="T30">зайти-PST.NAR.[3SG.S]</ta>
            <ta e="T32" id="Seg_864" s="T31">мука-ADJZ</ta>
            <ta e="T33" id="Seg_865" s="T32">мешок-ACC</ta>
            <ta e="T34" id="Seg_866" s="T33">прочь</ta>
            <ta e="T35" id="Seg_867" s="T34">порвать-RES-PST.NAR-3SG.O</ta>
            <ta e="T36" id="Seg_868" s="T35">сам.3SG</ta>
            <ta e="T37" id="Seg_869" s="T36">рост.[NOM]-3SG</ta>
            <ta e="T38" id="Seg_870" s="T37">весь</ta>
            <ta e="T39" id="Seg_871" s="T38">белый-стать-PST.NAR.[3SG.S]</ta>
            <ta e="T40" id="Seg_872" s="T39">потом</ta>
            <ta e="T41" id="Seg_873" s="T40">дом.[NOM]</ta>
            <ta e="T42" id="Seg_874" s="T41">хотеть-RES-PST.NAR-3SG.S</ta>
            <ta e="T43" id="Seg_875" s="T42">женщина-ADJZ-человек-PL.[NOM]</ta>
            <ta e="T44" id="Seg_876" s="T43">%который</ta>
            <ta e="T45" id="Seg_877" s="T44">подпол-ILL2</ta>
            <ta e="T46" id="Seg_878" s="T45">спрятаться-PFV-PST.NAR-3PL</ta>
            <ta e="T47" id="Seg_879" s="T46">один</ta>
            <ta e="T48" id="Seg_880" s="T47">девушка.[NOM]</ta>
            <ta e="T49" id="Seg_881" s="T48">пуля-ACC</ta>
            <ta e="T50" id="Seg_882" s="T49">делать-IPFV2-3SG.O</ta>
            <ta e="T51" id="Seg_883" s="T50">пуля-ACC</ta>
            <ta e="T52" id="Seg_884" s="T51">делать-PST.NAR-3SG.O</ta>
            <ta e="T53" id="Seg_885" s="T52">ружье-ACC</ta>
            <ta e="T54" id="Seg_886" s="T53">есть-EP-TR-PST.NAR-3SG.O</ta>
            <ta e="T55" id="Seg_887" s="T54">медведь.[NOM]</ta>
            <ta e="T56" id="Seg_888" s="T55">окно-ALL</ta>
            <ta e="T57" id="Seg_889" s="T56">встать.быстро-PST.NAR.[3SG.S]</ta>
            <ta e="T58" id="Seg_890" s="T57">дом-ILL</ta>
            <ta e="T59" id="Seg_891" s="T58">смотреть-DUR-INF</ta>
            <ta e="T60" id="Seg_892" s="T59">он(а).[NOM]</ta>
            <ta e="T61" id="Seg_893" s="T60">грудь.[NOM]</ta>
            <ta e="T62" id="Seg_894" s="T61">%%</ta>
            <ta e="T63" id="Seg_895" s="T62">стрелять-PST.NAR-3SG.O</ta>
            <ta e="T64" id="Seg_896" s="T63">потом</ta>
            <ta e="T65" id="Seg_897" s="T64">что.делать-INF</ta>
            <ta e="T66" id="Seg_898" s="T65">ободрать-INF</ta>
            <ta e="T67" id="Seg_899" s="T66">NEG</ta>
            <ta e="T68" id="Seg_900" s="T67">знать-CO-3PL</ta>
            <ta e="T69" id="Seg_901" s="T68">один</ta>
            <ta e="T70" id="Seg_902" s="T69">человек.[NOM]</ta>
            <ta e="T71" id="Seg_903" s="T70">посылать-PST.NAR-3SG.O</ta>
            <ta e="T72" id="Seg_904" s="T71">двадцать</ta>
            <ta e="T73" id="Seg_905" s="T72">километр-ILL</ta>
            <ta e="T74" id="Seg_906" s="T73">верста-ILL-%%</ta>
            <ta e="T75" id="Seg_907" s="T74">оттуда</ta>
            <ta e="T76" id="Seg_908" s="T75">мужчина-человек.[NOM]</ta>
            <ta e="T77" id="Seg_909" s="T76">прийти-PST.NAR-3PL</ta>
            <ta e="T78" id="Seg_910" s="T77">медведь-ACC</ta>
            <ta e="T79" id="Seg_911" s="T78">ободрать-EP-PST.NAR-3PL</ta>
            <ta e="T80" id="Seg_912" s="T79">ободрать-CVB</ta>
            <ta e="T81" id="Seg_913" s="T80">после</ta>
            <ta e="T82" id="Seg_914" s="T81">этот</ta>
            <ta e="T83" id="Seg_915" s="T82">медведь-GEN</ta>
            <ta e="T84" id="Seg_916" s="T83">мясо-ACC</ta>
            <ta e="T85" id="Seg_917" s="T84">весь</ta>
            <ta e="T86" id="Seg_918" s="T85">есть-PST.NAR-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_919" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_920" s="T1">n-n:num</ta>
            <ta e="T3" id="Seg_921" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_922" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_923" s="T4">n-n&gt;adj-n-n:num</ta>
            <ta e="T6" id="Seg_924" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_925" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_926" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_927" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_928" s="T9">adj</ta>
            <ta e="T11" id="Seg_929" s="T10">n-n:ins-n:case</ta>
            <ta e="T12" id="Seg_930" s="T11">n-n:case-n:poss</ta>
            <ta e="T13" id="Seg_931" s="T12">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_932" s="T13">n-n&gt;adj-n-n:num-n:case</ta>
            <ta e="T15" id="Seg_933" s="T14">n-n:case</ta>
            <ta e="T17" id="Seg_934" s="T15">adv-n&gt;adj</ta>
            <ta e="T18" id="Seg_935" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_936" s="T18">n-n:ins-n:case</ta>
            <ta e="T20" id="Seg_937" s="T19">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_938" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_939" s="T21">preverb</ta>
            <ta e="T23" id="Seg_940" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_941" s="T23">dem</ta>
            <ta e="T25" id="Seg_942" s="T24">adj</ta>
            <ta e="T26" id="Seg_943" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_944" s="T26">quant</ta>
            <ta e="T28" id="Seg_945" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_946" s="T28">adv</ta>
            <ta e="T30" id="Seg_947" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_948" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_949" s="T31">n-n&gt;adj</ta>
            <ta e="T33" id="Seg_950" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_951" s="T33">preverb</ta>
            <ta e="T35" id="Seg_952" s="T34">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_953" s="T35">emphpro</ta>
            <ta e="T37" id="Seg_954" s="T36">n-n:case-n:poss</ta>
            <ta e="T38" id="Seg_955" s="T37">quant</ta>
            <ta e="T39" id="Seg_956" s="T38">adj-v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_957" s="T39">adv</ta>
            <ta e="T41" id="Seg_958" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_959" s="T41">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_960" s="T42">n-n&gt;adj-n-n:num-n:case</ta>
            <ta e="T44" id="Seg_961" s="T43">%pro</ta>
            <ta e="T45" id="Seg_962" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_963" s="T45">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_964" s="T46">num</ta>
            <ta e="T48" id="Seg_965" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_966" s="T48">n-n:case</ta>
            <ta e="T50" id="Seg_967" s="T49">v-v&gt;v-v:pn</ta>
            <ta e="T51" id="Seg_968" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_969" s="T51">v-v:tense-v:pn</ta>
            <ta e="T53" id="Seg_970" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_971" s="T53">v-n:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_972" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_973" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_974" s="T56">v-v:tense-v:pn</ta>
            <ta e="T58" id="Seg_975" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_976" s="T58">v-v&gt;v-v:inf</ta>
            <ta e="T60" id="Seg_977" s="T59">pers-n:case</ta>
            <ta e="T61" id="Seg_978" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_979" s="T61">%%</ta>
            <ta e="T63" id="Seg_980" s="T62">v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_981" s="T63">adv</ta>
            <ta e="T65" id="Seg_982" s="T64">v-v:inf</ta>
            <ta e="T66" id="Seg_983" s="T65">v-v:inf</ta>
            <ta e="T67" id="Seg_984" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_985" s="T67">v-v:ins-v:pn</ta>
            <ta e="T69" id="Seg_986" s="T68">num</ta>
            <ta e="T70" id="Seg_987" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_988" s="T70">v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_989" s="T71">num</ta>
            <ta e="T73" id="Seg_990" s="T72">n-n:case</ta>
            <ta e="T74" id="Seg_991" s="T73">n-n:case-%%</ta>
            <ta e="T75" id="Seg_992" s="T74">adv</ta>
            <ta e="T76" id="Seg_993" s="T75">n-n-n:case</ta>
            <ta e="T77" id="Seg_994" s="T76">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_995" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_996" s="T78">v-n:ins-v:tense-v:pn</ta>
            <ta e="T80" id="Seg_997" s="T79">v-v&gt;adv</ta>
            <ta e="T81" id="Seg_998" s="T80">pp</ta>
            <ta e="T82" id="Seg_999" s="T81">dem</ta>
            <ta e="T83" id="Seg_1000" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_1001" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_1002" s="T84">quant</ta>
            <ta e="T86" id="Seg_1003" s="T85">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1004" s="T0">n</ta>
            <ta e="T2" id="Seg_1005" s="T1">n</ta>
            <ta e="T3" id="Seg_1006" s="T2">n</ta>
            <ta e="T4" id="Seg_1007" s="T3">v</ta>
            <ta e="T5" id="Seg_1008" s="T4">n</ta>
            <ta e="T6" id="Seg_1009" s="T5">n</ta>
            <ta e="T7" id="Seg_1010" s="T6">v</ta>
            <ta e="T8" id="Seg_1011" s="T7">n</ta>
            <ta e="T9" id="Seg_1012" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_1013" s="T9">adj</ta>
            <ta e="T11" id="Seg_1014" s="T10">n</ta>
            <ta e="T12" id="Seg_1015" s="T11">n</ta>
            <ta e="T13" id="Seg_1016" s="T12">v</ta>
            <ta e="T16" id="Seg_1017" s="T13">n</ta>
            <ta e="T15" id="Seg_1018" s="T14">n</ta>
            <ta e="T17" id="Seg_1019" s="T15">adv</ta>
            <ta e="T18" id="Seg_1020" s="T17">n</ta>
            <ta e="T19" id="Seg_1021" s="T18">n</ta>
            <ta e="T20" id="Seg_1022" s="T19">v</ta>
            <ta e="T21" id="Seg_1023" s="T20">n</ta>
            <ta e="T22" id="Seg_1024" s="T21">preverb</ta>
            <ta e="T23" id="Seg_1025" s="T22">v</ta>
            <ta e="T24" id="Seg_1026" s="T23">pro</ta>
            <ta e="T25" id="Seg_1027" s="T24">adj</ta>
            <ta e="T26" id="Seg_1028" s="T25">n</ta>
            <ta e="T27" id="Seg_1029" s="T26">quant</ta>
            <ta e="T28" id="Seg_1030" s="T27">v</ta>
            <ta e="T29" id="Seg_1031" s="T28">adv</ta>
            <ta e="T30" id="Seg_1032" s="T29">n</ta>
            <ta e="T31" id="Seg_1033" s="T30">v</ta>
            <ta e="T32" id="Seg_1034" s="T31">adj</ta>
            <ta e="T33" id="Seg_1035" s="T32">n</ta>
            <ta e="T34" id="Seg_1036" s="T33">preverb</ta>
            <ta e="T35" id="Seg_1037" s="T34">v</ta>
            <ta e="T36" id="Seg_1038" s="T35">emphpro</ta>
            <ta e="T37" id="Seg_1039" s="T36">n</ta>
            <ta e="T38" id="Seg_1040" s="T37">quant</ta>
            <ta e="T39" id="Seg_1041" s="T38">adj</ta>
            <ta e="T40" id="Seg_1042" s="T39">adv</ta>
            <ta e="T41" id="Seg_1043" s="T40">n</ta>
            <ta e="T42" id="Seg_1044" s="T41">v</ta>
            <ta e="T43" id="Seg_1045" s="T42">adj</ta>
            <ta e="T44" id="Seg_1046" s="T43">%pro</ta>
            <ta e="T45" id="Seg_1047" s="T44">n</ta>
            <ta e="T46" id="Seg_1048" s="T45">v</ta>
            <ta e="T47" id="Seg_1049" s="T46">num</ta>
            <ta e="T48" id="Seg_1050" s="T47">n</ta>
            <ta e="T49" id="Seg_1051" s="T48">n</ta>
            <ta e="T50" id="Seg_1052" s="T49">v</ta>
            <ta e="T51" id="Seg_1053" s="T50">n</ta>
            <ta e="T52" id="Seg_1054" s="T51">v</ta>
            <ta e="T53" id="Seg_1055" s="T52">n</ta>
            <ta e="T54" id="Seg_1056" s="T53">v</ta>
            <ta e="T55" id="Seg_1057" s="T54">n</ta>
            <ta e="T56" id="Seg_1058" s="T55">n</ta>
            <ta e="T57" id="Seg_1059" s="T56">v</ta>
            <ta e="T58" id="Seg_1060" s="T57">n</ta>
            <ta e="T59" id="Seg_1061" s="T58">v</ta>
            <ta e="T60" id="Seg_1062" s="T59">pers</ta>
            <ta e="T61" id="Seg_1063" s="T60">n</ta>
            <ta e="T62" id="Seg_1064" s="T61">%%</ta>
            <ta e="T63" id="Seg_1065" s="T62">v</ta>
            <ta e="T64" id="Seg_1066" s="T63">adv</ta>
            <ta e="T65" id="Seg_1067" s="T64">v</ta>
            <ta e="T66" id="Seg_1068" s="T65">v</ta>
            <ta e="T67" id="Seg_1069" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_1070" s="T67">v</ta>
            <ta e="T69" id="Seg_1071" s="T68">num</ta>
            <ta e="T70" id="Seg_1072" s="T69">n</ta>
            <ta e="T71" id="Seg_1073" s="T70">v</ta>
            <ta e="T72" id="Seg_1074" s="T71">num</ta>
            <ta e="T73" id="Seg_1075" s="T72">n</ta>
            <ta e="T74" id="Seg_1076" s="T73">n</ta>
            <ta e="T75" id="Seg_1077" s="T74">adv</ta>
            <ta e="T76" id="Seg_1078" s="T75">n</ta>
            <ta e="T77" id="Seg_1079" s="T76">v</ta>
            <ta e="T78" id="Seg_1080" s="T77">n</ta>
            <ta e="T79" id="Seg_1081" s="T78">v</ta>
            <ta e="T80" id="Seg_1082" s="T79">adv</ta>
            <ta e="T81" id="Seg_1083" s="T80">pp</ta>
            <ta e="T82" id="Seg_1084" s="T81">pro</ta>
            <ta e="T83" id="Seg_1085" s="T82">n</ta>
            <ta e="T84" id="Seg_1086" s="T83">n</ta>
            <ta e="T85" id="Seg_1087" s="T84">quant</ta>
            <ta e="T86" id="Seg_1088" s="T85">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_1089" s="T0">np:Time</ta>
            <ta e="T2" id="Seg_1090" s="T1">np.h:A</ta>
            <ta e="T3" id="Seg_1091" s="T2">np:G</ta>
            <ta e="T5" id="Seg_1092" s="T4">np.h:Th</ta>
            <ta e="T6" id="Seg_1093" s="T5">np:L</ta>
            <ta e="T8" id="Seg_1094" s="T7">np:Time</ta>
            <ta e="T11" id="Seg_1095" s="T10">np:L</ta>
            <ta e="T12" id="Seg_1096" s="T11">np:A</ta>
            <ta e="T16" id="Seg_1097" s="T13">np.h:A</ta>
            <ta e="T15" id="Seg_1098" s="T14">np:G</ta>
            <ta e="T18" id="Seg_1099" s="T17">np:G</ta>
            <ta e="T19" id="Seg_1100" s="T18">np:Th</ta>
            <ta e="T21" id="Seg_1101" s="T20">np:A</ta>
            <ta e="T23" id="Seg_1102" s="T22">0.3:P</ta>
            <ta e="T26" id="Seg_1103" s="T25">np:P</ta>
            <ta e="T28" id="Seg_1104" s="T27">0.3:A</ta>
            <ta e="T29" id="Seg_1105" s="T28">adv:Time</ta>
            <ta e="T30" id="Seg_1106" s="T29">np:G</ta>
            <ta e="T31" id="Seg_1107" s="T30">0.3:A</ta>
            <ta e="T33" id="Seg_1108" s="T32">np:P</ta>
            <ta e="T35" id="Seg_1109" s="T34">0.3:A</ta>
            <ta e="T37" id="Seg_1110" s="T36">np:P 0.3:Poss</ta>
            <ta e="T40" id="Seg_1111" s="T39">adv:Time</ta>
            <ta e="T42" id="Seg_1112" s="T41">0.3:E</ta>
            <ta e="T43" id="Seg_1113" s="T42">np.h:Th</ta>
            <ta e="T45" id="Seg_1114" s="T44">np:L</ta>
            <ta e="T48" id="Seg_1115" s="T47">np.h:A</ta>
            <ta e="T49" id="Seg_1116" s="T48">np:P</ta>
            <ta e="T51" id="Seg_1117" s="T50">np:P</ta>
            <ta e="T52" id="Seg_1118" s="T51">0.3.h:A</ta>
            <ta e="T53" id="Seg_1119" s="T52">np:Th</ta>
            <ta e="T54" id="Seg_1120" s="T53">0.3.h:A</ta>
            <ta e="T55" id="Seg_1121" s="T54">np:A</ta>
            <ta e="T56" id="Seg_1122" s="T55">np:G</ta>
            <ta e="T58" id="Seg_1123" s="T57">np:G</ta>
            <ta e="T60" id="Seg_1124" s="T59">pro.h:A</ta>
            <ta e="T61" id="Seg_1125" s="T60">np:G</ta>
            <ta e="T64" id="Seg_1126" s="T63">adv:Time</ta>
            <ta e="T65" id="Seg_1127" s="T64">v:Th</ta>
            <ta e="T66" id="Seg_1128" s="T65">v:Th</ta>
            <ta e="T68" id="Seg_1129" s="T67">0.3.h:E</ta>
            <ta e="T70" id="Seg_1130" s="T69">np.h:Th</ta>
            <ta e="T71" id="Seg_1131" s="T70">0.3.h:A</ta>
            <ta e="T74" id="Seg_1132" s="T73">np:G</ta>
            <ta e="T75" id="Seg_1133" s="T74">adv:So</ta>
            <ta e="T76" id="Seg_1134" s="T75">np.h:A</ta>
            <ta e="T78" id="Seg_1135" s="T77">np:P</ta>
            <ta e="T79" id="Seg_1136" s="T78">0.3.h:A</ta>
            <ta e="T83" id="Seg_1137" s="T82">np:Poss</ta>
            <ta e="T84" id="Seg_1138" s="T83">np:P</ta>
            <ta e="T86" id="Seg_1139" s="T85">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1140" s="T1">np.h:S</ta>
            <ta e="T4" id="Seg_1141" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_1142" s="T4">np.h:S</ta>
            <ta e="T7" id="Seg_1143" s="T6">v:pred</ta>
            <ta e="T12" id="Seg_1144" s="T11">np:S</ta>
            <ta e="T13" id="Seg_1145" s="T12">v:pred</ta>
            <ta e="T16" id="Seg_1146" s="T13">np.h:S</ta>
            <ta e="T19" id="Seg_1147" s="T18">np:O</ta>
            <ta e="T20" id="Seg_1148" s="T19">v:pred</ta>
            <ta e="T21" id="Seg_1149" s="T20">np:S</ta>
            <ta e="T23" id="Seg_1150" s="T22">v:pred 0.3:O</ta>
            <ta e="T26" id="Seg_1151" s="T25">np:O</ta>
            <ta e="T28" id="Seg_1152" s="T27">0.3:S v:pred</ta>
            <ta e="T31" id="Seg_1153" s="T30">0.3:S v:pred</ta>
            <ta e="T33" id="Seg_1154" s="T32">np:O</ta>
            <ta e="T35" id="Seg_1155" s="T34">0.3:S v:pred</ta>
            <ta e="T37" id="Seg_1156" s="T36">np:S</ta>
            <ta e="T39" id="Seg_1157" s="T38">v:pred</ta>
            <ta e="T42" id="Seg_1158" s="T41">0.3:S v:pred</ta>
            <ta e="T43" id="Seg_1159" s="T42">np.h:S</ta>
            <ta e="T46" id="Seg_1160" s="T43">s:rel</ta>
            <ta e="T48" id="Seg_1161" s="T47">np.h:S</ta>
            <ta e="T49" id="Seg_1162" s="T48">np:O</ta>
            <ta e="T50" id="Seg_1163" s="T49">v:pred</ta>
            <ta e="T51" id="Seg_1164" s="T50">np:O</ta>
            <ta e="T52" id="Seg_1165" s="T51">0.3.h:S v:pred</ta>
            <ta e="T53" id="Seg_1166" s="T52">np:O</ta>
            <ta e="T54" id="Seg_1167" s="T53">0.3.h:S v:pred</ta>
            <ta e="T55" id="Seg_1168" s="T54">np:S</ta>
            <ta e="T57" id="Seg_1169" s="T56">v:pred</ta>
            <ta e="T59" id="Seg_1170" s="T57">s:purp</ta>
            <ta e="T60" id="Seg_1171" s="T59">pro.h:S</ta>
            <ta e="T61" id="Seg_1172" s="T60">np:O</ta>
            <ta e="T63" id="Seg_1173" s="T62">v:pred</ta>
            <ta e="T65" id="Seg_1174" s="T64">v:O</ta>
            <ta e="T66" id="Seg_1175" s="T65">v:O</ta>
            <ta e="T68" id="Seg_1176" s="T67">0.3.h:S v:pred</ta>
            <ta e="T70" id="Seg_1177" s="T69">np.h:O</ta>
            <ta e="T71" id="Seg_1178" s="T70">0.3.h:S v:pred</ta>
            <ta e="T76" id="Seg_1179" s="T75">np.h:S</ta>
            <ta e="T77" id="Seg_1180" s="T76">v:pred</ta>
            <ta e="T78" id="Seg_1181" s="T77">np:O</ta>
            <ta e="T79" id="Seg_1182" s="T78">0.3.h:S v:pred</ta>
            <ta e="T81" id="Seg_1183" s="T79">s:temp</ta>
            <ta e="T84" id="Seg_1184" s="T83">np:O</ta>
            <ta e="T86" id="Seg_1185" s="T85">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T27" id="Seg_1186" s="T26">RUS:core</ta>
            <ta e="T29" id="Seg_1187" s="T28">RUS:core</ta>
            <ta e="T32" id="Seg_1188" s="T31">RUS:cult</ta>
            <ta e="T38" id="Seg_1189" s="T37">RUS:core</ta>
            <ta e="T40" id="Seg_1190" s="T39">RUS:core</ta>
            <ta e="T44" id="Seg_1191" s="T43">%RUS:gram</ta>
            <ta e="T45" id="Seg_1192" s="T44">RUS:cult</ta>
            <ta e="T56" id="Seg_1193" s="T55">RUS:cult</ta>
            <ta e="T64" id="Seg_1194" s="T63">RUS:core</ta>
            <ta e="T74" id="Seg_1195" s="T73">RUS:cult</ta>
            <ta e="T85" id="Seg_1196" s="T84">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_1197" s="T0">Осенью люди ушли в тайгу.</ta>
            <ta e="T7" id="Seg_1198" s="T4">Женщины остались дома.</ta>
            <ta e="T13" id="Seg_1199" s="T7">Осенью по небольшому снегу медведь пришел.</ta>
            <ta e="T20" id="Seg_1200" s="T13">Женщины в уличную печку хлеб поставили.</ta>
            <ta e="T23" id="Seg_1201" s="T20">Медведь (прочь) сломал.</ta>
            <ta e="T28" id="Seg_1202" s="T23">Этот сырой хлеб весь съел.</ta>
            <ta e="T31" id="Seg_1203" s="T28">Потом в сени зашел.</ta>
            <ta e="T35" id="Seg_1204" s="T31">Мучной мешок весь порвал.</ta>
            <ta e="T39" id="Seg_1205" s="T35">Сам весь побелел.</ta>
            <ta e="T42" id="Seg_1206" s="T39">Потом в дом захотел.</ta>
            <ta e="T46" id="Seg_1207" s="T42">Женщины, которые в подпол попрятались… [?]</ta>
            <ta e="T50" id="Seg_1208" s="T46">Одна девушка пулю делает.</ta>
            <ta e="T54" id="Seg_1209" s="T50">Пулю сделала, ружье зарядила (=накормила). </ta>
            <ta e="T59" id="Seg_1210" s="T54">Медведь к окошку встал, в дом посмотреть.</ta>
            <ta e="T63" id="Seg_1211" s="T59">Она в грудь (?) выстрелила.</ta>
            <ta e="T68" id="Seg_1212" s="T63">Потом это сделать, шкуру ободрать не могут.</ta>
            <ta e="T74" id="Seg_1213" s="T68">Одного человека отправил за 20 километров, вёрст.</ta>
            <ta e="T77" id="Seg_1214" s="T74">Оттуда мужчины пришли.</ta>
            <ta e="T79" id="Seg_1215" s="T77">Медведя ободрали.</ta>
            <ta e="T86" id="Seg_1216" s="T79">Ободрав, это медвежье мясо всё съели.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_1217" s="T0">In autumn people went to the taiga.</ta>
            <ta e="T7" id="Seg_1218" s="T4">Women stayed at home.</ta>
            <ta e="T13" id="Seg_1219" s="T7">In autumn a bear came by thin snow.</ta>
            <ta e="T20" id="Seg_1220" s="T13">The women put bread into the outdoor stove.</ta>
            <ta e="T23" id="Seg_1221" s="T20">The bear broke it.</ta>
            <ta e="T28" id="Seg_1222" s="T23">It ate all this raw bread.</ta>
            <ta e="T31" id="Seg_1223" s="T28">Then it entered the porch.</ta>
            <ta e="T35" id="Seg_1224" s="T31">It tore the flour sack entirely.</ta>
            <ta e="T39" id="Seg_1225" s="T35">It became white with flour itself.</ta>
            <ta e="T42" id="Seg_1226" s="T39">Then it wanted (to enter) the house.</ta>
            <ta e="T46" id="Seg_1227" s="T42">The women, that hid in the cellar… [?]</ta>
            <ta e="T50" id="Seg_1228" s="T46">One girl made a bullet.</ta>
            <ta e="T54" id="Seg_1229" s="T50">She made a bullet and loaded the rifle.</ta>
            <ta e="T59" id="Seg_1230" s="T54">The bear got up at the window to look inside the house.</ta>
            <ta e="T63" id="Seg_1231" s="T59">She shot [it] (?) into the chest.</ta>
            <ta e="T68" id="Seg_1232" s="T63">They can't do that, skin the bear.</ta>
            <ta e="T74" id="Seg_1233" s="T68">One person was sent twenty kilometers away (?).</ta>
            <ta e="T77" id="Seg_1234" s="T74">The men came from there.</ta>
            <ta e="T79" id="Seg_1235" s="T77">They skinned the bear.</ta>
            <ta e="T86" id="Seg_1236" s="T79">Having skinned it, they ate all of the bear's meat.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_1237" s="T0">Im Herbst gingen die Menschen in die Taiga.</ta>
            <ta e="T7" id="Seg_1238" s="T4">Die Frauen blieben zu Hause.</ta>
            <ta e="T13" id="Seg_1239" s="T7">Im Herbst kam ein Bär im dünnen Schnee.</ta>
            <ta e="T20" id="Seg_1240" s="T13">Die Frauen schoben Brot in den Ofen draußen.</ta>
            <ta e="T23" id="Seg_1241" s="T20">Der Bär zerschlug ihn.</ta>
            <ta e="T28" id="Seg_1242" s="T23">Er aß das ganze rohe Brot.</ta>
            <ta e="T31" id="Seg_1243" s="T28">Dann trat er in den Windfang.</ta>
            <ta e="T35" id="Seg_1244" s="T31">Er zerriss den Mehlsack vollkommen.</ta>
            <ta e="T39" id="Seg_1245" s="T35">Er wurde selbst ganz weiß (durch das Mehl).</ta>
            <ta e="T42" id="Seg_1246" s="T39">Dann wollte er ins Haus.</ta>
            <ta e="T46" id="Seg_1247" s="T42">Die Frauen, die sich im Keller versteckten… [?]</ta>
            <ta e="T50" id="Seg_1248" s="T46">Ein Mädchen machte eine Kugel.</ta>
            <ta e="T54" id="Seg_1249" s="T50">Sie machte eine Kugel und lud das Gewehr damit.</ta>
            <ta e="T59" id="Seg_1250" s="T54">Der Bär stand zum Fenster auf, um ins Haus zu sehen.</ta>
            <ta e="T63" id="Seg_1251" s="T59">Sie schoss [ihm] (?) in die Brust.</ta>
            <ta e="T68" id="Seg_1252" s="T63">Sie können es nicht machen, den Bär häuten.</ta>
            <ta e="T74" id="Seg_1253" s="T68">Eine Person schickten sie zwanzig Kilometer weit weg, nach (?).</ta>
            <ta e="T77" id="Seg_1254" s="T74">Die Männer kamen von dort.</ta>
            <ta e="T79" id="Seg_1255" s="T77">Sie häuteten den Bär.</ta>
            <ta e="T86" id="Seg_1256" s="T79">Nach dem Häuten aßen sie das ganze Bärenfleisch.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_1257" s="T0">Зимой люди ушли в тайгу.</ta>
            <ta e="T7" id="Seg_1258" s="T4">Женщины остались дома.</ta>
            <ta e="T13" id="Seg_1259" s="T7">Зимой по небольшому снегу медведь пришел.</ta>
            <ta e="T20" id="Seg_1260" s="T13">Женщины в уличную печку хлеб посадили.</ta>
            <ta e="T23" id="Seg_1261" s="T20">Медведь (прочь) сломал.</ta>
            <ta e="T28" id="Seg_1262" s="T23">Этот хлеб весь съел.</ta>
            <ta e="T31" id="Seg_1263" s="T28">Потом в сени зашел.</ta>
            <ta e="T35" id="Seg_1264" s="T31">Мучной мешок весь порвал.</ta>
            <ta e="T39" id="Seg_1265" s="T35">Сам весь побелел.</ta>
            <ta e="T42" id="Seg_1266" s="T39">Потом в дом захотел.</ta>
            <ta e="T46" id="Seg_1267" s="T42">Женщины которые в подпол попрятались.</ta>
            <ta e="T50" id="Seg_1268" s="T46">Одна девушка пулю делает.</ta>
            <ta e="T54" id="Seg_1269" s="T50">Пулю сделала, ружье зарядила (=накормила). </ta>
            <ta e="T59" id="Seg_1270" s="T54">Медведь к окошку встал, в дом посмотреть.</ta>
            <ta e="T63" id="Seg_1271" s="T59">Она в грудь выстрелила.</ta>
            <ta e="T68" id="Seg_1272" s="T63">Потом шкуру ободрать не могут (не знают, не умеют).</ta>
            <ta e="T74" id="Seg_1273" s="T68">Один человек отправил за 20 километров (?), верст.</ta>
            <ta e="T77" id="Seg_1274" s="T74">Оттуда мужчины пришли.</ta>
            <ta e="T79" id="Seg_1275" s="T77">Медведя ободрали.</ta>
            <ta e="T86" id="Seg_1276" s="T79">Обдирая, это медвежье мясо всё съели.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T23" id="Seg_1277" s="T20">[AAV:] Корг &gt; qorʁu</ta>
            <ta e="T31" id="Seg_1278" s="T28">[AAV:] Illative expected (qošünent)</ta>
            <ta e="T46" id="Seg_1279" s="T42">[AAV:] podpolǯəlʼ ?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T16" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
