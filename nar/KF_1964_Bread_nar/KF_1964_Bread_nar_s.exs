<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KF_1964_Bread_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KF_1964_Bread_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">75</ud-information>
            <ud-information attribute-name="# HIAT:w">57</ud-information>
            <ud-information attribute-name="# e">57</ud-information>
            <ud-information attribute-name="# HIAT:u">13</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KF">
            <abbreviation>KF</abbreviation>
            <sex value="f" />
            <languages-used>
               <language lang="sel" />
            </languages-used>
            <l1>
               <language lang="sel" />
            </l1>
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KF"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T57" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">kuldʼiŋ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">man</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">nʼäjam</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">meːkwam</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">man</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">iːqwam</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">mukam</ts>
                  <nts id="Seg_26" n="HIAT:ip">,</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">magazinqan</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">muka</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">iːkowam</ts>
                  <nts id="Seg_36" n="HIAT:ip">,</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_39" n="HIAT:w" s="T10">täwwam</ts>
                  <nts id="Seg_40" n="HIAT:ip">.</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_43" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_45" n="HIAT:w" s="T11">towwam</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">mukam</ts>
                  <nts id="Seg_49" n="HIAT:ip">,</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">maːttə</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">taːtnaw</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">sečas</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">meːku</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">nʼäːjam</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_68" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">man</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">kwašonkam</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">iːqwam</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">twäktəku</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">nʼäjam</ts>
                  <nts id="Seg_83" n="HIAT:ip">,</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">mukam</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">tupalgu</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_93" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">qaran</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">meːčam</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">nʼäːjam</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_105" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_107" n="HIAT:w" s="T28">man</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">nʼäːj</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">iːsqlaːŋ</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_116" n="HIAT:w" s="T31">eːsuŋ</ts>
                  <nts id="Seg_117" n="HIAT:ip">,</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_120" n="HIAT:w" s="T32">assə</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_123" n="HIAT:w" s="T33">nʼüːetʼi</ts>
                  <nts id="Seg_124" n="HIAT:ip">.</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_127" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_129" n="HIAT:w" s="T34">man</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_132" n="HIAT:w" s="T35">nʼäːjjam</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_135" n="HIAT:w" s="T36">omtalǯan</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_138" n="HIAT:w" s="T37">tʼüj</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_141" n="HIAT:w" s="T38">pečʼində</ts>
                  <nts id="Seg_142" n="HIAT:ip">.</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_145" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_147" n="HIAT:w" s="T39">nʼäj</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_150" n="HIAT:w" s="T40">müssaːŋ</ts>
                  <nts id="Seg_151" n="HIAT:ip">.</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_154" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_156" n="HIAT:w" s="T41">pečʼiqɨnnɨ</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_159" n="HIAT:w" s="T42">sapaspaŋ</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_163" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_165" n="HIAT:w" s="T43">assə</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_168" n="HIAT:w" s="T44">müsambə</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_171" n="HIAT:w" s="T45">nʼäːj</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_174" n="HIAT:w" s="T46">tarassə</ts>
                  <nts id="Seg_175" n="HIAT:ip">.</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_178" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_180" n="HIAT:w" s="T47">sičas</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_183" n="HIAT:w" s="T48">man</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_186" n="HIAT:w" s="T49">nʼäːj</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_189" n="HIAT:w" s="T50">meːǯanda</ts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_193" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_195" n="HIAT:w" s="T51">müsamba</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_198" n="HIAT:w" s="T52">nʼäːj</ts>
                  <nts id="Seg_199" n="HIAT:ip">.</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_202" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_204" n="HIAT:w" s="T53">man</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_207" n="HIAT:w" s="T54">sapanʼnʼam</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_210" n="HIAT:w" s="T55">nʼäːjam</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_213" n="HIAT:w" s="T56">pečʼiqannɨ</ts>
                  <nts id="Seg_214" n="HIAT:ip">.</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T57" id="Seg_216" n="sc" s="T0">
               <ts e="T1" id="Seg_218" n="e" s="T0">kuldʼiŋ </ts>
               <ts e="T2" id="Seg_220" n="e" s="T1">man </ts>
               <ts e="T3" id="Seg_222" n="e" s="T2">nʼäjam </ts>
               <ts e="T4" id="Seg_224" n="e" s="T3">meːkwam. </ts>
               <ts e="T5" id="Seg_226" n="e" s="T4">man </ts>
               <ts e="T6" id="Seg_228" n="e" s="T5">iːqwam </ts>
               <ts e="T7" id="Seg_230" n="e" s="T6">mukam, </ts>
               <ts e="T8" id="Seg_232" n="e" s="T7">magazinqan </ts>
               <ts e="T9" id="Seg_234" n="e" s="T8">muka </ts>
               <ts e="T10" id="Seg_236" n="e" s="T9">iːkowam, </ts>
               <ts e="T11" id="Seg_238" n="e" s="T10">täwwam. </ts>
               <ts e="T12" id="Seg_240" n="e" s="T11">towwam </ts>
               <ts e="T13" id="Seg_242" n="e" s="T12">mukam, </ts>
               <ts e="T14" id="Seg_244" n="e" s="T13">maːttə </ts>
               <ts e="T15" id="Seg_246" n="e" s="T14">taːtnaw </ts>
               <ts e="T16" id="Seg_248" n="e" s="T15">sečas </ts>
               <ts e="T17" id="Seg_250" n="e" s="T16">meːku </ts>
               <ts e="T18" id="Seg_252" n="e" s="T17">nʼäːjam. </ts>
               <ts e="T19" id="Seg_254" n="e" s="T18">man </ts>
               <ts e="T20" id="Seg_256" n="e" s="T19">kwašonkam </ts>
               <ts e="T21" id="Seg_258" n="e" s="T20">iːqwam </ts>
               <ts e="T22" id="Seg_260" n="e" s="T21">twäktəku </ts>
               <ts e="T23" id="Seg_262" n="e" s="T22">nʼäjam, </ts>
               <ts e="T24" id="Seg_264" n="e" s="T23">mukam </ts>
               <ts e="T25" id="Seg_266" n="e" s="T24">tupalgu. </ts>
               <ts e="T26" id="Seg_268" n="e" s="T25">qaran </ts>
               <ts e="T27" id="Seg_270" n="e" s="T26">meːčam </ts>
               <ts e="T28" id="Seg_272" n="e" s="T27">nʼäːjam. </ts>
               <ts e="T29" id="Seg_274" n="e" s="T28">man </ts>
               <ts e="T30" id="Seg_276" n="e" s="T29">nʼäːj </ts>
               <ts e="T31" id="Seg_278" n="e" s="T30">iːsqlaːŋ </ts>
               <ts e="T32" id="Seg_280" n="e" s="T31">eːsuŋ, </ts>
               <ts e="T33" id="Seg_282" n="e" s="T32">assə </ts>
               <ts e="T34" id="Seg_284" n="e" s="T33">nʼüːetʼi. </ts>
               <ts e="T35" id="Seg_286" n="e" s="T34">man </ts>
               <ts e="T36" id="Seg_288" n="e" s="T35">nʼäːjjam </ts>
               <ts e="T37" id="Seg_290" n="e" s="T36">omtalǯan </ts>
               <ts e="T38" id="Seg_292" n="e" s="T37">tʼüj </ts>
               <ts e="T39" id="Seg_294" n="e" s="T38">pečʼində. </ts>
               <ts e="T40" id="Seg_296" n="e" s="T39">nʼäj </ts>
               <ts e="T41" id="Seg_298" n="e" s="T40">müssaːŋ. </ts>
               <ts e="T42" id="Seg_300" n="e" s="T41">pečʼiqɨnnɨ </ts>
               <ts e="T43" id="Seg_302" n="e" s="T42">sapaspaŋ. </ts>
               <ts e="T44" id="Seg_304" n="e" s="T43">assə </ts>
               <ts e="T45" id="Seg_306" n="e" s="T44">müsambə </ts>
               <ts e="T46" id="Seg_308" n="e" s="T45">nʼäːj </ts>
               <ts e="T47" id="Seg_310" n="e" s="T46">tarassə. </ts>
               <ts e="T48" id="Seg_312" n="e" s="T47">sičas </ts>
               <ts e="T49" id="Seg_314" n="e" s="T48">man </ts>
               <ts e="T50" id="Seg_316" n="e" s="T49">nʼäːj </ts>
               <ts e="T51" id="Seg_318" n="e" s="T50">meːǯanda. </ts>
               <ts e="T52" id="Seg_320" n="e" s="T51">müsamba </ts>
               <ts e="T53" id="Seg_322" n="e" s="T52">nʼäːj. </ts>
               <ts e="T54" id="Seg_324" n="e" s="T53">man </ts>
               <ts e="T55" id="Seg_326" n="e" s="T54">sapanʼnʼam </ts>
               <ts e="T56" id="Seg_328" n="e" s="T55">nʼäːjam </ts>
               <ts e="T57" id="Seg_330" n="e" s="T56">pečʼiqannɨ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_331" s="T0">KF_1964_Bread_nar.001 (001.001)</ta>
            <ta e="T11" id="Seg_332" s="T4">KF_1964_Bread_nar.002 (001.002)</ta>
            <ta e="T18" id="Seg_333" s="T11">KF_1964_Bread_nar.003 (001.003)</ta>
            <ta e="T25" id="Seg_334" s="T18">KF_1964_Bread_nar.004 (001.004)</ta>
            <ta e="T28" id="Seg_335" s="T25">KF_1964_Bread_nar.005 (001.005)</ta>
            <ta e="T34" id="Seg_336" s="T28">KF_1964_Bread_nar.006 (001.006)</ta>
            <ta e="T39" id="Seg_337" s="T34">KF_1964_Bread_nar.007 (001.007)</ta>
            <ta e="T41" id="Seg_338" s="T39">KF_1964_Bread_nar.008 (001.008)</ta>
            <ta e="T43" id="Seg_339" s="T41">KF_1964_Bread_nar.009 (001.009)</ta>
            <ta e="T47" id="Seg_340" s="T43">KF_1964_Bread_nar.010 (001.010)</ta>
            <ta e="T51" id="Seg_341" s="T47">KF_1964_Bread_nar.011 (001.011)</ta>
            <ta e="T53" id="Seg_342" s="T51">KF_1964_Bread_nar.012 (001.012)</ta>
            <ta e="T57" id="Seg_343" s="T53">KF_1964_Bread_nar.013 (001.013)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_344" s="T0">куlдʼиң ман ′нʼӓjам ме̄квам.</ta>
            <ta e="T11" id="Seg_345" s="T4">ман ′ӣkwам му′кам, мага′зинkан ′мука ӣко′вам, ′тӓввам.</ta>
            <ta e="T18" id="Seg_346" s="T11">′товвам ′мукам, ма̄ттъ ′та̄тнаw сечас ме̄ку ′нʼӓ̄jам.</ta>
            <ta e="T25" id="Seg_347" s="T18">ман квашонкам ′ӣkwам тwӓктъку ′нʼӓjам, му′кам ту′пал(l)гу.</ta>
            <ta e="T28" id="Seg_348" s="T25">kа′ран ме̄тшам ′нʼӓ̄jам.</ta>
            <ta e="T34" id="Seg_349" s="T28">ман нʼӓ̄й ӣсklа̄ң е̄′суң, ′ассъ ′нʼӱ̄етʼи.</ta>
            <ta e="T39" id="Seg_350" s="T34">ман нʼӓ̄йjам ′омтал′джан тʼӱй печʼиндъ.</ta>
            <ta e="T41" id="Seg_351" s="T39">нʼӓй мӱ′сса̄ң.</ta>
            <ta e="T43" id="Seg_352" s="T41">′печиkынны са′пас‵паң.</ta>
            <ta e="T47" id="Seg_353" s="T43">ассъ ′мӱс(с)амбъ нʼӓ̄й та′рассъ.</ta>
            <ta e="T51" id="Seg_354" s="T47">сичас ман нʼӓ̄й ′ме̄джанда.</ta>
            <ta e="T53" id="Seg_355" s="T51">мӱ′самба нʼӓ̄й.</ta>
            <ta e="T57" id="Seg_356" s="T53">ман са′панʼнʼам ′нʼӓ̄jам ‵печиkа′нны.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_357" s="T0">kuldʼiŋ man nʼäjam meːkvam.</ta>
            <ta e="T11" id="Seg_358" s="T4">man iːqwam mukam, magazinqan muka iːkovam, tävvam.</ta>
            <ta e="T18" id="Seg_359" s="T11">tovvam mukam, maːttə taːtnaw sečas meːku nʼäːjam.</ta>
            <ta e="T25" id="Seg_360" s="T18">man kwašonkam iːqwam twäktəku nʼäjam, mukam tupal(l)gu.</ta>
            <ta e="T28" id="Seg_361" s="T25">qaran meːtšam nʼäːjam.</ta>
            <ta e="T34" id="Seg_362" s="T28">man nʼäːj iːsqlaːŋ eːsuŋ, assə nʼüːetʼi.</ta>
            <ta e="T39" id="Seg_363" s="T34">man nʼäːjjam omtalǯan tʼüj pečʼində.</ta>
            <ta e="T41" id="Seg_364" s="T39">nʼäj müssaːŋ.</ta>
            <ta e="T43" id="Seg_365" s="T41">pečiqɨnnɨ sapaspaŋ.</ta>
            <ta e="T47" id="Seg_366" s="T43">assə müs(s)ambə nʼäːj tarassə.</ta>
            <ta e="T51" id="Seg_367" s="T47">sičas man nʼäːj meːǯanda.</ta>
            <ta e="T53" id="Seg_368" s="T51">müsamba nʼäːj.</ta>
            <ta e="T57" id="Seg_369" s="T53">man sapanʼnʼam nʼäːjam pečiqannɨ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_370" s="T0">kuldʼiŋ man nʼäjam meːkwam. </ta>
            <ta e="T11" id="Seg_371" s="T4">man iːqwam mukam, magazinqan muka iːkowam, täwwam. </ta>
            <ta e="T18" id="Seg_372" s="T11">towwam mukam, maːttə taːtnaw sečas meːku nʼäːjam. </ta>
            <ta e="T25" id="Seg_373" s="T18">man kwašonkam iːqwam twäktəku nʼäjam, mukam tupalgu. </ta>
            <ta e="T28" id="Seg_374" s="T25">qaran meːčam nʼäːjam. </ta>
            <ta e="T34" id="Seg_375" s="T28">man nʼäːj iːsqlaːŋ eːsuŋ, assə nʼüːetʼi. </ta>
            <ta e="T39" id="Seg_376" s="T34">man nʼäːjjam omtalǯan tʼüj pečʼində. </ta>
            <ta e="T41" id="Seg_377" s="T39">nʼäj müssaːŋ. </ta>
            <ta e="T43" id="Seg_378" s="T41">pečʼiqɨnnɨ sapaspaŋ. </ta>
            <ta e="T47" id="Seg_379" s="T43">assə müsambə nʼäːj tarassə. </ta>
            <ta e="T51" id="Seg_380" s="T47">sičas man nʼäːj meːǯanda. </ta>
            <ta e="T53" id="Seg_381" s="T51">müsamba nʼäːj. </ta>
            <ta e="T57" id="Seg_382" s="T53">man sapanʼnʼam nʼäːjam pečʼiqannɨ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_383" s="T0">kuldʼi-ŋ</ta>
            <ta e="T2" id="Seg_384" s="T1">man</ta>
            <ta e="T3" id="Seg_385" s="T2">nʼäj-a-m</ta>
            <ta e="T4" id="Seg_386" s="T3">meː-k-wa-m</ta>
            <ta e="T5" id="Seg_387" s="T4">man</ta>
            <ta e="T6" id="Seg_388" s="T5">iː-q-wa-m</ta>
            <ta e="T7" id="Seg_389" s="T6">muka-m</ta>
            <ta e="T8" id="Seg_390" s="T7">magazin-qan</ta>
            <ta e="T9" id="Seg_391" s="T8">muka</ta>
            <ta e="T10" id="Seg_392" s="T9">iː-ko-wa-m</ta>
            <ta e="T11" id="Seg_393" s="T10">täw-wa-m</ta>
            <ta e="T12" id="Seg_394" s="T11">tow-wa-m</ta>
            <ta e="T13" id="Seg_395" s="T12">muka-m</ta>
            <ta e="T14" id="Seg_396" s="T13">maːt-tə</ta>
            <ta e="T15" id="Seg_397" s="T14">taːt-na-w</ta>
            <ta e="T16" id="Seg_398" s="T15">sečas</ta>
            <ta e="T17" id="Seg_399" s="T16">meː-ku</ta>
            <ta e="T18" id="Seg_400" s="T17">nʼäːj-a-m</ta>
            <ta e="T19" id="Seg_401" s="T18">man</ta>
            <ta e="T20" id="Seg_402" s="T19">kwašonka-m</ta>
            <ta e="T21" id="Seg_403" s="T20">iː-q-wa-m</ta>
            <ta e="T22" id="Seg_404" s="T21">twäktə-ku</ta>
            <ta e="T23" id="Seg_405" s="T22">nʼäj-a-m</ta>
            <ta e="T24" id="Seg_406" s="T23">muka-m</ta>
            <ta e="T25" id="Seg_407" s="T24">tupa-l-gu</ta>
            <ta e="T26" id="Seg_408" s="T25">qara-n</ta>
            <ta e="T27" id="Seg_409" s="T26">meː-ča-m</ta>
            <ta e="T28" id="Seg_410" s="T27">nʼäːj-a-m</ta>
            <ta e="T29" id="Seg_411" s="T28">man</ta>
            <ta e="T30" id="Seg_412" s="T29">nʼäːj</ta>
            <ta e="T31" id="Seg_413" s="T30">iːsqlaː-ŋ</ta>
            <ta e="T32" id="Seg_414" s="T31">eː-su-ŋ</ta>
            <ta e="T33" id="Seg_415" s="T32">assə</ta>
            <ta e="T34" id="Seg_416" s="T33">nʼüːetʼi</ta>
            <ta e="T35" id="Seg_417" s="T34">man</ta>
            <ta e="T36" id="Seg_418" s="T35">nʼäːjj-a-m</ta>
            <ta e="T37" id="Seg_419" s="T36">omta-lǯa-n</ta>
            <ta e="T38" id="Seg_420" s="T37">tʼü-j</ta>
            <ta e="T39" id="Seg_421" s="T38">pečʼ-i-ndə</ta>
            <ta e="T40" id="Seg_422" s="T39">nʼäj</ta>
            <ta e="T41" id="Seg_423" s="T40">müssaː-ŋ</ta>
            <ta e="T42" id="Seg_424" s="T41">pečʼ-i-qɨnnɨ</ta>
            <ta e="T43" id="Seg_425" s="T42">sapas-pa-ŋ</ta>
            <ta e="T44" id="Seg_426" s="T43">assə</ta>
            <ta e="T45" id="Seg_427" s="T44">müsa-mbə</ta>
            <ta e="T46" id="Seg_428" s="T45">nʼäːj</ta>
            <ta e="T47" id="Seg_429" s="T46">taras-sə</ta>
            <ta e="T48" id="Seg_430" s="T47">sičas</ta>
            <ta e="T49" id="Seg_431" s="T48">man</ta>
            <ta e="T50" id="Seg_432" s="T49">nʼäːj</ta>
            <ta e="T51" id="Seg_433" s="T50">meː-ǯa-nda</ta>
            <ta e="T52" id="Seg_434" s="T51">müsa-mba</ta>
            <ta e="T53" id="Seg_435" s="T52">nʼäːj</ta>
            <ta e="T54" id="Seg_436" s="T53">man</ta>
            <ta e="T55" id="Seg_437" s="T54">sapanʼ-nʼa-m</ta>
            <ta e="T56" id="Seg_438" s="T55">nʼäːj-a-m</ta>
            <ta e="T57" id="Seg_439" s="T56">pečʼ-i-qannɨ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_440" s="T0">kulʼdi-k</ta>
            <ta e="T2" id="Seg_441" s="T1">man</ta>
            <ta e="T3" id="Seg_442" s="T2">nʼaj-ɨ-m</ta>
            <ta e="T4" id="Seg_443" s="T3">meː-kku-ŋɨ-m</ta>
            <ta e="T5" id="Seg_444" s="T4">man</ta>
            <ta e="T6" id="Seg_445" s="T5">iː-kku-ŋɨ-m</ta>
            <ta e="T7" id="Seg_446" s="T6">muka-m</ta>
            <ta e="T8" id="Seg_447" s="T7">magazin-qən</ta>
            <ta e="T9" id="Seg_448" s="T8">muka</ta>
            <ta e="T10" id="Seg_449" s="T9">iː-kku-ŋɨ-m</ta>
            <ta e="T11" id="Seg_450" s="T10">taw-ŋɨ-m</ta>
            <ta e="T12" id="Seg_451" s="T11">taw-ŋɨ-m</ta>
            <ta e="T13" id="Seg_452" s="T12">muka-m</ta>
            <ta e="T14" id="Seg_453" s="T13">maːt-ndɨ</ta>
            <ta e="T15" id="Seg_454" s="T14">tadɨ-ŋɨ-m</ta>
            <ta e="T16" id="Seg_455" s="T15">sečas</ta>
            <ta e="T17" id="Seg_456" s="T16">meː-gu</ta>
            <ta e="T18" id="Seg_457" s="T17">nʼaj-ɨ-m</ta>
            <ta e="T19" id="Seg_458" s="T18">man</ta>
            <ta e="T20" id="Seg_459" s="T19">kwašonka-m</ta>
            <ta e="T21" id="Seg_460" s="T20">iː-kku-ŋɨ-m</ta>
            <ta e="T22" id="Seg_461" s="T21">twäktə-gu</ta>
            <ta e="T23" id="Seg_462" s="T22">nʼaj-ɨ-m</ta>
            <ta e="T24" id="Seg_463" s="T23">muka-m</ta>
            <ta e="T25" id="Seg_464" s="T24">tupa-lɨ-gu</ta>
            <ta e="T26" id="Seg_465" s="T25">qarɨ-n</ta>
            <ta e="T27" id="Seg_466" s="T26">meː-nǯɨ-m</ta>
            <ta e="T28" id="Seg_467" s="T27">nʼaj-ɨ-m</ta>
            <ta e="T29" id="Seg_468" s="T28">man</ta>
            <ta e="T30" id="Seg_469" s="T29">nʼaj</ta>
            <ta e="T31" id="Seg_470" s="T30">isklaj-k</ta>
            <ta e="T32" id="Seg_471" s="T31">eː-sɨ-n</ta>
            <ta e="T33" id="Seg_472" s="T32">assɨ</ta>
            <ta e="T34" id="Seg_473" s="T33">nʼuːji</ta>
            <ta e="T35" id="Seg_474" s="T34">man</ta>
            <ta e="T36" id="Seg_475" s="T35">nʼaj-ɨ-m</ta>
            <ta e="T37" id="Seg_476" s="T36">omdɨ-lʼčǝ-ŋ</ta>
            <ta e="T38" id="Seg_477" s="T37">tʼü-lʼ</ta>
            <ta e="T39" id="Seg_478" s="T38">pečʼ-ɨ-ndɨ</ta>
            <ta e="T40" id="Seg_479" s="T39">nʼaj</ta>
            <ta e="T41" id="Seg_480" s="T40">müssɨ-n</ta>
            <ta e="T42" id="Seg_481" s="T41">pečʼ-ɨ-qɨnnɨ</ta>
            <ta e="T43" id="Seg_482" s="T42">sabɨs-mbɨ-ŋ</ta>
            <ta e="T44" id="Seg_483" s="T43">assɨ</ta>
            <ta e="T45" id="Seg_484" s="T44">müssɨ-mbɨ</ta>
            <ta e="T46" id="Seg_485" s="T45">nʼaj</ta>
            <ta e="T47" id="Seg_486" s="T46">taras-sɨ</ta>
            <ta e="T48" id="Seg_487" s="T47">sečas</ta>
            <ta e="T49" id="Seg_488" s="T48">man</ta>
            <ta e="T50" id="Seg_489" s="T49">nʼaj</ta>
            <ta e="T51" id="Seg_490" s="T50">meː-nǯɨ-ntɨ</ta>
            <ta e="T52" id="Seg_491" s="T51">müssɨ-mbɨ</ta>
            <ta e="T53" id="Seg_492" s="T52">nʼaj</ta>
            <ta e="T54" id="Seg_493" s="T53">man</ta>
            <ta e="T55" id="Seg_494" s="T54">sabɨs-ŋɨ-m</ta>
            <ta e="T56" id="Seg_495" s="T55">nʼaj-ɨ-m</ta>
            <ta e="T57" id="Seg_496" s="T56">pečʼ-ɨ-qɨnnɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_497" s="T0">which-ADVZ</ta>
            <ta e="T2" id="Seg_498" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_499" s="T2">bread-EP-ACC</ta>
            <ta e="T4" id="Seg_500" s="T3">do-HAB-CO-1SG.O</ta>
            <ta e="T5" id="Seg_501" s="T4">I.NOM</ta>
            <ta e="T6" id="Seg_502" s="T5">take-HAB-CO-1SG.O</ta>
            <ta e="T7" id="Seg_503" s="T6">flour-ACC</ta>
            <ta e="T8" id="Seg_504" s="T7">shop-LOC</ta>
            <ta e="T9" id="Seg_505" s="T8">flour.[NOM]</ta>
            <ta e="T10" id="Seg_506" s="T9">take-HAB-CO-1SG.O</ta>
            <ta e="T11" id="Seg_507" s="T10">buy-CO-1SG.O</ta>
            <ta e="T12" id="Seg_508" s="T11">buy-CO-1SG.O</ta>
            <ta e="T13" id="Seg_509" s="T12">flour-ACC</ta>
            <ta e="T14" id="Seg_510" s="T13">house-ILL</ta>
            <ta e="T15" id="Seg_511" s="T14">bring-CO-1SG.O</ta>
            <ta e="T16" id="Seg_512" s="T15">now</ta>
            <ta e="T17" id="Seg_513" s="T16">do-INF</ta>
            <ta e="T18" id="Seg_514" s="T17">bread-EP-ACC</ta>
            <ta e="T19" id="Seg_515" s="T18">I.NOM</ta>
            <ta e="T20" id="Seg_516" s="T19">kneading.trough-ACC</ta>
            <ta e="T21" id="Seg_517" s="T20">take-HAB-CO-1SG.O</ta>
            <ta e="T22" id="Seg_518" s="T21">knead.dough-INF</ta>
            <ta e="T23" id="Seg_519" s="T22">bread-EP-ACC</ta>
            <ta e="T24" id="Seg_520" s="T23">flour-ACC</ta>
            <ta e="T25" id="Seg_521" s="T24">sieve-RES-INF</ta>
            <ta e="T26" id="Seg_522" s="T25">morning-ADV.LOC</ta>
            <ta e="T27" id="Seg_523" s="T26">do-FUT-1SG.O</ta>
            <ta e="T28" id="Seg_524" s="T27">bread-EP-ACC</ta>
            <ta e="T29" id="Seg_525" s="T28">I.GEN</ta>
            <ta e="T30" id="Seg_526" s="T29">bread.[NOM]</ta>
            <ta e="T31" id="Seg_527" s="T30">bad-ADVZ</ta>
            <ta e="T32" id="Seg_528" s="T31">be-PST-3SG.S</ta>
            <ta e="T33" id="Seg_529" s="T32">NEG</ta>
            <ta e="T34" id="Seg_530" s="T33">sweet.[NOM]</ta>
            <ta e="T35" id="Seg_531" s="T34">I.NOM</ta>
            <ta e="T36" id="Seg_532" s="T35">bread-EP-ACC</ta>
            <ta e="T37" id="Seg_533" s="T36">sit.down-PFV-1SG.S</ta>
            <ta e="T38" id="Seg_534" s="T37">earth-ADJZ</ta>
            <ta e="T39" id="Seg_535" s="T38">oven-EP-ILL</ta>
            <ta e="T40" id="Seg_536" s="T39">bread.[NOM]</ta>
            <ta e="T41" id="Seg_537" s="T40">keep.up-3SG.S</ta>
            <ta e="T42" id="Seg_538" s="T41">oven-EP-EL</ta>
            <ta e="T43" id="Seg_539" s="T42">pull.out-PST.NAR-1SG.S</ta>
            <ta e="T44" id="Seg_540" s="T43">NEG</ta>
            <ta e="T45" id="Seg_541" s="T44">keep.up-PST.NAR.[3SG.S]</ta>
            <ta e="T46" id="Seg_542" s="T45">bread.[NOM]</ta>
            <ta e="T47" id="Seg_543" s="T46">keep.up-PST.[3SG.S]</ta>
            <ta e="T48" id="Seg_544" s="T47">now</ta>
            <ta e="T49" id="Seg_545" s="T48">I.NOM</ta>
            <ta e="T50" id="Seg_546" s="T49">bread.[NOM]</ta>
            <ta e="T51" id="Seg_547" s="T50">do-FUT-INFER.[3SG.S]</ta>
            <ta e="T52" id="Seg_548" s="T51">keep.up-PST.NAR.[3SG.S]</ta>
            <ta e="T53" id="Seg_549" s="T52">bread.[NOM]</ta>
            <ta e="T54" id="Seg_550" s="T53">I.NOM</ta>
            <ta e="T55" id="Seg_551" s="T54">jump.out-CO-1SG.O</ta>
            <ta e="T56" id="Seg_552" s="T55">bread-EP-ACC</ta>
            <ta e="T57" id="Seg_553" s="T56">oven-EP-EL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_554" s="T0">какой-ADVZ</ta>
            <ta e="T2" id="Seg_555" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_556" s="T2">хлеб-EP-ACC</ta>
            <ta e="T4" id="Seg_557" s="T3">делать-HAB-CO-1SG.O</ta>
            <ta e="T5" id="Seg_558" s="T4">я.NOM</ta>
            <ta e="T6" id="Seg_559" s="T5">взять-HAB-CO-1SG.O</ta>
            <ta e="T7" id="Seg_560" s="T6">мука-ACC</ta>
            <ta e="T8" id="Seg_561" s="T7">магазин-LOC</ta>
            <ta e="T9" id="Seg_562" s="T8">мука.[NOM]</ta>
            <ta e="T10" id="Seg_563" s="T9">взять-HAB-CO-1SG.O</ta>
            <ta e="T11" id="Seg_564" s="T10">купить-CO-1SG.O</ta>
            <ta e="T12" id="Seg_565" s="T11">купить-CO-1SG.O</ta>
            <ta e="T13" id="Seg_566" s="T12">мука-ACC</ta>
            <ta e="T14" id="Seg_567" s="T13">дом-ILL</ta>
            <ta e="T15" id="Seg_568" s="T14">принести-CO-1SG.O</ta>
            <ta e="T16" id="Seg_569" s="T15">сейчас</ta>
            <ta e="T17" id="Seg_570" s="T16">делать-INF</ta>
            <ta e="T18" id="Seg_571" s="T17">хлеб-EP-ACC</ta>
            <ta e="T19" id="Seg_572" s="T18">я.NOM</ta>
            <ta e="T20" id="Seg_573" s="T19">квашонка-ACC</ta>
            <ta e="T21" id="Seg_574" s="T20">взять-HAB-CO-1SG.O</ta>
            <ta e="T22" id="Seg_575" s="T21">замесить.тесто-INF</ta>
            <ta e="T23" id="Seg_576" s="T22">хлеб-EP-ACC</ta>
            <ta e="T24" id="Seg_577" s="T23">мука-ACC</ta>
            <ta e="T25" id="Seg_578" s="T24">просеять-RES-INF</ta>
            <ta e="T26" id="Seg_579" s="T25">утро-ADV.LOC</ta>
            <ta e="T27" id="Seg_580" s="T26">делать-FUT-1SG.O</ta>
            <ta e="T28" id="Seg_581" s="T27">хлеб-EP-ACC</ta>
            <ta e="T29" id="Seg_582" s="T28">я.GEN</ta>
            <ta e="T30" id="Seg_583" s="T29">хлеб.[NOM]</ta>
            <ta e="T31" id="Seg_584" s="T30">плохой-ADVZ</ta>
            <ta e="T32" id="Seg_585" s="T31">быть-PST-3SG.S</ta>
            <ta e="T33" id="Seg_586" s="T32">NEG</ta>
            <ta e="T34" id="Seg_587" s="T33">сладкий.[NOM]</ta>
            <ta e="T35" id="Seg_588" s="T34">я.NOM</ta>
            <ta e="T36" id="Seg_589" s="T35">хлеб-EP-ACC</ta>
            <ta e="T37" id="Seg_590" s="T36">сесть-PFV-1SG.S</ta>
            <ta e="T38" id="Seg_591" s="T37">земля-ADJZ</ta>
            <ta e="T39" id="Seg_592" s="T38">печь-EP-ILL</ta>
            <ta e="T40" id="Seg_593" s="T39">хлеб.[NOM]</ta>
            <ta e="T41" id="Seg_594" s="T40">поспеть-3SG.S</ta>
            <ta e="T42" id="Seg_595" s="T41">печь-EP-EL</ta>
            <ta e="T43" id="Seg_596" s="T42">вытащить-PST.NAR-1SG.S</ta>
            <ta e="T44" id="Seg_597" s="T43">NEG</ta>
            <ta e="T45" id="Seg_598" s="T44">поспеть-PST.NAR.[3SG.S]</ta>
            <ta e="T46" id="Seg_599" s="T45">хлеб.[NOM]</ta>
            <ta e="T47" id="Seg_600" s="T46">поспеть-PST.[3SG.S]</ta>
            <ta e="T48" id="Seg_601" s="T47">сейчас</ta>
            <ta e="T49" id="Seg_602" s="T48">я.NOM</ta>
            <ta e="T50" id="Seg_603" s="T49">хлеб.[NOM]</ta>
            <ta e="T51" id="Seg_604" s="T50">делать-FUT-INFER.[3SG.S]</ta>
            <ta e="T52" id="Seg_605" s="T51">поспеть-PST.NAR.[3SG.S]</ta>
            <ta e="T53" id="Seg_606" s="T52">хлеб.[NOM]</ta>
            <ta e="T54" id="Seg_607" s="T53">я.NOM</ta>
            <ta e="T55" id="Seg_608" s="T54">выскочить-CO-1SG.O</ta>
            <ta e="T56" id="Seg_609" s="T55">хлеб-EP-ACC</ta>
            <ta e="T57" id="Seg_610" s="T56">печь-EP-EL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_611" s="T0">interrog-adj&gt;adv</ta>
            <ta e="T2" id="Seg_612" s="T1">pers</ta>
            <ta e="T3" id="Seg_613" s="T2">n-n:ins-n:case</ta>
            <ta e="T4" id="Seg_614" s="T3">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T5" id="Seg_615" s="T4">pers</ta>
            <ta e="T6" id="Seg_616" s="T5">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T7" id="Seg_617" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_618" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_619" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_620" s="T9">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T11" id="Seg_621" s="T10">v-v:ins-v:pn</ta>
            <ta e="T12" id="Seg_622" s="T11">v-v:ins-v:pn</ta>
            <ta e="T13" id="Seg_623" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_624" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_625" s="T14">v-v:ins-v:pn</ta>
            <ta e="T16" id="Seg_626" s="T15">adv</ta>
            <ta e="T17" id="Seg_627" s="T16">v-v:inf</ta>
            <ta e="T18" id="Seg_628" s="T17">n-n:ins-n:case</ta>
            <ta e="T19" id="Seg_629" s="T18">pers</ta>
            <ta e="T20" id="Seg_630" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_631" s="T20">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T22" id="Seg_632" s="T21">v-v:inf</ta>
            <ta e="T23" id="Seg_633" s="T22">n-n:ins-n:case</ta>
            <ta e="T24" id="Seg_634" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_635" s="T24">v-v&gt;v-v:inf</ta>
            <ta e="T26" id="Seg_636" s="T25">n-adv:case</ta>
            <ta e="T27" id="Seg_637" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_638" s="T27">n-n:ins-n:case</ta>
            <ta e="T29" id="Seg_639" s="T28">pers</ta>
            <ta e="T30" id="Seg_640" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_641" s="T30">adj-adj&gt;adv</ta>
            <ta e="T32" id="Seg_642" s="T31">v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_643" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_644" s="T33">adj-n:case</ta>
            <ta e="T35" id="Seg_645" s="T34">pers</ta>
            <ta e="T36" id="Seg_646" s="T35">n-n:ins-n:case</ta>
            <ta e="T37" id="Seg_647" s="T36">v-v&gt;v-v:pn</ta>
            <ta e="T38" id="Seg_648" s="T37">n-n&gt;adj</ta>
            <ta e="T39" id="Seg_649" s="T38">n-n:ins-n:case</ta>
            <ta e="T40" id="Seg_650" s="T39">n-n:case</ta>
            <ta e="T41" id="Seg_651" s="T40">v-v:pn</ta>
            <ta e="T42" id="Seg_652" s="T41">n-n:ins-n:case</ta>
            <ta e="T43" id="Seg_653" s="T42">v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_654" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_655" s="T44">v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_656" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_657" s="T46">v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_658" s="T47">adv</ta>
            <ta e="T49" id="Seg_659" s="T48">pers</ta>
            <ta e="T50" id="Seg_660" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_661" s="T50">v-v:tense-v:mood-v:pn</ta>
            <ta e="T52" id="Seg_662" s="T51">v-v:tense-v:pn</ta>
            <ta e="T53" id="Seg_663" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_664" s="T53">pers</ta>
            <ta e="T55" id="Seg_665" s="T54">v-v:ins-v:pn</ta>
            <ta e="T56" id="Seg_666" s="T55">n-n:ins-n:case</ta>
            <ta e="T57" id="Seg_667" s="T56">n-n:ins-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_668" s="T0">adv</ta>
            <ta e="T2" id="Seg_669" s="T1">pers</ta>
            <ta e="T3" id="Seg_670" s="T2">n</ta>
            <ta e="T4" id="Seg_671" s="T3">v</ta>
            <ta e="T5" id="Seg_672" s="T4">pers</ta>
            <ta e="T6" id="Seg_673" s="T5">v</ta>
            <ta e="T7" id="Seg_674" s="T6">n</ta>
            <ta e="T8" id="Seg_675" s="T7">n</ta>
            <ta e="T9" id="Seg_676" s="T8">n</ta>
            <ta e="T10" id="Seg_677" s="T9">v</ta>
            <ta e="T11" id="Seg_678" s="T10">v</ta>
            <ta e="T12" id="Seg_679" s="T11">v</ta>
            <ta e="T13" id="Seg_680" s="T12">n</ta>
            <ta e="T14" id="Seg_681" s="T13">n</ta>
            <ta e="T15" id="Seg_682" s="T14">v</ta>
            <ta e="T16" id="Seg_683" s="T15">adv</ta>
            <ta e="T17" id="Seg_684" s="T16">v</ta>
            <ta e="T18" id="Seg_685" s="T17">n</ta>
            <ta e="T19" id="Seg_686" s="T18">pers</ta>
            <ta e="T20" id="Seg_687" s="T19">n</ta>
            <ta e="T21" id="Seg_688" s="T20">v</ta>
            <ta e="T22" id="Seg_689" s="T21">v</ta>
            <ta e="T23" id="Seg_690" s="T22">n</ta>
            <ta e="T24" id="Seg_691" s="T23">n</ta>
            <ta e="T25" id="Seg_692" s="T24">v</ta>
            <ta e="T26" id="Seg_693" s="T25">n</ta>
            <ta e="T27" id="Seg_694" s="T26">v</ta>
            <ta e="T28" id="Seg_695" s="T27">n</ta>
            <ta e="T29" id="Seg_696" s="T28">pers</ta>
            <ta e="T30" id="Seg_697" s="T29">n</ta>
            <ta e="T31" id="Seg_698" s="T30">adv</ta>
            <ta e="T32" id="Seg_699" s="T31">v</ta>
            <ta e="T33" id="Seg_700" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_701" s="T33">adj</ta>
            <ta e="T35" id="Seg_702" s="T34">pers</ta>
            <ta e="T36" id="Seg_703" s="T35">n</ta>
            <ta e="T37" id="Seg_704" s="T36">v</ta>
            <ta e="T38" id="Seg_705" s="T37">adj</ta>
            <ta e="T39" id="Seg_706" s="T38">n</ta>
            <ta e="T40" id="Seg_707" s="T39">v</ta>
            <ta e="T41" id="Seg_708" s="T40">v</ta>
            <ta e="T42" id="Seg_709" s="T41">n</ta>
            <ta e="T43" id="Seg_710" s="T42">v</ta>
            <ta e="T44" id="Seg_711" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_712" s="T44">v</ta>
            <ta e="T46" id="Seg_713" s="T45">n</ta>
            <ta e="T47" id="Seg_714" s="T46">v</ta>
            <ta e="T48" id="Seg_715" s="T47">adv</ta>
            <ta e="T49" id="Seg_716" s="T48">pers</ta>
            <ta e="T50" id="Seg_717" s="T49">n</ta>
            <ta e="T51" id="Seg_718" s="T50">v</ta>
            <ta e="T52" id="Seg_719" s="T51">v</ta>
            <ta e="T53" id="Seg_720" s="T52">n</ta>
            <ta e="T54" id="Seg_721" s="T53">pers</ta>
            <ta e="T55" id="Seg_722" s="T54">v</ta>
            <ta e="T56" id="Seg_723" s="T55">n</ta>
            <ta e="T57" id="Seg_724" s="T56">n</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_725" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_726" s="T2">np:O</ta>
            <ta e="T4" id="Seg_727" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_728" s="T4">pro.h:S</ta>
            <ta e="T6" id="Seg_729" s="T5">v:pred</ta>
            <ta e="T7" id="Seg_730" s="T6">np:O</ta>
            <ta e="T9" id="Seg_731" s="T8">np:O</ta>
            <ta e="T10" id="Seg_732" s="T9">0.1.h:S v:pred</ta>
            <ta e="T11" id="Seg_733" s="T10">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T12" id="Seg_734" s="T11">0.1.h:S v:pred</ta>
            <ta e="T13" id="Seg_735" s="T12">np:O</ta>
            <ta e="T15" id="Seg_736" s="T14">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T18" id="Seg_737" s="T15">s:purp</ta>
            <ta e="T19" id="Seg_738" s="T18">pro.h:S</ta>
            <ta e="T20" id="Seg_739" s="T19">np:O</ta>
            <ta e="T21" id="Seg_740" s="T20">v:pred</ta>
            <ta e="T23" id="Seg_741" s="T21">s:purp</ta>
            <ta e="T25" id="Seg_742" s="T23">s:purp</ta>
            <ta e="T27" id="Seg_743" s="T26">0.1.h:S v:pred</ta>
            <ta e="T28" id="Seg_744" s="T27">np:O</ta>
            <ta e="T30" id="Seg_745" s="T29">np:S</ta>
            <ta e="T32" id="Seg_746" s="T31">v:pred</ta>
            <ta e="T34" id="Seg_747" s="T33">0.3:S adj:pred</ta>
            <ta e="T35" id="Seg_748" s="T34">pro.h:S</ta>
            <ta e="T36" id="Seg_749" s="T35">np:O</ta>
            <ta e="T37" id="Seg_750" s="T36">v:pred</ta>
            <ta e="T40" id="Seg_751" s="T39">np:S</ta>
            <ta e="T41" id="Seg_752" s="T40">v:pred</ta>
            <ta e="T43" id="Seg_753" s="T42">0.1.h:S v:pred</ta>
            <ta e="T45" id="Seg_754" s="T44">0.3:S v:pred</ta>
            <ta e="T46" id="Seg_755" s="T45">np:S</ta>
            <ta e="T47" id="Seg_756" s="T46">v:pred</ta>
            <ta e="T49" id="Seg_757" s="T48">pro.h:S</ta>
            <ta e="T50" id="Seg_758" s="T49">np:O</ta>
            <ta e="T51" id="Seg_759" s="T50">v:pred</ta>
            <ta e="T52" id="Seg_760" s="T51">v:pred</ta>
            <ta e="T53" id="Seg_761" s="T52">np:S</ta>
            <ta e="T54" id="Seg_762" s="T53">pro.h:S</ta>
            <ta e="T55" id="Seg_763" s="T54">v:pred</ta>
            <ta e="T56" id="Seg_764" s="T55">np:O</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_765" s="T1">pro.h:A</ta>
            <ta e="T3" id="Seg_766" s="T2">np:P</ta>
            <ta e="T5" id="Seg_767" s="T4">pro.h:A</ta>
            <ta e="T7" id="Seg_768" s="T6">np:Th</ta>
            <ta e="T8" id="Seg_769" s="T7">np:L</ta>
            <ta e="T9" id="Seg_770" s="T8">np:Th</ta>
            <ta e="T10" id="Seg_771" s="T9">0.1.h:A</ta>
            <ta e="T11" id="Seg_772" s="T10">0.1.h:A 0.3:Th</ta>
            <ta e="T12" id="Seg_773" s="T11">0.1.h:A</ta>
            <ta e="T13" id="Seg_774" s="T12">np:Th</ta>
            <ta e="T14" id="Seg_775" s="T13">np:G</ta>
            <ta e="T15" id="Seg_776" s="T14">0.1.h:A 0.3:Th</ta>
            <ta e="T16" id="Seg_777" s="T15">adv:Time</ta>
            <ta e="T18" id="Seg_778" s="T17">np:P</ta>
            <ta e="T19" id="Seg_779" s="T18">pro.h:A</ta>
            <ta e="T20" id="Seg_780" s="T19">np:Th</ta>
            <ta e="T23" id="Seg_781" s="T22">np:P</ta>
            <ta e="T24" id="Seg_782" s="T23">np:Th</ta>
            <ta e="T26" id="Seg_783" s="T25">adv:Time</ta>
            <ta e="T27" id="Seg_784" s="T26">0.1.h:A</ta>
            <ta e="T28" id="Seg_785" s="T27">np:P</ta>
            <ta e="T29" id="Seg_786" s="T28">pro.h:Poss</ta>
            <ta e="T30" id="Seg_787" s="T29">np:Th</ta>
            <ta e="T34" id="Seg_788" s="T33">0.3:Th</ta>
            <ta e="T35" id="Seg_789" s="T34">pro.h:A</ta>
            <ta e="T36" id="Seg_790" s="T35">np:Th</ta>
            <ta e="T39" id="Seg_791" s="T38">np:G</ta>
            <ta e="T40" id="Seg_792" s="T39">np:Th</ta>
            <ta e="T42" id="Seg_793" s="T41">np:So</ta>
            <ta e="T43" id="Seg_794" s="T42">0.1.h:A</ta>
            <ta e="T45" id="Seg_795" s="T44">0.3:Th</ta>
            <ta e="T46" id="Seg_796" s="T45">np:Th</ta>
            <ta e="T48" id="Seg_797" s="T47">adv:Time</ta>
            <ta e="T49" id="Seg_798" s="T48">pro.h:A</ta>
            <ta e="T50" id="Seg_799" s="T49">np:P</ta>
            <ta e="T53" id="Seg_800" s="T52">np:Th</ta>
            <ta e="T54" id="Seg_801" s="T53">pro.h:A</ta>
            <ta e="T56" id="Seg_802" s="T55">np:P</ta>
            <ta e="T57" id="Seg_803" s="T56">np:So</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T7" id="Seg_804" s="T6">RUS:cult</ta>
            <ta e="T8" id="Seg_805" s="T7">RUS:cult</ta>
            <ta e="T9" id="Seg_806" s="T8">RUS:cult</ta>
            <ta e="T13" id="Seg_807" s="T12">RUS:cult</ta>
            <ta e="T16" id="Seg_808" s="T15">RUS:cult</ta>
            <ta e="T20" id="Seg_809" s="T19">RUS:cult</ta>
            <ta e="T24" id="Seg_810" s="T23">RUS:cult</ta>
            <ta e="T39" id="Seg_811" s="T38">RUS:cult</ta>
            <ta e="T42" id="Seg_812" s="T41">RUS:cult</ta>
            <ta e="T48" id="Seg_813" s="T47">RUS:cult</ta>
            <ta e="T57" id="Seg_814" s="T56">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_815" s="T0">Как я делаю хлеб.</ta>
            <ta e="T11" id="Seg_816" s="T4">Я беру муку, в магазине муку беру, покупаю.</ta>
            <ta e="T18" id="Seg_817" s="T11">Покупаю муку, домой приношу, (чтобы) сейчас делать хлеб.</ta>
            <ta e="T25" id="Seg_818" s="T18">Возьму квашонку, чтобы замесить тесто, хлеб, муку просеять.</ta>
            <ta e="T28" id="Seg_819" s="T25">Завтра сделаю хлеб.</ta>
            <ta e="T34" id="Seg_820" s="T28">Мой хлеб был плохой, не сладкий.</ta>
            <ta e="T39" id="Seg_821" s="T34">Я хлеб поставила в русскую печь.</ta>
            <ta e="T41" id="Seg_822" s="T39">Хлеб поспел.</ta>
            <ta e="T43" id="Seg_823" s="T41">Из печки вытащила.</ta>
            <ta e="T47" id="Seg_824" s="T43">Он не поспел, хлеб не поспел.</ta>
            <ta e="T51" id="Seg_825" s="T47">Сейчас я хлеб сделаю.</ta>
            <ta e="T53" id="Seg_826" s="T51">Хлеб поспел.</ta>
            <ta e="T57" id="Seg_827" s="T53">Я вытащила хлеб из печки.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_828" s="T0">How I make bread.</ta>
            <ta e="T11" id="Seg_829" s="T4">I take flour, I take flour in a shop, I buy it.</ta>
            <ta e="T18" id="Seg_830" s="T11">I buy flour, I bring it home to make bread.</ta>
            <ta e="T25" id="Seg_831" s="T18">I take kneading trough to knead dough, (to make) bread, to sift flour.</ta>
            <ta e="T28" id="Seg_832" s="T25">Tomorrow I'll make bread.</ta>
            <ta e="T34" id="Seg_833" s="T28">My bread was bad, it wasn't sweet.</ta>
            <ta e="T39" id="Seg_834" s="T34">I put the bread into the oven.</ta>
            <ta e="T41" id="Seg_835" s="T39">The bread was ready.</ta>
            <ta e="T43" id="Seg_836" s="T41">I took it from the oven.</ta>
            <ta e="T47" id="Seg_837" s="T43">It wasn't ready, the bread wasn't ready.</ta>
            <ta e="T51" id="Seg_838" s="T47">Now I'll make bread.</ta>
            <ta e="T53" id="Seg_839" s="T51">The bread was ready.</ta>
            <ta e="T57" id="Seg_840" s="T53">I took the bread out of the oven.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_841" s="T0">Wie ich Brot mache.</ta>
            <ta e="T11" id="Seg_842" s="T4">Ich nehme Mehl, ich nehme Mehl in einem Laden, ich kaufe es.</ta>
            <ta e="T18" id="Seg_843" s="T11">Ich kaufe Mehl, ich bringe es nach Hause um Brot zu machen.</ta>
            <ta e="T25" id="Seg_844" s="T18">Ich nehme einen Knettrog um Teig zu kneten, um Brot zu machen, um Mehl zu sieben.</ta>
            <ta e="T28" id="Seg_845" s="T25">Morgen werde ich Brot machen.</ta>
            <ta e="T34" id="Seg_846" s="T28">Mein Brot war schlecht, es war nicht süß.</ta>
            <ta e="T39" id="Seg_847" s="T34">Ich schob das Brot in den Ofen.</ta>
            <ta e="T41" id="Seg_848" s="T39">Das Brot war fertig.</ta>
            <ta e="T43" id="Seg_849" s="T41">Ich holte es aus dem Ofen.</ta>
            <ta e="T47" id="Seg_850" s="T43">Es war nicht fertig, das Brot war nicht fertig.</ta>
            <ta e="T51" id="Seg_851" s="T47">Jetzt mache ich Brot.</ta>
            <ta e="T53" id="Seg_852" s="T51">Das Brot war fertig.</ta>
            <ta e="T57" id="Seg_853" s="T53">Ich holte das Brot aus dem Ofen.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_854" s="T0">как я делаю хлеб</ta>
            <ta e="T11" id="Seg_855" s="T4">я беру муку в магазине муку беру покупаю</ta>
            <ta e="T18" id="Seg_856" s="T11">покупаю муку домой приношу сейчас делать хлеб</ta>
            <ta e="T25" id="Seg_857" s="T18">растворю (замесить) хлеба муки просеить</ta>
            <ta e="T28" id="Seg_858" s="T25">завтра сделаю хлеб</ta>
            <ta e="T34" id="Seg_859" s="T28">хлеб плохой был не сладкий</ta>
            <ta e="T39" id="Seg_860" s="T34">я хлеб посадила в русскую печь</ta>
            <ta e="T41" id="Seg_861" s="T39">хлеб поспел</ta>
            <ta e="T43" id="Seg_862" s="T41">из печки вытащила</ta>
            <ta e="T47" id="Seg_863" s="T43">еще не поспел хлеб</ta>
            <ta e="T51" id="Seg_864" s="T47">сейчас я хлеб сделаю</ta>
            <ta e="T53" id="Seg_865" s="T51">поспел хлеб</ta>
            <ta e="T57" id="Seg_866" s="T53">я вытащила хлеб из печки</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
