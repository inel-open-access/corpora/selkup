<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KMG_1976_BriefVacation_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KMG_1976_BriefVacation_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">299</ud-information>
            <ud-information attribute-name="# HIAT:w">218</ud-information>
            <ud-information attribute-name="# e">218</ud-information>
            <ud-information attribute-name="# HIAT:u">35</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KMG">
            <abbreviation>KMG</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KMG"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T218" id="Seg_0" n="sc" s="T0">
               <ts e="T2" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Korotkij</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">otpusk</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3" id="Seg_11" n="HIAT:u" s="T2">
                  <ts e="T3" id="Seg_13" n="HIAT:w" s="T2">Filʼm</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_17" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_19" n="HIAT:w" s="T3">Italʼaqɨt</ts>
                  <nts id="Seg_20" n="HIAT:ip">,</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_23" n="HIAT:w" s="T4">mɨnɨl</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_26" n="HIAT:w" s="T5">ɔːmtɨlʼ</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_29" n="HIAT:w" s="T6">qoːt</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_32" n="HIAT:w" s="T7">tättoːqɨt</ts>
                  <nts id="Seg_33" n="HIAT:ip">.</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_36" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_38" n="HIAT:w" s="T8">Iːma</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_41" n="HIAT:w" s="T9">ilimpa</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_44" n="HIAT:w" s="T10">irasɨmɨlʼ</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_48" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_50" n="HIAT:w" s="T11">Iːman</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_53" n="HIAT:w" s="T12">mɨqɨt</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_56" n="HIAT:w" s="T13">nɔːkɨr</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_59" n="HIAT:w" s="T14">kɨplʼa</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_62" n="HIAT:w" s="T15">ijaiːtɨ</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">ɛːsa</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_69" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_71" n="HIAT:w" s="T17">Na</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_74" n="HIAT:w" s="T18">iːman</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_77" n="HIAT:w" s="T19">ira</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">kos</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">kunɨ</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">uːčʼimpa</ts>
                  <nts id="Seg_87" n="HIAT:ip">,</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_90" n="HIAT:w" s="T23">topɨtɨ</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_93" n="HIAT:w" s="T24">səpɨtɛːmpatɨ</ts>
                  <nts id="Seg_94" n="HIAT:ip">.</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_97" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_99" n="HIAT:w" s="T25">Topɨtɨ</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_102" n="HIAT:w" s="T26">čʼüšelʼä</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_105" n="HIAT:w" s="T27">aša</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_108" n="HIAT:w" s="T28">uːčʼimpa</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_111" n="HIAT:w" s="T29">poːntɨ</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_114" n="HIAT:w" s="T30">käš</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_117" n="HIAT:w" s="T31">kuntɨ</ts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_121" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_123" n="HIAT:w" s="T32">Iːmatɨ</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_126" n="HIAT:w" s="T33">ottɨqɨntɨ</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_129" n="HIAT:w" s="T34">uːčʼimpa</ts>
                  <nts id="Seg_130" n="HIAT:ip">.</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_133" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_135" n="HIAT:w" s="T35">A</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_138" n="HIAT:w" s="T36">irantɨ</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_141" n="HIAT:w" s="T37">tʼimnʼa</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_144" n="HIAT:w" s="T38">iŋkalpɨlʼä</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_147" n="HIAT:w" s="T39">mɔːtqɨn</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_150" n="HIAT:w" s="T40">meːlqo</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_153" n="HIAT:w" s="T41">ɔːmnɨmpa</ts>
                  <nts id="Seg_154" n="HIAT:ip">.</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_157" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_159" n="HIAT:w" s="T42">Ni</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_162" n="HIAT:w" s="T43">kunɨ</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_164" n="HIAT:ip">(</nts>
                  <nts id="Seg_165" n="HIAT:ip">/</nts>
                  <ts e="T45" id="Seg_167" n="HIAT:w" s="T44">ni</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_170" n="HIAT:w" s="T45">kut</ts>
                  <nts id="Seg_171" n="HIAT:ip">)</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_174" n="HIAT:w" s="T46">uːčʼiqo</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_177" n="HIAT:w" s="T47">aša</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_180" n="HIAT:w" s="T48">kɨkɨlʼä</ts>
                  <nts id="Seg_181" n="HIAT:ip">.</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_184" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_186" n="HIAT:w" s="T49">Iːma</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_189" n="HIAT:w" s="T50">ontɨ</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_192" n="HIAT:w" s="T51">uːčʼimpa</ts>
                  <nts id="Seg_193" n="HIAT:ip">.</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_196" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_198" n="HIAT:w" s="T52">Täpɨn</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_201" n="HIAT:w" s="T53">ilimpɔːtɨt</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_204" n="HIAT:w" s="T54">qaj</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_207" n="HIAT:w" s="T55">muktɨt</ts>
                  <nts id="Seg_208" n="HIAT:ip">,</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_211" n="HIAT:w" s="T56">qaj</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_214" n="HIAT:w" s="T57">seːlʼčʼi</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_217" n="HIAT:w" s="T58">qup</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_219" n="HIAT:ip">(</nts>
                  <nts id="Seg_220" n="HIAT:ip">/</nts>
                  <ts e="T60" id="Seg_222" n="HIAT:w" s="T59">muktut</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_225" n="HIAT:w" s="T60">qup</ts>
                  <nts id="Seg_226" n="HIAT:ip">)</nts>
                  <nts id="Seg_227" n="HIAT:ip">.</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_230" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_232" n="HIAT:w" s="T61">Iːma</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_235" n="HIAT:w" s="T62">ottəqən</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_238" n="HIAT:w" s="T63">uːčʼimpa</ts>
                  <nts id="Seg_239" n="HIAT:ip">.</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_242" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_244" n="HIAT:w" s="T64">Meːlqo</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_247" n="HIAT:w" s="T65">amnat</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_250" n="HIAT:w" s="T66">ɔːmnɨmpɔːtɨt</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_252" n="HIAT:ip">(</nts>
                  <nts id="Seg_253" n="HIAT:ip">/</nts>
                  <ts e="T68" id="Seg_255" n="HIAT:w" s="T67">ɔːmtɔːtɨt</ts>
                  <nts id="Seg_256" n="HIAT:ip">)</nts>
                  <nts id="Seg_257" n="HIAT:ip">.</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_260" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_262" n="HIAT:w" s="T68">Qomtätɨt</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_265" n="HIAT:w" s="T69">čʼäːŋkɨmpa</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_268" n="HIAT:w" s="T70">apsɨp</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_271" n="HIAT:w" s="T71">iːpsa</ts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_275" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_277" n="HIAT:w" s="T72">Nɨːnɨ</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_280" n="HIAT:w" s="T73">iːma</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_283" n="HIAT:w" s="T74">qüːtälʼimpa</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_285" n="HIAT:ip">(</nts>
                  <nts id="Seg_286" n="HIAT:ip">/</nts>
                  <ts e="T76" id="Seg_288" n="HIAT:w" s="T75">to</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_291" n="HIAT:w" s="T76">qüːtälʼimpa</ts>
                  <nts id="Seg_292" n="HIAT:ip">)</nts>
                  <nts id="Seg_293" n="HIAT:ip">.</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_296" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_298" n="HIAT:w" s="T77">Bolʼnicantɨ</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_301" n="HIAT:w" s="T78">pinpɔːtɨt</ts>
                  <nts id="Seg_302" n="HIAT:ip">.</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_305" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_307" n="HIAT:w" s="T79">Täp</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_310" n="HIAT:w" s="T80">palʼnicaqɨt</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_313" n="HIAT:w" s="T81">qaj</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_316" n="HIAT:w" s="T82">iräntɨ</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_319" n="HIAT:w" s="T83">kuntɨ</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_322" n="HIAT:w" s="T84">patpa</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_324" n="HIAT:ip">(</nts>
                  <nts id="Seg_325" n="HIAT:ip">/</nts>
                  <ts e="T86" id="Seg_327" n="HIAT:w" s="T85">ipɨmpa</ts>
                  <nts id="Seg_328" n="HIAT:ip">)</nts>
                  <nts id="Seg_329" n="HIAT:ip">,</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_332" n="HIAT:w" s="T86">qaj</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_335" n="HIAT:w" s="T87">iräntɨ</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_338" n="HIAT:w" s="T88">kuntɨ</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_341" n="HIAT:w" s="T89">ippɨmpa</ts>
                  <nts id="Seg_342" n="HIAT:ip">,</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_345" n="HIAT:w" s="T90">qaj</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_348" n="HIAT:w" s="T91">šitɨ</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_351" n="HIAT:w" s="T92">iräntɨ</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_354" n="HIAT:w" s="T93">kuntɨ</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_357" n="HIAT:w" s="T94">ippɨmpa</ts>
                  <nts id="Seg_358" n="HIAT:ip">.</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_361" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_363" n="HIAT:w" s="T95">Puːqa</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_366" n="HIAT:w" s="T96">šünčʼitɨ</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_369" n="HIAT:w" s="T97">čʼüšəqolampa</ts>
                  <nts id="Seg_370" n="HIAT:ip">,</nts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_373" n="HIAT:w" s="T98">na</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_376" n="HIAT:w" s="T99">iːma</ts>
                  <nts id="Seg_377" n="HIAT:ip">.</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_380" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_382" n="HIAT:w" s="T100">Polʼnicantɨ</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_385" n="HIAT:w" s="T101">türpɔːtɨt</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_388" n="HIAT:w" s="T102">irantɨ</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_391" n="HIAT:w" s="T103">äma</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_394" n="HIAT:w" s="T104">irantɨ</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_397" n="HIAT:w" s="T105">timnʼantɨn</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_400" n="HIAT:w" s="T106">optısa</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_403" n="HIAT:w" s="T107">aj</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_406" n="HIAT:w" s="T108">nɔːkur</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_409" n="HIAT:w" s="T109">iːjalʼatä</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_411" n="HIAT:ip">(</nts>
                  <ts e="T111" id="Seg_413" n="HIAT:w" s="T110">kɨplʼa</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_416" n="HIAT:w" s="T111">ijantɨsa</ts>
                  <nts id="Seg_417" n="HIAT:ip">)</nts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_420" n="HIAT:w" s="T112">moqonä</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_423" n="HIAT:w" s="T113">qärqo</ts>
                  <nts id="Seg_424" n="HIAT:ip">.</nts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_427" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_429" n="HIAT:w" s="T114">Iːma</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_432" n="HIAT:w" s="T115">nılʼčʼik</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_435" n="HIAT:w" s="T116">tomnätɨ</ts>
                  <nts id="Seg_436" n="HIAT:ip">:</nts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_439" n="HIAT:w" s="T117">Mat</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_442" n="HIAT:w" s="T118">tarɨ</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_445" n="HIAT:w" s="T119">qüːtak</ts>
                  <nts id="Seg_446" n="HIAT:ip">.</nts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_449" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_451" n="HIAT:w" s="T120">Puːqa</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_454" n="HIAT:w" s="T121">šünčʼon</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_457" n="HIAT:w" s="T122">tarɨ</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_460" n="HIAT:w" s="T123">čʼüsa</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_462" n="HIAT:ip">(</nts>
                  <nts id="Seg_463" n="HIAT:ip">/</nts>
                  <ts e="T125" id="Seg_465" n="HIAT:w" s="T124">čʼüša</ts>
                  <nts id="Seg_466" n="HIAT:ip">)</nts>
                  <nts id="Seg_467" n="HIAT:ip">,</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_470" n="HIAT:w" s="T125">čʼeːl</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_473" n="HIAT:w" s="T126">moqona</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_476" n="HIAT:w" s="T127">aša</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_479" n="HIAT:w" s="T128">qättak</ts>
                  <nts id="Seg_480" n="HIAT:ip">.</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_483" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_485" n="HIAT:w" s="T129">Nɨːnä</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_488" n="HIAT:w" s="T130">na</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_491" n="HIAT:w" s="T131">iːma</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_494" n="HIAT:w" s="T132">balʼnicaqɨt</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_497" n="HIAT:w" s="T133">imatɨtki</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_500" n="HIAT:w" s="T134">tantɛːsa</ts>
                  <nts id="Seg_501" n="HIAT:ip">.</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_504" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_506" n="HIAT:w" s="T135">Somak</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_509" n="HIAT:w" s="T136">iləqolapsa</ts>
                  <nts id="Seg_510" n="HIAT:ip">.</nts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_513" n="HIAT:u" s="T137">
                  <ts e="T138" id="Seg_515" n="HIAT:w" s="T137">Iːmat</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_518" n="HIAT:w" s="T138">täpɨtkinɨk</ts>
                  <nts id="Seg_519" n="HIAT:ip">,</nts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_522" n="HIAT:w" s="T139">na</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_525" n="HIAT:w" s="T140">iːmatki</ts>
                  <nts id="Seg_526" n="HIAT:ip">,</nts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_529" n="HIAT:w" s="T141">miqulʼölʼčʼimpuqolopsɔːtɨt</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_531" n="HIAT:ip">(</nts>
                  <nts id="Seg_532" n="HIAT:ip">/</nts>
                  <ts e="T143" id="Seg_534" n="HIAT:w" s="T142">mikkolʼčʼimpuqolapsɔːtɨt</ts>
                  <nts id="Seg_535" n="HIAT:ip">)</nts>
                  <nts id="Seg_536" n="HIAT:ip">,</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_539" n="HIAT:w" s="T143">na</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_542" n="HIAT:w" s="T144">imatkinɨk</ts>
                  <nts id="Seg_543" n="HIAT:ip">,</nts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_546" n="HIAT:w" s="T145">toːnna</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_549" n="HIAT:w" s="T146">iːmat</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_552" n="HIAT:w" s="T147">na</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_555" n="HIAT:w" s="T148">iːmatki</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_558" n="HIAT:w" s="T149">mikkolʼolʼčʼimpuqolopsɔːtɨt</ts>
                  <nts id="Seg_559" n="HIAT:ip">:</nts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_562" n="HIAT:w" s="T150">qalʼporqo</ts>
                  <nts id="Seg_563" n="HIAT:ip">,</nts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_566" n="HIAT:w" s="T151">porqɨ</ts>
                  <nts id="Seg_567" n="HIAT:ip">,</nts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_570" n="HIAT:w" s="T152">ükɨ</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_573" n="HIAT:w" s="T153">kušakwaj</ts>
                  <nts id="Seg_574" n="HIAT:ip">.</nts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_577" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_579" n="HIAT:w" s="T154">Nɨːnɨ</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_582" n="HIAT:w" s="T155">na</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_585" n="HIAT:w" s="T156">iːma</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_588" n="HIAT:w" s="T157">polʼnicaqɨt</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_591" n="HIAT:w" s="T158">aša</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_594" n="HIAT:w" s="T159">kunt</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_597" n="HIAT:w" s="T160">ɛːsa</ts>
                  <nts id="Seg_598" n="HIAT:ip">.</nts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_601" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_603" n="HIAT:w" s="T161">Moqonɨ</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_606" n="HIAT:w" s="T162">üːtsɔːt</ts>
                  <nts id="Seg_607" n="HIAT:ip">.</nts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_610" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_612" n="HIAT:w" s="T163">Ukoːt</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_615" n="HIAT:w" s="T164">na</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_618" n="HIAT:w" s="T165">iːma</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_621" n="HIAT:w" s="T166">ni</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_624" n="HIAT:w" s="T167">kučʼ</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_627" n="HIAT:w" s="T168">aša</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_630" n="HIAT:w" s="T169">qalʼtresa</ts>
                  <nts id="Seg_631" n="HIAT:ip">.</nts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_634" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_636" n="HIAT:w" s="T170">Puːt</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_639" n="HIAT:w" s="T171">somalʼak</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_642" n="HIAT:w" s="T172">ɛselʼa</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_645" n="HIAT:w" s="T173">qälʼtroqolopsa</ts>
                  <nts id="Seg_646" n="HIAT:ip">.</nts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_649" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_651" n="HIAT:w" s="T174">Ponä</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_654" n="HIAT:w" s="T175">paktɨlʼä</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_657" n="HIAT:w" s="T176">kučʼä</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_660" n="HIAT:w" s="T177">popal</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_663" n="HIAT:w" s="T178">kuralka</ts>
                  <nts id="Seg_664" n="HIAT:ip">.</nts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_667" n="HIAT:u" s="T179">
                  <ts e="T180" id="Seg_669" n="HIAT:w" s="T179">Nɨːnɨ</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_672" n="HIAT:w" s="T180">toktɨr</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_675" n="HIAT:w" s="T181">ira</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_678" n="HIAT:w" s="T182">nılʼčʼik</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_681" n="HIAT:w" s="T183">kätsɨtɨ</ts>
                  <nts id="Seg_682" n="HIAT:ip">:</nts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_685" n="HIAT:w" s="T184">Kusa</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_688" n="HIAT:w" s="T185">kuš</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_691" n="HIAT:w" s="T186">na</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_694" n="HIAT:w" s="T187">čʼeːlʼe</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_697" n="HIAT:w" s="T188">ɛːmä</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_699" n="HIAT:ip">(</nts>
                  <nts id="Seg_700" n="HIAT:ip">/</nts>
                  <ts e="T190" id="Seg_702" n="HIAT:w" s="T189">kuš</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_705" n="HIAT:w" s="T190">täːl</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_708" n="HIAT:w" s="T191">čʼeːlɨ</ts>
                  <nts id="Seg_709" n="HIAT:ip">,</nts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_712" n="HIAT:w" s="T192">kos</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_715" n="HIAT:w" s="T193">tap</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_718" n="HIAT:w" s="T194">čʼeːlɨ</ts>
                  <nts id="Seg_719" n="HIAT:ip">)</nts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_722" n="HIAT:w" s="T195">moqonä</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_725" n="HIAT:w" s="T196">šıntɨ</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_728" n="HIAT:w" s="T197">üːtɛntɨmɨt</ts>
                  <nts id="Seg_729" n="HIAT:ip">.</nts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T202" id="Seg_732" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_734" n="HIAT:w" s="T198">Tat</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_737" n="HIAT:w" s="T199">tiː</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_740" n="HIAT:w" s="T200">somalʼak</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_743" n="HIAT:w" s="T201">ɛːŋantɨ</ts>
                  <nts id="Seg_744" n="HIAT:ip">.</nts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T211" id="Seg_747" n="HIAT:u" s="T202">
                  <ts e="T203" id="Seg_749" n="HIAT:w" s="T202">Moqonä</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_752" n="HIAT:w" s="T203">qänmal</ts>
                  <nts id="Seg_753" n="HIAT:ip">,</nts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_756" n="HIAT:w" s="T204">mɔːtqɨn</ts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_759" n="HIAT:w" s="T205">ilʼäšʼik</ts>
                  <nts id="Seg_760" n="HIAT:ip">,</nts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_763" n="HIAT:w" s="T206">köt</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_766" n="HIAT:w" s="T207">qaj</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_769" n="HIAT:w" s="T208">čʼeːlʼ</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_772" n="HIAT:w" s="T209">mɔːtqɨn</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_775" n="HIAT:w" s="T210">ilʼäšik</ts>
                  <nts id="Seg_776" n="HIAT:ip">.</nts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_779" n="HIAT:u" s="T211">
                  <ts e="T212" id="Seg_781" n="HIAT:w" s="T211">Uːčʼila</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_784" n="HIAT:w" s="T212">ɨkɨ</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_787" n="HIAT:w" s="T213">qänaš</ts>
                  <nts id="Seg_788" n="HIAT:ip">.</nts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_791" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_793" n="HIAT:w" s="T214">Moqonä</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_796" n="HIAT:w" s="T215">qänmantɨ</ts>
                  <nts id="Seg_797" n="HIAT:ip">,</nts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_800" n="HIAT:w" s="T216">mɔːtqɨn</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_803" n="HIAT:w" s="T217">ɔːmtäšʼik</ts>
                  <nts id="Seg_804" n="HIAT:ip">.</nts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T218" id="Seg_806" n="sc" s="T0">
               <ts e="T1" id="Seg_808" n="e" s="T0">Korotkij </ts>
               <ts e="T2" id="Seg_810" n="e" s="T1">otpusk. </ts>
               <ts e="T3" id="Seg_812" n="e" s="T2">Filʼm. </ts>
               <ts e="T4" id="Seg_814" n="e" s="T3">Italʼaqɨt, </ts>
               <ts e="T5" id="Seg_816" n="e" s="T4">mɨnɨl </ts>
               <ts e="T6" id="Seg_818" n="e" s="T5">ɔːmtɨlʼ </ts>
               <ts e="T7" id="Seg_820" n="e" s="T6">qoːt </ts>
               <ts e="T8" id="Seg_822" n="e" s="T7">tättoːqɨt. </ts>
               <ts e="T9" id="Seg_824" n="e" s="T8">Iːma </ts>
               <ts e="T10" id="Seg_826" n="e" s="T9">ilimpa </ts>
               <ts e="T11" id="Seg_828" n="e" s="T10">irasɨmɨlʼ. </ts>
               <ts e="T12" id="Seg_830" n="e" s="T11">Iːman </ts>
               <ts e="T13" id="Seg_832" n="e" s="T12">mɨqɨt </ts>
               <ts e="T14" id="Seg_834" n="e" s="T13">nɔːkɨr </ts>
               <ts e="T15" id="Seg_836" n="e" s="T14">kɨplʼa </ts>
               <ts e="T16" id="Seg_838" n="e" s="T15">ijaiːtɨ </ts>
               <ts e="T17" id="Seg_840" n="e" s="T16">ɛːsa. </ts>
               <ts e="T18" id="Seg_842" n="e" s="T17">Na </ts>
               <ts e="T19" id="Seg_844" n="e" s="T18">iːman </ts>
               <ts e="T20" id="Seg_846" n="e" s="T19">ira </ts>
               <ts e="T21" id="Seg_848" n="e" s="T20">kos </ts>
               <ts e="T22" id="Seg_850" n="e" s="T21">kunɨ </ts>
               <ts e="T23" id="Seg_852" n="e" s="T22">uːčʼimpa, </ts>
               <ts e="T24" id="Seg_854" n="e" s="T23">topɨtɨ </ts>
               <ts e="T25" id="Seg_856" n="e" s="T24">səpɨtɛːmpatɨ. </ts>
               <ts e="T26" id="Seg_858" n="e" s="T25">Topɨtɨ </ts>
               <ts e="T27" id="Seg_860" n="e" s="T26">čʼüšelʼä </ts>
               <ts e="T28" id="Seg_862" n="e" s="T27">aša </ts>
               <ts e="T29" id="Seg_864" n="e" s="T28">uːčʼimpa </ts>
               <ts e="T30" id="Seg_866" n="e" s="T29">poːntɨ </ts>
               <ts e="T31" id="Seg_868" n="e" s="T30">käš </ts>
               <ts e="T32" id="Seg_870" n="e" s="T31">kuntɨ. </ts>
               <ts e="T33" id="Seg_872" n="e" s="T32">Iːmatɨ </ts>
               <ts e="T34" id="Seg_874" n="e" s="T33">ottɨqɨntɨ </ts>
               <ts e="T35" id="Seg_876" n="e" s="T34">uːčʼimpa. </ts>
               <ts e="T36" id="Seg_878" n="e" s="T35">A </ts>
               <ts e="T37" id="Seg_880" n="e" s="T36">irantɨ </ts>
               <ts e="T38" id="Seg_882" n="e" s="T37">tʼimnʼa </ts>
               <ts e="T39" id="Seg_884" n="e" s="T38">iŋkalpɨlʼä </ts>
               <ts e="T40" id="Seg_886" n="e" s="T39">mɔːtqɨn </ts>
               <ts e="T41" id="Seg_888" n="e" s="T40">meːlqo </ts>
               <ts e="T42" id="Seg_890" n="e" s="T41">ɔːmnɨmpa. </ts>
               <ts e="T43" id="Seg_892" n="e" s="T42">Ni </ts>
               <ts e="T44" id="Seg_894" n="e" s="T43">kunɨ </ts>
               <ts e="T45" id="Seg_896" n="e" s="T44">(/ni </ts>
               <ts e="T46" id="Seg_898" n="e" s="T45">kut) </ts>
               <ts e="T47" id="Seg_900" n="e" s="T46">uːčʼiqo </ts>
               <ts e="T48" id="Seg_902" n="e" s="T47">aša </ts>
               <ts e="T49" id="Seg_904" n="e" s="T48">kɨkɨlʼä. </ts>
               <ts e="T50" id="Seg_906" n="e" s="T49">Iːma </ts>
               <ts e="T51" id="Seg_908" n="e" s="T50">ontɨ </ts>
               <ts e="T52" id="Seg_910" n="e" s="T51">uːčʼimpa. </ts>
               <ts e="T53" id="Seg_912" n="e" s="T52">Täpɨn </ts>
               <ts e="T54" id="Seg_914" n="e" s="T53">ilimpɔːtɨt </ts>
               <ts e="T55" id="Seg_916" n="e" s="T54">qaj </ts>
               <ts e="T56" id="Seg_918" n="e" s="T55">muktɨt, </ts>
               <ts e="T57" id="Seg_920" n="e" s="T56">qaj </ts>
               <ts e="T58" id="Seg_922" n="e" s="T57">seːlʼčʼi </ts>
               <ts e="T59" id="Seg_924" n="e" s="T58">qup </ts>
               <ts e="T60" id="Seg_926" n="e" s="T59">(/muktut </ts>
               <ts e="T61" id="Seg_928" n="e" s="T60">qup). </ts>
               <ts e="T62" id="Seg_930" n="e" s="T61">Iːma </ts>
               <ts e="T63" id="Seg_932" n="e" s="T62">ottəqən </ts>
               <ts e="T64" id="Seg_934" n="e" s="T63">uːčʼimpa. </ts>
               <ts e="T65" id="Seg_936" n="e" s="T64">Meːlqo </ts>
               <ts e="T66" id="Seg_938" n="e" s="T65">amnat </ts>
               <ts e="T67" id="Seg_940" n="e" s="T66">ɔːmnɨmpɔːtɨt </ts>
               <ts e="T68" id="Seg_942" n="e" s="T67">(/ɔːmtɔːtɨt). </ts>
               <ts e="T69" id="Seg_944" n="e" s="T68">Qomtätɨt </ts>
               <ts e="T70" id="Seg_946" n="e" s="T69">čʼäːŋkɨmpa </ts>
               <ts e="T71" id="Seg_948" n="e" s="T70">apsɨp </ts>
               <ts e="T72" id="Seg_950" n="e" s="T71">iːpsa. </ts>
               <ts e="T73" id="Seg_952" n="e" s="T72">Nɨːnɨ </ts>
               <ts e="T74" id="Seg_954" n="e" s="T73">iːma </ts>
               <ts e="T75" id="Seg_956" n="e" s="T74">qüːtälʼimpa </ts>
               <ts e="T76" id="Seg_958" n="e" s="T75">(/to </ts>
               <ts e="T77" id="Seg_960" n="e" s="T76">qüːtälʼimpa). </ts>
               <ts e="T78" id="Seg_962" n="e" s="T77">Bolʼnicantɨ </ts>
               <ts e="T79" id="Seg_964" n="e" s="T78">pinpɔːtɨt. </ts>
               <ts e="T80" id="Seg_966" n="e" s="T79">Täp </ts>
               <ts e="T81" id="Seg_968" n="e" s="T80">palʼnicaqɨt </ts>
               <ts e="T82" id="Seg_970" n="e" s="T81">qaj </ts>
               <ts e="T83" id="Seg_972" n="e" s="T82">iräntɨ </ts>
               <ts e="T84" id="Seg_974" n="e" s="T83">kuntɨ </ts>
               <ts e="T85" id="Seg_976" n="e" s="T84">patpa </ts>
               <ts e="T86" id="Seg_978" n="e" s="T85">(/ipɨmpa), </ts>
               <ts e="T87" id="Seg_980" n="e" s="T86">qaj </ts>
               <ts e="T88" id="Seg_982" n="e" s="T87">iräntɨ </ts>
               <ts e="T89" id="Seg_984" n="e" s="T88">kuntɨ </ts>
               <ts e="T90" id="Seg_986" n="e" s="T89">ippɨmpa, </ts>
               <ts e="T91" id="Seg_988" n="e" s="T90">qaj </ts>
               <ts e="T92" id="Seg_990" n="e" s="T91">šitɨ </ts>
               <ts e="T93" id="Seg_992" n="e" s="T92">iräntɨ </ts>
               <ts e="T94" id="Seg_994" n="e" s="T93">kuntɨ </ts>
               <ts e="T95" id="Seg_996" n="e" s="T94">ippɨmpa. </ts>
               <ts e="T96" id="Seg_998" n="e" s="T95">Puːqa </ts>
               <ts e="T97" id="Seg_1000" n="e" s="T96">šünčʼitɨ </ts>
               <ts e="T98" id="Seg_1002" n="e" s="T97">čʼüšəqolampa, </ts>
               <ts e="T99" id="Seg_1004" n="e" s="T98">na </ts>
               <ts e="T100" id="Seg_1006" n="e" s="T99">iːma. </ts>
               <ts e="T101" id="Seg_1008" n="e" s="T100">Polʼnicantɨ </ts>
               <ts e="T102" id="Seg_1010" n="e" s="T101">türpɔːtɨt </ts>
               <ts e="T103" id="Seg_1012" n="e" s="T102">irantɨ </ts>
               <ts e="T104" id="Seg_1014" n="e" s="T103">äma </ts>
               <ts e="T105" id="Seg_1016" n="e" s="T104">irantɨ </ts>
               <ts e="T106" id="Seg_1018" n="e" s="T105">timnʼantɨn </ts>
               <ts e="T107" id="Seg_1020" n="e" s="T106">optısa </ts>
               <ts e="T108" id="Seg_1022" n="e" s="T107">aj </ts>
               <ts e="T109" id="Seg_1024" n="e" s="T108">nɔːkur </ts>
               <ts e="T110" id="Seg_1026" n="e" s="T109">iːjalʼatä </ts>
               <ts e="T111" id="Seg_1028" n="e" s="T110">(kɨplʼa </ts>
               <ts e="T112" id="Seg_1030" n="e" s="T111">ijantɨsa) </ts>
               <ts e="T113" id="Seg_1032" n="e" s="T112">moqonä </ts>
               <ts e="T114" id="Seg_1034" n="e" s="T113">qärqo. </ts>
               <ts e="T115" id="Seg_1036" n="e" s="T114">Iːma </ts>
               <ts e="T116" id="Seg_1038" n="e" s="T115">nılʼčʼik </ts>
               <ts e="T117" id="Seg_1040" n="e" s="T116">tomnätɨ: </ts>
               <ts e="T118" id="Seg_1042" n="e" s="T117">Mat </ts>
               <ts e="T119" id="Seg_1044" n="e" s="T118">tarɨ </ts>
               <ts e="T120" id="Seg_1046" n="e" s="T119">qüːtak. </ts>
               <ts e="T121" id="Seg_1048" n="e" s="T120">Puːqa </ts>
               <ts e="T122" id="Seg_1050" n="e" s="T121">šünčʼon </ts>
               <ts e="T123" id="Seg_1052" n="e" s="T122">tarɨ </ts>
               <ts e="T124" id="Seg_1054" n="e" s="T123">čʼüsa </ts>
               <ts e="T125" id="Seg_1056" n="e" s="T124">(/čʼüša), </ts>
               <ts e="T126" id="Seg_1058" n="e" s="T125">čʼeːl </ts>
               <ts e="T127" id="Seg_1060" n="e" s="T126">moqona </ts>
               <ts e="T128" id="Seg_1062" n="e" s="T127">aša </ts>
               <ts e="T129" id="Seg_1064" n="e" s="T128">qättak. </ts>
               <ts e="T130" id="Seg_1066" n="e" s="T129">Nɨːnä </ts>
               <ts e="T131" id="Seg_1068" n="e" s="T130">na </ts>
               <ts e="T132" id="Seg_1070" n="e" s="T131">iːma </ts>
               <ts e="T133" id="Seg_1072" n="e" s="T132">balʼnicaqɨt </ts>
               <ts e="T134" id="Seg_1074" n="e" s="T133">imatɨtki </ts>
               <ts e="T135" id="Seg_1076" n="e" s="T134">tantɛːsa. </ts>
               <ts e="T136" id="Seg_1078" n="e" s="T135">Somak </ts>
               <ts e="T137" id="Seg_1080" n="e" s="T136">iləqolapsa. </ts>
               <ts e="T138" id="Seg_1082" n="e" s="T137">Iːmat </ts>
               <ts e="T139" id="Seg_1084" n="e" s="T138">täpɨtkinɨk, </ts>
               <ts e="T140" id="Seg_1086" n="e" s="T139">na </ts>
               <ts e="T141" id="Seg_1088" n="e" s="T140">iːmatki, </ts>
               <ts e="T142" id="Seg_1090" n="e" s="T141">miqulʼölʼčʼimpuqolopsɔːtɨt </ts>
               <ts e="T143" id="Seg_1092" n="e" s="T142">(/mikkolʼčʼimpuqolapsɔːtɨt), </ts>
               <ts e="T144" id="Seg_1094" n="e" s="T143">na </ts>
               <ts e="T145" id="Seg_1096" n="e" s="T144">imatkinɨk, </ts>
               <ts e="T146" id="Seg_1098" n="e" s="T145">toːnna </ts>
               <ts e="T147" id="Seg_1100" n="e" s="T146">iːmat </ts>
               <ts e="T148" id="Seg_1102" n="e" s="T147">na </ts>
               <ts e="T149" id="Seg_1104" n="e" s="T148">iːmatki </ts>
               <ts e="T150" id="Seg_1106" n="e" s="T149">mikkolʼolʼčʼimpuqolopsɔːtɨt: </ts>
               <ts e="T151" id="Seg_1108" n="e" s="T150">qalʼporqo, </ts>
               <ts e="T152" id="Seg_1110" n="e" s="T151">porqɨ, </ts>
               <ts e="T153" id="Seg_1112" n="e" s="T152">ükɨ </ts>
               <ts e="T154" id="Seg_1114" n="e" s="T153">kušakwaj. </ts>
               <ts e="T155" id="Seg_1116" n="e" s="T154">Nɨːnɨ </ts>
               <ts e="T156" id="Seg_1118" n="e" s="T155">na </ts>
               <ts e="T157" id="Seg_1120" n="e" s="T156">iːma </ts>
               <ts e="T158" id="Seg_1122" n="e" s="T157">polʼnicaqɨt </ts>
               <ts e="T159" id="Seg_1124" n="e" s="T158">aša </ts>
               <ts e="T160" id="Seg_1126" n="e" s="T159">kunt </ts>
               <ts e="T161" id="Seg_1128" n="e" s="T160">ɛːsa. </ts>
               <ts e="T162" id="Seg_1130" n="e" s="T161">Moqonɨ </ts>
               <ts e="T163" id="Seg_1132" n="e" s="T162">üːtsɔːt. </ts>
               <ts e="T164" id="Seg_1134" n="e" s="T163">Ukoːt </ts>
               <ts e="T165" id="Seg_1136" n="e" s="T164">na </ts>
               <ts e="T166" id="Seg_1138" n="e" s="T165">iːma </ts>
               <ts e="T167" id="Seg_1140" n="e" s="T166">ni </ts>
               <ts e="T168" id="Seg_1142" n="e" s="T167">kučʼ </ts>
               <ts e="T169" id="Seg_1144" n="e" s="T168">aša </ts>
               <ts e="T170" id="Seg_1146" n="e" s="T169">qalʼtresa. </ts>
               <ts e="T171" id="Seg_1148" n="e" s="T170">Puːt </ts>
               <ts e="T172" id="Seg_1150" n="e" s="T171">somalʼak </ts>
               <ts e="T173" id="Seg_1152" n="e" s="T172">ɛselʼa </ts>
               <ts e="T174" id="Seg_1154" n="e" s="T173">qälʼtroqolopsa. </ts>
               <ts e="T175" id="Seg_1156" n="e" s="T174">Ponä </ts>
               <ts e="T176" id="Seg_1158" n="e" s="T175">paktɨlʼä </ts>
               <ts e="T177" id="Seg_1160" n="e" s="T176">kučʼä </ts>
               <ts e="T178" id="Seg_1162" n="e" s="T177">popal </ts>
               <ts e="T179" id="Seg_1164" n="e" s="T178">kuralka. </ts>
               <ts e="T180" id="Seg_1166" n="e" s="T179">Nɨːnɨ </ts>
               <ts e="T181" id="Seg_1168" n="e" s="T180">toktɨr </ts>
               <ts e="T182" id="Seg_1170" n="e" s="T181">ira </ts>
               <ts e="T183" id="Seg_1172" n="e" s="T182">nılʼčʼik </ts>
               <ts e="T184" id="Seg_1174" n="e" s="T183">kätsɨtɨ: </ts>
               <ts e="T185" id="Seg_1176" n="e" s="T184">Kusa </ts>
               <ts e="T186" id="Seg_1178" n="e" s="T185">kuš </ts>
               <ts e="T187" id="Seg_1180" n="e" s="T186">na </ts>
               <ts e="T188" id="Seg_1182" n="e" s="T187">čʼeːlʼe </ts>
               <ts e="T189" id="Seg_1184" n="e" s="T188">ɛːmä </ts>
               <ts e="T190" id="Seg_1186" n="e" s="T189">(/kuš </ts>
               <ts e="T191" id="Seg_1188" n="e" s="T190">täːl </ts>
               <ts e="T192" id="Seg_1190" n="e" s="T191">čʼeːlɨ, </ts>
               <ts e="T193" id="Seg_1192" n="e" s="T192">kos </ts>
               <ts e="T194" id="Seg_1194" n="e" s="T193">tap </ts>
               <ts e="T195" id="Seg_1196" n="e" s="T194">čʼeːlɨ) </ts>
               <ts e="T196" id="Seg_1198" n="e" s="T195">moqonä </ts>
               <ts e="T197" id="Seg_1200" n="e" s="T196">šıntɨ </ts>
               <ts e="T198" id="Seg_1202" n="e" s="T197">üːtɛntɨmɨt. </ts>
               <ts e="T199" id="Seg_1204" n="e" s="T198">Tat </ts>
               <ts e="T200" id="Seg_1206" n="e" s="T199">tiː </ts>
               <ts e="T201" id="Seg_1208" n="e" s="T200">somalʼak </ts>
               <ts e="T202" id="Seg_1210" n="e" s="T201">ɛːŋantɨ. </ts>
               <ts e="T203" id="Seg_1212" n="e" s="T202">Moqonä </ts>
               <ts e="T204" id="Seg_1214" n="e" s="T203">qänmal, </ts>
               <ts e="T205" id="Seg_1216" n="e" s="T204">mɔːtqɨn </ts>
               <ts e="T206" id="Seg_1218" n="e" s="T205">ilʼäšʼik, </ts>
               <ts e="T207" id="Seg_1220" n="e" s="T206">köt </ts>
               <ts e="T208" id="Seg_1222" n="e" s="T207">qaj </ts>
               <ts e="T209" id="Seg_1224" n="e" s="T208">čʼeːlʼ </ts>
               <ts e="T210" id="Seg_1226" n="e" s="T209">mɔːtqɨn </ts>
               <ts e="T211" id="Seg_1228" n="e" s="T210">ilʼäšik. </ts>
               <ts e="T212" id="Seg_1230" n="e" s="T211">Uːčʼila </ts>
               <ts e="T213" id="Seg_1232" n="e" s="T212">ɨkɨ </ts>
               <ts e="T214" id="Seg_1234" n="e" s="T213">qänaš. </ts>
               <ts e="T215" id="Seg_1236" n="e" s="T214">Moqonä </ts>
               <ts e="T216" id="Seg_1238" n="e" s="T215">qänmantɨ, </ts>
               <ts e="T217" id="Seg_1240" n="e" s="T216">mɔːtqɨn </ts>
               <ts e="T218" id="Seg_1242" n="e" s="T217">ɔːmtäšʼik. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2" id="Seg_1243" s="T0">KMG_1976_BriefVacation_nar.001 (001.001)</ta>
            <ta e="T3" id="Seg_1244" s="T2">KMG_1976_BriefVacation_nar.002 (001.002)</ta>
            <ta e="T8" id="Seg_1245" s="T3">KMG_1976_BriefVacation_nar.003 (002.001)</ta>
            <ta e="T11" id="Seg_1246" s="T8">KMG_1976_BriefVacation_nar.004 (002.002)</ta>
            <ta e="T17" id="Seg_1247" s="T11">KMG_1976_BriefVacation_nar.005 (002.003)</ta>
            <ta e="T25" id="Seg_1248" s="T17">KMG_1976_BriefVacation_nar.006 (002.004)</ta>
            <ta e="T32" id="Seg_1249" s="T25">KMG_1976_BriefVacation_nar.007 (002.005)</ta>
            <ta e="T35" id="Seg_1250" s="T32">KMG_1976_BriefVacation_nar.008 (002.006)</ta>
            <ta e="T42" id="Seg_1251" s="T35">KMG_1976_BriefVacation_nar.009 (002.007)</ta>
            <ta e="T49" id="Seg_1252" s="T42">KMG_1976_BriefVacation_nar.010 (002.008)</ta>
            <ta e="T52" id="Seg_1253" s="T49">KMG_1976_BriefVacation_nar.011 (002.009)</ta>
            <ta e="T61" id="Seg_1254" s="T52">KMG_1976_BriefVacation_nar.012 (002.010)</ta>
            <ta e="T64" id="Seg_1255" s="T61">KMG_1976_BriefVacation_nar.013 (002.011)</ta>
            <ta e="T68" id="Seg_1256" s="T64">KMG_1976_BriefVacation_nar.014 (002.012)</ta>
            <ta e="T72" id="Seg_1257" s="T68">KMG_1976_BriefVacation_nar.015 (002.013)</ta>
            <ta e="T77" id="Seg_1258" s="T72">KMG_1976_BriefVacation_nar.016 (002.014)</ta>
            <ta e="T79" id="Seg_1259" s="T77">KMG_1976_BriefVacation_nar.017 (002.015)</ta>
            <ta e="T95" id="Seg_1260" s="T79">KMG_1976_BriefVacation_nar.018 (002.016)</ta>
            <ta e="T100" id="Seg_1261" s="T95">KMG_1976_BriefVacation_nar.019 (002.017)</ta>
            <ta e="T114" id="Seg_1262" s="T100">KMG_1976_BriefVacation_nar.020 (002.018)</ta>
            <ta e="T120" id="Seg_1263" s="T114">KMG_1976_BriefVacation_nar.021 (002.019)</ta>
            <ta e="T129" id="Seg_1264" s="T120">KMG_1976_BriefVacation_nar.022 (002.020)</ta>
            <ta e="T135" id="Seg_1265" s="T129">KMG_1976_BriefVacation_nar.023 (002.021)</ta>
            <ta e="T137" id="Seg_1266" s="T135">KMG_1976_BriefVacation_nar.024 (002.022)</ta>
            <ta e="T154" id="Seg_1267" s="T137">KMG_1976_BriefVacation_nar.025 (002.023)</ta>
            <ta e="T161" id="Seg_1268" s="T154">KMG_1976_BriefVacation_nar.026 (002.024)</ta>
            <ta e="T163" id="Seg_1269" s="T161">KMG_1976_BriefVacation_nar.027 (002.025)</ta>
            <ta e="T170" id="Seg_1270" s="T163">KMG_1976_BriefVacation_nar.028 (002.026)</ta>
            <ta e="T174" id="Seg_1271" s="T170">KMG_1976_BriefVacation_nar.029 (002.027)</ta>
            <ta e="T179" id="Seg_1272" s="T174">KMG_1976_BriefVacation_nar.030 (002.028)</ta>
            <ta e="T198" id="Seg_1273" s="T179">KMG_1976_BriefVacation_nar.031 (002.029)</ta>
            <ta e="T202" id="Seg_1274" s="T198">KMG_1976_BriefVacation_nar.032 (002.030)</ta>
            <ta e="T211" id="Seg_1275" s="T202">KMG_1976_BriefVacation_nar.033 (002.031)</ta>
            <ta e="T214" id="Seg_1276" s="T211">KMG_1976_BriefVacation_nar.034 (002.032)</ta>
            <ta e="T218" id="Seg_1277" s="T214">KMG_1976_BriefVacation_nar.035 (002.033)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T2" id="Seg_1278" s="T0">Короткий отпуск.</ta>
            <ta e="T3" id="Seg_1279" s="T2">Фильм.</ta>
            <ta e="T8" id="Seg_1280" s="T3">И′талʼаɣыт ′мынныл ′ондыlkут ′тӓттоɣыт.</ta>
            <ta e="T11" id="Seg_1281" s="T8">′Ӣма ӣlимпа и′расымыl.</ta>
            <ta e="T17" id="Seg_1282" s="T11">ӣ′манныɣыт ′ногыр ′кыблʼа и′jаӣты е̄за.</ta>
            <ta e="T25" id="Seg_1283" s="T17">на ӣ′ман и′ра ку[o]с[ш]′куны ӯдʼимба, топыты ′съ̄[ӓ]быте̄мбаты.</ta>
            <ta e="T32" id="Seg_1284" s="T25">тобыты ′чужелʼӓ а′жа ′ӯдʼжʼимба ′понды кӓшкунды.</ta>
            <ta e="T35" id="Seg_1285" s="T32">′ӣматы ′отдыгынды ′ӯдʼимба.</ta>
            <ta e="T42" id="Seg_1286" s="T35">а и′ранды тʼим′нʼа иң[н]′галбыlӓ мо̄тkын ′меlко омнымба.</ta>
            <ta e="T49" id="Seg_1287" s="T42">ни куны [ни кут] ӯдʼиkо ′ажа ′кы′гылʼӓ.</ta>
            <ta e="T52" id="Seg_1288" s="T49">′ӣма ′от[н]ды ′ӯдʼимба.</ta>
            <ta e="T61" id="Seg_1289" s="T52">′тӓбын ӣlимпо̄тыт kай муkтыт, kай селʼдʼи kуп (муkтут kуп).</ta>
            <ta e="T64" id="Seg_1290" s="T61">′ӣма ′отдъгън ′ӯдʼимб̂а.</ta>
            <ta e="T68" id="Seg_1291" s="T64">′меlко ′амнат ′омнымботыт (′о̄мто̄тыт).</ta>
            <ta e="T72" id="Seg_1292" s="T68">′kомдӓтыт че̄ң[н]гымба апсып ′ӣпса.</ta>
            <ta e="T77" id="Seg_1293" s="T72">ныны ӣ′ма ′кӱ̄делʼимба (то кӱ̄делʼимба).</ta>
            <ta e="T79" id="Seg_1294" s="T77">болʼницанды пинбо̄тыт.</ta>
            <ta e="T95" id="Seg_1295" s="T79">тӓп б̂алʼницаɣыт kай и′рендыkунды патпа [′ипымпа], kай и′рендыkунды ′иппымба, kай шʼиты и′рендыkунды ′иппымба.</ta>
            <ta e="T100" id="Seg_1296" s="T95">′пӯга ′шʼундʼжʼиты ′чӯжъкоlамба, на ӣ′ма.</ta>
            <ta e="T114" id="Seg_1297" s="T100">б̂олʼ′ницанды ′тӱ̄рп[б̂]о̄тыт. и′ранды ′э̄ма и′ранды тимнʼанды ноптыса ай ′но̄Гур ӣ′jаlатӓ кыб′лʼа иjандыса ′моkонӓ kӓ̄рГо.</ta>
            <ta e="T120" id="Seg_1298" s="T114">′ӣма ′ниlдʼжʼик ′томнӓты, мат тары кӱ̄так.</ta>
            <ta e="T129" id="Seg_1299" s="T120">′пӯгашʼунджоп тары чӯса (чӯша) че̄лмоГона а′жа kӓтдак.</ta>
            <ta e="T135" id="Seg_1300" s="T129">′нынӓ на ӣ′ма балʼницаɣыт и′матытки тант е̄за.</ta>
            <ta e="T137" id="Seg_1301" s="T135">со′мак иlъкоlапса.</ta>
            <ta e="T154" id="Seg_1302" s="T137">ӣ′мат ′тӓбыт‵кинык на ӣ′матки ′мику′лʼӧлʼчимбуколопсоды[ъ]т [мӣ′колʼчимбуколап′со̄дыт] на иматкинык, тонна ӣ′мат на ӣ′матки ′мико‵лʼолʼчимбуколопсодыт. ′kалʼборkо, порkы, ӱ̄кы ку′шакwай.</ta>
            <ta e="T161" id="Seg_1303" s="T154">′ныны на ӣ′ма б̂олʼницаɣыт а′жа ′кунд ′е̄за.</ta>
            <ta e="T163" id="Seg_1304" s="T161">моГоны ӱт′со̄т.</ta>
            <ta e="T170" id="Seg_1305" s="T163">у′гот на ӣ′ма ни куч а′жа kалʼтреса.</ta>
            <ta e="T174" id="Seg_1306" s="T170">пӯт ′сомаlа ′kӓзелʼа ′kӓлʼтро‵коlопса.</ta>
            <ta e="T179" id="Seg_1307" s="T174">понӓ паkтылʼӓ ку′че по′пал ку′ралга.</ta>
            <ta e="T198" id="Seg_1308" s="T179">ныны токтыр и′ра нилʼдʼжʼи ′kӓтсыты ку′са кушʼ на че̄лʼе е̄′мэ (ку′штел чʼе̄лы кос тап че̄лы) ′моkонӓ шʼинды ′ӱ̄дендымыт.</ta>
            <ta e="T202" id="Seg_1309" s="T198">тат тӣ со̄′маlак е̄ңанты.</ta>
            <ta e="T211" id="Seg_1310" s="T202">′моГонӓ kӓнмал, мотkын ӣlʼӓшʼик, ′кӧт kай че̄лʼ ′мо̄тkын ӣ′лʼӓшʼик.</ta>
            <ta e="T214" id="Seg_1311" s="T211">′ӯдʼила ′ыгы ′kӓнаш.</ta>
            <ta e="T218" id="Seg_1312" s="T214">′моkонӓ ′kӓнманты, мотkын ′омдӓшʼик.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T2" id="Seg_1313" s="T0">Кorotkij otpusk.</ta>
            <ta e="T3" id="Seg_1314" s="T2">Filʼm.</ta>
            <ta e="T8" id="Seg_1315" s="T3">Italʼaqɨt mɨnnɨl ondɨlʼqut tättoqɨt.</ta>
            <ta e="T11" id="Seg_1316" s="T8">Иːma iːlʼimpa irasɨmɨlʼ.</ta>
            <ta e="T17" id="Seg_1317" s="T11">iːmannɨqɨt nogɨr kɨplʼa ijaiːtɨ eːza.</ta>
            <ta e="T25" id="Seg_1318" s="T17">na iːman ira ku[o]s[š]kunɨ uːdʼimpa, topɨtɨ səː[ä]pɨteːmpatɨ.</ta>
            <ta e="T32" id="Seg_1319" s="T25">topɨtɨ čʼuжelʼä aжa uːdʼжʼimpa pondɨ käškundɨ.</ta>
            <ta e="T35" id="Seg_1320" s="T32">iːmatɨ otdɨgɨndɨ uːdʼimpa.</ta>
            <ta e="T42" id="Seg_1321" s="T35">a irandɨ tʼimnʼa iŋ[n]galpɨlʼä moːtqɨn melʼko omnɨmpa.</ta>
            <ta e="T49" id="Seg_1322" s="T42">ni kunɨ [ni kut] uːdʼiqo aжa kɨgɨlʼä.</ta>
            <ta e="T52" id="Seg_1323" s="T49">iːma ot[n]dɨ uːdʼimpa.</ta>
            <ta e="T61" id="Seg_1324" s="T52">täpɨn iːlʼimpoːtɨt qaj muqtɨt, qaj selʼdʼi qup (muqtut qup).</ta>
            <ta e="T64" id="Seg_1325" s="T61">iːma otdəgən uːdʼimp̂a.</ta>
            <ta e="T68" id="Seg_1326" s="T64">melʼko amnat omnɨmpotɨt (oːmtoːtɨt).</ta>
            <ta e="T72" id="Seg_1327" s="T68">qomdätɨt čʼeːŋ[n]gɨmpa apsɨp iːpsa.</ta>
            <ta e="T77" id="Seg_1328" s="T72">nɨnɨ iːma küːdelʼimpa (to küːdelʼimpa).</ta>
            <ta e="T79" id="Seg_1329" s="T77">bolʼnicandɨ ′pinpoːtɨt.</ta>
            <ta e="T95" id="Seg_1330" s="T79">täp p̂alʼnicaqɨt qaj irendɨqundɨ patpa [ipɨmpa], qaj irendɨqundɨ ippɨmpa, qaj šʼitɨ irendɨqundɨ ippɨmpa.</ta>
            <ta e="T100" id="Seg_1331" s="T95">puːga šʼundʼжʼitɨ čʼuːжəkolʼampa, na iːma.</ta>
            <ta e="T114" id="Seg_1332" s="T100">p̂olʼnicandɨ tüːrp[p̂]oːtɨt. irandɨ эːma irandɨ timnʼandɨ noptɨsa aj noːГur iːjalʼatä kɨplʼa ijandɨsa moqonä qäːrГo.</ta>
            <ta e="T120" id="Seg_1333" s="T114">iːma nilʼdʼжʼik tomnätɨ, mat tarɨ küːtak.</ta>
            <ta e="T129" id="Seg_1334" s="T120">puːgašʼundжop tarɨ čʼuːsa (čʼuːša) čʼeːlmoГona aжa qätdak.</ta>
            <ta e="T135" id="Seg_1335" s="T129">nɨnä na iːma balʼnicaqɨt imatɨtki tant eːza.</ta>
            <ta e="T137" id="Seg_1336" s="T135">somak ilʼəkolʼapsa.</ta>
            <ta e="T154" id="Seg_1337" s="T137"> iːmat täpɨtkinɨk na iːmatki mikulʼölʼčʼimpukolopsodɨ[ə]t [miːkolʼčʼimpukolapsoːdɨt] na imatkinɨk, tonna iːmat na iːmatki mikolʼolʼčʼimpukolopsodɨt. qalʼporqo, porqɨ, üːkɨ kušakwaj.</ta>
            <ta e="T161" id="Seg_1338" s="T154">nɨnɨ na iːma p̂olʼnicaqɨt aжa kund eːza.</ta>
            <ta e="T163" id="Seg_1339" s="T161">moГonɨ ütsoːt.</ta>
            <ta e="T170" id="Seg_1340" s="T163">ugot na iːma ni kučʼ aжa qalʼtresa.</ta>
            <ta e="T174" id="Seg_1341" s="T170">puːt somalʼa qäzelʼa qälʼtrokolʼopsa.</ta>
            <ta e="T179" id="Seg_1342" s="T174">ponä paqtɨlʼä kučʼe popal kuralga.</ta>
            <ta e="T198" id="Seg_1343" s="T179">nɨnɨ toktɨr ira nilʼdʼжʼi qätsɨtɨ kusa kušʼ na čʼeːlʼe eːmэ (kuštel čʼeːlɨ kos tap čʼeːlɨ) moqonä šʼindɨ üːdendɨmɨt.</ta>
            <ta e="T202" id="Seg_1344" s="T198">tat tiː soːmalʼak eːŋantɨ.</ta>
            <ta e="T211" id="Seg_1345" s="T202">moГonä qänmal, motqɨn iːlʼäšʼik, köt qaj čʼeːlʼ moːtqɨn iːlʼäšik.</ta>
            <ta e="T214" id="Seg_1346" s="T211">uːdʼila ɨgɨ qänaš.</ta>
            <ta e="T218" id="Seg_1347" s="T214">moqonä qänmantɨ, motqɨn omdäšʼik.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2" id="Seg_1348" s="T0">Korotkij otpusk. </ta>
            <ta e="T3" id="Seg_1349" s="T2">Filʼm. </ta>
            <ta e="T8" id="Seg_1350" s="T3">Italʼaqɨt, mɨnɨl ɔːmtɨlʼ qoːt tättoːqɨt. </ta>
            <ta e="T11" id="Seg_1351" s="T8">Iːma ilimpa irasɨmɨlʼ. </ta>
            <ta e="T17" id="Seg_1352" s="T11">Iːman mɨqɨt nɔːkɨr kɨplʼa ijaiːtɨ ɛːsa. </ta>
            <ta e="T25" id="Seg_1353" s="T17">Na iːman ira kos kunɨ uːčʼimpa, topɨtɨ səpɨtɛːmpatɨ. </ta>
            <ta e="T32" id="Seg_1354" s="T25">Topɨtɨ čʼüšelʼä aša uːčʼimpa poːntɨ käš kuntɨ. </ta>
            <ta e="T35" id="Seg_1355" s="T32">Iːmatɨ ottɨqɨntɨ uːčʼimpa. </ta>
            <ta e="T42" id="Seg_1356" s="T35">A irantɨ tʼimnʼa iŋkalpɨlʼä mɔːtqɨn meːlqo ɔːmnɨmpa. </ta>
            <ta e="T49" id="Seg_1357" s="T42">Ni kunɨ (/ni kut) uːčʼiqo aša kɨkɨlʼä. </ta>
            <ta e="T52" id="Seg_1358" s="T49">Iːma ontɨ uːčʼimpa. </ta>
            <ta e="T61" id="Seg_1359" s="T52">Täpɨn ilimpɔːtɨt qaj muktɨt, qaj seːlʼčʼi qup (/muktut qup). </ta>
            <ta e="T64" id="Seg_1360" s="T61">Iːma ottəqən uːčʼimpa. </ta>
            <ta e="T68" id="Seg_1361" s="T64">Meːlqo amnat ɔːmnɨmpɔːtɨt (/ɔːmtɔːtɨt). </ta>
            <ta e="T72" id="Seg_1362" s="T68">Qomtätɨt čʼäːŋkɨmpa apsɨp iːpsa. </ta>
            <ta e="T77" id="Seg_1363" s="T72">Nɨːnɨ iːma qüːtälʼimpa (/to qüːtälʼimpa). </ta>
            <ta e="T79" id="Seg_1364" s="T77">Bolʼnicantɨ pinpɔːtɨt. </ta>
            <ta e="T95" id="Seg_1365" s="T79">Täp palʼnicaqɨt qaj iräntɨ kuntɨ patpa (/ipɨmpa), qaj iräntɨ kuntɨ ippɨmpa, qaj šitɨ iräntɨ kuntɨ ippɨmpa. </ta>
            <ta e="T100" id="Seg_1366" s="T95">Puːqa šünčʼitɨ čʼüšəqolampa, na iːma. </ta>
            <ta e="T114" id="Seg_1367" s="T100">Polʼnicantɨ türpɔːtɨt irantɨ äma irantɨ timnʼantɨn optısa aj nɔːkur iːjalʼatä (kɨplʼa ijantɨsa) moqonä qärqo. </ta>
            <ta e="T120" id="Seg_1368" s="T114">Iːma nılʼčʼik tomnätɨ: Mat tarɨ qüːtak. </ta>
            <ta e="T129" id="Seg_1369" s="T120">Puːqa šünčʼon tarɨ čʼüsa (/čʼüša), čʼeːl moqona aša qättak. </ta>
            <ta e="T135" id="Seg_1370" s="T129">Nɨːnä na iːma balʼnicaqɨt imatɨtki tantɛːsa. </ta>
            <ta e="T137" id="Seg_1371" s="T135">Somak iləqolapsa. </ta>
            <ta e="T154" id="Seg_1372" s="T137">Iːmat täpɨtkinɨk, na iːmatki, miqulʼölʼčʼimpuqolopsɔːtɨt (/mikkolʼčʼimpuqolapsɔːtɨt), na imatkinɨk, toːnna iːmat na iːmatki mikkolʼolʼčʼimpuqolopsɔːtɨt: qalʼporqo, porqɨ, ükɨ kušakwaj. </ta>
            <ta e="T161" id="Seg_1373" s="T154">Nɨːnɨ na iːma polʼnicaqɨt aša kunt ɛːsa. </ta>
            <ta e="T163" id="Seg_1374" s="T161">Moqonɨ üːtsɔːt. </ta>
            <ta e="T170" id="Seg_1375" s="T163">Ukoːt na iːma ni kučʼ aša qalʼtresa. </ta>
            <ta e="T174" id="Seg_1376" s="T170">Puːt somalʼak ɛselʼa qälʼtroqolopsa. </ta>
            <ta e="T179" id="Seg_1377" s="T174">Ponä paktɨlʼä kučʼä popal kuralka. </ta>
            <ta e="T198" id="Seg_1378" s="T179">Nɨːnɨ toktɨr ira nılʼčʼik kätsɨtɨ: Kusa kuš na čʼeːlʼe ɛːmä (/kuš täːl čʼeːlɨ, kos tap čʼeːlɨ) moqonä šıntɨ üːtɛntɨmɨt. </ta>
            <ta e="T202" id="Seg_1379" s="T198">Tat tiː somalʼak ɛːŋantɨ. </ta>
            <ta e="T211" id="Seg_1380" s="T202">Moqonä qänmal, mɔːtqɨn ilʼäšʼik, köt qaj čʼeːlʼ mɔːtqɨn ilʼäšik. </ta>
            <ta e="T214" id="Seg_1381" s="T211">Uːčʼila ɨkɨ qänaš. </ta>
            <ta e="T218" id="Seg_1382" s="T214">Moqonä qänmantɨ, mɔːtqɨn ɔːmtäšʼik. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1383" s="T0">korotkij</ta>
            <ta e="T2" id="Seg_1384" s="T1">otpusk</ta>
            <ta e="T3" id="Seg_1385" s="T2">filʼm</ta>
            <ta e="T4" id="Seg_1386" s="T3">Italʼa-qɨt</ta>
            <ta e="T5" id="Seg_1387" s="T4">mɨ-n-ɨ-l</ta>
            <ta e="T6" id="Seg_1388" s="T5">ɔːmtɨ-lʼ</ta>
            <ta e="T7" id="Seg_1389" s="T6">qoː-t</ta>
            <ta e="T8" id="Seg_1390" s="T7">tättoː-qɨt</ta>
            <ta e="T9" id="Seg_1391" s="T8">iːma</ta>
            <ta e="T10" id="Seg_1392" s="T9">ili-mpa</ta>
            <ta e="T11" id="Seg_1393" s="T10">ira-sɨmɨ-lʼ</ta>
            <ta e="T12" id="Seg_1394" s="T11">iːma-n</ta>
            <ta e="T13" id="Seg_1395" s="T12">mɨ-qɨt</ta>
            <ta e="T14" id="Seg_1396" s="T13">nɔːkɨr</ta>
            <ta e="T15" id="Seg_1397" s="T14">kɨp-lʼa</ta>
            <ta e="T16" id="Seg_1398" s="T15">ija-iː-tɨ</ta>
            <ta e="T17" id="Seg_1399" s="T16">ɛː-sa</ta>
            <ta e="T18" id="Seg_1400" s="T17">na</ta>
            <ta e="T19" id="Seg_1401" s="T18">iːma-n</ta>
            <ta e="T20" id="Seg_1402" s="T19">ira</ta>
            <ta e="T21" id="Seg_1403" s="T20">kos</ta>
            <ta e="T22" id="Seg_1404" s="T21">kunɨ</ta>
            <ta e="T23" id="Seg_1405" s="T22">uːčʼi-mpa</ta>
            <ta e="T24" id="Seg_1406" s="T23">topɨ-tɨ</ta>
            <ta e="T25" id="Seg_1407" s="T24">səpɨ-t-ɛː-mpa-tɨ</ta>
            <ta e="T26" id="Seg_1408" s="T25">topɨ-tɨ</ta>
            <ta e="T27" id="Seg_1409" s="T26">čʼüše-lʼä</ta>
            <ta e="T28" id="Seg_1410" s="T27">aša</ta>
            <ta e="T29" id="Seg_1411" s="T28">uːčʼi-mpa</ta>
            <ta e="T30" id="Seg_1412" s="T29">poː-n-tɨ</ta>
            <ta e="T31" id="Seg_1413" s="T30">käš</ta>
            <ta e="T32" id="Seg_1414" s="T31">kuntɨ</ta>
            <ta e="T33" id="Seg_1415" s="T32">iːma-tɨ</ta>
            <ta e="T34" id="Seg_1416" s="T33">ottɨ-qɨn-tɨ</ta>
            <ta e="T35" id="Seg_1417" s="T34">uːčʼi-mpa</ta>
            <ta e="T36" id="Seg_1418" s="T35">a</ta>
            <ta e="T37" id="Seg_1419" s="T36">ira-n-tɨ</ta>
            <ta e="T38" id="Seg_1420" s="T37">tʼimnʼa</ta>
            <ta e="T39" id="Seg_1421" s="T38">iŋkalpɨ-lʼä</ta>
            <ta e="T40" id="Seg_1422" s="T39">mɔːt-qɨn</ta>
            <ta e="T41" id="Seg_1423" s="T40">meːlqo</ta>
            <ta e="T42" id="Seg_1424" s="T41">ɔːmnɨ-mpa</ta>
            <ta e="T43" id="Seg_1425" s="T42">ni</ta>
            <ta e="T44" id="Seg_1426" s="T43">kunɨ</ta>
            <ta e="T45" id="Seg_1427" s="T44">ni</ta>
            <ta e="T46" id="Seg_1428" s="T45">kut</ta>
            <ta e="T47" id="Seg_1429" s="T46">uːčʼi-qo</ta>
            <ta e="T48" id="Seg_1430" s="T47">aša</ta>
            <ta e="T49" id="Seg_1431" s="T48">kɨkɨ-lʼä</ta>
            <ta e="T50" id="Seg_1432" s="T49">iːma</ta>
            <ta e="T51" id="Seg_1433" s="T50">ontɨ</ta>
            <ta e="T52" id="Seg_1434" s="T51">uːčʼi-mpa</ta>
            <ta e="T53" id="Seg_1435" s="T52">täp-ɨ-n</ta>
            <ta e="T54" id="Seg_1436" s="T53">ili-mpɔː-tɨt</ta>
            <ta e="T55" id="Seg_1437" s="T54">qaj</ta>
            <ta e="T56" id="Seg_1438" s="T55">muktɨt</ta>
            <ta e="T57" id="Seg_1439" s="T56">qaj</ta>
            <ta e="T58" id="Seg_1440" s="T57">seːlʼčʼi</ta>
            <ta e="T59" id="Seg_1441" s="T58">qup</ta>
            <ta e="T60" id="Seg_1442" s="T59">muktut</ta>
            <ta e="T61" id="Seg_1443" s="T60">qup</ta>
            <ta e="T62" id="Seg_1444" s="T61">iːma</ta>
            <ta e="T63" id="Seg_1445" s="T62">ottə-qən</ta>
            <ta e="T64" id="Seg_1446" s="T63">uːčʼi-mpa</ta>
            <ta e="T65" id="Seg_1447" s="T64">meːlqo</ta>
            <ta e="T66" id="Seg_1448" s="T65">amna-t</ta>
            <ta e="T67" id="Seg_1449" s="T66">ɔːmnɨ-mpɔː-tɨt</ta>
            <ta e="T68" id="Seg_1450" s="T67">ɔːmtɔː-tɨt</ta>
            <ta e="T69" id="Seg_1451" s="T68">qomtä-tɨt</ta>
            <ta e="T70" id="Seg_1452" s="T69">čʼäːŋkɨ-mpa</ta>
            <ta e="T71" id="Seg_1453" s="T70">apsɨ-p</ta>
            <ta e="T72" id="Seg_1454" s="T71">iː-psa</ta>
            <ta e="T73" id="Seg_1455" s="T72">nɨːnɨ</ta>
            <ta e="T74" id="Seg_1456" s="T73">iːma</ta>
            <ta e="T75" id="Seg_1457" s="T74">qüːt-älʼi-mpa</ta>
            <ta e="T76" id="Seg_1458" s="T75">to</ta>
            <ta e="T77" id="Seg_1459" s="T76">qüːt-älʼi-mpa</ta>
            <ta e="T78" id="Seg_1460" s="T77">bolʼnica-ntɨ</ta>
            <ta e="T79" id="Seg_1461" s="T78">pin-pɔː-tɨt</ta>
            <ta e="T80" id="Seg_1462" s="T79">täp</ta>
            <ta e="T81" id="Seg_1463" s="T80">palʼnica-qɨt</ta>
            <ta e="T82" id="Seg_1464" s="T81">qaj</ta>
            <ta e="T83" id="Seg_1465" s="T82">irä-n-tɨ</ta>
            <ta e="T84" id="Seg_1466" s="T83">kuntɨ</ta>
            <ta e="T85" id="Seg_1467" s="T84">pat-pa</ta>
            <ta e="T86" id="Seg_1468" s="T85">ipɨ-mpa</ta>
            <ta e="T87" id="Seg_1469" s="T86">qaj</ta>
            <ta e="T88" id="Seg_1470" s="T87">irä-n-tɨ</ta>
            <ta e="T89" id="Seg_1471" s="T88">kuntɨ</ta>
            <ta e="T90" id="Seg_1472" s="T89">ippɨ-mpa</ta>
            <ta e="T91" id="Seg_1473" s="T90">qaj</ta>
            <ta e="T92" id="Seg_1474" s="T91">šitɨ</ta>
            <ta e="T93" id="Seg_1475" s="T92">irä-n-tɨ</ta>
            <ta e="T94" id="Seg_1476" s="T93">kuntɨ</ta>
            <ta e="T95" id="Seg_1477" s="T94">ippɨ-mpa</ta>
            <ta e="T96" id="Seg_1478" s="T95">puː-qa</ta>
            <ta e="T97" id="Seg_1479" s="T96">šünčʼi-tɨ</ta>
            <ta e="T98" id="Seg_1480" s="T97">čʼüšə-q-olam-pa</ta>
            <ta e="T99" id="Seg_1481" s="T98">na</ta>
            <ta e="T100" id="Seg_1482" s="T99">iːma</ta>
            <ta e="T101" id="Seg_1483" s="T100">polʼnica-ntɨ</ta>
            <ta e="T102" id="Seg_1484" s="T101">tü-r-pɔː-tɨt</ta>
            <ta e="T103" id="Seg_1485" s="T102">ira-n-tɨ</ta>
            <ta e="T104" id="Seg_1486" s="T103">äma</ta>
            <ta e="T105" id="Seg_1487" s="T104">ira-n-tɨ</ta>
            <ta e="T106" id="Seg_1488" s="T105">timnʼa-n-tɨn</ta>
            <ta e="T107" id="Seg_1489" s="T106">optı-sa</ta>
            <ta e="T108" id="Seg_1490" s="T107">aj</ta>
            <ta e="T109" id="Seg_1491" s="T108">nɔːkur</ta>
            <ta e="T110" id="Seg_1492" s="T109">iːja-lʼa-tä</ta>
            <ta e="T111" id="Seg_1493" s="T110">kɨp-lʼa</ta>
            <ta e="T112" id="Seg_1494" s="T111">ija-ntɨ-sa</ta>
            <ta e="T113" id="Seg_1495" s="T112">moqonä</ta>
            <ta e="T114" id="Seg_1496" s="T113">qär-qo</ta>
            <ta e="T115" id="Seg_1497" s="T114">iːma</ta>
            <ta e="T116" id="Seg_1498" s="T115">nılʼčʼi-k</ta>
            <ta e="T117" id="Seg_1499" s="T116">tom-nä-tɨ</ta>
            <ta e="T118" id="Seg_1500" s="T117">Mat</ta>
            <ta e="T119" id="Seg_1501" s="T118">tarɨ</ta>
            <ta e="T120" id="Seg_1502" s="T119">qüːta-k</ta>
            <ta e="T121" id="Seg_1503" s="T120">puː-qa</ta>
            <ta e="T122" id="Seg_1504" s="T121">šünčʼo-n</ta>
            <ta e="T123" id="Seg_1505" s="T122">tarɨ</ta>
            <ta e="T124" id="Seg_1506" s="T123">čʼüsa</ta>
            <ta e="T125" id="Seg_1507" s="T124">čʼüša</ta>
            <ta e="T126" id="Seg_1508" s="T125">čʼeːl</ta>
            <ta e="T127" id="Seg_1509" s="T126">moqona</ta>
            <ta e="T128" id="Seg_1510" s="T127">aša</ta>
            <ta e="T129" id="Seg_1511" s="T128">qät-ta-k</ta>
            <ta e="T130" id="Seg_1512" s="T129">nɨːnä</ta>
            <ta e="T131" id="Seg_1513" s="T130">na</ta>
            <ta e="T132" id="Seg_1514" s="T131">iːma</ta>
            <ta e="T133" id="Seg_1515" s="T132">balʼnica-qɨt</ta>
            <ta e="T134" id="Seg_1516" s="T133">ima-t-ɨ-tki</ta>
            <ta e="T135" id="Seg_1517" s="T134">tant-ɛː-sa</ta>
            <ta e="T136" id="Seg_1518" s="T135">soma-k</ta>
            <ta e="T137" id="Seg_1519" s="T136">ilə-q-olap-sa</ta>
            <ta e="T138" id="Seg_1520" s="T137">iːma-t</ta>
            <ta e="T139" id="Seg_1521" s="T138">täp-ɨ-tkinɨk</ta>
            <ta e="T140" id="Seg_1522" s="T139">na</ta>
            <ta e="T141" id="Seg_1523" s="T140">iːma-tki</ta>
            <ta e="T142" id="Seg_1524" s="T141">mi-qulʼ-ölʼ-čʼi-mpu-qo-lop-sɔː-tɨt</ta>
            <ta e="T143" id="Seg_1525" s="T142">mi-kk-olʼ-čʼi-mpu-qo-lap-sɔː-tɨt</ta>
            <ta e="T144" id="Seg_1526" s="T143">na</ta>
            <ta e="T145" id="Seg_1527" s="T144">ima-tkinɨk</ta>
            <ta e="T146" id="Seg_1528" s="T145">toːnna</ta>
            <ta e="T147" id="Seg_1529" s="T146">iːma-t</ta>
            <ta e="T148" id="Seg_1530" s="T147">na</ta>
            <ta e="T149" id="Seg_1531" s="T148">iːma-tki</ta>
            <ta e="T150" id="Seg_1532" s="T149">mi-kk-olʼ-olʼ-čʼi-mpu-q-olop-sɔː-tɨt</ta>
            <ta e="T151" id="Seg_1533" s="T150">qalʼporqo</ta>
            <ta e="T152" id="Seg_1534" s="T151">porqɨ</ta>
            <ta e="T153" id="Seg_1535" s="T152">ükɨ</ta>
            <ta e="T154" id="Seg_1536" s="T153">kušak-waj</ta>
            <ta e="T155" id="Seg_1537" s="T154">nɨːnɨ</ta>
            <ta e="T156" id="Seg_1538" s="T155">na</ta>
            <ta e="T157" id="Seg_1539" s="T156">iːma</ta>
            <ta e="T158" id="Seg_1540" s="T157">polʼnica-qɨt</ta>
            <ta e="T159" id="Seg_1541" s="T158">aša</ta>
            <ta e="T160" id="Seg_1542" s="T159">kunt</ta>
            <ta e="T161" id="Seg_1543" s="T160">ɛː-sa</ta>
            <ta e="T162" id="Seg_1544" s="T161">moqonɨ</ta>
            <ta e="T163" id="Seg_1545" s="T162">üːt-sɔː-t</ta>
            <ta e="T164" id="Seg_1546" s="T163">ukoːt</ta>
            <ta e="T165" id="Seg_1547" s="T164">na</ta>
            <ta e="T166" id="Seg_1548" s="T165">iːma</ta>
            <ta e="T167" id="Seg_1549" s="T166">ni</ta>
            <ta e="T168" id="Seg_1550" s="T167">kučʼ</ta>
            <ta e="T169" id="Seg_1551" s="T168">aša</ta>
            <ta e="T170" id="Seg_1552" s="T169">qalʼ-tre-sa</ta>
            <ta e="T171" id="Seg_1553" s="T170">puːt</ta>
            <ta e="T172" id="Seg_1554" s="T171">soma-lʼa-k</ta>
            <ta e="T173" id="Seg_1555" s="T172">ɛse-lʼa</ta>
            <ta e="T174" id="Seg_1556" s="T173">qälʼ-tro-q-olop-sa</ta>
            <ta e="T175" id="Seg_1557" s="T174">ponä</ta>
            <ta e="T176" id="Seg_1558" s="T175">paktɨ-lʼä</ta>
            <ta e="T177" id="Seg_1559" s="T176">kutʼä</ta>
            <ta e="T178" id="Seg_1560" s="T177">popal</ta>
            <ta e="T179" id="Seg_1561" s="T178">kur-al-ka</ta>
            <ta e="T180" id="Seg_1562" s="T179">nɨːnɨ</ta>
            <ta e="T181" id="Seg_1563" s="T180">toktɨr</ta>
            <ta e="T182" id="Seg_1564" s="T181">ira</ta>
            <ta e="T183" id="Seg_1565" s="T182">nılʼčʼi-k</ta>
            <ta e="T184" id="Seg_1566" s="T183">kät-sɨ-tɨ</ta>
            <ta e="T185" id="Seg_1567" s="T184">kusa</ta>
            <ta e="T186" id="Seg_1568" s="T185">kuš</ta>
            <ta e="T187" id="Seg_1569" s="T186">na</ta>
            <ta e="T188" id="Seg_1570" s="T187">čʼeːlʼe</ta>
            <ta e="T189" id="Seg_1571" s="T188">ɛːmä</ta>
            <ta e="T190" id="Seg_1572" s="T189">kuš</ta>
            <ta e="T191" id="Seg_1573" s="T190">täːl</ta>
            <ta e="T192" id="Seg_1574" s="T191">čʼeːlɨ</ta>
            <ta e="T193" id="Seg_1575" s="T192">kos</ta>
            <ta e="T194" id="Seg_1576" s="T193">tap</ta>
            <ta e="T195" id="Seg_1577" s="T194">čʼeːlɨ</ta>
            <ta e="T196" id="Seg_1578" s="T195">moqonä</ta>
            <ta e="T197" id="Seg_1579" s="T196">šıntɨ</ta>
            <ta e="T198" id="Seg_1580" s="T197">üːt-ɛntɨ-mɨt</ta>
            <ta e="T199" id="Seg_1581" s="T198">tan</ta>
            <ta e="T200" id="Seg_1582" s="T199">tiː</ta>
            <ta e="T201" id="Seg_1583" s="T200">soma-lʼa-k</ta>
            <ta e="T202" id="Seg_1584" s="T201">ɛː-ŋa-ntɨ</ta>
            <ta e="T203" id="Seg_1585" s="T202">moqonä</ta>
            <ta e="T204" id="Seg_1586" s="T203">qän-ma-l</ta>
            <ta e="T205" id="Seg_1587" s="T204">mɔːt-qɨn</ta>
            <ta e="T206" id="Seg_1588" s="T205">ilʼ-äšʼik</ta>
            <ta e="T207" id="Seg_1589" s="T206">köt</ta>
            <ta e="T208" id="Seg_1590" s="T207">qaj</ta>
            <ta e="T209" id="Seg_1591" s="T208">čʼeːlʼ</ta>
            <ta e="T210" id="Seg_1592" s="T209">mɔːt-qɨn</ta>
            <ta e="T211" id="Seg_1593" s="T210">ilʼ-äšik</ta>
            <ta e="T212" id="Seg_1594" s="T211">uːčʼi-la</ta>
            <ta e="T213" id="Seg_1595" s="T212">ɨkɨ</ta>
            <ta e="T214" id="Seg_1596" s="T213">qän-aš</ta>
            <ta e="T215" id="Seg_1597" s="T214">moqonä</ta>
            <ta e="T216" id="Seg_1598" s="T215">qän-ma-ntɨ</ta>
            <ta e="T217" id="Seg_1599" s="T216">mɔːt-qɨn</ta>
            <ta e="T218" id="Seg_1600" s="T217">ɔːmt-äšʼik</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1601" s="T0">korotkij</ta>
            <ta e="T2" id="Seg_1602" s="T1">otpusk</ta>
            <ta e="T3" id="Seg_1603" s="T2">filʼm</ta>
            <ta e="T4" id="Seg_1604" s="T3">Italʼa-qɨn</ta>
            <ta e="T5" id="Seg_1605" s="T4">mɨ-t-ɨ-lʼ</ta>
            <ta e="T6" id="Seg_1606" s="T5">ɔːmtɨ-lʼ</ta>
            <ta e="T7" id="Seg_1607" s="T6">qok-n</ta>
            <ta e="T8" id="Seg_1608" s="T7">təttɨ-qɨn</ta>
            <ta e="T9" id="Seg_1609" s="T8">ima</ta>
            <ta e="T10" id="Seg_1610" s="T9">ilɨ-mpɨ</ta>
            <ta e="T11" id="Seg_1611" s="T10">ira-sɨma-lʼ</ta>
            <ta e="T12" id="Seg_1612" s="T11">ima-n</ta>
            <ta e="T13" id="Seg_1613" s="T12">mɨ-qɨn</ta>
            <ta e="T14" id="Seg_1614" s="T13">nɔːkɨr</ta>
            <ta e="T15" id="Seg_1615" s="T14">kɨpa-lʼa</ta>
            <ta e="T16" id="Seg_1616" s="T15">iːja-iː-tɨ</ta>
            <ta e="T17" id="Seg_1617" s="T16">ɛː-sɨ</ta>
            <ta e="T18" id="Seg_1618" s="T17">na</ta>
            <ta e="T19" id="Seg_1619" s="T18">ima-n</ta>
            <ta e="T20" id="Seg_1620" s="T19">ira</ta>
            <ta e="T21" id="Seg_1621" s="T20">kos</ta>
            <ta e="T22" id="Seg_1622" s="T21">kun</ta>
            <ta e="T23" id="Seg_1623" s="T22">uːčʼɨ-mpɨ</ta>
            <ta e="T24" id="Seg_1624" s="T23">topɨ-tɨ</ta>
            <ta e="T25" id="Seg_1625" s="T24">səpɨ-tɨ-ɛː-mpɨ-tɨ</ta>
            <ta e="T26" id="Seg_1626" s="T25">topɨ-tɨ</ta>
            <ta e="T27" id="Seg_1627" s="T26">čʼüšɨ-lä</ta>
            <ta e="T28" id="Seg_1628" s="T27">ašša</ta>
            <ta e="T29" id="Seg_1629" s="T28">uːčʼɨ-mpɨ</ta>
            <ta e="T30" id="Seg_1630" s="T29">poː-n-tɨ</ta>
            <ta e="T31" id="Seg_1631" s="T30">kəš</ta>
            <ta e="T32" id="Seg_1632" s="T31">kuntɨ</ta>
            <ta e="T33" id="Seg_1633" s="T32">ima-tɨ</ta>
            <ta e="T34" id="Seg_1634" s="T33">ontɨ-qɨn-ntɨ</ta>
            <ta e="T35" id="Seg_1635" s="T34">uːčʼɨ-mpɨ</ta>
            <ta e="T36" id="Seg_1636" s="T35">a</ta>
            <ta e="T37" id="Seg_1637" s="T36">ira-n-tɨ</ta>
            <ta e="T38" id="Seg_1638" s="T37">timnʼa</ta>
            <ta e="T39" id="Seg_1639" s="T38">ɨŋkalpɨ-lä</ta>
            <ta e="T40" id="Seg_1640" s="T39">mɔːt-qɨn</ta>
            <ta e="T41" id="Seg_1641" s="T40">meːltɨ</ta>
            <ta e="T42" id="Seg_1642" s="T41">ɔːmtɨ-mpɨ</ta>
            <ta e="T43" id="Seg_1643" s="T42">nʼi</ta>
            <ta e="T44" id="Seg_1644" s="T43">kun</ta>
            <ta e="T45" id="Seg_1645" s="T44">nʼi</ta>
            <ta e="T46" id="Seg_1646" s="T45">kun</ta>
            <ta e="T47" id="Seg_1647" s="T46">uːčʼɨ-qo</ta>
            <ta e="T48" id="Seg_1648" s="T47">ašša</ta>
            <ta e="T49" id="Seg_1649" s="T48">kɨkɨ-lä</ta>
            <ta e="T50" id="Seg_1650" s="T49">ima</ta>
            <ta e="T51" id="Seg_1651" s="T50">ontɨ</ta>
            <ta e="T52" id="Seg_1652" s="T51">uːčʼɨ-mpɨ</ta>
            <ta e="T53" id="Seg_1653" s="T52">təp-ɨ-t</ta>
            <ta e="T54" id="Seg_1654" s="T53">ilɨ-mpɨ-tɨt</ta>
            <ta e="T55" id="Seg_1655" s="T54">qaj</ta>
            <ta e="T56" id="Seg_1656" s="T55">muktɨt</ta>
            <ta e="T57" id="Seg_1657" s="T56">qaj</ta>
            <ta e="T58" id="Seg_1658" s="T57">seːlʼčʼɨ</ta>
            <ta e="T59" id="Seg_1659" s="T58">qum</ta>
            <ta e="T60" id="Seg_1660" s="T59">muktɨt</ta>
            <ta e="T61" id="Seg_1661" s="T60">qum</ta>
            <ta e="T62" id="Seg_1662" s="T61">ima</ta>
            <ta e="T63" id="Seg_1663" s="T62">ontɨ-qɨn</ta>
            <ta e="T64" id="Seg_1664" s="T63">uːčʼɨ-mpɨ</ta>
            <ta e="T65" id="Seg_1665" s="T64">meːltɨ</ta>
            <ta e="T66" id="Seg_1666" s="T65">amna-n</ta>
            <ta e="T67" id="Seg_1667" s="T66">ɔːmtɨ-mpɨ-tɨt</ta>
            <ta e="T68" id="Seg_1668" s="T67">ɔːmtɨ-tɨt</ta>
            <ta e="T69" id="Seg_1669" s="T68">qomtä-tɨt</ta>
            <ta e="T70" id="Seg_1670" s="T69">čʼäːŋkɨ-mpɨ</ta>
            <ta e="T71" id="Seg_1671" s="T70">apsɨ-m</ta>
            <ta e="T72" id="Seg_1672" s="T71">iː-pso</ta>
            <ta e="T73" id="Seg_1673" s="T72">nɨːnɨ</ta>
            <ta e="T74" id="Seg_1674" s="T73">ima</ta>
            <ta e="T75" id="Seg_1675" s="T74">qüːtɨ-alʼ-mpɨ</ta>
            <ta e="T76" id="Seg_1676" s="T75">to</ta>
            <ta e="T77" id="Seg_1677" s="T76">qüːtɨ-alʼ-mpɨ</ta>
            <ta e="T78" id="Seg_1678" s="T77">palʼnica-ntɨ</ta>
            <ta e="T79" id="Seg_1679" s="T78">pin-mpɨ-tɨt</ta>
            <ta e="T80" id="Seg_1680" s="T79">təp</ta>
            <ta e="T81" id="Seg_1681" s="T80">palʼnica-qɨn</ta>
            <ta e="T82" id="Seg_1682" s="T81">qaj</ta>
            <ta e="T83" id="Seg_1683" s="T82">irä-n-tɨ</ta>
            <ta e="T84" id="Seg_1684" s="T83">kuntɨ</ta>
            <ta e="T85" id="Seg_1685" s="T84">pat-mpɨ</ta>
            <ta e="T86" id="Seg_1686" s="T85">ippɨ-mpɨ</ta>
            <ta e="T87" id="Seg_1687" s="T86">qaj</ta>
            <ta e="T88" id="Seg_1688" s="T87">irä-n-tɨ</ta>
            <ta e="T89" id="Seg_1689" s="T88">kuntɨ</ta>
            <ta e="T90" id="Seg_1690" s="T89">ippɨ-mpɨ</ta>
            <ta e="T91" id="Seg_1691" s="T90">qaj</ta>
            <ta e="T92" id="Seg_1692" s="T91">šittɨ</ta>
            <ta e="T93" id="Seg_1693" s="T92">irä-n-tɨ</ta>
            <ta e="T94" id="Seg_1694" s="T93">kuntɨ</ta>
            <ta e="T95" id="Seg_1695" s="T94">ippɨ-mpɨ</ta>
            <ta e="T96" id="Seg_1696" s="T95">puː-qo</ta>
            <ta e="T97" id="Seg_1697" s="T96">šünʼčʼɨ-tɨ</ta>
            <ta e="T98" id="Seg_1698" s="T97">čʼüšɨ-qo-olam-mpɨ</ta>
            <ta e="T99" id="Seg_1699" s="T98">na</ta>
            <ta e="T100" id="Seg_1700" s="T99">ima</ta>
            <ta e="T101" id="Seg_1701" s="T100">palʼnica-ntɨ</ta>
            <ta e="T102" id="Seg_1702" s="T101">tü-r-mpɨ-tɨt</ta>
            <ta e="T103" id="Seg_1703" s="T102">ira-n-tɨ</ta>
            <ta e="T104" id="Seg_1704" s="T103">ama</ta>
            <ta e="T105" id="Seg_1705" s="T104">ira-n-tɨ</ta>
            <ta e="T106" id="Seg_1706" s="T105">timnʼa-n-tɨt</ta>
            <ta e="T107" id="Seg_1707" s="T106">əptı-sä</ta>
            <ta e="T108" id="Seg_1708" s="T107">aj</ta>
            <ta e="T109" id="Seg_1709" s="T108">nɔːkɨr</ta>
            <ta e="T110" id="Seg_1710" s="T109">iːja-lʼa-tɨ</ta>
            <ta e="T111" id="Seg_1711" s="T110">kɨpa-lʼa</ta>
            <ta e="T112" id="Seg_1712" s="T111">iːja-ntɨ-sä</ta>
            <ta e="T113" id="Seg_1713" s="T112">moqɨnä</ta>
            <ta e="T114" id="Seg_1714" s="T113">qərɨ-qo</ta>
            <ta e="T115" id="Seg_1715" s="T114">ima</ta>
            <ta e="T116" id="Seg_1716" s="T115">nılʼčʼɨ-k</ta>
            <ta e="T117" id="Seg_1717" s="T116">tom-ŋɨ-tɨ</ta>
            <ta e="T118" id="Seg_1718" s="T117">man</ta>
            <ta e="T119" id="Seg_1719" s="T118">tarɨ</ta>
            <ta e="T120" id="Seg_1720" s="T119">qüːtɨ-k</ta>
            <ta e="T121" id="Seg_1721" s="T120">puː-qo</ta>
            <ta e="T122" id="Seg_1722" s="T121">šünʼčʼɨ-n</ta>
            <ta e="T123" id="Seg_1723" s="T122">tarɨ</ta>
            <ta e="T124" id="Seg_1724" s="T123">čʼüšɨ</ta>
            <ta e="T125" id="Seg_1725" s="T124">čʼüšɨ</ta>
            <ta e="T126" id="Seg_1726" s="T125">čʼeːlɨ</ta>
            <ta e="T127" id="Seg_1727" s="T126">moqɨnä</ta>
            <ta e="T128" id="Seg_1728" s="T127">ašša</ta>
            <ta e="T129" id="Seg_1729" s="T128">qən-ɛntɨ-k</ta>
            <ta e="T130" id="Seg_1730" s="T129">nɨːnɨ</ta>
            <ta e="T131" id="Seg_1731" s="T130">na</ta>
            <ta e="T132" id="Seg_1732" s="T131">ima</ta>
            <ta e="T133" id="Seg_1733" s="T132">palʼnica-qɨn</ta>
            <ta e="T134" id="Seg_1734" s="T133">ima-t-ɨ-nkinı</ta>
            <ta e="T135" id="Seg_1735" s="T134">tantɨ-ɛː-sɨ</ta>
            <ta e="T136" id="Seg_1736" s="T135">soma-k</ta>
            <ta e="T137" id="Seg_1737" s="T136">ilɨ-qo-olam-sɨ</ta>
            <ta e="T138" id="Seg_1738" s="T137">ima-t</ta>
            <ta e="T139" id="Seg_1739" s="T138">təp-ɨ-nkinı</ta>
            <ta e="T140" id="Seg_1740" s="T139">na</ta>
            <ta e="T141" id="Seg_1741" s="T140">ima-nkinı</ta>
            <ta e="T142" id="Seg_1742" s="T141">mi-qɨl-olʼ-ntɨ-mpɨ-qo-olam-sɨ-tɨt</ta>
            <ta e="T143" id="Seg_1743" s="T142">mi-kkɨ-olʼ-ntɨ-mpɨ-qo-olam-sɨ-tɨt</ta>
            <ta e="T144" id="Seg_1744" s="T143">na</ta>
            <ta e="T145" id="Seg_1745" s="T144">ima-nkinı</ta>
            <ta e="T146" id="Seg_1746" s="T145">toːnna</ta>
            <ta e="T147" id="Seg_1747" s="T146">ima-t</ta>
            <ta e="T148" id="Seg_1748" s="T147">na</ta>
            <ta e="T149" id="Seg_1749" s="T148">ima-nkinı</ta>
            <ta e="T150" id="Seg_1750" s="T149">mi-kkɨ-olʼ-olʼ-ntɨ-mpɨ-qo-olam-sɨ-tɨt</ta>
            <ta e="T151" id="Seg_1751" s="T150">qalʼporqɨ</ta>
            <ta e="T152" id="Seg_1752" s="T151">porqɨ</ta>
            <ta e="T153" id="Seg_1753" s="T152">ükɨ</ta>
            <ta e="T154" id="Seg_1754" s="T153">kuššak-naj</ta>
            <ta e="T155" id="Seg_1755" s="T154">nɨːnɨ</ta>
            <ta e="T156" id="Seg_1756" s="T155">na</ta>
            <ta e="T157" id="Seg_1757" s="T156">ima</ta>
            <ta e="T158" id="Seg_1758" s="T157">palʼnica-qɨn</ta>
            <ta e="T159" id="Seg_1759" s="T158">ašša</ta>
            <ta e="T160" id="Seg_1760" s="T159">kuntɨ</ta>
            <ta e="T161" id="Seg_1761" s="T160">ɛː-sɨ</ta>
            <ta e="T162" id="Seg_1762" s="T161">moqɨnä</ta>
            <ta e="T163" id="Seg_1763" s="T162">üːtɨ-sɨ-tɨt</ta>
            <ta e="T164" id="Seg_1764" s="T163">ukoːn</ta>
            <ta e="T165" id="Seg_1765" s="T164">na</ta>
            <ta e="T166" id="Seg_1766" s="T165">ima</ta>
            <ta e="T167" id="Seg_1767" s="T166">nʼi</ta>
            <ta e="T168" id="Seg_1768" s="T167">kučʼčʼä</ta>
            <ta e="T169" id="Seg_1769" s="T168">ašša</ta>
            <ta e="T170" id="Seg_1770" s="T169">qälɨ-ntɨr-sɨ</ta>
            <ta e="T171" id="Seg_1771" s="T170">puːn</ta>
            <ta e="T172" id="Seg_1772" s="T171">soma-lʼa-k</ta>
            <ta e="T173" id="Seg_1773" s="T172">ɛsɨ-lä</ta>
            <ta e="T174" id="Seg_1774" s="T173">qälɨ-ntɨr-qo-olam-sɨ</ta>
            <ta e="T175" id="Seg_1775" s="T174">ponä</ta>
            <ta e="T176" id="Seg_1776" s="T175">paktɨ-lä</ta>
            <ta e="T177" id="Seg_1777" s="T176">kučʼčʼä</ta>
            <ta e="T178" id="Seg_1778" s="T177">popal</ta>
            <ta e="T179" id="Seg_1779" s="T178">*kurɨ-ätɔːl-kkɨ</ta>
            <ta e="T180" id="Seg_1780" s="T179">nɨːnɨ</ta>
            <ta e="T181" id="Seg_1781" s="T180">toktär</ta>
            <ta e="T182" id="Seg_1782" s="T181">ira</ta>
            <ta e="T183" id="Seg_1783" s="T182">nılʼčʼɨ-k</ta>
            <ta e="T184" id="Seg_1784" s="T183">kətɨ-sɨ-tɨ</ta>
            <ta e="T185" id="Seg_1785" s="T184">kuššat</ta>
            <ta e="T186" id="Seg_1786" s="T185">kos</ta>
            <ta e="T187" id="Seg_1787" s="T186">na</ta>
            <ta e="T188" id="Seg_1788" s="T187">čʼeːlɨ</ta>
            <ta e="T189" id="Seg_1789" s="T188">ɛːmä</ta>
            <ta e="T190" id="Seg_1790" s="T189">kos</ta>
            <ta e="T191" id="Seg_1791" s="T190">täːlɨ</ta>
            <ta e="T192" id="Seg_1792" s="T191">čʼeːlɨ</ta>
            <ta e="T193" id="Seg_1793" s="T192">kos</ta>
            <ta e="T194" id="Seg_1794" s="T193">tam</ta>
            <ta e="T195" id="Seg_1795" s="T194">čʼeːlɨ</ta>
            <ta e="T196" id="Seg_1796" s="T195">moqɨnä</ta>
            <ta e="T197" id="Seg_1797" s="T196">tašıntɨ</ta>
            <ta e="T198" id="Seg_1798" s="T197">üːtɨ-ɛntɨ-mɨt</ta>
            <ta e="T199" id="Seg_1799" s="T198">tan</ta>
            <ta e="T200" id="Seg_1800" s="T199">tiː</ta>
            <ta e="T201" id="Seg_1801" s="T200">soma-lʼa-k</ta>
            <ta e="T202" id="Seg_1802" s="T201">ɛː-ŋɨ-ntɨ</ta>
            <ta e="T203" id="Seg_1803" s="T202">moqɨnä</ta>
            <ta e="T204" id="Seg_1804" s="T203">qən-mmä-l</ta>
            <ta e="T205" id="Seg_1805" s="T204">mɔːt-qɨn</ta>
            <ta e="T206" id="Seg_1806" s="T205">ilɨ-äšɨk</ta>
            <ta e="T207" id="Seg_1807" s="T206">köt</ta>
            <ta e="T208" id="Seg_1808" s="T207">qaj</ta>
            <ta e="T209" id="Seg_1809" s="T208">čʼeːlɨ</ta>
            <ta e="T210" id="Seg_1810" s="T209">mɔːt-qɨn</ta>
            <ta e="T211" id="Seg_1811" s="T210">ilɨ-äšɨk</ta>
            <ta e="T212" id="Seg_1812" s="T211">uːčʼɨ-lä</ta>
            <ta e="T213" id="Seg_1813" s="T212">ɨkɨ</ta>
            <ta e="T214" id="Seg_1814" s="T213">qən-äšɨk</ta>
            <ta e="T215" id="Seg_1815" s="T214">moqɨnä</ta>
            <ta e="T216" id="Seg_1816" s="T215">qən-mmä-ntɨ</ta>
            <ta e="T217" id="Seg_1817" s="T216">mɔːt-qɨn</ta>
            <ta e="T218" id="Seg_1818" s="T217">ɔːmtɨ-äšɨk</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1819" s="T0">short</ta>
            <ta e="T2" id="Seg_1820" s="T1">vacation.[NOM]</ta>
            <ta e="T3" id="Seg_1821" s="T2">film.[NOM]</ta>
            <ta e="T4" id="Seg_1822" s="T3">Italy-LOC</ta>
            <ta e="T5" id="Seg_1823" s="T4">something-PL-EP-ADJZ</ta>
            <ta e="T6" id="Seg_1824" s="T5">horn-ADJZ</ta>
            <ta e="T7" id="Seg_1825" s="T6">head-GEN</ta>
            <ta e="T8" id="Seg_1826" s="T7">earth-LOC</ta>
            <ta e="T9" id="Seg_1827" s="T8">woman.[NOM]</ta>
            <ta e="T10" id="Seg_1828" s="T9">live-PST.NAR.[3SG.S]</ta>
            <ta e="T11" id="Seg_1829" s="T10">husband-PROPR-ADJZ</ta>
            <ta e="T12" id="Seg_1830" s="T11">woman-GEN</ta>
            <ta e="T13" id="Seg_1831" s="T12">something-LOC</ta>
            <ta e="T14" id="Seg_1832" s="T13">three</ta>
            <ta e="T15" id="Seg_1833" s="T14">small-DIM</ta>
            <ta e="T16" id="Seg_1834" s="T15">child-PL.[NOM]-3SG</ta>
            <ta e="T17" id="Seg_1835" s="T16">be-PST.[3SG.S]</ta>
            <ta e="T18" id="Seg_1836" s="T17">this</ta>
            <ta e="T19" id="Seg_1837" s="T18">woman-GEN</ta>
            <ta e="T20" id="Seg_1838" s="T19">husband.[NOM]</ta>
            <ta e="T21" id="Seg_1839" s="T20">INDEF3</ta>
            <ta e="T22" id="Seg_1840" s="T21">where</ta>
            <ta e="T23" id="Seg_1841" s="T22">work-PST.NAR.[3SG.S]</ta>
            <ta e="T24" id="Seg_1842" s="T23">leg.[NOM]-3SG</ta>
            <ta e="T25" id="Seg_1843" s="T24">break-TR-PFV-PST.NAR-3SG.O</ta>
            <ta e="T26" id="Seg_1844" s="T25">leg.[NOM]-3SG</ta>
            <ta e="T27" id="Seg_1845" s="T26">be.sick-CVB</ta>
            <ta e="T28" id="Seg_1846" s="T27">NEG</ta>
            <ta e="T29" id="Seg_1847" s="T28">work-PST.NAR.[3SG.S]</ta>
            <ta e="T30" id="Seg_1848" s="T29">year-GEN-3SG</ta>
            <ta e="T31" id="Seg_1849" s="T30">half.[NOM]</ta>
            <ta e="T32" id="Seg_1850" s="T31">during</ta>
            <ta e="T33" id="Seg_1851" s="T32">woman.[NOM]-3SG</ta>
            <ta e="T34" id="Seg_1852" s="T33">oneself.3SG-LOC-OBL.3SG</ta>
            <ta e="T35" id="Seg_1853" s="T34">work-PST.NAR.[3SG.S]</ta>
            <ta e="T36" id="Seg_1854" s="T35">and</ta>
            <ta e="T37" id="Seg_1855" s="T36">husband-GEN-3SG</ta>
            <ta e="T38" id="Seg_1856" s="T37">brother.[NOM]</ta>
            <ta e="T39" id="Seg_1857" s="T38">idle-CVB</ta>
            <ta e="T40" id="Seg_1858" s="T39">house-LOC</ta>
            <ta e="T41" id="Seg_1859" s="T40">always</ta>
            <ta e="T42" id="Seg_1860" s="T41">sit-PST.NAR.[3SG.S]</ta>
            <ta e="T43" id="Seg_1861" s="T42">NEG</ta>
            <ta e="T44" id="Seg_1862" s="T43">where</ta>
            <ta e="T45" id="Seg_1863" s="T44">NEG</ta>
            <ta e="T46" id="Seg_1864" s="T45">where</ta>
            <ta e="T47" id="Seg_1865" s="T46">work-INF</ta>
            <ta e="T48" id="Seg_1866" s="T47">NEG</ta>
            <ta e="T49" id="Seg_1867" s="T48">want-CVB</ta>
            <ta e="T50" id="Seg_1868" s="T49">woman.[NOM]</ta>
            <ta e="T51" id="Seg_1869" s="T50">oneself.3SG.[NOM]</ta>
            <ta e="T52" id="Seg_1870" s="T51">work-PST.NAR.[3SG.S]</ta>
            <ta e="T53" id="Seg_1871" s="T52">(s)he-EP-PL.[NOM]</ta>
            <ta e="T54" id="Seg_1872" s="T53">live-PST.NAR-3PL</ta>
            <ta e="T55" id="Seg_1873" s="T54">either.or.[NOM]</ta>
            <ta e="T56" id="Seg_1874" s="T55">six</ta>
            <ta e="T57" id="Seg_1875" s="T56">either.or.[NOM]</ta>
            <ta e="T58" id="Seg_1876" s="T57">seven</ta>
            <ta e="T59" id="Seg_1877" s="T58">human.being.[NOM]</ta>
            <ta e="T60" id="Seg_1878" s="T59">six</ta>
            <ta e="T61" id="Seg_1879" s="T60">human.being.[NOM]</ta>
            <ta e="T62" id="Seg_1880" s="T61">woman.[NOM]</ta>
            <ta e="T63" id="Seg_1881" s="T62">oneself.3SG-LOC</ta>
            <ta e="T64" id="Seg_1882" s="T63">work-PST.NAR.[3SG.S]</ta>
            <ta e="T65" id="Seg_1883" s="T64">always</ta>
            <ta e="T66" id="Seg_1884" s="T65">hunger-GEN</ta>
            <ta e="T67" id="Seg_1885" s="T66">sit-PST.NAR-3PL</ta>
            <ta e="T68" id="Seg_1886" s="T67">sit-3PL</ta>
            <ta e="T69" id="Seg_1887" s="T68">money.[NOM]-3PL</ta>
            <ta e="T70" id="Seg_1888" s="T69">NEG.EX-PST.NAR.[3SG.S]</ta>
            <ta e="T71" id="Seg_1889" s="T70">food-ACC</ta>
            <ta e="T72" id="Seg_1890" s="T71">take-PTCP.NEC</ta>
            <ta e="T73" id="Seg_1891" s="T72">then</ta>
            <ta e="T74" id="Seg_1892" s="T73">woman.[NOM]</ta>
            <ta e="T75" id="Seg_1893" s="T74">be.sick-INCH-PST.NAR.[3SG.S]</ta>
            <ta e="T76" id="Seg_1894" s="T75">that</ta>
            <ta e="T77" id="Seg_1895" s="T76">be.sick-INCH-PST.NAR.[3SG.S]</ta>
            <ta e="T78" id="Seg_1896" s="T77">hospital-ILL</ta>
            <ta e="T79" id="Seg_1897" s="T78">put-PST.NAR-3PL</ta>
            <ta e="T80" id="Seg_1898" s="T79">(s)he.[NOM]</ta>
            <ta e="T81" id="Seg_1899" s="T80">hospital-LOC</ta>
            <ta e="T82" id="Seg_1900" s="T81">either.or</ta>
            <ta e="T83" id="Seg_1901" s="T82">month-GEN-3SG</ta>
            <ta e="T84" id="Seg_1902" s="T83">during</ta>
            <ta e="T85" id="Seg_1903" s="T84">get.into-PST.NAR.[3SG.S]</ta>
            <ta e="T86" id="Seg_1904" s="T85">lie-PST.NAR.[3SG.S]</ta>
            <ta e="T87" id="Seg_1905" s="T86">either.or</ta>
            <ta e="T88" id="Seg_1906" s="T87">month-GEN-3SG</ta>
            <ta e="T89" id="Seg_1907" s="T88">during</ta>
            <ta e="T90" id="Seg_1908" s="T89">lie-PST.NAR.[3SG.S]</ta>
            <ta e="T91" id="Seg_1909" s="T90">either.or</ta>
            <ta e="T92" id="Seg_1910" s="T91">two</ta>
            <ta e="T93" id="Seg_1911" s="T92">month-GEN-3SG</ta>
            <ta e="T94" id="Seg_1912" s="T93">during</ta>
            <ta e="T95" id="Seg_1913" s="T94">lie-PST.NAR.[3SG.S]</ta>
            <ta e="T96" id="Seg_1914" s="T95">blow-INF</ta>
            <ta e="T97" id="Seg_1915" s="T96">inside.[NOM]-3SG</ta>
            <ta e="T98" id="Seg_1916" s="T97">be.sick-INF-begin-PST.NAR.[3SG.S]</ta>
            <ta e="T99" id="Seg_1917" s="T98">this</ta>
            <ta e="T100" id="Seg_1918" s="T99">woman.[NOM]</ta>
            <ta e="T101" id="Seg_1919" s="T100">hospital-ILL</ta>
            <ta e="T102" id="Seg_1920" s="T101">come-FRQ-PST.NAR-3PL</ta>
            <ta e="T103" id="Seg_1921" s="T102">husband-GEN-3SG</ta>
            <ta e="T104" id="Seg_1922" s="T103">mother.[NOM]</ta>
            <ta e="T105" id="Seg_1923" s="T104">husband-GEN-3SG</ta>
            <ta e="T106" id="Seg_1924" s="T105">brother-GEN-3PL</ta>
            <ta e="T107" id="Seg_1925" s="T106">with-INSTR</ta>
            <ta e="T108" id="Seg_1926" s="T107">and</ta>
            <ta e="T109" id="Seg_1927" s="T108">three</ta>
            <ta e="T110" id="Seg_1928" s="T109">child-DIM.[NOM]-3SG</ta>
            <ta e="T111" id="Seg_1929" s="T110">small-DIM</ta>
            <ta e="T112" id="Seg_1930" s="T111">child-OBL.3SG-COM</ta>
            <ta e="T113" id="Seg_1931" s="T112">home</ta>
            <ta e="T114" id="Seg_1932" s="T113">call-INF</ta>
            <ta e="T115" id="Seg_1933" s="T114">woman.[NOM]</ta>
            <ta e="T116" id="Seg_1934" s="T115">such-ADVZ</ta>
            <ta e="T117" id="Seg_1935" s="T116">speak-CO-3SG.O</ta>
            <ta e="T118" id="Seg_1936" s="T117">I.NOM</ta>
            <ta e="T119" id="Seg_1937" s="T118">still</ta>
            <ta e="T120" id="Seg_1938" s="T119">be.sick-1SG.S</ta>
            <ta e="T121" id="Seg_1939" s="T120">blow-INF</ta>
            <ta e="T122" id="Seg_1940" s="T121">inside-ADV.LOC</ta>
            <ta e="T123" id="Seg_1941" s="T122">still</ta>
            <ta e="T124" id="Seg_1942" s="T123">be.sick.[3SG.S]</ta>
            <ta e="T125" id="Seg_1943" s="T124">be.sick.[3SG.S]</ta>
            <ta e="T126" id="Seg_1944" s="T125">day.[NOM]</ta>
            <ta e="T127" id="Seg_1945" s="T126">home</ta>
            <ta e="T128" id="Seg_1946" s="T127">NEG</ta>
            <ta e="T129" id="Seg_1947" s="T128">leave-FUT-1SG.S</ta>
            <ta e="T130" id="Seg_1948" s="T129">then</ta>
            <ta e="T131" id="Seg_1949" s="T130">this</ta>
            <ta e="T132" id="Seg_1950" s="T131">woman.[NOM]</ta>
            <ta e="T133" id="Seg_1951" s="T132">hospital-LOC</ta>
            <ta e="T134" id="Seg_1952" s="T133">woman-PL-EP-ALL</ta>
            <ta e="T135" id="Seg_1953" s="T134">get.used-PFV-PST.[3SG.S]</ta>
            <ta e="T136" id="Seg_1954" s="T135">good-ADVZ</ta>
            <ta e="T137" id="Seg_1955" s="T136">live-INF-begin-PST.[3SG.S]</ta>
            <ta e="T138" id="Seg_1956" s="T137">woman-PL.[NOM]</ta>
            <ta e="T139" id="Seg_1957" s="T138">(s)he-EP-ALL</ta>
            <ta e="T140" id="Seg_1958" s="T139">this</ta>
            <ta e="T141" id="Seg_1959" s="T140">woman-ALL</ta>
            <ta e="T142" id="Seg_1960" s="T141">give-MULO-FRQ-IPFV-DUR-INF-begin-PST-3PL</ta>
            <ta e="T143" id="Seg_1961" s="T142">give-HAB-FRQ-IPFV-DUR-INF-begin-PST-3PL</ta>
            <ta e="T144" id="Seg_1962" s="T143">this</ta>
            <ta e="T145" id="Seg_1963" s="T144">woman-ALL</ta>
            <ta e="T146" id="Seg_1964" s="T145">that</ta>
            <ta e="T147" id="Seg_1965" s="T146">woman-PL.[NOM]</ta>
            <ta e="T148" id="Seg_1966" s="T147">this</ta>
            <ta e="T149" id="Seg_1967" s="T148">woman-ALL</ta>
            <ta e="T150" id="Seg_1968" s="T149">give-HAB-FRQ-FRQ-IPFV-DUR-INF-begin-PST-3PL</ta>
            <ta e="T151" id="Seg_1969" s="T150">dress.[NOM]</ta>
            <ta e="T152" id="Seg_1970" s="T151">clothing.[NOM]</ta>
            <ta e="T153" id="Seg_1971" s="T152">cap.[NOM]</ta>
            <ta e="T154" id="Seg_1972" s="T153">how.many-EMPH</ta>
            <ta e="T155" id="Seg_1973" s="T154">then</ta>
            <ta e="T156" id="Seg_1974" s="T155">this</ta>
            <ta e="T157" id="Seg_1975" s="T156">woman.[NOM]</ta>
            <ta e="T158" id="Seg_1976" s="T157">hospital-LOC</ta>
            <ta e="T159" id="Seg_1977" s="T158">NEG</ta>
            <ta e="T160" id="Seg_1978" s="T159">long</ta>
            <ta e="T161" id="Seg_1979" s="T160">be-PST.[3SG.S]</ta>
            <ta e="T162" id="Seg_1980" s="T161">home</ta>
            <ta e="T163" id="Seg_1981" s="T162">sent-PST-3PL</ta>
            <ta e="T164" id="Seg_1982" s="T163">earlier</ta>
            <ta e="T165" id="Seg_1983" s="T164">this</ta>
            <ta e="T166" id="Seg_1984" s="T165">woman.[NOM]</ta>
            <ta e="T167" id="Seg_1985" s="T166">NEG</ta>
            <ta e="T168" id="Seg_1986" s="T167">where</ta>
            <ta e="T169" id="Seg_1987" s="T168">NEG</ta>
            <ta e="T170" id="Seg_1988" s="T169">go-DRV-PST.[3SG.S]</ta>
            <ta e="T171" id="Seg_1989" s="T170">afterwards</ta>
            <ta e="T172" id="Seg_1990" s="T171">good-DIM-ADVZ</ta>
            <ta e="T173" id="Seg_1991" s="T172">become-CVB</ta>
            <ta e="T174" id="Seg_1992" s="T173">go-DRV-INF-begin-PST.[3SG.S]</ta>
            <ta e="T175" id="Seg_1993" s="T174">outwards</ta>
            <ta e="T176" id="Seg_1994" s="T175">run-CVB</ta>
            <ta e="T177" id="Seg_1995" s="T176">where</ta>
            <ta e="T178" id="Seg_1996" s="T177">INDEF5</ta>
            <ta e="T179" id="Seg_1997" s="T178">go-MOM-HAB.[3SG.S]</ta>
            <ta e="T180" id="Seg_1998" s="T179">then</ta>
            <ta e="T181" id="Seg_1999" s="T180">doctor.[NOM]</ta>
            <ta e="T182" id="Seg_2000" s="T181">old.man.[NOM]</ta>
            <ta e="T183" id="Seg_2001" s="T182">such-ADVZ</ta>
            <ta e="T184" id="Seg_2002" s="T183">say-PST-3SG.O</ta>
            <ta e="T185" id="Seg_2003" s="T184">when</ta>
            <ta e="T186" id="Seg_2004" s="T185">INDEF3</ta>
            <ta e="T187" id="Seg_2005" s="T186">this</ta>
            <ta e="T188" id="Seg_2006" s="T187">day.[NOM]</ta>
            <ta e="T189" id="Seg_2007" s="T188">some_</ta>
            <ta e="T190" id="Seg_2008" s="T189">INDEF3</ta>
            <ta e="T191" id="Seg_2009" s="T190">tomorrow</ta>
            <ta e="T192" id="Seg_2010" s="T191">day.[NOM]</ta>
            <ta e="T193" id="Seg_2011" s="T192">INDEF3</ta>
            <ta e="T194" id="Seg_2012" s="T193">this</ta>
            <ta e="T195" id="Seg_2013" s="T194">day.[NOM]</ta>
            <ta e="T196" id="Seg_2014" s="T195">home</ta>
            <ta e="T197" id="Seg_2015" s="T196">you.SG.ACC</ta>
            <ta e="T198" id="Seg_2016" s="T197">sent-FUT-1PL</ta>
            <ta e="T199" id="Seg_2017" s="T198">you.SG.NOM</ta>
            <ta e="T200" id="Seg_2018" s="T199">now</ta>
            <ta e="T201" id="Seg_2019" s="T200">good-DIM-ADVZ</ta>
            <ta e="T202" id="Seg_2020" s="T201">be-CO-2SG.S</ta>
            <ta e="T203" id="Seg_2021" s="T202">home</ta>
            <ta e="T204" id="Seg_2022" s="T203">leave-COND-2SG.O</ta>
            <ta e="T205" id="Seg_2023" s="T204">house-LOC</ta>
            <ta e="T206" id="Seg_2024" s="T205">live-IMP.2SG.S</ta>
            <ta e="T207" id="Seg_2025" s="T206">ten</ta>
            <ta e="T208" id="Seg_2026" s="T207">whether</ta>
            <ta e="T209" id="Seg_2027" s="T208">day.[NOM]</ta>
            <ta e="T210" id="Seg_2028" s="T209">house-LOC</ta>
            <ta e="T211" id="Seg_2029" s="T210">live-IMP.2SG.S</ta>
            <ta e="T212" id="Seg_2030" s="T211">work-CVB</ta>
            <ta e="T213" id="Seg_2031" s="T212">NEG.IMP</ta>
            <ta e="T214" id="Seg_2032" s="T213">leave-IMP.2SG.S</ta>
            <ta e="T215" id="Seg_2033" s="T214">home</ta>
            <ta e="T216" id="Seg_2034" s="T215">leave-COND-2SG.S</ta>
            <ta e="T217" id="Seg_2035" s="T216">house-LOC</ta>
            <ta e="T218" id="Seg_2036" s="T217">sit-IMP.2SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2037" s="T0">короткий</ta>
            <ta e="T2" id="Seg_2038" s="T1">отпуск.[NOM]</ta>
            <ta e="T3" id="Seg_2039" s="T2">фильм.[NOM]</ta>
            <ta e="T4" id="Seg_2040" s="T3">Италия-LOC</ta>
            <ta e="T5" id="Seg_2041" s="T4">нечто-PL-EP-ADJZ</ta>
            <ta e="T6" id="Seg_2042" s="T5">рог-ADJZ</ta>
            <ta e="T7" id="Seg_2043" s="T6">начальник-GEN</ta>
            <ta e="T8" id="Seg_2044" s="T7">земля-LOC</ta>
            <ta e="T9" id="Seg_2045" s="T8">женщина.[NOM]</ta>
            <ta e="T10" id="Seg_2046" s="T9">жить-PST.NAR.[3SG.S]</ta>
            <ta e="T11" id="Seg_2047" s="T10">муж-PROPR-ADJZ</ta>
            <ta e="T12" id="Seg_2048" s="T11">женщина-GEN</ta>
            <ta e="T13" id="Seg_2049" s="T12">нечто-LOC</ta>
            <ta e="T14" id="Seg_2050" s="T13">три</ta>
            <ta e="T15" id="Seg_2051" s="T14">маленький-DIM</ta>
            <ta e="T16" id="Seg_2052" s="T15">ребенок-PL.[NOM]-3SG</ta>
            <ta e="T17" id="Seg_2053" s="T16">быть-PST.[3SG.S]</ta>
            <ta e="T18" id="Seg_2054" s="T17">этот</ta>
            <ta e="T19" id="Seg_2055" s="T18">женщина-GEN</ta>
            <ta e="T20" id="Seg_2056" s="T19">муж.[NOM]</ta>
            <ta e="T21" id="Seg_2057" s="T20">INDEF3</ta>
            <ta e="T22" id="Seg_2058" s="T21">где</ta>
            <ta e="T23" id="Seg_2059" s="T22">работать-PST.NAR.[3SG.S]</ta>
            <ta e="T24" id="Seg_2060" s="T23">нога.[NOM]-3SG</ta>
            <ta e="T25" id="Seg_2061" s="T24">ломать-TR-PFV-PST.NAR-3SG.O</ta>
            <ta e="T26" id="Seg_2062" s="T25">нога.[NOM]-3SG</ta>
            <ta e="T27" id="Seg_2063" s="T26">болеть-CVB</ta>
            <ta e="T28" id="Seg_2064" s="T27">NEG</ta>
            <ta e="T29" id="Seg_2065" s="T28">работать-PST.NAR.[3SG.S]</ta>
            <ta e="T30" id="Seg_2066" s="T29">год-GEN-3SG</ta>
            <ta e="T31" id="Seg_2067" s="T30">половина.[NOM]</ta>
            <ta e="T32" id="Seg_2068" s="T31">в.течение</ta>
            <ta e="T33" id="Seg_2069" s="T32">женщина.[NOM]-3SG</ta>
            <ta e="T34" id="Seg_2070" s="T33">сам.3SG-LOC-OBL.3SG</ta>
            <ta e="T35" id="Seg_2071" s="T34">работать-PST.NAR.[3SG.S]</ta>
            <ta e="T36" id="Seg_2072" s="T35">а</ta>
            <ta e="T37" id="Seg_2073" s="T36">муж-GEN-3SG</ta>
            <ta e="T38" id="Seg_2074" s="T37">брат.[NOM]</ta>
            <ta e="T39" id="Seg_2075" s="T38">бездельничать-CVB</ta>
            <ta e="T40" id="Seg_2076" s="T39">дом-LOC</ta>
            <ta e="T41" id="Seg_2077" s="T40">всегда</ta>
            <ta e="T42" id="Seg_2078" s="T41">сидеть-PST.NAR.[3SG.S]</ta>
            <ta e="T43" id="Seg_2079" s="T42">NEG</ta>
            <ta e="T44" id="Seg_2080" s="T43">где</ta>
            <ta e="T45" id="Seg_2081" s="T44">NEG</ta>
            <ta e="T46" id="Seg_2082" s="T45">где</ta>
            <ta e="T47" id="Seg_2083" s="T46">работать-INF</ta>
            <ta e="T48" id="Seg_2084" s="T47">NEG</ta>
            <ta e="T49" id="Seg_2085" s="T48">хотеть-CVB</ta>
            <ta e="T50" id="Seg_2086" s="T49">женщина.[NOM]</ta>
            <ta e="T51" id="Seg_2087" s="T50">сам.3SG.[NOM]</ta>
            <ta e="T52" id="Seg_2088" s="T51">работать-PST.NAR.[3SG.S]</ta>
            <ta e="T53" id="Seg_2089" s="T52">он(а)-EP-PL.[NOM]</ta>
            <ta e="T54" id="Seg_2090" s="T53">жить-PST.NAR-3PL</ta>
            <ta e="T55" id="Seg_2091" s="T54">то.ли.[NOM]</ta>
            <ta e="T56" id="Seg_2092" s="T55">шесть</ta>
            <ta e="T57" id="Seg_2093" s="T56">то.ли.[NOM]</ta>
            <ta e="T58" id="Seg_2094" s="T57">семь</ta>
            <ta e="T59" id="Seg_2095" s="T58">человек.[NOM]</ta>
            <ta e="T60" id="Seg_2096" s="T59">шесть</ta>
            <ta e="T61" id="Seg_2097" s="T60">человек.[NOM]</ta>
            <ta e="T62" id="Seg_2098" s="T61">женщина.[NOM]</ta>
            <ta e="T63" id="Seg_2099" s="T62">сам.3SG-LOC</ta>
            <ta e="T64" id="Seg_2100" s="T63">работать-PST.NAR.[3SG.S]</ta>
            <ta e="T65" id="Seg_2101" s="T64">всегда</ta>
            <ta e="T66" id="Seg_2102" s="T65">голод-GEN</ta>
            <ta e="T67" id="Seg_2103" s="T66">сидеть-PST.NAR-3PL</ta>
            <ta e="T68" id="Seg_2104" s="T67">сидеть-3PL</ta>
            <ta e="T69" id="Seg_2105" s="T68">деньги.[NOM]-3PL</ta>
            <ta e="T70" id="Seg_2106" s="T69">NEG.EX-PST.NAR.[3SG.S]</ta>
            <ta e="T71" id="Seg_2107" s="T70">еда-ACC</ta>
            <ta e="T72" id="Seg_2108" s="T71">взять-PTCP.NEC</ta>
            <ta e="T73" id="Seg_2109" s="T72">потом</ta>
            <ta e="T74" id="Seg_2110" s="T73">женщина.[NOM]</ta>
            <ta e="T75" id="Seg_2111" s="T74">болеть-INCH-PST.NAR.[3SG.S]</ta>
            <ta e="T76" id="Seg_2112" s="T75">тот</ta>
            <ta e="T77" id="Seg_2113" s="T76">болеть-INCH-PST.NAR.[3SG.S]</ta>
            <ta e="T78" id="Seg_2114" s="T77">больница-ILL</ta>
            <ta e="T79" id="Seg_2115" s="T78">положить-PST.NAR-3PL</ta>
            <ta e="T80" id="Seg_2116" s="T79">он(а).[NOM]</ta>
            <ta e="T81" id="Seg_2117" s="T80">больница-LOC</ta>
            <ta e="T82" id="Seg_2118" s="T81">то.ли</ta>
            <ta e="T83" id="Seg_2119" s="T82">месяц-GEN-3SG</ta>
            <ta e="T84" id="Seg_2120" s="T83">в.течение</ta>
            <ta e="T85" id="Seg_2121" s="T84">попасть-PST.NAR.[3SG.S]</ta>
            <ta e="T86" id="Seg_2122" s="T85">лежать-PST.NAR.[3SG.S]</ta>
            <ta e="T87" id="Seg_2123" s="T86">то.ли</ta>
            <ta e="T88" id="Seg_2124" s="T87">месяц-GEN-3SG</ta>
            <ta e="T89" id="Seg_2125" s="T88">в.течение</ta>
            <ta e="T90" id="Seg_2126" s="T89">лежать-PST.NAR.[3SG.S]</ta>
            <ta e="T91" id="Seg_2127" s="T90">то.ли</ta>
            <ta e="T92" id="Seg_2128" s="T91">два</ta>
            <ta e="T93" id="Seg_2129" s="T92">месяц-GEN-3SG</ta>
            <ta e="T94" id="Seg_2130" s="T93">в.течение</ta>
            <ta e="T95" id="Seg_2131" s="T94">лежать-PST.NAR.[3SG.S]</ta>
            <ta e="T96" id="Seg_2132" s="T95">дуть-INF</ta>
            <ta e="T97" id="Seg_2133" s="T96">нутро.[NOM]-3SG</ta>
            <ta e="T98" id="Seg_2134" s="T97">болеть-INF-начать-PST.NAR.[3SG.S]</ta>
            <ta e="T99" id="Seg_2135" s="T98">этот</ta>
            <ta e="T100" id="Seg_2136" s="T99">женщина.[NOM]</ta>
            <ta e="T101" id="Seg_2137" s="T100">больница-ILL</ta>
            <ta e="T102" id="Seg_2138" s="T101">прийти-FRQ-PST.NAR-3PL</ta>
            <ta e="T103" id="Seg_2139" s="T102">муж-GEN-3SG</ta>
            <ta e="T104" id="Seg_2140" s="T103">мать.[NOM]</ta>
            <ta e="T105" id="Seg_2141" s="T104">муж-GEN-3SG</ta>
            <ta e="T106" id="Seg_2142" s="T105">брат-GEN-3PL</ta>
            <ta e="T107" id="Seg_2143" s="T106">с-INSTR</ta>
            <ta e="T108" id="Seg_2144" s="T107">и</ta>
            <ta e="T109" id="Seg_2145" s="T108">три</ta>
            <ta e="T110" id="Seg_2146" s="T109">ребенок-DIM.[NOM]-3SG</ta>
            <ta e="T111" id="Seg_2147" s="T110">маленький-DIM</ta>
            <ta e="T112" id="Seg_2148" s="T111">ребенок-OBL.3SG-COM</ta>
            <ta e="T113" id="Seg_2149" s="T112">домой</ta>
            <ta e="T114" id="Seg_2150" s="T113">звать-INF</ta>
            <ta e="T115" id="Seg_2151" s="T114">женщина.[NOM]</ta>
            <ta e="T116" id="Seg_2152" s="T115">такой-ADVZ</ta>
            <ta e="T117" id="Seg_2153" s="T116">сказать-CO-3SG.O</ta>
            <ta e="T118" id="Seg_2154" s="T117">я.NOM</ta>
            <ta e="T119" id="Seg_2155" s="T118">пока</ta>
            <ta e="T120" id="Seg_2156" s="T119">болеть-1SG.S</ta>
            <ta e="T121" id="Seg_2157" s="T120">дуть-INF</ta>
            <ta e="T122" id="Seg_2158" s="T121">нутро-ADV.LOC</ta>
            <ta e="T123" id="Seg_2159" s="T122">пока</ta>
            <ta e="T124" id="Seg_2160" s="T123">болеть.[3SG.S]</ta>
            <ta e="T125" id="Seg_2161" s="T124">болеть.[3SG.S]</ta>
            <ta e="T126" id="Seg_2162" s="T125">день.[NOM]</ta>
            <ta e="T127" id="Seg_2163" s="T126">домой</ta>
            <ta e="T128" id="Seg_2164" s="T127">NEG</ta>
            <ta e="T129" id="Seg_2165" s="T128">отправиться-FUT-1SG.S</ta>
            <ta e="T130" id="Seg_2166" s="T129">потом</ta>
            <ta e="T131" id="Seg_2167" s="T130">этот</ta>
            <ta e="T132" id="Seg_2168" s="T131">женщина.[NOM]</ta>
            <ta e="T133" id="Seg_2169" s="T132">больница-LOC</ta>
            <ta e="T134" id="Seg_2170" s="T133">женщина-PL-EP-ALL</ta>
            <ta e="T135" id="Seg_2171" s="T134">привыкать-PFV-PST.[3SG.S]</ta>
            <ta e="T136" id="Seg_2172" s="T135">хороший-ADVZ</ta>
            <ta e="T137" id="Seg_2173" s="T136">жить-INF-начать-PST.[3SG.S]</ta>
            <ta e="T138" id="Seg_2174" s="T137">женщина-PL.[NOM]</ta>
            <ta e="T139" id="Seg_2175" s="T138">он(а)-EP-ALL</ta>
            <ta e="T140" id="Seg_2176" s="T139">этот</ta>
            <ta e="T141" id="Seg_2177" s="T140">женщина-ALL</ta>
            <ta e="T142" id="Seg_2178" s="T141">дать-MULO-FRQ-IPFV-DUR-INF-начать-PST-3PL</ta>
            <ta e="T143" id="Seg_2179" s="T142">дать-HAB-FRQ-IPFV-DUR-INF-начать-PST-3PL</ta>
            <ta e="T144" id="Seg_2180" s="T143">этот</ta>
            <ta e="T145" id="Seg_2181" s="T144">женщина-ALL</ta>
            <ta e="T146" id="Seg_2182" s="T145">тот</ta>
            <ta e="T147" id="Seg_2183" s="T146">женщина-PL.[NOM]</ta>
            <ta e="T148" id="Seg_2184" s="T147">этот</ta>
            <ta e="T149" id="Seg_2185" s="T148">женщина-ALL</ta>
            <ta e="T150" id="Seg_2186" s="T149">дать-HAB-FRQ-FRQ-IPFV-DUR-INF-начать-PST-3PL</ta>
            <ta e="T151" id="Seg_2187" s="T150">платье.[NOM]</ta>
            <ta e="T152" id="Seg_2188" s="T151">одежда.[NOM]</ta>
            <ta e="T153" id="Seg_2189" s="T152">шапка.[NOM]</ta>
            <ta e="T154" id="Seg_2190" s="T153">сколько-EMPH</ta>
            <ta e="T155" id="Seg_2191" s="T154">потом</ta>
            <ta e="T156" id="Seg_2192" s="T155">этот</ta>
            <ta e="T157" id="Seg_2193" s="T156">женщина.[NOM]</ta>
            <ta e="T158" id="Seg_2194" s="T157">больница-LOC</ta>
            <ta e="T159" id="Seg_2195" s="T158">NEG</ta>
            <ta e="T160" id="Seg_2196" s="T159">долго</ta>
            <ta e="T161" id="Seg_2197" s="T160">быть-PST.[3SG.S]</ta>
            <ta e="T162" id="Seg_2198" s="T161">домой</ta>
            <ta e="T163" id="Seg_2199" s="T162">послать-PST-3PL</ta>
            <ta e="T164" id="Seg_2200" s="T163">раньше</ta>
            <ta e="T165" id="Seg_2201" s="T164">этот</ta>
            <ta e="T166" id="Seg_2202" s="T165">женщина.[NOM]</ta>
            <ta e="T167" id="Seg_2203" s="T166">NEG</ta>
            <ta e="T168" id="Seg_2204" s="T167">куда</ta>
            <ta e="T169" id="Seg_2205" s="T168">NEG</ta>
            <ta e="T170" id="Seg_2206" s="T169">ходить-DRV-PST.[3SG.S]</ta>
            <ta e="T171" id="Seg_2207" s="T170">потом</ta>
            <ta e="T172" id="Seg_2208" s="T171">хороший-DIM-ADVZ</ta>
            <ta e="T173" id="Seg_2209" s="T172">стать-CVB</ta>
            <ta e="T174" id="Seg_2210" s="T173">ходить-DRV-INF-начать-PST.[3SG.S]</ta>
            <ta e="T175" id="Seg_2211" s="T174">наружу</ta>
            <ta e="T176" id="Seg_2212" s="T175">побежать-CVB</ta>
            <ta e="T177" id="Seg_2213" s="T176">куда</ta>
            <ta e="T178" id="Seg_2214" s="T177">INDEF5</ta>
            <ta e="T179" id="Seg_2215" s="T178">идти-MOM-HAB.[3SG.S]</ta>
            <ta e="T180" id="Seg_2216" s="T179">потом</ta>
            <ta e="T181" id="Seg_2217" s="T180">доктор.[NOM]</ta>
            <ta e="T182" id="Seg_2218" s="T181">старик.[NOM]</ta>
            <ta e="T183" id="Seg_2219" s="T182">такой-ADVZ</ta>
            <ta e="T184" id="Seg_2220" s="T183">сказать-PST-3SG.O</ta>
            <ta e="T185" id="Seg_2221" s="T184">когда</ta>
            <ta e="T186" id="Seg_2222" s="T185">INDEF3</ta>
            <ta e="T187" id="Seg_2223" s="T186">этот</ta>
            <ta e="T188" id="Seg_2224" s="T187">день.[NOM]</ta>
            <ta e="T189" id="Seg_2225" s="T188">_либо</ta>
            <ta e="T190" id="Seg_2226" s="T189">INDEF3</ta>
            <ta e="T191" id="Seg_2227" s="T190">завтра</ta>
            <ta e="T192" id="Seg_2228" s="T191">день.[NOM]</ta>
            <ta e="T193" id="Seg_2229" s="T192">INDEF3</ta>
            <ta e="T194" id="Seg_2230" s="T193">этот</ta>
            <ta e="T195" id="Seg_2231" s="T194">день.[NOM]</ta>
            <ta e="T196" id="Seg_2232" s="T195">домой</ta>
            <ta e="T197" id="Seg_2233" s="T196">ты.ACC</ta>
            <ta e="T198" id="Seg_2234" s="T197">послать-FUT-1PL</ta>
            <ta e="T199" id="Seg_2235" s="T198">ты.NOM</ta>
            <ta e="T200" id="Seg_2236" s="T199">теперь</ta>
            <ta e="T201" id="Seg_2237" s="T200">хороший-DIM-ADVZ</ta>
            <ta e="T202" id="Seg_2238" s="T201">быть-CO-2SG.S</ta>
            <ta e="T203" id="Seg_2239" s="T202">домой</ta>
            <ta e="T204" id="Seg_2240" s="T203">отправиться-COND-2SG.O</ta>
            <ta e="T205" id="Seg_2241" s="T204">дом-LOC</ta>
            <ta e="T206" id="Seg_2242" s="T205">жить-IMP.2SG.S</ta>
            <ta e="T207" id="Seg_2243" s="T206">десять</ta>
            <ta e="T208" id="Seg_2244" s="T207">что.ли</ta>
            <ta e="T209" id="Seg_2245" s="T208">день.[NOM]</ta>
            <ta e="T210" id="Seg_2246" s="T209">дом-LOC</ta>
            <ta e="T211" id="Seg_2247" s="T210">жить-IMP.2SG.S</ta>
            <ta e="T212" id="Seg_2248" s="T211">работать-CVB</ta>
            <ta e="T213" id="Seg_2249" s="T212">NEG.IMP</ta>
            <ta e="T214" id="Seg_2250" s="T213">отправиться-IMP.2SG.S</ta>
            <ta e="T215" id="Seg_2251" s="T214">домой</ta>
            <ta e="T216" id="Seg_2252" s="T215">отправиться-COND-2SG.S</ta>
            <ta e="T217" id="Seg_2253" s="T216">дом-LOC</ta>
            <ta e="T218" id="Seg_2254" s="T217">сидеть-IMP.2SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_2255" s="T0">adj</ta>
            <ta e="T2" id="Seg_2256" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_2257" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_2258" s="T3">nprop-n:case</ta>
            <ta e="T5" id="Seg_2259" s="T4">n-n:num-n:ins-n&gt;adj</ta>
            <ta e="T6" id="Seg_2260" s="T5">n-n&gt;adj</ta>
            <ta e="T7" id="Seg_2261" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_2262" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_2263" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_2264" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_2265" s="T10">n--n&gt;n-n&gt;adj</ta>
            <ta e="T12" id="Seg_2266" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_2267" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_2268" s="T13">num</ta>
            <ta e="T15" id="Seg_2269" s="T14">adj-adj&gt;adj</ta>
            <ta e="T16" id="Seg_2270" s="T15">n-n:num-n:case-n:poss</ta>
            <ta e="T17" id="Seg_2271" s="T16">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_2272" s="T17">dem</ta>
            <ta e="T19" id="Seg_2273" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_2274" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_2275" s="T20">clit</ta>
            <ta e="T22" id="Seg_2276" s="T21">interrog</ta>
            <ta e="T23" id="Seg_2277" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_2278" s="T23">n-n:case-n:poss</ta>
            <ta e="T25" id="Seg_2279" s="T24">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_2280" s="T25">n-n:case-n:poss</ta>
            <ta e="T27" id="Seg_2281" s="T26">v-v&gt;adv</ta>
            <ta e="T28" id="Seg_2282" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_2283" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_2284" s="T29">n-n:case-n:poss</ta>
            <ta e="T31" id="Seg_2285" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_2286" s="T31">pp</ta>
            <ta e="T33" id="Seg_2287" s="T32">n-n:case-n:poss</ta>
            <ta e="T34" id="Seg_2288" s="T33">emphpro-n:case-n:obl.poss</ta>
            <ta e="T35" id="Seg_2289" s="T34">v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_2290" s="T35">conj</ta>
            <ta e="T37" id="Seg_2291" s="T36">n-n:case-n:poss</ta>
            <ta e="T38" id="Seg_2292" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_2293" s="T38">v-v&gt;adv</ta>
            <ta e="T40" id="Seg_2294" s="T39">n-n:case</ta>
            <ta e="T41" id="Seg_2295" s="T40">adv</ta>
            <ta e="T42" id="Seg_2296" s="T41">v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_2297" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_2298" s="T43">interrog</ta>
            <ta e="T45" id="Seg_2299" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_2300" s="T45">interrog</ta>
            <ta e="T47" id="Seg_2301" s="T46">v-v:inf</ta>
            <ta e="T48" id="Seg_2302" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_2303" s="T48">v-v&gt;adv</ta>
            <ta e="T50" id="Seg_2304" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_2305" s="T50">emphpro-n:case</ta>
            <ta e="T52" id="Seg_2306" s="T51">v-v:tense-v:pn</ta>
            <ta e="T53" id="Seg_2307" s="T52">pers-n:ins-n:num-n:case</ta>
            <ta e="T54" id="Seg_2308" s="T53">v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_2309" s="T54">conj-n:case</ta>
            <ta e="T56" id="Seg_2310" s="T55">num</ta>
            <ta e="T57" id="Seg_2311" s="T56">conj-n:case</ta>
            <ta e="T58" id="Seg_2312" s="T57">num</ta>
            <ta e="T59" id="Seg_2313" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_2314" s="T59">num</ta>
            <ta e="T61" id="Seg_2315" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_2316" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_2317" s="T62">emphpro-n:case</ta>
            <ta e="T64" id="Seg_2318" s="T63">v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_2319" s="T64">adv</ta>
            <ta e="T66" id="Seg_2320" s="T65">n-n:case</ta>
            <ta e="T67" id="Seg_2321" s="T66">v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_2322" s="T67">v-v:pn</ta>
            <ta e="T69" id="Seg_2323" s="T68">n-n:case-n:poss</ta>
            <ta e="T70" id="Seg_2324" s="T69">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_2325" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_2326" s="T71">v-v&gt;ptcp</ta>
            <ta e="T73" id="Seg_2327" s="T72">adv</ta>
            <ta e="T74" id="Seg_2328" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_2329" s="T74">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_2330" s="T75">dem</ta>
            <ta e="T77" id="Seg_2331" s="T76">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_2332" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_2333" s="T78">v-v:tense-v:pn</ta>
            <ta e="T80" id="Seg_2334" s="T79">pers-n:case</ta>
            <ta e="T81" id="Seg_2335" s="T80">n-n:case</ta>
            <ta e="T82" id="Seg_2336" s="T81">conj</ta>
            <ta e="T83" id="Seg_2337" s="T82">n-n:case-n:poss</ta>
            <ta e="T84" id="Seg_2338" s="T83">pp</ta>
            <ta e="T85" id="Seg_2339" s="T84">v-v:tense-v:pn</ta>
            <ta e="T86" id="Seg_2340" s="T85">v-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_2341" s="T86">conj</ta>
            <ta e="T88" id="Seg_2342" s="T87">n-n:case-n:poss</ta>
            <ta e="T89" id="Seg_2343" s="T88">pp</ta>
            <ta e="T90" id="Seg_2344" s="T89">v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_2345" s="T90">conj</ta>
            <ta e="T92" id="Seg_2346" s="T91">num</ta>
            <ta e="T93" id="Seg_2347" s="T92">n-n:case-n:poss</ta>
            <ta e="T94" id="Seg_2348" s="T93">pp</ta>
            <ta e="T95" id="Seg_2349" s="T94">v-v:tense-v:pn</ta>
            <ta e="T96" id="Seg_2350" s="T95">v-v:inf</ta>
            <ta e="T97" id="Seg_2351" s="T96">n-n:case-n:poss</ta>
            <ta e="T98" id="Seg_2352" s="T97">v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T99" id="Seg_2353" s="T98">dem</ta>
            <ta e="T100" id="Seg_2354" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_2355" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_2356" s="T101">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T103" id="Seg_2357" s="T102">n-n:case-n:poss</ta>
            <ta e="T104" id="Seg_2358" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_2359" s="T104">n-n:case-n:poss</ta>
            <ta e="T106" id="Seg_2360" s="T105">n-n:case-v:pn</ta>
            <ta e="T107" id="Seg_2361" s="T106">pp-n:case</ta>
            <ta e="T108" id="Seg_2362" s="T107">conj</ta>
            <ta e="T109" id="Seg_2363" s="T108">num</ta>
            <ta e="T110" id="Seg_2364" s="T109">n-n&gt;n-n:case-n:poss</ta>
            <ta e="T111" id="Seg_2365" s="T110">adj-adj&gt;adj</ta>
            <ta e="T112" id="Seg_2366" s="T111">n-n:obl.poss-n:case</ta>
            <ta e="T113" id="Seg_2367" s="T112">adv</ta>
            <ta e="T114" id="Seg_2368" s="T113">v-v:inf</ta>
            <ta e="T115" id="Seg_2369" s="T114">n-n:case</ta>
            <ta e="T116" id="Seg_2370" s="T115">dem-adj&gt;adv</ta>
            <ta e="T117" id="Seg_2371" s="T116">v-v:ins-v:pn</ta>
            <ta e="T118" id="Seg_2372" s="T117">pers</ta>
            <ta e="T119" id="Seg_2373" s="T118">adv</ta>
            <ta e="T120" id="Seg_2374" s="T119">v-v:pn</ta>
            <ta e="T121" id="Seg_2375" s="T120">v-v:inf</ta>
            <ta e="T122" id="Seg_2376" s="T121">n-n&gt;adv</ta>
            <ta e="T123" id="Seg_2377" s="T122">adv</ta>
            <ta e="T124" id="Seg_2378" s="T123">v-v:pn</ta>
            <ta e="T125" id="Seg_2379" s="T124">v-v:pn</ta>
            <ta e="T126" id="Seg_2380" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_2381" s="T126">adv</ta>
            <ta e="T128" id="Seg_2382" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_2383" s="T128">v-v:tense-v:pn</ta>
            <ta e="T130" id="Seg_2384" s="T129">adv</ta>
            <ta e="T131" id="Seg_2385" s="T130">dem</ta>
            <ta e="T132" id="Seg_2386" s="T131">n-n:case</ta>
            <ta e="T133" id="Seg_2387" s="T132">n-n:case</ta>
            <ta e="T134" id="Seg_2388" s="T133">n-n:num-n:ins-n:case</ta>
            <ta e="T135" id="Seg_2389" s="T134">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T136" id="Seg_2390" s="T135">adj-adj&gt;adv</ta>
            <ta e="T137" id="Seg_2391" s="T136">v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_2392" s="T137">n-n:num-n:case</ta>
            <ta e="T139" id="Seg_2393" s="T138">pers-n:ins-n:case</ta>
            <ta e="T140" id="Seg_2394" s="T139">dem</ta>
            <ta e="T141" id="Seg_2395" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_2396" s="T141">v-v&gt;v-v&gt;v-v&gt;v-v&gt;v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T143" id="Seg_2397" s="T142">v-v&gt;v-v&gt;v-v&gt;v-v&gt;v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T144" id="Seg_2398" s="T143">dem</ta>
            <ta e="T145" id="Seg_2399" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_2400" s="T145">dem</ta>
            <ta e="T147" id="Seg_2401" s="T146">n-n:num-n:case</ta>
            <ta e="T148" id="Seg_2402" s="T147">dem</ta>
            <ta e="T149" id="Seg_2403" s="T148">n-n:case</ta>
            <ta e="T150" id="Seg_2404" s="T149">v-v&gt;v-v&gt;v-v&gt;v-v&gt;v-v&gt;v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T151" id="Seg_2405" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_2406" s="T151">n-n:case</ta>
            <ta e="T153" id="Seg_2407" s="T152">n-n:case</ta>
            <ta e="T154" id="Seg_2408" s="T153">interrog-clit</ta>
            <ta e="T155" id="Seg_2409" s="T154">adv</ta>
            <ta e="T156" id="Seg_2410" s="T155">dem</ta>
            <ta e="T157" id="Seg_2411" s="T156">n-n:case</ta>
            <ta e="T158" id="Seg_2412" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_2413" s="T158">ptcl</ta>
            <ta e="T160" id="Seg_2414" s="T159">adv</ta>
            <ta e="T161" id="Seg_2415" s="T160">v-v:tense-v:pn</ta>
            <ta e="T162" id="Seg_2416" s="T161">adv</ta>
            <ta e="T163" id="Seg_2417" s="T162">v-v:tense-v:pn</ta>
            <ta e="T164" id="Seg_2418" s="T163">adv</ta>
            <ta e="T165" id="Seg_2419" s="T164">dem</ta>
            <ta e="T166" id="Seg_2420" s="T165">n-n:case</ta>
            <ta e="T167" id="Seg_2421" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_2422" s="T167">interrog</ta>
            <ta e="T169" id="Seg_2423" s="T168">ptcl</ta>
            <ta e="T170" id="Seg_2424" s="T169">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T171" id="Seg_2425" s="T170">adv</ta>
            <ta e="T172" id="Seg_2426" s="T171">adj-adj&gt;adj-adj&gt;adv</ta>
            <ta e="T173" id="Seg_2427" s="T172">v-v&gt;adv</ta>
            <ta e="T174" id="Seg_2428" s="T173">v-v&gt;v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T175" id="Seg_2429" s="T174">adv</ta>
            <ta e="T176" id="Seg_2430" s="T175">v-v&gt;adv</ta>
            <ta e="T177" id="Seg_2431" s="T176">interrog</ta>
            <ta e="T178" id="Seg_2432" s="T177">ptcl</ta>
            <ta e="T179" id="Seg_2433" s="T178">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T180" id="Seg_2434" s="T179">adv</ta>
            <ta e="T181" id="Seg_2435" s="T180">n-n:case</ta>
            <ta e="T182" id="Seg_2436" s="T181">n-n:case</ta>
            <ta e="T183" id="Seg_2437" s="T182">dem-adj&gt;adv</ta>
            <ta e="T184" id="Seg_2438" s="T183">v-v:tense-v:pn</ta>
            <ta e="T185" id="Seg_2439" s="T184">interrog</ta>
            <ta e="T186" id="Seg_2440" s="T185">clit</ta>
            <ta e="T187" id="Seg_2441" s="T186">dem</ta>
            <ta e="T188" id="Seg_2442" s="T187">n-n:case</ta>
            <ta e="T189" id="Seg_2443" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_2444" s="T189">clit</ta>
            <ta e="T191" id="Seg_2445" s="T190">adv</ta>
            <ta e="T192" id="Seg_2446" s="T191">n-n:case</ta>
            <ta e="T193" id="Seg_2447" s="T192">clit</ta>
            <ta e="T194" id="Seg_2448" s="T193">dem</ta>
            <ta e="T195" id="Seg_2449" s="T194">n-n:case</ta>
            <ta e="T196" id="Seg_2450" s="T195">adv</ta>
            <ta e="T197" id="Seg_2451" s="T196">pers</ta>
            <ta e="T198" id="Seg_2452" s="T197">v-v:tense-v:pn</ta>
            <ta e="T199" id="Seg_2453" s="T198">pers</ta>
            <ta e="T200" id="Seg_2454" s="T199">adv</ta>
            <ta e="T201" id="Seg_2455" s="T200">adj-adj&gt;adj-adj&gt;adv</ta>
            <ta e="T202" id="Seg_2456" s="T201">v-v:ins-v:pn</ta>
            <ta e="T203" id="Seg_2457" s="T202">adv</ta>
            <ta e="T204" id="Seg_2458" s="T203">v-v:mood-v:pn</ta>
            <ta e="T205" id="Seg_2459" s="T204">n-n:case</ta>
            <ta e="T206" id="Seg_2460" s="T205">v-v:mood.pn</ta>
            <ta e="T207" id="Seg_2461" s="T206">num</ta>
            <ta e="T208" id="Seg_2462" s="T207">ptcl</ta>
            <ta e="T209" id="Seg_2463" s="T208">n-n:case</ta>
            <ta e="T210" id="Seg_2464" s="T209">n-n:case</ta>
            <ta e="T211" id="Seg_2465" s="T210">v-v:mood.pn</ta>
            <ta e="T212" id="Seg_2466" s="T211">v-v&gt;adv</ta>
            <ta e="T213" id="Seg_2467" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_2468" s="T213">v-v:mood.pn</ta>
            <ta e="T215" id="Seg_2469" s="T214">adv</ta>
            <ta e="T216" id="Seg_2470" s="T215">v-v:mood-v:pn</ta>
            <ta e="T217" id="Seg_2471" s="T216">n-n:case</ta>
            <ta e="T218" id="Seg_2472" s="T217">v-v:mood.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2473" s="T0">adj</ta>
            <ta e="T2" id="Seg_2474" s="T1">n</ta>
            <ta e="T3" id="Seg_2475" s="T2">n</ta>
            <ta e="T4" id="Seg_2476" s="T3">nprop</ta>
            <ta e="T5" id="Seg_2477" s="T4">n</ta>
            <ta e="T6" id="Seg_2478" s="T5">adj</ta>
            <ta e="T7" id="Seg_2479" s="T6">n</ta>
            <ta e="T8" id="Seg_2480" s="T7">n</ta>
            <ta e="T9" id="Seg_2481" s="T8">n</ta>
            <ta e="T10" id="Seg_2482" s="T9">v</ta>
            <ta e="T11" id="Seg_2483" s="T10">adj</ta>
            <ta e="T12" id="Seg_2484" s="T11">n</ta>
            <ta e="T13" id="Seg_2485" s="T12">n</ta>
            <ta e="T14" id="Seg_2486" s="T13">num</ta>
            <ta e="T15" id="Seg_2487" s="T14">adj</ta>
            <ta e="T16" id="Seg_2488" s="T15">n</ta>
            <ta e="T17" id="Seg_2489" s="T16">v</ta>
            <ta e="T18" id="Seg_2490" s="T17">dem</ta>
            <ta e="T19" id="Seg_2491" s="T18">n</ta>
            <ta e="T20" id="Seg_2492" s="T19">n</ta>
            <ta e="T21" id="Seg_2493" s="T20">clit</ta>
            <ta e="T22" id="Seg_2494" s="T21">interrog</ta>
            <ta e="T23" id="Seg_2495" s="T22">v</ta>
            <ta e="T24" id="Seg_2496" s="T23">n</ta>
            <ta e="T25" id="Seg_2497" s="T24">v</ta>
            <ta e="T26" id="Seg_2498" s="T25">n</ta>
            <ta e="T27" id="Seg_2499" s="T26">adv</ta>
            <ta e="T28" id="Seg_2500" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_2501" s="T28">v</ta>
            <ta e="T30" id="Seg_2502" s="T29">n</ta>
            <ta e="T31" id="Seg_2503" s="T30">n</ta>
            <ta e="T32" id="Seg_2504" s="T31">pp</ta>
            <ta e="T33" id="Seg_2505" s="T32">n</ta>
            <ta e="T34" id="Seg_2506" s="T33">emphpro</ta>
            <ta e="T35" id="Seg_2507" s="T34">v</ta>
            <ta e="T36" id="Seg_2508" s="T35">conj</ta>
            <ta e="T37" id="Seg_2509" s="T36">n</ta>
            <ta e="T38" id="Seg_2510" s="T37">n</ta>
            <ta e="T39" id="Seg_2511" s="T38">adv</ta>
            <ta e="T40" id="Seg_2512" s="T39">n</ta>
            <ta e="T41" id="Seg_2513" s="T40">adv</ta>
            <ta e="T42" id="Seg_2514" s="T41">v</ta>
            <ta e="T43" id="Seg_2515" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_2516" s="T43">interrog</ta>
            <ta e="T45" id="Seg_2517" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_2518" s="T45">interrog</ta>
            <ta e="T47" id="Seg_2519" s="T46">v</ta>
            <ta e="T48" id="Seg_2520" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_2521" s="T48">adv</ta>
            <ta e="T50" id="Seg_2522" s="T49">n</ta>
            <ta e="T51" id="Seg_2523" s="T50">emphpro</ta>
            <ta e="T52" id="Seg_2524" s="T51">v</ta>
            <ta e="T53" id="Seg_2525" s="T52">pers</ta>
            <ta e="T54" id="Seg_2526" s="T53">v</ta>
            <ta e="T55" id="Seg_2527" s="T54">conj</ta>
            <ta e="T56" id="Seg_2528" s="T55">num</ta>
            <ta e="T57" id="Seg_2529" s="T56">conj</ta>
            <ta e="T58" id="Seg_2530" s="T57">num</ta>
            <ta e="T59" id="Seg_2531" s="T58">n</ta>
            <ta e="T60" id="Seg_2532" s="T59">num</ta>
            <ta e="T61" id="Seg_2533" s="T60">n</ta>
            <ta e="T62" id="Seg_2534" s="T61">n</ta>
            <ta e="T63" id="Seg_2535" s="T62">emphpro</ta>
            <ta e="T64" id="Seg_2536" s="T63">v</ta>
            <ta e="T65" id="Seg_2537" s="T64">adv</ta>
            <ta e="T66" id="Seg_2538" s="T65">n</ta>
            <ta e="T67" id="Seg_2539" s="T66">v</ta>
            <ta e="T68" id="Seg_2540" s="T67">v</ta>
            <ta e="T69" id="Seg_2541" s="T68">n</ta>
            <ta e="T70" id="Seg_2542" s="T69">v</ta>
            <ta e="T71" id="Seg_2543" s="T70">n</ta>
            <ta e="T72" id="Seg_2544" s="T71">ptcp</ta>
            <ta e="T73" id="Seg_2545" s="T72">adv</ta>
            <ta e="T74" id="Seg_2546" s="T73">n</ta>
            <ta e="T75" id="Seg_2547" s="T74">v</ta>
            <ta e="T76" id="Seg_2548" s="T75">dem</ta>
            <ta e="T77" id="Seg_2549" s="T76">v</ta>
            <ta e="T78" id="Seg_2550" s="T77">n</ta>
            <ta e="T79" id="Seg_2551" s="T78">v</ta>
            <ta e="T80" id="Seg_2552" s="T79">pers</ta>
            <ta e="T81" id="Seg_2553" s="T80">n</ta>
            <ta e="T82" id="Seg_2554" s="T81">conj</ta>
            <ta e="T83" id="Seg_2555" s="T82">n</ta>
            <ta e="T84" id="Seg_2556" s="T83">pp</ta>
            <ta e="T85" id="Seg_2557" s="T84">v</ta>
            <ta e="T86" id="Seg_2558" s="T85">v</ta>
            <ta e="T87" id="Seg_2559" s="T86">conj</ta>
            <ta e="T88" id="Seg_2560" s="T87">n</ta>
            <ta e="T89" id="Seg_2561" s="T88">pp</ta>
            <ta e="T90" id="Seg_2562" s="T89">v</ta>
            <ta e="T91" id="Seg_2563" s="T90">conj</ta>
            <ta e="T92" id="Seg_2564" s="T91">num</ta>
            <ta e="T93" id="Seg_2565" s="T92">n</ta>
            <ta e="T94" id="Seg_2566" s="T93">pp</ta>
            <ta e="T95" id="Seg_2567" s="T94">v</ta>
            <ta e="T96" id="Seg_2568" s="T95">v</ta>
            <ta e="T97" id="Seg_2569" s="T96">n</ta>
            <ta e="T98" id="Seg_2570" s="T97">v</ta>
            <ta e="T99" id="Seg_2571" s="T98">dem</ta>
            <ta e="T100" id="Seg_2572" s="T99">n</ta>
            <ta e="T101" id="Seg_2573" s="T100">n</ta>
            <ta e="T102" id="Seg_2574" s="T101">v</ta>
            <ta e="T103" id="Seg_2575" s="T102">n</ta>
            <ta e="T104" id="Seg_2576" s="T103">n</ta>
            <ta e="T105" id="Seg_2577" s="T104">n</ta>
            <ta e="T106" id="Seg_2578" s="T105">n</ta>
            <ta e="T107" id="Seg_2579" s="T106">pp</ta>
            <ta e="T108" id="Seg_2580" s="T107">conj</ta>
            <ta e="T109" id="Seg_2581" s="T108">num</ta>
            <ta e="T110" id="Seg_2582" s="T109">n</ta>
            <ta e="T111" id="Seg_2583" s="T110">adj</ta>
            <ta e="T112" id="Seg_2584" s="T111">n</ta>
            <ta e="T113" id="Seg_2585" s="T112">adv</ta>
            <ta e="T114" id="Seg_2586" s="T113">v</ta>
            <ta e="T115" id="Seg_2587" s="T114">n</ta>
            <ta e="T116" id="Seg_2588" s="T115">adv</ta>
            <ta e="T117" id="Seg_2589" s="T116">v</ta>
            <ta e="T118" id="Seg_2590" s="T117">pers</ta>
            <ta e="T119" id="Seg_2591" s="T118">adv</ta>
            <ta e="T120" id="Seg_2592" s="T119">v</ta>
            <ta e="T121" id="Seg_2593" s="T120">v</ta>
            <ta e="T122" id="Seg_2594" s="T121">adv</ta>
            <ta e="T123" id="Seg_2595" s="T122">adv</ta>
            <ta e="T124" id="Seg_2596" s="T123">v</ta>
            <ta e="T125" id="Seg_2597" s="T124">v</ta>
            <ta e="T126" id="Seg_2598" s="T125">n</ta>
            <ta e="T127" id="Seg_2599" s="T126">adv</ta>
            <ta e="T128" id="Seg_2600" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_2601" s="T128">v</ta>
            <ta e="T130" id="Seg_2602" s="T129">adv</ta>
            <ta e="T131" id="Seg_2603" s="T130">dem</ta>
            <ta e="T132" id="Seg_2604" s="T131">n</ta>
            <ta e="T133" id="Seg_2605" s="T132">n</ta>
            <ta e="T134" id="Seg_2606" s="T133">n</ta>
            <ta e="T135" id="Seg_2607" s="T134">v</ta>
            <ta e="T136" id="Seg_2608" s="T135">adv</ta>
            <ta e="T137" id="Seg_2609" s="T136">v</ta>
            <ta e="T138" id="Seg_2610" s="T137">n</ta>
            <ta e="T139" id="Seg_2611" s="T138">pers</ta>
            <ta e="T140" id="Seg_2612" s="T139">dem</ta>
            <ta e="T141" id="Seg_2613" s="T140">n</ta>
            <ta e="T142" id="Seg_2614" s="T141">v</ta>
            <ta e="T143" id="Seg_2615" s="T142">v</ta>
            <ta e="T144" id="Seg_2616" s="T143">dem</ta>
            <ta e="T145" id="Seg_2617" s="T144">n</ta>
            <ta e="T146" id="Seg_2618" s="T145">dem</ta>
            <ta e="T147" id="Seg_2619" s="T146">n</ta>
            <ta e="T148" id="Seg_2620" s="T147">dem</ta>
            <ta e="T149" id="Seg_2621" s="T148">n</ta>
            <ta e="T150" id="Seg_2622" s="T149">v</ta>
            <ta e="T151" id="Seg_2623" s="T150">n</ta>
            <ta e="T152" id="Seg_2624" s="T151">n</ta>
            <ta e="T153" id="Seg_2625" s="T152">n</ta>
            <ta e="T154" id="Seg_2626" s="T153">interrog</ta>
            <ta e="T155" id="Seg_2627" s="T154">adv</ta>
            <ta e="T156" id="Seg_2628" s="T155">dem</ta>
            <ta e="T157" id="Seg_2629" s="T156">n</ta>
            <ta e="T158" id="Seg_2630" s="T157">n</ta>
            <ta e="T159" id="Seg_2631" s="T158">ptcl</ta>
            <ta e="T160" id="Seg_2632" s="T159">adv</ta>
            <ta e="T161" id="Seg_2633" s="T160">v</ta>
            <ta e="T162" id="Seg_2634" s="T161">adv</ta>
            <ta e="T163" id="Seg_2635" s="T162">v</ta>
            <ta e="T164" id="Seg_2636" s="T163">adv</ta>
            <ta e="T165" id="Seg_2637" s="T164">dem</ta>
            <ta e="T166" id="Seg_2638" s="T165">n</ta>
            <ta e="T167" id="Seg_2639" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_2640" s="T167">interrog</ta>
            <ta e="T169" id="Seg_2641" s="T168">ptcl</ta>
            <ta e="T170" id="Seg_2642" s="T169">v</ta>
            <ta e="T171" id="Seg_2643" s="T170">adv</ta>
            <ta e="T172" id="Seg_2644" s="T171">adv</ta>
            <ta e="T173" id="Seg_2645" s="T172">adv</ta>
            <ta e="T174" id="Seg_2646" s="T173">v</ta>
            <ta e="T175" id="Seg_2647" s="T174">adv</ta>
            <ta e="T176" id="Seg_2648" s="T175">adv</ta>
            <ta e="T177" id="Seg_2649" s="T176">interrog</ta>
            <ta e="T178" id="Seg_2650" s="T177">ptcl</ta>
            <ta e="T179" id="Seg_2651" s="T178">v</ta>
            <ta e="T180" id="Seg_2652" s="T179">adv</ta>
            <ta e="T181" id="Seg_2653" s="T180">n</ta>
            <ta e="T182" id="Seg_2654" s="T181">n</ta>
            <ta e="T183" id="Seg_2655" s="T182">adv</ta>
            <ta e="T184" id="Seg_2656" s="T183">v</ta>
            <ta e="T185" id="Seg_2657" s="T184">interrog</ta>
            <ta e="T186" id="Seg_2658" s="T185">clit</ta>
            <ta e="T187" id="Seg_2659" s="T186">dem</ta>
            <ta e="T188" id="Seg_2660" s="T187">n</ta>
            <ta e="T189" id="Seg_2661" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_2662" s="T189">clit</ta>
            <ta e="T191" id="Seg_2663" s="T190">adv</ta>
            <ta e="T192" id="Seg_2664" s="T191">n</ta>
            <ta e="T193" id="Seg_2665" s="T192">clit</ta>
            <ta e="T194" id="Seg_2666" s="T193">dem</ta>
            <ta e="T195" id="Seg_2667" s="T194">n</ta>
            <ta e="T196" id="Seg_2668" s="T195">adv</ta>
            <ta e="T197" id="Seg_2669" s="T196">pers</ta>
            <ta e="T198" id="Seg_2670" s="T197">v</ta>
            <ta e="T199" id="Seg_2671" s="T198">pers</ta>
            <ta e="T200" id="Seg_2672" s="T199">adv</ta>
            <ta e="T201" id="Seg_2673" s="T200">adv</ta>
            <ta e="T202" id="Seg_2674" s="T201">v</ta>
            <ta e="T203" id="Seg_2675" s="T202">adv</ta>
            <ta e="T204" id="Seg_2676" s="T203">v</ta>
            <ta e="T205" id="Seg_2677" s="T204">n</ta>
            <ta e="T206" id="Seg_2678" s="T205">v</ta>
            <ta e="T207" id="Seg_2679" s="T206">num</ta>
            <ta e="T208" id="Seg_2680" s="T207">ptcl</ta>
            <ta e="T209" id="Seg_2681" s="T208">n</ta>
            <ta e="T210" id="Seg_2682" s="T209">n</ta>
            <ta e="T211" id="Seg_2683" s="T210">v</ta>
            <ta e="T212" id="Seg_2684" s="T211">adv</ta>
            <ta e="T213" id="Seg_2685" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_2686" s="T213">v</ta>
            <ta e="T215" id="Seg_2687" s="T214">adv</ta>
            <ta e="T216" id="Seg_2688" s="T215">v</ta>
            <ta e="T217" id="Seg_2689" s="T216">n</ta>
            <ta e="T218" id="Seg_2690" s="T217">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T4" id="Seg_2691" s="T3">np:L</ta>
            <ta e="T7" id="Seg_2692" s="T6">np:Poss</ta>
            <ta e="T8" id="Seg_2693" s="T7">np:L</ta>
            <ta e="T9" id="Seg_2694" s="T8">np.h:Th</ta>
            <ta e="T12" id="Seg_2695" s="T11">pp.h:Poss</ta>
            <ta e="T16" id="Seg_2696" s="T15">np.h:Th</ta>
            <ta e="T19" id="Seg_2697" s="T18">np.h:Poss</ta>
            <ta e="T20" id="Seg_2698" s="T19">np.h:A</ta>
            <ta e="T22" id="Seg_2699" s="T21">pro:L</ta>
            <ta e="T24" id="Seg_2700" s="T23">np:P</ta>
            <ta e="T25" id="Seg_2701" s="T24">0.3.h:A</ta>
            <ta e="T29" id="Seg_2702" s="T28">0.3.h:A</ta>
            <ta e="T31" id="Seg_2703" s="T30">pp:Time</ta>
            <ta e="T33" id="Seg_2704" s="T32">np.h:A</ta>
            <ta e="T37" id="Seg_2705" s="T36">np.h:Poss 0.3.h:Poss</ta>
            <ta e="T38" id="Seg_2706" s="T37">np.h:Th</ta>
            <ta e="T40" id="Seg_2707" s="T39">np:L</ta>
            <ta e="T41" id="Seg_2708" s="T40">adv:Time</ta>
            <ta e="T44" id="Seg_2709" s="T43">pro:L</ta>
            <ta e="T50" id="Seg_2710" s="T49">np.h:A</ta>
            <ta e="T53" id="Seg_2711" s="T52">pro.h:Th</ta>
            <ta e="T62" id="Seg_2712" s="T61">np.h:A</ta>
            <ta e="T65" id="Seg_2713" s="T64">adv:Time</ta>
            <ta e="T67" id="Seg_2714" s="T66">0.3.h:Th</ta>
            <ta e="T69" id="Seg_2715" s="T68">np:Th 0.3.h:Poss</ta>
            <ta e="T71" id="Seg_2716" s="T70">np:Th</ta>
            <ta e="T73" id="Seg_2717" s="T72">adv:Time</ta>
            <ta e="T74" id="Seg_2718" s="T73">np.h:P</ta>
            <ta e="T76" id="Seg_2719" s="T75">pro.h:P</ta>
            <ta e="T78" id="Seg_2720" s="T77">np:G</ta>
            <ta e="T79" id="Seg_2721" s="T78">0.3.h:A 0.3.h:Th</ta>
            <ta e="T80" id="Seg_2722" s="T79">pro.h:Th</ta>
            <ta e="T81" id="Seg_2723" s="T80">np:L</ta>
            <ta e="T83" id="Seg_2724" s="T82">pp:Time</ta>
            <ta e="T88" id="Seg_2725" s="T87">pp:Time</ta>
            <ta e="T90" id="Seg_2726" s="T89">0.3.h:Th</ta>
            <ta e="T93" id="Seg_2727" s="T92">pp:Time</ta>
            <ta e="T95" id="Seg_2728" s="T94">0.3.h:Th</ta>
            <ta e="T97" id="Seg_2729" s="T96">np:P</ta>
            <ta e="T100" id="Seg_2730" s="T99">np.h:Poss</ta>
            <ta e="T101" id="Seg_2731" s="T100">np:G</ta>
            <ta e="T103" id="Seg_2732" s="T102">0.3.h:Poss np.h:Poss</ta>
            <ta e="T104" id="Seg_2733" s="T103">np.h:A</ta>
            <ta e="T105" id="Seg_2734" s="T104">0.3.h:Poss np.h:Poss</ta>
            <ta e="T106" id="Seg_2735" s="T105">pp:Com</ta>
            <ta e="T110" id="Seg_2736" s="T109">np.h:A 0.3.h:Poss</ta>
            <ta e="T112" id="Seg_2737" s="T111">np:Com</ta>
            <ta e="T115" id="Seg_2738" s="T114">np.h:A</ta>
            <ta e="T118" id="Seg_2739" s="T117">pro.h:Th</ta>
            <ta e="T127" id="Seg_2740" s="T126">adv:G</ta>
            <ta e="T129" id="Seg_2741" s="T128">0.1.h:A</ta>
            <ta e="T130" id="Seg_2742" s="T129">adv:Time</ta>
            <ta e="T132" id="Seg_2743" s="T131">np.h:E</ta>
            <ta e="T133" id="Seg_2744" s="T132">np:L</ta>
            <ta e="T134" id="Seg_2745" s="T133">np.h:Th</ta>
            <ta e="T137" id="Seg_2746" s="T136">0.3.h:Th</ta>
            <ta e="T138" id="Seg_2747" s="T137">np.h:A</ta>
            <ta e="T139" id="Seg_2748" s="T138">pro.h:R</ta>
            <ta e="T141" id="Seg_2749" s="T140">np.h:R</ta>
            <ta e="T145" id="Seg_2750" s="T144">np.h:R</ta>
            <ta e="T147" id="Seg_2751" s="T146">np.h:A</ta>
            <ta e="T149" id="Seg_2752" s="T148">np.h:R</ta>
            <ta e="T151" id="Seg_2753" s="T150">np:Th</ta>
            <ta e="T152" id="Seg_2754" s="T151">np:Th</ta>
            <ta e="T153" id="Seg_2755" s="T152">np:Th</ta>
            <ta e="T154" id="Seg_2756" s="T153">np:Th</ta>
            <ta e="T155" id="Seg_2757" s="T154">adv:Time</ta>
            <ta e="T157" id="Seg_2758" s="T156">np.h:Th</ta>
            <ta e="T158" id="Seg_2759" s="T157">np:L</ta>
            <ta e="T162" id="Seg_2760" s="T161">adv:G</ta>
            <ta e="T163" id="Seg_2761" s="T162">0.3.h:A 0.3.h:Th</ta>
            <ta e="T164" id="Seg_2762" s="T163">adv:Time</ta>
            <ta e="T166" id="Seg_2763" s="T165">np.h:A</ta>
            <ta e="T168" id="Seg_2764" s="T167">pro:G</ta>
            <ta e="T171" id="Seg_2765" s="T170">adv:Time</ta>
            <ta e="T174" id="Seg_2766" s="T173">0.3.h:A</ta>
            <ta e="T177" id="Seg_2767" s="T176">adv:G</ta>
            <ta e="T179" id="Seg_2768" s="T178">0.3.h:A</ta>
            <ta e="T180" id="Seg_2769" s="T179">adv:Time</ta>
            <ta e="T181" id="Seg_2770" s="T180">np.h:A</ta>
            <ta e="T188" id="Seg_2771" s="T187">np:Time</ta>
            <ta e="T192" id="Seg_2772" s="T191">np:Time</ta>
            <ta e="T195" id="Seg_2773" s="T194">np:Time</ta>
            <ta e="T196" id="Seg_2774" s="T195">adv:G</ta>
            <ta e="T197" id="Seg_2775" s="T196">pro.h:Th</ta>
            <ta e="T198" id="Seg_2776" s="T197">0.1.h:A</ta>
            <ta e="T199" id="Seg_2777" s="T198">pro.h:Th</ta>
            <ta e="T200" id="Seg_2778" s="T199">adv:Time</ta>
            <ta e="T203" id="Seg_2779" s="T202">adv:G</ta>
            <ta e="T204" id="Seg_2780" s="T203">0.2.h:A</ta>
            <ta e="T205" id="Seg_2781" s="T204">np:L</ta>
            <ta e="T206" id="Seg_2782" s="T205">0.2.h:Th</ta>
            <ta e="T209" id="Seg_2783" s="T208">np:Time</ta>
            <ta e="T210" id="Seg_2784" s="T209">np:L</ta>
            <ta e="T211" id="Seg_2785" s="T210">0.2.h:Th</ta>
            <ta e="T214" id="Seg_2786" s="T213">0.2.h:A</ta>
            <ta e="T216" id="Seg_2787" s="T215">0.2.h:A</ta>
            <ta e="T217" id="Seg_2788" s="T216">np:L</ta>
            <ta e="T218" id="Seg_2789" s="T217">0.2.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T9" id="Seg_2790" s="T8">np.h:S</ta>
            <ta e="T10" id="Seg_2791" s="T9">v:pred</ta>
            <ta e="T16" id="Seg_2792" s="T15">np.h:S</ta>
            <ta e="T17" id="Seg_2793" s="T16">v:pred</ta>
            <ta e="T20" id="Seg_2794" s="T19">np.h:S</ta>
            <ta e="T23" id="Seg_2795" s="T22">v:pred</ta>
            <ta e="T24" id="Seg_2796" s="T23">np:O</ta>
            <ta e="T25" id="Seg_2797" s="T24">0.3.h:S v:pred</ta>
            <ta e="T27" id="Seg_2798" s="T25">s:temp</ta>
            <ta e="T29" id="Seg_2799" s="T28">0.3.h:S v:pred</ta>
            <ta e="T33" id="Seg_2800" s="T32">np.h:S</ta>
            <ta e="T35" id="Seg_2801" s="T34">v:pred</ta>
            <ta e="T38" id="Seg_2802" s="T37">np.h:S</ta>
            <ta e="T39" id="Seg_2803" s="T38">s:adv</ta>
            <ta e="T42" id="Seg_2804" s="T41">v:pred</ta>
            <ta e="T49" id="Seg_2805" s="T42">s:adv</ta>
            <ta e="T50" id="Seg_2806" s="T49">np.h:S</ta>
            <ta e="T52" id="Seg_2807" s="T51">v:pred</ta>
            <ta e="T53" id="Seg_2808" s="T52">pro.h:S</ta>
            <ta e="T54" id="Seg_2809" s="T53">v:pred</ta>
            <ta e="T59" id="Seg_2810" s="T58">n:pred</ta>
            <ta e="T62" id="Seg_2811" s="T61">np.h:S</ta>
            <ta e="T64" id="Seg_2812" s="T63">v:pred</ta>
            <ta e="T67" id="Seg_2813" s="T66">0.3.h:S v:pred</ta>
            <ta e="T69" id="Seg_2814" s="T68">np:S</ta>
            <ta e="T70" id="Seg_2815" s="T69">v:pred</ta>
            <ta e="T72" id="Seg_2816" s="T70">s:purp</ta>
            <ta e="T74" id="Seg_2817" s="T73">np.h:S</ta>
            <ta e="T75" id="Seg_2818" s="T74">v:pred</ta>
            <ta e="T76" id="Seg_2819" s="T75">pro.h:S</ta>
            <ta e="T77" id="Seg_2820" s="T76">v:pred</ta>
            <ta e="T79" id="Seg_2821" s="T78">0.3.h:S 0.3.h:O v:pred</ta>
            <ta e="T80" id="Seg_2822" s="T79">pro.h:S</ta>
            <ta e="T85" id="Seg_2823" s="T84">v:pred</ta>
            <ta e="T90" id="Seg_2824" s="T89">0.3.h:S v:pred</ta>
            <ta e="T95" id="Seg_2825" s="T94">0.3.h:S v:pred</ta>
            <ta e="T97" id="Seg_2826" s="T96">np:S</ta>
            <ta e="T98" id="Seg_2827" s="T97">v:pred</ta>
            <ta e="T102" id="Seg_2828" s="T101">v:pred</ta>
            <ta e="T104" id="Seg_2829" s="T103">np.h:S</ta>
            <ta e="T110" id="Seg_2830" s="T109">np.h:S</ta>
            <ta e="T114" id="Seg_2831" s="T112">s:purp</ta>
            <ta e="T115" id="Seg_2832" s="T114">np.h:S</ta>
            <ta e="T117" id="Seg_2833" s="T116">v:pred</ta>
            <ta e="T118" id="Seg_2834" s="T117">pro.h:S</ta>
            <ta e="T120" id="Seg_2835" s="T119">v:pred</ta>
            <ta e="T124" id="Seg_2836" s="T123">v:pred</ta>
            <ta e="T129" id="Seg_2837" s="T128">0.1.h:S v:pred</ta>
            <ta e="T132" id="Seg_2838" s="T131">np.h:S</ta>
            <ta e="T135" id="Seg_2839" s="T134">v:pred</ta>
            <ta e="T137" id="Seg_2840" s="T136">0.3.h:S v:pred</ta>
            <ta e="T138" id="Seg_2841" s="T137">np.h:S</ta>
            <ta e="T142" id="Seg_2842" s="T141">v:pred</ta>
            <ta e="T147" id="Seg_2843" s="T146">np.h:S</ta>
            <ta e="T150" id="Seg_2844" s="T149">v:pred</ta>
            <ta e="T151" id="Seg_2845" s="T150">np:O</ta>
            <ta e="T152" id="Seg_2846" s="T151">np:O</ta>
            <ta e="T153" id="Seg_2847" s="T152">np:O</ta>
            <ta e="T154" id="Seg_2848" s="T153">np:O</ta>
            <ta e="T157" id="Seg_2849" s="T156">np.h:S</ta>
            <ta e="T161" id="Seg_2850" s="T160">v:pred</ta>
            <ta e="T163" id="Seg_2851" s="T162">0.3.h:S 0.3.h:O v:pred</ta>
            <ta e="T166" id="Seg_2852" s="T165">np.h:S</ta>
            <ta e="T170" id="Seg_2853" s="T169">v:pred</ta>
            <ta e="T173" id="Seg_2854" s="T171">s:adv</ta>
            <ta e="T174" id="Seg_2855" s="T173">0.3.h:S v:pred</ta>
            <ta e="T176" id="Seg_2856" s="T174">s:adv</ta>
            <ta e="T179" id="Seg_2857" s="T178">0.3.h:S v:pred</ta>
            <ta e="T181" id="Seg_2858" s="T180">np.h:S</ta>
            <ta e="T184" id="Seg_2859" s="T183">v:pred</ta>
            <ta e="T197" id="Seg_2860" s="T196">pro.h:O</ta>
            <ta e="T198" id="Seg_2861" s="T197">0.1.h:S v:pred</ta>
            <ta e="T199" id="Seg_2862" s="T198">pro.h:S</ta>
            <ta e="T202" id="Seg_2863" s="T201">v:pred</ta>
            <ta e="T204" id="Seg_2864" s="T202">s:cond</ta>
            <ta e="T206" id="Seg_2865" s="T205">0.2.h:S v:pred</ta>
            <ta e="T211" id="Seg_2866" s="T210">0.2.h:S v:pred</ta>
            <ta e="T212" id="Seg_2867" s="T211">s:purp</ta>
            <ta e="T214" id="Seg_2868" s="T213">0.2.h:S v:pred</ta>
            <ta e="T216" id="Seg_2869" s="T214">s:cond</ta>
            <ta e="T218" id="Seg_2870" s="T217">0.2.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_2871" s="T3">RUS:cult</ta>
            <ta e="T36" id="Seg_2872" s="T35">RUS:gram</ta>
            <ta e="T43" id="Seg_2873" s="T42">RUS:gram</ta>
            <ta e="T45" id="Seg_2874" s="T44">RUS:gram</ta>
            <ta e="T78" id="Seg_2875" s="T77">RUS:cult</ta>
            <ta e="T81" id="Seg_2876" s="T80">RUS:cult</ta>
            <ta e="T101" id="Seg_2877" s="T100">RUS:cult</ta>
            <ta e="T133" id="Seg_2878" s="T132">RUS:cult</ta>
            <ta e="T158" id="Seg_2879" s="T157">RUS:cult</ta>
            <ta e="T167" id="Seg_2880" s="T166">RUS:gram</ta>
            <ta e="T178" id="Seg_2881" s="T177">RUS:gram</ta>
            <ta e="T181" id="Seg_2882" s="T180">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T4" id="Seg_2883" s="T3">medVdel</ta>
            <ta e="T81" id="Seg_2884" s="T80">Csub</ta>
            <ta e="T101" id="Seg_2885" s="T100">Csub</ta>
            <ta e="T158" id="Seg_2886" s="T157">Csub</ta>
            <ta e="T178" id="Seg_2887" s="T177">finVdel</ta>
            <ta e="T181" id="Seg_2888" s="T180">Csub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T4" id="Seg_2889" s="T3">dir:infl</ta>
            <ta e="T36" id="Seg_2890" s="T35">dir:bare</ta>
            <ta e="T43" id="Seg_2891" s="T42">dir:bare</ta>
            <ta e="T45" id="Seg_2892" s="T44">dir:bare</ta>
            <ta e="T78" id="Seg_2893" s="T77">dir:infl</ta>
            <ta e="T81" id="Seg_2894" s="T80">dir:infl</ta>
            <ta e="T101" id="Seg_2895" s="T100">dir:infl</ta>
            <ta e="T133" id="Seg_2896" s="T132">dir:infl</ta>
            <ta e="T158" id="Seg_2897" s="T157">dir:infl</ta>
            <ta e="T167" id="Seg_2898" s="T166">dir:bare</ta>
            <ta e="T178" id="Seg_2899" s="T177">dir:bare</ta>
            <ta e="T181" id="Seg_2900" s="T180">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T2" id="Seg_2901" s="T0">ext</ta>
            <ta e="T3" id="Seg_2902" s="T2">ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T2" id="Seg_2903" s="T0">"Короткий отпуск".</ta>
            <ta e="T3" id="Seg_2904" s="T2">Фильм.</ta>
            <ta e="T8" id="Seg_2905" s="T3">В Италии, в других правительственных странах (…).</ta>
            <ta e="T11" id="Seg_2906" s="T8">Жила замужняя женщина.</ta>
            <ta e="T17" id="Seg_2907" s="T11">У этой женщины было трое маленьких детей.</ta>
            <ta e="T25" id="Seg_2908" s="T17">Муж этой женщины где-то работал, ногу сломал.</ta>
            <ta e="T32" id="Seg_2909" s="T25">[Пока] нога болела, он не работал полгода.</ta>
            <ta e="T35" id="Seg_2910" s="T32">Эта женщина одна работала.</ta>
            <ta e="T42" id="Seg_2911" s="T35">А брат мужа бездельничал, всё время дома сидел.</ta>
            <ta e="T49" id="Seg_2912" s="T42">Он нигде работать не хотел.</ta>
            <ta e="T52" id="Seg_2913" s="T49">Женщина сама работала.</ta>
            <ta e="T61" id="Seg_2914" s="T52">Их жило шесть или семь человек (/шесть человек).</ta>
            <ta e="T64" id="Seg_2915" s="T61">Женщина одна работала.</ta>
            <ta e="T68" id="Seg_2916" s="T64">Вечно голодные сидели (/сидят).</ta>
            <ta e="T72" id="Seg_2917" s="T68">Денег у них не было, чтобы еду покупать.</ta>
            <ta e="T77" id="Seg_2918" s="T72">Потом женщина заболела (/та заболела).</ta>
            <ta e="T79" id="Seg_2919" s="T77">[Её] положили в больницу.</ta>
            <ta e="T95" id="Seg_2920" s="T79">Она в больницу то ли на месяц попала (/лежала), то ли месяц лежала, то ли два месяца лежала.</ta>
            <ta e="T100" id="Seg_2921" s="T95">[Лёгкие?] заболели, у этой женщины.</ta>
            <ta e="T114" id="Seg_2922" s="T100">В больницу пришли мать мужа, брат мужа и трое детей (/с маленькими детьми), чтобы её позвать домой .</ta>
            <ta e="T120" id="Seg_2923" s="T114">Женщина говорит: "Я пока болею.</ta>
            <ta e="T129" id="Seg_2924" s="T120">[Лёгкие?] пока болят, сегодня домой не пойду".</ta>
            <ta e="T135" id="Seg_2925" s="T129">Потом эта женщина к людям в больнице привыкла.</ta>
            <ta e="T137" id="Seg_2926" s="T135">Хорошо стала жить. </ta>
            <ta e="T154" id="Seg_2927" s="T137">Женщины ей, этой женщине, стали [всё] давать, этой женщине, эти женщины этой женщине стали [всё] давать: платье, одежду, шапки ― всё.</ta>
            <ta e="T161" id="Seg_2928" s="T154">Потом эта женщина в больнице недолго была.</ta>
            <ta e="T163" id="Seg_2929" s="T161">Её домой отправили.</ta>
            <ta e="T170" id="Seg_2930" s="T163">Раньше эта женщина никуда не ходила.</ta>
            <ta e="T174" id="Seg_2931" s="T170">Потом, поправившись, стала выходить.</ta>
            <ta e="T179" id="Seg_2932" s="T174">Выскочив на улицу, куда попало уходит.</ta>
            <ta e="T198" id="Seg_2933" s="T179">Потом врач сказал: "На днях (сегодня-завтра) мы тебя домой выпишем.</ta>
            <ta e="T202" id="Seg_2934" s="T198">Тебе сейчас лучше.</ta>
            <ta e="T211" id="Seg_2935" s="T202">Если домой приедешь, поживи дома, с десяток дней поживи дома.</ta>
            <ta e="T214" id="Seg_2936" s="T211">На работу не ходи.</ta>
            <ta e="T218" id="Seg_2937" s="T214">Если приедешь домой, сиди дома".</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T2" id="Seg_2938" s="T0">"A brief vacation".</ta>
            <ta e="T3" id="Seg_2939" s="T2">A movie.</ta>
            <ta e="T8" id="Seg_2940" s="T3">In Italy, in other governmental countries (…).</ta>
            <ta e="T11" id="Seg_2941" s="T8">There lived a married woman.</ta>
            <ta e="T17" id="Seg_2942" s="T11">This woman had three small children.</ta>
            <ta e="T25" id="Seg_2943" s="T17">This woman's husband worked somewhere, he broke his leg.</ta>
            <ta e="T32" id="Seg_2944" s="T25">[While] his leg hurt, he didn't work for half a year.</ta>
            <ta e="T35" id="Seg_2945" s="T32">This woman worked.</ta>
            <ta e="T42" id="Seg_2946" s="T35">And her husband's brother loafed, he spent the whole time at home.</ta>
            <ta e="T49" id="Seg_2947" s="T42">He didn't want to work anywhere.</ta>
            <ta e="T52" id="Seg_2948" s="T49">Only the woman worked.</ta>
            <ta e="T61" id="Seg_2949" s="T52">They were six or seven people.</ta>
            <ta e="T64" id="Seg_2950" s="T61">Only the woman worked.</ta>
            <ta e="T68" id="Seg_2951" s="T64">They always were hungry.</ta>
            <ta e="T72" id="Seg_2952" s="T68">They didn't have money to buy food.</ta>
            <ta e="T77" id="Seg_2953" s="T72">Then the woman got sick.</ta>
            <ta e="T79" id="Seg_2954" s="T77">They brought her to the hospital.</ta>
            <ta e="T95" id="Seg_2955" s="T79">She got into hospital for one month, she stayed there one or two months.</ta>
            <ta e="T100" id="Seg_2956" s="T95">That woman's [lungs] hurt.</ta>
            <ta e="T114" id="Seg_2957" s="T100">Her husband's mother, his brother and her three small children came to the hospital to call her back home.</ta>
            <ta e="T120" id="Seg_2958" s="T114">The woman says: "I am still sick.</ta>
            <ta e="T129" id="Seg_2959" s="T120">My [lungs] still hurt, today I won't go home".</ta>
            <ta e="T135" id="Seg_2960" s="T129">Then this woman got used to the hospital.</ta>
            <ta e="T137" id="Seg_2961" s="T135">She started to live well.</ta>
            <ta e="T154" id="Seg_2962" s="T137">Women started to give this woman [everything], those women started to give that woman: dress, clothes, hats ― everything. </ta>
            <ta e="T161" id="Seg_2963" s="T154">Then this woman didn't stay long in the hospital.</ta>
            <ta e="T163" id="Seg_2964" s="T161">They sent her home.</ta>
            <ta e="T170" id="Seg_2965" s="T163">Earlier this woman didn't go anywhere.</ta>
            <ta e="T174" id="Seg_2966" s="T170">After she got better, she started to go out.</ta>
            <ta e="T179" id="Seg_2967" s="T174">She ran out and went somewhere.</ta>
            <ta e="T198" id="Seg_2968" s="T179">Then the doctor said: "These days (today, tomorrow) we will discharge you.</ta>
            <ta e="T202" id="Seg_2969" s="T198">You are good now.</ta>
            <ta e="T211" id="Seg_2970" s="T202">If you come home, stay for a while at home, around ten days.</ta>
            <ta e="T214" id="Seg_2971" s="T211">Don't go to work.</ta>
            <ta e="T218" id="Seg_2972" s="T214">If you go home, stay at home".</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2" id="Seg_2973" s="T0">"Ein kurzer Urlaub."</ta>
            <ta e="T3" id="Seg_2974" s="T2">Ein Film.</ta>
            <ta e="T8" id="Seg_2975" s="T3">In Italien, in anderen Regierungsländern (…).</ta>
            <ta e="T11" id="Seg_2976" s="T8">Es lebte eine verheiratete Frau.</ta>
            <ta e="T17" id="Seg_2977" s="T11">Diese Frau hatte drei kleine Kinder.</ta>
            <ta e="T25" id="Seg_2978" s="T17">Der Mann dieser Frau arbeitete irgendwo, er brach sich ein Bein.</ta>
            <ta e="T32" id="Seg_2979" s="T25">[Während] sein Bein tat weh, er arbeitete ein halbes Jahr lang nicht.</ta>
            <ta e="T35" id="Seg_2980" s="T32">Diese Frau arbeitete.</ta>
            <ta e="T42" id="Seg_2981" s="T35">Und der Bruder ihres Mannes faulenzte, er blieb die ganze Zeit zuhause.</ta>
            <ta e="T49" id="Seg_2982" s="T42">Er wollte nirgendwo arbeiten.</ta>
            <ta e="T52" id="Seg_2983" s="T49">Nur die Frau arbeitete.</ta>
            <ta e="T61" id="Seg_2984" s="T52">Es waren sechs oder sieben Menschen.</ta>
            <ta e="T64" id="Seg_2985" s="T61">Nur die Frau arbeitete.</ta>
            <ta e="T68" id="Seg_2986" s="T64">Sie waren immer hungrig.</ta>
            <ta e="T72" id="Seg_2987" s="T68">Sie hatten kein Geld, um Essen zu kaufen.</ta>
            <ta e="T77" id="Seg_2988" s="T72">Dann wurde die Frau krank.</ta>
            <ta e="T79" id="Seg_2989" s="T77">Sie wurde ins Krankenhaus gebracht.</ta>
            <ta e="T95" id="Seg_2990" s="T79">Sie kam für einen Monat ins Krankenhaus, einen oder zwei Monate lag sie [dort].</ta>
            <ta e="T100" id="Seg_2991" s="T95">[Die Lungen] dieser Frau waren krank.</ta>
            <ta e="T114" id="Seg_2992" s="T100">Die Mutter ihres Mannes, sein Bruder und ihre drei kleinen Kinder kamen, um sie nach Hause zu holen.</ta>
            <ta e="T120" id="Seg_2993" s="T114">Die Frau sagt: "Ich bin noch krank.</ta>
            <ta e="T129" id="Seg_2994" s="T120">Meine [Lungen] sind noch krank, heute gehe ich nicht nach Hause."</ta>
            <ta e="T135" id="Seg_2995" s="T129">Dann gewöhnte sich die Frau an das Krankenhaus.</ta>
            <ta e="T137" id="Seg_2996" s="T135">Sie fing an gut zu leben.</ta>
            <ta e="T154" id="Seg_2997" s="T137">Frauen fingen an, dieser Frau [alles] zu geben, die Frauen gaben der Frau: Kleider, Kleidung, Mützen ― alles.</ta>
            <ta e="T161" id="Seg_2998" s="T154">Dann blieb die Frau nicht mehr lange im Krankenhaus.</ta>
            <ta e="T163" id="Seg_2999" s="T161">Sie wurde nach Hause geschickt.</ta>
            <ta e="T170" id="Seg_3000" s="T163">Früher war diese Frau nirgendwohin gegangen.</ta>
            <ta e="T174" id="Seg_3001" s="T170">Als es ihr besser ging, fing sie an auszugehen.</ta>
            <ta e="T179" id="Seg_3002" s="T174">Sie lief hinaus und ging irgendwohin.</ta>
            <ta e="T198" id="Seg_3003" s="T179">Dann sagte der Arzt: "Die Tage (heute, morgen) werden wir dich entlassen.</ta>
            <ta e="T202" id="Seg_3004" s="T198">Jetzt geht es dir gut.</ta>
            <ta e="T211" id="Seg_3005" s="T202">Wenn du nach Hause kommst, bleib ungefähr zehn Tage zuhause.</ta>
            <ta e="T214" id="Seg_3006" s="T211">Geh nicht arbeiten.</ta>
            <ta e="T218" id="Seg_3007" s="T214">Wenn du nach Hause gehst, bleib zuhause."</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T8" id="Seg_3008" s="T3">в Италии в других правительственных странах.</ta>
            <ta e="T11" id="Seg_3009" s="T8">женщина жила замужняя.</ta>
            <ta e="T17" id="Seg_3010" s="T11">у этой женщины 3 маленьких ребятишки был</ta>
            <ta e="T25" id="Seg_3011" s="T17">этой женщины муж где-то рабтал, ногу (свою) сломал.</ta>
            <ta e="T32" id="Seg_3012" s="T25">нога болела, он не работал. с полгода.</ta>
            <ta e="T35" id="Seg_3013" s="T32">женщина одна работала</ta>
            <ta e="T42" id="Seg_3014" s="T35">брат мужа лентяйничал, дома всегда сидели</ta>
            <ta e="T49" id="Seg_3015" s="T42">ни куда (где) работать не хотел </ta>
            <ta e="T52" id="Seg_3016" s="T49">жена сама работала</ta>
            <ta e="T61" id="Seg_3017" s="T52">их жило 6 или семь человек</ta>
            <ta e="T64" id="Seg_3018" s="T61">жена одна работала</ta>
            <ta e="T68" id="Seg_3019" s="T64">вечно голодные сидели</ta>
            <ta e="T72" id="Seg_3020" s="T68">денег не было у них продуктов брать</ta>
            <ta e="T77" id="Seg_3021" s="T72">потом женщина заболела</ta>
            <ta e="T79" id="Seg_3022" s="T77">в больницу ее положили</ta>
            <ta e="T95" id="Seg_3023" s="T79">она в больнице месяц лежала или два месяца</ta>
            <ta e="T100" id="Seg_3024" s="T95">легкие болели. у этой женщины</ta>
            <ta e="T114" id="Seg_3025" s="T100">домой звать (чтоб приехала)</ta>
            <ta e="T135" id="Seg_3026" s="T129">к людям привыкла.</ta>
            <ta e="T137" id="Seg_3027" s="T135">хорошо стала жить </ta>
            <ta e="T154" id="Seg_3028" s="T137">женщины ей этой женщине стали все давать, эти женщины этой женщине платье (рубаха), пальто шапку (всё) (всё короче говоря)</ta>
            <ta e="T161" id="Seg_3029" s="T154">Потом эта женщина в больнице не долго была</ta>
            <ta e="T163" id="Seg_3030" s="T161">домой отправили</ta>
            <ta e="T170" id="Seg_3031" s="T163">раньше эта женщина никуда не ходила</ta>
            <ta e="T174" id="Seg_3032" s="T170">после того как она немного поправилась [поправившись] стала ходит.</ta>
            <ta e="T179" id="Seg_3033" s="T174">выйдя на улицу куда попало уходила.</ta>
            <ta e="T198" id="Seg_3034" s="T179">Потом доктор врач так сказал на днях домой тебя выпишtм</ta>
            <ta e="T202" id="Seg_3035" s="T198">ты сейчас ничего стала.</ta>
            <ta e="T211" id="Seg_3036" s="T202">Домой приедеш, дома живи, с десяток дней поживи дома.</ta>
            <ta e="T214" id="Seg_3037" s="T211">сразу на работу не ходи.</ta>
            <ta e="T218" id="Seg_3038" s="T214">домой приедешь, дома сиди.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T8" id="Seg_3039" s="T3">[OSV:] 1) the form "ontɨlqut" has been edited into "ɔːmtɨlʼ qoːt"; 2) "ɔːmtɨlʼ qoːt" - "a tsar"; 3) the sentence is not finished.</ta>
            <ta e="T17" id="Seg_3040" s="T11">[OSV:] " ‎iːmannɨqɨt" has been edited into " ‎‎iːman mɨqɨt".</ta>
            <ta e="T35" id="Seg_3041" s="T32">[OSV:] unclear pronominal form "ottɨqɨntɨ" (see also 2.11), it is not clear if here is Locative, the further investigation is needed.</ta>
            <ta e="T100" id="Seg_3042" s="T95">[OSV:] Unclear word combination "pu:qa šünčʼitɨ" (blow-INF inside-NOM-3SG).</ta>
            <ta e="T129" id="Seg_3043" s="T120">[OSV:] Unclear word combination "pu:qa šünčʼon" (blow-INF inside-ADV.LOC). </ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
