<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KMS_1963_Owl_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KMS_1963_EagleOwl_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">98</ud-information>
            <ud-information attribute-name="# HIAT:w">73</ud-information>
            <ud-information attribute-name="# e">73</ud-information>
            <ud-information attribute-name="# HIAT:u">13</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KMS">
            <abbreviation>KMS</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KMS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T74" id="Seg_0" n="sc" s="T1">
               <ts e="T2" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Püːja</ts>
                  <nts id="Seg_5" n="HIAT:ip">.</nts>
                  <nts id="Seg_6" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_8" n="HIAT:u" s="T2">
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">Me</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">maːdʼi</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">čwätilaɣɨnɨt</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">eːkwattə</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">püːjala</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">Okkɨrɨŋ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">me</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">ilʼdʼäwse</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">qwässo</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">qaːmbaj</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">čwäčondə</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">suːrujgu</ts>
                  <nts id="Seg_47" n="HIAT:ip">,</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">nʼäjajgu</ts>
                  <nts id="Seg_51" n="HIAT:ip">.</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_54" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">Nɨːtʼän</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">ilʼdʼäwnan</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">äːssan</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">lʼeːwü</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">maːttə</ts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_72" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">Me</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_77" n="HIAT:w" s="T21">na</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_80" n="HIAT:w" s="T22">maːtqɨn</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_83" n="HIAT:w" s="T23">illɨkuzo</ts>
                  <nts id="Seg_84" n="HIAT:ip">,</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_87" n="HIAT:w" s="T24">mɨmbɨkuzo</ts>
                  <nts id="Seg_88" n="HIAT:ip">,</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_91" n="HIAT:w" s="T25">nʼäjajguzo</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_95" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">Üːdəmɨn</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_100" n="HIAT:w" s="T27">tüːkkuzo</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_103" n="HIAT:w" s="T28">lʼewü</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_106" n="HIAT:w" s="T29">maːtqanɨ</ts>
                  <nts id="Seg_107" n="HIAT:ip">.</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_110" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_112" n="HIAT:w" s="T30">Qajdaka</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_115" n="HIAT:w" s="T31">dʼel</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_118" n="HIAT:w" s="T32">man</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_121" n="HIAT:w" s="T33">tida</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_124" n="HIAT:w" s="T34">awilǯimbaw</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_127" n="HIAT:w" s="T35">nuːnumbitäw</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_130" n="HIAT:w" s="T36">man</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_133" n="HIAT:w" s="T37">tida</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_136" n="HIAT:w" s="T38">tinnäwaw</ts>
                  <nts id="Seg_137" n="HIAT:ip">.</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_140" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_142" n="HIAT:w" s="T39">üːdəmɨɣɨnnä</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_145" n="HIAT:w" s="T40">man</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_148" n="HIAT:w" s="T41">nʼüːilʼe</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_151" n="HIAT:w" s="T42">qondämbaŋ</ts>
                  <nts id="Seg_152" n="HIAT:ip">.</nts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_155" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_157" n="HIAT:w" s="T43">Nɨŋga</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_160" n="HIAT:w" s="T44">sittɨlʼe</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_163" n="HIAT:w" s="T45">oldaŋ</ts>
                  <nts id="Seg_164" n="HIAT:ip">.</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_167" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_169" n="HIAT:w" s="T46">Qajdaka</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_172" n="HIAT:w" s="T47">pizänʼnʼe</ts>
                  <nts id="Seg_173" n="HIAT:ip">,</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_176" n="HIAT:w" s="T48">üːtčeːdər</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_179" n="HIAT:w" s="T49">tʼürɨŋ</ts>
                  <nts id="Seg_180" n="HIAT:ip">,</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_183" n="HIAT:w" s="T50">kanadər</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_186" n="HIAT:w" s="T51">muːdɨŋ</ts>
                  <nts id="Seg_187" n="HIAT:ip">,</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_190" n="HIAT:w" s="T52">qwärɣədər</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_193" n="HIAT:w" s="T53">qaːrɨnʼe</ts>
                  <nts id="Seg_194" n="HIAT:ip">,</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_197" n="HIAT:w" s="T54">sümdɨmba</ts>
                  <nts id="Seg_198" n="HIAT:ip">.</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_201" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_203" n="HIAT:w" s="T55">Nɨŋa</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_206" n="HIAT:w" s="T56">man</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_209" n="HIAT:w" s="T57">čiuaŋ</ts>
                  <nts id="Seg_210" n="HIAT:ip">,</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_213" n="HIAT:w" s="T58">nännə</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_216" n="HIAT:w" s="T59">aj</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_219" n="HIAT:w" s="T60">qondämbaŋ</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_222" n="HIAT:w" s="T61">to</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_225" n="HIAT:w" s="T62">qarottə</ts>
                  <nts id="Seg_226" n="HIAT:ip">.</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_229" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_231" n="HIAT:w" s="T63">Qariməɣɨn</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_234" n="HIAT:w" s="T64">ilʼdʼäwki</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_237" n="HIAT:w" s="T65">kätkulʼe</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_240" n="HIAT:w" s="T66">oːlʼdaw</ts>
                  <nts id="Seg_241" n="HIAT:ip">.</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_244" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_246" n="HIAT:w" s="T67">Ilʼdʼäw</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_249" n="HIAT:w" s="T68">meŋga</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_252" n="HIAT:w" s="T69">tʼärɨŋ</ts>
                  <nts id="Seg_253" n="HIAT:ip">:</nts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_256" n="HIAT:w" s="T70">Na</ts>
                  <nts id="Seg_257" n="HIAT:ip">,</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_260" n="HIAT:w" s="T71">taba</ts>
                  <nts id="Seg_261" n="HIAT:ip">,</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_264" n="HIAT:w" s="T72">imat</ts>
                  <nts id="Seg_265" n="HIAT:ip">,</nts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_268" n="HIAT:w" s="T73">püːja</ts>
                  <nts id="Seg_269" n="HIAT:ip">.</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T74" id="Seg_271" n="sc" s="T1">
               <ts e="T2" id="Seg_273" n="e" s="T1">Püːja. </ts>
               <ts e="T3" id="Seg_275" n="e" s="T2">Me </ts>
               <ts e="T4" id="Seg_277" n="e" s="T3">maːdʼi </ts>
               <ts e="T5" id="Seg_279" n="e" s="T4">čwätilaɣɨnɨt </ts>
               <ts e="T6" id="Seg_281" n="e" s="T5">eːkwattə </ts>
               <ts e="T7" id="Seg_283" n="e" s="T6">püːjala. </ts>
               <ts e="T8" id="Seg_285" n="e" s="T7">Okkɨrɨŋ </ts>
               <ts e="T9" id="Seg_287" n="e" s="T8">me </ts>
               <ts e="T10" id="Seg_289" n="e" s="T9">ilʼdʼäwse </ts>
               <ts e="T11" id="Seg_291" n="e" s="T10">qwässo </ts>
               <ts e="T12" id="Seg_293" n="e" s="T11">qaːmbaj </ts>
               <ts e="T13" id="Seg_295" n="e" s="T12">čwäčondə </ts>
               <ts e="T14" id="Seg_297" n="e" s="T13">suːrujgu, </ts>
               <ts e="T15" id="Seg_299" n="e" s="T14">nʼäjajgu. </ts>
               <ts e="T16" id="Seg_301" n="e" s="T15">Nɨːtʼän </ts>
               <ts e="T17" id="Seg_303" n="e" s="T16">ilʼdʼäwnan </ts>
               <ts e="T18" id="Seg_305" n="e" s="T17">äːssan </ts>
               <ts e="T19" id="Seg_307" n="e" s="T18">lʼeːwü </ts>
               <ts e="T20" id="Seg_309" n="e" s="T19">maːttə. </ts>
               <ts e="T21" id="Seg_311" n="e" s="T20">Me </ts>
               <ts e="T22" id="Seg_313" n="e" s="T21">na </ts>
               <ts e="T23" id="Seg_315" n="e" s="T22">maːtqɨn </ts>
               <ts e="T24" id="Seg_317" n="e" s="T23">illɨkuzo, </ts>
               <ts e="T25" id="Seg_319" n="e" s="T24">mɨmbɨkuzo, </ts>
               <ts e="T26" id="Seg_321" n="e" s="T25">nʼäjajguzo. </ts>
               <ts e="T27" id="Seg_323" n="e" s="T26">Üːdəmɨn </ts>
               <ts e="T28" id="Seg_325" n="e" s="T27">tüːkkuzo </ts>
               <ts e="T29" id="Seg_327" n="e" s="T28">lʼewü </ts>
               <ts e="T30" id="Seg_329" n="e" s="T29">maːtqanɨ. </ts>
               <ts e="T31" id="Seg_331" n="e" s="T30">Qajdaka </ts>
               <ts e="T32" id="Seg_333" n="e" s="T31">dʼel </ts>
               <ts e="T33" id="Seg_335" n="e" s="T32">man </ts>
               <ts e="T34" id="Seg_337" n="e" s="T33">tida </ts>
               <ts e="T35" id="Seg_339" n="e" s="T34">awilǯimbaw </ts>
               <ts e="T36" id="Seg_341" n="e" s="T35">nuːnumbitäw </ts>
               <ts e="T37" id="Seg_343" n="e" s="T36">man </ts>
               <ts e="T38" id="Seg_345" n="e" s="T37">tida </ts>
               <ts e="T39" id="Seg_347" n="e" s="T38">tinnäwaw. </ts>
               <ts e="T40" id="Seg_349" n="e" s="T39">üːdəmɨɣɨnnä </ts>
               <ts e="T41" id="Seg_351" n="e" s="T40">man </ts>
               <ts e="T42" id="Seg_353" n="e" s="T41">nʼüːilʼe </ts>
               <ts e="T43" id="Seg_355" n="e" s="T42">qondämbaŋ. </ts>
               <ts e="T44" id="Seg_357" n="e" s="T43">Nɨŋga </ts>
               <ts e="T45" id="Seg_359" n="e" s="T44">sittɨlʼe </ts>
               <ts e="T46" id="Seg_361" n="e" s="T45">oldaŋ. </ts>
               <ts e="T47" id="Seg_363" n="e" s="T46">Qajdaka </ts>
               <ts e="T48" id="Seg_365" n="e" s="T47">pizänʼnʼe, </ts>
               <ts e="T49" id="Seg_367" n="e" s="T48">üːtčeːdər </ts>
               <ts e="T50" id="Seg_369" n="e" s="T49">tʼürɨŋ, </ts>
               <ts e="T51" id="Seg_371" n="e" s="T50">kanadər </ts>
               <ts e="T52" id="Seg_373" n="e" s="T51">muːdɨŋ, </ts>
               <ts e="T53" id="Seg_375" n="e" s="T52">qwärɣədər </ts>
               <ts e="T54" id="Seg_377" n="e" s="T53">qaːrɨnʼe, </ts>
               <ts e="T55" id="Seg_379" n="e" s="T54">sümdɨmba. </ts>
               <ts e="T56" id="Seg_381" n="e" s="T55">Nɨŋa </ts>
               <ts e="T57" id="Seg_383" n="e" s="T56">man </ts>
               <ts e="T58" id="Seg_385" n="e" s="T57">čiuaŋ, </ts>
               <ts e="T59" id="Seg_387" n="e" s="T58">nännə </ts>
               <ts e="T60" id="Seg_389" n="e" s="T59">aj </ts>
               <ts e="T61" id="Seg_391" n="e" s="T60">qondämbaŋ </ts>
               <ts e="T62" id="Seg_393" n="e" s="T61">to </ts>
               <ts e="T63" id="Seg_395" n="e" s="T62">qarottə. </ts>
               <ts e="T64" id="Seg_397" n="e" s="T63">Qariməɣɨn </ts>
               <ts e="T65" id="Seg_399" n="e" s="T64">ilʼdʼäwki </ts>
               <ts e="T66" id="Seg_401" n="e" s="T65">kätkulʼe </ts>
               <ts e="T67" id="Seg_403" n="e" s="T66">oːlʼdaw. </ts>
               <ts e="T68" id="Seg_405" n="e" s="T67">Ilʼdʼäw </ts>
               <ts e="T69" id="Seg_407" n="e" s="T68">meŋga </ts>
               <ts e="T70" id="Seg_409" n="e" s="T69">tʼärɨŋ: </ts>
               <ts e="T71" id="Seg_411" n="e" s="T70">Na, </ts>
               <ts e="T72" id="Seg_413" n="e" s="T71">taba, </ts>
               <ts e="T73" id="Seg_415" n="e" s="T72">imat, </ts>
               <ts e="T74" id="Seg_417" n="e" s="T73">püːja. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2" id="Seg_418" s="T1">KMS_1963_EagleOwl_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_419" s="T2">KMS_1963_EagleOwl_nar.002 (001.002)</ta>
            <ta e="T15" id="Seg_420" s="T7">KMS_1963_EagleOwl_nar.003 (001.003)</ta>
            <ta e="T20" id="Seg_421" s="T15">KMS_1963_EagleOwl_nar.004 (001.004)</ta>
            <ta e="T26" id="Seg_422" s="T20">KMS_1963_EagleOwl_nar.005 (001.005)</ta>
            <ta e="T30" id="Seg_423" s="T26">KMS_1963_EagleOwl_nar.006 (001.006)</ta>
            <ta e="T39" id="Seg_424" s="T30">KMS_1963_EagleOwl_nar.007 (001.007)</ta>
            <ta e="T43" id="Seg_425" s="T39">KMS_1963_EagleOwl_nar.008 (001.008)</ta>
            <ta e="T46" id="Seg_426" s="T43">KMS_1963_EagleOwl_nar.009 (001.009)</ta>
            <ta e="T55" id="Seg_427" s="T46">KMS_1963_EagleOwl_nar.010 (001.010)</ta>
            <ta e="T63" id="Seg_428" s="T55">KMS_1963_EagleOwl_nar.011 (001.011)</ta>
            <ta e="T67" id="Seg_429" s="T63">KMS_1963_EagleOwl_nar.012 (001.012)</ta>
            <ta e="T74" id="Seg_430" s="T67">KMS_1963_EagleOwl_nar.013 (001.013)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T2" id="Seg_431" s="T1">пӱ̄jа</ta>
            <ta e="T7" id="Seg_432" s="T2">ме ма̄′дʼи ′тшвӓтилаɣыныт е̄кваттъ пӱ̄jала</ta>
            <ta e="T15" id="Seg_433" s="T7">okkyryң me</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T2" id="Seg_434" s="T1">Püːja.</ta>
            <ta e="T7" id="Seg_435" s="T2">мe maːdʼi tšvätilaɣɨnɨt eːkwattə püːjala.</ta>
            <ta e="T15" id="Seg_436" s="T7">okkɨrɨŋ me ilʼdʼäwse qwässo qaːmbaj tšwätšondə suːrujgu, nʼäjajgu.</ta>
            <ta e="T20" id="Seg_437" s="T15">Nɨːtʼän ilʼdʼäwnan äːssan lʼeːwü maːttə.</ta>
            <ta e="T26" id="Seg_438" s="T20">Me na maːtqɨn illɨkuzo, peː mɨmbɨkuzo, nʼäjajguzo.</ta>
            <ta e="T30" id="Seg_439" s="T26">Üːdəmɨn tüːkkuzo lʼewü maːtqanɨ.</ta>
            <ta e="T39" id="Seg_440" s="T30">Qajdaka dʼel man tida äwilǯimbaw nuːnum (nuːnɨm) bɨtäw man tida tinnäwaw</ta>
            <ta e="T43" id="Seg_441" s="T39">üːdəmɨɣɨnnä man nʼüːilʼe qondämbaŋ</ta>
            <ta e="T46" id="Seg_442" s="T43">nɨŋga sittɨlʼe oldaŋ</ta>
            <ta e="T55" id="Seg_443" s="T46">Qajdaka pizänʼnʼe, üːttšeːdər tʼürɨŋ, kanadər muːdɨŋ, qwärɣədər qaːrɨnʼe, sümdɨmba.</ta>
            <ta e="T63" id="Seg_444" s="T55">nɨŋa man tšiuaŋ, nännə aj qondämbaŋ to qarottə.</ta>
            <ta e="T67" id="Seg_445" s="T63">qariməɣɨn ilʼdʼäwki kätkulʼe oːlʼdaw.</ta>
            <ta e="T74" id="Seg_446" s="T67">Ilʼdʼäw meŋga tʼärɨŋ na, taba, imat, püːja.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2" id="Seg_447" s="T1">Püːja. </ta>
            <ta e="T7" id="Seg_448" s="T2">Me maːdʼi čwätilaɣɨnɨt eːkwattə püːjala. </ta>
            <ta e="T15" id="Seg_449" s="T7">Okkɨrɨŋ me ilʼdʼäwse qwässo qaːmbaj čwäčondə suːrujgu, nʼäjajgu. </ta>
            <ta e="T20" id="Seg_450" s="T15">Nɨːtʼän ilʼdʼäwnan äːssan lʼeːwü maːttə. </ta>
            <ta e="T26" id="Seg_451" s="T20">Me na maːtqɨn illɨkuzo, mɨmbɨkuzo, nʼäjajguzo. </ta>
            <ta e="T30" id="Seg_452" s="T26">Üːdəmɨn tüːkkuzo lʼewü maːtqanɨ. </ta>
            <ta e="T39" id="Seg_453" s="T30">Qajdaka dʼel man tida awilǯimbaw nuːnumbitäw man tida tinnäwaw. </ta>
            <ta e="T43" id="Seg_454" s="T39">üːdəmɨɣɨnnä man nʼüːilʼe qondämbaŋ. </ta>
            <ta e="T46" id="Seg_455" s="T43">Nɨŋga sittɨlʼe oldaŋ. </ta>
            <ta e="T55" id="Seg_456" s="T46">Qajdaka pizänʼnʼe, üːtčeːdər tʼürɨŋ, kanadər muːdɨŋ, qwärɣədər qaːrɨnʼe, sümdɨmba. </ta>
            <ta e="T63" id="Seg_457" s="T55">Nɨŋa man čiuaŋ, nännə aj qondämbaŋ to qarottə. </ta>
            <ta e="T67" id="Seg_458" s="T63">Qariməɣɨn ilʼdʼäwki kätkulʼe oːlʼdaw. </ta>
            <ta e="T74" id="Seg_459" s="T67">Ilʼdʼäw meŋga tʼärɨŋ: Na, taba, imat, püːja. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_460" s="T1">püːja</ta>
            <ta e="T3" id="Seg_461" s="T2">me</ta>
            <ta e="T4" id="Seg_462" s="T3">maːdʼi</ta>
            <ta e="T5" id="Seg_463" s="T4">čwät-i-la-ɣɨnɨt</ta>
            <ta e="T6" id="Seg_464" s="T5">eː-kwa-ttə</ta>
            <ta e="T7" id="Seg_465" s="T6">püːja-la</ta>
            <ta e="T8" id="Seg_466" s="T7">okkɨr-ɨ-ŋ</ta>
            <ta e="T9" id="Seg_467" s="T8">me</ta>
            <ta e="T10" id="Seg_468" s="T9">ilʼdʼä-w-se</ta>
            <ta e="T11" id="Seg_469" s="T10">qwäs-s-o</ta>
            <ta e="T12" id="Seg_470" s="T11">qaːmba-j</ta>
            <ta e="T13" id="Seg_471" s="T12">čwäč-o-ndə</ta>
            <ta e="T14" id="Seg_472" s="T13">suːru-j-gu</ta>
            <ta e="T15" id="Seg_473" s="T14">nʼäja-j-gu</ta>
            <ta e="T16" id="Seg_474" s="T15">nɨːtʼä-n</ta>
            <ta e="T17" id="Seg_475" s="T16">ilʼdʼä-w-nan</ta>
            <ta e="T18" id="Seg_476" s="T17">äː-ssa-n</ta>
            <ta e="T19" id="Seg_477" s="T18">lʼeːwü</ta>
            <ta e="T20" id="Seg_478" s="T19">maːt-tə</ta>
            <ta e="T21" id="Seg_479" s="T20">me</ta>
            <ta e="T22" id="Seg_480" s="T21">na</ta>
            <ta e="T23" id="Seg_481" s="T22">maːt-qən</ta>
            <ta e="T24" id="Seg_482" s="T23">illɨ-ku-z-o</ta>
            <ta e="T25" id="Seg_483" s="T24">mɨm-bɨ-ku-z-o</ta>
            <ta e="T26" id="Seg_484" s="T25">nʼäja-j-gu-z-o</ta>
            <ta e="T27" id="Seg_485" s="T26">üːdə-mɨn</ta>
            <ta e="T28" id="Seg_486" s="T27">tüː-ku-z-o</ta>
            <ta e="T29" id="Seg_487" s="T28">lʼewü</ta>
            <ta e="T30" id="Seg_488" s="T29">maːt-qa-nɨ</ta>
            <ta e="T31" id="Seg_489" s="T30">qaj-daka</ta>
            <ta e="T32" id="Seg_490" s="T31">dʼel</ta>
            <ta e="T33" id="Seg_491" s="T32">man</ta>
            <ta e="T34" id="Seg_492" s="T33">tida</ta>
            <ta e="T35" id="Seg_493" s="T34">äwɨlǯi-mba-w</ta>
            <ta e="T36" id="Seg_494" s="T35">nuːnumbitä-w</ta>
            <ta e="T37" id="Seg_495" s="T36">man</ta>
            <ta e="T38" id="Seg_496" s="T37">tida</ta>
            <ta e="T39" id="Seg_497" s="T38">tinnäwa-w</ta>
            <ta e="T40" id="Seg_498" s="T39">üːdə-mɨ-ɣɨn-nä</ta>
            <ta e="T41" id="Seg_499" s="T40">man</ta>
            <ta e="T42" id="Seg_500" s="T41">nʼüːi-lʼe</ta>
            <ta e="T43" id="Seg_501" s="T42">qondä-mba-ŋ</ta>
            <ta e="T44" id="Seg_502" s="T43">nɨŋga</ta>
            <ta e="T45" id="Seg_503" s="T44">sittɨ-lʼe</ta>
            <ta e="T46" id="Seg_504" s="T45">olda-ŋ</ta>
            <ta e="T47" id="Seg_505" s="T46">qaj-daka</ta>
            <ta e="T48" id="Seg_506" s="T47">pizä-nʼ-nʼe</ta>
            <ta e="T49" id="Seg_507" s="T48">üːtčeː-dər</ta>
            <ta e="T50" id="Seg_508" s="T49">tʼürɨ-ŋ</ta>
            <ta e="T51" id="Seg_509" s="T50">kana-dər</ta>
            <ta e="T52" id="Seg_510" s="T51">muːd-ɨ-ŋ</ta>
            <ta e="T53" id="Seg_511" s="T52">qwärɣə-dər</ta>
            <ta e="T54" id="Seg_512" s="T53">qaːrɨ-nʼe</ta>
            <ta e="T55" id="Seg_513" s="T54">süm-dɨ-mba</ta>
            <ta e="T56" id="Seg_514" s="T55">nɨŋa</ta>
            <ta e="T57" id="Seg_515" s="T56">man</ta>
            <ta e="T58" id="Seg_516" s="T57">čiu-a-ŋ</ta>
            <ta e="T59" id="Seg_517" s="T58">nännə</ta>
            <ta e="T60" id="Seg_518" s="T59">aj</ta>
            <ta e="T61" id="Seg_519" s="T60">qondä-mba-ŋ</ta>
            <ta e="T62" id="Seg_520" s="T61">to</ta>
            <ta e="T63" id="Seg_521" s="T62">qaro-ttə</ta>
            <ta e="T64" id="Seg_522" s="T63">qari-mə-ɣɨn</ta>
            <ta e="T65" id="Seg_523" s="T64">ilʼdʼä-w-ki</ta>
            <ta e="T66" id="Seg_524" s="T65">kät-ku-lʼe</ta>
            <ta e="T67" id="Seg_525" s="T66">oːlʼda-w</ta>
            <ta e="T68" id="Seg_526" s="T67">ilʼdʼä-w</ta>
            <ta e="T69" id="Seg_527" s="T68">meŋga</ta>
            <ta e="T70" id="Seg_528" s="T69">tʼärɨ-n</ta>
            <ta e="T71" id="Seg_529" s="T70">na</ta>
            <ta e="T72" id="Seg_530" s="T71">taba</ta>
            <ta e="T73" id="Seg_531" s="T72">imat</ta>
            <ta e="T74" id="Seg_532" s="T73">püːja</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_533" s="T1">puja</ta>
            <ta e="T3" id="Seg_534" s="T2">meː</ta>
            <ta e="T4" id="Seg_535" s="T3">matʼtʼi</ta>
            <ta e="T5" id="Seg_536" s="T4">tʼwät-ɨ-la-qəntɨ</ta>
            <ta e="T6" id="Seg_537" s="T5">eː-ku-tɨt</ta>
            <ta e="T7" id="Seg_538" s="T6">puja-la</ta>
            <ta e="T8" id="Seg_539" s="T7">okkɨr-ɨ-k</ta>
            <ta e="T9" id="Seg_540" s="T8">meː</ta>
            <ta e="T10" id="Seg_541" s="T9">ildʼa-mɨ-se</ta>
            <ta e="T11" id="Seg_542" s="T10">qwən-sɨ-ut</ta>
            <ta e="T12" id="Seg_543" s="T11">qamba-lʼ</ta>
            <ta e="T13" id="Seg_544" s="T12">tʼwät-ɨ-ndɨ</ta>
            <ta e="T14" id="Seg_545" s="T13">suːrǝm-j-gu</ta>
            <ta e="T15" id="Seg_546" s="T14">nʼaja-j-gu</ta>
            <ta e="T16" id="Seg_547" s="T15">natʼtʼa-n</ta>
            <ta e="T17" id="Seg_548" s="T16">ildʼa-mɨ-nan</ta>
            <ta e="T18" id="Seg_549" s="T17">eː-sɨ-n</ta>
            <ta e="T19" id="Seg_550" s="T18">lewi</ta>
            <ta e="T20" id="Seg_551" s="T19">maːt-tɨ</ta>
            <ta e="T21" id="Seg_552" s="T20">meː</ta>
            <ta e="T22" id="Seg_553" s="T21">na</ta>
            <ta e="T23" id="Seg_554" s="T22">maːt-qən</ta>
            <ta e="T24" id="Seg_555" s="T23">illɨ-ku-sɨ-ut</ta>
            <ta e="T25" id="Seg_556" s="T24">pemɨ-mbɨ-ku-sɨ-ut</ta>
            <ta e="T26" id="Seg_557" s="T25">nʼaja-lʼ-ku-sɨ-ut</ta>
            <ta e="T27" id="Seg_558" s="T26">üːdɨ-mɨn</ta>
            <ta e="T28" id="Seg_559" s="T27">tüː-ku-sɨ-ut</ta>
            <ta e="T29" id="Seg_560" s="T28">lewi</ta>
            <ta e="T30" id="Seg_561" s="T29">maːt-ka-ndɨ</ta>
            <ta e="T31" id="Seg_562" s="T30">qaj-taka</ta>
            <ta e="T32" id="Seg_563" s="T31">tʼeːlɨ</ta>
            <ta e="T33" id="Seg_564" s="T32">man</ta>
            <ta e="T34" id="Seg_565" s="T33">tʼida</ta>
            <ta e="T35" id="Seg_566" s="T34">äwɨlǯi-mbɨ-m</ta>
            <ta e="T36" id="Seg_567" s="T35">nuːnumbitä-m</ta>
            <ta e="T37" id="Seg_568" s="T36">man</ta>
            <ta e="T38" id="Seg_569" s="T37">tʼida</ta>
            <ta e="T39" id="Seg_570" s="T38">tinnewu-m</ta>
            <ta e="T40" id="Seg_571" s="T39">üːdɨ-mɨ-qən-naj</ta>
            <ta e="T41" id="Seg_572" s="T40">man</ta>
            <ta e="T42" id="Seg_573" s="T41">nʼuːi-le</ta>
            <ta e="T43" id="Seg_574" s="T42">qontə-mbɨ-ŋ</ta>
            <ta e="T44" id="Seg_575" s="T43">nik</ta>
            <ta e="T45" id="Seg_576" s="T44">söde-le</ta>
            <ta e="T46" id="Seg_577" s="T45">oldǝ-ŋ</ta>
            <ta e="T47" id="Seg_578" s="T46">qaj-taka</ta>
            <ta e="T48" id="Seg_579" s="T47">pisɨ-š-ŋɨ</ta>
            <ta e="T49" id="Seg_580" s="T48">ütče-tare</ta>
            <ta e="T50" id="Seg_581" s="T49">tʼüːrɨ-n</ta>
            <ta e="T51" id="Seg_582" s="T50">kanak-tare</ta>
            <ta e="T52" id="Seg_583" s="T51">muːt-ɨ-n</ta>
            <ta e="T53" id="Seg_584" s="T52">qwärqa-tare</ta>
            <ta e="T54" id="Seg_585" s="T53">qarrə-ŋɨ</ta>
            <ta e="T55" id="Seg_586" s="T54">šum-tɨ-mbɨ</ta>
            <ta e="T56" id="Seg_587" s="T55">nik</ta>
            <ta e="T57" id="Seg_588" s="T56">man</ta>
            <ta e="T58" id="Seg_589" s="T57">čiu-ŋɨ-ŋ</ta>
            <ta e="T59" id="Seg_590" s="T58">nɨːnɨ</ta>
            <ta e="T60" id="Seg_591" s="T59">aj</ta>
            <ta e="T61" id="Seg_592" s="T60">qontə-mbɨ-ŋ</ta>
            <ta e="T62" id="Seg_593" s="T61">to</ta>
            <ta e="T63" id="Seg_594" s="T62">qarɨ-ndɨ</ta>
            <ta e="T64" id="Seg_595" s="T63">qarɨ-mɨ-qən</ta>
            <ta e="T65" id="Seg_596" s="T64">ildʼa-mɨ-ki</ta>
            <ta e="T66" id="Seg_597" s="T65">kät-ku-le</ta>
            <ta e="T67" id="Seg_598" s="T66">oldǝ-m</ta>
            <ta e="T68" id="Seg_599" s="T67">ildʼa-mɨ</ta>
            <ta e="T69" id="Seg_600" s="T68">mäkkä</ta>
            <ta e="T70" id="Seg_601" s="T69">tʼarɨ-n</ta>
            <ta e="T71" id="Seg_602" s="T70">na</ta>
            <ta e="T72" id="Seg_603" s="T71">taba</ta>
            <ta e="T73" id="Seg_604" s="T72">ijmаt</ta>
            <ta e="T74" id="Seg_605" s="T73">puja</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_606" s="T1">owl.[NOM]</ta>
            <ta e="T3" id="Seg_607" s="T2">we.PL.GEN</ta>
            <ta e="T4" id="Seg_608" s="T3">taiga.[NOM]</ta>
            <ta e="T5" id="Seg_609" s="T4">place-EP-PL-ILL.3SG</ta>
            <ta e="T6" id="Seg_610" s="T5">be-HAB-3PL</ta>
            <ta e="T7" id="Seg_611" s="T6">owl-PL</ta>
            <ta e="T8" id="Seg_612" s="T7">one-EP-ADVZ</ta>
            <ta e="T9" id="Seg_613" s="T8">we.DU.NOM</ta>
            <ta e="T10" id="Seg_614" s="T9">grandfather-1SG-COM</ta>
            <ta e="T11" id="Seg_615" s="T10">go.away-PST-1PL</ta>
            <ta e="T12" id="Seg_616" s="T11">spring-ADJZ</ta>
            <ta e="T13" id="Seg_617" s="T12">place-EP-ILL</ta>
            <ta e="T14" id="Seg_618" s="T13">wild.animal-CAP-INF</ta>
            <ta e="T15" id="Seg_619" s="T14">squirrel-CAP-INF</ta>
            <ta e="T16" id="Seg_620" s="T15">there-ADV.LOC</ta>
            <ta e="T17" id="Seg_621" s="T16">grandfather-1SG-ADES</ta>
            <ta e="T18" id="Seg_622" s="T17">be-PST-3SG.S</ta>
            <ta e="T19" id="Seg_623" s="T18">made.of.boards</ta>
            <ta e="T20" id="Seg_624" s="T19">tent-3SG</ta>
            <ta e="T21" id="Seg_625" s="T20">we.NOM</ta>
            <ta e="T22" id="Seg_626" s="T21">this</ta>
            <ta e="T23" id="Seg_627" s="T22">house-LOC</ta>
            <ta e="T24" id="Seg_628" s="T23">live-HAB-PST-1PL</ta>
            <ta e="T25" id="Seg_629" s="T24">hunt-DUR-HAB-PST-1PL</ta>
            <ta e="T26" id="Seg_630" s="T25">squirrel-ADJZ-HAB-PST-1PL</ta>
            <ta e="T27" id="Seg_631" s="T26">evening-PROL</ta>
            <ta e="T28" id="Seg_632" s="T27">come-HAB-PST-1PL</ta>
            <ta e="T29" id="Seg_633" s="T28">made.of.boards</ta>
            <ta e="T30" id="Seg_634" s="T29">house-DIM-ILL</ta>
            <ta e="T31" id="Seg_635" s="T30">what.[NOM]-INDEF4</ta>
            <ta e="T32" id="Seg_636" s="T31">day.[NOM]</ta>
            <ta e="T33" id="Seg_637" s="T32">I.NOM</ta>
            <ta e="T34" id="Seg_638" s="T33">now</ta>
            <ta e="T35" id="Seg_639" s="T34">forgot-PST.NAR-1SG.O</ta>
            <ta e="T36" id="Seg_640" s="T35">tiredness-ACC</ta>
            <ta e="T37" id="Seg_641" s="T36">I.NOM</ta>
            <ta e="T38" id="Seg_642" s="T37">now</ta>
            <ta e="T39" id="Seg_643" s="T38">know-1SG.O</ta>
            <ta e="T40" id="Seg_644" s="T39">evening-something-LOC-EMPH</ta>
            <ta e="T41" id="Seg_645" s="T40">I.NOM</ta>
            <ta e="T42" id="Seg_646" s="T41">be.sweet-CVB</ta>
            <ta e="T43" id="Seg_647" s="T42">sleep-PST.NAR-1SG.S</ta>
            <ta e="T44" id="Seg_648" s="T43">so</ta>
            <ta e="T45" id="Seg_649" s="T44">wake.up-CVB</ta>
            <ta e="T46" id="Seg_650" s="T45">begin-1SG.S</ta>
            <ta e="T47" id="Seg_651" s="T46">what.[NOM]-INDEF4</ta>
            <ta e="T48" id="Seg_652" s="T47">laugh-US-CO.[3SG.S]</ta>
            <ta e="T49" id="Seg_653" s="T48">child-like</ta>
            <ta e="T50" id="Seg_654" s="T49">cry-3SG.S</ta>
            <ta e="T51" id="Seg_655" s="T50">dog-like</ta>
            <ta e="T52" id="Seg_656" s="T51">bark-EP-3SG.S</ta>
            <ta e="T53" id="Seg_657" s="T52">bear-like</ta>
            <ta e="T54" id="Seg_658" s="T53">cry-CO.[3SG.S]</ta>
            <ta e="T55" id="Seg_659" s="T54">noise-TR-PST.NAR.[3SG.S]</ta>
            <ta e="T56" id="Seg_660" s="T55">so</ta>
            <ta e="T57" id="Seg_661" s="T56">I.NOM</ta>
            <ta e="T58" id="Seg_662" s="T57">wake.up-CO-1SG.S</ta>
            <ta e="T59" id="Seg_663" s="T58">then</ta>
            <ta e="T60" id="Seg_664" s="T59">again</ta>
            <ta e="T61" id="Seg_665" s="T60">sleep-PST.NAR-1SG.S</ta>
            <ta e="T62" id="Seg_666" s="T61">up.to</ta>
            <ta e="T63" id="Seg_667" s="T62">morning-ILL</ta>
            <ta e="T64" id="Seg_668" s="T63">morning-something-LOC</ta>
            <ta e="T65" id="Seg_669" s="T64">grandfather-1SG-%%</ta>
            <ta e="T66" id="Seg_670" s="T65">say-HAB-CVB</ta>
            <ta e="T67" id="Seg_671" s="T66">begin-1SG.O</ta>
            <ta e="T68" id="Seg_672" s="T67">grandfather.[NOM]-1SG</ta>
            <ta e="T69" id="Seg_673" s="T68">I.ALL</ta>
            <ta e="T70" id="Seg_674" s="T69">say-3SG.S</ta>
            <ta e="T71" id="Seg_675" s="T70">this</ta>
            <ta e="T72" id="Seg_676" s="T71">in.fact</ta>
            <ta e="T73" id="Seg_677" s="T72">child.[NOM]</ta>
            <ta e="T74" id="Seg_678" s="T73">owl.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_679" s="T1">филин.[NOM]</ta>
            <ta e="T3" id="Seg_680" s="T2">мы.PL.GEN</ta>
            <ta e="T4" id="Seg_681" s="T3">тайга.[NOM]</ta>
            <ta e="T5" id="Seg_682" s="T4">местность-EP-PL-ILL.3SG</ta>
            <ta e="T6" id="Seg_683" s="T5">быть-HAB-3PL</ta>
            <ta e="T7" id="Seg_684" s="T6">филин-PL</ta>
            <ta e="T8" id="Seg_685" s="T7">один-EP-ADVZ</ta>
            <ta e="T9" id="Seg_686" s="T8">мы.DU.NOM</ta>
            <ta e="T10" id="Seg_687" s="T9">дедушка-1SG-COM</ta>
            <ta e="T11" id="Seg_688" s="T10">уйти-PST-1PL</ta>
            <ta e="T12" id="Seg_689" s="T11">весна-ADJZ</ta>
            <ta e="T13" id="Seg_690" s="T12">местность-EP-ILL</ta>
            <ta e="T14" id="Seg_691" s="T13">зверь-CAP-INF</ta>
            <ta e="T15" id="Seg_692" s="T14">белка-CAP-INF</ta>
            <ta e="T16" id="Seg_693" s="T15">туда-ADV.LOC</ta>
            <ta e="T17" id="Seg_694" s="T16">дедушка-1SG-ADES</ta>
            <ta e="T18" id="Seg_695" s="T17">быть-PST-3SG.S</ta>
            <ta e="T19" id="Seg_696" s="T18">дощатый</ta>
            <ta e="T20" id="Seg_697" s="T19">чум-3SG</ta>
            <ta e="T21" id="Seg_698" s="T20">мы.NOM</ta>
            <ta e="T22" id="Seg_699" s="T21">этот</ta>
            <ta e="T23" id="Seg_700" s="T22">дом-LOC</ta>
            <ta e="T24" id="Seg_701" s="T23">жить-HAB-PST-1PL</ta>
            <ta e="T25" id="Seg_702" s="T24">охотиться-DUR-HAB-PST-1PL</ta>
            <ta e="T26" id="Seg_703" s="T25">белка-ADJZ-HAB-PST-1PL</ta>
            <ta e="T27" id="Seg_704" s="T26">вечер-PROL</ta>
            <ta e="T28" id="Seg_705" s="T27">прийти-HAB-PST-1PL</ta>
            <ta e="T29" id="Seg_706" s="T28">дощатый</ta>
            <ta e="T30" id="Seg_707" s="T29">дом-DIM-ILL</ta>
            <ta e="T31" id="Seg_708" s="T30">что.[NOM]-INDEF4</ta>
            <ta e="T32" id="Seg_709" s="T31">день.[NOM]</ta>
            <ta e="T33" id="Seg_710" s="T32">я.NOM</ta>
            <ta e="T34" id="Seg_711" s="T33">сейчас</ta>
            <ta e="T35" id="Seg_712" s="T34">забыть-PST.NAR-1SG.O</ta>
            <ta e="T36" id="Seg_713" s="T35">усталость-ACC</ta>
            <ta e="T37" id="Seg_714" s="T36">я.NOM</ta>
            <ta e="T38" id="Seg_715" s="T37">сейчас</ta>
            <ta e="T39" id="Seg_716" s="T38">знать-1SG.O</ta>
            <ta e="T40" id="Seg_717" s="T39">вечер-нечто-LOC-EMPH</ta>
            <ta e="T41" id="Seg_718" s="T40">я.NOM</ta>
            <ta e="T42" id="Seg_719" s="T41">быть.сладким-CVB</ta>
            <ta e="T43" id="Seg_720" s="T42">спать-PST.NAR-1SG.S</ta>
            <ta e="T44" id="Seg_721" s="T43">так</ta>
            <ta e="T45" id="Seg_722" s="T44">проснуться-CVB</ta>
            <ta e="T46" id="Seg_723" s="T45">начать-1SG.S</ta>
            <ta e="T47" id="Seg_724" s="T46">что.[NOM]-INDEF4</ta>
            <ta e="T48" id="Seg_725" s="T47">смеяться-US-CO.[3SG.S]</ta>
            <ta e="T49" id="Seg_726" s="T48">ребёнок-как</ta>
            <ta e="T50" id="Seg_727" s="T49">плакать-3SG.S</ta>
            <ta e="T51" id="Seg_728" s="T50">собака-как</ta>
            <ta e="T52" id="Seg_729" s="T51">лаять-EP-3SG.S</ta>
            <ta e="T53" id="Seg_730" s="T52">медведь-как</ta>
            <ta e="T54" id="Seg_731" s="T53">кричать-CO.[3SG.S]</ta>
            <ta e="T55" id="Seg_732" s="T54">шум-TR-PST.NAR.[3SG.S]</ta>
            <ta e="T56" id="Seg_733" s="T55">так</ta>
            <ta e="T57" id="Seg_734" s="T56">я.NOM</ta>
            <ta e="T58" id="Seg_735" s="T57">проснуться-CO-1SG.S</ta>
            <ta e="T59" id="Seg_736" s="T58">потом</ta>
            <ta e="T60" id="Seg_737" s="T59">опять</ta>
            <ta e="T61" id="Seg_738" s="T60">спать-PST.NAR-1SG.S</ta>
            <ta e="T62" id="Seg_739" s="T61">до</ta>
            <ta e="T63" id="Seg_740" s="T62">утро-ILL</ta>
            <ta e="T64" id="Seg_741" s="T63">утро-нечто-LOC</ta>
            <ta e="T65" id="Seg_742" s="T64">дедушка-1SG-%%</ta>
            <ta e="T66" id="Seg_743" s="T65">сказать-HAB-CVB</ta>
            <ta e="T67" id="Seg_744" s="T66">начать-1SG.O</ta>
            <ta e="T68" id="Seg_745" s="T67">дедушка.[NOM]-1SG</ta>
            <ta e="T69" id="Seg_746" s="T68">я.ALL</ta>
            <ta e="T70" id="Seg_747" s="T69">сказать-3SG.S</ta>
            <ta e="T71" id="Seg_748" s="T70">этот</ta>
            <ta e="T72" id="Seg_749" s="T71">ведь</ta>
            <ta e="T73" id="Seg_750" s="T72">ребёнок.[NOM]</ta>
            <ta e="T74" id="Seg_751" s="T73">филин.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_752" s="T1">n.[n:case]</ta>
            <ta e="T3" id="Seg_753" s="T2">pers</ta>
            <ta e="T4" id="Seg_754" s="T3">n.[n:case]</ta>
            <ta e="T5" id="Seg_755" s="T4">n-n:ins-n:num-n:case.poss</ta>
            <ta e="T6" id="Seg_756" s="T5">v-v&gt;v-v:pn</ta>
            <ta e="T7" id="Seg_757" s="T6">n-n:num</ta>
            <ta e="T8" id="Seg_758" s="T7">num-n:ins-adj&gt;adv</ta>
            <ta e="T9" id="Seg_759" s="T8">pers</ta>
            <ta e="T10" id="Seg_760" s="T9">n-n:poss-n:case</ta>
            <ta e="T11" id="Seg_761" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_762" s="T11">n-n&gt;adj</ta>
            <ta e="T13" id="Seg_763" s="T12">n-n:ins-n:case</ta>
            <ta e="T14" id="Seg_764" s="T13">n-n&gt;v-v:inf</ta>
            <ta e="T15" id="Seg_765" s="T14">n-n&gt;v-v:inf</ta>
            <ta e="T16" id="Seg_766" s="T15">adv-adv:case</ta>
            <ta e="T17" id="Seg_767" s="T16">n-n:poss-n:case</ta>
            <ta e="T18" id="Seg_768" s="T17">v-v:tense.[v:pn]</ta>
            <ta e="T19" id="Seg_769" s="T18">adj</ta>
            <ta e="T20" id="Seg_770" s="T19">n-n:poss</ta>
            <ta e="T21" id="Seg_771" s="T20">pers</ta>
            <ta e="T22" id="Seg_772" s="T21">dem</ta>
            <ta e="T23" id="Seg_773" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_774" s="T23">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_775" s="T24">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_776" s="T25">n-n&gt;adj-v&gt;v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_777" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_778" s="T27">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_779" s="T28">adj</ta>
            <ta e="T30" id="Seg_780" s="T29">n-n&gt;n-n:case</ta>
            <ta e="T31" id="Seg_781" s="T30">interrog.[n:case]-clit</ta>
            <ta e="T32" id="Seg_782" s="T31">n.[n:case]</ta>
            <ta e="T33" id="Seg_783" s="T32">pers</ta>
            <ta e="T34" id="Seg_784" s="T33">adv</ta>
            <ta e="T35" id="Seg_785" s="T34">v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_786" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_787" s="T36">pers</ta>
            <ta e="T38" id="Seg_788" s="T37">adv</ta>
            <ta e="T39" id="Seg_789" s="T38">v-v:pn</ta>
            <ta e="T40" id="Seg_790" s="T39">n-n-n:case-clit</ta>
            <ta e="T41" id="Seg_791" s="T40">pers</ta>
            <ta e="T42" id="Seg_792" s="T41">v-v&gt;adv</ta>
            <ta e="T43" id="Seg_793" s="T42">v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_794" s="T43">adv</ta>
            <ta e="T45" id="Seg_795" s="T44">v-v&gt;adv</ta>
            <ta e="T46" id="Seg_796" s="T45">v-v:pn</ta>
            <ta e="T47" id="Seg_797" s="T46">interrog.[n:case]-clit</ta>
            <ta e="T48" id="Seg_798" s="T47">v-v&gt;v-v:ins.[v:pn]</ta>
            <ta e="T49" id="Seg_799" s="T48">n-pp</ta>
            <ta e="T50" id="Seg_800" s="T49">v-v:pn</ta>
            <ta e="T51" id="Seg_801" s="T50">n-pp</ta>
            <ta e="T52" id="Seg_802" s="T51">v-v:ins-v:pn</ta>
            <ta e="T53" id="Seg_803" s="T52">n-pp</ta>
            <ta e="T54" id="Seg_804" s="T53">v-v:ins.[v:pn]</ta>
            <ta e="T55" id="Seg_805" s="T54">n-n&gt;v-v:tense.[v:pn]</ta>
            <ta e="T56" id="Seg_806" s="T55">adv</ta>
            <ta e="T57" id="Seg_807" s="T56">pers</ta>
            <ta e="T58" id="Seg_808" s="T57">v-v:ins-v:pn</ta>
            <ta e="T59" id="Seg_809" s="T58">adv</ta>
            <ta e="T60" id="Seg_810" s="T59">adv</ta>
            <ta e="T61" id="Seg_811" s="T60">v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_812" s="T61">prep</ta>
            <ta e="T63" id="Seg_813" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_814" s="T63">n-n-n:case</ta>
            <ta e="T65" id="Seg_815" s="T64">n-n:poss-n:case</ta>
            <ta e="T66" id="Seg_816" s="T65">v-v&gt;v-v&gt;adv</ta>
            <ta e="T67" id="Seg_817" s="T66">v-v:pn</ta>
            <ta e="T68" id="Seg_818" s="T67">n.[n:case]-n:poss</ta>
            <ta e="T69" id="Seg_819" s="T68">pers</ta>
            <ta e="T70" id="Seg_820" s="T69">v-v:pn</ta>
            <ta e="T71" id="Seg_821" s="T70">dem</ta>
            <ta e="T72" id="Seg_822" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_823" s="T72">n.[n:case]</ta>
            <ta e="T74" id="Seg_824" s="T73">n.[n:case]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_825" s="T1">n</ta>
            <ta e="T3" id="Seg_826" s="T2">pers</ta>
            <ta e="T4" id="Seg_827" s="T3">n</ta>
            <ta e="T5" id="Seg_828" s="T4">n</ta>
            <ta e="T6" id="Seg_829" s="T5">v</ta>
            <ta e="T7" id="Seg_830" s="T6">n</ta>
            <ta e="T8" id="Seg_831" s="T7">adv</ta>
            <ta e="T9" id="Seg_832" s="T8">pers</ta>
            <ta e="T10" id="Seg_833" s="T9">n</ta>
            <ta e="T11" id="Seg_834" s="T10">v</ta>
            <ta e="T12" id="Seg_835" s="T11">adj</ta>
            <ta e="T13" id="Seg_836" s="T12">n</ta>
            <ta e="T14" id="Seg_837" s="T13">v</ta>
            <ta e="T15" id="Seg_838" s="T14">v</ta>
            <ta e="T16" id="Seg_839" s="T15">adv</ta>
            <ta e="T17" id="Seg_840" s="T16">n</ta>
            <ta e="T18" id="Seg_841" s="T17">v</ta>
            <ta e="T19" id="Seg_842" s="T18">adj</ta>
            <ta e="T20" id="Seg_843" s="T19">n</ta>
            <ta e="T21" id="Seg_844" s="T20">pers</ta>
            <ta e="T22" id="Seg_845" s="T21">dem</ta>
            <ta e="T23" id="Seg_846" s="T22">n</ta>
            <ta e="T24" id="Seg_847" s="T23">v</ta>
            <ta e="T25" id="Seg_848" s="T24">v</ta>
            <ta e="T26" id="Seg_849" s="T25">adj</ta>
            <ta e="T27" id="Seg_850" s="T26">n</ta>
            <ta e="T28" id="Seg_851" s="T27">v</ta>
            <ta e="T29" id="Seg_852" s="T28">adj</ta>
            <ta e="T30" id="Seg_853" s="T29">n</ta>
            <ta e="T31" id="Seg_854" s="T30">interrog</ta>
            <ta e="T32" id="Seg_855" s="T31">n</ta>
            <ta e="T33" id="Seg_856" s="T32">pers</ta>
            <ta e="T34" id="Seg_857" s="T33">adv</ta>
            <ta e="T35" id="Seg_858" s="T34">v</ta>
            <ta e="T36" id="Seg_859" s="T35">n</ta>
            <ta e="T37" id="Seg_860" s="T36">pers</ta>
            <ta e="T38" id="Seg_861" s="T37">adv</ta>
            <ta e="T39" id="Seg_862" s="T38">v</ta>
            <ta e="T40" id="Seg_863" s="T39">n</ta>
            <ta e="T41" id="Seg_864" s="T40">pers</ta>
            <ta e="T42" id="Seg_865" s="T41">adv</ta>
            <ta e="T43" id="Seg_866" s="T42">v</ta>
            <ta e="T44" id="Seg_867" s="T43">v</ta>
            <ta e="T45" id="Seg_868" s="T44">adv</ta>
            <ta e="T46" id="Seg_869" s="T45">v</ta>
            <ta e="T47" id="Seg_870" s="T46">interrog</ta>
            <ta e="T48" id="Seg_871" s="T47">v</ta>
            <ta e="T49" id="Seg_872" s="T48">n</ta>
            <ta e="T50" id="Seg_873" s="T49">v</ta>
            <ta e="T51" id="Seg_874" s="T50">n</ta>
            <ta e="T52" id="Seg_875" s="T51">v</ta>
            <ta e="T53" id="Seg_876" s="T52">n</ta>
            <ta e="T54" id="Seg_877" s="T53">v</ta>
            <ta e="T55" id="Seg_878" s="T54">n</ta>
            <ta e="T56" id="Seg_879" s="T55">v</ta>
            <ta e="T57" id="Seg_880" s="T56">pers</ta>
            <ta e="T58" id="Seg_881" s="T57">v</ta>
            <ta e="T59" id="Seg_882" s="T58">adv</ta>
            <ta e="T60" id="Seg_883" s="T59">adv</ta>
            <ta e="T61" id="Seg_884" s="T60">v</ta>
            <ta e="T62" id="Seg_885" s="T61">prep</ta>
            <ta e="T63" id="Seg_886" s="T62">n</ta>
            <ta e="T64" id="Seg_887" s="T63">n</ta>
            <ta e="T65" id="Seg_888" s="T64">n</ta>
            <ta e="T66" id="Seg_889" s="T65">adv</ta>
            <ta e="T67" id="Seg_890" s="T66">v</ta>
            <ta e="T68" id="Seg_891" s="T67">n</ta>
            <ta e="T69" id="Seg_892" s="T68">pers</ta>
            <ta e="T70" id="Seg_893" s="T69">v</ta>
            <ta e="T71" id="Seg_894" s="T70">dem</ta>
            <ta e="T72" id="Seg_895" s="T71">pers</ta>
            <ta e="T73" id="Seg_896" s="T72">n</ta>
            <ta e="T74" id="Seg_897" s="T73">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_898" s="T2">pro.h:Poss</ta>
            <ta e="T5" id="Seg_899" s="T4">np:L</ta>
            <ta e="T7" id="Seg_900" s="T6">np:Th</ta>
            <ta e="T8" id="Seg_901" s="T7">adv:Time</ta>
            <ta e="T9" id="Seg_902" s="T8">pro.h:A</ta>
            <ta e="T10" id="Seg_903" s="T9">np:Com 0.1.h:Poss</ta>
            <ta e="T13" id="Seg_904" s="T12">np:G</ta>
            <ta e="T14" id="Seg_905" s="T13">0.1.h:A</ta>
            <ta e="T15" id="Seg_906" s="T14">0.1.h:A</ta>
            <ta e="T16" id="Seg_907" s="T15">adv:L</ta>
            <ta e="T17" id="Seg_908" s="T16">np.h:Poss 0.1.h:Poss</ta>
            <ta e="T20" id="Seg_909" s="T19">np:Th </ta>
            <ta e="T21" id="Seg_910" s="T20">pro.h:Th</ta>
            <ta e="T23" id="Seg_911" s="T22">np:L</ta>
            <ta e="T25" id="Seg_912" s="T24">0.1.h:A</ta>
            <ta e="T26" id="Seg_913" s="T25">0.1.h:A</ta>
            <ta e="T27" id="Seg_914" s="T26">np:Time</ta>
            <ta e="T28" id="Seg_915" s="T27">0.1.h:A</ta>
            <ta e="T30" id="Seg_916" s="T29">np:G</ta>
            <ta e="T32" id="Seg_917" s="T31">np:Time</ta>
            <ta e="T33" id="Seg_918" s="T32">pro.h:E</ta>
            <ta e="T34" id="Seg_919" s="T33">adv:Time</ta>
            <ta e="T37" id="Seg_920" s="T36">pro.h:E</ta>
            <ta e="T38" id="Seg_921" s="T37">adv:Time</ta>
            <ta e="T40" id="Seg_922" s="T39">np:Time</ta>
            <ta e="T41" id="Seg_923" s="T40">pro.h:Th</ta>
            <ta e="T46" id="Seg_924" s="T45">0.1.h:P</ta>
            <ta e="T47" id="Seg_925" s="T46">pro.h:A</ta>
            <ta e="T50" id="Seg_926" s="T49">0.3.h:A</ta>
            <ta e="T52" id="Seg_927" s="T51">0.3.h:A</ta>
            <ta e="T54" id="Seg_928" s="T53">0.3.h:A</ta>
            <ta e="T55" id="Seg_929" s="T54">0.3.h:A</ta>
            <ta e="T57" id="Seg_930" s="T56">pro.h:P</ta>
            <ta e="T59" id="Seg_931" s="T58">adv:Time</ta>
            <ta e="T61" id="Seg_932" s="T60">0.1.h:Th</ta>
            <ta e="T63" id="Seg_933" s="T62">pp:Time</ta>
            <ta e="T64" id="Seg_934" s="T63">np:Time</ta>
            <ta e="T65" id="Seg_935" s="T64">np.h:R 0.1.h:Poss</ta>
            <ta e="T66" id="Seg_936" s="T65">0.1.h:A</ta>
            <ta e="T67" id="Seg_937" s="T66">0.1.h:A</ta>
            <ta e="T68" id="Seg_938" s="T67">np.h:A</ta>
            <ta e="T69" id="Seg_939" s="T68">pro.h:R</ta>
            <ta e="T71" id="Seg_940" s="T70">pro:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T6" id="Seg_941" s="T5">v:pred</ta>
            <ta e="T7" id="Seg_942" s="T6">np:S</ta>
            <ta e="T9" id="Seg_943" s="T8">pro.h:S</ta>
            <ta e="T11" id="Seg_944" s="T10">v:pred</ta>
            <ta e="T14" id="Seg_945" s="T13">s:purp</ta>
            <ta e="T15" id="Seg_946" s="T14">s:purp</ta>
            <ta e="T18" id="Seg_947" s="T17">v:pred</ta>
            <ta e="T20" id="Seg_948" s="T19">np:S</ta>
            <ta e="T21" id="Seg_949" s="T20">pro.h:S</ta>
            <ta e="T24" id="Seg_950" s="T23">v:pred</ta>
            <ta e="T25" id="Seg_951" s="T24">0.1.h:S v:pred</ta>
            <ta e="T26" id="Seg_952" s="T25">0.1.h:S v:pred</ta>
            <ta e="T28" id="Seg_953" s="T27">0.1.h:S v:pred</ta>
            <ta e="T33" id="Seg_954" s="T32">pro.h:S</ta>
            <ta e="T35" id="Seg_955" s="T34">v:pred</ta>
            <ta e="T37" id="Seg_956" s="T36">pro.h:S</ta>
            <ta e="T39" id="Seg_957" s="T38">v:pred</ta>
            <ta e="T41" id="Seg_958" s="T40">pro.h:S</ta>
            <ta e="T42" id="Seg_959" s="T41">s:adv</ta>
            <ta e="T43" id="Seg_960" s="T42">v:pred</ta>
            <ta e="T45" id="Seg_961" s="T44">s:adv</ta>
            <ta e="T46" id="Seg_962" s="T45">0.1.h:S v:pred</ta>
            <ta e="T47" id="Seg_963" s="T46">pro.h:S</ta>
            <ta e="T48" id="Seg_964" s="T47">v:pred</ta>
            <ta e="T50" id="Seg_965" s="T49">0.3.h:S v:pred</ta>
            <ta e="T52" id="Seg_966" s="T51">0.3.h:S v:pred</ta>
            <ta e="T54" id="Seg_967" s="T53">0.3.h:S v:pred</ta>
            <ta e="T55" id="Seg_968" s="T54">0.3.h:S v:pred</ta>
            <ta e="T57" id="Seg_969" s="T56">pro.h:S</ta>
            <ta e="T58" id="Seg_970" s="T57">v:pred</ta>
            <ta e="T61" id="Seg_971" s="T60">0.1.h:S v:pred</ta>
            <ta e="T66" id="Seg_972" s="T64">s:adv</ta>
            <ta e="T67" id="Seg_973" s="T66">0.1.h:S v:pred</ta>
            <ta e="T68" id="Seg_974" s="T67">np.h:S</ta>
            <ta e="T70" id="Seg_975" s="T69">v:pred</ta>
            <ta e="T71" id="Seg_976" s="T70">pro:S</ta>
            <ta e="T74" id="Seg_977" s="T73">n:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T31" id="Seg_978" s="T30">-</ta>
            <ta e="T40" id="Seg_979" s="T39">-</ta>
            <ta e="T47" id="Seg_980" s="T46">-</ta>
            <ta e="T55" id="Seg_981" s="T54">RUS:core</ta>
            <ta e="T62" id="Seg_982" s="T61">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T2" id="Seg_983" s="T1">Owl.</ta>
            <ta e="T7" id="Seg_984" s="T2">In our woods there are owls.</ta>
            <ta e="T15" id="Seg_985" s="T7">Once I went with my grandfather in spring to hunt, to hunt squirrels.</ta>
            <ta e="T20" id="Seg_986" s="T15">My grandfather had a lodge there.</ta>
            <ta e="T26" id="Seg_987" s="T20">We lived in this lodge, we hunted, we hunted squirrels.</ta>
            <ta e="T30" id="Seg_988" s="T26">In the evening we came to the little lodge.</ta>
            <ta e="T39" id="Seg_989" s="T30">One day, I forgot it now, tiredness, I'll remember now.</ta>
            <ta e="T43" id="Seg_990" s="T39">I slept softly in the evening.</ta>
            <ta e="T46" id="Seg_991" s="T43">Then I woke up.</ta>
            <ta e="T55" id="Seg_992" s="T46">Someone laughs, someone cries like a child, barks like a dog, roars like a bear, wheezes.</ta>
            <ta e="T63" id="Seg_993" s="T55">So I woke up, then I slept till the morning.</ta>
            <ta e="T67" id="Seg_994" s="T63">In the morning I told it to my grandfather.</ta>
            <ta e="T74" id="Seg_995" s="T67">My grandfather told me: This, my son, is an owl.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2" id="Seg_996" s="T1">Eule.</ta>
            <ta e="T7" id="Seg_997" s="T2">An unseren Waldplätzen gibt es Eulen.</ta>
            <ta e="T15" id="Seg_998" s="T7">Einmal ging ich mit Großvater am Frühlingsplatz jagen, Eichhörnchen jagen.</ta>
            <ta e="T20" id="Seg_999" s="T15">Dort hatte Großvater eine Bretterhütte.</ta>
            <ta e="T26" id="Seg_1000" s="T20">Wir lebten in dieser Hütte, jagten, jagten Eichhörnchen.</ta>
            <ta e="T30" id="Seg_1001" s="T26">Abends kamen wir zu der kleinen Hütte.</ta>
            <ta e="T39" id="Seg_1002" s="T30">Eines Tages, ich habe es jetzt vergessen, Müdigkeit, ich erinnere mich jetzt.</ta>
            <ta e="T43" id="Seg_1003" s="T39">An einem dieser Tage schlief ich süß am Abend.</ta>
            <ta e="T46" id="Seg_1004" s="T43">So begann ich aufzuwachen.</ta>
            <ta e="T55" id="Seg_1005" s="T46">Jemand lachte, weinte wie ein Kind, bellte wie ein Hund, brüllte wie ein Bär, pfeift.</ta>
            <ta e="T63" id="Seg_1006" s="T55">So wachte ich auf, dann schlief ich wieder bis zum Morgen.</ta>
            <ta e="T67" id="Seg_1007" s="T63">Am Morgen fing ich an dem Großvater zu erzählen.</ta>
            <ta e="T74" id="Seg_1008" s="T67">Großvater sagte mir: Dieses, mein Sohn (ist) eine Eule.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T2" id="Seg_1009" s="T1">Филин.</ta>
            <ta e="T7" id="Seg_1010" s="T2">В нашей таёжной местности водятся филины.</ta>
            <ta e="T15" id="Seg_1011" s="T7">Однажды мы с дедушкой шли по весенней местности охотиться, белковать там.</ta>
            <ta e="T20" id="Seg_1012" s="T15">Там у дедушки была дощатая избушка. </ta>
            <ta e="T26" id="Seg_1013" s="T20">Мы в этой избушке жили, охотились, белковали.</ta>
            <ta e="T30" id="Seg_1014" s="T26">Вечером приходили в дощатую избушку. </ta>
            <ta e="T39" id="Seg_1015" s="T30">В какой-то из день, я сейчас позабыл, усталость, и я сейчас вспомню. </ta>
            <ta e="T43" id="Seg_1016" s="T39">С вечера я сладко уснул.</ta>
            <ta e="T46" id="Seg_1017" s="T43">Тут просыпаться стал.</ta>
            <ta e="T55" id="Seg_1018" s="T46">Кто-то смеётся, по-ребячьи плачет, по-собачьи лает, по-медвежьи ревёт, свистит.</ta>
            <ta e="T63" id="Seg_1019" s="T55">Tут я проснулся, потом опять заснул до утра. </ta>
            <ta e="T67" id="Seg_1020" s="T63">Утром дедушке рассказывать стал.</ta>
            <ta e="T74" id="Seg_1021" s="T67">Дедушка мне сказал: Это ведь, детка, филин.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T2" id="Seg_1022" s="T1">Филин</ta>
            <ta e="T7" id="Seg_1023" s="T2">в нашей таёжной местности бывают филины.</ta>
            <ta e="T15" id="Seg_1024" s="T7">однажды мы с дедушкой ушли весенней местностью охотится белковать.</ta>
            <ta e="T20" id="Seg_1025" s="T15">У дедушки была досченная (из досок) избушка.</ta>
            <ta e="T26" id="Seg_1026" s="T20">мы б этой избушке жили охотились, белковали.</ta>
            <ta e="T30" id="Seg_1027" s="T26">Вечером приходили в досчаную избушку.</ta>
            <ta e="T39" id="Seg_1028" s="T30">В какой-то день я сейчас позабыл, усталость, и я сейчас знаю (вспоминаю)</ta>
            <ta e="T43" id="Seg_1029" s="T39">с вечера я сладко уснул</ta>
            <ta e="T46" id="Seg_1030" s="T43">тут просыпаться стал</ta>
            <ta e="T55" id="Seg_1031" s="T46">кто-то смеётся по ребячьи плачет. но собачьи лает по медвежьи ревет свистит</ta>
            <ta e="T63" id="Seg_1032" s="T55">на это я проснулся потом опять заснул до утра</ta>
            <ta e="T67" id="Seg_1033" s="T63">утром дедушке рассказывать стал.</ta>
            <ta e="T74" id="Seg_1034" s="T67">дедушка мне сказал это ведь дедка филин</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
