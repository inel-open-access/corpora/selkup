<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KMS_1967_Elk_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KMS_1963_Elk_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">73</ud-information>
            <ud-information attribute-name="# HIAT:w">57</ud-information>
            <ud-information attribute-name="# e">57</ud-information>
            <ud-information attribute-name="# HIAT:u">11</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="4ddcbef0-6aba-4bb4-9640-253639128a28">
            <abbreviation>KMS</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="4ddcbef0-6aba-4bb4-9640-253639128a28"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T58" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Peŋa</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">warɣə</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">suːrɨm</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Ondə</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">täp</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">pirgeŋ</ts>
                  <nts id="Seg_23" n="HIAT:ip">,</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">aːmdɨlat</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">tʼümbə</ts>
                  <nts id="Seg_30" n="HIAT:ip">,</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_33" n="HIAT:w" s="T9">warɣä</ts>
                  <nts id="Seg_34" n="HIAT:ip">,</nts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">toppɨlat</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">tʼümbə</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_44" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">Ondə</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">täp</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">assä</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">tʼüumbədi</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_59" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">Qaːmban</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">täp</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">tʼüːumbɨkuŋ</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_71" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">Okkɨrɨŋ</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">peŋə</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">uːlnə</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">ton</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">taj</ts>
                  <nts id="Seg_86" n="HIAT:ip">.</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_89" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">A</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">toɣɨn</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">qwälɨlʼdi</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_100" n="HIAT:w" s="T27">qula</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_103" n="HIAT:w" s="T28">potpɨzattə</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_106" n="HIAT:w" s="T29">poŋgɨlam</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_109" n="HIAT:w" s="T30">qwälanni</ts>
                  <nts id="Seg_110" n="HIAT:ip">.</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_113" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_115" n="HIAT:w" s="T31">Uːldäɣandə</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_118" n="HIAT:w" s="T32">peŋa</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_121" n="HIAT:w" s="T33">qwändɨt</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_124" n="HIAT:w" s="T34">ondɨndɨzä</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_127" n="HIAT:w" s="T35">poŋgɨm</ts>
                  <nts id="Seg_128" n="HIAT:ip">.</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_131" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_133" n="HIAT:w" s="T36">Potpädi</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_136" n="HIAT:w" s="T37">poŋgɨm</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_139" n="HIAT:w" s="T38">nɨškəlkɨlbat</ts>
                  <nts id="Seg_140" n="HIAT:ip">.</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_143" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_145" n="HIAT:w" s="T39">Qwälɨlʼdi</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_148" n="HIAT:w" s="T40">qum</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_151" n="HIAT:w" s="T41">tüːan</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_154" n="HIAT:w" s="T42">tondə</ts>
                  <nts id="Seg_155" n="HIAT:ip">.</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_158" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_160" n="HIAT:w" s="T43">Poŋgɨmdä</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_163" n="HIAT:w" s="T44">potpädimɨɣɨn</ts>
                  <nts id="Seg_164" n="HIAT:ip">,</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_167" n="HIAT:w" s="T45">qombat</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_170" n="HIAT:w" s="T46">pilʼekamdə</ts>
                  <nts id="Seg_171" n="HIAT:ip">,</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_174" n="HIAT:w" s="T47">a</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_177" n="HIAT:w" s="T48">pilʼekat</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_180" n="HIAT:w" s="T49">nɨškɨlbädi</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_183" n="HIAT:w" s="T50">qombat</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_186" n="HIAT:w" s="T51">konnän</ts>
                  <nts id="Seg_187" n="HIAT:ip">.</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_190" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_192" n="HIAT:w" s="T52">Qwälɨlʼdʼi</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_195" n="HIAT:w" s="T53">qunni</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_198" n="HIAT:w" s="T54">nadomnɨŋ</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_201" n="HIAT:w" s="T55">ezuŋ</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_204" n="HIAT:w" s="T56">poŋgɨmdə</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_207" n="HIAT:w" s="T57">qaunǯegu</ts>
                  <nts id="Seg_208" n="HIAT:ip">.</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T58" id="Seg_210" n="sc" s="T1">
               <ts e="T2" id="Seg_212" n="e" s="T1">Peŋa </ts>
               <ts e="T3" id="Seg_214" n="e" s="T2">warɣə </ts>
               <ts e="T4" id="Seg_216" n="e" s="T3">suːrɨm. </ts>
               <ts e="T5" id="Seg_218" n="e" s="T4">Ondə </ts>
               <ts e="T6" id="Seg_220" n="e" s="T5">täp </ts>
               <ts e="T7" id="Seg_222" n="e" s="T6">pirgeŋ, </ts>
               <ts e="T8" id="Seg_224" n="e" s="T7">aːmdɨlat </ts>
               <ts e="T9" id="Seg_226" n="e" s="T8">tʼümbə, </ts>
               <ts e="T10" id="Seg_228" n="e" s="T9">warɣä, </ts>
               <ts e="T11" id="Seg_230" n="e" s="T10">toppɨlat </ts>
               <ts e="T12" id="Seg_232" n="e" s="T11">tʼümbə. </ts>
               <ts e="T13" id="Seg_234" n="e" s="T12">Ondə </ts>
               <ts e="T14" id="Seg_236" n="e" s="T13">täp </ts>
               <ts e="T15" id="Seg_238" n="e" s="T14">assä </ts>
               <ts e="T16" id="Seg_240" n="e" s="T15">tʼüumbədi. </ts>
               <ts e="T17" id="Seg_242" n="e" s="T16">Qaːmban </ts>
               <ts e="T18" id="Seg_244" n="e" s="T17">täp </ts>
               <ts e="T19" id="Seg_246" n="e" s="T18">tʼüːumbɨkuŋ. </ts>
               <ts e="T20" id="Seg_248" n="e" s="T19">Okkɨrɨŋ </ts>
               <ts e="T21" id="Seg_250" n="e" s="T20">peŋə </ts>
               <ts e="T22" id="Seg_252" n="e" s="T21">uːlnə </ts>
               <ts e="T23" id="Seg_254" n="e" s="T22">ton </ts>
               <ts e="T24" id="Seg_256" n="e" s="T23">taj. </ts>
               <ts e="T25" id="Seg_258" n="e" s="T24">A </ts>
               <ts e="T26" id="Seg_260" n="e" s="T25">toɣɨn </ts>
               <ts e="T27" id="Seg_262" n="e" s="T26">qwälɨlʼdi </ts>
               <ts e="T28" id="Seg_264" n="e" s="T27">qula </ts>
               <ts e="T29" id="Seg_266" n="e" s="T28">potpɨzattə </ts>
               <ts e="T30" id="Seg_268" n="e" s="T29">poŋgɨlam </ts>
               <ts e="T31" id="Seg_270" n="e" s="T30">qwälanni. </ts>
               <ts e="T32" id="Seg_272" n="e" s="T31">Uːldäɣandə </ts>
               <ts e="T33" id="Seg_274" n="e" s="T32">peŋa </ts>
               <ts e="T34" id="Seg_276" n="e" s="T33">qwändɨt </ts>
               <ts e="T35" id="Seg_278" n="e" s="T34">ondɨndɨzä </ts>
               <ts e="T36" id="Seg_280" n="e" s="T35">poŋgɨm. </ts>
               <ts e="T37" id="Seg_282" n="e" s="T36">Potpädi </ts>
               <ts e="T38" id="Seg_284" n="e" s="T37">poŋgɨm </ts>
               <ts e="T39" id="Seg_286" n="e" s="T38">nɨškəlkɨlbat. </ts>
               <ts e="T40" id="Seg_288" n="e" s="T39">Qwälɨlʼdi </ts>
               <ts e="T41" id="Seg_290" n="e" s="T40">qum </ts>
               <ts e="T42" id="Seg_292" n="e" s="T41">tüːan </ts>
               <ts e="T43" id="Seg_294" n="e" s="T42">tondə. </ts>
               <ts e="T44" id="Seg_296" n="e" s="T43">Poŋgɨmdä </ts>
               <ts e="T45" id="Seg_298" n="e" s="T44">potpädimɨɣɨn, </ts>
               <ts e="T46" id="Seg_300" n="e" s="T45">qombat </ts>
               <ts e="T47" id="Seg_302" n="e" s="T46">pilʼekamdə, </ts>
               <ts e="T48" id="Seg_304" n="e" s="T47">a </ts>
               <ts e="T49" id="Seg_306" n="e" s="T48">pilʼekat </ts>
               <ts e="T50" id="Seg_308" n="e" s="T49">nɨškɨlbädi </ts>
               <ts e="T51" id="Seg_310" n="e" s="T50">qombat </ts>
               <ts e="T52" id="Seg_312" n="e" s="T51">konnän. </ts>
               <ts e="T53" id="Seg_314" n="e" s="T52">Qwälɨlʼdʼi </ts>
               <ts e="T54" id="Seg_316" n="e" s="T53">qunni </ts>
               <ts e="T55" id="Seg_318" n="e" s="T54">nadomnɨŋ </ts>
               <ts e="T56" id="Seg_320" n="e" s="T55">ezuŋ </ts>
               <ts e="T57" id="Seg_322" n="e" s="T56">poŋgɨmdə </ts>
               <ts e="T58" id="Seg_324" n="e" s="T57">qaunǯegu. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_325" s="T1">KMS_1963_Elk_nar.001 (001.001)</ta>
            <ta e="T12" id="Seg_326" s="T4">KMS_1963_Elk_nar.002 (001.002)</ta>
            <ta e="T16" id="Seg_327" s="T12">KMS_1963_Elk_nar.003 (001.003)</ta>
            <ta e="T19" id="Seg_328" s="T16">KMS_1963_Elk_nar.004 (001.004)</ta>
            <ta e="T24" id="Seg_329" s="T19">KMS_1963_Elk_nar.005 (001.005)</ta>
            <ta e="T31" id="Seg_330" s="T24">KMS_1963_Elk_nar.006 (001.006)</ta>
            <ta e="T36" id="Seg_331" s="T31">KMS_1963_Elk_nar.007 (001.007)</ta>
            <ta e="T39" id="Seg_332" s="T36">KMS_1963_Elk_nar.008 (001.008)</ta>
            <ta e="T43" id="Seg_333" s="T39">KMS_1963_Elk_nar.009 (001.009)</ta>
            <ta e="T52" id="Seg_334" s="T43">KMS_1963_Elk_nar.010 (001.010)</ta>
            <ta e="T58" id="Seg_335" s="T52">KMS_1963_Elk_nar.011 (001.011)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="stl" tierref="stl" />
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_336" s="T1">Peŋa warɣə suːrɨm. </ta>
            <ta e="T12" id="Seg_337" s="T4">Ondə täp pirgeŋ, aːmdɨlat tʼümbə, warɣä, toppɨlat tʼümbə. </ta>
            <ta e="T16" id="Seg_338" s="T12">Ondə täp assä tʼüumbədi. </ta>
            <ta e="T19" id="Seg_339" s="T16">Qaːmban täp tʼüːumbɨkuŋ. </ta>
            <ta e="T24" id="Seg_340" s="T19">Okkɨrɨŋ peŋə uːlnə ton taj. </ta>
            <ta e="T31" id="Seg_341" s="T24">A toɣɨn qwälɨlʼdi qula potpɨzattə poŋgɨlam qwälanni. </ta>
            <ta e="T36" id="Seg_342" s="T31">Uːldäɣandə peŋa qwändɨt ondɨndɨzä poŋgɨm. </ta>
            <ta e="T39" id="Seg_343" s="T36">Potpädi poŋgɨm nɨškəlkɨlbat. </ta>
            <ta e="T43" id="Seg_344" s="T39">Qwälɨlʼdi qum tüːan tondə. </ta>
            <ta e="T52" id="Seg_345" s="T43">Poŋgɨmdä potpädimɨɣɨn, qombat pilʼekamdə, a pilʼekat nɨškɨlbädi qombat konnän. </ta>
            <ta e="T58" id="Seg_346" s="T52">Qwälɨlʼdʼi qunni nadomnɨŋ ezuŋ poŋgɨmdə qaunǯegu. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_347" s="T1">peŋa</ta>
            <ta e="T3" id="Seg_348" s="T2">warɣə</ta>
            <ta e="T4" id="Seg_349" s="T3">suːrɨm</ta>
            <ta e="T5" id="Seg_350" s="T4">ondə</ta>
            <ta e="T6" id="Seg_351" s="T5">täp</ta>
            <ta e="T7" id="Seg_352" s="T6">pirge-n</ta>
            <ta e="T8" id="Seg_353" s="T7">aːmdɨ-la-tɨ</ta>
            <ta e="T9" id="Seg_354" s="T8">tʼümbə</ta>
            <ta e="T10" id="Seg_355" s="T9">warɣä</ta>
            <ta e="T11" id="Seg_356" s="T10">toppɨ-la-tɨ</ta>
            <ta e="T12" id="Seg_357" s="T11">tʼümbə</ta>
            <ta e="T13" id="Seg_358" s="T12">ondə</ta>
            <ta e="T14" id="Seg_359" s="T13">täp</ta>
            <ta e="T15" id="Seg_360" s="T14">assä</ta>
            <ta e="T16" id="Seg_361" s="T15">tʼüu-mbədi</ta>
            <ta e="T17" id="Seg_362" s="T16">qaːmba-n</ta>
            <ta e="T18" id="Seg_363" s="T17">täp</ta>
            <ta e="T19" id="Seg_364" s="T18">tʼüːu-mbɨ-ku-n</ta>
            <ta e="T20" id="Seg_365" s="T19">okkɨr-ɨ-ŋ</ta>
            <ta e="T21" id="Seg_366" s="T20">peŋə</ta>
            <ta e="T22" id="Seg_367" s="T21">uː-le-nə</ta>
            <ta e="T23" id="Seg_368" s="T22">to-n</ta>
            <ta e="T24" id="Seg_369" s="T23">taj</ta>
            <ta e="T25" id="Seg_370" s="T24">a</ta>
            <ta e="T26" id="Seg_371" s="T25">to-ɣɨn</ta>
            <ta e="T27" id="Seg_372" s="T26">qwälɨ-lʼdi</ta>
            <ta e="T28" id="Seg_373" s="T27">qu-la</ta>
            <ta e="T29" id="Seg_374" s="T28">pot-pɨ-za-ttə</ta>
            <ta e="T30" id="Seg_375" s="T29">poŋgɨ-la-m</ta>
            <ta e="T31" id="Seg_376" s="T30">qwäla-n-ni</ta>
            <ta e="T32" id="Seg_377" s="T31">uː-le-dä-ɣan-də</ta>
            <ta e="T33" id="Seg_378" s="T32">peŋa</ta>
            <ta e="T34" id="Seg_379" s="T33">qwändɨ-t</ta>
            <ta e="T35" id="Seg_380" s="T34">ondɨ-ndɨ-zä</ta>
            <ta e="T36" id="Seg_381" s="T35">poŋgɨ-m</ta>
            <ta e="T37" id="Seg_382" s="T36">pot-pädi</ta>
            <ta e="T38" id="Seg_383" s="T37">poŋgɨ-m</ta>
            <ta e="T39" id="Seg_384" s="T38">nɨškə-le-kɨl-ba-t</ta>
            <ta e="T40" id="Seg_385" s="T39">qwälɨ-lʼdi</ta>
            <ta e="T41" id="Seg_386" s="T40">qum</ta>
            <ta e="T42" id="Seg_387" s="T41">tüː-a-ŋ</ta>
            <ta e="T43" id="Seg_388" s="T42">to-ndə</ta>
            <ta e="T44" id="Seg_389" s="T43">poŋgɨ-m-dä</ta>
            <ta e="T45" id="Seg_390" s="T44">pot-pädi-mɨ-ɣɨn</ta>
            <ta e="T46" id="Seg_391" s="T45">qo-mba-t</ta>
            <ta e="T47" id="Seg_392" s="T46">pilʼeka-m-də</ta>
            <ta e="T48" id="Seg_393" s="T47">a</ta>
            <ta e="T49" id="Seg_394" s="T48">pilʼeka-t</ta>
            <ta e="T50" id="Seg_395" s="T49">nɨškɨ-le-bädi</ta>
            <ta e="T51" id="Seg_396" s="T50">qo-mba-t</ta>
            <ta e="T52" id="Seg_397" s="T51">konnä-n</ta>
            <ta e="T53" id="Seg_398" s="T52">qwälɨ-lʼdʼi</ta>
            <ta e="T54" id="Seg_399" s="T53">qun-ni</ta>
            <ta e="T55" id="Seg_400" s="T54">nado-m-nɨ-ŋ</ta>
            <ta e="T56" id="Seg_401" s="T55">e-zu-n</ta>
            <ta e="T57" id="Seg_402" s="T56">poŋgɨ-m-də</ta>
            <ta e="T58" id="Seg_403" s="T57">qau-nǯe-gu</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_404" s="T1">päŋqa</ta>
            <ta e="T3" id="Seg_405" s="T2">wərkə</ta>
            <ta e="T4" id="Seg_406" s="T3">suːrǝm</ta>
            <ta e="T5" id="Seg_407" s="T4">ontɨ</ta>
            <ta e="T6" id="Seg_408" s="T5">tap</ta>
            <ta e="T7" id="Seg_409" s="T6">pirgə-n</ta>
            <ta e="T8" id="Seg_410" s="T7">aːmdɨ-la-tɨ</ta>
            <ta e="T9" id="Seg_411" s="T8">tʼumbɨ</ta>
            <ta e="T10" id="Seg_412" s="T9">wərkə</ta>
            <ta e="T11" id="Seg_413" s="T10">topǝ-la-tɨ</ta>
            <ta e="T12" id="Seg_414" s="T11">tʼumbɨ</ta>
            <ta e="T13" id="Seg_415" s="T12">ontɨ</ta>
            <ta e="T14" id="Seg_416" s="T13">tap</ta>
            <ta e="T15" id="Seg_417" s="T14">assɨ</ta>
            <ta e="T16" id="Seg_418" s="T15">töu-mbɨdi</ta>
            <ta e="T17" id="Seg_419" s="T16">qamba-n</ta>
            <ta e="T18" id="Seg_420" s="T17">tap</ta>
            <ta e="T19" id="Seg_421" s="T18">töu-mbɨ-ku-n</ta>
            <ta e="T20" id="Seg_422" s="T19">okkɨr-ɨ-k</ta>
            <ta e="T21" id="Seg_423" s="T20">päŋqa</ta>
            <ta e="T22" id="Seg_424" s="T21">uː-le-ŋɨ</ta>
            <ta e="T23" id="Seg_425" s="T22">toː-n</ta>
            <ta e="T24" id="Seg_426" s="T23">taːj</ta>
            <ta e="T25" id="Seg_427" s="T24">a</ta>
            <ta e="T26" id="Seg_428" s="T25">toː-qən</ta>
            <ta e="T27" id="Seg_429" s="T26">qwǝlɨ-lʼdi</ta>
            <ta e="T28" id="Seg_430" s="T27">qum-la</ta>
            <ta e="T29" id="Seg_431" s="T28">pot-mbɨ-sɨ-tɨt</ta>
            <ta e="T30" id="Seg_432" s="T29">poqqo-la-m</ta>
            <ta e="T31" id="Seg_433" s="T30">qwǝlɨ-n-nɨ</ta>
            <ta e="T32" id="Seg_434" s="T31">uː-le-ptä-qən-tɨ</ta>
            <ta e="T33" id="Seg_435" s="T32">päŋqa</ta>
            <ta e="T34" id="Seg_436" s="T33">qwandɛ-tɨ</ta>
            <ta e="T35" id="Seg_437" s="T34">ontɨ-ndɨ-se</ta>
            <ta e="T36" id="Seg_438" s="T35">poqqo-m</ta>
            <ta e="T37" id="Seg_439" s="T36">pot-mbɨdi</ta>
            <ta e="T38" id="Seg_440" s="T37">poqqo-m</ta>
            <ta e="T39" id="Seg_441" s="T38">nɨškä-le-qɨl-mbɨ-tɨ</ta>
            <ta e="T40" id="Seg_442" s="T39">qwǝlɨ-lʼdi</ta>
            <ta e="T41" id="Seg_443" s="T40">qum</ta>
            <ta e="T42" id="Seg_444" s="T41">tüː-ŋɨ-n</ta>
            <ta e="T43" id="Seg_445" s="T42">toː-ndɨ</ta>
            <ta e="T44" id="Seg_446" s="T43">poqqo-m-tɨ</ta>
            <ta e="T45" id="Seg_447" s="T44">pot-mbɨdi-mɨ-qən</ta>
            <ta e="T46" id="Seg_448" s="T45">qo-mbɨ-tɨ</ta>
            <ta e="T47" id="Seg_449" s="T46">pilɨŋ-m-tɨ</ta>
            <ta e="T48" id="Seg_450" s="T47">a</ta>
            <ta e="T49" id="Seg_451" s="T48">pilɨŋ-tɨ</ta>
            <ta e="T50" id="Seg_452" s="T49">nɨškä-le-mbɨdi</ta>
            <ta e="T51" id="Seg_453" s="T50">qo-mbɨ-tɨ</ta>
            <ta e="T52" id="Seg_454" s="T51">konne-n</ta>
            <ta e="T53" id="Seg_455" s="T52">qwǝlɨ-lʼdi</ta>
            <ta e="T54" id="Seg_456" s="T53">qum-nɨ</ta>
            <ta e="T55" id="Seg_457" s="T54">nadə-m-ŋɨ-n</ta>
            <ta e="T56" id="Seg_458" s="T55">eː-sɨ-n</ta>
            <ta e="T57" id="Seg_459" s="T56">poqqo-m-tɨ</ta>
            <ta e="T58" id="Seg_460" s="T57">qawij-nče-gu</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_461" s="T1">elk.[NOM]</ta>
            <ta e="T3" id="Seg_462" s="T2">big</ta>
            <ta e="T4" id="Seg_463" s="T3">wild.animal.[NOM]</ta>
            <ta e="T5" id="Seg_464" s="T4">oneself.3SG</ta>
            <ta e="T6" id="Seg_465" s="T5">(s)he.[NOM]</ta>
            <ta e="T7" id="Seg_466" s="T6">high-3SG.S</ta>
            <ta e="T8" id="Seg_467" s="T7">horn-PL.[NOM]-3SG</ta>
            <ta e="T9" id="Seg_468" s="T8">long.[3SG.S]</ta>
            <ta e="T10" id="Seg_469" s="T9">big.[3SG.S]</ta>
            <ta e="T11" id="Seg_470" s="T10">leg-PL.[NOM]-3SG</ta>
            <ta e="T12" id="Seg_471" s="T11">long.[3SG.S]</ta>
            <ta e="T13" id="Seg_472" s="T12">oneself.3SG</ta>
            <ta e="T14" id="Seg_473" s="T13">(s)he.[NOM]</ta>
            <ta e="T15" id="Seg_474" s="T14">NEG</ta>
            <ta e="T16" id="Seg_475" s="T15">get.angry-PTCP.PST.[3SG.S]</ta>
            <ta e="T17" id="Seg_476" s="T16">spring-ADV.LOC</ta>
            <ta e="T18" id="Seg_477" s="T17">(s)he.[NOM]</ta>
            <ta e="T19" id="Seg_478" s="T18">get.angry-DUR-HAB-3SG.S</ta>
            <ta e="T20" id="Seg_479" s="T19">one-EP-ADVZ</ta>
            <ta e="T21" id="Seg_480" s="T20">elk.[NOM]</ta>
            <ta e="T22" id="Seg_481" s="T21">swim-INCH-CO.[3SG.S]</ta>
            <ta e="T23" id="Seg_482" s="T22">lake-GEN</ta>
            <ta e="T24" id="Seg_483" s="T23">across</ta>
            <ta e="T25" id="Seg_484" s="T24">but</ta>
            <ta e="T26" id="Seg_485" s="T25">lake-LOC</ta>
            <ta e="T27" id="Seg_486" s="T26">fish-CAP.ADJZ</ta>
            <ta e="T28" id="Seg_487" s="T27">human.being-PL.[NOM]</ta>
            <ta e="T29" id="Seg_488" s="T28">settle.net-DUR-PST-3PL</ta>
            <ta e="T30" id="Seg_489" s="T29">net-PL-ACC</ta>
            <ta e="T31" id="Seg_490" s="T30">fish-GEN-ALL</ta>
            <ta e="T32" id="Seg_491" s="T31">swim-INCH-ACTN-LOC-3SG</ta>
            <ta e="T33" id="Seg_492" s="T32">elk.[NOM]</ta>
            <ta e="T34" id="Seg_493" s="T33">carry.away-3SG.O</ta>
            <ta e="T35" id="Seg_494" s="T34">oneself.3SG-OBL.3SG-INSTR</ta>
            <ta e="T36" id="Seg_495" s="T35">net-ACC</ta>
            <ta e="T37" id="Seg_496" s="T36">settle.net-PTCP.PST</ta>
            <ta e="T38" id="Seg_497" s="T37">net-ACC</ta>
            <ta e="T39" id="Seg_498" s="T38">tear-INCH-MULO-PST.NAR-3SG.O</ta>
            <ta e="T40" id="Seg_499" s="T39">fish-CAP.ADJZ</ta>
            <ta e="T41" id="Seg_500" s="T40">human.being.[NOM]</ta>
            <ta e="T42" id="Seg_501" s="T41">come-CO-3SG.S</ta>
            <ta e="T43" id="Seg_502" s="T42">lake-ILL</ta>
            <ta e="T44" id="Seg_503" s="T43">net-ACC-3SG</ta>
            <ta e="T45" id="Seg_504" s="T44">settle.net-PTCP.PST-something-LOC</ta>
            <ta e="T46" id="Seg_505" s="T45">find-PST.NAR-3SG.O</ta>
            <ta e="T47" id="Seg_506" s="T46">half-ACC-3SG</ta>
            <ta e="T48" id="Seg_507" s="T47">but</ta>
            <ta e="T49" id="Seg_508" s="T48">half.[NOM]-3SG</ta>
            <ta e="T50" id="Seg_509" s="T49">tear-INCH-PTCP.PST</ta>
            <ta e="T51" id="Seg_510" s="T50">find-PST.NAR-3SG.O</ta>
            <ta e="T52" id="Seg_511" s="T51">upwards-ADV.LOC</ta>
            <ta e="T53" id="Seg_512" s="T52">fish-CAP.ADJZ</ta>
            <ta e="T54" id="Seg_513" s="T53">human.being-ALL</ta>
            <ta e="T55" id="Seg_514" s="T54">one.should-TRL-CO-3SG.S</ta>
            <ta e="T56" id="Seg_515" s="T55">be-PST-3SG.S</ta>
            <ta e="T57" id="Seg_516" s="T56">net-ACC-3SG</ta>
            <ta e="T58" id="Seg_517" s="T57">short-IPFV3-INF</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_518" s="T1">лось.[NOM]</ta>
            <ta e="T3" id="Seg_519" s="T2">большой</ta>
            <ta e="T4" id="Seg_520" s="T3">зверь.[NOM]</ta>
            <ta e="T5" id="Seg_521" s="T4">сам.3SG</ta>
            <ta e="T6" id="Seg_522" s="T5">он(а).[NOM]</ta>
            <ta e="T7" id="Seg_523" s="T6">высокий-3SG.S</ta>
            <ta e="T8" id="Seg_524" s="T7">рог-PL.[NOM]-3SG</ta>
            <ta e="T9" id="Seg_525" s="T8">длинный.[3SG.S]</ta>
            <ta e="T10" id="Seg_526" s="T9">большой.[3SG.S]</ta>
            <ta e="T11" id="Seg_527" s="T10">нога-PL.[NOM]-3SG</ta>
            <ta e="T12" id="Seg_528" s="T11">длинный.[3SG.S]</ta>
            <ta e="T13" id="Seg_529" s="T12">сам.3SG</ta>
            <ta e="T14" id="Seg_530" s="T13">он(а).[NOM]</ta>
            <ta e="T15" id="Seg_531" s="T14">NEG</ta>
            <ta e="T16" id="Seg_532" s="T15">рассердиться-PTCP.PST.[3SG.S]</ta>
            <ta e="T17" id="Seg_533" s="T16">весна-ADV.LOC</ta>
            <ta e="T18" id="Seg_534" s="T17">он(а).[NOM]</ta>
            <ta e="T19" id="Seg_535" s="T18">рассердиться-DUR-HAB-3SG.S</ta>
            <ta e="T20" id="Seg_536" s="T19">один-EP-ADVZ</ta>
            <ta e="T21" id="Seg_537" s="T20">лось.[NOM]</ta>
            <ta e="T22" id="Seg_538" s="T21">плыть-INCH-CO.[3SG.S]</ta>
            <ta e="T23" id="Seg_539" s="T22">озеро-GEN</ta>
            <ta e="T24" id="Seg_540" s="T23">через</ta>
            <ta e="T25" id="Seg_541" s="T24">а</ta>
            <ta e="T26" id="Seg_542" s="T25">озеро-LOC</ta>
            <ta e="T27" id="Seg_543" s="T26">рыба-CAP.ADJZ</ta>
            <ta e="T28" id="Seg_544" s="T27">человек-PL.[NOM]</ta>
            <ta e="T29" id="Seg_545" s="T28">поставить.сеть-DUR-PST-3PL</ta>
            <ta e="T30" id="Seg_546" s="T29">сеть-PL-ACC</ta>
            <ta e="T31" id="Seg_547" s="T30">рыба-GEN-ALL</ta>
            <ta e="T32" id="Seg_548" s="T31">плыть-INCH-ACTN-LOC-3SG</ta>
            <ta e="T33" id="Seg_549" s="T32">лось.[NOM]</ta>
            <ta e="T34" id="Seg_550" s="T33">отнести-3SG.O</ta>
            <ta e="T35" id="Seg_551" s="T34">сам.3SG-OBL.3SG-INSTR</ta>
            <ta e="T36" id="Seg_552" s="T35">сеть-ACC</ta>
            <ta e="T37" id="Seg_553" s="T36">поставить.сеть-PTCP.PST</ta>
            <ta e="T38" id="Seg_554" s="T37">сеть-ACC</ta>
            <ta e="T39" id="Seg_555" s="T38">сорвать-INCH-MULO-PST.NAR-3SG.O</ta>
            <ta e="T40" id="Seg_556" s="T39">рыба-CAP.ADJZ</ta>
            <ta e="T41" id="Seg_557" s="T40">человек.[NOM]</ta>
            <ta e="T42" id="Seg_558" s="T41">прийти-CO-3SG.S</ta>
            <ta e="T43" id="Seg_559" s="T42">озеро-ILL</ta>
            <ta e="T44" id="Seg_560" s="T43">сеть-ACC-3SG</ta>
            <ta e="T45" id="Seg_561" s="T44">поставить.сеть-PTCP.PST-нечто-LOC</ta>
            <ta e="T46" id="Seg_562" s="T45">найти-PST.NAR-3SG.O</ta>
            <ta e="T47" id="Seg_563" s="T46">половина-ACC-3SG</ta>
            <ta e="T48" id="Seg_564" s="T47">а</ta>
            <ta e="T49" id="Seg_565" s="T48">половина.[NOM]-3SG</ta>
            <ta e="T50" id="Seg_566" s="T49">сорвать-INCH-PTCP.PST</ta>
            <ta e="T51" id="Seg_567" s="T50">найти-PST.NAR-3SG.O</ta>
            <ta e="T52" id="Seg_568" s="T51">вверх-ADV.LOC</ta>
            <ta e="T53" id="Seg_569" s="T52">рыба-CAP.ADJZ</ta>
            <ta e="T54" id="Seg_570" s="T53">человек-ALL</ta>
            <ta e="T55" id="Seg_571" s="T54">надо-TRL-CO-3SG.S</ta>
            <ta e="T56" id="Seg_572" s="T55">быть-PST-3SG.S</ta>
            <ta e="T57" id="Seg_573" s="T56">сеть-ACC-3SG</ta>
            <ta e="T58" id="Seg_574" s="T57">короткий-IPFV3-INF</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_575" s="T1">n.[n:case]</ta>
            <ta e="T3" id="Seg_576" s="T2">adj</ta>
            <ta e="T4" id="Seg_577" s="T3">n.[n:case]</ta>
            <ta e="T5" id="Seg_578" s="T4">emphpro</ta>
            <ta e="T6" id="Seg_579" s="T5">pers.[n:case]</ta>
            <ta e="T7" id="Seg_580" s="T6">adj-v:pn</ta>
            <ta e="T8" id="Seg_581" s="T7">n-n:num.[n:case]-n:poss</ta>
            <ta e="T9" id="Seg_582" s="T8">adj.[v:pn]</ta>
            <ta e="T10" id="Seg_583" s="T9">adj.[v:pn]</ta>
            <ta e="T11" id="Seg_584" s="T10">n-n:num.[n:case]-n:poss</ta>
            <ta e="T12" id="Seg_585" s="T11">adj.[v:pn]</ta>
            <ta e="T13" id="Seg_586" s="T12">emphpro</ta>
            <ta e="T14" id="Seg_587" s="T13">pers.[n:case]</ta>
            <ta e="T15" id="Seg_588" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_589" s="T15">v-v&gt;ptcp.[v:pn]</ta>
            <ta e="T17" id="Seg_590" s="T16">n-adv:case</ta>
            <ta e="T18" id="Seg_591" s="T17">pers.[n:case]</ta>
            <ta e="T19" id="Seg_592" s="T18">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T20" id="Seg_593" s="T19">num-n:ins-adj&gt;adv</ta>
            <ta e="T21" id="Seg_594" s="T20">n.[n:case]</ta>
            <ta e="T22" id="Seg_595" s="T21">v-v&gt;v-v:ins.[v:pn]</ta>
            <ta e="T23" id="Seg_596" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_597" s="T23">pp</ta>
            <ta e="T25" id="Seg_598" s="T24">conj</ta>
            <ta e="T26" id="Seg_599" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_600" s="T26">n&gt;adj</ta>
            <ta e="T28" id="Seg_601" s="T27">n-n:num.[n:case]</ta>
            <ta e="T29" id="Seg_602" s="T28">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_603" s="T29">n-n:num-n:case</ta>
            <ta e="T31" id="Seg_604" s="T30">n-n:case-n:case</ta>
            <ta e="T32" id="Seg_605" s="T31">v-v&gt;v-v&gt;n-n:case-n:poss</ta>
            <ta e="T33" id="Seg_606" s="T32">n.[n:case]</ta>
            <ta e="T34" id="Seg_607" s="T33">v-v:pn</ta>
            <ta e="T35" id="Seg_608" s="T34">emphpro-n:obl.poss-n:case</ta>
            <ta e="T36" id="Seg_609" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_610" s="T36">v-v&gt;ptcp</ta>
            <ta e="T38" id="Seg_611" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_612" s="T38">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_613" s="T39">n&gt;adj</ta>
            <ta e="T41" id="Seg_614" s="T40">n.[n:case]</ta>
            <ta e="T42" id="Seg_615" s="T41">v-v:ins-v:pn</ta>
            <ta e="T43" id="Seg_616" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_617" s="T43">n-n:case-n:poss</ta>
            <ta e="T45" id="Seg_618" s="T44">v-v&gt;ptcp-n-n:case</ta>
            <ta e="T46" id="Seg_619" s="T45">v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_620" s="T46">n-n:case-n:poss</ta>
            <ta e="T48" id="Seg_621" s="T47">conj</ta>
            <ta e="T49" id="Seg_622" s="T48">n.[n:case]-n:poss</ta>
            <ta e="T50" id="Seg_623" s="T49">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T51" id="Seg_624" s="T50">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_625" s="T51">adv-adv:case</ta>
            <ta e="T53" id="Seg_626" s="T52">n&gt;adj</ta>
            <ta e="T54" id="Seg_627" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_628" s="T54">ptcl-n&gt;v-v:ins-v:pn</ta>
            <ta e="T56" id="Seg_629" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_630" s="T56">n-n:case-n:poss</ta>
            <ta e="T58" id="Seg_631" s="T57">adj-v&gt;v-v:inf</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_632" s="T1">n</ta>
            <ta e="T3" id="Seg_633" s="T2">adj</ta>
            <ta e="T4" id="Seg_634" s="T3">n</ta>
            <ta e="T5" id="Seg_635" s="T4">emphpro</ta>
            <ta e="T6" id="Seg_636" s="T5">pers</ta>
            <ta e="T7" id="Seg_637" s="T6">adj</ta>
            <ta e="T8" id="Seg_638" s="T7">v</ta>
            <ta e="T9" id="Seg_639" s="T8">adj</ta>
            <ta e="T10" id="Seg_640" s="T9">adj</ta>
            <ta e="T11" id="Seg_641" s="T10">n</ta>
            <ta e="T12" id="Seg_642" s="T11">adj</ta>
            <ta e="T13" id="Seg_643" s="T12">emphpro</ta>
            <ta e="T14" id="Seg_644" s="T13">pers</ta>
            <ta e="T15" id="Seg_645" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_646" s="T15">ptcp</ta>
            <ta e="T17" id="Seg_647" s="T16">n</ta>
            <ta e="T18" id="Seg_648" s="T17">pers</ta>
            <ta e="T19" id="Seg_649" s="T18">v</ta>
            <ta e="T20" id="Seg_650" s="T19">adv</ta>
            <ta e="T21" id="Seg_651" s="T20">pers</ta>
            <ta e="T22" id="Seg_652" s="T21">v</ta>
            <ta e="T23" id="Seg_653" s="T22">n</ta>
            <ta e="T24" id="Seg_654" s="T23">pp</ta>
            <ta e="T25" id="Seg_655" s="T24">conj</ta>
            <ta e="T26" id="Seg_656" s="T25">n</ta>
            <ta e="T27" id="Seg_657" s="T26">adj</ta>
            <ta e="T28" id="Seg_658" s="T27">n</ta>
            <ta e="T29" id="Seg_659" s="T28">v</ta>
            <ta e="T30" id="Seg_660" s="T29">n</ta>
            <ta e="T31" id="Seg_661" s="T30">n</ta>
            <ta e="T32" id="Seg_662" s="T31">v</ta>
            <ta e="T33" id="Seg_663" s="T32">n</ta>
            <ta e="T34" id="Seg_664" s="T33">v</ta>
            <ta e="T35" id="Seg_665" s="T34">emphpro</ta>
            <ta e="T36" id="Seg_666" s="T35">n</ta>
            <ta e="T37" id="Seg_667" s="T36">ptcp</ta>
            <ta e="T38" id="Seg_668" s="T37">n</ta>
            <ta e="T39" id="Seg_669" s="T38">v</ta>
            <ta e="T40" id="Seg_670" s="T39">adj</ta>
            <ta e="T41" id="Seg_671" s="T40">n</ta>
            <ta e="T42" id="Seg_672" s="T41">v</ta>
            <ta e="T43" id="Seg_673" s="T42">n</ta>
            <ta e="T44" id="Seg_674" s="T43">n</ta>
            <ta e="T45" id="Seg_675" s="T44">n</ta>
            <ta e="T46" id="Seg_676" s="T45">v</ta>
            <ta e="T47" id="Seg_677" s="T46">n</ta>
            <ta e="T48" id="Seg_678" s="T47">conj</ta>
            <ta e="T49" id="Seg_679" s="T48">n</ta>
            <ta e="T50" id="Seg_680" s="T49">adj</ta>
            <ta e="T51" id="Seg_681" s="T50">v</ta>
            <ta e="T52" id="Seg_682" s="T51">adv</ta>
            <ta e="T53" id="Seg_683" s="T52">adj</ta>
            <ta e="T54" id="Seg_684" s="T53">n</ta>
            <ta e="T55" id="Seg_685" s="T54">v</ta>
            <ta e="T56" id="Seg_686" s="T55">v</ta>
            <ta e="T57" id="Seg_687" s="T56">n</ta>
            <ta e="T58" id="Seg_688" s="T57">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_689" s="T1">np:S</ta>
            <ta e="T4" id="Seg_690" s="T3">n:pred</ta>
            <ta e="T6" id="Seg_691" s="T5">pro:S</ta>
            <ta e="T7" id="Seg_692" s="T6">adj:pred</ta>
            <ta e="T8" id="Seg_693" s="T7">np:S</ta>
            <ta e="T9" id="Seg_694" s="T8">adj:pred</ta>
            <ta e="T10" id="Seg_695" s="T9">0.3:S adj:pred</ta>
            <ta e="T11" id="Seg_696" s="T10">np:S</ta>
            <ta e="T12" id="Seg_697" s="T11">adj:pred</ta>
            <ta e="T14" id="Seg_698" s="T13">pro.h:S</ta>
            <ta e="T16" id="Seg_699" s="T15">adj:pred</ta>
            <ta e="T18" id="Seg_700" s="T17">pro.h:S</ta>
            <ta e="T19" id="Seg_701" s="T18">v:pred</ta>
            <ta e="T21" id="Seg_702" s="T20">np:S</ta>
            <ta e="T22" id="Seg_703" s="T21">v:pred</ta>
            <ta e="T28" id="Seg_704" s="T27">np.h:S</ta>
            <ta e="T29" id="Seg_705" s="T28">v:pred</ta>
            <ta e="T30" id="Seg_706" s="T29">np:O</ta>
            <ta e="T32" id="Seg_707" s="T31">s:temp</ta>
            <ta e="T33" id="Seg_708" s="T32">np:S</ta>
            <ta e="T34" id="Seg_709" s="T33">v:pred</ta>
            <ta e="T36" id="Seg_710" s="T35">np:O</ta>
            <ta e="T38" id="Seg_711" s="T37">np:O</ta>
            <ta e="T39" id="Seg_712" s="T38">0.3:S v:pred</ta>
            <ta e="T41" id="Seg_713" s="T40">np.h:S</ta>
            <ta e="T42" id="Seg_714" s="T41">v:pred</ta>
            <ta e="T44" id="Seg_715" s="T43">np:O</ta>
            <ta e="T46" id="Seg_716" s="T45">0.3.h:S v:pred</ta>
            <ta e="T47" id="Seg_717" s="T46">np:O</ta>
            <ta e="T49" id="Seg_718" s="T48">np:O</ta>
            <ta e="T50" id="Seg_719" s="T49">s:rel</ta>
            <ta e="T51" id="Seg_720" s="T50">0.3.h:S v:pred</ta>
            <ta e="T55" id="Seg_721" s="T54">v:pred</ta>
            <ta e="T58" id="Seg_722" s="T57">v:O</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_723" s="T1">np:Th</ta>
            <ta e="T6" id="Seg_724" s="T5">pro:Th</ta>
            <ta e="T8" id="Seg_725" s="T7">np:Th 0.3:Poss</ta>
            <ta e="T10" id="Seg_726" s="T9">0.3:Th</ta>
            <ta e="T11" id="Seg_727" s="T10">np:Th 0.3:Poss</ta>
            <ta e="T14" id="Seg_728" s="T13">pro.h:E</ta>
            <ta e="T17" id="Seg_729" s="T16">adv:Time</ta>
            <ta e="T18" id="Seg_730" s="T17">pro.h:E</ta>
            <ta e="T20" id="Seg_731" s="T19">adv:Time</ta>
            <ta e="T21" id="Seg_732" s="T20">np:A</ta>
            <ta e="T24" id="Seg_733" s="T22">pp:Path</ta>
            <ta e="T26" id="Seg_734" s="T25">np:L</ta>
            <ta e="T28" id="Seg_735" s="T27">np.h:A</ta>
            <ta e="T30" id="Seg_736" s="T29">np:Th</ta>
            <ta e="T33" id="Seg_737" s="T32">np:A</ta>
            <ta e="T36" id="Seg_738" s="T35">np:Th</ta>
            <ta e="T38" id="Seg_739" s="T37">np:P</ta>
            <ta e="T39" id="Seg_740" s="T38">0.3:A</ta>
            <ta e="T41" id="Seg_741" s="T40">np.h:A</ta>
            <ta e="T43" id="Seg_742" s="T42">np:G</ta>
            <ta e="T44" id="Seg_743" s="T43">np:Th 0.3.h:Poss</ta>
            <ta e="T46" id="Seg_744" s="T45">0.3.h:E</ta>
            <ta e="T47" id="Seg_745" s="T46">np:Th</ta>
            <ta e="T49" id="Seg_746" s="T48">np:Th</ta>
            <ta e="T51" id="Seg_747" s="T50">0.3.h:A</ta>
            <ta e="T52" id="Seg_748" s="T51">adv:L</ta>
            <ta e="T54" id="Seg_749" s="T53">np.h:A</ta>
            <ta e="T57" id="Seg_750" s="T56">np:Th 0.3.h:Poss</ta>
            <ta e="T58" id="Seg_751" s="T57">0.3.h:A v:Th</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T25" id="Seg_752" s="T24">RUS:gram</ta>
            <ta e="T48" id="Seg_753" s="T47">RUS:gram</ta>
            <ta e="T55" id="Seg_754" s="T54">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_755" s="T1">The elk is a big animal.</ta>
            <ta e="T12" id="Seg_756" s="T4">He is tall, his antlers are long, big, his legs are long.</ta>
            <ta e="T16" id="Seg_757" s="T12">He is not angry.</ta>
            <ta e="T19" id="Seg_758" s="T16">In spring he gets angry.</ta>
            <ta e="T24" id="Seg_759" s="T19">Once the elk swims across the lake.</ta>
            <ta e="T31" id="Seg_760" s="T24">The fishermen cast nets in the lake for fish.</ta>
            <ta e="T36" id="Seg_761" s="T31">When the elk swims, it carries the net with it.</ta>
            <ta e="T39" id="Seg_762" s="T36">He tore the settled nets.</ta>
            <ta e="T43" id="Seg_763" s="T39">The fisherman comes to the lake.</ta>
            <ta e="T52" id="Seg_764" s="T43">He saw one half, he saw the other half at the shore.</ta>
            <ta e="T58" id="Seg_765" s="T52">The fisherman had to cut the net.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_766" s="T1">Der Elch ist ein großes Tier.</ta>
            <ta e="T12" id="Seg_767" s="T4">Er selbst ist hoch, sein Geweih sind lang, groß, seine Beine sind lang.</ta>
            <ta e="T16" id="Seg_768" s="T12">Er ist nicht böse.</ta>
            <ta e="T19" id="Seg_769" s="T16">Im Frühling wird er böse.</ta>
            <ta e="T24" id="Seg_770" s="T19">Einmal schwamm der Elch durch den See.</ta>
            <ta e="T31" id="Seg_771" s="T24">Im See stellten die Fischer die Netze für die Fische auf.</ta>
            <ta e="T36" id="Seg_772" s="T31">Wenn der Elch schwimmt, nimmt er das Netz mit sich.</ta>
            <ta e="T39" id="Seg_773" s="T36">Er zerriß das aufgestellte Netz.</ta>
            <ta e="T43" id="Seg_774" s="T39">Der Fischer kommt zum See. </ta>
            <ta e="T52" id="Seg_775" s="T43">Er findet eine Hälfte des Netzes, dort wohin er das Netz gestellt hat; die andere Hälfte fand er am Ufer.</ta>
            <ta e="T58" id="Seg_776" s="T52">Der Fischer musste das Netz kürzen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_777" s="T1">Лось – зверь большой. </ta>
            <ta e="T12" id="Seg_778" s="T4">Он высокий, рога большие, ноги длинные. </ta>
            <ta e="T16" id="Seg_779" s="T12">Он не сердитый.</ta>
            <ta e="T19" id="Seg_780" s="T16">Весной он бывает сердитым.</ta>
            <ta e="T24" id="Seg_781" s="T19">Однажды лось плыл через озеро. </ta>
            <ta e="T31" id="Seg_782" s="T24">Рыбаки ставили в озере сети. </ta>
            <ta e="T36" id="Seg_783" s="T31">Когда лось плыл, унёс их с собой. </ta>
            <ta e="T39" id="Seg_784" s="T36">Он порвал сети. </ta>
            <ta e="T43" id="Seg_785" s="T39">Приехал рыбак на озеро.</ta>
            <ta e="T52" id="Seg_786" s="T43">Он нашёл только половину, другую, порванную, нашёл на берегу.</ta>
            <ta e="T58" id="Seg_787" s="T52">Рыбаку пришлось сеть укоротить.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_788" s="T1">лось большой зверь</ta>
            <ta e="T12" id="Seg_789" s="T4">сам по себе он высокий, рога длинные большие ноги длинные</ta>
            <ta e="T16" id="Seg_790" s="T12">сам по себе он не сердитый</ta>
            <ta e="T19" id="Seg_791" s="T16">весной он бывает сердитым</ta>
            <ta e="T24" id="Seg_792" s="T19">однажды лось плыл через озеро</ta>
            <ta e="T31" id="Seg_793" s="T24">в озере рыбаки ставили сети для рыбы.</ta>
            <ta e="T36" id="Seg_794" s="T31">когда плыл лось унес с собой сетку </ta>
            <ta e="T39" id="Seg_795" s="T36">поставленную сетку порвал.</ta>
            <ta e="T43" id="Seg_796" s="T39">рыбак приехал в озеро.</ta>
            <ta e="T52" id="Seg_797" s="T43">сетку где ставил нашел половину (сети), а половину изорванную (другую) нашел на берегу</ta>
            <ta e="T58" id="Seg_798" s="T52">рыбаку нужно было окоротить.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
