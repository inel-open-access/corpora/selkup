<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KMS_1963_FortuneTelling_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KMS_1963_FortuneTelling_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">51</ud-information>
            <ud-information attribute-name="# HIAT:w">40</ud-information>
            <ud-information attribute-name="# e">40</ud-information>
            <ud-information attribute-name="# HIAT:u">7</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KMS">
            <abbreviation>KMS</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KMS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T40" id="Seg_0" n="sc" s="T0">
               <ts e="T8" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">sombiri</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">qwelum</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">sombɨreɣɨndɨ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">meŋa</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">sombɨrsä</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">qaːltan</ts>
                  <nts id="Seg_20" n="HIAT:ip">:</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">soːŋ</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">ilenǯandə</ts>
                  <nts id="Seg_27" n="HIAT:ip">.</nts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_30" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">tamgän</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">qwärɣə</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">qwatčendal</ts>
                  <nts id="Seg_39" n="HIAT:ip">,</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">surum</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">maːt</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">qonǯindal</ts>
                  <nts id="Seg_49" n="HIAT:ip">,</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">qonɨŋ</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">nʼäjam</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">qwätčindal</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_62" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">pot</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">mandəzijen</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_71" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">man</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">qüdälʼe</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">ippɨzaŋ</ts>
                  <nts id="Seg_80" n="HIAT:ip">.</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_83" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">qaj</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">qwäːrɣɨm</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">nassə</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">qwässaw</ts>
                  <nts id="Seg_95" n="HIAT:ip">,</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">nʼäja</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">suːrum</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">assə</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">qwässaw</ts>
                  <nts id="Seg_108" n="HIAT:ip">.</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_111" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">i</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_116" n="HIAT:w" s="T31">naːdɨm</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">wes</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">küderen</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">mɨndə</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">qonǯersaw</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_132" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_134" n="HIAT:w" s="T36">täp</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_137" n="HIAT:w" s="T37">meŋa</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_140" n="HIAT:w" s="T38">sain</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_143" n="HIAT:w" s="T39">sitʼeptɨŋ</ts>
                  <nts id="Seg_144" n="HIAT:ip">.</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T40" id="Seg_146" n="sc" s="T0">
               <ts e="T1" id="Seg_148" n="e" s="T0">sombiri </ts>
               <ts e="T2" id="Seg_150" n="e" s="T1">qwelum </ts>
               <ts e="T3" id="Seg_152" n="e" s="T2">sombɨreɣɨndɨ </ts>
               <ts e="T4" id="Seg_154" n="e" s="T3">meŋa </ts>
               <ts e="T5" id="Seg_156" n="e" s="T4">sombɨrsä </ts>
               <ts e="T6" id="Seg_158" n="e" s="T5">qaːltan: </ts>
               <ts e="T7" id="Seg_160" n="e" s="T6">soːŋ </ts>
               <ts e="T8" id="Seg_162" n="e" s="T7">ilenǯandə. </ts>
               <ts e="T9" id="Seg_164" n="e" s="T8">tamgän </ts>
               <ts e="T10" id="Seg_166" n="e" s="T9">qwärɣə </ts>
               <ts e="T11" id="Seg_168" n="e" s="T10">qwatčendal, </ts>
               <ts e="T12" id="Seg_170" n="e" s="T11">surum </ts>
               <ts e="T13" id="Seg_172" n="e" s="T12">maːt </ts>
               <ts e="T14" id="Seg_174" n="e" s="T13">qonǯindal, </ts>
               <ts e="T15" id="Seg_176" n="e" s="T14">qonɨŋ </ts>
               <ts e="T16" id="Seg_178" n="e" s="T15">nʼäjam </ts>
               <ts e="T17" id="Seg_180" n="e" s="T16">qwätčindal. </ts>
               <ts e="T18" id="Seg_182" n="e" s="T17">pot </ts>
               <ts e="T19" id="Seg_184" n="e" s="T18">mandəzijen. </ts>
               <ts e="T20" id="Seg_186" n="e" s="T19">man </ts>
               <ts e="T21" id="Seg_188" n="e" s="T20">qüdälʼe </ts>
               <ts e="T22" id="Seg_190" n="e" s="T21">ippɨzaŋ. </ts>
               <ts e="T23" id="Seg_192" n="e" s="T22">qaj </ts>
               <ts e="T24" id="Seg_194" n="e" s="T23">qwäːrɣɨm </ts>
               <ts e="T25" id="Seg_196" n="e" s="T24">nassə </ts>
               <ts e="T26" id="Seg_198" n="e" s="T25">qwässaw, </ts>
               <ts e="T27" id="Seg_200" n="e" s="T26">nʼäja </ts>
               <ts e="T28" id="Seg_202" n="e" s="T27">suːrum </ts>
               <ts e="T29" id="Seg_204" n="e" s="T28">assə </ts>
               <ts e="T30" id="Seg_206" n="e" s="T29">qwässaw. </ts>
               <ts e="T31" id="Seg_208" n="e" s="T30">i </ts>
               <ts e="T32" id="Seg_210" n="e" s="T31">naːdɨm </ts>
               <ts e="T33" id="Seg_212" n="e" s="T32">wes </ts>
               <ts e="T34" id="Seg_214" n="e" s="T33">küderen </ts>
               <ts e="T35" id="Seg_216" n="e" s="T34">mɨndə </ts>
               <ts e="T36" id="Seg_218" n="e" s="T35">qonǯersaw. </ts>
               <ts e="T37" id="Seg_220" n="e" s="T36">täp </ts>
               <ts e="T38" id="Seg_222" n="e" s="T37">meŋa </ts>
               <ts e="T39" id="Seg_224" n="e" s="T38">sain </ts>
               <ts e="T40" id="Seg_226" n="e" s="T39">sitʼeptɨŋ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T8" id="Seg_227" s="T0">KMS_1963_FortuneTelling_nar.001 (001.001)</ta>
            <ta e="T17" id="Seg_228" s="T8">KMS_1963_FortuneTelling_nar.002 (001.002)</ta>
            <ta e="T19" id="Seg_229" s="T17">KMS_1963_FortuneTelling_nar.003 (001.003)</ta>
            <ta e="T22" id="Seg_230" s="T19">KMS_1963_FortuneTelling_nar.004 (001.004)</ta>
            <ta e="T30" id="Seg_231" s="T22">KMS_1963_FortuneTelling_nar.005 (001.005)</ta>
            <ta e="T36" id="Seg_232" s="T30">KMS_1963_FortuneTelling_nar.006 (001.006)</ta>
            <ta e="T40" id="Seg_233" s="T36">KMS_1963_FortuneTelling_nar.007 (001.007)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T8" id="Seg_234" s="T0">сомби′ри ′kw(в)елум ‵сомбы′реɣынды ′меңа ′сомбыр‵сӓ kа̄л′тан: со̄ң и′ленджандъ.</ta>
            <ta e="T17" id="Seg_235" s="T8">‵там′гӓн ′kwӓрɣъ ‵kwаттшен′дал, ′сурум ма̄т ‵kонджиндал, ′kоның ′нʼӓjам ′kwӓттшин′дал.</ta>
            <ta e="T19" id="Seg_236" s="T17">пот мандъ′зиjен.</ta>
            <ta e="T22" id="Seg_237" s="T19">ман ′kӱдӓлʼе ‵иппы′заң.</ta>
            <ta e="T30" id="Seg_238" s="T22">kай ′kwӓ̄рɣым ′нассъ kwӓ′ссаw, ′нʼӓjа ′сӯрум ассъ kwӓ′ссаw.</ta>
            <ta e="T36" id="Seg_239" s="T30">и ′на̄дым вес ′кӱдерен ′мындъ kонджер′саw.</ta>
            <ta e="T40" id="Seg_240" s="T36">тӓп ′меңа ′са′ин си′тʼептың.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T8" id="Seg_241" s="T0">sombiri qw(v)elum sombɨreɣɨndɨ meŋa sombɨrsä qaːltan: soːŋ ilenǯandə.</ta>
            <ta e="T17" id="Seg_242" s="T8">tamgän qwärɣə qwattšendal, surum maːt qonǯindal, qonɨŋ nʼäjam qwättšindal.</ta>
            <ta e="T19" id="Seg_243" s="T17">pot mandəzijen.</ta>
            <ta e="T22" id="Seg_244" s="T19">man qüdälʼe ippɨzaŋ.</ta>
            <ta e="T30" id="Seg_245" s="T22">qaj qwäːrɣɨm nassə qwässaw, nʼäja suːrum assə qwässaw.</ta>
            <ta e="T36" id="Seg_246" s="T30">i naːdɨm ves küderen mɨndə qonǯersaw.</ta>
            <ta e="T40" id="Seg_247" s="T36">täp meŋa sain sitʼeptɨŋ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T8" id="Seg_248" s="T0">sombiri qwelum sombɨreɣɨndɨ meŋa sombɨrsä qaːltan: soːŋ ilenǯandə. </ta>
            <ta e="T17" id="Seg_249" s="T8">tamgän qwärɣə qwatčendal, surum maːt qonǯindal, qonɨŋ nʼäjam qwätčindal. </ta>
            <ta e="T19" id="Seg_250" s="T17">pot mandəzijen. </ta>
            <ta e="T22" id="Seg_251" s="T19">man qüdälʼe ippɨzaŋ. </ta>
            <ta e="T30" id="Seg_252" s="T22">qaj qwäːrɣɨm nassə qwässaw, nʼäja suːrum assə qwässaw. </ta>
            <ta e="T36" id="Seg_253" s="T30">i naːdɨm wes küderen mɨndə qonǯersaw. </ta>
            <ta e="T40" id="Seg_254" s="T36">täp meŋa sain sitʼeptɨŋ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_255" s="T0">sombiri</ta>
            <ta e="T2" id="Seg_256" s="T1">qwelu-m</ta>
            <ta e="T3" id="Seg_257" s="T2">sombɨ-r-e-ɣɨndɨ</ta>
            <ta e="T4" id="Seg_258" s="T3">meŋa</ta>
            <ta e="T5" id="Seg_259" s="T4">sombɨ-r-sä</ta>
            <ta e="T6" id="Seg_260" s="T5">qaːl-ta-n</ta>
            <ta e="T7" id="Seg_261" s="T6">soː-ŋ</ta>
            <ta e="T8" id="Seg_262" s="T7">ile-nǯa-ndə</ta>
            <ta e="T9" id="Seg_263" s="T8">tam-gä-n</ta>
            <ta e="T10" id="Seg_264" s="T9">qwärɣə</ta>
            <ta e="T11" id="Seg_265" s="T10">qwat-če-nda-l</ta>
            <ta e="T12" id="Seg_266" s="T11">surum</ta>
            <ta e="T13" id="Seg_267" s="T12">maːt</ta>
            <ta e="T14" id="Seg_268" s="T13">qo-nǯi-nda-l</ta>
            <ta e="T15" id="Seg_269" s="T14">qonɨŋ</ta>
            <ta e="T16" id="Seg_270" s="T15">nʼäja-m</ta>
            <ta e="T17" id="Seg_271" s="T16">qwät-či-nda-l</ta>
            <ta e="T18" id="Seg_272" s="T17">po-t</ta>
            <ta e="T19" id="Seg_273" s="T18">mandə-zi-je-n</ta>
            <ta e="T20" id="Seg_274" s="T19">man</ta>
            <ta e="T21" id="Seg_275" s="T20">qüdä-lʼe</ta>
            <ta e="T22" id="Seg_276" s="T21">ippɨ-za-ŋ</ta>
            <ta e="T23" id="Seg_277" s="T22">qaj</ta>
            <ta e="T24" id="Seg_278" s="T23">qwäːrɣɨ-m</ta>
            <ta e="T25" id="Seg_279" s="T24">nassə</ta>
            <ta e="T26" id="Seg_280" s="T25">qwäs-sa-w</ta>
            <ta e="T27" id="Seg_281" s="T26">nʼäja</ta>
            <ta e="T28" id="Seg_282" s="T27">suːrum</ta>
            <ta e="T29" id="Seg_283" s="T28">assə</ta>
            <ta e="T30" id="Seg_284" s="T29">qwäs-sa-w</ta>
            <ta e="T31" id="Seg_285" s="T30">i</ta>
            <ta e="T32" id="Seg_286" s="T31">naːdɨ-m</ta>
            <ta e="T33" id="Seg_287" s="T32">wes</ta>
            <ta e="T34" id="Seg_288" s="T33">küder-e-n</ta>
            <ta e="T35" id="Seg_289" s="T34">mɨ-ndə</ta>
            <ta e="T36" id="Seg_290" s="T35">qo-nǯe-r-sa-w</ta>
            <ta e="T37" id="Seg_291" s="T36">täp</ta>
            <ta e="T38" id="Seg_292" s="T37">meŋa</ta>
            <ta e="T39" id="Seg_293" s="T38">sai-n</ta>
            <ta e="T40" id="Seg_294" s="T39">sitʼeptɨ-ŋ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_295" s="T0">sombiri</ta>
            <ta e="T2" id="Seg_296" s="T1">qwelu-m</ta>
            <ta e="T3" id="Seg_297" s="T2">somba-r-e-qɨntɨ</ta>
            <ta e="T4" id="Seg_298" s="T3">mäkkä</ta>
            <ta e="T5" id="Seg_299" s="T4">somba-r-sɨ</ta>
            <ta e="T6" id="Seg_300" s="T5">qalɨ-ntɨ-n</ta>
            <ta e="T7" id="Seg_301" s="T6">sawa-k</ta>
            <ta e="T8" id="Seg_302" s="T7">ilɨ-nǯɨ-ndɨ</ta>
            <ta e="T9" id="Seg_303" s="T8">taw-ka-n</ta>
            <ta e="T10" id="Seg_304" s="T9">qwärqa</ta>
            <ta e="T11" id="Seg_305" s="T10">qwat-nǯɨ-ntɨ-l</ta>
            <ta e="T12" id="Seg_306" s="T11">suːrǝm</ta>
            <ta e="T13" id="Seg_307" s="T12">maːt</ta>
            <ta e="T14" id="Seg_308" s="T13">qo-nǯɨ-ntɨ-l</ta>
            <ta e="T15" id="Seg_309" s="T14">qoːnɨŋ</ta>
            <ta e="T16" id="Seg_310" s="T15">nʼaja-m</ta>
            <ta e="T17" id="Seg_311" s="T16">qwat-nǯɨ-ntɨ-l</ta>
            <ta e="T18" id="Seg_312" s="T17">po-t</ta>
            <ta e="T19" id="Seg_313" s="T18">mändə-sɨ-ŋɨ-n</ta>
            <ta e="T20" id="Seg_314" s="T19">man</ta>
            <ta e="T21" id="Seg_315" s="T20">qüːtǝ-le</ta>
            <ta e="T22" id="Seg_316" s="T21">ippi-sɨ-ŋ</ta>
            <ta e="T23" id="Seg_317" s="T22">qaj</ta>
            <ta e="T24" id="Seg_318" s="T23">qwärqa-m</ta>
            <ta e="T25" id="Seg_319" s="T24">ašša</ta>
            <ta e="T26" id="Seg_320" s="T25">qwat-sɨ-m</ta>
            <ta e="T27" id="Seg_321" s="T26">nʼaja</ta>
            <ta e="T28" id="Seg_322" s="T27">suːrǝm</ta>
            <ta e="T29" id="Seg_323" s="T28">ašša</ta>
            <ta e="T30" id="Seg_324" s="T29">qwat-sɨ-m</ta>
            <ta e="T31" id="Seg_325" s="T30">i</ta>
            <ta e="T32" id="Seg_326" s="T31">naːdə-m</ta>
            <ta e="T33" id="Seg_327" s="T32">wesʼ</ta>
            <ta e="T34" id="Seg_328" s="T33">küdɨr-e-n</ta>
            <ta e="T35" id="Seg_329" s="T34">mɨ-ndɨ</ta>
            <ta e="T36" id="Seg_330" s="T35">qo-nče-r-sɨ-m</ta>
            <ta e="T37" id="Seg_331" s="T36">tap</ta>
            <ta e="T38" id="Seg_332" s="T37">mäkkä</ta>
            <ta e="T39" id="Seg_333" s="T38">saj-nɨ</ta>
            <ta e="T40" id="Seg_334" s="T39">sitteptɨ-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_335" s="T0">%%</ta>
            <ta e="T2" id="Seg_336" s="T1">Evenki-1SG</ta>
            <ta e="T3" id="Seg_337" s="T2">shamanize-FRQ-NMLZ-LOC.3SG</ta>
            <ta e="T4" id="Seg_338" s="T3">I.ALL</ta>
            <ta e="T5" id="Seg_339" s="T4">shamanize-FRQ-PST.[3SG.S]</ta>
            <ta e="T6" id="Seg_340" s="T5">stay-IPFV-3SG.S</ta>
            <ta e="T7" id="Seg_341" s="T6">good-ADVZ</ta>
            <ta e="T8" id="Seg_342" s="T7">live-FUT-2SG.S</ta>
            <ta e="T9" id="Seg_343" s="T8">there-winter-ADV.LOC</ta>
            <ta e="T10" id="Seg_344" s="T9">bear.[NOM]</ta>
            <ta e="T11" id="Seg_345" s="T10">kill-FUT-INFER-2SG.O</ta>
            <ta e="T12" id="Seg_346" s="T11">wild.animal.[NOM]</ta>
            <ta e="T13" id="Seg_347" s="T12">house.[NOM]</ta>
            <ta e="T14" id="Seg_348" s="T13">see-FUT-INFER-2SG.O</ta>
            <ta e="T15" id="Seg_349" s="T14">many</ta>
            <ta e="T16" id="Seg_350" s="T15">squirrel-ACC</ta>
            <ta e="T17" id="Seg_351" s="T16">kill-FUT-INFER-2SG.O</ta>
            <ta e="T18" id="Seg_352" s="T17">year-3SG</ta>
            <ta e="T19" id="Seg_353" s="T18">pass.through-PST-CO-3SG.S</ta>
            <ta e="T20" id="Seg_354" s="T19">I.NOM</ta>
            <ta e="T21" id="Seg_355" s="T20">be.ill-CVB</ta>
            <ta e="T22" id="Seg_356" s="T21">lie-PST-1SG.S</ta>
            <ta e="T23" id="Seg_357" s="T22">what</ta>
            <ta e="T24" id="Seg_358" s="T23">bear-ACC</ta>
            <ta e="T25" id="Seg_359" s="T24">NEG</ta>
            <ta e="T26" id="Seg_360" s="T25">kill-PST-1SG.O</ta>
            <ta e="T27" id="Seg_361" s="T26">squirrel.[NOM]</ta>
            <ta e="T28" id="Seg_362" s="T27">wild.animal.[NOM]</ta>
            <ta e="T29" id="Seg_363" s="T28">NEG</ta>
            <ta e="T30" id="Seg_364" s="T29">kill-PST-1SG.O</ta>
            <ta e="T31" id="Seg_365" s="T30">and</ta>
            <ta e="T32" id="Seg_366" s="T31">this-ACC</ta>
            <ta e="T33" id="Seg_367" s="T32">all</ta>
            <ta e="T34" id="Seg_368" s="T33">see.dreams-NMLZ-GEN</ta>
            <ta e="T35" id="Seg_369" s="T34">something-ILL</ta>
            <ta e="T36" id="Seg_370" s="T35">see-IPFV-FRQ-PST-1SG.O</ta>
            <ta e="T37" id="Seg_371" s="T36">he.NOM</ta>
            <ta e="T38" id="Seg_372" s="T37">I.ALL</ta>
            <ta e="T39" id="Seg_373" s="T38">eye-ALL</ta>
            <ta e="T40" id="Seg_374" s="T39">cheat-3SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_375" s="T0">ворожец</ta>
            <ta e="T2" id="Seg_376" s="T1">эвенк-1SG</ta>
            <ta e="T3" id="Seg_377" s="T2">шаманить-FRQ-NMLZ-LOC.3SG</ta>
            <ta e="T4" id="Seg_378" s="T3">я.ALL</ta>
            <ta e="T5" id="Seg_379" s="T4">шаманить-FRQ-PST.[3SG.S]</ta>
            <ta e="T6" id="Seg_380" s="T5">остаться-IPFV-3SG.S</ta>
            <ta e="T7" id="Seg_381" s="T6">хороший-ADVZ</ta>
            <ta e="T8" id="Seg_382" s="T7">жить-FUT-2SG.S</ta>
            <ta e="T9" id="Seg_383" s="T8">там-зима-ADV.LOC</ta>
            <ta e="T10" id="Seg_384" s="T9">медведь.[NOM]</ta>
            <ta e="T11" id="Seg_385" s="T10">убить-FUT-INFER-2SG.O</ta>
            <ta e="T12" id="Seg_386" s="T11">зверь.[NOM]</ta>
            <ta e="T13" id="Seg_387" s="T12">дом.[NOM]</ta>
            <ta e="T14" id="Seg_388" s="T13">видеть-FUT-INFER-2SG.O</ta>
            <ta e="T15" id="Seg_389" s="T14">много</ta>
            <ta e="T16" id="Seg_390" s="T15">белка-ACC</ta>
            <ta e="T17" id="Seg_391" s="T16">убить-FUT-INFER-2SG.O</ta>
            <ta e="T18" id="Seg_392" s="T17">год-3SG</ta>
            <ta e="T19" id="Seg_393" s="T18">пройти-PST-CO-3SG.S</ta>
            <ta e="T20" id="Seg_394" s="T19">я.NOM</ta>
            <ta e="T21" id="Seg_395" s="T20">быть.больным-CVB</ta>
            <ta e="T22" id="Seg_396" s="T21">лежать-PST-1SG.S</ta>
            <ta e="T23" id="Seg_397" s="T22">что</ta>
            <ta e="T24" id="Seg_398" s="T23">медведь-ACC</ta>
            <ta e="T25" id="Seg_399" s="T24">NEG</ta>
            <ta e="T26" id="Seg_400" s="T25">убить-PST-1SG.O</ta>
            <ta e="T27" id="Seg_401" s="T26">белка.[NOM]</ta>
            <ta e="T28" id="Seg_402" s="T27">зверь.[NOM]</ta>
            <ta e="T29" id="Seg_403" s="T28">NEG</ta>
            <ta e="T30" id="Seg_404" s="T29">убить-PST-1SG.O</ta>
            <ta e="T31" id="Seg_405" s="T30">и</ta>
            <ta e="T32" id="Seg_406" s="T31">этот-ACC</ta>
            <ta e="T33" id="Seg_407" s="T32">весь</ta>
            <ta e="T34" id="Seg_408" s="T33">видеть.сны-NMLZ-GEN</ta>
            <ta e="T35" id="Seg_409" s="T34">нечто-ILL</ta>
            <ta e="T36" id="Seg_410" s="T35">видеть-IPFV-FRQ-PST-1SG.O</ta>
            <ta e="T37" id="Seg_411" s="T36">он.NOM</ta>
            <ta e="T38" id="Seg_412" s="T37">я.ALL</ta>
            <ta e="T39" id="Seg_413" s="T38">глаз-ALL</ta>
            <ta e="T40" id="Seg_414" s="T39">обмануть-3SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_415" s="T0">n</ta>
            <ta e="T2" id="Seg_416" s="T1">n-n:poss</ta>
            <ta e="T3" id="Seg_417" s="T2">v-v&gt;v-v&gt;n-n:case.poss</ta>
            <ta e="T4" id="Seg_418" s="T3">pers</ta>
            <ta e="T5" id="Seg_419" s="T4">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_420" s="T5">v-v&gt;v-v:pn</ta>
            <ta e="T7" id="Seg_421" s="T6">adj-adj&gt;adv</ta>
            <ta e="T8" id="Seg_422" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_423" s="T8">adv-n-adv:case</ta>
            <ta e="T10" id="Seg_424" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_425" s="T10">v-v:tense-v:mood-v:pn</ta>
            <ta e="T12" id="Seg_426" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_427" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_428" s="T13">v-v:tense-v:mood-v:pn</ta>
            <ta e="T15" id="Seg_429" s="T14">quant</ta>
            <ta e="T16" id="Seg_430" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_431" s="T16">v-v:tense-v:mood-v:pn</ta>
            <ta e="T18" id="Seg_432" s="T17">n-n:poss</ta>
            <ta e="T19" id="Seg_433" s="T18">v-v:ins-v:ins-v:pn</ta>
            <ta e="T20" id="Seg_434" s="T19">pers</ta>
            <ta e="T21" id="Seg_435" s="T20">v-v&gt;adv</ta>
            <ta e="T22" id="Seg_436" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_437" s="T22">interrog</ta>
            <ta e="T24" id="Seg_438" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_439" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_440" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_441" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_442" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_443" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_444" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_445" s="T30">conj</ta>
            <ta e="T32" id="Seg_446" s="T31">dem-n:case</ta>
            <ta e="T33" id="Seg_447" s="T32">quant</ta>
            <ta e="T34" id="Seg_448" s="T33">v-v&gt;n-n:case</ta>
            <ta e="T35" id="Seg_449" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_450" s="T35">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_451" s="T36">pers</ta>
            <ta e="T38" id="Seg_452" s="T37">pers</ta>
            <ta e="T39" id="Seg_453" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_454" s="T39">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_455" s="T0">n</ta>
            <ta e="T2" id="Seg_456" s="T1">n</ta>
            <ta e="T3" id="Seg_457" s="T2">n</ta>
            <ta e="T4" id="Seg_458" s="T3">pers</ta>
            <ta e="T5" id="Seg_459" s="T4">v</ta>
            <ta e="T6" id="Seg_460" s="T5">v</ta>
            <ta e="T7" id="Seg_461" s="T6">adv</ta>
            <ta e="T8" id="Seg_462" s="T7">v</ta>
            <ta e="T9" id="Seg_463" s="T8">adv</ta>
            <ta e="T10" id="Seg_464" s="T9">n</ta>
            <ta e="T11" id="Seg_465" s="T10">v</ta>
            <ta e="T12" id="Seg_466" s="T11">n</ta>
            <ta e="T13" id="Seg_467" s="T12">n</ta>
            <ta e="T14" id="Seg_468" s="T13">v</ta>
            <ta e="T15" id="Seg_469" s="T14">quant</ta>
            <ta e="T16" id="Seg_470" s="T15">n</ta>
            <ta e="T17" id="Seg_471" s="T16">v</ta>
            <ta e="T18" id="Seg_472" s="T17">n</ta>
            <ta e="T19" id="Seg_473" s="T18">v</ta>
            <ta e="T20" id="Seg_474" s="T19">pers</ta>
            <ta e="T21" id="Seg_475" s="T20">adv</ta>
            <ta e="T22" id="Seg_476" s="T21">v</ta>
            <ta e="T23" id="Seg_477" s="T22">interrog</ta>
            <ta e="T24" id="Seg_478" s="T23">n</ta>
            <ta e="T25" id="Seg_479" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_480" s="T25">v</ta>
            <ta e="T27" id="Seg_481" s="T26">n</ta>
            <ta e="T28" id="Seg_482" s="T27">n</ta>
            <ta e="T29" id="Seg_483" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_484" s="T29">v</ta>
            <ta e="T31" id="Seg_485" s="T30">conj</ta>
            <ta e="T32" id="Seg_486" s="T31">dem</ta>
            <ta e="T33" id="Seg_487" s="T32">quant</ta>
            <ta e="T34" id="Seg_488" s="T33">v</ta>
            <ta e="T35" id="Seg_489" s="T34">n</ta>
            <ta e="T36" id="Seg_490" s="T35">v</ta>
            <ta e="T37" id="Seg_491" s="T36">pers</ta>
            <ta e="T38" id="Seg_492" s="T37">pers</ta>
            <ta e="T39" id="Seg_493" s="T38">n</ta>
            <ta e="T40" id="Seg_494" s="T39">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_495" s="T0">s:temp</ta>
            <ta e="T5" id="Seg_496" s="T4">0.3.h:S v:pred</ta>
            <ta e="T6" id="Seg_497" s="T5">0.3.h:S v:pred</ta>
            <ta e="T8" id="Seg_498" s="T7">0.2.h:S v:pred</ta>
            <ta e="T10" id="Seg_499" s="T9">np:O</ta>
            <ta e="T11" id="Seg_500" s="T10">0.2.h:S v:pred</ta>
            <ta e="T13" id="Seg_501" s="T12">np:O</ta>
            <ta e="T14" id="Seg_502" s="T13">0.2.h:S v:pred</ta>
            <ta e="T16" id="Seg_503" s="T15">np:O</ta>
            <ta e="T17" id="Seg_504" s="T16">0.2.h:S v:pred</ta>
            <ta e="T18" id="Seg_505" s="T17">np:S</ta>
            <ta e="T19" id="Seg_506" s="T18">v:pred</ta>
            <ta e="T20" id="Seg_507" s="T19">pro.h:S</ta>
            <ta e="T22" id="Seg_508" s="T21">v:pred</ta>
            <ta e="T24" id="Seg_509" s="T23">np:O</ta>
            <ta e="T26" id="Seg_510" s="T25">0.1.h:S v:pred</ta>
            <ta e="T27" id="Seg_511" s="T26">np:O</ta>
            <ta e="T28" id="Seg_512" s="T27">np:O</ta>
            <ta e="T30" id="Seg_513" s="T29">0.1.h:S v:pred</ta>
            <ta e="T32" id="Seg_514" s="T31">pro:O</ta>
            <ta e="T36" id="Seg_515" s="T35">0.1.h:S v:pred</ta>
            <ta e="T37" id="Seg_516" s="T36">pro.h:S</ta>
            <ta e="T40" id="Seg_517" s="T39">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_518" s="T1">0.1.h:Poss np.h:A</ta>
            <ta e="T5" id="Seg_519" s="T4">0.3.h:A</ta>
            <ta e="T6" id="Seg_520" s="T5">0.3.h:A</ta>
            <ta e="T8" id="Seg_521" s="T7">0.2.h:Th</ta>
            <ta e="T9" id="Seg_522" s="T8">adv:Time</ta>
            <ta e="T10" id="Seg_523" s="T9">np:P</ta>
            <ta e="T11" id="Seg_524" s="T10">0.2.h:A</ta>
            <ta e="T12" id="Seg_525" s="T11">np:Poss</ta>
            <ta e="T13" id="Seg_526" s="T12">np:Th</ta>
            <ta e="T14" id="Seg_527" s="T13">0.2.h:E</ta>
            <ta e="T16" id="Seg_528" s="T15">np:P</ta>
            <ta e="T17" id="Seg_529" s="T16">0.2.h:A</ta>
            <ta e="T18" id="Seg_530" s="T17">np:Th</ta>
            <ta e="T20" id="Seg_531" s="T19">pro.h:Th</ta>
            <ta e="T24" id="Seg_532" s="T23">np:P</ta>
            <ta e="T26" id="Seg_533" s="T25">0.1.h:A</ta>
            <ta e="T27" id="Seg_534" s="T26">np:P</ta>
            <ta e="T28" id="Seg_535" s="T27">np:P</ta>
            <ta e="T30" id="Seg_536" s="T29">0.1.h:A</ta>
            <ta e="T32" id="Seg_537" s="T31">pro:Th</ta>
            <ta e="T36" id="Seg_538" s="T35">0.1.h:E</ta>
            <ta e="T37" id="Seg_539" s="T36">pro.h:A</ta>
            <ta e="T38" id="Seg_540" s="T37">pro.h:R</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T31" id="Seg_541" s="T30">RUS:gram</ta>
            <ta e="T33" id="Seg_542" s="T32">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T8" id="Seg_543" s="T0">Ворожец эвенок, когда ворожил, мне ворожил, говорит: "Ты хорошо жить будешь.</ta>
            <ta e="T17" id="Seg_544" s="T8">В эту зиму ты медведя убьешь, берлогу найдешь, много белок добудешь.</ta>
            <ta e="T19" id="Seg_545" s="T17">Год прошел.</ta>
            <ta e="T22" id="Seg_546" s="T19">Я больной лежал.</ta>
            <ta e="T30" id="Seg_547" s="T22">Никакого медведя я не убил, белок, зверьков не убил.</ta>
            <ta e="T36" id="Seg_548" s="T30">Это я только во сне видел.</ta>
            <ta e="T40" id="Seg_549" s="T36">Он мне в глаза соврал.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T8" id="Seg_550" s="T0">When the Evenki shaman was fortune-telling for me, he said: "You will live well.</ta>
            <ta e="T17" id="Seg_551" s="T8">This winter, you will kill a bear, you’ll find a lair, you’ll kill many squirrels.</ta>
            <ta e="T19" id="Seg_552" s="T17">A year passed.</ta>
            <ta e="T22" id="Seg_553" s="T19">I had been ill.</ta>
            <ta e="T30" id="Seg_554" s="T22">I haven’t killed any bear, haven’t killed squirrels or [other] animals.</ta>
            <ta e="T36" id="Seg_555" s="T30">I only saw all this in my dreams.</ta>
            <ta e="T40" id="Seg_556" s="T36">He lied to me in throat.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T8" id="Seg_557" s="T0">Als mein Ewenke schamanisierte, er schamanisierte mich, sagte er: "Du wirst gut leben.</ta>
            <ta e="T17" id="Seg_558" s="T8">In diesem Winter wirst du einen Bären töten, du wirst die Höhle finden, du wirst viele Eichhörnchen töten."</ta>
            <ta e="T19" id="Seg_559" s="T17">Das Jahr ging vorüber.</ta>
            <ta e="T22" id="Seg_560" s="T19">Ich war krank. </ta>
            <ta e="T30" id="Seg_561" s="T22">Ich tötete keinen einzigen Bären, ich tötete keine Eichhörnchen oder [andere] wilde Tiere.</ta>
            <ta e="T36" id="Seg_562" s="T30">Das alles hatte ich nur geträumt.</ta>
            <ta e="T40" id="Seg_563" s="T36">Er hat mir ins Gesicht gelogen.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T8" id="Seg_564" s="T0">ворожец эвенок (тунгус) когда ворожил мне ворожил говорит (якобы) ты хорошо жить будешь</ta>
            <ta e="T17" id="Seg_565" s="T8">нынешнюю зиму медведя убьешь берлогу найдешь много белок убьешь (добудешь)</ta>
            <ta e="T19" id="Seg_566" s="T17">год прошел</ta>
            <ta e="T22" id="Seg_567" s="T19">я больной лежал</ta>
            <ta e="T30" id="Seg_568" s="T22">никакого медведя не убил белок зверьков не убил</ta>
            <ta e="T36" id="Seg_569" s="T30">это все во сне только видел</ta>
            <ta e="T40" id="Seg_570" s="T36">он мне на глазах соврал</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
