<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KMS_1963_FrozenBear_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KMS_1963_FrozenBear_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">227</ud-information>
            <ud-information attribute-name="# HIAT:w">173</ud-information>
            <ud-information attribute-name="# e">173</ud-information>
            <ud-information attribute-name="# HIAT:u">36</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KMS">
            <abbreviation>KMS</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KMS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T174" id="Seg_0" n="sc" s="T1">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">uːlɣorbädi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">qwärɣə</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_11" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">ässaw</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">qwässɨ</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">matʼtʼöndə</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_23" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">ilɨkkus</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">suːrulʼdʼi</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">maːtqɨn</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_35" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">nʼäjajgus</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_41" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">soŋ</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">qwatkuzɨt</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">nʼäjam</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">amgu</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_56" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_58" n="HIAT:w" s="T14">peːmɨmbɨtäɣən</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">matʼtʼöɣɨn</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">tʼäǯikuzɨt</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">saŋgɨm</ts>
                  <nts id="Seg_68" n="HIAT:ip">,</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">peːkʼkʼem</ts>
                  <nts id="Seg_72" n="HIAT:ip">.</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_75" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_77" n="HIAT:w" s="T19">amgundʼälɨk</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">matʼtʼöɣɨn</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">suːrulʼdʼi</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">maːtqɨn</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_89" n="HIAT:w" s="T23">assə</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_92" n="HIAT:w" s="T24">eːkus</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_96" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">kanaŋdə</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_101" n="HIAT:w" s="T26">tülɨsät</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_104" n="HIAT:w" s="T27">soː</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_107" n="HIAT:w" s="T28">äkkus</ts>
                  <nts id="Seg_108" n="HIAT:ip">.</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_111" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_113" n="HIAT:w" s="T29">okkɨrɨŋ</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_116" n="HIAT:w" s="T30">ässäw</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_119" n="HIAT:w" s="T31">surulʼdʼi</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_122" n="HIAT:w" s="T32">maːtqɨnnä</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_125" n="HIAT:w" s="T33">qwannä</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_128" n="HIAT:w" s="T34">šɨdɨŋ</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_131" n="HIAT:w" s="T35">naːrɨŋ</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_134" n="HIAT:w" s="T36">säŋgɨgu</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_137" n="HIAT:w" s="T37">qoːreɣɨn</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_141" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_143" n="HIAT:w" s="T38">qarimɨɣənno</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_146" n="HIAT:w" s="T39">to</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_149" n="HIAT:w" s="T40">üːtättä</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_152" n="HIAT:w" s="T41">täp</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_155" n="HIAT:w" s="T42">palʼdʼukus</ts>
                  <nts id="Seg_156" n="HIAT:ip">.</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_159" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_161" n="HIAT:w" s="T43">na</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_164" n="HIAT:w" s="T44">dʼel</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_167" n="HIAT:w" s="T45">qwätnɨt</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_170" n="HIAT:w" s="T46">naːr</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_173" n="HIAT:w" s="T47">sarum</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_176" n="HIAT:w" s="T48">sombəlʼe</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_179" n="HIAT:w" s="T49">qwäj</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_182" n="HIAT:w" s="T50">nʼäjam</ts>
                  <nts id="Seg_183" n="HIAT:ip">,</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_186" n="HIAT:w" s="T51">okkə</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_189" n="HIAT:w" s="T52">kazɨm</ts>
                  <nts id="Seg_190" n="HIAT:ip">,</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_193" n="HIAT:w" s="T53">okkɨr</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_196" n="HIAT:w" s="T54">siːm</ts>
                  <nts id="Seg_197" n="HIAT:ip">.</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_200" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_202" n="HIAT:w" s="T55">nadə</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_205" n="HIAT:w" s="T56">ässan</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_208" n="HIAT:w" s="T57">äran</ts>
                  <nts id="Seg_209" n="HIAT:ip">.</nts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_212" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_214" n="HIAT:w" s="T58">nɨwaŋ</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_217" n="HIAT:w" s="T59">üːdumnä</ts>
                  <nts id="Seg_218" n="HIAT:ip">.</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_221" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_223" n="HIAT:w" s="T60">äsäw</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_226" n="HIAT:w" s="T61">uːttäreŋ</ts>
                  <nts id="Seg_227" n="HIAT:ip">.</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_230" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_232" n="HIAT:w" s="T62">pom</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_235" n="HIAT:w" s="T63">ladiŋgnɨt</ts>
                  <nts id="Seg_236" n="HIAT:ip">,</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_239" n="HIAT:w" s="T64">paːtʼännɨt</ts>
                  <nts id="Seg_240" n="HIAT:ip">,</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_243" n="HIAT:w" s="T65">qorem</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_246" n="HIAT:w" s="T66">meːɣɨt</ts>
                  <nts id="Seg_247" n="HIAT:ip">,</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_250" n="HIAT:w" s="T67">pom</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_253" n="HIAT:w" s="T68">tuɣonnɨt</ts>
                  <nts id="Seg_254" n="HIAT:ip">,</nts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_257" n="HIAT:w" s="T69">iːttärnä</ts>
                  <nts id="Seg_258" n="HIAT:ip">.</nts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_261" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_263" n="HIAT:w" s="T70">qwätpɨdi</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_266" n="HIAT:w" s="T71">suːrulamdə</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_269" n="HIAT:w" s="T72">kɨːrennɨt</ts>
                  <nts id="Seg_270" n="HIAT:ip">.</nts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_273" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_275" n="HIAT:w" s="T73">ondə</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_278" n="HIAT:w" s="T74">awräːan</ts>
                  <nts id="Seg_279" n="HIAT:ip">,</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_282" n="HIAT:w" s="T75">kanamdə</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_285" n="HIAT:w" s="T76">abəstɨt</ts>
                  <nts id="Seg_286" n="HIAT:ip">.</nts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_289" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_291" n="HIAT:w" s="T77">patronlamdə</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_294" n="HIAT:w" s="T78">abəstɨt</ts>
                  <nts id="Seg_295" n="HIAT:ip">.</nts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_298" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_300" n="HIAT:w" s="T79">qoptɨm</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_303" n="HIAT:w" s="T80">meːɣɨt</ts>
                  <nts id="Seg_304" n="HIAT:ip">,</nts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_307" n="HIAT:w" s="T81">kučannä</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_310" n="HIAT:w" s="T82">qondegu</ts>
                  <nts id="Seg_311" n="HIAT:ip">.</nts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_314" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_316" n="HIAT:w" s="T83">kungonɨn</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_319" n="HIAT:w" s="T84">qondɨs</ts>
                  <nts id="Seg_320" n="HIAT:ip">.</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_323" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_325" n="HIAT:w" s="T85">kundə</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_328" n="HIAT:w" s="T86">alʼi</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_331" n="HIAT:w" s="T87">mu</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_334" n="HIAT:w" s="T88">assä</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_337" n="HIAT:w" s="T89">küderen</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_340" n="HIAT:w" s="T90">mor</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_343" n="HIAT:w" s="T91">qonǯernɨt</ts>
                  <nts id="Seg_344" n="HIAT:ip">.</nts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_347" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_349" n="HIAT:w" s="T92">ündədit</ts>
                  <nts id="Seg_350" n="HIAT:ip">,</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_353" n="HIAT:w" s="T93">kanaŋ</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_356" n="HIAT:w" s="T94">muːdɨm</ts>
                  <nts id="Seg_357" n="HIAT:ip">.</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_360" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_362" n="HIAT:w" s="T95">kanan</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_365" n="HIAT:w" s="T96">muːtondə</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_368" n="HIAT:w" s="T97">sɨdɨŋ</ts>
                  <nts id="Seg_369" n="HIAT:ip">.</nts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_372" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_374" n="HIAT:w" s="T98">wätʼtʼipčiŋ</ts>
                  <nts id="Seg_375" n="HIAT:ip">,</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_378" n="HIAT:w" s="T99">tʼündə</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_381" n="HIAT:w" s="T100">pom</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_384" n="HIAT:w" s="T101">pännɨt</ts>
                  <nts id="Seg_385" n="HIAT:ip">.</nts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_388" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_390" n="HIAT:w" s="T102">tüdə</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_393" n="HIAT:w" s="T103">poruŋ</ts>
                  <nts id="Seg_394" n="HIAT:ip">,</nts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_397" n="HIAT:w" s="T104">kundar</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_400" n="HIAT:w" s="T105">tʼeːlɨn</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_403" n="HIAT:w" s="T106">ezuŋ</ts>
                  <nts id="Seg_404" n="HIAT:ip">.</nts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_407" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_409" n="HIAT:w" s="T107">tüːləsäm</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_412" n="HIAT:w" s="T108">iːɣət</ts>
                  <nts id="Seg_413" n="HIAT:ip">,</nts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_416" n="HIAT:w" s="T109">puːlʼäj</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_419" n="HIAT:w" s="T110">patronɨn</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_422" n="HIAT:w" s="T111">tüːləsämdə</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_425" n="HIAT:w" s="T112">aːbəstɨt</ts>
                  <nts id="Seg_426" n="HIAT:ip">.</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_429" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_431" n="HIAT:w" s="T113">tʼün</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_434" n="HIAT:w" s="T114">dopqɨnnä</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_437" n="HIAT:w" s="T115">aːrutqö</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_440" n="HIAT:w" s="T116">qwännɨ</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_443" n="HIAT:w" s="T117">i</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_446" n="HIAT:w" s="T118">niːn</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_449" n="HIAT:w" s="T119">nɨŋa</ts>
                  <nts id="Seg_450" n="HIAT:ip">.</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_453" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_455" n="HIAT:w" s="T120">manǯimbat</ts>
                  <nts id="Seg_456" n="HIAT:ip">,</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_459" n="HIAT:w" s="T121">čaǯiŋ</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_462" n="HIAT:w" s="T122">qwärɣə</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_465" n="HIAT:w" s="T123">uːlɣorbədi</ts>
                  <nts id="Seg_466" n="HIAT:ip">.</nts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_469" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_471" n="HIAT:w" s="T124">qwärɣə</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_474" n="HIAT:w" s="T125">üttə</ts>
                  <nts id="Seg_475" n="HIAT:ip">,</nts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_478" n="HIAT:w" s="T126">sɨrondə</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_481" n="HIAT:w" s="T127">pümgujba</ts>
                  <nts id="Seg_482" n="HIAT:ip">,</nts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_485" n="HIAT:w" s="T128">ondə</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_488" n="HIAT:w" s="T129">simdə</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_491" n="HIAT:w" s="T130">tartə</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_494" n="HIAT:w" s="T131">qandeptɨmbat</ts>
                  <nts id="Seg_495" n="HIAT:ip">.</nts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_498" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_500" n="HIAT:w" s="T132">i</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_503" n="HIAT:w" s="T133">tüan</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_506" n="HIAT:w" s="T134">äsännä</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_509" n="HIAT:w" s="T135">tʼün</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_512" n="HIAT:w" s="T136">doptə</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_515" n="HIAT:w" s="T137">amgu</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_518" n="HIAT:w" s="T138">asäw</ts>
                  <nts id="Seg_519" n="HIAT:ip">.</nts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_522" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_524" n="HIAT:w" s="T139">äsäw</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_527" n="HIAT:w" s="T140">tündə</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_530" n="HIAT:w" s="T141">qoːnɨŋ</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_533" n="HIAT:w" s="T142">pännɨt</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_536" n="HIAT:w" s="T143">poːm</ts>
                  <nts id="Seg_537" n="HIAT:ip">.</nts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_540" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_542" n="HIAT:w" s="T144">tü</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_545" n="HIAT:w" s="T145">poruŋ</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_548" n="HIAT:w" s="T146">soŋ</ts>
                  <nts id="Seg_549" n="HIAT:ip">.</nts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_552" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_554" n="HIAT:w" s="T147">qwärɣə</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_557" n="HIAT:w" s="T148">äsännə</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_560" n="HIAT:w" s="T149">moqoɣan</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_563" n="HIAT:w" s="T150">tün</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_566" n="HIAT:w" s="T151">püruŋ</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_569" n="HIAT:w" s="T152">üttɨmbɨlʼe</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_572" n="HIAT:w" s="T153">qwärɣɨn</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_575" n="HIAT:w" s="T154">ulɣoŋ</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_578" n="HIAT:w" s="T155">qandəbədi</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_581" n="HIAT:w" s="T156">tartä</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_584" n="HIAT:w" s="T157">tün</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_587" n="HIAT:w" s="T158">qöɣɨn</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_590" n="HIAT:w" s="T159">tʼüan</ts>
                  <nts id="Seg_591" n="HIAT:ip">.</nts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_594" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_596" n="HIAT:w" s="T160">assäw</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_599" n="HIAT:w" s="T161">tʼüləsän</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_602" n="HIAT:w" s="T162">tʼäǯit</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_605" n="HIAT:w" s="T163">qwärɣɨm</ts>
                  <nts id="Seg_606" n="HIAT:ip">.</nts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T166" id="Seg_609" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_611" n="HIAT:w" s="T164">qwärɣa</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_614" n="HIAT:w" s="T165">čäčolʼdʼiŋ</ts>
                  <nts id="Seg_615" n="HIAT:ip">.</nts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_618" n="HIAT:u" s="T166">
                  <ts e="T167" id="Seg_620" n="HIAT:w" s="T166">nilʼdʼiŋ</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_623" n="HIAT:w" s="T167">okkɨrɨŋ</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_626" n="HIAT:w" s="T168">man</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_629" n="HIAT:w" s="T169">äsäw</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_632" n="HIAT:w" s="T170">aːlɨnbɨzɨt</ts>
                  <nts id="Seg_633" n="HIAT:ip">,</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_636" n="HIAT:w" s="T171">qwätpɨzɨt</ts>
                  <nts id="Seg_637" n="HIAT:ip">,</nts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_640" n="HIAT:w" s="T172">ambɨzɨt</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_643" n="HIAT:w" s="T173">qwärɣɨm</ts>
                  <nts id="Seg_644" n="HIAT:ip">.</nts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T174" id="Seg_646" n="sc" s="T1">
               <ts e="T2" id="Seg_648" n="e" s="T1">uːlɣorbädi </ts>
               <ts e="T3" id="Seg_650" n="e" s="T2">qwärɣə. </ts>
               <ts e="T4" id="Seg_652" n="e" s="T3">ässaw </ts>
               <ts e="T5" id="Seg_654" n="e" s="T4">qwässɨ </ts>
               <ts e="T6" id="Seg_656" n="e" s="T5">matʼtʼöndə. </ts>
               <ts e="T7" id="Seg_658" n="e" s="T6">ilɨkkus </ts>
               <ts e="T8" id="Seg_660" n="e" s="T7">suːrulʼdʼi </ts>
               <ts e="T9" id="Seg_662" n="e" s="T8">maːtqɨn. </ts>
               <ts e="T10" id="Seg_664" n="e" s="T9">nʼäjajgus. </ts>
               <ts e="T11" id="Seg_666" n="e" s="T10">soŋ </ts>
               <ts e="T12" id="Seg_668" n="e" s="T11">qwatkuzɨt </ts>
               <ts e="T13" id="Seg_670" n="e" s="T12">nʼäjam </ts>
               <ts e="T14" id="Seg_672" n="e" s="T13">amgu. </ts>
               <ts e="T15" id="Seg_674" n="e" s="T14">peːmɨmbɨtäɣən </ts>
               <ts e="T16" id="Seg_676" n="e" s="T15">matʼtʼöɣɨn </ts>
               <ts e="T17" id="Seg_678" n="e" s="T16">tʼäǯikuzɨt </ts>
               <ts e="T18" id="Seg_680" n="e" s="T17">saŋgɨm, </ts>
               <ts e="T19" id="Seg_682" n="e" s="T18">peːkʼkʼem. </ts>
               <ts e="T20" id="Seg_684" n="e" s="T19">amgundʼälɨk </ts>
               <ts e="T21" id="Seg_686" n="e" s="T20">matʼtʼöɣɨn </ts>
               <ts e="T22" id="Seg_688" n="e" s="T21">suːrulʼdʼi </ts>
               <ts e="T23" id="Seg_690" n="e" s="T22">maːtqɨn </ts>
               <ts e="T24" id="Seg_692" n="e" s="T23">assə </ts>
               <ts e="T25" id="Seg_694" n="e" s="T24">eːkus. </ts>
               <ts e="T26" id="Seg_696" n="e" s="T25">kanaŋdə </ts>
               <ts e="T27" id="Seg_698" n="e" s="T26">tülɨsät </ts>
               <ts e="T28" id="Seg_700" n="e" s="T27">soː </ts>
               <ts e="T29" id="Seg_702" n="e" s="T28">äkkus. </ts>
               <ts e="T30" id="Seg_704" n="e" s="T29">okkɨrɨŋ </ts>
               <ts e="T31" id="Seg_706" n="e" s="T30">ässäw </ts>
               <ts e="T32" id="Seg_708" n="e" s="T31">surulʼdʼi </ts>
               <ts e="T33" id="Seg_710" n="e" s="T32">maːtqɨnnä </ts>
               <ts e="T34" id="Seg_712" n="e" s="T33">qwannä </ts>
               <ts e="T35" id="Seg_714" n="e" s="T34">šɨdɨŋ </ts>
               <ts e="T36" id="Seg_716" n="e" s="T35">naːrɨŋ </ts>
               <ts e="T37" id="Seg_718" n="e" s="T36">säŋgɨgu </ts>
               <ts e="T38" id="Seg_720" n="e" s="T37">qoːreɣɨn. </ts>
               <ts e="T39" id="Seg_722" n="e" s="T38">qarimɨɣənno </ts>
               <ts e="T40" id="Seg_724" n="e" s="T39">to </ts>
               <ts e="T41" id="Seg_726" n="e" s="T40">üːtättä </ts>
               <ts e="T42" id="Seg_728" n="e" s="T41">täp </ts>
               <ts e="T43" id="Seg_730" n="e" s="T42">palʼdʼukus. </ts>
               <ts e="T44" id="Seg_732" n="e" s="T43">na </ts>
               <ts e="T45" id="Seg_734" n="e" s="T44">dʼel </ts>
               <ts e="T46" id="Seg_736" n="e" s="T45">qwätnɨt </ts>
               <ts e="T47" id="Seg_738" n="e" s="T46">naːr </ts>
               <ts e="T48" id="Seg_740" n="e" s="T47">sarum </ts>
               <ts e="T49" id="Seg_742" n="e" s="T48">sombəlʼe </ts>
               <ts e="T50" id="Seg_744" n="e" s="T49">qwäj </ts>
               <ts e="T51" id="Seg_746" n="e" s="T50">nʼäjam, </ts>
               <ts e="T52" id="Seg_748" n="e" s="T51">okkə </ts>
               <ts e="T53" id="Seg_750" n="e" s="T52">kazɨm, </ts>
               <ts e="T54" id="Seg_752" n="e" s="T53">okkɨr </ts>
               <ts e="T55" id="Seg_754" n="e" s="T54">siːm. </ts>
               <ts e="T56" id="Seg_756" n="e" s="T55">nadə </ts>
               <ts e="T57" id="Seg_758" n="e" s="T56">ässan </ts>
               <ts e="T58" id="Seg_760" n="e" s="T57">äran. </ts>
               <ts e="T59" id="Seg_762" n="e" s="T58">nɨwaŋ </ts>
               <ts e="T60" id="Seg_764" n="e" s="T59">üːdumnä. </ts>
               <ts e="T61" id="Seg_766" n="e" s="T60">äsäw </ts>
               <ts e="T62" id="Seg_768" n="e" s="T61">uːttäreŋ. </ts>
               <ts e="T63" id="Seg_770" n="e" s="T62">pom </ts>
               <ts e="T64" id="Seg_772" n="e" s="T63">ladiŋgnɨt, </ts>
               <ts e="T65" id="Seg_774" n="e" s="T64">paːtʼännɨt, </ts>
               <ts e="T66" id="Seg_776" n="e" s="T65">qorem </ts>
               <ts e="T67" id="Seg_778" n="e" s="T66">meːɣɨt, </ts>
               <ts e="T68" id="Seg_780" n="e" s="T67">pom </ts>
               <ts e="T69" id="Seg_782" n="e" s="T68">tuɣonnɨt, </ts>
               <ts e="T70" id="Seg_784" n="e" s="T69">iːttärnä. </ts>
               <ts e="T71" id="Seg_786" n="e" s="T70">qwätpɨdi </ts>
               <ts e="T72" id="Seg_788" n="e" s="T71">suːrulamdə </ts>
               <ts e="T73" id="Seg_790" n="e" s="T72">kɨːrennɨt. </ts>
               <ts e="T74" id="Seg_792" n="e" s="T73">ondə </ts>
               <ts e="T75" id="Seg_794" n="e" s="T74">awräːan, </ts>
               <ts e="T76" id="Seg_796" n="e" s="T75">kanamdə </ts>
               <ts e="T77" id="Seg_798" n="e" s="T76">abəstɨt. </ts>
               <ts e="T78" id="Seg_800" n="e" s="T77">patronlamdə </ts>
               <ts e="T79" id="Seg_802" n="e" s="T78">abəstɨt. </ts>
               <ts e="T80" id="Seg_804" n="e" s="T79">qoptɨm </ts>
               <ts e="T81" id="Seg_806" n="e" s="T80">meːɣɨt, </ts>
               <ts e="T82" id="Seg_808" n="e" s="T81">kučannä </ts>
               <ts e="T83" id="Seg_810" n="e" s="T82">qondegu. </ts>
               <ts e="T84" id="Seg_812" n="e" s="T83">kungonɨn </ts>
               <ts e="T85" id="Seg_814" n="e" s="T84">qondɨs. </ts>
               <ts e="T86" id="Seg_816" n="e" s="T85">kundə </ts>
               <ts e="T87" id="Seg_818" n="e" s="T86">alʼi </ts>
               <ts e="T88" id="Seg_820" n="e" s="T87">mu </ts>
               <ts e="T89" id="Seg_822" n="e" s="T88">assä </ts>
               <ts e="T90" id="Seg_824" n="e" s="T89">küderen </ts>
               <ts e="T91" id="Seg_826" n="e" s="T90">mor </ts>
               <ts e="T92" id="Seg_828" n="e" s="T91">qonǯernɨt. </ts>
               <ts e="T93" id="Seg_830" n="e" s="T92">ündədit, </ts>
               <ts e="T94" id="Seg_832" n="e" s="T93">kanaŋ </ts>
               <ts e="T95" id="Seg_834" n="e" s="T94">muːdɨm. </ts>
               <ts e="T96" id="Seg_836" n="e" s="T95">kanan </ts>
               <ts e="T97" id="Seg_838" n="e" s="T96">muːtondə </ts>
               <ts e="T98" id="Seg_840" n="e" s="T97">sɨdɨŋ. </ts>
               <ts e="T99" id="Seg_842" n="e" s="T98">wätʼtʼipčiŋ, </ts>
               <ts e="T100" id="Seg_844" n="e" s="T99">tʼündə </ts>
               <ts e="T101" id="Seg_846" n="e" s="T100">pom </ts>
               <ts e="T102" id="Seg_848" n="e" s="T101">pännɨt. </ts>
               <ts e="T103" id="Seg_850" n="e" s="T102">tüdə </ts>
               <ts e="T104" id="Seg_852" n="e" s="T103">poruŋ, </ts>
               <ts e="T105" id="Seg_854" n="e" s="T104">kundar </ts>
               <ts e="T106" id="Seg_856" n="e" s="T105">tʼeːlɨn </ts>
               <ts e="T107" id="Seg_858" n="e" s="T106">ezuŋ. </ts>
               <ts e="T108" id="Seg_860" n="e" s="T107">tüːləsäm </ts>
               <ts e="T109" id="Seg_862" n="e" s="T108">iːɣət, </ts>
               <ts e="T110" id="Seg_864" n="e" s="T109">puːlʼäj </ts>
               <ts e="T111" id="Seg_866" n="e" s="T110">patronɨn </ts>
               <ts e="T112" id="Seg_868" n="e" s="T111">tüːləsämdə </ts>
               <ts e="T113" id="Seg_870" n="e" s="T112">aːbəstɨt. </ts>
               <ts e="T114" id="Seg_872" n="e" s="T113">tʼün </ts>
               <ts e="T115" id="Seg_874" n="e" s="T114">dopqɨnnä </ts>
               <ts e="T116" id="Seg_876" n="e" s="T115">aːrutqö </ts>
               <ts e="T117" id="Seg_878" n="e" s="T116">qwännɨ </ts>
               <ts e="T118" id="Seg_880" n="e" s="T117">i </ts>
               <ts e="T119" id="Seg_882" n="e" s="T118">niːn </ts>
               <ts e="T120" id="Seg_884" n="e" s="T119">nɨŋa. </ts>
               <ts e="T121" id="Seg_886" n="e" s="T120">manǯimbat, </ts>
               <ts e="T122" id="Seg_888" n="e" s="T121">čaǯiŋ </ts>
               <ts e="T123" id="Seg_890" n="e" s="T122">qwärɣə </ts>
               <ts e="T124" id="Seg_892" n="e" s="T123">uːlɣorbədi. </ts>
               <ts e="T125" id="Seg_894" n="e" s="T124">qwärɣə </ts>
               <ts e="T126" id="Seg_896" n="e" s="T125">üttə, </ts>
               <ts e="T127" id="Seg_898" n="e" s="T126">sɨrondə </ts>
               <ts e="T128" id="Seg_900" n="e" s="T127">pümgujba, </ts>
               <ts e="T129" id="Seg_902" n="e" s="T128">ondə </ts>
               <ts e="T130" id="Seg_904" n="e" s="T129">simdə </ts>
               <ts e="T131" id="Seg_906" n="e" s="T130">tartə </ts>
               <ts e="T132" id="Seg_908" n="e" s="T131">qandeptɨmbat. </ts>
               <ts e="T133" id="Seg_910" n="e" s="T132">i </ts>
               <ts e="T134" id="Seg_912" n="e" s="T133">tüan </ts>
               <ts e="T135" id="Seg_914" n="e" s="T134">äsännä </ts>
               <ts e="T136" id="Seg_916" n="e" s="T135">tʼün </ts>
               <ts e="T137" id="Seg_918" n="e" s="T136">doptə </ts>
               <ts e="T138" id="Seg_920" n="e" s="T137">amgu </ts>
               <ts e="T139" id="Seg_922" n="e" s="T138">asäw. </ts>
               <ts e="T140" id="Seg_924" n="e" s="T139">äsäw </ts>
               <ts e="T141" id="Seg_926" n="e" s="T140">tündə </ts>
               <ts e="T142" id="Seg_928" n="e" s="T141">qoːnɨŋ </ts>
               <ts e="T143" id="Seg_930" n="e" s="T142">pännɨt </ts>
               <ts e="T144" id="Seg_932" n="e" s="T143">poːm. </ts>
               <ts e="T145" id="Seg_934" n="e" s="T144">tü </ts>
               <ts e="T146" id="Seg_936" n="e" s="T145">poruŋ </ts>
               <ts e="T147" id="Seg_938" n="e" s="T146">soŋ. </ts>
               <ts e="T148" id="Seg_940" n="e" s="T147">qwärɣə </ts>
               <ts e="T149" id="Seg_942" n="e" s="T148">äsännə </ts>
               <ts e="T150" id="Seg_944" n="e" s="T149">moqoɣan </ts>
               <ts e="T151" id="Seg_946" n="e" s="T150">tün </ts>
               <ts e="T152" id="Seg_948" n="e" s="T151">püruŋ </ts>
               <ts e="T153" id="Seg_950" n="e" s="T152">üttɨmbɨlʼe </ts>
               <ts e="T154" id="Seg_952" n="e" s="T153">qwärɣɨn </ts>
               <ts e="T155" id="Seg_954" n="e" s="T154">ulɣoŋ </ts>
               <ts e="T156" id="Seg_956" n="e" s="T155">qandəbədi </ts>
               <ts e="T157" id="Seg_958" n="e" s="T156">tartä </ts>
               <ts e="T158" id="Seg_960" n="e" s="T157">tün </ts>
               <ts e="T159" id="Seg_962" n="e" s="T158">qöɣɨn </ts>
               <ts e="T160" id="Seg_964" n="e" s="T159">tʼüan. </ts>
               <ts e="T161" id="Seg_966" n="e" s="T160">assäw </ts>
               <ts e="T162" id="Seg_968" n="e" s="T161">tʼüləsän </ts>
               <ts e="T163" id="Seg_970" n="e" s="T162">tʼäǯit </ts>
               <ts e="T164" id="Seg_972" n="e" s="T163">qwärɣɨm. </ts>
               <ts e="T165" id="Seg_974" n="e" s="T164">qwärɣa </ts>
               <ts e="T166" id="Seg_976" n="e" s="T165">čäčolʼdʼiŋ. </ts>
               <ts e="T167" id="Seg_978" n="e" s="T166">nilʼdʼiŋ </ts>
               <ts e="T168" id="Seg_980" n="e" s="T167">okkɨrɨŋ </ts>
               <ts e="T169" id="Seg_982" n="e" s="T168">man </ts>
               <ts e="T170" id="Seg_984" n="e" s="T169">äsäw </ts>
               <ts e="T171" id="Seg_986" n="e" s="T170">aːlɨnbɨzɨt, </ts>
               <ts e="T172" id="Seg_988" n="e" s="T171">qwätpɨzɨt, </ts>
               <ts e="T173" id="Seg_990" n="e" s="T172">ambɨzɨt </ts>
               <ts e="T174" id="Seg_992" n="e" s="T173">qwärɣɨm. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_993" s="T1">KMS_1963_FrozenBear_nar.001 (001.001)</ta>
            <ta e="T6" id="Seg_994" s="T3">KMS_1963_FrozenBear_nar.002 (001.002)</ta>
            <ta e="T9" id="Seg_995" s="T6">KMS_1963_FrozenBear_nar.003 (001.003)</ta>
            <ta e="T10" id="Seg_996" s="T9">KMS_1963_FrozenBear_nar.004 (001.004)</ta>
            <ta e="T14" id="Seg_997" s="T10">KMS_1963_FrozenBear_nar.005 (001.005)</ta>
            <ta e="T19" id="Seg_998" s="T14">KMS_1963_FrozenBear_nar.006 (001.006)</ta>
            <ta e="T25" id="Seg_999" s="T19">KMS_1963_FrozenBear_nar.007 (001.007)</ta>
            <ta e="T29" id="Seg_1000" s="T25">KMS_1963_FrozenBear_nar.008 (001.008)</ta>
            <ta e="T38" id="Seg_1001" s="T29">KMS_1963_FrozenBear_nar.009 (001.009)</ta>
            <ta e="T43" id="Seg_1002" s="T38">KMS_1963_FrozenBear_nar.010 (001.010)</ta>
            <ta e="T55" id="Seg_1003" s="T43">KMS_1963_FrozenBear_nar.011 (001.011)</ta>
            <ta e="T58" id="Seg_1004" s="T55">KMS_1963_FrozenBear_nar.012 (001.012)</ta>
            <ta e="T60" id="Seg_1005" s="T58">KMS_1963_FrozenBear_nar.013 (001.013)</ta>
            <ta e="T62" id="Seg_1006" s="T60">KMS_1963_FrozenBear_nar.014 (001.014)</ta>
            <ta e="T70" id="Seg_1007" s="T62">KMS_1963_FrozenBear_nar.015 (001.015)</ta>
            <ta e="T73" id="Seg_1008" s="T70">KMS_1963_FrozenBear_nar.016 (001.016)</ta>
            <ta e="T77" id="Seg_1009" s="T73">KMS_1963_FrozenBear_nar.017 (001.017)</ta>
            <ta e="T79" id="Seg_1010" s="T77">KMS_1963_FrozenBear_nar.018 (001.018)</ta>
            <ta e="T83" id="Seg_1011" s="T79">KMS_1963_FrozenBear_nar.019 (001.019)</ta>
            <ta e="T85" id="Seg_1012" s="T83">KMS_1963_FrozenBear_nar.020 (001.020)</ta>
            <ta e="T92" id="Seg_1013" s="T85">KMS_1963_FrozenBear_nar.021 (001.021)</ta>
            <ta e="T95" id="Seg_1014" s="T92">KMS_1963_FrozenBear_nar.022 (001.022)</ta>
            <ta e="T98" id="Seg_1015" s="T95">KMS_1963_FrozenBear_nar.023 (001.023)</ta>
            <ta e="T102" id="Seg_1016" s="T98">KMS_1963_FrozenBear_nar.024 (001.024)</ta>
            <ta e="T107" id="Seg_1017" s="T102">KMS_1963_FrozenBear_nar.025 (001.025)</ta>
            <ta e="T113" id="Seg_1018" s="T107">KMS_1963_FrozenBear_nar.026 (001.026)</ta>
            <ta e="T120" id="Seg_1019" s="T113">KMS_1963_FrozenBear_nar.027 (001.027)</ta>
            <ta e="T124" id="Seg_1020" s="T120">KMS_1963_FrozenBear_nar.028 (001.028)</ta>
            <ta e="T132" id="Seg_1021" s="T124">KMS_1963_FrozenBear_nar.029 (001.029)</ta>
            <ta e="T139" id="Seg_1022" s="T132">KMS_1963_FrozenBear_nar.030 (001.030)</ta>
            <ta e="T144" id="Seg_1023" s="T139">KMS_1963_FrozenBear_nar.031 (001.031)</ta>
            <ta e="T147" id="Seg_1024" s="T144">KMS_1963_FrozenBear_nar.032 (001.032)</ta>
            <ta e="T160" id="Seg_1025" s="T147">KMS_1963_FrozenBear_nar.033 (001.033)</ta>
            <ta e="T164" id="Seg_1026" s="T160">KMS_1963_FrozenBear_nar.034 (001.034)</ta>
            <ta e="T166" id="Seg_1027" s="T164">KMS_1963_FrozenBear_nar.035 (001.035)</ta>
            <ta e="T174" id="Seg_1028" s="T166">KMS_1963_FrozenBear_nar.036 (001.036)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_1029" s="T1">′ӯлɣорбӓ′ди ′kwӓрɣъ.</ta>
            <ta e="T6" id="Seg_1030" s="T3">ӓ′ссаw ′kwӓссы ма′тʼтʼӧндъ.</ta>
            <ta e="T9" id="Seg_1031" s="T6">илы′ккус ′сӯрулʼдʼи ′ма̄тkын.</ta>
            <ta e="T10" id="Seg_1032" s="T9">′нʼӓjайгус.</ta>
            <ta e="T14" id="Seg_1033" s="T10">соң kwатку′зыт ′нʼӓjам ам′гу.</ta>
            <ta e="T19" id="Seg_1034" s="T14">‵пе̄мымбы′тӓɣън ма′тʼтʼӧɣын ‵тʼӓджику′зыт саң′гым, ′пе̄кʼ(кʼ)ем.</ta>
            <ta e="T25" id="Seg_1035" s="T19">‵амгун′дʼӓлык ма′тʼтʼӧɣын ′сӯрулʼдʼи ′ма̄тkын ′ассъ ′е̄кус.</ta>
            <ta e="T29" id="Seg_1036" s="T25">ка′наңдъ ′тӱлысӓт со̄ ′ӓккус.</ta>
            <ta e="T38" id="Seg_1037" s="T29">оккы′рың ӓс′сӓw ′сурулʼдʼи ′ма̄тkыннӓ kwа′ннӓ(ъ) ′шыдың ′на̄рың ′сӓңгыгу kо̄′реɣын.</ta>
            <ta e="T43" id="Seg_1038" s="T38">kа′римыɣънно то ӱ̄′тӓттӓ ′тӓп палʼдʼу′кус.</ta>
            <ta e="T55" id="Seg_1039" s="T43">на ′дʼел kwӓт′ныт ′на̄р ′сарум сомбъ′лʼе ′kwӓй ′нʼӓjам, ′оккъ ′казым, ′оккыр сӣм.</ta>
            <ta e="T58" id="Seg_1040" s="T55">′надъ ′ӓссан ӓ′ран.</ta>
            <ta e="T60" id="Seg_1041" s="T58">ны′ваң ′ӱ̄дум‵нӓ.</ta>
            <ta e="T62" id="Seg_1042" s="T60">ӓ′сӓw ‵ӯттӓ′рең.</ta>
            <ta e="T70" id="Seg_1043" s="T62">пом ′ладиңгныт, па̄′тʼӓнныт, kо′рем ′ме̄ɣыт, ′пом ′туɣонныт, ӣттӓр(ъ)′нӓ.</ta>
            <ta e="T73" id="Seg_1044" s="T70">kwӓтпы′ди ′сӯруламдъ кы̄(и)′ренныт.</ta>
            <ta e="T77" id="Seg_1045" s="T73">ондъ ав′рӓ̄(е̄)ан, ка′намдъ ‵абъс′тыт.</ta>
            <ta e="T79" id="Seg_1046" s="T77">пат′ронламдъ ‵абъс′тыт.</ta>
            <ta e="T83" id="Seg_1047" s="T79">′kоптым ′ме̄ɣыт, ку′тшаннӓ kонде′гу.</ta>
            <ta e="T85" id="Seg_1048" s="T83">кун′гонын kон′дыс.</ta>
            <ta e="T92" id="Seg_1049" s="T85">′кундъ ′алʼи му ′ассӓ(ъ) ′кӱдерен мор kонджер′ныт.</ta>
            <ta e="T95" id="Seg_1050" s="T92">ӱндъ′дит, ка′наң ′мӯды(ъ)м.</ta>
            <ta e="T98" id="Seg_1051" s="T95">ка′нан ′мӯтондъ ′сыдың.</ta>
            <ta e="T102" id="Seg_1052" s="T98">вӓтʼтʼип′тшиң, ′тʼӱндъ пом ′пӓнныт.</ta>
            <ta e="T107" id="Seg_1053" s="T102">′тӱдъ по′руң, кун′дар ′тʼе̄лын е′зуң.</ta>
            <ta e="T113" id="Seg_1054" s="T107">′тӱ̄лъсӓм ′ӣɣът, ′пӯлʼӓй пат′ронын ′тӱ̄лъсӓмдъ а̄бъс′тыт.</ta>
            <ta e="T120" id="Seg_1055" s="T113">′тʼӱн ′допkын(н)ӓ ′а̄рутkӧ kwӓ′нны и ′нӣн ны′ңа.</ta>
            <ta e="T124" id="Seg_1056" s="T120">манджим′бат, ′тшаджиң ′kwӓрɣъ ′ӯлɣорбъди.</ta>
            <ta e="T132" id="Seg_1057" s="T124">′kwӓрɣъ ӱт′тъ, сы′рондъ пӱмгуй′ба, ′ондъ ′симдъ тар′тъ kан′дептымбат.</ta>
            <ta e="T139" id="Seg_1058" s="T132">и ′тӱан ӓ′сӓннӓ тʼӱн ′доптъ ам′гу а′сӓw.</ta>
            <ta e="T144" id="Seg_1059" s="T139">ӓ′сӓw ′тӱндъ ′kо̄ның ′пӓнныт по̄м.</ta>
            <ta e="T147" id="Seg_1060" s="T144">тӱ по′руң соң.</ta>
            <ta e="T160" id="Seg_1061" s="T147">′kwӓрɣъ ӓ′сӓннъ мо′kоɣа(ъ)н тӱн ′пӱруң ′ӱттымбылʼе ′kwӓрɣын ′улɣоң ‵kандъбъ′ди тар′тӓ тӱн ′kӧɣын тʼӱ′ан.</ta>
            <ta e="T164" id="Seg_1062" s="T160">а′ссӓw ′тʼӱлъсӓн ‵тʼӓджит ′kwӓрɣым.</ta>
            <ta e="T166" id="Seg_1063" s="T164">′kwӓрɣа тшӓ′тшолʼдʼиң.</ta>
            <ta e="T174" id="Seg_1064" s="T166">нилʼ′дʼиң оккы′рың ман ӓ′сӓw ′а̄лынбызыт, kwӓтпы′зыт, ‵амбы′зыт ′kwӓрɣым.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_1065" s="T1">uːlɣorbädi qwärɣə.</ta>
            <ta e="T6" id="Seg_1066" s="T3">ässaw qwässɨ matʼtʼöndə.</ta>
            <ta e="T9" id="Seg_1067" s="T6">ilɨkkus suːrulʼdʼi maːtqɨn.</ta>
            <ta e="T10" id="Seg_1068" s="T9">nʼäjajgus.</ta>
            <ta e="T14" id="Seg_1069" s="T10">soŋ qwatkuzɨt nʼäjam amgu.</ta>
            <ta e="T19" id="Seg_1070" s="T14">peːmɨmbɨtäɣən matʼtʼöɣɨn tʼäǯikuzɨt saŋgɨm, peːkʼ(kʼ)em.</ta>
            <ta e="T25" id="Seg_1071" s="T19">amgundʼälɨk matʼtʼöɣɨn suːrulʼdʼi maːtqɨn assə eːkus.</ta>
            <ta e="T29" id="Seg_1072" s="T25">kanaŋdə tülɨsät soː äkkus.</ta>
            <ta e="T38" id="Seg_1073" s="T29">okkɨrɨŋ ässäw surulʼdʼi maːtqɨnnä qwannä(ə) šɨdɨŋ naːrɨŋ säŋgɨgu qoːreɣɨn.</ta>
            <ta e="T43" id="Seg_1074" s="T38">qarimɨɣənno to üːtättä täp palʼdʼukus.</ta>
            <ta e="T55" id="Seg_1075" s="T43">na dʼel qwätnɨt naːr sarum sombəlʼe qwäj nʼäjam, okkə kazɨm, okkɨr siːm.</ta>
            <ta e="T58" id="Seg_1076" s="T55">nadə ässan äran.</ta>
            <ta e="T60" id="Seg_1077" s="T58">nɨvaŋ üːdumnä.</ta>
            <ta e="T62" id="Seg_1078" s="T60">äsäw uːttäreŋ.</ta>
            <ta e="T70" id="Seg_1079" s="T62">pom ladiŋgnɨt, paːtʼännɨt, qorem meːɣɨt, pom tuɣonnɨt, iːttär(ə)nä.</ta>
            <ta e="T73" id="Seg_1080" s="T70">qwätpɨdi suːrulamdə kɨː(i)rennɨt.</ta>
            <ta e="T77" id="Seg_1081" s="T73">ondə avräː(eː)an, kanamdə abəstɨt.</ta>
            <ta e="T79" id="Seg_1082" s="T77">patronlamdə abəstɨt.</ta>
            <ta e="T83" id="Seg_1083" s="T79">qoptɨm meːɣɨt, kutšannä qondegu.</ta>
            <ta e="T85" id="Seg_1084" s="T83">kungonɨn qondɨs.</ta>
            <ta e="T92" id="Seg_1085" s="T85">kundə alʼi mu assä(ə) küderen mor qonǯernɨt.</ta>
            <ta e="T95" id="Seg_1086" s="T92">ündədit, kanaŋ muːdɨ(ə)m.</ta>
            <ta e="T98" id="Seg_1087" s="T95">kanan muːtondə sɨdɨŋ.</ta>
            <ta e="T102" id="Seg_1088" s="T98">vätʼtʼiptšiŋ, tʼündə pom pännɨt.</ta>
            <ta e="T107" id="Seg_1089" s="T102">tüdə poruŋ, kundar tʼeːlɨn ezuŋ.</ta>
            <ta e="T113" id="Seg_1090" s="T107">tüːləsäm iːɣət, puːlʼäj patronɨn tüːləsämdə aːbəstɨt.</ta>
            <ta e="T120" id="Seg_1091" s="T113">tʼün dopqɨn(n)ä aːrutqö qwännɨ i niːn nɨŋa.</ta>
            <ta e="T124" id="Seg_1092" s="T120">manǯimbat, tšaǯiŋ qwärɣə uːlɣorbədi.</ta>
            <ta e="T132" id="Seg_1093" s="T124">qwärɣə üttə, sɨrondə pümgujba, ondə simdə tartə qandeptɨmbat.</ta>
            <ta e="T139" id="Seg_1094" s="T132">i tüan äsännä tʼün doptə amgu asäw.</ta>
            <ta e="T144" id="Seg_1095" s="T139">äsäw tündə qoːnɨŋ pännɨt poːm.</ta>
            <ta e="T147" id="Seg_1096" s="T144">tü poruŋ soŋ.</ta>
            <ta e="T160" id="Seg_1097" s="T147">qwärɣə äsännə moqoɣa(ə)n tün püruŋ üttɨmbɨlʼe qwärɣɨn ulɣoŋ qandəbədi tartä tün qöɣɨn tʼüan.</ta>
            <ta e="T164" id="Seg_1098" s="T160">assäw tʼüləsän tʼäǯit qwärɣɨm.</ta>
            <ta e="T166" id="Seg_1099" s="T164">qwärɣa tšätšolʼdʼiŋ.</ta>
            <ta e="T174" id="Seg_1100" s="T166">nilʼdʼiŋ okkɨrɨŋ man äsäw aːlɨnbɨzɨt, qwätpɨzɨt, ambɨzɨt qwärɣɨm.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_1101" s="T1">uːlɣorbädi qwärɣə. </ta>
            <ta e="T6" id="Seg_1102" s="T3">ässaw qwässɨ matʼtʼöndə. </ta>
            <ta e="T9" id="Seg_1103" s="T6">ilɨkkus suːrulʼdʼi maːtqɨn. </ta>
            <ta e="T10" id="Seg_1104" s="T9">nʼäjajgus. </ta>
            <ta e="T14" id="Seg_1105" s="T10">soŋ qwatkuzɨt nʼäjam amgu. </ta>
            <ta e="T19" id="Seg_1106" s="T14">peːmɨmbɨtäɣən matʼtʼöɣɨn tʼäǯikuzɨt saŋgɨm, peːkʼkʼem. </ta>
            <ta e="T25" id="Seg_1107" s="T19">amgundʼälɨk matʼtʼöɣɨn suːrulʼdʼi maːtqɨn assə eːkus. </ta>
            <ta e="T29" id="Seg_1108" s="T25">kanaŋdə tülɨsät soː äkkus. </ta>
            <ta e="T38" id="Seg_1109" s="T29">okkɨrɨŋ ässäw surulʼdʼi maːtqɨnnä qwannä šɨdɨŋ naːrɨŋ säŋgɨgu qoːreɣɨn. </ta>
            <ta e="T43" id="Seg_1110" s="T38">qarimɨɣənno to üːtättä täp palʼdʼukus. </ta>
            <ta e="T55" id="Seg_1111" s="T43">na dʼel qwätnɨt naːr sarum sombəlʼe qwäj nʼäjam, okkə kazɨm, okkɨr siːm. </ta>
            <ta e="T58" id="Seg_1112" s="T55">nadə ässan äran. </ta>
            <ta e="T60" id="Seg_1113" s="T58">nɨwaŋ üːdumnä. </ta>
            <ta e="T62" id="Seg_1114" s="T60">äsäw uːttäreŋ. </ta>
            <ta e="T70" id="Seg_1115" s="T62">pom ladiŋgnɨt, paːtʼännɨt, qorem meːɣɨt, pom tuɣonnɨt, iːttärnä. </ta>
            <ta e="T73" id="Seg_1116" s="T70">qwätpɨdi suːrulamdə kɨːrennɨt. </ta>
            <ta e="T77" id="Seg_1117" s="T73">ondə awräːan, kanamdə abəstɨt. </ta>
            <ta e="T79" id="Seg_1118" s="T77">patronlamdə abəstɨt. </ta>
            <ta e="T83" id="Seg_1119" s="T79">qoptɨm meːɣɨt, kučannä qondegu. </ta>
            <ta e="T85" id="Seg_1120" s="T83">kungonɨn qondɨs. </ta>
            <ta e="T92" id="Seg_1121" s="T85">kundə alʼi mu assä küderen mor qonǯernɨt. </ta>
            <ta e="T95" id="Seg_1122" s="T92">ündədit, kanaŋ muːdɨm. </ta>
            <ta e="T98" id="Seg_1123" s="T95">kanan muːtondə sɨdɨŋ. </ta>
            <ta e="T102" id="Seg_1124" s="T98">wätʼtʼipčiŋ, tʼündə pom pännɨt. </ta>
            <ta e="T107" id="Seg_1125" s="T102">tüdə poruŋ, kundar tʼeːlɨn ezuŋ. </ta>
            <ta e="T113" id="Seg_1126" s="T107">tüːləsäm iːɣət, puːlʼäj patronɨn tüːləsämdə aːbəstɨt. </ta>
            <ta e="T120" id="Seg_1127" s="T113">tʼün dopqɨnnä aːrutqö qwännɨ i niːn nɨŋa. </ta>
            <ta e="T124" id="Seg_1128" s="T120">manǯimbat, čaǯiŋ qwärɣə uːlɣorbədi. </ta>
            <ta e="T132" id="Seg_1129" s="T124">qwärɣə üttə, sɨrondə pümgujba, ondə simdə tartə qandeptɨmbat. </ta>
            <ta e="T139" id="Seg_1130" s="T132">i tüan äsännä tʼün doptə amgu asäw. </ta>
            <ta e="T144" id="Seg_1131" s="T139">äsäw tündə qoːnɨŋ pännɨt poːm. </ta>
            <ta e="T147" id="Seg_1132" s="T144">tü poruŋ soŋ. </ta>
            <ta e="T160" id="Seg_1133" s="T147">qwärɣə äsännə moqoɣan tün püruŋ üttɨmbɨlʼe qwärɣɨn ulɣoŋ qandəbədi tartä tün qöɣɨn tʼüan. </ta>
            <ta e="T164" id="Seg_1134" s="T160">assäw tʼüləsän tʼäǯit qwärɣɨm. </ta>
            <ta e="T166" id="Seg_1135" s="T164">qwärɣa čäčolʼdʼiŋ. </ta>
            <ta e="T174" id="Seg_1136" s="T166">nilʼdʼiŋ okkɨrɨŋ man äsäw aːlɨnbɨzɨt, qwätpɨzɨt, ambɨzɨt qwärɣɨm. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_1137" s="T1">uːlɣo-r-bädi</ta>
            <ta e="T3" id="Seg_1138" s="T2">qwärɣə</ta>
            <ta e="T4" id="Seg_1139" s="T3">ässa-w</ta>
            <ta e="T5" id="Seg_1140" s="T4">qwäs-sɨ</ta>
            <ta e="T6" id="Seg_1141" s="T5">matʼtʼö-ndə</ta>
            <ta e="T7" id="Seg_1142" s="T6">illɨ-ku-s</ta>
            <ta e="T8" id="Seg_1143" s="T7">suːru-lʼdʼi</ta>
            <ta e="T9" id="Seg_1144" s="T8">maːt-qən</ta>
            <ta e="T10" id="Seg_1145" s="T9">nʼäja-j-gu-s</ta>
            <ta e="T11" id="Seg_1146" s="T10">so-ŋ</ta>
            <ta e="T12" id="Seg_1147" s="T11">qwat-ku-zɨ-t</ta>
            <ta e="T13" id="Seg_1148" s="T12">nʼäja-m</ta>
            <ta e="T14" id="Seg_1149" s="T13">am-ɨ-gu</ta>
            <ta e="T15" id="Seg_1150" s="T14">peːmɨ-mbɨ-tä-ɣən</ta>
            <ta e="T16" id="Seg_1151" s="T15">matʼtʼö-ɣɨn</ta>
            <ta e="T17" id="Seg_1152" s="T16">tʼäǯi-ku-zɨ-t</ta>
            <ta e="T18" id="Seg_1153" s="T17">saŋgɨ-m</ta>
            <ta e="T19" id="Seg_1154" s="T18">peːkʼkʼe-m</ta>
            <ta e="T20" id="Seg_1155" s="T19">am-gu-n-dʼälɨ-k</ta>
            <ta e="T21" id="Seg_1156" s="T20">matʼtʼö-ɣɨn</ta>
            <ta e="T22" id="Seg_1157" s="T21">suːru-lʼdʼi</ta>
            <ta e="T23" id="Seg_1158" s="T22">maːt-qən</ta>
            <ta e="T24" id="Seg_1159" s="T23">assə</ta>
            <ta e="T25" id="Seg_1160" s="T24">eː-ku-s</ta>
            <ta e="T26" id="Seg_1161" s="T25">kanaŋ-də</ta>
            <ta e="T27" id="Seg_1162" s="T26">tülɨsä-tɨ</ta>
            <ta e="T28" id="Seg_1163" s="T27">soː</ta>
            <ta e="T29" id="Seg_1164" s="T28">ä-ku-s</ta>
            <ta e="T30" id="Seg_1165" s="T29">okkɨr-ɨ-ŋ</ta>
            <ta e="T31" id="Seg_1166" s="T30">ässä-w</ta>
            <ta e="T32" id="Seg_1167" s="T31">suru-lʼdʼi</ta>
            <ta e="T33" id="Seg_1168" s="T32">maːt-qɨnnä</ta>
            <ta e="T34" id="Seg_1169" s="T33">qwan-nä</ta>
            <ta e="T35" id="Seg_1170" s="T34">šɨdɨ-ŋ</ta>
            <ta e="T36" id="Seg_1171" s="T35">naːr-ɨ-ŋ</ta>
            <ta e="T37" id="Seg_1172" s="T36">säŋgɨ-gu</ta>
            <ta e="T38" id="Seg_1173" s="T37">qoːre-ɣɨn</ta>
            <ta e="T39" id="Seg_1174" s="T38">qari-mɨ-ɣənno</ta>
            <ta e="T40" id="Seg_1175" s="T39">to</ta>
            <ta e="T41" id="Seg_1176" s="T40">üːtä-ttä</ta>
            <ta e="T42" id="Seg_1177" s="T41">täp</ta>
            <ta e="T43" id="Seg_1178" s="T42">palʼdʼu-ku-s</ta>
            <ta e="T44" id="Seg_1179" s="T43">na</ta>
            <ta e="T45" id="Seg_1180" s="T44">dʼel</ta>
            <ta e="T46" id="Seg_1181" s="T45">qwät-nɨ-t</ta>
            <ta e="T47" id="Seg_1182" s="T46">naːr</ta>
            <ta e="T48" id="Seg_1183" s="T47">sarum</ta>
            <ta e="T49" id="Seg_1184" s="T48">sombəlʼe</ta>
            <ta e="T50" id="Seg_1185" s="T49">qwäj</ta>
            <ta e="T51" id="Seg_1186" s="T50">nʼäja-m</ta>
            <ta e="T52" id="Seg_1187" s="T51">okkə</ta>
            <ta e="T53" id="Seg_1188" s="T52">kaz-ɨ-m</ta>
            <ta e="T54" id="Seg_1189" s="T53">okkɨr</ta>
            <ta e="T55" id="Seg_1190" s="T54">siː-m</ta>
            <ta e="T56" id="Seg_1191" s="T55">nadə</ta>
            <ta e="T57" id="Seg_1192" s="T56">ä-ssa-ŋ</ta>
            <ta e="T58" id="Seg_1193" s="T57">ära-n</ta>
            <ta e="T59" id="Seg_1194" s="T58">nɨwa-ŋ</ta>
            <ta e="T60" id="Seg_1195" s="T59">üːdu-m-nä</ta>
            <ta e="T61" id="Seg_1196" s="T60">äsä-w</ta>
            <ta e="T62" id="Seg_1197" s="T61">uːttäre-ŋ</ta>
            <ta e="T63" id="Seg_1198" s="T62">po-m</ta>
            <ta e="T64" id="Seg_1199" s="T63">ladiŋg-nɨ-t</ta>
            <ta e="T65" id="Seg_1200" s="T64">paːtʼän-nɨ-t</ta>
            <ta e="T66" id="Seg_1201" s="T65">qore-m</ta>
            <ta e="T67" id="Seg_1202" s="T66">meː-ɣɨ-t</ta>
            <ta e="T68" id="Seg_1203" s="T67">po-m</ta>
            <ta e="T69" id="Seg_1204" s="T68">tuɣon-nɨ-t</ta>
            <ta e="T70" id="Seg_1205" s="T69">iːttä-r-nä</ta>
            <ta e="T71" id="Seg_1206" s="T70">qwät-pɨdi</ta>
            <ta e="T72" id="Seg_1207" s="T71">suːru-la-m-də</ta>
            <ta e="T73" id="Seg_1208" s="T72">kɨːr-e-n-nɨ-t</ta>
            <ta e="T74" id="Seg_1209" s="T73">ondə</ta>
            <ta e="T75" id="Seg_1210" s="T74">aw-r-äː-a-n</ta>
            <ta e="T76" id="Seg_1211" s="T75">kana-m-də</ta>
            <ta e="T77" id="Seg_1212" s="T76">abəstɨ-t</ta>
            <ta e="T78" id="Seg_1213" s="T77">patron-la-m-də</ta>
            <ta e="T79" id="Seg_1214" s="T78">abəstɨ-t</ta>
            <ta e="T80" id="Seg_1215" s="T79">qoptɨ-m</ta>
            <ta e="T81" id="Seg_1216" s="T80">meː-ɣɨ-t</ta>
            <ta e="T82" id="Seg_1217" s="T81">kuča-nnä</ta>
            <ta e="T83" id="Seg_1218" s="T82">qonde-gu</ta>
            <ta e="T84" id="Seg_1219" s="T83">kun-gonɨn</ta>
            <ta e="T85" id="Seg_1220" s="T84">qondɨ-s</ta>
            <ta e="T86" id="Seg_1221" s="T85">kundə</ta>
            <ta e="T87" id="Seg_1222" s="T86">alʼi</ta>
            <ta e="T88" id="Seg_1223" s="T87">mu</ta>
            <ta e="T89" id="Seg_1224" s="T88">assä</ta>
            <ta e="T90" id="Seg_1225" s="T89">küdere-n</ta>
            <ta e="T91" id="Seg_1226" s="T90">mor</ta>
            <ta e="T92" id="Seg_1227" s="T91">qo-nǯe-r-nɨ-t</ta>
            <ta e="T93" id="Seg_1228" s="T92">ündə-dit</ta>
            <ta e="T94" id="Seg_1229" s="T93">kanaŋ</ta>
            <ta e="T95" id="Seg_1230" s="T94">muːdɨ-m</ta>
            <ta e="T96" id="Seg_1231" s="T95">kana-n</ta>
            <ta e="T97" id="Seg_1232" s="T96">muːto-ndə</ta>
            <ta e="T98" id="Seg_1233" s="T97">sɨdɨ-ŋ</ta>
            <ta e="T99" id="Seg_1234" s="T98">wätʼtʼi-p-či-ŋ</ta>
            <ta e="T100" id="Seg_1235" s="T99">tʼü-ndə</ta>
            <ta e="T101" id="Seg_1236" s="T100">po-m</ta>
            <ta e="T102" id="Seg_1237" s="T101">pän-nɨ-t</ta>
            <ta e="T103" id="Seg_1238" s="T102">tü-də</ta>
            <ta e="T104" id="Seg_1239" s="T103">por-u-n</ta>
            <ta e="T105" id="Seg_1240" s="T104">kundar</ta>
            <ta e="T106" id="Seg_1241" s="T105">tʼeːlɨ-n</ta>
            <ta e="T107" id="Seg_1242" s="T106">ezu-n</ta>
            <ta e="T108" id="Seg_1243" s="T107">tüːləsä-m</ta>
            <ta e="T109" id="Seg_1244" s="T108">iː-ɣə-t</ta>
            <ta e="T110" id="Seg_1245" s="T109">puːlʼä-j</ta>
            <ta e="T111" id="Seg_1246" s="T110">patron-ɨ-n</ta>
            <ta e="T112" id="Seg_1247" s="T111">tüːləsä-m-də</ta>
            <ta e="T113" id="Seg_1248" s="T112">aːbəstɨ-t</ta>
            <ta e="T114" id="Seg_1249" s="T113">tʼü-n</ta>
            <ta e="T115" id="Seg_1250" s="T114">dop-qɨnnä</ta>
            <ta e="T116" id="Seg_1251" s="T115">aːrutqö</ta>
            <ta e="T117" id="Seg_1252" s="T116">qwän-nɨ</ta>
            <ta e="T118" id="Seg_1253" s="T117">i</ta>
            <ta e="T119" id="Seg_1254" s="T118">niːn</ta>
            <ta e="T120" id="Seg_1255" s="T119">nɨ-ŋa</ta>
            <ta e="T121" id="Seg_1256" s="T120">manǯi-mba-t</ta>
            <ta e="T122" id="Seg_1257" s="T121">čaǯi-ŋ</ta>
            <ta e="T123" id="Seg_1258" s="T122">qwärɣə</ta>
            <ta e="T124" id="Seg_1259" s="T123">uːlɣo-r-bədi</ta>
            <ta e="T125" id="Seg_1260" s="T124">qwärɣə</ta>
            <ta e="T126" id="Seg_1261" s="T125">üt-tə</ta>
            <ta e="T127" id="Seg_1262" s="T126">sɨro-ndə</ta>
            <ta e="T128" id="Seg_1263" s="T127">pümguj-ba</ta>
            <ta e="T129" id="Seg_1264" s="T128">ondə</ta>
            <ta e="T130" id="Seg_1265" s="T129">*sim-də</ta>
            <ta e="T131" id="Seg_1266" s="T130">tar-tə</ta>
            <ta e="T132" id="Seg_1267" s="T131">qande-ptɨ-mba-t</ta>
            <ta e="T133" id="Seg_1268" s="T132">i</ta>
            <ta e="T134" id="Seg_1269" s="T133">tü-a-ŋ</ta>
            <ta e="T135" id="Seg_1270" s="T134">äsä-nn-ä</ta>
            <ta e="T136" id="Seg_1271" s="T135">tʼü-n</ta>
            <ta e="T137" id="Seg_1272" s="T136">dop-tə</ta>
            <ta e="T138" id="Seg_1273" s="T137">am-ɨ-gu</ta>
            <ta e="T139" id="Seg_1274" s="T138">asä-w</ta>
            <ta e="T140" id="Seg_1275" s="T139">äsä-w</ta>
            <ta e="T141" id="Seg_1276" s="T140">tü-ndə</ta>
            <ta e="T142" id="Seg_1277" s="T141">qoːnɨŋ</ta>
            <ta e="T143" id="Seg_1278" s="T142">pän-nɨ-t</ta>
            <ta e="T144" id="Seg_1279" s="T143">poː-m</ta>
            <ta e="T145" id="Seg_1280" s="T144">tü</ta>
            <ta e="T146" id="Seg_1281" s="T145">por-u-n</ta>
            <ta e="T147" id="Seg_1282" s="T146">so-ŋ</ta>
            <ta e="T148" id="Seg_1283" s="T147">qwärɣə</ta>
            <ta e="T149" id="Seg_1284" s="T148">äsä-n-nə</ta>
            <ta e="T150" id="Seg_1285" s="T149">moqo-ɣan</ta>
            <ta e="T151" id="Seg_1286" s="T150">tü-n</ta>
            <ta e="T152" id="Seg_1287" s="T151">püru-ŋ</ta>
            <ta e="T153" id="Seg_1288" s="T152">üttɨ-mbɨ-lʼe</ta>
            <ta e="T154" id="Seg_1289" s="T153">qwärɣɨ-n</ta>
            <ta e="T155" id="Seg_1290" s="T154">ulɣo-ŋ</ta>
            <ta e="T156" id="Seg_1291" s="T155">qandə-bədi</ta>
            <ta e="T157" id="Seg_1292" s="T156">tar-tä</ta>
            <ta e="T158" id="Seg_1293" s="T157">tü-n</ta>
            <ta e="T159" id="Seg_1294" s="T158">qö-ɣɨn</ta>
            <ta e="T160" id="Seg_1295" s="T159">tʼü-a-ŋ</ta>
            <ta e="T161" id="Seg_1296" s="T160">assä-w</ta>
            <ta e="T162" id="Seg_1297" s="T161">tʼüləsä-n</ta>
            <ta e="T163" id="Seg_1298" s="T162">tʼäǯi-t</ta>
            <ta e="T164" id="Seg_1299" s="T163">qwärɣɨ-m</ta>
            <ta e="T165" id="Seg_1300" s="T164">qwärqa</ta>
            <ta e="T166" id="Seg_1301" s="T165">čäčo-lʼdʼi-ŋ</ta>
            <ta e="T167" id="Seg_1302" s="T166">nilʼdʼi-ŋ</ta>
            <ta e="T168" id="Seg_1303" s="T167">okkɨr-ɨ-ŋ</ta>
            <ta e="T169" id="Seg_1304" s="T168">man</ta>
            <ta e="T170" id="Seg_1305" s="T169">äsä-w</ta>
            <ta e="T171" id="Seg_1306" s="T170">aːlɨ-n-bɨ-zɨ-t</ta>
            <ta e="T172" id="Seg_1307" s="T171">qwät-pɨ-zɨ-t</ta>
            <ta e="T173" id="Seg_1308" s="T172">am-bɨ-zɨ-t</ta>
            <ta e="T174" id="Seg_1309" s="T173">qwärɣɨ-m</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1310" s="T1">ulgo-r-mbɨdi</ta>
            <ta e="T3" id="Seg_1311" s="T2">qwärqa</ta>
            <ta e="T4" id="Seg_1312" s="T3">ässɨ-mɨ</ta>
            <ta e="T5" id="Seg_1313" s="T4">qwən-sɨ</ta>
            <ta e="T6" id="Seg_1314" s="T5">matʼtʼi-ndɨ</ta>
            <ta e="T7" id="Seg_1315" s="T6">illɨ-ku-sɨ</ta>
            <ta e="T8" id="Seg_1316" s="T7">suːrəm-lʼdi</ta>
            <ta e="T9" id="Seg_1317" s="T8">maːt-qən</ta>
            <ta e="T10" id="Seg_1318" s="T9">nʼaja-j-ku-sɨ</ta>
            <ta e="T11" id="Seg_1319" s="T10">soː-k</ta>
            <ta e="T12" id="Seg_1320" s="T11">qwat-ku-sɨ-tɨ</ta>
            <ta e="T13" id="Seg_1321" s="T12">nʼaja-m</ta>
            <ta e="T14" id="Seg_1322" s="T13">am-ɨ-gu</ta>
            <ta e="T15" id="Seg_1323" s="T14">pemɨ-mbɨ-ptä-qən</ta>
            <ta e="T16" id="Seg_1324" s="T15">matʼtʼi-qən</ta>
            <ta e="T17" id="Seg_1325" s="T16">tʼätča-ku-sɨ-tɨ</ta>
            <ta e="T18" id="Seg_1326" s="T17">säŋɨ-m</ta>
            <ta e="T19" id="Seg_1327" s="T18">peːge-m</ta>
            <ta e="T20" id="Seg_1328" s="T19">am-ku-n-gaːlɨ-k</ta>
            <ta e="T21" id="Seg_1329" s="T20">matʼtʼi-qən</ta>
            <ta e="T22" id="Seg_1330" s="T21">suːrəm-lʼdi</ta>
            <ta e="T23" id="Seg_1331" s="T22">maːt-qən</ta>
            <ta e="T24" id="Seg_1332" s="T23">assɨ</ta>
            <ta e="T25" id="Seg_1333" s="T24">eː-ku-sɨ</ta>
            <ta e="T26" id="Seg_1334" s="T25">kanak-tɨ</ta>
            <ta e="T27" id="Seg_1335" s="T26">tülʼse-tɨ</ta>
            <ta e="T28" id="Seg_1336" s="T27">soː</ta>
            <ta e="T29" id="Seg_1337" s="T28">eː-ku-sɨ</ta>
            <ta e="T30" id="Seg_1338" s="T29">okkɨr-ɨ-k</ta>
            <ta e="T31" id="Seg_1339" s="T30">ässɨ-mɨ</ta>
            <ta e="T32" id="Seg_1340" s="T31">suːrəm-lʼdi</ta>
            <ta e="T33" id="Seg_1341" s="T32">maːt-qɨnnɨ</ta>
            <ta e="T34" id="Seg_1342" s="T33">qwən-ŋɨ</ta>
            <ta e="T35" id="Seg_1343" s="T34">šittə-k</ta>
            <ta e="T36" id="Seg_1344" s="T35">nakkɨr-ɨ-k</ta>
            <ta e="T37" id="Seg_1345" s="T36">šäqqɨ-gu</ta>
            <ta e="T38" id="Seg_1346" s="T37">qoːre-qən</ta>
            <ta e="T39" id="Seg_1347" s="T38">qarɨ-mɨ-qɨnnɨ</ta>
            <ta e="T40" id="Seg_1348" s="T39">to</ta>
            <ta e="T41" id="Seg_1349" s="T40">üːdɨ-ndɨ</ta>
            <ta e="T42" id="Seg_1350" s="T41">tap</ta>
            <ta e="T43" id="Seg_1351" s="T42">paldʼu-ku-sɨ</ta>
            <ta e="T44" id="Seg_1352" s="T43">na</ta>
            <ta e="T45" id="Seg_1353" s="T44">tʼeːlɨ</ta>
            <ta e="T46" id="Seg_1354" s="T45">qwat-ŋɨ-tɨ</ta>
            <ta e="T47" id="Seg_1355" s="T46">nakkɨr</ta>
            <ta e="T48" id="Seg_1356" s="T47">saːrum</ta>
            <ta e="T49" id="Seg_1357" s="T48">sombɨlʼe</ta>
            <ta e="T50" id="Seg_1358" s="T49">qwäj</ta>
            <ta e="T51" id="Seg_1359" s="T50">nʼaja-m</ta>
            <ta e="T52" id="Seg_1360" s="T51">okkɨr</ta>
            <ta e="T53" id="Seg_1361" s="T52">qaːs-ɨ-m</ta>
            <ta e="T54" id="Seg_1362" s="T53">okkɨr</ta>
            <ta e="T55" id="Seg_1363" s="T54">sɨ-m</ta>
            <ta e="T56" id="Seg_1364" s="T55">naːdə</ta>
            <ta e="T57" id="Seg_1365" s="T56">eː-sɨ-n</ta>
            <ta e="T58" id="Seg_1366" s="T57">ara-n</ta>
            <ta e="T59" id="Seg_1367" s="T58">nɨwaj-k</ta>
            <ta e="T60" id="Seg_1368" s="T59">üːdɨ-m-ŋɨ</ta>
            <ta e="T61" id="Seg_1369" s="T60">ässɨ-mɨ</ta>
            <ta e="T62" id="Seg_1370" s="T61">uttɨrɨ-n</ta>
            <ta e="T63" id="Seg_1371" s="T62">po-m</ta>
            <ta e="T64" id="Seg_1372" s="T63">ladiŋg-ŋɨ-tɨ</ta>
            <ta e="T65" id="Seg_1373" s="T64">patʼtʼal-ŋɨ-tɨ</ta>
            <ta e="T66" id="Seg_1374" s="T65">qoːre-m</ta>
            <ta e="T67" id="Seg_1375" s="T66">meː-ŋɨ-tɨ</ta>
            <ta e="T68" id="Seg_1376" s="T67">po-m</ta>
            <ta e="T69" id="Seg_1377" s="T68">tuɣol-ŋɨ-tɨ</ta>
            <ta e="T70" id="Seg_1378" s="T69">iːttä-r-ŋɨ</ta>
            <ta e="T71" id="Seg_1379" s="T70">qwat-mbɨdi</ta>
            <ta e="T72" id="Seg_1380" s="T71">suːrǝm-la-m-tɨ</ta>
            <ta e="T73" id="Seg_1381" s="T72">qɨr-ɨ-ne-ŋɨ-tɨ</ta>
            <ta e="T74" id="Seg_1382" s="T73">ontɨ</ta>
            <ta e="T75" id="Seg_1383" s="T74">am-r-ɨ-ŋɨ-n</ta>
            <ta e="T76" id="Seg_1384" s="T75">kanak-m-tɨ</ta>
            <ta e="T77" id="Seg_1385" s="T76">abastɨ-tɨ</ta>
            <ta e="T78" id="Seg_1386" s="T77">patron-la-m-tɨ</ta>
            <ta e="T79" id="Seg_1387" s="T78">abastɨ-tɨ</ta>
            <ta e="T80" id="Seg_1388" s="T79">koːptɨ-m</ta>
            <ta e="T81" id="Seg_1389" s="T80">meː-ŋɨ-tɨ</ta>
            <ta e="T82" id="Seg_1390" s="T81">quča-ŋɨ</ta>
            <ta e="T83" id="Seg_1391" s="T82">qontə-gu</ta>
            <ta e="T84" id="Seg_1392" s="T83">kuːn-gonɨn</ta>
            <ta e="T85" id="Seg_1393" s="T84">qontə-sɨ</ta>
            <ta e="T86" id="Seg_1394" s="T85">kundɨ</ta>
            <ta e="T87" id="Seg_1395" s="T86">ilʼi</ta>
            <ta e="T88" id="Seg_1396" s="T87">mu</ta>
            <ta e="T89" id="Seg_1397" s="T88">assɨ</ta>
            <ta e="T90" id="Seg_1398" s="T89">qüːdere-n</ta>
            <ta e="T91" id="Seg_1399" s="T90">moːrɨ</ta>
            <ta e="T92" id="Seg_1400" s="T91">qo-nče-r-ŋɨ-tɨ</ta>
            <ta e="T93" id="Seg_1401" s="T92">ündɨ-tɨt</ta>
            <ta e="T94" id="Seg_1402" s="T93">kanak</ta>
            <ta e="T95" id="Seg_1403" s="T94">muːdɨ-m</ta>
            <ta e="T96" id="Seg_1404" s="T95">kanak-n</ta>
            <ta e="T97" id="Seg_1405" s="T96">muːdɨ-ndɨ</ta>
            <ta e="T98" id="Seg_1406" s="T97">sittə-n</ta>
            <ta e="T99" id="Seg_1407" s="T98">watti-p-či-n</ta>
            <ta e="T100" id="Seg_1408" s="T99">tüː-ndɨ</ta>
            <ta e="T101" id="Seg_1409" s="T100">po-m</ta>
            <ta e="T102" id="Seg_1410" s="T101">pan-ŋɨ-tɨ</ta>
            <ta e="T103" id="Seg_1411" s="T102">tüː-tɨ</ta>
            <ta e="T104" id="Seg_1412" s="T103">por-ɨ-n</ta>
            <ta e="T105" id="Seg_1413" s="T104">kundar</ta>
            <ta e="T106" id="Seg_1414" s="T105">tʼeːlɨ-n</ta>
            <ta e="T107" id="Seg_1415" s="T106">äsɨ-n</ta>
            <ta e="T108" id="Seg_1416" s="T107">tülʼse-m</ta>
            <ta e="T109" id="Seg_1417" s="T108">iː-ŋɨ-tɨ</ta>
            <ta e="T110" id="Seg_1418" s="T109">pulʼa-lʼ</ta>
            <ta e="T111" id="Seg_1419" s="T110">patron-ɨ-n</ta>
            <ta e="T112" id="Seg_1420" s="T111">tülʼse-m-tɨ</ta>
            <ta e="T113" id="Seg_1421" s="T112">abastɨ-tɨ</ta>
            <ta e="T114" id="Seg_1422" s="T113">tüː-n</ta>
            <ta e="T115" id="Seg_1423" s="T114">toːp-qɨnnɨ</ta>
            <ta e="T116" id="Seg_1424" s="T115">aːrɨtqö</ta>
            <ta e="T117" id="Seg_1425" s="T116">qwən-ŋɨ</ta>
            <ta e="T118" id="Seg_1426" s="T117">i</ta>
            <ta e="T119" id="Seg_1427" s="T118">nɨːnɨ</ta>
            <ta e="T120" id="Seg_1428" s="T119">nɨ-ŋɨ</ta>
            <ta e="T121" id="Seg_1429" s="T120">manǯɨ-mbɨ-tɨ</ta>
            <ta e="T122" id="Seg_1430" s="T121">čaːǯɨ-n</ta>
            <ta e="T123" id="Seg_1431" s="T122">qwärqa</ta>
            <ta e="T124" id="Seg_1432" s="T123">ulgo-r-mbɨdi</ta>
            <ta e="T125" id="Seg_1433" s="T124">qwärqa</ta>
            <ta e="T126" id="Seg_1434" s="T125">üt-ndɨ</ta>
            <ta e="T127" id="Seg_1435" s="T126">sɨrrɨ-ndɨ</ta>
            <ta e="T128" id="Seg_1436" s="T127">pümguj-mbɨ</ta>
            <ta e="T129" id="Seg_1437" s="T128">ontɨ</ta>
            <ta e="T130" id="Seg_1438" s="T129">*siːm-tɨ</ta>
            <ta e="T131" id="Seg_1439" s="T130">tar-tɨ</ta>
            <ta e="T132" id="Seg_1440" s="T131">qantǝ-ptɨ-mbɨ-tɨ</ta>
            <ta e="T133" id="Seg_1441" s="T132">i</ta>
            <ta e="T134" id="Seg_1442" s="T133">tüː-ŋɨ-n</ta>
            <ta e="T135" id="Seg_1443" s="T134">ässɨ-nan-lʼ</ta>
            <ta e="T136" id="Seg_1444" s="T135">tüː-n</ta>
            <ta e="T137" id="Seg_1445" s="T136">toːp-ndɨ</ta>
            <ta e="T138" id="Seg_1446" s="T137">am-ɨ-gu</ta>
            <ta e="T139" id="Seg_1447" s="T138">ässɨ-m</ta>
            <ta e="T140" id="Seg_1448" s="T139">ässɨ-mɨ</ta>
            <ta e="T141" id="Seg_1449" s="T140">tüː-ndɨ</ta>
            <ta e="T142" id="Seg_1450" s="T141">qoːnɨŋ</ta>
            <ta e="T143" id="Seg_1451" s="T142">pan-ŋɨ-tɨ</ta>
            <ta e="T144" id="Seg_1452" s="T143">po-m</ta>
            <ta e="T145" id="Seg_1453" s="T144">tüː</ta>
            <ta e="T146" id="Seg_1454" s="T145">por-ɨ-n</ta>
            <ta e="T147" id="Seg_1455" s="T146">soː-k</ta>
            <ta e="T148" id="Seg_1456" s="T147">qwärqa</ta>
            <ta e="T149" id="Seg_1457" s="T148">ässɨ-n-nɨ</ta>
            <ta e="T150" id="Seg_1458" s="T149">moqə-qən</ta>
            <ta e="T151" id="Seg_1459" s="T150">tüː-n</ta>
            <ta e="T152" id="Seg_1460" s="T151">püruj-k</ta>
            <ta e="T153" id="Seg_1461" s="T152">üːtɨ-mbɨ-le</ta>
            <ta e="T154" id="Seg_1462" s="T153">qwärqa-m</ta>
            <ta e="T155" id="Seg_1463" s="T154">ulgo-k</ta>
            <ta e="T156" id="Seg_1464" s="T155">qantǝ-mbɨdi</ta>
            <ta e="T157" id="Seg_1465" s="T156">tar-tɨ</ta>
            <ta e="T158" id="Seg_1466" s="T157">tüː-n</ta>
            <ta e="T159" id="Seg_1467" s="T158">kö-qən</ta>
            <ta e="T160" id="Seg_1468" s="T159">tüː-ŋɨ-n</ta>
            <ta e="T161" id="Seg_1469" s="T160">ässɨ-m</ta>
            <ta e="T162" id="Seg_1470" s="T161">tülʼse-n</ta>
            <ta e="T163" id="Seg_1471" s="T162">tʼaččɨ-tɨ</ta>
            <ta e="T164" id="Seg_1472" s="T163">qwärqa-m</ta>
            <ta e="T165" id="Seg_1473" s="T164">qwärqa</ta>
            <ta e="T166" id="Seg_1474" s="T165">čečo-lʼčǝ-n</ta>
            <ta e="T167" id="Seg_1475" s="T166">nɨlʼǯi-k</ta>
            <ta e="T168" id="Seg_1476" s="T167">okkɨr-ɨ-k</ta>
            <ta e="T169" id="Seg_1477" s="T168">man</ta>
            <ta e="T170" id="Seg_1478" s="T169">ässɨ-mɨ</ta>
            <ta e="T171" id="Seg_1479" s="T170">aːlə-ne-mbɨ-sɨ-tɨ</ta>
            <ta e="T172" id="Seg_1480" s="T171">qwat-mbɨ-sɨ-tɨ</ta>
            <ta e="T173" id="Seg_1481" s="T172">am-mbɨ-sɨ-tɨ</ta>
            <ta e="T174" id="Seg_1482" s="T173">qwärqa-m</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1483" s="T1">ice-VBLZ-PTCP.PST</ta>
            <ta e="T3" id="Seg_1484" s="T2">bear.[NOM]</ta>
            <ta e="T4" id="Seg_1485" s="T3">father.[NOM]-1SG</ta>
            <ta e="T5" id="Seg_1486" s="T4">go.away-PST.[3SG.S]</ta>
            <ta e="T6" id="Seg_1487" s="T5">taiga-ILL</ta>
            <ta e="T7" id="Seg_1488" s="T6">live-HAB-PST.[3SG.S]</ta>
            <ta e="T8" id="Seg_1489" s="T7">wild.animal-CAP.ADJZ</ta>
            <ta e="T9" id="Seg_1490" s="T8">house-LOC</ta>
            <ta e="T10" id="Seg_1491" s="T9">squirrel-CAP-HAB-PST.[3SG.S]</ta>
            <ta e="T11" id="Seg_1492" s="T10">good-ADVZ</ta>
            <ta e="T12" id="Seg_1493" s="T11">catch-HAB-PST-3SG.O</ta>
            <ta e="T13" id="Seg_1494" s="T12">squirrel-ACC</ta>
            <ta e="T14" id="Seg_1495" s="T13">eat-EP-INF</ta>
            <ta e="T15" id="Seg_1496" s="T14">hunt-DUR-ACTN-LOC</ta>
            <ta e="T16" id="Seg_1497" s="T15">taiga-LOC</ta>
            <ta e="T17" id="Seg_1498" s="T16">shoot-HAB-PST-3SG.O</ta>
            <ta e="T18" id="Seg_1499" s="T17">capercaillie-ACC</ta>
            <ta e="T19" id="Seg_1500" s="T18">hazelhen-ACC</ta>
            <ta e="T20" id="Seg_1501" s="T19">eat-ACTN-GEN-CAR-ADVZ</ta>
            <ta e="T21" id="Seg_1502" s="T20">taiga-LOC</ta>
            <ta e="T22" id="Seg_1503" s="T21">wild.animal-CAP.ADJZ</ta>
            <ta e="T23" id="Seg_1504" s="T22">house-LOC</ta>
            <ta e="T24" id="Seg_1505" s="T23">NEG</ta>
            <ta e="T25" id="Seg_1506" s="T24">be-HAB-PST.[3SG.S]</ta>
            <ta e="T26" id="Seg_1507" s="T25">dog-3SG</ta>
            <ta e="T27" id="Seg_1508" s="T26">rifle-3SG</ta>
            <ta e="T28" id="Seg_1509" s="T27">good</ta>
            <ta e="T29" id="Seg_1510" s="T28">be-HAB-PST.[3SG.S]</ta>
            <ta e="T30" id="Seg_1511" s="T29">one-EP-ADVZ</ta>
            <ta e="T31" id="Seg_1512" s="T30">father-1SG</ta>
            <ta e="T32" id="Seg_1513" s="T31">wild.animal-CAP.ADJZ</ta>
            <ta e="T33" id="Seg_1514" s="T32">house-EL</ta>
            <ta e="T34" id="Seg_1515" s="T33">go.away-CO.[3SG.S]</ta>
            <ta e="T35" id="Seg_1516" s="T34">two-ADVZ</ta>
            <ta e="T36" id="Seg_1517" s="T35">three-EP-ADVZ</ta>
            <ta e="T37" id="Seg_1518" s="T36">spend.night-INF</ta>
            <ta e="T38" id="Seg_1519" s="T37">lodging-LOC</ta>
            <ta e="T39" id="Seg_1520" s="T38">morning-something-EL</ta>
            <ta e="T40" id="Seg_1521" s="T39">up.to</ta>
            <ta e="T41" id="Seg_1522" s="T40">evening-ILL</ta>
            <ta e="T42" id="Seg_1523" s="T41">(s)he.[NOM]</ta>
            <ta e="T43" id="Seg_1524" s="T42">go-HAB-PST.[3SG.S]</ta>
            <ta e="T44" id="Seg_1525" s="T43">this</ta>
            <ta e="T45" id="Seg_1526" s="T44">day.[NOM]</ta>
            <ta e="T46" id="Seg_1527" s="T45">catch-CO-3SG.O</ta>
            <ta e="T47" id="Seg_1528" s="T46">three</ta>
            <ta e="T48" id="Seg_1529" s="T47">ten</ta>
            <ta e="T49" id="Seg_1530" s="T48">five</ta>
            <ta e="T50" id="Seg_1531" s="T49">superfluous</ta>
            <ta e="T51" id="Seg_1532" s="T50">squirrel-ACC</ta>
            <ta e="T52" id="Seg_1533" s="T51">one</ta>
            <ta e="T53" id="Seg_1534" s="T52">weasel-EP-ACC</ta>
            <ta e="T54" id="Seg_1535" s="T53">one</ta>
            <ta e="T55" id="Seg_1536" s="T54">sable-ACC</ta>
            <ta e="T56" id="Seg_1537" s="T55">this</ta>
            <ta e="T57" id="Seg_1538" s="T56">be-PST-3SG.S</ta>
            <ta e="T58" id="Seg_1539" s="T57">autumn-ADV.LOC</ta>
            <ta e="T59" id="Seg_1540" s="T58">dark-ADVZ</ta>
            <ta e="T60" id="Seg_1541" s="T59">evening-TRL-CO.[3SG.S]</ta>
            <ta e="T61" id="Seg_1542" s="T60">father-1SG</ta>
            <ta e="T62" id="Seg_1543" s="T61">stop-3SG.S</ta>
            <ta e="T63" id="Seg_1544" s="T62">tree-ACC</ta>
            <ta e="T64" id="Seg_1545" s="T63">%%-CO-3SG.O</ta>
            <ta e="T65" id="Seg_1546" s="T64">hack-CO-3SG.O</ta>
            <ta e="T66" id="Seg_1547" s="T65">lodging-ACC</ta>
            <ta e="T67" id="Seg_1548" s="T66">do-CO-3SG.O</ta>
            <ta e="T68" id="Seg_1549" s="T67">tree-ACC</ta>
            <ta e="T69" id="Seg_1550" s="T68">carry-CO-3SG.O</ta>
            <ta e="T70" id="Seg_1551" s="T69">%%-FRQ-CO.[3SG.S]</ta>
            <ta e="T71" id="Seg_1552" s="T70">kill-PTCP.PST</ta>
            <ta e="T72" id="Seg_1553" s="T71">wild.animal-PL-ACC-3SG</ta>
            <ta e="T73" id="Seg_1554" s="T72">tear.off-EP-DRV-CO-3SG.O</ta>
            <ta e="T74" id="Seg_1555" s="T73">oneself.3SG</ta>
            <ta e="T75" id="Seg_1556" s="T74">eat-FRQ-EP-CO-3SG.S</ta>
            <ta e="T76" id="Seg_1557" s="T75">dog-ACC-3SG</ta>
            <ta e="T77" id="Seg_1558" s="T76">feed-3SG.O</ta>
            <ta e="T78" id="Seg_1559" s="T77">cartridge-PL-ACC-3SG</ta>
            <ta e="T79" id="Seg_1560" s="T78">feed-3SG.O</ta>
            <ta e="T80" id="Seg_1561" s="T79">bed-ACC</ta>
            <ta e="T81" id="Seg_1562" s="T80">do-CO-3SG.O</ta>
            <ta e="T82" id="Seg_1563" s="T81">go.to.sleep-CO.[3SG.S]</ta>
            <ta e="T83" id="Seg_1564" s="T82">sleep-INF</ta>
            <ta e="T84" id="Seg_1565" s="T83">where-%%</ta>
            <ta e="T85" id="Seg_1566" s="T84">sleep-PST.[3SG.S]</ta>
            <ta e="T86" id="Seg_1567" s="T85">long</ta>
            <ta e="T87" id="Seg_1568" s="T86">or</ta>
            <ta e="T88" id="Seg_1569" s="T87">%%</ta>
            <ta e="T89" id="Seg_1570" s="T88">NEG</ta>
            <ta e="T90" id="Seg_1571" s="T89">dream-GEN</ta>
            <ta e="T91" id="Seg_1572" s="T90">through</ta>
            <ta e="T92" id="Seg_1573" s="T91">see-IPFV3-FRQ-CO-3SG.O</ta>
            <ta e="T93" id="Seg_1574" s="T92">hear-3PL</ta>
            <ta e="T94" id="Seg_1575" s="T93">dog.[NOM]</ta>
            <ta e="T95" id="Seg_1576" s="T94">barking-ACC</ta>
            <ta e="T96" id="Seg_1577" s="T95">dog-GEN</ta>
            <ta e="T97" id="Seg_1578" s="T96">barking-ILL</ta>
            <ta e="T98" id="Seg_1579" s="T97">wake.up-3SG.S</ta>
            <ta e="T99" id="Seg_1580" s="T98">lift-DRV-RFL-3SG.S</ta>
            <ta e="T100" id="Seg_1581" s="T99">fire-ILL</ta>
            <ta e="T101" id="Seg_1582" s="T100">tree-ACC</ta>
            <ta e="T102" id="Seg_1583" s="T101">put-CO-3SG.O</ta>
            <ta e="T103" id="Seg_1584" s="T102">fire-3SG</ta>
            <ta e="T104" id="Seg_1585" s="T103">burn-EP-3SG.S</ta>
            <ta e="T105" id="Seg_1586" s="T104">how</ta>
            <ta e="T106" id="Seg_1587" s="T105">day-ADV.LOC</ta>
            <ta e="T107" id="Seg_1588" s="T106">become-3SG.S</ta>
            <ta e="T108" id="Seg_1589" s="T107">rifle-ACC</ta>
            <ta e="T109" id="Seg_1590" s="T108">take-CO-3SG.O</ta>
            <ta e="T110" id="Seg_1591" s="T109">bullet-ADJZ</ta>
            <ta e="T111" id="Seg_1592" s="T110">cartridge-EP-GEN</ta>
            <ta e="T112" id="Seg_1593" s="T111">rifle-ACC-3SG</ta>
            <ta e="T113" id="Seg_1594" s="T112">charge-3SG.O</ta>
            <ta e="T114" id="Seg_1595" s="T113">fire-GEN</ta>
            <ta e="T115" id="Seg_1596" s="T114">edge-EL</ta>
            <ta e="T116" id="Seg_1597" s="T115">aside</ta>
            <ta e="T117" id="Seg_1598" s="T116">go.away-CO.[3SG.S]</ta>
            <ta e="T118" id="Seg_1599" s="T117">and</ta>
            <ta e="T119" id="Seg_1600" s="T118">then</ta>
            <ta e="T120" id="Seg_1601" s="T119">stand-CO.[3SG.S]</ta>
            <ta e="T121" id="Seg_1602" s="T120">look-PST.NAR-3SG.O</ta>
            <ta e="T122" id="Seg_1603" s="T121">go-3SG.S</ta>
            <ta e="T123" id="Seg_1604" s="T122">bear.[NOM]</ta>
            <ta e="T124" id="Seg_1605" s="T123">ice-VBLZ-PTCP.PST</ta>
            <ta e="T125" id="Seg_1606" s="T124">bear.[NOM]</ta>
            <ta e="T126" id="Seg_1607" s="T125">water-ILL</ta>
            <ta e="T127" id="Seg_1608" s="T126">snow-ILL</ta>
            <ta e="T128" id="Seg_1609" s="T127">roll.out-PST.NAR.[3SG.S]</ta>
            <ta e="T129" id="Seg_1610" s="T128">oneself.3SG</ta>
            <ta e="T130" id="Seg_1611" s="T129">self-3SG</ta>
            <ta e="T131" id="Seg_1612" s="T130">fur-3SG</ta>
            <ta e="T132" id="Seg_1613" s="T131">freeze-CAUS-PST.NAR-3SG.O</ta>
            <ta e="T133" id="Seg_1614" s="T132">and</ta>
            <ta e="T134" id="Seg_1615" s="T133">come-CO-3SG.S</ta>
            <ta e="T135" id="Seg_1616" s="T134">father-ADES-ADJZ</ta>
            <ta e="T136" id="Seg_1617" s="T135">fire-GEN</ta>
            <ta e="T137" id="Seg_1618" s="T136">edge-ILL</ta>
            <ta e="T138" id="Seg_1619" s="T137">eat-EP-INF</ta>
            <ta e="T139" id="Seg_1620" s="T138">father-ACC</ta>
            <ta e="T140" id="Seg_1621" s="T139">father-1SG</ta>
            <ta e="T141" id="Seg_1622" s="T140">fire-ILL</ta>
            <ta e="T142" id="Seg_1623" s="T141">many</ta>
            <ta e="T143" id="Seg_1624" s="T142">put-CO-3SG.O</ta>
            <ta e="T144" id="Seg_1625" s="T143">tree-ACC</ta>
            <ta e="T145" id="Seg_1626" s="T144">fire.[NOM]</ta>
            <ta e="T146" id="Seg_1627" s="T145">burn-EP-3SG.S</ta>
            <ta e="T147" id="Seg_1628" s="T146">good-ADVZ</ta>
            <ta e="T148" id="Seg_1629" s="T147">bear.[NOM]</ta>
            <ta e="T149" id="Seg_1630" s="T148">father-GEN-OBL.1SG</ta>
            <ta e="T150" id="Seg_1631" s="T149">back-LOC</ta>
            <ta e="T151" id="Seg_1632" s="T150">fire-GEN</ta>
            <ta e="T152" id="Seg_1633" s="T151">round-ADVZ</ta>
            <ta e="T153" id="Seg_1634" s="T152">descend-DUR-CVB</ta>
            <ta e="T154" id="Seg_1635" s="T153">bear-ACC</ta>
            <ta e="T155" id="Seg_1636" s="T154">ice-ADVZ</ta>
            <ta e="T156" id="Seg_1637" s="T155">freeze-PTCP.PST</ta>
            <ta e="T157" id="Seg_1638" s="T156">fur-3SG</ta>
            <ta e="T158" id="Seg_1639" s="T157">fire-GEN</ta>
            <ta e="T159" id="Seg_1640" s="T158">side-LOC</ta>
            <ta e="T160" id="Seg_1641" s="T159">come-CO-3SG.S</ta>
            <ta e="T161" id="Seg_1642" s="T160">father-ACC</ta>
            <ta e="T162" id="Seg_1643" s="T161">rifle-INSTR2</ta>
            <ta e="T163" id="Seg_1644" s="T162">shoot-3SG.O</ta>
            <ta e="T164" id="Seg_1645" s="T163">bear-ACC</ta>
            <ta e="T165" id="Seg_1646" s="T164">bear.[NOM]</ta>
            <ta e="T166" id="Seg_1647" s="T165">fall-PFV-3SG.S</ta>
            <ta e="T167" id="Seg_1648" s="T166">such-ADVZ</ta>
            <ta e="T168" id="Seg_1649" s="T167">one-EP-ADVZ</ta>
            <ta e="T169" id="Seg_1650" s="T168">I.NOM</ta>
            <ta e="T170" id="Seg_1651" s="T169">father-1SG</ta>
            <ta e="T171" id="Seg_1652" s="T170">cheat-DRV-DUR-PST-3SG.O</ta>
            <ta e="T172" id="Seg_1653" s="T171">kill-DUR-PST-3SG.O</ta>
            <ta e="T173" id="Seg_1654" s="T172">eat-DUR-PST-3SG.O</ta>
            <ta e="T174" id="Seg_1655" s="T173">bear-ACC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1656" s="T1">лёд-VBLZ-PTCP.PST</ta>
            <ta e="T3" id="Seg_1657" s="T2">медведь.[NOM]</ta>
            <ta e="T4" id="Seg_1658" s="T3">отец.[NOM]-1SG</ta>
            <ta e="T5" id="Seg_1659" s="T4">уйти-PST.[3SG.S]</ta>
            <ta e="T6" id="Seg_1660" s="T5">тайга-ILL</ta>
            <ta e="T7" id="Seg_1661" s="T6">жить-HAB-PST.[3SG.S]</ta>
            <ta e="T8" id="Seg_1662" s="T7">зверь-CAP.ADJZ</ta>
            <ta e="T9" id="Seg_1663" s="T8">дом-LOC</ta>
            <ta e="T10" id="Seg_1664" s="T9">белка-CAP-HAB-PST.[3SG.S]</ta>
            <ta e="T11" id="Seg_1665" s="T10">хороший-ADVZ</ta>
            <ta e="T12" id="Seg_1666" s="T11">поймать-HAB-PST-3SG.O</ta>
            <ta e="T13" id="Seg_1667" s="T12">белка-ACC</ta>
            <ta e="T14" id="Seg_1668" s="T13">съесть-EP-INF</ta>
            <ta e="T15" id="Seg_1669" s="T14">охотиться-DUR-ACTN-LOC</ta>
            <ta e="T16" id="Seg_1670" s="T15">тайга-LOC</ta>
            <ta e="T17" id="Seg_1671" s="T16">стрелять-HAB-PST-3SG.O</ta>
            <ta e="T18" id="Seg_1672" s="T17">глухарь-ACC</ta>
            <ta e="T19" id="Seg_1673" s="T18">рябчик-ACC</ta>
            <ta e="T20" id="Seg_1674" s="T19">съесть-ACTN-GEN-CAR-ADVZ</ta>
            <ta e="T21" id="Seg_1675" s="T20">тайга-LOC</ta>
            <ta e="T22" id="Seg_1676" s="T21">зверь-CAP.ADJZ</ta>
            <ta e="T23" id="Seg_1677" s="T22">дом-LOC</ta>
            <ta e="T24" id="Seg_1678" s="T23">NEG</ta>
            <ta e="T25" id="Seg_1679" s="T24">быть-HAB-PST.[3SG.S]</ta>
            <ta e="T26" id="Seg_1680" s="T25">собака-3SG</ta>
            <ta e="T27" id="Seg_1681" s="T26">ружье-3SG</ta>
            <ta e="T28" id="Seg_1682" s="T27">хороший</ta>
            <ta e="T29" id="Seg_1683" s="T28">быть-HAB-PST.[3SG.S]</ta>
            <ta e="T30" id="Seg_1684" s="T29">один-EP-ADVZ</ta>
            <ta e="T31" id="Seg_1685" s="T30">отец-1SG</ta>
            <ta e="T32" id="Seg_1686" s="T31">зверь-CAP.ADJZ</ta>
            <ta e="T33" id="Seg_1687" s="T32">дом-EL</ta>
            <ta e="T34" id="Seg_1688" s="T33">уйти-CO.[3SG.S]</ta>
            <ta e="T35" id="Seg_1689" s="T34">два-ADVZ</ta>
            <ta e="T36" id="Seg_1690" s="T35">три-EP-ADVZ</ta>
            <ta e="T37" id="Seg_1691" s="T36">ночевать-INF</ta>
            <ta e="T38" id="Seg_1692" s="T37">стан-LOC</ta>
            <ta e="T39" id="Seg_1693" s="T38">утро-нечто-EL</ta>
            <ta e="T40" id="Seg_1694" s="T39">до</ta>
            <ta e="T41" id="Seg_1695" s="T40">вечер-ILL</ta>
            <ta e="T42" id="Seg_1696" s="T41">он(а).[NOM]</ta>
            <ta e="T43" id="Seg_1697" s="T42">ходить-HAB-PST.[3SG.S]</ta>
            <ta e="T44" id="Seg_1698" s="T43">этот</ta>
            <ta e="T45" id="Seg_1699" s="T44">день.[NOM]</ta>
            <ta e="T46" id="Seg_1700" s="T45">поймать-CO-3SG.O</ta>
            <ta e="T47" id="Seg_1701" s="T46">три</ta>
            <ta e="T48" id="Seg_1702" s="T47">десять</ta>
            <ta e="T49" id="Seg_1703" s="T48">пять</ta>
            <ta e="T50" id="Seg_1704" s="T49">излишний</ta>
            <ta e="T51" id="Seg_1705" s="T50">белка-ACC</ta>
            <ta e="T52" id="Seg_1706" s="T51">один</ta>
            <ta e="T53" id="Seg_1707" s="T52">колонок-EP-ACC</ta>
            <ta e="T54" id="Seg_1708" s="T53">один</ta>
            <ta e="T55" id="Seg_1709" s="T54">соболь-ACC</ta>
            <ta e="T56" id="Seg_1710" s="T55">этот</ta>
            <ta e="T57" id="Seg_1711" s="T56">быть-PST-3SG.S</ta>
            <ta e="T58" id="Seg_1712" s="T57">осень-ADV.LOC</ta>
            <ta e="T59" id="Seg_1713" s="T58">тёмный-ADVZ</ta>
            <ta e="T60" id="Seg_1714" s="T59">вечер-TRL-CO.[3SG.S]</ta>
            <ta e="T61" id="Seg_1715" s="T60">отец-1SG</ta>
            <ta e="T62" id="Seg_1716" s="T61">остановиться-3SG.S</ta>
            <ta e="T63" id="Seg_1717" s="T62">дерево-ACC</ta>
            <ta e="T64" id="Seg_1718" s="T63">%%-CO-3SG.O</ta>
            <ta e="T65" id="Seg_1719" s="T64">рубить-CO-3SG.O</ta>
            <ta e="T66" id="Seg_1720" s="T65">стан-ACC</ta>
            <ta e="T67" id="Seg_1721" s="T66">делать-CO-3SG.O</ta>
            <ta e="T68" id="Seg_1722" s="T67">дерево-ACC</ta>
            <ta e="T69" id="Seg_1723" s="T68">таскать-CO-3SG.O</ta>
            <ta e="T70" id="Seg_1724" s="T69">%%-FRQ-CO.[3SG.S]</ta>
            <ta e="T71" id="Seg_1725" s="T70">убить-PTCP.PST</ta>
            <ta e="T72" id="Seg_1726" s="T71">зверь-PL-ACC-3SG</ta>
            <ta e="T73" id="Seg_1727" s="T72">ободрать-EP-DRV-CO-3SG.O</ta>
            <ta e="T74" id="Seg_1728" s="T73">сам.3SG</ta>
            <ta e="T75" id="Seg_1729" s="T74">съесть-FRQ-EP-CO-3SG.S</ta>
            <ta e="T76" id="Seg_1730" s="T75">собака-ACC-3SG</ta>
            <ta e="T77" id="Seg_1731" s="T76">кормить-3SG.O</ta>
            <ta e="T78" id="Seg_1732" s="T77">патрон-PL-ACC-3SG</ta>
            <ta e="T79" id="Seg_1733" s="T78">кормить-3SG.O</ta>
            <ta e="T80" id="Seg_1734" s="T79">кровать-ACC</ta>
            <ta e="T81" id="Seg_1735" s="T80">делать-CO-3SG.O</ta>
            <ta e="T82" id="Seg_1736" s="T81">улечься.спать-CO.[3SG.S]</ta>
            <ta e="T83" id="Seg_1737" s="T82">спать-INF</ta>
            <ta e="T84" id="Seg_1738" s="T83">где-%%</ta>
            <ta e="T85" id="Seg_1739" s="T84">спать-PST.[3SG.S]</ta>
            <ta e="T86" id="Seg_1740" s="T85">долго</ta>
            <ta e="T87" id="Seg_1741" s="T86">или</ta>
            <ta e="T88" id="Seg_1742" s="T87">%%</ta>
            <ta e="T89" id="Seg_1743" s="T88">NEG</ta>
            <ta e="T90" id="Seg_1744" s="T89">сон-GEN</ta>
            <ta e="T91" id="Seg_1745" s="T90">сквозь</ta>
            <ta e="T92" id="Seg_1746" s="T91">увидеть-IPFV3-FRQ-CO-3SG.O</ta>
            <ta e="T93" id="Seg_1747" s="T92">слышать-3PL</ta>
            <ta e="T94" id="Seg_1748" s="T93">собака.[NOM]</ta>
            <ta e="T95" id="Seg_1749" s="T94">лай-ACC</ta>
            <ta e="T96" id="Seg_1750" s="T95">собака-GEN</ta>
            <ta e="T97" id="Seg_1751" s="T96">лай-ILL</ta>
            <ta e="T98" id="Seg_1752" s="T97">проснуться-3SG.S</ta>
            <ta e="T99" id="Seg_1753" s="T98">поднять-DRV-RFL-3SG.S</ta>
            <ta e="T100" id="Seg_1754" s="T99">огонь-ILL</ta>
            <ta e="T101" id="Seg_1755" s="T100">дерево-ACC</ta>
            <ta e="T102" id="Seg_1756" s="T101">положить-CO-3SG.O</ta>
            <ta e="T103" id="Seg_1757" s="T102">огонь-3SG</ta>
            <ta e="T104" id="Seg_1758" s="T103">гореть-EP-3SG.S</ta>
            <ta e="T105" id="Seg_1759" s="T104">как</ta>
            <ta e="T106" id="Seg_1760" s="T105">день-ADV.LOC</ta>
            <ta e="T107" id="Seg_1761" s="T106">стать-3SG.S</ta>
            <ta e="T108" id="Seg_1762" s="T107">ружье-ACC</ta>
            <ta e="T109" id="Seg_1763" s="T108">взять-CO-3SG.O</ta>
            <ta e="T110" id="Seg_1764" s="T109">пуля-ADJZ</ta>
            <ta e="T111" id="Seg_1765" s="T110">патрон-EP-GEN</ta>
            <ta e="T112" id="Seg_1766" s="T111">ружье-ACC-3SG</ta>
            <ta e="T113" id="Seg_1767" s="T112">зарядить-3SG.O</ta>
            <ta e="T114" id="Seg_1768" s="T113">огонь-GEN</ta>
            <ta e="T115" id="Seg_1769" s="T114">край-EL</ta>
            <ta e="T116" id="Seg_1770" s="T115">в.сторону</ta>
            <ta e="T117" id="Seg_1771" s="T116">уйти-CO.[3SG.S]</ta>
            <ta e="T118" id="Seg_1772" s="T117">и</ta>
            <ta e="T119" id="Seg_1773" s="T118">потом</ta>
            <ta e="T120" id="Seg_1774" s="T119">стоять-CO.[3SG.S]</ta>
            <ta e="T121" id="Seg_1775" s="T120">смотреть-PST.NAR-3SG.O</ta>
            <ta e="T122" id="Seg_1776" s="T121">идти-3SG.S</ta>
            <ta e="T123" id="Seg_1777" s="T122">медведь.[NOM]</ta>
            <ta e="T124" id="Seg_1778" s="T123">лёд-VBLZ-PTCP.PST</ta>
            <ta e="T125" id="Seg_1779" s="T124">медведь.[NOM]</ta>
            <ta e="T126" id="Seg_1780" s="T125">вода-ILL</ta>
            <ta e="T127" id="Seg_1781" s="T126">снег-ILL</ta>
            <ta e="T128" id="Seg_1782" s="T127">вываляться-PST.NAR.[3SG.S]</ta>
            <ta e="T129" id="Seg_1783" s="T128">сам.3SG</ta>
            <ta e="T130" id="Seg_1784" s="T129">себя-3SG</ta>
            <ta e="T131" id="Seg_1785" s="T130">шерсть-3SG</ta>
            <ta e="T132" id="Seg_1786" s="T131">замерзнуть-CAUS-PST.NAR-3SG.O</ta>
            <ta e="T133" id="Seg_1787" s="T132">и</ta>
            <ta e="T134" id="Seg_1788" s="T133">прийти-CO-3SG.S</ta>
            <ta e="T135" id="Seg_1789" s="T134">отец-ADES-ADJZ</ta>
            <ta e="T136" id="Seg_1790" s="T135">огонь-GEN</ta>
            <ta e="T137" id="Seg_1791" s="T136">край-ILL</ta>
            <ta e="T138" id="Seg_1792" s="T137">съесть-EP-INF</ta>
            <ta e="T139" id="Seg_1793" s="T138">отец-ACC</ta>
            <ta e="T140" id="Seg_1794" s="T139">отец-1SG</ta>
            <ta e="T141" id="Seg_1795" s="T140">огонь-ILL</ta>
            <ta e="T142" id="Seg_1796" s="T141">много</ta>
            <ta e="T143" id="Seg_1797" s="T142">положить-CO-3SG.O</ta>
            <ta e="T144" id="Seg_1798" s="T143">дерево-ACC</ta>
            <ta e="T145" id="Seg_1799" s="T144">огонь.[NOM]</ta>
            <ta e="T146" id="Seg_1800" s="T145">гореть-EP-3SG.S</ta>
            <ta e="T147" id="Seg_1801" s="T146">хороший-ADVZ</ta>
            <ta e="T148" id="Seg_1802" s="T147">медведь.[NOM]</ta>
            <ta e="T149" id="Seg_1803" s="T148">отец-GEN-OBL.1SG</ta>
            <ta e="T150" id="Seg_1804" s="T149">спина-LOC</ta>
            <ta e="T151" id="Seg_1805" s="T150">огонь-GEN</ta>
            <ta e="T152" id="Seg_1806" s="T151">круглый-ADVZ</ta>
            <ta e="T153" id="Seg_1807" s="T152">спуститься-DUR-CVB</ta>
            <ta e="T154" id="Seg_1808" s="T153">медведь-ACC</ta>
            <ta e="T155" id="Seg_1809" s="T154">лёд-ADVZ</ta>
            <ta e="T156" id="Seg_1810" s="T155">замерзнуть-PTCP.PST</ta>
            <ta e="T157" id="Seg_1811" s="T156">шерсть-3SG</ta>
            <ta e="T158" id="Seg_1812" s="T157">огонь-GEN</ta>
            <ta e="T159" id="Seg_1813" s="T158">сторона-LOC</ta>
            <ta e="T160" id="Seg_1814" s="T159">прийти-CO-3SG.S</ta>
            <ta e="T161" id="Seg_1815" s="T160">отец-ACC</ta>
            <ta e="T162" id="Seg_1816" s="T161">ружье-INSTR2</ta>
            <ta e="T163" id="Seg_1817" s="T162">стрелять-3SG.O</ta>
            <ta e="T164" id="Seg_1818" s="T163">медведь-ACC</ta>
            <ta e="T165" id="Seg_1819" s="T164">медведь.[NOM]</ta>
            <ta e="T166" id="Seg_1820" s="T165">упасть-PFV-3SG.S</ta>
            <ta e="T167" id="Seg_1821" s="T166">такой-ADVZ</ta>
            <ta e="T168" id="Seg_1822" s="T167">один-EP-ADVZ</ta>
            <ta e="T169" id="Seg_1823" s="T168">я.NOM</ta>
            <ta e="T170" id="Seg_1824" s="T169">отец-1SG</ta>
            <ta e="T171" id="Seg_1825" s="T170">обмануть-DRV-DUR-PST-3SG.O</ta>
            <ta e="T172" id="Seg_1826" s="T171">убить-DUR-PST-3SG.O</ta>
            <ta e="T173" id="Seg_1827" s="T172">съесть-DUR-PST-3SG.O</ta>
            <ta e="T174" id="Seg_1828" s="T173">медведь-ACC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1829" s="T1">n-n&gt;v-v&gt;ptcp</ta>
            <ta e="T3" id="Seg_1830" s="T2">n.[n:case]</ta>
            <ta e="T4" id="Seg_1831" s="T3">n.[n:case]-n:poss</ta>
            <ta e="T5" id="Seg_1832" s="T4">v-v:tense.[v:pn]</ta>
            <ta e="T6" id="Seg_1833" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_1834" s="T6">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T8" id="Seg_1835" s="T7">n-n&gt;adj</ta>
            <ta e="T9" id="Seg_1836" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_1837" s="T9">n-n&gt;v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T11" id="Seg_1838" s="T10">adj-adj&gt;adv</ta>
            <ta e="T12" id="Seg_1839" s="T11">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_1840" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_1841" s="T13">v-n:ins-v:inf</ta>
            <ta e="T15" id="Seg_1842" s="T14">v-v&gt;v-v&gt;n-n:case</ta>
            <ta e="T16" id="Seg_1843" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_1844" s="T16">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_1845" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_1846" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_1847" s="T19">v-v&gt;n-n:case-n&gt;adj-adj&gt;adv</ta>
            <ta e="T21" id="Seg_1848" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_1849" s="T21">n-n&gt;adj</ta>
            <ta e="T23" id="Seg_1850" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_1851" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_1852" s="T24">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T26" id="Seg_1853" s="T25">n-n:poss</ta>
            <ta e="T27" id="Seg_1854" s="T26">n-n:poss</ta>
            <ta e="T28" id="Seg_1855" s="T27">adj</ta>
            <ta e="T29" id="Seg_1856" s="T28">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T30" id="Seg_1857" s="T29">num-n:ins-adj&gt;adv</ta>
            <ta e="T31" id="Seg_1858" s="T30">n-n:poss</ta>
            <ta e="T32" id="Seg_1859" s="T31">n-n&gt;adj</ta>
            <ta e="T33" id="Seg_1860" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_1861" s="T33">v-v:ins.[v:pn]</ta>
            <ta e="T35" id="Seg_1862" s="T34">num-adj&gt;adv</ta>
            <ta e="T36" id="Seg_1863" s="T35">num-n:ins-adj&gt;adv</ta>
            <ta e="T37" id="Seg_1864" s="T36">v-v:inf</ta>
            <ta e="T38" id="Seg_1865" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_1866" s="T38">n-n-n:case</ta>
            <ta e="T40" id="Seg_1867" s="T39">prep</ta>
            <ta e="T41" id="Seg_1868" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_1869" s="T41">pers.[n:case]</ta>
            <ta e="T43" id="Seg_1870" s="T42">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T44" id="Seg_1871" s="T43">dem</ta>
            <ta e="T45" id="Seg_1872" s="T44">n.[n:case]</ta>
            <ta e="T46" id="Seg_1873" s="T45">v-v:ins-v:pn</ta>
            <ta e="T47" id="Seg_1874" s="T46">num</ta>
            <ta e="T48" id="Seg_1875" s="T47">num</ta>
            <ta e="T49" id="Seg_1876" s="T48">num</ta>
            <ta e="T50" id="Seg_1877" s="T49">adj</ta>
            <ta e="T51" id="Seg_1878" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_1879" s="T51">num</ta>
            <ta e="T53" id="Seg_1880" s="T52">n-n:ins-n:case</ta>
            <ta e="T54" id="Seg_1881" s="T53">num</ta>
            <ta e="T55" id="Seg_1882" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_1883" s="T55">dem</ta>
            <ta e="T57" id="Seg_1884" s="T56">v-v:tense-v:pn</ta>
            <ta e="T58" id="Seg_1885" s="T57">n-adv:case</ta>
            <ta e="T59" id="Seg_1886" s="T58">adj-adj&gt;adv</ta>
            <ta e="T60" id="Seg_1887" s="T59">n-n&gt;v-v:ins.[v:pn]</ta>
            <ta e="T61" id="Seg_1888" s="T60">n-n:poss</ta>
            <ta e="T62" id="Seg_1889" s="T61">v-v:pn</ta>
            <ta e="T63" id="Seg_1890" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_1891" s="T63">v-v:ins-v:pn</ta>
            <ta e="T65" id="Seg_1892" s="T64">v-v:ins-v:pn</ta>
            <ta e="T66" id="Seg_1893" s="T65">n-n:case</ta>
            <ta e="T67" id="Seg_1894" s="T66">v-v:ins-v:pn</ta>
            <ta e="T68" id="Seg_1895" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_1896" s="T68">v-v:ins-v:pn</ta>
            <ta e="T70" id="Seg_1897" s="T69">v-v&gt;v-v:ins.[v:pn]</ta>
            <ta e="T71" id="Seg_1898" s="T70">v-v&gt;ptcp</ta>
            <ta e="T72" id="Seg_1899" s="T71">n-n:num-n:case-n:poss</ta>
            <ta e="T73" id="Seg_1900" s="T72">v-n:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T74" id="Seg_1901" s="T73">emphpro</ta>
            <ta e="T75" id="Seg_1902" s="T74">v-v&gt;v-n:ins-v:ins-v:pn</ta>
            <ta e="T76" id="Seg_1903" s="T75">n-n:case-n:poss</ta>
            <ta e="T77" id="Seg_1904" s="T76">v-v:pn</ta>
            <ta e="T78" id="Seg_1905" s="T77">n-n:num-n:case-n:poss</ta>
            <ta e="T79" id="Seg_1906" s="T78">v-v:pn</ta>
            <ta e="T80" id="Seg_1907" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_1908" s="T80">v-v:ins-v:pn</ta>
            <ta e="T82" id="Seg_1909" s="T81">v-v:ins.[v:pn]</ta>
            <ta e="T83" id="Seg_1910" s="T82">v-v:inf</ta>
            <ta e="T84" id="Seg_1911" s="T83">interrog</ta>
            <ta e="T85" id="Seg_1912" s="T84">v-v:tense.[v:pn]</ta>
            <ta e="T86" id="Seg_1913" s="T85">adv</ta>
            <ta e="T87" id="Seg_1914" s="T86">conj</ta>
            <ta e="T88" id="Seg_1915" s="T87">adj</ta>
            <ta e="T89" id="Seg_1916" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_1917" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_1918" s="T90">pp</ta>
            <ta e="T92" id="Seg_1919" s="T91">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T93" id="Seg_1920" s="T92">v-v:pn</ta>
            <ta e="T94" id="Seg_1921" s="T93">n.[n:case]</ta>
            <ta e="T95" id="Seg_1922" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_1923" s="T95">n-n:case</ta>
            <ta e="T97" id="Seg_1924" s="T96">n-n:case</ta>
            <ta e="T98" id="Seg_1925" s="T97">v-v:pn</ta>
            <ta e="T99" id="Seg_1926" s="T98">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T100" id="Seg_1927" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_1928" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_1929" s="T101">v-v:ins-v:pn</ta>
            <ta e="T103" id="Seg_1930" s="T102">n-n:poss</ta>
            <ta e="T104" id="Seg_1931" s="T103">v-n:ins-v:pn</ta>
            <ta e="T105" id="Seg_1932" s="T104">interrog</ta>
            <ta e="T106" id="Seg_1933" s="T105">n-adv:case</ta>
            <ta e="T107" id="Seg_1934" s="T106">v-v:pn</ta>
            <ta e="T108" id="Seg_1935" s="T107">n-n:case</ta>
            <ta e="T109" id="Seg_1936" s="T108">v-v:ins-v:pn</ta>
            <ta e="T110" id="Seg_1937" s="T109">n-n&gt;adj</ta>
            <ta e="T111" id="Seg_1938" s="T110">n-n:ins-n:case</ta>
            <ta e="T112" id="Seg_1939" s="T111">n-n:case-n:poss</ta>
            <ta e="T113" id="Seg_1940" s="T112">v-v:pn</ta>
            <ta e="T114" id="Seg_1941" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_1942" s="T114">n-n:case</ta>
            <ta e="T116" id="Seg_1943" s="T115">adv</ta>
            <ta e="T117" id="Seg_1944" s="T116">v-v:ins.[v:pn]</ta>
            <ta e="T118" id="Seg_1945" s="T117">conj</ta>
            <ta e="T119" id="Seg_1946" s="T118">adv</ta>
            <ta e="T120" id="Seg_1947" s="T119">v-v:ins.[v:pn]</ta>
            <ta e="T121" id="Seg_1948" s="T120">v-v:tense-v:pn</ta>
            <ta e="T122" id="Seg_1949" s="T121">v-v:pn</ta>
            <ta e="T123" id="Seg_1950" s="T122">n.[n:case]</ta>
            <ta e="T124" id="Seg_1951" s="T123">n-n&gt;v-v&gt;ptcp</ta>
            <ta e="T125" id="Seg_1952" s="T124">n.[n:case]</ta>
            <ta e="T126" id="Seg_1953" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_1954" s="T126">n-n:case</ta>
            <ta e="T128" id="Seg_1955" s="T127">v-v:tense.[v:pn]</ta>
            <ta e="T129" id="Seg_1956" s="T128">emphpro</ta>
            <ta e="T130" id="Seg_1957" s="T129">pro-n:poss</ta>
            <ta e="T131" id="Seg_1958" s="T130">n-n:poss</ta>
            <ta e="T132" id="Seg_1959" s="T131">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T133" id="Seg_1960" s="T132">conj</ta>
            <ta e="T134" id="Seg_1961" s="T133">v-v:ins-v:pn</ta>
            <ta e="T135" id="Seg_1962" s="T134">n-n:case-n&gt;adj</ta>
            <ta e="T136" id="Seg_1963" s="T135">n-n:case</ta>
            <ta e="T137" id="Seg_1964" s="T136">n-n:case</ta>
            <ta e="T138" id="Seg_1965" s="T137">v-n:ins-v:inf</ta>
            <ta e="T139" id="Seg_1966" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_1967" s="T139">n-n:poss</ta>
            <ta e="T141" id="Seg_1968" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_1969" s="T141">quant</ta>
            <ta e="T143" id="Seg_1970" s="T142">v-v:ins-v:pn</ta>
            <ta e="T144" id="Seg_1971" s="T143">n-n:case</ta>
            <ta e="T145" id="Seg_1972" s="T144">n.[n:case]</ta>
            <ta e="T146" id="Seg_1973" s="T145">v-n:ins-v:pn</ta>
            <ta e="T147" id="Seg_1974" s="T146">adj-adj&gt;adv</ta>
            <ta e="T148" id="Seg_1975" s="T147">n.[n:case]</ta>
            <ta e="T149" id="Seg_1976" s="T148">n-n:case-n:obl.poss</ta>
            <ta e="T150" id="Seg_1977" s="T149">n-n:case</ta>
            <ta e="T151" id="Seg_1978" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_1979" s="T151">adj-adj&gt;adv</ta>
            <ta e="T153" id="Seg_1980" s="T152">v-v&gt;v-v&gt;adv</ta>
            <ta e="T154" id="Seg_1981" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_1982" s="T154">n-adj&gt;adv</ta>
            <ta e="T156" id="Seg_1983" s="T155">v-v&gt;ptcp</ta>
            <ta e="T157" id="Seg_1984" s="T156">n-n:poss</ta>
            <ta e="T158" id="Seg_1985" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_1986" s="T158">n-n:case</ta>
            <ta e="T160" id="Seg_1987" s="T159">v-v:ins-v:pn</ta>
            <ta e="T161" id="Seg_1988" s="T160">n-n:case</ta>
            <ta e="T162" id="Seg_1989" s="T161">n-n:case</ta>
            <ta e="T163" id="Seg_1990" s="T162">v-v:pn</ta>
            <ta e="T164" id="Seg_1991" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_1992" s="T164">n.[n:case]</ta>
            <ta e="T166" id="Seg_1993" s="T165">v-v&gt;v-v:pn</ta>
            <ta e="T167" id="Seg_1994" s="T166">dem-adj&gt;adv</ta>
            <ta e="T168" id="Seg_1995" s="T167">num-n:ins-adj&gt;adv</ta>
            <ta e="T169" id="Seg_1996" s="T168">pers</ta>
            <ta e="T170" id="Seg_1997" s="T169">n-n:poss</ta>
            <ta e="T171" id="Seg_1998" s="T170">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T172" id="Seg_1999" s="T171">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T173" id="Seg_2000" s="T172">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T174" id="Seg_2001" s="T173">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_2002" s="T1">ptcp</ta>
            <ta e="T3" id="Seg_2003" s="T2">n</ta>
            <ta e="T4" id="Seg_2004" s="T3">n</ta>
            <ta e="T5" id="Seg_2005" s="T4">v</ta>
            <ta e="T6" id="Seg_2006" s="T5">n</ta>
            <ta e="T7" id="Seg_2007" s="T6">v</ta>
            <ta e="T8" id="Seg_2008" s="T7">adj</ta>
            <ta e="T9" id="Seg_2009" s="T8">n</ta>
            <ta e="T10" id="Seg_2010" s="T9">v</ta>
            <ta e="T11" id="Seg_2011" s="T10">adv</ta>
            <ta e="T12" id="Seg_2012" s="T11">v</ta>
            <ta e="T13" id="Seg_2013" s="T12">n</ta>
            <ta e="T14" id="Seg_2014" s="T13">v</ta>
            <ta e="T15" id="Seg_2015" s="T14">n</ta>
            <ta e="T16" id="Seg_2016" s="T15">n</ta>
            <ta e="T17" id="Seg_2017" s="T16">v</ta>
            <ta e="T18" id="Seg_2018" s="T17">n</ta>
            <ta e="T19" id="Seg_2019" s="T18">n</ta>
            <ta e="T20" id="Seg_2020" s="T19">adv</ta>
            <ta e="T21" id="Seg_2021" s="T20">n</ta>
            <ta e="T22" id="Seg_2022" s="T21">adj</ta>
            <ta e="T23" id="Seg_2023" s="T22">n</ta>
            <ta e="T24" id="Seg_2024" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_2025" s="T24">v</ta>
            <ta e="T26" id="Seg_2026" s="T25">n</ta>
            <ta e="T27" id="Seg_2027" s="T26">n</ta>
            <ta e="T28" id="Seg_2028" s="T27">adj</ta>
            <ta e="T29" id="Seg_2029" s="T28">v</ta>
            <ta e="T30" id="Seg_2030" s="T29">adv</ta>
            <ta e="T31" id="Seg_2031" s="T30">n</ta>
            <ta e="T32" id="Seg_2032" s="T31">adj</ta>
            <ta e="T33" id="Seg_2033" s="T32">n</ta>
            <ta e="T34" id="Seg_2034" s="T33">v</ta>
            <ta e="T35" id="Seg_2035" s="T34">adv</ta>
            <ta e="T36" id="Seg_2036" s="T35">adv</ta>
            <ta e="T37" id="Seg_2037" s="T36">v</ta>
            <ta e="T38" id="Seg_2038" s="T37">n</ta>
            <ta e="T39" id="Seg_2039" s="T38">n</ta>
            <ta e="T40" id="Seg_2040" s="T39">prep</ta>
            <ta e="T41" id="Seg_2041" s="T40">n</ta>
            <ta e="T42" id="Seg_2042" s="T41">pers</ta>
            <ta e="T43" id="Seg_2043" s="T42">v</ta>
            <ta e="T44" id="Seg_2044" s="T43">dem</ta>
            <ta e="T45" id="Seg_2045" s="T44">n</ta>
            <ta e="T46" id="Seg_2046" s="T45">v</ta>
            <ta e="T47" id="Seg_2047" s="T46">num</ta>
            <ta e="T48" id="Seg_2048" s="T47">num</ta>
            <ta e="T49" id="Seg_2049" s="T48">num</ta>
            <ta e="T50" id="Seg_2050" s="T49">adj</ta>
            <ta e="T51" id="Seg_2051" s="T50">n</ta>
            <ta e="T52" id="Seg_2052" s="T51">num</ta>
            <ta e="T53" id="Seg_2053" s="T52">n</ta>
            <ta e="T54" id="Seg_2054" s="T53">num</ta>
            <ta e="T55" id="Seg_2055" s="T54">n</ta>
            <ta e="T56" id="Seg_2056" s="T55">dem</ta>
            <ta e="T57" id="Seg_2057" s="T56">v</ta>
            <ta e="T58" id="Seg_2058" s="T57">adv</ta>
            <ta e="T59" id="Seg_2059" s="T58">adj</ta>
            <ta e="T60" id="Seg_2060" s="T59">n</ta>
            <ta e="T61" id="Seg_2061" s="T60">n</ta>
            <ta e="T62" id="Seg_2062" s="T61">v</ta>
            <ta e="T63" id="Seg_2063" s="T62">n</ta>
            <ta e="T64" id="Seg_2064" s="T63">v</ta>
            <ta e="T65" id="Seg_2065" s="T64">v</ta>
            <ta e="T66" id="Seg_2066" s="T65">n</ta>
            <ta e="T67" id="Seg_2067" s="T66">v</ta>
            <ta e="T68" id="Seg_2068" s="T67">n</ta>
            <ta e="T69" id="Seg_2069" s="T68">v</ta>
            <ta e="T70" id="Seg_2070" s="T69">v</ta>
            <ta e="T71" id="Seg_2071" s="T70">ptcp</ta>
            <ta e="T72" id="Seg_2072" s="T71">n</ta>
            <ta e="T73" id="Seg_2073" s="T72">v</ta>
            <ta e="T74" id="Seg_2074" s="T73">emphpro</ta>
            <ta e="T75" id="Seg_2075" s="T74">v</ta>
            <ta e="T76" id="Seg_2076" s="T75">n</ta>
            <ta e="T77" id="Seg_2077" s="T76">v</ta>
            <ta e="T78" id="Seg_2078" s="T77">n</ta>
            <ta e="T79" id="Seg_2079" s="T78">v</ta>
            <ta e="T80" id="Seg_2080" s="T79">n</ta>
            <ta e="T81" id="Seg_2081" s="T80">v</ta>
            <ta e="T82" id="Seg_2082" s="T81">v</ta>
            <ta e="T83" id="Seg_2083" s="T82">v</ta>
            <ta e="T84" id="Seg_2084" s="T83">interrog</ta>
            <ta e="T85" id="Seg_2085" s="T84">v</ta>
            <ta e="T86" id="Seg_2086" s="T85">adj</ta>
            <ta e="T87" id="Seg_2087" s="T86">conj</ta>
            <ta e="T88" id="Seg_2088" s="T87">adj</ta>
            <ta e="T89" id="Seg_2089" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_2090" s="T89">n</ta>
            <ta e="T91" id="Seg_2091" s="T90">pp</ta>
            <ta e="T92" id="Seg_2092" s="T91">v</ta>
            <ta e="T93" id="Seg_2093" s="T92">v</ta>
            <ta e="T94" id="Seg_2094" s="T93">n</ta>
            <ta e="T95" id="Seg_2095" s="T94">n</ta>
            <ta e="T96" id="Seg_2096" s="T95">n</ta>
            <ta e="T97" id="Seg_2097" s="T96">n</ta>
            <ta e="T98" id="Seg_2098" s="T97">v</ta>
            <ta e="T99" id="Seg_2099" s="T98">v</ta>
            <ta e="T100" id="Seg_2100" s="T99">n</ta>
            <ta e="T101" id="Seg_2101" s="T100">n</ta>
            <ta e="T102" id="Seg_2102" s="T101">v</ta>
            <ta e="T103" id="Seg_2103" s="T102">n</ta>
            <ta e="T104" id="Seg_2104" s="T103">v</ta>
            <ta e="T105" id="Seg_2105" s="T104">interrog</ta>
            <ta e="T106" id="Seg_2106" s="T105">n</ta>
            <ta e="T107" id="Seg_2107" s="T106">v</ta>
            <ta e="T108" id="Seg_2108" s="T107">n</ta>
            <ta e="T109" id="Seg_2109" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_2110" s="T109">adj</ta>
            <ta e="T111" id="Seg_2111" s="T110">n</ta>
            <ta e="T112" id="Seg_2112" s="T111">n</ta>
            <ta e="T113" id="Seg_2113" s="T112">v</ta>
            <ta e="T114" id="Seg_2114" s="T113">n</ta>
            <ta e="T115" id="Seg_2115" s="T114">n</ta>
            <ta e="T116" id="Seg_2116" s="T115">adv</ta>
            <ta e="T117" id="Seg_2117" s="T116">v</ta>
            <ta e="T118" id="Seg_2118" s="T117">conj</ta>
            <ta e="T119" id="Seg_2119" s="T118">adv</ta>
            <ta e="T120" id="Seg_2120" s="T119">v</ta>
            <ta e="T121" id="Seg_2121" s="T120">v</ta>
            <ta e="T122" id="Seg_2122" s="T121">v</ta>
            <ta e="T123" id="Seg_2123" s="T122">n</ta>
            <ta e="T124" id="Seg_2124" s="T123">ptcp</ta>
            <ta e="T125" id="Seg_2125" s="T124">n</ta>
            <ta e="T126" id="Seg_2126" s="T125">n</ta>
            <ta e="T127" id="Seg_2127" s="T126">n</ta>
            <ta e="T128" id="Seg_2128" s="T127">v</ta>
            <ta e="T129" id="Seg_2129" s="T128">emphpro</ta>
            <ta e="T130" id="Seg_2130" s="T129">pro</ta>
            <ta e="T131" id="Seg_2131" s="T130">n</ta>
            <ta e="T132" id="Seg_2132" s="T131">v</ta>
            <ta e="T133" id="Seg_2133" s="T132">conj</ta>
            <ta e="T134" id="Seg_2134" s="T133">v</ta>
            <ta e="T135" id="Seg_2135" s="T134">n</ta>
            <ta e="T136" id="Seg_2136" s="T135">n</ta>
            <ta e="T137" id="Seg_2137" s="T136">n</ta>
            <ta e="T138" id="Seg_2138" s="T137">v</ta>
            <ta e="T139" id="Seg_2139" s="T138">n</ta>
            <ta e="T140" id="Seg_2140" s="T139">n</ta>
            <ta e="T141" id="Seg_2141" s="T140">v</ta>
            <ta e="T142" id="Seg_2142" s="T141">quant</ta>
            <ta e="T143" id="Seg_2143" s="T142">v</ta>
            <ta e="T144" id="Seg_2144" s="T143">n</ta>
            <ta e="T145" id="Seg_2145" s="T144">n</ta>
            <ta e="T146" id="Seg_2146" s="T145">v</ta>
            <ta e="T147" id="Seg_2147" s="T146">adj</ta>
            <ta e="T148" id="Seg_2148" s="T147">n</ta>
            <ta e="T149" id="Seg_2149" s="T148">n</ta>
            <ta e="T150" id="Seg_2150" s="T149">n</ta>
            <ta e="T151" id="Seg_2151" s="T150">n</ta>
            <ta e="T152" id="Seg_2152" s="T151">adv</ta>
            <ta e="T153" id="Seg_2153" s="T152">adv</ta>
            <ta e="T154" id="Seg_2154" s="T153">n</ta>
            <ta e="T155" id="Seg_2155" s="T154">adv</ta>
            <ta e="T156" id="Seg_2156" s="T155">ptcp</ta>
            <ta e="T157" id="Seg_2157" s="T156">n</ta>
            <ta e="T158" id="Seg_2158" s="T157">n</ta>
            <ta e="T159" id="Seg_2159" s="T158">n</ta>
            <ta e="T160" id="Seg_2160" s="T159">v</ta>
            <ta e="T161" id="Seg_2161" s="T160">n</ta>
            <ta e="T162" id="Seg_2162" s="T161">n</ta>
            <ta e="T163" id="Seg_2163" s="T162">v</ta>
            <ta e="T164" id="Seg_2164" s="T163">n</ta>
            <ta e="T165" id="Seg_2165" s="T164">n</ta>
            <ta e="T166" id="Seg_2166" s="T165">v</ta>
            <ta e="T167" id="Seg_2167" s="T166">adv</ta>
            <ta e="T168" id="Seg_2168" s="T167">adv</ta>
            <ta e="T169" id="Seg_2169" s="T168">pers</ta>
            <ta e="T170" id="Seg_2170" s="T169">n</ta>
            <ta e="T171" id="Seg_2171" s="T170">v</ta>
            <ta e="T172" id="Seg_2172" s="T171">v</ta>
            <ta e="T173" id="Seg_2173" s="T172">v</ta>
            <ta e="T174" id="Seg_2174" s="T173">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T4" id="Seg_2175" s="T3">np.h:A 0.1.h:Poss</ta>
            <ta e="T6" id="Seg_2176" s="T5">np:G</ta>
            <ta e="T7" id="Seg_2177" s="T6">0.3.h:Th</ta>
            <ta e="T9" id="Seg_2178" s="T8">np:L</ta>
            <ta e="T10" id="Seg_2179" s="T9">0.3.h:A</ta>
            <ta e="T12" id="Seg_2180" s="T11">0.3.h:A</ta>
            <ta e="T13" id="Seg_2181" s="T12">np:P</ta>
            <ta e="T14" id="Seg_2182" s="T13">0.3.h:A</ta>
            <ta e="T16" id="Seg_2183" s="T15">np:L</ta>
            <ta e="T17" id="Seg_2184" s="T16">0.3.h:A</ta>
            <ta e="T18" id="Seg_2185" s="T17">np:P</ta>
            <ta e="T19" id="Seg_2186" s="T18">np:P</ta>
            <ta e="T21" id="Seg_2187" s="T20">np:L</ta>
            <ta e="T23" id="Seg_2188" s="T22">np:L</ta>
            <ta e="T25" id="Seg_2189" s="T24">0.3.h:Th</ta>
            <ta e="T26" id="Seg_2190" s="T25">np:Th 0.3.h:Poss</ta>
            <ta e="T27" id="Seg_2191" s="T26">np:Th 0.3.h:Poss</ta>
            <ta e="T30" id="Seg_2192" s="T29">adv:Time</ta>
            <ta e="T31" id="Seg_2193" s="T30">np.h:A 0.1.h:Poss</ta>
            <ta e="T33" id="Seg_2194" s="T32">np:So</ta>
            <ta e="T37" id="Seg_2195" s="T36">0.3.h:Th</ta>
            <ta e="T38" id="Seg_2196" s="T37">np:L</ta>
            <ta e="T39" id="Seg_2197" s="T38">np:Time</ta>
            <ta e="T41" id="Seg_2198" s="T40">pp:Time</ta>
            <ta e="T42" id="Seg_2199" s="T41">pro.h:A</ta>
            <ta e="T45" id="Seg_2200" s="T44">np:Time</ta>
            <ta e="T46" id="Seg_2201" s="T45">0.3.h:A</ta>
            <ta e="T51" id="Seg_2202" s="T50">np:P</ta>
            <ta e="T53" id="Seg_2203" s="T52">np:P</ta>
            <ta e="T55" id="Seg_2204" s="T54">np:P</ta>
            <ta e="T56" id="Seg_2205" s="T55">pro:Th</ta>
            <ta e="T58" id="Seg_2206" s="T57">adv:Time</ta>
            <ta e="T60" id="Seg_2207" s="T59">0.3:Th</ta>
            <ta e="T61" id="Seg_2208" s="T60">np.h:A 0.1.h:Poss</ta>
            <ta e="T64" id="Seg_2209" s="T63">0.3.h:A</ta>
            <ta e="T65" id="Seg_2210" s="T64">0.3.h:A 0.3:P</ta>
            <ta e="T66" id="Seg_2211" s="T65">np:P</ta>
            <ta e="T67" id="Seg_2212" s="T66">0.3.h:A</ta>
            <ta e="T68" id="Seg_2213" s="T67">np:Th</ta>
            <ta e="T69" id="Seg_2214" s="T68">0.3.h:A</ta>
            <ta e="T70" id="Seg_2215" s="T69">0.3.h:A</ta>
            <ta e="T72" id="Seg_2216" s="T71">np:P 0.3.h:Poss</ta>
            <ta e="T73" id="Seg_2217" s="T72">0.3.h:A</ta>
            <ta e="T75" id="Seg_2218" s="T74">0.3.h:A</ta>
            <ta e="T76" id="Seg_2219" s="T75">np:Th 0.3.h:Poss</ta>
            <ta e="T77" id="Seg_2220" s="T76">0.3.h:A</ta>
            <ta e="T78" id="Seg_2221" s="T77">np:Th 0.3.h:Poss</ta>
            <ta e="T79" id="Seg_2222" s="T78">0.3.h:A</ta>
            <ta e="T80" id="Seg_2223" s="T79">np:P</ta>
            <ta e="T81" id="Seg_2224" s="T80">0.3.h:A</ta>
            <ta e="T82" id="Seg_2225" s="T81">0.3.h:A</ta>
            <ta e="T85" id="Seg_2226" s="T84">0.3.h:P</ta>
            <ta e="T90" id="Seg_2227" s="T89">pp:Path</ta>
            <ta e="T92" id="Seg_2228" s="T91">0.3.h:E 0.3:Th</ta>
            <ta e="T93" id="Seg_2229" s="T92">0.3.h:E</ta>
            <ta e="T94" id="Seg_2230" s="T93">np:Poss</ta>
            <ta e="T95" id="Seg_2231" s="T94">np:Th</ta>
            <ta e="T96" id="Seg_2232" s="T95">np:Poss</ta>
            <ta e="T97" id="Seg_2233" s="T96">np:Cau</ta>
            <ta e="T98" id="Seg_2234" s="T97">0.3.h:P</ta>
            <ta e="T99" id="Seg_2235" s="T98">0.3.h:A</ta>
            <ta e="T100" id="Seg_2236" s="T99">np:G</ta>
            <ta e="T101" id="Seg_2237" s="T100">np:Th</ta>
            <ta e="T102" id="Seg_2238" s="T101">0.3.h:A</ta>
            <ta e="T103" id="Seg_2239" s="T102">np:P</ta>
            <ta e="T107" id="Seg_2240" s="T106">0.3:Th</ta>
            <ta e="T108" id="Seg_2241" s="T107">np:Th</ta>
            <ta e="T109" id="Seg_2242" s="T108">0.3.h:A</ta>
            <ta e="T111" id="Seg_2243" s="T110">np:Poss</ta>
            <ta e="T112" id="Seg_2244" s="T111">np:Th 0.3.h:Poss</ta>
            <ta e="T113" id="Seg_2245" s="T112">0.3.h:A</ta>
            <ta e="T114" id="Seg_2246" s="T113">np:Poss</ta>
            <ta e="T115" id="Seg_2247" s="T114">np:So</ta>
            <ta e="T116" id="Seg_2248" s="T115">adv:G</ta>
            <ta e="T117" id="Seg_2249" s="T116">0.3.h:A</ta>
            <ta e="T119" id="Seg_2250" s="T118">adv:Time</ta>
            <ta e="T120" id="Seg_2251" s="T119">0.3.h:Th</ta>
            <ta e="T121" id="Seg_2252" s="T120">0.3.h:A</ta>
            <ta e="T123" id="Seg_2253" s="T122">np:A</ta>
            <ta e="T125" id="Seg_2254" s="T124">np:A</ta>
            <ta e="T126" id="Seg_2255" s="T125">np:G</ta>
            <ta e="T127" id="Seg_2256" s="T126">np:G</ta>
            <ta e="T131" id="Seg_2257" s="T130">np:P 0.3:Poss</ta>
            <ta e="T132" id="Seg_2258" s="T131">0.3:A</ta>
            <ta e="T134" id="Seg_2259" s="T133">0.3:A</ta>
            <ta e="T135" id="Seg_2260" s="T134">np.h:Poss</ta>
            <ta e="T136" id="Seg_2261" s="T135">np:Poss</ta>
            <ta e="T137" id="Seg_2262" s="T136">np:G</ta>
            <ta e="T138" id="Seg_2263" s="T137">0.3:A</ta>
            <ta e="T139" id="Seg_2264" s="T138">np:P</ta>
            <ta e="T140" id="Seg_2265" s="T139">np.h:A 0.1.h:Poss</ta>
            <ta e="T141" id="Seg_2266" s="T140">np:G</ta>
            <ta e="T144" id="Seg_2267" s="T143">np:Th</ta>
            <ta e="T145" id="Seg_2268" s="T144">np:P</ta>
            <ta e="T148" id="Seg_2269" s="T147">np:A</ta>
            <ta e="T149" id="Seg_2270" s="T148">np.h:Poss</ta>
            <ta e="T150" id="Seg_2271" s="T149">np:L</ta>
            <ta e="T157" id="Seg_2272" s="T156">np:Th 0.3:Poss</ta>
            <ta e="T158" id="Seg_2273" s="T157">np:Poss</ta>
            <ta e="T159" id="Seg_2274" s="T158">np:L</ta>
            <ta e="T161" id="Seg_2275" s="T160">np.h:A</ta>
            <ta e="T162" id="Seg_2276" s="T161">np:Ins</ta>
            <ta e="T164" id="Seg_2277" s="T163">np:P</ta>
            <ta e="T165" id="Seg_2278" s="T164">np:P</ta>
            <ta e="T168" id="Seg_2279" s="T167">adv:Time</ta>
            <ta e="T169" id="Seg_2280" s="T168">pro.h:Poss</ta>
            <ta e="T170" id="Seg_2281" s="T169">np.h:A</ta>
            <ta e="T174" id="Seg_2282" s="T173">np:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_2283" s="T3">np.h:S</ta>
            <ta e="T5" id="Seg_2284" s="T4">v:pred</ta>
            <ta e="T7" id="Seg_2285" s="T6">0.3.h:S v:pred</ta>
            <ta e="T10" id="Seg_2286" s="T9">0.3.h:S v:pred</ta>
            <ta e="T12" id="Seg_2287" s="T11">0.3.h:S v:pred</ta>
            <ta e="T14" id="Seg_2288" s="T12">s:purp</ta>
            <ta e="T16" id="Seg_2289" s="T14">s:temp</ta>
            <ta e="T17" id="Seg_2290" s="T16">0.3.h:S v:pred</ta>
            <ta e="T18" id="Seg_2291" s="T17">np:O</ta>
            <ta e="T19" id="Seg_2292" s="T18">np:O</ta>
            <ta e="T25" id="Seg_2293" s="T24">0.3.h:S v:pred</ta>
            <ta e="T26" id="Seg_2294" s="T25">np:S</ta>
            <ta e="T27" id="Seg_2295" s="T26">np:S</ta>
            <ta e="T28" id="Seg_2296" s="T27">adj:pred</ta>
            <ta e="T29" id="Seg_2297" s="T28">cop</ta>
            <ta e="T31" id="Seg_2298" s="T30">np.h:S</ta>
            <ta e="T34" id="Seg_2299" s="T33">v:pred</ta>
            <ta e="T38" id="Seg_2300" s="T34">s:purp</ta>
            <ta e="T42" id="Seg_2301" s="T41">pro.h:S</ta>
            <ta e="T43" id="Seg_2302" s="T42">v:pred</ta>
            <ta e="T46" id="Seg_2303" s="T45">0.3.h:S v:pred</ta>
            <ta e="T51" id="Seg_2304" s="T50">np:O</ta>
            <ta e="T53" id="Seg_2305" s="T52">np:O</ta>
            <ta e="T55" id="Seg_2306" s="T54">np:O</ta>
            <ta e="T56" id="Seg_2307" s="T55">pro:S</ta>
            <ta e="T57" id="Seg_2308" s="T56">v:pred</ta>
            <ta e="T60" id="Seg_2309" s="T59">0.3:S v:pred</ta>
            <ta e="T61" id="Seg_2310" s="T60">np.h:S</ta>
            <ta e="T62" id="Seg_2311" s="T61">v:pred</ta>
            <ta e="T63" id="Seg_2312" s="T62">np:O</ta>
            <ta e="T64" id="Seg_2313" s="T63">0.3.h:S v:pred</ta>
            <ta e="T65" id="Seg_2314" s="T64">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T66" id="Seg_2315" s="T65">np:O</ta>
            <ta e="T67" id="Seg_2316" s="T66">0.3.h:S v:pred</ta>
            <ta e="T68" id="Seg_2317" s="T67">np:O</ta>
            <ta e="T69" id="Seg_2318" s="T68">0.3.h:S v:pred</ta>
            <ta e="T70" id="Seg_2319" s="T69">0.3.h:S v:pred</ta>
            <ta e="T72" id="Seg_2320" s="T71">np:O</ta>
            <ta e="T73" id="Seg_2321" s="T72">0.3.h:S v:pred</ta>
            <ta e="T75" id="Seg_2322" s="T74">0.3.h:S v:pred</ta>
            <ta e="T76" id="Seg_2323" s="T75">np:O</ta>
            <ta e="T77" id="Seg_2324" s="T76">0.3.h:S v:pred</ta>
            <ta e="T78" id="Seg_2325" s="T77">np:O</ta>
            <ta e="T79" id="Seg_2326" s="T78">0.3.h:S v:pred</ta>
            <ta e="T80" id="Seg_2327" s="T79">np:O</ta>
            <ta e="T81" id="Seg_2328" s="T80">0.3.h:S v:pred</ta>
            <ta e="T82" id="Seg_2329" s="T81">0.3.h:S v:pred</ta>
            <ta e="T83" id="Seg_2330" s="T82">s:purp</ta>
            <ta e="T85" id="Seg_2331" s="T84">0.3.h:S v:pred</ta>
            <ta e="T92" id="Seg_2332" s="T91">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T93" id="Seg_2333" s="T92">0.3.h:S v:pred</ta>
            <ta e="T95" id="Seg_2334" s="T94">np:O</ta>
            <ta e="T98" id="Seg_2335" s="T97">0.3.h:S v:pred</ta>
            <ta e="T99" id="Seg_2336" s="T98">0.3.h:S v:pred</ta>
            <ta e="T101" id="Seg_2337" s="T100">np:O</ta>
            <ta e="T102" id="Seg_2338" s="T101">0.3.h:S v:pred</ta>
            <ta e="T103" id="Seg_2339" s="T102">np:S</ta>
            <ta e="T104" id="Seg_2340" s="T103">v:pred</ta>
            <ta e="T107" id="Seg_2341" s="T106">0.3:S v:pred</ta>
            <ta e="T108" id="Seg_2342" s="T107">np:O</ta>
            <ta e="T109" id="Seg_2343" s="T108">0.3.h:S v:pred</ta>
            <ta e="T112" id="Seg_2344" s="T111">np:O</ta>
            <ta e="T113" id="Seg_2345" s="T112">0.3.h:S v:pred</ta>
            <ta e="T117" id="Seg_2346" s="T116">0.3.h:S v:pred</ta>
            <ta e="T120" id="Seg_2347" s="T119">0.3.h:S v:pred</ta>
            <ta e="T121" id="Seg_2348" s="T120">0.3.h:S v:pred</ta>
            <ta e="T122" id="Seg_2349" s="T121">v:pred</ta>
            <ta e="T123" id="Seg_2350" s="T122">np:S</ta>
            <ta e="T125" id="Seg_2351" s="T124">np:S</ta>
            <ta e="T128" id="Seg_2352" s="T127">v:pred</ta>
            <ta e="T131" id="Seg_2353" s="T130">np:O</ta>
            <ta e="T132" id="Seg_2354" s="T131">0.3:S v:pred</ta>
            <ta e="T134" id="Seg_2355" s="T133">0.3:S v:pred</ta>
            <ta e="T139" id="Seg_2356" s="T137">s:purp</ta>
            <ta e="T140" id="Seg_2357" s="T139">np.h:S</ta>
            <ta e="T143" id="Seg_2358" s="T142">v:pred</ta>
            <ta e="T144" id="Seg_2359" s="T143">np:O</ta>
            <ta e="T145" id="Seg_2360" s="T144">np:S</ta>
            <ta e="T146" id="Seg_2361" s="T145">v:pred</ta>
            <ta e="T153" id="Seg_2362" s="T147">s:temp</ta>
            <ta e="T157" id="Seg_2363" s="T156">np:S</ta>
            <ta e="T160" id="Seg_2364" s="T159">v:pred</ta>
            <ta e="T161" id="Seg_2365" s="T160">np.h:S</ta>
            <ta e="T163" id="Seg_2366" s="T162">v:pred</ta>
            <ta e="T164" id="Seg_2367" s="T163">np:O</ta>
            <ta e="T165" id="Seg_2368" s="T164">np:S</ta>
            <ta e="T166" id="Seg_2369" s="T165">v:pred</ta>
            <ta e="T170" id="Seg_2370" s="T169">np.h:S</ta>
            <ta e="T171" id="Seg_2371" s="T170">v:pred</ta>
            <ta e="T172" id="Seg_2372" s="T171">v:pred </ta>
            <ta e="T173" id="Seg_2373" s="T172">v:pred</ta>
            <ta e="T174" id="Seg_2374" s="T173">np:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T40" id="Seg_2375" s="T39">RUS:gram</ta>
            <ta e="T78" id="Seg_2376" s="T77">RUS:cult</ta>
            <ta e="T87" id="Seg_2377" s="T86">RUS:gram</ta>
            <ta e="T110" id="Seg_2378" s="T109">RUS:cult</ta>
            <ta e="T111" id="Seg_2379" s="T110">RUS:cult</ta>
            <ta e="T118" id="Seg_2380" s="T117">RUS:gram</ta>
            <ta e="T133" id="Seg_2381" s="T132">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_2382" s="T1">A frozen bear.</ta>
            <ta e="T6" id="Seg_2383" s="T3">My father went to the taiga.</ta>
            <ta e="T9" id="Seg_2384" s="T6">He lived in a hunting lodge.</ta>
            <ta e="T10" id="Seg_2385" s="T9">He huntes squirrels.</ta>
            <ta e="T14" id="Seg_2386" s="T10">He hunted well to eat squirrels.</ta>
            <ta e="T19" id="Seg_2387" s="T14">When he was hunting in the taiga, he shot wood grooses and hazel-hen.</ta>
            <ta e="T25" id="Seg_2388" s="T19">He didn't use to be in the hunting lodge without food.</ta>
            <ta e="T29" id="Seg_2389" s="T25">His dog and his rifle were good.</ta>
            <ta e="T38" id="Seg_2390" s="T29">Once upon a time my father left the hunting lodge to spend two or three nights in another lodge.</ta>
            <ta e="T43" id="Seg_2391" s="T38">He was walking from morning to night.</ta>
            <ta e="T55" id="Seg_2392" s="T43">On this day he caught thirty five squirrels, one Siberian weasel and one sable.</ta>
            <ta e="T58" id="Seg_2393" s="T55">It was in autumn.</ta>
            <ta e="T60" id="Seg_2394" s="T58">It got dark.</ta>
            <ta e="T62" id="Seg_2395" s="T60">He stopped.</ta>
            <ta e="T70" id="Seg_2396" s="T62">He (?), chopped wood, made a shelter, brought wood and made dinner(?).</ta>
            <ta e="T73" id="Seg_2397" s="T70">He skinned the killed animals.</ta>
            <ta e="T77" id="Seg_2398" s="T73">He ate himself and he fed his dog.</ta>
            <ta e="T79" id="Seg_2399" s="T77">He charged (his gun) with cartridges.</ta>
            <ta e="T83" id="Seg_2400" s="T79">He made a bed and went to sleep.</ta>
            <ta e="T85" id="Seg_2401" s="T83">Once he fell asleep.</ta>
            <ta e="T92" id="Seg_2402" s="T85">After some time he saw (something) through his dream.</ta>
            <ta e="T95" id="Seg_2403" s="T92">He heard the dog barking.</ta>
            <ta e="T98" id="Seg_2404" s="T95">He woke up from this barking.</ta>
            <ta e="T102" id="Seg_2405" s="T98">He rose up and put wood into fire.</ta>
            <ta e="T107" id="Seg_2406" s="T102">The fire flamed up – it became like a day.</ta>
            <ta e="T113" id="Seg_2407" s="T107">He took his rifle and charged it with bullets.</ta>
            <ta e="T120" id="Seg_2408" s="T113">He went aside from the fire and stopped there.</ta>
            <ta e="T124" id="Seg_2409" s="T120">He looked, a bear covered with ice is coming.</ta>
            <ta e="T132" id="Seg_2410" s="T124">(The bear) himself had frozen his fur by rolling in water, in snow.</ta>
            <ta e="T139" id="Seg_2411" s="T132">He came to the father's fire to eat him.</ta>
            <ta e="T144" id="Seg_2412" s="T139">My father had put a lot of wood into the fire.</ta>
            <ta e="T147" id="Seg_2413" s="T144">The fire burned well.</ta>
            <ta e="T160" id="Seg_2414" s="T147">While the bear was running behind my father round the fire, his iced fur melted near the fire.</ta>
            <ta e="T164" id="Seg_2415" s="T160">My father shot the bear from his rifle.</ta>
            <ta e="T166" id="Seg_2416" s="T164">The bear fell down.</ta>
            <ta e="T174" id="Seg_2417" s="T166">That's how my father once cheated, killed and ate a bear.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_2418" s="T1">Der erfrorene Bär. </ta>
            <ta e="T6" id="Seg_2419" s="T3">Mein Vater ging in die Taiga.</ta>
            <ta e="T9" id="Seg_2420" s="T6">Er lebte in der Jagdhütte</ta>
            <ta e="T10" id="Seg_2421" s="T9">Er jagte Eichhörnchen.</ta>
            <ta e="T14" id="Seg_2422" s="T10">Er jagte gut, um Eichhörnchen zu essen.</ta>
            <ta e="T19" id="Seg_2423" s="T14">Als er in der Taiga jagte, schoß er Auerhähne und Moorhühner.</ta>
            <ta e="T25" id="Seg_2424" s="T19">Er war nicht lange in der Jagdhütte ohne Essen.</ta>
            <ta e="T29" id="Seg_2425" s="T25">Sein Hund und seine Waffe waren gut.</ta>
            <ta e="T38" id="Seg_2426" s="T29">Einmal verließ mein Vater die Jagdhütte um zwei, drei Nächte in einem anderen Hütte zu verbringen.</ta>
            <ta e="T43" id="Seg_2427" s="T38">Er ging von morgens bis abends.</ta>
            <ta e="T55" id="Seg_2428" s="T43">An diesem Tag hat er mehr als 35 Eichhörnchen, ein Wiesel und einen Zobel (erlegt). </ta>
            <ta e="T58" id="Seg_2429" s="T55">Das war im Herbst.</ta>
            <ta e="T60" id="Seg_2430" s="T58">Es wurde dunkel.</ta>
            <ta e="T62" id="Seg_2431" s="T60">Mein Vater hielt an.</ta>
            <ta e="T70" id="Seg_2432" s="T62">Er hackte Holz, baute sich einen Unterschlupf, brachte Holz und machte Essen(?).</ta>
            <ta e="T73" id="Seg_2433" s="T70">Er häutete die getöteten Tiere.</ta>
            <ta e="T77" id="Seg_2434" s="T73">Er aß selbst und fütterte seinen Hund.</ta>
            <ta e="T79" id="Seg_2435" s="T77">Er hat die Munition geladen.</ta>
            <ta e="T83" id="Seg_2436" s="T79">Er machte sich ein Bett und ging schlafen.</ta>
            <ta e="T85" id="Seg_2437" s="T83">Auf einmal schlief er ein.</ta>
            <ta e="T92" id="Seg_2438" s="T85">Nach einiger Zeit sah er (etwas) durch seinen Traum.</ta>
            <ta e="T95" id="Seg_2439" s="T92">Er hörte Hundebellen.</ta>
            <ta e="T98" id="Seg_2440" s="T95">Er wachte durch das Bellen des Hundes auf.</ta>
            <ta e="T102" id="Seg_2441" s="T98">Er erhob sich und legte Holz ins Feuer.</ta>
            <ta e="T107" id="Seg_2442" s="T102">Das Feuer flammte auf - es wurde (hell) wie der Tag.</ta>
            <ta e="T113" id="Seg_2443" s="T107">Er nahm sein Gewehr und lud es mit Patronen.</ta>
            <ta e="T120" id="Seg_2444" s="T113">Er ging vom Feuer zur Seite weg und hielt dann an.</ta>
            <ta e="T124" id="Seg_2445" s="T120">Er schaut, ein mit Eis bedeckter Bär kommt.</ta>
            <ta e="T132" id="Seg_2446" s="T124">Der Bär rollte sich im Wasser, im Schnee, er fror (so) sein Fell selbst ein.</ta>
            <ta e="T139" id="Seg_2447" s="T132">Er kam an Vaters Feuer um ihn zu fressen.</ta>
            <ta e="T144" id="Seg_2448" s="T139">Mein Vater legte viel Holz ins Feuer.</ta>
            <ta e="T147" id="Seg_2449" s="T144">Das Feuer brannte gut.</ta>
            <ta e="T160" id="Seg_2450" s="T147">Während der Bär hinter meinem Vater um das Feuer herum rannte, schmolz sein gefrorenes Fell nahe dem Feuer.</ta>
            <ta e="T164" id="Seg_2451" s="T160">Vater erschoß den Bären mit dem Gewehr.</ta>
            <ta e="T166" id="Seg_2452" s="T164">Der Bär fiel.</ta>
            <ta e="T174" id="Seg_2453" s="T166">So überlistete, tötete und aß einst mein Vater einen Bären.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_2454" s="T1">Обледеневший медведь.</ta>
            <ta e="T6" id="Seg_2455" s="T3">Мой отец пошел в тайгу.</ta>
            <ta e="T9" id="Seg_2456" s="T6">Он жил в охотничей избушке.</ta>
            <ta e="T10" id="Seg_2457" s="T9">Белковал.</ta>
            <ta e="T14" id="Seg_2458" s="T10">Хорошо добывал, чтобы белок есть.</ta>
            <ta e="T19" id="Seg_2459" s="T14">Когда он ходил на охоту в тайге, стрелял глухаря и рябчика.</ta>
            <ta e="T25" id="Seg_2460" s="T19">Без еды в тайге, в охотничей избушке, не бывал.</ta>
            <ta e="T29" id="Seg_2461" s="T25">Собака и ружье у него хорошие были.</ta>
            <ta e="T38" id="Seg_2462" s="T29">Однажды мой отец из охотничей избушки ходил ночевать два-три раза в стан.</ta>
            <ta e="T43" id="Seg_2463" s="T38">Он шел с утра до вечера.</ta>
            <ta e="T55" id="Seg_2464" s="T43">В этот день он добыл больше чем 35 белок, одного колонка и одного соболя.</ta>
            <ta e="T58" id="Seg_2465" s="T55">Это было осенью.</ta>
            <ta e="T60" id="Seg_2466" s="T58">Стемнело.</ta>
            <ta e="T62" id="Seg_2467" s="T60">Отец остановился.</ta>
            <ta e="T70" id="Seg_2468" s="T62">Дрова наготовил, срубил, стан сделал, дрова натаскал, приготовил ужин(?).</ta>
            <ta e="T73" id="Seg_2469" s="T70">Убитых зверей ободрал.</ta>
            <ta e="T77" id="Seg_2470" s="T73">Сам поел и собаку накормил.</ta>
            <ta e="T79" id="Seg_2471" s="T77">Патроны зарядил.</ta>
            <ta e="T83" id="Seg_2472" s="T79">Постель себе сделал, улегся спать.</ta>
            <ta e="T85" id="Seg_2473" s="T83">Когда-то уснул.</ta>
            <ta e="T92" id="Seg_2474" s="T85">Долго ли, коротко ли – сквозь сон видит.</ta>
            <ta e="T95" id="Seg_2475" s="T92">Слышит: собака лает.</ta>
            <ta e="T98" id="Seg_2476" s="T95">От собачьего лая проснулся.</ta>
            <ta e="T102" id="Seg_2477" s="T98">Вскочил, в огонь дрова положил.</ta>
            <ta e="T107" id="Seg_2478" s="T102">Огонь разгорелся – как днем стало.</ta>
            <ta e="T113" id="Seg_2479" s="T107">Ружье взял, пулевыми патронами ружье зарядил.</ta>
            <ta e="T120" id="Seg_2480" s="T113">От костра в сторону ушел, там стоит.</ta>
            <ta e="T124" id="Seg_2481" s="T120">Смотрит – идет медведь обледеневший.</ta>
            <ta e="T132" id="Seg_2482" s="T124">Медведь в воде, в снегу вывалялся, сам себе шерсть заморозил.</ta>
            <ta e="T139" id="Seg_2483" s="T132">Пришел к костру отца, чтобы съесть отца.</ta>
            <ta e="T144" id="Seg_2484" s="T139">Мой отец в огонь много положил дров.</ta>
            <ta e="T147" id="Seg_2485" s="T144">Огонь разгорелся хорошо.</ta>
            <ta e="T160" id="Seg_2486" s="T147">Пока медведь сзади отца вокруг костра бегал, его замерзшая шерсть около костра растаяла.</ta>
            <ta e="T164" id="Seg_2487" s="T160">Отец из ружья выстрелил в медведя.</ta>
            <ta e="T166" id="Seg_2488" s="T164">Медведь упал.</ta>
            <ta e="T174" id="Seg_2489" s="T166">Так однажды мой отец обманул, убил и съел медведя.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_2490" s="T1">обледеневший медведь</ta>
            <ta e="T6" id="Seg_2491" s="T3">отец пошел в тайгу</ta>
            <ta e="T9" id="Seg_2492" s="T6">жил в охот. избушке</ta>
            <ta e="T10" id="Seg_2493" s="T9">белковал</ta>
            <ta e="T14" id="Seg_2494" s="T10">хорошо добывал белок есть</ta>
            <ta e="T19" id="Seg_2495" s="T14">ходя на охоту в тайге стрелял глухаря рябчика</ta>
            <ta e="T25" id="Seg_2496" s="T19">без еды в тайге в охот. избушке не бывал</ta>
            <ta e="T29" id="Seg_2497" s="T25">собака ружье у него хор. были</ta>
            <ta e="T38" id="Seg_2498" s="T29">однажды отец с охот. избушки пошел два три переночевать в стану</ta>
            <ta e="T43" id="Seg_2499" s="T38">с утра до вечера он ходил</ta>
            <ta e="T55" id="Seg_2500" s="T43">в этот день добыл тридцать пять с лишним (больше 35) белок одного колонка одного соболя</ta>
            <ta e="T58" id="Seg_2501" s="T55">это было осенью</ta>
            <ta e="T60" id="Seg_2502" s="T58">потемнело</ta>
            <ta e="T62" id="Seg_2503" s="T60">отец остановился</ta>
            <ta e="T70" id="Seg_2504" s="T62">дрова наготовил срубил стан сделал дрова натаскал наготовил ужин</ta>
            <ta e="T73" id="Seg_2505" s="T70">убитых зверьков ободрал</ta>
            <ta e="T77" id="Seg_2506" s="T73">сам покушал собаку накормил</ta>
            <ta e="T79" id="Seg_2507" s="T77">патроны зарядил</ta>
            <ta e="T83" id="Seg_2508" s="T79">постель себе сделал улегся спать</ta>
            <ta e="T85" id="Seg_2509" s="T83">сколько уснул</ta>
            <ta e="T92" id="Seg_2510" s="T85">долго ли коротко сон видит</ta>
            <ta e="T95" id="Seg_2511" s="T92">слышит собака лает</ta>
            <ta e="T98" id="Seg_2512" s="T95">на собачий лай проснулся</ta>
            <ta e="T102" id="Seg_2513" s="T98">соскочил в огонь дрова положил</ta>
            <ta e="T107" id="Seg_2514" s="T102">огонь разгорелся как днем стало</ta>
            <ta e="T113" id="Seg_2515" s="T107">ружье взял пулевыми патронами ружье зарядил</ta>
            <ta e="T120" id="Seg_2516" s="T113">от огнища в сторону ушел там стоит</ta>
            <ta e="T124" id="Seg_2517" s="T120">смотрит идет медведь обледеневший</ta>
            <ta e="T132" id="Seg_2518" s="T124">медведь в воду в снег (на)валялся самого себя шерсть заморозил</ta>
            <ta e="T139" id="Seg_2519" s="T132">пришел к отцову огнищу съесть отца</ta>
            <ta e="T144" id="Seg_2520" s="T139">отец в огонь много положил дров</ta>
            <ta e="T147" id="Seg_2521" s="T144">огонь разгорелся хорошо</ta>
            <ta e="T160" id="Seg_2522" s="T147">медведь сзади отца кругом огня бегая медведя льдом застылая шерсть около огня растаяла</ta>
            <ta e="T164" id="Seg_2523" s="T160">отец ружьем стрелял в медведя</ta>
            <ta e="T166" id="Seg_2524" s="T164">медведь упал</ta>
            <ta e="T174" id="Seg_2525" s="T166">так однажды мой отец обманул убил съел медведя</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
