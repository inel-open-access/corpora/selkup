<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KMS_1963_InsectsInOurLife_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KMS_1963_InsectsInOurLife_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">27</ud-information>
            <ud-information attribute-name="# HIAT:w">19</ud-information>
            <ud-information attribute-name="# e">19</ud-information>
            <ud-information attribute-name="# HIAT:u">4</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KMS">
            <abbreviation>KMS</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KMS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T19" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">tämdə</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">ilɨgu</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">sättšim</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">eŋ</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">qarimɨɣɨn</ts>
                  <nts id="Seg_20" n="HIAT:ip">,</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">suː</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">üdəmɨɣɨn</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">nɨnɨŋa</ts>
                  <nts id="Seg_30" n="HIAT:ip">.</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_33" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_35" n="HIAT:w" s="T8">qajmi</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">pöttə</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">tʼeːlɨdʼel</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">püː</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_48" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">püːn</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">malmɨlʼewlʼe</ts>
                  <nts id="Seg_54" n="HIAT:ip">,</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_57" n="HIAT:w" s="T14">sɨppa</ts>
                  <nts id="Seg_58" n="HIAT:ip">,</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">sɨppa</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">wazakuŋ</ts>
                  <nts id="Seg_65" n="HIAT:ip">,</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">sərəndə</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">ǯennä</ts>
                  <nts id="Seg_72" n="HIAT:ip">.</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T19" id="Seg_74" n="sc" s="T0">
               <ts e="T1" id="Seg_76" n="e" s="T0">tämdə </ts>
               <ts e="T2" id="Seg_78" n="e" s="T1">ilɨgu </ts>
               <ts e="T3" id="Seg_80" n="e" s="T2">sättšim </ts>
               <ts e="T4" id="Seg_82" n="e" s="T3">eŋ. </ts>
               <ts e="T5" id="Seg_84" n="e" s="T4">qarimɨɣɨn, </ts>
               <ts e="T6" id="Seg_86" n="e" s="T5">suː </ts>
               <ts e="T7" id="Seg_88" n="e" s="T6">üdəmɨɣɨn </ts>
               <ts e="T8" id="Seg_90" n="e" s="T7">nɨnɨŋa. </ts>
               <ts e="T9" id="Seg_92" n="e" s="T8">qajmi </ts>
               <ts e="T10" id="Seg_94" n="e" s="T9">pöttə </ts>
               <ts e="T11" id="Seg_96" n="e" s="T10">tʼeːlɨdʼel </ts>
               <ts e="T12" id="Seg_98" n="e" s="T11">püː. </ts>
               <ts e="T13" id="Seg_100" n="e" s="T12">püːn </ts>
               <ts e="T14" id="Seg_102" n="e" s="T13">malmɨlʼewlʼe, </ts>
               <ts e="T15" id="Seg_104" n="e" s="T14">sɨppa, </ts>
               <ts e="T16" id="Seg_106" n="e" s="T15">sɨppa </ts>
               <ts e="T17" id="Seg_108" n="e" s="T16">wazakuŋ, </ts>
               <ts e="T18" id="Seg_110" n="e" s="T17">sərəndə </ts>
               <ts e="T19" id="Seg_112" n="e" s="T18">ǯennä. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_113" s="T0">KMS_1963_InsectsInOurLife_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_114" s="T4">KMS_1963_InsectsInOurLife_nar.002 (001.002)</ta>
            <ta e="T12" id="Seg_115" s="T8">KMS_1963_InsectsInOurLife_nar.003 (001.003)</ta>
            <ta e="T19" id="Seg_116" s="T12">KMS_1963_InsectsInOurLife_nar.004 (001.004)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_117" s="T0">тӓм′дъ илы′гу ′сӓттшим ең.</ta>
            <ta e="T8" id="Seg_118" s="T4">kа′римыɣын, сӯ ′ӱдъмыɣын ныны′ңа.</ta>
            <ta e="T12" id="Seg_119" s="T8">kай′ми ′пӧттъ ′тʼе̄лыдʼел пӱ̄.</ta>
            <ta e="T19" id="Seg_120" s="T12">′пӱ̄н ‵малмы′лʼевлʼе, сы′ппа, сы′ппа ваза′куң, ′съ(ы)ръндъ ′дженнӓ.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_121" s="T0">tämdə ilɨgu sättšim eŋ.</ta>
            <ta e="T8" id="Seg_122" s="T4">qarimɨɣɨn, suː üdəmɨɣɨn nɨnɨŋa.</ta>
            <ta e="T12" id="Seg_123" s="T8">qajmi pöttə tʼeːlɨdʼel püː.</ta>
            <ta e="T19" id="Seg_124" s="T12">püːn malmɨlʼevlʼe, sɨppa, sɨppa vazakuŋ, sə(ɨ)rəndə ǯennä.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_125" s="T0">tämdə ilɨgu sättšim eŋ. </ta>
            <ta e="T8" id="Seg_126" s="T4">qarimɨɣɨn, suː üdəmɨɣɨn nɨnɨŋa. </ta>
            <ta e="T12" id="Seg_127" s="T8">qajmi pöttə tʼeːlɨdʼel püː. </ta>
            <ta e="T19" id="Seg_128" s="T12">püːn malmɨlʼewlʼe, sɨppa, sɨppa wazakuŋ, sərəndə ǯennä. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_129" s="T0">tämdə</ta>
            <ta e="T2" id="Seg_130" s="T1">illɨ-gu</ta>
            <ta e="T3" id="Seg_131" s="T2">sätčim</ta>
            <ta e="T4" id="Seg_132" s="T3">e-n</ta>
            <ta e="T5" id="Seg_133" s="T4">qari-mɨ-ɣɨn</ta>
            <ta e="T6" id="Seg_134" s="T5">suː</ta>
            <ta e="T7" id="Seg_135" s="T6">üdə-mɨ-ɣɨn</ta>
            <ta e="T8" id="Seg_136" s="T7">nɨnɨŋa</ta>
            <ta e="T9" id="Seg_137" s="T8">qajmi</ta>
            <ta e="T10" id="Seg_138" s="T9">pöttə</ta>
            <ta e="T11" id="Seg_139" s="T10">tʼeːlɨ-dʼel</ta>
            <ta e="T12" id="Seg_140" s="T11">püː</ta>
            <ta e="T13" id="Seg_141" s="T12">püː-n</ta>
            <ta e="T14" id="Seg_142" s="T13">malmɨ-lʼewlʼe</ta>
            <ta e="T15" id="Seg_143" s="T14">sɨppa</ta>
            <ta e="T16" id="Seg_144" s="T15">sɨppa</ta>
            <ta e="T17" id="Seg_145" s="T16">waza-ku-n</ta>
            <ta e="T18" id="Seg_146" s="T17">sərə-n-də</ta>
            <ta e="T19" id="Seg_147" s="T18">čennɨ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_148" s="T0">tɨmtɨ</ta>
            <ta e="T2" id="Seg_149" s="T1">illɨ-gu</ta>
            <ta e="T3" id="Seg_150" s="T2">sätčuŋ</ta>
            <ta e="T4" id="Seg_151" s="T3">eː-n</ta>
            <ta e="T5" id="Seg_152" s="T4">qarɨ-mɨ-qɨn</ta>
            <ta e="T6" id="Seg_153" s="T5">suː</ta>
            <ta e="T7" id="Seg_154" s="T6">üːdɨ-mɨ-qɨn</ta>
            <ta e="T8" id="Seg_155" s="T7">nɨnka</ta>
            <ta e="T9" id="Seg_156" s="T8">qaime</ta>
            <ta e="T10" id="Seg_157" s="T9">pöttə</ta>
            <ta e="T11" id="Seg_158" s="T10">tʼeːlɨ-tʼeːlɨ</ta>
            <ta e="T12" id="Seg_159" s="T11">püː</ta>
            <ta e="T13" id="Seg_160" s="T12">püː-n</ta>
            <ta e="T14" id="Seg_161" s="T13">malma-lewle</ta>
            <ta e="T15" id="Seg_162" s="T14">sɨppa</ta>
            <ta e="T16" id="Seg_163" s="T15">sɨppa</ta>
            <ta e="T17" id="Seg_164" s="T16">wessɨ-kku-n</ta>
            <ta e="T18" id="Seg_165" s="T17">särro-n-ndɨ</ta>
            <ta e="T19" id="Seg_166" s="T18">čennɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_167" s="T0">here</ta>
            <ta e="T2" id="Seg_168" s="T1">live-INF</ta>
            <ta e="T3" id="Seg_169" s="T2">hard</ta>
            <ta e="T4" id="Seg_170" s="T3">be-3SG.S</ta>
            <ta e="T5" id="Seg_171" s="T4">morning-something-LOC</ta>
            <ta e="T6" id="Seg_172" s="T5">still</ta>
            <ta e="T7" id="Seg_173" s="T6">evening-something-LOC</ta>
            <ta e="T8" id="Seg_174" s="T7">mosquito.[NOM]</ta>
            <ta e="T9" id="Seg_175" s="T8">clear</ta>
            <ta e="T10" id="Seg_176" s="T9">warm</ta>
            <ta e="T11" id="Seg_177" s="T10">day-day</ta>
            <ta e="T12" id="Seg_178" s="T11">botfly</ta>
            <ta e="T13" id="Seg_179" s="T12">botfly-GEN</ta>
            <ta e="T14" id="Seg_180" s="T13">go.off-CVB</ta>
            <ta e="T15" id="Seg_181" s="T14">blackfly.[NOM]</ta>
            <ta e="T16" id="Seg_182" s="T15">blackfly.[NOM]</ta>
            <ta e="T17" id="Seg_183" s="T16">get.up-HAB-3SG.S</ta>
            <ta e="T18" id="Seg_184" s="T17">snow-GEN-OBL.3SG</ta>
            <ta e="T19" id="Seg_185" s="T18">until</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_186" s="T0">здесь</ta>
            <ta e="T2" id="Seg_187" s="T1">жить-INF</ta>
            <ta e="T3" id="Seg_188" s="T2">тяжело</ta>
            <ta e="T4" id="Seg_189" s="T3">быть-3SG.S</ta>
            <ta e="T5" id="Seg_190" s="T4">утро-нечто-LOC</ta>
            <ta e="T6" id="Seg_191" s="T5">тихий</ta>
            <ta e="T7" id="Seg_192" s="T6">вечер-нечто-LOC</ta>
            <ta e="T8" id="Seg_193" s="T7">комар.[NOM]</ta>
            <ta e="T9" id="Seg_194" s="T8">ясный</ta>
            <ta e="T10" id="Seg_195" s="T9">теплый</ta>
            <ta e="T11" id="Seg_196" s="T10">день-день</ta>
            <ta e="T12" id="Seg_197" s="T11">оводы</ta>
            <ta e="T13" id="Seg_198" s="T12">оводы-GEN</ta>
            <ta e="T14" id="Seg_199" s="T13">пройти-CVB</ta>
            <ta e="T15" id="Seg_200" s="T14">мошка.[NOM]</ta>
            <ta e="T16" id="Seg_201" s="T15">мошка.[NOM]</ta>
            <ta e="T17" id="Seg_202" s="T16">встать-HAB-3SG.S</ta>
            <ta e="T18" id="Seg_203" s="T17">снег-GEN-OBL.3SG</ta>
            <ta e="T19" id="Seg_204" s="T18">до</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_205" s="T0">adv</ta>
            <ta e="T2" id="Seg_206" s="T1">v-v:inf</ta>
            <ta e="T3" id="Seg_207" s="T2">adv</ta>
            <ta e="T4" id="Seg_208" s="T3">v-v:pn</ta>
            <ta e="T5" id="Seg_209" s="T4">n-n-n:case</ta>
            <ta e="T6" id="Seg_210" s="T5">adj</ta>
            <ta e="T7" id="Seg_211" s="T6">n-n-n:case</ta>
            <ta e="T8" id="Seg_212" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_213" s="T8">adj</ta>
            <ta e="T10" id="Seg_214" s="T9">adj</ta>
            <ta e="T11" id="Seg_215" s="T10">n-n</ta>
            <ta e="T12" id="Seg_216" s="T11">n</ta>
            <ta e="T13" id="Seg_217" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_218" s="T13">v-v&gt;adv</ta>
            <ta e="T15" id="Seg_219" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_220" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_221" s="T16">v-v&gt;v-v:pn</ta>
            <ta e="T18" id="Seg_222" s="T17">n-n:case-n:obl.poss</ta>
            <ta e="T19" id="Seg_223" s="T18">pp</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_224" s="T0">adv</ta>
            <ta e="T2" id="Seg_225" s="T1">v</ta>
            <ta e="T3" id="Seg_226" s="T2">adv</ta>
            <ta e="T4" id="Seg_227" s="T3">v</ta>
            <ta e="T5" id="Seg_228" s="T4">n</ta>
            <ta e="T6" id="Seg_229" s="T5">adj</ta>
            <ta e="T7" id="Seg_230" s="T6">n</ta>
            <ta e="T8" id="Seg_231" s="T7">n</ta>
            <ta e="T9" id="Seg_232" s="T8">adj</ta>
            <ta e="T10" id="Seg_233" s="T9">adj</ta>
            <ta e="T11" id="Seg_234" s="T10">n</ta>
            <ta e="T12" id="Seg_235" s="T11">n</ta>
            <ta e="T13" id="Seg_236" s="T12">n</ta>
            <ta e="T14" id="Seg_237" s="T13">adv</ta>
            <ta e="T15" id="Seg_238" s="T14">n</ta>
            <ta e="T16" id="Seg_239" s="T15">n</ta>
            <ta e="T17" id="Seg_240" s="T16">v</ta>
            <ta e="T18" id="Seg_241" s="T17">n</ta>
            <ta e="T19" id="Seg_242" s="T18">pp</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_243" s="T1">v:S</ta>
            <ta e="T4" id="Seg_244" s="T3">v:pred</ta>
            <ta e="T8" id="Seg_245" s="T7">np:S</ta>
            <ta e="T12" id="Seg_246" s="T11">np:S</ta>
            <ta e="T14" id="Seg_247" s="T12">s:temp</ta>
            <ta e="T16" id="Seg_248" s="T15">np:S</ta>
            <ta e="T17" id="Seg_249" s="T16">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_250" s="T1">v:Th</ta>
            <ta e="T5" id="Seg_251" s="T4">np:Time</ta>
            <ta e="T7" id="Seg_252" s="T6">np:Time</ta>
            <ta e="T8" id="Seg_253" s="T7">np:Th</ta>
            <ta e="T11" id="Seg_254" s="T10">np:Time</ta>
            <ta e="T12" id="Seg_255" s="T11">np:Th</ta>
            <ta e="T16" id="Seg_256" s="T15">np:Th</ta>
            <ta e="T19" id="Seg_257" s="T17">pp:Time</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_258" s="T0">Здесь жить тяжело.</ta>
            <ta e="T8" id="Seg_259" s="T4">Утром и тихим вечером комары.</ta>
            <ta e="T12" id="Seg_260" s="T8">В ясный теплый день паут.</ta>
            <ta e="T19" id="Seg_261" s="T12">Паут кончится, мошка, мошка подымается до белого снега (вплоть до снега).</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_262" s="T0">It is difficult to live here.</ta>
            <ta e="T8" id="Seg_263" s="T4">There are mosquitos in the morning and in the quiet evening.</ta>
            <ta e="T12" id="Seg_264" s="T8">There are botflies on a clear, warm day. </ta>
            <ta e="T19" id="Seg_265" s="T12">When the botflies are over, the blackflies come as long as snow comes.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_266" s="T0">Es ist schwer hier zu leben.</ta>
            <ta e="T8" id="Seg_267" s="T4">Morgens und abends gibt es Mücken.</ta>
            <ta e="T12" id="Seg_268" s="T8">An klaren warmen Tagen gibt es Dasselfliegen.</ta>
            <ta e="T19" id="Seg_269" s="T12">Wenn die Dasselfliegen vorbei sind, kommen die Kriebelmücken, bis zum Schnee.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_270" s="T0">здесь жить тяжело</ta>
            <ta e="T8" id="Seg_271" s="T4">утром тихим вечером комары</ta>
            <ta e="T12" id="Seg_272" s="T8">в ясный теплый день паут</ta>
            <ta e="T19" id="Seg_273" s="T12">паут кончится мошка мошка подымается до белого снега (вплоть до снега)</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
