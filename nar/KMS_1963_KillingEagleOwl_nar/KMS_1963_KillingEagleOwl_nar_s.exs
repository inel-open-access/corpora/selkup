<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KMS_1963_EagleOwl_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KMS_1963_KillingEagleOwl_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">44</ud-information>
            <ud-information attribute-name="# HIAT:w">32</ud-information>
            <ud-information attribute-name="# e">32</ud-information>
            <ud-information attribute-name="# HIAT:u">8</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KMS">
            <abbreviation>KMS</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KMS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T32" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">qwänǯitäɣɨn</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">me</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">tʼätčizo</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">püjam</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">matčizo</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">čuːppalʼam</ts>
                  <nts id="Seg_23" n="HIAT:ip">,</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">toppɨlʼamdə</ts>
                  <nts id="Seg_27" n="HIAT:ip">.</nts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_30" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_32" n="HIAT:w" s="T7">lʼaqčizo</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_35" n="HIAT:w" s="T8">olʼɨmdə</ts>
                  <nts id="Seg_36" n="HIAT:ip">.</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_39" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_41" n="HIAT:w" s="T9">pergɨmdə</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_44" n="HIAT:w" s="T10">pan</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_47" n="HIAT:w" s="T11">porgɨsso</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_51" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_53" n="HIAT:w" s="T12">qʼedumdə</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_56" n="HIAT:w" s="T13">to</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_59" n="HIAT:w" s="T14">tʼätčizoː</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_63" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_65" n="HIAT:w" s="T15">powɨmdə</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_68" n="HIAT:w" s="T16">qananni</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_71" n="HIAT:w" s="T17">amgu</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_74" n="HIAT:w" s="T18">misso</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_78" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_80" n="HIAT:w" s="T19">a</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_83" n="HIAT:w" s="T20">ondə</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_86" n="HIAT:w" s="T21">simdə</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_89" n="HIAT:w" s="T22">qalɨmdə</ts>
                  <nts id="Seg_90" n="HIAT:ip">,</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_93" n="HIAT:w" s="T23">pon</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_96" n="HIAT:w" s="T24">molʼandə</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_99" n="HIAT:w" s="T25">ɨttɨlʼe</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_102" n="HIAT:w" s="T26">qwädʼizo</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_105" n="HIAT:w" s="T27">telʼɨzä</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_107" n="HIAT:ip">(</nts>
                  <ts e="T29" id="Seg_109" n="HIAT:w" s="T28">tʼelʼɨndɨzä</ts>
                  <nts id="Seg_110" n="HIAT:ip">)</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_113" n="HIAT:w" s="T29">čeqqɨrɨgundɨgo</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_117" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_119" n="HIAT:w" s="T30">čeqqɨlʼe</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_122" n="HIAT:w" s="T31">čaǯi</ts>
                  <nts id="Seg_123" n="HIAT:ip">.</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T32" id="Seg_125" n="sc" s="T0">
               <ts e="T1" id="Seg_127" n="e" s="T0">qwänǯitäɣɨn </ts>
               <ts e="T2" id="Seg_129" n="e" s="T1">me </ts>
               <ts e="T3" id="Seg_131" n="e" s="T2">tʼätčizo </ts>
               <ts e="T4" id="Seg_133" n="e" s="T3">püjam. </ts>
               <ts e="T5" id="Seg_135" n="e" s="T4">matčizo </ts>
               <ts e="T6" id="Seg_137" n="e" s="T5">čuːppalʼam, </ts>
               <ts e="T7" id="Seg_139" n="e" s="T6">toppɨlʼamdə. </ts>
               <ts e="T8" id="Seg_141" n="e" s="T7">lʼaqčizo </ts>
               <ts e="T9" id="Seg_143" n="e" s="T8">olʼɨmdə. </ts>
               <ts e="T10" id="Seg_145" n="e" s="T9">pergɨmdə </ts>
               <ts e="T11" id="Seg_147" n="e" s="T10">pan </ts>
               <ts e="T12" id="Seg_149" n="e" s="T11">porgɨsso. </ts>
               <ts e="T13" id="Seg_151" n="e" s="T12">qʼedumdə </ts>
               <ts e="T14" id="Seg_153" n="e" s="T13">to </ts>
               <ts e="T15" id="Seg_155" n="e" s="T14">tʼätčizoː. </ts>
               <ts e="T16" id="Seg_157" n="e" s="T15">powɨmdə </ts>
               <ts e="T17" id="Seg_159" n="e" s="T16">qananni </ts>
               <ts e="T18" id="Seg_161" n="e" s="T17">amgu </ts>
               <ts e="T19" id="Seg_163" n="e" s="T18">misso. </ts>
               <ts e="T20" id="Seg_165" n="e" s="T19">a </ts>
               <ts e="T21" id="Seg_167" n="e" s="T20">ondə </ts>
               <ts e="T22" id="Seg_169" n="e" s="T21">simdə </ts>
               <ts e="T23" id="Seg_171" n="e" s="T22">qalɨmdə, </ts>
               <ts e="T24" id="Seg_173" n="e" s="T23">pon </ts>
               <ts e="T25" id="Seg_175" n="e" s="T24">molʼandə </ts>
               <ts e="T26" id="Seg_177" n="e" s="T25">ɨttɨlʼe </ts>
               <ts e="T27" id="Seg_179" n="e" s="T26">qwädʼizo </ts>
               <ts e="T28" id="Seg_181" n="e" s="T27">telʼɨzä </ts>
               <ts e="T29" id="Seg_183" n="e" s="T28">(tʼelʼɨndɨzä) </ts>
               <ts e="T30" id="Seg_185" n="e" s="T29">čeqqɨrɨgundɨgo. </ts>
               <ts e="T31" id="Seg_187" n="e" s="T30">čeqqɨlʼe </ts>
               <ts e="T32" id="Seg_189" n="e" s="T31">čaǯi. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_190" s="T0">KMS_1963_KillingEagleOwl_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_191" s="T4">KMS_1963_KillingEagleOwl_nar.002 (001.002)</ta>
            <ta e="T9" id="Seg_192" s="T7">KMS_1963_KillingEagleOwl_nar.003 (001.003)</ta>
            <ta e="T12" id="Seg_193" s="T9">KMS_1963_KillingEagleOwl_nar.004 (001.004)</ta>
            <ta e="T15" id="Seg_194" s="T12">KMS_1963_KillingEagleOwl_nar.005 (001.005)</ta>
            <ta e="T19" id="Seg_195" s="T15">KMS_1963_KillingEagleOwl_nar.006 (001.006)</ta>
            <ta e="T30" id="Seg_196" s="T19">KMS_1963_KillingEagleOwl_nar.007 (001.007)</ta>
            <ta e="T32" id="Seg_197" s="T30">KMS_1963_KillingEagleOwl_nar.008 (001.008)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_198" s="T0">′kwӓнджи ′тӓɣын ме тʼӓттшизо пӱ′jам.</ta>
            <ta e="T7" id="Seg_199" s="T4">маттши′зо ′тшӯп(п)алам, топпыламдъ.</ta>
            <ta e="T9" id="Seg_200" s="T7">лаkтши′зо о′лымдъ.</ta>
            <ta e="T12" id="Seg_201" s="T9">пергым′дъ пан ′поргыс(с)о.</ta>
            <ta e="T15" id="Seg_202" s="T12">′кʼедумдъ то ‵тʼӓттши′зо̄.</ta>
            <ta e="T19" id="Seg_203" s="T15">′повымдъ ка′нанни ′амгу ми′ссо.</ta>
            <ta e="T30" id="Seg_204" s="T19">а ′ондъ ′симдъ ′kалымдъ, ′пон ′моландъ ытты′лʼе ′kwӓдʼизо ′телызӓ (′тʼелындызӓ) ‵тше(ӓ)ккырыгунды′го.</ta>
            <ta e="T32" id="Seg_205" s="T30">′тшеккылʼе ′тшаджи.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_206" s="T0">qwänǯi täɣɨn me tʼättšizo püjam.</ta>
            <ta e="T7" id="Seg_207" s="T4">mattšizo tšuːp(p)alʼam, toppɨlʼamdə.</ta>
            <ta e="T9" id="Seg_208" s="T7">lʼaqtšizo olʼɨmdə.</ta>
            <ta e="T12" id="Seg_209" s="T9">pergɨmdə pan porgɨs(s)o.</ta>
            <ta e="T15" id="Seg_210" s="T12">qʼedumdə to tʼättšizoː.</ta>
            <ta e="T19" id="Seg_211" s="T15">povɨmdə qananni amgu misso.</ta>
            <ta e="T30" id="Seg_212" s="T19">a ondə simdə qalɨmdə, pon molʼandə ɨttɨlʼe qwädʼizo telʼɨzä (tʼelʼɨndɨzä) tše(ä)qqɨrɨgundɨgo.</ta>
            <ta e="T32" id="Seg_213" s="T30">tšeqqɨlʼe tšaǯi.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_214" s="T0">qwänǯitäɣɨn me tʼätčizo püjam. </ta>
            <ta e="T7" id="Seg_215" s="T4">matčizo čuːppalʼam, toppɨlʼamdə. </ta>
            <ta e="T9" id="Seg_216" s="T7">lʼaqčizo olʼɨmdə. </ta>
            <ta e="T12" id="Seg_217" s="T9">pergɨmdə pan porgɨsso. </ta>
            <ta e="T15" id="Seg_218" s="T12">qʼedumdə to tʼätčizoː. </ta>
            <ta e="T19" id="Seg_219" s="T15">powɨmdə qananni amgu misso. </ta>
            <ta e="T30" id="Seg_220" s="T19">a ondə simdə qalɨmdə, pon molʼandə ɨttɨlʼe qwädʼizo telʼɨzä (tʼelʼɨndɨzä) čeqqɨrɨgundɨgo. </ta>
            <ta e="T32" id="Seg_221" s="T30">čeqqɨlʼe čaǯi. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_222" s="T0">qwä-nǯi-tä-ɣɨn</ta>
            <ta e="T2" id="Seg_223" s="T1">me</ta>
            <ta e="T3" id="Seg_224" s="T2">tʼätči-z-o</ta>
            <ta e="T4" id="Seg_225" s="T3">püja-m</ta>
            <ta e="T5" id="Seg_226" s="T4">matči-z-o</ta>
            <ta e="T6" id="Seg_227" s="T5">čuːppa-lʼa-m</ta>
            <ta e="T7" id="Seg_228" s="T6">toppɨ-lʼa-m-də</ta>
            <ta e="T8" id="Seg_229" s="T7">lʼaqči-z-o</ta>
            <ta e="T9" id="Seg_230" s="T8">olʼɨ-m-də</ta>
            <ta e="T10" id="Seg_231" s="T9">pergɨ-m-də</ta>
            <ta e="T11" id="Seg_232" s="T10">pa-n</ta>
            <ta e="T12" id="Seg_233" s="T11">porgɨs-s-o</ta>
            <ta e="T13" id="Seg_234" s="T12">qʼedu-m-də</ta>
            <ta e="T14" id="Seg_235" s="T13">to</ta>
            <ta e="T15" id="Seg_236" s="T14">tʼätči-z-oː</ta>
            <ta e="T16" id="Seg_237" s="T15">powɨ-m-də</ta>
            <ta e="T17" id="Seg_238" s="T16">qanan-ni</ta>
            <ta e="T18" id="Seg_239" s="T17">am-ɨ-gu</ta>
            <ta e="T19" id="Seg_240" s="T18">mi-ss-o</ta>
            <ta e="T20" id="Seg_241" s="T19">a</ta>
            <ta e="T21" id="Seg_242" s="T20">ondə</ta>
            <ta e="T22" id="Seg_243" s="T21">*sim-də</ta>
            <ta e="T23" id="Seg_244" s="T22">qalɨ-m-də</ta>
            <ta e="T24" id="Seg_245" s="T23">po-n</ta>
            <ta e="T25" id="Seg_246" s="T24">mo-lʼa-ndə</ta>
            <ta e="T26" id="Seg_247" s="T25">ɨttɨ-lʼe</ta>
            <ta e="T27" id="Seg_248" s="T26">qwädʼi-z-o</ta>
            <ta e="T28" id="Seg_249" s="T27">telʼɨ-zä</ta>
            <ta e="T29" id="Seg_250" s="T28">tʼelʼɨ-n-dɨ-zä</ta>
            <ta e="T30" id="Seg_251" s="T29">čeqqɨrɨ-gundɨgo</ta>
            <ta e="T31" id="Seg_252" s="T30">čeqqɨ-lʼe</ta>
            <ta e="T32" id="Seg_253" s="T31">čaǯi</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_254" s="T0">qwən-nče-ptä-qən</ta>
            <ta e="T2" id="Seg_255" s="T1">meː</ta>
            <ta e="T3" id="Seg_256" s="T2">tʼaččɨ-sɨ-ut</ta>
            <ta e="T4" id="Seg_257" s="T3">puja-m</ta>
            <ta e="T5" id="Seg_258" s="T4">maččɨ-sɨ-ut</ta>
            <ta e="T6" id="Seg_259" s="T5">čuppa-la-m</ta>
            <ta e="T7" id="Seg_260" s="T6">topǝ-la-m-tɨ</ta>
            <ta e="T8" id="Seg_261" s="T7">laqčɨ-sɨ-ut</ta>
            <ta e="T9" id="Seg_262" s="T8">olɨ-m-tɨ</ta>
            <ta e="T10" id="Seg_263" s="T9">perkə-m-tɨ</ta>
            <ta e="T11" id="Seg_264" s="T10">paː-n</ta>
            <ta e="T12" id="Seg_265" s="T11">porgɨl-sɨ-ut</ta>
            <ta e="T13" id="Seg_266" s="T12">kettu-m-tɨ</ta>
            <ta e="T14" id="Seg_267" s="T13">teː</ta>
            <ta e="T15" id="Seg_268" s="T14">tʼaččɨ-sɨ-ut</ta>
            <ta e="T16" id="Seg_269" s="T15">poːju-m-tɨ</ta>
            <ta e="T17" id="Seg_270" s="T16">kanak-nɨ</ta>
            <ta e="T18" id="Seg_271" s="T17">am-ɨ-gu</ta>
            <ta e="T19" id="Seg_272" s="T18">mi-sɨ-ut</ta>
            <ta e="T20" id="Seg_273" s="T19">a</ta>
            <ta e="T21" id="Seg_274" s="T20">ontɨ</ta>
            <ta e="T22" id="Seg_275" s="T21">*siːm-tɨ</ta>
            <ta e="T23" id="Seg_276" s="T22">qalɨ-m-tɨ</ta>
            <ta e="T24" id="Seg_277" s="T23">po-n</ta>
            <ta e="T25" id="Seg_278" s="T24">moː-la-ndɨ</ta>
            <ta e="T26" id="Seg_279" s="T25">ittɨ-le</ta>
            <ta e="T27" id="Seg_280" s="T26">qwädɨ-sɨ-ut</ta>
            <ta e="T28" id="Seg_281" s="T27">tʼeːlɨ-se</ta>
            <ta e="T29" id="Seg_282" s="T28">tʼeːlɨ-n-tɨ-se</ta>
            <ta e="T30" id="Seg_283" s="T29">čeqqɨrɨ-gundɨgo</ta>
            <ta e="T31" id="Seg_284" s="T30">tʼikkɨ-le</ta>
            <ta e="T32" id="Seg_285" s="T31">čaːǯɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_286" s="T0">go.away-IPFV3-ACTN-LOC</ta>
            <ta e="T2" id="Seg_287" s="T1">we.NOM</ta>
            <ta e="T3" id="Seg_288" s="T2">shoot-PST-1PL</ta>
            <ta e="T4" id="Seg_289" s="T3">owl-ACC</ta>
            <ta e="T5" id="Seg_290" s="T4">cut-PST-1PL</ta>
            <ta e="T6" id="Seg_291" s="T5">wing-PL-ACC</ta>
            <ta e="T7" id="Seg_292" s="T6">leg-PL-ACC-3SG</ta>
            <ta e="T8" id="Seg_293" s="T7">break.down-PST-1PL</ta>
            <ta e="T9" id="Seg_294" s="T8">head-ACC-3SG</ta>
            <ta e="T10" id="Seg_295" s="T9">stomach-ACC-3SG</ta>
            <ta e="T11" id="Seg_296" s="T10">knife-INSTR2</ta>
            <ta e="T12" id="Seg_297" s="T11">rip.up-PST-1PL</ta>
            <ta e="T13" id="Seg_298" s="T12">intestine-ACC-3SG</ta>
            <ta e="T14" id="Seg_299" s="T13">away</ta>
            <ta e="T15" id="Seg_300" s="T14">throw-PST-1PL</ta>
            <ta e="T16" id="Seg_301" s="T15">navel-ACC-3SG</ta>
            <ta e="T17" id="Seg_302" s="T16">dog-ALL</ta>
            <ta e="T18" id="Seg_303" s="T17">eat-EP-INF</ta>
            <ta e="T19" id="Seg_304" s="T18">give-PST-1PL</ta>
            <ta e="T20" id="Seg_305" s="T19">but</ta>
            <ta e="T21" id="Seg_306" s="T20">oneself.3SG</ta>
            <ta e="T22" id="Seg_307" s="T21">self-3SG</ta>
            <ta e="T23" id="Seg_308" s="T22">rest-ACC-3SG</ta>
            <ta e="T24" id="Seg_309" s="T23">tree-GEN</ta>
            <ta e="T25" id="Seg_310" s="T24">branch-PL-ILL</ta>
            <ta e="T26" id="Seg_311" s="T25">hang-CVB</ta>
            <ta e="T27" id="Seg_312" s="T26">leave-PST-1PL</ta>
            <ta e="T28" id="Seg_313" s="T27">sun-INSTR</ta>
            <ta e="T29" id="Seg_314" s="T28">sun-GEN-3SG-INSTR</ta>
            <ta e="T30" id="Seg_315" s="T29">dry-SUP.3SG</ta>
            <ta e="T31" id="Seg_316" s="T30">dry-CVB</ta>
            <ta e="T32" id="Seg_317" s="T31">travel.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_318" s="T0">уйти-IPFV3-ACTN-LOC</ta>
            <ta e="T2" id="Seg_319" s="T1">мы.NOM</ta>
            <ta e="T3" id="Seg_320" s="T2">стрелять-PST-1PL</ta>
            <ta e="T4" id="Seg_321" s="T3">филин-ACC</ta>
            <ta e="T5" id="Seg_322" s="T4">резать-PST-1PL</ta>
            <ta e="T6" id="Seg_323" s="T5">крыло-PL-ACC</ta>
            <ta e="T7" id="Seg_324" s="T6">нога-PL-ACC-3SG</ta>
            <ta e="T8" id="Seg_325" s="T7">сломать-PST-1PL</ta>
            <ta e="T9" id="Seg_326" s="T8">голова-ACC-3SG</ta>
            <ta e="T10" id="Seg_327" s="T9">живот-ACC-3SG</ta>
            <ta e="T11" id="Seg_328" s="T10">нож-INSTR2</ta>
            <ta e="T12" id="Seg_329" s="T11">распороть-PST-1PL</ta>
            <ta e="T13" id="Seg_330" s="T12">кишка-ACC-3SG</ta>
            <ta e="T14" id="Seg_331" s="T13">прочь</ta>
            <ta e="T15" id="Seg_332" s="T14">бросать-PST-1PL</ta>
            <ta e="T16" id="Seg_333" s="T15">пупок-ACC-3SG</ta>
            <ta e="T17" id="Seg_334" s="T16">собака-ALL</ta>
            <ta e="T18" id="Seg_335" s="T17">съесть-EP-INF</ta>
            <ta e="T19" id="Seg_336" s="T18">дать-PST-1PL</ta>
            <ta e="T20" id="Seg_337" s="T19">а</ta>
            <ta e="T21" id="Seg_338" s="T20">сам.3SG</ta>
            <ta e="T22" id="Seg_339" s="T21">себя-3SG</ta>
            <ta e="T23" id="Seg_340" s="T22">остаток-ACC-3SG</ta>
            <ta e="T24" id="Seg_341" s="T23">дерево-GEN</ta>
            <ta e="T25" id="Seg_342" s="T24">ветка-PL-ILL</ta>
            <ta e="T26" id="Seg_343" s="T25">повесить-CVB</ta>
            <ta e="T27" id="Seg_344" s="T26">оставить-PST-1PL</ta>
            <ta e="T28" id="Seg_345" s="T27">солнце-INSTR</ta>
            <ta e="T29" id="Seg_346" s="T28">солнце-GEN-3SG-INSTR</ta>
            <ta e="T30" id="Seg_347" s="T29">сушить-SUP.3SG</ta>
            <ta e="T31" id="Seg_348" s="T30">сохнуть-CVB</ta>
            <ta e="T32" id="Seg_349" s="T31">ехать.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_350" s="T0">v-v&gt;v-v&gt;n-n:case</ta>
            <ta e="T2" id="Seg_351" s="T1">pers</ta>
            <ta e="T3" id="Seg_352" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_353" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_354" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_355" s="T5">n-n:num-n:case</ta>
            <ta e="T7" id="Seg_356" s="T6">n-n:num-n:case-n:poss</ta>
            <ta e="T8" id="Seg_357" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_358" s="T8">n-n:case-n:poss</ta>
            <ta e="T10" id="Seg_359" s="T9">n-n:case-n:poss</ta>
            <ta e="T11" id="Seg_360" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_361" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_362" s="T12">n-n:case-n:poss</ta>
            <ta e="T14" id="Seg_363" s="T13">preverb</ta>
            <ta e="T15" id="Seg_364" s="T14">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_365" s="T15">n-n:case-n:poss</ta>
            <ta e="T17" id="Seg_366" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_367" s="T17">v-n:ins-v:inf</ta>
            <ta e="T19" id="Seg_368" s="T18">v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_369" s="T19">conj</ta>
            <ta e="T21" id="Seg_370" s="T20">emphpro</ta>
            <ta e="T22" id="Seg_371" s="T21">pro-n:poss</ta>
            <ta e="T23" id="Seg_372" s="T22">n-n:case-n:poss</ta>
            <ta e="T24" id="Seg_373" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_374" s="T24">n-n:num-n:case</ta>
            <ta e="T26" id="Seg_375" s="T25">v-v&gt;adv</ta>
            <ta e="T27" id="Seg_376" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_377" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_378" s="T28">n-n:case-n:poss-n:case</ta>
            <ta e="T30" id="Seg_379" s="T29">v-v&gt;n</ta>
            <ta e="T31" id="Seg_380" s="T30">v-v&gt;adv</ta>
            <ta e="T32" id="Seg_381" s="T31">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_382" s="T0">n</ta>
            <ta e="T2" id="Seg_383" s="T1">pers</ta>
            <ta e="T3" id="Seg_384" s="T2">v</ta>
            <ta e="T4" id="Seg_385" s="T3">n</ta>
            <ta e="T5" id="Seg_386" s="T4">v</ta>
            <ta e="T6" id="Seg_387" s="T5">n</ta>
            <ta e="T7" id="Seg_388" s="T6">n</ta>
            <ta e="T8" id="Seg_389" s="T7">v</ta>
            <ta e="T9" id="Seg_390" s="T8">n</ta>
            <ta e="T10" id="Seg_391" s="T9">n</ta>
            <ta e="T11" id="Seg_392" s="T10">n</ta>
            <ta e="T12" id="Seg_393" s="T11">v</ta>
            <ta e="T13" id="Seg_394" s="T12">n</ta>
            <ta e="T14" id="Seg_395" s="T13">preverb</ta>
            <ta e="T15" id="Seg_396" s="T14">v</ta>
            <ta e="T16" id="Seg_397" s="T15">n</ta>
            <ta e="T17" id="Seg_398" s="T16">n</ta>
            <ta e="T18" id="Seg_399" s="T17">v</ta>
            <ta e="T19" id="Seg_400" s="T18">v</ta>
            <ta e="T20" id="Seg_401" s="T19">conj</ta>
            <ta e="T21" id="Seg_402" s="T20">emphpro</ta>
            <ta e="T22" id="Seg_403" s="T21">pro</ta>
            <ta e="T23" id="Seg_404" s="T22">n</ta>
            <ta e="T24" id="Seg_405" s="T23">n</ta>
            <ta e="T25" id="Seg_406" s="T24">n</ta>
            <ta e="T26" id="Seg_407" s="T25">adv</ta>
            <ta e="T27" id="Seg_408" s="T26">v</ta>
            <ta e="T28" id="Seg_409" s="T27">n</ta>
            <ta e="T29" id="Seg_410" s="T28">n</ta>
            <ta e="T30" id="Seg_411" s="T29">n</ta>
            <ta e="T31" id="Seg_412" s="T30">adv</ta>
            <ta e="T32" id="Seg_413" s="T31">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_414" s="T0">s:temp</ta>
            <ta e="T2" id="Seg_415" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_416" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_417" s="T3">np:O</ta>
            <ta e="T5" id="Seg_418" s="T4">0.1.h:S v:pred</ta>
            <ta e="T6" id="Seg_419" s="T5">np:O</ta>
            <ta e="T7" id="Seg_420" s="T6">np:O</ta>
            <ta e="T8" id="Seg_421" s="T7">0.1.h:S v:pred</ta>
            <ta e="T9" id="Seg_422" s="T8">np:O</ta>
            <ta e="T10" id="Seg_423" s="T9">np:O</ta>
            <ta e="T12" id="Seg_424" s="T11">0.1.h:S v:pred</ta>
            <ta e="T13" id="Seg_425" s="T12">np:O</ta>
            <ta e="T15" id="Seg_426" s="T14">0.1.h:S v:pred</ta>
            <ta e="T16" id="Seg_427" s="T15">np:O</ta>
            <ta e="T18" id="Seg_428" s="T17">s:purp</ta>
            <ta e="T19" id="Seg_429" s="T18">0.1.h:S v:pred</ta>
            <ta e="T23" id="Seg_430" s="T22">np:O</ta>
            <ta e="T27" id="Seg_431" s="T26">0.1.h:S v:pred</ta>
            <ta e="T30" id="Seg_432" s="T27">s:purp</ta>
            <ta e="T32" id="Seg_433" s="T31">0.3:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_434" s="T1">pro.h:A</ta>
            <ta e="T4" id="Seg_435" s="T3">np:P</ta>
            <ta e="T5" id="Seg_436" s="T4">0.1.h:A</ta>
            <ta e="T6" id="Seg_437" s="T5">np:P</ta>
            <ta e="T7" id="Seg_438" s="T6">np:P 0.3:Poss</ta>
            <ta e="T8" id="Seg_439" s="T7">0.1.h:A</ta>
            <ta e="T9" id="Seg_440" s="T8">np:P 0.3:Poss</ta>
            <ta e="T10" id="Seg_441" s="T9">np:P 0.3:Poss</ta>
            <ta e="T11" id="Seg_442" s="T10">np:Ins</ta>
            <ta e="T12" id="Seg_443" s="T11">0.1.h:A</ta>
            <ta e="T13" id="Seg_444" s="T12">np:Th 0.3:Poss</ta>
            <ta e="T15" id="Seg_445" s="T14">0.1.h:A</ta>
            <ta e="T16" id="Seg_446" s="T15">np:Th 0.3:Poss</ta>
            <ta e="T17" id="Seg_447" s="T16">np:R</ta>
            <ta e="T19" id="Seg_448" s="T18">0.1.h:A</ta>
            <ta e="T23" id="Seg_449" s="T22">np:Th</ta>
            <ta e="T25" id="Seg_450" s="T24">np:G</ta>
            <ta e="T27" id="Seg_451" s="T26">0.1.h:A</ta>
            <ta e="T30" id="Seg_452" s="T29">0.3:P</ta>
            <ta e="T32" id="Seg_453" s="T31">0.3:P</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T20" id="Seg_454" s="T19">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_455" s="T0">Перед тем как мы уехали, мы застрелили филина.</ta>
            <ta e="T7" id="Seg_456" s="T4">Отрезали крылья и ноги.</ta>
            <ta e="T9" id="Seg_457" s="T7">Оторвали голову.</ta>
            <ta e="T12" id="Seg_458" s="T9">Живот ножом распороли.</ta>
            <ta e="T15" id="Seg_459" s="T12">Кишки выбросили.</ta>
            <ta e="T19" id="Seg_460" s="T15">Пупок собаке съесть дали.</ta>
            <ta e="T30" id="Seg_461" s="T19">А сам остаток повесили на ветку, на солнце подсушить.</ta>
            <ta e="T32" id="Seg_462" s="T30">Пусть сохнет.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_463" s="T0">Before we were gone, we shot an eagle-owl.</ta>
            <ta e="T7" id="Seg_464" s="T4">We cut the wings and the legs off.</ta>
            <ta e="T9" id="Seg_465" s="T7">We tore away its head.</ta>
            <ta e="T12" id="Seg_466" s="T9">We ripped up its stomach with a knife.</ta>
            <ta e="T15" id="Seg_467" s="T12">We threw away its intestines.</ta>
            <ta e="T19" id="Seg_468" s="T15">We gave its navel for a dog to eat.</ta>
            <ta e="T30" id="Seg_469" s="T19">And the rest we hung onto a branch to dry it in the sun.</ta>
            <ta e="T32" id="Seg_470" s="T30">Let it dry out.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_471" s="T0">Vor dem Wegfahren erschossen wir eine Eule.</ta>
            <ta e="T7" id="Seg_472" s="T4">Wir schnitten die Flügel und Füße ab. </ta>
            <ta e="T9" id="Seg_473" s="T7">Wir rissen den Kopf ab.</ta>
            <ta e="T12" id="Seg_474" s="T9">Wir schlitzten den Bauch auf.</ta>
            <ta e="T15" id="Seg_475" s="T12">Wir warfen die Eingeweide raus.</ta>
            <ta e="T19" id="Seg_476" s="T15">Wir gaben seinen Nabel dem Hund zu essen.</ta>
            <ta e="T30" id="Seg_477" s="T19">Und der Rest wurde an einen Ast gehängt, damit es in der Sonne trocknet.</ta>
            <ta e="T32" id="Seg_478" s="T30">Lass es trocknen.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_479" s="T0">перед тем как уехать (накануне) (как только идти, уехать) мы застрелили филина</ta>
            <ta e="T7" id="Seg_480" s="T4">отрезали крылья ноги</ta>
            <ta e="T9" id="Seg_481" s="T7">оторвали голову</ta>
            <ta e="T12" id="Seg_482" s="T9">живот ножом распороли</ta>
            <ta e="T15" id="Seg_483" s="T12">кишки прочь выбросили</ta>
            <ta e="T19" id="Seg_484" s="T15">пупок собаке съесть дали.</ta>
            <ta e="T30" id="Seg_485" s="T19">а самого остаток на сучок повесив оставили на солнце подсушить</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
