<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>_KMS_1963_Lifestory1_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KMS_1963_LifestoryFull_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">423</ud-information>
            <ud-information attribute-name="# HIAT:w">324</ud-information>
            <ud-information attribute-name="# e">323</ud-information>
            <ud-information attribute-name="# HIAT:u">63</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SPK">
            <abbreviation>KMS</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T324" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T320" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T321" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
         <tli id="T243" />
         <tli id="T244" />
         <tli id="T245" />
         <tli id="T246" />
         <tli id="T247" />
         <tli id="T248" />
         <tli id="T319" />
         <tli id="T249" />
         <tli id="T250" />
         <tli id="T251" />
         <tli id="T252" />
         <tli id="T253" />
         <tli id="T254" />
         <tli id="T255" />
         <tli id="T256" />
         <tli id="T257" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" />
         <tli id="T261" />
         <tli id="T262" />
         <tli id="T263" />
         <tli id="T264" />
         <tli id="T265" />
         <tli id="T266" />
         <tli id="T267" />
         <tli id="T268" />
         <tli id="T269" />
         <tli id="T270" />
         <tli id="T271" />
         <tli id="T272" />
         <tli id="T273" />
         <tli id="T274" />
         <tli id="T275" />
         <tli id="T276" />
         <tli id="T277" />
         <tli id="T278" />
         <tli id="T279" />
         <tli id="T280" />
         <tli id="T281" />
         <tli id="T282" />
         <tli id="T283" />
         <tli id="T284" />
         <tli id="T322" />
         <tli id="T285" />
         <tli id="T286" />
         <tli id="T287" />
         <tli id="T288" />
         <tli id="T289" />
         <tli id="T290" />
         <tli id="T291" />
         <tli id="T292" />
         <tli id="T293" />
         <tli id="T294" />
         <tli id="T295" />
         <tli id="T296" />
         <tli id="T297" />
         <tli id="T298" />
         <tli id="T299" />
         <tli id="T300" />
         <tli id="T301" />
         <tli id="T302" />
         <tli id="T303" />
         <tli id="T304" />
         <tli id="T305" />
         <tli id="T306" />
         <tli id="T307" />
         <tli id="T308" />
         <tli id="T309" />
         <tli id="T310" />
         <tli id="T311" />
         <tli id="T312" />
         <tli id="T323" />
         <tli id="T313" />
         <tli id="T314" />
         <tli id="T315" />
         <tli id="T316" />
         <tli id="T317" />
         <tli id="T318" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SPK"
                      type="t">
         <timeline-fork end="T216" start="T215">
            <tli id="T215.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T318" id="Seg_0" n="sc" s="T0">
               <ts e="T11" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">oːruŋbaŋ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">muqtut</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">qwejgʼöt</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">poɣɨndə</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_16" n="HIAT:w" s="T4">šɨttə</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T324">saːrum</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">naːr</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">qwejgʼöt</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">čisloɣɨndə</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">sentʼäbrʼ</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">mesʼäcqɨn</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">äːttoɣɨn</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_41" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">äda</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">nimdə</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">Koːrela</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_53" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">nimdəgu</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">mazɨŋ</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">Madʼö</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">Koːrʼela</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_68" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">onäŋ</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">quːlaw</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_75" n="HIAT:ip">(</nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">kundoqɨn</ts>
                  <nts id="Seg_78" n="HIAT:ip">)</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_81" n="HIAT:w" s="T21">ukon</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">qwälɨjguzattə</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_87" n="HIAT:w" s="T23">i</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_90" n="HIAT:w" s="T24">suːrujguzattə</ts>
                  <nts id="Seg_91" n="HIAT:ip">.</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_94" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_96" n="HIAT:w" s="T25">meŋa</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_99" n="HIAT:w" s="T26">esan</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_102" n="HIAT:w" s="T27">naːr</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_105" n="HIAT:w" s="T28">pottə</ts>
                  <nts id="Seg_106" n="HIAT:ip">,</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_109" n="HIAT:w" s="T29">man</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_112" n="HIAT:w" s="T30">qalɨzaŋ</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_115" n="HIAT:w" s="T31">evewnannä</ts>
                  <nts id="Seg_116" n="HIAT:ip">.</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_119" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_121" n="HIAT:w" s="T32">evʼew</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">quːssan</ts>
                  <nts id="Seg_125" n="HIAT:ip">.</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_128" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">evewnan</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">esattə</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_136" n="HIAT:w" s="T36">üčelat</ts>
                  <nts id="Seg_137" n="HIAT:ip">,</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_140" n="HIAT:w" s="T37">quːkuzattə</ts>
                  <nts id="Seg_141" n="HIAT:ip">.</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_144" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_146" n="HIAT:w" s="T38">okkɨr</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_149" n="HIAT:w" s="T39">man</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_152" n="HIAT:w" s="T40">evewnan</ts>
                  <nts id="Seg_153" n="HIAT:ip">,</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_156" n="HIAT:w" s="T41">esewnan</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_159" n="HIAT:w" s="T42">qalɨmbaŋ</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_163" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_165" n="HIAT:w" s="T43">ässäw</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_168" n="HIAT:w" s="T44">palʼdʼukus</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_171" n="HIAT:w" s="T45">surujgu</ts>
                  <nts id="Seg_172" n="HIAT:ip">,</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_175" n="HIAT:w" s="T46">qwäːlɨjgu</ts>
                  <nts id="Seg_176" n="HIAT:ip">,</nts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_179" n="HIAT:w" s="T47">maːtqɨn</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_182" n="HIAT:w" s="T48">tʼäŋukus</ts>
                  <nts id="Seg_183" n="HIAT:ip">.</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_186" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_188" n="HIAT:w" s="T49">qalɨkkuzaŋ</ts>
                  <nts id="Seg_189" n="HIAT:ip">,</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_192" n="HIAT:w" s="T50">ilɨkuzaŋ</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_195" n="HIAT:w" s="T51">ilʼdʼäwsä</ts>
                  <nts id="Seg_196" n="HIAT:ip">.</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_199" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_201" n="HIAT:w" s="T52">tapsä</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_204" n="HIAT:w" s="T53">man</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_207" n="HIAT:w" s="T54">palʼdʼikuzaŋ</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_210" n="HIAT:w" s="T55">kän</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_213" n="HIAT:w" s="T56">tolʼdʼizä</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_216" n="HIAT:w" s="T57">matʼtʼöndə</ts>
                  <nts id="Seg_217" n="HIAT:ip">,</nts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_220" n="HIAT:w" s="T58">taːɣɨn</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_223" n="HIAT:w" s="T59">poŋgɨrlʼe</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_226" n="HIAT:w" s="T60">i</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_229" n="HIAT:w" s="T61">sapɨɣɨrlʼe</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_231" n="HIAT:ip">(</nts>
                  <ts e="T63" id="Seg_233" n="HIAT:w" s="T62">sappɨɣɨrgu</ts>
                  <nts id="Seg_234" n="HIAT:ip">)</nts>
                  <nts id="Seg_235" n="HIAT:ip">.</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_238" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_240" n="HIAT:w" s="T63">nilʼdʼiŋ</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_243" n="HIAT:w" s="T64">mat</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_246" n="HIAT:w" s="T65">ilɨkuzaŋ</ts>
                  <nts id="Seg_247" n="HIAT:ip">.</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_250" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_252" n="HIAT:w" s="T66">ilʼdäw</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_255" n="HIAT:w" s="T67">meŋnan</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_258" n="HIAT:w" s="T68">soː</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_261" n="HIAT:w" s="T69">eːkkus</ts>
                  <nts id="Seg_262" n="HIAT:ip">.</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_265" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_267" n="HIAT:w" s="T70">moːtku</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_270" n="HIAT:w" s="T71">mazɨŋ</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_273" n="HIAT:w" s="T72">ässäw</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_276" n="HIAT:w" s="T73">äːzundɨ</ts>
                  <nts id="Seg_277" n="HIAT:ip">,</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_280" n="HIAT:w" s="T74">ilʼdäw</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_283" n="HIAT:w" s="T75">peldɨkus</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_285" n="HIAT:ip">(</nts>
                  <ts e="T77" id="Seg_287" n="HIAT:w" s="T76">peldɨkuzan</ts>
                  <nts id="Seg_288" n="HIAT:ip">)</nts>
                  <nts id="Seg_289" n="HIAT:ip">.</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_292" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_294" n="HIAT:w" s="T77">täpsä</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_297" n="HIAT:w" s="T78">man</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_300" n="HIAT:w" s="T79">qondukuzaŋ</ts>
                  <nts id="Seg_301" n="HIAT:ip">.</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_304" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_306" n="HIAT:w" s="T80">kuː</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_309" n="HIAT:w" s="T81">täp</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_312" n="HIAT:w" s="T82">palʼdʼükus</ts>
                  <nts id="Seg_313" n="HIAT:ip">,</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_316" n="HIAT:w" s="T83">täpsä</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_319" n="HIAT:w" s="T84">man</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_322" n="HIAT:w" s="T85">äːkuzaŋ</ts>
                  <nts id="Seg_323" n="HIAT:ip">.</nts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_326" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_328" n="HIAT:w" s="T86">meŋnan</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_331" n="HIAT:w" s="T87">äːsan</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_334" n="HIAT:w" s="T88">čičew</ts>
                  <nts id="Seg_335" n="HIAT:ip">.</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_338" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_340" n="HIAT:w" s="T89">nimdə</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_343" n="HIAT:w" s="T90">täbɨn</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_346" n="HIAT:w" s="T91">Мihail</ts>
                  <nts id="Seg_347" n="HIAT:ip">.</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_350" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_352" n="HIAT:w" s="T92">mazɨŋ</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_355" n="HIAT:w" s="T93">soːrrɨkus</ts>
                  <nts id="Seg_356" n="HIAT:ip">.</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_359" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_361" n="HIAT:w" s="T94">oːɣɨlǯikus</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_364" n="HIAT:w" s="T95">palʼdʼügu</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_367" n="HIAT:w" s="T96">tolʼdʼin</ts>
                  <nts id="Seg_368" n="HIAT:ip">,</nts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_371" n="HIAT:w" s="T97">aːnduzä</ts>
                  <nts id="Seg_372" n="HIAT:ip">,</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_375" n="HIAT:w" s="T98">nʼäbɨjgu</ts>
                  <nts id="Seg_376" n="HIAT:ip">,</nts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_379" n="HIAT:w" s="T99">nʼäjajgu</ts>
                  <nts id="Seg_380" n="HIAT:ip">,</nts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_383" n="HIAT:w" s="T100">tüːlʼəsän</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_385" n="HIAT:ip">(</nts>
                  <ts e="T102" id="Seg_387" n="HIAT:w" s="T101">tüːləsäzä</ts>
                  <nts id="Seg_388" n="HIAT:ip">)</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_391" n="HIAT:w" s="T102">tʼätǯikugu</ts>
                  <nts id="Seg_392" n="HIAT:ip">,</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_395" n="HIAT:w" s="T103">kündɨnbarɣɨn</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_398" n="HIAT:w" s="T104">aːmdɨgu</ts>
                  <nts id="Seg_399" n="HIAT:ip">.</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_402" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_404" n="HIAT:w" s="T105">äːzɨn</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_407" n="HIAT:w" s="T106">müːttäɣɨn</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_410" n="HIAT:w" s="T107">täbɨm</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_413" n="HIAT:w" s="T108">qwätpattə</ts>
                  <nts id="Seg_414" n="HIAT:ip">.</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_417" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_419" n="HIAT:w" s="T109">meŋnan</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_422" n="HIAT:w" s="T110">eːsan</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_425" n="HIAT:w" s="T111">ojöw</ts>
                  <nts id="Seg_426" n="HIAT:ip">.</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_429" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_431" n="HIAT:w" s="T112">täp</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_434" n="HIAT:w" s="T113">äːkus</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_437" n="HIAT:w" s="T114">püdokus</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_440" n="HIAT:w" s="T115">qwärɣänni</ts>
                  <nts id="Seg_441" n="HIAT:ip">.</nts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_444" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_446" n="HIAT:w" s="T116">meŋa</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_449" n="HIAT:w" s="T117">qwäːdəmbɨkus</ts>
                  <nts id="Seg_450" n="HIAT:ip">,</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_453" n="HIAT:w" s="T118">mazɨŋ</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_456" n="HIAT:w" s="T119">moːtkuzɨt</ts>
                  <nts id="Seg_457" n="HIAT:ip">.</nts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_460" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_462" n="HIAT:w" s="T120">tidam</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_465" n="HIAT:w" s="T121">naj</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_468" n="HIAT:w" s="T122">qumba</ts>
                  <nts id="Seg_469" n="HIAT:ip">.</nts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_472" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_474" n="HIAT:w" s="T123">oːɣulʼǯukuzaŋ</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_477" n="HIAT:w" s="T124">okkə</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_480" n="HIAT:w" s="T125">klasqɨnä</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_483" n="HIAT:w" s="T126">äːttoɣɨn</ts>
                  <nts id="Seg_484" n="HIAT:ip">.</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_487" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_489" n="HIAT:w" s="T127">nimdə</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_492" n="HIAT:w" s="T128">täbɨn</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_495" n="HIAT:w" s="T129">äːkus</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_498" n="HIAT:w" s="T130">Pristanʼ</ts>
                  <nts id="Seg_499" n="HIAT:ip">.</nts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_502" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_504" n="HIAT:w" s="T131">tida</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_507" n="HIAT:w" s="T132">na</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_510" n="HIAT:w" s="T133">Pristanʼ</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_513" n="HIAT:w" s="T134">nimdə</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_516" n="HIAT:w" s="T135">tʼeɣa</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_519" n="HIAT:w" s="T136">qäː</ts>
                  <nts id="Seg_520" n="HIAT:ip">,</nts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_523" n="HIAT:w" s="T137">innäː-qäːtqi</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_526" n="HIAT:w" s="T138">rajon</ts>
                  <nts id="Seg_527" n="HIAT:ip">.</nts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_530" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_532" n="HIAT:w" s="T139">tättə</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_535" n="HIAT:w" s="T140">klasɨm</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_538" n="HIAT:w" s="T141">malčizaw</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_541" n="HIAT:w" s="T142">Maksɨ</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_544" n="HIAT:w" s="T143">matʼtʼöɣɨn</ts>
                  <nts id="Seg_545" n="HIAT:ip">.</nts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_548" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_550" n="HIAT:w" s="T144">Maksɨ</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_553" n="HIAT:w" s="T145">maːtʼtʼoɣənnɨ</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_556" n="HIAT:w" s="T146">qwässaŋ</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_559" n="HIAT:w" s="T147">oːɣulǯukugu</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_562" n="HIAT:w" s="T148">kargasokskij</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_565" n="HIAT:w" s="T149">rajondə</ts>
                  <nts id="Seg_566" n="HIAT:ip">,</nts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_569" n="HIAT:w" s="T150">škola</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_572" n="HIAT:w" s="T151">nimdə</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_575" n="HIAT:w" s="T152">Vʼertʼekos</ts>
                  <nts id="Seg_576" n="HIAT:ip">.</nts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_579" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_581" n="HIAT:w" s="T153">niːn</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_584" n="HIAT:w" s="T154">man</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_587" n="HIAT:w" s="T155">malčizaw</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_590" n="HIAT:w" s="T156">sälʼdʼü</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_593" n="HIAT:w" s="T157">klassam</ts>
                  <nts id="Seg_594" n="HIAT:ip">.</nts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_597" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_599" n="HIAT:w" s="T158">nänno</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_602" n="HIAT:w" s="T159">man</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_605" n="HIAT:w" s="T160">oːɣulǯukuzaŋ</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_608" n="HIAT:w" s="T161">kalpašiɣɨn</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_611" n="HIAT:w" s="T162">pedučilʼiše</ts>
                  <nts id="Seg_612" n="HIAT:ip">.</nts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_615" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_617" n="HIAT:w" s="T163">poɣɨndə</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_620" n="HIAT:w" s="T164">oːɣuǯɨkuzaŋ</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_623" n="HIAT:w" s="T165">padgatavitelʼnɨj</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_626" n="HIAT:w" s="T166">kursäɣɨn</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_629" n="HIAT:w" s="T167">i</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_632" n="HIAT:w" s="T168">puːssaŋ</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_635" n="HIAT:w" s="T169">man</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_638" n="HIAT:w" s="T170">okkɨ</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_641" n="HIAT:w" s="T171">kursanda</ts>
                  <nts id="Seg_642" n="HIAT:ip">.</nts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_645" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_647" n="HIAT:w" s="T172">okkə</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_650" n="HIAT:w" s="T173">kursaɣannä</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_653" n="HIAT:w" s="T174">naːr</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_656" n="HIAT:w" s="T320">saːrum</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_659" n="HIAT:w" s="T175">okkɨrtʼädʼigʼöt</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_662" n="HIAT:w" s="T176">poɣəndɨ</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_665" n="HIAT:w" s="T177">mazɨŋ</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_668" n="HIAT:w" s="T178">iːsattə</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_671" n="HIAT:w" s="T179">nʼärɣa</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_674" n="HIAT:w" s="T180">arminda</ts>
                  <nts id="Seg_675" n="HIAT:ip">.</nts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_678" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_680" n="HIAT:w" s="T181">nɨtʼän</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_683" n="HIAT:w" s="T182">man</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_686" n="HIAT:w" s="T183">eːsaŋ</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_689" n="HIAT:w" s="T184">sälʼdʼü</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_692" n="HIAT:w" s="T185">pon</ts>
                  <nts id="Seg_693" n="HIAT:ip">.</nts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_696" n="HIAT:u" s="T186">
                  <ts e="T321" id="Seg_698" n="HIAT:w" s="T186">tättə</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_701" n="HIAT:w" s="T321">saːrum</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_704" n="HIAT:w" s="T187">muqtutqwej</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_707" n="HIAT:w" s="T188">pon</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_710" n="HIAT:w" s="T189">mazɨŋ</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_713" n="HIAT:w" s="T190">ütattə</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_716" n="HIAT:w" s="T191">matqɨneŋ</ts>
                  <nts id="Seg_717" n="HIAT:ip">.</nts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_720" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_722" n="HIAT:w" s="T192">na</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_725" n="HIAT:w" s="T193">pon</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_728" n="HIAT:w" s="T194">man</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_731" n="HIAT:w" s="T195">tüːssaŋ</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_734" n="HIAT:w" s="T196">tʼeːɣə</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_737" n="HIAT:w" s="T197">matšondə</ts>
                  <nts id="Seg_738" n="HIAT:ip">.</nts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_741" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_743" n="HIAT:w" s="T198">tämdə</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_746" n="HIAT:w" s="T199">naʒə</ts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_749" n="HIAT:w" s="T201">bon</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_752" n="HIAT:w" s="T202">man</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_755" n="HIAT:w" s="T203">nätɨzaŋ</ts>
                  <nts id="Seg_756" n="HIAT:ip">.</nts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_759" n="HIAT:u" s="T204">
                  <ts e="T205" id="Seg_761" n="HIAT:w" s="T204">poɣɨndə</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_764" n="HIAT:w" s="T205">äːsaŋ</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_767" n="HIAT:w" s="T206">Arlopkaɣɨn</ts>
                  <nts id="Seg_768" n="HIAT:ip">.</nts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T211" id="Seg_771" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_773" n="HIAT:w" s="T207">nännɨ</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_776" n="HIAT:w" s="T208">laqqɨzaŋ</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_779" n="HIAT:w" s="T209">tʼeɣə</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_782" n="HIAT:w" s="T210">mačoɣɨn</ts>
                  <nts id="Seg_783" n="HIAT:ip">.</nts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_786" n="HIAT:u" s="T211">
                  <ts e="T212" id="Seg_788" n="HIAT:w" s="T211">tʼeɣə</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_791" n="HIAT:w" s="T212">mačoɣənnɨ</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_794" n="HIAT:w" s="T213">mazɨŋ</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_797" n="HIAT:w" s="T214">üːdizattə</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215.tx.1" id="Seg_800" n="HIAT:w" s="T215">ustʼ</ts>
                  <nts id="Seg_801" n="HIAT:ip">_</nts>
                  <ts e="T216" id="Seg_803" n="HIAT:w" s="T215.tx.1">azʼörnɨj</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_806" n="HIAT:w" s="T216">aːntondə</ts>
                  <nts id="Seg_807" n="HIAT:ip">.</nts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T228" id="Seg_810" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_812" n="HIAT:w" s="T217">aːntoːɣɨn</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_815" n="HIAT:w" s="T218">laqqɨzaŋ</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_818" n="HIAT:w" s="T219">klupqɨn</ts>
                  <nts id="Seg_819" n="HIAT:ip">,</nts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_822" n="HIAT:w" s="T220">nännä</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_825" n="HIAT:w" s="T221">qwälɨlʼdʼi</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_828" n="HIAT:w" s="T222">artʼelʼɣɨn</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_830" n="HIAT:ip">(</nts>
                  <ts e="T224" id="Seg_832" n="HIAT:w" s="T223">predsedatelem</ts>
                  <nts id="Seg_833" n="HIAT:ip">)</nts>
                  <nts id="Seg_834" n="HIAT:ip">,</nts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_837" n="HIAT:w" s="T224">nännə</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_840" n="HIAT:w" s="T225">selʼsavetqɨt</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_843" n="HIAT:w" s="T226">predsedatelem</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_846" n="HIAT:w" s="T227">selʼsoveta</ts>
                  <nts id="Seg_847" n="HIAT:ip">.</nts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T230" id="Seg_850" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_852" n="HIAT:w" s="T228">laqaŋ</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_855" n="HIAT:w" s="T229">nändə</ts>
                  <nts id="Seg_856" n="HIAT:ip">.</nts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T232" id="Seg_859" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_861" n="HIAT:w" s="T230">man</ts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_864" n="HIAT:w" s="T231">nädɨmbaŋ</ts>
                  <nts id="Seg_865" n="HIAT:ip">.</nts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_868" n="HIAT:u" s="T232">
                  <ts e="T233" id="Seg_870" n="HIAT:w" s="T232">man</ts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_873" n="HIAT:w" s="T233">pajannɨzä</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_876" n="HIAT:w" s="T234">ilaŋ</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_879" n="HIAT:w" s="T235">šɨttətʼädʼiqwäjgʼöt</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_882" n="HIAT:w" s="T236">pottə</ts>
                  <nts id="Seg_883" n="HIAT:ip">.</nts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T245" id="Seg_886" n="HIAT:u" s="T237">
                  <ts e="T238" id="Seg_888" n="HIAT:w" s="T237">učej</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_891" n="HIAT:w" s="T238">maːdə</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_894" n="HIAT:w" s="T239">umbaŋ</ts>
                  <nts id="Seg_895" n="HIAT:ip">:</nts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_898" n="HIAT:w" s="T240">sombɨlʼe</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_901" n="HIAT:w" s="T241">iːwə</ts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_904" n="HIAT:w" s="T242">i</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_907" n="HIAT:w" s="T243">šɨttə</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_910" n="HIAT:w" s="T244">näwä</ts>
                  <nts id="Seg_911" n="HIAT:ip">.</nts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T247" id="Seg_914" n="HIAT:u" s="T245">
                  <ts e="T246" id="Seg_916" n="HIAT:w" s="T245">pajaw</ts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_919" n="HIAT:w" s="T246">qassaɣi</ts>
                  <nts id="Seg_920" n="HIAT:ip">.</nts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_923" n="HIAT:u" s="T247">
                  <ts e="T248" id="Seg_925" n="HIAT:w" s="T247">üːčelaw</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_928" n="HIAT:w" s="T248">süsüɣulʼ</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_931" n="HIAT:w" s="T319">dʼe</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_934" n="HIAT:w" s="T249">assä</ts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_937" n="HIAT:w" s="T250">čenʼčuattə</ts>
                  <nts id="Seg_938" n="HIAT:ip">.</nts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T258" id="Seg_941" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_943" n="HIAT:w" s="T251">wärɣə</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_946" n="HIAT:w" s="T252">iːwä</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_949" n="HIAT:w" s="T253">malčimbat</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_952" n="HIAT:w" s="T254">tam</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_955" n="HIAT:w" s="T255">bon</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_958" n="HIAT:w" s="T256">köt</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_961" n="HIAT:w" s="T257">klazɨm</ts>
                  <nts id="Seg_962" n="HIAT:ip">.</nts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_965" n="HIAT:u" s="T258">
                  <ts e="T259" id="Seg_967" n="HIAT:w" s="T258">nʼünʼo</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_970" n="HIAT:w" s="T259">iːɣəneŋ</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_973" n="HIAT:w" s="T260">sombəlʼeːŋə</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_976" n="HIAT:w" s="T261">muqtumdälǯi</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_979" n="HIAT:w" s="T262">pottə</ts>
                  <nts id="Seg_980" n="HIAT:ip">.</nts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T267" id="Seg_983" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_985" n="HIAT:w" s="T263">okkɨ</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_988" n="HIAT:w" s="T264">iːwa</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_991" n="HIAT:w" s="T265">opsä</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_994" n="HIAT:w" s="T266">küdas</ts>
                  <nts id="Seg_995" n="HIAT:ip">.</nts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T270" id="Seg_998" n="HIAT:u" s="T267">
                  <ts e="T268" id="Seg_1000" n="HIAT:w" s="T267">täbɨm</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1003" n="HIAT:w" s="T268">qwändəkuzattə</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1006" n="HIAT:w" s="T269">Tomdə</ts>
                  <nts id="Seg_1007" n="HIAT:ip">.</nts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T275" id="Seg_1010" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_1012" n="HIAT:w" s="T270">täbɨm</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1015" n="HIAT:w" s="T271">nɨtän</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1018" n="HIAT:w" s="T272">soːmǯattə</ts>
                  <nts id="Seg_1019" n="HIAT:ip">,</nts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1022" n="HIAT:w" s="T273">tatpattə</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1025" n="HIAT:w" s="T274">qottä</ts>
                  <nts id="Seg_1026" n="HIAT:ip">.</nts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T278" id="Seg_1029" n="HIAT:u" s="T275">
                  <ts e="T276" id="Seg_1031" n="HIAT:w" s="T275">man</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1034" n="HIAT:w" s="T276">üːčelaw</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1037" n="HIAT:w" s="T277">süsüqula</ts>
                  <nts id="Seg_1038" n="HIAT:ip">.</nts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T281" id="Seg_1041" n="HIAT:u" s="T278">
                  <ts e="T279" id="Seg_1043" n="HIAT:w" s="T278">täppɨla</ts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1046" n="HIAT:w" s="T279">oːɣulǯuqwattə</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1049" n="HIAT:w" s="T280">qomdägalɨk</ts>
                  <nts id="Seg_1050" n="HIAT:ip">.</nts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_1053" n="HIAT:u" s="T281">
                  <ts e="T282" id="Seg_1055" n="HIAT:w" s="T281">onäŋ</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1058" n="HIAT:w" s="T282">man</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1061" n="HIAT:w" s="T283">robiŋwaw</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1064" n="HIAT:w" s="T284">tättä</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1067" n="HIAT:w" s="T322">saːrum</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1070" n="HIAT:w" s="T285">sombɨlʼe</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1073" n="HIAT:w" s="T286">qwäjgʼöt</ts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1076" n="HIAT:w" s="T287">lʼebɨm</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1079" n="HIAT:w" s="T288">ireɣəndə</ts>
                  <nts id="Seg_1080" n="HIAT:ip">.</nts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T292" id="Seg_1083" n="HIAT:u" s="T289">
                  <ts e="T290" id="Seg_1085" n="HIAT:w" s="T289">ilaŋ</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1088" n="HIAT:w" s="T290">man</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1091" n="HIAT:w" s="T291">soːŋ</ts>
                  <nts id="Seg_1092" n="HIAT:ip">.</nts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1095" n="HIAT:u" s="T292">
                  <ts e="T293" id="Seg_1097" n="HIAT:w" s="T292">äran</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1100" n="HIAT:w" s="T293">swäŋajgwaŋ</ts>
                  <nts id="Seg_1101" n="HIAT:ip">,</nts>
                  <nts id="Seg_1102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1104" n="HIAT:w" s="T294">säŋɨigwaŋ</ts>
                  <nts id="Seg_1105" n="HIAT:ip">,</nts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1108" n="HIAT:w" s="T295">čoppɨräigwaŋ</ts>
                  <nts id="Seg_1109" n="HIAT:ip">,</nts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1112" n="HIAT:w" s="T296">suːrujigwaŋ</ts>
                  <nts id="Seg_1113" n="HIAT:ip">.</nts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1116" n="HIAT:u" s="T297">
                  <ts e="T298" id="Seg_1118" n="HIAT:w" s="T297">taːɣɨn</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1121" n="HIAT:w" s="T298">poŋgɨrɨgwaŋ</ts>
                  <nts id="Seg_1122" n="HIAT:ip">,</nts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1125" n="HIAT:w" s="T299">qwälɨjgwaŋ</ts>
                  <nts id="Seg_1126" n="HIAT:ip">.</nts>
                  <nts id="Seg_1127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T305" id="Seg_1129" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1131" n="HIAT:w" s="T300">tɨdam</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1134" n="HIAT:w" s="T301">kɨkkaŋ</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1137" n="HIAT:w" s="T302">täwɨgu</ts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1140" n="HIAT:w" s="T303">sändə</ts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1143" n="HIAT:w" s="T304">maːdəm</ts>
                  <nts id="Seg_1144" n="HIAT:ip">.</nts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_1147" n="HIAT:u" s="T305">
                  <ts e="T306" id="Seg_1149" n="HIAT:w" s="T305">onäŋ</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1152" n="HIAT:w" s="T306">manmə</ts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1155" n="HIAT:w" s="T307">täːmba</ts>
                  <nts id="Seg_1156" n="HIAT:ip">.</nts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T314" id="Seg_1159" n="HIAT:u" s="T308">
                  <ts e="T309" id="Seg_1161" n="HIAT:w" s="T308">sändə</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1164" n="HIAT:w" s="T309">maːdɨm</ts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1167" n="HIAT:w" s="T310">täwɨgu</ts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1170" n="HIAT:w" s="T311">kɨkkaŋ</ts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1173" n="HIAT:w" s="T312">muqtut</ts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1177" n="HIAT:w" s="T323">saːrum</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1180" n="HIAT:w" s="T313">lʼependə</ts>
                  <nts id="Seg_1181" n="HIAT:ip">.</nts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T318" id="Seg_1184" n="HIAT:u" s="T314">
                  <ts e="T315" id="Seg_1186" n="HIAT:w" s="T314">man</ts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1189" n="HIAT:w" s="T315">täbɨm</ts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1192" n="HIAT:w" s="T316">täwenǯaw</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1195" n="HIAT:w" s="T317">ilʼenǯaŋ</ts>
                  <nts id="Seg_1196" n="HIAT:ip">.</nts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T318" id="Seg_1198" n="sc" s="T0">
               <ts e="T1" id="Seg_1200" n="e" s="T0">oːruŋbaŋ </ts>
               <ts e="T2" id="Seg_1202" n="e" s="T1">muqtut </ts>
               <ts e="T3" id="Seg_1204" n="e" s="T2">qwejgʼöt </ts>
               <ts e="T4" id="Seg_1206" n="e" s="T3">poɣɨndə </ts>
               <ts e="T324" id="Seg_1208" n="e" s="T4">šɨttə </ts>
               <ts e="T5" id="Seg_1210" n="e" s="T324">saːrum </ts>
               <ts e="T6" id="Seg_1212" n="e" s="T5">naːr </ts>
               <ts e="T7" id="Seg_1214" n="e" s="T6">qwejgʼöt </ts>
               <ts e="T8" id="Seg_1216" n="e" s="T7">čisloɣɨndə </ts>
               <ts e="T9" id="Seg_1218" n="e" s="T8">sentʼäbrʼ </ts>
               <ts e="T10" id="Seg_1220" n="e" s="T9">mesʼäcqɨn </ts>
               <ts e="T11" id="Seg_1222" n="e" s="T10">äːttoɣɨn. </ts>
               <ts e="T12" id="Seg_1224" n="e" s="T11">äda </ts>
               <ts e="T13" id="Seg_1226" n="e" s="T12">nimdə </ts>
               <ts e="T14" id="Seg_1228" n="e" s="T13">Koːrela. </ts>
               <ts e="T15" id="Seg_1230" n="e" s="T14">nimdəgu </ts>
               <ts e="T16" id="Seg_1232" n="e" s="T15">mazɨŋ </ts>
               <ts e="T17" id="Seg_1234" n="e" s="T16">Madʼö </ts>
               <ts e="T18" id="Seg_1236" n="e" s="T17">Koːrʼela. </ts>
               <ts e="T19" id="Seg_1238" n="e" s="T18">onäŋ </ts>
               <ts e="T20" id="Seg_1240" n="e" s="T19">quːlaw </ts>
               <ts e="T21" id="Seg_1242" n="e" s="T20">(kundoqɨn) </ts>
               <ts e="T22" id="Seg_1244" n="e" s="T21">ukon </ts>
               <ts e="T23" id="Seg_1246" n="e" s="T22">qwälɨjguzattə </ts>
               <ts e="T24" id="Seg_1248" n="e" s="T23">i </ts>
               <ts e="T25" id="Seg_1250" n="e" s="T24">suːrujguzattə. </ts>
               <ts e="T26" id="Seg_1252" n="e" s="T25">meŋa </ts>
               <ts e="T27" id="Seg_1254" n="e" s="T26">esan </ts>
               <ts e="T28" id="Seg_1256" n="e" s="T27">naːr </ts>
               <ts e="T29" id="Seg_1258" n="e" s="T28">pottə, </ts>
               <ts e="T30" id="Seg_1260" n="e" s="T29">man </ts>
               <ts e="T31" id="Seg_1262" n="e" s="T30">qalɨzaŋ </ts>
               <ts e="T32" id="Seg_1264" n="e" s="T31">evewnannä. </ts>
               <ts e="T33" id="Seg_1266" n="e" s="T32">evʼew </ts>
               <ts e="T34" id="Seg_1268" n="e" s="T33">quːssan. </ts>
               <ts e="T35" id="Seg_1270" n="e" s="T34">evewnan </ts>
               <ts e="T36" id="Seg_1272" n="e" s="T35">esattə </ts>
               <ts e="T37" id="Seg_1274" n="e" s="T36">üčelat, </ts>
               <ts e="T38" id="Seg_1276" n="e" s="T37">quːkuzattə. </ts>
               <ts e="T39" id="Seg_1278" n="e" s="T38">okkɨr </ts>
               <ts e="T40" id="Seg_1280" n="e" s="T39">man </ts>
               <ts e="T41" id="Seg_1282" n="e" s="T40">evewnan, </ts>
               <ts e="T42" id="Seg_1284" n="e" s="T41">esewnan </ts>
               <ts e="T43" id="Seg_1286" n="e" s="T42">qalɨmbaŋ. </ts>
               <ts e="T44" id="Seg_1288" n="e" s="T43">ässäw </ts>
               <ts e="T45" id="Seg_1290" n="e" s="T44">palʼdʼukus </ts>
               <ts e="T46" id="Seg_1292" n="e" s="T45">surujgu, </ts>
               <ts e="T47" id="Seg_1294" n="e" s="T46">qwäːlɨjgu, </ts>
               <ts e="T48" id="Seg_1296" n="e" s="T47">maːtqɨn </ts>
               <ts e="T49" id="Seg_1298" n="e" s="T48">tʼäŋukus. </ts>
               <ts e="T50" id="Seg_1300" n="e" s="T49">qalɨkkuzaŋ, </ts>
               <ts e="T51" id="Seg_1302" n="e" s="T50">ilɨkuzaŋ </ts>
               <ts e="T52" id="Seg_1304" n="e" s="T51">ilʼdʼäwsä. </ts>
               <ts e="T53" id="Seg_1306" n="e" s="T52">tapsä </ts>
               <ts e="T54" id="Seg_1308" n="e" s="T53">man </ts>
               <ts e="T55" id="Seg_1310" n="e" s="T54">palʼdʼikuzaŋ </ts>
               <ts e="T56" id="Seg_1312" n="e" s="T55">kän </ts>
               <ts e="T57" id="Seg_1314" n="e" s="T56">tolʼdʼizä </ts>
               <ts e="T58" id="Seg_1316" n="e" s="T57">matʼtʼöndə, </ts>
               <ts e="T59" id="Seg_1318" n="e" s="T58">taːɣɨn </ts>
               <ts e="T60" id="Seg_1320" n="e" s="T59">poŋgɨrlʼe </ts>
               <ts e="T61" id="Seg_1322" n="e" s="T60">i </ts>
               <ts e="T62" id="Seg_1324" n="e" s="T61">sapɨɣɨrlʼe </ts>
               <ts e="T63" id="Seg_1326" n="e" s="T62">(sappɨɣɨrgu). </ts>
               <ts e="T64" id="Seg_1328" n="e" s="T63">nilʼdʼiŋ </ts>
               <ts e="T65" id="Seg_1330" n="e" s="T64">mat </ts>
               <ts e="T66" id="Seg_1332" n="e" s="T65">ilɨkuzaŋ. </ts>
               <ts e="T67" id="Seg_1334" n="e" s="T66">ilʼdäw </ts>
               <ts e="T68" id="Seg_1336" n="e" s="T67">meŋnan </ts>
               <ts e="T69" id="Seg_1338" n="e" s="T68">soː </ts>
               <ts e="T70" id="Seg_1340" n="e" s="T69">eːkkus. </ts>
               <ts e="T71" id="Seg_1342" n="e" s="T70">moːtku </ts>
               <ts e="T72" id="Seg_1344" n="e" s="T71">mazɨŋ </ts>
               <ts e="T73" id="Seg_1346" n="e" s="T72">ässäw </ts>
               <ts e="T74" id="Seg_1348" n="e" s="T73">äːzundɨ, </ts>
               <ts e="T75" id="Seg_1350" n="e" s="T74">ilʼdäw </ts>
               <ts e="T76" id="Seg_1352" n="e" s="T75">peldɨkus </ts>
               <ts e="T77" id="Seg_1354" n="e" s="T76">(peldɨkuzan). </ts>
               <ts e="T78" id="Seg_1356" n="e" s="T77">täpsä </ts>
               <ts e="T79" id="Seg_1358" n="e" s="T78">man </ts>
               <ts e="T80" id="Seg_1360" n="e" s="T79">qondukuzaŋ. </ts>
               <ts e="T81" id="Seg_1362" n="e" s="T80">kuː </ts>
               <ts e="T82" id="Seg_1364" n="e" s="T81">täp </ts>
               <ts e="T83" id="Seg_1366" n="e" s="T82">palʼdʼükus, </ts>
               <ts e="T84" id="Seg_1368" n="e" s="T83">täpsä </ts>
               <ts e="T85" id="Seg_1370" n="e" s="T84">man </ts>
               <ts e="T86" id="Seg_1372" n="e" s="T85">äːkuzaŋ. </ts>
               <ts e="T87" id="Seg_1374" n="e" s="T86">meŋnan </ts>
               <ts e="T88" id="Seg_1376" n="e" s="T87">äːsan </ts>
               <ts e="T89" id="Seg_1378" n="e" s="T88">čičew. </ts>
               <ts e="T90" id="Seg_1380" n="e" s="T89">nimdə </ts>
               <ts e="T91" id="Seg_1382" n="e" s="T90">täbɨn </ts>
               <ts e="T92" id="Seg_1384" n="e" s="T91">Мihail. </ts>
               <ts e="T93" id="Seg_1386" n="e" s="T92">mazɨŋ </ts>
               <ts e="T94" id="Seg_1388" n="e" s="T93">soːrrɨkus. </ts>
               <ts e="T95" id="Seg_1390" n="e" s="T94">oːɣɨlǯikus </ts>
               <ts e="T96" id="Seg_1392" n="e" s="T95">palʼdʼügu </ts>
               <ts e="T97" id="Seg_1394" n="e" s="T96">tolʼdʼin, </ts>
               <ts e="T98" id="Seg_1396" n="e" s="T97">aːnduzä, </ts>
               <ts e="T99" id="Seg_1398" n="e" s="T98">nʼäbɨjgu, </ts>
               <ts e="T100" id="Seg_1400" n="e" s="T99">nʼäjajgu, </ts>
               <ts e="T101" id="Seg_1402" n="e" s="T100">tüːlʼəsän </ts>
               <ts e="T102" id="Seg_1404" n="e" s="T101">(tüːləsäzä) </ts>
               <ts e="T103" id="Seg_1406" n="e" s="T102">tʼätǯikugu, </ts>
               <ts e="T104" id="Seg_1408" n="e" s="T103">kündɨnbarɣɨn </ts>
               <ts e="T105" id="Seg_1410" n="e" s="T104">aːmdɨgu. </ts>
               <ts e="T106" id="Seg_1412" n="e" s="T105">äːzɨn </ts>
               <ts e="T107" id="Seg_1414" n="e" s="T106">müːttäɣɨn </ts>
               <ts e="T108" id="Seg_1416" n="e" s="T107">täbɨm </ts>
               <ts e="T109" id="Seg_1418" n="e" s="T108">qwätpattə. </ts>
               <ts e="T110" id="Seg_1420" n="e" s="T109">meŋnan </ts>
               <ts e="T111" id="Seg_1422" n="e" s="T110">eːsan </ts>
               <ts e="T112" id="Seg_1424" n="e" s="T111">ojöw. </ts>
               <ts e="T113" id="Seg_1426" n="e" s="T112">täp </ts>
               <ts e="T114" id="Seg_1428" n="e" s="T113">äːkus </ts>
               <ts e="T115" id="Seg_1430" n="e" s="T114">püdokus </ts>
               <ts e="T116" id="Seg_1432" n="e" s="T115">qwärɣänni. </ts>
               <ts e="T117" id="Seg_1434" n="e" s="T116">meŋa </ts>
               <ts e="T118" id="Seg_1436" n="e" s="T117">qwäːdəmbɨkus, </ts>
               <ts e="T119" id="Seg_1438" n="e" s="T118">mazɨŋ </ts>
               <ts e="T120" id="Seg_1440" n="e" s="T119">moːtkuzɨt. </ts>
               <ts e="T121" id="Seg_1442" n="e" s="T120">tidam </ts>
               <ts e="T122" id="Seg_1444" n="e" s="T121">naj </ts>
               <ts e="T123" id="Seg_1446" n="e" s="T122">qumba. </ts>
               <ts e="T124" id="Seg_1448" n="e" s="T123">oːɣulʼǯukuzaŋ </ts>
               <ts e="T125" id="Seg_1450" n="e" s="T124">okkə </ts>
               <ts e="T126" id="Seg_1452" n="e" s="T125">klasqɨnä </ts>
               <ts e="T127" id="Seg_1454" n="e" s="T126">äːttoɣɨn. </ts>
               <ts e="T128" id="Seg_1456" n="e" s="T127">nimdə </ts>
               <ts e="T129" id="Seg_1458" n="e" s="T128">täbɨn </ts>
               <ts e="T130" id="Seg_1460" n="e" s="T129">äːkus </ts>
               <ts e="T131" id="Seg_1462" n="e" s="T130">Pristanʼ. </ts>
               <ts e="T132" id="Seg_1464" n="e" s="T131">tida </ts>
               <ts e="T133" id="Seg_1466" n="e" s="T132">na </ts>
               <ts e="T134" id="Seg_1468" n="e" s="T133">Pristanʼ </ts>
               <ts e="T135" id="Seg_1470" n="e" s="T134">nimdə </ts>
               <ts e="T136" id="Seg_1472" n="e" s="T135">tʼeɣa </ts>
               <ts e="T137" id="Seg_1474" n="e" s="T136">qäː, </ts>
               <ts e="T138" id="Seg_1476" n="e" s="T137">innäː-qäːtqi </ts>
               <ts e="T139" id="Seg_1478" n="e" s="T138">rajon. </ts>
               <ts e="T140" id="Seg_1480" n="e" s="T139">tättə </ts>
               <ts e="T141" id="Seg_1482" n="e" s="T140">klasɨm </ts>
               <ts e="T142" id="Seg_1484" n="e" s="T141">malčizaw </ts>
               <ts e="T143" id="Seg_1486" n="e" s="T142">Maksɨ </ts>
               <ts e="T144" id="Seg_1488" n="e" s="T143">matʼtʼöɣɨn. </ts>
               <ts e="T145" id="Seg_1490" n="e" s="T144">Maksɨ </ts>
               <ts e="T146" id="Seg_1492" n="e" s="T145">maːtʼtʼoɣənnɨ </ts>
               <ts e="T147" id="Seg_1494" n="e" s="T146">qwässaŋ </ts>
               <ts e="T148" id="Seg_1496" n="e" s="T147">oːɣulǯukugu </ts>
               <ts e="T149" id="Seg_1498" n="e" s="T148">kargasokskij </ts>
               <ts e="T150" id="Seg_1500" n="e" s="T149">rajondə, </ts>
               <ts e="T151" id="Seg_1502" n="e" s="T150">škola </ts>
               <ts e="T152" id="Seg_1504" n="e" s="T151">nimdə </ts>
               <ts e="T153" id="Seg_1506" n="e" s="T152">Vʼertʼekos. </ts>
               <ts e="T154" id="Seg_1508" n="e" s="T153">niːn </ts>
               <ts e="T155" id="Seg_1510" n="e" s="T154">man </ts>
               <ts e="T156" id="Seg_1512" n="e" s="T155">malčizaw </ts>
               <ts e="T157" id="Seg_1514" n="e" s="T156">sälʼdʼü </ts>
               <ts e="T158" id="Seg_1516" n="e" s="T157">klassam. </ts>
               <ts e="T159" id="Seg_1518" n="e" s="T158">nänno </ts>
               <ts e="T160" id="Seg_1520" n="e" s="T159">man </ts>
               <ts e="T161" id="Seg_1522" n="e" s="T160">oːɣulǯukuzaŋ </ts>
               <ts e="T162" id="Seg_1524" n="e" s="T161">kalpašiɣɨn </ts>
               <ts e="T163" id="Seg_1526" n="e" s="T162">pedučilʼiše. </ts>
               <ts e="T164" id="Seg_1528" n="e" s="T163">poɣɨndə </ts>
               <ts e="T165" id="Seg_1530" n="e" s="T164">oːɣuǯɨkuzaŋ </ts>
               <ts e="T166" id="Seg_1532" n="e" s="T165">padgatavitelʼnɨj </ts>
               <ts e="T167" id="Seg_1534" n="e" s="T166">kursäɣɨn </ts>
               <ts e="T168" id="Seg_1536" n="e" s="T167">i </ts>
               <ts e="T169" id="Seg_1538" n="e" s="T168">puːssaŋ </ts>
               <ts e="T170" id="Seg_1540" n="e" s="T169">man </ts>
               <ts e="T171" id="Seg_1542" n="e" s="T170">okkɨ </ts>
               <ts e="T172" id="Seg_1544" n="e" s="T171">kursanda. </ts>
               <ts e="T173" id="Seg_1546" n="e" s="T172">okkə </ts>
               <ts e="T174" id="Seg_1548" n="e" s="T173">kursaɣannä </ts>
               <ts e="T320" id="Seg_1550" n="e" s="T174">naːr </ts>
               <ts e="T175" id="Seg_1552" n="e" s="T320">saːrum </ts>
               <ts e="T176" id="Seg_1554" n="e" s="T175">okkɨrtʼädʼigʼöt </ts>
               <ts e="T177" id="Seg_1556" n="e" s="T176">poɣəndɨ </ts>
               <ts e="T178" id="Seg_1558" n="e" s="T177">mazɨŋ </ts>
               <ts e="T179" id="Seg_1560" n="e" s="T178">iːsattə </ts>
               <ts e="T180" id="Seg_1562" n="e" s="T179">nʼärɣa </ts>
               <ts e="T181" id="Seg_1564" n="e" s="T180">arminda. </ts>
               <ts e="T182" id="Seg_1566" n="e" s="T181">nɨtʼän </ts>
               <ts e="T183" id="Seg_1568" n="e" s="T182">man </ts>
               <ts e="T184" id="Seg_1570" n="e" s="T183">eːsaŋ </ts>
               <ts e="T185" id="Seg_1572" n="e" s="T184">sälʼdʼü </ts>
               <ts e="T186" id="Seg_1574" n="e" s="T185">pon. </ts>
               <ts e="T321" id="Seg_1576" n="e" s="T186">tättə </ts>
               <ts e="T187" id="Seg_1578" n="e" s="T321">saːrum </ts>
               <ts e="T188" id="Seg_1580" n="e" s="T187">muqtutqwej </ts>
               <ts e="T189" id="Seg_1582" n="e" s="T188">pon </ts>
               <ts e="T190" id="Seg_1584" n="e" s="T189">mazɨŋ </ts>
               <ts e="T191" id="Seg_1586" n="e" s="T190">ütattə </ts>
               <ts e="T192" id="Seg_1588" n="e" s="T191">matqɨneŋ. </ts>
               <ts e="T193" id="Seg_1590" n="e" s="T192">na </ts>
               <ts e="T194" id="Seg_1592" n="e" s="T193">pon </ts>
               <ts e="T195" id="Seg_1594" n="e" s="T194">man </ts>
               <ts e="T196" id="Seg_1596" n="e" s="T195">tüːssaŋ </ts>
               <ts e="T197" id="Seg_1598" n="e" s="T196">tʼeːɣə </ts>
               <ts e="T198" id="Seg_1600" n="e" s="T197">matšondə. </ts>
               <ts e="T199" id="Seg_1602" n="e" s="T198">tämdə </ts>
               <ts e="T201" id="Seg_1604" n="e" s="T199">naʒə </ts>
               <ts e="T202" id="Seg_1606" n="e" s="T201">bon </ts>
               <ts e="T203" id="Seg_1608" n="e" s="T202">man </ts>
               <ts e="T204" id="Seg_1610" n="e" s="T203">nätɨzaŋ. </ts>
               <ts e="T205" id="Seg_1612" n="e" s="T204">poɣɨndə </ts>
               <ts e="T206" id="Seg_1614" n="e" s="T205">äːsaŋ </ts>
               <ts e="T207" id="Seg_1616" n="e" s="T206">Arlopkaɣɨn. </ts>
               <ts e="T208" id="Seg_1618" n="e" s="T207">nännɨ </ts>
               <ts e="T209" id="Seg_1620" n="e" s="T208">laqqɨzaŋ </ts>
               <ts e="T210" id="Seg_1622" n="e" s="T209">tʼeɣə </ts>
               <ts e="T211" id="Seg_1624" n="e" s="T210">mačoɣɨn. </ts>
               <ts e="T212" id="Seg_1626" n="e" s="T211">tʼeɣə </ts>
               <ts e="T213" id="Seg_1628" n="e" s="T212">mačoɣənnɨ </ts>
               <ts e="T214" id="Seg_1630" n="e" s="T213">mazɨŋ </ts>
               <ts e="T215" id="Seg_1632" n="e" s="T214">üːdizattə </ts>
               <ts e="T216" id="Seg_1634" n="e" s="T215">ustʼ_azʼörnɨj </ts>
               <ts e="T217" id="Seg_1636" n="e" s="T216">aːntondə. </ts>
               <ts e="T218" id="Seg_1638" n="e" s="T217">aːntoːɣɨn </ts>
               <ts e="T219" id="Seg_1640" n="e" s="T218">laqqɨzaŋ </ts>
               <ts e="T220" id="Seg_1642" n="e" s="T219">klupqɨn, </ts>
               <ts e="T221" id="Seg_1644" n="e" s="T220">nännä </ts>
               <ts e="T222" id="Seg_1646" n="e" s="T221">qwälɨlʼdʼi </ts>
               <ts e="T223" id="Seg_1648" n="e" s="T222">artʼelʼɣɨn </ts>
               <ts e="T224" id="Seg_1650" n="e" s="T223">(predsedatelem), </ts>
               <ts e="T225" id="Seg_1652" n="e" s="T224">nännə </ts>
               <ts e="T226" id="Seg_1654" n="e" s="T225">selʼsavetqɨt </ts>
               <ts e="T227" id="Seg_1656" n="e" s="T226">predsedatelem </ts>
               <ts e="T228" id="Seg_1658" n="e" s="T227">selʼsoveta. </ts>
               <ts e="T229" id="Seg_1660" n="e" s="T228">laqaŋ </ts>
               <ts e="T230" id="Seg_1662" n="e" s="T229">nändə. </ts>
               <ts e="T231" id="Seg_1664" n="e" s="T230">man </ts>
               <ts e="T232" id="Seg_1666" n="e" s="T231">nädɨmbaŋ. </ts>
               <ts e="T233" id="Seg_1668" n="e" s="T232">man </ts>
               <ts e="T234" id="Seg_1670" n="e" s="T233">pajannɨzä </ts>
               <ts e="T235" id="Seg_1672" n="e" s="T234">ilaŋ </ts>
               <ts e="T236" id="Seg_1674" n="e" s="T235">šɨttətʼädʼiqwäjgʼöt </ts>
               <ts e="T237" id="Seg_1676" n="e" s="T236">pottə. </ts>
               <ts e="T238" id="Seg_1678" n="e" s="T237">učej </ts>
               <ts e="T239" id="Seg_1680" n="e" s="T238">maːdə </ts>
               <ts e="T240" id="Seg_1682" n="e" s="T239">umbaŋ: </ts>
               <ts e="T241" id="Seg_1684" n="e" s="T240">sombɨlʼe </ts>
               <ts e="T242" id="Seg_1686" n="e" s="T241">iːwə </ts>
               <ts e="T243" id="Seg_1688" n="e" s="T242">i </ts>
               <ts e="T244" id="Seg_1690" n="e" s="T243">šɨttə </ts>
               <ts e="T245" id="Seg_1692" n="e" s="T244">näwä. </ts>
               <ts e="T246" id="Seg_1694" n="e" s="T245">pajaw </ts>
               <ts e="T247" id="Seg_1696" n="e" s="T246">qassaɣi. </ts>
               <ts e="T248" id="Seg_1698" n="e" s="T247">üːčelaw </ts>
               <ts e="T319" id="Seg_1700" n="e" s="T248">süsüɣulʼ </ts>
               <ts e="T249" id="Seg_1702" n="e" s="T319">dʼe </ts>
               <ts e="T250" id="Seg_1704" n="e" s="T249">assä </ts>
               <ts e="T251" id="Seg_1706" n="e" s="T250">čenʼčuattə. </ts>
               <ts e="T252" id="Seg_1708" n="e" s="T251">wärɣə </ts>
               <ts e="T253" id="Seg_1710" n="e" s="T252">iːwä </ts>
               <ts e="T254" id="Seg_1712" n="e" s="T253">malčimbat </ts>
               <ts e="T255" id="Seg_1714" n="e" s="T254">tam </ts>
               <ts e="T256" id="Seg_1716" n="e" s="T255">bon </ts>
               <ts e="T257" id="Seg_1718" n="e" s="T256">köt </ts>
               <ts e="T258" id="Seg_1720" n="e" s="T257">klazɨm. </ts>
               <ts e="T259" id="Seg_1722" n="e" s="T258">nʼünʼo </ts>
               <ts e="T260" id="Seg_1724" n="e" s="T259">iːɣəneŋ </ts>
               <ts e="T261" id="Seg_1726" n="e" s="T260">sombəlʼeːŋə </ts>
               <ts e="T262" id="Seg_1728" n="e" s="T261">muqtumdälǯi </ts>
               <ts e="T263" id="Seg_1730" n="e" s="T262">pottə. </ts>
               <ts e="T264" id="Seg_1732" n="e" s="T263">okkɨ </ts>
               <ts e="T265" id="Seg_1734" n="e" s="T264">iːwa </ts>
               <ts e="T266" id="Seg_1736" n="e" s="T265">opsä </ts>
               <ts e="T267" id="Seg_1738" n="e" s="T266">küdas. </ts>
               <ts e="T268" id="Seg_1740" n="e" s="T267">täbɨm </ts>
               <ts e="T269" id="Seg_1742" n="e" s="T268">qwändəkuzattə </ts>
               <ts e="T270" id="Seg_1744" n="e" s="T269">Tomdə. </ts>
               <ts e="T271" id="Seg_1746" n="e" s="T270">täbɨm </ts>
               <ts e="T272" id="Seg_1748" n="e" s="T271">nɨtän </ts>
               <ts e="T273" id="Seg_1750" n="e" s="T272">soːmǯattə, </ts>
               <ts e="T274" id="Seg_1752" n="e" s="T273">tatpattə </ts>
               <ts e="T275" id="Seg_1754" n="e" s="T274">qottä. </ts>
               <ts e="T276" id="Seg_1756" n="e" s="T275">man </ts>
               <ts e="T277" id="Seg_1758" n="e" s="T276">üːčelaw </ts>
               <ts e="T278" id="Seg_1760" n="e" s="T277">süsüqula. </ts>
               <ts e="T279" id="Seg_1762" n="e" s="T278">täppɨla </ts>
               <ts e="T280" id="Seg_1764" n="e" s="T279">oːɣulǯuqwattə </ts>
               <ts e="T281" id="Seg_1766" n="e" s="T280">qomdägalɨk. </ts>
               <ts e="T282" id="Seg_1768" n="e" s="T281">onäŋ </ts>
               <ts e="T283" id="Seg_1770" n="e" s="T282">man </ts>
               <ts e="T284" id="Seg_1772" n="e" s="T283">robiŋwaw </ts>
               <ts e="T322" id="Seg_1774" n="e" s="T284">tättä </ts>
               <ts e="T285" id="Seg_1776" n="e" s="T322">saːrum </ts>
               <ts e="T286" id="Seg_1778" n="e" s="T285">sombɨlʼe </ts>
               <ts e="T287" id="Seg_1780" n="e" s="T286">qwäjgʼöt </ts>
               <ts e="T288" id="Seg_1782" n="e" s="T287">lʼebɨm </ts>
               <ts e="T289" id="Seg_1784" n="e" s="T288">ireɣəndə. </ts>
               <ts e="T290" id="Seg_1786" n="e" s="T289">ilaŋ </ts>
               <ts e="T291" id="Seg_1788" n="e" s="T290">man </ts>
               <ts e="T292" id="Seg_1790" n="e" s="T291">soːŋ. </ts>
               <ts e="T293" id="Seg_1792" n="e" s="T292">äran </ts>
               <ts e="T294" id="Seg_1794" n="e" s="T293">swäŋajgwaŋ, </ts>
               <ts e="T295" id="Seg_1796" n="e" s="T294">säŋɨigwaŋ, </ts>
               <ts e="T296" id="Seg_1798" n="e" s="T295">čoppɨräigwaŋ, </ts>
               <ts e="T297" id="Seg_1800" n="e" s="T296">suːrujigwaŋ. </ts>
               <ts e="T298" id="Seg_1802" n="e" s="T297">taːɣɨn </ts>
               <ts e="T299" id="Seg_1804" n="e" s="T298">poŋgɨrɨgwaŋ, </ts>
               <ts e="T300" id="Seg_1806" n="e" s="T299">qwälɨjgwaŋ. </ts>
               <ts e="T301" id="Seg_1808" n="e" s="T300">tɨdam </ts>
               <ts e="T302" id="Seg_1810" n="e" s="T301">kɨkkaŋ </ts>
               <ts e="T303" id="Seg_1812" n="e" s="T302">täwɨgu </ts>
               <ts e="T304" id="Seg_1814" n="e" s="T303">sändə </ts>
               <ts e="T305" id="Seg_1816" n="e" s="T304">maːdəm. </ts>
               <ts e="T306" id="Seg_1818" n="e" s="T305">onäŋ </ts>
               <ts e="T307" id="Seg_1820" n="e" s="T306">manmə </ts>
               <ts e="T308" id="Seg_1822" n="e" s="T307">täːmba. </ts>
               <ts e="T309" id="Seg_1824" n="e" s="T308">sändə </ts>
               <ts e="T310" id="Seg_1826" n="e" s="T309">maːdɨm </ts>
               <ts e="T311" id="Seg_1828" n="e" s="T310">täwɨgu </ts>
               <ts e="T312" id="Seg_1830" n="e" s="T311">kɨkkaŋ </ts>
               <ts e="T323" id="Seg_1832" n="e" s="T312">muqtut </ts>
               <ts e="T313" id="Seg_1834" n="e" s="T323"> saːrum </ts>
               <ts e="T314" id="Seg_1836" n="e" s="T313">lʼependə. </ts>
               <ts e="T315" id="Seg_1838" n="e" s="T314">man </ts>
               <ts e="T316" id="Seg_1840" n="e" s="T315">täbɨm </ts>
               <ts e="T317" id="Seg_1842" n="e" s="T316">täwenǯaw </ts>
               <ts e="T318" id="Seg_1844" n="e" s="T317">ilʼenǯaŋ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T11" id="Seg_1845" s="T0">KMS_1963_LifestoryFull_nar.001 (001.001)</ta>
            <ta e="T14" id="Seg_1846" s="T11">KMS_1963_LifestoryFull_nar.002 (001.002)</ta>
            <ta e="T18" id="Seg_1847" s="T14">KMS_1963_LifestoryFull_nar.003 (001.003)</ta>
            <ta e="T25" id="Seg_1848" s="T18">KMS_1963_LifestoryFull_nar.004 (001.004)</ta>
            <ta e="T32" id="Seg_1849" s="T25">KMS_1963_LifestoryFull_nar.005 (001.005)</ta>
            <ta e="T34" id="Seg_1850" s="T32">KMS_1963_LifestoryFull_nar.006 (001.006)</ta>
            <ta e="T38" id="Seg_1851" s="T34">KMS_1963_LifestoryFull_nar.007 (001.007)</ta>
            <ta e="T43" id="Seg_1852" s="T38">KMS_1963_LifestoryFull_nar.008 (001.008)</ta>
            <ta e="T49" id="Seg_1853" s="T43">KMS_1963_LifestoryFull_nar.009 (001.009)</ta>
            <ta e="T52" id="Seg_1854" s="T49">KMS_1963_LifestoryFull_nar.010 (001.010)</ta>
            <ta e="T63" id="Seg_1855" s="T52">KMS_1963_LifestoryFull_nar.011 (001.011)</ta>
            <ta e="T66" id="Seg_1856" s="T63">KMS_1963_LifestoryFull_nar.012 (001.012)</ta>
            <ta e="T70" id="Seg_1857" s="T66">KMS_1963_LifestoryFull_nar.013 (001.013)</ta>
            <ta e="T77" id="Seg_1858" s="T70">KMS_1963_LifestoryFull_nar.014 (001.014)</ta>
            <ta e="T80" id="Seg_1859" s="T77">KMS_1963_LifestoryFull_nar.015 (001.015)</ta>
            <ta e="T86" id="Seg_1860" s="T80">KMS_1963_LifestoryFull_nar.016 (001.016)</ta>
            <ta e="T89" id="Seg_1861" s="T86">KMS_1963_LifestoryFull_nar.017 (001.017)</ta>
            <ta e="T92" id="Seg_1862" s="T89">KMS_1963_LifestoryFull_nar.018 (001.018)</ta>
            <ta e="T94" id="Seg_1863" s="T92">KMS_1963_LifestoryFull_nar.019 (001.019)</ta>
            <ta e="T105" id="Seg_1864" s="T94">KMS_1963_LifestoryFull_nar.020 (001.020)</ta>
            <ta e="T109" id="Seg_1865" s="T105">KMS_1963_LifestoryFull_nar.021 (001.021)</ta>
            <ta e="T112" id="Seg_1866" s="T109">KMS_1963_LifestoryFull_nar.022 (001.022)</ta>
            <ta e="T116" id="Seg_1867" s="T112">KMS_1963_LifestoryFull_nar.023 (001.023)</ta>
            <ta e="T120" id="Seg_1868" s="T116">KMS_1963_LifestoryFull_nar.024 (001.024)</ta>
            <ta e="T123" id="Seg_1869" s="T120">KMS_1963_LifestoryFull_nar.025 (001.025)</ta>
            <ta e="T127" id="Seg_1870" s="T123">KMS_1963_LifestoryFull_nar.026 (001.026)</ta>
            <ta e="T131" id="Seg_1871" s="T127">KMS_1963_LifestoryFull_nar.027 (001.027)</ta>
            <ta e="T139" id="Seg_1872" s="T131">KMS_1963_LifestoryFull_nar.028 (001.028)</ta>
            <ta e="T144" id="Seg_1873" s="T139">KMS_1963_LifestoryFull_nar.029 (001.029)</ta>
            <ta e="T153" id="Seg_1874" s="T144">KMS_1963_LifestoryFull_nar.030 (001.030)</ta>
            <ta e="T158" id="Seg_1875" s="T153">KMS_1963_LifestoryFull_nar.031 (001.031)</ta>
            <ta e="T163" id="Seg_1876" s="T158">KMS_1963_LifestoryFull_nar.032 (001.032)</ta>
            <ta e="T172" id="Seg_1877" s="T163">KMS_1963_LifestoryFull_nar.033 (001.033)</ta>
            <ta e="T181" id="Seg_1878" s="T172">KMS_1963_LifestoryFull_nar.034 (001.034)</ta>
            <ta e="T186" id="Seg_1879" s="T181">KMS_1963_LifestoryFull_nar.035 (001.035)</ta>
            <ta e="T192" id="Seg_1880" s="T186">KMS_1963_LifestoryFull_nar.036 (001.036)</ta>
            <ta e="T198" id="Seg_1881" s="T192">KMS_1963_LifestoryFull_nar.037 (001.037)</ta>
            <ta e="T204" id="Seg_1882" s="T198">KMS_1963_LifestoryFull_nar.038 (001.038)</ta>
            <ta e="T207" id="Seg_1883" s="T204">KMS_1963_LifestoryFull_nar.039 (001.039)</ta>
            <ta e="T211" id="Seg_1884" s="T207">KMS_1963_LifestoryFull_nar.040 (001.040)</ta>
            <ta e="T217" id="Seg_1885" s="T211">KMS_1963_LifestoryFull_nar.041 (001.041)</ta>
            <ta e="T228" id="Seg_1886" s="T217">KMS_1963_LifestoryFull_nar.042 (001.042)</ta>
            <ta e="T230" id="Seg_1887" s="T228">KMS_1963_LifestoryFull_nar.043 (001.043)</ta>
            <ta e="T232" id="Seg_1888" s="T230">KMS_1963_LifestoryFull_nar.044 (001.044)</ta>
            <ta e="T237" id="Seg_1889" s="T232">KMS_1963_LifestoryFull_nar.045 (001.045)</ta>
            <ta e="T245" id="Seg_1890" s="T237">KMS_1963_LifestoryFull_nar.046 (001.046)</ta>
            <ta e="T247" id="Seg_1891" s="T245">KMS_1963_LifestoryFull_nar.047 (001.047)</ta>
            <ta e="T251" id="Seg_1892" s="T247">KMS_1963_LifestoryFull_nar.048 (001.048)</ta>
            <ta e="T258" id="Seg_1893" s="T251">KMS_1963_LifestoryFull_nar.049 (001.049)</ta>
            <ta e="T263" id="Seg_1894" s="T258">KMS_1963_LifestoryFull_nar.050 (001.050)</ta>
            <ta e="T267" id="Seg_1895" s="T263">KMS_1963_LifestoryFull_nar.051 (001.051)</ta>
            <ta e="T270" id="Seg_1896" s="T267">KMS_1963_LifestoryFull_nar.052 (001.052)</ta>
            <ta e="T275" id="Seg_1897" s="T270">KMS_1963_LifestoryFull_nar.053 (001.053)</ta>
            <ta e="T278" id="Seg_1898" s="T275">KMS_1963_LifestoryFull_nar.054 (001.054)</ta>
            <ta e="T281" id="Seg_1899" s="T278">KMS_1963_LifestoryFull_nar.055 (001.055)</ta>
            <ta e="T289" id="Seg_1900" s="T281">KMS_1963_LifestoryFull_nar.056 (001.056)</ta>
            <ta e="T292" id="Seg_1901" s="T289">KMS_1963_LifestoryFull_nar.057 (001.057)</ta>
            <ta e="T297" id="Seg_1902" s="T292">KMS_1963_LifestoryFull_nar.058 (001.058)</ta>
            <ta e="T300" id="Seg_1903" s="T297">KMS_1963_LifestoryFull_nar.059 (001.059)</ta>
            <ta e="T305" id="Seg_1904" s="T300">KMS_1963_LifestoryFull_nar.060 (001.060)</ta>
            <ta e="T308" id="Seg_1905" s="T305">KMS_1963_LifestoryFull_nar.061 (001.061)</ta>
            <ta e="T314" id="Seg_1906" s="T308">KMS_1963_LifestoryFull_nar.062 (001.062)</ta>
            <ta e="T318" id="Seg_1907" s="T314">KMS_1963_LifestoryFull_nar.063 (001.063)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T11" id="Seg_1908" s="T0">о̄′руң′баң ′муkтут ′kwе(ӓ̄)йгʼӧт поɣындъ ′шыттъ са̄рум на̄р kwе(ӓ)йгʼӧт чис′лоɣындъ сен′тʼӓбрʼ ′месʼӓцkын ӓ̄ттоɣын.</ta>
            <ta e="T14" id="Seg_1909" s="T11">ӓда нимдъ ′ко̄рела.</ta>
            <ta e="T18" id="Seg_1910" s="T14">нимдъгу мазың ма′дʼӧ ′ко̄рʼела.</ta>
            <ta e="T25" id="Seg_1911" s="T18">о′нӓң ′kӯлаw (кун′доkын) ′укон ′kwӓлыйгу′заттъ и ′сӯруйгу′заттъ.</ta>
            <ta e="T32" id="Seg_1912" s="T25">′меңа ′е(ӓ)сан ′на̄р поттъ, ман kал(л)ы′заң е′веw′наннӓ.</ta>
            <ta e="T34" id="Seg_1913" s="T32">е′вʼеw ′kӯссан.</ta>
            <ta e="T38" id="Seg_1914" s="T34">е′веwвнан е(ӓ̄)саттъ ӱ′челат, ′kӯку‵заттъ.</ta>
            <ta e="T43" id="Seg_1915" s="T38">о′ккыр ман е′веwнан, е′сеwнан ‵kалым′баң.</ta>
            <ta e="T49" id="Seg_1916" s="T43">ӓ′ссӓw палʼдʼу′к(к)ус ′суруйгу, ′kwӓ̄лыйгу, ′ма̄тkын ′тʼӓңукус.</ta>
            <ta e="T52" id="Seg_1917" s="T49">‵kалыкку′заң, ′илык(к)узаң илʼ′дʼӓwсӓ.</ta>
            <ta e="T63" id="Seg_1918" s="T52">тап′сӓ ман ‵палʼдʼику′заң кӓн толʼдʼи′зӓ ма′тʼтʼӧндъ, та̄′ɣын ‵поңгыр′лʼе и ′сап(п)ыɣыр′лʼе (саппыɣыргу).</ta>
            <ta e="T66" id="Seg_1919" s="T63">нилʼдʼиң мат ‵илыку′заң.</ta>
            <ta e="T70" id="Seg_1920" s="T66">илʼ′дӓw ′меңнан со̄ ′е̄(ӓ̄)ккус.</ta>
            <ta e="T77" id="Seg_1921" s="T70">′мо̄тку ′ма′зың ӓ′ссӓw ′ӓ̄зунды, илʼ′дӓw ′пелдыкус (′пелдыку′зан).</ta>
            <ta e="T80" id="Seg_1922" s="T77">тӓп′сӓ ман kондуку′заң.</ta>
            <ta e="T86" id="Seg_1923" s="T80">кӯ тӓп палʼдʼӱ′кус, тӓп′сӓ ман ′ӓ̄кузаң.</ta>
            <ta e="T89" id="Seg_1924" s="T86">′меңнан ′ӓ̄сан тши′тшеw.</ta>
            <ta e="T92" id="Seg_1925" s="T89">′нимдъ ′тӓбын Михаил.</ta>
            <ta e="T94" id="Seg_1926" s="T92">′мазың ′со̄ррыкус.</ta>
            <ta e="T105" id="Seg_1927" s="T94">′о̄ɣылджикус палʼдʼӱ′гу толʼ′дʼин, а̄нду′зӓ, ′нʼӓбы(а)йгу, нʼӓjайгу, тӱ̄l(л)ъ‵сӓн (тӱ̄лъсӓ′зӓ) ‵тʼӓтджику′гу, кӱндын′барɣын ′а̄мдыгу.</ta>
            <ta e="T109" id="Seg_1928" s="T105">′ӓ̄′зын ′мӱ̄т′тӓɣын тӓ′бым kwӓт′паттъ.</ta>
            <ta e="T112" id="Seg_1929" s="T109">′меңнан е̄(ӓ̄)сан о′jӧw.</ta>
            <ta e="T116" id="Seg_1930" s="T112">тӓп ′ӓ̄кус ′пӱдокус ′kwӓрɣӓнни.</ta>
            <ta e="T120" id="Seg_1931" s="T116">′меңа ′kwӓ̄дъмбы‵кус, ма′зың ′мо̄ткузыт.</ta>
            <ta e="T123" id="Seg_1932" s="T120">ти′дам най ′kумба.</ta>
            <ta e="T127" id="Seg_1933" s="T123">′о̄ɣулʼджукузаң ′оккъ ′класkынӓ ′ӓ̄ттоɣын.</ta>
            <ta e="T131" id="Seg_1934" s="T127">′нимдъ ′тӓбын ′ӓ̄кус Пристань.</ta>
            <ta e="T139" id="Seg_1935" s="T131">ти′да на Пристань ′нимдъ ′тʼеɣа kӓ̄, иннӓ̄-kӓ̄тkи район.</ta>
            <ta e="T144" id="Seg_1936" s="T139">′тӓттъ ′класым малтши′заw ′максы ма′тʼтʼӧɣын.</ta>
            <ta e="T153" id="Seg_1937" s="T144">′максы ма̄′тʼтʼоɣънны(ӓ) kwӓ′ссаң ′о̄ɣулджукугу карга′сокский рай′ондъ, школа ′нимдъ Вʼертʼе′кос.</ta>
            <ta e="T158" id="Seg_1938" s="T153">нӣн ман малтши′заw ′сӓлʼдʼӱ ′классам.</ta>
            <ta e="T163" id="Seg_1939" s="T158">′нӓнно(ъ) ман ′о̄ɣулджукузаң кал′паши(е)ɣын педу′чилʼище.</ta>
            <ta e="T172" id="Seg_1940" s="T163">′поɣындъ о̄ɣуджыку‵заң падгата′вителʼный ′курсӓɣын и ′пӯссаң ман ′оккы ′курсанда.</ta>
            <ta e="T181" id="Seg_1941" s="T172">′оккъ ′курсаɣаннӓ на̄р ′са̄рум ′оккыртʼӓдʼигʼӧт ′поɣънды ′мазың ′ӣсаттъ ′нʼӓрɣа ′арминда.</ta>
            <ta e="T186" id="Seg_1942" s="T181">ны′тʼӓн ман ′е̄(ӓ̄)саң сӓлʼдʼӱ пон.</ta>
            <ta e="T192" id="Seg_1943" s="T186">′тӓттъ ′са̄рум ′муkтут‵kwе(ӓ)й пон ма′зың ′ӱтаттъ матkы′нең.</ta>
            <ta e="T198" id="Seg_1944" s="T192">на ′пон ман ′тӱ̄ссаң ′тʼе̄ɣъ ма′тшондъ.</ta>
            <ta e="T204" id="Seg_1945" s="T198">′тӓмдъ на жъ б̂он ман нӓты′заң.</ta>
            <ta e="T207" id="Seg_1946" s="T204">′поɣындъ ′ӓ̄саң ар′лопкаɣын.</ta>
            <ta e="T211" id="Seg_1947" s="T207">′нӓнны ′лаkkы‵заң ′тʼеɣъ ма′тшоɣын.</ta>
            <ta e="T217" id="Seg_1948" s="T211">′тʼеɣъ ма′тшоɣънны(ъ) ′мазың ′ӱ̄ди‵заттъ устʼ азʼӧрный а̄н′тондъ.</ta>
            <ta e="T228" id="Seg_1949" s="T217">′а̄нто̄ɣын лаkkы′заң клупkын, ′нӓннӓ(ъ) kwӓлылʼдʼи артʼелʼɣын (председателем), ′нӓннъ селʼсаветkыт председателем сельсовета.</ta>
            <ta e="T230" id="Seg_1950" s="T228">ла′kаң ′нӓндъ.</ta>
            <ta e="T232" id="Seg_1951" s="T230">ман нӓдым′баң.</ta>
            <ta e="T237" id="Seg_1952" s="T232">ман па′jаннызӓ и′лаң ′шыттъ тʼӓдʼиkwӓйгʼӧт ′поттъ.</ta>
            <ta e="T245" id="Seg_1953" s="T237">учей ′ма̄дъ ‵ум′баң: сомбы′лʼе ′ӣwъ и ′шыттъ нӓвӓ.</ta>
            <ta e="T247" id="Seg_1954" s="T245">па′jаw kа′ссаɣи.</ta>
            <ta e="T251" id="Seg_1955" s="T247">ӱ̄′челаw сӱсӱɣулʼдʼе ассӓ ′ченʼчу‵аттъ.</ta>
            <ta e="T258" id="Seg_1956" s="T251">wӓрɣъ ′ӣwӓ малтшим′бат там′бон кӧт ′клазым.</ta>
            <ta e="T263" id="Seg_1957" s="T258">нʼӱнʼо ′ӣɣънең сомбъ′лʼе̄ңъ ′муkтум′дӓлджи ′поттъ.</ta>
            <ta e="T267" id="Seg_1958" s="T263">оккы ′ӣва ′опсӓ ′кӱдас.</ta>
            <ta e="T270" id="Seg_1959" s="T267">′тӓбым kwӓндъку′заттъ томдъ.</ta>
            <ta e="T275" id="Seg_1960" s="T270">′тӓбым ны′тӓн ′со̄мджаттъ, тат′паттъ kо′ттӓ.</ta>
            <ta e="T278" id="Seg_1961" s="T275">ман ӱ̄′челаw ′сӱсӱkула.</ta>
            <ta e="T281" id="Seg_1962" s="T278">тӓппы′ла ′о̄ɣулджуkwаттъ kом′дӓ‵галык.</ta>
            <ta e="T289" id="Seg_1963" s="T281">о′нӓң ман ′робиңваw ′тӓттӓ ′са̄рум сомбы′лʼе ′kwӓйгʼӧт ′лʼебым и′реɣъндъ.</ta>
            <ta e="T292" id="Seg_1964" s="T289">и′лаң ман со̄ң.</ta>
            <ta e="T297" id="Seg_1965" s="T292">ӓ′ран ′свӓңайгваң, ′сӓңыи′гваң, ′тшоппырӓ и′гваң, ′сӯруйигваң.</ta>
            <ta e="T300" id="Seg_1966" s="T297">та̄′ɣын ′поңгыры‵гваң, ′kwӓлый(и)‵гваң.</ta>
            <ta e="T305" id="Seg_1967" s="T300">ты′дам кы′ккаң ‵тӓввы′гу ′сӓндъ ′ма̄дъм.</ta>
            <ta e="T308" id="Seg_1968" s="T305">о′нӓң ′манмъ ′тӓ̄мба.</ta>
            <ta e="T314" id="Seg_1969" s="T308">′сӓндъ ′ма̄дым ′тӓввыгу кы′ккаң муkтут ′са̄рум ′лʼепендъ.</ta>
            <ta e="T318" id="Seg_1970" s="T314">ман ′тӓбым тӓ′венджаw и′лʼенджаң.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T11" id="Seg_1971" s="T0">oːruŋbaŋ muqtut qwe(äː)jgʼöt poɣɨndə šɨttə saːrum naːr qwe(ä)jgʼöt čisloɣɨndə sentʼäbrʼ mesʼäcqɨn äːttoɣɨn.</ta>
            <ta e="T14" id="Seg_1972" s="T11">äda nimdə koːrela.</ta>
            <ta e="T18" id="Seg_1973" s="T14">nimdəgu mazɨŋ madʼö koːrʼela.</ta>
            <ta e="T25" id="Seg_1974" s="T18">onäŋ quːlaw (kundoqɨn) ukon qwälɨjguzattə i suːrujguzattə.</ta>
            <ta e="T32" id="Seg_1975" s="T25">meŋa e(ä)san naːr pottə, man qal(l)ɨzaŋ evewnannä.</ta>
            <ta e="T34" id="Seg_1976" s="T32">evʼew quːssan.</ta>
            <ta e="T38" id="Seg_1977" s="T34">evewvnan e(äː)sattə üčelat, quːkuzattə.</ta>
            <ta e="T43" id="Seg_1978" s="T38">okkɨr man evewnan, esewnan qalɨmbaŋ.</ta>
            <ta e="T49" id="Seg_1979" s="T43">ässäw palʼdʼuk(k)us surujgu, qwäːlɨjgu, maːtqɨn tʼäŋukus.</ta>
            <ta e="T52" id="Seg_1980" s="T49">qalɨkkuzaŋ, ilɨk(k)uzaŋ ilʼdʼäwsä.</ta>
            <ta e="T63" id="Seg_1981" s="T52">tapsä man palʼdʼikuzaŋ kän tolʼdʼizä matʼtʼöndə, taːɣɨn poŋgɨrlʼe i sap(p)ɨɣɨrlʼe (sappɨɣɨrgu).</ta>
            <ta e="T66" id="Seg_1982" s="T63">nilʼdʼiŋ mat ilɨkuzaŋ.</ta>
            <ta e="T70" id="Seg_1983" s="T66">ilʼdäw meŋnan soː eː(äː)kkus.</ta>
            <ta e="T77" id="Seg_1984" s="T70">moːtku mazɨŋ ässäw äːzundɨ, ilʼdäw peldɨkus (peldɨkuzan).</ta>
            <ta e="T80" id="Seg_1985" s="T77">täpsä man qondukuzaŋ.</ta>
            <ta e="T86" id="Seg_1986" s="T80">kuː täp palʼdʼükus, täpsä man äːkuzaŋ.</ta>
            <ta e="T89" id="Seg_1987" s="T86">meŋnan äːsan tšitšew.</ta>
            <ta e="T92" id="Seg_1988" s="T89">nimdə täbɨn Мihail.</ta>
            <ta e="T94" id="Seg_1989" s="T92">mazɨŋ soːrrɨkus.</ta>
            <ta e="T105" id="Seg_1990" s="T94">oːɣɨlǯikus palʼdʼügu tolʼdʼin, aːnduzä, nʼäbɨ(a)jgu, nʼäjajgu, tüːlʼ(l)əsän (tüːləsäzä) tʼätǯikugu, kündɨnbarɣɨn aːmdɨgu.</ta>
            <ta e="T109" id="Seg_1991" s="T105">äːzɨn müːttäɣɨn täbɨm qwätpattə.</ta>
            <ta e="T112" id="Seg_1992" s="T109">meŋnan eː(äː)san ojöw.</ta>
            <ta e="T116" id="Seg_1993" s="T112">täp äːkus püdokus qwärɣänni.</ta>
            <ta e="T120" id="Seg_1994" s="T116">meŋa qwäːdəmbɨkus, mazɨŋ moːtkuzɨt.</ta>
            <ta e="T123" id="Seg_1995" s="T120">tidam naj qumba.</ta>
            <ta e="T127" id="Seg_1996" s="T123">oːɣulʼǯukuzaŋ okkə klasqɨnä äːttoɣɨn.</ta>
            <ta e="T131" id="Seg_1997" s="T127">nimdə täbɨn äːkus Pristanʼ.</ta>
            <ta e="T139" id="Seg_1998" s="T131">tida na Пristanʼ nimdə tʼeɣa qäː, innäː-qäːtqi rajon.</ta>
            <ta e="T144" id="Seg_1999" s="T139">tättə klasɨm maltšizaw maksɨ matʼtʼöɣɨn.</ta>
            <ta e="T153" id="Seg_2000" s="T144">maksɨ maːtʼtʼoɣənnɨ(ä) qwässaŋ oːɣulǯukugu kargasokskij rajondə, škola nimdə Vʼertʼekos.</ta>
            <ta e="T158" id="Seg_2001" s="T153">niːn man maltšizaw sälʼdʼü klassam.</ta>
            <ta e="T163" id="Seg_2002" s="T158">nänno(ə) man oːɣulǯukuzaŋ kalpaši(e)ɣɨn pedučilʼiše.</ta>
            <ta e="T172" id="Seg_2003" s="T163">poɣɨndə oːɣuǯɨkuzaŋ padgatavitelʼnɨj kursäɣɨn i puːssaŋ man okkɨ kursanda.</ta>
            <ta e="T181" id="Seg_2004" s="T172">okkə kursaɣannä naːr saːrum okkɨrtʼädʼigʼöt poɣəndɨ mazɨŋ iːsattə nʼärɣa arminda.</ta>
            <ta e="T186" id="Seg_2005" s="T181">nɨtʼän man eː(äː)saŋ sälʼdʼü pon.</ta>
            <ta e="T192" id="Seg_2006" s="T186">tättə saːrum muqtutqwe(ä)j pon mazɨŋ ütattə matqɨneŋ.</ta>
            <ta e="T198" id="Seg_2007" s="T192">na pon man tüːssaŋ tʼeːɣə matšondə.</ta>
            <ta e="T204" id="Seg_2008" s="T198">tämdə na ʒə b̂on man nätɨzaŋ.</ta>
            <ta e="T207" id="Seg_2009" s="T204">poɣɨndə äːsaŋ arlopkaɣɨn.</ta>
            <ta e="T211" id="Seg_2010" s="T207">nännɨ laqqɨzaŋ tʼeɣə matšoɣɨn.</ta>
            <ta e="T217" id="Seg_2011" s="T211">tʼeɣə matšoɣənnɨ(ə) mazɨŋ üːdizattə ustʼ azʼörnɨj aːntondə.</ta>
            <ta e="T228" id="Seg_2012" s="T217">aːntoːɣɨn laqqɨzaŋ klupqɨn, nännä(ə) qwälɨlʼdʼi artʼelʼɣɨn (predsedatelem), nännə selʼsavetqɨt predsedatelem selʼsoveta.</ta>
            <ta e="T230" id="Seg_2013" s="T228">laqaŋ nändə.</ta>
            <ta e="T232" id="Seg_2014" s="T230">man nädɨmbaŋ.</ta>
            <ta e="T237" id="Seg_2015" s="T232">man pajannɨzä ilaŋ šɨttə tʼädʼiqwäjgʼöt pottə.</ta>
            <ta e="T245" id="Seg_2016" s="T237">učej maːdə umbaŋ: sombɨlʼe iːwə i šɨttə nävä.</ta>
            <ta e="T247" id="Seg_2017" s="T245">pajaw qassaɣi.</ta>
            <ta e="T251" id="Seg_2018" s="T247">üːčelaw süsüɣulʼdʼe assä čenʼčuattə.</ta>
            <ta e="T258" id="Seg_2019" s="T251">wärɣə iːwä maltšimbat tambon köt klazɨm.</ta>
            <ta e="T263" id="Seg_2020" s="T258">nʼünʼo iːɣəneŋ sombəlʼeːŋə muqtumdälǯi pottə.</ta>
            <ta e="T267" id="Seg_2021" s="T263">okkɨ iːva opsä küdas.</ta>
            <ta e="T270" id="Seg_2022" s="T267">täbɨm qwändəkuzattə tomdə.</ta>
            <ta e="T275" id="Seg_2023" s="T270">täbɨm nɨtän soːmǯattə, tatpattə qottä.</ta>
            <ta e="T278" id="Seg_2024" s="T275">man üːčelaw süsüqula.</ta>
            <ta e="T281" id="Seg_2025" s="T278">täppɨla oːɣulǯuqwattə qomdägalɨk.</ta>
            <ta e="T289" id="Seg_2026" s="T281">onäŋ man robiŋvaw tättä saːrum sombɨlʼe qwäjgʼöt lʼebɨm ireɣəndə.</ta>
            <ta e="T292" id="Seg_2027" s="T289">ilaŋ man soːŋ.</ta>
            <ta e="T297" id="Seg_2028" s="T292">äran sväŋajgvaŋ, säŋɨigvaŋ, tšoppɨrä igvaŋ, suːrujigvaŋ.</ta>
            <ta e="T300" id="Seg_2029" s="T297">taːɣɨn poŋgɨrɨgvaŋ, qwälɨj(i)gvaŋ.</ta>
            <ta e="T305" id="Seg_2030" s="T300">tɨdam kɨkkaŋ tävvɨgu sändə maːdəm.</ta>
            <ta e="T308" id="Seg_2031" s="T305">onäŋ manmə täːmba.</ta>
            <ta e="T314" id="Seg_2032" s="T308">sändə maːdɨm tävvɨgu kɨkkaŋ muqtut saːrum lʼependə.</ta>
            <ta e="T318" id="Seg_2033" s="T314">man täbɨm tävenǯaw ilʼenǯaŋ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T11" id="Seg_2034" s="T0">oːruŋbaŋ muqtut qwejgʼöt poɣɨndə šɨttə saːrum naːr qwejgʼöt čisloɣɨndə sentʼäbrʼ mesʼäcqɨn äːttoɣɨn. </ta>
            <ta e="T14" id="Seg_2035" s="T11">äda nimdə Koːrela. </ta>
            <ta e="T18" id="Seg_2036" s="T14">nimdəgu mazɨŋ Madʼö Koːrʼela. </ta>
            <ta e="T25" id="Seg_2037" s="T18">onäŋ quːlaw (kundoqɨn) ukon qwälɨjguzattə i suːrujguzattə. </ta>
            <ta e="T32" id="Seg_2038" s="T25">meŋa esan naːr pottə, man qalɨzaŋ evewnannä. </ta>
            <ta e="T34" id="Seg_2039" s="T32">evʼew quːssan. </ta>
            <ta e="T38" id="Seg_2040" s="T34">evewnan esattə üčelat, quːkuzattə. </ta>
            <ta e="T43" id="Seg_2041" s="T38">okkɨr man evewnan, esewnan qalɨmbaŋ. </ta>
            <ta e="T49" id="Seg_2042" s="T43">ässäw palʼdʼukus surujgu, qwäːlɨjgu, maːtqɨn tʼäŋukus. </ta>
            <ta e="T52" id="Seg_2043" s="T49">qalɨkkuzaŋ, ilɨkuzaŋ ilʼdʼäwsä. </ta>
            <ta e="T63" id="Seg_2044" s="T52">tapsä man palʼdʼikuzaŋ kän tolʼdʼizä matʼtʼöndə, taːɣɨn poŋgɨrlʼe i sapɨɣɨrlʼe (sappɨɣɨrgu). </ta>
            <ta e="T66" id="Seg_2045" s="T63">nilʼdʼiŋ mat ilɨkuzaŋ. </ta>
            <ta e="T70" id="Seg_2046" s="T66">ilʼdäw meŋnan soː eːkkus. </ta>
            <ta e="T77" id="Seg_2047" s="T70">moːtku mazɨŋ ässäw äːzundɨ, ilʼdäw peldɨkus (peldɨkuzan). </ta>
            <ta e="T80" id="Seg_2048" s="T77">täpsä man qondukuzaŋ. </ta>
            <ta e="T86" id="Seg_2049" s="T80">kuː täp palʼdʼükus, täpsä man äːkuzaŋ. </ta>
            <ta e="T89" id="Seg_2050" s="T86">meŋnan äːsan čičew. </ta>
            <ta e="T92" id="Seg_2051" s="T89">nimdə täbɨn Мihail. </ta>
            <ta e="T94" id="Seg_2052" s="T92">mazɨŋ soːrrɨkus. </ta>
            <ta e="T105" id="Seg_2053" s="T94">oːɣɨlǯikus palʼdʼügu tolʼdʼin, aːnduzä, nʼäbɨjgu, nʼäjajgu, tüːlʼəsän (tüːləsäzä) tʼätǯikugu, kündɨnbarɣɨn aːmdɨgu. </ta>
            <ta e="T109" id="Seg_2054" s="T105">äːzɨn müːttäɣɨn täbɨm qwätpattə. </ta>
            <ta e="T112" id="Seg_2055" s="T109">meŋnan eːsan ojöw. </ta>
            <ta e="T116" id="Seg_2056" s="T112">täp äːkus püdokus qwärɣänni. </ta>
            <ta e="T120" id="Seg_2057" s="T116">meŋa qwäːdəmbɨkus, mazɨŋ moːtkuzɨt. </ta>
            <ta e="T123" id="Seg_2058" s="T120">tidam naj qumba. </ta>
            <ta e="T127" id="Seg_2059" s="T123">oːɣulʼǯukuzaŋ okkə klasqɨnä äːttoɣɨn. </ta>
            <ta e="T131" id="Seg_2060" s="T127">nimdə täbɨn äːkus Pristanʼ. </ta>
            <ta e="T139" id="Seg_2061" s="T131">tida na Pristanʼ nimdə tʼeɣa qäː, innäː-qäːtqi rajon. </ta>
            <ta e="T144" id="Seg_2062" s="T139">tättə klasɨm malčizaw Maksɨ matʼtʼöɣɨn. </ta>
            <ta e="T153" id="Seg_2063" s="T144">maksɨ maːtʼtʼoɣənnɨ qwässaŋ oːɣulǯukugu kargasokskij rajondə, škola nimdə Vʼertʼekos. </ta>
            <ta e="T158" id="Seg_2064" s="T153">niːn man malčizaw sälʼdʼü klassam. </ta>
            <ta e="T163" id="Seg_2065" s="T158">nänno man oːɣulǯukuzaŋ kalpašiɣɨn pedučilʼiše. </ta>
            <ta e="T172" id="Seg_2066" s="T163">poɣɨndə oːɣuǯɨkuzaŋ padgatavitelʼnɨj kursäɣɨn i puːssaŋ man okkɨ kursanda. </ta>
            <ta e="T181" id="Seg_2067" s="T172">okkə kursaɣannä naːr saːrum okkɨrtʼädʼigʼöt poɣəndɨ mazɨŋ iːsattə nʼärɣa arminda. </ta>
            <ta e="T186" id="Seg_2068" s="T181">nɨtʼän man eːsaŋ sälʼdʼü pon. </ta>
            <ta e="T192" id="Seg_2069" s="T186">tättə saːrum muqtutqwej pon mazɨŋ ütattə matqɨneŋ. </ta>
            <ta e="T198" id="Seg_2070" s="T192">na pon man tüːssaŋ tʼeːɣə matšondə. </ta>
            <ta e="T204" id="Seg_2071" s="T198">tämdə na ʒə bon man nätɨzaŋ. </ta>
            <ta e="T207" id="Seg_2072" s="T204">poɣɨndə äːsaŋ Arlopkaɣɨn. </ta>
            <ta e="T211" id="Seg_2073" s="T207">nännɨ laqqɨzaŋ tʼeɣə mačoɣɨn. </ta>
            <ta e="T217" id="Seg_2074" s="T211">tʼeɣə mačoɣənnɨ mazɨŋ üːdizattə ustʼ-azʼörnɨj aːntondə. </ta>
            <ta e="T228" id="Seg_2075" s="T217">aːntoːɣɨn laqqɨzaŋ klupqɨn, nännä qwälɨlʼdʼi artʼelʼɣɨn (predsedatelem), nännə selʼsavetqɨt predsedatelem selʼsoveta. </ta>
            <ta e="T230" id="Seg_2076" s="T228">laqaŋ nändə. </ta>
            <ta e="T232" id="Seg_2077" s="T230">man nädɨmbaŋ. </ta>
            <ta e="T237" id="Seg_2078" s="T232">man pajannɨzä ilaŋ šɨttətʼädʼiqwäjgʼöt pottə. </ta>
            <ta e="T245" id="Seg_2079" s="T237">učej maːdə umbaŋ: sombɨlʼe iːwə i šɨttə näwä. </ta>
            <ta e="T247" id="Seg_2080" s="T245">pajaw qassaɣi. </ta>
            <ta e="T251" id="Seg_2081" s="T247">üːčelaw süsüɣulʼdʼe assä čenʼčuattə. </ta>
            <ta e="T258" id="Seg_2082" s="T251">wärɣə iːwä malčimbat tam bon köt klazɨm. </ta>
            <ta e="T263" id="Seg_2083" s="T258">nʼünʼo iːɣəneŋ sombəlʼeːŋə muqtumdälǯi pottə. </ta>
            <ta e="T267" id="Seg_2084" s="T263">okkɨ iːwa opsä küdas. </ta>
            <ta e="T270" id="Seg_2085" s="T267">täbɨm qwändəkuzattə tomdə. </ta>
            <ta e="T275" id="Seg_2086" s="T270">täbɨm nɨtän soːmǯattə, tatpattə qottä. </ta>
            <ta e="T278" id="Seg_2087" s="T275">man üːčelaw süsüqula. </ta>
            <ta e="T281" id="Seg_2088" s="T278">täppɨla oːɣulǯuqwattə qomdägalɨk. </ta>
            <ta e="T289" id="Seg_2089" s="T281">onäŋ man robiŋwaw tättä saːrum sombɨlʼe qwäjgʼöt lʼebɨm ireɣəndə. </ta>
            <ta e="T292" id="Seg_2090" s="T289">ilaŋ man soːŋ. </ta>
            <ta e="T297" id="Seg_2091" s="T292">äran swäŋajgwaŋ, säŋɨigwaŋ, čoppɨräigwaŋ, suːrujigwaŋ. </ta>
            <ta e="T300" id="Seg_2092" s="T297">taːɣɨn poŋgɨrɨgwaŋ, qwälɨjgwaŋ. </ta>
            <ta e="T305" id="Seg_2093" s="T300">tɨdam kɨkkaŋ täwɨgu sändə maːdəm. </ta>
            <ta e="T308" id="Seg_2094" s="T305">onäŋ manmə täːmba. </ta>
            <ta e="T314" id="Seg_2095" s="T308">sändə maːdɨm täwɨgu kɨkkaŋ muqtut saːrum lʼependə. </ta>
            <ta e="T318" id="Seg_2096" s="T314">man täbɨm täwenǯaw ilʼenǯaŋ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2097" s="T0">oːr-u-ŋ-ba-ŋ</ta>
            <ta e="T2" id="Seg_2098" s="T1">muqtut</ta>
            <ta e="T3" id="Seg_2099" s="T2">qwej-gʼöt</ta>
            <ta e="T4" id="Seg_2100" s="T3">po-ɣɨndə</ta>
            <ta e="T324" id="Seg_2101" s="T4">šɨttə</ta>
            <ta e="T5" id="Seg_2102" s="T324">saːrum</ta>
            <ta e="T6" id="Seg_2103" s="T5">naːr</ta>
            <ta e="T7" id="Seg_2104" s="T6">qwej-gʼöt</ta>
            <ta e="T8" id="Seg_2105" s="T7">čislo-ɣɨndə</ta>
            <ta e="T9" id="Seg_2106" s="T8">sentʼäbrʼ</ta>
            <ta e="T10" id="Seg_2107" s="T9">mesʼäc-qɨn</ta>
            <ta e="T11" id="Seg_2108" s="T10">äːtto-ɣɨn</ta>
            <ta e="T12" id="Seg_2109" s="T11">äda</ta>
            <ta e="T13" id="Seg_2110" s="T12">nim-də</ta>
            <ta e="T14" id="Seg_2111" s="T13">Koːrela</ta>
            <ta e="T15" id="Seg_2112" s="T14">nim-də-gu</ta>
            <ta e="T16" id="Seg_2113" s="T15">mazɨŋ</ta>
            <ta e="T17" id="Seg_2114" s="T16">Madʼö</ta>
            <ta e="T18" id="Seg_2115" s="T17">Koːrʼela</ta>
            <ta e="T19" id="Seg_2116" s="T18">onäŋ</ta>
            <ta e="T20" id="Seg_2117" s="T19">quː-la-w</ta>
            <ta e="T21" id="Seg_2118" s="T20">kundo-qən</ta>
            <ta e="T22" id="Seg_2119" s="T21">ukon</ta>
            <ta e="T23" id="Seg_2120" s="T22">qwälɨ-j-gu-za-ttə</ta>
            <ta e="T24" id="Seg_2121" s="T23">i</ta>
            <ta e="T25" id="Seg_2122" s="T24">suːru-j-gu-za-ttə</ta>
            <ta e="T26" id="Seg_2123" s="T25">meŋa</ta>
            <ta e="T27" id="Seg_2124" s="T26">e-sa-ŋ</ta>
            <ta e="T28" id="Seg_2125" s="T27">naːr</ta>
            <ta e="T29" id="Seg_2126" s="T28">po-ttə</ta>
            <ta e="T30" id="Seg_2127" s="T29">man</ta>
            <ta e="T31" id="Seg_2128" s="T30">qalɨ-za-ŋ</ta>
            <ta e="T32" id="Seg_2129" s="T31">eve-w-nannä</ta>
            <ta e="T33" id="Seg_2130" s="T32">evʼe-w</ta>
            <ta e="T34" id="Seg_2131" s="T33">quː-ssa-ŋ</ta>
            <ta e="T35" id="Seg_2132" s="T34">eve-w-nan</ta>
            <ta e="T36" id="Seg_2133" s="T35">e-sa-ttə</ta>
            <ta e="T37" id="Seg_2134" s="T36">üče-la-tɨ</ta>
            <ta e="T38" id="Seg_2135" s="T37">quː-ku-za-ttə</ta>
            <ta e="T39" id="Seg_2136" s="T38">okkɨr</ta>
            <ta e="T40" id="Seg_2137" s="T39">man</ta>
            <ta e="T41" id="Seg_2138" s="T40">eve-w-nan</ta>
            <ta e="T42" id="Seg_2139" s="T41">ese-w-nan</ta>
            <ta e="T43" id="Seg_2140" s="T42">qalɨ-mba-ŋ</ta>
            <ta e="T44" id="Seg_2141" s="T43">ässä-w</ta>
            <ta e="T45" id="Seg_2142" s="T44">palʼdʼu-ku-s</ta>
            <ta e="T46" id="Seg_2143" s="T45">suru-j-gu</ta>
            <ta e="T47" id="Seg_2144" s="T46">qwäːlɨ-j-gu</ta>
            <ta e="T48" id="Seg_2145" s="T47">maːt-qən</ta>
            <ta e="T49" id="Seg_2146" s="T48">tʼäŋu-ku-s</ta>
            <ta e="T50" id="Seg_2147" s="T49">qalɨ-kku-za-ŋ</ta>
            <ta e="T51" id="Seg_2148" s="T50">illɨ-ku-za-ŋ</ta>
            <ta e="T52" id="Seg_2149" s="T51">ilʼdʼä-w-sä</ta>
            <ta e="T53" id="Seg_2150" s="T52">tap-sä</ta>
            <ta e="T54" id="Seg_2151" s="T53">man</ta>
            <ta e="T55" id="Seg_2152" s="T54">palʼdʼi-ku-za-n</ta>
            <ta e="T56" id="Seg_2153" s="T55">kä-n</ta>
            <ta e="T57" id="Seg_2154" s="T56">tolʼdʼi-zä</ta>
            <ta e="T58" id="Seg_2155" s="T57">matʼtʼö-ndə</ta>
            <ta e="T59" id="Seg_2156" s="T58">taːɣ-ɨ-n</ta>
            <ta e="T60" id="Seg_2157" s="T59">poŋgɨ-r-lʼe</ta>
            <ta e="T61" id="Seg_2158" s="T60">i</ta>
            <ta e="T62" id="Seg_2159" s="T61">sapɨɣɨ-r-lʼe</ta>
            <ta e="T63" id="Seg_2160" s="T62">sappɨɣɨ-r-gu</ta>
            <ta e="T64" id="Seg_2161" s="T63">nilʼdʼi-ŋ</ta>
            <ta e="T65" id="Seg_2162" s="T64">mat</ta>
            <ta e="T66" id="Seg_2163" s="T65">illɨ-ku-za-ŋ</ta>
            <ta e="T67" id="Seg_2164" s="T66">ilʼdä-w</ta>
            <ta e="T68" id="Seg_2165" s="T67">meŋnan</ta>
            <ta e="T69" id="Seg_2166" s="T68">soː</ta>
            <ta e="T70" id="Seg_2167" s="T69">eː-kku-s</ta>
            <ta e="T71" id="Seg_2168" s="T70">moːt-ku</ta>
            <ta e="T72" id="Seg_2169" s="T71">mazɨŋ</ta>
            <ta e="T73" id="Seg_2170" s="T72">ässä-w</ta>
            <ta e="T74" id="Seg_2171" s="T73">äːzu-ndɨ</ta>
            <ta e="T75" id="Seg_2172" s="T74">ilʼdä-w</ta>
            <ta e="T76" id="Seg_2173" s="T75">peldɨ-ku-s</ta>
            <ta e="T77" id="Seg_2174" s="T76">peldɨ-ku-za-ŋ</ta>
            <ta e="T78" id="Seg_2175" s="T77">täp-se</ta>
            <ta e="T79" id="Seg_2176" s="T78">man</ta>
            <ta e="T80" id="Seg_2177" s="T79">qondu-ku-za-ŋ</ta>
            <ta e="T81" id="Seg_2178" s="T80">kuː</ta>
            <ta e="T82" id="Seg_2179" s="T81">täp</ta>
            <ta e="T83" id="Seg_2180" s="T82">palʼdʼü-ku-s</ta>
            <ta e="T84" id="Seg_2181" s="T83">täp-se</ta>
            <ta e="T85" id="Seg_2182" s="T84">man</ta>
            <ta e="T86" id="Seg_2183" s="T85">äː-ku-za-ŋ</ta>
            <ta e="T87" id="Seg_2184" s="T86">meŋnan</ta>
            <ta e="T88" id="Seg_2185" s="T87">äː-sa-ŋ</ta>
            <ta e="T89" id="Seg_2186" s="T88">čiče-w</ta>
            <ta e="T90" id="Seg_2187" s="T89">nim-də</ta>
            <ta e="T91" id="Seg_2188" s="T90">täb-ɨ-n</ta>
            <ta e="T92" id="Seg_2189" s="T91">Мihail</ta>
            <ta e="T93" id="Seg_2190" s="T92">mazɨŋ</ta>
            <ta e="T94" id="Seg_2191" s="T93">soːrrɨ-ku-s</ta>
            <ta e="T95" id="Seg_2192" s="T94">oːɣɨ-lǯi-ku-s</ta>
            <ta e="T96" id="Seg_2193" s="T95">palʼdʼü-gu</ta>
            <ta e="T97" id="Seg_2194" s="T96">tolʼdʼi-n</ta>
            <ta e="T98" id="Seg_2195" s="T97">aːndu-zä</ta>
            <ta e="T99" id="Seg_2196" s="T98">nʼäb-ɨ-j-gu</ta>
            <ta e="T100" id="Seg_2197" s="T99">nʼäja-j-gu</ta>
            <ta e="T101" id="Seg_2198" s="T100">tüːlʼəsä-n</ta>
            <ta e="T102" id="Seg_2199" s="T101">tüːləsä-zä</ta>
            <ta e="T103" id="Seg_2200" s="T102">tʼätǯi-ku-gu</ta>
            <ta e="T104" id="Seg_2201" s="T103">kündɨ-n-bar-ɣɨn</ta>
            <ta e="T105" id="Seg_2202" s="T104">aːmdɨ-gu</ta>
            <ta e="T106" id="Seg_2203" s="T105">äːzɨ-n</ta>
            <ta e="T107" id="Seg_2204" s="T106">müːttä-ɣɨn</ta>
            <ta e="T108" id="Seg_2205" s="T107">täb-ɨ-m</ta>
            <ta e="T109" id="Seg_2206" s="T108">qwät-pa-ttə</ta>
            <ta e="T110" id="Seg_2207" s="T109">meŋnan</ta>
            <ta e="T111" id="Seg_2208" s="T110">eː-sa-ŋ</ta>
            <ta e="T112" id="Seg_2209" s="T111">ojö-w</ta>
            <ta e="T113" id="Seg_2210" s="T112">täp</ta>
            <ta e="T114" id="Seg_2211" s="T113">äː-ku-s</ta>
            <ta e="T115" id="Seg_2212" s="T114">püdo-ku-s</ta>
            <ta e="T116" id="Seg_2213" s="T115">qwärɣä-nni</ta>
            <ta e="T117" id="Seg_2214" s="T116">meŋa</ta>
            <ta e="T118" id="Seg_2215" s="T117">qwäːdə-mbɨ-ku-s</ta>
            <ta e="T119" id="Seg_2216" s="T118">mazɨŋ</ta>
            <ta e="T120" id="Seg_2217" s="T119">moːt-ku-zɨ-t</ta>
            <ta e="T121" id="Seg_2218" s="T120">tidam</ta>
            <ta e="T122" id="Seg_2219" s="T121">naj</ta>
            <ta e="T123" id="Seg_2220" s="T122">qu-mba</ta>
            <ta e="T124" id="Seg_2221" s="T123">oːɣu-lʼǯu-ku-za-ŋ</ta>
            <ta e="T125" id="Seg_2222" s="T124">okkə</ta>
            <ta e="T126" id="Seg_2223" s="T125">klas-qɨnä</ta>
            <ta e="T127" id="Seg_2224" s="T126">äːtto-ɣɨn</ta>
            <ta e="T128" id="Seg_2225" s="T127">nim-də</ta>
            <ta e="T129" id="Seg_2226" s="T128">täb-ɨ-n</ta>
            <ta e="T130" id="Seg_2227" s="T129">äː-ku-s</ta>
            <ta e="T131" id="Seg_2228" s="T130">Pristanʼ</ta>
            <ta e="T132" id="Seg_2229" s="T131">tida</ta>
            <ta e="T133" id="Seg_2230" s="T132">na</ta>
            <ta e="T134" id="Seg_2231" s="T133">Pristanʼ</ta>
            <ta e="T135" id="Seg_2232" s="T134">nim-də</ta>
            <ta e="T136" id="Seg_2233" s="T135">tʼeɣa</ta>
            <ta e="T137" id="Seg_2234" s="T136">qäː</ta>
            <ta e="T138" id="Seg_2235" s="T137">innäː_qäːt-qi</ta>
            <ta e="T139" id="Seg_2236" s="T138">rajon</ta>
            <ta e="T140" id="Seg_2237" s="T139">tättə</ta>
            <ta e="T141" id="Seg_2238" s="T140">klas-ɨ-m</ta>
            <ta e="T142" id="Seg_2239" s="T141">ma-lči-za-w</ta>
            <ta e="T143" id="Seg_2240" s="T142">maksɨ</ta>
            <ta e="T144" id="Seg_2241" s="T143">matʼtʼö-ɣɨn</ta>
            <ta e="T145" id="Seg_2242" s="T144">maksɨ</ta>
            <ta e="T146" id="Seg_2243" s="T145">maːtʼtʼo-ɣənnɨ</ta>
            <ta e="T147" id="Seg_2244" s="T146">qwäs-sa-ŋ</ta>
            <ta e="T148" id="Seg_2245" s="T147">oːɣu-lǯu-ku-gu</ta>
            <ta e="T149" id="Seg_2246" s="T148">kargasokskij</ta>
            <ta e="T150" id="Seg_2247" s="T149">rajon-də</ta>
            <ta e="T151" id="Seg_2248" s="T150">škola</ta>
            <ta e="T152" id="Seg_2249" s="T151">nim-də</ta>
            <ta e="T153" id="Seg_2250" s="T152">Vʼertʼekos</ta>
            <ta e="T154" id="Seg_2251" s="T153">niːn</ta>
            <ta e="T155" id="Seg_2252" s="T154">man</ta>
            <ta e="T156" id="Seg_2253" s="T155">ma-lči-za-w</ta>
            <ta e="T157" id="Seg_2254" s="T156">sälʼdʼü</ta>
            <ta e="T158" id="Seg_2255" s="T157">klass-a-m</ta>
            <ta e="T159" id="Seg_2256" s="T158">nänno</ta>
            <ta e="T160" id="Seg_2257" s="T159">man</ta>
            <ta e="T161" id="Seg_2258" s="T160">oːɣu-lǯu-ku-za-ŋ</ta>
            <ta e="T162" id="Seg_2259" s="T161">kalpaši-ɣɨn</ta>
            <ta e="T163" id="Seg_2260" s="T162">pedučilʼiše</ta>
            <ta e="T164" id="Seg_2261" s="T163">po-ɣɨndə</ta>
            <ta e="T165" id="Seg_2262" s="T164">oːɣu-ǯɨ-ku-za-ŋ</ta>
            <ta e="T166" id="Seg_2263" s="T165">padgatavitelʼnɨj</ta>
            <ta e="T167" id="Seg_2264" s="T166">kursä-ɣɨn</ta>
            <ta e="T168" id="Seg_2265" s="T167">i</ta>
            <ta e="T169" id="Seg_2266" s="T168">puː-ssa-ŋ</ta>
            <ta e="T170" id="Seg_2267" s="T169">man</ta>
            <ta e="T171" id="Seg_2268" s="T170">okkɨ</ta>
            <ta e="T172" id="Seg_2269" s="T171">kursa-nda</ta>
            <ta e="T173" id="Seg_2270" s="T172">okkə</ta>
            <ta e="T174" id="Seg_2271" s="T173">kursa-ɣannä</ta>
            <ta e="T320" id="Seg_2272" s="T174">naːr</ta>
            <ta e="T175" id="Seg_2273" s="T320">saːrum</ta>
            <ta e="T176" id="Seg_2274" s="T175">okkɨr-tʼädʼi-gʼöt</ta>
            <ta e="T177" id="Seg_2275" s="T176">po-ɣəndɨ</ta>
            <ta e="T178" id="Seg_2276" s="T177">mazɨŋ</ta>
            <ta e="T179" id="Seg_2277" s="T178">iː-sa-ttə</ta>
            <ta e="T180" id="Seg_2278" s="T179">nʼärɣa</ta>
            <ta e="T181" id="Seg_2279" s="T180">armi-nda</ta>
            <ta e="T182" id="Seg_2280" s="T181">nɨtʼä-n</ta>
            <ta e="T183" id="Seg_2281" s="T182">man</ta>
            <ta e="T184" id="Seg_2282" s="T183">eː-sa-ŋ</ta>
            <ta e="T185" id="Seg_2283" s="T184">sälʼdʼü</ta>
            <ta e="T186" id="Seg_2284" s="T185">po-n</ta>
            <ta e="T321" id="Seg_2285" s="T186">tättə</ta>
            <ta e="T187" id="Seg_2286" s="T321">saːrum</ta>
            <ta e="T188" id="Seg_2287" s="T187">muqtut-qwej</ta>
            <ta e="T189" id="Seg_2288" s="T188">po-n</ta>
            <ta e="T190" id="Seg_2289" s="T189">mazɨŋ</ta>
            <ta e="T191" id="Seg_2290" s="T190">üta-ttə</ta>
            <ta e="T192" id="Seg_2291" s="T191">mat-qɨneŋ</ta>
            <ta e="T193" id="Seg_2292" s="T192">na</ta>
            <ta e="T194" id="Seg_2293" s="T193">po-n</ta>
            <ta e="T195" id="Seg_2294" s="T194">man</ta>
            <ta e="T196" id="Seg_2295" s="T195">tüː-ssa-ŋ</ta>
            <ta e="T197" id="Seg_2296" s="T196">tʼeːɣə</ta>
            <ta e="T198" id="Seg_2297" s="T197">matšo-ndə</ta>
            <ta e="T199" id="Seg_2298" s="T198">tämdə</ta>
            <ta e="T201" id="Seg_2299" s="T199">na-ʒə</ta>
            <ta e="T202" id="Seg_2300" s="T201">bo-n</ta>
            <ta e="T203" id="Seg_2301" s="T202">man</ta>
            <ta e="T204" id="Seg_2302" s="T203">nätɨ-za-ŋ</ta>
            <ta e="T205" id="Seg_2303" s="T204">po-ɣɨndə</ta>
            <ta e="T206" id="Seg_2304" s="T205">äː-sa-ŋ</ta>
            <ta e="T207" id="Seg_2305" s="T206">Arlopka-ɣɨn</ta>
            <ta e="T208" id="Seg_2306" s="T207">nännɨ</ta>
            <ta e="T209" id="Seg_2307" s="T208">laqqɨ-za-ŋ</ta>
            <ta e="T210" id="Seg_2308" s="T209">tʼeɣə</ta>
            <ta e="T211" id="Seg_2309" s="T210">mačo-ɣɨn</ta>
            <ta e="T212" id="Seg_2310" s="T211">tʼeɣə</ta>
            <ta e="T213" id="Seg_2311" s="T212">mačo-ɣənnɨ</ta>
            <ta e="T214" id="Seg_2312" s="T213">mazɨŋ</ta>
            <ta e="T215" id="Seg_2313" s="T214">üːdi-za-ttə</ta>
            <ta e="T216" id="Seg_2314" s="T215">ustʼ_azʼörnɨj</ta>
            <ta e="T217" id="Seg_2315" s="T216">aːnto-ndə</ta>
            <ta e="T218" id="Seg_2316" s="T217">aːntoː-ɣɨn</ta>
            <ta e="T219" id="Seg_2317" s="T218">laqqɨ-za-ŋ</ta>
            <ta e="T220" id="Seg_2318" s="T219">klup-qɨn</ta>
            <ta e="T221" id="Seg_2319" s="T220">nännä</ta>
            <ta e="T222" id="Seg_2320" s="T221">qwälɨ-lʼdʼi</ta>
            <ta e="T223" id="Seg_2321" s="T222">artʼelʼ-ɣɨn</ta>
            <ta e="T225" id="Seg_2322" s="T224">nännə</ta>
            <ta e="T226" id="Seg_2323" s="T225">selʼsavet-qɨt</ta>
            <ta e="T229" id="Seg_2324" s="T228">laqa-ŋ</ta>
            <ta e="T230" id="Seg_2325" s="T229">nändə</ta>
            <ta e="T231" id="Seg_2326" s="T230">man</ta>
            <ta e="T232" id="Seg_2327" s="T231">nädɨ-mba-ŋ</ta>
            <ta e="T233" id="Seg_2328" s="T232">man</ta>
            <ta e="T234" id="Seg_2329" s="T233">paja-n-nɨ-zä</ta>
            <ta e="T235" id="Seg_2330" s="T234">ila-ŋ</ta>
            <ta e="T236" id="Seg_2331" s="T235">šɨttətʼädʼi-qwäj-gʼöt</ta>
            <ta e="T237" id="Seg_2332" s="T236">po-ttə</ta>
            <ta e="T238" id="Seg_2333" s="T237">učej</ta>
            <ta e="T239" id="Seg_2334" s="T238">maːdə</ta>
            <ta e="T240" id="Seg_2335" s="T239">u-mba-ŋ</ta>
            <ta e="T241" id="Seg_2336" s="T240">sombɨlʼe</ta>
            <ta e="T242" id="Seg_2337" s="T241">iː-wə</ta>
            <ta e="T243" id="Seg_2338" s="T242">i</ta>
            <ta e="T244" id="Seg_2339" s="T243">šɨttə</ta>
            <ta e="T245" id="Seg_2340" s="T244">nä-wä</ta>
            <ta e="T246" id="Seg_2341" s="T245">paja-w</ta>
            <ta e="T247" id="Seg_2342" s="T246">qassaɣi</ta>
            <ta e="T248" id="Seg_2343" s="T247">üːče-la-w</ta>
            <ta e="T319" id="Seg_2344" s="T248">süsüɣulʼ</ta>
            <ta e="T249" id="Seg_2345" s="T319">dʼe</ta>
            <ta e="T250" id="Seg_2346" s="T249">assä</ta>
            <ta e="T251" id="Seg_2347" s="T250">čenʼču-a-ttə</ta>
            <ta e="T252" id="Seg_2348" s="T251">wärɣə</ta>
            <ta e="T253" id="Seg_2349" s="T252">iː-wä</ta>
            <ta e="T254" id="Seg_2350" s="T253">ma-lči-mba-t</ta>
            <ta e="T255" id="Seg_2351" s="T254">tam</ta>
            <ta e="T256" id="Seg_2352" s="T255">bo-n</ta>
            <ta e="T257" id="Seg_2353" s="T256">köːt</ta>
            <ta e="T258" id="Seg_2354" s="T257">klaz-ɨ-m</ta>
            <ta e="T259" id="Seg_2355" s="T258">nʼünʼo</ta>
            <ta e="T260" id="Seg_2356" s="T259">iː-ɣəneŋ</ta>
            <ta e="T261" id="Seg_2357" s="T260">sombəlʼeːŋə</ta>
            <ta e="T262" id="Seg_2358" s="T261">muqtu-mdälǯi</ta>
            <ta e="T263" id="Seg_2359" s="T262">po-ttə</ta>
            <ta e="T264" id="Seg_2360" s="T263">okkɨ</ta>
            <ta e="T265" id="Seg_2361" s="T264">iː-wa</ta>
            <ta e="T266" id="Seg_2362" s="T265">opsä</ta>
            <ta e="T267" id="Seg_2363" s="T266">küda-s</ta>
            <ta e="T268" id="Seg_2364" s="T267">täb-ɨ-m</ta>
            <ta e="T269" id="Seg_2365" s="T268">qwän-də-ku-za-ttə</ta>
            <ta e="T270" id="Seg_2366" s="T269">Tom-də</ta>
            <ta e="T271" id="Seg_2367" s="T270">täb-ɨ-m</ta>
            <ta e="T272" id="Seg_2368" s="T271">nɨtän</ta>
            <ta e="T273" id="Seg_2369" s="T272">soː-m-ǯa-ttə</ta>
            <ta e="T274" id="Seg_2370" s="T273">tat-pa-ttə</ta>
            <ta e="T275" id="Seg_2371" s="T274">qottä</ta>
            <ta e="T276" id="Seg_2372" s="T275">man</ta>
            <ta e="T277" id="Seg_2373" s="T276">üːče-la-w</ta>
            <ta e="T278" id="Seg_2374" s="T277">süsüqu-la</ta>
            <ta e="T279" id="Seg_2375" s="T278">täpp-ɨ-la</ta>
            <ta e="T280" id="Seg_2376" s="T279">oːɣu-lǯu-q-wa-ttə</ta>
            <ta e="T281" id="Seg_2377" s="T280">qomdä-galɨ-k</ta>
            <ta e="T282" id="Seg_2378" s="T281">onäŋ</ta>
            <ta e="T283" id="Seg_2379" s="T282">man</ta>
            <ta e="T284" id="Seg_2380" s="T283">robi-ŋ-wa-w</ta>
            <ta e="T322" id="Seg_2381" s="T284">tättä</ta>
            <ta e="T285" id="Seg_2382" s="T322">saːrum</ta>
            <ta e="T286" id="Seg_2383" s="T285">sombɨlʼe</ta>
            <ta e="T287" id="Seg_2384" s="T286">qwäj-gʼöt</ta>
            <ta e="T288" id="Seg_2385" s="T287">lʼebɨ-m</ta>
            <ta e="T289" id="Seg_2386" s="T288">ire-ɣəndə</ta>
            <ta e="T290" id="Seg_2387" s="T289">ila-ŋ</ta>
            <ta e="T291" id="Seg_2388" s="T290">man</ta>
            <ta e="T292" id="Seg_2389" s="T291">soː-ŋ</ta>
            <ta e="T293" id="Seg_2390" s="T292">ära-n</ta>
            <ta e="T294" id="Seg_2391" s="T293">swäŋaj-g-wa-ŋ</ta>
            <ta e="T295" id="Seg_2392" s="T294">säŋɨ-i-g-wa-ŋ</ta>
            <ta e="T296" id="Seg_2393" s="T295">čoppɨr-ä-i-g-wa-ŋ</ta>
            <ta e="T297" id="Seg_2394" s="T296">suːru-j-i-g-wa-ŋ</ta>
            <ta e="T298" id="Seg_2395" s="T297">taːɣ-ɨ-n</ta>
            <ta e="T299" id="Seg_2396" s="T298">poŋgɨ-r-ɨ-g-wa-ŋ</ta>
            <ta e="T300" id="Seg_2397" s="T299">qwälɨ-j-g-wa-ŋ</ta>
            <ta e="T301" id="Seg_2398" s="T300">tɨdam</ta>
            <ta e="T302" id="Seg_2399" s="T301">kɨkka-ŋ</ta>
            <ta e="T303" id="Seg_2400" s="T302">täw-ɨ-gu</ta>
            <ta e="T304" id="Seg_2401" s="T303">sändə</ta>
            <ta e="T305" id="Seg_2402" s="T304">maːd-ə-m</ta>
            <ta e="T306" id="Seg_2403" s="T305">onäŋ</ta>
            <ta e="T307" id="Seg_2404" s="T306">man-mə</ta>
            <ta e="T308" id="Seg_2405" s="T307">täː-mba</ta>
            <ta e="T309" id="Seg_2406" s="T308">sändə</ta>
            <ta e="T310" id="Seg_2407" s="T309">maːd-ɨ-m</ta>
            <ta e="T311" id="Seg_2408" s="T310">täw-ɨ-gu</ta>
            <ta e="T312" id="Seg_2409" s="T311">kɨkka-ŋ</ta>
            <ta e="T323" id="Seg_2410" s="T312">muqtut</ta>
            <ta e="T313" id="Seg_2411" s="T323">saːrum</ta>
            <ta e="T314" id="Seg_2412" s="T313">lʼepe-ndə</ta>
            <ta e="T315" id="Seg_2413" s="T314">man</ta>
            <ta e="T316" id="Seg_2414" s="T315">täb-ɨ-m</ta>
            <ta e="T317" id="Seg_2415" s="T316">täw-e-nǯa-w</ta>
            <ta e="T318" id="Seg_2416" s="T317">ilʼe-nǯa-ŋ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2417" s="T0">or-ɨ-ŋ-mbɨ-ŋ</ta>
            <ta e="T2" id="Seg_2418" s="T1">muktut</ta>
            <ta e="T3" id="Seg_2419" s="T2">qwäj-köːt</ta>
            <ta e="T4" id="Seg_2420" s="T3">po-qɨntɨ</ta>
            <ta e="T324" id="Seg_2421" s="T4">šɨttə</ta>
            <ta e="T5" id="Seg_2422" s="T324">saːrum</ta>
            <ta e="T6" id="Seg_2423" s="T5">nakkɨr</ta>
            <ta e="T7" id="Seg_2424" s="T6">qwäj-köːt</ta>
            <ta e="T8" id="Seg_2425" s="T7">čislo-qɨntɨ</ta>
            <ta e="T9" id="Seg_2426" s="T8">sentʼäbrʼ</ta>
            <ta e="T10" id="Seg_2427" s="T9">mesʼäc-qən</ta>
            <ta e="T11" id="Seg_2428" s="T10">eːto-qən</ta>
            <ta e="T12" id="Seg_2429" s="T11">eːto</ta>
            <ta e="T13" id="Seg_2430" s="T12">nim-tɨ</ta>
            <ta e="T14" id="Seg_2431" s="T13">Koːrela</ta>
            <ta e="T15" id="Seg_2432" s="T14">nim-tɨ-gu</ta>
            <ta e="T16" id="Seg_2433" s="T15">mašim</ta>
            <ta e="T17" id="Seg_2434" s="T16">Madʼö</ta>
            <ta e="T18" id="Seg_2435" s="T17">Koːrela</ta>
            <ta e="T19" id="Seg_2436" s="T18">onäk</ta>
            <ta e="T20" id="Seg_2437" s="T19">qum-la-m</ta>
            <ta e="T21" id="Seg_2438" s="T20">*kundǝ-qən</ta>
            <ta e="T22" id="Seg_2439" s="T21">ugoːn</ta>
            <ta e="T23" id="Seg_2440" s="T22">qwǝlɨ-j-kku-sɨ-tɨt</ta>
            <ta e="T24" id="Seg_2441" s="T23">i</ta>
            <ta e="T25" id="Seg_2442" s="T24">suːrǝm-j-kku-sɨ-tɨt</ta>
            <ta e="T26" id="Seg_2443" s="T25">mäkkä</ta>
            <ta e="T27" id="Seg_2444" s="T26">eː-sɨ-n</ta>
            <ta e="T28" id="Seg_2445" s="T27">nakkɨr</ta>
            <ta e="T29" id="Seg_2446" s="T28">po-tɨ</ta>
            <ta e="T30" id="Seg_2447" s="T29">man</ta>
            <ta e="T31" id="Seg_2448" s="T30">qalɨ-sɨ-ŋ</ta>
            <ta e="T32" id="Seg_2449" s="T31">äwa-m-nannɨ</ta>
            <ta e="T33" id="Seg_2450" s="T32">äwa-m</ta>
            <ta e="T34" id="Seg_2451" s="T33">quː-sɨ-n</ta>
            <ta e="T35" id="Seg_2452" s="T34">äwa-m-nan</ta>
            <ta e="T36" id="Seg_2453" s="T35">eː-sɨ-tɨt</ta>
            <ta e="T37" id="Seg_2454" s="T36">üččəcʼä-la-tɨ</ta>
            <ta e="T38" id="Seg_2455" s="T37">quː-kku-sɨ-tɨt</ta>
            <ta e="T39" id="Seg_2456" s="T38">okkɨr</ta>
            <ta e="T40" id="Seg_2457" s="T39">man</ta>
            <ta e="T41" id="Seg_2458" s="T40">äwa-m-nan</ta>
            <ta e="T42" id="Seg_2459" s="T41">ässɨ-m-nan</ta>
            <ta e="T43" id="Seg_2460" s="T42">qalɨ-mbɨ-ŋ</ta>
            <ta e="T44" id="Seg_2461" s="T43">ässɨ-m</ta>
            <ta e="T45" id="Seg_2462" s="T44">paldʼu-kku-sɨ</ta>
            <ta e="T46" id="Seg_2463" s="T45">suːrǝm-j-gu</ta>
            <ta e="T47" id="Seg_2464" s="T46">qwǝlɨ-j-gu</ta>
            <ta e="T48" id="Seg_2465" s="T47">maːt-qən</ta>
            <ta e="T49" id="Seg_2466" s="T48">tʼäŋu-kku-sɨ</ta>
            <ta e="T50" id="Seg_2467" s="T49">qalɨ-kku-sɨ-ŋ</ta>
            <ta e="T51" id="Seg_2468" s="T50">illɨ-kku-sɨ-ŋ</ta>
            <ta e="T52" id="Seg_2469" s="T51">ilʼǯa-m-se</ta>
            <ta e="T53" id="Seg_2470" s="T52">tap-sɨ</ta>
            <ta e="T54" id="Seg_2471" s="T53">man</ta>
            <ta e="T55" id="Seg_2472" s="T54">paldʼu-kku-sɨ-n</ta>
            <ta e="T56" id="Seg_2473" s="T55">ka-n</ta>
            <ta e="T57" id="Seg_2474" s="T56">tolʼdʼi-se</ta>
            <ta e="T58" id="Seg_2475" s="T57">matʼtʼi-ndɨ</ta>
            <ta e="T59" id="Seg_2476" s="T58">taq-ɨ-n</ta>
            <ta e="T60" id="Seg_2477" s="T59">poqqo-r-le</ta>
            <ta e="T61" id="Seg_2478" s="T60">i</ta>
            <ta e="T62" id="Seg_2479" s="T61">sapqə-r-le</ta>
            <ta e="T63" id="Seg_2480" s="T62">sapqə-r-gu</ta>
            <ta e="T64" id="Seg_2481" s="T63">nik-k</ta>
            <ta e="T65" id="Seg_2482" s="T64">man</ta>
            <ta e="T66" id="Seg_2483" s="T65">illɨ-kku-sɨ-ŋ</ta>
            <ta e="T67" id="Seg_2484" s="T66">ilʼǯa-m</ta>
            <ta e="T68" id="Seg_2485" s="T67">meŋnan</ta>
            <ta e="T69" id="Seg_2486" s="T68">soː</ta>
            <ta e="T70" id="Seg_2487" s="T69">eː-kku-sɨ</ta>
            <ta e="T71" id="Seg_2488" s="T70">moːt-ku</ta>
            <ta e="T72" id="Seg_2489" s="T71">mašim</ta>
            <ta e="T73" id="Seg_2490" s="T72">ässɨ-m</ta>
            <ta e="T74" id="Seg_2491" s="T73">eːssu-ntɨ</ta>
            <ta e="T75" id="Seg_2492" s="T74">ilʼǯa-m</ta>
            <ta e="T76" id="Seg_2493" s="T75">päldɨ-kku-sɨ</ta>
            <ta e="T77" id="Seg_2494" s="T76">päldɨ-kku-sɨ-n</ta>
            <ta e="T78" id="Seg_2495" s="T77">tap-se</ta>
            <ta e="T79" id="Seg_2496" s="T78">man</ta>
            <ta e="T80" id="Seg_2497" s="T79">qontə-kku-sɨ-ŋ</ta>
            <ta e="T81" id="Seg_2498" s="T80">kuː</ta>
            <ta e="T82" id="Seg_2499" s="T81">tap</ta>
            <ta e="T83" id="Seg_2500" s="T82">paldʼu-kku-sɨ</ta>
            <ta e="T84" id="Seg_2501" s="T83">tap-se</ta>
            <ta e="T85" id="Seg_2502" s="T84">man</ta>
            <ta e="T86" id="Seg_2503" s="T85">eː-kku-sɨ-ŋ</ta>
            <ta e="T87" id="Seg_2504" s="T86">meŋnan</ta>
            <ta e="T88" id="Seg_2505" s="T87">eː-sɨ-n</ta>
            <ta e="T89" id="Seg_2506" s="T88">čiča-m</ta>
            <ta e="T90" id="Seg_2507" s="T89">nim-tɨ</ta>
            <ta e="T91" id="Seg_2508" s="T90">tap-ɨ-n</ta>
            <ta e="T92" id="Seg_2509" s="T91">Мihail</ta>
            <ta e="T93" id="Seg_2510" s="T92">mašim</ta>
            <ta e="T94" id="Seg_2511" s="T93">soːri-kku-sɨ</ta>
            <ta e="T95" id="Seg_2512" s="T94">oːqə-lʼčǝ-kku-sɨ</ta>
            <ta e="T96" id="Seg_2513" s="T95">paldʼu-gu</ta>
            <ta e="T97" id="Seg_2514" s="T96">tolʼdʼi-n</ta>
            <ta e="T98" id="Seg_2515" s="T97">andu-se</ta>
            <ta e="T99" id="Seg_2516" s="T98">nʼaːb-ɨ-j-gu</ta>
            <ta e="T100" id="Seg_2517" s="T99">nʼaja-j-gu</ta>
            <ta e="T101" id="Seg_2518" s="T100">tülʼse-n</ta>
            <ta e="T102" id="Seg_2519" s="T101">tülʼse-se</ta>
            <ta e="T103" id="Seg_2520" s="T102">tʼaččɨ-kku-gu</ta>
            <ta e="T104" id="Seg_2521" s="T103">kündɨ-n-par-qən</ta>
            <ta e="T105" id="Seg_2522" s="T104">omdɨ-gu</ta>
            <ta e="T106" id="Seg_2523" s="T105">ässɨ-n</ta>
            <ta e="T107" id="Seg_2524" s="T106">müːdu-qən</ta>
            <ta e="T108" id="Seg_2525" s="T107">tap-ɨ-m</ta>
            <ta e="T109" id="Seg_2526" s="T108">qwat-mbɨ-tɨt</ta>
            <ta e="T110" id="Seg_2527" s="T109">meŋnan</ta>
            <ta e="T111" id="Seg_2528" s="T110">eː-sɨ-n</ta>
            <ta e="T112" id="Seg_2529" s="T111">oːjo-m</ta>
            <ta e="T113" id="Seg_2530" s="T112">tap</ta>
            <ta e="T114" id="Seg_2531" s="T113">eː-kku-sɨ</ta>
            <ta e="T115" id="Seg_2532" s="T114">püdu-kku-sɨ</ta>
            <ta e="T116" id="Seg_2533" s="T115">qwärqa-nɨ</ta>
            <ta e="T117" id="Seg_2534" s="T116">mäkkä</ta>
            <ta e="T118" id="Seg_2535" s="T117">qwäːdə-mbɨ-kku-sɨ</ta>
            <ta e="T119" id="Seg_2536" s="T118">mašim</ta>
            <ta e="T120" id="Seg_2537" s="T119">moːt-kku-sɨ-tɨ</ta>
            <ta e="T121" id="Seg_2538" s="T120">tʼida</ta>
            <ta e="T122" id="Seg_2539" s="T121">naj</ta>
            <ta e="T123" id="Seg_2540" s="T122">quː-mbɨ</ta>
            <ta e="T124" id="Seg_2541" s="T123">oːqə-lʼčǝ-kku-sɨ-ŋ</ta>
            <ta e="T125" id="Seg_2542" s="T124">okkɨr</ta>
            <ta e="T126" id="Seg_2543" s="T125">klas-qɨnnɨ</ta>
            <ta e="T127" id="Seg_2544" s="T126">eːto-qən</ta>
            <ta e="T128" id="Seg_2545" s="T127">nim-tɨ</ta>
            <ta e="T129" id="Seg_2546" s="T128">tap-ɨ-n</ta>
            <ta e="T130" id="Seg_2547" s="T129">eː-kku-sɨ</ta>
            <ta e="T131" id="Seg_2548" s="T130">Pristanʼ</ta>
            <ta e="T132" id="Seg_2549" s="T131">tʼida</ta>
            <ta e="T133" id="Seg_2550" s="T132">na</ta>
            <ta e="T134" id="Seg_2551" s="T133">Pristanʼ</ta>
            <ta e="T135" id="Seg_2552" s="T134">nim-tɨ</ta>
            <ta e="T136" id="Seg_2553" s="T135">tʼeɣɨ</ta>
            <ta e="T137" id="Seg_2554" s="T136">kɨ</ta>
            <ta e="T138" id="Seg_2555" s="T137">innä_qät-qɨlʼ</ta>
            <ta e="T139" id="Seg_2556" s="T138">rajon</ta>
            <ta e="T140" id="Seg_2557" s="T139">tättɨ</ta>
            <ta e="T141" id="Seg_2558" s="T140">klas-ɨ-m</ta>
            <ta e="T142" id="Seg_2559" s="T141">meː-lʼčǝ-sɨ-m</ta>
            <ta e="T143" id="Seg_2560" s="T142">maksɨ</ta>
            <ta e="T144" id="Seg_2561" s="T143">matʼtʼi-qən</ta>
            <ta e="T145" id="Seg_2562" s="T144">maksɨ</ta>
            <ta e="T146" id="Seg_2563" s="T145">matʼtʼi-qɨntɨ</ta>
            <ta e="T147" id="Seg_2564" s="T146">qwən-sɨ-ŋ</ta>
            <ta e="T148" id="Seg_2565" s="T147">oːqə-lčɨ-kku-gu</ta>
            <ta e="T149" id="Seg_2566" s="T148">kargasokskij</ta>
            <ta e="T150" id="Seg_2567" s="T149">rajon-ndɨ</ta>
            <ta e="T151" id="Seg_2568" s="T150">škola</ta>
            <ta e="T152" id="Seg_2569" s="T151">nim-tɨ</ta>
            <ta e="T153" id="Seg_2570" s="T152">Vʼertʼekos</ta>
            <ta e="T154" id="Seg_2571" s="T153">nɨːnɨ</ta>
            <ta e="T155" id="Seg_2572" s="T154">man</ta>
            <ta e="T156" id="Seg_2573" s="T155">meː-lʼčǝ-sɨ-m</ta>
            <ta e="T157" id="Seg_2574" s="T156">seːlʼdu</ta>
            <ta e="T158" id="Seg_2575" s="T157">klas-ɨ-m</ta>
            <ta e="T159" id="Seg_2576" s="T158">nɨːnɨ</ta>
            <ta e="T160" id="Seg_2577" s="T159">man</ta>
            <ta e="T161" id="Seg_2578" s="T160">oːqə-lʼčǝ-kku-sɨ-ŋ</ta>
            <ta e="T162" id="Seg_2579" s="T161">kalpaši-qən</ta>
            <ta e="T163" id="Seg_2580" s="T162">pedučilʼiše</ta>
            <ta e="T164" id="Seg_2581" s="T163">po-qɨntɨ</ta>
            <ta e="T165" id="Seg_2582" s="T164">oːqə-lʼčǝ-kku-sɨ-ŋ</ta>
            <ta e="T166" id="Seg_2583" s="T165">padgatavitelʼnɨj</ta>
            <ta e="T167" id="Seg_2584" s="T166">kursä-qən</ta>
            <ta e="T168" id="Seg_2585" s="T167">i</ta>
            <ta e="T169" id="Seg_2586" s="T168">puː-sɨ-ŋ</ta>
            <ta e="T170" id="Seg_2587" s="T169">man</ta>
            <ta e="T171" id="Seg_2588" s="T170">okkɨr</ta>
            <ta e="T172" id="Seg_2589" s="T171">kursä-ndɨ</ta>
            <ta e="T173" id="Seg_2590" s="T172">okkɨr</ta>
            <ta e="T174" id="Seg_2591" s="T173">kursä-qɨnnɨ</ta>
            <ta e="T320" id="Seg_2592" s="T174">naːr</ta>
            <ta e="T175" id="Seg_2593" s="T320">saːrum</ta>
            <ta e="T176" id="Seg_2594" s="T175">okkɨr-tʼädʼi-göt</ta>
            <ta e="T177" id="Seg_2595" s="T176">po-qɨntɨ</ta>
            <ta e="T178" id="Seg_2596" s="T177">mašim</ta>
            <ta e="T179" id="Seg_2597" s="T178">iː-sɨ-tɨt</ta>
            <ta e="T180" id="Seg_2598" s="T179">nʼarqə</ta>
            <ta e="T181" id="Seg_2599" s="T180">armi-ndɨ</ta>
            <ta e="T182" id="Seg_2600" s="T181">načʼa-n</ta>
            <ta e="T183" id="Seg_2601" s="T182">man</ta>
            <ta e="T184" id="Seg_2602" s="T183">eː-sɨ-ŋ</ta>
            <ta e="T185" id="Seg_2603" s="T184">seːlʼdu</ta>
            <ta e="T186" id="Seg_2604" s="T185">po-n</ta>
            <ta e="T321" id="Seg_2605" s="T186">tättə</ta>
            <ta e="T187" id="Seg_2606" s="T321">saːrum</ta>
            <ta e="T188" id="Seg_2607" s="T187">muktut-qwäj</ta>
            <ta e="T189" id="Seg_2608" s="T188">po-n</ta>
            <ta e="T190" id="Seg_2609" s="T189">mašim</ta>
            <ta e="T191" id="Seg_2610" s="T190">üːtɨ-tɨt</ta>
            <ta e="T192" id="Seg_2611" s="T191">maːt-qɨntɨ</ta>
            <ta e="T193" id="Seg_2612" s="T192">na</ta>
            <ta e="T194" id="Seg_2613" s="T193">po-n</ta>
            <ta e="T195" id="Seg_2614" s="T194">man</ta>
            <ta e="T196" id="Seg_2615" s="T195">tüː-sɨ-ŋ</ta>
            <ta e="T197" id="Seg_2616" s="T196">tʼeɣɨ</ta>
            <ta e="T198" id="Seg_2617" s="T197">matšo-ndɨ</ta>
            <ta e="T199" id="Seg_2618" s="T198">tɨmtɨ</ta>
            <ta e="T201" id="Seg_2619" s="T199">na-ʒe</ta>
            <ta e="T202" id="Seg_2620" s="T201">bo-n</ta>
            <ta e="T203" id="Seg_2621" s="T202">man</ta>
            <ta e="T204" id="Seg_2622" s="T203">naːtɨ-sɨ-ŋ</ta>
            <ta e="T205" id="Seg_2623" s="T204">po-qɨntɨ</ta>
            <ta e="T206" id="Seg_2624" s="T205">eː-sɨ-ŋ</ta>
            <ta e="T207" id="Seg_2625" s="T206">Arlopka-qən</ta>
            <ta e="T208" id="Seg_2626" s="T207">nɨːnɨ</ta>
            <ta e="T209" id="Seg_2627" s="T208">laqqɨ-sɨ-ŋ</ta>
            <ta e="T210" id="Seg_2628" s="T209">tʼeɣɨ</ta>
            <ta e="T211" id="Seg_2629" s="T210">matʼtʼi-qən</ta>
            <ta e="T212" id="Seg_2630" s="T211">tʼeɣɨ</ta>
            <ta e="T213" id="Seg_2631" s="T212">matʼtʼi-qɨntɨ</ta>
            <ta e="T214" id="Seg_2632" s="T213">mašim</ta>
            <ta e="T215" id="Seg_2633" s="T214">üːtɨ-sɨ-tɨt</ta>
            <ta e="T216" id="Seg_2634" s="T215">ustʼ_azʼörnɨj</ta>
            <ta e="T217" id="Seg_2635" s="T216">eːto-ndɨ</ta>
            <ta e="T218" id="Seg_2636" s="T217">eːto-qən</ta>
            <ta e="T219" id="Seg_2637" s="T218">laqqɨ-sɨ-ŋ</ta>
            <ta e="T220" id="Seg_2638" s="T219">klup-qən</ta>
            <ta e="T221" id="Seg_2639" s="T220">nɨːnɨ</ta>
            <ta e="T222" id="Seg_2640" s="T221">qwǝlɨ-lʼdi</ta>
            <ta e="T223" id="Seg_2641" s="T222">artʼelʼ-qən</ta>
            <ta e="T225" id="Seg_2642" s="T224">nɨːnɨ</ta>
            <ta e="T226" id="Seg_2643" s="T225">selʼsavet-qən</ta>
            <ta e="T229" id="Seg_2644" s="T228">laqqɨ-ŋ</ta>
            <ta e="T230" id="Seg_2645" s="T229">nändə</ta>
            <ta e="T231" id="Seg_2646" s="T230">man</ta>
            <ta e="T232" id="Seg_2647" s="T231">naːtɨ-mbɨ-ŋ</ta>
            <ta e="T233" id="Seg_2648" s="T232">man</ta>
            <ta e="T234" id="Seg_2649" s="T233">paja-n-ni-se</ta>
            <ta e="T235" id="Seg_2650" s="T234">illɨ-ŋ</ta>
            <ta e="T236" id="Seg_2651" s="T235">šɨtʼätköt-qwäj-köːt</ta>
            <ta e="T237" id="Seg_2652" s="T236">po-tɨ</ta>
            <ta e="T238" id="Seg_2653" s="T237">ütče</ta>
            <ta e="T239" id="Seg_2654" s="T238">maːdə</ta>
            <ta e="T240" id="Seg_2655" s="T239">eː-mbɨ-n</ta>
            <ta e="T241" id="Seg_2656" s="T240">sombɨlʼe</ta>
            <ta e="T242" id="Seg_2657" s="T241">iː-m</ta>
            <ta e="T243" id="Seg_2658" s="T242">i</ta>
            <ta e="T244" id="Seg_2659" s="T243">šittə</ta>
            <ta e="T245" id="Seg_2660" s="T244">neː-m</ta>
            <ta e="T246" id="Seg_2661" s="T245">paja-m</ta>
            <ta e="T247" id="Seg_2662" s="T246">qassaːq</ta>
            <ta e="T248" id="Seg_2663" s="T247">ütče-la-m</ta>
            <ta e="T319" id="Seg_2664" s="T248">süsüqəlʼ</ta>
            <ta e="T249" id="Seg_2665" s="T319">dʼar</ta>
            <ta e="T250" id="Seg_2666" s="T249">assɨ</ta>
            <ta e="T251" id="Seg_2667" s="T250">čenču-ɨ-tɨt</ta>
            <ta e="T252" id="Seg_2668" s="T251">wərkə</ta>
            <ta e="T253" id="Seg_2669" s="T252">iː-m</ta>
            <ta e="T254" id="Seg_2670" s="T253">meː-lʼčǝ-mbɨ-tɨ</ta>
            <ta e="T255" id="Seg_2671" s="T254">taw</ta>
            <ta e="T256" id="Seg_2672" s="T255">po-n</ta>
            <ta e="T257" id="Seg_2673" s="T256">köːt</ta>
            <ta e="T258" id="Seg_2674" s="T257">klas-ɨ-m</ta>
            <ta e="T259" id="Seg_2675" s="T258">nʼünʼü</ta>
            <ta e="T260" id="Seg_2676" s="T259">iː-qɨntɨ</ta>
            <ta e="T261" id="Seg_2677" s="T260">sombɨlʼe</ta>
            <ta e="T262" id="Seg_2678" s="T261">muktut-mǯel</ta>
            <ta e="T263" id="Seg_2679" s="T262">po-tɨ</ta>
            <ta e="T264" id="Seg_2680" s="T263">okkɨr</ta>
            <ta e="T265" id="Seg_2681" s="T264">iː-m</ta>
            <ta e="T266" id="Seg_2682" s="T265">opsä</ta>
            <ta e="T267" id="Seg_2683" s="T266">küdə-sɨ</ta>
            <ta e="T268" id="Seg_2684" s="T267">tap-ɨ-m</ta>
            <ta e="T269" id="Seg_2685" s="T268">qwən-tɨ-kku-sɨ-tɨt</ta>
            <ta e="T270" id="Seg_2686" s="T269">Tom-ndɨ</ta>
            <ta e="T271" id="Seg_2687" s="T270">tap-ɨ-m</ta>
            <ta e="T272" id="Seg_2688" s="T271">nɨtʼan</ta>
            <ta e="T273" id="Seg_2689" s="T272">soː-m-ǯu-tɨt</ta>
            <ta e="T274" id="Seg_2690" s="T273">tatɨ-mbɨ-tɨt</ta>
            <ta e="T275" id="Seg_2691" s="T274">qotä</ta>
            <ta e="T276" id="Seg_2692" s="T275">man</ta>
            <ta e="T277" id="Seg_2693" s="T276">ütče-la-m</ta>
            <ta e="T278" id="Seg_2694" s="T277">süsügu-la</ta>
            <ta e="T279" id="Seg_2695" s="T278">tap-ɨ-la</ta>
            <ta e="T280" id="Seg_2696" s="T279">oːqə-lʼčǝ-kku-ŋɨ-tɨt</ta>
            <ta e="T281" id="Seg_2697" s="T280">qomdɛ-gaːlɨ-k</ta>
            <ta e="T282" id="Seg_2698" s="T281">onäk</ta>
            <ta e="T283" id="Seg_2699" s="T282">man</ta>
            <ta e="T284" id="Seg_2700" s="T283">robi-ŋ-ŋɨ-m</ta>
            <ta e="T322" id="Seg_2701" s="T284">tättə</ta>
            <ta e="T285" id="Seg_2702" s="T322">saːrum</ta>
            <ta e="T286" id="Seg_2703" s="T285">sombɨlʼe</ta>
            <ta e="T287" id="Seg_2704" s="T286">qwäj-köːt</ta>
            <ta e="T288" id="Seg_2705" s="T287">lʼebɨ-m</ta>
            <ta e="T289" id="Seg_2706" s="T288">irä-qɨntɨ</ta>
            <ta e="T290" id="Seg_2707" s="T289">illɨ-ŋ</ta>
            <ta e="T291" id="Seg_2708" s="T290">man</ta>
            <ta e="T292" id="Seg_2709" s="T291">soː-k</ta>
            <ta e="T293" id="Seg_2710" s="T292">ara-n</ta>
            <ta e="T294" id="Seg_2711" s="T293">swäŋgɨ-kku-ŋɨ-ŋ</ta>
            <ta e="T295" id="Seg_2712" s="T294">säŋɨ-j-kku-ŋɨ-ŋ</ta>
            <ta e="T296" id="Seg_2713" s="T295">čobɨr-ɨ-j-kku-ŋɨ-ŋ</ta>
            <ta e="T297" id="Seg_2714" s="T296">suːrǝm-j-ɨ-kku-ŋɨ-ŋ</ta>
            <ta e="T298" id="Seg_2715" s="T297">taq-ɨ-n</ta>
            <ta e="T299" id="Seg_2716" s="T298">poqqo-r-ɨ-kku-ŋɨ-ŋ</ta>
            <ta e="T300" id="Seg_2717" s="T299">qwǝlɨ-j-kku-ŋɨ-ŋ</ta>
            <ta e="T301" id="Seg_2718" s="T300">tʼida</ta>
            <ta e="T302" id="Seg_2719" s="T301">kɨkkɨ-ŋ</ta>
            <ta e="T303" id="Seg_2720" s="T302">taw-ɨ-gu</ta>
            <ta e="T304" id="Seg_2721" s="T303">sendɨ</ta>
            <ta e="T305" id="Seg_2722" s="T304">maːt-ɨ-m</ta>
            <ta e="T306" id="Seg_2723" s="T305">onäk</ta>
            <ta e="T307" id="Seg_2724" s="T306">maːt-m</ta>
            <ta e="T308" id="Seg_2725" s="T307">te-mbɨ</ta>
            <ta e="T309" id="Seg_2726" s="T308">sendɨ</ta>
            <ta e="T310" id="Seg_2727" s="T309">maːt-ɨ-m</ta>
            <ta e="T311" id="Seg_2728" s="T310">taw-ɨ-gu</ta>
            <ta e="T312" id="Seg_2729" s="T311">kɨkkɨ-ŋ</ta>
            <ta e="T323" id="Seg_2730" s="T312">muqtut</ta>
            <ta e="T313" id="Seg_2731" s="T323">saːrum</ta>
            <ta e="T314" id="Seg_2732" s="T313">lʼebɨ-ndɨ</ta>
            <ta e="T315" id="Seg_2733" s="T314">man</ta>
            <ta e="T316" id="Seg_2734" s="T315">tap-ɨ-m</ta>
            <ta e="T317" id="Seg_2735" s="T316">taw-ɨ-nǯɨ-m</ta>
            <ta e="T318" id="Seg_2736" s="T317">illɨ-nǯɨ-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2737" s="T0">force-EP-VBLZ-PST.NAR-1SG.S</ta>
            <ta e="T2" id="Seg_2738" s="T1">six</ta>
            <ta e="T3" id="Seg_2739" s="T2">superfluous-ten</ta>
            <ta e="T4" id="Seg_2740" s="T3">year-LOC.3SG</ta>
            <ta e="T324" id="Seg_2741" s="T4">two</ta>
            <ta e="T5" id="Seg_2742" s="T324">binding</ta>
            <ta e="T6" id="Seg_2743" s="T5">three</ta>
            <ta e="T7" id="Seg_2744" s="T6">superfluous-ten</ta>
            <ta e="T8" id="Seg_2745" s="T7">number-ILL.3SG</ta>
            <ta e="T9" id="Seg_2746" s="T8">September.[NOM]</ta>
            <ta e="T10" id="Seg_2747" s="T9">month-LOC</ta>
            <ta e="T11" id="Seg_2748" s="T10">village-LOC</ta>
            <ta e="T12" id="Seg_2749" s="T11">village.[NOM]</ta>
            <ta e="T13" id="Seg_2750" s="T12">name-3SG</ta>
            <ta e="T14" id="Seg_2751" s="T13">Karelino.[NOM]</ta>
            <ta e="T15" id="Seg_2752" s="T14">name-TR-INF</ta>
            <ta e="T16" id="Seg_2753" s="T15">I.ACC</ta>
            <ta e="T17" id="Seg_2754" s="T16">Matvey.[NOM]</ta>
            <ta e="T18" id="Seg_2755" s="T17">Karelin.[NOM]</ta>
            <ta e="T19" id="Seg_2756" s="T18">oneself.1SG</ta>
            <ta e="T20" id="Seg_2757" s="T19">human.being-PL-1SG</ta>
            <ta e="T21" id="Seg_2758" s="T20">long-LOC</ta>
            <ta e="T22" id="Seg_2759" s="T21">earlier</ta>
            <ta e="T23" id="Seg_2760" s="T22">fish-CAP-HAB-PST-3PL</ta>
            <ta e="T24" id="Seg_2761" s="T23">and</ta>
            <ta e="T25" id="Seg_2762" s="T24">wild.animal-CAP-HAB-PST-3PL</ta>
            <ta e="T26" id="Seg_2763" s="T25">I.ALL</ta>
            <ta e="T27" id="Seg_2764" s="T26">be-PST-3SG.S</ta>
            <ta e="T28" id="Seg_2765" s="T27">three</ta>
            <ta e="T29" id="Seg_2766" s="T28">year-3SG</ta>
            <ta e="T30" id="Seg_2767" s="T29">I.NOM</ta>
            <ta e="T31" id="Seg_2768" s="T30">stay-PST-1SG.S</ta>
            <ta e="T32" id="Seg_2769" s="T31">mother-1SG-ABL</ta>
            <ta e="T33" id="Seg_2770" s="T32">mother-1SG</ta>
            <ta e="T34" id="Seg_2771" s="T33">die-PST-3SG.S</ta>
            <ta e="T35" id="Seg_2772" s="T34">mother-1SG-ADES</ta>
            <ta e="T36" id="Seg_2773" s="T35">be-PST-3PL</ta>
            <ta e="T37" id="Seg_2774" s="T36">child-PL-3SG</ta>
            <ta e="T38" id="Seg_2775" s="T37">die-HAB-PST-3PL</ta>
            <ta e="T39" id="Seg_2776" s="T38">one</ta>
            <ta e="T40" id="Seg_2777" s="T39">I.NOM</ta>
            <ta e="T41" id="Seg_2778" s="T40">mother-1SG-ADES</ta>
            <ta e="T42" id="Seg_2779" s="T41">father-1SG-ADES</ta>
            <ta e="T43" id="Seg_2780" s="T42">stay-PST.NAR-1SG.S</ta>
            <ta e="T44" id="Seg_2781" s="T43">father-1SG</ta>
            <ta e="T45" id="Seg_2782" s="T44">go-HAB-PST.[3SG.S]</ta>
            <ta e="T46" id="Seg_2783" s="T45">wild.animal-CAP-INF</ta>
            <ta e="T47" id="Seg_2784" s="T46">fish-CAP-INF</ta>
            <ta e="T48" id="Seg_2785" s="T47">house-LOC</ta>
            <ta e="T49" id="Seg_2786" s="T48">NEG.EX-HAB-PST.[3SG.S]</ta>
            <ta e="T50" id="Seg_2787" s="T49">stay-HAB-PST-1SG.S</ta>
            <ta e="T51" id="Seg_2788" s="T50">live-HAB-PST-1SG.S</ta>
            <ta e="T52" id="Seg_2789" s="T51">grandfather-1SG-COM</ta>
            <ta e="T53" id="Seg_2790" s="T52">he.NOM-COM</ta>
            <ta e="T54" id="Seg_2791" s="T53">I.NOM</ta>
            <ta e="T55" id="Seg_2792" s="T54">go-HAB-PST-1SG.S</ta>
            <ta e="T56" id="Seg_2793" s="T55">winter-ADV.LOC</ta>
            <ta e="T57" id="Seg_2794" s="T56">ski-INSTR</ta>
            <ta e="T58" id="Seg_2795" s="T57">taiga-ILL</ta>
            <ta e="T59" id="Seg_2796" s="T58">summer-EP-ADV.LOC</ta>
            <ta e="T60" id="Seg_2797" s="T59">net-VBLZ-CVB</ta>
            <ta e="T61" id="Seg_2798" s="T60">and</ta>
            <ta e="T62" id="Seg_2799" s="T61">fish-FRQ-CVB</ta>
            <ta e="T63" id="Seg_2800" s="T62">fish-FRQ-INF</ta>
            <ta e="T64" id="Seg_2801" s="T63">so-ADVZ</ta>
            <ta e="T65" id="Seg_2802" s="T64">I.NOM</ta>
            <ta e="T66" id="Seg_2803" s="T65">live-HAB-PST-1SG.S</ta>
            <ta e="T67" id="Seg_2804" s="T66">grandfather-1SG</ta>
            <ta e="T68" id="Seg_2805" s="T67">I.ADES</ta>
            <ta e="T69" id="Seg_2806" s="T68">good</ta>
            <ta e="T70" id="Seg_2807" s="T69">be-HAB-PST</ta>
            <ta e="T71" id="Seg_2808" s="T70">whip-INF</ta>
            <ta e="T72" id="Seg_2809" s="T71">I.ACC</ta>
            <ta e="T73" id="Seg_2810" s="T72">father-1SG</ta>
            <ta e="T74" id="Seg_2811" s="T73">think-INFER.[3SG.S]</ta>
            <ta e="T75" id="Seg_2812" s="T74">grandfather-1SG</ta>
            <ta e="T76" id="Seg_2813" s="T75">help-HAB-PST.[3SG.S]</ta>
            <ta e="T77" id="Seg_2814" s="T76">help-HAB-PST-3SG.S</ta>
            <ta e="T78" id="Seg_2815" s="T77">he.NOM-COM</ta>
            <ta e="T79" id="Seg_2816" s="T78">I.NOM</ta>
            <ta e="T80" id="Seg_2817" s="T79">sleep-HAB-PST-1SG.S</ta>
            <ta e="T81" id="Seg_2818" s="T80">where</ta>
            <ta e="T82" id="Seg_2819" s="T81">he.NOM</ta>
            <ta e="T83" id="Seg_2820" s="T82">go-HAB-PST.[3SG.S]</ta>
            <ta e="T84" id="Seg_2821" s="T83">he.NOM-COM</ta>
            <ta e="T85" id="Seg_2822" s="T84">I.NOM</ta>
            <ta e="T86" id="Seg_2823" s="T85">be-HAB-PST-1SG.S</ta>
            <ta e="T87" id="Seg_2824" s="T86">I.ADES</ta>
            <ta e="T88" id="Seg_2825" s="T87">be-PST-3SG.S</ta>
            <ta e="T89" id="Seg_2826" s="T88">uncle-1SG</ta>
            <ta e="T90" id="Seg_2827" s="T89">name-3SG</ta>
            <ta e="T91" id="Seg_2828" s="T90">he.NOM-EP-GEN</ta>
            <ta e="T92" id="Seg_2829" s="T91">Mikhail.[NOM]</ta>
            <ta e="T93" id="Seg_2830" s="T92">I.ACC</ta>
            <ta e="T94" id="Seg_2831" s="T93">love-HAB-PST.[3SG.S]</ta>
            <ta e="T95" id="Seg_2832" s="T94">learn-PFV-HAB-PST.[3SG.S]</ta>
            <ta e="T96" id="Seg_2833" s="T95">go-INF</ta>
            <ta e="T97" id="Seg_2834" s="T96">ski-INSTR2</ta>
            <ta e="T98" id="Seg_2835" s="T97">boat-INSTR</ta>
            <ta e="T99" id="Seg_2836" s="T98">duck-EP-CAP-INF</ta>
            <ta e="T100" id="Seg_2837" s="T99">squirrel-CAP-INF</ta>
            <ta e="T101" id="Seg_2838" s="T100">rifle-INSTR2</ta>
            <ta e="T102" id="Seg_2839" s="T101">rifle-INSTR</ta>
            <ta e="T103" id="Seg_2840" s="T102">shoot-HAB-INF</ta>
            <ta e="T104" id="Seg_2841" s="T103">horse-GEN-top-LOC</ta>
            <ta e="T105" id="Seg_2842" s="T104">sit.down-INF</ta>
            <ta e="T106" id="Seg_2843" s="T105">father-GEN</ta>
            <ta e="T107" id="Seg_2844" s="T106">war-LOC</ta>
            <ta e="T108" id="Seg_2845" s="T107">he.NOM-EP-ACC</ta>
            <ta e="T109" id="Seg_2846" s="T108">kill-PST.NAR-3PL</ta>
            <ta e="T110" id="Seg_2847" s="T109">I.ADES</ta>
            <ta e="T111" id="Seg_2848" s="T110">be-PST-3SG.S</ta>
            <ta e="T112" id="Seg_2849" s="T111">aunt-1SG</ta>
            <ta e="T113" id="Seg_2850" s="T112">he.NOM</ta>
            <ta e="T114" id="Seg_2851" s="T113">be-HAB-PST.[3SG.S]</ta>
            <ta e="T115" id="Seg_2852" s="T114">be.similar-HAB-PST.[3SG.S]</ta>
            <ta e="T116" id="Seg_2853" s="T115">bear-ALL</ta>
            <ta e="T117" id="Seg_2854" s="T116">I.ALL</ta>
            <ta e="T118" id="Seg_2855" s="T117">swear-DUR-HAB-PST.[3SG.S]</ta>
            <ta e="T119" id="Seg_2856" s="T118">I.ACC</ta>
            <ta e="T120" id="Seg_2857" s="T119">whip-HAB-PST-3SG.O</ta>
            <ta e="T121" id="Seg_2858" s="T120">now</ta>
            <ta e="T122" id="Seg_2859" s="T121">also</ta>
            <ta e="T123" id="Seg_2860" s="T122">die-PST.NAR.[3SG.S]</ta>
            <ta e="T124" id="Seg_2861" s="T123">learn-PFV-HAB-PST-1SG.S</ta>
            <ta e="T125" id="Seg_2862" s="T124">one</ta>
            <ta e="T126" id="Seg_2863" s="T125">class-EL</ta>
            <ta e="T127" id="Seg_2864" s="T126">village-LOC</ta>
            <ta e="T128" id="Seg_2865" s="T127">name-3SG</ta>
            <ta e="T129" id="Seg_2866" s="T128">he.NOM-EP-GEN</ta>
            <ta e="T130" id="Seg_2867" s="T129">be-HAB-PST.[3SG.S]</ta>
            <ta e="T131" id="Seg_2868" s="T130">Pristan.[NOM]</ta>
            <ta e="T132" id="Seg_2869" s="T131">now</ta>
            <ta e="T133" id="Seg_2870" s="T132">this</ta>
            <ta e="T134" id="Seg_2871" s="T133">Pristan.[NOM]</ta>
            <ta e="T135" id="Seg_2872" s="T134">name-3SG</ta>
            <ta e="T136" id="Seg_2873" s="T135">white</ta>
            <ta e="T137" id="Seg_2874" s="T136">river</ta>
            <ta e="T138" id="Seg_2875" s="T137">Upper.Ket-ADJZ</ta>
            <ta e="T139" id="Seg_2876" s="T138">region.[NOM]</ta>
            <ta e="T140" id="Seg_2877" s="T139">four</ta>
            <ta e="T141" id="Seg_2878" s="T140">class-EP-ACC</ta>
            <ta e="T142" id="Seg_2879" s="T141">do-PFV-PST-1SG.O</ta>
            <ta e="T143" id="Seg_2880" s="T142">Maksimkin.Yar</ta>
            <ta e="T144" id="Seg_2881" s="T143">taiga-LOC</ta>
            <ta e="T145" id="Seg_2882" s="T144">Maksimkin.Yar</ta>
            <ta e="T146" id="Seg_2883" s="T145">taiga-EL.3SG</ta>
            <ta e="T147" id="Seg_2884" s="T146">go.away-PST-1SG.S</ta>
            <ta e="T148" id="Seg_2885" s="T147">learn-TR-HAB-INF</ta>
            <ta e="T149" id="Seg_2886" s="T148">Kargasok</ta>
            <ta e="T150" id="Seg_2887" s="T149">region-ILL</ta>
            <ta e="T151" id="Seg_2888" s="T150">school.[NOM]</ta>
            <ta e="T152" id="Seg_2889" s="T151">name-3SG</ta>
            <ta e="T153" id="Seg_2890" s="T152">Verte_Kos</ta>
            <ta e="T154" id="Seg_2891" s="T153">then</ta>
            <ta e="T155" id="Seg_2892" s="T154">I.NOM</ta>
            <ta e="T156" id="Seg_2893" s="T155">do-PFV-PST-1SG.O</ta>
            <ta e="T157" id="Seg_2894" s="T156">seven</ta>
            <ta e="T158" id="Seg_2895" s="T157">class-EP-ACC</ta>
            <ta e="T159" id="Seg_2896" s="T158">then</ta>
            <ta e="T160" id="Seg_2897" s="T159">I.NOM</ta>
            <ta e="T161" id="Seg_2898" s="T160">learn-PFV-HAB-PST-1SG.S</ta>
            <ta e="T162" id="Seg_2899" s="T161">Kolpashevo-LOC</ta>
            <ta e="T163" id="Seg_2900" s="T162">pedagogical.school.[NOM]</ta>
            <ta e="T164" id="Seg_2901" s="T163">year-ILL.3SG</ta>
            <ta e="T165" id="Seg_2902" s="T164">learn-PFV-HAB-PST-1SG.S</ta>
            <ta e="T166" id="Seg_2903" s="T165">preparatory</ta>
            <ta e="T167" id="Seg_2904" s="T166">course-LOC</ta>
            <ta e="T168" id="Seg_2905" s="T167">and</ta>
            <ta e="T169" id="Seg_2906" s="T168">cross-PST-1SG.S</ta>
            <ta e="T170" id="Seg_2907" s="T169">I.NOM</ta>
            <ta e="T171" id="Seg_2908" s="T170">one</ta>
            <ta e="T172" id="Seg_2909" s="T171">course-ILL</ta>
            <ta e="T173" id="Seg_2910" s="T172">one</ta>
            <ta e="T174" id="Seg_2911" s="T173">course-EL</ta>
            <ta e="T320" id="Seg_2912" s="T174">three</ta>
            <ta e="T175" id="Seg_2913" s="T320">binding</ta>
            <ta e="T176" id="Seg_2914" s="T175">nine-superfluous-ten</ta>
            <ta e="T177" id="Seg_2915" s="T176">year-LOC.3SG</ta>
            <ta e="T178" id="Seg_2916" s="T177">I.ACC</ta>
            <ta e="T179" id="Seg_2917" s="T178">take-PST-3PL</ta>
            <ta e="T180" id="Seg_2918" s="T179">red</ta>
            <ta e="T181" id="Seg_2919" s="T180">army-ILL</ta>
            <ta e="T182" id="Seg_2920" s="T181">there-ADV.LOC</ta>
            <ta e="T183" id="Seg_2921" s="T182">I.NOM</ta>
            <ta e="T184" id="Seg_2922" s="T183">be-PST-1SG.S</ta>
            <ta e="T185" id="Seg_2923" s="T184">seven</ta>
            <ta e="T186" id="Seg_2924" s="T185">year-ADV.LOC</ta>
            <ta e="T321" id="Seg_2925" s="T186">four</ta>
            <ta e="T187" id="Seg_2926" s="T321">binding</ta>
            <ta e="T188" id="Seg_2927" s="T187">six-superfluous</ta>
            <ta e="T189" id="Seg_2928" s="T188">year-ADV.LOC</ta>
            <ta e="T190" id="Seg_2929" s="T189">I.ACC</ta>
            <ta e="T191" id="Seg_2930" s="T190">let.go-3PL</ta>
            <ta e="T192" id="Seg_2931" s="T191">house-ILL.3SG</ta>
            <ta e="T193" id="Seg_2932" s="T192">this</ta>
            <ta e="T194" id="Seg_2933" s="T193">year-ADV.LOC</ta>
            <ta e="T195" id="Seg_2934" s="T194">I.NOM</ta>
            <ta e="T196" id="Seg_2935" s="T195">come-PST-1SG.S</ta>
            <ta e="T197" id="Seg_2936" s="T196">white</ta>
            <ta e="T198" id="Seg_2937" s="T197">taiga-ILL</ta>
            <ta e="T199" id="Seg_2938" s="T198">here</ta>
            <ta e="T201" id="Seg_2939" s="T199">this-EMPH3</ta>
            <ta e="T202" id="Seg_2940" s="T201">year-ADV.LOC</ta>
            <ta e="T203" id="Seg_2941" s="T202">I.NOM</ta>
            <ta e="T204" id="Seg_2942" s="T203">get.married-PST-1SG.S</ta>
            <ta e="T205" id="Seg_2943" s="T204">year-LOC.3SG</ta>
            <ta e="T206" id="Seg_2944" s="T205">be-PST-1SG.S</ta>
            <ta e="T207" id="Seg_2945" s="T206">Orlovka-LOC</ta>
            <ta e="T208" id="Seg_2946" s="T207">then</ta>
            <ta e="T209" id="Seg_2947" s="T208">work-PST-1SG.S</ta>
            <ta e="T210" id="Seg_2948" s="T209">white</ta>
            <ta e="T211" id="Seg_2949" s="T210">taiga-LOC</ta>
            <ta e="T212" id="Seg_2950" s="T211">white</ta>
            <ta e="T213" id="Seg_2951" s="T212">taiga-EL.3SG</ta>
            <ta e="T214" id="Seg_2952" s="T213">I.ACC</ta>
            <ta e="T215" id="Seg_2953" s="T214">let.go-PST-3PL</ta>
            <ta e="T216" id="Seg_2954" s="T215">Ust_Ozyornoe.[NOM]</ta>
            <ta e="T217" id="Seg_2955" s="T216">village-ILL</ta>
            <ta e="T218" id="Seg_2956" s="T217">village-LOC</ta>
            <ta e="T219" id="Seg_2957" s="T218">work-PST-1SG.S</ta>
            <ta e="T220" id="Seg_2958" s="T219">club-LOC</ta>
            <ta e="T221" id="Seg_2959" s="T220">then</ta>
            <ta e="T222" id="Seg_2960" s="T221">fish-CAP.ADJZ</ta>
            <ta e="T223" id="Seg_2961" s="T222">artels-LOC</ta>
            <ta e="T225" id="Seg_2962" s="T224">then</ta>
            <ta e="T226" id="Seg_2963" s="T225">village.council-LOC</ta>
            <ta e="T229" id="Seg_2964" s="T228">work-1SG.S</ta>
            <ta e="T230" id="Seg_2965" s="T229">there</ta>
            <ta e="T231" id="Seg_2966" s="T230">I.NOM</ta>
            <ta e="T232" id="Seg_2967" s="T231">get.married-PST.NAR-1SG.S</ta>
            <ta e="T233" id="Seg_2968" s="T232">I.NOM</ta>
            <ta e="T234" id="Seg_2969" s="T233">old.woman-GEN-OBL.1SG-COM</ta>
            <ta e="T235" id="Seg_2970" s="T234">live-1SG.S</ta>
            <ta e="T236" id="Seg_2971" s="T235">eight-superfluous-ten</ta>
            <ta e="T237" id="Seg_2972" s="T236">year-3SG</ta>
            <ta e="T238" id="Seg_2973" s="T237">child</ta>
            <ta e="T239" id="Seg_2974" s="T238">%%</ta>
            <ta e="T240" id="Seg_2975" s="T239">be-PST.NAR-3SG.S</ta>
            <ta e="T241" id="Seg_2976" s="T240">five</ta>
            <ta e="T242" id="Seg_2977" s="T241">son-1SG</ta>
            <ta e="T243" id="Seg_2978" s="T242">and</ta>
            <ta e="T244" id="Seg_2979" s="T243">two</ta>
            <ta e="T245" id="Seg_2980" s="T244">daughter-1SG</ta>
            <ta e="T246" id="Seg_2981" s="T245">old.woman.[NOM]-1SG</ta>
            <ta e="T247" id="Seg_2982" s="T246">Russian</ta>
            <ta e="T248" id="Seg_2983" s="T247">child-PL-1SG</ta>
            <ta e="T319" id="Seg_2984" s="T248">Selkup</ta>
            <ta e="T249" id="Seg_2985" s="T319">on</ta>
            <ta e="T250" id="Seg_2986" s="T249">NEG</ta>
            <ta e="T251" id="Seg_2987" s="T250">speak-EP-3PL</ta>
            <ta e="T252" id="Seg_2988" s="T251">big</ta>
            <ta e="T253" id="Seg_2989" s="T252">son-1SG</ta>
            <ta e="T254" id="Seg_2990" s="T253">do-PFV-PST.NAR-3SG.O</ta>
            <ta e="T255" id="Seg_2991" s="T254">this</ta>
            <ta e="T256" id="Seg_2992" s="T255">year-ADV.LOC</ta>
            <ta e="T257" id="Seg_2993" s="T256">ten</ta>
            <ta e="T258" id="Seg_2994" s="T257">class-EP-ACC</ta>
            <ta e="T259" id="Seg_2995" s="T258">small</ta>
            <ta e="T260" id="Seg_2996" s="T259">son-ILL.3SG</ta>
            <ta e="T261" id="Seg_2997" s="T260">five</ta>
            <ta e="T262" id="Seg_2998" s="T261">six-ORD</ta>
            <ta e="T263" id="Seg_2999" s="T262">year-3SG</ta>
            <ta e="T264" id="Seg_3000" s="T263">one</ta>
            <ta e="T265" id="Seg_3001" s="T264">son-1SG</ta>
            <ta e="T266" id="Seg_3002" s="T265">strongly</ta>
            <ta e="T267" id="Seg_3003" s="T266">hurt-PST.[3SG.S]</ta>
            <ta e="T268" id="Seg_3004" s="T267">he.NOM-EP-ACC</ta>
            <ta e="T269" id="Seg_3005" s="T268">go.away-TR-HAB-PST-3PL</ta>
            <ta e="T270" id="Seg_3006" s="T269">Tomsk-ILL</ta>
            <ta e="T271" id="Seg_3007" s="T270">he.NOM-EP-ACC</ta>
            <ta e="T272" id="Seg_3008" s="T271">there</ta>
            <ta e="T273" id="Seg_3009" s="T272">good-TRL-TR-3PL</ta>
            <ta e="T274" id="Seg_3010" s="T273">bring-PST.NAR-3PL</ta>
            <ta e="T275" id="Seg_3011" s="T274">back</ta>
            <ta e="T276" id="Seg_3012" s="T275">I.GEN</ta>
            <ta e="T277" id="Seg_3013" s="T276">child-PL-1SG</ta>
            <ta e="T278" id="Seg_3014" s="T277">Selkup-PL</ta>
            <ta e="T279" id="Seg_3015" s="T278">he.NOM-EP-PL</ta>
            <ta e="T280" id="Seg_3016" s="T279">learn-PFV-HAB-CO-3PL</ta>
            <ta e="T281" id="Seg_3017" s="T280">money-CAR-ADVZ</ta>
            <ta e="T282" id="Seg_3018" s="T281">oneself.1SG</ta>
            <ta e="T283" id="Seg_3019" s="T282">I.NOM</ta>
            <ta e="T284" id="Seg_3020" s="T283">work-DRV-CO-1SG.O</ta>
            <ta e="T322" id="Seg_3021" s="T284">four</ta>
            <ta e="T285" id="Seg_3022" s="T322">binding</ta>
            <ta e="T286" id="Seg_3023" s="T285">five</ta>
            <ta e="T287" id="Seg_3024" s="T286">superfluous-ten</ta>
            <ta e="T288" id="Seg_3025" s="T287">ruble-ACC</ta>
            <ta e="T289" id="Seg_3026" s="T288">month-LOC.3SG</ta>
            <ta e="T290" id="Seg_3027" s="T289">live-1SG.S</ta>
            <ta e="T291" id="Seg_3028" s="T290">I.NOM</ta>
            <ta e="T292" id="Seg_3029" s="T291">good-ADVZ</ta>
            <ta e="T293" id="Seg_3030" s="T292">autumn-ADV.LOC</ta>
            <ta e="T294" id="Seg_3031" s="T293">pick.nut-HAB-CO-1SG.S</ta>
            <ta e="T295" id="Seg_3032" s="T294">capercaillie-CAP-HAB-CO-1SG.S</ta>
            <ta e="T296" id="Seg_3033" s="T295">berry-EP-CAP-HAB-CO-1SG.S</ta>
            <ta e="T297" id="Seg_3034" s="T296">wild.animal-CAP-EP-HAB-CO-1SG.S</ta>
            <ta e="T298" id="Seg_3035" s="T297">summer-EP-ADV.LOC</ta>
            <ta e="T299" id="Seg_3036" s="T298">net-FRQ-EP-HAB-CO-1SG.S</ta>
            <ta e="T300" id="Seg_3037" s="T299">fish-CAP-HAB-CO-1SG.S</ta>
            <ta e="T301" id="Seg_3038" s="T300">now</ta>
            <ta e="T302" id="Seg_3039" s="T301">want-1SG.S</ta>
            <ta e="T303" id="Seg_3040" s="T302">buy-EP-INF</ta>
            <ta e="T304" id="Seg_3041" s="T303">new</ta>
            <ta e="T305" id="Seg_3042" s="T304">house-EP-ACC</ta>
            <ta e="T306" id="Seg_3043" s="T305">oneself.1SG</ta>
            <ta e="T307" id="Seg_3044" s="T306">house-1SG</ta>
            <ta e="T308" id="Seg_3045" s="T307">go.bad-PST.NAR.[3SG.S]</ta>
            <ta e="T309" id="Seg_3046" s="T308">new</ta>
            <ta e="T310" id="Seg_3047" s="T309">house-EP-ACC</ta>
            <ta e="T311" id="Seg_3048" s="T310">buy-EP-INF</ta>
            <ta e="T312" id="Seg_3049" s="T311">want-1SG.S</ta>
            <ta e="T323" id="Seg_3050" s="T312">six</ta>
            <ta e="T313" id="Seg_3051" s="T323">binding</ta>
            <ta e="T314" id="Seg_3052" s="T313">ruble-ILL</ta>
            <ta e="T315" id="Seg_3053" s="T314">I.NOM</ta>
            <ta e="T316" id="Seg_3054" s="T315">he.NOM-EP-ACC</ta>
            <ta e="T317" id="Seg_3055" s="T316">buy-EP-FUT-1SG.O</ta>
            <ta e="T318" id="Seg_3056" s="T317">live-FUT-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_3057" s="T0">сила-EP-VBLZ-PST.NAR-1SG.S</ta>
            <ta e="T2" id="Seg_3058" s="T1">шесть</ta>
            <ta e="T3" id="Seg_3059" s="T2">излишний-десять</ta>
            <ta e="T4" id="Seg_3060" s="T3">год-LOC.3SG</ta>
            <ta e="T324" id="Seg_3061" s="T4">два</ta>
            <ta e="T5" id="Seg_3062" s="T324">связь</ta>
            <ta e="T6" id="Seg_3063" s="T5">три</ta>
            <ta e="T7" id="Seg_3064" s="T6">излишний-десять</ta>
            <ta e="T8" id="Seg_3065" s="T7">число-ILL.3SG</ta>
            <ta e="T9" id="Seg_3066" s="T8">сентябрь.[NOM]</ta>
            <ta e="T10" id="Seg_3067" s="T9">месяц-LOC</ta>
            <ta e="T11" id="Seg_3068" s="T10">деревня-LOC</ta>
            <ta e="T12" id="Seg_3069" s="T11">деревня.[NOM]</ta>
            <ta e="T13" id="Seg_3070" s="T12">имя-3SG</ta>
            <ta e="T14" id="Seg_3071" s="T13">Карелино.[NOM]</ta>
            <ta e="T15" id="Seg_3072" s="T14">имя-TR-INF</ta>
            <ta e="T16" id="Seg_3073" s="T15">я.ACC</ta>
            <ta e="T17" id="Seg_3074" s="T16">Матвей.[NOM]</ta>
            <ta e="T18" id="Seg_3075" s="T17">Карелин.[NOM]</ta>
            <ta e="T19" id="Seg_3076" s="T18">сам.1SG</ta>
            <ta e="T20" id="Seg_3077" s="T19">человек-PL-1SG</ta>
            <ta e="T21" id="Seg_3078" s="T20">долго-LOC</ta>
            <ta e="T22" id="Seg_3079" s="T21">раньше</ta>
            <ta e="T23" id="Seg_3080" s="T22">рыба-CAP-HAB-PST-3PL</ta>
            <ta e="T24" id="Seg_3081" s="T23">и</ta>
            <ta e="T25" id="Seg_3082" s="T24">зверь-CAP-HAB-PST-3PL</ta>
            <ta e="T26" id="Seg_3083" s="T25">я.ALL</ta>
            <ta e="T27" id="Seg_3084" s="T26">быть-PST-3SG.S</ta>
            <ta e="T28" id="Seg_3085" s="T27">три</ta>
            <ta e="T29" id="Seg_3086" s="T28">год-3SG</ta>
            <ta e="T30" id="Seg_3087" s="T29">я.NOM</ta>
            <ta e="T31" id="Seg_3088" s="T30">остаться-PST-1SG.S</ta>
            <ta e="T32" id="Seg_3089" s="T31">мать-1SG-ABL</ta>
            <ta e="T33" id="Seg_3090" s="T32">мать-1SG</ta>
            <ta e="T34" id="Seg_3091" s="T33">умереть-PST-3SG.S</ta>
            <ta e="T35" id="Seg_3092" s="T34">мать-1SG-ADES</ta>
            <ta e="T36" id="Seg_3093" s="T35">быть-PST-3PL</ta>
            <ta e="T37" id="Seg_3094" s="T36">ребёнок-PL-3SG</ta>
            <ta e="T38" id="Seg_3095" s="T37">умереть-HAB-PST-3PL</ta>
            <ta e="T39" id="Seg_3096" s="T38">один</ta>
            <ta e="T40" id="Seg_3097" s="T39">я.NOM</ta>
            <ta e="T41" id="Seg_3098" s="T40">мать-1SG-ADES</ta>
            <ta e="T42" id="Seg_3099" s="T41">отец-1SG-ADES</ta>
            <ta e="T43" id="Seg_3100" s="T42">остаться-PST.NAR-1SG.S</ta>
            <ta e="T44" id="Seg_3101" s="T43">отец-1SG</ta>
            <ta e="T45" id="Seg_3102" s="T44">ходить-HAB-PST.[3SG.S]</ta>
            <ta e="T46" id="Seg_3103" s="T45">зверь-CAP-INF</ta>
            <ta e="T47" id="Seg_3104" s="T46">рыба-CAP-INF</ta>
            <ta e="T48" id="Seg_3105" s="T47">дом-LOC</ta>
            <ta e="T49" id="Seg_3106" s="T48">NEG.EX-HAB-PST.[3SG.S]</ta>
            <ta e="T50" id="Seg_3107" s="T49">остаться-HAB-PST-1SG.S</ta>
            <ta e="T51" id="Seg_3108" s="T50">жить-HAB-PST-1SG.S</ta>
            <ta e="T52" id="Seg_3109" s="T51">дедушка-1SG-COM</ta>
            <ta e="T53" id="Seg_3110" s="T52">он.NOM-COM</ta>
            <ta e="T54" id="Seg_3111" s="T53">я.NOM</ta>
            <ta e="T55" id="Seg_3112" s="T54">ходить-HAB-PST-1SG.S</ta>
            <ta e="T56" id="Seg_3113" s="T55">зима-ADV.LOC</ta>
            <ta e="T57" id="Seg_3114" s="T56">лыжа-INSTR</ta>
            <ta e="T58" id="Seg_3115" s="T57">тайга-ILL</ta>
            <ta e="T59" id="Seg_3116" s="T58">лето-EP-ADV.LOC</ta>
            <ta e="T60" id="Seg_3117" s="T59">сеть-VBLZ-CVB</ta>
            <ta e="T61" id="Seg_3118" s="T60">и</ta>
            <ta e="T62" id="Seg_3119" s="T61">удить.рыбу-FRQ-CVB</ta>
            <ta e="T63" id="Seg_3120" s="T62">удить.рыбу-FRQ-INF</ta>
            <ta e="T64" id="Seg_3121" s="T63">так-ADVZ</ta>
            <ta e="T65" id="Seg_3122" s="T64">я.NOM</ta>
            <ta e="T66" id="Seg_3123" s="T65">жить-HAB-PST-1SG.S</ta>
            <ta e="T67" id="Seg_3124" s="T66">дедушка-1SG</ta>
            <ta e="T68" id="Seg_3125" s="T67">я.ADES</ta>
            <ta e="T69" id="Seg_3126" s="T68">хороший</ta>
            <ta e="T70" id="Seg_3127" s="T69">быть-HAB-PST</ta>
            <ta e="T71" id="Seg_3128" s="T70">стегать-INF</ta>
            <ta e="T72" id="Seg_3129" s="T71">я.ACC</ta>
            <ta e="T73" id="Seg_3130" s="T72">отец-1SG</ta>
            <ta e="T74" id="Seg_3131" s="T73">вздумать-INFER.[3SG.S]</ta>
            <ta e="T75" id="Seg_3132" s="T74">дедушка-1SG</ta>
            <ta e="T76" id="Seg_3133" s="T75">помочь-HAB-PST.[3SG.S]</ta>
            <ta e="T77" id="Seg_3134" s="T76">помочь-HAB-PST-3SG.S</ta>
            <ta e="T78" id="Seg_3135" s="T77">он.NOM-COM</ta>
            <ta e="T79" id="Seg_3136" s="T78">я.NOM</ta>
            <ta e="T80" id="Seg_3137" s="T79">спать-HAB-PST-1SG.S</ta>
            <ta e="T81" id="Seg_3138" s="T80">куда</ta>
            <ta e="T82" id="Seg_3139" s="T81">он.NOM</ta>
            <ta e="T83" id="Seg_3140" s="T82">ходить-HAB-PST.[3SG.S]</ta>
            <ta e="T84" id="Seg_3141" s="T83">он.NOM-COM</ta>
            <ta e="T85" id="Seg_3142" s="T84">я.NOM</ta>
            <ta e="T86" id="Seg_3143" s="T85">быть-HAB-PST-1SG.S</ta>
            <ta e="T87" id="Seg_3144" s="T86">я.ADES</ta>
            <ta e="T88" id="Seg_3145" s="T87">быть-PST-3SG.S</ta>
            <ta e="T89" id="Seg_3146" s="T88">дядя-1SG</ta>
            <ta e="T90" id="Seg_3147" s="T89">имя-3SG</ta>
            <ta e="T91" id="Seg_3148" s="T90">он.NOM-EP-GEN</ta>
            <ta e="T92" id="Seg_3149" s="T91">Михаил.[NOM]</ta>
            <ta e="T93" id="Seg_3150" s="T92">я.ACC</ta>
            <ta e="T94" id="Seg_3151" s="T93">любить-HAB-PST.[3SG.S]</ta>
            <ta e="T95" id="Seg_3152" s="T94">учиться-PFV-HAB-PST.[3SG.S]</ta>
            <ta e="T96" id="Seg_3153" s="T95">ходить-INF</ta>
            <ta e="T97" id="Seg_3154" s="T96">лыжа-INSTR2</ta>
            <ta e="T98" id="Seg_3155" s="T97">ветка-INSTR</ta>
            <ta e="T99" id="Seg_3156" s="T98">утка-EP-CAP-INF</ta>
            <ta e="T100" id="Seg_3157" s="T99">белка-CAP-INF</ta>
            <ta e="T101" id="Seg_3158" s="T100">ружье-INSTR2</ta>
            <ta e="T102" id="Seg_3159" s="T101">ружье-INSTR</ta>
            <ta e="T103" id="Seg_3160" s="T102">стрелять-HAB-INF</ta>
            <ta e="T104" id="Seg_3161" s="T103">лошадь-GEN-верх-LOC</ta>
            <ta e="T105" id="Seg_3162" s="T104">сесть-INF</ta>
            <ta e="T106" id="Seg_3163" s="T105">отец-GEN</ta>
            <ta e="T107" id="Seg_3164" s="T106">война-LOC</ta>
            <ta e="T108" id="Seg_3165" s="T107">он.NOM-EP-ACC</ta>
            <ta e="T109" id="Seg_3166" s="T108">убить-PST.NAR-3PL</ta>
            <ta e="T110" id="Seg_3167" s="T109">я.ADES</ta>
            <ta e="T111" id="Seg_3168" s="T110">быть-PST-3SG.S</ta>
            <ta e="T112" id="Seg_3169" s="T111">тётя-1SG</ta>
            <ta e="T113" id="Seg_3170" s="T112">он.NOM</ta>
            <ta e="T114" id="Seg_3171" s="T113">быть-HAB-PST.[3SG.S]</ta>
            <ta e="T115" id="Seg_3172" s="T114">быть.похожими-HAB-PST.[3SG.S]</ta>
            <ta e="T116" id="Seg_3173" s="T115">медведь-ALL</ta>
            <ta e="T117" id="Seg_3174" s="T116">я.ALL</ta>
            <ta e="T118" id="Seg_3175" s="T117">ругаться-DUR-HAB-PST.[3SG.S]</ta>
            <ta e="T119" id="Seg_3176" s="T118">я.ACC</ta>
            <ta e="T120" id="Seg_3177" s="T119">стегать-HAB-PST-3SG.O</ta>
            <ta e="T121" id="Seg_3178" s="T120">сейчас</ta>
            <ta e="T122" id="Seg_3179" s="T121">тоже</ta>
            <ta e="T123" id="Seg_3180" s="T122">умереть-PST.NAR.[3SG.S]</ta>
            <ta e="T124" id="Seg_3181" s="T123">учиться-PFV-HAB-PST-1SG.S</ta>
            <ta e="T125" id="Seg_3182" s="T124">один</ta>
            <ta e="T126" id="Seg_3183" s="T125">класс-EL</ta>
            <ta e="T127" id="Seg_3184" s="T126">деревня-LOC</ta>
            <ta e="T128" id="Seg_3185" s="T127">имя-3SG</ta>
            <ta e="T129" id="Seg_3186" s="T128">он.NOM-EP-GEN</ta>
            <ta e="T130" id="Seg_3187" s="T129">быть-HAB-PST.[3SG.S]</ta>
            <ta e="T131" id="Seg_3188" s="T130">Пристань.[NOM]</ta>
            <ta e="T132" id="Seg_3189" s="T131">сейчас</ta>
            <ta e="T133" id="Seg_3190" s="T132">этот</ta>
            <ta e="T134" id="Seg_3191" s="T133">Пристань.[NOM]</ta>
            <ta e="T135" id="Seg_3192" s="T134">имя-3SG</ta>
            <ta e="T136" id="Seg_3193" s="T135">белый</ta>
            <ta e="T137" id="Seg_3194" s="T136">река</ta>
            <ta e="T138" id="Seg_3195" s="T137">Верхняя.Кеть-ADJZ</ta>
            <ta e="T139" id="Seg_3196" s="T138">район.[NOM]</ta>
            <ta e="T140" id="Seg_3197" s="T139">четыре</ta>
            <ta e="T141" id="Seg_3198" s="T140">класс-EP-ACC</ta>
            <ta e="T142" id="Seg_3199" s="T141">делать-PFV-PST-1SG.O</ta>
            <ta e="T143" id="Seg_3200" s="T142">Максимкин.Яр</ta>
            <ta e="T144" id="Seg_3201" s="T143">тайга-LOC</ta>
            <ta e="T145" id="Seg_3202" s="T144">Максимкин.Яр</ta>
            <ta e="T146" id="Seg_3203" s="T145">тайга-EL.3SG</ta>
            <ta e="T147" id="Seg_3204" s="T146">уйти-PST-1SG.S</ta>
            <ta e="T148" id="Seg_3205" s="T147">учиться-TR-HAB-INF</ta>
            <ta e="T149" id="Seg_3206" s="T148">Каргасокский</ta>
            <ta e="T150" id="Seg_3207" s="T149">район-ILL</ta>
            <ta e="T151" id="Seg_3208" s="T150">школа.[NOM]</ta>
            <ta e="T152" id="Seg_3209" s="T151">имя-3SG</ta>
            <ta e="T153" id="Seg_3210" s="T152">Верте_Кос</ta>
            <ta e="T154" id="Seg_3211" s="T153">потом</ta>
            <ta e="T155" id="Seg_3212" s="T154">я.NOM</ta>
            <ta e="T156" id="Seg_3213" s="T155">делать-PFV-PST-1SG.O</ta>
            <ta e="T157" id="Seg_3214" s="T156">семь</ta>
            <ta e="T158" id="Seg_3215" s="T157">класс-EP-ACC</ta>
            <ta e="T159" id="Seg_3216" s="T158">потом</ta>
            <ta e="T160" id="Seg_3217" s="T159">я.NOM</ta>
            <ta e="T161" id="Seg_3218" s="T160">учиться-PFV-HAB-PST-1SG.S</ta>
            <ta e="T162" id="Seg_3219" s="T161">Колпашево-LOC</ta>
            <ta e="T163" id="Seg_3220" s="T162">педучилище.[NOM]</ta>
            <ta e="T164" id="Seg_3221" s="T163">год-ILL.3SG</ta>
            <ta e="T165" id="Seg_3222" s="T164">учиться-PFV-HAB-PST-1SG.S</ta>
            <ta e="T166" id="Seg_3223" s="T165">подготовительный</ta>
            <ta e="T167" id="Seg_3224" s="T166">курс-LOC</ta>
            <ta e="T168" id="Seg_3225" s="T167">и</ta>
            <ta e="T169" id="Seg_3226" s="T168">перейти-PST-1SG.S</ta>
            <ta e="T170" id="Seg_3227" s="T169">я.NOM</ta>
            <ta e="T171" id="Seg_3228" s="T170">один</ta>
            <ta e="T172" id="Seg_3229" s="T171">курс-ILL</ta>
            <ta e="T173" id="Seg_3230" s="T172">один</ta>
            <ta e="T174" id="Seg_3231" s="T173">курс-EL</ta>
            <ta e="T320" id="Seg_3232" s="T174">три</ta>
            <ta e="T175" id="Seg_3233" s="T320">связь</ta>
            <ta e="T176" id="Seg_3234" s="T175">девять-излишний-десять</ta>
            <ta e="T177" id="Seg_3235" s="T176">год-LOC.3SG</ta>
            <ta e="T178" id="Seg_3236" s="T177">я.ACC</ta>
            <ta e="T179" id="Seg_3237" s="T178">взять-PST-3PL</ta>
            <ta e="T180" id="Seg_3238" s="T179">красный</ta>
            <ta e="T181" id="Seg_3239" s="T180">армия-ILL</ta>
            <ta e="T182" id="Seg_3240" s="T181">там-ADV.LOC</ta>
            <ta e="T183" id="Seg_3241" s="T182">я.NOM</ta>
            <ta e="T184" id="Seg_3242" s="T183">быть-PST-1SG.S</ta>
            <ta e="T185" id="Seg_3243" s="T184">семь</ta>
            <ta e="T186" id="Seg_3244" s="T185">год-ADV.LOC</ta>
            <ta e="T321" id="Seg_3245" s="T186">четыре</ta>
            <ta e="T187" id="Seg_3246" s="T321">связь</ta>
            <ta e="T188" id="Seg_3247" s="T187">шесть-излишний</ta>
            <ta e="T189" id="Seg_3248" s="T188">год-ADV.LOC</ta>
            <ta e="T190" id="Seg_3249" s="T189">я.ACC</ta>
            <ta e="T191" id="Seg_3250" s="T190">пустить-3PL</ta>
            <ta e="T192" id="Seg_3251" s="T191">дом-ILL.3SG</ta>
            <ta e="T193" id="Seg_3252" s="T192">этот</ta>
            <ta e="T194" id="Seg_3253" s="T193">год-ADV.LOC</ta>
            <ta e="T195" id="Seg_3254" s="T194">я.NOM</ta>
            <ta e="T196" id="Seg_3255" s="T195">прийти-PST-1SG.S</ta>
            <ta e="T197" id="Seg_3256" s="T196">белый</ta>
            <ta e="T198" id="Seg_3257" s="T197">тайга-ILL</ta>
            <ta e="T199" id="Seg_3258" s="T198">здесь</ta>
            <ta e="T201" id="Seg_3259" s="T199">этот-EMPH3</ta>
            <ta e="T202" id="Seg_3260" s="T201">год-ADV.LOC</ta>
            <ta e="T203" id="Seg_3261" s="T202">я.NOM</ta>
            <ta e="T204" id="Seg_3262" s="T203">жениться-PST-1SG.S</ta>
            <ta e="T205" id="Seg_3263" s="T204">год-LOC.3SG</ta>
            <ta e="T206" id="Seg_3264" s="T205">быть-PST-1SG.S</ta>
            <ta e="T207" id="Seg_3265" s="T206">Орловка-LOC</ta>
            <ta e="T208" id="Seg_3266" s="T207">потом</ta>
            <ta e="T209" id="Seg_3267" s="T208">работать-PST-1SG.S</ta>
            <ta e="T210" id="Seg_3268" s="T209">белый</ta>
            <ta e="T211" id="Seg_3269" s="T210">тайга-LOC</ta>
            <ta e="T212" id="Seg_3270" s="T211">белый</ta>
            <ta e="T213" id="Seg_3271" s="T212">тайга-EL.3SG</ta>
            <ta e="T214" id="Seg_3272" s="T213">я.ACC</ta>
            <ta e="T215" id="Seg_3273" s="T214">пустить-PST-3PL</ta>
            <ta e="T216" id="Seg_3274" s="T215">Усть_Озёрное.[NOM]</ta>
            <ta e="T217" id="Seg_3275" s="T216">деревня-ILL</ta>
            <ta e="T218" id="Seg_3276" s="T217">деревня-LOC</ta>
            <ta e="T219" id="Seg_3277" s="T218">работать-PST-1SG.S</ta>
            <ta e="T220" id="Seg_3278" s="T219">клуб-LOC</ta>
            <ta e="T221" id="Seg_3279" s="T220">потом</ta>
            <ta e="T222" id="Seg_3280" s="T221">рыба-CAP.ADJZ</ta>
            <ta e="T223" id="Seg_3281" s="T222">артель-LOC</ta>
            <ta e="T225" id="Seg_3282" s="T224">потом</ta>
            <ta e="T226" id="Seg_3283" s="T225">сельсовет-LOC</ta>
            <ta e="T229" id="Seg_3284" s="T228">работать-1SG.S</ta>
            <ta e="T230" id="Seg_3285" s="T229">тут</ta>
            <ta e="T231" id="Seg_3286" s="T230">я.NOM</ta>
            <ta e="T232" id="Seg_3287" s="T231">жениться-PST.NAR-1SG.S</ta>
            <ta e="T233" id="Seg_3288" s="T232">я.NOM</ta>
            <ta e="T234" id="Seg_3289" s="T233">старуха-GEN-OBL.1SG-COM</ta>
            <ta e="T235" id="Seg_3290" s="T234">жить-1SG.S</ta>
            <ta e="T236" id="Seg_3291" s="T235">восемь-излишний-десять</ta>
            <ta e="T237" id="Seg_3292" s="T236">год-3SG</ta>
            <ta e="T238" id="Seg_3293" s="T237">ребёнок</ta>
            <ta e="T239" id="Seg_3294" s="T238">%%</ta>
            <ta e="T240" id="Seg_3295" s="T239">быть-PST.NAR-3SG.S</ta>
            <ta e="T241" id="Seg_3296" s="T240">пять</ta>
            <ta e="T242" id="Seg_3297" s="T241">сын-1SG</ta>
            <ta e="T243" id="Seg_3298" s="T242">и</ta>
            <ta e="T244" id="Seg_3299" s="T243">два</ta>
            <ta e="T245" id="Seg_3300" s="T244">дочь-1SG</ta>
            <ta e="T246" id="Seg_3301" s="T245">старуха.[NOM]-1SG</ta>
            <ta e="T247" id="Seg_3302" s="T246">русский</ta>
            <ta e="T248" id="Seg_3303" s="T247">ребёнок-PL-1SG</ta>
            <ta e="T319" id="Seg_3304" s="T248">селькупский</ta>
            <ta e="T249" id="Seg_3305" s="T319">по</ta>
            <ta e="T250" id="Seg_3306" s="T249">NEG</ta>
            <ta e="T251" id="Seg_3307" s="T250">говорить-EP-3PL</ta>
            <ta e="T252" id="Seg_3308" s="T251">большой</ta>
            <ta e="T253" id="Seg_3309" s="T252">сын-1SG</ta>
            <ta e="T254" id="Seg_3310" s="T253">делать-PFV-PST.NAR-3SG.O</ta>
            <ta e="T255" id="Seg_3311" s="T254">этот</ta>
            <ta e="T256" id="Seg_3312" s="T255">год-ADV.LOC</ta>
            <ta e="T257" id="Seg_3313" s="T256">десять</ta>
            <ta e="T258" id="Seg_3314" s="T257">класс-EP-ACC</ta>
            <ta e="T259" id="Seg_3315" s="T258">маленький</ta>
            <ta e="T260" id="Seg_3316" s="T259">сын-ILL.3SG</ta>
            <ta e="T261" id="Seg_3317" s="T260">пять</ta>
            <ta e="T262" id="Seg_3318" s="T261">шесть-ORD</ta>
            <ta e="T263" id="Seg_3319" s="T262">год-3SG</ta>
            <ta e="T264" id="Seg_3320" s="T263">один</ta>
            <ta e="T265" id="Seg_3321" s="T264">сын-1SG</ta>
            <ta e="T266" id="Seg_3322" s="T265">сильно</ta>
            <ta e="T267" id="Seg_3323" s="T266">болеть-PST.[3SG.S]</ta>
            <ta e="T268" id="Seg_3324" s="T267">он.NOM-EP-ACC</ta>
            <ta e="T269" id="Seg_3325" s="T268">уйти-TR-HAB-PST-3PL</ta>
            <ta e="T270" id="Seg_3326" s="T269">Томск-ILL</ta>
            <ta e="T271" id="Seg_3327" s="T270">он.NOM-EP-ACC</ta>
            <ta e="T272" id="Seg_3328" s="T271">там</ta>
            <ta e="T273" id="Seg_3329" s="T272">хороший-TRL-TR-3PL</ta>
            <ta e="T274" id="Seg_3330" s="T273">принести-PST.NAR-3PL</ta>
            <ta e="T275" id="Seg_3331" s="T274">назад</ta>
            <ta e="T276" id="Seg_3332" s="T275">я.GEN</ta>
            <ta e="T277" id="Seg_3333" s="T276">ребёнок-PL-1SG</ta>
            <ta e="T278" id="Seg_3334" s="T277">селькуп-PL</ta>
            <ta e="T279" id="Seg_3335" s="T278">он.NOM-EP-PL</ta>
            <ta e="T280" id="Seg_3336" s="T279">учиться-PFV-HAB-CO-3PL</ta>
            <ta e="T281" id="Seg_3337" s="T280">деньги-CAR-ADVZ</ta>
            <ta e="T282" id="Seg_3338" s="T281">сам.1SG</ta>
            <ta e="T283" id="Seg_3339" s="T282">я.NOM</ta>
            <ta e="T284" id="Seg_3340" s="T283">работать-DRV-CO-1SG.O</ta>
            <ta e="T322" id="Seg_3341" s="T284">сорок</ta>
            <ta e="T285" id="Seg_3342" s="T322">связь</ta>
            <ta e="T286" id="Seg_3343" s="T285">пять</ta>
            <ta e="T287" id="Seg_3344" s="T286">излишний-десять</ta>
            <ta e="T288" id="Seg_3345" s="T287">рубль-ACC</ta>
            <ta e="T289" id="Seg_3346" s="T288">месяц-LOC.3SG</ta>
            <ta e="T290" id="Seg_3347" s="T289">жить-1SG.S</ta>
            <ta e="T291" id="Seg_3348" s="T290">я.NOM</ta>
            <ta e="T292" id="Seg_3349" s="T291">хороший-ADVZ</ta>
            <ta e="T293" id="Seg_3350" s="T292">осень-ADV.LOC</ta>
            <ta e="T294" id="Seg_3351" s="T293">добывать.орех-HAB-CO-1SG.S</ta>
            <ta e="T295" id="Seg_3352" s="T294">глухарь-CAP-HAB-CO-1SG.S</ta>
            <ta e="T296" id="Seg_3353" s="T295">ягода-EP-CAP-HAB-CO-1SG.S</ta>
            <ta e="T297" id="Seg_3354" s="T296">зверь-CAP-EP-HAB-CO-1SG.S</ta>
            <ta e="T298" id="Seg_3355" s="T297">лето-EP-ADV.LOC</ta>
            <ta e="T299" id="Seg_3356" s="T298">сеть-FRQ-EP-HAB-CO-1SG.S</ta>
            <ta e="T300" id="Seg_3357" s="T299">рыба-CAP-HAB-CO-1SG.S</ta>
            <ta e="T301" id="Seg_3358" s="T300">сейчас</ta>
            <ta e="T302" id="Seg_3359" s="T301">хотеть-1SG.S</ta>
            <ta e="T303" id="Seg_3360" s="T302">купить-EP-INF</ta>
            <ta e="T304" id="Seg_3361" s="T303">новый</ta>
            <ta e="T305" id="Seg_3362" s="T304">дом-EP-ACC</ta>
            <ta e="T306" id="Seg_3363" s="T305">сам.1SG</ta>
            <ta e="T307" id="Seg_3364" s="T306">дом-1SG</ta>
            <ta e="T308" id="Seg_3365" s="T307">сгнить-PST.NAR.[3SG.S]</ta>
            <ta e="T309" id="Seg_3366" s="T308">новый</ta>
            <ta e="T310" id="Seg_3367" s="T309">дом-EP-ACC</ta>
            <ta e="T311" id="Seg_3368" s="T310">купить-EP-INF</ta>
            <ta e="T312" id="Seg_3369" s="T311">хотеть-1SG.S</ta>
            <ta e="T323" id="Seg_3370" s="T312">шесть</ta>
            <ta e="T313" id="Seg_3371" s="T323">связь</ta>
            <ta e="T314" id="Seg_3372" s="T313">рубль-ILL</ta>
            <ta e="T315" id="Seg_3373" s="T314">я.NOM</ta>
            <ta e="T316" id="Seg_3374" s="T315">он.NOM-EP-ACC</ta>
            <ta e="T317" id="Seg_3375" s="T316">купить-EP-FUT-1SG.O</ta>
            <ta e="T318" id="Seg_3376" s="T317">жить-FUT-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_3377" s="T0">n-n:ins-n&gt;v-v:tense-v:pn</ta>
            <ta e="T2" id="Seg_3378" s="T1">num</ta>
            <ta e="T3" id="Seg_3379" s="T2">adj-num</ta>
            <ta e="T4" id="Seg_3380" s="T3">n-n:case.poss</ta>
            <ta e="T324" id="Seg_3381" s="T4">num</ta>
            <ta e="T5" id="Seg_3382" s="T324">n</ta>
            <ta e="T6" id="Seg_3383" s="T5">num</ta>
            <ta e="T7" id="Seg_3384" s="T6">adj-num</ta>
            <ta e="T8" id="Seg_3385" s="T7">n-n:case.poss</ta>
            <ta e="T9" id="Seg_3386" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_3387" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_3388" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_3389" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_3390" s="T12">n-n:poss</ta>
            <ta e="T14" id="Seg_3391" s="T13">nprop-n:case</ta>
            <ta e="T15" id="Seg_3392" s="T14">n-n&gt;v-v:inf</ta>
            <ta e="T16" id="Seg_3393" s="T15">pers</ta>
            <ta e="T17" id="Seg_3394" s="T16">nprop-n:case</ta>
            <ta e="T18" id="Seg_3395" s="T17">nprop-n:case</ta>
            <ta e="T19" id="Seg_3396" s="T18">emphpro</ta>
            <ta e="T20" id="Seg_3397" s="T19">n-n:num-n:poss</ta>
            <ta e="T21" id="Seg_3398" s="T20">adj-n:case</ta>
            <ta e="T22" id="Seg_3399" s="T21">adv</ta>
            <ta e="T23" id="Seg_3400" s="T22">n-n&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_3401" s="T23">conj</ta>
            <ta e="T25" id="Seg_3402" s="T24">n-n&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_3403" s="T25">pers</ta>
            <ta e="T27" id="Seg_3404" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_3405" s="T27">num</ta>
            <ta e="T29" id="Seg_3406" s="T28">n-n:poss</ta>
            <ta e="T30" id="Seg_3407" s="T29">pers</ta>
            <ta e="T31" id="Seg_3408" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_3409" s="T31">n-n:poss-n:case</ta>
            <ta e="T33" id="Seg_3410" s="T32">n-n:poss</ta>
            <ta e="T34" id="Seg_3411" s="T33">v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_3412" s="T34">n-n:poss-n:case</ta>
            <ta e="T36" id="Seg_3413" s="T35">v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_3414" s="T36">n-n:num-n:poss</ta>
            <ta e="T38" id="Seg_3415" s="T37">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_3416" s="T38">num</ta>
            <ta e="T40" id="Seg_3417" s="T39">pers</ta>
            <ta e="T41" id="Seg_3418" s="T40">n-n:poss-n:case</ta>
            <ta e="T42" id="Seg_3419" s="T41">n-n:poss-n:case</ta>
            <ta e="T43" id="Seg_3420" s="T42">v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_3421" s="T43">n-n:poss</ta>
            <ta e="T45" id="Seg_3422" s="T44">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_3423" s="T45">n-n&gt;v-v:inf</ta>
            <ta e="T47" id="Seg_3424" s="T46">n-n&gt;v-v:inf</ta>
            <ta e="T48" id="Seg_3425" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_3426" s="T48">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_3427" s="T49">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_3428" s="T50">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_3429" s="T51">n-n:poss-n:case</ta>
            <ta e="T53" id="Seg_3430" s="T52">pers-v:tense</ta>
            <ta e="T54" id="Seg_3431" s="T53">pers</ta>
            <ta e="T55" id="Seg_3432" s="T54">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_3433" s="T55">n-adv:case</ta>
            <ta e="T57" id="Seg_3434" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_3435" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_3436" s="T58">n-n:ins-adv:case</ta>
            <ta e="T60" id="Seg_3437" s="T59">n-n&gt;v-v&gt;adv</ta>
            <ta e="T61" id="Seg_3438" s="T60">conj</ta>
            <ta e="T62" id="Seg_3439" s="T61">v-v&gt;v-v&gt;adv</ta>
            <ta e="T63" id="Seg_3440" s="T62">v-v&gt;v-v:inf</ta>
            <ta e="T64" id="Seg_3441" s="T63">adv-adj&gt;adv</ta>
            <ta e="T65" id="Seg_3442" s="T64">pers</ta>
            <ta e="T66" id="Seg_3443" s="T65">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_3444" s="T66">n-n:poss</ta>
            <ta e="T68" id="Seg_3445" s="T67">pers</ta>
            <ta e="T69" id="Seg_3446" s="T68">adj</ta>
            <ta e="T70" id="Seg_3447" s="T69">v-v&gt;v-v:tense</ta>
            <ta e="T71" id="Seg_3448" s="T70">v-v&gt;v</ta>
            <ta e="T72" id="Seg_3449" s="T71">pers</ta>
            <ta e="T73" id="Seg_3450" s="T72">n-n:poss</ta>
            <ta e="T74" id="Seg_3451" s="T73">v-v:mood-v:pn</ta>
            <ta e="T75" id="Seg_3452" s="T74">n-n:poss</ta>
            <ta e="T76" id="Seg_3453" s="T75">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T77" id="Seg_3454" s="T76">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_3455" s="T77">pers-n:case</ta>
            <ta e="T79" id="Seg_3456" s="T78">pers</ta>
            <ta e="T80" id="Seg_3457" s="T79">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_3458" s="T80">interrog</ta>
            <ta e="T82" id="Seg_3459" s="T81">pers</ta>
            <ta e="T83" id="Seg_3460" s="T82">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T84" id="Seg_3461" s="T83">pers-n:case</ta>
            <ta e="T85" id="Seg_3462" s="T84">pers</ta>
            <ta e="T86" id="Seg_3463" s="T85">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_3464" s="T86">pers</ta>
            <ta e="T88" id="Seg_3465" s="T87">v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_3466" s="T88">n-n:poss</ta>
            <ta e="T90" id="Seg_3467" s="T89">n-n:poss</ta>
            <ta e="T91" id="Seg_3468" s="T90">pers-n:ins-n:case</ta>
            <ta e="T92" id="Seg_3469" s="T91">n-n:case</ta>
            <ta e="T93" id="Seg_3470" s="T92">pers</ta>
            <ta e="T94" id="Seg_3471" s="T93">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T95" id="Seg_3472" s="T94">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T96" id="Seg_3473" s="T95">v-v:inf</ta>
            <ta e="T97" id="Seg_3474" s="T96">n-adv:case</ta>
            <ta e="T98" id="Seg_3475" s="T97">n-n:case</ta>
            <ta e="T99" id="Seg_3476" s="T98">n-n:ins-n&gt;v-v:inf</ta>
            <ta e="T100" id="Seg_3477" s="T99">n-n&gt;v-v:inf</ta>
            <ta e="T101" id="Seg_3478" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_3479" s="T101">n-n:case</ta>
            <ta e="T103" id="Seg_3480" s="T102">v-v&gt;v-v:inf</ta>
            <ta e="T104" id="Seg_3481" s="T103">n-n:case-n-n:case</ta>
            <ta e="T105" id="Seg_3482" s="T104">v-v:inf</ta>
            <ta e="T106" id="Seg_3483" s="T105">n-n:case</ta>
            <ta e="T107" id="Seg_3484" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_3485" s="T107">pers-n:ins-n:case</ta>
            <ta e="T109" id="Seg_3486" s="T108">v-v:tense-v:pn</ta>
            <ta e="T110" id="Seg_3487" s="T109">pers</ta>
            <ta e="T111" id="Seg_3488" s="T110">v-v:tense-v:pn</ta>
            <ta e="T112" id="Seg_3489" s="T111">n-n:poss</ta>
            <ta e="T113" id="Seg_3490" s="T112">pers</ta>
            <ta e="T114" id="Seg_3491" s="T113">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T115" id="Seg_3492" s="T114">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T116" id="Seg_3493" s="T115">n-n:case</ta>
            <ta e="T117" id="Seg_3494" s="T116">pers</ta>
            <ta e="T118" id="Seg_3495" s="T117">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T119" id="Seg_3496" s="T118">pers</ta>
            <ta e="T120" id="Seg_3497" s="T119">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T121" id="Seg_3498" s="T120">adv</ta>
            <ta e="T122" id="Seg_3499" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_3500" s="T122">v-v:tense-v:pn</ta>
            <ta e="T124" id="Seg_3501" s="T123">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T125" id="Seg_3502" s="T124">num</ta>
            <ta e="T126" id="Seg_3503" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_3504" s="T126">n-n:case</ta>
            <ta e="T128" id="Seg_3505" s="T127">n-n:poss</ta>
            <ta e="T129" id="Seg_3506" s="T128">pers-n:ins-n:case</ta>
            <ta e="T130" id="Seg_3507" s="T129">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T131" id="Seg_3508" s="T130">nprop-n:case</ta>
            <ta e="T132" id="Seg_3509" s="T131">adv</ta>
            <ta e="T133" id="Seg_3510" s="T132">dem</ta>
            <ta e="T134" id="Seg_3511" s="T133">nprop-n:case</ta>
            <ta e="T135" id="Seg_3512" s="T134">n-n:poss</ta>
            <ta e="T136" id="Seg_3513" s="T135">adj</ta>
            <ta e="T137" id="Seg_3514" s="T136">n</ta>
            <ta e="T138" id="Seg_3515" s="T137">nprop-n&gt;adj</ta>
            <ta e="T139" id="Seg_3516" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_3517" s="T139">num</ta>
            <ta e="T141" id="Seg_3518" s="T140">n-n:ins-n:case</ta>
            <ta e="T142" id="Seg_3519" s="T141">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T143" id="Seg_3520" s="T142">nprop</ta>
            <ta e="T144" id="Seg_3521" s="T143">n-n:case</ta>
            <ta e="T145" id="Seg_3522" s="T144">nprop</ta>
            <ta e="T146" id="Seg_3523" s="T145">n-n:case.poss</ta>
            <ta e="T147" id="Seg_3524" s="T146">v-v:tense-v:pn</ta>
            <ta e="T148" id="Seg_3525" s="T147">v-v&gt;v-v&gt;v-v:inf</ta>
            <ta e="T149" id="Seg_3526" s="T148">adj</ta>
            <ta e="T150" id="Seg_3527" s="T149">n-n:case</ta>
            <ta e="T151" id="Seg_3528" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_3529" s="T151">n-n:poss</ta>
            <ta e="T153" id="Seg_3530" s="T152">nprop</ta>
            <ta e="T154" id="Seg_3531" s="T153">adv</ta>
            <ta e="T155" id="Seg_3532" s="T154">pers</ta>
            <ta e="T156" id="Seg_3533" s="T155">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T157" id="Seg_3534" s="T156">num</ta>
            <ta e="T158" id="Seg_3535" s="T157">n-n:ins-n:case</ta>
            <ta e="T159" id="Seg_3536" s="T158">adv</ta>
            <ta e="T160" id="Seg_3537" s="T159">pers</ta>
            <ta e="T161" id="Seg_3538" s="T160">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T162" id="Seg_3539" s="T161">nprop-n:case</ta>
            <ta e="T163" id="Seg_3540" s="T162">n-n:case</ta>
            <ta e="T164" id="Seg_3541" s="T163">n-n:case.poss</ta>
            <ta e="T165" id="Seg_3542" s="T164">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T166" id="Seg_3543" s="T165">adj</ta>
            <ta e="T167" id="Seg_3544" s="T166">n-n:case</ta>
            <ta e="T168" id="Seg_3545" s="T167">conj</ta>
            <ta e="T169" id="Seg_3546" s="T168">v-v:tense-v:pn</ta>
            <ta e="T170" id="Seg_3547" s="T169">pers</ta>
            <ta e="T171" id="Seg_3548" s="T170">num</ta>
            <ta e="T172" id="Seg_3549" s="T171">n-n:case</ta>
            <ta e="T173" id="Seg_3550" s="T172">num</ta>
            <ta e="T174" id="Seg_3551" s="T173">n-n:case</ta>
            <ta e="T320" id="Seg_3552" s="T174">num</ta>
            <ta e="T175" id="Seg_3553" s="T320">n</ta>
            <ta e="T176" id="Seg_3554" s="T175">num</ta>
            <ta e="T177" id="Seg_3555" s="T176">n-n:case.poss</ta>
            <ta e="T178" id="Seg_3556" s="T177">pers</ta>
            <ta e="T179" id="Seg_3557" s="T178">v-v:tense-v:pn</ta>
            <ta e="T180" id="Seg_3558" s="T179">adj</ta>
            <ta e="T181" id="Seg_3559" s="T180">n-n:case</ta>
            <ta e="T182" id="Seg_3560" s="T181">adv-adv:case</ta>
            <ta e="T183" id="Seg_3561" s="T182">pers</ta>
            <ta e="T184" id="Seg_3562" s="T183">v-v:tense-v:pn</ta>
            <ta e="T185" id="Seg_3563" s="T184">num</ta>
            <ta e="T186" id="Seg_3564" s="T185">n-adv:case</ta>
            <ta e="T321" id="Seg_3565" s="T186">num</ta>
            <ta e="T187" id="Seg_3566" s="T321">n</ta>
            <ta e="T188" id="Seg_3567" s="T187">num-adj</ta>
            <ta e="T189" id="Seg_3568" s="T188">n-adv:case</ta>
            <ta e="T190" id="Seg_3569" s="T189">pers</ta>
            <ta e="T191" id="Seg_3570" s="T190">v-v:pn</ta>
            <ta e="T192" id="Seg_3571" s="T191">n-n:case.poss</ta>
            <ta e="T193" id="Seg_3572" s="T192">dem</ta>
            <ta e="T194" id="Seg_3573" s="T193">n-adv:case</ta>
            <ta e="T195" id="Seg_3574" s="T194">pers</ta>
            <ta e="T196" id="Seg_3575" s="T195">v-v:tense-v:pn</ta>
            <ta e="T197" id="Seg_3576" s="T196">adj</ta>
            <ta e="T198" id="Seg_3577" s="T197">n-n:case</ta>
            <ta e="T199" id="Seg_3578" s="T198">adv</ta>
            <ta e="T201" id="Seg_3579" s="T199">dem-clit</ta>
            <ta e="T202" id="Seg_3580" s="T201">n-n&gt;adv</ta>
            <ta e="T203" id="Seg_3581" s="T202">pers</ta>
            <ta e="T204" id="Seg_3582" s="T203">v-v:tense-v:pn</ta>
            <ta e="T205" id="Seg_3583" s="T204">n-n:case.poss</ta>
            <ta e="T206" id="Seg_3584" s="T205">v-v:tense-v:pn</ta>
            <ta e="T207" id="Seg_3585" s="T206">nprop-n:case</ta>
            <ta e="T208" id="Seg_3586" s="T207">adv</ta>
            <ta e="T209" id="Seg_3587" s="T208">v-v:tense-v:pn</ta>
            <ta e="T210" id="Seg_3588" s="T209">adj</ta>
            <ta e="T211" id="Seg_3589" s="T210">n-n:case</ta>
            <ta e="T212" id="Seg_3590" s="T211">adj</ta>
            <ta e="T213" id="Seg_3591" s="T212">n-n:case.poss</ta>
            <ta e="T214" id="Seg_3592" s="T213">pers</ta>
            <ta e="T215" id="Seg_3593" s="T214">v-v:tense-v:pn</ta>
            <ta e="T216" id="Seg_3594" s="T215">n.[n:case]</ta>
            <ta e="T217" id="Seg_3595" s="T216">n-n:case</ta>
            <ta e="T218" id="Seg_3596" s="T217">n-n:case</ta>
            <ta e="T219" id="Seg_3597" s="T218">v-v:tense-v:pn</ta>
            <ta e="T220" id="Seg_3598" s="T219">n-n:case</ta>
            <ta e="T221" id="Seg_3599" s="T220">adv</ta>
            <ta e="T222" id="Seg_3600" s="T221">n-n&gt;adj</ta>
            <ta e="T223" id="Seg_3601" s="T222">n-n:case</ta>
            <ta e="T225" id="Seg_3602" s="T224">adv</ta>
            <ta e="T226" id="Seg_3603" s="T225">n-n:case</ta>
            <ta e="T229" id="Seg_3604" s="T228">v-v:pn</ta>
            <ta e="T230" id="Seg_3605" s="T229">adv</ta>
            <ta e="T231" id="Seg_3606" s="T230">pers</ta>
            <ta e="T232" id="Seg_3607" s="T231">v-v:tense-v:pn</ta>
            <ta e="T233" id="Seg_3608" s="T232">pers</ta>
            <ta e="T234" id="Seg_3609" s="T233">n-n:case-n:obl.poss-n:case</ta>
            <ta e="T235" id="Seg_3610" s="T234">v-v:pn</ta>
            <ta e="T236" id="Seg_3611" s="T235">num-adj-num</ta>
            <ta e="T237" id="Seg_3612" s="T236">n-n:poss</ta>
            <ta e="T238" id="Seg_3613" s="T237">n</ta>
            <ta e="T239" id="Seg_3614" s="T238">%%</ta>
            <ta e="T240" id="Seg_3615" s="T239">v-v:tense-v:pn</ta>
            <ta e="T241" id="Seg_3616" s="T240">num</ta>
            <ta e="T242" id="Seg_3617" s="T241">n-n:poss</ta>
            <ta e="T243" id="Seg_3618" s="T242">conj</ta>
            <ta e="T244" id="Seg_3619" s="T243">num</ta>
            <ta e="T245" id="Seg_3620" s="T244">n-n:poss</ta>
            <ta e="T246" id="Seg_3621" s="T245">n-n:case-n:poss</ta>
            <ta e="T247" id="Seg_3622" s="T246">n</ta>
            <ta e="T248" id="Seg_3623" s="T247">n-n:num-n:poss</ta>
            <ta e="T319" id="Seg_3624" s="T248">adj</ta>
            <ta e="T249" id="Seg_3625" s="T319">pp</ta>
            <ta e="T250" id="Seg_3626" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_3627" s="T250">v-n:ins-v:pn</ta>
            <ta e="T252" id="Seg_3628" s="T251">adj</ta>
            <ta e="T253" id="Seg_3629" s="T252">n-n:poss</ta>
            <ta e="T254" id="Seg_3630" s="T253">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T255" id="Seg_3631" s="T254">dem</ta>
            <ta e="T256" id="Seg_3632" s="T255">n-adv:case</ta>
            <ta e="T257" id="Seg_3633" s="T256">num</ta>
            <ta e="T258" id="Seg_3634" s="T257">n-n:ins-n:case</ta>
            <ta e="T259" id="Seg_3635" s="T258">adj</ta>
            <ta e="T260" id="Seg_3636" s="T259">n-n:case.poss</ta>
            <ta e="T261" id="Seg_3637" s="T260">num</ta>
            <ta e="T262" id="Seg_3638" s="T261">num-num&gt;adj</ta>
            <ta e="T263" id="Seg_3639" s="T262">n-n:poss</ta>
            <ta e="T264" id="Seg_3640" s="T263">num</ta>
            <ta e="T265" id="Seg_3641" s="T264">n-n:poss</ta>
            <ta e="T266" id="Seg_3642" s="T265">adv</ta>
            <ta e="T267" id="Seg_3643" s="T266">v-v:tense-v:pn</ta>
            <ta e="T268" id="Seg_3644" s="T267">pers-n:ins-n:case</ta>
            <ta e="T269" id="Seg_3645" s="T268">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T270" id="Seg_3646" s="T269">nprop-n:case</ta>
            <ta e="T271" id="Seg_3647" s="T270">pers-n:ins-n:case</ta>
            <ta e="T272" id="Seg_3648" s="T271">adv</ta>
            <ta e="T273" id="Seg_3649" s="T272">adj-n&gt;v-v&gt;v-v:pn</ta>
            <ta e="T274" id="Seg_3650" s="T273">v-v:tense-v:pn</ta>
            <ta e="T275" id="Seg_3651" s="T274">adv</ta>
            <ta e="T276" id="Seg_3652" s="T275">pers</ta>
            <ta e="T277" id="Seg_3653" s="T276">n-n:num-n:poss</ta>
            <ta e="T278" id="Seg_3654" s="T277">n-n:num</ta>
            <ta e="T279" id="Seg_3655" s="T278">pers-n:ins-n:num</ta>
            <ta e="T280" id="Seg_3656" s="T279">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T281" id="Seg_3657" s="T280">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T282" id="Seg_3658" s="T281">emphpro</ta>
            <ta e="T283" id="Seg_3659" s="T282">pers</ta>
            <ta e="T284" id="Seg_3660" s="T283">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T322" id="Seg_3661" s="T284">num</ta>
            <ta e="T285" id="Seg_3662" s="T322">n</ta>
            <ta e="T286" id="Seg_3663" s="T285">num</ta>
            <ta e="T287" id="Seg_3664" s="T286">adj-num</ta>
            <ta e="T288" id="Seg_3665" s="T287">n-n:case</ta>
            <ta e="T289" id="Seg_3666" s="T288">n-n:case.poss</ta>
            <ta e="T290" id="Seg_3667" s="T289">v-v:pn</ta>
            <ta e="T291" id="Seg_3668" s="T290">pers</ta>
            <ta e="T292" id="Seg_3669" s="T291">adj-adj&gt;adv</ta>
            <ta e="T293" id="Seg_3670" s="T292">n-adv:case</ta>
            <ta e="T294" id="Seg_3671" s="T293">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T295" id="Seg_3672" s="T294">n-n&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T296" id="Seg_3673" s="T295">n-n:ins-n&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T297" id="Seg_3674" s="T296">n-n&gt;v-n:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T298" id="Seg_3675" s="T297">n-n:ins-adv:case</ta>
            <ta e="T299" id="Seg_3676" s="T298">n-v&gt;v-n:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T300" id="Seg_3677" s="T299">n-n&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T301" id="Seg_3678" s="T300">adv</ta>
            <ta e="T302" id="Seg_3679" s="T301">v-v:pn</ta>
            <ta e="T303" id="Seg_3680" s="T302">v-n:ins-v:inf</ta>
            <ta e="T304" id="Seg_3681" s="T303">adj</ta>
            <ta e="T305" id="Seg_3682" s="T304">n-n:ins-n:case</ta>
            <ta e="T306" id="Seg_3683" s="T305">emphpro</ta>
            <ta e="T307" id="Seg_3684" s="T306">n-n:poss</ta>
            <ta e="T308" id="Seg_3685" s="T307">v-v:tense-v:pn</ta>
            <ta e="T309" id="Seg_3686" s="T308">adj</ta>
            <ta e="T310" id="Seg_3687" s="T309">n-n:ins-n:case</ta>
            <ta e="T311" id="Seg_3688" s="T310">v-n:ins-v:inf</ta>
            <ta e="T312" id="Seg_3689" s="T311">v-v:pn</ta>
            <ta e="T323" id="Seg_3690" s="T312">num</ta>
            <ta e="T313" id="Seg_3691" s="T323">n</ta>
            <ta e="T314" id="Seg_3692" s="T313">n-n:case</ta>
            <ta e="T315" id="Seg_3693" s="T314">pers</ta>
            <ta e="T316" id="Seg_3694" s="T315">pers-n:ins-n:case</ta>
            <ta e="T317" id="Seg_3695" s="T316">v-n:ins-v:tense-v:pn</ta>
            <ta e="T318" id="Seg_3696" s="T317">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_3697" s="T0">v</ta>
            <ta e="T2" id="Seg_3698" s="T1">num</ta>
            <ta e="T3" id="Seg_3699" s="T2">num</ta>
            <ta e="T4" id="Seg_3700" s="T3">n</ta>
            <ta e="T324" id="Seg_3701" s="T4">num</ta>
            <ta e="T5" id="Seg_3702" s="T324">n</ta>
            <ta e="T6" id="Seg_3703" s="T5">num</ta>
            <ta e="T7" id="Seg_3704" s="T6">num</ta>
            <ta e="T8" id="Seg_3705" s="T7">n</ta>
            <ta e="T9" id="Seg_3706" s="T8">n</ta>
            <ta e="T10" id="Seg_3707" s="T9">n</ta>
            <ta e="T11" id="Seg_3708" s="T10">n</ta>
            <ta e="T12" id="Seg_3709" s="T11">v</ta>
            <ta e="T13" id="Seg_3710" s="T12">n</ta>
            <ta e="T14" id="Seg_3711" s="T13">nprop</ta>
            <ta e="T15" id="Seg_3712" s="T14">v</ta>
            <ta e="T16" id="Seg_3713" s="T15">pers</ta>
            <ta e="T17" id="Seg_3714" s="T16">nprop</ta>
            <ta e="T18" id="Seg_3715" s="T17">nprop</ta>
            <ta e="T19" id="Seg_3716" s="T18">emphpro</ta>
            <ta e="T20" id="Seg_3717" s="T19">n</ta>
            <ta e="T21" id="Seg_3718" s="T20">adv</ta>
            <ta e="T22" id="Seg_3719" s="T21">adv</ta>
            <ta e="T23" id="Seg_3720" s="T22">v</ta>
            <ta e="T24" id="Seg_3721" s="T23">conj</ta>
            <ta e="T25" id="Seg_3722" s="T24">v</ta>
            <ta e="T26" id="Seg_3723" s="T25">pers</ta>
            <ta e="T27" id="Seg_3724" s="T26">v</ta>
            <ta e="T28" id="Seg_3725" s="T27">num</ta>
            <ta e="T29" id="Seg_3726" s="T28">n</ta>
            <ta e="T30" id="Seg_3727" s="T29">pers</ta>
            <ta e="T31" id="Seg_3728" s="T30">v</ta>
            <ta e="T32" id="Seg_3729" s="T31">n</ta>
            <ta e="T33" id="Seg_3730" s="T32">n</ta>
            <ta e="T34" id="Seg_3731" s="T33">v</ta>
            <ta e="T35" id="Seg_3732" s="T34">n</ta>
            <ta e="T36" id="Seg_3733" s="T35">v</ta>
            <ta e="T37" id="Seg_3734" s="T36">n</ta>
            <ta e="T38" id="Seg_3735" s="T37">v</ta>
            <ta e="T39" id="Seg_3736" s="T38">num</ta>
            <ta e="T40" id="Seg_3737" s="T39">pers</ta>
            <ta e="T41" id="Seg_3738" s="T40">n</ta>
            <ta e="T42" id="Seg_3739" s="T41">n</ta>
            <ta e="T43" id="Seg_3740" s="T42">v</ta>
            <ta e="T44" id="Seg_3741" s="T43">n</ta>
            <ta e="T45" id="Seg_3742" s="T44">v</ta>
            <ta e="T46" id="Seg_3743" s="T45">v</ta>
            <ta e="T47" id="Seg_3744" s="T46">v</ta>
            <ta e="T48" id="Seg_3745" s="T47">n</ta>
            <ta e="T49" id="Seg_3746" s="T48">v</ta>
            <ta e="T50" id="Seg_3747" s="T49">v</ta>
            <ta e="T51" id="Seg_3748" s="T50">v</ta>
            <ta e="T52" id="Seg_3749" s="T51">n</ta>
            <ta e="T53" id="Seg_3750" s="T52">pers</ta>
            <ta e="T54" id="Seg_3751" s="T53">pers</ta>
            <ta e="T55" id="Seg_3752" s="T54">v</ta>
            <ta e="T56" id="Seg_3753" s="T55">adv</ta>
            <ta e="T57" id="Seg_3754" s="T56">n</ta>
            <ta e="T58" id="Seg_3755" s="T57">n</ta>
            <ta e="T59" id="Seg_3756" s="T58">adv</ta>
            <ta e="T60" id="Seg_3757" s="T59">v</ta>
            <ta e="T61" id="Seg_3758" s="T60">conj</ta>
            <ta e="T62" id="Seg_3759" s="T61">v</ta>
            <ta e="T63" id="Seg_3760" s="T62">v</ta>
            <ta e="T64" id="Seg_3761" s="T63">adv</ta>
            <ta e="T65" id="Seg_3762" s="T64">pers</ta>
            <ta e="T66" id="Seg_3763" s="T65">v</ta>
            <ta e="T67" id="Seg_3764" s="T66">n</ta>
            <ta e="T68" id="Seg_3765" s="T67">pers</ta>
            <ta e="T69" id="Seg_3766" s="T68">adj</ta>
            <ta e="T70" id="Seg_3767" s="T69">v</ta>
            <ta e="T71" id="Seg_3768" s="T70">v</ta>
            <ta e="T72" id="Seg_3769" s="T71">pers</ta>
            <ta e="T73" id="Seg_3770" s="T72">n</ta>
            <ta e="T74" id="Seg_3771" s="T73">v</ta>
            <ta e="T75" id="Seg_3772" s="T74">n</ta>
            <ta e="T76" id="Seg_3773" s="T75">v</ta>
            <ta e="T77" id="Seg_3774" s="T76">v</ta>
            <ta e="T78" id="Seg_3775" s="T77">pers</ta>
            <ta e="T79" id="Seg_3776" s="T78">pers</ta>
            <ta e="T80" id="Seg_3777" s="T79">v</ta>
            <ta e="T81" id="Seg_3778" s="T80">interrog</ta>
            <ta e="T82" id="Seg_3779" s="T81">pers</ta>
            <ta e="T83" id="Seg_3780" s="T82">v</ta>
            <ta e="T84" id="Seg_3781" s="T83">pers</ta>
            <ta e="T85" id="Seg_3782" s="T84">pers</ta>
            <ta e="T86" id="Seg_3783" s="T85">v</ta>
            <ta e="T87" id="Seg_3784" s="T86">pers</ta>
            <ta e="T88" id="Seg_3785" s="T87">v</ta>
            <ta e="T89" id="Seg_3786" s="T88">n</ta>
            <ta e="T90" id="Seg_3787" s="T89">n</ta>
            <ta e="T91" id="Seg_3788" s="T90">pers</ta>
            <ta e="T92" id="Seg_3789" s="T91">n</ta>
            <ta e="T93" id="Seg_3790" s="T92">pers</ta>
            <ta e="T94" id="Seg_3791" s="T93">v</ta>
            <ta e="T95" id="Seg_3792" s="T94">v</ta>
            <ta e="T96" id="Seg_3793" s="T95">v</ta>
            <ta e="T97" id="Seg_3794" s="T96">n</ta>
            <ta e="T98" id="Seg_3795" s="T97">n</ta>
            <ta e="T99" id="Seg_3796" s="T98">v</ta>
            <ta e="T100" id="Seg_3797" s="T99">v</ta>
            <ta e="T101" id="Seg_3798" s="T100">n</ta>
            <ta e="T102" id="Seg_3799" s="T101">n</ta>
            <ta e="T103" id="Seg_3800" s="T102">v</ta>
            <ta e="T104" id="Seg_3801" s="T103">n</ta>
            <ta e="T105" id="Seg_3802" s="T104">v</ta>
            <ta e="T106" id="Seg_3803" s="T105">n</ta>
            <ta e="T107" id="Seg_3804" s="T106">n</ta>
            <ta e="T108" id="Seg_3805" s="T107">pers</ta>
            <ta e="T109" id="Seg_3806" s="T108">v</ta>
            <ta e="T110" id="Seg_3807" s="T109">pers</ta>
            <ta e="T111" id="Seg_3808" s="T110">v</ta>
            <ta e="T112" id="Seg_3809" s="T111">n</ta>
            <ta e="T113" id="Seg_3810" s="T112">pers</ta>
            <ta e="T114" id="Seg_3811" s="T113">v</ta>
            <ta e="T115" id="Seg_3812" s="T114">v</ta>
            <ta e="T116" id="Seg_3813" s="T115">n</ta>
            <ta e="T117" id="Seg_3814" s="T116">pers</ta>
            <ta e="T118" id="Seg_3815" s="T117">v</ta>
            <ta e="T119" id="Seg_3816" s="T118">pers</ta>
            <ta e="T120" id="Seg_3817" s="T119">v</ta>
            <ta e="T121" id="Seg_3818" s="T120">adv</ta>
            <ta e="T122" id="Seg_3819" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_3820" s="T122">v</ta>
            <ta e="T124" id="Seg_3821" s="T123">v</ta>
            <ta e="T125" id="Seg_3822" s="T124">num</ta>
            <ta e="T126" id="Seg_3823" s="T125">n</ta>
            <ta e="T127" id="Seg_3824" s="T126">n</ta>
            <ta e="T128" id="Seg_3825" s="T127">n</ta>
            <ta e="T129" id="Seg_3826" s="T128">pers</ta>
            <ta e="T130" id="Seg_3827" s="T129">v</ta>
            <ta e="T131" id="Seg_3828" s="T130">nprop</ta>
            <ta e="T132" id="Seg_3829" s="T131">adv</ta>
            <ta e="T133" id="Seg_3830" s="T132">dem</ta>
            <ta e="T134" id="Seg_3831" s="T133">nprop</ta>
            <ta e="T135" id="Seg_3832" s="T134">n</ta>
            <ta e="T136" id="Seg_3833" s="T135">adj</ta>
            <ta e="T137" id="Seg_3834" s="T136">n</ta>
            <ta e="T138" id="Seg_3835" s="T137">adj</ta>
            <ta e="T139" id="Seg_3836" s="T138">n</ta>
            <ta e="T140" id="Seg_3837" s="T139">num</ta>
            <ta e="T141" id="Seg_3838" s="T140">n</ta>
            <ta e="T142" id="Seg_3839" s="T141">v</ta>
            <ta e="T143" id="Seg_3840" s="T142">nprop</ta>
            <ta e="T144" id="Seg_3841" s="T143">n</ta>
            <ta e="T145" id="Seg_3842" s="T144">nprop</ta>
            <ta e="T146" id="Seg_3843" s="T145">n</ta>
            <ta e="T147" id="Seg_3844" s="T146">v</ta>
            <ta e="T148" id="Seg_3845" s="T147">v</ta>
            <ta e="T149" id="Seg_3846" s="T148">adj</ta>
            <ta e="T150" id="Seg_3847" s="T149">n</ta>
            <ta e="T151" id="Seg_3848" s="T150">n</ta>
            <ta e="T152" id="Seg_3849" s="T151">n</ta>
            <ta e="T153" id="Seg_3850" s="T152">nprop</ta>
            <ta e="T154" id="Seg_3851" s="T153">adv</ta>
            <ta e="T155" id="Seg_3852" s="T154">pers</ta>
            <ta e="T156" id="Seg_3853" s="T155">v</ta>
            <ta e="T157" id="Seg_3854" s="T156">num</ta>
            <ta e="T158" id="Seg_3855" s="T157">n</ta>
            <ta e="T159" id="Seg_3856" s="T158">adv</ta>
            <ta e="T160" id="Seg_3857" s="T159">pers</ta>
            <ta e="T161" id="Seg_3858" s="T160">v</ta>
            <ta e="T162" id="Seg_3859" s="T161">nprop</ta>
            <ta e="T163" id="Seg_3860" s="T162">n</ta>
            <ta e="T164" id="Seg_3861" s="T163">n</ta>
            <ta e="T165" id="Seg_3862" s="T164">v</ta>
            <ta e="T166" id="Seg_3863" s="T165">adj</ta>
            <ta e="T167" id="Seg_3864" s="T166">n</ta>
            <ta e="T168" id="Seg_3865" s="T167">conj</ta>
            <ta e="T169" id="Seg_3866" s="T168">v</ta>
            <ta e="T170" id="Seg_3867" s="T169">pers</ta>
            <ta e="T171" id="Seg_3868" s="T170">num</ta>
            <ta e="T172" id="Seg_3869" s="T171">n</ta>
            <ta e="T173" id="Seg_3870" s="T172">num</ta>
            <ta e="T174" id="Seg_3871" s="T173">n</ta>
            <ta e="T320" id="Seg_3872" s="T174">num</ta>
            <ta e="T175" id="Seg_3873" s="T320">n</ta>
            <ta e="T176" id="Seg_3874" s="T175">num</ta>
            <ta e="T177" id="Seg_3875" s="T176">n</ta>
            <ta e="T178" id="Seg_3876" s="T177">pers</ta>
            <ta e="T179" id="Seg_3877" s="T178">v</ta>
            <ta e="T180" id="Seg_3878" s="T179">adj</ta>
            <ta e="T181" id="Seg_3879" s="T180">n</ta>
            <ta e="T182" id="Seg_3880" s="T181">adv</ta>
            <ta e="T183" id="Seg_3881" s="T182">pers</ta>
            <ta e="T184" id="Seg_3882" s="T183">v</ta>
            <ta e="T185" id="Seg_3883" s="T184">num</ta>
            <ta e="T186" id="Seg_3884" s="T185">n</ta>
            <ta e="T321" id="Seg_3885" s="T186">num</ta>
            <ta e="T187" id="Seg_3886" s="T321">n</ta>
            <ta e="T188" id="Seg_3887" s="T187">num</ta>
            <ta e="T189" id="Seg_3888" s="T188">adv</ta>
            <ta e="T190" id="Seg_3889" s="T189">pers</ta>
            <ta e="T191" id="Seg_3890" s="T190">v</ta>
            <ta e="T192" id="Seg_3891" s="T191">n</ta>
            <ta e="T193" id="Seg_3892" s="T192">dem</ta>
            <ta e="T194" id="Seg_3893" s="T193">adv</ta>
            <ta e="T195" id="Seg_3894" s="T194">pers</ta>
            <ta e="T196" id="Seg_3895" s="T195">v</ta>
            <ta e="T197" id="Seg_3896" s="T196">adj</ta>
            <ta e="T198" id="Seg_3897" s="T197">n</ta>
            <ta e="T199" id="Seg_3898" s="T198">adv</ta>
            <ta e="T201" id="Seg_3899" s="T199">dem</ta>
            <ta e="T202" id="Seg_3900" s="T201">adv</ta>
            <ta e="T203" id="Seg_3901" s="T202">pers</ta>
            <ta e="T204" id="Seg_3902" s="T203">v</ta>
            <ta e="T205" id="Seg_3903" s="T204">n</ta>
            <ta e="T206" id="Seg_3904" s="T205">v</ta>
            <ta e="T207" id="Seg_3905" s="T206">nprop</ta>
            <ta e="T208" id="Seg_3906" s="T207">adv</ta>
            <ta e="T209" id="Seg_3907" s="T208">v</ta>
            <ta e="T210" id="Seg_3908" s="T209">adj</ta>
            <ta e="T211" id="Seg_3909" s="T210">n</ta>
            <ta e="T212" id="Seg_3910" s="T211">adj</ta>
            <ta e="T213" id="Seg_3911" s="T212">n</ta>
            <ta e="T214" id="Seg_3912" s="T213">pers</ta>
            <ta e="T215" id="Seg_3913" s="T214">v</ta>
            <ta e="T216" id="Seg_3914" s="T215">n</ta>
            <ta e="T217" id="Seg_3915" s="T216">n</ta>
            <ta e="T218" id="Seg_3916" s="T217">n</ta>
            <ta e="T219" id="Seg_3917" s="T218">v</ta>
            <ta e="T220" id="Seg_3918" s="T219">n</ta>
            <ta e="T221" id="Seg_3919" s="T220">adv</ta>
            <ta e="T222" id="Seg_3920" s="T221">adj</ta>
            <ta e="T223" id="Seg_3921" s="T222">n</ta>
            <ta e="T225" id="Seg_3922" s="T224">adv</ta>
            <ta e="T226" id="Seg_3923" s="T225">n</ta>
            <ta e="T229" id="Seg_3924" s="T228">v</ta>
            <ta e="T230" id="Seg_3925" s="T229">adv</ta>
            <ta e="T231" id="Seg_3926" s="T230">pers</ta>
            <ta e="T232" id="Seg_3927" s="T231">v</ta>
            <ta e="T233" id="Seg_3928" s="T232">pers</ta>
            <ta e="T234" id="Seg_3929" s="T233">n</ta>
            <ta e="T235" id="Seg_3930" s="T234">v</ta>
            <ta e="T236" id="Seg_3931" s="T235">num</ta>
            <ta e="T237" id="Seg_3932" s="T236">n</ta>
            <ta e="T238" id="Seg_3933" s="T237">n</ta>
            <ta e="T239" id="Seg_3934" s="T238">%%</ta>
            <ta e="T240" id="Seg_3935" s="T239">n</ta>
            <ta e="T241" id="Seg_3936" s="T240">num</ta>
            <ta e="T242" id="Seg_3937" s="T241">n</ta>
            <ta e="T243" id="Seg_3938" s="T242">conj</ta>
            <ta e="T244" id="Seg_3939" s="T243">num</ta>
            <ta e="T245" id="Seg_3940" s="T244">n</ta>
            <ta e="T246" id="Seg_3941" s="T245">n</ta>
            <ta e="T247" id="Seg_3942" s="T246">n</ta>
            <ta e="T248" id="Seg_3943" s="T247">n</ta>
            <ta e="T319" id="Seg_3944" s="T248">adj</ta>
            <ta e="T249" id="Seg_3945" s="T319">pp</ta>
            <ta e="T250" id="Seg_3946" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_3947" s="T250">v</ta>
            <ta e="T252" id="Seg_3948" s="T251">adj</ta>
            <ta e="T253" id="Seg_3949" s="T252">n</ta>
            <ta e="T254" id="Seg_3950" s="T253">v</ta>
            <ta e="T255" id="Seg_3951" s="T254">dem</ta>
            <ta e="T256" id="Seg_3952" s="T255">adv</ta>
            <ta e="T257" id="Seg_3953" s="T256">num</ta>
            <ta e="T258" id="Seg_3954" s="T257">n</ta>
            <ta e="T259" id="Seg_3955" s="T258">adj</ta>
            <ta e="T260" id="Seg_3956" s="T259">n</ta>
            <ta e="T261" id="Seg_3957" s="T260">num</ta>
            <ta e="T262" id="Seg_3958" s="T261">adj</ta>
            <ta e="T263" id="Seg_3959" s="T262">n</ta>
            <ta e="T264" id="Seg_3960" s="T263">num</ta>
            <ta e="T265" id="Seg_3961" s="T264">n</ta>
            <ta e="T266" id="Seg_3962" s="T265">adv</ta>
            <ta e="T267" id="Seg_3963" s="T266">v</ta>
            <ta e="T268" id="Seg_3964" s="T267">pers</ta>
            <ta e="T269" id="Seg_3965" s="T268">v</ta>
            <ta e="T270" id="Seg_3966" s="T269">nprop</ta>
            <ta e="T271" id="Seg_3967" s="T270">pers</ta>
            <ta e="T272" id="Seg_3968" s="T271">adv</ta>
            <ta e="T273" id="Seg_3969" s="T272">v</ta>
            <ta e="T274" id="Seg_3970" s="T273">v</ta>
            <ta e="T275" id="Seg_3971" s="T274">adv</ta>
            <ta e="T276" id="Seg_3972" s="T275">pers</ta>
            <ta e="T277" id="Seg_3973" s="T276">n</ta>
            <ta e="T278" id="Seg_3974" s="T277">n</ta>
            <ta e="T279" id="Seg_3975" s="T278">pers</ta>
            <ta e="T280" id="Seg_3976" s="T279">v</ta>
            <ta e="T281" id="Seg_3977" s="T280">adv</ta>
            <ta e="T282" id="Seg_3978" s="T281">emphpro</ta>
            <ta e="T283" id="Seg_3979" s="T282">pers</ta>
            <ta e="T284" id="Seg_3980" s="T283">v</ta>
            <ta e="T322" id="Seg_3981" s="T284">num</ta>
            <ta e="T285" id="Seg_3982" s="T322">n</ta>
            <ta e="T286" id="Seg_3983" s="T285">num</ta>
            <ta e="T287" id="Seg_3984" s="T286">adj</ta>
            <ta e="T288" id="Seg_3985" s="T287">n</ta>
            <ta e="T289" id="Seg_3986" s="T288">n</ta>
            <ta e="T290" id="Seg_3987" s="T289">v</ta>
            <ta e="T291" id="Seg_3988" s="T290">pers</ta>
            <ta e="T292" id="Seg_3989" s="T291">adv</ta>
            <ta e="T293" id="Seg_3990" s="T292">adv</ta>
            <ta e="T294" id="Seg_3991" s="T293">v</ta>
            <ta e="T295" id="Seg_3992" s="T294">v</ta>
            <ta e="T296" id="Seg_3993" s="T295">n</ta>
            <ta e="T297" id="Seg_3994" s="T296">v</ta>
            <ta e="T298" id="Seg_3995" s="T297">adv</ta>
            <ta e="T299" id="Seg_3996" s="T298">v</ta>
            <ta e="T300" id="Seg_3997" s="T299">v</ta>
            <ta e="T301" id="Seg_3998" s="T300">adv</ta>
            <ta e="T302" id="Seg_3999" s="T301">v</ta>
            <ta e="T303" id="Seg_4000" s="T302">v</ta>
            <ta e="T304" id="Seg_4001" s="T303">adj</ta>
            <ta e="T305" id="Seg_4002" s="T304">n</ta>
            <ta e="T306" id="Seg_4003" s="T305">emphpro</ta>
            <ta e="T307" id="Seg_4004" s="T306">n</ta>
            <ta e="T308" id="Seg_4005" s="T307">v</ta>
            <ta e="T309" id="Seg_4006" s="T308">adj</ta>
            <ta e="T310" id="Seg_4007" s="T309">n</ta>
            <ta e="T311" id="Seg_4008" s="T310">v</ta>
            <ta e="T312" id="Seg_4009" s="T311">v</ta>
            <ta e="T323" id="Seg_4010" s="T312">num</ta>
            <ta e="T313" id="Seg_4011" s="T323">n</ta>
            <ta e="T314" id="Seg_4012" s="T313">n</ta>
            <ta e="T315" id="Seg_4013" s="T314">pers</ta>
            <ta e="T316" id="Seg_4014" s="T315">pers</ta>
            <ta e="T317" id="Seg_4015" s="T316">v</ta>
            <ta e="T318" id="Seg_4016" s="T317">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_4017" s="T0">0.1.h:S v:pred</ta>
            <ta e="T13" id="Seg_4018" s="T12">np:S</ta>
            <ta e="T14" id="Seg_4019" s="T13">n:pred</ta>
            <ta e="T20" id="Seg_4020" s="T19">np.h:S</ta>
            <ta e="T23" id="Seg_4021" s="T22">v:pred</ta>
            <ta e="T25" id="Seg_4022" s="T24">0.3.h:S v:pred</ta>
            <ta e="T27" id="Seg_4023" s="T26">v:pred</ta>
            <ta e="T29" id="Seg_4024" s="T28">np:S</ta>
            <ta e="T30" id="Seg_4025" s="T29">pro.h:S</ta>
            <ta e="T31" id="Seg_4026" s="T30">v:pred</ta>
            <ta e="T33" id="Seg_4027" s="T32">np.h:S</ta>
            <ta e="T34" id="Seg_4028" s="T33">v:pred</ta>
            <ta e="T36" id="Seg_4029" s="T35">v:pred</ta>
            <ta e="T37" id="Seg_4030" s="T36">np.h:S</ta>
            <ta e="T38" id="Seg_4031" s="T37">0.3.h:S v:pred</ta>
            <ta e="T40" id="Seg_4032" s="T39">pro.h:S</ta>
            <ta e="T43" id="Seg_4033" s="T42">v:pred</ta>
            <ta e="T44" id="Seg_4034" s="T43">np.h:S</ta>
            <ta e="T45" id="Seg_4035" s="T44">v:pred</ta>
            <ta e="T46" id="Seg_4036" s="T45">s:purp</ta>
            <ta e="T47" id="Seg_4037" s="T46">s:purp</ta>
            <ta e="T49" id="Seg_4038" s="T48">0.3.h:S v:pred</ta>
            <ta e="T50" id="Seg_4039" s="T49">0.1.h:S v:pred</ta>
            <ta e="T51" id="Seg_4040" s="T50">0.1.h:S v:pred</ta>
            <ta e="T54" id="Seg_4041" s="T53">pro.h:S</ta>
            <ta e="T55" id="Seg_4042" s="T54">v:pred</ta>
            <ta e="T63" id="Seg_4043" s="T58">s:purp</ta>
            <ta e="T65" id="Seg_4044" s="T64">pro.h:S</ta>
            <ta e="T66" id="Seg_4045" s="T65">v:pred</ta>
            <ta e="T67" id="Seg_4046" s="T66">np.h:S</ta>
            <ta e="T69" id="Seg_4047" s="T68">adj:pred</ta>
            <ta e="T70" id="Seg_4048" s="T69">cop</ta>
            <ta e="T74" id="Seg_4049" s="T70">s:temp</ta>
            <ta e="T75" id="Seg_4050" s="T74">np.h:S</ta>
            <ta e="T76" id="Seg_4051" s="T75">v:pred</ta>
            <ta e="T77" id="Seg_4052" s="T76">0.3.h:S v:pred</ta>
            <ta e="T79" id="Seg_4053" s="T78">pro.h:S</ta>
            <ta e="T80" id="Seg_4054" s="T79">v:pred</ta>
            <ta e="T83" id="Seg_4055" s="T80">s:rel</ta>
            <ta e="T85" id="Seg_4056" s="T84">pro.h:S</ta>
            <ta e="T86" id="Seg_4057" s="T85">v:pred</ta>
            <ta e="T88" id="Seg_4058" s="T87">v:pred</ta>
            <ta e="T89" id="Seg_4059" s="T88">np.h:S</ta>
            <ta e="T90" id="Seg_4060" s="T89">np:S</ta>
            <ta e="T92" id="Seg_4061" s="T91">n:pred</ta>
            <ta e="T94" id="Seg_4062" s="T93">0.3.h:S v:pred</ta>
            <ta e="T95" id="Seg_4063" s="T94">0.1.h:S v:pred</ta>
            <ta e="T96" id="Seg_4064" s="T95">v:O</ta>
            <ta e="T99" id="Seg_4065" s="T98">v:O</ta>
            <ta e="T100" id="Seg_4066" s="T99">v:O</ta>
            <ta e="T103" id="Seg_4067" s="T102">v:O</ta>
            <ta e="T105" id="Seg_4068" s="T104">v:O</ta>
            <ta e="T108" id="Seg_4069" s="T107">pro.h:O</ta>
            <ta e="T109" id="Seg_4070" s="T108">0.3.h:S v:pred</ta>
            <ta e="T111" id="Seg_4071" s="T110">v:pred</ta>
            <ta e="T112" id="Seg_4072" s="T111">np.h:S</ta>
            <ta e="T113" id="Seg_4073" s="T112">pro.h:S</ta>
            <ta e="T114" id="Seg_4074" s="T113">v:pred</ta>
            <ta e="T115" id="Seg_4075" s="T114">v:pred</ta>
            <ta e="T118" id="Seg_4076" s="T117">0.3.h:S v:pred</ta>
            <ta e="T119" id="Seg_4077" s="T118">pro.h:O</ta>
            <ta e="T120" id="Seg_4078" s="T119">0.3.h:S v:pred</ta>
            <ta e="T123" id="Seg_4079" s="T122">0.3.h:S v:pred</ta>
            <ta e="T124" id="Seg_4080" s="T123">0.1.h:S v:pred</ta>
            <ta e="T128" id="Seg_4081" s="T127">np:S</ta>
            <ta e="T130" id="Seg_4082" s="T129">cop</ta>
            <ta e="T131" id="Seg_4083" s="T130">n:pred</ta>
            <ta e="T135" id="Seg_4084" s="T134">np:S</ta>
            <ta e="T137" id="Seg_4085" s="T136">n:pred</ta>
            <ta e="T141" id="Seg_4086" s="T140">np:O</ta>
            <ta e="T142" id="Seg_4087" s="T141">0.1.h:S v:pred</ta>
            <ta e="T147" id="Seg_4088" s="T146">0.1.h:S v:pred</ta>
            <ta e="T148" id="Seg_4089" s="T147">s:purp</ta>
            <ta e="T152" id="Seg_4090" s="T151">np:S</ta>
            <ta e="T153" id="Seg_4091" s="T152">n:pred</ta>
            <ta e="T155" id="Seg_4092" s="T154">pro.h:S</ta>
            <ta e="T156" id="Seg_4093" s="T155">v:pred</ta>
            <ta e="T158" id="Seg_4094" s="T157">np:O</ta>
            <ta e="T160" id="Seg_4095" s="T159">pro.h:S</ta>
            <ta e="T161" id="Seg_4096" s="T160">v:pred</ta>
            <ta e="T165" id="Seg_4097" s="T164">0.1.h:S v:pred</ta>
            <ta e="T169" id="Seg_4098" s="T168">v:pred</ta>
            <ta e="T170" id="Seg_4099" s="T169">pro.h:S</ta>
            <ta e="T178" id="Seg_4100" s="T177">pro.h:O</ta>
            <ta e="T179" id="Seg_4101" s="T178">0.3.h:S v:pred</ta>
            <ta e="T183" id="Seg_4102" s="T182">pro.h:S</ta>
            <ta e="T184" id="Seg_4103" s="T183">v:pred</ta>
            <ta e="T190" id="Seg_4104" s="T189">pro.h:O</ta>
            <ta e="T191" id="Seg_4105" s="T190">0.3.h:S v:pred</ta>
            <ta e="T195" id="Seg_4106" s="T194">pro.h:S</ta>
            <ta e="T196" id="Seg_4107" s="T195">v:pred</ta>
            <ta e="T203" id="Seg_4108" s="T202">pro.h:S</ta>
            <ta e="T204" id="Seg_4109" s="T203">v:pred</ta>
            <ta e="T206" id="Seg_4110" s="T205">0.1.h:S v:pred</ta>
            <ta e="T209" id="Seg_4111" s="T208">0.1.h:S v:pred</ta>
            <ta e="T214" id="Seg_4112" s="T213">pro.h:O</ta>
            <ta e="T215" id="Seg_4113" s="T214">0.3.h:S v:pred</ta>
            <ta e="T219" id="Seg_4114" s="T218">0.1.h:S v:pred</ta>
            <ta e="T229" id="Seg_4115" s="T228">0.1.h:S v:pred</ta>
            <ta e="T231" id="Seg_4116" s="T230">pro.h:S</ta>
            <ta e="T232" id="Seg_4117" s="T231">v:pred</ta>
            <ta e="T233" id="Seg_4118" s="T232">pro.h:S</ta>
            <ta e="T235" id="Seg_4119" s="T234">v:pred</ta>
            <ta e="T238" id="Seg_4120" s="T237">np.h:S</ta>
            <ta e="T240" id="Seg_4121" s="T239">v:pred</ta>
            <ta e="T246" id="Seg_4122" s="T245">np.h:S</ta>
            <ta e="T247" id="Seg_4123" s="T246">n:pred</ta>
            <ta e="T248" id="Seg_4124" s="T247">np.h:S</ta>
            <ta e="T251" id="Seg_4125" s="T250">v:pred</ta>
            <ta e="T253" id="Seg_4126" s="T252">np.h:S</ta>
            <ta e="T254" id="Seg_4127" s="T253">v:pred</ta>
            <ta e="T260" id="Seg_4128" s="T259">np.h:S</ta>
            <ta e="T265" id="Seg_4129" s="T264">np.h:S</ta>
            <ta e="T267" id="Seg_4130" s="T266">v:pred</ta>
            <ta e="T268" id="Seg_4131" s="T267">pro.h:O</ta>
            <ta e="T269" id="Seg_4132" s="T268">0.3.h:S v:pred</ta>
            <ta e="T271" id="Seg_4133" s="T270">pro.h:O</ta>
            <ta e="T273" id="Seg_4134" s="T272">0.3.h:S v:pred</ta>
            <ta e="T274" id="Seg_4135" s="T273">0.3.h:S v:pred</ta>
            <ta e="T277" id="Seg_4136" s="T276">np.h:S</ta>
            <ta e="T278" id="Seg_4137" s="T277">n:pred</ta>
            <ta e="T279" id="Seg_4138" s="T278">pro.h:S</ta>
            <ta e="T280" id="Seg_4139" s="T279">v:pred</ta>
            <ta e="T283" id="Seg_4140" s="T282">pro.h:S</ta>
            <ta e="T284" id="Seg_4141" s="T283">v:pred</ta>
            <ta e="T288" id="Seg_4142" s="T287">np:O</ta>
            <ta e="T290" id="Seg_4143" s="T289">v:pred</ta>
            <ta e="T291" id="Seg_4144" s="T290">pro.h:S</ta>
            <ta e="T294" id="Seg_4145" s="T293">0.1.h:S v:pred</ta>
            <ta e="T295" id="Seg_4146" s="T294">0.1.h:S v:pred</ta>
            <ta e="T296" id="Seg_4147" s="T295">0.1.h:S v:pred</ta>
            <ta e="T297" id="Seg_4148" s="T296">0.1.h:S v:pred</ta>
            <ta e="T299" id="Seg_4149" s="T298">0.1.h:S v:pred</ta>
            <ta e="T300" id="Seg_4150" s="T299">0.1.h:S v:pred</ta>
            <ta e="T302" id="Seg_4151" s="T301">0.1.h:S v:pred</ta>
            <ta e="T303" id="Seg_4152" s="T302">v:O</ta>
            <ta e="T307" id="Seg_4153" s="T306">np:S</ta>
            <ta e="T308" id="Seg_4154" s="T307">v:pred</ta>
            <ta e="T311" id="Seg_4155" s="T310">v:O</ta>
            <ta e="T312" id="Seg_4156" s="T311">0.1.h:S v:pred</ta>
            <ta e="T315" id="Seg_4157" s="T314">pro.h:S</ta>
            <ta e="T316" id="Seg_4158" s="T315">pro:O</ta>
            <ta e="T317" id="Seg_4159" s="T316">v:pred</ta>
            <ta e="T318" id="Seg_4160" s="T317">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_4161" s="T0">0.1.h:P</ta>
            <ta e="T4" id="Seg_4162" s="T3">np:Time</ta>
            <ta e="T10" id="Seg_4163" s="T9">np:Time</ta>
            <ta e="T11" id="Seg_4164" s="T10">np:L</ta>
            <ta e="T12" id="Seg_4165" s="T11">np:Poss</ta>
            <ta e="T13" id="Seg_4166" s="T12">np:Th</ta>
            <ta e="T20" id="Seg_4167" s="T19">np.h:A 0.1.h:Poss</ta>
            <ta e="T22" id="Seg_4168" s="T21">adv:Time</ta>
            <ta e="T25" id="Seg_4169" s="T24">0.3.h:A</ta>
            <ta e="T29" id="Seg_4170" s="T28">np:Th</ta>
            <ta e="T30" id="Seg_4171" s="T29">pro.h:Th</ta>
            <ta e="T32" id="Seg_4172" s="T31">0.1.h:Poss</ta>
            <ta e="T33" id="Seg_4173" s="T32">np.h:P 0.1.h:Poss</ta>
            <ta e="T35" id="Seg_4174" s="T34">np.h:Poss</ta>
            <ta e="T37" id="Seg_4175" s="T36">np.h:Th</ta>
            <ta e="T38" id="Seg_4176" s="T37">0.3.h:P</ta>
            <ta e="T40" id="Seg_4177" s="T39">pro.h:Th</ta>
            <ta e="T41" id="Seg_4178" s="T40">0.1.h:Poss np:L</ta>
            <ta e="T42" id="Seg_4179" s="T41">0.1.h:Poss np:L</ta>
            <ta e="T44" id="Seg_4180" s="T43">np.h:A 0.1.h:Poss</ta>
            <ta e="T48" id="Seg_4181" s="T47">np:L</ta>
            <ta e="T49" id="Seg_4182" s="T48">0.3.h:Th</ta>
            <ta e="T50" id="Seg_4183" s="T49">0.1.h:Th</ta>
            <ta e="T51" id="Seg_4184" s="T50">0.1.h:Th</ta>
            <ta e="T52" id="Seg_4185" s="T51">np:Com 0.1.h:Poss</ta>
            <ta e="T53" id="Seg_4186" s="T52">np:Com</ta>
            <ta e="T54" id="Seg_4187" s="T53">pro.h:A</ta>
            <ta e="T56" id="Seg_4188" s="T55">adv:Time</ta>
            <ta e="T57" id="Seg_4189" s="T56">np:Ins</ta>
            <ta e="T58" id="Seg_4190" s="T57">np:G</ta>
            <ta e="T59" id="Seg_4191" s="T58">adv:Time</ta>
            <ta e="T65" id="Seg_4192" s="T64">pro.h:Th</ta>
            <ta e="T67" id="Seg_4193" s="T66">np.h:Th 0.1.h:Poss</ta>
            <ta e="T72" id="Seg_4194" s="T71">pro.h:P</ta>
            <ta e="T73" id="Seg_4195" s="T72">np.h:A 0.1.h:Poss</ta>
            <ta e="T75" id="Seg_4196" s="T74">np.h:A 0.1.h:Poss</ta>
            <ta e="T77" id="Seg_4197" s="T76">0.3.h:A</ta>
            <ta e="T78" id="Seg_4198" s="T77">pro:Com</ta>
            <ta e="T79" id="Seg_4199" s="T78">pro.h:Th</ta>
            <ta e="T82" id="Seg_4200" s="T81">pro.h:A</ta>
            <ta e="T84" id="Seg_4201" s="T83">np:Com</ta>
            <ta e="T85" id="Seg_4202" s="T84">pro.h:Th</ta>
            <ta e="T87" id="Seg_4203" s="T86">pro.h:Poss</ta>
            <ta e="T89" id="Seg_4204" s="T88">np.h:Th </ta>
            <ta e="T90" id="Seg_4205" s="T89">np:Th</ta>
            <ta e="T91" id="Seg_4206" s="T90">pro.h:Poss</ta>
            <ta e="T93" id="Seg_4207" s="T92">pro.h:Th</ta>
            <ta e="T94" id="Seg_4208" s="T93">0.3.h:E</ta>
            <ta e="T95" id="Seg_4209" s="T94">0.1.h:A</ta>
            <ta e="T96" id="Seg_4210" s="T95">v:Th</ta>
            <ta e="T97" id="Seg_4211" s="T96">np:Ins</ta>
            <ta e="T98" id="Seg_4212" s="T97">np:Ins</ta>
            <ta e="T99" id="Seg_4213" s="T98">v:Th</ta>
            <ta e="T100" id="Seg_4214" s="T99">v:Th</ta>
            <ta e="T101" id="Seg_4215" s="T100">np:Ins</ta>
            <ta e="T102" id="Seg_4216" s="T101">np:Ins</ta>
            <ta e="T103" id="Seg_4217" s="T102">v:Th</ta>
            <ta e="T104" id="Seg_4218" s="T103">np:L</ta>
            <ta e="T105" id="Seg_4219" s="T104">v:Th</ta>
            <ta e="T106" id="Seg_4220" s="T105">np.h:Poss</ta>
            <ta e="T107" id="Seg_4221" s="T106">np:Time</ta>
            <ta e="T108" id="Seg_4222" s="T107">pro.h:P</ta>
            <ta e="T109" id="Seg_4223" s="T108">0.3.h:A</ta>
            <ta e="T110" id="Seg_4224" s="T109">pro.h:Poss</ta>
            <ta e="T112" id="Seg_4225" s="T111">np.h:Th</ta>
            <ta e="T113" id="Seg_4226" s="T112">pro.h:Th</ta>
            <ta e="T118" id="Seg_4227" s="T117">0.3.h:A</ta>
            <ta e="T119" id="Seg_4228" s="T118">pro.h:P</ta>
            <ta e="T120" id="Seg_4229" s="T119">0.3.h:A</ta>
            <ta e="T121" id="Seg_4230" s="T120">adv:Time</ta>
            <ta e="T123" id="Seg_4231" s="T122">0.3.h:P</ta>
            <ta e="T124" id="Seg_4232" s="T123">0.1.h:A</ta>
            <ta e="T126" id="Seg_4233" s="T125">np:Time</ta>
            <ta e="T127" id="Seg_4234" s="T126">np:L</ta>
            <ta e="T128" id="Seg_4235" s="T127">np:Th</ta>
            <ta e="T129" id="Seg_4236" s="T128">pro:Poss</ta>
            <ta e="T132" id="Seg_4237" s="T131">adv:Time</ta>
            <ta e="T134" id="Seg_4238" s="T133">np:Poss</ta>
            <ta e="T135" id="Seg_4239" s="T134">np:Th</ta>
            <ta e="T141" id="Seg_4240" s="T140">np:Th</ta>
            <ta e="T142" id="Seg_4241" s="T141">0.1.h:A</ta>
            <ta e="T144" id="Seg_4242" s="T143">np:L</ta>
            <ta e="T146" id="Seg_4243" s="T145">np:So</ta>
            <ta e="T147" id="Seg_4244" s="T146">0.1.h:A</ta>
            <ta e="T150" id="Seg_4245" s="T149">np:G</ta>
            <ta e="T151" id="Seg_4246" s="T150">np:Poss</ta>
            <ta e="T152" id="Seg_4247" s="T151">np:Th</ta>
            <ta e="T154" id="Seg_4248" s="T153">adv:Time</ta>
            <ta e="T155" id="Seg_4249" s="T154">pro.h:A</ta>
            <ta e="T158" id="Seg_4250" s="T157">np:Th</ta>
            <ta e="T159" id="Seg_4251" s="T158">adv:Time</ta>
            <ta e="T160" id="Seg_4252" s="T159">pro.h:A</ta>
            <ta e="T162" id="Seg_4253" s="T161">np:L</ta>
            <ta e="T164" id="Seg_4254" s="T163">np:Time</ta>
            <ta e="T165" id="Seg_4255" s="T164">0.1.h:A</ta>
            <ta e="T167" id="Seg_4256" s="T166">np:L</ta>
            <ta e="T170" id="Seg_4257" s="T169">pro.h:A</ta>
            <ta e="T172" id="Seg_4258" s="T171">np:G</ta>
            <ta e="T174" id="Seg_4259" s="T173">np:So</ta>
            <ta e="T177" id="Seg_4260" s="T176">:Time</ta>
            <ta e="T178" id="Seg_4261" s="T177">pro.h:Th</ta>
            <ta e="T179" id="Seg_4262" s="T178">0.3.h:A</ta>
            <ta e="T181" id="Seg_4263" s="T180">np:G</ta>
            <ta e="T182" id="Seg_4264" s="T181">adv:L</ta>
            <ta e="T183" id="Seg_4265" s="T182">pro.h:A</ta>
            <ta e="T186" id="Seg_4266" s="T185">np:Time</ta>
            <ta e="T189" id="Seg_4267" s="T188">adv:Time</ta>
            <ta e="T190" id="Seg_4268" s="T189">pro.h:Th</ta>
            <ta e="T191" id="Seg_4269" s="T190">0.3.h:A</ta>
            <ta e="T192" id="Seg_4270" s="T191">np:G</ta>
            <ta e="T194" id="Seg_4271" s="T193">adv:Time</ta>
            <ta e="T195" id="Seg_4272" s="T194">pro.h:A</ta>
            <ta e="T198" id="Seg_4273" s="T197">np:G</ta>
            <ta e="T199" id="Seg_4274" s="T198">adv:L</ta>
            <ta e="T202" id="Seg_4275" s="T201">adv:Time</ta>
            <ta e="T203" id="Seg_4276" s="T202">pro.h:A</ta>
            <ta e="T205" id="Seg_4277" s="T204">np:Time</ta>
            <ta e="T206" id="Seg_4278" s="T205">0.1.h:Th</ta>
            <ta e="T207" id="Seg_4279" s="T206">np:L</ta>
            <ta e="T208" id="Seg_4280" s="T207">adv:Time</ta>
            <ta e="T209" id="Seg_4281" s="T208">0.1.h:A</ta>
            <ta e="T211" id="Seg_4282" s="T210">np:L</ta>
            <ta e="T213" id="Seg_4283" s="T212">np:So</ta>
            <ta e="T214" id="Seg_4284" s="T213">pro.h:Th</ta>
            <ta e="T215" id="Seg_4285" s="T214">0.3.h:A</ta>
            <ta e="T217" id="Seg_4286" s="T216">np:G</ta>
            <ta e="T218" id="Seg_4287" s="T217">np:L</ta>
            <ta e="T219" id="Seg_4288" s="T218">0.1.h:A</ta>
            <ta e="T220" id="Seg_4289" s="T219">np:L</ta>
            <ta e="T221" id="Seg_4290" s="T220">adv:Time</ta>
            <ta e="T223" id="Seg_4291" s="T222">np:L</ta>
            <ta e="T226" id="Seg_4292" s="T225">np:L</ta>
            <ta e="T229" id="Seg_4293" s="T228">0.1.h:A</ta>
            <ta e="T230" id="Seg_4294" s="T229">adv:L</ta>
            <ta e="T231" id="Seg_4295" s="T230">pro.h:A</ta>
            <ta e="T233" id="Seg_4296" s="T232">pro.h:Th</ta>
            <ta e="T234" id="Seg_4297" s="T233">0.1.h:Poss np:Com</ta>
            <ta e="T237" id="Seg_4298" s="T236">np:Time</ta>
            <ta e="T238" id="Seg_4299" s="T237">np.h:Th</ta>
            <ta e="T242" id="Seg_4300" s="T241">0.1.h:Poss</ta>
            <ta e="T245" id="Seg_4301" s="T244">0.1.h:Poss</ta>
            <ta e="T246" id="Seg_4302" s="T245">np.h:Th 0.1.h:Poss</ta>
            <ta e="T248" id="Seg_4303" s="T247">0.1.h:Poss np.h:A</ta>
            <ta e="T253" id="Seg_4304" s="T252">np.h:A 0.1.h:Poss</ta>
            <ta e="T256" id="Seg_4305" s="T255">adv:Time</ta>
            <ta e="T258" id="Seg_4306" s="T257">np:Th</ta>
            <ta e="T260" id="Seg_4307" s="T259">np.h:Th</ta>
            <ta e="T265" id="Seg_4308" s="T264">np.h:Th 0.1.h:Poss</ta>
            <ta e="T268" id="Seg_4309" s="T267">pro.h:Th</ta>
            <ta e="T269" id="Seg_4310" s="T268">0.3.h:A</ta>
            <ta e="T270" id="Seg_4311" s="T269">np:G</ta>
            <ta e="T271" id="Seg_4312" s="T270">pro.h:P</ta>
            <ta e="T272" id="Seg_4313" s="T271">adv:L</ta>
            <ta e="T273" id="Seg_4314" s="T272">0.3.h:A</ta>
            <ta e="T274" id="Seg_4315" s="T273">0.3.h:A</ta>
            <ta e="T275" id="Seg_4316" s="T274">adv:G</ta>
            <ta e="T276" id="Seg_4317" s="T275">pro.h:Poss</ta>
            <ta e="T277" id="Seg_4318" s="T276">np.h:Th</ta>
            <ta e="T279" id="Seg_4319" s="T278">pro.h:A</ta>
            <ta e="T283" id="Seg_4320" s="T282">pro.h:A</ta>
            <ta e="T288" id="Seg_4321" s="T287">np:Th</ta>
            <ta e="T289" id="Seg_4322" s="T288">np:L</ta>
            <ta e="T291" id="Seg_4323" s="T290">pro.h:A</ta>
            <ta e="T293" id="Seg_4324" s="T292">adv:Time</ta>
            <ta e="T294" id="Seg_4325" s="T293">0.1.h:A</ta>
            <ta e="T295" id="Seg_4326" s="T294">0.1.h:A</ta>
            <ta e="T296" id="Seg_4327" s="T295">0.1.h:A</ta>
            <ta e="T297" id="Seg_4328" s="T296">0.1.h:A</ta>
            <ta e="T298" id="Seg_4329" s="T297">adv:Time</ta>
            <ta e="T299" id="Seg_4330" s="T298">0.1.h:A</ta>
            <ta e="T300" id="Seg_4331" s="T299">0.1.h:A</ta>
            <ta e="T301" id="Seg_4332" s="T300">adv:Time</ta>
            <ta e="T302" id="Seg_4333" s="T301">0.1.h:E</ta>
            <ta e="T303" id="Seg_4334" s="T302">v:Th</ta>
            <ta e="T305" id="Seg_4335" s="T304">np:Th</ta>
            <ta e="T307" id="Seg_4336" s="T306">np:P 0.1.h:Poss </ta>
            <ta e="T310" id="Seg_4337" s="T309">np:Th</ta>
            <ta e="T311" id="Seg_4338" s="T310">v:Th</ta>
            <ta e="T312" id="Seg_4339" s="T311">0.1.h:E</ta>
            <ta e="T315" id="Seg_4340" s="T314">pro.h:A</ta>
            <ta e="T316" id="Seg_4341" s="T315">pro:Th</ta>
            <ta e="T318" id="Seg_4342" s="T317">0.1.h:Th</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T8" id="Seg_4343" s="T7">RUS:cult</ta>
            <ta e="T9" id="Seg_4344" s="T8">RUS:cult</ta>
            <ta e="T10" id="Seg_4345" s="T9">RUS:cult</ta>
            <ta e="T24" id="Seg_4346" s="T23">RUS:gram</ta>
            <ta e="T61" id="Seg_4347" s="T60">RUS:gram</ta>
            <ta e="T126" id="Seg_4348" s="T125">RUS:cult</ta>
            <ta e="T131" id="Seg_4349" s="T130">RUS:cult</ta>
            <ta e="T134" id="Seg_4350" s="T133">RUS:cult</ta>
            <ta e="T139" id="Seg_4351" s="T138">RUS:cult</ta>
            <ta e="T141" id="Seg_4352" s="T140">RUS:cult</ta>
            <ta e="T149" id="Seg_4353" s="T148">RUS:cult</ta>
            <ta e="T150" id="Seg_4354" s="T149">RUS:cult</ta>
            <ta e="T151" id="Seg_4355" s="T150">RUS:cult</ta>
            <ta e="T158" id="Seg_4356" s="T157">RUS:cult</ta>
            <ta e="T163" id="Seg_4357" s="T162">RUS:cult</ta>
            <ta e="T166" id="Seg_4358" s="T165">RUS:cult</ta>
            <ta e="T167" id="Seg_4359" s="T166">RUS:cult</ta>
            <ta e="T168" id="Seg_4360" s="T167">RUS:gram</ta>
            <ta e="T172" id="Seg_4361" s="T171">RUS:cult</ta>
            <ta e="T174" id="Seg_4362" s="T173">RUS:cult</ta>
            <ta e="T181" id="Seg_4363" s="T180">RUS:cult</ta>
            <ta e="T201" id="Seg_4364" s="T199">RUS:disc</ta>
            <ta e="T216" id="Seg_4365" s="T215">RUS:cult</ta>
            <ta e="T220" id="Seg_4366" s="T219">RUS:cult</ta>
            <ta e="T223" id="Seg_4367" s="T222">RUS:cult</ta>
            <ta e="T226" id="Seg_4368" s="T225">RUS:cult</ta>
            <ta e="T243" id="Seg_4369" s="T242">RUS:gram</ta>
            <ta e="T258" id="Seg_4370" s="T257">RUS:cult</ta>
            <ta e="T270" id="Seg_4371" s="T269">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T181" id="Seg_4372" s="T180">medCdel finVdel</ta>
            <ta e="T201" id="Seg_4373" s="T199">Vsub</ta>
            <ta e="T220" id="Seg_4374" s="T219">Csub</ta>
            <ta e="T258" id="Seg_4375" s="T257">Csub</ta>
            <ta e="T270" id="Seg_4376" s="T269">finCdel</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T8" id="Seg_4377" s="T7">dir:infl</ta>
            <ta e="T9" id="Seg_4378" s="T8">dir:infl</ta>
            <ta e="T10" id="Seg_4379" s="T9">dir:infl</ta>
            <ta e="T126" id="Seg_4380" s="T125">dir:infl</ta>
            <ta e="T141" id="Seg_4381" s="T140">dir:infl</ta>
            <ta e="T149" id="Seg_4382" s="T148">dir:bare</ta>
            <ta e="T150" id="Seg_4383" s="T149">dir:infl</ta>
            <ta e="T151" id="Seg_4384" s="T150">dir</ta>
            <ta e="T158" id="Seg_4385" s="T157">dir:infl</ta>
            <ta e="T163" id="Seg_4386" s="T162">dir:bare</ta>
            <ta e="T166" id="Seg_4387" s="T165">dir:bare</ta>
            <ta e="T167" id="Seg_4388" s="T166">dir:infl</ta>
            <ta e="T172" id="Seg_4389" s="T171">dir:infl</ta>
            <ta e="T174" id="Seg_4390" s="T173">dir:infl</ta>
            <ta e="T181" id="Seg_4391" s="T180">dir:infl</ta>
            <ta e="T220" id="Seg_4392" s="T219">dir:infl</ta>
            <ta e="T223" id="Seg_4393" s="T222">dir:infl</ta>
            <ta e="T258" id="Seg_4394" s="T257">dir:infl</ta>
            <ta e="T270" id="Seg_4395" s="T269">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T224" id="Seg_4396" s="T223">RUS:int.ins</ta>
            <ta e="T228" id="Seg_4397" s="T226">RUS:int.ins</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T11" id="Seg_4398" s="T0">Я родился в шестнадцатом году, двадцать третьего числа, в сентябре месяце в деревне.</ta>
            <ta e="T14" id="Seg_4399" s="T11">Деревня называется Карелино.</ta>
            <ta e="T18" id="Seg_4400" s="T14">Меня зовут Матвей Карелин.</ta>
            <ta e="T25" id="Seg_4401" s="T18">Мои родители раньше рыбачили и охотились.</ta>
            <ta e="T32" id="Seg_4402" s="T25">Мне было три года, когда я остался (от/без?) матери.</ta>
            <ta e="T34" id="Seg_4403" s="T32">Моя мать умерла.</ta>
            <ta e="T38" id="Seg_4404" s="T34">У матери были дети, они умирали.</ta>
            <ta e="T43" id="Seg_4405" s="T38">Один я у матери, у отца остался.</ta>
            <ta e="T49" id="Seg_4406" s="T43">Мой отец ходил охотиться и рыбачить, он дома не бывал.</ta>
            <ta e="T52" id="Seg_4407" s="T49">Я оставался жить с дедушкой.</ta>
            <ta e="T63" id="Seg_4408" s="T52">С ним я ходил зимой на лыжах в тайгу, летом сети ставить, рыбачить.</ta>
            <ta e="T66" id="Seg_4409" s="T63">Так я жил.</ta>
            <ta e="T70" id="Seg_4410" s="T66">Дедушка у меня хороший был.</ta>
            <ta e="T77" id="Seg_4411" s="T70">Когда отец вздумает стегать меня, дедушка заступался.</ta>
            <ta e="T80" id="Seg_4412" s="T77">Я спал с ним.</ta>
            <ta e="T86" id="Seg_4413" s="T80">Куда он пойдет, с ним я бывал.</ta>
            <ta e="T89" id="Seg_4414" s="T86">У меня был дядя.</ta>
            <ta e="T92" id="Seg_4415" s="T89">Его имя [было] Михаил.</ta>
            <ta e="T94" id="Seg_4416" s="T92">Меня он любил.</ta>
            <ta e="T105" id="Seg_4417" s="T94">Учил ходить на лыжах, на лодке, охотиться на уток, на белок, стрелять из ружья, ездить на лошади.</ta>
            <ta e="T109" id="Seg_4418" s="T105">В войну его убили.</ta>
            <ta e="T112" id="Seg_4419" s="T109">У меня была тетя.</ta>
            <ta e="T116" id="Seg_4420" s="T112">Она была похожа на медведя.</ta>
            <ta e="T120" id="Seg_4421" s="T116">На меня она ругалась, она меня стегала.</ta>
            <ta e="T123" id="Seg_4422" s="T120">Сейчас она уже тоже умерла.</ta>
            <ta e="T127" id="Seg_4423" s="T123">Я учился с первого класса в деревне.</ta>
            <ta e="T131" id="Seg_4424" s="T127">Название ее было Пристань.</ta>
            <ta e="T139" id="Seg_4425" s="T131">Сейчас Пристань называется Белый Яр, Верхне-кетский район.</ta>
            <ta e="T144" id="Seg_4426" s="T139">Четыре класса я окончил в Максимкином Яру.</ta>
            <ta e="T153" id="Seg_4427" s="T144">Из Максимкиного Яра уехал учиться в Каргасогский район в школу в Вертикос.</ta>
            <ta e="T158" id="Seg_4428" s="T153">Там я окончил семь классов.</ta>
            <ta e="T163" id="Seg_4429" s="T158">Потом я учился в Колпашевском педучилище.</ta>
            <ta e="T172" id="Seg_4430" s="T163">Один год проучился на подготовительном курсе, перешел на первый курс.</ta>
            <ta e="T181" id="Seg_4431" s="T172">С первого курса в тридцать девятом году меня взяли в Красную армию.</ta>
            <ta e="T186" id="Seg_4432" s="T181">Там я был семь лет. </ta>
            <ta e="T192" id="Seg_4433" s="T186">В сорок шестом году меня отпустили домой.</ta>
            <ta e="T198" id="Seg_4434" s="T192">В этом году я приехал в Белый Яр.</ta>
            <ta e="T204" id="Seg_4435" s="T198">Здесь же в этом же году я женился.</ta>
            <ta e="T207" id="Seg_4436" s="T204">Год был в Орловке.</ta>
            <ta e="T211" id="Seg_4437" s="T207">Потом работал в Белом Яру.</ta>
            <ta e="T217" id="Seg_4438" s="T211">Из Белого Яра меня направили в Усть-Озерное.</ta>
            <ta e="T228" id="Seg_4439" s="T217">В [этой] деревне работал в клубе, затем в рыболовецкой артели (председателем), потом в сельсовете председателем сельсовета.</ta>
            <ta e="T230" id="Seg_4440" s="T228">Работаю тут.</ta>
            <ta e="T232" id="Seg_4441" s="T230">Я женился.</ta>
            <ta e="T237" id="Seg_4442" s="T232">Я с женой живу восемнадцать лет.</ta>
            <ta e="T245" id="Seg_4443" s="T237">Детей стало много: пять сыновей и две дочери.</ta>
            <ta e="T247" id="Seg_4444" s="T245">Жена русская.</ta>
            <ta e="T251" id="Seg_4445" s="T247">Дети по-селькупски не разговаривают.</ta>
            <ta e="T258" id="Seg_4446" s="T251">Старший сын окончил в этом году десять классов.</ta>
            <ta e="T263" id="Seg_4447" s="T258">Маленькому сыну пять, шестой год.</ta>
            <ta e="T267" id="Seg_4448" s="T263">Один сын сильно болел.</ta>
            <ta e="T270" id="Seg_4449" s="T267">Его увозили в Томск.</ta>
            <ta e="T275" id="Seg_4450" s="T270">Его там вылечили, привезли обратно.</ta>
            <ta e="T278" id="Seg_4451" s="T275">Мои дети селькупы.</ta>
            <ta e="T281" id="Seg_4452" s="T278">Они учатся бесплатно.</ta>
            <ta e="T289" id="Seg_4453" s="T281">Сам я зарабатываю сорок пять рублей в месяц.</ta>
            <ta e="T292" id="Seg_4454" s="T289">Живу я хорошо.</ta>
            <ta e="T297" id="Seg_4455" s="T292">Осенью собираю орехи, охочусь на глухаря, ягоды собираю, охочусь.</ta>
            <ta e="T300" id="Seg_4456" s="T297">Летом сетки ставлю, рыбачу.</ta>
            <ta e="T305" id="Seg_4457" s="T300">Сейчас хочу купить новый дом.</ta>
            <ta e="T308" id="Seg_4458" s="T305">Мой дом сгнил.</ta>
            <ta e="T314" id="Seg_4459" s="T308">Новый дом купить хочу за шестьсот рублей.</ta>
            <ta e="T318" id="Seg_4460" s="T314">Я его куплю и жить буду.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T11" id="Seg_4461" s="T0">I was born in 1916, on the 23-rd of September in a village.</ta>
            <ta e="T14" id="Seg_4462" s="T11">The village is called Karelino.</ta>
            <ta e="T18" id="Seg_4463" s="T14">My name is Matvey Karelin.</ta>
            <ta e="T25" id="Seg_4464" s="T18">Earlier my parents used to fish and to hunt.</ta>
            <ta e="T32" id="Seg_4465" s="T25">I was three years old as I stayed alone (from my mother).</ta>
            <ta e="T34" id="Seg_4466" s="T32">My mother died.</ta>
            <ta e="T38" id="Seg_4467" s="T34">My mother did have children, they died [one after another].</ta>
            <ta e="T43" id="Seg_4468" s="T38">Only me stayed by my mother, by my father.</ta>
            <ta e="T49" id="Seg_4469" s="T43">My father went hunting and fishing, he wasn't [usually] at home.</ta>
            <ta e="T52" id="Seg_4470" s="T49">I stayed to live with my grandfather.</ta>
            <ta e="T63" id="Seg_4471" s="T52">In winter I went with him skiing, in summer [we went] netting and fishing.</ta>
            <ta e="T66" id="Seg_4472" s="T63">So I lived.</ta>
            <ta e="T70" id="Seg_4473" s="T66">My grandfather was a good man.</ta>
            <ta e="T77" id="Seg_4474" s="T70">When my father wanted to whip me, my grandfather defended me.</ta>
            <ta e="T80" id="Seg_4475" s="T77">I slept with him.</ta>
            <ta e="T86" id="Seg_4476" s="T80">Wherever he went, I was with him.</ta>
            <ta e="T89" id="Seg_4477" s="T86">I had an uncle.</ta>
            <ta e="T92" id="Seg_4478" s="T89">His name [was] Mikhail.</ta>
            <ta e="T94" id="Seg_4479" s="T92">He loved me.</ta>
            <ta e="T105" id="Seg_4480" s="T94">He taught me skiing, driving a boat, hunting ducks and squirells, shooting with a rifle, riding a horse.</ta>
            <ta e="T109" id="Seg_4481" s="T105">He was killed during the war.</ta>
            <ta e="T112" id="Seg_4482" s="T109">I had an aunt.</ta>
            <ta e="T116" id="Seg_4483" s="T112">She looked like a bear.</ta>
            <ta e="T120" id="Seg_4484" s="T116">She scolded me, she whipped me.</ta>
            <ta e="T123" id="Seg_4485" s="T120">Now she is dead too.</ta>
            <ta e="T127" id="Seg_4486" s="T123">From the first class I studied in a village.</ta>
            <ta e="T131" id="Seg_4487" s="T127">The name [of the village] was Pristan.</ta>
            <ta e="T139" id="Seg_4488" s="T131">Now Pristan is called Beliy Yar, Upper Ket district.</ta>
            <ta e="T144" id="Seg_4489" s="T139">I finished four classes in Maksimkin Yar.</ta>
            <ta e="T153" id="Seg_4490" s="T144">From Maksimkin Yar I went to study to Kargasogsky district, to the school of Verte-kos.</ta>
            <ta e="T158" id="Seg_4491" s="T153">I finished seven classes there.</ta>
            <ta e="T163" id="Seg_4492" s="T158">Then I studied at teacher college.</ta>
            <ta e="T172" id="Seg_4493" s="T163">I studied one year in the preapratory class, then I entered the first year.</ta>
            <ta e="T181" id="Seg_4494" s="T172">From the first year I wast taken to the Red Army in 1939.</ta>
            <ta e="T186" id="Seg_4495" s="T181">I spent there seven years.</ta>
            <ta e="T192" id="Seg_4496" s="T186">In 1946 they let me go home.</ta>
            <ta e="T198" id="Seg_4497" s="T192">That year I came to Belyj Yar.</ta>
            <ta e="T204" id="Seg_4498" s="T198">Here I got married the same year.</ta>
            <ta e="T207" id="Seg_4499" s="T204">I spent one year in Orlovka.</ta>
            <ta e="T211" id="Seg_4500" s="T207">Then I worked in Belyj Yar.</ta>
            <ta e="T217" id="Seg_4501" s="T211">From Belyj Yar I was sent to Ust-Ozernoe.</ta>
            <ta e="T228" id="Seg_4502" s="T217">In [this] village I worked in the club, then in the fishers' partnership (as a chairman), then in the village council as a chairman.</ta>
            <ta e="T230" id="Seg_4503" s="T228">I work here.</ta>
            <ta e="T232" id="Seg_4504" s="T230">I got married.</ta>
            <ta e="T237" id="Seg_4505" s="T232">I live with my wife for eighteen years already.</ta>
            <ta e="T245" id="Seg_4506" s="T237">[I] have got many children: five sons and two daughters.</ta>
            <ta e="T247" id="Seg_4507" s="T245">My wife is Russian.</ta>
            <ta e="T251" id="Seg_4508" s="T247">My children don't speak Selkup.</ta>
            <ta e="T258" id="Seg_4509" s="T251">My elder son finished ten classes this year.</ta>
            <ta e="T263" id="Seg_4510" s="T258">My youngest son is five, will be six.</ta>
            <ta e="T267" id="Seg_4511" s="T263">One son was very ill.</ta>
            <ta e="T270" id="Seg_4512" s="T267">They took him to Tomsk.</ta>
            <ta e="T275" id="Seg_4513" s="T270">They cured him and brought him back.</ta>
            <ta e="T278" id="Seg_4514" s="T275">My children are Selkup.</ta>
            <ta e="T281" id="Seg_4515" s="T278">They study for free.</ta>
            <ta e="T289" id="Seg_4516" s="T281">I earn myself forty five rubles in a month.</ta>
            <ta e="T292" id="Seg_4517" s="T289">I live well.</ta>
            <ta e="T297" id="Seg_4518" s="T292">In autumn I gather nuts, I hunt capercaillie, I gather berries and I go hunting.</ta>
            <ta e="T300" id="Seg_4519" s="T297">In summer I go netting and fishing.</ta>
            <ta e="T305" id="Seg_4520" s="T300">Now I want to buy a new house.</ta>
            <ta e="T308" id="Seg_4521" s="T305">My house went bad.</ta>
            <ta e="T314" id="Seg_4522" s="T308">I want to buy a new house for six hundred rubles.</ta>
            <ta e="T318" id="Seg_4523" s="T314">I will buy it and I will live [there].</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T11" id="Seg_4524" s="T0">Ich wurde 1916, am 23. September in einem Dorf geboren.</ta>
            <ta e="T14" id="Seg_4525" s="T11">Das Dorf heißt Karelino.</ta>
            <ta e="T18" id="Seg_4526" s="T14">Mein Name ist Matvej Karelin.</ta>
            <ta e="T25" id="Seg_4527" s="T18">Früher fischten und jagten meine Eltern.</ta>
            <ta e="T32" id="Seg_4528" s="T25">Ich war drei Jahre alt, als ich alleine von meiner Mutter zurückblieb.</ta>
            <ta e="T34" id="Seg_4529" s="T32">Meine Mutter starb.</ta>
            <ta e="T38" id="Seg_4530" s="T34">Meine Mutter hatte Kinder, die starben [nacheinander].</ta>
            <ta e="T43" id="Seg_4531" s="T38">Nur ich blieb bei meinem Vater.</ta>
            <ta e="T49" id="Seg_4532" s="T43">Mein Vater ging jagen und fischen, er war nicht zuhause.</ta>
            <ta e="T52" id="Seg_4533" s="T49">Ich blieb und lebte bei meinem Großvater.</ta>
            <ta e="T63" id="Seg_4534" s="T52">Mit ihm bin ich im Winter in der Taiga Ski gefahren, im Sommer fuhren wir Netze stellen und fischen.</ta>
            <ta e="T66" id="Seg_4535" s="T63">So lebte ich.</ta>
            <ta e="T70" id="Seg_4536" s="T66">Mein Großvater war ein guter Mensch.</ta>
            <ta e="T77" id="Seg_4537" s="T70">Als mein Vater mich auspeitschen wollte, half er mir.</ta>
            <ta e="T80" id="Seg_4538" s="T77">Ich schlief bei ihm.</ta>
            <ta e="T86" id="Seg_4539" s="T80">Wohin er auch gegangen ist, ich war bei ihm.</ta>
            <ta e="T89" id="Seg_4540" s="T86">Ich hatte einen Onkel.</ta>
            <ta e="T92" id="Seg_4541" s="T89">Sein Name [war] Michail.</ta>
            <ta e="T94" id="Seg_4542" s="T92">Er liebte mich.</ta>
            <ta e="T105" id="Seg_4543" s="T94">Er brachte mir bei, wie man auf Ski fährt, wie man mit Boot fährt, wie man Enten und Eichhörnchen jagt, wie man ein auf einem Pferd reitet.</ta>
            <ta e="T109" id="Seg_4544" s="T105">Er wurde im Krieg getötet.</ta>
            <ta e="T112" id="Seg_4545" s="T109">Ich hatte eine Tante.</ta>
            <ta e="T116" id="Seg_4546" s="T112">Sie sah aus wie ein Bär.</ta>
            <ta e="T120" id="Seg_4547" s="T116">Sie schimpfte mit mir, sie peitschte mich aus.</ta>
            <ta e="T123" id="Seg_4548" s="T120">Sie ist auch gestorben.</ta>
            <ta e="T127" id="Seg_4549" s="T123">Von der ersten Klasse an lernte ich in der Dorfschule.</ta>
            <ta e="T131" id="Seg_4550" s="T127">Der Name [des Dorfes] war Pristan.</ta>
            <ta e="T139" id="Seg_4551" s="T131">Heute heißt Pristan Belyj Jar, Region Oberer Ket.</ta>
            <ta e="T144" id="Seg_4552" s="T139">Ich habe vier Klassen in Maksimkin Jar abgeschlossen.</ta>
            <ta e="T153" id="Seg_4553" s="T144">Von Maksimkin Jar ging ich zum Lernen in den Kreis Kargasogskij, der Name der Schule [war] Verte-Kos.</ta>
            <ta e="T158" id="Seg_4554" s="T153">Dort habe ich sieben Klassen abgeschlossen.</ta>
            <ta e="T163" id="Seg_4555" s="T158">Dann studieren ich an der pädagogischen Hochschule in Kolpaschevo.</ta>
            <ta e="T172" id="Seg_4556" s="T163">Ich studierte ein Jahr im Vorbereitungskurs, dann kam ich ins erste Jahr.</ta>
            <ta e="T181" id="Seg_4557" s="T172">Aus dem ersten Jahr, 1939, wurde ich in die Rote Armee berufen.</ta>
            <ta e="T186" id="Seg_4558" s="T181">Dort war ich sieben Jahre.</ta>
            <ta e="T192" id="Seg_4559" s="T186">1946 ließen sie mich nach Hause.</ta>
            <ta e="T198" id="Seg_4560" s="T192">In jenem Jahr kam ich nach Belyj Yar. </ta>
            <ta e="T204" id="Seg_4561" s="T198">Und hier, in jenem Jahr habe ich geheiratet.</ta>
            <ta e="T207" id="Seg_4562" s="T204">Ein Jahr lang war ich in Orlovka.</ta>
            <ta e="T211" id="Seg_4563" s="T207">Danach habe ich in Belyj Jar gearbeitet.</ta>
            <ta e="T217" id="Seg_4564" s="T211">Von Belyj Jar wurde ich nach Ust'-Ozernoe versetzt.</ta>
            <ta e="T228" id="Seg_4565" s="T217">In dem Dorf habe ich im Klub gearbeitet, danach in der Fischereigenossenschaft, danach im Gemeinderat.</ta>
            <ta e="T230" id="Seg_4566" s="T228">Ich arbeite hier.</ta>
            <ta e="T232" id="Seg_4567" s="T230">Ich habe geheiratet.</ta>
            <ta e="T237" id="Seg_4568" s="T232">Ich lebe mit meiner Frau schon achtzehn Jahre zusammen.</ta>
            <ta e="T245" id="Seg_4569" s="T237">Ich habe viele Kinder: fünf Söhne und zwei Töchter.</ta>
            <ta e="T247" id="Seg_4570" s="T245">Meine Frau ist Russin.</ta>
            <ta e="T251" id="Seg_4571" s="T247">Meine Kinder sprechen kein Selkupisch.</ta>
            <ta e="T258" id="Seg_4572" s="T251">Mein ältester Sohn beendete dieses Jahr die zehnte Klasse. </ta>
            <ta e="T263" id="Seg_4573" s="T258">Der jüngere ist fünf, er wird sechs Jahre alt.</ta>
            <ta e="T267" id="Seg_4574" s="T263">Einer von den Söhnen war sehr krank.</ta>
            <ta e="T270" id="Seg_4575" s="T267">Er wurde nach Tomsk gebracht.</ta>
            <ta e="T275" id="Seg_4576" s="T270">Dort hat man ihn geheilt und zurück gebracht.</ta>
            <ta e="T278" id="Seg_4577" s="T275">Meine Kinder sind Selkupen.</ta>
            <ta e="T281" id="Seg_4578" s="T278">Sie lernen kostenlos.</ta>
            <ta e="T289" id="Seg_4579" s="T281">Ich verdiene fünfundvierzig Rubel im Monat.</ta>
            <ta e="T292" id="Seg_4580" s="T289">Ich lebe gut.</ta>
            <ta e="T297" id="Seg_4581" s="T292">Im Herbst sammele ich Nüsse, ich jage Auerhähne, sammele Beeren und jage.</ta>
            <ta e="T300" id="Seg_4582" s="T297">Im Sommer stelle ich meine Netze und fische.</ta>
            <ta e="T305" id="Seg_4583" s="T300">Jetzt möchte ich ein neues Haus kaufen.</ta>
            <ta e="T308" id="Seg_4584" s="T305">Mein Haus ist verfault.</ta>
            <ta e="T314" id="Seg_4585" s="T308">Das neue Haus wil ich für 600 Rubel kaufen.</ta>
            <ta e="T318" id="Seg_4586" s="T314">Ich werde es kaufen und werde dort leben.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T11" id="Seg_4587" s="T0">родился в шестнадцатом году двадцать третьего числа в сентябре месяце в деревне</ta>
            <ta e="T14" id="Seg_4588" s="T11">деревня Корелино</ta>
            <ta e="T18" id="Seg_4589" s="T14">звать меня Матвей Карелин</ta>
            <ta e="T25" id="Seg_4590" s="T18">мои родители раньше рыбачили и охотились</ta>
            <ta e="T32" id="Seg_4591" s="T25">мне было три года я остался от матери</ta>
            <ta e="T34" id="Seg_4592" s="T32">мать умерла</ta>
            <ta e="T38" id="Seg_4593" s="T34">у матери были дети умирали</ta>
            <ta e="T43" id="Seg_4594" s="T38">один я у матери у отца остался</ta>
            <ta e="T49" id="Seg_4595" s="T43">отец ходил охотиться рыбачить дома не бывал</ta>
            <ta e="T52" id="Seg_4596" s="T49">я оставался жил с дедушкой</ta>
            <ta e="T63" id="Seg_4597" s="T52">с ним я ходил зимой на лыжах в тайгу летом сетничать (сетки ставить) удить</ta>
            <ta e="T66" id="Seg_4598" s="T63">так я жил</ta>
            <ta e="T70" id="Seg_4599" s="T66">дедушка у меня хороший был</ta>
            <ta e="T77" id="Seg_4600" s="T70">стегать меня отец вздумает дедушка восставал (заступался)</ta>
            <ta e="T80" id="Seg_4601" s="T77">с ним я спал</ta>
            <ta e="T86" id="Seg_4602" s="T80">куда он пойдет с ним я бывал</ta>
            <ta e="T89" id="Seg_4603" s="T86">у меня был дядя</ta>
            <ta e="T92" id="Seg_4604" s="T89">звать его Михаил</ta>
            <ta e="T94" id="Seg_4605" s="T92">меня любил</ta>
            <ta e="T105" id="Seg_4606" s="T94">учил ходить на лыжах на лодке утковать белковать с ружья стрелять (на лошади) верхом ездить (сидеть)</ta>
            <ta e="T109" id="Seg_4607" s="T105">в отечественную войну его убили</ta>
            <ta e="T112" id="Seg_4608" s="T109">у меня была тетя</ta>
            <ta e="T116" id="Seg_4609" s="T112">она была похожа на медведя</ta>
            <ta e="T120" id="Seg_4610" s="T116">на меня ругалась меня стегала</ta>
            <ta e="T123" id="Seg_4611" s="T120">сейчас тоже умерла</ta>
            <ta e="T127" id="Seg_4612" s="T123">учился с первого класса в деревне</ta>
            <ta e="T131" id="Seg_4613" s="T127">название ее было Пристань</ta>
            <ta e="T139" id="Seg_4614" s="T131">сейчас эта пристань называется Белый Яр верхне-кетский район</ta>
            <ta e="T144" id="Seg_4615" s="T139">четыре класса окончил в Максимкином Яру</ta>
            <ta e="T153" id="Seg_4616" s="T144">с Максимкиного Яра уехал учиться в Каргасогский район в школу Верте-кос</ta>
            <ta e="T158" id="Seg_4617" s="T153">там я окончил семь классов</ta>
            <ta e="T163" id="Seg_4618" s="T158">тогда (после этого) я учился в Колпаш. пед. уч.</ta>
            <ta e="T172" id="Seg_4619" s="T163">один год проучился на подгот. курсе перешел я на первый курс</ta>
            <ta e="T181" id="Seg_4620" s="T172">с первого курса в тридцать девятом году меня взяли в красную армию</ta>
            <ta e="T186" id="Seg_4621" s="T181">там я был</ta>
            <ta e="T192" id="Seg_4622" s="T186">в сорок шестом году меня отпустили (направили) домой</ta>
            <ta e="T198" id="Seg_4623" s="T192">в этом году я приехал в Белый Яр</ta>
            <ta e="T204" id="Seg_4624" s="T198">здесь же в этом же году я женился</ta>
            <ta e="T207" id="Seg_4625" s="T204">год был в Орловке</ta>
            <ta e="T211" id="Seg_4626" s="T207">потом работал в Белом Яру</ta>
            <ta e="T217" id="Seg_4627" s="T211">с Белого Яра меня направили в Усть-Озерное</ta>
            <ta e="T228" id="Seg_4628" s="T217">в Усть-Озерном работал в клубе затем в рыболовецкой артели потом в с/с</ta>
            <ta e="T230" id="Seg_4629" s="T228">работаю тут</ta>
            <ta e="T232" id="Seg_4630" s="T230">я женился</ta>
            <ta e="T237" id="Seg_4631" s="T232">я с женой (женат) живу восемнадцать лет</ta>
            <ta e="T245" id="Seg_4632" s="T237">детей стало много (одетился) пять сыновей две дочери</ta>
            <ta e="T247" id="Seg_4633" s="T245">жена русская</ta>
            <ta e="T251" id="Seg_4634" s="T247">ребята по-остяцки не разговаривают</ta>
            <ta e="T258" id="Seg_4635" s="T251">старший сын окончил в этом году десять классов</ta>
            <ta e="T263" id="Seg_4636" s="T258">маленькому сыну пять шестой год</ta>
            <ta e="T267" id="Seg_4637" s="T263">один сын сильно болел</ta>
            <ta e="T270" id="Seg_4638" s="T267">его увозили Томск</ta>
            <ta e="T275" id="Seg_4639" s="T270">его там вылечили, привезли обратно</ta>
            <ta e="T278" id="Seg_4640" s="T275">мои дети остяки</ta>
            <ta e="T281" id="Seg_4641" s="T278">они учатся бесплатно</ta>
            <ta e="T289" id="Seg_4642" s="T281">сам я зарабатываю сорок пять рублей в месяц</ta>
            <ta e="T292" id="Seg_4643" s="T289">живу я хорошо</ta>
            <ta e="T297" id="Seg_4644" s="T292">осенью шишкую глухарюю, ягоды собираю охотюсь</ta>
            <ta e="T300" id="Seg_4645" s="T297">летом сет′ничу (сетки ставлю) рыбачу</ta>
            <ta e="T305" id="Seg_4646" s="T300">сейчас хочу купить новый дом</ta>
            <ta e="T308" id="Seg_4647" s="T305">свой дом сгнил</ta>
            <ta e="T314" id="Seg_4648" s="T308">новый дом купить хочу за шестьсот рублей</ta>
            <ta e="T318" id="Seg_4649" s="T314">я его куплю и жить буду</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T14" id="Seg_4650" s="T11">[WNB:] Karelino is on the river Chulym (55.141217, 90.683032)</ta>
            <ta e="T131" id="Seg_4651" s="T127">[WNB:] pristanʼ means wharf.</ta>
            <ta e="T139" id="Seg_4652" s="T131">[WNB:] Belyj Yar is today in Teguldetskiy rayon, Tomsk Region (57.494756, 88.762345)</ta>
            <ta e="T198" id="Seg_4653" s="T192">[WNB:] Belij Yar: white taiga</ta>
            <ta e="T314" id="Seg_4654" s="T308">[WNB:] the speaker confounded sixty and six hundred.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T324" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T320" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T321" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T319" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T322" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T323" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
