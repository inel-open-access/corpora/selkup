<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KMS_1963_Mosquitos_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KMS_1963_Mosquitos_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">23</ud-information>
            <ud-information attribute-name="# HIAT:w">16</ud-information>
            <ud-information attribute-name="# e">16</ud-information>
            <ud-information attribute-name="# HIAT:u">5</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KMS">
            <abbreviation>KMS</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KMS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T16" id="Seg_0" n="sc" s="T0">
               <ts e="T2" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">nɨnɨŋa</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">qɨŋɨmba</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T4" id="Seg_11" n="HIAT:u" s="T2">
                  <ts e="T3" id="Seg_13" n="HIAT:w" s="T2">paj</ts>
                  <nts id="Seg_14" n="HIAT:ip">,</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_17" n="HIAT:w" s="T3">paj</ts>
                  <nts id="Seg_18" n="HIAT:ip">!</nts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_21" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_23" n="HIAT:w" s="T4">ponä</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_26" n="HIAT:w" s="T5">čanǯo</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_29" n="HIAT:w" s="T6">sombɨlʼe</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_32" n="HIAT:w" s="T7">minutɨɣɨndə</ts>
                  <nts id="Seg_33" n="HIAT:ip">.</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_36" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_38" n="HIAT:w" s="T8">maːttə</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_41" n="HIAT:w" s="T9">säːrɣuluwattə</ts>
                  <nts id="Seg_42" n="HIAT:ip">.</nts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_45" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_47" n="HIAT:w" s="T10">na</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_50" n="HIAT:w" s="T11">si</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_53" n="HIAT:w" s="T12">pü</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_56" n="HIAT:w" s="T13">qɨŋgɨmbattə</ts>
                  <nts id="Seg_57" n="HIAT:ip">,</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_60" n="HIAT:w" s="T14">assə</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_63" n="HIAT:w" s="T15">toɣəlǯigu</ts>
                  <nts id="Seg_64" n="HIAT:ip">.</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T16" id="Seg_66" n="sc" s="T0">
               <ts e="T1" id="Seg_68" n="e" s="T0">nɨnɨŋa </ts>
               <ts e="T2" id="Seg_70" n="e" s="T1">qɨŋɨmba. </ts>
               <ts e="T3" id="Seg_72" n="e" s="T2">paj, </ts>
               <ts e="T4" id="Seg_74" n="e" s="T3">paj! </ts>
               <ts e="T5" id="Seg_76" n="e" s="T4">ponä </ts>
               <ts e="T6" id="Seg_78" n="e" s="T5">čanǯo </ts>
               <ts e="T7" id="Seg_80" n="e" s="T6">sombɨlʼe </ts>
               <ts e="T8" id="Seg_82" n="e" s="T7">minutɨɣɨndə. </ts>
               <ts e="T9" id="Seg_84" n="e" s="T8">maːttə </ts>
               <ts e="T10" id="Seg_86" n="e" s="T9">säːrɣuluwattə. </ts>
               <ts e="T11" id="Seg_88" n="e" s="T10">na </ts>
               <ts e="T12" id="Seg_90" n="e" s="T11">si </ts>
               <ts e="T13" id="Seg_92" n="e" s="T12">pü </ts>
               <ts e="T14" id="Seg_94" n="e" s="T13">qɨŋgɨmbattə, </ts>
               <ts e="T15" id="Seg_96" n="e" s="T14">assə </ts>
               <ts e="T16" id="Seg_98" n="e" s="T15">toɣəlǯigu. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2" id="Seg_99" s="T0">KMS_1963_Mosquitos_nar.001 (001.001)</ta>
            <ta e="T4" id="Seg_100" s="T2">KMS_1963_Mosquitos_nar.002 (001.002)</ta>
            <ta e="T8" id="Seg_101" s="T4">KMS_1963_Mosquitos_nar.003 (001.003)</ta>
            <ta e="T10" id="Seg_102" s="T8">KMS_1963_Mosquitos_nar.004 (001.004)</ta>
            <ta e="T16" id="Seg_103" s="T10">KMS_1963_Mosquitos_nar.005 (001.005)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T2" id="Seg_104" s="T0">ныны′ңа kың(г)ымба.</ta>
            <ta e="T4" id="Seg_105" s="T2">пай, пай!</ta>
            <ta e="T8" id="Seg_106" s="T4">′понӓ тша′нджо сомбы′лʼе ми′нутыɣындъ.</ta>
            <ta e="T10" id="Seg_107" s="T8">ма̄ттъ ′сӓ̄рɣулуваттъ.</ta>
            <ta e="T16" id="Seg_108" s="T10">на си пӱ kыңгымбаттъ, ′ассъ ′тоɣълджигу.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T2" id="Seg_109" s="T0">nɨnɨŋa qɨŋ(g)ɨmba.</ta>
            <ta e="T4" id="Seg_110" s="T2">paj, paj!</ta>
            <ta e="T8" id="Seg_111" s="T4">ponä tšanǯo sombɨlʼe minutɨɣɨndə.</ta>
            <ta e="T10" id="Seg_112" s="T8">maːttə säːrɣuluvattə.</ta>
            <ta e="T16" id="Seg_113" s="T10">na si pü qɨŋgɨmbattə, assə toɣəlǯigu.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2" id="Seg_114" s="T0">nɨnɨŋa qɨŋɨmba. </ta>
            <ta e="T4" id="Seg_115" s="T2">paj, paj! </ta>
            <ta e="T8" id="Seg_116" s="T4">ponä čanǯo sombɨlʼe minutɨɣɨndə. </ta>
            <ta e="T10" id="Seg_117" s="T8">maːttə säːrɣuluwattə. </ta>
            <ta e="T16" id="Seg_118" s="T10">na si pü qɨŋgɨmbattə, assə toɣəlǯigu. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_119" s="T0">nɨnɨŋa</ta>
            <ta e="T2" id="Seg_120" s="T1">qɨŋɨ-mba</ta>
            <ta e="T3" id="Seg_121" s="T2">paj</ta>
            <ta e="T4" id="Seg_122" s="T3">paj</ta>
            <ta e="T5" id="Seg_123" s="T4">ponä</ta>
            <ta e="T6" id="Seg_124" s="T5">čanǯ-o</ta>
            <ta e="T7" id="Seg_125" s="T6">sombɨlʼe</ta>
            <ta e="T8" id="Seg_126" s="T7">minutɨ-ɣɨndə</ta>
            <ta e="T9" id="Seg_127" s="T8">maːt-tə</ta>
            <ta e="T10" id="Seg_128" s="T9">säːrɣu-lu-wa-ttə</ta>
            <ta e="T11" id="Seg_129" s="T10">na</ta>
            <ta e="T12" id="Seg_130" s="T11">si</ta>
            <ta e="T13" id="Seg_131" s="T12">pü</ta>
            <ta e="T14" id="Seg_132" s="T13">qɨŋgɨ-mba-ttə</ta>
            <ta e="T15" id="Seg_133" s="T14">assə</ta>
            <ta e="T16" id="Seg_134" s="T15">toɣə-lǯi-gu</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_135" s="T0">nɨnka</ta>
            <ta e="T2" id="Seg_136" s="T1">qɨŋɨ-mbɨ</ta>
            <ta e="T3" id="Seg_137" s="T2">paj</ta>
            <ta e="T4" id="Seg_138" s="T3">paj</ta>
            <ta e="T5" id="Seg_139" s="T4">poːne</ta>
            <ta e="T6" id="Seg_140" s="T5">čanǯɨ-ut</ta>
            <ta e="T7" id="Seg_141" s="T6">sombɨlʼe</ta>
            <ta e="T8" id="Seg_142" s="T7">minutɨ-qəntɨ</ta>
            <ta e="T9" id="Seg_143" s="T8">maːt-ndɨ</ta>
            <ta e="T10" id="Seg_144" s="T9">säːrqu-lɨ-ŋɨ-tɨt</ta>
            <ta e="T11" id="Seg_145" s="T10">na</ta>
            <ta e="T12" id="Seg_146" s="T11">si</ta>
            <ta e="T13" id="Seg_147" s="T12">püː</ta>
            <ta e="T14" id="Seg_148" s="T13">qɨŋɨ-mbɨ-tɨt</ta>
            <ta e="T15" id="Seg_149" s="T14">assɨ</ta>
            <ta e="T16" id="Seg_150" s="T15">toːqo-lʼčǝ-gu</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_151" s="T0">mosquito.[NOM]</ta>
            <ta e="T2" id="Seg_152" s="T1">buzz-DUR.[3SG.S]</ta>
            <ta e="T3" id="Seg_153" s="T2">aw</ta>
            <ta e="T4" id="Seg_154" s="T3">aw</ta>
            <ta e="T5" id="Seg_155" s="T4">outwards</ta>
            <ta e="T6" id="Seg_156" s="T5">go.out-1PL</ta>
            <ta e="T7" id="Seg_157" s="T6">five</ta>
            <ta e="T8" id="Seg_158" s="T7">minute-ILL.3SG</ta>
            <ta e="T9" id="Seg_159" s="T8">house-ILL</ta>
            <ta e="T10" id="Seg_160" s="T9">creep-RES-CO-3PL</ta>
            <ta e="T11" id="Seg_161" s="T10">this</ta>
            <ta e="T12" id="Seg_162" s="T11">ash</ta>
            <ta e="T13" id="Seg_163" s="T12">botfly.[NOM]</ta>
            <ta e="T14" id="Seg_164" s="T13">buzz-DUR-3PL</ta>
            <ta e="T15" id="Seg_165" s="T14">NEG</ta>
            <ta e="T16" id="Seg_166" s="T15">count-PFV-INF</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_167" s="T0">комар.[NOM]</ta>
            <ta e="T2" id="Seg_168" s="T1">жужжать-DUR.[3SG.S]</ta>
            <ta e="T3" id="Seg_169" s="T2">ай</ta>
            <ta e="T4" id="Seg_170" s="T3">ай</ta>
            <ta e="T5" id="Seg_171" s="T4">наружу</ta>
            <ta e="T6" id="Seg_172" s="T5">выйти-1PL</ta>
            <ta e="T7" id="Seg_173" s="T6">пять</ta>
            <ta e="T8" id="Seg_174" s="T7">минута-ILL.3SG</ta>
            <ta e="T9" id="Seg_175" s="T8">дом-ILL</ta>
            <ta e="T10" id="Seg_176" s="T9">забраться-RES-CO-3PL</ta>
            <ta e="T11" id="Seg_177" s="T10">этот</ta>
            <ta e="T12" id="Seg_178" s="T11">пепел</ta>
            <ta e="T13" id="Seg_179" s="T12">оводы.[NOM]</ta>
            <ta e="T14" id="Seg_180" s="T13">жужжать-DUR-3PL</ta>
            <ta e="T15" id="Seg_181" s="T14">NEG</ta>
            <ta e="T16" id="Seg_182" s="T15">считать-PFV-INF</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_183" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_184" s="T1">v-v&gt;v-v:pn</ta>
            <ta e="T3" id="Seg_185" s="T2">interj</ta>
            <ta e="T4" id="Seg_186" s="T3">interj</ta>
            <ta e="T5" id="Seg_187" s="T4">adv</ta>
            <ta e="T6" id="Seg_188" s="T5">v-v:pn</ta>
            <ta e="T7" id="Seg_189" s="T6">num</ta>
            <ta e="T8" id="Seg_190" s="T7">n-n:case.poss</ta>
            <ta e="T9" id="Seg_191" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_192" s="T9">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T11" id="Seg_193" s="T10">dem</ta>
            <ta e="T12" id="Seg_194" s="T11">n</ta>
            <ta e="T13" id="Seg_195" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_196" s="T13">v-v&gt;v-v:pn</ta>
            <ta e="T15" id="Seg_197" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_198" s="T15">v-v&gt;v-v:inf</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_199" s="T0">n</ta>
            <ta e="T2" id="Seg_200" s="T1">v</ta>
            <ta e="T3" id="Seg_201" s="T2">n</ta>
            <ta e="T4" id="Seg_202" s="T3">n</ta>
            <ta e="T5" id="Seg_203" s="T4">adv</ta>
            <ta e="T6" id="Seg_204" s="T5">v</ta>
            <ta e="T7" id="Seg_205" s="T6">num</ta>
            <ta e="T8" id="Seg_206" s="T7">n</ta>
            <ta e="T9" id="Seg_207" s="T8">n</ta>
            <ta e="T10" id="Seg_208" s="T9">v</ta>
            <ta e="T11" id="Seg_209" s="T10">dem</ta>
            <ta e="T12" id="Seg_210" s="T11">n</ta>
            <ta e="T13" id="Seg_211" s="T12">n</ta>
            <ta e="T14" id="Seg_212" s="T13">v</ta>
            <ta e="T15" id="Seg_213" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_214" s="T15">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_215" s="T0">np:S</ta>
            <ta e="T2" id="Seg_216" s="T1">v:pred</ta>
            <ta e="T6" id="Seg_217" s="T5">0.1.h:S v:pred</ta>
            <ta e="T10" id="Seg_218" s="T9">0.3:S v:pred</ta>
            <ta e="T13" id="Seg_219" s="T12">np:S</ta>
            <ta e="T14" id="Seg_220" s="T13">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_221" s="T0">np:A</ta>
            <ta e="T5" id="Seg_222" s="T4">np:G</ta>
            <ta e="T6" id="Seg_223" s="T5">0.1.h:A</ta>
            <ta e="T9" id="Seg_224" s="T8">np:G</ta>
            <ta e="T10" id="Seg_225" s="T9">0.3:A</ta>
            <ta e="T13" id="Seg_226" s="T12">np:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T2" id="Seg_227" s="T0">Комары жужжат.</ta>
            <ta e="T4" id="Seg_228" s="T2">Ай, ай.</ta>
            <ta e="T8" id="Seg_229" s="T4">Мы вышли на улицу на пять минут.</ta>
            <ta e="T10" id="Seg_230" s="T8">[Комары] залетели в дом.</ta>
            <ta e="T16" id="Seg_231" s="T10">Столько паутов жужжат, не сосчитать.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T2" id="Seg_232" s="T0">The mosquitoes are buzzing.</ta>
            <ta e="T4" id="Seg_233" s="T2">Ay, ah.</ta>
            <ta e="T8" id="Seg_234" s="T4">We went outside for five minutes.</ta>
            <ta e="T10" id="Seg_235" s="T8">[the mosquitoes] flew into the house.</ta>
            <ta e="T16" id="Seg_236" s="T10">There are so many fly, then you can not count them.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2" id="Seg_237" s="T0">Die Mücken brummen.</ta>
            <ta e="T4" id="Seg_238" s="T2">Ja, ah.</ta>
            <ta e="T8" id="Seg_239" s="T4">Wir gingen für fünf Minuten nach draußen.</ta>
            <ta e="T10" id="Seg_240" s="T8">[Die Mücken] flogen ins Haus.</ta>
            <ta e="T16" id="Seg_241" s="T10">Es gibt so viele Fliege, dann man sie nicht zählen kann.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T2" id="Seg_242" s="T0">комары жужжат</ta>
            <ta e="T4" id="Seg_243" s="T2">ай ай</ta>
            <ta e="T8" id="Seg_244" s="T4">на улицу вышли на пять минут</ta>
            <ta e="T10" id="Seg_245" s="T8">домой вошли (залетели)</ta>
            <ta e="T16" id="Seg_246" s="T10">столько паутов жужжат не сосчитать (счесть)</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
