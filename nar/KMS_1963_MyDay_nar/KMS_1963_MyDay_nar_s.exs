<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>_KMS_1963_MyDay_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KMS_1963_MyDay_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">288</ud-information>
            <ud-information attribute-name="# HIAT:w">210</ud-information>
            <ud-information attribute-name="# e">210</ud-information>
            <ud-information attribute-name="# HIAT:u">37</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KMS">
            <abbreviation>KMS</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T210" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KMS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T209" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">üdəmɨn</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">avɨrlʼevlʼe</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">kučalʼlʼe</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">qoːndäjeŋ</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">sittaŋ</ts>
                  <nts id="Seg_20" n="HIAT:ip">,</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">qamɣɨneŋ</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">qonɨŋ</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">taqqulɨmbattə</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">nɨnɨŋala</ts>
                  <nts id="Seg_33" n="HIAT:ip">.</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_36" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">ippaŋ</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">onäŋ</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">täːrbaŋ</ts>
                  <nts id="Seg_45" n="HIAT:ip">,</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">kundar</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">qamɣəneŋ</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">säːrɣulumbattə</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">nɨnɨŋala</ts>
                  <nts id="Seg_58" n="HIAT:ip">.</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_61" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_63" n="HIAT:w" s="T16">qaːwənbaːrɨːnda</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">čeːčun</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">ippɨlʼe</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">manǯeːan</ts>
                  <nts id="Seg_73" n="HIAT:ip">,</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">qaːwɨn</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">uːgoː</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">näːrgumba</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_86" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">kɨba</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">iːkaw</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">nimdə</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">täbɨn</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_100" n="HIAT:w" s="T27">Vova</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_103" n="HIAT:w" s="T28">soːɣɨndʼäŋ</ts>
                  <nts id="Seg_104" n="HIAT:ip">:</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">tan</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">nɨškɨlbal</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_112" n="HIAT:ip">(</nts>
                  <ts e="T32" id="Seg_114" n="HIAT:w" s="T31">qaːwɨm</ts>
                  <nts id="Seg_115" n="HIAT:ip">)</nts>
                  <nts id="Seg_116" n="HIAT:ip">?</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_119" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_121" n="HIAT:w" s="T32">täp</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">meŋa</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_127" n="HIAT:w" s="T34">tʼärɨŋ</ts>
                  <nts id="Seg_128" n="HIAT:ip">:</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_131" n="HIAT:w" s="T35">assä</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_134" n="HIAT:w" s="T36">man</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_137" n="HIAT:w" s="T37">nɨškɨlbaw</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_141" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_143" n="HIAT:w" s="T38">täp</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_146" n="HIAT:w" s="T39">tʼärɨŋ</ts>
                  <nts id="Seg_147" n="HIAT:ip">:</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_150" n="HIAT:w" s="T40">oːppow</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_153" n="HIAT:w" s="T41">Katʼa</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_156" n="HIAT:w" s="T42">omdɨlʼe</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_159" n="HIAT:w" s="T43">qoptɨn</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_162" n="HIAT:w" s="T44">baːrondə</ts>
                  <nts id="Seg_163" n="HIAT:ip">,</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_166" n="HIAT:w" s="T45">täp</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_169" n="HIAT:w" s="T46">nɨškəlbat</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_173" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_175" n="HIAT:w" s="T47">näːkaɣäneŋ</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_178" n="HIAT:w" s="T48">soːɣɨnʼdʼäŋ</ts>
                  <nts id="Seg_179" n="HIAT:ip">:</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_182" n="HIAT:w" s="T49">tan</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_185" n="HIAT:w" s="T50">nɨškɨlbal</ts>
                  <nts id="Seg_186" n="HIAT:ip">?</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_189" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_191" n="HIAT:w" s="T51">täp</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_194" n="HIAT:w" s="T52">meŋa</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_197" n="HIAT:w" s="T53">tʼärɨn</ts>
                  <nts id="Seg_198" n="HIAT:ip">:</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_201" n="HIAT:w" s="T54">man</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_204" n="HIAT:w" s="T55">naj</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_207" n="HIAT:w" s="T56">assä</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_210" n="HIAT:w" s="T57">nɨškɨlguzaw</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_213" n="HIAT:w" s="T58">i</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_216" n="HIAT:w" s="T59">assä</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_219" n="HIAT:w" s="T60">tükkuzaŋ</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_222" n="HIAT:w" s="T61">qaːwɨn</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_225" n="HIAT:w" s="T62">qöndo</ts>
                  <nts id="Seg_226" n="HIAT:ip">.</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_229" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_231" n="HIAT:w" s="T63">nilʼdʼiŋ</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_234" n="HIAT:w" s="T64">man</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_237" n="HIAT:w" s="T65">i</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_240" n="HIAT:w" s="T66">qaːlaŋ</ts>
                  <nts id="Seg_241" n="HIAT:ip">.</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_244" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_246" n="HIAT:w" s="T67">assä</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_249" n="HIAT:w" s="T68">qosolǯaw</ts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_253" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_255" n="HIAT:w" s="T69">näjɣunʼäɣaneŋ</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_258" n="HIAT:w" s="T70">tʼäraŋ</ts>
                  <nts id="Seg_259" n="HIAT:ip">:</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_262" n="HIAT:w" s="T71">süːttə</ts>
                  <nts id="Seg_263" n="HIAT:ip">.</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_266" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_268" n="HIAT:w" s="T72">täp</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_271" n="HIAT:w" s="T73">süːnnat</ts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_275" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_277" n="HIAT:w" s="T74">man</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_280" n="HIAT:w" s="T75">aj</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_283" n="HIAT:w" s="T76">kučalʼe</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_286" n="HIAT:w" s="T77">qondäaŋ</ts>
                  <nts id="Seg_287" n="HIAT:ip">.</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_290" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_292" n="HIAT:w" s="T78">qarimɨɣən</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_295" n="HIAT:w" s="T79">tättə</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_298" n="HIAT:w" s="T80">časoɣɨndə</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_301" n="HIAT:w" s="T81">kuttä-dakka</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_304" n="HIAT:w" s="T82">maːtam</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_307" n="HIAT:w" s="T83">lugornɨt</ts>
                  <nts id="Seg_308" n="HIAT:ip">.</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_311" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_313" n="HIAT:w" s="T84">näjɣunʼäw</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_316" n="HIAT:w" s="T85">väzɨŋ</ts>
                  <nts id="Seg_317" n="HIAT:ip">,</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_320" n="HIAT:w" s="T86">soːɣɨndiŋ</ts>
                  <nts id="Seg_321" n="HIAT:ip">:</nts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_324" n="HIAT:w" s="T87">kuttä</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_327" n="HIAT:w" s="T88">nɨtʼän</ts>
                  <nts id="Seg_328" n="HIAT:ip">.</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_331" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_333" n="HIAT:w" s="T89">iːwə</ts>
                  <nts id="Seg_334" n="HIAT:ip">,</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_337" n="HIAT:w" s="T90">nimdə</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_340" n="HIAT:w" s="T91">Мihail</ts>
                  <nts id="Seg_341" n="HIAT:ip">,</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_344" n="HIAT:w" s="T92">tʼärɨn</ts>
                  <nts id="Seg_345" n="HIAT:ip">:</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_348" n="HIAT:w" s="T93">qalman</ts>
                  <nts id="Seg_349" n="HIAT:ip">,</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_352" n="HIAT:w" s="T94">awa</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_354" n="HIAT:ip">(</nts>
                  <ts e="T96" id="Seg_356" n="HIAT:w" s="T95">üːttə</ts>
                  <nts id="Seg_357" n="HIAT:ip">)</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_360" n="HIAT:w" s="T96">awa</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_363" n="HIAT:w" s="T97">nʼüːdə</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_366" n="HIAT:w" s="T98">matta</ts>
                  <nts id="Seg_367" n="HIAT:ip">.</nts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_370" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_372" n="HIAT:w" s="T99">man</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_375" n="HIAT:w" s="T100">čaːǯindaŋ</ts>
                  <nts id="Seg_376" n="HIAT:ip">.</nts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_379" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_381" n="HIAT:w" s="T101">näjɣunʼäw</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_384" n="HIAT:w" s="T102">maːtam</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_387" n="HIAT:w" s="T103">nüːut</ts>
                  <nts id="Seg_388" n="HIAT:ip">.</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_391" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_393" n="HIAT:w" s="T104">iːwə</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_396" n="HIAT:w" s="T105">säːrnä</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_399" n="HIAT:w" s="T106">maːttə</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_402" n="HIAT:w" s="T107">ondɨ</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_405" n="HIAT:w" s="T108">lʼäɣandɨze</ts>
                  <nts id="Seg_406" n="HIAT:ip">.</nts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_409" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_411" n="HIAT:w" s="T109">meŋnan</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_414" n="HIAT:w" s="T110">soɣɨnʼdʼikuŋ</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_417" n="HIAT:w" s="T111">iːwə</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_420" n="HIAT:w" s="T112">anduŋgo</ts>
                  <nts id="Seg_421" n="HIAT:ip">,</nts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_424" n="HIAT:w" s="T113">maːdərnə</ts>
                  <nts id="Seg_425" n="HIAT:ip">:</nts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_428" n="HIAT:w" s="T114">lʼäɣaw</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_431" n="HIAT:w" s="T115">qwängu</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_434" n="HIAT:w" s="T116">piːrandə</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_436" n="HIAT:ip">(</nts>
                  <ts e="T118" id="Seg_438" n="HIAT:w" s="T117">piːraj</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_441" n="HIAT:w" s="T118">äːtondə</ts>
                  <nts id="Seg_442" n="HIAT:ip">)</nts>
                  <nts id="Seg_443" n="HIAT:ip">.</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_446" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_448" n="HIAT:w" s="T119">man</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_451" n="HIAT:w" s="T120">tʼäraŋ</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_454" n="HIAT:w" s="T121">täbɨnni</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_457" n="HIAT:w" s="T122">iːɣəneŋ</ts>
                  <nts id="Seg_458" n="HIAT:ip">:</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_461" n="HIAT:w" s="T123">iːmat</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_464" n="HIAT:w" s="T124">miːttə</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_467" n="HIAT:w" s="T125">aːndum</ts>
                  <nts id="Seg_468" n="HIAT:ip">.</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_471" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_473" n="HIAT:w" s="T126">nännə</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_475" n="HIAT:ip">(</nts>
                  <ts e="T128" id="Seg_477" n="HIAT:w" s="T127">nännɨdoː</ts>
                  <nts id="Seg_478" n="HIAT:ip">)</nts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_481" n="HIAT:w" s="T128">man</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_484" n="HIAT:w" s="T129">assä</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_487" n="HIAT:w" s="T130">qondəzaŋ</ts>
                  <nts id="Seg_488" n="HIAT:ip">.</nts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_491" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_493" n="HIAT:w" s="T131">qwännaŋ</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_496" n="HIAT:w" s="T132">manǯimbegu</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_499" n="HIAT:w" s="T133">poŋgɨm</ts>
                  <nts id="Seg_500" n="HIAT:ip">.</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_503" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_505" n="HIAT:w" s="T134">amgu</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_508" n="HIAT:w" s="T135">orannaw</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_511" n="HIAT:w" s="T136">poŋgoɣɨn</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_514" n="HIAT:w" s="T137">qwälɨm</ts>
                  <nts id="Seg_515" n="HIAT:ip">.</nts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_518" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_520" n="HIAT:w" s="T138">tüːaŋ</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_523" n="HIAT:w" s="T139">maːtqɨneŋ</ts>
                  <nts id="Seg_524" n="HIAT:ip">,</nts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_527" n="HIAT:w" s="T140">aːvvɨreeŋ</ts>
                  <nts id="Seg_528" n="HIAT:ip">,</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_531" n="HIAT:w" s="T141">nännä</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_534" n="HIAT:w" s="T142">püːdokuzaw</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_537" n="HIAT:w" s="T143">poŋgɨm</ts>
                  <nts id="Seg_538" n="HIAT:ip">.</nts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_541" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_543" n="HIAT:w" s="T144">köːdizaŋ</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_546" n="HIAT:w" s="T145">počtəndə</ts>
                  <nts id="Seg_547" n="HIAT:ip">,</nts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_550" n="HIAT:w" s="T146">okkə</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_553" n="HIAT:w" s="T147">nägər</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_556" n="HIAT:w" s="T148">iːndaŋ</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_559" n="HIAT:w" s="T149">ispolkomɣɨnnä</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_562" n="HIAT:w" s="T150">tüːmbädə</ts>
                  <nts id="Seg_563" n="HIAT:ip">.</nts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_566" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_568" n="HIAT:w" s="T151">nännə</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_571" n="HIAT:w" s="T152">särnaŋ</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_574" n="HIAT:w" s="T153">sovettə</ts>
                  <nts id="Seg_575" n="HIAT:ip">.</nts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_578" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_580" n="HIAT:w" s="T154">oldo</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_583" n="HIAT:w" s="T155">laqqɨlʼe</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_586" n="HIAT:w" s="T156">tümbədi</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_589" n="HIAT:w" s="T157">pajazä</ts>
                  <nts id="Seg_590" n="HIAT:ip">.</nts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_593" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_595" n="HIAT:w" s="T158">nimdə</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_598" n="HIAT:w" s="T159">täbɨn</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_601" n="HIAT:w" s="T160">Аngelina</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_604" n="HIAT:w" s="T161">Ivanovna</ts>
                  <nts id="Seg_605" n="HIAT:ip">.</nts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_608" n="HIAT:u" s="T162">
                  <ts e="T163" id="Seg_610" n="HIAT:w" s="T162">čenʼčuzo</ts>
                  <nts id="Seg_611" n="HIAT:ip">,</nts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_614" n="HIAT:w" s="T163">nägɨnǯizoː</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_617" n="HIAT:w" s="T164">süsüɣulʼ</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_620" n="HIAT:w" s="T210">dʼe</ts>
                  <nts id="Seg_621" n="HIAT:ip">.</nts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_624" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_626" n="HIAT:w" s="T165">nilʼdʼiŋa</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_629" n="HIAT:w" s="T166">mandɨziːkuzattə</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_632" n="HIAT:w" s="T167">tʼeːlɨlat</ts>
                  <nts id="Seg_633" n="HIAT:ip">,</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_636" n="HIAT:w" s="T168">qaːrɨnnɨ</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_638" n="HIAT:ip">(</nts>
                  <ts e="T170" id="Seg_640" n="HIAT:w" s="T169">qariːmɨɣɨnnɨ</ts>
                  <nts id="Seg_641" n="HIAT:ip">)</nts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_644" n="HIAT:w" s="T170">üːdɨmɨnǯenno</ts>
                  <nts id="Seg_645" n="HIAT:ip">.</nts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_648" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_650" n="HIAT:w" s="T171">laɣəkuzo</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_653" n="HIAT:w" s="T172">köt</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_656" n="HIAT:w" s="T173">okkɨr</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_659" n="HIAT:w" s="T174">qwäj</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_662" n="HIAT:w" s="T175">köt</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_665" n="HIAT:w" s="T176">časɨn</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_668" n="HIAT:w" s="T177">tʼelʼeɣɨndə</ts>
                  <nts id="Seg_669" n="HIAT:ip">.</nts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T185" id="Seg_672" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_674" n="HIAT:w" s="T178">süsügu</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_677" n="HIAT:w" s="T179">irala</ts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_680" n="HIAT:w" s="T180">kodɨt</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_683" n="HIAT:w" s="T181">qassaktə</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_686" n="HIAT:w" s="T182">assä</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_689" n="HIAT:w" s="T183">aːmdenǯiŋ</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_692" n="HIAT:w" s="T184">nagoːnɨŋ</ts>
                  <nts id="Seg_693" n="HIAT:ip">.</nts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_696" n="HIAT:u" s="T185">
                  <ts e="T186" id="Seg_698" n="HIAT:w" s="T185">täpɨla</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_701" n="HIAT:w" s="T186">aːmdəqwattə</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_704" n="HIAT:w" s="T187">assä</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_707" n="HIAT:w" s="T188">qoːnaŋ</ts>
                  <nts id="Seg_708" n="HIAT:ip">,</nts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_711" n="HIAT:w" s="T189">qöːdʼättə</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_714" n="HIAT:w" s="T190">sappɨɣərɨgu</ts>
                  <nts id="Seg_715" n="HIAT:ip">,</nts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_718" n="HIAT:w" s="T191">poŋgɨrgu</ts>
                  <nts id="Seg_719" n="HIAT:ip">,</nts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_722" n="HIAT:w" s="T192">mandujugu</ts>
                  <nts id="Seg_723" n="HIAT:ip">.</nts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T197" id="Seg_726" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_728" n="HIAT:w" s="T193">täppɨlane</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_731" n="HIAT:w" s="T194">nitʼän</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_734" n="HIAT:w" s="T195">soː</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_737" n="HIAT:w" s="T196">jeŋ</ts>
                  <nts id="Seg_738" n="HIAT:ip">.</nts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_741" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_743" n="HIAT:w" s="T197">nɨnɨŋa</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_746" n="HIAT:w" s="T198">oːttanendi</ts>
                  <nts id="Seg_747" n="HIAT:ip">,</nts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_750" n="HIAT:w" s="T199">sɨppa</ts>
                  <nts id="Seg_751" n="HIAT:ip">,</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_754" n="HIAT:w" s="T200">püː</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_757" n="HIAT:w" s="T201">aːmniattə</ts>
                  <nts id="Seg_758" n="HIAT:ip">,</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_761" n="HIAT:w" s="T202">assä</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_764" n="HIAT:w" s="T203">aːmdəgu</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_767" n="HIAT:w" s="T204">nägɨnǯigu</ts>
                  <nts id="Seg_768" n="HIAT:ip">,</nts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_771" n="HIAT:w" s="T205">toːrenǯigu</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_774" n="HIAT:w" s="T206">i</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_777" n="HIAT:w" s="T207">qätkugu</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_780" n="HIAT:w" s="T208">süsügäːǯim</ts>
                  <nts id="Seg_781" n="HIAT:ip">.</nts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T209" id="Seg_783" n="sc" s="T0">
               <ts e="T1" id="Seg_785" n="e" s="T0">üdəmɨn </ts>
               <ts e="T2" id="Seg_787" n="e" s="T1">avɨrlʼevlʼe </ts>
               <ts e="T3" id="Seg_789" n="e" s="T2">kučalʼlʼe </ts>
               <ts e="T4" id="Seg_791" n="e" s="T3">qoːndäjeŋ. </ts>
               <ts e="T5" id="Seg_793" n="e" s="T4">sittaŋ, </ts>
               <ts e="T6" id="Seg_795" n="e" s="T5">qamɣɨneŋ </ts>
               <ts e="T7" id="Seg_797" n="e" s="T6">qonɨŋ </ts>
               <ts e="T8" id="Seg_799" n="e" s="T7">taqqulɨmbattə </ts>
               <ts e="T9" id="Seg_801" n="e" s="T8">nɨnɨŋala. </ts>
               <ts e="T10" id="Seg_803" n="e" s="T9">ippaŋ </ts>
               <ts e="T11" id="Seg_805" n="e" s="T10">onäŋ </ts>
               <ts e="T12" id="Seg_807" n="e" s="T11">täːrbaŋ, </ts>
               <ts e="T13" id="Seg_809" n="e" s="T12">kundar </ts>
               <ts e="T14" id="Seg_811" n="e" s="T13">qamɣəneŋ </ts>
               <ts e="T15" id="Seg_813" n="e" s="T14">säːrɣulumbattə </ts>
               <ts e="T16" id="Seg_815" n="e" s="T15">nɨnɨŋala. </ts>
               <ts e="T17" id="Seg_817" n="e" s="T16">qaːwənbaːrɨːnda </ts>
               <ts e="T18" id="Seg_819" n="e" s="T17">čeːčun </ts>
               <ts e="T19" id="Seg_821" n="e" s="T18">ippɨlʼe </ts>
               <ts e="T20" id="Seg_823" n="e" s="T19">manǯeːan, </ts>
               <ts e="T21" id="Seg_825" n="e" s="T20">qaːwɨn </ts>
               <ts e="T22" id="Seg_827" n="e" s="T21">uːgoː </ts>
               <ts e="T23" id="Seg_829" n="e" s="T22">näːrgumba. </ts>
               <ts e="T24" id="Seg_831" n="e" s="T23">kɨba </ts>
               <ts e="T25" id="Seg_833" n="e" s="T24">iːkaw </ts>
               <ts e="T26" id="Seg_835" n="e" s="T25">nimdə </ts>
               <ts e="T27" id="Seg_837" n="e" s="T26">täbɨn </ts>
               <ts e="T28" id="Seg_839" n="e" s="T27">Vova </ts>
               <ts e="T29" id="Seg_841" n="e" s="T28">soːɣɨndʼäŋ: </ts>
               <ts e="T30" id="Seg_843" n="e" s="T29">tan </ts>
               <ts e="T31" id="Seg_845" n="e" s="T30">nɨškɨlbal </ts>
               <ts e="T32" id="Seg_847" n="e" s="T31">(qaːwɨm)? </ts>
               <ts e="T33" id="Seg_849" n="e" s="T32">täp </ts>
               <ts e="T34" id="Seg_851" n="e" s="T33">meŋa </ts>
               <ts e="T35" id="Seg_853" n="e" s="T34">tʼärɨŋ: </ts>
               <ts e="T36" id="Seg_855" n="e" s="T35">assä </ts>
               <ts e="T37" id="Seg_857" n="e" s="T36">man </ts>
               <ts e="T38" id="Seg_859" n="e" s="T37">nɨškɨlbaw. </ts>
               <ts e="T39" id="Seg_861" n="e" s="T38">täp </ts>
               <ts e="T40" id="Seg_863" n="e" s="T39">tʼärɨŋ: </ts>
               <ts e="T41" id="Seg_865" n="e" s="T40">oːppow </ts>
               <ts e="T42" id="Seg_867" n="e" s="T41">Katʼa </ts>
               <ts e="T43" id="Seg_869" n="e" s="T42">omdɨlʼe </ts>
               <ts e="T44" id="Seg_871" n="e" s="T43">qoptɨn </ts>
               <ts e="T45" id="Seg_873" n="e" s="T44">baːrondə, </ts>
               <ts e="T46" id="Seg_875" n="e" s="T45">täp </ts>
               <ts e="T47" id="Seg_877" n="e" s="T46">nɨškəlbat. </ts>
               <ts e="T48" id="Seg_879" n="e" s="T47">näːkaɣäneŋ </ts>
               <ts e="T49" id="Seg_881" n="e" s="T48">soːɣɨnʼdʼäŋ: </ts>
               <ts e="T50" id="Seg_883" n="e" s="T49">tan </ts>
               <ts e="T51" id="Seg_885" n="e" s="T50">nɨškɨlbal? </ts>
               <ts e="T52" id="Seg_887" n="e" s="T51">täp </ts>
               <ts e="T53" id="Seg_889" n="e" s="T52">meŋa </ts>
               <ts e="T54" id="Seg_891" n="e" s="T53">tʼärɨn: </ts>
               <ts e="T55" id="Seg_893" n="e" s="T54">man </ts>
               <ts e="T56" id="Seg_895" n="e" s="T55">naj </ts>
               <ts e="T57" id="Seg_897" n="e" s="T56">assä </ts>
               <ts e="T58" id="Seg_899" n="e" s="T57">nɨškɨlguzaw </ts>
               <ts e="T59" id="Seg_901" n="e" s="T58">i </ts>
               <ts e="T60" id="Seg_903" n="e" s="T59">assä </ts>
               <ts e="T61" id="Seg_905" n="e" s="T60">tükkuzaŋ </ts>
               <ts e="T62" id="Seg_907" n="e" s="T61">qaːwɨn </ts>
               <ts e="T63" id="Seg_909" n="e" s="T62">qöndo. </ts>
               <ts e="T64" id="Seg_911" n="e" s="T63">nilʼdʼiŋ </ts>
               <ts e="T65" id="Seg_913" n="e" s="T64">man </ts>
               <ts e="T66" id="Seg_915" n="e" s="T65">i </ts>
               <ts e="T67" id="Seg_917" n="e" s="T66">qaːlaŋ. </ts>
               <ts e="T68" id="Seg_919" n="e" s="T67">assä </ts>
               <ts e="T69" id="Seg_921" n="e" s="T68">qosolǯaw. </ts>
               <ts e="T70" id="Seg_923" n="e" s="T69">näjɣunʼäɣaneŋ </ts>
               <ts e="T71" id="Seg_925" n="e" s="T70">tʼäraŋ: </ts>
               <ts e="T72" id="Seg_927" n="e" s="T71">süːttə. </ts>
               <ts e="T73" id="Seg_929" n="e" s="T72">täp </ts>
               <ts e="T74" id="Seg_931" n="e" s="T73">süːnnat. </ts>
               <ts e="T75" id="Seg_933" n="e" s="T74">man </ts>
               <ts e="T76" id="Seg_935" n="e" s="T75">aj </ts>
               <ts e="T77" id="Seg_937" n="e" s="T76">kučalʼe </ts>
               <ts e="T78" id="Seg_939" n="e" s="T77">qondäaŋ. </ts>
               <ts e="T79" id="Seg_941" n="e" s="T78">qarimɨɣən </ts>
               <ts e="T80" id="Seg_943" n="e" s="T79">tättə </ts>
               <ts e="T81" id="Seg_945" n="e" s="T80">časoɣɨndə </ts>
               <ts e="T82" id="Seg_947" n="e" s="T81">kuttä-dakka </ts>
               <ts e="T83" id="Seg_949" n="e" s="T82">maːtam </ts>
               <ts e="T84" id="Seg_951" n="e" s="T83">lugornɨt. </ts>
               <ts e="T85" id="Seg_953" n="e" s="T84">näjɣunʼäw </ts>
               <ts e="T86" id="Seg_955" n="e" s="T85">väzɨŋ, </ts>
               <ts e="T87" id="Seg_957" n="e" s="T86">soːɣɨndiŋ: </ts>
               <ts e="T88" id="Seg_959" n="e" s="T87">kuttä </ts>
               <ts e="T89" id="Seg_961" n="e" s="T88">nɨtʼän. </ts>
               <ts e="T90" id="Seg_963" n="e" s="T89">iːwə, </ts>
               <ts e="T91" id="Seg_965" n="e" s="T90">nimdə </ts>
               <ts e="T92" id="Seg_967" n="e" s="T91">Мihail, </ts>
               <ts e="T93" id="Seg_969" n="e" s="T92">tʼärɨn: </ts>
               <ts e="T94" id="Seg_971" n="e" s="T93">qalman, </ts>
               <ts e="T95" id="Seg_973" n="e" s="T94">awa </ts>
               <ts e="T96" id="Seg_975" n="e" s="T95">(üːttə) </ts>
               <ts e="T97" id="Seg_977" n="e" s="T96">awa </ts>
               <ts e="T98" id="Seg_979" n="e" s="T97">nʼüːdə </ts>
               <ts e="T99" id="Seg_981" n="e" s="T98">matta. </ts>
               <ts e="T100" id="Seg_983" n="e" s="T99">man </ts>
               <ts e="T101" id="Seg_985" n="e" s="T100">čaːǯindaŋ. </ts>
               <ts e="T102" id="Seg_987" n="e" s="T101">näjɣunʼäw </ts>
               <ts e="T103" id="Seg_989" n="e" s="T102">maːtam </ts>
               <ts e="T104" id="Seg_991" n="e" s="T103">nüːut. </ts>
               <ts e="T105" id="Seg_993" n="e" s="T104">iːwə </ts>
               <ts e="T106" id="Seg_995" n="e" s="T105">säːrnä </ts>
               <ts e="T107" id="Seg_997" n="e" s="T106">maːttə </ts>
               <ts e="T108" id="Seg_999" n="e" s="T107">ondɨ </ts>
               <ts e="T109" id="Seg_1001" n="e" s="T108">lʼäɣandɨze. </ts>
               <ts e="T110" id="Seg_1003" n="e" s="T109">meŋnan </ts>
               <ts e="T111" id="Seg_1005" n="e" s="T110">soɣɨnʼdʼikuŋ </ts>
               <ts e="T112" id="Seg_1007" n="e" s="T111">iːwə </ts>
               <ts e="T113" id="Seg_1009" n="e" s="T112">anduŋgo, </ts>
               <ts e="T114" id="Seg_1011" n="e" s="T113">maːdərnə: </ts>
               <ts e="T115" id="Seg_1013" n="e" s="T114">lʼäɣaw </ts>
               <ts e="T116" id="Seg_1015" n="e" s="T115">qwängu </ts>
               <ts e="T117" id="Seg_1017" n="e" s="T116">piːrandə </ts>
               <ts e="T118" id="Seg_1019" n="e" s="T117">(piːraj </ts>
               <ts e="T119" id="Seg_1021" n="e" s="T118">äːtondə). </ts>
               <ts e="T120" id="Seg_1023" n="e" s="T119">man </ts>
               <ts e="T121" id="Seg_1025" n="e" s="T120">tʼäraŋ </ts>
               <ts e="T122" id="Seg_1027" n="e" s="T121">täbɨnni </ts>
               <ts e="T123" id="Seg_1029" n="e" s="T122">iːɣəneŋ: </ts>
               <ts e="T124" id="Seg_1031" n="e" s="T123">iːmat </ts>
               <ts e="T125" id="Seg_1033" n="e" s="T124">miːttə </ts>
               <ts e="T126" id="Seg_1035" n="e" s="T125">aːndum. </ts>
               <ts e="T127" id="Seg_1037" n="e" s="T126">nännə </ts>
               <ts e="T128" id="Seg_1039" n="e" s="T127">(nännɨdoː) </ts>
               <ts e="T129" id="Seg_1041" n="e" s="T128">man </ts>
               <ts e="T130" id="Seg_1043" n="e" s="T129">assä </ts>
               <ts e="T131" id="Seg_1045" n="e" s="T130">qondəzaŋ. </ts>
               <ts e="T132" id="Seg_1047" n="e" s="T131">qwännaŋ </ts>
               <ts e="T133" id="Seg_1049" n="e" s="T132">manǯimbegu </ts>
               <ts e="T134" id="Seg_1051" n="e" s="T133">poŋgɨm. </ts>
               <ts e="T135" id="Seg_1053" n="e" s="T134">amgu </ts>
               <ts e="T136" id="Seg_1055" n="e" s="T135">orannaw </ts>
               <ts e="T137" id="Seg_1057" n="e" s="T136">poŋgoɣɨn </ts>
               <ts e="T138" id="Seg_1059" n="e" s="T137">qwälɨm. </ts>
               <ts e="T139" id="Seg_1061" n="e" s="T138">tüːaŋ </ts>
               <ts e="T140" id="Seg_1063" n="e" s="T139">maːtqɨneŋ, </ts>
               <ts e="T141" id="Seg_1065" n="e" s="T140">aːvvɨreeŋ, </ts>
               <ts e="T142" id="Seg_1067" n="e" s="T141">nännä </ts>
               <ts e="T143" id="Seg_1069" n="e" s="T142">püːdokuzaw </ts>
               <ts e="T144" id="Seg_1071" n="e" s="T143">poŋgɨm. </ts>
               <ts e="T145" id="Seg_1073" n="e" s="T144">köːdizaŋ </ts>
               <ts e="T146" id="Seg_1075" n="e" s="T145">počtəndə, </ts>
               <ts e="T147" id="Seg_1077" n="e" s="T146">okkə </ts>
               <ts e="T148" id="Seg_1079" n="e" s="T147">nägər </ts>
               <ts e="T149" id="Seg_1081" n="e" s="T148">iːndaŋ </ts>
               <ts e="T150" id="Seg_1083" n="e" s="T149">ispolkomɣɨnnä </ts>
               <ts e="T151" id="Seg_1085" n="e" s="T150">tüːmbädə. </ts>
               <ts e="T152" id="Seg_1087" n="e" s="T151">nännə </ts>
               <ts e="T153" id="Seg_1089" n="e" s="T152">särnaŋ </ts>
               <ts e="T154" id="Seg_1091" n="e" s="T153">sovettə. </ts>
               <ts e="T155" id="Seg_1093" n="e" s="T154">oldo </ts>
               <ts e="T156" id="Seg_1095" n="e" s="T155">laqqɨlʼe </ts>
               <ts e="T157" id="Seg_1097" n="e" s="T156">tümbədi </ts>
               <ts e="T158" id="Seg_1099" n="e" s="T157">pajazä. </ts>
               <ts e="T159" id="Seg_1101" n="e" s="T158">nimdə </ts>
               <ts e="T160" id="Seg_1103" n="e" s="T159">täbɨn </ts>
               <ts e="T161" id="Seg_1105" n="e" s="T160">Аngelina </ts>
               <ts e="T162" id="Seg_1107" n="e" s="T161">Ivanovna. </ts>
               <ts e="T163" id="Seg_1109" n="e" s="T162">čenʼčuzo, </ts>
               <ts e="T164" id="Seg_1111" n="e" s="T163">nägɨnǯizoː </ts>
               <ts e="T210" id="Seg_1113" n="e" s="T164">süsüɣulʼ </ts>
               <ts e="T165" id="Seg_1115" n="e" s="T210">dʼe. </ts>
               <ts e="T166" id="Seg_1117" n="e" s="T165">nilʼdʼiŋa </ts>
               <ts e="T167" id="Seg_1119" n="e" s="T166">mandɨziːkuzattə </ts>
               <ts e="T168" id="Seg_1121" n="e" s="T167">tʼeːlɨlat, </ts>
               <ts e="T169" id="Seg_1123" n="e" s="T168">qaːrɨnnɨ </ts>
               <ts e="T170" id="Seg_1125" n="e" s="T169">(qariːmɨɣɨnnɨ) </ts>
               <ts e="T171" id="Seg_1127" n="e" s="T170">üːdɨmɨnǯenno. </ts>
               <ts e="T172" id="Seg_1129" n="e" s="T171">laɣəkuzo </ts>
               <ts e="T173" id="Seg_1131" n="e" s="T172">köt </ts>
               <ts e="T174" id="Seg_1133" n="e" s="T173">okkɨr </ts>
               <ts e="T175" id="Seg_1135" n="e" s="T174">qwäj </ts>
               <ts e="T176" id="Seg_1137" n="e" s="T175">köt </ts>
               <ts e="T177" id="Seg_1139" n="e" s="T176">časɨn </ts>
               <ts e="T178" id="Seg_1141" n="e" s="T177">tʼelʼeɣɨndə. </ts>
               <ts e="T179" id="Seg_1143" n="e" s="T178">süsügu </ts>
               <ts e="T180" id="Seg_1145" n="e" s="T179">irala </ts>
               <ts e="T181" id="Seg_1147" n="e" s="T180">kodɨt </ts>
               <ts e="T182" id="Seg_1149" n="e" s="T181">qassaktə </ts>
               <ts e="T183" id="Seg_1151" n="e" s="T182">assä </ts>
               <ts e="T184" id="Seg_1153" n="e" s="T183">aːmdenǯiŋ </ts>
               <ts e="T185" id="Seg_1155" n="e" s="T184">nagoːnɨŋ. </ts>
               <ts e="T186" id="Seg_1157" n="e" s="T185">täpɨla </ts>
               <ts e="T187" id="Seg_1159" n="e" s="T186">aːmdəqwattə </ts>
               <ts e="T188" id="Seg_1161" n="e" s="T187">assä </ts>
               <ts e="T189" id="Seg_1163" n="e" s="T188">qoːnaŋ, </ts>
               <ts e="T190" id="Seg_1165" n="e" s="T189">qöːdʼättə </ts>
               <ts e="T191" id="Seg_1167" n="e" s="T190">sappɨɣərɨgu, </ts>
               <ts e="T192" id="Seg_1169" n="e" s="T191">poŋgɨrgu, </ts>
               <ts e="T193" id="Seg_1171" n="e" s="T192">mandujugu. </ts>
               <ts e="T194" id="Seg_1173" n="e" s="T193">täppɨlane </ts>
               <ts e="T195" id="Seg_1175" n="e" s="T194">nitʼän </ts>
               <ts e="T196" id="Seg_1177" n="e" s="T195">soː </ts>
               <ts e="T197" id="Seg_1179" n="e" s="T196">jeŋ. </ts>
               <ts e="T198" id="Seg_1181" n="e" s="T197">nɨnɨŋa </ts>
               <ts e="T199" id="Seg_1183" n="e" s="T198">oːttanendi, </ts>
               <ts e="T200" id="Seg_1185" n="e" s="T199">sɨppa, </ts>
               <ts e="T201" id="Seg_1187" n="e" s="T200">püː </ts>
               <ts e="T202" id="Seg_1189" n="e" s="T201">aːmniattə, </ts>
               <ts e="T203" id="Seg_1191" n="e" s="T202">assä </ts>
               <ts e="T204" id="Seg_1193" n="e" s="T203">aːmdəgu </ts>
               <ts e="T205" id="Seg_1195" n="e" s="T204">nägɨnǯigu, </ts>
               <ts e="T206" id="Seg_1197" n="e" s="T205">toːrenǯigu </ts>
               <ts e="T207" id="Seg_1199" n="e" s="T206">i </ts>
               <ts e="T208" id="Seg_1201" n="e" s="T207">qätkugu </ts>
               <ts e="T209" id="Seg_1203" n="e" s="T208">süsügäːǯim. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_1204" s="T0">KMS_1963_MyDay_nar.001 (001.001)</ta>
            <ta e="T9" id="Seg_1205" s="T4">KMS_1963_MyDay_nar.002 (001.002)</ta>
            <ta e="T16" id="Seg_1206" s="T9">KMS_1963_MyDay_nar.003 (001.003)</ta>
            <ta e="T23" id="Seg_1207" s="T16">KMS_1963_MyDay_nar.004 (001.004)</ta>
            <ta e="T32" id="Seg_1208" s="T23">KMS_1963_MyDay_nar.005 (001.005)</ta>
            <ta e="T38" id="Seg_1209" s="T32">KMS_1963_MyDay_nar.006 (001.006)</ta>
            <ta e="T47" id="Seg_1210" s="T38">KMS_1963_MyDay_nar.007 (001.007)</ta>
            <ta e="T51" id="Seg_1211" s="T47">KMS_1963_MyDay_nar.008 (001.008)</ta>
            <ta e="T63" id="Seg_1212" s="T51">KMS_1963_MyDay_nar.009 (001.009)</ta>
            <ta e="T67" id="Seg_1213" s="T63">KMS_1963_MyDay_nar.010 (001.010)</ta>
            <ta e="T69" id="Seg_1214" s="T67">KMS_1963_MyDay_nar.011 (001.011)</ta>
            <ta e="T72" id="Seg_1215" s="T69">KMS_1963_MyDay_nar.012 (001.012)</ta>
            <ta e="T74" id="Seg_1216" s="T72">KMS_1963_MyDay_nar.013 (001.013)</ta>
            <ta e="T78" id="Seg_1217" s="T74">KMS_1963_MyDay_nar.014 (001.014)</ta>
            <ta e="T84" id="Seg_1218" s="T78">KMS_1963_MyDay_nar.015 (001.015)</ta>
            <ta e="T89" id="Seg_1219" s="T84">KMS_1963_MyDay_nar.016 (001.016)</ta>
            <ta e="T99" id="Seg_1220" s="T89">KMS_1963_MyDay_nar.017 (001.017)</ta>
            <ta e="T101" id="Seg_1221" s="T99">KMS_1963_MyDay_nar.018 (001.018)</ta>
            <ta e="T104" id="Seg_1222" s="T101">KMS_1963_MyDay_nar.019 (001.019)</ta>
            <ta e="T109" id="Seg_1223" s="T104">KMS_1963_MyDay_nar.020 (001.020)</ta>
            <ta e="T119" id="Seg_1224" s="T109">KMS_1963_MyDay_nar.021 (001.021)</ta>
            <ta e="T126" id="Seg_1225" s="T119">KMS_1963_MyDay_nar.022 (001.022)</ta>
            <ta e="T131" id="Seg_1226" s="T126">KMS_1963_MyDay_nar.023 (001.023)</ta>
            <ta e="T134" id="Seg_1227" s="T131">KMS_1963_MyDay_nar.024 (001.024)</ta>
            <ta e="T138" id="Seg_1228" s="T134">KMS_1963_MyDay_nar.025 (001.025)</ta>
            <ta e="T144" id="Seg_1229" s="T138">KMS_1963_MyDay_nar.026 (001.026)</ta>
            <ta e="T151" id="Seg_1230" s="T144">KMS_1963_MyDay_nar.027 (001.027)</ta>
            <ta e="T154" id="Seg_1231" s="T151">KMS_1963_MyDay_nar.028 (001.028)</ta>
            <ta e="T158" id="Seg_1232" s="T154">KMS_1963_MyDay_nar.029 (001.029)</ta>
            <ta e="T162" id="Seg_1233" s="T158">KMS_1963_MyDay_nar.030 (001.030)</ta>
            <ta e="T165" id="Seg_1234" s="T162">KMS_1963_MyDay_nar.031 (001.031)</ta>
            <ta e="T171" id="Seg_1235" s="T165">KMS_1963_MyDay_nar.032 (001.032)</ta>
            <ta e="T178" id="Seg_1236" s="T171">KMS_1963_MyDay_nar.033 (001.033)</ta>
            <ta e="T185" id="Seg_1237" s="T178">KMS_1963_MyDay_nar.034 (001.034)</ta>
            <ta e="T193" id="Seg_1238" s="T185">KMS_1963_MyDay_nar.035 (001.035)</ta>
            <ta e="T197" id="Seg_1239" s="T193">KMS_1963_MyDay_nar.036 (001.036)</ta>
            <ta e="T209" id="Seg_1240" s="T197">KMS_1963_MyDay_nar.037 (001.037)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_1241" s="T0">′ӱдъмын авыр′лʼевлʼе ку′тшалʼлʼе kо̄н′дӓjең.</ta>
            <ta e="T9" id="Seg_1242" s="T4">си′ттаң, ‵kамɣы′нең ′kоның ‵таkkулым′баттъ ‵ныны′ңала.</ta>
            <ta e="T16" id="Seg_1243" s="T9">′иппаң о′нӓң ′тӓ̄рбаң, кун′дар ‵kамɣъ′нең ′сӓ̄рɣулумбаттъ ныны′ңала.</ta>
            <ta e="T23" id="Seg_1244" s="T16">′kа̄wън′ба̄ры̄нда тше̄′тшун ′иппы′лʼе ‵ман′дже̄ан, kа̄′в(w)ын ′ӯго̄ ′нӓ̄ргумба.</ta>
            <ta e="T32" id="Seg_1245" s="T23">кы′ба ӣкаw ′нимдъ ′тӓбын Вова со̄ɣын′дʼӓң: тан ′нышкылбал (′kа̄wым)?</ta>
            <ta e="T38" id="Seg_1246" s="T32">тӓп ′меңа тʼӓ′рың: ′ассӓ ман ‵нышкыл′баw.</ta>
            <ta e="T47" id="Seg_1247" s="T38">тӓп тʼӓ′рың: о̄′ппоw Катя омды′лʼе ′kоптын ′ба̄ро(ы)ндъ, тӓп нышкъл′бат.</ta>
            <ta e="T51" id="Seg_1248" s="T47">′нӓ̄каɣӓнең со̄ɣынʼдʼӓң: тан ′нышкыл′бал?</ta>
            <ta e="T63" id="Seg_1249" s="T51">тӓп ′меңа тʼӓ′рын: ман ′най ′ассӓ ‵нышкылгу′заw и ′ассӓ ′тӱккузаң kа̄′wын ′kӧндо.</ta>
            <ta e="T67" id="Seg_1250" s="T63">нилʼ′дʼиң ман и kа̄′лаң.</ta>
            <ta e="T69" id="Seg_1251" s="T67">′ассӓ kо′солджаw.</ta>
            <ta e="T72" id="Seg_1252" s="T69">‵нӓйɣу′нʼӓɣане(ы)ң тʼӓ′раң: сӱ̄ттъ.</ta>
            <ta e="T74" id="Seg_1253" s="T72">тӓп ′сӱ̄нна(ъ)т.</ta>
            <ta e="T78" id="Seg_1254" s="T74">ман ай ку′тшалʼе kон′дӓ(е)аң.</ta>
            <ta e="T84" id="Seg_1255" s="T78">kа′римыɣън ′тӓттъ ′часоɣындъ ′куттӓ-′дакка ′ма̄там ′лугорныт.</ta>
            <ta e="T89" id="Seg_1256" s="T84">‵нӓйɣу′нʼӓw вӓ′зың, со̄ɣындиң: ′куттӓ ны′тʼӓн.</ta>
            <ta e="T99" id="Seg_1257" s="T89">′ӣвъ, ′нимдъ Михаил, тʼӓ′рын: kал′ман, а′ва (′ӱ̄ттъ) а′ва ′нʼӱ̄дъ матта.</ta>
            <ta e="T101" id="Seg_1258" s="T99">ман ′тша̄джиндаң.</ta>
            <ta e="T104" id="Seg_1259" s="T101">нӓйɣу′нʼӓw ′ма̄там ′нӱ̄ут.</ta>
            <ta e="T109" id="Seg_1260" s="T104">′ӣwъ ′сӓ̄рнӓ ′ма̄ттъ онды лʼӓ′ɣандызе.</ta>
            <ta e="T119" id="Seg_1261" s="T109">′меңнан соɣы(у)нʼдʼи′куң ′ӣwъ андуң′го, ма̄дърнъ лʼӓ′ɣаw kwӓнгу ′пӣрандъ (′пӣрай ′ӓ̄тондъ).</ta>
            <ta e="T126" id="Seg_1262" s="T119">ман тʼӓ′раң тӓбы′нни ′ӣɣъ‵нең: ӣ′мат мӣ′ттъ а̄н′дум.</ta>
            <ta e="T131" id="Seg_1263" s="T126">′нӓннъ (′нӓнныдо̄) ман ′ассӓ ′kондъзаң.</ta>
            <ta e="T134" id="Seg_1264" s="T131">kwӓ′ннаң манджимбе′гу поң′гым.</ta>
            <ta e="T138" id="Seg_1265" s="T134">ам′гу о′раннаw поң′гоɣын ′kwӓлым.</ta>
            <ta e="T144" id="Seg_1266" s="T138">′тӱ̄аң ма̄тkынең, а̄ввы′реең, ′нӓннӓ(ъ) ′пӱ̄докузаw ′поңг(ɣ)ым.</ta>
            <ta e="T151" id="Seg_1267" s="T144">′кӧ̄дизаң ′почтъндъ, ′оккъ ′нӓгър ′ӣндаң исполкомɣыннӓ ′тӱ̄мбӓдъ.</ta>
            <ta e="T154" id="Seg_1268" s="T151">′нӓннъ ′сӓрнаң со′веттъ.</ta>
            <ta e="T158" id="Seg_1269" s="T154">ол′до ‵лаkkы′лʼе ′тӱмбъди па′jазӓ.</ta>
            <ta e="T162" id="Seg_1270" s="T158">нимдъ ′тӓбын Ангелина Ивановна.</ta>
            <ta e="T165" id="Seg_1271" s="T162">′тшенʼтшузо, ‵нӓгынджизо̄ ′сӱсӱɣулʼдʼе.</ta>
            <ta e="T171" id="Seg_1272" s="T165">нилʼдʼи′ңа манды′зӣку′заттъ ′тʼе̄лылат, kа̄ры′нны (kа′рӣмыɣынны) ′ӱ̄дымын‵дженно(ъ).</ta>
            <ta e="T178" id="Seg_1273" s="T171">‵лаɣ(k)ъку′зо кӧт оккыр kwӓй гʼӧт ′часын ′тʼелʼеɣындъ.</ta>
            <ta e="T185" id="Seg_1274" s="T178">′сӱсӱг̂у и′рала ′кодыт kа′ссактъ ′ассӓ(ъ) ′а̄мденджиң на ′го̄ның.</ta>
            <ta e="T193" id="Seg_1275" s="T185">тӓп(п)ыла а̄мдъkwаттъ ′ассӓ ′kо̄наң, ′kӧ̄дʼӓттъ ‵саппыɣъры′гу, ‵поңгыр′гу, манду(й)jу′гу.</ta>
            <ta e="T197" id="Seg_1276" s="T193">тӓппы′лан(н)е ни′тʼӓн со̄ jең.</ta>
            <ta e="T209" id="Seg_1277" s="T197">ныны′ңа о̄′ттане(ӓ)нди, сы′ппа, ′пӱ̄ а̄′мниаттъ, ′ассӓ ′а̄мдъгу нӓгынджи′гу, то̄ренджигу и kӓтку′гу ′сӱсӱг(ɣ) ′ӓ̄джим.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_1278" s="T0">üdəmɨn avɨrlʼevlʼe kutšalʼlʼe qoːndäjeŋ.</ta>
            <ta e="T9" id="Seg_1279" s="T4">sittaŋ, qamɣɨneŋ qonɨŋ taqqulɨmbattə nɨnɨŋala.</ta>
            <ta e="T16" id="Seg_1280" s="T9">ippaŋ onäŋ täːrbaŋ, kundar qamɣəneŋ säːrɣulumbattə nɨnɨŋala.</ta>
            <ta e="T23" id="Seg_1281" s="T16">qaːwənbaːrɨːnda tšeːtšun ippɨlʼe manǯeːan, qaːv(w)ɨn uːgoː näːrgumba.</ta>
            <ta e="T32" id="Seg_1282" s="T23">kɨba iːkaw nimdə täbɨn Вova soːɣɨndʼäŋ: tan nɨškɨlbal (qaːwɨm)?</ta>
            <ta e="T38" id="Seg_1283" s="T32">täp meŋa tʼärɨŋ: assä man nɨškɨlbaw.</ta>
            <ta e="T47" id="Seg_1284" s="T38">täp tʼärɨŋ: oːppow Кatʼa omdɨlʼe qoptɨn baːro(ɨ)ndə, täp nɨškəlbat.</ta>
            <ta e="T51" id="Seg_1285" s="T47">näːkaɣäneŋ soːɣɨnʼdʼäŋ: tan nɨškɨlbal?</ta>
            <ta e="T63" id="Seg_1286" s="T51">täp meŋa tʼärɨn: man naj assä nɨškɨlguzaw i assä tükkuzaŋ qaːwɨn qöndo.</ta>
            <ta e="T67" id="Seg_1287" s="T63">nilʼdʼiŋ man i qaːlaŋ.</ta>
            <ta e="T69" id="Seg_1288" s="T67">assä qosolǯaw.</ta>
            <ta e="T72" id="Seg_1289" s="T69">näjɣunʼäɣane(ɨ)ŋ tʼäraŋ: süːttə.</ta>
            <ta e="T74" id="Seg_1290" s="T72">täp süːnna(ə)t.</ta>
            <ta e="T78" id="Seg_1291" s="T74">man aj kutšalʼe qondä(e)aŋ.</ta>
            <ta e="T84" id="Seg_1292" s="T78">qarimɨɣən tättə časoɣɨndə kuttä-dakka maːtam lugornɨt.</ta>
            <ta e="T89" id="Seg_1293" s="T84">näjɣunʼäw väzɨŋ, soːɣɨndiŋ: kuttä nɨtʼän.</ta>
            <ta e="T99" id="Seg_1294" s="T89">iːvə, nimdə Мihail, tʼärɨn: qalman, ava (üːttə) ava nʼüːdə matta.</ta>
            <ta e="T101" id="Seg_1295" s="T99">man tšaːǯindaŋ.</ta>
            <ta e="T104" id="Seg_1296" s="T101">näjɣunʼäw maːtam nüːut.</ta>
            <ta e="T109" id="Seg_1297" s="T104">iːwə säːrnä maːttə ondɨ lʼäɣandɨze.</ta>
            <ta e="T119" id="Seg_1298" s="T109">meŋnan soɣɨ(u)nʼdʼikuŋ iːwə anduŋgo, maːdərnə lʼäɣaw qwängu piːrandə (piːraj äːtondə).</ta>
            <ta e="T126" id="Seg_1299" s="T119">man tʼäraŋ täbɨnni iːɣəneŋ: iːmat miːttə aːndum.</ta>
            <ta e="T131" id="Seg_1300" s="T126">nännə (nännɨdoː) man assä qondəzaŋ.</ta>
            <ta e="T134" id="Seg_1301" s="T131">qwännaŋ manǯimbegu poŋgɨm.</ta>
            <ta e="T138" id="Seg_1302" s="T134">amgu orannaw poŋgoɣɨn qwälɨm.</ta>
            <ta e="T144" id="Seg_1303" s="T138">tüːaŋ maːtqɨneŋ, aːvvɨreeŋ, nännä(ə) püːdokuzaw poŋg(ɣ)ɨm.</ta>
            <ta e="T151" id="Seg_1304" s="T144">köːdizaŋ počtəndə, okkə nägər iːndaŋ ispolkomɣɨnnä tüːmbädə.</ta>
            <ta e="T154" id="Seg_1305" s="T151">nännə särnaŋ sovettə.</ta>
            <ta e="T158" id="Seg_1306" s="T154">oldo laqqɨlʼe tümbədi pajazä.</ta>
            <ta e="T162" id="Seg_1307" s="T158">nimdə täbɨn Аngelina Иvanovna.</ta>
            <ta e="T165" id="Seg_1308" s="T162">tšenʼtšuzo, nägɨnǯizoː süsüɣulʼdʼe.</ta>
            <ta e="T171" id="Seg_1309" s="T165">nilʼdʼiŋa mandɨziːkuzattə tʼeːlɨlat, qaːrɨnnɨ (qariːmɨɣɨnnɨ) üːdɨmɨnǯenno(ə).</ta>
            <ta e="T178" id="Seg_1310" s="T171">laɣ(q)əkuzo köt okkɨr qwäj gʼöt časɨn tʼelʼeɣɨndə.</ta>
            <ta e="T185" id="Seg_1311" s="T178">süsüĝu irala kodɨt qassaktə assä(ə) aːmdenǯiŋ na goːnɨŋ.</ta>
            <ta e="T193" id="Seg_1312" s="T185">täp(p)ɨla aːmdəqwattə assä qoːnaŋ, qöːdʼättə sappɨɣərɨgu, poŋgɨrgu, mandu(j)jugu.</ta>
            <ta e="T197" id="Seg_1313" s="T193">täppɨlan(n)e nitʼän soː jeŋ.</ta>
            <ta e="T209" id="Seg_1314" s="T197">nɨnɨŋa oːttane(ä)ndi, sɨppa, püː aːmniattə, assä aːmdəgu nägɨnǯigu, toːrenǯigu i qätkugu süsüg(ɣ) äːǯim.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_1315" s="T0">üdəmɨn avɨrlʼevlʼe kučalʼlʼe qoːndäjeŋ. </ta>
            <ta e="T9" id="Seg_1316" s="T4">sittaŋ, qamɣɨneŋ qonɨŋ taqqulɨmbattə nɨnɨŋala. </ta>
            <ta e="T16" id="Seg_1317" s="T9">ippaŋ onäŋ täːrbaŋ, kundar qamɣəneŋ säːrɣulumbattə nɨnɨŋala. </ta>
            <ta e="T23" id="Seg_1318" s="T16">qaːwənbaːrɨːnda čeːčun ippɨlʼe manǯeːan, qaːwɨn uːgoː näːrgumba. </ta>
            <ta e="T32" id="Seg_1319" s="T23">kɨba iːkaw nimdə täbɨn Vova soːɣɨndʼäŋ: tan nɨškɨlbal (qaːwɨm)? </ta>
            <ta e="T38" id="Seg_1320" s="T32">täp meŋa tʼärɨŋ: assä man nɨškɨlbaw. </ta>
            <ta e="T47" id="Seg_1321" s="T38">täp tʼärɨŋ: oːppow Katʼa omdɨlʼe qoptɨn baːrondə, täp nɨškəlbat. </ta>
            <ta e="T51" id="Seg_1322" s="T47">näːkaɣäneŋ soːɣɨnʼdʼäŋ: tan nɨškɨlbal? </ta>
            <ta e="T63" id="Seg_1323" s="T51">täp meŋa tʼärɨn: man naj assä nɨškɨlguzaw i assä tükkuzaŋ qaːwɨn qöndo. </ta>
            <ta e="T67" id="Seg_1324" s="T63">nilʼdʼiŋ man i qaːlaŋ. </ta>
            <ta e="T69" id="Seg_1325" s="T67">assä qosolǯaw. </ta>
            <ta e="T72" id="Seg_1326" s="T69">näjɣunʼäɣaneŋ tʼäraŋ: süːttə. </ta>
            <ta e="T74" id="Seg_1327" s="T72">täp süːnnat. </ta>
            <ta e="T78" id="Seg_1328" s="T74">man aj kučalʼe qondäaŋ. </ta>
            <ta e="T84" id="Seg_1329" s="T78">qarimɨɣən tättə časoɣɨndə kuttä-dakka maːtam lugornɨt. </ta>
            <ta e="T89" id="Seg_1330" s="T84">näjɣunʼäw väzɨŋ, soːɣɨndiŋ: kuttä nɨtʼän. </ta>
            <ta e="T99" id="Seg_1331" s="T89">iːwə, nimdə Мihail, tʼärɨn: qalman, awa (üːttə) awa nʼüːdə matta. </ta>
            <ta e="T101" id="Seg_1332" s="T99">man čaːǯindaŋ. </ta>
            <ta e="T104" id="Seg_1333" s="T101">näjɣunʼäw maːtam nüːut. </ta>
            <ta e="T109" id="Seg_1334" s="T104">iːwə säːrnä maːttə ondɨ lʼäɣandɨze. </ta>
            <ta e="T119" id="Seg_1335" s="T109">meŋnan soɣɨnʼdʼikuŋ iːwə anduŋgo, maːdərnə: lʼäɣaw qwängu piːrandə (piːraj äːtondə). </ta>
            <ta e="T126" id="Seg_1336" s="T119">man tʼäraŋ täbɨnni iːɣəneŋ: iːmat miːttə aːndum. </ta>
            <ta e="T131" id="Seg_1337" s="T126">nännə (nännɨdoː) man assä qondəzaŋ. </ta>
            <ta e="T134" id="Seg_1338" s="T131">qwännaŋ manǯimbegu poŋgɨm. </ta>
            <ta e="T138" id="Seg_1339" s="T134">amgu orannaw poŋgoɣɨn qwälɨm. </ta>
            <ta e="T144" id="Seg_1340" s="T138">tüːaŋ maːtqɨneŋ, aːvvɨreeŋ, nännä püːdokuzaw poŋgɨm. </ta>
            <ta e="T151" id="Seg_1341" s="T144">köːdizaŋ počtəndə, okkə nägər iːndaŋ ispolkomɣɨnnä tüːmbädə. </ta>
            <ta e="T154" id="Seg_1342" s="T151">nännə särnaŋ sovettə. </ta>
            <ta e="T158" id="Seg_1343" s="T154">oldo laqqɨlʼe tümbədi pajazä. </ta>
            <ta e="T162" id="Seg_1344" s="T158">nimdə täbɨn Аngelina Ivanovna. </ta>
            <ta e="T165" id="Seg_1345" s="T162">čenʼčuzo, nägɨnǯizoː süsüɣulʼdʼe. </ta>
            <ta e="T171" id="Seg_1346" s="T165">nilʼdʼiŋa mandɨziːkuzattə tʼeːlɨlat, qaːrɨnnɨ (qariːmɨɣɨnnɨ) üːdɨmɨnǯenno. </ta>
            <ta e="T178" id="Seg_1347" s="T171">laɣəkuzo köt okkɨr qwäj köt časɨn tʼelʼeɣɨndə. </ta>
            <ta e="T185" id="Seg_1348" s="T178">süsügu irala kodɨt qassaktə assä aːmdenǯiŋ nagoːnɨŋ. </ta>
            <ta e="T193" id="Seg_1349" s="T185">täpɨla aːmdəqwattə assä qoːnaŋ, qöːdʼättə sappɨɣərɨgu, poŋgɨrgu, mandujugu. </ta>
            <ta e="T197" id="Seg_1350" s="T193">täppɨlane nitʼän soː jeŋ. </ta>
            <ta e="T209" id="Seg_1351" s="T197">nɨnɨŋa oːttanendi, sɨppa, püː aːmniattə, assä aːmdəgu nägɨnǯigu, toːrenǯigu i qätkugu süsügäːǯim. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1352" s="T0">üdə-mɨn</ta>
            <ta e="T2" id="Seg_1353" s="T1">av-ɨ-r-lʼevlʼe</ta>
            <ta e="T3" id="Seg_1354" s="T2">kuča-lʼlʼe</ta>
            <ta e="T4" id="Seg_1355" s="T3">qoːndä-je-ŋ</ta>
            <ta e="T5" id="Seg_1356" s="T4">sitta-ŋ</ta>
            <ta e="T6" id="Seg_1357" s="T5">qam-ɣɨneŋ</ta>
            <ta e="T7" id="Seg_1358" s="T6">qonɨŋ</ta>
            <ta e="T8" id="Seg_1359" s="T7">taqqu-lɨ-mba-ttə</ta>
            <ta e="T9" id="Seg_1360" s="T8">nɨnɨŋa-la</ta>
            <ta e="T10" id="Seg_1361" s="T9">ippa-ŋ</ta>
            <ta e="T11" id="Seg_1362" s="T10">onäŋ</ta>
            <ta e="T12" id="Seg_1363" s="T11">täːrba-ŋ</ta>
            <ta e="T13" id="Seg_1364" s="T12">kundar</ta>
            <ta e="T14" id="Seg_1365" s="T13">qam-ɣəneŋ</ta>
            <ta e="T15" id="Seg_1366" s="T14">säːrɣu-lu-mba-ttə</ta>
            <ta e="T16" id="Seg_1367" s="T15">nɨnɨŋa-la</ta>
            <ta e="T17" id="Seg_1368" s="T16">qaːw-ə-n-baːr-ɨː-nda</ta>
            <ta e="T18" id="Seg_1369" s="T17">čeːčun</ta>
            <ta e="T19" id="Seg_1370" s="T18">ippɨ-lʼe</ta>
            <ta e="T20" id="Seg_1371" s="T19">manǯeː-a-n</ta>
            <ta e="T21" id="Seg_1372" s="T20">qaːw-ɨ-n</ta>
            <ta e="T22" id="Seg_1373" s="T21">uːgoː</ta>
            <ta e="T23" id="Seg_1374" s="T22">näːrgu-mba</ta>
            <ta e="T24" id="Seg_1375" s="T23">kɨba</ta>
            <ta e="T25" id="Seg_1376" s="T24">iː-ka-w</ta>
            <ta e="T26" id="Seg_1377" s="T25">nim-də</ta>
            <ta e="T27" id="Seg_1378" s="T26">täb-ɨ-n</ta>
            <ta e="T28" id="Seg_1379" s="T27">Vova</ta>
            <ta e="T29" id="Seg_1380" s="T28">soːɣɨ-ndʼä-ŋ</ta>
            <ta e="T30" id="Seg_1381" s="T29">tat</ta>
            <ta e="T31" id="Seg_1382" s="T30">nɨškɨ-l-ba-l</ta>
            <ta e="T32" id="Seg_1383" s="T31">qaːw-ɨ-m</ta>
            <ta e="T33" id="Seg_1384" s="T32">täp</ta>
            <ta e="T34" id="Seg_1385" s="T33">meŋa</ta>
            <ta e="T35" id="Seg_1386" s="T34">tʼärɨ-ŋ</ta>
            <ta e="T36" id="Seg_1387" s="T35">assä</ta>
            <ta e="T37" id="Seg_1388" s="T36">man</ta>
            <ta e="T38" id="Seg_1389" s="T37">nɨškɨ-l-ba-w</ta>
            <ta e="T39" id="Seg_1390" s="T38">täp</ta>
            <ta e="T40" id="Seg_1391" s="T39">tʼärɨ-ŋ</ta>
            <ta e="T41" id="Seg_1392" s="T40">oːppo-w</ta>
            <ta e="T42" id="Seg_1393" s="T41">Katʼa</ta>
            <ta e="T43" id="Seg_1394" s="T42">omdɨ-lʼe</ta>
            <ta e="T44" id="Seg_1395" s="T43">qopt-ɨ-n</ta>
            <ta e="T45" id="Seg_1396" s="T44">baːr-o-ndə</ta>
            <ta e="T46" id="Seg_1397" s="T45">täp</ta>
            <ta e="T47" id="Seg_1398" s="T46">nɨškə-l-ba-t</ta>
            <ta e="T48" id="Seg_1399" s="T47">näː-ka-ɣäneŋ</ta>
            <ta e="T49" id="Seg_1400" s="T48">soːɣɨ-nʼdʼä-ŋ</ta>
            <ta e="T50" id="Seg_1401" s="T49">tat</ta>
            <ta e="T51" id="Seg_1402" s="T50">nɨškɨ-l-ba-l</ta>
            <ta e="T52" id="Seg_1403" s="T51">täp</ta>
            <ta e="T53" id="Seg_1404" s="T52">meŋa</ta>
            <ta e="T54" id="Seg_1405" s="T53">tʼärɨ-n</ta>
            <ta e="T55" id="Seg_1406" s="T54">man</ta>
            <ta e="T56" id="Seg_1407" s="T55">naj</ta>
            <ta e="T57" id="Seg_1408" s="T56">assä</ta>
            <ta e="T58" id="Seg_1409" s="T57">nɨškɨ-l-gu-za-w</ta>
            <ta e="T59" id="Seg_1410" s="T58">i</ta>
            <ta e="T60" id="Seg_1411" s="T59">assä</ta>
            <ta e="T61" id="Seg_1412" s="T60">tü-kku-za-ŋ</ta>
            <ta e="T62" id="Seg_1413" s="T61">qaːw-ɨ-n</ta>
            <ta e="T63" id="Seg_1414" s="T62">qö-ndo</ta>
            <ta e="T64" id="Seg_1415" s="T63">nilʼdʼi-ŋ</ta>
            <ta e="T65" id="Seg_1416" s="T64">man</ta>
            <ta e="T66" id="Seg_1417" s="T65">i</ta>
            <ta e="T67" id="Seg_1418" s="T66">qaːla-ŋ</ta>
            <ta e="T68" id="Seg_1419" s="T67">assä</ta>
            <ta e="T69" id="Seg_1420" s="T68">qoso-lǯa-w</ta>
            <ta e="T70" id="Seg_1421" s="T69">nä-j-ɣunʼ-ä-ɣaneŋ</ta>
            <ta e="T71" id="Seg_1422" s="T70">tʼära-ŋ</ta>
            <ta e="T72" id="Seg_1423" s="T71">süːt-tə</ta>
            <ta e="T73" id="Seg_1424" s="T72">täp</ta>
            <ta e="T74" id="Seg_1425" s="T73">süːn-na-t</ta>
            <ta e="T75" id="Seg_1426" s="T74">man</ta>
            <ta e="T76" id="Seg_1427" s="T75">aj</ta>
            <ta e="T77" id="Seg_1428" s="T76">kuča-lʼe</ta>
            <ta e="T78" id="Seg_1429" s="T77">qondä-a-ŋ</ta>
            <ta e="T79" id="Seg_1430" s="T78">qari-mɨ-ɣən</ta>
            <ta e="T80" id="Seg_1431" s="T79">tättə</ta>
            <ta e="T81" id="Seg_1432" s="T80">čas-o-ɣɨndə</ta>
            <ta e="T82" id="Seg_1433" s="T81">kuttä-dakka</ta>
            <ta e="T83" id="Seg_1434" s="T82">maːta-m</ta>
            <ta e="T84" id="Seg_1435" s="T83">lugo-r-nɨ-t</ta>
            <ta e="T85" id="Seg_1436" s="T84">nä-j-ɣunʼ-ä-w</ta>
            <ta e="T86" id="Seg_1437" s="T85">väzɨ-ŋ</ta>
            <ta e="T87" id="Seg_1438" s="T86">soːɣɨ-ndi-ŋ</ta>
            <ta e="T88" id="Seg_1439" s="T87">kuttä</ta>
            <ta e="T89" id="Seg_1440" s="T88">nɨtʼä-n</ta>
            <ta e="T90" id="Seg_1441" s="T89">iː-wə</ta>
            <ta e="T91" id="Seg_1442" s="T90">nim-də</ta>
            <ta e="T92" id="Seg_1443" s="T91">Мihail</ta>
            <ta e="T93" id="Seg_1444" s="T92">tʼärɨ-n</ta>
            <ta e="T94" id="Seg_1445" s="T93">qal-ma-n</ta>
            <ta e="T95" id="Seg_1446" s="T94">awa</ta>
            <ta e="T96" id="Seg_1447" s="T95">üːt-tə</ta>
            <ta e="T97" id="Seg_1448" s="T96">awa</ta>
            <ta e="T98" id="Seg_1449" s="T97">nʼüː-də</ta>
            <ta e="T99" id="Seg_1450" s="T98">mat-ta</ta>
            <ta e="T100" id="Seg_1451" s="T99">man</ta>
            <ta e="T101" id="Seg_1452" s="T100">čaːǯi-nda-ŋ</ta>
            <ta e="T102" id="Seg_1453" s="T101">nä-j-ɣunʼ-ä-w</ta>
            <ta e="T103" id="Seg_1454" s="T102">maːta-m</ta>
            <ta e="T104" id="Seg_1455" s="T103">nüː-u-t</ta>
            <ta e="T105" id="Seg_1456" s="T104">iː-wə</ta>
            <ta e="T106" id="Seg_1457" s="T105">säːr-nä</ta>
            <ta e="T107" id="Seg_1458" s="T106">maːt-tə</ta>
            <ta e="T108" id="Seg_1459" s="T107">ondɨ</ta>
            <ta e="T109" id="Seg_1460" s="T108">lʼäɣa-n-d-ɨ-ze</ta>
            <ta e="T110" id="Seg_1461" s="T109">meŋnan</ta>
            <ta e="T111" id="Seg_1462" s="T110">soɣɨ-nʼdʼi-ku-ŋ</ta>
            <ta e="T112" id="Seg_1463" s="T111">iː-wə</ta>
            <ta e="T113" id="Seg_1464" s="T112">andu-ŋgo</ta>
            <ta e="T114" id="Seg_1465" s="T113">maːdə-r-nə</ta>
            <ta e="T115" id="Seg_1466" s="T114">lʼäɣa-w</ta>
            <ta e="T116" id="Seg_1467" s="T115">qwän-gu</ta>
            <ta e="T117" id="Seg_1468" s="T116">piːra-ndə</ta>
            <ta e="T118" id="Seg_1469" s="T117">piːra-j</ta>
            <ta e="T119" id="Seg_1470" s="T118">äːto-ndə</ta>
            <ta e="T120" id="Seg_1471" s="T119">man</ta>
            <ta e="T121" id="Seg_1472" s="T120">tʼära-ŋ</ta>
            <ta e="T122" id="Seg_1473" s="T121">täb-ɨ-nni</ta>
            <ta e="T123" id="Seg_1474" s="T122">iː-ɣəneŋ</ta>
            <ta e="T124" id="Seg_1475" s="T123">iːma-t</ta>
            <ta e="T125" id="Seg_1476" s="T124">miː-ttə</ta>
            <ta e="T126" id="Seg_1477" s="T125">aːndu-m</ta>
            <ta e="T127" id="Seg_1478" s="T126">nännə</ta>
            <ta e="T128" id="Seg_1479" s="T127">nännɨdoː</ta>
            <ta e="T129" id="Seg_1480" s="T128">man</ta>
            <ta e="T130" id="Seg_1481" s="T129">assä</ta>
            <ta e="T131" id="Seg_1482" s="T130">qondə-za-ŋ</ta>
            <ta e="T132" id="Seg_1483" s="T131">qwän-na-ŋ</ta>
            <ta e="T133" id="Seg_1484" s="T132">manǯi-mbe-gu</ta>
            <ta e="T134" id="Seg_1485" s="T133">poŋgɨ-m</ta>
            <ta e="T135" id="Seg_1486" s="T134">am-ɨ-gu</ta>
            <ta e="T136" id="Seg_1487" s="T135">oran-na-w</ta>
            <ta e="T137" id="Seg_1488" s="T136">poŋgo-ɣɨn</ta>
            <ta e="T138" id="Seg_1489" s="T137">qwälɨ-m</ta>
            <ta e="T139" id="Seg_1490" s="T138">tüː-a-ŋ</ta>
            <ta e="T140" id="Seg_1491" s="T139">maːt-qɨneŋ</ta>
            <ta e="T141" id="Seg_1492" s="T140">aːvv-ɨ-r-ee-ŋ</ta>
            <ta e="T142" id="Seg_1493" s="T141">nännä</ta>
            <ta e="T143" id="Seg_1494" s="T142">püːdo-ku-za-w</ta>
            <ta e="T144" id="Seg_1495" s="T143">poŋgɨ-m</ta>
            <ta e="T145" id="Seg_1496" s="T144">köːdi-za-ŋ</ta>
            <ta e="T146" id="Seg_1497" s="T145">počtə-ndə</ta>
            <ta e="T147" id="Seg_1498" s="T146">okkə</ta>
            <ta e="T148" id="Seg_1499" s="T147">nägər</ta>
            <ta e="T149" id="Seg_1500" s="T148">iː-nda-ŋ</ta>
            <ta e="T150" id="Seg_1501" s="T149">ispolkom-ɣɨnnä</ta>
            <ta e="T151" id="Seg_1502" s="T150">tüː-mbädə</ta>
            <ta e="T152" id="Seg_1503" s="T151">nännə</ta>
            <ta e="T153" id="Seg_1504" s="T152">sär-na-ŋ</ta>
            <ta e="T154" id="Seg_1505" s="T153">sovet-tə</ta>
            <ta e="T155" id="Seg_1506" s="T154">%%</ta>
            <ta e="T156" id="Seg_1507" s="T155">laqqɨ-lʼe</ta>
            <ta e="T157" id="Seg_1508" s="T156">tü-mbədi</ta>
            <ta e="T158" id="Seg_1509" s="T157">paja-zä</ta>
            <ta e="T159" id="Seg_1510" s="T158">nim-də</ta>
            <ta e="T160" id="Seg_1511" s="T159">täb-ɨ-n</ta>
            <ta e="T161" id="Seg_1512" s="T160">Аngelina</ta>
            <ta e="T162" id="Seg_1513" s="T161">Ivanovna</ta>
            <ta e="T163" id="Seg_1514" s="T162">čenʼču-z-o</ta>
            <ta e="T164" id="Seg_1515" s="T163">nägɨ-nǯi-z-oː</ta>
            <ta e="T210" id="Seg_1516" s="T164">süsüɣulʼ</ta>
            <ta e="T165" id="Seg_1517" s="T210">dʼe</ta>
            <ta e="T166" id="Seg_1518" s="T165">nilʼdʼi-ŋa</ta>
            <ta e="T167" id="Seg_1519" s="T166">mandɨ-ziː-ku-za-ttə</ta>
            <ta e="T168" id="Seg_1520" s="T167">tʼeːlɨ-la-t</ta>
            <ta e="T169" id="Seg_1521" s="T168">qaːr-ɨ-nnɨ</ta>
            <ta e="T170" id="Seg_1522" s="T169">qar-iː-mɨ-ɣɨnnɨ</ta>
            <ta e="T171" id="Seg_1523" s="T170">üːdɨ-mɨ-nǯenno</ta>
            <ta e="T172" id="Seg_1524" s="T171">laɣə-ku-z-o</ta>
            <ta e="T173" id="Seg_1525" s="T172">köt</ta>
            <ta e="T174" id="Seg_1526" s="T173">okkɨr</ta>
            <ta e="T175" id="Seg_1527" s="T174">qwäj</ta>
            <ta e="T176" id="Seg_1528" s="T175">köt</ta>
            <ta e="T177" id="Seg_1529" s="T176">čas-ɨ-n</ta>
            <ta e="T178" id="Seg_1530" s="T177">tʼelʼe-ɣɨndə</ta>
            <ta e="T179" id="Seg_1531" s="T178">süsügu</ta>
            <ta e="T180" id="Seg_1532" s="T179">ira-la</ta>
            <ta e="T181" id="Seg_1533" s="T180">kodɨ-t</ta>
            <ta e="T182" id="Seg_1534" s="T181">qassak-tə</ta>
            <ta e="T183" id="Seg_1535" s="T182">assä</ta>
            <ta e="T184" id="Seg_1536" s="T183">aːmde-nǯi-ŋ</ta>
            <ta e="T185" id="Seg_1537" s="T184">nagoːnɨŋ</ta>
            <ta e="T186" id="Seg_1538" s="T185">täp-ɨ-la</ta>
            <ta e="T187" id="Seg_1539" s="T186">aːmdə-q-wa-ttə</ta>
            <ta e="T188" id="Seg_1540" s="T187">assä</ta>
            <ta e="T189" id="Seg_1541" s="T188">qoːnaŋ</ta>
            <ta e="T190" id="Seg_1542" s="T189">qöː-dʼä-ttə</ta>
            <ta e="T191" id="Seg_1543" s="T190">sappɨɣə-r-ɨ-gu</ta>
            <ta e="T192" id="Seg_1544" s="T191">poŋgɨ-r-gu</ta>
            <ta e="T193" id="Seg_1545" s="T192">mandu-ju-gu</ta>
            <ta e="T194" id="Seg_1546" s="T193">täpp-ɨ-la-ne</ta>
            <ta e="T195" id="Seg_1547" s="T194">nitʼä-n</ta>
            <ta e="T196" id="Seg_1548" s="T195">soː</ta>
            <ta e="T197" id="Seg_1549" s="T196">je-ŋ</ta>
            <ta e="T198" id="Seg_1550" s="T197">nɨnɨŋa</ta>
            <ta e="T199" id="Seg_1551" s="T198">oːtta-nendi</ta>
            <ta e="T200" id="Seg_1552" s="T199">sɨppa</ta>
            <ta e="T201" id="Seg_1553" s="T200">püː</ta>
            <ta e="T202" id="Seg_1554" s="T201">aːm-ni-a-ttə</ta>
            <ta e="T203" id="Seg_1555" s="T202">assä</ta>
            <ta e="T204" id="Seg_1556" s="T203">aːmdə-gu</ta>
            <ta e="T205" id="Seg_1557" s="T204">nägɨ-nǯi-gu</ta>
            <ta e="T206" id="Seg_1558" s="T205">toːr-e-nǯi-gu</ta>
            <ta e="T207" id="Seg_1559" s="T206">i</ta>
            <ta e="T208" id="Seg_1560" s="T207">qät-ku-gu</ta>
            <ta e="T209" id="Seg_1561" s="T208">süsüg-äːǯi-m</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1562" s="T0">üːtǝ-mɨn</ta>
            <ta e="T2" id="Seg_1563" s="T1">am-ɨ-r-lewle</ta>
            <ta e="T3" id="Seg_1564" s="T2">quča-le</ta>
            <ta e="T4" id="Seg_1565" s="T3">qontə-ŋɨ-ŋ</ta>
            <ta e="T5" id="Seg_1566" s="T4">sittə-ŋ</ta>
            <ta e="T6" id="Seg_1567" s="T5">qam-qɨntɨ</ta>
            <ta e="T7" id="Seg_1568" s="T6">qoːnɨŋ</ta>
            <ta e="T8" id="Seg_1569" s="T7">taqqu-lɨ-mbɨ-tɨt</ta>
            <ta e="T9" id="Seg_1570" s="T8">nɨnka-la</ta>
            <ta e="T10" id="Seg_1571" s="T9">ippi-ŋ</ta>
            <ta e="T11" id="Seg_1572" s="T10">onäk</ta>
            <ta e="T12" id="Seg_1573" s="T11">tärba-ŋ</ta>
            <ta e="T13" id="Seg_1574" s="T12">kuttar</ta>
            <ta e="T14" id="Seg_1575" s="T13">qam-qɨntɨ</ta>
            <ta e="T15" id="Seg_1576" s="T14">säːrqu-lɨ-mbɨ-tɨt</ta>
            <ta e="T16" id="Seg_1577" s="T15">nɨnka-la</ta>
            <ta e="T17" id="Seg_1578" s="T16">qam-ɨ-n-par-ɨ-ndɨ</ta>
            <ta e="T18" id="Seg_1579" s="T17">čeːčun</ta>
            <ta e="T19" id="Seg_1580" s="T18">ippi-le</ta>
            <ta e="T20" id="Seg_1581" s="T19">manǯu-ɨ-ŋ</ta>
            <ta e="T21" id="Seg_1582" s="T20">qam-ɨ-n</ta>
            <ta e="T22" id="Seg_1583" s="T21">uːgoː</ta>
            <ta e="T23" id="Seg_1584" s="T22">naːrga-mbɨ</ta>
            <ta e="T24" id="Seg_1585" s="T23">kɨba</ta>
            <ta e="T25" id="Seg_1586" s="T24">iː-ka-m</ta>
            <ta e="T26" id="Seg_1587" s="T25">nim-t</ta>
            <ta e="T27" id="Seg_1588" s="T26">tap-ɨ-n</ta>
            <ta e="T28" id="Seg_1589" s="T27">Vova</ta>
            <ta e="T29" id="Seg_1590" s="T28">soqə-ntɨ-ŋ</ta>
            <ta e="T30" id="Seg_1591" s="T29">tan</ta>
            <ta e="T31" id="Seg_1592" s="T30">nɨškä-l-mbɨ-l</ta>
            <ta e="T32" id="Seg_1593" s="T31">qam-ɨ-m</ta>
            <ta e="T33" id="Seg_1594" s="T32">tap</ta>
            <ta e="T34" id="Seg_1595" s="T33">mäkkä</ta>
            <ta e="T35" id="Seg_1596" s="T34">tʼarɨ-ŋ</ta>
            <ta e="T36" id="Seg_1597" s="T35">ašša</ta>
            <ta e="T37" id="Seg_1598" s="T36">man</ta>
            <ta e="T38" id="Seg_1599" s="T37">nɨškä-l-mbɨ-m</ta>
            <ta e="T39" id="Seg_1600" s="T38">tap</ta>
            <ta e="T40" id="Seg_1601" s="T39">tʼarɨ-ŋ</ta>
            <ta e="T41" id="Seg_1602" s="T40">oba-m</ta>
            <ta e="T42" id="Seg_1603" s="T41">Katʼa</ta>
            <ta e="T43" id="Seg_1604" s="T42">omtə-le</ta>
            <ta e="T44" id="Seg_1605" s="T43">koːptǝ-ɨ-n</ta>
            <ta e="T45" id="Seg_1606" s="T44">par-ɨ-ndɨ</ta>
            <ta e="T46" id="Seg_1607" s="T45">tap</ta>
            <ta e="T47" id="Seg_1608" s="T46">nɨškä-l-mbɨ-tɨ</ta>
            <ta e="T48" id="Seg_1609" s="T47">neː-ka-qɨntɨ</ta>
            <ta e="T49" id="Seg_1610" s="T48">soqə-nče-ŋ</ta>
            <ta e="T50" id="Seg_1611" s="T49">tan</ta>
            <ta e="T51" id="Seg_1612" s="T50">nɨškä-l-mbɨ-l</ta>
            <ta e="T52" id="Seg_1613" s="T51">tap</ta>
            <ta e="T53" id="Seg_1614" s="T52">mäkkä</ta>
            <ta e="T54" id="Seg_1615" s="T53">tʼarɨ-n</ta>
            <ta e="T55" id="Seg_1616" s="T54">man</ta>
            <ta e="T56" id="Seg_1617" s="T55">naj</ta>
            <ta e="T57" id="Seg_1618" s="T56">ašša</ta>
            <ta e="T58" id="Seg_1619" s="T57">nɨškä-l-gu-sɨ-m</ta>
            <ta e="T59" id="Seg_1620" s="T58">i</ta>
            <ta e="T60" id="Seg_1621" s="T59">ašša</ta>
            <ta e="T61" id="Seg_1622" s="T60">tüː-kku-sɨ-ŋ</ta>
            <ta e="T62" id="Seg_1623" s="T61">qaːw-ɨ-n</ta>
            <ta e="T63" id="Seg_1624" s="T62">kö-ndɨ</ta>
            <ta e="T64" id="Seg_1625" s="T63">nik-k</ta>
            <ta e="T65" id="Seg_1626" s="T64">man</ta>
            <ta e="T66" id="Seg_1627" s="T65">i</ta>
            <ta e="T67" id="Seg_1628" s="T66">qalɨ-ŋ</ta>
            <ta e="T68" id="Seg_1629" s="T67">ašša</ta>
            <ta e="T69" id="Seg_1630" s="T68">qoso-lʼčǝ-m</ta>
            <ta e="T70" id="Seg_1631" s="T69">neː-lʼ-qum-ɨ-qənɨŋ</ta>
            <ta e="T71" id="Seg_1632" s="T70">tʼarɨ-ŋ</ta>
            <ta e="T72" id="Seg_1633" s="T71">šüt-ätɨ</ta>
            <ta e="T73" id="Seg_1634" s="T72">tap</ta>
            <ta e="T74" id="Seg_1635" s="T73">šüt-ŋɨ-tɨ</ta>
            <ta e="T75" id="Seg_1636" s="T74">man</ta>
            <ta e="T76" id="Seg_1637" s="T75">aj</ta>
            <ta e="T77" id="Seg_1638" s="T76">quča-le</ta>
            <ta e="T78" id="Seg_1639" s="T77">qontə-ɨ-ŋ</ta>
            <ta e="T79" id="Seg_1640" s="T78">qarɨ-mɨ-qən</ta>
            <ta e="T80" id="Seg_1641" s="T79">tättɨ</ta>
            <ta e="T81" id="Seg_1642" s="T80">čas-ɨ-qɨntɨ</ta>
            <ta e="T82" id="Seg_1643" s="T81">kutɨ-taka</ta>
            <ta e="T83" id="Seg_1644" s="T82">maːta-m</ta>
            <ta e="T84" id="Seg_1645" s="T83">lukkɨ-r-ŋɨ-tɨ</ta>
            <ta e="T85" id="Seg_1646" s="T84">neː-lʼ-qum-ɨ-m</ta>
            <ta e="T86" id="Seg_1647" s="T85">weᴣɨ-ŋ</ta>
            <ta e="T87" id="Seg_1648" s="T86">soqə-ntɨ-ŋ</ta>
            <ta e="T88" id="Seg_1649" s="T87">kutɨ</ta>
            <ta e="T89" id="Seg_1650" s="T88">načʼa-n</ta>
            <ta e="T90" id="Seg_1651" s="T89">iː-m</ta>
            <ta e="T91" id="Seg_1652" s="T90">nim-t</ta>
            <ta e="T92" id="Seg_1653" s="T91">Мihail</ta>
            <ta e="T93" id="Seg_1654" s="T92">tʼarɨ-n</ta>
            <ta e="T94" id="Seg_1655" s="T93">kaǝt-mbɨ-ŋ</ta>
            <ta e="T95" id="Seg_1656" s="T94">awa</ta>
            <ta e="T96" id="Seg_1657" s="T95">üt-ndɨ</ta>
            <ta e="T97" id="Seg_1658" s="T96">awa</ta>
            <ta e="T98" id="Seg_1659" s="T97">nʼoː-ätɨ</ta>
            <ta e="T99" id="Seg_1660" s="T98">maːt-ndɨ</ta>
            <ta e="T100" id="Seg_1661" s="T99">man</ta>
            <ta e="T101" id="Seg_1662" s="T100">čaːǯɨ-ntɨ-ŋ</ta>
            <ta e="T102" id="Seg_1663" s="T101">neː-lʼ-qum-ɨ-m</ta>
            <ta e="T103" id="Seg_1664" s="T102">maːta-m</ta>
            <ta e="T104" id="Seg_1665" s="T103">nʼoː-ŋɨ-tɨ</ta>
            <ta e="T105" id="Seg_1666" s="T104">iː-m</ta>
            <ta e="T106" id="Seg_1667" s="T105">šeːr-ŋɨ</ta>
            <ta e="T107" id="Seg_1668" s="T106">maːt-ndɨ</ta>
            <ta e="T108" id="Seg_1669" s="T107">ontɨ</ta>
            <ta e="T109" id="Seg_1670" s="T108">lʼaqa-n-t-ɨ-se</ta>
            <ta e="T110" id="Seg_1671" s="T109">meŋnan</ta>
            <ta e="T111" id="Seg_1672" s="T110">soqə-nče-kku-ŋ</ta>
            <ta e="T112" id="Seg_1673" s="T111">iː-m</ta>
            <ta e="T113" id="Seg_1674" s="T112">and-nqo</ta>
            <ta e="T114" id="Seg_1675" s="T113">madɨ-r-ŋɨ</ta>
            <ta e="T115" id="Seg_1676" s="T114">lʼaqa-m</ta>
            <ta e="T116" id="Seg_1677" s="T115">qwən-gu</ta>
            <ta e="T117" id="Seg_1678" s="T116">piːra-ndɨ</ta>
            <ta e="T118" id="Seg_1679" s="T117">piːra-lʼ</ta>
            <ta e="T119" id="Seg_1680" s="T118">eːtə-ndɨ</ta>
            <ta e="T120" id="Seg_1681" s="T119">man</ta>
            <ta e="T121" id="Seg_1682" s="T120">tʼarɨ-ŋ</ta>
            <ta e="T122" id="Seg_1683" s="T121">tap-ɨ-nɨ</ta>
            <ta e="T123" id="Seg_1684" s="T122">iː-qɨntɨ</ta>
            <ta e="T124" id="Seg_1685" s="T123">iː-t</ta>
            <ta e="T125" id="Seg_1686" s="T124">mi-ätɨ</ta>
            <ta e="T126" id="Seg_1687" s="T125">and-m</ta>
            <ta e="T127" id="Seg_1688" s="T126">nɨːnɨ</ta>
            <ta e="T128" id="Seg_1689" s="T127">nanto</ta>
            <ta e="T129" id="Seg_1690" s="T128">man</ta>
            <ta e="T130" id="Seg_1691" s="T129">ašša</ta>
            <ta e="T131" id="Seg_1692" s="T130">qontə-sɨ-ŋ</ta>
            <ta e="T132" id="Seg_1693" s="T131">qwən-ŋɨ-ŋ</ta>
            <ta e="T133" id="Seg_1694" s="T132">manǯu-mbɨ-gu</ta>
            <ta e="T134" id="Seg_1695" s="T133">poqqo-m</ta>
            <ta e="T135" id="Seg_1696" s="T134">am-ɨ-gu</ta>
            <ta e="T136" id="Seg_1697" s="T135">oral-ŋɨ-m</ta>
            <ta e="T137" id="Seg_1698" s="T136">poqqo-qən</ta>
            <ta e="T138" id="Seg_1699" s="T137">qwǝlɨ-m</ta>
            <ta e="T139" id="Seg_1700" s="T138">tüː-ɨ-ŋ</ta>
            <ta e="T140" id="Seg_1701" s="T139">maːt-qɨntɨ</ta>
            <ta e="T141" id="Seg_1702" s="T140">am-ɨ-r-ŋɨ-ŋ</ta>
            <ta e="T142" id="Seg_1703" s="T141">nɨːnɨ</ta>
            <ta e="T143" id="Seg_1704" s="T142">%%-kku-sɨ-m</ta>
            <ta e="T144" id="Seg_1705" s="T143">poqqo-m</ta>
            <ta e="T145" id="Seg_1706" s="T144">köːdi-sɨ-ŋ</ta>
            <ta e="T146" id="Seg_1707" s="T145">počtə-ndɨ</ta>
            <ta e="T147" id="Seg_1708" s="T146">okkɨr</ta>
            <ta e="T148" id="Seg_1709" s="T147">naːgɛr</ta>
            <ta e="T149" id="Seg_1710" s="T148">iː-ntɨ-ŋ</ta>
            <ta e="T150" id="Seg_1711" s="T149">ispolkom-qɨnnɨ</ta>
            <ta e="T151" id="Seg_1712" s="T150">tüː-mbɨdi</ta>
            <ta e="T152" id="Seg_1713" s="T151">nɨːnɨ</ta>
            <ta e="T153" id="Seg_1714" s="T152">šeːr-ŋɨ-ŋ</ta>
            <ta e="T154" id="Seg_1715" s="T153">sovet-ndɨ</ta>
            <ta e="T155" id="Seg_1716" s="T154">%%</ta>
            <ta e="T156" id="Seg_1717" s="T155">laqǝ-le</ta>
            <ta e="T157" id="Seg_1718" s="T156">tüː-mbɨdi</ta>
            <ta e="T158" id="Seg_1719" s="T157">paja-se</ta>
            <ta e="T159" id="Seg_1720" s="T158">nim-t</ta>
            <ta e="T160" id="Seg_1721" s="T159">tap-ɨ-n</ta>
            <ta e="T161" id="Seg_1722" s="T160">Аngelina</ta>
            <ta e="T162" id="Seg_1723" s="T161">Ivanovna</ta>
            <ta e="T163" id="Seg_1724" s="T162">čenču-sɨ-ut</ta>
            <ta e="T164" id="Seg_1725" s="T163">nagǝ-nče-sɨ-ut</ta>
            <ta e="T210" id="Seg_1726" s="T164">süsüqəlʼ</ta>
            <ta e="T165" id="Seg_1727" s="T210">dʼar</ta>
            <ta e="T166" id="Seg_1728" s="T165">nik-k</ta>
            <ta e="T167" id="Seg_1729" s="T166">mändə-ziː-kku-sɨ-tɨt</ta>
            <ta e="T168" id="Seg_1730" s="T167">tʼeːlɨ-la-t</ta>
            <ta e="T169" id="Seg_1731" s="T168">qarɨ-ɨ-nɨ</ta>
            <ta e="T170" id="Seg_1732" s="T169">qarɨ-ɨ-mɨ-qɨntɨ</ta>
            <ta e="T171" id="Seg_1733" s="T170">üːtǝ-mɨ-nǯenno</ta>
            <ta e="T172" id="Seg_1734" s="T171">laqǝ-kku-sɨ-ut</ta>
            <ta e="T173" id="Seg_1735" s="T172">qötqaj</ta>
            <ta e="T174" id="Seg_1736" s="T173">okkɨr</ta>
            <ta e="T175" id="Seg_1737" s="T174">qwäj</ta>
            <ta e="T176" id="Seg_1738" s="T175">qötqaj</ta>
            <ta e="T177" id="Seg_1739" s="T176">čas-ɨ-n</ta>
            <ta e="T178" id="Seg_1740" s="T177">tʼeːlɨ-qɨntɨ</ta>
            <ta e="T179" id="Seg_1741" s="T178">süsügu</ta>
            <ta e="T180" id="Seg_1742" s="T179">ira-la</ta>
            <ta e="T181" id="Seg_1743" s="T180">kutɨ-t</ta>
            <ta e="T182" id="Seg_1744" s="T181">qassaːq-t</ta>
            <ta e="T183" id="Seg_1745" s="T182">ašša</ta>
            <ta e="T184" id="Seg_1746" s="T183">omtə-nǯɨ-ŋ</ta>
            <ta e="T185" id="Seg_1747" s="T184">nagoːnɨŋ</ta>
            <ta e="T186" id="Seg_1748" s="T185">tap-ɨ-la</ta>
            <ta e="T187" id="Seg_1749" s="T186">omtə-kku-ŋɨ-tɨt</ta>
            <ta e="T188" id="Seg_1750" s="T187">ašša</ta>
            <ta e="T189" id="Seg_1751" s="T188">qoːnɨŋ</ta>
            <ta e="T190" id="Seg_1752" s="T189">qo-ntɨ-tɨt</ta>
            <ta e="T191" id="Seg_1753" s="T190">sapqə-r-ɨ-gu</ta>
            <ta e="T192" id="Seg_1754" s="T191">poqqo-r-gu</ta>
            <ta e="T193" id="Seg_1755" s="T192">mandu-j-gu</ta>
            <ta e="T194" id="Seg_1756" s="T193">tap-ɨ-la-nɨ</ta>
            <ta e="T195" id="Seg_1757" s="T194">načʼa-n</ta>
            <ta e="T196" id="Seg_1758" s="T195">sawa</ta>
            <ta e="T197" id="Seg_1759" s="T196">eː-ŋ</ta>
            <ta e="T198" id="Seg_1760" s="T197">nɨnka</ta>
            <ta e="T199" id="Seg_1761" s="T198">otta-nendi</ta>
            <ta e="T200" id="Seg_1762" s="T199">sɨppa</ta>
            <ta e="T201" id="Seg_1763" s="T200">püː</ta>
            <ta e="T202" id="Seg_1764" s="T201">amɨ-ne-ɨ-tɨt</ta>
            <ta e="T203" id="Seg_1765" s="T202">ašša</ta>
            <ta e="T204" id="Seg_1766" s="T203">omtə-gu</ta>
            <ta e="T205" id="Seg_1767" s="T204">nagǝ-nče-gu</ta>
            <ta e="T206" id="Seg_1768" s="T205">toːr-ɨ-nǯɨ-gu</ta>
            <ta e="T207" id="Seg_1769" s="T206">i</ta>
            <ta e="T208" id="Seg_1770" s="T207">kaǝt-kku-gu</ta>
            <ta e="T209" id="Seg_1771" s="T208">süsügu-ɛːǯa-m</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1772" s="T0">evening-PROL</ta>
            <ta e="T2" id="Seg_1773" s="T1">eat-EP-FRQ-CVB</ta>
            <ta e="T3" id="Seg_1774" s="T2">go.to.sleep-CVB</ta>
            <ta e="T4" id="Seg_1775" s="T3">sleep-CO-1SG.S</ta>
            <ta e="T5" id="Seg_1776" s="T4">wake.up-3SG.S</ta>
            <ta e="T6" id="Seg_1777" s="T5">bed.curtains-LOC.3SG</ta>
            <ta e="T7" id="Seg_1778" s="T6">many</ta>
            <ta e="T8" id="Seg_1779" s="T7">burrow-RES-PST.NAR-3PL</ta>
            <ta e="T9" id="Seg_1780" s="T8">mosquito-PL</ta>
            <ta e="T10" id="Seg_1781" s="T9">lie-1SG.S</ta>
            <ta e="T11" id="Seg_1782" s="T10">oneself.1SG</ta>
            <ta e="T12" id="Seg_1783" s="T11">think-1SG.S</ta>
            <ta e="T13" id="Seg_1784" s="T12">how</ta>
            <ta e="T14" id="Seg_1785" s="T13">bed.curtains-ILL.3SG</ta>
            <ta e="T15" id="Seg_1786" s="T14">creep-RES-PST.NAR-3PL</ta>
            <ta e="T16" id="Seg_1787" s="T15">mosquito-PL</ta>
            <ta e="T17" id="Seg_1788" s="T16">bed.curtains-EP-GEN-top-EP-ILL</ta>
            <ta e="T18" id="Seg_1789" s="T17">face.up</ta>
            <ta e="T19" id="Seg_1790" s="T18">lie-CVB</ta>
            <ta e="T20" id="Seg_1791" s="T19">look-EP-3SG.S</ta>
            <ta e="T21" id="Seg_1792" s="T20">bed.curtains-EP-GEN</ta>
            <ta e="T22" id="Seg_1793" s="T21">end</ta>
            <ta e="T23" id="Seg_1794" s="T22">%%-PST.NAR.[NOM]</ta>
            <ta e="T24" id="Seg_1795" s="T23">small</ta>
            <ta e="T25" id="Seg_1796" s="T24">son-DIM-1SG</ta>
            <ta e="T26" id="Seg_1797" s="T25">name-3SG</ta>
            <ta e="T27" id="Seg_1798" s="T26">he.NOM-EP-GEN</ta>
            <ta e="T28" id="Seg_1799" s="T27">Vova.[NOM]</ta>
            <ta e="T29" id="Seg_1800" s="T28">ask-IPFV-3SG.S</ta>
            <ta e="T30" id="Seg_1801" s="T29">you.SG.NOM</ta>
            <ta e="T31" id="Seg_1802" s="T30">tear-INCH-PST.NAR-2SG.O</ta>
            <ta e="T32" id="Seg_1803" s="T31">bed.curtains-EP-ACC</ta>
            <ta e="T33" id="Seg_1804" s="T32">he.NOM</ta>
            <ta e="T34" id="Seg_1805" s="T33">I.ALL</ta>
            <ta e="T35" id="Seg_1806" s="T34">say-3SG.S</ta>
            <ta e="T36" id="Seg_1807" s="T35">NEG</ta>
            <ta e="T37" id="Seg_1808" s="T36">I.NOM</ta>
            <ta e="T38" id="Seg_1809" s="T37">tear-INCH-PST.NAR-1SG.O</ta>
            <ta e="T39" id="Seg_1810" s="T38">he.NOM</ta>
            <ta e="T40" id="Seg_1811" s="T39">say-3SG.S</ta>
            <ta e="T41" id="Seg_1812" s="T40">sister-1SG</ta>
            <ta e="T42" id="Seg_1813" s="T41">Katya.[NOM]</ta>
            <ta e="T43" id="Seg_1814" s="T42">sit.down-CVB</ta>
            <ta e="T44" id="Seg_1815" s="T43">bed-EP-GEN</ta>
            <ta e="T45" id="Seg_1816" s="T44">top-EP-ILL</ta>
            <ta e="T46" id="Seg_1817" s="T45">he.NOM</ta>
            <ta e="T47" id="Seg_1818" s="T46">tear-INCH-PST.NAR-3SG.O</ta>
            <ta e="T48" id="Seg_1819" s="T47">daughter-DIM-ILL.3SG</ta>
            <ta e="T49" id="Seg_1820" s="T48">ask-IPFV-3SG.S</ta>
            <ta e="T50" id="Seg_1821" s="T49">you.SG.NOM</ta>
            <ta e="T51" id="Seg_1822" s="T50">tear-INCH-PST.NAR-2SG.O</ta>
            <ta e="T52" id="Seg_1823" s="T51">he.NOM</ta>
            <ta e="T53" id="Seg_1824" s="T52">I.ALL</ta>
            <ta e="T54" id="Seg_1825" s="T53">say-3SG.S</ta>
            <ta e="T55" id="Seg_1826" s="T54">I.NOM</ta>
            <ta e="T56" id="Seg_1827" s="T55">also</ta>
            <ta e="T57" id="Seg_1828" s="T56">NEG</ta>
            <ta e="T58" id="Seg_1829" s="T57">tear-INCH-INF-PST-1SG.O</ta>
            <ta e="T59" id="Seg_1830" s="T58">and</ta>
            <ta e="T60" id="Seg_1831" s="T59">NEG</ta>
            <ta e="T61" id="Seg_1832" s="T60">come-HAB-PST-1SG.S</ta>
            <ta e="T62" id="Seg_1833" s="T61">body-EP-ADV.LOC</ta>
            <ta e="T63" id="Seg_1834" s="T62">side-ILL</ta>
            <ta e="T64" id="Seg_1835" s="T63">so-ADVZ</ta>
            <ta e="T65" id="Seg_1836" s="T64">I.NOM</ta>
            <ta e="T66" id="Seg_1837" s="T65">and</ta>
            <ta e="T67" id="Seg_1838" s="T66">stay-3SG.S</ta>
            <ta e="T68" id="Seg_1839" s="T67">NEG</ta>
            <ta e="T69" id="Seg_1840" s="T68">discover-PFV-1SG.O</ta>
            <ta e="T70" id="Seg_1841" s="T69">daughter-ADJZ-human.being-EP-ALL.3SG</ta>
            <ta e="T71" id="Seg_1842" s="T70">say-3SG.S</ta>
            <ta e="T72" id="Seg_1843" s="T71">sew-IMP.2SG.O</ta>
            <ta e="T73" id="Seg_1844" s="T72">he.NOM</ta>
            <ta e="T74" id="Seg_1845" s="T73">sew-CO-3SG.O</ta>
            <ta e="T75" id="Seg_1846" s="T74">I.NOM</ta>
            <ta e="T76" id="Seg_1847" s="T75">again</ta>
            <ta e="T77" id="Seg_1848" s="T76">go.to.sleep-CVB</ta>
            <ta e="T78" id="Seg_1849" s="T77">sleep-EP-3SG.S</ta>
            <ta e="T79" id="Seg_1850" s="T78">morning-something-LOC</ta>
            <ta e="T80" id="Seg_1851" s="T79">four</ta>
            <ta e="T81" id="Seg_1852" s="T80">hour-EP-ILL.3SG</ta>
            <ta e="T82" id="Seg_1853" s="T81">who.[NOM]-INDEF4</ta>
            <ta e="T83" id="Seg_1854" s="T82">door-ACC</ta>
            <ta e="T84" id="Seg_1855" s="T83">knock-FRQ-CO-3SG.O</ta>
            <ta e="T85" id="Seg_1856" s="T84">woman-ADJZ-human.being-EP-1SG</ta>
            <ta e="T86" id="Seg_1857" s="T85">get.up-3SG.S</ta>
            <ta e="T87" id="Seg_1858" s="T86">ask-IPFV-3SG.S</ta>
            <ta e="T88" id="Seg_1859" s="T87">who</ta>
            <ta e="T89" id="Seg_1860" s="T88">there-ADV.LOC</ta>
            <ta e="T90" id="Seg_1861" s="T89">son-1SG</ta>
            <ta e="T91" id="Seg_1862" s="T90">name-3SG</ta>
            <ta e="T92" id="Seg_1863" s="T91">Mikhail.[NOM]</ta>
            <ta e="T93" id="Seg_1864" s="T92">say-3SG.S</ta>
            <ta e="T94" id="Seg_1865" s="T93">say-DUR-3SG.S</ta>
            <ta e="T95" id="Seg_1866" s="T94">mother.[NOM]</ta>
            <ta e="T96" id="Seg_1867" s="T95">water-ILL</ta>
            <ta e="T97" id="Seg_1868" s="T96">mother.[NOM]</ta>
            <ta e="T98" id="Seg_1869" s="T97">open-IMP.2SG.O</ta>
            <ta e="T99" id="Seg_1870" s="T98">house-ILL</ta>
            <ta e="T100" id="Seg_1871" s="T99">I.NOM</ta>
            <ta e="T101" id="Seg_1872" s="T100">go-IPFV-3SG.S</ta>
            <ta e="T102" id="Seg_1873" s="T101">woman-ADJZ-human.being-EP-1SG</ta>
            <ta e="T103" id="Seg_1874" s="T102">door-ACC</ta>
            <ta e="T104" id="Seg_1875" s="T103">open-CO-3SG.O</ta>
            <ta e="T105" id="Seg_1876" s="T104">son-1SG</ta>
            <ta e="T106" id="Seg_1877" s="T105">come.in-CO.[3SG.S]</ta>
            <ta e="T107" id="Seg_1878" s="T106">house-ILL</ta>
            <ta e="T108" id="Seg_1879" s="T107">oneself.3SG</ta>
            <ta e="T109" id="Seg_1880" s="T108">friend-GEN-PL-EP-COM</ta>
            <ta e="T110" id="Seg_1881" s="T109">I.ADES</ta>
            <ta e="T111" id="Seg_1882" s="T110">ask-IPFV-HAB-3SG.S</ta>
            <ta e="T112" id="Seg_1883" s="T111">son-1SG</ta>
            <ta e="T113" id="Seg_1884" s="T112">boat-TRL</ta>
            <ta e="T114" id="Seg_1885" s="T113">ask.for-FRQ-CO.[3SG.S]</ta>
            <ta e="T115" id="Seg_1886" s="T114">friend-1SG</ta>
            <ta e="T116" id="Seg_1887" s="T115">go.away-INF</ta>
            <ta e="T117" id="Seg_1888" s="T116">Pirino-ILL</ta>
            <ta e="T118" id="Seg_1889" s="T117">Pirino-ADJZ</ta>
            <ta e="T119" id="Seg_1890" s="T118">village-ILL</ta>
            <ta e="T120" id="Seg_1891" s="T119">I.NOM</ta>
            <ta e="T121" id="Seg_1892" s="T120">say-3SG.S</ta>
            <ta e="T122" id="Seg_1893" s="T121">he.NOM-EP-ALL</ta>
            <ta e="T123" id="Seg_1894" s="T122">son-ILL.3SG</ta>
            <ta e="T124" id="Seg_1895" s="T123">son-3SG</ta>
            <ta e="T125" id="Seg_1896" s="T124">give-IMP.2SG.O</ta>
            <ta e="T126" id="Seg_1897" s="T125">boat-ACC</ta>
            <ta e="T127" id="Seg_1898" s="T126">then</ta>
            <ta e="T128" id="Seg_1899" s="T127">after.that</ta>
            <ta e="T129" id="Seg_1900" s="T128">I.NOM</ta>
            <ta e="T130" id="Seg_1901" s="T129">NEG</ta>
            <ta e="T131" id="Seg_1902" s="T130">sleep-PST-3SG.S</ta>
            <ta e="T132" id="Seg_1903" s="T131">go.away-CO-1SG.S</ta>
            <ta e="T133" id="Seg_1904" s="T132">look-DUR-INF</ta>
            <ta e="T134" id="Seg_1905" s="T133">net-ACC</ta>
            <ta e="T135" id="Seg_1906" s="T134">eat-EP-INF</ta>
            <ta e="T136" id="Seg_1907" s="T135">catch-CO-1SG.O</ta>
            <ta e="T137" id="Seg_1908" s="T136">net-LOC</ta>
            <ta e="T138" id="Seg_1909" s="T137">fish-ACC</ta>
            <ta e="T139" id="Seg_1910" s="T138">come-EP-3SG.S</ta>
            <ta e="T140" id="Seg_1911" s="T139">house-ILL.3SG</ta>
            <ta e="T141" id="Seg_1912" s="T140">eat-EP-FRQ-CO-3SG.S</ta>
            <ta e="T142" id="Seg_1913" s="T141">then</ta>
            <ta e="T143" id="Seg_1914" s="T142">%%-HAB-PST-1SG.O</ta>
            <ta e="T144" id="Seg_1915" s="T143">net-ACC</ta>
            <ta e="T145" id="Seg_1916" s="T144">go-PST-3SG.S</ta>
            <ta e="T146" id="Seg_1917" s="T145">post-ILL</ta>
            <ta e="T147" id="Seg_1918" s="T146">one</ta>
            <ta e="T148" id="Seg_1919" s="T147">letter.[NOM]</ta>
            <ta e="T149" id="Seg_1920" s="T148">take-INFER-1SG.S</ta>
            <ta e="T150" id="Seg_1921" s="T149">executive.committee-EL</ta>
            <ta e="T151" id="Seg_1922" s="T150">come-PTCP.PST</ta>
            <ta e="T152" id="Seg_1923" s="T151">then</ta>
            <ta e="T153" id="Seg_1924" s="T152">come.in-CO-1SG.S</ta>
            <ta e="T154" id="Seg_1925" s="T153">%%-ILL</ta>
            <ta e="T155" id="Seg_1926" s="T154">%%</ta>
            <ta e="T156" id="Seg_1927" s="T155">move-CVB</ta>
            <ta e="T157" id="Seg_1928" s="T156">come-PTCP.PST</ta>
            <ta e="T158" id="Seg_1929" s="T157">old.woman-COM</ta>
            <ta e="T159" id="Seg_1930" s="T158">name-3SG</ta>
            <ta e="T160" id="Seg_1931" s="T159">he.NOM-EP-GEN</ta>
            <ta e="T161" id="Seg_1932" s="T160">Angelina</ta>
            <ta e="T162" id="Seg_1933" s="T161">Ivanovna</ta>
            <ta e="T163" id="Seg_1934" s="T162">speak-PST-1PL</ta>
            <ta e="T164" id="Seg_1935" s="T163">write-IPFV-PST-1PL</ta>
            <ta e="T210" id="Seg_1936" s="T164">Selkup</ta>
            <ta e="T165" id="Seg_1937" s="T210">on</ta>
            <ta e="T166" id="Seg_1938" s="T165">so-ADVZ</ta>
            <ta e="T167" id="Seg_1939" s="T166">pass.through-%%-HAB-PST-3PL</ta>
            <ta e="T168" id="Seg_1940" s="T167">day-PL-3SG</ta>
            <ta e="T169" id="Seg_1941" s="T168">morning-EP-ADV.EL</ta>
            <ta e="T170" id="Seg_1942" s="T169">morning-EP-something-EL.3SG</ta>
            <ta e="T171" id="Seg_1943" s="T170">evening-something-ILL2.3SG</ta>
            <ta e="T172" id="Seg_1944" s="T171">work-HAB-PST-1PL</ta>
            <ta e="T173" id="Seg_1945" s="T172">ten</ta>
            <ta e="T174" id="Seg_1946" s="T173">one</ta>
            <ta e="T175" id="Seg_1947" s="T174">superfluous</ta>
            <ta e="T176" id="Seg_1948" s="T175">ten</ta>
            <ta e="T177" id="Seg_1949" s="T176">hour-EP-GEN</ta>
            <ta e="T178" id="Seg_1950" s="T177">day-ILL.3SG</ta>
            <ta e="T179" id="Seg_1951" s="T178">Selkup.[NOM]</ta>
            <ta e="T180" id="Seg_1952" s="T179">husband-PL</ta>
            <ta e="T181" id="Seg_1953" s="T180">who-3SG</ta>
            <ta e="T182" id="Seg_1954" s="T181">Russian-PL</ta>
            <ta e="T183" id="Seg_1955" s="T182">NEG</ta>
            <ta e="T184" id="Seg_1956" s="T183">sit-FUT-3SG.S</ta>
            <ta e="T185" id="Seg_1957" s="T184">so.many</ta>
            <ta e="T186" id="Seg_1958" s="T185">he.NOM-EP-PL</ta>
            <ta e="T187" id="Seg_1959" s="T186">sit-HAB-CO-3PL</ta>
            <ta e="T188" id="Seg_1960" s="T187">NEG</ta>
            <ta e="T189" id="Seg_1961" s="T188">many</ta>
            <ta e="T190" id="Seg_1962" s="T189">go-IPFV-3PL</ta>
            <ta e="T191" id="Seg_1963" s="T190">fish-FRQ-EP-INF</ta>
            <ta e="T192" id="Seg_1964" s="T191">net-VBLZ-INF</ta>
            <ta e="T193" id="Seg_1965" s="T192">cowberry-CAP-INF</ta>
            <ta e="T194" id="Seg_1966" s="T193">he.NOM-EP-PL-ALL</ta>
            <ta e="T195" id="Seg_1967" s="T194">there-ADV.LOC</ta>
            <ta e="T196" id="Seg_1968" s="T195">good</ta>
            <ta e="T197" id="Seg_1969" s="T196">be-3SG.S</ta>
            <ta e="T198" id="Seg_1970" s="T197">mosquito.[NOM]</ta>
            <ta e="T199" id="Seg_1971" s="T198">bite-%%.[3SG.S]</ta>
            <ta e="T200" id="Seg_1972" s="T199">blackfly.[NOM]</ta>
            <ta e="T201" id="Seg_1973" s="T200">botfly.[NOM]</ta>
            <ta e="T202" id="Seg_1974" s="T201">burn-DRV-EP-3PL</ta>
            <ta e="T203" id="Seg_1975" s="T202">NEG</ta>
            <ta e="T204" id="Seg_1976" s="T203">sit-INF</ta>
            <ta e="T205" id="Seg_1977" s="T204">write-IPFV-INF</ta>
            <ta e="T206" id="Seg_1978" s="T205">read-EP-FUT-INF</ta>
            <ta e="T207" id="Seg_1979" s="T206">and</ta>
            <ta e="T208" id="Seg_1980" s="T207">say-HAB-INF</ta>
            <ta e="T209" id="Seg_1981" s="T208">Selkup-word-ACC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1982" s="T0">вечер-PROL</ta>
            <ta e="T2" id="Seg_1983" s="T1">съесть-EP-FRQ-CVB</ta>
            <ta e="T3" id="Seg_1984" s="T2">улечься.спать-CVB</ta>
            <ta e="T4" id="Seg_1985" s="T3">спать-CO-1SG.S</ta>
            <ta e="T5" id="Seg_1986" s="T4">проснуться-3SG.S</ta>
            <ta e="T6" id="Seg_1987" s="T5">полог-LOC.3SG</ta>
            <ta e="T7" id="Seg_1988" s="T6">много</ta>
            <ta e="T8" id="Seg_1989" s="T7">зарывать-RES-PST.NAR-3PL</ta>
            <ta e="T9" id="Seg_1990" s="T8">комар-PL</ta>
            <ta e="T10" id="Seg_1991" s="T9">лежать-1SG.S</ta>
            <ta e="T11" id="Seg_1992" s="T10">сам.1SG</ta>
            <ta e="T12" id="Seg_1993" s="T11">думать-1SG.S</ta>
            <ta e="T13" id="Seg_1994" s="T12">как</ta>
            <ta e="T14" id="Seg_1995" s="T13">полог-ILL.3SG</ta>
            <ta e="T15" id="Seg_1996" s="T14">забраться-RES-PST.NAR-3PL</ta>
            <ta e="T16" id="Seg_1997" s="T15">комар-PL</ta>
            <ta e="T17" id="Seg_1998" s="T16">полог-EP-GEN-верх-EP-ILL</ta>
            <ta e="T18" id="Seg_1999" s="T17">вверх.лицом</ta>
            <ta e="T19" id="Seg_2000" s="T18">лежать-CVB</ta>
            <ta e="T20" id="Seg_2001" s="T19">смотреть-EP-3SG.S</ta>
            <ta e="T21" id="Seg_2002" s="T20">полог-EP-GEN</ta>
            <ta e="T22" id="Seg_2003" s="T21">конец</ta>
            <ta e="T23" id="Seg_2004" s="T22">%%-PST.NAR.[NOM]</ta>
            <ta e="T24" id="Seg_2005" s="T23">маленький</ta>
            <ta e="T25" id="Seg_2006" s="T24">сын-DIM-1SG</ta>
            <ta e="T26" id="Seg_2007" s="T25">имя-3SG</ta>
            <ta e="T27" id="Seg_2008" s="T26">он.NOM-EP-GEN</ta>
            <ta e="T28" id="Seg_2009" s="T27">Вова.[NOM]</ta>
            <ta e="T29" id="Seg_2010" s="T28">спросить-IPFV-3SG.S</ta>
            <ta e="T30" id="Seg_2011" s="T29">ты.NOM</ta>
            <ta e="T31" id="Seg_2012" s="T30">сорвать-INCH-PST.NAR-2SG.O</ta>
            <ta e="T32" id="Seg_2013" s="T31">полог-EP-ACC</ta>
            <ta e="T33" id="Seg_2014" s="T32">он.NOM</ta>
            <ta e="T34" id="Seg_2015" s="T33">я.ALL</ta>
            <ta e="T35" id="Seg_2016" s="T34">сказать-3SG.S</ta>
            <ta e="T36" id="Seg_2017" s="T35">NEG</ta>
            <ta e="T37" id="Seg_2018" s="T36">я.NOM</ta>
            <ta e="T38" id="Seg_2019" s="T37">сорвать-INCH-PST.NAR-1SG.O</ta>
            <ta e="T39" id="Seg_2020" s="T38">он.NOM</ta>
            <ta e="T40" id="Seg_2021" s="T39">сказать-3SG.S</ta>
            <ta e="T41" id="Seg_2022" s="T40">сестра-1SG</ta>
            <ta e="T42" id="Seg_2023" s="T41">Катя.[NOM]</ta>
            <ta e="T43" id="Seg_2024" s="T42">сесть-CVB</ta>
            <ta e="T44" id="Seg_2025" s="T43">кровать-EP-GEN</ta>
            <ta e="T45" id="Seg_2026" s="T44">верх-EP-ILL</ta>
            <ta e="T46" id="Seg_2027" s="T45">он.NOM</ta>
            <ta e="T47" id="Seg_2028" s="T46">сорвать-INCH-PST.NAR-3SG.O</ta>
            <ta e="T48" id="Seg_2029" s="T47">дочь-DIM-ILL.3SG</ta>
            <ta e="T49" id="Seg_2030" s="T48">спросить-IPFV-3SG.S</ta>
            <ta e="T50" id="Seg_2031" s="T49">ты.NOM</ta>
            <ta e="T51" id="Seg_2032" s="T50">сорвать-INCH-PST.NAR-2SG.O</ta>
            <ta e="T52" id="Seg_2033" s="T51">он.NOM</ta>
            <ta e="T53" id="Seg_2034" s="T52">я.ALL</ta>
            <ta e="T54" id="Seg_2035" s="T53">сказать-3SG.S</ta>
            <ta e="T55" id="Seg_2036" s="T54">я.NOM</ta>
            <ta e="T56" id="Seg_2037" s="T55">тоже</ta>
            <ta e="T57" id="Seg_2038" s="T56">NEG</ta>
            <ta e="T58" id="Seg_2039" s="T57">сорвать-INCH-INF-PST-1SG.O</ta>
            <ta e="T59" id="Seg_2040" s="T58">и</ta>
            <ta e="T60" id="Seg_2041" s="T59">NEG</ta>
            <ta e="T61" id="Seg_2042" s="T60">прийти-HAB-PST-1SG.S</ta>
            <ta e="T62" id="Seg_2043" s="T61">тело-EP-ADV.LOC</ta>
            <ta e="T63" id="Seg_2044" s="T62">сторона-ILL</ta>
            <ta e="T64" id="Seg_2045" s="T63">так-ADVZ</ta>
            <ta e="T65" id="Seg_2046" s="T64">я.NOM</ta>
            <ta e="T66" id="Seg_2047" s="T65">и</ta>
            <ta e="T67" id="Seg_2048" s="T66">остаться-3SG.S</ta>
            <ta e="T68" id="Seg_2049" s="T67">NEG</ta>
            <ta e="T69" id="Seg_2050" s="T68">узнать-PFV-1SG.O</ta>
            <ta e="T70" id="Seg_2051" s="T69">дочь-ADJZ-человек-EP-ALL.3SG</ta>
            <ta e="T71" id="Seg_2052" s="T70">сказать-3SG.S</ta>
            <ta e="T72" id="Seg_2053" s="T71">сшить-IMP.2SG.O</ta>
            <ta e="T73" id="Seg_2054" s="T72">он.NOM</ta>
            <ta e="T74" id="Seg_2055" s="T73">сшить-CO-3SG.O</ta>
            <ta e="T75" id="Seg_2056" s="T74">я.NOM</ta>
            <ta e="T76" id="Seg_2057" s="T75">опять</ta>
            <ta e="T77" id="Seg_2058" s="T76">улечься.спать-CVB</ta>
            <ta e="T78" id="Seg_2059" s="T77">спать-EP-3SG.S</ta>
            <ta e="T79" id="Seg_2060" s="T78">утро-нечто-LOC</ta>
            <ta e="T80" id="Seg_2061" s="T79">четыре</ta>
            <ta e="T81" id="Seg_2062" s="T80">час-EP-ILL.3SG</ta>
            <ta e="T82" id="Seg_2063" s="T81">кто.[NOM]-INDEF4</ta>
            <ta e="T83" id="Seg_2064" s="T82">дверь-ACC</ta>
            <ta e="T84" id="Seg_2065" s="T83">стучать-FRQ-CO-3SG.O</ta>
            <ta e="T85" id="Seg_2066" s="T84">женщина-ADJZ-человек-EP-1SG</ta>
            <ta e="T86" id="Seg_2067" s="T85">встать-3SG.S</ta>
            <ta e="T87" id="Seg_2068" s="T86">спросить-IPFV-3SG.S</ta>
            <ta e="T88" id="Seg_2069" s="T87">кто</ta>
            <ta e="T89" id="Seg_2070" s="T88">там-ADV.LOC</ta>
            <ta e="T90" id="Seg_2071" s="T89">сын-1SG</ta>
            <ta e="T91" id="Seg_2072" s="T90">имя-3SG</ta>
            <ta e="T92" id="Seg_2073" s="T91">Михаил.[NOM]</ta>
            <ta e="T93" id="Seg_2074" s="T92">сказать-3SG.S</ta>
            <ta e="T94" id="Seg_2075" s="T93">говорить-DUR-3SG.S</ta>
            <ta e="T95" id="Seg_2076" s="T94">мать.[NOM]</ta>
            <ta e="T96" id="Seg_2077" s="T95">вода-ILL</ta>
            <ta e="T97" id="Seg_2078" s="T96">мать.[NOM]</ta>
            <ta e="T98" id="Seg_2079" s="T97">открыть-IMP.2SG.O</ta>
            <ta e="T99" id="Seg_2080" s="T98">дом-ILL</ta>
            <ta e="T100" id="Seg_2081" s="T99">я.NOM</ta>
            <ta e="T101" id="Seg_2082" s="T100">ехать-IPFV-3SG.S</ta>
            <ta e="T102" id="Seg_2083" s="T101">женщина-ADJZ-человек-EP-1SG</ta>
            <ta e="T103" id="Seg_2084" s="T102">дверь-ACC</ta>
            <ta e="T104" id="Seg_2085" s="T103">открыть-CO-3SG.O</ta>
            <ta e="T105" id="Seg_2086" s="T104">сын-1SG</ta>
            <ta e="T106" id="Seg_2087" s="T105">войти-CO.[3SG.S]</ta>
            <ta e="T107" id="Seg_2088" s="T106">дом-ILL</ta>
            <ta e="T108" id="Seg_2089" s="T107">сам.3SG</ta>
            <ta e="T109" id="Seg_2090" s="T108">друг-GEN-PL-EP-COM</ta>
            <ta e="T110" id="Seg_2091" s="T109">я.ADES</ta>
            <ta e="T111" id="Seg_2092" s="T110">спросить-IPFV-HAB-3SG.S</ta>
            <ta e="T112" id="Seg_2093" s="T111">сын-1SG</ta>
            <ta e="T113" id="Seg_2094" s="T112">ветка-TRL</ta>
            <ta e="T114" id="Seg_2095" s="T113">просить-FRQ-CO.[3SG.S]</ta>
            <ta e="T115" id="Seg_2096" s="T114">друг-1SG</ta>
            <ta e="T116" id="Seg_2097" s="T115">уйти-INF</ta>
            <ta e="T117" id="Seg_2098" s="T116">Пирино-ILL</ta>
            <ta e="T118" id="Seg_2099" s="T117">Пирино-ADJZ</ta>
            <ta e="T119" id="Seg_2100" s="T118">деревня-ILL</ta>
            <ta e="T120" id="Seg_2101" s="T119">я.NOM</ta>
            <ta e="T121" id="Seg_2102" s="T120">сказать-3SG.S</ta>
            <ta e="T122" id="Seg_2103" s="T121">он.NOM-EP-ALL</ta>
            <ta e="T123" id="Seg_2104" s="T122">сын-ILL.3SG</ta>
            <ta e="T124" id="Seg_2105" s="T123">сын-3SG</ta>
            <ta e="T125" id="Seg_2106" s="T124">дать-IMP.2SG.O</ta>
            <ta e="T126" id="Seg_2107" s="T125">ветка-ACC</ta>
            <ta e="T127" id="Seg_2108" s="T126">потом</ta>
            <ta e="T128" id="Seg_2109" s="T127">после.этого</ta>
            <ta e="T129" id="Seg_2110" s="T128">я.NOM</ta>
            <ta e="T130" id="Seg_2111" s="T129">NEG</ta>
            <ta e="T131" id="Seg_2112" s="T130">спать-PST-3SG.S</ta>
            <ta e="T132" id="Seg_2113" s="T131">уйти-CO-1SG.S</ta>
            <ta e="T133" id="Seg_2114" s="T132">смотреть-DUR-INF</ta>
            <ta e="T134" id="Seg_2115" s="T133">сеть-ACC</ta>
            <ta e="T135" id="Seg_2116" s="T134">съесть-EP-INF</ta>
            <ta e="T136" id="Seg_2117" s="T135">поймать-CO-1SG.O</ta>
            <ta e="T137" id="Seg_2118" s="T136">сеть-LOC</ta>
            <ta e="T138" id="Seg_2119" s="T137">рыба-ACC</ta>
            <ta e="T139" id="Seg_2120" s="T138">прийти-EP-3SG.S</ta>
            <ta e="T140" id="Seg_2121" s="T139">дом-ILL.3SG</ta>
            <ta e="T141" id="Seg_2122" s="T140">съесть-EP-FRQ-CO-3SG.S</ta>
            <ta e="T142" id="Seg_2123" s="T141">потом</ta>
            <ta e="T143" id="Seg_2124" s="T142">%%-HAB-PST-1SG.O</ta>
            <ta e="T144" id="Seg_2125" s="T143">сеть-ACC</ta>
            <ta e="T145" id="Seg_2126" s="T144">ходить-PST-3SG.S</ta>
            <ta e="T146" id="Seg_2127" s="T145">почта-ILL</ta>
            <ta e="T147" id="Seg_2128" s="T146">один</ta>
            <ta e="T148" id="Seg_2129" s="T147">письмо.[NOM]</ta>
            <ta e="T149" id="Seg_2130" s="T148">взять-INFER-1SG.S</ta>
            <ta e="T150" id="Seg_2131" s="T149">исполком-EL</ta>
            <ta e="T151" id="Seg_2132" s="T150">прийти-PTCP.PST</ta>
            <ta e="T152" id="Seg_2133" s="T151">потом</ta>
            <ta e="T153" id="Seg_2134" s="T152">войти-CO-1SG.S</ta>
            <ta e="T154" id="Seg_2135" s="T153">совет-ILL</ta>
            <ta e="T155" id="Seg_2136" s="T154">%%</ta>
            <ta e="T156" id="Seg_2137" s="T155">двигаться-CVB</ta>
            <ta e="T157" id="Seg_2138" s="T156">прийти-PTCP.PST</ta>
            <ta e="T158" id="Seg_2139" s="T157">старуха-COM</ta>
            <ta e="T159" id="Seg_2140" s="T158">имя-3SG</ta>
            <ta e="T160" id="Seg_2141" s="T159">он.NOM-EP-GEN</ta>
            <ta e="T161" id="Seg_2142" s="T160">Ангелина</ta>
            <ta e="T162" id="Seg_2143" s="T161">Ивановна</ta>
            <ta e="T163" id="Seg_2144" s="T162">говорить-PST-1PL</ta>
            <ta e="T164" id="Seg_2145" s="T163">писать-IPFV-PST-1PL</ta>
            <ta e="T210" id="Seg_2146" s="T164">селькупский</ta>
            <ta e="T165" id="Seg_2147" s="T210">по</ta>
            <ta e="T166" id="Seg_2148" s="T165">так-ADVZ</ta>
            <ta e="T167" id="Seg_2149" s="T166">пройти-%%-HAB-PST-3PL</ta>
            <ta e="T168" id="Seg_2150" s="T167">день-PL-3SG</ta>
            <ta e="T169" id="Seg_2151" s="T168">утро-EP-ADV.EL</ta>
            <ta e="T170" id="Seg_2152" s="T169">утро-EP-нечто-EL.3SG</ta>
            <ta e="T171" id="Seg_2153" s="T170">вечер-нечто-ILL2.3SG</ta>
            <ta e="T172" id="Seg_2154" s="T171">работать-HAB-PST-1PL</ta>
            <ta e="T173" id="Seg_2155" s="T172">десять</ta>
            <ta e="T174" id="Seg_2156" s="T173">один</ta>
            <ta e="T175" id="Seg_2157" s="T174">излишний</ta>
            <ta e="T176" id="Seg_2158" s="T175">десять</ta>
            <ta e="T177" id="Seg_2159" s="T176">час-EP-GEN</ta>
            <ta e="T178" id="Seg_2160" s="T177">день-ILL.3SG</ta>
            <ta e="T179" id="Seg_2161" s="T178">селькуп.[NOM]</ta>
            <ta e="T180" id="Seg_2162" s="T179">муж-PL</ta>
            <ta e="T181" id="Seg_2163" s="T180">кто-3SG</ta>
            <ta e="T182" id="Seg_2164" s="T181">русский-PL</ta>
            <ta e="T183" id="Seg_2165" s="T182">NEG</ta>
            <ta e="T184" id="Seg_2166" s="T183">сидеть-FUT-3SG.S</ta>
            <ta e="T185" id="Seg_2167" s="T184">столько</ta>
            <ta e="T186" id="Seg_2168" s="T185">он.NOM-EP-PL</ta>
            <ta e="T187" id="Seg_2169" s="T186">сидеть-HAB-CO-3PL</ta>
            <ta e="T188" id="Seg_2170" s="T187">NEG</ta>
            <ta e="T189" id="Seg_2171" s="T188">много</ta>
            <ta e="T190" id="Seg_2172" s="T189">идти-IPFV-3PL</ta>
            <ta e="T191" id="Seg_2173" s="T190">удить.рыбу-FRQ-EP-INF</ta>
            <ta e="T192" id="Seg_2174" s="T191">сеть-VBLZ-INF</ta>
            <ta e="T193" id="Seg_2175" s="T192">брусника-CAP-INF</ta>
            <ta e="T194" id="Seg_2176" s="T193">он.NOM-EP-PL-ALL</ta>
            <ta e="T195" id="Seg_2177" s="T194">там-ADV.LOC</ta>
            <ta e="T196" id="Seg_2178" s="T195">хороший</ta>
            <ta e="T197" id="Seg_2179" s="T196">быть-3SG.S</ta>
            <ta e="T198" id="Seg_2180" s="T197">комар.[NOM]</ta>
            <ta e="T199" id="Seg_2181" s="T198">кусать-%%.[3SG.S]</ta>
            <ta e="T200" id="Seg_2182" s="T199">мошка.[NOM]</ta>
            <ta e="T201" id="Seg_2183" s="T200">оводы.[NOM]</ta>
            <ta e="T202" id="Seg_2184" s="T201">гореть-DRV-EP-3PL</ta>
            <ta e="T203" id="Seg_2185" s="T202">NEG</ta>
            <ta e="T204" id="Seg_2186" s="T203">сидеть-INF</ta>
            <ta e="T205" id="Seg_2187" s="T204">писать-IPFV-INF</ta>
            <ta e="T206" id="Seg_2188" s="T205">читать-EP-FUT-INF</ta>
            <ta e="T207" id="Seg_2189" s="T206">и</ta>
            <ta e="T208" id="Seg_2190" s="T207">говорить-HAB-INF</ta>
            <ta e="T209" id="Seg_2191" s="T208">селькуп-слово-ACC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_2192" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_2193" s="T1">v-n:ins-v&gt;v-v&gt;adv</ta>
            <ta e="T3" id="Seg_2194" s="T2">v-v&gt;adv</ta>
            <ta e="T4" id="Seg_2195" s="T3">v-v:ins-v:pn</ta>
            <ta e="T5" id="Seg_2196" s="T4">v-v:pn</ta>
            <ta e="T6" id="Seg_2197" s="T5">n-n:case.poss</ta>
            <ta e="T7" id="Seg_2198" s="T6">quant</ta>
            <ta e="T8" id="Seg_2199" s="T7">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_2200" s="T8">n-n:num</ta>
            <ta e="T10" id="Seg_2201" s="T9">v-v:pn</ta>
            <ta e="T11" id="Seg_2202" s="T10">emphpro</ta>
            <ta e="T12" id="Seg_2203" s="T11">v-v:pn</ta>
            <ta e="T13" id="Seg_2204" s="T12">interrog</ta>
            <ta e="T14" id="Seg_2205" s="T13">n-n:case.poss</ta>
            <ta e="T15" id="Seg_2206" s="T14">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_2207" s="T15">n-n:num</ta>
            <ta e="T17" id="Seg_2208" s="T16">n-n:ins-n:case-n-n:ins-n:case</ta>
            <ta e="T18" id="Seg_2209" s="T17">adv</ta>
            <ta e="T19" id="Seg_2210" s="T18">v-v&gt;adv</ta>
            <ta e="T20" id="Seg_2211" s="T19">v-n:ins-v:pn</ta>
            <ta e="T21" id="Seg_2212" s="T20">n-n:ins-n:case</ta>
            <ta e="T22" id="Seg_2213" s="T21">n</ta>
            <ta e="T23" id="Seg_2214" s="T22">v-v:tense-n:case</ta>
            <ta e="T24" id="Seg_2215" s="T23">adj</ta>
            <ta e="T25" id="Seg_2216" s="T24">n-n&gt;n-n:poss</ta>
            <ta e="T26" id="Seg_2217" s="T25">n-n:poss</ta>
            <ta e="T27" id="Seg_2218" s="T26">pers-n:ins-n:case</ta>
            <ta e="T28" id="Seg_2219" s="T27">nprop-n:case</ta>
            <ta e="T29" id="Seg_2220" s="T28">v-v&gt;v-v:pn</ta>
            <ta e="T30" id="Seg_2221" s="T29">pers</ta>
            <ta e="T31" id="Seg_2222" s="T30">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_2223" s="T31">n-n:ins-n:case</ta>
            <ta e="T33" id="Seg_2224" s="T32">pers</ta>
            <ta e="T34" id="Seg_2225" s="T33">pers</ta>
            <ta e="T35" id="Seg_2226" s="T34">v-v:pn</ta>
            <ta e="T36" id="Seg_2227" s="T35">ptcl</ta>
            <ta e="T37" id="Seg_2228" s="T36">pers</ta>
            <ta e="T38" id="Seg_2229" s="T37">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_2230" s="T38">pers</ta>
            <ta e="T40" id="Seg_2231" s="T39">v-v:pn</ta>
            <ta e="T41" id="Seg_2232" s="T40">n-n:poss</ta>
            <ta e="T42" id="Seg_2233" s="T41">nprop-n:case</ta>
            <ta e="T43" id="Seg_2234" s="T42">v-v&gt;adv</ta>
            <ta e="T44" id="Seg_2235" s="T43">n-n:ins-n:case</ta>
            <ta e="T45" id="Seg_2236" s="T44">n-n:ins-n:case</ta>
            <ta e="T46" id="Seg_2237" s="T45">pers</ta>
            <ta e="T47" id="Seg_2238" s="T46">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_2239" s="T47">n-n&gt;n-n:case.poss</ta>
            <ta e="T49" id="Seg_2240" s="T48">v-v&gt;v-v:pn</ta>
            <ta e="T50" id="Seg_2241" s="T49">pers</ta>
            <ta e="T51" id="Seg_2242" s="T50">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_2243" s="T51">pers</ta>
            <ta e="T53" id="Seg_2244" s="T52">pers</ta>
            <ta e="T54" id="Seg_2245" s="T53">v-v:pn</ta>
            <ta e="T55" id="Seg_2246" s="T54">pers</ta>
            <ta e="T56" id="Seg_2247" s="T55">ptcl</ta>
            <ta e="T57" id="Seg_2248" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_2249" s="T57">v-v&gt;v-v:inf-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_2250" s="T58">conj</ta>
            <ta e="T60" id="Seg_2251" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_2252" s="T60">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_2253" s="T61">n-n:ins-adv:case</ta>
            <ta e="T63" id="Seg_2254" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_2255" s="T63">adv-adj&gt;adv</ta>
            <ta e="T65" id="Seg_2256" s="T64">pers</ta>
            <ta e="T66" id="Seg_2257" s="T65">conj</ta>
            <ta e="T67" id="Seg_2258" s="T66">v-v:pn</ta>
            <ta e="T68" id="Seg_2259" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_2260" s="T68">v-v&gt;v-v:pn</ta>
            <ta e="T70" id="Seg_2261" s="T69">n-n&gt;adj-n-n:ins-n:case.poss</ta>
            <ta e="T71" id="Seg_2262" s="T70">v-v:pn</ta>
            <ta e="T72" id="Seg_2263" s="T71">v-v:mood.pn</ta>
            <ta e="T73" id="Seg_2264" s="T72">pers</ta>
            <ta e="T74" id="Seg_2265" s="T73">v-v:ins-v:pn</ta>
            <ta e="T75" id="Seg_2266" s="T74">pers</ta>
            <ta e="T76" id="Seg_2267" s="T75">adv</ta>
            <ta e="T77" id="Seg_2268" s="T76">v-v&gt;adv</ta>
            <ta e="T78" id="Seg_2269" s="T77">v-n:ins-v:pn</ta>
            <ta e="T79" id="Seg_2270" s="T78">n-n-n:case</ta>
            <ta e="T80" id="Seg_2271" s="T79">num</ta>
            <ta e="T81" id="Seg_2272" s="T80">n-n:ins-n:case.poss</ta>
            <ta e="T82" id="Seg_2273" s="T81">interrog-n:case-clit</ta>
            <ta e="T83" id="Seg_2274" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_2275" s="T83">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T85" id="Seg_2276" s="T84">n-n&gt;adj-n-n:ins-n:poss</ta>
            <ta e="T86" id="Seg_2277" s="T85">v-v:pn</ta>
            <ta e="T87" id="Seg_2278" s="T86">v-v&gt;v-v:pn</ta>
            <ta e="T88" id="Seg_2279" s="T87">interrog</ta>
            <ta e="T89" id="Seg_2280" s="T88">adv-adv:case</ta>
            <ta e="T90" id="Seg_2281" s="T89">n-n:poss</ta>
            <ta e="T91" id="Seg_2282" s="T90">n-n:poss</ta>
            <ta e="T92" id="Seg_2283" s="T91">n-n:case</ta>
            <ta e="T93" id="Seg_2284" s="T92">v-v:pn</ta>
            <ta e="T94" id="Seg_2285" s="T93">v-v&gt;v-v:pn</ta>
            <ta e="T95" id="Seg_2286" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_2287" s="T95">n-n:case</ta>
            <ta e="T97" id="Seg_2288" s="T96">n-n:case</ta>
            <ta e="T98" id="Seg_2289" s="T97">v-v:mood.pn</ta>
            <ta e="T99" id="Seg_2290" s="T98">n-n:case</ta>
            <ta e="T100" id="Seg_2291" s="T99">pers</ta>
            <ta e="T101" id="Seg_2292" s="T100">v-v&gt;v-v:pn</ta>
            <ta e="T102" id="Seg_2293" s="T101">n-n&gt;adj-n-n:ins-n:poss</ta>
            <ta e="T103" id="Seg_2294" s="T102">n-n:case</ta>
            <ta e="T104" id="Seg_2295" s="T103">v-v:ins-v:pn</ta>
            <ta e="T105" id="Seg_2296" s="T104">n-n:poss</ta>
            <ta e="T106" id="Seg_2297" s="T105">v-v:ins.[v:pn]</ta>
            <ta e="T107" id="Seg_2298" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_2299" s="T107">emphpro</ta>
            <ta e="T109" id="Seg_2300" s="T108">n-n:case-n:num-n:ins-n:case</ta>
            <ta e="T110" id="Seg_2301" s="T109">pers</ta>
            <ta e="T111" id="Seg_2302" s="T110">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T112" id="Seg_2303" s="T111">n-n:poss</ta>
            <ta e="T113" id="Seg_2304" s="T112">n-n:case</ta>
            <ta e="T114" id="Seg_2305" s="T113">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T115" id="Seg_2306" s="T114">n-n:poss</ta>
            <ta e="T116" id="Seg_2307" s="T115">v-v:inf</ta>
            <ta e="T117" id="Seg_2308" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_2309" s="T117">n-n&gt;adj</ta>
            <ta e="T119" id="Seg_2310" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_2311" s="T119">pers</ta>
            <ta e="T121" id="Seg_2312" s="T120">v-v:pn</ta>
            <ta e="T122" id="Seg_2313" s="T121">pers-n:ins-n:case</ta>
            <ta e="T123" id="Seg_2314" s="T122">n-n:case.poss</ta>
            <ta e="T124" id="Seg_2315" s="T123">n-n:poss</ta>
            <ta e="T125" id="Seg_2316" s="T124">v-v:mood.pn</ta>
            <ta e="T126" id="Seg_2317" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_2318" s="T126">adv</ta>
            <ta e="T128" id="Seg_2319" s="T127">adv</ta>
            <ta e="T129" id="Seg_2320" s="T128">pers</ta>
            <ta e="T130" id="Seg_2321" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_2322" s="T130">v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_2323" s="T131">v-v:ins-v:pn</ta>
            <ta e="T133" id="Seg_2324" s="T132">v-v&gt;v-v:inf</ta>
            <ta e="T134" id="Seg_2325" s="T133">n-n:case</ta>
            <ta e="T135" id="Seg_2326" s="T134">v-n:ins-v:inf</ta>
            <ta e="T136" id="Seg_2327" s="T135">v-v:ins-v:pn</ta>
            <ta e="T137" id="Seg_2328" s="T136">n-n:case</ta>
            <ta e="T138" id="Seg_2329" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_2330" s="T138">v-n:ins-v:pn</ta>
            <ta e="T140" id="Seg_2331" s="T139">n-n:case.poss</ta>
            <ta e="T141" id="Seg_2332" s="T140">v-n:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T142" id="Seg_2333" s="T141">adv</ta>
            <ta e="T143" id="Seg_2334" s="T142">v&gt;v-v:tense-v:pn</ta>
            <ta e="T144" id="Seg_2335" s="T143">n-n:case</ta>
            <ta e="T145" id="Seg_2336" s="T144">v-v:tense-v:pn</ta>
            <ta e="T146" id="Seg_2337" s="T145">n-n:case</ta>
            <ta e="T147" id="Seg_2338" s="T146">num</ta>
            <ta e="T148" id="Seg_2339" s="T147">n-n:case</ta>
            <ta e="T149" id="Seg_2340" s="T148">v-v:mood-v:pn</ta>
            <ta e="T150" id="Seg_2341" s="T149">n-n:case</ta>
            <ta e="T151" id="Seg_2342" s="T150">v-v&gt;ptcp</ta>
            <ta e="T152" id="Seg_2343" s="T151">adv</ta>
            <ta e="T153" id="Seg_2344" s="T152">v-v:ins-v:pn</ta>
            <ta e="T154" id="Seg_2345" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_2346" s="T154">%%</ta>
            <ta e="T156" id="Seg_2347" s="T155">v-v&gt;adv</ta>
            <ta e="T157" id="Seg_2348" s="T156">v-v&gt;ptcp</ta>
            <ta e="T158" id="Seg_2349" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_2350" s="T158">n-n:poss</ta>
            <ta e="T160" id="Seg_2351" s="T159">pers-n:ins-n:case</ta>
            <ta e="T161" id="Seg_2352" s="T160">nprop</ta>
            <ta e="T162" id="Seg_2353" s="T161">nprop</ta>
            <ta e="T163" id="Seg_2354" s="T162">v-v:tense-v:pn</ta>
            <ta e="T164" id="Seg_2355" s="T163">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T210" id="Seg_2356" s="T164">adj</ta>
            <ta e="T165" id="Seg_2357" s="T210">pp</ta>
            <ta e="T166" id="Seg_2358" s="T165">adv-adj&gt;adv</ta>
            <ta e="T167" id="Seg_2359" s="T166">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T168" id="Seg_2360" s="T167">n-n:num-n:poss</ta>
            <ta e="T169" id="Seg_2361" s="T168">n-n:ins-adv:case</ta>
            <ta e="T170" id="Seg_2362" s="T169">n-n:ins-n-n:case.poss</ta>
            <ta e="T171" id="Seg_2363" s="T170">n-n-n:case.poss</ta>
            <ta e="T172" id="Seg_2364" s="T171">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T173" id="Seg_2365" s="T172">num</ta>
            <ta e="T174" id="Seg_2366" s="T173">num</ta>
            <ta e="T175" id="Seg_2367" s="T174">adj</ta>
            <ta e="T176" id="Seg_2368" s="T175">num</ta>
            <ta e="T177" id="Seg_2369" s="T176">n-n:ins-n:case</ta>
            <ta e="T178" id="Seg_2370" s="T177">n-n:case.poss</ta>
            <ta e="T179" id="Seg_2371" s="T178">n-n:case</ta>
            <ta e="T180" id="Seg_2372" s="T179">n-n:num</ta>
            <ta e="T181" id="Seg_2373" s="T180">interrog-n:poss</ta>
            <ta e="T182" id="Seg_2374" s="T181">n-n:num</ta>
            <ta e="T183" id="Seg_2375" s="T182">ptcl</ta>
            <ta e="T184" id="Seg_2376" s="T183">v-v:tense-v:pn</ta>
            <ta e="T185" id="Seg_2377" s="T184">adv</ta>
            <ta e="T186" id="Seg_2378" s="T185">pers-n:ins-n:num</ta>
            <ta e="T187" id="Seg_2379" s="T186">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T188" id="Seg_2380" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_2381" s="T188">quant</ta>
            <ta e="T190" id="Seg_2382" s="T189">v-v&gt;v-v:pn</ta>
            <ta e="T191" id="Seg_2383" s="T190">v-v&gt;v-n:ins-v:inf</ta>
            <ta e="T192" id="Seg_2384" s="T191">n-n&gt;v-v:inf</ta>
            <ta e="T193" id="Seg_2385" s="T192">n-n&gt;v-v:inf</ta>
            <ta e="T194" id="Seg_2386" s="T193">pers-n:ins-n:num-n:case</ta>
            <ta e="T195" id="Seg_2387" s="T194">adv-adv:case</ta>
            <ta e="T196" id="Seg_2388" s="T195">adj</ta>
            <ta e="T197" id="Seg_2389" s="T196">v-v:pn</ta>
            <ta e="T198" id="Seg_2390" s="T197">n-n:case</ta>
            <ta e="T199" id="Seg_2391" s="T198">v-v&gt;v-v:pn</ta>
            <ta e="T200" id="Seg_2392" s="T199">n-n:case</ta>
            <ta e="T201" id="Seg_2393" s="T200">n-n:case</ta>
            <ta e="T202" id="Seg_2394" s="T201">v-v&gt;v-n:ins-v:pn</ta>
            <ta e="T203" id="Seg_2395" s="T202">ptcl</ta>
            <ta e="T204" id="Seg_2396" s="T203">v-v:inf</ta>
            <ta e="T205" id="Seg_2397" s="T204">v-v&gt;v-v:inf</ta>
            <ta e="T206" id="Seg_2398" s="T205">v-n:ins-v:tense-v:inf</ta>
            <ta e="T207" id="Seg_2399" s="T206">conj</ta>
            <ta e="T208" id="Seg_2400" s="T207">v-v&gt;v-v:inf</ta>
            <ta e="T209" id="Seg_2401" s="T208">n-n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2402" s="T0">n</ta>
            <ta e="T2" id="Seg_2403" s="T1">v</ta>
            <ta e="T3" id="Seg_2404" s="T2">adv</ta>
            <ta e="T4" id="Seg_2405" s="T3">v</ta>
            <ta e="T5" id="Seg_2406" s="T4">v</ta>
            <ta e="T6" id="Seg_2407" s="T5">n</ta>
            <ta e="T7" id="Seg_2408" s="T6">quant</ta>
            <ta e="T8" id="Seg_2409" s="T7">v</ta>
            <ta e="T9" id="Seg_2410" s="T8">n</ta>
            <ta e="T10" id="Seg_2411" s="T9">v</ta>
            <ta e="T11" id="Seg_2412" s="T10">emphpro</ta>
            <ta e="T12" id="Seg_2413" s="T11">v</ta>
            <ta e="T13" id="Seg_2414" s="T12">interrog</ta>
            <ta e="T14" id="Seg_2415" s="T13">n</ta>
            <ta e="T15" id="Seg_2416" s="T14">v</ta>
            <ta e="T16" id="Seg_2417" s="T15">n</ta>
            <ta e="T17" id="Seg_2418" s="T16">n</ta>
            <ta e="T18" id="Seg_2419" s="T17">adv</ta>
            <ta e="T19" id="Seg_2420" s="T18">adv</ta>
            <ta e="T20" id="Seg_2421" s="T19">v</ta>
            <ta e="T21" id="Seg_2422" s="T20">n</ta>
            <ta e="T22" id="Seg_2423" s="T21">n</ta>
            <ta e="T23" id="Seg_2424" s="T22">v</ta>
            <ta e="T24" id="Seg_2425" s="T23">n</ta>
            <ta e="T25" id="Seg_2426" s="T24">n</ta>
            <ta e="T26" id="Seg_2427" s="T25">n</ta>
            <ta e="T27" id="Seg_2428" s="T26">pers</ta>
            <ta e="T28" id="Seg_2429" s="T27">nprop</ta>
            <ta e="T29" id="Seg_2430" s="T28">v</ta>
            <ta e="T30" id="Seg_2431" s="T29">pers</ta>
            <ta e="T31" id="Seg_2432" s="T30">v</ta>
            <ta e="T32" id="Seg_2433" s="T31">n</ta>
            <ta e="T33" id="Seg_2434" s="T32">pers</ta>
            <ta e="T34" id="Seg_2435" s="T33">pers</ta>
            <ta e="T35" id="Seg_2436" s="T34">v</ta>
            <ta e="T36" id="Seg_2437" s="T35">ptcl</ta>
            <ta e="T37" id="Seg_2438" s="T36">pers</ta>
            <ta e="T38" id="Seg_2439" s="T37">v</ta>
            <ta e="T39" id="Seg_2440" s="T38">pers</ta>
            <ta e="T40" id="Seg_2441" s="T39">v</ta>
            <ta e="T41" id="Seg_2442" s="T40">n</ta>
            <ta e="T42" id="Seg_2443" s="T41">nprop</ta>
            <ta e="T43" id="Seg_2444" s="T42">adv</ta>
            <ta e="T44" id="Seg_2445" s="T43">n</ta>
            <ta e="T45" id="Seg_2446" s="T44">n</ta>
            <ta e="T46" id="Seg_2447" s="T45">pers</ta>
            <ta e="T47" id="Seg_2448" s="T46">v</ta>
            <ta e="T48" id="Seg_2449" s="T47">n</ta>
            <ta e="T49" id="Seg_2450" s="T48">v</ta>
            <ta e="T50" id="Seg_2451" s="T49">pers</ta>
            <ta e="T51" id="Seg_2452" s="T50">v</ta>
            <ta e="T52" id="Seg_2453" s="T51">pers</ta>
            <ta e="T53" id="Seg_2454" s="T52">pers</ta>
            <ta e="T54" id="Seg_2455" s="T53">v</ta>
            <ta e="T55" id="Seg_2456" s="T54">pers</ta>
            <ta e="T56" id="Seg_2457" s="T55">ptcl</ta>
            <ta e="T57" id="Seg_2458" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_2459" s="T57">v</ta>
            <ta e="T59" id="Seg_2460" s="T58">conj</ta>
            <ta e="T60" id="Seg_2461" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_2462" s="T60">v</ta>
            <ta e="T62" id="Seg_2463" s="T61">n</ta>
            <ta e="T63" id="Seg_2464" s="T62">n</ta>
            <ta e="T64" id="Seg_2465" s="T63">adv</ta>
            <ta e="T65" id="Seg_2466" s="T64">pers</ta>
            <ta e="T66" id="Seg_2467" s="T65">conj</ta>
            <ta e="T67" id="Seg_2468" s="T66">v</ta>
            <ta e="T68" id="Seg_2469" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_2470" s="T68">v</ta>
            <ta e="T70" id="Seg_2471" s="T69">n</ta>
            <ta e="T71" id="Seg_2472" s="T70">v</ta>
            <ta e="T72" id="Seg_2473" s="T71">v</ta>
            <ta e="T73" id="Seg_2474" s="T72">pers</ta>
            <ta e="T74" id="Seg_2475" s="T73">v</ta>
            <ta e="T75" id="Seg_2476" s="T74">pers</ta>
            <ta e="T76" id="Seg_2477" s="T75">adv</ta>
            <ta e="T77" id="Seg_2478" s="T76">adv</ta>
            <ta e="T78" id="Seg_2479" s="T77">v</ta>
            <ta e="T79" id="Seg_2480" s="T78">n</ta>
            <ta e="T80" id="Seg_2481" s="T79">num</ta>
            <ta e="T81" id="Seg_2482" s="T80">n</ta>
            <ta e="T82" id="Seg_2483" s="T81">interrog</ta>
            <ta e="T83" id="Seg_2484" s="T82">n</ta>
            <ta e="T84" id="Seg_2485" s="T83">v</ta>
            <ta e="T85" id="Seg_2486" s="T84">adj</ta>
            <ta e="T86" id="Seg_2487" s="T85">v</ta>
            <ta e="T87" id="Seg_2488" s="T86">v</ta>
            <ta e="T88" id="Seg_2489" s="T87">interrog</ta>
            <ta e="T89" id="Seg_2490" s="T88">adv</ta>
            <ta e="T90" id="Seg_2491" s="T89">n</ta>
            <ta e="T91" id="Seg_2492" s="T90">n</ta>
            <ta e="T92" id="Seg_2493" s="T91">n</ta>
            <ta e="T93" id="Seg_2494" s="T92">v</ta>
            <ta e="T94" id="Seg_2495" s="T93">v</ta>
            <ta e="T95" id="Seg_2496" s="T94">n</ta>
            <ta e="T96" id="Seg_2497" s="T95">n</ta>
            <ta e="T97" id="Seg_2498" s="T96">n</ta>
            <ta e="T98" id="Seg_2499" s="T97">v</ta>
            <ta e="T99" id="Seg_2500" s="T98">n</ta>
            <ta e="T100" id="Seg_2501" s="T99">v</ta>
            <ta e="T101" id="Seg_2502" s="T100">v</ta>
            <ta e="T102" id="Seg_2503" s="T101">adj</ta>
            <ta e="T103" id="Seg_2504" s="T102">n</ta>
            <ta e="T104" id="Seg_2505" s="T103">v</ta>
            <ta e="T105" id="Seg_2506" s="T104">n</ta>
            <ta e="T106" id="Seg_2507" s="T105">v</ta>
            <ta e="T107" id="Seg_2508" s="T106">n</ta>
            <ta e="T108" id="Seg_2509" s="T107">emphpro</ta>
            <ta e="T109" id="Seg_2510" s="T108">n</ta>
            <ta e="T110" id="Seg_2511" s="T109">pers</ta>
            <ta e="T111" id="Seg_2512" s="T110">v</ta>
            <ta e="T112" id="Seg_2513" s="T111">n</ta>
            <ta e="T113" id="Seg_2514" s="T112">n</ta>
            <ta e="T114" id="Seg_2515" s="T113">v</ta>
            <ta e="T115" id="Seg_2516" s="T114">n</ta>
            <ta e="T116" id="Seg_2517" s="T115">v</ta>
            <ta e="T117" id="Seg_2518" s="T116">n</ta>
            <ta e="T118" id="Seg_2519" s="T117">adj</ta>
            <ta e="T119" id="Seg_2520" s="T118">n</ta>
            <ta e="T120" id="Seg_2521" s="T119">pers</ta>
            <ta e="T121" id="Seg_2522" s="T120">v</ta>
            <ta e="T122" id="Seg_2523" s="T121">pers</ta>
            <ta e="T123" id="Seg_2524" s="T122">n</ta>
            <ta e="T124" id="Seg_2525" s="T123">n</ta>
            <ta e="T125" id="Seg_2526" s="T124">v</ta>
            <ta e="T126" id="Seg_2527" s="T125">n</ta>
            <ta e="T127" id="Seg_2528" s="T126">adv</ta>
            <ta e="T128" id="Seg_2529" s="T127">adv</ta>
            <ta e="T129" id="Seg_2530" s="T128">pers</ta>
            <ta e="T130" id="Seg_2531" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_2532" s="T130">v</ta>
            <ta e="T132" id="Seg_2533" s="T131">v</ta>
            <ta e="T133" id="Seg_2534" s="T132">v</ta>
            <ta e="T134" id="Seg_2535" s="T133">n</ta>
            <ta e="T135" id="Seg_2536" s="T134">v</ta>
            <ta e="T136" id="Seg_2537" s="T135">v</ta>
            <ta e="T137" id="Seg_2538" s="T136">n</ta>
            <ta e="T138" id="Seg_2539" s="T137">n</ta>
            <ta e="T139" id="Seg_2540" s="T138">v</ta>
            <ta e="T140" id="Seg_2541" s="T139">n</ta>
            <ta e="T141" id="Seg_2542" s="T140">v</ta>
            <ta e="T142" id="Seg_2543" s="T141">adv</ta>
            <ta e="T143" id="Seg_2544" s="T142">v</ta>
            <ta e="T144" id="Seg_2545" s="T143">n</ta>
            <ta e="T145" id="Seg_2546" s="T144">v</ta>
            <ta e="T146" id="Seg_2547" s="T145">n</ta>
            <ta e="T147" id="Seg_2548" s="T146">num</ta>
            <ta e="T148" id="Seg_2549" s="T147">n</ta>
            <ta e="T149" id="Seg_2550" s="T148">v</ta>
            <ta e="T150" id="Seg_2551" s="T149">n</ta>
            <ta e="T151" id="Seg_2552" s="T150">v</ta>
            <ta e="T152" id="Seg_2553" s="T151">adv</ta>
            <ta e="T153" id="Seg_2554" s="T152">v</ta>
            <ta e="T154" id="Seg_2555" s="T153">n</ta>
            <ta e="T155" id="Seg_2556" s="T154">%%</ta>
            <ta e="T156" id="Seg_2557" s="T155">adv</ta>
            <ta e="T157" id="Seg_2558" s="T156">ptcp</ta>
            <ta e="T158" id="Seg_2559" s="T157">n</ta>
            <ta e="T159" id="Seg_2560" s="T158">n</ta>
            <ta e="T160" id="Seg_2561" s="T159">pers</ta>
            <ta e="T161" id="Seg_2562" s="T160">nprop</ta>
            <ta e="T162" id="Seg_2563" s="T161">nprop</ta>
            <ta e="T163" id="Seg_2564" s="T162">v</ta>
            <ta e="T164" id="Seg_2565" s="T163">v</ta>
            <ta e="T210" id="Seg_2566" s="T164">adj</ta>
            <ta e="T165" id="Seg_2567" s="T210">pp</ta>
            <ta e="T166" id="Seg_2568" s="T165">adv</ta>
            <ta e="T167" id="Seg_2569" s="T166">v</ta>
            <ta e="T168" id="Seg_2570" s="T167">n</ta>
            <ta e="T169" id="Seg_2571" s="T168">n</ta>
            <ta e="T170" id="Seg_2572" s="T169">n</ta>
            <ta e="T171" id="Seg_2573" s="T170">n</ta>
            <ta e="T172" id="Seg_2574" s="T171">v</ta>
            <ta e="T173" id="Seg_2575" s="T172">num</ta>
            <ta e="T174" id="Seg_2576" s="T173">num</ta>
            <ta e="T175" id="Seg_2577" s="T174">adj</ta>
            <ta e="T176" id="Seg_2578" s="T175">num</ta>
            <ta e="T177" id="Seg_2579" s="T176">n</ta>
            <ta e="T178" id="Seg_2580" s="T177">n</ta>
            <ta e="T179" id="Seg_2581" s="T178">n</ta>
            <ta e="T180" id="Seg_2582" s="T179">n</ta>
            <ta e="T181" id="Seg_2583" s="T180">interrog</ta>
            <ta e="T182" id="Seg_2584" s="T181">n</ta>
            <ta e="T183" id="Seg_2585" s="T182">ptcl</ta>
            <ta e="T184" id="Seg_2586" s="T183">v</ta>
            <ta e="T185" id="Seg_2587" s="T184">adv</ta>
            <ta e="T186" id="Seg_2588" s="T185">pers</ta>
            <ta e="T187" id="Seg_2589" s="T186">v</ta>
            <ta e="T188" id="Seg_2590" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_2591" s="T188">quant</ta>
            <ta e="T190" id="Seg_2592" s="T189">v</ta>
            <ta e="T191" id="Seg_2593" s="T190">v</ta>
            <ta e="T192" id="Seg_2594" s="T191">v</ta>
            <ta e="T193" id="Seg_2595" s="T192">n</ta>
            <ta e="T194" id="Seg_2596" s="T193">pers</ta>
            <ta e="T195" id="Seg_2597" s="T194">adv</ta>
            <ta e="T196" id="Seg_2598" s="T195">adj</ta>
            <ta e="T197" id="Seg_2599" s="T196">v</ta>
            <ta e="T198" id="Seg_2600" s="T197">n</ta>
            <ta e="T199" id="Seg_2601" s="T198">v</ta>
            <ta e="T200" id="Seg_2602" s="T199">n</ta>
            <ta e="T201" id="Seg_2603" s="T200">n</ta>
            <ta e="T202" id="Seg_2604" s="T201">v</ta>
            <ta e="T203" id="Seg_2605" s="T202">ptcl</ta>
            <ta e="T204" id="Seg_2606" s="T203">v</ta>
            <ta e="T205" id="Seg_2607" s="T204">v</ta>
            <ta e="T206" id="Seg_2608" s="T205">v</ta>
            <ta e="T207" id="Seg_2609" s="T206">conj</ta>
            <ta e="T208" id="Seg_2610" s="T207">v</ta>
            <ta e="T209" id="Seg_2611" s="T208">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_2612" s="T0">np:Time</ta>
            <ta e="T2" id="Seg_2613" s="T1">0.1.h:A</ta>
            <ta e="T3" id="Seg_2614" s="T2">0.1.h:A</ta>
            <ta e="T4" id="Seg_2615" s="T3">0.1.h:P</ta>
            <ta e="T5" id="Seg_2616" s="T4">0.1.h:P</ta>
            <ta e="T6" id="Seg_2617" s="T5">np:L</ta>
            <ta e="T9" id="Seg_2618" s="T8">np:A</ta>
            <ta e="T10" id="Seg_2619" s="T9">0.1.h:Th</ta>
            <ta e="T12" id="Seg_2620" s="T11">0.1.h:E</ta>
            <ta e="T14" id="Seg_2621" s="T13">np:G</ta>
            <ta e="T16" id="Seg_2622" s="T15">np:A</ta>
            <ta e="T17" id="Seg_2623" s="T16">np:G</ta>
            <ta e="T19" id="Seg_2624" s="T18">0.1.h:Th</ta>
            <ta e="T20" id="Seg_2625" s="T19">0.1.h:A</ta>
            <ta e="T21" id="Seg_2626" s="T20">np:Poss</ta>
            <ta e="T22" id="Seg_2627" s="T21">np:P</ta>
            <ta e="T25" id="Seg_2628" s="T24">np.h:R 0.1.h:Poss</ta>
            <ta e="T26" id="Seg_2629" s="T25">np:Th </ta>
            <ta e="T27" id="Seg_2630" s="T26">pro.h:Poss</ta>
            <ta e="T29" id="Seg_2631" s="T28">0.1.h:A</ta>
            <ta e="T30" id="Seg_2632" s="T29">pro.h:A</ta>
            <ta e="T32" id="Seg_2633" s="T31">np:P</ta>
            <ta e="T33" id="Seg_2634" s="T32">pro.h:A</ta>
            <ta e="T34" id="Seg_2635" s="T33">pro.h:R</ta>
            <ta e="T37" id="Seg_2636" s="T36">pro.h:A</ta>
            <ta e="T38" id="Seg_2637" s="T37">0.3:P</ta>
            <ta e="T39" id="Seg_2638" s="T38">pro.h:A</ta>
            <ta e="T41" id="Seg_2639" s="T40">np.h:A 0.1.h:Poss</ta>
            <ta e="T44" id="Seg_2640" s="T43">np:Poss</ta>
            <ta e="T45" id="Seg_2641" s="T44">np:G</ta>
            <ta e="T46" id="Seg_2642" s="T45">pro.h:A</ta>
            <ta e="T47" id="Seg_2643" s="T46">0.3:P</ta>
            <ta e="T48" id="Seg_2644" s="T47">np.h:R</ta>
            <ta e="T49" id="Seg_2645" s="T48">0.1.h:A</ta>
            <ta e="T50" id="Seg_2646" s="T49">pro.h:A</ta>
            <ta e="T51" id="Seg_2647" s="T50">0.3:P</ta>
            <ta e="T52" id="Seg_2648" s="T51">pro.h:A</ta>
            <ta e="T53" id="Seg_2649" s="T52">pro.h:R</ta>
            <ta e="T55" id="Seg_2650" s="T54">pro.h:A</ta>
            <ta e="T58" id="Seg_2651" s="T57">0.3:P</ta>
            <ta e="T61" id="Seg_2652" s="T60">0.1.h:A</ta>
            <ta e="T63" id="Seg_2653" s="T62">np:G</ta>
            <ta e="T65" id="Seg_2654" s="T64">pro.h:Th</ta>
            <ta e="T69" id="Seg_2655" s="T68">0.1.h:E 0.3:Th</ta>
            <ta e="T70" id="Seg_2656" s="T69">np.h:R</ta>
            <ta e="T71" id="Seg_2657" s="T70">0.1.h:A</ta>
            <ta e="T72" id="Seg_2658" s="T71">0.2.h:A 0.3:P</ta>
            <ta e="T73" id="Seg_2659" s="T72">pro.h:A</ta>
            <ta e="T74" id="Seg_2660" s="T73">0.3:P</ta>
            <ta e="T75" id="Seg_2661" s="T74">pro.h:P</ta>
            <ta e="T77" id="Seg_2662" s="T76">0.1.h:A</ta>
            <ta e="T79" id="Seg_2663" s="T78">np:Time</ta>
            <ta e="T81" id="Seg_2664" s="T80">np:Time</ta>
            <ta e="T82" id="Seg_2665" s="T81">pro.h:A</ta>
            <ta e="T83" id="Seg_2666" s="T82">np:P</ta>
            <ta e="T85" id="Seg_2667" s="T84">np.h:A 0.1.h:Poss</ta>
            <ta e="T87" id="Seg_2668" s="T86">0.3.h:A</ta>
            <ta e="T88" id="Seg_2669" s="T87">pro.h:Th</ta>
            <ta e="T89" id="Seg_2670" s="T88">adv:L</ta>
            <ta e="T90" id="Seg_2671" s="T89">np.h:A 0.1.h:Poss</ta>
            <ta e="T91" id="Seg_2672" s="T90">np:Th 0.3.h:Poss</ta>
            <ta e="T98" id="Seg_2673" s="T97">0.2.h:A 0.3:P</ta>
            <ta e="T99" id="Seg_2674" s="T98">np:G</ta>
            <ta e="T100" id="Seg_2675" s="T99">pro.h:A</ta>
            <ta e="T102" id="Seg_2676" s="T101">np.h:A 0.1.h:Poss</ta>
            <ta e="T103" id="Seg_2677" s="T102">np:P</ta>
            <ta e="T105" id="Seg_2678" s="T104">np.h:A 0.1.h:Poss</ta>
            <ta e="T107" id="Seg_2679" s="T106">np:G</ta>
            <ta e="T108" id="Seg_2680" s="T107">pro.h:Poss</ta>
            <ta e="T109" id="Seg_2681" s="T108">np:Com</ta>
            <ta e="T110" id="Seg_2682" s="T109">pro.h:R</ta>
            <ta e="T112" id="Seg_2683" s="T111">np.h:A 0.1.h:Poss</ta>
            <ta e="T114" id="Seg_2684" s="T113">0.3.h:A</ta>
            <ta e="T115" id="Seg_2685" s="T114">np.h:A 0.1.h:Poss</ta>
            <ta e="T117" id="Seg_2686" s="T116">np:G</ta>
            <ta e="T120" id="Seg_2687" s="T119">pro.h:A</ta>
            <ta e="T122" id="Seg_2688" s="T121">pro.h:R</ta>
            <ta e="T123" id="Seg_2689" s="T122">np.h:R</ta>
            <ta e="T125" id="Seg_2690" s="T124">0.2.h:A</ta>
            <ta e="T126" id="Seg_2691" s="T125">np:Th</ta>
            <ta e="T127" id="Seg_2692" s="T126">adv:Time</ta>
            <ta e="T129" id="Seg_2693" s="T128">pro.h:Th</ta>
            <ta e="T132" id="Seg_2694" s="T131">0.1.h:A</ta>
            <ta e="T133" id="Seg_2695" s="T132">0.1.h:A</ta>
            <ta e="T134" id="Seg_2696" s="T133">np:Th</ta>
            <ta e="T135" id="Seg_2697" s="T134">0.1.h:A</ta>
            <ta e="T136" id="Seg_2698" s="T135">0.1.h:A</ta>
            <ta e="T137" id="Seg_2699" s="T136">np:L</ta>
            <ta e="T138" id="Seg_2700" s="T137">np:P</ta>
            <ta e="T139" id="Seg_2701" s="T138">0.1.h:A</ta>
            <ta e="T140" id="Seg_2702" s="T139">np:G</ta>
            <ta e="T141" id="Seg_2703" s="T140">0.1.h:A</ta>
            <ta e="T142" id="Seg_2704" s="T141">adv:Time</ta>
            <ta e="T143" id="Seg_2705" s="T142">0.1.h:A</ta>
            <ta e="T144" id="Seg_2706" s="T143">np:Th</ta>
            <ta e="T145" id="Seg_2707" s="T144">0.1.h:A</ta>
            <ta e="T146" id="Seg_2708" s="T145">np:G</ta>
            <ta e="T148" id="Seg_2709" s="T147">np:Th</ta>
            <ta e="T149" id="Seg_2710" s="T148">0.1.h:A</ta>
            <ta e="T150" id="Seg_2711" s="T149">np:So</ta>
            <ta e="T152" id="Seg_2712" s="T151">adv:Time</ta>
            <ta e="T153" id="Seg_2713" s="T152">0.1.h:A</ta>
            <ta e="T154" id="Seg_2714" s="T153">np:G</ta>
            <ta e="T158" id="Seg_2715" s="T157">np:Com</ta>
            <ta e="T159" id="Seg_2716" s="T158">np:Th</ta>
            <ta e="T160" id="Seg_2717" s="T159">pro.h:Poss</ta>
            <ta e="T163" id="Seg_2718" s="T162">0.1.h:A</ta>
            <ta e="T164" id="Seg_2719" s="T163">0.1.h:A</ta>
            <ta e="T168" id="Seg_2720" s="T167">np:Th</ta>
            <ta e="T169" id="Seg_2721" s="T168">adv:Time</ta>
            <ta e="T171" id="Seg_2722" s="T170">np:Time</ta>
            <ta e="T172" id="Seg_2723" s="T171">0.1.h:A</ta>
            <ta e="T178" id="Seg_2724" s="T177">np:Time</ta>
            <ta e="T182" id="Seg_2725" s="T181">np.h:Th</ta>
            <ta e="T186" id="Seg_2726" s="T185">pro.h:Th</ta>
            <ta e="T190" id="Seg_2727" s="T189">0.3.h:A</ta>
            <ta e="T191" id="Seg_2728" s="T190">0.3.h:A</ta>
            <ta e="T192" id="Seg_2729" s="T191">0.3.h:A</ta>
            <ta e="T193" id="Seg_2730" s="T192">0.3.h:A</ta>
            <ta e="T194" id="Seg_2731" s="T193">pro.h:B</ta>
            <ta e="T195" id="Seg_2732" s="T194">adv:L</ta>
            <ta e="T197" id="Seg_2733" s="T196">0.3:Th</ta>
            <ta e="T198" id="Seg_2734" s="T197">np:A</ta>
            <ta e="T200" id="Seg_2735" s="T199">np:A</ta>
            <ta e="T201" id="Seg_2736" s="T200">np:A</ta>
            <ta e="T204" id="Seg_2737" s="T203">v:Th</ta>
            <ta e="T205" id="Seg_2738" s="T204">v:Th</ta>
            <ta e="T206" id="Seg_2739" s="T205">v:Th</ta>
            <ta e="T208" id="Seg_2740" s="T207">v:Th</ta>
            <ta e="T209" id="Seg_2741" s="T208">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_2742" s="T1">s:temp</ta>
            <ta e="T3" id="Seg_2743" s="T2">s:temp</ta>
            <ta e="T4" id="Seg_2744" s="T3">0.1.h:S v:pred</ta>
            <ta e="T5" id="Seg_2745" s="T4">0.1.h:S v:pred</ta>
            <ta e="T8" id="Seg_2746" s="T7">v:pred</ta>
            <ta e="T9" id="Seg_2747" s="T8">np:S</ta>
            <ta e="T10" id="Seg_2748" s="T9">0.1.h:S v:pred</ta>
            <ta e="T12" id="Seg_2749" s="T11">0.1.h:S v:pred</ta>
            <ta e="T15" id="Seg_2750" s="T14">v:pred</ta>
            <ta e="T16" id="Seg_2751" s="T15">np:S</ta>
            <ta e="T19" id="Seg_2752" s="T16">s:adv</ta>
            <ta e="T20" id="Seg_2753" s="T19">0.1.h:S v:pred</ta>
            <ta e="T22" id="Seg_2754" s="T21">np:S</ta>
            <ta e="T23" id="Seg_2755" s="T22">v:pred</ta>
            <ta e="T26" id="Seg_2756" s="T25">np:S</ta>
            <ta e="T28" id="Seg_2757" s="T27">n:pred</ta>
            <ta e="T29" id="Seg_2758" s="T28">0.1.h:S v:pred</ta>
            <ta e="T30" id="Seg_2759" s="T29">pro.h:S</ta>
            <ta e="T31" id="Seg_2760" s="T30">v:pred</ta>
            <ta e="T32" id="Seg_2761" s="T31">np:O</ta>
            <ta e="T33" id="Seg_2762" s="T32">pro.h:S</ta>
            <ta e="T35" id="Seg_2763" s="T34">v:pred</ta>
            <ta e="T37" id="Seg_2764" s="T36">pro.h:S</ta>
            <ta e="T38" id="Seg_2765" s="T37">v:pred 0.3:O</ta>
            <ta e="T39" id="Seg_2766" s="T38">pro.h:S</ta>
            <ta e="T40" id="Seg_2767" s="T39">v:pred</ta>
            <ta e="T45" id="Seg_2768" s="T40">s:temp</ta>
            <ta e="T46" id="Seg_2769" s="T45">pro.h:S</ta>
            <ta e="T47" id="Seg_2770" s="T46">v:pred 0.3:O</ta>
            <ta e="T49" id="Seg_2771" s="T48">0.1.h:S v:pred</ta>
            <ta e="T50" id="Seg_2772" s="T49">pro.h:S</ta>
            <ta e="T51" id="Seg_2773" s="T50">v:pred 0.3:O</ta>
            <ta e="T52" id="Seg_2774" s="T51">pro.h:S</ta>
            <ta e="T54" id="Seg_2775" s="T53">v:pred</ta>
            <ta e="T55" id="Seg_2776" s="T54">pro.h:S</ta>
            <ta e="T58" id="Seg_2777" s="T57">v:pred 0.3:O</ta>
            <ta e="T61" id="Seg_2778" s="T60">0.1.h:S v:pred</ta>
            <ta e="T65" id="Seg_2779" s="T64">pro.h:S</ta>
            <ta e="T67" id="Seg_2780" s="T66">v:pred</ta>
            <ta e="T69" id="Seg_2781" s="T68">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T71" id="Seg_2782" s="T70">0.1.h:S v:pred</ta>
            <ta e="T72" id="Seg_2783" s="T71">0.2.h:S v:pred 0.3:O</ta>
            <ta e="T73" id="Seg_2784" s="T72">pro.h:S</ta>
            <ta e="T74" id="Seg_2785" s="T73">v:pred 0.3:O</ta>
            <ta e="T75" id="Seg_2786" s="T74">pro.h:S</ta>
            <ta e="T77" id="Seg_2787" s="T75">s:temp</ta>
            <ta e="T78" id="Seg_2788" s="T77">v:pred</ta>
            <ta e="T82" id="Seg_2789" s="T81">pro.h:S</ta>
            <ta e="T83" id="Seg_2790" s="T82">np:O</ta>
            <ta e="T84" id="Seg_2791" s="T83">v:pred</ta>
            <ta e="T85" id="Seg_2792" s="T84">np.h:S</ta>
            <ta e="T86" id="Seg_2793" s="T85">v:pred</ta>
            <ta e="T87" id="Seg_2794" s="T86">0.3.h:S v:pred</ta>
            <ta e="T88" id="Seg_2795" s="T87">pro.h:S</ta>
            <ta e="T90" id="Seg_2796" s="T89">np.h:S</ta>
            <ta e="T91" id="Seg_2797" s="T90">np:S</ta>
            <ta e="T92" id="Seg_2798" s="T91">n:pred</ta>
            <ta e="T93" id="Seg_2799" s="T92">v:pred</ta>
            <ta e="T98" id="Seg_2800" s="T97">0.2.h:S v:pred 0.3:O</ta>
            <ta e="T100" id="Seg_2801" s="T99">pro.h:S</ta>
            <ta e="T101" id="Seg_2802" s="T100">v:pred</ta>
            <ta e="T102" id="Seg_2803" s="T101">np.h:S</ta>
            <ta e="T103" id="Seg_2804" s="T102">np:O</ta>
            <ta e="T104" id="Seg_2805" s="T103">v:pred</ta>
            <ta e="T105" id="Seg_2806" s="T104">np.h:S</ta>
            <ta e="T106" id="Seg_2807" s="T105">v:pred</ta>
            <ta e="T111" id="Seg_2808" s="T110">v:pred</ta>
            <ta e="T112" id="Seg_2809" s="T111">np.h:S</ta>
            <ta e="T114" id="Seg_2810" s="T113">0.3.h:S v:pred</ta>
            <ta e="T115" id="Seg_2811" s="T114">np.h:S</ta>
            <ta e="T120" id="Seg_2812" s="T119">pro.h:S</ta>
            <ta e="T121" id="Seg_2813" s="T120">v:pred</ta>
            <ta e="T125" id="Seg_2814" s="T124">0.2.h:S v:pred</ta>
            <ta e="T126" id="Seg_2815" s="T125">np:O</ta>
            <ta e="T129" id="Seg_2816" s="T128">pro.h:S</ta>
            <ta e="T131" id="Seg_2817" s="T130">v:pred</ta>
            <ta e="T132" id="Seg_2818" s="T131">0.1.h:S v:pred</ta>
            <ta e="T134" id="Seg_2819" s="T132">s:purp</ta>
            <ta e="T135" id="Seg_2820" s="T134">s:purp</ta>
            <ta e="T136" id="Seg_2821" s="T135">0.1.h:S v:pred</ta>
            <ta e="T138" id="Seg_2822" s="T137">np:O</ta>
            <ta e="T139" id="Seg_2823" s="T138">0.1.h:S v:pred</ta>
            <ta e="T141" id="Seg_2824" s="T140">0.1.h:S v:pred</ta>
            <ta e="T143" id="Seg_2825" s="T142">0.1.h:S v:pred</ta>
            <ta e="T144" id="Seg_2826" s="T143">np:O</ta>
            <ta e="T145" id="Seg_2827" s="T144">0.1.h:S v:pred</ta>
            <ta e="T148" id="Seg_2828" s="T147">np:O</ta>
            <ta e="T149" id="Seg_2829" s="T148">0.1.h:S v:pred</ta>
            <ta e="T151" id="Seg_2830" s="T149">s:rel</ta>
            <ta e="T153" id="Seg_2831" s="T152">0.1.h:S v:pred</ta>
            <ta e="T157" id="Seg_2832" s="T155">s:rel</ta>
            <ta e="T159" id="Seg_2833" s="T158">np:S</ta>
            <ta e="T162" id="Seg_2834" s="T161">n:pred</ta>
            <ta e="T163" id="Seg_2835" s="T162">0.1.h:S v:pred</ta>
            <ta e="T164" id="Seg_2836" s="T163">0.1.h:S v:pred</ta>
            <ta e="T167" id="Seg_2837" s="T166">v:pred</ta>
            <ta e="T168" id="Seg_2838" s="T167">np:S</ta>
            <ta e="T172" id="Seg_2839" s="T171">0.1.h:S v:pred</ta>
            <ta e="T182" id="Seg_2840" s="T181">np.h:S</ta>
            <ta e="T184" id="Seg_2841" s="T183">v:pred</ta>
            <ta e="T186" id="Seg_2842" s="T185">pro.h:S</ta>
            <ta e="T187" id="Seg_2843" s="T186">v:pred</ta>
            <ta e="T190" id="Seg_2844" s="T189">0.3.h:S v:pred</ta>
            <ta e="T191" id="Seg_2845" s="T190">s:purp</ta>
            <ta e="T192" id="Seg_2846" s="T191">s:purp</ta>
            <ta e="T193" id="Seg_2847" s="T192">s:purp</ta>
            <ta e="T196" id="Seg_2848" s="T195">adj:pred</ta>
            <ta e="T197" id="Seg_2849" s="T196">0.3:S cop</ta>
            <ta e="T198" id="Seg_2850" s="T197">np:S</ta>
            <ta e="T199" id="Seg_2851" s="T198">v:pred</ta>
            <ta e="T200" id="Seg_2852" s="T199">np:S</ta>
            <ta e="T201" id="Seg_2853" s="T200">np:S</ta>
            <ta e="T202" id="Seg_2854" s="T201">v:pred</ta>
            <ta e="T204" id="Seg_2855" s="T203">v:S</ta>
            <ta e="T205" id="Seg_2856" s="T204">v:S</ta>
            <ta e="T206" id="Seg_2857" s="T205">v:S</ta>
            <ta e="T208" id="Seg_2858" s="T207">v:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T28" id="Seg_2859" s="T27">RUS:cult</ta>
            <ta e="T42" id="Seg_2860" s="T41">RUS:cult</ta>
            <ta e="T59" id="Seg_2861" s="T58">RUS:gram</ta>
            <ta e="T66" id="Seg_2862" s="T65">RUS:gram</ta>
            <ta e="T146" id="Seg_2863" s="T145">RUS:cult</ta>
            <ta e="T150" id="Seg_2864" s="T149">RUS:cult</ta>
            <ta e="T154" id="Seg_2865" s="T153">RUS:cult</ta>
            <ta e="T161" id="Seg_2866" s="T160">RUS:cult</ta>
            <ta e="T162" id="Seg_2867" s="T161">RUS:cult</ta>
            <ta e="T207" id="Seg_2868" s="T206">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_2869" s="T0">Вечером покушав, лег, уснул.</ta>
            <ta e="T9" id="Seg_2870" s="T4">Проснулся, в пологе много комаров собралось.</ta>
            <ta e="T16" id="Seg_2871" s="T9">Лежу, сам думаю: "Как в мой полог забрались комары?"</ta>
            <ta e="T23" id="Seg_2872" s="T16">Лежа на спине (/лицом к пологу), взглянул: кромка полога распоролась.</ta>
            <ta e="T32" id="Seg_2873" s="T23">Младшего своего сынка по имени Вова я спросил: "Ты порвал полог?"</ta>
            <ta e="T38" id="Seg_2874" s="T32">Он мне сказал: "Не я разорвал".</ta>
            <ta e="T47" id="Seg_2875" s="T38">Он сказал: "Когда моя сестра Катя села на койку, она разорвала".</ta>
            <ta e="T51" id="Seg_2876" s="T47">Дочку спрашиваю: "Ты разорвала?"</ta>
            <ta e="T63" id="Seg_2877" s="T51">Она мне сказала: "Я тоже не рвала и не подходила к пологу".</ta>
            <ta e="T67" id="Seg_2878" s="T63">Так я и остался.</ta>
            <ta e="T69" id="Seg_2879" s="T67">Не узнал.</ta>
            <ta e="T72" id="Seg_2880" s="T69">Жене сказал: "Сшей!"</ta>
            <ta e="T74" id="Seg_2881" s="T72">Она сшила.</ta>
            <ta e="T78" id="Seg_2882" s="T74">Я опять лег, заснул.</ta>
            <ta e="T84" id="Seg_2883" s="T78">Утром, в четыре часа, кто-то в дверь стучит.</ta>
            <ta e="T89" id="Seg_2884" s="T84">Моя жена встала и спрашивает: "Кто там?"</ta>
            <ta e="T99" id="Seg_2885" s="T89">Мой сын, которого зовут Михаил, говорит: "Это я, мать, открой дверь".</ta>
            <ta e="T101" id="Seg_2886" s="T99">"Я иду".</ta>
            <ta e="T104" id="Seg_2887" s="T101">Моя жена дверь открыла.</ta>
            <ta e="T109" id="Seg_2888" s="T104">Мой сын вошел в дом со своим товарищем.</ta>
            <ta e="T119" id="Seg_2889" s="T109">Меня спрашивает, мой сын лодку просит: "Мой товарищ [хочет] в Пирино ехать".</ta>
            <ta e="T126" id="Seg_2890" s="T119">Я сказал ему, сыну: "Сын, дай [ему] ветку".</ta>
            <ta e="T131" id="Seg_2891" s="T126">Потом я не спал.</ta>
            <ta e="T134" id="Seg_2892" s="T131">Поехал посмотреть сеть.</ta>
            <ta e="T138" id="Seg_2893" s="T134">Я поймал в сетке рыбу для еды.</ta>
            <ta e="T144" id="Seg_2894" s="T138">Пришел домой, поел, потом насаживал сеть.</ta>
            <ta e="T151" id="Seg_2895" s="T144">Ходил на почту, одно письмо взял, из исполкома пришедшее.</ta>
            <ta e="T154" id="Seg_2896" s="T151">Потом я зашел в совет.</ta>
            <ta e="T158" id="Seg_2897" s="T154">Мы начали работать с приезжей женщиной.</ta>
            <ta e="T162" id="Seg_2898" s="T158">Ее зовут Ангелина Ивановна.</ta>
            <ta e="T165" id="Seg_2899" s="T162">Мы говорили и писали по-селькупски.</ta>
            <ta e="T171" id="Seg_2900" s="T165">И так проходили дни с утра до вечера.</ta>
            <ta e="T178" id="Seg_2901" s="T171">Так работали мы по десять, одиннадцать часов в день.</ta>
            <ta e="T185" id="Seg_2902" s="T178">Селькупские старики, некоторые русские не усидят так долго.</ta>
            <ta e="T193" id="Seg_2903" s="T185">Они посидят немного, потом уходят удить рыбу и собирать бруснику.</ta>
            <ta e="T197" id="Seg_2904" s="T193">Им там хорошо.</ta>
            <ta e="T209" id="Seg_2905" s="T197">Комары кусают, мошка и оводы кусают, невозможно сидеть и писать, читать и селькупские слова говорить.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_2906" s="T0">Having eaten in the evening, I lied down and fell asleep.</ta>
            <ta e="T9" id="Seg_2907" s="T4">I woke up, many mosquitoes had gathered in the bed curtains.</ta>
            <ta e="T16" id="Seg_2908" s="T9">I lie and I think: "How did mosquitoes manage to get into my bed curtains?"</ta>
            <ta e="T23" id="Seg_2909" s="T16">As I was lying on my back (/my face to the bed curtains), I looked up: the edge of the curtains got ripped up.</ta>
            <ta e="T32" id="Seg_2910" s="T23">I asked my youngest son named Vova: "Did you tear the curtains?"</ta>
            <ta e="T38" id="Seg_2911" s="T32">He told me: "It wasn't me who tore it."</ta>
            <ta e="T47" id="Seg_2912" s="T38">He said: "When my sister Katya sat down to the bed, she tore it."</ta>
            <ta e="T51" id="Seg_2913" s="T47">I ask my daughter: "Did you tear it?"</ta>
            <ta e="T63" id="Seg_2914" s="T51">She told me: "I didn't tear it either and I didn't approach the bed."</ta>
            <ta e="T67" id="Seg_2915" s="T63">So I stayed.</ta>
            <ta e="T69" id="Seg_2916" s="T67">I didn't get to know it.</ta>
            <ta e="T72" id="Seg_2917" s="T69">I told to my wife: "Sew it!"</ta>
            <ta e="T74" id="Seg_2918" s="T72">She sewed it.</ta>
            <ta e="T78" id="Seg_2919" s="T74">I lied down again and fell asleep.</ta>
            <ta e="T84" id="Seg_2920" s="T78">At four in the morning someone is knocking at the door.</ta>
            <ta e="T89" id="Seg_2921" s="T84">My wife got up and asked: "Who is there?"</ta>
            <ta e="T99" id="Seg_2922" s="T89">My son named Mikhail, said: "Mother, that's me, open the door."</ta>
            <ta e="T101" id="Seg_2923" s="T99">"I am coming."</ta>
            <ta e="T104" id="Seg_2924" s="T101">My wife opened the door.</ta>
            <ta e="T109" id="Seg_2925" s="T104">My son entered the house with his friend.</ta>
            <ta e="T119" id="Seg_2926" s="T109">He asked me, asking for a boat: "My friend [wants] to go to Pirino (/the Pirino village)."</ta>
            <ta e="T126" id="Seg_2927" s="T119">I told him, to my son: "Son, give [him] the boat."</ta>
            <ta e="T131" id="Seg_2928" s="T126">Then (/after that) I didn't sleep [any more].</ta>
            <ta e="T134" id="Seg_2929" s="T131">I went to check the fishnet.</ta>
            <ta e="T138" id="Seg_2930" s="T134">I caught a fish into the fishnet to eat.</ta>
            <ta e="T144" id="Seg_2931" s="T138">I came back home, I ate, then I set the fishnet.</ta>
            <ta e="T151" id="Seg_2932" s="T144">I went to the post, took one letter, which came from the executive committee.</ta>
            <ta e="T154" id="Seg_2933" s="T151">Then I went to the village council.</ta>
            <ta e="T158" id="Seg_2934" s="T154">We started to work with a woman, who had come [here].</ta>
            <ta e="T162" id="Seg_2935" s="T158">Her name was Angelina Ivanovna.</ta>
            <ta e="T165" id="Seg_2936" s="T162">We spoke and we wrote in Selkup.</ta>
            <ta e="T171" id="Seg_2937" s="T165">And so the days passed from morning till evening.</ta>
            <ta e="T178" id="Seg_2938" s="T171">We worked ten, eleven hours a day.</ta>
            <ta e="T185" id="Seg_2939" s="T178">Selkup old men, some Russians are not able to sit so long.</ta>
            <ta e="T193" id="Seg_2940" s="T185">They sit a little, then they go fishing or collecting cowberry.</ta>
            <ta e="T197" id="Seg_2941" s="T193">They are lucky there. </ta>
            <ta e="T209" id="Seg_2942" s="T197">Mosquitoes are biting, blackflies and botflies are biting, it's impossible to sit, to write, to read and to tell Selkup words.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_2943" s="T0">Am Abend nach dem Essen legte ich mich hin und schlief ein.</ta>
            <ta e="T9" id="Seg_2944" s="T4">Ich wachte auf, im Bettvorhang hatten sich Mücken versammelt.</ta>
            <ta e="T16" id="Seg_2945" s="T9">Ich liege da und denke: "Wie konnten die Mücken hereinkommen?"</ta>
            <ta e="T23" id="Seg_2946" s="T16">Auf dem Rücken liegend (/mein Gesicht zum Bettvorhang) schaute ich: Die Ecke des Baldachins ist eingerissen.</ta>
            <ta e="T32" id="Seg_2947" s="T23">Ich fragte meinen jüngsten Sohn namens Vova: "Hast du den Bettvorhang zerrissen?"</ta>
            <ta e="T38" id="Seg_2948" s="T32">Er sagte mir: "Ich habe ihn nicht zerrissen."</ta>
            <ta e="T47" id="Seg_2949" s="T38">Er sagte: "Als meine Schwester, Katja, sich auf das Bett gesetzt hat, hat sie ihn zerrissen."</ta>
            <ta e="T51" id="Seg_2950" s="T47">Ich fragte meine Tochter: "Hast du ihn zerrissen?"</ta>
            <ta e="T63" id="Seg_2951" s="T51">Sie sagte mir: "Ich habe ihn auch nicht zerrissen, ich bin nicht zum Bettvorhang hingegangen."</ta>
            <ta e="T67" id="Seg_2952" s="T63">Und so verblieb ich.</ta>
            <ta e="T69" id="Seg_2953" s="T67">Ich fand es nicht heraus.</ta>
            <ta e="T72" id="Seg_2954" s="T69">Ich sagte meiner Frau: "Nähe ihn!"</ta>
            <ta e="T74" id="Seg_2955" s="T72">Sie hat ihn genäht.</ta>
            <ta e="T78" id="Seg_2956" s="T74">Ich legte mich wieder hin, schlief ein.</ta>
            <ta e="T84" id="Seg_2957" s="T78">Am Morgen um vier Uhr klopft jemand an der Tür.</ta>
            <ta e="T89" id="Seg_2958" s="T84">Meine Frau stand auf und fragte: "Wer ist da?"</ta>
            <ta e="T99" id="Seg_2959" s="T89">Mein Sohn, sein Name ist Michail, sagte: "Ich bin es, Mutter, öffne die Tür."</ta>
            <ta e="T101" id="Seg_2960" s="T99">"Ich komme."</ta>
            <ta e="T104" id="Seg_2961" s="T101">Meine Frau öffnete die Tür.</ta>
            <ta e="T109" id="Seg_2962" s="T104">Mein Sohn kam mit seinen Freunden herein.</ta>
            <ta e="T119" id="Seg_2963" s="T109">Mein Sohn fragte mich um ein Boot, er bat: "Mein Freund [möchte] nach Pirino (/ins Dorf Pirino) fahren."</ta>
            <ta e="T126" id="Seg_2964" s="T119">Ich sagte ihm, dem Sohn: "Sohn, gib [ihm] das Boot."</ta>
            <ta e="T131" id="Seg_2965" s="T126">Dann (/danach) schlief ich nicht [mehr].</ta>
            <ta e="T134" id="Seg_2966" s="T131">Ich fuhr die Netze ansehen.</ta>
            <ta e="T138" id="Seg_2967" s="T134">Ich fing im Netz Fische zum Essen.</ta>
            <ta e="T144" id="Seg_2968" s="T138">Ich kam nach Hause, habe gegessen und stellte(?) das Netz.</ta>
            <ta e="T151" id="Seg_2969" s="T144">Ich ging zur Post, nahm einen Brief mit, der vom Exekutivkomitee gekommen war.</ta>
            <ta e="T154" id="Seg_2970" s="T151">Dann ging ich ins zum Dorfrat.</ta>
            <ta e="T158" id="Seg_2971" s="T154">Wir fingen an mit der Frau zu arbeiten, die gekommen war.</ta>
            <ta e="T162" id="Seg_2972" s="T158">Ihr Name ist Angelina Ivanovna.</ta>
            <ta e="T165" id="Seg_2973" s="T162">Wir sprachen und schrieben auf Selkupisch.</ta>
            <ta e="T171" id="Seg_2974" s="T165">Und so verliefen die Tage von morgens bis abends.</ta>
            <ta e="T178" id="Seg_2975" s="T171">So arbeiteten wir zehn, elf Stunden täglich. </ta>
            <ta e="T185" id="Seg_2976" s="T178">Die selkupischen Alten, die Russen sitzen nicht so lange.</ta>
            <ta e="T193" id="Seg_2977" s="T185">Sie sitzen ein wenig, dann gehen sie fischen und Beeren sammeln.</ta>
            <ta e="T197" id="Seg_2978" s="T193">Es geht ihnen dort gut.</ta>
            <ta e="T209" id="Seg_2979" s="T197">Die Mücken stechen, die Schwarzfliegen und die Bremsen beißen, es ist unmöglich zu sitzen und zu lesen und selkupische Wörter zu sagen.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_2980" s="T0">вечером покушав лег уснул</ta>
            <ta e="T9" id="Seg_2981" s="T4">проснулся (пробудился, разбудился) в пологу много собралось комаров</ta>
            <ta e="T16" id="Seg_2982" s="T9">лежу сам думаю как в мой полог забрались (взошли) комары</ta>
            <ta e="T23" id="Seg_2983" s="T16">на верх полога вверх лицом (на спине) лежа взглянул кромка полога (конец) распоролась</ta>
            <ta e="T32" id="Seg_2984" s="T23">младшего сынка по имени (звать его) Вова спросил ты изорвал полог</ta>
            <ta e="T38" id="Seg_2985" s="T32">Он мне сказал: не я разорвал.</ta>
            <ta e="T47" id="Seg_2986" s="T38">он сказал сестра (старшая) Катя села на койку она разорвала</ta>
            <ta e="T51" id="Seg_2987" s="T47">дочку спрашиваю ты разорвала</ta>
            <ta e="T63" id="Seg_2988" s="T51">она мне сказала я тоже не рвала и не приходила к пологу (около полога)</ta>
            <ta e="T67" id="Seg_2989" s="T63">так я и остался</ta>
            <ta e="T69" id="Seg_2990" s="T67">не узнал</ta>
            <ta e="T72" id="Seg_2991" s="T69">жене сказал зашей (сшей)</ta>
            <ta e="T74" id="Seg_2992" s="T72">она зашила (сшила)</ta>
            <ta e="T78" id="Seg_2993" s="T74">я опять лег заснул</ta>
            <ta e="T84" id="Seg_2994" s="T78">утром в четыре часа кто-то в дверь стучит</ta>
            <ta e="T89" id="Seg_2995" s="T84">жена встала спрашивает кто там</ta>
            <ta e="T99" id="Seg_2996" s="T89">сын звать Михаил говорит это я мать открой дверь</ta>
            <ta e="T101" id="Seg_2997" s="T99">я иду</ta>
            <ta e="T104" id="Seg_2998" s="T101">жена дверь открыла</ta>
            <ta e="T109" id="Seg_2999" s="T104">сын вошел в дом со своим товарищем</ta>
            <ta e="T119" id="Seg_3000" s="T109">у меня спрашивает сын лодку просит товарищ ехать (идти) в Пирино (в деревню Пирино)</ta>
            <ta e="T126" id="Seg_3001" s="T119">я сказал ему сыну сын дай обласок</ta>
            <ta e="T131" id="Seg_3002" s="T126">потом (после этого) я не спал</ta>
            <ta e="T134" id="Seg_3003" s="T131">поехал посмотреть сеть</ta>
            <ta e="T138" id="Seg_3004" s="T134">поесть поймал в сетке рыбу</ta>
            <ta e="T144" id="Seg_3005" s="T138">пришел домой поел потом насаживал сеть</ta>
            <ta e="T151" id="Seg_3006" s="T144">ходил на почту одно письмо взял (получил) из исполкома пришедшее</ta>
            <ta e="T154" id="Seg_3007" s="T151">потом зашел в с/совет</ta>
            <ta e="T158" id="Seg_3008" s="T154">начали работать с приезжей женщиной</ta>
            <ta e="T162" id="Seg_3009" s="T158">ее звать</ta>
            <ta e="T165" id="Seg_3010" s="T162">говорили писали по-остяцки</ta>
            <ta e="T171" id="Seg_3011" s="T165">и так проходили дни с утра до вечера</ta>
            <ta e="T178" id="Seg_3012" s="T171">работали по десять одиннадцать часов в день</ta>
            <ta e="T185" id="Seg_3013" s="T178">остяки-старики некоторые русские не усидят столько</ta>
            <ta e="T193" id="Seg_3014" s="T185">они (по)сидят немного уходят удить сетничать по ягоды (за брусникой)</ta>
            <ta e="T197" id="Seg_3015" s="T193">им там хорошо</ta>
            <ta e="T209" id="Seg_3016" s="T197">комар укусит мошка паут едят не сидеть писать читать и говорить остяцкий разговор (слова)</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T162" id="Seg_3017" s="T158">WNB: Meant is Kuzmina.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T210" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
