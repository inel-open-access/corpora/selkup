<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KMS_1963_NutHunting_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KMS_1963_NutHunting_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">131</ud-information>
            <ud-information attribute-name="# HIAT:w">101</ud-information>
            <ud-information attribute-name="# e">101</ud-information>
            <ud-information attribute-name="# HIAT:u">20</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KMS">
            <abbreviation>KMS</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KMS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T102" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">kundar</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">me</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">swäŋɨjgot</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">kundar</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">taqkɨlgwot</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">suːrulʼe</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">nandar</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">me</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">taqkɨlgwot</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">i</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_37" n="HIAT:w" s="T11">swäŋɨjgu</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_41" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">čekɨrkwot</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">nʼäjim</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_49" n="HIAT:w" s="T14">suharɨŋlʼe</ts>
                  <nts id="Seg_50" n="HIAT:ip">,</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">iːkwot</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_56" n="HIAT:w" s="T16">tüːləsäm</ts>
                  <nts id="Seg_57" n="HIAT:ip">,</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_60" n="HIAT:w" s="T17">kanam</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_64" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_66" n="HIAT:w" s="T18">tüːkwot</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_69" n="HIAT:w" s="T19">swäŋɨjej</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_72" n="HIAT:w" s="T20">čwäčondə</ts>
                  <nts id="Seg_73" n="HIAT:ip">,</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">patʼölgwot</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">lʼeːwɨm</ts>
                  <nts id="Seg_80" n="HIAT:ip">,</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_83" n="HIAT:w" s="T23">meːkwot</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_86" n="HIAT:w" s="T24">lʼewi</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_89" n="HIAT:w" s="T25">maːdəm</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_93" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_95" n="HIAT:w" s="T26">nännä</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_98" n="HIAT:w" s="T27">meːkwot</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_101" n="HIAT:w" s="T28">poj</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_104" n="HIAT:w" s="T29">tʼäǯim</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_108" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">poj</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">tʼäǯim</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_116" n="HIAT:w" s="T32">meːkwot</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_119" n="HIAT:w" s="T33">qweːɣännä</ts>
                  <nts id="Seg_120" n="HIAT:ip">.</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_123" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">taqkɨlgwot</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">mergɨn</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_131" n="HIAT:w" s="T36">ästɨmbɨdi</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_134" n="HIAT:w" s="T37">swäŋɨm</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_137" n="HIAT:w" s="T38">tʼäǯičikwot</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_141" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_143" n="HIAT:w" s="T39">okkɨr</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_146" n="HIAT:w" s="T40">taːlǯikut</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_149" n="HIAT:w" s="T41">tʼätǯim</ts>
                  <nts id="Seg_150" n="HIAT:ip">,</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_153" n="HIAT:w" s="T42">šɨttə</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_156" n="HIAT:w" s="T43">qaj</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_159" n="HIAT:w" s="T44">tättə</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_162" n="HIAT:w" s="T45">qum</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_165" n="HIAT:w" s="T46">na</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_168" n="HIAT:w" s="T47">tʼätǯim</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_171" n="HIAT:w" s="T48">mišalgut</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_174" n="HIAT:w" s="T49">kuːdgon</ts>
                  <nts id="Seg_175" n="HIAT:ip">.</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_178" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_180" n="HIAT:w" s="T50">tʼätčizä</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_183" n="HIAT:w" s="T51">qättɨlʼe</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_186" n="HIAT:w" s="T52">tɨːdɨm</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_189" n="HIAT:w" s="T53">swäŋɣə</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_192" n="HIAT:w" s="T54">innännɨ</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_195" n="HIAT:w" s="T55">čečolʼdʼikuŋ</ts>
                  <nts id="Seg_196" n="HIAT:ip">.</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_199" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_201" n="HIAT:w" s="T56">čečolʼdimbədi</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_204" n="HIAT:w" s="T57">swäŋɨm</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_207" n="HIAT:w" s="T58">ilʼlʼe</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_210" n="HIAT:w" s="T59">me</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_213" n="HIAT:w" s="T60">taqkɨlgwot</ts>
                  <nts id="Seg_214" n="HIAT:ip">.</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_217" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_219" n="HIAT:w" s="T61">okkɨrɨŋ</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_222" n="HIAT:w" s="T62">tʼätǯičitäːɣɨn</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_225" n="HIAT:w" s="T63">man</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_228" n="HIAT:w" s="T64">innä</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_231" n="HIAT:w" s="T65">manǯiäŋ</ts>
                  <nts id="Seg_232" n="HIAT:ip">.</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_235" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_237" n="HIAT:w" s="T66">swäŋgi</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_240" n="HIAT:w" s="T67">oːllə</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_243" n="HIAT:w" s="T68">qäːttɨt</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_246" n="HIAT:w" s="T69">man</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_249" n="HIAT:w" s="T70">saːjow</ts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_253" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_255" n="HIAT:w" s="T71">man</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_258" n="HIAT:w" s="T72">kum</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_261" n="HIAT:w" s="T73">üːttaw</ts>
                  <nts id="Seg_262" n="HIAT:ip">.</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_265" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_267" n="HIAT:w" s="T74">orannaw</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_270" n="HIAT:w" s="T75">piːŋganɨzä</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_273" n="HIAT:w" s="T76">taːdɨbɨlʼe</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_276" n="HIAT:w" s="T77">saːjow</ts>
                  <nts id="Seg_277" n="HIAT:ip">.</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_280" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_282" n="HIAT:w" s="T78">niːdʼadäk</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_285" n="HIAT:w" s="T79">qaːrɨnʼnʼaŋ</ts>
                  <nts id="Seg_286" n="HIAT:ip">:</nts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_289" n="HIAT:w" s="T80">paːj</ts>
                  <nts id="Seg_290" n="HIAT:ip">,</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_293" n="HIAT:w" s="T81">paːj</ts>
                  <nts id="Seg_294" n="HIAT:ip">,</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_297" n="HIAT:w" s="T82">paːj</ts>
                  <nts id="Seg_298" n="HIAT:ip">!</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_301" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_303" n="HIAT:w" s="T83">i</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_306" n="HIAT:w" s="T84">čečolʼdʼäŋ</ts>
                  <nts id="Seg_307" n="HIAT:ip">.</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_310" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_312" n="HIAT:w" s="T85">tʼeːlɨmdə</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_315" n="HIAT:w" s="T86">korʼčaw</ts>
                  <nts id="Seg_316" n="HIAT:ip">.</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_319" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_321" n="HIAT:w" s="T87">kundə</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_324" n="HIAT:w" s="T88">ippɨzaŋ</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_327" n="HIAT:w" s="T89">tʼümbɨn</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_330" n="HIAT:w" s="T90">barɣɨn</ts>
                  <nts id="Seg_331" n="HIAT:ip">.</nts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_334" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_336" n="HIAT:w" s="T91">wossɨlʼewlʼe</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_339" n="HIAT:w" s="T92">aj</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_342" n="HIAT:w" s="T93">laqɨzaŋ</ts>
                  <nts id="Seg_343" n="HIAT:ip">.</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_346" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_348" n="HIAT:w" s="T94">nilʼdʼiŋ</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_351" n="HIAT:w" s="T95">me</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_354" n="HIAT:w" s="T96">astə</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_357" n="HIAT:w" s="T97">kutčikwot</ts>
                  <nts id="Seg_358" n="HIAT:ip">,</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_361" n="HIAT:w" s="T98">taqtəkwot</ts>
                  <nts id="Seg_362" n="HIAT:ip">,</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_365" n="HIAT:w" s="T99">tundəkwot</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_368" n="HIAT:w" s="T100">swäŋgɨm</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_371" n="HIAT:w" s="T101">säsandə</ts>
                  <nts id="Seg_372" n="HIAT:ip">.</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T102" id="Seg_374" n="sc" s="T1">
               <ts e="T2" id="Seg_376" n="e" s="T1">kundar </ts>
               <ts e="T3" id="Seg_378" n="e" s="T2">me </ts>
               <ts e="T4" id="Seg_380" n="e" s="T3">swäŋɨjgot. </ts>
               <ts e="T5" id="Seg_382" n="e" s="T4">kundar </ts>
               <ts e="T6" id="Seg_384" n="e" s="T5">taqkɨlgwot </ts>
               <ts e="T7" id="Seg_386" n="e" s="T6">suːrulʼe </ts>
               <ts e="T8" id="Seg_388" n="e" s="T7">nandar </ts>
               <ts e="T9" id="Seg_390" n="e" s="T8">me </ts>
               <ts e="T10" id="Seg_392" n="e" s="T9">taqkɨlgwot </ts>
               <ts e="T11" id="Seg_394" n="e" s="T10">i </ts>
               <ts e="T12" id="Seg_396" n="e" s="T11">swäŋɨjgu. </ts>
               <ts e="T13" id="Seg_398" n="e" s="T12">čekɨrkwot </ts>
               <ts e="T14" id="Seg_400" n="e" s="T13">nʼäjim </ts>
               <ts e="T15" id="Seg_402" n="e" s="T14">suharɨŋlʼe, </ts>
               <ts e="T16" id="Seg_404" n="e" s="T15">iːkwot </ts>
               <ts e="T17" id="Seg_406" n="e" s="T16">tüːləsäm, </ts>
               <ts e="T18" id="Seg_408" n="e" s="T17">kanam. </ts>
               <ts e="T19" id="Seg_410" n="e" s="T18">tüːkwot </ts>
               <ts e="T20" id="Seg_412" n="e" s="T19">swäŋɨjej </ts>
               <ts e="T21" id="Seg_414" n="e" s="T20">čwäčondə, </ts>
               <ts e="T22" id="Seg_416" n="e" s="T21">patʼölgwot </ts>
               <ts e="T23" id="Seg_418" n="e" s="T22">lʼeːwɨm, </ts>
               <ts e="T24" id="Seg_420" n="e" s="T23">meːkwot </ts>
               <ts e="T25" id="Seg_422" n="e" s="T24">lʼewi </ts>
               <ts e="T26" id="Seg_424" n="e" s="T25">maːdəm. </ts>
               <ts e="T27" id="Seg_426" n="e" s="T26">nännä </ts>
               <ts e="T28" id="Seg_428" n="e" s="T27">meːkwot </ts>
               <ts e="T29" id="Seg_430" n="e" s="T28">poj </ts>
               <ts e="T30" id="Seg_432" n="e" s="T29">tʼäǯim. </ts>
               <ts e="T31" id="Seg_434" n="e" s="T30">poj </ts>
               <ts e="T32" id="Seg_436" n="e" s="T31">tʼäǯim </ts>
               <ts e="T33" id="Seg_438" n="e" s="T32">meːkwot </ts>
               <ts e="T34" id="Seg_440" n="e" s="T33">qweːɣännä. </ts>
               <ts e="T35" id="Seg_442" n="e" s="T34">taqkɨlgwot </ts>
               <ts e="T36" id="Seg_444" n="e" s="T35">mergɨn </ts>
               <ts e="T37" id="Seg_446" n="e" s="T36">ästɨmbɨdi </ts>
               <ts e="T38" id="Seg_448" n="e" s="T37">swäŋɨm </ts>
               <ts e="T39" id="Seg_450" n="e" s="T38">tʼäǯičikwot. </ts>
               <ts e="T40" id="Seg_452" n="e" s="T39">okkɨr </ts>
               <ts e="T41" id="Seg_454" n="e" s="T40">taːlǯikut </ts>
               <ts e="T42" id="Seg_456" n="e" s="T41">tʼätǯim, </ts>
               <ts e="T43" id="Seg_458" n="e" s="T42">šɨttə </ts>
               <ts e="T44" id="Seg_460" n="e" s="T43">qaj </ts>
               <ts e="T45" id="Seg_462" n="e" s="T44">tättə </ts>
               <ts e="T46" id="Seg_464" n="e" s="T45">qum </ts>
               <ts e="T47" id="Seg_466" n="e" s="T46">na </ts>
               <ts e="T48" id="Seg_468" n="e" s="T47">tʼätǯim </ts>
               <ts e="T49" id="Seg_470" n="e" s="T48">mišalgut </ts>
               <ts e="T50" id="Seg_472" n="e" s="T49">kuːdgon. </ts>
               <ts e="T51" id="Seg_474" n="e" s="T50">tʼätčizä </ts>
               <ts e="T52" id="Seg_476" n="e" s="T51">qättɨlʼe </ts>
               <ts e="T53" id="Seg_478" n="e" s="T52">tɨːdɨm </ts>
               <ts e="T54" id="Seg_480" n="e" s="T53">swäŋɣə </ts>
               <ts e="T55" id="Seg_482" n="e" s="T54">innännɨ </ts>
               <ts e="T56" id="Seg_484" n="e" s="T55">čečolʼdʼikuŋ. </ts>
               <ts e="T57" id="Seg_486" n="e" s="T56">čečolʼdimbədi </ts>
               <ts e="T58" id="Seg_488" n="e" s="T57">swäŋɨm </ts>
               <ts e="T59" id="Seg_490" n="e" s="T58">ilʼlʼe </ts>
               <ts e="T60" id="Seg_492" n="e" s="T59">me </ts>
               <ts e="T61" id="Seg_494" n="e" s="T60">taqkɨlgwot. </ts>
               <ts e="T62" id="Seg_496" n="e" s="T61">okkɨrɨŋ </ts>
               <ts e="T63" id="Seg_498" n="e" s="T62">tʼätǯičitäːɣɨn </ts>
               <ts e="T64" id="Seg_500" n="e" s="T63">man </ts>
               <ts e="T65" id="Seg_502" n="e" s="T64">innä </ts>
               <ts e="T66" id="Seg_504" n="e" s="T65">manǯiäŋ. </ts>
               <ts e="T67" id="Seg_506" n="e" s="T66">swäŋgi </ts>
               <ts e="T68" id="Seg_508" n="e" s="T67">oːllə </ts>
               <ts e="T69" id="Seg_510" n="e" s="T68">qäːttɨt </ts>
               <ts e="T70" id="Seg_512" n="e" s="T69">man </ts>
               <ts e="T71" id="Seg_514" n="e" s="T70">saːjow. </ts>
               <ts e="T72" id="Seg_516" n="e" s="T71">man </ts>
               <ts e="T73" id="Seg_518" n="e" s="T72">kum </ts>
               <ts e="T74" id="Seg_520" n="e" s="T73">üːttaw. </ts>
               <ts e="T75" id="Seg_522" n="e" s="T74">orannaw </ts>
               <ts e="T76" id="Seg_524" n="e" s="T75">piːŋganɨzä </ts>
               <ts e="T77" id="Seg_526" n="e" s="T76">taːdɨbɨlʼe </ts>
               <ts e="T78" id="Seg_528" n="e" s="T77">saːjow. </ts>
               <ts e="T79" id="Seg_530" n="e" s="T78">niːdʼadäk </ts>
               <ts e="T80" id="Seg_532" n="e" s="T79">qaːrɨnʼnʼaŋ: </ts>
               <ts e="T81" id="Seg_534" n="e" s="T80">paːj, </ts>
               <ts e="T82" id="Seg_536" n="e" s="T81">paːj, </ts>
               <ts e="T83" id="Seg_538" n="e" s="T82">paːj! </ts>
               <ts e="T84" id="Seg_540" n="e" s="T83">i </ts>
               <ts e="T85" id="Seg_542" n="e" s="T84">čečolʼdʼäŋ. </ts>
               <ts e="T86" id="Seg_544" n="e" s="T85">tʼeːlɨmdə </ts>
               <ts e="T87" id="Seg_546" n="e" s="T86">korʼčaw. </ts>
               <ts e="T88" id="Seg_548" n="e" s="T87">kundə </ts>
               <ts e="T89" id="Seg_550" n="e" s="T88">ippɨzaŋ </ts>
               <ts e="T90" id="Seg_552" n="e" s="T89">tʼümbɨn </ts>
               <ts e="T91" id="Seg_554" n="e" s="T90">barɣɨn. </ts>
               <ts e="T92" id="Seg_556" n="e" s="T91">wossɨlʼewlʼe </ts>
               <ts e="T93" id="Seg_558" n="e" s="T92">aj </ts>
               <ts e="T94" id="Seg_560" n="e" s="T93">laqɨzaŋ. </ts>
               <ts e="T95" id="Seg_562" n="e" s="T94">nilʼdʼiŋ </ts>
               <ts e="T96" id="Seg_564" n="e" s="T95">me </ts>
               <ts e="T97" id="Seg_566" n="e" s="T96">astə </ts>
               <ts e="T98" id="Seg_568" n="e" s="T97">kutčikwot, </ts>
               <ts e="T99" id="Seg_570" n="e" s="T98">taqtəkwot, </ts>
               <ts e="T100" id="Seg_572" n="e" s="T99">tundəkwot </ts>
               <ts e="T101" id="Seg_574" n="e" s="T100">swäŋgɨm </ts>
               <ts e="T102" id="Seg_576" n="e" s="T101">säsandə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_577" s="T1">KMS_1963_NutHunting_nar.001 (001.001)</ta>
            <ta e="T12" id="Seg_578" s="T4">KMS_1963_NutHunting_nar.002 (001.002)</ta>
            <ta e="T18" id="Seg_579" s="T12">KMS_1963_NutHunting_nar.003 (001.003)</ta>
            <ta e="T26" id="Seg_580" s="T18">KMS_1963_NutHunting_nar.004 (001.004)</ta>
            <ta e="T30" id="Seg_581" s="T26">KMS_1963_NutHunting_nar.005 (001.005)</ta>
            <ta e="T34" id="Seg_582" s="T30">KMS_1963_NutHunting_nar.006 (001.006)</ta>
            <ta e="T39" id="Seg_583" s="T34">KMS_1963_NutHunting_nar.007 (001.007)</ta>
            <ta e="T50" id="Seg_584" s="T39">KMS_1963_NutHunting_nar.008 (001.008)</ta>
            <ta e="T56" id="Seg_585" s="T50">KMS_1963_NutHunting_nar.009 (001.009)</ta>
            <ta e="T61" id="Seg_586" s="T56">KMS_1963_NutHunting_nar.010 (001.010)</ta>
            <ta e="T66" id="Seg_587" s="T61">KMS_1963_NutHunting_nar.011 (001.011)</ta>
            <ta e="T71" id="Seg_588" s="T66">KMS_1963_NutHunting_nar.012 (001.012)</ta>
            <ta e="T74" id="Seg_589" s="T71">KMS_1963_NutHunting_nar.013 (001.013)</ta>
            <ta e="T78" id="Seg_590" s="T74">KMS_1963_NutHunting_nar.014 (001.014)</ta>
            <ta e="T83" id="Seg_591" s="T78">KMS_1963_NutHunting_nar.015 (001.015)</ta>
            <ta e="T85" id="Seg_592" s="T83">KMS_1963_NutHunting_nar.016 (001.016)</ta>
            <ta e="T87" id="Seg_593" s="T85">KMS_1963_NutHunting_nar.017 (001.017)</ta>
            <ta e="T91" id="Seg_594" s="T87">KMS_1963_NutHunting_nar.018 (001.018)</ta>
            <ta e="T94" id="Seg_595" s="T91">KMS_1963_NutHunting_nar.019 (001.019)</ta>
            <ta e="T102" id="Seg_596" s="T94">KMS_1963_NutHunting_nar.020 (001.020)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_597" s="T1">кун′дар ме ′свӓңый‵гот.</ta>
            <ta e="T12" id="Seg_598" s="T4">кун′дар ‵таkкыл′гвот ′сӯрулʼе нан′дар ме таkкыл′гвот и ′свӓңы(а)йгу.</ta>
            <ta e="T18" id="Seg_599" s="T12">‵тшекыр′кwот ′нʼӓ(j)им су′харыңлʼе, ′ӣкwот ′тӱ̄лъсӓм, ка′нам.</ta>
            <ta e="T26" id="Seg_600" s="T18">′тӱ̄квот ′свӓңыjей ′тшвӓ‵тшондъ, па′тʼӧлгвот ′лʼе̄вым, ′ме̄кwот ′лʼеви ′ма̄дъм.</ta>
            <ta e="T30" id="Seg_601" s="T26">′нӓннӓ ′ме̄квот ′пой ′тʼӓджим.</ta>
            <ta e="T34" id="Seg_602" s="T30">′пой ′тʼӓджим ′ме̄квот ′kве̄ɣӓннӓ.</ta>
            <ta e="T39" id="Seg_603" s="T34">′таkкыл′гвот ‵мер′гын ′ӓстымбыди ′свӓң(ɣ)ым ‵тʼӓджитши′квот.</ta>
            <ta e="T50" id="Seg_604" s="T39">′оккыр ′та̄лджикут тʼӓтджим, ′шыттъ kай ′тӓттъ kум на тʼӓт′джим ми′шалгут ′кӯдгон.</ta>
            <ta e="T56" id="Seg_605" s="T50">тʼӓттшизӓ ′kӓттылʼе ты̄′дым ′свӓңɣъ и′ннӓн(н)ы тше′тшолʼдʼикуң.</ta>
            <ta e="T61" id="Seg_606" s="T56">тше‵тшолʼдимбъди ′свӓң(ɣ)ым и′лʼлʼе ме ‵таkкыл′гвот.</ta>
            <ta e="T66" id="Seg_607" s="T61">оккы′рың ‵тʼӓтджитши′тӓ̄ɣын ман и′ннӓ ‵манджи′ӓң.</ta>
            <ta e="T71" id="Seg_608" s="T66">′свӓңги о̄′ллъ ′kӓ̄ттыт ман са̄′jоw.</ta>
            <ta e="T74" id="Seg_609" s="T71">ман ′кум ′ӱ̄т(т)аw.</ta>
            <ta e="T78" id="Seg_610" s="T74">о′раннаw ′пӣңганы‵зӓ ′та̄дыбылʼе са̄′jоw.</ta>
            <ta e="T83" id="Seg_611" s="T78">′нӣдʼадӓк ′kа̄рынʼнʼаң: па̄й, па̄й, па̄й!</ta>
            <ta e="T85" id="Seg_612" s="T83">и тше′тшолʼдʼӓң.</ta>
            <ta e="T87" id="Seg_613" s="T85">′тʼе̄лымдъ корʼ′тшаw.</ta>
            <ta e="T91" id="Seg_614" s="T87">′кундъ ′иппызаң ′тʼӱмбын ′барɣын.</ta>
            <ta e="T94" id="Seg_615" s="T91">воссы′лʼевлʼе ай ‵лаkы′заң.</ta>
            <ta e="T102" id="Seg_616" s="T94">нилʼ′дʼиң ме ′астъ ‵куттши′кwот, ′таkтъкwот, ′тундъкwот свӓңгым ′сӓсандъ.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_617" s="T1">kundar me sväŋɨjgot.</ta>
            <ta e="T12" id="Seg_618" s="T4">kundar taqkɨlgvot suːrulʼe nandar me taqkɨlgvot i sväŋɨ(a)jgu.</ta>
            <ta e="T18" id="Seg_619" s="T12">tšekɨrkwot nʼä(j)im suharɨŋlʼe, iːkwot tüːləsäm, kanam.</ta>
            <ta e="T26" id="Seg_620" s="T18">tüːkvot sväŋɨjej tšvätšondə, patʼölgvot lʼeːvɨm, meːkwot lʼevi maːdəm.</ta>
            <ta e="T30" id="Seg_621" s="T26">nännä meːkvot poj tʼäǯim.</ta>
            <ta e="T34" id="Seg_622" s="T30">poj tʼäǯim meːkvot qveːɣännä.</ta>
            <ta e="T39" id="Seg_623" s="T34">taqkɨlgvot mergɨn ästɨmbɨdi sväŋ(ɣ)ɨm tʼäǯitšikvot.</ta>
            <ta e="T50" id="Seg_624" s="T39">okkɨr taːlǯikut tʼätǯim, šɨttə qaj tättə qum na tʼätǯim mišalgut kuːdgon.</ta>
            <ta e="T56" id="Seg_625" s="T50">tʼättšizä qättɨlʼe tɨːdɨm sväŋɣə innän(n)ɨ tšetšolʼdʼikuŋ.</ta>
            <ta e="T61" id="Seg_626" s="T56">tšetšolʼdimbədi sväŋ(ɣ)ɨm ilʼlʼe me taqkɨlgvot.</ta>
            <ta e="T66" id="Seg_627" s="T61">okkɨrɨŋ tʼätǯitšitäːɣɨn man innä manǯiäŋ.</ta>
            <ta e="T71" id="Seg_628" s="T66">sväŋgi oːllə qäːttɨt man saːjow.</ta>
            <ta e="T74" id="Seg_629" s="T71">man kum üːt(t)aw.</ta>
            <ta e="T78" id="Seg_630" s="T74">orannaw piːŋganɨzä taːdɨbɨlʼe saːjow.</ta>
            <ta e="T83" id="Seg_631" s="T78">niːdʼadäk qaːrɨnʼnʼaŋ: paːj, paːj, paːj!</ta>
            <ta e="T85" id="Seg_632" s="T83">i tšetšolʼdʼäŋ.</ta>
            <ta e="T87" id="Seg_633" s="T85">tʼeːlɨmdə korʼtšaw.</ta>
            <ta e="T91" id="Seg_634" s="T87">kundə ippɨzaŋ tʼümbɨn barɣɨn.</ta>
            <ta e="T94" id="Seg_635" s="T91">vossɨlʼevlʼe aj laqɨzaŋ.</ta>
            <ta e="T102" id="Seg_636" s="T94">nilʼdʼiŋ me astə kuttšikwot, taqtəkwot, tundəkwot sväŋgɨm säsandə.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_637" s="T1">kundar me swäŋɨjgot. </ta>
            <ta e="T12" id="Seg_638" s="T4">kundar taqkɨlgwot suːrulʼe nandar me taqkɨlgwot i swäŋɨjgu. </ta>
            <ta e="T18" id="Seg_639" s="T12">čekɨrkwot nʼäjim suharɨŋlʼe, iːkwot tüːləsäm, kanam. </ta>
            <ta e="T26" id="Seg_640" s="T18">tüːkwot swäŋɨjej čwäčondə, patʼölgwot lʼeːwɨm, meːkwot lʼewi maːdəm. </ta>
            <ta e="T30" id="Seg_641" s="T26">nännä meːkwot poj tʼäǯim. </ta>
            <ta e="T34" id="Seg_642" s="T30">poj tʼäǯim meːkwot qweːɣännä. </ta>
            <ta e="T39" id="Seg_643" s="T34">taqkɨlgwot mergɨn ästɨmbɨdi swäŋɨm tʼäǯičikwot. </ta>
            <ta e="T50" id="Seg_644" s="T39">okkɨr taːlǯikut tʼätǯim, šɨttə qaj tättə qum na tʼätǯim mišalgut kuːdgon. </ta>
            <ta e="T56" id="Seg_645" s="T50">tʼätčizä qättɨlʼe tɨːdɨm swäŋɣə innännɨ čečolʼdʼikuŋ. </ta>
            <ta e="T61" id="Seg_646" s="T56">čečolʼdimbədi swäŋɨm ilʼlʼe me taqkɨlgwot. </ta>
            <ta e="T66" id="Seg_647" s="T61">okkɨrɨŋ tʼätǯičitäːɣɨn man innä manǯiäŋ. </ta>
            <ta e="T71" id="Seg_648" s="T66">swäŋgi oːllə qäːttɨt man saːjow. </ta>
            <ta e="T74" id="Seg_649" s="T71">man kum üːttaw. </ta>
            <ta e="T78" id="Seg_650" s="T74">orannaw piːŋganɨzä taːdɨbɨlʼe saːjow. </ta>
            <ta e="T83" id="Seg_651" s="T78">niːdʼadäk qaːrɨnʼnʼaŋ: paːj, paːj, paːj! </ta>
            <ta e="T85" id="Seg_652" s="T83">i čečolʼdʼäŋ. </ta>
            <ta e="T87" id="Seg_653" s="T85">tʼeːlɨmdə korʼčaw. </ta>
            <ta e="T91" id="Seg_654" s="T87">kundə ippɨzaŋ tʼümbɨn barɣɨn. </ta>
            <ta e="T94" id="Seg_655" s="T91">wossɨlʼewlʼe aj laqɨzaŋ. </ta>
            <ta e="T102" id="Seg_656" s="T94">nilʼdʼiŋ me astə kutčikwot, taqtəkwot, tundəkwot swäŋgɨm säsandə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_657" s="T1">kundar</ta>
            <ta e="T3" id="Seg_658" s="T2">me</ta>
            <ta e="T4" id="Seg_659" s="T3">swäŋɨ-j-g-ot</ta>
            <ta e="T5" id="Seg_660" s="T4">kundar</ta>
            <ta e="T6" id="Seg_661" s="T5">taqkɨ-le-g-w-ot</ta>
            <ta e="T7" id="Seg_662" s="T6">suːru-lʼe</ta>
            <ta e="T8" id="Seg_663" s="T7">nandar</ta>
            <ta e="T9" id="Seg_664" s="T8">me</ta>
            <ta e="T10" id="Seg_665" s="T9">taqkɨ-le-g-w-ot</ta>
            <ta e="T11" id="Seg_666" s="T10">i</ta>
            <ta e="T12" id="Seg_667" s="T11">swäŋɨ-j-gu</ta>
            <ta e="T13" id="Seg_668" s="T12">čekɨr-k-w-ot</ta>
            <ta e="T14" id="Seg_669" s="T13">nʼäj-i-m</ta>
            <ta e="T15" id="Seg_670" s="T14">suhar-ɨ-ŋlʼe</ta>
            <ta e="T16" id="Seg_671" s="T15">iː-k-w-ot</ta>
            <ta e="T17" id="Seg_672" s="T16">tüːləsä-m</ta>
            <ta e="T18" id="Seg_673" s="T17">kana-m</ta>
            <ta e="T19" id="Seg_674" s="T18">tüː-k-w-ot</ta>
            <ta e="T20" id="Seg_675" s="T19">swäŋɨ-je-j</ta>
            <ta e="T21" id="Seg_676" s="T20">čwäč-o-ndə</ta>
            <ta e="T22" id="Seg_677" s="T21">patʼöl-g-w-ot</ta>
            <ta e="T23" id="Seg_678" s="T22">lʼeːwɨ-m</ta>
            <ta e="T24" id="Seg_679" s="T23">meː-k-w-ot</ta>
            <ta e="T25" id="Seg_680" s="T24">lʼewi</ta>
            <ta e="T26" id="Seg_681" s="T25">maːd-ə-m</ta>
            <ta e="T27" id="Seg_682" s="T26">nännä</ta>
            <ta e="T28" id="Seg_683" s="T27">meː-k-w-ot</ta>
            <ta e="T29" id="Seg_684" s="T28">po-j</ta>
            <ta e="T30" id="Seg_685" s="T29">tʼäǯi-m</ta>
            <ta e="T31" id="Seg_686" s="T30">po-j</ta>
            <ta e="T32" id="Seg_687" s="T31">tʼäǯi-m</ta>
            <ta e="T33" id="Seg_688" s="T32">meː-k-w-ot</ta>
            <ta e="T34" id="Seg_689" s="T33">qweː-ɣännä</ta>
            <ta e="T35" id="Seg_690" s="T34">taqkɨ-le-g-w-ot</ta>
            <ta e="T36" id="Seg_691" s="T35">mergɨ-n</ta>
            <ta e="T37" id="Seg_692" s="T36">ästɨ-mbɨdi</ta>
            <ta e="T38" id="Seg_693" s="T37">swäŋɨ-m</ta>
            <ta e="T39" id="Seg_694" s="T38">tʼäǯi-či-k-w-ot</ta>
            <ta e="T40" id="Seg_695" s="T39">okkɨr</ta>
            <ta e="T41" id="Seg_696" s="T40">taːlǯi-ku-t</ta>
            <ta e="T42" id="Seg_697" s="T41">tʼätǯi-m</ta>
            <ta e="T43" id="Seg_698" s="T42">šɨttə</ta>
            <ta e="T44" id="Seg_699" s="T43">qaj</ta>
            <ta e="T45" id="Seg_700" s="T44">tättə</ta>
            <ta e="T46" id="Seg_701" s="T45">qum</ta>
            <ta e="T47" id="Seg_702" s="T46">na</ta>
            <ta e="T48" id="Seg_703" s="T47">tʼätǯi-m</ta>
            <ta e="T49" id="Seg_704" s="T48">miša-le-gu-t</ta>
            <ta e="T50" id="Seg_705" s="T49">kuːdgo-n</ta>
            <ta e="T51" id="Seg_706" s="T50">tʼätči-zä</ta>
            <ta e="T52" id="Seg_707" s="T51">qättɨ-lʼe</ta>
            <ta e="T53" id="Seg_708" s="T52">tɨːdɨ-m</ta>
            <ta e="T54" id="Seg_709" s="T53">swäŋɣə</ta>
            <ta e="T55" id="Seg_710" s="T54">innä-nnɨ</ta>
            <ta e="T56" id="Seg_711" s="T55">čečo-lʼdʼi-ku-ŋ</ta>
            <ta e="T57" id="Seg_712" s="T56">čečo-lʼdi-mbədi</ta>
            <ta e="T58" id="Seg_713" s="T57">swäŋɨ-m</ta>
            <ta e="T59" id="Seg_714" s="T58">ilʼlʼe</ta>
            <ta e="T60" id="Seg_715" s="T59">me</ta>
            <ta e="T61" id="Seg_716" s="T60">taqkɨ-le-g-w-ot</ta>
            <ta e="T62" id="Seg_717" s="T61">okkɨr-ɨ-ŋ</ta>
            <ta e="T63" id="Seg_718" s="T62">tʼätǯi-či-täː-ɣɨn</ta>
            <ta e="T64" id="Seg_719" s="T63">man</ta>
            <ta e="T65" id="Seg_720" s="T64">innä</ta>
            <ta e="T66" id="Seg_721" s="T65">manǯi-ä-ŋ</ta>
            <ta e="T67" id="Seg_722" s="T66">swäŋgi</ta>
            <ta e="T68" id="Seg_723" s="T67">oːllə</ta>
            <ta e="T69" id="Seg_724" s="T68">qäːttɨ-t</ta>
            <ta e="T70" id="Seg_725" s="T69">man</ta>
            <ta e="T71" id="Seg_726" s="T70">saːj-o-w</ta>
            <ta e="T72" id="Seg_727" s="T71">man</ta>
            <ta e="T73" id="Seg_728" s="T72">kum</ta>
            <ta e="T74" id="Seg_729" s="T73">üːtta-w</ta>
            <ta e="T75" id="Seg_730" s="T74">oran-na-w</ta>
            <ta e="T76" id="Seg_731" s="T75">piːŋga-nɨ-zä</ta>
            <ta e="T77" id="Seg_732" s="T76">taːdɨ-bɨlʼe</ta>
            <ta e="T78" id="Seg_733" s="T77">saːj-o-w</ta>
            <ta e="T79" id="Seg_734" s="T78">niːdʼa-däk</ta>
            <ta e="T80" id="Seg_735" s="T79">qaːrɨ-nʼ-nʼa-ŋ</ta>
            <ta e="T81" id="Seg_736" s="T80">paːj</ta>
            <ta e="T82" id="Seg_737" s="T81">paːj</ta>
            <ta e="T83" id="Seg_738" s="T82">paːj</ta>
            <ta e="T84" id="Seg_739" s="T83">i</ta>
            <ta e="T85" id="Seg_740" s="T84">čečo-lʼdʼä-ŋ</ta>
            <ta e="T86" id="Seg_741" s="T85">tʼeːlɨ-m-də</ta>
            <ta e="T87" id="Seg_742" s="T86">korʼča-w</ta>
            <ta e="T88" id="Seg_743" s="T87">kundə</ta>
            <ta e="T89" id="Seg_744" s="T88">ippɨ-za-ŋ</ta>
            <ta e="T90" id="Seg_745" s="T89">tʼümbɨ-n</ta>
            <ta e="T91" id="Seg_746" s="T90">bar-ɣɨn</ta>
            <ta e="T92" id="Seg_747" s="T91">wossɨ-lʼewlʼe</ta>
            <ta e="T93" id="Seg_748" s="T92">aj</ta>
            <ta e="T94" id="Seg_749" s="T93">laqɨ-za-ŋ</ta>
            <ta e="T95" id="Seg_750" s="T94">nilʼdʼi-ŋ</ta>
            <ta e="T96" id="Seg_751" s="T95">me</ta>
            <ta e="T97" id="Seg_752" s="T96">astə</ta>
            <ta e="T98" id="Seg_753" s="T97">kutči-k-w-ot</ta>
            <ta e="T99" id="Seg_754" s="T98">taq-tə-k-w-ot</ta>
            <ta e="T100" id="Seg_755" s="T99">tundə-k-w-ot</ta>
            <ta e="T101" id="Seg_756" s="T100">swäŋgɨ-m</ta>
            <ta e="T102" id="Seg_757" s="T101">säsan-də</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_758" s="T1">kundar</ta>
            <ta e="T3" id="Seg_759" s="T2">meː</ta>
            <ta e="T4" id="Seg_760" s="T3">swäŋɨ-j-ku-ut</ta>
            <ta e="T5" id="Seg_761" s="T4">kundar</ta>
            <ta e="T6" id="Seg_762" s="T5">takǝ-le-ku-ŋɨ-ut</ta>
            <ta e="T7" id="Seg_763" s="T6">suːrɨ-le</ta>
            <ta e="T8" id="Seg_764" s="T7">nandar</ta>
            <ta e="T9" id="Seg_765" s="T8">meː</ta>
            <ta e="T10" id="Seg_766" s="T9">takǝ-le-ku-ŋɨ-ut</ta>
            <ta e="T11" id="Seg_767" s="T10">i</ta>
            <ta e="T12" id="Seg_768" s="T11">swäŋɨ-j-gu</ta>
            <ta e="T13" id="Seg_769" s="T12">čeqqɨr-ku-ŋɨ-ut</ta>
            <ta e="T14" id="Seg_770" s="T13">nʼaj-ɨ-m</ta>
            <ta e="T15" id="Seg_771" s="T14">suhar-ɨ-wlä</ta>
            <ta e="T16" id="Seg_772" s="T15">iː-ku-ŋɨ-ut</ta>
            <ta e="T17" id="Seg_773" s="T16">tülʼse-m</ta>
            <ta e="T18" id="Seg_774" s="T17">kanak-m</ta>
            <ta e="T19" id="Seg_775" s="T18">tüː-ku-ŋɨ-ut</ta>
            <ta e="T20" id="Seg_776" s="T19">swäŋɨ-ɨ-lʼ</ta>
            <ta e="T21" id="Seg_777" s="T20">tʼwät-ɨ-ndɨ</ta>
            <ta e="T22" id="Seg_778" s="T21">počol-ku-ŋɨ-ut</ta>
            <ta e="T23" id="Seg_779" s="T22">lewi-m</ta>
            <ta e="T24" id="Seg_780" s="T23">meː-ku-ŋɨ-ut</ta>
            <ta e="T25" id="Seg_781" s="T24">lewi</ta>
            <ta e="T26" id="Seg_782" s="T25">maːt-ɨ-m</ta>
            <ta e="T27" id="Seg_783" s="T26">nɨːnɨ</ta>
            <ta e="T28" id="Seg_784" s="T27">meː-ku-ŋɨ-ut</ta>
            <ta e="T29" id="Seg_785" s="T28">po-lʼ</ta>
            <ta e="T30" id="Seg_786" s="T29">tʼäǯe-m</ta>
            <ta e="T31" id="Seg_787" s="T30">po-lʼ</ta>
            <ta e="T32" id="Seg_788" s="T31">tʼäǯe-m</ta>
            <ta e="T33" id="Seg_789" s="T32">meː-ku-ŋɨ-ut</ta>
            <ta e="T34" id="Seg_790" s="T33">qwɛ-qɨnnɨ</ta>
            <ta e="T35" id="Seg_791" s="T34">takǝ-le-ku-ŋɨ-ut</ta>
            <ta e="T36" id="Seg_792" s="T35">merkə-n</ta>
            <ta e="T37" id="Seg_793" s="T36">ästɨ-mbɨdi</ta>
            <ta e="T38" id="Seg_794" s="T37">swäŋɨ-m</ta>
            <ta e="T39" id="Seg_795" s="T38">tʼäǯi-či-ŋ-m-ut</ta>
            <ta e="T40" id="Seg_796" s="T39">okkɨr</ta>
            <ta e="T41" id="Seg_797" s="T40">tuɣol-ku-tɨ</ta>
            <ta e="T42" id="Seg_798" s="T41">tʼäǯe-m</ta>
            <ta e="T43" id="Seg_799" s="T42">šittə</ta>
            <ta e="T44" id="Seg_800" s="T43">qaj</ta>
            <ta e="T45" id="Seg_801" s="T44">tättɨ</ta>
            <ta e="T46" id="Seg_802" s="T45">qum</ta>
            <ta e="T47" id="Seg_803" s="T46">na</ta>
            <ta e="T48" id="Seg_804" s="T47">tʼäǯe-m</ta>
            <ta e="T49" id="Seg_805" s="T48">mišša-le-ku-tɨ</ta>
            <ta e="T50" id="Seg_806" s="T49">kuːdɨgo-n</ta>
            <ta e="T51" id="Seg_807" s="T50">tʼäǯe-se</ta>
            <ta e="T52" id="Seg_808" s="T51">qätə-le</ta>
            <ta e="T53" id="Seg_809" s="T52">tɨtɨŋ-m</ta>
            <ta e="T54" id="Seg_810" s="T53">swäŋɨ</ta>
            <ta e="T55" id="Seg_811" s="T54">innä-nɨ</ta>
            <ta e="T56" id="Seg_812" s="T55">čečo-lʼčǝ-ku-n</ta>
            <ta e="T57" id="Seg_813" s="T56">čečo-lʼčǝ-mbɨdi</ta>
            <ta e="T58" id="Seg_814" s="T57">swäŋɨ-m</ta>
            <ta e="T59" id="Seg_815" s="T58">illä</ta>
            <ta e="T60" id="Seg_816" s="T59">meː</ta>
            <ta e="T61" id="Seg_817" s="T60">takǝ-le-ku-ŋɨ-ut</ta>
            <ta e="T62" id="Seg_818" s="T61">okkɨr-ɨ-k</ta>
            <ta e="T63" id="Seg_819" s="T62">tʼäǯi-či-ptä-qən</ta>
            <ta e="T64" id="Seg_820" s="T63">man</ta>
            <ta e="T65" id="Seg_821" s="T64">innä</ta>
            <ta e="T66" id="Seg_822" s="T65">manǯɨ-ŋɨ-ŋ</ta>
            <ta e="T67" id="Seg_823" s="T66">swäŋɨ</ta>
            <ta e="T68" id="Seg_824" s="T67">olɨ</ta>
            <ta e="T69" id="Seg_825" s="T68">qätə-tɨ</ta>
            <ta e="T70" id="Seg_826" s="T69">man</ta>
            <ta e="T71" id="Seg_827" s="T70">saj-ɨ-m</ta>
            <ta e="T72" id="Seg_828" s="T71">man</ta>
            <ta e="T73" id="Seg_829" s="T72">kuːdɨgo</ta>
            <ta e="T74" id="Seg_830" s="T73">üːtɨ-m</ta>
            <ta e="T75" id="Seg_831" s="T74">oral-ŋɨ-m</ta>
            <ta e="T76" id="Seg_832" s="T75">piːŋga-nɨ-se</ta>
            <ta e="T77" id="Seg_833" s="T76">taːdɨ-mbɨdi</ta>
            <ta e="T78" id="Seg_834" s="T77">saj-ɨ-m</ta>
            <ta e="T79" id="Seg_835" s="T78">nidʼaka-tak</ta>
            <ta e="T80" id="Seg_836" s="T79">qarrə-š-ŋɨ-ŋ</ta>
            <ta e="T81" id="Seg_837" s="T80">paːj</ta>
            <ta e="T82" id="Seg_838" s="T81">paːj</ta>
            <ta e="T83" id="Seg_839" s="T82">paːj</ta>
            <ta e="T84" id="Seg_840" s="T83">i</ta>
            <ta e="T85" id="Seg_841" s="T84">čečo-lʼčǝ-ŋ</ta>
            <ta e="T86" id="Seg_842" s="T85">tʼeːlɨ-m-ndɨ</ta>
            <ta e="T87" id="Seg_843" s="T86">korʼča-m</ta>
            <ta e="T88" id="Seg_844" s="T87">kundɨ</ta>
            <ta e="T89" id="Seg_845" s="T88">ippi-sɨ-ŋ</ta>
            <ta e="T90" id="Seg_846" s="T89">tʼümbɨ-n</ta>
            <ta e="T91" id="Seg_847" s="T90">par-qən</ta>
            <ta e="T92" id="Seg_848" s="T91">wessɨ-lewle</ta>
            <ta e="T93" id="Seg_849" s="T92">aj</ta>
            <ta e="T94" id="Seg_850" s="T93">laqqɨ-sɨ-ŋ</ta>
            <ta e="T95" id="Seg_851" s="T94">nɨlʼǯi-k</ta>
            <ta e="T96" id="Seg_852" s="T95">meː</ta>
            <ta e="T97" id="Seg_853" s="T96">astə</ta>
            <ta e="T98" id="Seg_854" s="T97">kutči-ku-m-ut</ta>
            <ta e="T99" id="Seg_855" s="T98">takǝ-ntɨ-ku-ŋɨ-ut</ta>
            <ta e="T100" id="Seg_856" s="T99">tuːntə-ku-ŋɨ-ut</ta>
            <ta e="T101" id="Seg_857" s="T100">swäŋɨ-m</ta>
            <ta e="T102" id="Seg_858" s="T101">sosan-ndɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_859" s="T1">how</ta>
            <ta e="T3" id="Seg_860" s="T2">we.NOM</ta>
            <ta e="T4" id="Seg_861" s="T3">cone-CAP-HAB-1PL</ta>
            <ta e="T5" id="Seg_862" s="T4">how</ta>
            <ta e="T6" id="Seg_863" s="T5">gather-INCH-HAB-CO-1PL</ta>
            <ta e="T7" id="Seg_864" s="T6">hunt-CVB</ta>
            <ta e="T8" id="Seg_865" s="T7">so</ta>
            <ta e="T9" id="Seg_866" s="T8">we.NOM</ta>
            <ta e="T10" id="Seg_867" s="T9">gather-INCH-HAB-CO-1PL</ta>
            <ta e="T11" id="Seg_868" s="T10">and</ta>
            <ta e="T12" id="Seg_869" s="T11">cone-CAP-INF</ta>
            <ta e="T13" id="Seg_870" s="T12">dry-HAB-CO-1PL</ta>
            <ta e="T14" id="Seg_871" s="T13">bread-EP-ACC</ta>
            <ta e="T15" id="Seg_872" s="T14">rusk-EP-TRL</ta>
            <ta e="T16" id="Seg_873" s="T15">take-HAB-CO-1PL</ta>
            <ta e="T17" id="Seg_874" s="T16">rifle-ACC</ta>
            <ta e="T18" id="Seg_875" s="T17">dog-ACC</ta>
            <ta e="T19" id="Seg_876" s="T18">come-HAB-CO-1PL</ta>
            <ta e="T20" id="Seg_877" s="T19">cone-EP-ADJZ</ta>
            <ta e="T21" id="Seg_878" s="T20">place-EP-ILL</ta>
            <ta e="T22" id="Seg_879" s="T21">prickle-HAB-CO-1PL</ta>
            <ta e="T23" id="Seg_880" s="T22">board-ACC</ta>
            <ta e="T24" id="Seg_881" s="T23">do-HAB-CO-1PL</ta>
            <ta e="T25" id="Seg_882" s="T24">board.[NOM]</ta>
            <ta e="T26" id="Seg_883" s="T25">house-EP-ACC</ta>
            <ta e="T27" id="Seg_884" s="T26">then</ta>
            <ta e="T28" id="Seg_885" s="T27">do-HAB-CO-1PL</ta>
            <ta e="T29" id="Seg_886" s="T28">tree-ADJZ</ta>
            <ta e="T30" id="Seg_887" s="T29">beater-ACC</ta>
            <ta e="T31" id="Seg_888" s="T30">tree-ADJZ</ta>
            <ta e="T32" id="Seg_889" s="T31">beater-ACC</ta>
            <ta e="T33" id="Seg_890" s="T32">do-HAB-CO-1PL</ta>
            <ta e="T34" id="Seg_891" s="T33">birch-EL</ta>
            <ta e="T35" id="Seg_892" s="T34">gather-INCH-HAB-CO-1PL</ta>
            <ta e="T36" id="Seg_893" s="T35">wind-INSTR2</ta>
            <ta e="T37" id="Seg_894" s="T36">drop-PTCP.PST</ta>
            <ta e="T38" id="Seg_895" s="T37">cone-ACC</ta>
            <ta e="T39" id="Seg_896" s="T38">beat-DRV-1SG.S-1SG.O-1PL</ta>
            <ta e="T40" id="Seg_897" s="T39">one</ta>
            <ta e="T41" id="Seg_898" s="T40">carry-HAB-3SG.O</ta>
            <ta e="T42" id="Seg_899" s="T41">beater-ACC</ta>
            <ta e="T43" id="Seg_900" s="T42">two</ta>
            <ta e="T44" id="Seg_901" s="T43">what</ta>
            <ta e="T45" id="Seg_902" s="T44">four</ta>
            <ta e="T46" id="Seg_903" s="T45">human.being.[NOM]</ta>
            <ta e="T47" id="Seg_904" s="T46">this</ta>
            <ta e="T48" id="Seg_905" s="T47">beater-ACC</ta>
            <ta e="T49" id="Seg_906" s="T48">pull.out-INCH-HAB-3SG.O</ta>
            <ta e="T50" id="Seg_907" s="T49">rope-INSTR2</ta>
            <ta e="T51" id="Seg_908" s="T50">beater-INSTR</ta>
            <ta e="T52" id="Seg_909" s="T51">beat-CVB</ta>
            <ta e="T53" id="Seg_910" s="T52">cedar-ACC</ta>
            <ta e="T54" id="Seg_911" s="T53">cone.[NOM]</ta>
            <ta e="T55" id="Seg_912" s="T54">up-ADV.EL</ta>
            <ta e="T56" id="Seg_913" s="T55">fall-PFV-HAB-3SG.S</ta>
            <ta e="T57" id="Seg_914" s="T56">fall-PFV-PTCP.PST</ta>
            <ta e="T58" id="Seg_915" s="T57">cone-ACC</ta>
            <ta e="T59" id="Seg_916" s="T58">down</ta>
            <ta e="T60" id="Seg_917" s="T59">we.NOM</ta>
            <ta e="T61" id="Seg_918" s="T60">gather-INCH-HAB-CO-1PL</ta>
            <ta e="T62" id="Seg_919" s="T61">one-EP-ADVZ</ta>
            <ta e="T63" id="Seg_920" s="T62">beat-DRV-ACTN-LOC</ta>
            <ta e="T64" id="Seg_921" s="T63">I.NOM</ta>
            <ta e="T65" id="Seg_922" s="T64">up</ta>
            <ta e="T66" id="Seg_923" s="T65">look-CO-1SG.S</ta>
            <ta e="T67" id="Seg_924" s="T66">cone.[NOM]</ta>
            <ta e="T68" id="Seg_925" s="T67">head.[NOM]</ta>
            <ta e="T69" id="Seg_926" s="T68">beat-3SG.O</ta>
            <ta e="T70" id="Seg_927" s="T69">I.GEN</ta>
            <ta e="T71" id="Seg_928" s="T70">eye-EP-ACC</ta>
            <ta e="T72" id="Seg_929" s="T71">I.NOM</ta>
            <ta e="T73" id="Seg_930" s="T72">rope.[NOM]</ta>
            <ta e="T74" id="Seg_931" s="T73">let.go-1SG.O</ta>
            <ta e="T75" id="Seg_932" s="T74">catch-CO-1SG.O</ta>
            <ta e="T76" id="Seg_933" s="T75">palm-OBL.1SG-INSTR</ta>
            <ta e="T77" id="Seg_934" s="T76">right.[VBLZ]-PTCP.PST</ta>
            <ta e="T78" id="Seg_935" s="T77">eye-EP-ACC</ta>
            <ta e="T79" id="Seg_936" s="T78">mother.bear-so</ta>
            <ta e="T80" id="Seg_937" s="T79">cry-US-CO-1SG.S</ta>
            <ta e="T81" id="Seg_938" s="T80">oh</ta>
            <ta e="T82" id="Seg_939" s="T81">oh</ta>
            <ta e="T83" id="Seg_940" s="T82">oh</ta>
            <ta e="T84" id="Seg_941" s="T83">and</ta>
            <ta e="T85" id="Seg_942" s="T84">fall-PFV-1SG.S</ta>
            <ta e="T86" id="Seg_943" s="T85">sun-ACC-OBL.3SG</ta>
            <ta e="T87" id="Seg_944" s="T86">%%-1SG.O</ta>
            <ta e="T88" id="Seg_945" s="T87">long</ta>
            <ta e="T89" id="Seg_946" s="T88">lie-PST-1SG.S</ta>
            <ta e="T90" id="Seg_947" s="T89">swamp.moss-GEN</ta>
            <ta e="T91" id="Seg_948" s="T90">top-LOC</ta>
            <ta e="T92" id="Seg_949" s="T91">get.up-CVB2</ta>
            <ta e="T93" id="Seg_950" s="T92">again</ta>
            <ta e="T94" id="Seg_951" s="T93">work-PST-1SG.S</ta>
            <ta e="T95" id="Seg_952" s="T94">such-ADVZ</ta>
            <ta e="T96" id="Seg_953" s="T95">we.NOM</ta>
            <ta e="T97" id="Seg_954" s="T96">%%</ta>
            <ta e="T98" id="Seg_955" s="T97">%%-HAB-1SG.O-1PL</ta>
            <ta e="T99" id="Seg_956" s="T98">gather-IPFV-HAB-CO-1PL</ta>
            <ta e="T100" id="Seg_957" s="T99">carry-HAB-CO-1PL</ta>
            <ta e="T101" id="Seg_958" s="T100">cone-ACC</ta>
            <ta e="T102" id="Seg_959" s="T101">granary-ILL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_960" s="T1">как</ta>
            <ta e="T3" id="Seg_961" s="T2">мы.NOM</ta>
            <ta e="T4" id="Seg_962" s="T3">шишка-CAP-HAB-1PL</ta>
            <ta e="T5" id="Seg_963" s="T4">как</ta>
            <ta e="T6" id="Seg_964" s="T5">собирать-INCH-HAB-CO-1PL</ta>
            <ta e="T7" id="Seg_965" s="T6">охотиться-CVB</ta>
            <ta e="T8" id="Seg_966" s="T7">так</ta>
            <ta e="T9" id="Seg_967" s="T8">мы.NOM</ta>
            <ta e="T10" id="Seg_968" s="T9">собирать-INCH-HAB-CO-1PL</ta>
            <ta e="T11" id="Seg_969" s="T10">и</ta>
            <ta e="T12" id="Seg_970" s="T11">шишка-CAP-INF</ta>
            <ta e="T13" id="Seg_971" s="T12">сушить-HAB-CO-1PL</ta>
            <ta e="T14" id="Seg_972" s="T13">хлеб-EP-ACC</ta>
            <ta e="T15" id="Seg_973" s="T14">сухарь-EP-TRL</ta>
            <ta e="T16" id="Seg_974" s="T15">взять-HAB-CO-1PL</ta>
            <ta e="T17" id="Seg_975" s="T16">ружье-ACC</ta>
            <ta e="T18" id="Seg_976" s="T17">собака-ACC</ta>
            <ta e="T19" id="Seg_977" s="T18">прийти-HAB-CO-1PL</ta>
            <ta e="T20" id="Seg_978" s="T19">шишка-EP-ADJZ</ta>
            <ta e="T21" id="Seg_979" s="T20">местность-EP-ILL</ta>
            <ta e="T22" id="Seg_980" s="T21">уколоть-HAB-CO-1PL</ta>
            <ta e="T23" id="Seg_981" s="T22">доска-ACC</ta>
            <ta e="T24" id="Seg_982" s="T23">делать-HAB-CO-1PL</ta>
            <ta e="T25" id="Seg_983" s="T24">доска.[NOM]</ta>
            <ta e="T26" id="Seg_984" s="T25">дом-EP-ACC</ta>
            <ta e="T27" id="Seg_985" s="T26">потом</ta>
            <ta e="T28" id="Seg_986" s="T27">делать-HAB-CO-1PL</ta>
            <ta e="T29" id="Seg_987" s="T28">дерево-ADJZ</ta>
            <ta e="T30" id="Seg_988" s="T29">колотушка-ACC</ta>
            <ta e="T31" id="Seg_989" s="T30">дерево-ADJZ</ta>
            <ta e="T32" id="Seg_990" s="T31">колотушка-ACC</ta>
            <ta e="T33" id="Seg_991" s="T32">делать-HAB-CO-1PL</ta>
            <ta e="T34" id="Seg_992" s="T33">берёза-EL</ta>
            <ta e="T35" id="Seg_993" s="T34">собирать-INCH-HAB-CO-1PL</ta>
            <ta e="T36" id="Seg_994" s="T35">ветер-INSTR2</ta>
            <ta e="T37" id="Seg_995" s="T36">уронить-PTCP.PST</ta>
            <ta e="T38" id="Seg_996" s="T37">шишка-ACC</ta>
            <ta e="T39" id="Seg_997" s="T38">бить-DRV-1SG.S-1SG.O-1PL</ta>
            <ta e="T40" id="Seg_998" s="T39">один</ta>
            <ta e="T41" id="Seg_999" s="T40">таскать-HAB-3SG.O</ta>
            <ta e="T42" id="Seg_1000" s="T41">колотушка-ACC</ta>
            <ta e="T43" id="Seg_1001" s="T42">два</ta>
            <ta e="T44" id="Seg_1002" s="T43">что</ta>
            <ta e="T45" id="Seg_1003" s="T44">четыре</ta>
            <ta e="T46" id="Seg_1004" s="T45">человек.[NOM]</ta>
            <ta e="T47" id="Seg_1005" s="T46">этот</ta>
            <ta e="T48" id="Seg_1006" s="T47">колотушка-ACC</ta>
            <ta e="T49" id="Seg_1007" s="T48">выдернуть-INCH-HAB-3SG.O</ta>
            <ta e="T50" id="Seg_1008" s="T49">веревка-INSTR2</ta>
            <ta e="T51" id="Seg_1009" s="T50">колотушка-INSTR</ta>
            <ta e="T52" id="Seg_1010" s="T51">бить-CVB</ta>
            <ta e="T53" id="Seg_1011" s="T52">кедр-ACC</ta>
            <ta e="T54" id="Seg_1012" s="T53">шишка.[NOM]</ta>
            <ta e="T55" id="Seg_1013" s="T54">вверх-ADV.EL</ta>
            <ta e="T56" id="Seg_1014" s="T55">упасть-PFV-HAB-3SG.S</ta>
            <ta e="T57" id="Seg_1015" s="T56">упасть-PFV-PTCP.PST</ta>
            <ta e="T58" id="Seg_1016" s="T57">шишка-ACC</ta>
            <ta e="T59" id="Seg_1017" s="T58">вниз</ta>
            <ta e="T60" id="Seg_1018" s="T59">мы.NOM</ta>
            <ta e="T61" id="Seg_1019" s="T60">собирать-INCH-HAB-CO-1PL</ta>
            <ta e="T62" id="Seg_1020" s="T61">один-EP-ADVZ</ta>
            <ta e="T63" id="Seg_1021" s="T62">бить-DRV-ACTN-LOC</ta>
            <ta e="T64" id="Seg_1022" s="T63">я.NOM</ta>
            <ta e="T65" id="Seg_1023" s="T64">вверх</ta>
            <ta e="T66" id="Seg_1024" s="T65">смотреть-CO-1SG.S</ta>
            <ta e="T67" id="Seg_1025" s="T66">шишка.[NOM]</ta>
            <ta e="T68" id="Seg_1026" s="T67">голова.[NOM]</ta>
            <ta e="T69" id="Seg_1027" s="T68">бить-3SG.O</ta>
            <ta e="T70" id="Seg_1028" s="T69">я.GEN</ta>
            <ta e="T71" id="Seg_1029" s="T70">глаз-EP-ACC</ta>
            <ta e="T72" id="Seg_1030" s="T71">я.NOM</ta>
            <ta e="T73" id="Seg_1031" s="T72">веревка.[NOM]</ta>
            <ta e="T74" id="Seg_1032" s="T73">пустить-1SG.O</ta>
            <ta e="T75" id="Seg_1033" s="T74">поймать-CO-1SG.O</ta>
            <ta e="T76" id="Seg_1034" s="T75">ладонь-OBL.1SG-INSTR</ta>
            <ta e="T77" id="Seg_1035" s="T76">правый.[VBLZ]-PTCP.PST</ta>
            <ta e="T78" id="Seg_1036" s="T77">глаз-EP-ACC</ta>
            <ta e="T79" id="Seg_1037" s="T78">медведица-так</ta>
            <ta e="T80" id="Seg_1038" s="T79">кричать-US-CO-1SG.S</ta>
            <ta e="T81" id="Seg_1039" s="T80">ой</ta>
            <ta e="T82" id="Seg_1040" s="T81">ой</ta>
            <ta e="T83" id="Seg_1041" s="T82">ой</ta>
            <ta e="T84" id="Seg_1042" s="T83">и</ta>
            <ta e="T85" id="Seg_1043" s="T84">упасть-PFV-1SG.S</ta>
            <ta e="T86" id="Seg_1044" s="T85">солнце-ACC-OBL.3SG</ta>
            <ta e="T87" id="Seg_1045" s="T86">%%-1SG.O</ta>
            <ta e="T88" id="Seg_1046" s="T87">долго</ta>
            <ta e="T89" id="Seg_1047" s="T88">лежать-PST-1SG.S</ta>
            <ta e="T90" id="Seg_1048" s="T89">болотный.мох-GEN</ta>
            <ta e="T91" id="Seg_1049" s="T90">верх-LOC</ta>
            <ta e="T92" id="Seg_1050" s="T91">встать-CVB2</ta>
            <ta e="T93" id="Seg_1051" s="T92">опять</ta>
            <ta e="T94" id="Seg_1052" s="T93">работать-PST-1SG.S</ta>
            <ta e="T95" id="Seg_1053" s="T94">такой-ADVZ</ta>
            <ta e="T96" id="Seg_1054" s="T95">мы.NOM</ta>
            <ta e="T97" id="Seg_1055" s="T96">%%</ta>
            <ta e="T98" id="Seg_1056" s="T97">%%-HAB-1SG.O-1PL</ta>
            <ta e="T99" id="Seg_1057" s="T98">собирать-IPFV-HAB-CO-1PL</ta>
            <ta e="T100" id="Seg_1058" s="T99">таскать-HAB-CO-1PL</ta>
            <ta e="T101" id="Seg_1059" s="T100">шишка-ACC</ta>
            <ta e="T102" id="Seg_1060" s="T101">амбар-ILL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1061" s="T1">interrog</ta>
            <ta e="T3" id="Seg_1062" s="T2">pers</ta>
            <ta e="T4" id="Seg_1063" s="T3">n-n&gt;v-v&gt;v-v:pn</ta>
            <ta e="T5" id="Seg_1064" s="T4">interrog</ta>
            <ta e="T6" id="Seg_1065" s="T5">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T7" id="Seg_1066" s="T6">v-v&gt;adv</ta>
            <ta e="T8" id="Seg_1067" s="T7">adv</ta>
            <ta e="T9" id="Seg_1068" s="T8">pers</ta>
            <ta e="T10" id="Seg_1069" s="T9">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T11" id="Seg_1070" s="T10">conj</ta>
            <ta e="T12" id="Seg_1071" s="T11">n-n&gt;v-v:inf</ta>
            <ta e="T13" id="Seg_1072" s="T12">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T14" id="Seg_1073" s="T13">n-n:ins-n:case</ta>
            <ta e="T15" id="Seg_1074" s="T14">n-n:ins-n:case</ta>
            <ta e="T16" id="Seg_1075" s="T15">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T17" id="Seg_1076" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_1077" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_1078" s="T18">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T20" id="Seg_1079" s="T19">n-n:ins-n&gt;adj</ta>
            <ta e="T21" id="Seg_1080" s="T20">n-n:ins-n:case</ta>
            <ta e="T22" id="Seg_1081" s="T21">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T23" id="Seg_1082" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_1083" s="T23">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T25" id="Seg_1084" s="T24">n.[n:case]</ta>
            <ta e="T26" id="Seg_1085" s="T25">n-n:ins-n:case</ta>
            <ta e="T27" id="Seg_1086" s="T26">adv</ta>
            <ta e="T28" id="Seg_1087" s="T27">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T29" id="Seg_1088" s="T28">n-n&gt;adj</ta>
            <ta e="T30" id="Seg_1089" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_1090" s="T30">n-n&gt;adj</ta>
            <ta e="T32" id="Seg_1091" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_1092" s="T32">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T34" id="Seg_1093" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_1094" s="T34">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T36" id="Seg_1095" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_1096" s="T36">v-v&gt;ptcp</ta>
            <ta e="T38" id="Seg_1097" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_1098" s="T38">v-v&gt;v-v:pn-v:pn-v:pn</ta>
            <ta e="T40" id="Seg_1099" s="T39">num</ta>
            <ta e="T41" id="Seg_1100" s="T40">v-v&gt;v-v:pn</ta>
            <ta e="T42" id="Seg_1101" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_1102" s="T42">num</ta>
            <ta e="T44" id="Seg_1103" s="T43">interrog</ta>
            <ta e="T45" id="Seg_1104" s="T44">num</ta>
            <ta e="T46" id="Seg_1105" s="T45">n.[n:case]</ta>
            <ta e="T47" id="Seg_1106" s="T46">dem</ta>
            <ta e="T48" id="Seg_1107" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_1108" s="T48">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T50" id="Seg_1109" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_1110" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_1111" s="T51">v-v&gt;adv</ta>
            <ta e="T53" id="Seg_1112" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_1113" s="T53">n.[n:case]</ta>
            <ta e="T55" id="Seg_1114" s="T54">adv-adv:case</ta>
            <ta e="T56" id="Seg_1115" s="T55">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T57" id="Seg_1116" s="T56">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T58" id="Seg_1117" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_1118" s="T58">adv</ta>
            <ta e="T60" id="Seg_1119" s="T59">pers</ta>
            <ta e="T61" id="Seg_1120" s="T60">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T62" id="Seg_1121" s="T61">num-n:ins-adj&gt;adv</ta>
            <ta e="T63" id="Seg_1122" s="T62">v-v&gt;v-v&gt;n-n:case</ta>
            <ta e="T64" id="Seg_1123" s="T63">pers</ta>
            <ta e="T65" id="Seg_1124" s="T64">adv</ta>
            <ta e="T66" id="Seg_1125" s="T65">v-v:ins-v:pn</ta>
            <ta e="T67" id="Seg_1126" s="T66">n.[n:case]</ta>
            <ta e="T68" id="Seg_1127" s="T67">n.[n:case]</ta>
            <ta e="T69" id="Seg_1128" s="T68">v-v:pn</ta>
            <ta e="T70" id="Seg_1129" s="T69">pers</ta>
            <ta e="T71" id="Seg_1130" s="T70">n-n:ins-n:case</ta>
            <ta e="T72" id="Seg_1131" s="T71">pers</ta>
            <ta e="T73" id="Seg_1132" s="T72">n.[n:case]</ta>
            <ta e="T74" id="Seg_1133" s="T73">v-v:pn</ta>
            <ta e="T75" id="Seg_1134" s="T74">v-v:ins-v:pn</ta>
            <ta e="T76" id="Seg_1135" s="T75">n-n:obl.poss-n:case</ta>
            <ta e="T77" id="Seg_1136" s="T76">adj.[n&gt;v]-v&gt;ptcp</ta>
            <ta e="T78" id="Seg_1137" s="T77">n-n:ins-n:case</ta>
            <ta e="T79" id="Seg_1138" s="T78">n-adv</ta>
            <ta e="T80" id="Seg_1139" s="T79">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T81" id="Seg_1140" s="T80">interj</ta>
            <ta e="T82" id="Seg_1141" s="T81">interj</ta>
            <ta e="T83" id="Seg_1142" s="T82">interj</ta>
            <ta e="T84" id="Seg_1143" s="T83">conj</ta>
            <ta e="T85" id="Seg_1144" s="T84">v-v&gt;v-v:pn</ta>
            <ta e="T86" id="Seg_1145" s="T85">n-n:case-n:obl.poss</ta>
            <ta e="T87" id="Seg_1146" s="T86">v-v:pn</ta>
            <ta e="T88" id="Seg_1147" s="T87">adv</ta>
            <ta e="T89" id="Seg_1148" s="T88">v-v:tense-v:pn</ta>
            <ta e="T90" id="Seg_1149" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_1150" s="T90">n-n:case</ta>
            <ta e="T92" id="Seg_1151" s="T91">v-v&gt;adv</ta>
            <ta e="T93" id="Seg_1152" s="T92">adv</ta>
            <ta e="T94" id="Seg_1153" s="T93">v-v:tense-v:pn</ta>
            <ta e="T95" id="Seg_1154" s="T94">dem-adj&gt;adv</ta>
            <ta e="T96" id="Seg_1155" s="T95">pers</ta>
            <ta e="T98" id="Seg_1156" s="T97">v-v&gt;v-v:pn-v:pn</ta>
            <ta e="T99" id="Seg_1157" s="T98">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T100" id="Seg_1158" s="T99">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T101" id="Seg_1159" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_1160" s="T101">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1161" s="T1">interrog</ta>
            <ta e="T3" id="Seg_1162" s="T2">pers</ta>
            <ta e="T4" id="Seg_1163" s="T3">v</ta>
            <ta e="T5" id="Seg_1164" s="T4">interrog</ta>
            <ta e="T6" id="Seg_1165" s="T5">v</ta>
            <ta e="T7" id="Seg_1166" s="T6">adv</ta>
            <ta e="T8" id="Seg_1167" s="T7">adv</ta>
            <ta e="T9" id="Seg_1168" s="T8">pers</ta>
            <ta e="T10" id="Seg_1169" s="T9">v</ta>
            <ta e="T11" id="Seg_1170" s="T10">conj</ta>
            <ta e="T12" id="Seg_1171" s="T11">v</ta>
            <ta e="T13" id="Seg_1172" s="T12">v</ta>
            <ta e="T14" id="Seg_1173" s="T13">n</ta>
            <ta e="T15" id="Seg_1174" s="T14">n</ta>
            <ta e="T16" id="Seg_1175" s="T15">v</ta>
            <ta e="T17" id="Seg_1176" s="T16">n</ta>
            <ta e="T18" id="Seg_1177" s="T17">n</ta>
            <ta e="T19" id="Seg_1178" s="T18">v</ta>
            <ta e="T20" id="Seg_1179" s="T19">adj</ta>
            <ta e="T21" id="Seg_1180" s="T20">n</ta>
            <ta e="T22" id="Seg_1181" s="T21">v</ta>
            <ta e="T23" id="Seg_1182" s="T22">n</ta>
            <ta e="T24" id="Seg_1183" s="T23">v</ta>
            <ta e="T25" id="Seg_1184" s="T24">n</ta>
            <ta e="T26" id="Seg_1185" s="T25">n</ta>
            <ta e="T27" id="Seg_1186" s="T26">adv</ta>
            <ta e="T28" id="Seg_1187" s="T27">v</ta>
            <ta e="T29" id="Seg_1188" s="T28">n</ta>
            <ta e="T30" id="Seg_1189" s="T29">n</ta>
            <ta e="T31" id="Seg_1190" s="T30">n</ta>
            <ta e="T32" id="Seg_1191" s="T31">n</ta>
            <ta e="T33" id="Seg_1192" s="T32">v</ta>
            <ta e="T34" id="Seg_1193" s="T33">n</ta>
            <ta e="T35" id="Seg_1194" s="T34">v</ta>
            <ta e="T36" id="Seg_1195" s="T35">n</ta>
            <ta e="T37" id="Seg_1196" s="T36">ptcp</ta>
            <ta e="T38" id="Seg_1197" s="T37">n</ta>
            <ta e="T39" id="Seg_1198" s="T38">v</ta>
            <ta e="T40" id="Seg_1199" s="T39">num</ta>
            <ta e="T41" id="Seg_1200" s="T40">v</ta>
            <ta e="T42" id="Seg_1201" s="T41">n</ta>
            <ta e="T43" id="Seg_1202" s="T42">num</ta>
            <ta e="T44" id="Seg_1203" s="T43">interrog</ta>
            <ta e="T45" id="Seg_1204" s="T44">num</ta>
            <ta e="T46" id="Seg_1205" s="T45">n</ta>
            <ta e="T47" id="Seg_1206" s="T46">dem</ta>
            <ta e="T48" id="Seg_1207" s="T47">n</ta>
            <ta e="T49" id="Seg_1208" s="T48">v</ta>
            <ta e="T50" id="Seg_1209" s="T49">n</ta>
            <ta e="T51" id="Seg_1210" s="T50">n</ta>
            <ta e="T52" id="Seg_1211" s="T51">adv</ta>
            <ta e="T53" id="Seg_1212" s="T52">n</ta>
            <ta e="T54" id="Seg_1213" s="T53">n</ta>
            <ta e="T55" id="Seg_1214" s="T54">adv</ta>
            <ta e="T56" id="Seg_1215" s="T55">v</ta>
            <ta e="T57" id="Seg_1216" s="T56">ptcp</ta>
            <ta e="T58" id="Seg_1217" s="T57">n</ta>
            <ta e="T59" id="Seg_1218" s="T58">adv</ta>
            <ta e="T60" id="Seg_1219" s="T59">pers</ta>
            <ta e="T61" id="Seg_1220" s="T60">v</ta>
            <ta e="T62" id="Seg_1221" s="T61">adv</ta>
            <ta e="T63" id="Seg_1222" s="T62">n</ta>
            <ta e="T64" id="Seg_1223" s="T63">pers</ta>
            <ta e="T65" id="Seg_1224" s="T64">adv</ta>
            <ta e="T66" id="Seg_1225" s="T65">v</ta>
            <ta e="T67" id="Seg_1226" s="T66">n</ta>
            <ta e="T68" id="Seg_1227" s="T67">n</ta>
            <ta e="T69" id="Seg_1228" s="T68">v</ta>
            <ta e="T70" id="Seg_1229" s="T69">pers</ta>
            <ta e="T71" id="Seg_1230" s="T70">n</ta>
            <ta e="T72" id="Seg_1231" s="T71">pers</ta>
            <ta e="T73" id="Seg_1232" s="T72">n</ta>
            <ta e="T74" id="Seg_1233" s="T73">v</ta>
            <ta e="T75" id="Seg_1234" s="T74">v</ta>
            <ta e="T76" id="Seg_1235" s="T75">n</ta>
            <ta e="T77" id="Seg_1236" s="T76">ptcp</ta>
            <ta e="T78" id="Seg_1237" s="T77">n</ta>
            <ta e="T79" id="Seg_1238" s="T78">adv</ta>
            <ta e="T80" id="Seg_1239" s="T79">v</ta>
            <ta e="T81" id="Seg_1240" s="T80">interj</ta>
            <ta e="T82" id="Seg_1241" s="T81">interj</ta>
            <ta e="T83" id="Seg_1242" s="T82">interj</ta>
            <ta e="T84" id="Seg_1243" s="T83">conj</ta>
            <ta e="T85" id="Seg_1244" s="T84">v</ta>
            <ta e="T86" id="Seg_1245" s="T85">n</ta>
            <ta e="T87" id="Seg_1246" s="T86">v</ta>
            <ta e="T88" id="Seg_1247" s="T87">adv</ta>
            <ta e="T89" id="Seg_1248" s="T88">v</ta>
            <ta e="T90" id="Seg_1249" s="T89">n</ta>
            <ta e="T91" id="Seg_1250" s="T90">n</ta>
            <ta e="T92" id="Seg_1251" s="T91">adv</ta>
            <ta e="T93" id="Seg_1252" s="T92">adv</ta>
            <ta e="T94" id="Seg_1253" s="T93">v</ta>
            <ta e="T95" id="Seg_1254" s="T94">adv</ta>
            <ta e="T96" id="Seg_1255" s="T95">pers</ta>
            <ta e="T98" id="Seg_1256" s="T97">v</ta>
            <ta e="T99" id="Seg_1257" s="T98">v</ta>
            <ta e="T100" id="Seg_1258" s="T99">v</ta>
            <ta e="T101" id="Seg_1259" s="T100">n</ta>
            <ta e="T102" id="Seg_1260" s="T101">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_1261" s="T2">pro.h:A</ta>
            <ta e="T6" id="Seg_1262" s="T5">0.1.h:A</ta>
            <ta e="T9" id="Seg_1263" s="T8">pro.h:A</ta>
            <ta e="T13" id="Seg_1264" s="T12">0.1.h:A</ta>
            <ta e="T14" id="Seg_1265" s="T13">np:P</ta>
            <ta e="T16" id="Seg_1266" s="T15">0.1.h:A</ta>
            <ta e="T17" id="Seg_1267" s="T16">np:Th</ta>
            <ta e="T18" id="Seg_1268" s="T17">np:Th</ta>
            <ta e="T19" id="Seg_1269" s="T18">0.1.h:A</ta>
            <ta e="T21" id="Seg_1270" s="T20">np:G</ta>
            <ta e="T22" id="Seg_1271" s="T21">0.1.h:A</ta>
            <ta e="T23" id="Seg_1272" s="T22">np:P</ta>
            <ta e="T24" id="Seg_1273" s="T23">0.1.h:A</ta>
            <ta e="T26" id="Seg_1274" s="T25">np:P</ta>
            <ta e="T27" id="Seg_1275" s="T26">adv:Time</ta>
            <ta e="T28" id="Seg_1276" s="T27">0.1.h:A</ta>
            <ta e="T30" id="Seg_1277" s="T29">np:P</ta>
            <ta e="T32" id="Seg_1278" s="T31">np:P</ta>
            <ta e="T33" id="Seg_1279" s="T32">0.1.h:A</ta>
            <ta e="T34" id="Seg_1280" s="T33">np:So</ta>
            <ta e="T35" id="Seg_1281" s="T34">0.1.h:A</ta>
            <ta e="T36" id="Seg_1282" s="T35">np:Cau</ta>
            <ta e="T38" id="Seg_1283" s="T37">np:Th</ta>
            <ta e="T39" id="Seg_1284" s="T38">0.1.h:A 0.3:P</ta>
            <ta e="T40" id="Seg_1285" s="T39">np.h:A</ta>
            <ta e="T42" id="Seg_1286" s="T41">np:Th</ta>
            <ta e="T46" id="Seg_1287" s="T45">np.h:A</ta>
            <ta e="T48" id="Seg_1288" s="T47">np:Th</ta>
            <ta e="T50" id="Seg_1289" s="T49">np:Ins</ta>
            <ta e="T51" id="Seg_1290" s="T50">np:Ins</ta>
            <ta e="T53" id="Seg_1291" s="T52">np:P</ta>
            <ta e="T54" id="Seg_1292" s="T53">np:P</ta>
            <ta e="T55" id="Seg_1293" s="T54">adv:So</ta>
            <ta e="T58" id="Seg_1294" s="T57">np:Th</ta>
            <ta e="T60" id="Seg_1295" s="T59">pro.h:A</ta>
            <ta e="T64" id="Seg_1296" s="T63">pro.h:A</ta>
            <ta e="T65" id="Seg_1297" s="T64">adv:G</ta>
            <ta e="T68" id="Seg_1298" s="T67">np:A</ta>
            <ta e="T70" id="Seg_1299" s="T69">pro.h:Poss</ta>
            <ta e="T71" id="Seg_1300" s="T70">np:P</ta>
            <ta e="T72" id="Seg_1301" s="T71">pro.h:A</ta>
            <ta e="T73" id="Seg_1302" s="T72">np:Th</ta>
            <ta e="T75" id="Seg_1303" s="T74">0.1.h:A</ta>
            <ta e="T76" id="Seg_1304" s="T75">0.1.h:Poss np:Ins</ta>
            <ta e="T78" id="Seg_1305" s="T77">np:Th</ta>
            <ta e="T80" id="Seg_1306" s="T79">0.1.h:A</ta>
            <ta e="T85" id="Seg_1307" s="T84">0.1.h:P</ta>
            <ta e="T89" id="Seg_1308" s="T88">0.1.h:Th</ta>
            <ta e="T90" id="Seg_1309" s="T89">np:Poss</ta>
            <ta e="T91" id="Seg_1310" s="T90">np:L</ta>
            <ta e="T94" id="Seg_1311" s="T93">0.1.h:A</ta>
            <ta e="T96" id="Seg_1312" s="T95">pro.h:A</ta>
            <ta e="T99" id="Seg_1313" s="T98">0.1.h:A</ta>
            <ta e="T100" id="Seg_1314" s="T99">0.1.h:A</ta>
            <ta e="T101" id="Seg_1315" s="T100">np:Th</ta>
            <ta e="T102" id="Seg_1316" s="T101">np:G</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_1317" s="T2">pro.h:S</ta>
            <ta e="T4" id="Seg_1318" s="T3">v:pred</ta>
            <ta e="T6" id="Seg_1319" s="T5">0.1.h:S v:pred</ta>
            <ta e="T7" id="Seg_1320" s="T6">s:purp</ta>
            <ta e="T9" id="Seg_1321" s="T8">pro.h:S</ta>
            <ta e="T10" id="Seg_1322" s="T9">v:pred</ta>
            <ta e="T12" id="Seg_1323" s="T11">s:purp</ta>
            <ta e="T13" id="Seg_1324" s="T12">0.1.h:S v:pred</ta>
            <ta e="T14" id="Seg_1325" s="T13">np:O</ta>
            <ta e="T16" id="Seg_1326" s="T15">0.1.h:S v:pred</ta>
            <ta e="T17" id="Seg_1327" s="T16">np:O</ta>
            <ta e="T18" id="Seg_1328" s="T17">np:O</ta>
            <ta e="T19" id="Seg_1329" s="T18">0.1.h:S v:pred</ta>
            <ta e="T22" id="Seg_1330" s="T21">0.1.h:S v:pred</ta>
            <ta e="T23" id="Seg_1331" s="T22">np:O</ta>
            <ta e="T24" id="Seg_1332" s="T23">0.1.h:S v:pred</ta>
            <ta e="T26" id="Seg_1333" s="T25">np:O</ta>
            <ta e="T28" id="Seg_1334" s="T27">0.1.h:S v:pred</ta>
            <ta e="T30" id="Seg_1335" s="T29">np:O</ta>
            <ta e="T32" id="Seg_1336" s="T31">np:O</ta>
            <ta e="T33" id="Seg_1337" s="T32">0.1.h:S v:pred</ta>
            <ta e="T35" id="Seg_1338" s="T34">0.1.h:S v:pred</ta>
            <ta e="T38" id="Seg_1339" s="T37">np:O</ta>
            <ta e="T39" id="Seg_1340" s="T38">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T40" id="Seg_1341" s="T39">np.h:S</ta>
            <ta e="T41" id="Seg_1342" s="T40">v:pred</ta>
            <ta e="T42" id="Seg_1343" s="T41">np:O</ta>
            <ta e="T46" id="Seg_1344" s="T45">np.h:S</ta>
            <ta e="T48" id="Seg_1345" s="T47">np:O</ta>
            <ta e="T49" id="Seg_1346" s="T48">v:pred</ta>
            <ta e="T53" id="Seg_1347" s="T50">s:temp</ta>
            <ta e="T54" id="Seg_1348" s="T53">np:S</ta>
            <ta e="T56" id="Seg_1349" s="T55">v:pred</ta>
            <ta e="T58" id="Seg_1350" s="T57">np:O</ta>
            <ta e="T60" id="Seg_1351" s="T59">pro.h:S</ta>
            <ta e="T61" id="Seg_1352" s="T60">v:pred</ta>
            <ta e="T63" id="Seg_1353" s="T61">s:temp</ta>
            <ta e="T64" id="Seg_1354" s="T63">pro.h:S</ta>
            <ta e="T66" id="Seg_1355" s="T65">v:pred</ta>
            <ta e="T68" id="Seg_1356" s="T67">np:S</ta>
            <ta e="T69" id="Seg_1357" s="T68">v:pred</ta>
            <ta e="T71" id="Seg_1358" s="T70">np:O</ta>
            <ta e="T72" id="Seg_1359" s="T71">pro.h:S</ta>
            <ta e="T73" id="Seg_1360" s="T72">np:O</ta>
            <ta e="T74" id="Seg_1361" s="T73">v:pred</ta>
            <ta e="T75" id="Seg_1362" s="T74">0.1.h:S v:pred</ta>
            <ta e="T78" id="Seg_1363" s="T77">np:O</ta>
            <ta e="T80" id="Seg_1364" s="T79">0.1.h:S v:pred</ta>
            <ta e="T85" id="Seg_1365" s="T84">0.1.h:S v:pred</ta>
            <ta e="T86" id="Seg_1366" s="T85">np:O</ta>
            <ta e="T87" id="Seg_1367" s="T86">0.1.h:S v:pred</ta>
            <ta e="T89" id="Seg_1368" s="T88">0.1.h:S v:pred</ta>
            <ta e="T92" id="Seg_1369" s="T91">s:temp</ta>
            <ta e="T94" id="Seg_1370" s="T93">0.1.h:S v:pred</ta>
            <ta e="T96" id="Seg_1371" s="T95">pro.h:S</ta>
            <ta e="T98" id="Seg_1372" s="T97">v:pred</ta>
            <ta e="T99" id="Seg_1373" s="T98">0.1.h:S v:pred</ta>
            <ta e="T100" id="Seg_1374" s="T99">0.1.h:S v:pred</ta>
            <ta e="T101" id="Seg_1375" s="T100">np:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T11" id="Seg_1376" s="T10">RUS:gram</ta>
            <ta e="T15" id="Seg_1377" s="T14">RUS:cult</ta>
            <ta e="T79" id="Seg_1378" s="T78">RUS:cult</ta>
            <ta e="T84" id="Seg_1379" s="T83">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_1380" s="T1">How we gather cones.</ta>
            <ta e="T12" id="Seg_1381" s="T4">How we are going to hunt, so we are going to gather cones.</ta>
            <ta e="T18" id="Seg_1382" s="T12">We dry bread to get zwiebacks, we take rifles and dogs.</ta>
            <ta e="T26" id="Seg_1383" s="T18">We come to a place with cones, we chop planks and make a plank shelter.</ta>
            <ta e="T30" id="Seg_1384" s="T26">Then we make a wooden hammer.</ta>
            <ta e="T34" id="Seg_1385" s="T30">We make a wooden hammer out of birch.</ta>
            <ta e="T39" id="Seg_1386" s="T34">We gather cones dropped by wind and beat them.</ta>
            <ta e="T50" id="Seg_1387" s="T39">One man carries the wooden hammer, two or four others pull this hammer with a rope.</ta>
            <ta e="T56" id="Seg_1388" s="T50">When one beats a cedar with hammer, cones fall from above.</ta>
            <ta e="T61" id="Seg_1389" s="T56">We gather the fallen cones.</ta>
            <ta e="T66" id="Seg_1390" s="T61">Once, when I was hitting with a hammer, I looked up.</ta>
            <ta e="T71" id="Seg_1391" s="T66">And the head of a cone fell into my eye.</ta>
            <ta e="T74" id="Seg_1392" s="T71">I let the rope off.</ta>
            <ta e="T78" id="Seg_1393" s="T74">I grasped my right eye with my palm.</ta>
            <ta e="T83" id="Seg_1394" s="T78">I began to cry as a bear: oh, oh, oh.</ta>
            <ta e="T85" id="Seg_1395" s="T83">And I fell down.</ta>
            <ta e="T87" id="Seg_1396" s="T85">I lost(?) the sun.</ta>
            <ta e="T91" id="Seg_1397" s="T87">I was lying on the moss for a long time.</ta>
            <ta e="T94" id="Seg_1398" s="T91">I got up and started working again.</ta>
            <ta e="T102" id="Seg_1399" s="T94">So we knock down(?) the cones, we gather them and carry them to the granary.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_1400" s="T1">Wie wir Zapfen sammeln.</ta>
            <ta e="T12" id="Seg_1401" s="T4">Wie wir jagen werden, so werden wir Zapfen sammeln.</ta>
            <ta e="T18" id="Seg_1402" s="T12">Wir trocknen Brot, um Zwieback zu kriegen, wir nehmen Gewehre und Hunde (mit).</ta>
            <ta e="T26" id="Seg_1403" s="T18">Wir kommen zu einer Stelle mit Zapfen, wir schlagen Bretter und bauen einen Unterschlupf aus Brettern.</ta>
            <ta e="T30" id="Seg_1404" s="T26">Dann machen wir einen Holzhammer.</ta>
            <ta e="T34" id="Seg_1405" s="T30">Wir machen einen Holzhammer aus Birke.</ta>
            <ta e="T39" id="Seg_1406" s="T34">Wir sammeln die durch Wind heruntergefallenen Zapfen und schlagen sie.</ta>
            <ta e="T50" id="Seg_1407" s="T39">Ein Mann trägt den Holzhammer, zwei oder vier andere ziehen den Hammer mit einem Seil.</ta>
            <ta e="T56" id="Seg_1408" s="T50">Wenn einer die Zeder mit dem Hammer schlägt, fallen die Zapfen herunter.</ta>
            <ta e="T61" id="Seg_1409" s="T56">Wir sammeln die gefallenen Zapfen.</ta>
            <ta e="T66" id="Seg_1410" s="T61">Einmal, als ich mit dem Hammer schlug, sah ich hinauf.</ta>
            <ta e="T71" id="Seg_1411" s="T66">Und die Spitze eines Zapfen fiel mir ins Auge.</ta>
            <ta e="T74" id="Seg_1412" s="T71">Ich ließ das Seil los.</ta>
            <ta e="T78" id="Seg_1413" s="T74">Ich griff mir mit der Handfläche ans rechte Auge.</ta>
            <ta e="T83" id="Seg_1414" s="T78">Ich begann zu schreien wie ein Bär: oh, oh, oh.</ta>
            <ta e="T85" id="Seg_1415" s="T83">Und ich fiel nieder.</ta>
            <ta e="T87" id="Seg_1416" s="T85">Ich verlor(?) die Sonne.</ta>
            <ta e="T91" id="Seg_1417" s="T87">Lange lag ich auf dem Moos.</ta>
            <ta e="T94" id="Seg_1418" s="T91">Ich stand auf und begann wieder zu arbeiten,</ta>
            <ta e="T102" id="Seg_1419" s="T94">So schlugen wir die Zapfen herunter, wir sammeln sie und wir tragen sie zum Speicher.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_1420" s="T1">Как мы собираем шишки.</ta>
            <ta e="T12" id="Seg_1421" s="T4">Так же, как мы собираемся на охоту, так мы и собираемся за шишками.</ta>
            <ta e="T18" id="Seg_1422" s="T12">Сушим хлеб на сухари, берем ружье и собаку.</ta>
            <ta e="T26" id="Seg_1423" s="T18">Приходим в ореховое место, колем доски, делаем дощатую избушку.</ta>
            <ta e="T30" id="Seg_1424" s="T26">Потом делаем деревянную колотушку.</ta>
            <ta e="T34" id="Seg_1425" s="T30">Деревянную колотушку делаем из березы.</ta>
            <ta e="T39" id="Seg_1426" s="T34">Мы собираем ветром сбитые шишки, бьем колотушкой.</ta>
            <ta e="T50" id="Seg_1427" s="T39">Один таскает колотушку, двое или четверо эту колотушку дергают веревкой.</ta>
            <ta e="T56" id="Seg_1428" s="T50">Когда колотушкой ударяют по кедру, шишка сверху падает.</ta>
            <ta e="T61" id="Seg_1429" s="T56">Упавшие вниз шишки мы собираем.</ta>
            <ta e="T66" id="Seg_1430" s="T61">Однажды, когда я ударял колотушкой, я посмотрел наверх.</ta>
            <ta e="T71" id="Seg_1431" s="T66">Головка шишки попала меня в глаз.</ta>
            <ta e="T74" id="Seg_1432" s="T71">Я отпустил веревку.</ta>
            <ta e="T78" id="Seg_1433" s="T74">Я схватился ладонью за правый глаз.</ta>
            <ta e="T83" id="Seg_1434" s="T78">Я по-медвежьи закричал: ой ой ой.</ta>
            <ta e="T85" id="Seg_1435" s="T83">И я упал.</ta>
            <ta e="T87" id="Seg_1436" s="T85">Солнце потерял (?).</ta>
            <ta e="T91" id="Seg_1437" s="T87">Долго лежал во мху.</ta>
            <ta e="T94" id="Seg_1438" s="T91">Встав, опять работал.</ta>
            <ta e="T102" id="Seg_1439" s="T94">Так мы сшибаем (?), собираем, таскаем шишки в амбар.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_1440" s="T1">как мы шишкуем</ta>
            <ta e="T12" id="Seg_1441" s="T4">так же как мы собираемся на охоту так мы и собираемся шишковать</ta>
            <ta e="T18" id="Seg_1442" s="T12">сушим хлеб на сухари берем ружье собаку</ta>
            <ta e="T26" id="Seg_1443" s="T18">приходим в ореховое место колим доски делаем досчаную (из досок) избушку</ta>
            <ta e="T30" id="Seg_1444" s="T26">потом делаем деревянную колотушку</ta>
            <ta e="T34" id="Seg_1445" s="T30">деревянную колотушку делаем из березы</ta>
            <ta e="T39" id="Seg_1446" s="T34">собираем ветром уроненную шишку бьем колотом (колотушкой)</ta>
            <ta e="T50" id="Seg_1447" s="T39">один таскает колотушку двое или четвером эту колотушку (этот колот) дергают веревкой (за веревку)</ta>
            <ta e="T56" id="Seg_1448" s="T50">колотом ударяя кедру шишка сверху падает</ta>
            <ta e="T61" id="Seg_1449" s="T56">упавшую шишку (вниз) мы собираем</ta>
            <ta e="T66" id="Seg_1450" s="T61">однажды ударяя колотом я вверх посмотрел</ta>
            <ta e="T71" id="Seg_1451" s="T66">головка шишки ударила в мой глаз</ta>
            <ta e="T74" id="Seg_1452" s="T71">я веревку отпустил</ta>
            <ta e="T78" id="Seg_1453" s="T74">схватил ладошкой правый глаз</ta>
            <ta e="T83" id="Seg_1454" s="T78">по-медвежьи закричал ой ой ой</ta>
            <ta e="T85" id="Seg_1455" s="T83">и упал</ta>
            <ta e="T87" id="Seg_1456" s="T85">солнце потерял</ta>
            <ta e="T91" id="Seg_1457" s="T87">долго лежал во мху (в моху)</ta>
            <ta e="T94" id="Seg_1458" s="T91">встал опять работал</ta>
            <ta e="T102" id="Seg_1459" s="T94">так мы сшибаем собираем таскаем шишку в амбар</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
