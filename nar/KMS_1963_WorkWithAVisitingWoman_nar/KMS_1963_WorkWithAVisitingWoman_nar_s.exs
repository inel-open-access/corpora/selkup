<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KMS_1963_WorkWithAVisitingWoman_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KMS_1963_WorkWithAVisitingWoman_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">52</ud-information>
            <ud-information attribute-name="# HIAT:w">36</ud-information>
            <ud-information attribute-name="# e">36</ud-information>
            <ud-information attribute-name="# HIAT:u">7</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KMS">
            <abbreviation>KMS</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KMS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T37" id="Seg_0" n="sc" s="T1">
               <ts e="T9" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Ivan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">Pavlovič</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">ärɨmbat</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">tümbədi</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">pajam</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_19" n="HIAT:w" s="T6">oːɣulǯukundi</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_22" n="HIAT:w" s="T7">süsügi</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_25" n="HIAT:w" s="T8">äːǯim</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_29" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">assə</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">kundə</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_37" n="HIAT:w" s="T11">laɣəkuzadi</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_41" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">Täp</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">täːerba</ts>
                  <nts id="Seg_47" n="HIAT:ip">:</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">assä</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">qwändigaj</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_56" n="HIAT:w" s="T16">nädä</ts>
                  <nts id="Seg_57" n="HIAT:ip">,</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_60" n="HIAT:w" s="T17">palʼdʼulʼdʼei</ts>
                  <nts id="Seg_61" n="HIAT:ip">,</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_64" n="HIAT:w" s="T18">niːlʼdʼei</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_66" n="HIAT:ip">(</nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">niːgu</ts>
                  <nts id="Seg_69" n="HIAT:ip">)</nts>
                  <nts id="Seg_70" n="HIAT:ip">.</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_73" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">Täp</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">tümba</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">irandɨzä</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_84" n="HIAT:w" s="T23">okkə</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_87" n="HIAT:w" s="T24">mɨɣɨn</ts>
                  <nts id="Seg_88" n="HIAT:ip">.</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_91" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_93" n="HIAT:w" s="T25">ondə</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_96" n="HIAT:w" s="T26">täp</ts>
                  <nts id="Seg_97" n="HIAT:ip">,</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_100" n="HIAT:w" s="T27">Ivan</ts>
                  <nts id="Seg_101" n="HIAT:ip">,</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">kundə</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">assə</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">aːmdenǯiŋ</ts>
                  <nts id="Seg_111" n="HIAT:ip">.</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_114" n="HIAT:u" s="T31">
                  <nts id="Seg_115" n="HIAT:ip">(</nts>
                  <ts e="T32" id="Seg_117" n="HIAT:w" s="T31">täbɨni</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_120" n="HIAT:w" s="T32">assə</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_123" n="HIAT:w" s="T33">nadomneŋ</ts>
                  <nts id="Seg_124" n="HIAT:ip">)</nts>
                  <nts id="Seg_125" n="HIAT:ip">.</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_128" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">Täp</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">tʼäk</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_136" n="HIAT:w" s="T36">nuːnänǯiŋ</ts>
                  <nts id="Seg_137" n="HIAT:ip">.</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T37" id="Seg_139" n="sc" s="T1">
               <ts e="T2" id="Seg_141" n="e" s="T1">Ivan </ts>
               <ts e="T3" id="Seg_143" n="e" s="T2">Pavlovič </ts>
               <ts e="T4" id="Seg_145" n="e" s="T3">ärɨmbat </ts>
               <ts e="T5" id="Seg_147" n="e" s="T4">tümbədi </ts>
               <ts e="T6" id="Seg_149" n="e" s="T5">pajam </ts>
               <ts e="T7" id="Seg_151" n="e" s="T6">oːɣulǯukundi </ts>
               <ts e="T8" id="Seg_153" n="e" s="T7">süsügi </ts>
               <ts e="T9" id="Seg_155" n="e" s="T8">äːǯim. </ts>
               <ts e="T10" id="Seg_157" n="e" s="T9">assə </ts>
               <ts e="T11" id="Seg_159" n="e" s="T10">kundə </ts>
               <ts e="T12" id="Seg_161" n="e" s="T11">laɣəkuzadi. </ts>
               <ts e="T13" id="Seg_163" n="e" s="T12">Täp </ts>
               <ts e="T14" id="Seg_165" n="e" s="T13">täːerba: </ts>
               <ts e="T15" id="Seg_167" n="e" s="T14">assä </ts>
               <ts e="T16" id="Seg_169" n="e" s="T15">qwändigaj </ts>
               <ts e="T17" id="Seg_171" n="e" s="T16">nädä, </ts>
               <ts e="T18" id="Seg_173" n="e" s="T17">palʼdʼulʼdʼei, </ts>
               <ts e="T19" id="Seg_175" n="e" s="T18">niːlʼdʼei </ts>
               <ts e="T20" id="Seg_177" n="e" s="T19">(niːgu). </ts>
               <ts e="T21" id="Seg_179" n="e" s="T20">Täp </ts>
               <ts e="T22" id="Seg_181" n="e" s="T21">tümba </ts>
               <ts e="T23" id="Seg_183" n="e" s="T22">irandɨzä </ts>
               <ts e="T24" id="Seg_185" n="e" s="T23">okkə </ts>
               <ts e="T25" id="Seg_187" n="e" s="T24">mɨɣɨn. </ts>
               <ts e="T26" id="Seg_189" n="e" s="T25">ondə </ts>
               <ts e="T27" id="Seg_191" n="e" s="T26">täp, </ts>
               <ts e="T28" id="Seg_193" n="e" s="T27">Ivan, </ts>
               <ts e="T29" id="Seg_195" n="e" s="T28">kundə </ts>
               <ts e="T30" id="Seg_197" n="e" s="T29">assə </ts>
               <ts e="T31" id="Seg_199" n="e" s="T30">aːmdenǯiŋ. </ts>
               <ts e="T32" id="Seg_201" n="e" s="T31">(täbɨni </ts>
               <ts e="T33" id="Seg_203" n="e" s="T32">assə </ts>
               <ts e="T34" id="Seg_205" n="e" s="T33">nadomneŋ). </ts>
               <ts e="T35" id="Seg_207" n="e" s="T34">Täp </ts>
               <ts e="T36" id="Seg_209" n="e" s="T35">tʼäk </ts>
               <ts e="T37" id="Seg_211" n="e" s="T36">nuːnänǯiŋ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T9" id="Seg_212" s="T1">KMS_1963_WorkWithAVisitingWoman_nar.001 (001.001)</ta>
            <ta e="T12" id="Seg_213" s="T9">KMS_1963_WorkWithAVisitingWoman_nar.002 (001.002)</ta>
            <ta e="T20" id="Seg_214" s="T12">KMS_1963_WorkWithAVisitingWoman_nar.003 (001.003)</ta>
            <ta e="T25" id="Seg_215" s="T20">KMS_1963_WorkWithAVisitingWoman_nar.004 (001.004)</ta>
            <ta e="T31" id="Seg_216" s="T25">KMS_1963_WorkWithAVisitingWoman_nar.005 (001.005)</ta>
            <ta e="T34" id="Seg_217" s="T31">KMS_1963_WorkWithAVisitingWoman_nar.006 (001.006)</ta>
            <ta e="T37" id="Seg_218" s="T34">KMS_1963_WorkWithAVisitingWoman_nar.007 (001.007)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T9" id="Seg_219" s="T1">Иван Павлович ӓрым′бат ′тӱмбъди па′jам ′о̄ɣулджукунди ′сӱсӱг(ɣ)и ′ӓ̄джим.</ta>
            <ta e="T12" id="Seg_220" s="T9">′ассъ ′кундъ ′лаɣъкузади.</ta>
            <ta e="T20" id="Seg_221" s="T12">тӓп ′тӓ̄ерба: ассӓ ′kwӓнди(ы)гай нӓ′дӓ, палʼдʼулʼ′дʼеи, ′нӣлʼдʼеи (нӣгу).</ta>
            <ta e="T25" id="Seg_222" s="T20">тӓп ′тӱмба и′рандызӓ ′оккъ мы′ɣын.</ta>
            <ta e="T31" id="Seg_223" s="T25">′ондъ тӓп, Иван, кундъ ′ассъ ′а̄мденджиң.</ta>
            <ta e="T34" id="Seg_224" s="T31">(тӓбы′ни ′ассъ ′надомнең).</ta>
            <ta e="T37" id="Seg_225" s="T34">тӓп тʼӓк ′нӯ‵нӓнджиң.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T9" id="Seg_226" s="T1">Ivan Pavlovič ärɨmbat tümbədi pajam oːɣulǯukundi süsüg(ɣ)i äːǯim.</ta>
            <ta e="T12" id="Seg_227" s="T9">assə kundə laɣəkuzadi.</ta>
            <ta e="T20" id="Seg_228" s="T12">täp täːerba: assä qwändi(ɨ)gaj nädä, palʼdʼulʼdʼei, niːlʼdʼei (niːgu).</ta>
            <ta e="T25" id="Seg_229" s="T20">täp tümba irandɨzä okkə mɨɣɨn.</ta>
            <ta e="T31" id="Seg_230" s="T25">ondə täp, Иvan, kundə assə aːmdenǯiŋ.</ta>
            <ta e="T34" id="Seg_231" s="T31">(täbɨni assə nadomneŋ).</ta>
            <ta e="T37" id="Seg_232" s="T34">täp tʼäk nuːnänǯiŋ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T9" id="Seg_233" s="T1">Ivan Pavlovič ärɨmbat tümbədi pajam oːɣulǯukundi süsügi äːǯim. </ta>
            <ta e="T12" id="Seg_234" s="T9">assə kundə laɣəkuzadi. </ta>
            <ta e="T20" id="Seg_235" s="T12">Täp täːerba: assä qwändigaj nädä, palʼdʼulʼdʼei, niːlʼdʼei (niːgu). </ta>
            <ta e="T25" id="Seg_236" s="T20">Täp tümba irandɨzä okkə mɨɣɨn. </ta>
            <ta e="T31" id="Seg_237" s="T25">ondə täp, Ivan, kundə assə aːmdenǯiŋ. </ta>
            <ta e="T34" id="Seg_238" s="T31">(täbɨni assə nadomneŋ). </ta>
            <ta e="T37" id="Seg_239" s="T34">Täp tʼäk nuːnänǯiŋ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_240" s="T1">Ivan</ta>
            <ta e="T3" id="Seg_241" s="T2">Pavlovič</ta>
            <ta e="T4" id="Seg_242" s="T3">ärɨ-mba-t</ta>
            <ta e="T5" id="Seg_243" s="T4">tü-mbədi</ta>
            <ta e="T6" id="Seg_244" s="T5">paja-m</ta>
            <ta e="T7" id="Seg_245" s="T6">oːɣul-lǯu-ku-ndi</ta>
            <ta e="T8" id="Seg_246" s="T7">süsügi</ta>
            <ta e="T9" id="Seg_247" s="T8">äːǯi-m</ta>
            <ta e="T10" id="Seg_248" s="T9">assə</ta>
            <ta e="T11" id="Seg_249" s="T10">kundə</ta>
            <ta e="T12" id="Seg_250" s="T11">laɣə-ku-za-di</ta>
            <ta e="T13" id="Seg_251" s="T12">täp</ta>
            <ta e="T14" id="Seg_252" s="T13">täːerba</ta>
            <ta e="T15" id="Seg_253" s="T14">assä</ta>
            <ta e="T16" id="Seg_254" s="T15">qwändi-gaj</ta>
            <ta e="T17" id="Seg_255" s="T16">nädä</ta>
            <ta e="T18" id="Seg_256" s="T17">palʼdʼu-lʼdʼe-i</ta>
            <ta e="T19" id="Seg_257" s="T18">niː-lʼdʼe-i</ta>
            <ta e="T20" id="Seg_258" s="T19">niː-gu</ta>
            <ta e="T21" id="Seg_259" s="T20">täp</ta>
            <ta e="T22" id="Seg_260" s="T21">tü-mba</ta>
            <ta e="T23" id="Seg_261" s="T22">ira-n-dɨ-zä</ta>
            <ta e="T24" id="Seg_262" s="T23">okkə</ta>
            <ta e="T25" id="Seg_263" s="T24">mɨ-ɣɨn</ta>
            <ta e="T26" id="Seg_264" s="T25">ondə</ta>
            <ta e="T27" id="Seg_265" s="T26">täp</ta>
            <ta e="T28" id="Seg_266" s="T27">Ivan</ta>
            <ta e="T29" id="Seg_267" s="T28">kundə</ta>
            <ta e="T30" id="Seg_268" s="T29">assə</ta>
            <ta e="T31" id="Seg_269" s="T30">aːmde-nǯi-n</ta>
            <ta e="T32" id="Seg_270" s="T31">täb-ɨ-ni</ta>
            <ta e="T33" id="Seg_271" s="T32">assə</ta>
            <ta e="T34" id="Seg_272" s="T33">nadom-ne-ŋ</ta>
            <ta e="T35" id="Seg_273" s="T34">täp</ta>
            <ta e="T36" id="Seg_274" s="T35">tʼäk</ta>
            <ta e="T37" id="Seg_275" s="T36">nuːnä-nǯi-ŋ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_276" s="T1">Ivan</ta>
            <ta e="T3" id="Seg_277" s="T2">Pavlovič</ta>
            <ta e="T4" id="Seg_278" s="T3">ärɨ-mbɨ-tɨ</ta>
            <ta e="T5" id="Seg_279" s="T4">tüː-mbɨdi</ta>
            <ta e="T6" id="Seg_280" s="T5">paja-m</ta>
            <ta e="T7" id="Seg_281" s="T6">oːqəl-lǯi-ku-ntɨ</ta>
            <ta e="T8" id="Seg_282" s="T7">süsügu</ta>
            <ta e="T9" id="Seg_283" s="T8">ɛːǯa-m</ta>
            <ta e="T10" id="Seg_284" s="T9">assɨ</ta>
            <ta e="T11" id="Seg_285" s="T10">kundɨ</ta>
            <ta e="T12" id="Seg_286" s="T11">laqqɨ-ku-sɨ-di</ta>
            <ta e="T13" id="Seg_287" s="T12">tap</ta>
            <ta e="T14" id="Seg_288" s="T13">tärba</ta>
            <ta e="T15" id="Seg_289" s="T14">assɨ</ta>
            <ta e="T16" id="Seg_290" s="T15">qwändi-ka</ta>
            <ta e="T17" id="Seg_291" s="T16">näde</ta>
            <ta e="T18" id="Seg_292" s="T17">paldʼu-lʼčǝ-ŋɨjä</ta>
            <ta e="T19" id="Seg_293" s="T18">niː-lʼčǝ-ŋɨjä</ta>
            <ta e="T20" id="Seg_294" s="T19">niː-gu</ta>
            <ta e="T21" id="Seg_295" s="T20">tap</ta>
            <ta e="T22" id="Seg_296" s="T21">tüː-mbɨ</ta>
            <ta e="T23" id="Seg_297" s="T22">ira-n-tɨ-se</ta>
            <ta e="T24" id="Seg_298" s="T23">okkɨr</ta>
            <ta e="T25" id="Seg_299" s="T24">mɨ-qən</ta>
            <ta e="T26" id="Seg_300" s="T25">ontɨ</ta>
            <ta e="T27" id="Seg_301" s="T26">tap</ta>
            <ta e="T28" id="Seg_302" s="T27">Ivan</ta>
            <ta e="T29" id="Seg_303" s="T28">kundɨ</ta>
            <ta e="T30" id="Seg_304" s="T29">assɨ</ta>
            <ta e="T31" id="Seg_305" s="T30">aːmdi-nǯɨ-n</ta>
            <ta e="T32" id="Seg_306" s="T31">tap-ɨ-nɨ</ta>
            <ta e="T33" id="Seg_307" s="T32">assɨ</ta>
            <ta e="T34" id="Seg_308" s="T33">nadum-ŋɨ-n</ta>
            <ta e="T35" id="Seg_309" s="T34">tap</ta>
            <ta e="T36" id="Seg_310" s="T35">tʼak</ta>
            <ta e="T37" id="Seg_311" s="T36">nuːnə-nǯɨ-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_312" s="T1">Ivan.[NOM]</ta>
            <ta e="T3" id="Seg_313" s="T2">Pavlovich.[NOM]</ta>
            <ta e="T4" id="Seg_314" s="T3">take.care-DUR-3SG.O</ta>
            <ta e="T5" id="Seg_315" s="T4">come-PTCP.PST</ta>
            <ta e="T6" id="Seg_316" s="T5">old.woman-ACC</ta>
            <ta e="T7" id="Seg_317" s="T6">learn-TR-HAB-PTCP.PRS</ta>
            <ta e="T8" id="Seg_318" s="T7">Selkup.[NOM]</ta>
            <ta e="T9" id="Seg_319" s="T8">word-ACC</ta>
            <ta e="T10" id="Seg_320" s="T9">NEG</ta>
            <ta e="T11" id="Seg_321" s="T10">long</ta>
            <ta e="T12" id="Seg_322" s="T11">work-HAB-PST-3DU.O</ta>
            <ta e="T13" id="Seg_323" s="T12">(s)he.[NOM]</ta>
            <ta e="T14" id="Seg_324" s="T13">think.[3SG.S]</ta>
            <ta e="T15" id="Seg_325" s="T14">NEG</ta>
            <ta e="T16" id="Seg_326" s="T15">old-DIM</ta>
            <ta e="T17" id="Seg_327" s="T16">girl.[NOM]</ta>
            <ta e="T18" id="Seg_328" s="T17">go-PFV-IMP.3SG.S</ta>
            <ta e="T19" id="Seg_329" s="T18">rest-PFV-IMP.3SG.S</ta>
            <ta e="T20" id="Seg_330" s="T19">rest-INF</ta>
            <ta e="T21" id="Seg_331" s="T20">(s)he.[NOM]</ta>
            <ta e="T22" id="Seg_332" s="T21">come-PST.NAR.[3SG.S]</ta>
            <ta e="T23" id="Seg_333" s="T22">husband-GEN-3SG-COM</ta>
            <ta e="T24" id="Seg_334" s="T23">one</ta>
            <ta e="T25" id="Seg_335" s="T24">something-LOC</ta>
            <ta e="T26" id="Seg_336" s="T25">oneself.3SG</ta>
            <ta e="T27" id="Seg_337" s="T26">(s)he.[NOM]</ta>
            <ta e="T28" id="Seg_338" s="T27">Ivan.[NOM]</ta>
            <ta e="T29" id="Seg_339" s="T28">long</ta>
            <ta e="T30" id="Seg_340" s="T29">NEG</ta>
            <ta e="T31" id="Seg_341" s="T30">sit-FUT-3SG.S</ta>
            <ta e="T32" id="Seg_342" s="T31">(s)he-EP-ALL</ta>
            <ta e="T33" id="Seg_343" s="T32">NEG</ta>
            <ta e="T34" id="Seg_344" s="T33">be.needed-CO-3SG.S</ta>
            <ta e="T35" id="Seg_345" s="T34">(s)he.[NOM]</ta>
            <ta e="T36" id="Seg_346" s="T35">fast</ta>
            <ta e="T37" id="Seg_347" s="T36">get.tired-FUT-3SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_348" s="T1">Иван.[NOM]</ta>
            <ta e="T3" id="Seg_349" s="T2">Павлович.[NOM]</ta>
            <ta e="T4" id="Seg_350" s="T3">беречь-DUR-3SG.O</ta>
            <ta e="T5" id="Seg_351" s="T4">прийти-PTCP.PST</ta>
            <ta e="T6" id="Seg_352" s="T5">старуха-ACC</ta>
            <ta e="T7" id="Seg_353" s="T6">учиться-TR-HAB-PTCP.PRS</ta>
            <ta e="T8" id="Seg_354" s="T7">селькуп.[NOM]</ta>
            <ta e="T9" id="Seg_355" s="T8">слово-ACC</ta>
            <ta e="T10" id="Seg_356" s="T9">NEG</ta>
            <ta e="T11" id="Seg_357" s="T10">долго</ta>
            <ta e="T12" id="Seg_358" s="T11">работать-HAB-PST-3DU.O</ta>
            <ta e="T13" id="Seg_359" s="T12">он(а).[NOM]</ta>
            <ta e="T14" id="Seg_360" s="T13">думать.[3SG.S]</ta>
            <ta e="T15" id="Seg_361" s="T14">NEG</ta>
            <ta e="T16" id="Seg_362" s="T15">старый-DIM</ta>
            <ta e="T17" id="Seg_363" s="T16">девушка.[NOM]</ta>
            <ta e="T18" id="Seg_364" s="T17">ходить-PFV-IMP.3SG.S</ta>
            <ta e="T19" id="Seg_365" s="T18">отдохнуть-PFV-IMP.3SG.S</ta>
            <ta e="T20" id="Seg_366" s="T19">отдохнуть-INF</ta>
            <ta e="T21" id="Seg_367" s="T20">он(а).[NOM]</ta>
            <ta e="T22" id="Seg_368" s="T21">прийти-PST.NAR.[3SG.S]</ta>
            <ta e="T23" id="Seg_369" s="T22">муж-GEN-3SG-COM</ta>
            <ta e="T24" id="Seg_370" s="T23">один</ta>
            <ta e="T25" id="Seg_371" s="T24">нечто-LOC</ta>
            <ta e="T26" id="Seg_372" s="T25">сам.3SG</ta>
            <ta e="T27" id="Seg_373" s="T26">он(а).[NOM]</ta>
            <ta e="T28" id="Seg_374" s="T27">Иван.[NOM]</ta>
            <ta e="T29" id="Seg_375" s="T28">долго</ta>
            <ta e="T30" id="Seg_376" s="T29">NEG</ta>
            <ta e="T31" id="Seg_377" s="T30">сидеть-FUT-3SG.S</ta>
            <ta e="T32" id="Seg_378" s="T31">он(а)-EP-ALL</ta>
            <ta e="T33" id="Seg_379" s="T32">NEG</ta>
            <ta e="T34" id="Seg_380" s="T33">быть.нужным-CO-3SG.S</ta>
            <ta e="T35" id="Seg_381" s="T34">он(а).[NOM]</ta>
            <ta e="T36" id="Seg_382" s="T35">быстро</ta>
            <ta e="T37" id="Seg_383" s="T36">устать-FUT-3SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_384" s="T1">nprop.[n:case]</ta>
            <ta e="T3" id="Seg_385" s="T2">nprop.[n:case]</ta>
            <ta e="T4" id="Seg_386" s="T3">v-v&gt;v-v:pn</ta>
            <ta e="T5" id="Seg_387" s="T4">v-v&gt;ptcp</ta>
            <ta e="T6" id="Seg_388" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_389" s="T6">v-v&gt;v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T8" id="Seg_390" s="T7">n.[n:case]</ta>
            <ta e="T9" id="Seg_391" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_392" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_393" s="T10">adv</ta>
            <ta e="T12" id="Seg_394" s="T11">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_395" s="T12">pers.[n:case]</ta>
            <ta e="T14" id="Seg_396" s="T13">v.[v:pn]</ta>
            <ta e="T15" id="Seg_397" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_398" s="T15">adj-n&gt;n</ta>
            <ta e="T17" id="Seg_399" s="T16">n.[n:case]</ta>
            <ta e="T18" id="Seg_400" s="T17">v-v&gt;v-v:mood.pn</ta>
            <ta e="T19" id="Seg_401" s="T18">v-v&gt;v-v:mood.pn</ta>
            <ta e="T20" id="Seg_402" s="T19">v-v:inf</ta>
            <ta e="T21" id="Seg_403" s="T20">pers.[n:case]</ta>
            <ta e="T22" id="Seg_404" s="T21">v-v:tense.[v:pn]</ta>
            <ta e="T23" id="Seg_405" s="T22">n-n:case-n:poss-n:case</ta>
            <ta e="T24" id="Seg_406" s="T23">num</ta>
            <ta e="T25" id="Seg_407" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_408" s="T25">emphpro</ta>
            <ta e="T27" id="Seg_409" s="T26">pers.[n:case]</ta>
            <ta e="T28" id="Seg_410" s="T27">nprop.[n:case]</ta>
            <ta e="T29" id="Seg_411" s="T28">adv</ta>
            <ta e="T30" id="Seg_412" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_413" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_414" s="T31">pers-n:ins-n:case</ta>
            <ta e="T33" id="Seg_415" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_416" s="T33">v-v:ins-v:pn</ta>
            <ta e="T35" id="Seg_417" s="T34">pers.[n:case]</ta>
            <ta e="T36" id="Seg_418" s="T35">adv</ta>
            <ta e="T37" id="Seg_419" s="T36">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_420" s="T1">nprop</ta>
            <ta e="T3" id="Seg_421" s="T2">nprop</ta>
            <ta e="T4" id="Seg_422" s="T3">v</ta>
            <ta e="T5" id="Seg_423" s="T4">ptcp</ta>
            <ta e="T6" id="Seg_424" s="T5">n</ta>
            <ta e="T7" id="Seg_425" s="T6">adj</ta>
            <ta e="T8" id="Seg_426" s="T7">n</ta>
            <ta e="T9" id="Seg_427" s="T8">n</ta>
            <ta e="T10" id="Seg_428" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_429" s="T10">adj</ta>
            <ta e="T12" id="Seg_430" s="T11">v</ta>
            <ta e="T13" id="Seg_431" s="T12">pers</ta>
            <ta e="T14" id="Seg_432" s="T13">v</ta>
            <ta e="T15" id="Seg_433" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_434" s="T15">n</ta>
            <ta e="T17" id="Seg_435" s="T16">n</ta>
            <ta e="T18" id="Seg_436" s="T17">v</ta>
            <ta e="T19" id="Seg_437" s="T18">v</ta>
            <ta e="T20" id="Seg_438" s="T19">v</ta>
            <ta e="T21" id="Seg_439" s="T20">pers</ta>
            <ta e="T22" id="Seg_440" s="T21">v</ta>
            <ta e="T23" id="Seg_441" s="T22">n</ta>
            <ta e="T24" id="Seg_442" s="T23">num</ta>
            <ta e="T25" id="Seg_443" s="T24">n</ta>
            <ta e="T26" id="Seg_444" s="T25">emphpro</ta>
            <ta e="T27" id="Seg_445" s="T26">pers</ta>
            <ta e="T28" id="Seg_446" s="T27">nprop</ta>
            <ta e="T29" id="Seg_447" s="T28">adv</ta>
            <ta e="T30" id="Seg_448" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_449" s="T30">v</ta>
            <ta e="T32" id="Seg_450" s="T31">pers</ta>
            <ta e="T33" id="Seg_451" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_452" s="T33">v</ta>
            <ta e="T35" id="Seg_453" s="T34">pers</ta>
            <ta e="T36" id="Seg_454" s="T35">adv</ta>
            <ta e="T37" id="Seg_455" s="T36">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_456" s="T1">np.h:S</ta>
            <ta e="T4" id="Seg_457" s="T3">v:pred</ta>
            <ta e="T6" id="Seg_458" s="T5">np.h:O</ta>
            <ta e="T9" id="Seg_459" s="T6">s:rel</ta>
            <ta e="T12" id="Seg_460" s="T11">0.3.h:S v:pred</ta>
            <ta e="T13" id="Seg_461" s="T12">pro.h:S</ta>
            <ta e="T14" id="Seg_462" s="T13">v:pred</ta>
            <ta e="T17" id="Seg_463" s="T16">0.3.h:S n:pred</ta>
            <ta e="T18" id="Seg_464" s="T17">0.3.h:S v:pred</ta>
            <ta e="T19" id="Seg_465" s="T18">0.3.h:S v:pred</ta>
            <ta e="T21" id="Seg_466" s="T20">pro.h:S</ta>
            <ta e="T22" id="Seg_467" s="T21">v:pred</ta>
            <ta e="T27" id="Seg_468" s="T26">pro.h:S</ta>
            <ta e="T31" id="Seg_469" s="T30">v:pred</ta>
            <ta e="T35" id="Seg_470" s="T34">pro.h:S</ta>
            <ta e="T37" id="Seg_471" s="T36">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_472" s="T1">np.h:A</ta>
            <ta e="T6" id="Seg_473" s="T5">np.h:Th</ta>
            <ta e="T7" id="Seg_474" s="T6">0.3.h:E</ta>
            <ta e="T9" id="Seg_475" s="T8">np:Th</ta>
            <ta e="T11" id="Seg_476" s="T10">adv:Time</ta>
            <ta e="T12" id="Seg_477" s="T11">0.3.h:A</ta>
            <ta e="T13" id="Seg_478" s="T12">pro.h:E</ta>
            <ta e="T17" id="Seg_479" s="T16">0.3.h:Th</ta>
            <ta e="T18" id="Seg_480" s="T17">0.3.h:A</ta>
            <ta e="T19" id="Seg_481" s="T18">0.3.h:A</ta>
            <ta e="T21" id="Seg_482" s="T20">pro.h:A</ta>
            <ta e="T23" id="Seg_483" s="T22">np:Com 0.3.h:Poss</ta>
            <ta e="T27" id="Seg_484" s="T26">pro.h:Th</ta>
            <ta e="T29" id="Seg_485" s="T28">adv:Time</ta>
            <ta e="T35" id="Seg_486" s="T34">pro.h:E</ta>
            <ta e="T36" id="Seg_487" s="T35">adv:Time</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_488" s="T1">RUS:cult</ta>
            <ta e="T3" id="Seg_489" s="T2">RUS:cult</ta>
            <ta e="T28" id="Seg_490" s="T27">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T9" id="Seg_491" s="T1">Ivan Pavlovich takes care of the woman who came to study Selkup language.</ta>
            <ta e="T12" id="Seg_492" s="T9">They didn't use to work long.</ta>
            <ta e="T20" id="Seg_493" s="T12">He thinks: she is not an old woman, let her walk, let her have a rest.</ta>
            <ta e="T25" id="Seg_494" s="T20">She came together with her husband.</ta>
            <ta e="T31" id="Seg_495" s="T25">Ivan himself won't sit for a long time.</ta>
            <ta e="T34" id="Seg_496" s="T31">He doesn't need it.</ta>
            <ta e="T37" id="Seg_497" s="T34">He gets tired quickly.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T9" id="Seg_498" s="T1">Ivan Pavlovich kümmert sich um die Frau, die kam, um die selkupische Sprache zu lernen.</ta>
            <ta e="T12" id="Seg_499" s="T9">Sie pflegten nicht lange zu arbeiten.</ta>
            <ta e="T20" id="Seg_500" s="T12">Er denkt: Sie ist keine alte Frau, sie soll laufen, sie soll eine Pause machen.</ta>
            <ta e="T25" id="Seg_501" s="T20">Sie kam zusammen mit ihrem Mann.</ta>
            <ta e="T31" id="Seg_502" s="T25">Ivan selbst wird nicht lange sitzen.</ta>
            <ta e="T34" id="Seg_503" s="T31">Er braucht es nicht.</ta>
            <ta e="T37" id="Seg_504" s="T34">Er wird schnell müde.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T9" id="Seg_505" s="T1">Иван Павлович бережет приезжую женщину, которая изучает селькупский язык.</ta>
            <ta e="T12" id="Seg_506" s="T9">Они не долго работали.</ta>
            <ta e="T20" id="Seg_507" s="T12">Он думает: не старая девушка, пусть походит, пусть отдохнет.</ta>
            <ta e="T25" id="Seg_508" s="T20">Она вместе с мужем приехала.</ta>
            <ta e="T31" id="Seg_509" s="T25">Сам он, Иван, долго не просидит.</ta>
            <ta e="T34" id="Seg_510" s="T31">Ему не нужно.</ta>
            <ta e="T37" id="Seg_511" s="T34">Он быстро устанет.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T9" id="Seg_512" s="T1">бережет приезжую женщину изучающую (обучающую) остяцкий (селькупский) язык (разговор)</ta>
            <ta e="T12" id="Seg_513" s="T9">не долго работали они</ta>
            <ta e="T20" id="Seg_514" s="T12">он думает не старая девушка пусть походит (погуляет) (пусть) отдохнет конец слова </ta>
            <ta e="T25" id="Seg_515" s="T20">она приехала с мужем вместе</ta>
            <ta e="T31" id="Seg_516" s="T25">сам он Иван тоже долго не просидит</ta>
            <ta e="T34" id="Seg_517" s="T31">ему не нужно зачеркнуто (видимо, из-за содержания фразы)</ta>
            <ta e="T37" id="Seg_518" s="T34">он быстро устанет</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto">
            <ta e="T20" id="Seg_519" s="T12">′kwӓнди(ы)га(й?) написан неразборчиво</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
