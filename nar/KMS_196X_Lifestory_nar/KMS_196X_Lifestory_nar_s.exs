<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KMS_196X_Lifestory_nar</transcription-name>
         <referenced-file url="KMS_196X_Lifestory_nar.wav" />
         <referenced-file url="KMS_196X_Lifestory_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KMS_196X_Lifestory_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">145</ud-information>
            <ud-information attribute-name="# HIAT:w">107</ud-information>
            <ud-information attribute-name="# e">107</ud-information>
            <ud-information attribute-name="# HIAT:u">24</ud-information>
            <ud-information attribute-name="# sc">48</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KMS">
            <abbreviation>KMS</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.52" type="appl" />
         <tli id="T2" time="2.0580000000000003" type="appl" />
         <tli id="T3" time="3.5960000000000005" type="appl" />
         <tli id="T4" time="5.134" type="appl" />
         <tli id="T5" time="6.672000000000001" type="appl" />
         <tli id="T6" time="8.21" type="appl" />
         <tli id="T7" time="10.687" type="appl" />
         <tli id="T8" time="13.247285714285715" type="appl" />
         <tli id="T9" time="15.807571428571428" type="appl" />
         <tli id="T10" time="18.367857142857144" type="appl" />
         <tli id="T11" time="20.92814285714286" type="appl" />
         <tli id="T12" time="23.488428571428575" type="appl" />
         <tli id="T13" time="26.04871428571429" type="appl" />
         <tli id="T14" time="28.609" type="appl" />
         <tli id="T15" time="34.674" type="appl" />
         <tli id="T16" time="37.608999999999995" type="appl" />
         <tli id="T17" time="40.544" type="appl" />
         <tli id="T18" time="45.726" type="appl" />
         <tli id="T19" time="46.427499999999995" type="appl" />
         <tli id="T20" time="47.129" type="appl" />
         <tli id="T21" time="49.773" type="appl" />
         <tli id="T22" time="52.074200000000005" type="appl" />
         <tli id="T23" time="54.375400000000006" type="appl" />
         <tli id="T24" time="56.67660000000001" type="appl" />
         <tli id="T25" time="58.9778" type="appl" />
         <tli id="T26" time="61.279" type="appl" />
         <tli id="T27" time="64.838" type="appl" />
         <tli id="T28" time="65.2795" type="appl" />
         <tli id="T29" time="65.721" type="appl" />
         <tli id="T30" time="67.968" type="appl" />
         <tli id="T31" time="69.17566666666667" type="appl" />
         <tli id="T32" time="70.38333333333333" type="appl" />
         <tli id="T33" time="71.591" type="appl" />
         <tli id="T34" time="71.916" type="appl" />
         <tli id="T35" time="73.39666666666666" type="appl" />
         <tli id="T36" time="74.87733333333334" type="appl" />
         <tli id="T37" time="76.358" type="appl" />
         <tli id="T38" time="78.098" type="appl" />
         <tli id="T39" time="78.7765" type="appl" />
         <tli id="T40" time="79.455" type="appl" />
         <tli id="T41" time="80.1335" type="appl" />
         <tli id="T42" time="80.812" type="appl" />
         <tli id="T43" time="89.475" type="appl" />
         <tli id="T44" time="90.5112" type="appl" />
         <tli id="T45" time="91.5474" type="appl" />
         <tli id="T46" time="92.5836" type="appl" />
         <tli id="T47" time="93.6198" type="appl" />
         <tli id="T48" time="94.656" type="appl" />
         <tli id="T49" time="97.76" type="appl" />
         <tli id="T50" time="99.09028571428571" type="appl" />
         <tli id="T51" time="100.42057142857144" type="appl" />
         <tli id="T52" time="101.75085714285714" type="appl" />
         <tli id="T53" time="103.08114285714286" type="appl" />
         <tli id="T54" time="104.41142857142857" type="appl" />
         <tli id="T55" time="105.7417142857143" type="appl" />
         <tli id="T56" time="107.072" type="appl" />
         <tli id="T57" time="110.345" type="appl" />
         <tli id="T58" time="111.1275" type="appl" />
         <tli id="T59" time="111.91" type="appl" />
         <tli id="T60" time="112.6925" type="appl" />
         <tli id="T61" time="113.475" type="appl" />
         <tli id="T62" time="117.332" type="appl" />
         <tli id="T63" time="118.39699999999999" type="appl" />
         <tli id="T64" time="119.462" type="appl" />
         <tli id="T65" time="120.527" type="appl" />
         <tli id="T66" time="125.843" type="appl" />
         <tli id="T67" time="128.58333333333334" type="appl" />
         <tli id="T68" time="131.32366666666667" type="appl" />
         <tli id="T69" time="134.064" type="appl" />
         <tli id="T70" time="139.61" type="appl" />
         <tli id="T71" time="141.08833333333334" type="appl" />
         <tli id="T72" time="142.56666666666666" type="appl" />
         <tli id="T73" time="144.04500000000002" type="appl" />
         <tli id="T74" time="145.52333333333334" type="appl" />
         <tli id="T75" time="147.00166666666667" type="appl" />
         <tli id="T76" time="148.48" type="appl" />
         <tli id="T77" time="150.065" type="appl" />
         <tli id="T78" time="151.43083333333334" type="appl" />
         <tli id="T79" time="152.79666666666665" type="appl" />
         <tli id="T80" time="154.1625" type="appl" />
         <tli id="T81" time="155.52833333333334" type="appl" />
         <tli id="T82" time="156.89416666666665" type="appl" />
         <tli id="T83" time="158.26" type="appl" />
         <tli id="T84" time="162.819" type="appl" />
         <tli id="T85" time="163.87099999999998" type="appl" />
         <tli id="T86" time="164.923" type="appl" />
         <tli id="T87" time="165.975" type="appl" />
         <tli id="T88" time="167.02700000000002" type="appl" />
         <tli id="T89" time="168.079" type="appl" />
         <tli id="T90" time="172.869" type="appl" />
         <tli id="T91" time="174.6742857142857" type="appl" />
         <tli id="T92" time="176.47957142857143" type="appl" />
         <tli id="T93" time="178.28485714285713" type="appl" />
         <tli id="T94" time="180.09014285714287" type="appl" />
         <tli id="T95" time="181.89542857142857" type="appl" />
         <tli id="T96" time="183.7007142857143" type="appl" />
         <tli id="T97" time="185.506" type="appl" />
         <tli id="T98" time="195.258" type="appl" />
         <tli id="T99" time="196.41014285714286" type="appl" />
         <tli id="T100" time="197.56228571428574" type="appl" />
         <tli id="T101" time="198.71442857142858" type="appl" />
         <tli id="T102" time="199.86657142857143" type="appl" />
         <tli id="T103" time="201.01871428571428" type="appl" />
         <tli id="T104" time="202.17085714285716" type="appl" />
         <tli id="T105" time="203.323" type="appl" />
         <tli id="T106" time="209.246" type="appl" />
         <tli id="T107" time="209.94357142857143" type="appl" />
         <tli id="T108" time="210.64114285714285" type="appl" />
         <tli id="T109" time="211.33871428571427" type="appl" />
         <tli id="T110" time="212.03628571428573" type="appl" />
         <tli id="T111" time="212.73385714285715" type="appl" />
         <tli id="T112" time="213.43142857142857" type="appl" />
         <tli id="T113" time="214.129" type="appl" />
         <tli id="T114" time="219.87633019747005" />
         <tli id="T115" time="220.95100000000002" type="appl" />
         <tli id="T116" time="222.019" type="appl" />
         <tli id="T117" time="223.087" type="appl" />
         <tli id="T118" time="224.155" type="appl" />
         <tli id="T119" time="231.558" type="appl" />
         <tli id="T120" time="232.188" type="appl" />
         <tli id="T121" time="232.818" type="appl" />
         <tli id="T122" time="236.117" type="appl" />
         <tli id="T123" time="236.7405" type="appl" />
         <tli id="T124" time="237.36399999999998" type="appl" />
         <tli id="T125" time="237.98749999999998" type="appl" />
         <tli id="T126" time="238.611" type="appl" />
         <tli id="T127" time="244.819" type="appl" />
         <tli id="T128" time="245.1925" type="appl" />
         <tli id="T129" time="245.56599999999997" type="appl" />
         <tli id="T130" time="245.93949999999998" type="appl" />
         <tli id="T131" time="246.313" type="appl" />
         <tli id="T0" time="249.044" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KMS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T6" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Meŋga</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">tessarɨm</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">šɨt</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">kwejgen</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">pot</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T14" id="Seg_19" n="sc" s="T7">
               <ts e="T14" id="Seg_21" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_23" n="HIAT:w" s="T7">Orombaŋ</ts>
                  <nts id="Seg_24" n="HIAT:ip">,</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_27" n="HIAT:w" s="T8">oqqer</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_30" n="HIAT:w" s="T9">köt</ts>
                  <nts id="Seg_31" n="HIAT:ip">,</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">muktetsarum</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_37" n="HIAT:w" s="T11">muktet</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_40" n="HIAT:w" s="T12">kwelǯišat</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_43" n="HIAT:w" s="T13">kod-to</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T17" id="Seg_46" n="sc" s="T15">
               <ts e="T17" id="Seg_48" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_50" n="HIAT:w" s="T15">Tümbaŋ</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_53" n="HIAT:w" s="T16">Ustʼ-Ozjorende</ts>
                  <nts id="Seg_54" n="HIAT:ip">.</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T20" id="Seg_56" n="sc" s="T18">
               <ts e="T20" id="Seg_58" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_60" n="HIAT:w" s="T18">Tende</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_63" n="HIAT:w" s="T19">ilɨkwaŋ</ts>
                  <nts id="Seg_64" n="HIAT:ip">.</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T26" id="Seg_66" n="sc" s="T21">
               <ts e="T26" id="Seg_68" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_70" n="HIAT:w" s="T21">Man</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_73" n="HIAT:w" s="T22">tüsaŋ</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_76" n="HIAT:w" s="T23">uzinde</ts>
                  <nts id="Seg_77" n="HIAT:ip">,</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_80" n="HIAT:w" s="T24">ilegu</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_83" n="HIAT:w" s="T25">zeran</ts>
                  <nts id="Seg_84" n="HIAT:ip">.</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T29" id="Seg_86" n="sc" s="T27">
               <ts e="T29" id="Seg_88" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_90" n="HIAT:w" s="T27">No</ts>
                  <nts id="Seg_91" n="HIAT:ip">,</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_94" n="HIAT:w" s="T28">ilkuzan</ts>
                  <nts id="Seg_95" n="HIAT:ip">.</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T33" id="Seg_97" n="sc" s="T30">
               <ts e="T33" id="Seg_99" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_101" n="HIAT:w" s="T30">Tend</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_104" n="HIAT:w" s="T31">ilond</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_107" n="HIAT:w" s="T32">medak</ts>
                  <nts id="Seg_108" n="HIAT:ip">.</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T37" id="Seg_110" n="sc" s="T34">
               <ts e="T37" id="Seg_112" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_114" n="HIAT:w" s="T34">Kɨndabɨɣɨn</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_117" n="HIAT:w" s="T35">medrabotnika</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_120" n="HIAT:w" s="T36">tʼaŋguŋ</ts>
                  <nts id="Seg_121" n="HIAT:ip">.</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T42" id="Seg_123" n="sc" s="T38">
               <ts e="T42" id="Seg_125" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_127" n="HIAT:w" s="T38">Miɣen</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_130" n="HIAT:w" s="T39">kadekwat</ts>
                  <nts id="Seg_131" n="HIAT:ip">:</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_134" n="HIAT:w" s="T40">tülʼden</ts>
                  <nts id="Seg_135" n="HIAT:ip">,</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_138" n="HIAT:w" s="T41">tülʼden</ts>
                  <nts id="Seg_139" n="HIAT:ip">.</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T48" id="Seg_141" n="sc" s="T43">
               <ts e="T48" id="Seg_143" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_145" n="HIAT:w" s="T43">Mat</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_148" n="HIAT:w" s="T44">Kɨndabɨgɨn</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_151" n="HIAT:w" s="T45">tüsaŋ</ts>
                  <nts id="Seg_152" n="HIAT:ip">,</nts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_155" n="HIAT:w" s="T46">Kɨndabɨn</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_158" n="HIAT:w" s="T47">tüsaŋ</ts>
                  <nts id="Seg_159" n="HIAT:ip">.</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T56" id="Seg_161" n="sc" s="T49">
               <ts e="T56" id="Seg_163" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_165" n="HIAT:w" s="T49">Tet</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_168" n="HIAT:w" s="T50">kwejgɨt</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_171" n="HIAT:w" s="T51">tetsarɨm</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_174" n="HIAT:w" s="T52">oqqer</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_177" n="HIAT:w" s="T53">tʼadiɣot</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_180" n="HIAT:w" s="T54">pogɨnde</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_183" n="HIAT:w" s="T55">kögɨt</ts>
                  <nts id="Seg_184" n="HIAT:ip">.</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T61" id="Seg_186" n="sc" s="T57">
               <ts e="T61" id="Seg_188" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_190" n="HIAT:w" s="T57">Kɨndabɨgɨn</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_193" n="HIAT:w" s="T58">onek</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_196" n="HIAT:w" s="T59">qula</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_199" n="HIAT:w" s="T60">tänguzat</ts>
                  <nts id="Seg_200" n="HIAT:ip">.</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T65" id="Seg_202" n="sc" s="T62">
               <ts e="T65" id="Seg_204" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_206" n="HIAT:w" s="T62">Tüsaŋ</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_209" n="HIAT:w" s="T63">arɨn</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_212" n="HIAT:w" s="T64">qulane</ts>
                  <nts id="Seg_213" n="HIAT:ip">.</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T69" id="Seg_215" n="sc" s="T66">
               <ts e="T69" id="Seg_217" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_219" n="HIAT:w" s="T66">Ilkuzan</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_222" n="HIAT:w" s="T67">arɨn</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_225" n="HIAT:w" s="T68">qunaqqɨn</ts>
                  <nts id="Seg_226" n="HIAT:ip">.</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T76" id="Seg_228" n="sc" s="T70">
               <ts e="T76" id="Seg_230" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_232" n="HIAT:w" s="T70">Amban</ts>
                  <nts id="Seg_233" n="HIAT:ip">,</nts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_236" n="HIAT:w" s="T71">essen</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_239" n="HIAT:w" s="T72">tären</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_242" n="HIAT:w" s="T73">madap</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_245" n="HIAT:w" s="T74">patʼalgu</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_248" n="HIAT:w" s="T75">kalɨmbat</ts>
                  <nts id="Seg_249" n="HIAT:ip">.</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T83" id="Seg_251" n="sc" s="T77">
               <ts e="T83" id="Seg_253" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_255" n="HIAT:w" s="T77">Ille</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_258" n="HIAT:w" s="T78">tan</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_261" n="HIAT:w" s="T79">tül</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_264" n="HIAT:w" s="T80">parond</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_267" n="HIAT:w" s="T81">pačänut</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_270" n="HIAT:w" s="T82">madom</ts>
                  <nts id="Seg_271" n="HIAT:ip">.</nts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T89" id="Seg_273" n="sc" s="T84">
               <ts e="T89" id="Seg_275" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_277" n="HIAT:w" s="T84">Tün</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_280" n="HIAT:w" s="T85">parɣen</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_283" n="HIAT:w" s="T86">tɨnd</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_286" n="HIAT:w" s="T87">qutnaj</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_289" n="HIAT:w" s="T88">katɨlot</ts>
                  <nts id="Seg_290" n="HIAT:ip">.</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T97" id="Seg_292" n="sc" s="T90">
               <ts e="T97" id="Seg_294" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_296" n="HIAT:w" s="T90">Meka</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_299" n="HIAT:w" s="T91">qulon</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_302" n="HIAT:w" s="T92">kuʒat-to</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_305" n="HIAT:w" s="T93">madom</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_308" n="HIAT:w" s="T94">pačalgu</ts>
                  <nts id="Seg_309" n="HIAT:ip">,</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_312" n="HIAT:w" s="T95">onǯ</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_315" n="HIAT:w" s="T96">pačannam</ts>
                  <nts id="Seg_316" n="HIAT:ip">.</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T105" id="Seg_318" n="sc" s="T98">
               <ts e="T105" id="Seg_320" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_322" n="HIAT:w" s="T98">Lɨbokoɣen</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_325" n="HIAT:w" s="T99">madla</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_328" n="HIAT:w" s="T100">onǯ</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_331" n="HIAT:w" s="T101">kuʒat-to</ts>
                  <nts id="Seg_332" n="HIAT:ip">,</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_335" n="HIAT:w" s="T102">madla</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_338" n="HIAT:w" s="T103">onǯut</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_341" n="HIAT:w" s="T104">kuʒat-to</ts>
                  <nts id="Seg_342" n="HIAT:ip">.</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T113" id="Seg_344" n="sc" s="T106">
               <ts e="T113" id="Seg_346" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_348" n="HIAT:w" s="T106">Kal</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_351" n="HIAT:w" s="T107">tende</ts>
                  <nts id="Seg_352" n="HIAT:ip">,</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_355" n="HIAT:w" s="T108">kal</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_358" n="HIAT:w" s="T109">tümbaŋ</ts>
                  <nts id="Seg_359" n="HIAT:ip">,</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_362" n="HIAT:w" s="T110">man</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_365" n="HIAT:w" s="T111">madom</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_368" n="HIAT:w" s="T112">meam</ts>
                  <nts id="Seg_369" n="HIAT:ip">.</nts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T118" id="Seg_371" n="sc" s="T114">
               <ts e="T118" id="Seg_373" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_375" n="HIAT:w" s="T114">Nɨnd</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_378" n="HIAT:w" s="T115">erukaŋk</ts>
                  <nts id="Seg_379" n="HIAT:ip">,</nts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_382" n="HIAT:w" s="T116">tona</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_385" n="HIAT:w" s="T117">posёlkaɣet</ts>
                  <nts id="Seg_386" n="HIAT:ip">.</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T121" id="Seg_388" n="sc" s="T119">
               <ts e="T121" id="Seg_390" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_392" n="HIAT:w" s="T119">Tüate</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_395" n="HIAT:w" s="T120">qula</ts>
                  <nts id="Seg_396" n="HIAT:ip">.</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T126" id="Seg_398" n="sc" s="T122">
               <ts e="T126" id="Seg_400" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_402" n="HIAT:w" s="T122">Naj</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_405" n="HIAT:w" s="T123">madoet</ts>
                  <nts id="Seg_406" n="HIAT:ip">,</nts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_409" n="HIAT:w" s="T124">madom</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_412" n="HIAT:w" s="T125">omdelǯembɨkwat</ts>
                  <nts id="Seg_413" n="HIAT:ip">.</nts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T131" id="Seg_415" n="sc" s="T127">
               <ts e="T131" id="Seg_417" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_419" n="HIAT:w" s="T127">I</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_422" n="HIAT:w" s="T128">wot</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_425" n="HIAT:w" s="T129">tend</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_428" n="HIAT:w" s="T130">ilaɣ</ts>
                  <nts id="Seg_429" n="HIAT:ip">.</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T6" id="Seg_431" n="sc" s="T1">
               <ts e="T2" id="Seg_433" n="e" s="T1">Meŋga </ts>
               <ts e="T3" id="Seg_435" n="e" s="T2">tessarɨm </ts>
               <ts e="T4" id="Seg_437" n="e" s="T3">šɨt </ts>
               <ts e="T5" id="Seg_439" n="e" s="T4">kwejgen </ts>
               <ts e="T6" id="Seg_441" n="e" s="T5">pot. </ts>
            </ts>
            <ts e="T14" id="Seg_442" n="sc" s="T7">
               <ts e="T8" id="Seg_444" n="e" s="T7">Orombaŋ, </ts>
               <ts e="T9" id="Seg_446" n="e" s="T8">oqqer </ts>
               <ts e="T10" id="Seg_448" n="e" s="T9">köt, </ts>
               <ts e="T11" id="Seg_450" n="e" s="T10">muktetsarum </ts>
               <ts e="T12" id="Seg_452" n="e" s="T11">muktet </ts>
               <ts e="T13" id="Seg_454" n="e" s="T12">kwelǯišat </ts>
               <ts e="T14" id="Seg_456" n="e" s="T13">kod-to. </ts>
            </ts>
            <ts e="T17" id="Seg_457" n="sc" s="T15">
               <ts e="T16" id="Seg_459" n="e" s="T15">Tümbaŋ </ts>
               <ts e="T17" id="Seg_461" n="e" s="T16">Ustʼ-Ozjorende. </ts>
            </ts>
            <ts e="T20" id="Seg_462" n="sc" s="T18">
               <ts e="T19" id="Seg_464" n="e" s="T18">Tende </ts>
               <ts e="T20" id="Seg_466" n="e" s="T19">ilɨkwaŋ. </ts>
            </ts>
            <ts e="T26" id="Seg_467" n="sc" s="T21">
               <ts e="T22" id="Seg_469" n="e" s="T21">Man </ts>
               <ts e="T23" id="Seg_471" n="e" s="T22">tüsaŋ </ts>
               <ts e="T24" id="Seg_473" n="e" s="T23">uzinde, </ts>
               <ts e="T25" id="Seg_475" n="e" s="T24">ilegu </ts>
               <ts e="T26" id="Seg_477" n="e" s="T25">zeran. </ts>
            </ts>
            <ts e="T29" id="Seg_478" n="sc" s="T27">
               <ts e="T28" id="Seg_480" n="e" s="T27">No, </ts>
               <ts e="T29" id="Seg_482" n="e" s="T28">ilkuzan. </ts>
            </ts>
            <ts e="T33" id="Seg_483" n="sc" s="T30">
               <ts e="T31" id="Seg_485" n="e" s="T30">Tend </ts>
               <ts e="T32" id="Seg_487" n="e" s="T31">ilond </ts>
               <ts e="T33" id="Seg_489" n="e" s="T32">medak. </ts>
            </ts>
            <ts e="T37" id="Seg_490" n="sc" s="T34">
               <ts e="T35" id="Seg_492" n="e" s="T34">Kɨndabɨɣɨn </ts>
               <ts e="T36" id="Seg_494" n="e" s="T35">medrabotnika </ts>
               <ts e="T37" id="Seg_496" n="e" s="T36">tʼaŋguŋ. </ts>
            </ts>
            <ts e="T42" id="Seg_497" n="sc" s="T38">
               <ts e="T39" id="Seg_499" n="e" s="T38">Miɣen </ts>
               <ts e="T40" id="Seg_501" n="e" s="T39">kadekwat: </ts>
               <ts e="T41" id="Seg_503" n="e" s="T40">tülʼden, </ts>
               <ts e="T42" id="Seg_505" n="e" s="T41">tülʼden. </ts>
            </ts>
            <ts e="T48" id="Seg_506" n="sc" s="T43">
               <ts e="T44" id="Seg_508" n="e" s="T43">Mat </ts>
               <ts e="T45" id="Seg_510" n="e" s="T44">Kɨndabɨgɨn </ts>
               <ts e="T46" id="Seg_512" n="e" s="T45">tüsaŋ, </ts>
               <ts e="T47" id="Seg_514" n="e" s="T46">Kɨndabɨn </ts>
               <ts e="T48" id="Seg_516" n="e" s="T47">tüsaŋ. </ts>
            </ts>
            <ts e="T56" id="Seg_517" n="sc" s="T49">
               <ts e="T50" id="Seg_519" n="e" s="T49">Tet </ts>
               <ts e="T51" id="Seg_521" n="e" s="T50">kwejgɨt </ts>
               <ts e="T52" id="Seg_523" n="e" s="T51">tetsarɨm </ts>
               <ts e="T53" id="Seg_525" n="e" s="T52">oqqer </ts>
               <ts e="T54" id="Seg_527" n="e" s="T53">tʼadiɣot </ts>
               <ts e="T55" id="Seg_529" n="e" s="T54">pogɨnde </ts>
               <ts e="T56" id="Seg_531" n="e" s="T55">kögɨt. </ts>
            </ts>
            <ts e="T61" id="Seg_532" n="sc" s="T57">
               <ts e="T58" id="Seg_534" n="e" s="T57">Kɨndabɨgɨn </ts>
               <ts e="T59" id="Seg_536" n="e" s="T58">onek </ts>
               <ts e="T60" id="Seg_538" n="e" s="T59">qula </ts>
               <ts e="T61" id="Seg_540" n="e" s="T60">tänguzat. </ts>
            </ts>
            <ts e="T65" id="Seg_541" n="sc" s="T62">
               <ts e="T63" id="Seg_543" n="e" s="T62">Tüsaŋ </ts>
               <ts e="T64" id="Seg_545" n="e" s="T63">arɨn </ts>
               <ts e="T65" id="Seg_547" n="e" s="T64">qulane. </ts>
            </ts>
            <ts e="T69" id="Seg_548" n="sc" s="T66">
               <ts e="T67" id="Seg_550" n="e" s="T66">Ilkuzan </ts>
               <ts e="T68" id="Seg_552" n="e" s="T67">arɨn </ts>
               <ts e="T69" id="Seg_554" n="e" s="T68">qunaqqɨn. </ts>
            </ts>
            <ts e="T76" id="Seg_555" n="sc" s="T70">
               <ts e="T71" id="Seg_557" n="e" s="T70">Amban, </ts>
               <ts e="T72" id="Seg_559" n="e" s="T71">essen </ts>
               <ts e="T73" id="Seg_561" n="e" s="T72">tären </ts>
               <ts e="T74" id="Seg_563" n="e" s="T73">madap </ts>
               <ts e="T75" id="Seg_565" n="e" s="T74">patʼalgu </ts>
               <ts e="T76" id="Seg_567" n="e" s="T75">kalɨmbat. </ts>
            </ts>
            <ts e="T83" id="Seg_568" n="sc" s="T77">
               <ts e="T78" id="Seg_570" n="e" s="T77">Ille </ts>
               <ts e="T79" id="Seg_572" n="e" s="T78">tan </ts>
               <ts e="T80" id="Seg_574" n="e" s="T79">tül </ts>
               <ts e="T81" id="Seg_576" n="e" s="T80">parond </ts>
               <ts e="T82" id="Seg_578" n="e" s="T81">pačänut </ts>
               <ts e="T83" id="Seg_580" n="e" s="T82">madom. </ts>
            </ts>
            <ts e="T89" id="Seg_581" n="sc" s="T84">
               <ts e="T85" id="Seg_583" n="e" s="T84">Tün </ts>
               <ts e="T86" id="Seg_585" n="e" s="T85">parɣen </ts>
               <ts e="T87" id="Seg_587" n="e" s="T86">tɨnd </ts>
               <ts e="T88" id="Seg_589" n="e" s="T87">qutnaj </ts>
               <ts e="T89" id="Seg_591" n="e" s="T88">katɨlot. </ts>
            </ts>
            <ts e="T97" id="Seg_592" n="sc" s="T90">
               <ts e="T91" id="Seg_594" n="e" s="T90">Meka </ts>
               <ts e="T92" id="Seg_596" n="e" s="T91">qulon </ts>
               <ts e="T93" id="Seg_598" n="e" s="T92">kuʒat-to </ts>
               <ts e="T94" id="Seg_600" n="e" s="T93">madom </ts>
               <ts e="T95" id="Seg_602" n="e" s="T94">pačalgu, </ts>
               <ts e="T96" id="Seg_604" n="e" s="T95">onǯ </ts>
               <ts e="T97" id="Seg_606" n="e" s="T96">pačannam. </ts>
            </ts>
            <ts e="T105" id="Seg_607" n="sc" s="T98">
               <ts e="T99" id="Seg_609" n="e" s="T98">Lɨbokoɣen </ts>
               <ts e="T100" id="Seg_611" n="e" s="T99">madla </ts>
               <ts e="T101" id="Seg_613" n="e" s="T100">onǯ </ts>
               <ts e="T102" id="Seg_615" n="e" s="T101">kuʒat-to, </ts>
               <ts e="T103" id="Seg_617" n="e" s="T102">madla </ts>
               <ts e="T104" id="Seg_619" n="e" s="T103">onǯut </ts>
               <ts e="T105" id="Seg_621" n="e" s="T104">kuʒat-to. </ts>
            </ts>
            <ts e="T113" id="Seg_622" n="sc" s="T106">
               <ts e="T107" id="Seg_624" n="e" s="T106">Kal </ts>
               <ts e="T108" id="Seg_626" n="e" s="T107">tende, </ts>
               <ts e="T109" id="Seg_628" n="e" s="T108">kal </ts>
               <ts e="T110" id="Seg_630" n="e" s="T109">tümbaŋ, </ts>
               <ts e="T111" id="Seg_632" n="e" s="T110">man </ts>
               <ts e="T112" id="Seg_634" n="e" s="T111">madom </ts>
               <ts e="T113" id="Seg_636" n="e" s="T112">meam. </ts>
            </ts>
            <ts e="T118" id="Seg_637" n="sc" s="T114">
               <ts e="T115" id="Seg_639" n="e" s="T114">Nɨnd </ts>
               <ts e="T116" id="Seg_641" n="e" s="T115">erukaŋk, </ts>
               <ts e="T117" id="Seg_643" n="e" s="T116">tona </ts>
               <ts e="T118" id="Seg_645" n="e" s="T117">posёlkaɣet. </ts>
            </ts>
            <ts e="T121" id="Seg_646" n="sc" s="T119">
               <ts e="T120" id="Seg_648" n="e" s="T119">Tüate </ts>
               <ts e="T121" id="Seg_650" n="e" s="T120">qula. </ts>
            </ts>
            <ts e="T126" id="Seg_651" n="sc" s="T122">
               <ts e="T123" id="Seg_653" n="e" s="T122">Naj </ts>
               <ts e="T124" id="Seg_655" n="e" s="T123">madoet, </ts>
               <ts e="T125" id="Seg_657" n="e" s="T124">madom </ts>
               <ts e="T126" id="Seg_659" n="e" s="T125">omdelǯembɨkwat. </ts>
            </ts>
            <ts e="T131" id="Seg_660" n="sc" s="T127">
               <ts e="T128" id="Seg_662" n="e" s="T127">I </ts>
               <ts e="T129" id="Seg_664" n="e" s="T128">wot </ts>
               <ts e="T130" id="Seg_666" n="e" s="T129">tend </ts>
               <ts e="T131" id="Seg_668" n="e" s="T130">ilaɣ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_669" s="T1">KMS_196X_Lifestory_nar.001 (001)</ta>
            <ta e="T14" id="Seg_670" s="T7">KMS_196X_Lifestory_nar.002 (002)</ta>
            <ta e="T17" id="Seg_671" s="T15">KMS_196X_Lifestory_nar.003 (003)</ta>
            <ta e="T20" id="Seg_672" s="T18">KMS_196X_Lifestory_nar.004 (004)</ta>
            <ta e="T26" id="Seg_673" s="T21">KMS_196X_Lifestory_nar.005 (005)</ta>
            <ta e="T29" id="Seg_674" s="T27">KMS_196X_Lifestory_nar.006 (006)</ta>
            <ta e="T33" id="Seg_675" s="T30">KMS_196X_Lifestory_nar.007 (007)</ta>
            <ta e="T37" id="Seg_676" s="T34">KMS_196X_Lifestory_nar.008 (008.001)</ta>
            <ta e="T42" id="Seg_677" s="T38">KMS_196X_Lifestory_nar.009 (009)</ta>
            <ta e="T48" id="Seg_678" s="T43">KMS_196X_Lifestory_nar.010 (010)</ta>
            <ta e="T56" id="Seg_679" s="T49">KMS_196X_Lifestory_nar.011 (011)</ta>
            <ta e="T61" id="Seg_680" s="T57">KMS_196X_Lifestory_nar.012 (012)</ta>
            <ta e="T65" id="Seg_681" s="T62">KMS_196X_Lifestory_nar.013 (013)</ta>
            <ta e="T69" id="Seg_682" s="T66">KMS_196X_Lifestory_nar.014 (014)</ta>
            <ta e="T76" id="Seg_683" s="T70">KMS_196X_Lifestory_nar.015 (015)</ta>
            <ta e="T83" id="Seg_684" s="T77">KMS_196X_Lifestory_nar.016 (016)</ta>
            <ta e="T89" id="Seg_685" s="T84">KMS_196X_Lifestory_nar.017 (017)</ta>
            <ta e="T97" id="Seg_686" s="T90">KMS_196X_Lifestory_nar.018 (018)</ta>
            <ta e="T105" id="Seg_687" s="T98">KMS_196X_Lifestory_nar.019 (019)</ta>
            <ta e="T113" id="Seg_688" s="T106">KMS_196X_Lifestory_nar.020 (020)</ta>
            <ta e="T118" id="Seg_689" s="T114">KMS_196X_Lifestory_nar.021 (021)</ta>
            <ta e="T121" id="Seg_690" s="T119">KMS_196X_Lifestory_nar.022 (022)</ta>
            <ta e="T126" id="Seg_691" s="T122">KMS_196X_Lifestory_nar.023 (023)</ta>
            <ta e="T131" id="Seg_692" s="T127">KMS_196X_Lifestory_nar.024 (024)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_693" s="T1">Ныңга тэссарым шыт квэйгэн пот.</ta>
            <ta e="T14" id="Seg_694" s="T7">Оромбаң, оӄӄэр кӧт, муктэтсарум муктэт квэлджишат код-то.</ta>
            <ta e="T17" id="Seg_695" s="T15">Тӱмбаң Усть-Озёрэндэ.</ta>
            <ta e="T20" id="Seg_696" s="T18">Тэндэ илыкваң.</ta>
            <ta e="T26" id="Seg_697" s="T21">Ман тӱсаң узиндэ, илэгу зэран.</ta>
            <ta e="T29" id="Seg_698" s="T27">Но, илкузан.</ta>
            <ta e="T33" id="Seg_699" s="T30">Тэнд илонд медак.</ta>
            <ta e="T37" id="Seg_700" s="T34">Кындабыӷын мед.работника тяңгуң.</ta>
            <ta e="T42" id="Seg_701" s="T38">Миӷэн кадэкват: тӱльдэн, тӱльдэн.</ta>
            <ta e="T48" id="Seg_702" s="T43">Мат Кындабыгын тӱсаң, Кындабын тӱсаң.</ta>
            <ta e="T56" id="Seg_703" s="T49">Тэт квэйгыт тэтсарым оӄӄэр тядиӷот погындэ кӧгыт.</ta>
            <ta e="T61" id="Seg_704" s="T57">Кындабыгын онэк ӄула тӓнгузат.</ta>
            <ta e="T65" id="Seg_705" s="T62">Тӱсаң арын ӄуланэ.</ta>
            <ta e="T69" id="Seg_706" s="T66">Илкузан арын ӄунаӄӄын.</ta>
            <ta e="T76" id="Seg_707" s="T70">А́мбан, эссэ́н тӓрэн ма́дап патя́лгу калымбат.</ta>
            <ta e="T83" id="Seg_708" s="T77">Илле тан тӱл паронд пачӓнут мадом.</ta>
            <ta e="T89" id="Seg_709" s="T84">Тӱн парӷэн тынд ӄутнай катылот.</ta>
            <ta e="T97" id="Seg_710" s="T90">Мека ӄулон кужат-то мадом пачалгу, ондж пачаннам.</ta>
            <ta e="T105" id="Seg_711" s="T98">Лыбокоӷэн мадла ондж кужат-то, мадла онджут кужат-то.</ta>
            <ta e="T113" id="Seg_712" s="T106">Кал тэндэ, кал тӱмбаң, ман мадом меам.</ta>
            <ta e="T118" id="Seg_713" s="T114">Нынд эрукаңк, тона посёлкаӷэт.</ta>
            <ta e="T121" id="Seg_714" s="T119">Тӱатэ ӄула.</ta>
            <ta e="T126" id="Seg_715" s="T122">Най мадоэт, мадом омдэлджэмбыкват.</ta>
            <ta e="T131" id="Seg_716" s="T127">И вот тэнд илаӷ.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_717" s="T1">Nɨŋga tessarɨm šɨt kvejgen pot.</ta>
            <ta e="T14" id="Seg_718" s="T7">Orombaŋ, oqqer köt, muktetsarum muktet kvelǯišat kod-to.</ta>
            <ta e="T17" id="Seg_719" s="T15">Tümbaŋ Ustʼ-Ozjorende.</ta>
            <ta e="T20" id="Seg_720" s="T18">Tende ilɨkvaŋ.</ta>
            <ta e="T26" id="Seg_721" s="T21">Man tüsaŋ uzinde, ilegu zeran.</ta>
            <ta e="T29" id="Seg_722" s="T27">No, ilkuzan.</ta>
            <ta e="T33" id="Seg_723" s="T30">Tend ilond medak.</ta>
            <ta e="T37" id="Seg_724" s="T34">Kɨndabɨɣɨn med.rabotnika tʼaŋguŋ.</ta>
            <ta e="T42" id="Seg_725" s="T38">Miɣen kadekvat: tülʼden, tülʼden.</ta>
            <ta e="T48" id="Seg_726" s="T43">Mat Kɨndabɨgɨn tüsaŋ, Kɨndabɨn tüsaŋ.</ta>
            <ta e="T56" id="Seg_727" s="T49">Tet kvejgɨt tetsarɨm oqqer tʼadiɣot pogɨnde kögɨt.</ta>
            <ta e="T61" id="Seg_728" s="T57">Kɨndabɨgɨn onek qula tänguzat.</ta>
            <ta e="T65" id="Seg_729" s="T62">Tüsaŋ arɨn qulane.</ta>
            <ta e="T69" id="Seg_730" s="T66">Ilkuzan arɨn qunaqqɨn.</ta>
            <ta e="T76" id="Seg_731" s="T70">Amban, essen tären madap patʼalgu kalɨmbat.</ta>
            <ta e="T83" id="Seg_732" s="T77">Ille tan tül parond pačʼänut madom.</ta>
            <ta e="T89" id="Seg_733" s="T84">Tün parɣen tɨnd qutnaj katɨlot.</ta>
            <ta e="T97" id="Seg_734" s="T90">Meka qulon kuʒat-to madom pačʼalgu, onǯ pačʼannam.</ta>
            <ta e="T105" id="Seg_735" s="T98">Lɨbokoɣen madla onǯ kuʒat-to, madla onǯut kuʒat-to.</ta>
            <ta e="T113" id="Seg_736" s="T106">Kal tende, kal tümbaŋ, man madom meam.</ta>
            <ta e="T118" id="Seg_737" s="T114">Nɨnd erukaŋk, tona posёlkaɣet.</ta>
            <ta e="T121" id="Seg_738" s="T119">Tüate qula.</ta>
            <ta e="T126" id="Seg_739" s="T122">Naj madoet, madom omdelǯembɨkvat.</ta>
            <ta e="T131" id="Seg_740" s="T127">I vot tend ilaɣ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_741" s="T1">Meŋga tessarɨm šɨt kwejgen pot. </ta>
            <ta e="T14" id="Seg_742" s="T7">Orombaŋ, oqqer köt, muktetsarum muktet kwelǯišat kod-to. </ta>
            <ta e="T17" id="Seg_743" s="T15">Tümbaŋ Ustʼ_Ozjorende. </ta>
            <ta e="T20" id="Seg_744" s="T18">Tende ilɨkwaŋ. </ta>
            <ta e="T26" id="Seg_745" s="T21">Man tüsaŋ uzinde, ilegu zeran. </ta>
            <ta e="T29" id="Seg_746" s="T27">No, ilkuzan. </ta>
            <ta e="T33" id="Seg_747" s="T30">Tend ilond medak. </ta>
            <ta e="T37" id="Seg_748" s="T34">Kɨndabɨɣɨn medrabotnika tʼaŋguŋ. </ta>
            <ta e="T42" id="Seg_749" s="T38">Miɣen kadekwat: tülʼden, tülʼden. </ta>
            <ta e="T48" id="Seg_750" s="T43">Mat Kɨndabɨgɨn tüsaŋ, Kɨndabɨn tüsaŋ. </ta>
            <ta e="T56" id="Seg_751" s="T49">Tet kwejgɨt tetsarɨm oqqer tʼadiɣot pogɨnde kögɨt. </ta>
            <ta e="T61" id="Seg_752" s="T57">Kɨndabɨgɨn onek qula tänguzat. </ta>
            <ta e="T65" id="Seg_753" s="T62">Tüsaŋ arɨn qulane. </ta>
            <ta e="T69" id="Seg_754" s="T66">Ilkuzan arɨn qunaqqɨn. </ta>
            <ta e="T76" id="Seg_755" s="T70">Amban, essen tären madap patʼalgu kalɨmbat. </ta>
            <ta e="T83" id="Seg_756" s="T77">Ille tan tül parond pačänut madom. </ta>
            <ta e="T89" id="Seg_757" s="T84">Tün parɣen tɨnd qutnaj katɨlot. </ta>
            <ta e="T97" id="Seg_758" s="T90">Meka qulon kuʒat-to madom pačalgu, onǯ pačannam. </ta>
            <ta e="T105" id="Seg_759" s="T98">Lɨbokoɣen madla onǯ kuʒat-to, madla onǯut kuʒat-to. </ta>
            <ta e="T113" id="Seg_760" s="T106">Kal tende, kal tümbaŋ, man madom meam. </ta>
            <ta e="T118" id="Seg_761" s="T114">Nɨnd erukaŋk, tona posёlkaɣet. </ta>
            <ta e="T121" id="Seg_762" s="T119">Tüate qula. </ta>
            <ta e="T126" id="Seg_763" s="T122">Naj madoet, madom omdelǯembɨkwat. </ta>
            <ta e="T131" id="Seg_764" s="T127">I wot tend ilaɣ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_765" s="T1">meŋga</ta>
            <ta e="T3" id="Seg_766" s="T2">tessarɨm</ta>
            <ta e="T4" id="Seg_767" s="T3">šɨt</ta>
            <ta e="T5" id="Seg_768" s="T4">kwej-gen</ta>
            <ta e="T6" id="Seg_769" s="T5">po-tɨ</ta>
            <ta e="T8" id="Seg_770" s="T7">orom-ba-ŋ</ta>
            <ta e="T9" id="Seg_771" s="T8">oqqer</ta>
            <ta e="T10" id="Seg_772" s="T9">köt</ta>
            <ta e="T11" id="Seg_773" s="T10">muktet-sarum</ta>
            <ta e="T12" id="Seg_774" s="T11">muktet</ta>
            <ta e="T13" id="Seg_775" s="T12">kwel-ǯi-ša-t</ta>
            <ta e="T14" id="Seg_776" s="T13">kod-to</ta>
            <ta e="T16" id="Seg_777" s="T15">tü-mba-ŋ</ta>
            <ta e="T17" id="Seg_778" s="T16">Ustʼ_Ozjore-nde</ta>
            <ta e="T19" id="Seg_779" s="T18">tende</ta>
            <ta e="T20" id="Seg_780" s="T19">ilɨ-k-wa-ŋ</ta>
            <ta e="T22" id="Seg_781" s="T21">man</ta>
            <ta e="T23" id="Seg_782" s="T22">tü-sa-ŋ</ta>
            <ta e="T24" id="Seg_783" s="T23">uzi-nde</ta>
            <ta e="T25" id="Seg_784" s="T24">ile-gu</ta>
            <ta e="T26" id="Seg_785" s="T25">zeran</ta>
            <ta e="T28" id="Seg_786" s="T27">no</ta>
            <ta e="T29" id="Seg_787" s="T28">il-ku-za-n</ta>
            <ta e="T31" id="Seg_788" s="T30">tend</ta>
            <ta e="T32" id="Seg_789" s="T31">ilo-nd</ta>
            <ta e="T33" id="Seg_790" s="T32">meda-k</ta>
            <ta e="T35" id="Seg_791" s="T34">kɨndabɨ-ɣɨn</ta>
            <ta e="T36" id="Seg_792" s="T35">medrabotnika</ta>
            <ta e="T37" id="Seg_793" s="T36">tʼaŋgu-ŋ</ta>
            <ta e="T39" id="Seg_794" s="T38">miɣen</ta>
            <ta e="T40" id="Seg_795" s="T39">kad-e-k-wa-t</ta>
            <ta e="T41" id="Seg_796" s="T40">tü-lʼ-den</ta>
            <ta e="T42" id="Seg_797" s="T41">tü-lʼ-den</ta>
            <ta e="T44" id="Seg_798" s="T43">mat</ta>
            <ta e="T45" id="Seg_799" s="T44">Kɨndabɨ-gɨn</ta>
            <ta e="T46" id="Seg_800" s="T45">tü-sa-ŋ</ta>
            <ta e="T47" id="Seg_801" s="T46">Kɨndabɨ-n</ta>
            <ta e="T48" id="Seg_802" s="T47">tü-sa-ŋ</ta>
            <ta e="T50" id="Seg_803" s="T49">tetɨ</ta>
            <ta e="T51" id="Seg_804" s="T50">kwej-gɨt</ta>
            <ta e="T52" id="Seg_805" s="T51">tetsarɨm</ta>
            <ta e="T53" id="Seg_806" s="T52">oqqer</ta>
            <ta e="T54" id="Seg_807" s="T53">tʼadiɣot</ta>
            <ta e="T55" id="Seg_808" s="T54">po-gɨnde</ta>
            <ta e="T56" id="Seg_809" s="T55">kö-gɨt</ta>
            <ta e="T58" id="Seg_810" s="T57">Kɨndabɨ-gɨn</ta>
            <ta e="T59" id="Seg_811" s="T58">onek</ta>
            <ta e="T60" id="Seg_812" s="T59">qu-la</ta>
            <ta e="T61" id="Seg_813" s="T60">tängu-za-t</ta>
            <ta e="T63" id="Seg_814" s="T62">tü-sa-ŋ</ta>
            <ta e="T64" id="Seg_815" s="T63">arɨn</ta>
            <ta e="T65" id="Seg_816" s="T64">qu-la-ne</ta>
            <ta e="T67" id="Seg_817" s="T66">il-ku-za-n</ta>
            <ta e="T68" id="Seg_818" s="T67">arɨn</ta>
            <ta e="T69" id="Seg_819" s="T68">qu-naq-qɨn</ta>
            <ta e="T71" id="Seg_820" s="T70">amba-n</ta>
            <ta e="T72" id="Seg_821" s="T71">esse-n</ta>
            <ta e="T73" id="Seg_822" s="T72">tären</ta>
            <ta e="T74" id="Seg_823" s="T73">mad-a-p</ta>
            <ta e="T75" id="Seg_824" s="T74">patʼal-gu</ta>
            <ta e="T76" id="Seg_825" s="T75">kalɨ-mba-t</ta>
            <ta e="T78" id="Seg_826" s="T77">ille</ta>
            <ta e="T79" id="Seg_827" s="T78">tat</ta>
            <ta e="T80" id="Seg_828" s="T79">tü-l</ta>
            <ta e="T81" id="Seg_829" s="T80">par-nd</ta>
            <ta e="T82" id="Seg_830" s="T81">pačä-nu-t</ta>
            <ta e="T83" id="Seg_831" s="T82">mad-o-m</ta>
            <ta e="T85" id="Seg_832" s="T84">tü-n</ta>
            <ta e="T86" id="Seg_833" s="T85">par-ɣen</ta>
            <ta e="T87" id="Seg_834" s="T86">tɨnd</ta>
            <ta e="T88" id="Seg_835" s="T87">qut-naj</ta>
            <ta e="T89" id="Seg_836" s="T88">katɨl-o-t</ta>
            <ta e="T91" id="Seg_837" s="T90">meka</ta>
            <ta e="T92" id="Seg_838" s="T91">qu-lo-n</ta>
            <ta e="T93" id="Seg_839" s="T92">kuʒat-to</ta>
            <ta e="T94" id="Seg_840" s="T93">mad-o-m</ta>
            <ta e="T95" id="Seg_841" s="T94">pačal-gu</ta>
            <ta e="T96" id="Seg_842" s="T95">onǯ</ta>
            <ta e="T97" id="Seg_843" s="T96">pačan-na-m</ta>
            <ta e="T99" id="Seg_844" s="T98">lɨboko-ɣen</ta>
            <ta e="T100" id="Seg_845" s="T99">mad-la</ta>
            <ta e="T101" id="Seg_846" s="T100">onǯ</ta>
            <ta e="T102" id="Seg_847" s="T101">kuʒat-to</ta>
            <ta e="T103" id="Seg_848" s="T102">mad-la</ta>
            <ta e="T104" id="Seg_849" s="T103">onǯ-ut</ta>
            <ta e="T105" id="Seg_850" s="T104">kuʒat-to</ta>
            <ta e="T107" id="Seg_851" s="T106">ka-l</ta>
            <ta e="T108" id="Seg_852" s="T107">tende</ta>
            <ta e="T109" id="Seg_853" s="T108">ka-l</ta>
            <ta e="T110" id="Seg_854" s="T109">tü-mba-ŋ</ta>
            <ta e="T111" id="Seg_855" s="T110">man</ta>
            <ta e="T112" id="Seg_856" s="T111">mad-o-m</ta>
            <ta e="T113" id="Seg_857" s="T112">me-a-m</ta>
            <ta e="T115" id="Seg_858" s="T114">nɨnd</ta>
            <ta e="T116" id="Seg_859" s="T115">er-u-ka-ŋk</ta>
            <ta e="T117" id="Seg_860" s="T116">tona</ta>
            <ta e="T118" id="Seg_861" s="T117">posёlka-ɣet</ta>
            <ta e="T120" id="Seg_862" s="T119">tü-a-te</ta>
            <ta e="T121" id="Seg_863" s="T120">qu-la</ta>
            <ta e="T123" id="Seg_864" s="T122">naj</ta>
            <ta e="T124" id="Seg_865" s="T123">mad-o-et</ta>
            <ta e="T125" id="Seg_866" s="T124">mad-o-m</ta>
            <ta e="T126" id="Seg_867" s="T125">omde-lǯe-mbɨ-k-wa-t</ta>
            <ta e="T128" id="Seg_868" s="T127">i</ta>
            <ta e="T129" id="Seg_869" s="T128">wot</ta>
            <ta e="T130" id="Seg_870" s="T129">tend</ta>
            <ta e="T131" id="Seg_871" s="T130">ila-ɣ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_872" s="T1">mäkkä</ta>
            <ta e="T3" id="Seg_873" s="T2">tessarɨm</ta>
            <ta e="T4" id="Seg_874" s="T3">šittə</ta>
            <ta e="T5" id="Seg_875" s="T4">qwäj-köt</ta>
            <ta e="T6" id="Seg_876" s="T5">po-tɨ</ta>
            <ta e="T8" id="Seg_877" s="T7">orɨm-mbɨ-ŋ</ta>
            <ta e="T9" id="Seg_878" s="T8">okkɨr</ta>
            <ta e="T10" id="Seg_879" s="T9">köːt</ta>
            <ta e="T11" id="Seg_880" s="T10">muktut-saːrum</ta>
            <ta e="T12" id="Seg_881" s="T11">muktut</ta>
            <ta e="T13" id="Seg_882" s="T12">qwǝlɨ-ǯə-sɨ-tɨ</ta>
            <ta e="T14" id="Seg_883" s="T13">kutti-to</ta>
            <ta e="T16" id="Seg_884" s="T15">tüː-mbɨ-ŋ</ta>
            <ta e="T17" id="Seg_885" s="T16">Ustʼ_Ozjore-ndɨ</ta>
            <ta e="T19" id="Seg_886" s="T18">təndə</ta>
            <ta e="T20" id="Seg_887" s="T19">illɨ-ku-ŋɨ-ŋ</ta>
            <ta e="T22" id="Seg_888" s="T21">man</ta>
            <ta e="T23" id="Seg_889" s="T22">tüː-sɨ-n</ta>
            <ta e="T24" id="Seg_890" s="T23">uːdi-ndɨ</ta>
            <ta e="T25" id="Seg_891" s="T24">illɨ-gu</ta>
            <ta e="T26" id="Seg_892" s="T25">zeran</ta>
            <ta e="T28" id="Seg_893" s="T27">nu</ta>
            <ta e="T29" id="Seg_894" s="T28">illɨ-ku-sɨ-mɨ</ta>
            <ta e="T31" id="Seg_895" s="T30">təndə</ta>
            <ta e="T32" id="Seg_896" s="T31">illɨ-ndɨ</ta>
            <ta e="T33" id="Seg_897" s="T32">mittɨ-ŋ</ta>
            <ta e="T35" id="Seg_898" s="T34">Kɨndabu-qən</ta>
            <ta e="T36" id="Seg_899" s="T35">medrabotnika</ta>
            <ta e="T37" id="Seg_900" s="T36">tʼäŋu-n</ta>
            <ta e="T39" id="Seg_901" s="T38">miɣnut</ta>
            <ta e="T40" id="Seg_902" s="T39">kät-ɨ-ku-ŋɨ-tɨt</ta>
            <ta e="T41" id="Seg_903" s="T40">tüː-lä-tɨt</ta>
            <ta e="T42" id="Seg_904" s="T41">tüː-lä-tɨt</ta>
            <ta e="T44" id="Seg_905" s="T43">man</ta>
            <ta e="T45" id="Seg_906" s="T44">Kɨndabu-qən</ta>
            <ta e="T46" id="Seg_907" s="T45">tüː-sɨ-ŋ</ta>
            <ta e="T47" id="Seg_908" s="T46">Kɨndabu-nɨ</ta>
            <ta e="T48" id="Seg_909" s="T47">tüː-sɨ-ŋ</ta>
            <ta e="T50" id="Seg_910" s="T49">tättɨ</ta>
            <ta e="T51" id="Seg_911" s="T50">qwäj-köt</ta>
            <ta e="T52" id="Seg_912" s="T51">tessarɨm</ta>
            <ta e="T53" id="Seg_913" s="T52">okkɨr</ta>
            <ta e="T54" id="Seg_914" s="T53">tʼädʼigʼöt</ta>
            <ta e="T55" id="Seg_915" s="T54">po-qonde</ta>
            <ta e="T56" id="Seg_916" s="T55">kö-qən</ta>
            <ta e="T58" id="Seg_917" s="T57">Kɨndabu-qən</ta>
            <ta e="T59" id="Seg_918" s="T58">onäk</ta>
            <ta e="T60" id="Seg_919" s="T59">qum-la</ta>
            <ta e="T61" id="Seg_920" s="T60">tʼäŋu-sɨ-tɨt</ta>
            <ta e="T63" id="Seg_921" s="T62">tüː-sɨ-ŋ</ta>
            <ta e="T64" id="Seg_922" s="T63">aːrɨŋ</ta>
            <ta e="T65" id="Seg_923" s="T64">qum-la-nɨ</ta>
            <ta e="T67" id="Seg_924" s="T66">illɨ-ku-sɨ-mɨ</ta>
            <ta e="T68" id="Seg_925" s="T67">aːrɨŋ</ta>
            <ta e="T69" id="Seg_926" s="T68">qum-naq-qən</ta>
            <ta e="T71" id="Seg_927" s="T70">äwa-naj</ta>
            <ta e="T72" id="Seg_928" s="T71">ässɨ-naj</ta>
            <ta e="T73" id="Seg_929" s="T72">tären</ta>
            <ta e="T74" id="Seg_930" s="T73">maːt-ɨ-m</ta>
            <ta e="T75" id="Seg_931" s="T74">patʼäl-gu</ta>
            <ta e="T76" id="Seg_932" s="T75">qalɨ-mbɨ-tɨt</ta>
            <ta e="T78" id="Seg_933" s="T77">illä</ta>
            <ta e="T79" id="Seg_934" s="T78">tan</ta>
            <ta e="T80" id="Seg_935" s="T79">tʼü-lʼ</ta>
            <ta e="T81" id="Seg_936" s="T80">par-ndɨ</ta>
            <ta e="T82" id="Seg_937" s="T81">pače-ne-tɨt</ta>
            <ta e="T83" id="Seg_938" s="T82">maːt-ɨ-m</ta>
            <ta e="T85" id="Seg_939" s="T84">tʼü-n</ta>
            <ta e="T86" id="Seg_940" s="T85">par-qən</ta>
            <ta e="T87" id="Seg_941" s="T86">təndə</ta>
            <ta e="T88" id="Seg_942" s="T87">kutti-naj</ta>
            <ta e="T89" id="Seg_943" s="T88">katɨl-ɨ-tɨt</ta>
            <ta e="T91" id="Seg_944" s="T90">mäkkä</ta>
            <ta e="T92" id="Seg_945" s="T91">qum-la-naj</ta>
            <ta e="T93" id="Seg_946" s="T92">kuːča-to</ta>
            <ta e="T94" id="Seg_947" s="T93">maːt-ɨ-m</ta>
            <ta e="T95" id="Seg_948" s="T94">patʼäl-gu</ta>
            <ta e="T96" id="Seg_949" s="T95">ontɨ</ta>
            <ta e="T97" id="Seg_950" s="T96">patʼäl-ŋɨ-m</ta>
            <ta e="T99" id="Seg_951" s="T98">lɨboko-qən</ta>
            <ta e="T100" id="Seg_952" s="T99">maːt-la</ta>
            <ta e="T101" id="Seg_953" s="T100">ontɨ</ta>
            <ta e="T102" id="Seg_954" s="T101">kuːča-to</ta>
            <ta e="T103" id="Seg_955" s="T102">maːt-la</ta>
            <ta e="T104" id="Seg_956" s="T103">ontɨ-wɨt</ta>
            <ta e="T105" id="Seg_957" s="T104">kuːča-to</ta>
            <ta e="T107" id="Seg_958" s="T106">ka-lʼ</ta>
            <ta e="T108" id="Seg_959" s="T107">təndə</ta>
            <ta e="T109" id="Seg_960" s="T108">ka-lʼ</ta>
            <ta e="T110" id="Seg_961" s="T109">tüː-mbɨ-ŋ</ta>
            <ta e="T111" id="Seg_962" s="T110">man</ta>
            <ta e="T112" id="Seg_963" s="T111">maːt-ɨ-m</ta>
            <ta e="T113" id="Seg_964" s="T112">meː-ɨ-m</ta>
            <ta e="T115" id="Seg_965" s="T114">nɨmtɨ</ta>
            <ta e="T116" id="Seg_966" s="T115">er-ɨ-ku-ŋk</ta>
            <ta e="T117" id="Seg_967" s="T116">tona</ta>
            <ta e="T118" id="Seg_968" s="T117">posёlka-qən</ta>
            <ta e="T120" id="Seg_969" s="T119">tüː-ɨ-tɨt</ta>
            <ta e="T121" id="Seg_970" s="T120">qum-la</ta>
            <ta e="T123" id="Seg_971" s="T122">naj</ta>
            <ta e="T124" id="Seg_972" s="T123">maːt-ɨ-et</ta>
            <ta e="T125" id="Seg_973" s="T124">maːt-ɨ-m</ta>
            <ta e="T126" id="Seg_974" s="T125">omdɨ-lǯi-mbɨ-ku-ŋɨ-tɨt</ta>
            <ta e="T128" id="Seg_975" s="T127">i</ta>
            <ta e="T129" id="Seg_976" s="T128">wot</ta>
            <ta e="T130" id="Seg_977" s="T129">təndə</ta>
            <ta e="T131" id="Seg_978" s="T130">illɨ-qi</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_979" s="T1">I.ALL</ta>
            <ta e="T3" id="Seg_980" s="T2">forty</ta>
            <ta e="T4" id="Seg_981" s="T3">two</ta>
            <ta e="T5" id="Seg_982" s="T4">superfluous-ten</ta>
            <ta e="T6" id="Seg_983" s="T5">year.[NOM]-3SG</ta>
            <ta e="T8" id="Seg_984" s="T7">grow.up-PST.NAR-1SG.S</ta>
            <ta e="T9" id="Seg_985" s="T8">one</ta>
            <ta e="T10" id="Seg_986" s="T9">ten</ta>
            <ta e="T11" id="Seg_987" s="T10">six-ten</ta>
            <ta e="T12" id="Seg_988" s="T11">six</ta>
            <ta e="T13" id="Seg_989" s="T12">fish-VBLZ-PST-3SG.O</ta>
            <ta e="T14" id="Seg_990" s="T13">who-INDEF</ta>
            <ta e="T16" id="Seg_991" s="T15">come-PST.NAR-1SG.S</ta>
            <ta e="T17" id="Seg_992" s="T16">Ust_Ozyornoe-ILL</ta>
            <ta e="T19" id="Seg_993" s="T18">here</ta>
            <ta e="T20" id="Seg_994" s="T19">live-HAB-CO-1SG.S</ta>
            <ta e="T22" id="Seg_995" s="T21">I.NOM</ta>
            <ta e="T23" id="Seg_996" s="T22">come-PST-3SG.S</ta>
            <ta e="T24" id="Seg_997" s="T23">work-ILL</ta>
            <ta e="T25" id="Seg_998" s="T24">live-INF</ta>
            <ta e="T26" id="Seg_999" s="T25">%%</ta>
            <ta e="T28" id="Seg_1000" s="T27">now</ta>
            <ta e="T29" id="Seg_1001" s="T28">live-HAB-PST-1SG</ta>
            <ta e="T31" id="Seg_1002" s="T30">here</ta>
            <ta e="T32" id="Seg_1003" s="T31">life-ILL</ta>
            <ta e="T33" id="Seg_1004" s="T32">reach-1SG.S</ta>
            <ta e="T35" id="Seg_1005" s="T34">Ust_Ozyornoe-LOC</ta>
            <ta e="T36" id="Seg_1006" s="T35">medicine.worker.GEN</ta>
            <ta e="T37" id="Seg_1007" s="T36">NEG.EX-3SG.S</ta>
            <ta e="T39" id="Seg_1008" s="T38">we.ALL</ta>
            <ta e="T40" id="Seg_1009" s="T39">say-EP-HAB-CO-3PL</ta>
            <ta e="T41" id="Seg_1010" s="T40">come-OPT-3PL</ta>
            <ta e="T42" id="Seg_1011" s="T41">come-OPT-3PL</ta>
            <ta e="T44" id="Seg_1012" s="T43">I.NOM</ta>
            <ta e="T45" id="Seg_1013" s="T44">Ust_Ozyornoe-LOC</ta>
            <ta e="T46" id="Seg_1014" s="T45">come-PST-1SG.S</ta>
            <ta e="T47" id="Seg_1015" s="T46">Ust_Ozyornoe-ALL</ta>
            <ta e="T48" id="Seg_1016" s="T47">come-PST-1SG.S</ta>
            <ta e="T50" id="Seg_1017" s="T49">four</ta>
            <ta e="T51" id="Seg_1018" s="T50">superfluous-ten</ta>
            <ta e="T52" id="Seg_1019" s="T51">forty</ta>
            <ta e="T53" id="Seg_1020" s="T52">one</ta>
            <ta e="T54" id="Seg_1021" s="T53">nine</ta>
            <ta e="T55" id="Seg_1022" s="T54">year-ILL.3SG</ta>
            <ta e="T56" id="Seg_1023" s="T55">side-LOC</ta>
            <ta e="T58" id="Seg_1024" s="T57">Ust_Ozyornoe-LOC</ta>
            <ta e="T59" id="Seg_1025" s="T58">oneself.1SG</ta>
            <ta e="T60" id="Seg_1026" s="T59">human.being-PL.[NOM]</ta>
            <ta e="T61" id="Seg_1027" s="T60">NEG.EX-PST-3PL</ta>
            <ta e="T63" id="Seg_1028" s="T62">come-PST-1SG.S</ta>
            <ta e="T64" id="Seg_1029" s="T63">foreign</ta>
            <ta e="T65" id="Seg_1030" s="T64">human.being-PL-ALL</ta>
            <ta e="T67" id="Seg_1031" s="T66">live-HAB-PST-1SG</ta>
            <ta e="T68" id="Seg_1032" s="T67">foreign</ta>
            <ta e="T69" id="Seg_1033" s="T68">human.being-%%-LOC</ta>
            <ta e="T71" id="Seg_1034" s="T70">mother-EMPH</ta>
            <ta e="T72" id="Seg_1035" s="T71">father-EMPH</ta>
            <ta e="T73" id="Seg_1036" s="T72">%%</ta>
            <ta e="T74" id="Seg_1037" s="T73">house-EP-ACC</ta>
            <ta e="T75" id="Seg_1038" s="T74">chop-INF</ta>
            <ta e="T76" id="Seg_1039" s="T75">stay-PST.NAR-3PL</ta>
            <ta e="T78" id="Seg_1040" s="T77">down</ta>
            <ta e="T79" id="Seg_1041" s="T78">you.SG.NOM</ta>
            <ta e="T80" id="Seg_1042" s="T79">earth-ADJZ</ta>
            <ta e="T81" id="Seg_1043" s="T80">top-ILL</ta>
            <ta e="T82" id="Seg_1044" s="T81">chop-DRV-3PL</ta>
            <ta e="T83" id="Seg_1045" s="T82">house-EP-ACC</ta>
            <ta e="T85" id="Seg_1046" s="T84">earth-GEN</ta>
            <ta e="T86" id="Seg_1047" s="T85">top-LOC</ta>
            <ta e="T87" id="Seg_1048" s="T86">here</ta>
            <ta e="T88" id="Seg_1049" s="T87">who-EMPH</ta>
            <ta e="T89" id="Seg_1050" s="T88">%%-EP-3PL</ta>
            <ta e="T91" id="Seg_1051" s="T90">I.ALL</ta>
            <ta e="T92" id="Seg_1052" s="T91">human.being-PL-EMPH</ta>
            <ta e="T93" id="Seg_1053" s="T92">where-INDEF</ta>
            <ta e="T94" id="Seg_1054" s="T93">house-EP-ACC</ta>
            <ta e="T95" id="Seg_1055" s="T94">chop-INF</ta>
            <ta e="T96" id="Seg_1056" s="T95">oneself.3SG</ta>
            <ta e="T97" id="Seg_1057" s="T96">chop-CO-1SG.O</ta>
            <ta e="T99" id="Seg_1058" s="T98">darkness-LOC</ta>
            <ta e="T100" id="Seg_1059" s="T99">house-PL.[NOM]</ta>
            <ta e="T101" id="Seg_1060" s="T100">oneself.3SG</ta>
            <ta e="T102" id="Seg_1061" s="T101">where-INDEF</ta>
            <ta e="T103" id="Seg_1062" s="T102">house-PL.[NOM]</ta>
            <ta e="T104" id="Seg_1063" s="T103">oneself.3SG-1PL</ta>
            <ta e="T105" id="Seg_1064" s="T104">where-INDEF</ta>
            <ta e="T107" id="Seg_1065" s="T106">winter-ADJZ</ta>
            <ta e="T108" id="Seg_1066" s="T107">here</ta>
            <ta e="T109" id="Seg_1067" s="T108">winter-ADJZ</ta>
            <ta e="T110" id="Seg_1068" s="T109">come-PST.NAR-1SG.S</ta>
            <ta e="T111" id="Seg_1069" s="T110">I.NOM</ta>
            <ta e="T112" id="Seg_1070" s="T111">house-EP-ACC</ta>
            <ta e="T113" id="Seg_1071" s="T112">do-EP-1SG.O</ta>
            <ta e="T115" id="Seg_1072" s="T114">here</ta>
            <ta e="T116" id="Seg_1073" s="T115">walk-EP-HAB-%%</ta>
            <ta e="T117" id="Seg_1074" s="T116">that</ta>
            <ta e="T118" id="Seg_1075" s="T117">village-LOC</ta>
            <ta e="T120" id="Seg_1076" s="T119">come-EP-3PL</ta>
            <ta e="T121" id="Seg_1077" s="T120">human.being-PL.[NOM]</ta>
            <ta e="T123" id="Seg_1078" s="T122">also</ta>
            <ta e="T124" id="Seg_1079" s="T123">house-EP-%%</ta>
            <ta e="T125" id="Seg_1080" s="T124">house-EP-ACC</ta>
            <ta e="T126" id="Seg_1081" s="T125">sit.down-TR-DUR-HAB-CO-3PL</ta>
            <ta e="T128" id="Seg_1082" s="T127">and</ta>
            <ta e="T129" id="Seg_1083" s="T128">look.here</ta>
            <ta e="T130" id="Seg_1084" s="T129">here</ta>
            <ta e="T131" id="Seg_1085" s="T130">live-3DU.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1086" s="T1">я.ALL</ta>
            <ta e="T3" id="Seg_1087" s="T2">сорок</ta>
            <ta e="T4" id="Seg_1088" s="T3">два</ta>
            <ta e="T5" id="Seg_1089" s="T4">излишний-десять</ta>
            <ta e="T6" id="Seg_1090" s="T5">год.[NOM]-3SG</ta>
            <ta e="T8" id="Seg_1091" s="T7">вырасти-PST.NAR-1SG.S</ta>
            <ta e="T9" id="Seg_1092" s="T8">один</ta>
            <ta e="T10" id="Seg_1093" s="T9">десять</ta>
            <ta e="T11" id="Seg_1094" s="T10">шесть-десять</ta>
            <ta e="T12" id="Seg_1095" s="T11">шесть</ta>
            <ta e="T13" id="Seg_1096" s="T12">рыба-VBLZ-PST-3SG.O</ta>
            <ta e="T14" id="Seg_1097" s="T13">кто-INDEF</ta>
            <ta e="T16" id="Seg_1098" s="T15">прийти-PST.NAR-1SG.S</ta>
            <ta e="T17" id="Seg_1099" s="T16">Усть_Озёрное-ILL</ta>
            <ta e="T19" id="Seg_1100" s="T18">здесь</ta>
            <ta e="T20" id="Seg_1101" s="T19">жить-HAB-CO-1SG.S</ta>
            <ta e="T22" id="Seg_1102" s="T21">я.NOM</ta>
            <ta e="T23" id="Seg_1103" s="T22">прийти-PST-3SG.S</ta>
            <ta e="T24" id="Seg_1104" s="T23">работать-ILL</ta>
            <ta e="T25" id="Seg_1105" s="T24">жить-INF</ta>
            <ta e="T26" id="Seg_1106" s="T25">%%</ta>
            <ta e="T28" id="Seg_1107" s="T27">ну</ta>
            <ta e="T29" id="Seg_1108" s="T28">жить-HAB-PST-1SG</ta>
            <ta e="T31" id="Seg_1109" s="T30">здесь</ta>
            <ta e="T32" id="Seg_1110" s="T31">жизнь-ILL</ta>
            <ta e="T33" id="Seg_1111" s="T32">дойти-1SG.S</ta>
            <ta e="T35" id="Seg_1112" s="T34">Усть_Озёрное-LOC</ta>
            <ta e="T36" id="Seg_1113" s="T35">медработник.GEN</ta>
            <ta e="T37" id="Seg_1114" s="T36">NEG.EX-3SG.S</ta>
            <ta e="T39" id="Seg_1115" s="T38">мы.ALL</ta>
            <ta e="T40" id="Seg_1116" s="T39">сказать-EP-HAB-CO-3PL</ta>
            <ta e="T41" id="Seg_1117" s="T40">прийти-OPT-3PL</ta>
            <ta e="T42" id="Seg_1118" s="T41">прийти-OPT-3PL</ta>
            <ta e="T44" id="Seg_1119" s="T43">я.NOM</ta>
            <ta e="T45" id="Seg_1120" s="T44">Усть_Озёрное-LOC</ta>
            <ta e="T46" id="Seg_1121" s="T45">прийти-PST-1SG.S</ta>
            <ta e="T47" id="Seg_1122" s="T46">Усть_Озёрное-ALL</ta>
            <ta e="T48" id="Seg_1123" s="T47">прийти-PST-1SG.S</ta>
            <ta e="T50" id="Seg_1124" s="T49">четыре</ta>
            <ta e="T51" id="Seg_1125" s="T50">излишний-десять</ta>
            <ta e="T52" id="Seg_1126" s="T51">сорок</ta>
            <ta e="T53" id="Seg_1127" s="T52">один</ta>
            <ta e="T54" id="Seg_1128" s="T53">девять</ta>
            <ta e="T55" id="Seg_1129" s="T54">год-ILL.3SG</ta>
            <ta e="T56" id="Seg_1130" s="T55">сторона-LOC</ta>
            <ta e="T58" id="Seg_1131" s="T57">Усть_Озёрное-LOC</ta>
            <ta e="T59" id="Seg_1132" s="T58">сам.1SG</ta>
            <ta e="T60" id="Seg_1133" s="T59">человек-PL.[NOM]</ta>
            <ta e="T61" id="Seg_1134" s="T60">NEG.EX-PST-3PL</ta>
            <ta e="T63" id="Seg_1135" s="T62">прийти-PST-1SG.S</ta>
            <ta e="T64" id="Seg_1136" s="T63">чужой</ta>
            <ta e="T65" id="Seg_1137" s="T64">человек-PL-ALL</ta>
            <ta e="T67" id="Seg_1138" s="T66">жить-HAB-PST-1SG</ta>
            <ta e="T68" id="Seg_1139" s="T67">чужой</ta>
            <ta e="T69" id="Seg_1140" s="T68">человек-%%-LOC</ta>
            <ta e="T71" id="Seg_1141" s="T70">мать-EMPH</ta>
            <ta e="T72" id="Seg_1142" s="T71">отец-EMPH</ta>
            <ta e="T73" id="Seg_1143" s="T72">%%</ta>
            <ta e="T74" id="Seg_1144" s="T73">дом-EP-ACC</ta>
            <ta e="T75" id="Seg_1145" s="T74">срубить-INF</ta>
            <ta e="T76" id="Seg_1146" s="T75">остаться-PST.NAR-3PL</ta>
            <ta e="T78" id="Seg_1147" s="T77">вниз</ta>
            <ta e="T79" id="Seg_1148" s="T78">ты.NOM</ta>
            <ta e="T80" id="Seg_1149" s="T79">земля-ADJZ</ta>
            <ta e="T81" id="Seg_1150" s="T80">верх-ILL</ta>
            <ta e="T82" id="Seg_1151" s="T81">срубить-DRV-3PL</ta>
            <ta e="T83" id="Seg_1152" s="T82">дом-EP-ACC</ta>
            <ta e="T85" id="Seg_1153" s="T84">земля-GEN</ta>
            <ta e="T86" id="Seg_1154" s="T85">верх-LOC</ta>
            <ta e="T87" id="Seg_1155" s="T86">здесь</ta>
            <ta e="T88" id="Seg_1156" s="T87">кто-EMPH</ta>
            <ta e="T89" id="Seg_1157" s="T88">%%-EP-3PL</ta>
            <ta e="T91" id="Seg_1158" s="T90">я.ALL</ta>
            <ta e="T92" id="Seg_1159" s="T91">человек-PL-EMPH</ta>
            <ta e="T93" id="Seg_1160" s="T92">куда-INDEF</ta>
            <ta e="T94" id="Seg_1161" s="T93">дом-EP-ACC</ta>
            <ta e="T95" id="Seg_1162" s="T94">срубить-INF</ta>
            <ta e="T96" id="Seg_1163" s="T95">сам.3SG</ta>
            <ta e="T97" id="Seg_1164" s="T96">срубить-CO-1SG.O</ta>
            <ta e="T99" id="Seg_1165" s="T98">темнота-LOC</ta>
            <ta e="T100" id="Seg_1166" s="T99">дом-PL.[NOM]</ta>
            <ta e="T101" id="Seg_1167" s="T100">сам.3SG</ta>
            <ta e="T102" id="Seg_1168" s="T101">куда-INDEF</ta>
            <ta e="T103" id="Seg_1169" s="T102">дом-PL.[NOM]</ta>
            <ta e="T104" id="Seg_1170" s="T103">сам.3SG-1PL</ta>
            <ta e="T105" id="Seg_1171" s="T104">куда-INDEF</ta>
            <ta e="T107" id="Seg_1172" s="T106">зима-ADJZ</ta>
            <ta e="T108" id="Seg_1173" s="T107">здесь</ta>
            <ta e="T109" id="Seg_1174" s="T108">зима-ADJZ</ta>
            <ta e="T110" id="Seg_1175" s="T109">прийти-PST.NAR-1SG.S</ta>
            <ta e="T111" id="Seg_1176" s="T110">я.NOM</ta>
            <ta e="T112" id="Seg_1177" s="T111">дом-EP-ACC</ta>
            <ta e="T113" id="Seg_1178" s="T112">делать-EP-1SG.O</ta>
            <ta e="T115" id="Seg_1179" s="T114">здесь</ta>
            <ta e="T116" id="Seg_1180" s="T115">погулять-EP-HAB-%%</ta>
            <ta e="T117" id="Seg_1181" s="T116">тот</ta>
            <ta e="T118" id="Seg_1182" s="T117">поселок-LOC</ta>
            <ta e="T120" id="Seg_1183" s="T119">прийти-EP-3PL</ta>
            <ta e="T121" id="Seg_1184" s="T120">человек-PL.[NOM]</ta>
            <ta e="T123" id="Seg_1185" s="T122">тоже</ta>
            <ta e="T124" id="Seg_1186" s="T123">дом-EP-%%</ta>
            <ta e="T125" id="Seg_1187" s="T124">дом-EP-ACC</ta>
            <ta e="T126" id="Seg_1188" s="T125">сесть-TR-DUR-HAB-CO-3PL</ta>
            <ta e="T128" id="Seg_1189" s="T127">и</ta>
            <ta e="T129" id="Seg_1190" s="T128">вот</ta>
            <ta e="T130" id="Seg_1191" s="T129">здесь</ta>
            <ta e="T131" id="Seg_1192" s="T130">жить-3DU.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1193" s="T1">pers</ta>
            <ta e="T3" id="Seg_1194" s="T2">num</ta>
            <ta e="T4" id="Seg_1195" s="T3">num</ta>
            <ta e="T5" id="Seg_1196" s="T4">adj-num</ta>
            <ta e="T6" id="Seg_1197" s="T5">n.[n:case]-n:poss</ta>
            <ta e="T8" id="Seg_1198" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_1199" s="T8">num</ta>
            <ta e="T10" id="Seg_1200" s="T9">num</ta>
            <ta e="T11" id="Seg_1201" s="T10">num-num</ta>
            <ta e="T12" id="Seg_1202" s="T11">num</ta>
            <ta e="T13" id="Seg_1203" s="T12">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_1204" s="T13">interrog-clit</ta>
            <ta e="T16" id="Seg_1205" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_1206" s="T16">nprop-n:case</ta>
            <ta e="T19" id="Seg_1207" s="T18">adv</ta>
            <ta e="T20" id="Seg_1208" s="T19">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T22" id="Seg_1209" s="T21">pers</ta>
            <ta e="T23" id="Seg_1210" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_1211" s="T23">v-n:case</ta>
            <ta e="T25" id="Seg_1212" s="T24">v-v:inf</ta>
            <ta e="T26" id="Seg_1213" s="T25">adv</ta>
            <ta e="T28" id="Seg_1214" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_1215" s="T28">v-v&gt;v-v:tense-n:poss</ta>
            <ta e="T31" id="Seg_1216" s="T30">adv</ta>
            <ta e="T32" id="Seg_1217" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_1218" s="T32">v-v:pn</ta>
            <ta e="T35" id="Seg_1219" s="T34">nprop-n:case</ta>
            <ta e="T36" id="Seg_1220" s="T35">n</ta>
            <ta e="T37" id="Seg_1221" s="T36">v-v:pn</ta>
            <ta e="T39" id="Seg_1222" s="T38">pers</ta>
            <ta e="T40" id="Seg_1223" s="T39">v-n:ins-v&gt;v-v:ins-n:poss</ta>
            <ta e="T41" id="Seg_1224" s="T40">v-v:mood-v:pn</ta>
            <ta e="T42" id="Seg_1225" s="T41">v-v:mood-v:pn</ta>
            <ta e="T44" id="Seg_1226" s="T43">pers</ta>
            <ta e="T45" id="Seg_1227" s="T44">nprop-n:case</ta>
            <ta e="T46" id="Seg_1228" s="T45">v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_1229" s="T46">nprop-n:case</ta>
            <ta e="T48" id="Seg_1230" s="T47">v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_1231" s="T49">num</ta>
            <ta e="T51" id="Seg_1232" s="T50">adj-num</ta>
            <ta e="T52" id="Seg_1233" s="T51">num</ta>
            <ta e="T53" id="Seg_1234" s="T52">num</ta>
            <ta e="T54" id="Seg_1235" s="T53">num</ta>
            <ta e="T55" id="Seg_1236" s="T54">n-n:case.poss</ta>
            <ta e="T56" id="Seg_1237" s="T55">n-n:case</ta>
            <ta e="T58" id="Seg_1238" s="T57">nprop-n:case</ta>
            <ta e="T59" id="Seg_1239" s="T58">emphpro</ta>
            <ta e="T60" id="Seg_1240" s="T59">n-n:num.[n:case]</ta>
            <ta e="T61" id="Seg_1241" s="T60">v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_1242" s="T62">v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_1243" s="T63">adj</ta>
            <ta e="T65" id="Seg_1244" s="T64">n-n:num-n:case</ta>
            <ta e="T67" id="Seg_1245" s="T66">v-v&gt;v-v:tense-n:poss</ta>
            <ta e="T68" id="Seg_1246" s="T67">adj</ta>
            <ta e="T69" id="Seg_1247" s="T68">n-n&gt;n-n:case</ta>
            <ta e="T71" id="Seg_1248" s="T70">n-clit</ta>
            <ta e="T72" id="Seg_1249" s="T71">n-clit</ta>
            <ta e="T73" id="Seg_1250" s="T72">adj</ta>
            <ta e="T74" id="Seg_1251" s="T73">n-n:ins-n:case</ta>
            <ta e="T75" id="Seg_1252" s="T74">v-v:inf</ta>
            <ta e="T76" id="Seg_1253" s="T75">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_1254" s="T77">adv</ta>
            <ta e="T79" id="Seg_1255" s="T78">pers</ta>
            <ta e="T80" id="Seg_1256" s="T79">n-n&gt;adj</ta>
            <ta e="T81" id="Seg_1257" s="T80">n-n:case</ta>
            <ta e="T82" id="Seg_1258" s="T81">v-v&gt;v-v:pn</ta>
            <ta e="T83" id="Seg_1259" s="T82">n-n:ins-n:case</ta>
            <ta e="T85" id="Seg_1260" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_1261" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_1262" s="T86">adv</ta>
            <ta e="T88" id="Seg_1263" s="T87">interrog-clit</ta>
            <ta e="T89" id="Seg_1264" s="T88">v-n:ins-v:pn</ta>
            <ta e="T91" id="Seg_1265" s="T90">pers</ta>
            <ta e="T92" id="Seg_1266" s="T91">n-n:num--clit</ta>
            <ta e="T93" id="Seg_1267" s="T92">interrog-clit</ta>
            <ta e="T94" id="Seg_1268" s="T93">n-n:ins-n:case</ta>
            <ta e="T95" id="Seg_1269" s="T94">v-v:inf</ta>
            <ta e="T96" id="Seg_1270" s="T95">emphpro</ta>
            <ta e="T97" id="Seg_1271" s="T96">v-v:ins-v:pn</ta>
            <ta e="T99" id="Seg_1272" s="T98">n-n:case</ta>
            <ta e="T100" id="Seg_1273" s="T99">n-n:num.[n:case]</ta>
            <ta e="T101" id="Seg_1274" s="T100">emphpro</ta>
            <ta e="T102" id="Seg_1275" s="T101">interrog-clit</ta>
            <ta e="T103" id="Seg_1276" s="T102">n-n:num.[n:case]</ta>
            <ta e="T104" id="Seg_1277" s="T103">emphpro-n:poss</ta>
            <ta e="T105" id="Seg_1278" s="T104">interrog-clit</ta>
            <ta e="T107" id="Seg_1279" s="T106">n-adj&gt;adj</ta>
            <ta e="T108" id="Seg_1280" s="T107">adv</ta>
            <ta e="T109" id="Seg_1281" s="T108">n-adj&gt;adj</ta>
            <ta e="T110" id="Seg_1282" s="T109">v-v:tense-v:pn</ta>
            <ta e="T111" id="Seg_1283" s="T110">pers</ta>
            <ta e="T112" id="Seg_1284" s="T111">n-n:ins-n:case</ta>
            <ta e="T113" id="Seg_1285" s="T112">v-n:ins-v:pn</ta>
            <ta e="T115" id="Seg_1286" s="T114">adv</ta>
            <ta e="T116" id="Seg_1287" s="T115">v-n:ins-v&gt;v-v:pn</ta>
            <ta e="T117" id="Seg_1288" s="T116">dem</ta>
            <ta e="T118" id="Seg_1289" s="T117">n-n:case</ta>
            <ta e="T120" id="Seg_1290" s="T119">v-v:ins-v:pn</ta>
            <ta e="T121" id="Seg_1291" s="T120">n-n:num.[n:case]</ta>
            <ta e="T123" id="Seg_1292" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_1293" s="T123">n-n:ins-n:case</ta>
            <ta e="T125" id="Seg_1294" s="T124">n-n:ins-n:case</ta>
            <ta e="T126" id="Seg_1295" s="T125">v-v&gt;v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T128" id="Seg_1296" s="T127">conj</ta>
            <ta e="T129" id="Seg_1297" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_1298" s="T129">adv</ta>
            <ta e="T131" id="Seg_1299" s="T130">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1300" s="T1">pers</ta>
            <ta e="T3" id="Seg_1301" s="T2">num</ta>
            <ta e="T4" id="Seg_1302" s="T3">num</ta>
            <ta e="T5" id="Seg_1303" s="T4">num</ta>
            <ta e="T6" id="Seg_1304" s="T5">n</ta>
            <ta e="T8" id="Seg_1305" s="T7">v</ta>
            <ta e="T9" id="Seg_1306" s="T8">num</ta>
            <ta e="T10" id="Seg_1307" s="T9">num</ta>
            <ta e="T11" id="Seg_1308" s="T10">num</ta>
            <ta e="T12" id="Seg_1309" s="T11">num</ta>
            <ta e="T13" id="Seg_1310" s="T12">n</ta>
            <ta e="T14" id="Seg_1311" s="T13">interrog</ta>
            <ta e="T16" id="Seg_1312" s="T15">v</ta>
            <ta e="T17" id="Seg_1313" s="T16">nprop</ta>
            <ta e="T19" id="Seg_1314" s="T18">adv</ta>
            <ta e="T20" id="Seg_1315" s="T19">v</ta>
            <ta e="T22" id="Seg_1316" s="T21">pers</ta>
            <ta e="T23" id="Seg_1317" s="T22">v</ta>
            <ta e="T24" id="Seg_1318" s="T23">v</ta>
            <ta e="T25" id="Seg_1319" s="T24">v</ta>
            <ta e="T26" id="Seg_1320" s="T25">adv</ta>
            <ta e="T28" id="Seg_1321" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_1322" s="T28">v</ta>
            <ta e="T31" id="Seg_1323" s="T30">adv</ta>
            <ta e="T32" id="Seg_1324" s="T31">v</ta>
            <ta e="T33" id="Seg_1325" s="T32">v</ta>
            <ta e="T35" id="Seg_1326" s="T34">nprop</ta>
            <ta e="T36" id="Seg_1327" s="T35">n</ta>
            <ta e="T37" id="Seg_1328" s="T36">v</ta>
            <ta e="T39" id="Seg_1329" s="T38">pers</ta>
            <ta e="T40" id="Seg_1330" s="T39">v</ta>
            <ta e="T41" id="Seg_1331" s="T40">v</ta>
            <ta e="T42" id="Seg_1332" s="T41">v</ta>
            <ta e="T44" id="Seg_1333" s="T43">pers</ta>
            <ta e="T45" id="Seg_1334" s="T44">nprop</ta>
            <ta e="T46" id="Seg_1335" s="T45">v</ta>
            <ta e="T47" id="Seg_1336" s="T46">nprop</ta>
            <ta e="T48" id="Seg_1337" s="T47">v</ta>
            <ta e="T50" id="Seg_1338" s="T49">pers</ta>
            <ta e="T51" id="Seg_1339" s="T50">adj</ta>
            <ta e="T52" id="Seg_1340" s="T51">num</ta>
            <ta e="T53" id="Seg_1341" s="T52">num</ta>
            <ta e="T54" id="Seg_1342" s="T53">num</ta>
            <ta e="T55" id="Seg_1343" s="T54">n</ta>
            <ta e="T56" id="Seg_1344" s="T55">n</ta>
            <ta e="T58" id="Seg_1345" s="T57">nprop</ta>
            <ta e="T59" id="Seg_1346" s="T58">emphpro</ta>
            <ta e="T60" id="Seg_1347" s="T59">n</ta>
            <ta e="T61" id="Seg_1348" s="T60">v</ta>
            <ta e="T63" id="Seg_1349" s="T62">v</ta>
            <ta e="T64" id="Seg_1350" s="T63">adj</ta>
            <ta e="T65" id="Seg_1351" s="T64">n</ta>
            <ta e="T67" id="Seg_1352" s="T66">v</ta>
            <ta e="T68" id="Seg_1353" s="T67">adj</ta>
            <ta e="T69" id="Seg_1354" s="T68">n</ta>
            <ta e="T71" id="Seg_1355" s="T70">n</ta>
            <ta e="T72" id="Seg_1356" s="T71">n</ta>
            <ta e="T73" id="Seg_1357" s="T72">adj</ta>
            <ta e="T74" id="Seg_1358" s="T73">n</ta>
            <ta e="T75" id="Seg_1359" s="T74">v</ta>
            <ta e="T76" id="Seg_1360" s="T75">v</ta>
            <ta e="T78" id="Seg_1361" s="T77">adv</ta>
            <ta e="T79" id="Seg_1362" s="T78">pers</ta>
            <ta e="T80" id="Seg_1363" s="T79">n</ta>
            <ta e="T81" id="Seg_1364" s="T80">n</ta>
            <ta e="T82" id="Seg_1365" s="T81">v</ta>
            <ta e="T83" id="Seg_1366" s="T82">n</ta>
            <ta e="T85" id="Seg_1367" s="T84">n</ta>
            <ta e="T86" id="Seg_1368" s="T85">n</ta>
            <ta e="T87" id="Seg_1369" s="T86">adv</ta>
            <ta e="T88" id="Seg_1370" s="T87">interrog</ta>
            <ta e="T89" id="Seg_1371" s="T88">v</ta>
            <ta e="T91" id="Seg_1372" s="T90">pers</ta>
            <ta e="T92" id="Seg_1373" s="T91">n</ta>
            <ta e="T93" id="Seg_1374" s="T92">interrog</ta>
            <ta e="T94" id="Seg_1375" s="T93">n</ta>
            <ta e="T95" id="Seg_1376" s="T94">v</ta>
            <ta e="T96" id="Seg_1377" s="T95">emphpro</ta>
            <ta e="T97" id="Seg_1378" s="T96">v</ta>
            <ta e="T99" id="Seg_1379" s="T98">n</ta>
            <ta e="T100" id="Seg_1380" s="T99">n</ta>
            <ta e="T101" id="Seg_1381" s="T100">emphpro</ta>
            <ta e="T102" id="Seg_1382" s="T101">interrog</ta>
            <ta e="T103" id="Seg_1383" s="T102">n</ta>
            <ta e="T104" id="Seg_1384" s="T103">emphpro</ta>
            <ta e="T105" id="Seg_1385" s="T104">interrog</ta>
            <ta e="T107" id="Seg_1386" s="T106">v</ta>
            <ta e="T108" id="Seg_1387" s="T107">adv</ta>
            <ta e="T109" id="Seg_1388" s="T108">v</ta>
            <ta e="T110" id="Seg_1389" s="T109">v</ta>
            <ta e="T111" id="Seg_1390" s="T110">pers</ta>
            <ta e="T112" id="Seg_1391" s="T111">n</ta>
            <ta e="T113" id="Seg_1392" s="T112">v</ta>
            <ta e="T115" id="Seg_1393" s="T114">adv</ta>
            <ta e="T116" id="Seg_1394" s="T115">v</ta>
            <ta e="T117" id="Seg_1395" s="T116">dem</ta>
            <ta e="T118" id="Seg_1396" s="T117">n</ta>
            <ta e="T120" id="Seg_1397" s="T119">v</ta>
            <ta e="T121" id="Seg_1398" s="T120">n</ta>
            <ta e="T123" id="Seg_1399" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_1400" s="T123">n</ta>
            <ta e="T125" id="Seg_1401" s="T124">n</ta>
            <ta e="T126" id="Seg_1402" s="T125">v</ta>
            <ta e="T128" id="Seg_1403" s="T127">conj</ta>
            <ta e="T129" id="Seg_1404" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_1405" s="T129">adv</ta>
            <ta e="T131" id="Seg_1406" s="T130">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1407" s="T1">pro.h:Poss</ta>
            <ta e="T6" id="Seg_1408" s="T5">np:Th</ta>
            <ta e="T8" id="Seg_1409" s="T7">0.1.h:Th</ta>
            <ta e="T14" id="Seg_1410" s="T13">pro.h:A</ta>
            <ta e="T16" id="Seg_1411" s="T15">0.1.h:A</ta>
            <ta e="T17" id="Seg_1412" s="T16">np:G</ta>
            <ta e="T19" id="Seg_1413" s="T18">adv:L</ta>
            <ta e="T20" id="Seg_1414" s="T19">0.1.h:Th</ta>
            <ta e="T22" id="Seg_1415" s="T21">pro.h:A</ta>
            <ta e="T24" id="Seg_1416" s="T23">np:G</ta>
            <ta e="T25" id="Seg_1417" s="T24">0.1.h:Th</ta>
            <ta e="T29" id="Seg_1418" s="T28">0.1.h:Th</ta>
            <ta e="T32" id="Seg_1419" s="T31">np:G</ta>
            <ta e="T33" id="Seg_1420" s="T32">0.1.h:A</ta>
            <ta e="T35" id="Seg_1421" s="T34">np:L</ta>
            <ta e="T36" id="Seg_1422" s="T35">np.h:Th</ta>
            <ta e="T39" id="Seg_1423" s="T38">pro.h:R</ta>
            <ta e="T40" id="Seg_1424" s="T39">0.3.h:A</ta>
            <ta e="T41" id="Seg_1425" s="T40">0.3.h:A</ta>
            <ta e="T42" id="Seg_1426" s="T41">0.3.h:A</ta>
            <ta e="T44" id="Seg_1427" s="T43">pro.h:A</ta>
            <ta e="T45" id="Seg_1428" s="T44">np:G</ta>
            <ta e="T47" id="Seg_1429" s="T46">np:G</ta>
            <ta e="T48" id="Seg_1430" s="T47">0.1.h:A</ta>
            <ta e="T58" id="Seg_1431" s="T57">np:L</ta>
            <ta e="T60" id="Seg_1432" s="T59">np.h:Th</ta>
            <ta e="T63" id="Seg_1433" s="T62">0.1.h:A</ta>
            <ta e="T65" id="Seg_1434" s="T64">np.h:G</ta>
            <ta e="T67" id="Seg_1435" s="T66">0.1.h:Th</ta>
            <ta e="T69" id="Seg_1436" s="T68">np.h:L</ta>
            <ta e="T71" id="Seg_1437" s="T70">np.h:Th</ta>
            <ta e="T72" id="Seg_1438" s="T71">np.h:Th</ta>
            <ta e="T74" id="Seg_1439" s="T73">np:P</ta>
            <ta e="T75" id="Seg_1440" s="T74">0.3.h:A</ta>
            <ta e="T81" id="Seg_1441" s="T80">np:G</ta>
            <ta e="T82" id="Seg_1442" s="T81">0.3.h:A</ta>
            <ta e="T83" id="Seg_1443" s="T82">np:P</ta>
            <ta e="T85" id="Seg_1444" s="T84">np:Poss</ta>
            <ta e="T86" id="Seg_1445" s="T85">np:L</ta>
            <ta e="T88" id="Seg_1446" s="T87">pro.h:Th</ta>
            <ta e="T91" id="Seg_1447" s="T90">pro.h:B</ta>
            <ta e="T92" id="Seg_1448" s="T91">np.h:A</ta>
            <ta e="T94" id="Seg_1449" s="T93">np:P</ta>
            <ta e="T97" id="Seg_1450" s="T96">0.1.h:A 0.3:P</ta>
            <ta e="T108" id="Seg_1451" s="T107">adv:G</ta>
            <ta e="T109" id="Seg_1452" s="T108">np:Time</ta>
            <ta e="T110" id="Seg_1453" s="T109">0.1.h:A</ta>
            <ta e="T111" id="Seg_1454" s="T110">pro.h:A</ta>
            <ta e="T112" id="Seg_1455" s="T111">np:P</ta>
            <ta e="T116" id="Seg_1456" s="T115">0.1.h:A</ta>
            <ta e="T118" id="Seg_1457" s="T117">np:L</ta>
            <ta e="T121" id="Seg_1458" s="T120">np.h:A</ta>
            <ta e="T125" id="Seg_1459" s="T124">np:P</ta>
            <ta e="T126" id="Seg_1460" s="T125">0.3.h:A</ta>
            <ta e="T130" id="Seg_1461" s="T129">adv:L</ta>
            <ta e="T131" id="Seg_1462" s="T130">0.3.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T6" id="Seg_1463" s="T5">np:S</ta>
            <ta e="T8" id="Seg_1464" s="T7">0.1.h:S v:pred</ta>
            <ta e="T13" id="Seg_1465" s="T12">v:pred</ta>
            <ta e="T14" id="Seg_1466" s="T13">pro.h:S</ta>
            <ta e="T16" id="Seg_1467" s="T15">0.1.h:S v:pred</ta>
            <ta e="T20" id="Seg_1468" s="T19">0.1.h:S v:pred</ta>
            <ta e="T22" id="Seg_1469" s="T21">pro.h:S</ta>
            <ta e="T23" id="Seg_1470" s="T22">v:pred</ta>
            <ta e="T25" id="Seg_1471" s="T24">s:purp</ta>
            <ta e="T29" id="Seg_1472" s="T28">0.1.h:S v:pred</ta>
            <ta e="T33" id="Seg_1473" s="T32">0.1.h:S v:pred</ta>
            <ta e="T36" id="Seg_1474" s="T35">np.h:S</ta>
            <ta e="T37" id="Seg_1475" s="T36">v:pred</ta>
            <ta e="T40" id="Seg_1476" s="T39">0.3.h:S v:pred</ta>
            <ta e="T41" id="Seg_1477" s="T40">0.3.h:S v:pred</ta>
            <ta e="T42" id="Seg_1478" s="T41">0.3.h:S v:pred</ta>
            <ta e="T44" id="Seg_1479" s="T43">pro.h:S</ta>
            <ta e="T46" id="Seg_1480" s="T45">v:pred</ta>
            <ta e="T48" id="Seg_1481" s="T47">0.1.h:S v:pred</ta>
            <ta e="T60" id="Seg_1482" s="T59">np.h:S</ta>
            <ta e="T61" id="Seg_1483" s="T60">v:pred</ta>
            <ta e="T63" id="Seg_1484" s="T62">0.1.h:S v:pred</ta>
            <ta e="T67" id="Seg_1485" s="T66">0.1.h:S v:pred</ta>
            <ta e="T71" id="Seg_1486" s="T70">np.h:S</ta>
            <ta e="T72" id="Seg_1487" s="T71">np.h:S</ta>
            <ta e="T75" id="Seg_1488" s="T72">s:purp</ta>
            <ta e="T76" id="Seg_1489" s="T75">v:pred</ta>
            <ta e="T82" id="Seg_1490" s="T81">0.3.h:S v:pred</ta>
            <ta e="T83" id="Seg_1491" s="T82">np:O</ta>
            <ta e="T88" id="Seg_1492" s="T87">pro.h:S</ta>
            <ta e="T89" id="Seg_1493" s="T88">v:pred</ta>
            <ta e="T92" id="Seg_1494" s="T91">np.h:S</ta>
            <ta e="T94" id="Seg_1495" s="T93">np:O</ta>
            <ta e="T95" id="Seg_1496" s="T94">v:pred</ta>
            <ta e="T97" id="Seg_1497" s="T96">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T110" id="Seg_1498" s="T109">0.1.h:S v:pred</ta>
            <ta e="T111" id="Seg_1499" s="T110">pro.h:S</ta>
            <ta e="T112" id="Seg_1500" s="T111">np:O</ta>
            <ta e="T113" id="Seg_1501" s="T112">v:pred</ta>
            <ta e="T116" id="Seg_1502" s="T115">0.1.h:S v:pred</ta>
            <ta e="T120" id="Seg_1503" s="T119">v:pred</ta>
            <ta e="T121" id="Seg_1504" s="T120">np.h:S</ta>
            <ta e="T125" id="Seg_1505" s="T124">np:O</ta>
            <ta e="T126" id="Seg_1506" s="T125">0.3.h:S v:pred</ta>
            <ta e="T131" id="Seg_1507" s="T130">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T14" id="Seg_1508" s="T13">RUS:gram</ta>
            <ta e="T17" id="Seg_1509" s="T16">RUS:cult</ta>
            <ta e="T28" id="Seg_1510" s="T27">RUS:disc</ta>
            <ta e="T36" id="Seg_1511" s="T35">RUS:cult</ta>
            <ta e="T93" id="Seg_1512" s="T92">RUS:gram</ta>
            <ta e="T102" id="Seg_1513" s="T101">RUS:gram</ta>
            <ta e="T105" id="Seg_1514" s="T104">RUS:gram</ta>
            <ta e="T118" id="Seg_1515" s="T117">RUS:core</ta>
            <ta e="T128" id="Seg_1516" s="T127">RUS:gram</ta>
            <ta e="T129" id="Seg_1517" s="T128">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T17" id="Seg_1518" s="T16">medCdel medVdel</ta>
            <ta e="T28" id="Seg_1519" s="T27">Vsub</ta>
            <ta e="T118" id="Seg_1520" s="T117">medVdel finVins</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T17" id="Seg_1521" s="T16">dir:infl</ta>
            <ta e="T36" id="Seg_1522" s="T35">parad:bare</ta>
            <ta e="T118" id="Seg_1523" s="T117">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T36" id="Seg_1524" s="T35">RUS:int.ins</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_1525" s="T1">I am forty two years old.</ta>
            <ta e="T14" id="Seg_1526" s="T7">I grew up, someone was fishing (sixty six years?).</ta>
            <ta e="T17" id="Seg_1527" s="T15">I came to Ust'-Ozyornoe.</ta>
            <ta e="T20" id="Seg_1528" s="T18">I used to live here.</ta>
            <ta e="T26" id="Seg_1529" s="T21">I came to work and to live here.</ta>
            <ta e="T29" id="Seg_1530" s="T27">Well, I live.</ta>
            <ta e="T33" id="Seg_1531" s="T30">I came here to live.</ta>
            <ta e="T37" id="Seg_1532" s="T34">There is no health care workeres in Ust'-Ozyornoe.</ta>
            <ta e="T42" id="Seg_1533" s="T38">They told us: Come, come.</ta>
            <ta e="T48" id="Seg_1534" s="T43">I came to Ust'-Ozyornoe, I came to Ust'-Ozyornoe.</ta>
            <ta e="T56" id="Seg_1535" s="T49">Forty one, forty one years.</ta>
            <ta e="T61" id="Seg_1536" s="T57">There were no relatives in Ust'-Ozyornoe.</ta>
            <ta e="T65" id="Seg_1537" s="T62">I came to the strangers.</ta>
            <ta e="T69" id="Seg_1538" s="T66">I lived with the strangers.</ta>
            <ta e="T76" id="Seg_1539" s="T70">Mother, father, they stayed to build a big house.</ta>
            <ta e="T83" id="Seg_1540" s="T77">They built a house down on the ground.</ta>
            <ta e="T89" id="Seg_1541" s="T84">Noone stayed(?) here on the ground.</ta>
            <ta e="T97" id="Seg_1542" s="T90">Once people built a house for me, I cut trees myself.</ta>
            <ta e="T105" id="Seg_1543" s="T98">In the darkness the houses (?).</ta>
            <ta e="T113" id="Seg_1544" s="T106">In winter, I came here in winter, I built a house.</ta>
            <ta e="T118" id="Seg_1545" s="T114">We(?) were walking(?/celebrating?) here, in the village.</ta>
            <ta e="T121" id="Seg_1546" s="T119">Other people came.</ta>
            <ta e="T126" id="Seg_1547" s="T122">They also built houses.</ta>
            <ta e="T131" id="Seg_1548" s="T127">And now they live here.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_1549" s="T1">Ich bin zweiundvierzig Jahre alt.</ta>
            <ta e="T14" id="Seg_1550" s="T7">Ich bin aufgewachsen, jemand fischte (sechsundsechzig Jahre lang?).</ta>
            <ta e="T17" id="Seg_1551" s="T15">Ich kam nach Ust'-Ozyornoe.</ta>
            <ta e="T20" id="Seg_1552" s="T18">Ich lebte hier.</ta>
            <ta e="T26" id="Seg_1553" s="T21">Ich kam hierher um zu arbeiten und zu leben.</ta>
            <ta e="T29" id="Seg_1554" s="T27">Also, ich lebe.</ta>
            <ta e="T33" id="Seg_1555" s="T30">Ich kam hierher um zu leben.</ta>
            <ta e="T37" id="Seg_1556" s="T34">Es gibt in Ust'-Ozyornoe keine Krankenpfleger.</ta>
            <ta e="T42" id="Seg_1557" s="T38">Sie sagten uns: Kommt, kommt.</ta>
            <ta e="T48" id="Seg_1558" s="T43">Ich kam nach Ust'-Ozyornoe, ich kam nach Ust'-Ozyornoe.</ta>
            <ta e="T56" id="Seg_1559" s="T49">Einundvierzig, einundvierzig Jahre.</ta>
            <ta e="T61" id="Seg_1560" s="T57">Ich hatte keine Verwandten in Ust'-Ozyornoe.</ta>
            <ta e="T65" id="Seg_1561" s="T62">Ich kam zu Fremden.</ta>
            <ta e="T69" id="Seg_1562" s="T66">Ich lebte bei Fremden.</ta>
            <ta e="T76" id="Seg_1563" s="T70">Mutter, Vater, sie blieben, um ein großes Haus zu bauen.</ta>
            <ta e="T83" id="Seg_1564" s="T77">Sie bauten ein Haus unten auf dem Boden.</ta>
            <ta e="T89" id="Seg_1565" s="T84">Keiner blieb(?) hier auf dem Boden.</ta>
            <ta e="T97" id="Seg_1566" s="T90">Einmal bauten Leute ein Haus für mich, ich fällte die Bäume selbst.</ta>
            <ta e="T105" id="Seg_1567" s="T98">In der Dunkelheit die Häuser (?).</ta>
            <ta e="T113" id="Seg_1568" s="T106">Im Winter, ich kam hierher im Winter, ich baute ein Haus.</ta>
            <ta e="T118" id="Seg_1569" s="T114">Wir(?) liefen(?/feierten?) hier, im Dorf.</ta>
            <ta e="T121" id="Seg_1570" s="T119">Andere Menschen kamen.</ta>
            <ta e="T126" id="Seg_1571" s="T122">Sie bauten auch Häuser.</ta>
            <ta e="T131" id="Seg_1572" s="T127">Und jetzt leben sie hier.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_1573" s="T1">Мне сорок два с лишним года.</ta>
            <ta e="T14" id="Seg_1574" s="T7">Вырос, один (?), шестьдесят шесть лет рыбачил кто-то.</ta>
            <ta e="T17" id="Seg_1575" s="T15">Приехал в Усть-Озёрное.</ta>
            <ta e="T20" id="Seg_1576" s="T18">Здесь жил.</ta>
            <ta e="T26" id="Seg_1577" s="T21">Я приехал на работу, жить здесь.</ta>
            <ta e="T29" id="Seg_1578" s="T27">Да, живу.</ta>
            <ta e="T33" id="Seg_1579" s="T30">Сюда жить приехал.</ta>
            <ta e="T37" id="Seg_1580" s="T34">В Усть-Озёрном медработника нет.</ta>
            <ta e="T42" id="Seg_1581" s="T38">Нам сказали: приезжайте, приезжайте.</ta>
            <ta e="T48" id="Seg_1582" s="T43">Я в Усть-Озёрное приехал, в Усть-Озёрное приехал.</ta>
            <ta e="T56" id="Seg_1583" s="T49">Сорок один(?) год.</ta>
            <ta e="T61" id="Seg_1584" s="T57">В Усть-Озёрном родственников не было.</ta>
            <ta e="T65" id="Seg_1585" s="T62">Приехал к чужим людям.</ta>
            <ta e="T69" id="Seg_1586" s="T66">Жил у чужих людей.</ta>
            <ta e="T76" id="Seg_1587" s="T70">Мать, отец огромный дом строить остались.</ta>
            <ta e="T83" id="Seg_1588" s="T77">Внизу на земле срубили дом.</ta>
            <ta e="T89" id="Seg_1589" s="T84">На земле здесь никто не оставался.</ta>
            <ta e="T97" id="Seg_1590" s="T90">Мне люди когда-то дом рубили, сам срубил.</ta>
            <ta e="T105" id="Seg_1591" s="T98">В темноте дома сами когда-то, дома сами когда-то.</ta>
            <ta e="T113" id="Seg_1592" s="T106">Зимой сюда, зимой приехал, я дом построил.</ta>
            <ta e="T118" id="Seg_1593" s="T114">Здесь гуляли, в том посёлке.</ta>
            <ta e="T121" id="Seg_1594" s="T119">Понаехали люди.</ta>
            <ta e="T126" id="Seg_1595" s="T122">Тоже дома, дом построили.</ta>
            <ta e="T131" id="Seg_1596" s="T127">И вот здесь живут.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_1597" s="T1">Стоит сорок два с лишним года.</ta>
            <ta e="T14" id="Seg_1598" s="T7">Вырос, один год, шестьдесят шесть лет рыбачил кто-то.</ta>
            <ta e="T17" id="Seg_1599" s="T15">Приехала в Усть-Озёрное.</ta>
            <ta e="T20" id="Seg_1600" s="T18">Здесь жила.</ta>
            <ta e="T26" id="Seg_1601" s="T21">Я приехала на работу, жить здесь.</ta>
            <ta e="T29" id="Seg_1602" s="T27">Да, живу.</ta>
            <ta e="T33" id="Seg_1603" s="T30">Сюда жить приехала.</ta>
            <ta e="T37" id="Seg_1604" s="T34">В Усть-Озёрном медработника нет.</ta>
            <ta e="T42" id="Seg_1605" s="T38">Нам сказали: приезжайте, приезжайте.</ta>
            <ta e="T48" id="Seg_1606" s="T43">Я в Усть-Озёрное приехала, в Усть-Озёрное приехала.</ta>
            <ta e="T56" id="Seg_1607" s="T49">Четыре с лишним сорок один.</ta>
            <ta e="T61" id="Seg_1608" s="T57">В Усть- Озёрном родственников не было.</ta>
            <ta e="T65" id="Seg_1609" s="T62">Приехала к чужим людям.</ta>
            <ta e="T69" id="Seg_1610" s="T66">Жила у чужих людей.</ta>
            <ta e="T76" id="Seg_1611" s="T70">Мать, отец огромный дом строить остались.</ta>
            <ta e="T83" id="Seg_1612" s="T77">Внизу на земле срубили дом.</ta>
            <ta e="T89" id="Seg_1613" s="T84">На земле здесь никто не оставался.</ta>
            <ta e="T97" id="Seg_1614" s="T90">Мне люди когда-то дом рубили, сама срубила.</ta>
            <ta e="T105" id="Seg_1615" s="T98">В темноте дома сами когда-то, дома сами когда-то.</ta>
            <ta e="T113" id="Seg_1616" s="T106">Зимой сюда, зимой приехала, я дом построила.</ta>
            <ta e="T118" id="Seg_1617" s="T114">Здесь гуляли, в том посёлке.</ta>
            <ta e="T121" id="Seg_1618" s="T119">Понаехали люди.</ta>
            <ta e="T126" id="Seg_1619" s="T122">Тоже дома, дом построили.</ta>
            <ta e="T131" id="Seg_1620" s="T127">И вот здесь живут.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T56" id="Seg_1621" s="T49">[DCh:] May it mean 'in [19]41'?</ta>
            <ta e="T118" id="Seg_1622" s="T114">[WNB:] The verbal ending in erukaŋk is not clear.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
