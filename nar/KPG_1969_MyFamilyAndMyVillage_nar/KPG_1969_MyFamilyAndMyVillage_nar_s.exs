<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KPG_1969_MyFamilyAndMyVillage_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KPG_1969_MyFamilyAndMyVillage_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">101</ud-information>
            <ud-information attribute-name="# HIAT:w">82</ud-information>
            <ud-information attribute-name="# e">82</ud-information>
            <ud-information attribute-name="# HIAT:u">19</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KPG">
            <abbreviation>KPG</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KPG"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T82" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">ilisak</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">Tolʼkaqɨn</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Tɨmta</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">meːl</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">čʼäːŋkɨsan</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">rušilʼ</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">mɔːn</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_32" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">Šölʼqumɨlʼ</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">mɔːssa</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">ilesɔːtɨn</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_44" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">Puːn</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">štälla</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">qumɨt</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">tüsɔːtɨn</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_59" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">Mɔːtɨm</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">meːqolowsɔːtɨn</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_68" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_70" n="HIAT:w" s="T17">Mɔːt</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_73" n="HIAT:w" s="T18">meːlä</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_76" n="HIAT:w" s="T19">tuːrtukula</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">puːla</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">prikašʼima</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">tüsona</ts>
                  <nts id="Seg_86" n="HIAT:ip">.</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_89" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_91" n="HIAT:w" s="T23">Aj</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">tantalkutij</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_97" n="HIAT:w" s="T25">imman</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_101" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_103" n="HIAT:w" s="T26">Mi</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_106" n="HIAT:w" s="T27">tantaltukkoqolowsɔːmɨn</ts>
                  <nts id="Seg_107" n="HIAT:ip">.</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_110" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_112" n="HIAT:w" s="T28">Tantalkla</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_115" n="HIAT:w" s="T29">puːla</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_118" n="HIAT:w" s="T30">tuːrtukusak</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_121" n="HIAT:w" s="T31">kekkɨssa</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_124" n="HIAT:w" s="T32">nɔːkɨr</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">klass</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">končʼilisa</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_134" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_136" n="HIAT:w" s="T35">Šitɨ</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_139" n="HIAT:w" s="T36">poːntə</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_142" n="HIAT:w" s="T37">kunta</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_145" n="HIAT:w" s="T38">moqɨllän</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_148" n="HIAT:w" s="T39">wärkɨsaŋ</ts>
                  <nts id="Seg_149" n="HIAT:ip">.</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_152" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_154" n="HIAT:w" s="T40">Puːn</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_157" n="HIAT:w" s="T41">irra</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_160" n="HIAT:w" s="T42">qossak</ts>
                  <nts id="Seg_161" n="HIAT:ip">.</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_164" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_166" n="HIAT:w" s="T43">Ukkur</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_169" n="HIAT:w" s="T44">nätalʼem</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_172" n="HIAT:w" s="T45">ɛːsanä</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_175" n="HIAT:w" s="T46">aj</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_178" n="HIAT:w" s="T47">ukkur</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_181" n="HIAT:w" s="T48">iːjalʼam</ts>
                  <nts id="Seg_182" n="HIAT:ip">.</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_185" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_187" n="HIAT:w" s="T49">Na</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_190" n="HIAT:w" s="T50">iːjalʼa</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_193" n="HIAT:w" s="T51">qüːttäla</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_196" n="HIAT:w" s="T52">ılla</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_199" n="HIAT:w" s="T53">qusanä</ts>
                  <nts id="Seg_200" n="HIAT:ip">.</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_203" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_205" n="HIAT:w" s="T54">Kət</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_208" n="HIAT:w" s="T55">čʼantɨlʼ</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_211" n="HIAT:w" s="T56">kotaqɨnɨ</ts>
                  <nts id="Seg_212" n="HIAT:ip">.</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_215" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_217" n="HIAT:w" s="T57">Poːlʼ</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_220" n="HIAT:w" s="T58">qorsa</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_223" n="HIAT:w" s="T59">meːsɔːtɨn</ts>
                  <nts id="Seg_224" n="HIAT:ip">.</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_227" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_229" n="HIAT:w" s="T60">Säːrɨ</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_232" n="HIAT:w" s="T61">qampısa</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_235" n="HIAT:w" s="T62">aj</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_238" n="HIAT:w" s="T63">säːqä</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_241" n="HIAT:w" s="T64">qampısa</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_244" n="HIAT:w" s="T65">tamtɨlsɔːtɨn</ts>
                  <nts id="Seg_245" n="HIAT:ip">.</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_248" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_250" n="HIAT:w" s="T66">Stälʼ</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_253" n="HIAT:w" s="T67">qəntisɔːtɨn</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_256" n="HIAT:w" s="T68">qumɨlʼ</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_259" n="HIAT:w" s="T69">mʼaːktäntɨ</ts>
                  <nts id="Seg_260" n="HIAT:ip">.</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_263" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_265" n="HIAT:w" s="T70">Načʼä</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_268" n="HIAT:w" s="T71">qontɨlʼa</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_271" n="HIAT:w" s="T72">nɨː</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_274" n="HIAT:w" s="T73">tottɨsɔːtɨn</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_277" n="HIAT:w" s="T74">əsɨntɨ</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_280" n="HIAT:w" s="T75">tükɨ</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_283" n="HIAT:w" s="T76">təttɨ</ts>
                  <nts id="Seg_284" n="HIAT:ip">.</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_287" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_289" n="HIAT:w" s="T77">Əsɨtɨ</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_292" n="HIAT:w" s="T78">tülla</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_295" n="HIAT:w" s="T79">puːla</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_298" n="HIAT:w" s="T80">ılla</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_301" n="HIAT:w" s="T81">meːsɔːtɨn</ts>
                  <nts id="Seg_302" n="HIAT:ip">.</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T82" id="Seg_304" n="sc" s="T0">
               <ts e="T1" id="Seg_306" n="e" s="T0">Man </ts>
               <ts e="T2" id="Seg_308" n="e" s="T1">ilisak </ts>
               <ts e="T3" id="Seg_310" n="e" s="T2">Tolʼkaqɨn. </ts>
               <ts e="T4" id="Seg_312" n="e" s="T3">Tɨmta </ts>
               <ts e="T5" id="Seg_314" n="e" s="T4">meːl </ts>
               <ts e="T6" id="Seg_316" n="e" s="T5">čʼäːŋkɨsan </ts>
               <ts e="T7" id="Seg_318" n="e" s="T6">rušilʼ </ts>
               <ts e="T8" id="Seg_320" n="e" s="T7">mɔːn. </ts>
               <ts e="T9" id="Seg_322" n="e" s="T8">Šölʼqumɨlʼ </ts>
               <ts e="T10" id="Seg_324" n="e" s="T9">mɔːssa </ts>
               <ts e="T11" id="Seg_326" n="e" s="T10">ilesɔːtɨn. </ts>
               <ts e="T12" id="Seg_328" n="e" s="T11">Puːn </ts>
               <ts e="T13" id="Seg_330" n="e" s="T12">štälla </ts>
               <ts e="T14" id="Seg_332" n="e" s="T13">qumɨt </ts>
               <ts e="T15" id="Seg_334" n="e" s="T14">tüsɔːtɨn. </ts>
               <ts e="T16" id="Seg_336" n="e" s="T15">Mɔːtɨm </ts>
               <ts e="T17" id="Seg_338" n="e" s="T16">meːqolowsɔːtɨn. </ts>
               <ts e="T18" id="Seg_340" n="e" s="T17">Mɔːt </ts>
               <ts e="T19" id="Seg_342" n="e" s="T18">meːlä </ts>
               <ts e="T20" id="Seg_344" n="e" s="T19">tuːrtukula </ts>
               <ts e="T21" id="Seg_346" n="e" s="T20">puːla </ts>
               <ts e="T22" id="Seg_348" n="e" s="T21">prikašʼima </ts>
               <ts e="T23" id="Seg_350" n="e" s="T22">tüsona. </ts>
               <ts e="T24" id="Seg_352" n="e" s="T23">Aj </ts>
               <ts e="T25" id="Seg_354" n="e" s="T24">tantalkutij </ts>
               <ts e="T26" id="Seg_356" n="e" s="T25">imman. </ts>
               <ts e="T27" id="Seg_358" n="e" s="T26">Mi </ts>
               <ts e="T28" id="Seg_360" n="e" s="T27">tantaltukkoqolowsɔːmɨn. </ts>
               <ts e="T29" id="Seg_362" n="e" s="T28">Tantalkla </ts>
               <ts e="T30" id="Seg_364" n="e" s="T29">puːla </ts>
               <ts e="T31" id="Seg_366" n="e" s="T30">tuːrtukusak </ts>
               <ts e="T32" id="Seg_368" n="e" s="T31">kekkɨssa </ts>
               <ts e="T33" id="Seg_370" n="e" s="T32">nɔːkɨr </ts>
               <ts e="T34" id="Seg_372" n="e" s="T33">klass </ts>
               <ts e="T35" id="Seg_374" n="e" s="T34">končʼilisa. </ts>
               <ts e="T36" id="Seg_376" n="e" s="T35">Šitɨ </ts>
               <ts e="T37" id="Seg_378" n="e" s="T36">poːntə </ts>
               <ts e="T38" id="Seg_380" n="e" s="T37">kunta </ts>
               <ts e="T39" id="Seg_382" n="e" s="T38">moqɨllän </ts>
               <ts e="T40" id="Seg_384" n="e" s="T39">wärkɨsaŋ. </ts>
               <ts e="T41" id="Seg_386" n="e" s="T40">Puːn </ts>
               <ts e="T42" id="Seg_388" n="e" s="T41">irra </ts>
               <ts e="T43" id="Seg_390" n="e" s="T42">qossak. </ts>
               <ts e="T44" id="Seg_392" n="e" s="T43">Ukkur </ts>
               <ts e="T45" id="Seg_394" n="e" s="T44">nätalʼem </ts>
               <ts e="T46" id="Seg_396" n="e" s="T45">ɛːsanä </ts>
               <ts e="T47" id="Seg_398" n="e" s="T46">aj </ts>
               <ts e="T48" id="Seg_400" n="e" s="T47">ukkur </ts>
               <ts e="T49" id="Seg_402" n="e" s="T48">iːjalʼam. </ts>
               <ts e="T50" id="Seg_404" n="e" s="T49">Na </ts>
               <ts e="T51" id="Seg_406" n="e" s="T50">iːjalʼa </ts>
               <ts e="T52" id="Seg_408" n="e" s="T51">qüːttäla </ts>
               <ts e="T53" id="Seg_410" n="e" s="T52">ılla </ts>
               <ts e="T54" id="Seg_412" n="e" s="T53">qusanä. </ts>
               <ts e="T55" id="Seg_414" n="e" s="T54">Kət </ts>
               <ts e="T56" id="Seg_416" n="e" s="T55">čʼantɨlʼ </ts>
               <ts e="T57" id="Seg_418" n="e" s="T56">kotaqɨnɨ. </ts>
               <ts e="T58" id="Seg_420" n="e" s="T57">Poːlʼ </ts>
               <ts e="T59" id="Seg_422" n="e" s="T58">qorsa </ts>
               <ts e="T60" id="Seg_424" n="e" s="T59">meːsɔːtɨn. </ts>
               <ts e="T61" id="Seg_426" n="e" s="T60">Säːrɨ </ts>
               <ts e="T62" id="Seg_428" n="e" s="T61">qampısa </ts>
               <ts e="T63" id="Seg_430" n="e" s="T62">aj </ts>
               <ts e="T64" id="Seg_432" n="e" s="T63">säːqä </ts>
               <ts e="T65" id="Seg_434" n="e" s="T64">qampısa </ts>
               <ts e="T66" id="Seg_436" n="e" s="T65">tamtɨlsɔːtɨn. </ts>
               <ts e="T67" id="Seg_438" n="e" s="T66">Stälʼ </ts>
               <ts e="T68" id="Seg_440" n="e" s="T67">qəntisɔːtɨn </ts>
               <ts e="T69" id="Seg_442" n="e" s="T68">qumɨlʼ </ts>
               <ts e="T70" id="Seg_444" n="e" s="T69">mʼaːktäntɨ. </ts>
               <ts e="T71" id="Seg_446" n="e" s="T70">Načʼä </ts>
               <ts e="T72" id="Seg_448" n="e" s="T71">qontɨlʼa </ts>
               <ts e="T73" id="Seg_450" n="e" s="T72">nɨː </ts>
               <ts e="T74" id="Seg_452" n="e" s="T73">tottɨsɔːtɨn </ts>
               <ts e="T75" id="Seg_454" n="e" s="T74">əsɨntɨ </ts>
               <ts e="T76" id="Seg_456" n="e" s="T75">tükɨ </ts>
               <ts e="T77" id="Seg_458" n="e" s="T76">təttɨ. </ts>
               <ts e="T78" id="Seg_460" n="e" s="T77">Əsɨtɨ </ts>
               <ts e="T79" id="Seg_462" n="e" s="T78">tülla </ts>
               <ts e="T80" id="Seg_464" n="e" s="T79">puːla </ts>
               <ts e="T81" id="Seg_466" n="e" s="T80">ılla </ts>
               <ts e="T82" id="Seg_468" n="e" s="T81">meːsɔːtɨn. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_469" s="T0">KPG_1969_MyFamilyAndMyVillage_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_470" s="T3">KPG_1969_MyFamilyAndMyVillage_nar.002 (001.002)</ta>
            <ta e="T11" id="Seg_471" s="T8">KPG_1969_MyFamilyAndMyVillage_nar.003 (001.003)</ta>
            <ta e="T15" id="Seg_472" s="T11">KPG_1969_MyFamilyAndMyVillage_nar.004 (001.004)</ta>
            <ta e="T17" id="Seg_473" s="T15">KPG_1969_MyFamilyAndMyVillage_nar.005 (001.005)</ta>
            <ta e="T23" id="Seg_474" s="T17">KPG_1969_MyFamilyAndMyVillage_nar.006 (001.006)</ta>
            <ta e="T26" id="Seg_475" s="T23">KPG_1969_MyFamilyAndMyVillage_nar.007 (001.007)</ta>
            <ta e="T28" id="Seg_476" s="T26">KPG_1969_MyFamilyAndMyVillage_nar.008 (001.008)</ta>
            <ta e="T35" id="Seg_477" s="T28">KPG_1969_MyFamilyAndMyVillage_nar.009 (001.009)</ta>
            <ta e="T40" id="Seg_478" s="T35">KPG_1969_MyFamilyAndMyVillage_nar.010 (001.010)</ta>
            <ta e="T43" id="Seg_479" s="T40">KPG_1969_MyFamilyAndMyVillage_nar.011 (001.011)</ta>
            <ta e="T49" id="Seg_480" s="T43">KPG_1969_MyFamilyAndMyVillage_nar.012 (001.012)</ta>
            <ta e="T54" id="Seg_481" s="T49">KPG_1969_MyFamilyAndMyVillage_nar.013 (001.013)</ta>
            <ta e="T57" id="Seg_482" s="T54">KPG_1969_MyFamilyAndMyVillage_nar.014 (001.014)</ta>
            <ta e="T60" id="Seg_483" s="T57">KPG_1969_MyFamilyAndMyVillage_nar.015 (001.015)</ta>
            <ta e="T66" id="Seg_484" s="T60">KPG_1969_MyFamilyAndMyVillage_nar.016 (001.016)</ta>
            <ta e="T70" id="Seg_485" s="T66">KPG_1969_MyFamilyAndMyVillage_nar.017 (001.017)</ta>
            <ta e="T77" id="Seg_486" s="T70">KPG_1969_MyFamilyAndMyVillage_nar.018 (001.018)</ta>
            <ta e="T82" id="Seg_487" s="T77">KPG_1969_MyFamilyAndMyVillage_nar.019 (001.019)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_488" s="T0">ман ′ӣlисах ′толʼкаɣын.</ta>
            <ta e="T8" id="Seg_489" s="T3">′тымта меl ′чеңкысан ′рушʼиl ма̊н.</ta>
            <ta e="T11" id="Seg_490" s="T8">шʼӧlkумыlʼ ′ма̊̄сса ′иlе‵со̄тын.</ta>
            <ta e="T15" id="Seg_491" s="T11">пӯн ′штӓllа ′kумыт тӱ̄сотын.</ta>
            <ta e="T17" id="Seg_492" s="T15">ма̊̄тым ме̄kоlоwсотын.</ta>
            <ta e="T23" id="Seg_493" s="T17">ма̊̄т ме̄lе ′туртукуlа пӯlа прикащʼима ′тӱ̄сона.</ta>
            <ta e="T26" id="Seg_494" s="T23">ай ′тантаlкутий ′имман.</ta>
            <ta e="T28" id="Seg_495" s="T26">ми тантаl′тукокоlоw′соw[м]ын.</ta>
            <ta e="T35" id="Seg_496" s="T28">′тантаlкlа ′пӯlа ′тӯртукусах ′кеккысса на̊̄кыр кlасс ′кончиlиса.</ta>
            <ta e="T40" id="Seg_497" s="T35">шʼиты ′пондъ кунда моkыllӓн ′wӓркысаң.</ta>
            <ta e="T43" id="Seg_498" s="T40">пӯн ′ирра ′kо̄ссах.</ta>
            <ta e="T49" id="Seg_499" s="T43">′уккур ′нӓтаlем ′е̨̄санӓ ай ′уккур ӣjаlам.</ta>
            <ta e="T54" id="Seg_500" s="T49">на ′ӣjаlа ′кӱ̄ттӓlа ′иllа ′кӯсанӓ.</ta>
            <ta e="T57" id="Seg_501" s="T54">къ̊т чан′дыl ′k[к]о‵таkыны.</ta>
            <ta e="T60" id="Seg_502" s="T57">поlɣорса ′ме̄сотын.</ta>
            <ta e="T66" id="Seg_503" s="T60">′сӓ̄ры ′kамписа ай сӓɣ[k]ӓ ′kамписа ′тамтылʼсо̄тын.</ta>
            <ta e="T70" id="Seg_504" s="T66">штӓl ′kъ̊нтисотын kумыl мʼа̄к′тӓнды.</ta>
            <ta e="T77" id="Seg_505" s="T70">′наче ′kондылʼа ны ′тоттысотын ə̄сынты ‵тӱ̄кы′тътты.</ta>
            <ta e="T82" id="Seg_506" s="T77">′əсыты тӱ̄llа ′пӯlа ′иllа ′ме′со̄тын.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_507" s="T0">man iːlʼisah tolʼkaqɨn.</ta>
            <ta e="T8" id="Seg_508" s="T3">tɨmta melʼ čeŋkɨsan rušilʼ man.</ta>
            <ta e="T11" id="Seg_509" s="T8">šölʼqumɨlʼ maːssa ilʼesoːtɨn.</ta>
            <ta e="T15" id="Seg_510" s="T11">puːn štälʼlʼa qumɨt tüːsotɨn.</ta>
            <ta e="T17" id="Seg_511" s="T15">maːtɨm meːqolʼowsotɨn.</ta>
            <ta e="T23" id="Seg_512" s="T17">maːt meːlʼe turtukulʼa puːlʼa prikašʼima tüːsona.</ta>
            <ta e="T26" id="Seg_513" s="T23">aj tantalʼkutij imman.</ta>
            <ta e="T28" id="Seg_514" s="T26">mi tantalʼtukokolʼowsow[m]ɨn.</ta>
            <ta e="T35" id="Seg_515" s="T28">tantalʼklʼa puːlʼa tuːrtukusah kekkɨssa naːkɨr klʼass končilʼisa.</ta>
            <ta e="T40" id="Seg_516" s="T35">šitɨ pondə kunda moqɨlʼlʼän wärkɨsaŋ.</ta>
            <ta e="T43" id="Seg_517" s="T40">puːn irra qoːssah.</ta>
            <ta e="T49" id="Seg_518" s="T43">ukkur nätalʼem eːsanä aj ukkur iːjalʼam.</ta>
            <ta e="T54" id="Seg_519" s="T49">na iːjalʼa küːttälʼa ilʼlʼa kuːsanä.</ta>
            <ta e="T57" id="Seg_520" s="T54">kət čandɨlʼ q[k]otaqɨnɨ.</ta>
            <ta e="T60" id="Seg_521" s="T57">polʼqorsa meːsotɨn.</ta>
            <ta e="T66" id="Seg_522" s="T60">säːrɨ qampisa aj säq[q]ä qampisa tamtɨlʼsoːtɨn.</ta>
            <ta e="T70" id="Seg_523" s="T66">štälʼ qəntisotɨn qumɨlʼ mʼaːktändɨ.</ta>
            <ta e="T77" id="Seg_524" s="T70">nače qondɨlʼa nɨ tottɨsotɨn əːsɨntɨ tüːkɨtəttɨ.</ta>
            <ta e="T82" id="Seg_525" s="T77">əsɨtɨ tüːlʼlʼa puːlʼa ilʼlʼa mesoːtɨn.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_526" s="T0">Man ilisak Tolʼkaqɨn. </ta>
            <ta e="T8" id="Seg_527" s="T3">Tɨmta meːl čʼäːŋkɨsan rušilʼ mɔːn. </ta>
            <ta e="T11" id="Seg_528" s="T8">Šölʼqumɨlʼ mɔːssa ilesɔːtɨn. </ta>
            <ta e="T15" id="Seg_529" s="T11">Puːn štälla qumɨt tüsɔːtɨn. </ta>
            <ta e="T17" id="Seg_530" s="T15">Mɔːtɨm meːqolowsɔːtɨn. </ta>
            <ta e="T23" id="Seg_531" s="T17">Mɔːt meːlä tuːrtukula puːla prikašʼima tüsona. </ta>
            <ta e="T26" id="Seg_532" s="T23">Aj tantalkutij imman. </ta>
            <ta e="T28" id="Seg_533" s="T26">Mi tantaltukkoqolowsɔːmɨn. </ta>
            <ta e="T35" id="Seg_534" s="T28">Tantalkla puːla tuːrtukusak kekkɨssa nɔːkɨr klass končʼilisa. </ta>
            <ta e="T40" id="Seg_535" s="T35">Šitɨ poːntə kunta moqɨllän wärkɨsaŋ. </ta>
            <ta e="T43" id="Seg_536" s="T40">Puːn irra qossak. </ta>
            <ta e="T49" id="Seg_537" s="T43">Ukkur nätalʼem ɛːsanä aj ukkur iːjalʼam. </ta>
            <ta e="T54" id="Seg_538" s="T49">Na iːjalʼa qüːttäla ılla qusanä. </ta>
            <ta e="T57" id="Seg_539" s="T54">Kət čʼantɨlʼ kotaqɨnɨ. </ta>
            <ta e="T60" id="Seg_540" s="T57">Poːlʼ qorsa meːsɔːtɨn. </ta>
            <ta e="T66" id="Seg_541" s="T60">Säːrɨ qampısa aj säːqä qampısa tamtɨlsɔːtɨn. </ta>
            <ta e="T70" id="Seg_542" s="T66">Stälʼ qəntisɔːtɨn qumɨlʼ mʼaːktäntɨ. </ta>
            <ta e="T77" id="Seg_543" s="T70">Načʼä qontɨlʼa nɨː tottɨsɔːtɨn əsɨntɨ tükɨ təttɨ. </ta>
            <ta e="T82" id="Seg_544" s="T77">Əsɨtɨ tülla puːla ılla meːsɔːtɨn. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_545" s="T0">man</ta>
            <ta e="T2" id="Seg_546" s="T1">ili-sa-k</ta>
            <ta e="T3" id="Seg_547" s="T2">tolʼka-qɨn</ta>
            <ta e="T4" id="Seg_548" s="T3">tɨmta</ta>
            <ta e="T5" id="Seg_549" s="T4">meːl</ta>
            <ta e="T6" id="Seg_550" s="T5">čʼäːŋkɨ-sa-n</ta>
            <ta e="T7" id="Seg_551" s="T6">ruš-i-lʼ</ta>
            <ta e="T8" id="Seg_552" s="T7">mɔːn</ta>
            <ta e="T9" id="Seg_553" s="T8">šölʼqum-ɨ-lʼ</ta>
            <ta e="T10" id="Seg_554" s="T9">mɔːs-sa</ta>
            <ta e="T11" id="Seg_555" s="T10">ile-sɔː-tɨn</ta>
            <ta e="T12" id="Seg_556" s="T11">puːn</ta>
            <ta e="T13" id="Seg_557" s="T12">štälla</ta>
            <ta e="T14" id="Seg_558" s="T13">qum-ɨ-t</ta>
            <ta e="T15" id="Seg_559" s="T14">tü-sɔː-tɨn</ta>
            <ta e="T16" id="Seg_560" s="T15">mɔːt-ɨ-m</ta>
            <ta e="T17" id="Seg_561" s="T16">meː-q-olow-sɔː-tɨn</ta>
            <ta e="T18" id="Seg_562" s="T17">mɔːt</ta>
            <ta e="T19" id="Seg_563" s="T18">meː-lä</ta>
            <ta e="T20" id="Seg_564" s="T19">tuːr-tu-ku-la</ta>
            <ta e="T21" id="Seg_565" s="T20">puːla</ta>
            <ta e="T22" id="Seg_566" s="T21">prikašʼima</ta>
            <ta e="T23" id="Seg_567" s="T22">tü-so-na</ta>
            <ta e="T24" id="Seg_568" s="T23">aj</ta>
            <ta e="T25" id="Seg_569" s="T24">tant-al-ku-tij</ta>
            <ta e="T26" id="Seg_570" s="T25">imma-n</ta>
            <ta e="T27" id="Seg_571" s="T26">mi</ta>
            <ta e="T28" id="Seg_572" s="T27">tan-taltu-kko-q-olow-sɔː-mɨn</ta>
            <ta e="T29" id="Seg_573" s="T28">tant-al-k-la</ta>
            <ta e="T30" id="Seg_574" s="T29">puːla</ta>
            <ta e="T31" id="Seg_575" s="T30">tuːr-tu-ku-sa-k</ta>
            <ta e="T32" id="Seg_576" s="T31">kekkɨssa</ta>
            <ta e="T33" id="Seg_577" s="T32">nɔːkɨr</ta>
            <ta e="T34" id="Seg_578" s="T33">klass</ta>
            <ta e="T35" id="Seg_579" s="T34">končʼi-li-sa</ta>
            <ta e="T36" id="Seg_580" s="T35">šitɨ</ta>
            <ta e="T37" id="Seg_581" s="T36">poː-n-tə</ta>
            <ta e="T38" id="Seg_582" s="T37">kunta</ta>
            <ta e="T39" id="Seg_583" s="T38">moqɨllä-n</ta>
            <ta e="T40" id="Seg_584" s="T39">wärkɨ-sa-ŋ</ta>
            <ta e="T41" id="Seg_585" s="T40">puːn</ta>
            <ta e="T42" id="Seg_586" s="T41">irra</ta>
            <ta e="T43" id="Seg_587" s="T42">qo-ssa-k</ta>
            <ta e="T44" id="Seg_588" s="T43">ukkur</ta>
            <ta e="T45" id="Seg_589" s="T44">näta-lʼe-m</ta>
            <ta e="T46" id="Seg_590" s="T45">ɛː-sa-nä</ta>
            <ta e="T47" id="Seg_591" s="T46">aj</ta>
            <ta e="T48" id="Seg_592" s="T47">ukkur</ta>
            <ta e="T49" id="Seg_593" s="T48">iːja-lʼa-m</ta>
            <ta e="T50" id="Seg_594" s="T49">na</ta>
            <ta e="T51" id="Seg_595" s="T50">iːja-lʼa</ta>
            <ta e="T52" id="Seg_596" s="T51">qüːt-tä-la</ta>
            <ta e="T53" id="Seg_597" s="T52">ılla</ta>
            <ta e="T54" id="Seg_598" s="T53">qu-sa-nä</ta>
            <ta e="T55" id="Seg_599" s="T54">kə-t</ta>
            <ta e="T56" id="Seg_600" s="T55">čʼantɨ-lʼ</ta>
            <ta e="T57" id="Seg_601" s="T56">kota-qɨnɨ</ta>
            <ta e="T58" id="Seg_602" s="T57">poː-lʼ</ta>
            <ta e="T59" id="Seg_603" s="T58">qor-sa</ta>
            <ta e="T60" id="Seg_604" s="T59">meː-sɔː-tɨn</ta>
            <ta e="T61" id="Seg_605" s="T60">säːrɨ</ta>
            <ta e="T62" id="Seg_606" s="T61">qampı-sa</ta>
            <ta e="T63" id="Seg_607" s="T62">aj</ta>
            <ta e="T64" id="Seg_608" s="T63">säːqä</ta>
            <ta e="T65" id="Seg_609" s="T64">qampı-sa</ta>
            <ta e="T66" id="Seg_610" s="T65">tamtɨl-sɔː-tɨn</ta>
            <ta e="T67" id="Seg_611" s="T66">stälʼ</ta>
            <ta e="T68" id="Seg_612" s="T67">qən-ti-sɔː-tɨn</ta>
            <ta e="T69" id="Seg_613" s="T68">qum-ɨ-lʼ</ta>
            <ta e="T70" id="Seg_614" s="T69">mʼaːktä-ntɨ</ta>
            <ta e="T71" id="Seg_615" s="T70">načʼä</ta>
            <ta e="T72" id="Seg_616" s="T71">qon-tɨ-lʼa</ta>
            <ta e="T73" id="Seg_617" s="T72">nɨː</ta>
            <ta e="T74" id="Seg_618" s="T73">tottɨ-sɔː-tɨn</ta>
            <ta e="T75" id="Seg_619" s="T74">əsɨ-n-tɨ</ta>
            <ta e="T76" id="Seg_620" s="T75">tü-kɨ</ta>
            <ta e="T77" id="Seg_621" s="T76">təttɨ</ta>
            <ta e="T78" id="Seg_622" s="T77">əsɨ-tɨ</ta>
            <ta e="T79" id="Seg_623" s="T78">tü-lla</ta>
            <ta e="T80" id="Seg_624" s="T79">puːla</ta>
            <ta e="T81" id="Seg_625" s="T80">ılla</ta>
            <ta e="T82" id="Seg_626" s="T81">meː-sɔː-tɨn</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_627" s="T0">man</ta>
            <ta e="T2" id="Seg_628" s="T1">ilɨ-sɨ-k</ta>
            <ta e="T3" id="Seg_629" s="T2">Tolʼka-qɨn</ta>
            <ta e="T4" id="Seg_630" s="T3">tɨmtɨ</ta>
            <ta e="T5" id="Seg_631" s="T4">meːltɨ</ta>
            <ta e="T6" id="Seg_632" s="T5">čʼäːŋkɨ-sɨ-n</ta>
            <ta e="T7" id="Seg_633" s="T6">ruš-ɨ-lʼ</ta>
            <ta e="T8" id="Seg_634" s="T7">mɔːt</ta>
            <ta e="T9" id="Seg_635" s="T8">šölʼqum-ɨ-lʼ</ta>
            <ta e="T10" id="Seg_636" s="T9">mɔːt-sä</ta>
            <ta e="T11" id="Seg_637" s="T10">ilɨ-sɨ-tɨt</ta>
            <ta e="T12" id="Seg_638" s="T11">puːn</ta>
            <ta e="T13" id="Seg_639" s="T12">šittälʼ</ta>
            <ta e="T14" id="Seg_640" s="T13">qum-ɨ-t</ta>
            <ta e="T15" id="Seg_641" s="T14">tü-sɨ-tɨt</ta>
            <ta e="T16" id="Seg_642" s="T15">mɔːt-ɨ-m</ta>
            <ta e="T17" id="Seg_643" s="T16">meː-qo-olam-sɨ-tɨt</ta>
            <ta e="T18" id="Seg_644" s="T17">mɔːt</ta>
            <ta e="T19" id="Seg_645" s="T18">meː-lä</ta>
            <ta e="T20" id="Seg_646" s="T19">tuːrıː-tɨ-kkɨ-lä</ta>
            <ta e="T21" id="Seg_647" s="T20">puːlä</ta>
            <ta e="T22" id="Seg_648" s="T21">prikašʼima</ta>
            <ta e="T23" id="Seg_649" s="T22">tü-sɨ-n</ta>
            <ta e="T24" id="Seg_650" s="T23">aj</ta>
            <ta e="T25" id="Seg_651" s="T24">tantɨ-alʼ-kkɨ-ntɨlʼ</ta>
            <ta e="T26" id="Seg_652" s="T25">ima-t</ta>
            <ta e="T27" id="Seg_653" s="T26">meː</ta>
            <ta e="T28" id="Seg_654" s="T27">tantɨ-altɨ-kkɨ-qo-olam-sɨ-mɨt</ta>
            <ta e="T29" id="Seg_655" s="T28">tantɨ-alʼ-kkɨ-lä</ta>
            <ta e="T30" id="Seg_656" s="T29">puːlä</ta>
            <ta e="T31" id="Seg_657" s="T30">tuːrıː-tɨ-kkɨ-sɨ-k</ta>
            <ta e="T32" id="Seg_658" s="T31">kekkɨsä</ta>
            <ta e="T33" id="Seg_659" s="T32">nɔːkɨr</ta>
            <ta e="T34" id="Seg_660" s="T33">klass</ta>
            <ta e="T35" id="Seg_661" s="T34">končʼi-lɨ-sɨ</ta>
            <ta e="T36" id="Seg_662" s="T35">šittɨ</ta>
            <ta e="T37" id="Seg_663" s="T36">poː-n-tɨ</ta>
            <ta e="T38" id="Seg_664" s="T37">kuntɨ</ta>
            <ta e="T39" id="Seg_665" s="T38">moqɨnä-n</ta>
            <ta e="T40" id="Seg_666" s="T39">wərkɨ-sɨ-k</ta>
            <ta e="T41" id="Seg_667" s="T40">puːn</ta>
            <ta e="T42" id="Seg_668" s="T41">ira</ta>
            <ta e="T43" id="Seg_669" s="T42">qo-sɨ-k</ta>
            <ta e="T44" id="Seg_670" s="T43">ukkɨr</ta>
            <ta e="T45" id="Seg_671" s="T44">nätäk-lʼa-mɨ</ta>
            <ta e="T46" id="Seg_672" s="T45">ɛː-sɨ-n</ta>
            <ta e="T47" id="Seg_673" s="T46">aj</ta>
            <ta e="T48" id="Seg_674" s="T47">ukkɨr</ta>
            <ta e="T49" id="Seg_675" s="T48">iːja-lʼa-mɨ</ta>
            <ta e="T50" id="Seg_676" s="T49">na</ta>
            <ta e="T51" id="Seg_677" s="T50">iːja-lʼa</ta>
            <ta e="T52" id="Seg_678" s="T51">qüːtɨ-tɨ-lä</ta>
            <ta e="T53" id="Seg_679" s="T52">ıllä</ta>
            <ta e="T54" id="Seg_680" s="T53">qu-sɨ-n</ta>
            <ta e="T55" id="Seg_681" s="T54">kə-n</ta>
            <ta e="T56" id="Seg_682" s="T55">čʼontɨ-lʼ</ta>
            <ta e="T57" id="Seg_683" s="T56">kotä-qɨnɨ</ta>
            <ta e="T58" id="Seg_684" s="T57">poː-lʼ</ta>
            <ta e="T59" id="Seg_685" s="T58">qor-sä</ta>
            <ta e="T60" id="Seg_686" s="T59">meː-sɨ-tɨt</ta>
            <ta e="T61" id="Seg_687" s="T60">sərɨ</ta>
            <ta e="T62" id="Seg_688" s="T61">qampı-sä</ta>
            <ta e="T63" id="Seg_689" s="T62">aj</ta>
            <ta e="T64" id="Seg_690" s="T63">säːqɨ</ta>
            <ta e="T65" id="Seg_691" s="T64">qampı-sä</ta>
            <ta e="T66" id="Seg_692" s="T65">tamtɨl-sɨ-tɨt</ta>
            <ta e="T67" id="Seg_693" s="T66">šittälʼ</ta>
            <ta e="T68" id="Seg_694" s="T67">qən-tɨ-sɨ-tɨt</ta>
            <ta e="T69" id="Seg_695" s="T68">qum-ɨ-lʼ</ta>
            <ta e="T70" id="Seg_696" s="T69">mɛktɨ-ntɨ</ta>
            <ta e="T71" id="Seg_697" s="T70">näčʼčʼä</ta>
            <ta e="T72" id="Seg_698" s="T71">qən-tɨ-lä</ta>
            <ta e="T73" id="Seg_699" s="T72">nɨː</ta>
            <ta e="T74" id="Seg_700" s="T73">tottɨ-sɨ-tɨt</ta>
            <ta e="T75" id="Seg_701" s="T74">əsɨ-n-tɨ</ta>
            <ta e="T76" id="Seg_702" s="T75">tü-ku</ta>
            <ta e="T77" id="Seg_703" s="T76">təttɨ</ta>
            <ta e="T78" id="Seg_704" s="T77">əsɨ-tɨ</ta>
            <ta e="T79" id="Seg_705" s="T78">tü-lä</ta>
            <ta e="T80" id="Seg_706" s="T79">puːlä</ta>
            <ta e="T81" id="Seg_707" s="T80">ıllä</ta>
            <ta e="T82" id="Seg_708" s="T81">meː-sɨ-tɨt</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_709" s="T0">I.NOM</ta>
            <ta e="T2" id="Seg_710" s="T1">live-PST-1SG.S</ta>
            <ta e="T3" id="Seg_711" s="T2">Tolka-LOC</ta>
            <ta e="T4" id="Seg_712" s="T3">here</ta>
            <ta e="T5" id="Seg_713" s="T4">always</ta>
            <ta e="T6" id="Seg_714" s="T5">NEG.EX-PST-3SG.S</ta>
            <ta e="T7" id="Seg_715" s="T6">Russian-EP-ADJZ</ta>
            <ta e="T8" id="Seg_716" s="T7">house.[NOM]</ta>
            <ta e="T9" id="Seg_717" s="T8">Selkup-EP-ADJZ</ta>
            <ta e="T10" id="Seg_718" s="T9">tent-INSTR</ta>
            <ta e="T11" id="Seg_719" s="T10">live-PST-3PL</ta>
            <ta e="T12" id="Seg_720" s="T11">afterwards</ta>
            <ta e="T13" id="Seg_721" s="T12">then</ta>
            <ta e="T14" id="Seg_722" s="T13">human.being-EP-PL.[NOM]</ta>
            <ta e="T15" id="Seg_723" s="T14">come-PST-3PL</ta>
            <ta e="T16" id="Seg_724" s="T15">tent-EP-ACC</ta>
            <ta e="T17" id="Seg_725" s="T16">make-INF-begin-PST-3PL</ta>
            <ta e="T18" id="Seg_726" s="T17">house.[NOM]</ta>
            <ta e="T19" id="Seg_727" s="T18">make-CVB</ta>
            <ta e="T20" id="Seg_728" s="T19">be.over-TR-HAB-CVB</ta>
            <ta e="T21" id="Seg_729" s="T20">after</ta>
            <ta e="T22" id="Seg_730" s="T21">saleswoman.[NOM]</ta>
            <ta e="T23" id="Seg_731" s="T22">come-PST-3SG.S</ta>
            <ta e="T24" id="Seg_732" s="T23">and</ta>
            <ta e="T25" id="Seg_733" s="T24">teach-INCH-HAB-PTCP.PRS</ta>
            <ta e="T26" id="Seg_734" s="T25">woman-PL.[NOM]</ta>
            <ta e="T27" id="Seg_735" s="T26">we.PL.NOM</ta>
            <ta e="T28" id="Seg_736" s="T27">study-CAUS-HAB-INF-begin-PST-1PL</ta>
            <ta e="T29" id="Seg_737" s="T28">teach-INCH-HAB-CVB</ta>
            <ta e="T30" id="Seg_738" s="T29">after</ta>
            <ta e="T31" id="Seg_739" s="T30">be.over-TR-HAB-PST-1SG.S</ta>
            <ta e="T32" id="Seg_740" s="T31">hardly</ta>
            <ta e="T33" id="Seg_741" s="T32">three</ta>
            <ta e="T34" id="Seg_742" s="T33">class.[NOM]</ta>
            <ta e="T35" id="Seg_743" s="T34">finish-RES-PST.[3SG.S]</ta>
            <ta e="T36" id="Seg_744" s="T35">two</ta>
            <ta e="T37" id="Seg_745" s="T36">year-GEN-3SG</ta>
            <ta e="T38" id="Seg_746" s="T37">during</ta>
            <ta e="T39" id="Seg_747" s="T38">home-ADV.LOC</ta>
            <ta e="T40" id="Seg_748" s="T39">live-PST-1SG.S</ta>
            <ta e="T41" id="Seg_749" s="T40">afterwards</ta>
            <ta e="T42" id="Seg_750" s="T41">husband.[NOM]</ta>
            <ta e="T43" id="Seg_751" s="T42">find-PST-1SG.S</ta>
            <ta e="T44" id="Seg_752" s="T43">one</ta>
            <ta e="T45" id="Seg_753" s="T44">daughter-DIM.[NOM]-1SG</ta>
            <ta e="T46" id="Seg_754" s="T45">be-PST-3SG.S</ta>
            <ta e="T47" id="Seg_755" s="T46">and</ta>
            <ta e="T48" id="Seg_756" s="T47">one</ta>
            <ta e="T49" id="Seg_757" s="T48">son-DIM.[NOM]-1SG</ta>
            <ta e="T50" id="Seg_758" s="T49">this</ta>
            <ta e="T51" id="Seg_759" s="T50">son-DIM.[NOM]</ta>
            <ta e="T52" id="Seg_760" s="T51">be.sick-%%-CVB</ta>
            <ta e="T53" id="Seg_761" s="T52">down</ta>
            <ta e="T54" id="Seg_762" s="T53">die-PST-3SG.S</ta>
            <ta e="T55" id="Seg_763" s="T54">winter-GEN</ta>
            <ta e="T56" id="Seg_764" s="T55">in.the.middle-ADJZ</ta>
            <ta e="T57" id="Seg_765" s="T56">interval-EL</ta>
            <ta e="T58" id="Seg_766" s="T57">tree-ADJZ</ta>
            <ta e="T59" id="Seg_767" s="T58">cover-INSTR</ta>
            <ta e="T60" id="Seg_768" s="T59">make-PST-3PL</ta>
            <ta e="T61" id="Seg_769" s="T60">white</ta>
            <ta e="T62" id="Seg_770" s="T61">headscarf-INSTR</ta>
            <ta e="T63" id="Seg_771" s="T62">and</ta>
            <ta e="T64" id="Seg_772" s="T63">black</ta>
            <ta e="T65" id="Seg_773" s="T64">headscarf-INSTR</ta>
            <ta e="T66" id="Seg_774" s="T65">wrap-PST-3PL</ta>
            <ta e="T67" id="Seg_775" s="T66">then</ta>
            <ta e="T68" id="Seg_776" s="T67">leave-TR-PST-3PL</ta>
            <ta e="T69" id="Seg_777" s="T68">human.being-EP-ADJZ</ta>
            <ta e="T70" id="Seg_778" s="T69">hill-ILL</ta>
            <ta e="T71" id="Seg_779" s="T70">there</ta>
            <ta e="T72" id="Seg_780" s="T71">leave-TR-CVB</ta>
            <ta e="T73" id="Seg_781" s="T72">there</ta>
            <ta e="T74" id="Seg_782" s="T73">put-PST-3PL</ta>
            <ta e="T75" id="Seg_783" s="T74">father-GEN-3SG</ta>
            <ta e="T76" id="Seg_784" s="T75">come-TEMPN.[NOM]</ta>
            <ta e="T77" id="Seg_785" s="T76">up.to</ta>
            <ta e="T78" id="Seg_786" s="T77">father.[NOM]-3SG</ta>
            <ta e="T79" id="Seg_787" s="T78">come-CVB</ta>
            <ta e="T80" id="Seg_788" s="T79">after</ta>
            <ta e="T81" id="Seg_789" s="T80">down</ta>
            <ta e="T82" id="Seg_790" s="T81">make-PST-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_791" s="T0">я.NOM</ta>
            <ta e="T2" id="Seg_792" s="T1">жить-PST-1SG.S</ta>
            <ta e="T3" id="Seg_793" s="T2">Толька-LOC</ta>
            <ta e="T4" id="Seg_794" s="T3">здесь</ta>
            <ta e="T5" id="Seg_795" s="T4">всегда</ta>
            <ta e="T6" id="Seg_796" s="T5">NEG.EX-PST-3SG.S</ta>
            <ta e="T7" id="Seg_797" s="T6">русский-EP-ADJZ</ta>
            <ta e="T8" id="Seg_798" s="T7">дом.[NOM]</ta>
            <ta e="T9" id="Seg_799" s="T8">селькуп-EP-ADJZ</ta>
            <ta e="T10" id="Seg_800" s="T9">чум-INSTR</ta>
            <ta e="T11" id="Seg_801" s="T10">жить-PST-3PL</ta>
            <ta e="T12" id="Seg_802" s="T11">потом</ta>
            <ta e="T13" id="Seg_803" s="T12">потом</ta>
            <ta e="T14" id="Seg_804" s="T13">человек-EP-PL.[NOM]</ta>
            <ta e="T15" id="Seg_805" s="T14">прийти-PST-3PL</ta>
            <ta e="T16" id="Seg_806" s="T15">чум-EP-ACC</ta>
            <ta e="T17" id="Seg_807" s="T16">сделать-INF-начать-PST-3PL</ta>
            <ta e="T18" id="Seg_808" s="T17">дом.[NOM]</ta>
            <ta e="T19" id="Seg_809" s="T18">сделать-CVB</ta>
            <ta e="T20" id="Seg_810" s="T19">закончиться-TR-HAB-CVB</ta>
            <ta e="T21" id="Seg_811" s="T20">после</ta>
            <ta e="T22" id="Seg_812" s="T21">продавщица.[NOM]</ta>
            <ta e="T23" id="Seg_813" s="T22">прийти-PST-3SG.S</ta>
            <ta e="T24" id="Seg_814" s="T23">и</ta>
            <ta e="T25" id="Seg_815" s="T24">учить-INCH-HAB-PTCP.PRS</ta>
            <ta e="T26" id="Seg_816" s="T25">женщина-PL.[NOM]</ta>
            <ta e="T27" id="Seg_817" s="T26">мы.PL.NOM</ta>
            <ta e="T28" id="Seg_818" s="T27">учиться-CAUS-HAB-INF-начать-PST-1PL</ta>
            <ta e="T29" id="Seg_819" s="T28">учить-INCH-HAB-CVB</ta>
            <ta e="T30" id="Seg_820" s="T29">после</ta>
            <ta e="T31" id="Seg_821" s="T30">закончиться-TR-HAB-PST-1SG.S</ta>
            <ta e="T32" id="Seg_822" s="T31">едва</ta>
            <ta e="T33" id="Seg_823" s="T32">три</ta>
            <ta e="T34" id="Seg_824" s="T33">класс.[NOM]</ta>
            <ta e="T35" id="Seg_825" s="T34">закончить-RES-PST.[3SG.S]</ta>
            <ta e="T36" id="Seg_826" s="T35">два</ta>
            <ta e="T37" id="Seg_827" s="T36">год-GEN-3SG</ta>
            <ta e="T38" id="Seg_828" s="T37">в.течение</ta>
            <ta e="T39" id="Seg_829" s="T38">домой-ADV.LOC</ta>
            <ta e="T40" id="Seg_830" s="T39">находиться-PST-1SG.S</ta>
            <ta e="T41" id="Seg_831" s="T40">потом</ta>
            <ta e="T42" id="Seg_832" s="T41">муж.[NOM]</ta>
            <ta e="T43" id="Seg_833" s="T42">находить-PST-1SG.S</ta>
            <ta e="T44" id="Seg_834" s="T43">один</ta>
            <ta e="T45" id="Seg_835" s="T44">дочь-DIM.[NOM]-1SG</ta>
            <ta e="T46" id="Seg_836" s="T45">быть-PST-3SG.S</ta>
            <ta e="T47" id="Seg_837" s="T46">и</ta>
            <ta e="T48" id="Seg_838" s="T47">один</ta>
            <ta e="T49" id="Seg_839" s="T48">сын-DIM.[NOM]-1SG</ta>
            <ta e="T50" id="Seg_840" s="T49">этот</ta>
            <ta e="T51" id="Seg_841" s="T50">сын-DIM.[NOM]</ta>
            <ta e="T52" id="Seg_842" s="T51">болеть-%%-CVB</ta>
            <ta e="T53" id="Seg_843" s="T52">вниз</ta>
            <ta e="T54" id="Seg_844" s="T53">умереть-PST-3SG.S</ta>
            <ta e="T55" id="Seg_845" s="T54">зима-GEN</ta>
            <ta e="T56" id="Seg_846" s="T55">посередине-ADJZ</ta>
            <ta e="T57" id="Seg_847" s="T56">промежуток-EL</ta>
            <ta e="T58" id="Seg_848" s="T57">дерево-ADJZ</ta>
            <ta e="T59" id="Seg_849" s="T58">оболочка-INSTR</ta>
            <ta e="T60" id="Seg_850" s="T59">сделать-PST-3PL</ta>
            <ta e="T61" id="Seg_851" s="T60">белый</ta>
            <ta e="T62" id="Seg_852" s="T61">платок-INSTR</ta>
            <ta e="T63" id="Seg_853" s="T62">и</ta>
            <ta e="T64" id="Seg_854" s="T63">чёрный</ta>
            <ta e="T65" id="Seg_855" s="T64">платок-INSTR</ta>
            <ta e="T66" id="Seg_856" s="T65">завернуть-PST-3PL</ta>
            <ta e="T67" id="Seg_857" s="T66">потом</ta>
            <ta e="T68" id="Seg_858" s="T67">отправиться-TR-PST-3PL</ta>
            <ta e="T69" id="Seg_859" s="T68">человек-EP-ADJZ</ta>
            <ta e="T70" id="Seg_860" s="T69">холм-ILL</ta>
            <ta e="T71" id="Seg_861" s="T70">туда</ta>
            <ta e="T72" id="Seg_862" s="T71">отправиться-TR-CVB</ta>
            <ta e="T73" id="Seg_863" s="T72">туда</ta>
            <ta e="T74" id="Seg_864" s="T73">положить-PST-3PL</ta>
            <ta e="T75" id="Seg_865" s="T74">отец-GEN-3SG</ta>
            <ta e="T76" id="Seg_866" s="T75">прийти-TEMPN.[NOM]</ta>
            <ta e="T77" id="Seg_867" s="T76">до</ta>
            <ta e="T78" id="Seg_868" s="T77">отец.[NOM]-3SG</ta>
            <ta e="T79" id="Seg_869" s="T78">прийти-CVB</ta>
            <ta e="T80" id="Seg_870" s="T79">после</ta>
            <ta e="T81" id="Seg_871" s="T80">вниз</ta>
            <ta e="T82" id="Seg_872" s="T81">сделать-PST-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_873" s="T0">pers</ta>
            <ta e="T2" id="Seg_874" s="T1">v-v:tense-v:pn</ta>
            <ta e="T3" id="Seg_875" s="T2">nprop-n:case</ta>
            <ta e="T4" id="Seg_876" s="T3">adv</ta>
            <ta e="T5" id="Seg_877" s="T4">adv</ta>
            <ta e="T6" id="Seg_878" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_879" s="T6">n-n:ins-n&gt;adj</ta>
            <ta e="T8" id="Seg_880" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_881" s="T8">n-n:ins-n&gt;adj</ta>
            <ta e="T10" id="Seg_882" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_883" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_884" s="T11">adv</ta>
            <ta e="T13" id="Seg_885" s="T12">adv</ta>
            <ta e="T14" id="Seg_886" s="T13">n-n:ins-n:num-n:case</ta>
            <ta e="T15" id="Seg_887" s="T14">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_888" s="T15">n-n:ins-n:case</ta>
            <ta e="T17" id="Seg_889" s="T16">v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_890" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_891" s="T18">v-v&gt;adv</ta>
            <ta e="T20" id="Seg_892" s="T19">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T21" id="Seg_893" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_894" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_895" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_896" s="T23">conj</ta>
            <ta e="T25" id="Seg_897" s="T24">v-v&gt;v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T26" id="Seg_898" s="T25">n-n:num-n:case</ta>
            <ta e="T27" id="Seg_899" s="T26">pers</ta>
            <ta e="T28" id="Seg_900" s="T27">v-v&gt;v-v&gt;v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_901" s="T28">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T30" id="Seg_902" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_903" s="T30">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_904" s="T31">adv</ta>
            <ta e="T33" id="Seg_905" s="T32">num</ta>
            <ta e="T34" id="Seg_906" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_907" s="T34">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_908" s="T35">num</ta>
            <ta e="T37" id="Seg_909" s="T36">n-n:case-n:poss</ta>
            <ta e="T38" id="Seg_910" s="T37">pp</ta>
            <ta e="T39" id="Seg_911" s="T38">adv-adv:case</ta>
            <ta e="T40" id="Seg_912" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_913" s="T40">adv</ta>
            <ta e="T42" id="Seg_914" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_915" s="T42">v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_916" s="T43">num</ta>
            <ta e="T45" id="Seg_917" s="T44">n-n&gt;n-n:case-n:poss</ta>
            <ta e="T46" id="Seg_918" s="T45">v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_919" s="T46">conj</ta>
            <ta e="T48" id="Seg_920" s="T47">num</ta>
            <ta e="T49" id="Seg_921" s="T48">n-n&gt;n-n:case-n:poss</ta>
            <ta e="T50" id="Seg_922" s="T49">dem</ta>
            <ta e="T51" id="Seg_923" s="T50">n-n&gt;n-n:case</ta>
            <ta e="T52" id="Seg_924" s="T51">v-v&gt;v-v&gt;adv</ta>
            <ta e="T53" id="Seg_925" s="T52">preverb</ta>
            <ta e="T54" id="Seg_926" s="T53">v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_927" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_928" s="T55">pp-n&gt;adj</ta>
            <ta e="T57" id="Seg_929" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_930" s="T57">n-n&gt;adj</ta>
            <ta e="T59" id="Seg_931" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_932" s="T59">v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_933" s="T60">adj</ta>
            <ta e="T62" id="Seg_934" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_935" s="T62">conj</ta>
            <ta e="T64" id="Seg_936" s="T63">adj</ta>
            <ta e="T65" id="Seg_937" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_938" s="T65">v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_939" s="T66">adv</ta>
            <ta e="T68" id="Seg_940" s="T67">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_941" s="T68">n-n:ins-n&gt;adj</ta>
            <ta e="T70" id="Seg_942" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_943" s="T70">adv</ta>
            <ta e="T72" id="Seg_944" s="T71">v-v&gt;v-v&gt;adv</ta>
            <ta e="T73" id="Seg_945" s="T72">adv</ta>
            <ta e="T74" id="Seg_946" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_947" s="T74">n-n:case-n:poss</ta>
            <ta e="T76" id="Seg_948" s="T75">v-v&gt;n-n:case</ta>
            <ta e="T77" id="Seg_949" s="T76">pp</ta>
            <ta e="T78" id="Seg_950" s="T77">n-n:case-n:poss</ta>
            <ta e="T79" id="Seg_951" s="T78">v-v&gt;adv</ta>
            <ta e="T80" id="Seg_952" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_953" s="T80">preverb</ta>
            <ta e="T82" id="Seg_954" s="T81">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_955" s="T0">pers</ta>
            <ta e="T2" id="Seg_956" s="T1">v</ta>
            <ta e="T3" id="Seg_957" s="T2">nprop</ta>
            <ta e="T4" id="Seg_958" s="T3">adv</ta>
            <ta e="T5" id="Seg_959" s="T4">adv</ta>
            <ta e="T6" id="Seg_960" s="T5">v</ta>
            <ta e="T7" id="Seg_961" s="T6">adj</ta>
            <ta e="T8" id="Seg_962" s="T7">n</ta>
            <ta e="T9" id="Seg_963" s="T8">adj</ta>
            <ta e="T10" id="Seg_964" s="T9">n</ta>
            <ta e="T11" id="Seg_965" s="T10">v</ta>
            <ta e="T12" id="Seg_966" s="T11">adv</ta>
            <ta e="T13" id="Seg_967" s="T12">adv</ta>
            <ta e="T14" id="Seg_968" s="T13">n</ta>
            <ta e="T15" id="Seg_969" s="T14">v</ta>
            <ta e="T16" id="Seg_970" s="T15">n</ta>
            <ta e="T17" id="Seg_971" s="T16">v</ta>
            <ta e="T18" id="Seg_972" s="T17">n</ta>
            <ta e="T19" id="Seg_973" s="T18">adv</ta>
            <ta e="T20" id="Seg_974" s="T19">adv</ta>
            <ta e="T21" id="Seg_975" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_976" s="T21">n</ta>
            <ta e="T23" id="Seg_977" s="T22">v</ta>
            <ta e="T24" id="Seg_978" s="T23">conj</ta>
            <ta e="T25" id="Seg_979" s="T24">ptcp</ta>
            <ta e="T26" id="Seg_980" s="T25">n</ta>
            <ta e="T27" id="Seg_981" s="T26">pers</ta>
            <ta e="T28" id="Seg_982" s="T27">v</ta>
            <ta e="T29" id="Seg_983" s="T28">adv</ta>
            <ta e="T30" id="Seg_984" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_985" s="T30">v</ta>
            <ta e="T32" id="Seg_986" s="T31">adv</ta>
            <ta e="T33" id="Seg_987" s="T32">num</ta>
            <ta e="T34" id="Seg_988" s="T33">n</ta>
            <ta e="T35" id="Seg_989" s="T34">v</ta>
            <ta e="T36" id="Seg_990" s="T35">num</ta>
            <ta e="T37" id="Seg_991" s="T36">n</ta>
            <ta e="T38" id="Seg_992" s="T37">pp</ta>
            <ta e="T39" id="Seg_993" s="T38">adv</ta>
            <ta e="T40" id="Seg_994" s="T39">v</ta>
            <ta e="T41" id="Seg_995" s="T40">adv</ta>
            <ta e="T42" id="Seg_996" s="T41">n</ta>
            <ta e="T43" id="Seg_997" s="T42">v</ta>
            <ta e="T44" id="Seg_998" s="T43">num</ta>
            <ta e="T45" id="Seg_999" s="T44">n</ta>
            <ta e="T46" id="Seg_1000" s="T45">v</ta>
            <ta e="T47" id="Seg_1001" s="T46">conj</ta>
            <ta e="T48" id="Seg_1002" s="T47">num</ta>
            <ta e="T49" id="Seg_1003" s="T48">n</ta>
            <ta e="T50" id="Seg_1004" s="T49">dem</ta>
            <ta e="T51" id="Seg_1005" s="T50">n</ta>
            <ta e="T52" id="Seg_1006" s="T51">adv</ta>
            <ta e="T53" id="Seg_1007" s="T52">preverb</ta>
            <ta e="T54" id="Seg_1008" s="T53">v</ta>
            <ta e="T55" id="Seg_1009" s="T54">n</ta>
            <ta e="T56" id="Seg_1010" s="T55">adj</ta>
            <ta e="T57" id="Seg_1011" s="T56">n</ta>
            <ta e="T58" id="Seg_1012" s="T57">adj</ta>
            <ta e="T59" id="Seg_1013" s="T58">n</ta>
            <ta e="T60" id="Seg_1014" s="T59">v</ta>
            <ta e="T61" id="Seg_1015" s="T60">adj</ta>
            <ta e="T62" id="Seg_1016" s="T61">n</ta>
            <ta e="T63" id="Seg_1017" s="T62">conj</ta>
            <ta e="T64" id="Seg_1018" s="T63">adj</ta>
            <ta e="T65" id="Seg_1019" s="T64">n</ta>
            <ta e="T66" id="Seg_1020" s="T65">v</ta>
            <ta e="T67" id="Seg_1021" s="T66">adv</ta>
            <ta e="T68" id="Seg_1022" s="T67">v</ta>
            <ta e="T69" id="Seg_1023" s="T68">adj</ta>
            <ta e="T70" id="Seg_1024" s="T69">n</ta>
            <ta e="T71" id="Seg_1025" s="T70">adv</ta>
            <ta e="T72" id="Seg_1026" s="T71">adv</ta>
            <ta e="T73" id="Seg_1027" s="T72">adv</ta>
            <ta e="T74" id="Seg_1028" s="T73">v</ta>
            <ta e="T75" id="Seg_1029" s="T74">n</ta>
            <ta e="T76" id="Seg_1030" s="T75">n</ta>
            <ta e="T77" id="Seg_1031" s="T76">pp</ta>
            <ta e="T78" id="Seg_1032" s="T77">n</ta>
            <ta e="T79" id="Seg_1033" s="T78">adv</ta>
            <ta e="T80" id="Seg_1034" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_1035" s="T80">preverb</ta>
            <ta e="T82" id="Seg_1036" s="T81">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_1037" s="T0">pro.h:Th</ta>
            <ta e="T3" id="Seg_1038" s="T2">np:L</ta>
            <ta e="T4" id="Seg_1039" s="T3">adv:L</ta>
            <ta e="T5" id="Seg_1040" s="T4">adv:Time</ta>
            <ta e="T8" id="Seg_1041" s="T7">np:Th</ta>
            <ta e="T10" id="Seg_1042" s="T9">np:L</ta>
            <ta e="T11" id="Seg_1043" s="T10">0.3.h:Th</ta>
            <ta e="T12" id="Seg_1044" s="T11">adv:Time</ta>
            <ta e="T13" id="Seg_1045" s="T12">adv:Time</ta>
            <ta e="T14" id="Seg_1046" s="T13">np.h:A</ta>
            <ta e="T16" id="Seg_1047" s="T15">np:P</ta>
            <ta e="T17" id="Seg_1048" s="T16">0.3.h:A</ta>
            <ta e="T18" id="Seg_1049" s="T17">np:P</ta>
            <ta e="T22" id="Seg_1050" s="T21">np.h:A</ta>
            <ta e="T27" id="Seg_1051" s="T26">pro.h:A</ta>
            <ta e="T31" id="Seg_1052" s="T30">0.1.h:A</ta>
            <ta e="T35" id="Seg_1053" s="T34">0.1.h:A</ta>
            <ta e="T37" id="Seg_1054" s="T36">pp:Time</ta>
            <ta e="T39" id="Seg_1055" s="T38">adv:L</ta>
            <ta e="T40" id="Seg_1056" s="T39">0.1.h:Th</ta>
            <ta e="T41" id="Seg_1057" s="T40">adv:Time</ta>
            <ta e="T42" id="Seg_1058" s="T41">np.h:Th</ta>
            <ta e="T43" id="Seg_1059" s="T42">0.1.h:B</ta>
            <ta e="T45" id="Seg_1060" s="T44">np.h:Th 0.1.h:Poss</ta>
            <ta e="T49" id="Seg_1061" s="T48">np.h:Th 0.1.h:Poss</ta>
            <ta e="T51" id="Seg_1062" s="T50">np.h:P</ta>
            <ta e="T57" id="Seg_1063" s="T56">np:L</ta>
            <ta e="T59" id="Seg_1064" s="T58">np:P</ta>
            <ta e="T60" id="Seg_1065" s="T59">0.3.h:A</ta>
            <ta e="T62" id="Seg_1066" s="T61">np:Ins</ta>
            <ta e="T65" id="Seg_1067" s="T64">np:Ins</ta>
            <ta e="T66" id="Seg_1068" s="T65">0.3.h:A 0.3.h:Th</ta>
            <ta e="T68" id="Seg_1069" s="T67">0.3.h:A 0.3.h:Th</ta>
            <ta e="T70" id="Seg_1070" s="T69">np:G</ta>
            <ta e="T73" id="Seg_1071" s="T72">adv:L</ta>
            <ta e="T74" id="Seg_1072" s="T73">0.3.h:A 0.3.h:Th</ta>
            <ta e="T75" id="Seg_1073" s="T74">np.h:A</ta>
            <ta e="T78" id="Seg_1074" s="T77">np.h:A</ta>
            <ta e="T82" id="Seg_1075" s="T81">0.3.h:A 0.3.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_1076" s="T0">pro.h:S</ta>
            <ta e="T2" id="Seg_1077" s="T1">v:pred</ta>
            <ta e="T6" id="Seg_1078" s="T5">v:pred</ta>
            <ta e="T8" id="Seg_1079" s="T7">np:S</ta>
            <ta e="T11" id="Seg_1080" s="T10">0.3.h:S v:pred</ta>
            <ta e="T14" id="Seg_1081" s="T13">np.h:S</ta>
            <ta e="T15" id="Seg_1082" s="T14">v:pred</ta>
            <ta e="T16" id="Seg_1083" s="T15">np:O</ta>
            <ta e="T17" id="Seg_1084" s="T16">0.3.h:S v:pred</ta>
            <ta e="T21" id="Seg_1085" s="T17">s:temp</ta>
            <ta e="T22" id="Seg_1086" s="T21">np.h:S</ta>
            <ta e="T23" id="Seg_1087" s="T22">v:pred</ta>
            <ta e="T27" id="Seg_1088" s="T26">pro.h:S</ta>
            <ta e="T28" id="Seg_1089" s="T27">v:pred</ta>
            <ta e="T31" id="Seg_1090" s="T28">s:temp</ta>
            <ta e="T35" id="Seg_1091" s="T34">0.1.h:S v:pred</ta>
            <ta e="T40" id="Seg_1092" s="T39">0.1.h:S v:pred</ta>
            <ta e="T42" id="Seg_1093" s="T41">np.h:O</ta>
            <ta e="T43" id="Seg_1094" s="T42">0.1.h:S v:pred</ta>
            <ta e="T45" id="Seg_1095" s="T44">np.h:S</ta>
            <ta e="T46" id="Seg_1096" s="T45">v:pred</ta>
            <ta e="T49" id="Seg_1097" s="T48">np.h:S</ta>
            <ta e="T51" id="Seg_1098" s="T50">np.h:S</ta>
            <ta e="T52" id="Seg_1099" s="T51">s:adv</ta>
            <ta e="T54" id="Seg_1100" s="T53">v:pred</ta>
            <ta e="T59" id="Seg_1101" s="T58">np:O</ta>
            <ta e="T60" id="Seg_1102" s="T59">0.3.h:S v:pred</ta>
            <ta e="T66" id="Seg_1103" s="T65">0.3.h:S 0.3.h:O v:pred</ta>
            <ta e="T68" id="Seg_1104" s="T67">0.3.h:S 0.3.h:O v:pred</ta>
            <ta e="T72" id="Seg_1105" s="T70">s:adv</ta>
            <ta e="T74" id="Seg_1106" s="T73">0.3.h:S 0.3.h:O v:pred</ta>
            <ta e="T77" id="Seg_1107" s="T74">s:temp</ta>
            <ta e="T80" id="Seg_1108" s="T77">s:temp</ta>
            <ta e="T82" id="Seg_1109" s="T81">0.3.h:S 0.3.h:O v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_1110" s="T0">new</ta>
            <ta e="T3" id="Seg_1111" s="T2">accs-gen</ta>
            <ta e="T8" id="Seg_1112" s="T7">new</ta>
            <ta e="T10" id="Seg_1113" s="T9">new</ta>
            <ta e="T11" id="Seg_1114" s="T10">accs-sit</ta>
            <ta e="T14" id="Seg_1115" s="T13">new</ta>
            <ta e="T16" id="Seg_1116" s="T15">new</ta>
            <ta e="T17" id="Seg_1117" s="T16">0.giv-active</ta>
            <ta e="T18" id="Seg_1118" s="T17">giv-active</ta>
            <ta e="T22" id="Seg_1119" s="T21">new</ta>
            <ta e="T26" id="Seg_1120" s="T25">new</ta>
            <ta e="T27" id="Seg_1121" s="T26">accs-sit</ta>
            <ta e="T34" id="Seg_1122" s="T33">accs-sit</ta>
            <ta e="T35" id="Seg_1123" s="T34">giv-inactive</ta>
            <ta e="T37" id="Seg_1124" s="T36">accs-gen</ta>
            <ta e="T40" id="Seg_1125" s="T39">0.giv-active</ta>
            <ta e="T42" id="Seg_1126" s="T41">new</ta>
            <ta e="T43" id="Seg_1127" s="T42">0.giv-active</ta>
            <ta e="T45" id="Seg_1128" s="T44">new</ta>
            <ta e="T49" id="Seg_1129" s="T48">new</ta>
            <ta e="T51" id="Seg_1130" s="T50">giv-active</ta>
            <ta e="T57" id="Seg_1131" s="T56">accs-gen</ta>
            <ta e="T59" id="Seg_1132" s="T58">accs-sit</ta>
            <ta e="T60" id="Seg_1133" s="T59">0.accs-gen</ta>
            <ta e="T62" id="Seg_1134" s="T61">accs-sit</ta>
            <ta e="T65" id="Seg_1135" s="T64">accs-sit</ta>
            <ta e="T66" id="Seg_1136" s="T65">0.giv-active 0.giv-inactive</ta>
            <ta e="T68" id="Seg_1137" s="T67">0.giv-active.0</ta>
            <ta e="T70" id="Seg_1138" s="T69">accs-sit</ta>
            <ta e="T74" id="Seg_1139" s="T73">0.giv-active.0</ta>
            <ta e="T75" id="Seg_1140" s="T74">accs-inf</ta>
            <ta e="T78" id="Seg_1141" s="T77">giv-active</ta>
            <ta e="T82" id="Seg_1142" s="T81">0.giv-active.0</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T7" id="Seg_1143" s="T6">RUS:cult</ta>
            <ta e="T22" id="Seg_1144" s="T21">RUS:cult</ta>
            <ta e="T34" id="Seg_1145" s="T33">RUS:cult</ta>
            <ta e="T35" id="Seg_1146" s="T34">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_1147" s="T0">Я жила в Тольке.</ta>
            <ta e="T8" id="Seg_1148" s="T3">Здесь никогда не было русских домов.</ta>
            <ta e="T11" id="Seg_1149" s="T8">В селькупском чуме жили.</ta>
            <ta e="T15" id="Seg_1150" s="T11">Потом люди приехали.</ta>
            <ta e="T17" id="Seg_1151" s="T15">Дома стали строить.</ta>
            <ta e="T23" id="Seg_1152" s="T17">После того, как дом строить закончили, продавщица приехала.</ta>
            <ta e="T26" id="Seg_1153" s="T23">И учительницы.</ta>
            <ta e="T28" id="Seg_1154" s="T26">Мы стали учиться.</ta>
            <ta e="T35" id="Seg_1155" s="T28">После учёбы закончила, с трудом 3 класса закончила.</ta>
            <ta e="T40" id="Seg_1156" s="T35">Два года дома была.</ta>
            <ta e="T43" id="Seg_1157" s="T40">Потом мужа нашла.</ta>
            <ta e="T49" id="Seg_1158" s="T43">Одна дочка у меня была и один сынок.</ta>
            <ta e="T54" id="Seg_1159" s="T49">Этот сынок, поболев, умер.</ta>
            <ta e="T57" id="Seg_1160" s="T54">Среди зимы.</ta>
            <ta e="T60" id="Seg_1161" s="T57">Гроб сделали.</ta>
            <ta e="T66" id="Seg_1162" s="T60">В белый платок и черный платок завернули.</ta>
            <ta e="T70" id="Seg_1163" s="T66">Потом унесли на кладбище.</ta>
            <ta e="T77" id="Seg_1164" s="T70">Туда унесли, туда поставили до приезда отца.</ta>
            <ta e="T82" id="Seg_1165" s="T77">После того, как отец приехал, похоронили.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_1166" s="T0">I lived in Tolka.</ta>
            <ta e="T8" id="Seg_1167" s="T3">There have never been any Russian houses here.</ta>
            <ta e="T11" id="Seg_1168" s="T8">One used to live in Selkup tents.</ta>
            <ta e="T15" id="Seg_1169" s="T11">Then people came.</ta>
            <ta e="T17" id="Seg_1170" s="T15">They started building houses.</ta>
            <ta e="T23" id="Seg_1171" s="T17">After they have finished to build the house, a saleswoman arrived.</ta>
            <ta e="T26" id="Seg_1172" s="T23">And school teachers.</ta>
            <ta e="T28" id="Seg_1173" s="T26">We started to study.</ta>
            <ta e="T35" id="Seg_1174" s="T28">Having finished my studies, I hardly finished three grades.</ta>
            <ta e="T40" id="Seg_1175" s="T35">I stayed home for two years.</ta>
            <ta e="T43" id="Seg_1176" s="T40">Then I met my husband.</ta>
            <ta e="T49" id="Seg_1177" s="T43">I had a daughter and a son.</ta>
            <ta e="T54" id="Seg_1178" s="T49">The son got sick and died.</ta>
            <ta e="T57" id="Seg_1179" s="T54">In winter.</ta>
            <ta e="T60" id="Seg_1180" s="T57">They made a coffin.</ta>
            <ta e="T66" id="Seg_1181" s="T60">They wrapped him into a black and a white headscarf.</ta>
            <ta e="T70" id="Seg_1182" s="T66">Then they brought him to the cemetery. </ta>
            <ta e="T77" id="Seg_1183" s="T70">They brought him there and left him until his father came.</ta>
            <ta e="T82" id="Seg_1184" s="T77">After his father had come they buried him.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_1185" s="T0">Ich lebte in Tolka.</ta>
            <ta e="T8" id="Seg_1186" s="T3">Hier gab es nie russische Häuser.</ta>
            <ta e="T11" id="Seg_1187" s="T8">Man lebte in selkupischen Zelten.</ta>
            <ta e="T15" id="Seg_1188" s="T11">Dann kamen Leute.</ta>
            <ta e="T17" id="Seg_1189" s="T15">Sie fingen an Häuser zu bauen.</ta>
            <ta e="T23" id="Seg_1190" s="T17">Nachdem sie das Haus gebaut hatten, kam eine Verkäuferin.</ta>
            <ta e="T26" id="Seg_1191" s="T23">Und Lehrerinnen kamen auch.</ta>
            <ta e="T28" id="Seg_1192" s="T26">Wir fingen an zu lernen.</ta>
            <ta e="T35" id="Seg_1193" s="T28">Als ich aufhörte zu lernen, hatte ich kaum drei Klassen beendet.</ta>
            <ta e="T40" id="Seg_1194" s="T35">Ich blieb zwei Jahre lang zu Hause.</ta>
            <ta e="T43" id="Seg_1195" s="T40">Dann traf ich meinen Mann.</ta>
            <ta e="T49" id="Seg_1196" s="T43">Ich hatte eine Tochter und einen Sohn.</ta>
            <ta e="T54" id="Seg_1197" s="T49">Der Sohn wurde krank und starb.</ta>
            <ta e="T57" id="Seg_1198" s="T54">Im Winter.</ta>
            <ta e="T60" id="Seg_1199" s="T57">Man machte einen Sarg.</ta>
            <ta e="T66" id="Seg_1200" s="T60">Man wickelte ihn in einen schwarzen und einen weißen Schal ein.</ta>
            <ta e="T70" id="Seg_1201" s="T66">Dann brachte man ihn zum Friedhof.</ta>
            <ta e="T77" id="Seg_1202" s="T70">Man brachte ihn dorthin und ließ ihn dort, bis sein Vater kam.</ta>
            <ta e="T82" id="Seg_1203" s="T77">Nachdem sein Vater gekommen war, beerdigte man ihn.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_1204" s="T0">я жил в Тольке</ta>
            <ta e="T8" id="Seg_1205" s="T3">здесь вообще не было русских домов</ta>
            <ta e="T11" id="Seg_1206" s="T8">в чуме [селькупский дом] жили</ta>
            <ta e="T15" id="Seg_1207" s="T11">потом люди приехали</ta>
            <ta e="T17" id="Seg_1208" s="T15">дома стали строить</ta>
            <ta e="T23" id="Seg_1209" s="T17">дом строить закончили, после продавщица приехала</ta>
            <ta e="T26" id="Seg_1210" s="T23">еще учителя</ta>
            <ta e="T28" id="Seg_1211" s="T26">мы стали учиться</ta>
            <ta e="T35" id="Seg_1212" s="T28">после учебы [учиться] кончила с трудом 3 класса кончили</ta>
            <ta e="T40" id="Seg_1213" s="T35">2 года дома была</ta>
            <ta e="T43" id="Seg_1214" s="T40">потом мужа нашла</ta>
            <ta e="T49" id="Seg_1215" s="T43">одна девочка была и один сынок</ta>
            <ta e="T54" id="Seg_1216" s="T49">этот сынок поболев умер</ta>
            <ta e="T57" id="Seg_1217" s="T54">[половина] среди зимы умер</ta>
            <ta e="T60" id="Seg_1218" s="T57">гроб сделали</ta>
            <ta e="T66" id="Seg_1219" s="T60">белый товар и черный товар завернули</ta>
            <ta e="T70" id="Seg_1220" s="T66">унесли к покойникам [на кладбище]</ta>
            <ta e="T77" id="Seg_1221" s="T70">туда унесли, туда поставили отец когда приедет</ta>
            <ta e="T82" id="Seg_1222" s="T77">когда [после того как] отец приехал похоронили</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T35" id="Seg_1223" s="T28">[MBr:] unclear syntax of the verb "končʼi-"</ta>
            <ta e="T60" id="Seg_1224" s="T57">[OSV:] "poːlʼ qor" - "a hearse".</ta>
            <ta e="T70" id="Seg_1225" s="T66">[OSV:] "qumɨlʼ mɛktɨ" has here the meaning "cemetery".</ta>
            <ta e="T82" id="Seg_1226" s="T77">[OSV:] "ıllä meːqo" - "to bury".</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
