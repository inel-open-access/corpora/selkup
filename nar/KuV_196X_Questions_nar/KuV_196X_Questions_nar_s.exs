<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KuV_196X_Questions_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KuV_196X_Questions_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">121</ud-information>
            <ud-information attribute-name="# HIAT:w">93</ud-information>
            <ud-information attribute-name="# e">93</ud-information>
            <ud-information attribute-name="# HIAT:u">23</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KuV">
            <abbreviation>KuV</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KuV"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T93" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">tanan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">eːk</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">bratlal</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">manan</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">eːk</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">brat</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">alka</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">qwädɨmbadə</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">kɨbačip</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">täbat</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">pajat</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_44" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">po</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">paɣelǯigu</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">i</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">pingu</ts>
                  <nts id="Seg_56" n="HIAT:ip">,</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">a</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">potom</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">merɨngu</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_69" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">natko</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">qomde</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">meladi</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_81" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">nap</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">qomdep</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_89" n="HIAT:w" s="T23">üdəgu</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_93" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_95" n="HIAT:w" s="T24">tat</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">kundɨqat</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_101" n="HIAT:w" s="T26">üǯandə</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_104" n="HIAT:w" s="T27">tɨrdə</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_108" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_110" n="HIAT:w" s="T28">kudanɨn</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_113" n="HIAT:w" s="T29">wargande</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_117" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_119" n="HIAT:w" s="T30">načaɣɨt</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_122" n="HIAT:w" s="T31">Iːrišanen</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_125" n="HIAT:w" s="T32">warkandə</ts>
                  <nts id="Seg_126" n="HIAT:ip">.</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_129" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_131" n="HIAT:w" s="T33">kušak</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_134" n="HIAT:w" s="T34">mešpal</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_137" n="HIAT:w" s="T35">qumdep</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_140" n="HIAT:w" s="T36">madətko</ts>
                  <nts id="Seg_141" n="HIAT:ip">.</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_144" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_146" n="HIAT:w" s="T37">na</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_149" n="HIAT:w" s="T38">školaɣɨt</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_152" n="HIAT:w" s="T39">wargandə</ts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_156" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_158" n="HIAT:w" s="T40">pasuda</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_161" n="HIAT:w" s="T41">lɨk</ts>
                  <nts id="Seg_162" n="HIAT:ip">.</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_165" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_167" n="HIAT:w" s="T42">stolovande</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_170" n="HIAT:w" s="T43">kujašpet</ts>
                  <nts id="Seg_171" n="HIAT:ip">.</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_174" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_176" n="HIAT:w" s="T44">kušak</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_179" n="HIAT:w" s="T45">merǯešpal</ts>
                  <nts id="Seg_180" n="HIAT:ip">?</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_183" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_185" n="HIAT:w" s="T46">načat</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_188" n="HIAT:w" s="T47">ešo</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_191" n="HIAT:w" s="T48">eje</ts>
                  <nts id="Seg_192" n="HIAT:ip">,</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_195" n="HIAT:w" s="T49">wargaat</ts>
                  <nts id="Seg_196" n="HIAT:ip">,</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_199" n="HIAT:w" s="T50">školaɣɨt</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_202" n="HIAT:w" s="T51">ili</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_205" n="HIAT:w" s="T52">tat</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_208" n="HIAT:w" s="T53">unenǯe</ts>
                  <nts id="Seg_209" n="HIAT:ip">.</nts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_212" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_214" n="HIAT:w" s="T54">pelgalɨk</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_217" n="HIAT:w" s="T55">wargandə</ts>
                  <nts id="Seg_218" n="HIAT:ip">.</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_221" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_223" n="HIAT:w" s="T56">a</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_226" n="HIAT:w" s="T57">sejčas</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_229" n="HIAT:w" s="T58">ku</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_232" n="HIAT:w" s="T59">kwelande</ts>
                  <nts id="Seg_233" n="HIAT:ip">.</nts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_236" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_238" n="HIAT:w" s="T60">Kilʼikejkin</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_241" n="HIAT:w" s="T61">oralǯiga</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_244" n="HIAT:w" s="T62">üt</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_247" n="HIAT:w" s="T63">šeːrbadə</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_250" n="HIAT:w" s="T64">ilʼi</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_253" n="HIAT:w" s="T65">qaj</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_256" n="HIAT:w" s="T66">lʼi</ts>
                  <nts id="Seg_257" n="HIAT:ip">.</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_260" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_262" n="HIAT:w" s="T67">a</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_265" n="HIAT:w" s="T68">pajadə</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_268" n="HIAT:w" s="T69">qeɣan</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_271" n="HIAT:w" s="T70">eːk</ts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_275" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_277" n="HIAT:w" s="T71">ondə</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_280" n="HIAT:w" s="T72">pelgalɨk</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_283" n="HIAT:w" s="T73">kujašpa</ts>
                  <nts id="Seg_284" n="HIAT:ip">.</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_287" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_289" n="HIAT:w" s="T74">tabə</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_292" n="HIAT:w" s="T75">aː</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_295" n="HIAT:w" s="T76">qwälǯa</ts>
                  <nts id="Seg_296" n="HIAT:ip">.</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_299" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_301" n="HIAT:w" s="T77">a</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_304" n="HIAT:w" s="T78">sejčas</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_307" n="HIAT:w" s="T79">qwäla</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_310" n="HIAT:w" s="T80">kuːšaɣɨt</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_313" n="HIAT:w" s="T81">töla</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_316" n="HIAT:w" s="T82">teka</ts>
                  <nts id="Seg_317" n="HIAT:ip">.</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_320" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_322" n="HIAT:w" s="T83">tabət</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_325" n="HIAT:w" s="T84">pajamdə</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_328" n="HIAT:w" s="T85">hoɣončihal</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_331" n="HIAT:w" s="T86">kučaɣɨt</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_333" n="HIAT:ip">(</nts>
                  <ts e="T88" id="Seg_335" n="HIAT:w" s="T87">kušiɣat</ts>
                  <nts id="Seg_336" n="HIAT:ip">)</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_339" n="HIAT:w" s="T88">belkap</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_342" n="HIAT:w" s="T89">qwatpadə</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_345" n="HIAT:w" s="T90">ilʼi</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_348" n="HIAT:w" s="T91">warge</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_351" n="HIAT:w" s="T92">huːrup</ts>
                  <nts id="Seg_352" n="HIAT:ip">.</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T93" id="Seg_354" n="sc" s="T0">
               <ts e="T1" id="Seg_356" n="e" s="T0">tanan </ts>
               <ts e="T2" id="Seg_358" n="e" s="T1">eːk </ts>
               <ts e="T3" id="Seg_360" n="e" s="T2">bratlal. </ts>
               <ts e="T4" id="Seg_362" n="e" s="T3">manan </ts>
               <ts e="T5" id="Seg_364" n="e" s="T4">eːk </ts>
               <ts e="T6" id="Seg_366" n="e" s="T5">brat </ts>
               <ts e="T7" id="Seg_368" n="e" s="T6">alka. </ts>
               <ts e="T8" id="Seg_370" n="e" s="T7">qwädɨmbadə </ts>
               <ts e="T9" id="Seg_372" n="e" s="T8">kɨbačip </ts>
               <ts e="T10" id="Seg_374" n="e" s="T9">täbat </ts>
               <ts e="T11" id="Seg_376" n="e" s="T10">pajat. </ts>
               <ts e="T12" id="Seg_378" n="e" s="T11">po </ts>
               <ts e="T13" id="Seg_380" n="e" s="T12">paɣelǯigu </ts>
               <ts e="T14" id="Seg_382" n="e" s="T13">i </ts>
               <ts e="T15" id="Seg_384" n="e" s="T14">pingu, </ts>
               <ts e="T16" id="Seg_386" n="e" s="T15">a </ts>
               <ts e="T17" id="Seg_388" n="e" s="T16">potom </ts>
               <ts e="T18" id="Seg_390" n="e" s="T17">merɨngu. </ts>
               <ts e="T19" id="Seg_392" n="e" s="T18">natko </ts>
               <ts e="T20" id="Seg_394" n="e" s="T19">qomde </ts>
               <ts e="T21" id="Seg_396" n="e" s="T20">meladi. </ts>
               <ts e="T22" id="Seg_398" n="e" s="T21">nap </ts>
               <ts e="T23" id="Seg_400" n="e" s="T22">qomdep </ts>
               <ts e="T24" id="Seg_402" n="e" s="T23">üdəgu. </ts>
               <ts e="T25" id="Seg_404" n="e" s="T24">tat </ts>
               <ts e="T26" id="Seg_406" n="e" s="T25">kundɨqat </ts>
               <ts e="T27" id="Seg_408" n="e" s="T26">üǯandə </ts>
               <ts e="T28" id="Seg_410" n="e" s="T27">tɨrdə. </ts>
               <ts e="T29" id="Seg_412" n="e" s="T28">kudanɨn </ts>
               <ts e="T30" id="Seg_414" n="e" s="T29">wargande. </ts>
               <ts e="T31" id="Seg_416" n="e" s="T30">načaɣɨt </ts>
               <ts e="T32" id="Seg_418" n="e" s="T31">Iːrišanen </ts>
               <ts e="T33" id="Seg_420" n="e" s="T32">warkandə. </ts>
               <ts e="T34" id="Seg_422" n="e" s="T33">kušak </ts>
               <ts e="T35" id="Seg_424" n="e" s="T34">mešpal </ts>
               <ts e="T36" id="Seg_426" n="e" s="T35">qumdep </ts>
               <ts e="T37" id="Seg_428" n="e" s="T36">madətko. </ts>
               <ts e="T38" id="Seg_430" n="e" s="T37">na </ts>
               <ts e="T39" id="Seg_432" n="e" s="T38">školaɣɨt </ts>
               <ts e="T40" id="Seg_434" n="e" s="T39">wargandə. </ts>
               <ts e="T41" id="Seg_436" n="e" s="T40">pasuda </ts>
               <ts e="T42" id="Seg_438" n="e" s="T41">lɨk. </ts>
               <ts e="T43" id="Seg_440" n="e" s="T42">stolovande </ts>
               <ts e="T44" id="Seg_442" n="e" s="T43">kujašpet. </ts>
               <ts e="T45" id="Seg_444" n="e" s="T44">kušak </ts>
               <ts e="T46" id="Seg_446" n="e" s="T45">merǯešpal? </ts>
               <ts e="T47" id="Seg_448" n="e" s="T46">načat </ts>
               <ts e="T48" id="Seg_450" n="e" s="T47">ešo </ts>
               <ts e="T49" id="Seg_452" n="e" s="T48">eje, </ts>
               <ts e="T50" id="Seg_454" n="e" s="T49">wargaat, </ts>
               <ts e="T51" id="Seg_456" n="e" s="T50">školaɣɨt </ts>
               <ts e="T52" id="Seg_458" n="e" s="T51">ili </ts>
               <ts e="T53" id="Seg_460" n="e" s="T52">tat </ts>
               <ts e="T54" id="Seg_462" n="e" s="T53">unenǯe. </ts>
               <ts e="T55" id="Seg_464" n="e" s="T54">pelgalɨk </ts>
               <ts e="T56" id="Seg_466" n="e" s="T55">wargandə. </ts>
               <ts e="T57" id="Seg_468" n="e" s="T56">a </ts>
               <ts e="T58" id="Seg_470" n="e" s="T57">sejčas </ts>
               <ts e="T59" id="Seg_472" n="e" s="T58">ku </ts>
               <ts e="T60" id="Seg_474" n="e" s="T59">kwelande. </ts>
               <ts e="T61" id="Seg_476" n="e" s="T60">Kilʼikejkin </ts>
               <ts e="T62" id="Seg_478" n="e" s="T61">oralǯiga </ts>
               <ts e="T63" id="Seg_480" n="e" s="T62">üt </ts>
               <ts e="T64" id="Seg_482" n="e" s="T63">šeːrbadə </ts>
               <ts e="T65" id="Seg_484" n="e" s="T64">ilʼi </ts>
               <ts e="T66" id="Seg_486" n="e" s="T65">qaj </ts>
               <ts e="T67" id="Seg_488" n="e" s="T66">lʼi. </ts>
               <ts e="T68" id="Seg_490" n="e" s="T67">a </ts>
               <ts e="T69" id="Seg_492" n="e" s="T68">pajadə </ts>
               <ts e="T70" id="Seg_494" n="e" s="T69">qeɣan </ts>
               <ts e="T71" id="Seg_496" n="e" s="T70">eːk. </ts>
               <ts e="T72" id="Seg_498" n="e" s="T71">ondə </ts>
               <ts e="T73" id="Seg_500" n="e" s="T72">pelgalɨk </ts>
               <ts e="T74" id="Seg_502" n="e" s="T73">kujašpa. </ts>
               <ts e="T75" id="Seg_504" n="e" s="T74">tabə </ts>
               <ts e="T76" id="Seg_506" n="e" s="T75">aː </ts>
               <ts e="T77" id="Seg_508" n="e" s="T76">qwälǯa. </ts>
               <ts e="T78" id="Seg_510" n="e" s="T77">a </ts>
               <ts e="T79" id="Seg_512" n="e" s="T78">sejčas </ts>
               <ts e="T80" id="Seg_514" n="e" s="T79">qwäla </ts>
               <ts e="T81" id="Seg_516" n="e" s="T80">kuːšaɣɨt </ts>
               <ts e="T82" id="Seg_518" n="e" s="T81">töla </ts>
               <ts e="T83" id="Seg_520" n="e" s="T82">teka. </ts>
               <ts e="T84" id="Seg_522" n="e" s="T83">tabət </ts>
               <ts e="T85" id="Seg_524" n="e" s="T84">pajamdə </ts>
               <ts e="T86" id="Seg_526" n="e" s="T85">hoɣončihal </ts>
               <ts e="T87" id="Seg_528" n="e" s="T86">kučaɣɨt </ts>
               <ts e="T88" id="Seg_530" n="e" s="T87">(kušiɣat) </ts>
               <ts e="T89" id="Seg_532" n="e" s="T88">belkap </ts>
               <ts e="T90" id="Seg_534" n="e" s="T89">qwatpadə </ts>
               <ts e="T91" id="Seg_536" n="e" s="T90">ilʼi </ts>
               <ts e="T92" id="Seg_538" n="e" s="T91">warge </ts>
               <ts e="T93" id="Seg_540" n="e" s="T92">huːrup. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_541" s="T0">KuV_196X_Questions_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_542" s="T3">KuV_196X_Questions_nar.002 (001.002)</ta>
            <ta e="T11" id="Seg_543" s="T7">KuV_196X_Questions_nar.003 (001.003)</ta>
            <ta e="T18" id="Seg_544" s="T11">KuV_196X_Questions_nar.004 (001.004)</ta>
            <ta e="T21" id="Seg_545" s="T18">KuV_196X_Questions_nar.005 (001.005)</ta>
            <ta e="T24" id="Seg_546" s="T21">KuV_196X_Questions_nar.006 (001.006)</ta>
            <ta e="T28" id="Seg_547" s="T24">KuV_196X_Questions_nar.007 (001.007)</ta>
            <ta e="T30" id="Seg_548" s="T28">KuV_196X_Questions_nar.008 (001.008)</ta>
            <ta e="T33" id="Seg_549" s="T30">KuV_196X_Questions_nar.009 (001.009)</ta>
            <ta e="T37" id="Seg_550" s="T33">KuV_196X_Questions_nar.010 (001.010)</ta>
            <ta e="T40" id="Seg_551" s="T37">KuV_196X_Questions_nar.011 (001.011)</ta>
            <ta e="T42" id="Seg_552" s="T40">KuV_196X_Questions_nar.012 (001.012)</ta>
            <ta e="T44" id="Seg_553" s="T42">KuV_196X_Questions_nar.013 (001.013)</ta>
            <ta e="T46" id="Seg_554" s="T44">KuV_196X_Questions_nar.014 (001.014)</ta>
            <ta e="T54" id="Seg_555" s="T46">KuV_196X_Questions_nar.015 (001.015)</ta>
            <ta e="T56" id="Seg_556" s="T54">KuV_196X_Questions_nar.016 (001.016)</ta>
            <ta e="T60" id="Seg_557" s="T56">KuV_196X_Questions_nar.017 (001.017)</ta>
            <ta e="T67" id="Seg_558" s="T60">KuV_196X_Questions_nar.018 (001.018)</ta>
            <ta e="T71" id="Seg_559" s="T67">KuV_196X_Questions_nar.019 (001.019)</ta>
            <ta e="T74" id="Seg_560" s="T71">KuV_196X_Questions_nar.020 (001.020)</ta>
            <ta e="T77" id="Seg_561" s="T74">KuV_196X_Questions_nar.021 (001.021)</ta>
            <ta e="T83" id="Seg_562" s="T77">KuV_196X_Questions_nar.022 (001.022)</ta>
            <ta e="T93" id="Seg_563" s="T83">KuV_196X_Questions_nar.023 (001.023)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_564" s="T0">та′нан е̄к братлал.</ta>
            <ta e="T7" id="Seg_565" s="T3">ма′нан е̄к брат аlка.</ta>
            <ta e="T11" id="Seg_566" s="T7">′kwӓдымбадъ кы′бачип ′тӓбат па′jат.</ta>
            <ta e="T18" id="Seg_567" s="T11">по ′паɣеlджигу и пин′гу, а потом мерынгу.</ta>
            <ta e="T21" id="Seg_568" s="T18">′натко ′kомде ме′лади.</ta>
            <ta e="T24" id="Seg_569" s="T21">нап kомдеп ӱдъ′гу.</ta>
            <ta e="T28" id="Seg_570" s="T24">тат ′кундыkат ′ӱджандъ тырдъ.</ta>
            <ta e="T30" id="Seg_571" s="T28">куда′нын вар′ганде.</ta>
            <ta e="T33" id="Seg_572" s="T30">на′чаɣыт ′ӣришанен вар′кандъ.</ta>
            <ta e="T37" id="Seg_573" s="T33">ку′шак ′мешпал kум′деп ′мадътко.</ta>
            <ta e="T40" id="Seg_574" s="T37">на школаɣыт вар′гандъ.</ta>
            <ta e="T42" id="Seg_575" s="T40">па′суда ′лы(е)к.</ta>
            <ta e="T44" id="Seg_576" s="T42">столованде ку′jашпет.</ta>
            <ta e="T46" id="Seg_577" s="T44">ку′шак мер′джешпал?</ta>
            <ta e="T54" id="Seg_578" s="T46">на′чат е′шо еjе, вар′га̄т, школаɣыт или тат у′нендже.</ta>
            <ta e="T56" id="Seg_579" s="T54">пелгалык вар′гандъ.</ta>
            <ta e="T60" id="Seg_580" s="T56">а сейчас ку ′квеланде.</ta>
            <ta e="T67" id="Seg_581" s="T60">килʼикейкин о′раlджига ӱт ше̄рбадъ илʼи kай лʼи.</ta>
            <ta e="T71" id="Seg_582" s="T67">а паjадъ ′kеɣан е̄к.</ta>
            <ta e="T74" id="Seg_583" s="T71">ондъ пел′галык ку′jашпа.</ta>
            <ta e="T77" id="Seg_584" s="T74">табъ а̄ kwӓlджа.</ta>
            <ta e="T83" id="Seg_585" s="T77">а сейчас kwӓlа кӯшаɣыт тӧlа тека.</ta>
            <ta e="T93" id="Seg_586" s="T83">табът па′jамдъ hоɣончиhал кучаɣыт (кушиɣат) белкап kwатпадъ илʼи варге hӯруп.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_587" s="T0">tanan eːk bratlal.</ta>
            <ta e="T7" id="Seg_588" s="T3">manan eːk brat alka.</ta>
            <ta e="T11" id="Seg_589" s="T7">qwädɨmbadə kɨbačip täbat pajat.</ta>
            <ta e="T18" id="Seg_590" s="T11">po paɣelǯigu i pingu, a potom merɨngu.</ta>
            <ta e="T21" id="Seg_591" s="T18">natko qomde meladi.</ta>
            <ta e="T24" id="Seg_592" s="T21">nap qomdep üdəgu.</ta>
            <ta e="T28" id="Seg_593" s="T24">tat kundɨqat üǯandə tɨrdə.</ta>
            <ta e="T30" id="Seg_594" s="T28">kudanɨn vargande.</ta>
            <ta e="T33" id="Seg_595" s="T30">načaɣɨt iːrišanen varkandə.</ta>
            <ta e="T37" id="Seg_596" s="T33">kušak mešpal qumdep madətko.</ta>
            <ta e="T40" id="Seg_597" s="T37">na školaɣɨt vargandə.</ta>
            <ta e="T42" id="Seg_598" s="T40">pasuda lɨ(e)k.</ta>
            <ta e="T44" id="Seg_599" s="T42">stolovande kujašpet.</ta>
            <ta e="T46" id="Seg_600" s="T44">kušak merǯešpal?</ta>
            <ta e="T54" id="Seg_601" s="T46">načat ešo eje, vargaːt, školaɣɨt ili tat unenǯe.</ta>
            <ta e="T56" id="Seg_602" s="T54">pelgalɨk vargandə.</ta>
            <ta e="T60" id="Seg_603" s="T56">a sejčas ku kvelande.</ta>
            <ta e="T67" id="Seg_604" s="T60">kilʼikejkin oralǯiga üt šeːrbadə ilʼi qaj lʼi.</ta>
            <ta e="T71" id="Seg_605" s="T67">a pajadə qeɣan eːk.</ta>
            <ta e="T74" id="Seg_606" s="T71">ondə pelgalɨk kujašpa.</ta>
            <ta e="T77" id="Seg_607" s="T74">tabə aː qwälǯa.</ta>
            <ta e="T83" id="Seg_608" s="T77">a sejčas qwäla kuːšaɣɨt töla teka.</ta>
            <ta e="T93" id="Seg_609" s="T83">tabət pajamdə hoɣončihal kučaɣɨt (kušiɣat) belkap qwatpadə ilʼi varge huːrup.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_610" s="T0">tanan eːk bratlal. </ta>
            <ta e="T7" id="Seg_611" s="T3">manan eːk brat alka. </ta>
            <ta e="T11" id="Seg_612" s="T7">qwädɨmbadə kɨbačip täbat pajat. </ta>
            <ta e="T18" id="Seg_613" s="T11">po paɣelǯigu i pingu, a potom merɨngu. </ta>
            <ta e="T21" id="Seg_614" s="T18">natko qomde meladi. </ta>
            <ta e="T24" id="Seg_615" s="T21">nap qomdep üdəgu. </ta>
            <ta e="T28" id="Seg_616" s="T24">tat kundɨqat üǯandə tɨrdə. </ta>
            <ta e="T30" id="Seg_617" s="T28">kudanɨn wargande. </ta>
            <ta e="T33" id="Seg_618" s="T30">načaɣɨt Iːrišanen warkandə. </ta>
            <ta e="T37" id="Seg_619" s="T33">kušak mešpal qumdep madətko. </ta>
            <ta e="T40" id="Seg_620" s="T37">na školaɣɨt wargandə. </ta>
            <ta e="T42" id="Seg_621" s="T40">pasuda lɨk. </ta>
            <ta e="T44" id="Seg_622" s="T42">stolovande kujašpet. </ta>
            <ta e="T46" id="Seg_623" s="T44">kušak merǯešpal? </ta>
            <ta e="T54" id="Seg_624" s="T46">načat ešo eje, wargaat, školaɣɨt ili tat unenǯe. </ta>
            <ta e="T56" id="Seg_625" s="T54">pelgalɨk wargandə. </ta>
            <ta e="T60" id="Seg_626" s="T56">a sejčas ku kwelande. </ta>
            <ta e="T67" id="Seg_627" s="T60">Kilʼikejkin oralǯiga üt šeːrbadə ilʼi qaj lʼi. </ta>
            <ta e="T71" id="Seg_628" s="T67">a pajadə qeɣan eːk. </ta>
            <ta e="T74" id="Seg_629" s="T71">ondə pelgalɨk kujašpa. </ta>
            <ta e="T77" id="Seg_630" s="T74">tabə aː qwälǯa. </ta>
            <ta e="T83" id="Seg_631" s="T77">a sejčas qwäla kuːšaɣɨt töla teka. </ta>
            <ta e="T93" id="Seg_632" s="T83">tabət pajamdə hoɣončihal kučaɣɨt (kušiɣat) belkap qwatpadə ilʼi warge huːrup. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_633" s="T0">ta-nan</ta>
            <ta e="T2" id="Seg_634" s="T1">eː-k</ta>
            <ta e="T3" id="Seg_635" s="T2">brat-la-l</ta>
            <ta e="T4" id="Seg_636" s="T3">man-a-n</ta>
            <ta e="T5" id="Seg_637" s="T4">eː-k</ta>
            <ta e="T6" id="Seg_638" s="T5">brat</ta>
            <ta e="T7" id="Seg_639" s="T6">alka</ta>
            <ta e="T8" id="Seg_640" s="T7">qwädɨ-mba-də</ta>
            <ta e="T9" id="Seg_641" s="T8">kɨbači-p</ta>
            <ta e="T10" id="Seg_642" s="T9">täb-a-t</ta>
            <ta e="T11" id="Seg_643" s="T10">paja-t</ta>
            <ta e="T12" id="Seg_644" s="T11">po</ta>
            <ta e="T13" id="Seg_645" s="T12">paɣe-lǯi-gu</ta>
            <ta e="T14" id="Seg_646" s="T13">i</ta>
            <ta e="T15" id="Seg_647" s="T14">pin-gu</ta>
            <ta e="T16" id="Seg_648" s="T15">a</ta>
            <ta e="T17" id="Seg_649" s="T16">potom</ta>
            <ta e="T18" id="Seg_650" s="T17">merɨn-gu</ta>
            <ta e="T19" id="Seg_651" s="T18">na-tqo</ta>
            <ta e="T20" id="Seg_652" s="T19">qomde</ta>
            <ta e="T21" id="Seg_653" s="T20">me-la-di</ta>
            <ta e="T22" id="Seg_654" s="T21">na-p</ta>
            <ta e="T23" id="Seg_655" s="T22">qomde-p</ta>
            <ta e="T24" id="Seg_656" s="T23">üdə-gu</ta>
            <ta e="T25" id="Seg_657" s="T24">tat</ta>
            <ta e="T26" id="Seg_658" s="T25">kundɨ-qat</ta>
            <ta e="T27" id="Seg_659" s="T26">üǯa-ndə</ta>
            <ta e="T28" id="Seg_660" s="T27">tɨrdə</ta>
            <ta e="T29" id="Seg_661" s="T28">kuda-nɨn</ta>
            <ta e="T30" id="Seg_662" s="T29">warga-nde</ta>
            <ta e="T31" id="Seg_663" s="T30">nača-ɣɨt</ta>
            <ta e="T32" id="Seg_664" s="T31">Iːriša-nen</ta>
            <ta e="T33" id="Seg_665" s="T32">warka-ndə</ta>
            <ta e="T34" id="Seg_666" s="T33">kušak</ta>
            <ta e="T35" id="Seg_667" s="T34">me-špa-l</ta>
            <ta e="T36" id="Seg_668" s="T35">qumde-p</ta>
            <ta e="T37" id="Seg_669" s="T36">mad-ə-tko</ta>
            <ta e="T38" id="Seg_670" s="T37">na</ta>
            <ta e="T39" id="Seg_671" s="T38">škola-ɣɨt</ta>
            <ta e="T40" id="Seg_672" s="T39">warga-ndə</ta>
            <ta e="T41" id="Seg_673" s="T40">pasuda</ta>
            <ta e="T42" id="Seg_674" s="T41">lɨ-k</ta>
            <ta e="T43" id="Seg_675" s="T42">stolova-nde</ta>
            <ta e="T44" id="Seg_676" s="T43">kuja-špe-t</ta>
            <ta e="T45" id="Seg_677" s="T44">kušak</ta>
            <ta e="T46" id="Seg_678" s="T45">merǯe-špa-l</ta>
            <ta e="T47" id="Seg_679" s="T46">nača-t</ta>
            <ta e="T48" id="Seg_680" s="T47">ešo</ta>
            <ta e="T49" id="Seg_681" s="T48">e-je</ta>
            <ta e="T50" id="Seg_682" s="T49">warga-a-t</ta>
            <ta e="T51" id="Seg_683" s="T50">škola-ɣɨt</ta>
            <ta e="T52" id="Seg_684" s="T51">ili</ta>
            <ta e="T53" id="Seg_685" s="T52">tat</ta>
            <ta e="T54" id="Seg_686" s="T53">unenǯe</ta>
            <ta e="T55" id="Seg_687" s="T54">pel-galɨ-k</ta>
            <ta e="T56" id="Seg_688" s="T55">warga-ndə</ta>
            <ta e="T57" id="Seg_689" s="T56">a</ta>
            <ta e="T58" id="Seg_690" s="T57">sejčas</ta>
            <ta e="T59" id="Seg_691" s="T58">ku</ta>
            <ta e="T60" id="Seg_692" s="T59">kwe-la-nde</ta>
            <ta e="T61" id="Seg_693" s="T60">kilʼikejkin</ta>
            <ta e="T62" id="Seg_694" s="T61">oralǯiga</ta>
            <ta e="T63" id="Seg_695" s="T62">üt</ta>
            <ta e="T64" id="Seg_696" s="T63">šeːr-ba-də</ta>
            <ta e="T65" id="Seg_697" s="T64">ilʼi</ta>
            <ta e="T66" id="Seg_698" s="T65">qaj</ta>
            <ta e="T67" id="Seg_699" s="T66">lʼi</ta>
            <ta e="T68" id="Seg_700" s="T67">a</ta>
            <ta e="T69" id="Seg_701" s="T68">paja-də</ta>
            <ta e="T70" id="Seg_702" s="T69">qe-ɣan</ta>
            <ta e="T71" id="Seg_703" s="T70">eː-k</ta>
            <ta e="T72" id="Seg_704" s="T71">ondə</ta>
            <ta e="T73" id="Seg_705" s="T72">pel-galɨ-k</ta>
            <ta e="T74" id="Seg_706" s="T73">kuja-špa</ta>
            <ta e="T75" id="Seg_707" s="T74">tabə</ta>
            <ta e="T76" id="Seg_708" s="T75">aː</ta>
            <ta e="T77" id="Seg_709" s="T76">qwäl-ǯa</ta>
            <ta e="T78" id="Seg_710" s="T77">a</ta>
            <ta e="T79" id="Seg_711" s="T78">sejčas</ta>
            <ta e="T80" id="Seg_712" s="T79">qwä-la</ta>
            <ta e="T81" id="Seg_713" s="T80">kuːša-ɣɨt</ta>
            <ta e="T82" id="Seg_714" s="T81">tö-le</ta>
            <ta e="T83" id="Seg_715" s="T82">teka</ta>
            <ta e="T84" id="Seg_716" s="T83">tab-ə-t</ta>
            <ta e="T85" id="Seg_717" s="T84">paja-m-də</ta>
            <ta e="T86" id="Seg_718" s="T85">hoɣonči-ha-l</ta>
            <ta e="T87" id="Seg_719" s="T86">kučaɣɨt</ta>
            <ta e="T88" id="Seg_720" s="T87">kušiɣat</ta>
            <ta e="T89" id="Seg_721" s="T88">belka-p</ta>
            <ta e="T90" id="Seg_722" s="T89">qwat-pa-də</ta>
            <ta e="T91" id="Seg_723" s="T90">ilʼi</ta>
            <ta e="T92" id="Seg_724" s="T91">warge</ta>
            <ta e="T93" id="Seg_725" s="T92">huːru-p</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_726" s="T0">tan-nan</ta>
            <ta e="T2" id="Seg_727" s="T1">e-k</ta>
            <ta e="T3" id="Seg_728" s="T2">brat-la-lə</ta>
            <ta e="T4" id="Seg_729" s="T3">man-ɨ-n</ta>
            <ta e="T5" id="Seg_730" s="T4">e-k</ta>
            <ta e="T6" id="Seg_731" s="T5">brat</ta>
            <ta e="T7" id="Seg_732" s="T6">alka</ta>
            <ta e="T8" id="Seg_733" s="T7">qweːda-mbɨ-tɨ</ta>
            <ta e="T9" id="Seg_734" s="T8">kɨbače-p</ta>
            <ta e="T10" id="Seg_735" s="T9">tab-ɨ-n</ta>
            <ta e="T11" id="Seg_736" s="T10">paja-tɨ</ta>
            <ta e="T12" id="Seg_737" s="T11">po</ta>
            <ta e="T13" id="Seg_738" s="T12">paqe-lʼčǝ-gu</ta>
            <ta e="T14" id="Seg_739" s="T13">i</ta>
            <ta e="T15" id="Seg_740" s="T14">pat-gu</ta>
            <ta e="T16" id="Seg_741" s="T15">a</ta>
            <ta e="T17" id="Seg_742" s="T16">patom</ta>
            <ta e="T18" id="Seg_743" s="T17">merɨn-gu</ta>
            <ta e="T19" id="Seg_744" s="T18">na-tqo</ta>
            <ta e="T20" id="Seg_745" s="T19">qomde</ta>
            <ta e="T21" id="Seg_746" s="T20">me-la-di</ta>
            <ta e="T22" id="Seg_747" s="T21">na-p</ta>
            <ta e="T23" id="Seg_748" s="T22">qomde-p</ta>
            <ta e="T24" id="Seg_749" s="T23">üdɨ-gu</ta>
            <ta e="T25" id="Seg_750" s="T24">tan</ta>
            <ta e="T26" id="Seg_751" s="T25">kundɨ-qɨn</ta>
            <ta e="T27" id="Seg_752" s="T26">üdʼa-nd</ta>
            <ta e="T28" id="Seg_753" s="T27">tɨrdə</ta>
            <ta e="T29" id="Seg_754" s="T28">kutɨ-nan</ta>
            <ta e="T30" id="Seg_755" s="T29">wargɨ-nd</ta>
            <ta e="T31" id="Seg_756" s="T30">nača-qɨn</ta>
            <ta e="T32" id="Seg_757" s="T31">Iːriša-nan</ta>
            <ta e="T33" id="Seg_758" s="T32">wargɨ-nd</ta>
            <ta e="T34" id="Seg_759" s="T33">kuššak</ta>
            <ta e="T35" id="Seg_760" s="T34">me-špɨ-l</ta>
            <ta e="T36" id="Seg_761" s="T35">qomde-p</ta>
            <ta e="T37" id="Seg_762" s="T36">maːt-ɨ-tqo</ta>
            <ta e="T38" id="Seg_763" s="T37">na</ta>
            <ta e="T39" id="Seg_764" s="T38">škola-qɨn</ta>
            <ta e="T40" id="Seg_765" s="T39">wargɨ-nd</ta>
            <ta e="T41" id="Seg_766" s="T40">pasuda</ta>
            <ta e="T42" id="Seg_767" s="T41">lɨ-k</ta>
            <ta e="T43" id="Seg_768" s="T42">stalowaj-nde</ta>
            <ta e="T44" id="Seg_769" s="T43">koja-špɨ-nd</ta>
            <ta e="T45" id="Seg_770" s="T44">kuššak</ta>
            <ta e="T46" id="Seg_771" s="T45">merǯi-špɨ-l</ta>
            <ta e="T47" id="Seg_772" s="T46">nača-tɨ</ta>
            <ta e="T48" id="Seg_773" s="T47">iššo</ta>
            <ta e="T49" id="Seg_774" s="T48">e-ja</ta>
            <ta e="T50" id="Seg_775" s="T49">wargɨ-ɨ-dət</ta>
            <ta e="T51" id="Seg_776" s="T50">škola-qɨn</ta>
            <ta e="T52" id="Seg_777" s="T51">ali</ta>
            <ta e="T53" id="Seg_778" s="T52">tan</ta>
            <ta e="T54" id="Seg_779" s="T53">unenǯe</ta>
            <ta e="T55" id="Seg_780" s="T54">pelka-galɨ-k</ta>
            <ta e="T56" id="Seg_781" s="T55">wargɨ-nd</ta>
            <ta e="T57" id="Seg_782" s="T56">a</ta>
            <ta e="T58" id="Seg_783" s="T57">sečas</ta>
            <ta e="T59" id="Seg_784" s="T58">ku</ta>
            <ta e="T60" id="Seg_785" s="T59">qwän-la-nd</ta>
            <ta e="T61" id="Seg_786" s="T60">kilʼikejkin</ta>
            <ta e="T62" id="Seg_787" s="T61">oralǯiga</ta>
            <ta e="T63" id="Seg_788" s="T62">üt</ta>
            <ta e="T64" id="Seg_789" s="T63">əːšeːr-mbɨ-tɨ</ta>
            <ta e="T65" id="Seg_790" s="T64">ali</ta>
            <ta e="T66" id="Seg_791" s="T65">qaj</ta>
            <ta e="T67" id="Seg_792" s="T66">lʼi</ta>
            <ta e="T68" id="Seg_793" s="T67">a</ta>
            <ta e="T69" id="Seg_794" s="T68">paja-tɨ</ta>
            <ta e="T70" id="Seg_795" s="T69">%%-qɨn</ta>
            <ta e="T71" id="Seg_796" s="T70">e-k</ta>
            <ta e="T72" id="Seg_797" s="T71">onǯe</ta>
            <ta e="T73" id="Seg_798" s="T72">pelka-galɨ-k</ta>
            <ta e="T74" id="Seg_799" s="T73">koja-špɨ</ta>
            <ta e="T75" id="Seg_800" s="T74">tab</ta>
            <ta e="T76" id="Seg_801" s="T75">aː</ta>
            <ta e="T77" id="Seg_802" s="T76">qwel-nǯe</ta>
            <ta e="T78" id="Seg_803" s="T77">a</ta>
            <ta e="T79" id="Seg_804" s="T78">sečas</ta>
            <ta e="T80" id="Seg_805" s="T79">qwän-la</ta>
            <ta e="T81" id="Seg_806" s="T80">kuča-qɨn</ta>
            <ta e="T82" id="Seg_807" s="T81">töː-la</ta>
            <ta e="T83" id="Seg_808" s="T82">teːka</ta>
            <ta e="T84" id="Seg_809" s="T83">tab-ɨ-n</ta>
            <ta e="T85" id="Seg_810" s="T84">paja-m-tɨ</ta>
            <ta e="T86" id="Seg_811" s="T85">hoqonǯe-ŋɨ-l</ta>
            <ta e="T87" id="Seg_812" s="T86">kučaqɨt</ta>
            <ta e="T88" id="Seg_813" s="T87">kučaqɨt</ta>
            <ta e="T89" id="Seg_814" s="T88">belka-p</ta>
            <ta e="T90" id="Seg_815" s="T89">kwat-mbɨ-tɨ</ta>
            <ta e="T91" id="Seg_816" s="T90">ali</ta>
            <ta e="T92" id="Seg_817" s="T91">wargɨ</ta>
            <ta e="T93" id="Seg_818" s="T92">surup-p</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_819" s="T0">you.SG.NOM-ADES</ta>
            <ta e="T2" id="Seg_820" s="T1">be-3SG.S</ta>
            <ta e="T3" id="Seg_821" s="T2">brother-PL-2SG</ta>
            <ta e="T4" id="Seg_822" s="T3">I.NOM-EP-GEN</ta>
            <ta e="T5" id="Seg_823" s="T4">be-3SG.S</ta>
            <ta e="T6" id="Seg_824" s="T5">brother.[NOM]</ta>
            <ta e="T7" id="Seg_825" s="T6">%%</ta>
            <ta e="T8" id="Seg_826" s="T7">get.caught-PST.NAR-3SG.O</ta>
            <ta e="T9" id="Seg_827" s="T8">boy-ACC</ta>
            <ta e="T10" id="Seg_828" s="T9">(s)he-EP-GEN</ta>
            <ta e="T11" id="Seg_829" s="T10">wife.[NOM]-3SG</ta>
            <ta e="T12" id="Seg_830" s="T11">tree.[NOM]</ta>
            <ta e="T13" id="Seg_831" s="T12">pack-PFV-INF</ta>
            <ta e="T14" id="Seg_832" s="T13">and</ta>
            <ta e="T15" id="Seg_833" s="T14">put-INF</ta>
            <ta e="T16" id="Seg_834" s="T15">but</ta>
            <ta e="T17" id="Seg_835" s="T16">then</ta>
            <ta e="T18" id="Seg_836" s="T17">sell-INF</ta>
            <ta e="T19" id="Seg_837" s="T18">this-TRL</ta>
            <ta e="T20" id="Seg_838" s="T19">money.[NOM]</ta>
            <ta e="T21" id="Seg_839" s="T20">give-FUT-3DU.O</ta>
            <ta e="T22" id="Seg_840" s="T21">this-ACC</ta>
            <ta e="T23" id="Seg_841" s="T22">money-ACC</ta>
            <ta e="T24" id="Seg_842" s="T23">drink-INF</ta>
            <ta e="T25" id="Seg_843" s="T24">you.SG.NOM</ta>
            <ta e="T26" id="Seg_844" s="T25">long-LOC</ta>
            <ta e="T27" id="Seg_845" s="T26">work-2SG.S</ta>
            <ta e="T28" id="Seg_846" s="T27">%%</ta>
            <ta e="T29" id="Seg_847" s="T28">who-ADES</ta>
            <ta e="T30" id="Seg_848" s="T29">live-2SG.S</ta>
            <ta e="T31" id="Seg_849" s="T30">there-LOC</ta>
            <ta e="T32" id="Seg_850" s="T31">Irisha-ADES</ta>
            <ta e="T33" id="Seg_851" s="T32">live-2SG.S</ta>
            <ta e="T34" id="Seg_852" s="T33">how.many</ta>
            <ta e="T35" id="Seg_853" s="T34">give-IPFV2-2SG.O</ta>
            <ta e="T36" id="Seg_854" s="T35">money-ACC</ta>
            <ta e="T37" id="Seg_855" s="T36">house-EP-TRL</ta>
            <ta e="T38" id="Seg_856" s="T37">this</ta>
            <ta e="T39" id="Seg_857" s="T38">school-LOC</ta>
            <ta e="T40" id="Seg_858" s="T39">live-2SG.S</ta>
            <ta e="T41" id="Seg_859" s="T40">dishes.[NOM]</ta>
            <ta e="T42" id="Seg_860" s="T41">be-3SG.S</ta>
            <ta e="T43" id="Seg_861" s="T42">canteen-ILL</ta>
            <ta e="T44" id="Seg_862" s="T43">go-IPFV2-2SG.S</ta>
            <ta e="T45" id="Seg_863" s="T44">how.many</ta>
            <ta e="T46" id="Seg_864" s="T45">pay-IPFV2-2SG.O</ta>
            <ta e="T47" id="Seg_865" s="T46">there-ADV.LOC</ta>
            <ta e="T48" id="Seg_866" s="T47">still</ta>
            <ta e="T49" id="Seg_867" s="T48">be-CO.[3SG.S]</ta>
            <ta e="T50" id="Seg_868" s="T49">live-EP-3PL</ta>
            <ta e="T51" id="Seg_869" s="T50">school-LOC</ta>
            <ta e="T52" id="Seg_870" s="T51">or</ta>
            <ta e="T53" id="Seg_871" s="T52">you.SG.NOM</ta>
            <ta e="T54" id="Seg_872" s="T53">alone</ta>
            <ta e="T55" id="Seg_873" s="T54">half-CAR-ADVZ</ta>
            <ta e="T56" id="Seg_874" s="T55">live-2SG.S</ta>
            <ta e="T57" id="Seg_875" s="T56">but</ta>
            <ta e="T58" id="Seg_876" s="T57">now</ta>
            <ta e="T59" id="Seg_877" s="T58">where</ta>
            <ta e="T60" id="Seg_878" s="T59">go.away-FUT-2SG.S</ta>
            <ta e="T61" id="Seg_879" s="T60">Kilikejkin.[NOM]</ta>
            <ta e="T62" id="Seg_880" s="T61">%%</ta>
            <ta e="T63" id="Seg_881" s="T62">water.[NOM]</ta>
            <ta e="T64" id="Seg_882" s="T63">get.drunk-PST.NAR-3SG.O</ta>
            <ta e="T65" id="Seg_883" s="T64">or</ta>
            <ta e="T66" id="Seg_884" s="T65">what</ta>
            <ta e="T67" id="Seg_885" s="T66">whether</ta>
            <ta e="T68" id="Seg_886" s="T67">but</ta>
            <ta e="T69" id="Seg_887" s="T68">old.woman.[NOM]-3SG</ta>
            <ta e="T70" id="Seg_888" s="T69">%%-LOC</ta>
            <ta e="T71" id="Seg_889" s="T70">be-3SG.S</ta>
            <ta e="T72" id="Seg_890" s="T71">oneself.3SG.[NOM]</ta>
            <ta e="T73" id="Seg_891" s="T72">half-CAR-ADVZ</ta>
            <ta e="T74" id="Seg_892" s="T73">go-IPFV2.[3SG.S]</ta>
            <ta e="T75" id="Seg_893" s="T74">(s)he.[NOM]</ta>
            <ta e="T76" id="Seg_894" s="T75">NEG</ta>
            <ta e="T77" id="Seg_895" s="T76">fish-IPFV3.[3SG.S]</ta>
            <ta e="T78" id="Seg_896" s="T77">but</ta>
            <ta e="T79" id="Seg_897" s="T78">now</ta>
            <ta e="T80" id="Seg_898" s="T79">go.away-FUT.[3SG.S]</ta>
            <ta e="T81" id="Seg_899" s="T80">when-LOC</ta>
            <ta e="T82" id="Seg_900" s="T81">come-FUT.[3SG.S]</ta>
            <ta e="T83" id="Seg_901" s="T82">you.SG.ALL</ta>
            <ta e="T84" id="Seg_902" s="T83">(s)he-EP-GEN</ta>
            <ta e="T85" id="Seg_903" s="T84">wife-ACC-3SG</ta>
            <ta e="T86" id="Seg_904" s="T85">ask-CO-2SG.O</ta>
            <ta e="T87" id="Seg_905" s="T86">how.many</ta>
            <ta e="T88" id="Seg_906" s="T87">how.many</ta>
            <ta e="T89" id="Seg_907" s="T88">squirrel-ACC</ta>
            <ta e="T90" id="Seg_908" s="T89">kill-PST.NAR-3SG.O</ta>
            <ta e="T91" id="Seg_909" s="T90">or</ta>
            <ta e="T92" id="Seg_910" s="T91">big</ta>
            <ta e="T93" id="Seg_911" s="T92">wild.animal-ACC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_912" s="T0">ты.NOM-ADES</ta>
            <ta e="T2" id="Seg_913" s="T1">быть-3SG.S</ta>
            <ta e="T3" id="Seg_914" s="T2">брат-PL-2SG</ta>
            <ta e="T4" id="Seg_915" s="T3">я.NOM-EP-GEN</ta>
            <ta e="T5" id="Seg_916" s="T4">быть-3SG.S</ta>
            <ta e="T6" id="Seg_917" s="T5">брат.[NOM]</ta>
            <ta e="T7" id="Seg_918" s="T6">%%</ta>
            <ta e="T8" id="Seg_919" s="T7">попасться-PST.NAR-3SG.O</ta>
            <ta e="T9" id="Seg_920" s="T8">мальчик-ACC</ta>
            <ta e="T10" id="Seg_921" s="T9">он(а)-EP-GEN</ta>
            <ta e="T11" id="Seg_922" s="T10">жена.[NOM]-3SG</ta>
            <ta e="T12" id="Seg_923" s="T11">дерево.[NOM]</ta>
            <ta e="T13" id="Seg_924" s="T12">сложить-PFV-INF</ta>
            <ta e="T14" id="Seg_925" s="T13">и</ta>
            <ta e="T15" id="Seg_926" s="T14">положить-INF</ta>
            <ta e="T16" id="Seg_927" s="T15">а</ta>
            <ta e="T17" id="Seg_928" s="T16">потом</ta>
            <ta e="T18" id="Seg_929" s="T17">продать-INF</ta>
            <ta e="T19" id="Seg_930" s="T18">этот-TRL</ta>
            <ta e="T20" id="Seg_931" s="T19">деньги.[NOM]</ta>
            <ta e="T21" id="Seg_932" s="T20">дать-FUT-3DU.O</ta>
            <ta e="T22" id="Seg_933" s="T21">этот-ACC</ta>
            <ta e="T23" id="Seg_934" s="T22">деньги-ACC</ta>
            <ta e="T24" id="Seg_935" s="T23">пить-INF</ta>
            <ta e="T25" id="Seg_936" s="T24">ты.NOM</ta>
            <ta e="T26" id="Seg_937" s="T25">длинный-LOC</ta>
            <ta e="T27" id="Seg_938" s="T26">работать-2SG.S</ta>
            <ta e="T28" id="Seg_939" s="T27">%%</ta>
            <ta e="T29" id="Seg_940" s="T28">кто-ADES</ta>
            <ta e="T30" id="Seg_941" s="T29">жить-2SG.S</ta>
            <ta e="T31" id="Seg_942" s="T30">туда-LOC</ta>
            <ta e="T32" id="Seg_943" s="T31">Ириша-ADES</ta>
            <ta e="T33" id="Seg_944" s="T32">жить-2SG.S</ta>
            <ta e="T34" id="Seg_945" s="T33">сколько</ta>
            <ta e="T35" id="Seg_946" s="T34">дать-IPFV2-2SG.O</ta>
            <ta e="T36" id="Seg_947" s="T35">деньги-ACC</ta>
            <ta e="T37" id="Seg_948" s="T36">дом-EP-TRL</ta>
            <ta e="T38" id="Seg_949" s="T37">этот</ta>
            <ta e="T39" id="Seg_950" s="T38">школа-LOC</ta>
            <ta e="T40" id="Seg_951" s="T39">жить-2SG.S</ta>
            <ta e="T41" id="Seg_952" s="T40">посуда.[NOM]</ta>
            <ta e="T42" id="Seg_953" s="T41">быть-3SG.S</ta>
            <ta e="T43" id="Seg_954" s="T42">столовая-ILL</ta>
            <ta e="T44" id="Seg_955" s="T43">ходить-IPFV2-2SG.S</ta>
            <ta e="T45" id="Seg_956" s="T44">сколько</ta>
            <ta e="T46" id="Seg_957" s="T45">платить-IPFV2-2SG.O</ta>
            <ta e="T47" id="Seg_958" s="T46">туда-ADV.LOC</ta>
            <ta e="T48" id="Seg_959" s="T47">еще</ta>
            <ta e="T49" id="Seg_960" s="T48">быть-CO.[3SG.S]</ta>
            <ta e="T50" id="Seg_961" s="T49">жить-EP-3PL</ta>
            <ta e="T51" id="Seg_962" s="T50">школа-LOC</ta>
            <ta e="T52" id="Seg_963" s="T51">или</ta>
            <ta e="T53" id="Seg_964" s="T52">ты.NOM</ta>
            <ta e="T54" id="Seg_965" s="T53">в.одиночестве</ta>
            <ta e="T55" id="Seg_966" s="T54">половина-CAR-ADVZ</ta>
            <ta e="T56" id="Seg_967" s="T55">жить-2SG.S</ta>
            <ta e="T57" id="Seg_968" s="T56">а</ta>
            <ta e="T58" id="Seg_969" s="T57">сейчас</ta>
            <ta e="T59" id="Seg_970" s="T58">куда</ta>
            <ta e="T60" id="Seg_971" s="T59">пойти-FUT-2SG.S</ta>
            <ta e="T61" id="Seg_972" s="T60">Киликейкин.[NOM]</ta>
            <ta e="T62" id="Seg_973" s="T61">%%</ta>
            <ta e="T63" id="Seg_974" s="T62">вода.[NOM]</ta>
            <ta e="T64" id="Seg_975" s="T63">опьянеть-PST.NAR-3SG.O</ta>
            <ta e="T65" id="Seg_976" s="T64">или</ta>
            <ta e="T66" id="Seg_977" s="T65">что</ta>
            <ta e="T67" id="Seg_978" s="T66">что.ли</ta>
            <ta e="T68" id="Seg_979" s="T67">а</ta>
            <ta e="T69" id="Seg_980" s="T68">старуха.[NOM]-3SG</ta>
            <ta e="T70" id="Seg_981" s="T69">%%-LOC</ta>
            <ta e="T71" id="Seg_982" s="T70">быть-3SG.S</ta>
            <ta e="T72" id="Seg_983" s="T71">сам.3SG.[NOM]</ta>
            <ta e="T73" id="Seg_984" s="T72">половина-CAR-ADVZ</ta>
            <ta e="T74" id="Seg_985" s="T73">ходить-IPFV2.[3SG.S]</ta>
            <ta e="T75" id="Seg_986" s="T74">он(а).[NOM]</ta>
            <ta e="T76" id="Seg_987" s="T75">NEG</ta>
            <ta e="T77" id="Seg_988" s="T76">рыба-IPFV3.[3SG.S]</ta>
            <ta e="T78" id="Seg_989" s="T77">а</ta>
            <ta e="T79" id="Seg_990" s="T78">сейчас</ta>
            <ta e="T80" id="Seg_991" s="T79">пойти-FUT.[3SG.S]</ta>
            <ta e="T81" id="Seg_992" s="T80">когда-LOC</ta>
            <ta e="T82" id="Seg_993" s="T81">прийти-FUT.[3SG.S]</ta>
            <ta e="T83" id="Seg_994" s="T82">ты.ALL</ta>
            <ta e="T84" id="Seg_995" s="T83">он(а)-EP-GEN</ta>
            <ta e="T85" id="Seg_996" s="T84">жена-ACC-3SG</ta>
            <ta e="T86" id="Seg_997" s="T85">спрашивать-CO-2SG.O</ta>
            <ta e="T87" id="Seg_998" s="T86">сколько</ta>
            <ta e="T88" id="Seg_999" s="T87">сколько</ta>
            <ta e="T89" id="Seg_1000" s="T88">белка-ACC</ta>
            <ta e="T90" id="Seg_1001" s="T89">убить-PST.NAR-3SG.O</ta>
            <ta e="T91" id="Seg_1002" s="T90">или</ta>
            <ta e="T92" id="Seg_1003" s="T91">большой</ta>
            <ta e="T93" id="Seg_1004" s="T92">зверь-ACC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1005" s="T0">pers-n:case</ta>
            <ta e="T2" id="Seg_1006" s="T1">v-v:pn</ta>
            <ta e="T3" id="Seg_1007" s="T2">n-n:num-n:poss</ta>
            <ta e="T4" id="Seg_1008" s="T3">pers-n:ins-n:case</ta>
            <ta e="T5" id="Seg_1009" s="T4">v-v:pn</ta>
            <ta e="T6" id="Seg_1010" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_1011" s="T6">n</ta>
            <ta e="T8" id="Seg_1012" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_1013" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_1014" s="T9">pers-n:ins-n:case</ta>
            <ta e="T11" id="Seg_1015" s="T10">n-n:case-n:poss</ta>
            <ta e="T12" id="Seg_1016" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_1017" s="T12">v-v&gt;v-v:inf</ta>
            <ta e="T14" id="Seg_1018" s="T13">conj</ta>
            <ta e="T15" id="Seg_1019" s="T14">v-v:inf</ta>
            <ta e="T16" id="Seg_1020" s="T15">conj</ta>
            <ta e="T17" id="Seg_1021" s="T16">adv</ta>
            <ta e="T18" id="Seg_1022" s="T17">v-v:inf</ta>
            <ta e="T19" id="Seg_1023" s="T18">dem-n:case</ta>
            <ta e="T20" id="Seg_1024" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_1025" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_1026" s="T21">dem-n:case</ta>
            <ta e="T23" id="Seg_1027" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_1028" s="T23">v-v:inf</ta>
            <ta e="T25" id="Seg_1029" s="T24">pers</ta>
            <ta e="T26" id="Seg_1030" s="T25">adv-n:case</ta>
            <ta e="T27" id="Seg_1031" s="T26">v-v:pn</ta>
            <ta e="T28" id="Seg_1032" s="T27">adv</ta>
            <ta e="T29" id="Seg_1033" s="T28">interrog-n:case</ta>
            <ta e="T30" id="Seg_1034" s="T29">v-v:pn</ta>
            <ta e="T31" id="Seg_1035" s="T30">adv-n:case</ta>
            <ta e="T32" id="Seg_1036" s="T31">nprop-n:case</ta>
            <ta e="T33" id="Seg_1037" s="T32">v-v:pn</ta>
            <ta e="T34" id="Seg_1038" s="T33">interrog</ta>
            <ta e="T35" id="Seg_1039" s="T34">v-v&gt;v-v:pn</ta>
            <ta e="T36" id="Seg_1040" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_1041" s="T36">n-n:ins-n:case</ta>
            <ta e="T38" id="Seg_1042" s="T37">dem</ta>
            <ta e="T39" id="Seg_1043" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_1044" s="T39">v-v:pn</ta>
            <ta e="T41" id="Seg_1045" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_1046" s="T41">v-v:pn</ta>
            <ta e="T43" id="Seg_1047" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_1048" s="T43">v-v&gt;v-v:pn</ta>
            <ta e="T45" id="Seg_1049" s="T44">interrog</ta>
            <ta e="T46" id="Seg_1050" s="T45">v-v&gt;v-v:pn</ta>
            <ta e="T47" id="Seg_1051" s="T46">adv-adv:case</ta>
            <ta e="T48" id="Seg_1052" s="T47">adv</ta>
            <ta e="T49" id="Seg_1053" s="T48">v-v:ins-v:pn</ta>
            <ta e="T50" id="Seg_1054" s="T49">v-n:ins-v:pn</ta>
            <ta e="T51" id="Seg_1055" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_1056" s="T51">conj</ta>
            <ta e="T53" id="Seg_1057" s="T52">pers</ta>
            <ta e="T54" id="Seg_1058" s="T53">adv</ta>
            <ta e="T55" id="Seg_1059" s="T54">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T56" id="Seg_1060" s="T55">v-v:pn</ta>
            <ta e="T57" id="Seg_1061" s="T56">conj</ta>
            <ta e="T58" id="Seg_1062" s="T57">adv</ta>
            <ta e="T59" id="Seg_1063" s="T58">interrog</ta>
            <ta e="T60" id="Seg_1064" s="T59">v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_1065" s="T60">nprop-n:case</ta>
            <ta e="T63" id="Seg_1066" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_1067" s="T63">v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_1068" s="T64">conj</ta>
            <ta e="T66" id="Seg_1069" s="T65">interrog</ta>
            <ta e="T67" id="Seg_1070" s="T66">conj</ta>
            <ta e="T68" id="Seg_1071" s="T67">conj</ta>
            <ta e="T69" id="Seg_1072" s="T68">n-n:case-n:poss</ta>
            <ta e="T70" id="Seg_1073" s="T69">-n:case</ta>
            <ta e="T71" id="Seg_1074" s="T70">v-v:pn</ta>
            <ta e="T72" id="Seg_1075" s="T71">emphpro-n:case</ta>
            <ta e="T73" id="Seg_1076" s="T72">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T74" id="Seg_1077" s="T73">v-v&gt;v-v:pn</ta>
            <ta e="T75" id="Seg_1078" s="T74">pers-n:case</ta>
            <ta e="T76" id="Seg_1079" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_1080" s="T76">n-v&gt;v-v:pn</ta>
            <ta e="T78" id="Seg_1081" s="T77">conj</ta>
            <ta e="T79" id="Seg_1082" s="T78">adv</ta>
            <ta e="T80" id="Seg_1083" s="T79">v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_1084" s="T80">interrog-n:case</ta>
            <ta e="T82" id="Seg_1085" s="T81">v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_1086" s="T82">pers</ta>
            <ta e="T84" id="Seg_1087" s="T83">pers-n:ins-n:case</ta>
            <ta e="T85" id="Seg_1088" s="T84">n-n:case-n:poss</ta>
            <ta e="T86" id="Seg_1089" s="T85">v-v:ins-v:pn</ta>
            <ta e="T87" id="Seg_1090" s="T86">interrog</ta>
            <ta e="T88" id="Seg_1091" s="T87">interrog</ta>
            <ta e="T89" id="Seg_1092" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_1093" s="T89">v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_1094" s="T90">conj</ta>
            <ta e="T92" id="Seg_1095" s="T91">adj</ta>
            <ta e="T93" id="Seg_1096" s="T92">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1097" s="T0">pers</ta>
            <ta e="T2" id="Seg_1098" s="T1">v</ta>
            <ta e="T3" id="Seg_1099" s="T2">n</ta>
            <ta e="T4" id="Seg_1100" s="T3">pers</ta>
            <ta e="T5" id="Seg_1101" s="T4">v</ta>
            <ta e="T6" id="Seg_1102" s="T5">n</ta>
            <ta e="T7" id="Seg_1103" s="T6">n</ta>
            <ta e="T8" id="Seg_1104" s="T7">v</ta>
            <ta e="T9" id="Seg_1105" s="T8">n</ta>
            <ta e="T10" id="Seg_1106" s="T9">pers</ta>
            <ta e="T11" id="Seg_1107" s="T10">n</ta>
            <ta e="T12" id="Seg_1108" s="T11">n</ta>
            <ta e="T13" id="Seg_1109" s="T12">v</ta>
            <ta e="T14" id="Seg_1110" s="T13">conj</ta>
            <ta e="T15" id="Seg_1111" s="T14">v</ta>
            <ta e="T16" id="Seg_1112" s="T15">conj</ta>
            <ta e="T17" id="Seg_1113" s="T16">adv</ta>
            <ta e="T18" id="Seg_1114" s="T17">v</ta>
            <ta e="T19" id="Seg_1115" s="T18">dem</ta>
            <ta e="T20" id="Seg_1116" s="T19">n</ta>
            <ta e="T21" id="Seg_1117" s="T20">v</ta>
            <ta e="T22" id="Seg_1118" s="T21">dem</ta>
            <ta e="T23" id="Seg_1119" s="T22">n</ta>
            <ta e="T24" id="Seg_1120" s="T23">v</ta>
            <ta e="T25" id="Seg_1121" s="T24">pers</ta>
            <ta e="T26" id="Seg_1122" s="T25">adv</ta>
            <ta e="T27" id="Seg_1123" s="T26">v</ta>
            <ta e="T28" id="Seg_1124" s="T27">adv</ta>
            <ta e="T29" id="Seg_1125" s="T28">interrog</ta>
            <ta e="T30" id="Seg_1126" s="T29">v</ta>
            <ta e="T31" id="Seg_1127" s="T30">adv</ta>
            <ta e="T32" id="Seg_1128" s="T31">nprop</ta>
            <ta e="T33" id="Seg_1129" s="T32">v</ta>
            <ta e="T34" id="Seg_1130" s="T33">interrog</ta>
            <ta e="T35" id="Seg_1131" s="T34">v</ta>
            <ta e="T36" id="Seg_1132" s="T35">n</ta>
            <ta e="T37" id="Seg_1133" s="T36">n</ta>
            <ta e="T38" id="Seg_1134" s="T37">pro</ta>
            <ta e="T39" id="Seg_1135" s="T38">n</ta>
            <ta e="T40" id="Seg_1136" s="T39">v</ta>
            <ta e="T41" id="Seg_1137" s="T40">n</ta>
            <ta e="T42" id="Seg_1138" s="T41">v</ta>
            <ta e="T43" id="Seg_1139" s="T42">n</ta>
            <ta e="T44" id="Seg_1140" s="T43">v</ta>
            <ta e="T45" id="Seg_1141" s="T44">interrog</ta>
            <ta e="T46" id="Seg_1142" s="T45">v</ta>
            <ta e="T47" id="Seg_1143" s="T46">adv</ta>
            <ta e="T48" id="Seg_1144" s="T47">adv</ta>
            <ta e="T49" id="Seg_1145" s="T48">v</ta>
            <ta e="T50" id="Seg_1146" s="T49">v</ta>
            <ta e="T51" id="Seg_1147" s="T50">n</ta>
            <ta e="T52" id="Seg_1148" s="T51">conj</ta>
            <ta e="T53" id="Seg_1149" s="T52">pers</ta>
            <ta e="T54" id="Seg_1150" s="T53">adv</ta>
            <ta e="T55" id="Seg_1151" s="T54">adv</ta>
            <ta e="T56" id="Seg_1152" s="T55">v</ta>
            <ta e="T57" id="Seg_1153" s="T56">conj</ta>
            <ta e="T58" id="Seg_1154" s="T57">adv</ta>
            <ta e="T59" id="Seg_1155" s="T58">interrog</ta>
            <ta e="T60" id="Seg_1156" s="T59">v</ta>
            <ta e="T61" id="Seg_1157" s="T60">nprop</ta>
            <ta e="T63" id="Seg_1158" s="T62">n</ta>
            <ta e="T64" id="Seg_1159" s="T63">v</ta>
            <ta e="T65" id="Seg_1160" s="T64">conj</ta>
            <ta e="T66" id="Seg_1161" s="T65">interrog</ta>
            <ta e="T67" id="Seg_1162" s="T66">conj</ta>
            <ta e="T68" id="Seg_1163" s="T67">conj</ta>
            <ta e="T69" id="Seg_1164" s="T68">n</ta>
            <ta e="T71" id="Seg_1165" s="T70">v</ta>
            <ta e="T72" id="Seg_1166" s="T71">emphpro</ta>
            <ta e="T73" id="Seg_1167" s="T72">adv</ta>
            <ta e="T74" id="Seg_1168" s="T73">v</ta>
            <ta e="T75" id="Seg_1169" s="T74">pers</ta>
            <ta e="T76" id="Seg_1170" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_1171" s="T76">v</ta>
            <ta e="T78" id="Seg_1172" s="T77">conj</ta>
            <ta e="T79" id="Seg_1173" s="T78">adv</ta>
            <ta e="T80" id="Seg_1174" s="T79">v</ta>
            <ta e="T81" id="Seg_1175" s="T80">conj</ta>
            <ta e="T82" id="Seg_1176" s="T81">v</ta>
            <ta e="T83" id="Seg_1177" s="T82">pers</ta>
            <ta e="T84" id="Seg_1178" s="T83">pers</ta>
            <ta e="T85" id="Seg_1179" s="T84">n</ta>
            <ta e="T86" id="Seg_1180" s="T85">v</ta>
            <ta e="T87" id="Seg_1181" s="T86">interrog</ta>
            <ta e="T88" id="Seg_1182" s="T87">interrog</ta>
            <ta e="T89" id="Seg_1183" s="T88">n</ta>
            <ta e="T90" id="Seg_1184" s="T89">v</ta>
            <ta e="T91" id="Seg_1185" s="T90">conj</ta>
            <ta e="T92" id="Seg_1186" s="T91">adj</ta>
            <ta e="T93" id="Seg_1187" s="T92">n</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1188" s="T1">v:pred</ta>
            <ta e="T3" id="Seg_1189" s="T2">np.h:S</ta>
            <ta e="T5" id="Seg_1190" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_1191" s="T5">np.h:S</ta>
            <ta e="T8" id="Seg_1192" s="T7">v:pred</ta>
            <ta e="T9" id="Seg_1193" s="T8">np.h:O</ta>
            <ta e="T11" id="Seg_1194" s="T10">np.h:S</ta>
            <ta e="T15" id="Seg_1195" s="T11">s:purp</ta>
            <ta e="T18" id="Seg_1196" s="T15">s:purp</ta>
            <ta e="T20" id="Seg_1197" s="T19">np:O</ta>
            <ta e="T21" id="Seg_1198" s="T20">0.3.h:S v:pred</ta>
            <ta e="T25" id="Seg_1199" s="T24">pro.h:S</ta>
            <ta e="T27" id="Seg_1200" s="T26">v:pred</ta>
            <ta e="T30" id="Seg_1201" s="T29">0.2.h:S v:pred</ta>
            <ta e="T33" id="Seg_1202" s="T32">0.2.h:S v:pred</ta>
            <ta e="T35" id="Seg_1203" s="T34">0.2.h:S v:pred</ta>
            <ta e="T36" id="Seg_1204" s="T35">np:O</ta>
            <ta e="T40" id="Seg_1205" s="T39">0.2.h:S v:pred</ta>
            <ta e="T41" id="Seg_1206" s="T40">np:S</ta>
            <ta e="T42" id="Seg_1207" s="T41">v:pred</ta>
            <ta e="T44" id="Seg_1208" s="T43">0.2.h:S v:pred</ta>
            <ta e="T46" id="Seg_1209" s="T45">0.2.h:S v:pred</ta>
            <ta e="T49" id="Seg_1210" s="T48">0.3.h:S v:pred</ta>
            <ta e="T50" id="Seg_1211" s="T49">0.3.h:S v:pred</ta>
            <ta e="T53" id="Seg_1212" s="T52">pro.h:S</ta>
            <ta e="T54" id="Seg_1213" s="T53">adj:pred</ta>
            <ta e="T56" id="Seg_1214" s="T55">0.2.h:S v:pred</ta>
            <ta e="T60" id="Seg_1215" s="T59">0.2.h:S v:pred</ta>
            <ta e="T61" id="Seg_1216" s="T60">np.h:S</ta>
            <ta e="T63" id="Seg_1217" s="T62">np:O</ta>
            <ta e="T64" id="Seg_1218" s="T63">v:pred</ta>
            <ta e="T69" id="Seg_1219" s="T68">np.h:S</ta>
            <ta e="T71" id="Seg_1220" s="T70">v:pred</ta>
            <ta e="T72" id="Seg_1221" s="T71">pro.h:S</ta>
            <ta e="T74" id="Seg_1222" s="T73">v:pred</ta>
            <ta e="T75" id="Seg_1223" s="T74">pro.h:S</ta>
            <ta e="T77" id="Seg_1224" s="T76">v:pred</ta>
            <ta e="T81" id="Seg_1225" s="T77">s:temp</ta>
            <ta e="T82" id="Seg_1226" s="T81">0.3.h:S v:pred</ta>
            <ta e="T85" id="Seg_1227" s="T84">np.h:O</ta>
            <ta e="T86" id="Seg_1228" s="T85">0.2.h:S v:pred</ta>
            <ta e="T89" id="Seg_1229" s="T88">np:O</ta>
            <ta e="T90" id="Seg_1230" s="T89">0.3.h:S v:pred</ta>
            <ta e="T93" id="Seg_1231" s="T92">np:O</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_1232" s="T0">pro.h:Poss</ta>
            <ta e="T3" id="Seg_1233" s="T2">np.h:Th</ta>
            <ta e="T4" id="Seg_1234" s="T3">pro.h:Poss</ta>
            <ta e="T6" id="Seg_1235" s="T5">np.h:Th</ta>
            <ta e="T9" id="Seg_1236" s="T8">np.h:P</ta>
            <ta e="T10" id="Seg_1237" s="T9">pro.h:Poss</ta>
            <ta e="T11" id="Seg_1238" s="T10">np.h:A</ta>
            <ta e="T12" id="Seg_1239" s="T11">np:Th</ta>
            <ta e="T17" id="Seg_1240" s="T16">adv:Time</ta>
            <ta e="T20" id="Seg_1241" s="T19">np:Th</ta>
            <ta e="T21" id="Seg_1242" s="T20">0.3.h:A</ta>
            <ta e="T25" id="Seg_1243" s="T24">pro.h:A</ta>
            <ta e="T29" id="Seg_1244" s="T28">pro:L</ta>
            <ta e="T30" id="Seg_1245" s="T29">0.2.h:Th</ta>
            <ta e="T31" id="Seg_1246" s="T30">adv:L</ta>
            <ta e="T32" id="Seg_1247" s="T31">np:L</ta>
            <ta e="T33" id="Seg_1248" s="T32">0.2.h:Th</ta>
            <ta e="T35" id="Seg_1249" s="T34">0.2.h:A</ta>
            <ta e="T36" id="Seg_1250" s="T35">np:Th</ta>
            <ta e="T39" id="Seg_1251" s="T38">np:L</ta>
            <ta e="T40" id="Seg_1252" s="T39">0.2.h:Th</ta>
            <ta e="T41" id="Seg_1253" s="T40">np:Th</ta>
            <ta e="T43" id="Seg_1254" s="T42">np:G</ta>
            <ta e="T44" id="Seg_1255" s="T43">0.2.h:A</ta>
            <ta e="T46" id="Seg_1256" s="T45">0.2.h:A</ta>
            <ta e="T47" id="Seg_1257" s="T46">adv:L</ta>
            <ta e="T49" id="Seg_1258" s="T48">0.3.h:Th</ta>
            <ta e="T50" id="Seg_1259" s="T49">0.3.h:Th</ta>
            <ta e="T51" id="Seg_1260" s="T50">np:L</ta>
            <ta e="T53" id="Seg_1261" s="T52">pro.h:Th</ta>
            <ta e="T56" id="Seg_1262" s="T55">0.2.h:Th</ta>
            <ta e="T58" id="Seg_1263" s="T57">adv:Time</ta>
            <ta e="T59" id="Seg_1264" s="T58">pro:G</ta>
            <ta e="T60" id="Seg_1265" s="T59">0.2.h:A</ta>
            <ta e="T61" id="Seg_1266" s="T60">np.h:A</ta>
            <ta e="T69" id="Seg_1267" s="T68">0.3.h:Poss np.h:Th</ta>
            <ta e="T70" id="Seg_1268" s="T69">np:L</ta>
            <ta e="T72" id="Seg_1269" s="T71">pro.h:A</ta>
            <ta e="T75" id="Seg_1270" s="T74">pro.h:A</ta>
            <ta e="T79" id="Seg_1271" s="T78">adv:Time</ta>
            <ta e="T80" id="Seg_1272" s="T79">0.3.h:A</ta>
            <ta e="T82" id="Seg_1273" s="T81">0.3.h:A</ta>
            <ta e="T83" id="Seg_1274" s="T82">pro:G</ta>
            <ta e="T84" id="Seg_1275" s="T83">pro.h:Poss</ta>
            <ta e="T85" id="Seg_1276" s="T84">np.h:Th</ta>
            <ta e="T86" id="Seg_1277" s="T85">0.2.h:A</ta>
            <ta e="T89" id="Seg_1278" s="T88">np:P</ta>
            <ta e="T90" id="Seg_1279" s="T89">0.3.h:A</ta>
            <ta e="T93" id="Seg_1280" s="T92">np:P</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_1281" s="T2">RUS:core</ta>
            <ta e="T6" id="Seg_1282" s="T5">RUS:core</ta>
            <ta e="T14" id="Seg_1283" s="T13">RUS:gram</ta>
            <ta e="T16" id="Seg_1284" s="T15">RUS:gram</ta>
            <ta e="T17" id="Seg_1285" s="T16">RUS:core</ta>
            <ta e="T39" id="Seg_1286" s="T38">RUS:cult</ta>
            <ta e="T41" id="Seg_1287" s="T40">RUS:cult</ta>
            <ta e="T43" id="Seg_1288" s="T42">RUS:cult</ta>
            <ta e="T48" id="Seg_1289" s="T47">RUS:gram</ta>
            <ta e="T51" id="Seg_1290" s="T50">RUS:cult</ta>
            <ta e="T52" id="Seg_1291" s="T51">RUS:gram</ta>
            <ta e="T57" id="Seg_1292" s="T56">RUS:gram</ta>
            <ta e="T58" id="Seg_1293" s="T57">RUS:core</ta>
            <ta e="T65" id="Seg_1294" s="T64">RUS:gram</ta>
            <ta e="T68" id="Seg_1295" s="T67">RUS:gram</ta>
            <ta e="T78" id="Seg_1296" s="T77">RUS:gram</ta>
            <ta e="T79" id="Seg_1297" s="T78">RUS:core</ta>
            <ta e="T91" id="Seg_1298" s="T90">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_1299" s="T0">У тебя есть братья?</ta>
            <ta e="T7" id="Seg_1300" s="T3">У меня есть брат.</ta>
            <ta e="T11" id="Seg_1301" s="T7">Его жена родила мальчишку.</ta>
            <ta e="T18" id="Seg_1302" s="T11">Дрова сложить и положить, а потом продать.</ta>
            <ta e="T21" id="Seg_1303" s="T18">За это деньги дадут.</ta>
            <ta e="T24" id="Seg_1304" s="T21">На эти деньги пить.</ta>
            <ta e="T28" id="Seg_1305" s="T24">Ты долго работаешь здесь.</ta>
            <ta e="T30" id="Seg_1306" s="T28">У кого живешь?</ta>
            <ta e="T33" id="Seg_1307" s="T30">Там у Ириши живешь.</ta>
            <ta e="T37" id="Seg_1308" s="T33">Сколько денег даешь за дом (за квартиру)?</ta>
            <ta e="T40" id="Seg_1309" s="T37">В школе живешь.</ta>
            <ta e="T42" id="Seg_1310" s="T40">Посуда есть.</ta>
            <ta e="T44" id="Seg_1311" s="T42">В столовую ходишь.</ta>
            <ta e="T46" id="Seg_1312" s="T44">Сколько платишь (за еду)?</ta>
            <ta e="T54" id="Seg_1313" s="T46">Там еще есть, живут в школе или ты одна?</ta>
            <ta e="T56" id="Seg_1314" s="T54">Ты одна живешь.</ta>
            <ta e="T60" id="Seg_1315" s="T56">А сейчас куда идешь?</ta>
            <ta e="T67" id="Seg_1316" s="T60">Киликейкин (пьяный)? или что ли.</ta>
            <ta e="T71" id="Seg_1317" s="T67">А старуха в (?) .</ta>
            <ta e="T74" id="Seg_1318" s="T71">Он один ходит.</ta>
            <ta e="T77" id="Seg_1319" s="T74">Она не рыбачит.</ta>
            <ta e="T83" id="Seg_1320" s="T77">А сейчас придет когда, (сколько) придет к тебе.</ta>
            <ta e="T93" id="Seg_1321" s="T83">Ты его старуху спрашивала, сколько белок убили или медведей.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_1322" s="T0">Do you have brothers?</ta>
            <ta e="T7" id="Seg_1323" s="T3">I have a brother.</ta>
            <ta e="T11" id="Seg_1324" s="T7">His wife gave birth to a boy.</ta>
            <ta e="T18" id="Seg_1325" s="T11">To pack and to put firewood, and then to sell it.</ta>
            <ta e="T21" id="Seg_1326" s="T18">They will give money for it.</ta>
            <ta e="T24" id="Seg_1327" s="T21">To drink using this money.</ta>
            <ta e="T28" id="Seg_1328" s="T24">You have been working here for a long time.</ta>
            <ta e="T30" id="Seg_1329" s="T28">Who do you live with?</ta>
            <ta e="T33" id="Seg_1330" s="T30">You live there with Irisha.</ta>
            <ta e="T37" id="Seg_1331" s="T33">How much do you pay?</ta>
            <ta e="T40" id="Seg_1332" s="T37">You live in the school.</ta>
            <ta e="T42" id="Seg_1333" s="T40">There are dishes there.</ta>
            <ta e="T44" id="Seg_1334" s="T42">You go to the canteen.</ta>
            <ta e="T46" id="Seg_1335" s="T44">How much do you pay (for food)?</ta>
            <ta e="T54" id="Seg_1336" s="T46">Is there anyone else living in the school or are you alone?</ta>
            <ta e="T56" id="Seg_1337" s="T54">You live alone.</ta>
            <ta e="T60" id="Seg_1338" s="T56">And where are you going now?</ta>
            <ta e="T67" id="Seg_1339" s="T60">Kilikejkin is (drunk)? or what.</ta>
            <ta e="T71" id="Seg_1340" s="T67">And his wife is in (?).</ta>
            <ta e="T74" id="Seg_1341" s="T71">He walks alone.</ta>
            <ta e="T77" id="Seg_1342" s="T74">She doesn't go fishing.</ta>
            <ta e="T83" id="Seg_1343" s="T77">And when she comes now, she'll come to you.</ta>
            <ta e="T93" id="Seg_1344" s="T83">You asked his wife, how many squirrels or bears were killed.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_1345" s="T0">Hast du Brüder?</ta>
            <ta e="T7" id="Seg_1346" s="T3">Ich habe einen Bruder, …</ta>
            <ta e="T11" id="Seg_1347" s="T7">Seine Frau gebar einen Jungen.</ta>
            <ta e="T18" id="Seg_1348" s="T11">Feuerholz zusammenpacken und hinlegen, und es dann verkaufen.</ta>
            <ta e="T21" id="Seg_1349" s="T18">Sie werden dafür Geld geben.</ta>
            <ta e="T24" id="Seg_1350" s="T21">Um mit diesem Geld zu trinken.</ta>
            <ta e="T28" id="Seg_1351" s="T24">Du hast hier lange gearbeitet.</ta>
            <ta e="T30" id="Seg_1352" s="T28">Bei wem wohnst du?</ta>
            <ta e="T33" id="Seg_1353" s="T30">Du wohnst dort bei Irischa.</ta>
            <ta e="T37" id="Seg_1354" s="T33">Wie viel bezahlst du?</ta>
            <ta e="T40" id="Seg_1355" s="T37">Du wohnst in der Schule.</ta>
            <ta e="T42" id="Seg_1356" s="T40">Es gibt dort Verpflegung.</ta>
            <ta e="T44" id="Seg_1357" s="T42">Du gehst in die Kantine.</ta>
            <ta e="T46" id="Seg_1358" s="T44">Wie viel bezahlst du (für das Essen)?</ta>
            <ta e="T54" id="Seg_1359" s="T46">Lebt dort noch jemanden in der Schule oder bist du allein?</ta>
            <ta e="T56" id="Seg_1360" s="T54">Du lebst allein.</ta>
            <ta e="T60" id="Seg_1361" s="T56">Und wohin gehst du jetzt?</ta>
            <ta e="T67" id="Seg_1362" s="T60">Kilikejkin ist (betrunken)? oder was.</ta>
            <ta e="T71" id="Seg_1363" s="T67">Und seine Frau ist in (?).</ta>
            <ta e="T74" id="Seg_1364" s="T71">Er läuft allein.</ta>
            <ta e="T77" id="Seg_1365" s="T74">Sie geht nicht fischen.</ta>
            <ta e="T83" id="Seg_1366" s="T77">Und wenn sie jetzt kommt, kommt sie zu dir.</ta>
            <ta e="T93" id="Seg_1367" s="T83">Du hast seine Frau gefragt, wie viele Eichhörnchen oder Bären getötet wurden.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_1368" s="T0">у тебя есть братья</ta>
            <ta e="T11" id="Seg_1369" s="T7">родила мальчишку</ta>
            <ta e="T18" id="Seg_1370" s="T11">дрова сложить дрова продать</ta>
            <ta e="T21" id="Seg_1371" s="T18">за это деньги дадут</ta>
            <ta e="T24" id="Seg_1372" s="T21">на эти деньги пить</ta>
            <ta e="T28" id="Seg_1373" s="T24">ты долго работаешь здесь</ta>
            <ta e="T30" id="Seg_1374" s="T28">у кого живешь</ta>
            <ta e="T33" id="Seg_1375" s="T30">там у Ириши живешь</ta>
            <ta e="T37" id="Seg_1376" s="T33">сколько даешь денег за дом (за квартиру)</ta>
            <ta e="T40" id="Seg_1377" s="T37">в школе живешь</ta>
            <ta e="T42" id="Seg_1378" s="T40">посуда есть</ta>
            <ta e="T44" id="Seg_1379" s="T42">В столовую ходишь.</ta>
            <ta e="T46" id="Seg_1380" s="T44">сколько платишь (за еду)</ta>
            <ta e="T54" id="Seg_1381" s="T46">там еще есть живут в школе или ты одна</ta>
            <ta e="T56" id="Seg_1382" s="T54">живешь</ta>
            <ta e="T60" id="Seg_1383" s="T56">сейчас куда идешь</ta>
            <ta e="T67" id="Seg_1384" s="T60">старик нет</ta>
            <ta e="T71" id="Seg_1385" s="T67">а старуха</ta>
            <ta e="T74" id="Seg_1386" s="T71">он один ходит</ta>
            <ta e="T77" id="Seg_1387" s="T74">она не рыбачит</ta>
            <ta e="T83" id="Seg_1388" s="T77">придет когда (сколько) придет к тебе</ta>
            <ta e="T93" id="Seg_1389" s="T83">его старуху спрашивала сколько белок убили или медведей</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T3" id="Seg_1390" s="T0">WNB: agreement is not correct.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
