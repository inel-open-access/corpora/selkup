<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>MKP_1964_MyGrandmother_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">MKP_1964_MyGrandmother_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">288</ud-information>
            <ud-information attribute-name="# HIAT:w">215</ud-information>
            <ud-information attribute-name="# e">215</ud-information>
            <ud-information attribute-name="# HIAT:u">55</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="MKP">
            <abbreviation>MKP</abbreviation>
            <sex value="f" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="MKP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T216" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">niːpamni</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">ton</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">pottə</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">iːsklaj</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">iːlapsan</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">üčeŋ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">elli</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_33" n="HIAT:ip">(</nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">üčeŋ</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">ela</ts>
                  <nts id="Seg_39" n="HIAT:ip">)</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">tipindamba</ts>
                  <nts id="Seg_43" n="HIAT:ip">.</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_46" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">nar</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">pon</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">iːlɨsati</ts>
                  <nts id="Seg_55" n="HIAT:ip">,</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">üče</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">tʼäŋus</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_65" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">nänno</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">okkar</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">äːsus</ts>
                  <nts id="Seg_74" n="HIAT:ip">,</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">quːsan</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">eːzuŋ</ts>
                  <nts id="Seg_81" n="HIAT:ip">.</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_84" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">nagur</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_89" n="HIAT:w" s="T23">pon</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_92" n="HIAT:w" s="T24">iːlɨsati</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_96" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">üčela</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_101" n="HIAT:w" s="T26">qotʼi</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_104" n="HIAT:w" s="T27">eːzus</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_108" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_110" n="HIAT:w" s="T28">kundir</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_113" n="HIAT:w" s="T29">orapsut</ts>
                  <nts id="Seg_114" n="HIAT:ip">?</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_117" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_119" n="HIAT:w" s="T30">nʼünʼükʼelʼa</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_122" n="HIAT:w" s="T31">quːklusattə</ts>
                  <nts id="Seg_123" n="HIAT:ip">.</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_126" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_128" n="HIAT:w" s="T32">nipam</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_131" n="HIAT:w" s="T33">i</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_134" n="HIAT:w" s="T34">iːlʼdʼäm</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_137" n="HIAT:w" s="T35">latatuksat</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_141" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_143" n="HIAT:w" s="T36">iːrrat</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_146" n="HIAT:w" s="T37">üːtam</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_149" n="HIAT:w" s="T38">särpɨlʼe</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_152" n="HIAT:w" s="T39">tɨttərukus</ts>
                  <nts id="Seg_153" n="HIAT:ip">,</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_156" n="HIAT:w" s="T40">qwotumbikus</ts>
                  <nts id="Seg_157" n="HIAT:ip">.</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_160" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_162" n="HIAT:w" s="T41">äsan</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_165" n="HIAT:w" s="T42">tʼürumbi-t</ts>
                  <nts id="Seg_166" n="HIAT:ip">.</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_169" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_171" n="HIAT:w" s="T43">täp</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_174" n="HIAT:w" s="T44">tipindis</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_177" n="HIAT:w" s="T45">ssəruj</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_180" n="HIAT:w" s="T46">quwanni</ts>
                  <nts id="Seg_181" n="HIAT:ip">.</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_184" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_186" n="HIAT:w" s="T47">täːpaːnnan</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_189" n="HIAT:w" s="T48">nʼünʼü</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_192" n="HIAT:w" s="T49">üčet</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_195" n="HIAT:w" s="T50">ässan</ts>
                  <nts id="Seg_196" n="HIAT:ip">.</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_199" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_201" n="HIAT:w" s="T51">niːpam</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_204" n="HIAT:w" s="T52">täːpam</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_207" n="HIAT:w" s="T53">oramǯisat</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_210" n="HIAT:w" s="T54">i</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_213" n="HIAT:w" s="T55">nätsit</ts>
                  <nts id="Seg_214" n="HIAT:ip">.</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_217" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_219" n="HIAT:w" s="T56">täpqinnap</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_222" n="HIAT:w" s="T57">okkar</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_225" n="HIAT:w" s="T58">näɣuwaj</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_228" n="HIAT:w" s="T59">üːčetiː</ts>
                  <nts id="Seg_229" n="HIAT:ip">.</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_232" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_234" n="HIAT:w" s="T60">ondi</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_237" n="HIAT:w" s="T61">tʼärruŋ</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_240" n="HIAT:w" s="T62">quːsati</ts>
                  <nts id="Seg_241" n="HIAT:ip">.</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_244" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_246" n="HIAT:w" s="T63">niːpannan</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_249" n="HIAT:w" s="T64">wes</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_252" n="HIAT:w" s="T65">üčelat</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_255" n="HIAT:w" s="T66">aːnukoj</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_258" n="HIAT:w" s="T67">irradnannä</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_261" n="HIAT:w" s="T68">quːmbat</ts>
                  <nts id="Seg_262" n="HIAT:ip">.</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_265" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_267" n="HIAT:w" s="T69">naːkur</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_270" n="HIAT:w" s="T70">nät</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_273" n="HIAT:w" s="T71">pirreqɨnt</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_276" n="HIAT:w" s="T72">mitelʼewlʼe</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_279" n="HIAT:w" s="T73">nännɨ</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_282" n="HIAT:w" s="T74">qumbat</ts>
                  <nts id="Seg_283" n="HIAT:ip">.</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_286" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_288" n="HIAT:w" s="T75">aːnukoj</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_291" n="HIAT:w" s="T76">irrat</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_294" n="HIAT:w" s="T77">maːtqan</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_297" n="HIAT:w" s="T78">küːtlʼe</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_300" n="HIAT:w" s="T79">quːssan</ts>
                  <nts id="Seg_301" n="HIAT:ip">.</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_304" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_306" n="HIAT:w" s="T80">nännɨ</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_309" n="HIAT:w" s="T81">ondə</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_312" n="HIAT:w" s="T82">ilakus</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_315" n="HIAT:w" s="T83">kundə</ts>
                  <nts id="Seg_316" n="HIAT:ip">.</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_319" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_321" n="HIAT:w" s="T84">nännɨ</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_324" n="HIAT:w" s="T85">tipindisan</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_327" n="HIAT:w" s="T86">araŋ</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_330" n="HIAT:w" s="T87">irranni</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_332" n="HIAT:ip">(</nts>
                  <ts e="T89" id="Seg_334" n="HIAT:w" s="T88">quwanni</ts>
                  <nts id="Seg_335" n="HIAT:ip">)</nts>
                  <nts id="Seg_336" n="HIAT:ip">.</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_339" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_341" n="HIAT:w" s="T89">täp</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_344" n="HIAT:w" s="T90">äsan</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_347" n="HIAT:w" s="T91">ssərumbitaj</ts>
                  <nts id="Seg_348" n="HIAT:ip">.</nts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_351" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_353" n="HIAT:w" s="T92">täpannan</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_356" n="HIAT:w" s="T93">aːnukoj</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_359" n="HIAT:w" s="T94">näɣumnänan</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_362" n="HIAT:w" s="T95">šɨttɨ</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_365" n="HIAT:w" s="T96">üːčet</ts>
                  <nts id="Seg_366" n="HIAT:ip">.</nts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_369" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_371" n="HIAT:w" s="T97">nän</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_374" n="HIAT:w" s="T98">ondi</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_377" n="HIAT:w" s="T99">üčelati</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_380" n="HIAT:w" s="T100">ezus</ts>
                  <nts id="Seg_381" n="HIAT:ip">.</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_384" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_386" n="HIAT:w" s="T101">kun</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_389" n="HIAT:w" s="T102">qöniŋ</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_392" n="HIAT:w" s="T103">eːsan</ts>
                  <nts id="Seg_393" n="HIAT:ip">,</nts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_396" n="HIAT:w" s="T104">täp</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_399" n="HIAT:w" s="T105">äwalʼǯimbat</ts>
                  <nts id="Seg_400" n="HIAT:ip">.</nts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_403" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_405" n="HIAT:w" s="T106">šɨttäɣ</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_408" n="HIAT:w" s="T107">oːraːpsati</ts>
                  <nts id="Seg_409" n="HIAT:ip">.</nts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_412" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_414" n="HIAT:w" s="T108">okkar</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_417" n="HIAT:w" s="T109">orapsan</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_420" n="HIAT:w" s="T110">i</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_423" n="HIAT:w" s="T111">üːt</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_426" n="HIAT:w" s="T112">koŋnan</ts>
                  <nts id="Seg_427" n="HIAT:ip">.</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_430" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_432" n="HIAT:w" s="T113">okkə</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_435" n="HIAT:w" s="T114">jat</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_438" n="HIAT:w" s="T115">iːlaŋ</ts>
                  <nts id="Seg_439" n="HIAT:ip">.</nts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_442" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_444" n="HIAT:w" s="T116">tibeɣuwanni</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_447" n="HIAT:w" s="T117">tʼärəsattə</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_450" n="HIAT:w" s="T118">kuːnambitaj</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_452" n="HIAT:ip">(</nts>
                  <ts e="T120" id="Seg_454" n="HIAT:w" s="T119">qunpä</ts>
                  <nts id="Seg_455" n="HIAT:ip">)</nts>
                  <nts id="Seg_456" n="HIAT:ip">,</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_459" n="HIAT:w" s="T120">qunpäm</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_462" n="HIAT:w" s="T121">qwandəku</ts>
                  <nts id="Seg_463" n="HIAT:ip">.</nts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_466" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_468" n="HIAT:w" s="T122">täp</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_471" n="HIAT:w" s="T123">qwandɨsɨt</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_474" n="HIAT:w" s="T124">nändʼisä</ts>
                  <nts id="Seg_475" n="HIAT:ip">.</nts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_478" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_480" n="HIAT:w" s="T125">qunpä</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_483" n="HIAT:w" s="T126">täpqiːm</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_486" n="HIAT:w" s="T127">qwatpat</ts>
                  <nts id="Seg_487" n="HIAT:ip">.</nts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_490" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_492" n="HIAT:w" s="T128">nämdɨ</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_495" n="HIAT:w" s="T129">assɨ</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_498" n="HIAT:w" s="T130">qombatit</ts>
                  <nts id="Seg_499" n="HIAT:ip">.</nts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_502" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_504" n="HIAT:w" s="T131">täpam</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_507" n="HIAT:w" s="T132">oralpatat</ts>
                  <nts id="Seg_508" n="HIAT:ip">,</nts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_511" n="HIAT:w" s="T133">omdalʼǯimbat</ts>
                  <nts id="Seg_512" n="HIAT:ip">.</nts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_515" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_517" n="HIAT:w" s="T134">näkwaj</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_520" n="HIAT:w" s="T135">üče</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_523" n="HIAT:w" s="T136">soqandʼüklʼe</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_526" n="HIAT:w" s="T137">kutʼä</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_529" n="HIAT:w" s="T138">qatpumbalʼ</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_532" n="HIAT:w" s="T139">täːpɨ</ts>
                  <nts id="Seg_533" n="HIAT:ip">.</nts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_536" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_538" n="HIAT:w" s="T140">assɨ</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_541" n="HIAT:w" s="T141">qässat</ts>
                  <nts id="Seg_542" n="HIAT:ip">.</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_545" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_547" n="HIAT:w" s="T142">täp</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_550" n="HIAT:w" s="T143">tʼäraŋ</ts>
                  <nts id="Seg_551" n="HIAT:ip">:</nts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_554" n="HIAT:w" s="T144">tʼäŋoj</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_557" n="HIAT:w" s="T145">wättoɨn</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_560" n="HIAT:w" s="T146">qwänba</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_563" n="HIAT:w" s="T147">maːttə</ts>
                  <nts id="Seg_564" n="HIAT:ip">.</nts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_567" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_569" n="HIAT:w" s="T148">täp</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_572" n="HIAT:w" s="T149">sitʼeptɨs</ts>
                  <nts id="Seg_573" n="HIAT:ip">.</nts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_576" n="HIAT:u" s="T150">
                  <ts e="T151" id="Seg_578" n="HIAT:w" s="T150">täp</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_581" n="HIAT:w" s="T151">kuːda</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_584" n="HIAT:w" s="T152">qatpɨmbat</ts>
                  <nts id="Seg_585" n="HIAT:ip">.</nts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_588" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_590" n="HIAT:w" s="T153">nipa</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_593" n="HIAT:w" s="T154">qalʼiŋ</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_596" n="HIAT:w" s="T155">apʼätʼ</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_599" n="HIAT:w" s="T156">irrakaːlʼäk</ts>
                  <nts id="Seg_600" n="HIAT:ip">.</nts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_603" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_605" n="HIAT:w" s="T157">narɨmdʼelʼǯij</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_608" n="HIAT:w" s="T158">iːrrat</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_611" n="HIAT:w" s="T159">matʼtʼöɣɨn</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_614" n="HIAT:w" s="T160">tüː</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_617" n="HIAT:w" s="T161">ambat</ts>
                  <nts id="Seg_618" n="HIAT:ip">.</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_621" n="HIAT:u" s="T162">
                  <ts e="T163" id="Seg_623" n="HIAT:w" s="T162">täp</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_626" n="HIAT:w" s="T163">illass</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_629" n="HIAT:w" s="T164">iːndɨsä</ts>
                  <nts id="Seg_630" n="HIAT:ip">.</nts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_633" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_635" n="HIAT:w" s="T165">nännɨ</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_638" n="HIAT:w" s="T166">na</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_641" n="HIAT:w" s="T167">iːt</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_644" n="HIAT:w" s="T168">qoqsan</ts>
                  <nts id="Seg_645" n="HIAT:ip">.</nts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_648" n="HIAT:u" s="T169">
                  <ts e="T170" id="Seg_650" n="HIAT:w" s="T169">täp</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_653" n="HIAT:w" s="T170">äːssan</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_656" n="HIAT:w" s="T171">nar</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_659" n="HIAT:w" s="T172">sarum</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_661" n="HIAT:ip">(</nts>
                  <ts e="T174" id="Seg_663" n="HIAT:w" s="T173">saraj</ts>
                  <nts id="Seg_664" n="HIAT:ip">)</nts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_667" n="HIAT:w" s="T174">pəttə</ts>
                  <nts id="Seg_668" n="HIAT:ip">.</nts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T180" id="Seg_671" n="HIAT:u" s="T175">
                  <ts e="T176" id="Seg_673" n="HIAT:w" s="T175">niːpam</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_676" n="HIAT:w" s="T176">illas</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_679" n="HIAT:w" s="T177">matʼtʼöɣɨn</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_682" n="HIAT:w" s="T178">üːčelandse</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_685" n="HIAT:w" s="T179">nʼäjalʼe</ts>
                  <nts id="Seg_686" n="HIAT:ip">.</nts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_689" n="HIAT:u" s="T180">
                  <ts e="T181" id="Seg_691" n="HIAT:w" s="T180">i</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_694" n="HIAT:w" s="T181">näti</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_697" n="HIAT:w" s="T182">ont</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_700" n="HIAT:w" s="T183">sijamt</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_703" n="HIAT:w" s="T184">tʼäčambat</ts>
                  <nts id="Seg_704" n="HIAT:ip">,</nts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_707" n="HIAT:w" s="T185">kənnamdi</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_710" n="HIAT:w" s="T186">qätenǯilʼe</ts>
                  <nts id="Seg_711" n="HIAT:ip">.</nts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_714" n="HIAT:u" s="T187">
                  <ts e="T188" id="Seg_716" n="HIAT:w" s="T187">nätɨ</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_719" n="HIAT:w" s="T188">ukon</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_722" n="HIAT:w" s="T189">qwässɨ</ts>
                  <nts id="Seg_723" n="HIAT:ip">.</nts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_726" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_728" n="HIAT:w" s="T190">nännɨ</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_731" n="HIAT:w" s="T191">niːpam</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_734" n="HIAT:w" s="T192">qwässɨ</ts>
                  <nts id="Seg_735" n="HIAT:ip">.</nts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_738" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_740" n="HIAT:w" s="T193">niːpam</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_743" n="HIAT:w" s="T194">tüssan</ts>
                  <nts id="Seg_744" n="HIAT:ip">,</nts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_747" n="HIAT:w" s="T195">täp</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_750" n="HIAT:w" s="T196">uš</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_753" n="HIAT:w" s="T197">qumba</ts>
                  <nts id="Seg_754" n="HIAT:ip">.</nts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_757" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_759" n="HIAT:w" s="T198">niːpam</ts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_762" n="HIAT:w" s="T199">kɨsikus</ts>
                  <nts id="Seg_763" n="HIAT:ip">.</nts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T203" id="Seg_766" n="HIAT:u" s="T200">
                  <ts e="T201" id="Seg_768" n="HIAT:w" s="T200">qanǯisä</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_771" n="HIAT:w" s="T201">maːttə</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_774" n="HIAT:w" s="T202">taːssaːtit</ts>
                  <nts id="Seg_775" n="HIAT:ip">.</nts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_778" n="HIAT:u" s="T203">
                  <ts e="T204" id="Seg_780" n="HIAT:w" s="T203">kundɨ</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_783" n="HIAT:w" s="T204">maːtqan</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_786" n="HIAT:w" s="T205">iːppis</ts>
                  <nts id="Seg_787" n="HIAT:ip">.</nts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_790" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_792" n="HIAT:w" s="T206">tüssan</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_795" n="HIAT:w" s="T207">vrač</ts>
                  <nts id="Seg_796" n="HIAT:ip">,</nts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_799" n="HIAT:w" s="T208">manǯesit</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_802" n="HIAT:w" s="T209">i</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_805" n="HIAT:w" s="T210">tʼäras</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_808" n="HIAT:w" s="T211">oːtʼäŋu</ts>
                  <nts id="Seg_809" n="HIAT:ip">.</nts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_812" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_814" n="HIAT:w" s="T212">oːtʼäksot</ts>
                  <nts id="Seg_815" n="HIAT:ip">.</nts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T216" id="Seg_818" n="HIAT:u" s="T213">
                  <ts e="T214" id="Seg_820" n="HIAT:w" s="T213">nʼärnä</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_823" n="HIAT:w" s="T214">iːlilʼe</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_826" n="HIAT:w" s="T215">oldʼisot</ts>
                  <nts id="Seg_827" n="HIAT:ip">.</nts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T216" id="Seg_829" n="sc" s="T1">
               <ts e="T2" id="Seg_831" n="e" s="T1">man </ts>
               <ts e="T3" id="Seg_833" n="e" s="T2">niːpamni </ts>
               <ts e="T4" id="Seg_835" n="e" s="T3">ton </ts>
               <ts e="T5" id="Seg_837" n="e" s="T4">pottə. </ts>
               <ts e="T6" id="Seg_839" n="e" s="T5">iːsklaj </ts>
               <ts e="T7" id="Seg_841" n="e" s="T6">iːlapsan. </ts>
               <ts e="T8" id="Seg_843" n="e" s="T7">üčeŋ </ts>
               <ts e="T9" id="Seg_845" n="e" s="T8">elli </ts>
               <ts e="T10" id="Seg_847" n="e" s="T9">(üčeŋ </ts>
               <ts e="T11" id="Seg_849" n="e" s="T10">ela) </ts>
               <ts e="T12" id="Seg_851" n="e" s="T11">tipindamba. </ts>
               <ts e="T13" id="Seg_853" n="e" s="T12">nar </ts>
               <ts e="T14" id="Seg_855" n="e" s="T13">pon </ts>
               <ts e="T15" id="Seg_857" n="e" s="T14">iːlɨsati, </ts>
               <ts e="T16" id="Seg_859" n="e" s="T15">üče </ts>
               <ts e="T17" id="Seg_861" n="e" s="T16">tʼäŋus. </ts>
               <ts e="T18" id="Seg_863" n="e" s="T17">nänno </ts>
               <ts e="T19" id="Seg_865" n="e" s="T18">okkar </ts>
               <ts e="T20" id="Seg_867" n="e" s="T19">äːsus, </ts>
               <ts e="T21" id="Seg_869" n="e" s="T20">quːsan </ts>
               <ts e="T22" id="Seg_871" n="e" s="T21">eːzuŋ. </ts>
               <ts e="T23" id="Seg_873" n="e" s="T22">nagur </ts>
               <ts e="T24" id="Seg_875" n="e" s="T23">pon </ts>
               <ts e="T25" id="Seg_877" n="e" s="T24">iːlɨsati. </ts>
               <ts e="T26" id="Seg_879" n="e" s="T25">üčela </ts>
               <ts e="T27" id="Seg_881" n="e" s="T26">qotʼi </ts>
               <ts e="T28" id="Seg_883" n="e" s="T27">eːzus. </ts>
               <ts e="T29" id="Seg_885" n="e" s="T28">kundir </ts>
               <ts e="T30" id="Seg_887" n="e" s="T29">orapsut? </ts>
               <ts e="T31" id="Seg_889" n="e" s="T30">nʼünʼükʼelʼa </ts>
               <ts e="T32" id="Seg_891" n="e" s="T31">quːklusattə. </ts>
               <ts e="T33" id="Seg_893" n="e" s="T32">nipam </ts>
               <ts e="T34" id="Seg_895" n="e" s="T33">i </ts>
               <ts e="T35" id="Seg_897" n="e" s="T34">iːlʼdʼäm </ts>
               <ts e="T36" id="Seg_899" n="e" s="T35">latatuksat. </ts>
               <ts e="T37" id="Seg_901" n="e" s="T36">iːrrat </ts>
               <ts e="T38" id="Seg_903" n="e" s="T37">üːtam </ts>
               <ts e="T39" id="Seg_905" n="e" s="T38">särpɨlʼe </ts>
               <ts e="T40" id="Seg_907" n="e" s="T39">tɨttərukus, </ts>
               <ts e="T41" id="Seg_909" n="e" s="T40">qwotumbikus. </ts>
               <ts e="T42" id="Seg_911" n="e" s="T41">äsan </ts>
               <ts e="T43" id="Seg_913" n="e" s="T42">tʼürumbi-t. </ts>
               <ts e="T44" id="Seg_915" n="e" s="T43">täp </ts>
               <ts e="T45" id="Seg_917" n="e" s="T44">tipindis </ts>
               <ts e="T46" id="Seg_919" n="e" s="T45">ssəruj </ts>
               <ts e="T47" id="Seg_921" n="e" s="T46">quwanni. </ts>
               <ts e="T48" id="Seg_923" n="e" s="T47">täːpaːnnan </ts>
               <ts e="T49" id="Seg_925" n="e" s="T48">nʼünʼü </ts>
               <ts e="T50" id="Seg_927" n="e" s="T49">üčet </ts>
               <ts e="T51" id="Seg_929" n="e" s="T50">ässan. </ts>
               <ts e="T52" id="Seg_931" n="e" s="T51">niːpam </ts>
               <ts e="T53" id="Seg_933" n="e" s="T52">täːpam </ts>
               <ts e="T54" id="Seg_935" n="e" s="T53">oramǯisat </ts>
               <ts e="T55" id="Seg_937" n="e" s="T54">i </ts>
               <ts e="T56" id="Seg_939" n="e" s="T55">nätsit. </ts>
               <ts e="T57" id="Seg_941" n="e" s="T56">täpqinnap </ts>
               <ts e="T58" id="Seg_943" n="e" s="T57">okkar </ts>
               <ts e="T59" id="Seg_945" n="e" s="T58">näɣuwaj </ts>
               <ts e="T60" id="Seg_947" n="e" s="T59">üːčetiː. </ts>
               <ts e="T61" id="Seg_949" n="e" s="T60">ondi </ts>
               <ts e="T62" id="Seg_951" n="e" s="T61">tʼärruŋ </ts>
               <ts e="T63" id="Seg_953" n="e" s="T62">quːsati. </ts>
               <ts e="T64" id="Seg_955" n="e" s="T63">niːpannan </ts>
               <ts e="T65" id="Seg_957" n="e" s="T64">wes </ts>
               <ts e="T66" id="Seg_959" n="e" s="T65">üčelat </ts>
               <ts e="T67" id="Seg_961" n="e" s="T66">aːnukoj </ts>
               <ts e="T68" id="Seg_963" n="e" s="T67">irradnannä </ts>
               <ts e="T69" id="Seg_965" n="e" s="T68">quːmbat. </ts>
               <ts e="T70" id="Seg_967" n="e" s="T69">naːkur </ts>
               <ts e="T71" id="Seg_969" n="e" s="T70">nät </ts>
               <ts e="T72" id="Seg_971" n="e" s="T71">pirreqɨnt </ts>
               <ts e="T73" id="Seg_973" n="e" s="T72">mitelʼewlʼe </ts>
               <ts e="T74" id="Seg_975" n="e" s="T73">nännɨ </ts>
               <ts e="T75" id="Seg_977" n="e" s="T74">qumbat. </ts>
               <ts e="T76" id="Seg_979" n="e" s="T75">aːnukoj </ts>
               <ts e="T77" id="Seg_981" n="e" s="T76">irrat </ts>
               <ts e="T78" id="Seg_983" n="e" s="T77">maːtqan </ts>
               <ts e="T79" id="Seg_985" n="e" s="T78">küːtlʼe </ts>
               <ts e="T80" id="Seg_987" n="e" s="T79">quːssan. </ts>
               <ts e="T81" id="Seg_989" n="e" s="T80">nännɨ </ts>
               <ts e="T82" id="Seg_991" n="e" s="T81">ondə </ts>
               <ts e="T83" id="Seg_993" n="e" s="T82">ilakus </ts>
               <ts e="T84" id="Seg_995" n="e" s="T83">kundə. </ts>
               <ts e="T85" id="Seg_997" n="e" s="T84">nännɨ </ts>
               <ts e="T86" id="Seg_999" n="e" s="T85">tipindisan </ts>
               <ts e="T87" id="Seg_1001" n="e" s="T86">araŋ </ts>
               <ts e="T88" id="Seg_1003" n="e" s="T87">irranni </ts>
               <ts e="T89" id="Seg_1005" n="e" s="T88">(quwanni). </ts>
               <ts e="T90" id="Seg_1007" n="e" s="T89">täp </ts>
               <ts e="T91" id="Seg_1009" n="e" s="T90">äsan </ts>
               <ts e="T92" id="Seg_1011" n="e" s="T91">ssərumbitaj. </ts>
               <ts e="T93" id="Seg_1013" n="e" s="T92">täpannan </ts>
               <ts e="T94" id="Seg_1015" n="e" s="T93">aːnukoj </ts>
               <ts e="T95" id="Seg_1017" n="e" s="T94">näɣumnänan </ts>
               <ts e="T96" id="Seg_1019" n="e" s="T95">šɨttɨ </ts>
               <ts e="T97" id="Seg_1021" n="e" s="T96">üːčet. </ts>
               <ts e="T98" id="Seg_1023" n="e" s="T97">nän </ts>
               <ts e="T99" id="Seg_1025" n="e" s="T98">ondi </ts>
               <ts e="T100" id="Seg_1027" n="e" s="T99">üčelati </ts>
               <ts e="T101" id="Seg_1029" n="e" s="T100">ezus. </ts>
               <ts e="T102" id="Seg_1031" n="e" s="T101">kun </ts>
               <ts e="T103" id="Seg_1033" n="e" s="T102">qöniŋ </ts>
               <ts e="T104" id="Seg_1035" n="e" s="T103">eːsan, </ts>
               <ts e="T105" id="Seg_1037" n="e" s="T104">täp </ts>
               <ts e="T106" id="Seg_1039" n="e" s="T105">äwalʼǯimbat. </ts>
               <ts e="T107" id="Seg_1041" n="e" s="T106">šɨttäɣ </ts>
               <ts e="T108" id="Seg_1043" n="e" s="T107">oːraːpsati. </ts>
               <ts e="T109" id="Seg_1045" n="e" s="T108">okkar </ts>
               <ts e="T110" id="Seg_1047" n="e" s="T109">orapsan </ts>
               <ts e="T111" id="Seg_1049" n="e" s="T110">i </ts>
               <ts e="T112" id="Seg_1051" n="e" s="T111">üːt </ts>
               <ts e="T113" id="Seg_1053" n="e" s="T112">koŋnan. </ts>
               <ts e="T114" id="Seg_1055" n="e" s="T113">okkə </ts>
               <ts e="T115" id="Seg_1057" n="e" s="T114">jat </ts>
               <ts e="T116" id="Seg_1059" n="e" s="T115">iːlaŋ. </ts>
               <ts e="T117" id="Seg_1061" n="e" s="T116">tibeɣuwanni </ts>
               <ts e="T118" id="Seg_1063" n="e" s="T117">tʼärəsattə </ts>
               <ts e="T119" id="Seg_1065" n="e" s="T118">kuːnambitaj </ts>
               <ts e="T120" id="Seg_1067" n="e" s="T119">(qunpä), </ts>
               <ts e="T121" id="Seg_1069" n="e" s="T120">qunpäm </ts>
               <ts e="T122" id="Seg_1071" n="e" s="T121">qwandəku. </ts>
               <ts e="T123" id="Seg_1073" n="e" s="T122">täp </ts>
               <ts e="T124" id="Seg_1075" n="e" s="T123">qwandɨsɨt </ts>
               <ts e="T125" id="Seg_1077" n="e" s="T124">nändʼisä. </ts>
               <ts e="T126" id="Seg_1079" n="e" s="T125">qunpä </ts>
               <ts e="T127" id="Seg_1081" n="e" s="T126">täpqiːm </ts>
               <ts e="T128" id="Seg_1083" n="e" s="T127">qwatpat. </ts>
               <ts e="T129" id="Seg_1085" n="e" s="T128">nämdɨ </ts>
               <ts e="T130" id="Seg_1087" n="e" s="T129">assɨ </ts>
               <ts e="T131" id="Seg_1089" n="e" s="T130">qombatit. </ts>
               <ts e="T132" id="Seg_1091" n="e" s="T131">täpam </ts>
               <ts e="T133" id="Seg_1093" n="e" s="T132">oralpatat, </ts>
               <ts e="T134" id="Seg_1095" n="e" s="T133">omdalʼǯimbat. </ts>
               <ts e="T135" id="Seg_1097" n="e" s="T134">näkwaj </ts>
               <ts e="T136" id="Seg_1099" n="e" s="T135">üče </ts>
               <ts e="T137" id="Seg_1101" n="e" s="T136">soqandʼüklʼe </ts>
               <ts e="T138" id="Seg_1103" n="e" s="T137">kutʼä </ts>
               <ts e="T139" id="Seg_1105" n="e" s="T138">qatpumbalʼ </ts>
               <ts e="T140" id="Seg_1107" n="e" s="T139">täːpɨ. </ts>
               <ts e="T141" id="Seg_1109" n="e" s="T140">assɨ </ts>
               <ts e="T142" id="Seg_1111" n="e" s="T141">qässat. </ts>
               <ts e="T143" id="Seg_1113" n="e" s="T142">täp </ts>
               <ts e="T144" id="Seg_1115" n="e" s="T143">tʼäraŋ: </ts>
               <ts e="T145" id="Seg_1117" n="e" s="T144">tʼäŋoj </ts>
               <ts e="T146" id="Seg_1119" n="e" s="T145">wättoɨn </ts>
               <ts e="T147" id="Seg_1121" n="e" s="T146">qwänba </ts>
               <ts e="T148" id="Seg_1123" n="e" s="T147">maːttə. </ts>
               <ts e="T149" id="Seg_1125" n="e" s="T148">täp </ts>
               <ts e="T150" id="Seg_1127" n="e" s="T149">sitʼeptɨs. </ts>
               <ts e="T151" id="Seg_1129" n="e" s="T150">täp </ts>
               <ts e="T152" id="Seg_1131" n="e" s="T151">kuːda </ts>
               <ts e="T153" id="Seg_1133" n="e" s="T152">qatpɨmbat. </ts>
               <ts e="T154" id="Seg_1135" n="e" s="T153">nipa </ts>
               <ts e="T155" id="Seg_1137" n="e" s="T154">qalʼiŋ </ts>
               <ts e="T156" id="Seg_1139" n="e" s="T155">apʼätʼ </ts>
               <ts e="T157" id="Seg_1141" n="e" s="T156">irrakaːlʼäk. </ts>
               <ts e="T158" id="Seg_1143" n="e" s="T157">narɨmdʼelʼǯij </ts>
               <ts e="T159" id="Seg_1145" n="e" s="T158">iːrrat </ts>
               <ts e="T160" id="Seg_1147" n="e" s="T159">matʼtʼöɣɨn </ts>
               <ts e="T161" id="Seg_1149" n="e" s="T160">tüː </ts>
               <ts e="T162" id="Seg_1151" n="e" s="T161">ambat. </ts>
               <ts e="T163" id="Seg_1153" n="e" s="T162">täp </ts>
               <ts e="T164" id="Seg_1155" n="e" s="T163">illass </ts>
               <ts e="T165" id="Seg_1157" n="e" s="T164">iːndɨsä. </ts>
               <ts e="T166" id="Seg_1159" n="e" s="T165">nännɨ </ts>
               <ts e="T167" id="Seg_1161" n="e" s="T166">na </ts>
               <ts e="T168" id="Seg_1163" n="e" s="T167">iːt </ts>
               <ts e="T169" id="Seg_1165" n="e" s="T168">qoqsan. </ts>
               <ts e="T170" id="Seg_1167" n="e" s="T169">täp </ts>
               <ts e="T171" id="Seg_1169" n="e" s="T170">äːssan </ts>
               <ts e="T172" id="Seg_1171" n="e" s="T171">nar </ts>
               <ts e="T173" id="Seg_1173" n="e" s="T172">sarum </ts>
               <ts e="T174" id="Seg_1175" n="e" s="T173">(saraj) </ts>
               <ts e="T175" id="Seg_1177" n="e" s="T174">pəttə. </ts>
               <ts e="T176" id="Seg_1179" n="e" s="T175">niːpam </ts>
               <ts e="T177" id="Seg_1181" n="e" s="T176">illas </ts>
               <ts e="T178" id="Seg_1183" n="e" s="T177">matʼtʼöɣɨn </ts>
               <ts e="T179" id="Seg_1185" n="e" s="T178">üːčelandse </ts>
               <ts e="T180" id="Seg_1187" n="e" s="T179">nʼäjalʼe. </ts>
               <ts e="T181" id="Seg_1189" n="e" s="T180">i </ts>
               <ts e="T182" id="Seg_1191" n="e" s="T181">näti </ts>
               <ts e="T183" id="Seg_1193" n="e" s="T182">ont </ts>
               <ts e="T184" id="Seg_1195" n="e" s="T183">sijamt </ts>
               <ts e="T185" id="Seg_1197" n="e" s="T184">tʼäčambat, </ts>
               <ts e="T186" id="Seg_1199" n="e" s="T185">kənnamdi </ts>
               <ts e="T187" id="Seg_1201" n="e" s="T186">qätenǯilʼe. </ts>
               <ts e="T188" id="Seg_1203" n="e" s="T187">nätɨ </ts>
               <ts e="T189" id="Seg_1205" n="e" s="T188">ukon </ts>
               <ts e="T190" id="Seg_1207" n="e" s="T189">qwässɨ. </ts>
               <ts e="T191" id="Seg_1209" n="e" s="T190">nännɨ </ts>
               <ts e="T192" id="Seg_1211" n="e" s="T191">niːpam </ts>
               <ts e="T193" id="Seg_1213" n="e" s="T192">qwässɨ. </ts>
               <ts e="T194" id="Seg_1215" n="e" s="T193">niːpam </ts>
               <ts e="T195" id="Seg_1217" n="e" s="T194">tüssan, </ts>
               <ts e="T196" id="Seg_1219" n="e" s="T195">täp </ts>
               <ts e="T197" id="Seg_1221" n="e" s="T196">uš </ts>
               <ts e="T198" id="Seg_1223" n="e" s="T197">qumba. </ts>
               <ts e="T199" id="Seg_1225" n="e" s="T198">niːpam </ts>
               <ts e="T200" id="Seg_1227" n="e" s="T199">kɨsikus. </ts>
               <ts e="T201" id="Seg_1229" n="e" s="T200">qanǯisä </ts>
               <ts e="T202" id="Seg_1231" n="e" s="T201">maːttə </ts>
               <ts e="T203" id="Seg_1233" n="e" s="T202">taːssaːtit. </ts>
               <ts e="T204" id="Seg_1235" n="e" s="T203">kundɨ </ts>
               <ts e="T205" id="Seg_1237" n="e" s="T204">maːtqan </ts>
               <ts e="T206" id="Seg_1239" n="e" s="T205">iːppis. </ts>
               <ts e="T207" id="Seg_1241" n="e" s="T206">tüssan </ts>
               <ts e="T208" id="Seg_1243" n="e" s="T207">vrač, </ts>
               <ts e="T209" id="Seg_1245" n="e" s="T208">manǯesit </ts>
               <ts e="T210" id="Seg_1247" n="e" s="T209">i </ts>
               <ts e="T211" id="Seg_1249" n="e" s="T210">tʼäras </ts>
               <ts e="T212" id="Seg_1251" n="e" s="T211">oːtʼäŋu. </ts>
               <ts e="T213" id="Seg_1253" n="e" s="T212">oːtʼäksot. </ts>
               <ts e="T214" id="Seg_1255" n="e" s="T213">nʼärnä </ts>
               <ts e="T215" id="Seg_1257" n="e" s="T214">iːlilʼe </ts>
               <ts e="T216" id="Seg_1259" n="e" s="T215">oldʼisot. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_1260" s="T1">MKP_1964_MyGrandmother_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_1261" s="T5">MKP_1964_MyGrandmother_nar.002 (001.002)</ta>
            <ta e="T12" id="Seg_1262" s="T7">MKP_1964_MyGrandmother_nar.003 (001.003)</ta>
            <ta e="T17" id="Seg_1263" s="T12">MKP_1964_MyGrandmother_nar.004 (001.004)</ta>
            <ta e="T22" id="Seg_1264" s="T17">MKP_1964_MyGrandmother_nar.005 (001.005)</ta>
            <ta e="T25" id="Seg_1265" s="T22">MKP_1964_MyGrandmother_nar.006 (001.006)</ta>
            <ta e="T28" id="Seg_1266" s="T25">MKP_1964_MyGrandmother_nar.007 (001.007)</ta>
            <ta e="T30" id="Seg_1267" s="T28">MKP_1964_MyGrandmother_nar.008 (001.008)</ta>
            <ta e="T32" id="Seg_1268" s="T30">MKP_1964_MyGrandmother_nar.009 (001.009)</ta>
            <ta e="T36" id="Seg_1269" s="T32">MKP_1964_MyGrandmother_nar.010 (001.010)</ta>
            <ta e="T41" id="Seg_1270" s="T36">MKP_1964_MyGrandmother_nar.011 (001.011)</ta>
            <ta e="T43" id="Seg_1271" s="T41">MKP_1964_MyGrandmother_nar.012 (001.012)</ta>
            <ta e="T47" id="Seg_1272" s="T43">MKP_1964_MyGrandmother_nar.013 (001.013)</ta>
            <ta e="T51" id="Seg_1273" s="T47">MKP_1964_MyGrandmother_nar.014 (001.014)</ta>
            <ta e="T56" id="Seg_1274" s="T51">MKP_1964_MyGrandmother_nar.015 (001.015)</ta>
            <ta e="T60" id="Seg_1275" s="T56">MKP_1964_MyGrandmother_nar.016 (001.016)</ta>
            <ta e="T63" id="Seg_1276" s="T60">MKP_1964_MyGrandmother_nar.017 (001.017)</ta>
            <ta e="T69" id="Seg_1277" s="T63">MKP_1964_MyGrandmother_nar.018 (001.018)</ta>
            <ta e="T75" id="Seg_1278" s="T69">MKP_1964_MyGrandmother_nar.019 (001.019)</ta>
            <ta e="T80" id="Seg_1279" s="T75">MKP_1964_MyGrandmother_nar.020 (001.020)</ta>
            <ta e="T84" id="Seg_1280" s="T80">MKP_1964_MyGrandmother_nar.021 (001.021)</ta>
            <ta e="T89" id="Seg_1281" s="T84">MKP_1964_MyGrandmother_nar.022 (001.022)</ta>
            <ta e="T92" id="Seg_1282" s="T89">MKP_1964_MyGrandmother_nar.023 (001.023)</ta>
            <ta e="T97" id="Seg_1283" s="T92">MKP_1964_MyGrandmother_nar.024 (001.024)</ta>
            <ta e="T101" id="Seg_1284" s="T97">MKP_1964_MyGrandmother_nar.025 (001.025)</ta>
            <ta e="T106" id="Seg_1285" s="T101">MKP_1964_MyGrandmother_nar.026 (001.026)</ta>
            <ta e="T108" id="Seg_1286" s="T106">MKP_1964_MyGrandmother_nar.027 (001.027)</ta>
            <ta e="T113" id="Seg_1287" s="T108">MKP_1964_MyGrandmother_nar.028 (001.028)</ta>
            <ta e="T116" id="Seg_1288" s="T113">MKP_1964_MyGrandmother_nar.029 (001.029)</ta>
            <ta e="T122" id="Seg_1289" s="T116">MKP_1964_MyGrandmother_nar.030 (001.030)</ta>
            <ta e="T125" id="Seg_1290" s="T122">MKP_1964_MyGrandmother_nar.031 (001.031)</ta>
            <ta e="T128" id="Seg_1291" s="T125">MKP_1964_MyGrandmother_nar.032 (001.032)</ta>
            <ta e="T131" id="Seg_1292" s="T128">MKP_1964_MyGrandmother_nar.033 (001.033)</ta>
            <ta e="T134" id="Seg_1293" s="T131">MKP_1964_MyGrandmother_nar.034 (001.034)</ta>
            <ta e="T140" id="Seg_1294" s="T134">MKP_1964_MyGrandmother_nar.035 (001.035)</ta>
            <ta e="T142" id="Seg_1295" s="T140">MKP_1964_MyGrandmother_nar.036 (001.036)</ta>
            <ta e="T148" id="Seg_1296" s="T142">MKP_1964_MyGrandmother_nar.037 (001.037)</ta>
            <ta e="T150" id="Seg_1297" s="T148">MKP_1964_MyGrandmother_nar.038 (001.038)</ta>
            <ta e="T153" id="Seg_1298" s="T150">MKP_1964_MyGrandmother_nar.039 (001.039)</ta>
            <ta e="T157" id="Seg_1299" s="T153">MKP_1964_MyGrandmother_nar.040 (001.040)</ta>
            <ta e="T162" id="Seg_1300" s="T157">MKP_1964_MyGrandmother_nar.041 (001.041)</ta>
            <ta e="T165" id="Seg_1301" s="T162">MKP_1964_MyGrandmother_nar.042 (001.042)</ta>
            <ta e="T169" id="Seg_1302" s="T165">MKP_1964_MyGrandmother_nar.043 (001.043)</ta>
            <ta e="T175" id="Seg_1303" s="T169">MKP_1964_MyGrandmother_nar.044 (001.044)</ta>
            <ta e="T180" id="Seg_1304" s="T175">MKP_1964_MyGrandmother_nar.045 (001.045)</ta>
            <ta e="T187" id="Seg_1305" s="T180">MKP_1964_MyGrandmother_nar.046 (001.046)</ta>
            <ta e="T190" id="Seg_1306" s="T187">MKP_1964_MyGrandmother_nar.047 (001.047)</ta>
            <ta e="T193" id="Seg_1307" s="T190">MKP_1964_MyGrandmother_nar.048 (001.048)</ta>
            <ta e="T198" id="Seg_1308" s="T193">MKP_1964_MyGrandmother_nar.049 (001.049)</ta>
            <ta e="T200" id="Seg_1309" s="T198">MKP_1964_MyGrandmother_nar.050 (001.050)</ta>
            <ta e="T203" id="Seg_1310" s="T200">MKP_1964_MyGrandmother_nar.051 (001.051)</ta>
            <ta e="T206" id="Seg_1311" s="T203">MKP_1964_MyGrandmother_nar.052 (001.052)</ta>
            <ta e="T212" id="Seg_1312" s="T206">MKP_1964_MyGrandmother_nar.053 (001.053)</ta>
            <ta e="T213" id="Seg_1313" s="T212">MKP_1964_MyGrandmother_nar.054 (001.054)</ta>
            <ta e="T216" id="Seg_1314" s="T213">MKP_1964_MyGrandmother_nar.055 (001.055)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_1315" s="T1">ман ′нӣпамни тон ′поттъ.</ta>
            <ta e="T7" id="Seg_1316" s="T5">ӣск′lай ′ӣlапсан.</ta>
            <ta e="T12" id="Seg_1317" s="T7">ӱ′чең ′е̨llи (ӱ′чең ′еlа) ти′п(б̂)индамба.</ta>
            <ta e="T17" id="Seg_1318" s="T12">нар пон ӣlы′сати, ӱ′че ′тʼӓңус.</ta>
            <ta e="T22" id="Seg_1319" s="T17">′нӓнно ′оккар ӓ̄′сус, ′kӯсан е̄зуң.</ta>
            <ta e="T25" id="Seg_1320" s="T22">нагур пон ӣlы′сати.</ta>
            <ta e="T28" id="Seg_1321" s="T25">ӱ′чеlа kотʼ(и) е̄′зус.</ta>
            <ta e="T30" id="Seg_1322" s="T28">кундир о′рапсут?</ta>
            <ta e="T32" id="Seg_1323" s="T30">нʼӱнʼӱ′кʼелʼа kӯклу′саттъ.</ta>
            <ta e="T36" id="Seg_1324" s="T32">′нипам и ′ӣлʼдʼӓм lа′татук′сат.</ta>
            <ta e="T41" id="Seg_1325" s="T36">ӣ′ррат(м) ′ӱ̄там ′сӓрпылʼе ′тыттърукус, kwо̨тумби′кус.</ta>
            <ta e="T43" id="Seg_1326" s="T41">ӓ′сан ′тʼӱрумбит.</ta>
            <ta e="T47" id="Seg_1327" s="T43">тӓп ′типиндис ссъ′руй kу′ванни.</ta>
            <ta e="T51" id="Seg_1328" s="T47">тӓ̄па̄′н(н)ан нʼӱнʼӱ ӱ′чет ′ӓссан.</ta>
            <ta e="T56" id="Seg_1329" s="T51">′нӣпам тӓ̄′пам о′рамджисат и ′нӓтсит.</ta>
            <ta e="T60" id="Seg_1330" s="T56">тӓп′kиннап оккар ‵нӓɣу′вай ӱ̄′четӣ.</ta>
            <ta e="T63" id="Seg_1331" s="T60">′онди ′тʼӓрруң ′kӯсат(т)и.</ta>
            <ta e="T69" id="Seg_1332" s="T63">′нӣпаннан вес ӱ′чеlат ′а̄нукой и‵ррад′наннӓ kӯмбат.</ta>
            <ta e="T75" id="Seg_1333" s="T69">на̄кур ′нӓт ′пирреkынт мите′лʼевлʼе ′нӓнны ′kумбат.</ta>
            <ta e="T80" id="Seg_1334" s="T75">′а̄нукой и′ррат ′ма̄тkан ′кӱ̄тлʼе kӯ′ссан.</ta>
            <ta e="T84" id="Seg_1335" s="T80">нӓнны ′ондъ(ы) и′lакус ′кундъ(ы).</ta>
            <ta e="T89" id="Seg_1336" s="T84">′нӓнны ти′пиндисан а′раң и′рранни (kу′ванни).</ta>
            <ta e="T92" id="Seg_1337" s="T89">тӓп ′ӓсан ссъ′румби′тай.</ta>
            <ta e="T97" id="Seg_1338" s="T92">тӓпан′нан а̄нукой нӓɣумнӓ′нан ′шытты ӱ̄′чет.</ta>
            <ta e="T101" id="Seg_1339" s="T97">нӓн ′онди ӱ′чеlати е′зус.</ta>
            <ta e="T106" id="Seg_1340" s="T101">′кун ‵kӧ′ниң ′е̨̄сан, тӓп ӓ′валʼджимбат.</ta>
            <ta e="T108" id="Seg_1341" s="T106">шы′ттӓɣ о̄ра̄′псати.</ta>
            <ta e="T113" id="Seg_1342" s="T108">оккар ′орапсан и ‵ӱ̄ткоң′нан.</ta>
            <ta e="T116" id="Seg_1343" s="T113">оккъ ′jат ӣ′lаң.</ta>
            <ta e="T122" id="Seg_1344" s="T116">тиб̂еɣу′ванни ‵тʼӓръ′саттъ кӯ′намбитай (kунпӓ), kун′пӓм ′kwандъку.</ta>
            <ta e="T125" id="Seg_1345" s="T122">тӓп ′kwа̊ндысыт ′нӓндʼисӓ.</ta>
            <ta e="T128" id="Seg_1346" s="T125">′kунпӓ тӓп′kӣм kwат′пат.</ta>
            <ta e="T131" id="Seg_1347" s="T128">′нӓмды ассы kом′батит.</ta>
            <ta e="T134" id="Seg_1348" s="T131">′тӓпам о′раlпатат, ом′далʼджимбат.</ta>
            <ta e="T140" id="Seg_1349" s="T134">′нӓквай ӱ′че со′kандʼӱклʼе ′кутʼӓ ′kатпумбалʼ тӓ̄′пы.</ta>
            <ta e="T142" id="Seg_1350" s="T140">′ассы kӓ′ссат.</ta>
            <ta e="T148" id="Seg_1351" s="T142">тӓп тʼӓраң: тʼӓ′ңой ′вӓттоын kwӓн′ба ма̄ттъ.</ta>
            <ta e="T150" id="Seg_1352" s="T148">тӓп си′тʼептыс.</ta>
            <ta e="T153" id="Seg_1353" s="T150">тӓп кӯда ′kатпымбат.</ta>
            <ta e="T157" id="Seg_1354" s="T153">′нипа kалʼиң апʼӓтʼ и′рра′ка̄лʼӓк.</ta>
            <ta e="T162" id="Seg_1355" s="T157">′нарым′дʼелʼджий ӣррат ма′тʼтʼӧɣын тӱ̄ ′амбат.</ta>
            <ta e="T165" id="Seg_1356" s="T162">тӓп и′llасс ӣндысӓ.</ta>
            <ta e="T169" id="Seg_1357" s="T165">′нӓнны на ӣт kоk′сан.</ta>
            <ta e="T175" id="Seg_1358" s="T169">тӓп ′ӓ̄с(с)ан нар ′сарум (сарай) ′пъ̊ттъ.</ta>
            <ta e="T180" id="Seg_1359" s="T175">′нӣпам и′llас ма′тʼтʼӧɣын ӱ̄′че′lанд̂се нʼӓjалʼе.</ta>
            <ta e="T187" id="Seg_1360" s="T180">и ′нӓти ′онт си′jамт тʼӓ′тшамбат, къ̊′ннамди kӓ′тенджилʼе.</ta>
            <ta e="T190" id="Seg_1361" s="T187">′нӓты у′кон kwӓ′ссы.</ta>
            <ta e="T193" id="Seg_1362" s="T190">′нӓнны ′нӣпам kwӓссы.</ta>
            <ta e="T198" id="Seg_1363" s="T193">′нӣпам тӱ′ссан, тӓп уш ′kумба.</ta>
            <ta e="T200" id="Seg_1364" s="T198">нӣпам кыси′кус.</ta>
            <ta e="T203" id="Seg_1365" s="T200">kанджи′сӓ ма̄ттъ та̄сса̄′тит.</ta>
            <ta e="T206" id="Seg_1366" s="T203">′кунды ма̄тkан ӣ′ппис.</ta>
            <ta e="T212" id="Seg_1367" s="T206">′тӱссан врач, ман′джесит и тʼӓ′рас о̄′тʼӓңу.</ta>
            <ta e="T213" id="Seg_1368" s="T212">о̄′тʼӓксот.</ta>
            <ta e="T216" id="Seg_1369" s="T213">нʼӓр′нӓ ӣlи′лʼе ′оlдʼисот.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_1370" s="T1">man niːpamni ton pottə.</ta>
            <ta e="T7" id="Seg_1371" s="T5">iːsklaj iːlapsan.</ta>
            <ta e="T12" id="Seg_1372" s="T7">üčeŋ elli (üčeŋ ela) tip(b̂)indamba.</ta>
            <ta e="T17" id="Seg_1373" s="T12">nar pon iːlɨsati, üče tʼäŋus.</ta>
            <ta e="T22" id="Seg_1374" s="T17">nänno okkar äːsus, quːsan eːzuŋ.</ta>
            <ta e="T25" id="Seg_1375" s="T22">nagur pon iːlɨsati.</ta>
            <ta e="T28" id="Seg_1376" s="T25">üčela qotʼ(i) eːzus.</ta>
            <ta e="T30" id="Seg_1377" s="T28">kundir orapsut?</ta>
            <ta e="T32" id="Seg_1378" s="T30">nʼünʼükʼelʼa quːklusattə.</ta>
            <ta e="T36" id="Seg_1379" s="T32">nipam i iːlʼdʼäm latatuksat.</ta>
            <ta e="T41" id="Seg_1380" s="T36">iːrrat(m) üːtam särpɨlʼe tɨttərukus, qwotumbikus.</ta>
            <ta e="T43" id="Seg_1381" s="T41">äsan tʼürumbit.</ta>
            <ta e="T47" id="Seg_1382" s="T43">täp tipindis ssəruj quvanni.</ta>
            <ta e="T51" id="Seg_1383" s="T47">täːpaːn(n)an nʼünʼü üčet ässan.</ta>
            <ta e="T56" id="Seg_1384" s="T51">niːpam täːpam oramǯisat i nätsit.</ta>
            <ta e="T60" id="Seg_1385" s="T56">täpqinnap okkar näɣuvaj üːčetiː.</ta>
            <ta e="T63" id="Seg_1386" s="T60">ondi tʼärruŋ quːsat(t)i.</ta>
            <ta e="T69" id="Seg_1387" s="T63">niːpannan ves üčelat aːnukoj irradnannä quːmbat.</ta>
            <ta e="T75" id="Seg_1388" s="T69">naːkur nät pirreqɨnt mitelʼevlʼe nännɨ qumbat.</ta>
            <ta e="T80" id="Seg_1389" s="T75">aːnukoj irrat maːtqan küːtlʼe quːssan.</ta>
            <ta e="T84" id="Seg_1390" s="T80">nännɨ ondə(ɨ) ilakus kundə(ɨ).</ta>
            <ta e="T89" id="Seg_1391" s="T84">nännɨ tipindisan araŋ irranni (quvanni).</ta>
            <ta e="T92" id="Seg_1392" s="T89">täp äsan ssərumbitaj.</ta>
            <ta e="T97" id="Seg_1393" s="T92">täpannan aːnukoj näɣumnänan šɨttɨ üːčet.</ta>
            <ta e="T101" id="Seg_1394" s="T97">nän ondi üčelati ezus.</ta>
            <ta e="T106" id="Seg_1395" s="T101">kun qöniŋ eːsan, täp ävalʼǯimbat.</ta>
            <ta e="T108" id="Seg_1396" s="T106">šɨttäɣ oːraːpsati.</ta>
            <ta e="T113" id="Seg_1397" s="T108">okkar orapsan i üːtkoŋnan.</ta>
            <ta e="T116" id="Seg_1398" s="T113">okkə jat iːlaŋ.</ta>
            <ta e="T122" id="Seg_1399" s="T116">tib̂eɣuvanni tʼärəsattə kuːnambitaj (qunpä), qunpäm qwandəku.</ta>
            <ta e="T125" id="Seg_1400" s="T122">täp qwandɨsɨt nändʼisä.</ta>
            <ta e="T128" id="Seg_1401" s="T125">qunpä täpqiːm qwatpat.</ta>
            <ta e="T131" id="Seg_1402" s="T128">nämdɨ assɨ qombatit.</ta>
            <ta e="T134" id="Seg_1403" s="T131">täpam oralpatat, omdalʼǯimbat.</ta>
            <ta e="T140" id="Seg_1404" s="T134">näkvaj üče soqandʼüklʼe kutʼä qatpumbalʼ täːpɨ.</ta>
            <ta e="T142" id="Seg_1405" s="T140">assɨ qässat.</ta>
            <ta e="T148" id="Seg_1406" s="T142">täp tʼäraŋ: tʼäŋoj vättoɨn qwänba maːttə.</ta>
            <ta e="T150" id="Seg_1407" s="T148">täp sitʼeptɨs.</ta>
            <ta e="T153" id="Seg_1408" s="T150">täp kuːda qatpɨmbat.</ta>
            <ta e="T157" id="Seg_1409" s="T153">nipa qalʼiŋ apʼätʼ irrakaːlʼäk.</ta>
            <ta e="T162" id="Seg_1410" s="T157">narɨmdʼelʼǯij iːrrat matʼtʼöɣɨn tüː ambat.</ta>
            <ta e="T165" id="Seg_1411" s="T162">täp illass iːndɨsä.</ta>
            <ta e="T169" id="Seg_1412" s="T165">nännɨ na iːt qoqsan.</ta>
            <ta e="T175" id="Seg_1413" s="T169">täp äːs(s)an nar sarum (saraj) pəttə.</ta>
            <ta e="T180" id="Seg_1414" s="T175">niːpam illas matʼtʼöɣɨn üːčeland̂se nʼäjalʼe.</ta>
            <ta e="T187" id="Seg_1415" s="T180">i näti ont sijamt tʼätšambat, kənnamdi qätenǯilʼe.</ta>
            <ta e="T190" id="Seg_1416" s="T187">nätɨ ukon qwässɨ.</ta>
            <ta e="T193" id="Seg_1417" s="T190">nännɨ niːpam qwässɨ.</ta>
            <ta e="T198" id="Seg_1418" s="T193">niːpam tüssan, täp uš qumba.</ta>
            <ta e="T200" id="Seg_1419" s="T198">niːpam kɨsikus.</ta>
            <ta e="T203" id="Seg_1420" s="T200">qanǯisä maːttə taːssaːtit.</ta>
            <ta e="T206" id="Seg_1421" s="T203">kundɨ maːtqan iːppis.</ta>
            <ta e="T212" id="Seg_1422" s="T206">tüssan vrač, manǯesit i tʼäras oːtʼäŋu.</ta>
            <ta e="T213" id="Seg_1423" s="T212">oːtʼäksot.</ta>
            <ta e="T216" id="Seg_1424" s="T213">nʼärnä iːlilʼe oldʼisot.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_1425" s="T1">man niːpamni ton pottə. </ta>
            <ta e="T7" id="Seg_1426" s="T5">iːsklaj iːlapsan. </ta>
            <ta e="T12" id="Seg_1427" s="T7">üčeŋ elli (üčeŋ ela) tipindamba. </ta>
            <ta e="T17" id="Seg_1428" s="T12">nar pon iːlɨsati, üče tʼäŋus. </ta>
            <ta e="T22" id="Seg_1429" s="T17">nänno okkar äːsus, quːsan eːzuŋ. </ta>
            <ta e="T25" id="Seg_1430" s="T22">nagur pon iːlɨsati. </ta>
            <ta e="T28" id="Seg_1431" s="T25">üčela qotʼi eːzus. </ta>
            <ta e="T30" id="Seg_1432" s="T28">kundir orapsut? </ta>
            <ta e="T32" id="Seg_1433" s="T30">nʼünʼükʼelʼa quːklusattə. </ta>
            <ta e="T36" id="Seg_1434" s="T32">nipam i iːlʼdʼäm latatuksat. </ta>
            <ta e="T41" id="Seg_1435" s="T36">iːrrat üːtam särpɨlʼe tɨttərukus, qwotumbikus. </ta>
            <ta e="T43" id="Seg_1436" s="T41">äsan tʼürumbit. </ta>
            <ta e="T47" id="Seg_1437" s="T43">täp tipindis ssəruj quwanni. </ta>
            <ta e="T51" id="Seg_1438" s="T47">täːpaːnnan nʼünʼü üčet ässan. </ta>
            <ta e="T56" id="Seg_1439" s="T51">niːpam täːpam oramǯisat i nätsit. </ta>
            <ta e="T60" id="Seg_1440" s="T56">täpqinnap okkar näɣuwaj üːčetiː. </ta>
            <ta e="T63" id="Seg_1441" s="T60">ondi tʼärruŋ quːsati. </ta>
            <ta e="T69" id="Seg_1442" s="T63">niːpannan wes üčelat aːnukoj irradnannä quːmbat. </ta>
            <ta e="T75" id="Seg_1443" s="T69">naːkur nät pirreqɨnt mitelʼewlʼe nännɨ qumbat. </ta>
            <ta e="T80" id="Seg_1444" s="T75">aːnukoj irrat maːtqan küːtlʼe quːssan. </ta>
            <ta e="T84" id="Seg_1445" s="T80">nännɨ ondə ilakus kundə. </ta>
            <ta e="T89" id="Seg_1446" s="T84">nännɨ tipindisan araŋ irranni (quwanni). </ta>
            <ta e="T92" id="Seg_1447" s="T89">täp äsan ssərumbitaj. </ta>
            <ta e="T97" id="Seg_1448" s="T92">täpannan aːnukoj näɣumnänan šɨttɨ üːčet. </ta>
            <ta e="T101" id="Seg_1449" s="T97">nän ondi üčelati ezus. </ta>
            <ta e="T106" id="Seg_1450" s="T101">kun qöniŋ eːsan, täp äwalʼǯimbat. </ta>
            <ta e="T108" id="Seg_1451" s="T106">šɨttäɣ oːraːpsati. </ta>
            <ta e="T113" id="Seg_1452" s="T108">okkar orapsan i üːt koŋnan. </ta>
            <ta e="T116" id="Seg_1453" s="T113">okkə jat iːlaŋ. </ta>
            <ta e="T122" id="Seg_1454" s="T116">tibeɣuwanni tʼärəsattə kuːnambitaj (qunpä), qunpäm qwandəku. </ta>
            <ta e="T125" id="Seg_1455" s="T122">täp qwandɨsɨt nändʼisä. </ta>
            <ta e="T128" id="Seg_1456" s="T125">qunpä täpqiːm qwatpat. </ta>
            <ta e="T131" id="Seg_1457" s="T128">nämdɨ assɨ qombatit. </ta>
            <ta e="T134" id="Seg_1458" s="T131">täpam oralpatat, omdalʼǯimbat. </ta>
            <ta e="T140" id="Seg_1459" s="T134">näkwaj üče soqandʼüklʼe kutʼä qatpumbalʼ täːpɨ. </ta>
            <ta e="T142" id="Seg_1460" s="T140">assɨ qässat. </ta>
            <ta e="T148" id="Seg_1461" s="T142">täp tʼäraŋ: tʼäŋoj wättoɨn qwänba maːttə. </ta>
            <ta e="T150" id="Seg_1462" s="T148">täp sitʼeptɨs. </ta>
            <ta e="T153" id="Seg_1463" s="T150">täp kuːda qatpɨmbat. </ta>
            <ta e="T157" id="Seg_1464" s="T153">nipa qalʼiŋ apʼätʼ irrakaːlʼäk. </ta>
            <ta e="T162" id="Seg_1465" s="T157">narɨmdʼelʼǯij iːrrat matʼtʼöɣɨn tüː ambat. </ta>
            <ta e="T165" id="Seg_1466" s="T162">täp illass iːndɨsä. </ta>
            <ta e="T169" id="Seg_1467" s="T165">nännɨ na iːt qoqsan. </ta>
            <ta e="T175" id="Seg_1468" s="T169">täp äːssan nar sarum (saraj) pəttə. </ta>
            <ta e="T180" id="Seg_1469" s="T175">niːpam illas matʼtʼöɣɨn üːčelandse nʼäjalʼe. </ta>
            <ta e="T187" id="Seg_1470" s="T180">i näti ont sijamt tʼäčambat, kənnamdi qätenǯilʼe. </ta>
            <ta e="T190" id="Seg_1471" s="T187">nätɨ ukon qwässɨ. </ta>
            <ta e="T193" id="Seg_1472" s="T190">nännɨ niːpam qwässɨ. </ta>
            <ta e="T198" id="Seg_1473" s="T193">niːpam tüssan, täp uš qumba. </ta>
            <ta e="T200" id="Seg_1474" s="T198">niːpam kɨsikus. </ta>
            <ta e="T203" id="Seg_1475" s="T200">qanǯisä maːttə taːssaːtit. </ta>
            <ta e="T206" id="Seg_1476" s="T203">kundɨ maːtqan iːppis. </ta>
            <ta e="T212" id="Seg_1477" s="T206">tüssan vrač, manǯesit i tʼäras oːtʼäŋu. </ta>
            <ta e="T213" id="Seg_1478" s="T212">oːtʼäksot. </ta>
            <ta e="T216" id="Seg_1479" s="T213">nʼärnä iːlilʼe oldʼisot. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_1480" s="T1">man</ta>
            <ta e="T3" id="Seg_1481" s="T2">niːpa-m-ni</ta>
            <ta e="T4" id="Seg_1482" s="T3">ton</ta>
            <ta e="T5" id="Seg_1483" s="T4">po-ttə</ta>
            <ta e="T6" id="Seg_1484" s="T5">iːsklaj</ta>
            <ta e="T7" id="Seg_1485" s="T6">iːla-psan</ta>
            <ta e="T8" id="Seg_1486" s="T7">üče-ŋ</ta>
            <ta e="T9" id="Seg_1487" s="T8">elli</ta>
            <ta e="T10" id="Seg_1488" s="T9">üče-ŋ</ta>
            <ta e="T11" id="Seg_1489" s="T10">ela</ta>
            <ta e="T12" id="Seg_1490" s="T11">tipinda-mba</ta>
            <ta e="T13" id="Seg_1491" s="T12">nar</ta>
            <ta e="T14" id="Seg_1492" s="T13">po-n</ta>
            <ta e="T15" id="Seg_1493" s="T14">iːlɨ-sa-ti</ta>
            <ta e="T16" id="Seg_1494" s="T15">üče</ta>
            <ta e="T17" id="Seg_1495" s="T16">tʼäŋu-s</ta>
            <ta e="T18" id="Seg_1496" s="T17">nänno</ta>
            <ta e="T19" id="Seg_1497" s="T18">okkar</ta>
            <ta e="T20" id="Seg_1498" s="T19">äːsu-s</ta>
            <ta e="T21" id="Seg_1499" s="T20">quː-san</ta>
            <ta e="T22" id="Seg_1500" s="T21">eːzu-n</ta>
            <ta e="T23" id="Seg_1501" s="T22">nakkɨr</ta>
            <ta e="T24" id="Seg_1502" s="T23">po-n</ta>
            <ta e="T25" id="Seg_1503" s="T24">iːlɨ-sa-ti</ta>
            <ta e="T26" id="Seg_1504" s="T25">üče-la</ta>
            <ta e="T27" id="Seg_1505" s="T26">qotʼi</ta>
            <ta e="T28" id="Seg_1506" s="T27">eːzu-s</ta>
            <ta e="T29" id="Seg_1507" s="T28">kundir</ta>
            <ta e="T30" id="Seg_1508" s="T29">orap-su-t</ta>
            <ta e="T31" id="Seg_1509" s="T30">nʼünʼü-kʼe-lʼa</ta>
            <ta e="T32" id="Seg_1510" s="T31">quː-k-lu-sa-ttə</ta>
            <ta e="T33" id="Seg_1511" s="T32">nipa-m</ta>
            <ta e="T34" id="Seg_1512" s="T33">i</ta>
            <ta e="T35" id="Seg_1513" s="T34">iːlʼdʼä-m</ta>
            <ta e="T36" id="Seg_1514" s="T35">lata-tu-k-sa-t</ta>
            <ta e="T37" id="Seg_1515" s="T36">iːrra-t</ta>
            <ta e="T38" id="Seg_1516" s="T37">üːt-a-m</ta>
            <ta e="T39" id="Seg_1517" s="T38">särpɨ-lʼe</ta>
            <ta e="T40" id="Seg_1518" s="T39">tɨttə-r-u-ku-s</ta>
            <ta e="T41" id="Seg_1519" s="T40">qwotu-mbi-ku-s</ta>
            <ta e="T42" id="Seg_1520" s="T41">ä-sa-ŋ</ta>
            <ta e="T43" id="Seg_1521" s="T42">tʼürumbi-t</ta>
            <ta e="T44" id="Seg_1522" s="T43">täp</ta>
            <ta e="T45" id="Seg_1523" s="T44">tipindi-s</ta>
            <ta e="T46" id="Seg_1524" s="T45">ssəru-j</ta>
            <ta e="T47" id="Seg_1525" s="T46">quw-a-n-ni</ta>
            <ta e="T48" id="Seg_1526" s="T47">täːp-aː-n-nan</ta>
            <ta e="T49" id="Seg_1527" s="T48">nʼünʼü</ta>
            <ta e="T50" id="Seg_1528" s="T49">üče-t</ta>
            <ta e="T51" id="Seg_1529" s="T50">ä-ssa-n</ta>
            <ta e="T52" id="Seg_1530" s="T51">niːpa-m</ta>
            <ta e="T53" id="Seg_1531" s="T52">täːp-a-m</ta>
            <ta e="T54" id="Seg_1532" s="T53">oram-ǯi-sa-t</ta>
            <ta e="T55" id="Seg_1533" s="T54">i</ta>
            <ta e="T56" id="Seg_1534" s="T55">nät-si-t</ta>
            <ta e="T57" id="Seg_1535" s="T56">täp-qi-n-nap</ta>
            <ta e="T58" id="Seg_1536" s="T57">okkar</ta>
            <ta e="T59" id="Seg_1537" s="T58">nä-ɣuw-a-j</ta>
            <ta e="T60" id="Seg_1538" s="T59">üːče-tiː</ta>
            <ta e="T61" id="Seg_1539" s="T60">ondi</ta>
            <ta e="T62" id="Seg_1540" s="T61">tʼärruŋ</ta>
            <ta e="T63" id="Seg_1541" s="T62">quː-sa-ti</ta>
            <ta e="T64" id="Seg_1542" s="T63">niːpa-n-nan</ta>
            <ta e="T65" id="Seg_1543" s="T64">wes</ta>
            <ta e="T66" id="Seg_1544" s="T65">üče-la-t</ta>
            <ta e="T67" id="Seg_1545" s="T66">aːnukoj</ta>
            <ta e="T68" id="Seg_1546" s="T67">irra-d-nannä</ta>
            <ta e="T69" id="Seg_1547" s="T68">quː-mba-t</ta>
            <ta e="T70" id="Seg_1548" s="T69">naːkur</ta>
            <ta e="T71" id="Seg_1549" s="T70">nä-t</ta>
            <ta e="T72" id="Seg_1550" s="T71">pirre-qɨnt</ta>
            <ta e="T73" id="Seg_1551" s="T72">mite-lʼewlʼe</ta>
            <ta e="T74" id="Seg_1552" s="T73">nännɨ</ta>
            <ta e="T75" id="Seg_1553" s="T74">qu-mba-t</ta>
            <ta e="T76" id="Seg_1554" s="T75">aːnukoj</ta>
            <ta e="T77" id="Seg_1555" s="T76">irra-t</ta>
            <ta e="T78" id="Seg_1556" s="T77">maːt-qan</ta>
            <ta e="T79" id="Seg_1557" s="T78">küːt-lʼe</ta>
            <ta e="T80" id="Seg_1558" s="T79">quː-ssa-n</ta>
            <ta e="T81" id="Seg_1559" s="T80">nännɨ</ta>
            <ta e="T82" id="Seg_1560" s="T81">ondə</ta>
            <ta e="T83" id="Seg_1561" s="T82">ila-ku-s</ta>
            <ta e="T84" id="Seg_1562" s="T83">kundə</ta>
            <ta e="T85" id="Seg_1563" s="T84">nännɨ</ta>
            <ta e="T86" id="Seg_1564" s="T85">tipindi-sa-n</ta>
            <ta e="T87" id="Seg_1565" s="T86">araŋ</ta>
            <ta e="T88" id="Seg_1566" s="T87">irra-n-ni</ta>
            <ta e="T89" id="Seg_1567" s="T88">quw-a-n-ni</ta>
            <ta e="T90" id="Seg_1568" s="T89">täp</ta>
            <ta e="T91" id="Seg_1569" s="T90">ä-sa-ŋ</ta>
            <ta e="T92" id="Seg_1570" s="T91">ssəru-m-bitaj</ta>
            <ta e="T93" id="Seg_1571" s="T92">täp-a-n-nan</ta>
            <ta e="T94" id="Seg_1572" s="T93">aːnukoj</ta>
            <ta e="T95" id="Seg_1573" s="T94">nä-ɣum-nänan</ta>
            <ta e="T96" id="Seg_1574" s="T95">šɨttɨ</ta>
            <ta e="T97" id="Seg_1575" s="T96">üːče-t</ta>
            <ta e="T98" id="Seg_1576" s="T97">nän</ta>
            <ta e="T99" id="Seg_1577" s="T98">ondi</ta>
            <ta e="T100" id="Seg_1578" s="T99">üče-la-ti</ta>
            <ta e="T101" id="Seg_1579" s="T100">ezu-s</ta>
            <ta e="T102" id="Seg_1580" s="T101">kun</ta>
            <ta e="T103" id="Seg_1581" s="T102">qöniŋ</ta>
            <ta e="T104" id="Seg_1582" s="T103">eː-sa-ŋ</ta>
            <ta e="T105" id="Seg_1583" s="T104">täp</ta>
            <ta e="T106" id="Seg_1584" s="T105">äwalʼǯi-mba-t</ta>
            <ta e="T107" id="Seg_1585" s="T106">šɨttä-ɣ</ta>
            <ta e="T108" id="Seg_1586" s="T107">oːraːp-sa-ti</ta>
            <ta e="T109" id="Seg_1587" s="T108">okkar</ta>
            <ta e="T110" id="Seg_1588" s="T109">orap-sa-n</ta>
            <ta e="T111" id="Seg_1589" s="T110">i</ta>
            <ta e="T112" id="Seg_1590" s="T111">üːt</ta>
            <ta e="T113" id="Seg_1591" s="T112">koŋ-na-n</ta>
            <ta e="T114" id="Seg_1592" s="T113">okkə</ta>
            <ta e="T115" id="Seg_1593" s="T114">jat</ta>
            <ta e="T116" id="Seg_1594" s="T115">iːla-ŋ</ta>
            <ta e="T117" id="Seg_1595" s="T116">tibe-ɣuw-a-n-ni</ta>
            <ta e="T118" id="Seg_1596" s="T117">tʼärə-sa-ttə</ta>
            <ta e="T119" id="Seg_1597" s="T118">kuːna-mbitaj</ta>
            <ta e="T121" id="Seg_1598" s="T120">qunpä-m</ta>
            <ta e="T122" id="Seg_1599" s="T121">qwandə-ku</ta>
            <ta e="T123" id="Seg_1600" s="T122">täp</ta>
            <ta e="T124" id="Seg_1601" s="T123">qwandɨ-sɨ-t</ta>
            <ta e="T125" id="Seg_1602" s="T124">nändʼi-sä</ta>
            <ta e="T126" id="Seg_1603" s="T125">%%</ta>
            <ta e="T127" id="Seg_1604" s="T126">täp-qiː-m</ta>
            <ta e="T128" id="Seg_1605" s="T127">qwat-pa-t</ta>
            <ta e="T129" id="Seg_1606" s="T128">nä-m-dɨ</ta>
            <ta e="T130" id="Seg_1607" s="T129">assɨ</ta>
            <ta e="T131" id="Seg_1608" s="T130">qo-mba-tit</ta>
            <ta e="T132" id="Seg_1609" s="T131">täp-a-m</ta>
            <ta e="T133" id="Seg_1610" s="T132">oral-pa-tat</ta>
            <ta e="T134" id="Seg_1611" s="T133">omda-lʼǯi-mba-t</ta>
            <ta e="T135" id="Seg_1612" s="T134">nä-kwaj</ta>
            <ta e="T136" id="Seg_1613" s="T135">üče</ta>
            <ta e="T137" id="Seg_1614" s="T136">soqa-ndʼü-k-lʼe</ta>
            <ta e="T138" id="Seg_1615" s="T137">kutʼä</ta>
            <ta e="T139" id="Seg_1616" s="T138">qat-pu-mbalʼ</ta>
            <ta e="T140" id="Seg_1617" s="T139">täːp-ɨ</ta>
            <ta e="T141" id="Seg_1618" s="T140">assɨ</ta>
            <ta e="T142" id="Seg_1619" s="T141">qäs-sa-t</ta>
            <ta e="T143" id="Seg_1620" s="T142">täp</ta>
            <ta e="T144" id="Seg_1621" s="T143">tʼära-ŋ</ta>
            <ta e="T145" id="Seg_1622" s="T144">tʼäŋo-j</ta>
            <ta e="T146" id="Seg_1623" s="T145">wätto-ɨn</ta>
            <ta e="T147" id="Seg_1624" s="T146">qwän-ba</ta>
            <ta e="T148" id="Seg_1625" s="T147">maːt-tə</ta>
            <ta e="T149" id="Seg_1626" s="T148">täp</ta>
            <ta e="T150" id="Seg_1627" s="T149">sitʼeptɨ-s</ta>
            <ta e="T151" id="Seg_1628" s="T150">täp</ta>
            <ta e="T152" id="Seg_1629" s="T151">kuːda</ta>
            <ta e="T153" id="Seg_1630" s="T152">qat-pɨ-mba-t</ta>
            <ta e="T154" id="Seg_1631" s="T153">nipa</ta>
            <ta e="T155" id="Seg_1632" s="T154">qalʼi-n</ta>
            <ta e="T156" id="Seg_1633" s="T155">apʼätʼ</ta>
            <ta e="T157" id="Seg_1634" s="T156">irra-kaːlʼä-k</ta>
            <ta e="T158" id="Seg_1635" s="T157">narɨ-mdʼelʼ-ǯij</ta>
            <ta e="T159" id="Seg_1636" s="T158">iːrra-t</ta>
            <ta e="T160" id="Seg_1637" s="T159">matʼtʼö-ɣɨn</ta>
            <ta e="T161" id="Seg_1638" s="T160">tüː</ta>
            <ta e="T162" id="Seg_1639" s="T161">am-ɨ-ba-t</ta>
            <ta e="T163" id="Seg_1640" s="T162">täp</ta>
            <ta e="T164" id="Seg_1641" s="T163">illa-ss</ta>
            <ta e="T165" id="Seg_1642" s="T164">iː-n-dɨ-sä</ta>
            <ta e="T166" id="Seg_1643" s="T165">nännɨ</ta>
            <ta e="T167" id="Seg_1644" s="T166">na</ta>
            <ta e="T168" id="Seg_1645" s="T167">iː-t</ta>
            <ta e="T169" id="Seg_1646" s="T168">qoq-sa-n</ta>
            <ta e="T170" id="Seg_1647" s="T169">täp</ta>
            <ta e="T171" id="Seg_1648" s="T170">äː-ssa-n</ta>
            <ta e="T172" id="Seg_1649" s="T171">nar</ta>
            <ta e="T173" id="Seg_1650" s="T172">sarum</ta>
            <ta e="T174" id="Seg_1651" s="T173">*sar-a-j</ta>
            <ta e="T175" id="Seg_1652" s="T174">pə-ttə</ta>
            <ta e="T176" id="Seg_1653" s="T175">niːpa-m</ta>
            <ta e="T177" id="Seg_1654" s="T176">illa-s</ta>
            <ta e="T178" id="Seg_1655" s="T177">matʼtʼö-ɣɨn</ta>
            <ta e="T179" id="Seg_1656" s="T178">üːče-la-n-d-se</ta>
            <ta e="T180" id="Seg_1657" s="T179">nʼäja-lʼe</ta>
            <ta e="T181" id="Seg_1658" s="T180">i</ta>
            <ta e="T182" id="Seg_1659" s="T181">nä-ti</ta>
            <ta e="T183" id="Seg_1660" s="T182">ont</ta>
            <ta e="T184" id="Seg_1661" s="T183">sija-m-t</ta>
            <ta e="T185" id="Seg_1662" s="T184">tʼäča-mba-t</ta>
            <ta e="T186" id="Seg_1663" s="T185">kənna-m-di</ta>
            <ta e="T187" id="Seg_1664" s="T186">qäte-nǯi-lʼe</ta>
            <ta e="T188" id="Seg_1665" s="T187">nä-tɨ</ta>
            <ta e="T189" id="Seg_1666" s="T188">ukon</ta>
            <ta e="T190" id="Seg_1667" s="T189">qwäs-sɨ</ta>
            <ta e="T191" id="Seg_1668" s="T190">nännɨ</ta>
            <ta e="T192" id="Seg_1669" s="T191">niːpa-m</ta>
            <ta e="T193" id="Seg_1670" s="T192">qwäs-sɨ</ta>
            <ta e="T194" id="Seg_1671" s="T193">niːpa-m</ta>
            <ta e="T195" id="Seg_1672" s="T194">tü-ssa-n</ta>
            <ta e="T196" id="Seg_1673" s="T195">täp</ta>
            <ta e="T197" id="Seg_1674" s="T196">uš</ta>
            <ta e="T198" id="Seg_1675" s="T197">qu-mba</ta>
            <ta e="T199" id="Seg_1676" s="T198">niːpa-m</ta>
            <ta e="T200" id="Seg_1677" s="T199">kɨsi-ku-s</ta>
            <ta e="T201" id="Seg_1678" s="T200">qanǯi-sä</ta>
            <ta e="T202" id="Seg_1679" s="T201">maːt-tə</ta>
            <ta e="T203" id="Seg_1680" s="T202">taːs-saː-tit</ta>
            <ta e="T204" id="Seg_1681" s="T203">kundɨ</ta>
            <ta e="T205" id="Seg_1682" s="T204">maːt-qan</ta>
            <ta e="T206" id="Seg_1683" s="T205">iːppi-s</ta>
            <ta e="T207" id="Seg_1684" s="T206">tü-ssa-ŋ</ta>
            <ta e="T208" id="Seg_1685" s="T207">vrač</ta>
            <ta e="T209" id="Seg_1686" s="T208">manǯe-si-t</ta>
            <ta e="T210" id="Seg_1687" s="T209">i</ta>
            <ta e="T211" id="Seg_1688" s="T210">tʼära-s</ta>
            <ta e="T212" id="Seg_1689" s="T211">oːtʼä-ŋu</ta>
            <ta e="T213" id="Seg_1690" s="T212">oːtʼä-k-so-t</ta>
            <ta e="T214" id="Seg_1691" s="T213">nʼärnä</ta>
            <ta e="T215" id="Seg_1692" s="T214">iːli-lʼe</ta>
            <ta e="T216" id="Seg_1693" s="T215">oldʼi-so-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1694" s="T1">man</ta>
            <ta e="T3" id="Seg_1695" s="T2">niːba-mɨ-nɨ</ta>
            <ta e="T4" id="Seg_1696" s="T3">ton</ta>
            <ta e="T5" id="Seg_1697" s="T4">po-tɨ</ta>
            <ta e="T6" id="Seg_1698" s="T5">isklaj</ta>
            <ta e="T7" id="Seg_1699" s="T6">illɨ-psan</ta>
            <ta e="T8" id="Seg_1700" s="T7">üččə-k</ta>
            <ta e="T9" id="Seg_1701" s="T8">illɨ</ta>
            <ta e="T10" id="Seg_1702" s="T9">üččə-k</ta>
            <ta e="T11" id="Seg_1703" s="T10">illɨ</ta>
            <ta e="T12" id="Seg_1704" s="T11">tibindɨ-mbɨ</ta>
            <ta e="T13" id="Seg_1705" s="T12">nakkɨr</ta>
            <ta e="T14" id="Seg_1706" s="T13">po-n</ta>
            <ta e="T15" id="Seg_1707" s="T14">illɨ-sɨ-di</ta>
            <ta e="T16" id="Seg_1708" s="T15">üččə</ta>
            <ta e="T17" id="Seg_1709" s="T16">tʼäŋu-sɨ</ta>
            <ta e="T18" id="Seg_1710" s="T17">nɨːnɨ</ta>
            <ta e="T19" id="Seg_1711" s="T18">okkɨr</ta>
            <ta e="T20" id="Seg_1712" s="T19">äsɨ-sɨ</ta>
            <ta e="T21" id="Seg_1713" s="T20">qum-psan</ta>
            <ta e="T22" id="Seg_1714" s="T21">äsɨ-n</ta>
            <ta e="T23" id="Seg_1715" s="T22">nakkɨr</ta>
            <ta e="T24" id="Seg_1716" s="T23">po-n</ta>
            <ta e="T25" id="Seg_1717" s="T24">illɨ-sɨ-di</ta>
            <ta e="T26" id="Seg_1718" s="T25">üččə-la</ta>
            <ta e="T27" id="Seg_1719" s="T26">koči</ta>
            <ta e="T28" id="Seg_1720" s="T27">äsɨ-sɨ</ta>
            <ta e="T29" id="Seg_1721" s="T28">kundar</ta>
            <ta e="T30" id="Seg_1722" s="T29">orɨm-sɨ-tɨt</ta>
            <ta e="T31" id="Seg_1723" s="T30">nʼünʼü-ka-la</ta>
            <ta e="T32" id="Seg_1724" s="T31">quː-ku-lɨ-sɨ-tɨt</ta>
            <ta e="T33" id="Seg_1725" s="T32">niːba-mɨ</ta>
            <ta e="T34" id="Seg_1726" s="T33">i</ta>
            <ta e="T35" id="Seg_1727" s="T34">ildʼa-mɨ</ta>
            <ta e="T36" id="Seg_1728" s="T35">lata-tɨ-ŋ-sɨ-tɨt</ta>
            <ta e="T37" id="Seg_1729" s="T36">ira-tɨ</ta>
            <ta e="T38" id="Seg_1730" s="T37">üt-ɨ-m</ta>
            <ta e="T39" id="Seg_1731" s="T38">särbɨ-le</ta>
            <ta e="T40" id="Seg_1732" s="T39">tɨtə-r-ɨ-ku-sɨ</ta>
            <ta e="T41" id="Seg_1733" s="T40">qwodɨ-mbɨ-ku-sɨ</ta>
            <ta e="T42" id="Seg_1734" s="T41">eː-sɨ-n</ta>
            <ta e="T43" id="Seg_1735" s="T42">tʼürumbi-t</ta>
            <ta e="T44" id="Seg_1736" s="T43">tap</ta>
            <ta e="T45" id="Seg_1737" s="T44">tibindɨ-sɨ</ta>
            <ta e="T46" id="Seg_1738" s="T45">səru-lʼ</ta>
            <ta e="T47" id="Seg_1739" s="T46">qum-ɨ-n-nɨ</ta>
            <ta e="T48" id="Seg_1740" s="T47">tap-ɨ-n-nan</ta>
            <ta e="T49" id="Seg_1741" s="T48">nʼünʼü</ta>
            <ta e="T50" id="Seg_1742" s="T49">üččə-tɨ</ta>
            <ta e="T51" id="Seg_1743" s="T50">eː-sɨ-n</ta>
            <ta e="T52" id="Seg_1744" s="T51">niːba-mɨ</ta>
            <ta e="T53" id="Seg_1745" s="T52">tap-ɨ-m</ta>
            <ta e="T54" id="Seg_1746" s="T53">orɨm-nče-sɨ-tɨ</ta>
            <ta e="T55" id="Seg_1747" s="T54">i</ta>
            <ta e="T56" id="Seg_1748" s="T55">nät-sɨ-tɨ</ta>
            <ta e="T57" id="Seg_1749" s="T56">tap-qi-n-nan</ta>
            <ta e="T58" id="Seg_1750" s="T57">okkɨr</ta>
            <ta e="T59" id="Seg_1751" s="T58">neː-qum-ɨ-lʼ</ta>
            <ta e="T60" id="Seg_1752" s="T59">ütče-di</ta>
            <ta e="T61" id="Seg_1753" s="T60">ontɨ</ta>
            <ta e="T62" id="Seg_1754" s="T61">tʼärruŋ</ta>
            <ta e="T63" id="Seg_1755" s="T62">quː-sɨ-di</ta>
            <ta e="T64" id="Seg_1756" s="T63">niːba-n-nan</ta>
            <ta e="T65" id="Seg_1757" s="T64">wesʼ</ta>
            <ta e="T66" id="Seg_1758" s="T65">üččə-la-tɨ</ta>
            <ta e="T67" id="Seg_1759" s="T66">aːnuqoj</ta>
            <ta e="T68" id="Seg_1760" s="T67">ira-tɨ-nannɨ</ta>
            <ta e="T69" id="Seg_1761" s="T68">quː-mbɨ-tɨt</ta>
            <ta e="T70" id="Seg_1762" s="T69">nakkɨr</ta>
            <ta e="T71" id="Seg_1763" s="T70">neː-tɨ</ta>
            <ta e="T72" id="Seg_1764" s="T71">pirə-qəntɨ</ta>
            <ta e="T73" id="Seg_1765" s="T72">miːta-lewle</ta>
            <ta e="T74" id="Seg_1766" s="T73">nɨːnɨ</ta>
            <ta e="T75" id="Seg_1767" s="T74">quː-mbɨ-tɨt</ta>
            <ta e="T76" id="Seg_1768" s="T75">aːnuqoj</ta>
            <ta e="T77" id="Seg_1769" s="T76">ira-tɨ</ta>
            <ta e="T78" id="Seg_1770" s="T77">maːt-qən</ta>
            <ta e="T79" id="Seg_1771" s="T78">küːdɨ-le</ta>
            <ta e="T80" id="Seg_1772" s="T79">quː-sɨ-n</ta>
            <ta e="T81" id="Seg_1773" s="T80">nɨːnɨ</ta>
            <ta e="T82" id="Seg_1774" s="T81">ontɨ</ta>
            <ta e="T83" id="Seg_1775" s="T82">illɨ-ku-sɨ</ta>
            <ta e="T84" id="Seg_1776" s="T83">kundɨ</ta>
            <ta e="T85" id="Seg_1777" s="T84">nɨːnɨ</ta>
            <ta e="T86" id="Seg_1778" s="T85">tibindɨ-sɨ-n</ta>
            <ta e="T87" id="Seg_1779" s="T86">aːrɨŋ</ta>
            <ta e="T88" id="Seg_1780" s="T87">ira-n-nɨ</ta>
            <ta e="T89" id="Seg_1781" s="T88">qum-ɨ-n-nɨ</ta>
            <ta e="T90" id="Seg_1782" s="T89">tap</ta>
            <ta e="T91" id="Seg_1783" s="T90">eː-sɨ-n</ta>
            <ta e="T92" id="Seg_1784" s="T91">səru-m-mbɨdi</ta>
            <ta e="T93" id="Seg_1785" s="T92">tap-ɨ-n-nan</ta>
            <ta e="T94" id="Seg_1786" s="T93">aːnuqoj</ta>
            <ta e="T95" id="Seg_1787" s="T94">neː-qum-nannɨ</ta>
            <ta e="T96" id="Seg_1788" s="T95">šittə</ta>
            <ta e="T97" id="Seg_1789" s="T96">ütče-tɨ</ta>
            <ta e="T98" id="Seg_1790" s="T97">nɨːnɨ</ta>
            <ta e="T99" id="Seg_1791" s="T98">ontɨ</ta>
            <ta e="T100" id="Seg_1792" s="T99">üččə-la-di</ta>
            <ta e="T101" id="Seg_1793" s="T100">äsɨ-sɨ</ta>
            <ta e="T102" id="Seg_1794" s="T101">kuːn</ta>
            <ta e="T104" id="Seg_1795" s="T103">eː-sɨ-n</ta>
            <ta e="T105" id="Seg_1796" s="T104">tap</ta>
            <ta e="T106" id="Seg_1797" s="T105">äwɨlǯi-mbɨ-tɨ</ta>
            <ta e="T107" id="Seg_1798" s="T106">šittə-qi</ta>
            <ta e="T108" id="Seg_1799" s="T107">orɨm-sɨ-di</ta>
            <ta e="T109" id="Seg_1800" s="T108">okkɨr</ta>
            <ta e="T110" id="Seg_1801" s="T109">orɨm-sɨ-n</ta>
            <ta e="T111" id="Seg_1802" s="T110">i</ta>
            <ta e="T112" id="Seg_1803" s="T111">üt</ta>
            <ta e="T113" id="Seg_1804" s="T112">qoŋ-ŋɨ-n</ta>
            <ta e="T114" id="Seg_1805" s="T113">okkɨr</ta>
            <ta e="T115" id="Seg_1806" s="T114">jat</ta>
            <ta e="T116" id="Seg_1807" s="T115">illɨ-n</ta>
            <ta e="T117" id="Seg_1808" s="T116">tebe-qum-ɨ-n-nɨ</ta>
            <ta e="T118" id="Seg_1809" s="T117">tʼarɨ-sɨ-tɨt</ta>
            <ta e="T119" id="Seg_1810" s="T118">kuːnɨ-mbɨdi</ta>
            <ta e="T121" id="Seg_1811" s="T120">%%-m</ta>
            <ta e="T122" id="Seg_1812" s="T121">qwandɛ-gu</ta>
            <ta e="T123" id="Seg_1813" s="T122">tap</ta>
            <ta e="T124" id="Seg_1814" s="T123">qwandɛ-sɨ-tɨ</ta>
            <ta e="T125" id="Seg_1815" s="T124">%%-se</ta>
            <ta e="T126" id="Seg_1816" s="T125">%%</ta>
            <ta e="T127" id="Seg_1817" s="T126">tap-qi-m</ta>
            <ta e="T128" id="Seg_1818" s="T127">qwat-mbɨ-tɨ</ta>
            <ta e="T129" id="Seg_1819" s="T128">neː-m-tɨ</ta>
            <ta e="T130" id="Seg_1820" s="T129">assɨ</ta>
            <ta e="T131" id="Seg_1821" s="T130">qo-mbɨ-tɨt</ta>
            <ta e="T132" id="Seg_1822" s="T131">tap-ɨ-m</ta>
            <ta e="T133" id="Seg_1823" s="T132">oral-mbɨ-tɨt</ta>
            <ta e="T134" id="Seg_1824" s="T133">omdɨ-lʼčǝ-mbɨ-tɨ</ta>
            <ta e="T135" id="Seg_1825" s="T134">neː-qwaj</ta>
            <ta e="T136" id="Seg_1826" s="T135">üččə</ta>
            <ta e="T137" id="Seg_1827" s="T136">soqu-ndʼü-ku-le</ta>
            <ta e="T138" id="Seg_1828" s="T137">kuːča</ta>
            <ta e="T139" id="Seg_1829" s="T138">qattə-mbɨ-mbɨdi</ta>
            <ta e="T140" id="Seg_1830" s="T139">tap-ɨ</ta>
            <ta e="T141" id="Seg_1831" s="T140">assɨ</ta>
            <ta e="T142" id="Seg_1832" s="T141">kät-sɨ-tɨ</ta>
            <ta e="T143" id="Seg_1833" s="T142">tap</ta>
            <ta e="T144" id="Seg_1834" s="T143">tʼarɨ-n</ta>
            <ta e="T145" id="Seg_1835" s="T144">tаŋо-lʼ</ta>
            <ta e="T146" id="Seg_1836" s="T145">wattə-mɨn</ta>
            <ta e="T147" id="Seg_1837" s="T146">qwən-mbɨ</ta>
            <ta e="T148" id="Seg_1838" s="T147">maːt-ndɨ</ta>
            <ta e="T149" id="Seg_1839" s="T148">tap</ta>
            <ta e="T150" id="Seg_1840" s="T149">sitteptɨ-sɨ</ta>
            <ta e="T151" id="Seg_1841" s="T150">tap</ta>
            <ta e="T152" id="Seg_1842" s="T151">kuːda</ta>
            <ta e="T153" id="Seg_1843" s="T152">qattə-mbɨ-mbɨ-tɨ</ta>
            <ta e="T154" id="Seg_1844" s="T153">niːba</ta>
            <ta e="T155" id="Seg_1845" s="T154">qalɨ-n</ta>
            <ta e="T156" id="Seg_1846" s="T155">apjatʼ</ta>
            <ta e="T157" id="Seg_1847" s="T156">ira-gaːlɨ-k</ta>
            <ta e="T158" id="Seg_1848" s="T157">nakkɨr-mǯel-ǯij</ta>
            <ta e="T159" id="Seg_1849" s="T158">ira-tɨ</ta>
            <ta e="T160" id="Seg_1850" s="T159">matʼtʼi-qən</ta>
            <ta e="T161" id="Seg_1851" s="T160">tüː</ta>
            <ta e="T162" id="Seg_1852" s="T161">am-ɨ-mbɨ-tɨ</ta>
            <ta e="T163" id="Seg_1853" s="T162">tap</ta>
            <ta e="T164" id="Seg_1854" s="T163">illɨ-sɨ</ta>
            <ta e="T165" id="Seg_1855" s="T164">iː-n-tɨ-se</ta>
            <ta e="T166" id="Seg_1856" s="T165">nɨːnɨ</ta>
            <ta e="T167" id="Seg_1857" s="T166">na</ta>
            <ta e="T168" id="Seg_1858" s="T167">iː-tɨ</ta>
            <ta e="T169" id="Seg_1859" s="T168">qoŋ-sɨ-n</ta>
            <ta e="T170" id="Seg_1860" s="T169">tap</ta>
            <ta e="T171" id="Seg_1861" s="T170">eː-sɨ-n</ta>
            <ta e="T172" id="Seg_1862" s="T171">nakkɨr</ta>
            <ta e="T173" id="Seg_1863" s="T172">saːrum</ta>
            <ta e="T174" id="Seg_1864" s="T173">saːrum-ɨ-lʼ</ta>
            <ta e="T175" id="Seg_1865" s="T174">po-tɨ</ta>
            <ta e="T176" id="Seg_1866" s="T175">niːba-mɨ</ta>
            <ta e="T177" id="Seg_1867" s="T176">illɨ-sɨ</ta>
            <ta e="T178" id="Seg_1868" s="T177">matʼtʼi-qən</ta>
            <ta e="T179" id="Seg_1869" s="T178">ütče-la-n-tɨ-se</ta>
            <ta e="T180" id="Seg_1870" s="T179">nʼaja-le</ta>
            <ta e="T181" id="Seg_1871" s="T180">i</ta>
            <ta e="T182" id="Seg_1872" s="T181">neː-tɨ</ta>
            <ta e="T183" id="Seg_1873" s="T182">ontɨ</ta>
            <ta e="T184" id="Seg_1874" s="T183">sija-m-tɨ</ta>
            <ta e="T185" id="Seg_1875" s="T184">tʼaččɨ-mbɨ-tɨ</ta>
            <ta e="T186" id="Seg_1876" s="T185">kanak-m-tɨ</ta>
            <ta e="T187" id="Seg_1877" s="T186">qäte-nče-le</ta>
            <ta e="T188" id="Seg_1878" s="T187">neː-tɨ</ta>
            <ta e="T189" id="Seg_1879" s="T188">ugoːn</ta>
            <ta e="T190" id="Seg_1880" s="T189">qwən-sɨ</ta>
            <ta e="T191" id="Seg_1881" s="T190">nɨːnɨ</ta>
            <ta e="T192" id="Seg_1882" s="T191">niːba-mɨ</ta>
            <ta e="T193" id="Seg_1883" s="T192">qwən-sɨ</ta>
            <ta e="T194" id="Seg_1884" s="T193">niːba-mɨ</ta>
            <ta e="T195" id="Seg_1885" s="T194">tüː-sɨ-n</ta>
            <ta e="T196" id="Seg_1886" s="T195">tap</ta>
            <ta e="T197" id="Seg_1887" s="T196">uʒ</ta>
            <ta e="T198" id="Seg_1888" s="T197">quː-mbɨ</ta>
            <ta e="T199" id="Seg_1889" s="T198">niːba-mɨ</ta>
            <ta e="T200" id="Seg_1890" s="T199">kɨsi-ku-sɨ</ta>
            <ta e="T201" id="Seg_1891" s="T200">qanǯe-se</ta>
            <ta e="T202" id="Seg_1892" s="T201">maːt-ndɨ</ta>
            <ta e="T203" id="Seg_1893" s="T202">tat-sɨ-tɨt</ta>
            <ta e="T204" id="Seg_1894" s="T203">kundɨ</ta>
            <ta e="T205" id="Seg_1895" s="T204">maːt-qən</ta>
            <ta e="T206" id="Seg_1896" s="T205">ippi-sɨ</ta>
            <ta e="T207" id="Seg_1897" s="T206">tüː-sɨ-n</ta>
            <ta e="T208" id="Seg_1898" s="T207">vrač</ta>
            <ta e="T209" id="Seg_1899" s="T208">manǯɨ-sɨ-tɨ</ta>
            <ta e="T210" id="Seg_1900" s="T209">i</ta>
            <ta e="T211" id="Seg_1901" s="T210">tʼarɨ-sɨ</ta>
            <ta e="T212" id="Seg_1902" s="T211">oːtʼä-gu</ta>
            <ta e="T213" id="Seg_1903" s="T212">oːtʼä-ku-sɨ-tɨt</ta>
            <ta e="T214" id="Seg_1904" s="T213">nʼarne</ta>
            <ta e="T215" id="Seg_1905" s="T214">illɨ-le</ta>
            <ta e="T216" id="Seg_1906" s="T215">oldǝ-sɨ-tɨt</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1907" s="T1">I.GEN</ta>
            <ta e="T3" id="Seg_1908" s="T2">grandmother-1SG-ALL</ta>
            <ta e="T4" id="Seg_1909" s="T3">hundred</ta>
            <ta e="T5" id="Seg_1910" s="T4">year.[NOM]-3SG</ta>
            <ta e="T6" id="Seg_1911" s="T5">bad</ta>
            <ta e="T7" id="Seg_1912" s="T6">live-ACTN2</ta>
            <ta e="T8" id="Seg_1913" s="T7">child-ADVZ</ta>
            <ta e="T9" id="Seg_1914" s="T8">live.[3SG.S]</ta>
            <ta e="T10" id="Seg_1915" s="T9">child-ADVZ</ta>
            <ta e="T11" id="Seg_1916" s="T10">live.[3SG.S]</ta>
            <ta e="T12" id="Seg_1917" s="T11">marry-PST.NAR.[3SG.S]</ta>
            <ta e="T13" id="Seg_1918" s="T12">three</ta>
            <ta e="T14" id="Seg_1919" s="T13">year-ADV.LOC</ta>
            <ta e="T15" id="Seg_1920" s="T14">live-PST-3DU.S</ta>
            <ta e="T16" id="Seg_1921" s="T15">child.[NOM]</ta>
            <ta e="T17" id="Seg_1922" s="T16">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T18" id="Seg_1923" s="T17">then</ta>
            <ta e="T19" id="Seg_1924" s="T18">one</ta>
            <ta e="T20" id="Seg_1925" s="T19">become-PST.[3SG.S]</ta>
            <ta e="T21" id="Seg_1926" s="T20">human.being-ACTN2</ta>
            <ta e="T22" id="Seg_1927" s="T21">become-3SG.S</ta>
            <ta e="T23" id="Seg_1928" s="T22">three</ta>
            <ta e="T24" id="Seg_1929" s="T23">year-ADV.LOC</ta>
            <ta e="T25" id="Seg_1930" s="T24">live-PST-3DU.S</ta>
            <ta e="T26" id="Seg_1931" s="T25">child-PL.[NOM]</ta>
            <ta e="T27" id="Seg_1932" s="T26">much</ta>
            <ta e="T28" id="Seg_1933" s="T27">become-PST.[3SG.S]</ta>
            <ta e="T29" id="Seg_1934" s="T28">how</ta>
            <ta e="T30" id="Seg_1935" s="T29">grow.up-PST-3PL</ta>
            <ta e="T31" id="Seg_1936" s="T30">small-DIM-PL.[NOM]</ta>
            <ta e="T32" id="Seg_1937" s="T31">die-HAB-RES-PST-3PL</ta>
            <ta e="T33" id="Seg_1938" s="T32">grandmother.[NOM]-1SG</ta>
            <ta e="T34" id="Seg_1939" s="T33">and</ta>
            <ta e="T35" id="Seg_1940" s="T34">grandfather.[NOM]-1SG</ta>
            <ta e="T36" id="Seg_1941" s="T35">trap-TR-1SG.S-PST-3PL</ta>
            <ta e="T37" id="Seg_1942" s="T36">husband.[NOM]-3SG</ta>
            <ta e="T38" id="Seg_1943" s="T37">water-EP-ACC</ta>
            <ta e="T39" id="Seg_1944" s="T38">be.drunken-CVB</ta>
            <ta e="T40" id="Seg_1945" s="T39">quarrel-FRQ-EP-HAB-PST.[3SG.S]</ta>
            <ta e="T41" id="Seg_1946" s="T40">scold-DUR-HAB-PST.[3SG.S]</ta>
            <ta e="T42" id="Seg_1947" s="T41">be-PST-3SG.S</ta>
            <ta e="T43" id="Seg_1948" s="T42">be.angry-PTCP.PRS.[3SG.S]</ta>
            <ta e="T44" id="Seg_1949" s="T43">(s)he.[NOM]</ta>
            <ta e="T45" id="Seg_1950" s="T44">marry-PST.[3SG.S]</ta>
            <ta e="T46" id="Seg_1951" s="T45">widower-ADJZ</ta>
            <ta e="T47" id="Seg_1952" s="T46">human.being-EP-GEN-ALL</ta>
            <ta e="T48" id="Seg_1953" s="T47">(s)he-EP-GEN-ADES</ta>
            <ta e="T49" id="Seg_1954" s="T48">small</ta>
            <ta e="T50" id="Seg_1955" s="T49">child.[NOM]-3SG</ta>
            <ta e="T51" id="Seg_1956" s="T50">be-PST-3SG.S</ta>
            <ta e="T52" id="Seg_1957" s="T51">grandmother.[NOM]-1SG</ta>
            <ta e="T53" id="Seg_1958" s="T52">(s)he-EP-ACC</ta>
            <ta e="T54" id="Seg_1959" s="T53">grow.up-IPFV3-PST-3SG.O</ta>
            <ta e="T55" id="Seg_1960" s="T54">and</ta>
            <ta e="T56" id="Seg_1961" s="T55">marry-PST-3SG.O</ta>
            <ta e="T57" id="Seg_1962" s="T56">(s)he-DU-GEN-ADES</ta>
            <ta e="T58" id="Seg_1963" s="T57">one</ta>
            <ta e="T59" id="Seg_1964" s="T58">daughter-human.being-EP-ADJZ</ta>
            <ta e="T60" id="Seg_1965" s="T59">child.[NOM]-3DU</ta>
            <ta e="T61" id="Seg_1966" s="T60">oneself.3SG</ta>
            <ta e="T62" id="Seg_1967" s="T61">%%</ta>
            <ta e="T63" id="Seg_1968" s="T62">die-PST-3DU.S</ta>
            <ta e="T64" id="Seg_1969" s="T63">grandmother-GEN-ADES</ta>
            <ta e="T65" id="Seg_1970" s="T64">all</ta>
            <ta e="T66" id="Seg_1971" s="T65">child-PL.[NOM]-3SG</ta>
            <ta e="T67" id="Seg_1972" s="T66">first</ta>
            <ta e="T68" id="Seg_1973" s="T67">husband-3SG-ABL</ta>
            <ta e="T69" id="Seg_1974" s="T68">die-PST.NAR-3PL</ta>
            <ta e="T70" id="Seg_1975" s="T69">three</ta>
            <ta e="T71" id="Seg_1976" s="T70">daughter.[NOM]-3SG</ta>
            <ta e="T72" id="Seg_1977" s="T71">stature-ILL.3SG</ta>
            <ta e="T73" id="Seg_1978" s="T72">achieve-CVB2</ta>
            <ta e="T74" id="Seg_1979" s="T73">then</ta>
            <ta e="T75" id="Seg_1980" s="T74">die-PST.NAR-3PL</ta>
            <ta e="T76" id="Seg_1981" s="T75">first</ta>
            <ta e="T77" id="Seg_1982" s="T76">husband.[NOM]-3SG</ta>
            <ta e="T78" id="Seg_1983" s="T77">house-LOC</ta>
            <ta e="T79" id="Seg_1984" s="T78">be.ill-CVB</ta>
            <ta e="T80" id="Seg_1985" s="T79">die-PST-3SG.S</ta>
            <ta e="T81" id="Seg_1986" s="T80">then</ta>
            <ta e="T82" id="Seg_1987" s="T81">oneself.3SG</ta>
            <ta e="T83" id="Seg_1988" s="T82">live-HAB-PST.[3SG.S]</ta>
            <ta e="T84" id="Seg_1989" s="T83">long</ta>
            <ta e="T85" id="Seg_1990" s="T84">then</ta>
            <ta e="T86" id="Seg_1991" s="T85">marry-PST-3SG.S</ta>
            <ta e="T87" id="Seg_1992" s="T86">other</ta>
            <ta e="T88" id="Seg_1993" s="T87">husband-GEN-ALL</ta>
            <ta e="T89" id="Seg_1994" s="T88">human.being-EP-GEN-ALL</ta>
            <ta e="T90" id="Seg_1995" s="T89">(s)he.[NOM]</ta>
            <ta e="T91" id="Seg_1996" s="T90">be-PST-3SG.S</ta>
            <ta e="T92" id="Seg_1997" s="T91">widower-TRL-PTCP.PST</ta>
            <ta e="T93" id="Seg_1998" s="T92">(s)he-EP-GEN-ADES</ta>
            <ta e="T94" id="Seg_1999" s="T93">first</ta>
            <ta e="T95" id="Seg_2000" s="T94">woman-human.being-ABL</ta>
            <ta e="T96" id="Seg_2001" s="T95">two</ta>
            <ta e="T97" id="Seg_2002" s="T96">child-3SG</ta>
            <ta e="T98" id="Seg_2003" s="T97">then</ta>
            <ta e="T99" id="Seg_2004" s="T98">oneself.3SG</ta>
            <ta e="T100" id="Seg_2005" s="T99">child-PL.[NOM]-3DU</ta>
            <ta e="T101" id="Seg_2006" s="T100">become-PST.[3SG.S]</ta>
            <ta e="T102" id="Seg_2007" s="T101">where</ta>
            <ta e="T103" id="Seg_2008" s="T102">%%</ta>
            <ta e="T104" id="Seg_2009" s="T103">be-PST-3SG.S</ta>
            <ta e="T105" id="Seg_2010" s="T104">(s)he.[NOM]</ta>
            <ta e="T106" id="Seg_2011" s="T105">forgot-PST.NAR-3SG.O</ta>
            <ta e="T107" id="Seg_2012" s="T106">two-DU.[NOM]</ta>
            <ta e="T108" id="Seg_2013" s="T107">grow.up-PST-3DU.S</ta>
            <ta e="T109" id="Seg_2014" s="T108">one</ta>
            <ta e="T110" id="Seg_2015" s="T109">grow.up-PST-3SG.S</ta>
            <ta e="T111" id="Seg_2016" s="T110">and</ta>
            <ta e="T112" id="Seg_2017" s="T111">water.[NOM]</ta>
            <ta e="T113" id="Seg_2018" s="T112">drown-CO-3SG.S</ta>
            <ta e="T114" id="Seg_2019" s="T113">one</ta>
            <ta e="T115" id="Seg_2020" s="T114">%%</ta>
            <ta e="T116" id="Seg_2021" s="T115">live-3SG.S</ta>
            <ta e="T117" id="Seg_2022" s="T116">man-human.being-EP-GEN-ALL</ta>
            <ta e="T118" id="Seg_2023" s="T117">say-PST-3PL</ta>
            <ta e="T119" id="Seg_2024" s="T118">run.away-PTCP.PST</ta>
            <ta e="T121" id="Seg_2025" s="T120">%%-ACC</ta>
            <ta e="T122" id="Seg_2026" s="T121">carry.away-INF</ta>
            <ta e="T123" id="Seg_2027" s="T122">(s)he.[NOM]</ta>
            <ta e="T124" id="Seg_2028" s="T123">carry.away-PST-3SG.O</ta>
            <ta e="T125" id="Seg_2029" s="T124">%%-COM</ta>
            <ta e="T127" id="Seg_2030" s="T126">(s)he-DU-ACC</ta>
            <ta e="T128" id="Seg_2031" s="T127">kill-PST.NAR-3SG.O</ta>
            <ta e="T129" id="Seg_2032" s="T128">daughter-ACC-3SG</ta>
            <ta e="T130" id="Seg_2033" s="T129">NEG</ta>
            <ta e="T131" id="Seg_2034" s="T130">find-PST.NAR-3PL</ta>
            <ta e="T132" id="Seg_2035" s="T131">(s)he-EP-ACC</ta>
            <ta e="T133" id="Seg_2036" s="T132">catch-PST.NAR-3PL</ta>
            <ta e="T134" id="Seg_2037" s="T133">sit.down-PFV-PST.NAR-3SG.O</ta>
            <ta e="T135" id="Seg_2038" s="T134">daughter-soul</ta>
            <ta e="T136" id="Seg_2039" s="T135">child.[NOM]</ta>
            <ta e="T137" id="Seg_2040" s="T136">ask-DRV-HAB-CVB</ta>
            <ta e="T138" id="Seg_2041" s="T137">where</ta>
            <ta e="T139" id="Seg_2042" s="T138">disappear-DUR-PTCP.PST</ta>
            <ta e="T140" id="Seg_2043" s="T139">(s)he-EP.[NOM]</ta>
            <ta e="T141" id="Seg_2044" s="T140">NEG</ta>
            <ta e="T142" id="Seg_2045" s="T141">say-PST-3SG.O</ta>
            <ta e="T143" id="Seg_2046" s="T142">(s)he.[NOM]</ta>
            <ta e="T144" id="Seg_2047" s="T143">say-3SG.S</ta>
            <ta e="T145" id="Seg_2048" s="T144">trap-ADJZ</ta>
            <ta e="T146" id="Seg_2049" s="T145">road-PROL</ta>
            <ta e="T147" id="Seg_2050" s="T146">go.away-PST.NAR.[3SG.S]</ta>
            <ta e="T148" id="Seg_2051" s="T147">house-ILL</ta>
            <ta e="T149" id="Seg_2052" s="T148">(s)he.[NOM]</ta>
            <ta e="T150" id="Seg_2053" s="T149">cheat-PST.[3SG.S]</ta>
            <ta e="T151" id="Seg_2054" s="T150">(s)he.[NOM]</ta>
            <ta e="T152" id="Seg_2055" s="T151">where</ta>
            <ta e="T153" id="Seg_2056" s="T152">disappear-DUR-PST.NAR-3SG.O</ta>
            <ta e="T154" id="Seg_2057" s="T153">grandmother.[NOM]</ta>
            <ta e="T155" id="Seg_2058" s="T154">stay-3SG.S</ta>
            <ta e="T156" id="Seg_2059" s="T155">again</ta>
            <ta e="T157" id="Seg_2060" s="T156">husband-CAR-ADVZ</ta>
            <ta e="T158" id="Seg_2061" s="T157">three-ORD-DRV</ta>
            <ta e="T159" id="Seg_2062" s="T158">husband.[NOM]-3SG</ta>
            <ta e="T160" id="Seg_2063" s="T159">taiga-LOC</ta>
            <ta e="T161" id="Seg_2064" s="T160">fire.[NOM]</ta>
            <ta e="T162" id="Seg_2065" s="T161">eat-EP-PST.NAR-3SG.O</ta>
            <ta e="T163" id="Seg_2066" s="T162">(s)he.[NOM]</ta>
            <ta e="T164" id="Seg_2067" s="T163">live-PST.[3SG.S]</ta>
            <ta e="T165" id="Seg_2068" s="T164">son-GEN-3SG-COM</ta>
            <ta e="T166" id="Seg_2069" s="T165">then</ta>
            <ta e="T167" id="Seg_2070" s="T166">this</ta>
            <ta e="T168" id="Seg_2071" s="T167">son.[NOM]-3SG</ta>
            <ta e="T169" id="Seg_2072" s="T168">drown-PST-3SG.S</ta>
            <ta e="T170" id="Seg_2073" s="T169">(s)he.[NOM]</ta>
            <ta e="T171" id="Seg_2074" s="T170">be-PST-3SG.S</ta>
            <ta e="T172" id="Seg_2075" s="T171">three</ta>
            <ta e="T173" id="Seg_2076" s="T172">ten</ta>
            <ta e="T174" id="Seg_2077" s="T173">ten-EP-ADJZ</ta>
            <ta e="T175" id="Seg_2078" s="T174">year-3SG</ta>
            <ta e="T176" id="Seg_2079" s="T175">grandmother.[NOM]-1SG</ta>
            <ta e="T177" id="Seg_2080" s="T176">live-PST.[3SG.S]</ta>
            <ta e="T178" id="Seg_2081" s="T177">taiga-LOC</ta>
            <ta e="T179" id="Seg_2082" s="T178">child-PL-GEN-3SG-COM</ta>
            <ta e="T180" id="Seg_2083" s="T179">squirrel.[VBLZ]-CVB</ta>
            <ta e="T181" id="Seg_2084" s="T180">and</ta>
            <ta e="T182" id="Seg_2085" s="T181">daughter.[NOM]-3SG</ta>
            <ta e="T183" id="Seg_2086" s="T182">oneself.3SG</ta>
            <ta e="T184" id="Seg_2087" s="T183">%%-ACC-3SG</ta>
            <ta e="T185" id="Seg_2088" s="T184">shoot-PST.NAR-3SG.O</ta>
            <ta e="T186" id="Seg_2089" s="T185">dog-ACC-3SG</ta>
            <ta e="T187" id="Seg_2090" s="T186">hit-IPFV3-CVB</ta>
            <ta e="T188" id="Seg_2091" s="T187">daughter.[NOM]-3SG</ta>
            <ta e="T189" id="Seg_2092" s="T188">in.front</ta>
            <ta e="T190" id="Seg_2093" s="T189">go.away-PST.[3SG.S]</ta>
            <ta e="T191" id="Seg_2094" s="T190">then</ta>
            <ta e="T192" id="Seg_2095" s="T191">grandmother.[NOM]-1SG</ta>
            <ta e="T193" id="Seg_2096" s="T192">go.away-PST.[3SG.S]</ta>
            <ta e="T194" id="Seg_2097" s="T193">grandmother.[NOM]-1SG</ta>
            <ta e="T195" id="Seg_2098" s="T194">come-PST-3SG.S</ta>
            <ta e="T196" id="Seg_2099" s="T195">(s)he.[NOM]</ta>
            <ta e="T197" id="Seg_2100" s="T196">well</ta>
            <ta e="T198" id="Seg_2101" s="T197">die-PST.NAR.[3SG.S]</ta>
            <ta e="T199" id="Seg_2102" s="T198">grandmother.[NOM]-1SG</ta>
            <ta e="T200" id="Seg_2103" s="T199">%%-HAB-PST.[3SG.S]</ta>
            <ta e="T201" id="Seg_2104" s="T200">sledge-INSTR</ta>
            <ta e="T202" id="Seg_2105" s="T201">house-ILL</ta>
            <ta e="T203" id="Seg_2106" s="T202">bring-PST-3PL</ta>
            <ta e="T204" id="Seg_2107" s="T203">long</ta>
            <ta e="T205" id="Seg_2108" s="T204">house-LOC</ta>
            <ta e="T206" id="Seg_2109" s="T205">lie-PST.[3SG.S]</ta>
            <ta e="T207" id="Seg_2110" s="T206">come-PST-3SG.S</ta>
            <ta e="T208" id="Seg_2111" s="T207">doctor</ta>
            <ta e="T209" id="Seg_2112" s="T208">look-PST-3SG.O</ta>
            <ta e="T210" id="Seg_2113" s="T209">and</ta>
            <ta e="T211" id="Seg_2114" s="T210">say-PST.[3SG.S]</ta>
            <ta e="T212" id="Seg_2115" s="T211">bury-INF</ta>
            <ta e="T213" id="Seg_2116" s="T212">bury-HAB-PST-3PL</ta>
            <ta e="T214" id="Seg_2117" s="T213">forward</ta>
            <ta e="T215" id="Seg_2118" s="T214">live-CVB</ta>
            <ta e="T216" id="Seg_2119" s="T215">start-PST-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_2120" s="T1">я.GEN</ta>
            <ta e="T3" id="Seg_2121" s="T2">бабушка-1SG-ALL</ta>
            <ta e="T4" id="Seg_2122" s="T3">сто</ta>
            <ta e="T5" id="Seg_2123" s="T4">год.[NOM]-3SG</ta>
            <ta e="T6" id="Seg_2124" s="T5">плохой</ta>
            <ta e="T7" id="Seg_2125" s="T6">жить-ACTN2</ta>
            <ta e="T8" id="Seg_2126" s="T7">ребёнок-ADVZ</ta>
            <ta e="T9" id="Seg_2127" s="T8">жить.[3SG.S]</ta>
            <ta e="T10" id="Seg_2128" s="T9">ребёнок-ADVZ</ta>
            <ta e="T11" id="Seg_2129" s="T10">жить.[3SG.S]</ta>
            <ta e="T12" id="Seg_2130" s="T11">выйти.замуж-PST.NAR.[3SG.S]</ta>
            <ta e="T13" id="Seg_2131" s="T12">три</ta>
            <ta e="T14" id="Seg_2132" s="T13">год-ADV.LOC</ta>
            <ta e="T15" id="Seg_2133" s="T14">жить-PST-3DU.S</ta>
            <ta e="T16" id="Seg_2134" s="T15">ребёнок.[NOM]</ta>
            <ta e="T17" id="Seg_2135" s="T16">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T18" id="Seg_2136" s="T17">потом</ta>
            <ta e="T19" id="Seg_2137" s="T18">один</ta>
            <ta e="T20" id="Seg_2138" s="T19">стать-PST.[3SG.S]</ta>
            <ta e="T21" id="Seg_2139" s="T20">человек-ACTN2</ta>
            <ta e="T22" id="Seg_2140" s="T21">стать-3SG.S</ta>
            <ta e="T23" id="Seg_2141" s="T22">три</ta>
            <ta e="T24" id="Seg_2142" s="T23">год-ADV.LOC</ta>
            <ta e="T25" id="Seg_2143" s="T24">жить-PST-3DU.S</ta>
            <ta e="T26" id="Seg_2144" s="T25">ребёнок-PL.[NOM]</ta>
            <ta e="T27" id="Seg_2145" s="T26">много</ta>
            <ta e="T28" id="Seg_2146" s="T27">стать-PST.[3SG.S]</ta>
            <ta e="T29" id="Seg_2147" s="T28">как</ta>
            <ta e="T30" id="Seg_2148" s="T29">вырасти-PST-3PL</ta>
            <ta e="T31" id="Seg_2149" s="T30">маленький-DIM-PL.[NOM]</ta>
            <ta e="T32" id="Seg_2150" s="T31">умереть-HAB-RES-PST-3PL</ta>
            <ta e="T33" id="Seg_2151" s="T32">бабушка.[NOM]-1SG</ta>
            <ta e="T34" id="Seg_2152" s="T33">и</ta>
            <ta e="T35" id="Seg_2153" s="T34">дедушка.[NOM]-1SG</ta>
            <ta e="T36" id="Seg_2154" s="T35">черкан-TR-1SG.S-PST-3PL</ta>
            <ta e="T37" id="Seg_2155" s="T36">муж.[NOM]-3SG</ta>
            <ta e="T38" id="Seg_2156" s="T37">вода-EP-ACC</ta>
            <ta e="T39" id="Seg_2157" s="T38">быть.пьяным-CVB</ta>
            <ta e="T40" id="Seg_2158" s="T39">драться-FRQ-EP-HAB-PST.[3SG.S]</ta>
            <ta e="T41" id="Seg_2159" s="T40">выругать-DUR-HAB-PST.[3SG.S]</ta>
            <ta e="T42" id="Seg_2160" s="T41">быть-PST-3SG.S</ta>
            <ta e="T43" id="Seg_2161" s="T42">быть.сердитым-PTCP.PRS.[3SG.S]</ta>
            <ta e="T44" id="Seg_2162" s="T43">он(а).[NOM]</ta>
            <ta e="T45" id="Seg_2163" s="T44">выйти.замуж-PST.[3SG.S]</ta>
            <ta e="T46" id="Seg_2164" s="T45">вдовец-ADJZ</ta>
            <ta e="T47" id="Seg_2165" s="T46">человек-EP-GEN-ALL</ta>
            <ta e="T48" id="Seg_2166" s="T47">он(а)-EP-GEN-ADES</ta>
            <ta e="T49" id="Seg_2167" s="T48">маленький</ta>
            <ta e="T50" id="Seg_2168" s="T49">ребёнок.[NOM]-3SG</ta>
            <ta e="T51" id="Seg_2169" s="T50">быть-PST-3SG.S</ta>
            <ta e="T52" id="Seg_2170" s="T51">бабушка.[NOM]-1SG</ta>
            <ta e="T53" id="Seg_2171" s="T52">он(а)-EP-ACC</ta>
            <ta e="T54" id="Seg_2172" s="T53">вырасти-IPFV3-PST-3SG.O</ta>
            <ta e="T55" id="Seg_2173" s="T54">и</ta>
            <ta e="T56" id="Seg_2174" s="T55">женить-PST-3SG.O</ta>
            <ta e="T57" id="Seg_2175" s="T56">он(а)-DU-GEN-ADES</ta>
            <ta e="T58" id="Seg_2176" s="T57">один</ta>
            <ta e="T59" id="Seg_2177" s="T58">дочь-человек-EP-ADJZ</ta>
            <ta e="T60" id="Seg_2178" s="T59">ребёнок.[NOM]-3DU</ta>
            <ta e="T61" id="Seg_2179" s="T60">сам.3SG</ta>
            <ta e="T62" id="Seg_2180" s="T61">%%</ta>
            <ta e="T63" id="Seg_2181" s="T62">умереть-PST-3DU.S</ta>
            <ta e="T64" id="Seg_2182" s="T63">бабушка-GEN-ADES</ta>
            <ta e="T65" id="Seg_2183" s="T64">весь</ta>
            <ta e="T66" id="Seg_2184" s="T65">ребёнок-PL.[NOM]-3SG</ta>
            <ta e="T67" id="Seg_2185" s="T66">первый</ta>
            <ta e="T68" id="Seg_2186" s="T67">муж-3SG-ABL</ta>
            <ta e="T69" id="Seg_2187" s="T68">умереть-PST.NAR-3PL</ta>
            <ta e="T70" id="Seg_2188" s="T69">три</ta>
            <ta e="T71" id="Seg_2189" s="T70">дочь.[NOM]-3SG</ta>
            <ta e="T72" id="Seg_2190" s="T71">рост-ILL.3SG</ta>
            <ta e="T73" id="Seg_2191" s="T72">достичь-CVB2</ta>
            <ta e="T74" id="Seg_2192" s="T73">потом</ta>
            <ta e="T75" id="Seg_2193" s="T74">умереть-PST.NAR-3PL</ta>
            <ta e="T76" id="Seg_2194" s="T75">первый</ta>
            <ta e="T77" id="Seg_2195" s="T76">муж.[NOM]-3SG</ta>
            <ta e="T78" id="Seg_2196" s="T77">дом-LOC</ta>
            <ta e="T79" id="Seg_2197" s="T78">болеть-CVB</ta>
            <ta e="T80" id="Seg_2198" s="T79">умереть-PST-3SG.S</ta>
            <ta e="T81" id="Seg_2199" s="T80">потом</ta>
            <ta e="T82" id="Seg_2200" s="T81">сам.3SG</ta>
            <ta e="T83" id="Seg_2201" s="T82">жить-HAB-PST.[3SG.S]</ta>
            <ta e="T84" id="Seg_2202" s="T83">долго</ta>
            <ta e="T85" id="Seg_2203" s="T84">потом</ta>
            <ta e="T86" id="Seg_2204" s="T85">выйти.замуж-PST-3SG.S</ta>
            <ta e="T87" id="Seg_2205" s="T86">другой</ta>
            <ta e="T88" id="Seg_2206" s="T87">муж-GEN-ALL</ta>
            <ta e="T89" id="Seg_2207" s="T88">человек-EP-GEN-ALL</ta>
            <ta e="T90" id="Seg_2208" s="T89">он(а).[NOM]</ta>
            <ta e="T91" id="Seg_2209" s="T90">быть-PST-3SG.S</ta>
            <ta e="T92" id="Seg_2210" s="T91">вдовец-TRL-PTCP.PST</ta>
            <ta e="T93" id="Seg_2211" s="T92">он(а)-EP-GEN-ADES</ta>
            <ta e="T94" id="Seg_2212" s="T93">первый</ta>
            <ta e="T95" id="Seg_2213" s="T94">женщина-человек-ABL</ta>
            <ta e="T96" id="Seg_2214" s="T95">два</ta>
            <ta e="T97" id="Seg_2215" s="T96">ребёнок-3SG</ta>
            <ta e="T98" id="Seg_2216" s="T97">потом</ta>
            <ta e="T99" id="Seg_2217" s="T98">сам.3SG</ta>
            <ta e="T100" id="Seg_2218" s="T99">ребёнок-PL.[NOM]-3DU</ta>
            <ta e="T101" id="Seg_2219" s="T100">стать-PST.[3SG.S]</ta>
            <ta e="T102" id="Seg_2220" s="T101">где</ta>
            <ta e="T103" id="Seg_2221" s="T102">%%</ta>
            <ta e="T104" id="Seg_2222" s="T103">быть-PST-3SG.S</ta>
            <ta e="T105" id="Seg_2223" s="T104">он(а).[NOM]</ta>
            <ta e="T106" id="Seg_2224" s="T105">забыть-PST.NAR-3SG.O</ta>
            <ta e="T107" id="Seg_2225" s="T106">два-DU.[NOM]</ta>
            <ta e="T108" id="Seg_2226" s="T107">вырасти-PST-3DU.S</ta>
            <ta e="T109" id="Seg_2227" s="T108">один</ta>
            <ta e="T110" id="Seg_2228" s="T109">вырасти-PST-3SG.S</ta>
            <ta e="T111" id="Seg_2229" s="T110">и</ta>
            <ta e="T112" id="Seg_2230" s="T111">вода.[NOM]</ta>
            <ta e="T113" id="Seg_2231" s="T112">утонуть-CO-3SG.S</ta>
            <ta e="T114" id="Seg_2232" s="T113">один</ta>
            <ta e="T115" id="Seg_2233" s="T114">%%</ta>
            <ta e="T116" id="Seg_2234" s="T115">жить-3SG.S</ta>
            <ta e="T117" id="Seg_2235" s="T116">мужчина-человек-EP-GEN-ALL</ta>
            <ta e="T118" id="Seg_2236" s="T117">сказать-PST-3PL</ta>
            <ta e="T119" id="Seg_2237" s="T118">убежать-PTCP.PST</ta>
            <ta e="T121" id="Seg_2238" s="T120">%%-ACC</ta>
            <ta e="T122" id="Seg_2239" s="T121">отнести-INF</ta>
            <ta e="T123" id="Seg_2240" s="T122">он(а).[NOM]</ta>
            <ta e="T124" id="Seg_2241" s="T123">отнести-PST-3SG.O</ta>
            <ta e="T125" id="Seg_2242" s="T124">%%-COM</ta>
            <ta e="T127" id="Seg_2243" s="T126">он(а)-DU-ACC</ta>
            <ta e="T128" id="Seg_2244" s="T127">убить-PST.NAR-3SG.O</ta>
            <ta e="T129" id="Seg_2245" s="T128">дочь-ACC-3SG</ta>
            <ta e="T130" id="Seg_2246" s="T129">NEG</ta>
            <ta e="T131" id="Seg_2247" s="T130">найти-PST.NAR-3PL</ta>
            <ta e="T132" id="Seg_2248" s="T131">он(а)-EP-ACC</ta>
            <ta e="T133" id="Seg_2249" s="T132">поймать-PST.NAR-3PL</ta>
            <ta e="T134" id="Seg_2250" s="T133">сесть-PFV-PST.NAR-3SG.O</ta>
            <ta e="T135" id="Seg_2251" s="T134">дочь-душа</ta>
            <ta e="T136" id="Seg_2252" s="T135">ребёнок.[NOM]</ta>
            <ta e="T137" id="Seg_2253" s="T136">спросить-DRV-HAB-CVB</ta>
            <ta e="T138" id="Seg_2254" s="T137">куда</ta>
            <ta e="T139" id="Seg_2255" s="T138">исчезнуть-DUR-PTCP.PST</ta>
            <ta e="T140" id="Seg_2256" s="T139">он(а)-EP.[NOM]</ta>
            <ta e="T141" id="Seg_2257" s="T140">NEG</ta>
            <ta e="T142" id="Seg_2258" s="T141">сказать-PST-3SG.O</ta>
            <ta e="T143" id="Seg_2259" s="T142">он(а).[NOM]</ta>
            <ta e="T144" id="Seg_2260" s="T143">сказать-3SG.S</ta>
            <ta e="T145" id="Seg_2261" s="T144">ловушка-ADJZ</ta>
            <ta e="T146" id="Seg_2262" s="T145">дорога-PROL</ta>
            <ta e="T147" id="Seg_2263" s="T146">уйти-PST.NAR.[3SG.S]</ta>
            <ta e="T148" id="Seg_2264" s="T147">дом-ILL</ta>
            <ta e="T149" id="Seg_2265" s="T148">он(а).[NOM]</ta>
            <ta e="T150" id="Seg_2266" s="T149">обмануть-PST.[3SG.S]</ta>
            <ta e="T151" id="Seg_2267" s="T150">он(а).[NOM]</ta>
            <ta e="T152" id="Seg_2268" s="T151">куда</ta>
            <ta e="T153" id="Seg_2269" s="T152">исчезнуть-DUR-PST.NAR-3SG.O</ta>
            <ta e="T154" id="Seg_2270" s="T153">бабушка.[NOM]</ta>
            <ta e="T155" id="Seg_2271" s="T154">остаться-3SG.S</ta>
            <ta e="T156" id="Seg_2272" s="T155">опять</ta>
            <ta e="T157" id="Seg_2273" s="T156">муж-CAR-ADVZ</ta>
            <ta e="T158" id="Seg_2274" s="T157">три-ORD-DRV</ta>
            <ta e="T159" id="Seg_2275" s="T158">муж.[NOM]-3SG</ta>
            <ta e="T160" id="Seg_2276" s="T159">тайга-LOC</ta>
            <ta e="T161" id="Seg_2277" s="T160">огонь.[NOM]</ta>
            <ta e="T162" id="Seg_2278" s="T161">съесть-EP-PST.NAR-3SG.O</ta>
            <ta e="T163" id="Seg_2279" s="T162">он(а).[NOM]</ta>
            <ta e="T164" id="Seg_2280" s="T163">жить-PST.[3SG.S]</ta>
            <ta e="T165" id="Seg_2281" s="T164">сын-GEN-3SG-COM</ta>
            <ta e="T166" id="Seg_2282" s="T165">потом</ta>
            <ta e="T167" id="Seg_2283" s="T166">этот</ta>
            <ta e="T168" id="Seg_2284" s="T167">сын.[NOM]-3SG</ta>
            <ta e="T169" id="Seg_2285" s="T168">утонуть-PST-3SG.S</ta>
            <ta e="T170" id="Seg_2286" s="T169">он(а).[NOM]</ta>
            <ta e="T171" id="Seg_2287" s="T170">быть-PST-3SG.S</ta>
            <ta e="T172" id="Seg_2288" s="T171">три</ta>
            <ta e="T173" id="Seg_2289" s="T172">десять</ta>
            <ta e="T174" id="Seg_2290" s="T173">десять-EP-ADJZ</ta>
            <ta e="T175" id="Seg_2291" s="T174">год-3SG</ta>
            <ta e="T176" id="Seg_2292" s="T175">бабушка.[NOM]-1SG</ta>
            <ta e="T177" id="Seg_2293" s="T176">жить-PST.[3SG.S]</ta>
            <ta e="T178" id="Seg_2294" s="T177">тайга-LOC</ta>
            <ta e="T179" id="Seg_2295" s="T178">ребёнок-PL-GEN-3SG-COM</ta>
            <ta e="T180" id="Seg_2296" s="T179">белка.[VBLZ]-CVB</ta>
            <ta e="T181" id="Seg_2297" s="T180">и</ta>
            <ta e="T182" id="Seg_2298" s="T181">дочь.[NOM]-3SG</ta>
            <ta e="T183" id="Seg_2299" s="T182">сам.3SG</ta>
            <ta e="T184" id="Seg_2300" s="T183">%%-ACC-3SG</ta>
            <ta e="T185" id="Seg_2301" s="T184">стрелять-PST.NAR-3SG.O</ta>
            <ta e="T186" id="Seg_2302" s="T185">собака-ACC-3SG</ta>
            <ta e="T187" id="Seg_2303" s="T186">ударить-IPFV3-CVB</ta>
            <ta e="T188" id="Seg_2304" s="T187">дочь.[NOM]-3SG</ta>
            <ta e="T189" id="Seg_2305" s="T188">впереди</ta>
            <ta e="T190" id="Seg_2306" s="T189">уйти-PST.[3SG.S]</ta>
            <ta e="T191" id="Seg_2307" s="T190">потом</ta>
            <ta e="T192" id="Seg_2308" s="T191">бабушка.[NOM]-1SG</ta>
            <ta e="T193" id="Seg_2309" s="T192">уйти-PST.[3SG.S]</ta>
            <ta e="T194" id="Seg_2310" s="T193">бабушка.[NOM]-1SG</ta>
            <ta e="T195" id="Seg_2311" s="T194">прийти-PST-3SG.S</ta>
            <ta e="T196" id="Seg_2312" s="T195">он(а).[NOM]</ta>
            <ta e="T197" id="Seg_2313" s="T196">уж</ta>
            <ta e="T198" id="Seg_2314" s="T197">умереть-PST.NAR.[3SG.S]</ta>
            <ta e="T199" id="Seg_2315" s="T198">бабушка.[NOM]-1SG</ta>
            <ta e="T200" id="Seg_2316" s="T199">тосковать-HAB-PST.[3SG.S]</ta>
            <ta e="T201" id="Seg_2317" s="T200">нарты-INSTR</ta>
            <ta e="T202" id="Seg_2318" s="T201">дом-ILL</ta>
            <ta e="T203" id="Seg_2319" s="T202">привезти-PST-3PL</ta>
            <ta e="T204" id="Seg_2320" s="T203">долго</ta>
            <ta e="T205" id="Seg_2321" s="T204">дом-LOC</ta>
            <ta e="T206" id="Seg_2322" s="T205">лежать-PST.[3SG.S]</ta>
            <ta e="T207" id="Seg_2323" s="T206">прийти-PST-3SG.S</ta>
            <ta e="T208" id="Seg_2324" s="T207">врач</ta>
            <ta e="T209" id="Seg_2325" s="T208">смотреть-PST-3SG.O</ta>
            <ta e="T210" id="Seg_2326" s="T209">и</ta>
            <ta e="T211" id="Seg_2327" s="T210">сказать-PST.[3SG.S]</ta>
            <ta e="T212" id="Seg_2328" s="T211">похоронить-INF</ta>
            <ta e="T213" id="Seg_2329" s="T212">похоронить-HAB-PST-3PL</ta>
            <ta e="T214" id="Seg_2330" s="T213">вперёд</ta>
            <ta e="T215" id="Seg_2331" s="T214">жить-CVB</ta>
            <ta e="T216" id="Seg_2332" s="T215">начать-PST-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_2333" s="T1">pers</ta>
            <ta e="T3" id="Seg_2334" s="T2">n-n:poss-n:case</ta>
            <ta e="T4" id="Seg_2335" s="T3">num</ta>
            <ta e="T5" id="Seg_2336" s="T4">n.[n:case]-n:poss</ta>
            <ta e="T6" id="Seg_2337" s="T5">adj</ta>
            <ta e="T7" id="Seg_2338" s="T6">v-v&gt;n</ta>
            <ta e="T8" id="Seg_2339" s="T7">n-adj&gt;adv</ta>
            <ta e="T9" id="Seg_2340" s="T8">v.[v:pn]</ta>
            <ta e="T10" id="Seg_2341" s="T9">n-adj&gt;adv</ta>
            <ta e="T11" id="Seg_2342" s="T10">v.[v:pn]</ta>
            <ta e="T12" id="Seg_2343" s="T11">v-v:tense.[v:pn]</ta>
            <ta e="T13" id="Seg_2344" s="T12">num</ta>
            <ta e="T14" id="Seg_2345" s="T13">n-adv:case</ta>
            <ta e="T15" id="Seg_2346" s="T14">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_2347" s="T15">n.[n:case]</ta>
            <ta e="T17" id="Seg_2348" s="T16">v-v:tense.[v:pn]</ta>
            <ta e="T18" id="Seg_2349" s="T17">adv</ta>
            <ta e="T19" id="Seg_2350" s="T18">num</ta>
            <ta e="T20" id="Seg_2351" s="T19">v-v:tense.[v:pn]</ta>
            <ta e="T21" id="Seg_2352" s="T20">n-v&gt;n</ta>
            <ta e="T22" id="Seg_2353" s="T21">v-v:pn</ta>
            <ta e="T23" id="Seg_2354" s="T22">num</ta>
            <ta e="T24" id="Seg_2355" s="T23">n-adv:case</ta>
            <ta e="T25" id="Seg_2356" s="T24">v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_2357" s="T25">n-n:num.[n:case]</ta>
            <ta e="T27" id="Seg_2358" s="T26">quant</ta>
            <ta e="T28" id="Seg_2359" s="T27">v-v:tense.[v:pn]</ta>
            <ta e="T29" id="Seg_2360" s="T28">interrog</ta>
            <ta e="T30" id="Seg_2361" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_2362" s="T30">adj-n&gt;n-n:num.[n:case]</ta>
            <ta e="T32" id="Seg_2363" s="T31">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_2364" s="T32">n.[n:case]-n:poss</ta>
            <ta e="T34" id="Seg_2365" s="T33">conj</ta>
            <ta e="T35" id="Seg_2366" s="T34">n.[n:case]-n:poss</ta>
            <ta e="T36" id="Seg_2367" s="T35">n-n&gt;v-v:pn-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_2368" s="T36">n.[n:case]-n:poss</ta>
            <ta e="T38" id="Seg_2369" s="T37">n-n:ins-n:case</ta>
            <ta e="T39" id="Seg_2370" s="T38">v-v&gt;adv</ta>
            <ta e="T40" id="Seg_2371" s="T39">v-v&gt;v-n:ins-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T41" id="Seg_2372" s="T40">v-v&gt;v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T42" id="Seg_2373" s="T41">v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_2374" s="T42">v-v&gt;ptcp.[v:pn]</ta>
            <ta e="T44" id="Seg_2375" s="T43">pers.[n:case]</ta>
            <ta e="T45" id="Seg_2376" s="T44">v-v:tense.[v:pn]</ta>
            <ta e="T46" id="Seg_2377" s="T45">n-n&gt;adj</ta>
            <ta e="T47" id="Seg_2378" s="T46">n-n:ins-n:case-n:case</ta>
            <ta e="T48" id="Seg_2379" s="T47">pers-n:ins-n:case-n:case</ta>
            <ta e="T49" id="Seg_2380" s="T48">adj</ta>
            <ta e="T50" id="Seg_2381" s="T49">n.[n:case]-n:poss</ta>
            <ta e="T51" id="Seg_2382" s="T50">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_2383" s="T51">n.[n:case]-n:poss</ta>
            <ta e="T53" id="Seg_2384" s="T52">pers-n:ins-n:case</ta>
            <ta e="T54" id="Seg_2385" s="T53">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_2386" s="T54">conj</ta>
            <ta e="T56" id="Seg_2387" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_2388" s="T56">pers-n:num-n:case-n:case</ta>
            <ta e="T58" id="Seg_2389" s="T57">num</ta>
            <ta e="T59" id="Seg_2390" s="T58">n-n-n:ins-n&gt;adj</ta>
            <ta e="T60" id="Seg_2391" s="T59">n.[n:case]-n:poss</ta>
            <ta e="T61" id="Seg_2392" s="T60">emphpro</ta>
            <ta e="T63" id="Seg_2393" s="T62">v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_2394" s="T63">n-n:case-n:case</ta>
            <ta e="T65" id="Seg_2395" s="T64">quant</ta>
            <ta e="T66" id="Seg_2396" s="T65">n-n:num.[n:case]-n:poss</ta>
            <ta e="T67" id="Seg_2397" s="T66">adj</ta>
            <ta e="T68" id="Seg_2398" s="T67">n-n:poss-n:case</ta>
            <ta e="T69" id="Seg_2399" s="T68">v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_2400" s="T69">num</ta>
            <ta e="T71" id="Seg_2401" s="T70">n.[n:case]-n:poss</ta>
            <ta e="T72" id="Seg_2402" s="T71">n-n:case.poss</ta>
            <ta e="T73" id="Seg_2403" s="T72">v-v&gt;adv</ta>
            <ta e="T74" id="Seg_2404" s="T73">adv</ta>
            <ta e="T75" id="Seg_2405" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_2406" s="T75">adj</ta>
            <ta e="T77" id="Seg_2407" s="T76">n.[n:case]-n:poss</ta>
            <ta e="T78" id="Seg_2408" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_2409" s="T78">v-v&gt;adv</ta>
            <ta e="T80" id="Seg_2410" s="T79">v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_2411" s="T80">adv</ta>
            <ta e="T82" id="Seg_2412" s="T81">emphpro</ta>
            <ta e="T83" id="Seg_2413" s="T82">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T84" id="Seg_2414" s="T83">adv</ta>
            <ta e="T85" id="Seg_2415" s="T84">adv</ta>
            <ta e="T86" id="Seg_2416" s="T85">v-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_2417" s="T86">adj</ta>
            <ta e="T88" id="Seg_2418" s="T87">n-n:case-n:case</ta>
            <ta e="T89" id="Seg_2419" s="T88">n-n:ins-n:case-n:case</ta>
            <ta e="T90" id="Seg_2420" s="T89">pers.[n:case]</ta>
            <ta e="T91" id="Seg_2421" s="T90">v-v:tense-v:pn</ta>
            <ta e="T92" id="Seg_2422" s="T91">n-n&gt;v-v&gt;ptcp</ta>
            <ta e="T93" id="Seg_2423" s="T92">pers-n:ins-n:case-n:case</ta>
            <ta e="T94" id="Seg_2424" s="T93">adj</ta>
            <ta e="T95" id="Seg_2425" s="T94">n-n-n:case</ta>
            <ta e="T96" id="Seg_2426" s="T95">num</ta>
            <ta e="T97" id="Seg_2427" s="T96">n-n:poss</ta>
            <ta e="T98" id="Seg_2428" s="T97">adv</ta>
            <ta e="T99" id="Seg_2429" s="T98">emphpro</ta>
            <ta e="T100" id="Seg_2430" s="T99">n-n:num.[n:case]-n:poss</ta>
            <ta e="T101" id="Seg_2431" s="T100">v-v:tense.[v:pn]</ta>
            <ta e="T102" id="Seg_2432" s="T101">interrog</ta>
            <ta e="T104" id="Seg_2433" s="T103">v-v:tense-v:pn</ta>
            <ta e="T105" id="Seg_2434" s="T104">pers.[n:case]</ta>
            <ta e="T106" id="Seg_2435" s="T105">v-v:tense-v:pn</ta>
            <ta e="T107" id="Seg_2436" s="T106">num-n:num.[n:case]</ta>
            <ta e="T108" id="Seg_2437" s="T107">v-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_2438" s="T108">num</ta>
            <ta e="T110" id="Seg_2439" s="T109">v-v:tense-v:pn</ta>
            <ta e="T111" id="Seg_2440" s="T110">conj</ta>
            <ta e="T112" id="Seg_2441" s="T111">n.[n:case]</ta>
            <ta e="T113" id="Seg_2442" s="T112">v-v:ins-v:pn</ta>
            <ta e="T114" id="Seg_2443" s="T113">num</ta>
            <ta e="T116" id="Seg_2444" s="T115">v-v:pn</ta>
            <ta e="T117" id="Seg_2445" s="T116">n-n-n:ins-n:case-n:case</ta>
            <ta e="T118" id="Seg_2446" s="T117">v-v:tense-v:pn</ta>
            <ta e="T119" id="Seg_2447" s="T118">v-v&gt;ptcp</ta>
            <ta e="T121" id="Seg_2448" s="T120">n-n:case</ta>
            <ta e="T122" id="Seg_2449" s="T121">v-v:inf</ta>
            <ta e="T123" id="Seg_2450" s="T122">pers.[n:case]</ta>
            <ta e="T124" id="Seg_2451" s="T123">v-v:tense-v:pn</ta>
            <ta e="T125" id="Seg_2452" s="T124">n-n:case</ta>
            <ta e="T127" id="Seg_2453" s="T126">pers-n:num-n:case</ta>
            <ta e="T128" id="Seg_2454" s="T127">v-v:tense-v:pn</ta>
            <ta e="T129" id="Seg_2455" s="T128">n-n:case-n:poss</ta>
            <ta e="T130" id="Seg_2456" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_2457" s="T130">v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_2458" s="T131">pers-n:ins-n:case</ta>
            <ta e="T133" id="Seg_2459" s="T132">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_2460" s="T133">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T135" id="Seg_2461" s="T134">n-n</ta>
            <ta e="T136" id="Seg_2462" s="T135">n.[n:case]</ta>
            <ta e="T137" id="Seg_2463" s="T136">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T138" id="Seg_2464" s="T137">interrog</ta>
            <ta e="T139" id="Seg_2465" s="T138">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T140" id="Seg_2466" s="T139">pers-n:ins.[n:case]</ta>
            <ta e="T141" id="Seg_2467" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_2468" s="T141">v-v:tense-v:pn</ta>
            <ta e="T143" id="Seg_2469" s="T142">pers.[n:case]</ta>
            <ta e="T144" id="Seg_2470" s="T143">v-v:pn</ta>
            <ta e="T145" id="Seg_2471" s="T144">n-n&gt;adj</ta>
            <ta e="T146" id="Seg_2472" s="T145">n-n:case</ta>
            <ta e="T147" id="Seg_2473" s="T146">v-v:tense.[v:pn]</ta>
            <ta e="T148" id="Seg_2474" s="T147">n-n:case</ta>
            <ta e="T149" id="Seg_2475" s="T148">pers.[n:case]</ta>
            <ta e="T150" id="Seg_2476" s="T149">v-v:tense.[v:pn]</ta>
            <ta e="T151" id="Seg_2477" s="T150">pers.[n:case]</ta>
            <ta e="T152" id="Seg_2478" s="T151">interrog</ta>
            <ta e="T153" id="Seg_2479" s="T152">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T154" id="Seg_2480" s="T153">n.[n:case]</ta>
            <ta e="T155" id="Seg_2481" s="T154">v-v:pn</ta>
            <ta e="T156" id="Seg_2482" s="T155">adv</ta>
            <ta e="T157" id="Seg_2483" s="T156">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T158" id="Seg_2484" s="T157">num-num&gt;adj-adj&gt;adj</ta>
            <ta e="T159" id="Seg_2485" s="T158">n.[n:case]-n:poss</ta>
            <ta e="T160" id="Seg_2486" s="T159">n-n:case</ta>
            <ta e="T161" id="Seg_2487" s="T160">n.[n:case]</ta>
            <ta e="T162" id="Seg_2488" s="T161">v-n:ins-v:tense-v:pn</ta>
            <ta e="T163" id="Seg_2489" s="T162">pers.[n:case]</ta>
            <ta e="T164" id="Seg_2490" s="T163">v-v:tense.[v:pn]</ta>
            <ta e="T165" id="Seg_2491" s="T164">n-n:case-n:poss-n:case</ta>
            <ta e="T166" id="Seg_2492" s="T165">adv</ta>
            <ta e="T167" id="Seg_2493" s="T166">dem</ta>
            <ta e="T168" id="Seg_2494" s="T167">n.[n:case]-n:poss</ta>
            <ta e="T169" id="Seg_2495" s="T168">v-v:tense-v:pn</ta>
            <ta e="T170" id="Seg_2496" s="T169">pers.[n:case]</ta>
            <ta e="T171" id="Seg_2497" s="T170">v-v:tense-v:pn</ta>
            <ta e="T172" id="Seg_2498" s="T171">num</ta>
            <ta e="T173" id="Seg_2499" s="T172">num</ta>
            <ta e="T174" id="Seg_2500" s="T173">num-n:ins-n&gt;adj</ta>
            <ta e="T175" id="Seg_2501" s="T174">n-n:poss</ta>
            <ta e="T176" id="Seg_2502" s="T175">n.[n:case]-n:poss</ta>
            <ta e="T177" id="Seg_2503" s="T176">v-v:tense.[v:pn]</ta>
            <ta e="T178" id="Seg_2504" s="T177">n-n:case</ta>
            <ta e="T179" id="Seg_2505" s="T178">n-n:num-n:case-n:poss-n:case</ta>
            <ta e="T180" id="Seg_2506" s="T179">n.[n&gt;v]-v&gt;adv</ta>
            <ta e="T181" id="Seg_2507" s="T180">conj</ta>
            <ta e="T182" id="Seg_2508" s="T181">n.[n:case]-n:poss</ta>
            <ta e="T183" id="Seg_2509" s="T182">emphpro</ta>
            <ta e="T184" id="Seg_2510" s="T183">n-n:case-n:poss</ta>
            <ta e="T185" id="Seg_2511" s="T184">v-v:tense-v:pn</ta>
            <ta e="T186" id="Seg_2512" s="T185">n-n:case-n:poss</ta>
            <ta e="T187" id="Seg_2513" s="T186">v-v&gt;v-v&gt;adv</ta>
            <ta e="T188" id="Seg_2514" s="T187">n.[n:case]-n:poss</ta>
            <ta e="T189" id="Seg_2515" s="T188">adv</ta>
            <ta e="T190" id="Seg_2516" s="T189">v-v:tense.[v:pn]</ta>
            <ta e="T191" id="Seg_2517" s="T190">adv</ta>
            <ta e="T192" id="Seg_2518" s="T191">n.[n:case]-n:poss</ta>
            <ta e="T193" id="Seg_2519" s="T192">v-v:tense.[v:pn]</ta>
            <ta e="T194" id="Seg_2520" s="T193">n.[n:case]-n:poss</ta>
            <ta e="T195" id="Seg_2521" s="T194">v-v:tense-v:pn</ta>
            <ta e="T196" id="Seg_2522" s="T195">pers.[n:case]</ta>
            <ta e="T197" id="Seg_2523" s="T196">ptcl</ta>
            <ta e="T198" id="Seg_2524" s="T197">v-v:tense.[v:pn]</ta>
            <ta e="T199" id="Seg_2525" s="T198">n.[n:case]-n:poss</ta>
            <ta e="T200" id="Seg_2526" s="T199">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T201" id="Seg_2527" s="T200">n-n:case</ta>
            <ta e="T202" id="Seg_2528" s="T201">n-n:case</ta>
            <ta e="T203" id="Seg_2529" s="T202">v-v:tense-v:pn</ta>
            <ta e="T204" id="Seg_2530" s="T203">adv</ta>
            <ta e="T205" id="Seg_2531" s="T204">n-n:case</ta>
            <ta e="T206" id="Seg_2532" s="T205">v-v:tense.[v:pn]</ta>
            <ta e="T207" id="Seg_2533" s="T206">v-v:tense-v:pn</ta>
            <ta e="T208" id="Seg_2534" s="T207">n</ta>
            <ta e="T209" id="Seg_2535" s="T208">v-v:tense-v:pn</ta>
            <ta e="T210" id="Seg_2536" s="T209">conj</ta>
            <ta e="T211" id="Seg_2537" s="T210">v-v:tense.[v:pn]</ta>
            <ta e="T212" id="Seg_2538" s="T211">v-v:inf</ta>
            <ta e="T213" id="Seg_2539" s="T212">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T214" id="Seg_2540" s="T213">adv</ta>
            <ta e="T215" id="Seg_2541" s="T214">v-v&gt;adv</ta>
            <ta e="T216" id="Seg_2542" s="T215">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_2543" s="T1">pers</ta>
            <ta e="T3" id="Seg_2544" s="T2">n</ta>
            <ta e="T4" id="Seg_2545" s="T3">num</ta>
            <ta e="T5" id="Seg_2546" s="T4">n</ta>
            <ta e="T6" id="Seg_2547" s="T5">adj</ta>
            <ta e="T7" id="Seg_2548" s="T6">v</ta>
            <ta e="T8" id="Seg_2549" s="T7">adv</ta>
            <ta e="T9" id="Seg_2550" s="T8">v</ta>
            <ta e="T10" id="Seg_2551" s="T9">adv</ta>
            <ta e="T11" id="Seg_2552" s="T10">v</ta>
            <ta e="T12" id="Seg_2553" s="T11">v</ta>
            <ta e="T13" id="Seg_2554" s="T12">num</ta>
            <ta e="T14" id="Seg_2555" s="T13">n</ta>
            <ta e="T15" id="Seg_2556" s="T14">v</ta>
            <ta e="T16" id="Seg_2557" s="T15">n</ta>
            <ta e="T17" id="Seg_2558" s="T16">v</ta>
            <ta e="T18" id="Seg_2559" s="T17">adv</ta>
            <ta e="T19" id="Seg_2560" s="T18">num</ta>
            <ta e="T20" id="Seg_2561" s="T19">v</ta>
            <ta e="T22" id="Seg_2562" s="T21">v</ta>
            <ta e="T23" id="Seg_2563" s="T22">num</ta>
            <ta e="T24" id="Seg_2564" s="T23">n</ta>
            <ta e="T25" id="Seg_2565" s="T24">v</ta>
            <ta e="T26" id="Seg_2566" s="T25">n</ta>
            <ta e="T27" id="Seg_2567" s="T26">quant</ta>
            <ta e="T28" id="Seg_2568" s="T27">v</ta>
            <ta e="T29" id="Seg_2569" s="T28">interrog</ta>
            <ta e="T30" id="Seg_2570" s="T29">v</ta>
            <ta e="T31" id="Seg_2571" s="T30">n</ta>
            <ta e="T32" id="Seg_2572" s="T31">v</ta>
            <ta e="T33" id="Seg_2573" s="T32">n</ta>
            <ta e="T34" id="Seg_2574" s="T33">conj</ta>
            <ta e="T35" id="Seg_2575" s="T34">n</ta>
            <ta e="T36" id="Seg_2576" s="T35">v</ta>
            <ta e="T37" id="Seg_2577" s="T36">n</ta>
            <ta e="T38" id="Seg_2578" s="T37">n</ta>
            <ta e="T39" id="Seg_2579" s="T38">adv</ta>
            <ta e="T41" id="Seg_2580" s="T40">v</ta>
            <ta e="T42" id="Seg_2581" s="T41">v</ta>
            <ta e="T43" id="Seg_2582" s="T42">ptcp</ta>
            <ta e="T44" id="Seg_2583" s="T43">pers</ta>
            <ta e="T45" id="Seg_2584" s="T44">v</ta>
            <ta e="T46" id="Seg_2585" s="T45">adj</ta>
            <ta e="T47" id="Seg_2586" s="T46">n</ta>
            <ta e="T48" id="Seg_2587" s="T47">n</ta>
            <ta e="T49" id="Seg_2588" s="T48">adj</ta>
            <ta e="T50" id="Seg_2589" s="T49">n</ta>
            <ta e="T51" id="Seg_2590" s="T50">v</ta>
            <ta e="T52" id="Seg_2591" s="T51">n</ta>
            <ta e="T53" id="Seg_2592" s="T52">pers</ta>
            <ta e="T54" id="Seg_2593" s="T53">v</ta>
            <ta e="T55" id="Seg_2594" s="T54">conj</ta>
            <ta e="T56" id="Seg_2595" s="T55">v</ta>
            <ta e="T57" id="Seg_2596" s="T56">pers</ta>
            <ta e="T58" id="Seg_2597" s="T57">num</ta>
            <ta e="T59" id="Seg_2598" s="T58">n</ta>
            <ta e="T60" id="Seg_2599" s="T59">n</ta>
            <ta e="T61" id="Seg_2600" s="T60">emphpro</ta>
            <ta e="T63" id="Seg_2601" s="T62">n</ta>
            <ta e="T64" id="Seg_2602" s="T63">n</ta>
            <ta e="T65" id="Seg_2603" s="T64">quant</ta>
            <ta e="T66" id="Seg_2604" s="T65">n</ta>
            <ta e="T67" id="Seg_2605" s="T66">adj</ta>
            <ta e="T68" id="Seg_2606" s="T67">n</ta>
            <ta e="T69" id="Seg_2607" s="T68">n</ta>
            <ta e="T70" id="Seg_2608" s="T69">num</ta>
            <ta e="T71" id="Seg_2609" s="T70">v</ta>
            <ta e="T72" id="Seg_2610" s="T71">n</ta>
            <ta e="T73" id="Seg_2611" s="T72">adv</ta>
            <ta e="T74" id="Seg_2612" s="T73">adv</ta>
            <ta e="T75" id="Seg_2613" s="T74">v</ta>
            <ta e="T76" id="Seg_2614" s="T75">adj</ta>
            <ta e="T77" id="Seg_2615" s="T76">n</ta>
            <ta e="T78" id="Seg_2616" s="T77">n</ta>
            <ta e="T79" id="Seg_2617" s="T78">adv</ta>
            <ta e="T80" id="Seg_2618" s="T79">v</ta>
            <ta e="T81" id="Seg_2619" s="T80">adv</ta>
            <ta e="T82" id="Seg_2620" s="T81">emphpro</ta>
            <ta e="T83" id="Seg_2621" s="T82">v</ta>
            <ta e="T84" id="Seg_2622" s="T83">adv</ta>
            <ta e="T85" id="Seg_2623" s="T84">adv</ta>
            <ta e="T86" id="Seg_2624" s="T85">v</ta>
            <ta e="T87" id="Seg_2625" s="T86">adj</ta>
            <ta e="T88" id="Seg_2626" s="T87">n</ta>
            <ta e="T89" id="Seg_2627" s="T88">n</ta>
            <ta e="T90" id="Seg_2628" s="T89">pers</ta>
            <ta e="T91" id="Seg_2629" s="T90">v</ta>
            <ta e="T92" id="Seg_2630" s="T91">adj</ta>
            <ta e="T93" id="Seg_2631" s="T92">pers</ta>
            <ta e="T94" id="Seg_2632" s="T93">adj</ta>
            <ta e="T95" id="Seg_2633" s="T94">n</ta>
            <ta e="T96" id="Seg_2634" s="T95">num</ta>
            <ta e="T97" id="Seg_2635" s="T96">n</ta>
            <ta e="T98" id="Seg_2636" s="T97">adv</ta>
            <ta e="T99" id="Seg_2637" s="T98">emphpro</ta>
            <ta e="T100" id="Seg_2638" s="T99">n</ta>
            <ta e="T101" id="Seg_2639" s="T100">v</ta>
            <ta e="T102" id="Seg_2640" s="T101">interrog</ta>
            <ta e="T104" id="Seg_2641" s="T103">v</ta>
            <ta e="T105" id="Seg_2642" s="T104">pers</ta>
            <ta e="T106" id="Seg_2643" s="T105">v</ta>
            <ta e="T107" id="Seg_2644" s="T106">num</ta>
            <ta e="T108" id="Seg_2645" s="T107">v</ta>
            <ta e="T109" id="Seg_2646" s="T108">num</ta>
            <ta e="T110" id="Seg_2647" s="T109">v</ta>
            <ta e="T111" id="Seg_2648" s="T110">conj</ta>
            <ta e="T112" id="Seg_2649" s="T111">n</ta>
            <ta e="T113" id="Seg_2650" s="T112">v</ta>
            <ta e="T114" id="Seg_2651" s="T113">num</ta>
            <ta e="T116" id="Seg_2652" s="T115">v</ta>
            <ta e="T117" id="Seg_2653" s="T116">n</ta>
            <ta e="T118" id="Seg_2654" s="T117">v</ta>
            <ta e="T119" id="Seg_2655" s="T118">adj</ta>
            <ta e="T121" id="Seg_2656" s="T120">n</ta>
            <ta e="T122" id="Seg_2657" s="T121">v</ta>
            <ta e="T123" id="Seg_2658" s="T122">pers</ta>
            <ta e="T124" id="Seg_2659" s="T123">v</ta>
            <ta e="T125" id="Seg_2660" s="T124">n</ta>
            <ta e="T126" id="Seg_2661" s="T125">n</ta>
            <ta e="T127" id="Seg_2662" s="T126">pers</ta>
            <ta e="T128" id="Seg_2663" s="T127">v</ta>
            <ta e="T129" id="Seg_2664" s="T128">n</ta>
            <ta e="T130" id="Seg_2665" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_2666" s="T130">v</ta>
            <ta e="T132" id="Seg_2667" s="T131">pers</ta>
            <ta e="T133" id="Seg_2668" s="T132">v</ta>
            <ta e="T134" id="Seg_2669" s="T133">v</ta>
            <ta e="T136" id="Seg_2670" s="T135">n</ta>
            <ta e="T137" id="Seg_2671" s="T136">adv</ta>
            <ta e="T138" id="Seg_2672" s="T137">interrog</ta>
            <ta e="T139" id="Seg_2673" s="T138">v</ta>
            <ta e="T140" id="Seg_2674" s="T139">pers</ta>
            <ta e="T141" id="Seg_2675" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_2676" s="T141">v</ta>
            <ta e="T143" id="Seg_2677" s="T142">pers</ta>
            <ta e="T144" id="Seg_2678" s="T143">v</ta>
            <ta e="T145" id="Seg_2679" s="T144">adj</ta>
            <ta e="T146" id="Seg_2680" s="T145">n</ta>
            <ta e="T147" id="Seg_2681" s="T146">v</ta>
            <ta e="T148" id="Seg_2682" s="T147">n</ta>
            <ta e="T149" id="Seg_2683" s="T148">pers</ta>
            <ta e="T150" id="Seg_2684" s="T149">v</ta>
            <ta e="T151" id="Seg_2685" s="T150">pers</ta>
            <ta e="T152" id="Seg_2686" s="T151">interrog</ta>
            <ta e="T153" id="Seg_2687" s="T152">v</ta>
            <ta e="T154" id="Seg_2688" s="T153">n</ta>
            <ta e="T155" id="Seg_2689" s="T154">v</ta>
            <ta e="T156" id="Seg_2690" s="T155">adv</ta>
            <ta e="T157" id="Seg_2691" s="T156">adj</ta>
            <ta e="T158" id="Seg_2692" s="T157">adj</ta>
            <ta e="T159" id="Seg_2693" s="T158">n</ta>
            <ta e="T160" id="Seg_2694" s="T159">n</ta>
            <ta e="T161" id="Seg_2695" s="T160">n</ta>
            <ta e="T162" id="Seg_2696" s="T161">v</ta>
            <ta e="T163" id="Seg_2697" s="T162">pers</ta>
            <ta e="T164" id="Seg_2698" s="T163">v</ta>
            <ta e="T165" id="Seg_2699" s="T164">n</ta>
            <ta e="T166" id="Seg_2700" s="T165">adv</ta>
            <ta e="T167" id="Seg_2701" s="T166">dem</ta>
            <ta e="T168" id="Seg_2702" s="T167">n</ta>
            <ta e="T169" id="Seg_2703" s="T168">v</ta>
            <ta e="T170" id="Seg_2704" s="T169">pers</ta>
            <ta e="T171" id="Seg_2705" s="T170">v</ta>
            <ta e="T172" id="Seg_2706" s="T171">num</ta>
            <ta e="T173" id="Seg_2707" s="T172">num</ta>
            <ta e="T174" id="Seg_2708" s="T173">adj</ta>
            <ta e="T175" id="Seg_2709" s="T174">n</ta>
            <ta e="T176" id="Seg_2710" s="T175">n</ta>
            <ta e="T177" id="Seg_2711" s="T176">v</ta>
            <ta e="T178" id="Seg_2712" s="T177">n</ta>
            <ta e="T179" id="Seg_2713" s="T178">n</ta>
            <ta e="T180" id="Seg_2714" s="T179">adv</ta>
            <ta e="T181" id="Seg_2715" s="T180">conj</ta>
            <ta e="T182" id="Seg_2716" s="T181">n</ta>
            <ta e="T183" id="Seg_2717" s="T182">emphpro</ta>
            <ta e="T184" id="Seg_2718" s="T183">n</ta>
            <ta e="T185" id="Seg_2719" s="T184">v</ta>
            <ta e="T186" id="Seg_2720" s="T185">n</ta>
            <ta e="T187" id="Seg_2721" s="T186">adv</ta>
            <ta e="T188" id="Seg_2722" s="T187">n</ta>
            <ta e="T189" id="Seg_2723" s="T188">adv</ta>
            <ta e="T190" id="Seg_2724" s="T189">v</ta>
            <ta e="T191" id="Seg_2725" s="T190">adv</ta>
            <ta e="T192" id="Seg_2726" s="T191">n</ta>
            <ta e="T193" id="Seg_2727" s="T192">v</ta>
            <ta e="T194" id="Seg_2728" s="T193">n</ta>
            <ta e="T195" id="Seg_2729" s="T194">v</ta>
            <ta e="T196" id="Seg_2730" s="T195">pers</ta>
            <ta e="T197" id="Seg_2731" s="T196">adv</ta>
            <ta e="T198" id="Seg_2732" s="T197">v</ta>
            <ta e="T199" id="Seg_2733" s="T198">n</ta>
            <ta e="T200" id="Seg_2734" s="T199">v</ta>
            <ta e="T201" id="Seg_2735" s="T200">n</ta>
            <ta e="T202" id="Seg_2736" s="T201">n</ta>
            <ta e="T203" id="Seg_2737" s="T202">v</ta>
            <ta e="T204" id="Seg_2738" s="T203">adv</ta>
            <ta e="T205" id="Seg_2739" s="T204">n</ta>
            <ta e="T206" id="Seg_2740" s="T205">v</ta>
            <ta e="T207" id="Seg_2741" s="T206">v</ta>
            <ta e="T208" id="Seg_2742" s="T207">n</ta>
            <ta e="T209" id="Seg_2743" s="T208">v</ta>
            <ta e="T210" id="Seg_2744" s="T209">conj</ta>
            <ta e="T211" id="Seg_2745" s="T210">v</ta>
            <ta e="T212" id="Seg_2746" s="T211">v</ta>
            <ta e="T213" id="Seg_2747" s="T212">v</ta>
            <ta e="T214" id="Seg_2748" s="T213">adv</ta>
            <ta e="T215" id="Seg_2749" s="T214">preverb</ta>
            <ta e="T216" id="Seg_2750" s="T215">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_2751" s="T1">pro.h:Poss</ta>
            <ta e="T3" id="Seg_2752" s="T2">np.h:Poss </ta>
            <ta e="T5" id="Seg_2753" s="T4">np:Th</ta>
            <ta e="T7" id="Seg_2754" s="T6">0.3.h:Th</ta>
            <ta e="T9" id="Seg_2755" s="T8">0.3.h:Th</ta>
            <ta e="T12" id="Seg_2756" s="T11">0.3.h:A</ta>
            <ta e="T14" id="Seg_2757" s="T13">adv:Time</ta>
            <ta e="T15" id="Seg_2758" s="T14">0.3.h:Th</ta>
            <ta e="T16" id="Seg_2759" s="T15">np.h:Th</ta>
            <ta e="T21" id="Seg_2760" s="T20">np.h:Th</ta>
            <ta e="T24" id="Seg_2761" s="T23">adv:Time</ta>
            <ta e="T25" id="Seg_2762" s="T24">0.3.h:Th</ta>
            <ta e="T26" id="Seg_2763" s="T25">np.h:Th</ta>
            <ta e="T30" id="Seg_2764" s="T29">0.3.h:P</ta>
            <ta e="T31" id="Seg_2765" s="T30">np.h:P</ta>
            <ta e="T33" id="Seg_2766" s="T32">np.h:A 0.1.h:Poss</ta>
            <ta e="T35" id="Seg_2767" s="T34">np.h:A 0.1.h:Poss</ta>
            <ta e="T37" id="Seg_2768" s="T36">np.h:A 0.3.h:Poss</ta>
            <ta e="T39" id="Seg_2769" s="T38">0.3.h:P</ta>
            <ta e="T41" id="Seg_2770" s="T40">0.3.h:A</ta>
            <ta e="T42" id="Seg_2771" s="T41">0.3.h:E</ta>
            <ta e="T44" id="Seg_2772" s="T43">pro.h:A</ta>
            <ta e="T47" id="Seg_2773" s="T46">np.h:Th</ta>
            <ta e="T48" id="Seg_2774" s="T47">pro.h:Poss</ta>
            <ta e="T50" id="Seg_2775" s="T49">np.h:Th</ta>
            <ta e="T52" id="Seg_2776" s="T51">np.h:A 0.1.h:Poss</ta>
            <ta e="T53" id="Seg_2777" s="T52">pro.h:P</ta>
            <ta e="T56" id="Seg_2778" s="T55">0.3.h:A 0.3.h:Th</ta>
            <ta e="T57" id="Seg_2779" s="T56">pro.h:Poss</ta>
            <ta e="T60" id="Seg_2780" s="T59">np.h:Th</ta>
            <ta e="T63" id="Seg_2781" s="T62">0.3.h:P</ta>
            <ta e="T64" id="Seg_2782" s="T63">np.h:Poss</ta>
            <ta e="T66" id="Seg_2783" s="T65">np.h:P </ta>
            <ta e="T68" id="Seg_2784" s="T67">0.3.h:Poss np.h:So</ta>
            <ta e="T71" id="Seg_2785" s="T70">np.h:P 0.3.h:Poss</ta>
            <ta e="T74" id="Seg_2786" s="T73">adv:Time</ta>
            <ta e="T77" id="Seg_2787" s="T76">np.h:P 0.3.h:Poss</ta>
            <ta e="T78" id="Seg_2788" s="T77">np:L</ta>
            <ta e="T79" id="Seg_2789" s="T78">0.3.h:P</ta>
            <ta e="T81" id="Seg_2790" s="T80">adv:Time</ta>
            <ta e="T83" id="Seg_2791" s="T82">0.3.h:Th</ta>
            <ta e="T84" id="Seg_2792" s="T83">adv:Time</ta>
            <ta e="T85" id="Seg_2793" s="T84">adv:Time</ta>
            <ta e="T86" id="Seg_2794" s="T85">0.3.h:A</ta>
            <ta e="T88" id="Seg_2795" s="T87">np.h:Th</ta>
            <ta e="T90" id="Seg_2796" s="T89">pro.h:Th</ta>
            <ta e="T93" id="Seg_2797" s="T92">pro.h:Poss</ta>
            <ta e="T95" id="Seg_2798" s="T94">np.h:So</ta>
            <ta e="T97" id="Seg_2799" s="T96">np.h:Th</ta>
            <ta e="T98" id="Seg_2800" s="T97">adv:Time</ta>
            <ta e="T99" id="Seg_2801" s="T98">pro.h:Poss</ta>
            <ta e="T100" id="Seg_2802" s="T99">np.h:Th</ta>
            <ta e="T105" id="Seg_2803" s="T104">pro.h:E</ta>
            <ta e="T107" id="Seg_2804" s="T106">np.h:P</ta>
            <ta e="T109" id="Seg_2805" s="T108">np.h:P</ta>
            <ta e="T112" id="Seg_2806" s="T111">np:L</ta>
            <ta e="T113" id="Seg_2807" s="T112">0.3.h:P</ta>
            <ta e="T114" id="Seg_2808" s="T113">np.h:Th</ta>
            <ta e="T117" id="Seg_2809" s="T116">np.h:R</ta>
            <ta e="T118" id="Seg_2810" s="T117">0.3.h:A</ta>
            <ta e="T123" id="Seg_2811" s="T122">pro.h:A</ta>
            <ta e="T127" id="Seg_2812" s="T126">pro.h:P</ta>
            <ta e="T128" id="Seg_2813" s="T127">0.3.h:A</ta>
            <ta e="T129" id="Seg_2814" s="T128">np.h:Th 0.3.h:Poss</ta>
            <ta e="T131" id="Seg_2815" s="T130">0.3.h:B</ta>
            <ta e="T132" id="Seg_2816" s="T131">pro.h:Th</ta>
            <ta e="T133" id="Seg_2817" s="T132">0.3.h:A</ta>
            <ta e="T134" id="Seg_2818" s="T133">0.3.h:A 0.3.h:Th</ta>
            <ta e="T136" id="Seg_2819" s="T135">np.h:Th</ta>
            <ta e="T137" id="Seg_2820" s="T136">0.3.h:A</ta>
            <ta e="T138" id="Seg_2821" s="T137">pro:G</ta>
            <ta e="T140" id="Seg_2822" s="T139">pro.h:Th</ta>
            <ta e="T142" id="Seg_2823" s="T141">0.3.h:A 0.3:Th</ta>
            <ta e="T143" id="Seg_2824" s="T142">pro.h:A</ta>
            <ta e="T146" id="Seg_2825" s="T145">np:Path</ta>
            <ta e="T147" id="Seg_2826" s="T146">0.3.h:A</ta>
            <ta e="T148" id="Seg_2827" s="T147">np:G</ta>
            <ta e="T149" id="Seg_2828" s="T148">pro.h:A</ta>
            <ta e="T151" id="Seg_2829" s="T150">pro.h:A</ta>
            <ta e="T153" id="Seg_2830" s="T152">0.3.h:P</ta>
            <ta e="T154" id="Seg_2831" s="T153">np.h:Th</ta>
            <ta e="T159" id="Seg_2832" s="T158">np.h:P 0.3.h:Poss</ta>
            <ta e="T160" id="Seg_2833" s="T159">np:L</ta>
            <ta e="T161" id="Seg_2834" s="T160">np:A</ta>
            <ta e="T163" id="Seg_2835" s="T162">pro.h:Th</ta>
            <ta e="T165" id="Seg_2836" s="T164">np:Com 0.3.h:Poss</ta>
            <ta e="T166" id="Seg_2837" s="T165">adv:Time</ta>
            <ta e="T168" id="Seg_2838" s="T167">np.h:P 0.3.h:Poss</ta>
            <ta e="T170" id="Seg_2839" s="T169">pro.h:Poss</ta>
            <ta e="T175" id="Seg_2840" s="T174">np:Th</ta>
            <ta e="T176" id="Seg_2841" s="T175">np.h:Th 0.1.h:Poss</ta>
            <ta e="T178" id="Seg_2842" s="T177">np:L</ta>
            <ta e="T179" id="Seg_2843" s="T178">np:Com 0.3.h:Poss</ta>
            <ta e="T182" id="Seg_2844" s="T181">np.h:A 0.3.h:Poss</ta>
            <ta e="T186" id="Seg_2845" s="T185">np:P 0.3.h:Poss</ta>
            <ta e="T187" id="Seg_2846" s="T186">0.3.h:A</ta>
            <ta e="T188" id="Seg_2847" s="T187">np.h:A 0.3.h:Poss</ta>
            <ta e="T191" id="Seg_2848" s="T190">adv:Time</ta>
            <ta e="T192" id="Seg_2849" s="T191">np.h:A 0.1.h:Poss</ta>
            <ta e="T194" id="Seg_2850" s="T193">np.h:A 0.1.h:Poss</ta>
            <ta e="T196" id="Seg_2851" s="T195">pro.h:P</ta>
            <ta e="T199" id="Seg_2852" s="T198">np.h:E 0.1.h:Poss</ta>
            <ta e="T201" id="Seg_2853" s="T200">np:Ins</ta>
            <ta e="T202" id="Seg_2854" s="T201">np:G</ta>
            <ta e="T203" id="Seg_2855" s="T202">0.3.h:A 0.3.h:Th</ta>
            <ta e="T204" id="Seg_2856" s="T203">adv:Time</ta>
            <ta e="T205" id="Seg_2857" s="T204">np:L</ta>
            <ta e="T206" id="Seg_2858" s="T205">0.3.h:Th</ta>
            <ta e="T208" id="Seg_2859" s="T207">np.h:A</ta>
            <ta e="T209" id="Seg_2860" s="T208">0.3.h:A 0.3.h:Th</ta>
            <ta e="T211" id="Seg_2861" s="T210">0.3.h:A</ta>
            <ta e="T212" id="Seg_2862" s="T211">v:Th</ta>
            <ta e="T213" id="Seg_2863" s="T212">0.3.h:A 0.3.h:P</ta>
            <ta e="T216" id="Seg_2864" s="T215">0.3.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T5" id="Seg_2865" s="T4">np:S</ta>
            <ta e="T7" id="Seg_2866" s="T6">0.3.h:S v:pred</ta>
            <ta e="T9" id="Seg_2867" s="T8">0.3.h:S v:pred</ta>
            <ta e="T12" id="Seg_2868" s="T11">0.3.h:S v:pred</ta>
            <ta e="T15" id="Seg_2869" s="T14">0.3.h:S v:pred</ta>
            <ta e="T16" id="Seg_2870" s="T15">np.h:S</ta>
            <ta e="T17" id="Seg_2871" s="T16">v:pred</ta>
            <ta e="T21" id="Seg_2872" s="T20">np.h:S</ta>
            <ta e="T22" id="Seg_2873" s="T21">v:pred</ta>
            <ta e="T25" id="Seg_2874" s="T24">0.3.h:S v:pred</ta>
            <ta e="T26" id="Seg_2875" s="T25">np.h:S</ta>
            <ta e="T28" id="Seg_2876" s="T27">v:pred</ta>
            <ta e="T30" id="Seg_2877" s="T29">0.3.h:S v:pred</ta>
            <ta e="T31" id="Seg_2878" s="T30">np.h:S</ta>
            <ta e="T32" id="Seg_2879" s="T31">v:pred</ta>
            <ta e="T33" id="Seg_2880" s="T32">np.h:S</ta>
            <ta e="T35" id="Seg_2881" s="T34">np.h:S</ta>
            <ta e="T36" id="Seg_2882" s="T35">v:pred</ta>
            <ta e="T37" id="Seg_2883" s="T36">np.h:S</ta>
            <ta e="T39" id="Seg_2884" s="T37">s:temp</ta>
            <ta e="T40" id="Seg_2885" s="T39">v:pred</ta>
            <ta e="T41" id="Seg_2886" s="T40">0.3.h:S v:pred</ta>
            <ta e="T42" id="Seg_2887" s="T41">0.3.h:S cop</ta>
            <ta e="T43" id="Seg_2888" s="T42">adj:pred</ta>
            <ta e="T44" id="Seg_2889" s="T43">pro.h:S</ta>
            <ta e="T45" id="Seg_2890" s="T44">v:pred</ta>
            <ta e="T50" id="Seg_2891" s="T49">np.h:S</ta>
            <ta e="T51" id="Seg_2892" s="T50">v:pred</ta>
            <ta e="T52" id="Seg_2893" s="T51">np.h:S</ta>
            <ta e="T53" id="Seg_2894" s="T52">pro.h:O</ta>
            <ta e="T54" id="Seg_2895" s="T53">v:pred</ta>
            <ta e="T56" id="Seg_2896" s="T55">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T60" id="Seg_2897" s="T59">np.h:S</ta>
            <ta e="T63" id="Seg_2898" s="T62">0.3.h:S v:pred</ta>
            <ta e="T66" id="Seg_2899" s="T65">np.h:S</ta>
            <ta e="T69" id="Seg_2900" s="T68">v:pred</ta>
            <ta e="T71" id="Seg_2901" s="T70">np.h:S</ta>
            <ta e="T73" id="Seg_2902" s="T71">s:temp</ta>
            <ta e="T75" id="Seg_2903" s="T74">v:pred</ta>
            <ta e="T77" id="Seg_2904" s="T76">np.h:S</ta>
            <ta e="T79" id="Seg_2905" s="T77">s:temp</ta>
            <ta e="T80" id="Seg_2906" s="T79">v:pred</ta>
            <ta e="T83" id="Seg_2907" s="T82">0.3.h:S v:pred</ta>
            <ta e="T86" id="Seg_2908" s="T85">0.3.h:S v:pred</ta>
            <ta e="T90" id="Seg_2909" s="T89">pro.h:S</ta>
            <ta e="T91" id="Seg_2910" s="T90">cop</ta>
            <ta e="T92" id="Seg_2911" s="T91">adj:pred</ta>
            <ta e="T97" id="Seg_2912" s="T96">np.h:S</ta>
            <ta e="T100" id="Seg_2913" s="T99">np.h:S</ta>
            <ta e="T101" id="Seg_2914" s="T100">v:pred</ta>
            <ta e="T104" id="Seg_2915" s="T101">s:compl</ta>
            <ta e="T105" id="Seg_2916" s="T104">pro.h:S</ta>
            <ta e="T106" id="Seg_2917" s="T105">v:pred</ta>
            <ta e="T107" id="Seg_2918" s="T106">np.h:S</ta>
            <ta e="T108" id="Seg_2919" s="T107">v:pred</ta>
            <ta e="T109" id="Seg_2920" s="T108">np.h:S</ta>
            <ta e="T110" id="Seg_2921" s="T109">v:pred</ta>
            <ta e="T113" id="Seg_2922" s="T112">0.3.h:S v:pred</ta>
            <ta e="T114" id="Seg_2923" s="T113">np.h:S</ta>
            <ta e="T116" id="Seg_2924" s="T115">v:pred</ta>
            <ta e="T118" id="Seg_2925" s="T117">0.3.h:S v:pred</ta>
            <ta e="T123" id="Seg_2926" s="T122">pro.h:S</ta>
            <ta e="T124" id="Seg_2927" s="T123">v:pred</ta>
            <ta e="T127" id="Seg_2928" s="T126">pro.h:O</ta>
            <ta e="T128" id="Seg_2929" s="T127">0.3.h:S v:pred</ta>
            <ta e="T129" id="Seg_2930" s="T128">np.h:O</ta>
            <ta e="T131" id="Seg_2931" s="T130">0.3.h:S v:pred</ta>
            <ta e="T132" id="Seg_2932" s="T131">pro.h:O</ta>
            <ta e="T133" id="Seg_2933" s="T132">0.3.h:S v:pred</ta>
            <ta e="T134" id="Seg_2934" s="T133">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T137" id="Seg_2935" s="T134">s:temp</ta>
            <ta e="T140" id="Seg_2936" s="T137">s:compl</ta>
            <ta e="T142" id="Seg_2937" s="T141">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T143" id="Seg_2938" s="T142">pro.h:S</ta>
            <ta e="T144" id="Seg_2939" s="T143">v:pred</ta>
            <ta e="T147" id="Seg_2940" s="T146">0.3.h:S v:pred</ta>
            <ta e="T149" id="Seg_2941" s="T148">pro.h:S</ta>
            <ta e="T150" id="Seg_2942" s="T149">v:pred</ta>
            <ta e="T151" id="Seg_2943" s="T150">pro.h:S</ta>
            <ta e="T153" id="Seg_2944" s="T152">v:pred 0.3.h:O</ta>
            <ta e="T154" id="Seg_2945" s="T153">np.h:S</ta>
            <ta e="T155" id="Seg_2946" s="T154">v:pred</ta>
            <ta e="T159" id="Seg_2947" s="T158">np.h:O</ta>
            <ta e="T161" id="Seg_2948" s="T160">np:S</ta>
            <ta e="T162" id="Seg_2949" s="T161">v:pred</ta>
            <ta e="T163" id="Seg_2950" s="T162">pro.h:S</ta>
            <ta e="T164" id="Seg_2951" s="T163">v:pred</ta>
            <ta e="T168" id="Seg_2952" s="T167">np.h:S</ta>
            <ta e="T169" id="Seg_2953" s="T168">v:pred</ta>
            <ta e="T171" id="Seg_2954" s="T170">v:pred</ta>
            <ta e="T175" id="Seg_2955" s="T174">np:S</ta>
            <ta e="T176" id="Seg_2956" s="T175">np.h:S</ta>
            <ta e="T177" id="Seg_2957" s="T176">v:pred</ta>
            <ta e="T180" id="Seg_2958" s="T179">s:adv</ta>
            <ta e="T182" id="Seg_2959" s="T181">np.h:S</ta>
            <ta e="T185" id="Seg_2960" s="T184">v:pred</ta>
            <ta e="T187" id="Seg_2961" s="T185">s:temp</ta>
            <ta e="T188" id="Seg_2962" s="T187">np.h:S</ta>
            <ta e="T190" id="Seg_2963" s="T189">v:pred</ta>
            <ta e="T192" id="Seg_2964" s="T191">np.h:S</ta>
            <ta e="T193" id="Seg_2965" s="T192">v:pred</ta>
            <ta e="T194" id="Seg_2966" s="T193">np.h:S</ta>
            <ta e="T195" id="Seg_2967" s="T194">v:pred</ta>
            <ta e="T196" id="Seg_2968" s="T195">pro.h:S</ta>
            <ta e="T198" id="Seg_2969" s="T197">v:pred</ta>
            <ta e="T199" id="Seg_2970" s="T198">np.h:S</ta>
            <ta e="T200" id="Seg_2971" s="T199">v:pred</ta>
            <ta e="T203" id="Seg_2972" s="T202">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T206" id="Seg_2973" s="T205">0.3.h:S v:pred</ta>
            <ta e="T207" id="Seg_2974" s="T206">v:pred</ta>
            <ta e="T208" id="Seg_2975" s="T207">np.h:S</ta>
            <ta e="T209" id="Seg_2976" s="T208">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T211" id="Seg_2977" s="T210">0.3.h:S v:pred</ta>
            <ta e="T212" id="Seg_2978" s="T211">v:O</ta>
            <ta e="T213" id="Seg_2979" s="T212">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T216" id="Seg_2980" s="T215">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T34" id="Seg_2981" s="T33">RUS:gram</ta>
            <ta e="T55" id="Seg_2982" s="T54">RUS:gram</ta>
            <ta e="T65" id="Seg_2983" s="T64">RUS:core</ta>
            <ta e="T111" id="Seg_2984" s="T110">RUS:gram</ta>
            <ta e="T152" id="Seg_2985" s="T151">RUS:core</ta>
            <ta e="T156" id="Seg_2986" s="T155">RUS:cult</ta>
            <ta e="T181" id="Seg_2987" s="T180">RUS:gram</ta>
            <ta e="T197" id="Seg_2988" s="T196">RUS:disc</ta>
            <ta e="T208" id="Seg_2989" s="T207">RUS:cult</ta>
            <ta e="T210" id="Seg_2990" s="T209">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T208" id="Seg_2991" s="T207">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_2992" s="T1">My grandmother (would be) hundred years old.</ta>
            <ta e="T7" id="Seg_2993" s="T5">She lived badly.</ta>
            <ta e="T12" id="Seg_2994" s="T7">She got married, when she was a child.</ta>
            <ta e="T17" id="Seg_2995" s="T12">They lived for three years, they had no children.</ta>
            <ta e="T22" id="Seg_2996" s="T17">Then one (child?) appeared (?).</ta>
            <ta e="T25" id="Seg_2997" s="T22">They lived (together) for three years.</ta>
            <ta e="T28" id="Seg_2998" s="T25">They had many children.</ta>
            <ta e="T30" id="Seg_2999" s="T28">How did they grow up?</ta>
            <ta e="T32" id="Seg_3000" s="T30">They died, when they were small.</ta>
            <ta e="T36" id="Seg_3001" s="T32">My grandmother and grandfather hunted making traps.</ta>
            <ta e="T41" id="Seg_3002" s="T36">Her husband, when he got drunk, scolded and scuffled.</ta>
            <ta e="T43" id="Seg_3003" s="T41">He was angry.</ta>
            <ta e="T47" id="Seg_3004" s="T43">He married a widower.</ta>
            <ta e="T51" id="Seg_3005" s="T47">He had a little child.</ta>
            <ta e="T56" id="Seg_3006" s="T51">My grandmother raised him and made him marry.</ta>
            <ta e="T60" id="Seg_3007" s="T56">They had one daughter.</ta>
            <ta e="T63" id="Seg_3008" s="T60">Both of them died.</ta>
            <ta e="T69" id="Seg_3009" s="T63">All of my gandmother's children from her first husband died.</ta>
            <ta e="T75" id="Seg_3010" s="T69">Three daughters grew up and then they died.</ta>
            <ta e="T80" id="Seg_3011" s="T75">Her first husband got ill and died.</ta>
            <ta e="T84" id="Seg_3012" s="T80">Then she lived alone for a long time.</ta>
            <ta e="T89" id="Seg_3013" s="T84">Then she married another man.</ta>
            <ta e="T92" id="Seg_3014" s="T89">He was a widower.</ta>
            <ta e="T97" id="Seg_3015" s="T92">He had two children from his first wife.</ta>
            <ta e="T101" id="Seg_3016" s="T97">Then their own children appeared.</ta>
            <ta e="T106" id="Seg_3017" s="T101">How many children (there were), she forgot.</ta>
            <ta e="T108" id="Seg_3018" s="T106">Two (children) grew up.</ta>
            <ta e="T113" id="Seg_3019" s="T108">One (child) grew up and drowned in water.</ta>
            <ta e="T116" id="Seg_3020" s="T113">One is alive.</ta>
            <ta e="T122" id="Seg_3021" s="T116">They said to the man: (?).</ta>
            <ta e="T125" id="Seg_3022" s="T122">He carried (?).</ta>
            <ta e="T128" id="Seg_3023" s="T125">(?) he killed her.</ta>
            <ta e="T131" id="Seg_3024" s="T128">The daughter was not found.</ta>
            <ta e="T134" id="Seg_3025" s="T131">He was caught and put in prison.</ta>
            <ta e="T140" id="Seg_3026" s="T134">They asked him about the girl, what he did with her.</ta>
            <ta e="T142" id="Seg_3027" s="T140">He didn't tell.</ta>
            <ta e="T148" id="Seg_3028" s="T142">He said: “She went home along the road with traps.”</ta>
            <ta e="T150" id="Seg_3029" s="T148">He was lying.</ta>
            <ta e="T153" id="Seg_3030" s="T150">He made her disappear.</ta>
            <ta e="T157" id="Seg_3031" s="T153">My grandmother was left without husband again.</ta>
            <ta e="T162" id="Seg_3032" s="T157">Her third husband burnt in the forest.</ta>
            <ta e="T165" id="Seg_3033" s="T162">She lived with her son.</ta>
            <ta e="T169" id="Seg_3034" s="T165">Then her son drowned.</ta>
            <ta e="T175" id="Seg_3035" s="T169">He was thirty years old.</ta>
            <ta e="T180" id="Seg_3036" s="T175">My grandmother used to live in the forest with her children, hunting squirrels.</ta>
            <ta e="T187" id="Seg_3037" s="T180">Her daughter shot herself as she was hitting the dog.</ta>
            <ta e="T190" id="Seg_3038" s="T187">The daughter went forward.</ta>
            <ta e="T193" id="Seg_3039" s="T190">Then the grandmother went (after her).</ta>
            <ta e="T198" id="Seg_3040" s="T193">When my grandmother had come, she was already dead.</ta>
            <ta e="T200" id="Seg_3041" s="T198">My grandmother was grieved.</ta>
            <ta e="T203" id="Seg_3042" s="T200">She was brought home on the sledges.</ta>
            <ta e="T206" id="Seg_3043" s="T203">She was lying at home for a long time.</ta>
            <ta e="T212" id="Seg_3044" s="T206">A doctor came, looked at her and told to bury her.</ta>
            <ta e="T213" id="Seg_3045" s="T212">They burried her.</ta>
            <ta e="T216" id="Seg_3046" s="T213">Then they went on living.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_3047" s="T1">Meine Großmutter (wäre) hundert Jahre alt.</ta>
            <ta e="T7" id="Seg_3048" s="T5">Sie lebte schlecht.</ta>
            <ta e="T12" id="Seg_3049" s="T7">Sie heiratete, als sie noch ein Kind war.</ta>
            <ta e="T17" id="Seg_3050" s="T12">Sie lebten drei Jahre lang, sie hatten keine Kinder.</ta>
            <ta e="T22" id="Seg_3051" s="T17">Dann erschien ein (Kind?) (?).</ta>
            <ta e="T25" id="Seg_3052" s="T22">Sie lebten drei Jahre lang.</ta>
            <ta e="T28" id="Seg_3053" s="T25">Sie bekamen viele Kinder.</ta>
            <ta e="T30" id="Seg_3054" s="T28">Wie wuchsen sie auf?</ta>
            <ta e="T32" id="Seg_3055" s="T30">Die Kleinen starben.</ta>
            <ta e="T36" id="Seg_3056" s="T32">Meine Großmutter und mein Großvater jagten mit Fallen.</ta>
            <ta e="T41" id="Seg_3057" s="T36">Wenn er betrunken war, schimpfte und schlug ihr Mann um sich.</ta>
            <ta e="T43" id="Seg_3058" s="T41">Er war wütend.</ta>
            <ta e="T47" id="Seg_3059" s="T43">Sie heiratete einen Witwer.</ta>
            <ta e="T51" id="Seg_3060" s="T47">Er hatte ein kleines Kind.</ta>
            <ta e="T56" id="Seg_3061" s="T51">Meine Großmutter zog ihn auf und verheiratete ihn.</ta>
            <ta e="T60" id="Seg_3062" s="T56">Sie hatten eine Tochter.</ta>
            <ta e="T63" id="Seg_3063" s="T60">Beide starben.</ta>
            <ta e="T69" id="Seg_3064" s="T63">Alle Kinder meiner Großmutter von ihrem ersten Mann starben.</ta>
            <ta e="T75" id="Seg_3065" s="T69">Drei Töchter wurden groß und dann starben sie.</ta>
            <ta e="T80" id="Seg_3066" s="T75">Ihr erster Mann wurde krank und starb.</ta>
            <ta e="T84" id="Seg_3067" s="T80">Dann lebte sie lange allein.</ta>
            <ta e="T89" id="Seg_3068" s="T84">Dann heiratete sie einen anderen Mann.</ta>
            <ta e="T92" id="Seg_3069" s="T89">Er war ein Witwer.</ta>
            <ta e="T97" id="Seg_3070" s="T92">Er hatte zwei Kinder von seiner ersten Frau.</ta>
            <ta e="T101" id="Seg_3071" s="T97">Dann bekamen sie eigene Kinder.</ta>
            <ta e="T106" id="Seg_3072" s="T101">Wie viele Kinder es waren, hat sie vergessen.</ta>
            <ta e="T108" id="Seg_3073" s="T106">Zwei wurden groß.</ta>
            <ta e="T113" id="Seg_3074" s="T108">Eines wurde groß und ertrank im Wasser.</ta>
            <ta e="T116" id="Seg_3075" s="T113">Eines lebt.</ta>
            <ta e="T122" id="Seg_3076" s="T116">Sie sagten zum Mann: (?).</ta>
            <ta e="T125" id="Seg_3077" s="T122">Er trug (?).</ta>
            <ta e="T128" id="Seg_3078" s="T125">(?) tötete er sie.</ta>
            <ta e="T131" id="Seg_3079" s="T128">Sie fanden die Tochter nicht.</ta>
            <ta e="T134" id="Seg_3080" s="T131">Er wurde gefangen und ins Gefängnis gesperrt.</ta>
            <ta e="T140" id="Seg_3081" s="T134">Sie fragten nach der Tochter, wohin sie verschwunden ist.</ta>
            <ta e="T142" id="Seg_3082" s="T140">Er sagte es nicht.</ta>
            <ta e="T148" id="Seg_3083" s="T142">Er sagte: "Sie ging über den Weg mit den Fallen nach Hause."</ta>
            <ta e="T150" id="Seg_3084" s="T148">Er hat gelogen.</ta>
            <ta e="T153" id="Seg_3085" s="T150">Er hat sie verschwinden lassen.</ta>
            <ta e="T157" id="Seg_3086" s="T153">Meine Großmutter blieb wieder ohne Mann.</ta>
            <ta e="T162" id="Seg_3087" s="T157">Ihr dritter Mann verbrannte im Wald.</ta>
            <ta e="T165" id="Seg_3088" s="T162">Sie lebte mit ihrem Sohn.</ta>
            <ta e="T169" id="Seg_3089" s="T165">Dann ertrank ihr Sohn.</ta>
            <ta e="T175" id="Seg_3090" s="T169">Er war dreißig Jahre alt.</ta>
            <ta e="T180" id="Seg_3091" s="T175">Meine Großmutter lebte im Wald mit ihren Kindern und jagte Eichhörnchen.</ta>
            <ta e="T187" id="Seg_3092" s="T180">Ihre Tochter schoss sich selbst an, als sie den Hund schlug.</ta>
            <ta e="T190" id="Seg_3093" s="T187">Die Tochter ging weiter.</ta>
            <ta e="T193" id="Seg_3094" s="T190">Dann folgte die Großmutter ihr.</ta>
            <ta e="T198" id="Seg_3095" s="T193">Als meine Großmutter ankam, war sie bereits tot.</ta>
            <ta e="T200" id="Seg_3096" s="T198">Meine Großmutter trauerte.</ta>
            <ta e="T203" id="Seg_3097" s="T200">Sie wurde mit einem Schlitten nach Hause gebracht.</ta>
            <ta e="T206" id="Seg_3098" s="T203">Sie lag lange im Haus.</ta>
            <ta e="T212" id="Seg_3099" s="T206">Ein Arzt kam, sah sie an und trug auf, sie zu begraben.</ta>
            <ta e="T213" id="Seg_3100" s="T212">Sie begruben sie.</ta>
            <ta e="T216" id="Seg_3101" s="T213">Dann lebten sie weiter.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_3102" s="T1">Моей бабушке сто лет.</ta>
            <ta e="T7" id="Seg_3103" s="T5">Она плохо жила.</ta>
            <ta e="T12" id="Seg_3104" s="T7">Молодая вышла замуж.</ta>
            <ta e="T17" id="Seg_3105" s="T12">Три года они прожили, детей не было.</ta>
            <ta e="T22" id="Seg_3106" s="T17">Потом один стал(?), ???.</ta>
            <ta e="T25" id="Seg_3107" s="T22">Три года жили (вместе).</ta>
            <ta e="T28" id="Seg_3108" s="T25">Детей много было.</ta>
            <ta e="T30" id="Seg_3109" s="T28">Как вырасли?</ta>
            <ta e="T32" id="Seg_3110" s="T30">Маленькими умерли.</ta>
            <ta e="T36" id="Seg_3111" s="T32">Моя бабушка и дедушка промышляли черканами.</ta>
            <ta e="T41" id="Seg_3112" s="T36">Муж, когда был пьяный, дрался и ругался.</ta>
            <ta e="T43" id="Seg_3113" s="T41">Он был сердитый.</ta>
            <ta e="T47" id="Seg_3114" s="T43">Она вышла замуж за вдовца.</ta>
            <ta e="T51" id="Seg_3115" s="T47">У него был маленький ребенок.</ta>
            <ta e="T56" id="Seg_3116" s="T51">Моя бабушка его вырастила и женила.</ta>
            <ta e="T60" id="Seg_3117" s="T56">У них была одна девчонка.</ta>
            <ta e="T63" id="Seg_3118" s="T60">Они оба умерли.</ta>
            <ta e="T69" id="Seg_3119" s="T63">У бабушки все дети от первого мужа умерли.</ta>
            <ta e="T75" id="Seg_3120" s="T69">Три дочери выросли, потом умерли.</ta>
            <ta e="T80" id="Seg_3121" s="T75">Первый муж дома болел и умер.</ta>
            <ta e="T84" id="Seg_3122" s="T80">Потом она долго жила одна. </ta>
            <ta e="T89" id="Seg_3123" s="T84">Потом она вышла замуж за другого мужика.</ta>
            <ta e="T92" id="Seg_3124" s="T89">Он был вдовец.</ta>
            <ta e="T97" id="Seg_3125" s="T92">У него от первой жены двое детей.</ta>
            <ta e="T101" id="Seg_3126" s="T97">Потом свои дети появились.</ta>
            <ta e="T106" id="Seg_3127" s="T101">Сколько было, она забыла.</ta>
            <ta e="T108" id="Seg_3128" s="T106">Двое выросли.</ta>
            <ta e="T113" id="Seg_3129" s="T108">Один вырос и в воде утонул.</ta>
            <ta e="T116" id="Seg_3130" s="T113">Одна жива.</ta>
            <ta e="T122" id="Seg_3131" s="T116">Мужику сказали: (?).</ta>
            <ta e="T125" id="Seg_3132" s="T122">Он унес (?).</ta>
            <ta e="T128" id="Seg_3133" s="T125">(?) он ее убил.</ta>
            <ta e="T131" id="Seg_3134" s="T128">Дочку не нашли.</ta>
            <ta e="T134" id="Seg_3135" s="T131">Его поймали, посадили.</ta>
            <ta e="T140" id="Seg_3136" s="T134">Про девочку спрашивали, куда девал он.</ta>
            <ta e="T142" id="Seg_3137" s="T140">Он не сказал.</ta>
            <ta e="T148" id="Seg_3138" s="T142">Он сказал: по слопечной дороге ушла домой.</ta>
            <ta e="T150" id="Seg_3139" s="T148">Он врал.</ta>
            <ta e="T153" id="Seg_3140" s="T150">Он куда-то дел ее.</ta>
            <ta e="T157" id="Seg_3141" s="T153">Бабушка осталась опять без мужа.</ta>
            <ta e="T162" id="Seg_3142" s="T157">Третий муж в тайге сгорел.</ta>
            <ta e="T165" id="Seg_3143" s="T162">Она жила с сыном.</ta>
            <ta e="T169" id="Seg_3144" s="T165">Потом этот сын утонул.</ta>
            <ta e="T175" id="Seg_3145" s="T169">Ему было тридцать лет.</ta>
            <ta e="T180" id="Seg_3146" s="T175">Бабушка жила в тайге с детьми, (они) белковали.</ta>
            <ta e="T187" id="Seg_3147" s="T180">Дочь сама себя застрелила, собаку хотела ударить (?).</ta>
            <ta e="T190" id="Seg_3148" s="T187">Дочь вперед ушла.</ta>
            <ta e="T193" id="Seg_3149" s="T190">Потом бабушка пошла.</ta>
            <ta e="T198" id="Seg_3150" s="T193">(Когда) бабушка пришла, она уже умерла.</ta>
            <ta e="T200" id="Seg_3151" s="T198">Бабушка тосковала.</ta>
            <ta e="T203" id="Seg_3152" s="T200">На нартах (ее) домой привезли.</ta>
            <ta e="T206" id="Seg_3153" s="T203">Долго дома лежала.</ta>
            <ta e="T212" id="Seg_3154" s="T206">Врач приехал, посмотрел и сказал: похоронить.</ta>
            <ta e="T213" id="Seg_3155" s="T212">Похоронили ее.</ta>
            <ta e="T216" id="Seg_3156" s="T213">Потом дальше стали жить.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_3157" s="T1">моей бабушке сто лет</ta>
            <ta e="T7" id="Seg_3158" s="T5">плохо жила</ta>
            <ta e="T12" id="Seg_3159" s="T7">молодая вышла замуж</ta>
            <ta e="T17" id="Seg_3160" s="T12">три года прожили детей не было</ta>
            <ta e="T22" id="Seg_3161" s="T17">сного </ta>
            <ta e="T28" id="Seg_3162" s="T25">детей много было</ta>
            <ta e="T32" id="Seg_3163" s="T30">маленькие умерли</ta>
            <ta e="T36" id="Seg_3164" s="T32">бабушка и дедушка промышляли черканами (черканничали)</ta>
            <ta e="T41" id="Seg_3165" s="T36">мужик когда был пьяный дрался ругался</ta>
            <ta e="T43" id="Seg_3166" s="T41">был сердитый</ta>
            <ta e="T47" id="Seg_3167" s="T43">вышла замуж за вдовца</ta>
            <ta e="T51" id="Seg_3168" s="T47">у него был маленький ребенок</ta>
            <ta e="T56" id="Seg_3169" s="T51">бабушка его вырастила и (женила?)</ta>
            <ta e="T60" id="Seg_3170" s="T56">у них одна была девчонка</ta>
            <ta e="T63" id="Seg_3171" s="T60">сами обои умерли</ta>
            <ta e="T69" id="Seg_3172" s="T63">у бабушки все дети от первого мужа умерли</ta>
            <ta e="T75" id="Seg_3173" s="T69">три дочери взрослые стали потом умерли</ta>
            <ta e="T80" id="Seg_3174" s="T75">первый муж дома болел умер</ta>
            <ta e="T84" id="Seg_3175" s="T80">потом жила одна долго</ta>
            <ta e="T89" id="Seg_3176" s="T84">потом вышла за другого мужика</ta>
            <ta e="T92" id="Seg_3177" s="T89">он был вдовец</ta>
            <ta e="T97" id="Seg_3178" s="T92">у него от первой жены двое детей</ta>
            <ta e="T101" id="Seg_3179" s="T97">потом свои дети были</ta>
            <ta e="T106" id="Seg_3180" s="T101">сколько было она забыла</ta>
            <ta e="T108" id="Seg_3181" s="T106">двое выросли</ta>
            <ta e="T113" id="Seg_3182" s="T108">один вырос и в воде утонул</ta>
            <ta e="T116" id="Seg_3183" s="T113">одна жива</ta>
            <ta e="T122" id="Seg_3184" s="T116">мужику сказали (бегал)</ta>
            <ta e="T131" id="Seg_3185" s="T128">дочку не нашли</ta>
            <ta e="T134" id="Seg_3186" s="T131">его поймали посадили</ta>
            <ta e="T140" id="Seg_3187" s="T134">про девочку спрашивали куда девал он</ta>
            <ta e="T142" id="Seg_3188" s="T140">не сказал</ta>
            <ta e="T148" id="Seg_3189" s="T142">по слопечной дороге ушла домой</ta>
            <ta e="T150" id="Seg_3190" s="T148">он врал</ta>
            <ta e="T153" id="Seg_3191" s="T150">он куда-то девал</ta>
            <ta e="T157" id="Seg_3192" s="T153">бабушка осталась опять без мужа</ta>
            <ta e="T162" id="Seg_3193" s="T157">третий муж в тайге сгорел</ta>
            <ta e="T165" id="Seg_3194" s="T162">она жила с сыном</ta>
            <ta e="T169" id="Seg_3195" s="T165">потом этот сын утонул</ta>
            <ta e="T175" id="Seg_3196" s="T169">ему было тридцать лет</ta>
            <ta e="T180" id="Seg_3197" s="T175">бабушка жила в тайге с детьми белковали</ta>
            <ta e="T187" id="Seg_3198" s="T180">дочь сама себя застрелила собаку хотела ударить</ta>
            <ta e="T190" id="Seg_3199" s="T187">дочь вперед ушла</ta>
            <ta e="T193" id="Seg_3200" s="T190">потом бабушка пошла</ta>
            <ta e="T198" id="Seg_3201" s="T193">бабушка пришла она уже умерла</ta>
            <ta e="T200" id="Seg_3202" s="T198">бабушка тосковала</ta>
            <ta e="T203" id="Seg_3203" s="T200">на нартах домой привезли</ta>
            <ta e="T206" id="Seg_3204" s="T203">долго дома лежала</ta>
            <ta e="T212" id="Seg_3205" s="T206">приехал посмотрел сказал похоронить</ta>
            <ta e="T213" id="Seg_3206" s="T212">похоронили</ta>
            <ta e="T216" id="Seg_3207" s="T213">потом дальше стали жить</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
