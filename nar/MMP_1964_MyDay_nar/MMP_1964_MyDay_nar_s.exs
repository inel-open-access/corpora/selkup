<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>MMP_1964_MyDay_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">MMP_1964_MyDay_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">138</ud-information>
            <ud-information attribute-name="# HIAT:w">99</ud-information>
            <ud-information attribute-name="# e">99</ud-information>
            <ud-information attribute-name="# HIAT:u">20</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="MMP">
            <abbreviation>MMP</abbreviation>
            <sex value="f" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="MMP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T100" id="Seg_0" n="sc" s="T1">
               <ts e="T9" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">telʼde</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">qaraj</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">mɨɣan</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">man</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">wässesaŋ</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_19" n="HIAT:w" s="T6">šɨtʼätkət</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_21" n="HIAT:ip">(</nts>
                  <ts e="T8" id="Seg_23" n="HIAT:w" s="T7">šɨtkəjkət</ts>
                  <nts id="Seg_24" n="HIAT:ip">)</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_27" n="HIAT:w" s="T8">časqane</ts>
                  <nts id="Seg_28" n="HIAT:ip">.</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_31" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_33" n="HIAT:w" s="T9">man</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_36" n="HIAT:w" s="T10">müsalʼǯisan</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_39" n="HIAT:w" s="T11">qandäpiː</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_42" n="HIAT:w" s="T12">üssä</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_45" n="HIAT:w" s="T13">mɨlaze</ts>
                  <nts id="Seg_46" n="HIAT:ip">,</nts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_49" n="HIAT:w" s="T14">teː</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_52" n="HIAT:w" s="T15">qaseŋ</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_55" n="HIAT:w" s="T16">tettaze</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_59" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">tujam</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_64" n="HIAT:w" s="T18">qatossam</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_67" n="HIAT:w" s="T19">tipsese</ts>
                  <nts id="Seg_68" n="HIAT:ip">,</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_71" n="HIAT:w" s="T20">tujam</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_74" n="HIAT:w" s="T21">passam</ts>
                  <nts id="Seg_75" n="HIAT:ip">,</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_78" n="HIAT:w" s="T22">sersam</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_81" n="HIAT:w" s="T23">qawajmɨm</ts>
                  <nts id="Seg_82" n="HIAT:ip">.</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_85" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_87" n="HIAT:w" s="T24">essan</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_90" n="HIAT:w" s="T25">sändə</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_93" n="HIAT:w" s="T26">qawajmɨ</ts>
                  <nts id="Seg_94" n="HIAT:ip">.</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_97" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_99" n="HIAT:w" s="T27">man</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_102" n="HIAT:w" s="T28">qwässan</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_105" n="HIAT:w" s="T29">taqkərəgu</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_108" n="HIAT:w" s="T30">kartopka</ts>
                  <nts id="Seg_109" n="HIAT:ip">.</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_112" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_114" n="HIAT:w" s="T31">nɨn</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_117" n="HIAT:w" s="T32">äːssan</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_120" n="HIAT:w" s="T33">qotʼtʼi</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_123" n="HIAT:w" s="T34">nɨnaŋa</ts>
                  <nts id="Seg_124" n="HIAT:ip">.</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_127" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_129" n="HIAT:w" s="T35">täerbisaŋ</ts>
                  <nts id="Seg_130" n="HIAT:ip">,</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_133" n="HIAT:w" s="T36">serčam</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_136" n="HIAT:w" s="T37">pöwlam</ts>
                  <nts id="Seg_137" n="HIAT:ip">,</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_140" n="HIAT:w" s="T38">nɨnaŋala</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_143" n="HIAT:w" s="T39">massɨŋ</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_146" n="HIAT:w" s="T40">aːmǯat</ts>
                  <nts id="Seg_147" n="HIAT:ip">.</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_150" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_152" n="HIAT:w" s="T41">i</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_155" n="HIAT:w" s="T42">seːrsaŋ</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_158" n="HIAT:w" s="T43">pöwlam</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_161" n="HIAT:w" s="T44">pajusillaze</ts>
                  <nts id="Seg_162" n="HIAT:ip">.</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_165" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_167" n="HIAT:w" s="T45">pöwla</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_170" n="HIAT:w" s="T46">toppam</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_173" n="HIAT:w" s="T47">ɨpqarpat</ts>
                  <nts id="Seg_174" n="HIAT:ip">.</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_177" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_179" n="HIAT:w" s="T48">man</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_182" n="HIAT:w" s="T49">toppɨlam</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_185" n="HIAT:w" s="T50">natqassam</ts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_189" n="HIAT:u" s="T51">
                  <nts id="Seg_190" n="HIAT:ip">(</nts>
                  <ts e="T52" id="Seg_192" n="HIAT:w" s="T51">tan</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_195" n="HIAT:w" s="T52">assə</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_198" n="HIAT:w" s="T53">natqalʼǯenǯal</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_201" n="HIAT:w" s="T54">toppɨlamdi</ts>
                  <nts id="Seg_202" n="HIAT:ip">)</nts>
                  <nts id="Seg_203" n="HIAT:ip">.</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_206" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_208" n="HIAT:w" s="T55">eːssan</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_211" n="HIAT:w" s="T56">ombottə</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_214" n="HIAT:w" s="T57">pötpti</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_217" n="HIAT:w" s="T58">tʼeːlat</ts>
                  <nts id="Seg_218" n="HIAT:ip">.</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_221" n="HIAT:u" s="T59">
                  <nts id="Seg_222" n="HIAT:ip">(</nts>
                  <ts e="T60" id="Seg_224" n="HIAT:w" s="T59">man</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_227" n="HIAT:w" s="T60">ombottə</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_230" n="HIAT:w" s="T61">soraŋ</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_233" n="HIAT:w" s="T62">ɨnnäm</ts>
                  <nts id="Seg_234" n="HIAT:ip">)</nts>
                  <nts id="Seg_235" n="HIAT:ip">.</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_238" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_240" n="HIAT:w" s="T63">nänni</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_243" n="HIAT:w" s="T64">wässɨlusat</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_246" n="HIAT:w" s="T65">peŋaj</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_249" n="HIAT:w" s="T66">pülla</ts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_253" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_255" n="HIAT:w" s="T67">täppɨla</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_258" n="HIAT:w" s="T68">meŋ</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_261" n="HIAT:w" s="T69">assɨ</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_264" n="HIAT:w" s="T70">tassatit</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_267" n="HIAT:w" s="T71">laqqugu</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_269" n="HIAT:ip">(</nts>
                  <ts e="T73" id="Seg_271" n="HIAT:w" s="T72">laqkɨgu</ts>
                  <nts id="Seg_272" n="HIAT:ip">)</nts>
                  <nts id="Seg_273" n="HIAT:ip">.</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_276" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_278" n="HIAT:w" s="T73">man</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_281" n="HIAT:w" s="T74">qwässan</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_284" n="HIAT:w" s="T75">maːttə</ts>
                  <nts id="Seg_285" n="HIAT:ip">.</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_288" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_290" n="HIAT:w" s="T76">ippilʼdessaŋ</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_293" n="HIAT:w" s="T77">assɨ</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_296" n="HIAT:w" s="T78">kundə</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_299" n="HIAT:w" s="T79">qoptan</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_302" n="HIAT:w" s="T80">barin</ts>
                  <nts id="Seg_303" n="HIAT:ip">.</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_306" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_308" n="HIAT:w" s="T81">nännɨ</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_311" n="HIAT:w" s="T82">man</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_314" n="HIAT:w" s="T83">tʼätsam</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_317" n="HIAT:w" s="T84">tösiwam</ts>
                  <nts id="Seg_318" n="HIAT:ip">,</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_321" n="HIAT:w" s="T85">müssərisam</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_324" n="HIAT:w" s="T86">masson</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_327" n="HIAT:w" s="T87">swängolləlam</ts>
                  <nts id="Seg_328" n="HIAT:ip">.</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_331" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_333" n="HIAT:w" s="T88">üːčella</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_336" n="HIAT:w" s="T89">wes</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_339" n="HIAT:w" s="T90">toːqalʼpadat</ts>
                  <nts id="Seg_340" n="HIAT:ip">,</nts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_343" n="HIAT:w" s="T91">aːmbatit</ts>
                  <nts id="Seg_344" n="HIAT:ip">.</nts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_347" n="HIAT:u" s="T92">
                  <nts id="Seg_348" n="HIAT:ip">(</nts>
                  <ts e="T93" id="Seg_350" n="HIAT:w" s="T92">man</ts>
                  <nts id="Seg_351" n="HIAT:ip">)</nts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_354" n="HIAT:w" s="T93">qassalam</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_357" n="HIAT:w" s="T94">awlʼewlʼe</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_360" n="HIAT:w" s="T95">pümman</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_363" n="HIAT:w" s="T96">ɨːlam</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_366" n="HIAT:w" s="T97">poːčaːmbat</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_368" n="HIAT:ip">(</nts>
                  <ts e="T99" id="Seg_370" n="HIAT:w" s="T98">počä</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_373" n="HIAT:w" s="T99">ambat</ts>
                  <nts id="Seg_374" n="HIAT:ip">)</nts>
                  <nts id="Seg_375" n="HIAT:ip">.</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T100" id="Seg_377" n="sc" s="T1">
               <ts e="T2" id="Seg_379" n="e" s="T1">telʼde </ts>
               <ts e="T3" id="Seg_381" n="e" s="T2">qaraj </ts>
               <ts e="T4" id="Seg_383" n="e" s="T3">mɨɣan </ts>
               <ts e="T5" id="Seg_385" n="e" s="T4">man </ts>
               <ts e="T6" id="Seg_387" n="e" s="T5">wässesaŋ </ts>
               <ts e="T7" id="Seg_389" n="e" s="T6">šɨtʼätkət </ts>
               <ts e="T8" id="Seg_391" n="e" s="T7">(šɨtkəjkət) </ts>
               <ts e="T9" id="Seg_393" n="e" s="T8">časqane. </ts>
               <ts e="T10" id="Seg_395" n="e" s="T9">man </ts>
               <ts e="T11" id="Seg_397" n="e" s="T10">müsalʼǯisan </ts>
               <ts e="T12" id="Seg_399" n="e" s="T11">qandäpiː </ts>
               <ts e="T13" id="Seg_401" n="e" s="T12">üssä </ts>
               <ts e="T14" id="Seg_403" n="e" s="T13">mɨlaze, </ts>
               <ts e="T15" id="Seg_405" n="e" s="T14">teː </ts>
               <ts e="T16" id="Seg_407" n="e" s="T15">qaseŋ </ts>
               <ts e="T17" id="Seg_409" n="e" s="T16">tettaze. </ts>
               <ts e="T18" id="Seg_411" n="e" s="T17">tujam </ts>
               <ts e="T19" id="Seg_413" n="e" s="T18">qatossam </ts>
               <ts e="T20" id="Seg_415" n="e" s="T19">tipsese, </ts>
               <ts e="T21" id="Seg_417" n="e" s="T20">tujam </ts>
               <ts e="T22" id="Seg_419" n="e" s="T21">passam, </ts>
               <ts e="T23" id="Seg_421" n="e" s="T22">sersam </ts>
               <ts e="T24" id="Seg_423" n="e" s="T23">qawajmɨm. </ts>
               <ts e="T25" id="Seg_425" n="e" s="T24">essan </ts>
               <ts e="T26" id="Seg_427" n="e" s="T25">sändə </ts>
               <ts e="T27" id="Seg_429" n="e" s="T26">qawajmɨ. </ts>
               <ts e="T28" id="Seg_431" n="e" s="T27">man </ts>
               <ts e="T29" id="Seg_433" n="e" s="T28">qwässan </ts>
               <ts e="T30" id="Seg_435" n="e" s="T29">taqkərəgu </ts>
               <ts e="T31" id="Seg_437" n="e" s="T30">kartopka. </ts>
               <ts e="T32" id="Seg_439" n="e" s="T31">nɨn </ts>
               <ts e="T33" id="Seg_441" n="e" s="T32">äːssan </ts>
               <ts e="T34" id="Seg_443" n="e" s="T33">qotʼtʼi </ts>
               <ts e="T35" id="Seg_445" n="e" s="T34">nɨnaŋa. </ts>
               <ts e="T36" id="Seg_447" n="e" s="T35">täerbisaŋ, </ts>
               <ts e="T37" id="Seg_449" n="e" s="T36">serčam </ts>
               <ts e="T38" id="Seg_451" n="e" s="T37">pöwlam, </ts>
               <ts e="T39" id="Seg_453" n="e" s="T38">nɨnaŋala </ts>
               <ts e="T40" id="Seg_455" n="e" s="T39">massɨŋ </ts>
               <ts e="T41" id="Seg_457" n="e" s="T40">aːmǯat. </ts>
               <ts e="T42" id="Seg_459" n="e" s="T41">i </ts>
               <ts e="T43" id="Seg_461" n="e" s="T42">seːrsaŋ </ts>
               <ts e="T44" id="Seg_463" n="e" s="T43">pöwlam </ts>
               <ts e="T45" id="Seg_465" n="e" s="T44">pajusillaze. </ts>
               <ts e="T46" id="Seg_467" n="e" s="T45">pöwla </ts>
               <ts e="T47" id="Seg_469" n="e" s="T46">toppam </ts>
               <ts e="T48" id="Seg_471" n="e" s="T47">ɨpqarpat. </ts>
               <ts e="T49" id="Seg_473" n="e" s="T48">man </ts>
               <ts e="T50" id="Seg_475" n="e" s="T49">toppɨlam </ts>
               <ts e="T51" id="Seg_477" n="e" s="T50">natqassam. </ts>
               <ts e="T52" id="Seg_479" n="e" s="T51">(tan </ts>
               <ts e="T53" id="Seg_481" n="e" s="T52">assə </ts>
               <ts e="T54" id="Seg_483" n="e" s="T53">natqalʼǯenǯal </ts>
               <ts e="T55" id="Seg_485" n="e" s="T54">toppɨlamdi). </ts>
               <ts e="T56" id="Seg_487" n="e" s="T55">eːssan </ts>
               <ts e="T57" id="Seg_489" n="e" s="T56">ombottə </ts>
               <ts e="T58" id="Seg_491" n="e" s="T57">pötpti </ts>
               <ts e="T59" id="Seg_493" n="e" s="T58">tʼeːlat. </ts>
               <ts e="T60" id="Seg_495" n="e" s="T59">(man </ts>
               <ts e="T61" id="Seg_497" n="e" s="T60">ombottə </ts>
               <ts e="T62" id="Seg_499" n="e" s="T61">soraŋ </ts>
               <ts e="T63" id="Seg_501" n="e" s="T62">ɨnnäm). </ts>
               <ts e="T64" id="Seg_503" n="e" s="T63">nänni </ts>
               <ts e="T65" id="Seg_505" n="e" s="T64">wässɨlusat </ts>
               <ts e="T66" id="Seg_507" n="e" s="T65">peŋaj </ts>
               <ts e="T67" id="Seg_509" n="e" s="T66">pülla. </ts>
               <ts e="T68" id="Seg_511" n="e" s="T67">täppɨla </ts>
               <ts e="T69" id="Seg_513" n="e" s="T68">meŋ </ts>
               <ts e="T70" id="Seg_515" n="e" s="T69">assɨ </ts>
               <ts e="T71" id="Seg_517" n="e" s="T70">tassatit </ts>
               <ts e="T72" id="Seg_519" n="e" s="T71">laqqugu </ts>
               <ts e="T73" id="Seg_521" n="e" s="T72">(laqkɨgu). </ts>
               <ts e="T74" id="Seg_523" n="e" s="T73">man </ts>
               <ts e="T75" id="Seg_525" n="e" s="T74">qwässan </ts>
               <ts e="T76" id="Seg_527" n="e" s="T75">maːttə. </ts>
               <ts e="T77" id="Seg_529" n="e" s="T76">ippilʼdessaŋ </ts>
               <ts e="T78" id="Seg_531" n="e" s="T77">assɨ </ts>
               <ts e="T79" id="Seg_533" n="e" s="T78">kundə </ts>
               <ts e="T80" id="Seg_535" n="e" s="T79">qoptan </ts>
               <ts e="T81" id="Seg_537" n="e" s="T80">barin. </ts>
               <ts e="T82" id="Seg_539" n="e" s="T81">nännɨ </ts>
               <ts e="T83" id="Seg_541" n="e" s="T82">man </ts>
               <ts e="T84" id="Seg_543" n="e" s="T83">tʼätsam </ts>
               <ts e="T85" id="Seg_545" n="e" s="T84">tösiwam, </ts>
               <ts e="T86" id="Seg_547" n="e" s="T85">müssərisam </ts>
               <ts e="T87" id="Seg_549" n="e" s="T86">masson </ts>
               <ts e="T88" id="Seg_551" n="e" s="T87">swängolləlam. </ts>
               <ts e="T89" id="Seg_553" n="e" s="T88">üːčella </ts>
               <ts e="T90" id="Seg_555" n="e" s="T89">wes </ts>
               <ts e="T91" id="Seg_557" n="e" s="T90">toːqalʼpadat, </ts>
               <ts e="T92" id="Seg_559" n="e" s="T91">aːmbatit. </ts>
               <ts e="T93" id="Seg_561" n="e" s="T92">(man) </ts>
               <ts e="T94" id="Seg_563" n="e" s="T93">qassalam </ts>
               <ts e="T95" id="Seg_565" n="e" s="T94">awlʼewlʼe </ts>
               <ts e="T96" id="Seg_567" n="e" s="T95">pümman </ts>
               <ts e="T97" id="Seg_569" n="e" s="T96">ɨːlam </ts>
               <ts e="T98" id="Seg_571" n="e" s="T97">poːčaːmbat </ts>
               <ts e="T99" id="Seg_573" n="e" s="T98">(počä </ts>
               <ts e="T100" id="Seg_575" n="e" s="T99">ambat). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T9" id="Seg_576" s="T1">MMP_1964_MyDay_nar.001 (001.001)</ta>
            <ta e="T17" id="Seg_577" s="T9">MMP_1964_MyDay_nar.002 (001.002)</ta>
            <ta e="T24" id="Seg_578" s="T17">MMP_1964_MyDay_nar.003 (001.003)</ta>
            <ta e="T27" id="Seg_579" s="T24">MMP_1964_MyDay_nar.004 (001.004)</ta>
            <ta e="T31" id="Seg_580" s="T27">MMP_1964_MyDay_nar.005 (001.005)</ta>
            <ta e="T35" id="Seg_581" s="T31">MMP_1964_MyDay_nar.006 (001.006)</ta>
            <ta e="T41" id="Seg_582" s="T35">MMP_1964_MyDay_nar.007 (001.007)</ta>
            <ta e="T45" id="Seg_583" s="T41">MMP_1964_MyDay_nar.008 (001.008)</ta>
            <ta e="T48" id="Seg_584" s="T45">MMP_1964_MyDay_nar.009 (001.009)</ta>
            <ta e="T51" id="Seg_585" s="T48">MMP_1964_MyDay_nar.010 (001.010)</ta>
            <ta e="T55" id="Seg_586" s="T51">MMP_1964_MyDay_nar.011 (001.011)</ta>
            <ta e="T59" id="Seg_587" s="T55">MMP_1964_MyDay_nar.012 (001.012)</ta>
            <ta e="T63" id="Seg_588" s="T59">MMP_1964_MyDay_nar.013 (001.013)</ta>
            <ta e="T67" id="Seg_589" s="T63">MMP_1964_MyDay_nar.014 (001.014)</ta>
            <ta e="T73" id="Seg_590" s="T67">MMP_1964_MyDay_nar.015 (001.015)</ta>
            <ta e="T76" id="Seg_591" s="T73">MMP_1964_MyDay_nar.016 (001.016)</ta>
            <ta e="T81" id="Seg_592" s="T76">MMP_1964_MyDay_nar.017 (001.017)</ta>
            <ta e="T88" id="Seg_593" s="T81">MMP_1964_MyDay_nar.018 (001.018)</ta>
            <ta e="T92" id="Seg_594" s="T88">MMP_1964_MyDay_nar.019 (001.019)</ta>
            <ta e="T100" id="Seg_595" s="T92">MMP_1964_MyDay_nar.020 (001.020)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T9" id="Seg_596" s="T1">′телʼде kа′рай мыɣан ман ′вӓссе(ъ)‵саң шы′тʼӓткъ̊т (шыткъ̊йкъ̊т) часkане.</ta>
            <ta e="T17" id="Seg_597" s="T9">ман ′мӱсалʼджи′сан ′kандӓпӣ ӱссӓ ′мылазе, ′те̄(ӓ̄) kа′сең ′теттазе.</ta>
            <ta e="T24" id="Seg_598" s="T17">′туjам kа′тоссам типсесе, ′туjам ′пассам, ′серсам kа′ваймым.</ta>
            <ta e="T27" id="Seg_599" s="T24">′ессан сӓндъ kа′ваймы.</ta>
            <ta e="T31" id="Seg_600" s="T27">ман ′kwӓссан ′таkкър(ъ)гу кар′топка.</ta>
            <ta e="T35" id="Seg_601" s="T31">нын ′ӓ̄ссан ′kотʼтʼи ны′наңа.</ta>
            <ta e="T41" id="Seg_602" s="T35">′тӓерб̂(п)исаң, ′сертшам ′пӧвлам, нына′ңаlа ′массы(и)ң ′а̄мджат.</ta>
            <ta e="T45" id="Seg_603" s="T41">и ′се̄рсаң ′пӧвlам ′паjу ′сиllазе.</ta>
            <ta e="T48" id="Seg_604" s="T45">пӧwла ′топпам ′ыпkарпат.</ta>
            <ta e="T51" id="Seg_605" s="T48">ман ′топпыlам ‵нат′kассам.</ta>
            <ta e="T55" id="Seg_606" s="T51">(тан ′ассъ нат′kалʼдженджал ′топпыламди.)</ta>
            <ta e="T59" id="Seg_607" s="T55">′е̨̄ссан ом′боттъ ′пӧтпти ′тʼе̄лат.</ta>
            <ta e="T63" id="Seg_608" s="T59">(ман ом′боттъ ′сораң ы′ннӓм.)</ta>
            <ta e="T67" id="Seg_609" s="T63">′нӓнни ′вӓссыlу‵сат ′пеңай ′пӱllа.</ta>
            <ta e="T73" id="Seg_610" s="T67">′тӓппыlа ′мең ′ассы та′ссатит lаkkугу (‵lаkкы′гу).</ta>
            <ta e="T76" id="Seg_611" s="T73">ман ′kwӓссан ′ма̄ттъ.</ta>
            <ta e="T81" id="Seg_612" s="T76">и′ппилʼ′дессаң ′ассы ′кундъ ′kоптан ′б̂арин.</ta>
            <ta e="T88" id="Seg_613" s="T81">′нӓнны ман ′тʼӓтсам ′тӧсивам, ′мӱссърисам ′массон ‵сwӓн′гоllъ(ӓ)lам.</ta>
            <ta e="T92" id="Seg_614" s="T88">ӱ̄′чеllа ′вес то̄′kалʼпад̂ат, а̄м′батит.</ta>
            <ta e="T100" id="Seg_615" s="T92">(ман) ′kассаlам ав′лʼевлʼе ′пӱмман ′ы̄lам ′по̄тша̄мбат (′потшӓ ам′бат).</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T9" id="Seg_616" s="T1">telʼde qaraj mɨɣan man vässe(ə)saŋ šɨtʼätkət (šɨtkəjkət) časqane.</ta>
            <ta e="T17" id="Seg_617" s="T9">man müsalʼǯisan qandäpiː üssä mɨlaze, teː(äː) qaseŋ tettaze.</ta>
            <ta e="T24" id="Seg_618" s="T17">tujam qatossam tipsese, tujam passam, sersam qavajmɨm.</ta>
            <ta e="T27" id="Seg_619" s="T24">essan sändə qavajmɨ.</ta>
            <ta e="T31" id="Seg_620" s="T27">man qwässan taqkər(ə)gu kartopka.</ta>
            <ta e="T35" id="Seg_621" s="T31">nɨn äːssan qotʼtʼi nɨnaŋa.</ta>
            <ta e="T41" id="Seg_622" s="T35">täerb̂(p)isaŋ, sertšam pövlam, nɨnaŋala massɨ(i)ŋ aːmǯat.</ta>
            <ta e="T45" id="Seg_623" s="T41">i seːrsaŋ pövlam paju sillaze.</ta>
            <ta e="T48" id="Seg_624" s="T45">pöwla toppam ɨpqarpat.</ta>
            <ta e="T51" id="Seg_625" s="T48">man toppɨlam natqassam.</ta>
            <ta e="T55" id="Seg_626" s="T51">(tan assə natqalʼǯenǯal toppɨlamdi.)</ta>
            <ta e="T59" id="Seg_627" s="T55">eːssan ombottə pötpti tʼeːlat.</ta>
            <ta e="T63" id="Seg_628" s="T59">(man ombottə soraŋ ɨnnäm.)</ta>
            <ta e="T67" id="Seg_629" s="T63">nänni vässɨlusat peŋaj pülla.</ta>
            <ta e="T73" id="Seg_630" s="T67">täppɨla meŋ assɨ tassatit laqqugu (laqkɨgu).</ta>
            <ta e="T76" id="Seg_631" s="T73">man qwässan maːttə.</ta>
            <ta e="T81" id="Seg_632" s="T76">ippilʼdessaŋ assɨ kundə qoptan b̂arin.</ta>
            <ta e="T88" id="Seg_633" s="T81">nännɨ man tʼätsam tösivam, müssərisam masson swängollə(ä)lam.</ta>
            <ta e="T92" id="Seg_634" s="T88">üːčella ves toːqalʼpad̂at, aːmbatit.</ta>
            <ta e="T100" id="Seg_635" s="T92">(man) qassalam avlʼevlʼe pümman ɨːlam poːtšaːmbat (potšä ambat).</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T9" id="Seg_636" s="T1">telʼde qaraj mɨɣan man wässesaŋ šɨtʼätkət (šɨtkəjkət) časqane. </ta>
            <ta e="T17" id="Seg_637" s="T9">man müsalʼǯisan qandäpiː üssä mɨlaze, teː qaseŋ tettaze. </ta>
            <ta e="T24" id="Seg_638" s="T17">tujam qatossam tipsese, tujam passam, sersam qawajmɨm. </ta>
            <ta e="T27" id="Seg_639" s="T24">essan sändə qawajmɨ. </ta>
            <ta e="T31" id="Seg_640" s="T27">man qwässan taqkərəgu kartopka. </ta>
            <ta e="T35" id="Seg_641" s="T31">nɨn äːssan qotʼtʼi nɨnaŋa. </ta>
            <ta e="T41" id="Seg_642" s="T35">täerbisaŋ, serčam pöwlam, nɨnaŋala massɨŋ aːmǯat. </ta>
            <ta e="T45" id="Seg_643" s="T41">i seːrsaŋ pöwlam pajusillaze. </ta>
            <ta e="T48" id="Seg_644" s="T45">pöwla toppam ɨpqarpat. </ta>
            <ta e="T51" id="Seg_645" s="T48">man toppɨlam natqassam. </ta>
            <ta e="T55" id="Seg_646" s="T51">(tan assə natqalʼǯenǯal toppɨlamdi.) </ta>
            <ta e="T59" id="Seg_647" s="T55">eːssan ombottə pötpti tʼeːlat. </ta>
            <ta e="T63" id="Seg_648" s="T59">(man ombottə soraŋ ɨnnäm.) </ta>
            <ta e="T67" id="Seg_649" s="T63">nänni wässɨlusat peŋaj pülla. </ta>
            <ta e="T73" id="Seg_650" s="T67">täppɨla meŋ assɨ tassatit laqqugu (laqkɨgu). </ta>
            <ta e="T76" id="Seg_651" s="T73">man qwässan maːttə. </ta>
            <ta e="T81" id="Seg_652" s="T76">ippilʼdessaŋ assɨ kundə qoptan barin. </ta>
            <ta e="T88" id="Seg_653" s="T81">nännɨ man tʼätsam tösiwam, müssərisam masson swängolləlam. </ta>
            <ta e="T92" id="Seg_654" s="T88">üːčella wes toːqalʼpadat, aːmbatit. </ta>
            <ta e="T100" id="Seg_655" s="T92">(man) qassalam awlʼewlʼe pümman ɨːlam poːčaːmbat (počä ambat). </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_656" s="T1">telʼde</ta>
            <ta e="T3" id="Seg_657" s="T2">qara-j</ta>
            <ta e="T4" id="Seg_658" s="T3">mɨ-ɣan</ta>
            <ta e="T5" id="Seg_659" s="T4">man</ta>
            <ta e="T6" id="Seg_660" s="T5">wässe-sa-ŋ</ta>
            <ta e="T7" id="Seg_661" s="T6">šɨtʼätkət</ta>
            <ta e="T8" id="Seg_662" s="T7">šɨtkəjkət</ta>
            <ta e="T9" id="Seg_663" s="T8">čas-qane</ta>
            <ta e="T10" id="Seg_664" s="T9">man</ta>
            <ta e="T11" id="Seg_665" s="T10">müsa-lʼǯi-sa-n</ta>
            <ta e="T12" id="Seg_666" s="T11">qandä-piː</ta>
            <ta e="T13" id="Seg_667" s="T12">üs-sä</ta>
            <ta e="T14" id="Seg_668" s="T13">mɨla-ze</ta>
            <ta e="T15" id="Seg_669" s="T14">teː</ta>
            <ta e="T16" id="Seg_670" s="T15">qa-se-ŋ</ta>
            <ta e="T17" id="Seg_671" s="T16">tetta-ze</ta>
            <ta e="T18" id="Seg_672" s="T17">tuja-m</ta>
            <ta e="T19" id="Seg_673" s="T18">qato-ssa-m</ta>
            <ta e="T20" id="Seg_674" s="T19">tipse-se</ta>
            <ta e="T21" id="Seg_675" s="T20">tuja-m</ta>
            <ta e="T22" id="Seg_676" s="T21">pas-sa-m</ta>
            <ta e="T23" id="Seg_677" s="T22">ser-sa-m</ta>
            <ta e="T24" id="Seg_678" s="T23">qawaj-mɨ-m</ta>
            <ta e="T25" id="Seg_679" s="T24">e-ssa-ŋ</ta>
            <ta e="T26" id="Seg_680" s="T25">sändə</ta>
            <ta e="T27" id="Seg_681" s="T26">qawaj-mɨ</ta>
            <ta e="T28" id="Seg_682" s="T27">man</ta>
            <ta e="T29" id="Seg_683" s="T28">qwäs-sa-ŋ</ta>
            <ta e="T30" id="Seg_684" s="T29">taqkə-r-ə-gu</ta>
            <ta e="T31" id="Seg_685" s="T30">kartopka</ta>
            <ta e="T32" id="Seg_686" s="T31">nɨn</ta>
            <ta e="T33" id="Seg_687" s="T32">äː-ssa-ŋ</ta>
            <ta e="T34" id="Seg_688" s="T33">qotʼtʼi</ta>
            <ta e="T35" id="Seg_689" s="T34">nɨnaŋa</ta>
            <ta e="T36" id="Seg_690" s="T35">täerbi-sa-ŋ</ta>
            <ta e="T37" id="Seg_691" s="T36">ser-ča-m</ta>
            <ta e="T38" id="Seg_692" s="T37">pöw-la-m</ta>
            <ta e="T39" id="Seg_693" s="T38">nɨnaŋa-la</ta>
            <ta e="T40" id="Seg_694" s="T39">massɨŋ</ta>
            <ta e="T41" id="Seg_695" s="T40">aːm-ǯa-t</ta>
            <ta e="T42" id="Seg_696" s="T41">i</ta>
            <ta e="T43" id="Seg_697" s="T42">seːr-sa-ŋ</ta>
            <ta e="T44" id="Seg_698" s="T43">pöw-la-m</ta>
            <ta e="T45" id="Seg_699" s="T44">pajus-i-lla-ze</ta>
            <ta e="T46" id="Seg_700" s="T45">pöw-la</ta>
            <ta e="T47" id="Seg_701" s="T46">toppa-m</ta>
            <ta e="T48" id="Seg_702" s="T47">ɨpqa-r-pa-t</ta>
            <ta e="T49" id="Seg_703" s="T48">man</ta>
            <ta e="T50" id="Seg_704" s="T49">toppɨ-la-m</ta>
            <ta e="T51" id="Seg_705" s="T50">natqas-sa-m</ta>
            <ta e="T52" id="Seg_706" s="T51">tat</ta>
            <ta e="T53" id="Seg_707" s="T52">assə</ta>
            <ta e="T54" id="Seg_708" s="T53">natqalʼ-ǯe-nǯa-l</ta>
            <ta e="T55" id="Seg_709" s="T54">toppɨ-la-m-di</ta>
            <ta e="T56" id="Seg_710" s="T55">eː-ssa-n</ta>
            <ta e="T57" id="Seg_711" s="T56">ombottə</ta>
            <ta e="T58" id="Seg_712" s="T57">pötpti</ta>
            <ta e="T59" id="Seg_713" s="T58">tʼeːla-tɨ</ta>
            <ta e="T60" id="Seg_714" s="T59">man</ta>
            <ta e="T61" id="Seg_715" s="T60">ombottə</ta>
            <ta e="T62" id="Seg_716" s="T61">sora-ŋ</ta>
            <ta e="T63" id="Seg_717" s="T62">ɨnnä-m</ta>
            <ta e="T64" id="Seg_718" s="T63">nänni</ta>
            <ta e="T65" id="Seg_719" s="T64">wässɨ-lu-sa-t</ta>
            <ta e="T66" id="Seg_720" s="T65">peŋa-j</ta>
            <ta e="T67" id="Seg_721" s="T66">pü-lla</ta>
            <ta e="T68" id="Seg_722" s="T67">täpp-ɨ-la</ta>
            <ta e="T69" id="Seg_723" s="T68">meŋ</ta>
            <ta e="T70" id="Seg_724" s="T69">assɨ</ta>
            <ta e="T71" id="Seg_725" s="T70">tas-sa-tit</ta>
            <ta e="T72" id="Seg_726" s="T71">laqqu-gu</ta>
            <ta e="T73" id="Seg_727" s="T72">laqkɨ-gu</ta>
            <ta e="T74" id="Seg_728" s="T73">man</ta>
            <ta e="T75" id="Seg_729" s="T74">qwäs-sa-n</ta>
            <ta e="T76" id="Seg_730" s="T75">maːt-tə</ta>
            <ta e="T77" id="Seg_731" s="T76">ippi-lʼde-ssa-ŋ</ta>
            <ta e="T78" id="Seg_732" s="T77">assɨ</ta>
            <ta e="T79" id="Seg_733" s="T78">kundə</ta>
            <ta e="T80" id="Seg_734" s="T79">qopta-n</ta>
            <ta e="T81" id="Seg_735" s="T80">bar-i-n</ta>
            <ta e="T82" id="Seg_736" s="T81">nännɨ</ta>
            <ta e="T83" id="Seg_737" s="T82">man</ta>
            <ta e="T84" id="Seg_738" s="T83">tʼät-sa-m</ta>
            <ta e="T85" id="Seg_739" s="T84">tö-siwa-m</ta>
            <ta e="T86" id="Seg_740" s="T85">müssə-r-i-sa-m</ta>
            <ta e="T87" id="Seg_741" s="T86">masso-n</ta>
            <ta e="T88" id="Seg_742" s="T87">swäng-ollə-la-m</ta>
            <ta e="T89" id="Seg_743" s="T88">üːče-lla</ta>
            <ta e="T90" id="Seg_744" s="T89">wes</ta>
            <ta e="T91" id="Seg_745" s="T90">toːqalʼ-pa-da-t</ta>
            <ta e="T92" id="Seg_746" s="T91">aːm-ba-tit</ta>
            <ta e="T93" id="Seg_747" s="T92">man</ta>
            <ta e="T94" id="Seg_748" s="T93">qassa-la-m</ta>
            <ta e="T95" id="Seg_749" s="T94">aw-lʼewlʼe</ta>
            <ta e="T96" id="Seg_750" s="T95">pümma-n</ta>
            <ta e="T97" id="Seg_751" s="T96">ɨː-la-m</ta>
            <ta e="T98" id="Seg_752" s="T97">poːčaː-mba-t</ta>
            <ta e="T99" id="Seg_753" s="T98">počä</ta>
            <ta e="T100" id="Seg_754" s="T99">am-ɨ-ba-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_755" s="T1">telʼdʼe</ta>
            <ta e="T3" id="Seg_756" s="T2">qarɨ-lʼ</ta>
            <ta e="T4" id="Seg_757" s="T3">mɨ-qən</ta>
            <ta e="T5" id="Seg_758" s="T4">man</ta>
            <ta e="T6" id="Seg_759" s="T5">wessɨ-sɨ-ŋ</ta>
            <ta e="T7" id="Seg_760" s="T6">šɨtʼätköt</ta>
            <ta e="T8" id="Seg_761" s="T7">šittəqwäjgöt</ta>
            <ta e="T9" id="Seg_762" s="T8">čas-qɨnnɨ</ta>
            <ta e="T10" id="Seg_763" s="T9">man</ta>
            <ta e="T11" id="Seg_764" s="T10">müsɨ-lʼčǝ-sɨ-ŋ</ta>
            <ta e="T12" id="Seg_765" s="T11">qantǝ-mbɨdi</ta>
            <ta e="T13" id="Seg_766" s="T12">üt-se</ta>
            <ta e="T14" id="Seg_767" s="T13">mɨla-se</ta>
            <ta e="T15" id="Seg_768" s="T14">teː</ta>
            <ta e="T16" id="Seg_769" s="T15">qal-sɨ-ŋ</ta>
            <ta e="T17" id="Seg_770" s="T16">tetta-se</ta>
            <ta e="T18" id="Seg_771" s="T17">tuːja-m</ta>
            <ta e="T19" id="Seg_772" s="T18">qato-sɨ-m</ta>
            <ta e="T20" id="Seg_773" s="T19">tipsin-se</ta>
            <ta e="T21" id="Seg_774" s="T20">tuːja-m</ta>
            <ta e="T22" id="Seg_775" s="T21">paŋ-sɨ-m</ta>
            <ta e="T23" id="Seg_776" s="T22">šeːr-sɨ-m</ta>
            <ta e="T24" id="Seg_777" s="T23">qawaj-mɨ-m</ta>
            <ta e="T25" id="Seg_778" s="T24">eː-sɨ-n</ta>
            <ta e="T26" id="Seg_779" s="T25">sendɨ</ta>
            <ta e="T27" id="Seg_780" s="T26">qawaj-mɨ</ta>
            <ta e="T28" id="Seg_781" s="T27">man</ta>
            <ta e="T29" id="Seg_782" s="T28">qwən-sɨ-ŋ</ta>
            <ta e="T30" id="Seg_783" s="T29">takǝ-r-ɨ-gu</ta>
            <ta e="T31" id="Seg_784" s="T30">kartopka</ta>
            <ta e="T32" id="Seg_785" s="T31">nɨmtɨ</ta>
            <ta e="T33" id="Seg_786" s="T32">eː-sɨ-n</ta>
            <ta e="T34" id="Seg_787" s="T33">koči</ta>
            <ta e="T35" id="Seg_788" s="T34">nɨnka</ta>
            <ta e="T36" id="Seg_789" s="T35">tärba-sɨ-ŋ</ta>
            <ta e="T37" id="Seg_790" s="T36">šeːr-nǯɨ-m</ta>
            <ta e="T38" id="Seg_791" s="T37">pöwə-la-m</ta>
            <ta e="T39" id="Seg_792" s="T38">nɨnka-la</ta>
            <ta e="T40" id="Seg_793" s="T39">mašim</ta>
            <ta e="T41" id="Seg_794" s="T40">am-nče-tɨt</ta>
            <ta e="T42" id="Seg_795" s="T41">i</ta>
            <ta e="T43" id="Seg_796" s="T42">šeːr-sɨ-ŋ</ta>
            <ta e="T44" id="Seg_797" s="T43">pöwə-la-m</ta>
            <ta e="T45" id="Seg_798" s="T44">pajus-ɨ-la-se</ta>
            <ta e="T46" id="Seg_799" s="T45">pöwə-la</ta>
            <ta e="T47" id="Seg_800" s="T46">topǝ-m</ta>
            <ta e="T48" id="Seg_801" s="T47">ɨpqa-r-mbɨ-tɨt</ta>
            <ta e="T49" id="Seg_802" s="T48">man</ta>
            <ta e="T50" id="Seg_803" s="T49">topǝ-la-m</ta>
            <ta e="T51" id="Seg_804" s="T50">natqal-sɨ-m</ta>
            <ta e="T52" id="Seg_805" s="T51">tan</ta>
            <ta e="T53" id="Seg_806" s="T52">assɨ</ta>
            <ta e="T54" id="Seg_807" s="T53">natqal-nče-nǯɨ-l</ta>
            <ta e="T55" id="Seg_808" s="T54">topǝ-la-m-di</ta>
            <ta e="T56" id="Seg_809" s="T55">eː-sɨ-n</ta>
            <ta e="T57" id="Seg_810" s="T56">ombottə</ta>
            <ta e="T58" id="Seg_811" s="T57">pöttɨ</ta>
            <ta e="T59" id="Seg_812" s="T58">tʼeːlɨ-tɨ</ta>
            <ta e="T60" id="Seg_813" s="T59">man</ta>
            <ta e="T61" id="Seg_814" s="T60">ombottə</ta>
            <ta e="T62" id="Seg_815" s="T61">soːri-ŋ</ta>
            <ta e="T63" id="Seg_816" s="T62">imnʼa-m</ta>
            <ta e="T64" id="Seg_817" s="T63">nɨːnɨ</ta>
            <ta e="T65" id="Seg_818" s="T64">wessɨ-lɨ-sɨ-tɨt</ta>
            <ta e="T66" id="Seg_819" s="T65">päŋqa-lʼ</ta>
            <ta e="T67" id="Seg_820" s="T66">püː-la</ta>
            <ta e="T68" id="Seg_821" s="T67">tap-ɨ-la</ta>
            <ta e="T69" id="Seg_822" s="T68">mäkkä</ta>
            <ta e="T70" id="Seg_823" s="T69">assɨ</ta>
            <ta e="T71" id="Seg_824" s="T70">tadɨ-sɨ-tɨt</ta>
            <ta e="T72" id="Seg_825" s="T71">laqqɨ-gu</ta>
            <ta e="T73" id="Seg_826" s="T72">laqqɨ-gu</ta>
            <ta e="T74" id="Seg_827" s="T73">man</ta>
            <ta e="T75" id="Seg_828" s="T74">qwən-sɨ-ŋ</ta>
            <ta e="T76" id="Seg_829" s="T75">maːt-ndɨ</ta>
            <ta e="T77" id="Seg_830" s="T76">ippi-lʼčǝ-sɨ-n</ta>
            <ta e="T78" id="Seg_831" s="T77">assɨ</ta>
            <ta e="T79" id="Seg_832" s="T78">kundɨ</ta>
            <ta e="T80" id="Seg_833" s="T79">koːptɨ-n</ta>
            <ta e="T81" id="Seg_834" s="T80">par-ɨ-n</ta>
            <ta e="T82" id="Seg_835" s="T81">nɨːnɨ</ta>
            <ta e="T83" id="Seg_836" s="T82">man</ta>
            <ta e="T84" id="Seg_837" s="T83">tʼadɨ-sɨ-m</ta>
            <ta e="T85" id="Seg_838" s="T84">tüː-siwa-m</ta>
            <ta e="T86" id="Seg_839" s="T85">muᴣǝ-r-ɨ-sɨ-m</ta>
            <ta e="T87" id="Seg_840" s="T86">massu-n</ta>
            <ta e="T88" id="Seg_841" s="T87">swäŋɨ-olɨ-la-m</ta>
            <ta e="T89" id="Seg_842" s="T88">ütče-la</ta>
            <ta e="T90" id="Seg_843" s="T89">wesʼ</ta>
            <ta e="T91" id="Seg_844" s="T90">tuɣol-mbɨ-ntɨ-tɨt</ta>
            <ta e="T92" id="Seg_845" s="T91">am-mbɨ-tɨt</ta>
            <ta e="T93" id="Seg_846" s="T92">man</ta>
            <ta e="T94" id="Seg_847" s="T93">qassa-la-m</ta>
            <ta e="T95" id="Seg_848" s="T94">am-lewle</ta>
            <ta e="T96" id="Seg_849" s="T95">pimmɨ-t</ta>
            <ta e="T97" id="Seg_850" s="T96">iː-lɨ-m</ta>
            <ta e="T98" id="Seg_851" s="T97">poːčaː-mbɨ-tɨt</ta>
            <ta e="T99" id="Seg_852" s="T98">poːčaː</ta>
            <ta e="T100" id="Seg_853" s="T99">am-ɨ-mbɨ-tɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_854" s="T1">yesterday</ta>
            <ta e="T3" id="Seg_855" s="T2">morning-ADJZ</ta>
            <ta e="T4" id="Seg_856" s="T3">something-LOC</ta>
            <ta e="T5" id="Seg_857" s="T4">I.NOM</ta>
            <ta e="T6" id="Seg_858" s="T5">get.up-PST-1SG.S</ta>
            <ta e="T7" id="Seg_859" s="T6">eight</ta>
            <ta e="T8" id="Seg_860" s="T7">twelve</ta>
            <ta e="T9" id="Seg_861" s="T8">hour-EL</ta>
            <ta e="T10" id="Seg_862" s="T9">I.NOM</ta>
            <ta e="T11" id="Seg_863" s="T10">wash-PFV-PST-1SG.S</ta>
            <ta e="T12" id="Seg_864" s="T11">freeze-PTCP.PST</ta>
            <ta e="T13" id="Seg_865" s="T12">water-INSTR</ta>
            <ta e="T14" id="Seg_866" s="T13">soap-INSTR</ta>
            <ta e="T15" id="Seg_867" s="T14">away</ta>
            <ta e="T16" id="Seg_868" s="T15">wipe.out-PST-1SG.S</ta>
            <ta e="T17" id="Seg_869" s="T16">towel-INSTR</ta>
            <ta e="T18" id="Seg_870" s="T17">hair-ACC</ta>
            <ta e="T19" id="Seg_871" s="T18">scratch-PST-1SG.O</ta>
            <ta e="T20" id="Seg_872" s="T19">ridge-INSTR</ta>
            <ta e="T21" id="Seg_873" s="T20">hair-ACC</ta>
            <ta e="T22" id="Seg_874" s="T21">plait-PST-1SG.O</ta>
            <ta e="T23" id="Seg_875" s="T22">dress-PST-1SG.O</ta>
            <ta e="T24" id="Seg_876" s="T23">dress-something-ACC</ta>
            <ta e="T25" id="Seg_877" s="T24">be-PST-3SG.S</ta>
            <ta e="T26" id="Seg_878" s="T25">new</ta>
            <ta e="T27" id="Seg_879" s="T26">dress.[NOM]-1SG</ta>
            <ta e="T28" id="Seg_880" s="T27">I.NOM</ta>
            <ta e="T29" id="Seg_881" s="T28">go.away-PST-1SG.S</ta>
            <ta e="T30" id="Seg_882" s="T29">gather-FRQ-EP-INF</ta>
            <ta e="T31" id="Seg_883" s="T30">potato.[NOM]</ta>
            <ta e="T32" id="Seg_884" s="T31">here</ta>
            <ta e="T33" id="Seg_885" s="T32">be-PST-3SG.S</ta>
            <ta e="T34" id="Seg_886" s="T33">much</ta>
            <ta e="T35" id="Seg_887" s="T34">mosquito.[NOM]</ta>
            <ta e="T36" id="Seg_888" s="T35">think-PST-1SG.S</ta>
            <ta e="T37" id="Seg_889" s="T36">dress-FUT-1SG.O</ta>
            <ta e="T38" id="Seg_890" s="T37">boots-PL-ACC</ta>
            <ta e="T39" id="Seg_891" s="T38">mosquito-PL.[NOM]</ta>
            <ta e="T40" id="Seg_892" s="T39">I.ACC</ta>
            <ta e="T41" id="Seg_893" s="T40">eat-IPFV3-3PL</ta>
            <ta e="T42" id="Seg_894" s="T41">and</ta>
            <ta e="T43" id="Seg_895" s="T42">dress-PST-1SG.S</ta>
            <ta e="T44" id="Seg_896" s="T43">boots-PL-ACC</ta>
            <ta e="T45" id="Seg_897" s="T44">footwrap-EP-PL-INSTR</ta>
            <ta e="T46" id="Seg_898" s="T45">boots-PL.[NOM]</ta>
            <ta e="T47" id="Seg_899" s="T46">leg-ACC</ta>
            <ta e="T48" id="Seg_900" s="T47">%%-FRQ-PST.NAR-3PL</ta>
            <ta e="T49" id="Seg_901" s="T48">I.NOM</ta>
            <ta e="T50" id="Seg_902" s="T49">leg-PL-ACC</ta>
            <ta e="T51" id="Seg_903" s="T50">make.callous-PST-1SG.O</ta>
            <ta e="T52" id="Seg_904" s="T51">you.SG.NOM</ta>
            <ta e="T53" id="Seg_905" s="T52">NEG</ta>
            <ta e="T54" id="Seg_906" s="T53">make.callous-IPFV3-FUT-2SG.O</ta>
            <ta e="T55" id="Seg_907" s="T54">leg-PL-ACC-3DU</ta>
            <ta e="T56" id="Seg_908" s="T55">be-PST-3SG.S</ta>
            <ta e="T57" id="Seg_909" s="T56">very</ta>
            <ta e="T58" id="Seg_910" s="T57">hot</ta>
            <ta e="T59" id="Seg_911" s="T58">day.[NOM]-3SG</ta>
            <ta e="T60" id="Seg_912" s="T59">I.NOM</ta>
            <ta e="T61" id="Seg_913" s="T60">very</ta>
            <ta e="T62" id="Seg_914" s="T61">love-1SG.S</ta>
            <ta e="T63" id="Seg_915" s="T62">brother-ACC</ta>
            <ta e="T64" id="Seg_916" s="T63">then</ta>
            <ta e="T65" id="Seg_917" s="T64">get.up-RES-PST-3PL</ta>
            <ta e="T66" id="Seg_918" s="T65">elk-ADJZ</ta>
            <ta e="T67" id="Seg_919" s="T66">botfly-PL.[NOM]</ta>
            <ta e="T68" id="Seg_920" s="T67">(s)he-EP-PL.[NOM]</ta>
            <ta e="T69" id="Seg_921" s="T68">I.ALL</ta>
            <ta e="T70" id="Seg_922" s="T69">NEG</ta>
            <ta e="T71" id="Seg_923" s="T70">bring-PST-3PL</ta>
            <ta e="T72" id="Seg_924" s="T71">work-INF</ta>
            <ta e="T73" id="Seg_925" s="T72">work-INF</ta>
            <ta e="T74" id="Seg_926" s="T73">I.NOM</ta>
            <ta e="T75" id="Seg_927" s="T74">go.away-PST-1SG.S</ta>
            <ta e="T76" id="Seg_928" s="T75">house-ILL</ta>
            <ta e="T77" id="Seg_929" s="T76">lie-PFV-PST-3SG.S</ta>
            <ta e="T78" id="Seg_930" s="T77">NEG</ta>
            <ta e="T79" id="Seg_931" s="T78">long</ta>
            <ta e="T80" id="Seg_932" s="T79">place-GEN</ta>
            <ta e="T81" id="Seg_933" s="T80">top-EP-ADV.LOC</ta>
            <ta e="T82" id="Seg_934" s="T81">then</ta>
            <ta e="T83" id="Seg_935" s="T82">I.NOM</ta>
            <ta e="T84" id="Seg_936" s="T83">burn-PST-1SG.O</ta>
            <ta e="T85" id="Seg_937" s="T84">fire-%%-ACC</ta>
            <ta e="T86" id="Seg_938" s="T85">cook-FRQ-EP-PST-1SG.O</ta>
            <ta e="T87" id="Seg_939" s="T86">cedar-GEN</ta>
            <ta e="T88" id="Seg_940" s="T87">cone-head-PL-ACC</ta>
            <ta e="T89" id="Seg_941" s="T88">child-PL.[NOM]</ta>
            <ta e="T90" id="Seg_942" s="T89">all</ta>
            <ta e="T91" id="Seg_943" s="T90">carry-DUR-IPFV-3PL</ta>
            <ta e="T92" id="Seg_944" s="T91">eat-PST.NAR-3PL</ta>
            <ta e="T93" id="Seg_945" s="T92">I.NOM</ta>
            <ta e="T94" id="Seg_946" s="T93">perch-PL-ACC</ta>
            <ta e="T95" id="Seg_947" s="T94">eat-CVB2</ta>
            <ta e="T96" id="Seg_948" s="T95">trousers-PL</ta>
            <ta e="T97" id="Seg_949" s="T96">take-RES-1SG.O</ta>
            <ta e="T98" id="Seg_950" s="T97">%%-PST.NAR-3PL</ta>
            <ta e="T99" id="Seg_951" s="T98">%%</ta>
            <ta e="T100" id="Seg_952" s="T99">eat-EP-PST.NAR-3SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_953" s="T1">вчера</ta>
            <ta e="T3" id="Seg_954" s="T2">утро-ADJZ</ta>
            <ta e="T4" id="Seg_955" s="T3">нечто-LOC</ta>
            <ta e="T5" id="Seg_956" s="T4">я.NOM</ta>
            <ta e="T6" id="Seg_957" s="T5">встать-PST-1SG.S</ta>
            <ta e="T7" id="Seg_958" s="T6">восемь</ta>
            <ta e="T8" id="Seg_959" s="T7">двенадцать</ta>
            <ta e="T9" id="Seg_960" s="T8">час-EL</ta>
            <ta e="T10" id="Seg_961" s="T9">я.NOM</ta>
            <ta e="T11" id="Seg_962" s="T10">мыться-PFV-PST-1SG.S</ta>
            <ta e="T12" id="Seg_963" s="T11">замерзнуть-PTCP.PST</ta>
            <ta e="T13" id="Seg_964" s="T12">вода-INSTR</ta>
            <ta e="T14" id="Seg_965" s="T13">мыло-INSTR</ta>
            <ta e="T15" id="Seg_966" s="T14">прочь</ta>
            <ta e="T16" id="Seg_967" s="T15">вытереть-PST-1SG.S</ta>
            <ta e="T17" id="Seg_968" s="T16">полотенце-INSTR</ta>
            <ta e="T18" id="Seg_969" s="T17">волос-ACC</ta>
            <ta e="T19" id="Seg_970" s="T18">расцарапать-PST-1SG.O</ta>
            <ta e="T20" id="Seg_971" s="T19">гребень-INSTR</ta>
            <ta e="T21" id="Seg_972" s="T20">волос-ACC</ta>
            <ta e="T22" id="Seg_973" s="T21">заплетать-PST-1SG.O</ta>
            <ta e="T23" id="Seg_974" s="T22">одеться-PST-1SG.O</ta>
            <ta e="T24" id="Seg_975" s="T23">платье-нечто-ACC</ta>
            <ta e="T25" id="Seg_976" s="T24">быть-PST-3SG.S</ta>
            <ta e="T26" id="Seg_977" s="T25">новый</ta>
            <ta e="T27" id="Seg_978" s="T26">платье.[NOM]-1SG</ta>
            <ta e="T28" id="Seg_979" s="T27">я.NOM</ta>
            <ta e="T29" id="Seg_980" s="T28">уйти-PST-1SG.S</ta>
            <ta e="T30" id="Seg_981" s="T29">собирать-FRQ-EP-INF</ta>
            <ta e="T31" id="Seg_982" s="T30">картошка.[NOM]</ta>
            <ta e="T32" id="Seg_983" s="T31">здесь</ta>
            <ta e="T33" id="Seg_984" s="T32">быть-PST-3SG.S</ta>
            <ta e="T34" id="Seg_985" s="T33">много</ta>
            <ta e="T35" id="Seg_986" s="T34">комар.[NOM]</ta>
            <ta e="T36" id="Seg_987" s="T35">думать-PST-1SG.S</ta>
            <ta e="T37" id="Seg_988" s="T36">одеться-FUT-1SG.O</ta>
            <ta e="T38" id="Seg_989" s="T37">чирки-PL-ACC</ta>
            <ta e="T39" id="Seg_990" s="T38">комар-PL.[NOM]</ta>
            <ta e="T40" id="Seg_991" s="T39">я.ACC</ta>
            <ta e="T41" id="Seg_992" s="T40">съесть-IPFV3-3PL</ta>
            <ta e="T42" id="Seg_993" s="T41">и</ta>
            <ta e="T43" id="Seg_994" s="T42">одеться-PST-1SG.S</ta>
            <ta e="T44" id="Seg_995" s="T43">чирки-PL-ACC</ta>
            <ta e="T45" id="Seg_996" s="T44">портянка-EP-PL-INSTR</ta>
            <ta e="T46" id="Seg_997" s="T45">чирки-PL.[NOM]</ta>
            <ta e="T47" id="Seg_998" s="T46">нога-ACC</ta>
            <ta e="T48" id="Seg_999" s="T47">%%-FRQ-PST.NAR-3PL</ta>
            <ta e="T49" id="Seg_1000" s="T48">я.NOM</ta>
            <ta e="T50" id="Seg_1001" s="T49">нога-PL-ACC</ta>
            <ta e="T51" id="Seg_1002" s="T50">намозолить-PST-1SG.O</ta>
            <ta e="T52" id="Seg_1003" s="T51">ты.NOM</ta>
            <ta e="T53" id="Seg_1004" s="T52">NEG</ta>
            <ta e="T54" id="Seg_1005" s="T53">намозолить-IPFV3-FUT-2SG.O</ta>
            <ta e="T55" id="Seg_1006" s="T54">нога-PL-ACC-3DU</ta>
            <ta e="T56" id="Seg_1007" s="T55">быть-PST-3SG.S</ta>
            <ta e="T57" id="Seg_1008" s="T56">очень</ta>
            <ta e="T58" id="Seg_1009" s="T57">жаркий</ta>
            <ta e="T59" id="Seg_1010" s="T58">день.[NOM]-3SG</ta>
            <ta e="T60" id="Seg_1011" s="T59">я.NOM</ta>
            <ta e="T61" id="Seg_1012" s="T60">очень</ta>
            <ta e="T62" id="Seg_1013" s="T61">любить-1SG.S</ta>
            <ta e="T63" id="Seg_1014" s="T62">брат-ACC</ta>
            <ta e="T64" id="Seg_1015" s="T63">потом</ta>
            <ta e="T65" id="Seg_1016" s="T64">встать-RES-PST-3PL</ta>
            <ta e="T66" id="Seg_1017" s="T65">лось-ADJZ</ta>
            <ta e="T67" id="Seg_1018" s="T66">оводы-PL.[NOM]</ta>
            <ta e="T68" id="Seg_1019" s="T67">он(а)-EP-PL.[NOM]</ta>
            <ta e="T69" id="Seg_1020" s="T68">я.ALL</ta>
            <ta e="T70" id="Seg_1021" s="T69">NEG</ta>
            <ta e="T71" id="Seg_1022" s="T70">принести-PST-3PL</ta>
            <ta e="T72" id="Seg_1023" s="T71">работать-INF</ta>
            <ta e="T73" id="Seg_1024" s="T72">работать-INF</ta>
            <ta e="T74" id="Seg_1025" s="T73">я.NOM</ta>
            <ta e="T75" id="Seg_1026" s="T74">уйти-PST-1SG.S</ta>
            <ta e="T76" id="Seg_1027" s="T75">дом-ILL</ta>
            <ta e="T77" id="Seg_1028" s="T76">лежать-PFV-PST-3SG.S</ta>
            <ta e="T78" id="Seg_1029" s="T77">NEG</ta>
            <ta e="T79" id="Seg_1030" s="T78">долго</ta>
            <ta e="T80" id="Seg_1031" s="T79">место-GEN</ta>
            <ta e="T81" id="Seg_1032" s="T80">верх-EP-ADV.LOC</ta>
            <ta e="T82" id="Seg_1033" s="T81">потом</ta>
            <ta e="T83" id="Seg_1034" s="T82">я.NOM</ta>
            <ta e="T84" id="Seg_1035" s="T83">гореть-PST-1SG.O</ta>
            <ta e="T85" id="Seg_1036" s="T84">огонь-%%-ACC</ta>
            <ta e="T86" id="Seg_1037" s="T85">сварить-FRQ-EP-PST-1SG.O</ta>
            <ta e="T87" id="Seg_1038" s="T86">кедр-GEN</ta>
            <ta e="T88" id="Seg_1039" s="T87">шишка-голова-PL-ACC</ta>
            <ta e="T89" id="Seg_1040" s="T88">ребёнок-PL.[NOM]</ta>
            <ta e="T90" id="Seg_1041" s="T89">весь</ta>
            <ta e="T91" id="Seg_1042" s="T90">таскать-DUR-IPFV-3PL</ta>
            <ta e="T92" id="Seg_1043" s="T91">съесть-PST.NAR-3PL</ta>
            <ta e="T93" id="Seg_1044" s="T92">я.NOM</ta>
            <ta e="T94" id="Seg_1045" s="T93">окунь-PL-ACC</ta>
            <ta e="T95" id="Seg_1046" s="T94">съесть-CVB2</ta>
            <ta e="T96" id="Seg_1047" s="T95">штаны-PL</ta>
            <ta e="T97" id="Seg_1048" s="T96">взять-RES-1SG.O</ta>
            <ta e="T98" id="Seg_1049" s="T97">%%-PST.NAR-3PL</ta>
            <ta e="T99" id="Seg_1050" s="T98">%%</ta>
            <ta e="T100" id="Seg_1051" s="T99">съесть-EP-PST.NAR-3SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1052" s="T1">adv</ta>
            <ta e="T3" id="Seg_1053" s="T2">n-n&gt;adj</ta>
            <ta e="T4" id="Seg_1054" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_1055" s="T4">pers</ta>
            <ta e="T6" id="Seg_1056" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_1057" s="T6">num</ta>
            <ta e="T8" id="Seg_1058" s="T7">num</ta>
            <ta e="T9" id="Seg_1059" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_1060" s="T9">pers</ta>
            <ta e="T11" id="Seg_1061" s="T10">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_1062" s="T11">v-v&gt;ptcp</ta>
            <ta e="T13" id="Seg_1063" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_1064" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_1065" s="T14">preverb</ta>
            <ta e="T16" id="Seg_1066" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_1067" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_1068" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_1069" s="T18">v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_1070" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_1071" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_1072" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_1073" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_1074" s="T23">n-n-n:case</ta>
            <ta e="T25" id="Seg_1075" s="T24">v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_1076" s="T25">adj</ta>
            <ta e="T27" id="Seg_1077" s="T26">n.[n:case]-n:poss</ta>
            <ta e="T28" id="Seg_1078" s="T27">pers</ta>
            <ta e="T29" id="Seg_1079" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_1080" s="T29">v-v&gt;v-n:ins-v:inf</ta>
            <ta e="T31" id="Seg_1081" s="T30">n.[n:case]</ta>
            <ta e="T32" id="Seg_1082" s="T31">adv</ta>
            <ta e="T33" id="Seg_1083" s="T32">v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_1084" s="T33">quant</ta>
            <ta e="T35" id="Seg_1085" s="T34">n.[n:case]</ta>
            <ta e="T36" id="Seg_1086" s="T35">v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_1087" s="T36">v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_1088" s="T37">n-n:num-n:case</ta>
            <ta e="T39" id="Seg_1089" s="T38">n-n:num.[n:case]</ta>
            <ta e="T40" id="Seg_1090" s="T39">pers</ta>
            <ta e="T41" id="Seg_1091" s="T40">v-v&gt;v-v:pn</ta>
            <ta e="T42" id="Seg_1092" s="T41">conj</ta>
            <ta e="T43" id="Seg_1093" s="T42">v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_1094" s="T43">n-n:num-n:case</ta>
            <ta e="T45" id="Seg_1095" s="T44">n-n:ins-n:num-n:case</ta>
            <ta e="T46" id="Seg_1096" s="T45">n-n:num.[n:case]</ta>
            <ta e="T47" id="Seg_1097" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_1098" s="T47">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_1099" s="T48">pers</ta>
            <ta e="T50" id="Seg_1100" s="T49">n-n:num-n:case</ta>
            <ta e="T51" id="Seg_1101" s="T50">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_1102" s="T51">pers</ta>
            <ta e="T53" id="Seg_1103" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_1104" s="T53">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_1105" s="T54">n-n:num-n:case-n:poss</ta>
            <ta e="T56" id="Seg_1106" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_1107" s="T56">adv</ta>
            <ta e="T58" id="Seg_1108" s="T57">adj</ta>
            <ta e="T59" id="Seg_1109" s="T58">n.[n:case]-n:poss</ta>
            <ta e="T60" id="Seg_1110" s="T59">pers</ta>
            <ta e="T61" id="Seg_1111" s="T60">adv</ta>
            <ta e="T62" id="Seg_1112" s="T61">v-v:pn</ta>
            <ta e="T63" id="Seg_1113" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_1114" s="T63">adv</ta>
            <ta e="T65" id="Seg_1115" s="T64">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T66" id="Seg_1116" s="T65">n-n&gt;adj</ta>
            <ta e="T67" id="Seg_1117" s="T66">n-n:num.[n:case]</ta>
            <ta e="T68" id="Seg_1118" s="T67">pers-n:ins-n:num.[n:case]</ta>
            <ta e="T69" id="Seg_1119" s="T68">pers</ta>
            <ta e="T70" id="Seg_1120" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_1121" s="T70">v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_1122" s="T71">v-v:inf</ta>
            <ta e="T73" id="Seg_1123" s="T72">v-v:inf</ta>
            <ta e="T74" id="Seg_1124" s="T73">pers</ta>
            <ta e="T75" id="Seg_1125" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_1126" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_1127" s="T76">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_1128" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_1129" s="T78">adv</ta>
            <ta e="T80" id="Seg_1130" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_1131" s="T80">n-n:ins-adv:case</ta>
            <ta e="T82" id="Seg_1132" s="T81">adv</ta>
            <ta e="T83" id="Seg_1133" s="T82">pers</ta>
            <ta e="T84" id="Seg_1134" s="T83">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_1135" s="T84">n-n&gt;n-n:case</ta>
            <ta e="T86" id="Seg_1136" s="T85">v-v&gt;v-n:ins-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_1137" s="T86">n-n:case</ta>
            <ta e="T88" id="Seg_1138" s="T87">n-n-n:num-n:case</ta>
            <ta e="T89" id="Seg_1139" s="T88">n-n:num.[n:case]</ta>
            <ta e="T90" id="Seg_1140" s="T89">quant</ta>
            <ta e="T91" id="Seg_1141" s="T90">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T92" id="Seg_1142" s="T91">v-v:tense-v:pn</ta>
            <ta e="T93" id="Seg_1143" s="T92">pers</ta>
            <ta e="T94" id="Seg_1144" s="T93">n-n:num-n:case</ta>
            <ta e="T95" id="Seg_1145" s="T94">v-v&gt;adv</ta>
            <ta e="T96" id="Seg_1146" s="T95">n-n:num</ta>
            <ta e="T97" id="Seg_1147" s="T96">v-v&gt;v-v:pn</ta>
            <ta e="T98" id="Seg_1148" s="T97">%%-v:tense-v:pn</ta>
            <ta e="T99" id="Seg_1149" s="T98">%%</ta>
            <ta e="T100" id="Seg_1150" s="T99">v-n:ins-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1151" s="T1">adv</ta>
            <ta e="T3" id="Seg_1152" s="T2">adj</ta>
            <ta e="T4" id="Seg_1153" s="T3">n</ta>
            <ta e="T5" id="Seg_1154" s="T4">pers</ta>
            <ta e="T6" id="Seg_1155" s="T5">v</ta>
            <ta e="T7" id="Seg_1156" s="T6">num</ta>
            <ta e="T8" id="Seg_1157" s="T7">num</ta>
            <ta e="T9" id="Seg_1158" s="T8">n</ta>
            <ta e="T10" id="Seg_1159" s="T9">pers</ta>
            <ta e="T11" id="Seg_1160" s="T10">v</ta>
            <ta e="T12" id="Seg_1161" s="T11">ptcp</ta>
            <ta e="T13" id="Seg_1162" s="T12">n</ta>
            <ta e="T14" id="Seg_1163" s="T13">n</ta>
            <ta e="T15" id="Seg_1164" s="T14">preverb</ta>
            <ta e="T16" id="Seg_1165" s="T15">v</ta>
            <ta e="T17" id="Seg_1166" s="T16">n</ta>
            <ta e="T18" id="Seg_1167" s="T17">n</ta>
            <ta e="T19" id="Seg_1168" s="T18">v</ta>
            <ta e="T20" id="Seg_1169" s="T19">n</ta>
            <ta e="T21" id="Seg_1170" s="T20">n</ta>
            <ta e="T22" id="Seg_1171" s="T21">v</ta>
            <ta e="T23" id="Seg_1172" s="T22">v</ta>
            <ta e="T24" id="Seg_1173" s="T23">n</ta>
            <ta e="T25" id="Seg_1174" s="T24">n</ta>
            <ta e="T26" id="Seg_1175" s="T25">adj</ta>
            <ta e="T27" id="Seg_1176" s="T26">n</ta>
            <ta e="T28" id="Seg_1177" s="T27">pers</ta>
            <ta e="T29" id="Seg_1178" s="T28">v</ta>
            <ta e="T30" id="Seg_1179" s="T29">v</ta>
            <ta e="T31" id="Seg_1180" s="T30">n</ta>
            <ta e="T32" id="Seg_1181" s="T31">adv</ta>
            <ta e="T33" id="Seg_1182" s="T32">v</ta>
            <ta e="T34" id="Seg_1183" s="T33">quant</ta>
            <ta e="T35" id="Seg_1184" s="T34">n</ta>
            <ta e="T36" id="Seg_1185" s="T35">v</ta>
            <ta e="T37" id="Seg_1186" s="T36">v</ta>
            <ta e="T38" id="Seg_1187" s="T37">n</ta>
            <ta e="T39" id="Seg_1188" s="T38">n</ta>
            <ta e="T40" id="Seg_1189" s="T39">pers</ta>
            <ta e="T41" id="Seg_1190" s="T40">v</ta>
            <ta e="T42" id="Seg_1191" s="T41">conj</ta>
            <ta e="T43" id="Seg_1192" s="T42">v</ta>
            <ta e="T44" id="Seg_1193" s="T43">n</ta>
            <ta e="T45" id="Seg_1194" s="T44">n</ta>
            <ta e="T46" id="Seg_1195" s="T45">n</ta>
            <ta e="T47" id="Seg_1196" s="T46">n</ta>
            <ta e="T48" id="Seg_1197" s="T47">v</ta>
            <ta e="T49" id="Seg_1198" s="T48">pers</ta>
            <ta e="T50" id="Seg_1199" s="T49">n</ta>
            <ta e="T51" id="Seg_1200" s="T50">v</ta>
            <ta e="T52" id="Seg_1201" s="T51">pers</ta>
            <ta e="T53" id="Seg_1202" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_1203" s="T53">v</ta>
            <ta e="T55" id="Seg_1204" s="T54">n</ta>
            <ta e="T56" id="Seg_1205" s="T55">v</ta>
            <ta e="T57" id="Seg_1206" s="T56">adv</ta>
            <ta e="T58" id="Seg_1207" s="T57">adj</ta>
            <ta e="T59" id="Seg_1208" s="T58">n</ta>
            <ta e="T60" id="Seg_1209" s="T59">pers</ta>
            <ta e="T61" id="Seg_1210" s="T60">adv</ta>
            <ta e="T62" id="Seg_1211" s="T61">v</ta>
            <ta e="T63" id="Seg_1212" s="T62">n</ta>
            <ta e="T64" id="Seg_1213" s="T63">adv</ta>
            <ta e="T65" id="Seg_1214" s="T64">v</ta>
            <ta e="T66" id="Seg_1215" s="T65">adj</ta>
            <ta e="T67" id="Seg_1216" s="T66">n</ta>
            <ta e="T68" id="Seg_1217" s="T67">pers</ta>
            <ta e="T69" id="Seg_1218" s="T68">pers</ta>
            <ta e="T70" id="Seg_1219" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_1220" s="T70">v</ta>
            <ta e="T72" id="Seg_1221" s="T71">v</ta>
            <ta e="T73" id="Seg_1222" s="T72">v</ta>
            <ta e="T74" id="Seg_1223" s="T73">pers</ta>
            <ta e="T75" id="Seg_1224" s="T74">v</ta>
            <ta e="T76" id="Seg_1225" s="T75">n</ta>
            <ta e="T77" id="Seg_1226" s="T76">v</ta>
            <ta e="T78" id="Seg_1227" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_1228" s="T78">adv</ta>
            <ta e="T80" id="Seg_1229" s="T79">n</ta>
            <ta e="T81" id="Seg_1230" s="T80">n</ta>
            <ta e="T82" id="Seg_1231" s="T81">adv</ta>
            <ta e="T83" id="Seg_1232" s="T82">pers</ta>
            <ta e="T84" id="Seg_1233" s="T83">v</ta>
            <ta e="T85" id="Seg_1234" s="T84">n</ta>
            <ta e="T86" id="Seg_1235" s="T85">v</ta>
            <ta e="T87" id="Seg_1236" s="T86">n</ta>
            <ta e="T88" id="Seg_1237" s="T87">n</ta>
            <ta e="T89" id="Seg_1238" s="T88">n</ta>
            <ta e="T90" id="Seg_1239" s="T89">quant</ta>
            <ta e="T91" id="Seg_1240" s="T90">v</ta>
            <ta e="T92" id="Seg_1241" s="T91">v</ta>
            <ta e="T93" id="Seg_1242" s="T92">pers</ta>
            <ta e="T94" id="Seg_1243" s="T93">n</ta>
            <ta e="T95" id="Seg_1244" s="T94">adv</ta>
            <ta e="T96" id="Seg_1245" s="T95">n</ta>
            <ta e="T97" id="Seg_1246" s="T96">v</ta>
            <ta e="T100" id="Seg_1247" s="T99">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T5" id="Seg_1248" s="T4">pro.h:S</ta>
            <ta e="T6" id="Seg_1249" s="T5">v:pred</ta>
            <ta e="T10" id="Seg_1250" s="T9">pro.h:S</ta>
            <ta e="T11" id="Seg_1251" s="T10">v:pred</ta>
            <ta e="T16" id="Seg_1252" s="T15">0.1.h:S v:pred</ta>
            <ta e="T18" id="Seg_1253" s="T17">np:O</ta>
            <ta e="T19" id="Seg_1254" s="T18">0.1.h:S v:pred</ta>
            <ta e="T21" id="Seg_1255" s="T20">np:O</ta>
            <ta e="T22" id="Seg_1256" s="T21">0.1.h:S v:pred</ta>
            <ta e="T23" id="Seg_1257" s="T22">0.1.h:S v:pred</ta>
            <ta e="T25" id="Seg_1258" s="T24">0.3:S cop</ta>
            <ta e="T27" id="Seg_1259" s="T26">n:pred</ta>
            <ta e="T28" id="Seg_1260" s="T27">pro.h:S</ta>
            <ta e="T29" id="Seg_1261" s="T28">v:pred</ta>
            <ta e="T31" id="Seg_1262" s="T29">s:purp</ta>
            <ta e="T33" id="Seg_1263" s="T32">v:pred</ta>
            <ta e="T35" id="Seg_1264" s="T34">np:S</ta>
            <ta e="T36" id="Seg_1265" s="T35">0.1.h:S v:pred</ta>
            <ta e="T41" id="Seg_1266" s="T36">s:compl</ta>
            <ta e="T43" id="Seg_1267" s="T42">0.1.h:S v:pred</ta>
            <ta e="T44" id="Seg_1268" s="T43">np:O</ta>
            <ta e="T46" id="Seg_1269" s="T45">np:S</ta>
            <ta e="T47" id="Seg_1270" s="T46">np:O</ta>
            <ta e="T48" id="Seg_1271" s="T47">v:pred</ta>
            <ta e="T49" id="Seg_1272" s="T48">pro.h:S</ta>
            <ta e="T50" id="Seg_1273" s="T49">np:O</ta>
            <ta e="T51" id="Seg_1274" s="T50">v:pred</ta>
            <ta e="T52" id="Seg_1275" s="T51">pro.h:S</ta>
            <ta e="T54" id="Seg_1276" s="T53">v:pred</ta>
            <ta e="T55" id="Seg_1277" s="T54">np:O</ta>
            <ta e="T56" id="Seg_1278" s="T55">0.3:S cop</ta>
            <ta e="T59" id="Seg_1279" s="T58">n:pred</ta>
            <ta e="T60" id="Seg_1280" s="T59">pro.h:S</ta>
            <ta e="T62" id="Seg_1281" s="T61">v:pred</ta>
            <ta e="T63" id="Seg_1282" s="T62">np.h:O</ta>
            <ta e="T65" id="Seg_1283" s="T64">v:pred</ta>
            <ta e="T67" id="Seg_1284" s="T66">np:S</ta>
            <ta e="T68" id="Seg_1285" s="T67">pro:S</ta>
            <ta e="T71" id="Seg_1286" s="T70">v:pred</ta>
            <ta e="T74" id="Seg_1287" s="T73">pro.h:S</ta>
            <ta e="T75" id="Seg_1288" s="T74">v:pred</ta>
            <ta e="T77" id="Seg_1289" s="T76">0.1.h:S v:pred</ta>
            <ta e="T83" id="Seg_1290" s="T82">pro.h:S</ta>
            <ta e="T84" id="Seg_1291" s="T83">v:pred</ta>
            <ta e="T85" id="Seg_1292" s="T84">np:O</ta>
            <ta e="T86" id="Seg_1293" s="T85">0.1.h:S v:pred</ta>
            <ta e="T88" id="Seg_1294" s="T87">np:O</ta>
            <ta e="T89" id="Seg_1295" s="T88">np.h:S</ta>
            <ta e="T91" id="Seg_1296" s="T90">v:pred</ta>
            <ta e="T92" id="Seg_1297" s="T91">0.3.h:S v:pred</ta>
            <ta e="T93" id="Seg_1298" s="T92">pro.h:S</ta>
            <ta e="T94" id="Seg_1299" s="T93">np:O</ta>
            <ta e="T97" id="Seg_1300" s="T96">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1301" s="T1">adv:Time</ta>
            <ta e="T5" id="Seg_1302" s="T4">pro.h:A</ta>
            <ta e="T9" id="Seg_1303" s="T8">np:Time</ta>
            <ta e="T10" id="Seg_1304" s="T9">pro.h:A</ta>
            <ta e="T13" id="Seg_1305" s="T12">np:Ins</ta>
            <ta e="T14" id="Seg_1306" s="T13">np:Ins</ta>
            <ta e="T16" id="Seg_1307" s="T15">0.1.h:A</ta>
            <ta e="T17" id="Seg_1308" s="T16">np:Ins</ta>
            <ta e="T18" id="Seg_1309" s="T17">np:P</ta>
            <ta e="T19" id="Seg_1310" s="T18">0.1.h:A</ta>
            <ta e="T20" id="Seg_1311" s="T19">np:Ins</ta>
            <ta e="T21" id="Seg_1312" s="T20">np:P</ta>
            <ta e="T22" id="Seg_1313" s="T21">0.1.h:A</ta>
            <ta e="T23" id="Seg_1314" s="T22">0.1.h:A</ta>
            <ta e="T24" id="Seg_1315" s="T23">np:Th</ta>
            <ta e="T25" id="Seg_1316" s="T24">0.3:Th</ta>
            <ta e="T28" id="Seg_1317" s="T27">pro.h:A</ta>
            <ta e="T30" id="Seg_1318" s="T29">0.1.h:A</ta>
            <ta e="T31" id="Seg_1319" s="T30">np:Th</ta>
            <ta e="T32" id="Seg_1320" s="T31">adv:L</ta>
            <ta e="T35" id="Seg_1321" s="T34">np:Th</ta>
            <ta e="T36" id="Seg_1322" s="T35">0.1.h:E</ta>
            <ta e="T37" id="Seg_1323" s="T36">0.1.h:A</ta>
            <ta e="T38" id="Seg_1324" s="T37">np:Th</ta>
            <ta e="T39" id="Seg_1325" s="T38">np:A</ta>
            <ta e="T40" id="Seg_1326" s="T39">pro.h:P</ta>
            <ta e="T43" id="Seg_1327" s="T42">0.1.h:A</ta>
            <ta e="T44" id="Seg_1328" s="T43">np:Th</ta>
            <ta e="T46" id="Seg_1329" s="T45">np:A</ta>
            <ta e="T47" id="Seg_1330" s="T46">np:P</ta>
            <ta e="T49" id="Seg_1331" s="T48">pro.h:A</ta>
            <ta e="T50" id="Seg_1332" s="T49">np:P</ta>
            <ta e="T52" id="Seg_1333" s="T51">pro.h:A</ta>
            <ta e="T55" id="Seg_1334" s="T54">np:P</ta>
            <ta e="T56" id="Seg_1335" s="T55">0.3:Th</ta>
            <ta e="T60" id="Seg_1336" s="T59">pro.h:E</ta>
            <ta e="T63" id="Seg_1337" s="T62">np.h:Th</ta>
            <ta e="T64" id="Seg_1338" s="T63">adv:Time</ta>
            <ta e="T67" id="Seg_1339" s="T66">np:A</ta>
            <ta e="T68" id="Seg_1340" s="T67">pro:A</ta>
            <ta e="T74" id="Seg_1341" s="T73">pro.h:A</ta>
            <ta e="T76" id="Seg_1342" s="T75">np:G</ta>
            <ta e="T77" id="Seg_1343" s="T76">0.1.h:Th</ta>
            <ta e="T80" id="Seg_1344" s="T79">np:Poss</ta>
            <ta e="T81" id="Seg_1345" s="T80">adv:L</ta>
            <ta e="T82" id="Seg_1346" s="T81">adv:Time</ta>
            <ta e="T83" id="Seg_1347" s="T82">pro.h:A</ta>
            <ta e="T85" id="Seg_1348" s="T84">np:P</ta>
            <ta e="T86" id="Seg_1349" s="T85">0.1.h:A</ta>
            <ta e="T87" id="Seg_1350" s="T86">np:Poss</ta>
            <ta e="T88" id="Seg_1351" s="T87">np:P</ta>
            <ta e="T89" id="Seg_1352" s="T88">np.h:A</ta>
            <ta e="T92" id="Seg_1353" s="T91">0.3.h:A</ta>
            <ta e="T93" id="Seg_1354" s="T92">pro.h:A</ta>
            <ta e="T94" id="Seg_1355" s="T93">np:P</ta>
            <ta e="T97" id="Seg_1356" s="T96">0.1.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T9" id="Seg_1357" s="T8">RUS:cult</ta>
            <ta e="T14" id="Seg_1358" s="T13">RUS:cult</ta>
            <ta e="T31" id="Seg_1359" s="T30">RUS:cult</ta>
            <ta e="T42" id="Seg_1360" s="T41">RUS:gram</ta>
            <ta e="T90" id="Seg_1361" s="T89">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T31" id="Seg_1362" s="T30">Csub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T9" id="Seg_1363" s="T8">dir:infl</ta>
            <ta e="T14" id="Seg_1364" s="T13">dir:infl</ta>
            <ta e="T31" id="Seg_1365" s="T30">dir:bare</ta>
            <ta e="T42" id="Seg_1366" s="T41">dir:bare</ta>
            <ta e="T90" id="Seg_1367" s="T89">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T9" id="Seg_1368" s="T1">Yesterday morning, I got up at eight (twelve) o'clock.</ta>
            <ta e="T17" id="Seg_1369" s="T9">I washed myself with cold water and soap, wiped myself with a towel.</ta>
            <ta e="T24" id="Seg_1370" s="T17">I combed my hair with a comb, I plaited my hair and put on a dress.</ta>
            <ta e="T27" id="Seg_1371" s="T24">It was a new dress.</ta>
            <ta e="T31" id="Seg_1372" s="T27">I went to earth up potato.</ta>
            <ta e="T35" id="Seg_1373" s="T31">There were many mosquitos there.</ta>
            <ta e="T41" id="Seg_1374" s="T35">I thought: I should put on my boots, [otherwise] mosquitos will eat me.</ta>
            <ta e="T45" id="Seg_1375" s="T41">I put on boots with footwraps.</ta>
            <ta e="T48" id="Seg_1376" s="T45">The boots are tight.</ta>
            <ta e="T51" id="Seg_1377" s="T48">My feet became callous.</ta>
            <ta e="T55" id="Seg_1378" s="T51">(Your feet won't get callous.)</ta>
            <ta e="T59" id="Seg_1379" s="T55">The day was very hot.</ta>
            <ta e="T63" id="Seg_1380" s="T59">(I love my brother very much.)</ta>
            <ta e="T67" id="Seg_1381" s="T63">Then gadflies came flying.</ta>
            <ta e="T73" id="Seg_1382" s="T67">They didn't let me work.</ta>
            <ta e="T76" id="Seg_1383" s="T73">I went home.</ta>
            <ta e="T81" id="Seg_1384" s="T76">I lay down for a while.</ta>
            <ta e="T88" id="Seg_1385" s="T81">Then I made fire and cooked pine cones.</ta>
            <ta e="T92" id="Seg_1386" s="T88">The children took and ate them all.</ta>
            <ta e="T100" id="Seg_1387" s="T92">I ate a perch, the middle of my trousers got unripped (?).</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T9" id="Seg_1388" s="T1">Gestern morgen stand ich um acht (zwölf) Uhr auf.</ta>
            <ta e="T17" id="Seg_1389" s="T9">Ich wusch mich mit kaltem Wasser und Seife, trocknete mich mit einem Handtuch ab.</ta>
            <ta e="T24" id="Seg_1390" s="T17">Ich kämmte mein Haar mit einem Kamm, ich flocht mein Haar und zog ein Kleid an.</ta>
            <ta e="T27" id="Seg_1391" s="T24">Es war ein neues Kleid.</ta>
            <ta e="T31" id="Seg_1392" s="T27">Ich ging, um Kartoffeln auszugraben.</ta>
            <ta e="T35" id="Seg_1393" s="T31">Es gibt viele Moskitos dort.</ta>
            <ta e="T41" id="Seg_1394" s="T35">Ich dachte: Ich sollte meine Stiefel anziehen, [sonst] fressen mich die Moskitos.</ta>
            <ta e="T45" id="Seg_1395" s="T41">Ich zog Stiefel mit Fußlappen an.</ta>
            <ta e="T48" id="Seg_1396" s="T45">Die Stiefel sind eng.</ta>
            <ta e="T51" id="Seg_1397" s="T48">Meine Füße wurden schwielig.</ta>
            <ta e="T55" id="Seg_1398" s="T51">(Deine Füße werden nicht schwielig werden.)</ta>
            <ta e="T59" id="Seg_1399" s="T55">Es war ein sehr heißer Tag.</ta>
            <ta e="T63" id="Seg_1400" s="T59">(Ich liebe meinen Bruder sehr.)</ta>
            <ta e="T67" id="Seg_1401" s="T63">Dann kamen Bremsen angeflogen.</ta>
            <ta e="T73" id="Seg_1402" s="T67">Sie ließen mich nicht arbeiten.</ta>
            <ta e="T76" id="Seg_1403" s="T73">Ich ging nach Hause.</ta>
            <ta e="T81" id="Seg_1404" s="T76">Ich legte mich für eine Weile hin.</ta>
            <ta e="T88" id="Seg_1405" s="T81">Dann machte ich Feuer und kochte Pinienzapfen.</ta>
            <ta e="T92" id="Seg_1406" s="T88">Die Kinder nahmen sie und aßen sie alle auf.</ta>
            <ta e="T100" id="Seg_1407" s="T92">Ich aß Barsch, die Mitte meiner Hosen zerriss (?).</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T9" id="Seg_1408" s="T1">Вчера утром я встала в восемь (двенадцать) часов.</ta>
            <ta e="T17" id="Seg_1409" s="T9">Я умылась холодной водой с мылом, вытерлась полотенцем.</ta>
            <ta e="T24" id="Seg_1410" s="T17">Волосы причесала гребнем, волосы заплела, надела платье.</ta>
            <ta e="T27" id="Seg_1411" s="T24">Это было новое платье.</ta>
            <ta e="T31" id="Seg_1412" s="T27">Я пошла загребать (окучивать) картошку.</ta>
            <ta e="T35" id="Seg_1413" s="T31">Там было много комаров.</ta>
            <ta e="T41" id="Seg_1414" s="T35">Я подумала: надену чарки, комары меня съедят.</ta>
            <ta e="T45" id="Seg_1415" s="T41">Надела чарки с портянками.</ta>
            <ta e="T48" id="Seg_1416" s="T45">Чарки ноги жмут.</ta>
            <ta e="T51" id="Seg_1417" s="T48">Я ноги намозолила.</ta>
            <ta e="T55" id="Seg_1418" s="T51">(Ты не намозолишь ноги.)</ta>
            <ta e="T59" id="Seg_1419" s="T55">Был очень жаркий день.</ta>
            <ta e="T63" id="Seg_1420" s="T59">(Я очень люблю брата.)</ta>
            <ta e="T67" id="Seg_1421" s="T63">Потом налетели оводы.</ta>
            <ta e="T73" id="Seg_1422" s="T67">Они мне не дали работать.</ta>
            <ta e="T76" id="Seg_1423" s="T73">Я пошла домой.</ta>
            <ta e="T81" id="Seg_1424" s="T76">Полежала недолго на кровати.</ta>
            <ta e="T88" id="Seg_1425" s="T81">Потом я разожгла костер, сварила (нажарила) кедровых шишек.</ta>
            <ta e="T92" id="Seg_1426" s="T88">Ребята все растаскали, съели.</ta>
            <ta e="T100" id="Seg_1427" s="T92">Я съела окуня, штанов середина распоролась (?).</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr" />
         <annotation name="nt" tierref="nt">
            <ta e="T9" id="Seg_1428" s="T1">WNB: I'm not sure that it is in elative. It can be an allomorph of locative.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
