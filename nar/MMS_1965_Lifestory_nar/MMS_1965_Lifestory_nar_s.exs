<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>MMS_196X_Lifestory_nar</transcription-name>
         <referenced-file url="MMS_1965_Lifestory_nar.wav" />
         <referenced-file url="MMS_1965_Lifestory_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">MMS_1965_Lifestory_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">58</ud-information>
            <ud-information attribute-name="# HIAT:w">41</ud-information>
            <ud-information attribute-name="# e">42</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">12</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="MMS">
            <abbreviation>MMS</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" />
         <tli id="T1" time="15.44" />
         <tli id="T2" time="16.792" type="appl" />
         <tli id="T3" time="17.828" type="appl" />
         <tli id="T4" time="18.864" type="appl" />
         <tli id="T5" time="19.9" type="appl" />
         <tli id="T6" time="21.186" type="appl" />
         <tli id="T7" time="21.958" type="appl" />
         <tli id="T8" time="22.731" type="appl" />
         <tli id="T9" time="23.503" type="appl" />
         <tli id="T10" time="27.385" type="appl" />
         <tli id="T11" time="29.457" type="appl" />
         <tli id="T12" time="31.528" type="appl" />
         <tli id="T13" time="33.6" type="appl" />
         <tli id="T14" time="35.672" type="appl" />
         <tli id="T15" time="38.531" type="appl" />
         <tli id="T16" time="39.968" type="appl" />
         <tli id="T17" time="41.405" type="appl" />
         <tli id="T18" time="46.178" type="appl" />
         <tli id="T19" time="47.352" type="appl" />
         <tli id="T20" time="48.526" type="appl" />
         <tli id="T21" time="51.512" type="appl" />
         <tli id="T22" time="52.437" type="appl" />
         <tli id="T23" time="53.361" type="appl" />
         <tli id="T24" time="54.286" type="appl" />
         <tli id="T25" time="55.21" type="appl" />
         <tli id="T26" time="57.906" type="appl" />
         <tli id="T27" time="58.957" type="appl" />
         <tli id="T28" time="60.009" type="appl" />
         <tli id="T29" time="62.364" type="appl" />
         <tli id="T30" time="63.809" type="appl" />
         <tli id="T31" time="65.889" type="appl" />
         <tli id="T32" time="66.86" type="appl" />
         <tli id="T33" time="67.83" type="appl" />
         <tli id="T34" time="69.43" type="appl" />
         <tli id="T35" time="70.364" type="appl" />
         <tli id="T36" time="71.297" type="appl" />
         <tli id="T37" time="74.934" type="appl" />
         <tli id="T38" time="75.718" type="appl" />
         <tli id="T39" time="76.501" type="appl" />
         <tli id="T40" time="77.284" type="appl" />
         <tli id="T41" time="78.068" type="appl" />
         <tli id="T42" time="78.851" type="appl" />
         <tli id="T43" time="80.048" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="MMS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T42" id="Seg_0" n="sc" s="T0">
               <ts e="T1" id="Seg_2" n="HIAT:u" s="T0">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <nts id="Seg_4" n="HIAT:ip">(</nts>
                  <ats e="T1" id="Seg_5" n="HIAT:non-pho" s="T0">NOISE</ats>
                  <nts id="Seg_6" n="HIAT:ip">)</nts>
                  <nts id="Seg_7" n="HIAT:ip">)</nts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T5" id="Seg_11" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_13" n="HIAT:w" s="T1">Tazovska</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_16" n="HIAT:w" s="T2">nɔːn</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_19" n="HIAT:w" s="T3">qənəıːsak</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_22" n="HIAT:w" s="T4">Sovrečʼqan</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_26" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_28" n="HIAT:w" s="T5">Ninɨ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_31" n="HIAT:w" s="T6">šittälʼ</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_34" n="HIAT:w" s="T7">tüsak</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_37" n="HIAT:w" s="T8">Sidorovsqan</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_41" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_43" n="HIAT:w" s="T9">Anäm</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_46" n="HIAT:w" s="T10">quːsɨ</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_49" n="HIAT:w" s="T11">v</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_52" n="HIAT:w" s="T12">1947</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_55" n="HIAT:w" s="T13">godu</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_59" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_61" n="HIAT:w" s="T14">Əsäp</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_64" n="HIAT:w" s="T15">mütoːqɨt</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_67" n="HIAT:w" s="T16">qəssɔːtɨn</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_71" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_73" n="HIAT:w" s="T17">Pumlɔːt</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_76" n="HIAT:w" s="T18">quːsɨ</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_79" n="HIAT:w" s="T19">ilʼčʼam</ts>
                  <nts id="Seg_80" n="HIAT:ip">.</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_83" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_85" n="HIAT:w" s="T20">Ilʼčʼam</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_88" n="HIAT:w" s="T21">quːla</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_91" n="HIAT:w" s="T22">pula</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_94" n="HIAT:w" s="T23">qənəıːsak</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_97" n="HIAT:w" s="T24">učʼitela</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_101" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_103" n="HIAT:w" s="T25">Školaqɨnɨ</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_106" n="HIAT:w" s="T26">tüːsak</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_109" n="HIAT:w" s="T27">Sidorovsqan</ts>
                  <nts id="Seg_110" n="HIAT:ip">.</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_113" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_115" n="HIAT:w" s="T28">Uːčʼisak</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_118" n="HIAT:w" s="T29">kolhozqɨt</ts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_122" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_124" n="HIAT:w" s="T30">Qəːlɨp</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_127" n="HIAT:w" s="T31">qəttɨsam</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_130" n="HIAT:w" s="T32">kət</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_134" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_136" n="HIAT:w" s="T33">Taŋɨt</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_139" n="HIAT:w" s="T34">nüːtɨp</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_142" n="HIAT:w" s="T35">kosimpɨsam</ts>
                  <nts id="Seg_143" n="HIAT:ip">.</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_146" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_148" n="HIAT:w" s="T36">Sovhozqɨt</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_151" n="HIAT:w" s="T37">tıː</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_154" n="HIAT:w" s="T38">man</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_157" n="HIAT:w" s="T39">uːčʼak</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_160" n="HIAT:w" s="T40">olqa</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_163" n="HIAT:w" s="T41">uːtɨlpoqɨt</ts>
                  <nts id="Seg_164" n="HIAT:ip">.</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T42" id="Seg_166" n="sc" s="T0">
               <ts e="T1" id="Seg_168" n="e" s="T0">((NOISE)). </ts>
               <ts e="T2" id="Seg_170" n="e" s="T1">Tazovska </ts>
               <ts e="T3" id="Seg_172" n="e" s="T2">nɔːn </ts>
               <ts e="T4" id="Seg_174" n="e" s="T3">qənəıːsak </ts>
               <ts e="T5" id="Seg_176" n="e" s="T4">Sovrečʼqan. </ts>
               <ts e="T6" id="Seg_178" n="e" s="T5">Ninɨ </ts>
               <ts e="T7" id="Seg_180" n="e" s="T6">šittälʼ </ts>
               <ts e="T8" id="Seg_182" n="e" s="T7">tüsak </ts>
               <ts e="T9" id="Seg_184" n="e" s="T8">Sidorovsqan. </ts>
               <ts e="T10" id="Seg_186" n="e" s="T9">Anäm </ts>
               <ts e="T11" id="Seg_188" n="e" s="T10">quːsɨ </ts>
               <ts e="T12" id="Seg_190" n="e" s="T11">v </ts>
               <ts e="T13" id="Seg_192" n="e" s="T12">1947 </ts>
               <ts e="T14" id="Seg_194" n="e" s="T13">godu. </ts>
               <ts e="T15" id="Seg_196" n="e" s="T14">Əsäp </ts>
               <ts e="T16" id="Seg_198" n="e" s="T15">mütoːqɨt </ts>
               <ts e="T17" id="Seg_200" n="e" s="T16">qəssɔːtɨn. </ts>
               <ts e="T18" id="Seg_202" n="e" s="T17">Pumlɔːt </ts>
               <ts e="T19" id="Seg_204" n="e" s="T18">quːsɨ </ts>
               <ts e="T20" id="Seg_206" n="e" s="T19">ilʼčʼam. </ts>
               <ts e="T21" id="Seg_208" n="e" s="T20">Ilʼčʼam </ts>
               <ts e="T22" id="Seg_210" n="e" s="T21">quːla </ts>
               <ts e="T23" id="Seg_212" n="e" s="T22">pula </ts>
               <ts e="T24" id="Seg_214" n="e" s="T23">qənəıːsak </ts>
               <ts e="T25" id="Seg_216" n="e" s="T24">učʼitela. </ts>
               <ts e="T26" id="Seg_218" n="e" s="T25">Školaqɨnɨ </ts>
               <ts e="T27" id="Seg_220" n="e" s="T26">tüːsak </ts>
               <ts e="T28" id="Seg_222" n="e" s="T27">Sidorovsqan. </ts>
               <ts e="T29" id="Seg_224" n="e" s="T28">Uːčʼisak </ts>
               <ts e="T30" id="Seg_226" n="e" s="T29">kolhozqɨt. </ts>
               <ts e="T31" id="Seg_228" n="e" s="T30">Qəːlɨp </ts>
               <ts e="T32" id="Seg_230" n="e" s="T31">qəttɨsam </ts>
               <ts e="T33" id="Seg_232" n="e" s="T32">kət. </ts>
               <ts e="T34" id="Seg_234" n="e" s="T33">Taŋɨt </ts>
               <ts e="T35" id="Seg_236" n="e" s="T34">nüːtɨp </ts>
               <ts e="T36" id="Seg_238" n="e" s="T35">kosimpɨsam. </ts>
               <ts e="T37" id="Seg_240" n="e" s="T36">Sovhozqɨt </ts>
               <ts e="T38" id="Seg_242" n="e" s="T37">tıː </ts>
               <ts e="T39" id="Seg_244" n="e" s="T38">man </ts>
               <ts e="T40" id="Seg_246" n="e" s="T39">uːčʼak </ts>
               <ts e="T41" id="Seg_248" n="e" s="T40">olqa </ts>
               <ts e="T42" id="Seg_250" n="e" s="T41">uːtɨlpoqɨt. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T1" id="Seg_251" s="T0">MMS_1965_Lifestory_nar.001 (001)</ta>
            <ta e="T5" id="Seg_252" s="T1">MMS_1965_Lifestory_nar.002 (002)</ta>
            <ta e="T9" id="Seg_253" s="T5">MMS_1965_Lifestory_nar.003 (003)</ta>
            <ta e="T14" id="Seg_254" s="T9">MMS_1965_Lifestory_nar.004 (004)</ta>
            <ta e="T17" id="Seg_255" s="T14">MMS_1965_Lifestory_nar.005 (005)</ta>
            <ta e="T20" id="Seg_256" s="T17">MMS_1965_Lifestory_nar.006 (006)</ta>
            <ta e="T25" id="Seg_257" s="T20">MMS_1965_Lifestory_nar.007 (007)</ta>
            <ta e="T28" id="Seg_258" s="T25">MMS_1965_Lifestory_nar.008 (008)</ta>
            <ta e="T30" id="Seg_259" s="T28">MMS_1965_Lifestory_nar.009 (009)</ta>
            <ta e="T33" id="Seg_260" s="T30">MMS_1965_Lifestory_nar.010 (010)</ta>
            <ta e="T36" id="Seg_261" s="T33">MMS_1965_Lifestory_nar.011 (011)</ta>
            <ta e="T42" id="Seg_262" s="T36">MMS_1965_Lifestory_nar.012 (012)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_263" s="T1">Tazovska nɔːn qənəıːsak Sovrečʼqan.</ta>
            <ta e="T9" id="Seg_264" s="T5">Ninɨ šʼittälʼ tüsak Sidorovsqan.</ta>
            <ta e="T14" id="Seg_265" s="T9">Anäm quːsɨ v 1947 godu.</ta>
            <ta e="T17" id="Seg_266" s="T14">Əsäp mütoːqɨt qəssɔːtɨn.</ta>
            <ta e="T20" id="Seg_267" s="T17">Pumlɔːt quːsɨ ilʼčʼam. </ta>
            <ta e="T25" id="Seg_268" s="T20">Ilʼčʼam quːla pula qənəıːsak učʼitela.</ta>
            <ta e="T28" id="Seg_269" s="T25">Školaqɨnɨ tüːsak Sidorovsqan.</ta>
            <ta e="T30" id="Seg_270" s="T28">Uːčʼisak kolhozqɨt.</ta>
            <ta e="T33" id="Seg_271" s="T30">Qəːlɨp qəttɨsam kət.</ta>
            <ta e="T36" id="Seg_272" s="T33">Taŋɨt nüːtɨp kosimpɨsam.</ta>
            <ta e="T42" id="Seg_273" s="T36">Sovhozqɨt tıː man uːčʼak olqa uːtɨlpoqɨt.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T1" id="Seg_274" s="T0">((NOISE)). </ta>
            <ta e="T5" id="Seg_275" s="T1">Tazovskanɔːn qənəıːsak Sovrečʼqan. </ta>
            <ta e="T9" id="Seg_276" s="T5">Ninɨ šittälʼ tüsak Sidorovsqan. </ta>
            <ta e="T14" id="Seg_277" s="T9">Anäm quːsɨ v 1947 godu. </ta>
            <ta e="T17" id="Seg_278" s="T14">Əsäp mütoːqɨt qəssɔːtɨn. </ta>
            <ta e="T20" id="Seg_279" s="T17">Pumlɔːt quːsɨ ilʼčʼam. </ta>
            <ta e="T25" id="Seg_280" s="T20">Ilʼčʼam quːla pula qənəıːsak učʼitela. </ta>
            <ta e="T28" id="Seg_281" s="T25">Školaqɨnɨ tüːsak Sidorovsqan. </ta>
            <ta e="T30" id="Seg_282" s="T28">Uːčʼisak kolhozqɨt. </ta>
            <ta e="T33" id="Seg_283" s="T30">Qəːlɨp qəttɨsam kət. </ta>
            <ta e="T36" id="Seg_284" s="T33">Taŋɨt nüːtɨp kosimpɨsam. </ta>
            <ta e="T42" id="Seg_285" s="T36">Sovhozqɨt tıː man uːčʼak olqa uːtɨlpoqɨt. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_286" s="T1">Tazovska</ta>
            <ta e="T3" id="Seg_287" s="T2">nɔː-n</ta>
            <ta e="T4" id="Seg_288" s="T3">qən-əıː-sa-k</ta>
            <ta e="T5" id="Seg_289" s="T4">Sovrečʼqa-n.</ta>
            <ta e="T6" id="Seg_290" s="T5">ninɨ</ta>
            <ta e="T7" id="Seg_291" s="T6">šittälʼ</ta>
            <ta e="T8" id="Seg_292" s="T7">tü-sa-k</ta>
            <ta e="T9" id="Seg_293" s="T8">Sidorovsq-a-n</ta>
            <ta e="T10" id="Seg_294" s="T9">anä-m</ta>
            <ta e="T11" id="Seg_295" s="T10">quː-sɨ</ta>
            <ta e="T15" id="Seg_296" s="T14">əsä-p</ta>
            <ta e="T16" id="Seg_297" s="T15">mütoː-qɨt</ta>
            <ta e="T17" id="Seg_298" s="T16">qəs-sɔː-tɨn</ta>
            <ta e="T18" id="Seg_299" s="T17">pumlɔːt</ta>
            <ta e="T19" id="Seg_300" s="T18">quː-sɨ</ta>
            <ta e="T20" id="Seg_301" s="T19">ilʼčʼa-m</ta>
            <ta e="T21" id="Seg_302" s="T20">ilʼčʼa-m</ta>
            <ta e="T22" id="Seg_303" s="T21">quː-la</ta>
            <ta e="T23" id="Seg_304" s="T22">pula</ta>
            <ta e="T24" id="Seg_305" s="T23">qən-əıː-sa-k</ta>
            <ta e="T25" id="Seg_306" s="T24">učʼite-la</ta>
            <ta e="T26" id="Seg_307" s="T25">škola-qɨnɨ</ta>
            <ta e="T27" id="Seg_308" s="T26">tüː-sa-k</ta>
            <ta e="T28" id="Seg_309" s="T27">Sidorovsq-a-n</ta>
            <ta e="T29" id="Seg_310" s="T28">uːčʼi-sa-k</ta>
            <ta e="T30" id="Seg_311" s="T29">kolhoz-qɨt</ta>
            <ta e="T31" id="Seg_312" s="T30">qəːlɨ-p</ta>
            <ta e="T32" id="Seg_313" s="T31">qət-tɨ-sa-m</ta>
            <ta e="T33" id="Seg_314" s="T32">kə-t</ta>
            <ta e="T34" id="Seg_315" s="T33">taŋɨ-t</ta>
            <ta e="T35" id="Seg_316" s="T34">nüːtɨ-p</ta>
            <ta e="T36" id="Seg_317" s="T35">kosi-mpɨ-sa-m</ta>
            <ta e="T37" id="Seg_318" s="T36">sovhoz-qɨt</ta>
            <ta e="T38" id="Seg_319" s="T37">tıː</ta>
            <ta e="T39" id="Seg_320" s="T38">man</ta>
            <ta e="T40" id="Seg_321" s="T39">uːčʼa-k</ta>
            <ta e="T41" id="Seg_322" s="T40">olqa</ta>
            <ta e="T42" id="Seg_323" s="T41">uːtɨlpo-qɨt</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_324" s="T1">Tazovska</ta>
            <ta e="T3" id="Seg_325" s="T2">*nɔː-nɨ</ta>
            <ta e="T4" id="Seg_326" s="T3">qən-ɛː-sɨ-k</ta>
            <ta e="T5" id="Seg_327" s="T4">Sovrečʼqa-n.</ta>
            <ta e="T6" id="Seg_328" s="T5">nɨːnɨ</ta>
            <ta e="T7" id="Seg_329" s="T6">šittälʼ</ta>
            <ta e="T8" id="Seg_330" s="T7">tü-sɨ-k</ta>
            <ta e="T9" id="Seg_331" s="T8">Sidorovsk-ɨ-n</ta>
            <ta e="T10" id="Seg_332" s="T9">ama-mɨ</ta>
            <ta e="T11" id="Seg_333" s="T10">qu-sɨ</ta>
            <ta e="T15" id="Seg_334" s="T14">əsɨ-m</ta>
            <ta e="T16" id="Seg_335" s="T15">mütɨ-qɨn</ta>
            <ta e="T17" id="Seg_336" s="T16">qət-sɨ-tɨt</ta>
            <ta e="T18" id="Seg_337" s="T17">pumlɔːt</ta>
            <ta e="T19" id="Seg_338" s="T18">qu-sɨ</ta>
            <ta e="T20" id="Seg_339" s="T19">ilʼčʼa-mɨ</ta>
            <ta e="T21" id="Seg_340" s="T20">ilʼčʼa-mɨ</ta>
            <ta e="T22" id="Seg_341" s="T21">qu-lä</ta>
            <ta e="T23" id="Seg_342" s="T22">puːlä</ta>
            <ta e="T24" id="Seg_343" s="T23">qən-ɛː-sɨ-k</ta>
            <ta e="T25" id="Seg_344" s="T24">učʼite-lä</ta>
            <ta e="T26" id="Seg_345" s="T25">škola-qɨnɨ</ta>
            <ta e="T27" id="Seg_346" s="T26">tü-sɨ-k</ta>
            <ta e="T28" id="Seg_347" s="T27">Sidorovsk-ɨ-n</ta>
            <ta e="T29" id="Seg_348" s="T28">uːčʼɨ-sɨ-k</ta>
            <ta e="T30" id="Seg_349" s="T29">kolhoz-qɨn</ta>
            <ta e="T31" id="Seg_350" s="T30">qəːlɨ-m</ta>
            <ta e="T32" id="Seg_351" s="T31">qət-ntɨ-sɨ-m</ta>
            <ta e="T33" id="Seg_352" s="T32">kə-n</ta>
            <ta e="T34" id="Seg_353" s="T33">taŋɨ-n</ta>
            <ta e="T35" id="Seg_354" s="T34">nʼuːtɨ-m</ta>
            <ta e="T36" id="Seg_355" s="T35">kosi-mpɨ-sɨ-m</ta>
            <ta e="T37" id="Seg_356" s="T36">sovhoz-qɨn</ta>
            <ta e="T38" id="Seg_357" s="T37">tıː</ta>
            <ta e="T39" id="Seg_358" s="T38">man</ta>
            <ta e="T40" id="Seg_359" s="T39">uːčʼɨ-k</ta>
            <ta e="T41" id="Seg_360" s="T40">olqa</ta>
            <ta e="T42" id="Seg_361" s="T41">uːtɨlʼpo-qɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_362" s="T1">Tazovsk.GEN</ta>
            <ta e="T3" id="Seg_363" s="T2">out-ADV.EL</ta>
            <ta e="T4" id="Seg_364" s="T3">leave-PFV-PST-1SG.S</ta>
            <ta e="T5" id="Seg_365" s="T4">Sovrechka-ILL2</ta>
            <ta e="T6" id="Seg_366" s="T5">then</ta>
            <ta e="T7" id="Seg_367" s="T6">then</ta>
            <ta e="T8" id="Seg_368" s="T7">come-PST-1SG.S</ta>
            <ta e="T9" id="Seg_369" s="T8">Sidorovsk-EP-ILL2</ta>
            <ta e="T10" id="Seg_370" s="T9">mother.[NOM]-1SG</ta>
            <ta e="T11" id="Seg_371" s="T10">die-PST.[3SG.S]</ta>
            <ta e="T15" id="Seg_372" s="T14">father-ACC</ta>
            <ta e="T16" id="Seg_373" s="T15">war-LOC</ta>
            <ta e="T17" id="Seg_374" s="T16">kill-PST-3PL</ta>
            <ta e="T18" id="Seg_375" s="T17">%%</ta>
            <ta e="T19" id="Seg_376" s="T18">die-PST.[3SG.S]</ta>
            <ta e="T20" id="Seg_377" s="T19">grandfather.[NOM]-1SG</ta>
            <ta e="T21" id="Seg_378" s="T20">grandfather.[NOM]-1SG</ta>
            <ta e="T22" id="Seg_379" s="T21">die-CVB</ta>
            <ta e="T23" id="Seg_380" s="T22">after</ta>
            <ta e="T24" id="Seg_381" s="T23">leave-PFV-PST-1SG.S</ta>
            <ta e="T25" id="Seg_382" s="T24">study-CVB</ta>
            <ta e="T26" id="Seg_383" s="T25">school-EL</ta>
            <ta e="T27" id="Seg_384" s="T26">come-PST-1SG.S</ta>
            <ta e="T28" id="Seg_385" s="T27">Sidorovsk-EP-ILL2</ta>
            <ta e="T29" id="Seg_386" s="T28">work-PST-1SG.S</ta>
            <ta e="T30" id="Seg_387" s="T29">kolkhoz-LOC</ta>
            <ta e="T31" id="Seg_388" s="T30">fish-ACC</ta>
            <ta e="T32" id="Seg_389" s="T31">kill-IPFV-PST-1SG.O</ta>
            <ta e="T33" id="Seg_390" s="T32">winter-ADV.LOC</ta>
            <ta e="T34" id="Seg_391" s="T33">summer-ADV.LOC</ta>
            <ta e="T35" id="Seg_392" s="T34">grass-ACC</ta>
            <ta e="T36" id="Seg_393" s="T35">cut-DUR-PST-1SG.O</ta>
            <ta e="T37" id="Seg_394" s="T36">sovkhoz-LOC</ta>
            <ta e="T38" id="Seg_395" s="T37">now</ta>
            <ta e="T39" id="Seg_396" s="T38">I.NOM</ta>
            <ta e="T40" id="Seg_397" s="T39">work-1SG.S</ta>
            <ta e="T41" id="Seg_398" s="T40">simply</ta>
            <ta e="T42" id="Seg_399" s="T41">work-LOC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_400" s="T1">Тазовск.GEN</ta>
            <ta e="T3" id="Seg_401" s="T2">из-ADV.EL</ta>
            <ta e="T4" id="Seg_402" s="T3">отправиться-PFV-PST-1SG.S</ta>
            <ta e="T5" id="Seg_403" s="T4">Совречка-ILL2</ta>
            <ta e="T6" id="Seg_404" s="T5">потом</ta>
            <ta e="T7" id="Seg_405" s="T6">потом</ta>
            <ta e="T8" id="Seg_406" s="T7">прийти-PST-1SG.S</ta>
            <ta e="T9" id="Seg_407" s="T8">Сидоровск-EP-ILL2</ta>
            <ta e="T10" id="Seg_408" s="T9">мать.[NOM]-1SG</ta>
            <ta e="T11" id="Seg_409" s="T10">умереть-PST.[3SG.S]</ta>
            <ta e="T15" id="Seg_410" s="T14">отец-ACC</ta>
            <ta e="T16" id="Seg_411" s="T15">война-LOC</ta>
            <ta e="T17" id="Seg_412" s="T16">убить-PST-3PL</ta>
            <ta e="T18" id="Seg_413" s="T17">%%</ta>
            <ta e="T19" id="Seg_414" s="T18">умереть-PST.[3SG.S]</ta>
            <ta e="T20" id="Seg_415" s="T19">дедушка.[NOM]-1SG</ta>
            <ta e="T21" id="Seg_416" s="T20">дедушка.[NOM]-1SG</ta>
            <ta e="T22" id="Seg_417" s="T21">умереть-CVB</ta>
            <ta e="T23" id="Seg_418" s="T22">после</ta>
            <ta e="T24" id="Seg_419" s="T23">отправиться-PFV-PST-1SG.S</ta>
            <ta e="T25" id="Seg_420" s="T24">учиться-CVB</ta>
            <ta e="T26" id="Seg_421" s="T25">школа-EL</ta>
            <ta e="T27" id="Seg_422" s="T26">прийти-PST-1SG.S</ta>
            <ta e="T28" id="Seg_423" s="T27">Сидоровск-EP-ILL2</ta>
            <ta e="T29" id="Seg_424" s="T28">работать-PST-1SG.S</ta>
            <ta e="T30" id="Seg_425" s="T29">колхоз-LOC</ta>
            <ta e="T31" id="Seg_426" s="T30">рыба-ACC</ta>
            <ta e="T32" id="Seg_427" s="T31">убить-IPFV-PST-1SG.O</ta>
            <ta e="T33" id="Seg_428" s="T32">зима-ADV.LOC</ta>
            <ta e="T34" id="Seg_429" s="T33">лето-ADV.LOC</ta>
            <ta e="T35" id="Seg_430" s="T34">трава-ACC</ta>
            <ta e="T36" id="Seg_431" s="T35">косить-DUR-PST-1SG.O</ta>
            <ta e="T37" id="Seg_432" s="T36">совхоз-LOC</ta>
            <ta e="T38" id="Seg_433" s="T37">сейчас</ta>
            <ta e="T39" id="Seg_434" s="T38">я.NOM</ta>
            <ta e="T40" id="Seg_435" s="T39">работать-1SG.S</ta>
            <ta e="T41" id="Seg_436" s="T40">просто.так</ta>
            <ta e="T42" id="Seg_437" s="T41">работа-LOC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_438" s="T1">nprop.[n:case]</ta>
            <ta e="T3" id="Seg_439" s="T2">pp-n&gt;adv</ta>
            <ta e="T4" id="Seg_440" s="T3">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_441" s="T4">nprop-n:case</ta>
            <ta e="T6" id="Seg_442" s="T5">adv</ta>
            <ta e="T7" id="Seg_443" s="T6">adv</ta>
            <ta e="T8" id="Seg_444" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_445" s="T8">nprop-n:ins-n:case</ta>
            <ta e="T10" id="Seg_446" s="T9">n-n:case-n:poss</ta>
            <ta e="T11" id="Seg_447" s="T10">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_448" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_449" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_450" s="T16">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_451" s="T17">adv</ta>
            <ta e="T19" id="Seg_452" s="T18">v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_453" s="T19">n-n:case-n:poss</ta>
            <ta e="T21" id="Seg_454" s="T20">n-n:case-n:poss</ta>
            <ta e="T22" id="Seg_455" s="T21">v-v&gt;adv</ta>
            <ta e="T23" id="Seg_456" s="T22">pp</ta>
            <ta e="T24" id="Seg_457" s="T23">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_458" s="T24">v-v&gt;adv</ta>
            <ta e="T26" id="Seg_459" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_460" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_461" s="T27">nprop-n:ins-n:case</ta>
            <ta e="T29" id="Seg_462" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_463" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_464" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_465" s="T31">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_466" s="T32">n-n&gt;adv</ta>
            <ta e="T34" id="Seg_467" s="T33">n-n&gt;adv</ta>
            <ta e="T35" id="Seg_468" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_469" s="T35">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_470" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_471" s="T37">adv</ta>
            <ta e="T39" id="Seg_472" s="T38">pers</ta>
            <ta e="T40" id="Seg_473" s="T39">v-v:pn</ta>
            <ta e="T41" id="Seg_474" s="T40">adv</ta>
            <ta e="T42" id="Seg_475" s="T41">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_476" s="T1">nprop</ta>
            <ta e="T3" id="Seg_477" s="T2">adv</ta>
            <ta e="T4" id="Seg_478" s="T3">v</ta>
            <ta e="T5" id="Seg_479" s="T4">nprop</ta>
            <ta e="T6" id="Seg_480" s="T5">adv</ta>
            <ta e="T7" id="Seg_481" s="T6">adv</ta>
            <ta e="T8" id="Seg_482" s="T7">v</ta>
            <ta e="T9" id="Seg_483" s="T8">nprop</ta>
            <ta e="T10" id="Seg_484" s="T9">n</ta>
            <ta e="T11" id="Seg_485" s="T10">v</ta>
            <ta e="T15" id="Seg_486" s="T14">n</ta>
            <ta e="T16" id="Seg_487" s="T15">n</ta>
            <ta e="T17" id="Seg_488" s="T16">v</ta>
            <ta e="T18" id="Seg_489" s="T17">adv</ta>
            <ta e="T19" id="Seg_490" s="T18">v</ta>
            <ta e="T20" id="Seg_491" s="T19">n</ta>
            <ta e="T21" id="Seg_492" s="T20">n</ta>
            <ta e="T22" id="Seg_493" s="T21">adv</ta>
            <ta e="T23" id="Seg_494" s="T22">pp</ta>
            <ta e="T24" id="Seg_495" s="T23">v</ta>
            <ta e="T25" id="Seg_496" s="T24">adv</ta>
            <ta e="T26" id="Seg_497" s="T25">n</ta>
            <ta e="T27" id="Seg_498" s="T26">v</ta>
            <ta e="T28" id="Seg_499" s="T27">nprop</ta>
            <ta e="T29" id="Seg_500" s="T28">v</ta>
            <ta e="T30" id="Seg_501" s="T29">n</ta>
            <ta e="T31" id="Seg_502" s="T30">n</ta>
            <ta e="T32" id="Seg_503" s="T31">v</ta>
            <ta e="T33" id="Seg_504" s="T32">adv</ta>
            <ta e="T34" id="Seg_505" s="T33">adv</ta>
            <ta e="T35" id="Seg_506" s="T34">n</ta>
            <ta e="T36" id="Seg_507" s="T35">v</ta>
            <ta e="T37" id="Seg_508" s="T36">n</ta>
            <ta e="T38" id="Seg_509" s="T37">adv</ta>
            <ta e="T39" id="Seg_510" s="T38">pers</ta>
            <ta e="T40" id="Seg_511" s="T39">v</ta>
            <ta e="T41" id="Seg_512" s="T40">adv</ta>
            <ta e="T42" id="Seg_513" s="T41">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_514" s="T1">np:So</ta>
            <ta e="T4" id="Seg_515" s="T3">0.1.h:A </ta>
            <ta e="T5" id="Seg_516" s="T4">np:G</ta>
            <ta e="T6" id="Seg_517" s="T5">adv:Time</ta>
            <ta e="T8" id="Seg_518" s="T7">0.1.h:A</ta>
            <ta e="T9" id="Seg_519" s="T8">np:G</ta>
            <ta e="T10" id="Seg_520" s="T9">np.h:P 0.1.h:Poss</ta>
            <ta e="T14" id="Seg_521" s="T11">pp:Time</ta>
            <ta e="T15" id="Seg_522" s="T14">np.h:P</ta>
            <ta e="T16" id="Seg_523" s="T15">np:L</ta>
            <ta e="T17" id="Seg_524" s="T16">0.3.h:A </ta>
            <ta e="T20" id="Seg_525" s="T19">np.h:P 0.1.h:Poss</ta>
            <ta e="T21" id="Seg_526" s="T20">np.h:P 0.1.h:Poss</ta>
            <ta e="T24" id="Seg_527" s="T23">0.1.h:A </ta>
            <ta e="T25" id="Seg_528" s="T24">0.1.h:A</ta>
            <ta e="T26" id="Seg_529" s="T25">np:So</ta>
            <ta e="T27" id="Seg_530" s="T26">0.1.h:A</ta>
            <ta e="T28" id="Seg_531" s="T27">np:G</ta>
            <ta e="T29" id="Seg_532" s="T28">0.1.h:A</ta>
            <ta e="T30" id="Seg_533" s="T29">np:L</ta>
            <ta e="T31" id="Seg_534" s="T30">np:P</ta>
            <ta e="T32" id="Seg_535" s="T31">0.1.h:A</ta>
            <ta e="T33" id="Seg_536" s="T32">adv:L</ta>
            <ta e="T34" id="Seg_537" s="T33">adv:L</ta>
            <ta e="T35" id="Seg_538" s="T34">np:P</ta>
            <ta e="T36" id="Seg_539" s="T35">0.1.h:A</ta>
            <ta e="T37" id="Seg_540" s="T36">np:L</ta>
            <ta e="T38" id="Seg_541" s="T37">adv:Time</ta>
            <ta e="T39" id="Seg_542" s="T38">pro.h:A</ta>
            <ta e="T42" id="Seg_543" s="T41">np:L</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_544" s="T3">0.1.h:S v:pred</ta>
            <ta e="T8" id="Seg_545" s="T7">0.1.h:S v:pred</ta>
            <ta e="T10" id="Seg_546" s="T9">np.h:S</ta>
            <ta e="T11" id="Seg_547" s="T10">v:pred</ta>
            <ta e="T15" id="Seg_548" s="T14">np.h:O</ta>
            <ta e="T17" id="Seg_549" s="T16">0.3.h:S</ta>
            <ta e="T19" id="Seg_550" s="T18">v:pred</ta>
            <ta e="T20" id="Seg_551" s="T19">np.h:S</ta>
            <ta e="T23" id="Seg_552" s="T20">s:temp</ta>
            <ta e="T24" id="Seg_553" s="T23">0.1.h:S v:pred</ta>
            <ta e="T25" id="Seg_554" s="T24">s:purp</ta>
            <ta e="T27" id="Seg_555" s="T26">0.1.h:S v:pred</ta>
            <ta e="T29" id="Seg_556" s="T28">0.1.h:S v:pred</ta>
            <ta e="T31" id="Seg_557" s="T30">np:O</ta>
            <ta e="T32" id="Seg_558" s="T31">0.1.h:S v:pred</ta>
            <ta e="T35" id="Seg_559" s="T34">np:O</ta>
            <ta e="T36" id="Seg_560" s="T35">0.1.h:S v:pred</ta>
            <ta e="T39" id="Seg_561" s="T38">pro.h:S</ta>
            <ta e="T40" id="Seg_562" s="T39">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_563" s="T4">RUS:cult</ta>
            <ta e="T9" id="Seg_564" s="T8">RUS:cult</ta>
            <ta e="T25" id="Seg_565" s="T24">RUS:cult</ta>
            <ta e="T26" id="Seg_566" s="T25">RUS:cult</ta>
            <ta e="T28" id="Seg_567" s="T27">RUS:cult</ta>
            <ta e="T30" id="Seg_568" s="T29">RUS:cult</ta>
            <ta e="T36" id="Seg_569" s="T35">RUS:cult</ta>
            <ta e="T37" id="Seg_570" s="T36">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T25" id="Seg_571" s="T24">indir:infl</ta>
            <ta e="T26" id="Seg_572" s="T25">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_573" s="T1">Из Тазовска я поехал в Совречку.</ta>
            <ta e="T9" id="Seg_574" s="T5">Потом я поехал в Сидоровск.</ta>
            <ta e="T14" id="Seg_575" s="T9">Мама моя умерла в 1947 году.</ta>
            <ta e="T17" id="Seg_576" s="T14">Отца моего на войне убили.</ta>
            <ta e="T20" id="Seg_577" s="T17">(Позже?) умер мой дед. </ta>
            <ta e="T25" id="Seg_578" s="T20">После смерти деда я поехал учиться.</ta>
            <ta e="T28" id="Seg_579" s="T25">Из школы я приехал в Сидоровск.</ta>
            <ta e="T30" id="Seg_580" s="T28">Я работал в колхозе.</ta>
            <ta e="T33" id="Seg_581" s="T30">Рыбу добывал зимой.</ta>
            <ta e="T36" id="Seg_582" s="T33">Летом я сено косил.</ta>
            <ta e="T42" id="Seg_583" s="T36">В совхозе сейчас я работаю на простой работе.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_584" s="T1">From Tazovsk I went to Sovrechka.</ta>
            <ta e="T9" id="Seg_585" s="T5">Then I went to Sidorovsk.</ta>
            <ta e="T14" id="Seg_586" s="T9">My mother died in 1947.</ta>
            <ta e="T17" id="Seg_587" s="T14">My father was killed in the war.</ta>
            <ta e="T20" id="Seg_588" s="T17">(Later?) my grandfather died.</ta>
            <ta e="T25" id="Seg_589" s="T20">After the death of my grandfather, I went to study.</ta>
            <ta e="T28" id="Seg_590" s="T25">From school I came to Sidorovsk.</ta>
            <ta e="T30" id="Seg_591" s="T28">I worked on the collective farm/kolkhoz.</ta>
            <ta e="T33" id="Seg_592" s="T30">I caught fish in the winter. </ta>
            <ta e="T36" id="Seg_593" s="T33">In the summer I mowed the hay.</ta>
            <ta e="T42" id="Seg_594" s="T36">At the farm I am currently working on a simple job.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_595" s="T1">Von Tazovsk bin ich nach Sovrechka gegangen.</ta>
            <ta e="T9" id="Seg_596" s="T5">Dann ging ich nach Sidorovsk.</ta>
            <ta e="T14" id="Seg_597" s="T9">Meine Mutter starb 1947.</ta>
            <ta e="T17" id="Seg_598" s="T14">Mein Vater wurde im Krieg getötet.</ta>
            <ta e="T20" id="Seg_599" s="T17">(Später?) starb mein Großvater. </ta>
            <ta e="T25" id="Seg_600" s="T20">Nach dem Tod meines Großvaters, ging ich zum Studieren weg. </ta>
            <ta e="T28" id="Seg_601" s="T25">Von der Schule bin ich nach Sidorovsk gekommen.</ta>
            <ta e="T30" id="Seg_602" s="T28">Ich habe auf der kollektiven Farm/Kolchose gearbeitet.</ta>
            <ta e="T33" id="Seg_603" s="T30">Ich habe Fische im Winter gefangen.</ta>
            <ta e="T36" id="Seg_604" s="T33">Im Sommer habe ich das Heu gemäht.</ta>
            <ta e="T42" id="Seg_605" s="T36">Auf dem Bauernhof arbeite ich gerade in einer einfachen Arbeit.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_606" s="T1">Из Тазовска я поехал в Совречку.</ta>
            <ta e="T9" id="Seg_607" s="T5">Потом я поехал в Сидоровск.</ta>
            <ta e="T14" id="Seg_608" s="T9">Мама умерла в 1947 году.</ta>
            <ta e="T17" id="Seg_609" s="T14">Отца на войне убили.</ta>
            <ta e="T20" id="Seg_610" s="T17">(Позже?) умер дед. </ta>
            <ta e="T25" id="Seg_611" s="T20">После смерти деда поехал учиться.</ta>
            <ta e="T28" id="Seg_612" s="T25">Из школы я приехал в Сидоровск.</ta>
            <ta e="T30" id="Seg_613" s="T28">Работал в колхозе.</ta>
            <ta e="T33" id="Seg_614" s="T30">Рыбу добывал (=убивал) зимой.</ta>
            <ta e="T36" id="Seg_615" s="T33">Летом сено косил.</ta>
            <ta e="T42" id="Seg_616" s="T36">В совхозе сейчас я работаю на простой работе.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T5" id="Seg_617" s="T1">[BrM:] -n as ILL2 is usual for Ket and Tym dialects, not for the Taz dialect.</ta>
            <ta e="T9" id="Seg_618" s="T5">[BrM:] -n as ILL2 is usual for Ket and Tym dialects, not for the Taz dialect.</ta>
            <ta e="T28" id="Seg_619" s="T25">[BrM:] -n as ILL2 is usual for Ket and Tym dialects, not for the Taz dialect.</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T5" id="Seg_620" s="T1">[WNB] Is it a lative?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
