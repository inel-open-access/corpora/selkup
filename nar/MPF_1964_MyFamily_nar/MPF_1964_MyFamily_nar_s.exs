<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>MPF_1964_MyFamily_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">MPF_1964_MyFamily_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">173</ud-information>
            <ud-information attribute-name="# HIAT:w">130</ud-information>
            <ud-information attribute-name="# e">130</ud-information>
            <ud-information attribute-name="# HIAT:u">25</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="MPF">
            <abbreviation>MPF</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T130" time="37.22727272727273" type="intp" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="MPF"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T129" id="Seg_0" n="sc" s="T0">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">meŋnan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">üːčem</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">egan</ts>
                  <nts id="Seg_11" n="HIAT:ip">,</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">okkar</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">üːčem</ts>
                  <nts id="Seg_18" n="HIAT:ip">,</nts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_21" n="HIAT:w" s="T5">tipekuwaj</ts>
                  <nts id="Seg_22" n="HIAT:ip">.</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_25" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_27" n="HIAT:w" s="T6">massə</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_30" n="HIAT:w" s="T7">täp</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">iːlaŋ</ts>
                  <nts id="Seg_34" n="HIAT:ip">,</nts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">äwəgalak</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_41" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">meː</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">malwumboo</ts>
                  <nts id="Seg_47" n="HIAT:ip">,</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">somblʼe</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">pɨat</ts>
                  <nts id="Seg_54" n="HIAT:ip">.</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_57" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_59" n="HIAT:w" s="T14">näwɨ</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_62" n="HIAT:w" s="T15">ewadnan</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">iːlaŋ</ts>
                  <nts id="Seg_66" n="HIAT:ip">,</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_69" n="HIAT:w" s="T17">äwandɨse</ts>
                  <nts id="Seg_70" n="HIAT:ip">.</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_73" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_75" n="HIAT:w" s="T18">man</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_78" n="HIAT:w" s="T19">assə</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_81" n="HIAT:w" s="T20">tiːnnowam</ts>
                  <nts id="Seg_82" n="HIAT:ip">,</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_85" n="HIAT:w" s="T21">kuldiŋ</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_88" n="HIAT:w" s="T22">täp</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_91" n="HIAT:w" s="T23">iːlaŋ</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_95" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_97" n="HIAT:w" s="T24">täp</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_100" n="HIAT:w" s="T25">assɨ</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_103" n="HIAT:w" s="T26">tinnuwɨt</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_106" n="HIAT:w" s="T27">kuldʼiŋ</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_109" n="HIAT:w" s="T28">me</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_112" n="HIAT:w" s="T29">iːllot</ts>
                  <nts id="Seg_113" n="HIAT:ip">.</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_116" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_118" n="HIAT:w" s="T30">me</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_121" n="HIAT:w" s="T31">assə</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_124" n="HIAT:w" s="T32">nɨkarkuwot</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">nʼäːqnɨt</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">qala</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">me</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_136" n="HIAT:w" s="T36">nʼäwat</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_139" n="HIAT:w" s="T37">ewalǯimbot</ts>
                  <nts id="Seg_140" n="HIAT:ip">.</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_143" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_145" n="HIAT:w" s="T38">qosi</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_148" n="HIAT:w" s="T39">qaɣi</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_151" n="HIAT:w" s="T40">me</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_154" n="HIAT:w" s="T41">nʼäwat</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_157" n="HIAT:w" s="T42">qonǯurčenǯat</ts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_161" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_163" n="HIAT:w" s="T43">oːpat</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_166" n="HIAT:w" s="T44">qojjat</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_169" n="HIAT:w" s="T45">tipeqowaj</ts>
                  <nts id="Seg_170" n="HIAT:ip">,</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_173" n="HIAT:w" s="T46">täpqiː</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_176" n="HIAT:w" s="T47">qala</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_179" n="HIAT:w" s="T48">araŋ</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_182" n="HIAT:w" s="T49">quːlla</ts>
                  <nts id="Seg_183" n="HIAT:ip">.</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_186" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_188" n="HIAT:w" s="T50">assə</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_191" n="HIAT:w" s="T51">nɨkarquwti</ts>
                  <nts id="Seg_192" n="HIAT:ip">.</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_195" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_197" n="HIAT:w" s="T52">nänɨ</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_200" n="HIAT:w" s="T53">ewat</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_203" n="HIAT:w" s="T54">qassaj</ts>
                  <nts id="Seg_204" n="HIAT:ip">.</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_207" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_209" n="HIAT:w" s="T55">täpɨ</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_212" n="HIAT:w" s="T56">qwatukuŋ</ts>
                  <nts id="Seg_213" n="HIAT:ip">,</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_216" n="HIAT:w" s="T57">ɨkkə</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_219" n="HIAT:w" s="T58">nɨkarkut</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_222" n="HIAT:w" s="T59">esändɨni</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_224" n="HIAT:ip">(</nts>
                  <ts e="T61" id="Seg_226" n="HIAT:w" s="T60">i</ts>
                  <nts id="Seg_227" n="HIAT:ip">)</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_230" n="HIAT:w" s="T61">qojjadni</ts>
                  <nts id="Seg_231" n="HIAT:ip">.</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_234" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_236" n="HIAT:w" s="T62">ass</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_239" n="HIAT:w" s="T63">qoːnaŋ</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_242" n="HIAT:w" s="T64">tädni</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_245" n="HIAT:w" s="T65">kəːtčam</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_248" n="HIAT:w" s="T66">äwem</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_251" n="HIAT:w" s="T130">tʼät</ts>
                  <nts id="Seg_252" n="HIAT:ip">.</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_255" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_257" n="HIAT:w" s="T67">kundoqɨn</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_260" n="HIAT:w" s="T68">qumba</ts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_264" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_266" n="HIAT:w" s="T69">qosi</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_269" n="HIAT:w" s="T70">titam</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_272" n="HIAT:w" s="T71">tämba</ts>
                  <nts id="Seg_273" n="HIAT:ip">.</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_276" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_278" n="HIAT:w" s="T72">äwem</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_281" n="HIAT:w" s="T73">tita</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_284" n="HIAT:w" s="T74">qwɔntqumba</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_287" n="HIAT:w" s="T75">muqtut</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_290" n="HIAT:w" s="T76">saːroj</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_293" n="HIAT:w" s="T77">päatə</ts>
                  <nts id="Seg_294" n="HIAT:ip">.</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_297" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_299" n="HIAT:w" s="T78">meŋa</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_302" n="HIAT:w" s="T79">čarakuŋ</ts>
                  <nts id="Seg_303" n="HIAT:ip">:</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_306" n="HIAT:w" s="T80">näːkar</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_309" n="HIAT:w" s="T81">näːkaːrkut</ts>
                  <nts id="Seg_310" n="HIAT:ip">.</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_313" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_315" n="HIAT:w" s="T82">meŋa</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_318" n="HIAT:w" s="T83">titam</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_321" n="HIAT:w" s="T84">assə</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_324" n="HIAT:w" s="T85">kundɨ</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_327" n="HIAT:w" s="T86">iːləku</ts>
                  <nts id="Seg_328" n="HIAT:ip">.</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_331" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_333" n="HIAT:w" s="T87">qallo</ts>
                  <nts id="Seg_334" n="HIAT:ip">:</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_337" n="HIAT:w" s="T88">meŋnan</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_340" n="HIAT:w" s="T89">šɨttə</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_343" n="HIAT:w" s="T90">iːw</ts>
                  <nts id="Seg_344" n="HIAT:ip">,</nts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_347" n="HIAT:w" s="T91">okkə</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_350" n="HIAT:w" s="T92">näw</ts>
                  <nts id="Seg_351" n="HIAT:ip">.</nts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_354" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_356" n="HIAT:w" s="T93">täpɨ</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_359" n="HIAT:w" s="T94">ilaŋ</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_362" n="HIAT:w" s="T95">nändɨnan</ts>
                  <nts id="Seg_363" n="HIAT:ip">.</nts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_366" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_368" n="HIAT:w" s="T96">qoːsi</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_371" n="HIAT:w" s="T97">čarakus</ts>
                  <nts id="Seg_372" n="HIAT:ip">:</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_375" n="HIAT:w" s="T98">man</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_378" n="HIAT:w" s="T99">täni</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_381" n="HIAT:w" s="T100">tüːkunǯaŋ</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_384" n="HIAT:w" s="T101">sindat</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_387" n="HIAT:w" s="T102">manǯeku</ts>
                  <nts id="Seg_388" n="HIAT:ip">.</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_391" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_393" n="HIAT:w" s="T103">manǯe</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_396" n="HIAT:w" s="T104">man</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_399" n="HIAT:w" s="T105">ässäm</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_402" n="HIAT:w" s="T106">qumba</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_405" n="HIAT:w" s="T107">kundoqɨn</ts>
                  <nts id="Seg_406" n="HIAT:ip">.</nts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_409" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_411" n="HIAT:w" s="T108">somblʼe</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_414" n="HIAT:w" s="T109">saːrum</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_417" n="HIAT:w" s="T110">pɨätɨ</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_420" n="HIAT:w" s="T111">iːlas</ts>
                  <nts id="Seg_421" n="HIAT:ip">,</nts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_424" n="HIAT:w" s="T112">säː</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_427" n="HIAT:w" s="T113">saːrum</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_430" n="HIAT:w" s="T114">pɨat</ts>
                  <nts id="Seg_431" n="HIAT:ip">.</nts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_434" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_436" n="HIAT:w" s="T115">ass</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_439" n="HIAT:w" s="T116">kundə</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_442" n="HIAT:w" s="T117">opsä</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_445" n="HIAT:w" s="T118">qüːtas</ts>
                  <nts id="Seg_446" n="HIAT:ip">,</nts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_449" n="HIAT:w" s="T119">somblʼe</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_452" n="HIAT:w" s="T120">kʼəjkʼöt</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_454" n="HIAT:ip">(</nts>
                  <ts e="T122" id="Seg_456" n="HIAT:w" s="T121">somblʼe</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_459" n="HIAT:w" s="T122">köj</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_462" n="HIAT:w" s="T123">kʼöt</ts>
                  <nts id="Seg_463" n="HIAT:ip">)</nts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_466" n="HIAT:w" s="T124">čʼeːlat</ts>
                  <nts id="Seg_467" n="HIAT:ip">.</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_470" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_472" n="HIAT:w" s="T125">man</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_475" n="HIAT:w" s="T126">naq</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_478" n="HIAT:w" s="T127">čakə</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_481" n="HIAT:w" s="T128">qwɔntqunǯaŋ</ts>
                  <nts id="Seg_482" n="HIAT:ip">.</nts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T129" id="Seg_484" n="sc" s="T0">
               <ts e="T1" id="Seg_486" n="e" s="T0">meŋnan </ts>
               <ts e="T2" id="Seg_488" n="e" s="T1">üːčem </ts>
               <ts e="T3" id="Seg_490" n="e" s="T2">egan, </ts>
               <ts e="T4" id="Seg_492" n="e" s="T3">okkar </ts>
               <ts e="T5" id="Seg_494" n="e" s="T4">üːčem, </ts>
               <ts e="T6" id="Seg_496" n="e" s="T5">tipekuwaj. </ts>
               <ts e="T7" id="Seg_498" n="e" s="T6">massə </ts>
               <ts e="T8" id="Seg_500" n="e" s="T7">täp </ts>
               <ts e="T9" id="Seg_502" n="e" s="T8">iːlaŋ, </ts>
               <ts e="T10" id="Seg_504" n="e" s="T9">äwəgalak. </ts>
               <ts e="T11" id="Seg_506" n="e" s="T10">meː </ts>
               <ts e="T12" id="Seg_508" n="e" s="T11">malwumboo, </ts>
               <ts e="T13" id="Seg_510" n="e" s="T12">somblʼe </ts>
               <ts e="T14" id="Seg_512" n="e" s="T13">pɨat. </ts>
               <ts e="T15" id="Seg_514" n="e" s="T14">näwɨ </ts>
               <ts e="T16" id="Seg_516" n="e" s="T15">ewadnan </ts>
               <ts e="T17" id="Seg_518" n="e" s="T16">iːlaŋ, </ts>
               <ts e="T18" id="Seg_520" n="e" s="T17">äwandɨse. </ts>
               <ts e="T19" id="Seg_522" n="e" s="T18">man </ts>
               <ts e="T20" id="Seg_524" n="e" s="T19">assə </ts>
               <ts e="T21" id="Seg_526" n="e" s="T20">tiːnnowam, </ts>
               <ts e="T22" id="Seg_528" n="e" s="T21">kuldiŋ </ts>
               <ts e="T23" id="Seg_530" n="e" s="T22">täp </ts>
               <ts e="T24" id="Seg_532" n="e" s="T23">iːlaŋ. </ts>
               <ts e="T25" id="Seg_534" n="e" s="T24">täp </ts>
               <ts e="T26" id="Seg_536" n="e" s="T25">assɨ </ts>
               <ts e="T27" id="Seg_538" n="e" s="T26">tinnuwɨt </ts>
               <ts e="T28" id="Seg_540" n="e" s="T27">kuldʼiŋ </ts>
               <ts e="T29" id="Seg_542" n="e" s="T28">me </ts>
               <ts e="T30" id="Seg_544" n="e" s="T29">iːllot. </ts>
               <ts e="T31" id="Seg_546" n="e" s="T30">me </ts>
               <ts e="T32" id="Seg_548" n="e" s="T31">assə </ts>
               <ts e="T33" id="Seg_550" n="e" s="T32">nɨkarkuwot </ts>
               <ts e="T34" id="Seg_552" n="e" s="T33">nʼäːqnɨt </ts>
               <ts e="T35" id="Seg_554" n="e" s="T34">qala </ts>
               <ts e="T36" id="Seg_556" n="e" s="T35">me </ts>
               <ts e="T37" id="Seg_558" n="e" s="T36">nʼäwat </ts>
               <ts e="T38" id="Seg_560" n="e" s="T37">ewalǯimbot. </ts>
               <ts e="T39" id="Seg_562" n="e" s="T38">qosi </ts>
               <ts e="T40" id="Seg_564" n="e" s="T39">qaɣi </ts>
               <ts e="T41" id="Seg_566" n="e" s="T40">me </ts>
               <ts e="T42" id="Seg_568" n="e" s="T41">nʼäwat </ts>
               <ts e="T43" id="Seg_570" n="e" s="T42">qonǯurčenǯat. </ts>
               <ts e="T44" id="Seg_572" n="e" s="T43">oːpat </ts>
               <ts e="T45" id="Seg_574" n="e" s="T44">qojjat </ts>
               <ts e="T46" id="Seg_576" n="e" s="T45">tipeqowaj, </ts>
               <ts e="T47" id="Seg_578" n="e" s="T46">täpqiː </ts>
               <ts e="T48" id="Seg_580" n="e" s="T47">qala </ts>
               <ts e="T49" id="Seg_582" n="e" s="T48">araŋ </ts>
               <ts e="T50" id="Seg_584" n="e" s="T49">quːlla. </ts>
               <ts e="T51" id="Seg_586" n="e" s="T50">assə </ts>
               <ts e="T52" id="Seg_588" n="e" s="T51">nɨkarquwti. </ts>
               <ts e="T53" id="Seg_590" n="e" s="T52">nänɨ </ts>
               <ts e="T54" id="Seg_592" n="e" s="T53">ewat </ts>
               <ts e="T55" id="Seg_594" n="e" s="T54">qassaj. </ts>
               <ts e="T56" id="Seg_596" n="e" s="T55">täpɨ </ts>
               <ts e="T57" id="Seg_598" n="e" s="T56">qwatukuŋ, </ts>
               <ts e="T58" id="Seg_600" n="e" s="T57">ɨkkə </ts>
               <ts e="T59" id="Seg_602" n="e" s="T58">nɨkarkut </ts>
               <ts e="T60" id="Seg_604" n="e" s="T59">esändɨni </ts>
               <ts e="T61" id="Seg_606" n="e" s="T60">(i) </ts>
               <ts e="T62" id="Seg_608" n="e" s="T61">qojjadni. </ts>
               <ts e="T63" id="Seg_610" n="e" s="T62">ass </ts>
               <ts e="T64" id="Seg_612" n="e" s="T63">qoːnaŋ </ts>
               <ts e="T65" id="Seg_614" n="e" s="T64">tädni </ts>
               <ts e="T66" id="Seg_616" n="e" s="T65">kəːtčam </ts>
               <ts e="T130" id="Seg_618" n="e" s="T66">äwem </ts>
               <ts e="T67" id="Seg_620" n="e" s="T130">tʼät. </ts>
               <ts e="T68" id="Seg_622" n="e" s="T67">kundoqɨn </ts>
               <ts e="T69" id="Seg_624" n="e" s="T68">qumba. </ts>
               <ts e="T70" id="Seg_626" n="e" s="T69">qosi </ts>
               <ts e="T71" id="Seg_628" n="e" s="T70">titam </ts>
               <ts e="T72" id="Seg_630" n="e" s="T71">tämba. </ts>
               <ts e="T73" id="Seg_632" n="e" s="T72">äwem </ts>
               <ts e="T74" id="Seg_634" n="e" s="T73">tita </ts>
               <ts e="T75" id="Seg_636" n="e" s="T74">qwɔntqumba </ts>
               <ts e="T76" id="Seg_638" n="e" s="T75">muqtut </ts>
               <ts e="T77" id="Seg_640" n="e" s="T76">saːroj </ts>
               <ts e="T78" id="Seg_642" n="e" s="T77">päatə. </ts>
               <ts e="T79" id="Seg_644" n="e" s="T78">meŋa </ts>
               <ts e="T80" id="Seg_646" n="e" s="T79">čarakuŋ: </ts>
               <ts e="T81" id="Seg_648" n="e" s="T80">näːkar </ts>
               <ts e="T82" id="Seg_650" n="e" s="T81">näːkaːrkut. </ts>
               <ts e="T83" id="Seg_652" n="e" s="T82">meŋa </ts>
               <ts e="T84" id="Seg_654" n="e" s="T83">titam </ts>
               <ts e="T85" id="Seg_656" n="e" s="T84">assə </ts>
               <ts e="T86" id="Seg_658" n="e" s="T85">kundɨ </ts>
               <ts e="T87" id="Seg_660" n="e" s="T86">iːləku. </ts>
               <ts e="T88" id="Seg_662" n="e" s="T87">qallo: </ts>
               <ts e="T89" id="Seg_664" n="e" s="T88">meŋnan </ts>
               <ts e="T90" id="Seg_666" n="e" s="T89">šɨttə </ts>
               <ts e="T91" id="Seg_668" n="e" s="T90">iːw, </ts>
               <ts e="T92" id="Seg_670" n="e" s="T91">okkə </ts>
               <ts e="T93" id="Seg_672" n="e" s="T92">näw. </ts>
               <ts e="T94" id="Seg_674" n="e" s="T93">täpɨ </ts>
               <ts e="T95" id="Seg_676" n="e" s="T94">ilaŋ </ts>
               <ts e="T96" id="Seg_678" n="e" s="T95">nändɨnan. </ts>
               <ts e="T97" id="Seg_680" n="e" s="T96">qoːsi </ts>
               <ts e="T98" id="Seg_682" n="e" s="T97">čarakus: </ts>
               <ts e="T99" id="Seg_684" n="e" s="T98">man </ts>
               <ts e="T100" id="Seg_686" n="e" s="T99">täni </ts>
               <ts e="T101" id="Seg_688" n="e" s="T100">tüːkunǯaŋ </ts>
               <ts e="T102" id="Seg_690" n="e" s="T101">sindat </ts>
               <ts e="T103" id="Seg_692" n="e" s="T102">manǯeku. </ts>
               <ts e="T104" id="Seg_694" n="e" s="T103">manǯe </ts>
               <ts e="T105" id="Seg_696" n="e" s="T104">man </ts>
               <ts e="T106" id="Seg_698" n="e" s="T105">ässäm </ts>
               <ts e="T107" id="Seg_700" n="e" s="T106">qumba </ts>
               <ts e="T108" id="Seg_702" n="e" s="T107">kundoqɨn. </ts>
               <ts e="T109" id="Seg_704" n="e" s="T108">somblʼe </ts>
               <ts e="T110" id="Seg_706" n="e" s="T109">saːrum </ts>
               <ts e="T111" id="Seg_708" n="e" s="T110">pɨätɨ </ts>
               <ts e="T112" id="Seg_710" n="e" s="T111">iːlas, </ts>
               <ts e="T113" id="Seg_712" n="e" s="T112">säː </ts>
               <ts e="T114" id="Seg_714" n="e" s="T113">saːrum </ts>
               <ts e="T115" id="Seg_716" n="e" s="T114">pɨat. </ts>
               <ts e="T116" id="Seg_718" n="e" s="T115">ass </ts>
               <ts e="T117" id="Seg_720" n="e" s="T116">kundə </ts>
               <ts e="T118" id="Seg_722" n="e" s="T117">opsä </ts>
               <ts e="T119" id="Seg_724" n="e" s="T118">qüːtas, </ts>
               <ts e="T120" id="Seg_726" n="e" s="T119">somblʼe </ts>
               <ts e="T121" id="Seg_728" n="e" s="T120">kʼəjkʼöt </ts>
               <ts e="T122" id="Seg_730" n="e" s="T121">(somblʼe </ts>
               <ts e="T123" id="Seg_732" n="e" s="T122">köj </ts>
               <ts e="T124" id="Seg_734" n="e" s="T123">kʼöt) </ts>
               <ts e="T125" id="Seg_736" n="e" s="T124">čʼeːlat. </ts>
               <ts e="T126" id="Seg_738" n="e" s="T125">man </ts>
               <ts e="T127" id="Seg_740" n="e" s="T126">naq </ts>
               <ts e="T128" id="Seg_742" n="e" s="T127">čakə </ts>
               <ts e="T129" id="Seg_744" n="e" s="T128">qwɔntqunǯaŋ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_745" s="T0">MPF_1964_MyFamily_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_746" s="T6">MPF_1964_MyFamily_nar.002 (001.002)</ta>
            <ta e="T14" id="Seg_747" s="T10">MPF_1964_MyFamily_nar.003 (001.003)</ta>
            <ta e="T18" id="Seg_748" s="T14">MPF_1964_MyFamily_nar.004 (001.004)</ta>
            <ta e="T24" id="Seg_749" s="T18">MPF_1964_MyFamily_nar.005 (001.005)</ta>
            <ta e="T30" id="Seg_750" s="T24">MPF_1964_MyFamily_nar.006 (001.006)</ta>
            <ta e="T38" id="Seg_751" s="T30">MPF_1964_MyFamily_nar.007 (001.007)</ta>
            <ta e="T43" id="Seg_752" s="T38">MPF_1964_MyFamily_nar.008 (001.008)</ta>
            <ta e="T50" id="Seg_753" s="T43">MPF_1964_MyFamily_nar.009 (001.009)</ta>
            <ta e="T52" id="Seg_754" s="T50">MPF_1964_MyFamily_nar.010 (001.010)</ta>
            <ta e="T55" id="Seg_755" s="T52">MPF_1964_MyFamily_nar.011 (001.011)</ta>
            <ta e="T62" id="Seg_756" s="T55">MPF_1964_MyFamily_nar.012 (001.012)</ta>
            <ta e="T67" id="Seg_757" s="T62">MPF_1964_MyFamily_nar.013 (001.013)</ta>
            <ta e="T69" id="Seg_758" s="T67">MPF_1964_MyFamily_nar.014 (001.014)</ta>
            <ta e="T72" id="Seg_759" s="T69">MPF_1964_MyFamily_nar.015 (001.015)</ta>
            <ta e="T78" id="Seg_760" s="T72">MPF_1964_MyFamily_nar.016 (001.016)</ta>
            <ta e="T82" id="Seg_761" s="T78">MPF_1964_MyFamily_nar.017 (001.017)</ta>
            <ta e="T87" id="Seg_762" s="T82">MPF_1964_MyFamily_nar.018 (001.018)</ta>
            <ta e="T93" id="Seg_763" s="T87">MPF_1964_MyFamily_nar.019 (001.019)</ta>
            <ta e="T96" id="Seg_764" s="T93">MPF_1964_MyFamily_nar.020 (001.020)</ta>
            <ta e="T103" id="Seg_765" s="T96">MPF_1964_MyFamily_nar.021 (001.021)</ta>
            <ta e="T108" id="Seg_766" s="T103">MPF_1964_MyFamily_nar.022 (001.022)</ta>
            <ta e="T115" id="Seg_767" s="T108">MPF_1964_MyFamily_nar.023 (001.023)</ta>
            <ta e="T125" id="Seg_768" s="T115">MPF_1964_MyFamily_nar.024 (001.024)</ta>
            <ta e="T129" id="Seg_769" s="T125">MPF_1964_MyFamily_nar.025 (001.025)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_770" s="T0">′меңнан ӱ̄′ч(тш)ем ′е̨ган, оккар ӱ̄′чем, ‵типеку′вай.</ta>
            <ta e="T10" id="Seg_771" s="T6">′массъ тӓп ӣ′lаң, ′е(ӓ)в(ъ)гаlак.</ta>
            <ta e="T14" id="Seg_772" s="T10">ме̄ ′маlвумбо̄, ′сомблʼе пы(ъ)′ат.</ta>
            <ta e="T18" id="Seg_773" s="T14">′нӓвы е′ваднан ӣ′lаң, ӓ′вандысе.</ta>
            <ta e="T24" id="Seg_774" s="T18">ман ′ассъ ′тӣнно(е)вам, ′куlдиң тӓп ′ӣlаң.</ta>
            <ta e="T30" id="Seg_775" s="T24">тӓп ′ассы тинну′выт куl′дʼиң ме ′ӣllот.</ta>
            <ta e="T38" id="Seg_776" s="T30">ме ′ассъ ны′каркувот ′нʼӓ̄kныт kа′lа ме ′нʼӓват е̨′ваlджимбот.</ta>
            <ta e="T43" id="Seg_777" s="T38">′kоси ′kаɣ(г)и ме ′нʼӓват ‵kонджур′тшенджат.</ta>
            <ta e="T50" id="Seg_778" s="T43">о̄′пат kой′jат ‵типеkо′вай, ′тӓпkӣ kа′lа а′раң ′kӯllа.</ta>
            <ta e="T52" id="Seg_779" s="T50">′ассъ ны′карkувти.</ta>
            <ta e="T55" id="Seg_780" s="T52">′нӓны е′ват kассай.</ta>
            <ta e="T62" id="Seg_781" s="T55">′тӓпы kwа(ɔ)ту′куң, ′ыккъ ны′каркут е̨′сӓндыни (и) kой′jадни.</ta>
            <ta e="T67" id="Seg_782" s="T62">асс kо̄′наң ′тӓдни къ̊̄т′тшам ӓ′вем′тʼӓт.</ta>
            <ta e="T69" id="Seg_783" s="T67">кун′доkын kумба.</ta>
            <ta e="T72" id="Seg_784" s="T69">′koси ′титам ′тӓмба.</ta>
            <ta e="T78" id="Seg_785" s="T72">ӓ′вем ти′та ′kwɔнтkум′ба ′муkтут ′са̄рой ′пӓатъ.</ta>
            <ta e="T82" id="Seg_786" s="T78">′меңа тша′ракуң: нӓ̄′кар нӓ̄ка̄р′кут.</ta>
            <ta e="T87" id="Seg_787" s="T82">′меңа ′титам ′ассъ ′кунды ӣlъ′ку.</ta>
            <ta e="T93" id="Seg_788" s="T87">kа′llо: ′меңнан ′шыттъ ′ӣв, ′оккъ ′нӓв(w).</ta>
            <ta e="T96" id="Seg_789" s="T93">тӓпы и′lаң ′нӓндынан.</ta>
            <ta e="T103" id="Seg_790" s="T96">kо̄′си тʼшʼа′ракус: ман ′тӓни ′тӱ̄кунджаң ′синдат ман′джеку.</ta>
            <ta e="T108" id="Seg_791" s="T103">ман′дже ман ӓ′ссӓм ′kумба кун′доkын.</ta>
            <ta e="T115" id="Seg_792" s="T108">′сомблʼе ′са̄рум ′пыӓты ӣ′lас, ′сӓ̄ са̄рум ′пыат.</ta>
            <ta e="T125" id="Seg_793" s="T115">асс ′кундъ ′опсӓ kӱ̄′тас, ′сомблʼе кʼъ̊йкʼӧт (сомблʼе кӧй кʼӧт) ′тʼшʼе̄lат.</ta>
            <ta e="T129" id="Seg_794" s="T125">ман наk ′тшакъ kwɔнтkунджаң.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_795" s="T0">meŋnan üːč(tš)em egan, okkar üːčem, tipekuvaj.</ta>
            <ta e="T10" id="Seg_796" s="T6">massə täp iːlaŋ, e(ä)v(ə)galak.</ta>
            <ta e="T14" id="Seg_797" s="T10">meː malvumboː, somblʼe pɨ(ə)at.</ta>
            <ta e="T18" id="Seg_798" s="T14">nävɨ evadnan iːlaŋ, ävandɨse.</ta>
            <ta e="T24" id="Seg_799" s="T18">man assə tiːnno(e)vam, kuldiŋ täp iːlaŋ.</ta>
            <ta e="T30" id="Seg_800" s="T24">täp assɨ tinnuvɨt kuldʼiŋ me iːllot.</ta>
            <ta e="T38" id="Seg_801" s="T30">me assə nɨkarkuvot nʼäːqnɨt qala me nʼävat evalǯimbot.</ta>
            <ta e="T43" id="Seg_802" s="T38">qosi qaɣ(g)i me nʼävat qonǯurtšenǯat.</ta>
            <ta e="T50" id="Seg_803" s="T43">oːpat qojjat tipeqovaj, täpqiː qala araŋ quːlla.</ta>
            <ta e="T52" id="Seg_804" s="T50">assə nɨkarquvti.</ta>
            <ta e="T55" id="Seg_805" s="T52">nänɨ evat qassaj.</ta>
            <ta e="T62" id="Seg_806" s="T55">täpɨ qwa(ɔ)tukuŋ, ɨkkə nɨkarkut esändɨni (i) qojjadni.</ta>
            <ta e="T67" id="Seg_807" s="T62">ass qoːnaŋ tädni kəːttšam ävemtʼät.</ta>
            <ta e="T69" id="Seg_808" s="T67">kundoqɨn qumba.</ta>
            <ta e="T72" id="Seg_809" s="T69">qosi titam tämba.</ta>
            <ta e="T78" id="Seg_810" s="T72">ävem tita qwɔntqumba muqtut saːroj päatə.</ta>
            <ta e="T82" id="Seg_811" s="T78">meŋa tšarakuŋ: näːkar näːkaːrkut.</ta>
            <ta e="T87" id="Seg_812" s="T82">meŋa titam assə kundɨ iːləku.</ta>
            <ta e="T93" id="Seg_813" s="T87">qallo: meŋnan šɨttə iːv, okkə näv(w).</ta>
            <ta e="T96" id="Seg_814" s="T93">täpɨ ilaŋ nändɨnan.</ta>
            <ta e="T103" id="Seg_815" s="T96">qoːsi tʼšarakus: man täni tüːkunǯaŋ sindat manǯeku.</ta>
            <ta e="T108" id="Seg_816" s="T103">manǯe man ässäm qumba kundoqɨn.</ta>
            <ta e="T115" id="Seg_817" s="T108">somblʼe saːrum pɨätɨ iːlas, säː saːrum pɨat.</ta>
            <ta e="T125" id="Seg_818" s="T115">ass kundə opsä qüːtas, somblʼe kʼəjkʼöt (somblʼe köj kʼöt) tʼšeːlat.</ta>
            <ta e="T129" id="Seg_819" s="T125">man naq tšakə qwɔntqunǯaŋ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_820" s="T0">meŋnan üːčem egan, okkar üːčem, tipekuwaj. </ta>
            <ta e="T10" id="Seg_821" s="T6">massə täp iːlaŋ, äwəgalak. </ta>
            <ta e="T14" id="Seg_822" s="T10">meː malwumboo, somblʼe pɨat. </ta>
            <ta e="T18" id="Seg_823" s="T14">näwɨ ewadnan iːlaŋ, äwandɨse. </ta>
            <ta e="T24" id="Seg_824" s="T18">man assə tiːnnowam, kuldiŋ täp iːlaŋ. </ta>
            <ta e="T30" id="Seg_825" s="T24">täp assɨ tinnuwɨt kuldʼiŋ me iːllot. </ta>
            <ta e="T38" id="Seg_826" s="T30">me assə nɨkarkuwot nʼäːqnɨt qala me nʼäwat ewalǯimbot. </ta>
            <ta e="T43" id="Seg_827" s="T38">qosi qaɣi me nʼäwat qonǯurčenǯat. </ta>
            <ta e="T50" id="Seg_828" s="T43">oːpat qojjat tipeqowaj, täpqiː qala araŋ quːlla. </ta>
            <ta e="T52" id="Seg_829" s="T50">assə nɨkarquwti. </ta>
            <ta e="T55" id="Seg_830" s="T52">nänɨ ewat qassaj. </ta>
            <ta e="T62" id="Seg_831" s="T55">täpɨ qwatukuŋ, ɨkkə nɨkarkut esändɨni (i) qojjadni. </ta>
            <ta e="T67" id="Seg_832" s="T62">ass qoːnaŋ tädni kəːtčam äwemtʼät. </ta>
            <ta e="T69" id="Seg_833" s="T67">kundoqɨn qumba. </ta>
            <ta e="T72" id="Seg_834" s="T69">qosi titam tämba. </ta>
            <ta e="T78" id="Seg_835" s="T72">äwem tita qwɔntqumba muqtut saːroj päatə. </ta>
            <ta e="T82" id="Seg_836" s="T78">meŋa čarakuŋ: näːkar näːkaːrkut. </ta>
            <ta e="T87" id="Seg_837" s="T82">meŋa titam assə kundɨ iːləku. </ta>
            <ta e="T93" id="Seg_838" s="T87">qallo: meŋnan šɨttə iːw, okkə näw. </ta>
            <ta e="T96" id="Seg_839" s="T93">täpɨ ilaŋ nändɨnan. </ta>
            <ta e="T103" id="Seg_840" s="T96">qoːsi čarakus: man täni tüːkunǯaŋ sindat manǯeku. </ta>
            <ta e="T108" id="Seg_841" s="T103">manǯe man ässäm qumba kundoqɨn. </ta>
            <ta e="T115" id="Seg_842" s="T108">somblʼe saːrum pɨätɨ iːlas, säː saːrum pɨat. </ta>
            <ta e="T125" id="Seg_843" s="T115">ass kundə opsä qüːtas, somblʼe kʼəjkʼöt (somblʼe köj kʼöt) čʼeːlat. </ta>
            <ta e="T129" id="Seg_844" s="T125">man naq čakə qwɔntqunǯaŋ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_845" s="T0">meŋ-nan</ta>
            <ta e="T2" id="Seg_846" s="T1">üːče-m</ta>
            <ta e="T3" id="Seg_847" s="T2">e-ga-n</ta>
            <ta e="T4" id="Seg_848" s="T3">okkar</ta>
            <ta e="T5" id="Seg_849" s="T4">üːče-m</ta>
            <ta e="T6" id="Seg_850" s="T5">tipe-kuw-aj</ta>
            <ta e="T7" id="Seg_851" s="T6">mas-sə</ta>
            <ta e="T8" id="Seg_852" s="T7">täp</ta>
            <ta e="T9" id="Seg_853" s="T8">iːla-n</ta>
            <ta e="T10" id="Seg_854" s="T9">äwə-gala-k</ta>
            <ta e="T11" id="Seg_855" s="T10">meː</ta>
            <ta e="T12" id="Seg_856" s="T11">malwum-bo-o</ta>
            <ta e="T13" id="Seg_857" s="T12">somblʼe</ta>
            <ta e="T14" id="Seg_858" s="T13">pɨa-t</ta>
            <ta e="T15" id="Seg_859" s="T14">nä-wɨ</ta>
            <ta e="T16" id="Seg_860" s="T15">ewa-d-nan</ta>
            <ta e="T17" id="Seg_861" s="T16">iːla-n</ta>
            <ta e="T18" id="Seg_862" s="T17">äwa-ndɨ-se</ta>
            <ta e="T19" id="Seg_863" s="T18">man</ta>
            <ta e="T20" id="Seg_864" s="T19">assə</ta>
            <ta e="T21" id="Seg_865" s="T20">tiːnno-wa-m</ta>
            <ta e="T22" id="Seg_866" s="T21">kuldi-ŋ</ta>
            <ta e="T23" id="Seg_867" s="T22">täp</ta>
            <ta e="T24" id="Seg_868" s="T23">iːla-n</ta>
            <ta e="T25" id="Seg_869" s="T24">täp</ta>
            <ta e="T26" id="Seg_870" s="T25">assɨ</ta>
            <ta e="T27" id="Seg_871" s="T26">tinnu-wɨ-t</ta>
            <ta e="T28" id="Seg_872" s="T27">kuldʼi-ŋ</ta>
            <ta e="T29" id="Seg_873" s="T28">me</ta>
            <ta e="T30" id="Seg_874" s="T29">iːll-ot</ta>
            <ta e="T31" id="Seg_875" s="T30">me</ta>
            <ta e="T32" id="Seg_876" s="T31">assə</ta>
            <ta e="T33" id="Seg_877" s="T32">nɨka-r-ku-wot</ta>
            <ta e="T34" id="Seg_878" s="T33">nʼäː-qnɨt</ta>
            <ta e="T35" id="Seg_879" s="T34">qala</ta>
            <ta e="T36" id="Seg_880" s="T35">me</ta>
            <ta e="T37" id="Seg_881" s="T36">nʼä-wat</ta>
            <ta e="T38" id="Seg_882" s="T37">ewalǯi-mb-ot</ta>
            <ta e="T39" id="Seg_883" s="T38">qosi</ta>
            <ta e="T40" id="Seg_884" s="T39">qaɣi</ta>
            <ta e="T41" id="Seg_885" s="T40">me</ta>
            <ta e="T42" id="Seg_886" s="T41">nʼä-wat</ta>
            <ta e="T43" id="Seg_887" s="T42">qonǯu-r-če-nǯ-at</ta>
            <ta e="T44" id="Seg_888" s="T43">oːpa-t</ta>
            <ta e="T45" id="Seg_889" s="T44">qojja-tɨ</ta>
            <ta e="T46" id="Seg_890" s="T45">tipe-qow-aj</ta>
            <ta e="T47" id="Seg_891" s="T46">täp-qiː</ta>
            <ta e="T48" id="Seg_892" s="T47">qala</ta>
            <ta e="T49" id="Seg_893" s="T48">araŋ</ta>
            <ta e="T50" id="Seg_894" s="T49">quːl-la</ta>
            <ta e="T51" id="Seg_895" s="T50">assə</ta>
            <ta e="T52" id="Seg_896" s="T51">nɨka-r-qu-w-ti</ta>
            <ta e="T53" id="Seg_897" s="T52">nä-nɨ</ta>
            <ta e="T54" id="Seg_898" s="T53">ewa-tɨ</ta>
            <ta e="T55" id="Seg_899" s="T54">qassa-j</ta>
            <ta e="T56" id="Seg_900" s="T55">täpɨ</ta>
            <ta e="T57" id="Seg_901" s="T56">qwatu-ku-ŋ</ta>
            <ta e="T58" id="Seg_902" s="T57">ɨkkə</ta>
            <ta e="T59" id="Seg_903" s="T58">nɨka-r-ku-t</ta>
            <ta e="T60" id="Seg_904" s="T59">esä-n-dɨ-ni</ta>
            <ta e="T61" id="Seg_905" s="T60">i</ta>
            <ta e="T62" id="Seg_906" s="T61">qojja-d-ni</ta>
            <ta e="T63" id="Seg_907" s="T62">ass</ta>
            <ta e="T64" id="Seg_908" s="T63">qoːnɨŋ</ta>
            <ta e="T65" id="Seg_909" s="T64">täd-ni</ta>
            <ta e="T66" id="Seg_910" s="T65">kəːt-ča-m</ta>
            <ta e="T130" id="Seg_911" s="T66">äwe-m</ta>
            <ta e="T67" id="Seg_912" s="T130">tʼät</ta>
            <ta e="T68" id="Seg_913" s="T67">kundo-qən</ta>
            <ta e="T69" id="Seg_914" s="T68">qu-mba</ta>
            <ta e="T70" id="Seg_915" s="T69">qosi</ta>
            <ta e="T71" id="Seg_916" s="T70">titam</ta>
            <ta e="T72" id="Seg_917" s="T71">tä-mba</ta>
            <ta e="T73" id="Seg_918" s="T72">äwe-mɨ</ta>
            <ta e="T74" id="Seg_919" s="T73">tita</ta>
            <ta e="T75" id="Seg_920" s="T74">qwɔnt-qu-mba</ta>
            <ta e="T76" id="Seg_921" s="T75">muqtut</ta>
            <ta e="T77" id="Seg_922" s="T76">saːr-o-j</ta>
            <ta e="T78" id="Seg_923" s="T77">päa-tə</ta>
            <ta e="T79" id="Seg_924" s="T78">meŋa</ta>
            <ta e="T80" id="Seg_925" s="T79">čara-ku-ŋ</ta>
            <ta e="T81" id="Seg_926" s="T80">näːkar</ta>
            <ta e="T82" id="Seg_927" s="T81">näːkaː-r-ku-t</ta>
            <ta e="T83" id="Seg_928" s="T82">meŋa</ta>
            <ta e="T84" id="Seg_929" s="T83">titam</ta>
            <ta e="T85" id="Seg_930" s="T84">assə</ta>
            <ta e="T86" id="Seg_931" s="T85">kundɨ</ta>
            <ta e="T87" id="Seg_932" s="T86">iːlə-ku</ta>
            <ta e="T88" id="Seg_933" s="T87">qallo</ta>
            <ta e="T89" id="Seg_934" s="T88">meŋ-nan</ta>
            <ta e="T90" id="Seg_935" s="T89">šɨttə</ta>
            <ta e="T91" id="Seg_936" s="T90">iː-w</ta>
            <ta e="T92" id="Seg_937" s="T91">okkə</ta>
            <ta e="T93" id="Seg_938" s="T92">nä-w</ta>
            <ta e="T94" id="Seg_939" s="T93">täpɨ</ta>
            <ta e="T95" id="Seg_940" s="T94">ila-ŋ</ta>
            <ta e="T96" id="Seg_941" s="T95">nä-ndɨ-nan</ta>
            <ta e="T97" id="Seg_942" s="T96">qoːsi</ta>
            <ta e="T98" id="Seg_943" s="T97">čara-ku-s</ta>
            <ta e="T99" id="Seg_944" s="T98">man</ta>
            <ta e="T100" id="Seg_945" s="T99">tä-ni</ta>
            <ta e="T101" id="Seg_946" s="T100">tüː-ku-nǯa-ŋ</ta>
            <ta e="T102" id="Seg_947" s="T101">%%</ta>
            <ta e="T103" id="Seg_948" s="T102">manǯe-ku</ta>
            <ta e="T104" id="Seg_949" s="T103">manǯ-e</ta>
            <ta e="T105" id="Seg_950" s="T104">man</ta>
            <ta e="T106" id="Seg_951" s="T105">ässä-mɨ</ta>
            <ta e="T107" id="Seg_952" s="T106">qu-mba</ta>
            <ta e="T108" id="Seg_953" s="T107">kundo-qən</ta>
            <ta e="T109" id="Seg_954" s="T108">somblʼe</ta>
            <ta e="T110" id="Seg_955" s="T109">*saːrum</ta>
            <ta e="T111" id="Seg_956" s="T110">pɨä-tɨ</ta>
            <ta e="T112" id="Seg_957" s="T111">iːla-s</ta>
            <ta e="T113" id="Seg_958" s="T112">säː</ta>
            <ta e="T114" id="Seg_959" s="T113">*saːrum</ta>
            <ta e="T115" id="Seg_960" s="T114">pɨa-t</ta>
            <ta e="T116" id="Seg_961" s="T115">ass</ta>
            <ta e="T117" id="Seg_962" s="T116">kundə</ta>
            <ta e="T118" id="Seg_963" s="T117">opsä</ta>
            <ta e="T119" id="Seg_964" s="T118">qüːta-s</ta>
            <ta e="T120" id="Seg_965" s="T119">somblʼe</ta>
            <ta e="T121" id="Seg_966" s="T120">kʼəj-kʼöt</ta>
            <ta e="T122" id="Seg_967" s="T121">somblʼe</ta>
            <ta e="T124" id="Seg_968" s="T123">kʼöt</ta>
            <ta e="T125" id="Seg_969" s="T124">čʼeːla-t</ta>
            <ta e="T126" id="Seg_970" s="T125">man</ta>
            <ta e="T127" id="Seg_971" s="T126">naq</ta>
            <ta e="T128" id="Seg_972" s="T127">čakə</ta>
            <ta e="T129" id="Seg_973" s="T128">qwɔnt-qu-nǯa-ŋ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_974" s="T0">man-nan</ta>
            <ta e="T2" id="Seg_975" s="T1">ütče-mɨ</ta>
            <ta e="T3" id="Seg_976" s="T2">eː-ŋɨ-ŋ</ta>
            <ta e="T4" id="Seg_977" s="T3">okkɨr</ta>
            <ta e="T5" id="Seg_978" s="T4">ütče-mɨ</ta>
            <ta e="T6" id="Seg_979" s="T5">tebe-qum-naj</ta>
            <ta e="T7" id="Seg_980" s="T6">man-se</ta>
            <ta e="T8" id="Seg_981" s="T7">tap</ta>
            <ta e="T9" id="Seg_982" s="T8">illɨ-n</ta>
            <ta e="T10" id="Seg_983" s="T9">äwa-gaːlɨ-k</ta>
            <ta e="T11" id="Seg_984" s="T10">meː</ta>
            <ta e="T12" id="Seg_985" s="T11">malwum-mbɨ-ut</ta>
            <ta e="T13" id="Seg_986" s="T12">sombɨlʼe</ta>
            <ta e="T14" id="Seg_987" s="T13">po-n</ta>
            <ta e="T15" id="Seg_988" s="T14">neː-mɨ</ta>
            <ta e="T16" id="Seg_989" s="T15">äwa-tɨ-nan</ta>
            <ta e="T17" id="Seg_990" s="T16">illɨ-n</ta>
            <ta e="T18" id="Seg_991" s="T17">äwa-ndɨ-se</ta>
            <ta e="T19" id="Seg_992" s="T18">man</ta>
            <ta e="T20" id="Seg_993" s="T19">assɨ</ta>
            <ta e="T21" id="Seg_994" s="T20">tinnɨ-ŋɨ-m</ta>
            <ta e="T22" id="Seg_995" s="T21">kulʼdi-k</ta>
            <ta e="T23" id="Seg_996" s="T22">tap</ta>
            <ta e="T24" id="Seg_997" s="T23">illɨ-n</ta>
            <ta e="T25" id="Seg_998" s="T24">tap</ta>
            <ta e="T26" id="Seg_999" s="T25">assɨ</ta>
            <ta e="T27" id="Seg_1000" s="T26">tinnɨ-ŋɨ-tɨ</ta>
            <ta e="T28" id="Seg_1001" s="T27">kulʼdi-k</ta>
            <ta e="T29" id="Seg_1002" s="T28">meː</ta>
            <ta e="T30" id="Seg_1003" s="T29">illɨ-ut</ta>
            <ta e="T31" id="Seg_1004" s="T30">meː</ta>
            <ta e="T32" id="Seg_1005" s="T31">assɨ</ta>
            <ta e="T33" id="Seg_1006" s="T32">nägɨ-r-kku-ut</ta>
            <ta e="T34" id="Seg_1007" s="T33">nʼaː-qnɨt</ta>
            <ta e="T35" id="Seg_1008" s="T34">qalla</ta>
            <ta e="T36" id="Seg_1009" s="T35">meː</ta>
            <ta e="T37" id="Seg_1010" s="T36">nʼaː-wɨt</ta>
            <ta e="T38" id="Seg_1011" s="T37">äwɨlǯi-mbɨ-ut</ta>
            <ta e="T39" id="Seg_1012" s="T38">qoːsi</ta>
            <ta e="T40" id="Seg_1013" s="T39">qaqä</ta>
            <ta e="T41" id="Seg_1014" s="T40">meː</ta>
            <ta e="T42" id="Seg_1015" s="T41">nʼaː-wɨt</ta>
            <ta e="T43" id="Seg_1016" s="T42">qonǯu-r-nče-nče-ut</ta>
            <ta e="T44" id="Seg_1017" s="T43">oba-tɨ</ta>
            <ta e="T45" id="Seg_1018" s="T44">qoja-tɨ</ta>
            <ta e="T46" id="Seg_1019" s="T45">tebe-qum-naj</ta>
            <ta e="T47" id="Seg_1020" s="T46">tap-qi</ta>
            <ta e="T48" id="Seg_1021" s="T47">qalla</ta>
            <ta e="T49" id="Seg_1022" s="T48">aːrɨŋ</ta>
            <ta e="T50" id="Seg_1023" s="T49">qum-la</ta>
            <ta e="T51" id="Seg_1024" s="T50">assɨ</ta>
            <ta e="T52" id="Seg_1025" s="T51">nägɨ-r-kku-ŋɨ-di</ta>
            <ta e="T53" id="Seg_1026" s="T52">neː-nɨ</ta>
            <ta e="T54" id="Seg_1027" s="T53">äwa-tɨ</ta>
            <ta e="T55" id="Seg_1028" s="T54">qassaːq-lʼ</ta>
            <ta e="T56" id="Seg_1029" s="T55">tap</ta>
            <ta e="T57" id="Seg_1030" s="T56">qwodɨ-kku-n</ta>
            <ta e="T58" id="Seg_1031" s="T57">ɨkɨ</ta>
            <ta e="T59" id="Seg_1032" s="T58">nägɨ-r-kku-ätɨ</ta>
            <ta e="T60" id="Seg_1033" s="T59">ässɨ-n-tɨ-nɨ</ta>
            <ta e="T61" id="Seg_1034" s="T60">i</ta>
            <ta e="T62" id="Seg_1035" s="T61">qoja-n-nɨ</ta>
            <ta e="T63" id="Seg_1036" s="T62">assɨ</ta>
            <ta e="T64" id="Seg_1037" s="T63">qoːnɨŋ</ta>
            <ta e="T65" id="Seg_1038" s="T64">tan-nɨ</ta>
            <ta e="T66" id="Seg_1039" s="T65">kät-nǯɨ-m</ta>
            <ta e="T130" id="Seg_1040" s="T66">äwa-mɨ</ta>
            <ta e="T67" id="Seg_1041" s="T130">tʼaːt</ta>
            <ta e="T68" id="Seg_1042" s="T67">kundɨ-qən</ta>
            <ta e="T69" id="Seg_1043" s="T68">quː-mbɨ</ta>
            <ta e="T70" id="Seg_1044" s="T69">qoːsi</ta>
            <ta e="T71" id="Seg_1045" s="T70">tiːtam</ta>
            <ta e="T72" id="Seg_1046" s="T71">tä-mbɨ</ta>
            <ta e="T73" id="Seg_1047" s="T72">äwa-mɨ</ta>
            <ta e="T74" id="Seg_1048" s="T73">tʼida</ta>
            <ta e="T75" id="Seg_1049" s="T74">qwändi-kku-mbɨ</ta>
            <ta e="T76" id="Seg_1050" s="T75">muktut</ta>
            <ta e="T77" id="Seg_1051" s="T76">saːrum-ɨ-lʼ</ta>
            <ta e="T78" id="Seg_1052" s="T77">po-tɨ</ta>
            <ta e="T79" id="Seg_1053" s="T78">mäkkä</ta>
            <ta e="T80" id="Seg_1054" s="T79">tʼarɨ-kku-n</ta>
            <ta e="T81" id="Seg_1055" s="T80">näkɨr</ta>
            <ta e="T82" id="Seg_1056" s="T81">nägɨ-r-kku-ätɨ</ta>
            <ta e="T83" id="Seg_1057" s="T82">mäkkä</ta>
            <ta e="T84" id="Seg_1058" s="T83">tiːtam</ta>
            <ta e="T85" id="Seg_1059" s="T84">assɨ</ta>
            <ta e="T86" id="Seg_1060" s="T85">kundɨ</ta>
            <ta e="T87" id="Seg_1061" s="T86">illɨ-gu</ta>
            <ta e="T88" id="Seg_1062" s="T87">qal</ta>
            <ta e="T89" id="Seg_1063" s="T88">man-nan</ta>
            <ta e="T90" id="Seg_1064" s="T89">šittə</ta>
            <ta e="T91" id="Seg_1065" s="T90">iː-mɨ</ta>
            <ta e="T92" id="Seg_1066" s="T91">okkɨr</ta>
            <ta e="T93" id="Seg_1067" s="T92">neː-mɨ</ta>
            <ta e="T94" id="Seg_1068" s="T93">tap</ta>
            <ta e="T95" id="Seg_1069" s="T94">illɨ-ŋ</ta>
            <ta e="T96" id="Seg_1070" s="T95">neː-ndɨ-nan</ta>
            <ta e="T97" id="Seg_1071" s="T96">kos</ta>
            <ta e="T98" id="Seg_1072" s="T97">tʼarɨ-kku-sɨ</ta>
            <ta e="T99" id="Seg_1073" s="T98">man</ta>
            <ta e="T100" id="Seg_1074" s="T99">tɛː-nɨ</ta>
            <ta e="T101" id="Seg_1075" s="T100">tüː-kku-nǯɨ-ŋ</ta>
            <ta e="T102" id="Seg_1076" s="T101">%%</ta>
            <ta e="T103" id="Seg_1077" s="T102">manǯɨ-gu</ta>
            <ta e="T104" id="Seg_1078" s="T103">manǯɨ-ätɨ</ta>
            <ta e="T105" id="Seg_1079" s="T104">man</ta>
            <ta e="T106" id="Seg_1080" s="T105">ässɨ-mɨ</ta>
            <ta e="T107" id="Seg_1081" s="T106">quː-mbɨ</ta>
            <ta e="T108" id="Seg_1082" s="T107">kundɨ-qən</ta>
            <ta e="T109" id="Seg_1083" s="T108">sombɨlʼe</ta>
            <ta e="T110" id="Seg_1084" s="T109">saːrum</ta>
            <ta e="T111" id="Seg_1085" s="T110">po-tɨ</ta>
            <ta e="T112" id="Seg_1086" s="T111">illɨ-sɨ</ta>
            <ta e="T113" id="Seg_1087" s="T112">säː</ta>
            <ta e="T114" id="Seg_1088" s="T113">saːrum</ta>
            <ta e="T115" id="Seg_1089" s="T114">po-tɨ</ta>
            <ta e="T116" id="Seg_1090" s="T115">assɨ</ta>
            <ta e="T117" id="Seg_1091" s="T116">kundɨ</ta>
            <ta e="T118" id="Seg_1092" s="T117">opsä</ta>
            <ta e="T119" id="Seg_1093" s="T118">küːdɨ-sɨ</ta>
            <ta e="T120" id="Seg_1094" s="T119">sombɨlʼe</ta>
            <ta e="T121" id="Seg_1095" s="T120">kʼəj-köt</ta>
            <ta e="T122" id="Seg_1096" s="T121">sombɨlʼe</ta>
            <ta e="T124" id="Seg_1097" s="T123">köːt</ta>
            <ta e="T125" id="Seg_1098" s="T124">tʼeːlɨ-tɨ</ta>
            <ta e="T126" id="Seg_1099" s="T125">man</ta>
            <ta e="T127" id="Seg_1100" s="T126">naj</ta>
            <ta e="T128" id="Seg_1101" s="T127">tʼak</ta>
            <ta e="T129" id="Seg_1102" s="T128">qwändɨ-kku-nǯɨ-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1103" s="T0">I.NOM-ADES</ta>
            <ta e="T2" id="Seg_1104" s="T1">child-1SG</ta>
            <ta e="T3" id="Seg_1105" s="T2">be-CO-1SG.S</ta>
            <ta e="T4" id="Seg_1106" s="T3">one</ta>
            <ta e="T5" id="Seg_1107" s="T4">child-1SG</ta>
            <ta e="T6" id="Seg_1108" s="T5">man-human.being-EMPH</ta>
            <ta e="T7" id="Seg_1109" s="T6">I.NOM-COM</ta>
            <ta e="T8" id="Seg_1110" s="T7">(s)he.[NOM]</ta>
            <ta e="T9" id="Seg_1111" s="T8">live-3SG.S</ta>
            <ta e="T10" id="Seg_1112" s="T9">mother-CAR-ADVZ</ta>
            <ta e="T11" id="Seg_1113" s="T10">we.NOM</ta>
            <ta e="T12" id="Seg_1114" s="T11">split-PST.NAR-1PL</ta>
            <ta e="T13" id="Seg_1115" s="T12">five</ta>
            <ta e="T14" id="Seg_1116" s="T13">year-ADV.LOC</ta>
            <ta e="T15" id="Seg_1117" s="T14">daughter-1SG</ta>
            <ta e="T16" id="Seg_1118" s="T15">mother-3SG-ADES</ta>
            <ta e="T17" id="Seg_1119" s="T16">live-3SG.S</ta>
            <ta e="T18" id="Seg_1120" s="T17">mother-OBL.3SG-COM</ta>
            <ta e="T19" id="Seg_1121" s="T18">I.NOM</ta>
            <ta e="T20" id="Seg_1122" s="T19">NEG</ta>
            <ta e="T21" id="Seg_1123" s="T20">know-CO-1SG.O</ta>
            <ta e="T22" id="Seg_1124" s="T21">which-ADVZ</ta>
            <ta e="T23" id="Seg_1125" s="T22">(s)he.[NOM]</ta>
            <ta e="T24" id="Seg_1126" s="T23">live-3SG.S</ta>
            <ta e="T25" id="Seg_1127" s="T24">(s)he.[NOM]</ta>
            <ta e="T26" id="Seg_1128" s="T25">NEG</ta>
            <ta e="T27" id="Seg_1129" s="T26">know-CO-3SG.O</ta>
            <ta e="T28" id="Seg_1130" s="T27">which-ADVZ</ta>
            <ta e="T29" id="Seg_1131" s="T28">we.NOM</ta>
            <ta e="T30" id="Seg_1132" s="T29">live-1PL</ta>
            <ta e="T31" id="Seg_1133" s="T30">we.NOM</ta>
            <ta e="T32" id="Seg_1134" s="T31">NEG</ta>
            <ta e="T33" id="Seg_1135" s="T32">write-FRQ-HAB-1PL</ta>
            <ta e="T34" id="Seg_1136" s="T33">friend-%%</ta>
            <ta e="T35" id="Seg_1137" s="T34">how</ta>
            <ta e="T36" id="Seg_1138" s="T35">we.NOM</ta>
            <ta e="T37" id="Seg_1139" s="T36">friend-1PL</ta>
            <ta e="T38" id="Seg_1140" s="T37">forget-DUR-1PL</ta>
            <ta e="T39" id="Seg_1141" s="T38">probably</ta>
            <ta e="T40" id="Seg_1142" s="T39">when</ta>
            <ta e="T41" id="Seg_1143" s="T40">we.NOM</ta>
            <ta e="T42" id="Seg_1144" s="T41">friend-1PL</ta>
            <ta e="T43" id="Seg_1145" s="T42">appear-FRQ-IPFV3-IPFV3-1PL</ta>
            <ta e="T44" id="Seg_1146" s="T43">sister.[NOM]-3SG</ta>
            <ta e="T45" id="Seg_1147" s="T44">younger.sibling.[NOM]-3SG</ta>
            <ta e="T46" id="Seg_1148" s="T45">man-human.being-EMPH</ta>
            <ta e="T47" id="Seg_1149" s="T46">(s)he-DU.[NOM]</ta>
            <ta e="T48" id="Seg_1150" s="T47">how</ta>
            <ta e="T49" id="Seg_1151" s="T48">foreign</ta>
            <ta e="T50" id="Seg_1152" s="T49">human.being-PL.[NOM]</ta>
            <ta e="T51" id="Seg_1153" s="T50">NEG</ta>
            <ta e="T52" id="Seg_1154" s="T51">write-FRQ-HAB-CO-3DU.O</ta>
            <ta e="T53" id="Seg_1155" s="T52">daughter-GEN.1SG</ta>
            <ta e="T54" id="Seg_1156" s="T53">mother.[NOM]-3SG</ta>
            <ta e="T55" id="Seg_1157" s="T54">Russian-ADJZ</ta>
            <ta e="T56" id="Seg_1158" s="T55">(s)he.[NOM]</ta>
            <ta e="T57" id="Seg_1159" s="T56">scold-HAB-3SG.S</ta>
            <ta e="T58" id="Seg_1160" s="T57">NEG.IMP</ta>
            <ta e="T59" id="Seg_1161" s="T58">write-FRQ-HAB-IMP.2SG.O</ta>
            <ta e="T60" id="Seg_1162" s="T59">father-GEN-3SG-ALL</ta>
            <ta e="T61" id="Seg_1163" s="T60">and</ta>
            <ta e="T62" id="Seg_1164" s="T61">younger.sibling-GEN-ALL</ta>
            <ta e="T63" id="Seg_1165" s="T62">NEG</ta>
            <ta e="T64" id="Seg_1166" s="T63">many</ta>
            <ta e="T65" id="Seg_1167" s="T64">you.SG.NOM-ALL</ta>
            <ta e="T66" id="Seg_1168" s="T65">say-FUT-1SG.O</ta>
            <ta e="T130" id="Seg_1169" s="T66">mother-1SG</ta>
            <ta e="T67" id="Seg_1170" s="T130">about</ta>
            <ta e="T68" id="Seg_1171" s="T67">long-LOC</ta>
            <ta e="T69" id="Seg_1172" s="T68">die-PST.NAR.[3SG.S]</ta>
            <ta e="T70" id="Seg_1173" s="T69">probably</ta>
            <ta e="T71" id="Seg_1174" s="T70">now</ta>
            <ta e="T72" id="Seg_1175" s="T71">rot-PST.NAR.[NOM]</ta>
            <ta e="T73" id="Seg_1176" s="T72">mother-1SG</ta>
            <ta e="T74" id="Seg_1177" s="T73">now</ta>
            <ta e="T75" id="Seg_1178" s="T74">old-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T76" id="Seg_1179" s="T75">six</ta>
            <ta e="T77" id="Seg_1180" s="T76">ten-EP-ADJZ</ta>
            <ta e="T78" id="Seg_1181" s="T77">year-3SG</ta>
            <ta e="T79" id="Seg_1182" s="T78">I.ALL</ta>
            <ta e="T80" id="Seg_1183" s="T79">say-HAB-3SG.S</ta>
            <ta e="T81" id="Seg_1184" s="T80">letter.[NOM]</ta>
            <ta e="T82" id="Seg_1185" s="T81">write-FRQ-HAB-IMP.2SG.O</ta>
            <ta e="T83" id="Seg_1186" s="T82">I.ALL</ta>
            <ta e="T84" id="Seg_1187" s="T83">now</ta>
            <ta e="T85" id="Seg_1188" s="T84">NEG</ta>
            <ta e="T86" id="Seg_1189" s="T85">long</ta>
            <ta e="T87" id="Seg_1190" s="T86">live-INF</ta>
            <ta e="T88" id="Seg_1191" s="T87">it.is.said</ta>
            <ta e="T89" id="Seg_1192" s="T88">I.NOM-ADES</ta>
            <ta e="T90" id="Seg_1193" s="T89">two</ta>
            <ta e="T91" id="Seg_1194" s="T90">son.[NOM]-1SG</ta>
            <ta e="T92" id="Seg_1195" s="T91">one</ta>
            <ta e="T93" id="Seg_1196" s="T92">daughter.[NOM]-1SG</ta>
            <ta e="T94" id="Seg_1197" s="T93">(s)he.[NOM]</ta>
            <ta e="T95" id="Seg_1198" s="T94">live-1SG.S</ta>
            <ta e="T96" id="Seg_1199" s="T95">daughter-OBL.3SG-ADES</ta>
            <ta e="T97" id="Seg_1200" s="T96">INDEF3</ta>
            <ta e="T98" id="Seg_1201" s="T97">say-HAB-PST.[3SG.S]</ta>
            <ta e="T99" id="Seg_1202" s="T98">I.NOM</ta>
            <ta e="T100" id="Seg_1203" s="T99">you.DU.NOM-ALL</ta>
            <ta e="T101" id="Seg_1204" s="T100">come-HAB-FUT-1SG.S</ta>
            <ta e="T103" id="Seg_1205" s="T102">look-INF</ta>
            <ta e="T104" id="Seg_1206" s="T103">look-IMP.2SG.O</ta>
            <ta e="T105" id="Seg_1207" s="T104">I.NOM</ta>
            <ta e="T106" id="Seg_1208" s="T105">father.[NOM]-1SG</ta>
            <ta e="T107" id="Seg_1209" s="T106">die-PST.NAR.[3SG.S]</ta>
            <ta e="T108" id="Seg_1210" s="T107">long-LOC</ta>
            <ta e="T109" id="Seg_1211" s="T108">five</ta>
            <ta e="T110" id="Seg_1212" s="T109">ten</ta>
            <ta e="T111" id="Seg_1213" s="T110">year-3SG</ta>
            <ta e="T112" id="Seg_1214" s="T111">live-PST.[3SG.S]</ta>
            <ta e="T113" id="Seg_1215" s="T112">%%</ta>
            <ta e="T114" id="Seg_1216" s="T113">ten</ta>
            <ta e="T115" id="Seg_1217" s="T114">year-3SG</ta>
            <ta e="T116" id="Seg_1218" s="T115">NEG</ta>
            <ta e="T117" id="Seg_1219" s="T116">long</ta>
            <ta e="T118" id="Seg_1220" s="T117">strongly</ta>
            <ta e="T119" id="Seg_1221" s="T118">be.ill-PST.[3SG.S]</ta>
            <ta e="T120" id="Seg_1222" s="T119">five</ta>
            <ta e="T121" id="Seg_1223" s="T120">superfluous-ten</ta>
            <ta e="T122" id="Seg_1224" s="T121">five</ta>
            <ta e="T124" id="Seg_1225" s="T123">ten</ta>
            <ta e="T125" id="Seg_1226" s="T124">day-3SG</ta>
            <ta e="T126" id="Seg_1227" s="T125">I.NOM</ta>
            <ta e="T127" id="Seg_1228" s="T126">also</ta>
            <ta e="T128" id="Seg_1229" s="T127">fast</ta>
            <ta e="T129" id="Seg_1230" s="T128">become.old-HAB-FUT-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1231" s="T0">я.NOM-ADES</ta>
            <ta e="T2" id="Seg_1232" s="T1">ребёнок-1SG</ta>
            <ta e="T3" id="Seg_1233" s="T2">быть-CO-1SG.S</ta>
            <ta e="T4" id="Seg_1234" s="T3">один</ta>
            <ta e="T5" id="Seg_1235" s="T4">ребёнок-1SG</ta>
            <ta e="T6" id="Seg_1236" s="T5">мужчина-человек-EMPH</ta>
            <ta e="T7" id="Seg_1237" s="T6">я.NOM-COM</ta>
            <ta e="T8" id="Seg_1238" s="T7">он(а).[NOM]</ta>
            <ta e="T9" id="Seg_1239" s="T8">жить-3SG.S</ta>
            <ta e="T10" id="Seg_1240" s="T9">мать-CAR-ADVZ</ta>
            <ta e="T11" id="Seg_1241" s="T10">мы.NOM</ta>
            <ta e="T12" id="Seg_1242" s="T11">разделиться-PST.NAR-1PL</ta>
            <ta e="T13" id="Seg_1243" s="T12">пять</ta>
            <ta e="T14" id="Seg_1244" s="T13">год-ADV.LOC</ta>
            <ta e="T15" id="Seg_1245" s="T14">дочь-1SG</ta>
            <ta e="T16" id="Seg_1246" s="T15">мать-3SG-ADES</ta>
            <ta e="T17" id="Seg_1247" s="T16">жить-3SG.S</ta>
            <ta e="T18" id="Seg_1248" s="T17">мать-OBL.3SG-COM</ta>
            <ta e="T19" id="Seg_1249" s="T18">я.NOM</ta>
            <ta e="T20" id="Seg_1250" s="T19">NEG</ta>
            <ta e="T21" id="Seg_1251" s="T20">знать-CO-1SG.O</ta>
            <ta e="T22" id="Seg_1252" s="T21">какой-ADVZ</ta>
            <ta e="T23" id="Seg_1253" s="T22">он(а).[NOM]</ta>
            <ta e="T24" id="Seg_1254" s="T23">жить-3SG.S</ta>
            <ta e="T25" id="Seg_1255" s="T24">он(а).[NOM]</ta>
            <ta e="T26" id="Seg_1256" s="T25">NEG</ta>
            <ta e="T27" id="Seg_1257" s="T26">знать-CO-3SG.O</ta>
            <ta e="T28" id="Seg_1258" s="T27">какой-ADVZ</ta>
            <ta e="T29" id="Seg_1259" s="T28">мы.NOM</ta>
            <ta e="T30" id="Seg_1260" s="T29">жить-1PL</ta>
            <ta e="T31" id="Seg_1261" s="T30">мы.NOM</ta>
            <ta e="T32" id="Seg_1262" s="T31">NEG</ta>
            <ta e="T33" id="Seg_1263" s="T32">писать-FRQ-HAB-1PL</ta>
            <ta e="T34" id="Seg_1264" s="T33">друг-%%</ta>
            <ta e="T35" id="Seg_1265" s="T34">как</ta>
            <ta e="T36" id="Seg_1266" s="T35">мы.NOM</ta>
            <ta e="T37" id="Seg_1267" s="T36">друг-1PL</ta>
            <ta e="T38" id="Seg_1268" s="T37">забыть-DUR-1PL</ta>
            <ta e="T39" id="Seg_1269" s="T38">наверно</ta>
            <ta e="T40" id="Seg_1270" s="T39">когда</ta>
            <ta e="T41" id="Seg_1271" s="T40">мы.NOM</ta>
            <ta e="T42" id="Seg_1272" s="T41">друг-1PL</ta>
            <ta e="T43" id="Seg_1273" s="T42">показаться-FRQ-IPFV3-IPFV3-1PL</ta>
            <ta e="T44" id="Seg_1274" s="T43">сестра.[NOM]-3SG</ta>
            <ta e="T45" id="Seg_1275" s="T44">младший.брат/сестра.[NOM]-3SG</ta>
            <ta e="T46" id="Seg_1276" s="T45">мужчина-человек-EMPH</ta>
            <ta e="T47" id="Seg_1277" s="T46">он(а)-DU.[NOM]</ta>
            <ta e="T48" id="Seg_1278" s="T47">как</ta>
            <ta e="T49" id="Seg_1279" s="T48">чужой</ta>
            <ta e="T50" id="Seg_1280" s="T49">человек-PL.[NOM]</ta>
            <ta e="T51" id="Seg_1281" s="T50">NEG</ta>
            <ta e="T52" id="Seg_1282" s="T51">писать-FRQ-HAB-CO-3DU.O</ta>
            <ta e="T53" id="Seg_1283" s="T52">дочь-GEN.1SG</ta>
            <ta e="T54" id="Seg_1284" s="T53">мать.[NOM]-3SG</ta>
            <ta e="T55" id="Seg_1285" s="T54">русский-ADJZ</ta>
            <ta e="T56" id="Seg_1286" s="T55">он(а).[NOM]</ta>
            <ta e="T57" id="Seg_1287" s="T56">выругать-HAB-3SG.S</ta>
            <ta e="T58" id="Seg_1288" s="T57">NEG.IMP</ta>
            <ta e="T59" id="Seg_1289" s="T58">писать-FRQ-HAB-IMP.2SG.O</ta>
            <ta e="T60" id="Seg_1290" s="T59">отец-GEN-3SG-ALL</ta>
            <ta e="T61" id="Seg_1291" s="T60">и</ta>
            <ta e="T62" id="Seg_1292" s="T61">младший.брат/сестра-GEN-ALL</ta>
            <ta e="T63" id="Seg_1293" s="T62">NEG</ta>
            <ta e="T64" id="Seg_1294" s="T63">много</ta>
            <ta e="T65" id="Seg_1295" s="T64">ты.NOM-ALL</ta>
            <ta e="T66" id="Seg_1296" s="T65">сказать-FUT-1SG.O</ta>
            <ta e="T130" id="Seg_1297" s="T66">мать-1SG</ta>
            <ta e="T67" id="Seg_1298" s="T130">про</ta>
            <ta e="T68" id="Seg_1299" s="T67">долго-LOC</ta>
            <ta e="T69" id="Seg_1300" s="T68">умереть-PST.NAR.[3SG.S]</ta>
            <ta e="T70" id="Seg_1301" s="T69">наверно</ta>
            <ta e="T71" id="Seg_1302" s="T70">сейчас</ta>
            <ta e="T72" id="Seg_1303" s="T71">сгнить-PST.NAR.[NOM]</ta>
            <ta e="T73" id="Seg_1304" s="T72">мать-1SG</ta>
            <ta e="T74" id="Seg_1305" s="T73">сейчас</ta>
            <ta e="T75" id="Seg_1306" s="T74">старый-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T76" id="Seg_1307" s="T75">шесть</ta>
            <ta e="T77" id="Seg_1308" s="T76">десять-EP-ADJZ</ta>
            <ta e="T78" id="Seg_1309" s="T77">год-3SG</ta>
            <ta e="T79" id="Seg_1310" s="T78">я.ALL</ta>
            <ta e="T80" id="Seg_1311" s="T79">сказать-HAB-3SG.S</ta>
            <ta e="T81" id="Seg_1312" s="T80">письмо.[NOM]</ta>
            <ta e="T82" id="Seg_1313" s="T81">писать-FRQ-HAB-IMP.2SG.O</ta>
            <ta e="T83" id="Seg_1314" s="T82">я.ALL</ta>
            <ta e="T84" id="Seg_1315" s="T83">сейчас</ta>
            <ta e="T85" id="Seg_1316" s="T84">NEG</ta>
            <ta e="T86" id="Seg_1317" s="T85">долго</ta>
            <ta e="T87" id="Seg_1318" s="T86">жить-INF</ta>
            <ta e="T88" id="Seg_1319" s="T87">мол</ta>
            <ta e="T89" id="Seg_1320" s="T88">я.NOM-ADES</ta>
            <ta e="T90" id="Seg_1321" s="T89">два</ta>
            <ta e="T91" id="Seg_1322" s="T90">сын.[NOM]-1SG</ta>
            <ta e="T92" id="Seg_1323" s="T91">один</ta>
            <ta e="T93" id="Seg_1324" s="T92">дочь.[NOM]-1SG</ta>
            <ta e="T94" id="Seg_1325" s="T93">он(а).[NOM]</ta>
            <ta e="T95" id="Seg_1326" s="T94">жить-1SG.S</ta>
            <ta e="T96" id="Seg_1327" s="T95">дочь-OBL.3SG-ADES</ta>
            <ta e="T97" id="Seg_1328" s="T96">INDEF3</ta>
            <ta e="T98" id="Seg_1329" s="T97">сказать-HAB-PST.[3SG.S]</ta>
            <ta e="T99" id="Seg_1330" s="T98">я.NOM</ta>
            <ta e="T100" id="Seg_1331" s="T99">вы.DU.NOM-ALL</ta>
            <ta e="T101" id="Seg_1332" s="T100">прийти-HAB-FUT-1SG.S</ta>
            <ta e="T103" id="Seg_1333" s="T102">смотреть-INF</ta>
            <ta e="T104" id="Seg_1334" s="T103">смотреть-IMP.2SG.O</ta>
            <ta e="T105" id="Seg_1335" s="T104">я.NOM</ta>
            <ta e="T106" id="Seg_1336" s="T105">отец.[NOM]-1SG</ta>
            <ta e="T107" id="Seg_1337" s="T106">умереть-PST.NAR.[3SG.S]</ta>
            <ta e="T108" id="Seg_1338" s="T107">долго-LOC</ta>
            <ta e="T109" id="Seg_1339" s="T108">пять</ta>
            <ta e="T110" id="Seg_1340" s="T109">десять</ta>
            <ta e="T111" id="Seg_1341" s="T110">год-3SG</ta>
            <ta e="T112" id="Seg_1342" s="T111">жить-PST.[3SG.S]</ta>
            <ta e="T113" id="Seg_1343" s="T112">%%</ta>
            <ta e="T114" id="Seg_1344" s="T113">десять</ta>
            <ta e="T115" id="Seg_1345" s="T114">год-3SG</ta>
            <ta e="T116" id="Seg_1346" s="T115">NEG</ta>
            <ta e="T117" id="Seg_1347" s="T116">долго</ta>
            <ta e="T118" id="Seg_1348" s="T117">сильно</ta>
            <ta e="T119" id="Seg_1349" s="T118">болеть-PST.[3SG.S]</ta>
            <ta e="T120" id="Seg_1350" s="T119">пять</ta>
            <ta e="T121" id="Seg_1351" s="T120">лишний-десять</ta>
            <ta e="T122" id="Seg_1352" s="T121">пять</ta>
            <ta e="T124" id="Seg_1353" s="T123">десять</ta>
            <ta e="T125" id="Seg_1354" s="T124">день-3SG</ta>
            <ta e="T126" id="Seg_1355" s="T125">я.NOM</ta>
            <ta e="T127" id="Seg_1356" s="T126">тоже</ta>
            <ta e="T128" id="Seg_1357" s="T127">быстро</ta>
            <ta e="T129" id="Seg_1358" s="T128">стареть-HAB-FUT-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1359" s="T0">pers-n:case</ta>
            <ta e="T2" id="Seg_1360" s="T1">n-n:poss</ta>
            <ta e="T3" id="Seg_1361" s="T2">v-v:ins-v:pn</ta>
            <ta e="T4" id="Seg_1362" s="T3">num</ta>
            <ta e="T5" id="Seg_1363" s="T4">n-n:poss</ta>
            <ta e="T6" id="Seg_1364" s="T5">n-n-clit</ta>
            <ta e="T7" id="Seg_1365" s="T6">pers-n:case</ta>
            <ta e="T8" id="Seg_1366" s="T7">pers-n:case</ta>
            <ta e="T9" id="Seg_1367" s="T8">v-v:pn</ta>
            <ta e="T10" id="Seg_1368" s="T9">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T11" id="Seg_1369" s="T10">pers</ta>
            <ta e="T12" id="Seg_1370" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_1371" s="T12">num</ta>
            <ta e="T14" id="Seg_1372" s="T13">n-adv:case</ta>
            <ta e="T15" id="Seg_1373" s="T14">n-n:poss</ta>
            <ta e="T16" id="Seg_1374" s="T15">n-n:poss-n:case</ta>
            <ta e="T17" id="Seg_1375" s="T16">v-v:pn</ta>
            <ta e="T18" id="Seg_1376" s="T17">n-n:obl.poss-n:case</ta>
            <ta e="T19" id="Seg_1377" s="T18">pers</ta>
            <ta e="T20" id="Seg_1378" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_1379" s="T20">v-v:ins-v:pn</ta>
            <ta e="T22" id="Seg_1380" s="T21">interrog-adj&gt;adv</ta>
            <ta e="T23" id="Seg_1381" s="T22">pers-n:case</ta>
            <ta e="T24" id="Seg_1382" s="T23">v-v:pn</ta>
            <ta e="T25" id="Seg_1383" s="T24">pers-n:case</ta>
            <ta e="T26" id="Seg_1384" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_1385" s="T26">v-v:ins-v:pn</ta>
            <ta e="T28" id="Seg_1386" s="T27">interrog-adj&gt;adv</ta>
            <ta e="T29" id="Seg_1387" s="T28">pers</ta>
            <ta e="T30" id="Seg_1388" s="T29">v-v:pn</ta>
            <ta e="T31" id="Seg_1389" s="T30">pers</ta>
            <ta e="T32" id="Seg_1390" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_1391" s="T32">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T34" id="Seg_1392" s="T33">n-%%</ta>
            <ta e="T35" id="Seg_1393" s="T34">conj</ta>
            <ta e="T36" id="Seg_1394" s="T35">pers</ta>
            <ta e="T37" id="Seg_1395" s="T36">n-n:poss</ta>
            <ta e="T38" id="Seg_1396" s="T37">v-v&gt;v-v:pn</ta>
            <ta e="T39" id="Seg_1397" s="T38">adv</ta>
            <ta e="T40" id="Seg_1398" s="T39">conj</ta>
            <ta e="T41" id="Seg_1399" s="T40">pers</ta>
            <ta e="T42" id="Seg_1400" s="T41">n-n:poss</ta>
            <ta e="T43" id="Seg_1401" s="T42">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T44" id="Seg_1402" s="T43">n-n:case-n:poss</ta>
            <ta e="T45" id="Seg_1403" s="T44">n-n:case-n:poss</ta>
            <ta e="T46" id="Seg_1404" s="T45">n-n-clit</ta>
            <ta e="T47" id="Seg_1405" s="T46">pers-n:num-n:case</ta>
            <ta e="T48" id="Seg_1406" s="T47">conj</ta>
            <ta e="T49" id="Seg_1407" s="T48">adj</ta>
            <ta e="T50" id="Seg_1408" s="T49">n-n:num-n:case</ta>
            <ta e="T51" id="Seg_1409" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_1410" s="T51">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T53" id="Seg_1411" s="T52">n-n:obl.poss</ta>
            <ta e="T54" id="Seg_1412" s="T53">n-n:case-n:poss</ta>
            <ta e="T55" id="Seg_1413" s="T54">n-n&gt;adj</ta>
            <ta e="T56" id="Seg_1414" s="T55">pers-n:case</ta>
            <ta e="T57" id="Seg_1415" s="T56">v-v&gt;v-v:pn</ta>
            <ta e="T58" id="Seg_1416" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_1417" s="T58">v-v&gt;v-v&gt;v-v:mood.pn</ta>
            <ta e="T60" id="Seg_1418" s="T59">n-n:case-n:poss-n:case</ta>
            <ta e="T61" id="Seg_1419" s="T60">conj</ta>
            <ta e="T62" id="Seg_1420" s="T61">n-n:case-n:case</ta>
            <ta e="T63" id="Seg_1421" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_1422" s="T63">quant</ta>
            <ta e="T65" id="Seg_1423" s="T64">pers-n:case</ta>
            <ta e="T66" id="Seg_1424" s="T65">v-v:tense-v:pn</ta>
            <ta e="T130" id="Seg_1425" s="T66">n-n:poss</ta>
            <ta e="T67" id="Seg_1426" s="T130">pp</ta>
            <ta e="T68" id="Seg_1427" s="T67">adv-n:case</ta>
            <ta e="T69" id="Seg_1428" s="T68">v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_1429" s="T69">adv</ta>
            <ta e="T71" id="Seg_1430" s="T70">adv</ta>
            <ta e="T72" id="Seg_1431" s="T71">v-v:tense-n:case</ta>
            <ta e="T73" id="Seg_1432" s="T72">n-n:poss</ta>
            <ta e="T74" id="Seg_1433" s="T73">adv</ta>
            <ta e="T75" id="Seg_1434" s="T74">adj-v&gt;v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_1435" s="T75">num</ta>
            <ta e="T77" id="Seg_1436" s="T76">num-n:ins-n&gt;adj</ta>
            <ta e="T78" id="Seg_1437" s="T77">n-n:poss</ta>
            <ta e="T79" id="Seg_1438" s="T78">pers</ta>
            <ta e="T80" id="Seg_1439" s="T79">v-v&gt;v-v:pn</ta>
            <ta e="T81" id="Seg_1440" s="T80">n-n:case</ta>
            <ta e="T82" id="Seg_1441" s="T81">v-v&gt;v-v&gt;v-v:mood.pn</ta>
            <ta e="T83" id="Seg_1442" s="T82">pers</ta>
            <ta e="T84" id="Seg_1443" s="T83">adv</ta>
            <ta e="T85" id="Seg_1444" s="T84">ptcl</ta>
            <ta e="T86" id="Seg_1445" s="T85">adv</ta>
            <ta e="T87" id="Seg_1446" s="T86">v-v:inf</ta>
            <ta e="T88" id="Seg_1447" s="T87">ptcl</ta>
            <ta e="T89" id="Seg_1448" s="T88">pers-n:case</ta>
            <ta e="T90" id="Seg_1449" s="T89">num</ta>
            <ta e="T91" id="Seg_1450" s="T90">n-n:case-n:poss</ta>
            <ta e="T92" id="Seg_1451" s="T91">num</ta>
            <ta e="T93" id="Seg_1452" s="T92">n-n:case-n:poss</ta>
            <ta e="T94" id="Seg_1453" s="T93">pers-n:case</ta>
            <ta e="T95" id="Seg_1454" s="T94">v-v:pn</ta>
            <ta e="T96" id="Seg_1455" s="T95">n-n:obl.poss-n:case</ta>
            <ta e="T97" id="Seg_1456" s="T96">clit</ta>
            <ta e="T98" id="Seg_1457" s="T97">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T99" id="Seg_1458" s="T98">pers</ta>
            <ta e="T100" id="Seg_1459" s="T99">pers-n:case</ta>
            <ta e="T101" id="Seg_1460" s="T100">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T103" id="Seg_1461" s="T102">v-v:inf</ta>
            <ta e="T104" id="Seg_1462" s="T103">v-v:mood.pn</ta>
            <ta e="T105" id="Seg_1463" s="T104">pers</ta>
            <ta e="T106" id="Seg_1464" s="T105">n-n:case-n:poss</ta>
            <ta e="T107" id="Seg_1465" s="T106">v-v:tense-v:pn</ta>
            <ta e="T108" id="Seg_1466" s="T107">adv-n:case</ta>
            <ta e="T109" id="Seg_1467" s="T108">num</ta>
            <ta e="T110" id="Seg_1468" s="T109">num</ta>
            <ta e="T111" id="Seg_1469" s="T110">n-n:poss</ta>
            <ta e="T112" id="Seg_1470" s="T111">v-v:tense-v:pn</ta>
            <ta e="T114" id="Seg_1471" s="T113">num</ta>
            <ta e="T115" id="Seg_1472" s="T114">n-n:poss</ta>
            <ta e="T116" id="Seg_1473" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_1474" s="T116">adv</ta>
            <ta e="T118" id="Seg_1475" s="T117">adv</ta>
            <ta e="T119" id="Seg_1476" s="T118">v-v:tense-v:pn</ta>
            <ta e="T120" id="Seg_1477" s="T119">num</ta>
            <ta e="T121" id="Seg_1478" s="T120">adj-num</ta>
            <ta e="T122" id="Seg_1479" s="T121">num</ta>
            <ta e="T124" id="Seg_1480" s="T123">num</ta>
            <ta e="T125" id="Seg_1481" s="T124">n-n:poss</ta>
            <ta e="T126" id="Seg_1482" s="T125">pers</ta>
            <ta e="T127" id="Seg_1483" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_1484" s="T127">adv</ta>
            <ta e="T129" id="Seg_1485" s="T128">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1486" s="T0">pers</ta>
            <ta e="T2" id="Seg_1487" s="T1">n</ta>
            <ta e="T3" id="Seg_1488" s="T2">v</ta>
            <ta e="T4" id="Seg_1489" s="T3">num</ta>
            <ta e="T5" id="Seg_1490" s="T4">n</ta>
            <ta e="T6" id="Seg_1491" s="T5">v</ta>
            <ta e="T7" id="Seg_1492" s="T6">pers</ta>
            <ta e="T8" id="Seg_1493" s="T7">pers</ta>
            <ta e="T9" id="Seg_1494" s="T8">v</ta>
            <ta e="T10" id="Seg_1495" s="T9">n</ta>
            <ta e="T11" id="Seg_1496" s="T10">pers</ta>
            <ta e="T12" id="Seg_1497" s="T11">v</ta>
            <ta e="T13" id="Seg_1498" s="T12">num</ta>
            <ta e="T14" id="Seg_1499" s="T13">n</ta>
            <ta e="T15" id="Seg_1500" s="T14">n</ta>
            <ta e="T16" id="Seg_1501" s="T15">n</ta>
            <ta e="T17" id="Seg_1502" s="T16">v</ta>
            <ta e="T18" id="Seg_1503" s="T17">n</ta>
            <ta e="T19" id="Seg_1504" s="T18">pers</ta>
            <ta e="T20" id="Seg_1505" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_1506" s="T20">v</ta>
            <ta e="T22" id="Seg_1507" s="T21">interrog</ta>
            <ta e="T23" id="Seg_1508" s="T22">pers</ta>
            <ta e="T24" id="Seg_1509" s="T23">v</ta>
            <ta e="T25" id="Seg_1510" s="T24">pers</ta>
            <ta e="T26" id="Seg_1511" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_1512" s="T26">v</ta>
            <ta e="T28" id="Seg_1513" s="T27">adv</ta>
            <ta e="T29" id="Seg_1514" s="T28">pers</ta>
            <ta e="T30" id="Seg_1515" s="T29">v</ta>
            <ta e="T31" id="Seg_1516" s="T30">pers</ta>
            <ta e="T32" id="Seg_1517" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_1518" s="T32">v</ta>
            <ta e="T34" id="Seg_1519" s="T33">n</ta>
            <ta e="T35" id="Seg_1520" s="T34">v</ta>
            <ta e="T36" id="Seg_1521" s="T35">pers</ta>
            <ta e="T37" id="Seg_1522" s="T36">n</ta>
            <ta e="T38" id="Seg_1523" s="T37">v</ta>
            <ta e="T39" id="Seg_1524" s="T38">adv</ta>
            <ta e="T40" id="Seg_1525" s="T39">interrog</ta>
            <ta e="T41" id="Seg_1526" s="T40">pers</ta>
            <ta e="T42" id="Seg_1527" s="T41">n</ta>
            <ta e="T43" id="Seg_1528" s="T42">v</ta>
            <ta e="T44" id="Seg_1529" s="T43">n</ta>
            <ta e="T45" id="Seg_1530" s="T44">n</ta>
            <ta e="T46" id="Seg_1531" s="T45">n</ta>
            <ta e="T47" id="Seg_1532" s="T46">n</ta>
            <ta e="T48" id="Seg_1533" s="T47">v</ta>
            <ta e="T49" id="Seg_1534" s="T48">adj</ta>
            <ta e="T50" id="Seg_1535" s="T49">n</ta>
            <ta e="T51" id="Seg_1536" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_1537" s="T51">v</ta>
            <ta e="T53" id="Seg_1538" s="T52">n</ta>
            <ta e="T54" id="Seg_1539" s="T53">n</ta>
            <ta e="T55" id="Seg_1540" s="T54">adj</ta>
            <ta e="T56" id="Seg_1541" s="T55">pers</ta>
            <ta e="T57" id="Seg_1542" s="T56">v</ta>
            <ta e="T58" id="Seg_1543" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_1544" s="T58">v</ta>
            <ta e="T60" id="Seg_1545" s="T59">n</ta>
            <ta e="T61" id="Seg_1546" s="T60">conj</ta>
            <ta e="T62" id="Seg_1547" s="T61">n</ta>
            <ta e="T63" id="Seg_1548" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_1549" s="T63">quant</ta>
            <ta e="T65" id="Seg_1550" s="T64">pers</ta>
            <ta e="T66" id="Seg_1551" s="T65">v</ta>
            <ta e="T130" id="Seg_1552" s="T66">n</ta>
            <ta e="T67" id="Seg_1553" s="T130">pp</ta>
            <ta e="T68" id="Seg_1554" s="T67">adv</ta>
            <ta e="T69" id="Seg_1555" s="T68">v</ta>
            <ta e="T70" id="Seg_1556" s="T69">adv</ta>
            <ta e="T71" id="Seg_1557" s="T70">adv</ta>
            <ta e="T72" id="Seg_1558" s="T71">v</ta>
            <ta e="T73" id="Seg_1559" s="T72">n</ta>
            <ta e="T74" id="Seg_1560" s="T73">adv</ta>
            <ta e="T75" id="Seg_1561" s="T74">v</ta>
            <ta e="T76" id="Seg_1562" s="T75">num</ta>
            <ta e="T77" id="Seg_1563" s="T76">adj</ta>
            <ta e="T78" id="Seg_1564" s="T77">n</ta>
            <ta e="T79" id="Seg_1565" s="T78">pers</ta>
            <ta e="T80" id="Seg_1566" s="T79">v</ta>
            <ta e="T81" id="Seg_1567" s="T80">n</ta>
            <ta e="T82" id="Seg_1568" s="T81">v</ta>
            <ta e="T83" id="Seg_1569" s="T82">pers</ta>
            <ta e="T84" id="Seg_1570" s="T83">adv</ta>
            <ta e="T85" id="Seg_1571" s="T84">ptcl</ta>
            <ta e="T86" id="Seg_1572" s="T85">adv</ta>
            <ta e="T87" id="Seg_1573" s="T86">v</ta>
            <ta e="T88" id="Seg_1574" s="T87">v</ta>
            <ta e="T89" id="Seg_1575" s="T88">pers</ta>
            <ta e="T90" id="Seg_1576" s="T89">num</ta>
            <ta e="T91" id="Seg_1577" s="T90">n</ta>
            <ta e="T92" id="Seg_1578" s="T91">num</ta>
            <ta e="T93" id="Seg_1579" s="T92">n</ta>
            <ta e="T94" id="Seg_1580" s="T93">pers</ta>
            <ta e="T95" id="Seg_1581" s="T94">v</ta>
            <ta e="T96" id="Seg_1582" s="T95">n</ta>
            <ta e="T97" id="Seg_1583" s="T96">clit</ta>
            <ta e="T98" id="Seg_1584" s="T97">v</ta>
            <ta e="T99" id="Seg_1585" s="T98">pers</ta>
            <ta e="T100" id="Seg_1586" s="T99">pers</ta>
            <ta e="T101" id="Seg_1587" s="T100">v</ta>
            <ta e="T103" id="Seg_1588" s="T102">v</ta>
            <ta e="T104" id="Seg_1589" s="T103">v</ta>
            <ta e="T105" id="Seg_1590" s="T104">pers</ta>
            <ta e="T106" id="Seg_1591" s="T105">n</ta>
            <ta e="T107" id="Seg_1592" s="T106">v</ta>
            <ta e="T108" id="Seg_1593" s="T107">adv</ta>
            <ta e="T109" id="Seg_1594" s="T108">num</ta>
            <ta e="T110" id="Seg_1595" s="T109">num</ta>
            <ta e="T111" id="Seg_1596" s="T110">n</ta>
            <ta e="T112" id="Seg_1597" s="T111">v</ta>
            <ta e="T113" id="Seg_1598" s="T112">n</ta>
            <ta e="T114" id="Seg_1599" s="T113">num</ta>
            <ta e="T115" id="Seg_1600" s="T114">n</ta>
            <ta e="T116" id="Seg_1601" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_1602" s="T116">adj</ta>
            <ta e="T118" id="Seg_1603" s="T117">adv</ta>
            <ta e="T119" id="Seg_1604" s="T118">v</ta>
            <ta e="T120" id="Seg_1605" s="T119">num</ta>
            <ta e="T121" id="Seg_1606" s="T120">num</ta>
            <ta e="T122" id="Seg_1607" s="T121">num</ta>
            <ta e="T124" id="Seg_1608" s="T123">num</ta>
            <ta e="T125" id="Seg_1609" s="T124">n</ta>
            <ta e="T126" id="Seg_1610" s="T125">pers</ta>
            <ta e="T127" id="Seg_1611" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_1612" s="T127">adv</ta>
            <ta e="T129" id="Seg_1613" s="T128">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1614" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_1615" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_1616" s="T4">np.h:S</ta>
            <ta e="T8" id="Seg_1617" s="T7">pro.h:S</ta>
            <ta e="T9" id="Seg_1618" s="T8">v:pred</ta>
            <ta e="T11" id="Seg_1619" s="T10">pro.h:S</ta>
            <ta e="T12" id="Seg_1620" s="T11">v:pred</ta>
            <ta e="T15" id="Seg_1621" s="T14">np.h:S</ta>
            <ta e="T17" id="Seg_1622" s="T16">v:pred</ta>
            <ta e="T19" id="Seg_1623" s="T18">pro.h:S</ta>
            <ta e="T21" id="Seg_1624" s="T20">v:pred</ta>
            <ta e="T24" id="Seg_1625" s="T21">s:compl</ta>
            <ta e="T25" id="Seg_1626" s="T24">pro.h:S</ta>
            <ta e="T27" id="Seg_1627" s="T26">v:pred</ta>
            <ta e="T30" id="Seg_1628" s="T27">s:compl</ta>
            <ta e="T31" id="Seg_1629" s="T30">pro.h:S</ta>
            <ta e="T33" id="Seg_1630" s="T32">v:pred</ta>
            <ta e="T38" id="Seg_1631" s="T34">s:compl</ta>
            <ta e="T41" id="Seg_1632" s="T40">pro.h:S</ta>
            <ta e="T42" id="Seg_1633" s="T41">np.h:O</ta>
            <ta e="T43" id="Seg_1634" s="T42">v:pred</ta>
            <ta e="T47" id="Seg_1635" s="T46">pro.h:S</ta>
            <ta e="T50" id="Seg_1636" s="T49">n:pred</ta>
            <ta e="T52" id="Seg_1637" s="T51">0.3.h:S v:pred</ta>
            <ta e="T54" id="Seg_1638" s="T53">np.h:S</ta>
            <ta e="T55" id="Seg_1639" s="T54">adj:pred</ta>
            <ta e="T56" id="Seg_1640" s="T55">pro.h:S</ta>
            <ta e="T57" id="Seg_1641" s="T56">v:pred</ta>
            <ta e="T59" id="Seg_1642" s="T58">0.2.h:S v:pred</ta>
            <ta e="T66" id="Seg_1643" s="T65">0.1.h:S v:pred</ta>
            <ta e="T69" id="Seg_1644" s="T68">0.3.h:S v:pred</ta>
            <ta e="T72" id="Seg_1645" s="T71">0.3.h:S v:pred</ta>
            <ta e="T73" id="Seg_1646" s="T72">np.h:S</ta>
            <ta e="T75" id="Seg_1647" s="T74">v:pred</ta>
            <ta e="T80" id="Seg_1648" s="T79">0.3.h:S v:pred</ta>
            <ta e="T81" id="Seg_1649" s="T80">np:O</ta>
            <ta e="T82" id="Seg_1650" s="T81">0.2.h:S v:pred</ta>
            <ta e="T87" id="Seg_1651" s="T86">v:S</ta>
            <ta e="T91" id="Seg_1652" s="T90">np.h:S</ta>
            <ta e="T93" id="Seg_1653" s="T92">np.h:S</ta>
            <ta e="T94" id="Seg_1654" s="T93">pro.h:S</ta>
            <ta e="T95" id="Seg_1655" s="T94">v:pred</ta>
            <ta e="T98" id="Seg_1656" s="T97">0.3.h:S v:pred</ta>
            <ta e="T99" id="Seg_1657" s="T98">pro.h:S</ta>
            <ta e="T101" id="Seg_1658" s="T100">v:pred</ta>
            <ta e="T103" id="Seg_1659" s="T101">s:purp</ta>
            <ta e="T104" id="Seg_1660" s="T103">0.2.h:S v:pred</ta>
            <ta e="T106" id="Seg_1661" s="T105">np.h:S</ta>
            <ta e="T107" id="Seg_1662" s="T106">v:pred</ta>
            <ta e="T112" id="Seg_1663" s="T111">0.3.h:S v:pred</ta>
            <ta e="T119" id="Seg_1664" s="T118">0.3.h:S v:pred</ta>
            <ta e="T126" id="Seg_1665" s="T125">pro.h:S</ta>
            <ta e="T129" id="Seg_1666" s="T128">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_1667" s="T0">pro.h:Poss</ta>
            <ta e="T2" id="Seg_1668" s="T1">np.h:Th</ta>
            <ta e="T5" id="Seg_1669" s="T4">np.h:Th</ta>
            <ta e="T7" id="Seg_1670" s="T6">pro:Com</ta>
            <ta e="T8" id="Seg_1671" s="T7">pro.h:Th</ta>
            <ta e="T11" id="Seg_1672" s="T10">pro.h:A</ta>
            <ta e="T14" id="Seg_1673" s="T13">adv:Time</ta>
            <ta e="T15" id="Seg_1674" s="T14">np.h:Th 0.1.h:Poss</ta>
            <ta e="T16" id="Seg_1675" s="T15">np.h:L 0.3.h:Poss</ta>
            <ta e="T18" id="Seg_1676" s="T17">np:Com 0.3.h:Poss</ta>
            <ta e="T19" id="Seg_1677" s="T18">pro.h:E</ta>
            <ta e="T23" id="Seg_1678" s="T22">pro.h:Th</ta>
            <ta e="T25" id="Seg_1679" s="T24">pro.h:E</ta>
            <ta e="T29" id="Seg_1680" s="T28">pro.h:Th</ta>
            <ta e="T31" id="Seg_1681" s="T30">pro.h:A</ta>
            <ta e="T36" id="Seg_1682" s="T35">pro.h:E</ta>
            <ta e="T37" id="Seg_1683" s="T36">np.h:Th</ta>
            <ta e="T41" id="Seg_1684" s="T40">pro.h:A</ta>
            <ta e="T42" id="Seg_1685" s="T41">np.h:Th</ta>
            <ta e="T47" id="Seg_1686" s="T46">pro.h:Th</ta>
            <ta e="T52" id="Seg_1687" s="T51">0.3.h:A</ta>
            <ta e="T53" id="Seg_1688" s="T52">np.h:Poss 0.1.h:Poss</ta>
            <ta e="T54" id="Seg_1689" s="T53">np.h:Th</ta>
            <ta e="T56" id="Seg_1690" s="T55">pro.h:A</ta>
            <ta e="T59" id="Seg_1691" s="T58">0.2.h:A</ta>
            <ta e="T60" id="Seg_1692" s="T59">np.h:R</ta>
            <ta e="T62" id="Seg_1693" s="T61">np.h:R</ta>
            <ta e="T65" id="Seg_1694" s="T64">pro.h:R</ta>
            <ta e="T66" id="Seg_1695" s="T65">0.1.h:A</ta>
            <ta e="T130" id="Seg_1696" s="T66">0.1.h:Poss</ta>
            <ta e="T68" id="Seg_1697" s="T67">adv:Time</ta>
            <ta e="T69" id="Seg_1698" s="T68">0.3.h:P</ta>
            <ta e="T71" id="Seg_1699" s="T70">adv:Time</ta>
            <ta e="T72" id="Seg_1700" s="T71">0.3.h:P</ta>
            <ta e="T73" id="Seg_1701" s="T72">np.h:Th 0.1.h:Poss</ta>
            <ta e="T74" id="Seg_1702" s="T73">adv:Time</ta>
            <ta e="T79" id="Seg_1703" s="T78">pro.h:R</ta>
            <ta e="T80" id="Seg_1704" s="T79">0.3.h:A</ta>
            <ta e="T81" id="Seg_1705" s="T80">np:Th</ta>
            <ta e="T82" id="Seg_1706" s="T81">0.2.h:A</ta>
            <ta e="T83" id="Seg_1707" s="T82">pro.h:B</ta>
            <ta e="T84" id="Seg_1708" s="T83">adv:Time</ta>
            <ta e="T87" id="Seg_1709" s="T86">v:Th</ta>
            <ta e="T89" id="Seg_1710" s="T88">pro.h:Poss</ta>
            <ta e="T91" id="Seg_1711" s="T90">np.h:Th</ta>
            <ta e="T93" id="Seg_1712" s="T92">np.h:Th</ta>
            <ta e="T94" id="Seg_1713" s="T93">pro.h:Th</ta>
            <ta e="T96" id="Seg_1714" s="T95">np.h:L 0.3.h:Poss</ta>
            <ta e="T98" id="Seg_1715" s="T97">0.3.h:A</ta>
            <ta e="T99" id="Seg_1716" s="T98">pro.h:A</ta>
            <ta e="T100" id="Seg_1717" s="T99">pro.h:G</ta>
            <ta e="T103" id="Seg_1718" s="T102">0.1.h:А</ta>
            <ta e="T104" id="Seg_1719" s="T103">0.2.h:А</ta>
            <ta e="T105" id="Seg_1720" s="T104">pro.h:Poss</ta>
            <ta e="T106" id="Seg_1721" s="T105">np.h:P</ta>
            <ta e="T108" id="Seg_1722" s="T107">adv:Time</ta>
            <ta e="T111" id="Seg_1723" s="T110">np:Time</ta>
            <ta e="T112" id="Seg_1724" s="T111">0.3.h:Th</ta>
            <ta e="T115" id="Seg_1725" s="T114">np:Time</ta>
            <ta e="T119" id="Seg_1726" s="T118">0.3.h:P</ta>
            <ta e="T125" id="Seg_1727" s="T124">np:Time</ta>
            <ta e="T126" id="Seg_1728" s="T125">pro.h:Th</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T61" id="Seg_1729" s="T60">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_1730" s="T0">У меня есть ребенок, один ребенок, мальчик.</ta>
            <ta e="T10" id="Seg_1731" s="T6">Со мной он живет, без матери.</ta>
            <ta e="T14" id="Seg_1732" s="T10">Мы разошлись пять лет назад.</ta>
            <ta e="T18" id="Seg_1733" s="T14">Дочь у матери живет, с матерью.</ta>
            <ta e="T24" id="Seg_1734" s="T18">Я не знаю, как она живет.</ta>
            <ta e="T30" id="Seg_1735" s="T24">Она не знает, как мы живем.</ta>
            <ta e="T38" id="Seg_1736" s="T30">Мы не пишем друг другу, как мы друг друга забыли.</ta>
            <ta e="T43" id="Seg_1737" s="T38">Наврное, когда-то мы друг с другом встретимся.</ta>
            <ta e="T50" id="Seg_1738" s="T43">Сестра, брат, мальчик, они двое как чужие люди.</ta>
            <ta e="T52" id="Seg_1739" s="T50">Они друг другу не пишут.</ta>
            <ta e="T55" id="Seg_1740" s="T52">Мать моей дочери русская.</ta>
            <ta e="T62" id="Seg_1741" s="T55">Она ругается: не пиши отцу и брату.</ta>
            <ta e="T67" id="Seg_1742" s="T62">Не много я вам скажу про свою мать.</ta>
            <ta e="T69" id="Seg_1743" s="T67">Она давно умерла.</ta>
            <ta e="T72" id="Seg_1744" s="T69">Наверно, сейчас она сгнила.</ta>
            <ta e="T78" id="Seg_1745" s="T72">Моя мать теперь постарела, ей шестьдесят лет.</ta>
            <ta e="T82" id="Seg_1746" s="T78">Мне говорит: письмо пиши.</ta>
            <ta e="T87" id="Seg_1747" s="T82">Мне теперь не долго жить.</ta>
            <ta e="T93" id="Seg_1748" s="T87">Говорит: у меня два сына_ одна дочь.</ta>
            <ta e="T96" id="Seg_1749" s="T93">Она живет у дочери.</ta>
            <ta e="T103" id="Seg_1750" s="T96">Она говорила: Я к вам приеду вас посмотреть.</ta>
            <ta e="T108" id="Seg_1751" s="T103">У меня мой отец умер давно.</ta>
            <ta e="T115" id="Seg_1752" s="T108">Пятьдесят лет жил, семьдесят(?) лет.</ta>
            <ta e="T125" id="Seg_1753" s="T115">Недолго сильно он болел, пятнадцать дней.</ta>
            <ta e="T129" id="Seg_1754" s="T125">Я скоро тоже постарею.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_1755" s="T0">I have got a child, one child, a boy.</ta>
            <ta e="T10" id="Seg_1756" s="T6">He lives with me without mother.</ta>
            <ta e="T14" id="Seg_1757" s="T10">We separated five years ago.</ta>
            <ta e="T18" id="Seg_1758" s="T14">My daughter lives by her mother, with her mother.</ta>
            <ta e="T24" id="Seg_1759" s="T18">I don't know how she lives.</ta>
            <ta e="T30" id="Seg_1760" s="T24">She doesn't know how we live.</ta>
            <ta e="T38" id="Seg_1761" s="T30">We don't write each other, how we came to forget each other.</ta>
            <ta e="T43" id="Seg_1762" s="T38">Maybe we'll meet each other.</ta>
            <ta e="T50" id="Seg_1763" s="T43">The sister, the brother, the boy, they both are like strangers.</ta>
            <ta e="T52" id="Seg_1764" s="T50">They don't write.</ta>
            <ta e="T55" id="Seg_1765" s="T52">My daughter's mother is Russian.</ta>
            <ta e="T62" id="Seg_1766" s="T55">She scolds: don't write to your father or to your brother.</ta>
            <ta e="T67" id="Seg_1767" s="T62">I can't tell you much about my mother.</ta>
            <ta e="T69" id="Seg_1768" s="T67">She died long ago.</ta>
            <ta e="T72" id="Seg_1769" s="T69">Probobly she had rotted by now.</ta>
            <ta e="T78" id="Seg_1770" s="T72">Now my mother got old, she is sixty.</ta>
            <ta e="T82" id="Seg_1771" s="T78">She says to me: Write a letter.</ta>
            <ta e="T87" id="Seg_1772" s="T82">I won't stay alive for a long time.</ta>
            <ta e="T93" id="Seg_1773" s="T87">She says: I have two sons and one daughter.</ta>
            <ta e="T96" id="Seg_1774" s="T93">She lives by her daughter.</ta>
            <ta e="T103" id="Seg_1775" s="T96">She used to say: I'll come to you to look at you.</ta>
            <ta e="T108" id="Seg_1776" s="T103">My father died long ago.</ta>
            <ta e="T115" id="Seg_1777" s="T108">He lived for fifty years, seventy(?) years.</ta>
            <ta e="T125" id="Seg_1778" s="T115">He was very ill for a short time, for fifteen days.</ta>
            <ta e="T129" id="Seg_1779" s="T125">I'll get old soon too.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_1780" s="T0">Ich habe ein Kind, ein Kind, einen Jungen.</ta>
            <ta e="T10" id="Seg_1781" s="T6">Er lebt mit mir ohne Mutter.</ta>
            <ta e="T14" id="Seg_1782" s="T10">Wir trennten uns vor fünf Jahren.</ta>
            <ta e="T18" id="Seg_1783" s="T14">Meine Tochter lebt bei ihrer Mutter, mit ihrer Mutter.</ta>
            <ta e="T24" id="Seg_1784" s="T18">Ich weiß nicht, wie sie lebt.</ta>
            <ta e="T30" id="Seg_1785" s="T24">Sie weiß nicht, wie wir leben.</ta>
            <ta e="T38" id="Seg_1786" s="T30">Wir schreiben einander nicht, wie wir einander vergessen haben.</ta>
            <ta e="T43" id="Seg_1787" s="T38">Vielleicht treffen wir einander.</ta>
            <ta e="T50" id="Seg_1788" s="T43">Die Schwester, der Bruder, der Junge, sie sind beide wie Fremde.</ta>
            <ta e="T52" id="Seg_1789" s="T50">Sie schreiben nicht.</ta>
            <ta e="T55" id="Seg_1790" s="T52">Die Mutter meiner Tochter ist russisch.</ta>
            <ta e="T62" id="Seg_1791" s="T55">Sie tadelt: Schreib nicht deinem Vater und deinem Bruder.</ta>
            <ta e="T67" id="Seg_1792" s="T62">Ich kann dir nicht viel über meine Mutter sagen.</ta>
            <ta e="T69" id="Seg_1793" s="T67">Sie ist vor langer Zeit gestorben.</ta>
            <ta e="T72" id="Seg_1794" s="T69">Vermutlich verrottet sie jetzt.</ta>
            <ta e="T78" id="Seg_1795" s="T72">Jetzt ist meine Mutter alt geworden, sie ist sechzig.</ta>
            <ta e="T82" id="Seg_1796" s="T78">Sie sagt mir: Schreib einen Brief.</ta>
            <ta e="T87" id="Seg_1797" s="T82">Mir bleibt jetzt nicht lange zu leben.</ta>
            <ta e="T93" id="Seg_1798" s="T87">Sie sagt: Ich habe zwei Söhne und eine Tochter.</ta>
            <ta e="T96" id="Seg_1799" s="T93">Sie lebt bei ihrer Tochter.</ta>
            <ta e="T103" id="Seg_1800" s="T96">Sie pflegte zu sagen: Ich werde zu dir kommen um dich anzusehen.</ta>
            <ta e="T108" id="Seg_1801" s="T103">Schau, mein Vater starb vor langer Zeit.</ta>
            <ta e="T115" id="Seg_1802" s="T108">Er lebte fünfzig, siebzig (?) Jahre.</ta>
            <ta e="T125" id="Seg_1803" s="T115">Er war sehr krank für kurze Zeit, fünfzehn Tage lang.</ta>
            <ta e="T129" id="Seg_1804" s="T125">Ich werde auch bald alt werden.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_1805" s="T0">у меня есть ребенок один ребенок мальчик</ta>
            <ta e="T10" id="Seg_1806" s="T6">со мной он живет без матери</ta>
            <ta e="T14" id="Seg_1807" s="T10">мы разделились уже 5 лет</ta>
            <ta e="T18" id="Seg_1808" s="T14">дочь у матери живет с матерью</ta>
            <ta e="T24" id="Seg_1809" s="T18">я не знаю как она живет</ta>
            <ta e="T30" id="Seg_1810" s="T24">она не знает как мы живем</ta>
            <ta e="T38" id="Seg_1811" s="T30">мы не пишем друг другу как мы друг друга забыли</ta>
            <ta e="T43" id="Seg_1812" s="T38">когда мы друг с другом встретимся</ta>
            <ta e="T50" id="Seg_1813" s="T43">сестра брат мальчик они как чужие люди</ta>
            <ta e="T52" id="Seg_1814" s="T50">они друг другу не пишут</ta>
            <ta e="T55" id="Seg_1815" s="T52">дочери мать русская</ta>
            <ta e="T62" id="Seg_1816" s="T55">она ругается что не пиши отцу брату</ta>
            <ta e="T67" id="Seg_1817" s="T62">не много я вам скажу про мать</ta>
            <ta e="T69" id="Seg_1818" s="T67">давно умер .</ta>
            <ta e="T72" id="Seg_1819" s="T69">наверно сейчас сгнил</ta>
            <ta e="T78" id="Seg_1820" s="T72">мать теперь пристарела шестьдесят лет</ta>
            <ta e="T82" id="Seg_1821" s="T78">мне говорит письмо пиши</ta>
            <ta e="T87" id="Seg_1822" s="T82">мне теперь не долго жить</ta>
            <ta e="T93" id="Seg_1823" s="T87">говорит у меня два сына одна дочь</ta>
            <ta e="T96" id="Seg_1824" s="T93">она живет у дочери</ta>
            <ta e="T103" id="Seg_1825" s="T96">она говорила что я к вам приеду вас посмотреть</ta>
            <ta e="T108" id="Seg_1826" s="T103">у меня мой отец умер давно</ta>
            <ta e="T115" id="Seg_1827" s="T108">пятьдесят лет жил семьдесят лет</ta>
            <ta e="T125" id="Seg_1828" s="T115">не долго (много) он болел пятнадцать дней</ta>
            <ta e="T129" id="Seg_1829" s="T125">я скоро тоже пристарею</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T38" id="Seg_1830" s="T30">[WNB:] -qnɨt is probably 3SG.ILL (-qəntɨ)</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T69" id="Seg_1831" s="T67">May be this and the next sentence are not about the speaker’s mother, but about some other person (see further: the speaker tells about his mother as if she still lived at that time)</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T130" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
