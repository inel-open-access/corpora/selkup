<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>MPF_1964_MyRelationsWithWomen_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">MPF_1964_MyRelationsWithWomen_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">61</ud-information>
            <ud-information attribute-name="# HIAT:w">47</ud-information>
            <ud-information attribute-name="# e">47</ud-information>
            <ud-information attribute-name="# HIAT:u">11</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="MPF">
            <abbreviation>MPF</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="MPF"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T48" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">nätambisaŋ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">sälʼdʼüŋ</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">šɨtʼtʼat</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">kʼötan</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">man</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">nätenǯaːŋ</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">nildʼiŋ</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">man</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">täːrpaŋ</ts>
                  <nts id="Seg_38" n="HIAT:ip">:</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">pajala</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">maːnaŋ</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">eːgat</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">qoːtat</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">qəːčat</ts>
                  <nts id="Seg_54" n="HIAT:ip">.</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_57" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">araŋmɨ</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">nändɨ</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">eːgan</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_69" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">qallo</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">qwanni</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_77" n="HIAT:w" s="T21">toj</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_81" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">täp</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">man</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">täpani</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">qwanǯaŋ</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_96" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">tipindenǯaŋ</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">täpanni</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_105" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_107" n="HIAT:w" s="T28">man</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">täpam</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">qaqi</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_116" n="HIAT:w" s="T31">moːtkɨwam</ts>
                  <nts id="Seg_117" n="HIAT:ip">,</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_120" n="HIAT:w" s="T32">čʼarakuwaŋ</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_123" n="HIAT:w" s="T33">uːjak</ts>
                  <nts id="Seg_124" n="HIAT:ip">,</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_127" n="HIAT:w" s="T34">a</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_130" n="HIAT:w" s="T35">täp</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_133" n="HIAT:w" s="T36">qwatumba</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_137" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_139" n="HIAT:w" s="T37">man</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_142" n="HIAT:w" s="T38">nän</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_145" n="HIAT:w" s="T39">qəl</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_148" n="HIAT:w" s="T40">qätaquwam</ts>
                  <nts id="Seg_149" n="HIAT:ip">.</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_152" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_154" n="HIAT:w" s="T41">qoːnaŋ</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_157" n="HIAT:w" s="T42">moːtku</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_160" n="HIAT:w" s="T43">qaim</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_163" n="HIAT:w" s="T44">naːkuwan</ts>
                  <nts id="Seg_164" n="HIAT:ip">.</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_167" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_169" n="HIAT:w" s="T45">qəːn</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_172" n="HIAT:w" s="T46">seːpeŋ</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_175" n="HIAT:w" s="T47">täpani</ts>
                  <nts id="Seg_176" n="HIAT:ip">.</nts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T48" id="Seg_178" n="sc" s="T1">
               <ts e="T2" id="Seg_180" n="e" s="T1">man </ts>
               <ts e="T3" id="Seg_182" n="e" s="T2">nätambisaŋ </ts>
               <ts e="T4" id="Seg_184" n="e" s="T3">sälʼdʼüŋ. </ts>
               <ts e="T5" id="Seg_186" n="e" s="T4">šɨtʼtʼat </ts>
               <ts e="T6" id="Seg_188" n="e" s="T5">kʼötan </ts>
               <ts e="T7" id="Seg_190" n="e" s="T6">man </ts>
               <ts e="T8" id="Seg_192" n="e" s="T7">nätenǯaːŋ. </ts>
               <ts e="T9" id="Seg_194" n="e" s="T8">nildʼiŋ </ts>
               <ts e="T10" id="Seg_196" n="e" s="T9">man </ts>
               <ts e="T11" id="Seg_198" n="e" s="T10">täːrpaŋ: </ts>
               <ts e="T12" id="Seg_200" n="e" s="T11">pajala </ts>
               <ts e="T13" id="Seg_202" n="e" s="T12">maːnaŋ </ts>
               <ts e="T14" id="Seg_204" n="e" s="T13">eːgat </ts>
               <ts e="T15" id="Seg_206" n="e" s="T14">qoːtat </ts>
               <ts e="T16" id="Seg_208" n="e" s="T15">qəːčat. </ts>
               <ts e="T17" id="Seg_210" n="e" s="T16">araŋmɨ </ts>
               <ts e="T18" id="Seg_212" n="e" s="T17">nändɨ </ts>
               <ts e="T19" id="Seg_214" n="e" s="T18">eːgan. </ts>
               <ts e="T20" id="Seg_216" n="e" s="T19">qallo </ts>
               <ts e="T21" id="Seg_218" n="e" s="T20">qwanni </ts>
               <ts e="T22" id="Seg_220" n="e" s="T21">toj. </ts>
               <ts e="T23" id="Seg_222" n="e" s="T22">täp </ts>
               <ts e="T24" id="Seg_224" n="e" s="T23">man </ts>
               <ts e="T25" id="Seg_226" n="e" s="T24">täpani </ts>
               <ts e="T26" id="Seg_228" n="e" s="T25">qwanǯaŋ. </ts>
               <ts e="T27" id="Seg_230" n="e" s="T26">tipindenǯaŋ </ts>
               <ts e="T28" id="Seg_232" n="e" s="T27">täpanni. </ts>
               <ts e="T29" id="Seg_234" n="e" s="T28">man </ts>
               <ts e="T30" id="Seg_236" n="e" s="T29">täpam </ts>
               <ts e="T31" id="Seg_238" n="e" s="T30">qaqi </ts>
               <ts e="T32" id="Seg_240" n="e" s="T31">moːtkɨwam, </ts>
               <ts e="T33" id="Seg_242" n="e" s="T32">čʼarakuwaŋ </ts>
               <ts e="T34" id="Seg_244" n="e" s="T33">uːjak, </ts>
               <ts e="T35" id="Seg_246" n="e" s="T34">a </ts>
               <ts e="T36" id="Seg_248" n="e" s="T35">täp </ts>
               <ts e="T37" id="Seg_250" n="e" s="T36">qwatumba. </ts>
               <ts e="T38" id="Seg_252" n="e" s="T37">man </ts>
               <ts e="T39" id="Seg_254" n="e" s="T38">nän </ts>
               <ts e="T40" id="Seg_256" n="e" s="T39">qəl </ts>
               <ts e="T41" id="Seg_258" n="e" s="T40">qätaquwam. </ts>
               <ts e="T42" id="Seg_260" n="e" s="T41">qoːnaŋ </ts>
               <ts e="T43" id="Seg_262" n="e" s="T42">moːtku </ts>
               <ts e="T44" id="Seg_264" n="e" s="T43">qaim </ts>
               <ts e="T45" id="Seg_266" n="e" s="T44">naːkuwan. </ts>
               <ts e="T46" id="Seg_268" n="e" s="T45">qəːn </ts>
               <ts e="T47" id="Seg_270" n="e" s="T46">seːpeŋ </ts>
               <ts e="T48" id="Seg_272" n="e" s="T47">täpani. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_273" s="T1">MPF_1964_MyRelationsWithWomen_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_274" s="T4">MPF_1964_MyRelationsWithWomen_nar.002 (001.002)</ta>
            <ta e="T16" id="Seg_275" s="T8">MPF_1964_MyRelationsWithWomen_nar.003 (001.003)</ta>
            <ta e="T19" id="Seg_276" s="T16">MPF_1964_MyRelationsWithWomen_nar.004 (001.004)</ta>
            <ta e="T22" id="Seg_277" s="T19">MPF_1964_MyRelationsWithWomen_nar.005 (001.005)</ta>
            <ta e="T26" id="Seg_278" s="T22">MPF_1964_MyRelationsWithWomen_nar.006 (001.006)</ta>
            <ta e="T28" id="Seg_279" s="T26">MPF_1964_MyRelationsWithWomen_nar.007 (001.007)</ta>
            <ta e="T37" id="Seg_280" s="T28">MPF_1964_MyRelationsWithWomen_nar.008 (001.008)</ta>
            <ta e="T41" id="Seg_281" s="T37">MPF_1964_MyRelationsWithWomen_nar.009 (001.009)</ta>
            <ta e="T45" id="Seg_282" s="T41">MPF_1964_MyRelationsWithWomen_nar.010 (001.010)</ta>
            <ta e="T48" id="Seg_283" s="T45">MPF_1964_MyRelationsWithWomen_nar.011 (001.011)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_284" s="T1">ман нӓ′тамби‵саң сӓлʼдʼӱң.</ta>
            <ta e="T8" id="Seg_285" s="T4">шы′тʼтʼат кʼӧтан ман нӓ′те(ӓ̄)н′джа̄ң.</ta>
            <ta e="T16" id="Seg_286" s="T8">ниl′дʼиң ман ′тӓ̄рпаң: ′паjаlа ′ма̄наң ′е̨̄гат kо̄′тат ′kъ̊̄тшат.</ta>
            <ta e="T19" id="Seg_287" s="T16">а′раңмы ′нӓнды ′е̄ган.</ta>
            <ta e="T22" id="Seg_288" s="T19">kа′llо kва′нни ′той.</ta>
            <ta e="T26" id="Seg_289" s="T22">тӓп ман тӓ′пан(н)и kwан′джаң.</ta>
            <ta e="T28" id="Seg_290" s="T26">типин′денджаң тӓ′панни.</ta>
            <ta e="T37" id="Seg_291" s="T28">ман тӓ′пам ′kаk(ɣ)и ′мо̄ткывам, тʼша′ракуваң ′ӯjак. а тӓп ′kwатумба.</ta>
            <ta e="T41" id="Seg_292" s="T37">ман нӓн ′kъ̊l kӓ′таkу‵вам.</ta>
            <ta e="T45" id="Seg_293" s="T41">kо̄′наң ′мо̄тку ′kаим ′на̄куван.</ta>
            <ta e="T48" id="Seg_294" s="T45">′kъ̊̄н ′се̄пең тӓ′пани.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_295" s="T1">man nätambisaŋ sälʼdʼüŋ.</ta>
            <ta e="T8" id="Seg_296" s="T4">šɨtʼtʼat kʼötan man näte(äː)nǯaːŋ.</ta>
            <ta e="T16" id="Seg_297" s="T8">nildʼiŋ man täːrpaŋ: pajala maːnaŋ eːgat qoːtat qəːtšat.</ta>
            <ta e="T19" id="Seg_298" s="T16">araŋmɨ nändɨ eːgan.</ta>
            <ta e="T22" id="Seg_299" s="T19">qallo qvanni toj.</ta>
            <ta e="T26" id="Seg_300" s="T22">täp man täpan(n)i qwanǯaŋ.</ta>
            <ta e="T28" id="Seg_301" s="T26">tipindenǯaŋ täpanni.</ta>
            <ta e="T37" id="Seg_302" s="T28">man täpam qaq(ɣ)i moːtkɨvam, tʼšarakuvaŋ uːjak. a täp qwatumba.</ta>
            <ta e="T41" id="Seg_303" s="T37">man nän qəl qätaquvam.</ta>
            <ta e="T45" id="Seg_304" s="T41">qoːnaŋ moːtku qaim naːkuvan.</ta>
            <ta e="T48" id="Seg_305" s="T45">qəːn seːpeŋ täpani.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_306" s="T1">man nätambisaŋ sälʼdʼüŋ. </ta>
            <ta e="T8" id="Seg_307" s="T4">šɨtʼtʼat kʼötan man nätenǯaːŋ. </ta>
            <ta e="T16" id="Seg_308" s="T8">nildʼiŋ man täːrpaŋ: pajala maːnaŋ eːgat qoːtat qəːčat. </ta>
            <ta e="T19" id="Seg_309" s="T16">araŋmɨ nändɨ eːgan. </ta>
            <ta e="T22" id="Seg_310" s="T19">qallo qwanni toj. </ta>
            <ta e="T26" id="Seg_311" s="T22">täp man täpani qwanǯaŋ. </ta>
            <ta e="T28" id="Seg_312" s="T26">tipindenǯaŋ täpanni. </ta>
            <ta e="T37" id="Seg_313" s="T28">man täpam qaqi moːtkɨwam, čʼarakuwaŋ uːjak, a täp qwatumba. </ta>
            <ta e="T41" id="Seg_314" s="T37">man nän qəl qätaquwam. </ta>
            <ta e="T45" id="Seg_315" s="T41">qoːnaŋ moːtku qaim naːkuwan. </ta>
            <ta e="T48" id="Seg_316" s="T45">qəːn seːpeŋ täpani. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_317" s="T1">man</ta>
            <ta e="T3" id="Seg_318" s="T2">näta-mbi-sa-ŋ</ta>
            <ta e="T4" id="Seg_319" s="T3">sälʼdʼü-ŋ</ta>
            <ta e="T5" id="Seg_320" s="T4">šɨtʼtʼa-t</ta>
            <ta e="T6" id="Seg_321" s="T5">kʼöt-a-n</ta>
            <ta e="T7" id="Seg_322" s="T6">man</ta>
            <ta e="T8" id="Seg_323" s="T7">nät-e-nǯaː-ŋ</ta>
            <ta e="T9" id="Seg_324" s="T8">nildʼi-ŋ</ta>
            <ta e="T10" id="Seg_325" s="T9">man</ta>
            <ta e="T11" id="Seg_326" s="T10">täːrpa-ŋ</ta>
            <ta e="T12" id="Seg_327" s="T11">paja-la</ta>
            <ta e="T13" id="Seg_328" s="T12">maːnaŋ</ta>
            <ta e="T14" id="Seg_329" s="T13">eː-ga-t</ta>
            <ta e="T15" id="Seg_330" s="T14">qoːtat</ta>
            <ta e="T16" id="Seg_331" s="T15">qəː-ča-t</ta>
            <ta e="T17" id="Seg_332" s="T16">araŋ-mɨ</ta>
            <ta e="T18" id="Seg_333" s="T17">nändɨ</ta>
            <ta e="T19" id="Seg_334" s="T18">eː-ga-n</ta>
            <ta e="T20" id="Seg_335" s="T19">qallo</ta>
            <ta e="T21" id="Seg_336" s="T20">qwan-ni</ta>
            <ta e="T22" id="Seg_337" s="T21">toj</ta>
            <ta e="T23" id="Seg_338" s="T22">täp</ta>
            <ta e="T24" id="Seg_339" s="T23">man</ta>
            <ta e="T25" id="Seg_340" s="T24">täp-a-ni</ta>
            <ta e="T26" id="Seg_341" s="T25">qwan-ǯa-ŋ</ta>
            <ta e="T27" id="Seg_342" s="T26">tipinde-nǯa-ŋ</ta>
            <ta e="T28" id="Seg_343" s="T27">täp-a-nni</ta>
            <ta e="T29" id="Seg_344" s="T28">man</ta>
            <ta e="T30" id="Seg_345" s="T29">täp-a-m</ta>
            <ta e="T31" id="Seg_346" s="T30">qaqi</ta>
            <ta e="T32" id="Seg_347" s="T31">moːt-kɨ-wa-m</ta>
            <ta e="T33" id="Seg_348" s="T32">čʼara-ku-wa-ŋ</ta>
            <ta e="T34" id="Seg_349" s="T33">uːja-k</ta>
            <ta e="T35" id="Seg_350" s="T34">a</ta>
            <ta e="T36" id="Seg_351" s="T35">täp</ta>
            <ta e="T37" id="Seg_352" s="T36">qwatu-mba</ta>
            <ta e="T38" id="Seg_353" s="T37">man</ta>
            <ta e="T39" id="Seg_354" s="T38">nän</ta>
            <ta e="T40" id="Seg_355" s="T39">qəl</ta>
            <ta e="T41" id="Seg_356" s="T40">qäta-qu-wa-m</ta>
            <ta e="T42" id="Seg_357" s="T41">qoːnɨŋ</ta>
            <ta e="T43" id="Seg_358" s="T42">moːt-ku</ta>
            <ta e="T44" id="Seg_359" s="T43">qai-m</ta>
            <ta e="T45" id="Seg_360" s="T44">naː-ku-wan</ta>
            <ta e="T46" id="Seg_361" s="T45">qəːn</ta>
            <ta e="T47" id="Seg_362" s="T46">seːp-e-ŋ</ta>
            <ta e="T48" id="Seg_363" s="T47">täp-a-ni</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_364" s="T1">man</ta>
            <ta e="T3" id="Seg_365" s="T2">naːtɨ-mbɨ-sɨ-ŋ</ta>
            <ta e="T4" id="Seg_366" s="T3">seːlʼdu-k</ta>
            <ta e="T5" id="Seg_367" s="T4">šittə-n</ta>
            <ta e="T6" id="Seg_368" s="T5">köːt-ɨ-k</ta>
            <ta e="T7" id="Seg_369" s="T6">man</ta>
            <ta e="T8" id="Seg_370" s="T7">nät-ɨ-nǯɨ-n</ta>
            <ta e="T9" id="Seg_371" s="T8">nɨlʼǯi-n</ta>
            <ta e="T10" id="Seg_372" s="T9">man</ta>
            <ta e="T11" id="Seg_373" s="T10">tärba-ŋ</ta>
            <ta e="T12" id="Seg_374" s="T11">paja-la</ta>
            <ta e="T13" id="Seg_375" s="T12">maːnaŋ</ta>
            <ta e="T14" id="Seg_376" s="T13">eː-ku-tɨt</ta>
            <ta e="T15" id="Seg_377" s="T14">%%</ta>
            <ta e="T16" id="Seg_378" s="T15">qwən-nče-tɨt</ta>
            <ta e="T17" id="Seg_379" s="T16">aːrɨŋ-mɨ</ta>
            <ta e="T18" id="Seg_380" s="T17">nɨmtɨ</ta>
            <ta e="T19" id="Seg_381" s="T18">eː-ku-n</ta>
            <ta e="T20" id="Seg_382" s="T19">qal</ta>
            <ta e="T21" id="Seg_383" s="T20">qwən-ŋɨjä</ta>
            <ta e="T22" id="Seg_384" s="T21">teː</ta>
            <ta e="T23" id="Seg_385" s="T22">tap</ta>
            <ta e="T24" id="Seg_386" s="T23">man</ta>
            <ta e="T25" id="Seg_387" s="T24">tap-ɨ-nɨ</ta>
            <ta e="T26" id="Seg_388" s="T25">qwən-nǯɨ-ŋ</ta>
            <ta e="T27" id="Seg_389" s="T26">tibindɨ-nǯɨ-ŋ</ta>
            <ta e="T28" id="Seg_390" s="T27">tap-ɨ-nɨ</ta>
            <ta e="T29" id="Seg_391" s="T28">man</ta>
            <ta e="T30" id="Seg_392" s="T29">tap-ɨ-m</ta>
            <ta e="T31" id="Seg_393" s="T30">qaqä</ta>
            <ta e="T32" id="Seg_394" s="T31">moːt-ku-ŋɨ-m</ta>
            <ta e="T33" id="Seg_395" s="T32">tʼarɨ-ku-ŋɨ-ŋ</ta>
            <ta e="T34" id="Seg_396" s="T33">uːja-äšɨk</ta>
            <ta e="T35" id="Seg_397" s="T34">a</ta>
            <ta e="T36" id="Seg_398" s="T35">tap</ta>
            <ta e="T37" id="Seg_399" s="T36">qwodɨ-mbɨ</ta>
            <ta e="T38" id="Seg_400" s="T37">man</ta>
            <ta e="T39" id="Seg_401" s="T38">nɨːnɨ</ta>
            <ta e="T40" id="Seg_402" s="T39">qal</ta>
            <ta e="T41" id="Seg_403" s="T40">qätə-ku-ŋɨ-m</ta>
            <ta e="T42" id="Seg_404" s="T41">qoːnɨŋ</ta>
            <ta e="T43" id="Seg_405" s="T42">moːt-ku</ta>
            <ta e="T44" id="Seg_406" s="T43">qaj-m</ta>
            <ta e="T45" id="Seg_407" s="T44">neː-qum-mɨn</ta>
            <ta e="T47" id="Seg_408" s="T46">säːp-ɨ-k</ta>
            <ta e="T48" id="Seg_409" s="T47">tap-ɨ-nɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_410" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_411" s="T2">get.married-DUR-PST-1SG.S</ta>
            <ta e="T4" id="Seg_412" s="T3">seven-ADVZ</ta>
            <ta e="T5" id="Seg_413" s="T4">two-GEN</ta>
            <ta e="T6" id="Seg_414" s="T5">ten-EP-ADVZ</ta>
            <ta e="T7" id="Seg_415" s="T6">I.NOM</ta>
            <ta e="T8" id="Seg_416" s="T7">marry-EP-FUT-3SG.S</ta>
            <ta e="T9" id="Seg_417" s="T8">such-ADV.LOC</ta>
            <ta e="T10" id="Seg_418" s="T9">I.NOM</ta>
            <ta e="T11" id="Seg_419" s="T10">think-1SG.S</ta>
            <ta e="T12" id="Seg_420" s="T11">old.woman-PL.[NOM]</ta>
            <ta e="T13" id="Seg_421" s="T12">stupid</ta>
            <ta e="T14" id="Seg_422" s="T13">be-HAB-3PL</ta>
            <ta e="T15" id="Seg_423" s="T14">%%</ta>
            <ta e="T16" id="Seg_424" s="T15">go.away-IPFV3-3PL</ta>
            <ta e="T17" id="Seg_425" s="T16">other-1SG</ta>
            <ta e="T18" id="Seg_426" s="T17">here</ta>
            <ta e="T19" id="Seg_427" s="T18">be-HAB-3SG.S</ta>
            <ta e="T20" id="Seg_428" s="T19">it.is.said</ta>
            <ta e="T21" id="Seg_429" s="T20">go.away-IMP.3SG.S</ta>
            <ta e="T22" id="Seg_430" s="T21">away</ta>
            <ta e="T23" id="Seg_431" s="T22">(s)he.[NOM]</ta>
            <ta e="T24" id="Seg_432" s="T23">I.NOM</ta>
            <ta e="T25" id="Seg_433" s="T24">(s)he-EP-ALL</ta>
            <ta e="T26" id="Seg_434" s="T25">go.away-FUT-1SG.S</ta>
            <ta e="T27" id="Seg_435" s="T26">marry-FUT-1SG.S</ta>
            <ta e="T28" id="Seg_436" s="T27">(s)he-EP-ALL</ta>
            <ta e="T29" id="Seg_437" s="T28">I.NOM</ta>
            <ta e="T30" id="Seg_438" s="T29">(s)he-EP-ACC</ta>
            <ta e="T31" id="Seg_439" s="T30">when</ta>
            <ta e="T32" id="Seg_440" s="T31">whip-HAB-CO-1SG.O</ta>
            <ta e="T33" id="Seg_441" s="T32">say-HAB-CO-1SG.S</ta>
            <ta e="T34" id="Seg_442" s="T33">keep.silent-IMP.2SG.S</ta>
            <ta e="T35" id="Seg_443" s="T34">but</ta>
            <ta e="T36" id="Seg_444" s="T35">(s)he.[NOM]</ta>
            <ta e="T37" id="Seg_445" s="T36">scold-PST.NAR.[3SG.S]</ta>
            <ta e="T38" id="Seg_446" s="T37">I.NOM</ta>
            <ta e="T39" id="Seg_447" s="T38">then</ta>
            <ta e="T40" id="Seg_448" s="T39">it.is.said</ta>
            <ta e="T41" id="Seg_449" s="T40">beat-HAB-CO-1SG.O</ta>
            <ta e="T42" id="Seg_450" s="T41">many</ta>
            <ta e="T43" id="Seg_451" s="T42">whip-HAB</ta>
            <ta e="T44" id="Seg_452" s="T43">what-ACC</ta>
            <ta e="T45" id="Seg_453" s="T44">woman-human.being-PROL</ta>
            <ta e="T47" id="Seg_454" s="T46">enough-EP-ADVZ</ta>
            <ta e="T48" id="Seg_455" s="T47">(s)he-EP-ALL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_456" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_457" s="T2">жениться-DUR-PST-1SG.S</ta>
            <ta e="T4" id="Seg_458" s="T3">семь-ADVZ</ta>
            <ta e="T5" id="Seg_459" s="T4">два-GEN</ta>
            <ta e="T6" id="Seg_460" s="T5">десять-EP-ADVZ</ta>
            <ta e="T7" id="Seg_461" s="T6">я.NOM</ta>
            <ta e="T8" id="Seg_462" s="T7">женить-EP-FUT-3SG.S</ta>
            <ta e="T9" id="Seg_463" s="T8">такой-ADV.LOC</ta>
            <ta e="T10" id="Seg_464" s="T9">я.NOM</ta>
            <ta e="T11" id="Seg_465" s="T10">думать-1SG.S</ta>
            <ta e="T12" id="Seg_466" s="T11">старуха-PL.[NOM]</ta>
            <ta e="T13" id="Seg_467" s="T12">дурной</ta>
            <ta e="T14" id="Seg_468" s="T13">быть-HAB-3PL</ta>
            <ta e="T15" id="Seg_469" s="T14">%%</ta>
            <ta e="T16" id="Seg_470" s="T15">уйти-IPFV3-3PL</ta>
            <ta e="T17" id="Seg_471" s="T16">другой-1SG</ta>
            <ta e="T18" id="Seg_472" s="T17">здесь</ta>
            <ta e="T19" id="Seg_473" s="T18">быть-HAB-3SG.S</ta>
            <ta e="T20" id="Seg_474" s="T19">мол</ta>
            <ta e="T21" id="Seg_475" s="T20">уйти-IMP.3SG.S</ta>
            <ta e="T22" id="Seg_476" s="T21">прочь</ta>
            <ta e="T23" id="Seg_477" s="T22">он(а).[NOM]</ta>
            <ta e="T24" id="Seg_478" s="T23">я.NOM</ta>
            <ta e="T25" id="Seg_479" s="T24">он(а)-EP-ALL</ta>
            <ta e="T26" id="Seg_480" s="T25">уйти-FUT-1SG.S</ta>
            <ta e="T27" id="Seg_481" s="T26">выйти.замуж-FUT-1SG.S</ta>
            <ta e="T28" id="Seg_482" s="T27">он(а)-EP-ALL</ta>
            <ta e="T29" id="Seg_483" s="T28">я.NOM</ta>
            <ta e="T30" id="Seg_484" s="T29">он(а)-EP-ACC</ta>
            <ta e="T31" id="Seg_485" s="T30">когда</ta>
            <ta e="T32" id="Seg_486" s="T31">стегать-HAB-CO-1SG.O</ta>
            <ta e="T33" id="Seg_487" s="T32">сказать-HAB-CO-1SG.S</ta>
            <ta e="T34" id="Seg_488" s="T33">молчать-IMP.2SG.S</ta>
            <ta e="T35" id="Seg_489" s="T34">а</ta>
            <ta e="T36" id="Seg_490" s="T35">он(а).[NOM]</ta>
            <ta e="T37" id="Seg_491" s="T36">выругать-PST.NAR.[3SG.S]</ta>
            <ta e="T38" id="Seg_492" s="T37">я.NOM</ta>
            <ta e="T39" id="Seg_493" s="T38">потом</ta>
            <ta e="T40" id="Seg_494" s="T39">мол</ta>
            <ta e="T41" id="Seg_495" s="T40">бить-HAB-CO-1SG.O</ta>
            <ta e="T42" id="Seg_496" s="T41">много</ta>
            <ta e="T43" id="Seg_497" s="T42">стегать-HAB</ta>
            <ta e="T44" id="Seg_498" s="T43">что-ACC</ta>
            <ta e="T45" id="Seg_499" s="T44">женщина-человек-PROL</ta>
            <ta e="T47" id="Seg_500" s="T46">достаточно-EP-ADVZ</ta>
            <ta e="T48" id="Seg_501" s="T47">он(а)-EP-ALL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_502" s="T1">pers</ta>
            <ta e="T3" id="Seg_503" s="T2">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_504" s="T3">num-adj&gt;adv</ta>
            <ta e="T5" id="Seg_505" s="T4">num-n:case1</ta>
            <ta e="T6" id="Seg_506" s="T5">num-n:ins-num&gt;adv</ta>
            <ta e="T7" id="Seg_507" s="T6">pers</ta>
            <ta e="T8" id="Seg_508" s="T7">v-n:ins-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_509" s="T8">dem-adv:case</ta>
            <ta e="T10" id="Seg_510" s="T9">pers</ta>
            <ta e="T11" id="Seg_511" s="T10">v-v:pn</ta>
            <ta e="T12" id="Seg_512" s="T11">n-n:num.[n:case1]</ta>
            <ta e="T13" id="Seg_513" s="T12">adj</ta>
            <ta e="T14" id="Seg_514" s="T13">v-v&gt;v-v:pn</ta>
            <ta e="T16" id="Seg_515" s="T15">v-v&gt;v-v:pn</ta>
            <ta e="T17" id="Seg_516" s="T16">adj-n:poss</ta>
            <ta e="T18" id="Seg_517" s="T17">adv</ta>
            <ta e="T19" id="Seg_518" s="T18">v-v&gt;v-v:pn</ta>
            <ta e="T20" id="Seg_519" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_520" s="T20">v-v:mood.pn</ta>
            <ta e="T22" id="Seg_521" s="T21">adv</ta>
            <ta e="T23" id="Seg_522" s="T22">pers.[n:case1]</ta>
            <ta e="T24" id="Seg_523" s="T23">pers</ta>
            <ta e="T25" id="Seg_524" s="T24">pers-n:ins-n:case2</ta>
            <ta e="T26" id="Seg_525" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_526" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_527" s="T27">pers-n:ins-n:case2</ta>
            <ta e="T29" id="Seg_528" s="T28">pers</ta>
            <ta e="T30" id="Seg_529" s="T29">pers-n:ins-n:case1</ta>
            <ta e="T31" id="Seg_530" s="T30">conj</ta>
            <ta e="T32" id="Seg_531" s="T31">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T33" id="Seg_532" s="T32">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T34" id="Seg_533" s="T33">v-v:mood.pn</ta>
            <ta e="T35" id="Seg_534" s="T34">conj</ta>
            <ta e="T36" id="Seg_535" s="T35">pers.[n:case1]</ta>
            <ta e="T37" id="Seg_536" s="T36">v-v:tense.[v:pn]</ta>
            <ta e="T38" id="Seg_537" s="T37">pers</ta>
            <ta e="T39" id="Seg_538" s="T38">adv</ta>
            <ta e="T40" id="Seg_539" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_540" s="T40">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T42" id="Seg_541" s="T41">quant</ta>
            <ta e="T43" id="Seg_542" s="T42">v-v&gt;v</ta>
            <ta e="T44" id="Seg_543" s="T43">interrog-n:case1</ta>
            <ta e="T45" id="Seg_544" s="T44">n-n-n:case2</ta>
            <ta e="T47" id="Seg_545" s="T46">adv-n:ins-adj&gt;adv</ta>
            <ta e="T48" id="Seg_546" s="T47">pers-n:ins-n:case2</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_547" s="T1">pers</ta>
            <ta e="T3" id="Seg_548" s="T2">v</ta>
            <ta e="T4" id="Seg_549" s="T3">adv</ta>
            <ta e="T5" id="Seg_550" s="T4">num</ta>
            <ta e="T6" id="Seg_551" s="T5">adv</ta>
            <ta e="T7" id="Seg_552" s="T6">pers</ta>
            <ta e="T8" id="Seg_553" s="T7">v</ta>
            <ta e="T9" id="Seg_554" s="T8">dem</ta>
            <ta e="T10" id="Seg_555" s="T9">pers</ta>
            <ta e="T11" id="Seg_556" s="T10">v</ta>
            <ta e="T12" id="Seg_557" s="T11">n</ta>
            <ta e="T13" id="Seg_558" s="T12">n</ta>
            <ta e="T14" id="Seg_559" s="T13">v</ta>
            <ta e="T15" id="Seg_560" s="T14">interrog</ta>
            <ta e="T16" id="Seg_561" s="T15">v</ta>
            <ta e="T17" id="Seg_562" s="T16">n</ta>
            <ta e="T18" id="Seg_563" s="T17">adv</ta>
            <ta e="T19" id="Seg_564" s="T18">v</ta>
            <ta e="T20" id="Seg_565" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_566" s="T20">v</ta>
            <ta e="T22" id="Seg_567" s="T21">adv</ta>
            <ta e="T23" id="Seg_568" s="T22">pers</ta>
            <ta e="T24" id="Seg_569" s="T23">pers</ta>
            <ta e="T25" id="Seg_570" s="T24">pers</ta>
            <ta e="T26" id="Seg_571" s="T25">v</ta>
            <ta e="T27" id="Seg_572" s="T26">v</ta>
            <ta e="T28" id="Seg_573" s="T27">pers</ta>
            <ta e="T29" id="Seg_574" s="T28">pers</ta>
            <ta e="T30" id="Seg_575" s="T29">pers</ta>
            <ta e="T31" id="Seg_576" s="T30">conj</ta>
            <ta e="T32" id="Seg_577" s="T31">v</ta>
            <ta e="T33" id="Seg_578" s="T32">v</ta>
            <ta e="T34" id="Seg_579" s="T33">v</ta>
            <ta e="T35" id="Seg_580" s="T34">conj</ta>
            <ta e="T36" id="Seg_581" s="T35">pers</ta>
            <ta e="T37" id="Seg_582" s="T36">v</ta>
            <ta e="T38" id="Seg_583" s="T37">pers</ta>
            <ta e="T39" id="Seg_584" s="T38">adv</ta>
            <ta e="T40" id="Seg_585" s="T39">v</ta>
            <ta e="T41" id="Seg_586" s="T40">v</ta>
            <ta e="T42" id="Seg_587" s="T41">quant</ta>
            <ta e="T43" id="Seg_588" s="T42">v</ta>
            <ta e="T45" id="Seg_589" s="T44">n</ta>
            <ta e="T46" id="Seg_590" s="T45">n</ta>
            <ta e="T47" id="Seg_591" s="T46">adv</ta>
            <ta e="T48" id="Seg_592" s="T47">pers</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_593" s="T1">pro.h:A</ta>
            <ta e="T4" id="Seg_594" s="T3">adv:Time</ta>
            <ta e="T6" id="Seg_595" s="T5">adv:Time</ta>
            <ta e="T7" id="Seg_596" s="T6">pro.h:A</ta>
            <ta e="T10" id="Seg_597" s="T9">pro.h:E</ta>
            <ta e="T12" id="Seg_598" s="T11">np.h:Th</ta>
            <ta e="T16" id="Seg_599" s="T15">0.3.h:A</ta>
            <ta e="T17" id="Seg_600" s="T16">np.h:Th </ta>
            <ta e="T18" id="Seg_601" s="T17">adv:L</ta>
            <ta e="T21" id="Seg_602" s="T20">0.3.h:A</ta>
            <ta e="T22" id="Seg_603" s="T21">adv:G</ta>
            <ta e="T24" id="Seg_604" s="T23">pro.h:A</ta>
            <ta e="T25" id="Seg_605" s="T24">pro.h:G</ta>
            <ta e="T27" id="Seg_606" s="T26">0.1.h:A</ta>
            <ta e="T28" id="Seg_607" s="T27">pro.h:Th</ta>
            <ta e="T29" id="Seg_608" s="T28">pro.h:A</ta>
            <ta e="T30" id="Seg_609" s="T29">pro.h:P</ta>
            <ta e="T33" id="Seg_610" s="T32">0.1.h:A</ta>
            <ta e="T34" id="Seg_611" s="T33">0.2.h:A</ta>
            <ta e="T36" id="Seg_612" s="T35">pro.h:A</ta>
            <ta e="T38" id="Seg_613" s="T37">pro.h:A</ta>
            <ta e="T39" id="Seg_614" s="T38">adv:Time</ta>
            <ta e="T41" id="Seg_615" s="T40">0.3.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_616" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_617" s="T2">v:pred</ta>
            <ta e="T7" id="Seg_618" s="T6">pro.h:S</ta>
            <ta e="T8" id="Seg_619" s="T7">v:pred</ta>
            <ta e="T10" id="Seg_620" s="T9">pro.h:S</ta>
            <ta e="T11" id="Seg_621" s="T10">v:pred</ta>
            <ta e="T12" id="Seg_622" s="T11">np.h:S</ta>
            <ta e="T13" id="Seg_623" s="T12">adj:pred</ta>
            <ta e="T14" id="Seg_624" s="T13">cop</ta>
            <ta e="T16" id="Seg_625" s="T14">s:rel</ta>
            <ta e="T17" id="Seg_626" s="T16">np.h:S</ta>
            <ta e="T19" id="Seg_627" s="T18">v:pred</ta>
            <ta e="T21" id="Seg_628" s="T20">0.3.h:S v:pred</ta>
            <ta e="T24" id="Seg_629" s="T23">pro.h:S</ta>
            <ta e="T26" id="Seg_630" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_631" s="T26">0.1.h:S v:pred</ta>
            <ta e="T28" id="Seg_632" s="T27">pro.h:O</ta>
            <ta e="T32" id="Seg_633" s="T28">s:temp</ta>
            <ta e="T33" id="Seg_634" s="T32">0.1.h:S v:pred</ta>
            <ta e="T34" id="Seg_635" s="T33">0.2.h:S v:pred</ta>
            <ta e="T36" id="Seg_636" s="T35">pro.h:S</ta>
            <ta e="T37" id="Seg_637" s="T36">v:pred</ta>
            <ta e="T38" id="Seg_638" s="T37">pro.h:S</ta>
            <ta e="T41" id="Seg_639" s="T40">v:pred 0.3.h:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T35" id="Seg_640" s="T34">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_641" s="T1">I've been married seven times.</ta>
            <ta e="T8" id="Seg_642" s="T4">Eighth time I'm getting married.</ta>
            <ta e="T16" id="Seg_643" s="T8">That's what I think: women are fools who go away.</ta>
            <ta e="T19" id="Seg_644" s="T16">There are others here.</ta>
            <ta e="T22" id="Seg_645" s="T19">(I think) let her go!</ta>
            <ta e="T26" id="Seg_646" s="T22">I'll go to him.</ta>
            <ta e="T28" id="Seg_647" s="T26">I'll marry him.</ta>
            <ta e="T37" id="Seg_648" s="T28">When I hit her, I say you don't say anything, but she's fighting.</ta>
            <ta e="T41" id="Seg_649" s="T37">I'll hit her once.</ta>
            <ta e="T45" id="Seg_650" s="T41">That hitting a woman a lot</ta>
            <ta e="T48" id="Seg_651" s="T45">If it's enough for her.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_652" s="T1">Ich war sieben Mal verheiratet.</ta>
            <ta e="T8" id="Seg_653" s="T4">Ich werde zum achten Mal heiraten.</ta>
            <ta e="T16" id="Seg_654" s="T8">Das denke ich: Frauen sind Idioten, die weggehen.</ta>
            <ta e="T19" id="Seg_655" s="T16">Es gibt andere hier.</ta>
            <ta e="T22" id="Seg_656" s="T19">(Ich denke) lass sie gehen!</ta>
            <ta e="T26" id="Seg_657" s="T22">Ich werde zu ihm (ihr?) gehen.</ta>
            <ta e="T28" id="Seg_658" s="T26">Ich werde ihn (sie?) heiraten.</ta>
            <ta e="T37" id="Seg_659" s="T28">Wenn ich sie schlage, sage ich "sei still", aber sie wehrt sich.</ta>
            <ta e="T41" id="Seg_660" s="T37">Ich schlage sie einmal.</ta>
            <ta e="T45" id="Seg_661" s="T41">Eine Frau oft zu schlagen.</ta>
            <ta e="T48" id="Seg_662" s="T45">Wenn es genug für sie ist.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_663" s="T1">Я был женат семь раз.</ta>
            <ta e="T8" id="Seg_664" s="T4">Восьмой раз я женюсь.</ta>
            <ta e="T16" id="Seg_665" s="T8">Так я думаю: бабы глупые, которые уходят.</ta>
            <ta e="T19" id="Seg_666" s="T16">Другие тут есть.</ta>
            <ta e="T22" id="Seg_667" s="T19">[я думаю] Пусть она уйдет!</ta>
            <ta e="T26" id="Seg_668" s="T22">Она: Я к нему пойду.</ta>
            <ta e="T28" id="Seg_669" s="T26">Я выйду замуж за него.</ta>
            <ta e="T37" id="Seg_670" s="T28">Когда я ее бью, говорю, чтобы молчала, но она ругается.</ta>
            <ta e="T41" id="Seg_671" s="T37">Я ее раз ударю.</ta>
            <ta e="T45" id="Seg_672" s="T41">Что много бить женщину.</ta>
            <ta e="T48" id="Seg_673" s="T45">Раза хватит ей.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr" />
         <annotation name="nt" tierref="nt">
            <ta e="T8" id="Seg_674" s="T4">[WNB:] köt is not clear. An ordinal numeral is normally built by -mtäl.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
