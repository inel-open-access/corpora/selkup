<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>MPT_1964_Lifestory_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">MPT_1964_Lifestory_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">40</ud-information>
            <ud-information attribute-name="# HIAT:w">31</ud-information>
            <ud-information attribute-name="# e">31</ud-information>
            <ud-information attribute-name="# HIAT:u">9</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="MPT">
            <abbreviation>MPT</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="MPT"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T32" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">tʼelamba</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">taw</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">äːtoɣɨn</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">tämdə</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">man</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">orambaŋ</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">meŋa</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">somblʼe</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">qwäj</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">sasarɨj</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">päːwə</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_47" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">man</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">illaŋ</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">opomnaŋ</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_59" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">tepanan</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">tättə</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">üːčet</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_71" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">täːpan</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">tipiqumdə</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">laqaŋ</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">kolhosqɨn</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_86" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">üčelat</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">nʼünʼökʼ</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">ejgat</ts>
                  <nts id="Seg_95" n="HIAT:ip">.</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_98" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_100" n="HIAT:w" s="T26">wärkəjakandit</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">muqtut</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">pät</ts>
                  <nts id="Seg_107" n="HIAT:ip">.</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_110" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_112" n="HIAT:w" s="T29">nʼünʼojakandit</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">šɨt</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_118" n="HIAT:w" s="T31">pät</ts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T32" id="Seg_121" n="sc" s="T1">
               <ts e="T2" id="Seg_123" n="e" s="T1">man </ts>
               <ts e="T3" id="Seg_125" n="e" s="T2">tʼelamba </ts>
               <ts e="T4" id="Seg_127" n="e" s="T3">taw </ts>
               <ts e="T5" id="Seg_129" n="e" s="T4">äːtoɣɨn. </ts>
               <ts e="T6" id="Seg_131" n="e" s="T5">tämdə </ts>
               <ts e="T7" id="Seg_133" n="e" s="T6">man </ts>
               <ts e="T8" id="Seg_135" n="e" s="T7">orambaŋ. </ts>
               <ts e="T9" id="Seg_137" n="e" s="T8">meŋa </ts>
               <ts e="T10" id="Seg_139" n="e" s="T9">somblʼe </ts>
               <ts e="T11" id="Seg_141" n="e" s="T10">qwäj </ts>
               <ts e="T12" id="Seg_143" n="e" s="T11">sasarɨj </ts>
               <ts e="T13" id="Seg_145" n="e" s="T12">päːwə. </ts>
               <ts e="T14" id="Seg_147" n="e" s="T13">man </ts>
               <ts e="T15" id="Seg_149" n="e" s="T14">illaŋ </ts>
               <ts e="T16" id="Seg_151" n="e" s="T15">opomnaŋ. </ts>
               <ts e="T17" id="Seg_153" n="e" s="T16">tepanan </ts>
               <ts e="T18" id="Seg_155" n="e" s="T17">tättə </ts>
               <ts e="T19" id="Seg_157" n="e" s="T18">üːčet. </ts>
               <ts e="T20" id="Seg_159" n="e" s="T19">täːpan </ts>
               <ts e="T21" id="Seg_161" n="e" s="T20">tipiqumdə </ts>
               <ts e="T22" id="Seg_163" n="e" s="T21">laqaŋ </ts>
               <ts e="T23" id="Seg_165" n="e" s="T22">kolhosqɨn. </ts>
               <ts e="T24" id="Seg_167" n="e" s="T23">üčelat </ts>
               <ts e="T25" id="Seg_169" n="e" s="T24">nʼünʼökʼ </ts>
               <ts e="T26" id="Seg_171" n="e" s="T25">ejgat. </ts>
               <ts e="T27" id="Seg_173" n="e" s="T26">wärkəjakandit </ts>
               <ts e="T28" id="Seg_175" n="e" s="T27">muqtut </ts>
               <ts e="T29" id="Seg_177" n="e" s="T28">pät. </ts>
               <ts e="T30" id="Seg_179" n="e" s="T29">nʼünʼojakandit </ts>
               <ts e="T31" id="Seg_181" n="e" s="T30">šɨt </ts>
               <ts e="T32" id="Seg_183" n="e" s="T31">pät. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_184" s="T1">MPT_1964_Lifestory_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_185" s="T5">MPT_1964_Lifestory_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_186" s="T8">MPT_1964_Lifestory_nar.003 (001.003)</ta>
            <ta e="T16" id="Seg_187" s="T13">MPT_1964_Lifestory_nar.004 (001.004)</ta>
            <ta e="T19" id="Seg_188" s="T16">MPT_1964_Lifestory_nar.005 (001.005)</ta>
            <ta e="T23" id="Seg_189" s="T19">MPT_1964_Lifestory_nar.006 (001.006)</ta>
            <ta e="T26" id="Seg_190" s="T23">MPT_1964_Lifestory_nar.007 (001.007)</ta>
            <ta e="T29" id="Seg_191" s="T26">MPT_1964_Lifestory_nar.008 (001.008)</ta>
            <ta e="T32" id="Seg_192" s="T29">MPT_1964_Lifestory_nar.009 (001.009)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_193" s="T1">ман ′тʼеlамба таw ′ӓ̄тоɣын.</ta>
            <ta e="T8" id="Seg_194" s="T5">′тӓмдъ ман о′рамбаң.</ta>
            <ta e="T13" id="Seg_195" s="T8">′меңа сомб′лʼе kwӓй са′сарый ′пӓ̄(ъ)wъ.</ta>
            <ta e="T16" id="Seg_196" s="T13">ман ′иllаң о′помнаң.</ta>
            <ta e="T19" id="Seg_197" s="T16">теп ′ан(н)ан ′тӓттъ ӱ̄′чет.</ta>
            <ta e="T23" id="Seg_198" s="T19">′тӓ̄пан ′типи‵kумдъ lа′kаң кол′хосkын.</ta>
            <ta e="T26" id="Seg_199" s="T23">ӱ′челат ′нʼӱнʼӧ′кʼейгат.</ta>
            <ta e="T29" id="Seg_200" s="T26">′вӓркъ′jа ′кандит ′муkтут пӓт.</ta>
            <ta e="T32" id="Seg_201" s="T29">′нʼӱнʼо‵jа ′кандит шыт пӓт.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_202" s="T1">man tʼelamba taw äːtoɣɨn.</ta>
            <ta e="T8" id="Seg_203" s="T5">tämdə man orambaŋ.</ta>
            <ta e="T13" id="Seg_204" s="T8">meŋa somblʼe qwäj sasarɨj päː(ə)wə.</ta>
            <ta e="T16" id="Seg_205" s="T13">man illaŋ opomnaŋ.</ta>
            <ta e="T19" id="Seg_206" s="T16">tep an(n)an tättə üːčet.</ta>
            <ta e="T23" id="Seg_207" s="T19">täːpan tipiqumdə laqaŋ kolhosqɨn.</ta>
            <ta e="T26" id="Seg_208" s="T23">üčelat nʼünʼökʼejgat.</ta>
            <ta e="T29" id="Seg_209" s="T26">värkəja kandit muqtut pät.</ta>
            <ta e="T32" id="Seg_210" s="T29">nʼünʼoja kandit šɨt pät.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_211" s="T1">man tʼelamba taw äːtoɣɨn. </ta>
            <ta e="T8" id="Seg_212" s="T5">tämdə man orambaŋ. </ta>
            <ta e="T13" id="Seg_213" s="T8">meŋa somblʼe qwäj sasarɨj päːwə. </ta>
            <ta e="T16" id="Seg_214" s="T13">man illaŋ opomnaŋ. </ta>
            <ta e="T19" id="Seg_215" s="T16">tepanan tättə üːčet. </ta>
            <ta e="T23" id="Seg_216" s="T19">täːpan tipiqumdə laqaŋ kolhosqɨn. </ta>
            <ta e="T26" id="Seg_217" s="T23">üčelat nʼünʼökʼ ejgat. </ta>
            <ta e="T29" id="Seg_218" s="T26">wärkəjakandit muqtut pät. </ta>
            <ta e="T32" id="Seg_219" s="T29">nʼünʼojakandit šɨt pät. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_220" s="T1">man</ta>
            <ta e="T3" id="Seg_221" s="T2">tʼela-mba</ta>
            <ta e="T4" id="Seg_222" s="T3">taw</ta>
            <ta e="T5" id="Seg_223" s="T4">äːto-ɣɨn</ta>
            <ta e="T6" id="Seg_224" s="T5">tämdə</ta>
            <ta e="T7" id="Seg_225" s="T6">man</ta>
            <ta e="T8" id="Seg_226" s="T7">oram-ba-ŋ</ta>
            <ta e="T9" id="Seg_227" s="T8">meŋa</ta>
            <ta e="T10" id="Seg_228" s="T9">somblʼe</ta>
            <ta e="T11" id="Seg_229" s="T10">qwäj</ta>
            <ta e="T12" id="Seg_230" s="T11">sasarɨj</ta>
            <ta e="T13" id="Seg_231" s="T12">päː-wə</ta>
            <ta e="T14" id="Seg_232" s="T13">man</ta>
            <ta e="T15" id="Seg_233" s="T14">illa-ŋ</ta>
            <ta e="T16" id="Seg_234" s="T15">opo-mɨ-naŋ</ta>
            <ta e="T17" id="Seg_235" s="T16">tep-a-nan</ta>
            <ta e="T18" id="Seg_236" s="T17">tättə</ta>
            <ta e="T19" id="Seg_237" s="T18">üːče-tɨ</ta>
            <ta e="T20" id="Seg_238" s="T19">täːp-a-n</ta>
            <ta e="T21" id="Seg_239" s="T20">tipi-qum-də</ta>
            <ta e="T22" id="Seg_240" s="T21">laqa-ŋ</ta>
            <ta e="T23" id="Seg_241" s="T22">kolhos-qɨn</ta>
            <ta e="T24" id="Seg_242" s="T23">üče-la-tɨ</ta>
            <ta e="T25" id="Seg_243" s="T24">nʼünʼö-kʼ</ta>
            <ta e="T26" id="Seg_244" s="T25">ej-ga-t</ta>
            <ta e="T27" id="Seg_245" s="T26">wärkə-ja-kandit</ta>
            <ta e="T28" id="Seg_246" s="T27">muqtut</ta>
            <ta e="T29" id="Seg_247" s="T28">pä-tɨ</ta>
            <ta e="T30" id="Seg_248" s="T29">nʼünʼo-ja-kandit</ta>
            <ta e="T31" id="Seg_249" s="T30">šɨt</ta>
            <ta e="T32" id="Seg_250" s="T31">pä-tɨ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_251" s="T1">man</ta>
            <ta e="T3" id="Seg_252" s="T2">tʼeːlɨ-mbɨ</ta>
            <ta e="T4" id="Seg_253" s="T3">taw</ta>
            <ta e="T5" id="Seg_254" s="T4">eːto-qən</ta>
            <ta e="T6" id="Seg_255" s="T5">tɨmtɨ</ta>
            <ta e="T7" id="Seg_256" s="T6">man</ta>
            <ta e="T8" id="Seg_257" s="T7">orɨm-mbɨ-ŋ</ta>
            <ta e="T9" id="Seg_258" s="T8">mäkkä</ta>
            <ta e="T10" id="Seg_259" s="T9">sombɨlʼe</ta>
            <ta e="T11" id="Seg_260" s="T10">qwäj</ta>
            <ta e="T12" id="Seg_261" s="T11">sɨsarum</ta>
            <ta e="T13" id="Seg_262" s="T12">po-mɨ</ta>
            <ta e="T14" id="Seg_263" s="T13">man</ta>
            <ta e="T15" id="Seg_264" s="T14">illɨ-ŋ</ta>
            <ta e="T16" id="Seg_265" s="T15">oppɨ-mɨ-nan</ta>
            <ta e="T17" id="Seg_266" s="T16">tap-ɨ-nan</ta>
            <ta e="T18" id="Seg_267" s="T17">tättɨ</ta>
            <ta e="T19" id="Seg_268" s="T18">ütče-tɨ</ta>
            <ta e="T20" id="Seg_269" s="T19">tap-ɨ-n</ta>
            <ta e="T21" id="Seg_270" s="T20">tebe-qum-tɨ</ta>
            <ta e="T22" id="Seg_271" s="T21">laqqɨ-n</ta>
            <ta e="T23" id="Seg_272" s="T22">kalhos-qən</ta>
            <ta e="T24" id="Seg_273" s="T23">üččə-la-tɨ</ta>
            <ta e="T25" id="Seg_274" s="T24">nʼünʼü-k</ta>
            <ta e="T26" id="Seg_275" s="T25">eː-ŋɨ-tɨt</ta>
            <ta e="T27" id="Seg_276" s="T26">wərkə-ja-qəntɨ</ta>
            <ta e="T28" id="Seg_277" s="T27">muktut</ta>
            <ta e="T29" id="Seg_278" s="T28">po-tɨ</ta>
            <ta e="T30" id="Seg_279" s="T29">nʼünʼü-ja-qəntɨ</ta>
            <ta e="T31" id="Seg_280" s="T30">šittə</ta>
            <ta e="T32" id="Seg_281" s="T31">po-tɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_282" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_283" s="T2">be.born-PST.NAR.[3SG.S]</ta>
            <ta e="T4" id="Seg_284" s="T3">this</ta>
            <ta e="T5" id="Seg_285" s="T4">village-LOC</ta>
            <ta e="T6" id="Seg_286" s="T5">here</ta>
            <ta e="T7" id="Seg_287" s="T6">I.NOM</ta>
            <ta e="T8" id="Seg_288" s="T7">grow.up-PST.NAR-1SG.S</ta>
            <ta e="T9" id="Seg_289" s="T8">I.ALL</ta>
            <ta e="T10" id="Seg_290" s="T9">five</ta>
            <ta e="T11" id="Seg_291" s="T10">superfluous</ta>
            <ta e="T12" id="Seg_292" s="T11">twenty</ta>
            <ta e="T13" id="Seg_293" s="T12">year.[NOM]-1SG</ta>
            <ta e="T14" id="Seg_294" s="T13">I.NOM</ta>
            <ta e="T15" id="Seg_295" s="T14">live-1SG.S</ta>
            <ta e="T16" id="Seg_296" s="T15">older.sister-1SG-ADES</ta>
            <ta e="T17" id="Seg_297" s="T16">(s)he-EP-ADES</ta>
            <ta e="T18" id="Seg_298" s="T17">four</ta>
            <ta e="T19" id="Seg_299" s="T18">child.[NOM]-3SG</ta>
            <ta e="T20" id="Seg_300" s="T19">(s)he-EP-GEN</ta>
            <ta e="T21" id="Seg_301" s="T20">man-human.being.[NOM]-3SG</ta>
            <ta e="T22" id="Seg_302" s="T21">work-3SG.S</ta>
            <ta e="T23" id="Seg_303" s="T22">collective-LOC</ta>
            <ta e="T24" id="Seg_304" s="T23">child-PL.[NOM]-3SG</ta>
            <ta e="T25" id="Seg_305" s="T24">small-ADVZ</ta>
            <ta e="T26" id="Seg_306" s="T25">be-CO-3PL</ta>
            <ta e="T27" id="Seg_307" s="T26">big-DIM-EL.3SG</ta>
            <ta e="T28" id="Seg_308" s="T27">six</ta>
            <ta e="T29" id="Seg_309" s="T28">year.[NOM]-3SG</ta>
            <ta e="T30" id="Seg_310" s="T29">small-DIM-EL.3SG</ta>
            <ta e="T31" id="Seg_311" s="T30">two</ta>
            <ta e="T32" id="Seg_312" s="T31">year.[NOM]-3SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_313" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_314" s="T2">родиться-PST.NAR.[3SG.S]</ta>
            <ta e="T4" id="Seg_315" s="T3">этот</ta>
            <ta e="T5" id="Seg_316" s="T4">деревня-LOC</ta>
            <ta e="T6" id="Seg_317" s="T5">здесь</ta>
            <ta e="T7" id="Seg_318" s="T6">я.NOM</ta>
            <ta e="T8" id="Seg_319" s="T7">вырасти-PST.NAR-1SG.S</ta>
            <ta e="T9" id="Seg_320" s="T8">я.ALL</ta>
            <ta e="T10" id="Seg_321" s="T9">пять</ta>
            <ta e="T11" id="Seg_322" s="T10">излишний</ta>
            <ta e="T12" id="Seg_323" s="T11">двадцать</ta>
            <ta e="T13" id="Seg_324" s="T12">год.[NOM]-1SG</ta>
            <ta e="T14" id="Seg_325" s="T13">я.NOM</ta>
            <ta e="T15" id="Seg_326" s="T14">жить-1SG.S</ta>
            <ta e="T16" id="Seg_327" s="T15">старшая.сестра-1SG-ADES</ta>
            <ta e="T17" id="Seg_328" s="T16">он(а)-EP-ADES</ta>
            <ta e="T18" id="Seg_329" s="T17">четыре</ta>
            <ta e="T19" id="Seg_330" s="T18">ребёнок.[NOM]-3SG</ta>
            <ta e="T20" id="Seg_331" s="T19">он(а)-EP-GEN</ta>
            <ta e="T21" id="Seg_332" s="T20">мужчина-человек.[NOM]-3SG</ta>
            <ta e="T22" id="Seg_333" s="T21">работать-3SG.S</ta>
            <ta e="T23" id="Seg_334" s="T22">колхоз-LOC</ta>
            <ta e="T24" id="Seg_335" s="T23">ребёнок-PL.[NOM]-3SG</ta>
            <ta e="T25" id="Seg_336" s="T24">маленький-ADVZ</ta>
            <ta e="T26" id="Seg_337" s="T25">быть-CO-3PL</ta>
            <ta e="T27" id="Seg_338" s="T26">большой-DIM-EL.3SG</ta>
            <ta e="T28" id="Seg_339" s="T27">шесть</ta>
            <ta e="T29" id="Seg_340" s="T28">год.[NOM]-3SG</ta>
            <ta e="T30" id="Seg_341" s="T29">маленький-DIM-EL.3SG</ta>
            <ta e="T31" id="Seg_342" s="T30">два</ta>
            <ta e="T32" id="Seg_343" s="T31">год.[NOM]-3SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_344" s="T1">pers</ta>
            <ta e="T3" id="Seg_345" s="T2">v-v:tense.[v:pn]</ta>
            <ta e="T4" id="Seg_346" s="T3">dem</ta>
            <ta e="T5" id="Seg_347" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_348" s="T5">adv</ta>
            <ta e="T7" id="Seg_349" s="T6">pers</ta>
            <ta e="T8" id="Seg_350" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_351" s="T8">pers</ta>
            <ta e="T10" id="Seg_352" s="T9">num</ta>
            <ta e="T11" id="Seg_353" s="T10">adj</ta>
            <ta e="T12" id="Seg_354" s="T11">num</ta>
            <ta e="T13" id="Seg_355" s="T12">n.[n:case]-n:poss</ta>
            <ta e="T14" id="Seg_356" s="T13">pers</ta>
            <ta e="T15" id="Seg_357" s="T14">v-v:pn</ta>
            <ta e="T16" id="Seg_358" s="T15">n-n:poss-n:case</ta>
            <ta e="T17" id="Seg_359" s="T16">pers-n:ins-n:case</ta>
            <ta e="T18" id="Seg_360" s="T17">num</ta>
            <ta e="T19" id="Seg_361" s="T18">n.[n:case]-n:poss</ta>
            <ta e="T20" id="Seg_362" s="T19">pers-n:ins-n:case</ta>
            <ta e="T21" id="Seg_363" s="T20">n-n.[n:case]-n:poss</ta>
            <ta e="T22" id="Seg_364" s="T21">v-v:pn</ta>
            <ta e="T23" id="Seg_365" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_366" s="T23">n-n:num.[n:case]-n:poss</ta>
            <ta e="T25" id="Seg_367" s="T24">adj-adj&gt;adv</ta>
            <ta e="T26" id="Seg_368" s="T25">v-v:ins-v:pn</ta>
            <ta e="T27" id="Seg_369" s="T26">adj-n&gt;n-n:case.poss</ta>
            <ta e="T28" id="Seg_370" s="T27">num</ta>
            <ta e="T29" id="Seg_371" s="T28">n-n:poss</ta>
            <ta e="T30" id="Seg_372" s="T29">adj-n&gt;n-n:case.poss</ta>
            <ta e="T31" id="Seg_373" s="T30">num</ta>
            <ta e="T32" id="Seg_374" s="T31">n-n:poss</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_375" s="T1">pers</ta>
            <ta e="T3" id="Seg_376" s="T2">v</ta>
            <ta e="T4" id="Seg_377" s="T3">dem</ta>
            <ta e="T5" id="Seg_378" s="T4">n</ta>
            <ta e="T6" id="Seg_379" s="T5">adv</ta>
            <ta e="T7" id="Seg_380" s="T6">pers</ta>
            <ta e="T8" id="Seg_381" s="T7">v</ta>
            <ta e="T9" id="Seg_382" s="T8">pers</ta>
            <ta e="T10" id="Seg_383" s="T9">num</ta>
            <ta e="T11" id="Seg_384" s="T10">adj</ta>
            <ta e="T12" id="Seg_385" s="T11">num</ta>
            <ta e="T13" id="Seg_386" s="T12">n</ta>
            <ta e="T14" id="Seg_387" s="T13">pers</ta>
            <ta e="T15" id="Seg_388" s="T14">v</ta>
            <ta e="T16" id="Seg_389" s="T15">n</ta>
            <ta e="T17" id="Seg_390" s="T16">pers</ta>
            <ta e="T18" id="Seg_391" s="T17">num</ta>
            <ta e="T19" id="Seg_392" s="T18">n</ta>
            <ta e="T20" id="Seg_393" s="T19">pers</ta>
            <ta e="T21" id="Seg_394" s="T20">n</ta>
            <ta e="T22" id="Seg_395" s="T21">v</ta>
            <ta e="T23" id="Seg_396" s="T22">n</ta>
            <ta e="T24" id="Seg_397" s="T23">n</ta>
            <ta e="T25" id="Seg_398" s="T24">adv</ta>
            <ta e="T26" id="Seg_399" s="T25">v</ta>
            <ta e="T27" id="Seg_400" s="T26">n</ta>
            <ta e="T28" id="Seg_401" s="T27">num</ta>
            <ta e="T29" id="Seg_402" s="T28">v</ta>
            <ta e="T30" id="Seg_403" s="T29">n</ta>
            <ta e="T31" id="Seg_404" s="T30">num</ta>
            <ta e="T32" id="Seg_405" s="T31">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_406" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_407" s="T2">v:pred</ta>
            <ta e="T7" id="Seg_408" s="T6">pro.h:S</ta>
            <ta e="T8" id="Seg_409" s="T7">v:pred</ta>
            <ta e="T13" id="Seg_410" s="T12">np:S</ta>
            <ta e="T14" id="Seg_411" s="T13">pro.h:S</ta>
            <ta e="T15" id="Seg_412" s="T14">v:pred</ta>
            <ta e="T19" id="Seg_413" s="T18">np.h:S</ta>
            <ta e="T21" id="Seg_414" s="T20">np.h:S</ta>
            <ta e="T22" id="Seg_415" s="T21">v:pred</ta>
            <ta e="T24" id="Seg_416" s="T23">np.h:S</ta>
            <ta e="T25" id="Seg_417" s="T24">adj:pred</ta>
            <ta e="T26" id="Seg_418" s="T25">cop</ta>
            <ta e="T29" id="Seg_419" s="T28">np:S</ta>
            <ta e="T32" id="Seg_420" s="T31">np:S</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_421" s="T1">pro.h:P</ta>
            <ta e="T5" id="Seg_422" s="T4">np:L</ta>
            <ta e="T6" id="Seg_423" s="T5">adv:L</ta>
            <ta e="T7" id="Seg_424" s="T6">pro.h:P</ta>
            <ta e="T9" id="Seg_425" s="T8">pro.h:Poss</ta>
            <ta e="T13" id="Seg_426" s="T12">np:Th</ta>
            <ta e="T14" id="Seg_427" s="T13">pro.h:Th</ta>
            <ta e="T16" id="Seg_428" s="T15">np:L 0.1.h:Poss</ta>
            <ta e="T17" id="Seg_429" s="T16">np.h:Poss</ta>
            <ta e="T19" id="Seg_430" s="T18">np.h:Th</ta>
            <ta e="T20" id="Seg_431" s="T19">pro.h:Poss</ta>
            <ta e="T21" id="Seg_432" s="T20">np.h:A</ta>
            <ta e="T23" id="Seg_433" s="T22">np:L</ta>
            <ta e="T24" id="Seg_434" s="T23">np.h:Th 0.3.h:Poss</ta>
            <ta e="T27" id="Seg_435" s="T26">np.h:Poss 0.3.h:Poss</ta>
            <ta e="T29" id="Seg_436" s="T28">np:Th</ta>
            <ta e="T30" id="Seg_437" s="T29">np.h:Poss 0.3.h:Poss</ta>
            <ta e="T32" id="Seg_438" s="T31">np:Th</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T23" id="Seg_439" s="T22">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T23" id="Seg_440" s="T22">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_441" s="T1">I was born in this village.</ta>
            <ta e="T8" id="Seg_442" s="T5">I grew up here.</ta>
            <ta e="T13" id="Seg_443" s="T8">I am 25 years old.</ta>
            <ta e="T16" id="Seg_444" s="T13">I live with my sister.</ta>
            <ta e="T19" id="Seg_445" s="T16">She has got four children.</ta>
            <ta e="T23" id="Seg_446" s="T19">Her husband is working at the collective farm.</ta>
            <ta e="T26" id="Seg_447" s="T23">Her children are young.</ta>
            <ta e="T29" id="Seg_448" s="T26">The older one is six years old.</ta>
            <ta e="T32" id="Seg_449" s="T29">The younger one is two years old.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_450" s="T1">Ich bin in diesem Dorf geboren.</ta>
            <ta e="T8" id="Seg_451" s="T5">Ich bin hier groß geworden.</ta>
            <ta e="T13" id="Seg_452" s="T8">Ich bin 25 Jahre alt.</ta>
            <ta e="T16" id="Seg_453" s="T13">Ich wohne bei meiner Schwester.</ta>
            <ta e="T19" id="Seg_454" s="T16">Sie hat vier Kinder.</ta>
            <ta e="T23" id="Seg_455" s="T19">Ihr Mann arbeitet im Kolchos.</ta>
            <ta e="T26" id="Seg_456" s="T23">Ihre Kinder sind klein.</ta>
            <ta e="T29" id="Seg_457" s="T26">Der Ältere ist sechs Jahre alt.</ta>
            <ta e="T32" id="Seg_458" s="T29">Der Jüngere ist zwei Jahre alt.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_459" s="T1">Я родился в этой деревне.</ta>
            <ta e="T8" id="Seg_460" s="T5">Здесь я вырос.</ta>
            <ta e="T13" id="Seg_461" s="T8">Мне двадцать пять лет.</ta>
            <ta e="T16" id="Seg_462" s="T13">Я живу у моей сестры.</ta>
            <ta e="T19" id="Seg_463" s="T16">У нее четверо детей.</ta>
            <ta e="T23" id="Seg_464" s="T19">Ее муж работает в колхозе.</ta>
            <ta e="T26" id="Seg_465" s="T23">Ее дети маленькие.</ta>
            <ta e="T29" id="Seg_466" s="T26">Старшему шесть лет.</ta>
            <ta e="T32" id="Seg_467" s="T29">Младшему два года.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_468" s="T1">я родился в этой деревне</ta>
            <ta e="T8" id="Seg_469" s="T5">здесь я рос</ta>
            <ta e="T13" id="Seg_470" s="T8">мне двадцать пять лет</ta>
            <ta e="T16" id="Seg_471" s="T13">я живу с сестрой</ta>
            <ta e="T19" id="Seg_472" s="T16">у нее четверо детей</ta>
            <ta e="T23" id="Seg_473" s="T19">ее муж работает в колхозе</ta>
            <ta e="T26" id="Seg_474" s="T23">дети маленькие</ta>
            <ta e="T29" id="Seg_475" s="T26">старшему шесть лет</ta>
            <ta e="T32" id="Seg_476" s="T29">младшему два года</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
