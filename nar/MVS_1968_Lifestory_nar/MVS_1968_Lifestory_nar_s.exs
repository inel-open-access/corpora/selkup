<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>MVS_1968_Lifestory_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">MVS_1968_Lifestory_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">314</ud-information>
            <ud-information attribute-name="# HIAT:w">246</ud-information>
            <ud-information attribute-name="# e">246</ud-information>
            <ud-information attribute-name="# HIAT:u">52</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="MVS">
            <abbreviation>MVS</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="MVS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T246" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">mašek</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">saldat</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">iːmbat</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">mašek</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">qwändat</ts>
                  <nts id="Seg_20" n="HIAT:ip">,</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">mašek</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">oːɣolalešpɨt</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">Aːsenaɣɨt</ts>
                  <nts id="Seg_30" n="HIAT:ip">.</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_33" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_35" n="HIAT:w" s="T8">mašek</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">nɨnd</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">naːgur</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">äreɣɨnd</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">oːɣolalǯešpɨt</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_51" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">nutä</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">mašek</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">qwändɨt</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">Kʼemerowand</ts>
                  <nts id="Seg_63" n="HIAT:ip">.</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_66" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">mat</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">načit</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">uːǯʼak</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">šedə</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">pod</ts>
                  <nts id="Seg_81" n="HIAT:ip">.</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_84" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">mašek</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_89" n="HIAT:w" s="T23">načində</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_92" n="HIAT:w" s="T24">iːɣɨt</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">saldat</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_99" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_101" n="HIAT:w" s="T26">mat</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_104" n="HIAT:w" s="T27">načit</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_107" n="HIAT:w" s="T28">eːɣak</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">armijaɣɨt</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">muqtɨt</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_116" n="HIAT:w" s="T31">äret</ts>
                  <nts id="Seg_117" n="HIAT:ip">.</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_120" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_122" n="HIAT:w" s="T32">mi</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_125" n="HIAT:w" s="T33">načit</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_127" n="HIAT:ip">(</nts>
                  <ts e="T35" id="Seg_129" n="HIAT:w" s="T34">wajujčut</ts>
                  <nts id="Seg_130" n="HIAT:ip">)</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">mödetčut</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_137" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_139" n="HIAT:w" s="T36">mašek</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_142" n="HIAT:w" s="T37">mat</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_145" n="HIAT:w" s="T38">eːǯʼər</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_148" n="HIAT:w" s="T39">tuːdərak</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_151" n="HIAT:w" s="T40">tankət</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_154" n="HIAT:w" s="T41">petqɨn</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_157" n="HIAT:w" s="T42">tobowek</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_160" n="HIAT:w" s="T43">tank</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_163" n="HIAT:w" s="T44">mʼanʼteǯʼa</ts>
                  <nts id="Seg_164" n="HIAT:ip">.</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_167" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_169" n="HIAT:w" s="T45">mat</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_172" n="HIAT:w" s="T46">naːgur</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_175" n="HIAT:w" s="T47">čeːl</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_178" n="HIAT:w" s="T48">oːnek</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_181" n="HIAT:w" s="T49">tavaršlɨnanto</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_184" n="HIAT:w" s="T50">qaːlak</ts>
                  <nts id="Seg_185" n="HIAT:ip">.</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_188" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_190" n="HIAT:w" s="T51">mašek</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_193" n="HIAT:w" s="T52">nuːte</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_196" n="HIAT:w" s="T53">lʼečiješpɨt</ts>
                  <nts id="Seg_197" n="HIAT:ip">,</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_200" n="HIAT:w" s="T54">naːgur</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_203" n="HIAT:w" s="T55">čeːl</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_206" n="HIAT:w" s="T56">lʼečiišpɨt</ts>
                  <nts id="Seg_207" n="HIAT:ip">.</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_210" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_212" n="HIAT:w" s="T57">mašek</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_215" n="HIAT:w" s="T58">nannä</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_218" n="HIAT:w" s="T59">üːdɨt</ts>
                  <nts id="Seg_219" n="HIAT:ip">.</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_222" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_224" n="HIAT:w" s="T60">mat</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_227" n="HIAT:w" s="T61">mödəčak</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_230" n="HIAT:w" s="T62">šedəqoldət</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_233" n="HIAT:w" s="T63">tʼeres</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_236" n="HIAT:w" s="T64">pušpak</ts>
                  <nts id="Seg_237" n="HIAT:ip">.</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_240" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_242" n="HIAT:w" s="T65">nannä</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_245" n="HIAT:w" s="T66">üdat</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_248" n="HIAT:w" s="T67">mašek</ts>
                  <nts id="Seg_249" n="HIAT:ip">.</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_252" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_254" n="HIAT:w" s="T68">pudɨk</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_257" n="HIAT:w" s="T69">mat</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_260" n="HIAT:w" s="T70">nʼännä</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_263" n="HIAT:w" s="T71">čaːǯak</ts>
                  <nts id="Seg_264" n="HIAT:ip">.</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_267" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_269" n="HIAT:w" s="T72">nannäut</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_272" n="HIAT:w" s="T73">mašek</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_275" n="HIAT:w" s="T74">üːdaut</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_278" n="HIAT:w" s="T75">čwetčep</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_281" n="HIAT:w" s="T76">nannäl</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_284" n="HIAT:w" s="T77">mannɨmbɨgu</ts>
                  <nts id="Seg_285" n="HIAT:ip">.</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_288" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_290" n="HIAT:w" s="T78">mat</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_293" n="HIAT:w" s="T79">načit</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_296" n="HIAT:w" s="T80">qwajaɣak</ts>
                  <nts id="Seg_297" n="HIAT:ip">.</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_300" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_302" n="HIAT:w" s="T81">twesʼse</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_305" n="HIAT:w" s="T82">töːleblʼe</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_308" n="HIAT:w" s="T83">qaːdap</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_311" n="HIAT:w" s="T84">kamandirant</ts>
                  <nts id="Seg_312" n="HIAT:ip">:</nts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_315" n="HIAT:w" s="T85">nemʼes</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_318" n="HIAT:w" s="T86">nɨnd</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_321" n="HIAT:w" s="T87">eja</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_324" n="HIAT:w" s="T88">qättaɣɨt</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_327" n="HIAT:w" s="T89">aːmdat</ts>
                  <nts id="Seg_328" n="HIAT:ip">.</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_331" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_333" n="HIAT:w" s="T90">tap</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_336" n="HIAT:w" s="T91">meka</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_339" n="HIAT:w" s="T92">qadɨt</ts>
                  <nts id="Seg_340" n="HIAT:ip">:</nts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_343" n="HIAT:w" s="T93">dawaj</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_346" n="HIAT:w" s="T94">čeːk</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_349" n="HIAT:w" s="T95">čaːǯlut</ts>
                  <nts id="Seg_350" n="HIAT:ip">.</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_353" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_355" n="HIAT:w" s="T96">mat</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_358" n="HIAT:w" s="T97">üːbɨǯʼak</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_361" n="HIAT:w" s="T98">medrap</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_364" n="HIAT:w" s="T99">i</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_367" n="HIAT:w" s="T100">ček</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_370" n="HIAT:w" s="T101">čaːǯaj</ts>
                  <nts id="Seg_371" n="HIAT:ip">.</nts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_374" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_376" n="HIAT:w" s="T102">moɣoɣot</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_379" n="HIAT:w" s="T103">saldanla</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_382" n="HIAT:w" s="T104">čaːǯat</ts>
                  <nts id="Seg_383" n="HIAT:ip">.</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_386" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_388" n="HIAT:w" s="T105">mi</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_391" n="HIAT:w" s="T106">čagetɨmbaut</ts>
                  <nts id="Seg_392" n="HIAT:ip">.</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_395" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_397" n="HIAT:w" s="T107">mat</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_400" n="HIAT:w" s="T108">taːdap</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_403" n="HIAT:w" s="T109">kamandirap</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_406" n="HIAT:w" s="T110">qät</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_409" n="HIAT:w" s="T111">tomn</ts>
                  <nts id="Seg_410" n="HIAT:ip">.</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_413" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_415" n="HIAT:w" s="T112">mi</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_418" n="HIAT:w" s="T113">tabnopti</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_421" n="HIAT:w" s="T114">kɨgelaj</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_424" n="HIAT:w" s="T115">nemecla</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_427" n="HIAT:w" s="T116">mantčeǯʼigu</ts>
                  <nts id="Seg_428" n="HIAT:ip">.</nts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_431" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_433" n="HIAT:w" s="T117">kamandir</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_436" n="HIAT:w" s="T118">ɨnnä</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_439" n="HIAT:w" s="T119">wašeǯʼa</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_442" n="HIAT:w" s="T120">mantčeǯʼigu</ts>
                  <nts id="Seg_443" n="HIAT:ip">.</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_446" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_448" n="HIAT:w" s="T121">načindo</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_451" n="HIAT:w" s="T122">nʼemec</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_454" n="HIAT:w" s="T123">qolča</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_457" n="HIAT:w" s="T124">miši</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_460" n="HIAT:w" s="T125">i</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_463" n="HIAT:w" s="T126">načindo</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_466" n="HIAT:w" s="T127">čaːtča</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_469" n="HIAT:w" s="T128">miši</ts>
                  <nts id="Seg_470" n="HIAT:ip">,</nts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_473" n="HIAT:w" s="T129">ranniŋ</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_476" n="HIAT:w" s="T130">šinnä</ts>
                  <nts id="Seg_477" n="HIAT:ip">.</nts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_480" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_482" n="HIAT:w" s="T131">kamandʼirom</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_485" n="HIAT:w" s="T132">aː</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_488" n="HIAT:w" s="T133">qwäjarh</ts>
                  <nts id="Seg_489" n="HIAT:ip">.</nts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_492" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_494" n="HIAT:w" s="T134">nɨnd</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_497" n="HIAT:w" s="T135">mat</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_500" n="HIAT:w" s="T136">udonä</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_503" n="HIAT:w" s="T137">aːdəlǯap</ts>
                  <nts id="Seg_504" n="HIAT:ip">,</nts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_507" n="HIAT:w" s="T138">uːdunde</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_510" n="HIAT:w" s="T139">qöːrälǯit</ts>
                  <nts id="Seg_511" n="HIAT:ip">,</nts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_514" n="HIAT:w" s="T140">olomd</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_517" n="HIAT:w" s="T141">ännä</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_520" n="HIAT:w" s="T142">əg</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_523" n="HIAT:w" s="T143">wačoǯembeːt</ts>
                  <nts id="Seg_524" n="HIAT:ip">.</nts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_527" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_529" n="HIAT:w" s="T144">tab</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_532" n="HIAT:w" s="T145">qoštɨt</ts>
                  <nts id="Seg_533" n="HIAT:ip">.</nts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_536" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_538" n="HIAT:w" s="T146">čwesse</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_541" n="HIAT:w" s="T147">parkwa</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_544" n="HIAT:w" s="T148">tab</ts>
                  <nts id="Seg_545" n="HIAT:ip">.</nts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_548" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_550" n="HIAT:w" s="T149">načindo</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_553" n="HIAT:w" s="T150">töšpa</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_556" n="HIAT:w" s="T151">feršel</ts>
                  <nts id="Seg_557" n="HIAT:ip">.</nts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_560" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_562" n="HIAT:w" s="T152">miɣeni</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_565" n="HIAT:w" s="T153">töːa</ts>
                  <nts id="Seg_566" n="HIAT:ip">.</nts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_569" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_571" n="HIAT:w" s="T154">medɨgu</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_574" n="HIAT:w" s="T155">ne</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_577" n="HIAT:w" s="T156">moʒet</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_580" n="HIAT:w" s="T157">miːɣənni</ts>
                  <nts id="Seg_581" n="HIAT:ip">.</nts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_584" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_586" n="HIAT:w" s="T158">tabə</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_589" n="HIAT:w" s="T159">abmotkam</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_592" n="HIAT:w" s="T160">tä</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_595" n="HIAT:w" s="T161">tamdɨlʼešpat</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_597" n="HIAT:ip">(</nts>
                  <ts e="T163" id="Seg_599" n="HIAT:w" s="T162">tä</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_602" n="HIAT:w" s="T163">tamdännɨt</ts>
                  <nts id="Seg_603" n="HIAT:ip">)</nts>
                  <nts id="Seg_604" n="HIAT:ip">.</nts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_607" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_609" n="HIAT:w" s="T164">miši</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_612" n="HIAT:w" s="T165">nute</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_615" n="HIAT:w" s="T166">tawaj</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_618" n="HIAT:w" s="T167">ügəlešpɨgu</ts>
                  <nts id="Seg_619" n="HIAT:ip">.</nts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_622" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_624" n="HIAT:w" s="T168">wot</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_627" n="HIAT:w" s="T169">üːgənna</ts>
                  <nts id="Seg_628" n="HIAT:ip">,</nts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_631" n="HIAT:w" s="T170">abmotkaɣe</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_634" n="HIAT:w" s="T171">üːgənna</ts>
                  <nts id="Seg_635" n="HIAT:ip">.</nts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_638" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_640" n="HIAT:w" s="T172">kamandirom</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_643" n="HIAT:w" s="T173">qwannat</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_646" n="HIAT:w" s="T174">sawsem</ts>
                  <nts id="Seg_647" n="HIAT:ip">.</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_650" n="HIAT:u" s="T175">
                  <ts e="T176" id="Seg_652" n="HIAT:w" s="T175">mašik</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_655" n="HIAT:w" s="T176">kuːrennat</ts>
                  <nts id="Seg_656" n="HIAT:ip">,</nts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_659" n="HIAT:w" s="T177">pannatnɨt</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_662" n="HIAT:w" s="T178">akopoɣɨnt</ts>
                  <nts id="Seg_663" n="HIAT:ip">.</nts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_666" n="HIAT:u" s="T179">
                  <ts e="T180" id="Seg_668" n="HIAT:w" s="T179">muktɨt</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_671" n="HIAT:w" s="T180">čaːstkundɨ</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_674" n="HIAT:w" s="T181">mat</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_677" n="HIAT:w" s="T182">akopoɣɨn</ts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_680" n="HIAT:w" s="T183">eːpak</ts>
                  <nts id="Seg_681" n="HIAT:ip">.</nts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_684" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_686" n="HIAT:w" s="T184">mašik</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_689" n="HIAT:w" s="T185">nutə</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_692" n="HIAT:w" s="T186">üːgɨn</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_695" n="HIAT:w" s="T187">saninstruktor</ts>
                  <nts id="Seg_696" n="HIAT:ip">.</nts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_699" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_701" n="HIAT:w" s="T188">mašik</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_704" n="HIAT:w" s="T189">tabə</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_707" n="HIAT:w" s="T190">taːdərɨt</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_710" n="HIAT:w" s="T191">hombla</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_713" n="HIAT:w" s="T192">faːron</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_716" n="HIAT:w" s="T193">tin</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_718" n="HIAT:ip">(</nts>
                  <ts e="T195" id="Seg_720" n="HIAT:w" s="T194">hombla</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_723" n="HIAT:w" s="T195">faːrot</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_726" n="HIAT:w" s="T196">tiː</ts>
                  <nts id="Seg_727" n="HIAT:ip">,</nts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_730" n="HIAT:w" s="T197">homblaqwäj</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_733" n="HIAT:w" s="T198">tiː</ts>
                  <nts id="Seg_734" n="HIAT:ip">)</nts>
                  <nts id="Seg_735" n="HIAT:ip">.</nts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T202" id="Seg_738" n="HIAT:u" s="T199">
                  <ts e="T200" id="Seg_740" n="HIAT:w" s="T199">patom</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_743" n="HIAT:w" s="T200">čundeː</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_746" n="HIAT:w" s="T201">taːdərɨt</ts>
                  <nts id="Seg_747" n="HIAT:ip">.</nts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_750" n="HIAT:u" s="T202">
                  <ts e="T203" id="Seg_752" n="HIAT:w" s="T202">mašik</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_755" n="HIAT:w" s="T203">perwal</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_758" n="HIAT:w" s="T204">qärant</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_761" n="HIAT:w" s="T205">taːdɨt</ts>
                  <nts id="Seg_762" n="HIAT:ip">.</nts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_765" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_767" n="HIAT:w" s="T206">nɨnd</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_770" n="HIAT:w" s="T207">manana</ts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_773" n="HIAT:w" s="T208">tä</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_776" n="HIAT:w" s="T209">meʒalqɨlbat</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_779" n="HIAT:w" s="T210">askolalap</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_782" n="HIAT:w" s="T211">toboɣɨndo</ts>
                  <nts id="Seg_783" n="HIAT:ip">.</nts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_786" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_788" n="HIAT:w" s="T212">nute</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_791" n="HIAT:w" s="T213">mašik</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_794" n="HIAT:w" s="T214">tʼwesse</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_797" n="HIAT:w" s="T215">taːdərɨgu</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_800" n="HIAT:w" s="T216">üːbərat</ts>
                  <nts id="Seg_801" n="HIAT:ip">.</nts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_804" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_806" n="HIAT:w" s="T217">mašik</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_809" n="HIAT:w" s="T218">nutə</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_812" n="HIAT:w" s="T219">linijend</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_815" n="HIAT:w" s="T220">qwändɨt</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_818" n="HIAT:w" s="T221">mašinaɣe</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_821" n="HIAT:w" s="T222">taːdərɨt</ts>
                  <nts id="Seg_822" n="HIAT:ip">.</nts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T230" id="Seg_825" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_827" n="HIAT:w" s="T223">mat</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_830" n="HIAT:w" s="T224">eːpak</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_833" n="HIAT:w" s="T225">palnisaɣät</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_836" n="HIAT:w" s="T226">šədə</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_839" n="HIAT:w" s="T227">pod</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_842" n="HIAT:w" s="T228">tät</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_845" n="HIAT:w" s="T229">äːret</ts>
                  <nts id="Seg_846" n="HIAT:ip">.</nts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_849" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_851" n="HIAT:w" s="T230">nutə</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_854" n="HIAT:w" s="T231">mašik</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_857" n="HIAT:w" s="T232">lečiɣit</ts>
                  <nts id="Seg_858" n="HIAT:ip">.</nts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_861" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_863" n="HIAT:w" s="T233">mat</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_866" n="HIAT:w" s="T234">qwajaɣak</ts>
                  <nts id="Seg_867" n="HIAT:ip">.</nts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T239" id="Seg_870" n="HIAT:u" s="T235">
                  <ts e="T236" id="Seg_872" n="HIAT:w" s="T235">nutə</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_875" n="HIAT:w" s="T236">mašik</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_878" n="HIAT:w" s="T237">čwesse</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_881" n="HIAT:w" s="T238">üːdət</ts>
                  <nts id="Seg_882" n="HIAT:ip">.</nts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_885" n="HIAT:u" s="T239">
                  <ts e="T240" id="Seg_887" n="HIAT:w" s="T239">mat</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_890" n="HIAT:w" s="T240">moɣonä</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_893" n="HIAT:w" s="T241">töːɣak</ts>
                  <nts id="Seg_894" n="HIAT:ip">.</nts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T246" id="Seg_897" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_899" n="HIAT:w" s="T242">nɨnblʼe</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_902" n="HIAT:w" s="T243">mat</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_905" n="HIAT:w" s="T244">maːtqɨt</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_908" n="HIAT:w" s="T245">wargak</ts>
                  <nts id="Seg_909" n="HIAT:ip">.</nts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T246" id="Seg_911" n="sc" s="T0">
               <ts e="T1" id="Seg_913" n="e" s="T0">mašek </ts>
               <ts e="T2" id="Seg_915" n="e" s="T1">saldat </ts>
               <ts e="T3" id="Seg_917" n="e" s="T2">iːmbat. </ts>
               <ts e="T4" id="Seg_919" n="e" s="T3">mašek </ts>
               <ts e="T5" id="Seg_921" n="e" s="T4">qwändat, </ts>
               <ts e="T6" id="Seg_923" n="e" s="T5">mašek </ts>
               <ts e="T7" id="Seg_925" n="e" s="T6">oːɣolalešpɨt </ts>
               <ts e="T8" id="Seg_927" n="e" s="T7">Aːsenaɣɨt. </ts>
               <ts e="T9" id="Seg_929" n="e" s="T8">mašek </ts>
               <ts e="T10" id="Seg_931" n="e" s="T9">nɨnd </ts>
               <ts e="T11" id="Seg_933" n="e" s="T10">naːgur </ts>
               <ts e="T12" id="Seg_935" n="e" s="T11">äreɣɨnd </ts>
               <ts e="T13" id="Seg_937" n="e" s="T12">oːɣolalǯešpɨt. </ts>
               <ts e="T14" id="Seg_939" n="e" s="T13">nutä </ts>
               <ts e="T15" id="Seg_941" n="e" s="T14">mašek </ts>
               <ts e="T16" id="Seg_943" n="e" s="T15">qwändɨt </ts>
               <ts e="T17" id="Seg_945" n="e" s="T16">Kʼemerowand. </ts>
               <ts e="T18" id="Seg_947" n="e" s="T17">mat </ts>
               <ts e="T19" id="Seg_949" n="e" s="T18">načit </ts>
               <ts e="T20" id="Seg_951" n="e" s="T19">uːǯʼak </ts>
               <ts e="T21" id="Seg_953" n="e" s="T20">šedə </ts>
               <ts e="T22" id="Seg_955" n="e" s="T21">pod. </ts>
               <ts e="T23" id="Seg_957" n="e" s="T22">mašek </ts>
               <ts e="T24" id="Seg_959" n="e" s="T23">načində </ts>
               <ts e="T25" id="Seg_961" n="e" s="T24">iːɣɨt </ts>
               <ts e="T26" id="Seg_963" n="e" s="T25">saldat. </ts>
               <ts e="T27" id="Seg_965" n="e" s="T26">mat </ts>
               <ts e="T28" id="Seg_967" n="e" s="T27">načit </ts>
               <ts e="T29" id="Seg_969" n="e" s="T28">eːɣak </ts>
               <ts e="T30" id="Seg_971" n="e" s="T29">armijaɣɨt </ts>
               <ts e="T31" id="Seg_973" n="e" s="T30">muqtɨt </ts>
               <ts e="T32" id="Seg_975" n="e" s="T31">äret. </ts>
               <ts e="T33" id="Seg_977" n="e" s="T32">mi </ts>
               <ts e="T34" id="Seg_979" n="e" s="T33">načit </ts>
               <ts e="T35" id="Seg_981" n="e" s="T34">(wajujčut) </ts>
               <ts e="T36" id="Seg_983" n="e" s="T35">mödetčut. </ts>
               <ts e="T37" id="Seg_985" n="e" s="T36">mašek </ts>
               <ts e="T38" id="Seg_987" n="e" s="T37">mat </ts>
               <ts e="T39" id="Seg_989" n="e" s="T38">eːǯʼər </ts>
               <ts e="T40" id="Seg_991" n="e" s="T39">tuːdərak </ts>
               <ts e="T41" id="Seg_993" n="e" s="T40">tankət </ts>
               <ts e="T42" id="Seg_995" n="e" s="T41">petqɨn </ts>
               <ts e="T43" id="Seg_997" n="e" s="T42">tobowek </ts>
               <ts e="T44" id="Seg_999" n="e" s="T43">tank </ts>
               <ts e="T45" id="Seg_1001" n="e" s="T44">mʼanʼteǯʼa. </ts>
               <ts e="T46" id="Seg_1003" n="e" s="T45">mat </ts>
               <ts e="T47" id="Seg_1005" n="e" s="T46">naːgur </ts>
               <ts e="T48" id="Seg_1007" n="e" s="T47">čeːl </ts>
               <ts e="T49" id="Seg_1009" n="e" s="T48">oːnek </ts>
               <ts e="T50" id="Seg_1011" n="e" s="T49">tavaršlɨnanto </ts>
               <ts e="T51" id="Seg_1013" n="e" s="T50">qaːlak. </ts>
               <ts e="T52" id="Seg_1015" n="e" s="T51">mašek </ts>
               <ts e="T53" id="Seg_1017" n="e" s="T52">nuːte </ts>
               <ts e="T54" id="Seg_1019" n="e" s="T53">lʼečiješpɨt, </ts>
               <ts e="T55" id="Seg_1021" n="e" s="T54">naːgur </ts>
               <ts e="T56" id="Seg_1023" n="e" s="T55">čeːl </ts>
               <ts e="T57" id="Seg_1025" n="e" s="T56">lʼečiišpɨt. </ts>
               <ts e="T58" id="Seg_1027" n="e" s="T57">mašek </ts>
               <ts e="T59" id="Seg_1029" n="e" s="T58">nannä </ts>
               <ts e="T60" id="Seg_1031" n="e" s="T59">üːdɨt. </ts>
               <ts e="T61" id="Seg_1033" n="e" s="T60">mat </ts>
               <ts e="T62" id="Seg_1035" n="e" s="T61">mödəčak </ts>
               <ts e="T63" id="Seg_1037" n="e" s="T62">šedəqoldət </ts>
               <ts e="T64" id="Seg_1039" n="e" s="T63">tʼeres </ts>
               <ts e="T65" id="Seg_1041" n="e" s="T64">pušpak. </ts>
               <ts e="T66" id="Seg_1043" n="e" s="T65">nannä </ts>
               <ts e="T67" id="Seg_1045" n="e" s="T66">üdat </ts>
               <ts e="T68" id="Seg_1047" n="e" s="T67">mašek. </ts>
               <ts e="T69" id="Seg_1049" n="e" s="T68">pudɨk </ts>
               <ts e="T70" id="Seg_1051" n="e" s="T69">mat </ts>
               <ts e="T71" id="Seg_1053" n="e" s="T70">nʼännä </ts>
               <ts e="T72" id="Seg_1055" n="e" s="T71">čaːǯak. </ts>
               <ts e="T73" id="Seg_1057" n="e" s="T72">nannäut </ts>
               <ts e="T74" id="Seg_1059" n="e" s="T73">mašek </ts>
               <ts e="T75" id="Seg_1061" n="e" s="T74">üːdaut </ts>
               <ts e="T76" id="Seg_1063" n="e" s="T75">čwetčep </ts>
               <ts e="T77" id="Seg_1065" n="e" s="T76">nannäl </ts>
               <ts e="T78" id="Seg_1067" n="e" s="T77">mannɨmbɨgu. </ts>
               <ts e="T79" id="Seg_1069" n="e" s="T78">mat </ts>
               <ts e="T80" id="Seg_1071" n="e" s="T79">načit </ts>
               <ts e="T81" id="Seg_1073" n="e" s="T80">qwajaɣak. </ts>
               <ts e="T82" id="Seg_1075" n="e" s="T81">twesʼse </ts>
               <ts e="T83" id="Seg_1077" n="e" s="T82">töːleblʼe </ts>
               <ts e="T84" id="Seg_1079" n="e" s="T83">qaːdap </ts>
               <ts e="T85" id="Seg_1081" n="e" s="T84">kamandirant: </ts>
               <ts e="T86" id="Seg_1083" n="e" s="T85">nemʼes </ts>
               <ts e="T87" id="Seg_1085" n="e" s="T86">nɨnd </ts>
               <ts e="T88" id="Seg_1087" n="e" s="T87">eja </ts>
               <ts e="T89" id="Seg_1089" n="e" s="T88">qättaɣɨt </ts>
               <ts e="T90" id="Seg_1091" n="e" s="T89">aːmdat. </ts>
               <ts e="T91" id="Seg_1093" n="e" s="T90">tap </ts>
               <ts e="T92" id="Seg_1095" n="e" s="T91">meka </ts>
               <ts e="T93" id="Seg_1097" n="e" s="T92">qadɨt: </ts>
               <ts e="T94" id="Seg_1099" n="e" s="T93">dawaj </ts>
               <ts e="T95" id="Seg_1101" n="e" s="T94">čeːk </ts>
               <ts e="T96" id="Seg_1103" n="e" s="T95">čaːǯlut. </ts>
               <ts e="T97" id="Seg_1105" n="e" s="T96">mat </ts>
               <ts e="T98" id="Seg_1107" n="e" s="T97">üːbɨǯʼak </ts>
               <ts e="T99" id="Seg_1109" n="e" s="T98">medrap </ts>
               <ts e="T100" id="Seg_1111" n="e" s="T99">i </ts>
               <ts e="T101" id="Seg_1113" n="e" s="T100">ček </ts>
               <ts e="T102" id="Seg_1115" n="e" s="T101">čaːǯaj. </ts>
               <ts e="T103" id="Seg_1117" n="e" s="T102">moɣoɣot </ts>
               <ts e="T104" id="Seg_1119" n="e" s="T103">saldanla </ts>
               <ts e="T105" id="Seg_1121" n="e" s="T104">čaːǯat. </ts>
               <ts e="T106" id="Seg_1123" n="e" s="T105">mi </ts>
               <ts e="T107" id="Seg_1125" n="e" s="T106">čagetɨmbaut. </ts>
               <ts e="T108" id="Seg_1127" n="e" s="T107">mat </ts>
               <ts e="T109" id="Seg_1129" n="e" s="T108">taːdap </ts>
               <ts e="T110" id="Seg_1131" n="e" s="T109">kamandirap </ts>
               <ts e="T111" id="Seg_1133" n="e" s="T110">qät </ts>
               <ts e="T112" id="Seg_1135" n="e" s="T111">tomn. </ts>
               <ts e="T113" id="Seg_1137" n="e" s="T112">mi </ts>
               <ts e="T114" id="Seg_1139" n="e" s="T113">tabnopti </ts>
               <ts e="T115" id="Seg_1141" n="e" s="T114">kɨgelaj </ts>
               <ts e="T116" id="Seg_1143" n="e" s="T115">nemecla </ts>
               <ts e="T117" id="Seg_1145" n="e" s="T116">mantčeǯʼigu. </ts>
               <ts e="T118" id="Seg_1147" n="e" s="T117">kamandir </ts>
               <ts e="T119" id="Seg_1149" n="e" s="T118">ɨnnä </ts>
               <ts e="T120" id="Seg_1151" n="e" s="T119">wašeǯʼa </ts>
               <ts e="T121" id="Seg_1153" n="e" s="T120">mantčeǯʼigu. </ts>
               <ts e="T122" id="Seg_1155" n="e" s="T121">načindo </ts>
               <ts e="T123" id="Seg_1157" n="e" s="T122">nʼemec </ts>
               <ts e="T124" id="Seg_1159" n="e" s="T123">qolča </ts>
               <ts e="T125" id="Seg_1161" n="e" s="T124">miši </ts>
               <ts e="T126" id="Seg_1163" n="e" s="T125">i </ts>
               <ts e="T127" id="Seg_1165" n="e" s="T126">načindo </ts>
               <ts e="T128" id="Seg_1167" n="e" s="T127">čaːtča </ts>
               <ts e="T129" id="Seg_1169" n="e" s="T128">miši, </ts>
               <ts e="T130" id="Seg_1171" n="e" s="T129">ranniŋ </ts>
               <ts e="T131" id="Seg_1173" n="e" s="T130">šinnä. </ts>
               <ts e="T132" id="Seg_1175" n="e" s="T131">kamandʼirom </ts>
               <ts e="T133" id="Seg_1177" n="e" s="T132">aː </ts>
               <ts e="T134" id="Seg_1179" n="e" s="T133">qwäjarh. </ts>
               <ts e="T135" id="Seg_1181" n="e" s="T134">nɨnd </ts>
               <ts e="T136" id="Seg_1183" n="e" s="T135">mat </ts>
               <ts e="T137" id="Seg_1185" n="e" s="T136">udonä </ts>
               <ts e="T138" id="Seg_1187" n="e" s="T137">aːdəlǯap, </ts>
               <ts e="T139" id="Seg_1189" n="e" s="T138">uːdunde </ts>
               <ts e="T140" id="Seg_1191" n="e" s="T139">qöːrälǯit, </ts>
               <ts e="T141" id="Seg_1193" n="e" s="T140">olomd </ts>
               <ts e="T142" id="Seg_1195" n="e" s="T141">ännä </ts>
               <ts e="T143" id="Seg_1197" n="e" s="T142">əg </ts>
               <ts e="T144" id="Seg_1199" n="e" s="T143">wačoǯembeːt. </ts>
               <ts e="T145" id="Seg_1201" n="e" s="T144">tab </ts>
               <ts e="T146" id="Seg_1203" n="e" s="T145">qoštɨt. </ts>
               <ts e="T147" id="Seg_1205" n="e" s="T146">čwesse </ts>
               <ts e="T148" id="Seg_1207" n="e" s="T147">parkwa </ts>
               <ts e="T149" id="Seg_1209" n="e" s="T148">tab. </ts>
               <ts e="T150" id="Seg_1211" n="e" s="T149">načindo </ts>
               <ts e="T151" id="Seg_1213" n="e" s="T150">töšpa </ts>
               <ts e="T152" id="Seg_1215" n="e" s="T151">feršel. </ts>
               <ts e="T153" id="Seg_1217" n="e" s="T152">miɣeni </ts>
               <ts e="T154" id="Seg_1219" n="e" s="T153">töːa. </ts>
               <ts e="T155" id="Seg_1221" n="e" s="T154">medɨgu </ts>
               <ts e="T156" id="Seg_1223" n="e" s="T155">ne </ts>
               <ts e="T157" id="Seg_1225" n="e" s="T156">moʒet </ts>
               <ts e="T158" id="Seg_1227" n="e" s="T157">miːɣənni. </ts>
               <ts e="T159" id="Seg_1229" n="e" s="T158">tabə </ts>
               <ts e="T160" id="Seg_1231" n="e" s="T159">abmotkam </ts>
               <ts e="T161" id="Seg_1233" n="e" s="T160">tä </ts>
               <ts e="T162" id="Seg_1235" n="e" s="T161">tamdɨlʼešpat </ts>
               <ts e="T163" id="Seg_1237" n="e" s="T162">(tä </ts>
               <ts e="T164" id="Seg_1239" n="e" s="T163">tamdännɨt). </ts>
               <ts e="T165" id="Seg_1241" n="e" s="T164">miši </ts>
               <ts e="T166" id="Seg_1243" n="e" s="T165">nute </ts>
               <ts e="T167" id="Seg_1245" n="e" s="T166">tawaj </ts>
               <ts e="T168" id="Seg_1247" n="e" s="T167">ügəlešpɨgu. </ts>
               <ts e="T169" id="Seg_1249" n="e" s="T168">wot </ts>
               <ts e="T170" id="Seg_1251" n="e" s="T169">üːgənna, </ts>
               <ts e="T171" id="Seg_1253" n="e" s="T170">abmotkaɣe </ts>
               <ts e="T172" id="Seg_1255" n="e" s="T171">üːgənna. </ts>
               <ts e="T173" id="Seg_1257" n="e" s="T172">kamandirom </ts>
               <ts e="T174" id="Seg_1259" n="e" s="T173">qwannat </ts>
               <ts e="T175" id="Seg_1261" n="e" s="T174">sawsem. </ts>
               <ts e="T176" id="Seg_1263" n="e" s="T175">mašik </ts>
               <ts e="T177" id="Seg_1265" n="e" s="T176">kuːrennat, </ts>
               <ts e="T178" id="Seg_1267" n="e" s="T177">pannatnɨt </ts>
               <ts e="T179" id="Seg_1269" n="e" s="T178">akopoɣɨnt. </ts>
               <ts e="T180" id="Seg_1271" n="e" s="T179">muktɨt </ts>
               <ts e="T181" id="Seg_1273" n="e" s="T180">čaːstkundɨ </ts>
               <ts e="T182" id="Seg_1275" n="e" s="T181">mat </ts>
               <ts e="T183" id="Seg_1277" n="e" s="T182">akopoɣɨn </ts>
               <ts e="T184" id="Seg_1279" n="e" s="T183">eːpak. </ts>
               <ts e="T185" id="Seg_1281" n="e" s="T184">mašik </ts>
               <ts e="T186" id="Seg_1283" n="e" s="T185">nutə </ts>
               <ts e="T187" id="Seg_1285" n="e" s="T186">üːgɨn </ts>
               <ts e="T188" id="Seg_1287" n="e" s="T187">saninstruktor. </ts>
               <ts e="T189" id="Seg_1289" n="e" s="T188">mašik </ts>
               <ts e="T190" id="Seg_1291" n="e" s="T189">tabə </ts>
               <ts e="T191" id="Seg_1293" n="e" s="T190">taːdərɨt </ts>
               <ts e="T192" id="Seg_1295" n="e" s="T191">hombla </ts>
               <ts e="T193" id="Seg_1297" n="e" s="T192">faːron </ts>
               <ts e="T194" id="Seg_1299" n="e" s="T193">tin </ts>
               <ts e="T195" id="Seg_1301" n="e" s="T194">(hombla </ts>
               <ts e="T196" id="Seg_1303" n="e" s="T195">faːrot </ts>
               <ts e="T197" id="Seg_1305" n="e" s="T196">tiː, </ts>
               <ts e="T198" id="Seg_1307" n="e" s="T197">homblaqwäj </ts>
               <ts e="T199" id="Seg_1309" n="e" s="T198">tiː). </ts>
               <ts e="T200" id="Seg_1311" n="e" s="T199">patom </ts>
               <ts e="T201" id="Seg_1313" n="e" s="T200">čundeː </ts>
               <ts e="T202" id="Seg_1315" n="e" s="T201">taːdərɨt. </ts>
               <ts e="T203" id="Seg_1317" n="e" s="T202">mašik </ts>
               <ts e="T204" id="Seg_1319" n="e" s="T203">perwal </ts>
               <ts e="T205" id="Seg_1321" n="e" s="T204">qärant </ts>
               <ts e="T206" id="Seg_1323" n="e" s="T205">taːdɨt. </ts>
               <ts e="T207" id="Seg_1325" n="e" s="T206">nɨnd </ts>
               <ts e="T208" id="Seg_1327" n="e" s="T207">manana </ts>
               <ts e="T209" id="Seg_1329" n="e" s="T208">tä </ts>
               <ts e="T210" id="Seg_1331" n="e" s="T209">meʒalqɨlbat </ts>
               <ts e="T211" id="Seg_1333" n="e" s="T210">askolalap </ts>
               <ts e="T212" id="Seg_1335" n="e" s="T211">toboɣɨndo. </ts>
               <ts e="T213" id="Seg_1337" n="e" s="T212">nute </ts>
               <ts e="T214" id="Seg_1339" n="e" s="T213">mašik </ts>
               <ts e="T215" id="Seg_1341" n="e" s="T214">tʼwesse </ts>
               <ts e="T216" id="Seg_1343" n="e" s="T215">taːdərɨgu </ts>
               <ts e="T217" id="Seg_1345" n="e" s="T216">üːbərat. </ts>
               <ts e="T218" id="Seg_1347" n="e" s="T217">mašik </ts>
               <ts e="T219" id="Seg_1349" n="e" s="T218">nutə </ts>
               <ts e="T220" id="Seg_1351" n="e" s="T219">linijend </ts>
               <ts e="T221" id="Seg_1353" n="e" s="T220">qwändɨt </ts>
               <ts e="T222" id="Seg_1355" n="e" s="T221">mašinaɣe </ts>
               <ts e="T223" id="Seg_1357" n="e" s="T222">taːdərɨt. </ts>
               <ts e="T224" id="Seg_1359" n="e" s="T223">mat </ts>
               <ts e="T225" id="Seg_1361" n="e" s="T224">eːpak </ts>
               <ts e="T226" id="Seg_1363" n="e" s="T225">palnisaɣät </ts>
               <ts e="T227" id="Seg_1365" n="e" s="T226">šədə </ts>
               <ts e="T228" id="Seg_1367" n="e" s="T227">pod </ts>
               <ts e="T229" id="Seg_1369" n="e" s="T228">tät </ts>
               <ts e="T230" id="Seg_1371" n="e" s="T229">äːret. </ts>
               <ts e="T231" id="Seg_1373" n="e" s="T230">nutə </ts>
               <ts e="T232" id="Seg_1375" n="e" s="T231">mašik </ts>
               <ts e="T233" id="Seg_1377" n="e" s="T232">lečiɣit. </ts>
               <ts e="T234" id="Seg_1379" n="e" s="T233">mat </ts>
               <ts e="T235" id="Seg_1381" n="e" s="T234">qwajaɣak. </ts>
               <ts e="T236" id="Seg_1383" n="e" s="T235">nutə </ts>
               <ts e="T237" id="Seg_1385" n="e" s="T236">mašik </ts>
               <ts e="T238" id="Seg_1387" n="e" s="T237">čwesse </ts>
               <ts e="T239" id="Seg_1389" n="e" s="T238">üːdət. </ts>
               <ts e="T240" id="Seg_1391" n="e" s="T239">mat </ts>
               <ts e="T241" id="Seg_1393" n="e" s="T240">moɣonä </ts>
               <ts e="T242" id="Seg_1395" n="e" s="T241">töːɣak. </ts>
               <ts e="T243" id="Seg_1397" n="e" s="T242">nɨnblʼe </ts>
               <ts e="T244" id="Seg_1399" n="e" s="T243">mat </ts>
               <ts e="T245" id="Seg_1401" n="e" s="T244">maːtqɨt </ts>
               <ts e="T246" id="Seg_1403" n="e" s="T245">wargak. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_1404" s="T0">MVS_1968_Lifestory_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_1405" s="T3">MVS_1968_Lifestory_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_1406" s="T8">MVS_1968_Lifestory_nar.003 (001.003)</ta>
            <ta e="T17" id="Seg_1407" s="T13">MVS_1968_Lifestory_nar.004 (001.004)</ta>
            <ta e="T22" id="Seg_1408" s="T17">MVS_1968_Lifestory_nar.005 (001.005)</ta>
            <ta e="T26" id="Seg_1409" s="T22">MVS_1968_Lifestory_nar.006 (001.006)</ta>
            <ta e="T32" id="Seg_1410" s="T26">MVS_1968_Lifestory_nar.007 (001.007)</ta>
            <ta e="T36" id="Seg_1411" s="T32">MVS_1968_Lifestory_nar.008 (001.008)</ta>
            <ta e="T45" id="Seg_1412" s="T36">MVS_1968_Lifestory_nar.009 (001.009)</ta>
            <ta e="T51" id="Seg_1413" s="T45">MVS_1968_Lifestory_nar.010 (001.010)</ta>
            <ta e="T57" id="Seg_1414" s="T51">MVS_1968_Lifestory_nar.011 (001.011)</ta>
            <ta e="T60" id="Seg_1415" s="T57">MVS_1968_Lifestory_nar.012 (001.012)</ta>
            <ta e="T65" id="Seg_1416" s="T60">MVS_1968_Lifestory_nar.013 (001.013)</ta>
            <ta e="T68" id="Seg_1417" s="T65">MVS_1968_Lifestory_nar.014 (001.014)</ta>
            <ta e="T72" id="Seg_1418" s="T68">MVS_1968_Lifestory_nar.015 (001.015)</ta>
            <ta e="T78" id="Seg_1419" s="T72">MVS_1968_Lifestory_nar.016 (001.016)</ta>
            <ta e="T81" id="Seg_1420" s="T78">MVS_1968_Lifestory_nar.017 (001.017)</ta>
            <ta e="T90" id="Seg_1421" s="T81">MVS_1968_Lifestory_nar.018 (001.018)</ta>
            <ta e="T96" id="Seg_1422" s="T90">MVS_1968_Lifestory_nar.019 (001.019)</ta>
            <ta e="T102" id="Seg_1423" s="T96">MVS_1968_Lifestory_nar.020 (001.020)</ta>
            <ta e="T105" id="Seg_1424" s="T102">MVS_1968_Lifestory_nar.021 (001.021)</ta>
            <ta e="T107" id="Seg_1425" s="T105">MVS_1968_Lifestory_nar.022 (001.022)</ta>
            <ta e="T112" id="Seg_1426" s="T107">MVS_1968_Lifestory_nar.023 (001.023)</ta>
            <ta e="T117" id="Seg_1427" s="T112">MVS_1968_Lifestory_nar.024 (001.024)</ta>
            <ta e="T121" id="Seg_1428" s="T117">MVS_1968_Lifestory_nar.025 (001.025)</ta>
            <ta e="T131" id="Seg_1429" s="T121">MVS_1968_Lifestory_nar.026 (001.026)</ta>
            <ta e="T134" id="Seg_1430" s="T131">MVS_1968_Lifestory_nar.027 (001.027)</ta>
            <ta e="T144" id="Seg_1431" s="T134">MVS_1968_Lifestory_nar.028 (001.028)</ta>
            <ta e="T146" id="Seg_1432" s="T144">MVS_1968_Lifestory_nar.029 (001.029)</ta>
            <ta e="T149" id="Seg_1433" s="T146">MVS_1968_Lifestory_nar.030 (001.030)</ta>
            <ta e="T152" id="Seg_1434" s="T149">MVS_1968_Lifestory_nar.031 (001.031)</ta>
            <ta e="T154" id="Seg_1435" s="T152">MVS_1968_Lifestory_nar.032 (001.032)</ta>
            <ta e="T158" id="Seg_1436" s="T154">MVS_1968_Lifestory_nar.033 (001.033)</ta>
            <ta e="T164" id="Seg_1437" s="T158">MVS_1968_Lifestory_nar.034 (001.034)</ta>
            <ta e="T168" id="Seg_1438" s="T164">MVS_1968_Lifestory_nar.035 (001.035)</ta>
            <ta e="T172" id="Seg_1439" s="T168">MVS_1968_Lifestory_nar.036 (001.036)</ta>
            <ta e="T175" id="Seg_1440" s="T172">MVS_1968_Lifestory_nar.037 (001.037)</ta>
            <ta e="T179" id="Seg_1441" s="T175">MVS_1968_Lifestory_nar.038 (001.038)</ta>
            <ta e="T184" id="Seg_1442" s="T179">MVS_1968_Lifestory_nar.039 (001.039)</ta>
            <ta e="T188" id="Seg_1443" s="T184">MVS_1968_Lifestory_nar.040 (001.040)</ta>
            <ta e="T199" id="Seg_1444" s="T188">MVS_1968_Lifestory_nar.041 (001.041)</ta>
            <ta e="T202" id="Seg_1445" s="T199">MVS_1968_Lifestory_nar.042 (001.042)</ta>
            <ta e="T206" id="Seg_1446" s="T202">MVS_1968_Lifestory_nar.043 (001.043)</ta>
            <ta e="T212" id="Seg_1447" s="T206">MVS_1968_Lifestory_nar.044 (001.044)</ta>
            <ta e="T217" id="Seg_1448" s="T212">MVS_1968_Lifestory_nar.045 (001.045)</ta>
            <ta e="T223" id="Seg_1449" s="T217">MVS_1968_Lifestory_nar.046 (001.046)</ta>
            <ta e="T230" id="Seg_1450" s="T223">MVS_1968_Lifestory_nar.047 (001.047)</ta>
            <ta e="T233" id="Seg_1451" s="T230">MVS_1968_Lifestory_nar.048 (001.048)</ta>
            <ta e="T235" id="Seg_1452" s="T233">MVS_1968_Lifestory_nar.049 (001.049)</ta>
            <ta e="T239" id="Seg_1453" s="T235">MVS_1968_Lifestory_nar.050 (001.050)</ta>
            <ta e="T242" id="Seg_1454" s="T239">MVS_1968_Lifestory_nar.051 (001.051)</ta>
            <ta e="T246" id="Seg_1455" s="T242">MVS_1968_Lifestory_nar.052 (001.052)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_1456" s="T0">ма′шек саl′дат ӣмбат.</ta>
            <ta e="T8" id="Seg_1457" s="T3">ма′шек ‵kwӓндат, ма′шек ‵о̄ɣоlаlеш′пыт а̄се′наɣыт.</ta>
            <ta e="T13" id="Seg_1458" s="T8">ма′шек нынд на̄гур (и)ä(е)′реɣынд о̄ɣоlаlджеш′пыт.</ta>
            <ta e="T17" id="Seg_1459" s="T13">ну′тӓ ма′шек kwӓндыт кʼемерованд.</ta>
            <ta e="T22" id="Seg_1460" s="T17">мат на′чит ӯдʼжʼак ‵шедъ ′под.</ta>
            <ta e="T26" id="Seg_1461" s="T22">ма′шек на′чиндъ ′ӣɣыт ‵саl′дат.</ta>
            <ta e="T32" id="Seg_1462" s="T26">мат на′чит ′е̄ɣак ′армиjаɣыт ′муkтыт ӓ′рет.</ta>
            <ta e="T36" id="Seg_1463" s="T32">ми на′чит (ва′jуйчут) ‵мӧдет′чут.</ta>
            <ta e="T45" id="Seg_1464" s="T36">ма′шек мат е̄дʼжʼър ′тӯдърак ′танкът пет′kын ′тобовек танк мʼа′нʼтедʼжʼа.</ta>
            <ta e="T51" id="Seg_1465" s="T45">мат на̄гур че̄l о̄′нек та′варшлынанто kа̄′lак.</ta>
            <ta e="T57" id="Seg_1466" s="T51">ма′шек нӯ′те̨ лʼе′чиjешпыт, на̄гур че̄l лʼе′чиишпыт.</ta>
            <ta e="T60" id="Seg_1467" s="T57">ма′шек на′ннӓ ′ӱ̄дыт.</ta>
            <ta e="T65" id="Seg_1468" s="T60">мат ‵мӧдъ′чак ‵шедъkоlдът тʼерес ′пуш‵пак.</ta>
            <ta e="T68" id="Seg_1469" s="T65">на′ннӓ ′ӱдат машек.</ta>
            <ta e="T72" id="Seg_1470" s="T68">′пудык мат нʼӓ′ннӓ ′ча̄джак.</ta>
            <ta e="T78" id="Seg_1471" s="T72">на′ннӓут ма′шек ′ӱ̄даут ′чветчеп на′ннӓl ‵манным′быгу.</ta>
            <ta e="T81" id="Seg_1472" s="T78">мат на′чит kwа′jаɣак.</ta>
            <ta e="T90" id="Seg_1473" s="T81">твесʼсе тӧ̄lеп(б)лʼе kа̄′дап ‵каман′дира(ъ)нт: немʼес нындеjа kӓттаɣыт а̄мдат.</ta>
            <ta e="T96" id="Seg_1474" s="T90">тап мека kа′дыт: давай че̄к ча̄джlут.</ta>
            <ta e="T102" id="Seg_1475" s="T96">мат ӱ̄быдʼжʼак мед′рап и чек ча̄джай.</ta>
            <ta e="T105" id="Seg_1476" s="T102">мо′ɣоɣот саl′данlа ча̄джат.</ta>
            <ta e="T107" id="Seg_1477" s="T105">ми ча′гетымбаут.</ta>
            <ta e="T112" id="Seg_1478" s="T107">мат та̄′дап каман′дирап kӓт ′томн.</ta>
            <ta e="T117" id="Seg_1479" s="T112">ми таб′нопти кы′гелай немецла ман′тчедʼжʼигу.</ta>
            <ta e="T121" id="Seg_1480" s="T117">камандир ы′ннӓ ва′шедʼжʼа ман′тчедʼжʼигу.</ta>
            <ta e="T131" id="Seg_1481" s="T121">на′чиндо нʼемец kол′ча ′миши и на′чиндо ча̄т′ча ′мишʼи, ′ранниң ши′ннӓ.</ta>
            <ta e="T134" id="Seg_1482" s="T131">каман′дʼиром а̄ ′kwӓjарh.</ta>
            <ta e="T144" id="Seg_1483" s="T134">′нынд мат у′донӓ а̄дъl′джап, ӯду(ы)нде ′kӧ̄′рӓlджит, о′lомд ӓ′ннӓ ъг ва′чоджем′бе̄т.</ta>
            <ta e="T146" id="Seg_1484" s="T144">таб kоштыт.</ta>
            <ta e="T149" id="Seg_1485" s="T146">′чвессе парква таб.</ta>
            <ta e="T152" id="Seg_1486" s="T149">на′чиндо тӧшпа фершел.</ta>
            <ta e="T154" id="Seg_1487" s="T152">миɣени ′тӧ̄а.</ta>
            <ta e="T158" id="Seg_1488" s="T154">‵меды′гу не ′может мӣɣънни.</ta>
            <ta e="T164" id="Seg_1489" s="T158">табъ аб′моткам тӓ тамды′лʼешпат (тӓ тамдӓнныт).</ta>
            <ta e="T168" id="Seg_1490" s="T164">′миши нуте та′вай ′ӱгъlешпыгу.</ta>
            <ta e="T172" id="Seg_1491" s="T168">вот ′ӱ̄гънна, а′бмоткаɣе ӱ̄гънна.</ta>
            <ta e="T175" id="Seg_1492" s="T172">каман′диром kwа′ннат сав′сем.</ta>
            <ta e="T179" id="Seg_1493" s="T175">′машик кӯ′реннат, па′ннатныт а′копоɣынт.</ta>
            <ta e="T184" id="Seg_1494" s="T179">муктыт ча̄сткунды мат а′копоɣын е̄′пак.</ta>
            <ta e="T188" id="Seg_1495" s="T184">ма′шик ′нутъ ′ӱ̄гын санинструктор.</ta>
            <ta e="T199" id="Seg_1496" s="T188">машик табъ та̄дърыт hомбlа фа̄рон тин (hом′бlа фа̄рот тӣ, hомбlаkwӓй тӣ).</ta>
            <ta e="T202" id="Seg_1497" s="T199">патом чун′де̄ та̄дърыт.</ta>
            <ta e="T206" id="Seg_1498" s="T202">ма′шик перваl kӓ′рант та̄′дыт.</ta>
            <ta e="T212" id="Seg_1499" s="T206">′нынд ма′нана тӓ ме′жалkылбат ас′коlаlап то′боɣындо.</ta>
            <ta e="T217" id="Seg_1500" s="T212">′нуте ′ма′шик тʼвессе та̄дърыгу ӱ̄бърат.</ta>
            <ta e="T223" id="Seg_1501" s="T217">ма′шик ′нутъ ′lиниjенд ‵kwӓн′дыт ма′шинаɣе та̄дърыт.</ta>
            <ta e="T230" id="Seg_1502" s="T223">мат е̄′пак паl′нисаɣӓт ‵шъдъ ′под̂ тӓт ӓ̄′рет.</ta>
            <ta e="T233" id="Seg_1503" s="T230">′нутъ ма′шик lе′чиɣ(h)ит.</ta>
            <ta e="T235" id="Seg_1504" s="T233">мат kwа′jаɣак.</ta>
            <ta e="T239" id="Seg_1505" s="T235">′нутъ машик ′чвессе ′ӱ̄дът.</ta>
            <ta e="T242" id="Seg_1506" s="T239">мат моɣонӓ ′тӧ̄ɣак.</ta>
            <ta e="T246" id="Seg_1507" s="T242">нынб′лʼе мат ма̄тkыт вар′гак.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_1508" s="T0">mašek saldat iːmbat.</ta>
            <ta e="T8" id="Seg_1509" s="T3">mašek qwändat, mašek oːɣolalešpɨt aːsenaɣɨt.</ta>
            <ta e="T13" id="Seg_1510" s="T8">mašek nɨnd naːgur ä(e)reɣɨnd oːɣolalǯešpɨt.</ta>
            <ta e="T17" id="Seg_1511" s="T13">nutä mašek qwändɨt kʼemerovand.</ta>
            <ta e="T22" id="Seg_1512" s="T17">mat načit uːǯʼak šedə pod.</ta>
            <ta e="T26" id="Seg_1513" s="T22">mašek načində iːɣɨt saldat.</ta>
            <ta e="T32" id="Seg_1514" s="T26">mat načit eːɣak armijaɣɨt muqtɨt äret.</ta>
            <ta e="T36" id="Seg_1515" s="T32">mi načit (vajujčut) mödetčut.</ta>
            <ta e="T45" id="Seg_1516" s="T36">mašek mat eːdʼʒʼər tuːdərak tankət petqɨn (pötqɨŋ) tobovek tank mʼanʼtedʼʒʼa.</ta>
            <ta e="T51" id="Seg_1517" s="T45">mat naːgur čeːl oːnek tavaršlɨnanto qaːlak.</ta>
            <ta e="T57" id="Seg_1518" s="T51">mašek nuːte lʼečiješpɨt, naːgur čeːl lʼečiišpɨt.</ta>
            <ta e="T60" id="Seg_1519" s="T57">mašek nannä üːdɨt.</ta>
            <ta e="T65" id="Seg_1520" s="T60">mat mödəčak šedəqoldət tʼeres pušpak.</ta>
            <ta e="T68" id="Seg_1521" s="T65">nannä üdat mašek.</ta>
            <ta e="T72" id="Seg_1522" s="T68">pudɨk mat nʼännä čaːǯak.</ta>
            <ta e="T78" id="Seg_1523" s="T72">nannäut mašek üːdaut čvetčep nannäl mannɨmbɨgu.</ta>
            <ta e="T81" id="Seg_1524" s="T78">mat načit qwajaɣak.</ta>
            <ta e="T90" id="Seg_1525" s="T81">tvesʼse töːlep(b)lʼe qaːdap kamandira(ə)nt: nemʼes nɨndeja qättaɣɨt aːmdat.</ta>
            <ta e="T96" id="Seg_1526" s="T90">tap meka qadɨt: davaj čeːk čaːǯlut.</ta>
            <ta e="T102" id="Seg_1527" s="T96">mat üːbɨdʼʒʼak medrap i ček čaːǯaj.</ta>
            <ta e="T105" id="Seg_1528" s="T102">moɣoɣot saldanla čaːǯat.</ta>
            <ta e="T107" id="Seg_1529" s="T105">mi čagetɨmbaut.</ta>
            <ta e="T112" id="Seg_1530" s="T107">mat taːdap kamandirap qät tomn.</ta>
            <ta e="T117" id="Seg_1531" s="T112">mi tabnopti kɨgelaj nemecla mantčedʼʒʼigu.</ta>
            <ta e="T121" id="Seg_1532" s="T117">kamandir ɨnnä vašedʼʒʼa mantčedʼʒʼigu.</ta>
            <ta e="T131" id="Seg_1533" s="T121">načindo nʼemec qolča miši i načindo čaːtča miši, ranniŋ šinnä.</ta>
            <ta e="T134" id="Seg_1534" s="T131">kamandʼirom aː qwäjarh.</ta>
            <ta e="T144" id="Seg_1535" s="T134">nɨnd mat udonä aːdəlǯap, uːdu(ɨ)nde qöːrälǯit, olomd ännä əg vačoǯembeːt.</ta>
            <ta e="T146" id="Seg_1536" s="T144">tab qoštɨt.</ta>
            <ta e="T149" id="Seg_1537" s="T146">čvesse parkva tab.</ta>
            <ta e="T152" id="Seg_1538" s="T149">načindo töšpa feršel.</ta>
            <ta e="T154" id="Seg_1539" s="T152">miɣeni töːa.</ta>
            <ta e="T158" id="Seg_1540" s="T154">medɨgu ne moʒet miːɣənni.</ta>
            <ta e="T164" id="Seg_1541" s="T158">tabə abmotkam tä tamdɨlʼešpat (tä tamdännɨt).</ta>
            <ta e="T168" id="Seg_1542" s="T164">miši nute tavaj ügəlešpɨgu.</ta>
            <ta e="T172" id="Seg_1543" s="T168">vot üːgənna, abmotkaɣe üːgənna.</ta>
            <ta e="T175" id="Seg_1544" s="T172">kamandirom qwannat savsem.</ta>
            <ta e="T179" id="Seg_1545" s="T175">mašik kuːrennat, pannatnɨt akopoɣɨnt.</ta>
            <ta e="T184" id="Seg_1546" s="T179">muktɨt čaːstkundɨ mat akopoɣɨn eːpak.</ta>
            <ta e="T188" id="Seg_1547" s="T184">mašik nutə üːgɨn saninstruktor.</ta>
            <ta e="T199" id="Seg_1548" s="T188">mašik tabə taːdərɨt hombla faːron tin (hombla faːrot tiː, homblaqwäj tiː).</ta>
            <ta e="T202" id="Seg_1549" s="T199">patom čundeː taːdərɨt.</ta>
            <ta e="T206" id="Seg_1550" s="T202">mašik perval qärant taːdɨt.</ta>
            <ta e="T212" id="Seg_1551" s="T206">nɨnd manana tä meʒalqɨlbat askolalap toboɣɨndo.</ta>
            <ta e="T217" id="Seg_1552" s="T212">nute mašik tʼvesse taːdərɨgu üːbərat.</ta>
            <ta e="T223" id="Seg_1553" s="T217">mašik nutə linijend qwändɨt mašinaɣe taːdərɨt.</ta>
            <ta e="T230" id="Seg_1554" s="T223">mat eːpak palnisaɣät šədə pod̂ tät äːret.</ta>
            <ta e="T233" id="Seg_1555" s="T230">nutə mašik lečiɣ(h)it.</ta>
            <ta e="T235" id="Seg_1556" s="T233">mat qwajaɣak.</ta>
            <ta e="T239" id="Seg_1557" s="T235">nutə mašik čvesse üːdət.</ta>
            <ta e="T242" id="Seg_1558" s="T239">mat moɣonä töːɣak.</ta>
            <ta e="T246" id="Seg_1559" s="T242">nɨnblʼe mat maːtqɨt vargak.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_1560" s="T0">mašek saldat iːmbat. </ta>
            <ta e="T8" id="Seg_1561" s="T3">mašek qwändat, mašek oːɣolalešpɨt Aːsenaɣɨt. </ta>
            <ta e="T13" id="Seg_1562" s="T8">mašek nɨnd naːgur äreɣɨnd oːɣolalǯešpɨt. </ta>
            <ta e="T17" id="Seg_1563" s="T13">nutä mašek qwändɨt Kʼemerowand. </ta>
            <ta e="T22" id="Seg_1564" s="T17">mat načit uːǯʼak šedə pod. </ta>
            <ta e="T26" id="Seg_1565" s="T22">mašek načində iːɣɨt saldat. </ta>
            <ta e="T32" id="Seg_1566" s="T26">mat načit eːɣak armijaɣɨt muqtɨt äret. </ta>
            <ta e="T36" id="Seg_1567" s="T32">mi načit (wajujčut) mödetčut. </ta>
            <ta e="T45" id="Seg_1568" s="T36">mašek mat eːǯʼər tuːdərak tankət petqɨn tobowek tank mʼanʼteǯʼa. </ta>
            <ta e="T51" id="Seg_1569" s="T45">mat naːgur čeːl oːnek tavaršlɨnanto qaːlak. </ta>
            <ta e="T57" id="Seg_1570" s="T51">mašek nuːte lʼečiješpɨt, naːgur čeːl lʼečiišpɨt. </ta>
            <ta e="T60" id="Seg_1571" s="T57">mašek nannä üːdɨt. </ta>
            <ta e="T65" id="Seg_1572" s="T60">mat mödəčak šedəqoldət tʼeres pušpak. </ta>
            <ta e="T68" id="Seg_1573" s="T65">nannä üdat mašek. </ta>
            <ta e="T72" id="Seg_1574" s="T68">pudɨk mat nʼännä čaːǯak. </ta>
            <ta e="T78" id="Seg_1575" s="T72">nannäut mašek üːdaut čwetčep nannäl mannɨmbɨgu. </ta>
            <ta e="T81" id="Seg_1576" s="T78">mat načit qwajaɣak. </ta>
            <ta e="T90" id="Seg_1577" s="T81">twesʼse töːleblʼe qaːdap kamandirant: nemʼes nɨnd eja qättaɣɨt aːmdat. </ta>
            <ta e="T96" id="Seg_1578" s="T90">tap meka qadɨt: dawaj čeːk čaːǯlut. </ta>
            <ta e="T102" id="Seg_1579" s="T96">mat üːbɨǯʼak medrap i ček čaːǯaj. </ta>
            <ta e="T105" id="Seg_1580" s="T102">moɣoɣot saldanla čaːǯat. </ta>
            <ta e="T107" id="Seg_1581" s="T105">mi čagetɨmbaut. </ta>
            <ta e="T112" id="Seg_1582" s="T107">mat taːdap kamandirap qät tomn. </ta>
            <ta e="T117" id="Seg_1583" s="T112">mi tabnopti kɨgelaj nemecla mantčeǯʼigu. </ta>
            <ta e="T121" id="Seg_1584" s="T117">kamandir ɨnnä wašeǯʼa mantčeǯʼigu. </ta>
            <ta e="T131" id="Seg_1585" s="T121">načindo nʼemec qolča miši i načindo čaːtča miši, ranniŋ šinnä. </ta>
            <ta e="T134" id="Seg_1586" s="T131">kamandʼirom aː qwäjarh. </ta>
            <ta e="T144" id="Seg_1587" s="T134">nɨnd mat udonä aːdəlǯap, uːdunde qöːrälǯit, olomd ännä əg wačoǯembeːt. </ta>
            <ta e="T146" id="Seg_1588" s="T144">tab qoštɨt. </ta>
            <ta e="T149" id="Seg_1589" s="T146">čwesse parkwa tab. </ta>
            <ta e="T152" id="Seg_1590" s="T149">načindo töšpa feršel. </ta>
            <ta e="T154" id="Seg_1591" s="T152">miɣeni töːa. </ta>
            <ta e="T158" id="Seg_1592" s="T154">medɨgu ne moʒet miːɣənni. </ta>
            <ta e="T164" id="Seg_1593" s="T158">tabə abmotkam tä tamdɨlʼešpat (tä tamdännɨt). </ta>
            <ta e="T168" id="Seg_1594" s="T164">miši nute tawaj ügəlešpɨgu. </ta>
            <ta e="T172" id="Seg_1595" s="T168">wot üːgənna, abmotkaɣe üːgənna. </ta>
            <ta e="T175" id="Seg_1596" s="T172">kamandirom qwannat sawsem. </ta>
            <ta e="T179" id="Seg_1597" s="T175">mašik kuːrennat, pannatnɨt akopoɣɨnt. </ta>
            <ta e="T184" id="Seg_1598" s="T179">muktɨt čaːstkundɨ mat akopoɣɨn eːpak. </ta>
            <ta e="T188" id="Seg_1599" s="T184">mašik nutə üːgɨn saninstruktor. </ta>
            <ta e="T199" id="Seg_1600" s="T188">mašik tabə taːdərɨt hombla faːron tin (hombla faːrot tiː, homblaqwäj tiː). </ta>
            <ta e="T202" id="Seg_1601" s="T199">patom čundeː taːdərɨt. </ta>
            <ta e="T206" id="Seg_1602" s="T202">mašik perwal qärant taːdɨt. </ta>
            <ta e="T212" id="Seg_1603" s="T206">nɨnd manana tä meʒalqɨlbat askolalap toboɣɨndo. </ta>
            <ta e="T217" id="Seg_1604" s="T212">nute mašik tʼwesse taːdərɨgu üːbərat. </ta>
            <ta e="T223" id="Seg_1605" s="T217">mašik nutə linijend qwändɨt mašinaɣe taːdərɨt. </ta>
            <ta e="T230" id="Seg_1606" s="T223">mat eːpak palnisaɣät šədə pod tät äːret. </ta>
            <ta e="T233" id="Seg_1607" s="T230">nutə mašik lečiɣit. </ta>
            <ta e="T235" id="Seg_1608" s="T233">mat qwajaɣak. </ta>
            <ta e="T239" id="Seg_1609" s="T235">nutə mašik čwesse üːdət. </ta>
            <ta e="T242" id="Seg_1610" s="T239">mat moɣonä töːɣak. </ta>
            <ta e="T246" id="Seg_1611" s="T242">nɨnblʼe mat maːtqɨt wargak. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1612" s="T0">mašek</ta>
            <ta e="T2" id="Seg_1613" s="T1">salda-t</ta>
            <ta e="T3" id="Seg_1614" s="T2">iː-mba-t</ta>
            <ta e="T4" id="Seg_1615" s="T3">mašek</ta>
            <ta e="T5" id="Seg_1616" s="T4">qwän-da-t</ta>
            <ta e="T6" id="Seg_1617" s="T5">mašek</ta>
            <ta e="T7" id="Seg_1618" s="T6">oːɣol-a-le-špɨ-t</ta>
            <ta e="T8" id="Seg_1619" s="T7">Aːsena-ɣɨt</ta>
            <ta e="T9" id="Seg_1620" s="T8">mašek</ta>
            <ta e="T10" id="Seg_1621" s="T9">nɨnd</ta>
            <ta e="T11" id="Seg_1622" s="T10">naːgur</ta>
            <ta e="T12" id="Seg_1623" s="T11">äre-ɣɨnd</ta>
            <ta e="T13" id="Seg_1624" s="T12">oːɣol-a-lǯe-špɨ-t</ta>
            <ta e="T14" id="Seg_1625" s="T13">nutä</ta>
            <ta e="T15" id="Seg_1626" s="T14">mašek</ta>
            <ta e="T16" id="Seg_1627" s="T15">qwän-dɨ-t</ta>
            <ta e="T17" id="Seg_1628" s="T16">Kʼemerowa-nd</ta>
            <ta e="T18" id="Seg_1629" s="T17">mat</ta>
            <ta e="T19" id="Seg_1630" s="T18">nači-t</ta>
            <ta e="T20" id="Seg_1631" s="T19">uːǯʼa-k</ta>
            <ta e="T21" id="Seg_1632" s="T20">šedə</ta>
            <ta e="T22" id="Seg_1633" s="T21">po-d</ta>
            <ta e="T23" id="Seg_1634" s="T22">mašek</ta>
            <ta e="T24" id="Seg_1635" s="T23">nači-ndə</ta>
            <ta e="T25" id="Seg_1636" s="T24">iː-ɣɨ-t</ta>
            <ta e="T26" id="Seg_1637" s="T25">salda-t</ta>
            <ta e="T27" id="Seg_1638" s="T26">mat</ta>
            <ta e="T28" id="Seg_1639" s="T27">nači-t</ta>
            <ta e="T29" id="Seg_1640" s="T28">eː-ɣa-k</ta>
            <ta e="T30" id="Seg_1641" s="T29">armija-ɣɨt</ta>
            <ta e="T31" id="Seg_1642" s="T30">muqtɨt</ta>
            <ta e="T32" id="Seg_1643" s="T31">äre-t</ta>
            <ta e="T33" id="Seg_1644" s="T32">mi</ta>
            <ta e="T34" id="Seg_1645" s="T33">nači-t</ta>
            <ta e="T35" id="Seg_1646" s="T34">wajuj-č-ut</ta>
            <ta e="T36" id="Seg_1647" s="T35">möde-tč-ut</ta>
            <ta e="T37" id="Seg_1648" s="T36">mašek</ta>
            <ta e="T38" id="Seg_1649" s="T37">mat</ta>
            <ta e="T39" id="Seg_1650" s="T38">eːǯʼər</ta>
            <ta e="T40" id="Seg_1651" s="T39">tuːdəra-k</ta>
            <ta e="T41" id="Seg_1652" s="T40">tank-ə-t</ta>
            <ta e="T42" id="Seg_1653" s="T41">pet-qɨn</ta>
            <ta e="T43" id="Seg_1654" s="T42">tob-o-wek</ta>
            <ta e="T44" id="Seg_1655" s="T43">tank</ta>
            <ta e="T45" id="Seg_1656" s="T44">mʼanʼteǯʼa</ta>
            <ta e="T46" id="Seg_1657" s="T45">mat</ta>
            <ta e="T47" id="Seg_1658" s="T46">naːgur</ta>
            <ta e="T48" id="Seg_1659" s="T47">čeːl</ta>
            <ta e="T49" id="Seg_1660" s="T48">oːnek</ta>
            <ta e="T50" id="Seg_1661" s="T49">tavarš-lɨ-nanto</ta>
            <ta e="T51" id="Seg_1662" s="T50">qaːla-k</ta>
            <ta e="T52" id="Seg_1663" s="T51">mašek</ta>
            <ta e="T53" id="Seg_1664" s="T52">nuːte</ta>
            <ta e="T54" id="Seg_1665" s="T53">lʼečij-e-špɨ-t</ta>
            <ta e="T55" id="Seg_1666" s="T54">naːgur</ta>
            <ta e="T56" id="Seg_1667" s="T55">čeːl</ta>
            <ta e="T57" id="Seg_1668" s="T56">lʼečii-špɨ-t</ta>
            <ta e="T58" id="Seg_1669" s="T57">mašek</ta>
            <ta e="T59" id="Seg_1670" s="T58">nannä</ta>
            <ta e="T60" id="Seg_1671" s="T59">üːdɨ-t</ta>
            <ta e="T61" id="Seg_1672" s="T60">mat</ta>
            <ta e="T62" id="Seg_1673" s="T61">mödə-ča-k</ta>
            <ta e="T63" id="Seg_1674" s="T62">šedə-qoldə-t</ta>
            <ta e="T64" id="Seg_1675" s="T63">tʼeres</ta>
            <ta e="T65" id="Seg_1676" s="T64">pu-špa-k</ta>
            <ta e="T66" id="Seg_1677" s="T65">nannä</ta>
            <ta e="T67" id="Seg_1678" s="T66">üda-t</ta>
            <ta e="T68" id="Seg_1679" s="T67">mašek</ta>
            <ta e="T69" id="Seg_1680" s="T68">pudɨ-k</ta>
            <ta e="T70" id="Seg_1681" s="T69">mat</ta>
            <ta e="T71" id="Seg_1682" s="T70">nʼännä</ta>
            <ta e="T72" id="Seg_1683" s="T71">čaːǯa-k</ta>
            <ta e="T73" id="Seg_1684" s="T72">nannä-ut</ta>
            <ta e="T74" id="Seg_1685" s="T73">mašek</ta>
            <ta e="T75" id="Seg_1686" s="T74">üːda-ut</ta>
            <ta e="T76" id="Seg_1687" s="T75">čwetče-p</ta>
            <ta e="T77" id="Seg_1688" s="T76">nannä-l</ta>
            <ta e="T78" id="Seg_1689" s="T77">mannɨ-mbɨ-gu</ta>
            <ta e="T79" id="Seg_1690" s="T78">mat</ta>
            <ta e="T80" id="Seg_1691" s="T79">nači-t</ta>
            <ta e="T81" id="Seg_1692" s="T80">qwaja-ɣa-k</ta>
            <ta e="T82" id="Seg_1693" s="T81">twesʼse</ta>
            <ta e="T83" id="Seg_1694" s="T82">töː-leblʼe</ta>
            <ta e="T84" id="Seg_1695" s="T83">qaːda-p</ta>
            <ta e="T85" id="Seg_1696" s="T84">kamandir-a-nt</ta>
            <ta e="T86" id="Seg_1697" s="T85">nemʼes</ta>
            <ta e="T87" id="Seg_1698" s="T86">nɨnd</ta>
            <ta e="T88" id="Seg_1699" s="T87">e-ja</ta>
            <ta e="T89" id="Seg_1700" s="T88">qätta-ɣɨt</ta>
            <ta e="T90" id="Seg_1701" s="T89">aːmda-t</ta>
            <ta e="T91" id="Seg_1702" s="T90">tap</ta>
            <ta e="T92" id="Seg_1703" s="T91">meka</ta>
            <ta e="T93" id="Seg_1704" s="T92">qadɨ-t</ta>
            <ta e="T94" id="Seg_1705" s="T93">dawaj</ta>
            <ta e="T95" id="Seg_1706" s="T94">čeːk</ta>
            <ta e="T96" id="Seg_1707" s="T95">čaːǯ-l-ut</ta>
            <ta e="T97" id="Seg_1708" s="T96">mat</ta>
            <ta e="T98" id="Seg_1709" s="T97">üːbɨǯʼa-k</ta>
            <ta e="T99" id="Seg_1710" s="T98">med-r-a-p</ta>
            <ta e="T100" id="Seg_1711" s="T99">i</ta>
            <ta e="T101" id="Seg_1712" s="T100">ček</ta>
            <ta e="T102" id="Seg_1713" s="T101">čaːǯa-j</ta>
            <ta e="T103" id="Seg_1714" s="T102">moqo-ɣot</ta>
            <ta e="T104" id="Seg_1715" s="T103">saldan-la</ta>
            <ta e="T105" id="Seg_1716" s="T104">čaːǯa-t</ta>
            <ta e="T106" id="Seg_1717" s="T105">mi</ta>
            <ta e="T107" id="Seg_1718" s="T106">čagetɨ-mba-ut</ta>
            <ta e="T108" id="Seg_1719" s="T107">mat</ta>
            <ta e="T109" id="Seg_1720" s="T108">taːda-p</ta>
            <ta e="T110" id="Seg_1721" s="T109">kamandir-a-p</ta>
            <ta e="T111" id="Seg_1722" s="T110">qä-t</ta>
            <ta e="T112" id="Seg_1723" s="T111">tom-n</ta>
            <ta e="T113" id="Seg_1724" s="T112">mi</ta>
            <ta e="T114" id="Seg_1725" s="T113">tab-n-opti</ta>
            <ta e="T115" id="Seg_1726" s="T114">kɨge-la-j</ta>
            <ta e="T116" id="Seg_1727" s="T115">nemec-la</ta>
            <ta e="T117" id="Seg_1728" s="T116">mant-če-ǯʼi-gu</ta>
            <ta e="T118" id="Seg_1729" s="T117">kamandir</ta>
            <ta e="T119" id="Seg_1730" s="T118">ɨnnä</ta>
            <ta e="T120" id="Seg_1731" s="T119">waše-ǯʼa</ta>
            <ta e="T121" id="Seg_1732" s="T120">mant-če-ǯʼi-gu</ta>
            <ta e="T122" id="Seg_1733" s="T121">načindo</ta>
            <ta e="T123" id="Seg_1734" s="T122">nʼemec</ta>
            <ta e="T124" id="Seg_1735" s="T123">qo-lča</ta>
            <ta e="T125" id="Seg_1736" s="T124">miši</ta>
            <ta e="T126" id="Seg_1737" s="T125">i</ta>
            <ta e="T127" id="Seg_1738" s="T126">načindo</ta>
            <ta e="T128" id="Seg_1739" s="T127">čaːtča</ta>
            <ta e="T129" id="Seg_1740" s="T128">miši</ta>
            <ta e="T130" id="Seg_1741" s="T129">ranni-ŋ</ta>
            <ta e="T131" id="Seg_1742" s="T130">šinnä</ta>
            <ta e="T132" id="Seg_1743" s="T131">kamandʼir-o-m</ta>
            <ta e="T133" id="Seg_1744" s="T132">aː</ta>
            <ta e="T134" id="Seg_1745" s="T133">qwäja-r-h</ta>
            <ta e="T135" id="Seg_1746" s="T134">nɨnd</ta>
            <ta e="T136" id="Seg_1747" s="T135">mat</ta>
            <ta e="T137" id="Seg_1748" s="T136">udo-n-ä</ta>
            <ta e="T138" id="Seg_1749" s="T137">aːdəlǯa-p</ta>
            <ta e="T139" id="Seg_1750" s="T138">uːdu-n-de</ta>
            <ta e="T140" id="Seg_1751" s="T139">qöːrä-lǯi-t</ta>
            <ta e="T141" id="Seg_1752" s="T140">olo-m-d</ta>
            <ta e="T142" id="Seg_1753" s="T141">ännä</ta>
            <ta e="T143" id="Seg_1754" s="T142">əg</ta>
            <ta e="T144" id="Seg_1755" s="T143">wačo-ǯe-mbeː-t</ta>
            <ta e="T145" id="Seg_1756" s="T144">tab</ta>
            <ta e="T146" id="Seg_1757" s="T145">qoš-tɨ-t</ta>
            <ta e="T147" id="Seg_1758" s="T146">čwesse</ta>
            <ta e="T148" id="Seg_1759" s="T147">park-wa</ta>
            <ta e="T149" id="Seg_1760" s="T148">tab</ta>
            <ta e="T150" id="Seg_1761" s="T149">načindo</ta>
            <ta e="T151" id="Seg_1762" s="T150">tö-špa</ta>
            <ta e="T152" id="Seg_1763" s="T151">feršel</ta>
            <ta e="T153" id="Seg_1764" s="T152">miɣeni</ta>
            <ta e="T154" id="Seg_1765" s="T153">töː-a</ta>
            <ta e="T155" id="Seg_1766" s="T154">medɨ-gu</ta>
            <ta e="T156" id="Seg_1767" s="T155">ne</ta>
            <ta e="T157" id="Seg_1768" s="T156">moʒet</ta>
            <ta e="T158" id="Seg_1769" s="T157">miːɣənni</ta>
            <ta e="T159" id="Seg_1770" s="T158">tabə</ta>
            <ta e="T160" id="Seg_1771" s="T159">abmotka-m</ta>
            <ta e="T161" id="Seg_1772" s="T160">tä</ta>
            <ta e="T162" id="Seg_1773" s="T161">tamdɨ-lʼe-špa-t</ta>
            <ta e="T163" id="Seg_1774" s="T162">tä</ta>
            <ta e="T164" id="Seg_1775" s="T163">tamdä-nnɨ-t</ta>
            <ta e="T165" id="Seg_1776" s="T164">miši</ta>
            <ta e="T166" id="Seg_1777" s="T165">nute</ta>
            <ta e="T167" id="Seg_1778" s="T166">tawaj</ta>
            <ta e="T168" id="Seg_1779" s="T167">ügə-e-špɨ-gu</ta>
            <ta e="T169" id="Seg_1780" s="T168">wot</ta>
            <ta e="T170" id="Seg_1781" s="T169">üːgə-na</ta>
            <ta e="T171" id="Seg_1782" s="T170">abmotka-ɣe</ta>
            <ta e="T172" id="Seg_1783" s="T171">üːgə-na</ta>
            <ta e="T173" id="Seg_1784" s="T172">kamandir-o-m</ta>
            <ta e="T174" id="Seg_1785" s="T173">qwan-na-t</ta>
            <ta e="T175" id="Seg_1786" s="T174">sawsem</ta>
            <ta e="T176" id="Seg_1787" s="T175">mašik</ta>
            <ta e="T177" id="Seg_1788" s="T176">kuːr-e-nna-t</ta>
            <ta e="T178" id="Seg_1789" s="T177">pan-nat-nɨ-t</ta>
            <ta e="T179" id="Seg_1790" s="T178">akop-o-ɣɨnt</ta>
            <ta e="T180" id="Seg_1791" s="T179">muktət</ta>
            <ta e="T181" id="Seg_1792" s="T180">čaːs-t-kundɨ</ta>
            <ta e="T182" id="Seg_1793" s="T181">mat</ta>
            <ta e="T183" id="Seg_1794" s="T182">akop-o-ɣɨn</ta>
            <ta e="T184" id="Seg_1795" s="T183">eːpa-k</ta>
            <ta e="T185" id="Seg_1796" s="T184">mašik</ta>
            <ta e="T186" id="Seg_1797" s="T185">nutə</ta>
            <ta e="T187" id="Seg_1798" s="T186">üːgɨ-n</ta>
            <ta e="T188" id="Seg_1799" s="T187">saninstruktor</ta>
            <ta e="T189" id="Seg_1800" s="T188">mašik</ta>
            <ta e="T190" id="Seg_1801" s="T189">tabə</ta>
            <ta e="T191" id="Seg_1802" s="T190">taːdə-r-ɨ-t</ta>
            <ta e="T192" id="Seg_1803" s="T191">hombla</ta>
            <ta e="T193" id="Seg_1804" s="T192">faːron</ta>
            <ta e="T194" id="Seg_1805" s="T193">ti-n</ta>
            <ta e="T195" id="Seg_1806" s="T194">hombla</ta>
            <ta e="T196" id="Seg_1807" s="T195">faːrot</ta>
            <ta e="T197" id="Seg_1808" s="T196">tiː</ta>
            <ta e="T198" id="Seg_1809" s="T197">hombla-qwäj</ta>
            <ta e="T199" id="Seg_1810" s="T198">tiː</ta>
            <ta e="T200" id="Seg_1811" s="T199">patom</ta>
            <ta e="T201" id="Seg_1812" s="T200">čund-eː</ta>
            <ta e="T202" id="Seg_1813" s="T201">taːdə-r-ɨ-t</ta>
            <ta e="T203" id="Seg_1814" s="T202">mašik</ta>
            <ta e="T204" id="Seg_1815" s="T203">perwa-l</ta>
            <ta e="T205" id="Seg_1816" s="T204">qära-nt</ta>
            <ta e="T206" id="Seg_1817" s="T205">taːdɨ-t</ta>
            <ta e="T207" id="Seg_1818" s="T206">nɨnd</ta>
            <ta e="T208" id="Seg_1819" s="T207">man-ana</ta>
            <ta e="T209" id="Seg_1820" s="T208">tä</ta>
            <ta e="T210" id="Seg_1821" s="T209">meʒal-qɨl-ba-t</ta>
            <ta e="T211" id="Seg_1822" s="T210">askola-la-p</ta>
            <ta e="T212" id="Seg_1823" s="T211">tob-o-ɣɨndo</ta>
            <ta e="T213" id="Seg_1824" s="T212">nute</ta>
            <ta e="T214" id="Seg_1825" s="T213">mašik</ta>
            <ta e="T215" id="Seg_1826" s="T214">tʼwesse</ta>
            <ta e="T216" id="Seg_1827" s="T215">taːdə-r-ɨ-gu</ta>
            <ta e="T217" id="Seg_1828" s="T216">üːbə-r-a-t</ta>
            <ta e="T218" id="Seg_1829" s="T217">mašik</ta>
            <ta e="T219" id="Seg_1830" s="T218">nutə</ta>
            <ta e="T220" id="Seg_1831" s="T219">linije-nd</ta>
            <ta e="T221" id="Seg_1832" s="T220">qwän-dɨ-t</ta>
            <ta e="T222" id="Seg_1833" s="T221">mašina-ɣe</ta>
            <ta e="T223" id="Seg_1834" s="T222">taːdə-r-ɨ-t</ta>
            <ta e="T224" id="Seg_1835" s="T223">mat</ta>
            <ta e="T225" id="Seg_1836" s="T224">eːpa-k</ta>
            <ta e="T226" id="Seg_1837" s="T225">palnisa-ɣät</ta>
            <ta e="T227" id="Seg_1838" s="T226">šədə</ta>
            <ta e="T228" id="Seg_1839" s="T227">po-d</ta>
            <ta e="T229" id="Seg_1840" s="T228">tät</ta>
            <ta e="T230" id="Seg_1841" s="T229">äːre-tɨ</ta>
            <ta e="T231" id="Seg_1842" s="T230">nutə</ta>
            <ta e="T232" id="Seg_1843" s="T231">mašik</ta>
            <ta e="T233" id="Seg_1844" s="T232">leči-ɣi-t</ta>
            <ta e="T234" id="Seg_1845" s="T233">mat</ta>
            <ta e="T235" id="Seg_1846" s="T234">qwaja-ɣa-k</ta>
            <ta e="T236" id="Seg_1847" s="T235">nutə</ta>
            <ta e="T237" id="Seg_1848" s="T236">mašik</ta>
            <ta e="T238" id="Seg_1849" s="T237">čwesse</ta>
            <ta e="T239" id="Seg_1850" s="T238">üːdə-t</ta>
            <ta e="T240" id="Seg_1851" s="T239">mat</ta>
            <ta e="T241" id="Seg_1852" s="T240">moɣonä</ta>
            <ta e="T242" id="Seg_1853" s="T241">töː-ɣa-k</ta>
            <ta e="T243" id="Seg_1854" s="T242">nɨnblʼe</ta>
            <ta e="T244" id="Seg_1855" s="T243">mat</ta>
            <ta e="T245" id="Seg_1856" s="T244">maːt-qɨt</ta>
            <ta e="T246" id="Seg_1857" s="T245">warga-k</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1858" s="T0">maᴣik</ta>
            <ta e="T2" id="Seg_1859" s="T1">soldat-nde</ta>
            <ta e="T3" id="Seg_1860" s="T2">iː-mbɨ-dət</ta>
            <ta e="T4" id="Seg_1861" s="T3">maᴣik</ta>
            <ta e="T5" id="Seg_1862" s="T4">qwän-dɨ-dət</ta>
            <ta e="T6" id="Seg_1863" s="T5">maᴣik</ta>
            <ta e="T7" id="Seg_1864" s="T6">oqol-ɨ-lɨ-špɨ-dət</ta>
            <ta e="T8" id="Seg_1865" s="T7">Aːsena-qɨn</ta>
            <ta e="T9" id="Seg_1866" s="T8">maᴣik</ta>
            <ta e="T10" id="Seg_1867" s="T9">nɨndɨ</ta>
            <ta e="T11" id="Seg_1868" s="T10">nagur</ta>
            <ta e="T12" id="Seg_1869" s="T11">ärä-qɨnt</ta>
            <ta e="T13" id="Seg_1870" s="T12">oqol-ɨ-lʼčǝ-špɨ-dət</ta>
            <ta e="T14" id="Seg_1871" s="T13">nutä</ta>
            <ta e="T15" id="Seg_1872" s="T14">maᴣik</ta>
            <ta e="T16" id="Seg_1873" s="T15">qwän-dɨ-dət</ta>
            <ta e="T17" id="Seg_1874" s="T16">Kʼemerowa-nde</ta>
            <ta e="T18" id="Seg_1875" s="T17">man</ta>
            <ta e="T19" id="Seg_1876" s="T18">nača-tɨ</ta>
            <ta e="T20" id="Seg_1877" s="T19">üdʼa-k</ta>
            <ta e="T21" id="Seg_1878" s="T20">šitə</ta>
            <ta e="T22" id="Seg_1879" s="T21">po-tɨ</ta>
            <ta e="T23" id="Seg_1880" s="T22">maᴣik</ta>
            <ta e="T24" id="Seg_1881" s="T23">nača-nde</ta>
            <ta e="T25" id="Seg_1882" s="T24">iː-ŋɨ-dət</ta>
            <ta e="T26" id="Seg_1883" s="T25">soldat-nde</ta>
            <ta e="T27" id="Seg_1884" s="T26">man</ta>
            <ta e="T28" id="Seg_1885" s="T27">nača-n</ta>
            <ta e="T29" id="Seg_1886" s="T28">e-wa-k</ta>
            <ta e="T30" id="Seg_1887" s="T29">armija-qɨn</ta>
            <ta e="T31" id="Seg_1888" s="T30">muktət</ta>
            <ta e="T32" id="Seg_1889" s="T31">ärä-t</ta>
            <ta e="T33" id="Seg_1890" s="T32">mi</ta>
            <ta e="T34" id="Seg_1891" s="T33">nača-n</ta>
            <ta e="T35" id="Seg_1892" s="T34">wajuj-ču-ut</ta>
            <ta e="T36" id="Seg_1893" s="T35">mödɨ-ču-ut</ta>
            <ta e="T37" id="Seg_1894" s="T36">maᴣik</ta>
            <ta e="T38" id="Seg_1895" s="T37">man</ta>
            <ta e="T39" id="Seg_1896" s="T38">eǯʼər</ta>
            <ta e="T40" id="Seg_1897" s="T39">tuːdɨrɨ-k</ta>
            <ta e="T41" id="Seg_1898" s="T40">tank-ɨ-nde</ta>
            <ta e="T42" id="Seg_1899" s="T41">pet-qɨn</ta>
            <ta e="T43" id="Seg_1900" s="T42">tob-ɨ-wek</ta>
            <ta e="T44" id="Seg_1901" s="T43">tank</ta>
            <ta e="T45" id="Seg_1902" s="T44">mʼanʼdʼeǯʼi</ta>
            <ta e="T46" id="Seg_1903" s="T45">man</ta>
            <ta e="T47" id="Seg_1904" s="T46">nagur</ta>
            <ta e="T48" id="Seg_1905" s="T47">čeːl</ta>
            <ta e="T49" id="Seg_1906" s="T48">onek</ta>
            <ta e="T50" id="Seg_1907" s="T49">tovariš-la-nando</ta>
            <ta e="T51" id="Seg_1908" s="T50">qalɨ-k</ta>
            <ta e="T52" id="Seg_1909" s="T51">maᴣik</ta>
            <ta e="T53" id="Seg_1910" s="T52">nutä</ta>
            <ta e="T54" id="Seg_1911" s="T53">lʼeči-ɨ-špɨ-dət</ta>
            <ta e="T55" id="Seg_1912" s="T54">nagur</ta>
            <ta e="T56" id="Seg_1913" s="T55">čeːl</ta>
            <ta e="T57" id="Seg_1914" s="T56">lʼeči-špɨ-dət</ta>
            <ta e="T58" id="Seg_1915" s="T57">maᴣik</ta>
            <ta e="T59" id="Seg_1916" s="T58">nänne</ta>
            <ta e="T60" id="Seg_1917" s="T59">üdɨ-dət</ta>
            <ta e="T61" id="Seg_1918" s="T60">man</ta>
            <ta e="T62" id="Seg_1919" s="T61">mödɨ-ču-k</ta>
            <ta e="T63" id="Seg_1920" s="T62">šitə-qoldə-n</ta>
            <ta e="T64" id="Seg_1921" s="T63">tʼeres</ta>
            <ta e="T65" id="Seg_1922" s="T64">pu-špɨ-k</ta>
            <ta e="T66" id="Seg_1923" s="T65">nänne</ta>
            <ta e="T67" id="Seg_1924" s="T66">üdɨ-dət</ta>
            <ta e="T68" id="Seg_1925" s="T67">maᴣik</ta>
            <ta e="T69" id="Seg_1926" s="T68">pudɨ-k</ta>
            <ta e="T70" id="Seg_1927" s="T69">man</ta>
            <ta e="T71" id="Seg_1928" s="T70">nänne</ta>
            <ta e="T72" id="Seg_1929" s="T71">čaːǯɨ-k</ta>
            <ta e="T73" id="Seg_1930" s="T72">nänne-ut</ta>
            <ta e="T74" id="Seg_1931" s="T73">maᴣik</ta>
            <ta e="T75" id="Seg_1932" s="T74">üdɨ-dət</ta>
            <ta e="T76" id="Seg_1933" s="T75">čwɛčɛ-p</ta>
            <ta e="T77" id="Seg_1934" s="T76">nänne-lʼ</ta>
            <ta e="T78" id="Seg_1935" s="T77">mantɨ-mbɨ-gu</ta>
            <ta e="T79" id="Seg_1936" s="T78">man</ta>
            <ta e="T80" id="Seg_1937" s="T79">nača-n</ta>
            <ta e="T81" id="Seg_1938" s="T80">qwaja-wa-k</ta>
            <ta e="T82" id="Seg_1939" s="T81">čwesse</ta>
            <ta e="T83" id="Seg_1940" s="T82">töː-lewle</ta>
            <ta e="T84" id="Seg_1941" s="T83">kadɨ-m</ta>
            <ta e="T85" id="Seg_1942" s="T84">kamandir-ɨ-nde</ta>
            <ta e="T86" id="Seg_1943" s="T85">nemʼes</ta>
            <ta e="T87" id="Seg_1944" s="T86">nɨndɨ</ta>
            <ta e="T88" id="Seg_1945" s="T87">e-ŋɨ</ta>
            <ta e="T89" id="Seg_1946" s="T88">qätte-qɨn</ta>
            <ta e="T90" id="Seg_1947" s="T89">omde-dət</ta>
            <ta e="T91" id="Seg_1948" s="T90">tab</ta>
            <ta e="T92" id="Seg_1949" s="T91">mäkkä</ta>
            <ta e="T93" id="Seg_1950" s="T92">kadɨ-tɨ</ta>
            <ta e="T94" id="Seg_1951" s="T93">dawaj</ta>
            <ta e="T95" id="Seg_1952" s="T94">ček</ta>
            <ta e="T96" id="Seg_1953" s="T95">čaːǯɨ-le-ut</ta>
            <ta e="T97" id="Seg_1954" s="T96">man</ta>
            <ta e="T98" id="Seg_1955" s="T97">üːbɨǯe-k</ta>
            <ta e="T99" id="Seg_1956" s="T98">medɨ-r-ŋɨ-m</ta>
            <ta e="T100" id="Seg_1957" s="T99">i</ta>
            <ta e="T101" id="Seg_1958" s="T100">ček</ta>
            <ta e="T102" id="Seg_1959" s="T101">čaːǯɨ-j</ta>
            <ta e="T103" id="Seg_1960" s="T102">moqo-qɨt</ta>
            <ta e="T104" id="Seg_1961" s="T103">soldat-la</ta>
            <ta e="T105" id="Seg_1962" s="T104">čaːǯɨ-dət</ta>
            <ta e="T106" id="Seg_1963" s="T105">mi</ta>
            <ta e="T107" id="Seg_1964" s="T106">čageːtɨ-mbɨ-ut</ta>
            <ta e="T108" id="Seg_1965" s="T107">man</ta>
            <ta e="T109" id="Seg_1966" s="T108">tade-m</ta>
            <ta e="T110" id="Seg_1967" s="T109">kamandir-ɨ-p</ta>
            <ta e="T111" id="Seg_1968" s="T110">kɨ-n</ta>
            <ta e="T112" id="Seg_1969" s="T111">tom-nde</ta>
            <ta e="T113" id="Seg_1970" s="T112">mi</ta>
            <ta e="T114" id="Seg_1971" s="T113">tab-n-optɨ</ta>
            <ta e="T115" id="Seg_1972" s="T114">kɨge-lə-j</ta>
            <ta e="T116" id="Seg_1973" s="T115">nemʼes-la</ta>
            <ta e="T117" id="Seg_1974" s="T116">mantɨ-če-lʼčǝ-gu</ta>
            <ta e="T118" id="Seg_1975" s="T117">kamandir</ta>
            <ta e="T119" id="Seg_1976" s="T118">inne</ta>
            <ta e="T120" id="Seg_1977" s="T119">waše-lʼčǝ</ta>
            <ta e="T121" id="Seg_1978" s="T120">mantɨ-če-lʼčǝ-gu</ta>
            <ta e="T122" id="Seg_1979" s="T121">načindo</ta>
            <ta e="T123" id="Seg_1980" s="T122">nemʼes</ta>
            <ta e="T124" id="Seg_1981" s="T123">qo-lʼčǝ</ta>
            <ta e="T125" id="Seg_1982" s="T124">miǯnɨt</ta>
            <ta e="T126" id="Seg_1983" s="T125">i</ta>
            <ta e="T127" id="Seg_1984" s="T126">načindo</ta>
            <ta e="T128" id="Seg_1985" s="T127">čačɨ</ta>
            <ta e="T129" id="Seg_1986" s="T128">miǯnɨt</ta>
            <ta e="T130" id="Seg_1987" s="T129">ranni-k</ta>
            <ta e="T131" id="Seg_1988" s="T130">miǯnɨt</ta>
            <ta e="T132" id="Seg_1989" s="T131">kamandir-ɨ-mɨ</ta>
            <ta e="T133" id="Seg_1990" s="T132">aː</ta>
            <ta e="T134" id="Seg_1991" s="T133">kwɛjе-r-k</ta>
            <ta e="T135" id="Seg_1992" s="T134">nɨndɨ</ta>
            <ta e="T136" id="Seg_1993" s="T135">man</ta>
            <ta e="T137" id="Seg_1994" s="T136">udɨ-nɨ-se</ta>
            <ta e="T138" id="Seg_1995" s="T137">aːdəlǯɨ-m</ta>
            <ta e="T139" id="Seg_1996" s="T138">udɨ-n-tɨ</ta>
            <ta e="T140" id="Seg_1997" s="T139">korɨ-lʼčǝ-tɨ</ta>
            <ta e="T141" id="Seg_1998" s="T140">olo-m-tɨ</ta>
            <ta e="T142" id="Seg_1999" s="T141">inne</ta>
            <ta e="T143" id="Seg_2000" s="T142">ɨgɨ</ta>
            <ta e="T144" id="Seg_2001" s="T143">wače-nǯe-mbɨ-ätɨ</ta>
            <ta e="T145" id="Seg_2002" s="T144">tab</ta>
            <ta e="T146" id="Seg_2003" s="T145">qoš-ntɨ-tɨ</ta>
            <ta e="T147" id="Seg_2004" s="T146">čwesse</ta>
            <ta e="T148" id="Seg_2005" s="T147">parka-wa</ta>
            <ta e="T149" id="Seg_2006" s="T148">tab</ta>
            <ta e="T150" id="Seg_2007" s="T149">načindo</ta>
            <ta e="T151" id="Seg_2008" s="T150">töː-špɨ</ta>
            <ta e="T152" id="Seg_2009" s="T151">feršel</ta>
            <ta e="T153" id="Seg_2010" s="T152">miːɣənni</ta>
            <ta e="T154" id="Seg_2011" s="T153">töː-ŋɨ</ta>
            <ta e="T155" id="Seg_2012" s="T154">medɨ-gu</ta>
            <ta e="T156" id="Seg_2013" s="T155">ne</ta>
            <ta e="T157" id="Seg_2014" s="T156">moʒət</ta>
            <ta e="T158" id="Seg_2015" s="T157">miːɣənni</ta>
            <ta e="T159" id="Seg_2016" s="T158">tab</ta>
            <ta e="T160" id="Seg_2017" s="T159">abmotka-m</ta>
            <ta e="T161" id="Seg_2018" s="T160">teː</ta>
            <ta e="T162" id="Seg_2019" s="T161">tamdɨ-lə-špɨ-tɨ</ta>
            <ta e="T163" id="Seg_2020" s="T162">teː</ta>
            <ta e="T164" id="Seg_2021" s="T163">tamdɨ-ŋɨ-tɨ</ta>
            <ta e="T165" id="Seg_2022" s="T164">miǯnɨt</ta>
            <ta e="T166" id="Seg_2023" s="T165">nutä</ta>
            <ta e="T167" id="Seg_2024" s="T166">dawaj</ta>
            <ta e="T168" id="Seg_2025" s="T167">ügə-ɨ-špɨ-gu</ta>
            <ta e="T169" id="Seg_2026" s="T168">wot</ta>
            <ta e="T170" id="Seg_2027" s="T169">ügə-ŋɨ</ta>
            <ta e="T171" id="Seg_2028" s="T170">abmotka-se</ta>
            <ta e="T172" id="Seg_2029" s="T171">ügə-ŋɨ</ta>
            <ta e="T173" id="Seg_2030" s="T172">kamandir-ɨ-m</ta>
            <ta e="T174" id="Seg_2031" s="T173">kwat-ŋɨ-dət</ta>
            <ta e="T175" id="Seg_2032" s="T174">sawsem</ta>
            <ta e="T176" id="Seg_2033" s="T175">maᴣik</ta>
            <ta e="T177" id="Seg_2034" s="T176">qur-ɨ-ŋɨ-dət</ta>
            <ta e="T178" id="Seg_2035" s="T177">pan-mat-ŋɨ-dət</ta>
            <ta e="T179" id="Seg_2036" s="T178">akop-ɨ-qɨnt</ta>
            <ta e="T180" id="Seg_2037" s="T179">muktət</ta>
            <ta e="T181" id="Seg_2038" s="T180">čaːs-n-kundɨ</ta>
            <ta e="T182" id="Seg_2039" s="T181">man</ta>
            <ta e="T183" id="Seg_2040" s="T182">akop-ɨ-qɨn</ta>
            <ta e="T184" id="Seg_2041" s="T183">eppɨ-k</ta>
            <ta e="T185" id="Seg_2042" s="T184">maᴣik</ta>
            <ta e="T186" id="Seg_2043" s="T185">nutä</ta>
            <ta e="T187" id="Seg_2044" s="T186">ügə-n</ta>
            <ta e="T188" id="Seg_2045" s="T187">saninstruktor</ta>
            <ta e="T189" id="Seg_2046" s="T188">maᴣik</ta>
            <ta e="T190" id="Seg_2047" s="T189">tab</ta>
            <ta e="T191" id="Seg_2048" s="T190">tade-r-ɨ-tɨ</ta>
            <ta e="T192" id="Seg_2049" s="T191">sombla</ta>
            <ta e="T193" id="Seg_2050" s="T192">sarum</ta>
            <ta e="T194" id="Seg_2051" s="T193">tiː-n</ta>
            <ta e="T195" id="Seg_2052" s="T194">sombla</ta>
            <ta e="T196" id="Seg_2053" s="T195">sarum</ta>
            <ta e="T197" id="Seg_2054" s="T196">tiː</ta>
            <ta e="T198" id="Seg_2055" s="T197">sombla-qwäj</ta>
            <ta e="T199" id="Seg_2056" s="T198">tiː</ta>
            <ta e="T200" id="Seg_2057" s="T199">patom</ta>
            <ta e="T201" id="Seg_2058" s="T200">čünd-se</ta>
            <ta e="T202" id="Seg_2059" s="T201">tade-r-ɨ-dət</ta>
            <ta e="T203" id="Seg_2060" s="T202">maᴣik</ta>
            <ta e="T204" id="Seg_2061" s="T203">perwa-lʼ</ta>
            <ta e="T205" id="Seg_2062" s="T204">qara-nde</ta>
            <ta e="T206" id="Seg_2063" s="T205">tade-dət</ta>
            <ta e="T207" id="Seg_2064" s="T206">nɨndɨ</ta>
            <ta e="T208" id="Seg_2065" s="T207">man-nan</ta>
            <ta e="T209" id="Seg_2066" s="T208">teː</ta>
            <ta e="T210" id="Seg_2067" s="T209">mešal-qəl-mbɨ-dət</ta>
            <ta e="T211" id="Seg_2068" s="T210">askola-la-p</ta>
            <ta e="T212" id="Seg_2069" s="T211">tob-ɨ-ɣɨndo</ta>
            <ta e="T213" id="Seg_2070" s="T212">nutä</ta>
            <ta e="T214" id="Seg_2071" s="T213">maᴣik</ta>
            <ta e="T215" id="Seg_2072" s="T214">čwesse</ta>
            <ta e="T216" id="Seg_2073" s="T215">tade-r-ɨ-gu</ta>
            <ta e="T217" id="Seg_2074" s="T216">übə-r-ɨ-dət</ta>
            <ta e="T218" id="Seg_2075" s="T217">maᴣik</ta>
            <ta e="T219" id="Seg_2076" s="T218">nutä</ta>
            <ta e="T220" id="Seg_2077" s="T219">linije-nde</ta>
            <ta e="T221" id="Seg_2078" s="T220">qwän-dɨ-dət</ta>
            <ta e="T222" id="Seg_2079" s="T221">mašina-se</ta>
            <ta e="T223" id="Seg_2080" s="T222">tade-r-ɨ-dət</ta>
            <ta e="T224" id="Seg_2081" s="T223">man</ta>
            <ta e="T225" id="Seg_2082" s="T224">eppɨ-k</ta>
            <ta e="T226" id="Seg_2083" s="T225">palnisa-qɨn</ta>
            <ta e="T227" id="Seg_2084" s="T226">šitə</ta>
            <ta e="T228" id="Seg_2085" s="T227">po-tɨ</ta>
            <ta e="T229" id="Seg_2086" s="T228">tettɨ</ta>
            <ta e="T230" id="Seg_2087" s="T229">ärä-tɨ</ta>
            <ta e="T231" id="Seg_2088" s="T230">nutä</ta>
            <ta e="T232" id="Seg_2089" s="T231">maᴣik</ta>
            <ta e="T233" id="Seg_2090" s="T232">lʼeči-ŋɨ-dət</ta>
            <ta e="T234" id="Seg_2091" s="T233">man</ta>
            <ta e="T235" id="Seg_2092" s="T234">qwaja-wa-k</ta>
            <ta e="T236" id="Seg_2093" s="T235">nutä</ta>
            <ta e="T237" id="Seg_2094" s="T236">maᴣik</ta>
            <ta e="T238" id="Seg_2095" s="T237">čwesse</ta>
            <ta e="T239" id="Seg_2096" s="T238">üdɨ-dət</ta>
            <ta e="T240" id="Seg_2097" s="T239">man</ta>
            <ta e="T241" id="Seg_2098" s="T240">moqne</ta>
            <ta e="T242" id="Seg_2099" s="T241">töː-wa-k</ta>
            <ta e="T243" id="Seg_2100" s="T242">nɨnblʼe</ta>
            <ta e="T244" id="Seg_2101" s="T243">man</ta>
            <ta e="T245" id="Seg_2102" s="T244">maːt-qɨt</ta>
            <ta e="T246" id="Seg_2103" s="T245">wargɨ-k</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2104" s="T0">I.ACC</ta>
            <ta e="T2" id="Seg_2105" s="T1">soldier-ILL</ta>
            <ta e="T3" id="Seg_2106" s="T2">take-PST.NAR-3PL</ta>
            <ta e="T4" id="Seg_2107" s="T3">I.ACC</ta>
            <ta e="T5" id="Seg_2108" s="T4">go.away-TR-3PL</ta>
            <ta e="T6" id="Seg_2109" s="T5">I.ACC</ta>
            <ta e="T7" id="Seg_2110" s="T6">learn-EP-RES-IPFV2-3PL</ta>
            <ta e="T8" id="Seg_2111" s="T7">Asino-LOC</ta>
            <ta e="T9" id="Seg_2112" s="T8">I.ACC</ta>
            <ta e="T10" id="Seg_2113" s="T9">here</ta>
            <ta e="T11" id="Seg_2114" s="T10">three</ta>
            <ta e="T12" id="Seg_2115" s="T11">month-LOC.3SG</ta>
            <ta e="T13" id="Seg_2116" s="T12">learn-EP-PFV-IPFV2-3PL</ta>
            <ta e="T14" id="Seg_2117" s="T13">from.there</ta>
            <ta e="T15" id="Seg_2118" s="T14">I.ACC</ta>
            <ta e="T16" id="Seg_2119" s="T15">go.away-TR-3PL</ta>
            <ta e="T17" id="Seg_2120" s="T16">Kemerovo-ILL</ta>
            <ta e="T18" id="Seg_2121" s="T17">I.NOM</ta>
            <ta e="T19" id="Seg_2122" s="T18">there-ADV.LOC</ta>
            <ta e="T20" id="Seg_2123" s="T19">work-1SG.S</ta>
            <ta e="T21" id="Seg_2124" s="T20">two</ta>
            <ta e="T22" id="Seg_2125" s="T21">year.[NOM]-3SG</ta>
            <ta e="T23" id="Seg_2126" s="T22">I.ACC</ta>
            <ta e="T24" id="Seg_2127" s="T23">there-ILL</ta>
            <ta e="T25" id="Seg_2128" s="T24">take-CO-3PL</ta>
            <ta e="T26" id="Seg_2129" s="T25">soldier-ILL</ta>
            <ta e="T27" id="Seg_2130" s="T26">I.NOM</ta>
            <ta e="T28" id="Seg_2131" s="T27">there-ADV.LOC</ta>
            <ta e="T29" id="Seg_2132" s="T28">be-CO-1SG.S</ta>
            <ta e="T30" id="Seg_2133" s="T29">army-LOC</ta>
            <ta e="T31" id="Seg_2134" s="T30">six</ta>
            <ta e="T32" id="Seg_2135" s="T31">month-PL.[NOM]</ta>
            <ta e="T33" id="Seg_2136" s="T32">we.PL.NOM</ta>
            <ta e="T34" id="Seg_2137" s="T33">there-ADV.LOC</ta>
            <ta e="T35" id="Seg_2138" s="T34">make.war-VBLZ-1PL</ta>
            <ta e="T36" id="Seg_2139" s="T35">war-TR-1PL</ta>
            <ta e="T37" id="Seg_2140" s="T36">I.ACC</ta>
            <ta e="T38" id="Seg_2141" s="T37">I.NOM</ta>
            <ta e="T39" id="Seg_2142" s="T38">first.time</ta>
            <ta e="T40" id="Seg_2143" s="T39">get-1SG.S</ta>
            <ta e="T41" id="Seg_2144" s="T40">panzer-EP-ILL</ta>
            <ta e="T42" id="Seg_2145" s="T41">night-LOC</ta>
            <ta e="T43" id="Seg_2146" s="T42">leg-EP-PROL.1SG</ta>
            <ta e="T44" id="Seg_2147" s="T43">panzer.[NOM]</ta>
            <ta e="T45" id="Seg_2148" s="T44">pass.[3SG.S]</ta>
            <ta e="T46" id="Seg_2149" s="T45">I.NOM</ta>
            <ta e="T47" id="Seg_2150" s="T46">three</ta>
            <ta e="T48" id="Seg_2151" s="T47">day.[NOM]</ta>
            <ta e="T49" id="Seg_2152" s="T48">oneself.1SG</ta>
            <ta e="T50" id="Seg_2153" s="T49">companion-PL-ABL</ta>
            <ta e="T51" id="Seg_2154" s="T50">stay-1SG.S</ta>
            <ta e="T52" id="Seg_2155" s="T51">I.ACC</ta>
            <ta e="T53" id="Seg_2156" s="T52">from.there</ta>
            <ta e="T54" id="Seg_2157" s="T53">cure-EP-IPFV2-3PL</ta>
            <ta e="T55" id="Seg_2158" s="T54">three</ta>
            <ta e="T56" id="Seg_2159" s="T55">day.[NOM]</ta>
            <ta e="T57" id="Seg_2160" s="T56">cure-IPFV2-3PL</ta>
            <ta e="T58" id="Seg_2161" s="T57">I.ACC</ta>
            <ta e="T59" id="Seg_2162" s="T58">forward</ta>
            <ta e="T60" id="Seg_2163" s="T59">send-3PL</ta>
            <ta e="T61" id="Seg_2164" s="T60">I.NOM</ta>
            <ta e="T62" id="Seg_2165" s="T61">war-TR-1SG.S</ta>
            <ta e="T63" id="Seg_2166" s="T62">two-big.river-GEN</ta>
            <ta e="T64" id="Seg_2167" s="T63">through</ta>
            <ta e="T65" id="Seg_2168" s="T64">%%-IPFV2-1SG.S</ta>
            <ta e="T66" id="Seg_2169" s="T65">forward</ta>
            <ta e="T67" id="Seg_2170" s="T66">send-3PL</ta>
            <ta e="T68" id="Seg_2171" s="T67">I.ACC</ta>
            <ta e="T69" id="Seg_2172" s="T68">put.over-1SG.S</ta>
            <ta e="T70" id="Seg_2173" s="T69">I.NOM</ta>
            <ta e="T71" id="Seg_2174" s="T70">forward</ta>
            <ta e="T72" id="Seg_2175" s="T71">go-1SG.S</ta>
            <ta e="T73" id="Seg_2176" s="T72">forward-1PL</ta>
            <ta e="T74" id="Seg_2177" s="T73">I.ACC</ta>
            <ta e="T75" id="Seg_2178" s="T74">send-3PL</ta>
            <ta e="T76" id="Seg_2179" s="T75">place-ACC</ta>
            <ta e="T77" id="Seg_2180" s="T76">forward-ADJZ</ta>
            <ta e="T78" id="Seg_2181" s="T77">look-DUR-INF</ta>
            <ta e="T79" id="Seg_2182" s="T78">I.NOM</ta>
            <ta e="T80" id="Seg_2183" s="T79">there-ADV.LOC</ta>
            <ta e="T81" id="Seg_2184" s="T80">go-CO-1SG.S</ta>
            <ta e="T82" id="Seg_2185" s="T81">backward</ta>
            <ta e="T83" id="Seg_2186" s="T82">come-CVB2</ta>
            <ta e="T84" id="Seg_2187" s="T83">say-1SG.O</ta>
            <ta e="T85" id="Seg_2188" s="T84">commander-EP-ILL</ta>
            <ta e="T86" id="Seg_2189" s="T85">German.[NOM]</ta>
            <ta e="T87" id="Seg_2190" s="T86">here</ta>
            <ta e="T88" id="Seg_2191" s="T87">be-CO.[3SG.S]</ta>
            <ta e="T89" id="Seg_2192" s="T88">river-LOC</ta>
            <ta e="T90" id="Seg_2193" s="T89">sit-3PL</ta>
            <ta e="T91" id="Seg_2194" s="T90">(s)he.[NOM]</ta>
            <ta e="T92" id="Seg_2195" s="T91">I.ALL</ta>
            <ta e="T93" id="Seg_2196" s="T92">say-3SG.O</ta>
            <ta e="T94" id="Seg_2197" s="T93">HORT</ta>
            <ta e="T95" id="Seg_2198" s="T94">fast</ta>
            <ta e="T96" id="Seg_2199" s="T95">run-OPT-1PL</ta>
            <ta e="T97" id="Seg_2200" s="T96">I.NOM</ta>
            <ta e="T98" id="Seg_2201" s="T97">depart-1SG.S</ta>
            <ta e="T99" id="Seg_2202" s="T98">reach-FRQ-CO-1SG.O</ta>
            <ta e="T100" id="Seg_2203" s="T99">and</ta>
            <ta e="T101" id="Seg_2204" s="T100">fast</ta>
            <ta e="T102" id="Seg_2205" s="T101">run-1DU</ta>
            <ta e="T103" id="Seg_2206" s="T102">back-LOC</ta>
            <ta e="T104" id="Seg_2207" s="T103">soldier-PL.[NOM]</ta>
            <ta e="T105" id="Seg_2208" s="T104">go-3PL</ta>
            <ta e="T106" id="Seg_2209" s="T105">we.PL.NOM</ta>
            <ta e="T107" id="Seg_2210" s="T106">hurry-DUR-1PL</ta>
            <ta e="T108" id="Seg_2211" s="T107">I.NOM</ta>
            <ta e="T109" id="Seg_2212" s="T108">bring-1SG.O</ta>
            <ta e="T110" id="Seg_2213" s="T109">commander-EP-ACC</ta>
            <ta e="T111" id="Seg_2214" s="T110">river-GEN</ta>
            <ta e="T112" id="Seg_2215" s="T111">Tom-ILL</ta>
            <ta e="T113" id="Seg_2216" s="T112">we.DU.NOM</ta>
            <ta e="T114" id="Seg_2217" s="T113">(s)he-GEN-with</ta>
            <ta e="T115" id="Seg_2218" s="T114">want-INCH-1DU</ta>
            <ta e="T116" id="Seg_2219" s="T115">German-PL.[NOM]</ta>
            <ta e="T117" id="Seg_2220" s="T116">look-DRV-PFV-INF</ta>
            <ta e="T118" id="Seg_2221" s="T117">commander.[NOM]</ta>
            <ta e="T119" id="Seg_2222" s="T118">up</ta>
            <ta e="T120" id="Seg_2223" s="T119">get.up-PFV.[3SG.S]</ta>
            <ta e="T121" id="Seg_2224" s="T120">look-DRV-PFV-INF</ta>
            <ta e="T122" id="Seg_2225" s="T121">from.there</ta>
            <ta e="T123" id="Seg_2226" s="T122">German.[NOM]</ta>
            <ta e="T124" id="Seg_2227" s="T123">see-PFV.[3SG.S]</ta>
            <ta e="T125" id="Seg_2228" s="T124">we.DU.ACC</ta>
            <ta e="T126" id="Seg_2229" s="T125">and</ta>
            <ta e="T127" id="Seg_2230" s="T126">from.there</ta>
            <ta e="T128" id="Seg_2231" s="T127">shoot.[3SG.S]</ta>
            <ta e="T129" id="Seg_2232" s="T128">we.DU.ACC</ta>
            <ta e="T130" id="Seg_2233" s="T129">hurt-3SG.S</ta>
            <ta e="T131" id="Seg_2234" s="T130">we.DU.ACC</ta>
            <ta e="T132" id="Seg_2235" s="T131">commander-EP-1SG</ta>
            <ta e="T133" id="Seg_2236" s="T132">NEG</ta>
            <ta e="T134" id="Seg_2237" s="T133">breath-FRQ-3SG.S</ta>
            <ta e="T135" id="Seg_2238" s="T134">here</ta>
            <ta e="T136" id="Seg_2239" s="T135">I.NOM</ta>
            <ta e="T137" id="Seg_2240" s="T136">hand-GEN.1SG-INSTR</ta>
            <ta e="T138" id="Seg_2241" s="T137">show-1SG.O</ta>
            <ta e="T139" id="Seg_2242" s="T138">hand-INSTR2-3SG</ta>
            <ta e="T140" id="Seg_2243" s="T139">swing-PFV-3SG.O</ta>
            <ta e="T141" id="Seg_2244" s="T140">head-ACC-3SG</ta>
            <ta e="T142" id="Seg_2245" s="T141">up</ta>
            <ta e="T143" id="Seg_2246" s="T142">NEG.IMP</ta>
            <ta e="T144" id="Seg_2247" s="T143">lift.up-IPFV3-DUR-IMP.2SG.O</ta>
            <ta e="T145" id="Seg_2248" s="T144">(s)he.[NOM]</ta>
            <ta e="T146" id="Seg_2249" s="T145">discover-IPFV-3SG.O</ta>
            <ta e="T147" id="Seg_2250" s="T146">backward</ta>
            <ta e="T148" id="Seg_2251" s="T147">shout-CO.[3SG.S]</ta>
            <ta e="T149" id="Seg_2252" s="T148">(s)he.[NOM]</ta>
            <ta e="T150" id="Seg_2253" s="T149">from.there</ta>
            <ta e="T151" id="Seg_2254" s="T150">come-IPFV2.[3SG.S]</ta>
            <ta e="T152" id="Seg_2255" s="T151">medical.assistant.[NOM]</ta>
            <ta e="T153" id="Seg_2256" s="T152">we.DU.ALL</ta>
            <ta e="T154" id="Seg_2257" s="T153">come-CO.[3SG.S]</ta>
            <ta e="T155" id="Seg_2258" s="T154">reach-INF</ta>
            <ta e="T156" id="Seg_2259" s="T155">NEG</ta>
            <ta e="T157" id="Seg_2260" s="T156">maybe</ta>
            <ta e="T158" id="Seg_2261" s="T157">we.DU.ALL</ta>
            <ta e="T159" id="Seg_2262" s="T158">(s)he.[NOM]</ta>
            <ta e="T160" id="Seg_2263" s="T159">winding-ACC</ta>
            <ta e="T161" id="Seg_2264" s="T160">away</ta>
            <ta e="T162" id="Seg_2265" s="T161">wrap-INCH-IPFV2-3SG.O</ta>
            <ta e="T163" id="Seg_2266" s="T162">away</ta>
            <ta e="T164" id="Seg_2267" s="T163">wrap-CO-3SG.O</ta>
            <ta e="T165" id="Seg_2268" s="T164">we.DU.ACC</ta>
            <ta e="T166" id="Seg_2269" s="T165">from.there</ta>
            <ta e="T167" id="Seg_2270" s="T166">INCH</ta>
            <ta e="T168" id="Seg_2271" s="T167">pull.away-EP-IPFV2-INF</ta>
            <ta e="T169" id="Seg_2272" s="T168">look.here</ta>
            <ta e="T170" id="Seg_2273" s="T169">pull.away-CO.[3SG.S]</ta>
            <ta e="T171" id="Seg_2274" s="T170">winding-INSTR</ta>
            <ta e="T172" id="Seg_2275" s="T171">pull.away-CO.[3SG.S]</ta>
            <ta e="T173" id="Seg_2276" s="T172">commander-EP-ACC</ta>
            <ta e="T174" id="Seg_2277" s="T173">kill-CO-3PL</ta>
            <ta e="T175" id="Seg_2278" s="T174">completely</ta>
            <ta e="T176" id="Seg_2279" s="T175">I.ACC</ta>
            <ta e="T177" id="Seg_2280" s="T176">tie-EP-CO-3PL</ta>
            <ta e="T178" id="Seg_2281" s="T177">put-DRV-CO-3PL</ta>
            <ta e="T179" id="Seg_2282" s="T178">trench-EP-ILL.3SG</ta>
            <ta e="T180" id="Seg_2283" s="T179">six</ta>
            <ta e="T181" id="Seg_2284" s="T180">hour-GEN-during</ta>
            <ta e="T182" id="Seg_2285" s="T181">I.NOM</ta>
            <ta e="T183" id="Seg_2286" s="T182">trench-EP-LOC</ta>
            <ta e="T184" id="Seg_2287" s="T183">lie-1SG.S</ta>
            <ta e="T185" id="Seg_2288" s="T184">I.ACC</ta>
            <ta e="T186" id="Seg_2289" s="T185">from.there</ta>
            <ta e="T187" id="Seg_2290" s="T186">pull.away-3SG.S</ta>
            <ta e="T188" id="Seg_2291" s="T187">medical.instructor.[NOM]</ta>
            <ta e="T189" id="Seg_2292" s="T188">I.ACC</ta>
            <ta e="T190" id="Seg_2293" s="T189">(s)he.[NOM]</ta>
            <ta e="T191" id="Seg_2294" s="T190">bring-FRQ-EP-3SG.O</ta>
            <ta e="T192" id="Seg_2295" s="T191">five</ta>
            <ta e="T193" id="Seg_2296" s="T192">ten</ta>
            <ta e="T194" id="Seg_2297" s="T193">sazhen-ADV.LOC</ta>
            <ta e="T195" id="Seg_2298" s="T194">five</ta>
            <ta e="T196" id="Seg_2299" s="T195">ten</ta>
            <ta e="T197" id="Seg_2300" s="T196">sazhen.[NOM]</ta>
            <ta e="T198" id="Seg_2301" s="T197">five-superfluous</ta>
            <ta e="T199" id="Seg_2302" s="T198">sazhen.[NOM]</ta>
            <ta e="T200" id="Seg_2303" s="T199">then</ta>
            <ta e="T201" id="Seg_2304" s="T200">horse-INSTR</ta>
            <ta e="T202" id="Seg_2305" s="T201">bring-FRQ-EP-3PL</ta>
            <ta e="T203" id="Seg_2306" s="T202">I.ACC</ta>
            <ta e="T204" id="Seg_2307" s="T203">first-ADJZ</ta>
            <ta e="T205" id="Seg_2308" s="T204">village-ILL</ta>
            <ta e="T206" id="Seg_2309" s="T205">bring-3PL</ta>
            <ta e="T207" id="Seg_2310" s="T206">here</ta>
            <ta e="T208" id="Seg_2311" s="T207">I.NOM-ADES</ta>
            <ta e="T209" id="Seg_2312" s="T208">away</ta>
            <ta e="T210" id="Seg_2313" s="T209">pull-MULS-PST.NAR-3PL</ta>
            <ta e="T211" id="Seg_2314" s="T210">splinter-PL-ACC</ta>
            <ta e="T212" id="Seg_2315" s="T211">leg-EP-EL.3SG</ta>
            <ta e="T213" id="Seg_2316" s="T212">from.there</ta>
            <ta e="T214" id="Seg_2317" s="T213">I.ACC</ta>
            <ta e="T215" id="Seg_2318" s="T214">backward</ta>
            <ta e="T216" id="Seg_2319" s="T215">bring-FRQ-EP-INF</ta>
            <ta e="T217" id="Seg_2320" s="T216">begin-FRQ-EP-3PL</ta>
            <ta e="T218" id="Seg_2321" s="T217">I.ACC</ta>
            <ta e="T219" id="Seg_2322" s="T218">from.there</ta>
            <ta e="T220" id="Seg_2323" s="T219">line-ILL</ta>
            <ta e="T221" id="Seg_2324" s="T220">go.away-TR-3PL</ta>
            <ta e="T222" id="Seg_2325" s="T221">car-INSTR</ta>
            <ta e="T223" id="Seg_2326" s="T222">bring-FRQ-EP-3PL</ta>
            <ta e="T224" id="Seg_2327" s="T223">I.NOM</ta>
            <ta e="T225" id="Seg_2328" s="T224">lie-1SG.S</ta>
            <ta e="T226" id="Seg_2329" s="T225">hospital-LOC</ta>
            <ta e="T227" id="Seg_2330" s="T226">two</ta>
            <ta e="T228" id="Seg_2331" s="T227">year.[NOM]-3SG</ta>
            <ta e="T229" id="Seg_2332" s="T228">four</ta>
            <ta e="T230" id="Seg_2333" s="T229">month.[NOM]-3SG</ta>
            <ta e="T231" id="Seg_2334" s="T230">from.there</ta>
            <ta e="T232" id="Seg_2335" s="T231">I.ACC</ta>
            <ta e="T233" id="Seg_2336" s="T232">cure-CO-3PL</ta>
            <ta e="T234" id="Seg_2337" s="T233">I.NOM</ta>
            <ta e="T235" id="Seg_2338" s="T234">go-CO-1SG.S</ta>
            <ta e="T236" id="Seg_2339" s="T235">from.there</ta>
            <ta e="T237" id="Seg_2340" s="T236">I.ACC</ta>
            <ta e="T238" id="Seg_2341" s="T237">backward</ta>
            <ta e="T239" id="Seg_2342" s="T238">send-3PL</ta>
            <ta e="T240" id="Seg_2343" s="T239">I.NOM</ta>
            <ta e="T241" id="Seg_2344" s="T240">home</ta>
            <ta e="T242" id="Seg_2345" s="T241">come-CO-1SG.S</ta>
            <ta e="T243" id="Seg_2346" s="T242">there.after</ta>
            <ta e="T244" id="Seg_2347" s="T243">I.NOM</ta>
            <ta e="T245" id="Seg_2348" s="T244">house-LOC</ta>
            <ta e="T246" id="Seg_2349" s="T245">live-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2350" s="T0">я.ACC</ta>
            <ta e="T2" id="Seg_2351" s="T1">солдат-ILL</ta>
            <ta e="T3" id="Seg_2352" s="T2">взять-PST.NAR-3PL</ta>
            <ta e="T4" id="Seg_2353" s="T3">я.ACC</ta>
            <ta e="T5" id="Seg_2354" s="T4">пойти-TR-3PL</ta>
            <ta e="T6" id="Seg_2355" s="T5">я.ACC</ta>
            <ta e="T7" id="Seg_2356" s="T6">обучить-EP-RES-IPFV2-3PL</ta>
            <ta e="T8" id="Seg_2357" s="T7">Асино-LOC</ta>
            <ta e="T9" id="Seg_2358" s="T8">я.ACC</ta>
            <ta e="T10" id="Seg_2359" s="T9">здесь</ta>
            <ta e="T11" id="Seg_2360" s="T10">три</ta>
            <ta e="T12" id="Seg_2361" s="T11">месяц-LOC.3SG</ta>
            <ta e="T13" id="Seg_2362" s="T12">обучить-EP-PFV-IPFV2-3PL</ta>
            <ta e="T14" id="Seg_2363" s="T13">оттуда</ta>
            <ta e="T15" id="Seg_2364" s="T14">я.ACC</ta>
            <ta e="T16" id="Seg_2365" s="T15">пойти-TR-3PL</ta>
            <ta e="T17" id="Seg_2366" s="T16">Кемерово-ILL</ta>
            <ta e="T18" id="Seg_2367" s="T17">я.NOM</ta>
            <ta e="T19" id="Seg_2368" s="T18">туда-ADV.LOC</ta>
            <ta e="T20" id="Seg_2369" s="T19">работать-1SG.S</ta>
            <ta e="T21" id="Seg_2370" s="T20">два</ta>
            <ta e="T22" id="Seg_2371" s="T21">год.[NOM]-3SG</ta>
            <ta e="T23" id="Seg_2372" s="T22">я.ACC</ta>
            <ta e="T24" id="Seg_2373" s="T23">туда-ILL</ta>
            <ta e="T25" id="Seg_2374" s="T24">взять-CO-3PL</ta>
            <ta e="T26" id="Seg_2375" s="T25">солдат-ILL</ta>
            <ta e="T27" id="Seg_2376" s="T26">я.NOM</ta>
            <ta e="T28" id="Seg_2377" s="T27">туда-ADV.LOC</ta>
            <ta e="T29" id="Seg_2378" s="T28">быть-CO-1SG.S</ta>
            <ta e="T30" id="Seg_2379" s="T29">армия-LOC</ta>
            <ta e="T31" id="Seg_2380" s="T30">шесть</ta>
            <ta e="T32" id="Seg_2381" s="T31">месяц-PL.[NOM]</ta>
            <ta e="T33" id="Seg_2382" s="T32">мы.PL.NOM</ta>
            <ta e="T34" id="Seg_2383" s="T33">туда-ADV.LOC</ta>
            <ta e="T35" id="Seg_2384" s="T34">воевать-VBLZ-1PL</ta>
            <ta e="T36" id="Seg_2385" s="T35">война-TR-1PL</ta>
            <ta e="T37" id="Seg_2386" s="T36">я.ACC</ta>
            <ta e="T38" id="Seg_2387" s="T37">я.NOM</ta>
            <ta e="T39" id="Seg_2388" s="T38">первый.раз</ta>
            <ta e="T40" id="Seg_2389" s="T39">попасть-1SG.S</ta>
            <ta e="T41" id="Seg_2390" s="T40">танк-EP-ILL</ta>
            <ta e="T42" id="Seg_2391" s="T41">ночь-LOC</ta>
            <ta e="T43" id="Seg_2392" s="T42">нога-EP-PROL.1SG</ta>
            <ta e="T44" id="Seg_2393" s="T43">танк.[NOM]</ta>
            <ta e="T45" id="Seg_2394" s="T44">пройти.[3SG.S]</ta>
            <ta e="T46" id="Seg_2395" s="T45">я.NOM</ta>
            <ta e="T47" id="Seg_2396" s="T46">три</ta>
            <ta e="T48" id="Seg_2397" s="T47">день.[NOM]</ta>
            <ta e="T49" id="Seg_2398" s="T48">сам.1SG</ta>
            <ta e="T50" id="Seg_2399" s="T49">товарищ-PL-ABL</ta>
            <ta e="T51" id="Seg_2400" s="T50">остаться-1SG.S</ta>
            <ta e="T52" id="Seg_2401" s="T51">я.ACC</ta>
            <ta e="T53" id="Seg_2402" s="T52">оттуда</ta>
            <ta e="T54" id="Seg_2403" s="T53">лечить-EP-IPFV2-3PL</ta>
            <ta e="T55" id="Seg_2404" s="T54">три</ta>
            <ta e="T56" id="Seg_2405" s="T55">день.[NOM]</ta>
            <ta e="T57" id="Seg_2406" s="T56">лечить-IPFV2-3PL</ta>
            <ta e="T58" id="Seg_2407" s="T57">я.ACC</ta>
            <ta e="T59" id="Seg_2408" s="T58">вперёд</ta>
            <ta e="T60" id="Seg_2409" s="T59">посылать-3PL</ta>
            <ta e="T61" id="Seg_2410" s="T60">я.NOM</ta>
            <ta e="T62" id="Seg_2411" s="T61">война-TR-1SG.S</ta>
            <ta e="T63" id="Seg_2412" s="T62">два-большая.река-GEN</ta>
            <ta e="T64" id="Seg_2413" s="T63">через</ta>
            <ta e="T65" id="Seg_2414" s="T64">%%-IPFV2-1SG.S</ta>
            <ta e="T66" id="Seg_2415" s="T65">вперёд</ta>
            <ta e="T67" id="Seg_2416" s="T66">посылать-3PL</ta>
            <ta e="T68" id="Seg_2417" s="T67">я.ACC</ta>
            <ta e="T69" id="Seg_2418" s="T68">переправить-1SG.S</ta>
            <ta e="T70" id="Seg_2419" s="T69">я.NOM</ta>
            <ta e="T71" id="Seg_2420" s="T70">вперёд</ta>
            <ta e="T72" id="Seg_2421" s="T71">идти-1SG.S</ta>
            <ta e="T73" id="Seg_2422" s="T72">вперёд-1PL</ta>
            <ta e="T74" id="Seg_2423" s="T73">я.ACC</ta>
            <ta e="T75" id="Seg_2424" s="T74">посылать-3PL</ta>
            <ta e="T76" id="Seg_2425" s="T75">место-ACC</ta>
            <ta e="T77" id="Seg_2426" s="T76">вперёд-ADJZ</ta>
            <ta e="T78" id="Seg_2427" s="T77">смотреть-DUR-INF</ta>
            <ta e="T79" id="Seg_2428" s="T78">я.NOM</ta>
            <ta e="T80" id="Seg_2429" s="T79">туда-ADV.LOC</ta>
            <ta e="T81" id="Seg_2430" s="T80">идти-CO-1SG.S</ta>
            <ta e="T82" id="Seg_2431" s="T81">назад</ta>
            <ta e="T83" id="Seg_2432" s="T82">прийти-CVB2</ta>
            <ta e="T84" id="Seg_2433" s="T83">сказать-1SG.O</ta>
            <ta e="T85" id="Seg_2434" s="T84">командир-EP-ILL</ta>
            <ta e="T86" id="Seg_2435" s="T85">немец.[NOM]</ta>
            <ta e="T87" id="Seg_2436" s="T86">здесь</ta>
            <ta e="T88" id="Seg_2437" s="T87">быть-CO.[3SG.S]</ta>
            <ta e="T89" id="Seg_2438" s="T88">река-LOC</ta>
            <ta e="T90" id="Seg_2439" s="T89">сидеть-3PL</ta>
            <ta e="T91" id="Seg_2440" s="T90">он(а).[NOM]</ta>
            <ta e="T92" id="Seg_2441" s="T91">я.ALL</ta>
            <ta e="T93" id="Seg_2442" s="T92">сказать-3SG.O</ta>
            <ta e="T94" id="Seg_2443" s="T93">HORT</ta>
            <ta e="T95" id="Seg_2444" s="T94">быстро</ta>
            <ta e="T96" id="Seg_2445" s="T95">бегать-OPT-1PL</ta>
            <ta e="T97" id="Seg_2446" s="T96">я.NOM</ta>
            <ta e="T98" id="Seg_2447" s="T97">отправляться-1SG.S</ta>
            <ta e="T99" id="Seg_2448" s="T98">достичь-FRQ-CO-1SG.O</ta>
            <ta e="T100" id="Seg_2449" s="T99">и</ta>
            <ta e="T101" id="Seg_2450" s="T100">быстро</ta>
            <ta e="T102" id="Seg_2451" s="T101">бегать-1DU</ta>
            <ta e="T103" id="Seg_2452" s="T102">спина-LOC</ta>
            <ta e="T104" id="Seg_2453" s="T103">солдат-PL.[NOM]</ta>
            <ta e="T105" id="Seg_2454" s="T104">идти-3PL</ta>
            <ta e="T106" id="Seg_2455" s="T105">мы.PL.NOM</ta>
            <ta e="T107" id="Seg_2456" s="T106">спешить-DUR-1PL</ta>
            <ta e="T108" id="Seg_2457" s="T107">я.NOM</ta>
            <ta e="T109" id="Seg_2458" s="T108">принести-1SG.O</ta>
            <ta e="T110" id="Seg_2459" s="T109">командир-EP-ACC</ta>
            <ta e="T111" id="Seg_2460" s="T110">река-GEN</ta>
            <ta e="T112" id="Seg_2461" s="T111">Томь-ILL</ta>
            <ta e="T113" id="Seg_2462" s="T112">мы.DU.NOM</ta>
            <ta e="T114" id="Seg_2463" s="T113">он(а)-GEN-с</ta>
            <ta e="T115" id="Seg_2464" s="T114">хотеть-INCH-1DU</ta>
            <ta e="T116" id="Seg_2465" s="T115">немец-PL.[NOM]</ta>
            <ta e="T117" id="Seg_2466" s="T116">смотреть-DRV-PFV-INF</ta>
            <ta e="T118" id="Seg_2467" s="T117">командир.[NOM]</ta>
            <ta e="T119" id="Seg_2468" s="T118">вверх</ta>
            <ta e="T120" id="Seg_2469" s="T119">вставать-PFV.[3SG.S]</ta>
            <ta e="T121" id="Seg_2470" s="T120">смотреть-DRV-PFV-INF</ta>
            <ta e="T122" id="Seg_2471" s="T121">оттуда</ta>
            <ta e="T123" id="Seg_2472" s="T122">немец.[NOM]</ta>
            <ta e="T124" id="Seg_2473" s="T123">увидеть-PFV.[3SG.S]</ta>
            <ta e="T125" id="Seg_2474" s="T124">мы.DU.ACC</ta>
            <ta e="T126" id="Seg_2475" s="T125">и</ta>
            <ta e="T127" id="Seg_2476" s="T126">оттуда</ta>
            <ta e="T128" id="Seg_2477" s="T127">стрелять.[3SG.S]</ta>
            <ta e="T129" id="Seg_2478" s="T128">мы.DU.ACC</ta>
            <ta e="T130" id="Seg_2479" s="T129">поранить-3SG.S</ta>
            <ta e="T131" id="Seg_2480" s="T130">мы.DU.ACC</ta>
            <ta e="T132" id="Seg_2481" s="T131">командир-EP-1SG</ta>
            <ta e="T133" id="Seg_2482" s="T132">NEG</ta>
            <ta e="T134" id="Seg_2483" s="T133">дышать-FRQ-3SG.S</ta>
            <ta e="T135" id="Seg_2484" s="T134">здесь</ta>
            <ta e="T136" id="Seg_2485" s="T135">я.NOM</ta>
            <ta e="T137" id="Seg_2486" s="T136">рука-GEN.1SG-INSTR</ta>
            <ta e="T138" id="Seg_2487" s="T137">показать-1SG.O</ta>
            <ta e="T139" id="Seg_2488" s="T138">рука-INSTR2-3SG</ta>
            <ta e="T140" id="Seg_2489" s="T139">махать-PFV-3SG.O</ta>
            <ta e="T141" id="Seg_2490" s="T140">голова-ACC-3SG</ta>
            <ta e="T142" id="Seg_2491" s="T141">наверх</ta>
            <ta e="T143" id="Seg_2492" s="T142">NEG.IMP</ta>
            <ta e="T144" id="Seg_2493" s="T143">поднимать-IPFV3-DUR-IMP.2SG.O</ta>
            <ta e="T145" id="Seg_2494" s="T144">он(а).[NOM]</ta>
            <ta e="T146" id="Seg_2495" s="T145">узнать-IPFV-3SG.O</ta>
            <ta e="T147" id="Seg_2496" s="T146">назад</ta>
            <ta e="T148" id="Seg_2497" s="T147">кричать-CO.[3SG.S]</ta>
            <ta e="T149" id="Seg_2498" s="T148">он(а).[NOM]</ta>
            <ta e="T150" id="Seg_2499" s="T149">оттуда</ta>
            <ta e="T151" id="Seg_2500" s="T150">прийти-IPFV2.[3SG.S]</ta>
            <ta e="T152" id="Seg_2501" s="T151">фельдшер.[NOM]</ta>
            <ta e="T153" id="Seg_2502" s="T152">мы.DU.ALL</ta>
            <ta e="T154" id="Seg_2503" s="T153">прийти-CO.[3SG.S]</ta>
            <ta e="T155" id="Seg_2504" s="T154">достичь-INF</ta>
            <ta e="T156" id="Seg_2505" s="T155">NEG</ta>
            <ta e="T157" id="Seg_2506" s="T156">может.быть</ta>
            <ta e="T158" id="Seg_2507" s="T157">мы.DU.ALL</ta>
            <ta e="T159" id="Seg_2508" s="T158">он(а).[NOM]</ta>
            <ta e="T160" id="Seg_2509" s="T159">обмотка-ACC</ta>
            <ta e="T161" id="Seg_2510" s="T160">прочь</ta>
            <ta e="T162" id="Seg_2511" s="T161">завернуть-INCH-IPFV2-3SG.O</ta>
            <ta e="T163" id="Seg_2512" s="T162">прочь</ta>
            <ta e="T164" id="Seg_2513" s="T163">завернуть-CO-3SG.O</ta>
            <ta e="T165" id="Seg_2514" s="T164">мы.DU.ACC</ta>
            <ta e="T166" id="Seg_2515" s="T165">оттуда</ta>
            <ta e="T167" id="Seg_2516" s="T166">INCH</ta>
            <ta e="T168" id="Seg_2517" s="T167">оттащить-EP-IPFV2-INF</ta>
            <ta e="T169" id="Seg_2518" s="T168">вот</ta>
            <ta e="T170" id="Seg_2519" s="T169">оттащить-CO.[3SG.S]</ta>
            <ta e="T171" id="Seg_2520" s="T170">обмотка-INSTR</ta>
            <ta e="T172" id="Seg_2521" s="T171">оттащить-CO.[3SG.S]</ta>
            <ta e="T173" id="Seg_2522" s="T172">командир-EP-ACC</ta>
            <ta e="T174" id="Seg_2523" s="T173">убить-CO-3PL</ta>
            <ta e="T175" id="Seg_2524" s="T174">совсем</ta>
            <ta e="T176" id="Seg_2525" s="T175">я.ACC</ta>
            <ta e="T177" id="Seg_2526" s="T176">завязать-EP-CO-3PL</ta>
            <ta e="T178" id="Seg_2527" s="T177">положить-DRV-CO-3PL</ta>
            <ta e="T179" id="Seg_2528" s="T178">окоп-EP-ILL.3SG</ta>
            <ta e="T180" id="Seg_2529" s="T179">шесть</ta>
            <ta e="T181" id="Seg_2530" s="T180">час-GEN-в.течение</ta>
            <ta e="T182" id="Seg_2531" s="T181">я.NOM</ta>
            <ta e="T183" id="Seg_2532" s="T182">окоп-EP-LOC</ta>
            <ta e="T184" id="Seg_2533" s="T183">лежать-1SG.S</ta>
            <ta e="T185" id="Seg_2534" s="T184">я.ACC</ta>
            <ta e="T186" id="Seg_2535" s="T185">оттуда</ta>
            <ta e="T187" id="Seg_2536" s="T186">оттащить-3SG.S</ta>
            <ta e="T188" id="Seg_2537" s="T187">санинструктор.[NOM]</ta>
            <ta e="T189" id="Seg_2538" s="T188">я.ACC</ta>
            <ta e="T190" id="Seg_2539" s="T189">он(а).[NOM]</ta>
            <ta e="T191" id="Seg_2540" s="T190">принести-FRQ-EP-3SG.O</ta>
            <ta e="T192" id="Seg_2541" s="T191">пять</ta>
            <ta e="T193" id="Seg_2542" s="T192">десять</ta>
            <ta e="T194" id="Seg_2543" s="T193">сажень-ADV.LOC</ta>
            <ta e="T195" id="Seg_2544" s="T194">пять</ta>
            <ta e="T196" id="Seg_2545" s="T195">десять</ta>
            <ta e="T197" id="Seg_2546" s="T196">сажень.[NOM]</ta>
            <ta e="T198" id="Seg_2547" s="T197">пять-излишний</ta>
            <ta e="T199" id="Seg_2548" s="T198">сажень.[NOM]</ta>
            <ta e="T200" id="Seg_2549" s="T199">потом</ta>
            <ta e="T201" id="Seg_2550" s="T200">лошадь-INSTR</ta>
            <ta e="T202" id="Seg_2551" s="T201">принести-FRQ-EP-3PL</ta>
            <ta e="T203" id="Seg_2552" s="T202">я.ACC</ta>
            <ta e="T204" id="Seg_2553" s="T203">первый-ADJZ</ta>
            <ta e="T205" id="Seg_2554" s="T204">деревня-ILL</ta>
            <ta e="T206" id="Seg_2555" s="T205">принести-3PL</ta>
            <ta e="T207" id="Seg_2556" s="T206">здесь</ta>
            <ta e="T208" id="Seg_2557" s="T207">я.NOM-ADES</ta>
            <ta e="T209" id="Seg_2558" s="T208">прочь</ta>
            <ta e="T210" id="Seg_2559" s="T209">дёрнуть-MULS-PST.NAR-3PL</ta>
            <ta e="T211" id="Seg_2560" s="T210">осколок-PL-ACC</ta>
            <ta e="T212" id="Seg_2561" s="T211">нога-EP-EL.3SG</ta>
            <ta e="T213" id="Seg_2562" s="T212">оттуда</ta>
            <ta e="T214" id="Seg_2563" s="T213">я.ACC</ta>
            <ta e="T215" id="Seg_2564" s="T214">назад</ta>
            <ta e="T216" id="Seg_2565" s="T215">принести-FRQ-EP-INF</ta>
            <ta e="T217" id="Seg_2566" s="T216">начать-FRQ-EP-3PL</ta>
            <ta e="T218" id="Seg_2567" s="T217">я.ACC</ta>
            <ta e="T219" id="Seg_2568" s="T218">оттуда</ta>
            <ta e="T220" id="Seg_2569" s="T219">линия-ILL</ta>
            <ta e="T221" id="Seg_2570" s="T220">пойти-TR-3PL</ta>
            <ta e="T222" id="Seg_2571" s="T221">машина-INSTR</ta>
            <ta e="T223" id="Seg_2572" s="T222">принести-FRQ-EP-3PL</ta>
            <ta e="T224" id="Seg_2573" s="T223">я.NOM</ta>
            <ta e="T225" id="Seg_2574" s="T224">лежать-1SG.S</ta>
            <ta e="T226" id="Seg_2575" s="T225">больница-LOC</ta>
            <ta e="T227" id="Seg_2576" s="T226">два</ta>
            <ta e="T228" id="Seg_2577" s="T227">год.[NOM]-3SG</ta>
            <ta e="T229" id="Seg_2578" s="T228">четыре</ta>
            <ta e="T230" id="Seg_2579" s="T229">месяц.[NOM]-3SG</ta>
            <ta e="T231" id="Seg_2580" s="T230">оттуда</ta>
            <ta e="T232" id="Seg_2581" s="T231">я.ACC</ta>
            <ta e="T233" id="Seg_2582" s="T232">лечить-CO-3PL</ta>
            <ta e="T234" id="Seg_2583" s="T233">я.NOM</ta>
            <ta e="T235" id="Seg_2584" s="T234">идти-CO-1SG.S</ta>
            <ta e="T236" id="Seg_2585" s="T235">оттуда</ta>
            <ta e="T237" id="Seg_2586" s="T236">я.ACC</ta>
            <ta e="T238" id="Seg_2587" s="T237">назад</ta>
            <ta e="T239" id="Seg_2588" s="T238">посылать-3PL</ta>
            <ta e="T240" id="Seg_2589" s="T239">я.NOM</ta>
            <ta e="T241" id="Seg_2590" s="T240">домой</ta>
            <ta e="T242" id="Seg_2591" s="T241">прийти-CO-1SG.S</ta>
            <ta e="T243" id="Seg_2592" s="T242">после.этого</ta>
            <ta e="T244" id="Seg_2593" s="T243">я.NOM</ta>
            <ta e="T245" id="Seg_2594" s="T244">дом-LOC</ta>
            <ta e="T246" id="Seg_2595" s="T245">жить-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_2596" s="T0">pers</ta>
            <ta e="T2" id="Seg_2597" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_2598" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_2599" s="T3">pers</ta>
            <ta e="T5" id="Seg_2600" s="T4">v-v&gt;v-v:pn</ta>
            <ta e="T6" id="Seg_2601" s="T5">pers</ta>
            <ta e="T7" id="Seg_2602" s="T6">v-n:ins-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T8" id="Seg_2603" s="T7">nprop-n:case</ta>
            <ta e="T9" id="Seg_2604" s="T8">pers</ta>
            <ta e="T10" id="Seg_2605" s="T9">adv</ta>
            <ta e="T11" id="Seg_2606" s="T10">num</ta>
            <ta e="T12" id="Seg_2607" s="T11">n-n:case.poss</ta>
            <ta e="T13" id="Seg_2608" s="T12">v-n:ins-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T14" id="Seg_2609" s="T13">adv</ta>
            <ta e="T15" id="Seg_2610" s="T14">pers</ta>
            <ta e="T16" id="Seg_2611" s="T15">v-v&gt;v-v:pn</ta>
            <ta e="T17" id="Seg_2612" s="T16">nprop-n:case</ta>
            <ta e="T18" id="Seg_2613" s="T17">pers</ta>
            <ta e="T19" id="Seg_2614" s="T18">adv-adv:case</ta>
            <ta e="T20" id="Seg_2615" s="T19">v-v:pn</ta>
            <ta e="T21" id="Seg_2616" s="T20">num</ta>
            <ta e="T22" id="Seg_2617" s="T21">n-n:case-n:poss</ta>
            <ta e="T23" id="Seg_2618" s="T22">pers</ta>
            <ta e="T24" id="Seg_2619" s="T23">adv-n:case</ta>
            <ta e="T25" id="Seg_2620" s="T24">v-v:ins-v:pn</ta>
            <ta e="T26" id="Seg_2621" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_2622" s="T26">pers</ta>
            <ta e="T28" id="Seg_2623" s="T27">adv-adv:case</ta>
            <ta e="T29" id="Seg_2624" s="T28">v-v:ins-v:pn</ta>
            <ta e="T30" id="Seg_2625" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_2626" s="T30">num</ta>
            <ta e="T32" id="Seg_2627" s="T31">n-n:num-n:case</ta>
            <ta e="T33" id="Seg_2628" s="T32">pers</ta>
            <ta e="T34" id="Seg_2629" s="T33">adv-adv:case</ta>
            <ta e="T35" id="Seg_2630" s="T34">v-v&gt;v-v:pn</ta>
            <ta e="T36" id="Seg_2631" s="T35">n-n&gt;v-v:pn</ta>
            <ta e="T37" id="Seg_2632" s="T36">pers</ta>
            <ta e="T38" id="Seg_2633" s="T37">pers</ta>
            <ta e="T39" id="Seg_2634" s="T38">adv</ta>
            <ta e="T40" id="Seg_2635" s="T39">v-v:pn</ta>
            <ta e="T41" id="Seg_2636" s="T40">n-n:ins-n:case</ta>
            <ta e="T42" id="Seg_2637" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_2638" s="T42">n-n:ins-n:case.poss</ta>
            <ta e="T44" id="Seg_2639" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_2640" s="T44">v-v:pn</ta>
            <ta e="T46" id="Seg_2641" s="T45">pers</ta>
            <ta e="T47" id="Seg_2642" s="T46">num</ta>
            <ta e="T48" id="Seg_2643" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_2644" s="T48">emphpro</ta>
            <ta e="T50" id="Seg_2645" s="T49">n-n:num-n:case</ta>
            <ta e="T51" id="Seg_2646" s="T50">v-v:pn</ta>
            <ta e="T52" id="Seg_2647" s="T51">pers</ta>
            <ta e="T53" id="Seg_2648" s="T52">adv</ta>
            <ta e="T54" id="Seg_2649" s="T53">v-n:ins-v&gt;v-v:pn</ta>
            <ta e="T55" id="Seg_2650" s="T54">num</ta>
            <ta e="T56" id="Seg_2651" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_2652" s="T56">v-v&gt;v-v:pn</ta>
            <ta e="T58" id="Seg_2653" s="T57">pers</ta>
            <ta e="T59" id="Seg_2654" s="T58">adv</ta>
            <ta e="T60" id="Seg_2655" s="T59">v-v:pn</ta>
            <ta e="T61" id="Seg_2656" s="T60">pers</ta>
            <ta e="T62" id="Seg_2657" s="T61">n-n&gt;v-v:pn</ta>
            <ta e="T63" id="Seg_2658" s="T62">num-n-n:case</ta>
            <ta e="T64" id="Seg_2659" s="T63">pp</ta>
            <ta e="T65" id="Seg_2660" s="T64">v-v&gt;v-v:pn</ta>
            <ta e="T66" id="Seg_2661" s="T65">adv</ta>
            <ta e="T67" id="Seg_2662" s="T66">v-v:pn</ta>
            <ta e="T68" id="Seg_2663" s="T67">pers</ta>
            <ta e="T69" id="Seg_2664" s="T68">v-v:pn</ta>
            <ta e="T70" id="Seg_2665" s="T69">pers</ta>
            <ta e="T71" id="Seg_2666" s="T70">adv</ta>
            <ta e="T72" id="Seg_2667" s="T71">v-v:pn</ta>
            <ta e="T73" id="Seg_2668" s="T72">adv-n:poss</ta>
            <ta e="T74" id="Seg_2669" s="T73">pers</ta>
            <ta e="T75" id="Seg_2670" s="T74">v-v:pn</ta>
            <ta e="T76" id="Seg_2671" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_2672" s="T76">adv-n&gt;adj</ta>
            <ta e="T78" id="Seg_2673" s="T77">v-v&gt;v-v:inf</ta>
            <ta e="T79" id="Seg_2674" s="T78">pers</ta>
            <ta e="T80" id="Seg_2675" s="T79">adv-adv:case</ta>
            <ta e="T81" id="Seg_2676" s="T80">v-v:ins-v:pn</ta>
            <ta e="T82" id="Seg_2677" s="T81">adv</ta>
            <ta e="T83" id="Seg_2678" s="T82">v-v&gt;adv</ta>
            <ta e="T84" id="Seg_2679" s="T83">v-v:pn</ta>
            <ta e="T85" id="Seg_2680" s="T84">n-n:ins-n:case</ta>
            <ta e="T86" id="Seg_2681" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_2682" s="T86">adv</ta>
            <ta e="T88" id="Seg_2683" s="T87">v-v:ins-v:pn</ta>
            <ta e="T89" id="Seg_2684" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_2685" s="T89">v-v:pn</ta>
            <ta e="T91" id="Seg_2686" s="T90">pers-n:case</ta>
            <ta e="T92" id="Seg_2687" s="T91">pers</ta>
            <ta e="T93" id="Seg_2688" s="T92">v-v:pn</ta>
            <ta e="T94" id="Seg_2689" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_2690" s="T94">adv</ta>
            <ta e="T96" id="Seg_2691" s="T95">v-v:mood-v:pn</ta>
            <ta e="T97" id="Seg_2692" s="T96">pers</ta>
            <ta e="T98" id="Seg_2693" s="T97">v-v:pn</ta>
            <ta e="T99" id="Seg_2694" s="T98">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T100" id="Seg_2695" s="T99">conj</ta>
            <ta e="T101" id="Seg_2696" s="T100">adv</ta>
            <ta e="T102" id="Seg_2697" s="T101">v-v:pn</ta>
            <ta e="T103" id="Seg_2698" s="T102">n-n:case</ta>
            <ta e="T104" id="Seg_2699" s="T103">n-n:num-n:case</ta>
            <ta e="T105" id="Seg_2700" s="T104">v-v:pn</ta>
            <ta e="T106" id="Seg_2701" s="T105">pers</ta>
            <ta e="T107" id="Seg_2702" s="T106">v-v&gt;v-v:pn</ta>
            <ta e="T108" id="Seg_2703" s="T107">pers</ta>
            <ta e="T109" id="Seg_2704" s="T108">v-v:pn</ta>
            <ta e="T110" id="Seg_2705" s="T109">n-n:ins-n:case</ta>
            <ta e="T111" id="Seg_2706" s="T110">n-n:case</ta>
            <ta e="T112" id="Seg_2707" s="T111">nprop-n:case</ta>
            <ta e="T113" id="Seg_2708" s="T112">pers</ta>
            <ta e="T114" id="Seg_2709" s="T113">pers-n:case-pp</ta>
            <ta e="T115" id="Seg_2710" s="T114">v-v&gt;v-v:pn</ta>
            <ta e="T116" id="Seg_2711" s="T115">n-n:num-n:case</ta>
            <ta e="T117" id="Seg_2712" s="T116">v-v&gt;v-v&gt;v-v:inf</ta>
            <ta e="T118" id="Seg_2713" s="T117">n-n:case</ta>
            <ta e="T119" id="Seg_2714" s="T118">preverb</ta>
            <ta e="T120" id="Seg_2715" s="T119">v-v&gt;v-v:pn</ta>
            <ta e="T121" id="Seg_2716" s="T120">v-v&gt;v-v&gt;v-v:inf</ta>
            <ta e="T122" id="Seg_2717" s="T121">adv</ta>
            <ta e="T123" id="Seg_2718" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_2719" s="T123">v-v&gt;v-v:pn</ta>
            <ta e="T125" id="Seg_2720" s="T124">pers</ta>
            <ta e="T126" id="Seg_2721" s="T125">conj</ta>
            <ta e="T127" id="Seg_2722" s="T126">adv</ta>
            <ta e="T128" id="Seg_2723" s="T127">v-v:pn</ta>
            <ta e="T129" id="Seg_2724" s="T128">pers</ta>
            <ta e="T130" id="Seg_2725" s="T129">v-v:pn</ta>
            <ta e="T131" id="Seg_2726" s="T130">pers</ta>
            <ta e="T132" id="Seg_2727" s="T131">n-n:ins-n:poss</ta>
            <ta e="T133" id="Seg_2728" s="T132">ptcl</ta>
            <ta e="T134" id="Seg_2729" s="T133">v-v&gt;v-v:pn</ta>
            <ta e="T135" id="Seg_2730" s="T134">adv</ta>
            <ta e="T136" id="Seg_2731" s="T135">pers</ta>
            <ta e="T137" id="Seg_2732" s="T136">n-n:obl.poss-n:case</ta>
            <ta e="T138" id="Seg_2733" s="T137">v-v:pn</ta>
            <ta e="T139" id="Seg_2734" s="T138">n-n:case-n:poss</ta>
            <ta e="T140" id="Seg_2735" s="T139">v-v&gt;v-v:pn</ta>
            <ta e="T141" id="Seg_2736" s="T140">n-n:case-n:poss</ta>
            <ta e="T142" id="Seg_2737" s="T141">adv</ta>
            <ta e="T143" id="Seg_2738" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_2739" s="T143">v-v&gt;v-v&gt;v-v:mood.pn</ta>
            <ta e="T145" id="Seg_2740" s="T144">pers-n:case</ta>
            <ta e="T146" id="Seg_2741" s="T145">v-v&gt;v-v:pn</ta>
            <ta e="T147" id="Seg_2742" s="T146">adv</ta>
            <ta e="T148" id="Seg_2743" s="T147">v-v:ins-v:pn</ta>
            <ta e="T149" id="Seg_2744" s="T148">pers-n:case</ta>
            <ta e="T150" id="Seg_2745" s="T149">adv</ta>
            <ta e="T151" id="Seg_2746" s="T150">v-v&gt;v-v:pn</ta>
            <ta e="T152" id="Seg_2747" s="T151">n-n:case</ta>
            <ta e="T153" id="Seg_2748" s="T152">pers</ta>
            <ta e="T154" id="Seg_2749" s="T153">v-v:ins-v:pn</ta>
            <ta e="T155" id="Seg_2750" s="T154">v-v:inf</ta>
            <ta e="T156" id="Seg_2751" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_2752" s="T156">v</ta>
            <ta e="T158" id="Seg_2753" s="T157">pers</ta>
            <ta e="T159" id="Seg_2754" s="T158">pers-n:case</ta>
            <ta e="T160" id="Seg_2755" s="T159">n-n:case</ta>
            <ta e="T161" id="Seg_2756" s="T160">preverb</ta>
            <ta e="T162" id="Seg_2757" s="T161">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T163" id="Seg_2758" s="T162">preverb</ta>
            <ta e="T164" id="Seg_2759" s="T163">v-v:ins-v:pn</ta>
            <ta e="T165" id="Seg_2760" s="T164">pers</ta>
            <ta e="T166" id="Seg_2761" s="T165">adv</ta>
            <ta e="T167" id="Seg_2762" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_2763" s="T167">v-n:ins-v&gt;v-v:inf</ta>
            <ta e="T169" id="Seg_2764" s="T168">interj</ta>
            <ta e="T170" id="Seg_2765" s="T169">v-v:ins-v:pn</ta>
            <ta e="T171" id="Seg_2766" s="T170">n-n:case</ta>
            <ta e="T172" id="Seg_2767" s="T171">v-v:ins-v:pn</ta>
            <ta e="T173" id="Seg_2768" s="T172">n-n:ins-n:case</ta>
            <ta e="T174" id="Seg_2769" s="T173">v-v:ins-v:pn</ta>
            <ta e="T175" id="Seg_2770" s="T174">adv</ta>
            <ta e="T176" id="Seg_2771" s="T175">pers</ta>
            <ta e="T177" id="Seg_2772" s="T176">v-n:ins-v:ins-v:pn</ta>
            <ta e="T178" id="Seg_2773" s="T177">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T179" id="Seg_2774" s="T178">n-n:ins-n:case.poss</ta>
            <ta e="T180" id="Seg_2775" s="T179">num</ta>
            <ta e="T181" id="Seg_2776" s="T180">n-n:case-pp</ta>
            <ta e="T182" id="Seg_2777" s="T181">pers</ta>
            <ta e="T183" id="Seg_2778" s="T182">n-n:ins-n:case</ta>
            <ta e="T184" id="Seg_2779" s="T183">v-v:pn</ta>
            <ta e="T185" id="Seg_2780" s="T184">pers</ta>
            <ta e="T186" id="Seg_2781" s="T185">adv</ta>
            <ta e="T187" id="Seg_2782" s="T186">v-v:pn</ta>
            <ta e="T188" id="Seg_2783" s="T187">n-n:case</ta>
            <ta e="T189" id="Seg_2784" s="T188">pers</ta>
            <ta e="T190" id="Seg_2785" s="T189">pers-n:case</ta>
            <ta e="T191" id="Seg_2786" s="T190">v-v&gt;v-n:ins-v:pn</ta>
            <ta e="T192" id="Seg_2787" s="T191">num</ta>
            <ta e="T193" id="Seg_2788" s="T192">num</ta>
            <ta e="T194" id="Seg_2789" s="T193">n-adv:case</ta>
            <ta e="T195" id="Seg_2790" s="T194">num</ta>
            <ta e="T196" id="Seg_2791" s="T195">num</ta>
            <ta e="T197" id="Seg_2792" s="T196">n-n:case</ta>
            <ta e="T198" id="Seg_2793" s="T197">num-num&gt;num</ta>
            <ta e="T199" id="Seg_2794" s="T198">n-n:case</ta>
            <ta e="T200" id="Seg_2795" s="T199">adv</ta>
            <ta e="T201" id="Seg_2796" s="T200">n-n:case</ta>
            <ta e="T202" id="Seg_2797" s="T201">v-v&gt;v-n:ins-v:pn</ta>
            <ta e="T203" id="Seg_2798" s="T202">pers</ta>
            <ta e="T204" id="Seg_2799" s="T203">adj-n&gt;adj</ta>
            <ta e="T205" id="Seg_2800" s="T204">n-n:case</ta>
            <ta e="T206" id="Seg_2801" s="T205">v-v:pn</ta>
            <ta e="T207" id="Seg_2802" s="T206">adv</ta>
            <ta e="T208" id="Seg_2803" s="T207">pers-n:case</ta>
            <ta e="T209" id="Seg_2804" s="T208">preverb</ta>
            <ta e="T210" id="Seg_2805" s="T209">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T211" id="Seg_2806" s="T210">n-n:num-n:case</ta>
            <ta e="T212" id="Seg_2807" s="T211">n-n:ins-n:case.poss</ta>
            <ta e="T213" id="Seg_2808" s="T212">adv</ta>
            <ta e="T214" id="Seg_2809" s="T213">pers</ta>
            <ta e="T215" id="Seg_2810" s="T214">adv</ta>
            <ta e="T216" id="Seg_2811" s="T215">v-v&gt;v-n:ins-v:inf</ta>
            <ta e="T217" id="Seg_2812" s="T216">v-v&gt;v-n:ins-v:pn</ta>
            <ta e="T218" id="Seg_2813" s="T217">pers</ta>
            <ta e="T219" id="Seg_2814" s="T218">adv</ta>
            <ta e="T220" id="Seg_2815" s="T219">n-n:case</ta>
            <ta e="T221" id="Seg_2816" s="T220">v-v&gt;v-v:pn</ta>
            <ta e="T222" id="Seg_2817" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_2818" s="T222">v-v&gt;v-n:ins-v:pn</ta>
            <ta e="T224" id="Seg_2819" s="T223">pers</ta>
            <ta e="T225" id="Seg_2820" s="T224">v-v:pn</ta>
            <ta e="T226" id="Seg_2821" s="T225">n-n:case</ta>
            <ta e="T227" id="Seg_2822" s="T226">num</ta>
            <ta e="T228" id="Seg_2823" s="T227">n-n:case-n:poss</ta>
            <ta e="T229" id="Seg_2824" s="T228">num</ta>
            <ta e="T230" id="Seg_2825" s="T229">n-n:case-n:poss</ta>
            <ta e="T231" id="Seg_2826" s="T230">adv</ta>
            <ta e="T232" id="Seg_2827" s="T231">pers</ta>
            <ta e="T233" id="Seg_2828" s="T232">v-v:ins-v:pn</ta>
            <ta e="T234" id="Seg_2829" s="T233">pers</ta>
            <ta e="T235" id="Seg_2830" s="T234">v-v:ins-v:pn</ta>
            <ta e="T236" id="Seg_2831" s="T235">adv</ta>
            <ta e="T237" id="Seg_2832" s="T236">pers</ta>
            <ta e="T238" id="Seg_2833" s="T237">adv</ta>
            <ta e="T239" id="Seg_2834" s="T238">v-v:pn</ta>
            <ta e="T240" id="Seg_2835" s="T239">pers</ta>
            <ta e="T241" id="Seg_2836" s="T240">adv</ta>
            <ta e="T242" id="Seg_2837" s="T241">v-v:ins-v:pn</ta>
            <ta e="T243" id="Seg_2838" s="T242">adv</ta>
            <ta e="T244" id="Seg_2839" s="T243">pers</ta>
            <ta e="T245" id="Seg_2840" s="T244">n-n:case</ta>
            <ta e="T246" id="Seg_2841" s="T245">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2842" s="T0">pers</ta>
            <ta e="T2" id="Seg_2843" s="T1">n</ta>
            <ta e="T3" id="Seg_2844" s="T2">v</ta>
            <ta e="T4" id="Seg_2845" s="T3">pers</ta>
            <ta e="T5" id="Seg_2846" s="T4">v</ta>
            <ta e="T6" id="Seg_2847" s="T5">pers</ta>
            <ta e="T7" id="Seg_2848" s="T6">v</ta>
            <ta e="T8" id="Seg_2849" s="T7">nprop</ta>
            <ta e="T9" id="Seg_2850" s="T8">pers</ta>
            <ta e="T10" id="Seg_2851" s="T9">adv</ta>
            <ta e="T11" id="Seg_2852" s="T10">num</ta>
            <ta e="T12" id="Seg_2853" s="T11">n</ta>
            <ta e="T13" id="Seg_2854" s="T12">v</ta>
            <ta e="T14" id="Seg_2855" s="T13">adv</ta>
            <ta e="T15" id="Seg_2856" s="T14">pers</ta>
            <ta e="T16" id="Seg_2857" s="T15">v</ta>
            <ta e="T17" id="Seg_2858" s="T16">nprop</ta>
            <ta e="T18" id="Seg_2859" s="T17">pers</ta>
            <ta e="T19" id="Seg_2860" s="T18">adv</ta>
            <ta e="T20" id="Seg_2861" s="T19">v</ta>
            <ta e="T21" id="Seg_2862" s="T20">num</ta>
            <ta e="T22" id="Seg_2863" s="T21">v</ta>
            <ta e="T23" id="Seg_2864" s="T22">pers</ta>
            <ta e="T24" id="Seg_2865" s="T23">adv</ta>
            <ta e="T25" id="Seg_2866" s="T24">v</ta>
            <ta e="T26" id="Seg_2867" s="T25">n</ta>
            <ta e="T27" id="Seg_2868" s="T26">pers</ta>
            <ta e="T28" id="Seg_2869" s="T27">adv</ta>
            <ta e="T29" id="Seg_2870" s="T28">v</ta>
            <ta e="T30" id="Seg_2871" s="T29">n</ta>
            <ta e="T31" id="Seg_2872" s="T30">num</ta>
            <ta e="T32" id="Seg_2873" s="T31">n</ta>
            <ta e="T33" id="Seg_2874" s="T32">pers</ta>
            <ta e="T34" id="Seg_2875" s="T33">adv</ta>
            <ta e="T35" id="Seg_2876" s="T34">v</ta>
            <ta e="T36" id="Seg_2877" s="T35">v</ta>
            <ta e="T37" id="Seg_2878" s="T36">pers</ta>
            <ta e="T38" id="Seg_2879" s="T37">pers</ta>
            <ta e="T39" id="Seg_2880" s="T38">adv</ta>
            <ta e="T40" id="Seg_2881" s="T39">v</ta>
            <ta e="T41" id="Seg_2882" s="T40">n</ta>
            <ta e="T42" id="Seg_2883" s="T41">n</ta>
            <ta e="T43" id="Seg_2884" s="T42">n</ta>
            <ta e="T44" id="Seg_2885" s="T43">n</ta>
            <ta e="T45" id="Seg_2886" s="T44">v</ta>
            <ta e="T46" id="Seg_2887" s="T45">pers</ta>
            <ta e="T47" id="Seg_2888" s="T46">num</ta>
            <ta e="T48" id="Seg_2889" s="T47">n</ta>
            <ta e="T49" id="Seg_2890" s="T48">emphpro</ta>
            <ta e="T50" id="Seg_2891" s="T49">n</ta>
            <ta e="T51" id="Seg_2892" s="T50">v</ta>
            <ta e="T52" id="Seg_2893" s="T51">pers</ta>
            <ta e="T53" id="Seg_2894" s="T52">adv</ta>
            <ta e="T54" id="Seg_2895" s="T53">v</ta>
            <ta e="T55" id="Seg_2896" s="T54">num</ta>
            <ta e="T56" id="Seg_2897" s="T55">n</ta>
            <ta e="T57" id="Seg_2898" s="T56">v</ta>
            <ta e="T58" id="Seg_2899" s="T57">pers</ta>
            <ta e="T59" id="Seg_2900" s="T58">adv</ta>
            <ta e="T60" id="Seg_2901" s="T59">v</ta>
            <ta e="T61" id="Seg_2902" s="T60">pers</ta>
            <ta e="T62" id="Seg_2903" s="T61">v</ta>
            <ta e="T63" id="Seg_2904" s="T62">n</ta>
            <ta e="T64" id="Seg_2905" s="T63">prep</ta>
            <ta e="T65" id="Seg_2906" s="T64">v</ta>
            <ta e="T66" id="Seg_2907" s="T65">adv</ta>
            <ta e="T67" id="Seg_2908" s="T66">v</ta>
            <ta e="T68" id="Seg_2909" s="T67">pers</ta>
            <ta e="T69" id="Seg_2910" s="T68">v</ta>
            <ta e="T70" id="Seg_2911" s="T69">pers</ta>
            <ta e="T71" id="Seg_2912" s="T70">adv</ta>
            <ta e="T72" id="Seg_2913" s="T71">v</ta>
            <ta e="T73" id="Seg_2914" s="T72">adv</ta>
            <ta e="T74" id="Seg_2915" s="T73">pers</ta>
            <ta e="T75" id="Seg_2916" s="T74">v</ta>
            <ta e="T76" id="Seg_2917" s="T75">n</ta>
            <ta e="T77" id="Seg_2918" s="T76">adj</ta>
            <ta e="T78" id="Seg_2919" s="T77">v</ta>
            <ta e="T79" id="Seg_2920" s="T78">pers</ta>
            <ta e="T80" id="Seg_2921" s="T79">adv</ta>
            <ta e="T81" id="Seg_2922" s="T80">v</ta>
            <ta e="T82" id="Seg_2923" s="T81">adv</ta>
            <ta e="T83" id="Seg_2924" s="T82">adv</ta>
            <ta e="T84" id="Seg_2925" s="T83">v</ta>
            <ta e="T85" id="Seg_2926" s="T84">v</ta>
            <ta e="T86" id="Seg_2927" s="T85">n</ta>
            <ta e="T87" id="Seg_2928" s="T86">adv</ta>
            <ta e="T88" id="Seg_2929" s="T87">v</ta>
            <ta e="T89" id="Seg_2930" s="T88">n</ta>
            <ta e="T90" id="Seg_2931" s="T89">v</ta>
            <ta e="T91" id="Seg_2932" s="T90">dem</ta>
            <ta e="T92" id="Seg_2933" s="T91">pers</ta>
            <ta e="T93" id="Seg_2934" s="T92">v</ta>
            <ta e="T94" id="Seg_2935" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_2936" s="T94">adv</ta>
            <ta e="T96" id="Seg_2937" s="T95">v</ta>
            <ta e="T97" id="Seg_2938" s="T96">pers</ta>
            <ta e="T98" id="Seg_2939" s="T97">v</ta>
            <ta e="T99" id="Seg_2940" s="T98">v</ta>
            <ta e="T100" id="Seg_2941" s="T99">conj</ta>
            <ta e="T101" id="Seg_2942" s="T100">adv</ta>
            <ta e="T102" id="Seg_2943" s="T101">v</ta>
            <ta e="T103" id="Seg_2944" s="T102">n</ta>
            <ta e="T104" id="Seg_2945" s="T103">n</ta>
            <ta e="T105" id="Seg_2946" s="T104">v</ta>
            <ta e="T106" id="Seg_2947" s="T105">pers</ta>
            <ta e="T107" id="Seg_2948" s="T106">v</ta>
            <ta e="T108" id="Seg_2949" s="T107">pers</ta>
            <ta e="T109" id="Seg_2950" s="T108">v</ta>
            <ta e="T110" id="Seg_2951" s="T109">n</ta>
            <ta e="T111" id="Seg_2952" s="T110">n</ta>
            <ta e="T112" id="Seg_2953" s="T111">nprop</ta>
            <ta e="T113" id="Seg_2954" s="T112">pers</ta>
            <ta e="T114" id="Seg_2955" s="T113">pers</ta>
            <ta e="T115" id="Seg_2956" s="T114">n</ta>
            <ta e="T116" id="Seg_2957" s="T115">n</ta>
            <ta e="T117" id="Seg_2958" s="T116">v</ta>
            <ta e="T118" id="Seg_2959" s="T117">v</ta>
            <ta e="T119" id="Seg_2960" s="T118">preverb</ta>
            <ta e="T120" id="Seg_2961" s="T119">v</ta>
            <ta e="T121" id="Seg_2962" s="T120">v</ta>
            <ta e="T122" id="Seg_2963" s="T121">adv</ta>
            <ta e="T123" id="Seg_2964" s="T122">n</ta>
            <ta e="T124" id="Seg_2965" s="T123">v</ta>
            <ta e="T125" id="Seg_2966" s="T124">pers</ta>
            <ta e="T126" id="Seg_2967" s="T125">conj</ta>
            <ta e="T127" id="Seg_2968" s="T126">adv</ta>
            <ta e="T128" id="Seg_2969" s="T127">v</ta>
            <ta e="T129" id="Seg_2970" s="T128">pers</ta>
            <ta e="T130" id="Seg_2971" s="T129">v</ta>
            <ta e="T131" id="Seg_2972" s="T130">pers</ta>
            <ta e="T132" id="Seg_2973" s="T131">n</ta>
            <ta e="T133" id="Seg_2974" s="T132">ptcl</ta>
            <ta e="T134" id="Seg_2975" s="T133">v</ta>
            <ta e="T135" id="Seg_2976" s="T134">adv</ta>
            <ta e="T136" id="Seg_2977" s="T135">pers</ta>
            <ta e="T137" id="Seg_2978" s="T136">n</ta>
            <ta e="T138" id="Seg_2979" s="T137">v</ta>
            <ta e="T139" id="Seg_2980" s="T138">n</ta>
            <ta e="T140" id="Seg_2981" s="T139">v</ta>
            <ta e="T141" id="Seg_2982" s="T140">n</ta>
            <ta e="T142" id="Seg_2983" s="T141">adv</ta>
            <ta e="T143" id="Seg_2984" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_2985" s="T143">v</ta>
            <ta e="T145" id="Seg_2986" s="T144">pers</ta>
            <ta e="T146" id="Seg_2987" s="T145">v</ta>
            <ta e="T147" id="Seg_2988" s="T146">adv</ta>
            <ta e="T148" id="Seg_2989" s="T147">v</ta>
            <ta e="T149" id="Seg_2990" s="T148">pers</ta>
            <ta e="T150" id="Seg_2991" s="T149">adv</ta>
            <ta e="T151" id="Seg_2992" s="T150">v</ta>
            <ta e="T152" id="Seg_2993" s="T151">n</ta>
            <ta e="T153" id="Seg_2994" s="T152">pers</ta>
            <ta e="T154" id="Seg_2995" s="T153">v</ta>
            <ta e="T155" id="Seg_2996" s="T154">v</ta>
            <ta e="T156" id="Seg_2997" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_2998" s="T156">v</ta>
            <ta e="T158" id="Seg_2999" s="T157">pers</ta>
            <ta e="T159" id="Seg_3000" s="T158">pers</ta>
            <ta e="T160" id="Seg_3001" s="T159">n</ta>
            <ta e="T161" id="Seg_3002" s="T160">preverb</ta>
            <ta e="T162" id="Seg_3003" s="T161">v</ta>
            <ta e="T163" id="Seg_3004" s="T162">preverb</ta>
            <ta e="T164" id="Seg_3005" s="T163">v</ta>
            <ta e="T165" id="Seg_3006" s="T164">pers</ta>
            <ta e="T166" id="Seg_3007" s="T165">adv</ta>
            <ta e="T167" id="Seg_3008" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_3009" s="T167">v</ta>
            <ta e="T169" id="Seg_3010" s="T168">interj</ta>
            <ta e="T170" id="Seg_3011" s="T169">v</ta>
            <ta e="T171" id="Seg_3012" s="T170">n</ta>
            <ta e="T172" id="Seg_3013" s="T171">v</ta>
            <ta e="T173" id="Seg_3014" s="T172">n</ta>
            <ta e="T174" id="Seg_3015" s="T173">v</ta>
            <ta e="T175" id="Seg_3016" s="T174">adv</ta>
            <ta e="T176" id="Seg_3017" s="T175">pers</ta>
            <ta e="T177" id="Seg_3018" s="T176">v</ta>
            <ta e="T178" id="Seg_3019" s="T177">v</ta>
            <ta e="T179" id="Seg_3020" s="T178">n</ta>
            <ta e="T180" id="Seg_3021" s="T179">num</ta>
            <ta e="T181" id="Seg_3022" s="T180">n-pp</ta>
            <ta e="T182" id="Seg_3023" s="T181">pers</ta>
            <ta e="T183" id="Seg_3024" s="T182">n</ta>
            <ta e="T184" id="Seg_3025" s="T183">v</ta>
            <ta e="T185" id="Seg_3026" s="T184">pers</ta>
            <ta e="T186" id="Seg_3027" s="T185">adv</ta>
            <ta e="T187" id="Seg_3028" s="T186">v</ta>
            <ta e="T188" id="Seg_3029" s="T187">n</ta>
            <ta e="T189" id="Seg_3030" s="T188">pers</ta>
            <ta e="T190" id="Seg_3031" s="T189">pers</ta>
            <ta e="T191" id="Seg_3032" s="T190">v</ta>
            <ta e="T192" id="Seg_3033" s="T191">num</ta>
            <ta e="T193" id="Seg_3034" s="T192">num</ta>
            <ta e="T194" id="Seg_3035" s="T193">n</ta>
            <ta e="T195" id="Seg_3036" s="T194">num</ta>
            <ta e="T196" id="Seg_3037" s="T195">num</ta>
            <ta e="T197" id="Seg_3038" s="T196">n</ta>
            <ta e="T198" id="Seg_3039" s="T197">num</ta>
            <ta e="T199" id="Seg_3040" s="T198">n</ta>
            <ta e="T200" id="Seg_3041" s="T199">adv</ta>
            <ta e="T201" id="Seg_3042" s="T200">n</ta>
            <ta e="T202" id="Seg_3043" s="T201">v</ta>
            <ta e="T203" id="Seg_3044" s="T202">pers</ta>
            <ta e="T204" id="Seg_3045" s="T203">adj</ta>
            <ta e="T205" id="Seg_3046" s="T204">n</ta>
            <ta e="T206" id="Seg_3047" s="T205">v</ta>
            <ta e="T207" id="Seg_3048" s="T206">adv</ta>
            <ta e="T208" id="Seg_3049" s="T207">pers</ta>
            <ta e="T209" id="Seg_3050" s="T208">preverb</ta>
            <ta e="T210" id="Seg_3051" s="T209">v</ta>
            <ta e="T211" id="Seg_3052" s="T210">n</ta>
            <ta e="T212" id="Seg_3053" s="T211">n</ta>
            <ta e="T213" id="Seg_3054" s="T212">adv</ta>
            <ta e="T214" id="Seg_3055" s="T213">pers</ta>
            <ta e="T215" id="Seg_3056" s="T214">adv</ta>
            <ta e="T216" id="Seg_3057" s="T215">v</ta>
            <ta e="T217" id="Seg_3058" s="T216">v</ta>
            <ta e="T218" id="Seg_3059" s="T217">pers</ta>
            <ta e="T219" id="Seg_3060" s="T218">adv</ta>
            <ta e="T220" id="Seg_3061" s="T219">n</ta>
            <ta e="T221" id="Seg_3062" s="T220">v</ta>
            <ta e="T222" id="Seg_3063" s="T221">n</ta>
            <ta e="T223" id="Seg_3064" s="T222">v</ta>
            <ta e="T224" id="Seg_3065" s="T223">pers</ta>
            <ta e="T225" id="Seg_3066" s="T224">v</ta>
            <ta e="T226" id="Seg_3067" s="T225">v</ta>
            <ta e="T227" id="Seg_3068" s="T226">num</ta>
            <ta e="T228" id="Seg_3069" s="T227">v</ta>
            <ta e="T229" id="Seg_3070" s="T228">num</ta>
            <ta e="T230" id="Seg_3071" s="T229">n</ta>
            <ta e="T231" id="Seg_3072" s="T230">adv</ta>
            <ta e="T232" id="Seg_3073" s="T231">pers</ta>
            <ta e="T233" id="Seg_3074" s="T232">v</ta>
            <ta e="T234" id="Seg_3075" s="T233">pers</ta>
            <ta e="T235" id="Seg_3076" s="T234">v</ta>
            <ta e="T236" id="Seg_3077" s="T235">adv</ta>
            <ta e="T237" id="Seg_3078" s="T236">pers</ta>
            <ta e="T238" id="Seg_3079" s="T237">adv</ta>
            <ta e="T239" id="Seg_3080" s="T238">v</ta>
            <ta e="T240" id="Seg_3081" s="T239">pers</ta>
            <ta e="T241" id="Seg_3082" s="T240">adv</ta>
            <ta e="T242" id="Seg_3083" s="T241">v</ta>
            <ta e="T243" id="Seg_3084" s="T242">adv</ta>
            <ta e="T244" id="Seg_3085" s="T243">pers</ta>
            <ta e="T245" id="Seg_3086" s="T244">n</ta>
            <ta e="T246" id="Seg_3087" s="T245">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_3088" s="T0">pro.h:O</ta>
            <ta e="T3" id="Seg_3089" s="T2">0.3.h:S v:pred</ta>
            <ta e="T4" id="Seg_3090" s="T3">pro.h:O</ta>
            <ta e="T5" id="Seg_3091" s="T4">0.3.h:S v:pred</ta>
            <ta e="T6" id="Seg_3092" s="T5">pro.h:O</ta>
            <ta e="T7" id="Seg_3093" s="T6">0.3.h:S v:pred</ta>
            <ta e="T9" id="Seg_3094" s="T8">pro.h:O</ta>
            <ta e="T13" id="Seg_3095" s="T12">0.3.h:S v:pred</ta>
            <ta e="T15" id="Seg_3096" s="T14">pro.h:O</ta>
            <ta e="T16" id="Seg_3097" s="T15">0.3.h:S v:pred</ta>
            <ta e="T18" id="Seg_3098" s="T17">pro.h:S</ta>
            <ta e="T20" id="Seg_3099" s="T19">v:pred</ta>
            <ta e="T23" id="Seg_3100" s="T22">pro.h:O</ta>
            <ta e="T25" id="Seg_3101" s="T24">0.3.h:S v:pred</ta>
            <ta e="T27" id="Seg_3102" s="T26">pro.h:S</ta>
            <ta e="T29" id="Seg_3103" s="T28">v:pred</ta>
            <ta e="T33" id="Seg_3104" s="T32">pro.h:S</ta>
            <ta e="T35" id="Seg_3105" s="T34">v:pred</ta>
            <ta e="T36" id="Seg_3106" s="T35">v:pred</ta>
            <ta e="T37" id="Seg_3107" s="T36">pro.h:O</ta>
            <ta e="T38" id="Seg_3108" s="T37">pro.h:S</ta>
            <ta e="T40" id="Seg_3109" s="T39">v:pred</ta>
            <ta e="T44" id="Seg_3110" s="T43">np:S</ta>
            <ta e="T45" id="Seg_3111" s="T44">v:pred</ta>
            <ta e="T46" id="Seg_3112" s="T45">pro.h:S</ta>
            <ta e="T51" id="Seg_3113" s="T50">v:pred</ta>
            <ta e="T52" id="Seg_3114" s="T51">pro.h:O</ta>
            <ta e="T54" id="Seg_3115" s="T53">0.3.h:S v:pred</ta>
            <ta e="T57" id="Seg_3116" s="T56">0.3.h:S v:pred</ta>
            <ta e="T58" id="Seg_3117" s="T57">pro.h:O</ta>
            <ta e="T60" id="Seg_3118" s="T59">0.3.h:S v:pred</ta>
            <ta e="T61" id="Seg_3119" s="T60">pro.h:S</ta>
            <ta e="T62" id="Seg_3120" s="T61">v:pred</ta>
            <ta e="T65" id="Seg_3121" s="T64">0.1.h:S v:pred</ta>
            <ta e="T67" id="Seg_3122" s="T66">0.3.h:S v:pred</ta>
            <ta e="T68" id="Seg_3123" s="T67">pro.h:O</ta>
            <ta e="T69" id="Seg_3124" s="T68">0.1.h:S v:pred</ta>
            <ta e="T70" id="Seg_3125" s="T69">pro.h:S</ta>
            <ta e="T72" id="Seg_3126" s="T71">v:pred</ta>
            <ta e="T74" id="Seg_3127" s="T73">pro.h:O</ta>
            <ta e="T75" id="Seg_3128" s="T74">0.3.h:S v:pred</ta>
            <ta e="T78" id="Seg_3129" s="T75">s:purp</ta>
            <ta e="T79" id="Seg_3130" s="T78">pro.h:S</ta>
            <ta e="T81" id="Seg_3131" s="T80">v:pred</ta>
            <ta e="T83" id="Seg_3132" s="T81">s:temp</ta>
            <ta e="T84" id="Seg_3133" s="T83">0.1.h:S v:pred</ta>
            <ta e="T86" id="Seg_3134" s="T85">np.h:S</ta>
            <ta e="T88" id="Seg_3135" s="T87">v:pred</ta>
            <ta e="T90" id="Seg_3136" s="T89">0.3.h:S v:pred</ta>
            <ta e="T91" id="Seg_3137" s="T90">pro.h:S</ta>
            <ta e="T93" id="Seg_3138" s="T92">v:pred</ta>
            <ta e="T96" id="Seg_3139" s="T95">0.1.h:S v:pred</ta>
            <ta e="T97" id="Seg_3140" s="T96">pro.h:S</ta>
            <ta e="T98" id="Seg_3141" s="T97">v:pred</ta>
            <ta e="T99" id="Seg_3142" s="T98">0.1.h:S v:pred 0.3.h:O</ta>
            <ta e="T102" id="Seg_3143" s="T101">0.1.h:S v:pred</ta>
            <ta e="T104" id="Seg_3144" s="T103">np.h:S</ta>
            <ta e="T105" id="Seg_3145" s="T104">v:pred</ta>
            <ta e="T106" id="Seg_3146" s="T105">pro.h:S</ta>
            <ta e="T107" id="Seg_3147" s="T106">v:pred</ta>
            <ta e="T108" id="Seg_3148" s="T107">pro.h:S</ta>
            <ta e="T109" id="Seg_3149" s="T108">v:pred</ta>
            <ta e="T110" id="Seg_3150" s="T109">np.h:O</ta>
            <ta e="T113" id="Seg_3151" s="T112">pro.h:S</ta>
            <ta e="T115" id="Seg_3152" s="T114">v:pred</ta>
            <ta e="T117" id="Seg_3153" s="T116">v:O</ta>
            <ta e="T118" id="Seg_3154" s="T117">np.h:S</ta>
            <ta e="T120" id="Seg_3155" s="T119">v:pred</ta>
            <ta e="T121" id="Seg_3156" s="T120">s:purp</ta>
            <ta e="T123" id="Seg_3157" s="T122">np.h:S</ta>
            <ta e="T124" id="Seg_3158" s="T123">v:pred</ta>
            <ta e="T125" id="Seg_3159" s="T124">pro.h:O</ta>
            <ta e="T128" id="Seg_3160" s="T127">0.3.h:S v:pred</ta>
            <ta e="T129" id="Seg_3161" s="T128">pro.h:O</ta>
            <ta e="T130" id="Seg_3162" s="T129">0.3.h:S v:pred</ta>
            <ta e="T131" id="Seg_3163" s="T130">pro.h:O</ta>
            <ta e="T132" id="Seg_3164" s="T131">np.h:S</ta>
            <ta e="T134" id="Seg_3165" s="T133">v:pred</ta>
            <ta e="T136" id="Seg_3166" s="T135">pro.h:S</ta>
            <ta e="T138" id="Seg_3167" s="T137">v:pred</ta>
            <ta e="T140" id="Seg_3168" s="T139">0.3.h:S v:pred</ta>
            <ta e="T141" id="Seg_3169" s="T140">np:O</ta>
            <ta e="T144" id="Seg_3170" s="T143">0.2.h:S v:pred</ta>
            <ta e="T145" id="Seg_3171" s="T144">pro.h:S</ta>
            <ta e="T146" id="Seg_3172" s="T145">v:pred</ta>
            <ta e="T148" id="Seg_3173" s="T147">v:pred</ta>
            <ta e="T149" id="Seg_3174" s="T148">pro.h:S</ta>
            <ta e="T151" id="Seg_3175" s="T150">v:pred</ta>
            <ta e="T152" id="Seg_3176" s="T151">np.h:S</ta>
            <ta e="T154" id="Seg_3177" s="T153">0.3.h:S v:pred</ta>
            <ta e="T155" id="Seg_3178" s="T154">v:O</ta>
            <ta e="T157" id="Seg_3179" s="T156">0.3.h:S v:pred</ta>
            <ta e="T159" id="Seg_3180" s="T158">pro.h:S</ta>
            <ta e="T160" id="Seg_3181" s="T159">np:O</ta>
            <ta e="T162" id="Seg_3182" s="T161">v:pred</ta>
            <ta e="T164" id="Seg_3183" s="T163">0.3.h:S v:pred</ta>
            <ta e="T165" id="Seg_3184" s="T164">pro.h:O</ta>
            <ta e="T170" id="Seg_3185" s="T169">0.3.h:S v:pred</ta>
            <ta e="T172" id="Seg_3186" s="T171">0.3.h:S v:pred</ta>
            <ta e="T173" id="Seg_3187" s="T172">np.h:O</ta>
            <ta e="T174" id="Seg_3188" s="T173">0.3.h:S v:pred</ta>
            <ta e="T176" id="Seg_3189" s="T175">pro.h:O</ta>
            <ta e="T177" id="Seg_3190" s="T176">0.3.h:S v:pred</ta>
            <ta e="T178" id="Seg_3191" s="T177">0.3.h:S v:pred</ta>
            <ta e="T182" id="Seg_3192" s="T181">pro.h:S</ta>
            <ta e="T184" id="Seg_3193" s="T183">v:pred</ta>
            <ta e="T185" id="Seg_3194" s="T184">pro.h:O</ta>
            <ta e="T187" id="Seg_3195" s="T186">v:pred</ta>
            <ta e="T188" id="Seg_3196" s="T187">np.h:S</ta>
            <ta e="T189" id="Seg_3197" s="T188">pro.h:O</ta>
            <ta e="T190" id="Seg_3198" s="T189">pro.h:S</ta>
            <ta e="T191" id="Seg_3199" s="T190">v:pred</ta>
            <ta e="T202" id="Seg_3200" s="T201">0.3.h:S v:pred</ta>
            <ta e="T203" id="Seg_3201" s="T202">pro.h:O</ta>
            <ta e="T206" id="Seg_3202" s="T205">0.3.h:S v:pred</ta>
            <ta e="T210" id="Seg_3203" s="T209">0.3.h:S v:pred</ta>
            <ta e="T211" id="Seg_3204" s="T210">np:O</ta>
            <ta e="T214" id="Seg_3205" s="T213">pro.h:O</ta>
            <ta e="T216" id="Seg_3206" s="T215">v:O</ta>
            <ta e="T217" id="Seg_3207" s="T216">0.3.h:S v:pred</ta>
            <ta e="T218" id="Seg_3208" s="T217">pro.h:O</ta>
            <ta e="T221" id="Seg_3209" s="T220">0.3.h:S v:pred</ta>
            <ta e="T223" id="Seg_3210" s="T222">0.3.h:S v:pred</ta>
            <ta e="T224" id="Seg_3211" s="T223">pro.h:S</ta>
            <ta e="T225" id="Seg_3212" s="T224">v:pred</ta>
            <ta e="T232" id="Seg_3213" s="T231">pro.h:O</ta>
            <ta e="T233" id="Seg_3214" s="T232">0.3.h:S v:pred</ta>
            <ta e="T234" id="Seg_3215" s="T233">pro.h:S</ta>
            <ta e="T235" id="Seg_3216" s="T234">v:pred</ta>
            <ta e="T237" id="Seg_3217" s="T236">pro.h:O</ta>
            <ta e="T239" id="Seg_3218" s="T238">0.3.h:S v:pred</ta>
            <ta e="T240" id="Seg_3219" s="T239">pro.h:S</ta>
            <ta e="T242" id="Seg_3220" s="T241">v:pred</ta>
            <ta e="T244" id="Seg_3221" s="T243">pro.h:S</ta>
            <ta e="T246" id="Seg_3222" s="T245">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_3223" s="T0">pro.h:Th</ta>
            <ta e="T2" id="Seg_3224" s="T1">np:G</ta>
            <ta e="T3" id="Seg_3225" s="T2">0.3.h:A</ta>
            <ta e="T4" id="Seg_3226" s="T3">pro.h:Th</ta>
            <ta e="T5" id="Seg_3227" s="T4">0.3.h:A</ta>
            <ta e="T6" id="Seg_3228" s="T5">pro.h:Th</ta>
            <ta e="T7" id="Seg_3229" s="T6">0.3.h:A</ta>
            <ta e="T8" id="Seg_3230" s="T7">np:L</ta>
            <ta e="T9" id="Seg_3231" s="T8">pro.h:Th</ta>
            <ta e="T10" id="Seg_3232" s="T9">adv:L</ta>
            <ta e="T12" id="Seg_3233" s="T11">np:Time</ta>
            <ta e="T13" id="Seg_3234" s="T12">0.3.h:A</ta>
            <ta e="T14" id="Seg_3235" s="T13">adv:So</ta>
            <ta e="T15" id="Seg_3236" s="T14">pro.h:Th</ta>
            <ta e="T16" id="Seg_3237" s="T15">0.3.h:A</ta>
            <ta e="T17" id="Seg_3238" s="T16">np:G</ta>
            <ta e="T18" id="Seg_3239" s="T17">pro.h:A</ta>
            <ta e="T19" id="Seg_3240" s="T18">adv:L</ta>
            <ta e="T22" id="Seg_3241" s="T21">np:Time</ta>
            <ta e="T23" id="Seg_3242" s="T22">pro.h:Th</ta>
            <ta e="T24" id="Seg_3243" s="T23">adv:G</ta>
            <ta e="T25" id="Seg_3244" s="T24">0.3.h:A</ta>
            <ta e="T26" id="Seg_3245" s="T25">np:G</ta>
            <ta e="T27" id="Seg_3246" s="T26">pro.h:Th</ta>
            <ta e="T28" id="Seg_3247" s="T27">adv:L</ta>
            <ta e="T30" id="Seg_3248" s="T29">np:L</ta>
            <ta e="T32" id="Seg_3249" s="T31">np:Time</ta>
            <ta e="T33" id="Seg_3250" s="T32">pro.h:A</ta>
            <ta e="T34" id="Seg_3251" s="T33">adv:L</ta>
            <ta e="T38" id="Seg_3252" s="T37">pro.h:A</ta>
            <ta e="T42" id="Seg_3253" s="T41">np:Time</ta>
            <ta e="T43" id="Seg_3254" s="T42">np:Path 0.1.h:Poss</ta>
            <ta e="T44" id="Seg_3255" s="T43">np:A</ta>
            <ta e="T46" id="Seg_3256" s="T45">pro.h:Th</ta>
            <ta e="T48" id="Seg_3257" s="T47">np:Time</ta>
            <ta e="T50" id="Seg_3258" s="T49">np.h:So</ta>
            <ta e="T52" id="Seg_3259" s="T51">pro.h:Th</ta>
            <ta e="T54" id="Seg_3260" s="T53">0.3.h:A</ta>
            <ta e="T56" id="Seg_3261" s="T55">np:Time</ta>
            <ta e="T57" id="Seg_3262" s="T56">0.3.h:A</ta>
            <ta e="T58" id="Seg_3263" s="T57">pro.h:Th</ta>
            <ta e="T59" id="Seg_3264" s="T58">adv:G</ta>
            <ta e="T60" id="Seg_3265" s="T59">0.3.h:A</ta>
            <ta e="T61" id="Seg_3266" s="T60">pro.h:A</ta>
            <ta e="T63" id="Seg_3267" s="T62">pp:Path</ta>
            <ta e="T65" id="Seg_3268" s="T64">0.1.h:A</ta>
            <ta e="T66" id="Seg_3269" s="T65">adv:G</ta>
            <ta e="T67" id="Seg_3270" s="T66">0.3.h:A</ta>
            <ta e="T68" id="Seg_3271" s="T67">pro.h:Th</ta>
            <ta e="T69" id="Seg_3272" s="T68">0.1.h:A</ta>
            <ta e="T70" id="Seg_3273" s="T69">pro.h:A</ta>
            <ta e="T71" id="Seg_3274" s="T70">adv:G</ta>
            <ta e="T73" id="Seg_3275" s="T72">adv:G</ta>
            <ta e="T74" id="Seg_3276" s="T73">pro.h:Th</ta>
            <ta e="T75" id="Seg_3277" s="T74">0.3.h:A</ta>
            <ta e="T76" id="Seg_3278" s="T75">np:Th</ta>
            <ta e="T79" id="Seg_3279" s="T78">pro.h:A</ta>
            <ta e="T80" id="Seg_3280" s="T79">adv:G</ta>
            <ta e="T82" id="Seg_3281" s="T81">adv:G</ta>
            <ta e="T84" id="Seg_3282" s="T83">0.1.h:A</ta>
            <ta e="T85" id="Seg_3283" s="T84">np.h:R</ta>
            <ta e="T86" id="Seg_3284" s="T85">np.h:Th</ta>
            <ta e="T87" id="Seg_3285" s="T86">adv:L</ta>
            <ta e="T89" id="Seg_3286" s="T88">np:L</ta>
            <ta e="T90" id="Seg_3287" s="T89">0.3.h:Th</ta>
            <ta e="T91" id="Seg_3288" s="T90">pro.h:A</ta>
            <ta e="T92" id="Seg_3289" s="T91">pro.h:R</ta>
            <ta e="T96" id="Seg_3290" s="T95">0.1.h:A</ta>
            <ta e="T97" id="Seg_3291" s="T96">pro.h:A</ta>
            <ta e="T99" id="Seg_3292" s="T98">0.1.h:A 0.3.h:Th</ta>
            <ta e="T102" id="Seg_3293" s="T101">0.1.h:A</ta>
            <ta e="T103" id="Seg_3294" s="T102">np:L</ta>
            <ta e="T104" id="Seg_3295" s="T103">np.h:A</ta>
            <ta e="T106" id="Seg_3296" s="T105">pro.h:A</ta>
            <ta e="T108" id="Seg_3297" s="T107">pro.h:A</ta>
            <ta e="T110" id="Seg_3298" s="T109">np.h:Th</ta>
            <ta e="T112" id="Seg_3299" s="T111">np:G</ta>
            <ta e="T113" id="Seg_3300" s="T112">pro.h:E</ta>
            <ta e="T116" id="Seg_3301" s="T115">np.h:Th</ta>
            <ta e="T117" id="Seg_3302" s="T116">v:Th</ta>
            <ta e="T118" id="Seg_3303" s="T117">np.h:A</ta>
            <ta e="T122" id="Seg_3304" s="T121">adv:So</ta>
            <ta e="T123" id="Seg_3305" s="T122">np.h:E</ta>
            <ta e="T125" id="Seg_3306" s="T124">pro.h:Th</ta>
            <ta e="T127" id="Seg_3307" s="T126">adv:So</ta>
            <ta e="T128" id="Seg_3308" s="T127">0.3.h:A</ta>
            <ta e="T129" id="Seg_3309" s="T128">pro.h:P</ta>
            <ta e="T130" id="Seg_3310" s="T129">0.3.h:A</ta>
            <ta e="T131" id="Seg_3311" s="T130">pro.h:P</ta>
            <ta e="T132" id="Seg_3312" s="T131">np.h:A 0.1.h:Poss</ta>
            <ta e="T136" id="Seg_3313" s="T135">pro.h:A</ta>
            <ta e="T137" id="Seg_3314" s="T136">np:Ins 0.1.h:Poss</ta>
            <ta e="T139" id="Seg_3315" s="T138">np:Ins 0.3.h:Poss</ta>
            <ta e="T140" id="Seg_3316" s="T139">0.3.h:A</ta>
            <ta e="T141" id="Seg_3317" s="T140">np:Th</ta>
            <ta e="T142" id="Seg_3318" s="T141">adv:G</ta>
            <ta e="T144" id="Seg_3319" s="T143">0.2.h:A</ta>
            <ta e="T145" id="Seg_3320" s="T144">pro.h:E</ta>
            <ta e="T147" id="Seg_3321" s="T146">adv:G</ta>
            <ta e="T149" id="Seg_3322" s="T148">pro.h:A</ta>
            <ta e="T150" id="Seg_3323" s="T149">adv:So</ta>
            <ta e="T152" id="Seg_3324" s="T151">np.h:A</ta>
            <ta e="T153" id="Seg_3325" s="T152">pro.h:G</ta>
            <ta e="T154" id="Seg_3326" s="T153">0.3.h:A</ta>
            <ta e="T155" id="Seg_3327" s="T154">v:Th</ta>
            <ta e="T157" id="Seg_3328" s="T156">0.3.h:A</ta>
            <ta e="T158" id="Seg_3329" s="T157">pro.h:G</ta>
            <ta e="T159" id="Seg_3330" s="T158">pro.h:A</ta>
            <ta e="T160" id="Seg_3331" s="T159">np:Th</ta>
            <ta e="T164" id="Seg_3332" s="T163">0.3.h:A</ta>
            <ta e="T165" id="Seg_3333" s="T164">pro.h:Th</ta>
            <ta e="T166" id="Seg_3334" s="T165">adv:So</ta>
            <ta e="T170" id="Seg_3335" s="T169">0.3.h:A</ta>
            <ta e="T171" id="Seg_3336" s="T170">np:Ins</ta>
            <ta e="T172" id="Seg_3337" s="T171">0.3.h:A</ta>
            <ta e="T173" id="Seg_3338" s="T172">np.h:P</ta>
            <ta e="T174" id="Seg_3339" s="T173">0.3.h:A</ta>
            <ta e="T176" id="Seg_3340" s="T175">pro.h:Th</ta>
            <ta e="T177" id="Seg_3341" s="T176">0.3.h:A</ta>
            <ta e="T178" id="Seg_3342" s="T177">0.3.h:A</ta>
            <ta e="T179" id="Seg_3343" s="T178">np:G</ta>
            <ta e="T181" id="Seg_3344" s="T180">pp:Time</ta>
            <ta e="T182" id="Seg_3345" s="T181">pro.h:Th</ta>
            <ta e="T183" id="Seg_3346" s="T182">np:L</ta>
            <ta e="T185" id="Seg_3347" s="T184">pro.h:Th</ta>
            <ta e="T186" id="Seg_3348" s="T185">adv:So</ta>
            <ta e="T188" id="Seg_3349" s="T187">np.h:A</ta>
            <ta e="T189" id="Seg_3350" s="T188">pro.h:Th</ta>
            <ta e="T190" id="Seg_3351" s="T189">pro.h:A</ta>
            <ta e="T194" id="Seg_3352" s="T193">np:Path</ta>
            <ta e="T200" id="Seg_3353" s="T199">adv:Time</ta>
            <ta e="T201" id="Seg_3354" s="T200">np:Ins</ta>
            <ta e="T202" id="Seg_3355" s="T201">0.3.h:A</ta>
            <ta e="T203" id="Seg_3356" s="T202">pro.h:Th</ta>
            <ta e="T205" id="Seg_3357" s="T204">np:G</ta>
            <ta e="T206" id="Seg_3358" s="T205">0.3.h:A</ta>
            <ta e="T207" id="Seg_3359" s="T206">adv:L</ta>
            <ta e="T208" id="Seg_3360" s="T207">pro.h:B</ta>
            <ta e="T210" id="Seg_3361" s="T209">0.3.h:A</ta>
            <ta e="T211" id="Seg_3362" s="T210">np:Th</ta>
            <ta e="T212" id="Seg_3363" s="T211">np:So</ta>
            <ta e="T213" id="Seg_3364" s="T212">adv:So</ta>
            <ta e="T214" id="Seg_3365" s="T213">pro.h:Th</ta>
            <ta e="T215" id="Seg_3366" s="T214">adv:G</ta>
            <ta e="T216" id="Seg_3367" s="T215">v:Th</ta>
            <ta e="T217" id="Seg_3368" s="T216">0.3.h:A</ta>
            <ta e="T218" id="Seg_3369" s="T217">pro.h:Th</ta>
            <ta e="T219" id="Seg_3370" s="T218">adv:So</ta>
            <ta e="T220" id="Seg_3371" s="T219">np:G</ta>
            <ta e="T221" id="Seg_3372" s="T220">0.3.h:A</ta>
            <ta e="T222" id="Seg_3373" s="T221">np:Ins</ta>
            <ta e="T223" id="Seg_3374" s="T222">0.3.h:A</ta>
            <ta e="T224" id="Seg_3375" s="T223">pro.h:Th</ta>
            <ta e="T226" id="Seg_3376" s="T225">np:L</ta>
            <ta e="T228" id="Seg_3377" s="T227">np:Time</ta>
            <ta e="T230" id="Seg_3378" s="T229">np:Time</ta>
            <ta e="T232" id="Seg_3379" s="T231">pro.h:Th</ta>
            <ta e="T233" id="Seg_3380" s="T232">0.3.h:A</ta>
            <ta e="T234" id="Seg_3381" s="T233">pro.h:A</ta>
            <ta e="T236" id="Seg_3382" s="T235">adv:So</ta>
            <ta e="T237" id="Seg_3383" s="T236">pro.h:Th</ta>
            <ta e="T238" id="Seg_3384" s="T237">adv:G</ta>
            <ta e="T239" id="Seg_3385" s="T238">0.3.h:A</ta>
            <ta e="T240" id="Seg_3386" s="T239">pro.h:A</ta>
            <ta e="T241" id="Seg_3387" s="T240">adv:G</ta>
            <ta e="T243" id="Seg_3388" s="T242">adv:Time</ta>
            <ta e="T244" id="Seg_3389" s="T243">pro.h:Th</ta>
            <ta e="T245" id="Seg_3390" s="T244">np:L</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_3391" s="T1">RUS:cult</ta>
            <ta e="T8" id="Seg_3392" s="T7">RUS:cult</ta>
            <ta e="T17" id="Seg_3393" s="T16">RUS:cult</ta>
            <ta e="T26" id="Seg_3394" s="T25">RUS:cult</ta>
            <ta e="T30" id="Seg_3395" s="T29">RUS:cult</ta>
            <ta e="T35" id="Seg_3396" s="T34">RUS:core</ta>
            <ta e="T41" id="Seg_3397" s="T40">RUS:cult</ta>
            <ta e="T44" id="Seg_3398" s="T43">RUS:cult</ta>
            <ta e="T50" id="Seg_3399" s="T49">RUS:cult</ta>
            <ta e="T54" id="Seg_3400" s="T53">RUS:cult</ta>
            <ta e="T57" id="Seg_3401" s="T56">RUS:cult</ta>
            <ta e="T64" id="Seg_3402" s="T63">RUS:core</ta>
            <ta e="T85" id="Seg_3403" s="T84">RUS:cult</ta>
            <ta e="T86" id="Seg_3404" s="T85">RUS:cult</ta>
            <ta e="T94" id="Seg_3405" s="T93">RUS:gram</ta>
            <ta e="T100" id="Seg_3406" s="T99">RUS:gram</ta>
            <ta e="T110" id="Seg_3407" s="T109">RUS:cult</ta>
            <ta e="T112" id="Seg_3408" s="T111">RUS:cult</ta>
            <ta e="T116" id="Seg_3409" s="T115">RUS:cult</ta>
            <ta e="T118" id="Seg_3410" s="T117">RUS:cult</ta>
            <ta e="T123" id="Seg_3411" s="T122">RUS:cult</ta>
            <ta e="T126" id="Seg_3412" s="T125">RUS:gram</ta>
            <ta e="T130" id="Seg_3413" s="T129">RUS:core</ta>
            <ta e="T132" id="Seg_3414" s="T131">RUS:cult</ta>
            <ta e="T152" id="Seg_3415" s="T151">RUS:cult</ta>
            <ta e="T156" id="Seg_3416" s="T155">RUS:core</ta>
            <ta e="T157" id="Seg_3417" s="T156">RUS:core</ta>
            <ta e="T160" id="Seg_3418" s="T159">RUS:cult</ta>
            <ta e="T167" id="Seg_3419" s="T166">RUS:gram</ta>
            <ta e="T169" id="Seg_3420" s="T168">RUS:disc</ta>
            <ta e="T171" id="Seg_3421" s="T170">RUS:cult</ta>
            <ta e="T173" id="Seg_3422" s="T172">RUS:cult</ta>
            <ta e="T175" id="Seg_3423" s="T174">RUS:core</ta>
            <ta e="T179" id="Seg_3424" s="T178">RUS:cult</ta>
            <ta e="T181" id="Seg_3425" s="T180">RUS:cult</ta>
            <ta e="T183" id="Seg_3426" s="T182">RUS:cult</ta>
            <ta e="T188" id="Seg_3427" s="T187">RUS:cult</ta>
            <ta e="T200" id="Seg_3428" s="T199">RUS:core</ta>
            <ta e="T204" id="Seg_3429" s="T203">RUS:core</ta>
            <ta e="T211" id="Seg_3430" s="T210">RUS:cult</ta>
            <ta e="T220" id="Seg_3431" s="T219">RUS:cult</ta>
            <ta e="T222" id="Seg_3432" s="T221">RUS:cult</ta>
            <ta e="T226" id="Seg_3433" s="T225">RUS:cult</ta>
            <ta e="T233" id="Seg_3434" s="T232">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T8" id="Seg_3435" s="T7">Vsub</ta>
            <ta e="T50" id="Seg_3436" s="T49">Csub</ta>
            <ta e="T54" id="Seg_3437" s="T53">finVins</ta>
            <ta e="T57" id="Seg_3438" s="T56">finVins</ta>
            <ta e="T64" id="Seg_3439" s="T63">Csub</ta>
            <ta e="T86" id="Seg_3440" s="T85">Csub</ta>
            <ta e="T112" id="Seg_3441" s="T111">Csub</ta>
            <ta e="T152" id="Seg_3442" s="T151">Csub</ta>
            <ta e="T204" id="Seg_3443" s="T203">Vsub finCdel</ta>
            <ta e="T211" id="Seg_3444" s="T210">finCdel medVdel</ta>
            <ta e="T220" id="Seg_3445" s="T219">Vsub</ta>
            <ta e="T226" id="Seg_3446" s="T225">Csub Csub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T2" id="Seg_3447" s="T1">dir:infl</ta>
            <ta e="T8" id="Seg_3448" s="T7">dir:infl</ta>
            <ta e="T17" id="Seg_3449" s="T16">dir:infl</ta>
            <ta e="T26" id="Seg_3450" s="T25">dir:infl</ta>
            <ta e="T30" id="Seg_3451" s="T29">dir:infl</ta>
            <ta e="T35" id="Seg_3452" s="T34">parad:infl</ta>
            <ta e="T41" id="Seg_3453" s="T40">dir:infl</ta>
            <ta e="T44" id="Seg_3454" s="T43">dir:infl</ta>
            <ta e="T50" id="Seg_3455" s="T49">dir:infl</ta>
            <ta e="T54" id="Seg_3456" s="T53">parad:infl</ta>
            <ta e="T57" id="Seg_3457" s="T56">parad:infl</ta>
            <ta e="T64" id="Seg_3458" s="T63">dir:bare</ta>
            <ta e="T85" id="Seg_3459" s="T84">dir:infl</ta>
            <ta e="T94" id="Seg_3460" s="T93">dir:bare</ta>
            <ta e="T100" id="Seg_3461" s="T99">dir:bare</ta>
            <ta e="T110" id="Seg_3462" s="T109">dir:infl</ta>
            <ta e="T112" id="Seg_3463" s="T111">dir:infl</ta>
            <ta e="T116" id="Seg_3464" s="T115">dir:infl</ta>
            <ta e="T118" id="Seg_3465" s="T117">dir:bare</ta>
            <ta e="T123" id="Seg_3466" s="T122">dir:bare</ta>
            <ta e="T126" id="Seg_3467" s="T125">dir:bare</ta>
            <ta e="T130" id="Seg_3468" s="T129">dir:infl</ta>
            <ta e="T132" id="Seg_3469" s="T131">dir:infl</ta>
            <ta e="T152" id="Seg_3470" s="T151">dir:bare</ta>
            <ta e="T157" id="Seg_3471" s="T156">parad:bare</ta>
            <ta e="T160" id="Seg_3472" s="T159">dir:infl</ta>
            <ta e="T167" id="Seg_3473" s="T166">dir:bare</ta>
            <ta e="T169" id="Seg_3474" s="T168">dir:bare</ta>
            <ta e="T171" id="Seg_3475" s="T170">dir:infl</ta>
            <ta e="T173" id="Seg_3476" s="T172">dir:infl</ta>
            <ta e="T179" id="Seg_3477" s="T178">dir:infl</ta>
            <ta e="T181" id="Seg_3478" s="T180">dir:infl</ta>
            <ta e="T183" id="Seg_3479" s="T182">dir:infl</ta>
            <ta e="T188" id="Seg_3480" s="T187">dir:bare</ta>
            <ta e="T204" id="Seg_3481" s="T203">indir:bare</ta>
            <ta e="T211" id="Seg_3482" s="T210">dir:infl</ta>
            <ta e="T220" id="Seg_3483" s="T219">dir:infl</ta>
            <ta e="T222" id="Seg_3484" s="T221">dir:infl</ta>
            <ta e="T226" id="Seg_3485" s="T225">dir:bare</ta>
            <ta e="T233" id="Seg_3486" s="T232">parad:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T157" id="Seg_3487" s="T155">RUS:int.ins</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_3488" s="T0">Меня в армию взяли.</ta>
            <ta e="T8" id="Seg_3489" s="T3">Меня увезли, меня учили в Асино.</ta>
            <ta e="T13" id="Seg_3490" s="T8">Там меня три месяца учили.</ta>
            <ta e="T17" id="Seg_3491" s="T13">Оттуда меня увезли в Кемерово.</ta>
            <ta e="T22" id="Seg_3492" s="T17">Я там работал два года.</ta>
            <ta e="T26" id="Seg_3493" s="T22">Меня туда взяли в армию.</ta>
            <ta e="T32" id="Seg_3494" s="T26">Я там был в армии шесть месяцев.</ta>
            <ta e="T36" id="Seg_3495" s="T32">Мы там воевали.</ta>
            <ta e="T45" id="Seg_3496" s="T36">Меня, я первый раз пошел, танк, смял, ночью по ноге моей танк прошел.</ta>
            <ta e="T51" id="Seg_3497" s="T45">Я на три дня от своих товарищей отстал.</ta>
            <ta e="T57" id="Seg_3498" s="T51">Меня там лечили, три дня лечили.</ta>
            <ta e="T60" id="Seg_3499" s="T57">Меня вперед отправили.</ta>
            <ta e="T65" id="Seg_3500" s="T60">Я воевал, две реки перешел(?).</ta>
            <ta e="T68" id="Seg_3501" s="T65">Меня вперед отправили.</ta>
            <ta e="T72" id="Seg_3502" s="T68">Я переправился (через реку), я вперед иду.</ta>
            <ta e="T78" id="Seg_3503" s="T72">Вперед меня отправили, впереди местность проверить.</ta>
            <ta e="T81" id="Seg_3504" s="T78">Я туда пошел.</ta>
            <ta e="T90" id="Seg_3505" s="T81">Придя обратно, сказал командиру: “Немцы здесь сидят, за речкой”.</ta>
            <ta e="T96" id="Seg_3506" s="T90">Он мне сказал: “Давай быстро пойдем”.</ta>
            <ta e="T102" id="Seg_3507" s="T96">Я отправился, догнал (его), скорей пошли.</ta>
            <ta e="T105" id="Seg_3508" s="T102">Позади солдаты идут.</ta>
            <ta e="T107" id="Seg_3509" s="T105">Мы торопимся.</ta>
            <ta e="T112" id="Seg_3510" s="T107">Я привел командира к речке Томь.</ta>
            <ta e="T117" id="Seg_3511" s="T112">Мы с ним хотели на немцев взглянуть.</ta>
            <ta e="T121" id="Seg_3512" s="T117">Командир встал, чтобы взглянуть.</ta>
            <ta e="T131" id="Seg_3513" s="T121">Оттуда немец увидел нас и оттуда стрелял в нас, он нас обоих ранил.</ta>
            <ta e="T134" id="Seg_3514" s="T131">Мой командир не дышал.</ta>
            <ta e="T144" id="Seg_3515" s="T134">Потом я рукой показал (знак сделал), он рукой махнул: голову вверх не поднимай!</ta>
            <ta e="T146" id="Seg_3516" s="T144">Он понял.</ta>
            <ta e="T149" id="Seg_3517" s="T146">Он крикнул: “Назад!”</ta>
            <ta e="T152" id="Seg_3518" s="T149">Оттуда идет фельдшер.</ta>
            <ta e="T154" id="Seg_3519" s="T152">Он пришел к нам.</ta>
            <ta e="T158" id="Seg_3520" s="T154">Он не может дойти к нам.</ta>
            <ta e="T164" id="Seg_3521" s="T158">Он обмотку разматывает, (размотал).</ta>
            <ta e="T168" id="Seg_3522" s="T164">Нас оттуда давай подтаскивать.</ta>
            <ta e="T172" id="Seg_3523" s="T168">Вот, вытащил, обмоткой вытащил.</ta>
            <ta e="T175" id="Seg_3524" s="T172">Командира убили совсем.</ta>
            <ta e="T179" id="Seg_3525" s="T175">Меня перевязали, положили в окоп.</ta>
            <ta e="T184" id="Seg_3526" s="T179">Я шесть часов лежал в окопе.</ta>
            <ta e="T188" id="Seg_3527" s="T184">Меня оттуда потащил санинструктор.</ta>
            <ta e="T199" id="Seg_3528" s="T188">Меня он тащил пятьдесят саженей.</ta>
            <ta e="T202" id="Seg_3529" s="T199">Потом на лошади везли.</ta>
            <ta e="T206" id="Seg_3530" s="T202">Меня в первую деревню привезли.</ta>
            <ta e="T212" id="Seg_3531" s="T206">Тут у меня выдернули осколки из ноги.</ta>
            <ta e="T217" id="Seg_3532" s="T212">Оттуда меня обратно везти начали.</ta>
            <ta e="T223" id="Seg_3533" s="T217">Меня оттуда на линию привезли на машине.</ta>
            <ta e="T230" id="Seg_3534" s="T223">Я лежал в больнице два года четыре месяца.</ta>
            <ta e="T233" id="Seg_3535" s="T230">Меня вылечили.</ta>
            <ta e="T235" id="Seg_3536" s="T233">Я ходил.</ta>
            <ta e="T239" id="Seg_3537" s="T235">Оттуда меня обратно отправили.</ta>
            <ta e="T242" id="Seg_3538" s="T239">Я домой приехал.</ta>
            <ta e="T246" id="Seg_3539" s="T242">После этого я дома живу.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_3540" s="T0">I was taken to the army.</ta>
            <ta e="T8" id="Seg_3541" s="T3">I was taken, I was trained in Asino.</ta>
            <ta e="T13" id="Seg_3542" s="T8">There I was trained for three months.</ta>
            <ta e="T17" id="Seg_3543" s="T13">From there I was taken to Kemerovo.</ta>
            <ta e="T22" id="Seg_3544" s="T17">I worked there for two years.</ta>
            <ta e="T26" id="Seg_3545" s="T22">I was taken to the army.</ta>
            <ta e="T32" id="Seg_3546" s="T26">I was there in the army for six months.</ta>
            <ta e="T36" id="Seg_3547" s="T32">We fought there.</ta>
            <ta e="T45" id="Seg_3548" s="T36">I went for the first time, by a tank, a tank passed over my leg at night.</ta>
            <ta e="T51" id="Seg_3549" s="T45">I stayed behind my companions for three days.</ta>
            <ta e="T57" id="Seg_3550" s="T51">I was treated there, I was treated during three days.</ta>
            <ta e="T60" id="Seg_3551" s="T57">I was sent forward.</ta>
            <ta e="T65" id="Seg_3552" s="T60">I fought, I crossed(?) two rivers.</ta>
            <ta e="T68" id="Seg_3553" s="T65">I was sent forward.</ta>
            <ta e="T72" id="Seg_3554" s="T68">I crossed (a river), I went forward.</ta>
            <ta e="T78" id="Seg_3555" s="T72">I was sent forward to inspect the area.</ta>
            <ta e="T81" id="Seg_3556" s="T78">I went there.</ta>
            <ta e="T90" id="Seg_3557" s="T81">As I came back I told to my commander: “There are Germans here, they sit behind the river.”</ta>
            <ta e="T96" id="Seg_3558" s="T90">He told me: “Let's go fast.”</ta>
            <ta e="T102" id="Seg_3559" s="T96">I left, I came up with him, we went fast.</ta>
            <ta e="T105" id="Seg_3560" s="T102">The soldiers go behind.</ta>
            <ta e="T107" id="Seg_3561" s="T105">We are in a hurry.</ta>
            <ta e="T112" id="Seg_3562" s="T107">I took my commander to the Tom river.</ta>
            <ta e="T117" id="Seg_3563" s="T112">He and I wanted to look at the Germans.</ta>
            <ta e="T121" id="Seg_3564" s="T117">The commander got up to have a look.</ta>
            <ta e="T131" id="Seg_3565" s="T121">From there a German saw us and he shooted, both of us were wounded.</ta>
            <ta e="T134" id="Seg_3566" s="T131">My commander was not breathing.</ta>
            <ta e="T144" id="Seg_3567" s="T134">Then I showed with my arm, he showed with his arm: don't lift your head up.</ta>
            <ta e="T146" id="Seg_3568" s="T144">He understood.</ta>
            <ta e="T149" id="Seg_3569" s="T146">He shouted: “Backwards!”</ta>
            <ta e="T152" id="Seg_3570" s="T149">From there came a medical assistant.</ta>
            <ta e="T154" id="Seg_3571" s="T152">He came to us.</ta>
            <ta e="T158" id="Seg_3572" s="T154">He cannot reach us.</ta>
            <ta e="T164" id="Seg_3573" s="T158">He began to unwind his wrap (he unwinded it).</ta>
            <ta e="T168" id="Seg_3574" s="T164">He began to pull us away.</ta>
            <ta e="T172" id="Seg_3575" s="T168">Then he pulled us away with the help of his wrap.</ta>
            <ta e="T175" id="Seg_3576" s="T172">The comander was totally killed.</ta>
            <ta e="T179" id="Seg_3577" s="T175">I was bandaged up and put into a trench.</ta>
            <ta e="T184" id="Seg_3578" s="T179">I spent six hours in a trench.</ta>
            <ta e="T188" id="Seg_3579" s="T184">A medical instructor pulled me away from there.</ta>
            <ta e="T199" id="Seg_3580" s="T188">He was pulling me for more than hundred meters.</ta>
            <ta e="T202" id="Seg_3581" s="T199">Then I was carried by a horse.</ta>
            <ta e="T206" id="Seg_3582" s="T202">I was brought to the first village.</ta>
            <ta e="T212" id="Seg_3583" s="T206">here a fragment was taken out from my leg.</ta>
            <ta e="T217" id="Seg_3584" s="T212">From there they started to transport me back.</ta>
            <ta e="T223" id="Seg_3585" s="T217">From there I was brought to the front line by a car.</ta>
            <ta e="T230" id="Seg_3586" s="T223">I spent two years and four months in a hospital.</ta>
            <ta e="T233" id="Seg_3587" s="T230">I was cured.</ta>
            <ta e="T235" id="Seg_3588" s="T233">I walked.</ta>
            <ta e="T239" id="Seg_3589" s="T235">Then I was sent back.</ta>
            <ta e="T242" id="Seg_3590" s="T239">I came back.</ta>
            <ta e="T246" id="Seg_3591" s="T242">After that I live at home.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_3592" s="T0">Ich wurde zur Armee geholt.</ta>
            <ta e="T8" id="Seg_3593" s="T3">Ich wurde geholt, ich wurde in Asino ausgebildet.</ta>
            <ta e="T13" id="Seg_3594" s="T8">Dort wurde ich drei Monate lang trainiert.</ta>
            <ta e="T17" id="Seg_3595" s="T13">Von dort wurde ich nach Kemerovo gebracht.</ta>
            <ta e="T22" id="Seg_3596" s="T17">Ich arbeitete dort zwei Jahre lang.</ta>
            <ta e="T26" id="Seg_3597" s="T22">Ich wurde zur Armee geholt.</ta>
            <ta e="T32" id="Seg_3598" s="T26">Ich war dort in der Armee sechs Monate lang.</ta>
            <ta e="T36" id="Seg_3599" s="T32">Wir kämpften dort.</ta>
            <ta e="T45" id="Seg_3600" s="T36">Ich kam dorthin zum ersten Mal mit dem Panzer, ein Panzer fuhr in der Nacht über mein Bein.</ta>
            <ta e="T51" id="Seg_3601" s="T45">Ich blieb drei Tage hinter meinen Kameraden zurück.</ta>
            <ta e="T57" id="Seg_3602" s="T51">Ich wurde dort behandelt, ich wurde drei Tage lang behandelt.</ta>
            <ta e="T60" id="Seg_3603" s="T57">Ich wurde weiter geschickt.</ta>
            <ta e="T65" id="Seg_3604" s="T60">Ich kämpfte, ich überquerte(?) zwei Flüsse.</ta>
            <ta e="T68" id="Seg_3605" s="T65">Ich wurde weiter geschickt.</ta>
            <ta e="T72" id="Seg_3606" s="T68">Ich überquerte (einen Fluss), ich ging weiter.</ta>
            <ta e="T78" id="Seg_3607" s="T72">Ich wurde voraus geschickt, um die Umgebung auszuspähen.</ta>
            <ta e="T81" id="Seg_3608" s="T78">Ich ging dorthin.</ta>
            <ta e="T90" id="Seg_3609" s="T81">Als ich zurückkam, sagte ich meinem Befehlshaber: "Die Deutschen sind hier, sie sitzen hinter dem Fluss."</ta>
            <ta e="T96" id="Seg_3610" s="T90">Er sagte mir: "Lass uns schnell hinlaufen."</ta>
            <ta e="T102" id="Seg_3611" s="T96">Ich brach auf, ich kam mit ihm, wir rannten schnell.</ta>
            <ta e="T105" id="Seg_3612" s="T102">Die Soldaten gehen hinten.</ta>
            <ta e="T107" id="Seg_3613" s="T105">Wir beeilen uns.</ta>
            <ta e="T112" id="Seg_3614" s="T107">Ich brachte meinen Befehlshaber zum Tom-Fluss.</ta>
            <ta e="T117" id="Seg_3615" s="T112">Wir wollten beide nach den Deutschen sehen.</ta>
            <ta e="T121" id="Seg_3616" s="T117">Der Befehlshaber stand auf, um zu schauen.</ta>
            <ta e="T131" id="Seg_3617" s="T121">Von dort sah uns ein Deutscher und er schoss von dort, wir beide wurden verwundet.</ta>
            <ta e="T134" id="Seg_3618" s="T131">Mein Befehlshaber atmete nicht.</ta>
            <ta e="T144" id="Seg_3619" s="T134">Ich zeigte mit meinem Arm, er zeigte mit seinem Arm: Heb nicht den Kopf.</ta>
            <ta e="T146" id="Seg_3620" s="T144">Er verstand.</ta>
            <ta e="T149" id="Seg_3621" s="T146">Er rief: "Zurück!"</ta>
            <ta e="T152" id="Seg_3622" s="T149">Von dort kam ein Sanitäter.</ta>
            <ta e="T154" id="Seg_3623" s="T152">Er kam zu uns.</ta>
            <ta e="T158" id="Seg_3624" s="T154">Er kann uns nicht erreichen.</ta>
            <ta e="T164" id="Seg_3625" s="T158">Er begann seinen Mantel zu lösen.</ta>
            <ta e="T168" id="Seg_3626" s="T164">Er begann uns von dort wegzuziehen.</ta>
            <ta e="T172" id="Seg_3627" s="T168">Dann zog er uns weg mit dem Mantel.</ta>
            <ta e="T175" id="Seg_3628" s="T172">Der Befehlshaber war vollkommen tot.</ta>
            <ta e="T179" id="Seg_3629" s="T175">Ich wurde verbunden und in einen Graben gelegt.</ta>
            <ta e="T184" id="Seg_3630" s="T179">Ich lag sechs Stunden im Graben.</ta>
            <ta e="T188" id="Seg_3631" s="T184">Ein Sanitätsausbilder zog mich von dort weg.</ta>
            <ta e="T199" id="Seg_3632" s="T188">Er zog mich mehr als hundert Meter.</ta>
            <ta e="T202" id="Seg_3633" s="T199">Dann wurde ich von einem Pferd getragen.</ta>
            <ta e="T206" id="Seg_3634" s="T202">Ich wurde ins erste Dorf gebracht.</ta>
            <ta e="T212" id="Seg_3635" s="T206">Hier holten sie einen Splitter aus meinem Bein.</ta>
            <ta e="T217" id="Seg_3636" s="T212">Von dort begannen sie mich zurückzubringen.</ta>
            <ta e="T223" id="Seg_3637" s="T217">Von dort brachten sie mich zur Frontlinie mit dem Auto.</ta>
            <ta e="T230" id="Seg_3638" s="T223">Ich lag zwei Jahre und vier Monate in einem Krankenhaus.</ta>
            <ta e="T233" id="Seg_3639" s="T230">Ich wurde gesund gepflegt.</ta>
            <ta e="T235" id="Seg_3640" s="T233">Ich ging.</ta>
            <ta e="T239" id="Seg_3641" s="T235">Sie schickten mich von dort zurück.</ta>
            <ta e="T242" id="Seg_3642" s="T239">Ich kam nach Hause.</ta>
            <ta e="T246" id="Seg_3643" s="T242">Danach lebe ich zu Hause.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_3644" s="T0">меня в солдаты взяли</ta>
            <ta e="T8" id="Seg_3645" s="T3">повезли меня учили в Асино</ta>
            <ta e="T13" id="Seg_3646" s="T8">меня три месяца учили</ta>
            <ta e="T17" id="Seg_3647" s="T13">оттуда меня увезли в Кемерово</ta>
            <ta e="T22" id="Seg_3648" s="T17">я там работал два года</ta>
            <ta e="T26" id="Seg_3649" s="T22">взяли в армию</ta>
            <ta e="T32" id="Seg_3650" s="T26">я там был в армии шесть месяцев</ta>
            <ta e="T36" id="Seg_3651" s="T32">мы там воевали</ta>
            <ta e="T45" id="Seg_3652" s="T36">я первый раз пошел танк смял по ноге моей танк прошел</ta>
            <ta e="T51" id="Seg_3653" s="T45">я три дня от своих товарищей остался (отстал)</ta>
            <ta e="T57" id="Seg_3654" s="T51">меня оттуда лечили три дня лечили</ta>
            <ta e="T60" id="Seg_3655" s="T57">меня вперед отправили</ta>
            <ta e="T65" id="Seg_3656" s="T60">я воевал две реки перешел</ta>
            <ta e="T68" id="Seg_3657" s="T65">меня вперед отправили перевезли</ta>
            <ta e="T72" id="Seg_3658" s="T68">перевезли (переправился (я через реку)) я вперед иду</ta>
            <ta e="T78" id="Seg_3659" s="T72">вперед меня отправили впереди местность проверить (смотреть)</ta>
            <ta e="T81" id="Seg_3660" s="T78">я туда пошел</ta>
            <ta e="T90" id="Seg_3661" s="T81">придя обратно сказал командиру немец здесь сидят за речкой</ta>
            <ta e="T96" id="Seg_3662" s="T90">он мне сказал давай скорей пойдем</ta>
            <ta e="T102" id="Seg_3663" s="T96">я отправился догнал (его) скорей пошли</ta>
            <ta e="T105" id="Seg_3664" s="T102">позади солдаты идут</ta>
            <ta e="T107" id="Seg_3665" s="T105">торопимся</ta>
            <ta e="T112" id="Seg_3666" s="T107">я привел командира к речке</ta>
            <ta e="T117" id="Seg_3667" s="T112">мы с ним хотели на немцев взглянуть</ta>
            <ta e="T121" id="Seg_3668" s="T117">командир встал чтоб взглянуть</ta>
            <ta e="T131" id="Seg_3669" s="T121">оттуда немец увидел нас оттуда стрелял в нас обоих поранил</ta>
            <ta e="T134" id="Seg_3670" s="T131">мой командир не дышал</ta>
            <ta e="T144" id="Seg_3671" s="T134">потом я рукой показал (знак сделал) рукой махнул голову вверх не подымай</ta>
            <ta e="T146" id="Seg_3672" s="T144">он понял</ta>
            <ta e="T149" id="Seg_3673" s="T146">назад он крикнул</ta>
            <ta e="T152" id="Seg_3674" s="T149">оттуда идет фельдшер</ta>
            <ta e="T154" id="Seg_3675" s="T152">к нам пришел</ta>
            <ta e="T158" id="Seg_3676" s="T154">дойти он к нам</ta>
            <ta e="T164" id="Seg_3677" s="T158">он обмотку разматывает размотал</ta>
            <ta e="T168" id="Seg_3678" s="T164">нас оттуда давай подтаскивать</ta>
            <ta e="T172" id="Seg_3679" s="T168">вытащил обмоткой</ta>
            <ta e="T175" id="Seg_3680" s="T172">командира убили совсем</ta>
            <ta e="T179" id="Seg_3681" s="T175">меня перевязали положили в окоп</ta>
            <ta e="T184" id="Seg_3682" s="T179">шесть часов я в окопе лежал</ta>
            <ta e="T188" id="Seg_3683" s="T184">меня оттуда потащил</ta>
            <ta e="T199" id="Seg_3684" s="T188">меня он тащил пятьдесят сажень</ta>
            <ta e="T202" id="Seg_3685" s="T199">потом на лошади везли</ta>
            <ta e="T206" id="Seg_3686" s="T202">меня в первую деревню привезли</ta>
            <ta e="T212" id="Seg_3687" s="T206">тут у меня выдернули осколки из ноги</ta>
            <ta e="T217" id="Seg_3688" s="T212">оттуда меня обратно вести начали</ta>
            <ta e="T223" id="Seg_3689" s="T217">меня оттуда на линию привезли на машине</ta>
            <ta e="T230" id="Seg_3690" s="T223">я лежал в больнице два года четыре месяца</ta>
            <ta e="T233" id="Seg_3691" s="T230">меня вылечили</ta>
            <ta e="T235" id="Seg_3692" s="T233">я ходил</ta>
            <ta e="T239" id="Seg_3693" s="T235">меня обратно отправили</ta>
            <ta e="T242" id="Seg_3694" s="T239">я домой приехал</ta>
            <ta e="T246" id="Seg_3695" s="T242">после того я дома живу</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T8" id="Seg_3696" s="T3">[WNB:] Asino: 56.997514, 86.153906</ta>
            <ta e="T17" id="Seg_3697" s="T13">[WNB:] Kemerovo: 55.354968, 86.087314</ta>
            <ta e="T149" id="Seg_3698" s="T146">[AAV:] or: He shouted back (to call for help)?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
