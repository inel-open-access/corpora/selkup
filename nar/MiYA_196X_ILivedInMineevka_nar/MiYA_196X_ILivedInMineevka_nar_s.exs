<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>MiYA_196x_ILivedInMineevka_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">MiYA_196X_ILivedInMineevka_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">75</ud-information>
            <ud-information attribute-name="# HIAT:w">54</ud-information>
            <ud-information attribute-name="# e">54</ud-information>
            <ud-information attribute-name="# HIAT:u">11</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="MiYA">
            <abbreviation>MiYA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="MiYA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T54" id="Seg_0" n="sc" s="T0">
               <ts e="T2" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Minʼeiht</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">wargak</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_11" n="HIAT:u" s="T2">
                  <ts e="T3" id="Seg_13" n="HIAT:w" s="T2">qolčeqwaq</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">maǯʼot</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">qwajaɣɨq</ts>
                  <nts id="Seg_20" n="HIAT:ip">,</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">tabetčuqwaq</ts>
                  <nts id="Seg_24" n="HIAT:ip">,</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_27" n="HIAT:w" s="T6">patom</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_30" n="HIAT:w" s="T7">peqap</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">qwatqwat</ts>
                  <nts id="Seg_34" n="HIAT:ip">,</nts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">šip</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">qočeq</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">qwatqwaq</ts>
                  <nts id="Seg_44" n="HIAT:ip">,</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">naɣi</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">warɣeqwaq</ts>
                  <nts id="Seg_51" n="HIAT:ip">.</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_54" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">Minʼeewsqij</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">zaːimqaut</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">mat</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">täː</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">qwässaq</ts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_72" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">Staricaɣɨn</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">wargaq</ts>
                  <nts id="Seg_78" n="HIAT:ip">,</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_81" n="HIAT:w" s="T21">Staricaɣɨn</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">i</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_87" n="HIAT:w" s="T23">nädaq</ts>
                  <nts id="Seg_88" n="HIAT:ip">.</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_91" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_93" n="HIAT:w" s="T24">šətə</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_96" n="HIAT:w" s="T25">qɨba</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_99" n="HIAT:w" s="T26">mart</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_102" n="HIAT:w" s="T27">eːɣaq</ts>
                  <nts id="Seg_103" n="HIAT:ip">.</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_106" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_108" n="HIAT:w" s="T28">aːmelčiqot</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_111" n="HIAT:w" s="T29">promɨšlʼajčugu</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_114" n="HIAT:w" s="T30">qwajat</ts>
                  <nts id="Seg_115" n="HIAT:ip">.</nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_118" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_120" n="HIAT:w" s="T31">načat</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_123" n="HIAT:w" s="T32">mač</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_126" n="HIAT:w" s="T33">eːɣa</ts>
                  <nts id="Seg_127" n="HIAT:ip">.</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_130" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_132" n="HIAT:w" s="T34">tɨtɨʒaq</ts>
                  <nts id="Seg_133" n="HIAT:ip">,</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_136" n="HIAT:w" s="T35">načaɣɨt</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_139" n="HIAT:w" s="T36">tabetčiqwat</ts>
                  <nts id="Seg_140" n="HIAT:ip">,</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_143" n="HIAT:w" s="T37">šip</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_146" n="HIAT:w" s="T38">qwadešpɨqwat</ts>
                  <nts id="Seg_147" n="HIAT:ip">.</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_150" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_152" n="HIAT:w" s="T39">i</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_155" n="HIAT:w" s="T40">načat</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_158" n="HIAT:w" s="T41">qorɣɨp</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_161" n="HIAT:w" s="T42">qwatqwaq</ts>
                  <nts id="Seg_162" n="HIAT:ip">.</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_165" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_167" n="HIAT:w" s="T43">potom</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_170" n="HIAT:w" s="T44">töːɣaq</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_173" n="HIAT:w" s="T45">Staricaɣɨn</ts>
                  <nts id="Seg_174" n="HIAT:ip">,</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_177" n="HIAT:w" s="T46">moɣonä</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_180" n="HIAT:w" s="T47">töɣat</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_183" n="HIAT:w" s="T48">promɨšlʼajčaq</ts>
                  <nts id="Seg_184" n="HIAT:ip">.</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_187" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_189" n="HIAT:w" s="T49">Starican</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_192" n="HIAT:w" s="T50">wargaq</ts>
                  <nts id="Seg_193" n="HIAT:ip">,</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_196" n="HIAT:w" s="T51">paxajčaq</ts>
                  <nts id="Seg_197" n="HIAT:ip">,</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_200" n="HIAT:w" s="T52">abɨp</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_203" n="HIAT:w" s="T53">čoɣaq</ts>
                  <nts id="Seg_204" n="HIAT:ip">.</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T54" id="Seg_206" n="sc" s="T0">
               <ts e="T1" id="Seg_208" n="e" s="T0">Minʼeiht </ts>
               <ts e="T2" id="Seg_210" n="e" s="T1">wargak. </ts>
               <ts e="T3" id="Seg_212" n="e" s="T2">qolčeqwaq </ts>
               <ts e="T4" id="Seg_214" n="e" s="T3">maǯʼot </ts>
               <ts e="T5" id="Seg_216" n="e" s="T4">qwajaɣɨq, </ts>
               <ts e="T6" id="Seg_218" n="e" s="T5">tabetčuqwaq, </ts>
               <ts e="T7" id="Seg_220" n="e" s="T6">patom </ts>
               <ts e="T8" id="Seg_222" n="e" s="T7">peqap </ts>
               <ts e="T9" id="Seg_224" n="e" s="T8">qwatqwat, </ts>
               <ts e="T10" id="Seg_226" n="e" s="T9">šip </ts>
               <ts e="T11" id="Seg_228" n="e" s="T10">qočeq </ts>
               <ts e="T12" id="Seg_230" n="e" s="T11">qwatqwaq, </ts>
               <ts e="T13" id="Seg_232" n="e" s="T12">naɣi </ts>
               <ts e="T14" id="Seg_234" n="e" s="T13">warɣeqwaq. </ts>
               <ts e="T15" id="Seg_236" n="e" s="T14">Minʼeewsqij </ts>
               <ts e="T16" id="Seg_238" n="e" s="T15">zaːimqaut </ts>
               <ts e="T17" id="Seg_240" n="e" s="T16">mat </ts>
               <ts e="T18" id="Seg_242" n="e" s="T17">täː </ts>
               <ts e="T19" id="Seg_244" n="e" s="T18">qwässaq. </ts>
               <ts e="T20" id="Seg_246" n="e" s="T19">Staricaɣɨn </ts>
               <ts e="T21" id="Seg_248" n="e" s="T20">wargaq, </ts>
               <ts e="T22" id="Seg_250" n="e" s="T21">Staricaɣɨn </ts>
               <ts e="T23" id="Seg_252" n="e" s="T22">i </ts>
               <ts e="T24" id="Seg_254" n="e" s="T23">nädaq. </ts>
               <ts e="T25" id="Seg_256" n="e" s="T24">šətə </ts>
               <ts e="T26" id="Seg_258" n="e" s="T25">qɨba </ts>
               <ts e="T27" id="Seg_260" n="e" s="T26">mart </ts>
               <ts e="T28" id="Seg_262" n="e" s="T27">eːɣaq. </ts>
               <ts e="T29" id="Seg_264" n="e" s="T28">aːmelčiqot </ts>
               <ts e="T30" id="Seg_266" n="e" s="T29">promɨšlʼajčugu </ts>
               <ts e="T31" id="Seg_268" n="e" s="T30">qwajat. </ts>
               <ts e="T32" id="Seg_270" n="e" s="T31">načat </ts>
               <ts e="T33" id="Seg_272" n="e" s="T32">mač </ts>
               <ts e="T34" id="Seg_274" n="e" s="T33">eːɣa. </ts>
               <ts e="T35" id="Seg_276" n="e" s="T34">tɨtɨʒaq, </ts>
               <ts e="T36" id="Seg_278" n="e" s="T35">načaɣɨt </ts>
               <ts e="T37" id="Seg_280" n="e" s="T36">tabetčiqwat, </ts>
               <ts e="T38" id="Seg_282" n="e" s="T37">šip </ts>
               <ts e="T39" id="Seg_284" n="e" s="T38">qwadešpɨqwat. </ts>
               <ts e="T40" id="Seg_286" n="e" s="T39">i </ts>
               <ts e="T41" id="Seg_288" n="e" s="T40">načat </ts>
               <ts e="T42" id="Seg_290" n="e" s="T41">qorɣɨp </ts>
               <ts e="T43" id="Seg_292" n="e" s="T42">qwatqwaq. </ts>
               <ts e="T44" id="Seg_294" n="e" s="T43">potom </ts>
               <ts e="T45" id="Seg_296" n="e" s="T44">töːɣaq </ts>
               <ts e="T46" id="Seg_298" n="e" s="T45">Staricaɣɨn, </ts>
               <ts e="T47" id="Seg_300" n="e" s="T46">moɣonä </ts>
               <ts e="T48" id="Seg_302" n="e" s="T47">töɣat </ts>
               <ts e="T49" id="Seg_304" n="e" s="T48">promɨšlʼajčaq. </ts>
               <ts e="T50" id="Seg_306" n="e" s="T49">Starican </ts>
               <ts e="T51" id="Seg_308" n="e" s="T50">wargaq, </ts>
               <ts e="T52" id="Seg_310" n="e" s="T51">paxajčaq, </ts>
               <ts e="T53" id="Seg_312" n="e" s="T52">abɨp </ts>
               <ts e="T54" id="Seg_314" n="e" s="T53">čoɣaq. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2" id="Seg_315" s="T0">MiYA_196X_ILivedInMineevka_nar.001 (001.001)</ta>
            <ta e="T14" id="Seg_316" s="T2">MiYA_196X_ILivedInMineevka_nar.002 (001.002)</ta>
            <ta e="T19" id="Seg_317" s="T14">MiYA_196X_ILivedInMineevka_nar.003 (001.003)</ta>
            <ta e="T24" id="Seg_318" s="T19">MiYA_196X_ILivedInMineevka_nar.004 (001.004)</ta>
            <ta e="T28" id="Seg_319" s="T24">MiYA_196X_ILivedInMineevka_nar.005 (001.005)</ta>
            <ta e="T31" id="Seg_320" s="T28">MiYA_196X_ILivedInMineevka_nar.006 (001.006)</ta>
            <ta e="T34" id="Seg_321" s="T31">MiYA_196X_ILivedInMineevka_nar.007 (001.007)</ta>
            <ta e="T39" id="Seg_322" s="T34">MiYA_196X_ILivedInMineevka_nar.008 (001.008)</ta>
            <ta e="T43" id="Seg_323" s="T39">MiYA_196X_ILivedInMineevka_nar.009 (001.009)</ta>
            <ta e="T49" id="Seg_324" s="T43">MiYA_196X_ILivedInMineevka_nar.010 (001.010)</ta>
            <ta e="T54" id="Seg_325" s="T49">MiYA_196X_ILivedInMineevka_nar.011 (001.011)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T2" id="Seg_326" s="T0">минʼеиhт вар′гак.</ta>
            <ta e="T14" id="Seg_327" s="T2">kол′чеkwак мадʼжʼот kwа′jаɣык, та′бетчуквак, патом ′пекап kwат′кват, шип ′кочек kwат′кват[к], на′ɣи[е] варɣе′квак.</ta>
            <ta e="T19" id="Seg_328" s="T14">минʼеевский за̄′имкаут мат тӓ̄ ′kwӓссак.</ta>
            <ta e="T24" id="Seg_329" s="T19">старицаɣын вар′гак, старицаɣын и нӓ′дак.</ta>
            <ta e="T28" id="Seg_330" s="T24">шътъ кы′ба март ′е̄ɣак.</ta>
            <ta e="T31" id="Seg_331" s="T28">а̄меlчикот промышлʼайчугу kwа′jат.</ta>
            <ta e="T34" id="Seg_332" s="T31">на′чат ′мач(e) е̄ɣа.</ta>
            <ta e="T39" id="Seg_333" s="T34">ты′тыжак, на′чаɣыт та′бетчикват, шип kwа′дешпыкват.</ta>
            <ta e="T43" id="Seg_334" s="T39">и на′чат kорɣып kwат′квак.</ta>
            <ta e="T49" id="Seg_335" s="T43">потом тӧ̄ɣак старицаɣын, моɣонӓ тӧɣат промыш′лʼайчак.</ta>
            <ta e="T54" id="Seg_336" s="T49">ста′рицан варгак, па′хайчак, ′абып ′чоɣак.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T2" id="Seg_337" s="T0">minʼeiht vargak.</ta>
            <ta e="T14" id="Seg_338" s="T2">qolčeqwaq madʼʒʼot qwajaɣɨq, tabetčuqvaq, patom peqap qwatqvat, šip qočeq qwatqvat[q], naɣi[e] varɣeqvaq.</ta>
            <ta e="T19" id="Seg_339" s="T14">minʼeevsqij zaːimqaut mat täː qwässaq.</ta>
            <ta e="T24" id="Seg_340" s="T19">staricaɣɨn vargaq, staricaɣɨn i nädaq.</ta>
            <ta e="T28" id="Seg_341" s="T24">šətə qɨba mart eːɣaq.</ta>
            <ta e="T31" id="Seg_342" s="T28">aːmelčiqot promɨšlʼajčugu qwajat.</ta>
            <ta e="T34" id="Seg_343" s="T31">načat mač(e) eːɣa.</ta>
            <ta e="T39" id="Seg_344" s="T34">tɨtɨʒaq, načaɣɨt tabetčiqvat, šip qwadešpɨqvat.</ta>
            <ta e="T43" id="Seg_345" s="T39">i načat qorɣɨp qwatqvaq.</ta>
            <ta e="T49" id="Seg_346" s="T43">potom töːɣaq staricaɣɨn, moɣonä töɣat promɨšlʼajčaq.</ta>
            <ta e="T54" id="Seg_347" s="T49">starican vargaq, pahajčaq, abɨp čoɣaq.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2" id="Seg_348" s="T0">Minʼeiht wargak. </ta>
            <ta e="T14" id="Seg_349" s="T2">qolčeqwaq maǯʼot qwajaɣɨq, tabetčuqwaq, patom peqap qwatqwat, šip qočeq qwatqwaq, naɣi warɣeqwaq. </ta>
            <ta e="T19" id="Seg_350" s="T14">Minʼeewsqij zaːimqaut mat täː qwässaq. </ta>
            <ta e="T24" id="Seg_351" s="T19">Staricaɣɨn wargaq, Staricaɣɨn i nädaq. </ta>
            <ta e="T28" id="Seg_352" s="T24">šətə qɨba mart eːɣaq. </ta>
            <ta e="T31" id="Seg_353" s="T28">aːmelčiqot promɨšlʼajčugu qwajat. </ta>
            <ta e="T34" id="Seg_354" s="T31">načat mač eːɣa. </ta>
            <ta e="T39" id="Seg_355" s="T34">tɨtɨʒaq, načaɣɨt tabetčiqwat, šip qwadešpɨqwat. </ta>
            <ta e="T43" id="Seg_356" s="T39">i načat qorɣɨp qwatqwaq. </ta>
            <ta e="T49" id="Seg_357" s="T43">potom töːɣaq Staricaɣɨn, moɣonä töɣat promɨšlʼajčaq. </ta>
            <ta e="T54" id="Seg_358" s="T49">Starican wargaq, paxajčaq, abɨp čoɣaq. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_359" s="T0">Minʼeih-t</ta>
            <ta e="T2" id="Seg_360" s="T1">warga-k</ta>
            <ta e="T3" id="Seg_361" s="T2">qo-lče-q-wa-q</ta>
            <ta e="T4" id="Seg_362" s="T3">maǯʼo-t</ta>
            <ta e="T5" id="Seg_363" s="T4">qwaja-ɣɨ-q</ta>
            <ta e="T6" id="Seg_364" s="T5">tabe-tču-q-wa-q</ta>
            <ta e="T7" id="Seg_365" s="T6">patom</ta>
            <ta e="T8" id="Seg_366" s="T7">peqa-p</ta>
            <ta e="T9" id="Seg_367" s="T8">qwat-q-wa-t</ta>
            <ta e="T10" id="Seg_368" s="T9">ši-p</ta>
            <ta e="T11" id="Seg_369" s="T10">qočeq</ta>
            <ta e="T12" id="Seg_370" s="T11">qwat-q-wa-q</ta>
            <ta e="T13" id="Seg_371" s="T12">na-ɣi</ta>
            <ta e="T14" id="Seg_372" s="T13">warɣe-q-wa-q</ta>
            <ta e="T15" id="Seg_373" s="T14">minʼeewsqij</ta>
            <ta e="T16" id="Seg_374" s="T15">zaːimqa-ut</ta>
            <ta e="T17" id="Seg_375" s="T16">mat</ta>
            <ta e="T18" id="Seg_376" s="T17">täː</ta>
            <ta e="T19" id="Seg_377" s="T18">qwäs-sa-q</ta>
            <ta e="T20" id="Seg_378" s="T19">Starica-ɣɨn</ta>
            <ta e="T21" id="Seg_379" s="T20">warga-q</ta>
            <ta e="T22" id="Seg_380" s="T21">Starica-ɣɨn</ta>
            <ta e="T23" id="Seg_381" s="T22">i</ta>
            <ta e="T24" id="Seg_382" s="T23">näda-q</ta>
            <ta e="T25" id="Seg_383" s="T24">šətə</ta>
            <ta e="T26" id="Seg_384" s="T25">qɨba</ta>
            <ta e="T27" id="Seg_385" s="T26">mar-t</ta>
            <ta e="T28" id="Seg_386" s="T27">eː-ɣa-q</ta>
            <ta e="T29" id="Seg_387" s="T28">aːm-e-lči-qo-t</ta>
            <ta e="T30" id="Seg_388" s="T29">promɨšlʼaj-ču-gu</ta>
            <ta e="T31" id="Seg_389" s="T30">qwaja-t</ta>
            <ta e="T32" id="Seg_390" s="T31">nača-t</ta>
            <ta e="T33" id="Seg_391" s="T32">mač</ta>
            <ta e="T34" id="Seg_392" s="T33">eː-ɣa</ta>
            <ta e="T35" id="Seg_393" s="T34">tɨtɨ-ʒaq</ta>
            <ta e="T36" id="Seg_394" s="T35">nača-ɣɨt</ta>
            <ta e="T37" id="Seg_395" s="T36">tabe-tči-q-wa-t</ta>
            <ta e="T38" id="Seg_396" s="T37">ši-p</ta>
            <ta e="T39" id="Seg_397" s="T38">qwad-e-špɨ-q-wa-t</ta>
            <ta e="T40" id="Seg_398" s="T39">i</ta>
            <ta e="T41" id="Seg_399" s="T40">nača-t</ta>
            <ta e="T42" id="Seg_400" s="T41">qorɣɨ-p</ta>
            <ta e="T43" id="Seg_401" s="T42">qwat-q-wa-q</ta>
            <ta e="T44" id="Seg_402" s="T43">potom</ta>
            <ta e="T45" id="Seg_403" s="T44">töː-ɣa-q</ta>
            <ta e="T46" id="Seg_404" s="T45">Starica-ɣɨn</ta>
            <ta e="T47" id="Seg_405" s="T46">moɣonä</ta>
            <ta e="T48" id="Seg_406" s="T47">tö-ɣa-t</ta>
            <ta e="T49" id="Seg_407" s="T48">promɨšlʼaj-ča-q</ta>
            <ta e="T50" id="Seg_408" s="T49">Starica-n</ta>
            <ta e="T51" id="Seg_409" s="T50">warga-q</ta>
            <ta e="T52" id="Seg_410" s="T51">paxaj-ča-q</ta>
            <ta e="T53" id="Seg_411" s="T52">ab-ɨ-p</ta>
            <ta e="T54" id="Seg_412" s="T53">čoɣa-q</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_413" s="T0">Minʼeih-t</ta>
            <ta e="T2" id="Seg_414" s="T1">wargɨ-k</ta>
            <ta e="T3" id="Seg_415" s="T2">qo-lʼčǝ-ku-wa-k</ta>
            <ta e="T4" id="Seg_416" s="T3">maǯʼo-nde</ta>
            <ta e="T5" id="Seg_417" s="T4">qwaja-ŋɨ-k</ta>
            <ta e="T6" id="Seg_418" s="T5">tabek-ču-ku-wa-k</ta>
            <ta e="T7" id="Seg_419" s="T6">patom</ta>
            <ta e="T8" id="Seg_420" s="T7">peqqa-m</ta>
            <ta e="T9" id="Seg_421" s="T8">kwat-ku-wa-tɨ</ta>
            <ta e="T10" id="Seg_422" s="T9">ši-p</ta>
            <ta e="T11" id="Seg_423" s="T10">koček</ta>
            <ta e="T12" id="Seg_424" s="T11">kwat-ku-wa-k</ta>
            <ta e="T13" id="Seg_425" s="T12">na-se</ta>
            <ta e="T14" id="Seg_426" s="T13">wargɨ-ku-wa-k</ta>
            <ta e="T15" id="Seg_427" s="T14">minʼeewsqij</ta>
            <ta e="T16" id="Seg_428" s="T15">zaimqa-ute</ta>
            <ta e="T17" id="Seg_429" s="T16">man</ta>
            <ta e="T18" id="Seg_430" s="T17">teː</ta>
            <ta e="T19" id="Seg_431" s="T18">qwän-sɨ-k</ta>
            <ta e="T20" id="Seg_432" s="T19">Starica-qɨn</ta>
            <ta e="T21" id="Seg_433" s="T20">wargɨ-k</ta>
            <ta e="T22" id="Seg_434" s="T21">Starica-qɨn</ta>
            <ta e="T23" id="Seg_435" s="T22">i</ta>
            <ta e="T24" id="Seg_436" s="T23">nadɨ-k</ta>
            <ta e="T25" id="Seg_437" s="T24">šitə</ta>
            <ta e="T26" id="Seg_438" s="T25">kɨba</ta>
            <ta e="T27" id="Seg_439" s="T26">mar-tɨ</ta>
            <ta e="T28" id="Seg_440" s="T27">e-wa-q</ta>
            <ta e="T29" id="Seg_441" s="T28">am-ɨ-lʼčǝ-ku-dət</ta>
            <ta e="T30" id="Seg_442" s="T29">promɨšlʼaj-ču-gu</ta>
            <ta e="T31" id="Seg_443" s="T30">qwaja-dət</ta>
            <ta e="T32" id="Seg_444" s="T31">nača-tɨ</ta>
            <ta e="T33" id="Seg_445" s="T32">maǯʼo</ta>
            <ta e="T34" id="Seg_446" s="T33">e-wa</ta>
            <ta e="T35" id="Seg_447" s="T34">tɨtɨ-šak</ta>
            <ta e="T36" id="Seg_448" s="T35">nača-qɨn</ta>
            <ta e="T37" id="Seg_449" s="T36">tabek-ču-ku-wa-tɨ</ta>
            <ta e="T38" id="Seg_450" s="T37">ši-p</ta>
            <ta e="T39" id="Seg_451" s="T38">kwat-ɨ-špɨ-ku-wa-tɨ</ta>
            <ta e="T40" id="Seg_452" s="T39">i</ta>
            <ta e="T41" id="Seg_453" s="T40">nača-tɨ</ta>
            <ta e="T42" id="Seg_454" s="T41">qorqɨ-p</ta>
            <ta e="T43" id="Seg_455" s="T42">kwat-ku-wa-k</ta>
            <ta e="T44" id="Seg_456" s="T43">patom</ta>
            <ta e="T45" id="Seg_457" s="T44">töː-wa-k</ta>
            <ta e="T46" id="Seg_458" s="T45">Starica-qɨn</ta>
            <ta e="T47" id="Seg_459" s="T46">moqne</ta>
            <ta e="T48" id="Seg_460" s="T47">töː-wa-dət</ta>
            <ta e="T49" id="Seg_461" s="T48">promɨšlʼaj-če-k</ta>
            <ta e="T50" id="Seg_462" s="T49">Starica-n</ta>
            <ta e="T51" id="Seg_463" s="T50">wargɨ-k</ta>
            <ta e="T52" id="Seg_464" s="T51">paxaj-če-k</ta>
            <ta e="T53" id="Seg_465" s="T52">ab-ɨ-p</ta>
            <ta e="T54" id="Seg_466" s="T53">čoɣa-k</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_467" s="T0">Mineyevka-ADV.LOC</ta>
            <ta e="T2" id="Seg_468" s="T1">live-1SG.S</ta>
            <ta e="T3" id="Seg_469" s="T2">go-PFV-HAB-CO-1SG.S</ta>
            <ta e="T4" id="Seg_470" s="T3">taiga-ILL</ta>
            <ta e="T5" id="Seg_471" s="T4">go-CO-1SG.S</ta>
            <ta e="T6" id="Seg_472" s="T5">squirrel-CAP-HAB-CO-1SG.S</ta>
            <ta e="T7" id="Seg_473" s="T6">then</ta>
            <ta e="T8" id="Seg_474" s="T7">elk-ACC</ta>
            <ta e="T9" id="Seg_475" s="T8">kill-HAB-CO-3SG.O</ta>
            <ta e="T10" id="Seg_476" s="T9">sable-ACC</ta>
            <ta e="T11" id="Seg_477" s="T10">much</ta>
            <ta e="T12" id="Seg_478" s="T11">kill-HAB-CO-1SG.S</ta>
            <ta e="T13" id="Seg_479" s="T12">this-INSTR</ta>
            <ta e="T14" id="Seg_480" s="T13">live-HAB-CO-1SG.S</ta>
            <ta e="T15" id="Seg_481" s="T14">Mineevka</ta>
            <ta e="T16" id="Seg_482" s="T15">small.town-ABL2</ta>
            <ta e="T17" id="Seg_483" s="T16">I.NOM</ta>
            <ta e="T18" id="Seg_484" s="T17">away</ta>
            <ta e="T19" id="Seg_485" s="T18">go.away-PST-1SG.S</ta>
            <ta e="T20" id="Seg_486" s="T19">Staritsa-LOC</ta>
            <ta e="T21" id="Seg_487" s="T20">live-1SG.S</ta>
            <ta e="T22" id="Seg_488" s="T21">Staritsa-LOC</ta>
            <ta e="T23" id="Seg_489" s="T22">and</ta>
            <ta e="T24" id="Seg_490" s="T23">get.married-1SG.S</ta>
            <ta e="T25" id="Seg_491" s="T24">two</ta>
            <ta e="T26" id="Seg_492" s="T25">small</ta>
            <ta e="T27" id="Seg_493" s="T26">thing.[NOM]-3SG</ta>
            <ta e="T28" id="Seg_494" s="T27">be-CO-3DU.S</ta>
            <ta e="T29" id="Seg_495" s="T28">eat-EP-PFV-HAB-3PL</ta>
            <ta e="T30" id="Seg_496" s="T29">fish-VBLZ-INF</ta>
            <ta e="T31" id="Seg_497" s="T30">go-3PL</ta>
            <ta e="T32" id="Seg_498" s="T31">there-ADV.LOC</ta>
            <ta e="T33" id="Seg_499" s="T32">taiga.[NOM]</ta>
            <ta e="T34" id="Seg_500" s="T33">be-CO.[3SG.S]</ta>
            <ta e="T35" id="Seg_501" s="T34">cedar-COR</ta>
            <ta e="T36" id="Seg_502" s="T35">there-LOC</ta>
            <ta e="T37" id="Seg_503" s="T36">squirrel-CAP-HAB-CO-3SG.O</ta>
            <ta e="T38" id="Seg_504" s="T37">sable-ACC</ta>
            <ta e="T39" id="Seg_505" s="T38">catch-EP-IPFV2-HAB-CO-3SG.O</ta>
            <ta e="T40" id="Seg_506" s="T39">and</ta>
            <ta e="T41" id="Seg_507" s="T40">there-ADV.LOC</ta>
            <ta e="T42" id="Seg_508" s="T41">bear-ACC</ta>
            <ta e="T43" id="Seg_509" s="T42">kill-HAB-CO-1SG.S</ta>
            <ta e="T44" id="Seg_510" s="T43">then</ta>
            <ta e="T45" id="Seg_511" s="T44">come-CO-1SG.S</ta>
            <ta e="T46" id="Seg_512" s="T45">Staritsa-LOC</ta>
            <ta e="T47" id="Seg_513" s="T46">home</ta>
            <ta e="T48" id="Seg_514" s="T47">come-CO-3PL</ta>
            <ta e="T49" id="Seg_515" s="T48">hunt-DRV-1SG.S</ta>
            <ta e="T50" id="Seg_516" s="T49">Staritsa-ADV.LOC</ta>
            <ta e="T51" id="Seg_517" s="T50">live-1SG.S</ta>
            <ta e="T52" id="Seg_518" s="T51">plow-DRV-1SG.S</ta>
            <ta e="T53" id="Seg_519" s="T52">corn-EP-ACC</ta>
            <ta e="T54" id="Seg_520" s="T53">sow-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_521" s="T0">Минеевка-ADV.LOC</ta>
            <ta e="T2" id="Seg_522" s="T1">жить-1SG.S</ta>
            <ta e="T3" id="Seg_523" s="T2">идти-PFV-HAB-CO-1SG.S</ta>
            <ta e="T4" id="Seg_524" s="T3">тайга-ILL</ta>
            <ta e="T5" id="Seg_525" s="T4">идти-CO-1SG.S</ta>
            <ta e="T6" id="Seg_526" s="T5">белка-CAP-HAB-CO-1SG.S</ta>
            <ta e="T7" id="Seg_527" s="T6">потом</ta>
            <ta e="T8" id="Seg_528" s="T7">лось-ACC</ta>
            <ta e="T9" id="Seg_529" s="T8">убить-HAB-CO-3SG.O</ta>
            <ta e="T10" id="Seg_530" s="T9">соболь-ACC</ta>
            <ta e="T11" id="Seg_531" s="T10">много</ta>
            <ta e="T12" id="Seg_532" s="T11">убить-HAB-CO-1SG.S</ta>
            <ta e="T13" id="Seg_533" s="T12">этот-INSTR</ta>
            <ta e="T14" id="Seg_534" s="T13">жить-HAB-CO-1SG.S</ta>
            <ta e="T15" id="Seg_535" s="T14">минеевский</ta>
            <ta e="T16" id="Seg_536" s="T15">заимка-ABL2</ta>
            <ta e="T17" id="Seg_537" s="T16">я.NOM</ta>
            <ta e="T18" id="Seg_538" s="T17">прочь</ta>
            <ta e="T19" id="Seg_539" s="T18">пойти-PST-1SG.S</ta>
            <ta e="T20" id="Seg_540" s="T19">Старица-LOC</ta>
            <ta e="T21" id="Seg_541" s="T20">жить-1SG.S</ta>
            <ta e="T22" id="Seg_542" s="T21">Старица-LOC</ta>
            <ta e="T23" id="Seg_543" s="T22">и</ta>
            <ta e="T24" id="Seg_544" s="T23">жениться-1SG.S</ta>
            <ta e="T25" id="Seg_545" s="T24">два</ta>
            <ta e="T26" id="Seg_546" s="T25">маленький</ta>
            <ta e="T27" id="Seg_547" s="T26">вещь.[NOM]-3SG</ta>
            <ta e="T28" id="Seg_548" s="T27">быть-CO-3DU.S</ta>
            <ta e="T29" id="Seg_549" s="T28">есть-EP-PFV-HAB-3PL</ta>
            <ta e="T30" id="Seg_550" s="T29">промышлять-VBLZ-INF</ta>
            <ta e="T31" id="Seg_551" s="T30">идти-3PL</ta>
            <ta e="T32" id="Seg_552" s="T31">туда-ADV.LOC</ta>
            <ta e="T33" id="Seg_553" s="T32">тайга.[NOM]</ta>
            <ta e="T34" id="Seg_554" s="T33">быть-CO.[3SG.S]</ta>
            <ta e="T35" id="Seg_555" s="T34">кедр-COR</ta>
            <ta e="T36" id="Seg_556" s="T35">туда-LOC</ta>
            <ta e="T37" id="Seg_557" s="T36">белка-CAP-HAB-CO-3SG.O</ta>
            <ta e="T38" id="Seg_558" s="T37">соболь-ACC</ta>
            <ta e="T39" id="Seg_559" s="T38">поймать-EP-IPFV2-HAB-CO-3SG.O</ta>
            <ta e="T40" id="Seg_560" s="T39">и</ta>
            <ta e="T41" id="Seg_561" s="T40">туда-ADV.LOC</ta>
            <ta e="T42" id="Seg_562" s="T41">медведь-ACC</ta>
            <ta e="T43" id="Seg_563" s="T42">убить-HAB-CO-1SG.S</ta>
            <ta e="T44" id="Seg_564" s="T43">потом</ta>
            <ta e="T45" id="Seg_565" s="T44">прийти-CO-1SG.S</ta>
            <ta e="T46" id="Seg_566" s="T45">Старица-LOC</ta>
            <ta e="T47" id="Seg_567" s="T46">домой</ta>
            <ta e="T48" id="Seg_568" s="T47">прийти-CO-3PL</ta>
            <ta e="T49" id="Seg_569" s="T48">промышлять-DRV-1SG.S</ta>
            <ta e="T50" id="Seg_570" s="T49">Старица-ADV.LOC</ta>
            <ta e="T51" id="Seg_571" s="T50">жить-1SG.S</ta>
            <ta e="T52" id="Seg_572" s="T51">пахать-DRV-1SG.S</ta>
            <ta e="T53" id="Seg_573" s="T52">злаки-EP-ACC</ta>
            <ta e="T54" id="Seg_574" s="T53">посеять-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_575" s="T0">nprop-adv:case</ta>
            <ta e="T2" id="Seg_576" s="T1">v-v:pn</ta>
            <ta e="T3" id="Seg_577" s="T2">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T4" id="Seg_578" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_579" s="T4">v-v:ins-v:pn</ta>
            <ta e="T6" id="Seg_580" s="T5">n-n&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T7" id="Seg_581" s="T6">adv</ta>
            <ta e="T8" id="Seg_582" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_583" s="T8">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T10" id="Seg_584" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_585" s="T10">quant</ta>
            <ta e="T12" id="Seg_586" s="T11">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T13" id="Seg_587" s="T12">dem-n:case</ta>
            <ta e="T14" id="Seg_588" s="T13">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T15" id="Seg_589" s="T14">adj</ta>
            <ta e="T16" id="Seg_590" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_591" s="T16">pers</ta>
            <ta e="T18" id="Seg_592" s="T17">preverb</ta>
            <ta e="T19" id="Seg_593" s="T18">v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_594" s="T19">nprop-n:case</ta>
            <ta e="T21" id="Seg_595" s="T20">v-v:pn</ta>
            <ta e="T22" id="Seg_596" s="T21">nprop-n:case</ta>
            <ta e="T23" id="Seg_597" s="T22">conj</ta>
            <ta e="T24" id="Seg_598" s="T23">v-v:pn</ta>
            <ta e="T25" id="Seg_599" s="T24">num</ta>
            <ta e="T26" id="Seg_600" s="T25">adj</ta>
            <ta e="T27" id="Seg_601" s="T26">n-n:case-n:poss</ta>
            <ta e="T28" id="Seg_602" s="T27">v-v:ins-v:pn</ta>
            <ta e="T29" id="Seg_603" s="T28">v-n:ins-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T30" id="Seg_604" s="T29">v-v&gt;v-v:inf</ta>
            <ta e="T31" id="Seg_605" s="T30">v-v:pn</ta>
            <ta e="T32" id="Seg_606" s="T31">adv-adv:case</ta>
            <ta e="T33" id="Seg_607" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_608" s="T33">v-v:ins-v:pn</ta>
            <ta e="T35" id="Seg_609" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_610" s="T35">adv-n:case</ta>
            <ta e="T37" id="Seg_611" s="T36">n-n&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T38" id="Seg_612" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_613" s="T38">v-n:ins-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T40" id="Seg_614" s="T39">conj</ta>
            <ta e="T41" id="Seg_615" s="T40">adv-adv:case</ta>
            <ta e="T42" id="Seg_616" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_617" s="T42">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T44" id="Seg_618" s="T43">adv</ta>
            <ta e="T45" id="Seg_619" s="T44">v-v:ins-v:pn</ta>
            <ta e="T46" id="Seg_620" s="T45">nprop-n:case</ta>
            <ta e="T47" id="Seg_621" s="T46">adv</ta>
            <ta e="T48" id="Seg_622" s="T47">v-v:ins-v:pn</ta>
            <ta e="T49" id="Seg_623" s="T48">v-v&gt;v-v:pn</ta>
            <ta e="T50" id="Seg_624" s="T49">nprop-adv:case</ta>
            <ta e="T51" id="Seg_625" s="T50">v-v:pn</ta>
            <ta e="T52" id="Seg_626" s="T51">v-v&gt;v-v:pn</ta>
            <ta e="T53" id="Seg_627" s="T52">n-n:ins-n:case</ta>
            <ta e="T54" id="Seg_628" s="T53">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_629" s="T0">nprop</ta>
            <ta e="T2" id="Seg_630" s="T1">v</ta>
            <ta e="T3" id="Seg_631" s="T2">v</ta>
            <ta e="T4" id="Seg_632" s="T3">n</ta>
            <ta e="T5" id="Seg_633" s="T4">v</ta>
            <ta e="T6" id="Seg_634" s="T5">v</ta>
            <ta e="T7" id="Seg_635" s="T6">adv</ta>
            <ta e="T8" id="Seg_636" s="T7">n</ta>
            <ta e="T9" id="Seg_637" s="T8">v</ta>
            <ta e="T10" id="Seg_638" s="T9">n</ta>
            <ta e="T11" id="Seg_639" s="T10">quant</ta>
            <ta e="T12" id="Seg_640" s="T11">v</ta>
            <ta e="T13" id="Seg_641" s="T12">dem</ta>
            <ta e="T14" id="Seg_642" s="T13">v</ta>
            <ta e="T15" id="Seg_643" s="T14">adj</ta>
            <ta e="T16" id="Seg_644" s="T15">n</ta>
            <ta e="T17" id="Seg_645" s="T16">pers</ta>
            <ta e="T18" id="Seg_646" s="T17">preverb</ta>
            <ta e="T19" id="Seg_647" s="T18">v</ta>
            <ta e="T20" id="Seg_648" s="T19">nprop</ta>
            <ta e="T21" id="Seg_649" s="T20">v</ta>
            <ta e="T22" id="Seg_650" s="T21">nprop</ta>
            <ta e="T23" id="Seg_651" s="T22">conj</ta>
            <ta e="T24" id="Seg_652" s="T23">v</ta>
            <ta e="T25" id="Seg_653" s="T24">num</ta>
            <ta e="T26" id="Seg_654" s="T25">adj</ta>
            <ta e="T27" id="Seg_655" s="T26">n</ta>
            <ta e="T28" id="Seg_656" s="T27">v</ta>
            <ta e="T29" id="Seg_657" s="T28">v</ta>
            <ta e="T30" id="Seg_658" s="T29">v</ta>
            <ta e="T31" id="Seg_659" s="T30">v</ta>
            <ta e="T32" id="Seg_660" s="T31">adv</ta>
            <ta e="T33" id="Seg_661" s="T32">n</ta>
            <ta e="T34" id="Seg_662" s="T33">v</ta>
            <ta e="T35" id="Seg_663" s="T34">n</ta>
            <ta e="T36" id="Seg_664" s="T35">adv</ta>
            <ta e="T37" id="Seg_665" s="T36">v</ta>
            <ta e="T38" id="Seg_666" s="T37">n</ta>
            <ta e="T39" id="Seg_667" s="T38">v</ta>
            <ta e="T40" id="Seg_668" s="T39">conj</ta>
            <ta e="T41" id="Seg_669" s="T40">adv</ta>
            <ta e="T42" id="Seg_670" s="T41">n</ta>
            <ta e="T43" id="Seg_671" s="T42">v</ta>
            <ta e="T44" id="Seg_672" s="T43">adv</ta>
            <ta e="T45" id="Seg_673" s="T44">v</ta>
            <ta e="T46" id="Seg_674" s="T45">nprop</ta>
            <ta e="T47" id="Seg_675" s="T46">adv</ta>
            <ta e="T48" id="Seg_676" s="T47">v</ta>
            <ta e="T49" id="Seg_677" s="T48">v</ta>
            <ta e="T50" id="Seg_678" s="T49">nprop</ta>
            <ta e="T51" id="Seg_679" s="T50">v</ta>
            <ta e="T52" id="Seg_680" s="T51">v</ta>
            <ta e="T53" id="Seg_681" s="T52">n</ta>
            <ta e="T54" id="Seg_682" s="T53">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_683" s="T1">0.1.h:S v:pred</ta>
            <ta e="T3" id="Seg_684" s="T2">0.1.h:S v:pred</ta>
            <ta e="T5" id="Seg_685" s="T4">0.1.h:S v:pred</ta>
            <ta e="T6" id="Seg_686" s="T5">0.1.h:S v:pred</ta>
            <ta e="T8" id="Seg_687" s="T7">np:O</ta>
            <ta e="T9" id="Seg_688" s="T8">0.1.h:S v:pred</ta>
            <ta e="T10" id="Seg_689" s="T9">np:O</ta>
            <ta e="T12" id="Seg_690" s="T11">0.1.h:S v:pred</ta>
            <ta e="T14" id="Seg_691" s="T13">0.1.h:S v:pred</ta>
            <ta e="T17" id="Seg_692" s="T16">pro.h:S</ta>
            <ta e="T19" id="Seg_693" s="T18">v:pred</ta>
            <ta e="T21" id="Seg_694" s="T20">0.1.h:S v:pred</ta>
            <ta e="T24" id="Seg_695" s="T23">0.1.h:S v:pred</ta>
            <ta e="T27" id="Seg_696" s="T26">np.h:S</ta>
            <ta e="T28" id="Seg_697" s="T27">v:pred</ta>
            <ta e="T30" id="Seg_698" s="T29">s:purp</ta>
            <ta e="T31" id="Seg_699" s="T30">0.3.h:S v:pred</ta>
            <ta e="T33" id="Seg_700" s="T32">np:S</ta>
            <ta e="T34" id="Seg_701" s="T33">v:pred</ta>
            <ta e="T37" id="Seg_702" s="T36">0.3.h:S v:pred</ta>
            <ta e="T38" id="Seg_703" s="T37">np:O</ta>
            <ta e="T39" id="Seg_704" s="T38">0.3.h:S v:pred</ta>
            <ta e="T42" id="Seg_705" s="T41">np:O</ta>
            <ta e="T43" id="Seg_706" s="T42">0.1.h:S v:pred</ta>
            <ta e="T45" id="Seg_707" s="T44">0.1.h:S v:pred</ta>
            <ta e="T48" id="Seg_708" s="T47">0.3.h:S v:pred</ta>
            <ta e="T49" id="Seg_709" s="T48">0.1.h:S v:pred</ta>
            <ta e="T51" id="Seg_710" s="T50">0.1.h:S v:pred</ta>
            <ta e="T52" id="Seg_711" s="T51">0.1.h:S v:pred</ta>
            <ta e="T53" id="Seg_712" s="T52">np:O</ta>
            <ta e="T54" id="Seg_713" s="T53">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_714" s="T0">np:L</ta>
            <ta e="T2" id="Seg_715" s="T1">0.1.h:Th</ta>
            <ta e="T3" id="Seg_716" s="T2">0.1.h:A</ta>
            <ta e="T4" id="Seg_717" s="T3">np:G</ta>
            <ta e="T5" id="Seg_718" s="T4">0.1.h:A</ta>
            <ta e="T6" id="Seg_719" s="T5">0.1.h:A</ta>
            <ta e="T8" id="Seg_720" s="T7">np:P</ta>
            <ta e="T9" id="Seg_721" s="T8">0.1.h:A</ta>
            <ta e="T10" id="Seg_722" s="T9">np:P</ta>
            <ta e="T12" id="Seg_723" s="T11">0.1.h:A</ta>
            <ta e="T14" id="Seg_724" s="T13">0.1.h:Th</ta>
            <ta e="T16" id="Seg_725" s="T15">np:So</ta>
            <ta e="T17" id="Seg_726" s="T16">pro.h:A</ta>
            <ta e="T20" id="Seg_727" s="T19">np:L</ta>
            <ta e="T21" id="Seg_728" s="T20">0.1.h:Th</ta>
            <ta e="T22" id="Seg_729" s="T21">np:L</ta>
            <ta e="T24" id="Seg_730" s="T23">0.1.h:A</ta>
            <ta e="T27" id="Seg_731" s="T26">np.h:Th</ta>
            <ta e="T31" id="Seg_732" s="T30">0.3.h:A</ta>
            <ta e="T32" id="Seg_733" s="T31">adv:L</ta>
            <ta e="T33" id="Seg_734" s="T32">np:Th</ta>
            <ta e="T36" id="Seg_735" s="T35">adv:L</ta>
            <ta e="T37" id="Seg_736" s="T36">0.3.h:A</ta>
            <ta e="T38" id="Seg_737" s="T37">np:Th</ta>
            <ta e="T39" id="Seg_738" s="T38">0.3.h:A</ta>
            <ta e="T41" id="Seg_739" s="T40">adv:L</ta>
            <ta e="T42" id="Seg_740" s="T41">np:P</ta>
            <ta e="T43" id="Seg_741" s="T42">0.1.h:A</ta>
            <ta e="T45" id="Seg_742" s="T44">0.1.h:A</ta>
            <ta e="T46" id="Seg_743" s="T45">np:G</ta>
            <ta e="T47" id="Seg_744" s="T46">adv:G</ta>
            <ta e="T48" id="Seg_745" s="T47">0.3.h:A</ta>
            <ta e="T49" id="Seg_746" s="T48">0.1.h:A</ta>
            <ta e="T50" id="Seg_747" s="T49">np:L</ta>
            <ta e="T51" id="Seg_748" s="T50">0.1.h:Th</ta>
            <ta e="T52" id="Seg_749" s="T51">0.1.h:A</ta>
            <ta e="T53" id="Seg_750" s="T52">np:Th</ta>
            <ta e="T54" id="Seg_751" s="T53">0.1.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T7" id="Seg_752" s="T6">RUS:core</ta>
            <ta e="T16" id="Seg_753" s="T15">RUS:cult</ta>
            <ta e="T23" id="Seg_754" s="T22">RUS:gram</ta>
            <ta e="T30" id="Seg_755" s="T29">RUS:core</ta>
            <ta e="T40" id="Seg_756" s="T39">RUS:gram</ta>
            <ta e="T44" id="Seg_757" s="T43">RUS:core</ta>
            <ta e="T49" id="Seg_758" s="T48">RUS:core</ta>
            <ta e="T52" id="Seg_759" s="T51">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T49" id="Seg_760" s="T48">parad:infl</ta>
            <ta e="T52" id="Seg_761" s="T51">parad:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T2" id="Seg_762" s="T0">Я жил в Минеевке.</ta>
            <ta e="T14" id="Seg_763" s="T2">В тайгу ходил, белковал, потом лося добыл, соболей много добыл, этим и жил.</ta>
            <ta e="T19" id="Seg_764" s="T14">С Минеевской заимки я уехал.</ta>
            <ta e="T24" id="Seg_765" s="T19">В Старице жил, в Старице и женился.</ta>
            <ta e="T28" id="Seg_766" s="T24">Двое маленьких детей было (у меня).</ta>
            <ta e="T31" id="Seg_767" s="T28"> они кушали и ходили рыбачить.</ta>
            <ta e="T34" id="Seg_768" s="T31">Там тайга была.</ta>
            <ta e="T39" id="Seg_769" s="T34">Kедрач, там белковал, соболей добывал.</ta>
            <ta e="T43" id="Seg_770" s="T39">Там медведя убил.</ta>
            <ta e="T49" id="Seg_771" s="T43">Пошел в Старицу, обратно пошел, промышлял.</ta>
            <ta e="T54" id="Seg_772" s="T49">Я жил в Старице, пахал хлеб и сеял.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T2" id="Seg_773" s="T0">I lived in Mineevka.</ta>
            <ta e="T14" id="Seg_774" s="T2">I went to the taiga, I hunted squirrels, then I killed an elk, I killed many sables, through that I lived.</ta>
            <ta e="T19" id="Seg_775" s="T14">I left Mineevka squatting.</ta>
            <ta e="T24" id="Seg_776" s="T19">I lived in Staritsa, and in Staritsa I got married.</ta>
            <ta e="T28" id="Seg_777" s="T24">I had two small children.</ta>
            <ta e="T31" id="Seg_778" s="T28">Тhey usually eat and go fishing’</ta>
            <ta e="T34" id="Seg_779" s="T31">There was taiga there.</ta>
            <ta e="T39" id="Seg_780" s="T34">Cedars, there he(?) hunted squirrels and killed sables.</ta>
            <ta e="T43" id="Seg_781" s="T39">And there I killed a bear.</ta>
            <ta e="T49" id="Seg_782" s="T43">I came to Staritsa, I came back, I was hunting.</ta>
            <ta e="T54" id="Seg_783" s="T49">I lived in Staritsa, I plowed and I sowed cereals.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2" id="Seg_784" s="T0">Ich lebte in Mineevka.</ta>
            <ta e="T14" id="Seg_785" s="T2">Ich ging in die Taiga, jagte Eichhörnchen, dann erlegte ich einen Elch, Zobel habe ich viele erlegt, davon habe ich gelebt.</ta>
            <ta e="T19" id="Seg_786" s="T14">Ich habe Mineevka verlassen.</ta>
            <ta e="T24" id="Seg_787" s="T19">Ich lebte in Straritse, in Straritse heiratete ich.</ta>
            <ta e="T28" id="Seg_788" s="T24">Ich hatte zwei kleine Kinder.</ta>
            <ta e="T31" id="Seg_789" s="T28">Sie essen un fischen.</ta>
            <ta e="T34" id="Seg_790" s="T31">Es gab dort eine Taiga.</ta>
            <ta e="T39" id="Seg_791" s="T34">Zedern, dort jagte er Eichhörnchen und tötete Zobel.</ta>
            <ta e="T43" id="Seg_792" s="T39">Und dort tötete ich einen Bär.</ta>
            <ta e="T49" id="Seg_793" s="T43">Dann kam ich nach Staritsa, kam zurück, ging jagen.</ta>
            <ta e="T54" id="Seg_794" s="T49">Ich lebte in Staritse, ich pflügte, ich sähte Korn.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T2" id="Seg_795" s="T0">в Минеевке жил (я)</ta>
            <ta e="T14" id="Seg_796" s="T2">в тайгу ходил белковал потом лося добыл (убил) соболей много добыл этим и жил</ta>
            <ta e="T19" id="Seg_797" s="T14">с Минеевской заимки я уехал</ta>
            <ta e="T24" id="Seg_798" s="T19">в Старице жил в Старице и (женился?)</ta>
            <ta e="T28" id="Seg_799" s="T24">двое маленьких детей было</ta>
            <ta e="T31" id="Seg_800" s="T28">ходил</ta>
            <ta e="T34" id="Seg_801" s="T31">там тайга была</ta>
            <ta e="T39" id="Seg_802" s="T34">кедрач там белковал соболей добывал</ta>
            <ta e="T43" id="Seg_803" s="T39">там медведя убил</ta>
            <ta e="T49" id="Seg_804" s="T43">пошел в старицу обратно пошел промышлял</ta>
            <ta e="T54" id="Seg_805" s="T49">в старице жил пахал хлеб сеял</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T2" id="Seg_806" s="T0">[WNB:] Mineevka: 57.818775, 82.637299</ta>
            <ta e="T24" id="Seg_807" s="T19">[WNB:] Starica: 56.514927, 34.933586 (??)</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
