<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>MuIP_1964_BearHunting_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">MuIP_1964_BearHunting_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">198</ud-information>
            <ud-information attribute-name="# HIAT:w">161</ud-information>
            <ud-information attribute-name="# e">161</ud-information>
            <ud-information attribute-name="# HIAT:u">28</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="MuIP">
            <abbreviation>MuIP</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="MuIP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T162" id="Seg_0" n="sc" s="T1">
               <ts e="T8" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">kundokɨn</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">matʼtʼöɣɨn</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">ätoɣɨnnä</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">meːmba</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">maːtuŋa</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_19" n="HIAT:w" s="T6">surulʼdi</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_22" n="HIAT:w" s="T7">qullaŋo</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_26" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">na</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">maːtuŋandə</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">me</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_37" n="HIAT:w" s="T11">palʼdʼüsot</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_40" n="HIAT:w" s="T12">innemɨtlasä</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_43" n="HIAT:w" s="T13">kaʒnaj</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_46" n="HIAT:w" s="T14">kan</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_49" n="HIAT:w" s="T15">peːmambigu</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_53" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_55" n="HIAT:w" s="T16">okkɨrɨŋ</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_58" n="HIAT:w" s="T17">man</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_61" n="HIAT:w" s="T18">i</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_64" n="HIAT:w" s="T19">šɨt</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_67" n="HIAT:w" s="T20">innem</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_70" n="HIAT:w" s="T21">čikolʼǯisot</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_73" n="HIAT:w" s="T22">känalam</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_76" n="HIAT:w" s="T23">i</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_79" n="HIAT:w" s="T24">üpsot</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_82" n="HIAT:w" s="T25">peːmabilʼe</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_86" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_88" n="HIAT:w" s="T26">mendɨsot</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_91" n="HIAT:w" s="T27">massuj</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_94" n="HIAT:w" s="T28">päj</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_97" n="HIAT:w" s="T29">seːŋam</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_101" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_103" n="HIAT:w" s="T30">i</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_106" n="HIAT:w" s="T31">nʼärnä</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_109" n="HIAT:w" s="T32">qwännɨ</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_112" n="HIAT:w" s="T33">qwen</ts>
                  <nts id="Seg_113" n="HIAT:ip">.</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_116" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_118" n="HIAT:w" s="T34">qwennäqɨn</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_121" n="HIAT:w" s="T35">me</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_124" n="HIAT:w" s="T36">omdot</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_127" n="HIAT:w" s="T37">nʼäkačegu</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_130" n="HIAT:w" s="T38">okk</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_133" n="HIAT:w" s="T39">wärk</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_136" n="HIAT:w" s="T40">čen</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_139" n="HIAT:w" s="T41">parində</ts>
                  <nts id="Seg_140" n="HIAT:ip">.</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_143" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_145" n="HIAT:w" s="T42">neːkačikten</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_148" n="HIAT:w" s="T43">qwärsot</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_151" n="HIAT:w" s="T44">sejpakam</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_153" n="HIAT:ip">(</nts>
                  <ts e="T46" id="Seg_155" n="HIAT:w" s="T45">säːpakalʼem</ts>
                  <nts id="Seg_156" n="HIAT:ip">)</nts>
                  <nts id="Seg_157" n="HIAT:ip">.</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_160" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_162" n="HIAT:w" s="T46">saːbolʼ</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_165" n="HIAT:w" s="T47">täpam</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_168" n="HIAT:w" s="T48">qolʼdʼisatat</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_171" n="HIAT:w" s="T49">kännalla</ts>
                  <nts id="Seg_172" n="HIAT:ip">.</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_175" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_177" n="HIAT:w" s="T50">čarupčisatit</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_180" n="HIAT:w" s="T51">pändi</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_183" n="HIAT:w" s="T52">i</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_186" n="HIAT:w" s="T53">oltatat</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_189" n="HIAT:w" s="T54">ombottə</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_192" n="HIAT:w" s="T55">muːtku</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_194" n="HIAT:ip">(</nts>
                  <ts e="T57" id="Seg_196" n="HIAT:w" s="T56">muːdlʼe</ts>
                  <nts id="Seg_197" n="HIAT:ip">)</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_200" n="HIAT:w" s="T57">nakoni</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_203" n="HIAT:w" s="T58">nʼünʼokaj</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_206" n="HIAT:w" s="T59">suːrukani</ts>
                  <nts id="Seg_207" n="HIAT:ip">.</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_210" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_212" n="HIAT:w" s="T60">qojam</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_215" n="HIAT:w" s="T61">tʼäran</ts>
                  <nts id="Seg_216" n="HIAT:ip">:</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_219" n="HIAT:w" s="T62">qwärkam</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_222" n="HIAT:w" s="T63">bə</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_225" n="HIAT:w" s="T64">qonnat</ts>
                  <nts id="Seg_226" n="HIAT:ip">.</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_229" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_231" n="HIAT:w" s="T65">man</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_234" n="HIAT:w" s="T66">tʼäraŋ</ts>
                  <nts id="Seg_235" n="HIAT:ip">:</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_238" n="HIAT:w" s="T67">qwällot</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_241" n="HIAT:w" s="T68">nʼärna</ts>
                  <nts id="Seg_242" n="HIAT:ip">.</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_245" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_247" n="HIAT:w" s="T69">qossi</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_250" n="HIAT:w" s="T70">qonǯatit</ts>
                  <nts id="Seg_251" n="HIAT:ip">.</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_254" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_256" n="HIAT:w" s="T71">i</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_259" n="HIAT:w" s="T72">me</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_262" n="HIAT:w" s="T73">üːpsot</ts>
                  <nts id="Seg_263" n="HIAT:ip">.</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_266" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_268" n="HIAT:w" s="T74">assə</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_271" n="HIAT:w" s="T75">kundoktə</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_274" n="HIAT:w" s="T76">mendɨsot</ts>
                  <nts id="Seg_275" n="HIAT:ip">.</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_278" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_280" n="HIAT:w" s="T77">i</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_283" n="HIAT:w" s="T78">qwesiŋen</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_286" n="HIAT:w" s="T79">kännalla</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_289" n="HIAT:w" s="T80">muːtanʼät</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_292" n="HIAT:w" s="T81">tʼejmaːttə</ts>
                  <nts id="Seg_293" n="HIAT:ip">.</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_296" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_298" n="HIAT:w" s="T82">man</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_301" n="HIAT:w" s="T83">qojamnäj</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_304" n="HIAT:w" s="T84">tʼäraŋ</ts>
                  <nts id="Seg_305" n="HIAT:ip">:</nts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_308" n="HIAT:w" s="T85">qoɣattit</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_311" n="HIAT:w" s="T86">qwärgam</ts>
                  <nts id="Seg_312" n="HIAT:ip">.</nts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_315" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_317" n="HIAT:w" s="T87">aːnukon</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_320" n="HIAT:w" s="T88">kännalla</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_323" n="HIAT:w" s="T89">üːtimbisat</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_326" n="HIAT:w" s="T90">püruŋ</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_329" n="HIAT:w" s="T91">tʼejmaːttan</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_332" n="HIAT:w" s="T92">aːŋan</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_335" n="HIAT:w" s="T93">i</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_338" n="HIAT:w" s="T94">muːtsat</ts>
                  <nts id="Seg_339" n="HIAT:ip">.</nts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_342" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_344" n="HIAT:w" s="T95">a</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_347" n="HIAT:w" s="T96">nänni</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_350" n="HIAT:w" s="T97">meː</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_353" n="HIAT:w" s="T98">mittəlʼe</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_356" n="HIAT:w" s="T99">oltot</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_359" n="HIAT:w" s="T100">činɨm</ts>
                  <nts id="Seg_360" n="HIAT:ip">.</nts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_363" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_365" n="HIAT:w" s="T101">i</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_368" n="HIAT:w" s="T102">ok</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_371" n="HIAT:w" s="T103">kännaŋ</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_374" n="HIAT:w" s="T104">ajmərɨŋ</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_377" n="HIAT:w" s="T105">tʼejmattə</ts>
                  <nts id="Seg_378" n="HIAT:ip">.</nts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_381" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_383" n="HIAT:w" s="T106">a</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_386" n="HIAT:w" s="T107">nänni</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_389" n="HIAT:w" s="T108">nʼändat</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_392" n="HIAT:w" s="T109">muqon</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_395" n="HIAT:w" s="T110">aj</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_398" n="HIAT:w" s="T111">meːlugatiː</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_401" n="HIAT:w" s="T112">aj</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_404" n="HIAT:w" s="T113">šɨt</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_407" n="HIAT:w" s="T114">kənnan</ts>
                  <nts id="Seg_408" n="HIAT:ip">.</nts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_411" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_413" n="HIAT:w" s="T115">nännɨ</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_416" n="HIAT:w" s="T116">kännan</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_419" n="HIAT:w" s="T117">muːt</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_422" n="HIAT:w" s="T118">i</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_425" n="HIAT:w" s="T119">qä</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_428" n="HIAT:w" s="T120">qwärɣan</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_431" n="HIAT:w" s="T121">rɨŋɨtʼe</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_434" n="HIAT:w" s="T122">ündus</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_437" n="HIAT:w" s="T123">kalʼä</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_440" n="HIAT:w" s="T124">okkar</ts>
                  <nts id="Seg_441" n="HIAT:ip">.</nts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_444" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_446" n="HIAT:w" s="T125">me</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_449" n="HIAT:w" s="T126">nɨŋsot</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_452" n="HIAT:w" s="T127">tʼejmaːttan</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_455" n="HIAT:w" s="T128">aːŋan</ts>
                  <nts id="Seg_456" n="HIAT:ip">.</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_459" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_461" n="HIAT:w" s="T129">aːčisot</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_464" n="HIAT:w" s="T130">qaɣi</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_467" n="HIAT:w" s="T131">täp</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_470" n="HIAT:w" s="T132">čarunǯiŋ</ts>
                  <nts id="Seg_471" n="HIAT:ip">.</nts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_474" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_476" n="HIAT:w" s="T133">täp</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_479" n="HIAT:w" s="T134">assə</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_482" n="HIAT:w" s="T135">čanǯakus</ts>
                  <nts id="Seg_483" n="HIAT:ip">.</nts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_486" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_488" n="HIAT:w" s="T136">nän</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_491" n="HIAT:w" s="T137">man</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_494" n="HIAT:w" s="T138">čambot</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_496" n="HIAT:ip">(</nts>
                  <ts e="T140" id="Seg_498" n="HIAT:w" s="T139">čaːčot</ts>
                  <nts id="Seg_499" n="HIAT:ip">)</nts>
                  <nts id="Seg_500" n="HIAT:ip">.</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_503" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_505" n="HIAT:w" s="T140">čaːruŋ</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_508" n="HIAT:w" s="T141">okkə</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_511" n="HIAT:w" s="T142">kännaŋ</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_514" n="HIAT:w" s="T143">oːlamdə</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_517" n="HIAT:w" s="T144">palʼdʼeret</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_520" n="HIAT:w" s="T145">i</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_523" n="HIAT:w" s="T146">pɨškərɨt</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_526" n="HIAT:w" s="T147">püruŋ</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_529" n="HIAT:w" s="T148">qɨːwam</ts>
                  <nts id="Seg_530" n="HIAT:ip">.</nts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_533" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_535" n="HIAT:w" s="T149">nänna</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_538" n="HIAT:w" s="T150">čaruŋ</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_541" n="HIAT:w" s="T151">šɨtamdelǯit</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_544" n="HIAT:w" s="T152">nak</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_547" n="HIAT:w" s="T153">moːkalʼpitaj</ts>
                  <nts id="Seg_548" n="HIAT:ip">.</nts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_551" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_553" n="HIAT:w" s="T154">a</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_556" n="HIAT:w" s="T155">narɨmdelǯuj</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_559" n="HIAT:w" s="T156">kännaŋ</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_562" n="HIAT:w" s="T157">qaːliŋ</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_565" n="HIAT:w" s="T158">tʼejmatqan</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_568" n="HIAT:w" s="T159">qwärqan</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_571" n="HIAT:w" s="T160">sutan</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_574" n="HIAT:w" s="T161">iːllən</ts>
                  <nts id="Seg_575" n="HIAT:ip">.</nts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T162" id="Seg_577" n="sc" s="T1">
               <ts e="T2" id="Seg_579" n="e" s="T1">kundokɨn </ts>
               <ts e="T3" id="Seg_581" n="e" s="T2">matʼtʼöɣɨn </ts>
               <ts e="T4" id="Seg_583" n="e" s="T3">ätoɣɨnnä </ts>
               <ts e="T5" id="Seg_585" n="e" s="T4">meːmba </ts>
               <ts e="T6" id="Seg_587" n="e" s="T5">maːtuŋa </ts>
               <ts e="T7" id="Seg_589" n="e" s="T6">surulʼdi </ts>
               <ts e="T8" id="Seg_591" n="e" s="T7">qullaŋo. </ts>
               <ts e="T9" id="Seg_593" n="e" s="T8">na </ts>
               <ts e="T10" id="Seg_595" n="e" s="T9">maːtuŋandə </ts>
               <ts e="T11" id="Seg_597" n="e" s="T10">me </ts>
               <ts e="T12" id="Seg_599" n="e" s="T11">palʼdʼüsot </ts>
               <ts e="T13" id="Seg_601" n="e" s="T12">innemɨtlasä </ts>
               <ts e="T14" id="Seg_603" n="e" s="T13">kaʒnaj </ts>
               <ts e="T15" id="Seg_605" n="e" s="T14">kan </ts>
               <ts e="T16" id="Seg_607" n="e" s="T15">peːmambigu. </ts>
               <ts e="T17" id="Seg_609" n="e" s="T16">okkɨrɨŋ </ts>
               <ts e="T18" id="Seg_611" n="e" s="T17">man </ts>
               <ts e="T19" id="Seg_613" n="e" s="T18">i </ts>
               <ts e="T20" id="Seg_615" n="e" s="T19">šɨt </ts>
               <ts e="T21" id="Seg_617" n="e" s="T20">innem </ts>
               <ts e="T22" id="Seg_619" n="e" s="T21">čikolʼǯisot </ts>
               <ts e="T23" id="Seg_621" n="e" s="T22">känalam </ts>
               <ts e="T24" id="Seg_623" n="e" s="T23">i </ts>
               <ts e="T25" id="Seg_625" n="e" s="T24">üpsot </ts>
               <ts e="T26" id="Seg_627" n="e" s="T25">peːmabilʼe. </ts>
               <ts e="T27" id="Seg_629" n="e" s="T26">mendɨsot </ts>
               <ts e="T28" id="Seg_631" n="e" s="T27">massuj </ts>
               <ts e="T29" id="Seg_633" n="e" s="T28">päj </ts>
               <ts e="T30" id="Seg_635" n="e" s="T29">seːŋam. </ts>
               <ts e="T31" id="Seg_637" n="e" s="T30">i </ts>
               <ts e="T32" id="Seg_639" n="e" s="T31">nʼärnä </ts>
               <ts e="T33" id="Seg_641" n="e" s="T32">qwännɨ </ts>
               <ts e="T34" id="Seg_643" n="e" s="T33">qwen. </ts>
               <ts e="T35" id="Seg_645" n="e" s="T34">qwennäqɨn </ts>
               <ts e="T36" id="Seg_647" n="e" s="T35">me </ts>
               <ts e="T37" id="Seg_649" n="e" s="T36">omdot </ts>
               <ts e="T38" id="Seg_651" n="e" s="T37">nʼäkačegu </ts>
               <ts e="T39" id="Seg_653" n="e" s="T38">okk </ts>
               <ts e="T40" id="Seg_655" n="e" s="T39">wärk </ts>
               <ts e="T41" id="Seg_657" n="e" s="T40">čen </ts>
               <ts e="T42" id="Seg_659" n="e" s="T41">parində. </ts>
               <ts e="T43" id="Seg_661" n="e" s="T42">neːkačikten </ts>
               <ts e="T44" id="Seg_663" n="e" s="T43">qwärsot </ts>
               <ts e="T45" id="Seg_665" n="e" s="T44">sejpakam </ts>
               <ts e="T46" id="Seg_667" n="e" s="T45">(säːpakalʼem). </ts>
               <ts e="T47" id="Seg_669" n="e" s="T46">saːbolʼ </ts>
               <ts e="T48" id="Seg_671" n="e" s="T47">täpam </ts>
               <ts e="T49" id="Seg_673" n="e" s="T48">qolʼdʼisatat </ts>
               <ts e="T50" id="Seg_675" n="e" s="T49">kännalla. </ts>
               <ts e="T51" id="Seg_677" n="e" s="T50">čarupčisatit </ts>
               <ts e="T52" id="Seg_679" n="e" s="T51">pändi </ts>
               <ts e="T53" id="Seg_681" n="e" s="T52">i </ts>
               <ts e="T54" id="Seg_683" n="e" s="T53">oltatat </ts>
               <ts e="T55" id="Seg_685" n="e" s="T54">ombottə </ts>
               <ts e="T56" id="Seg_687" n="e" s="T55">muːtku </ts>
               <ts e="T57" id="Seg_689" n="e" s="T56">(muːdlʼe) </ts>
               <ts e="T58" id="Seg_691" n="e" s="T57">nakoni </ts>
               <ts e="T59" id="Seg_693" n="e" s="T58">nʼünʼokaj </ts>
               <ts e="T60" id="Seg_695" n="e" s="T59">suːrukani. </ts>
               <ts e="T61" id="Seg_697" n="e" s="T60">qojam </ts>
               <ts e="T62" id="Seg_699" n="e" s="T61">tʼäran: </ts>
               <ts e="T63" id="Seg_701" n="e" s="T62">qwärkam </ts>
               <ts e="T64" id="Seg_703" n="e" s="T63">bə </ts>
               <ts e="T65" id="Seg_705" n="e" s="T64">qonnat. </ts>
               <ts e="T66" id="Seg_707" n="e" s="T65">man </ts>
               <ts e="T67" id="Seg_709" n="e" s="T66">tʼäraŋ: </ts>
               <ts e="T68" id="Seg_711" n="e" s="T67">qwällot </ts>
               <ts e="T69" id="Seg_713" n="e" s="T68">nʼärna. </ts>
               <ts e="T70" id="Seg_715" n="e" s="T69">qossi </ts>
               <ts e="T71" id="Seg_717" n="e" s="T70">qonǯatit. </ts>
               <ts e="T72" id="Seg_719" n="e" s="T71">i </ts>
               <ts e="T73" id="Seg_721" n="e" s="T72">me </ts>
               <ts e="T74" id="Seg_723" n="e" s="T73">üːpsot. </ts>
               <ts e="T75" id="Seg_725" n="e" s="T74">assə </ts>
               <ts e="T76" id="Seg_727" n="e" s="T75">kundoktə </ts>
               <ts e="T77" id="Seg_729" n="e" s="T76">mendɨsot. </ts>
               <ts e="T78" id="Seg_731" n="e" s="T77">i </ts>
               <ts e="T79" id="Seg_733" n="e" s="T78">qwesiŋen </ts>
               <ts e="T80" id="Seg_735" n="e" s="T79">kännalla </ts>
               <ts e="T81" id="Seg_737" n="e" s="T80">muːtanʼät </ts>
               <ts e="T82" id="Seg_739" n="e" s="T81">tʼejmaːttə. </ts>
               <ts e="T83" id="Seg_741" n="e" s="T82">man </ts>
               <ts e="T84" id="Seg_743" n="e" s="T83">qojamnäj </ts>
               <ts e="T85" id="Seg_745" n="e" s="T84">tʼäraŋ: </ts>
               <ts e="T86" id="Seg_747" n="e" s="T85">qoɣattit </ts>
               <ts e="T87" id="Seg_749" n="e" s="T86">qwärgam. </ts>
               <ts e="T88" id="Seg_751" n="e" s="T87">aːnukon </ts>
               <ts e="T89" id="Seg_753" n="e" s="T88">kännalla </ts>
               <ts e="T90" id="Seg_755" n="e" s="T89">üːtimbisat </ts>
               <ts e="T91" id="Seg_757" n="e" s="T90">püruŋ </ts>
               <ts e="T92" id="Seg_759" n="e" s="T91">tʼejmaːttan </ts>
               <ts e="T93" id="Seg_761" n="e" s="T92">aːŋan </ts>
               <ts e="T94" id="Seg_763" n="e" s="T93">i </ts>
               <ts e="T95" id="Seg_765" n="e" s="T94">muːtsat. </ts>
               <ts e="T96" id="Seg_767" n="e" s="T95">a </ts>
               <ts e="T97" id="Seg_769" n="e" s="T96">nänni </ts>
               <ts e="T98" id="Seg_771" n="e" s="T97">meː </ts>
               <ts e="T99" id="Seg_773" n="e" s="T98">mittəlʼe </ts>
               <ts e="T100" id="Seg_775" n="e" s="T99">oltot </ts>
               <ts e="T101" id="Seg_777" n="e" s="T100">činɨm. </ts>
               <ts e="T102" id="Seg_779" n="e" s="T101">i </ts>
               <ts e="T103" id="Seg_781" n="e" s="T102">ok </ts>
               <ts e="T104" id="Seg_783" n="e" s="T103">kännaŋ </ts>
               <ts e="T105" id="Seg_785" n="e" s="T104">ajmərɨŋ </ts>
               <ts e="T106" id="Seg_787" n="e" s="T105">tʼejmattə. </ts>
               <ts e="T107" id="Seg_789" n="e" s="T106">a </ts>
               <ts e="T108" id="Seg_791" n="e" s="T107">nänni </ts>
               <ts e="T109" id="Seg_793" n="e" s="T108">nʼändat </ts>
               <ts e="T110" id="Seg_795" n="e" s="T109">muqon </ts>
               <ts e="T111" id="Seg_797" n="e" s="T110">aj </ts>
               <ts e="T112" id="Seg_799" n="e" s="T111">meːlugatiː </ts>
               <ts e="T113" id="Seg_801" n="e" s="T112">aj </ts>
               <ts e="T114" id="Seg_803" n="e" s="T113">šɨt </ts>
               <ts e="T115" id="Seg_805" n="e" s="T114">kənnan. </ts>
               <ts e="T116" id="Seg_807" n="e" s="T115">nännɨ </ts>
               <ts e="T117" id="Seg_809" n="e" s="T116">kännan </ts>
               <ts e="T118" id="Seg_811" n="e" s="T117">muːt </ts>
               <ts e="T119" id="Seg_813" n="e" s="T118">i </ts>
               <ts e="T120" id="Seg_815" n="e" s="T119">qä </ts>
               <ts e="T121" id="Seg_817" n="e" s="T120">qwärɣan </ts>
               <ts e="T122" id="Seg_819" n="e" s="T121">rɨŋɨtʼe </ts>
               <ts e="T123" id="Seg_821" n="e" s="T122">ündus </ts>
               <ts e="T124" id="Seg_823" n="e" s="T123">kalʼä </ts>
               <ts e="T125" id="Seg_825" n="e" s="T124">okkar. </ts>
               <ts e="T126" id="Seg_827" n="e" s="T125">me </ts>
               <ts e="T127" id="Seg_829" n="e" s="T126">nɨŋsot </ts>
               <ts e="T128" id="Seg_831" n="e" s="T127">tʼejmaːttan </ts>
               <ts e="T129" id="Seg_833" n="e" s="T128">aːŋan. </ts>
               <ts e="T130" id="Seg_835" n="e" s="T129">aːčisot </ts>
               <ts e="T131" id="Seg_837" n="e" s="T130">qaɣi </ts>
               <ts e="T132" id="Seg_839" n="e" s="T131">täp </ts>
               <ts e="T133" id="Seg_841" n="e" s="T132">čarunǯiŋ. </ts>
               <ts e="T134" id="Seg_843" n="e" s="T133">täp </ts>
               <ts e="T135" id="Seg_845" n="e" s="T134">assə </ts>
               <ts e="T136" id="Seg_847" n="e" s="T135">čanǯakus. </ts>
               <ts e="T137" id="Seg_849" n="e" s="T136">nän </ts>
               <ts e="T138" id="Seg_851" n="e" s="T137">man </ts>
               <ts e="T139" id="Seg_853" n="e" s="T138">čambot </ts>
               <ts e="T140" id="Seg_855" n="e" s="T139">(čaːčot). </ts>
               <ts e="T141" id="Seg_857" n="e" s="T140">čaːruŋ </ts>
               <ts e="T142" id="Seg_859" n="e" s="T141">okkə </ts>
               <ts e="T143" id="Seg_861" n="e" s="T142">kännaŋ </ts>
               <ts e="T144" id="Seg_863" n="e" s="T143">oːlamdə </ts>
               <ts e="T145" id="Seg_865" n="e" s="T144">palʼdʼeret </ts>
               <ts e="T146" id="Seg_867" n="e" s="T145">i </ts>
               <ts e="T147" id="Seg_869" n="e" s="T146">pɨškərɨt </ts>
               <ts e="T148" id="Seg_871" n="e" s="T147">püruŋ </ts>
               <ts e="T149" id="Seg_873" n="e" s="T148">qɨːwam. </ts>
               <ts e="T150" id="Seg_875" n="e" s="T149">nänna </ts>
               <ts e="T151" id="Seg_877" n="e" s="T150">čaruŋ </ts>
               <ts e="T152" id="Seg_879" n="e" s="T151">šɨtamdelǯit </ts>
               <ts e="T153" id="Seg_881" n="e" s="T152">nak </ts>
               <ts e="T154" id="Seg_883" n="e" s="T153">moːkalʼpitaj. </ts>
               <ts e="T155" id="Seg_885" n="e" s="T154">a </ts>
               <ts e="T156" id="Seg_887" n="e" s="T155">narɨmdelǯuj </ts>
               <ts e="T157" id="Seg_889" n="e" s="T156">kännaŋ </ts>
               <ts e="T158" id="Seg_891" n="e" s="T157">qaːliŋ </ts>
               <ts e="T159" id="Seg_893" n="e" s="T158">tʼejmatqan </ts>
               <ts e="T160" id="Seg_895" n="e" s="T159">qwärqan </ts>
               <ts e="T161" id="Seg_897" n="e" s="T160">sutan </ts>
               <ts e="T162" id="Seg_899" n="e" s="T161">iːllən. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T8" id="Seg_900" s="T1">MuIP_1964_BearHunting_nar.001 (001.001)</ta>
            <ta e="T16" id="Seg_901" s="T8">MuIP_1964_BearHunting_nar.002 (001.002)</ta>
            <ta e="T26" id="Seg_902" s="T16">MuIP_1964_BearHunting_nar.003 (001.003)</ta>
            <ta e="T30" id="Seg_903" s="T26">MuIP_1964_BearHunting_nar.004 (001.004)</ta>
            <ta e="T34" id="Seg_904" s="T30">MuIP_1964_BearHunting_nar.005 (001.005)</ta>
            <ta e="T42" id="Seg_905" s="T34">MuIP_1964_BearHunting_nar.006 (001.006)</ta>
            <ta e="T46" id="Seg_906" s="T42">MuIP_1964_BearHunting_nar.007 (001.007)</ta>
            <ta e="T50" id="Seg_907" s="T46">MuIP_1964_BearHunting_nar.008 (001.008)</ta>
            <ta e="T60" id="Seg_908" s="T50">MuIP_1964_BearHunting_nar.009 (001.009)</ta>
            <ta e="T65" id="Seg_909" s="T60">MuIP_1964_BearHunting_nar.010 (001.010)</ta>
            <ta e="T69" id="Seg_910" s="T65">MuIP_1964_BearHunting_nar.011 (001.011)</ta>
            <ta e="T71" id="Seg_911" s="T69">MuIP_1964_BearHunting_nar.012 (001.012)</ta>
            <ta e="T74" id="Seg_912" s="T71">MuIP_1964_BearHunting_nar.013 (001.013)</ta>
            <ta e="T77" id="Seg_913" s="T74">MuIP_1964_BearHunting_nar.014 (001.014)</ta>
            <ta e="T82" id="Seg_914" s="T77">MuIP_1964_BearHunting_nar.015 (001.015)</ta>
            <ta e="T87" id="Seg_915" s="T82">MuIP_1964_BearHunting_nar.016 (001.016)</ta>
            <ta e="T95" id="Seg_916" s="T87">MuIP_1964_BearHunting_nar.017 (001.017)</ta>
            <ta e="T101" id="Seg_917" s="T95">MuIP_1964_BearHunting_nar.018 (001.018)</ta>
            <ta e="T106" id="Seg_918" s="T101">MuIP_1964_BearHunting_nar.019 (001.019)</ta>
            <ta e="T115" id="Seg_919" s="T106">MuIP_1964_BearHunting_nar.020 (001.020)</ta>
            <ta e="T125" id="Seg_920" s="T115">MuIP_1964_BearHunting_nar.021 (001.021)</ta>
            <ta e="T129" id="Seg_921" s="T125">MuIP_1964_BearHunting_nar.022 (001.022)</ta>
            <ta e="T133" id="Seg_922" s="T129">MuIP_1964_BearHunting_nar.023 (001.023)</ta>
            <ta e="T136" id="Seg_923" s="T133">MuIP_1964_BearHunting_nar.024 (001.024)</ta>
            <ta e="T140" id="Seg_924" s="T136">MuIP_1964_BearHunting_nar.025 (001.025)</ta>
            <ta e="T149" id="Seg_925" s="T140">MuIP_1964_BearHunting_nar.026 (001.026)</ta>
            <ta e="T154" id="Seg_926" s="T149">MuIP_1964_BearHunting_nar.027 (001.027)</ta>
            <ta e="T162" id="Seg_927" s="T154">MuIP_1964_BearHunting_nar.028 (001.028)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T8" id="Seg_928" s="T1">кун′докын ма′тʼтʼӧɣын ′ӓтоɣыннӓ ′ме̄мба ′ма̄туңа ′сурулʼди ′kуllаңо.</ta>
            <ta e="T16" id="Seg_929" s="T8">на ′ма̄ту′ңандъ ме ′палʼдʼӱсот и′нне‵мыт′lасӓ ′кажнай кан ′пе̄мамбигу.</ta>
            <ta e="T26" id="Seg_930" s="T16">′оккырың ман и шыт и′ннем т(ч)и′колʼджисот кӓ′наlам и ′ӱпсот ′пе̄мабилʼе.</ta>
            <ta e="T30" id="Seg_931" s="T26">′мендысот ′массуй пӓй ′се̄(ӓ)ңам.</ta>
            <ta e="T34" id="Seg_932" s="T30">и ′нʼӓрнӓ kwӓ′нны kвен.</ta>
            <ta e="T42" id="Seg_933" s="T34">kwе′ннӓkын ме ′омдот нʼӓ′катшегу окк ′вӓрк ′тшен ′париндъ.</ta>
            <ta e="T46" id="Seg_934" s="T42">не̄′катшиктен kwӓр′сот сей′пакам (сӓ̄′пакалʼем).</ta>
            <ta e="T50" id="Seg_935" s="T46">′са̄болʼ ′тӓпам ′kолʼдʼи′сатат ‵кӓ′ннаllа.</ta>
            <ta e="T60" id="Seg_936" s="T50">′тшаруп‵тши′сатит ′пӓнди и ′оlтатат ом′боттъ ′мӯтку (мӯдлʼе) ′накони ′нʼӱнʼокай ‵сӯру′кани.</ta>
            <ta e="T65" id="Seg_937" s="T60">′kоjам тʼӓ′ран: ′kwӓркам бъ kо′ннат.</ta>
            <ta e="T69" id="Seg_938" s="T65">ман тʼӓ′раң: ′kwӓ′llот ′нʼӓрна.</ta>
            <ta e="T71" id="Seg_939" s="T69">′kосси ′kонджа‵тит.</ta>
            <ta e="T74" id="Seg_940" s="T71">и ме ′ӱ̄псот.</ta>
            <ta e="T77" id="Seg_941" s="T74">′ассъ кун′доктъ ′мендысот.</ta>
            <ta e="T82" id="Seg_942" s="T77">и ′kwесиңен кӓ′ннаllа ′мӯтанʼӓт ′тʼей ′ма̄ттъ.</ta>
            <ta e="T87" id="Seg_943" s="T82">ман kо′jамнӓй тʼӓ′раң: kо′ɣаттит kwӓр′г(к)ам.</ta>
            <ta e="T95" id="Seg_944" s="T87">′а̄нукон кӓ′ннаllа ′ӱ̄тимбисат ′пӱруң тʼей′ма̄ттан а̄′ңан и ′мӯтсат.</ta>
            <ta e="T101" id="Seg_945" s="T95">а ′нӓнни ме̄ ′миттъ′лʼе ′оlтот ′тшиным.</ta>
            <ta e="T106" id="Seg_946" s="T101">и ок ′кӓннаң ′аймърың тʼей′маттъ.</ta>
            <ta e="T115" id="Seg_947" s="T106">а ′нӓнни ′нʼӓндат му′kон ай ме̄lу′гатӣ ай шыт ‵къ̊′ннан.</ta>
            <ta e="T125" id="Seg_948" s="T115">′нӓнны ′кӓннан ′мӯт и kӓ kwӓрɣан ′рыңытʼе ӱн′дус ′калʼӓ ок′кар.</ta>
            <ta e="T129" id="Seg_949" s="T125">ме ′ныңсот тʼей′ма̄ттан ′а̄ңан.</ta>
            <ta e="T133" id="Seg_950" s="T129">′а̄тшисот ′kаɣи тӓп тша′рунджиң.</ta>
            <ta e="T136" id="Seg_951" s="T133">тӓп ′ассъ тшан′джакус.</ta>
            <ta e="T140" id="Seg_952" s="T136">нӓн ман ′тшамбот (тша̄′тшот).</ta>
            <ta e="T149" id="Seg_953" s="T140">′тша̄руң ′оккъ ′кӓннаң о̄′lамдъ ′палʼдʼерет и ′пышкърыт ′пӱруң ′kы̄вам.</ta>
            <ta e="T154" id="Seg_954" s="T149">′нӓнна ′тшʼаруң ′шытам′делджит нак ′мо̄калʼпитай.</ta>
            <ta e="T162" id="Seg_955" s="T154">а ′нарымдеlджуй кӓннаң kа̄лиң ‵тʼей′матkан ′kwӓрkан ′сутан ӣ′llън.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T8" id="Seg_956" s="T1">kundokɨn matʼtʼöɣɨn ätoɣɨnnä meːmba maːtuŋa surulʼdi qullaŋo.</ta>
            <ta e="T16" id="Seg_957" s="T8">na maːtuŋandə me palʼdʼüsot innemɨtlasä kaʒnaj kan peːmambigu.</ta>
            <ta e="T26" id="Seg_958" s="T16">okkɨrɨŋ man i šɨt innem t(č)ikolʼǯisot känalam i üpsot peːmabilʼe.</ta>
            <ta e="T30" id="Seg_959" s="T26">mendɨsot massuj päj seː(ä)ŋam.</ta>
            <ta e="T34" id="Seg_960" s="T30">i nʼärnä qwännɨ qven.</ta>
            <ta e="T42" id="Seg_961" s="T34">qwennäqɨn me omdot nʼäkatšegu okk värk tšen parində.</ta>
            <ta e="T46" id="Seg_962" s="T42">neːkatšikten qwärsot sejpakam (säːpakalʼem).</ta>
            <ta e="T50" id="Seg_963" s="T46">saːbolʼ täpam qolʼdʼisatat kännalla.</ta>
            <ta e="T60" id="Seg_964" s="T50">tšaruptšisatit pändi i oltatat ombottə muːtku (muːdlʼe) nakoni nʼünʼokaj suːrukani.</ta>
            <ta e="T65" id="Seg_965" s="T60">qojam tʼäran: qwärkam bə qonnat.</ta>
            <ta e="T69" id="Seg_966" s="T65">man tʼäraŋ: qwällot nʼärna.</ta>
            <ta e="T71" id="Seg_967" s="T69">qossi qonǯatit.</ta>
            <ta e="T74" id="Seg_968" s="T71">i me üːpsot.</ta>
            <ta e="T77" id="Seg_969" s="T74">assə kundoktə mendɨsot.</ta>
            <ta e="T82" id="Seg_970" s="T77">i qwesiŋen kännalla muːtanʼät tʼej maːttə.</ta>
            <ta e="T87" id="Seg_971" s="T82">man qojamnäj tʼäraŋ: qoɣattit qwärg(k)am.</ta>
            <ta e="T95" id="Seg_972" s="T87">aːnukon kännalla üːtimbisat püruŋ tʼejmaːttan aːŋan i muːtsat.</ta>
            <ta e="T101" id="Seg_973" s="T95">a nänni meː mittəlʼe oltot tšinɨm.</ta>
            <ta e="T106" id="Seg_974" s="T101">i ok kännaŋ ajmərɨŋ tʼejmattə.</ta>
            <ta e="T115" id="Seg_975" s="T106">a nänni nʼändat muqon aj meːlugatiː aj šɨt kənnan.</ta>
            <ta e="T125" id="Seg_976" s="T115">nännɨ kännan muːt i qä qwärɣan rɨŋɨtʼe ündus kalʼä okkar.</ta>
            <ta e="T129" id="Seg_977" s="T125">me nɨŋsot tʼejmaːttan aːŋan.</ta>
            <ta e="T133" id="Seg_978" s="T129">aːtšisot qaɣi täp tšarunǯiŋ.</ta>
            <ta e="T136" id="Seg_979" s="T133">täp assə tšanǯakus.</ta>
            <ta e="T140" id="Seg_980" s="T136">nän man tšambot (tšaːtšot).</ta>
            <ta e="T149" id="Seg_981" s="T140">tšaːruŋ okkə kännaŋ oːlamdə palʼdʼeret i pɨškərɨt püruŋ qɨːvam.</ta>
            <ta e="T154" id="Seg_982" s="T149">nänna tšaruŋ šɨtamdelǯit nak moːkalʼpitaj.</ta>
            <ta e="T162" id="Seg_983" s="T154">a narɨmdelǯuj kännaŋ qaːliŋ tʼejmatqan qwärqan sutan iːllən.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T8" id="Seg_984" s="T1">kundokɨn matʼtʼöɣɨn ätoɣɨnnä meːmba maːtuŋa surulʼdi qullaŋo. </ta>
            <ta e="T16" id="Seg_985" s="T8">na maːtuŋandə me palʼdʼüsot innemɨtlasä kaʒnaj kan peːmambigu. </ta>
            <ta e="T26" id="Seg_986" s="T16">okkɨrɨŋ man i šɨt innem čikolʼǯisot känalam i üpsot peːmabilʼe. </ta>
            <ta e="T30" id="Seg_987" s="T26">mendɨsot massuj päj seːŋam. </ta>
            <ta e="T34" id="Seg_988" s="T30">i nʼärnä qwännɨ qwen. </ta>
            <ta e="T42" id="Seg_989" s="T34">qwennäqɨn me omdot nʼäkačegu okk wärk čen parində. </ta>
            <ta e="T46" id="Seg_990" s="T42">neːkačikten qwärsot sejpakam (säːpakalʼem). </ta>
            <ta e="T50" id="Seg_991" s="T46">saːbolʼ täpam qolʼdʼisatat kännalla. </ta>
            <ta e="T60" id="Seg_992" s="T50">čarupčisatit pändi i oltatat ombottə muːtku (muːdlʼe) nakoni nʼünʼokaj suːrukani. </ta>
            <ta e="T65" id="Seg_993" s="T60">qojam tʼäran: qwärkam bə qonnat. </ta>
            <ta e="T69" id="Seg_994" s="T65">man tʼäraŋ: qwällot nʼärna. </ta>
            <ta e="T71" id="Seg_995" s="T69">qossi qonǯatit. </ta>
            <ta e="T74" id="Seg_996" s="T71">i me üːpsot. </ta>
            <ta e="T77" id="Seg_997" s="T74">assə kundoktə mendɨsot. </ta>
            <ta e="T82" id="Seg_998" s="T77">i qwesiŋen kännalla muːtanʼät tʼejmaːttə. </ta>
            <ta e="T87" id="Seg_999" s="T82">man qojamnäj tʼäraŋ: qoɣattit qwärgam. </ta>
            <ta e="T95" id="Seg_1000" s="T87">aːnukon kännalla üːtimbisat püruŋ tʼejmaːttan aːŋan i muːtsat. </ta>
            <ta e="T101" id="Seg_1001" s="T95">a nänni meː mittəlʼe oltot činɨm. </ta>
            <ta e="T106" id="Seg_1002" s="T101">i ok kännaŋ ajmərɨŋ tʼejmattə. </ta>
            <ta e="T115" id="Seg_1003" s="T106">a nänni nʼändat muqon aj meːlugatiː aj šɨt kənnan. </ta>
            <ta e="T125" id="Seg_1004" s="T115">nännɨ kännan muːt i qä qwärɣan rɨŋɨtʼe ündus kalʼä okkar. </ta>
            <ta e="T129" id="Seg_1005" s="T125">me nɨŋsot tʼejmaːttan aːŋan. </ta>
            <ta e="T133" id="Seg_1006" s="T129">aːčisot qaɣi täp čarunǯiŋ. </ta>
            <ta e="T136" id="Seg_1007" s="T133">täp assə čanǯakus. </ta>
            <ta e="T140" id="Seg_1008" s="T136">nän man čambot (čaːčot). </ta>
            <ta e="T149" id="Seg_1009" s="T140">čaːruŋ okkə kännaŋ oːlamdə palʼdʼeret i pɨškərɨt püruŋ qɨːwam. </ta>
            <ta e="T154" id="Seg_1010" s="T149">nänna čaruŋ šɨtamdelǯit nak moːkalʼpitaj. </ta>
            <ta e="T162" id="Seg_1011" s="T154">a narɨmdelǯuj kännaŋ qaːliŋ tʼejmatqan qwärqan sutan iːllən. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_1012" s="T1">kundo-kɨn</ta>
            <ta e="T3" id="Seg_1013" s="T2">matʼtʼö-ɣɨn</ta>
            <ta e="T4" id="Seg_1014" s="T3">äto-ɣɨnnä</ta>
            <ta e="T5" id="Seg_1015" s="T4">meː-mba</ta>
            <ta e="T6" id="Seg_1016" s="T5">maːt-u-ŋa</ta>
            <ta e="T7" id="Seg_1017" s="T6">suru-lʼdi</ta>
            <ta e="T8" id="Seg_1018" s="T7">qul-la-ŋo</ta>
            <ta e="T9" id="Seg_1019" s="T8">na</ta>
            <ta e="T10" id="Seg_1020" s="T9">maːt-u-ŋandə</ta>
            <ta e="T11" id="Seg_1021" s="T10">me</ta>
            <ta e="T12" id="Seg_1022" s="T11">palʼdʼü-s-ot</ta>
            <ta e="T13" id="Seg_1023" s="T12">inne-mɨ-t-la-sä</ta>
            <ta e="T14" id="Seg_1024" s="T13">kaʒnaj</ta>
            <ta e="T15" id="Seg_1025" s="T14">ka-n</ta>
            <ta e="T16" id="Seg_1026" s="T15">peːma-mbi-gu</ta>
            <ta e="T17" id="Seg_1027" s="T16">okkɨr-ɨ-ŋ</ta>
            <ta e="T18" id="Seg_1028" s="T17">man</ta>
            <ta e="T19" id="Seg_1029" s="T18">i</ta>
            <ta e="T20" id="Seg_1030" s="T19">šɨt</ta>
            <ta e="T21" id="Seg_1031" s="T20">inne-m</ta>
            <ta e="T22" id="Seg_1032" s="T21">čiko-lʼǯi-s-ot</ta>
            <ta e="T23" id="Seg_1033" s="T22">käna-la-m</ta>
            <ta e="T24" id="Seg_1034" s="T23">i</ta>
            <ta e="T25" id="Seg_1035" s="T24">üp-s-ot</ta>
            <ta e="T26" id="Seg_1036" s="T25">peːma-bi-lʼe</ta>
            <ta e="T27" id="Seg_1037" s="T26">mendɨ-s-ot</ta>
            <ta e="T28" id="Seg_1038" s="T27">massu-j</ta>
            <ta e="T29" id="Seg_1039" s="T28">pä-j</ta>
            <ta e="T30" id="Seg_1040" s="T29">seːŋa-m</ta>
            <ta e="T31" id="Seg_1041" s="T30">i</ta>
            <ta e="T32" id="Seg_1042" s="T31">nʼärnä</ta>
            <ta e="T33" id="Seg_1043" s="T32">qwän-nɨ</ta>
            <ta e="T34" id="Seg_1044" s="T33">qwe-n</ta>
            <ta e="T35" id="Seg_1045" s="T34">qwenn-ä-qɨn</ta>
            <ta e="T36" id="Seg_1046" s="T35">me</ta>
            <ta e="T37" id="Seg_1047" s="T36">omd-ot</ta>
            <ta e="T38" id="Seg_1048" s="T37">nʼäka-če-gu</ta>
            <ta e="T39" id="Seg_1049" s="T38">okk</ta>
            <ta e="T40" id="Seg_1050" s="T39">wärk</ta>
            <ta e="T41" id="Seg_1051" s="T40">če-n</ta>
            <ta e="T42" id="Seg_1052" s="T41">pari-ndə</ta>
            <ta e="T43" id="Seg_1053" s="T42">neːka-či-k-ten</ta>
            <ta e="T44" id="Seg_1054" s="T43">qwär-s-ot</ta>
            <ta e="T45" id="Seg_1055" s="T44">sejpa-ka-m</ta>
            <ta e="T46" id="Seg_1056" s="T45">säːpa-ka-lʼe-m</ta>
            <ta e="T47" id="Seg_1057" s="T46">saːbolʼ</ta>
            <ta e="T48" id="Seg_1058" s="T47">täp-a-m</ta>
            <ta e="T49" id="Seg_1059" s="T48">qolʼdʼi-sa-tat</ta>
            <ta e="T50" id="Seg_1060" s="T49">känna-lla</ta>
            <ta e="T51" id="Seg_1061" s="T50">čaru-pči-sa-tit</ta>
            <ta e="T52" id="Seg_1062" s="T51">pä-ndi</ta>
            <ta e="T53" id="Seg_1063" s="T52">i</ta>
            <ta e="T54" id="Seg_1064" s="T53">olta-tat</ta>
            <ta e="T55" id="Seg_1065" s="T54">ombottə</ta>
            <ta e="T56" id="Seg_1066" s="T55">muːt-ku</ta>
            <ta e="T57" id="Seg_1067" s="T56">muːd-lʼe</ta>
            <ta e="T58" id="Seg_1068" s="T57">na-ko-ni</ta>
            <ta e="T59" id="Seg_1069" s="T58">nʼünʼo-kaj</ta>
            <ta e="T60" id="Seg_1070" s="T59">suːru-ka-ni</ta>
            <ta e="T61" id="Seg_1071" s="T60">qoja-m</ta>
            <ta e="T62" id="Seg_1072" s="T61">tʼära-ŋ</ta>
            <ta e="T63" id="Seg_1073" s="T62">qwärka-m</ta>
            <ta e="T64" id="Seg_1074" s="T63">bə</ta>
            <ta e="T65" id="Seg_1075" s="T64">qo-nn-at</ta>
            <ta e="T66" id="Seg_1076" s="T65">man</ta>
            <ta e="T67" id="Seg_1077" s="T66">tʼära-n</ta>
            <ta e="T68" id="Seg_1078" s="T67">qwäl-l-ot</ta>
            <ta e="T69" id="Seg_1079" s="T68">nʼärna</ta>
            <ta e="T70" id="Seg_1080" s="T69">qossi</ta>
            <ta e="T71" id="Seg_1081" s="T70">qo-nǯa-tit</ta>
            <ta e="T72" id="Seg_1082" s="T71">i</ta>
            <ta e="T73" id="Seg_1083" s="T72">me</ta>
            <ta e="T74" id="Seg_1084" s="T73">üːp-s-ot</ta>
            <ta e="T75" id="Seg_1085" s="T74">assə</ta>
            <ta e="T76" id="Seg_1086" s="T75">kundok-tə</ta>
            <ta e="T77" id="Seg_1087" s="T76">mendɨ-s-ot</ta>
            <ta e="T78" id="Seg_1088" s="T77">i</ta>
            <ta e="T79" id="Seg_1089" s="T78">qwesiŋ-e-n</ta>
            <ta e="T80" id="Seg_1090" s="T79">känna-lla</ta>
            <ta e="T81" id="Seg_1091" s="T80">muːt-a-nʼä-t</ta>
            <ta e="T82" id="Seg_1092" s="T81">tʼejmaːt-tə</ta>
            <ta e="T83" id="Seg_1093" s="T82">man</ta>
            <ta e="T84" id="Seg_1094" s="T83">qoja-m-näj</ta>
            <ta e="T85" id="Seg_1095" s="T84">tʼära-n</ta>
            <ta e="T86" id="Seg_1096" s="T85">qo-ɣa-ttit</ta>
            <ta e="T87" id="Seg_1097" s="T86">qwärga-m</ta>
            <ta e="T88" id="Seg_1098" s="T87">aːnukon</ta>
            <ta e="T89" id="Seg_1099" s="T88">känna-lla</ta>
            <ta e="T90" id="Seg_1100" s="T89">üːti-mbi-sa-t</ta>
            <ta e="T91" id="Seg_1101" s="T90">püru-ŋ</ta>
            <ta e="T92" id="Seg_1102" s="T91">tʼejmaːtt-a-n</ta>
            <ta e="T93" id="Seg_1103" s="T92">aːŋ-a-n</ta>
            <ta e="T94" id="Seg_1104" s="T93">i</ta>
            <ta e="T95" id="Seg_1105" s="T94">muːt-sa-t</ta>
            <ta e="T96" id="Seg_1106" s="T95">a</ta>
            <ta e="T97" id="Seg_1107" s="T96">nänni</ta>
            <ta e="T98" id="Seg_1108" s="T97">meː</ta>
            <ta e="T99" id="Seg_1109" s="T98">mittə-lʼe</ta>
            <ta e="T100" id="Seg_1110" s="T99">olt-ot</ta>
            <ta e="T101" id="Seg_1111" s="T100">činɨm</ta>
            <ta e="T102" id="Seg_1112" s="T101">i</ta>
            <ta e="T103" id="Seg_1113" s="T102">ok</ta>
            <ta e="T104" id="Seg_1114" s="T103">kännaŋ</ta>
            <ta e="T105" id="Seg_1115" s="T104">ajmərɨ-ŋ</ta>
            <ta e="T106" id="Seg_1116" s="T105">tʼejmat-tə</ta>
            <ta e="T107" id="Seg_1117" s="T106">a</ta>
            <ta e="T108" id="Seg_1118" s="T107">nänni</ta>
            <ta e="T109" id="Seg_1119" s="T108">nʼä-n-dat</ta>
            <ta e="T110" id="Seg_1120" s="T109">muqon</ta>
            <ta e="T111" id="Seg_1121" s="T110">aj</ta>
            <ta e="T112" id="Seg_1122" s="T111">meːlu-ga-tiː</ta>
            <ta e="T113" id="Seg_1123" s="T112">aj</ta>
            <ta e="T114" id="Seg_1124" s="T113">šɨt</ta>
            <ta e="T115" id="Seg_1125" s="T114">kənna-n</ta>
            <ta e="T116" id="Seg_1126" s="T115">nännɨ</ta>
            <ta e="T117" id="Seg_1127" s="T116">känna-n</ta>
            <ta e="T118" id="Seg_1128" s="T117">muːt</ta>
            <ta e="T119" id="Seg_1129" s="T118">i</ta>
            <ta e="T120" id="Seg_1130" s="T119">qä</ta>
            <ta e="T121" id="Seg_1131" s="T120">qwärqa-n</ta>
            <ta e="T122" id="Seg_1132" s="T121">rɨŋɨtʼe</ta>
            <ta e="T123" id="Seg_1133" s="T122">ündu-s</ta>
            <ta e="T124" id="Seg_1134" s="T123">kalʼä</ta>
            <ta e="T125" id="Seg_1135" s="T124">okkar</ta>
            <ta e="T126" id="Seg_1136" s="T125">me</ta>
            <ta e="T127" id="Seg_1137" s="T126">nɨŋ-s-ot</ta>
            <ta e="T128" id="Seg_1138" s="T127">tʼejmaːtt-a-n</ta>
            <ta e="T129" id="Seg_1139" s="T128">aːŋ-a-n</ta>
            <ta e="T130" id="Seg_1140" s="T129">aːči-s-ot</ta>
            <ta e="T131" id="Seg_1141" s="T130">qaɣi</ta>
            <ta e="T132" id="Seg_1142" s="T131">täp</ta>
            <ta e="T133" id="Seg_1143" s="T132">čaru-nǯi-ŋ</ta>
            <ta e="T134" id="Seg_1144" s="T133">täp</ta>
            <ta e="T135" id="Seg_1145" s="T134">assə</ta>
            <ta e="T136" id="Seg_1146" s="T135">čanǯa-ku-s</ta>
            <ta e="T137" id="Seg_1147" s="T136">nän</ta>
            <ta e="T138" id="Seg_1148" s="T137">man</ta>
            <ta e="T139" id="Seg_1149" s="T138">ča-mb-ot</ta>
            <ta e="T140" id="Seg_1150" s="T139">čaːč-ot</ta>
            <ta e="T141" id="Seg_1151" s="T140">čaːr-u-ŋ</ta>
            <ta e="T142" id="Seg_1152" s="T141">okkə</ta>
            <ta e="T143" id="Seg_1153" s="T142">kännaŋ</ta>
            <ta e="T144" id="Seg_1154" s="T143">oːla-m-də</ta>
            <ta e="T145" id="Seg_1155" s="T144">*pa-lʼdʼe-r-e-t</ta>
            <ta e="T146" id="Seg_1156" s="T145">i</ta>
            <ta e="T147" id="Seg_1157" s="T146">pɨškərɨ-t</ta>
            <ta e="T148" id="Seg_1158" s="T147">püru-ŋ</ta>
            <ta e="T149" id="Seg_1159" s="T148">qɨːwa-m</ta>
            <ta e="T150" id="Seg_1160" s="T149">nänna</ta>
            <ta e="T151" id="Seg_1161" s="T150">čaru-ŋ</ta>
            <ta e="T152" id="Seg_1162" s="T151">šɨta-mdelǯi-t</ta>
            <ta e="T153" id="Seg_1163" s="T152">nak</ta>
            <ta e="T154" id="Seg_1164" s="T153">moːka-lʼ-pitaj</ta>
            <ta e="T155" id="Seg_1165" s="T154">a</ta>
            <ta e="T156" id="Seg_1166" s="T155">narɨ-mdelǯu-j</ta>
            <ta e="T157" id="Seg_1167" s="T156">kännaŋ</ta>
            <ta e="T158" id="Seg_1168" s="T157">qaːli-ŋ</ta>
            <ta e="T159" id="Seg_1169" s="T158">tʼejmat-qan</ta>
            <ta e="T160" id="Seg_1170" s="T159">qwärqa-n</ta>
            <ta e="T161" id="Seg_1171" s="T160">suta-n</ta>
            <ta e="T162" id="Seg_1172" s="T161">iːllə-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1173" s="T1">kundok-qən</ta>
            <ta e="T3" id="Seg_1174" s="T2">matʼtʼi-qən</ta>
            <ta e="T4" id="Seg_1175" s="T3">eːto-qɨnnɨ</ta>
            <ta e="T5" id="Seg_1176" s="T4">meː-mbɨ</ta>
            <ta e="T6" id="Seg_1177" s="T5">maːt-ɨ-ka</ta>
            <ta e="T7" id="Seg_1178" s="T6">suːrɨ-lʼdi</ta>
            <ta e="T8" id="Seg_1179" s="T7">qum-la-nqo</ta>
            <ta e="T9" id="Seg_1180" s="T8">na</ta>
            <ta e="T10" id="Seg_1181" s="T9">maːt-ɨ-qəntɨ</ta>
            <ta e="T11" id="Seg_1182" s="T10">meː</ta>
            <ta e="T12" id="Seg_1183" s="T11">paldʼu-sɨ-ut</ta>
            <ta e="T13" id="Seg_1184" s="T12">imnʼa-mɨ-n-la-se</ta>
            <ta e="T14" id="Seg_1185" s="T13">kaːǯna</ta>
            <ta e="T15" id="Seg_1186" s="T14">ka-n</ta>
            <ta e="T16" id="Seg_1187" s="T15">pemɨ-mbɨ-gu</ta>
            <ta e="T17" id="Seg_1188" s="T16">okkɨr-ɨ-k</ta>
            <ta e="T18" id="Seg_1189" s="T17">man</ta>
            <ta e="T19" id="Seg_1190" s="T18">i</ta>
            <ta e="T20" id="Seg_1191" s="T19">šittə</ta>
            <ta e="T21" id="Seg_1192" s="T20">imnʼa-mɨ</ta>
            <ta e="T22" id="Seg_1193" s="T21">čikə-lʼčǝ-sɨ-ut</ta>
            <ta e="T23" id="Seg_1194" s="T22">kanak-la-m</ta>
            <ta e="T24" id="Seg_1195" s="T23">i</ta>
            <ta e="T25" id="Seg_1196" s="T24">übə-sɨ-ut</ta>
            <ta e="T26" id="Seg_1197" s="T25">pemɨ-mbɨ-le</ta>
            <ta e="T27" id="Seg_1198" s="T26">mendi-sɨ-ut</ta>
            <ta e="T28" id="Seg_1199" s="T27">massu-lʼ</ta>
            <ta e="T29" id="Seg_1200" s="T28">po-lʼ</ta>
            <ta e="T30" id="Seg_1201" s="T29">seaŋgə-m</ta>
            <ta e="T31" id="Seg_1202" s="T30">i</ta>
            <ta e="T32" id="Seg_1203" s="T31">nʼarne</ta>
            <ta e="T33" id="Seg_1204" s="T32">qwən-ŋɨ</ta>
            <ta e="T34" id="Seg_1205" s="T33">qwɛ-t</ta>
            <ta e="T35" id="Seg_1206" s="T34">qwen-ɨ-qən</ta>
            <ta e="T36" id="Seg_1207" s="T35">meː</ta>
            <ta e="T37" id="Seg_1208" s="T36">omdɨ-ut</ta>
            <ta e="T38" id="Seg_1209" s="T37">nʼekke-nče-gu</ta>
            <ta e="T39" id="Seg_1210" s="T38">okkɨr</ta>
            <ta e="T40" id="Seg_1211" s="T39">wərkə</ta>
            <ta e="T41" id="Seg_1212" s="T40">če-n</ta>
            <ta e="T42" id="Seg_1213" s="T41">par-ndɨ</ta>
            <ta e="T43" id="Seg_1214" s="T42">nʼekke-či-ku-ten</ta>
            <ta e="T44" id="Seg_1215" s="T43">qwär-sɨ-ut</ta>
            <ta e="T45" id="Seg_1216" s="T44">seba-ka-m</ta>
            <ta e="T46" id="Seg_1217" s="T45">seba-ka-lʼe-m</ta>
            <ta e="T47" id="Seg_1218" s="T46">saːbolʼ</ta>
            <ta e="T48" id="Seg_1219" s="T47">tap-ɨ-m</ta>
            <ta e="T49" id="Seg_1220" s="T48">qolʼdʼi-sɨ-tɨt</ta>
            <ta e="T50" id="Seg_1221" s="T49">kanak-la</ta>
            <ta e="T51" id="Seg_1222" s="T50">čarɨ-ptɨ-sɨ-tɨt</ta>
            <ta e="T52" id="Seg_1223" s="T51">po-ndɨ</ta>
            <ta e="T53" id="Seg_1224" s="T52">i</ta>
            <ta e="T54" id="Seg_1225" s="T53">oldǝ-tɨt</ta>
            <ta e="T55" id="Seg_1226" s="T54">ombottə</ta>
            <ta e="T56" id="Seg_1227" s="T55">muːt-gu</ta>
            <ta e="T57" id="Seg_1228" s="T56">muːt-le</ta>
            <ta e="T58" id="Seg_1229" s="T57">na-ka-nɨ</ta>
            <ta e="T59" id="Seg_1230" s="T58">nʼünʼü-ka</ta>
            <ta e="T60" id="Seg_1231" s="T59">suːrǝm-ka-nɨ</ta>
            <ta e="T61" id="Seg_1232" s="T60">qoja-mɨ</ta>
            <ta e="T62" id="Seg_1233" s="T61">tʼarɨ-n</ta>
            <ta e="T63" id="Seg_1234" s="T62">qwärqa-m</ta>
            <ta e="T64" id="Seg_1235" s="T63">bɨ</ta>
            <ta e="T65" id="Seg_1236" s="T64">qo-ŋɨ-ut</ta>
            <ta e="T66" id="Seg_1237" s="T65">man</ta>
            <ta e="T67" id="Seg_1238" s="T66">tʼarɨ-n</ta>
            <ta e="T68" id="Seg_1239" s="T67">qwən-lä-ut</ta>
            <ta e="T69" id="Seg_1240" s="T68">nʼarne</ta>
            <ta e="T70" id="Seg_1241" s="T69">qossi</ta>
            <ta e="T71" id="Seg_1242" s="T70">qo-nǯɨ-tɨt</ta>
            <ta e="T72" id="Seg_1243" s="T71">i</ta>
            <ta e="T73" id="Seg_1244" s="T72">meː</ta>
            <ta e="T74" id="Seg_1245" s="T73">üːb-sɨ-ut</ta>
            <ta e="T75" id="Seg_1246" s="T74">assɨ</ta>
            <ta e="T76" id="Seg_1247" s="T75">kundok-ndɨ</ta>
            <ta e="T77" id="Seg_1248" s="T76">mendi-sɨ-ut</ta>
            <ta e="T78" id="Seg_1249" s="T77">i</ta>
            <ta e="T79" id="Seg_1250" s="T78">qwesiŋ-ɨ-n</ta>
            <ta e="T80" id="Seg_1251" s="T79">kanak-la</ta>
            <ta e="T81" id="Seg_1252" s="T80">muːt-ɨ-ŋɨ-tɨt</ta>
            <ta e="T82" id="Seg_1253" s="T81">tʼajmaːt-ndɨ</ta>
            <ta e="T83" id="Seg_1254" s="T82">man</ta>
            <ta e="T84" id="Seg_1255" s="T83">qoja-m-naj</ta>
            <ta e="T85" id="Seg_1256" s="T84">tʼarɨ-n</ta>
            <ta e="T86" id="Seg_1257" s="T85">qo-ŋɨ-tɨt</ta>
            <ta e="T87" id="Seg_1258" s="T86">qwärqa-m</ta>
            <ta e="T88" id="Seg_1259" s="T87">aːnuːkkon</ta>
            <ta e="T89" id="Seg_1260" s="T88">kanak-la</ta>
            <ta e="T90" id="Seg_1261" s="T89">üːdi-mbɨ-sɨ-tɨt</ta>
            <ta e="T91" id="Seg_1262" s="T90">pürru-k</ta>
            <ta e="T92" id="Seg_1263" s="T91">tʼajmaːt-ɨ-n</ta>
            <ta e="T93" id="Seg_1264" s="T92">aːŋ-ɨ-n</ta>
            <ta e="T94" id="Seg_1265" s="T93">i</ta>
            <ta e="T95" id="Seg_1266" s="T94">muːt-sɨ-tɨt</ta>
            <ta e="T96" id="Seg_1267" s="T95">a</ta>
            <ta e="T97" id="Seg_1268" s="T96">nɨːnɨ</ta>
            <ta e="T98" id="Seg_1269" s="T97">meː</ta>
            <ta e="T99" id="Seg_1270" s="T98">mittɨ-le</ta>
            <ta e="T100" id="Seg_1271" s="T99">oldǝ-ut</ta>
            <ta e="T101" id="Seg_1272" s="T100">činɨm</ta>
            <ta e="T102" id="Seg_1273" s="T101">i</ta>
            <ta e="T103" id="Seg_1274" s="T102">okkɨr</ta>
            <ta e="T104" id="Seg_1275" s="T103">kanak</ta>
            <ta e="T105" id="Seg_1276" s="T104">ajmərɨ-n</ta>
            <ta e="T106" id="Seg_1277" s="T105">tʼajmaːt-ndɨ</ta>
            <ta e="T107" id="Seg_1278" s="T106">a</ta>
            <ta e="T108" id="Seg_1279" s="T107">nɨːnɨ</ta>
            <ta e="T109" id="Seg_1280" s="T108">nʼaː-n-tɨt</ta>
            <ta e="T110" id="Seg_1281" s="T109">moqənä</ta>
            <ta e="T111" id="Seg_1282" s="T110">aj</ta>
            <ta e="T112" id="Seg_1283" s="T111">meːlu-ku-di</ta>
            <ta e="T113" id="Seg_1284" s="T112">aj</ta>
            <ta e="T114" id="Seg_1285" s="T113">šittə</ta>
            <ta e="T115" id="Seg_1286" s="T114">kanak-t</ta>
            <ta e="T116" id="Seg_1287" s="T115">nɨːnɨ</ta>
            <ta e="T117" id="Seg_1288" s="T116">kanak-n</ta>
            <ta e="T118" id="Seg_1289" s="T117">muːdɨ</ta>
            <ta e="T119" id="Seg_1290" s="T118">i</ta>
            <ta e="T120" id="Seg_1291" s="T119">qəː</ta>
            <ta e="T121" id="Seg_1292" s="T120">qwärqa-n</ta>
            <ta e="T122" id="Seg_1293" s="T121">rɨŋɨtʼe</ta>
            <ta e="T123" id="Seg_1294" s="T122">ündɨ-sɨ</ta>
            <ta e="T124" id="Seg_1295" s="T123">kalʼä</ta>
            <ta e="T125" id="Seg_1296" s="T124">okkɨr</ta>
            <ta e="T126" id="Seg_1297" s="T125">meː</ta>
            <ta e="T127" id="Seg_1298" s="T126">nɨŋ-sɨ-ut</ta>
            <ta e="T128" id="Seg_1299" s="T127">tʼajmaːt-ɨ-n</ta>
            <ta e="T129" id="Seg_1300" s="T128">aːŋ-ɨ-n</ta>
            <ta e="T130" id="Seg_1301" s="T129">atʼɨ-sɨ-ut</ta>
            <ta e="T131" id="Seg_1302" s="T130">qaqä</ta>
            <ta e="T132" id="Seg_1303" s="T131">tap</ta>
            <ta e="T133" id="Seg_1304" s="T132">čarɨ-nǯɨ-n</ta>
            <ta e="T134" id="Seg_1305" s="T133">tap</ta>
            <ta e="T135" id="Seg_1306" s="T134">assɨ</ta>
            <ta e="T136" id="Seg_1307" s="T135">čanǯɨ-ku-sɨ</ta>
            <ta e="T137" id="Seg_1308" s="T136">nɨːnɨ</ta>
            <ta e="T138" id="Seg_1309" s="T137">man</ta>
            <ta e="T139" id="Seg_1310" s="T138">čʼo-mbɨ-ut</ta>
            <ta e="T140" id="Seg_1311" s="T139">tʼaččɨ-ut</ta>
            <ta e="T141" id="Seg_1312" s="T140">čaːr-ɨ-n</ta>
            <ta e="T142" id="Seg_1313" s="T141">okkɨr</ta>
            <ta e="T143" id="Seg_1314" s="T142">kanak</ta>
            <ta e="T144" id="Seg_1315" s="T143">olɨ-m-tɨ</ta>
            <ta e="T145" id="Seg_1316" s="T144">*pa-lʼčǝ-r-ɨ-tɨ</ta>
            <ta e="T146" id="Seg_1317" s="T145">i</ta>
            <ta e="T147" id="Seg_1318" s="T146">pɨškərɨ-tɨ</ta>
            <ta e="T148" id="Seg_1319" s="T147">pürru-k</ta>
            <ta e="T149" id="Seg_1320" s="T148">qawə-m</ta>
            <ta e="T150" id="Seg_1321" s="T149">nɨːnɨ</ta>
            <ta e="T151" id="Seg_1322" s="T150">čarɨ-n</ta>
            <ta e="T152" id="Seg_1323" s="T151">šittə-mtelǯij-tɨ</ta>
            <ta e="T153" id="Seg_1324" s="T152">naj</ta>
            <ta e="T154" id="Seg_1325" s="T153">moːɣa-lɨ-mbɨdi</ta>
            <ta e="T155" id="Seg_1326" s="T154">a</ta>
            <ta e="T156" id="Seg_1327" s="T155">nakkɨr-mtelǯij-lʼ</ta>
            <ta e="T157" id="Seg_1328" s="T156">kanak</ta>
            <ta e="T158" id="Seg_1329" s="T157">qalɨ-n</ta>
            <ta e="T159" id="Seg_1330" s="T158">tʼajmaːt-qən</ta>
            <ta e="T160" id="Seg_1331" s="T159">qwärqa-n</ta>
            <ta e="T161" id="Seg_1332" s="T160">suta-n</ta>
            <ta e="T162" id="Seg_1333" s="T161">ɨlə-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1334" s="T1">distance-LOC</ta>
            <ta e="T3" id="Seg_1335" s="T2">taiga-LOC</ta>
            <ta e="T4" id="Seg_1336" s="T3">village-EL</ta>
            <ta e="T5" id="Seg_1337" s="T4">do-PST.NAR.[3SG.S]</ta>
            <ta e="T6" id="Seg_1338" s="T5">house-EP-DIM</ta>
            <ta e="T7" id="Seg_1339" s="T6">hunt-CAP.ADJZ</ta>
            <ta e="T8" id="Seg_1340" s="T7">human.being-PL-TRL</ta>
            <ta e="T9" id="Seg_1341" s="T8">this</ta>
            <ta e="T10" id="Seg_1342" s="T9">house-EP-ILL.3SG</ta>
            <ta e="T11" id="Seg_1343" s="T10">we.NOM</ta>
            <ta e="T12" id="Seg_1344" s="T11">go-PST-1PL</ta>
            <ta e="T13" id="Seg_1345" s="T12">brother-something-GEN-PL-COM</ta>
            <ta e="T14" id="Seg_1346" s="T13">every</ta>
            <ta e="T15" id="Seg_1347" s="T14">winter-ADV.LOC</ta>
            <ta e="T16" id="Seg_1348" s="T15">hunt-DUR-INF</ta>
            <ta e="T17" id="Seg_1349" s="T16">one-EP-ADVZ</ta>
            <ta e="T18" id="Seg_1350" s="T17">I.NOM</ta>
            <ta e="T19" id="Seg_1351" s="T18">and</ta>
            <ta e="T20" id="Seg_1352" s="T19">two</ta>
            <ta e="T21" id="Seg_1353" s="T20">brother.[NOM]-1SG</ta>
            <ta e="T22" id="Seg_1354" s="T21">untie-PFV-PST-1PL</ta>
            <ta e="T23" id="Seg_1355" s="T22">dog-PL-ACC</ta>
            <ta e="T24" id="Seg_1356" s="T23">and</ta>
            <ta e="T25" id="Seg_1357" s="T24">begin-PST-1PL</ta>
            <ta e="T26" id="Seg_1358" s="T25">hunt-DUR-CVB</ta>
            <ta e="T27" id="Seg_1359" s="T26">pass-PST-1PL</ta>
            <ta e="T28" id="Seg_1360" s="T27">cedar-ADJZ</ta>
            <ta e="T29" id="Seg_1361" s="T28">tree-ADJZ</ta>
            <ta e="T30" id="Seg_1362" s="T29">forest-ACC</ta>
            <ta e="T31" id="Seg_1363" s="T30">and</ta>
            <ta e="T32" id="Seg_1364" s="T31">forward</ta>
            <ta e="T33" id="Seg_1365" s="T32">go.away-CO.[3SG.S]</ta>
            <ta e="T34" id="Seg_1366" s="T33">birch-PL.[NOM]</ta>
            <ta e="T35" id="Seg_1367" s="T34">birch.forest-EP-LOC</ta>
            <ta e="T36" id="Seg_1368" s="T35">we.NOM</ta>
            <ta e="T37" id="Seg_1369" s="T36">sit.down-1PL</ta>
            <ta e="T38" id="Seg_1370" s="T37">pull-IPFV3-INF</ta>
            <ta e="T39" id="Seg_1371" s="T38">one</ta>
            <ta e="T40" id="Seg_1372" s="T39">big</ta>
            <ta e="T41" id="Seg_1373" s="T40">%%-GEN</ta>
            <ta e="T42" id="Seg_1374" s="T41">top-ILL</ta>
            <ta e="T43" id="Seg_1375" s="T42">pull-DRV-ACTN-%%</ta>
            <ta e="T44" id="Seg_1376" s="T43">beckon-PST-1PL</ta>
            <ta e="T45" id="Seg_1377" s="T44">chipmunk-DIM-ACC</ta>
            <ta e="T46" id="Seg_1378" s="T45">chipmunk-DIM-%%-ACC</ta>
            <ta e="T47" id="Seg_1379" s="T46">rightly</ta>
            <ta e="T48" id="Seg_1380" s="T47">(s)he-EP-ACC</ta>
            <ta e="T49" id="Seg_1381" s="T48">see-PST-3PL</ta>
            <ta e="T50" id="Seg_1382" s="T49">dog-PL.[NOM]</ta>
            <ta e="T51" id="Seg_1383" s="T50">appear-CAUS-PST-3PL</ta>
            <ta e="T52" id="Seg_1384" s="T51">tree-ILL</ta>
            <ta e="T53" id="Seg_1385" s="T52">and</ta>
            <ta e="T54" id="Seg_1386" s="T53">start-3PL</ta>
            <ta e="T55" id="Seg_1387" s="T54">very</ta>
            <ta e="T56" id="Seg_1388" s="T55">bark-INF</ta>
            <ta e="T57" id="Seg_1389" s="T56">bark-CVB</ta>
            <ta e="T58" id="Seg_1390" s="T57">this-DIM-ALL</ta>
            <ta e="T59" id="Seg_1391" s="T58">small-DIM</ta>
            <ta e="T60" id="Seg_1392" s="T59">wild.animal-DIM-ALL</ta>
            <ta e="T61" id="Seg_1393" s="T60">younger.sibling-1SG</ta>
            <ta e="T62" id="Seg_1394" s="T61">say-3SG.S</ta>
            <ta e="T63" id="Seg_1395" s="T62">bear-ACC</ta>
            <ta e="T64" id="Seg_1396" s="T63">IRREAL</ta>
            <ta e="T65" id="Seg_1397" s="T64">see-CO-1PL</ta>
            <ta e="T66" id="Seg_1398" s="T65">I.NOM</ta>
            <ta e="T67" id="Seg_1399" s="T66">say-3SG.S</ta>
            <ta e="T68" id="Seg_1400" s="T67">go.away-OPT-1PL</ta>
            <ta e="T69" id="Seg_1401" s="T68">forward</ta>
            <ta e="T70" id="Seg_1402" s="T69">maybe</ta>
            <ta e="T71" id="Seg_1403" s="T70">see-FUT-3PL</ta>
            <ta e="T72" id="Seg_1404" s="T71">and</ta>
            <ta e="T73" id="Seg_1405" s="T72">we.NOM</ta>
            <ta e="T74" id="Seg_1406" s="T73">go-PST-1PL</ta>
            <ta e="T75" id="Seg_1407" s="T74">NEG</ta>
            <ta e="T76" id="Seg_1408" s="T75">distance-ILL</ta>
            <ta e="T77" id="Seg_1409" s="T76">pass-PST-1PL</ta>
            <ta e="T78" id="Seg_1410" s="T77">and</ta>
            <ta e="T79" id="Seg_1411" s="T78">birch.forest-EP-ADV.LOC</ta>
            <ta e="T80" id="Seg_1412" s="T79">dog-PL.[NOM]</ta>
            <ta e="T81" id="Seg_1413" s="T80">bark-EP-CO-3PL</ta>
            <ta e="T82" id="Seg_1414" s="T81">lair-ILL</ta>
            <ta e="T83" id="Seg_1415" s="T82">I.NOM</ta>
            <ta e="T84" id="Seg_1416" s="T83">younger.sibling-ACC-EMPH</ta>
            <ta e="T85" id="Seg_1417" s="T84">say-3SG.S</ta>
            <ta e="T86" id="Seg_1418" s="T85">see-CO-3PL</ta>
            <ta e="T87" id="Seg_1419" s="T86">bear-ACC</ta>
            <ta e="T88" id="Seg_1420" s="T87">first</ta>
            <ta e="T89" id="Seg_1421" s="T88">dog-PL.[NOM]</ta>
            <ta e="T90" id="Seg_1422" s="T89">run.away-DUR-PST-3PL</ta>
            <ta e="T91" id="Seg_1423" s="T90">ring-ADVZ</ta>
            <ta e="T92" id="Seg_1424" s="T91">lair-EP-GEN</ta>
            <ta e="T93" id="Seg_1425" s="T92">mouth-EP-ADV.LOC</ta>
            <ta e="T94" id="Seg_1426" s="T93">and</ta>
            <ta e="T95" id="Seg_1427" s="T94">bark-PST-3PL</ta>
            <ta e="T96" id="Seg_1428" s="T95">but</ta>
            <ta e="T97" id="Seg_1429" s="T96">then</ta>
            <ta e="T98" id="Seg_1430" s="T97">we.NOM</ta>
            <ta e="T99" id="Seg_1431" s="T98">reach-CVB</ta>
            <ta e="T100" id="Seg_1432" s="T99">begin-1PL</ta>
            <ta e="T101" id="Seg_1433" s="T100">%%</ta>
            <ta e="T102" id="Seg_1434" s="T101">and</ta>
            <ta e="T103" id="Seg_1435" s="T102">one</ta>
            <ta e="T104" id="Seg_1436" s="T103">dog.[NOM]</ta>
            <ta e="T105" id="Seg_1437" s="T104">jump-3SG.S</ta>
            <ta e="T106" id="Seg_1438" s="T105">lair-ILL</ta>
            <ta e="T107" id="Seg_1439" s="T106">but</ta>
            <ta e="T108" id="Seg_1440" s="T107">then</ta>
            <ta e="T109" id="Seg_1441" s="T108">friend-GEN-3PL</ta>
            <ta e="T110" id="Seg_1442" s="T109">after</ta>
            <ta e="T111" id="Seg_1443" s="T110">again</ta>
            <ta e="T112" id="Seg_1444" s="T111">%%-HAB-3DU.S</ta>
            <ta e="T113" id="Seg_1445" s="T112">again</ta>
            <ta e="T114" id="Seg_1446" s="T113">two</ta>
            <ta e="T115" id="Seg_1447" s="T114">dog-PL</ta>
            <ta e="T116" id="Seg_1448" s="T115">then</ta>
            <ta e="T117" id="Seg_1449" s="T116">dog-GEN</ta>
            <ta e="T118" id="Seg_1450" s="T117">barking.[NOM]</ta>
            <ta e="T119" id="Seg_1451" s="T118">and</ta>
            <ta e="T120" id="Seg_1452" s="T119">steep.bank.[NOM]</ta>
            <ta e="T121" id="Seg_1453" s="T120">bear-GEN</ta>
            <ta e="T122" id="Seg_1454" s="T121">growling.[NOM]</ta>
            <ta e="T123" id="Seg_1455" s="T122">hear-PST.[3SG.S]</ta>
            <ta e="T124" id="Seg_1456" s="T123">%%</ta>
            <ta e="T125" id="Seg_1457" s="T124">one</ta>
            <ta e="T126" id="Seg_1458" s="T125">we.NOM</ta>
            <ta e="T127" id="Seg_1459" s="T126">stand-PST-1PL</ta>
            <ta e="T128" id="Seg_1460" s="T127">lair-EP-GEN</ta>
            <ta e="T129" id="Seg_1461" s="T128">mouth-EP-ADV.LOC</ta>
            <ta e="T130" id="Seg_1462" s="T129">watch.for-PST-1PL</ta>
            <ta e="T131" id="Seg_1463" s="T130">when</ta>
            <ta e="T132" id="Seg_1464" s="T131">(s)he.[NOM]</ta>
            <ta e="T133" id="Seg_1465" s="T132">appear-FUT-3SG.S</ta>
            <ta e="T134" id="Seg_1466" s="T133">(s)he.[NOM]</ta>
            <ta e="T135" id="Seg_1467" s="T134">NEG</ta>
            <ta e="T136" id="Seg_1468" s="T135">go.out-HAB-PST.[3SG.S]</ta>
            <ta e="T137" id="Seg_1469" s="T136">then</ta>
            <ta e="T138" id="Seg_1470" s="T137">I.NOM</ta>
            <ta e="T139" id="Seg_1471" s="T138">shoot-DUR-1PL</ta>
            <ta e="T140" id="Seg_1472" s="T139">shoot-1PL</ta>
            <ta e="T141" id="Seg_1473" s="T140">jump.out-EP-3SG.S</ta>
            <ta e="T142" id="Seg_1474" s="T141">one</ta>
            <ta e="T143" id="Seg_1475" s="T142">dog.[NOM]</ta>
            <ta e="T144" id="Seg_1476" s="T143">head-ACC-3SG</ta>
            <ta e="T145" id="Seg_1477" s="T144">wrap-PFV-FRQ-EP-3SG.O</ta>
            <ta e="T146" id="Seg_1478" s="T145">and</ta>
            <ta e="T147" id="Seg_1479" s="T146">%%-3SG.O</ta>
            <ta e="T148" id="Seg_1480" s="T147">ring-ADVZ</ta>
            <ta e="T149" id="Seg_1481" s="T148">blood-ACC</ta>
            <ta e="T150" id="Seg_1482" s="T149">then</ta>
            <ta e="T151" id="Seg_1483" s="T150">go.out-3SG.S</ta>
            <ta e="T152" id="Seg_1484" s="T151">two-ORD-3SG</ta>
            <ta e="T153" id="Seg_1485" s="T152">also</ta>
            <ta e="T154" id="Seg_1486" s="T153">beat-RES-PTCP.PST</ta>
            <ta e="T155" id="Seg_1487" s="T154">but</ta>
            <ta e="T156" id="Seg_1488" s="T155">three-ORD-ADJZ</ta>
            <ta e="T157" id="Seg_1489" s="T156">dog.[NOM]</ta>
            <ta e="T158" id="Seg_1490" s="T157">stay-3SG.S</ta>
            <ta e="T159" id="Seg_1491" s="T158">lair-LOC</ta>
            <ta e="T160" id="Seg_1492" s="T159">bear-GEN</ta>
            <ta e="T161" id="Seg_1493" s="T160">%%-GEN</ta>
            <ta e="T162" id="Seg_1494" s="T161">bottom-ADV.LOC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1495" s="T1">даль-LOC</ta>
            <ta e="T3" id="Seg_1496" s="T2">тайга-LOC</ta>
            <ta e="T4" id="Seg_1497" s="T3">деревня-EL</ta>
            <ta e="T5" id="Seg_1498" s="T4">делать-PST.NAR.[3SG.S]</ta>
            <ta e="T6" id="Seg_1499" s="T5">дом-EP-DIM</ta>
            <ta e="T7" id="Seg_1500" s="T6">охотиться-CAP.ADJZ</ta>
            <ta e="T8" id="Seg_1501" s="T7">человек-PL-TRL</ta>
            <ta e="T9" id="Seg_1502" s="T8">этот</ta>
            <ta e="T10" id="Seg_1503" s="T9">дом-EP-ILL.3SG</ta>
            <ta e="T11" id="Seg_1504" s="T10">мы.NOM</ta>
            <ta e="T12" id="Seg_1505" s="T11">ходить-PST-1PL</ta>
            <ta e="T13" id="Seg_1506" s="T12">брат-нечто-GEN-PL-COM</ta>
            <ta e="T14" id="Seg_1507" s="T13">каждый</ta>
            <ta e="T15" id="Seg_1508" s="T14">зима-ADV.LOC</ta>
            <ta e="T16" id="Seg_1509" s="T15">охотиться-DUR-INF</ta>
            <ta e="T17" id="Seg_1510" s="T16">один-EP-ADVZ</ta>
            <ta e="T18" id="Seg_1511" s="T17">я.NOM</ta>
            <ta e="T19" id="Seg_1512" s="T18">и</ta>
            <ta e="T20" id="Seg_1513" s="T19">два</ta>
            <ta e="T21" id="Seg_1514" s="T20">брат.[NOM]-1SG</ta>
            <ta e="T22" id="Seg_1515" s="T21">отвязать-PFV-PST-1PL</ta>
            <ta e="T23" id="Seg_1516" s="T22">собака-PL-ACC</ta>
            <ta e="T24" id="Seg_1517" s="T23">и</ta>
            <ta e="T25" id="Seg_1518" s="T24">начать-PST-1PL</ta>
            <ta e="T26" id="Seg_1519" s="T25">охотиться-DUR-CVB</ta>
            <ta e="T27" id="Seg_1520" s="T26">пройти-PST-1PL</ta>
            <ta e="T28" id="Seg_1521" s="T27">кедр-ADJZ</ta>
            <ta e="T29" id="Seg_1522" s="T28">дерево-ADJZ</ta>
            <ta e="T30" id="Seg_1523" s="T29">лес-ACC</ta>
            <ta e="T31" id="Seg_1524" s="T30">и</ta>
            <ta e="T32" id="Seg_1525" s="T31">вперёд</ta>
            <ta e="T33" id="Seg_1526" s="T32">уйти-CO.[3SG.S]</ta>
            <ta e="T34" id="Seg_1527" s="T33">берёза-PL.[NOM]</ta>
            <ta e="T35" id="Seg_1528" s="T34">березняк-EP-LOC</ta>
            <ta e="T36" id="Seg_1529" s="T35">мы.NOM</ta>
            <ta e="T37" id="Seg_1530" s="T36">сесть-1PL</ta>
            <ta e="T38" id="Seg_1531" s="T37">тянуть-IPFV3-INF</ta>
            <ta e="T39" id="Seg_1532" s="T38">один</ta>
            <ta e="T40" id="Seg_1533" s="T39">большой</ta>
            <ta e="T41" id="Seg_1534" s="T40">%%-GEN</ta>
            <ta e="T42" id="Seg_1535" s="T41">верх-ILL</ta>
            <ta e="T43" id="Seg_1536" s="T42">тянуть-DRV-ACTN-%%</ta>
            <ta e="T44" id="Seg_1537" s="T43">подманить-PST-1PL</ta>
            <ta e="T45" id="Seg_1538" s="T44">бурундук-DIM-ACC</ta>
            <ta e="T46" id="Seg_1539" s="T45">бурундук-DIM-%%-ACC</ta>
            <ta e="T47" id="Seg_1540" s="T46">верно</ta>
            <ta e="T48" id="Seg_1541" s="T47">он(а)-EP-ACC</ta>
            <ta e="T49" id="Seg_1542" s="T48">увидеть-PST-3PL</ta>
            <ta e="T50" id="Seg_1543" s="T49">собака-PL.[NOM]</ta>
            <ta e="T51" id="Seg_1544" s="T50">выскакивать-CAUS-PST-3PL</ta>
            <ta e="T52" id="Seg_1545" s="T51">дерево-ILL</ta>
            <ta e="T53" id="Seg_1546" s="T52">и</ta>
            <ta e="T54" id="Seg_1547" s="T53">начать-3PL</ta>
            <ta e="T55" id="Seg_1548" s="T54">очень</ta>
            <ta e="T56" id="Seg_1549" s="T55">лаять-INF</ta>
            <ta e="T57" id="Seg_1550" s="T56">лаять-CVB</ta>
            <ta e="T58" id="Seg_1551" s="T57">этот-DIM-ALL</ta>
            <ta e="T59" id="Seg_1552" s="T58">маленький-DIM</ta>
            <ta e="T60" id="Seg_1553" s="T59">зверь-DIM-ALL</ta>
            <ta e="T61" id="Seg_1554" s="T60">младший.брат/сестра-1SG</ta>
            <ta e="T62" id="Seg_1555" s="T61">сказать-3SG.S</ta>
            <ta e="T63" id="Seg_1556" s="T62">медведь-ACC</ta>
            <ta e="T64" id="Seg_1557" s="T63">IRREAL</ta>
            <ta e="T65" id="Seg_1558" s="T64">увидеть-CO-1PL</ta>
            <ta e="T66" id="Seg_1559" s="T65">я.NOM</ta>
            <ta e="T67" id="Seg_1560" s="T66">сказать-3SG.S</ta>
            <ta e="T68" id="Seg_1561" s="T67">уйти-OPT-1PL</ta>
            <ta e="T69" id="Seg_1562" s="T68">вперёд</ta>
            <ta e="T70" id="Seg_1563" s="T69">может.быть</ta>
            <ta e="T71" id="Seg_1564" s="T70">увидеть-FUT-3PL</ta>
            <ta e="T72" id="Seg_1565" s="T71">и</ta>
            <ta e="T73" id="Seg_1566" s="T72">мы.NOM</ta>
            <ta e="T74" id="Seg_1567" s="T73">пойти-PST-1PL</ta>
            <ta e="T75" id="Seg_1568" s="T74">NEG</ta>
            <ta e="T76" id="Seg_1569" s="T75">даль-ILL</ta>
            <ta e="T77" id="Seg_1570" s="T76">пройти-PST-1PL</ta>
            <ta e="T78" id="Seg_1571" s="T77">и</ta>
            <ta e="T79" id="Seg_1572" s="T78">березняк-EP-ADV.LOC</ta>
            <ta e="T80" id="Seg_1573" s="T79">собака-PL.[NOM]</ta>
            <ta e="T81" id="Seg_1574" s="T80">лаять-EP-CO-3PL</ta>
            <ta e="T82" id="Seg_1575" s="T81">берлога-ILL</ta>
            <ta e="T83" id="Seg_1576" s="T82">я.NOM</ta>
            <ta e="T84" id="Seg_1577" s="T83">младший.брат/сестра-ACC-EMPH</ta>
            <ta e="T85" id="Seg_1578" s="T84">сказать-3SG.S</ta>
            <ta e="T86" id="Seg_1579" s="T85">увидеть-CO-3PL</ta>
            <ta e="T87" id="Seg_1580" s="T86">медведь-ACC</ta>
            <ta e="T88" id="Seg_1581" s="T87">сперва</ta>
            <ta e="T89" id="Seg_1582" s="T88">собака-PL.[NOM]</ta>
            <ta e="T90" id="Seg_1583" s="T89">убежать-DUR-PST-3PL</ta>
            <ta e="T91" id="Seg_1584" s="T90">кольцо-ADVZ</ta>
            <ta e="T92" id="Seg_1585" s="T91">берлога-EP-GEN</ta>
            <ta e="T93" id="Seg_1586" s="T92">рот-EP-ADV.LOC</ta>
            <ta e="T94" id="Seg_1587" s="T93">и</ta>
            <ta e="T95" id="Seg_1588" s="T94">лаять-PST-3PL</ta>
            <ta e="T96" id="Seg_1589" s="T95">а</ta>
            <ta e="T97" id="Seg_1590" s="T96">потом</ta>
            <ta e="T98" id="Seg_1591" s="T97">мы.NOM</ta>
            <ta e="T99" id="Seg_1592" s="T98">дойти-CVB</ta>
            <ta e="T100" id="Seg_1593" s="T99">начать-1PL</ta>
            <ta e="T101" id="Seg_1594" s="T100">%%</ta>
            <ta e="T102" id="Seg_1595" s="T101">и</ta>
            <ta e="T103" id="Seg_1596" s="T102">один</ta>
            <ta e="T104" id="Seg_1597" s="T103">собака.[NOM]</ta>
            <ta e="T105" id="Seg_1598" s="T104">заскочить-3SG.S</ta>
            <ta e="T106" id="Seg_1599" s="T105">берлога-ILL</ta>
            <ta e="T107" id="Seg_1600" s="T106">а</ta>
            <ta e="T108" id="Seg_1601" s="T107">потом</ta>
            <ta e="T109" id="Seg_1602" s="T108">друг-GEN-3PL</ta>
            <ta e="T110" id="Seg_1603" s="T109">за</ta>
            <ta e="T111" id="Seg_1604" s="T110">опять</ta>
            <ta e="T112" id="Seg_1605" s="T111">%%-HAB-3DU.S</ta>
            <ta e="T113" id="Seg_1606" s="T112">опять</ta>
            <ta e="T114" id="Seg_1607" s="T113">два</ta>
            <ta e="T115" id="Seg_1608" s="T114">собака-PL</ta>
            <ta e="T116" id="Seg_1609" s="T115">потом</ta>
            <ta e="T117" id="Seg_1610" s="T116">собака-GEN</ta>
            <ta e="T118" id="Seg_1611" s="T117">лай.[NOM]</ta>
            <ta e="T119" id="Seg_1612" s="T118">и</ta>
            <ta e="T120" id="Seg_1613" s="T119">крутой.берег.[NOM]</ta>
            <ta e="T121" id="Seg_1614" s="T120">медведь-GEN</ta>
            <ta e="T122" id="Seg_1615" s="T121">рычание.[NOM]</ta>
            <ta e="T123" id="Seg_1616" s="T122">слышать-PST.[3SG.S]</ta>
            <ta e="T124" id="Seg_1617" s="T123">%%</ta>
            <ta e="T125" id="Seg_1618" s="T124">один</ta>
            <ta e="T126" id="Seg_1619" s="T125">мы.NOM</ta>
            <ta e="T127" id="Seg_1620" s="T126">стоять-PST-1PL</ta>
            <ta e="T128" id="Seg_1621" s="T127">берлога-EP-GEN</ta>
            <ta e="T129" id="Seg_1622" s="T128">рот-EP-ADV.LOC</ta>
            <ta e="T130" id="Seg_1623" s="T129">караулить-PST-1PL</ta>
            <ta e="T131" id="Seg_1624" s="T130">когда</ta>
            <ta e="T132" id="Seg_1625" s="T131">он(а).[NOM]</ta>
            <ta e="T133" id="Seg_1626" s="T132">выскакивать-FUT-3SG.S</ta>
            <ta e="T134" id="Seg_1627" s="T133">он(а).[NOM]</ta>
            <ta e="T135" id="Seg_1628" s="T134">NEG</ta>
            <ta e="T136" id="Seg_1629" s="T135">выйти-HAB-PST.[3SG.S]</ta>
            <ta e="T137" id="Seg_1630" s="T136">потом</ta>
            <ta e="T138" id="Seg_1631" s="T137">я.NOM</ta>
            <ta e="T139" id="Seg_1632" s="T138">выстрелять-DUR-1PL</ta>
            <ta e="T140" id="Seg_1633" s="T139">стрелять-1PL</ta>
            <ta e="T141" id="Seg_1634" s="T140">выскочить-EP-3SG.S</ta>
            <ta e="T142" id="Seg_1635" s="T141">один</ta>
            <ta e="T143" id="Seg_1636" s="T142">собака.[NOM]</ta>
            <ta e="T144" id="Seg_1637" s="T143">голова-ACC-3SG</ta>
            <ta e="T145" id="Seg_1638" s="T144">обмотать-PFV-FRQ-EP-3SG.O</ta>
            <ta e="T146" id="Seg_1639" s="T145">и</ta>
            <ta e="T147" id="Seg_1640" s="T146">%%-3SG.O</ta>
            <ta e="T148" id="Seg_1641" s="T147">кольцо-ADVZ</ta>
            <ta e="T149" id="Seg_1642" s="T148">кровь-ACC</ta>
            <ta e="T150" id="Seg_1643" s="T149">потом</ta>
            <ta e="T151" id="Seg_1644" s="T150">выйти-3SG.S</ta>
            <ta e="T152" id="Seg_1645" s="T151">два-ORD-3SG</ta>
            <ta e="T153" id="Seg_1646" s="T152">тоже</ta>
            <ta e="T154" id="Seg_1647" s="T153">бить-RES-PTCP.PST</ta>
            <ta e="T155" id="Seg_1648" s="T154">а</ta>
            <ta e="T156" id="Seg_1649" s="T155">три-ORD-ADJZ</ta>
            <ta e="T157" id="Seg_1650" s="T156">собака.[NOM]</ta>
            <ta e="T158" id="Seg_1651" s="T157">остаться-3SG.S</ta>
            <ta e="T159" id="Seg_1652" s="T158">берлога-LOC</ta>
            <ta e="T160" id="Seg_1653" s="T159">медведь-GEN</ta>
            <ta e="T161" id="Seg_1654" s="T160">%%-GEN</ta>
            <ta e="T162" id="Seg_1655" s="T161">низ-ADV.LOC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1656" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_1657" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_1658" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_1659" s="T4">v-v:tense.[v:pn]</ta>
            <ta e="T6" id="Seg_1660" s="T5">n-n:ins-n&gt;n</ta>
            <ta e="T7" id="Seg_1661" s="T6">v&gt;adj</ta>
            <ta e="T8" id="Seg_1662" s="T7">n-n:num-n:case</ta>
            <ta e="T9" id="Seg_1663" s="T8">dem</ta>
            <ta e="T10" id="Seg_1664" s="T9">n-n:ins-n:case.poss</ta>
            <ta e="T11" id="Seg_1665" s="T10">pers</ta>
            <ta e="T12" id="Seg_1666" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_1667" s="T12">n-n-n:case-n:num-n:case</ta>
            <ta e="T14" id="Seg_1668" s="T13">adj</ta>
            <ta e="T15" id="Seg_1669" s="T14">n-adv:case</ta>
            <ta e="T16" id="Seg_1670" s="T15">v-v&gt;v-v:inf</ta>
            <ta e="T17" id="Seg_1671" s="T16">num-n:ins-adj&gt;adv</ta>
            <ta e="T18" id="Seg_1672" s="T17">pers</ta>
            <ta e="T19" id="Seg_1673" s="T18">conj</ta>
            <ta e="T20" id="Seg_1674" s="T19">num</ta>
            <ta e="T21" id="Seg_1675" s="T20">n.[n:case]-n:poss</ta>
            <ta e="T22" id="Seg_1676" s="T21">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_1677" s="T22">n-n:num-n:case</ta>
            <ta e="T24" id="Seg_1678" s="T23">conj</ta>
            <ta e="T25" id="Seg_1679" s="T24">v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_1680" s="T25">v-v&gt;v-v&gt;adv</ta>
            <ta e="T27" id="Seg_1681" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_1682" s="T27">n-n&gt;adj</ta>
            <ta e="T29" id="Seg_1683" s="T28">n-n&gt;adj</ta>
            <ta e="T30" id="Seg_1684" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_1685" s="T30">conj</ta>
            <ta e="T32" id="Seg_1686" s="T31">adv</ta>
            <ta e="T33" id="Seg_1687" s="T32">v-v:ins.[v:pn]</ta>
            <ta e="T34" id="Seg_1688" s="T33">n-n:num.[n:case]</ta>
            <ta e="T35" id="Seg_1689" s="T34">n-n:ins-n:case</ta>
            <ta e="T36" id="Seg_1690" s="T35">pers</ta>
            <ta e="T37" id="Seg_1691" s="T36">v-v:pn</ta>
            <ta e="T38" id="Seg_1692" s="T37">v-v&gt;v-v:inf</ta>
            <ta e="T39" id="Seg_1693" s="T38">num</ta>
            <ta e="T40" id="Seg_1694" s="T39">adj</ta>
            <ta e="T41" id="Seg_1695" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_1696" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_1697" s="T42">v-v&gt;v-v&gt;n-n:case</ta>
            <ta e="T44" id="Seg_1698" s="T43">v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_1699" s="T44">n-n&gt;n-n:case</ta>
            <ta e="T46" id="Seg_1700" s="T45">n-n&gt;n-n&gt;n-n:case</ta>
            <ta e="T47" id="Seg_1701" s="T46">adv</ta>
            <ta e="T48" id="Seg_1702" s="T47">pers-n:ins-n:case</ta>
            <ta e="T49" id="Seg_1703" s="T48">v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_1704" s="T49">n-n:num.[n:case]</ta>
            <ta e="T51" id="Seg_1705" s="T50">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_1706" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_1707" s="T52">conj</ta>
            <ta e="T54" id="Seg_1708" s="T53">v-v:pn</ta>
            <ta e="T55" id="Seg_1709" s="T54">adv</ta>
            <ta e="T56" id="Seg_1710" s="T55">v-v:inf</ta>
            <ta e="T57" id="Seg_1711" s="T56">v-v&gt;adv</ta>
            <ta e="T58" id="Seg_1712" s="T57">dem-adj&gt;adj-n:case</ta>
            <ta e="T59" id="Seg_1713" s="T58">adj-n&gt;n</ta>
            <ta e="T60" id="Seg_1714" s="T59">n-n&gt;n-n:case</ta>
            <ta e="T61" id="Seg_1715" s="T60">n-n:poss</ta>
            <ta e="T62" id="Seg_1716" s="T61">v-v:pn</ta>
            <ta e="T63" id="Seg_1717" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_1718" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_1719" s="T64">v-v:ins-v:pn</ta>
            <ta e="T66" id="Seg_1720" s="T65">pers</ta>
            <ta e="T67" id="Seg_1721" s="T66">v-v:pn</ta>
            <ta e="T68" id="Seg_1722" s="T67">v-v:mood-v:pn</ta>
            <ta e="T69" id="Seg_1723" s="T68">adv</ta>
            <ta e="T70" id="Seg_1724" s="T69">adv</ta>
            <ta e="T71" id="Seg_1725" s="T70">v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_1726" s="T71">conj</ta>
            <ta e="T73" id="Seg_1727" s="T72">pers</ta>
            <ta e="T74" id="Seg_1728" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_1729" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_1730" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_1731" s="T76">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_1732" s="T77">conj</ta>
            <ta e="T79" id="Seg_1733" s="T78">n-n:ins-adv:case</ta>
            <ta e="T80" id="Seg_1734" s="T79">n-n:num.[n:case]</ta>
            <ta e="T81" id="Seg_1735" s="T80">v-n:ins-v:ins-v:pn</ta>
            <ta e="T82" id="Seg_1736" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_1737" s="T82">pers</ta>
            <ta e="T84" id="Seg_1738" s="T83">n-n:case-clit</ta>
            <ta e="T85" id="Seg_1739" s="T84">v-v:pn</ta>
            <ta e="T86" id="Seg_1740" s="T85">v-v:ins-v:pn</ta>
            <ta e="T87" id="Seg_1741" s="T86">n-n:case</ta>
            <ta e="T88" id="Seg_1742" s="T87">adv</ta>
            <ta e="T89" id="Seg_1743" s="T88">n-n:num.[n:case]</ta>
            <ta e="T90" id="Seg_1744" s="T89">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_1745" s="T90">n-adj&gt;adv</ta>
            <ta e="T92" id="Seg_1746" s="T91">n-n:ins-n:case</ta>
            <ta e="T93" id="Seg_1747" s="T92">n-n:ins-adv:case</ta>
            <ta e="T94" id="Seg_1748" s="T93">conj</ta>
            <ta e="T95" id="Seg_1749" s="T94">v-v:tense-v:pn</ta>
            <ta e="T96" id="Seg_1750" s="T95">conj</ta>
            <ta e="T97" id="Seg_1751" s="T96">adv</ta>
            <ta e="T98" id="Seg_1752" s="T97">pers</ta>
            <ta e="T99" id="Seg_1753" s="T98">v-v&gt;adv</ta>
            <ta e="T100" id="Seg_1754" s="T99">v-v:pn</ta>
            <ta e="T101" id="Seg_1755" s="T100">adv</ta>
            <ta e="T102" id="Seg_1756" s="T101">conj</ta>
            <ta e="T103" id="Seg_1757" s="T102">num</ta>
            <ta e="T104" id="Seg_1758" s="T103">n.[n:case]</ta>
            <ta e="T105" id="Seg_1759" s="T104">v-v:pn</ta>
            <ta e="T106" id="Seg_1760" s="T105">n-n:case</ta>
            <ta e="T107" id="Seg_1761" s="T106">conj</ta>
            <ta e="T108" id="Seg_1762" s="T107">adv</ta>
            <ta e="T109" id="Seg_1763" s="T108">n-n:case-v:pn</ta>
            <ta e="T110" id="Seg_1764" s="T109">pp</ta>
            <ta e="T111" id="Seg_1765" s="T110">adv</ta>
            <ta e="T112" id="Seg_1766" s="T111">v-v&gt;v-v:pn</ta>
            <ta e="T113" id="Seg_1767" s="T112">adv</ta>
            <ta e="T114" id="Seg_1768" s="T113">num</ta>
            <ta e="T115" id="Seg_1769" s="T114">n-n:num</ta>
            <ta e="T116" id="Seg_1770" s="T115">adv</ta>
            <ta e="T117" id="Seg_1771" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_1772" s="T117">n.[n:case]</ta>
            <ta e="T119" id="Seg_1773" s="T118">conj</ta>
            <ta e="T120" id="Seg_1774" s="T119">n.[n:case]</ta>
            <ta e="T121" id="Seg_1775" s="T120">n-n:case</ta>
            <ta e="T122" id="Seg_1776" s="T121">n.[n:case]</ta>
            <ta e="T123" id="Seg_1777" s="T122">v-v:tense.[v:pn]</ta>
            <ta e="T124" id="Seg_1778" s="T123">%%</ta>
            <ta e="T125" id="Seg_1779" s="T124">num</ta>
            <ta e="T126" id="Seg_1780" s="T125">pers</ta>
            <ta e="T127" id="Seg_1781" s="T126">v-v:tense-v:pn</ta>
            <ta e="T128" id="Seg_1782" s="T127">n-n:ins-n:case</ta>
            <ta e="T129" id="Seg_1783" s="T128">n-n:ins-adv:case</ta>
            <ta e="T130" id="Seg_1784" s="T129">v-v:tense-v:pn</ta>
            <ta e="T131" id="Seg_1785" s="T130">conj</ta>
            <ta e="T132" id="Seg_1786" s="T131">pers.[n:case]</ta>
            <ta e="T133" id="Seg_1787" s="T132">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_1788" s="T133">pers.[n:case]</ta>
            <ta e="T135" id="Seg_1789" s="T134">ptcl</ta>
            <ta e="T136" id="Seg_1790" s="T135">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T137" id="Seg_1791" s="T136">adv</ta>
            <ta e="T138" id="Seg_1792" s="T137">pers</ta>
            <ta e="T139" id="Seg_1793" s="T138">v-v&gt;v-v:pn</ta>
            <ta e="T140" id="Seg_1794" s="T139">v-v:pn</ta>
            <ta e="T141" id="Seg_1795" s="T140">v-n:ins-v:pn</ta>
            <ta e="T142" id="Seg_1796" s="T141">num</ta>
            <ta e="T143" id="Seg_1797" s="T142">n.[n:case]</ta>
            <ta e="T144" id="Seg_1798" s="T143">n-n:case-n:poss</ta>
            <ta e="T145" id="Seg_1799" s="T144">v-v&gt;v-v&gt;v-n:ins-v:pn</ta>
            <ta e="T146" id="Seg_1800" s="T145">conj</ta>
            <ta e="T147" id="Seg_1801" s="T146">v-v:pn</ta>
            <ta e="T148" id="Seg_1802" s="T147">n-adj&gt;adv</ta>
            <ta e="T149" id="Seg_1803" s="T148">n-n:case</ta>
            <ta e="T150" id="Seg_1804" s="T149">adv</ta>
            <ta e="T151" id="Seg_1805" s="T150">v-v:pn</ta>
            <ta e="T152" id="Seg_1806" s="T151">num-num&gt;adj-n:poss</ta>
            <ta e="T153" id="Seg_1807" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_1808" s="T153">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T155" id="Seg_1809" s="T154">conj</ta>
            <ta e="T156" id="Seg_1810" s="T155">num-num&gt;adj-n&gt;adj</ta>
            <ta e="T157" id="Seg_1811" s="T156">n.[n:case]</ta>
            <ta e="T158" id="Seg_1812" s="T157">v-v:pn</ta>
            <ta e="T159" id="Seg_1813" s="T158">n-n:case</ta>
            <ta e="T160" id="Seg_1814" s="T159">n-n:case</ta>
            <ta e="T161" id="Seg_1815" s="T160">n-n:case</ta>
            <ta e="T162" id="Seg_1816" s="T161">n-adv:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1817" s="T1">n</ta>
            <ta e="T3" id="Seg_1818" s="T2">n</ta>
            <ta e="T4" id="Seg_1819" s="T3">n</ta>
            <ta e="T5" id="Seg_1820" s="T4">v</ta>
            <ta e="T6" id="Seg_1821" s="T5">n</ta>
            <ta e="T7" id="Seg_1822" s="T6">adj</ta>
            <ta e="T8" id="Seg_1823" s="T7">n</ta>
            <ta e="T9" id="Seg_1824" s="T8">dem</ta>
            <ta e="T10" id="Seg_1825" s="T9">n</ta>
            <ta e="T11" id="Seg_1826" s="T10">pers</ta>
            <ta e="T12" id="Seg_1827" s="T11">v</ta>
            <ta e="T13" id="Seg_1828" s="T12">n</ta>
            <ta e="T14" id="Seg_1829" s="T13">adj</ta>
            <ta e="T15" id="Seg_1830" s="T14">n</ta>
            <ta e="T16" id="Seg_1831" s="T15">v</ta>
            <ta e="T17" id="Seg_1832" s="T16">adv</ta>
            <ta e="T18" id="Seg_1833" s="T17">pers</ta>
            <ta e="T19" id="Seg_1834" s="T18">conj</ta>
            <ta e="T20" id="Seg_1835" s="T19">num</ta>
            <ta e="T21" id="Seg_1836" s="T20">n</ta>
            <ta e="T22" id="Seg_1837" s="T21">v</ta>
            <ta e="T23" id="Seg_1838" s="T22">n</ta>
            <ta e="T24" id="Seg_1839" s="T23">conj</ta>
            <ta e="T25" id="Seg_1840" s="T24">v</ta>
            <ta e="T26" id="Seg_1841" s="T25">adv</ta>
            <ta e="T27" id="Seg_1842" s="T26">v</ta>
            <ta e="T28" id="Seg_1843" s="T27">adj</ta>
            <ta e="T29" id="Seg_1844" s="T28">adj</ta>
            <ta e="T30" id="Seg_1845" s="T29">n</ta>
            <ta e="T31" id="Seg_1846" s="T30">conj</ta>
            <ta e="T32" id="Seg_1847" s="T31">adv</ta>
            <ta e="T33" id="Seg_1848" s="T32">v</ta>
            <ta e="T34" id="Seg_1849" s="T33">n</ta>
            <ta e="T35" id="Seg_1850" s="T34">n</ta>
            <ta e="T36" id="Seg_1851" s="T35">pers</ta>
            <ta e="T37" id="Seg_1852" s="T36">v</ta>
            <ta e="T38" id="Seg_1853" s="T37">v</ta>
            <ta e="T39" id="Seg_1854" s="T38">num</ta>
            <ta e="T40" id="Seg_1855" s="T39">adj</ta>
            <ta e="T41" id="Seg_1856" s="T40">n</ta>
            <ta e="T42" id="Seg_1857" s="T41">n</ta>
            <ta e="T43" id="Seg_1858" s="T42">v</ta>
            <ta e="T44" id="Seg_1859" s="T43">v</ta>
            <ta e="T45" id="Seg_1860" s="T44">n</ta>
            <ta e="T46" id="Seg_1861" s="T45">n</ta>
            <ta e="T47" id="Seg_1862" s="T46">adv</ta>
            <ta e="T48" id="Seg_1863" s="T47">pers</ta>
            <ta e="T49" id="Seg_1864" s="T48">v</ta>
            <ta e="T50" id="Seg_1865" s="T49">n</ta>
            <ta e="T51" id="Seg_1866" s="T50">v</ta>
            <ta e="T52" id="Seg_1867" s="T51">ptcp</ta>
            <ta e="T53" id="Seg_1868" s="T52">conj</ta>
            <ta e="T54" id="Seg_1869" s="T53">v</ta>
            <ta e="T55" id="Seg_1870" s="T54">adv</ta>
            <ta e="T56" id="Seg_1871" s="T55">v</ta>
            <ta e="T57" id="Seg_1872" s="T56">adv</ta>
            <ta e="T58" id="Seg_1873" s="T57">adj</ta>
            <ta e="T59" id="Seg_1874" s="T58">adj</ta>
            <ta e="T60" id="Seg_1875" s="T59">n</ta>
            <ta e="T61" id="Seg_1876" s="T60">n</ta>
            <ta e="T62" id="Seg_1877" s="T61">v</ta>
            <ta e="T63" id="Seg_1878" s="T62">n</ta>
            <ta e="T64" id="Seg_1879" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_1880" s="T64">v</ta>
            <ta e="T66" id="Seg_1881" s="T65">pers</ta>
            <ta e="T67" id="Seg_1882" s="T66">v</ta>
            <ta e="T68" id="Seg_1883" s="T67">v</ta>
            <ta e="T69" id="Seg_1884" s="T68">adv</ta>
            <ta e="T70" id="Seg_1885" s="T69">adv</ta>
            <ta e="T71" id="Seg_1886" s="T70">v</ta>
            <ta e="T72" id="Seg_1887" s="T71">conj</ta>
            <ta e="T73" id="Seg_1888" s="T72">pers</ta>
            <ta e="T74" id="Seg_1889" s="T73">v</ta>
            <ta e="T75" id="Seg_1890" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_1891" s="T75">n</ta>
            <ta e="T77" id="Seg_1892" s="T76">v</ta>
            <ta e="T78" id="Seg_1893" s="T77">conj</ta>
            <ta e="T79" id="Seg_1894" s="T78">n</ta>
            <ta e="T80" id="Seg_1895" s="T79">n</ta>
            <ta e="T81" id="Seg_1896" s="T80">v</ta>
            <ta e="T82" id="Seg_1897" s="T81">n</ta>
            <ta e="T83" id="Seg_1898" s="T82">pers</ta>
            <ta e="T84" id="Seg_1899" s="T83">n</ta>
            <ta e="T85" id="Seg_1900" s="T84">v</ta>
            <ta e="T86" id="Seg_1901" s="T85">v</ta>
            <ta e="T87" id="Seg_1902" s="T86">n</ta>
            <ta e="T88" id="Seg_1903" s="T87">adv</ta>
            <ta e="T89" id="Seg_1904" s="T88">n</ta>
            <ta e="T90" id="Seg_1905" s="T89">v</ta>
            <ta e="T91" id="Seg_1906" s="T90">adv</ta>
            <ta e="T92" id="Seg_1907" s="T91">n</ta>
            <ta e="T93" id="Seg_1908" s="T92">n</ta>
            <ta e="T94" id="Seg_1909" s="T93">conj</ta>
            <ta e="T95" id="Seg_1910" s="T94">v</ta>
            <ta e="T96" id="Seg_1911" s="T95">conj</ta>
            <ta e="T97" id="Seg_1912" s="T96">adv</ta>
            <ta e="T98" id="Seg_1913" s="T97">pers</ta>
            <ta e="T99" id="Seg_1914" s="T98">adv</ta>
            <ta e="T100" id="Seg_1915" s="T99">v</ta>
            <ta e="T101" id="Seg_1916" s="T100">adv</ta>
            <ta e="T102" id="Seg_1917" s="T101">conj</ta>
            <ta e="T103" id="Seg_1918" s="T102">num</ta>
            <ta e="T104" id="Seg_1919" s="T103">n</ta>
            <ta e="T105" id="Seg_1920" s="T104">v</ta>
            <ta e="T106" id="Seg_1921" s="T105">n</ta>
            <ta e="T107" id="Seg_1922" s="T106">conj</ta>
            <ta e="T108" id="Seg_1923" s="T107">adv</ta>
            <ta e="T109" id="Seg_1924" s="T108">n</ta>
            <ta e="T110" id="Seg_1925" s="T109">pp</ta>
            <ta e="T111" id="Seg_1926" s="T110">adv</ta>
            <ta e="T112" id="Seg_1927" s="T111">v</ta>
            <ta e="T113" id="Seg_1928" s="T112">adv</ta>
            <ta e="T114" id="Seg_1929" s="T113">num</ta>
            <ta e="T115" id="Seg_1930" s="T114">n</ta>
            <ta e="T116" id="Seg_1931" s="T115">adv</ta>
            <ta e="T117" id="Seg_1932" s="T116">n</ta>
            <ta e="T118" id="Seg_1933" s="T117">n</ta>
            <ta e="T119" id="Seg_1934" s="T118">conj</ta>
            <ta e="T120" id="Seg_1935" s="T119">n</ta>
            <ta e="T121" id="Seg_1936" s="T120">n</ta>
            <ta e="T122" id="Seg_1937" s="T121">n</ta>
            <ta e="T123" id="Seg_1938" s="T122">v</ta>
            <ta e="T125" id="Seg_1939" s="T124">num</ta>
            <ta e="T126" id="Seg_1940" s="T125">pers</ta>
            <ta e="T127" id="Seg_1941" s="T126">v</ta>
            <ta e="T128" id="Seg_1942" s="T127">n</ta>
            <ta e="T129" id="Seg_1943" s="T128">n</ta>
            <ta e="T130" id="Seg_1944" s="T129">v</ta>
            <ta e="T131" id="Seg_1945" s="T130">interrog</ta>
            <ta e="T132" id="Seg_1946" s="T131">pers</ta>
            <ta e="T133" id="Seg_1947" s="T132">v</ta>
            <ta e="T134" id="Seg_1948" s="T133">pers</ta>
            <ta e="T135" id="Seg_1949" s="T134">ptcl</ta>
            <ta e="T136" id="Seg_1950" s="T135">v</ta>
            <ta e="T137" id="Seg_1951" s="T136">adv</ta>
            <ta e="T138" id="Seg_1952" s="T137">pers</ta>
            <ta e="T139" id="Seg_1953" s="T138">v</ta>
            <ta e="T140" id="Seg_1954" s="T139">v</ta>
            <ta e="T141" id="Seg_1955" s="T140">v</ta>
            <ta e="T142" id="Seg_1956" s="T141">num</ta>
            <ta e="T143" id="Seg_1957" s="T142">n</ta>
            <ta e="T144" id="Seg_1958" s="T143">n</ta>
            <ta e="T145" id="Seg_1959" s="T144">v</ta>
            <ta e="T146" id="Seg_1960" s="T145">conj</ta>
            <ta e="T147" id="Seg_1961" s="T146">v</ta>
            <ta e="T148" id="Seg_1962" s="T147">adv</ta>
            <ta e="T149" id="Seg_1963" s="T148">n</ta>
            <ta e="T150" id="Seg_1964" s="T149">adv</ta>
            <ta e="T151" id="Seg_1965" s="T150">v</ta>
            <ta e="T152" id="Seg_1966" s="T151">adj</ta>
            <ta e="T153" id="Seg_1967" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_1968" s="T153">v</ta>
            <ta e="T155" id="Seg_1969" s="T154">conj</ta>
            <ta e="T156" id="Seg_1970" s="T155">adj</ta>
            <ta e="T157" id="Seg_1971" s="T156">n</ta>
            <ta e="T158" id="Seg_1972" s="T157">adv</ta>
            <ta e="T159" id="Seg_1973" s="T158">n</ta>
            <ta e="T160" id="Seg_1974" s="T159">n</ta>
            <ta e="T161" id="Seg_1975" s="T160">n</ta>
            <ta e="T162" id="Seg_1976" s="T161">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_1977" s="T2">np:L</ta>
            <ta e="T4" id="Seg_1978" s="T3">np:So</ta>
            <ta e="T5" id="Seg_1979" s="T4">0.3.h:A</ta>
            <ta e="T6" id="Seg_1980" s="T5">np:P</ta>
            <ta e="T8" id="Seg_1981" s="T7">np.h:B</ta>
            <ta e="T10" id="Seg_1982" s="T9">np:G</ta>
            <ta e="T11" id="Seg_1983" s="T10">pro.h:A</ta>
            <ta e="T13" id="Seg_1984" s="T12">np:Com</ta>
            <ta e="T15" id="Seg_1985" s="T14">adv:Time</ta>
            <ta e="T16" id="Seg_1986" s="T15">0.1.h:A</ta>
            <ta e="T17" id="Seg_1987" s="T16">adv:Time</ta>
            <ta e="T18" id="Seg_1988" s="T17">pro.h:A</ta>
            <ta e="T21" id="Seg_1989" s="T20">np.h:A 0.1.h:Poss</ta>
            <ta e="T23" id="Seg_1990" s="T22">np:Th</ta>
            <ta e="T25" id="Seg_1991" s="T24">0.1.h:A</ta>
            <ta e="T27" id="Seg_1992" s="T26">0.1.h:A</ta>
            <ta e="T30" id="Seg_1993" s="T29">np:Path</ta>
            <ta e="T34" id="Seg_1994" s="T33">np:Th</ta>
            <ta e="T35" id="Seg_1995" s="T34">np:L</ta>
            <ta e="T36" id="Seg_1996" s="T35">pro.h:A</ta>
            <ta e="T38" id="Seg_1997" s="T37">0.1.h:A</ta>
            <ta e="T41" id="Seg_1998" s="T40">np:Poss</ta>
            <ta e="T42" id="Seg_1999" s="T41">np:G</ta>
            <ta e="T44" id="Seg_2000" s="T43">0.1.h:A</ta>
            <ta e="T45" id="Seg_2001" s="T44">np:Th</ta>
            <ta e="T48" id="Seg_2002" s="T47">pro:Th</ta>
            <ta e="T50" id="Seg_2003" s="T49">np:E</ta>
            <ta e="T51" id="Seg_2004" s="T50">0.3:A</ta>
            <ta e="T52" id="Seg_2005" s="T51">np:G</ta>
            <ta e="T54" id="Seg_2006" s="T53">0.3:A</ta>
            <ta e="T56" id="Seg_2007" s="T55">v:Th</ta>
            <ta e="T60" id="Seg_2008" s="T59">np:R</ta>
            <ta e="T61" id="Seg_2009" s="T60">np.h:A 0.1.h:Poss</ta>
            <ta e="T63" id="Seg_2010" s="T62">np:Th</ta>
            <ta e="T65" id="Seg_2011" s="T64">0.1.h:E</ta>
            <ta e="T66" id="Seg_2012" s="T65">pro.h:A</ta>
            <ta e="T68" id="Seg_2013" s="T67">0.1.h:A</ta>
            <ta e="T69" id="Seg_2014" s="T68">adv:G</ta>
            <ta e="T71" id="Seg_2015" s="T70">0.3:E</ta>
            <ta e="T73" id="Seg_2016" s="T72">pro.h:A</ta>
            <ta e="T76" id="Seg_2017" s="T75">np:G</ta>
            <ta e="T77" id="Seg_2018" s="T76">0.1.h:A</ta>
            <ta e="T79" id="Seg_2019" s="T78">adv:L</ta>
            <ta e="T80" id="Seg_2020" s="T79">np:A</ta>
            <ta e="T82" id="Seg_2021" s="T81">np:G</ta>
            <ta e="T83" id="Seg_2022" s="T82">pro.h:A</ta>
            <ta e="T84" id="Seg_2023" s="T83">np.h:R</ta>
            <ta e="T86" id="Seg_2024" s="T85">0.3:E</ta>
            <ta e="T87" id="Seg_2025" s="T86">np:Th</ta>
            <ta e="T88" id="Seg_2026" s="T87">adv:Time</ta>
            <ta e="T89" id="Seg_2027" s="T88">np:A</ta>
            <ta e="T92" id="Seg_2028" s="T91">np:Poss</ta>
            <ta e="T93" id="Seg_2029" s="T92">adv:L</ta>
            <ta e="T95" id="Seg_2030" s="T94">0.3:A</ta>
            <ta e="T97" id="Seg_2031" s="T96">adv:Time</ta>
            <ta e="T98" id="Seg_2032" s="T97">pro.h:A</ta>
            <ta e="T104" id="Seg_2033" s="T103">np:A</ta>
            <ta e="T106" id="Seg_2034" s="T105">np:G</ta>
            <ta e="T108" id="Seg_2035" s="T107">adv:Time</ta>
            <ta e="T109" id="Seg_2036" s="T108">pp:G</ta>
            <ta e="T115" id="Seg_2037" s="T114">np:A</ta>
            <ta e="T116" id="Seg_2038" s="T115">adv:Time</ta>
            <ta e="T117" id="Seg_2039" s="T116">np:Poss</ta>
            <ta e="T118" id="Seg_2040" s="T117">np:Th</ta>
            <ta e="T121" id="Seg_2041" s="T120">np:Poss</ta>
            <ta e="T122" id="Seg_2042" s="T121">np:Th</ta>
            <ta e="T123" id="Seg_2043" s="T122">0.3.h:E</ta>
            <ta e="T126" id="Seg_2044" s="T125">pro.h:Th</ta>
            <ta e="T128" id="Seg_2045" s="T127">np:Poss</ta>
            <ta e="T129" id="Seg_2046" s="T128">adv:L</ta>
            <ta e="T130" id="Seg_2047" s="T129">0.1.h:A</ta>
            <ta e="T132" id="Seg_2048" s="T131">pro:A</ta>
            <ta e="T134" id="Seg_2049" s="T133">pro:A</ta>
            <ta e="T137" id="Seg_2050" s="T136">adv:Time</ta>
            <ta e="T138" id="Seg_2051" s="T137">pro.h:A</ta>
            <ta e="T143" id="Seg_2052" s="T142">np:A</ta>
            <ta e="T144" id="Seg_2053" s="T143">np:Th 0.3:Poss</ta>
            <ta e="T145" id="Seg_2054" s="T144">0.3:A</ta>
            <ta e="T147" id="Seg_2055" s="T146">0.3:A</ta>
            <ta e="T149" id="Seg_2056" s="T148">np:Th</ta>
            <ta e="T150" id="Seg_2057" s="T149">adv:Time</ta>
            <ta e="T152" id="Seg_2058" s="T151">np:A</ta>
            <ta e="T154" id="Seg_2059" s="T153">0.3:P</ta>
            <ta e="T157" id="Seg_2060" s="T156">np:Th</ta>
            <ta e="T159" id="Seg_2061" s="T158">np:L</ta>
            <ta e="T160" id="Seg_2062" s="T159">np:Poss</ta>
            <ta e="T161" id="Seg_2063" s="T160">np:Poss</ta>
            <ta e="T162" id="Seg_2064" s="T161">adv:L</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T5" id="Seg_2065" s="T4">0.3.h:S v:pred</ta>
            <ta e="T6" id="Seg_2066" s="T5">np:O</ta>
            <ta e="T11" id="Seg_2067" s="T10">pro.h:S</ta>
            <ta e="T12" id="Seg_2068" s="T11">v:pred</ta>
            <ta e="T16" id="Seg_2069" s="T15">s:purp</ta>
            <ta e="T18" id="Seg_2070" s="T17">pro.h:S</ta>
            <ta e="T21" id="Seg_2071" s="T20">np.h:S</ta>
            <ta e="T22" id="Seg_2072" s="T21">v:pred</ta>
            <ta e="T23" id="Seg_2073" s="T22">np:O</ta>
            <ta e="T25" id="Seg_2074" s="T24">0.1.h:S v:pred</ta>
            <ta e="T27" id="Seg_2075" s="T26">0.1.h:S v:pred</ta>
            <ta e="T30" id="Seg_2076" s="T29">np:O</ta>
            <ta e="T33" id="Seg_2077" s="T32">v:pred</ta>
            <ta e="T34" id="Seg_2078" s="T33">np:S</ta>
            <ta e="T36" id="Seg_2079" s="T35">pro.h:S</ta>
            <ta e="T37" id="Seg_2080" s="T36">v:pred</ta>
            <ta e="T38" id="Seg_2081" s="T37">s:purp</ta>
            <ta e="T43" id="Seg_2082" s="T42">s:temp</ta>
            <ta e="T44" id="Seg_2083" s="T43">0.1.h:S v:pred</ta>
            <ta e="T45" id="Seg_2084" s="T44">np:O</ta>
            <ta e="T48" id="Seg_2085" s="T47">pro:O</ta>
            <ta e="T49" id="Seg_2086" s="T48">v:pred</ta>
            <ta e="T50" id="Seg_2087" s="T49">np:S</ta>
            <ta e="T51" id="Seg_2088" s="T50">0.3:S v:pred</ta>
            <ta e="T54" id="Seg_2089" s="T53">0.3:S v:pred</ta>
            <ta e="T56" id="Seg_2090" s="T55">v:O</ta>
            <ta e="T61" id="Seg_2091" s="T60">np.h:S</ta>
            <ta e="T62" id="Seg_2092" s="T61">v:pred</ta>
            <ta e="T63" id="Seg_2093" s="T62">np:O</ta>
            <ta e="T65" id="Seg_2094" s="T64">0.1.h:S v:pred</ta>
            <ta e="T66" id="Seg_2095" s="T65">pro.h:S</ta>
            <ta e="T67" id="Seg_2096" s="T66">v:pred</ta>
            <ta e="T68" id="Seg_2097" s="T67">0.1.h:S v:pred</ta>
            <ta e="T71" id="Seg_2098" s="T70">0.3:S v:pred</ta>
            <ta e="T73" id="Seg_2099" s="T72">pro.h:S</ta>
            <ta e="T74" id="Seg_2100" s="T73">v:pred</ta>
            <ta e="T77" id="Seg_2101" s="T76">0.1.h:S v:pred</ta>
            <ta e="T80" id="Seg_2102" s="T79">np:S</ta>
            <ta e="T81" id="Seg_2103" s="T80">v:pred</ta>
            <ta e="T83" id="Seg_2104" s="T82">pro.h:S</ta>
            <ta e="T85" id="Seg_2105" s="T84">v:pred</ta>
            <ta e="T86" id="Seg_2106" s="T85">0.3:S v:pred</ta>
            <ta e="T87" id="Seg_2107" s="T86">np:O</ta>
            <ta e="T89" id="Seg_2108" s="T88">np:S</ta>
            <ta e="T90" id="Seg_2109" s="T89">v:pred</ta>
            <ta e="T95" id="Seg_2110" s="T94">0.3:S v:pred</ta>
            <ta e="T98" id="Seg_2111" s="T97">pro.h:S</ta>
            <ta e="T100" id="Seg_2112" s="T99">v:pred</ta>
            <ta e="T104" id="Seg_2113" s="T103">np:S</ta>
            <ta e="T105" id="Seg_2114" s="T104">v:pred</ta>
            <ta e="T112" id="Seg_2115" s="T111">v:pred</ta>
            <ta e="T115" id="Seg_2116" s="T114">np:S</ta>
            <ta e="T118" id="Seg_2117" s="T117">np:O</ta>
            <ta e="T122" id="Seg_2118" s="T121">np:O</ta>
            <ta e="T123" id="Seg_2119" s="T122">0.3.h:S v:pred</ta>
            <ta e="T126" id="Seg_2120" s="T125">pro.h:S</ta>
            <ta e="T127" id="Seg_2121" s="T126">v:pred</ta>
            <ta e="T130" id="Seg_2122" s="T129">0.1.h:S v:pred</ta>
            <ta e="T133" id="Seg_2123" s="T130">s:compl</ta>
            <ta e="T134" id="Seg_2124" s="T133">pro:S</ta>
            <ta e="T136" id="Seg_2125" s="T135">v:pred</ta>
            <ta e="T138" id="Seg_2126" s="T137">pro.h:S</ta>
            <ta e="T139" id="Seg_2127" s="T138">v:pred</ta>
            <ta e="T141" id="Seg_2128" s="T140">v:pred</ta>
            <ta e="T143" id="Seg_2129" s="T142">np:S</ta>
            <ta e="T144" id="Seg_2130" s="T143">np:O</ta>
            <ta e="T145" id="Seg_2131" s="T144">0.3:S v:pred</ta>
            <ta e="T147" id="Seg_2132" s="T146">0.3:S v:pred</ta>
            <ta e="T149" id="Seg_2133" s="T148">np:O</ta>
            <ta e="T151" id="Seg_2134" s="T150">v:pred</ta>
            <ta e="T152" id="Seg_2135" s="T151">np:S</ta>
            <ta e="T154" id="Seg_2136" s="T153">0.3:S adj:pred</ta>
            <ta e="T157" id="Seg_2137" s="T156">np:S</ta>
            <ta e="T158" id="Seg_2138" s="T157">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T19" id="Seg_2139" s="T18">RUS:gram</ta>
            <ta e="T24" id="Seg_2140" s="T23">RUS:gram</ta>
            <ta e="T31" id="Seg_2141" s="T30">RUS:gram</ta>
            <ta e="T53" id="Seg_2142" s="T52">RUS:gram</ta>
            <ta e="T64" id="Seg_2143" s="T63">RUS:gram</ta>
            <ta e="T72" id="Seg_2144" s="T71">RUS:gram</ta>
            <ta e="T78" id="Seg_2145" s="T77">RUS:gram</ta>
            <ta e="T84" id="Seg_2146" s="T83">-</ta>
            <ta e="T94" id="Seg_2147" s="T93">RUS:gram</ta>
            <ta e="T96" id="Seg_2148" s="T95">RUS:gram</ta>
            <ta e="T102" id="Seg_2149" s="T101">RUS:gram</ta>
            <ta e="T107" id="Seg_2150" s="T106">RUS:gram</ta>
            <ta e="T119" id="Seg_2151" s="T118">RUS:gram</ta>
            <ta e="T146" id="Seg_2152" s="T145">RUS:gram</ta>
            <ta e="T155" id="Seg_2153" s="T154">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T8" id="Seg_2154" s="T1">Far away from the village in the taiga there is a small hut built for hunters.</ta>
            <ta e="T16" id="Seg_2155" s="T8">Every winter I used to go to this hut to hunt with my brothers.</ta>
            <ta e="T26" id="Seg_2156" s="T16">Once two my brothers and I untied dogs and started out to hunt.</ta>
            <ta e="T30" id="Seg_2157" s="T26">We passed through the cedar forest.</ta>
            <ta e="T34" id="Seg_2158" s="T30">Then the birch forest (?) began.</ta>
            <ta e="T42" id="Seg_2159" s="T34">In the birch forest we sat down on a log to smoke.</ta>
            <ta e="T46" id="Seg_2160" s="T42">During the smoking break we beckoned a chipmunk to come to us.</ta>
            <ta e="T50" id="Seg_2161" s="T46">But then the dogs noticed it.</ta>
            <ta e="T60" id="Seg_2162" s="T50">They treed it and began to bark at such a small animal.</ta>
            <ta e="T65" id="Seg_2163" s="T60">My younger brother said: “If only we could find a bear.”</ta>
            <ta e="T69" id="Seg_2164" s="T65">I said: “Let’s move on!</ta>
            <ta e="T71" id="Seg_2165" s="T69">Maybe they will find one.”</ta>
            <ta e="T74" id="Seg_2166" s="T71">And we set off.</ta>
            <ta e="T77" id="Seg_2167" s="T74">We walked a bit further.</ta>
            <ta e="T82" id="Seg_2168" s="T77">And in the birch forest the dogs began to bark at a lair.</ta>
            <ta e="T87" id="Seg_2169" s="T82">I said to my brother: “We have found a bear.”</ta>
            <ta e="T95" id="Seg_2170" s="T87">At first the dogs were running and barking round the entry to the lair.</ta>
            <ta e="T101" id="Seg_2171" s="T95">And then we moved closer.</ta>
            <ta e="T106" id="Seg_2172" s="T101">And one dog jumped into the lair.</ta>
            <ta e="T115" id="Seg_2173" s="T106">And then two more dogs jumped [into the lair] one by one.</ta>
            <ta e="T125" id="Seg_2174" s="T115">And then the dog bark the bear growl merged.</ta>
            <ta e="T129" id="Seg_2175" s="T125">We stood at the lair entrance.</ta>
            <ta e="T133" id="Seg_2176" s="T129">We were watching for it to jump out.</ta>
            <ta e="T136" id="Seg_2177" s="T133">But it didn't go out.</ta>
            <ta e="T140" id="Seg_2178" s="T136">Then we shoot.</ta>
            <ta e="T149" id="Seg_2179" s="T140">Then we saw one dog jumping out, it shook its head, there was blood all around.</ta>
            <ta e="T154" id="Seg_2180" s="T149">Then the second one jumped out, it was also beaten.</ta>
            <ta e="T162" id="Seg_2181" s="T154">And the third dog stayed in the lair under the bear pad.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T8" id="Seg_2182" s="T1">Weit vom Dorf entfernt in der Taiga gibt es eine kleine Jagdhütte gebaut für Jäger.</ta>
            <ta e="T16" id="Seg_2183" s="T8">Ich pflegte, jeden Winter zu dieser Hütte zu gehen um mit meinen Brüdern zu jagen.</ta>
            <ta e="T26" id="Seg_2184" s="T16">Einmal banden meine Brüder und ich die Hunde los und brachen zur Jagd auf.</ta>
            <ta e="T30" id="Seg_2185" s="T26">Wir kamen durch einen Zedernwald.</ta>
            <ta e="T34" id="Seg_2186" s="T30">Dann begann der Birkenwald (?).</ta>
            <ta e="T42" id="Seg_2187" s="T34">Im Birkenwald setzten wir uns auf einen Baumstamm um zu rauchen.</ta>
            <ta e="T46" id="Seg_2188" s="T42">Während der Raucherpause lockten wir ein Streifenhörnchen an.</ta>
            <ta e="T50" id="Seg_2189" s="T46">Aber die Hunde bemerkten es gleich.</ta>
            <ta e="T60" id="Seg_2190" s="T50">Sie trieben es einen Baum hinauf und begannen dieses kleine Tierchen anzubellen.</ta>
            <ta e="T65" id="Seg_2191" s="T60">Mein kleiner Bruder sagte: "Wenn wir nur einen Bär fänden."</ta>
            <ta e="T69" id="Seg_2192" s="T65">Ich sagte: "Lass uns weitergehen!</ta>
            <ta e="T71" id="Seg_2193" s="T69">Vielleicht finden sie einen."</ta>
            <ta e="T74" id="Seg_2194" s="T71">Und wir brachen auf.</ta>
            <ta e="T77" id="Seg_2195" s="T74">Wir liefen ein wenig weiter.</ta>
            <ta e="T82" id="Seg_2196" s="T77">Und im Birkenwald begannen die Hunde einen Bau anzubellen.</ta>
            <ta e="T87" id="Seg_2197" s="T82">Ich sagte zu meinem Bruder: "Sie haben einen Bären gefunden."</ta>
            <ta e="T95" id="Seg_2198" s="T87">Zunächst rannten die Hunde rund um den Eingang des Baus und bellten.</ta>
            <ta e="T101" id="Seg_2199" s="T95">Und dann näherten wir uns.</ta>
            <ta e="T106" id="Seg_2200" s="T101">Und ein Hund sprang in den Bau.</ta>
            <ta e="T115" id="Seg_2201" s="T106">Und dann sprangen zwei weitere Hunde ihm nach [in den Bau].</ta>
            <ta e="T125" id="Seg_2202" s="T115">Dann vermischte sich das Hundebellen mit dem Brummen des Bären.</ta>
            <ta e="T129" id="Seg_2203" s="T125">Wir standen am Eingang des Baus.</ta>
            <ta e="T133" id="Seg_2204" s="T129">Wir warteten darauf, dass er heraussprang.</ta>
            <ta e="T136" id="Seg_2205" s="T133">Aber er kam nicht heraus.</ta>
            <ta e="T140" id="Seg_2206" s="T136">Dann schießen wir.</ta>
            <ta e="T149" id="Seg_2207" s="T140">Dann sahen wir einen Hund herausspringen, er schüttelte den Kopf, überall war Blut.</ta>
            <ta e="T154" id="Seg_2208" s="T149">Dann kam der zweite heraus, er war auch verletzt.</ta>
            <ta e="T162" id="Seg_2209" s="T154">Und der dritte Hund blieb im Bau unter der Bärentatze.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T8" id="Seg_2210" s="T1">Далеко в тайге от деревни построена избушка для охотников.</ta>
            <ta e="T16" id="Seg_2211" s="T8">В эту избушку мы ходили с братьями каждую зиму охотиться.</ta>
            <ta e="T26" id="Seg_2212" s="T16">Однажды я и два брата отвязали собак и отправились на охоту.</ta>
            <ta e="T30" id="Seg_2213" s="T26">Прошли кедровую тайгу.</ta>
            <ta e="T34" id="Seg_2214" s="T30">А дальше пошли бельники.</ta>
            <ta e="T42" id="Seg_2215" s="T34">В бельниках мы сели покурить на одну большую колодину.</ta>
            <ta e="T46" id="Seg_2216" s="T42">На перекуре подманили бурундука.</ta>
            <ta e="T50" id="Seg_2217" s="T46">Но, правда, его увидели собаки.</ta>
            <ta e="T60" id="Seg_2218" s="T50">Они загнали его на дерево и начали сильно лаять на такого маленького зверька.</ta>
            <ta e="T65" id="Seg_2219" s="T60">Младший брат говорит: «Вот бы медведя нашли».</ta>
            <ta e="T69" id="Seg_2220" s="T65">Я сказал: «Пойдем дальше.</ta>
            <ta e="T71" id="Seg_2221" s="T69">Может, найдут».</ta>
            <ta e="T74" id="Seg_2222" s="T71">И мы отправились.</ta>
            <ta e="T77" id="Seg_2223" s="T74">Недалеко прошли.</ta>
            <ta e="T82" id="Seg_2224" s="T77">И в березняке собаки залаяли на берлогу.</ta>
            <ta e="T87" id="Seg_2225" s="T82">Я брату и говорю: «Вот и нашли медведя».</ta>
            <ta e="T95" id="Seg_2226" s="T87">Сначала собаки бегали вокруг входа в берлогу и лаяли.</ta>
            <ta e="T101" id="Seg_2227" s="T95">А потом мы стали подходить ближе.</ta>
            <ta e="T106" id="Seg_2228" s="T101">И одна собака заскочила в берлогу.</ta>
            <ta e="T115" id="Seg_2229" s="T106">А потом друг за дружкой заскочили еще две собаки.</ta>
            <ta e="T125" id="Seg_2230" s="T115">И тогда собачий лай и медвежье рычание слились в одно.</ta>
            <ta e="T129" id="Seg_2231" s="T125">Мы стояли у входа в берлогу.</ta>
            <ta e="T133" id="Seg_2232" s="T129">Мы караулили, когда он выскочит.</ta>
            <ta e="T136" id="Seg_2233" s="T133">Но он не вылезал.</ta>
            <ta e="T140" id="Seg_2234" s="T136">Потом мы стреляли.</ta>
            <ta e="T149" id="Seg_2235" s="T140">Потом смотрим, выскочила одна собака, головой мотает и кругом брызжет кровь.</ta>
            <ta e="T154" id="Seg_2236" s="T149">После выскочила вторая, тоже избитая.</ta>
            <ta e="T162" id="Seg_2237" s="T154">А третья собака осталась в берлоге под медвежьей лапой.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr" />
         <annotation name="nt" tierref="nt">
            <ta e="T34" id="Seg_2238" s="T30">[Kuzmina original] березник, осенник и тайга вместе</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
