<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>MuIP_1964_Lifestory_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">MuIP_1964_Lifestory_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">164</ud-information>
            <ud-information attribute-name="# HIAT:w">126</ud-information>
            <ud-information attribute-name="# e">126</ud-information>
            <ud-information attribute-name="# HIAT:u">21</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="MuIP">
            <abbreviation>MuIP</abbreviation>
            <sex value="m" />
            <languages-used>
               <language lang="sel" />
            </languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="MuIP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T127" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">tʼeːlambaŋ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">ätoɣɨn</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">Мarkovo</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">meŋ</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">sumblej</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">qwäj</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">sisarɨj</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">päwwɨ</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_35" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">man</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">wes</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">ilapsaŋɨnen</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">illaŋ</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_49" n="HIAT:w" s="T14">okkə</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_52" n="HIAT:w" s="T15">ätoqɨn</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_56" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">man</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">ewem</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_64" n="HIAT:w" s="T18">qumba</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_68" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">qaɣə</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_73" n="HIAT:w" s="T20">meŋ</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">äːsan</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">somblʼe</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_82" n="HIAT:w" s="T23">päːwwɨ</ts>
                  <nts id="Seg_83" n="HIAT:ip">,</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_86" n="HIAT:w" s="T24">essem</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_89" n="HIAT:w" s="T25">näːtass</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_93" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_95" n="HIAT:w" s="T26">esemnan</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_98" n="HIAT:w" s="T27">qalʼis</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_100" n="HIAT:ip">(</nts>
                  <ts e="T29" id="Seg_102" n="HIAT:w" s="T28">äːssan</ts>
                  <nts id="Seg_103" n="HIAT:ip">)</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_106" n="HIAT:w" s="T29">tätt</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_109" n="HIAT:w" s="T30">üče</ts>
                  <nts id="Seg_110" n="HIAT:ip">.</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_113" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_115" n="HIAT:w" s="T31">aːnuqojmɨnni</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_118" n="HIAT:w" s="T32">äsan</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_121" n="HIAT:w" s="T33">qət</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_124" n="HIAT:w" s="T34">pät</ts>
                  <nts id="Seg_125" n="HIAT:ip">.</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_128" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_130" n="HIAT:w" s="T35">šɨtamdelǯuimɨnni</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_133" n="HIAT:w" s="T36">äsan</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_136" n="HIAT:w" s="T37">selʼdʼu</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_139" n="HIAT:w" s="T38">päːt</ts>
                  <nts id="Seg_140" n="HIAT:ip">.</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_143" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_145" n="HIAT:w" s="T39">man</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_148" n="HIAT:w" s="T40">äːsan</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_151" n="HIAT:w" s="T41">naːrumdälǯuj</ts>
                  <nts id="Seg_152" n="HIAT:ip">.</nts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_155" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_157" n="HIAT:w" s="T42">tettamdelǯij</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_160" n="HIAT:w" s="T43">näquwaj</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_163" n="HIAT:w" s="T44">üːčenni</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_166" n="HIAT:w" s="T45">eːsan</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_169" n="HIAT:w" s="T46">naːr</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_172" n="HIAT:w" s="T47">pät</ts>
                  <nts id="Seg_173" n="HIAT:ip">.</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_176" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_178" n="HIAT:w" s="T48">aːnuqon</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_181" n="HIAT:w" s="T49">mesinat</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_184" n="HIAT:w" s="T50">oramǯimbis</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_187" n="HIAT:w" s="T51">aːtʼö</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_190" n="HIAT:w" s="T52">iːldʼä</ts>
                  <nts id="Seg_191" n="HIAT:ip">.</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_194" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_196" n="HIAT:w" s="T53">qaɣə</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_199" n="HIAT:w" s="T54">esem</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_202" n="HIAT:w" s="T55">nätas</ts>
                  <nts id="Seg_203" n="HIAT:ip">,</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_206" n="HIAT:w" s="T56">me</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_209" n="HIAT:w" s="T57">orawlʼe</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_212" n="HIAT:w" s="T58">čaːčesot</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_215" n="HIAT:w" s="T59">sänd</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_218" n="HIAT:w" s="T60">ewannan</ts>
                  <nts id="Seg_219" n="HIAT:ip">.</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_222" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_224" n="HIAT:w" s="T61">oqqar</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_227" n="HIAT:w" s="T62">tʼät</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_230" n="HIAT:w" s="T63">qöːtaj</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_233" n="HIAT:w" s="T64">päːqan</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_236" n="HIAT:w" s="T65">man</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_239" n="HIAT:w" s="T66">qwässaŋ</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_242" n="HIAT:w" s="T67">šqolandə</ts>
                  <nts id="Seg_243" n="HIAT:ip">.</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_246" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_248" n="HIAT:w" s="T68">škola</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_251" n="HIAT:w" s="T69">äːsan</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_254" n="HIAT:w" s="T70">meːnanni</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_257" n="HIAT:w" s="T71">qöt</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_260" n="HIAT:w" s="T72">qilometram</ts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_264" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_266" n="HIAT:w" s="T73">aːnuqon</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_269" n="HIAT:w" s="T74">man</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_272" n="HIAT:w" s="T75">iləsan</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_275" n="HIAT:w" s="T76">quːlannan</ts>
                  <nts id="Seg_276" n="HIAT:ip">,</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_279" n="HIAT:w" s="T77">a</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_282" n="HIAT:w" s="T78">nän</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_285" n="HIAT:w" s="T79">paldʼuzaŋ</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_288" n="HIAT:w" s="T80">topan</ts>
                  <nts id="Seg_289" n="HIAT:ip">.</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_292" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_294" n="HIAT:w" s="T81">qəːn</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_297" n="HIAT:w" s="T82">äːqus</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_300" n="HIAT:w" s="T83">tassuqus</ts>
                  <nts id="Seg_301" n="HIAT:ip">.</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_304" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_306" n="HIAT:w" s="T84">niːn</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_309" n="HIAT:w" s="T85">man</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_312" n="HIAT:w" s="T86">oːqalǯuqsan</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_315" n="HIAT:w" s="T87">naːr</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_318" n="HIAT:w" s="T88">päːn</ts>
                  <nts id="Seg_319" n="HIAT:ip">.</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_322" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_324" n="HIAT:w" s="T89">taːɣan</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_327" n="HIAT:w" s="T90">man</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_330" n="HIAT:w" s="T91">peldibiqusaŋ</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_333" n="HIAT:w" s="T92">maːtqan</ts>
                  <nts id="Seg_334" n="HIAT:ip">:</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_337" n="HIAT:w" s="T93">oːmdalǯiqusaŋ</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_340" n="HIAT:w" s="T94">qartopqam</ts>
                  <nts id="Seg_341" n="HIAT:ip">,</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_344" n="HIAT:w" s="T95">paldʼüqusaŋ</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_347" n="HIAT:w" s="T96">nʼürtɨ</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_350" n="HIAT:w" s="T97">patʼätɨlʼlʼe</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_353" n="HIAT:w" s="T98">nüčaj</ts>
                  <nts id="Seg_354" n="HIAT:ip">,</nts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_357" n="HIAT:w" s="T99">poŋgam</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_360" n="HIAT:w" s="T100">päːtqusaŋ</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_363" n="HIAT:w" s="T101">qwollondə</ts>
                  <nts id="Seg_364" n="HIAT:ip">,</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_367" n="HIAT:w" s="T102">paldʼüqusaŋ</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_370" n="HIAT:w" s="T103">tʼunbaːrint</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_373" n="HIAT:w" s="T104">puːnaj</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_376" n="HIAT:w" s="T105">taqtɨlʼe</ts>
                  <nts id="Seg_377" n="HIAT:ip">,</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_380" n="HIAT:w" s="T106">mantuj</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_383" n="HIAT:w" s="T107">wätanduqsaŋ</ts>
                  <nts id="Seg_384" n="HIAT:ip">.</nts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_387" n="HIAT:u" s="T108">
                  <nts id="Seg_388" n="HIAT:ip">(</nts>
                  <ts e="T109" id="Seg_390" n="HIAT:w" s="T108">man</ts>
                  <nts id="Seg_391" n="HIAT:ip">)</nts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_394" n="HIAT:w" s="T109">malməčisaŋ</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_397" n="HIAT:w" s="T110">oːqalǯiqulʼe</ts>
                  <nts id="Seg_398" n="HIAT:ip">,</nts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_401" n="HIAT:w" s="T111">oldisaŋ</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_404" n="HIAT:w" s="T112">suːrɨlʼe</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_406" n="HIAT:ip">(</nts>
                  <ts e="T114" id="Seg_408" n="HIAT:w" s="T113">peːmambilʼe</ts>
                  <nts id="Seg_409" n="HIAT:ip">)</nts>
                  <nts id="Seg_410" n="HIAT:ip">:</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_413" n="HIAT:w" s="T114">man</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_416" n="HIAT:w" s="T115">nʼäjasaŋ</ts>
                  <nts id="Seg_417" n="HIAT:ip">,</nts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_420" n="HIAT:w" s="T116">peŋasaŋ</ts>
                  <nts id="Seg_421" n="HIAT:ip">.</nts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_424" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_426" n="HIAT:w" s="T117">tiːtam</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_429" n="HIAT:w" s="T118">man</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_432" n="HIAT:w" s="T119">naq</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_435" n="HIAT:w" s="T120">suːrɨjquɣaŋ</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_438" n="HIAT:w" s="T121">i</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_441" n="HIAT:w" s="T122">laqqaŋ</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_444" n="HIAT:w" s="T123">qalhosqan</ts>
                  <nts id="Seg_445" n="HIAT:ip">.</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_448" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_450" n="HIAT:w" s="T124">man</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_453" n="HIAT:w" s="T125">tamdʼe</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_456" n="HIAT:w" s="T126">niːɣimbaŋ</ts>
                  <nts id="Seg_457" n="HIAT:ip">.</nts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T127" id="Seg_459" n="sc" s="T1">
               <ts e="T2" id="Seg_461" n="e" s="T1">man </ts>
               <ts e="T3" id="Seg_463" n="e" s="T2">tʼeːlambaŋ </ts>
               <ts e="T4" id="Seg_465" n="e" s="T3">ätoɣɨn </ts>
               <ts e="T5" id="Seg_467" n="e" s="T4">Мarkovo. </ts>
               <ts e="T6" id="Seg_469" n="e" s="T5">meŋ </ts>
               <ts e="T7" id="Seg_471" n="e" s="T6">sumblej </ts>
               <ts e="T8" id="Seg_473" n="e" s="T7">qwäj </ts>
               <ts e="T9" id="Seg_475" n="e" s="T8">sisarɨj </ts>
               <ts e="T10" id="Seg_477" n="e" s="T9">päwwɨ. </ts>
               <ts e="T11" id="Seg_479" n="e" s="T10">man </ts>
               <ts e="T12" id="Seg_481" n="e" s="T11">wes </ts>
               <ts e="T13" id="Seg_483" n="e" s="T12">ilapsaŋɨnen </ts>
               <ts e="T14" id="Seg_485" n="e" s="T13">illaŋ </ts>
               <ts e="T15" id="Seg_487" n="e" s="T14">okkə </ts>
               <ts e="T16" id="Seg_489" n="e" s="T15">ätoqɨn. </ts>
               <ts e="T17" id="Seg_491" n="e" s="T16">man </ts>
               <ts e="T18" id="Seg_493" n="e" s="T17">ewem </ts>
               <ts e="T19" id="Seg_495" n="e" s="T18">qumba. </ts>
               <ts e="T20" id="Seg_497" n="e" s="T19">qaɣə </ts>
               <ts e="T21" id="Seg_499" n="e" s="T20">meŋ </ts>
               <ts e="T22" id="Seg_501" n="e" s="T21">äːsan </ts>
               <ts e="T23" id="Seg_503" n="e" s="T22">somblʼe </ts>
               <ts e="T24" id="Seg_505" n="e" s="T23">päːwwɨ, </ts>
               <ts e="T25" id="Seg_507" n="e" s="T24">essem </ts>
               <ts e="T26" id="Seg_509" n="e" s="T25">näːtass. </ts>
               <ts e="T27" id="Seg_511" n="e" s="T26">esemnan </ts>
               <ts e="T28" id="Seg_513" n="e" s="T27">qalʼis </ts>
               <ts e="T29" id="Seg_515" n="e" s="T28">(äːssan) </ts>
               <ts e="T30" id="Seg_517" n="e" s="T29">tätt </ts>
               <ts e="T31" id="Seg_519" n="e" s="T30">üče. </ts>
               <ts e="T32" id="Seg_521" n="e" s="T31">aːnuqojmɨnni </ts>
               <ts e="T33" id="Seg_523" n="e" s="T32">äsan </ts>
               <ts e="T34" id="Seg_525" n="e" s="T33">qət </ts>
               <ts e="T35" id="Seg_527" n="e" s="T34">pät. </ts>
               <ts e="T36" id="Seg_529" n="e" s="T35">šɨtamdelǯuimɨnni </ts>
               <ts e="T37" id="Seg_531" n="e" s="T36">äsan </ts>
               <ts e="T38" id="Seg_533" n="e" s="T37">selʼdʼu </ts>
               <ts e="T39" id="Seg_535" n="e" s="T38">päːt. </ts>
               <ts e="T40" id="Seg_537" n="e" s="T39">man </ts>
               <ts e="T41" id="Seg_539" n="e" s="T40">äːsan </ts>
               <ts e="T42" id="Seg_541" n="e" s="T41">naːrumdälǯuj. </ts>
               <ts e="T43" id="Seg_543" n="e" s="T42">tettamdelǯij </ts>
               <ts e="T44" id="Seg_545" n="e" s="T43">näquwaj </ts>
               <ts e="T45" id="Seg_547" n="e" s="T44">üːčenni </ts>
               <ts e="T46" id="Seg_549" n="e" s="T45">eːsan </ts>
               <ts e="T47" id="Seg_551" n="e" s="T46">naːr </ts>
               <ts e="T48" id="Seg_553" n="e" s="T47">pät. </ts>
               <ts e="T49" id="Seg_555" n="e" s="T48">aːnuqon </ts>
               <ts e="T50" id="Seg_557" n="e" s="T49">mesinat </ts>
               <ts e="T51" id="Seg_559" n="e" s="T50">oramǯimbis </ts>
               <ts e="T52" id="Seg_561" n="e" s="T51">aːtʼö </ts>
               <ts e="T53" id="Seg_563" n="e" s="T52">iːldʼä. </ts>
               <ts e="T54" id="Seg_565" n="e" s="T53">qaɣə </ts>
               <ts e="T55" id="Seg_567" n="e" s="T54">esem </ts>
               <ts e="T56" id="Seg_569" n="e" s="T55">nätas, </ts>
               <ts e="T57" id="Seg_571" n="e" s="T56">me </ts>
               <ts e="T58" id="Seg_573" n="e" s="T57">orawlʼe </ts>
               <ts e="T59" id="Seg_575" n="e" s="T58">čaːčesot </ts>
               <ts e="T60" id="Seg_577" n="e" s="T59">sänd </ts>
               <ts e="T61" id="Seg_579" n="e" s="T60">ewannan. </ts>
               <ts e="T62" id="Seg_581" n="e" s="T61">oqqar </ts>
               <ts e="T63" id="Seg_583" n="e" s="T62">tʼät </ts>
               <ts e="T64" id="Seg_585" n="e" s="T63">qöːtaj </ts>
               <ts e="T65" id="Seg_587" n="e" s="T64">päːqan </ts>
               <ts e="T66" id="Seg_589" n="e" s="T65">man </ts>
               <ts e="T67" id="Seg_591" n="e" s="T66">qwässaŋ </ts>
               <ts e="T68" id="Seg_593" n="e" s="T67">šqolandə. </ts>
               <ts e="T69" id="Seg_595" n="e" s="T68">škola </ts>
               <ts e="T70" id="Seg_597" n="e" s="T69">äːsan </ts>
               <ts e="T71" id="Seg_599" n="e" s="T70">meːnanni </ts>
               <ts e="T72" id="Seg_601" n="e" s="T71">qöt </ts>
               <ts e="T73" id="Seg_603" n="e" s="T72">qilometram. </ts>
               <ts e="T74" id="Seg_605" n="e" s="T73">aːnuqon </ts>
               <ts e="T75" id="Seg_607" n="e" s="T74">man </ts>
               <ts e="T76" id="Seg_609" n="e" s="T75">iləsan </ts>
               <ts e="T77" id="Seg_611" n="e" s="T76">quːlannan, </ts>
               <ts e="T78" id="Seg_613" n="e" s="T77">a </ts>
               <ts e="T79" id="Seg_615" n="e" s="T78">nän </ts>
               <ts e="T80" id="Seg_617" n="e" s="T79">paldʼuzaŋ </ts>
               <ts e="T81" id="Seg_619" n="e" s="T80">topan. </ts>
               <ts e="T82" id="Seg_621" n="e" s="T81">qəːn </ts>
               <ts e="T83" id="Seg_623" n="e" s="T82">äːqus </ts>
               <ts e="T84" id="Seg_625" n="e" s="T83">tassuqus. </ts>
               <ts e="T85" id="Seg_627" n="e" s="T84">niːn </ts>
               <ts e="T86" id="Seg_629" n="e" s="T85">man </ts>
               <ts e="T87" id="Seg_631" n="e" s="T86">oːqalǯuqsan </ts>
               <ts e="T88" id="Seg_633" n="e" s="T87">naːr </ts>
               <ts e="T89" id="Seg_635" n="e" s="T88">päːn. </ts>
               <ts e="T90" id="Seg_637" n="e" s="T89">taːɣan </ts>
               <ts e="T91" id="Seg_639" n="e" s="T90">man </ts>
               <ts e="T92" id="Seg_641" n="e" s="T91">peldibiqusaŋ </ts>
               <ts e="T93" id="Seg_643" n="e" s="T92">maːtqan: </ts>
               <ts e="T94" id="Seg_645" n="e" s="T93">oːmdalǯiqusaŋ </ts>
               <ts e="T95" id="Seg_647" n="e" s="T94">qartopqam, </ts>
               <ts e="T96" id="Seg_649" n="e" s="T95">paldʼüqusaŋ </ts>
               <ts e="T97" id="Seg_651" n="e" s="T96">nʼürtɨ </ts>
               <ts e="T98" id="Seg_653" n="e" s="T97">patʼätɨlʼlʼe </ts>
               <ts e="T99" id="Seg_655" n="e" s="T98">nüčaj, </ts>
               <ts e="T100" id="Seg_657" n="e" s="T99">poŋgam </ts>
               <ts e="T101" id="Seg_659" n="e" s="T100">päːtqusaŋ </ts>
               <ts e="T102" id="Seg_661" n="e" s="T101">qwollondə, </ts>
               <ts e="T103" id="Seg_663" n="e" s="T102">paldʼüqusaŋ </ts>
               <ts e="T104" id="Seg_665" n="e" s="T103">tʼunbaːrint </ts>
               <ts e="T105" id="Seg_667" n="e" s="T104">puːnaj </ts>
               <ts e="T106" id="Seg_669" n="e" s="T105">taqtɨlʼe, </ts>
               <ts e="T107" id="Seg_671" n="e" s="T106">mantuj </ts>
               <ts e="T108" id="Seg_673" n="e" s="T107">wätanduqsaŋ. </ts>
               <ts e="T109" id="Seg_675" n="e" s="T108">(man) </ts>
               <ts e="T110" id="Seg_677" n="e" s="T109">malməčisaŋ </ts>
               <ts e="T111" id="Seg_679" n="e" s="T110">oːqalǯiqulʼe, </ts>
               <ts e="T112" id="Seg_681" n="e" s="T111">oldisaŋ </ts>
               <ts e="T113" id="Seg_683" n="e" s="T112">suːrɨlʼe </ts>
               <ts e="T114" id="Seg_685" n="e" s="T113">(peːmambilʼe): </ts>
               <ts e="T115" id="Seg_687" n="e" s="T114">man </ts>
               <ts e="T116" id="Seg_689" n="e" s="T115">nʼäjasaŋ, </ts>
               <ts e="T117" id="Seg_691" n="e" s="T116">peŋasaŋ. </ts>
               <ts e="T118" id="Seg_693" n="e" s="T117">tiːtam </ts>
               <ts e="T119" id="Seg_695" n="e" s="T118">man </ts>
               <ts e="T120" id="Seg_697" n="e" s="T119">naq </ts>
               <ts e="T121" id="Seg_699" n="e" s="T120">suːrɨjquɣaŋ </ts>
               <ts e="T122" id="Seg_701" n="e" s="T121">i </ts>
               <ts e="T123" id="Seg_703" n="e" s="T122">laqqaŋ </ts>
               <ts e="T124" id="Seg_705" n="e" s="T123">qalhosqan. </ts>
               <ts e="T125" id="Seg_707" n="e" s="T124">man </ts>
               <ts e="T126" id="Seg_709" n="e" s="T125">tamdʼe </ts>
               <ts e="T127" id="Seg_711" n="e" s="T126">niːɣimbaŋ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_712" s="T1">MuIP_1964_Lifestory_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_713" s="T5">MuIP_1964_Lifestory_nar.002 (001.002)</ta>
            <ta e="T16" id="Seg_714" s="T10">MuIP_1964_Lifestory_nar.003 (001.003)</ta>
            <ta e="T19" id="Seg_715" s="T16">MuIP_1964_Lifestory_nar.004 (001.004)</ta>
            <ta e="T26" id="Seg_716" s="T19">MuIP_1964_Lifestory_nar.005 (001.005)</ta>
            <ta e="T31" id="Seg_717" s="T26">MuIP_1964_Lifestory_nar.006 (001.006)</ta>
            <ta e="T35" id="Seg_718" s="T31">MuIP_1964_Lifestory_nar.007 (001.007)</ta>
            <ta e="T39" id="Seg_719" s="T35">MuIP_1964_Lifestory_nar.008 (001.008)</ta>
            <ta e="T42" id="Seg_720" s="T39">MuIP_1964_Lifestory_nar.009 (001.009)</ta>
            <ta e="T48" id="Seg_721" s="T42">MuIP_1964_Lifestory_nar.010 (001.010)</ta>
            <ta e="T53" id="Seg_722" s="T48">MuIP_1964_Lifestory_nar.011 (001.011)</ta>
            <ta e="T61" id="Seg_723" s="T53">MuIP_1964_Lifestory_nar.012 (001.012)</ta>
            <ta e="T68" id="Seg_724" s="T61">MuIP_1964_Lifestory_nar.013 (001.013)</ta>
            <ta e="T73" id="Seg_725" s="T68">MuIP_1964_Lifestory_nar.014 (001.014)</ta>
            <ta e="T81" id="Seg_726" s="T73">MuIP_1964_Lifestory_nar.015 (001.015)</ta>
            <ta e="T84" id="Seg_727" s="T81">MuIP_1964_Lifestory_nar.016 (001.016)</ta>
            <ta e="T89" id="Seg_728" s="T84">MuIP_1964_Lifestory_nar.017 (001.017)</ta>
            <ta e="T108" id="Seg_729" s="T89">MuIP_1964_Lifestory_nar.018 (001.018)</ta>
            <ta e="T117" id="Seg_730" s="T108">MuIP_1964_Lifestory_nar.019 (001.019)</ta>
            <ta e="T124" id="Seg_731" s="T117">MuIP_1964_Lifestory_nar.020 (001.020)</ta>
            <ta e="T127" id="Seg_732" s="T124">MuIP_1964_Lifestory_nar.021 (001.021)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_733" s="T1">ман тʼе̄lам′баң ӓ′тоɣын Марково.</ta>
            <ta e="T10" id="Seg_734" s="T5">мең ′сумбlей kwе(ӓ)й си′сарый ′пӓввы.</ta>
            <ta e="T16" id="Seg_735" s="T10">ман вес и′lапсаңынен ′иllаң ′оккъ ӓ′тоkын.</ta>
            <ta e="T19" id="Seg_736" s="T16">ман е′вем ′kумба.</ta>
            <ta e="T26" id="Seg_737" s="T19">′kаɣ(г)ъ мең ′ӓ̄сан ′сомблʼе ′пӓ̄ввы, ′е(ӓ)ссем нӓ̄′тасс.</ta>
            <ta e="T31" id="Seg_738" s="T26">е′семнан ′kалʼис (′ӓ̄ссан) ′тӓтт ӱче.</ta>
            <ta e="T35" id="Seg_739" s="T31">а̄ну′kоймын(н)и ′ӓ(е̄)сан къ̊т ′пӓт.</ta>
            <ta e="T39" id="Seg_740" s="T35">′шыт(т)ам′де(ӓ)lджуимын(н)и ′ӓ(е̄)сан ′се(ӓ)лʼдʼу пӓ̄т.</ta>
            <ta e="T42" id="Seg_741" s="T39">ман ′ӓ̄сан ′на̄рум‵дӓлджуй.</ta>
            <ta e="T48" id="Seg_742" s="T42">′теттамделджий ′нӓkувай ӱ̄′ченни ′е̄сан на̄р пӓт.</ta>
            <ta e="T53" id="Seg_743" s="T48">′а̄нукон меси′нат о′рамджимбис а̄′тʼӧ ′ӣlдʼӓ.</ta>
            <ta e="T61" id="Seg_744" s="T53">′kаɣъ е′сем ′нӓтас, ме о′равлʼе тша̄тшесот сӓнд е′ван(н)ан.</ta>
            <ta e="T68" id="Seg_745" s="T61">о′ккар тʼӓт кӧ̄′тай ′пӓ̄kан ман ′kwӓссаң ′школандъ.</ta>
            <ta e="T73" id="Seg_746" s="T68">шкоlа ӓ̄′сан ме̄′нанни кӧт кило′метрам.</ta>
            <ta e="T81" id="Seg_747" s="T73">′а̄нукон ман ′иlъсан ′kӯlан(н)ан, а нӓн ′паlдʼузаң то′пан.</ta>
            <ta e="T84" id="Seg_748" s="T81">′къ̊̄н ′ӓ̄кус ′тассукус.</ta>
            <ta e="T89" id="Seg_749" s="T84">нӣн ман о̄′kаlджуксан на̄р пӓ̄н.</ta>
            <ta e="T108" id="Seg_750" s="T89">та̄′ɣан ман ′пеlдибику′саң ма̄тkан: о̄м′даlджикусаң кар′топкам, ′паlдʼӱкусаң ′нʼӱрты па′тʼӓтылʼлʼе ′нӱтшай, ′поңгам ‵пӓ̄тку′саң kwо(ӓ)′llондъ, ′паlдʼӱкусаң ‵тʼун′б̂(п)а̄ринт пӯ′най таkтылʼе, ′мантуй вӓ′тандуксаң.</ta>
            <ta e="T117" id="Seg_751" s="T108">(ман) ‵маlмътши′саң о̄kаlджику‵лʼе, ‵оlди′саң ′сӯрылʼе (′пе̄мамби‵лʼе): ман ′нʼӓjасаң, ′пеңасаң.</ta>
            <ta e="T124" id="Seg_752" s="T117">тӣ′там ман нак ′сӯры(у)й kу′ɣаң и ′лаkkаң кал′хоскан.</ta>
            <ta e="T127" id="Seg_753" s="T124">ман там′дʼе ′нӣ(ɣ)имбаң.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_754" s="T1">man tʼeːlambaŋ ätoɣɨn Мarkovo.</ta>
            <ta e="T10" id="Seg_755" s="T5">meŋ sumblej qwe(ä)j sisarɨj pävvɨ.</ta>
            <ta e="T16" id="Seg_756" s="T10">man ves ilapsaŋɨnen illaŋ okkə ätoqɨn.</ta>
            <ta e="T19" id="Seg_757" s="T16">man evem qumba.</ta>
            <ta e="T26" id="Seg_758" s="T19">qaɣ(g)ə meŋ äːsan somblʼe päːvvɨ, e(ä)ssem näːtass.</ta>
            <ta e="T31" id="Seg_759" s="T26">esemnan qalʼis (äːssan) tätt üče.</ta>
            <ta e="T35" id="Seg_760" s="T31">aːnuqojmɨn(n)i ä(eː)san qət pät.</ta>
            <ta e="T39" id="Seg_761" s="T35">šɨt(t)amde(ä)lǯuimɨn(n)i ä(eː)san se(ä)lʼdʼu päːt.</ta>
            <ta e="T42" id="Seg_762" s="T39">man äːsan naːrumdälǯuj.</ta>
            <ta e="T48" id="Seg_763" s="T42">tettamdelǯij näquvaj üːčenni eːsan naːr pät.</ta>
            <ta e="T53" id="Seg_764" s="T48">aːnuqon mesinat oramǯimbis aːtʼö iːldʼä.</ta>
            <ta e="T61" id="Seg_765" s="T53">qaɣə esem nätas, me oravlʼe tšaːtšesot sänd evan(n)an.</ta>
            <ta e="T68" id="Seg_766" s="T61">oqqar tʼät qöːtaj päːqan man qwässaŋ šqolandə.</ta>
            <ta e="T73" id="Seg_767" s="T68">škola äːsan meːnanni qöt qilometram.</ta>
            <ta e="T81" id="Seg_768" s="T73">aːnuqon man iləsan quːlan(n)an, a nän paldʼuzaŋ topan.</ta>
            <ta e="T84" id="Seg_769" s="T81">qəːn äːqus tassuqus.</ta>
            <ta e="T89" id="Seg_770" s="T84">niːn man oːqalǯuqsan naːr päːn.</ta>
            <ta e="T108" id="Seg_771" s="T89">taːɣan man peldibiqusaŋ maːtqan: oːmdalǯiqusaŋ qartopqam, paldʼüqusaŋ nʼürtɨ patʼätɨlʼlʼe nütšaj, poŋgam päːtqusaŋ qwo(ä)llondə, paldʼüqusaŋ tʼunb̂(p)aːrint puːnaj taqtɨlʼe, mantuj vätanduqsaŋ.</ta>
            <ta e="T117" id="Seg_772" s="T108">(man) malmətšisaŋ oːqalǯiqulʼe, oldisaŋ suːrɨlʼe (peːmambilʼe): man nʼäjasaŋ, peŋasaŋ.</ta>
            <ta e="T124" id="Seg_773" s="T117">tiːtam man naq suːrɨ(u)j quɣaŋ i laqqaŋ qalhosqan.</ta>
            <ta e="T127" id="Seg_774" s="T124">man tamdʼe niː(ɣ)imbaŋ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_775" s="T1">man tʼeːlambaŋ ätoɣɨn Мarkovo. </ta>
            <ta e="T10" id="Seg_776" s="T5">meŋ sumblej qwäj sisarɨj päwwɨ. </ta>
            <ta e="T16" id="Seg_777" s="T10">man wes ilapsaŋɨnen illaŋ okkə ätoqɨn. </ta>
            <ta e="T19" id="Seg_778" s="T16">man ewem qumba. </ta>
            <ta e="T26" id="Seg_779" s="T19">qaɣə meŋ äːsan somblʼe päːwwɨ, essem näːtass. </ta>
            <ta e="T31" id="Seg_780" s="T26">esemnan qalʼis (äːssan) tätt üče. </ta>
            <ta e="T35" id="Seg_781" s="T31">aːnuqojmɨnni äsan qət pät. </ta>
            <ta e="T39" id="Seg_782" s="T35">šɨtamdelǯuimɨnni äsan selʼdʼu päːt. </ta>
            <ta e="T42" id="Seg_783" s="T39">man äːsan naːrumdälǯuj. </ta>
            <ta e="T48" id="Seg_784" s="T42">tettamdelǯij näquwaj üːčenni eːsan naːr pät. </ta>
            <ta e="T53" id="Seg_785" s="T48">aːnuqon mesinat oramǯimbis aːtʼö iːldʼä. </ta>
            <ta e="T61" id="Seg_786" s="T53">qaɣə esem nätas, me orawlʼe čaːčesot sänd ewannan. </ta>
            <ta e="T68" id="Seg_787" s="T61">oqqar tʼät qöːtaj päːqan man qwässaŋ šqolandə. </ta>
            <ta e="T73" id="Seg_788" s="T68">škola äːsan meːnanni qöt qilometram. </ta>
            <ta e="T81" id="Seg_789" s="T73">aːnuqon man iləsan quːlannan, a nän paldʼuzaŋ topan. </ta>
            <ta e="T84" id="Seg_790" s="T81">qəːn äːqus tassuqus. </ta>
            <ta e="T89" id="Seg_791" s="T84">niːn man oːqalǯuqsan naːr päːn. </ta>
            <ta e="T108" id="Seg_792" s="T89">taːɣan man peldibiqusaŋ maːtqan: oːmdalǯiqusaŋ qartopqam, paldʼüqusaŋ nʼürtɨ patʼätɨlʼlʼe nüčaj, poŋgam päːtqusaŋ qwollondə, paldʼüqusaŋ tʼunbaːrint puːnaj taqtɨlʼe, mantuj wätanduqsaŋ. </ta>
            <ta e="T117" id="Seg_793" s="T108">(man) malməčisaŋ oːqalǯiqulʼe, oldisaŋ suːrɨlʼe (peːmambilʼe): man nʼäjasaŋ, peŋasaŋ. </ta>
            <ta e="T124" id="Seg_794" s="T117">tiːtam man naq suːrɨjquɣaŋ i laqqaŋ qalhosqan. </ta>
            <ta e="T127" id="Seg_795" s="T124">man tamdʼe niːɣimbaŋ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_796" s="T1">man</ta>
            <ta e="T3" id="Seg_797" s="T2">tʼeːla-mba-ŋ</ta>
            <ta e="T4" id="Seg_798" s="T3">äto-ɣɨn</ta>
            <ta e="T5" id="Seg_799" s="T4">Мarkovo</ta>
            <ta e="T6" id="Seg_800" s="T5">meŋ</ta>
            <ta e="T7" id="Seg_801" s="T6">sumble-j</ta>
            <ta e="T8" id="Seg_802" s="T7">qwäj</ta>
            <ta e="T9" id="Seg_803" s="T8">sisarɨ-j</ta>
            <ta e="T10" id="Seg_804" s="T9">pä-wwɨ</ta>
            <ta e="T11" id="Seg_805" s="T10">man</ta>
            <ta e="T12" id="Seg_806" s="T11">wes</ta>
            <ta e="T13" id="Seg_807" s="T12">ila-psaŋ-ɨ-nen</ta>
            <ta e="T14" id="Seg_808" s="T13">illa-ŋ</ta>
            <ta e="T15" id="Seg_809" s="T14">okkə</ta>
            <ta e="T16" id="Seg_810" s="T15">äto-qɨn</ta>
            <ta e="T17" id="Seg_811" s="T16">man</ta>
            <ta e="T18" id="Seg_812" s="T17">ewe-mɨ</ta>
            <ta e="T19" id="Seg_813" s="T18">qu-mba</ta>
            <ta e="T20" id="Seg_814" s="T19">qaɣə</ta>
            <ta e="T21" id="Seg_815" s="T20">meŋ</ta>
            <ta e="T22" id="Seg_816" s="T21">äː-sa-ŋ</ta>
            <ta e="T23" id="Seg_817" s="T22">somblʼe</ta>
            <ta e="T24" id="Seg_818" s="T23">päː-wwɨ</ta>
            <ta e="T25" id="Seg_819" s="T24">esse-mɨ</ta>
            <ta e="T26" id="Seg_820" s="T25">näːta-ss</ta>
            <ta e="T27" id="Seg_821" s="T26">ese-mɨ-nan</ta>
            <ta e="T28" id="Seg_822" s="T27">qalʼi-s</ta>
            <ta e="T29" id="Seg_823" s="T28">äː-ssa-ŋ</ta>
            <ta e="T30" id="Seg_824" s="T29">tätt</ta>
            <ta e="T31" id="Seg_825" s="T30">üče</ta>
            <ta e="T32" id="Seg_826" s="T31">aːnuqoj-mɨ-n-ni</ta>
            <ta e="T33" id="Seg_827" s="T32">ä-sa-ŋ</ta>
            <ta e="T34" id="Seg_828" s="T33">qət</ta>
            <ta e="T35" id="Seg_829" s="T34">pä-t</ta>
            <ta e="T36" id="Seg_830" s="T35">šɨta-mdelǯu-i-mɨ-n-ni</ta>
            <ta e="T37" id="Seg_831" s="T36">ä-sa-ŋ</ta>
            <ta e="T38" id="Seg_832" s="T37">selʼdʼu</ta>
            <ta e="T39" id="Seg_833" s="T38">päː-t</ta>
            <ta e="T40" id="Seg_834" s="T39">man</ta>
            <ta e="T41" id="Seg_835" s="T40">äː-sa-ŋ</ta>
            <ta e="T42" id="Seg_836" s="T41">naːru-mdälǯuj</ta>
            <ta e="T43" id="Seg_837" s="T42">tetta-mdelǯij</ta>
            <ta e="T44" id="Seg_838" s="T43">nä-quw-a-j</ta>
            <ta e="T45" id="Seg_839" s="T44">üːče-n-ni</ta>
            <ta e="T46" id="Seg_840" s="T45">eː-sa-ŋ</ta>
            <ta e="T47" id="Seg_841" s="T46">naːr</ta>
            <ta e="T48" id="Seg_842" s="T47">pä-t</ta>
            <ta e="T49" id="Seg_843" s="T48">aːnuqon</ta>
            <ta e="T50" id="Seg_844" s="T49">mesinat</ta>
            <ta e="T51" id="Seg_845" s="T50">oram-ǯi-mbi-s</ta>
            <ta e="T52" id="Seg_846" s="T51">aːtʼö</ta>
            <ta e="T53" id="Seg_847" s="T52">iːldʼä</ta>
            <ta e="T54" id="Seg_848" s="T53">qaɣə</ta>
            <ta e="T55" id="Seg_849" s="T54">ese-mɨ</ta>
            <ta e="T56" id="Seg_850" s="T55">näta-s</ta>
            <ta e="T57" id="Seg_851" s="T56">me</ta>
            <ta e="T58" id="Seg_852" s="T57">oraw-lʼe</ta>
            <ta e="T59" id="Seg_853" s="T58">čaːče-so-t</ta>
            <ta e="T60" id="Seg_854" s="T59">sänd</ta>
            <ta e="T61" id="Seg_855" s="T60">ew-n-nan</ta>
            <ta e="T62" id="Seg_856" s="T61">oqqar</ta>
            <ta e="T63" id="Seg_857" s="T62">tʼät</ta>
            <ta e="T64" id="Seg_858" s="T63">qöːt-a-j</ta>
            <ta e="T65" id="Seg_859" s="T64">päː-qan</ta>
            <ta e="T66" id="Seg_860" s="T65">man</ta>
            <ta e="T67" id="Seg_861" s="T66">qwäs-sa-ŋ</ta>
            <ta e="T68" id="Seg_862" s="T67">šqola-ndə</ta>
            <ta e="T69" id="Seg_863" s="T68">škola</ta>
            <ta e="T70" id="Seg_864" s="T69">äː-sa-ŋ</ta>
            <ta e="T71" id="Seg_865" s="T70">meː-nanni</ta>
            <ta e="T72" id="Seg_866" s="T71">qöt</ta>
            <ta e="T73" id="Seg_867" s="T72">qilometr-a-m</ta>
            <ta e="T74" id="Seg_868" s="T73">aːnuqon</ta>
            <ta e="T75" id="Seg_869" s="T74">man</ta>
            <ta e="T76" id="Seg_870" s="T75">ilə-sa-n</ta>
            <ta e="T77" id="Seg_871" s="T76">quː-la-n-nan</ta>
            <ta e="T78" id="Seg_872" s="T77">a</ta>
            <ta e="T79" id="Seg_873" s="T78">nän</ta>
            <ta e="T80" id="Seg_874" s="T79">paldʼu-za-ŋ</ta>
            <ta e="T81" id="Seg_875" s="T80">topa-n</ta>
            <ta e="T82" id="Seg_876" s="T81">qəː-n</ta>
            <ta e="T83" id="Seg_877" s="T82">äː-qu-s</ta>
            <ta e="T84" id="Seg_878" s="T83">tassu-qu-s</ta>
            <ta e="T85" id="Seg_879" s="T84">niːn</ta>
            <ta e="T86" id="Seg_880" s="T85">man</ta>
            <ta e="T87" id="Seg_881" s="T86">oːqal-lǯu-q-sa-n</ta>
            <ta e="T88" id="Seg_882" s="T87">naːr</ta>
            <ta e="T89" id="Seg_883" s="T88">päː-n</ta>
            <ta e="T90" id="Seg_884" s="T89">taːɣa-n</ta>
            <ta e="T91" id="Seg_885" s="T90">man</ta>
            <ta e="T92" id="Seg_886" s="T91">peldi-bi-qu-sa-ŋ</ta>
            <ta e="T93" id="Seg_887" s="T92">maːt-qan</ta>
            <ta e="T94" id="Seg_888" s="T93">oːmda-lǯi-qu-sa-ŋ</ta>
            <ta e="T95" id="Seg_889" s="T94">qartopqa-m</ta>
            <ta e="T96" id="Seg_890" s="T95">paldʼü-qu-sa-ŋ</ta>
            <ta e="T97" id="Seg_891" s="T96">nʼür-tɨ</ta>
            <ta e="T98" id="Seg_892" s="T97">patʼä-tɨ-lʼ-lʼe</ta>
            <ta e="T99" id="Seg_893" s="T98">nüča-j</ta>
            <ta e="T100" id="Seg_894" s="T99">poŋga-m</ta>
            <ta e="T101" id="Seg_895" s="T100">päːt-qu-sa-ŋ</ta>
            <ta e="T102" id="Seg_896" s="T101">qwollo-ndə</ta>
            <ta e="T103" id="Seg_897" s="T102">paldʼü-qu-sa-ŋ</ta>
            <ta e="T104" id="Seg_898" s="T103">tʼu-n-baːr-i-nt</ta>
            <ta e="T105" id="Seg_899" s="T104">puːna-j</ta>
            <ta e="T106" id="Seg_900" s="T105">taq-tɨ-lʼe</ta>
            <ta e="T107" id="Seg_901" s="T106">mantu-j</ta>
            <ta e="T108" id="Seg_902" s="T107">wäta-ndu-q-sa-ŋ</ta>
            <ta e="T109" id="Seg_903" s="T108">man</ta>
            <ta e="T110" id="Seg_904" s="T109">malmə-či-sa-ŋ</ta>
            <ta e="T111" id="Seg_905" s="T110">oːqal-lǯi-qu-lʼe</ta>
            <ta e="T112" id="Seg_906" s="T111">oldi-sa-ŋ</ta>
            <ta e="T113" id="Seg_907" s="T112">suːrɨ-lʼe</ta>
            <ta e="T114" id="Seg_908" s="T113">peːma-mbi-lʼe</ta>
            <ta e="T115" id="Seg_909" s="T114">man</ta>
            <ta e="T116" id="Seg_910" s="T115">nʼäja-sa-ŋ</ta>
            <ta e="T117" id="Seg_911" s="T116">peŋa-sa-ŋ</ta>
            <ta e="T118" id="Seg_912" s="T117">tiːtam</ta>
            <ta e="T119" id="Seg_913" s="T118">man</ta>
            <ta e="T120" id="Seg_914" s="T119">naq</ta>
            <ta e="T121" id="Seg_915" s="T120">suːrɨ-j-qu-ɣa-ŋ</ta>
            <ta e="T122" id="Seg_916" s="T121">i</ta>
            <ta e="T123" id="Seg_917" s="T122">laqqa-ŋ</ta>
            <ta e="T124" id="Seg_918" s="T123">qalhos-qan</ta>
            <ta e="T125" id="Seg_919" s="T124">man</ta>
            <ta e="T126" id="Seg_920" s="T125">tam-dʼe</ta>
            <ta e="T127" id="Seg_921" s="T126">niː-ɣi-mba-ŋ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_922" s="T1">man</ta>
            <ta e="T3" id="Seg_923" s="T2">tʼeːlɨ-mbɨ-ŋ</ta>
            <ta e="T4" id="Seg_924" s="T3">eːto-qən</ta>
            <ta e="T5" id="Seg_925" s="T4">Мarkovo</ta>
            <ta e="T6" id="Seg_926" s="T5">mäkkä</ta>
            <ta e="T7" id="Seg_927" s="T6">sombɨlʼe-lʼ</ta>
            <ta e="T8" id="Seg_928" s="T7">qwäj</ta>
            <ta e="T9" id="Seg_929" s="T8">sɨsarum-lʼ</ta>
            <ta e="T10" id="Seg_930" s="T9">po-mɨ</ta>
            <ta e="T11" id="Seg_931" s="T10">man</ta>
            <ta e="T12" id="Seg_932" s="T11">wesʼ</ta>
            <ta e="T13" id="Seg_933" s="T12">illɨ-psan-ɨ-nan</ta>
            <ta e="T14" id="Seg_934" s="T13">illɨ-ŋ</ta>
            <ta e="T15" id="Seg_935" s="T14">okkɨr</ta>
            <ta e="T16" id="Seg_936" s="T15">eːto-qən</ta>
            <ta e="T17" id="Seg_937" s="T16">man</ta>
            <ta e="T18" id="Seg_938" s="T17">äwa-mɨ</ta>
            <ta e="T19" id="Seg_939" s="T18">quː-mbɨ</ta>
            <ta e="T20" id="Seg_940" s="T19">qaqä</ta>
            <ta e="T21" id="Seg_941" s="T20">mäkkä</ta>
            <ta e="T22" id="Seg_942" s="T21">eː-sɨ-n</ta>
            <ta e="T23" id="Seg_943" s="T22">sombɨlʼe</ta>
            <ta e="T24" id="Seg_944" s="T23">po-mɨ</ta>
            <ta e="T25" id="Seg_945" s="T24">ässɨ-mɨ</ta>
            <ta e="T26" id="Seg_946" s="T25">naːtɨ-sɨ</ta>
            <ta e="T27" id="Seg_947" s="T26">ässɨ-mɨ-nan</ta>
            <ta e="T28" id="Seg_948" s="T27">qalɨ-sɨ</ta>
            <ta e="T29" id="Seg_949" s="T28">eː-sɨ-n</ta>
            <ta e="T30" id="Seg_950" s="T29">tättɨ</ta>
            <ta e="T31" id="Seg_951" s="T30">üččə</ta>
            <ta e="T32" id="Seg_952" s="T31">aːnuqoj-mɨ-n-nɨ</ta>
            <ta e="T33" id="Seg_953" s="T32">eː-sɨ-n</ta>
            <ta e="T34" id="Seg_954" s="T33">köːt</ta>
            <ta e="T35" id="Seg_955" s="T34">po-tɨ</ta>
            <ta e="T36" id="Seg_956" s="T35">šittə-mtelǯij-lʼ-mɨ-n-nɨ</ta>
            <ta e="T37" id="Seg_957" s="T36">eː-sɨ-n</ta>
            <ta e="T38" id="Seg_958" s="T37">seːlʼdu</ta>
            <ta e="T39" id="Seg_959" s="T38">po-tɨ</ta>
            <ta e="T40" id="Seg_960" s="T39">man</ta>
            <ta e="T41" id="Seg_961" s="T40">eː-sɨ-n</ta>
            <ta e="T42" id="Seg_962" s="T41">nakkɨr-mtelǯij</ta>
            <ta e="T43" id="Seg_963" s="T42">tättɨ-mtelǯij</ta>
            <ta e="T44" id="Seg_964" s="T43">neː-qum-ɨ-lʼ</ta>
            <ta e="T45" id="Seg_965" s="T44">ütče-n-nɨ</ta>
            <ta e="T46" id="Seg_966" s="T45">eː-sɨ-n</ta>
            <ta e="T47" id="Seg_967" s="T46">nakkɨr</ta>
            <ta e="T48" id="Seg_968" s="T47">po-tɨ</ta>
            <ta e="T49" id="Seg_969" s="T48">aːnuːkkon</ta>
            <ta e="T50" id="Seg_970" s="T49">meːzanäd</ta>
            <ta e="T51" id="Seg_971" s="T50">orɨm-tɨ-mbɨ-sɨ</ta>
            <ta e="T52" id="Seg_972" s="T51">adʼu</ta>
            <ta e="T53" id="Seg_973" s="T52">ildʼa</ta>
            <ta e="T54" id="Seg_974" s="T53">qaqä</ta>
            <ta e="T55" id="Seg_975" s="T54">ässɨ-mɨ</ta>
            <ta e="T56" id="Seg_976" s="T55">naːtɨ-sɨ</ta>
            <ta e="T57" id="Seg_977" s="T56">meː</ta>
            <ta e="T58" id="Seg_978" s="T57">orɨm-le</ta>
            <ta e="T59" id="Seg_979" s="T58">čaːǯɨ-sɨ-ut</ta>
            <ta e="T60" id="Seg_980" s="T59">sendɨ</ta>
            <ta e="T61" id="Seg_981" s="T60">äwa-n-nan</ta>
            <ta e="T62" id="Seg_982" s="T61">okkɨr</ta>
            <ta e="T63" id="Seg_983" s="T62">tʼаːt</ta>
            <ta e="T64" id="Seg_984" s="T63">köːt-ɨ-lʼ</ta>
            <ta e="T65" id="Seg_985" s="T64">po-qən</ta>
            <ta e="T66" id="Seg_986" s="T65">man</ta>
            <ta e="T67" id="Seg_987" s="T66">qwən-sɨ-ŋ</ta>
            <ta e="T68" id="Seg_988" s="T67">škola-ndɨ</ta>
            <ta e="T69" id="Seg_989" s="T68">škola</ta>
            <ta e="T70" id="Seg_990" s="T69">eː-sɨ-n</ta>
            <ta e="T71" id="Seg_991" s="T70">meː-nannɨ</ta>
            <ta e="T72" id="Seg_992" s="T71">köːt</ta>
            <ta e="T73" id="Seg_993" s="T72">kilometr-ɨ-m</ta>
            <ta e="T74" id="Seg_994" s="T73">aːnuːkkon</ta>
            <ta e="T75" id="Seg_995" s="T74">man</ta>
            <ta e="T76" id="Seg_996" s="T75">illɨ-sɨ-ŋ</ta>
            <ta e="T77" id="Seg_997" s="T76">qum-la-n-nan</ta>
            <ta e="T78" id="Seg_998" s="T77">a</ta>
            <ta e="T79" id="Seg_999" s="T78">nɨːnɨ</ta>
            <ta e="T80" id="Seg_1000" s="T79">paldʼu-sɨ-ŋ</ta>
            <ta e="T81" id="Seg_1001" s="T80">topǝ-n</ta>
            <ta e="T82" id="Seg_1002" s="T81">ka-n</ta>
            <ta e="T83" id="Seg_1003" s="T82">eː-ku-sɨ</ta>
            <ta e="T84" id="Seg_1004" s="T83">tassu-ku-sɨ</ta>
            <ta e="T85" id="Seg_1005" s="T84">nɨːnɨ</ta>
            <ta e="T86" id="Seg_1006" s="T85">man</ta>
            <ta e="T87" id="Seg_1007" s="T86">oːqəl-lʼčǝ-ku-sɨ-ŋ</ta>
            <ta e="T88" id="Seg_1008" s="T87">nakkɨr</ta>
            <ta e="T89" id="Seg_1009" s="T88">po-n</ta>
            <ta e="T90" id="Seg_1010" s="T89">taqɨ-n</ta>
            <ta e="T91" id="Seg_1011" s="T90">man</ta>
            <ta e="T92" id="Seg_1012" s="T91">päldɨ-mbɨ-ku-sɨ-ŋ</ta>
            <ta e="T93" id="Seg_1013" s="T92">maːt-qən</ta>
            <ta e="T94" id="Seg_1014" s="T93">omdɨ-lʼčǝ-ku-sɨ-ŋ</ta>
            <ta e="T95" id="Seg_1015" s="T94">kartopka-m</ta>
            <ta e="T96" id="Seg_1016" s="T95">paldʼu-ku-sɨ-ŋ</ta>
            <ta e="T97" id="Seg_1017" s="T96">nʼuːǯə-ndɨ</ta>
            <ta e="T98" id="Seg_1018" s="T97">padʼa-ntɨ-lɨ-le</ta>
            <ta e="T99" id="Seg_1019" s="T98">nʼuːǯə-lʼ</ta>
            <ta e="T100" id="Seg_1020" s="T99">poqqo-m</ta>
            <ta e="T101" id="Seg_1021" s="T100">pot-ku-sɨ-ŋ</ta>
            <ta e="T102" id="Seg_1022" s="T101">qwǝlɨ-ndɨ</ta>
            <ta e="T103" id="Seg_1023" s="T102">paldʼu-ku-sɨ-ŋ</ta>
            <ta e="T104" id="Seg_1024" s="T103">tʼü-n-par-ɨ-ndɨ</ta>
            <ta e="T105" id="Seg_1025" s="T104">puːna-lʼ</ta>
            <ta e="T106" id="Seg_1026" s="T105">takǝ-ntɨ-le</ta>
            <ta e="T107" id="Seg_1027" s="T106">mandu-lʼ</ta>
            <ta e="T108" id="Seg_1028" s="T107">wättɨ-ntɨ-qi-sɨ-n</ta>
            <ta e="T109" id="Seg_1029" s="T108">man</ta>
            <ta e="T110" id="Seg_1030" s="T109">malmɨ-či-sɨ-ŋ</ta>
            <ta e="T111" id="Seg_1031" s="T110">oːqəl-lǯi-ku-le</ta>
            <ta e="T112" id="Seg_1032" s="T111">oldǝ-sɨ-n</ta>
            <ta e="T113" id="Seg_1033" s="T112">suːrɨ-le</ta>
            <ta e="T114" id="Seg_1034" s="T113">pemɨ-mbɨ-le</ta>
            <ta e="T115" id="Seg_1035" s="T114">man</ta>
            <ta e="T116" id="Seg_1036" s="T115">nʼaja-sɨ-ŋ</ta>
            <ta e="T117" id="Seg_1037" s="T116">päŋqa-sɨ-ŋ</ta>
            <ta e="T118" id="Seg_1038" s="T117">tiːtam</ta>
            <ta e="T119" id="Seg_1039" s="T118">man</ta>
            <ta e="T120" id="Seg_1040" s="T119">naj</ta>
            <ta e="T121" id="Seg_1041" s="T120">suːrǝm-j-ku-ŋɨ-ŋ</ta>
            <ta e="T122" id="Seg_1042" s="T121">i</ta>
            <ta e="T123" id="Seg_1043" s="T122">laqqɨ-ŋ</ta>
            <ta e="T124" id="Seg_1044" s="T123">kalhos-qən</ta>
            <ta e="T125" id="Seg_1045" s="T124">man</ta>
            <ta e="T126" id="Seg_1046" s="T125">taw-tʼelɨ</ta>
            <ta e="T127" id="Seg_1047" s="T126">niː-ku-mbɨ-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1048" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_1049" s="T2">be.born-PST.NAR-1SG.S</ta>
            <ta e="T4" id="Seg_1050" s="T3">village-LOC</ta>
            <ta e="T5" id="Seg_1051" s="T4">Markovo</ta>
            <ta e="T6" id="Seg_1052" s="T5">I.ALL</ta>
            <ta e="T7" id="Seg_1053" s="T6">five-ADJZ</ta>
            <ta e="T8" id="Seg_1054" s="T7">superfluous</ta>
            <ta e="T9" id="Seg_1055" s="T8">twenty-ADJZ</ta>
            <ta e="T10" id="Seg_1056" s="T9">year.[NOM]-1SG</ta>
            <ta e="T11" id="Seg_1057" s="T10">I.NOM</ta>
            <ta e="T12" id="Seg_1058" s="T11">all</ta>
            <ta e="T13" id="Seg_1059" s="T12">live-ACTN2-EP-ADES</ta>
            <ta e="T14" id="Seg_1060" s="T13">live-1SG.S</ta>
            <ta e="T15" id="Seg_1061" s="T14">one</ta>
            <ta e="T16" id="Seg_1062" s="T15">village-LOC</ta>
            <ta e="T17" id="Seg_1063" s="T16">I.NOM</ta>
            <ta e="T18" id="Seg_1064" s="T17">mother.[NOM]-1SG</ta>
            <ta e="T19" id="Seg_1065" s="T18">die-PST.NAR.[3SG.S]</ta>
            <ta e="T20" id="Seg_1066" s="T19">when</ta>
            <ta e="T21" id="Seg_1067" s="T20">I.ALL</ta>
            <ta e="T22" id="Seg_1068" s="T21">be-PST-3SG.S</ta>
            <ta e="T23" id="Seg_1069" s="T22">five</ta>
            <ta e="T24" id="Seg_1070" s="T23">year.[NOM]-1SG</ta>
            <ta e="T25" id="Seg_1071" s="T24">father.[NOM]-1SG</ta>
            <ta e="T26" id="Seg_1072" s="T25">get.married-PST.[3SG.S]</ta>
            <ta e="T27" id="Seg_1073" s="T26">father.[NOM]-1SG-ADES</ta>
            <ta e="T28" id="Seg_1074" s="T27">stay-PST.[3SG.S]</ta>
            <ta e="T29" id="Seg_1075" s="T28">be-PST-3SG.S</ta>
            <ta e="T30" id="Seg_1076" s="T29">four</ta>
            <ta e="T31" id="Seg_1077" s="T30">child.[NOM]</ta>
            <ta e="T32" id="Seg_1078" s="T31">first-something-GEN-ALL</ta>
            <ta e="T33" id="Seg_1079" s="T32">be-PST-3SG.S</ta>
            <ta e="T34" id="Seg_1080" s="T33">ten</ta>
            <ta e="T35" id="Seg_1081" s="T34">year.[NOM]-3SG</ta>
            <ta e="T36" id="Seg_1082" s="T35">two-ORD-ADJZ-something-GEN-ALL</ta>
            <ta e="T37" id="Seg_1083" s="T36">be-PST-3SG.S</ta>
            <ta e="T38" id="Seg_1084" s="T37">seven</ta>
            <ta e="T39" id="Seg_1085" s="T38">year.[NOM]-3SG</ta>
            <ta e="T40" id="Seg_1086" s="T39">I.NOM</ta>
            <ta e="T41" id="Seg_1087" s="T40">be-PST-3SG.S</ta>
            <ta e="T42" id="Seg_1088" s="T41">three-ORD</ta>
            <ta e="T43" id="Seg_1089" s="T42">four-ORD</ta>
            <ta e="T44" id="Seg_1090" s="T43">daughter-human.being-EP-ADJZ</ta>
            <ta e="T45" id="Seg_1091" s="T44">child-GEN-ALL</ta>
            <ta e="T46" id="Seg_1092" s="T45">be-PST-3SG.S</ta>
            <ta e="T47" id="Seg_1093" s="T46">three</ta>
            <ta e="T48" id="Seg_1094" s="T47">year.[NOM]-3SG</ta>
            <ta e="T49" id="Seg_1095" s="T48">first</ta>
            <ta e="T50" id="Seg_1096" s="T49">we.ACC</ta>
            <ta e="T51" id="Seg_1097" s="T50">grow.up-TR-DUR-PST.[3SG.S]</ta>
            <ta e="T52" id="Seg_1098" s="T51">grandmother.[NOM]</ta>
            <ta e="T53" id="Seg_1099" s="T52">grandfather.[NOM]</ta>
            <ta e="T54" id="Seg_1100" s="T53">when</ta>
            <ta e="T55" id="Seg_1101" s="T54">father.[NOM]-1SG</ta>
            <ta e="T56" id="Seg_1102" s="T55">get.married-PST.[3SG.S]</ta>
            <ta e="T57" id="Seg_1103" s="T56">we.NOM</ta>
            <ta e="T58" id="Seg_1104" s="T57">grow.up-CVB</ta>
            <ta e="T59" id="Seg_1105" s="T58">go-PST-1PL</ta>
            <ta e="T60" id="Seg_1106" s="T59">new</ta>
            <ta e="T61" id="Seg_1107" s="T60">mother-GEN-ADES</ta>
            <ta e="T62" id="Seg_1108" s="T61">one</ta>
            <ta e="T63" id="Seg_1109" s="T62">instead.of</ta>
            <ta e="T64" id="Seg_1110" s="T63">ten-EP-ADJZ</ta>
            <ta e="T65" id="Seg_1111" s="T64">year-LOC</ta>
            <ta e="T66" id="Seg_1112" s="T65">I.NOM</ta>
            <ta e="T67" id="Seg_1113" s="T66">go.away-PST-1SG.S</ta>
            <ta e="T68" id="Seg_1114" s="T67">school-ILL</ta>
            <ta e="T69" id="Seg_1115" s="T68">school.[NOM]</ta>
            <ta e="T70" id="Seg_1116" s="T69">be-PST-3SG.S</ta>
            <ta e="T71" id="Seg_1117" s="T70">we.NOM-ABL</ta>
            <ta e="T72" id="Seg_1118" s="T71">ten</ta>
            <ta e="T73" id="Seg_1119" s="T72">kilometer-EP-ACC</ta>
            <ta e="T74" id="Seg_1120" s="T73">first</ta>
            <ta e="T75" id="Seg_1121" s="T74">I.NOM</ta>
            <ta e="T76" id="Seg_1122" s="T75">live-PST-1SG.S</ta>
            <ta e="T77" id="Seg_1123" s="T76">human.being-PL-GEN-ADES</ta>
            <ta e="T78" id="Seg_1124" s="T77">but</ta>
            <ta e="T79" id="Seg_1125" s="T78">then</ta>
            <ta e="T80" id="Seg_1126" s="T79">go-PST-1SG.S</ta>
            <ta e="T81" id="Seg_1127" s="T80">leg-INSTR2</ta>
            <ta e="T82" id="Seg_1128" s="T81">winter-ADV.LOC</ta>
            <ta e="T83" id="Seg_1129" s="T82">be-HAB-PST.[3SG.S]</ta>
            <ta e="T84" id="Seg_1130" s="T83">be.frosty-HAB-PST.[3SG.S]</ta>
            <ta e="T85" id="Seg_1131" s="T84">then</ta>
            <ta e="T86" id="Seg_1132" s="T85">I.NOM</ta>
            <ta e="T87" id="Seg_1133" s="T86">learn-PFV-HAB-PST-1SG.S</ta>
            <ta e="T88" id="Seg_1134" s="T87">three</ta>
            <ta e="T89" id="Seg_1135" s="T88">year-ADV.LOC</ta>
            <ta e="T90" id="Seg_1136" s="T89">summer-ADV.LOC</ta>
            <ta e="T91" id="Seg_1137" s="T90">I.NOM</ta>
            <ta e="T92" id="Seg_1138" s="T91">help-DUR-HAB-PST-1SG.S</ta>
            <ta e="T93" id="Seg_1139" s="T92">house-LOC</ta>
            <ta e="T94" id="Seg_1140" s="T93">sit.down-PFV-HAB-PST-1SG.S</ta>
            <ta e="T95" id="Seg_1141" s="T94">potato-ACC</ta>
            <ta e="T96" id="Seg_1142" s="T95">go-HAB-PST-1SG.S</ta>
            <ta e="T97" id="Seg_1143" s="T96">hay-ILL</ta>
            <ta e="T98" id="Seg_1144" s="T97">chop-IPFV-RES-CVB</ta>
            <ta e="T99" id="Seg_1145" s="T98">grass-ADJZ</ta>
            <ta e="T100" id="Seg_1146" s="T99">net-ACC</ta>
            <ta e="T101" id="Seg_1147" s="T100">settle.net-HAB-PST-1SG.S</ta>
            <ta e="T102" id="Seg_1148" s="T101">fish-ILL</ta>
            <ta e="T103" id="Seg_1149" s="T102">go-HAB-PST-1SG.S</ta>
            <ta e="T104" id="Seg_1150" s="T103">earth-GEN-top-EP-ILL</ta>
            <ta e="T105" id="Seg_1151" s="T104">mushroom-ADJZ</ta>
            <ta e="T106" id="Seg_1152" s="T105">gather-IPFV-CVB</ta>
            <ta e="T107" id="Seg_1153" s="T106">cowberry-ADJZ</ta>
            <ta e="T108" id="Seg_1154" s="T107">collect-IPFV-3DU.S-PST-3SG.S</ta>
            <ta e="T109" id="Seg_1155" s="T108">I.NOM</ta>
            <ta e="T110" id="Seg_1156" s="T109">go.off-DRV-PST-1SG.S</ta>
            <ta e="T111" id="Seg_1157" s="T110">learn-TR-HAB-CVB</ta>
            <ta e="T112" id="Seg_1158" s="T111">start-PST-3SG.S</ta>
            <ta e="T113" id="Seg_1159" s="T112">hunt-CVB</ta>
            <ta e="T114" id="Seg_1160" s="T113">hunt-DUR-CVB</ta>
            <ta e="T115" id="Seg_1161" s="T114">I.NOM</ta>
            <ta e="T116" id="Seg_1162" s="T115">squirrel.[CAP]-PST-1SG.S</ta>
            <ta e="T117" id="Seg_1163" s="T116">elk.[CAP]-PST-1SG.S</ta>
            <ta e="T118" id="Seg_1164" s="T117">now</ta>
            <ta e="T119" id="Seg_1165" s="T118">I.NOM</ta>
            <ta e="T120" id="Seg_1166" s="T119">also</ta>
            <ta e="T121" id="Seg_1167" s="T120">wild.animal-CAP-HAB-CO-1SG.S</ta>
            <ta e="T122" id="Seg_1168" s="T121">and</ta>
            <ta e="T123" id="Seg_1169" s="T122">work-1SG.S</ta>
            <ta e="T124" id="Seg_1170" s="T123">collective-LOC</ta>
            <ta e="T125" id="Seg_1171" s="T124">I.NOM</ta>
            <ta e="T126" id="Seg_1172" s="T125">this-day.[NOM]</ta>
            <ta e="T127" id="Seg_1173" s="T126">rest-HAB-PST.NAR-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1174" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_1175" s="T2">родиться-PST.NAR-1SG.S</ta>
            <ta e="T4" id="Seg_1176" s="T3">деревня-LOC</ta>
            <ta e="T5" id="Seg_1177" s="T4">Марково</ta>
            <ta e="T6" id="Seg_1178" s="T5">я.ALL</ta>
            <ta e="T7" id="Seg_1179" s="T6">пять-ADJZ</ta>
            <ta e="T8" id="Seg_1180" s="T7">излишний</ta>
            <ta e="T9" id="Seg_1181" s="T8">двадцать-ADJZ</ta>
            <ta e="T10" id="Seg_1182" s="T9">год.[NOM]-1SG</ta>
            <ta e="T11" id="Seg_1183" s="T10">я.NOM</ta>
            <ta e="T12" id="Seg_1184" s="T11">весь</ta>
            <ta e="T13" id="Seg_1185" s="T12">жить-ACTN2-EP-ADES</ta>
            <ta e="T14" id="Seg_1186" s="T13">жить-1SG.S</ta>
            <ta e="T15" id="Seg_1187" s="T14">один</ta>
            <ta e="T16" id="Seg_1188" s="T15">деревня-LOC</ta>
            <ta e="T17" id="Seg_1189" s="T16">я.NOM</ta>
            <ta e="T18" id="Seg_1190" s="T17">мать.[NOM]-1SG</ta>
            <ta e="T19" id="Seg_1191" s="T18">умереть-PST.NAR.[3SG.S]</ta>
            <ta e="T20" id="Seg_1192" s="T19">когда</ta>
            <ta e="T21" id="Seg_1193" s="T20">я.ALL</ta>
            <ta e="T22" id="Seg_1194" s="T21">быть-PST-3SG.S</ta>
            <ta e="T23" id="Seg_1195" s="T22">пять</ta>
            <ta e="T24" id="Seg_1196" s="T23">год.[NOM]-1SG</ta>
            <ta e="T25" id="Seg_1197" s="T24">отец.[NOM]-1SG</ta>
            <ta e="T26" id="Seg_1198" s="T25">жениться-PST.[3SG.S]</ta>
            <ta e="T27" id="Seg_1199" s="T26">отец.[NOM]-1SG-ADES</ta>
            <ta e="T28" id="Seg_1200" s="T27">остаться-PST.[3SG.S]</ta>
            <ta e="T29" id="Seg_1201" s="T28">быть-PST-3SG.S</ta>
            <ta e="T30" id="Seg_1202" s="T29">четыре</ta>
            <ta e="T31" id="Seg_1203" s="T30">ребёнок.[NOM]</ta>
            <ta e="T32" id="Seg_1204" s="T31">первый-нечто-GEN-ALL</ta>
            <ta e="T33" id="Seg_1205" s="T32">быть-PST-3SG.S</ta>
            <ta e="T34" id="Seg_1206" s="T33">десять</ta>
            <ta e="T35" id="Seg_1207" s="T34">год.[NOM]-3SG</ta>
            <ta e="T36" id="Seg_1208" s="T35">два-ORD-ADJZ-нечто-GEN-ALL</ta>
            <ta e="T37" id="Seg_1209" s="T36">быть-PST-3SG.S</ta>
            <ta e="T38" id="Seg_1210" s="T37">семь</ta>
            <ta e="T39" id="Seg_1211" s="T38">год.[NOM]-3SG</ta>
            <ta e="T40" id="Seg_1212" s="T39">я.NOM</ta>
            <ta e="T41" id="Seg_1213" s="T40">быть-PST-3SG.S</ta>
            <ta e="T42" id="Seg_1214" s="T41">три-ORD</ta>
            <ta e="T43" id="Seg_1215" s="T42">четыре-ORD</ta>
            <ta e="T44" id="Seg_1216" s="T43">дочь-человек-EP-ADJZ</ta>
            <ta e="T45" id="Seg_1217" s="T44">ребёнок-GEN-ALL</ta>
            <ta e="T46" id="Seg_1218" s="T45">быть-PST-3SG.S</ta>
            <ta e="T47" id="Seg_1219" s="T46">три</ta>
            <ta e="T48" id="Seg_1220" s="T47">год.[NOM]-3SG</ta>
            <ta e="T49" id="Seg_1221" s="T48">сперва</ta>
            <ta e="T50" id="Seg_1222" s="T49">мы.ACC</ta>
            <ta e="T51" id="Seg_1223" s="T50">вырасти-TR-DUR-PST.[3SG.S]</ta>
            <ta e="T52" id="Seg_1224" s="T51">бабушка.[NOM]</ta>
            <ta e="T53" id="Seg_1225" s="T52">дедушка.[NOM]</ta>
            <ta e="T54" id="Seg_1226" s="T53">когда</ta>
            <ta e="T55" id="Seg_1227" s="T54">отец.[NOM]-1SG</ta>
            <ta e="T56" id="Seg_1228" s="T55">жениться-PST.[3SG.S]</ta>
            <ta e="T57" id="Seg_1229" s="T56">мы.NOM</ta>
            <ta e="T58" id="Seg_1230" s="T57">вырасти-CVB</ta>
            <ta e="T59" id="Seg_1231" s="T58">идти-PST-1PL</ta>
            <ta e="T60" id="Seg_1232" s="T59">новый</ta>
            <ta e="T61" id="Seg_1233" s="T60">мать-GEN-ADES</ta>
            <ta e="T62" id="Seg_1234" s="T61">один</ta>
            <ta e="T63" id="Seg_1235" s="T62">вместо</ta>
            <ta e="T64" id="Seg_1236" s="T63">десять-EP-ADJZ</ta>
            <ta e="T65" id="Seg_1237" s="T64">год-LOC</ta>
            <ta e="T66" id="Seg_1238" s="T65">я.NOM</ta>
            <ta e="T67" id="Seg_1239" s="T66">уйти-PST-1SG.S</ta>
            <ta e="T68" id="Seg_1240" s="T67">школа-ILL</ta>
            <ta e="T69" id="Seg_1241" s="T68">школа.[NOM]</ta>
            <ta e="T70" id="Seg_1242" s="T69">быть-PST-3SG.S</ta>
            <ta e="T71" id="Seg_1243" s="T70">мы.NOM-ABL</ta>
            <ta e="T72" id="Seg_1244" s="T71">десять</ta>
            <ta e="T73" id="Seg_1245" s="T72">километр-EP-ACC</ta>
            <ta e="T74" id="Seg_1246" s="T73">сперва</ta>
            <ta e="T75" id="Seg_1247" s="T74">я.NOM</ta>
            <ta e="T76" id="Seg_1248" s="T75">жить-PST-1SG.S</ta>
            <ta e="T77" id="Seg_1249" s="T76">человек-PL-GEN-ADES</ta>
            <ta e="T78" id="Seg_1250" s="T77">а</ta>
            <ta e="T79" id="Seg_1251" s="T78">потом</ta>
            <ta e="T80" id="Seg_1252" s="T79">ходить-PST-1SG.S</ta>
            <ta e="T81" id="Seg_1253" s="T80">нога-INSTR2</ta>
            <ta e="T82" id="Seg_1254" s="T81">зима-ADV.LOC</ta>
            <ta e="T83" id="Seg_1255" s="T82">быть-HAB-PST.[3SG.S]</ta>
            <ta e="T84" id="Seg_1256" s="T83">быть.морозу-HAB-PST.[3SG.S]</ta>
            <ta e="T85" id="Seg_1257" s="T84">потом</ta>
            <ta e="T86" id="Seg_1258" s="T85">я.NOM</ta>
            <ta e="T87" id="Seg_1259" s="T86">учиться-PFV-HAB-PST-1SG.S</ta>
            <ta e="T88" id="Seg_1260" s="T87">три</ta>
            <ta e="T89" id="Seg_1261" s="T88">год-ADV.LOC</ta>
            <ta e="T90" id="Seg_1262" s="T89">лето-ADV.LOC</ta>
            <ta e="T91" id="Seg_1263" s="T90">я.NOM</ta>
            <ta e="T92" id="Seg_1264" s="T91">помочь-DUR-HAB-PST-1SG.S</ta>
            <ta e="T93" id="Seg_1265" s="T92">дом-LOC</ta>
            <ta e="T94" id="Seg_1266" s="T93">сесть-PFV-HAB-PST-1SG.S</ta>
            <ta e="T95" id="Seg_1267" s="T94">картошка-ACC</ta>
            <ta e="T96" id="Seg_1268" s="T95">ходить-HAB-PST-1SG.S</ta>
            <ta e="T97" id="Seg_1269" s="T96">сено-ILL</ta>
            <ta e="T98" id="Seg_1270" s="T97">рубить-IPFV-RES-CVB</ta>
            <ta e="T99" id="Seg_1271" s="T98">трава-ADJZ</ta>
            <ta e="T100" id="Seg_1272" s="T99">сеть-ACC</ta>
            <ta e="T101" id="Seg_1273" s="T100">поставить.сеть-HAB-PST-1SG.S</ta>
            <ta e="T102" id="Seg_1274" s="T101">рыба-ILL</ta>
            <ta e="T103" id="Seg_1275" s="T102">ходить-HAB-PST-1SG.S</ta>
            <ta e="T104" id="Seg_1276" s="T103">земля-GEN-верх-EP-ILL</ta>
            <ta e="T105" id="Seg_1277" s="T104">гриб-ADJZ</ta>
            <ta e="T106" id="Seg_1278" s="T105">собирать-IPFV-CVB</ta>
            <ta e="T107" id="Seg_1279" s="T106">брусника-ADJZ</ta>
            <ta e="T108" id="Seg_1280" s="T107">собирать-IPFV-3DU.S-PST-3SG.S</ta>
            <ta e="T109" id="Seg_1281" s="T108">я.NOM</ta>
            <ta e="T110" id="Seg_1282" s="T109">пройти-DRV-PST-1SG.S</ta>
            <ta e="T111" id="Seg_1283" s="T110">учиться-TR-HAB-CVB</ta>
            <ta e="T112" id="Seg_1284" s="T111">начать-PST-3SG.S</ta>
            <ta e="T113" id="Seg_1285" s="T112">охотиться-CVB</ta>
            <ta e="T114" id="Seg_1286" s="T113">охотиться-DUR-CVB</ta>
            <ta e="T115" id="Seg_1287" s="T114">я.NOM</ta>
            <ta e="T116" id="Seg_1288" s="T115">белка.[CAP]-PST-1SG.S</ta>
            <ta e="T117" id="Seg_1289" s="T116">лось.[CAP]-PST-1SG.S</ta>
            <ta e="T118" id="Seg_1290" s="T117">сейчас</ta>
            <ta e="T119" id="Seg_1291" s="T118">я.NOM</ta>
            <ta e="T120" id="Seg_1292" s="T119">тоже</ta>
            <ta e="T121" id="Seg_1293" s="T120">зверь-CAP-HAB-CO-1SG.S</ta>
            <ta e="T122" id="Seg_1294" s="T121">и</ta>
            <ta e="T123" id="Seg_1295" s="T122">работать-1SG.S</ta>
            <ta e="T124" id="Seg_1296" s="T123">колхоз-LOC</ta>
            <ta e="T125" id="Seg_1297" s="T124">я.NOM</ta>
            <ta e="T126" id="Seg_1298" s="T125">этот-день.[NOM]</ta>
            <ta e="T127" id="Seg_1299" s="T126">отдохнуть-HAB-PST.NAR-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1300" s="T1">pers</ta>
            <ta e="T3" id="Seg_1301" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_1302" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_1303" s="T4">nprop</ta>
            <ta e="T6" id="Seg_1304" s="T5">pers</ta>
            <ta e="T7" id="Seg_1305" s="T6">num-n&gt;adj</ta>
            <ta e="T8" id="Seg_1306" s="T7">adj</ta>
            <ta e="T9" id="Seg_1307" s="T8">num-n&gt;adj</ta>
            <ta e="T10" id="Seg_1308" s="T9">n.[n:case]-n:poss</ta>
            <ta e="T11" id="Seg_1309" s="T10">pers</ta>
            <ta e="T12" id="Seg_1310" s="T11">quant</ta>
            <ta e="T13" id="Seg_1311" s="T12">v-v&gt;n-n:ins-n:case</ta>
            <ta e="T14" id="Seg_1312" s="T13">v-v:pn</ta>
            <ta e="T15" id="Seg_1313" s="T14">num</ta>
            <ta e="T16" id="Seg_1314" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_1315" s="T16">pers</ta>
            <ta e="T18" id="Seg_1316" s="T17">n.[n:case]-n:poss</ta>
            <ta e="T19" id="Seg_1317" s="T18">v-v:tense.[v:pn]</ta>
            <ta e="T20" id="Seg_1318" s="T19">conj</ta>
            <ta e="T21" id="Seg_1319" s="T20">pers</ta>
            <ta e="T22" id="Seg_1320" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_1321" s="T22">num</ta>
            <ta e="T24" id="Seg_1322" s="T23">n.[n:case]-n:poss</ta>
            <ta e="T25" id="Seg_1323" s="T24">n.[n:case]-n:poss</ta>
            <ta e="T26" id="Seg_1324" s="T25">v-v:tense.[v:pn]</ta>
            <ta e="T27" id="Seg_1325" s="T26">n.[n:case]-n:poss-n:case</ta>
            <ta e="T28" id="Seg_1326" s="T27">v-v:tense.[v:pn]</ta>
            <ta e="T29" id="Seg_1327" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_1328" s="T29">num</ta>
            <ta e="T31" id="Seg_1329" s="T30">n.[n:case]</ta>
            <ta e="T32" id="Seg_1330" s="T31">adj-n-n:case-n:case</ta>
            <ta e="T33" id="Seg_1331" s="T32">v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_1332" s="T33">num</ta>
            <ta e="T35" id="Seg_1333" s="T34">n.[n:case]-n:poss</ta>
            <ta e="T36" id="Seg_1334" s="T35">num-num&gt;adj-n&gt;adj-n-n:case-n:case</ta>
            <ta e="T37" id="Seg_1335" s="T36">v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_1336" s="T37">num</ta>
            <ta e="T39" id="Seg_1337" s="T38">n.[n:case]-n:poss</ta>
            <ta e="T40" id="Seg_1338" s="T39">pers</ta>
            <ta e="T41" id="Seg_1339" s="T40">v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_1340" s="T41">num-num&gt;adj</ta>
            <ta e="T43" id="Seg_1341" s="T42">num-num&gt;adj</ta>
            <ta e="T44" id="Seg_1342" s="T43">n-n-n:ins-n&gt;adj</ta>
            <ta e="T45" id="Seg_1343" s="T44">n-n:case-n:case</ta>
            <ta e="T46" id="Seg_1344" s="T45">v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_1345" s="T46">num</ta>
            <ta e="T48" id="Seg_1346" s="T47">n.[n:case]-n:poss</ta>
            <ta e="T49" id="Seg_1347" s="T48">adv</ta>
            <ta e="T50" id="Seg_1348" s="T49">pers</ta>
            <ta e="T51" id="Seg_1349" s="T50">v-v&gt;v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T52" id="Seg_1350" s="T51">n.[n:case]</ta>
            <ta e="T53" id="Seg_1351" s="T52">n.[n:case]</ta>
            <ta e="T54" id="Seg_1352" s="T53">conj</ta>
            <ta e="T55" id="Seg_1353" s="T54">n.[n:case]-n:poss</ta>
            <ta e="T56" id="Seg_1354" s="T55">v-v:tense.[v:pn]</ta>
            <ta e="T57" id="Seg_1355" s="T56">pers</ta>
            <ta e="T58" id="Seg_1356" s="T57">v-v&gt;adv</ta>
            <ta e="T59" id="Seg_1357" s="T58">v-v:tense-v:pn</ta>
            <ta e="T60" id="Seg_1358" s="T59">adj</ta>
            <ta e="T61" id="Seg_1359" s="T60">n-n:case-n:case</ta>
            <ta e="T62" id="Seg_1360" s="T61">num</ta>
            <ta e="T63" id="Seg_1361" s="T62">pp</ta>
            <ta e="T64" id="Seg_1362" s="T63">num-n:ins-n&gt;adj</ta>
            <ta e="T65" id="Seg_1363" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_1364" s="T65">pers</ta>
            <ta e="T67" id="Seg_1365" s="T66">v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_1366" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_1367" s="T68">n.[n:case]</ta>
            <ta e="T70" id="Seg_1368" s="T69">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_1369" s="T70">pers-n:case</ta>
            <ta e="T72" id="Seg_1370" s="T71">num</ta>
            <ta e="T73" id="Seg_1371" s="T72">n-n:ins-n:case</ta>
            <ta e="T74" id="Seg_1372" s="T73">adv</ta>
            <ta e="T75" id="Seg_1373" s="T74">pers</ta>
            <ta e="T76" id="Seg_1374" s="T75">v-v:tense-v:pn</ta>
            <ta e="T77" id="Seg_1375" s="T76">n-n:num-n:case-n:case</ta>
            <ta e="T78" id="Seg_1376" s="T77">conj</ta>
            <ta e="T79" id="Seg_1377" s="T78">adv</ta>
            <ta e="T80" id="Seg_1378" s="T79">v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_1379" s="T80">n-n:case</ta>
            <ta e="T82" id="Seg_1380" s="T81">n-adv:case</ta>
            <ta e="T83" id="Seg_1381" s="T82">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T84" id="Seg_1382" s="T83">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T85" id="Seg_1383" s="T84">adv</ta>
            <ta e="T86" id="Seg_1384" s="T85">pers</ta>
            <ta e="T87" id="Seg_1385" s="T86">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_1386" s="T87">num</ta>
            <ta e="T89" id="Seg_1387" s="T88">n-adv:case</ta>
            <ta e="T90" id="Seg_1388" s="T89">n-adv:case</ta>
            <ta e="T91" id="Seg_1389" s="T90">pers</ta>
            <ta e="T92" id="Seg_1390" s="T91">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T93" id="Seg_1391" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_1392" s="T93">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T95" id="Seg_1393" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_1394" s="T95">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_1395" s="T96">n-n:case</ta>
            <ta e="T98" id="Seg_1396" s="T97">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T99" id="Seg_1397" s="T98">n-n&gt;adj</ta>
            <ta e="T100" id="Seg_1398" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_1399" s="T100">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T102" id="Seg_1400" s="T101">n-n:case</ta>
            <ta e="T103" id="Seg_1401" s="T102">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T104" id="Seg_1402" s="T103">n-n:case-n-n:ins-n:case</ta>
            <ta e="T105" id="Seg_1403" s="T104">n-n&gt;adj</ta>
            <ta e="T106" id="Seg_1404" s="T105">v-v&gt;v-v&gt;adv</ta>
            <ta e="T107" id="Seg_1405" s="T106">n-n&gt;adj</ta>
            <ta e="T108" id="Seg_1406" s="T107">v-v&gt;v-v:pn-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_1407" s="T108">pers</ta>
            <ta e="T110" id="Seg_1408" s="T109">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T111" id="Seg_1409" s="T110">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T112" id="Seg_1410" s="T111">v-v:tense-v:pn</ta>
            <ta e="T113" id="Seg_1411" s="T112">v-v&gt;adv</ta>
            <ta e="T114" id="Seg_1412" s="T113">v-v&gt;v-v&gt;adv</ta>
            <ta e="T115" id="Seg_1413" s="T114">pers</ta>
            <ta e="T116" id="Seg_1414" s="T115">n.[n&gt;v]-v:tense-v:pn</ta>
            <ta e="T117" id="Seg_1415" s="T116">n.[n&gt;v]-v:tense-v:pn</ta>
            <ta e="T118" id="Seg_1416" s="T117">adv</ta>
            <ta e="T119" id="Seg_1417" s="T118">pers</ta>
            <ta e="T120" id="Seg_1418" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_1419" s="T120">n-n&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T122" id="Seg_1420" s="T121">conj</ta>
            <ta e="T123" id="Seg_1421" s="T122">v-v:pn</ta>
            <ta e="T124" id="Seg_1422" s="T123">n-n:case</ta>
            <ta e="T125" id="Seg_1423" s="T124">pers</ta>
            <ta e="T126" id="Seg_1424" s="T125">dem-n.[n:case]</ta>
            <ta e="T127" id="Seg_1425" s="T126">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1426" s="T1">pers</ta>
            <ta e="T3" id="Seg_1427" s="T2">v</ta>
            <ta e="T4" id="Seg_1428" s="T3">n</ta>
            <ta e="T5" id="Seg_1429" s="T4">nprop</ta>
            <ta e="T6" id="Seg_1430" s="T5">pers</ta>
            <ta e="T7" id="Seg_1431" s="T6">adj</ta>
            <ta e="T8" id="Seg_1432" s="T7">adj</ta>
            <ta e="T9" id="Seg_1433" s="T8">adj</ta>
            <ta e="T10" id="Seg_1434" s="T9">n</ta>
            <ta e="T11" id="Seg_1435" s="T10">pers</ta>
            <ta e="T12" id="Seg_1436" s="T11">quant</ta>
            <ta e="T13" id="Seg_1437" s="T12">n</ta>
            <ta e="T14" id="Seg_1438" s="T13">v</ta>
            <ta e="T15" id="Seg_1439" s="T14">num</ta>
            <ta e="T16" id="Seg_1440" s="T15">n</ta>
            <ta e="T17" id="Seg_1441" s="T16">pers</ta>
            <ta e="T18" id="Seg_1442" s="T17">n</ta>
            <ta e="T19" id="Seg_1443" s="T18">v</ta>
            <ta e="T20" id="Seg_1444" s="T19">conj</ta>
            <ta e="T21" id="Seg_1445" s="T20">pers</ta>
            <ta e="T22" id="Seg_1446" s="T21">v</ta>
            <ta e="T23" id="Seg_1447" s="T22">num</ta>
            <ta e="T24" id="Seg_1448" s="T23">n</ta>
            <ta e="T25" id="Seg_1449" s="T24">n</ta>
            <ta e="T26" id="Seg_1450" s="T25">v</ta>
            <ta e="T27" id="Seg_1451" s="T26">n</ta>
            <ta e="T28" id="Seg_1452" s="T27">v</ta>
            <ta e="T29" id="Seg_1453" s="T28">v</ta>
            <ta e="T30" id="Seg_1454" s="T29">num</ta>
            <ta e="T31" id="Seg_1455" s="T30">n</ta>
            <ta e="T32" id="Seg_1456" s="T31">adj</ta>
            <ta e="T33" id="Seg_1457" s="T32">v</ta>
            <ta e="T34" id="Seg_1458" s="T33">num</ta>
            <ta e="T35" id="Seg_1459" s="T34">v</ta>
            <ta e="T36" id="Seg_1460" s="T35">n</ta>
            <ta e="T37" id="Seg_1461" s="T36">v</ta>
            <ta e="T38" id="Seg_1462" s="T37">num</ta>
            <ta e="T39" id="Seg_1463" s="T38">n</ta>
            <ta e="T40" id="Seg_1464" s="T39">pers</ta>
            <ta e="T41" id="Seg_1465" s="T40">v</ta>
            <ta e="T42" id="Seg_1466" s="T41">num</ta>
            <ta e="T43" id="Seg_1467" s="T42">num</ta>
            <ta e="T44" id="Seg_1468" s="T43">n</ta>
            <ta e="T45" id="Seg_1469" s="T44">n</ta>
            <ta e="T46" id="Seg_1470" s="T45">v</ta>
            <ta e="T47" id="Seg_1471" s="T46">num</ta>
            <ta e="T48" id="Seg_1472" s="T47">n</ta>
            <ta e="T49" id="Seg_1473" s="T48">adv</ta>
            <ta e="T50" id="Seg_1474" s="T49">pers</ta>
            <ta e="T51" id="Seg_1475" s="T50">v</ta>
            <ta e="T52" id="Seg_1476" s="T51">n</ta>
            <ta e="T53" id="Seg_1477" s="T52">n</ta>
            <ta e="T54" id="Seg_1478" s="T53">interrog</ta>
            <ta e="T55" id="Seg_1479" s="T54">n</ta>
            <ta e="T56" id="Seg_1480" s="T55">v</ta>
            <ta e="T57" id="Seg_1481" s="T56">pers</ta>
            <ta e="T58" id="Seg_1482" s="T57">adv</ta>
            <ta e="T59" id="Seg_1483" s="T58">v</ta>
            <ta e="T60" id="Seg_1484" s="T59">adj</ta>
            <ta e="T61" id="Seg_1485" s="T60">n</ta>
            <ta e="T62" id="Seg_1486" s="T61">num</ta>
            <ta e="T63" id="Seg_1487" s="T62">pp</ta>
            <ta e="T64" id="Seg_1488" s="T63">adj</ta>
            <ta e="T65" id="Seg_1489" s="T64">n</ta>
            <ta e="T66" id="Seg_1490" s="T65">pers</ta>
            <ta e="T67" id="Seg_1491" s="T66">v</ta>
            <ta e="T68" id="Seg_1492" s="T67">n</ta>
            <ta e="T69" id="Seg_1493" s="T68">n</ta>
            <ta e="T70" id="Seg_1494" s="T69">v</ta>
            <ta e="T71" id="Seg_1495" s="T70">pers</ta>
            <ta e="T72" id="Seg_1496" s="T71">num</ta>
            <ta e="T73" id="Seg_1497" s="T72">n</ta>
            <ta e="T74" id="Seg_1498" s="T73">adv</ta>
            <ta e="T75" id="Seg_1499" s="T74">pers</ta>
            <ta e="T76" id="Seg_1500" s="T75">v</ta>
            <ta e="T77" id="Seg_1501" s="T76">n</ta>
            <ta e="T78" id="Seg_1502" s="T77">conj</ta>
            <ta e="T79" id="Seg_1503" s="T78">adv</ta>
            <ta e="T80" id="Seg_1504" s="T79">v</ta>
            <ta e="T81" id="Seg_1505" s="T80">n</ta>
            <ta e="T82" id="Seg_1506" s="T81">n</ta>
            <ta e="T83" id="Seg_1507" s="T82">v</ta>
            <ta e="T84" id="Seg_1508" s="T83">v</ta>
            <ta e="T85" id="Seg_1509" s="T84">adv</ta>
            <ta e="T86" id="Seg_1510" s="T85">pers</ta>
            <ta e="T87" id="Seg_1511" s="T86">v</ta>
            <ta e="T88" id="Seg_1512" s="T87">num</ta>
            <ta e="T89" id="Seg_1513" s="T88">n</ta>
            <ta e="T90" id="Seg_1514" s="T89">n</ta>
            <ta e="T91" id="Seg_1515" s="T90">pers</ta>
            <ta e="T92" id="Seg_1516" s="T91">v</ta>
            <ta e="T93" id="Seg_1517" s="T92">n</ta>
            <ta e="T94" id="Seg_1518" s="T93">v</ta>
            <ta e="T95" id="Seg_1519" s="T94">n</ta>
            <ta e="T96" id="Seg_1520" s="T95">v</ta>
            <ta e="T97" id="Seg_1521" s="T96">n</ta>
            <ta e="T99" id="Seg_1522" s="T98">adj</ta>
            <ta e="T100" id="Seg_1523" s="T99">n</ta>
            <ta e="T101" id="Seg_1524" s="T100">v</ta>
            <ta e="T102" id="Seg_1525" s="T101">n</ta>
            <ta e="T103" id="Seg_1526" s="T102">v</ta>
            <ta e="T104" id="Seg_1527" s="T103">n</ta>
            <ta e="T105" id="Seg_1528" s="T104">adj</ta>
            <ta e="T106" id="Seg_1529" s="T105">adv</ta>
            <ta e="T107" id="Seg_1530" s="T106">adj</ta>
            <ta e="T108" id="Seg_1531" s="T107">v</ta>
            <ta e="T109" id="Seg_1532" s="T108">pers</ta>
            <ta e="T110" id="Seg_1533" s="T109">v</ta>
            <ta e="T111" id="Seg_1534" s="T110">adv</ta>
            <ta e="T112" id="Seg_1535" s="T111">v</ta>
            <ta e="T113" id="Seg_1536" s="T112">adv</ta>
            <ta e="T114" id="Seg_1537" s="T113">adv</ta>
            <ta e="T115" id="Seg_1538" s="T114">pers</ta>
            <ta e="T116" id="Seg_1539" s="T115">v</ta>
            <ta e="T117" id="Seg_1540" s="T116">v</ta>
            <ta e="T118" id="Seg_1541" s="T117">adv</ta>
            <ta e="T119" id="Seg_1542" s="T118">pers</ta>
            <ta e="T120" id="Seg_1543" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_1544" s="T120">adj</ta>
            <ta e="T122" id="Seg_1545" s="T121">conj</ta>
            <ta e="T123" id="Seg_1546" s="T122">v</ta>
            <ta e="T124" id="Seg_1547" s="T123">n</ta>
            <ta e="T125" id="Seg_1548" s="T124">pers</ta>
            <ta e="T126" id="Seg_1549" s="T125">n</ta>
            <ta e="T127" id="Seg_1550" s="T126">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1551" s="T1">pro.h:P</ta>
            <ta e="T4" id="Seg_1552" s="T3">np:L</ta>
            <ta e="T6" id="Seg_1553" s="T5">pro.h:Poss</ta>
            <ta e="T10" id="Seg_1554" s="T9">np:Th</ta>
            <ta e="T11" id="Seg_1555" s="T10">pro.h:Th</ta>
            <ta e="T16" id="Seg_1556" s="T15">np:L</ta>
            <ta e="T17" id="Seg_1557" s="T16">pro.h:Poss</ta>
            <ta e="T18" id="Seg_1558" s="T17">np.h:P</ta>
            <ta e="T21" id="Seg_1559" s="T20">pro.h:Poss</ta>
            <ta e="T24" id="Seg_1560" s="T23">np:Th</ta>
            <ta e="T25" id="Seg_1561" s="T24">np.h:A 0.1.h:Poss</ta>
            <ta e="T27" id="Seg_1562" s="T26">np.h:Poss 0.1.h:Poss</ta>
            <ta e="T31" id="Seg_1563" s="T30">np.h:Th</ta>
            <ta e="T32" id="Seg_1564" s="T31">np.h:Poss</ta>
            <ta e="T35" id="Seg_1565" s="T34">np:Th</ta>
            <ta e="T36" id="Seg_1566" s="T35">np.h:Poss</ta>
            <ta e="T39" id="Seg_1567" s="T38">np:Th</ta>
            <ta e="T40" id="Seg_1568" s="T39">pro.h:Th</ta>
            <ta e="T45" id="Seg_1569" s="T44">np.h:Poss</ta>
            <ta e="T48" id="Seg_1570" s="T47">np:Th</ta>
            <ta e="T50" id="Seg_1571" s="T49">pro.h:Th</ta>
            <ta e="T52" id="Seg_1572" s="T51">np.h:A</ta>
            <ta e="T53" id="Seg_1573" s="T52">np.h:A</ta>
            <ta e="T55" id="Seg_1574" s="T54">np.h:A 0.1.h:Poss</ta>
            <ta e="T57" id="Seg_1575" s="T56">pro.h:Th</ta>
            <ta e="T61" id="Seg_1576" s="T60">np.h:L</ta>
            <ta e="T65" id="Seg_1577" s="T64">np:Time</ta>
            <ta e="T66" id="Seg_1578" s="T65">pro.h:A</ta>
            <ta e="T68" id="Seg_1579" s="T67">np:G</ta>
            <ta e="T69" id="Seg_1580" s="T68">np:Th</ta>
            <ta e="T71" id="Seg_1581" s="T70">pro.h:So</ta>
            <ta e="T73" id="Seg_1582" s="T72">np:L</ta>
            <ta e="T74" id="Seg_1583" s="T73">adv:Time</ta>
            <ta e="T75" id="Seg_1584" s="T74">pro.h:Th</ta>
            <ta e="T77" id="Seg_1585" s="T76">np.h:L</ta>
            <ta e="T79" id="Seg_1586" s="T78">adv:Time</ta>
            <ta e="T80" id="Seg_1587" s="T79">0.1.h:A</ta>
            <ta e="T81" id="Seg_1588" s="T80">np:Ins</ta>
            <ta e="T82" id="Seg_1589" s="T81">adv:Time</ta>
            <ta e="T83" id="Seg_1590" s="T82">0.3:Th</ta>
            <ta e="T84" id="Seg_1591" s="T83">0.3:Th</ta>
            <ta e="T85" id="Seg_1592" s="T84">adv:Time</ta>
            <ta e="T86" id="Seg_1593" s="T85">pro.h:A</ta>
            <ta e="T89" id="Seg_1594" s="T88">adv:Time</ta>
            <ta e="T90" id="Seg_1595" s="T89">adv:Time</ta>
            <ta e="T91" id="Seg_1596" s="T90">pro.h:A</ta>
            <ta e="T93" id="Seg_1597" s="T92">np:L</ta>
            <ta e="T94" id="Seg_1598" s="T93">0.1.h:A</ta>
            <ta e="T95" id="Seg_1599" s="T94">np:Th</ta>
            <ta e="T96" id="Seg_1600" s="T95">0.1.h:A</ta>
            <ta e="T97" id="Seg_1601" s="T96">np:P</ta>
            <ta e="T98" id="Seg_1602" s="T97">0.1.h:A</ta>
            <ta e="T100" id="Seg_1603" s="T99">np:Th</ta>
            <ta e="T101" id="Seg_1604" s="T100">0.1.h:A</ta>
            <ta e="T103" id="Seg_1605" s="T102">0.1.h:A</ta>
            <ta e="T104" id="Seg_1606" s="T103">np:G</ta>
            <ta e="T105" id="Seg_1607" s="T104">np:Th</ta>
            <ta e="T106" id="Seg_1608" s="T105">0.1.h:A</ta>
            <ta e="T107" id="Seg_1609" s="T106">np:Th</ta>
            <ta e="T108" id="Seg_1610" s="T107">0.3.h:A</ta>
            <ta e="T109" id="Seg_1611" s="T108">pro.h:A</ta>
            <ta e="T112" id="Seg_1612" s="T111">0.1.h:A</ta>
            <ta e="T115" id="Seg_1613" s="T114">pro.h:A</ta>
            <ta e="T117" id="Seg_1614" s="T116">0.1.h:A</ta>
            <ta e="T118" id="Seg_1615" s="T117">adv:Time</ta>
            <ta e="T119" id="Seg_1616" s="T118">pro.h:A</ta>
            <ta e="T123" id="Seg_1617" s="T122">0.1.h:A</ta>
            <ta e="T124" id="Seg_1618" s="T123">np:L</ta>
            <ta e="T125" id="Seg_1619" s="T124">pro.h:Th</ta>
            <ta e="T126" id="Seg_1620" s="T125">np:Time</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1621" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_1622" s="T2">v:pred</ta>
            <ta e="T10" id="Seg_1623" s="T9">np:S</ta>
            <ta e="T11" id="Seg_1624" s="T10">pro.h:S</ta>
            <ta e="T14" id="Seg_1625" s="T13">v:pred</ta>
            <ta e="T18" id="Seg_1626" s="T17">np.h:S</ta>
            <ta e="T19" id="Seg_1627" s="T18">v:pred</ta>
            <ta e="T24" id="Seg_1628" s="T19">s:temp</ta>
            <ta e="T25" id="Seg_1629" s="T24">np.h:S</ta>
            <ta e="T26" id="Seg_1630" s="T25">v:pred</ta>
            <ta e="T29" id="Seg_1631" s="T28">v:pred</ta>
            <ta e="T31" id="Seg_1632" s="T30">np.h:S</ta>
            <ta e="T33" id="Seg_1633" s="T32">v:pred</ta>
            <ta e="T35" id="Seg_1634" s="T34">np:S</ta>
            <ta e="T37" id="Seg_1635" s="T36">v:pred</ta>
            <ta e="T39" id="Seg_1636" s="T38">np:S</ta>
            <ta e="T40" id="Seg_1637" s="T39">pro.h:S</ta>
            <ta e="T41" id="Seg_1638" s="T40">cop</ta>
            <ta e="T42" id="Seg_1639" s="T41">adj:pred</ta>
            <ta e="T46" id="Seg_1640" s="T45">v:pred</ta>
            <ta e="T48" id="Seg_1641" s="T47">np:S</ta>
            <ta e="T50" id="Seg_1642" s="T49">pro.h:O</ta>
            <ta e="T51" id="Seg_1643" s="T50">v:pred</ta>
            <ta e="T52" id="Seg_1644" s="T51">np.h:S</ta>
            <ta e="T53" id="Seg_1645" s="T52">np.h:S</ta>
            <ta e="T56" id="Seg_1646" s="T53">s:temp</ta>
            <ta e="T57" id="Seg_1647" s="T56">pro.h:S</ta>
            <ta e="T59" id="Seg_1648" s="T58">v:pred</ta>
            <ta e="T66" id="Seg_1649" s="T65">pro.h:S</ta>
            <ta e="T67" id="Seg_1650" s="T66">v:pred</ta>
            <ta e="T69" id="Seg_1651" s="T68">np:S</ta>
            <ta e="T70" id="Seg_1652" s="T69">v:pred</ta>
            <ta e="T75" id="Seg_1653" s="T74">pro.h:S</ta>
            <ta e="T76" id="Seg_1654" s="T75">v:pred</ta>
            <ta e="T80" id="Seg_1655" s="T79">0.1.h:S v:pred</ta>
            <ta e="T83" id="Seg_1656" s="T82">0.3:S v:pred</ta>
            <ta e="T84" id="Seg_1657" s="T83">0.3:S v:pred</ta>
            <ta e="T86" id="Seg_1658" s="T85">pro.h:S</ta>
            <ta e="T87" id="Seg_1659" s="T86">v:pred</ta>
            <ta e="T91" id="Seg_1660" s="T90">pro.h:S</ta>
            <ta e="T92" id="Seg_1661" s="T91">v:pred</ta>
            <ta e="T94" id="Seg_1662" s="T93">0.1.h:S v:pred</ta>
            <ta e="T95" id="Seg_1663" s="T94">np:O</ta>
            <ta e="T96" id="Seg_1664" s="T95">0.1.h:S v:pred</ta>
            <ta e="T98" id="Seg_1665" s="T96">s:purp</ta>
            <ta e="T100" id="Seg_1666" s="T99">np:O</ta>
            <ta e="T101" id="Seg_1667" s="T100">0.1.h:S v:pred</ta>
            <ta e="T103" id="Seg_1668" s="T102">0.1.h:S v:pred</ta>
            <ta e="T106" id="Seg_1669" s="T104">s:purp</ta>
            <ta e="T107" id="Seg_1670" s="T106">np:O</ta>
            <ta e="T108" id="Seg_1671" s="T107">0.3.h:S v:pred</ta>
            <ta e="T109" id="Seg_1672" s="T108">pro.h:S</ta>
            <ta e="T110" id="Seg_1673" s="T109">v:pred</ta>
            <ta e="T112" id="Seg_1674" s="T111">0.1.h:S v:pred</ta>
            <ta e="T115" id="Seg_1675" s="T114">pro.h:S</ta>
            <ta e="T116" id="Seg_1676" s="T115">v:pred</ta>
            <ta e="T117" id="Seg_1677" s="T116">0.1.h:S v:pred</ta>
            <ta e="T119" id="Seg_1678" s="T118">pro.h:S</ta>
            <ta e="T121" id="Seg_1679" s="T120">v:pred</ta>
            <ta e="T123" id="Seg_1680" s="T122">0.1.h:S v:pred</ta>
            <ta e="T125" id="Seg_1681" s="T124">pro.h:S</ta>
            <ta e="T127" id="Seg_1682" s="T126">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T12" id="Seg_1683" s="T11">RUS:core</ta>
            <ta e="T68" id="Seg_1684" s="T67">RUS:cult</ta>
            <ta e="T69" id="Seg_1685" s="T68">RUS:cult</ta>
            <ta e="T73" id="Seg_1686" s="T72">RUS:cult</ta>
            <ta e="T78" id="Seg_1687" s="T77">RUS:gram</ta>
            <ta e="T95" id="Seg_1688" s="T94">RUS:cult</ta>
            <ta e="T122" id="Seg_1689" s="T121">RUS:gram</ta>
            <ta e="T124" id="Seg_1690" s="T123">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T95" id="Seg_1691" s="T94">Csub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T68" id="Seg_1692" s="T67">dir:infl</ta>
            <ta e="T69" id="Seg_1693" s="T68">dir:bare</ta>
            <ta e="T73" id="Seg_1694" s="T72">dir:infl</ta>
            <ta e="T95" id="Seg_1695" s="T94">dir:infl</ta>
            <ta e="T124" id="Seg_1696" s="T123">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_1697" s="T1">I was born in Markovo.</ta>
            <ta e="T10" id="Seg_1698" s="T5">I am twenty-five years old.</ta>
            <ta e="T16" id="Seg_1699" s="T10">I have lived my whole life in the same village.</ta>
            <ta e="T19" id="Seg_1700" s="T16">My mother is dead.</ta>
            <ta e="T26" id="Seg_1701" s="T19">When I was five years old, my father got married.</ta>
            <ta e="T31" id="Seg_1702" s="T26">My father had four children.</ta>
            <ta e="T35" id="Seg_1703" s="T31">The first one was ten years old.</ta>
            <ta e="T39" id="Seg_1704" s="T35">The second was seven years old.</ta>
            <ta e="T42" id="Seg_1705" s="T39">I was the third.</ta>
            <ta e="T48" id="Seg_1706" s="T42">The fourth girl was three years old.</ta>
            <ta e="T53" id="Seg_1707" s="T48">First we were grown by grandparents.</ta>
            <ta e="T61" id="Seg_1708" s="T53">After my father got married, we grew up with our new mom.</ta>
            <ta e="T68" id="Seg_1709" s="T61">I went to school for nine years.</ta>
            <ta e="T73" id="Seg_1710" s="T68">The school was ten kilometers away from us.</ta>
            <ta e="T81" id="Seg_1711" s="T73">First I lived with people, then I went on foot.</ta>
            <ta e="T84" id="Seg_1712" s="T81">It was cold in the winter.</ta>
            <ta e="T89" id="Seg_1713" s="T84">I learned there for three years.</ta>
            <ta e="T108" id="Seg_1714" s="T89">I helped out at home in the summer: went mowing the grass, put nets, went to the forest to collect mushrooms, cranberries collected.</ta>
            <ta e="T117" id="Seg_1715" s="T108">I finished the school, started hunting, hunting elks.</ta>
            <ta e="T124" id="Seg_1716" s="T117">Now I'm hunting, too, and I'm working on a collective farm.</ta>
            <ta e="T127" id="Seg_1717" s="T124">I'm resting today.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_1718" s="T1">Ich wurde im Dorf Markovo geboren.</ta>
            <ta e="T10" id="Seg_1719" s="T5">Ich bin fünfundzwanzig Jahre alt.</ta>
            <ta e="T16" id="Seg_1720" s="T10">Ich habe mein ganzes Leben im selben Dorf verbracht.</ta>
            <ta e="T19" id="Seg_1721" s="T16">Meine Mutter ist gestorben.</ta>
            <ta e="T26" id="Seg_1722" s="T19">Als ich fünf Jahre alt war, hat mein Vater geheiratet.</ta>
            <ta e="T31" id="Seg_1723" s="T26">Mein Vater hatte vier Kinder.</ta>
            <ta e="T35" id="Seg_1724" s="T31">Der erste war zehn Jahre alt.</ta>
            <ta e="T39" id="Seg_1725" s="T35">Der zweite war sieben Jahre alt.</ta>
            <ta e="T42" id="Seg_1726" s="T39">Ich war der dritte.</ta>
            <ta e="T48" id="Seg_1727" s="T42">Das vierte Mädchen war drei Jahre alt.</ta>
            <ta e="T53" id="Seg_1728" s="T48">Zuerst wurden wir von Großeltern aufgezogen.</ta>
            <ta e="T61" id="Seg_1729" s="T53">Nachdem mein Vater geheiratet hat, sind wir mit unserer neuen Mutter aufgewachsen.</ta>
            <ta e="T68" id="Seg_1730" s="T61">Ich ging neun Jahre lang zur Schule.</ta>
            <ta e="T73" id="Seg_1731" s="T68">Die Schule war zehn Kilometer von uns entfernt.</ta>
            <ta e="T81" id="Seg_1732" s="T73">Zuerst lebte ich bei Leuten, dann ging ich zu Fuß.</ta>
            <ta e="T84" id="Seg_1733" s="T81">Im Winter war es kalt.</ta>
            <ta e="T89" id="Seg_1734" s="T84">Ich lernte drei Jahre lang dort.</ta>
            <ta e="T108" id="Seg_1735" s="T89">Ich habe im Sommer zu Hause geholfen: ging das Gras mähen, legte Netze aus, ging Pilze zu sammeln, Preiselbeeren gesammelt.</ta>
            <ta e="T117" id="Seg_1736" s="T108">Ich beendete die Schule, habe angefangen zu jagen, %%.</ta>
            <ta e="T124" id="Seg_1737" s="T117">Jetzt bin ich auch auf der Jagd und arbeite in einer Kolchose.</ta>
            <ta e="T127" id="Seg_1738" s="T124">Ich ruhe mich heute aus.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_1739" s="T1">Я родился в деревне Марково.</ta>
            <ta e="T10" id="Seg_1740" s="T5">Мне двадцать пять лет.</ta>
            <ta e="T16" id="Seg_1741" s="T10">Я всю жизнь живу в одной деревне.</ta>
            <ta e="T19" id="Seg_1742" s="T16">Моя мать умерла.</ta>
            <ta e="T26" id="Seg_1743" s="T19">Когда мне было пять лет, (мой) отец женился.</ta>
            <ta e="T31" id="Seg_1744" s="T26">У моего отца было (осталось) четверо детей.</ta>
            <ta e="T35" id="Seg_1745" s="T31">Первому было десять лет.</ta>
            <ta e="T39" id="Seg_1746" s="T35">Второму было семь лет.</ta>
            <ta e="T42" id="Seg_1747" s="T39">Я был третий.</ta>
            <ta e="T48" id="Seg_1748" s="T42">Четвертой девочке было три года.</ta>
            <ta e="T53" id="Seg_1749" s="T48">Сначала нас растили бабушка и дедушка.</ta>
            <ta e="T61" id="Seg_1750" s="T53">После того как отец женился, мы росли у новой мамы.</ta>
            <ta e="T68" id="Seg_1751" s="T61">В девять лет я пошел в школу.</ta>
            <ta e="T73" id="Seg_1752" s="T68">Школа была от нас за десять километров.</ta>
            <ta e="T81" id="Seg_1753" s="T73">Сначала я жил у людей, а потом ходил пешком.</ta>
            <ta e="T84" id="Seg_1754" s="T81">Зимой холодно было.</ta>
            <ta e="T89" id="Seg_1755" s="T84">Там я учился три года.</ta>
            <ta e="T108" id="Seg_1756" s="T89">Летом я помогал дома: ходил на покос косить траву, сети ставил на рыбу, ходил на бор грибы собирать, бруснику собирал.</ta>
            <ta e="T117" id="Seg_1757" s="T108">Кончил учиться, начал охотиться, лосевал.</ta>
            <ta e="T124" id="Seg_1758" s="T117">Сейчас я тоже охотничаю и работаю в колхозе.</ta>
            <ta e="T127" id="Seg_1759" s="T124">Я сегодня отдыхаю.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_1760" s="T1">я родился в деревне М.</ta>
            <ta e="T10" id="Seg_1761" s="T5">мне двадцать пять (пять лишний от двадцати) лет</ta>
            <ta e="T16" id="Seg_1762" s="T10">я всю жизнь живу в одной деревне</ta>
            <ta e="T19" id="Seg_1763" s="T16">There is no Russian translation of this sentence.</ta>
            <ta e="T26" id="Seg_1764" s="T19">когда мне было пять лет (мой) отец женился</ta>
            <ta e="T31" id="Seg_1765" s="T26">у отца было (осталось) четверо детей</ta>
            <ta e="T35" id="Seg_1766" s="T31">первому было десять лет</ta>
            <ta e="T39" id="Seg_1767" s="T35">второму было семь</ta>
            <ta e="T42" id="Seg_1768" s="T39">я был третий</ta>
            <ta e="T48" id="Seg_1769" s="T42">четвертой девочке было три года</ta>
            <ta e="T53" id="Seg_1770" s="T48">сначала нас выращивала бабка дедушка</ta>
            <ta e="T61" id="Seg_1771" s="T53">отец женился мы росли у новой мамы</ta>
            <ta e="T68" id="Seg_1772" s="T61">девяти лет я пошел в школу</ta>
            <ta e="T73" id="Seg_1773" s="T68">школа была от нас за десять километров</ta>
            <ta e="T81" id="Seg_1774" s="T73">сначала я жил у людей а потом ходил пешком (ногами)</ta>
            <ta e="T84" id="Seg_1775" s="T81">зимой было холодно</ta>
            <ta e="T89" id="Seg_1776" s="T84">там я учился три года</ta>
            <ta e="T108" id="Seg_1777" s="T89">летом я помогал дома ходил на покос косить траву сети ставил на рыбу ходил на бор грибы собирать бруснику собирал</ta>
            <ta e="T117" id="Seg_1778" s="T108">кончил учиться начал охотиться лосевал</ta>
            <ta e="T124" id="Seg_1779" s="T117">сейчас я тоже охотничаю и работаю в колхозе</ta>
            <ta e="T127" id="Seg_1780" s="T124">я сегодня отдыхаю</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
