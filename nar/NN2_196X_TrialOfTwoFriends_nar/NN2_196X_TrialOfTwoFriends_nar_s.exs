<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>NN2_196X_TrialOfTwoFriends_nar</transcription-name>
         <referenced-file url="NN2_196X_TrialOfTwoFriends_nar.wav" />
         <referenced-file url="NN2_196X_TrialOfTwoFriends_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">NN2_196X_TrialOfTwoFriends_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">287</ud-information>
            <ud-information attribute-name="# HIAT:w">171</ud-information>
            <ud-information attribute-name="# e">185</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">14</ud-information>
            <ud-information attribute-name="# HIAT:u">24</ud-information>
            <ud-information attribute-name="# sc">4</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="NN2">
            <abbreviation>NN2</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T66" time="0.6165" type="intp" />
         <tli id="T1" time="1.233" type="appl" />
         <tli id="T2" time="2.989" type="appl" />
         <tli id="T3" time="3.634" type="appl" />
         <tli id="T4" time="4.278" type="appl" />
         <tli id="T5" time="4.923" type="appl" />
         <tli id="T6" time="5.568" type="appl" />
         <tli id="T7" time="6.213" type="appl" />
         <tli id="T8" time="6.857" type="appl" />
         <tli id="T9" time="7.502" type="appl" />
         <tli id="T10" time="8.147" type="appl" />
         <tli id="T11" time="8.792" type="appl" />
         <tli id="T12" time="9.436" type="appl" />
         <tli id="T13" time="10.081" type="appl" />
         <tli id="T14" time="10.726" type="appl" />
         <tli id="T15" time="12.066" type="appl" />
         <tli id="T16" time="12.586" type="appl" />
         <tli id="T17" time="13.106" type="appl" />
         <tli id="T18" time="13.626" type="appl" />
         <tli id="T19" time="14.146" type="appl" />
         <tli id="T20" time="14.666" type="appl" />
         <tli id="T21" time="15.735" type="appl" />
         <tli id="T22" time="16.237" type="appl" />
         <tli id="T23" time="16.74" type="appl" />
         <tli id="T24" time="17.242" type="appl" />
         <tli id="T25" time="17.744" type="appl" />
         <tli id="T26" time="18.246" type="appl" />
         <tli id="T27" time="18.749" type="appl" />
         <tli id="T28" time="19.251" type="appl" />
         <tli id="T29" time="19.753" type="appl" />
         <tli id="T30" time="20.593" type="appl" />
         <tli id="T31" time="21.094" type="appl" />
         <tli id="T32" time="21.595" type="appl" />
         <tli id="T33" time="22.096" type="appl" />
         <tli id="T34" time="22.597" type="appl" />
         <tli id="T35" time="23.098" type="appl" />
         <tli id="T36" time="23.599" type="appl" />
         <tli id="T37" time="24.276" type="appl" />
         <tli id="T38" time="24.741" type="appl" />
         <tli id="T39" time="25.205" type="appl" />
         <tli id="T40" time="25.67" type="appl" />
         <tli id="T41" time="26.134" type="appl" />
         <tli id="T42" time="26.599" type="appl" />
         <tli id="T43" time="27.063" type="appl" />
         <tli id="T44" time="27.528" type="appl" />
         <tli id="T45" time="27.992" type="appl" />
         <tli id="T46" time="28.7" type="appl" />
         <tli id="T47" time="29.253" type="appl" />
         <tli id="T48" time="29.806" type="appl" />
         <tli id="T49" time="30.36" type="appl" />
         <tli id="T50" time="31.141" type="appl" />
         <tli id="T51" time="31.516" type="appl" />
         <tli id="T52" time="31.891" type="appl" />
         <tli id="T53" time="32.266" type="appl" />
         <tli id="T54" time="32.641" type="appl" />
         <tli id="T55" time="33.016" type="appl" />
         <tli id="T56" time="33.391" type="appl" />
         <tli id="T57" time="33.766" type="appl" />
         <tli id="T58" time="34.891" type="appl" />
         <tli id="T59" time="35.469" type="appl" />
         <tli id="T60" time="36.046" type="appl" />
         <tli id="T61" time="36.624" type="appl" />
         <tli id="T62" time="37.202" type="appl" />
         <tli id="T63" time="37.78" type="appl" />
         <tli id="T64" time="38.433" type="appl" />
         <tli id="T65" time="38.773" type="appl" />
         <tli id="T67" time="39.453" type="appl" />
         <tli id="T68" time="39.793" type="appl" />
         <tli id="T69" time="40.133" type="appl" />
         <tli id="T70" time="40.943" type="appl" />
         <tli id="T71" time="41.648" type="appl" />
         <tli id="T72" time="42.352" type="appl" />
         <tli id="T73" time="43.825" type="appl" />
         <tli id="T74" time="44.144" type="appl" />
         <tli id="T75" time="44.463" type="appl" />
         <tli id="T76" time="44.782" type="appl" />
         <tli id="T77" time="45.101" type="appl" />
         <tli id="T78" time="45.42" type="appl" />
         <tli id="T79" time="45.739" type="appl" />
         <tli id="T80" time="46.058" type="appl" />
         <tli id="T81" time="46.377" type="appl" />
         <tli id="T82" time="46.696" type="appl" />
         <tli id="T83" time="47.015" type="appl" />
         <tli id="T84" time="47.334" type="appl" />
         <tli id="T85" time="47.653" type="appl" />
         <tli id="T86" time="48.355" type="appl" />
         <tli id="T87" time="48.644" type="appl" />
         <tli id="T88" time="48.934" type="appl" />
         <tli id="T89" time="49.223" type="appl" />
         <tli id="T90" time="49.512" type="appl" />
         <tli id="T91" time="49.801" type="appl" />
         <tli id="T92" time="50.091" type="appl" />
         <tli id="T93" time="50.38" type="appl" />
         <tli id="T94" time="50.669" type="appl" />
         <tli id="T95" time="50.958" type="appl" />
         <tli id="T96" time="51.248" type="appl" />
         <tli id="T97" time="51.537" type="appl" />
         <tli id="T98" time="51.826" type="appl" />
         <tli id="T99" time="54.168" type="appl" />
         <tli id="T100" time="54.65" type="appl" />
         <tli id="T101" time="55.132" type="appl" />
         <tli id="T102" time="55.613" type="appl" />
         <tli id="T103" time="56.095" type="appl" />
         <tli id="T104" time="56.577" type="appl" />
         <tli id="T105" time="57.059" type="appl" />
         <tli id="T106" time="58.536" type="appl" />
         <tli id="T107" time="58.847" type="appl" />
         <tli id="T108" time="59.157" type="appl" />
         <tli id="T109" time="59.468" type="appl" />
         <tli id="T110" time="59.778" type="appl" />
         <tli id="T111" time="60.089" type="appl" />
         <tli id="T112" time="60.399" type="appl" />
         <tli id="T113" time="60.71" type="appl" />
         <tli id="T114" time="61.02" type="appl" />
         <tli id="T115" time="62.672" type="appl" />
         <tli id="T116" time="62.95" type="appl" />
         <tli id="T117" time="63.229" type="appl" />
         <tli id="T118" time="63.507" type="appl" />
         <tli id="T119" time="63.786" type="appl" />
         <tli id="T120" time="64.064" type="appl" />
         <tli id="T121" time="64.343" type="appl" />
         <tli id="T122" time="64.622" type="appl" />
         <tli id="T123" time="64.9" type="appl" />
         <tli id="T124" time="65.179" type="appl" />
         <tli id="T125" time="65.457" type="appl" />
         <tli id="T126" time="65.736" type="appl" />
         <tli id="T127" time="66.014" type="appl" />
         <tli id="T128" time="66.293" type="appl" />
         <tli id="T129" time="67.163" type="appl" />
         <tli id="T130" time="67.541" type="appl" />
         <tli id="T131" time="67.918" type="appl" />
         <tli id="T132" time="68.295" type="appl" />
         <tli id="T133" time="68.672" type="appl" />
         <tli id="T134" time="69.05" type="appl" />
         <tli id="T135" time="69.427" type="appl" />
         <tli id="T136" time="69.804" type="appl" />
         <tli id="T137" time="70.181" type="appl" />
         <tli id="T138" time="70.559" type="appl" />
         <tli id="T139" time="70.936" type="appl" />
         <tli id="T140" time="71.313" type="appl" />
         <tli id="T141" time="72.604" type="appl" />
         <tli id="T142" time="73.209" type="appl" />
         <tli id="T143" time="73.813" type="appl" />
         <tli id="T144" time="74.417" type="appl" />
         <tli id="T145" time="75.022" type="appl" />
         <tli id="T146" time="75.626" type="appl" />
         <tli id="T147" time="76.897" type="appl" />
         <tli id="T148" time="77.362" type="appl" />
         <tli id="T149" time="77.828" type="appl" />
         <tli id="T150" time="78.293" type="appl" />
         <tli id="T151" time="78.758" type="appl" />
         <tli id="T152" time="79.223" type="appl" />
         <tli id="T153" time="79.689" type="appl" />
         <tli id="T154" time="80.154" type="appl" />
         <tli id="T155" time="80.619" type="appl" />
         <tli id="T156" time="82.241" type="appl" />
         <tli id="T157" time="82.956" type="appl" />
         <tli id="T158" time="83.671" type="appl" />
         <tli id="T159" time="84.386" type="appl" />
         <tli id="T160" time="85.118" type="appl" />
         <tli id="T161" time="85.63" type="appl" />
         <tli id="T162" time="86.141" type="appl" />
         <tli id="T163" time="86.653" type="appl" />
         <tli id="T164" time="91.569" type="appl" />
         <tli id="T165" time="92.019" type="appl" />
         <tli id="T166" time="92.469" type="appl" />
         <tli id="T167" time="92.919" type="appl" />
         <tli id="T168" time="93.369" type="appl" />
         <tli id="T169" time="93.819" type="appl" />
         <tli id="T170" time="94.269" type="appl" />
         <tli id="T171" time="94.719" type="appl" />
         <tli id="T172" time="95.506" type="appl" />
         <tli id="T173" time="95.84" type="appl" />
         <tli id="T174" time="96.173" type="appl" />
         <tli id="T175" time="96.506" type="appl" />
         <tli id="T176" time="96.84" type="appl" />
         <tli id="T177" time="97.173" type="appl" />
         <tli id="T178" time="97.506" type="appl" />
         <tli id="T179" time="97.84" type="appl" />
         <tli id="T180" time="98.173" type="appl" />
         <tli id="T181" time="98.506" type="appl" />
         <tli id="T182" time="98.84" type="appl" />
         <tli id="T183" time="99.173" type="appl" />
         <tli id="T184" time="99.506" type="appl" />
         <tli id="T185" time="99.84" type="appl" />
         <tli id="T186" time="100.173" type="appl" />
         <tli id="T187" time="100.861" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="NN2"
                      type="t">
         <timeline-fork end="T66" start="T0">
            <tli id="T0.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T39" id="Seg_0" n="sc" s="T0">
               <ts e="T66" id="Seg_2" n="HIAT:u" s="T0">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <nts id="Seg_4" n="HIAT:ip">(</nts>
                  <ats e="T0.tx.1" id="Seg_5" n="HIAT:non-pho" s="T0">KuAI:</ats>
                  <nts id="Seg_6" n="HIAT:ip">)</nts>
                  <nts id="Seg_7" n="HIAT:ip">)</nts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_10" n="HIAT:w" s="T0.tx.1">Bolʼšoj</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_14" n="HIAT:u" s="T66">
                  <nts id="Seg_15" n="HIAT:ip">(</nts>
                  <nts id="Seg_16" n="HIAT:ip">(</nts>
                  <ats e="T1" id="Seg_17" n="HIAT:non-pho" s="T66">NN2:</ats>
                  <nts id="Seg_18" n="HIAT:ip">)</nts>
                  <nts id="Seg_19" n="HIAT:ip">)</nts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_22" n="HIAT:w" s="T1">Tap</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_25" n="HIAT:w" s="T2">čʼeːlʼ</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_27" n="HIAT:ip">(</nts>
                  <ts e="T4" id="Seg_29" n="HIAT:w" s="T3">takkɨ-</ts>
                  <nts id="Seg_30" n="HIAT:ip">)</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_33" n="HIAT:w" s="T4">tap</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_36" n="HIAT:w" s="T5">čʼeːlʼ</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_38" n="HIAT:ip">(</nts>
                  <ts e="T7" id="Seg_40" n="HIAT:w" s="T6">takkɨ-</ts>
                  <nts id="Seg_41" n="HIAT:ip">)</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_44" n="HIAT:w" s="T7">taqqɨlıːmpɔːtɨn</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_47" n="HIAT:w" s="T8">šʼittɨ</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_50" n="HIAT:w" s="T9">qumɔːqı</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_53" n="HIAT:w" s="T10">tɨmtɨ</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_55" n="HIAT:ip">(</nts>
                  <ts e="T12" id="Seg_57" n="HIAT:w" s="T11">sutʼisɔː-</ts>
                  <nts id="Seg_58" n="HIAT:ip">)</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_61" n="HIAT:w" s="T12">sutʼisɔːtɨn</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_64" n="HIAT:w" s="T13">kontorqɨn</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_68" n="HIAT:u" s="T14">
                  <nts id="Seg_69" n="HIAT:ip">(</nts>
                  <ts e="T15" id="Seg_71" n="HIAT:w" s="T14">Lʼonja</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_74" n="HIAT:w" s="T15">aj=</ts>
                  <nts id="Seg_75" n="HIAT:ip">)</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_78" n="HIAT:w" s="T16">Lʼonja</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_80" n="HIAT:ip">(</nts>
                  <ts e="T18" id="Seg_82" n="HIAT:w" s="T17">aj=</ts>
                  <nts id="Seg_83" n="HIAT:ip">)</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_86" n="HIAT:w" s="T18">aj</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_89" n="HIAT:w" s="T19">Petɨja</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_93" n="HIAT:u" s="T20">
                  <nts id="Seg_94" n="HIAT:ip">(</nts>
                  <ts e="T21" id="Seg_96" n="HIAT:w" s="T20">Ukkɨr</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_99" n="HIAT:w" s="T21">nʼeːmtɨ=</ts>
                  <nts id="Seg_100" n="HIAT:ip">)</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_103" n="HIAT:w" s="T22">ukkɨr</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_106" n="HIAT:w" s="T23">nʼeːmtɨ</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_109" n="HIAT:w" s="T24">pissɔːtɨn</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_111" n="HIAT:ip">(</nts>
                  <ts e="T26" id="Seg_113" n="HIAT:w" s="T25">kəš-</ts>
                  <nts id="Seg_114" n="HIAT:ip">)</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_117" n="HIAT:w" s="T26">posʼolkaqɨn</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_120" n="HIAT:w" s="T27">štrafsä</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_123" n="HIAT:w" s="T28">pissɔːtɨn</ts>
                  <nts id="Seg_124" n="HIAT:ip">.</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_127" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_129" n="HIAT:w" s="T29">Ukkɨr</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_132" n="HIAT:w" s="T30">nʼeːmtɨ</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_134" n="HIAT:ip">(</nts>
                  <ts e="T32" id="Seg_136" n="HIAT:w" s="T31">qap</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_139" n="HIAT:w" s="T32">sučʼip-</ts>
                  <nts id="Seg_140" n="HIAT:ip">)</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_143" n="HIAT:w" s="T33">qap</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_146" n="HIAT:w" s="T34">sučʼipqolapsɔːtɨn</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_148" n="HIAT:ip">(</nts>
                  <nts id="Seg_149" n="HIAT:ip">(</nts>
                  <ats e="T36" id="Seg_150" n="HIAT:non-pho" s="T35">…</ats>
                  <nts id="Seg_151" n="HIAT:ip">)</nts>
                  <nts id="Seg_152" n="HIAT:ip">)</nts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_156" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_158" n="HIAT:w" s="T36">Šittalä</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_160" n="HIAT:ip">(</nts>
                  <nts id="Seg_161" n="HIAT:ip">(</nts>
                  <ats e="T39" id="Seg_162" n="HIAT:non-pho" s="T37">sudja naːte=</ats>
                  <nts id="Seg_163" n="HIAT:ip">)</nts>
                  <nts id="Seg_164" n="HIAT:ip">)</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T186" id="Seg_166" n="sc" s="T40">
               <ts e="T45" id="Seg_168" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_170" n="HIAT:w" s="T40">qəqɨlsɔːtɨn</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_173" n="HIAT:w" s="T41">šittalä</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_176" n="HIAT:w" s="T42">nɔːt</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_179" n="HIAT:w" s="T43">aša</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_182" n="HIAT:w" s="T44">sučʼipqolapsɔːtɨn</ts>
                  <nts id="Seg_183" n="HIAT:ip">.</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_186" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_188" n="HIAT:w" s="T45">Tɨtoj</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_191" n="HIAT:w" s="T46">pot</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_194" n="HIAT:w" s="T47">čʼot</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_197" n="HIAT:w" s="T48">pissɔːtɨn</ts>
                  <nts id="Seg_198" n="HIAT:ip">.</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_201" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_203" n="HIAT:w" s="T49">Na</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_206" n="HIAT:w" s="T50">čʼap</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_209" n="HIAT:w" s="T51">tamtɨr</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_212" n="HIAT:w" s="T52">čʼap</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_215" n="HIAT:w" s="T53">pissɔːtɨn</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_218" n="HIAT:w" s="T54">na</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_221" n="HIAT:w" s="T55">Petɨja</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_224" n="HIAT:w" s="T56">pitpɨj</ts>
                  <nts id="Seg_225" n="HIAT:ip">.</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_228" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_230" n="HIAT:w" s="T57">Qara</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_233" n="HIAT:w" s="T58">Tarkosalente</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_236" n="HIAT:w" s="T59">wərkɨsɔːtɨn</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_239" n="HIAT:w" s="T60">kušak</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_241" n="HIAT:ip">(</nts>
                  <nts id="Seg_242" n="HIAT:ip">(</nts>
                  <ats e="T62" id="Seg_243" n="HIAT:non-pho" s="T61">nemɨ</ats>
                  <nts id="Seg_244" n="HIAT:ip">)</nts>
                  <nts id="Seg_245" n="HIAT:ip">)</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_248" n="HIAT:w" s="T62">ütiqa</ts>
                  <nts id="Seg_249" n="HIAT:ip">.</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_252" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_254" n="HIAT:w" s="T63">Šʼittalʼ</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_256" n="HIAT:ip">(</nts>
                  <nts id="Seg_257" n="HIAT:ip">(</nts>
                  <ats e="T65" id="Seg_258" n="HIAT:non-pho" s="T64">…</ats>
                  <nts id="Seg_259" n="HIAT:ip">)</nts>
                  <nts id="Seg_260" n="HIAT:ip">)</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_262" n="HIAT:ip">(</nts>
                  <nts id="Seg_263" n="HIAT:ip">(</nts>
                  <ats e="T67" id="Seg_264" n="HIAT:non-pho" s="T65">sudja-</ats>
                  <nts id="Seg_265" n="HIAT:ip">)</nts>
                  <nts id="Seg_266" n="HIAT:ip">)</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_269" n="HIAT:w" s="T67">nɔːtɨ</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_272" n="HIAT:w" s="T68">qəqɨlsɔːtɨn</ts>
                  <nts id="Seg_273" n="HIAT:ip">.</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_276" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_278" n="HIAT:w" s="T69">Me</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_281" n="HIAT:w" s="T70">əːtɨm</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_283" n="HIAT:ip">(</nts>
                  <nts id="Seg_284" n="HIAT:ip">(</nts>
                  <ats e="T72" id="Seg_285" n="HIAT:non-pho" s="T71">…</ats>
                  <nts id="Seg_286" n="HIAT:ip">)</nts>
                  <nts id="Seg_287" n="HIAT:ip">)</nts>
                  <nts id="Seg_288" n="HIAT:ip">.</nts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_291" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_293" n="HIAT:w" s="T72">Šʼittalʼ</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_296" n="HIAT:w" s="T73">me</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_299" n="HIAT:w" s="T74">nɨ</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_302" n="HIAT:w" s="T75">mat</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_305" n="HIAT:w" s="T76">me</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_308" n="HIAT:w" s="T77">nık</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_311" n="HIAT:w" s="T78">kətɨsɔːmɨn</ts>
                  <nts id="Seg_312" n="HIAT:ip">:</nts>
                  <nts id="Seg_313" n="HIAT:ip">“</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_316" n="HIAT:w" s="T79">Lʼa</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_319" n="HIAT:w" s="T80">te</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_322" n="HIAT:w" s="T81">qoštɨ</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_325" n="HIAT:w" s="T82">qumɨlʼa</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_328" n="HIAT:w" s="T83">qoštɨ</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_331" n="HIAT:w" s="T84">qumɨlʼa</ts>
                  <nts id="Seg_332" n="HIAT:ip">.</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_335" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_337" n="HIAT:w" s="T85">Tɨta</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_340" n="HIAT:w" s="T86">na</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_343" n="HIAT:w" s="T87">qumɨ</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_346" n="HIAT:w" s="T88">qaj</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_349" n="HIAT:w" s="T89">qaː</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_352" n="HIAT:w" s="T90">nʼenna</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_355" n="HIAT:w" s="T91">üːtɨsɔːlɨt</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_358" n="HIAT:w" s="T92">tɨ</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_361" n="HIAT:w" s="T93">to</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_364" n="HIAT:w" s="T94">aj</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_367" n="HIAT:w" s="T95">qumɨ</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_370" n="HIAT:w" s="T96">na</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_373" n="HIAT:w" s="T97">qättɨt</ts>
                  <nts id="Seg_374" n="HIAT:ip">”</nts>
                  <nts id="Seg_375" n="HIAT:ip">.</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_378" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_380" n="HIAT:w" s="T98">Me</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_382" n="HIAT:ip">(</nts>
                  <nts id="Seg_383" n="HIAT:ip">(</nts>
                  <ats e="T100" id="Seg_384" n="HIAT:non-pho" s="T99">tı</ats>
                  <nts id="Seg_385" n="HIAT:ip">)</nts>
                  <nts id="Seg_386" n="HIAT:ip">)</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_389" n="HIAT:w" s="T100">kɨropo</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_392" n="HIAT:w" s="T101">nennımɔːtıːsɔːmɨn</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_395" n="HIAT:w" s="T102">na</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_398" n="HIAT:w" s="T103">Peːtɨja</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_401" n="HIAT:w" s="T104">čʼɔːtɨ</ts>
                  <nts id="Seg_402" n="HIAT:ip">.</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_405" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_407" n="HIAT:w" s="T105">Meː</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_410" n="HIAT:w" s="T106">nı</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_413" n="HIAT:w" s="T107">tɛnɨrpɔːmɨn</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_416" n="HIAT:w" s="T108">qara</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_419" n="HIAT:w" s="T109">qəntɨqa</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_422" n="HIAT:w" s="T110">nada</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_424" n="HIAT:ip">(</nts>
                  <nts id="Seg_425" n="HIAT:ip">(</nts>
                  <ats e="T112" id="Seg_426" n="HIAT:non-pho" s="T111">…</ats>
                  <nts id="Seg_427" n="HIAT:ip">)</nts>
                  <nts id="Seg_428" n="HIAT:ip">)</nts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_431" n="HIAT:w" s="T112">ılla</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_434" n="HIAT:w" s="T113">omtɨltɨqa</ts>
                  <nts id="Seg_435" n="HIAT:ip">.</nts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_438" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_440" n="HIAT:w" s="T114">Meː</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_443" n="HIAT:w" s="T115">qumɨj</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_446" n="HIAT:w" s="T116">tamtɨr</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_448" n="HIAT:ip">(</nts>
                  <nts id="Seg_449" n="HIAT:ip">(</nts>
                  <ats e="T118" id="Seg_450" n="HIAT:non-pho" s="T117">…</ats>
                  <nts id="Seg_451" n="HIAT:ip">)</nts>
                  <nts id="Seg_452" n="HIAT:ip">)</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_455" n="HIAT:w" s="T118">nıː</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_458" n="HIAT:w" s="T119">pissɨtɨ</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_460" n="HIAT:ip">(</nts>
                  <nts id="Seg_461" n="HIAT:ip">(</nts>
                  <ats e="T121" id="Seg_462" n="HIAT:non-pho" s="T120">…</ats>
                  <nts id="Seg_463" n="HIAT:ip">)</nts>
                  <nts id="Seg_464" n="HIAT:ip">)</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_467" n="HIAT:w" s="T121">a</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_470" n="HIAT:w" s="T122">sudji</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_473" n="HIAT:w" s="T123">aša</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_476" n="HIAT:w" s="T124">kɨkala</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_478" n="HIAT:ip">(</nts>
                  <ts e="T126" id="Seg_480" n="HIAT:w" s="T125">nı</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_483" n="HIAT:w" s="T126">pissɔːtɨn</ts>
                  <nts id="Seg_484" n="HIAT:ip">)</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_487" n="HIAT:w" s="T127">šittalä</ts>
                  <nts id="Seg_488" n="HIAT:ip">.</nts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_491" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_493" n="HIAT:w" s="T128">Qumɨt</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_496" n="HIAT:w" s="T129">qaj</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_499" n="HIAT:w" s="T130">mɔːt</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_502" n="HIAT:w" s="T131">šunčʼa</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_505" n="HIAT:w" s="T132">ne</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_508" n="HIAT:w" s="T133">mɔːt</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_511" n="HIAT:w" s="T134">šunčʼa</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_514" n="HIAT:w" s="T135">qaj</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_517" n="HIAT:w" s="T136">təpɨt</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_520" n="HIAT:w" s="T137">ne</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_523" n="HIAT:w" s="T138">wenɨpor</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_526" n="HIAT:w" s="T139">sučʼɨlejčʼɔːtɨn</ts>
                  <nts id="Seg_527" n="HIAT:ip">.</nts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_530" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_532" n="HIAT:w" s="T140">Qumɨn</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_535" n="HIAT:w" s="T141">qotola</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_538" n="HIAT:w" s="T142">nenʼimɔːtsɔːtɨn</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_541" n="HIAT:w" s="T143">na</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_544" n="HIAT:w" s="T144">Petɨlʼa</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_547" n="HIAT:w" s="T145">čʼɔːtɨ</ts>
                  <nts id="Seg_548" n="HIAT:ip">.</nts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_551" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_553" n="HIAT:w" s="T146">Əːtɨp</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_556" n="HIAT:w" s="T147">qara</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_559" n="HIAT:w" s="T148">üːtɨqa</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_562" n="HIAT:w" s="T149">ılla</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_565" n="HIAT:w" s="T150">omtɨltɨqa</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_568" n="HIAT:w" s="T151">temɨ</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_571" n="HIAT:w" s="T152">sučʼiptɨqa</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_573" n="HIAT:ip">(</nts>
                  <nts id="Seg_574" n="HIAT:ip">(</nts>
                  <ats e="T154" id="Seg_575" n="HIAT:non-pho" s="T153">…</ats>
                  <nts id="Seg_576" n="HIAT:ip">)</nts>
                  <nts id="Seg_577" n="HIAT:ip">)</nts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_580" n="HIAT:w" s="T154">pissɔːtɨn</ts>
                  <nts id="Seg_581" n="HIAT:ip">.</nts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T159" id="Seg_584" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_586" n="HIAT:w" s="T155">Kisʼa</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_589" n="HIAT:w" s="T156">nɔːt</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_592" n="HIAT:w" s="T157">qəqɨlsɔːtɨn</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_595" n="HIAT:w" s="T158">qəqɨlsɔːtɨn</ts>
                  <nts id="Seg_596" n="HIAT:ip">.</nts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_599" n="HIAT:u" s="T159">
                  <ts e="T160" id="Seg_601" n="HIAT:w" s="T159">Šʼittalʼ</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_604" n="HIAT:w" s="T160">meː</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_607" n="HIAT:w" s="T161">nɔːtɨ</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_610" n="HIAT:w" s="T162">qəqɨlsɔːmɨn</ts>
                  <nts id="Seg_611" n="HIAT:ip">.</nts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_614" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_616" n="HIAT:w" s="T163">Sɨrqɨ</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_619" n="HIAT:w" s="T164">sajavlenie</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_622" n="HIAT:w" s="T165">miːmpɨsɨt</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_625" n="HIAT:w" s="T166">Sɨrqɨ</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_627" n="HIAT:ip">(</nts>
                  <ts e="T168" id="Seg_629" n="HIAT:w" s="T167">mɔː-</ts>
                  <nts id="Seg_630" n="HIAT:ip">)</nts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_633" n="HIAT:w" s="T168">mɔːt</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_636" n="HIAT:w" s="T169">tɨ</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_639" n="HIAT:w" s="T170">nɨtaŋɨt</ts>
                  <nts id="Seg_640" n="HIAT:ip">.</nts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_643" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_645" n="HIAT:w" s="T171">Sɨrgɨ</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_648" n="HIAT:w" s="T172">mɔːt</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_651" n="HIAT:w" s="T173">tɨ</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_654" n="HIAT:w" s="T174">nɨtaŋɨt</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_656" n="HIAT:ip">(</nts>
                  <nts id="Seg_657" n="HIAT:ip">(</nts>
                  <ats e="T176" id="Seg_658" n="HIAT:non-pho" s="T175">…</ats>
                  <nts id="Seg_659" n="HIAT:ip">)</nts>
                  <nts id="Seg_660" n="HIAT:ip">)</nts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_663" n="HIAT:w" s="T176">šʼittɨ</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_666" n="HIAT:w" s="T177">pɔːrɨ</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_669" n="HIAT:w" s="T178">pit</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_672" n="HIAT:w" s="T179">sɨqɨlla</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_675" n="HIAT:w" s="T180">pin</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_678" n="HIAT:w" s="T181">Sɨrgɨn</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_681" n="HIAT:w" s="T182">mɔːn</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_684" n="HIAT:w" s="T183">qoptɨkɔːl</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_687" n="HIAT:w" s="T184">tɨ</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_690" n="HIAT:w" s="T185">nɨtampat</ts>
                  <nts id="Seg_691" n="HIAT:ip">.</nts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T39" id="Seg_693" n="sc" s="T0">
               <ts e="T66" id="Seg_695" n="e" s="T0">((KuAI:)) Bolʼšoj. </ts>
               <ts e="T1" id="Seg_697" n="e" s="T66">((NN2:)) </ts>
               <ts e="T2" id="Seg_699" n="e" s="T1">Tap </ts>
               <ts e="T3" id="Seg_701" n="e" s="T2">čʼeːlʼ </ts>
               <ts e="T4" id="Seg_703" n="e" s="T3">(takkɨ-) </ts>
               <ts e="T5" id="Seg_705" n="e" s="T4">tap </ts>
               <ts e="T6" id="Seg_707" n="e" s="T5">čʼeːlʼ </ts>
               <ts e="T7" id="Seg_709" n="e" s="T6">(takkɨ-) </ts>
               <ts e="T8" id="Seg_711" n="e" s="T7">taqqɨlıːmpɔːtɨn </ts>
               <ts e="T9" id="Seg_713" n="e" s="T8">šʼittɨ </ts>
               <ts e="T10" id="Seg_715" n="e" s="T9">qumɔːqı </ts>
               <ts e="T11" id="Seg_717" n="e" s="T10">tɨmtɨ </ts>
               <ts e="T12" id="Seg_719" n="e" s="T11">(sutʼisɔː-) </ts>
               <ts e="T13" id="Seg_721" n="e" s="T12">sutʼisɔːtɨn </ts>
               <ts e="T14" id="Seg_723" n="e" s="T13">kontorqɨn. </ts>
               <ts e="T15" id="Seg_725" n="e" s="T14">(Lʼonja </ts>
               <ts e="T16" id="Seg_727" n="e" s="T15">aj=) </ts>
               <ts e="T17" id="Seg_729" n="e" s="T16">Lʼonja </ts>
               <ts e="T18" id="Seg_731" n="e" s="T17">(aj=) </ts>
               <ts e="T19" id="Seg_733" n="e" s="T18">aj </ts>
               <ts e="T20" id="Seg_735" n="e" s="T19">Petɨja. </ts>
               <ts e="T21" id="Seg_737" n="e" s="T20">(Ukkɨr </ts>
               <ts e="T22" id="Seg_739" n="e" s="T21">nʼeːmtɨ=) </ts>
               <ts e="T23" id="Seg_741" n="e" s="T22">ukkɨr </ts>
               <ts e="T24" id="Seg_743" n="e" s="T23">nʼeːmtɨ </ts>
               <ts e="T25" id="Seg_745" n="e" s="T24">pissɔːtɨn </ts>
               <ts e="T26" id="Seg_747" n="e" s="T25">(kəš-) </ts>
               <ts e="T27" id="Seg_749" n="e" s="T26">posʼolkaqɨn </ts>
               <ts e="T28" id="Seg_751" n="e" s="T27">štrafsä </ts>
               <ts e="T29" id="Seg_753" n="e" s="T28">pissɔːtɨn. </ts>
               <ts e="T30" id="Seg_755" n="e" s="T29">Ukkɨr </ts>
               <ts e="T31" id="Seg_757" n="e" s="T30">nʼeːmtɨ </ts>
               <ts e="T32" id="Seg_759" n="e" s="T31">(qap </ts>
               <ts e="T33" id="Seg_761" n="e" s="T32">sučʼip-) </ts>
               <ts e="T34" id="Seg_763" n="e" s="T33">qap </ts>
               <ts e="T35" id="Seg_765" n="e" s="T34">sučʼipqolapsɔːtɨn </ts>
               <ts e="T36" id="Seg_767" n="e" s="T35">((…)). </ts>
               <ts e="T37" id="Seg_769" n="e" s="T36">Šittalä </ts>
               <ts e="T38" id="Seg_771" n="e" s="T37">((sudja </ts>
               <ts e="T39" id="Seg_773" n="e" s="T38">naːte=)) </ts>
            </ts>
            <ts e="T186" id="Seg_774" n="sc" s="T40">
               <ts e="T41" id="Seg_776" n="e" s="T40">qəqɨlsɔːtɨn </ts>
               <ts e="T42" id="Seg_778" n="e" s="T41">šittalä </ts>
               <ts e="T43" id="Seg_780" n="e" s="T42">nɔːt </ts>
               <ts e="T44" id="Seg_782" n="e" s="T43">aša </ts>
               <ts e="T45" id="Seg_784" n="e" s="T44">sučʼipqolapsɔːtɨn. </ts>
               <ts e="T46" id="Seg_786" n="e" s="T45">Tɨtoj </ts>
               <ts e="T47" id="Seg_788" n="e" s="T46">pot </ts>
               <ts e="T48" id="Seg_790" n="e" s="T47">čʼot </ts>
               <ts e="T49" id="Seg_792" n="e" s="T48">pissɔːtɨn. </ts>
               <ts e="T50" id="Seg_794" n="e" s="T49">Na </ts>
               <ts e="T51" id="Seg_796" n="e" s="T50">čʼap </ts>
               <ts e="T52" id="Seg_798" n="e" s="T51">tamtɨr </ts>
               <ts e="T53" id="Seg_800" n="e" s="T52">čʼap </ts>
               <ts e="T54" id="Seg_802" n="e" s="T53">pissɔːtɨn </ts>
               <ts e="T55" id="Seg_804" n="e" s="T54">na </ts>
               <ts e="T56" id="Seg_806" n="e" s="T55">Petɨja </ts>
               <ts e="T57" id="Seg_808" n="e" s="T56">pitpɨj. </ts>
               <ts e="T58" id="Seg_810" n="e" s="T57">Qara </ts>
               <ts e="T59" id="Seg_812" n="e" s="T58">Tarkosalente </ts>
               <ts e="T60" id="Seg_814" n="e" s="T59">wərkɨsɔːtɨn </ts>
               <ts e="T61" id="Seg_816" n="e" s="T60">kušak </ts>
               <ts e="T62" id="Seg_818" n="e" s="T61">((nemɨ)) </ts>
               <ts e="T63" id="Seg_820" n="e" s="T62">ütiqa. </ts>
               <ts e="T64" id="Seg_822" n="e" s="T63">Šʼittalʼ </ts>
               <ts e="T65" id="Seg_824" n="e" s="T64">((…)) </ts>
               <ts e="T67" id="Seg_826" n="e" s="T65">((sudja-)) </ts>
               <ts e="T68" id="Seg_828" n="e" s="T67">nɔːtɨ </ts>
               <ts e="T69" id="Seg_830" n="e" s="T68">qəqɨlsɔːtɨn. </ts>
               <ts e="T70" id="Seg_832" n="e" s="T69">Me </ts>
               <ts e="T71" id="Seg_834" n="e" s="T70">əːtɨm </ts>
               <ts e="T72" id="Seg_836" n="e" s="T71">((…)). </ts>
               <ts e="T73" id="Seg_838" n="e" s="T72">Šʼittalʼ </ts>
               <ts e="T74" id="Seg_840" n="e" s="T73">me </ts>
               <ts e="T75" id="Seg_842" n="e" s="T74">nɨ </ts>
               <ts e="T76" id="Seg_844" n="e" s="T75">mat </ts>
               <ts e="T77" id="Seg_846" n="e" s="T76">me </ts>
               <ts e="T78" id="Seg_848" n="e" s="T77">nık </ts>
               <ts e="T79" id="Seg_850" n="e" s="T78">kətɨsɔːmɨn:“ </ts>
               <ts e="T80" id="Seg_852" n="e" s="T79">Lʼa </ts>
               <ts e="T81" id="Seg_854" n="e" s="T80">te </ts>
               <ts e="T82" id="Seg_856" n="e" s="T81">qoštɨ </ts>
               <ts e="T83" id="Seg_858" n="e" s="T82">qumɨlʼa </ts>
               <ts e="T84" id="Seg_860" n="e" s="T83">qoštɨ </ts>
               <ts e="T85" id="Seg_862" n="e" s="T84">qumɨlʼa. </ts>
               <ts e="T86" id="Seg_864" n="e" s="T85">Tɨta </ts>
               <ts e="T87" id="Seg_866" n="e" s="T86">na </ts>
               <ts e="T88" id="Seg_868" n="e" s="T87">qumɨ </ts>
               <ts e="T89" id="Seg_870" n="e" s="T88">qaj </ts>
               <ts e="T90" id="Seg_872" n="e" s="T89">qaː </ts>
               <ts e="T91" id="Seg_874" n="e" s="T90">nʼenna </ts>
               <ts e="T92" id="Seg_876" n="e" s="T91">üːtɨsɔːlɨt </ts>
               <ts e="T93" id="Seg_878" n="e" s="T92">tɨ </ts>
               <ts e="T94" id="Seg_880" n="e" s="T93">to </ts>
               <ts e="T95" id="Seg_882" n="e" s="T94">aj </ts>
               <ts e="T96" id="Seg_884" n="e" s="T95">qumɨ </ts>
               <ts e="T97" id="Seg_886" n="e" s="T96">na </ts>
               <ts e="T98" id="Seg_888" n="e" s="T97">qättɨt”. </ts>
               <ts e="T99" id="Seg_890" n="e" s="T98">Me </ts>
               <ts e="T100" id="Seg_892" n="e" s="T99">((tı)) </ts>
               <ts e="T101" id="Seg_894" n="e" s="T100">kɨropo </ts>
               <ts e="T102" id="Seg_896" n="e" s="T101">nennımɔːtıːsɔːmɨn </ts>
               <ts e="T103" id="Seg_898" n="e" s="T102">na </ts>
               <ts e="T104" id="Seg_900" n="e" s="T103">Peːtɨja </ts>
               <ts e="T105" id="Seg_902" n="e" s="T104">čʼɔːtɨ. </ts>
               <ts e="T106" id="Seg_904" n="e" s="T105">Meː </ts>
               <ts e="T107" id="Seg_906" n="e" s="T106">nı </ts>
               <ts e="T108" id="Seg_908" n="e" s="T107">tɛnɨrpɔːmɨn </ts>
               <ts e="T109" id="Seg_910" n="e" s="T108">qara </ts>
               <ts e="T110" id="Seg_912" n="e" s="T109">qəntɨqa </ts>
               <ts e="T111" id="Seg_914" n="e" s="T110">nada </ts>
               <ts e="T112" id="Seg_916" n="e" s="T111">((…)) </ts>
               <ts e="T113" id="Seg_918" n="e" s="T112">ılla </ts>
               <ts e="T114" id="Seg_920" n="e" s="T113">omtɨltɨqa. </ts>
               <ts e="T115" id="Seg_922" n="e" s="T114">Meː </ts>
               <ts e="T116" id="Seg_924" n="e" s="T115">qumɨj </ts>
               <ts e="T117" id="Seg_926" n="e" s="T116">tamtɨr </ts>
               <ts e="T118" id="Seg_928" n="e" s="T117">((…)) </ts>
               <ts e="T119" id="Seg_930" n="e" s="T118">nıː </ts>
               <ts e="T120" id="Seg_932" n="e" s="T119">pissɨtɨ </ts>
               <ts e="T121" id="Seg_934" n="e" s="T120">((…)) </ts>
               <ts e="T122" id="Seg_936" n="e" s="T121">a </ts>
               <ts e="T123" id="Seg_938" n="e" s="T122">sudji </ts>
               <ts e="T124" id="Seg_940" n="e" s="T123">aša </ts>
               <ts e="T125" id="Seg_942" n="e" s="T124">kɨkala </ts>
               <ts e="T126" id="Seg_944" n="e" s="T125">(nı </ts>
               <ts e="T127" id="Seg_946" n="e" s="T126">pissɔːtɨn) </ts>
               <ts e="T128" id="Seg_948" n="e" s="T127">šittalä. </ts>
               <ts e="T129" id="Seg_950" n="e" s="T128">Qumɨt </ts>
               <ts e="T130" id="Seg_952" n="e" s="T129">qaj </ts>
               <ts e="T131" id="Seg_954" n="e" s="T130">mɔːt </ts>
               <ts e="T132" id="Seg_956" n="e" s="T131">šunčʼa </ts>
               <ts e="T133" id="Seg_958" n="e" s="T132">ne </ts>
               <ts e="T134" id="Seg_960" n="e" s="T133">mɔːt </ts>
               <ts e="T135" id="Seg_962" n="e" s="T134">šunčʼa </ts>
               <ts e="T136" id="Seg_964" n="e" s="T135">qaj </ts>
               <ts e="T137" id="Seg_966" n="e" s="T136">təpɨt </ts>
               <ts e="T138" id="Seg_968" n="e" s="T137">ne </ts>
               <ts e="T139" id="Seg_970" n="e" s="T138">wenɨpor </ts>
               <ts e="T140" id="Seg_972" n="e" s="T139">sučʼɨlejčʼɔːtɨn. </ts>
               <ts e="T141" id="Seg_974" n="e" s="T140">Qumɨn </ts>
               <ts e="T142" id="Seg_976" n="e" s="T141">qotola </ts>
               <ts e="T143" id="Seg_978" n="e" s="T142">nenʼimɔːtsɔːtɨn </ts>
               <ts e="T144" id="Seg_980" n="e" s="T143">na </ts>
               <ts e="T145" id="Seg_982" n="e" s="T144">Petɨlʼa </ts>
               <ts e="T146" id="Seg_984" n="e" s="T145">čʼɔːtɨ. </ts>
               <ts e="T147" id="Seg_986" n="e" s="T146">Əːtɨp </ts>
               <ts e="T148" id="Seg_988" n="e" s="T147">qara </ts>
               <ts e="T149" id="Seg_990" n="e" s="T148">üːtɨqa </ts>
               <ts e="T150" id="Seg_992" n="e" s="T149">ılla </ts>
               <ts e="T151" id="Seg_994" n="e" s="T150">omtɨltɨqa </ts>
               <ts e="T152" id="Seg_996" n="e" s="T151">temɨ </ts>
               <ts e="T153" id="Seg_998" n="e" s="T152">sučʼiptɨqa </ts>
               <ts e="T154" id="Seg_1000" n="e" s="T153">((…)) </ts>
               <ts e="T155" id="Seg_1002" n="e" s="T154">pissɔːtɨn. </ts>
               <ts e="T156" id="Seg_1004" n="e" s="T155">Kisʼa </ts>
               <ts e="T157" id="Seg_1006" n="e" s="T156">nɔːt </ts>
               <ts e="T158" id="Seg_1008" n="e" s="T157">qəqɨlsɔːtɨn </ts>
               <ts e="T159" id="Seg_1010" n="e" s="T158">qəqɨlsɔːtɨn. </ts>
               <ts e="T160" id="Seg_1012" n="e" s="T159">Šʼittalʼ </ts>
               <ts e="T161" id="Seg_1014" n="e" s="T160">meː </ts>
               <ts e="T162" id="Seg_1016" n="e" s="T161">nɔːtɨ </ts>
               <ts e="T163" id="Seg_1018" n="e" s="T162">qəqɨlsɔːmɨn. </ts>
               <ts e="T164" id="Seg_1020" n="e" s="T163">Sɨrqɨ </ts>
               <ts e="T165" id="Seg_1022" n="e" s="T164">sajavlenie </ts>
               <ts e="T166" id="Seg_1024" n="e" s="T165">miːmpɨsɨt </ts>
               <ts e="T167" id="Seg_1026" n="e" s="T166">Sɨrqɨ </ts>
               <ts e="T168" id="Seg_1028" n="e" s="T167">(mɔː-) </ts>
               <ts e="T169" id="Seg_1030" n="e" s="T168">mɔːt </ts>
               <ts e="T170" id="Seg_1032" n="e" s="T169">tɨ </ts>
               <ts e="T171" id="Seg_1034" n="e" s="T170">nɨtaŋɨt. </ts>
               <ts e="T172" id="Seg_1036" n="e" s="T171">Sɨrgɨ </ts>
               <ts e="T173" id="Seg_1038" n="e" s="T172">mɔːt </ts>
               <ts e="T174" id="Seg_1040" n="e" s="T173">tɨ </ts>
               <ts e="T175" id="Seg_1042" n="e" s="T174">nɨtaŋɨt </ts>
               <ts e="T176" id="Seg_1044" n="e" s="T175">((…)) </ts>
               <ts e="T177" id="Seg_1046" n="e" s="T176">šʼittɨ </ts>
               <ts e="T178" id="Seg_1048" n="e" s="T177">pɔːrɨ </ts>
               <ts e="T179" id="Seg_1050" n="e" s="T178">pit </ts>
               <ts e="T180" id="Seg_1052" n="e" s="T179">sɨqɨlla </ts>
               <ts e="T181" id="Seg_1054" n="e" s="T180">pin </ts>
               <ts e="T182" id="Seg_1056" n="e" s="T181">Sɨrgɨn </ts>
               <ts e="T183" id="Seg_1058" n="e" s="T182">mɔːn </ts>
               <ts e="T184" id="Seg_1060" n="e" s="T183">qoptɨkɔːl </ts>
               <ts e="T185" id="Seg_1062" n="e" s="T184">tɨ </ts>
               <ts e="T186" id="Seg_1064" n="e" s="T185">nɨtampat. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T66" id="Seg_1065" s="T0">NN2_196X_TrialOfTwoFriends_nar.001 (001)</ta>
            <ta e="T14" id="Seg_1066" s="T66">NN2_196X_TrialOfTwoFriends_nar.002 (002)</ta>
            <ta e="T20" id="Seg_1067" s="T14">NN2_196X_TrialOfTwoFriends_nar.003 (003)</ta>
            <ta e="T29" id="Seg_1068" s="T20">NN2_196X_TrialOfTwoFriends_nar.004 (004)</ta>
            <ta e="T36" id="Seg_1069" s="T29">NN2_196X_TrialOfTwoFriends_nar.005 (005)</ta>
            <ta e="T45" id="Seg_1070" s="T36">NN2_196X_TrialOfTwoFriends_nar.006 (006)</ta>
            <ta e="T49" id="Seg_1071" s="T45">NN2_196X_TrialOfTwoFriends_nar.007 (007)</ta>
            <ta e="T57" id="Seg_1072" s="T49">NN2_196X_TrialOfTwoFriends_nar.008 (008)</ta>
            <ta e="T63" id="Seg_1073" s="T57">NN2_196X_TrialOfTwoFriends_nar.009 (009)</ta>
            <ta e="T69" id="Seg_1074" s="T63">NN2_196X_TrialOfTwoFriends_nar.010 (010)</ta>
            <ta e="T72" id="Seg_1075" s="T69">NN2_196X_TrialOfTwoFriends_nar.011 (011)</ta>
            <ta e="T85" id="Seg_1076" s="T72">NN2_196X_TrialOfTwoFriends_nar.012 (012)</ta>
            <ta e="T98" id="Seg_1077" s="T85">NN2_196X_TrialOfTwoFriends_nar.013 (013)</ta>
            <ta e="T105" id="Seg_1078" s="T98">NN2_196X_TrialOfTwoFriends_nar.014 (014)</ta>
            <ta e="T114" id="Seg_1079" s="T105">NN2_196X_TrialOfTwoFriends_nar.015 (015)</ta>
            <ta e="T128" id="Seg_1080" s="T114">NN2_196X_TrialOfTwoFriends_nar.016 (016)</ta>
            <ta e="T140" id="Seg_1081" s="T128">NN2_196X_TrialOfTwoFriends_nar.017 (017)</ta>
            <ta e="T146" id="Seg_1082" s="T140">NN2_196X_TrialOfTwoFriends_nar.018 (018)</ta>
            <ta e="T155" id="Seg_1083" s="T146">NN2_196X_TrialOfTwoFriends_nar.019 (019)</ta>
            <ta e="T159" id="Seg_1084" s="T155">NN2_196X_TrialOfTwoFriends_nar.020 (020)</ta>
            <ta e="T163" id="Seg_1085" s="T159">NN2_196X_TrialOfTwoFriends_nar.021 (021)</ta>
            <ta e="T171" id="Seg_1086" s="T163">NN2_196X_TrialOfTwoFriends_nar.022 (022)</ta>
            <ta e="T186" id="Seg_1087" s="T171">NN2_196X_TrialOfTwoFriends_nar.023 (023)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="stl" tierref="stl">
            <ta e="T14" id="Seg_1088" s="T66">tap čʼeːlʼ takkɨ… tap čʼeːlʼ takkɨ… takkɨlʼimpɔːtɨn šʼittɨ qumaqı tɨmtɨ sutʼiɣsɔːtɨn kontorɣɨn</ta>
            <ta e="T20" id="Seg_1089" s="T14">Lʼonja aj Petɨja</ta>
            <ta e="T29" id="Seg_1090" s="T20">ukkɨr nʼemtɨ…ukkɨr nʼemtɨ pisɔːtɨn kəš… posjolkaɣan štrafsa pisɔːtɨn</ta>
            <ta e="T36" id="Seg_1091" s="T29">ukkɨr nʼemtɨ qap sučʼip…sučʼipqolapsɔːtɨn</ta>
            <ta e="T45" id="Seg_1092" s="T36">šʼittalä sudja naːte qəqɨlsɔːtɨn šʼittalä nɔːt aša sučʼipqolapsɔːtɨn</ta>
            <ta e="T49" id="Seg_1093" s="T45">tɨtoj počʼot pisɔːtɨn</ta>
            <ta e="T57" id="Seg_1094" s="T49">na tamtɨr čʼap pisɔːtɨn na Petɨja pitpɨj</ta>
            <ta e="T63" id="Seg_1095" s="T57">qara Tarko-Salente wərkɨsɔːtɨn kušak nemɨ ütiqa</ta>
            <ta e="T69" id="Seg_1096" s="T63">šʼittalʼ sudja nɔːtɨ qəɣɨlsoːtɨn</ta>
            <ta e="T72" id="Seg_1097" s="T69">me etɨm (təp ?)</ta>
            <ta e="T85" id="Seg_1098" s="T72">šʼittalʼ me nɨ man me nɨ kəttɨsɔːmɨn lʼa te qoštɨ qumɨlʼa qoštɨ qumɨlʼa</ta>
            <ta e="T98" id="Seg_1099" s="T85">tɨta na qumɨ qaj qa nʼenna ütɨsola tɨ to aj qumɨ na qättɨt</ta>
            <ta e="T105" id="Seg_1100" s="T98">me kɨropo nennımɔːtɨıːsɔːmɨn na Petɨja čʼotɨ</ta>
            <ta e="T114" id="Seg_1101" s="T105">meː nı tenɨrpɔmɨn qara qəntɨqa nada ılla omtɨltɨqa</ta>
            <ta e="T128" id="Seg_1102" s="T114">meː qumɨj tamtɨr nıː pisɨtɨ a sudji aša kɨkala nı pisɔːtɨn šʼittalä</ta>
            <ta e="T140" id="Seg_1103" s="T128">qumɨt qaj mɔːt šʼunčʼa ne mɔːt šʼunčʼa qaj təpɨt ne wenɨpor sučʼɨlejčʼɔːtɨn</ta>
            <ta e="T146" id="Seg_1104" s="T140">qumɨn qotola nenʼimɔːtsɔːtɨn na Petɨlʼa čʼotɨ</ta>
            <ta e="T155" id="Seg_1105" s="T146">etɨp qara ütɨqa ılla omtɨltɨqa temɨ sučʼiptɨqa pisɔːtɨn</ta>
            <ta e="T159" id="Seg_1106" s="T155">kisʼa nɔːt qəɣɨlsɔːtɨn qəɣɨlnɔːt</ta>
            <ta e="T163" id="Seg_1107" s="T159">šʼittalʼ meː nɔːtɨ qəɣɨlsɔːmɨn</ta>
            <ta e="T171" id="Seg_1108" s="T163">Sɨrqɨ sajavlenie miːmpɨsɨt Sɨrqɨ mɔːt tɨnɨtaɣɨt</ta>
            <ta e="T186" id="Seg_1109" s="T171">Sɨrgɨ mɔːt tɨnɨtaɣɨt šʼittɨ poːrɨ pit sɨɣɨla pin Sɨrgɨn mon qoptɨkɔːl tɨ nɨtampat</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T66" id="Seg_1110" s="T0">((KuAI:)) Bolʼšoj. </ta>
            <ta e="T14" id="Seg_1111" s="T66">((NN2:)) Tap čʼeːlʼ (takkɨ-) tap čʼeːlʼ (takkɨ-) taqqɨlıːmpɔːtɨn šʼittɨ qumɔːqı tɨmtɨ (sutʼisɔː-) sutʼisɔːtɨn kontorqɨn. </ta>
            <ta e="T20" id="Seg_1112" s="T14">(Lʼonja aj=) Lʼonja (aj=) aj Petɨja. </ta>
            <ta e="T29" id="Seg_1113" s="T20">(Ukkɨr nʼeːmtɨ=) ukkɨr nʼeːmtɨ pissɔːtɨn (kəš-) posʼolkaqɨn štrafsä pissɔːtɨn. </ta>
            <ta e="T36" id="Seg_1114" s="T29">Ukkɨr nʼeːmtɨ (qap sučʼip-) qap sučʼipqolapsɔːtɨn ((…)). </ta>
            <ta e="T45" id="Seg_1115" s="T36">Šittalä ((sudja naːte …)) qəqɨlsɔːtɨn šittalä nɔːt aša sučʼipqolapsɔːtɨn. </ta>
            <ta e="T49" id="Seg_1116" s="T45">Tɨtoj pot čʼot pissɔːtɨn. </ta>
            <ta e="T57" id="Seg_1117" s="T49">Na čʼap tamtɨr čʼap pissɔːtɨn na Petɨja pitpɨj. </ta>
            <ta e="T63" id="Seg_1118" s="T57">Qara Tarkosalente wərkɨsɔːtɨn kušak ((nemɨ)) ütiqa. </ta>
            <ta e="T69" id="Seg_1119" s="T63">Šʼittalʼ ((…)) ((sudja-)) nɔːtɨ qəqɨlsɔːtɨn. </ta>
            <ta e="T72" id="Seg_1120" s="T69">Me əːtɨm ((…)). </ta>
            <ta e="T85" id="Seg_1121" s="T72">Šʼittalʼ me nɨ mat me nık kətɨsɔːmɨn:“ Lʼa te qoštɨ qumɨlʼa qoštɨ qumɨlʼa. </ta>
            <ta e="T98" id="Seg_1122" s="T85">Tɨta na qumɨ qaj qaː nʼenna üːtɨsɔːlɨt tɨ to aj qumɨ na qättɨt”. </ta>
            <ta e="T105" id="Seg_1123" s="T98">Me ((tı)) kɨropo nennımɔːtıːsɔːmɨn na Peːtɨja čʼɔːtɨ. </ta>
            <ta e="T114" id="Seg_1124" s="T105">Meː nı tɛnɨrpɔːmɨn qara qəntɨqa nada ((…)) ılla omtɨltɨqa. </ta>
            <ta e="T128" id="Seg_1125" s="T114">Meː qumɨj tamtɨr ((…)) nıː pissɨtɨ ((…)) a sudji aša kɨkala (nı pissɔːtɨn) šittalä. </ta>
            <ta e="T140" id="Seg_1126" s="T128">Qumɨt qaj mɔːt šunčʼa ne mɔːt šunčʼa qaj təpɨt ne wenɨpor sučʼɨlejčʼɔːtɨn. </ta>
            <ta e="T146" id="Seg_1127" s="T140">Qumɨn qotola nenʼimɔːtsɔːtɨn na Petɨlʼa čʼɔːtɨ. </ta>
            <ta e="T155" id="Seg_1128" s="T146">Əːtɨp qara üːtɨqa ılla omtɨltɨqa temɨ sučʼiptɨqa ((…))pissɔːtɨn. </ta>
            <ta e="T159" id="Seg_1129" s="T155">Kisʼa nɔːt qəqɨlsɔːtɨn qəqɨlsɔːtɨn. </ta>
            <ta e="T163" id="Seg_1130" s="T159">Šʼittalʼ meː nɔːtɨ qəqɨlsɔːmɨn. </ta>
            <ta e="T171" id="Seg_1131" s="T163">Sɨrqɨ sajavlenie miːmpɨsɨt Sɨrqɨ (mɔː-) mɔːt tɨ nɨtaŋɨt. </ta>
            <ta e="T186" id="Seg_1132" s="T171">Sɨrgɨ mɔːt tɨ nɨtaŋɨt ((…)) šʼittɨ pɔːrɨ pit sɨqɨlla pin Sɨrgɨn mɔːn qoptɨkɔːl tɨ nɨtampat. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_1133" s="T1">tap</ta>
            <ta e="T3" id="Seg_1134" s="T2">čʼeːlʼ</ta>
            <ta e="T5" id="Seg_1135" s="T4">tap</ta>
            <ta e="T6" id="Seg_1136" s="T5">čʼeːlʼ</ta>
            <ta e="T8" id="Seg_1137" s="T7">taq-qɨl-ıː-mpɔː-tɨn</ta>
            <ta e="T9" id="Seg_1138" s="T8">šittɨ</ta>
            <ta e="T10" id="Seg_1139" s="T9">qum-ɔː-qı</ta>
            <ta e="T11" id="Seg_1140" s="T10">tɨmtɨ</ta>
            <ta e="T13" id="Seg_1141" s="T12">sutʼi-sɔː-tɨn</ta>
            <ta e="T14" id="Seg_1142" s="T13">kontor-qɨn</ta>
            <ta e="T15" id="Seg_1143" s="T14">Lʼonja</ta>
            <ta e="T16" id="Seg_1144" s="T15">aj</ta>
            <ta e="T17" id="Seg_1145" s="T16">Lʼonja</ta>
            <ta e="T18" id="Seg_1146" s="T17">aj</ta>
            <ta e="T19" id="Seg_1147" s="T18">aj</ta>
            <ta e="T20" id="Seg_1148" s="T19">Petɨja</ta>
            <ta e="T21" id="Seg_1149" s="T20">ukkɨr</ta>
            <ta e="T22" id="Seg_1150" s="T21">nʼeː-m-tɨ</ta>
            <ta e="T23" id="Seg_1151" s="T22">ukkɨr</ta>
            <ta e="T24" id="Seg_1152" s="T23">nʼeː-m-tɨ</ta>
            <ta e="T25" id="Seg_1153" s="T24">pis-sɔː-tɨn</ta>
            <ta e="T27" id="Seg_1154" s="T26">posʼolka-qɨn</ta>
            <ta e="T28" id="Seg_1155" s="T27">štraf-sä</ta>
            <ta e="T29" id="Seg_1156" s="T28">pis-sɔː-tɨn</ta>
            <ta e="T30" id="Seg_1157" s="T29">ukkɨr</ta>
            <ta e="T31" id="Seg_1158" s="T30">nʼeː-m-tɨ</ta>
            <ta e="T34" id="Seg_1159" s="T33">qap</ta>
            <ta e="T35" id="Seg_1160" s="T34">sučʼip-q-olap-sɔː-tɨn</ta>
            <ta e="T37" id="Seg_1161" s="T36">šittalä</ta>
            <ta e="T41" id="Seg_1162" s="T40">qəqɨl-sɔː-tɨn</ta>
            <ta e="T42" id="Seg_1163" s="T41">šittalä</ta>
            <ta e="T43" id="Seg_1164" s="T42">nɔːt</ta>
            <ta e="T44" id="Seg_1165" s="T43">aša</ta>
            <ta e="T45" id="Seg_1166" s="T44">sučʼip-q-olap-sɔː-tɨn</ta>
            <ta e="T46" id="Seg_1167" s="T45">tɨto-j</ta>
            <ta e="T47" id="Seg_1168" s="T46">po-t</ta>
            <ta e="T48" id="Seg_1169" s="T47">čʼot</ta>
            <ta e="T49" id="Seg_1170" s="T48">pis-sɔː-tɨn</ta>
            <ta e="T50" id="Seg_1171" s="T49">na</ta>
            <ta e="T51" id="Seg_1172" s="T50">čʼap</ta>
            <ta e="T52" id="Seg_1173" s="T51">tamtɨr</ta>
            <ta e="T53" id="Seg_1174" s="T52">čʼap</ta>
            <ta e="T54" id="Seg_1175" s="T53">pis-sɔː-tɨn</ta>
            <ta e="T55" id="Seg_1176" s="T54">na</ta>
            <ta e="T56" id="Seg_1177" s="T55">Petɨja</ta>
            <ta e="T57" id="Seg_1178" s="T56">pitpɨj</ta>
            <ta e="T58" id="Seg_1179" s="T57">qara</ta>
            <ta e="T59" id="Seg_1180" s="T58">Tarkosale-nte</ta>
            <ta e="T60" id="Seg_1181" s="T59">wərkɨ-sɔː-tɨn</ta>
            <ta e="T61" id="Seg_1182" s="T60">kušak</ta>
            <ta e="T63" id="Seg_1183" s="T62">üti-qa</ta>
            <ta e="T64" id="Seg_1184" s="T63">šʼittalʼ</ta>
            <ta e="T68" id="Seg_1185" s="T67">nɔːtɨ</ta>
            <ta e="T69" id="Seg_1186" s="T68">qəqɨl-sɔː-tɨn</ta>
            <ta e="T70" id="Seg_1187" s="T69">me</ta>
            <ta e="T71" id="Seg_1188" s="T70">əːtɨ-m</ta>
            <ta e="T73" id="Seg_1189" s="T72">šʼittalʼ</ta>
            <ta e="T74" id="Seg_1190" s="T73">me</ta>
            <ta e="T75" id="Seg_1191" s="T74">nı</ta>
            <ta e="T76" id="Seg_1192" s="T75">mat</ta>
            <ta e="T77" id="Seg_1193" s="T76">me</ta>
            <ta e="T78" id="Seg_1194" s="T77">nık</ta>
            <ta e="T79" id="Seg_1195" s="T78">kətɨ-sɔː-mɨn</ta>
            <ta e="T80" id="Seg_1196" s="T79">lʼa</ta>
            <ta e="T81" id="Seg_1197" s="T80">te</ta>
            <ta e="T82" id="Seg_1198" s="T81">qoštɨ</ta>
            <ta e="T83" id="Seg_1199" s="T82">qum-ɨ-lʼa</ta>
            <ta e="T84" id="Seg_1200" s="T83">qoštɨ</ta>
            <ta e="T85" id="Seg_1201" s="T84">qum-ɨ-lʼa</ta>
            <ta e="T86" id="Seg_1202" s="T85">tɨta</ta>
            <ta e="T87" id="Seg_1203" s="T86">na</ta>
            <ta e="T88" id="Seg_1204" s="T87">qum-ɨ</ta>
            <ta e="T89" id="Seg_1205" s="T88">qaj</ta>
            <ta e="T90" id="Seg_1206" s="T89">qaː</ta>
            <ta e="T91" id="Seg_1207" s="T90">nʼenna</ta>
            <ta e="T92" id="Seg_1208" s="T91">üːtɨ-sɔː-lɨt</ta>
            <ta e="T93" id="Seg_1209" s="T92">tɨ</ta>
            <ta e="T94" id="Seg_1210" s="T93">to</ta>
            <ta e="T95" id="Seg_1211" s="T94">aj</ta>
            <ta e="T96" id="Seg_1212" s="T95">qum-ɨ</ta>
            <ta e="T97" id="Seg_1213" s="T96">na</ta>
            <ta e="T98" id="Seg_1214" s="T97">qättɨ-t</ta>
            <ta e="T99" id="Seg_1215" s="T98">me</ta>
            <ta e="T101" id="Seg_1216" s="T100">kɨropo</ta>
            <ta e="T102" id="Seg_1217" s="T101">nennı-mɔːt-ıː-sɔː-mɨn</ta>
            <ta e="T103" id="Seg_1218" s="T102">na</ta>
            <ta e="T104" id="Seg_1219" s="T103">Peːtɨja</ta>
            <ta e="T105" id="Seg_1220" s="T104">čʼɔːtɨ</ta>
            <ta e="T106" id="Seg_1221" s="T105">meː</ta>
            <ta e="T107" id="Seg_1222" s="T106">nı</ta>
            <ta e="T108" id="Seg_1223" s="T107">tɛnɨ-r-pɔː-mɨn</ta>
            <ta e="T109" id="Seg_1224" s="T108">qara</ta>
            <ta e="T110" id="Seg_1225" s="T109">qən-tɨ-qa</ta>
            <ta e="T111" id="Seg_1226" s="T110">nada</ta>
            <ta e="T113" id="Seg_1227" s="T112">ılla</ta>
            <ta e="T114" id="Seg_1228" s="T113">omtɨ-ltɨ-qa</ta>
            <ta e="T115" id="Seg_1229" s="T114">meː</ta>
            <ta e="T116" id="Seg_1230" s="T115">qum-ɨ-j</ta>
            <ta e="T117" id="Seg_1231" s="T116">tamtɨr</ta>
            <ta e="T119" id="Seg_1232" s="T118">nıː</ta>
            <ta e="T120" id="Seg_1233" s="T119">pis-sɨ-tɨ</ta>
            <ta e="T122" id="Seg_1234" s="T121">a</ta>
            <ta e="T124" id="Seg_1235" s="T123">aša</ta>
            <ta e="T125" id="Seg_1236" s="T124">kɨka-la</ta>
            <ta e="T126" id="Seg_1237" s="T125">nı</ta>
            <ta e="T127" id="Seg_1238" s="T126">pis-sɔː-tɨn</ta>
            <ta e="T128" id="Seg_1239" s="T127">šittalä</ta>
            <ta e="T129" id="Seg_1240" s="T128">qum-ɨ-t</ta>
            <ta e="T130" id="Seg_1241" s="T129">qaj</ta>
            <ta e="T131" id="Seg_1242" s="T130">mɔːt</ta>
            <ta e="T132" id="Seg_1243" s="T131">šunčʼa</ta>
            <ta e="T133" id="Seg_1244" s="T132">ne</ta>
            <ta e="T134" id="Seg_1245" s="T133">mɔːt</ta>
            <ta e="T135" id="Seg_1246" s="T134">šunčʼa</ta>
            <ta e="T136" id="Seg_1247" s="T135">qaj</ta>
            <ta e="T137" id="Seg_1248" s="T136">təp-ɨ-t</ta>
            <ta e="T138" id="Seg_1249" s="T137">ne</ta>
            <ta e="T139" id="Seg_1250" s="T138">wenɨpor</ta>
            <ta e="T140" id="Seg_1251" s="T139">sučʼɨ-l-ej-čʼɔː-tɨn</ta>
            <ta e="T141" id="Seg_1252" s="T140">qum-ɨ-n</ta>
            <ta e="T142" id="Seg_1253" s="T141">qotola</ta>
            <ta e="T143" id="Seg_1254" s="T142">nenʼi-mɔːt-sɔː-tɨn</ta>
            <ta e="T144" id="Seg_1255" s="T143">na</ta>
            <ta e="T145" id="Seg_1256" s="T144">Petɨlʼa</ta>
            <ta e="T146" id="Seg_1257" s="T145">čʼɔːtɨ</ta>
            <ta e="T147" id="Seg_1258" s="T146">əːtɨ-p</ta>
            <ta e="T148" id="Seg_1259" s="T147">qara</ta>
            <ta e="T149" id="Seg_1260" s="T148">üːtɨ-qa</ta>
            <ta e="T150" id="Seg_1261" s="T149">ılla</ta>
            <ta e="T151" id="Seg_1262" s="T150">omtɨ-ltɨ-qa</ta>
            <ta e="T152" id="Seg_1263" s="T151">temɨ</ta>
            <ta e="T153" id="Seg_1264" s="T152">sučʼi-ptɨ-qa</ta>
            <ta e="T155" id="Seg_1265" s="T154">pis-sɔː-tɨn</ta>
            <ta e="T156" id="Seg_1266" s="T155">kisʼa</ta>
            <ta e="T157" id="Seg_1267" s="T156">nɔːt</ta>
            <ta e="T158" id="Seg_1268" s="T157">qəqɨl-sɔː-tɨn</ta>
            <ta e="T159" id="Seg_1269" s="T158">qəqɨl-sɔː-tɨn</ta>
            <ta e="T160" id="Seg_1270" s="T159">šʼittalʼ</ta>
            <ta e="T161" id="Seg_1271" s="T160">meː</ta>
            <ta e="T162" id="Seg_1272" s="T161">nɔːtɨ</ta>
            <ta e="T163" id="Seg_1273" s="T162">qəqɨl-sɔː-mɨn</ta>
            <ta e="T164" id="Seg_1274" s="T163">Sɨrqɨ</ta>
            <ta e="T165" id="Seg_1275" s="T164">sajavlenie</ta>
            <ta e="T166" id="Seg_1276" s="T165">miː-mpɨ-sɨ-t</ta>
            <ta e="T167" id="Seg_1277" s="T166">Sɨrqɨ</ta>
            <ta e="T169" id="Seg_1278" s="T168">mɔːt</ta>
            <ta e="T170" id="Seg_1279" s="T169">tɨ</ta>
            <ta e="T171" id="Seg_1280" s="T170">nɨta-ŋɨ-t</ta>
            <ta e="T172" id="Seg_1281" s="T171">Sɨrgɨ</ta>
            <ta e="T173" id="Seg_1282" s="T172">mɔːt</ta>
            <ta e="T174" id="Seg_1283" s="T173">tɨ</ta>
            <ta e="T175" id="Seg_1284" s="T174">nɨta-ŋɨ-t</ta>
            <ta e="T177" id="Seg_1285" s="T176">šittɨ</ta>
            <ta e="T178" id="Seg_1286" s="T177">pɔːrɨ</ta>
            <ta e="T179" id="Seg_1287" s="T178">pi-t</ta>
            <ta e="T180" id="Seg_1288" s="T179">sɨqɨl-la</ta>
            <ta e="T181" id="Seg_1289" s="T180">pi-n</ta>
            <ta e="T182" id="Seg_1290" s="T181">Sɨrgɨ-n</ta>
            <ta e="T183" id="Seg_1291" s="T182">mɔːn</ta>
            <ta e="T184" id="Seg_1292" s="T183">qoptɨkɔːl</ta>
            <ta e="T185" id="Seg_1293" s="T184">tɨ</ta>
            <ta e="T186" id="Seg_1294" s="T185">nɨta-mpa-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1295" s="T1">tam</ta>
            <ta e="T3" id="Seg_1296" s="T2">čʼeːlɨ</ta>
            <ta e="T5" id="Seg_1297" s="T4">tam</ta>
            <ta e="T6" id="Seg_1298" s="T5">čʼeːlɨ</ta>
            <ta e="T8" id="Seg_1299" s="T7">*taqɨ-qɨl-ıː-mpɨ-tɨt</ta>
            <ta e="T9" id="Seg_1300" s="T8">šittɨ</ta>
            <ta e="T10" id="Seg_1301" s="T9">qum-ɨ-qı</ta>
            <ta e="T11" id="Seg_1302" s="T10">tɨmtɨ</ta>
            <ta e="T13" id="Seg_1303" s="T12">sudʼin-sɨ-tɨt</ta>
            <ta e="T14" id="Seg_1304" s="T13">kontor-qɨn</ta>
            <ta e="T15" id="Seg_1305" s="T14">Lʼonja</ta>
            <ta e="T16" id="Seg_1306" s="T15">aj</ta>
            <ta e="T17" id="Seg_1307" s="T16">Lʼonja</ta>
            <ta e="T18" id="Seg_1308" s="T17">aj</ta>
            <ta e="T19" id="Seg_1309" s="T18">aj</ta>
            <ta e="T20" id="Seg_1310" s="T19">Petɨja</ta>
            <ta e="T21" id="Seg_1311" s="T20">ukkɨr</ta>
            <ta e="T22" id="Seg_1312" s="T21">nʼeː-m-tɨ</ta>
            <ta e="T23" id="Seg_1313" s="T22">ukkɨr</ta>
            <ta e="T24" id="Seg_1314" s="T23">nʼeː-m-tɨ</ta>
            <ta e="T25" id="Seg_1315" s="T24">pin-sɨ-tɨt</ta>
            <ta e="T27" id="Seg_1316" s="T26">posʼolka-qɨn</ta>
            <ta e="T28" id="Seg_1317" s="T27">štraf-sä</ta>
            <ta e="T29" id="Seg_1318" s="T28">pin-sɨ-tɨt</ta>
            <ta e="T30" id="Seg_1319" s="T29">ukkɨr</ta>
            <ta e="T31" id="Seg_1320" s="T30">nʼeː-m-tɨ</ta>
            <ta e="T34" id="Seg_1321" s="T33">qapı</ta>
            <ta e="T35" id="Seg_1322" s="T34">sudʼin-qo-olam-sɨ-tɨt</ta>
            <ta e="T37" id="Seg_1323" s="T36">šittälʼ</ta>
            <ta e="T41" id="Seg_1324" s="T40">qɨːqɨl-sɨ-tɨt</ta>
            <ta e="T42" id="Seg_1325" s="T41">šittälʼ</ta>
            <ta e="T43" id="Seg_1326" s="T42">nɔːtɨ</ta>
            <ta e="T44" id="Seg_1327" s="T43">ašša</ta>
            <ta e="T45" id="Seg_1328" s="T44">sudʼin-qo-olam-sɨ-tɨt</ta>
            <ta e="T46" id="Seg_1329" s="T45">tɨto-lʼ</ta>
            <ta e="T47" id="Seg_1330" s="T46">poː-n</ta>
            <ta e="T48" id="Seg_1331" s="T47">čʼɔːtɨ</ta>
            <ta e="T49" id="Seg_1332" s="T48">pin-sɨ-tɨt</ta>
            <ta e="T50" id="Seg_1333" s="T49">na</ta>
            <ta e="T51" id="Seg_1334" s="T50">čʼam</ta>
            <ta e="T52" id="Seg_1335" s="T51">tamtɨr</ta>
            <ta e="T53" id="Seg_1336" s="T52">čʼam</ta>
            <ta e="T54" id="Seg_1337" s="T53">pin-sɨ-tɨt</ta>
            <ta e="T55" id="Seg_1338" s="T54">na</ta>
            <ta e="T56" id="Seg_1339" s="T55">Petɨja</ta>
            <ta e="T57" id="Seg_1340" s="T56">pitpɨj</ta>
            <ta e="T58" id="Seg_1341" s="T57">qara</ta>
            <ta e="T59" id="Seg_1342" s="T58">Tarkosale-ntɨ</ta>
            <ta e="T60" id="Seg_1343" s="T59">wərkɨ-sɨ-tɨt</ta>
            <ta e="T61" id="Seg_1344" s="T60">kuššat</ta>
            <ta e="T63" id="Seg_1345" s="T62">üːtɨ-qo</ta>
            <ta e="T64" id="Seg_1346" s="T63">šittälʼ</ta>
            <ta e="T68" id="Seg_1347" s="T67">nɔːtɨ</ta>
            <ta e="T69" id="Seg_1348" s="T68">qɨːqɨl-sɨ-tɨt</ta>
            <ta e="T70" id="Seg_1349" s="T69">meː</ta>
            <ta e="T71" id="Seg_1350" s="T70">əːtɨ-m</ta>
            <ta e="T73" id="Seg_1351" s="T72">šittälʼ</ta>
            <ta e="T74" id="Seg_1352" s="T73">meː</ta>
            <ta e="T75" id="Seg_1353" s="T74">nılʼčʼɨ</ta>
            <ta e="T76" id="Seg_1354" s="T75">man</ta>
            <ta e="T77" id="Seg_1355" s="T76">meː</ta>
            <ta e="T78" id="Seg_1356" s="T77">nık</ta>
            <ta e="T79" id="Seg_1357" s="T78">kətɨ-sɨ-mɨt</ta>
            <ta e="T80" id="Seg_1358" s="T79">lʼa</ta>
            <ta e="T81" id="Seg_1359" s="T80">təp</ta>
            <ta e="T82" id="Seg_1360" s="T81">qoštɨ</ta>
            <ta e="T83" id="Seg_1361" s="T82">qum-ɨ-lʼa</ta>
            <ta e="T84" id="Seg_1362" s="T83">qoštɨ</ta>
            <ta e="T85" id="Seg_1363" s="T84">qum-ɨ-lʼa</ta>
            <ta e="T86" id="Seg_1364" s="T85">tɨta</ta>
            <ta e="T87" id="Seg_1365" s="T86">na</ta>
            <ta e="T88" id="Seg_1366" s="T87">qum-ɨ</ta>
            <ta e="T89" id="Seg_1367" s="T88">qaj</ta>
            <ta e="T90" id="Seg_1368" s="T89">qaː</ta>
            <ta e="T91" id="Seg_1369" s="T90">nʼennä</ta>
            <ta e="T92" id="Seg_1370" s="T91">üːtɨ-sɨ-lɨt</ta>
            <ta e="T93" id="Seg_1371" s="T92">tıː</ta>
            <ta e="T94" id="Seg_1372" s="T93">to</ta>
            <ta e="T95" id="Seg_1373" s="T94">aj</ta>
            <ta e="T96" id="Seg_1374" s="T95">qum-ɨ</ta>
            <ta e="T97" id="Seg_1375" s="T96">na</ta>
            <ta e="T98" id="Seg_1376" s="T97">qättɨ-tɨ</ta>
            <ta e="T99" id="Seg_1377" s="T98">meː</ta>
            <ta e="T101" id="Seg_1378" s="T100">kɨropo</ta>
            <ta e="T102" id="Seg_1379" s="T101">nʼenʼnʼɨ-mɔːt-ıː-sɨ-mɨt</ta>
            <ta e="T103" id="Seg_1380" s="T102">na</ta>
            <ta e="T104" id="Seg_1381" s="T103">Petɨja</ta>
            <ta e="T105" id="Seg_1382" s="T104">čʼɔːtɨ</ta>
            <ta e="T106" id="Seg_1383" s="T105">meː</ta>
            <ta e="T107" id="Seg_1384" s="T106">nık</ta>
            <ta e="T108" id="Seg_1385" s="T107">tɛnɨ-r-mpɨ-mɨt</ta>
            <ta e="T109" id="Seg_1386" s="T108">karrä</ta>
            <ta e="T110" id="Seg_1387" s="T109">qən-tɨ-qo</ta>
            <ta e="T111" id="Seg_1388" s="T110">naːda</ta>
            <ta e="T113" id="Seg_1389" s="T112">ıllä</ta>
            <ta e="T114" id="Seg_1390" s="T113">omtɨ-ltɨ-qo</ta>
            <ta e="T115" id="Seg_1391" s="T114">meː</ta>
            <ta e="T116" id="Seg_1392" s="T115">qum-ɨ-lʼ</ta>
            <ta e="T117" id="Seg_1393" s="T116">tamtɨr</ta>
            <ta e="T119" id="Seg_1394" s="T118">nık</ta>
            <ta e="T120" id="Seg_1395" s="T119">pin-sɨ-tɨ</ta>
            <ta e="T122" id="Seg_1396" s="T121">a</ta>
            <ta e="T124" id="Seg_1397" s="T123">ašša</ta>
            <ta e="T125" id="Seg_1398" s="T124">kɨkɨ-lä</ta>
            <ta e="T126" id="Seg_1399" s="T125">nık</ta>
            <ta e="T127" id="Seg_1400" s="T126">pin-sɨ-tɨt</ta>
            <ta e="T128" id="Seg_1401" s="T127">šittälʼ</ta>
            <ta e="T129" id="Seg_1402" s="T128">qum-ɨ-t</ta>
            <ta e="T130" id="Seg_1403" s="T129">qaj</ta>
            <ta e="T131" id="Seg_1404" s="T130">mɔːt</ta>
            <ta e="T132" id="Seg_1405" s="T131">šünʼčʼɨ</ta>
            <ta e="T133" id="Seg_1406" s="T132">ne</ta>
            <ta e="T134" id="Seg_1407" s="T133">mɔːt</ta>
            <ta e="T135" id="Seg_1408" s="T134">šünʼčʼɨ</ta>
            <ta e="T136" id="Seg_1409" s="T135">qaj</ta>
            <ta e="T137" id="Seg_1410" s="T136">təp-ɨ-n</ta>
            <ta e="T138" id="Seg_1411" s="T137">ne</ta>
            <ta e="T139" id="Seg_1412" s="T138">wenɨpor</ta>
            <ta e="T140" id="Seg_1413" s="T139">sudʼin-lɨ-ɛː-ntɨ-tɨt</ta>
            <ta e="T141" id="Seg_1414" s="T140">qum-ɨ-t</ta>
            <ta e="T142" id="Seg_1415" s="T141">qɔːtolä</ta>
            <ta e="T143" id="Seg_1416" s="T142">nʼenʼnʼɨ-mɔːt-sɨ-tɨt</ta>
            <ta e="T144" id="Seg_1417" s="T143">na</ta>
            <ta e="T145" id="Seg_1418" s="T144">Petɨja</ta>
            <ta e="T146" id="Seg_1419" s="T145">čʼɔːtɨ</ta>
            <ta e="T147" id="Seg_1420" s="T146">əːtɨ-m</ta>
            <ta e="T148" id="Seg_1421" s="T147">karrä</ta>
            <ta e="T149" id="Seg_1422" s="T148">üːtɨ-qo</ta>
            <ta e="T150" id="Seg_1423" s="T149">ıllä</ta>
            <ta e="T151" id="Seg_1424" s="T150">omtɨ-ltɨ-qo</ta>
            <ta e="T152" id="Seg_1425" s="T151">temɨ</ta>
            <ta e="T153" id="Seg_1426" s="T152">sudʼin-ptɨ-qo</ta>
            <ta e="T155" id="Seg_1427" s="T154">pin-sɨ-tɨt</ta>
            <ta e="T156" id="Seg_1428" s="T155">kɨssa</ta>
            <ta e="T157" id="Seg_1429" s="T156">nɔːtɨ</ta>
            <ta e="T158" id="Seg_1430" s="T157">qɨːqɨl-sɨ-tɨt</ta>
            <ta e="T159" id="Seg_1431" s="T158">qɨːqɨl-sɨ-tɨt</ta>
            <ta e="T160" id="Seg_1432" s="T159">šittälʼ</ta>
            <ta e="T161" id="Seg_1433" s="T160">meː</ta>
            <ta e="T162" id="Seg_1434" s="T161">nɔːtɨ</ta>
            <ta e="T163" id="Seg_1435" s="T162">qɨːqɨl-sɨ-mɨt</ta>
            <ta e="T164" id="Seg_1436" s="T163">Sɨrgɨ</ta>
            <ta e="T165" id="Seg_1437" s="T164">sajavlenie</ta>
            <ta e="T166" id="Seg_1438" s="T165">mi-mpɨ-sɨ-tɨ</ta>
            <ta e="T167" id="Seg_1439" s="T166">Sɨrgɨ</ta>
            <ta e="T169" id="Seg_1440" s="T168">mɔːt</ta>
            <ta e="T170" id="Seg_1441" s="T169">tɨ</ta>
            <ta e="T171" id="Seg_1442" s="T170">nɨta-ŋɨ-tɨ</ta>
            <ta e="T172" id="Seg_1443" s="T171">Sɨrgɨ</ta>
            <ta e="T173" id="Seg_1444" s="T172">mɔːt</ta>
            <ta e="T174" id="Seg_1445" s="T173">tɨ</ta>
            <ta e="T175" id="Seg_1446" s="T174">nɨta-ŋɨ-tɨ</ta>
            <ta e="T177" id="Seg_1447" s="T176">šittɨ</ta>
            <ta e="T178" id="Seg_1448" s="T177">pɔːrɨ</ta>
            <ta e="T179" id="Seg_1449" s="T178">pi-n</ta>
            <ta e="T180" id="Seg_1450" s="T179">sɨːqɨl-lä</ta>
            <ta e="T181" id="Seg_1451" s="T180">pi-n</ta>
            <ta e="T182" id="Seg_1452" s="T181">Sɨrgɨ-n</ta>
            <ta e="T183" id="Seg_1453" s="T182">mɔːt</ta>
            <ta e="T184" id="Seg_1454" s="T183">qoptɨkɔːl</ta>
            <ta e="T185" id="Seg_1455" s="T184">tɨ</ta>
            <ta e="T186" id="Seg_1456" s="T185">nɨta-mpɨ-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1457" s="T1">this</ta>
            <ta e="T3" id="Seg_1458" s="T2">day.[NOM]</ta>
            <ta e="T5" id="Seg_1459" s="T4">this</ta>
            <ta e="T6" id="Seg_1460" s="T5">day.[NOM]</ta>
            <ta e="T8" id="Seg_1461" s="T7">gather-MULO-RFL.PFV-DUR-3PL</ta>
            <ta e="T9" id="Seg_1462" s="T8">two</ta>
            <ta e="T10" id="Seg_1463" s="T9">human.being-EP-DU.[NOM]</ta>
            <ta e="T11" id="Seg_1464" s="T10">here</ta>
            <ta e="T13" id="Seg_1465" s="T12">judge-PST-3PL</ta>
            <ta e="T14" id="Seg_1466" s="T13">bureau-LOC</ta>
            <ta e="T15" id="Seg_1467" s="T14">Lyonya.[NOM]</ta>
            <ta e="T16" id="Seg_1468" s="T15">and</ta>
            <ta e="T17" id="Seg_1469" s="T16">Lyonya.[NOM]</ta>
            <ta e="T18" id="Seg_1470" s="T17">and</ta>
            <ta e="T19" id="Seg_1471" s="T18">and</ta>
            <ta e="T20" id="Seg_1472" s="T19">Nikolaj?.[NOM]</ta>
            <ta e="T21" id="Seg_1473" s="T20">one</ta>
            <ta e="T22" id="Seg_1474" s="T21">somebody-ACC-3SG</ta>
            <ta e="T23" id="Seg_1475" s="T22">one</ta>
            <ta e="T24" id="Seg_1476" s="T23">somebody-ACC-3SG</ta>
            <ta e="T25" id="Seg_1477" s="T24">put-PST-3PL</ta>
            <ta e="T27" id="Seg_1478" s="T26">village-LOC</ta>
            <ta e="T28" id="Seg_1479" s="T27">fine-INSTR</ta>
            <ta e="T29" id="Seg_1480" s="T28">put-PST-3PL</ta>
            <ta e="T30" id="Seg_1481" s="T29">one</ta>
            <ta e="T31" id="Seg_1482" s="T30">somebody-ACC-3SG</ta>
            <ta e="T34" id="Seg_1483" s="T33">supposedly</ta>
            <ta e="T35" id="Seg_1484" s="T34">judge-INF-be.going.to-PST-3PL</ta>
            <ta e="T37" id="Seg_1485" s="T36">then</ta>
            <ta e="T41" id="Seg_1486" s="T40">finish-PST-3PL</ta>
            <ta e="T42" id="Seg_1487" s="T41">then</ta>
            <ta e="T43" id="Seg_1488" s="T42">then</ta>
            <ta e="T44" id="Seg_1489" s="T43">NEG</ta>
            <ta e="T45" id="Seg_1490" s="T44">judge-INF-be.going.to-PST-3PL</ta>
            <ta e="T46" id="Seg_1491" s="T45">%%-ADJZ</ta>
            <ta e="T47" id="Seg_1492" s="T46">year-GEN</ta>
            <ta e="T48" id="Seg_1493" s="T47">for</ta>
            <ta e="T49" id="Seg_1494" s="T48">put-PST-3PL</ta>
            <ta e="T50" id="Seg_1495" s="T49">here</ta>
            <ta e="T51" id="Seg_1496" s="T50">only</ta>
            <ta e="T52" id="Seg_1497" s="T51">clan.[NOM]</ta>
            <ta e="T53" id="Seg_1498" s="T52">only</ta>
            <ta e="T54" id="Seg_1499" s="T53">put-PST-3PL</ta>
            <ta e="T55" id="Seg_1500" s="T54">here</ta>
            <ta e="T56" id="Seg_1501" s="T55">Nikolaj?.[NOM]</ta>
            <ta e="T57" id="Seg_1502" s="T56">%%</ta>
            <ta e="T58" id="Seg_1503" s="T57">%%</ta>
            <ta e="T59" id="Seg_1504" s="T58">Tarko_Sale-ILL</ta>
            <ta e="T60" id="Seg_1505" s="T59">be.situated-PST-3PL</ta>
            <ta e="T61" id="Seg_1506" s="T60">when</ta>
            <ta e="T63" id="Seg_1507" s="T62">let.go-INF</ta>
            <ta e="T64" id="Seg_1508" s="T63">then</ta>
            <ta e="T68" id="Seg_1509" s="T67">then</ta>
            <ta e="T69" id="Seg_1510" s="T68">finish-PST-3PL</ta>
            <ta e="T70" id="Seg_1511" s="T69">we.PL.GEN</ta>
            <ta e="T71" id="Seg_1512" s="T70">word-ACC</ta>
            <ta e="T73" id="Seg_1513" s="T72">then</ta>
            <ta e="T74" id="Seg_1514" s="T73">we.PL.NOM</ta>
            <ta e="T75" id="Seg_1515" s="T74">such</ta>
            <ta e="T76" id="Seg_1516" s="T75">I.NOM</ta>
            <ta e="T77" id="Seg_1517" s="T76">we.PL.NOM</ta>
            <ta e="T78" id="Seg_1518" s="T77">so</ta>
            <ta e="T79" id="Seg_1519" s="T78">say-PST-1PL</ta>
            <ta e="T80" id="Seg_1520" s="T79">hey</ta>
            <ta e="T81" id="Seg_1521" s="T80">(s)he</ta>
            <ta e="T82" id="Seg_1522" s="T81">bad</ta>
            <ta e="T83" id="Seg_1523" s="T82">human.being-EP-DIM.[NOM]</ta>
            <ta e="T84" id="Seg_1524" s="T83">bad</ta>
            <ta e="T85" id="Seg_1525" s="T84">human.being-EP-DIM.[NOM]</ta>
            <ta e="T86" id="Seg_1526" s="T85">%%</ta>
            <ta e="T87" id="Seg_1527" s="T86">this</ta>
            <ta e="T88" id="Seg_1528" s="T87">human.being-EP.[NOM]</ta>
            <ta e="T89" id="Seg_1529" s="T88">what.[NOM]</ta>
            <ta e="T90" id="Seg_1530" s="T89">what.for</ta>
            <ta e="T91" id="Seg_1531" s="T90">forward</ta>
            <ta e="T92" id="Seg_1532" s="T91">sent-PST-2PL</ta>
            <ta e="T93" id="Seg_1533" s="T92">now</ta>
            <ta e="T94" id="Seg_1534" s="T93">that</ta>
            <ta e="T95" id="Seg_1535" s="T94">again</ta>
            <ta e="T96" id="Seg_1536" s="T95">human.being-EP.[NOM]</ta>
            <ta e="T97" id="Seg_1537" s="T96">here</ta>
            <ta e="T98" id="Seg_1538" s="T97">hit-3SG.O</ta>
            <ta e="T99" id="Seg_1539" s="T98">we.PL.NOM</ta>
            <ta e="T101" id="Seg_1540" s="T100">%%</ta>
            <ta e="T102" id="Seg_1541" s="T101">get.angry-DRV-RFL.PFV-PST-1PL</ta>
            <ta e="T103" id="Seg_1542" s="T102">this</ta>
            <ta e="T104" id="Seg_1543" s="T103">Nikolaj</ta>
            <ta e="T105" id="Seg_1544" s="T104">about</ta>
            <ta e="T106" id="Seg_1545" s="T105">we.PL.NOM</ta>
            <ta e="T107" id="Seg_1546" s="T106">so</ta>
            <ta e="T108" id="Seg_1547" s="T107">think-FRQ-DUR-1PL</ta>
            <ta e="T109" id="Seg_1548" s="T108">down</ta>
            <ta e="T110" id="Seg_1549" s="T109">go.away-TR-INF</ta>
            <ta e="T111" id="Seg_1550" s="T110">one.should</ta>
            <ta e="T113" id="Seg_1551" s="T112">down</ta>
            <ta e="T114" id="Seg_1552" s="T113">sit.down-TR-INF</ta>
            <ta e="T115" id="Seg_1553" s="T114">we.PL.NOM</ta>
            <ta e="T116" id="Seg_1554" s="T115">human.being-EP-ADJZ</ta>
            <ta e="T117" id="Seg_1555" s="T116">clan.[NOM]</ta>
            <ta e="T119" id="Seg_1556" s="T118">so</ta>
            <ta e="T120" id="Seg_1557" s="T119">put-PST-3SG.O</ta>
            <ta e="T122" id="Seg_1558" s="T121">but</ta>
            <ta e="T124" id="Seg_1559" s="T123">NEG</ta>
            <ta e="T125" id="Seg_1560" s="T124">want-CVB</ta>
            <ta e="T126" id="Seg_1561" s="T125">so</ta>
            <ta e="T127" id="Seg_1562" s="T126">put-PST-3PL</ta>
            <ta e="T128" id="Seg_1563" s="T127">then</ta>
            <ta e="T129" id="Seg_1564" s="T128">human.being-EP-PL.[NOM]</ta>
            <ta e="T130" id="Seg_1565" s="T129">what.[NOM]</ta>
            <ta e="T131" id="Seg_1566" s="T130">house.[NOM]</ta>
            <ta e="T132" id="Seg_1567" s="T131">inside</ta>
            <ta e="T133" id="Seg_1568" s="T132">%%</ta>
            <ta e="T134" id="Seg_1569" s="T133">house.[NOM]</ta>
            <ta e="T135" id="Seg_1570" s="T134">inside</ta>
            <ta e="T136" id="Seg_1571" s="T135">what.[NOM]</ta>
            <ta e="T137" id="Seg_1572" s="T136">(s)he-EP-GEN</ta>
            <ta e="T138" id="Seg_1573" s="T137">%%</ta>
            <ta e="T139" id="Seg_1574" s="T138">%%</ta>
            <ta e="T140" id="Seg_1575" s="T139">judge-RES-PFV-INFER-3PL</ta>
            <ta e="T141" id="Seg_1576" s="T140">human.being-EP-PL.[NOM]</ta>
            <ta e="T142" id="Seg_1577" s="T141">strongly</ta>
            <ta e="T143" id="Seg_1578" s="T142">get.angry-DRV-PST-3PL</ta>
            <ta e="T144" id="Seg_1579" s="T143">this</ta>
            <ta e="T145" id="Seg_1580" s="T144">Nikolaj?.[NOM]</ta>
            <ta e="T146" id="Seg_1581" s="T145">for</ta>
            <ta e="T147" id="Seg_1582" s="T146">word-ACC</ta>
            <ta e="T148" id="Seg_1583" s="T147">down</ta>
            <ta e="T149" id="Seg_1584" s="T148">sent-INF</ta>
            <ta e="T150" id="Seg_1585" s="T149">down</ta>
            <ta e="T151" id="Seg_1586" s="T150">sit.down-TR-INF</ta>
            <ta e="T152" id="Seg_1587" s="T151">%%</ta>
            <ta e="T153" id="Seg_1588" s="T152">judge-CAUS-INF</ta>
            <ta e="T155" id="Seg_1589" s="T154">put-PST-3PL</ta>
            <ta e="T156" id="Seg_1590" s="T155">come.on</ta>
            <ta e="T157" id="Seg_1591" s="T156">then</ta>
            <ta e="T158" id="Seg_1592" s="T157">finish-PST-3PL</ta>
            <ta e="T159" id="Seg_1593" s="T158">finish-PST-3PL</ta>
            <ta e="T160" id="Seg_1594" s="T159">then</ta>
            <ta e="T161" id="Seg_1595" s="T160">we.PL.NOM</ta>
            <ta e="T162" id="Seg_1596" s="T161">then</ta>
            <ta e="T163" id="Seg_1597" s="T162">finish-PST-1PL</ta>
            <ta e="T164" id="Seg_1598" s="T163">Sergej.[NOM]</ta>
            <ta e="T165" id="Seg_1599" s="T164">request.[NOM]</ta>
            <ta e="T166" id="Seg_1600" s="T165">give-DUR-PST-3SG.O</ta>
            <ta e="T167" id="Seg_1601" s="T166">Sergej.[NOM]</ta>
            <ta e="T169" id="Seg_1602" s="T168">house.[NOM]</ta>
            <ta e="T170" id="Seg_1603" s="T169">%%</ta>
            <ta e="T171" id="Seg_1604" s="T170">tear-CO-3SG.O</ta>
            <ta e="T172" id="Seg_1605" s="T171">Sergej.[NOM]</ta>
            <ta e="T173" id="Seg_1606" s="T172">house.[NOM]</ta>
            <ta e="T174" id="Seg_1607" s="T173">%%</ta>
            <ta e="T175" id="Seg_1608" s="T174">tear-CO-3SG.O</ta>
            <ta e="T177" id="Seg_1609" s="T176">two</ta>
            <ta e="T178" id="Seg_1610" s="T177">time.[NOM]</ta>
            <ta e="T179" id="Seg_1611" s="T178">night-ADV.LOC</ta>
            <ta e="T180" id="Seg_1612" s="T179">get.into-CVB</ta>
            <ta e="T181" id="Seg_1613" s="T180">night-ADV.LOC</ta>
            <ta e="T182" id="Seg_1614" s="T181">Sergej-GEN</ta>
            <ta e="T183" id="Seg_1615" s="T182">house.[NOM]</ta>
            <ta e="T184" id="Seg_1616" s="T183">%%</ta>
            <ta e="T185" id="Seg_1617" s="T184">%%</ta>
            <ta e="T186" id="Seg_1618" s="T185">tear-PST.NAR-3SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1619" s="T1">этот</ta>
            <ta e="T3" id="Seg_1620" s="T2">день.[NOM]</ta>
            <ta e="T5" id="Seg_1621" s="T4">этот</ta>
            <ta e="T6" id="Seg_1622" s="T5">день.[NOM]</ta>
            <ta e="T8" id="Seg_1623" s="T7">собирать-MULO-RFL.PFV-DUR-3PL</ta>
            <ta e="T9" id="Seg_1624" s="T8">два</ta>
            <ta e="T10" id="Seg_1625" s="T9">человек-EP-DU.[NOM]</ta>
            <ta e="T11" id="Seg_1626" s="T10">здесь</ta>
            <ta e="T13" id="Seg_1627" s="T12">судить-PST-3PL</ta>
            <ta e="T14" id="Seg_1628" s="T13">контора-LOC</ta>
            <ta e="T15" id="Seg_1629" s="T14">Лёня.[NOM]</ta>
            <ta e="T16" id="Seg_1630" s="T15">и</ta>
            <ta e="T17" id="Seg_1631" s="T16">Лёня.[NOM]</ta>
            <ta e="T18" id="Seg_1632" s="T17">и</ta>
            <ta e="T19" id="Seg_1633" s="T18">и</ta>
            <ta e="T20" id="Seg_1634" s="T19">Николай?.[NOM]</ta>
            <ta e="T21" id="Seg_1635" s="T20">один</ta>
            <ta e="T22" id="Seg_1636" s="T21">некто-ACC-3SG</ta>
            <ta e="T23" id="Seg_1637" s="T22">один</ta>
            <ta e="T24" id="Seg_1638" s="T23">некто-ACC-3SG</ta>
            <ta e="T25" id="Seg_1639" s="T24">положить-PST-3PL</ta>
            <ta e="T27" id="Seg_1640" s="T26">поселок-LOC</ta>
            <ta e="T28" id="Seg_1641" s="T27">штраф-INSTR</ta>
            <ta e="T29" id="Seg_1642" s="T28">положить-PST-3PL</ta>
            <ta e="T30" id="Seg_1643" s="T29">один</ta>
            <ta e="T31" id="Seg_1644" s="T30">некто-ACC-3SG</ta>
            <ta e="T34" id="Seg_1645" s="T33">вроде</ta>
            <ta e="T35" id="Seg_1646" s="T34">судить-INF-собраться-PST-3PL</ta>
            <ta e="T37" id="Seg_1647" s="T36">потом</ta>
            <ta e="T41" id="Seg_1648" s="T40">кончить-PST-3PL</ta>
            <ta e="T42" id="Seg_1649" s="T41">потом</ta>
            <ta e="T43" id="Seg_1650" s="T42">затем</ta>
            <ta e="T44" id="Seg_1651" s="T43">NEG</ta>
            <ta e="T45" id="Seg_1652" s="T44">судить-INF-собраться-PST-3PL</ta>
            <ta e="T46" id="Seg_1653" s="T45">%%-ADJZ</ta>
            <ta e="T47" id="Seg_1654" s="T46">год-GEN</ta>
            <ta e="T48" id="Seg_1655" s="T47">для</ta>
            <ta e="T49" id="Seg_1656" s="T48">положить-PST-3PL</ta>
            <ta e="T50" id="Seg_1657" s="T49">вот</ta>
            <ta e="T51" id="Seg_1658" s="T50">только</ta>
            <ta e="T52" id="Seg_1659" s="T51">род.[NOM]</ta>
            <ta e="T53" id="Seg_1660" s="T52">только</ta>
            <ta e="T54" id="Seg_1661" s="T53">положить-PST-3PL</ta>
            <ta e="T55" id="Seg_1662" s="T54">вот</ta>
            <ta e="T56" id="Seg_1663" s="T55">Николай?.[NOM]</ta>
            <ta e="T57" id="Seg_1664" s="T56">%%</ta>
            <ta e="T58" id="Seg_1665" s="T57">%%</ta>
            <ta e="T59" id="Seg_1666" s="T58">Тарко_Сале-ILL</ta>
            <ta e="T60" id="Seg_1667" s="T59">находиться-PST-3PL</ta>
            <ta e="T61" id="Seg_1668" s="T60">когда</ta>
            <ta e="T63" id="Seg_1669" s="T62">пустить-INF</ta>
            <ta e="T64" id="Seg_1670" s="T63">потом</ta>
            <ta e="T68" id="Seg_1671" s="T67">затем</ta>
            <ta e="T69" id="Seg_1672" s="T68">кончить-PST-3PL</ta>
            <ta e="T70" id="Seg_1673" s="T69">мы.PL.GEN</ta>
            <ta e="T71" id="Seg_1674" s="T70">слово-ACC</ta>
            <ta e="T73" id="Seg_1675" s="T72">потом</ta>
            <ta e="T74" id="Seg_1676" s="T73">мы.PL.NOM</ta>
            <ta e="T75" id="Seg_1677" s="T74">такой</ta>
            <ta e="T76" id="Seg_1678" s="T75">я.NOM</ta>
            <ta e="T77" id="Seg_1679" s="T76">мы.PL.NOM</ta>
            <ta e="T78" id="Seg_1680" s="T77">так</ta>
            <ta e="T79" id="Seg_1681" s="T78">сказать-PST-1PL</ta>
            <ta e="T80" id="Seg_1682" s="T79">эй</ta>
            <ta e="T81" id="Seg_1683" s="T80">он(а)</ta>
            <ta e="T82" id="Seg_1684" s="T81">плохой</ta>
            <ta e="T83" id="Seg_1685" s="T82">человек-EP-DIM.[NOM]</ta>
            <ta e="T84" id="Seg_1686" s="T83">плохой</ta>
            <ta e="T85" id="Seg_1687" s="T84">человек-EP-DIM.[NOM]</ta>
            <ta e="T86" id="Seg_1688" s="T85">%%</ta>
            <ta e="T87" id="Seg_1689" s="T86">этот</ta>
            <ta e="T88" id="Seg_1690" s="T87">человек-EP.[NOM]</ta>
            <ta e="T89" id="Seg_1691" s="T88">что.[NOM]</ta>
            <ta e="T90" id="Seg_1692" s="T89">зачем</ta>
            <ta e="T91" id="Seg_1693" s="T90">вперёд</ta>
            <ta e="T92" id="Seg_1694" s="T91">послать-PST-2PL</ta>
            <ta e="T93" id="Seg_1695" s="T92">сейчас</ta>
            <ta e="T94" id="Seg_1696" s="T93">тот</ta>
            <ta e="T95" id="Seg_1697" s="T94">опять</ta>
            <ta e="T96" id="Seg_1698" s="T95">человек-EP.[NOM]</ta>
            <ta e="T97" id="Seg_1699" s="T96">вот</ta>
            <ta e="T98" id="Seg_1700" s="T97">ударить-3SG.O</ta>
            <ta e="T99" id="Seg_1701" s="T98">мы.PL.NOM</ta>
            <ta e="T101" id="Seg_1702" s="T100">%%</ta>
            <ta e="T102" id="Seg_1703" s="T101">сердиться-DRV-RFL.PFV-PST-1PL</ta>
            <ta e="T103" id="Seg_1704" s="T102">это</ta>
            <ta e="T104" id="Seg_1705" s="T103">Николай</ta>
            <ta e="T105" id="Seg_1706" s="T104">о</ta>
            <ta e="T106" id="Seg_1707" s="T105">мы.PL.NOM</ta>
            <ta e="T107" id="Seg_1708" s="T106">так</ta>
            <ta e="T108" id="Seg_1709" s="T107">думать-FRQ-DUR-1PL</ta>
            <ta e="T109" id="Seg_1710" s="T108">вниз</ta>
            <ta e="T110" id="Seg_1711" s="T109">уйти-TR-INF</ta>
            <ta e="T111" id="Seg_1712" s="T110">надо</ta>
            <ta e="T113" id="Seg_1713" s="T112">вниз</ta>
            <ta e="T114" id="Seg_1714" s="T113">сесть-TR-INF</ta>
            <ta e="T115" id="Seg_1715" s="T114">мы.PL.NOM</ta>
            <ta e="T116" id="Seg_1716" s="T115">человек-EP-ADJZ</ta>
            <ta e="T117" id="Seg_1717" s="T116">род.[NOM]</ta>
            <ta e="T119" id="Seg_1718" s="T118">так</ta>
            <ta e="T120" id="Seg_1719" s="T119">положить-PST-3SG.O</ta>
            <ta e="T122" id="Seg_1720" s="T121">а</ta>
            <ta e="T124" id="Seg_1721" s="T123">NEG</ta>
            <ta e="T125" id="Seg_1722" s="T124">хотеть-CVB</ta>
            <ta e="T126" id="Seg_1723" s="T125">так</ta>
            <ta e="T127" id="Seg_1724" s="T126">положить-PST-3PL</ta>
            <ta e="T128" id="Seg_1725" s="T127">потом</ta>
            <ta e="T129" id="Seg_1726" s="T128">человек-EP-PL.[NOM]</ta>
            <ta e="T130" id="Seg_1727" s="T129">что.[NOM]</ta>
            <ta e="T131" id="Seg_1728" s="T130">дом.[NOM]</ta>
            <ta e="T132" id="Seg_1729" s="T131">внутри</ta>
            <ta e="T133" id="Seg_1730" s="T132">%%</ta>
            <ta e="T134" id="Seg_1731" s="T133">дом.[NOM]</ta>
            <ta e="T135" id="Seg_1732" s="T134">внутри</ta>
            <ta e="T136" id="Seg_1733" s="T135">что.[NOM]</ta>
            <ta e="T137" id="Seg_1734" s="T136">он(а)-EP-GEN</ta>
            <ta e="T138" id="Seg_1735" s="T137">%%</ta>
            <ta e="T139" id="Seg_1736" s="T138">%%</ta>
            <ta e="T140" id="Seg_1737" s="T139">судить-RES-PFV-INFER-3PL</ta>
            <ta e="T141" id="Seg_1738" s="T140">человек-EP-PL.[NOM]</ta>
            <ta e="T142" id="Seg_1739" s="T141">сильно</ta>
            <ta e="T143" id="Seg_1740" s="T142">сердиться-DRV-PST-3PL</ta>
            <ta e="T144" id="Seg_1741" s="T143">этот</ta>
            <ta e="T145" id="Seg_1742" s="T144">Николай?.[NOM]</ta>
            <ta e="T146" id="Seg_1743" s="T145">для</ta>
            <ta e="T147" id="Seg_1744" s="T146">слово-ACC</ta>
            <ta e="T148" id="Seg_1745" s="T147">вниз</ta>
            <ta e="T149" id="Seg_1746" s="T148">послать-INF</ta>
            <ta e="T150" id="Seg_1747" s="T149">вниз</ta>
            <ta e="T151" id="Seg_1748" s="T150">сесть-TR-INF</ta>
            <ta e="T152" id="Seg_1749" s="T151">%%</ta>
            <ta e="T153" id="Seg_1750" s="T152">судить-CAUS-INF</ta>
            <ta e="T155" id="Seg_1751" s="T154">положить-PST-3PL</ta>
            <ta e="T156" id="Seg_1752" s="T155">ну.ка</ta>
            <ta e="T157" id="Seg_1753" s="T156">затем</ta>
            <ta e="T158" id="Seg_1754" s="T157">кончить-PST-3PL</ta>
            <ta e="T159" id="Seg_1755" s="T158">кончить-PST-3PL</ta>
            <ta e="T160" id="Seg_1756" s="T159">потом</ta>
            <ta e="T161" id="Seg_1757" s="T160">мы.PL.NOM</ta>
            <ta e="T162" id="Seg_1758" s="T161">затем</ta>
            <ta e="T163" id="Seg_1759" s="T162">кончить-PST-1PL</ta>
            <ta e="T164" id="Seg_1760" s="T163">Сергей.[NOM]</ta>
            <ta e="T165" id="Seg_1761" s="T164">заявление.[NOM]</ta>
            <ta e="T166" id="Seg_1762" s="T165">дать-DUR-PST-3SG.O</ta>
            <ta e="T167" id="Seg_1763" s="T166">Сергей.[NOM]</ta>
            <ta e="T169" id="Seg_1764" s="T168">дом.[NOM]</ta>
            <ta e="T170" id="Seg_1765" s="T169">%%</ta>
            <ta e="T171" id="Seg_1766" s="T170">рвать-CO-3SG.O</ta>
            <ta e="T172" id="Seg_1767" s="T171">Сергей.[NOM]</ta>
            <ta e="T173" id="Seg_1768" s="T172">дом.[NOM]</ta>
            <ta e="T174" id="Seg_1769" s="T173">%%</ta>
            <ta e="T175" id="Seg_1770" s="T174">рвать-CO-3SG.O</ta>
            <ta e="T177" id="Seg_1771" s="T176">два</ta>
            <ta e="T178" id="Seg_1772" s="T177">раз.[NOM]</ta>
            <ta e="T179" id="Seg_1773" s="T178">ночь-ADV.LOC</ta>
            <ta e="T180" id="Seg_1774" s="T179">залезть-CVB</ta>
            <ta e="T181" id="Seg_1775" s="T180">ночь-ADV.LOC</ta>
            <ta e="T182" id="Seg_1776" s="T181">Сергей-GEN</ta>
            <ta e="T183" id="Seg_1777" s="T182">дом.[NOM]</ta>
            <ta e="T184" id="Seg_1778" s="T183">%%</ta>
            <ta e="T185" id="Seg_1779" s="T184">%%</ta>
            <ta e="T186" id="Seg_1780" s="T185">рвать-PST.NAR-3SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1781" s="T1">dem</ta>
            <ta e="T3" id="Seg_1782" s="T2">n-n:case</ta>
            <ta e="T5" id="Seg_1783" s="T4">dem</ta>
            <ta e="T6" id="Seg_1784" s="T5">n-n:case</ta>
            <ta e="T8" id="Seg_1785" s="T7">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T9" id="Seg_1786" s="T8">num</ta>
            <ta e="T10" id="Seg_1787" s="T9">n-n:ins-n:num-n:case</ta>
            <ta e="T11" id="Seg_1788" s="T10">adv</ta>
            <ta e="T13" id="Seg_1789" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_1790" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_1791" s="T14">nprop-n:case</ta>
            <ta e="T16" id="Seg_1792" s="T15">conj</ta>
            <ta e="T17" id="Seg_1793" s="T16">nprop-n:case</ta>
            <ta e="T18" id="Seg_1794" s="T17">conj</ta>
            <ta e="T19" id="Seg_1795" s="T18">conj</ta>
            <ta e="T20" id="Seg_1796" s="T19">nprop-n:case</ta>
            <ta e="T21" id="Seg_1797" s="T20">num</ta>
            <ta e="T22" id="Seg_1798" s="T21">n-n:case-n:poss</ta>
            <ta e="T23" id="Seg_1799" s="T22">num</ta>
            <ta e="T24" id="Seg_1800" s="T23">n-n:case-n:poss</ta>
            <ta e="T25" id="Seg_1801" s="T24">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_1802" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_1803" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_1804" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_1805" s="T29">num</ta>
            <ta e="T31" id="Seg_1806" s="T30">n-n:case-n:poss</ta>
            <ta e="T34" id="Seg_1807" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_1808" s="T34">v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_1809" s="T36">adv</ta>
            <ta e="T41" id="Seg_1810" s="T40">v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_1811" s="T41">adv</ta>
            <ta e="T43" id="Seg_1812" s="T42">adv</ta>
            <ta e="T44" id="Seg_1813" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_1814" s="T44">v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_1815" s="T45">%%-n&gt;adj</ta>
            <ta e="T47" id="Seg_1816" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_1817" s="T47">pp</ta>
            <ta e="T49" id="Seg_1818" s="T48">v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_1819" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_1820" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_1821" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_1822" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_1823" s="T53">v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_1824" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_1825" s="T55">nprop-n:case</ta>
            <ta e="T57" id="Seg_1826" s="T56">%%</ta>
            <ta e="T58" id="Seg_1827" s="T57">%%</ta>
            <ta e="T59" id="Seg_1828" s="T58">nprop-n:case</ta>
            <ta e="T60" id="Seg_1829" s="T59">v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_1830" s="T60">interrog</ta>
            <ta e="T63" id="Seg_1831" s="T62">v-v:inf</ta>
            <ta e="T64" id="Seg_1832" s="T63">adv</ta>
            <ta e="T68" id="Seg_1833" s="T67">adv</ta>
            <ta e="T69" id="Seg_1834" s="T68">v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_1835" s="T69">pers</ta>
            <ta e="T71" id="Seg_1836" s="T70">n-n:case</ta>
            <ta e="T73" id="Seg_1837" s="T72">adv</ta>
            <ta e="T74" id="Seg_1838" s="T73">pers</ta>
            <ta e="T75" id="Seg_1839" s="T74">dem</ta>
            <ta e="T76" id="Seg_1840" s="T75">pers</ta>
            <ta e="T77" id="Seg_1841" s="T76">pers</ta>
            <ta e="T78" id="Seg_1842" s="T77">adv</ta>
            <ta e="T79" id="Seg_1843" s="T78">v-v:tense-n:poss</ta>
            <ta e="T80" id="Seg_1844" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_1845" s="T80">pers</ta>
            <ta e="T82" id="Seg_1846" s="T81">adj</ta>
            <ta e="T83" id="Seg_1847" s="T82">n-n:ins-n&gt;n-n:case</ta>
            <ta e="T84" id="Seg_1848" s="T83">adj</ta>
            <ta e="T85" id="Seg_1849" s="T84">n-n:ins-n&gt;n-n:case</ta>
            <ta e="T86" id="Seg_1850" s="T85">%%</ta>
            <ta e="T87" id="Seg_1851" s="T86">dem</ta>
            <ta e="T88" id="Seg_1852" s="T87">n-n:ins-n:case</ta>
            <ta e="T89" id="Seg_1853" s="T88">interrog-n:case</ta>
            <ta e="T90" id="Seg_1854" s="T89">interrog</ta>
            <ta e="T91" id="Seg_1855" s="T90">adv</ta>
            <ta e="T92" id="Seg_1856" s="T91">v-v:tense-v:pn</ta>
            <ta e="T93" id="Seg_1857" s="T92">adv</ta>
            <ta e="T94" id="Seg_1858" s="T93">dem</ta>
            <ta e="T95" id="Seg_1859" s="T94">adv</ta>
            <ta e="T96" id="Seg_1860" s="T95">n-n:ins-n:case</ta>
            <ta e="T97" id="Seg_1861" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_1862" s="T97">v-v:pn</ta>
            <ta e="T99" id="Seg_1863" s="T98">pers</ta>
            <ta e="T101" id="Seg_1864" s="T100">%%</ta>
            <ta e="T102" id="Seg_1865" s="T101">v-v&gt;v-v&gt;v-v:tense-n:poss</ta>
            <ta e="T103" id="Seg_1866" s="T102">pro</ta>
            <ta e="T104" id="Seg_1867" s="T103">nprop</ta>
            <ta e="T105" id="Seg_1868" s="T104">pp</ta>
            <ta e="T106" id="Seg_1869" s="T105">pers</ta>
            <ta e="T107" id="Seg_1870" s="T106">adv</ta>
            <ta e="T108" id="Seg_1871" s="T107">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T109" id="Seg_1872" s="T108">preverb</ta>
            <ta e="T110" id="Seg_1873" s="T109">v-v&gt;v-v:inf</ta>
            <ta e="T111" id="Seg_1874" s="T110">ptcl</ta>
            <ta e="T113" id="Seg_1875" s="T112">preverb</ta>
            <ta e="T114" id="Seg_1876" s="T113">v-v&gt;v-v:inf</ta>
            <ta e="T115" id="Seg_1877" s="T114">pers</ta>
            <ta e="T116" id="Seg_1878" s="T115">n-n:ins-n&gt;adj</ta>
            <ta e="T117" id="Seg_1879" s="T116">n-n:case</ta>
            <ta e="T119" id="Seg_1880" s="T118">adv</ta>
            <ta e="T120" id="Seg_1881" s="T119">v-v:tense-v:pn</ta>
            <ta e="T122" id="Seg_1882" s="T121">conj</ta>
            <ta e="T124" id="Seg_1883" s="T123">ptcl</ta>
            <ta e="T125" id="Seg_1884" s="T124">v-v&gt;adv</ta>
            <ta e="T126" id="Seg_1885" s="T125">adv</ta>
            <ta e="T127" id="Seg_1886" s="T126">v-v:tense-v:pn</ta>
            <ta e="T128" id="Seg_1887" s="T127">adv</ta>
            <ta e="T129" id="Seg_1888" s="T128">n-n:ins-n:num-n:case</ta>
            <ta e="T130" id="Seg_1889" s="T129">interrog-n:case</ta>
            <ta e="T131" id="Seg_1890" s="T130">n-n:case</ta>
            <ta e="T132" id="Seg_1891" s="T131">pp</ta>
            <ta e="T133" id="Seg_1892" s="T132">%%</ta>
            <ta e="T134" id="Seg_1893" s="T133">n-n:case</ta>
            <ta e="T135" id="Seg_1894" s="T134">pp</ta>
            <ta e="T136" id="Seg_1895" s="T135">interrog-n:case</ta>
            <ta e="T137" id="Seg_1896" s="T136">pers-n:ins-n:case</ta>
            <ta e="T138" id="Seg_1897" s="T137">%%</ta>
            <ta e="T139" id="Seg_1898" s="T138">%%</ta>
            <ta e="T140" id="Seg_1899" s="T139">v-v&gt;v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T141" id="Seg_1900" s="T140">n-n:ins-n:num-n:case</ta>
            <ta e="T142" id="Seg_1901" s="T141">adv</ta>
            <ta e="T143" id="Seg_1902" s="T142">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T144" id="Seg_1903" s="T143">dem</ta>
            <ta e="T145" id="Seg_1904" s="T144">nprop-n:case</ta>
            <ta e="T146" id="Seg_1905" s="T145">pp</ta>
            <ta e="T147" id="Seg_1906" s="T146">n-n:case</ta>
            <ta e="T148" id="Seg_1907" s="T147">preverb</ta>
            <ta e="T149" id="Seg_1908" s="T148">v-v:inf</ta>
            <ta e="T150" id="Seg_1909" s="T149">preverb</ta>
            <ta e="T151" id="Seg_1910" s="T150">v-v&gt;v-v:inf</ta>
            <ta e="T152" id="Seg_1911" s="T151">%%</ta>
            <ta e="T153" id="Seg_1912" s="T152">v-v&gt;v-v:inf</ta>
            <ta e="T155" id="Seg_1913" s="T154">v-v:tense-v:pn</ta>
            <ta e="T156" id="Seg_1914" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_1915" s="T156">adv</ta>
            <ta e="T158" id="Seg_1916" s="T157">v-v:tense-v:pn</ta>
            <ta e="T159" id="Seg_1917" s="T158">v-v:tense-v:pn</ta>
            <ta e="T160" id="Seg_1918" s="T159">adv</ta>
            <ta e="T161" id="Seg_1919" s="T160">pers</ta>
            <ta e="T162" id="Seg_1920" s="T161">adv</ta>
            <ta e="T163" id="Seg_1921" s="T162">v-v:tense-n:poss</ta>
            <ta e="T164" id="Seg_1922" s="T163">nprop-n:case</ta>
            <ta e="T165" id="Seg_1923" s="T164">n-n:case</ta>
            <ta e="T166" id="Seg_1924" s="T165">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T167" id="Seg_1925" s="T166">nprop-n:case</ta>
            <ta e="T169" id="Seg_1926" s="T168">n-n:case</ta>
            <ta e="T170" id="Seg_1927" s="T169">%%</ta>
            <ta e="T171" id="Seg_1928" s="T170">v-v:ins-v:pn</ta>
            <ta e="T172" id="Seg_1929" s="T171">nprop-n:case</ta>
            <ta e="T173" id="Seg_1930" s="T172">n-n:case</ta>
            <ta e="T174" id="Seg_1931" s="T173">%%</ta>
            <ta e="T175" id="Seg_1932" s="T174">v-v:ins-v:pn</ta>
            <ta e="T177" id="Seg_1933" s="T176">num</ta>
            <ta e="T178" id="Seg_1934" s="T177">n-n:case</ta>
            <ta e="T179" id="Seg_1935" s="T178">n-n&gt;adv</ta>
            <ta e="T180" id="Seg_1936" s="T179">v-v&gt;adv</ta>
            <ta e="T181" id="Seg_1937" s="T180">n-n&gt;adv</ta>
            <ta e="T182" id="Seg_1938" s="T181">nprop-n:case</ta>
            <ta e="T183" id="Seg_1939" s="T182">n-n:case</ta>
            <ta e="T184" id="Seg_1940" s="T183">%%</ta>
            <ta e="T185" id="Seg_1941" s="T184">%%</ta>
            <ta e="T186" id="Seg_1942" s="T185">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1943" s="T1">dem</ta>
            <ta e="T3" id="Seg_1944" s="T2">n</ta>
            <ta e="T5" id="Seg_1945" s="T4">dem</ta>
            <ta e="T6" id="Seg_1946" s="T5">n</ta>
            <ta e="T8" id="Seg_1947" s="T7">v</ta>
            <ta e="T9" id="Seg_1948" s="T8">num</ta>
            <ta e="T10" id="Seg_1949" s="T9">n</ta>
            <ta e="T11" id="Seg_1950" s="T10">adv</ta>
            <ta e="T13" id="Seg_1951" s="T12">v</ta>
            <ta e="T14" id="Seg_1952" s="T13">n</ta>
            <ta e="T15" id="Seg_1953" s="T14">nprop</ta>
            <ta e="T16" id="Seg_1954" s="T15">conj</ta>
            <ta e="T17" id="Seg_1955" s="T16">nprop</ta>
            <ta e="T18" id="Seg_1956" s="T17">conj</ta>
            <ta e="T19" id="Seg_1957" s="T18">conj</ta>
            <ta e="T20" id="Seg_1958" s="T19">nprop</ta>
            <ta e="T21" id="Seg_1959" s="T20">num</ta>
            <ta e="T22" id="Seg_1960" s="T21">n</ta>
            <ta e="T23" id="Seg_1961" s="T22">num</ta>
            <ta e="T24" id="Seg_1962" s="T23">n</ta>
            <ta e="T25" id="Seg_1963" s="T24">v</ta>
            <ta e="T27" id="Seg_1964" s="T26">n</ta>
            <ta e="T28" id="Seg_1965" s="T27">n</ta>
            <ta e="T29" id="Seg_1966" s="T28">v</ta>
            <ta e="T30" id="Seg_1967" s="T29">num</ta>
            <ta e="T31" id="Seg_1968" s="T30">n</ta>
            <ta e="T34" id="Seg_1969" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_1970" s="T34">v</ta>
            <ta e="T37" id="Seg_1971" s="T36">adv</ta>
            <ta e="T41" id="Seg_1972" s="T40">v</ta>
            <ta e="T42" id="Seg_1973" s="T41">adv</ta>
            <ta e="T43" id="Seg_1974" s="T42">adv</ta>
            <ta e="T44" id="Seg_1975" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_1976" s="T44">v</ta>
            <ta e="T47" id="Seg_1977" s="T46">n</ta>
            <ta e="T48" id="Seg_1978" s="T47">pp</ta>
            <ta e="T49" id="Seg_1979" s="T48">v</ta>
            <ta e="T50" id="Seg_1980" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_1981" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_1982" s="T51">n</ta>
            <ta e="T53" id="Seg_1983" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_1984" s="T53">v</ta>
            <ta e="T55" id="Seg_1985" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_1986" s="T55">nprop</ta>
            <ta e="T59" id="Seg_1987" s="T58">nprop</ta>
            <ta e="T60" id="Seg_1988" s="T59">v</ta>
            <ta e="T61" id="Seg_1989" s="T60">interrog</ta>
            <ta e="T63" id="Seg_1990" s="T62">v</ta>
            <ta e="T64" id="Seg_1991" s="T63">adv</ta>
            <ta e="T68" id="Seg_1992" s="T67">adv</ta>
            <ta e="T69" id="Seg_1993" s="T68">v</ta>
            <ta e="T70" id="Seg_1994" s="T69">pers</ta>
            <ta e="T71" id="Seg_1995" s="T70">n</ta>
            <ta e="T73" id="Seg_1996" s="T72">adv</ta>
            <ta e="T74" id="Seg_1997" s="T73">pers</ta>
            <ta e="T75" id="Seg_1998" s="T74">dem</ta>
            <ta e="T76" id="Seg_1999" s="T75">pers</ta>
            <ta e="T77" id="Seg_2000" s="T76">pers</ta>
            <ta e="T78" id="Seg_2001" s="T77">adv</ta>
            <ta e="T79" id="Seg_2002" s="T78">v</ta>
            <ta e="T80" id="Seg_2003" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_2004" s="T80">pers</ta>
            <ta e="T82" id="Seg_2005" s="T81">adj</ta>
            <ta e="T83" id="Seg_2006" s="T82">n</ta>
            <ta e="T84" id="Seg_2007" s="T83">adj</ta>
            <ta e="T85" id="Seg_2008" s="T84">n</ta>
            <ta e="T87" id="Seg_2009" s="T86">dem</ta>
            <ta e="T88" id="Seg_2010" s="T87">n</ta>
            <ta e="T89" id="Seg_2011" s="T88">interrog</ta>
            <ta e="T90" id="Seg_2012" s="T89">interrog</ta>
            <ta e="T91" id="Seg_2013" s="T90">adv</ta>
            <ta e="T92" id="Seg_2014" s="T91">v</ta>
            <ta e="T93" id="Seg_2015" s="T92">adv</ta>
            <ta e="T94" id="Seg_2016" s="T93">dem</ta>
            <ta e="T95" id="Seg_2017" s="T94">adv</ta>
            <ta e="T96" id="Seg_2018" s="T95">n</ta>
            <ta e="T97" id="Seg_2019" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_2020" s="T97">v</ta>
            <ta e="T99" id="Seg_2021" s="T98">pers</ta>
            <ta e="T102" id="Seg_2022" s="T101">v</ta>
            <ta e="T103" id="Seg_2023" s="T102">pro</ta>
            <ta e="T104" id="Seg_2024" s="T103">nprop</ta>
            <ta e="T105" id="Seg_2025" s="T104">pp</ta>
            <ta e="T106" id="Seg_2026" s="T105">pers</ta>
            <ta e="T107" id="Seg_2027" s="T106">adv</ta>
            <ta e="T108" id="Seg_2028" s="T107">v</ta>
            <ta e="T109" id="Seg_2029" s="T108">preverb</ta>
            <ta e="T110" id="Seg_2030" s="T109">v</ta>
            <ta e="T111" id="Seg_2031" s="T110">ptcl</ta>
            <ta e="T113" id="Seg_2032" s="T112">preverb</ta>
            <ta e="T114" id="Seg_2033" s="T113">v</ta>
            <ta e="T115" id="Seg_2034" s="T114">pers</ta>
            <ta e="T116" id="Seg_2035" s="T115">adj</ta>
            <ta e="T117" id="Seg_2036" s="T116">n</ta>
            <ta e="T119" id="Seg_2037" s="T118">adv</ta>
            <ta e="T120" id="Seg_2038" s="T119">v</ta>
            <ta e="T122" id="Seg_2039" s="T121">conj</ta>
            <ta e="T124" id="Seg_2040" s="T123">ptcl</ta>
            <ta e="T125" id="Seg_2041" s="T124">adv</ta>
            <ta e="T126" id="Seg_2042" s="T125">adv</ta>
            <ta e="T127" id="Seg_2043" s="T126">v</ta>
            <ta e="T128" id="Seg_2044" s="T127">adv</ta>
            <ta e="T129" id="Seg_2045" s="T128">n</ta>
            <ta e="T130" id="Seg_2046" s="T129">interrog</ta>
            <ta e="T131" id="Seg_2047" s="T130">n</ta>
            <ta e="T132" id="Seg_2048" s="T131">n</ta>
            <ta e="T133" id="Seg_2049" s="T132">ptcl</ta>
            <ta e="T134" id="Seg_2050" s="T133">n</ta>
            <ta e="T135" id="Seg_2051" s="T134">n</ta>
            <ta e="T136" id="Seg_2052" s="T135">interrog</ta>
            <ta e="T137" id="Seg_2053" s="T136">pers</ta>
            <ta e="T138" id="Seg_2054" s="T137">ptcl</ta>
            <ta e="T141" id="Seg_2055" s="T140">n</ta>
            <ta e="T142" id="Seg_2056" s="T141">adv</ta>
            <ta e="T143" id="Seg_2057" s="T142">v</ta>
            <ta e="T144" id="Seg_2058" s="T143">dem</ta>
            <ta e="T145" id="Seg_2059" s="T144">nprop</ta>
            <ta e="T146" id="Seg_2060" s="T145">pp</ta>
            <ta e="T147" id="Seg_2061" s="T146">n</ta>
            <ta e="T148" id="Seg_2062" s="T147">preverb</ta>
            <ta e="T149" id="Seg_2063" s="T148">v</ta>
            <ta e="T150" id="Seg_2064" s="T149">preverb</ta>
            <ta e="T151" id="Seg_2065" s="T150">v</ta>
            <ta e="T153" id="Seg_2066" s="T152">v</ta>
            <ta e="T155" id="Seg_2067" s="T154">v</ta>
            <ta e="T156" id="Seg_2068" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_2069" s="T156">adv</ta>
            <ta e="T158" id="Seg_2070" s="T157">v</ta>
            <ta e="T159" id="Seg_2071" s="T158">v</ta>
            <ta e="T160" id="Seg_2072" s="T159">adv</ta>
            <ta e="T161" id="Seg_2073" s="T160">pers</ta>
            <ta e="T162" id="Seg_2074" s="T161">adv</ta>
            <ta e="T163" id="Seg_2075" s="T162">v</ta>
            <ta e="T164" id="Seg_2076" s="T163">nprop</ta>
            <ta e="T165" id="Seg_2077" s="T164">n</ta>
            <ta e="T166" id="Seg_2078" s="T165">v</ta>
            <ta e="T167" id="Seg_2079" s="T166">nprop</ta>
            <ta e="T169" id="Seg_2080" s="T168">n</ta>
            <ta e="T170" id="Seg_2081" s="T169">adv</ta>
            <ta e="T171" id="Seg_2082" s="T170">v</ta>
            <ta e="T172" id="Seg_2083" s="T171">nprop</ta>
            <ta e="T173" id="Seg_2084" s="T172">n</ta>
            <ta e="T174" id="Seg_2085" s="T173">adv</ta>
            <ta e="T175" id="Seg_2086" s="T174">v</ta>
            <ta e="T177" id="Seg_2087" s="T176">num</ta>
            <ta e="T178" id="Seg_2088" s="T177">n</ta>
            <ta e="T179" id="Seg_2089" s="T178">adv</ta>
            <ta e="T180" id="Seg_2090" s="T179">adv</ta>
            <ta e="T181" id="Seg_2091" s="T180">adv</ta>
            <ta e="T182" id="Seg_2092" s="T181">n</ta>
            <ta e="T183" id="Seg_2093" s="T182">n</ta>
            <ta e="T184" id="Seg_2094" s="T183">adj</ta>
            <ta e="T185" id="Seg_2095" s="T184">adv</ta>
            <ta e="T186" id="Seg_2096" s="T185">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_2097" s="T2">np:Time</ta>
            <ta e="T6" id="Seg_2098" s="T5">np:Time</ta>
            <ta e="T8" id="Seg_2099" s="T7">0.3.h:A</ta>
            <ta e="T10" id="Seg_2100" s="T9">np.h:P</ta>
            <ta e="T11" id="Seg_2101" s="T10">adv:L</ta>
            <ta e="T13" id="Seg_2102" s="T12">0.3.h:A</ta>
            <ta e="T14" id="Seg_2103" s="T13">np:L</ta>
            <ta e="T24" id="Seg_2104" s="T23">np.h:P</ta>
            <ta e="T25" id="Seg_2105" s="T24">0.3.h:A</ta>
            <ta e="T27" id="Seg_2106" s="T26">np:L</ta>
            <ta e="T28" id="Seg_2107" s="T27">np:Ins</ta>
            <ta e="T29" id="Seg_2108" s="T28">0.3.h:A</ta>
            <ta e="T31" id="Seg_2109" s="T30">np.h:P</ta>
            <ta e="T35" id="Seg_2110" s="T34">0.3.h:A</ta>
            <ta e="T37" id="Seg_2111" s="T36">adv:Time</ta>
            <ta e="T41" id="Seg_2112" s="T40">0.3.h:A</ta>
            <ta e="T42" id="Seg_2113" s="T41">adv:Time</ta>
            <ta e="T45" id="Seg_2114" s="T44">0.3.h:A</ta>
            <ta e="T47" id="Seg_2115" s="T46">pp:Time</ta>
            <ta e="T49" id="Seg_2116" s="T48">0.3.h:A</ta>
            <ta e="T52" id="Seg_2117" s="T51">np.h:A</ta>
            <ta e="T56" id="Seg_2118" s="T55">np.h:Th</ta>
            <ta e="T59" id="Seg_2119" s="T58">np:L</ta>
            <ta e="T60" id="Seg_2120" s="T59">0.3.h:Th</ta>
            <ta e="T63" id="Seg_2121" s="T62">0.3.h:A</ta>
            <ta e="T64" id="Seg_2122" s="T63">adv:Time</ta>
            <ta e="T69" id="Seg_2123" s="T68">0.3.h:A</ta>
            <ta e="T70" id="Seg_2124" s="T69">pro.h:Poss</ta>
            <ta e="T73" id="Seg_2125" s="T72">adv:Time</ta>
            <ta e="T77" id="Seg_2126" s="T76">pro.h:A</ta>
            <ta e="T81" id="Seg_2127" s="T80">pro.h:Th</ta>
            <ta e="T88" id="Seg_2128" s="T87">np.h:Th</ta>
            <ta e="T91" id="Seg_2129" s="T90">adv:G</ta>
            <ta e="T92" id="Seg_2130" s="T91">0.2.h:A</ta>
            <ta e="T93" id="Seg_2131" s="T92">adv:Time</ta>
            <ta e="T96" id="Seg_2132" s="T95">np.h:A</ta>
            <ta e="T98" id="Seg_2133" s="T97">0.3.h:P</ta>
            <ta e="T99" id="Seg_2134" s="T98">pro.h:E</ta>
            <ta e="T104" id="Seg_2135" s="T103">pp:Cau</ta>
            <ta e="T106" id="Seg_2136" s="T105">pro.h:E</ta>
            <ta e="T110" id="Seg_2137" s="T109">0.3.h:A</ta>
            <ta e="T114" id="Seg_2138" s="T113">0.3.h:A</ta>
            <ta e="T117" id="Seg_2139" s="T116">np.h:A</ta>
            <ta e="T120" id="Seg_2140" s="T119">0.3:Th</ta>
            <ta e="T125" id="Seg_2141" s="T124">0.3.h:E</ta>
            <ta e="T127" id="Seg_2142" s="T126">0.3.h:A</ta>
            <ta e="T128" id="Seg_2143" s="T127">adv:Time</ta>
            <ta e="T129" id="Seg_2144" s="T128">np.h:A</ta>
            <ta e="T131" id="Seg_2145" s="T130">pp:L</ta>
            <ta e="T134" id="Seg_2146" s="T133">pp:L</ta>
            <ta e="T141" id="Seg_2147" s="T140">np.h:E</ta>
            <ta e="T145" id="Seg_2148" s="T144">pp:Cau</ta>
            <ta e="T147" id="Seg_2149" s="T146">np:Th</ta>
            <ta e="T149" id="Seg_2150" s="T148">0.3.h:A</ta>
            <ta e="T151" id="Seg_2151" s="T150">0.3.h:A</ta>
            <ta e="T153" id="Seg_2152" s="T152">0.3.h:A</ta>
            <ta e="T155" id="Seg_2153" s="T154">0.3.h:A</ta>
            <ta e="T157" id="Seg_2154" s="T156">adv:Time</ta>
            <ta e="T158" id="Seg_2155" s="T157">0.3.h:A</ta>
            <ta e="T159" id="Seg_2156" s="T158">0.3.h:A</ta>
            <ta e="T160" id="Seg_2157" s="T159">adv:Time</ta>
            <ta e="T161" id="Seg_2158" s="T160">pro.h:A</ta>
            <ta e="T164" id="Seg_2159" s="T163">np.h:A</ta>
            <ta e="T165" id="Seg_2160" s="T164">np:Th</ta>
            <ta e="T167" id="Seg_2161" s="T166">np.h:A</ta>
            <ta e="T169" id="Seg_2162" s="T168">np:P</ta>
            <ta e="T172" id="Seg_2163" s="T171">np.h:A</ta>
            <ta e="T173" id="Seg_2164" s="T172">np:P</ta>
            <ta e="T179" id="Seg_2165" s="T178">adv:Time</ta>
            <ta e="T180" id="Seg_2166" s="T179">0.3.h:A</ta>
            <ta e="T182" id="Seg_2167" s="T181">np.h:Poss</ta>
            <ta e="T183" id="Seg_2168" s="T182">np:P</ta>
            <ta e="T186" id="Seg_2169" s="T185">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T8" id="Seg_2170" s="T7">0.3.h:S v:pred</ta>
            <ta e="T10" id="Seg_2171" s="T9">np.h:O</ta>
            <ta e="T13" id="Seg_2172" s="T12">0.3.h:S v:pred</ta>
            <ta e="T24" id="Seg_2173" s="T23">np.h:O</ta>
            <ta e="T25" id="Seg_2174" s="T24">0.3.h:S v:pred</ta>
            <ta e="T29" id="Seg_2175" s="T28">0.3.h:S v:pred</ta>
            <ta e="T31" id="Seg_2176" s="T30">np.h:O</ta>
            <ta e="T35" id="Seg_2177" s="T34">0.3.h:S v:pred</ta>
            <ta e="T41" id="Seg_2178" s="T40">0.3.h:S v:pred</ta>
            <ta e="T45" id="Seg_2179" s="T44">0.3.h:S v:pred</ta>
            <ta e="T49" id="Seg_2180" s="T48">0.3.h:S v:pred</ta>
            <ta e="T52" id="Seg_2181" s="T51">np.h:S</ta>
            <ta e="T54" id="Seg_2182" s="T53">v:pred</ta>
            <ta e="T56" id="Seg_2183" s="T55">np.h:O</ta>
            <ta e="T60" id="Seg_2184" s="T59">0.3.h:S v:pred</ta>
            <ta e="T63" id="Seg_2185" s="T60">s:temp</ta>
            <ta e="T69" id="Seg_2186" s="T68">0.3.h:S v:pred</ta>
            <ta e="T77" id="Seg_2187" s="T76">pro.h:S</ta>
            <ta e="T79" id="Seg_2188" s="T78">v:pred</ta>
            <ta e="T81" id="Seg_2189" s="T80">pro.h:S</ta>
            <ta e="T83" id="Seg_2190" s="T82">n:pred</ta>
            <ta e="T85" id="Seg_2191" s="T84">n:pred</ta>
            <ta e="T88" id="Seg_2192" s="T87">np.h:O</ta>
            <ta e="T92" id="Seg_2193" s="T91">0.2.h:S v:pred</ta>
            <ta e="T96" id="Seg_2194" s="T95">np.h:S</ta>
            <ta e="T98" id="Seg_2195" s="T97">v:pred 0.3.h:O</ta>
            <ta e="T99" id="Seg_2196" s="T98">pro.h:S</ta>
            <ta e="T102" id="Seg_2197" s="T101">v:pred</ta>
            <ta e="T106" id="Seg_2198" s="T105">pro.h:S</ta>
            <ta e="T108" id="Seg_2199" s="T107">v:pred</ta>
            <ta e="T114" id="Seg_2200" s="T108">s:compl</ta>
            <ta e="T117" id="Seg_2201" s="T116">np.h:S</ta>
            <ta e="T120" id="Seg_2202" s="T119">v:pred 0.3:O</ta>
            <ta e="T125" id="Seg_2203" s="T123">s:temp</ta>
            <ta e="T127" id="Seg_2204" s="T126">0.3.h:S v:pred</ta>
            <ta e="T129" id="Seg_2205" s="T128">np.h:S</ta>
            <ta e="T140" id="Seg_2206" s="T139">v:pred</ta>
            <ta e="T141" id="Seg_2207" s="T140">np.h:S</ta>
            <ta e="T143" id="Seg_2208" s="T142">v:pred</ta>
            <ta e="T149" id="Seg_2209" s="T146">s:purp</ta>
            <ta e="T153" id="Seg_2210" s="T149">s:purp</ta>
            <ta e="T155" id="Seg_2211" s="T154">0.3.h:S v:pred</ta>
            <ta e="T158" id="Seg_2212" s="T157">0.3.h:S v:pred</ta>
            <ta e="T159" id="Seg_2213" s="T158">0.3.h:S v:pred</ta>
            <ta e="T161" id="Seg_2214" s="T160">pro.h:S</ta>
            <ta e="T163" id="Seg_2215" s="T162">v:pred</ta>
            <ta e="T164" id="Seg_2216" s="T163">np.h:S</ta>
            <ta e="T165" id="Seg_2217" s="T164">np:O</ta>
            <ta e="T166" id="Seg_2218" s="T165">v:pred</ta>
            <ta e="T167" id="Seg_2219" s="T166">np.h:S</ta>
            <ta e="T169" id="Seg_2220" s="T168">np:O</ta>
            <ta e="T171" id="Seg_2221" s="T170">v:pred</ta>
            <ta e="T172" id="Seg_2222" s="T171">np.h:S</ta>
            <ta e="T173" id="Seg_2223" s="T172">np:O</ta>
            <ta e="T175" id="Seg_2224" s="T174">v:pred</ta>
            <ta e="T180" id="Seg_2225" s="T176">s:temp</ta>
            <ta e="T183" id="Seg_2226" s="T182">np:O</ta>
            <ta e="T186" id="Seg_2227" s="T185">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T13" id="Seg_2228" s="T12">RUS:cult</ta>
            <ta e="T15" id="Seg_2229" s="T14">RUS:cult</ta>
            <ta e="T17" id="Seg_2230" s="T16">RUS:cult</ta>
            <ta e="T27" id="Seg_2231" s="T26">RUS:cult</ta>
            <ta e="T28" id="Seg_2232" s="T27">RUS:cult</ta>
            <ta e="T35" id="Seg_2233" s="T34">RUS:cult</ta>
            <ta e="T45" id="Seg_2234" s="T44">RUS:cult</ta>
            <ta e="T111" id="Seg_2235" s="T110">RUS:mod</ta>
            <ta e="T122" id="Seg_2236" s="T121">RUS:gram</ta>
            <ta e="T140" id="Seg_2237" s="T139">RUS:cult</ta>
            <ta e="T153" id="Seg_2238" s="T152">RUS:cult</ta>
            <ta e="T165" id="Seg_2239" s="T164">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T66" id="Seg_2240" s="T0">[KuAI:] Большой </ta>
            <ta e="T14" id="Seg_2241" s="T66">[NN2:] В этот день … в этот день собрались, двух человек здесь судили в конторе. </ta>
            <ta e="T20" id="Seg_2242" s="T14">Лёню и Николая (имя по-селькупски)</ta>
            <ta e="T29" id="Seg_2243" s="T20">Одному другу дали в посёлке штраф, дали.</ta>
            <ta e="T36" id="Seg_2244" s="T29">Одного друга вроде судить стали.</ta>
            <ta e="T45" id="Seg_2245" s="T36">Потом перестали судить, потом не стали судить.</ta>
            <ta e="T49" id="Seg_2246" s="T45">На другой год оставили.</ta>
            <ta e="T57" id="Seg_2247" s="T49">Вот только народ предположил, что вот Николай битый.</ta>
            <ta e="T63" id="Seg_2248" s="T57">Там(?) в Тарко-Сале находились, когда-нибудь (его)? отправить.</ta>
            <ta e="T69" id="Seg_2249" s="T63">Потом перестали [его] судить.</ta>
            <ta e="T72" id="Seg_2250" s="T69">Наши слова (не стали слушать)?.</ta>
            <ta e="T85" id="Seg_2251" s="T72">Потом мы так, я, мы сказали: “Эй, он плохой человек, плохой человек.</ta>
            <ta e="T98" id="Seg_2252" s="T85">(Вы)? этого человека зачем отпустили, теперь он опять кого-нибудь побьёт”.</ta>
            <ta e="T105" id="Seg_2253" s="T98">Мы (сильно)? рассердились на Николая.</ta>
            <ta e="T114" id="Seg_2254" s="T105">Мы так думаем, его надо увезти и посадить.</ta>
            <ta e="T128" id="Seg_2255" s="T114">Мы, народ так решил, а судьи не хотят (и так решили)? потом.</ta>
            <ta e="T140" id="Seg_2256" s="T128">Люди в доме [=конторе] по-другому судили.</ta>
            <ta e="T146" id="Seg_2257" s="T140">Люди сильно рассердились на этого Николая.</ta>
            <ta e="T155" id="Seg_2258" s="T146">(Надо)? послать весточку, чтобы [его] посадили и чтобы они решили судить.</ta>
            <ta e="T159" id="Seg_2259" s="T155">Всё, потом перестали [судить].</ta>
            <ta e="T163" id="Seg_2260" s="T159">Потом и мы перестали.</ta>
            <ta e="T171" id="Seg_2261" s="T163">Сергей (Камин Сергей Данилович?) заявление подавал, Сергей у него двери/дом(?) порвал/поломал(?).</ta>
            <ta e="T186" id="Seg_2262" s="T171">Сергей двери/дом(?) порвал/сломал(?), два раза ночью залез ночью, Сергея двери/дом(?) (совсем)? порвал/сломал(?).</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T66" id="Seg_2263" s="T0">[KuAI:] Big.</ta>
            <ta e="T14" id="Seg_2264" s="T66">[NN2:] On this day… on this day two men were judged here in this bureau. </ta>
            <ta e="T20" id="Seg_2265" s="T14">Lyonya and Nikolaj (names in Selkup)</ta>
            <ta e="T29" id="Seg_2266" s="T20">One friend was given a fine in the village.</ta>
            <ta e="T36" id="Seg_2267" s="T29">The other friend was judged similarly. </ta>
            <ta e="T45" id="Seg_2268" s="T36">Then they stopped judging, then they did not judge.</ta>
            <ta e="T49" id="Seg_2269" s="T45">They put it in the next year. </ta>
            <ta e="T57" id="Seg_2270" s="T49">The clan believes that Nikolaj is the one who did the beating.</ta>
            <ta e="T63" id="Seg_2271" s="T57">They were in Tarko-Sale, when they let (him)? go.</ta>
            <ta e="T69" id="Seg_2272" s="T63">Then they ended the process. </ta>
            <ta e="T72" id="Seg_2273" s="T69">Our words (…)</ta>
            <ta e="T85" id="Seg_2274" s="T72">Then we, I, we say so: "Hey, he is a bad human, a bad human.</ta>
            <ta e="T98" id="Seg_2275" s="T85">(You)? set this man free, he will beat somebody again."</ta>
            <ta e="T105" id="Seg_2276" s="T98">We are (very)? angry at Nikolaj.</ta>
            <ta e="T114" id="Seg_2277" s="T105">We think, one must take him away and send him to jail.</ta>
            <ta e="T128" id="Seg_2278" s="T114">We, the people, have decided but the judges did not want that (and decided like this)?.</ta>
            <ta e="T140" id="Seg_2279" s="T128">The people in the house [=bureau] have jugded differently. </ta>
            <ta e="T146" id="Seg_2280" s="T140">The people are very angry at Nikolaj.</ta>
            <ta e="T155" id="Seg_2281" s="T146">We have to send a message, so he will be send to jail and so that they decide to judge like this. </ta>
            <ta e="T159" id="Seg_2282" s="T155">Then they ended the process. </ta>
            <ta e="T163" id="Seg_2283" s="T159">Then we stopped. </ta>
            <ta e="T171" id="Seg_2284" s="T163">Sergej (Kamin Sergej Danilovich) put in a request, Sergej has broken his door/house. </ta>
            <ta e="T186" id="Seg_2285" s="T171">Sergej has broken is door/house, two times he entered in the night, Sergejs door/house is broken.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T66" id="Seg_2286" s="T0">[KuAI:] Groß.</ta>
            <ta e="T14" id="Seg_2287" s="T66">[NN2:] An diesem Tag… An diesem Tag wurden zwei Menschen hier in diesem Büro verurteilt. </ta>
            <ta e="T20" id="Seg_2288" s="T14">Lyonya und Nikolaj (Namen auf Selkupisch)</ta>
            <ta e="T29" id="Seg_2289" s="T20">Ein Freund erhielt im Dorf eine Strafe.</ta>
            <ta e="T36" id="Seg_2290" s="T29">Der andere Freund wurde ähnlich bestraft. </ta>
            <ta e="T45" id="Seg_2291" s="T36">Dann hörten sie auf zu urteilen, dann urteilten sie nicht.</ta>
            <ta e="T49" id="Seg_2292" s="T45">Sie machten es im nächsten Jahr. </ta>
            <ta e="T57" id="Seg_2293" s="T49">Hier nimmt der Stamm an, dass Nikolaj derjenige ist, der geschlagen hat.</ta>
            <ta e="T63" id="Seg_2294" s="T57">Sie befanden sich in Tarko-Sale als sie (ihn)? gehen ließen. </ta>
            <ta e="T69" id="Seg_2295" s="T63">Dann beendeten sie den Prozess.</ta>
            <ta e="T72" id="Seg_2296" s="T69">Unsere Worte (…)</ta>
            <ta e="T85" id="Seg_2297" s="T72">Dann wir so, ich, wir sagen: "Hey, er ist ein schlechter Mensch, ein schlechter Mensch.</ta>
            <ta e="T98" id="Seg_2298" s="T85">(Ihr)? habt diesen Mann gehen lassen, jetzt wird er wieder jemanden schlagen."</ta>
            <ta e="T105" id="Seg_2299" s="T98">Wir sind (sehr)? wütend auf Nikolaj. </ta>
            <ta e="T114" id="Seg_2300" s="T105">Wir denken, man muss ihn wegbringen und ins Gefängnis sperren. </ta>
            <ta e="T128" id="Seg_2301" s="T114">Wir, das Volk, haben so entschieden, aber die Richter haben es dann nicht gewollt (und haben so entschieden)?. </ta>
            <ta e="T140" id="Seg_2302" s="T128">Die Leute im Haus [=Büro] haben anders geurteilt.</ta>
            <ta e="T146" id="Seg_2303" s="T140">Die Leute sind sehr wütend auf diesen Nikolaj. </ta>
            <ta e="T155" id="Seg_2304" s="T146">Wir müssen eine Nachricht schicken, sodass [er] ins Gefängnis gesperrt wird und sodass sie entscheiden so zu urteilen. </ta>
            <ta e="T159" id="Seg_2305" s="T155">Dann haben sie den Prozess beendet. </ta>
            <ta e="T163" id="Seg_2306" s="T159">Dann hörten wir auf. </ta>
            <ta e="T171" id="Seg_2307" s="T163">Sergej (Kamin Sergej Danilovich) hat einen Antrag eingereicht, Sergej hat seine Tür/Haus(?) zerbrochen.</ta>
            <ta e="T186" id="Seg_2308" s="T171">Sergej hat seine Tür/Haus zerbrochen, zweimal kam er in der Nacht hinein, Sergejs Tür/Haus ist zerbrochen.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T66" id="Seg_2309" s="T0">большой </ta>
            <ta e="T14" id="Seg_2310" s="T66">в этот день … в этот день собрались двух друзей судили в конторе</ta>
            <ta e="T20" id="Seg_2311" s="T14">Лёню и Николая (имя по-селькупски)</ta>
            <ta e="T29" id="Seg_2312" s="T20">одному другу дали в посёлке штраф (=положили)</ta>
            <ta e="T36" id="Seg_2313" s="T29">одного друга судить стали</ta>
            <ta e="T45" id="Seg_2314" s="T36">потом перестали судить потом не стали судить</ta>
            <ta e="T49" id="Seg_2315" s="T45">на другой год оставили</ta>
            <ta e="T57" id="Seg_2316" s="T49">народ предположил что Николай битый</ta>
            <ta e="T63" id="Seg_2317" s="T57">там в Тарко-Сале останется когда-нибудь его отправят</ta>
            <ta e="T69" id="Seg_2318" s="T63">потом перестали судить</ta>
            <ta e="T72" id="Seg_2319" s="T69">наши слова (не стали слушать?)</ta>
            <ta e="T85" id="Seg_2320" s="T72">потом мы я мы сказали плохой он человек</ta>
            <ta e="T98" id="Seg_2321" s="T85">вы зачем его отпустили он потом опять кого-нибудь побьёт </ta>
            <ta e="T105" id="Seg_2322" s="T98">мы сильно рассердились на Николая</ta>
            <ta e="T114" id="Seg_2323" s="T105">мы так думаем его надо увезти и посадят</ta>
            <ta e="T128" id="Seg_2324" s="T114">мы народ так решил а судьи не хотят и так решили</ta>
            <ta e="T140" id="Seg_2325" s="T128">люди в доме (=конторе) по-другому судили </ta>
            <ta e="T146" id="Seg_2326" s="T140">люди сильно рассердилисьна Николая</ta>
            <ta e="T155" id="Seg_2327" s="T146">надо послать весточку чтобы его посадили и решили судить</ta>
            <ta e="T159" id="Seg_2328" s="T155">всё наверное перестали судить</ta>
            <ta e="T163" id="Seg_2329" s="T159">потом и мы перестали</ta>
            <ta e="T171" id="Seg_2330" s="T163">(Камин Сергей Данилович?) заявление давал у него дом (двери?) поломал </ta>
            <ta e="T186" id="Seg_2331" s="T171">Сергей двери сломал два раза ночью залез Сергей совсем сломал</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T66" id="Seg_2332" s="T0">[BrM:] A part from the previous conversation.</ta>
            <ta e="T29" id="Seg_2333" s="T20">[OSV:] Tentative transcription of 'posjolkaqɨn'.</ta>
            <ta e="T45" id="Seg_2334" s="T36">[BrM:] Tentative transcription.</ta>
            <ta e="T63" id="Seg_2335" s="T57">[BrM:] Tentative transcription and analysis.</ta>
            <ta e="T98" id="Seg_2336" s="T85">[BrM:] Tentative transcription.</ta>
            <ta e="T140" id="Seg_2337" s="T128">[BrM:] Tentative transcription and analysis.</ta>
            <ta e="T159" id="Seg_2338" s="T155">[BrM:] Tentative transcription and analysis.</ta>
            <ta e="T186" id="Seg_2339" s="T171">[BrM:] Tentative transcription.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T66" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
