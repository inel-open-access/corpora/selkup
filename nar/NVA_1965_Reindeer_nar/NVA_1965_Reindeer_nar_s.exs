<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>NVA_1965_Reindeer_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">NVA_1965_Reindeer_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">152</ud-information>
            <ud-information attribute-name="# HIAT:w">119</ud-information>
            <ud-information attribute-name="# e">119</ud-information>
            <ud-information attribute-name="# HIAT:u">25</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="NVA">
            <abbreviation>NVA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="NVA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T120" id="Seg_0" n="sc" s="T0">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Mat</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">täːle</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">söttɨ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">ɔːtatqä</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">mantalpɨlʼa</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">koranɛntak</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_23" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">Qarɨt</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">ınna</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">mesila</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">čʼoːip</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">ütɨŋɨtɨ</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_41" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">Nɨːnɨ</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">peːmelʼ</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">tɔːktɨ</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">seːrnɨtɨ</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_56" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">Nɨːnɨ</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">ınna</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">nɨlajila</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">mösɨla</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">pona</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_72" n="HIAT:ip">[</nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">potäna</ts>
                  <nts id="Seg_75" n="HIAT:ip">]</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_78" n="HIAT:w" s="T21">tanta</ts>
                  <nts id="Seg_79" n="HIAT:ip">.</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_82" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_84" n="HIAT:w" s="T23">Kanaktɨ</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_87" n="HIAT:w" s="T24">laŋtɨntɨ</ts>
                  <nts id="Seg_88" n="HIAT:ip">:</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_91" n="HIAT:w" s="T25">Pɨrr</ts>
                  <nts id="Seg_92" n="HIAT:ip">,</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_95" n="HIAT:w" s="T26">ɔːtä</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_98" n="HIAT:w" s="T27">ıllä</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_101" n="HIAT:w" s="T28">tɔːqqatɨ</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_105" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">Kanak</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">ɔːtaiːmtɨ</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">tɔːqqɨkkɨitɨ</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_117" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">Täp</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">tɛːmtiti</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">iːtät</ts>
                  <nts id="Seg_126" n="HIAT:ip">,</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_129" n="HIAT:w" s="T35">ɔːtalʼ</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_132" n="HIAT:w" s="T36">uratoqɨntoːqan</ts>
                  <nts id="Seg_133" n="HIAT:ip">.</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_136" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_138" n="HIAT:w" s="T37">Ɔːtamtɨ</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_141" n="HIAT:w" s="T38">orqɨlnɨtɨ</ts>
                  <nts id="Seg_142" n="HIAT:ip">,</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_145" n="HIAT:w" s="T39">nɨːnä</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_148" n="HIAT:w" s="T40">qaqɨlɨmtɨ</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_151" n="HIAT:w" s="T41">saralnɨtɨ</ts>
                  <nts id="Seg_152" n="HIAT:ip">.</nts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_155" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_157" n="HIAT:w" s="T42">Nɨːnä</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_160" n="HIAT:w" s="T43">šöttə</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_163" n="HIAT:w" s="T44">laqaltɛlʼčʼa</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_166" n="HIAT:w" s="T45">nʼoːtɨrlʼa</ts>
                  <nts id="Seg_167" n="HIAT:ip">.</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_170" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_172" n="HIAT:w" s="T46">Šittə</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_175" n="HIAT:w" s="T47">nʼarɨt</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_178" n="HIAT:w" s="T48">toː</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_181" n="HIAT:w" s="T49">puːkka</ts>
                  <nts id="Seg_182" n="HIAT:ip">.</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_185" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_187" n="HIAT:w" s="T50">Mačʼit</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_190" n="HIAT:w" s="T51">ɔːtat</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_193" n="HIAT:w" s="T52">wättɨ</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_196" n="HIAT:w" s="T53">qontɨtɨ</ts>
                  <nts id="Seg_197" n="HIAT:ip">.</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_200" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_202" n="HIAT:w" s="T54">Nɨːnɨ</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_205" n="HIAT:w" s="T55">nʼoːtɨkkɨtɨ</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_208" n="HIAT:w" s="T56">olʼeqatqɨt</ts>
                  <nts id="Seg_209" n="HIAT:ip">.</nts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_212" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_214" n="HIAT:w" s="T57">Ɔːtat</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_217" n="HIAT:w" s="T58">kuːnɨntɔːtɨt</ts>
                  <nts id="Seg_218" n="HIAT:ip">.</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_221" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_223" n="HIAT:w" s="T59">Мat</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_226" n="HIAT:w" s="T60">qɨntalʼap</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_229" n="HIAT:w" s="T61">čʼatɨqunaːqa</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_232" n="HIAT:w" s="T62">mɛrkɨt</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_235" n="HIAT:w" s="T63">ɔːrtɨn</ts>
                  <nts id="Seg_236" n="HIAT:ip">.</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_239" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_241" n="HIAT:w" s="T64">Nɨːnä</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_244" n="HIAT:w" s="T65">čʼattɨsap</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_247" n="HIAT:w" s="T66">ukkur</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_250" n="HIAT:w" s="T67">pɔːr</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_253" n="HIAT:w" s="T68">šʼittɨ</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_256" n="HIAT:w" s="T69">quːrop</ts>
                  <nts id="Seg_257" n="HIAT:ip">.</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_260" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_262" n="HIAT:w" s="T70">Ɔːma</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_265" n="HIAT:w" s="T71">ɔːtat</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_268" n="HIAT:w" s="T72">nɨːnɨ</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_271" n="HIAT:w" s="T73">paktɨsɔːtɨt</ts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_275" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_277" n="HIAT:w" s="T74">Tap</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_280" n="HIAT:w" s="T75">ɔːtamtɨ</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_283" n="HIAT:w" s="T76">kırɨkkɨtɨ</ts>
                  <nts id="Seg_284" n="HIAT:ip">,</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_287" n="HIAT:w" s="T77">čʼoːtɨqopɨp</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_290" n="HIAT:w" s="T78">toː</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_293" n="HIAT:w" s="T79">parqɨlnɨtɨ</ts>
                  <nts id="Seg_294" n="HIAT:ip">.</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_297" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_299" n="HIAT:w" s="T80">Nɨːnɨ</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_302" n="HIAT:w" s="T81">kəmtɨ</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_305" n="HIAT:w" s="T82">nɨː</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_308" n="HIAT:w" s="T83">qamtɨkkɨt</ts>
                  <nts id="Seg_309" n="HIAT:ip">.</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_312" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_314" n="HIAT:w" s="T84">Ɔːtantɨ</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_317" n="HIAT:w" s="T85">ukkur</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_320" n="HIAT:w" s="T86">močʼimtɨ</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_323" n="HIAT:w" s="T87">sɨrantɨ</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_326" n="HIAT:w" s="T88">taqɨnɨt</ts>
                  <nts id="Seg_327" n="HIAT:ip">.</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_330" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_332" n="HIAT:w" s="T89">Ukkur</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_335" n="HIAT:w" s="T90">nʼeːmtɨ</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_338" n="HIAT:w" s="T91">qaqlaqɨntɨ</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_341" n="HIAT:w" s="T92">ınna</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_344" n="HIAT:w" s="T93">tɛltɨkkɨt</ts>
                  <nts id="Seg_345" n="HIAT:ip">.</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_348" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_350" n="HIAT:w" s="T94">Nɨːnä</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_353" n="HIAT:w" s="T95">moqonä</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_356" n="HIAT:w" s="T96">laqaltɛlʼčʼa</ts>
                  <nts id="Seg_357" n="HIAT:ip">.</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_360" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_362" n="HIAT:w" s="T97">Üːtät</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_365" n="HIAT:w" s="T98">mɔːtqɨntä</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_368" n="HIAT:w" s="T99">tülʼčʼe</ts>
                  <nts id="Seg_369" n="HIAT:ip">.</nts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_372" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_374" n="HIAT:w" s="T100">Ɔːtantɨ</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_377" n="HIAT:w" s="T101">qümɨlʼ</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_380" n="HIAT:w" s="T102">lʼäj</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_383" n="HIAT:w" s="T103">mɨntɨ</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_386" n="HIAT:w" s="T104">mɔːttɨ</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_389" n="HIAT:w" s="T105">tultokkɨt</ts>
                  <nts id="Seg_390" n="HIAT:ip">.</nts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_393" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_395" n="HIAT:w" s="T106">Imatɨ</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_398" n="HIAT:w" s="T107">ɔːtantɨ</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_401" n="HIAT:w" s="T108">poːntɨ</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_404" n="HIAT:w" s="T109">mɨp</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_407" n="HIAT:w" s="T110">toː</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_410" n="HIAT:w" s="T111">kıralqolamnɨt</ts>
                  <nts id="Seg_411" n="HIAT:ip">.</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_414" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_416" n="HIAT:w" s="T112">Ɔːtanta</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_419" n="HIAT:w" s="T113">poːntɨlʼ</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_422" n="HIAT:w" s="T114">mɨp</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_425" n="HIAT:w" s="T115">kıralʼa</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_428" n="HIAT:w" s="T116">puːt</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_431" n="HIAT:w" s="T117">pona</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_434" n="HIAT:w" s="T118">tattɨkkɨt</ts>
                  <nts id="Seg_435" n="HIAT:ip">,</nts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_438" n="HIAT:w" s="T119">teːkɨrɨqɨntoːqa</ts>
                  <nts id="Seg_439" n="HIAT:ip">.</nts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T120" id="Seg_441" n="sc" s="T0">
               <ts e="T1" id="Seg_443" n="e" s="T0">Mat </ts>
               <ts e="T2" id="Seg_445" n="e" s="T1">täːle </ts>
               <ts e="T3" id="Seg_447" n="e" s="T2">söttɨ </ts>
               <ts e="T4" id="Seg_449" n="e" s="T3">ɔːtatqä </ts>
               <ts e="T5" id="Seg_451" n="e" s="T4">mantalpɨlʼa </ts>
               <ts e="T6" id="Seg_453" n="e" s="T5">koranɛntak. </ts>
               <ts e="T7" id="Seg_455" n="e" s="T6">Qarɨt </ts>
               <ts e="T8" id="Seg_457" n="e" s="T7">ınna </ts>
               <ts e="T9" id="Seg_459" n="e" s="T8">mesila </ts>
               <ts e="T10" id="Seg_461" n="e" s="T9">čʼoːip </ts>
               <ts e="T11" id="Seg_463" n="e" s="T10">ütɨŋɨtɨ. </ts>
               <ts e="T12" id="Seg_465" n="e" s="T11">Nɨːnɨ </ts>
               <ts e="T13" id="Seg_467" n="e" s="T12">peːmelʼ </ts>
               <ts e="T14" id="Seg_469" n="e" s="T13">tɔːktɨ </ts>
               <ts e="T15" id="Seg_471" n="e" s="T14">seːrnɨtɨ. </ts>
               <ts e="T16" id="Seg_473" n="e" s="T15">Nɨːnɨ </ts>
               <ts e="T17" id="Seg_475" n="e" s="T16">ınna </ts>
               <ts e="T18" id="Seg_477" n="e" s="T17">nɨlajila </ts>
               <ts e="T19" id="Seg_479" n="e" s="T18">mösɨla </ts>
               <ts e="T20" id="Seg_481" n="e" s="T19">pona </ts>
               <ts e="T21" id="Seg_483" n="e" s="T20">[potäna] </ts>
               <ts e="T23" id="Seg_485" n="e" s="T21">tanta. </ts>
               <ts e="T24" id="Seg_487" n="e" s="T23">Kanaktɨ </ts>
               <ts e="T25" id="Seg_489" n="e" s="T24">laŋtɨntɨ: </ts>
               <ts e="T26" id="Seg_491" n="e" s="T25">Pɨrr, </ts>
               <ts e="T27" id="Seg_493" n="e" s="T26">ɔːtä </ts>
               <ts e="T28" id="Seg_495" n="e" s="T27">ıllä </ts>
               <ts e="T29" id="Seg_497" n="e" s="T28">tɔːqqatɨ. </ts>
               <ts e="T30" id="Seg_499" n="e" s="T29">Kanak </ts>
               <ts e="T31" id="Seg_501" n="e" s="T30">ɔːtaiːmtɨ </ts>
               <ts e="T32" id="Seg_503" n="e" s="T31">tɔːqqɨkkɨitɨ. </ts>
               <ts e="T33" id="Seg_505" n="e" s="T32">Täp </ts>
               <ts e="T34" id="Seg_507" n="e" s="T33">tɛːmtiti </ts>
               <ts e="T35" id="Seg_509" n="e" s="T34">iːtät, </ts>
               <ts e="T36" id="Seg_511" n="e" s="T35">ɔːtalʼ </ts>
               <ts e="T37" id="Seg_513" n="e" s="T36">uratoqɨntoːqan. </ts>
               <ts e="T38" id="Seg_515" n="e" s="T37">Ɔːtamtɨ </ts>
               <ts e="T39" id="Seg_517" n="e" s="T38">orqɨlnɨtɨ, </ts>
               <ts e="T40" id="Seg_519" n="e" s="T39">nɨːnä </ts>
               <ts e="T41" id="Seg_521" n="e" s="T40">qaqɨlɨmtɨ </ts>
               <ts e="T42" id="Seg_523" n="e" s="T41">saralnɨtɨ. </ts>
               <ts e="T43" id="Seg_525" n="e" s="T42">Nɨːnä </ts>
               <ts e="T44" id="Seg_527" n="e" s="T43">šöttə </ts>
               <ts e="T45" id="Seg_529" n="e" s="T44">laqaltɛlʼčʼa </ts>
               <ts e="T46" id="Seg_531" n="e" s="T45">nʼoːtɨrlʼa. </ts>
               <ts e="T47" id="Seg_533" n="e" s="T46">Šittə </ts>
               <ts e="T48" id="Seg_535" n="e" s="T47">nʼarɨt </ts>
               <ts e="T49" id="Seg_537" n="e" s="T48">toː </ts>
               <ts e="T50" id="Seg_539" n="e" s="T49">puːkka. </ts>
               <ts e="T51" id="Seg_541" n="e" s="T50">Mačʼit </ts>
               <ts e="T52" id="Seg_543" n="e" s="T51">ɔːtat </ts>
               <ts e="T53" id="Seg_545" n="e" s="T52">wättɨ </ts>
               <ts e="T54" id="Seg_547" n="e" s="T53">qontɨtɨ. </ts>
               <ts e="T55" id="Seg_549" n="e" s="T54">Nɨːnɨ </ts>
               <ts e="T56" id="Seg_551" n="e" s="T55">nʼoːtɨkkɨtɨ </ts>
               <ts e="T57" id="Seg_553" n="e" s="T56">olʼeqatqɨt. </ts>
               <ts e="T58" id="Seg_555" n="e" s="T57">Ɔːtat </ts>
               <ts e="T59" id="Seg_557" n="e" s="T58">kuːnɨntɔːtɨt. </ts>
               <ts e="T60" id="Seg_559" n="e" s="T59">Мat </ts>
               <ts e="T61" id="Seg_561" n="e" s="T60">qɨntalʼap </ts>
               <ts e="T62" id="Seg_563" n="e" s="T61">čʼatɨqunaːqa </ts>
               <ts e="T63" id="Seg_565" n="e" s="T62">mɛrkɨt </ts>
               <ts e="T64" id="Seg_567" n="e" s="T63">ɔːrtɨn. </ts>
               <ts e="T65" id="Seg_569" n="e" s="T64">Nɨːnä </ts>
               <ts e="T66" id="Seg_571" n="e" s="T65">čʼattɨsap </ts>
               <ts e="T67" id="Seg_573" n="e" s="T66">ukkur </ts>
               <ts e="T68" id="Seg_575" n="e" s="T67">pɔːr </ts>
               <ts e="T69" id="Seg_577" n="e" s="T68">šʼittɨ </ts>
               <ts e="T70" id="Seg_579" n="e" s="T69">quːrop. </ts>
               <ts e="T71" id="Seg_581" n="e" s="T70">Ɔːma </ts>
               <ts e="T72" id="Seg_583" n="e" s="T71">ɔːtat </ts>
               <ts e="T73" id="Seg_585" n="e" s="T72">nɨːnɨ </ts>
               <ts e="T74" id="Seg_587" n="e" s="T73">paktɨsɔːtɨt. </ts>
               <ts e="T75" id="Seg_589" n="e" s="T74">Tap </ts>
               <ts e="T76" id="Seg_591" n="e" s="T75">ɔːtamtɨ </ts>
               <ts e="T77" id="Seg_593" n="e" s="T76">kırɨkkɨtɨ, </ts>
               <ts e="T78" id="Seg_595" n="e" s="T77">čʼoːtɨqopɨp </ts>
               <ts e="T79" id="Seg_597" n="e" s="T78">toː </ts>
               <ts e="T80" id="Seg_599" n="e" s="T79">parqɨlnɨtɨ. </ts>
               <ts e="T81" id="Seg_601" n="e" s="T80">Nɨːnɨ </ts>
               <ts e="T82" id="Seg_603" n="e" s="T81">kəmtɨ </ts>
               <ts e="T83" id="Seg_605" n="e" s="T82">nɨː </ts>
               <ts e="T84" id="Seg_607" n="e" s="T83">qamtɨkkɨt. </ts>
               <ts e="T85" id="Seg_609" n="e" s="T84">Ɔːtantɨ </ts>
               <ts e="T86" id="Seg_611" n="e" s="T85">ukkur </ts>
               <ts e="T87" id="Seg_613" n="e" s="T86">močʼimtɨ </ts>
               <ts e="T88" id="Seg_615" n="e" s="T87">sɨrantɨ </ts>
               <ts e="T89" id="Seg_617" n="e" s="T88">taqɨnɨt. </ts>
               <ts e="T90" id="Seg_619" n="e" s="T89">Ukkur </ts>
               <ts e="T91" id="Seg_621" n="e" s="T90">nʼeːmtɨ </ts>
               <ts e="T92" id="Seg_623" n="e" s="T91">qaqlaqɨntɨ </ts>
               <ts e="T93" id="Seg_625" n="e" s="T92">ınna </ts>
               <ts e="T94" id="Seg_627" n="e" s="T93">tɛltɨkkɨt. </ts>
               <ts e="T95" id="Seg_629" n="e" s="T94">Nɨːnä </ts>
               <ts e="T96" id="Seg_631" n="e" s="T95">moqonä </ts>
               <ts e="T97" id="Seg_633" n="e" s="T96">laqaltɛlʼčʼa. </ts>
               <ts e="T98" id="Seg_635" n="e" s="T97">Üːtät </ts>
               <ts e="T99" id="Seg_637" n="e" s="T98">mɔːtqɨntä </ts>
               <ts e="T100" id="Seg_639" n="e" s="T99">tülʼčʼe. </ts>
               <ts e="T101" id="Seg_641" n="e" s="T100">Ɔːtantɨ </ts>
               <ts e="T102" id="Seg_643" n="e" s="T101">qümɨlʼ </ts>
               <ts e="T103" id="Seg_645" n="e" s="T102">lʼäj </ts>
               <ts e="T104" id="Seg_647" n="e" s="T103">mɨntɨ </ts>
               <ts e="T105" id="Seg_649" n="e" s="T104">mɔːttɨ </ts>
               <ts e="T106" id="Seg_651" n="e" s="T105">tultokkɨt. </ts>
               <ts e="T107" id="Seg_653" n="e" s="T106">Imatɨ </ts>
               <ts e="T108" id="Seg_655" n="e" s="T107">ɔːtantɨ </ts>
               <ts e="T109" id="Seg_657" n="e" s="T108">poːntɨ </ts>
               <ts e="T110" id="Seg_659" n="e" s="T109">mɨp </ts>
               <ts e="T111" id="Seg_661" n="e" s="T110">toː </ts>
               <ts e="T112" id="Seg_663" n="e" s="T111">kıralqolamnɨt. </ts>
               <ts e="T113" id="Seg_665" n="e" s="T112">Ɔːtanta </ts>
               <ts e="T114" id="Seg_667" n="e" s="T113">poːntɨlʼ </ts>
               <ts e="T115" id="Seg_669" n="e" s="T114">mɨp </ts>
               <ts e="T116" id="Seg_671" n="e" s="T115">kıralʼa </ts>
               <ts e="T117" id="Seg_673" n="e" s="T116">puːt </ts>
               <ts e="T118" id="Seg_675" n="e" s="T117">pona </ts>
               <ts e="T119" id="Seg_677" n="e" s="T118">tattɨkkɨt, </ts>
               <ts e="T120" id="Seg_679" n="e" s="T119">teːkɨrɨqɨntoːqa. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_680" s="T0">NVA_1965_Reindeer_nar.001 (001.001)</ta>
            <ta e="T11" id="Seg_681" s="T6">NVA_1965_Reindeer_nar.002 (001.002)</ta>
            <ta e="T15" id="Seg_682" s="T11">NVA_1965_Reindeer_nar.003 (001.003)</ta>
            <ta e="T23" id="Seg_683" s="T15">NVA_1965_Reindeer_nar.004 (001.004)</ta>
            <ta e="T29" id="Seg_684" s="T23">NVA_1965_Reindeer_nar.005 (001.005)</ta>
            <ta e="T32" id="Seg_685" s="T29">NVA_1965_Reindeer_nar.006 (001.006)</ta>
            <ta e="T37" id="Seg_686" s="T32">NVA_1965_Reindeer_nar.007 (001.007)</ta>
            <ta e="T42" id="Seg_687" s="T37">NVA_1965_Reindeer_nar.008 (001.008)</ta>
            <ta e="T46" id="Seg_688" s="T42">NVA_1965_Reindeer_nar.009 (001.009)</ta>
            <ta e="T50" id="Seg_689" s="T46">NVA_1965_Reindeer_nar.010 (001.010)</ta>
            <ta e="T54" id="Seg_690" s="T50">NVA_1965_Reindeer_nar.011 (001.011)</ta>
            <ta e="T57" id="Seg_691" s="T54">NVA_1965_Reindeer_nar.012 (001.012)</ta>
            <ta e="T59" id="Seg_692" s="T57">NVA_1965_Reindeer_nar.013 (001.013)</ta>
            <ta e="T64" id="Seg_693" s="T59">NVA_1965_Reindeer_nar.014 (001.014)</ta>
            <ta e="T70" id="Seg_694" s="T64">NVA_1965_Reindeer_nar.015 (001.015)</ta>
            <ta e="T74" id="Seg_695" s="T70">NVA_1965_Reindeer_nar.016 (001.016)</ta>
            <ta e="T80" id="Seg_696" s="T74">NVA_1965_Reindeer_nar.017 (001.017)</ta>
            <ta e="T84" id="Seg_697" s="T80">NVA_1965_Reindeer_nar.018 (001.018)</ta>
            <ta e="T89" id="Seg_698" s="T84">NVA_1965_Reindeer_nar.019 (001.019)</ta>
            <ta e="T94" id="Seg_699" s="T89">NVA_1965_Reindeer_nar.020 (001.020)</ta>
            <ta e="T97" id="Seg_700" s="T94">NVA_1965_Reindeer_nar.021 (001.021)</ta>
            <ta e="T100" id="Seg_701" s="T97">NVA_1965_Reindeer_nar.022 (001.022)</ta>
            <ta e="T106" id="Seg_702" s="T100">NVA_1965_Reindeer_nar.023 (001.023)</ta>
            <ta e="T112" id="Seg_703" s="T106">NVA_1965_Reindeer_nar.024 (001.024)</ta>
            <ta e="T120" id="Seg_704" s="T112">NVA_1965_Reindeer_nar.025 (001.025)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_705" s="T0">мат ′тӓ̄lе ′сӧтты ′о̨̄татkӓ ′мандаlпылʼа ′kоранентак.</ta>
            <ta e="T11" id="Seg_706" s="T6">′kарыт ′инна ′месʼила ′чо̄ип ′ӱтыңыты.</ta>
            <ta e="T15" id="Seg_707" s="T11">ныны ′пе̄мелтоkты ′се̄рныты.</ta>
            <ta e="T23" id="Seg_708" s="T15">ныны ′инна ′нылʼай и′lа мӧсыла ′пона [′потӓна] ′танта.</ta>
            <ta e="T29" id="Seg_709" s="T23">′канакты ′лаңтынты ′пырр ′о̨тӓ илʼлʼе ′тоkkаты.</ta>
            <ta e="T32" id="Seg_710" s="T29">′канак ′отаимты ′тоkkыɣыиты.</ta>
            <ta e="T37" id="Seg_711" s="T32">тӓп ′тӓмтити ′ӣтӓт, ′о̨̄таl у′ратоɣынтоɣан.</ta>
            <ta e="T42" id="Seg_712" s="T37">′о̨̄тамты ′орkыlныты ′нынӓ ′kаɣылымты ′са̄раlныты.</ta>
            <ta e="T46" id="Seg_713" s="T42">′нынӓ с̌[сʼ]ӧттъ lаkалтелʼд͡жа ′нʼӧ̄тырлʼа.</ta>
            <ta e="T50" id="Seg_714" s="T46">′шʼиттъ ′нʼарыт то̄ ′пӯɣа.</ta>
            <ta e="T54" id="Seg_715" s="T50">′ма̄чит ′о̨тат ′вӓтты ′kонтыты.</ta>
            <ta e="T57" id="Seg_716" s="T54">ныны нʼӧ̄′тыɣыты ′олʼеkат′kыт.</ta>
            <ta e="T59" id="Seg_717" s="T57">о̨̄тат ′kуннынтотыт.</ta>
            <ta e="T64" id="Seg_718" s="T59">Мат ′kынталʼап ′ча̄тыkуна‵kа ′мӓрkыт ′ортын.</ta>
            <ta e="T70" id="Seg_719" s="T64">′нынӓ ′чаттысап ук′кур пор ′шʼитты ′kӯроп.</ta>
            <ta e="T74" id="Seg_720" s="T70">′о̨ма ′о̨̄тат ′ныны паkты′со̄тыт.</ta>
            <ta e="T80" id="Seg_721" s="T74">та[ӓ]п ′о̨̄тамты ′кӣрыɣыты ′чо̄тыkопып, то̄ ′парГылныты.</ta>
            <ta e="T84" id="Seg_722" s="T80">′ныны ′kо[ъ̊]мты ны ′kамдыгыт.</ta>
            <ta e="T89" id="Seg_723" s="T84">′о̨̄танты ′уккур ′мочисты ′сыранты ′таkыныт.</ta>
            <ta e="T94" id="Seg_724" s="T89">′уккур ′нʼемты ′kагла‵kынты ′инна ′теlдыɣыт.</ta>
            <ta e="T97" id="Seg_725" s="T94">′нынӓ моkонӓ ‵lа̄kаl′теlча.</ta>
            <ta e="T100" id="Seg_726" s="T97">′ӱ̄тӓт ′мо̄тkынтӓ ′тӱlче.</ta>
            <ta e="T106" id="Seg_727" s="T100">′о̨̄танты ′кӱмыl лʼӓймынты ′мо̄тты тулʼдоɣыт.</ta>
            <ta e="T112" id="Seg_728" s="T106">′ӣматы ′о̨танты ′понтымып то̄ ′кираl ′kоlамныт.</ta>
            <ta e="T120" id="Seg_729" s="T112">′о̨̄танта ′пондылмып ′кӣралʼа пӯт ′пона′таттыɣыт, ′те̨кырыkын′тока.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_730" s="T0">mat täːlʼe söttɨ oːtatqä mandalʼpɨlʼa qoranentak.</ta>
            <ta e="T11" id="Seg_731" s="T6">qarɨt inna mesʼila čʼoːip ütɨŋɨtɨ.</ta>
            <ta e="T15" id="Seg_732" s="T11">nɨnɨ peːmeltoqtɨ seːrnɨtɨ.</ta>
            <ta e="T23" id="Seg_733" s="T15">nɨnɨ inna nɨlʼaj ilʼa mösɨla pona [potäna] tanta.</ta>
            <ta e="T29" id="Seg_734" s="T23">kanaktɨ laŋtɨntɨ pɨrr otä ilʼlʼe toqqatɨ.</ta>
            <ta e="T32" id="Seg_735" s="T29">kanak otaimtɨ toqqɨqɨitɨ.</ta>
            <ta e="T37" id="Seg_736" s="T32">täp tämtiti iːtät, oːtalʼ uratoqɨntoqan.</ta>
            <ta e="T42" id="Seg_737" s="T37">oːtamtɨ orqɨlʼnɨtɨ nɨnä qaqɨlɨmtɨ saːralʼnɨtɨ.</ta>
            <ta e="T46" id="Seg_738" s="T42">nɨnä š [sʼ] öttə lʼaqaltelʼd͡жa nʼöːtɨrlʼa.</ta>
            <ta e="T50" id="Seg_739" s="T46">šittə nʼarɨt toː puːqa. </ta>
            <ta e="T54" id="Seg_740" s="T50">maːčʼit otat vättɨ qontɨtɨ.</ta>
            <ta e="T57" id="Seg_741" s="T54">nɨnɨ nʼöːtɨqɨtɨ olʼeqatqɨt. </ta>
            <ta e="T59" id="Seg_742" s="T57">oːtat qunnɨntotɨt. </ta>
            <ta e="T64" id="Seg_743" s="T59">Мat qɨntalʼap čʼaːtɨqunaqa märqɨt ortɨn. </ta>
            <ta e="T70" id="Seg_744" s="T64">nɨnä čʼattɨsap ukkur por šʼittɨ quːrop. </ta>
            <ta e="T74" id="Seg_745" s="T70">oma oːtat nɨnɨ paqtɨsoːtɨt. </ta>
            <ta e="T80" id="Seg_746" s="T74">ta [ä] p oːtamtɨ kiːrɨqɨtɨ čʼoːtɨqopɨp, toː parГɨlnɨtɨ. </ta>
            <ta e="T84" id="Seg_747" s="T80">nɨnɨ qo [ə] mtɨ nɨ qamdɨgɨt. </ta>
            <ta e="T89" id="Seg_748" s="T84">oːtantɨ ukkur močʼistɨ sɨrantɨ taqɨnɨt. </ta>
            <ta e="T94" id="Seg_749" s="T89">ukkur nʼemtɨ qaglaqɨntɨ inna telʼdɨqɨt. </ta>
            <ta e="T97" id="Seg_750" s="T94">nɨnä moqonä lʼaːqalʼtelʼčʼa. </ta>
            <ta e="T100" id="Seg_751" s="T97">üːtät moːtqɨntä tülʼčʼe. </ta>
            <ta e="T106" id="Seg_752" s="T100">oːtantɨ kümɨlʼ lʼäjmɨntɨ moːttɨ tulʼdoqɨt.</ta>
            <ta e="T112" id="Seg_753" s="T106">iːmatɨ otantɨ pontɨmɨp toː kiralʼ qolʼamnɨt. </ta>
            <ta e="T120" id="Seg_754" s="T112">oːtanta pondɨlmɨp kiːralʼa puːt ponatattɨqɨt, tekɨrɨqɨntoka.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_755" s="T0">Mat täːle söttɨ ɔːtatqä mantalpɨlʼa koranɛntak. </ta>
            <ta e="T11" id="Seg_756" s="T6">Qarɨt ınna mesila čʼoːip ütɨŋɨtɨ. </ta>
            <ta e="T15" id="Seg_757" s="T11">Nɨːnɨ peːmelʼ tɔːktɨ seːrnɨtɨ. </ta>
            <ta e="T23" id="Seg_758" s="T15">Nɨːnɨ ınna nɨlajila mösɨla pona [potäna] tanta. </ta>
            <ta e="T29" id="Seg_759" s="T23">Kanaktɨ laŋtɨntɨ: Pɨrr, ɔːtä ıllä tɔːqqatɨ. </ta>
            <ta e="T32" id="Seg_760" s="T29">Kanak ɔːtaiːmtɨ tɔːqqɨkkɨitɨ. </ta>
            <ta e="T37" id="Seg_761" s="T32">Täp tɛːmtiti iːtät, ɔːtalʼ uratoqɨntoːqan. </ta>
            <ta e="T42" id="Seg_762" s="T37">Ɔːtamtɨ orqɨlnɨtɨ, nɨːnä qaqɨlɨmtɨ saralnɨtɨ. </ta>
            <ta e="T46" id="Seg_763" s="T42">Nɨːnä šöttə laqaltɛlʼčʼa nʼoːtɨrlʼa. </ta>
            <ta e="T50" id="Seg_764" s="T46">Šittə nʼarɨt toː puːkka. </ta>
            <ta e="T54" id="Seg_765" s="T50">Mačʼit ɔːtat wättɨ qontɨtɨ. </ta>
            <ta e="T57" id="Seg_766" s="T54">Nɨːnɨ nʼoːtɨkkɨtɨ olʼeqatqɨt. </ta>
            <ta e="T59" id="Seg_767" s="T57">Ɔːtat kuːnɨntɔːtɨt. </ta>
            <ta e="T64" id="Seg_768" s="T59">Мat qɨntalʼap čʼatɨqunaːqa mɛrkɨt ɔːrtɨn. </ta>
            <ta e="T70" id="Seg_769" s="T64">Nɨːnä čʼattɨsap ukkur pɔːr šʼittɨ quːrop. </ta>
            <ta e="T74" id="Seg_770" s="T70">Ɔːma ɔːtat nɨːnɨ paktɨsɔːtɨt. </ta>
            <ta e="T80" id="Seg_771" s="T74">Tap ɔːtamtɨ kırɨkkɨtɨ, čʼoːtɨqopɨp toː parqɨlnɨtɨ. </ta>
            <ta e="T84" id="Seg_772" s="T80">Nɨːnɨ kəmtɨ nɨː qamtɨkkɨt. </ta>
            <ta e="T89" id="Seg_773" s="T84">Ɔːtantɨ ukkur močʼimtɨ sɨrantɨ taqɨnɨt. </ta>
            <ta e="T94" id="Seg_774" s="T89">Ukkur nʼeːmtɨ qaqlaqɨntɨ ınna tɛltɨkkɨt. </ta>
            <ta e="T97" id="Seg_775" s="T94">Nɨːnä moqonä laqaltɛlʼčʼa. </ta>
            <ta e="T100" id="Seg_776" s="T97">Üːtät mɔːtqɨntä tülʼčʼe. </ta>
            <ta e="T106" id="Seg_777" s="T100">Ɔːtantɨ qümɨlʼ lʼäj mɨntɨ mɔːttɨ tultokkɨt. </ta>
            <ta e="T112" id="Seg_778" s="T106">Imatɨ ɔːtantɨ poːntɨ mɨp toː kıralqolamnɨt. </ta>
            <ta e="T120" id="Seg_779" s="T112">Ɔːtanta poːntɨlʼ mɨp kıralʼa puːt pona tattɨkkɨt, teːkɨrɨqɨntoːqa. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_780" s="T0">mat</ta>
            <ta e="T2" id="Seg_781" s="T1">täːle</ta>
            <ta e="T3" id="Seg_782" s="T2">söt-tɨ</ta>
            <ta e="T4" id="Seg_783" s="T3">ɔːta-tqä</ta>
            <ta e="T5" id="Seg_784" s="T4">mant-al-pɨ-lʼa</ta>
            <ta e="T6" id="Seg_785" s="T5">kora-nɛnta-k</ta>
            <ta e="T7" id="Seg_786" s="T6">qarɨ-t</ta>
            <ta e="T8" id="Seg_787" s="T7">ınna</ta>
            <ta e="T9" id="Seg_788" s="T8">mesi-la</ta>
            <ta e="T10" id="Seg_789" s="T9">čʼoːi-p</ta>
            <ta e="T11" id="Seg_790" s="T10">ütɨ-ŋɨ-tɨ</ta>
            <ta e="T12" id="Seg_791" s="T11">nɨːnɨ</ta>
            <ta e="T13" id="Seg_792" s="T12">peːme-lʼ</ta>
            <ta e="T14" id="Seg_793" s="T13">tɔːk-tɨ</ta>
            <ta e="T15" id="Seg_794" s="T14">seːr-nɨ-tɨ</ta>
            <ta e="T16" id="Seg_795" s="T15">nɨːnɨ</ta>
            <ta e="T17" id="Seg_796" s="T16">ınna</ta>
            <ta e="T18" id="Seg_797" s="T17">nɨla-ji-la</ta>
            <ta e="T19" id="Seg_798" s="T18">mösɨ-la</ta>
            <ta e="T20" id="Seg_799" s="T19">pona</ta>
            <ta e="T21" id="Seg_800" s="T20">potäna</ta>
            <ta e="T23" id="Seg_801" s="T21">tanta</ta>
            <ta e="T24" id="Seg_802" s="T23">kanak-tɨ</ta>
            <ta e="T25" id="Seg_803" s="T24">laŋ-tɨ-ntɨ</ta>
            <ta e="T26" id="Seg_804" s="T25">pɨrr</ta>
            <ta e="T27" id="Seg_805" s="T26">ɔːtä</ta>
            <ta e="T28" id="Seg_806" s="T27">ıllä</ta>
            <ta e="T29" id="Seg_807" s="T28">tɔːqq-atɨ</ta>
            <ta e="T30" id="Seg_808" s="T29">kanak</ta>
            <ta e="T31" id="Seg_809" s="T30">ɔːta-iː-m-tɨ</ta>
            <ta e="T32" id="Seg_810" s="T31">tɔːqqɨ-kkɨ-i-tɨ</ta>
            <ta e="T33" id="Seg_811" s="T32">täp</ta>
            <ta e="T34" id="Seg_812" s="T33">tɛːmti-ti</ta>
            <ta e="T35" id="Seg_813" s="T34">iː-tä-t</ta>
            <ta e="T36" id="Seg_814" s="T35">ɔːta-lʼ</ta>
            <ta e="T37" id="Seg_815" s="T36">ura-to-qɨntoːqa-n</ta>
            <ta e="T38" id="Seg_816" s="T37">ɔːta-m-tɨ</ta>
            <ta e="T39" id="Seg_817" s="T38">orqɨl-nɨ-tɨ</ta>
            <ta e="T40" id="Seg_818" s="T39">nɨːnä</ta>
            <ta e="T41" id="Seg_819" s="T40">qaqɨlɨ-m-tɨ</ta>
            <ta e="T42" id="Seg_820" s="T41">sar-al-nɨ-tɨ</ta>
            <ta e="T43" id="Seg_821" s="T42">nɨːnä</ta>
            <ta e="T44" id="Seg_822" s="T43">šöt-tə</ta>
            <ta e="T45" id="Seg_823" s="T44">laqalt-ɛlʼčʼa</ta>
            <ta e="T46" id="Seg_824" s="T45">nʼoː-tɨ-r-lʼa</ta>
            <ta e="T47" id="Seg_825" s="T46">šittə</ta>
            <ta e="T48" id="Seg_826" s="T47">nʼarɨ-t</ta>
            <ta e="T49" id="Seg_827" s="T48">toː</ta>
            <ta e="T50" id="Seg_828" s="T49">puː-kka</ta>
            <ta e="T51" id="Seg_829" s="T50">mačʼi-t</ta>
            <ta e="T52" id="Seg_830" s="T51">ɔːta-t</ta>
            <ta e="T53" id="Seg_831" s="T52">wättɨ</ta>
            <ta e="T54" id="Seg_832" s="T53">qo-ntɨ-tɨ</ta>
            <ta e="T55" id="Seg_833" s="T54">nɨːnɨ</ta>
            <ta e="T56" id="Seg_834" s="T55">nʼoː-tɨ-kkɨ-tɨ</ta>
            <ta e="T57" id="Seg_835" s="T56">olʼeqat-qɨt</ta>
            <ta e="T58" id="Seg_836" s="T57">ɔːta-t</ta>
            <ta e="T59" id="Seg_837" s="T58">kuːnɨ-ntɔː-tɨt</ta>
            <ta e="T60" id="Seg_838" s="T59">mat</ta>
            <ta e="T61" id="Seg_839" s="T60">qɨn-ta-lʼa-p</ta>
            <ta e="T62" id="Seg_840" s="T61">čʼatɨ-qunaːqa</ta>
            <ta e="T63" id="Seg_841" s="T62">mɛrkɨ-t</ta>
            <ta e="T64" id="Seg_842" s="T63">ɔːr-tɨ-n</ta>
            <ta e="T65" id="Seg_843" s="T64">nɨːnä</ta>
            <ta e="T66" id="Seg_844" s="T65">čʼattɨ-sa-p</ta>
            <ta e="T67" id="Seg_845" s="T66">ukkur</ta>
            <ta e="T68" id="Seg_846" s="T67">pɔːr</ta>
            <ta e="T69" id="Seg_847" s="T68">šittɨ</ta>
            <ta e="T70" id="Seg_848" s="T69">quːro-p</ta>
            <ta e="T71" id="Seg_849" s="T70">ɔːma</ta>
            <ta e="T72" id="Seg_850" s="T71">ɔːta-t</ta>
            <ta e="T73" id="Seg_851" s="T72">nɨːnɨ</ta>
            <ta e="T74" id="Seg_852" s="T73">paktɨ-sɔː-tɨt</ta>
            <ta e="T75" id="Seg_853" s="T74">tap</ta>
            <ta e="T76" id="Seg_854" s="T75">ɔːta-m-tɨ</ta>
            <ta e="T77" id="Seg_855" s="T76">kırɨ-kkɨ-tɨ</ta>
            <ta e="T78" id="Seg_856" s="T77">čʼoːtɨqopɨ-p</ta>
            <ta e="T79" id="Seg_857" s="T78">toː</ta>
            <ta e="T80" id="Seg_858" s="T79">par-qɨl-nɨ-tɨ</ta>
            <ta e="T81" id="Seg_859" s="T80">nɨːnɨ</ta>
            <ta e="T82" id="Seg_860" s="T81">kəm-tɨ</ta>
            <ta e="T83" id="Seg_861" s="T82">nɨː</ta>
            <ta e="T84" id="Seg_862" s="T83">qamtɨ-kkɨ-t</ta>
            <ta e="T85" id="Seg_863" s="T84">ɔːta-n-tɨ</ta>
            <ta e="T86" id="Seg_864" s="T85">ukkur</ta>
            <ta e="T87" id="Seg_865" s="T86">močʼi-m-tɨ</ta>
            <ta e="T88" id="Seg_866" s="T87">sɨra-ntɨ</ta>
            <ta e="T89" id="Seg_867" s="T88">taqɨ-nɨ-t</ta>
            <ta e="T90" id="Seg_868" s="T89">ukkur</ta>
            <ta e="T91" id="Seg_869" s="T90">nʼeː-m-tɨ</ta>
            <ta e="T92" id="Seg_870" s="T91">qaqla-qɨn-tɨ</ta>
            <ta e="T93" id="Seg_871" s="T92">ınna</ta>
            <ta e="T94" id="Seg_872" s="T93">tɛltɨ-kkɨ-t</ta>
            <ta e="T95" id="Seg_873" s="T94">nɨːnä</ta>
            <ta e="T96" id="Seg_874" s="T95">moqonä</ta>
            <ta e="T97" id="Seg_875" s="T96">laqalt-ɛlʼčʼa</ta>
            <ta e="T98" id="Seg_876" s="T97">üːtä-t</ta>
            <ta e="T99" id="Seg_877" s="T98">mɔːt-qɨn-tä</ta>
            <ta e="T100" id="Seg_878" s="T99">tü-lʼčʼe</ta>
            <ta e="T101" id="Seg_879" s="T100">ɔːta-n-tɨ</ta>
            <ta e="T102" id="Seg_880" s="T101">qümɨ-lʼ</ta>
            <ta e="T103" id="Seg_881" s="T102">lʼä-j</ta>
            <ta e="T104" id="Seg_882" s="T103">mɨ-n-tɨ</ta>
            <ta e="T105" id="Seg_883" s="T104">mɔːt-tɨ</ta>
            <ta e="T106" id="Seg_884" s="T105">tul-to-kkɨ-t</ta>
            <ta e="T107" id="Seg_885" s="T106">ima-tɨ</ta>
            <ta e="T108" id="Seg_886" s="T107">ɔːta-n-tɨ</ta>
            <ta e="T109" id="Seg_887" s="T108">poːntɨ</ta>
            <ta e="T110" id="Seg_888" s="T109">mɨ-p</ta>
            <ta e="T111" id="Seg_889" s="T110">toː</ta>
            <ta e="T112" id="Seg_890" s="T111">kır-al-q-olam-nɨ-t</ta>
            <ta e="T113" id="Seg_891" s="T112">ɔːta-n-ta</ta>
            <ta e="T114" id="Seg_892" s="T113">poːntɨ-lʼ</ta>
            <ta e="T115" id="Seg_893" s="T114">mɨ-p</ta>
            <ta e="T116" id="Seg_894" s="T115">kıra-lʼa</ta>
            <ta e="T117" id="Seg_895" s="T116">puːt</ta>
            <ta e="T118" id="Seg_896" s="T117">pona</ta>
            <ta e="T119" id="Seg_897" s="T118">tattɨ-kkɨ-t</ta>
            <ta e="T120" id="Seg_898" s="T119">teːkɨ-r-ɨ-qɨntoːqa</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_899" s="T0">man</ta>
            <ta e="T2" id="Seg_900" s="T1">täːlɨ</ta>
            <ta e="T3" id="Seg_901" s="T2">šöt-ntɨ</ta>
            <ta e="T4" id="Seg_902" s="T3">ɔːtä-tqo</ta>
            <ta e="T5" id="Seg_903" s="T4">mantɨ-ätɔːl-mpɨ-lä</ta>
            <ta e="T6" id="Seg_904" s="T5">kora-ɛntɨ-k</ta>
            <ta e="T7" id="Seg_905" s="T6">qarɨ-ntɨ</ta>
            <ta e="T8" id="Seg_906" s="T7">ınnä</ta>
            <ta e="T9" id="Seg_907" s="T8">wəšɨ-lä</ta>
            <ta e="T10" id="Seg_908" s="T9">čʼaːj-m</ta>
            <ta e="T11" id="Seg_909" s="T10">ütɨ-ŋɨ-tɨ</ta>
            <ta e="T12" id="Seg_910" s="T11">nɨːnɨ</ta>
            <ta e="T13" id="Seg_911" s="T12">peːmɨ-lʼ</ta>
            <ta e="T14" id="Seg_912" s="T13">tɔːŋ-tɨ</ta>
            <ta e="T15" id="Seg_913" s="T14">šeːr-ŋɨ-tɨ</ta>
            <ta e="T16" id="Seg_914" s="T15">nɨːnɨ</ta>
            <ta e="T17" id="Seg_915" s="T16">ınnä</ta>
            <ta e="T18" id="Seg_916" s="T17">nɨl-lɨ-lä</ta>
            <ta e="T19" id="Seg_917" s="T18">wəšɨ-lä</ta>
            <ta e="T20" id="Seg_918" s="T19">ponä</ta>
            <ta e="T21" id="Seg_919" s="T20">ponä</ta>
            <ta e="T23" id="Seg_920" s="T21">tantɨ</ta>
            <ta e="T24" id="Seg_921" s="T23">kanaŋ-tɨ</ta>
            <ta e="T25" id="Seg_922" s="T24">laŋkɨ-tɨ-ntɨ</ta>
            <ta e="T26" id="Seg_923" s="T25">pɨrr</ta>
            <ta e="T27" id="Seg_924" s="T26">ɔːtä</ta>
            <ta e="T28" id="Seg_925" s="T27">ıllä</ta>
            <ta e="T29" id="Seg_926" s="T28">tɔːqqɨ-ätɨ</ta>
            <ta e="T30" id="Seg_927" s="T29">kanaŋ</ta>
            <ta e="T31" id="Seg_928" s="T30">ɔːtä-iː-m-tɨ</ta>
            <ta e="T32" id="Seg_929" s="T31">tɔːqqɨ-kkɨ-ŋɨ-tɨ</ta>
            <ta e="T33" id="Seg_930" s="T32">təp</ta>
            <ta e="T34" id="Seg_931" s="T33">tɛːmnɨ-tɨ</ta>
            <ta e="T35" id="Seg_932" s="T34">iː-tɨ-tɨ</ta>
            <ta e="T36" id="Seg_933" s="T35">ɔːtä-lʼ</ta>
            <ta e="T37" id="Seg_934" s="T36">ora-tɨ-qɨntoːqo-naj</ta>
            <ta e="T38" id="Seg_935" s="T37">ɔːtä-m-tɨ</ta>
            <ta e="T39" id="Seg_936" s="T38">orqɨl-ŋɨ-tɨ</ta>
            <ta e="T40" id="Seg_937" s="T39">nɨːnɨ</ta>
            <ta e="T41" id="Seg_938" s="T40">qaqlɨ-m-tɨ</ta>
            <ta e="T42" id="Seg_939" s="T41">*sar-ätɔːl-ŋɨ-tɨ</ta>
            <ta e="T43" id="Seg_940" s="T42">nɨːnɨ</ta>
            <ta e="T44" id="Seg_941" s="T43">šöt-ntɨ</ta>
            <ta e="T45" id="Seg_942" s="T44">laqaltɨ-ɛː</ta>
            <ta e="T46" id="Seg_943" s="T45">nʼoː-tɨ-r-lä</ta>
            <ta e="T47" id="Seg_944" s="T46">šittɨ</ta>
            <ta e="T48" id="Seg_945" s="T47">nʼarɨ-t</ta>
            <ta e="T49" id="Seg_946" s="T48">toː</ta>
            <ta e="T50" id="Seg_947" s="T49">puː-kkɨ</ta>
            <ta e="T51" id="Seg_948" s="T50">mačʼɨ-n</ta>
            <ta e="T52" id="Seg_949" s="T51">ɔːtä-n</ta>
            <ta e="T53" id="Seg_950" s="T52">wəttɨ</ta>
            <ta e="T54" id="Seg_951" s="T53">qo-ntɨ-tɨ</ta>
            <ta e="T55" id="Seg_952" s="T54">nɨːnɨ</ta>
            <ta e="T56" id="Seg_953" s="T55">nʼoː-tɨ-kkɨ-tɨ</ta>
            <ta e="T57" id="Seg_954" s="T56">olʼqan-qɨn</ta>
            <ta e="T58" id="Seg_955" s="T57">ɔːtä-t</ta>
            <ta e="T59" id="Seg_956" s="T58">kuːnɨ-ntɨ-tɨt</ta>
            <ta e="T60" id="Seg_957" s="T59">man</ta>
            <ta e="T61" id="Seg_958" s="T60">qən-ntɨ-lä-m</ta>
            <ta e="T62" id="Seg_959" s="T61">čʼattɨ-qɨnoːqo</ta>
            <ta e="T63" id="Seg_960" s="T62">mɛrkɨ-n</ta>
            <ta e="T64" id="Seg_961" s="T63">ɔːrɨ-tɨ-n</ta>
            <ta e="T65" id="Seg_962" s="T64">nɨːnɨ</ta>
            <ta e="T66" id="Seg_963" s="T65">čʼattɨ-sɨ-m</ta>
            <ta e="T67" id="Seg_964" s="T66">ukkɨr</ta>
            <ta e="T68" id="Seg_965" s="T67">pɔːrɨ</ta>
            <ta e="T69" id="Seg_966" s="T68">šittɨ</ta>
            <ta e="T70" id="Seg_967" s="T69">qoːrɨ-m</ta>
            <ta e="T71" id="Seg_968" s="T70">ɔːmɨ</ta>
            <ta e="T72" id="Seg_969" s="T71">ɔːtä-t</ta>
            <ta e="T73" id="Seg_970" s="T72">nɨːnɨ</ta>
            <ta e="T74" id="Seg_971" s="T73">paktɨ-sɨ-tɨt</ta>
            <ta e="T75" id="Seg_972" s="T74">təp</ta>
            <ta e="T76" id="Seg_973" s="T75">ɔːtä-m-tɨ</ta>
            <ta e="T77" id="Seg_974" s="T76">kırɨ-kkɨ-tɨ</ta>
            <ta e="T78" id="Seg_975" s="T77">čʼoːtɨqopɨ-m</ta>
            <ta e="T79" id="Seg_976" s="T78">toː</ta>
            <ta e="T80" id="Seg_977" s="T79">par-qɨl-ŋɨ-tɨ</ta>
            <ta e="T81" id="Seg_978" s="T80">nɨːnɨ</ta>
            <ta e="T82" id="Seg_979" s="T81">kəm-tɨ</ta>
            <ta e="T83" id="Seg_980" s="T82">nɨː</ta>
            <ta e="T84" id="Seg_981" s="T83">qamtɨ-kkɨ-tɨ</ta>
            <ta e="T85" id="Seg_982" s="T84">ɔːtä-n-tɨ</ta>
            <ta e="T86" id="Seg_983" s="T85">ukkɨr</ta>
            <ta e="T87" id="Seg_984" s="T86">wəčʼɨ-m-tɨ</ta>
            <ta e="T88" id="Seg_985" s="T87">sɨrɨ-ntɨ</ta>
            <ta e="T89" id="Seg_986" s="T88">taqɨ-ŋɨ-tɨ</ta>
            <ta e="T90" id="Seg_987" s="T89">ukkɨr</ta>
            <ta e="T91" id="Seg_988" s="T90">nʼeː-m-tɨ</ta>
            <ta e="T92" id="Seg_989" s="T91">qaqlɨ-qɨn-ntɨ</ta>
            <ta e="T93" id="Seg_990" s="T92">ınnä</ta>
            <ta e="T94" id="Seg_991" s="T93">tɛltɨ-kkɨ-tɨ</ta>
            <ta e="T95" id="Seg_992" s="T94">nɨːnɨ</ta>
            <ta e="T96" id="Seg_993" s="T95">moqɨnä</ta>
            <ta e="T97" id="Seg_994" s="T96">laqaltɨ-ɛː</ta>
            <ta e="T98" id="Seg_995" s="T97">üːtɨ-k</ta>
            <ta e="T99" id="Seg_996" s="T98">mɔːt-qɨn-ntɨ</ta>
            <ta e="T100" id="Seg_997" s="T99">tü-lʼčʼɨ</ta>
            <ta e="T101" id="Seg_998" s="T100">ɔːtä-n-tɨ</ta>
            <ta e="T102" id="Seg_999" s="T101">qümɨ-lʼ</ta>
            <ta e="T103" id="Seg_1000" s="T102">lə-lʼ</ta>
            <ta e="T104" id="Seg_1001" s="T103">mɨ-t-tɨ</ta>
            <ta e="T105" id="Seg_1002" s="T104">mɔːt-ntɨ</ta>
            <ta e="T106" id="Seg_1003" s="T105">tul-tɨ-kkɨ-tɨ</ta>
            <ta e="T107" id="Seg_1004" s="T106">ima-tɨ</ta>
            <ta e="T108" id="Seg_1005" s="T107">ɔːtä-n-tɨ</ta>
            <ta e="T109" id="Seg_1006" s="T108">poːntɨ</ta>
            <ta e="T110" id="Seg_1007" s="T109">mɨ-m</ta>
            <ta e="T111" id="Seg_1008" s="T110">toː</ta>
            <ta e="T112" id="Seg_1009" s="T111">kırɨ-ätɔːl-qo-olam-ŋɨ-tɨ</ta>
            <ta e="T113" id="Seg_1010" s="T112">ɔːtä-n-tɨ</ta>
            <ta e="T114" id="Seg_1011" s="T113">poːntɨ-lʼ</ta>
            <ta e="T115" id="Seg_1012" s="T114">mɨ-m</ta>
            <ta e="T116" id="Seg_1013" s="T115">kırɨ-lä</ta>
            <ta e="T117" id="Seg_1014" s="T116">puːn</ta>
            <ta e="T118" id="Seg_1015" s="T117">ponä</ta>
            <ta e="T119" id="Seg_1016" s="T118">taːtɨ-kkɨ-tɨ</ta>
            <ta e="T120" id="Seg_1017" s="T119">təːkɨ-r-ɨ-qɨntoːqo</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1018" s="T0">I.NOM</ta>
            <ta e="T2" id="Seg_1019" s="T1">tomorrow</ta>
            <ta e="T3" id="Seg_1020" s="T2">forest-ILL</ta>
            <ta e="T4" id="Seg_1021" s="T3">reindeer-TRL</ta>
            <ta e="T5" id="Seg_1022" s="T4">give.a.look-MOM-DUR-CVB</ta>
            <ta e="T6" id="Seg_1023" s="T5">go.hunting-FUT-1SG.S</ta>
            <ta e="T7" id="Seg_1024" s="T6">morning-ADV.LOC</ta>
            <ta e="T8" id="Seg_1025" s="T7">up</ta>
            <ta e="T9" id="Seg_1026" s="T8">get.up-CVB</ta>
            <ta e="T10" id="Seg_1027" s="T9">tea-ACC</ta>
            <ta e="T11" id="Seg_1028" s="T10">drink-CO-3SG.O</ta>
            <ta e="T12" id="Seg_1029" s="T11">then</ta>
            <ta e="T13" id="Seg_1030" s="T12">footwear-ADJZ</ta>
            <ta e="T14" id="Seg_1031" s="T13">all.the.rest.[NOM]-3SG</ta>
            <ta e="T15" id="Seg_1032" s="T14">put.on-CO-3SG.O</ta>
            <ta e="T16" id="Seg_1033" s="T15">then</ta>
            <ta e="T17" id="Seg_1034" s="T16">up</ta>
            <ta e="T18" id="Seg_1035" s="T17">stand-INCH-CVB</ta>
            <ta e="T19" id="Seg_1036" s="T18">get.up-CVB</ta>
            <ta e="T20" id="Seg_1037" s="T19">outwards</ta>
            <ta e="T21" id="Seg_1038" s="T20">outwards</ta>
            <ta e="T23" id="Seg_1039" s="T21">go.out.[3SG.S]</ta>
            <ta e="T24" id="Seg_1040" s="T23">dog.[NOM]-3SG</ta>
            <ta e="T25" id="Seg_1041" s="T24">cry-TR-IPFV.[3SG.S]</ta>
            <ta e="T26" id="Seg_1042" s="T25">tally_ho</ta>
            <ta e="T27" id="Seg_1043" s="T26">reindeer.[NOM]</ta>
            <ta e="T28" id="Seg_1044" s="T27">down</ta>
            <ta e="T29" id="Seg_1045" s="T28">shepherd-IMP.2SG.O</ta>
            <ta e="T30" id="Seg_1046" s="T29">dog.[NOM]</ta>
            <ta e="T31" id="Seg_1047" s="T30">reindeer-PL-ACC-3SG.O</ta>
            <ta e="T32" id="Seg_1048" s="T31">shepherd-HAB-CO-3SG.O</ta>
            <ta e="T33" id="Seg_1049" s="T32">(s)he.[NOM]</ta>
            <ta e="T34" id="Seg_1050" s="T33">rope.[NOM]-3SG</ta>
            <ta e="T35" id="Seg_1051" s="T34">take-TR-3SG.O</ta>
            <ta e="T36" id="Seg_1052" s="T35">reindeer-ADJZ</ta>
            <ta e="T37" id="Seg_1053" s="T36">catch-TR-SUP.2/3SG-EMPH</ta>
            <ta e="T38" id="Seg_1054" s="T37">reindeer-ACC-3SG</ta>
            <ta e="T39" id="Seg_1055" s="T38">catch-CO-3SG.O</ta>
            <ta e="T40" id="Seg_1056" s="T39">then</ta>
            <ta e="T41" id="Seg_1057" s="T40">sledge-ACC-3SG</ta>
            <ta e="T42" id="Seg_1058" s="T41">harness-MOM-CO-3SG.O</ta>
            <ta e="T43" id="Seg_1059" s="T42">then</ta>
            <ta e="T44" id="Seg_1060" s="T43">forest-ILL</ta>
            <ta e="T45" id="Seg_1061" s="T44">start.for-PFV.[3SG.S]</ta>
            <ta e="T46" id="Seg_1062" s="T45">catch.up-TR-FRQ-CVB</ta>
            <ta e="T47" id="Seg_1063" s="T46">two</ta>
            <ta e="T48" id="Seg_1064" s="T47">tundra-PL.[NOM]</ta>
            <ta e="T49" id="Seg_1065" s="T48">away</ta>
            <ta e="T50" id="Seg_1066" s="T49">cross-HAB.[3SG.S]</ta>
            <ta e="T51" id="Seg_1067" s="T50">forest-GEN</ta>
            <ta e="T52" id="Seg_1068" s="T51">reindeer-GEN</ta>
            <ta e="T53" id="Seg_1069" s="T52">trace.[NOM]</ta>
            <ta e="T54" id="Seg_1070" s="T53">find-IPFV-3SG.O</ta>
            <ta e="T55" id="Seg_1071" s="T54">then</ta>
            <ta e="T56" id="Seg_1072" s="T55">catch.up-TR-HAB-3SG.O</ta>
            <ta e="T57" id="Seg_1073" s="T56">taiga-LOC</ta>
            <ta e="T58" id="Seg_1074" s="T57">reindeer-PL.[NOM]</ta>
            <ta e="T59" id="Seg_1075" s="T58">run-IPFV-3PL</ta>
            <ta e="T60" id="Seg_1076" s="T59">I.NOM</ta>
            <ta e="T61" id="Seg_1077" s="T60">go.away-IPFV-OPT-1SG.O</ta>
            <ta e="T62" id="Seg_1078" s="T61">shoot-SUP.1SG</ta>
            <ta e="T63" id="Seg_1079" s="T62">wind-GEN</ta>
            <ta e="T64" id="Seg_1080" s="T63">across-3SG-ADV.LOC</ta>
            <ta e="T65" id="Seg_1081" s="T64">then</ta>
            <ta e="T66" id="Seg_1082" s="T65">shoot-PST-1SG.O</ta>
            <ta e="T67" id="Seg_1083" s="T66">one</ta>
            <ta e="T68" id="Seg_1084" s="T67">time.[NOM]</ta>
            <ta e="T69" id="Seg_1085" s="T68">two</ta>
            <ta e="T70" id="Seg_1086" s="T69">buck-ACC</ta>
            <ta e="T71" id="Seg_1087" s="T70">other</ta>
            <ta e="T72" id="Seg_1088" s="T71">reindeer-PL.[NOM]</ta>
            <ta e="T73" id="Seg_1089" s="T72">then</ta>
            <ta e="T74" id="Seg_1090" s="T73">run-PST-3PL</ta>
            <ta e="T75" id="Seg_1091" s="T74">(s)he</ta>
            <ta e="T76" id="Seg_1092" s="T75">reindeer-ACC-3SG</ta>
            <ta e="T77" id="Seg_1093" s="T76">skin-HAB-3SG.O</ta>
            <ta e="T78" id="Seg_1094" s="T77">stomach-ACC</ta>
            <ta e="T79" id="Seg_1095" s="T78">away</ta>
            <ta e="T80" id="Seg_1096" s="T79">evert-MULO-CO-3SG.O</ta>
            <ta e="T81" id="Seg_1097" s="T80">then</ta>
            <ta e="T82" id="Seg_1098" s="T81">blood.[NOM]-3SG</ta>
            <ta e="T83" id="Seg_1099" s="T82">there</ta>
            <ta e="T84" id="Seg_1100" s="T83">pour.out-HAB-3SG.O</ta>
            <ta e="T85" id="Seg_1101" s="T84">reindeer-GEN-3SG</ta>
            <ta e="T86" id="Seg_1102" s="T85">one</ta>
            <ta e="T87" id="Seg_1103" s="T86">meat-ACC-3SG</ta>
            <ta e="T88" id="Seg_1104" s="T87">snow-ILL</ta>
            <ta e="T89" id="Seg_1105" s="T88">close-CO-3SG.O</ta>
            <ta e="T90" id="Seg_1106" s="T89">one</ta>
            <ta e="T91" id="Seg_1107" s="T90">somebody-ACC-3SG</ta>
            <ta e="T92" id="Seg_1108" s="T91">sledge-ILL-OBL.3SG</ta>
            <ta e="T93" id="Seg_1109" s="T92">up</ta>
            <ta e="T94" id="Seg_1110" s="T93">load-HAB-3SG.O</ta>
            <ta e="T95" id="Seg_1111" s="T94">then</ta>
            <ta e="T96" id="Seg_1112" s="T95">home</ta>
            <ta e="T97" id="Seg_1113" s="T96">start.for-PFV.[3SG.S]</ta>
            <ta e="T98" id="Seg_1114" s="T97">evening-ADVZ</ta>
            <ta e="T99" id="Seg_1115" s="T98">house-ILL-OBL.3SG</ta>
            <ta e="T100" id="Seg_1116" s="T99">come-PFV.[3SG.S]</ta>
            <ta e="T101" id="Seg_1117" s="T100">reindeer-GEN-3SG</ta>
            <ta e="T102" id="Seg_1118" s="T101">brains-ADJZ</ta>
            <ta e="T103" id="Seg_1119" s="T102">bone-ADJZ</ta>
            <ta e="T104" id="Seg_1120" s="T103">something-PL.[NOM]-3SG</ta>
            <ta e="T105" id="Seg_1121" s="T104">house-ILL</ta>
            <ta e="T106" id="Seg_1122" s="T105">bring-TR-HAB-3SG.O</ta>
            <ta e="T107" id="Seg_1123" s="T106">wife.[NOM]-3SG</ta>
            <ta e="T108" id="Seg_1124" s="T107">reindeer-GEN-3SG</ta>
            <ta e="T109" id="Seg_1125" s="T108">fell.from.deer's.leg.[NOM]</ta>
            <ta e="T110" id="Seg_1126" s="T109">something-ACC</ta>
            <ta e="T111" id="Seg_1127" s="T110">away</ta>
            <ta e="T112" id="Seg_1128" s="T111">skin-MOM-INF-begin-CO-3SG.O</ta>
            <ta e="T113" id="Seg_1129" s="T112">reindeer-GEN-3SG</ta>
            <ta e="T114" id="Seg_1130" s="T113">fell.from.deer's.leg-ADJZ</ta>
            <ta e="T115" id="Seg_1131" s="T114">something-ACC</ta>
            <ta e="T116" id="Seg_1132" s="T115">skin-CVB</ta>
            <ta e="T117" id="Seg_1133" s="T116">afterwards</ta>
            <ta e="T118" id="Seg_1134" s="T117">outwards</ta>
            <ta e="T119" id="Seg_1135" s="T118">bring-HAB-3SG.O</ta>
            <ta e="T120" id="Seg_1136" s="T119">dry-FRQ-EP-SUP.2/3SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1137" s="T0">я.NOM</ta>
            <ta e="T2" id="Seg_1138" s="T1">завтра</ta>
            <ta e="T3" id="Seg_1139" s="T2">лес-ILL</ta>
            <ta e="T4" id="Seg_1140" s="T3">олень-TRL</ta>
            <ta e="T5" id="Seg_1141" s="T4">взглянуть-MOM-DUR-CVB</ta>
            <ta e="T6" id="Seg_1142" s="T5">отправиться.на.охоту-FUT-1SG.S</ta>
            <ta e="T7" id="Seg_1143" s="T6">утро-ADV.LOC</ta>
            <ta e="T8" id="Seg_1144" s="T7">вверх</ta>
            <ta e="T9" id="Seg_1145" s="T8">встать-CVB</ta>
            <ta e="T10" id="Seg_1146" s="T9">чай-ACC</ta>
            <ta e="T11" id="Seg_1147" s="T10">пить-CO-3SG.O</ta>
            <ta e="T12" id="Seg_1148" s="T11">потом</ta>
            <ta e="T13" id="Seg_1149" s="T12">обувь-ADJZ</ta>
            <ta e="T14" id="Seg_1150" s="T13">всё.прочее.[NOM]-3SG</ta>
            <ta e="T15" id="Seg_1151" s="T14">надеть-CO-3SG.O</ta>
            <ta e="T16" id="Seg_1152" s="T15">потом</ta>
            <ta e="T17" id="Seg_1153" s="T16">вверх</ta>
            <ta e="T18" id="Seg_1154" s="T17">встать-INCH-CVB</ta>
            <ta e="T19" id="Seg_1155" s="T18">встать-CVB</ta>
            <ta e="T20" id="Seg_1156" s="T19">наружу</ta>
            <ta e="T21" id="Seg_1157" s="T20">наружу</ta>
            <ta e="T23" id="Seg_1158" s="T21">выйти.[3SG.S]</ta>
            <ta e="T24" id="Seg_1159" s="T23">собака.[NOM]-3SG</ta>
            <ta e="T25" id="Seg_1160" s="T24">кричать-TR-IPFV.[3SG.S]</ta>
            <ta e="T26" id="Seg_1161" s="T25">ату</ta>
            <ta e="T27" id="Seg_1162" s="T26">олень.[NOM]</ta>
            <ta e="T28" id="Seg_1163" s="T27">вниз</ta>
            <ta e="T29" id="Seg_1164" s="T28">погнать-IMP.2SG.O</ta>
            <ta e="T30" id="Seg_1165" s="T29">собака.[NOM]</ta>
            <ta e="T31" id="Seg_1166" s="T30">олень-PL-ACC-3SG.O</ta>
            <ta e="T32" id="Seg_1167" s="T31">погнать-HAB-CO-3SG.O</ta>
            <ta e="T33" id="Seg_1168" s="T32">он(а).[NOM]</ta>
            <ta e="T34" id="Seg_1169" s="T33">верёвка.[NOM]-3SG</ta>
            <ta e="T35" id="Seg_1170" s="T34">взять-TR-3SG.O</ta>
            <ta e="T36" id="Seg_1171" s="T35">олень-ADJZ</ta>
            <ta e="T37" id="Seg_1172" s="T36">хватать-TR-SUP.2/3SG-EMPH</ta>
            <ta e="T38" id="Seg_1173" s="T37">олень-ACC-3SG</ta>
            <ta e="T39" id="Seg_1174" s="T38">схватить-CO-3SG.O</ta>
            <ta e="T40" id="Seg_1175" s="T39">потом</ta>
            <ta e="T41" id="Seg_1176" s="T40">нарта-ACC-3SG</ta>
            <ta e="T42" id="Seg_1177" s="T41">запрячь-MOM-CO-3SG.O</ta>
            <ta e="T43" id="Seg_1178" s="T42">потом</ta>
            <ta e="T44" id="Seg_1179" s="T43">лес-ILL</ta>
            <ta e="T45" id="Seg_1180" s="T44">тронуться-PFV.[3SG.S]</ta>
            <ta e="T46" id="Seg_1181" s="T45">догонять-TR-FRQ-CVB</ta>
            <ta e="T47" id="Seg_1182" s="T46">два</ta>
            <ta e="T48" id="Seg_1183" s="T47">тундра-PL.[NOM]</ta>
            <ta e="T49" id="Seg_1184" s="T48">прочь</ta>
            <ta e="T50" id="Seg_1185" s="T49">перейти-HAB.[3SG.S]</ta>
            <ta e="T51" id="Seg_1186" s="T50">лес-GEN</ta>
            <ta e="T52" id="Seg_1187" s="T51">олень-GEN</ta>
            <ta e="T53" id="Seg_1188" s="T52">след.[NOM]</ta>
            <ta e="T54" id="Seg_1189" s="T53">находить-IPFV-3SG.O</ta>
            <ta e="T55" id="Seg_1190" s="T54">потом</ta>
            <ta e="T56" id="Seg_1191" s="T55">догонять-TR-HAB-3SG.O</ta>
            <ta e="T57" id="Seg_1192" s="T56">тайга-LOC</ta>
            <ta e="T58" id="Seg_1193" s="T57">олень-PL.[NOM]</ta>
            <ta e="T59" id="Seg_1194" s="T58">бежать-IPFV-3PL</ta>
            <ta e="T60" id="Seg_1195" s="T59">я.NOM</ta>
            <ta e="T61" id="Seg_1196" s="T60">уйти-IPFV-OPT-1SG.O</ta>
            <ta e="T62" id="Seg_1197" s="T61">стрелять-SUP.1SG</ta>
            <ta e="T63" id="Seg_1198" s="T62">ветер-GEN</ta>
            <ta e="T64" id="Seg_1199" s="T63">поперёк-3SG-ADV.LOC</ta>
            <ta e="T65" id="Seg_1200" s="T64">потом</ta>
            <ta e="T66" id="Seg_1201" s="T65">стрелять-PST-1SG.O</ta>
            <ta e="T67" id="Seg_1202" s="T66">один</ta>
            <ta e="T68" id="Seg_1203" s="T67">раз.[NOM]</ta>
            <ta e="T69" id="Seg_1204" s="T68">два</ta>
            <ta e="T70" id="Seg_1205" s="T69">хор-ACC</ta>
            <ta e="T71" id="Seg_1206" s="T70">остальной</ta>
            <ta e="T72" id="Seg_1207" s="T71">олень-PL.[NOM]</ta>
            <ta e="T73" id="Seg_1208" s="T72">потом</ta>
            <ta e="T74" id="Seg_1209" s="T73">побежать-PST-3PL</ta>
            <ta e="T75" id="Seg_1210" s="T74">он(а)</ta>
            <ta e="T76" id="Seg_1211" s="T75">олень-ACC-3SG</ta>
            <ta e="T77" id="Seg_1212" s="T76">ободрать-HAB-3SG.O</ta>
            <ta e="T78" id="Seg_1213" s="T77">желудок-ACC</ta>
            <ta e="T79" id="Seg_1214" s="T78">прочь</ta>
            <ta e="T80" id="Seg_1215" s="T79">вывернуть-MULO-CO-3SG.O</ta>
            <ta e="T81" id="Seg_1216" s="T80">потом</ta>
            <ta e="T82" id="Seg_1217" s="T81">кровь.[NOM]-3SG</ta>
            <ta e="T83" id="Seg_1218" s="T82">туда</ta>
            <ta e="T84" id="Seg_1219" s="T83">вылить-HAB-3SG.O</ta>
            <ta e="T85" id="Seg_1220" s="T84">олень-GEN-3SG</ta>
            <ta e="T86" id="Seg_1221" s="T85">один</ta>
            <ta e="T87" id="Seg_1222" s="T86">мясо-ACC-3SG</ta>
            <ta e="T88" id="Seg_1223" s="T87">снег-ILL</ta>
            <ta e="T89" id="Seg_1224" s="T88">закрыть-CO-3SG.O</ta>
            <ta e="T90" id="Seg_1225" s="T89">один</ta>
            <ta e="T91" id="Seg_1226" s="T90">некто-ACC-3SG</ta>
            <ta e="T92" id="Seg_1227" s="T91">нарта-ILL-OBL.3SG</ta>
            <ta e="T93" id="Seg_1228" s="T92">вверх</ta>
            <ta e="T94" id="Seg_1229" s="T93">нагрузить-HAB-3SG.O</ta>
            <ta e="T95" id="Seg_1230" s="T94">потом</ta>
            <ta e="T96" id="Seg_1231" s="T95">домой</ta>
            <ta e="T97" id="Seg_1232" s="T96">тронуться-PFV.[3SG.S]</ta>
            <ta e="T98" id="Seg_1233" s="T97">вечер-ADVZ</ta>
            <ta e="T99" id="Seg_1234" s="T98">дом-ILL-OBL.3SG</ta>
            <ta e="T100" id="Seg_1235" s="T99">прийти-PFV.[3SG.S]</ta>
            <ta e="T101" id="Seg_1236" s="T100">олень-GEN-3SG</ta>
            <ta e="T102" id="Seg_1237" s="T101">мозги-ADJZ</ta>
            <ta e="T103" id="Seg_1238" s="T102">кость-ADJZ</ta>
            <ta e="T104" id="Seg_1239" s="T103">нечто-PL.[NOM]-3SG</ta>
            <ta e="T105" id="Seg_1240" s="T104">дом-ILL</ta>
            <ta e="T106" id="Seg_1241" s="T105">занести-TR-HAB-3SG.O</ta>
            <ta e="T107" id="Seg_1242" s="T106">жена.[NOM]-3SG</ta>
            <ta e="T108" id="Seg_1243" s="T107">олень-GEN-3SG</ta>
            <ta e="T109" id="Seg_1244" s="T108">камус.[NOM]</ta>
            <ta e="T110" id="Seg_1245" s="T109">нечто-ACC</ta>
            <ta e="T111" id="Seg_1246" s="T110">прочь</ta>
            <ta e="T112" id="Seg_1247" s="T111">ободрать-MOM-INF-начать-CO-3SG.O</ta>
            <ta e="T113" id="Seg_1248" s="T112">олень-GEN-3SG</ta>
            <ta e="T114" id="Seg_1249" s="T113">камус-ADJZ</ta>
            <ta e="T115" id="Seg_1250" s="T114">нечто-ACC</ta>
            <ta e="T116" id="Seg_1251" s="T115">ободрать-CVB</ta>
            <ta e="T117" id="Seg_1252" s="T116">потом</ta>
            <ta e="T118" id="Seg_1253" s="T117">наружу</ta>
            <ta e="T119" id="Seg_1254" s="T118">принести-HAB-3SG.O</ta>
            <ta e="T120" id="Seg_1255" s="T119">сохнуть-FRQ-EP-SUP.2/3SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1256" s="T0">pers</ta>
            <ta e="T2" id="Seg_1257" s="T1">adv</ta>
            <ta e="T3" id="Seg_1258" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_1259" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_1260" s="T4">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T6" id="Seg_1261" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_1262" s="T6">n-adv:case</ta>
            <ta e="T8" id="Seg_1263" s="T7">preverb</ta>
            <ta e="T9" id="Seg_1264" s="T8">v-v&gt;adv</ta>
            <ta e="T10" id="Seg_1265" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_1266" s="T10">v-v:ins-v:pn</ta>
            <ta e="T12" id="Seg_1267" s="T11">adv</ta>
            <ta e="T13" id="Seg_1268" s="T12">n-n&gt;adj</ta>
            <ta e="T14" id="Seg_1269" s="T13">n-n:case-n:poss</ta>
            <ta e="T15" id="Seg_1270" s="T14">v-v:ins-v:pn</ta>
            <ta e="T16" id="Seg_1271" s="T15">adv</ta>
            <ta e="T17" id="Seg_1272" s="T16">preverb</ta>
            <ta e="T18" id="Seg_1273" s="T17">v-v&gt;v-v&gt;adv</ta>
            <ta e="T19" id="Seg_1274" s="T18">v-v&gt;adv</ta>
            <ta e="T20" id="Seg_1275" s="T19">adv</ta>
            <ta e="T21" id="Seg_1276" s="T20">adv</ta>
            <ta e="T23" id="Seg_1277" s="T21">v-v:pn</ta>
            <ta e="T24" id="Seg_1278" s="T23">n-n:case-n:poss</ta>
            <ta e="T25" id="Seg_1279" s="T24">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T26" id="Seg_1280" s="T25">interj</ta>
            <ta e="T27" id="Seg_1281" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_1282" s="T27">adv</ta>
            <ta e="T29" id="Seg_1283" s="T28">v-v:mood.pn</ta>
            <ta e="T30" id="Seg_1284" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_1285" s="T30">n-n:num-n:case-v:pn</ta>
            <ta e="T32" id="Seg_1286" s="T31">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T33" id="Seg_1287" s="T32">pers-n:case</ta>
            <ta e="T34" id="Seg_1288" s="T33">n-n:case-n:poss</ta>
            <ta e="T35" id="Seg_1289" s="T34">v-v&gt;v-v:pn</ta>
            <ta e="T36" id="Seg_1290" s="T35">n-n&gt;adj</ta>
            <ta e="T37" id="Seg_1291" s="T36">v-v&gt;v-v:inf.poss-clit</ta>
            <ta e="T38" id="Seg_1292" s="T37">n-n:case-n:poss</ta>
            <ta e="T39" id="Seg_1293" s="T38">v-v:ins-v:pn</ta>
            <ta e="T40" id="Seg_1294" s="T39">adv</ta>
            <ta e="T41" id="Seg_1295" s="T40">n-n:case-n:poss</ta>
            <ta e="T42" id="Seg_1296" s="T41">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T43" id="Seg_1297" s="T42">adv</ta>
            <ta e="T44" id="Seg_1298" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_1299" s="T44">v-v&gt;v-v:pn</ta>
            <ta e="T46" id="Seg_1300" s="T45">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T47" id="Seg_1301" s="T46">num</ta>
            <ta e="T48" id="Seg_1302" s="T47">n-n:num-n:case</ta>
            <ta e="T49" id="Seg_1303" s="T48">preverb</ta>
            <ta e="T50" id="Seg_1304" s="T49">v-v&gt;v-v:pn</ta>
            <ta e="T51" id="Seg_1305" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_1306" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_1307" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_1308" s="T53">v-v&gt;v-v:pn</ta>
            <ta e="T55" id="Seg_1309" s="T54">adv</ta>
            <ta e="T56" id="Seg_1310" s="T55">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T57" id="Seg_1311" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_1312" s="T57">n-n:num-n:case</ta>
            <ta e="T59" id="Seg_1313" s="T58">v-v&gt;v-v:pn</ta>
            <ta e="T60" id="Seg_1314" s="T59">pers</ta>
            <ta e="T61" id="Seg_1315" s="T60">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T62" id="Seg_1316" s="T61">v-v:inf.poss</ta>
            <ta e="T63" id="Seg_1317" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_1318" s="T63">pp-n:poss-n&gt;adv</ta>
            <ta e="T65" id="Seg_1319" s="T64">adv</ta>
            <ta e="T66" id="Seg_1320" s="T65">v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_1321" s="T66">num</ta>
            <ta e="T68" id="Seg_1322" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_1323" s="T68">num</ta>
            <ta e="T70" id="Seg_1324" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_1325" s="T70">adj</ta>
            <ta e="T72" id="Seg_1326" s="T71">n-n:num-n:case</ta>
            <ta e="T73" id="Seg_1327" s="T72">adv</ta>
            <ta e="T74" id="Seg_1328" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_1329" s="T74">pers</ta>
            <ta e="T76" id="Seg_1330" s="T75">n-n:case-n:poss</ta>
            <ta e="T77" id="Seg_1331" s="T76">v-v&gt;v-v:pn</ta>
            <ta e="T78" id="Seg_1332" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_1333" s="T78">preverb</ta>
            <ta e="T80" id="Seg_1334" s="T79">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T81" id="Seg_1335" s="T80">adv</ta>
            <ta e="T82" id="Seg_1336" s="T81">n-n:case-n:poss</ta>
            <ta e="T83" id="Seg_1337" s="T82">adv</ta>
            <ta e="T84" id="Seg_1338" s="T83">v-v&gt;v-v:pn</ta>
            <ta e="T85" id="Seg_1339" s="T84">n-n:case-n:poss</ta>
            <ta e="T86" id="Seg_1340" s="T85">num</ta>
            <ta e="T87" id="Seg_1341" s="T86">n-n:case-n:poss</ta>
            <ta e="T88" id="Seg_1342" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_1343" s="T88">v-v:ins-v:pn</ta>
            <ta e="T90" id="Seg_1344" s="T89">num</ta>
            <ta e="T91" id="Seg_1345" s="T90">n-n:case-n:poss</ta>
            <ta e="T92" id="Seg_1346" s="T91">n-n:case-n:obl.poss</ta>
            <ta e="T93" id="Seg_1347" s="T92">preverb</ta>
            <ta e="T94" id="Seg_1348" s="T93">v-v&gt;v-v:pn</ta>
            <ta e="T95" id="Seg_1349" s="T94">adv</ta>
            <ta e="T96" id="Seg_1350" s="T95">adv</ta>
            <ta e="T97" id="Seg_1351" s="T96">v-v&gt;v-v:pn</ta>
            <ta e="T98" id="Seg_1352" s="T97">n-n&gt;adv</ta>
            <ta e="T99" id="Seg_1353" s="T98">n-n:case-n:obl.poss</ta>
            <ta e="T100" id="Seg_1354" s="T99">v-v&gt;v-v:pn</ta>
            <ta e="T101" id="Seg_1355" s="T100">n-n:case-n:poss</ta>
            <ta e="T102" id="Seg_1356" s="T101">n-n&gt;adj</ta>
            <ta e="T103" id="Seg_1357" s="T102">n-n&gt;adj</ta>
            <ta e="T104" id="Seg_1358" s="T103">n-n:num-n:case-n:poss</ta>
            <ta e="T105" id="Seg_1359" s="T104">n-n:case</ta>
            <ta e="T106" id="Seg_1360" s="T105">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T107" id="Seg_1361" s="T106">n-n:case-n:poss</ta>
            <ta e="T108" id="Seg_1362" s="T107">n-n:case-n:poss</ta>
            <ta e="T109" id="Seg_1363" s="T108">n-n:case</ta>
            <ta e="T110" id="Seg_1364" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_1365" s="T110">preverb</ta>
            <ta e="T112" id="Seg_1366" s="T111">v-v&gt;v-v:inf-v-v:ins-v:pn</ta>
            <ta e="T113" id="Seg_1367" s="T112">n-n:case-n:poss</ta>
            <ta e="T114" id="Seg_1368" s="T113">n-n&gt;adj</ta>
            <ta e="T115" id="Seg_1369" s="T114">n-n:case</ta>
            <ta e="T116" id="Seg_1370" s="T115">v-v&gt;adv</ta>
            <ta e="T117" id="Seg_1371" s="T116">adv</ta>
            <ta e="T118" id="Seg_1372" s="T117">adv</ta>
            <ta e="T119" id="Seg_1373" s="T118">v-v&gt;v-v:pn</ta>
            <ta e="T120" id="Seg_1374" s="T119">v-v&gt;v-v:ins-v:inf.poss</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1375" s="T0">pers</ta>
            <ta e="T2" id="Seg_1376" s="T1">adv</ta>
            <ta e="T3" id="Seg_1377" s="T2">n</ta>
            <ta e="T4" id="Seg_1378" s="T3">n</ta>
            <ta e="T5" id="Seg_1379" s="T4">adv</ta>
            <ta e="T6" id="Seg_1380" s="T5">v</ta>
            <ta e="T7" id="Seg_1381" s="T6">adv</ta>
            <ta e="T8" id="Seg_1382" s="T7">preverb</ta>
            <ta e="T9" id="Seg_1383" s="T8">v</ta>
            <ta e="T10" id="Seg_1384" s="T9">n</ta>
            <ta e="T11" id="Seg_1385" s="T10">v</ta>
            <ta e="T12" id="Seg_1386" s="T11">adv</ta>
            <ta e="T13" id="Seg_1387" s="T12">adj</ta>
            <ta e="T14" id="Seg_1388" s="T13">n</ta>
            <ta e="T15" id="Seg_1389" s="T14">v</ta>
            <ta e="T16" id="Seg_1390" s="T15">adv</ta>
            <ta e="T17" id="Seg_1391" s="T16">preverb</ta>
            <ta e="T18" id="Seg_1392" s="T17">v</ta>
            <ta e="T19" id="Seg_1393" s="T18">v</ta>
            <ta e="T20" id="Seg_1394" s="T19">adv</ta>
            <ta e="T21" id="Seg_1395" s="T20">adv</ta>
            <ta e="T23" id="Seg_1396" s="T21">v</ta>
            <ta e="T24" id="Seg_1397" s="T23">n</ta>
            <ta e="T25" id="Seg_1398" s="T24">v</ta>
            <ta e="T26" id="Seg_1399" s="T25">interj</ta>
            <ta e="T27" id="Seg_1400" s="T26">n</ta>
            <ta e="T28" id="Seg_1401" s="T27">adv</ta>
            <ta e="T29" id="Seg_1402" s="T28">v</ta>
            <ta e="T30" id="Seg_1403" s="T29">n</ta>
            <ta e="T31" id="Seg_1404" s="T30">n</ta>
            <ta e="T32" id="Seg_1405" s="T31">v</ta>
            <ta e="T33" id="Seg_1406" s="T32">pers</ta>
            <ta e="T34" id="Seg_1407" s="T33">n</ta>
            <ta e="T35" id="Seg_1408" s="T34">v</ta>
            <ta e="T36" id="Seg_1409" s="T35">adj</ta>
            <ta e="T37" id="Seg_1410" s="T36">v</ta>
            <ta e="T38" id="Seg_1411" s="T37">n</ta>
            <ta e="T39" id="Seg_1412" s="T38">v</ta>
            <ta e="T40" id="Seg_1413" s="T39">adv</ta>
            <ta e="T41" id="Seg_1414" s="T40">n</ta>
            <ta e="T42" id="Seg_1415" s="T41">v</ta>
            <ta e="T43" id="Seg_1416" s="T42">adv</ta>
            <ta e="T44" id="Seg_1417" s="T43">n</ta>
            <ta e="T45" id="Seg_1418" s="T44">v</ta>
            <ta e="T46" id="Seg_1419" s="T45">adv</ta>
            <ta e="T47" id="Seg_1420" s="T46">num</ta>
            <ta e="T48" id="Seg_1421" s="T47">n</ta>
            <ta e="T49" id="Seg_1422" s="T48">preverb</ta>
            <ta e="T50" id="Seg_1423" s="T49">v</ta>
            <ta e="T51" id="Seg_1424" s="T50">n</ta>
            <ta e="T52" id="Seg_1425" s="T51">n</ta>
            <ta e="T53" id="Seg_1426" s="T52">n</ta>
            <ta e="T54" id="Seg_1427" s="T53">v</ta>
            <ta e="T55" id="Seg_1428" s="T54">adv</ta>
            <ta e="T56" id="Seg_1429" s="T55">v</ta>
            <ta e="T57" id="Seg_1430" s="T56">n</ta>
            <ta e="T58" id="Seg_1431" s="T57">n</ta>
            <ta e="T59" id="Seg_1432" s="T58">v</ta>
            <ta e="T60" id="Seg_1433" s="T59">pers</ta>
            <ta e="T61" id="Seg_1434" s="T60">v</ta>
            <ta e="T62" id="Seg_1435" s="T61">v</ta>
            <ta e="T63" id="Seg_1436" s="T62">n</ta>
            <ta e="T64" id="Seg_1437" s="T63">pp</ta>
            <ta e="T65" id="Seg_1438" s="T64">adv</ta>
            <ta e="T66" id="Seg_1439" s="T65">v</ta>
            <ta e="T67" id="Seg_1440" s="T66">num</ta>
            <ta e="T68" id="Seg_1441" s="T67">n</ta>
            <ta e="T69" id="Seg_1442" s="T68">num</ta>
            <ta e="T70" id="Seg_1443" s="T69">n</ta>
            <ta e="T71" id="Seg_1444" s="T70">adj</ta>
            <ta e="T72" id="Seg_1445" s="T71">n</ta>
            <ta e="T73" id="Seg_1446" s="T72">adv</ta>
            <ta e="T74" id="Seg_1447" s="T73">v</ta>
            <ta e="T75" id="Seg_1448" s="T74">pers</ta>
            <ta e="T76" id="Seg_1449" s="T75">n</ta>
            <ta e="T77" id="Seg_1450" s="T76">v</ta>
            <ta e="T78" id="Seg_1451" s="T77">n</ta>
            <ta e="T79" id="Seg_1452" s="T78">preverb</ta>
            <ta e="T80" id="Seg_1453" s="T79">v</ta>
            <ta e="T81" id="Seg_1454" s="T80">adv</ta>
            <ta e="T82" id="Seg_1455" s="T81">n</ta>
            <ta e="T83" id="Seg_1456" s="T82">adv</ta>
            <ta e="T84" id="Seg_1457" s="T83">v</ta>
            <ta e="T85" id="Seg_1458" s="T84">n</ta>
            <ta e="T86" id="Seg_1459" s="T85">num</ta>
            <ta e="T87" id="Seg_1460" s="T86">n</ta>
            <ta e="T88" id="Seg_1461" s="T87">n</ta>
            <ta e="T89" id="Seg_1462" s="T88">v</ta>
            <ta e="T90" id="Seg_1463" s="T89">num</ta>
            <ta e="T91" id="Seg_1464" s="T90">n</ta>
            <ta e="T92" id="Seg_1465" s="T91">n</ta>
            <ta e="T93" id="Seg_1466" s="T92">preverb</ta>
            <ta e="T94" id="Seg_1467" s="T93">v</ta>
            <ta e="T95" id="Seg_1468" s="T94">adv</ta>
            <ta e="T96" id="Seg_1469" s="T95">adv</ta>
            <ta e="T97" id="Seg_1470" s="T96">v</ta>
            <ta e="T98" id="Seg_1471" s="T97">adv</ta>
            <ta e="T99" id="Seg_1472" s="T98">n</ta>
            <ta e="T100" id="Seg_1473" s="T99">v</ta>
            <ta e="T101" id="Seg_1474" s="T100">n</ta>
            <ta e="T102" id="Seg_1475" s="T101">adj</ta>
            <ta e="T103" id="Seg_1476" s="T102">adj</ta>
            <ta e="T104" id="Seg_1477" s="T103">n</ta>
            <ta e="T105" id="Seg_1478" s="T104">n</ta>
            <ta e="T106" id="Seg_1479" s="T105">v</ta>
            <ta e="T107" id="Seg_1480" s="T106">n</ta>
            <ta e="T108" id="Seg_1481" s="T107">n</ta>
            <ta e="T109" id="Seg_1482" s="T108">n</ta>
            <ta e="T110" id="Seg_1483" s="T109">n</ta>
            <ta e="T111" id="Seg_1484" s="T110">preverb</ta>
            <ta e="T112" id="Seg_1485" s="T111">v</ta>
            <ta e="T113" id="Seg_1486" s="T112">n</ta>
            <ta e="T114" id="Seg_1487" s="T113">adj</ta>
            <ta e="T115" id="Seg_1488" s="T114">n</ta>
            <ta e="T116" id="Seg_1489" s="T115">adv</ta>
            <ta e="T117" id="Seg_1490" s="T116">adv</ta>
            <ta e="T118" id="Seg_1491" s="T117">adv</ta>
            <ta e="T119" id="Seg_1492" s="T118">v</ta>
            <ta e="T120" id="Seg_1493" s="T119">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_1494" s="T0">pro.h:A</ta>
            <ta e="T2" id="Seg_1495" s="T1">adv:Time</ta>
            <ta e="T3" id="Seg_1496" s="T2">np:G</ta>
            <ta e="T4" id="Seg_1497" s="T3">np:Th</ta>
            <ta e="T7" id="Seg_1498" s="T6">adv:Time</ta>
            <ta e="T10" id="Seg_1499" s="T9">np:P</ta>
            <ta e="T11" id="Seg_1500" s="T10">0.3.h:A</ta>
            <ta e="T12" id="Seg_1501" s="T11">adv:Time</ta>
            <ta e="T14" id="Seg_1502" s="T13">np:Th</ta>
            <ta e="T15" id="Seg_1503" s="T14">0.3.h:A</ta>
            <ta e="T16" id="Seg_1504" s="T15">adv:Time</ta>
            <ta e="T20" id="Seg_1505" s="T19">adv:G</ta>
            <ta e="T21" id="Seg_1506" s="T20">adv:G</ta>
            <ta e="T23" id="Seg_1507" s="T21">0.3.h:A</ta>
            <ta e="T24" id="Seg_1508" s="T23">np:R</ta>
            <ta e="T25" id="Seg_1509" s="T24">0.3.h:A</ta>
            <ta e="T27" id="Seg_1510" s="T26">np:Th</ta>
            <ta e="T28" id="Seg_1511" s="T27">adv:G</ta>
            <ta e="T29" id="Seg_1512" s="T28">0.2:A</ta>
            <ta e="T30" id="Seg_1513" s="T29">np:A</ta>
            <ta e="T31" id="Seg_1514" s="T30">np:P</ta>
            <ta e="T33" id="Seg_1515" s="T32">pro.h:A</ta>
            <ta e="T34" id="Seg_1516" s="T33">np:Th</ta>
            <ta e="T36" id="Seg_1517" s="T35">np:Th</ta>
            <ta e="T37" id="Seg_1518" s="T36">0.3.h:A</ta>
            <ta e="T38" id="Seg_1519" s="T37">np:Th</ta>
            <ta e="T39" id="Seg_1520" s="T38">0.3.h:A</ta>
            <ta e="T40" id="Seg_1521" s="T39">adv:Time</ta>
            <ta e="T41" id="Seg_1522" s="T40">np:Th</ta>
            <ta e="T42" id="Seg_1523" s="T41">0.3.h:A</ta>
            <ta e="T43" id="Seg_1524" s="T42">adv:Time</ta>
            <ta e="T44" id="Seg_1525" s="T43">np:G</ta>
            <ta e="T45" id="Seg_1526" s="T44">0.3.h:A</ta>
            <ta e="T48" id="Seg_1527" s="T47">np:Path</ta>
            <ta e="T50" id="Seg_1528" s="T49">0.3.h:A</ta>
            <ta e="T52" id="Seg_1529" s="T51">np:Poss</ta>
            <ta e="T53" id="Seg_1530" s="T52">np:Th</ta>
            <ta e="T54" id="Seg_1531" s="T53">0.3.h:B</ta>
            <ta e="T55" id="Seg_1532" s="T54">adv:Time</ta>
            <ta e="T56" id="Seg_1533" s="T55">0.3.h:A 0.3:Th</ta>
            <ta e="T57" id="Seg_1534" s="T56">np:L</ta>
            <ta e="T58" id="Seg_1535" s="T57">np:A</ta>
            <ta e="T60" id="Seg_1536" s="T59">pro.h:E</ta>
            <ta e="T62" id="Seg_1537" s="T61">0.1.h:A</ta>
            <ta e="T63" id="Seg_1538" s="T62">pp:L</ta>
            <ta e="T65" id="Seg_1539" s="T64">adv:Time</ta>
            <ta e="T66" id="Seg_1540" s="T65">0.1.h:A</ta>
            <ta e="T70" id="Seg_1541" s="T69">np:P</ta>
            <ta e="T72" id="Seg_1542" s="T71">np:A</ta>
            <ta e="T73" id="Seg_1543" s="T72">adv:Time</ta>
            <ta e="T75" id="Seg_1544" s="T74">pro.h:A</ta>
            <ta e="T76" id="Seg_1545" s="T75">np:P</ta>
            <ta e="T78" id="Seg_1546" s="T77">np:P</ta>
            <ta e="T80" id="Seg_1547" s="T79">0.3.h:A</ta>
            <ta e="T81" id="Seg_1548" s="T80">adv:Time</ta>
            <ta e="T82" id="Seg_1549" s="T81">np:Th</ta>
            <ta e="T84" id="Seg_1550" s="T83">0.3.h:A</ta>
            <ta e="T85" id="Seg_1551" s="T84">np:Poss</ta>
            <ta e="T87" id="Seg_1552" s="T86">np:Th</ta>
            <ta e="T88" id="Seg_1553" s="T87">np:L</ta>
            <ta e="T89" id="Seg_1554" s="T88">0.3.h:A</ta>
            <ta e="T91" id="Seg_1555" s="T90">np:Th</ta>
            <ta e="T92" id="Seg_1556" s="T91">np:L</ta>
            <ta e="T94" id="Seg_1557" s="T93">0.3.h:A</ta>
            <ta e="T95" id="Seg_1558" s="T94">adv:Time</ta>
            <ta e="T96" id="Seg_1559" s="T95">adv:G</ta>
            <ta e="T97" id="Seg_1560" s="T96">0.3.h:A</ta>
            <ta e="T98" id="Seg_1561" s="T97">adv:Time</ta>
            <ta e="T99" id="Seg_1562" s="T98">adv:G</ta>
            <ta e="T100" id="Seg_1563" s="T99">0.3.h:A</ta>
            <ta e="T101" id="Seg_1564" s="T100">np:Poss</ta>
            <ta e="T104" id="Seg_1565" s="T103">np:Th</ta>
            <ta e="T105" id="Seg_1566" s="T104">np:G</ta>
            <ta e="T106" id="Seg_1567" s="T105">0.3.h:A</ta>
            <ta e="T107" id="Seg_1568" s="T106">np.h:A 0.3.h:Poss</ta>
            <ta e="T108" id="Seg_1569" s="T107">np:Poss</ta>
            <ta e="T110" id="Seg_1570" s="T109">np:P</ta>
            <ta e="T113" id="Seg_1571" s="T112">np:Poss</ta>
            <ta e="T115" id="Seg_1572" s="T114">np:P</ta>
            <ta e="T117" id="Seg_1573" s="T116">adv:Time</ta>
            <ta e="T118" id="Seg_1574" s="T117">adv:G</ta>
            <ta e="T119" id="Seg_1575" s="T118">0.3.h:A 0.3:Th</ta>
            <ta e="T120" id="Seg_1576" s="T119">0.3:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_1577" s="T0">pro.h:S</ta>
            <ta e="T5" id="Seg_1578" s="T3">s:purp</ta>
            <ta e="T6" id="Seg_1579" s="T5">v:pred</ta>
            <ta e="T9" id="Seg_1580" s="T6">s:temp</ta>
            <ta e="T10" id="Seg_1581" s="T9">np:O</ta>
            <ta e="T11" id="Seg_1582" s="T10">0.3.h:S v:pred</ta>
            <ta e="T14" id="Seg_1583" s="T13">np:O</ta>
            <ta e="T15" id="Seg_1584" s="T14">0.3.h:S v:pred</ta>
            <ta e="T18" id="Seg_1585" s="T15">s:adv</ta>
            <ta e="T19" id="Seg_1586" s="T18">s:adv</ta>
            <ta e="T23" id="Seg_1587" s="T21">0.3.h:S v:pred</ta>
            <ta e="T25" id="Seg_1588" s="T24">0.3.h:S v:pred</ta>
            <ta e="T27" id="Seg_1589" s="T26">np:O</ta>
            <ta e="T29" id="Seg_1590" s="T28">0.2:S v:pred</ta>
            <ta e="T30" id="Seg_1591" s="T29">np:S</ta>
            <ta e="T31" id="Seg_1592" s="T30">np:O</ta>
            <ta e="T32" id="Seg_1593" s="T31">v:pred</ta>
            <ta e="T33" id="Seg_1594" s="T32">pro.h:S</ta>
            <ta e="T34" id="Seg_1595" s="T33">np:O</ta>
            <ta e="T35" id="Seg_1596" s="T34">v:pred</ta>
            <ta e="T37" id="Seg_1597" s="T35">s:purp</ta>
            <ta e="T38" id="Seg_1598" s="T37">np:O</ta>
            <ta e="T39" id="Seg_1599" s="T38">0.3.h:S v:pred</ta>
            <ta e="T41" id="Seg_1600" s="T40">np:O</ta>
            <ta e="T42" id="Seg_1601" s="T41">0.3.h:S v:pred</ta>
            <ta e="T45" id="Seg_1602" s="T44">0.3.h:S v:pred</ta>
            <ta e="T46" id="Seg_1603" s="T45">s:temp</ta>
            <ta e="T50" id="Seg_1604" s="T49">0.3.h:S v:pred</ta>
            <ta e="T53" id="Seg_1605" s="T52">np:O</ta>
            <ta e="T54" id="Seg_1606" s="T53">0.3.h:S v:pred</ta>
            <ta e="T56" id="Seg_1607" s="T55">0.3.h:S 0.3:O v:pred</ta>
            <ta e="T58" id="Seg_1608" s="T57">np:S</ta>
            <ta e="T59" id="Seg_1609" s="T58">v:pred</ta>
            <ta e="T60" id="Seg_1610" s="T59">pro.h:S</ta>
            <ta e="T61" id="Seg_1611" s="T60">v:pred</ta>
            <ta e="T64" id="Seg_1612" s="T61">s:purp</ta>
            <ta e="T66" id="Seg_1613" s="T65">0.1.h:S v:pred</ta>
            <ta e="T70" id="Seg_1614" s="T69">np:O</ta>
            <ta e="T72" id="Seg_1615" s="T71">np:S</ta>
            <ta e="T74" id="Seg_1616" s="T73">v:pred</ta>
            <ta e="T75" id="Seg_1617" s="T74">pro.h:S</ta>
            <ta e="T76" id="Seg_1618" s="T75">np:O</ta>
            <ta e="T77" id="Seg_1619" s="T76">v:pred</ta>
            <ta e="T78" id="Seg_1620" s="T77">np:O</ta>
            <ta e="T80" id="Seg_1621" s="T79">0.3.h:S v:pred</ta>
            <ta e="T82" id="Seg_1622" s="T81">np:O</ta>
            <ta e="T84" id="Seg_1623" s="T83">0.3.h:S v:pred</ta>
            <ta e="T87" id="Seg_1624" s="T86">np:O</ta>
            <ta e="T89" id="Seg_1625" s="T88">0.3.h:S v:pred</ta>
            <ta e="T91" id="Seg_1626" s="T90">np:O</ta>
            <ta e="T94" id="Seg_1627" s="T93">0.3.h:S v:pred</ta>
            <ta e="T97" id="Seg_1628" s="T96">0.3.h:S v:pred</ta>
            <ta e="T100" id="Seg_1629" s="T99">0.3.h:S v:pred</ta>
            <ta e="T104" id="Seg_1630" s="T103">np:O</ta>
            <ta e="T106" id="Seg_1631" s="T105">0.3.h:S v:pred</ta>
            <ta e="T107" id="Seg_1632" s="T106">np.h:S</ta>
            <ta e="T110" id="Seg_1633" s="T109">np:O</ta>
            <ta e="T112" id="Seg_1634" s="T111">v:pred</ta>
            <ta e="T117" id="Seg_1635" s="T112">s:temp</ta>
            <ta e="T119" id="Seg_1636" s="T118">0.3.h:S 0.3:O v:pred</ta>
            <ta e="T120" id="Seg_1637" s="T119">s:purp</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_1638" s="T0">accs-sit</ta>
            <ta e="T2" id="Seg_1639" s="T1">accs-sit</ta>
            <ta e="T3" id="Seg_1640" s="T2">accs-gen</ta>
            <ta e="T4" id="Seg_1641" s="T3">accs-gen</ta>
            <ta e="T7" id="Seg_1642" s="T6">accs-sit</ta>
            <ta e="T10" id="Seg_1643" s="T9">new</ta>
            <ta e="T11" id="Seg_1644" s="T10">0.giv-active</ta>
            <ta e="T14" id="Seg_1645" s="T13">new</ta>
            <ta e="T15" id="Seg_1646" s="T14">0.giv-active</ta>
            <ta e="T23" id="Seg_1647" s="T21">0.giv-active</ta>
            <ta e="T24" id="Seg_1648" s="T23">new</ta>
            <ta e="T25" id="Seg_1649" s="T24">0.giv-active quot-sp</ta>
            <ta e="T27" id="Seg_1650" s="T26">giv-inactive-Q</ta>
            <ta e="T29" id="Seg_1651" s="T28">0.giv-active-Q</ta>
            <ta e="T30" id="Seg_1652" s="T29">giv-active</ta>
            <ta e="T31" id="Seg_1653" s="T30">giv-active</ta>
            <ta e="T33" id="Seg_1654" s="T32">giv-inactive</ta>
            <ta e="T34" id="Seg_1655" s="T33">new</ta>
            <ta e="T36" id="Seg_1656" s="T35">giv-active</ta>
            <ta e="T38" id="Seg_1657" s="T37">accs-inf</ta>
            <ta e="T39" id="Seg_1658" s="T38">0.giv-active</ta>
            <ta e="T41" id="Seg_1659" s="T40">new</ta>
            <ta e="T42" id="Seg_1660" s="T41">0.giv-active</ta>
            <ta e="T44" id="Seg_1661" s="T43">giv-inactive</ta>
            <ta e="T45" id="Seg_1662" s="T44">0.giv-active</ta>
            <ta e="T48" id="Seg_1663" s="T47">accs-gen</ta>
            <ta e="T50" id="Seg_1664" s="T49">0.giv-active</ta>
            <ta e="T53" id="Seg_1665" s="T52">new</ta>
            <ta e="T54" id="Seg_1666" s="T53">0.giv-active</ta>
            <ta e="T56" id="Seg_1667" s="T55">0.giv-active 0.giv-inactive</ta>
            <ta e="T57" id="Seg_1668" s="T56">accs-gen</ta>
            <ta e="T58" id="Seg_1669" s="T57">giv-active</ta>
            <ta e="T60" id="Seg_1670" s="T59">giv-inactive</ta>
            <ta e="T63" id="Seg_1671" s="T62">accs-gen</ta>
            <ta e="T66" id="Seg_1672" s="T65">0.giv-active</ta>
            <ta e="T70" id="Seg_1673" s="T69">accs-inf</ta>
            <ta e="T72" id="Seg_1674" s="T71">giv-inactive</ta>
            <ta e="T75" id="Seg_1675" s="T74">giv-inactive</ta>
            <ta e="T76" id="Seg_1676" s="T75">giv-inactive</ta>
            <ta e="T78" id="Seg_1677" s="T77">accs-inf</ta>
            <ta e="T80" id="Seg_1678" s="T79">0.giv-active</ta>
            <ta e="T82" id="Seg_1679" s="T81">accs-inf</ta>
            <ta e="T84" id="Seg_1680" s="T83">0.giv-active</ta>
            <ta e="T87" id="Seg_1681" s="T86">giv-active</ta>
            <ta e="T88" id="Seg_1682" s="T87">accs-gen</ta>
            <ta e="T89" id="Seg_1683" s="T88">0.giv-active</ta>
            <ta e="T91" id="Seg_1684" s="T90">accs-sit</ta>
            <ta e="T92" id="Seg_1685" s="T91">giv-inactive</ta>
            <ta e="T94" id="Seg_1686" s="T93">0.giv-active</ta>
            <ta e="T96" id="Seg_1687" s="T95">accs-gen</ta>
            <ta e="T97" id="Seg_1688" s="T96">0.giv-active</ta>
            <ta e="T99" id="Seg_1689" s="T98">accs-sit</ta>
            <ta e="T100" id="Seg_1690" s="T99">0.giv-active</ta>
            <ta e="T104" id="Seg_1691" s="T103">accs-inf</ta>
            <ta e="T105" id="Seg_1692" s="T104">giv-active</ta>
            <ta e="T106" id="Seg_1693" s="T105">0.giv-active</ta>
            <ta e="T107" id="Seg_1694" s="T106">new</ta>
            <ta e="T110" id="Seg_1695" s="T109">accs-inf</ta>
            <ta e="T115" id="Seg_1696" s="T114">giv-active</ta>
            <ta e="T118" id="Seg_1697" s="T117">accs-gen</ta>
            <ta e="T119" id="Seg_1698" s="T118">0.giv-active</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_1699" s="T0">Я завтра поеду в лес охотиться на оленей.</ta>
            <ta e="T11" id="Seg_1700" s="T6">Утром, встав, пил чай.</ta>
            <ta e="T15" id="Seg_1701" s="T11">Потом надел пимы.</ta>
            <ta e="T23" id="Seg_1702" s="T15">Потом, встав, вышел на улицу.</ta>
            <ta e="T29" id="Seg_1703" s="T23">[Своей] собаке кричал: "Ату, гони вниз оленей".</ta>
            <ta e="T32" id="Seg_1704" s="T29">Собака погнала оленей.</ta>
            <ta e="T37" id="Seg_1705" s="T32">Он взял аркан, [чтобы] ловить оленей.</ta>
            <ta e="T42" id="Seg_1706" s="T37">Оленей поймал, потом нарты запряг.</ta>
            <ta e="T46" id="Seg_1707" s="T42">Потом поехал в лес и гонял [диких оленей].</ta>
            <ta e="T50" id="Seg_1708" s="T46">Два [раза] переезжал тундру.</ta>
            <ta e="T54" id="Seg_1709" s="T50">Нашёл след диких оленей.</ta>
            <ta e="T57" id="Seg_1710" s="T54">Гонял [оленей] в тайге.</ta>
            <ta e="T59" id="Seg_1711" s="T57">Олени бежали.</ta>
            <ta e="T64" id="Seg_1712" s="T59">Я хотел подойти, чтобы стрелять против ветра.</ta>
            <ta e="T70" id="Seg_1713" s="T64">Потом я застрелил [за] один раз два хора.</ta>
            <ta e="T74" id="Seg_1714" s="T70">Остальные олени убежали.</ta>
            <ta e="T80" id="Seg_1715" s="T74">Он оленя ободрал, желудок выпотрошил.</ta>
            <ta e="T84" id="Seg_1716" s="T80">Потом кровь туда вылил.</ta>
            <ta e="T89" id="Seg_1717" s="T84">Одну оленью тушу в снег закопал.</ta>
            <ta e="T94" id="Seg_1718" s="T89">Одну [тушу] на нарты положил.</ta>
            <ta e="T97" id="Seg_1719" s="T94">Потом домой поехал.</ta>
            <ta e="T100" id="Seg_1720" s="T97">Вечером домой приехал.</ta>
            <ta e="T106" id="Seg_1721" s="T100">Оленьи кости и мозги занес домой.</ta>
            <ta e="T112" id="Seg_1722" s="T106">Жена стала олений камыс обдирать.</ta>
            <ta e="T120" id="Seg_1723" s="T112">Ободрав олений камыс, отнесла [его] потом на улицу, сохнуть.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_1724" s="T0">Tomorrow I will go to the forest to hunt reindeer.</ta>
            <ta e="T11" id="Seg_1725" s="T6">In the morning [he] got up and drank tea.</ta>
            <ta e="T15" id="Seg_1726" s="T11">Then [he] put on deerskin boots.</ta>
            <ta e="T23" id="Seg_1727" s="T15">Then [he] stood up and went outside.</ta>
            <ta e="T29" id="Seg_1728" s="T23">He shouted to his dog: "Tally-ho, shepherd reindeer down".</ta>
            <ta e="T32" id="Seg_1729" s="T29">The dog shepherded the reindeer.</ta>
            <ta e="T37" id="Seg_1730" s="T32">He took the rope to catch reindeer.</ta>
            <ta e="T42" id="Seg_1731" s="T37">He caught a reindeer, then he harnessed it to the sledge.</ta>
            <ta e="T46" id="Seg_1732" s="T42">Then he went to the forest and chased [wild reindeer].</ta>
            <ta e="T50" id="Seg_1733" s="T46">He crossed tundra twice.</ta>
            <ta e="T54" id="Seg_1734" s="T50">He found traces of wild reindeer. </ta>
            <ta e="T57" id="Seg_1735" s="T54">He chased [reindeer] in the taiga.</ta>
            <ta e="T59" id="Seg_1736" s="T57">The reindeer were running.</ta>
            <ta e="T64" id="Seg_1737" s="T59">I wanted to come up to shoot against the wind.</ta>
            <ta e="T70" id="Seg_1738" s="T64">Then I shot two bucks at once.</ta>
            <ta e="T74" id="Seg_1739" s="T70">Other reindeer ran away.</ta>
            <ta e="T80" id="Seg_1740" s="T74">He skinned the reindeer, cleaned out [its] stomach.</ta>
            <ta e="T84" id="Seg_1741" s="T80">Then he poured the blood out of them.</ta>
            <ta e="T89" id="Seg_1742" s="T84">He covered one reindeer corpse with snow.</ta>
            <ta e="T94" id="Seg_1743" s="T89">He loaded one [corpse] on the sledge.</ta>
            <ta e="T97" id="Seg_1744" s="T94">Then he went off home.</ta>
            <ta e="T100" id="Seg_1745" s="T97">In the evening he came home. </ta>
            <ta e="T106" id="Seg_1746" s="T100">He brought reindeer's brains and bones into the house.</ta>
            <ta e="T112" id="Seg_1747" s="T106">His wife began to skin the fell from the reindeer's legs.</ta>
            <ta e="T120" id="Seg_1748" s="T112">After she skinned the fell from the reindeer's legs, she brought it outside to dry.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_1749" s="T0">Morgen fahre ich in den Wald, um Rentiere zu jagen.</ta>
            <ta e="T11" id="Seg_1750" s="T6">Am Morgen stand er auf und trank Tee.</ta>
            <ta e="T15" id="Seg_1751" s="T11">Dann zog er Pelzstiefel an.</ta>
            <ta e="T23" id="Seg_1752" s="T15">Dann stand er auf und ging nach draußen.</ta>
            <ta e="T29" id="Seg_1753" s="T23">Er rief seinem Hund zu: "Hussa, treib die Rentiere herunter."</ta>
            <ta e="T32" id="Seg_1754" s="T29">Der Hund trieb die Rentiere.</ta>
            <ta e="T37" id="Seg_1755" s="T32">Er nahm das Seil, um die Rentiere zu fangen.</ta>
            <ta e="T42" id="Seg_1756" s="T37">Er fing ein Rentier, dann spannte er es an den Schlitten an.</ta>
            <ta e="T46" id="Seg_1757" s="T42">Dann fuhr er in den Wald und jagte [Wildrentiere].</ta>
            <ta e="T50" id="Seg_1758" s="T46">Er durchquerte die Tundra zweimal.</ta>
            <ta e="T54" id="Seg_1759" s="T50">Er fand Spuren von Wildrentieren.</ta>
            <ta e="T57" id="Seg_1760" s="T54">Er jagte die Rentiere in der Taiga.</ta>
            <ta e="T59" id="Seg_1761" s="T57">Die Rentiere liefen weg.</ta>
            <ta e="T64" id="Seg_1762" s="T59">Ich wollte näher kommen, um gegen den Wind zu schießen.</ta>
            <ta e="T70" id="Seg_1763" s="T64">Dann schoss ich zwei Böcke auf einmal.</ta>
            <ta e="T74" id="Seg_1764" s="T70">Die anderen Rentiere liefen weg.</ta>
            <ta e="T80" id="Seg_1765" s="T74">Er häutete das Rentier, brach sein Magen auf.</ta>
            <ta e="T84" id="Seg_1766" s="T80">Dann goss er ihr Blut aus.</ta>
            <ta e="T89" id="Seg_1767" s="T84">Er bedeckte einen Rentierkadaver mit Schnee.</ta>
            <ta e="T94" id="Seg_1768" s="T89">Er lud einen [Rentierkadaver] auf den Schlitten.</ta>
            <ta e="T97" id="Seg_1769" s="T94">Dann fuhr er los nach Hause.</ta>
            <ta e="T100" id="Seg_1770" s="T97">Am Abend kam er nach Hause.</ta>
            <ta e="T106" id="Seg_1771" s="T100">Er brachte das Gehirn und die Knochen des Rentiers ins Haus.</ta>
            <ta e="T112" id="Seg_1772" s="T106">Seine Frau fing an, das Fell von den Beinen des Rentiers abzuziehen.</ta>
            <ta e="T120" id="Seg_1773" s="T112">Nachdem sie das Fell von den Beinen des Rentiers abgezogen hatte, brachte sie es nach draußen zum Trocknen.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_1774" s="T0">я завтра в лес оленей искать съезжу</ta>
            <ta e="T11" id="Seg_1775" s="T6">утром встал чай пить</ta>
            <ta e="T15" id="Seg_1776" s="T11">потом стал одевать бакари (селькупские сапоги пимы)</ta>
            <ta e="T29" id="Seg_1777" s="T23">свою собаку кричит иди оленей гони</ta>
            <ta e="T32" id="Seg_1778" s="T29">собака пригнала оленей</ta>
            <ta e="T37" id="Seg_1779" s="T32">Он аркан берет оленей ловить</ta>
            <ta e="T42" id="Seg_1780" s="T37">оленей поймал, потом самку запрягает</ta>
            <ta e="T46" id="Seg_1781" s="T42">потом в лес уехал гонять диких оленей</ta>
            <ta e="T50" id="Seg_1782" s="T46">на две тундры уехал (он)</ta>
            <ta e="T54" id="Seg_1783" s="T50">диких оленей след нашел.</ta>
            <ta e="T57" id="Seg_1784" s="T54">он подкрадывается в тайге</ta>
            <ta e="T59" id="Seg_1785" s="T57">‎‎олени лежат.</ta>
            <ta e="T64" id="Seg_1786" s="T59">‎‎Я уже подкрадываюсь стрелять против ветра</ta>
            <ta e="T70" id="Seg_1787" s="T64">потом застрелил один раз два хора</ta>
            <ta e="T74" id="Seg_1788" s="T70">остальные олени убежали</ta>
            <ta e="T80" id="Seg_1789" s="T74">потом он стал оленей обдирать [ободрал] желудок выжал.</ta>
            <ta e="T84" id="Seg_1790" s="T80">потом кровь (оленя) туда черпал (налил)</ta>
            <ta e="T89" id="Seg_1791" s="T84">одну тушу в снег закопал</ta>
            <ta e="T94" id="Seg_1792" s="T89">одну тушу на санки положил</ta>
            <ta e="T97" id="Seg_1793" s="T94">‎‎потом домой поехал</ta>
            <ta e="T100" id="Seg_1794" s="T97">вечером домой приехал</ta>
            <ta e="T106" id="Seg_1795" s="T100">оленей ноги занес домой</ta>
            <ta e="T112" id="Seg_1796" s="T106">‎‎жена обдирать стала камысы обдирать стала</ta>
            <ta e="T120" id="Seg_1797" s="T112">камысы ободрала, потом понесла на улицу сушить</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T11" id="Seg_1798" s="T6">[OSV:] 1) The storyteller has switched to third person singular; 2) Here the Present Tense is used, but the story probably occured in the past.</ta>
            <ta e="T64" id="Seg_1799" s="T59">[OSV:] The storyteller has switched again into the first person singular.</ta>
            <ta e="T70" id="Seg_1800" s="T64">[SOV:] "qoːrɨ" - "a male deer, a buck".</ta>
            <ta e="T80" id="Seg_1801" s="T74">[OSV:] The storyteller has switched into third person singular.</ta>
            <ta e="T89" id="Seg_1802" s="T84">[OSV:] The nominal form "močʼistɨ" has been edited into "močʼimtɨ".</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T112" id="Seg_1803" s="T106">′понты – камыс</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
