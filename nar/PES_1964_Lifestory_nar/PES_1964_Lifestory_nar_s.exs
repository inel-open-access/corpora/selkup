<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PES_1964_Lifestory_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PES_1964_Lifestory_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">80</ud-information>
            <ud-information attribute-name="# HIAT:w">59</ud-information>
            <ud-information attribute-name="# e">59</ud-information>
            <ud-information attribute-name="# HIAT:u">14</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PES">
            <abbreviation>PES</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PES"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T60" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">tʼelɨksan</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">Koːrelaɣɨn</ts>
                  <nts id="Seg_11" n="HIAT:ip">,</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_14" n="HIAT:w" s="T4">taŋɨn</ts>
                  <nts id="Seg_15" n="HIAT:ip">.</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_18" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">äsewnan</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">äwewnan</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">sombɨlʼe</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">üčet</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">äsan</ts>
                  <nts id="Seg_33" n="HIAT:ip">.</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_36" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">sombɨlʼen</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">galɨk</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">äsan</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_48" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">man</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">assɨ</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">warɣä</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">äsaŋ</ts>
                  <nts id="Seg_60" n="HIAT:ip">,</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_63" n="HIAT:w" s="T17">äwem</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_66" n="HIAT:w" s="T18">qusan</ts>
                  <nts id="Seg_67" n="HIAT:ip">.</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_70" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">man</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">qaːlizan</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">nʼünʼökelʼe</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">i</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_84" n="HIAT:w" s="T23">nʼäinǯokuzaŋ</ts>
                  <nts id="Seg_85" n="HIAT:ip">.</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_88" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_90" n="HIAT:w" s="T24">äsew</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_93" n="HIAT:w" s="T25">assɨ</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_96" n="HIAT:w" s="T26">nädɨkus</ts>
                  <nts id="Seg_97" n="HIAT:ip">,</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_100" n="HIAT:w" s="T27">üčelam</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_103" n="HIAT:w" s="T28">ärambɨkuzat</ts>
                  <nts id="Seg_104" n="HIAT:ip">.</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_107" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_109" n="HIAT:w" s="T29">kulʼdʼi</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_112" n="HIAT:w" s="T30">näjgum</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_115" n="HIAT:w" s="T31">enǯaŋ</ts>
                  <nts id="Seg_116" n="HIAT:ip">,</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">motkunǯiŋ</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_121" n="HIAT:ip">(</nts>
                  <ts e="T34" id="Seg_123" n="HIAT:w" s="T33">tɨttärundi</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_126" n="HIAT:w" s="T34">moʒet</ts>
                  <nts id="Seg_127" n="HIAT:ip">)</nts>
                  <nts id="Seg_128" n="HIAT:ip">,</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_131" n="HIAT:w" s="T35">amnan</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_134" n="HIAT:w" s="T36">kwetʼänǯit</ts>
                  <nts id="Seg_135" n="HIAT:ip">.</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_138" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_140" n="HIAT:w" s="T37">übɨzot</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_143" n="HIAT:w" s="T38">pɨŋgɨri</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_146" n="HIAT:w" s="T39">matʼtʼöndə</ts>
                  <nts id="Seg_147" n="HIAT:ip">.</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_150" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_152" n="HIAT:w" s="T40">nɨtʼän</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_155" n="HIAT:w" s="T41">ilɨzaŋ</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_158" n="HIAT:w" s="T42">nɨːtäŋ</ts>
                  <nts id="Seg_159" n="HIAT:ip">.</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_162" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_164" n="HIAT:w" s="T43">nänno</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_167" n="HIAT:w" s="T44">tibindisaŋ</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_170" n="HIAT:w" s="T45">Aŋondə</ts>
                  <nts id="Seg_171" n="HIAT:ip">.</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_174" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_176" n="HIAT:w" s="T46">šɨttə</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_179" n="HIAT:w" s="T47">qojjam</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_182" n="HIAT:w" s="T48">äsan</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_185" n="HIAT:w" s="T49">man</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_188" n="HIAT:w" s="T50">qwätɨsaw</ts>
                  <nts id="Seg_189" n="HIAT:ip">.</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_192" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_194" n="HIAT:w" s="T51">täpɨlam</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_197" n="HIAT:w" s="T52">äzat</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_200" n="HIAT:w" s="T53">äwat</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_203" n="HIAT:w" s="T54">tʼäŋus</ts>
                  <nts id="Seg_204" n="HIAT:ip">.</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_207" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_209" n="HIAT:w" s="T55">tʼiːjeŋ</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_212" n="HIAT:w" s="T56">äsatät</ts>
                  <nts id="Seg_213" n="HIAT:ip">.</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_216" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_218" n="HIAT:w" s="T57">müːttaɣɨn</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_221" n="HIAT:w" s="T58">qwatpattə</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_224" n="HIAT:w" s="T59">tibequmma</ts>
                  <nts id="Seg_225" n="HIAT:ip">.</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T60" id="Seg_227" n="sc" s="T1">
               <ts e="T2" id="Seg_229" n="e" s="T1">man </ts>
               <ts e="T3" id="Seg_231" n="e" s="T2">tʼelɨksan </ts>
               <ts e="T4" id="Seg_233" n="e" s="T3">Koːrelaɣɨn, </ts>
               <ts e="T5" id="Seg_235" n="e" s="T4">taŋɨn. </ts>
               <ts e="T6" id="Seg_237" n="e" s="T5">äsewnan </ts>
               <ts e="T7" id="Seg_239" n="e" s="T6">äwewnan </ts>
               <ts e="T8" id="Seg_241" n="e" s="T7">sombɨlʼe </ts>
               <ts e="T9" id="Seg_243" n="e" s="T8">üčet </ts>
               <ts e="T10" id="Seg_245" n="e" s="T9">äsan. </ts>
               <ts e="T11" id="Seg_247" n="e" s="T10">sombɨlʼen </ts>
               <ts e="T12" id="Seg_249" n="e" s="T11">galɨk </ts>
               <ts e="T13" id="Seg_251" n="e" s="T12">äsan. </ts>
               <ts e="T14" id="Seg_253" n="e" s="T13">man </ts>
               <ts e="T15" id="Seg_255" n="e" s="T14">assɨ </ts>
               <ts e="T16" id="Seg_257" n="e" s="T15">warɣä </ts>
               <ts e="T17" id="Seg_259" n="e" s="T16">äsaŋ, </ts>
               <ts e="T18" id="Seg_261" n="e" s="T17">äwem </ts>
               <ts e="T19" id="Seg_263" n="e" s="T18">qusan. </ts>
               <ts e="T20" id="Seg_265" n="e" s="T19">man </ts>
               <ts e="T21" id="Seg_267" n="e" s="T20">qaːlizan </ts>
               <ts e="T22" id="Seg_269" n="e" s="T21">nʼünʼökelʼe </ts>
               <ts e="T23" id="Seg_271" n="e" s="T22">i </ts>
               <ts e="T24" id="Seg_273" n="e" s="T23">nʼäinǯokuzaŋ. </ts>
               <ts e="T25" id="Seg_275" n="e" s="T24">äsew </ts>
               <ts e="T26" id="Seg_277" n="e" s="T25">assɨ </ts>
               <ts e="T27" id="Seg_279" n="e" s="T26">nädɨkus, </ts>
               <ts e="T28" id="Seg_281" n="e" s="T27">üčelam </ts>
               <ts e="T29" id="Seg_283" n="e" s="T28">ärambɨkuzat. </ts>
               <ts e="T30" id="Seg_285" n="e" s="T29">kulʼdʼi </ts>
               <ts e="T31" id="Seg_287" n="e" s="T30">näjgum </ts>
               <ts e="T32" id="Seg_289" n="e" s="T31">enǯaŋ, </ts>
               <ts e="T33" id="Seg_291" n="e" s="T32">motkunǯiŋ </ts>
               <ts e="T34" id="Seg_293" n="e" s="T33">(tɨttärundi </ts>
               <ts e="T35" id="Seg_295" n="e" s="T34">moʒet), </ts>
               <ts e="T36" id="Seg_297" n="e" s="T35">amnan </ts>
               <ts e="T37" id="Seg_299" n="e" s="T36">kwetʼänǯit. </ts>
               <ts e="T38" id="Seg_301" n="e" s="T37">übɨzot </ts>
               <ts e="T39" id="Seg_303" n="e" s="T38">pɨŋgɨri </ts>
               <ts e="T40" id="Seg_305" n="e" s="T39">matʼtʼöndə. </ts>
               <ts e="T41" id="Seg_307" n="e" s="T40">nɨtʼän </ts>
               <ts e="T42" id="Seg_309" n="e" s="T41">ilɨzaŋ </ts>
               <ts e="T43" id="Seg_311" n="e" s="T42">nɨːtäŋ. </ts>
               <ts e="T44" id="Seg_313" n="e" s="T43">nänno </ts>
               <ts e="T45" id="Seg_315" n="e" s="T44">tibindisaŋ </ts>
               <ts e="T46" id="Seg_317" n="e" s="T45">Aŋondə. </ts>
               <ts e="T47" id="Seg_319" n="e" s="T46">šɨttə </ts>
               <ts e="T48" id="Seg_321" n="e" s="T47">qojjam </ts>
               <ts e="T49" id="Seg_323" n="e" s="T48">äsan </ts>
               <ts e="T50" id="Seg_325" n="e" s="T49">man </ts>
               <ts e="T51" id="Seg_327" n="e" s="T50">qwätɨsaw. </ts>
               <ts e="T52" id="Seg_329" n="e" s="T51">täpɨlam </ts>
               <ts e="T53" id="Seg_331" n="e" s="T52">äzat </ts>
               <ts e="T54" id="Seg_333" n="e" s="T53">äwat </ts>
               <ts e="T55" id="Seg_335" n="e" s="T54">tʼäŋus. </ts>
               <ts e="T56" id="Seg_337" n="e" s="T55">tʼiːjeŋ </ts>
               <ts e="T57" id="Seg_339" n="e" s="T56">äsatät. </ts>
               <ts e="T58" id="Seg_341" n="e" s="T57">müːttaɣɨn </ts>
               <ts e="T59" id="Seg_343" n="e" s="T58">qwatpattə </ts>
               <ts e="T60" id="Seg_345" n="e" s="T59">tibequmma. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_346" s="T1">PES_1964_Lifestory_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_347" s="T5">PES_1964_Lifestory_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_348" s="T10">PES_1964_Lifestory_nar.003 (001.003)</ta>
            <ta e="T19" id="Seg_349" s="T13">PES_1964_Lifestory_nar.004 (001.004)</ta>
            <ta e="T24" id="Seg_350" s="T19">PES_1964_Lifestory_nar.005 (001.005)</ta>
            <ta e="T29" id="Seg_351" s="T24">PES_1964_Lifestory_nar.006 (001.006)</ta>
            <ta e="T37" id="Seg_352" s="T29">PES_1964_Lifestory_nar.007 (001.007)</ta>
            <ta e="T40" id="Seg_353" s="T37">PES_1964_Lifestory_nar.008 (001.008)</ta>
            <ta e="T43" id="Seg_354" s="T40">PES_1964_Lifestory_nar.009 (001.009)</ta>
            <ta e="T46" id="Seg_355" s="T43">PES_1964_Lifestory_nar.010 (001.010)</ta>
            <ta e="T51" id="Seg_356" s="T46">PES_1964_Lifestory_nar.011 (001.011)</ta>
            <ta e="T55" id="Seg_357" s="T51">PES_1964_Lifestory_nar.012 (001.012)</ta>
            <ta e="T57" id="Seg_358" s="T55">PES_1964_Lifestory_nar.013 (001.013)</ta>
            <ta e="T60" id="Seg_359" s="T57">PES_1964_Lifestory_nar.014 (001.014)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_360" s="T1">ман ′тʼелыксан ′ко̄ре‵лаɣын, та′ңын.</ta>
            <ta e="T10" id="Seg_361" s="T5">ӓ′сеwнан ӓ′вевнан сомбы′лʼе ӱ′чет ′ӓсан.</ta>
            <ta e="T13" id="Seg_362" s="T10">сомбы′лʼен галык ′ӓсан.</ta>
            <ta e="T19" id="Seg_363" s="T13">ман ас′сы вар′ɣӓ ӓ′саң, ӓ′вем ′kусан.</ta>
            <ta e="T24" id="Seg_364" s="T19">ман kа̄ли′зан ′нʼӱнʼӧкелʼе и ′нʼӓинджокузаң.</ta>
            <ta e="T29" id="Seg_365" s="T24">ӓ′сеw а′ссы ′нӓдыкус, ӱ′челам ′ӓрамбыку′зат.</ta>
            <ta e="T37" id="Seg_366" s="T29">кулʼдʼ(и) ′нӓйг(ɣ)ум ′енджаң, ′моткунджиң (тыттӓрунди может), ам′нан ′кветʼӓ(е)нджит.</ta>
            <ta e="T40" id="Seg_367" s="T37">′ӱбызот ′пыңгыри ма′тʼтʼӧндъ.</ta>
            <ta e="T43" id="Seg_368" s="T40">ны′тʼӓн илы′заң ны̄′тӓң.</ta>
            <ta e="T46" id="Seg_369" s="T43">′нӓнно ′тибиндисаң ′аңондъ.</ta>
            <ta e="T51" id="Seg_370" s="T46">′шыттъ kо′(й)jам ′ӓсан ман ′kwӓты‵саw.</ta>
            <ta e="T55" id="Seg_371" s="T51">тӓпы′лам ′ӓзат ′ӓwат ′тʼӓңус.</ta>
            <ta e="T57" id="Seg_372" s="T55">тʼӣ′jең ′ӓсатӓт.</ta>
            <ta e="T60" id="Seg_373" s="T57">′мӱ̄тта(ъ)ɣын kwат′паттъ ′тибеkумма.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_374" s="T1">man tʼelɨksan koːrelaɣɨn, taŋɨn.</ta>
            <ta e="T10" id="Seg_375" s="T5">äsewnan ävevnan sombɨlʼe üčet äsan.</ta>
            <ta e="T13" id="Seg_376" s="T10">sombɨlʼen galɨk äsan.</ta>
            <ta e="T19" id="Seg_377" s="T13">man assɨ varɣä äsaŋ, ävem qusan.</ta>
            <ta e="T24" id="Seg_378" s="T19">man qaːlizan nʼünʼökelʼe i nʼäinǯokuzaŋ.</ta>
            <ta e="T29" id="Seg_379" s="T24">äsew assɨ nädɨkus, üčelam ärambɨkuzat.</ta>
            <ta e="T37" id="Seg_380" s="T29">kulʼdʼ(i) näjg(ɣ)um enǯaŋ, motkunǯiŋ (tɨttärundi moʒet), amnan kvetʼä(e)nǯit.</ta>
            <ta e="T40" id="Seg_381" s="T37">übɨzot pɨŋgɨri matʼtʼöndə.</ta>
            <ta e="T43" id="Seg_382" s="T40">nɨtʼän ilɨzaŋ nɨːtäŋ.</ta>
            <ta e="T46" id="Seg_383" s="T43">nänno tibindisaŋ aŋondə.</ta>
            <ta e="T51" id="Seg_384" s="T46">šɨttə qo(j)jam äsan man qwätɨsaw.</ta>
            <ta e="T55" id="Seg_385" s="T51">täpɨlam äzat äwat tʼäŋus.</ta>
            <ta e="T57" id="Seg_386" s="T55">tʼiːjeŋ äsatät.</ta>
            <ta e="T60" id="Seg_387" s="T57">müːtta(ə)ɣɨn qwatpattə tibequmma.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_388" s="T1">man tʼelɨksan Koːrelaɣɨn, taŋɨn. </ta>
            <ta e="T10" id="Seg_389" s="T5">äsewnan äwewnan sombɨlʼe üčet äsan. </ta>
            <ta e="T13" id="Seg_390" s="T10">sombɨlʼen galɨk äsan. </ta>
            <ta e="T19" id="Seg_391" s="T13">man assɨ warɣä äsaŋ, äwem qusan. </ta>
            <ta e="T24" id="Seg_392" s="T19">man qaːlizan nʼünʼökelʼe i nʼäinǯokuzaŋ. </ta>
            <ta e="T29" id="Seg_393" s="T24">äsew assɨ nädɨkus, üčelam ärambɨkuzat. </ta>
            <ta e="T37" id="Seg_394" s="T29">kulʼdʼi näjgum enǯaŋ, motkunǯiŋ (tɨttärundi moʒet), amnan kwetʼänǯit. </ta>
            <ta e="T40" id="Seg_395" s="T37">übɨzot pɨŋgɨri matʼtʼöndə. </ta>
            <ta e="T43" id="Seg_396" s="T40">nɨtʼän ilɨzaŋ nɨːtäŋ. </ta>
            <ta e="T46" id="Seg_397" s="T43">nänno tibindisaŋ Aŋondə. </ta>
            <ta e="T51" id="Seg_398" s="T46">šɨttə qojjam äsan man qwätɨsaw. </ta>
            <ta e="T55" id="Seg_399" s="T51">täpɨlam äzat äwat tʼäŋus. </ta>
            <ta e="T57" id="Seg_400" s="T55">tʼiːjeŋ äsatät. </ta>
            <ta e="T60" id="Seg_401" s="T57">müːttaɣɨn qwatpattə tibequmma. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_402" s="T1">man</ta>
            <ta e="T3" id="Seg_403" s="T2">tʼelɨ-k-sa-n</ta>
            <ta e="T4" id="Seg_404" s="T3">Koːrela-ɣɨn</ta>
            <ta e="T5" id="Seg_405" s="T4">taŋɨ-n</ta>
            <ta e="T6" id="Seg_406" s="T5">äse-w-nan</ta>
            <ta e="T7" id="Seg_407" s="T6">äwe-w-nan</ta>
            <ta e="T8" id="Seg_408" s="T7">sombɨlʼe</ta>
            <ta e="T9" id="Seg_409" s="T8">üče-t</ta>
            <ta e="T10" id="Seg_410" s="T9">ä-sa-ŋ</ta>
            <ta e="T11" id="Seg_411" s="T10">sombɨlʼe-n</ta>
            <ta e="T12" id="Seg_412" s="T11">galɨ-k</ta>
            <ta e="T13" id="Seg_413" s="T12">ä-sa-ŋ</ta>
            <ta e="T14" id="Seg_414" s="T13">man</ta>
            <ta e="T15" id="Seg_415" s="T14">assɨ</ta>
            <ta e="T16" id="Seg_416" s="T15">warɣä</ta>
            <ta e="T17" id="Seg_417" s="T16">ä-sa-ŋ</ta>
            <ta e="T18" id="Seg_418" s="T17">äwe-mɨ</ta>
            <ta e="T19" id="Seg_419" s="T18">qu-sa-n</ta>
            <ta e="T20" id="Seg_420" s="T19">man</ta>
            <ta e="T21" id="Seg_421" s="T20">qaːli-za-n</ta>
            <ta e="T22" id="Seg_422" s="T21">nʼünʼö-ke-lʼe</ta>
            <ta e="T23" id="Seg_423" s="T22">i</ta>
            <ta e="T24" id="Seg_424" s="T23">nʼäi-n-ǯo-ku-za-ŋ</ta>
            <ta e="T25" id="Seg_425" s="T24">äse-w</ta>
            <ta e="T26" id="Seg_426" s="T25">assɨ</ta>
            <ta e="T27" id="Seg_427" s="T26">nädɨ-ku-s</ta>
            <ta e="T28" id="Seg_428" s="T27">üče-la-m</ta>
            <ta e="T29" id="Seg_429" s="T28">ära-mbɨ-ku-za-t</ta>
            <ta e="T30" id="Seg_430" s="T29">kulʼdʼi</ta>
            <ta e="T31" id="Seg_431" s="T30">nä-j-gum</ta>
            <ta e="T32" id="Seg_432" s="T31">e-nǯa-n</ta>
            <ta e="T33" id="Seg_433" s="T32">mot-ku-nǯi-ŋ</ta>
            <ta e="T34" id="Seg_434" s="T33">tɨttäru-ndi</ta>
            <ta e="T35" id="Seg_435" s="T34">moʒet</ta>
            <ta e="T36" id="Seg_436" s="T35">amna-n</ta>
            <ta e="T37" id="Seg_437" s="T36">kwetʼä-nǯi-t</ta>
            <ta e="T38" id="Seg_438" s="T37">übɨ-zo-t</ta>
            <ta e="T39" id="Seg_439" s="T38">pɨŋgɨr-i</ta>
            <ta e="T40" id="Seg_440" s="T39">matʼtʼö-ndə</ta>
            <ta e="T41" id="Seg_441" s="T40">nɨtʼä-n</ta>
            <ta e="T42" id="Seg_442" s="T41">ilɨ-za-ŋ</ta>
            <ta e="T43" id="Seg_443" s="T42">nɨːtä-ŋ</ta>
            <ta e="T44" id="Seg_444" s="T43">nänno</ta>
            <ta e="T45" id="Seg_445" s="T44">tibindi-sa-ŋ</ta>
            <ta e="T46" id="Seg_446" s="T45">Aŋo-ndə</ta>
            <ta e="T47" id="Seg_447" s="T46">šɨttə</ta>
            <ta e="T48" id="Seg_448" s="T47">qojja-mɨ</ta>
            <ta e="T49" id="Seg_449" s="T48">ä-sa-ŋ</ta>
            <ta e="T50" id="Seg_450" s="T49">man</ta>
            <ta e="T51" id="Seg_451" s="T50">qwätɨ-sa-w</ta>
            <ta e="T52" id="Seg_452" s="T51">täp-ɨ-la-m</ta>
            <ta e="T53" id="Seg_453" s="T52">äza-t</ta>
            <ta e="T54" id="Seg_454" s="T53">äwa-t</ta>
            <ta e="T55" id="Seg_455" s="T54">tʼäŋu-s</ta>
            <ta e="T56" id="Seg_456" s="T55">tʼiːje-ŋ</ta>
            <ta e="T57" id="Seg_457" s="T56">ä-sa-tät</ta>
            <ta e="T58" id="Seg_458" s="T57">müːtta-ɣɨn</ta>
            <ta e="T59" id="Seg_459" s="T58">qwat-pa-ttə</ta>
            <ta e="T60" id="Seg_460" s="T59">tibe-qum-ma</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_461" s="T1">man</ta>
            <ta e="T3" id="Seg_462" s="T2">tʼeːlɨ-ku-sɨ-ŋ</ta>
            <ta e="T4" id="Seg_463" s="T3">Koːrela-qən</ta>
            <ta e="T5" id="Seg_464" s="T4">taŋɨ-n</ta>
            <ta e="T6" id="Seg_465" s="T5">ässɨ-mɨ-nan</ta>
            <ta e="T7" id="Seg_466" s="T6">äwa-mɨ-nan</ta>
            <ta e="T8" id="Seg_467" s="T7">sombɨlʼe</ta>
            <ta e="T9" id="Seg_468" s="T8">üččə-tɨ</ta>
            <ta e="T10" id="Seg_469" s="T9">eː-sɨ-n</ta>
            <ta e="T11" id="Seg_470" s="T10">sombɨlʼe-n</ta>
            <ta e="T12" id="Seg_471" s="T11">galɨ-k</ta>
            <ta e="T13" id="Seg_472" s="T12">eː-sɨ-n</ta>
            <ta e="T14" id="Seg_473" s="T13">man</ta>
            <ta e="T15" id="Seg_474" s="T14">assɨ</ta>
            <ta e="T16" id="Seg_475" s="T15">wərkə</ta>
            <ta e="T17" id="Seg_476" s="T16">eː-sɨ-ŋ</ta>
            <ta e="T18" id="Seg_477" s="T17">äwa-mɨ</ta>
            <ta e="T19" id="Seg_478" s="T18">quː-sɨ-n</ta>
            <ta e="T20" id="Seg_479" s="T19">man</ta>
            <ta e="T21" id="Seg_480" s="T20">qalɨ-sɨ-ŋ</ta>
            <ta e="T22" id="Seg_481" s="T21">nʼünʼü-ka-%%</ta>
            <ta e="T23" id="Seg_482" s="T22">i</ta>
            <ta e="T24" id="Seg_483" s="T23">nʼaj-ne-ǯə-ku-sɨ-ŋ</ta>
            <ta e="T25" id="Seg_484" s="T24">ässɨ-mɨ</ta>
            <ta e="T26" id="Seg_485" s="T25">assɨ</ta>
            <ta e="T27" id="Seg_486" s="T26">naːtɨ-ku-sɨ</ta>
            <ta e="T28" id="Seg_487" s="T27">üččə-la-m</ta>
            <ta e="T29" id="Seg_488" s="T28">ära-mbɨ-ku-sɨ-tɨ</ta>
            <ta e="T30" id="Seg_489" s="T29">kulʼdi</ta>
            <ta e="T31" id="Seg_490" s="T30">neː-lʼ-qum</ta>
            <ta e="T32" id="Seg_491" s="T31">eː-nǯɨ-n</ta>
            <ta e="T33" id="Seg_492" s="T32">mot-ku-nǯɨ-n</ta>
            <ta e="T34" id="Seg_493" s="T33">tɨttäru-ntɨ</ta>
            <ta e="T35" id="Seg_494" s="T34">moʒət</ta>
            <ta e="T36" id="Seg_495" s="T35">amna-m</ta>
            <ta e="T37" id="Seg_496" s="T36">kwetʼä-nǯɨ-tɨ</ta>
            <ta e="T38" id="Seg_497" s="T37">übə-sɨ-ut</ta>
            <ta e="T39" id="Seg_498" s="T38">pɨŋgɨr-lʼ</ta>
            <ta e="T40" id="Seg_499" s="T39">matʼtʼi-ndɨ</ta>
            <ta e="T41" id="Seg_500" s="T40">natʼtʼa-n</ta>
            <ta e="T42" id="Seg_501" s="T41">illɨ-sɨ-ŋ</ta>
            <ta e="T43" id="Seg_502" s="T42">näde-nqo</ta>
            <ta e="T44" id="Seg_503" s="T43">nɨːnɨ</ta>
            <ta e="T45" id="Seg_504" s="T44">tibindɨ-sɨ-ŋ</ta>
            <ta e="T46" id="Seg_505" s="T45">Aŋo-ndɨ</ta>
            <ta e="T47" id="Seg_506" s="T46">šittə</ta>
            <ta e="T48" id="Seg_507" s="T47">qoja-mɨ</ta>
            <ta e="T49" id="Seg_508" s="T48">eː-sɨ-n</ta>
            <ta e="T50" id="Seg_509" s="T49">man</ta>
            <ta e="T51" id="Seg_510" s="T50">%%-sɨ-m</ta>
            <ta e="T52" id="Seg_511" s="T51">tap-ɨ-la-n</ta>
            <ta e="T53" id="Seg_512" s="T52">ässɨ-tɨ</ta>
            <ta e="T54" id="Seg_513" s="T53">äwa-tɨ</ta>
            <ta e="T55" id="Seg_514" s="T54">tʼäŋu-sɨ</ta>
            <ta e="T56" id="Seg_515" s="T55">tʼiːje-k</ta>
            <ta e="T57" id="Seg_516" s="T56">eː-sɨ-tɨt</ta>
            <ta e="T58" id="Seg_517" s="T57">müːdu-qən</ta>
            <ta e="T59" id="Seg_518" s="T58">qwat-mbɨ-tɨt</ta>
            <ta e="T60" id="Seg_519" s="T59">tebe-qum-mɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_520" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_521" s="T2">be.born-HAB-PST-1SG.S</ta>
            <ta e="T4" id="Seg_522" s="T3">Karelino-LOC</ta>
            <ta e="T5" id="Seg_523" s="T4">downstream-ADV.LOC</ta>
            <ta e="T6" id="Seg_524" s="T5">father-1SG-ADES</ta>
            <ta e="T7" id="Seg_525" s="T6">mother-1SG-ADES</ta>
            <ta e="T8" id="Seg_526" s="T7">five</ta>
            <ta e="T9" id="Seg_527" s="T8">child.[NOM]-3SG</ta>
            <ta e="T10" id="Seg_528" s="T9">be-PST-3SG.S</ta>
            <ta e="T11" id="Seg_529" s="T10">five-GEN</ta>
            <ta e="T12" id="Seg_530" s="T11">%%-ADVZ</ta>
            <ta e="T13" id="Seg_531" s="T12">be-PST-3SG.S</ta>
            <ta e="T14" id="Seg_532" s="T13">I.NOM</ta>
            <ta e="T15" id="Seg_533" s="T14">NEG</ta>
            <ta e="T16" id="Seg_534" s="T15">big</ta>
            <ta e="T17" id="Seg_535" s="T16">be-PST-1SG.S</ta>
            <ta e="T18" id="Seg_536" s="T17">mother.[NOM]-1SG</ta>
            <ta e="T19" id="Seg_537" s="T18">die-PST-3SG.S</ta>
            <ta e="T20" id="Seg_538" s="T19">I.NOM</ta>
            <ta e="T21" id="Seg_539" s="T20">stay-PST-1SG.S</ta>
            <ta e="T22" id="Seg_540" s="T21">small-DIM-%%</ta>
            <ta e="T23" id="Seg_541" s="T22">and</ta>
            <ta e="T24" id="Seg_542" s="T23">bread-VBLZ-DRV-HAB-PST-1SG.S</ta>
            <ta e="T25" id="Seg_543" s="T24">father.[NOM]-1SG</ta>
            <ta e="T26" id="Seg_544" s="T25">NEG</ta>
            <ta e="T27" id="Seg_545" s="T26">get.married-HAB-PST.[3SG.S]</ta>
            <ta e="T28" id="Seg_546" s="T27">child-PL-ACC</ta>
            <ta e="T29" id="Seg_547" s="T28">regret-DUR-HAB-PST-3SG.O</ta>
            <ta e="T30" id="Seg_548" s="T29">which</ta>
            <ta e="T31" id="Seg_549" s="T30">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T32" id="Seg_550" s="T31">be-FUT-3SG.S</ta>
            <ta e="T33" id="Seg_551" s="T32">beat-HAB-FUT-3SG.S</ta>
            <ta e="T34" id="Seg_552" s="T33">fight-PTCP.PRS</ta>
            <ta e="T35" id="Seg_553" s="T34">maybe</ta>
            <ta e="T36" id="Seg_554" s="T35">hunger-ACC</ta>
            <ta e="T37" id="Seg_555" s="T36">starve-FUT-3SG.O</ta>
            <ta e="T38" id="Seg_556" s="T37">set.off-PST-1PL</ta>
            <ta e="T39" id="Seg_557" s="T38">drum-ADJZ</ta>
            <ta e="T40" id="Seg_558" s="T39">taiga-ILL</ta>
            <ta e="T41" id="Seg_559" s="T40">there-ADV.LOC</ta>
            <ta e="T42" id="Seg_560" s="T41">live-PST-1SG.S</ta>
            <ta e="T43" id="Seg_561" s="T42">girl-TRL</ta>
            <ta e="T44" id="Seg_562" s="T43">then</ta>
            <ta e="T45" id="Seg_563" s="T44">marry-PST-1SG.S</ta>
            <ta e="T46" id="Seg_564" s="T45">Lukyano-ILL</ta>
            <ta e="T47" id="Seg_565" s="T46">two</ta>
            <ta e="T48" id="Seg_566" s="T47">younger.sibling-1SG</ta>
            <ta e="T49" id="Seg_567" s="T48">be-PST-3SG.S</ta>
            <ta e="T50" id="Seg_568" s="T49">I.NOM</ta>
            <ta e="T51" id="Seg_569" s="T50">%%-PST-1SG.O</ta>
            <ta e="T52" id="Seg_570" s="T51">(s)he-EP-PL-GEN</ta>
            <ta e="T53" id="Seg_571" s="T52">father-3SG</ta>
            <ta e="T54" id="Seg_572" s="T53">mother-3SG</ta>
            <ta e="T55" id="Seg_573" s="T54">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T56" id="Seg_574" s="T55">orphan-ADVZ</ta>
            <ta e="T57" id="Seg_575" s="T56">be-PST-3PL</ta>
            <ta e="T58" id="Seg_576" s="T57">war-LOC</ta>
            <ta e="T59" id="Seg_577" s="T58">kill-PST.NAR-3PL</ta>
            <ta e="T60" id="Seg_578" s="T59">man-human.being-1SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_579" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_580" s="T2">родиться-HAB-PST-1SG.S</ta>
            <ta e="T4" id="Seg_581" s="T3">Карелино-LOC</ta>
            <ta e="T5" id="Seg_582" s="T4">вниз.по.течению-ADV.LOC</ta>
            <ta e="T6" id="Seg_583" s="T5">отец-1SG-ADES</ta>
            <ta e="T7" id="Seg_584" s="T6">мать-1SG-ADES</ta>
            <ta e="T8" id="Seg_585" s="T7">пять</ta>
            <ta e="T9" id="Seg_586" s="T8">ребёнок.[NOM]-3SG</ta>
            <ta e="T10" id="Seg_587" s="T9">быть-PST-3SG.S</ta>
            <ta e="T11" id="Seg_588" s="T10">пять-GEN</ta>
            <ta e="T12" id="Seg_589" s="T11">%%-ADVZ</ta>
            <ta e="T13" id="Seg_590" s="T12">быть-PST-3SG.S</ta>
            <ta e="T14" id="Seg_591" s="T13">я.NOM</ta>
            <ta e="T15" id="Seg_592" s="T14">NEG</ta>
            <ta e="T16" id="Seg_593" s="T15">большой</ta>
            <ta e="T17" id="Seg_594" s="T16">быть-PST-1SG.S</ta>
            <ta e="T18" id="Seg_595" s="T17">мать.[NOM]-1SG</ta>
            <ta e="T19" id="Seg_596" s="T18">умереть-PST-3SG.S</ta>
            <ta e="T20" id="Seg_597" s="T19">я.NOM</ta>
            <ta e="T21" id="Seg_598" s="T20">остаться-PST-1SG.S</ta>
            <ta e="T22" id="Seg_599" s="T21">маленький-DIM-%%</ta>
            <ta e="T23" id="Seg_600" s="T22">и</ta>
            <ta e="T24" id="Seg_601" s="T23">хлеб-VBLZ-DRV-HAB-PST-1SG.S</ta>
            <ta e="T25" id="Seg_602" s="T24">отец.[NOM]-1SG</ta>
            <ta e="T26" id="Seg_603" s="T25">NEG</ta>
            <ta e="T27" id="Seg_604" s="T26">жениться-HAB-PST.[3SG.S]</ta>
            <ta e="T28" id="Seg_605" s="T27">ребёнок-PL-ACC</ta>
            <ta e="T29" id="Seg_606" s="T28">жалеть-DUR-HAB-PST-3SG.O</ta>
            <ta e="T30" id="Seg_607" s="T29">какой</ta>
            <ta e="T31" id="Seg_608" s="T30">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T32" id="Seg_609" s="T31">быть-FUT-3SG.S</ta>
            <ta e="T33" id="Seg_610" s="T32">бить-HAB-FUT-3SG.S</ta>
            <ta e="T34" id="Seg_611" s="T33">драться-PTCP.PRS</ta>
            <ta e="T35" id="Seg_612" s="T34">может.быть</ta>
            <ta e="T36" id="Seg_613" s="T35">голод-ACC</ta>
            <ta e="T37" id="Seg_614" s="T36">морить-FUT-3SG.O</ta>
            <ta e="T38" id="Seg_615" s="T37">отправиться-PST-1PL</ta>
            <ta e="T39" id="Seg_616" s="T38">бубен-ADJZ</ta>
            <ta e="T40" id="Seg_617" s="T39">тайга-ILL</ta>
            <ta e="T41" id="Seg_618" s="T40">туда-ADV.LOC</ta>
            <ta e="T42" id="Seg_619" s="T41">жить-PST-1SG.S</ta>
            <ta e="T43" id="Seg_620" s="T42">девушка-TRL</ta>
            <ta e="T44" id="Seg_621" s="T43">потом</ta>
            <ta e="T45" id="Seg_622" s="T44">выйти.замуж-PST-1SG.S</ta>
            <ta e="T46" id="Seg_623" s="T45">Лукьяно-ILL</ta>
            <ta e="T47" id="Seg_624" s="T46">два</ta>
            <ta e="T48" id="Seg_625" s="T47">младший.брат/сестра-1SG</ta>
            <ta e="T49" id="Seg_626" s="T48">быть-PST-3SG.S</ta>
            <ta e="T50" id="Seg_627" s="T49">я.NOM</ta>
            <ta e="T51" id="Seg_628" s="T50">%%-PST-1SG.O</ta>
            <ta e="T52" id="Seg_629" s="T51">он(а)-EP-PL-GEN</ta>
            <ta e="T53" id="Seg_630" s="T52">отец-3SG</ta>
            <ta e="T54" id="Seg_631" s="T53">мать-3SG</ta>
            <ta e="T55" id="Seg_632" s="T54">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T56" id="Seg_633" s="T55">сирота-ADVZ</ta>
            <ta e="T57" id="Seg_634" s="T56">быть-PST-3PL</ta>
            <ta e="T58" id="Seg_635" s="T57">война-LOC</ta>
            <ta e="T59" id="Seg_636" s="T58">убить-PST.NAR-3PL</ta>
            <ta e="T60" id="Seg_637" s="T59">мужчина-человек-1SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_638" s="T1">pers</ta>
            <ta e="T3" id="Seg_639" s="T2">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_640" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_641" s="T4">adv-adv:case</ta>
            <ta e="T6" id="Seg_642" s="T5">n-n:poss-n:case</ta>
            <ta e="T7" id="Seg_643" s="T6">n-n:poss-n:case</ta>
            <ta e="T8" id="Seg_644" s="T7">num</ta>
            <ta e="T9" id="Seg_645" s="T8">n.[n:case]-n:poss</ta>
            <ta e="T10" id="Seg_646" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_647" s="T10">num-n:case</ta>
            <ta e="T12" id="Seg_648" s="T11">n-num&gt;adv</ta>
            <ta e="T13" id="Seg_649" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_650" s="T13">pers</ta>
            <ta e="T15" id="Seg_651" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_652" s="T15">adj</ta>
            <ta e="T17" id="Seg_653" s="T16">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_654" s="T17">n.[n:case]-n:poss</ta>
            <ta e="T19" id="Seg_655" s="T18">v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_656" s="T19">pers</ta>
            <ta e="T21" id="Seg_657" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_658" s="T21">adj-n&gt;n</ta>
            <ta e="T23" id="Seg_659" s="T22">conj</ta>
            <ta e="T24" id="Seg_660" s="T23">n-n&gt;v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_661" s="T24">n.[n:case]-n:poss</ta>
            <ta e="T26" id="Seg_662" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_663" s="T26">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T28" id="Seg_664" s="T27">n-n:num-n:case</ta>
            <ta e="T29" id="Seg_665" s="T28">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_666" s="T29">interrog</ta>
            <ta e="T31" id="Seg_667" s="T30">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T32" id="Seg_668" s="T31">v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_669" s="T32">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_670" s="T33">v-v&gt;ptcp</ta>
            <ta e="T35" id="Seg_671" s="T34">v</ta>
            <ta e="T36" id="Seg_672" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_673" s="T36">v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_674" s="T37">v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_675" s="T38">n-n&gt;adj</ta>
            <ta e="T40" id="Seg_676" s="T39">n-n:case</ta>
            <ta e="T41" id="Seg_677" s="T40">adv-adv:case</ta>
            <ta e="T42" id="Seg_678" s="T41">v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_679" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_680" s="T43">adv</ta>
            <ta e="T45" id="Seg_681" s="T44">v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_682" s="T45">nprop-n:case</ta>
            <ta e="T47" id="Seg_683" s="T46">num</ta>
            <ta e="T48" id="Seg_684" s="T47">n-n:poss</ta>
            <ta e="T49" id="Seg_685" s="T48">v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_686" s="T49">pers</ta>
            <ta e="T51" id="Seg_687" s="T50">v:tense-v:pn</ta>
            <ta e="T52" id="Seg_688" s="T51">pers-n:ins-n:num-n:case</ta>
            <ta e="T53" id="Seg_689" s="T52">n-n:poss</ta>
            <ta e="T54" id="Seg_690" s="T53">n-n:poss</ta>
            <ta e="T55" id="Seg_691" s="T54">v-v:tense.[v:pn]</ta>
            <ta e="T56" id="Seg_692" s="T55">n-adj&gt;adv</ta>
            <ta e="T57" id="Seg_693" s="T56">v-v:tense-v:pn</ta>
            <ta e="T58" id="Seg_694" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_695" s="T58">v-v:tense-v:pn</ta>
            <ta e="T60" id="Seg_696" s="T59">n-n-n:poss</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_697" s="T1">pers</ta>
            <ta e="T3" id="Seg_698" s="T2">v</ta>
            <ta e="T4" id="Seg_699" s="T3">n</ta>
            <ta e="T5" id="Seg_700" s="T4">adv</ta>
            <ta e="T6" id="Seg_701" s="T5">n</ta>
            <ta e="T7" id="Seg_702" s="T6">n</ta>
            <ta e="T8" id="Seg_703" s="T7">num</ta>
            <ta e="T9" id="Seg_704" s="T8">n</ta>
            <ta e="T10" id="Seg_705" s="T9">v</ta>
            <ta e="T11" id="Seg_706" s="T10">num</ta>
            <ta e="T12" id="Seg_707" s="T11">adv</ta>
            <ta e="T13" id="Seg_708" s="T12">v</ta>
            <ta e="T14" id="Seg_709" s="T13">pers</ta>
            <ta e="T15" id="Seg_710" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_711" s="T15">adj</ta>
            <ta e="T17" id="Seg_712" s="T16">v</ta>
            <ta e="T18" id="Seg_713" s="T17">n</ta>
            <ta e="T19" id="Seg_714" s="T18">v</ta>
            <ta e="T20" id="Seg_715" s="T19">pers</ta>
            <ta e="T21" id="Seg_716" s="T20">v</ta>
            <ta e="T22" id="Seg_717" s="T21">adj</ta>
            <ta e="T23" id="Seg_718" s="T22">conj</ta>
            <ta e="T24" id="Seg_719" s="T23">v</ta>
            <ta e="T25" id="Seg_720" s="T24">n</ta>
            <ta e="T26" id="Seg_721" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_722" s="T26">v</ta>
            <ta e="T28" id="Seg_723" s="T27">n</ta>
            <ta e="T29" id="Seg_724" s="T28">v</ta>
            <ta e="T30" id="Seg_725" s="T29">interrog</ta>
            <ta e="T31" id="Seg_726" s="T30">adj</ta>
            <ta e="T32" id="Seg_727" s="T31">v</ta>
            <ta e="T33" id="Seg_728" s="T32">v</ta>
            <ta e="T34" id="Seg_729" s="T33">ptcp</ta>
            <ta e="T35" id="Seg_730" s="T34">v</ta>
            <ta e="T36" id="Seg_731" s="T35">n</ta>
            <ta e="T37" id="Seg_732" s="T36">v</ta>
            <ta e="T38" id="Seg_733" s="T37">v</ta>
            <ta e="T39" id="Seg_734" s="T38">adj</ta>
            <ta e="T40" id="Seg_735" s="T39">n</ta>
            <ta e="T41" id="Seg_736" s="T40">adv</ta>
            <ta e="T42" id="Seg_737" s="T41">v</ta>
            <ta e="T43" id="Seg_738" s="T42">n</ta>
            <ta e="T44" id="Seg_739" s="T43">adv</ta>
            <ta e="T45" id="Seg_740" s="T44">v</ta>
            <ta e="T46" id="Seg_741" s="T45">nprop</ta>
            <ta e="T47" id="Seg_742" s="T46">num</ta>
            <ta e="T48" id="Seg_743" s="T47">n</ta>
            <ta e="T49" id="Seg_744" s="T48">v</ta>
            <ta e="T50" id="Seg_745" s="T49">pers</ta>
            <ta e="T52" id="Seg_746" s="T51">pers</ta>
            <ta e="T53" id="Seg_747" s="T52">n</ta>
            <ta e="T54" id="Seg_748" s="T53">n</ta>
            <ta e="T55" id="Seg_749" s="T54">v</ta>
            <ta e="T56" id="Seg_750" s="T55">adv</ta>
            <ta e="T57" id="Seg_751" s="T56">v</ta>
            <ta e="T58" id="Seg_752" s="T57">n</ta>
            <ta e="T59" id="Seg_753" s="T58">v</ta>
            <ta e="T60" id="Seg_754" s="T59">n</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_755" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_756" s="T2">v:pred</ta>
            <ta e="T6" id="Seg_757" s="T5">np.h:S</ta>
            <ta e="T7" id="Seg_758" s="T6">np.h:S</ta>
            <ta e="T10" id="Seg_759" s="T9">v:pred</ta>
            <ta e="T13" id="Seg_760" s="T12">v:pred</ta>
            <ta e="T14" id="Seg_761" s="T13">pro.h:S</ta>
            <ta e="T16" id="Seg_762" s="T15">adj:pred</ta>
            <ta e="T17" id="Seg_763" s="T16">cop</ta>
            <ta e="T19" id="Seg_764" s="T17">s:temp</ta>
            <ta e="T20" id="Seg_765" s="T19">pro.h:S</ta>
            <ta e="T21" id="Seg_766" s="T20">v:pred</ta>
            <ta e="T24" id="Seg_767" s="T23">0.1.h:S v:pred</ta>
            <ta e="T25" id="Seg_768" s="T24">np.h:S</ta>
            <ta e="T27" id="Seg_769" s="T26">v:pred</ta>
            <ta e="T28" id="Seg_770" s="T27">np:O</ta>
            <ta e="T29" id="Seg_771" s="T28">0.3.h:S v:pred</ta>
            <ta e="T31" id="Seg_772" s="T30">np.h:S</ta>
            <ta e="T32" id="Seg_773" s="T31">v:pred</ta>
            <ta e="T33" id="Seg_774" s="T32">0.3.h:S v:pred</ta>
            <ta e="T36" id="Seg_775" s="T35">np:O</ta>
            <ta e="T37" id="Seg_776" s="T36">0.3.h:S v:pred</ta>
            <ta e="T38" id="Seg_777" s="T37">0.1.h:S v:pred</ta>
            <ta e="T42" id="Seg_778" s="T41">0.1.h:S v:pred</ta>
            <ta e="T45" id="Seg_779" s="T44">0.1.h:S v:pred</ta>
            <ta e="T48" id="Seg_780" s="T47">np.h:S</ta>
            <ta e="T49" id="Seg_781" s="T48">v:pred</ta>
            <ta e="T50" id="Seg_782" s="T49">pro.h:S</ta>
            <ta e="T51" id="Seg_783" s="T50">v:pred</ta>
            <ta e="T53" id="Seg_784" s="T52">np.h:S</ta>
            <ta e="T54" id="Seg_785" s="T53">np.h:S</ta>
            <ta e="T55" id="Seg_786" s="T54">v:pred</ta>
            <ta e="T57" id="Seg_787" s="T56">0.3.h:S v:pred</ta>
            <ta e="T59" id="Seg_788" s="T58">v:pred</ta>
            <ta e="T60" id="Seg_789" s="T59">np.h:S</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_790" s="T1">pro.h:P</ta>
            <ta e="T4" id="Seg_791" s="T3">np:L</ta>
            <ta e="T6" id="Seg_792" s="T5">np.h:Poss 0.1.h:Poss</ta>
            <ta e="T7" id="Seg_793" s="T6">np.h:Poss 0.1.h:Poss</ta>
            <ta e="T9" id="Seg_794" s="T8">np.h:Th 0.3.h:Poss</ta>
            <ta e="T14" id="Seg_795" s="T13">pro.h:Th</ta>
            <ta e="T18" id="Seg_796" s="T17">np.h:P 0.1.h:Poss</ta>
            <ta e="T20" id="Seg_797" s="T19">pro.h:Th</ta>
            <ta e="T24" id="Seg_798" s="T23">0.1.h:A</ta>
            <ta e="T25" id="Seg_799" s="T24">np.h:A 0.1.h:Poss</ta>
            <ta e="T28" id="Seg_800" s="T27">np:Th</ta>
            <ta e="T29" id="Seg_801" s="T28">0.3.h:E</ta>
            <ta e="T31" id="Seg_802" s="T30">np.h:Th</ta>
            <ta e="T33" id="Seg_803" s="T32">0.3.h:A</ta>
            <ta e="T37" id="Seg_804" s="T36">0.3.h:A</ta>
            <ta e="T38" id="Seg_805" s="T37">0.1.h:A</ta>
            <ta e="T40" id="Seg_806" s="T39">np:G</ta>
            <ta e="T41" id="Seg_807" s="T40">adv:L</ta>
            <ta e="T42" id="Seg_808" s="T41">0.1.h:Th</ta>
            <ta e="T44" id="Seg_809" s="T43">adv:Time</ta>
            <ta e="T45" id="Seg_810" s="T44">0.1.h:A</ta>
            <ta e="T46" id="Seg_811" s="T45">np:L</ta>
            <ta e="T48" id="Seg_812" s="T47">np.h:Th 0.1.h:Poss</ta>
            <ta e="T52" id="Seg_813" s="T51">pro.h:Poss</ta>
            <ta e="T53" id="Seg_814" s="T52">np.h:Th 0.3.h:Poss</ta>
            <ta e="T54" id="Seg_815" s="T53">np.h:Th 0.3.h:Poss</ta>
            <ta e="T57" id="Seg_816" s="T56">0.3.h:Th</ta>
            <ta e="T58" id="Seg_817" s="T57">np:L</ta>
            <ta e="T60" id="Seg_818" s="T59">np.h:P 0.1.h:Poss</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T23" id="Seg_819" s="T22">RUS:gram</ta>
            <ta e="T35" id="Seg_820" s="T34">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T35" id="Seg_821" s="T34">parad:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_822" s="T1">I was born in Karelino.</ta>
            <ta e="T10" id="Seg_823" s="T5">My parents had five children.</ta>
            <ta e="T13" id="Seg_824" s="T10">We were five.</ta>
            <ta e="T19" id="Seg_825" s="T13">I was little when my mother died.</ta>
            <ta e="T24" id="Seg_826" s="T19">I stayed ((unknown)) and cooked bread.</ta>
            <ta e="T29" id="Seg_827" s="T24">My father did not marry again, he felt sorry for the children.</ta>
            <ta e="T37" id="Seg_828" s="T29">What kind of a woman she would be, she can beat or starve [the children].</ta>
            <ta e="T40" id="Seg_829" s="T37">We moved to Orlyakovo.</ta>
            <ta e="T43" id="Seg_830" s="T40">There I lived as an adult girl.</ta>
            <ta e="T46" id="Seg_831" s="T43">Then I got married in Lukyano.</ta>
            <ta e="T51" id="Seg_832" s="T46">I had two younger siblings ((unknown))</ta>
            <ta e="T55" id="Seg_833" s="T51">They had no father, no mother.</ta>
            <ta e="T57" id="Seg_834" s="T55">They were orphans.</ta>
            <ta e="T60" id="Seg_835" s="T57">My husband was killed in the war.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_836" s="T1">Ich wurde unten in Karelino geboren.</ta>
            <ta e="T10" id="Seg_837" s="T5">Meine Eltern hatten fünf Kinder.</ta>
            <ta e="T13" id="Seg_838" s="T10">Wir waren zu fünft.</ta>
            <ta e="T19" id="Seg_839" s="T13">Ich war klein, als meine Mutter starb.</ta>
            <ta e="T24" id="Seg_840" s="T19">Ich blieb und backte Brot.</ta>
            <ta e="T29" id="Seg_841" s="T24">Mein Vater heiratete nicht wieder, er hatte Mitleid mit den Kindern.</ta>
            <ta e="T37" id="Seg_842" s="T29">Welche Frau wird sie sein, sie wird kämpfen, sie wird [die Kinder] hungern lassen.</ta>
            <ta e="T40" id="Seg_843" s="T37">Wir zogen nach Orlyakovo.</ta>
            <ta e="T43" id="Seg_844" s="T40">Dort lebte ich als junges Mädchen.</ta>
            <ta e="T46" id="Seg_845" s="T43">Dann heiratete ich in Lukjano.</ta>
            <ta e="T51" id="Seg_846" s="T46">Ich hatte zwei jüngeren Geschwister …</ta>
            <ta e="T55" id="Seg_847" s="T51">Sie hatten keinen Vater, keine Mutter.</ta>
            <ta e="T57" id="Seg_848" s="T55">Sie waren Waisen.</ta>
            <ta e="T60" id="Seg_849" s="T57">Im Krieg wurde mein Mann getötet.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_850" s="T1">Я родилась в Карелино, вниз по течению.</ta>
            <ta e="T10" id="Seg_851" s="T5">У моих родителей было пять детей.</ta>
            <ta e="T13" id="Seg_852" s="T10">Нас было пятеро.</ta>
            <ta e="T19" id="Seg_853" s="T13">Я была маленькая, когда моя мать умерла.</ta>
            <ta e="T24" id="Seg_854" s="T19">Я осталась (?), стряпала хлеб.</ta>
            <ta e="T29" id="Seg_855" s="T24">Мой отец не женился снова, он детей жалел.</ta>
            <ta e="T37" id="Seg_856" s="T29">Какая жена будет, она драться будет (драчливая будет), голодом морить будет.</ta>
            <ta e="T40" id="Seg_857" s="T37">Мы переехали в Орляково.</ta>
            <ta e="T43" id="Seg_858" s="T40">Там я жила девушкой (взрослой).</ta>
            <ta e="T46" id="Seg_859" s="T43">Потом замуж вышла в Лукьяно.</ta>
            <ta e="T51" id="Seg_860" s="T46">У меня (?) было два младших брата.</ta>
            <ta e="T55" id="Seg_861" s="T51">У них отца и матери не было.</ta>
            <ta e="T57" id="Seg_862" s="T55">Они были сиротами.</ta>
            <ta e="T60" id="Seg_863" s="T57">На войне убили моего мужа.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr" />
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
