<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PFN_1964_MakingAHorseStable_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PFN_1964_MakingAHorseStable_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">18</ud-information>
            <ud-information attribute-name="# HIAT:w">14</ud-information>
            <ud-information attribute-name="# e">14</ud-information>
            <ud-information attribute-name="# HIAT:u">3</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PFN">
            <abbreviation>PFN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PFN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T15" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">tamdʼe</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">laɣakusaŋ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">aranɣɨn</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">me</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">meːkuzot</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">kjundɨ</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">aranɨm</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_32" n="HIAT:u" s="T8">
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T8">kutɨnassɨ</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">assɨ</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">laɣakus</ts>
                  <nts id="Seg_41" n="HIAT:ip">,</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">tolʼko</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">man</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">onnɛŋ</ts>
                  <nts id="Seg_51" n="HIAT:ip">.</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T15" id="Seg_53" n="sc" s="T0">
               <ts e="T1" id="Seg_55" n="e" s="T0">man </ts>
               <ts e="T2" id="Seg_57" n="e" s="T1">tamdʼe </ts>
               <ts e="T3" id="Seg_59" n="e" s="T2">laɣakusaŋ </ts>
               <ts e="T4" id="Seg_61" n="e" s="T3">aranɣɨn. </ts>
               <ts e="T5" id="Seg_63" n="e" s="T4">me </ts>
               <ts e="T6" id="Seg_65" n="e" s="T5">meːkuzot </ts>
               <ts e="T7" id="Seg_67" n="e" s="T6">kjundɨ </ts>
               <ts e="T8" id="Seg_69" n="e" s="T7">aranɨm. </ts>
               <ts e="T10" id="Seg_71" n="e" s="T8">kutɨnassɨ </ts>
               <ts e="T11" id="Seg_73" n="e" s="T10">assɨ </ts>
               <ts e="T12" id="Seg_75" n="e" s="T11">laɣakus, </ts>
               <ts e="T13" id="Seg_77" n="e" s="T12">tolʼko </ts>
               <ts e="T14" id="Seg_79" n="e" s="T13">man </ts>
               <ts e="T15" id="Seg_81" n="e" s="T14">onnɛŋ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_82" s="T0">PFN_1964_MakingAHorseStable_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_83" s="T4">PFN_1964_MakingAHorseStable_nar.002 (001.002)</ta>
            <ta e="T15" id="Seg_84" s="T8">PFN_1964_MakingAHorseStable_nar.003 (001.003)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_85" s="T0">ман там′дʼе лаɣаку′саң а′ранɣын.</ta>
            <ta e="T8" id="Seg_86" s="T4">ме ′ме̄кузот кюн′ды ара′ным.</ta>
            <ta e="T15" id="Seg_87" s="T8">ку′ты ′нассы ассы ′лаɣакус, только ман оннэң.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_88" s="T0">man tamdʼe laɣakusaŋ aranɣɨn.</ta>
            <ta e="T8" id="Seg_89" s="T4">me meːkuzot kjundɨ aranɨm.</ta>
            <ta e="T15" id="Seg_90" s="T8">kutɨ nassɨ assɨ laɣakus, tolʼko man onnɛŋ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_91" s="T0">man tamdʼe laɣakusaŋ aranɣɨn. </ta>
            <ta e="T8" id="Seg_92" s="T4">me meːkuzot kjundɨ aranɨm. </ta>
            <ta e="T15" id="Seg_93" s="T8">kutɨ nassɨ assɨ laɣakus, tolʼko man onnɛŋ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_94" s="T0">man</ta>
            <ta e="T2" id="Seg_95" s="T1">tam-dʼe</ta>
            <ta e="T3" id="Seg_96" s="T2">laɣa-ku-sa-ŋ</ta>
            <ta e="T4" id="Seg_97" s="T3">aran-ɣɨn</ta>
            <ta e="T5" id="Seg_98" s="T4">me</ta>
            <ta e="T6" id="Seg_99" s="T5">meː-ku-zo-t</ta>
            <ta e="T7" id="Seg_100" s="T6">kjundɨ</ta>
            <ta e="T8" id="Seg_101" s="T7">aran-ɨ-m</ta>
            <ta e="T10" id="Seg_102" s="T8">kutɨ-n-assɨ</ta>
            <ta e="T11" id="Seg_103" s="T10">assɨ</ta>
            <ta e="T12" id="Seg_104" s="T11">laɣa-ku-s</ta>
            <ta e="T13" id="Seg_105" s="T12">tolʼko</ta>
            <ta e="T14" id="Seg_106" s="T13">man</ta>
            <ta e="T15" id="Seg_107" s="T14">onnɛŋ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_108" s="T0">man</ta>
            <ta e="T2" id="Seg_109" s="T1">taw-tʼelɨ</ta>
            <ta e="T3" id="Seg_110" s="T2">laqqɨ-kku-sɨ-ŋ</ta>
            <ta e="T4" id="Seg_111" s="T3">aran-qən</ta>
            <ta e="T5" id="Seg_112" s="T4">meː</ta>
            <ta e="T6" id="Seg_113" s="T5">meː-kku-sɨ-tɨ</ta>
            <ta e="T7" id="Seg_114" s="T6">kündɨ</ta>
            <ta e="T8" id="Seg_115" s="T7">aran-ɨ-m</ta>
            <ta e="T10" id="Seg_116" s="T8">kutti-naj-assɨ</ta>
            <ta e="T11" id="Seg_117" s="T10">assɨ</ta>
            <ta e="T12" id="Seg_118" s="T11">laqqɨ-kku-sɨ</ta>
            <ta e="T13" id="Seg_119" s="T12">tolʼko</ta>
            <ta e="T14" id="Seg_120" s="T13">man</ta>
            <ta e="T15" id="Seg_121" s="T14">onäk</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_122" s="T0">I.NOM</ta>
            <ta e="T2" id="Seg_123" s="T1">this-day.[NOM]</ta>
            <ta e="T3" id="Seg_124" s="T2">work-HAB-PST-1SG.S</ta>
            <ta e="T4" id="Seg_125" s="T3">yard-LOC</ta>
            <ta e="T5" id="Seg_126" s="T4">we.NOM</ta>
            <ta e="T6" id="Seg_127" s="T5">do-HAB-PST-3SG.O</ta>
            <ta e="T7" id="Seg_128" s="T6">horse.[NOM]</ta>
            <ta e="T8" id="Seg_129" s="T7">yard-EP-ACC</ta>
            <ta e="T10" id="Seg_130" s="T8">who.[NOM]-EMPH-NEG</ta>
            <ta e="T11" id="Seg_131" s="T10">NEG</ta>
            <ta e="T12" id="Seg_132" s="T11">work-HAB-PST.[3SG.S]</ta>
            <ta e="T13" id="Seg_133" s="T12">only</ta>
            <ta e="T14" id="Seg_134" s="T13">I.NOM</ta>
            <ta e="T15" id="Seg_135" s="T14">oneself.1SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_136" s="T0">я.NOM</ta>
            <ta e="T2" id="Seg_137" s="T1">этот-день.[NOM]</ta>
            <ta e="T3" id="Seg_138" s="T2">работать-HAB-PST-1SG.S</ta>
            <ta e="T4" id="Seg_139" s="T3">двор-LOC</ta>
            <ta e="T5" id="Seg_140" s="T4">мы.NOM</ta>
            <ta e="T6" id="Seg_141" s="T5">делать-HAB-PST-3SG.O</ta>
            <ta e="T7" id="Seg_142" s="T6">лошадь.[NOM]</ta>
            <ta e="T8" id="Seg_143" s="T7">двор-EP-ACC</ta>
            <ta e="T10" id="Seg_144" s="T8">кто.[NOM]-EMPH-NEG</ta>
            <ta e="T11" id="Seg_145" s="T10">NEG</ta>
            <ta e="T12" id="Seg_146" s="T11">работать-HAB-PST.[3SG.S]</ta>
            <ta e="T13" id="Seg_147" s="T12">только</ta>
            <ta e="T14" id="Seg_148" s="T13">я.NOM</ta>
            <ta e="T15" id="Seg_149" s="T14">сам.1SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_150" s="T0">pers</ta>
            <ta e="T2" id="Seg_151" s="T1">dem-n-n:case</ta>
            <ta e="T3" id="Seg_152" s="T2">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_153" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_154" s="T4">pers</ta>
            <ta e="T6" id="Seg_155" s="T5">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_156" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_157" s="T7">n-n:ins-n:case</ta>
            <ta e="T10" id="Seg_158" s="T8">interrog-n:case-clit-ptcl</ta>
            <ta e="T11" id="Seg_159" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_160" s="T11">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_161" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_162" s="T13">pers</ta>
            <ta e="T15" id="Seg_163" s="T14">emphpro</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_164" s="T0">pers</ta>
            <ta e="T2" id="Seg_165" s="T1">n</ta>
            <ta e="T3" id="Seg_166" s="T2">v</ta>
            <ta e="T4" id="Seg_167" s="T3">n</ta>
            <ta e="T5" id="Seg_168" s="T4">pers</ta>
            <ta e="T6" id="Seg_169" s="T5">v</ta>
            <ta e="T7" id="Seg_170" s="T6">n</ta>
            <ta e="T8" id="Seg_171" s="T7">n</ta>
            <ta e="T10" id="Seg_172" s="T8">interrog</ta>
            <ta e="T11" id="Seg_173" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_174" s="T11">v</ta>
            <ta e="T13" id="Seg_175" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_176" s="T13">pers</ta>
            <ta e="T15" id="Seg_177" s="T14">emphpro</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_178" s="T0">pro.h:S</ta>
            <ta e="T3" id="Seg_179" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_180" s="T4">pro.h:S</ta>
            <ta e="T6" id="Seg_181" s="T5">v:pred</ta>
            <ta e="T8" id="Seg_182" s="T7">np:O</ta>
            <ta e="T10" id="Seg_183" s="T8">pro.h:S</ta>
            <ta e="T12" id="Seg_184" s="T11">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_185" s="T0">pro.h:A</ta>
            <ta e="T2" id="Seg_186" s="T1">np:Time</ta>
            <ta e="T4" id="Seg_187" s="T3">np:L</ta>
            <ta e="T5" id="Seg_188" s="T4">pro.h:A</ta>
            <ta e="T7" id="Seg_189" s="T6">np:Poss</ta>
            <ta e="T8" id="Seg_190" s="T7">np:P</ta>
            <ta e="T10" id="Seg_191" s="T8">pro.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T13" id="Seg_192" s="T12">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_193" s="T0">Я сегодня работал во дворе.</ta>
            <ta e="T8" id="Seg_194" s="T4">Мы делали конюшню.</ta>
            <ta e="T15" id="Seg_195" s="T8">Никто не работал, только я один.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_196" s="T0">Today I worked in the yard.</ta>
            <ta e="T8" id="Seg_197" s="T4">We made a stable.</ta>
            <ta e="T15" id="Seg_198" s="T8">No one worked, only I alone.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_199" s="T0">Heute habe ich auf dem Hof gearbeitet.</ta>
            <ta e="T8" id="Seg_200" s="T4">Wir haben einen Stall gemacht.</ta>
            <ta e="T15" id="Seg_201" s="T8">Niemand hat gearbeitet, nur ich allein.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_202" s="T0">я сегодня работал во дворе</ta>
            <ta e="T8" id="Seg_203" s="T4">мы делали конюшню</ta>
            <ta e="T15" id="Seg_204" s="T8">никто не работал только я один</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
