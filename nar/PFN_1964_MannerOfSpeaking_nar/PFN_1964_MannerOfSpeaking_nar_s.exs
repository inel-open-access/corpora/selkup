<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PFN_1964_MannerOfSpeaking_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PFN_1964_MannerOfSpeaking_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">20</ud-information>
            <ud-information attribute-name="# HIAT:w">16</ud-information>
            <ud-information attribute-name="# e">16</ud-information>
            <ud-information attribute-name="# HIAT:u">3</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PFN">
            <abbreviation>PFN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PFN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T16" id="Seg_0" n="sc" s="T0">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">eːɣat</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">süsü</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">quːla</ts>
                  <nts id="Seg_11" n="HIAT:ip">,</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">täpla</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">čenǯučat</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">aːran</ts>
                  <nts id="Seg_21" n="HIAT:ip">.</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_24" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">tammi</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">quːla</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">čenǯučat</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">protʼaʒno</ts>
                  <nts id="Seg_36" n="HIAT:ip">.</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_39" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">süsü</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">quːla</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">taŋi</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">quːla</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">čenǯičat</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">qala</ts>
                  <nts id="Seg_57" n="HIAT:ip">.</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T16" id="Seg_59" n="sc" s="T0">
               <ts e="T1" id="Seg_61" n="e" s="T0">eːɣat </ts>
               <ts e="T2" id="Seg_63" n="e" s="T1">süsü </ts>
               <ts e="T3" id="Seg_65" n="e" s="T2">quːla, </ts>
               <ts e="T4" id="Seg_67" n="e" s="T3">täpla </ts>
               <ts e="T5" id="Seg_69" n="e" s="T4">čenǯučat </ts>
               <ts e="T6" id="Seg_71" n="e" s="T5">aːran. </ts>
               <ts e="T7" id="Seg_73" n="e" s="T6">tammi </ts>
               <ts e="T8" id="Seg_75" n="e" s="T7">quːla </ts>
               <ts e="T9" id="Seg_77" n="e" s="T8">čenǯučat </ts>
               <ts e="T10" id="Seg_79" n="e" s="T9">protʼaʒno. </ts>
               <ts e="T11" id="Seg_81" n="e" s="T10">süsü </ts>
               <ts e="T12" id="Seg_83" n="e" s="T11">quːla </ts>
               <ts e="T13" id="Seg_85" n="e" s="T12">taŋi </ts>
               <ts e="T14" id="Seg_87" n="e" s="T13">quːla </ts>
               <ts e="T15" id="Seg_89" n="e" s="T14">čenǯičat </ts>
               <ts e="T16" id="Seg_91" n="e" s="T15">qala. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_92" s="T0">PFN_1964_MannerOfSpeaking_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_93" s="T6">PFN_1964_MannerOfSpeaking_nar.002 (001.002)</ta>
            <ta e="T16" id="Seg_94" s="T10">PFN_1964_MannerOfSpeaking_nar.003 (001.003)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_95" s="T0">′е̄ɣат ′сӱсӱ ′kӯла, тӓп′ла ′тшенджучат ′а̄ран.</ta>
            <ta e="T10" id="Seg_96" s="T6">′тамми ′kӯл(л)а ′тш(ч)енджучат протяжно.</ta>
            <ta e="T16" id="Seg_97" s="T10">′сӱсӱ ′kӯла ′таңи ′kӯла ′тшенджичат ′kала.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_98" s="T0">eːɣat süsü quːla, täpla tšenǯučat aːran.</ta>
            <ta e="T10" id="Seg_99" s="T6">tammi quːl(l)a tš(č)enǯučat protʼaʒno.</ta>
            <ta e="T16" id="Seg_100" s="T10">süsü quːla taŋi quːla tšenǯičat qala.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_101" s="T0">eːɣat süsü quːla, täpla čenǯučat aːran. </ta>
            <ta e="T10" id="Seg_102" s="T6">tammi quːla čenǯučat protʼaʒno. </ta>
            <ta e="T16" id="Seg_103" s="T10">süsü quːla taŋi quːla čenǯičat qala. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_104" s="T0">eː-ɣa-t</ta>
            <ta e="T2" id="Seg_105" s="T1">süsü</ta>
            <ta e="T3" id="Seg_106" s="T2">quː-la</ta>
            <ta e="T4" id="Seg_107" s="T3">täp-la</ta>
            <ta e="T5" id="Seg_108" s="T4">čenǯu-ča-t</ta>
            <ta e="T6" id="Seg_109" s="T5">aːran</ta>
            <ta e="T7" id="Seg_110" s="T6">tammi</ta>
            <ta e="T8" id="Seg_111" s="T7">quː-la</ta>
            <ta e="T9" id="Seg_112" s="T8">čenǯu-ča-t</ta>
            <ta e="T10" id="Seg_113" s="T9">protʼaʒno</ta>
            <ta e="T11" id="Seg_114" s="T10">süsü</ta>
            <ta e="T12" id="Seg_115" s="T11">quː-la</ta>
            <ta e="T13" id="Seg_116" s="T12">taŋi</ta>
            <ta e="T14" id="Seg_117" s="T13">quː-la</ta>
            <ta e="T15" id="Seg_118" s="T14">čenǯi-ča-t</ta>
            <ta e="T16" id="Seg_119" s="T15">qala</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_120" s="T0">eː-ŋɨ-tɨt</ta>
            <ta e="T2" id="Seg_121" s="T1">süsü</ta>
            <ta e="T3" id="Seg_122" s="T2">qum-la</ta>
            <ta e="T4" id="Seg_123" s="T3">tap-la</ta>
            <ta e="T5" id="Seg_124" s="T4">čenču-nče-tɨt</ta>
            <ta e="T6" id="Seg_125" s="T5">aːrɨŋ</ta>
            <ta e="T7" id="Seg_126" s="T6">taːmi</ta>
            <ta e="T8" id="Seg_127" s="T7">qum-la</ta>
            <ta e="T9" id="Seg_128" s="T8">čenču-nče-tɨt</ta>
            <ta e="T10" id="Seg_129" s="T9">protʼaʒno</ta>
            <ta e="T11" id="Seg_130" s="T10">süsü</ta>
            <ta e="T12" id="Seg_131" s="T11">qum-la</ta>
            <ta e="T13" id="Seg_132" s="T12">taŋi</ta>
            <ta e="T14" id="Seg_133" s="T13">qum-la</ta>
            <ta e="T15" id="Seg_134" s="T14">čenču-nče-tɨt</ta>
            <ta e="T16" id="Seg_135" s="T15">qalla</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_136" s="T0">be-CO-3PL</ta>
            <ta e="T2" id="Seg_137" s="T1">taiga.[NOM]</ta>
            <ta e="T3" id="Seg_138" s="T2">human.being-PL.[NOM]</ta>
            <ta e="T4" id="Seg_139" s="T3">(s)he-PL.[NOM]</ta>
            <ta e="T5" id="Seg_140" s="T4">speak-IPFV3-3PL</ta>
            <ta e="T6" id="Seg_141" s="T5">other</ta>
            <ta e="T7" id="Seg_142" s="T6">upper</ta>
            <ta e="T8" id="Seg_143" s="T7">human.being-PL.[NOM]</ta>
            <ta e="T9" id="Seg_144" s="T8">speak-IPFV3-3PL</ta>
            <ta e="T10" id="Seg_145" s="T9">slow</ta>
            <ta e="T11" id="Seg_146" s="T10">taiga.[NOM]</ta>
            <ta e="T12" id="Seg_147" s="T11">human.being-PL.[NOM]</ta>
            <ta e="T13" id="Seg_148" s="T12">lower</ta>
            <ta e="T14" id="Seg_149" s="T13">human.being-PL.[NOM]</ta>
            <ta e="T15" id="Seg_150" s="T14">speak-IPFV3-3PL</ta>
            <ta e="T16" id="Seg_151" s="T15">how</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_152" s="T0">быть-CO-3PL</ta>
            <ta e="T2" id="Seg_153" s="T1">тайга.[NOM]</ta>
            <ta e="T3" id="Seg_154" s="T2">человек-PL.[NOM]</ta>
            <ta e="T4" id="Seg_155" s="T3">он(а)-PL.[NOM]</ta>
            <ta e="T5" id="Seg_156" s="T4">говорить-IPFV3-3PL</ta>
            <ta e="T6" id="Seg_157" s="T5">другой</ta>
            <ta e="T7" id="Seg_158" s="T6">верхний</ta>
            <ta e="T8" id="Seg_159" s="T7">человек-PL.[NOM]</ta>
            <ta e="T9" id="Seg_160" s="T8">говорить-IPFV3-3PL</ta>
            <ta e="T10" id="Seg_161" s="T9">протяжно</ta>
            <ta e="T11" id="Seg_162" s="T10">тайга.[NOM]</ta>
            <ta e="T12" id="Seg_163" s="T11">человек-PL.[NOM]</ta>
            <ta e="T13" id="Seg_164" s="T12">нижний</ta>
            <ta e="T14" id="Seg_165" s="T13">человек-PL.[NOM]</ta>
            <ta e="T15" id="Seg_166" s="T14">говорить-IPFV3-3PL</ta>
            <ta e="T16" id="Seg_167" s="T15">как</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_168" s="T0">v-v:ins-v:pn</ta>
            <ta e="T2" id="Seg_169" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_170" s="T2">n-n:num-n:case</ta>
            <ta e="T4" id="Seg_171" s="T3">pers-n:num-n:case</ta>
            <ta e="T5" id="Seg_172" s="T4">v-v&gt;v-v:pn</ta>
            <ta e="T6" id="Seg_173" s="T5">adj</ta>
            <ta e="T7" id="Seg_174" s="T6">adj</ta>
            <ta e="T8" id="Seg_175" s="T7">n-n:num-n:case</ta>
            <ta e="T9" id="Seg_176" s="T8">v-v&gt;v-v:pn</ta>
            <ta e="T10" id="Seg_177" s="T9">adv</ta>
            <ta e="T11" id="Seg_178" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_179" s="T11">n-n:num-n:case</ta>
            <ta e="T13" id="Seg_180" s="T12">adj</ta>
            <ta e="T14" id="Seg_181" s="T13">n-n:num-n:case</ta>
            <ta e="T15" id="Seg_182" s="T14">v-v&gt;v-v:pn</ta>
            <ta e="T16" id="Seg_183" s="T15">conj</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_184" s="T0">v</ta>
            <ta e="T2" id="Seg_185" s="T1">v</ta>
            <ta e="T3" id="Seg_186" s="T2">n</ta>
            <ta e="T4" id="Seg_187" s="T3">pers</ta>
            <ta e="T5" id="Seg_188" s="T4">v</ta>
            <ta e="T6" id="Seg_189" s="T5">adj</ta>
            <ta e="T7" id="Seg_190" s="T6">adj</ta>
            <ta e="T8" id="Seg_191" s="T7">n</ta>
            <ta e="T9" id="Seg_192" s="T8">v</ta>
            <ta e="T10" id="Seg_193" s="T9">adv</ta>
            <ta e="T11" id="Seg_194" s="T10">v</ta>
            <ta e="T12" id="Seg_195" s="T11">n</ta>
            <ta e="T13" id="Seg_196" s="T12">adj</ta>
            <ta e="T14" id="Seg_197" s="T13">n</ta>
            <ta e="T15" id="Seg_198" s="T14">v</ta>
            <ta e="T16" id="Seg_199" s="T15">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_200" s="T0">v:pred</ta>
            <ta e="T3" id="Seg_201" s="T2">np.h:S</ta>
            <ta e="T4" id="Seg_202" s="T3">np.h:S</ta>
            <ta e="T5" id="Seg_203" s="T4">v:pred</ta>
            <ta e="T8" id="Seg_204" s="T7">np.h:S</ta>
            <ta e="T9" id="Seg_205" s="T8">v:pred</ta>
            <ta e="T12" id="Seg_206" s="T11">np.h:S</ta>
            <ta e="T14" id="Seg_207" s="T13">np.h:S</ta>
            <ta e="T15" id="Seg_208" s="T14">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_209" s="T2">np.h:Th</ta>
            <ta e="T4" id="Seg_210" s="T3">np.h:А</ta>
            <ta e="T8" id="Seg_211" s="T7">np.h:A</ta>
            <ta e="T12" id="Seg_212" s="T11">np.h:A</ta>
            <ta e="T14" id="Seg_213" s="T13">np.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T10" id="Seg_214" s="T9">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_215" s="T0">Есть селькупы, которые говорят по-другому.</ta>
            <ta e="T10" id="Seg_216" s="T6">Верховские селькупы говорят протяжно.</ta>
            <ta e="T16" id="Seg_217" s="T10">Средние селькупы и низовские селькупы говорят как (?) ("кала"?).</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_218" s="T0">There are Selkup people who speak in another way.</ta>
            <ta e="T10" id="Seg_219" s="T6">Upper Selkups speak in a drawlin tone.</ta>
            <ta e="T16" id="Seg_220" s="T10">Middle and lower Selkups speak (?).</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_221" s="T0">Es gibt Selkupen, die anders sprechen.</ta>
            <ta e="T10" id="Seg_222" s="T6">Obere Selkupen sprechen gedehnt.</ta>
            <ta e="T16" id="Seg_223" s="T10">Mittlere und untere Selkupen sprechen (?)</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_224" s="T0">есть остяки они говорят по-другому</ta>
            <ta e="T10" id="Seg_225" s="T6">верховские остяки говорят</ta>
            <ta e="T16" id="Seg_226" s="T10">средние остяки низовские остяки говорят как</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
