<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PMP_1961_MyDay_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PMP_1961_MyDay_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">76</ud-information>
            <ud-information attribute-name="# HIAT:w">57</ud-information>
            <ud-information attribute-name="# e">57</ud-information>
            <ud-information attribute-name="# HIAT:u">12</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PMP">
            <abbreviation>PMP</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PMP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T58" id="Seg_0" n="sc" s="T1">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Nagɨrt</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">qazasen</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_11" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">Tan</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">nagɨnnanǯ</ts>
                  <nts id="Seg_17" n="HIAT:ip">,</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">a</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">man</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">tan</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">köɣɨnt</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">aːmdaŋ</ts>
                  <nts id="Seg_33" n="HIAT:ip">.</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_36" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">Tajze</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">tʼälɨmpaŋ</ts>
                  <nts id="Seg_42" n="HIAT:ip">,</nts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">a</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">oneŋ</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_51" n="HIAT:w" s="T14">qwälʼe</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_54" n="HIAT:w" s="T15">meːʒəraŋ</ts>
                  <nts id="Seg_55" n="HIAT:ip">.</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_58" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">Mannan</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_63" n="HIAT:w" s="T17">qwäl</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_66" n="HIAT:w" s="T18">pedʼella</ts>
                  <nts id="Seg_67" n="HIAT:ip">,</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">qaːza</ts>
                  <nts id="Seg_71" n="HIAT:ip">,</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">oqqɨr</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_77" n="HIAT:w" s="T21">nɨrsa</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_81" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">Qwälam</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">meŋga</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">qwassɨt</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">tamdʼel</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_95" n="HIAT:w" s="T26">emneu</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_99" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">Tau</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">qarimɨɣɨn</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">man</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">qwälɨm</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">meːlʼe</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_116" n="HIAT:w" s="T32">taːderaw</ts>
                  <nts id="Seg_117" n="HIAT:ip">,</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_120" n="HIAT:w" s="T33">tɨssoɣɨn</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_123" n="HIAT:w" s="T34">perčenǯau</ts>
                  <nts id="Seg_124" n="HIAT:ip">.</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_127" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_129" n="HIAT:w" s="T35">Tabɨla</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_132" n="HIAT:w" s="T36">üːdəmɨɣɨn</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_135" n="HIAT:w" s="T37">tʼüːnǯatt</ts>
                  <nts id="Seg_136" n="HIAT:ip">.</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_139" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_141" n="HIAT:w" s="T38">Apsot</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_144" n="HIAT:w" s="T39">qodowaŋ</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_147" n="HIAT:w" s="T40">jenǯiŋ</ts>
                  <nts id="Seg_148" n="HIAT:ip">.</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_151" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_153" n="HIAT:w" s="T41">Omdɨq</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_156" n="HIAT:w" s="T42">awɨrq</ts>
                  <nts id="Seg_157" n="HIAT:ip">.</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_160" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_162" n="HIAT:w" s="T43">Nilʼdʼiŋ</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_165" n="HIAT:w" s="T44">menan</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_168" n="HIAT:w" s="T45">tʼelɨt</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_171" n="HIAT:w" s="T46">mendɨquŋ</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_173" n="HIAT:ip">(</nts>
                  <ts e="T48" id="Seg_175" n="HIAT:w" s="T47">tʼelɨmquŋ</ts>
                  <nts id="Seg_176" n="HIAT:ip">)</nts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_179" n="HIAT:w" s="T48">i</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_182" n="HIAT:w" s="T49">nilʼdʼiŋ</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_185" n="HIAT:w" s="T50">üːdɨmguŋ</ts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_189" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_191" n="HIAT:w" s="T51">Nilʼdʼiŋ</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_194" n="HIAT:w" s="T52">me</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_197" n="HIAT:w" s="T53">iːluttə</ts>
                  <nts id="Seg_198" n="HIAT:ip">.</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_201" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_203" n="HIAT:w" s="T54">Nilʼdʼi</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_206" n="HIAT:w" s="T55">menan</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_209" n="HIAT:w" s="T56">i</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_212" n="HIAT:w" s="T57">iləsan</ts>
                  <nts id="Seg_213" n="HIAT:ip">.</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T58" id="Seg_215" n="sc" s="T1">
               <ts e="T2" id="Seg_217" n="e" s="T1">Nagɨrt </ts>
               <ts e="T3" id="Seg_219" n="e" s="T2">qazasen. </ts>
               <ts e="T4" id="Seg_221" n="e" s="T3">Tan </ts>
               <ts e="T5" id="Seg_223" n="e" s="T4">nagɨnnanǯ, </ts>
               <ts e="T6" id="Seg_225" n="e" s="T5">a </ts>
               <ts e="T7" id="Seg_227" n="e" s="T6">man </ts>
               <ts e="T8" id="Seg_229" n="e" s="T7">tan </ts>
               <ts e="T9" id="Seg_231" n="e" s="T8">köɣɨnt </ts>
               <ts e="T10" id="Seg_233" n="e" s="T9">aːmdaŋ. </ts>
               <ts e="T11" id="Seg_235" n="e" s="T10">Tajze </ts>
               <ts e="T12" id="Seg_237" n="e" s="T11">tʼälɨmpaŋ, </ts>
               <ts e="T13" id="Seg_239" n="e" s="T12">a </ts>
               <ts e="T14" id="Seg_241" n="e" s="T13">oneŋ </ts>
               <ts e="T15" id="Seg_243" n="e" s="T14">qwälʼe </ts>
               <ts e="T16" id="Seg_245" n="e" s="T15">meːʒəraŋ. </ts>
               <ts e="T17" id="Seg_247" n="e" s="T16">Mannan </ts>
               <ts e="T18" id="Seg_249" n="e" s="T17">qwäl </ts>
               <ts e="T19" id="Seg_251" n="e" s="T18">pedʼella, </ts>
               <ts e="T20" id="Seg_253" n="e" s="T19">qaːza, </ts>
               <ts e="T21" id="Seg_255" n="e" s="T20">oqqɨr </ts>
               <ts e="T22" id="Seg_257" n="e" s="T21">nɨrsa. </ts>
               <ts e="T23" id="Seg_259" n="e" s="T22">Qwälam </ts>
               <ts e="T24" id="Seg_261" n="e" s="T23">meŋga </ts>
               <ts e="T25" id="Seg_263" n="e" s="T24">qwassɨt </ts>
               <ts e="T26" id="Seg_265" n="e" s="T25">tamdʼel </ts>
               <ts e="T27" id="Seg_267" n="e" s="T26">emneu. </ts>
               <ts e="T28" id="Seg_269" n="e" s="T27">Tau </ts>
               <ts e="T29" id="Seg_271" n="e" s="T28">qarimɨɣɨn </ts>
               <ts e="T30" id="Seg_273" n="e" s="T29">man </ts>
               <ts e="T31" id="Seg_275" n="e" s="T30">qwälɨm </ts>
               <ts e="T32" id="Seg_277" n="e" s="T31">meːlʼe </ts>
               <ts e="T33" id="Seg_279" n="e" s="T32">taːderaw, </ts>
               <ts e="T34" id="Seg_281" n="e" s="T33">tɨssoɣɨn </ts>
               <ts e="T35" id="Seg_283" n="e" s="T34">perčenǯau. </ts>
               <ts e="T36" id="Seg_285" n="e" s="T35">Tabɨla </ts>
               <ts e="T37" id="Seg_287" n="e" s="T36">üːdəmɨɣɨn </ts>
               <ts e="T38" id="Seg_289" n="e" s="T37">tʼüːnǯatt. </ts>
               <ts e="T39" id="Seg_291" n="e" s="T38">Apsot </ts>
               <ts e="T40" id="Seg_293" n="e" s="T39">qodowaŋ </ts>
               <ts e="T41" id="Seg_295" n="e" s="T40">jenǯiŋ. </ts>
               <ts e="T42" id="Seg_297" n="e" s="T41">Omdɨq </ts>
               <ts e="T43" id="Seg_299" n="e" s="T42">awɨrq. </ts>
               <ts e="T44" id="Seg_301" n="e" s="T43">Nilʼdʼiŋ </ts>
               <ts e="T45" id="Seg_303" n="e" s="T44">menan </ts>
               <ts e="T46" id="Seg_305" n="e" s="T45">tʼelɨt </ts>
               <ts e="T47" id="Seg_307" n="e" s="T46">mendɨquŋ </ts>
               <ts e="T48" id="Seg_309" n="e" s="T47">(tʼelɨmquŋ) </ts>
               <ts e="T49" id="Seg_311" n="e" s="T48">i </ts>
               <ts e="T50" id="Seg_313" n="e" s="T49">nilʼdʼiŋ </ts>
               <ts e="T51" id="Seg_315" n="e" s="T50">üːdɨmguŋ. </ts>
               <ts e="T52" id="Seg_317" n="e" s="T51">Nilʼdʼiŋ </ts>
               <ts e="T53" id="Seg_319" n="e" s="T52">me </ts>
               <ts e="T54" id="Seg_321" n="e" s="T53">iːluttə. </ts>
               <ts e="T55" id="Seg_323" n="e" s="T54">Nilʼdʼi </ts>
               <ts e="T56" id="Seg_325" n="e" s="T55">menan </ts>
               <ts e="T57" id="Seg_327" n="e" s="T56">i </ts>
               <ts e="T58" id="Seg_329" n="e" s="T57">iləsan. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_330" s="T1">PMP_1961_MyDay_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_331" s="T3">PMP_1961_MyDay_nar.002 (001.002)</ta>
            <ta e="T16" id="Seg_332" s="T10">PMP_1961_MyDay_nar.003 (001.003)</ta>
            <ta e="T22" id="Seg_333" s="T16">PMP_1961_MyDay_nar.004 (001.004)</ta>
            <ta e="T27" id="Seg_334" s="T22">PMP_1961_MyDay_nar.005 (001.005)</ta>
            <ta e="T35" id="Seg_335" s="T27">PMP_1961_MyDay_nar.006 (001.006)</ta>
            <ta e="T38" id="Seg_336" s="T35">PMP_1961_MyDay_nar.007 (001.007)</ta>
            <ta e="T41" id="Seg_337" s="T38">PMP_1961_MyDay_nar.008 (001.008)</ta>
            <ta e="T43" id="Seg_338" s="T41">PMP_1961_MyDay_nar.009 (001.009)</ta>
            <ta e="T51" id="Seg_339" s="T43">PMP_1961_MyDay_nar.010 (001.010)</ta>
            <ta e="T54" id="Seg_340" s="T51">PMP_1961_MyDay_nar.011 (001.011)</ta>
            <ta e="T58" id="Seg_341" s="T54">PMP_1961_MyDay_nar.012 (001.012)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_342" s="T1">нагырт kаза′сен.</ta>
            <ta e="T10" id="Seg_343" s="T3">тан нагын′нандж, а ман тан ′кӧɣынт ′а̄мдаң.</ta>
            <ta e="T16" id="Seg_344" s="T10">тай′зе тʼӓлым′паң, а о′нең ′kwӓлʼе ′ме̄жъ′раң.</ta>
            <ta e="T22" id="Seg_345" s="T16">ма′ннан kwӓл пе′дʼелла, ка̄за, оккыр ныр′са.</ta>
            <ta e="T27" id="Seg_346" s="T22">′kwӓлам ′меңга kwа′ссыт там′дʼел ем′неу.</ta>
            <ta e="T35" id="Seg_347" s="T27">′тау kа′римыɣын ман ′kwӓлым ′ме̄лʼе ′та̄дераw, ты′ссоɣын пер′тшенджау.</ta>
            <ta e="T38" id="Seg_348" s="T35">табы′ла ′ӱ̄дъмыɣын ′тʼӱ̄нджатт.</ta>
            <ta e="T41" id="Seg_349" s="T38">ап′сот ко′доваң ′jенджиң.</ta>
            <ta e="T43" id="Seg_350" s="T41">ом′дыk а′вырк.</ta>
            <ta e="T51" id="Seg_351" s="T43">нилʼ′дʼиң ме′нан ′тʼелыт менды′куң (тʼелымк(г)уң) и нилʼдʼиң ′ӱ̄дымгуң.</ta>
            <ta e="T54" id="Seg_352" s="T51">нилʼ′дʼиң ме ӣ′луттъ.</ta>
            <ta e="T58" id="Seg_353" s="T54">нилʼ′дʼи ме′нани илъ′сан.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_354" s="T1">nagɨrt qazasen.</ta>
            <ta e="T10" id="Seg_355" s="T3">tan nagɨnnanǯ, a man tan köɣɨnt aːmdaŋ.</ta>
            <ta e="T16" id="Seg_356" s="T10">tajze tʼälɨmpaŋ, a oneŋ qwälʼe meːʒəraŋ.</ta>
            <ta e="T22" id="Seg_357" s="T16">mannan qwäl pedʼella, qaːza, oqqɨr nɨrsa.</ta>
            <ta e="T27" id="Seg_358" s="T22">qwälam meŋga qwassɨt tamdʼel emneu.</ta>
            <ta e="T35" id="Seg_359" s="T27">tau qarimɨɣɨn man qwälɨm meːlʼe taːderaw, tɨssoɣɨn perčenǯau.</ta>
            <ta e="T38" id="Seg_360" s="T35">tabɨla üːdəmɨɣɨn tʼüːnǯatt.</ta>
            <ta e="T41" id="Seg_361" s="T38">apsot qodowaŋ jenǯiŋ.</ta>
            <ta e="T43" id="Seg_362" s="T41">omdɨq awɨrq.</ta>
            <ta e="T51" id="Seg_363" s="T43">nilʼdʼiŋ menan tʼelɨt mendɨquŋ (tʼelɨmq(g)uŋ) i nilʼdʼiŋ üːdɨmguŋ.</ta>
            <ta e="T54" id="Seg_364" s="T51">nilʼdʼiŋ me iːluttə.</ta>
            <ta e="T58" id="Seg_365" s="T54">nilʼdʼi menani iləsan.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_366" s="T1">Nagɨrt qazasen. </ta>
            <ta e="T10" id="Seg_367" s="T3">Tan nagɨnnanǯ, a man tan köɣɨnt aːmdaŋ. </ta>
            <ta e="T16" id="Seg_368" s="T10">Tajze tʼälɨmpaŋ, a oneŋ qwälʼe meːʒəraŋ. </ta>
            <ta e="T22" id="Seg_369" s="T16">Mannan qwäl pedʼella, qaːza, oqqɨr nɨrsa. </ta>
            <ta e="T27" id="Seg_370" s="T22">Qwälam meŋga qwassɨt tamdʼel emneu. </ta>
            <ta e="T35" id="Seg_371" s="T27">Tau qarimɨɣɨn man qwälɨm meːlʼe taːderaw, tɨssoɣɨn perčenǯau. </ta>
            <ta e="T38" id="Seg_372" s="T35">Tabɨla üːdəmɨɣɨn tʼüːnǯatt. </ta>
            <ta e="T41" id="Seg_373" s="T38">Apsot qodowaŋ jenǯiŋ. </ta>
            <ta e="T43" id="Seg_374" s="T41">Omdɨq awɨrq. </ta>
            <ta e="T51" id="Seg_375" s="T43">Nilʼdʼiŋ menan tʼelɨt mendɨquŋ (tʼelɨmquŋ) i nilʼdʼiŋ üːdɨmguŋ. </ta>
            <ta e="T54" id="Seg_376" s="T51">Nilʼdʼiŋ me iːluttə. </ta>
            <ta e="T58" id="Seg_377" s="T54">Nilʼdʼi menan i iləsan. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_378" s="T1">nagɨr-t</ta>
            <ta e="T3" id="Seg_379" s="T2">qaza-se-n</ta>
            <ta e="T4" id="Seg_380" s="T3">tat</ta>
            <ta e="T5" id="Seg_381" s="T4">nagɨn-na-nǯ</ta>
            <ta e="T6" id="Seg_382" s="T5">a</ta>
            <ta e="T7" id="Seg_383" s="T6">man</ta>
            <ta e="T8" id="Seg_384" s="T7">tat</ta>
            <ta e="T9" id="Seg_385" s="T8">kö-ɣɨnt</ta>
            <ta e="T10" id="Seg_386" s="T9">aːmda-ŋ</ta>
            <ta e="T11" id="Seg_387" s="T10">taj-ze</ta>
            <ta e="T12" id="Seg_388" s="T11">tʼälɨmpa-ŋ</ta>
            <ta e="T13" id="Seg_389" s="T12">a</ta>
            <ta e="T14" id="Seg_390" s="T13">oneŋ</ta>
            <ta e="T15" id="Seg_391" s="T14">qwälʼ-e</ta>
            <ta e="T16" id="Seg_392" s="T15">meː-ʒə-r-a-ŋ</ta>
            <ta e="T17" id="Seg_393" s="T16">man-nan</ta>
            <ta e="T18" id="Seg_394" s="T17">qwäl</ta>
            <ta e="T19" id="Seg_395" s="T18">pedʼe-lla</ta>
            <ta e="T20" id="Seg_396" s="T19">qaːza</ta>
            <ta e="T21" id="Seg_397" s="T20">oqqɨr</ta>
            <ta e="T22" id="Seg_398" s="T21">nɨrsa</ta>
            <ta e="T23" id="Seg_399" s="T22">qwäl-a-m</ta>
            <ta e="T24" id="Seg_400" s="T23">meŋga</ta>
            <ta e="T25" id="Seg_401" s="T24">qwas-sɨ-t</ta>
            <ta e="T26" id="Seg_402" s="T25">tam-dʼel</ta>
            <ta e="T27" id="Seg_403" s="T26">emne-u</ta>
            <ta e="T28" id="Seg_404" s="T27">tau</ta>
            <ta e="T29" id="Seg_405" s="T28">qari-mɨ-ɣɨn</ta>
            <ta e="T30" id="Seg_406" s="T29">man</ta>
            <ta e="T31" id="Seg_407" s="T30">qwäl-ɨ-m</ta>
            <ta e="T32" id="Seg_408" s="T31">meː-lʼe</ta>
            <ta e="T33" id="Seg_409" s="T32">taːd-e-r-a-w</ta>
            <ta e="T34" id="Seg_410" s="T33">tɨsso-ɣɨn</ta>
            <ta e="T35" id="Seg_411" s="T34">per-č-enǯa-u</ta>
            <ta e="T36" id="Seg_412" s="T35">tab-ɨ-la</ta>
            <ta e="T37" id="Seg_413" s="T36">üːdə-mɨ-ɣɨn</ta>
            <ta e="T38" id="Seg_414" s="T37">tʼüː-nǯa-tt</ta>
            <ta e="T39" id="Seg_415" s="T38">apso-t</ta>
            <ta e="T40" id="Seg_416" s="T39">qodowa-ŋ</ta>
            <ta e="T41" id="Seg_417" s="T40">je-nǯi-ŋ</ta>
            <ta e="T42" id="Seg_418" s="T41">omdɨ-q</ta>
            <ta e="T43" id="Seg_419" s="T42">aw-ɨ-r-q</ta>
            <ta e="T44" id="Seg_420" s="T43">nilʼdʼi-ŋ</ta>
            <ta e="T45" id="Seg_421" s="T44">me-nan</ta>
            <ta e="T46" id="Seg_422" s="T45">tʼelɨ-t</ta>
            <ta e="T47" id="Seg_423" s="T46">mendɨ-qu-ŋ</ta>
            <ta e="T48" id="Seg_424" s="T47">tʼelɨ-m-qu-ŋ</ta>
            <ta e="T49" id="Seg_425" s="T48">i</ta>
            <ta e="T50" id="Seg_426" s="T49">nilʼdʼi-ŋ</ta>
            <ta e="T51" id="Seg_427" s="T50">üːdɨ-m-gu-ŋ</ta>
            <ta e="T52" id="Seg_428" s="T51">nilʼdʼi-ŋ</ta>
            <ta e="T53" id="Seg_429" s="T52">me</ta>
            <ta e="T54" id="Seg_430" s="T53">iːl-uttə</ta>
            <ta e="T55" id="Seg_431" s="T54">nilʼdʼi</ta>
            <ta e="T56" id="Seg_432" s="T55">me-nan</ta>
            <ta e="T57" id="Seg_433" s="T56">i</ta>
            <ta e="T58" id="Seg_434" s="T57">ilə-san</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_435" s="T1">nagər-etɨ</ta>
            <ta e="T3" id="Seg_436" s="T2">qazaq-se-ŋ</ta>
            <ta e="T4" id="Seg_437" s="T3">tan</ta>
            <ta e="T5" id="Seg_438" s="T4">nagər-nɨ-ntə</ta>
            <ta e="T6" id="Seg_439" s="T5">a</ta>
            <ta e="T7" id="Seg_440" s="T6">man</ta>
            <ta e="T8" id="Seg_441" s="T7">tan</ta>
            <ta e="T9" id="Seg_442" s="T8">kö-qɨntɨ</ta>
            <ta e="T10" id="Seg_443" s="T9">amdɨ-ŋ</ta>
            <ta e="T11" id="Seg_444" s="T10">tan-se</ta>
            <ta e="T12" id="Seg_445" s="T11">tʼalɨmpə-ŋ</ta>
            <ta e="T13" id="Seg_446" s="T12">a</ta>
            <ta e="T14" id="Seg_447" s="T13">oneŋ</ta>
            <ta e="T15" id="Seg_448" s="T14">qwɛl-ɨ</ta>
            <ta e="T16" id="Seg_449" s="T15">meː-ʒə-r-nɨ-ŋ</ta>
            <ta e="T17" id="Seg_450" s="T16">man-nan</ta>
            <ta e="T18" id="Seg_451" s="T17">qwɛl</ta>
            <ta e="T19" id="Seg_452" s="T18">pedʼe-la</ta>
            <ta e="T20" id="Seg_453" s="T19">qaːza</ta>
            <ta e="T21" id="Seg_454" s="T20">okkɨr</ta>
            <ta e="T22" id="Seg_455" s="T21">nɨrsa</ta>
            <ta e="T23" id="Seg_456" s="T22">qwɛl-ɨ-m</ta>
            <ta e="T24" id="Seg_457" s="T23">mekka</ta>
            <ta e="T25" id="Seg_458" s="T24">qwat-sɨ-t</ta>
            <ta e="T26" id="Seg_459" s="T25">taw-dʼel</ta>
            <ta e="T27" id="Seg_460" s="T26">ämnä-nɨ</ta>
            <ta e="T28" id="Seg_461" s="T27">taw</ta>
            <ta e="T29" id="Seg_462" s="T28">qare-mɨ-qɨn</ta>
            <ta e="T30" id="Seg_463" s="T29">man</ta>
            <ta e="T31" id="Seg_464" s="T30">qwɛl-ɨ-m</ta>
            <ta e="T32" id="Seg_465" s="T31">meː-le</ta>
            <ta e="T33" id="Seg_466" s="T32">tat-ɨ-r-ɨ-w</ta>
            <ta e="T34" id="Seg_467" s="T33">tɨssɨ-qɨn</ta>
            <ta e="T35" id="Seg_468" s="T34">per-ču-enǯɨ-w</ta>
            <ta e="T36" id="Seg_469" s="T35">täp-ɨ-la</ta>
            <ta e="T37" id="Seg_470" s="T36">üdə-mɨ-qɨn</ta>
            <ta e="T38" id="Seg_471" s="T37">tüː-enǯɨ-tɨn</ta>
            <ta e="T39" id="Seg_472" s="T38">apsǝ-tə</ta>
            <ta e="T40" id="Seg_473" s="T39">qodowa-ŋ</ta>
            <ta e="T41" id="Seg_474" s="T40">eː-enǯɨ-n</ta>
            <ta e="T42" id="Seg_475" s="T41">amdɨ-kɨ</ta>
            <ta e="T43" id="Seg_476" s="T42">am-ɨ-r-kɨ</ta>
            <ta e="T44" id="Seg_477" s="T43">nʼilʼdʼi-ŋ</ta>
            <ta e="T45" id="Seg_478" s="T44">me-nan</ta>
            <ta e="T46" id="Seg_479" s="T45">tʼeːlɨ-tə</ta>
            <ta e="T47" id="Seg_480" s="T46">mendɨ-ku-n</ta>
            <ta e="T48" id="Seg_481" s="T47">tʼeːlɨ-m-ku-n</ta>
            <ta e="T49" id="Seg_482" s="T48">i</ta>
            <ta e="T50" id="Seg_483" s="T49">nʼilʼdʼi-ŋ</ta>
            <ta e="T51" id="Seg_484" s="T50">üdə-m-ku-n</ta>
            <ta e="T52" id="Seg_485" s="T51">nʼilʼdʼi-ŋ</ta>
            <ta e="T53" id="Seg_486" s="T52">me</ta>
            <ta e="T54" id="Seg_487" s="T53">elɨ-un</ta>
            <ta e="T55" id="Seg_488" s="T54">nʼilʼdʼi</ta>
            <ta e="T56" id="Seg_489" s="T55">me-nan</ta>
            <ta e="T57" id="Seg_490" s="T56">i</ta>
            <ta e="T58" id="Seg_491" s="T57">elɨ-san</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_492" s="T1">write-IMP.2SG.O</ta>
            <ta e="T3" id="Seg_493" s="T2">Russian-language-ADVZ</ta>
            <ta e="T4" id="Seg_494" s="T3">you.SG.NOM</ta>
            <ta e="T5" id="Seg_495" s="T4">write-CO-2SG.S</ta>
            <ta e="T6" id="Seg_496" s="T5">and</ta>
            <ta e="T7" id="Seg_497" s="T6">I.NOM</ta>
            <ta e="T8" id="Seg_498" s="T7">you.SG.GEN</ta>
            <ta e="T9" id="Seg_499" s="T8">side-ILL.3SG</ta>
            <ta e="T10" id="Seg_500" s="T9">sit-1SG.S</ta>
            <ta e="T11" id="Seg_501" s="T10">you.SG-COM</ta>
            <ta e="T12" id="Seg_502" s="T11">talk-1SG.S</ta>
            <ta e="T13" id="Seg_503" s="T12">and</ta>
            <ta e="T14" id="Seg_504" s="T13">oneself.1SG</ta>
            <ta e="T15" id="Seg_505" s="T14">fish-EP.[NOM]</ta>
            <ta e="T16" id="Seg_506" s="T15">do-DRV-DRV-CO-1SG.S</ta>
            <ta e="T17" id="Seg_507" s="T16">I-ADES</ta>
            <ta e="T18" id="Seg_508" s="T17">fish.[NOM]</ta>
            <ta e="T19" id="Seg_509" s="T18">roach-PL.[NOM]</ta>
            <ta e="T20" id="Seg_510" s="T19">perch.[NOM]</ta>
            <ta e="T21" id="Seg_511" s="T20">one</ta>
            <ta e="T22" id="Seg_512" s="T21">ruff.[NOM]</ta>
            <ta e="T23" id="Seg_513" s="T22">fish-EP-ACC</ta>
            <ta e="T24" id="Seg_514" s="T23">I.ALL</ta>
            <ta e="T25" id="Seg_515" s="T24">catch-PST-3SG.O</ta>
            <ta e="T26" id="Seg_516" s="T25">this-day.[NOM]</ta>
            <ta e="T27" id="Seg_517" s="T26">son_in_law.[NOM]-1SG</ta>
            <ta e="T28" id="Seg_518" s="T27">this</ta>
            <ta e="T29" id="Seg_519" s="T28">morning-something-LOC</ta>
            <ta e="T30" id="Seg_520" s="T29">I.NOM</ta>
            <ta e="T31" id="Seg_521" s="T30">fish-EP-ACC</ta>
            <ta e="T32" id="Seg_522" s="T31">do-CVB</ta>
            <ta e="T33" id="Seg_523" s="T32">bring-EP-FRQ-EP-1SG.O</ta>
            <ta e="T34" id="Seg_524" s="T33">vessel-LOC</ta>
            <ta e="T35" id="Seg_525" s="T34">fry-TR-FUT-1SG.O</ta>
            <ta e="T36" id="Seg_526" s="T35">(s)he-EP-PL.[NOM]</ta>
            <ta e="T37" id="Seg_527" s="T36">evening-something-LOC</ta>
            <ta e="T38" id="Seg_528" s="T37">come-FUT-3PL</ta>
            <ta e="T39" id="Seg_529" s="T38">food.[NOM]-3SG</ta>
            <ta e="T40" id="Seg_530" s="T39">ready-ADVZ</ta>
            <ta e="T41" id="Seg_531" s="T40">be-FUT-3SG.S</ta>
            <ta e="T42" id="Seg_532" s="T41">sit-IMP.2SG.S</ta>
            <ta e="T43" id="Seg_533" s="T42">eat-EP-FRQ-IMP.2SG.S</ta>
            <ta e="T44" id="Seg_534" s="T43">such-ADVZ</ta>
            <ta e="T45" id="Seg_535" s="T44">we-ADES</ta>
            <ta e="T46" id="Seg_536" s="T45">day.[NOM]-3SG</ta>
            <ta e="T47" id="Seg_537" s="T46">pass-HAB-3SG.S</ta>
            <ta e="T48" id="Seg_538" s="T47">day-TRL-HAB-3SG.S</ta>
            <ta e="T49" id="Seg_539" s="T48">and</ta>
            <ta e="T50" id="Seg_540" s="T49">so-ADVZ</ta>
            <ta e="T51" id="Seg_541" s="T50">evening-TRL-HAB-3SG.S</ta>
            <ta e="T52" id="Seg_542" s="T51">such-ADVZ</ta>
            <ta e="T53" id="Seg_543" s="T52">we.[NOM]</ta>
            <ta e="T54" id="Seg_544" s="T53">live-1PL</ta>
            <ta e="T55" id="Seg_545" s="T54">such</ta>
            <ta e="T56" id="Seg_546" s="T55">we-ADES</ta>
            <ta e="T57" id="Seg_547" s="T56">and</ta>
            <ta e="T58" id="Seg_548" s="T57">live-ACTN.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_549" s="T1">написать-IMP.2SG.O</ta>
            <ta e="T3" id="Seg_550" s="T2">русский-язык-ADVZ</ta>
            <ta e="T4" id="Seg_551" s="T3">ты.NOM</ta>
            <ta e="T5" id="Seg_552" s="T4">написать-CO-2SG.S</ta>
            <ta e="T6" id="Seg_553" s="T5">а</ta>
            <ta e="T7" id="Seg_554" s="T6">я.NOM</ta>
            <ta e="T8" id="Seg_555" s="T7">ты.GEN</ta>
            <ta e="T9" id="Seg_556" s="T8">бок-ILL.3SG</ta>
            <ta e="T10" id="Seg_557" s="T9">сидеть-1SG.S</ta>
            <ta e="T11" id="Seg_558" s="T10">ты-COM</ta>
            <ta e="T12" id="Seg_559" s="T11">разговаривать-1SG.S</ta>
            <ta e="T13" id="Seg_560" s="T12">а</ta>
            <ta e="T14" id="Seg_561" s="T13">сам.1SG</ta>
            <ta e="T15" id="Seg_562" s="T14">рыба-EP.[NOM]</ta>
            <ta e="T16" id="Seg_563" s="T15">сделать-DRV-DRV-CO-1SG.S</ta>
            <ta e="T17" id="Seg_564" s="T16">я-ADES</ta>
            <ta e="T18" id="Seg_565" s="T17">рыба.[NOM]</ta>
            <ta e="T19" id="Seg_566" s="T18">чебак-PL.[NOM]</ta>
            <ta e="T20" id="Seg_567" s="T19">окунь.[NOM]</ta>
            <ta e="T21" id="Seg_568" s="T20">один</ta>
            <ta e="T22" id="Seg_569" s="T21">ёрш.[NOM]</ta>
            <ta e="T23" id="Seg_570" s="T22">рыба-EP-ACC</ta>
            <ta e="T24" id="Seg_571" s="T23">я.ALL</ta>
            <ta e="T25" id="Seg_572" s="T24">поймать-PST-3SG.O</ta>
            <ta e="T26" id="Seg_573" s="T25">этот-день.[NOM]</ta>
            <ta e="T27" id="Seg_574" s="T26">зять.[NOM]-1SG</ta>
            <ta e="T28" id="Seg_575" s="T27">этот</ta>
            <ta e="T29" id="Seg_576" s="T28">утро-нечто-LOC</ta>
            <ta e="T30" id="Seg_577" s="T29">я.NOM</ta>
            <ta e="T31" id="Seg_578" s="T30">рыба-EP-ACC</ta>
            <ta e="T32" id="Seg_579" s="T31">сделать-CVB</ta>
            <ta e="T33" id="Seg_580" s="T32">принести-EP-FRQ-EP-1SG.O</ta>
            <ta e="T34" id="Seg_581" s="T33">посудина-LOC</ta>
            <ta e="T35" id="Seg_582" s="T34">пожарить-TR-FUT-1SG.O</ta>
            <ta e="T36" id="Seg_583" s="T35">он(а)-EP-PL.[NOM]</ta>
            <ta e="T37" id="Seg_584" s="T36">вечер-нечто-LOC</ta>
            <ta e="T38" id="Seg_585" s="T37">прийти-FUT-3PL</ta>
            <ta e="T39" id="Seg_586" s="T38">еда.[NOM]-3SG</ta>
            <ta e="T40" id="Seg_587" s="T39">готовый-ADVZ</ta>
            <ta e="T41" id="Seg_588" s="T40">быть-FUT-3SG.S</ta>
            <ta e="T42" id="Seg_589" s="T41">сидеть-IMP.2SG.S</ta>
            <ta e="T43" id="Seg_590" s="T42">съесть-EP-FRQ-IMP.2SG.S</ta>
            <ta e="T44" id="Seg_591" s="T43">такой-ADVZ</ta>
            <ta e="T45" id="Seg_592" s="T44">мы-ADES</ta>
            <ta e="T46" id="Seg_593" s="T45">день.[NOM]-3SG</ta>
            <ta e="T47" id="Seg_594" s="T46">пройти-HAB-3SG.S</ta>
            <ta e="T48" id="Seg_595" s="T47">день-TRL-HAB-3SG.S</ta>
            <ta e="T49" id="Seg_596" s="T48">и</ta>
            <ta e="T50" id="Seg_597" s="T49">так-ADVZ</ta>
            <ta e="T51" id="Seg_598" s="T50">вечер-TRL-HAB-3SG.S</ta>
            <ta e="T52" id="Seg_599" s="T51">такой-ADVZ</ta>
            <ta e="T53" id="Seg_600" s="T52">мы.[NOM]</ta>
            <ta e="T54" id="Seg_601" s="T53">жить-1PL</ta>
            <ta e="T55" id="Seg_602" s="T54">такой</ta>
            <ta e="T56" id="Seg_603" s="T55">мы-ADES</ta>
            <ta e="T57" id="Seg_604" s="T56">и</ta>
            <ta e="T58" id="Seg_605" s="T57">жить-ACTN.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_606" s="T1">v-v:mood.pn</ta>
            <ta e="T3" id="Seg_607" s="T2">adj-n-adj&gt;adv</ta>
            <ta e="T4" id="Seg_608" s="T3">pers</ta>
            <ta e="T5" id="Seg_609" s="T4">v-v:ins-v:pn</ta>
            <ta e="T6" id="Seg_610" s="T5">conj</ta>
            <ta e="T7" id="Seg_611" s="T6">pers</ta>
            <ta e="T8" id="Seg_612" s="T7">pers</ta>
            <ta e="T9" id="Seg_613" s="T8">n-n:case.poss</ta>
            <ta e="T10" id="Seg_614" s="T9">v-v:pn</ta>
            <ta e="T11" id="Seg_615" s="T10">pers-n:case</ta>
            <ta e="T12" id="Seg_616" s="T11">v-v:pn</ta>
            <ta e="T13" id="Seg_617" s="T12">conj</ta>
            <ta e="T14" id="Seg_618" s="T13">emphpro</ta>
            <ta e="T15" id="Seg_619" s="T14">n-n:ins.[n:case]</ta>
            <ta e="T16" id="Seg_620" s="T15">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T17" id="Seg_621" s="T16">pers-n:case</ta>
            <ta e="T18" id="Seg_622" s="T17">n.[n:case]</ta>
            <ta e="T19" id="Seg_623" s="T18">n-n:num.[n:case]</ta>
            <ta e="T20" id="Seg_624" s="T19">n.[n:case]</ta>
            <ta e="T21" id="Seg_625" s="T20">num</ta>
            <ta e="T22" id="Seg_626" s="T21">n.[n:case]</ta>
            <ta e="T23" id="Seg_627" s="T22">n-n:ins-n:case</ta>
            <ta e="T24" id="Seg_628" s="T23">pers</ta>
            <ta e="T25" id="Seg_629" s="T24">v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_630" s="T25">dem-n.[n:case]</ta>
            <ta e="T27" id="Seg_631" s="T26">n.[n:case]-n:poss</ta>
            <ta e="T28" id="Seg_632" s="T27">dem</ta>
            <ta e="T29" id="Seg_633" s="T28">n-n-n:case</ta>
            <ta e="T30" id="Seg_634" s="T29">pers</ta>
            <ta e="T31" id="Seg_635" s="T30">n-n:ins-n:case</ta>
            <ta e="T32" id="Seg_636" s="T31">v-v&gt;adv</ta>
            <ta e="T33" id="Seg_637" s="T32">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T34" id="Seg_638" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_639" s="T34">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_640" s="T35">pers-n:ins-n:num.[n:case]</ta>
            <ta e="T37" id="Seg_641" s="T36">n-n-n:case</ta>
            <ta e="T38" id="Seg_642" s="T37">v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_643" s="T38">n.[n:case]-n:poss</ta>
            <ta e="T40" id="Seg_644" s="T39">adj-adj&gt;adv</ta>
            <ta e="T41" id="Seg_645" s="T40">v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_646" s="T41">v-v:mood.pn</ta>
            <ta e="T43" id="Seg_647" s="T42">v-n:ins-v&gt;v-v:mood.pn</ta>
            <ta e="T44" id="Seg_648" s="T43">adj-adj&gt;adv</ta>
            <ta e="T45" id="Seg_649" s="T44">pers-n:case</ta>
            <ta e="T46" id="Seg_650" s="T45">n.[n:case]-n:poss</ta>
            <ta e="T47" id="Seg_651" s="T46">v-v&gt;v-v:pn</ta>
            <ta e="T48" id="Seg_652" s="T47">n-n&gt;v-v&gt;v-v:pn</ta>
            <ta e="T49" id="Seg_653" s="T48">conj</ta>
            <ta e="T50" id="Seg_654" s="T49">adv-adj&gt;adv</ta>
            <ta e="T51" id="Seg_655" s="T50">n-n&gt;v-v&gt;v-v:pn</ta>
            <ta e="T52" id="Seg_656" s="T51">adj-adj&gt;adv</ta>
            <ta e="T53" id="Seg_657" s="T52">pers.[n:case]</ta>
            <ta e="T54" id="Seg_658" s="T53">v-v:pn</ta>
            <ta e="T55" id="Seg_659" s="T54">adj</ta>
            <ta e="T56" id="Seg_660" s="T55">pers-n:case</ta>
            <ta e="T57" id="Seg_661" s="T56">conj</ta>
            <ta e="T58" id="Seg_662" s="T57">v-v&gt;n.[n:case]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_663" s="T1">v</ta>
            <ta e="T3" id="Seg_664" s="T2">adv</ta>
            <ta e="T4" id="Seg_665" s="T3">pers</ta>
            <ta e="T5" id="Seg_666" s="T4">v</ta>
            <ta e="T6" id="Seg_667" s="T5">conj</ta>
            <ta e="T7" id="Seg_668" s="T6">pers</ta>
            <ta e="T8" id="Seg_669" s="T7">pers</ta>
            <ta e="T9" id="Seg_670" s="T8">n</ta>
            <ta e="T10" id="Seg_671" s="T9">v</ta>
            <ta e="T11" id="Seg_672" s="T10">pers</ta>
            <ta e="T12" id="Seg_673" s="T11">v</ta>
            <ta e="T13" id="Seg_674" s="T12">conj</ta>
            <ta e="T14" id="Seg_675" s="T13">emphpro</ta>
            <ta e="T15" id="Seg_676" s="T14">n</ta>
            <ta e="T16" id="Seg_677" s="T15">v</ta>
            <ta e="T17" id="Seg_678" s="T16">pers</ta>
            <ta e="T18" id="Seg_679" s="T17">n</ta>
            <ta e="T19" id="Seg_680" s="T18">n</ta>
            <ta e="T20" id="Seg_681" s="T19">n</ta>
            <ta e="T21" id="Seg_682" s="T20">num</ta>
            <ta e="T22" id="Seg_683" s="T21">n</ta>
            <ta e="T23" id="Seg_684" s="T22">n</ta>
            <ta e="T24" id="Seg_685" s="T23">pers</ta>
            <ta e="T25" id="Seg_686" s="T24">v</ta>
            <ta e="T26" id="Seg_687" s="T25">n</ta>
            <ta e="T27" id="Seg_688" s="T26">n</ta>
            <ta e="T28" id="Seg_689" s="T27">dem</ta>
            <ta e="T29" id="Seg_690" s="T28">n</ta>
            <ta e="T30" id="Seg_691" s="T29">pers</ta>
            <ta e="T31" id="Seg_692" s="T30">n</ta>
            <ta e="T32" id="Seg_693" s="T31">adv</ta>
            <ta e="T33" id="Seg_694" s="T32">v</ta>
            <ta e="T34" id="Seg_695" s="T33">n</ta>
            <ta e="T35" id="Seg_696" s="T34">v</ta>
            <ta e="T36" id="Seg_697" s="T35">pers</ta>
            <ta e="T37" id="Seg_698" s="T36">n</ta>
            <ta e="T38" id="Seg_699" s="T37">v</ta>
            <ta e="T39" id="Seg_700" s="T38">n</ta>
            <ta e="T40" id="Seg_701" s="T39">adv</ta>
            <ta e="T41" id="Seg_702" s="T40">v</ta>
            <ta e="T42" id="Seg_703" s="T41">v</ta>
            <ta e="T43" id="Seg_704" s="T42">v</ta>
            <ta e="T44" id="Seg_705" s="T43">adv</ta>
            <ta e="T45" id="Seg_706" s="T44">pers</ta>
            <ta e="T46" id="Seg_707" s="T45">n</ta>
            <ta e="T47" id="Seg_708" s="T46">v</ta>
            <ta e="T48" id="Seg_709" s="T47">v</ta>
            <ta e="T49" id="Seg_710" s="T48">conj</ta>
            <ta e="T50" id="Seg_711" s="T49">adv</ta>
            <ta e="T51" id="Seg_712" s="T50">n</ta>
            <ta e="T52" id="Seg_713" s="T51">adv</ta>
            <ta e="T53" id="Seg_714" s="T52">pers</ta>
            <ta e="T54" id="Seg_715" s="T53">v</ta>
            <ta e="T55" id="Seg_716" s="T54">adj</ta>
            <ta e="T56" id="Seg_717" s="T55">pers</ta>
            <ta e="T57" id="Seg_718" s="T56">conj</ta>
            <ta e="T58" id="Seg_719" s="T57">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T6" id="Seg_720" s="T5">RUS:gram</ta>
            <ta e="T13" id="Seg_721" s="T12">RUS:gram</ta>
            <ta e="T40" id="Seg_722" s="T39">RUS:core</ta>
            <ta e="T49" id="Seg_723" s="T48">RUS:gram</ta>
            <ta e="T57" id="Seg_724" s="T56">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_725" s="T1">Write in Russian.</ta>
            <ta e="T10" id="Seg_726" s="T3">You are writing, and I am sitting near you.</ta>
            <ta e="T16" id="Seg_727" s="T10">I am cleaning the fish while talking to you.</ta>
            <ta e="T22" id="Seg_728" s="T16">I have fish: roach, perch, one ruff.</ta>
            <ta e="T27" id="Seg_729" s="T22">My son-in-law caught today this fish for me.</ta>
            <ta e="T35" id="Seg_730" s="T27">This morning I am cleaning fish, I'll fry it in a bowl.</ta>
            <ta e="T38" id="Seg_731" s="T35">In the evening they will come.</ta>
            <ta e="T41" id="Seg_732" s="T38">The food will be ready.</ta>
            <ta e="T43" id="Seg_733" s="T41">Sit down and eat.</ta>
            <ta e="T51" id="Seg_734" s="T43">So the day is by us, so the evening comes.</ta>
            <ta e="T54" id="Seg_735" s="T51">So we live.</ta>
            <ta e="T58" id="Seg_736" s="T54">That's our life.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_737" s="T1">Schreibe auf Russisch.</ta>
            <ta e="T10" id="Seg_738" s="T3">Du schreibst und ich sitze neben dir.</ta>
            <ta e="T16" id="Seg_739" s="T10">Ich säubere den Fisch, während ich mit dir rede.</ta>
            <ta e="T22" id="Seg_740" s="T16">Ich habe Fisch: Rotauge, Barsch, einen Kaulbarsch.</ta>
            <ta e="T27" id="Seg_741" s="T22">Mein Schwiegersohn hat heute diese Fische für mich gefangen.</ta>
            <ta e="T35" id="Seg_742" s="T27">Heute Morgen säubere ich Fische, ich werde sie in einer Schüssel braten.</ta>
            <ta e="T38" id="Seg_743" s="T35">Sie werden am Abend kommen.</ta>
            <ta e="T41" id="Seg_744" s="T38">Das Essen wird fertig sein.</ta>
            <ta e="T43" id="Seg_745" s="T41">Setz dich und iss.</ta>
            <ta e="T51" id="Seg_746" s="T43">So läuft der Tag bei uns, so kommt der Abend.</ta>
            <ta e="T54" id="Seg_747" s="T51">So leben wir.</ta>
            <ta e="T58" id="Seg_748" s="T54">Das ist unser Leben.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_749" s="T1">Пиши по-русски.</ta>
            <ta e="T10" id="Seg_750" s="T3">Ты пишешь, а я около тебя сижу.</ta>
            <ta e="T16" id="Seg_751" s="T10">С тобой разговариваю, а сама рыбу чищу.</ta>
            <ta e="T22" id="Seg_752" s="T16">У меня рыба: чебаки, окуни (/окунь), один ерш.</ta>
            <ta e="T27" id="Seg_753" s="T22">Рыбу мне поймал сегодня мой зять.</ta>
            <ta e="T35" id="Seg_754" s="T27">Этим утром я рыбу чищу, в посудине пожарю.</ta>
            <ta e="T38" id="Seg_755" s="T35">Они вечером придут.</ta>
            <ta e="T41" id="Seg_756" s="T38">Еда готовая будет.</ta>
            <ta e="T43" id="Seg_757" s="T41">Садись ешь.</ta>
            <ta e="T51" id="Seg_758" s="T43">Так у нас день проходит (день настает) и так настаёт вечер.</ta>
            <ta e="T54" id="Seg_759" s="T51">Так мы живем.</ta>
            <ta e="T58" id="Seg_760" s="T54">Такая у нас и жизнь.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_761" s="T1">пиши по-русски</ta>
            <ta e="T10" id="Seg_762" s="T3">ты пишешь а я около тебя сижу</ta>
            <ta e="T16" id="Seg_763" s="T10">с тобой разговариваю а сама рыбу чищу</ta>
            <ta e="T22" id="Seg_764" s="T16">у меня рыба чебаки окуни один ерш</ta>
            <ta e="T27" id="Seg_765" s="T22">рыбу мне добыл сегодня зять</ta>
            <ta e="T35" id="Seg_766" s="T27">этим утром я рыбу чищу в посуде пожарю</ta>
            <ta e="T38" id="Seg_767" s="T35">они вечером придут</ta>
            <ta e="T41" id="Seg_768" s="T38">пища готовая будет</ta>
            <ta e="T43" id="Seg_769" s="T41">садись ешь</ta>
            <ta e="T51" id="Seg_770" s="T43">так у нас день проходит (так день настанет) и придет (на)станет) вечер</ta>
            <ta e="T54" id="Seg_771" s="T51">так мы и живем</ta>
            <ta e="T58" id="Seg_772" s="T54">такая у нас и жизнь</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T16" id="Seg_773" s="T10">[BrM:] Tentative analysis of 'meːʒəraŋ'.</ta>
            <ta e="T51" id="Seg_774" s="T43">[KuAI:] Variant: 'tʼelɨmguŋ'.</ta>
            <ta e="T58" id="Seg_775" s="T54">[BrM:] 'menani' changed to 'menan i', according to translation. Another possible analysis of menani: me-na-ni we-ADES-OBL.1DU</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
