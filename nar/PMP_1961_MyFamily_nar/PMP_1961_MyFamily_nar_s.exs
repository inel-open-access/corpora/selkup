<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PMP_1961_MyFamily_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PMP_1961_MyFamily_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">190</ud-information>
            <ud-information attribute-name="# HIAT:w">151</ud-information>
            <ud-information attribute-name="# e">151</ud-information>
            <ud-information attribute-name="# HIAT:u">28</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PMP">
            <abbreviation>PMP</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PMP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T152" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Tau</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">pajaga</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">qaim</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">tʼeŋga</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">kennɨt</ts>
                  <nts id="Seg_17" n="HIAT:ip">?</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_20" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">A</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">tan</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">man</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">dʼät</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">wes</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_37" n="HIAT:w" s="T11">čenimenǯal</ts>
                  <nts id="Seg_38" n="HIAT:ip">?</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_41" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">Man</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">warkaŋ</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_49" n="HIAT:w" s="T14">neːwze</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_53" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">Manan</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">jeɣan</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">iːw</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_65" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">Tab</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">maze</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_73" n="HIAT:w" s="T20">az</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">warkaŋ</ts>
                  <nts id="Seg_77" n="HIAT:ip">.</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_80" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">Mannan</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">nagur</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_88" n="HIAT:w" s="T24">nʼeun</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_91" n="HIAT:w" s="T25">negaiː</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_94" n="HIAT:w" s="T26">i</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_97" n="HIAT:w" s="T27">okkɨr</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_100" n="HIAT:w" s="T28">neganeːu</ts>
                  <nts id="Seg_101" n="HIAT:ip">.</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_104" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_106" n="HIAT:w" s="T29">Manan</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_109" n="HIAT:w" s="T30">jewan</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_112" n="HIAT:w" s="T31">iːndimou</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_115" n="HIAT:w" s="T32">i</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_118" n="HIAT:w" s="T33">iːw</ts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_122" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_124" n="HIAT:w" s="T34">Tapstanan</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_127" n="HIAT:w" s="T35">okkɨr</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_130" n="HIAT:w" s="T36">ilʼmaːtti</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_134" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_136" n="HIAT:w" s="T37">Ešo</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_139" n="HIAT:w" s="T38">kɨba</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_142" n="HIAT:w" s="T39">jewan</ts>
                  <nts id="Seg_143" n="HIAT:ip">.</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_146" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_148" n="HIAT:w" s="T40">Tabne</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_151" n="HIAT:w" s="T41">nagur</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_154" n="HIAT:w" s="T42">eret</ts>
                  <nts id="Seg_155" n="HIAT:ip">,</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_158" n="HIAT:w" s="T43">titamǯelʼi</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_161" n="HIAT:w" s="T44">eret</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_164" n="HIAT:w" s="T45">üːbumpan</ts>
                  <nts id="Seg_165" n="HIAT:ip">.</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_168" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_170" n="HIAT:w" s="T46">Man</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_173" n="HIAT:w" s="T47">iːwse</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_176" n="HIAT:w" s="T48">az</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_179" n="HIAT:w" s="T49">warkaŋ</ts>
                  <nts id="Seg_180" n="HIAT:ip">.</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_183" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_185" n="HIAT:w" s="T50">Tap</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_188" n="HIAT:w" s="T51">pajandɨse</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_191" n="HIAT:w" s="T52">meŋga</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_194" n="HIAT:w" s="T53">tʼüːmban</ts>
                  <nts id="Seg_195" n="HIAT:ip">,</nts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_198" n="HIAT:w" s="T54">az</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_201" n="HIAT:w" s="T55">warqaŋ</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_204" n="HIAT:w" s="T56">menan</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_207" n="HIAT:w" s="T57">warketʼiku</ts>
                  <nts id="Seg_208" n="HIAT:ip">.</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_211" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_213" n="HIAT:w" s="T58">Nɨnto</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_216" n="HIAT:w" s="T59">aj</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_219" n="HIAT:w" s="T60">qwanǯaɣə</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_222" n="HIAT:w" s="T61">qardʼel</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_225" n="HIAT:w" s="T62">alʼi</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_228" n="HIAT:w" s="T63">awdel</ts>
                  <nts id="Seg_229" n="HIAT:ip">.</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_232" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_234" n="HIAT:w" s="T64">Mannani</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_237" n="HIAT:w" s="T65">sideom</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_240" n="HIAT:w" s="T66">aj</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_243" n="HIAT:w" s="T67">ɨbanǯet</ts>
                  <nts id="Seg_244" n="HIAT:ip">.</nts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_247" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_249" n="HIAT:w" s="T68">Tapstʼäk</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_252" n="HIAT:w" s="T69">üːbenǯaq</ts>
                  <nts id="Seg_253" n="HIAT:ip">,</nts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_256" n="HIAT:w" s="T70">a</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_259" n="HIAT:w" s="T71">man</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_262" n="HIAT:w" s="T72">aj</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_265" n="HIAT:w" s="T73">kalʼemǯaŋ</ts>
                  <nts id="Seg_266" n="HIAT:ip">.</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_269" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_271" n="HIAT:w" s="T74">Türʼänǯaŋ</ts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_275" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_277" n="HIAT:w" s="T75">Tʼürɨq</ts>
                  <nts id="Seg_278" n="HIAT:ip">,</nts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_281" n="HIAT:w" s="T76">ik</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_284" n="HIAT:w" s="T77">tʼürɨq</ts>
                  <nts id="Seg_285" n="HIAT:ip">,</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_288" n="HIAT:w" s="T78">qaimnej</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_291" n="HIAT:w" s="T79">as</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_294" n="HIAT:w" s="T80">meːnǯau</ts>
                  <nts id="Seg_295" n="HIAT:ip">.</nts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_298" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_300" n="HIAT:w" s="T81">Tabɨm</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_303" n="HIAT:w" s="T82">iːdergu</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_306" n="HIAT:w" s="T83">keregeŋ</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_309" n="HIAT:w" s="T84">i</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_312" n="HIAT:w" s="T85">taften</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_315" n="HIAT:w" s="T86">iːdergu</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_318" n="HIAT:w" s="T87">keregeŋ</ts>
                  <nts id="Seg_319" n="HIAT:ip">.</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_322" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_324" n="HIAT:w" s="T88">Nu</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_327" n="HIAT:w" s="T89">seːbeŋ</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_330" n="HIAT:w" s="T90">nagɨrǯiku</ts>
                  <nts id="Seg_331" n="HIAT:ip">.</nts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_334" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_336" n="HIAT:w" s="T91">Manan</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_339" n="HIAT:w" s="T92">tʼett</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_342" n="HIAT:w" s="T93">nʼäganeun</ts>
                  <nts id="Seg_343" n="HIAT:ip">:</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_346" n="HIAT:w" s="T94">nagur</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_349" n="HIAT:w" s="T95">nʼägaiːw</ts>
                  <nts id="Seg_350" n="HIAT:ip">,</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_353" n="HIAT:w" s="T96">okkɨr</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_356" n="HIAT:w" s="T97">nʼäganeːu</ts>
                  <nts id="Seg_357" n="HIAT:ip">.</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_360" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_362" n="HIAT:w" s="T98">Mannaŋ</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_365" n="HIAT:w" s="T99">tabla</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_368" n="HIAT:w" s="T100">koːcʼuŋ</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_371" n="HIAT:w" s="T101">jeɣat</ts>
                  <nts id="Seg_372" n="HIAT:ip">.</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_375" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_377" n="HIAT:w" s="T102">Nano</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_380" n="HIAT:w" s="T103">kabenǯaŋ</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_383" n="HIAT:w" s="T104">qaborglaj</ts>
                  <nts id="Seg_384" n="HIAT:ip">.</nts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_387" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_389" n="HIAT:w" s="T105">Kabelbədi</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_392" n="HIAT:w" s="T106">qaborɣɨm</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_395" n="HIAT:w" s="T107">az</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_398" n="HIAT:w" s="T108">wargu</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_401" n="HIAT:w" s="T109">i</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_404" n="HIAT:w" s="T110">soːcʼka</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_407" n="HIAT:w" s="T111">kaborɣɨm</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_410" n="HIAT:w" s="T112">as</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_413" n="HIAT:w" s="T113">seːrgu</ts>
                  <nts id="Seg_414" n="HIAT:ip">.</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_417" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_419" n="HIAT:w" s="T114">Mannan</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_422" n="HIAT:w" s="T115">tamdʼel</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_425" n="HIAT:w" s="T116">wes</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_428" n="HIAT:w" s="T117">uːtʼku</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_431" n="HIAT:w" s="T118">qwannattə</ts>
                  <nts id="Seg_432" n="HIAT:ip">,</nts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_435" n="HIAT:w" s="T119">as</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_438" n="HIAT:w" s="T120">ondɨt</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_441" n="HIAT:w" s="T121">meːsodimɨɣɨndɨt</ts>
                  <nts id="Seg_442" n="HIAT:ip">,</nts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_445" n="HIAT:w" s="T122">a</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_448" n="HIAT:w" s="T123">wargulʼe</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_451" n="HIAT:w" s="T124">qun</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_454" n="HIAT:w" s="T125">meːsodimɨm</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_457" n="HIAT:w" s="T126">megu</ts>
                  <nts id="Seg_458" n="HIAT:ip">.</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_461" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_463" n="HIAT:w" s="T127">No</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_466" n="HIAT:w" s="T128">kut</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_469" n="HIAT:w" s="T129">na</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_472" n="HIAT:w" s="T130">qum</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_475" n="HIAT:w" s="T131">wes</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_478" n="HIAT:w" s="T132">qulannando</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_481" n="HIAT:w" s="T133">warɣɨŋ</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_484" n="HIAT:w" s="T134">jewan</ts>
                  <nts id="Seg_485" n="HIAT:ip">.</nts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_488" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_490" n="HIAT:w" s="T135">Tabne</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_493" n="HIAT:w" s="T136">uːdʼättə</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_496" n="HIAT:w" s="T137">wes</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_499" n="HIAT:w" s="T138">qula</ts>
                  <nts id="Seg_500" n="HIAT:ip">,</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_503" n="HIAT:w" s="T139">üŋgulǯimbattə</ts>
                  <nts id="Seg_504" n="HIAT:ip">.</nts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_507" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_509" n="HIAT:w" s="T140">Nu</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_512" n="HIAT:w" s="T141">i</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_515" n="HIAT:w" s="T142">nilʼdʼäredɨn</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_518" n="HIAT:w" s="T143">uːdʼättə</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_521" n="HIAT:w" s="T144">wes</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_524" n="HIAT:w" s="T145">qula</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_527" n="HIAT:w" s="T146">tabɨne</ts>
                  <nts id="Seg_528" n="HIAT:ip">.</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_531" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_533" n="HIAT:w" s="T147">Nu</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_536" n="HIAT:w" s="T148">seːbeŋ</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_539" n="HIAT:w" s="T149">nagɨnǯiku</ts>
                  <nts id="Seg_540" n="HIAT:ip">,</nts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_543" n="HIAT:w" s="T150">as</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_546" n="HIAT:w" s="T151">kɨgaŋ</ts>
                  <nts id="Seg_547" n="HIAT:ip">.</nts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T152" id="Seg_549" n="sc" s="T1">
               <ts e="T2" id="Seg_551" n="e" s="T1">Tau </ts>
               <ts e="T3" id="Seg_553" n="e" s="T2">pajaga </ts>
               <ts e="T4" id="Seg_555" n="e" s="T3">qaim </ts>
               <ts e="T5" id="Seg_557" n="e" s="T4">tʼeŋga </ts>
               <ts e="T6" id="Seg_559" n="e" s="T5">kennɨt? </ts>
               <ts e="T7" id="Seg_561" n="e" s="T6">A </ts>
               <ts e="T8" id="Seg_563" n="e" s="T7">tan </ts>
               <ts e="T9" id="Seg_565" n="e" s="T8">man </ts>
               <ts e="T10" id="Seg_567" n="e" s="T9">dʼät </ts>
               <ts e="T11" id="Seg_569" n="e" s="T10">wes </ts>
               <ts e="T12" id="Seg_571" n="e" s="T11">čenimenǯal? </ts>
               <ts e="T13" id="Seg_573" n="e" s="T12">Man </ts>
               <ts e="T14" id="Seg_575" n="e" s="T13">warkaŋ </ts>
               <ts e="T15" id="Seg_577" n="e" s="T14">neːwze. </ts>
               <ts e="T16" id="Seg_579" n="e" s="T15">Manan </ts>
               <ts e="T17" id="Seg_581" n="e" s="T16">jeɣan </ts>
               <ts e="T18" id="Seg_583" n="e" s="T17">iːw. </ts>
               <ts e="T19" id="Seg_585" n="e" s="T18">Tab </ts>
               <ts e="T20" id="Seg_587" n="e" s="T19">maze </ts>
               <ts e="T21" id="Seg_589" n="e" s="T20">az </ts>
               <ts e="T22" id="Seg_591" n="e" s="T21">warkaŋ. </ts>
               <ts e="T23" id="Seg_593" n="e" s="T22">Mannan </ts>
               <ts e="T24" id="Seg_595" n="e" s="T23">nagur </ts>
               <ts e="T25" id="Seg_597" n="e" s="T24">nʼeun </ts>
               <ts e="T26" id="Seg_599" n="e" s="T25">negaiː </ts>
               <ts e="T27" id="Seg_601" n="e" s="T26">i </ts>
               <ts e="T28" id="Seg_603" n="e" s="T27">okkɨr </ts>
               <ts e="T29" id="Seg_605" n="e" s="T28">neganeːu. </ts>
               <ts e="T30" id="Seg_607" n="e" s="T29">Manan </ts>
               <ts e="T31" id="Seg_609" n="e" s="T30">jewan </ts>
               <ts e="T32" id="Seg_611" n="e" s="T31">iːndimou </ts>
               <ts e="T33" id="Seg_613" n="e" s="T32">i </ts>
               <ts e="T34" id="Seg_615" n="e" s="T33">iːw. </ts>
               <ts e="T35" id="Seg_617" n="e" s="T34">Tapstanan </ts>
               <ts e="T36" id="Seg_619" n="e" s="T35">okkɨr </ts>
               <ts e="T37" id="Seg_621" n="e" s="T36">ilʼmaːtti. </ts>
               <ts e="T38" id="Seg_623" n="e" s="T37">Ešo </ts>
               <ts e="T39" id="Seg_625" n="e" s="T38">kɨba </ts>
               <ts e="T40" id="Seg_627" n="e" s="T39">jewan. </ts>
               <ts e="T41" id="Seg_629" n="e" s="T40">Tabne </ts>
               <ts e="T42" id="Seg_631" n="e" s="T41">nagur </ts>
               <ts e="T43" id="Seg_633" n="e" s="T42">eret, </ts>
               <ts e="T44" id="Seg_635" n="e" s="T43">titamǯelʼi </ts>
               <ts e="T45" id="Seg_637" n="e" s="T44">eret </ts>
               <ts e="T46" id="Seg_639" n="e" s="T45">üːbumpan. </ts>
               <ts e="T47" id="Seg_641" n="e" s="T46">Man </ts>
               <ts e="T48" id="Seg_643" n="e" s="T47">iːwse </ts>
               <ts e="T49" id="Seg_645" n="e" s="T48">az </ts>
               <ts e="T50" id="Seg_647" n="e" s="T49">warkaŋ. </ts>
               <ts e="T51" id="Seg_649" n="e" s="T50">Tap </ts>
               <ts e="T52" id="Seg_651" n="e" s="T51">pajandɨse </ts>
               <ts e="T53" id="Seg_653" n="e" s="T52">meŋga </ts>
               <ts e="T54" id="Seg_655" n="e" s="T53">tʼüːmban, </ts>
               <ts e="T55" id="Seg_657" n="e" s="T54">az </ts>
               <ts e="T56" id="Seg_659" n="e" s="T55">warqaŋ </ts>
               <ts e="T57" id="Seg_661" n="e" s="T56">menan </ts>
               <ts e="T58" id="Seg_663" n="e" s="T57">warketʼiku. </ts>
               <ts e="T59" id="Seg_665" n="e" s="T58">Nɨnto </ts>
               <ts e="T60" id="Seg_667" n="e" s="T59">aj </ts>
               <ts e="T61" id="Seg_669" n="e" s="T60">qwanǯaɣə </ts>
               <ts e="T62" id="Seg_671" n="e" s="T61">qardʼel </ts>
               <ts e="T63" id="Seg_673" n="e" s="T62">alʼi </ts>
               <ts e="T64" id="Seg_675" n="e" s="T63">awdel. </ts>
               <ts e="T65" id="Seg_677" n="e" s="T64">Mannani </ts>
               <ts e="T66" id="Seg_679" n="e" s="T65">sideom </ts>
               <ts e="T67" id="Seg_681" n="e" s="T66">aj </ts>
               <ts e="T68" id="Seg_683" n="e" s="T67">ɨbanǯet. </ts>
               <ts e="T69" id="Seg_685" n="e" s="T68">Tapstʼäk </ts>
               <ts e="T70" id="Seg_687" n="e" s="T69">üːbenǯaq, </ts>
               <ts e="T71" id="Seg_689" n="e" s="T70">a </ts>
               <ts e="T72" id="Seg_691" n="e" s="T71">man </ts>
               <ts e="T73" id="Seg_693" n="e" s="T72">aj </ts>
               <ts e="T74" id="Seg_695" n="e" s="T73">kalʼemǯaŋ. </ts>
               <ts e="T75" id="Seg_697" n="e" s="T74">Türʼänǯaŋ. </ts>
               <ts e="T76" id="Seg_699" n="e" s="T75">Tʼürɨq, </ts>
               <ts e="T77" id="Seg_701" n="e" s="T76">ik </ts>
               <ts e="T78" id="Seg_703" n="e" s="T77">tʼürɨq, </ts>
               <ts e="T79" id="Seg_705" n="e" s="T78">qaimnej </ts>
               <ts e="T80" id="Seg_707" n="e" s="T79">as </ts>
               <ts e="T81" id="Seg_709" n="e" s="T80">meːnǯau. </ts>
               <ts e="T82" id="Seg_711" n="e" s="T81">Tabɨm </ts>
               <ts e="T83" id="Seg_713" n="e" s="T82">iːdergu </ts>
               <ts e="T84" id="Seg_715" n="e" s="T83">keregeŋ </ts>
               <ts e="T85" id="Seg_717" n="e" s="T84">i </ts>
               <ts e="T86" id="Seg_719" n="e" s="T85">taften </ts>
               <ts e="T87" id="Seg_721" n="e" s="T86">iːdergu </ts>
               <ts e="T88" id="Seg_723" n="e" s="T87">keregeŋ. </ts>
               <ts e="T89" id="Seg_725" n="e" s="T88">Nu </ts>
               <ts e="T90" id="Seg_727" n="e" s="T89">seːbeŋ </ts>
               <ts e="T91" id="Seg_729" n="e" s="T90">nagɨrǯiku. </ts>
               <ts e="T92" id="Seg_731" n="e" s="T91">Manan </ts>
               <ts e="T93" id="Seg_733" n="e" s="T92">tʼett </ts>
               <ts e="T94" id="Seg_735" n="e" s="T93">nʼäganeun: </ts>
               <ts e="T95" id="Seg_737" n="e" s="T94">nagur </ts>
               <ts e="T96" id="Seg_739" n="e" s="T95">nʼägaiːw, </ts>
               <ts e="T97" id="Seg_741" n="e" s="T96">okkɨr </ts>
               <ts e="T98" id="Seg_743" n="e" s="T97">nʼäganeːu. </ts>
               <ts e="T99" id="Seg_745" n="e" s="T98">Mannaŋ </ts>
               <ts e="T100" id="Seg_747" n="e" s="T99">tabla </ts>
               <ts e="T101" id="Seg_749" n="e" s="T100">koːcʼuŋ </ts>
               <ts e="T102" id="Seg_751" n="e" s="T101">jeɣat. </ts>
               <ts e="T103" id="Seg_753" n="e" s="T102">Nano </ts>
               <ts e="T104" id="Seg_755" n="e" s="T103">kabenǯaŋ </ts>
               <ts e="T105" id="Seg_757" n="e" s="T104">qaborglaj. </ts>
               <ts e="T106" id="Seg_759" n="e" s="T105">Kabelbədi </ts>
               <ts e="T107" id="Seg_761" n="e" s="T106">qaborɣɨm </ts>
               <ts e="T108" id="Seg_763" n="e" s="T107">az </ts>
               <ts e="T109" id="Seg_765" n="e" s="T108">wargu </ts>
               <ts e="T110" id="Seg_767" n="e" s="T109">i </ts>
               <ts e="T111" id="Seg_769" n="e" s="T110">soːcʼka </ts>
               <ts e="T112" id="Seg_771" n="e" s="T111">kaborɣɨm </ts>
               <ts e="T113" id="Seg_773" n="e" s="T112">as </ts>
               <ts e="T114" id="Seg_775" n="e" s="T113">seːrgu. </ts>
               <ts e="T115" id="Seg_777" n="e" s="T114">Mannan </ts>
               <ts e="T116" id="Seg_779" n="e" s="T115">tamdʼel </ts>
               <ts e="T117" id="Seg_781" n="e" s="T116">wes </ts>
               <ts e="T118" id="Seg_783" n="e" s="T117">uːtʼku </ts>
               <ts e="T119" id="Seg_785" n="e" s="T118">qwannattə, </ts>
               <ts e="T120" id="Seg_787" n="e" s="T119">as </ts>
               <ts e="T121" id="Seg_789" n="e" s="T120">ondɨt </ts>
               <ts e="T122" id="Seg_791" n="e" s="T121">meːsodimɨɣɨndɨt, </ts>
               <ts e="T123" id="Seg_793" n="e" s="T122">a </ts>
               <ts e="T124" id="Seg_795" n="e" s="T123">wargulʼe </ts>
               <ts e="T125" id="Seg_797" n="e" s="T124">qun </ts>
               <ts e="T126" id="Seg_799" n="e" s="T125">meːsodimɨm </ts>
               <ts e="T127" id="Seg_801" n="e" s="T126">megu. </ts>
               <ts e="T128" id="Seg_803" n="e" s="T127">No </ts>
               <ts e="T129" id="Seg_805" n="e" s="T128">kut </ts>
               <ts e="T130" id="Seg_807" n="e" s="T129">na </ts>
               <ts e="T131" id="Seg_809" n="e" s="T130">qum </ts>
               <ts e="T132" id="Seg_811" n="e" s="T131">wes </ts>
               <ts e="T133" id="Seg_813" n="e" s="T132">qulannando </ts>
               <ts e="T134" id="Seg_815" n="e" s="T133">warɣɨŋ </ts>
               <ts e="T135" id="Seg_817" n="e" s="T134">jewan. </ts>
               <ts e="T136" id="Seg_819" n="e" s="T135">Tabne </ts>
               <ts e="T137" id="Seg_821" n="e" s="T136">uːdʼättə </ts>
               <ts e="T138" id="Seg_823" n="e" s="T137">wes </ts>
               <ts e="T139" id="Seg_825" n="e" s="T138">qula, </ts>
               <ts e="T140" id="Seg_827" n="e" s="T139">üŋgulǯimbattə. </ts>
               <ts e="T141" id="Seg_829" n="e" s="T140">Nu </ts>
               <ts e="T142" id="Seg_831" n="e" s="T141">i </ts>
               <ts e="T143" id="Seg_833" n="e" s="T142">nilʼdʼäredɨn </ts>
               <ts e="T144" id="Seg_835" n="e" s="T143">uːdʼättə </ts>
               <ts e="T145" id="Seg_837" n="e" s="T144">wes </ts>
               <ts e="T146" id="Seg_839" n="e" s="T145">qula </ts>
               <ts e="T147" id="Seg_841" n="e" s="T146">tabɨne. </ts>
               <ts e="T148" id="Seg_843" n="e" s="T147">Nu </ts>
               <ts e="T149" id="Seg_845" n="e" s="T148">seːbeŋ </ts>
               <ts e="T150" id="Seg_847" n="e" s="T149">nagɨnǯiku, </ts>
               <ts e="T151" id="Seg_849" n="e" s="T150">as </ts>
               <ts e="T152" id="Seg_851" n="e" s="T151">kɨgaŋ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_852" s="T1">PMP_1961_MyFamily_nar.001 (001.001)</ta>
            <ta e="T12" id="Seg_853" s="T6">PMP_1961_MyFamily_nar.002 (001.002)</ta>
            <ta e="T15" id="Seg_854" s="T12">PMP_1961_MyFamily_nar.003 (001.003)</ta>
            <ta e="T18" id="Seg_855" s="T15">PMP_1961_MyFamily_nar.004 (001.004)</ta>
            <ta e="T22" id="Seg_856" s="T18">PMP_1961_MyFamily_nar.005 (001.005)</ta>
            <ta e="T29" id="Seg_857" s="T22">PMP_1961_MyFamily_nar.006 (001.006)</ta>
            <ta e="T34" id="Seg_858" s="T29">PMP_1961_MyFamily_nar.007 (001.007)</ta>
            <ta e="T37" id="Seg_859" s="T34">PMP_1961_MyFamily_nar.008 (001.008)</ta>
            <ta e="T40" id="Seg_860" s="T37">PMP_1961_MyFamily_nar.009 (001.009)</ta>
            <ta e="T46" id="Seg_861" s="T40">PMP_1961_MyFamily_nar.010 (001.010)</ta>
            <ta e="T50" id="Seg_862" s="T46">PMP_1961_MyFamily_nar.011 (001.011)</ta>
            <ta e="T58" id="Seg_863" s="T50">PMP_1961_MyFamily_nar.012 (001.012)</ta>
            <ta e="T64" id="Seg_864" s="T58">PMP_1961_MyFamily_nar.013 (001.013)</ta>
            <ta e="T68" id="Seg_865" s="T64">PMP_1961_MyFamily_nar.014 (001.014)</ta>
            <ta e="T74" id="Seg_866" s="T68">PMP_1961_MyFamily_nar.015 (001.015)</ta>
            <ta e="T75" id="Seg_867" s="T74">PMP_1961_MyFamily_nar.016 (001.016)</ta>
            <ta e="T81" id="Seg_868" s="T75">PMP_1961_MyFamily_nar.017 (001.017)</ta>
            <ta e="T88" id="Seg_869" s="T81">PMP_1961_MyFamily_nar.018 (001.018)</ta>
            <ta e="T91" id="Seg_870" s="T88">PMP_1961_MyFamily_nar.019 (001.019)</ta>
            <ta e="T98" id="Seg_871" s="T91">PMP_1961_MyFamily_nar.020 (001.020)</ta>
            <ta e="T102" id="Seg_872" s="T98">PMP_1961_MyFamily_nar.021 (001.021)</ta>
            <ta e="T105" id="Seg_873" s="T102">PMP_1961_MyFamily_nar.022 (001.022)</ta>
            <ta e="T114" id="Seg_874" s="T105">PMP_1961_MyFamily_nar.023 (001.023)</ta>
            <ta e="T127" id="Seg_875" s="T114">PMP_1961_MyFamily_nar.024 (001.024)</ta>
            <ta e="T135" id="Seg_876" s="T127">PMP_1961_MyFamily_nar.025 (001.025)</ta>
            <ta e="T140" id="Seg_877" s="T135">PMP_1961_MyFamily_nar.026 (001.026)</ta>
            <ta e="T147" id="Seg_878" s="T140">PMP_1961_MyFamily_nar.027 (001.027)</ta>
            <ta e="T152" id="Seg_879" s="T147">PMP_1961_MyFamily_nar.028 (001.028)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_880" s="T1">тау па′jага ′kаим тʼеңга ке′нныт?</ta>
            <ta e="T12" id="Seg_881" s="T6">а тан ман′дʼӓт вес ′тшенимен′джал?</ta>
            <ta e="T15" id="Seg_882" s="T12">ман вар′каң ′не̄в(у)зе.</ta>
            <ta e="T18" id="Seg_883" s="T15">ма′нан ′jеɣан ӣw.</ta>
            <ta e="T22" id="Seg_884" s="T18">таб ма′зе аз вар′каң.</ta>
            <ta e="T29" id="Seg_885" s="T22">ма′ннан ′нагур ′нʼеун нега′ӣ и ок′кыр нега′не̄у.</ta>
            <ta e="T34" id="Seg_886" s="T29">ма′нан ′jеван ′ӣнди′моу и ′ӣw.</ta>
            <ta e="T37" id="Seg_887" s="T34">‵тапста′нан о′ккыр илʼ′ма̄тти.</ta>
            <ta e="T40" id="Seg_888" s="T37">ещо кы′ба ′jеван.</ta>
            <ta e="T46" id="Seg_889" s="T40">таб′не̨ ′нагур е′рет, ‵титам′джелʼи е′рет ӱ̄бум′пан.</ta>
            <ta e="T50" id="Seg_890" s="T46">ман ′ӣвсе аз вар′каң.</ta>
            <ta e="T58" id="Seg_891" s="T50">тап па′jандысе ′меңга ′тʼӱ̄мбан, аз вар′kаң менан вар′кетʼ(цʼ)ику.</ta>
            <ta e="T64" id="Seg_892" s="T58">нын′то ай kwан′джаɣъ kар′дʼел алʼи ′аwдел.</ta>
            <ta e="T68" id="Seg_893" s="T64">ма′ннани ′сидеом ′ай ы′банджет.</ta>
            <ta e="T74" id="Seg_894" s="T68">тапс′тʼӓк ′ӱ̄бенджаk, а ман ай ка′лʼемджаң.</ta>
            <ta e="T75" id="Seg_895" s="T74">′тӱрʼӓнджаң.</ta>
            <ta e="T81" id="Seg_896" s="T75">тʼӱрыk, ик ′тʼӱрыk, kаим′не̨й ас ′ме̄нджау.</ta>
            <ta e="T88" id="Seg_897" s="T81">та′бым ′ӣдергу кере′гең и таф′тен ′ӣдергу кере′гең.</ta>
            <ta e="T91" id="Seg_898" s="T88">ну ′се̄бең ′нагырджику.</ta>
            <ta e="T98" id="Seg_899" s="T91">ма′нан ′тʼетт нʼӓ‵га′неун: ′нагур нʼӓга′ӣв, ок′кыр нʼӓ‵га′не̄у.</ta>
            <ta e="T102" id="Seg_900" s="T98">ма′ннаң таб′ла ′ко̄цʼуң ′jеɣат.</ta>
            <ta e="T105" id="Seg_901" s="T102">на′но ка′бенджаң kа′борглай.</ta>
            <ta e="T114" id="Seg_902" s="T105">ка′белбъ(ы)ди kа′борɣым аз вар′гу и ′со̄цʼка ка′борɣ(г)ым ас ′се̄ргу.</ta>
            <ta e="T127" id="Seg_903" s="T114">ма′ннан там′дʼел вес ′ӯтʼку kwа′ннаттъ, ас он′дыт ′ме̄ ′содимыɣын′дыт, а варгу′лʼе ′kун ме̄′содимым ′мегу.</ta>
            <ta e="T135" id="Seg_904" s="T127">но ′кут на kум вес kула′ннандо вар′ɣың ′jеван.</ta>
            <ta e="T140" id="Seg_905" s="T135">таб′не ′ӯдʼӓттъ вес kу′ла, ‵ӱңгулджимбаттъ.</ta>
            <ta e="T147" id="Seg_906" s="T140">ну и нилʼдʼӓ′редын ′ӯдʼӓттъ вес kу′ла ‵табы′не.</ta>
            <ta e="T152" id="Seg_907" s="T147">ну ′се̄бең ′нагынджику, ас кы′гаң.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_908" s="T1">tau pajaga qaim tʼeŋga kennɨt?</ta>
            <ta e="T12" id="Seg_909" s="T6">a tan mandʼät wes čenimenǯal?</ta>
            <ta e="T15" id="Seg_910" s="T12">man warkaŋ neːw(u)ze.</ta>
            <ta e="T18" id="Seg_911" s="T15">manan jeɣan iːw.</ta>
            <ta e="T22" id="Seg_912" s="T18">tab maze az warkaŋ.</ta>
            <ta e="T29" id="Seg_913" s="T22">mannan nagur nʼeun negaiː i okkɨr neganeːu.</ta>
            <ta e="T34" id="Seg_914" s="T29">manan jewan iːndimou i iːw.</ta>
            <ta e="T37" id="Seg_915" s="T34">tapstanan okkɨr ilʼmaːtti.</ta>
            <ta e="T40" id="Seg_916" s="T37">ešo kɨba jewan.</ta>
            <ta e="T46" id="Seg_917" s="T40">tabne nagur eret, titamǯelʼi eret üːbumpan.</ta>
            <ta e="T50" id="Seg_918" s="T46">man iːwse az warkaŋ.</ta>
            <ta e="T58" id="Seg_919" s="T50">tap pajandɨse meŋga tʼüːmban, az warqaŋ menan warketʼ(cʼ)iku.</ta>
            <ta e="T64" id="Seg_920" s="T58">nɨnto aj qwanǯaɣə qardʼel alʼi awdel.</ta>
            <ta e="T68" id="Seg_921" s="T64">mannani sideom aj ɨbanǯet.</ta>
            <ta e="T74" id="Seg_922" s="T68">tapstʼäk üːbenǯaq, a man aj kalʼemǯaŋ.</ta>
            <ta e="T75" id="Seg_923" s="T74">türʼänǯaŋ.</ta>
            <ta e="T81" id="Seg_924" s="T75">tʼürɨq, ik tʼürɨq, qaimnej as meːnǯau.</ta>
            <ta e="T88" id="Seg_925" s="T81">tabɨm iːdergu keregeŋ i taften iːdergu keregeŋ.</ta>
            <ta e="T91" id="Seg_926" s="T88">nu seːbeŋ nagɨrǯiku.</ta>
            <ta e="T98" id="Seg_927" s="T91">manan tʼett nʼäganeun: nagur nʼägaiːw, okkɨr nʼäganeːu.</ta>
            <ta e="T102" id="Seg_928" s="T98">mannaŋ tabla koːcʼuŋ jeɣat.</ta>
            <ta e="T105" id="Seg_929" s="T102">nano kabenǯaŋ qaborglaj.</ta>
            <ta e="T114" id="Seg_930" s="T105">kabelbə(ɨ)di qaborɣɨm az wargu i soːcʼka kaborɣ(g)ɨm as seːrgu.</ta>
            <ta e="T127" id="Seg_931" s="T114">mannan tamdʼel wes uːtʼku qwannattə, as ondɨt meː sodimɨɣɨndɨt, a wargulʼe qun meːsodimɨm megu.</ta>
            <ta e="T135" id="Seg_932" s="T127">no kut na qum wes qulannando warɣɨŋ jewan.</ta>
            <ta e="T140" id="Seg_933" s="T135">tabne uːdʼättə wes qula, üŋgulǯimbattə.</ta>
            <ta e="T147" id="Seg_934" s="T140">nu i nilʼdʼäredɨn uːdʼättə wes qula tabɨne.</ta>
            <ta e="T152" id="Seg_935" s="T147">nu seːbeŋ nagɨnǯiku, as kɨgaŋ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_936" s="T1">Tau pajaga qaim tʼeŋga kennɨt? </ta>
            <ta e="T12" id="Seg_937" s="T6">A tan man dʼät wes čenimenǯal? </ta>
            <ta e="T15" id="Seg_938" s="T12">Man warkaŋ neːwze. </ta>
            <ta e="T18" id="Seg_939" s="T15">Manan jeɣan iːw. </ta>
            <ta e="T22" id="Seg_940" s="T18">Tab maze az warkaŋ. </ta>
            <ta e="T29" id="Seg_941" s="T22">Mannan nagur nʼeun negaiː i okkɨr neganeːu. </ta>
            <ta e="T34" id="Seg_942" s="T29">Manan jewan iːndimou i iːw. </ta>
            <ta e="T37" id="Seg_943" s="T34">Tapstanan okkɨr ilʼmaːtti. </ta>
            <ta e="T40" id="Seg_944" s="T37">Ešo kɨba jewan. </ta>
            <ta e="T46" id="Seg_945" s="T40">Tabne nagur eret, titamǯelʼi eret üːbumpan. </ta>
            <ta e="T50" id="Seg_946" s="T46">Man iːwse az warkaŋ. </ta>
            <ta e="T58" id="Seg_947" s="T50">Tap pajandɨse meŋga tʼüːmban, az warqaŋ menan warketʼiku. </ta>
            <ta e="T64" id="Seg_948" s="T58">Nɨnto aj qwanǯaɣə qardʼel alʼi awdel. </ta>
            <ta e="T68" id="Seg_949" s="T64">Mannani sideom aj ɨbanǯet. </ta>
            <ta e="T74" id="Seg_950" s="T68">Tapstʼäk üːbenǯaq, a man aj kalʼemǯaŋ. </ta>
            <ta e="T75" id="Seg_951" s="T74">Türʼänǯaŋ. </ta>
            <ta e="T81" id="Seg_952" s="T75">Tʼürɨq, ik tʼürɨq, qaimnej as meːnǯau. </ta>
            <ta e="T88" id="Seg_953" s="T81">Tabɨm iːdergu keregeŋ i taften iːdergu keregeŋ. </ta>
            <ta e="T91" id="Seg_954" s="T88">Nu seːbeŋ nagɨrǯiku. </ta>
            <ta e="T98" id="Seg_955" s="T91">Manan tʼett nʼäganeun: nagur nʼägaiːw, okkɨr nʼäganeːu. </ta>
            <ta e="T102" id="Seg_956" s="T98">Mannaŋ tabla koːcʼuŋ jeɣat. </ta>
            <ta e="T105" id="Seg_957" s="T102">Nano kabenǯaŋ qaborglaj. </ta>
            <ta e="T114" id="Seg_958" s="T105">Kabelbədi qaborɣɨm az wargu i soːcʼka kaborɣɨm as seːrgu. </ta>
            <ta e="T127" id="Seg_959" s="T114">Mannan tamdʼel wes uːtʼku qwannattə, as ondɨt meːsodimɨɣɨndɨt, a wargulʼe qun meːsodimɨm megu. </ta>
            <ta e="T135" id="Seg_960" s="T127">No kut na qum wes qulannando warɣɨŋ jewan. </ta>
            <ta e="T140" id="Seg_961" s="T135">Tabne uːdʼättə wes qula, üŋgulǯimbattə. </ta>
            <ta e="T147" id="Seg_962" s="T140">Nu i nilʼdʼäredɨn uːdʼättə wes qula tabɨne. </ta>
            <ta e="T152" id="Seg_963" s="T147">Nu seːbeŋ nagɨnǯiku, as kɨgaŋ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_964" s="T1">tau</ta>
            <ta e="T3" id="Seg_965" s="T2">paja-ga</ta>
            <ta e="T4" id="Seg_966" s="T3">qai-m</ta>
            <ta e="T5" id="Seg_967" s="T4">tʼeŋga</ta>
            <ta e="T6" id="Seg_968" s="T5">ken-nɨ-t</ta>
            <ta e="T7" id="Seg_969" s="T6">a</ta>
            <ta e="T8" id="Seg_970" s="T7">tat</ta>
            <ta e="T9" id="Seg_971" s="T8">man</ta>
            <ta e="T10" id="Seg_972" s="T9">dʼät</ta>
            <ta e="T11" id="Seg_973" s="T10">wes</ta>
            <ta e="T12" id="Seg_974" s="T11">čeni-m-enǯa-l</ta>
            <ta e="T13" id="Seg_975" s="T12">man</ta>
            <ta e="T14" id="Seg_976" s="T13">warka-ŋ</ta>
            <ta e="T15" id="Seg_977" s="T14">neː-w-ze</ta>
            <ta e="T16" id="Seg_978" s="T15">ma-nan</ta>
            <ta e="T17" id="Seg_979" s="T16">je-ɣa-n</ta>
            <ta e="T18" id="Seg_980" s="T17">iː-w</ta>
            <ta e="T19" id="Seg_981" s="T18">tab</ta>
            <ta e="T20" id="Seg_982" s="T19">ma-ze</ta>
            <ta e="T21" id="Seg_983" s="T20">az</ta>
            <ta e="T22" id="Seg_984" s="T21">warka-ŋ</ta>
            <ta e="T23" id="Seg_985" s="T22">man-nan</ta>
            <ta e="T24" id="Seg_986" s="T23">nagur</ta>
            <ta e="T25" id="Seg_987" s="T24">nʼe-u-n</ta>
            <ta e="T26" id="Seg_988" s="T25">ne-ga-iː</ta>
            <ta e="T27" id="Seg_989" s="T26">i</ta>
            <ta e="T28" id="Seg_990" s="T27">okkɨr</ta>
            <ta e="T29" id="Seg_991" s="T28">ne-ga-neː-u</ta>
            <ta e="T30" id="Seg_992" s="T29">ma-nan</ta>
            <ta e="T31" id="Seg_993" s="T30">je-wa-n</ta>
            <ta e="T32" id="Seg_994" s="T31">iːndimo-u</ta>
            <ta e="T33" id="Seg_995" s="T32">i</ta>
            <ta e="T34" id="Seg_996" s="T33">iː-w</ta>
            <ta e="T35" id="Seg_997" s="T34">tap-sta-nan</ta>
            <ta e="T36" id="Seg_998" s="T35">okkɨr</ta>
            <ta e="T37" id="Seg_999" s="T36">ilʼmaːt-ti</ta>
            <ta e="T38" id="Seg_1000" s="T37">ešo</ta>
            <ta e="T39" id="Seg_1001" s="T38">kɨba</ta>
            <ta e="T40" id="Seg_1002" s="T39">je-wa-n</ta>
            <ta e="T41" id="Seg_1003" s="T40">tab-ne</ta>
            <ta e="T42" id="Seg_1004" s="T41">nagur</ta>
            <ta e="T43" id="Seg_1005" s="T42">eret</ta>
            <ta e="T44" id="Seg_1006" s="T43">tita-mǯelʼi</ta>
            <ta e="T45" id="Seg_1007" s="T44">eret</ta>
            <ta e="T46" id="Seg_1008" s="T45">üːbu-mpa-n</ta>
            <ta e="T47" id="Seg_1009" s="T46">man</ta>
            <ta e="T48" id="Seg_1010" s="T47">iː-w-se</ta>
            <ta e="T49" id="Seg_1011" s="T48">az</ta>
            <ta e="T50" id="Seg_1012" s="T49">warka-ŋ</ta>
            <ta e="T51" id="Seg_1013" s="T50">tap</ta>
            <ta e="T52" id="Seg_1014" s="T51">paja-ndɨ-se</ta>
            <ta e="T53" id="Seg_1015" s="T52">meŋga</ta>
            <ta e="T54" id="Seg_1016" s="T53">tʼüː-mba-n</ta>
            <ta e="T55" id="Seg_1017" s="T54">az</ta>
            <ta e="T56" id="Seg_1018" s="T55">warqa-ŋ</ta>
            <ta e="T57" id="Seg_1019" s="T56">me-nan</ta>
            <ta e="T58" id="Seg_1020" s="T57">warke-tʼi-ku</ta>
            <ta e="T59" id="Seg_1021" s="T58">nɨnto</ta>
            <ta e="T60" id="Seg_1022" s="T59">aj</ta>
            <ta e="T61" id="Seg_1023" s="T60">qwan-ǯa-ɣə</ta>
            <ta e="T62" id="Seg_1024" s="T61">qar-dʼel</ta>
            <ta e="T63" id="Seg_1025" s="T62">alʼi</ta>
            <ta e="T64" id="Seg_1026" s="T63">aw-del</ta>
            <ta e="T65" id="Seg_1027" s="T64">man-nan-i</ta>
            <ta e="T66" id="Seg_1028" s="T65">side-o-m</ta>
            <ta e="T67" id="Seg_1029" s="T66">aj</ta>
            <ta e="T68" id="Seg_1030" s="T67">ɨba-nǯe-t</ta>
            <ta e="T69" id="Seg_1031" s="T68">tap-stʼäk</ta>
            <ta e="T70" id="Seg_1032" s="T69">üːb-enǯa-q</ta>
            <ta e="T71" id="Seg_1033" s="T70">a</ta>
            <ta e="T72" id="Seg_1034" s="T71">man</ta>
            <ta e="T73" id="Seg_1035" s="T72">aj</ta>
            <ta e="T74" id="Seg_1036" s="T73">kalʼe-m-ǯa-ŋ</ta>
            <ta e="T75" id="Seg_1037" s="T74">türʼ-änǯa-ŋ</ta>
            <ta e="T76" id="Seg_1038" s="T75">tʼürɨ-q</ta>
            <ta e="T77" id="Seg_1039" s="T76">ik</ta>
            <ta e="T78" id="Seg_1040" s="T77">tʼürɨ-q</ta>
            <ta e="T79" id="Seg_1041" s="T78">qai-m-nej</ta>
            <ta e="T80" id="Seg_1042" s="T79">as</ta>
            <ta e="T81" id="Seg_1043" s="T80">meː-nǯa-u</ta>
            <ta e="T82" id="Seg_1044" s="T81">tab-ǝ-m</ta>
            <ta e="T83" id="Seg_1045" s="T82">iːder-gu</ta>
            <ta e="T84" id="Seg_1046" s="T83">keregeŋ</ta>
            <ta e="T85" id="Seg_1047" s="T84">i</ta>
            <ta e="T86" id="Seg_1048" s="T85">tafte-n</ta>
            <ta e="T87" id="Seg_1049" s="T86">iːder-gu</ta>
            <ta e="T88" id="Seg_1050" s="T87">keregeŋ</ta>
            <ta e="T89" id="Seg_1051" s="T88">nu</ta>
            <ta e="T90" id="Seg_1052" s="T89">seːb-e-ŋ</ta>
            <ta e="T91" id="Seg_1053" s="T90">nagɨr-ǯi-ku</ta>
            <ta e="T92" id="Seg_1054" s="T91">ma-nan</ta>
            <ta e="T93" id="Seg_1055" s="T92">tʼett</ta>
            <ta e="T94" id="Seg_1056" s="T93">nʼä-ga-ne-un</ta>
            <ta e="T95" id="Seg_1057" s="T94">nagur</ta>
            <ta e="T96" id="Seg_1058" s="T95">nʼä-ga-iː-w</ta>
            <ta e="T97" id="Seg_1059" s="T96">okkɨr</ta>
            <ta e="T98" id="Seg_1060" s="T97">nʼä-ga-neː-u</ta>
            <ta e="T99" id="Seg_1061" s="T98">man-naŋ</ta>
            <ta e="T100" id="Seg_1062" s="T99">tab-la</ta>
            <ta e="T101" id="Seg_1063" s="T100">koːcʼu-ŋ</ta>
            <ta e="T102" id="Seg_1064" s="T101">je-ɣa-t</ta>
            <ta e="T103" id="Seg_1065" s="T102">nano</ta>
            <ta e="T104" id="Seg_1066" s="T103">kabe-n-ǯa-ŋ</ta>
            <ta e="T105" id="Seg_1067" s="T104">qaborg-la-j</ta>
            <ta e="T106" id="Seg_1068" s="T105">kabe-l-bədi</ta>
            <ta e="T107" id="Seg_1069" s="T106">qaborɣ-ɨ-m</ta>
            <ta e="T108" id="Seg_1070" s="T107">az</ta>
            <ta e="T109" id="Seg_1071" s="T108">war-gu</ta>
            <ta e="T110" id="Seg_1072" s="T109">i</ta>
            <ta e="T111" id="Seg_1073" s="T110">soːcʼka</ta>
            <ta e="T112" id="Seg_1074" s="T111">kaborɣ-ɨ-m</ta>
            <ta e="T113" id="Seg_1075" s="T112">as</ta>
            <ta e="T114" id="Seg_1076" s="T113">seːr-gu</ta>
            <ta e="T115" id="Seg_1077" s="T114">man-nan</ta>
            <ta e="T116" id="Seg_1078" s="T115">tam-dʼel</ta>
            <ta e="T117" id="Seg_1079" s="T116">wes</ta>
            <ta e="T118" id="Seg_1080" s="T117">uːtʼ-ku</ta>
            <ta e="T119" id="Seg_1081" s="T118">qwan-na-ttə</ta>
            <ta e="T120" id="Seg_1082" s="T119">as</ta>
            <ta e="T121" id="Seg_1083" s="T120">ondɨt</ta>
            <ta e="T122" id="Seg_1084" s="T121">meː-sodi-mɨ-ɣɨndɨt</ta>
            <ta e="T123" id="Seg_1085" s="T122">a</ta>
            <ta e="T124" id="Seg_1086" s="T123">wargu-lʼe</ta>
            <ta e="T125" id="Seg_1087" s="T124">qu-n</ta>
            <ta e="T126" id="Seg_1088" s="T125">meː-sodi-mɨ-m</ta>
            <ta e="T127" id="Seg_1089" s="T126">me-gu</ta>
            <ta e="T128" id="Seg_1090" s="T127">no</ta>
            <ta e="T129" id="Seg_1091" s="T128">kut</ta>
            <ta e="T130" id="Seg_1092" s="T129">na</ta>
            <ta e="T131" id="Seg_1093" s="T130">qum</ta>
            <ta e="T132" id="Seg_1094" s="T131">wes</ta>
            <ta e="T133" id="Seg_1095" s="T132">qu-la-nnando</ta>
            <ta e="T134" id="Seg_1096" s="T133">warɣɨ-ŋ</ta>
            <ta e="T135" id="Seg_1097" s="T134">je-wa-n</ta>
            <ta e="T136" id="Seg_1098" s="T135">tab-ne</ta>
            <ta e="T137" id="Seg_1099" s="T136">uːdʼä-ttə</ta>
            <ta e="T138" id="Seg_1100" s="T137">wes</ta>
            <ta e="T139" id="Seg_1101" s="T138">qu-la</ta>
            <ta e="T140" id="Seg_1102" s="T139">üŋgulǯi-mba-ttə</ta>
            <ta e="T141" id="Seg_1103" s="T140">nu</ta>
            <ta e="T142" id="Seg_1104" s="T141">i</ta>
            <ta e="T144" id="Seg_1105" s="T143">uːdʼä-ttə</ta>
            <ta e="T145" id="Seg_1106" s="T144">wes</ta>
            <ta e="T146" id="Seg_1107" s="T145">qu-la</ta>
            <ta e="T147" id="Seg_1108" s="T146">tab-ɨ-ne</ta>
            <ta e="T148" id="Seg_1109" s="T147">nu</ta>
            <ta e="T149" id="Seg_1110" s="T148">seːb-e-ŋ</ta>
            <ta e="T150" id="Seg_1111" s="T149">nagɨn-ǯi-ku</ta>
            <ta e="T151" id="Seg_1112" s="T150">as</ta>
            <ta e="T152" id="Seg_1113" s="T151">kɨga-ŋ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1114" s="T1">taw</ta>
            <ta e="T3" id="Seg_1115" s="T2">paja-ka</ta>
            <ta e="T4" id="Seg_1116" s="T3">qaj-m</ta>
            <ta e="T5" id="Seg_1117" s="T4">tekka</ta>
            <ta e="T6" id="Seg_1118" s="T5">ket-nɨ-t</ta>
            <ta e="T7" id="Seg_1119" s="T6">a</ta>
            <ta e="T8" id="Seg_1120" s="T7">tan</ta>
            <ta e="T9" id="Seg_1121" s="T8">man</ta>
            <ta e="T10" id="Seg_1122" s="T9">tʼаːt</ta>
            <ta e="T11" id="Seg_1123" s="T10">wesʼ</ta>
            <ta e="T12" id="Seg_1124" s="T11">čenčɨ-mbɨ-enǯɨ-l</ta>
            <ta e="T13" id="Seg_1125" s="T12">man</ta>
            <ta e="T14" id="Seg_1126" s="T13">warkɨ-ŋ</ta>
            <ta e="T15" id="Seg_1127" s="T14">ne-w-se</ta>
            <ta e="T16" id="Seg_1128" s="T15">man-nan</ta>
            <ta e="T17" id="Seg_1129" s="T16">eː-nɨ-n</ta>
            <ta e="T18" id="Seg_1130" s="T17">iː-w</ta>
            <ta e="T19" id="Seg_1131" s="T18">täp</ta>
            <ta e="T20" id="Seg_1132" s="T19">man-se</ta>
            <ta e="T21" id="Seg_1133" s="T20">asa</ta>
            <ta e="T22" id="Seg_1134" s="T21">warkɨ-n</ta>
            <ta e="T23" id="Seg_1135" s="T22">man-nan</ta>
            <ta e="T24" id="Seg_1136" s="T23">nagur</ta>
            <ta e="T25" id="Seg_1137" s="T24">ne-w-n</ta>
            <ta e="T26" id="Seg_1138" s="T25">ne-ka-iː</ta>
            <ta e="T27" id="Seg_1139" s="T26">i</ta>
            <ta e="T28" id="Seg_1140" s="T27">okkɨr</ta>
            <ta e="T29" id="Seg_1141" s="T28">ne-ka-ne-w</ta>
            <ta e="T30" id="Seg_1142" s="T29">man-nan</ta>
            <ta e="T31" id="Seg_1143" s="T30">eː-nɨ-n</ta>
            <ta e="T32" id="Seg_1144" s="T31">indɨma-w</ta>
            <ta e="T33" id="Seg_1145" s="T32">i</ta>
            <ta e="T34" id="Seg_1146" s="T33">iː-w</ta>
            <ta e="T35" id="Seg_1147" s="T34">täp-štja-nan</ta>
            <ta e="T36" id="Seg_1148" s="T35">okkɨr</ta>
            <ta e="T37" id="Seg_1149" s="T36">ilmat-di</ta>
            <ta e="T38" id="Seg_1150" s="T37">ešo</ta>
            <ta e="T39" id="Seg_1151" s="T38">qɨba</ta>
            <ta e="T40" id="Seg_1152" s="T39">eː-nɨ-n</ta>
            <ta e="T41" id="Seg_1153" s="T40">täp-nä</ta>
            <ta e="T42" id="Seg_1154" s="T41">nagur</ta>
            <ta e="T43" id="Seg_1155" s="T42">eret</ta>
            <ta e="T44" id="Seg_1156" s="T43">tettɨ-mǯʼeːli</ta>
            <ta e="T45" id="Seg_1157" s="T44">eret</ta>
            <ta e="T46" id="Seg_1158" s="T45">übɨ-mbɨ-n</ta>
            <ta e="T47" id="Seg_1159" s="T46">man</ta>
            <ta e="T48" id="Seg_1160" s="T47">iː-w-se</ta>
            <ta e="T49" id="Seg_1161" s="T48">asa</ta>
            <ta e="T50" id="Seg_1162" s="T49">warkɨ-ŋ</ta>
            <ta e="T51" id="Seg_1163" s="T50">täp</ta>
            <ta e="T52" id="Seg_1164" s="T51">paja-ntɨ-se</ta>
            <ta e="T53" id="Seg_1165" s="T52">mekka</ta>
            <ta e="T54" id="Seg_1166" s="T53">tüː-mbɨ-n</ta>
            <ta e="T55" id="Seg_1167" s="T54">asa</ta>
            <ta e="T56" id="Seg_1168" s="T55">wargɨ-ŋ</ta>
            <ta e="T57" id="Seg_1169" s="T56">me-nan</ta>
            <ta e="T58" id="Seg_1170" s="T57">warkɨ-ntɨ-gu</ta>
            <ta e="T59" id="Seg_1171" s="T58">nɨnto</ta>
            <ta e="T60" id="Seg_1172" s="T59">aj</ta>
            <ta e="T61" id="Seg_1173" s="T60">qwan-enǯɨ-qij</ta>
            <ta e="T62" id="Seg_1174" s="T61">qare-dʼel</ta>
            <ta e="T63" id="Seg_1175" s="T62">alʼi</ta>
            <ta e="T64" id="Seg_1176" s="T63">au-dʼel</ta>
            <ta e="T65" id="Seg_1177" s="T64">man-nan-i</ta>
            <ta e="T66" id="Seg_1178" s="T65">sidʼe-ɨ-w</ta>
            <ta e="T67" id="Seg_1179" s="T66">aj</ta>
            <ta e="T68" id="Seg_1180" s="T67">ɨba-enǯɨ-t</ta>
            <ta e="T69" id="Seg_1181" s="T68">täp-staɣɨ</ta>
            <ta e="T70" id="Seg_1182" s="T69">übə-enǯɨ-qij</ta>
            <ta e="T71" id="Seg_1183" s="T70">a</ta>
            <ta e="T72" id="Seg_1184" s="T71">man</ta>
            <ta e="T73" id="Seg_1185" s="T72">aj</ta>
            <ta e="T74" id="Seg_1186" s="T73">qalɨ-mbɨ-enǯɨ-ŋ</ta>
            <ta e="T75" id="Seg_1187" s="T74">tʼüru-enǯɨ-ŋ</ta>
            <ta e="T76" id="Seg_1188" s="T75">tʼüru-kɨ</ta>
            <ta e="T77" id="Seg_1189" s="T76">igə</ta>
            <ta e="T78" id="Seg_1190" s="T77">tʼüru-kɨ</ta>
            <ta e="T79" id="Seg_1191" s="T78">qaj-m-näj</ta>
            <ta e="T80" id="Seg_1192" s="T79">asa</ta>
            <ta e="T81" id="Seg_1193" s="T80">meː-enǯɨ-w</ta>
            <ta e="T82" id="Seg_1194" s="T81">täp-ɨ-m</ta>
            <ta e="T83" id="Seg_1195" s="T82">edər-gu</ta>
            <ta e="T84" id="Seg_1196" s="T83">keregeŋ</ta>
            <ta e="T85" id="Seg_1197" s="T84">i</ta>
            <ta e="T86" id="Seg_1198" s="T85">tautʼe-n</ta>
            <ta e="T87" id="Seg_1199" s="T86">edər-gu</ta>
            <ta e="T88" id="Seg_1200" s="T87">keregeŋ</ta>
            <ta e="T89" id="Seg_1201" s="T88">nu</ta>
            <ta e="T90" id="Seg_1202" s="T89">seːp-ɨ-n</ta>
            <ta e="T91" id="Seg_1203" s="T90">nagər-ǯi-gu</ta>
            <ta e="T92" id="Seg_1204" s="T91">man-nan</ta>
            <ta e="T93" id="Seg_1205" s="T92">tettɨ</ta>
            <ta e="T94" id="Seg_1206" s="T93">ne-ka-ne-un</ta>
            <ta e="T95" id="Seg_1207" s="T94">nagur</ta>
            <ta e="T96" id="Seg_1208" s="T95">ne-ka-iː-w</ta>
            <ta e="T97" id="Seg_1209" s="T96">okkɨr</ta>
            <ta e="T98" id="Seg_1210" s="T97">ne-ka-ne-w</ta>
            <ta e="T99" id="Seg_1211" s="T98">man-nan</ta>
            <ta e="T100" id="Seg_1212" s="T99">täp-la</ta>
            <ta e="T101" id="Seg_1213" s="T100">koːci-n</ta>
            <ta e="T102" id="Seg_1214" s="T101">eː-nɨ-tɨn</ta>
            <ta e="T103" id="Seg_1215" s="T102">nanoː</ta>
            <ta e="T104" id="Seg_1216" s="T103">kabi-l-ntɨ-ŋ</ta>
            <ta e="T105" id="Seg_1217" s="T104">qaborɣ-la-j</ta>
            <ta e="T106" id="Seg_1218" s="T105">kabi-l-mbɨdi</ta>
            <ta e="T107" id="Seg_1219" s="T106">qaborɣ-ɨ-m</ta>
            <ta e="T108" id="Seg_1220" s="T107">asa</ta>
            <ta e="T109" id="Seg_1221" s="T108">warɨ-gu</ta>
            <ta e="T110" id="Seg_1222" s="T109">i</ta>
            <ta e="T111" id="Seg_1223" s="T110">soːdʼiga</ta>
            <ta e="T112" id="Seg_1224" s="T111">qaborɣ-ɨ-m</ta>
            <ta e="T113" id="Seg_1225" s="T112">asa</ta>
            <ta e="T114" id="Seg_1226" s="T113">ser-gu</ta>
            <ta e="T115" id="Seg_1227" s="T114">man-nan</ta>
            <ta e="T116" id="Seg_1228" s="T115">taw-dʼel</ta>
            <ta e="T117" id="Seg_1229" s="T116">wesʼ</ta>
            <ta e="T118" id="Seg_1230" s="T117">uːdʼi-gu</ta>
            <ta e="T119" id="Seg_1231" s="T118">qwan-nɨ-tɨn</ta>
            <ta e="T120" id="Seg_1232" s="T119">asa</ta>
            <ta e="T121" id="Seg_1233" s="T120">ondɨt</ta>
            <ta e="T122" id="Seg_1234" s="T121">meː-sodə-mɨ-qɨndɨt</ta>
            <ta e="T123" id="Seg_1235" s="T122">a</ta>
            <ta e="T124" id="Seg_1236" s="T123">wargɨ-lʼ</ta>
            <ta e="T125" id="Seg_1237" s="T124">qum-n</ta>
            <ta e="T126" id="Seg_1238" s="T125">meː-sodə-mɨ-m</ta>
            <ta e="T127" id="Seg_1239" s="T126">meː-gu</ta>
            <ta e="T128" id="Seg_1240" s="T127">nu</ta>
            <ta e="T129" id="Seg_1241" s="T128">kud</ta>
            <ta e="T130" id="Seg_1242" s="T129">na</ta>
            <ta e="T131" id="Seg_1243" s="T130">qum</ta>
            <ta e="T132" id="Seg_1244" s="T131">wesʼ</ta>
            <ta e="T133" id="Seg_1245" s="T132">qum-la-nanto</ta>
            <ta e="T134" id="Seg_1246" s="T133">wargɨ-ŋ</ta>
            <ta e="T135" id="Seg_1247" s="T134">eː-nɨ-n</ta>
            <ta e="T136" id="Seg_1248" s="T135">täp-nä</ta>
            <ta e="T137" id="Seg_1249" s="T136">uːdʼi-tɨn</ta>
            <ta e="T138" id="Seg_1250" s="T137">wesʼ</ta>
            <ta e="T139" id="Seg_1251" s="T138">qum-la</ta>
            <ta e="T140" id="Seg_1252" s="T139">üŋgulǯu-mbɨ-tɨn</ta>
            <ta e="T141" id="Seg_1253" s="T140">nu</ta>
            <ta e="T142" id="Seg_1254" s="T141">i</ta>
            <ta e="T144" id="Seg_1255" s="T143">uːdʼi-tɨn</ta>
            <ta e="T145" id="Seg_1256" s="T144">wesʼ</ta>
            <ta e="T146" id="Seg_1257" s="T145">qum-la</ta>
            <ta e="T147" id="Seg_1258" s="T146">täp-ɨ-nä</ta>
            <ta e="T148" id="Seg_1259" s="T147">nu</ta>
            <ta e="T149" id="Seg_1260" s="T148">seːp-ɨ-n</ta>
            <ta e="T150" id="Seg_1261" s="T149">nagər-ǯi-gu</ta>
            <ta e="T151" id="Seg_1262" s="T150">asa</ta>
            <ta e="T152" id="Seg_1263" s="T151">kɨgɨ-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1264" s="T1">this</ta>
            <ta e="T3" id="Seg_1265" s="T2">old.woman-DIM.[NOM]</ta>
            <ta e="T4" id="Seg_1266" s="T3">what-ACC</ta>
            <ta e="T5" id="Seg_1267" s="T4">you.ALL</ta>
            <ta e="T6" id="Seg_1268" s="T5">say-CO-3SG.O</ta>
            <ta e="T7" id="Seg_1269" s="T6">and</ta>
            <ta e="T8" id="Seg_1270" s="T7">you.SG.NOM</ta>
            <ta e="T9" id="Seg_1271" s="T8">I.GEN</ta>
            <ta e="T10" id="Seg_1272" s="T9">about</ta>
            <ta e="T11" id="Seg_1273" s="T10">all</ta>
            <ta e="T12" id="Seg_1274" s="T11">say-DUR-FUT-2SG.O</ta>
            <ta e="T13" id="Seg_1275" s="T12">I.NOM</ta>
            <ta e="T14" id="Seg_1276" s="T13">live-1SG.S</ta>
            <ta e="T15" id="Seg_1277" s="T14">daughter-1SG-COM</ta>
            <ta e="T16" id="Seg_1278" s="T15">I-ADES</ta>
            <ta e="T17" id="Seg_1279" s="T16">be-CO-3SG.S</ta>
            <ta e="T18" id="Seg_1280" s="T17">son.[NOM]-1SG</ta>
            <ta e="T19" id="Seg_1281" s="T18">(s)he</ta>
            <ta e="T20" id="Seg_1282" s="T19">I-COM</ta>
            <ta e="T21" id="Seg_1283" s="T20">NEG</ta>
            <ta e="T22" id="Seg_1284" s="T21">live-3SG.S</ta>
            <ta e="T23" id="Seg_1285" s="T22">I-ADES</ta>
            <ta e="T24" id="Seg_1286" s="T23">three</ta>
            <ta e="T25" id="Seg_1287" s="T24">daughter-1SG-GEN</ta>
            <ta e="T26" id="Seg_1288" s="T25">daughter-DIM-son.[NOM]</ta>
            <ta e="T27" id="Seg_1289" s="T26">and</ta>
            <ta e="T28" id="Seg_1290" s="T27">one</ta>
            <ta e="T29" id="Seg_1291" s="T28">daughter-DIM-daughter.[NOM]-1SG</ta>
            <ta e="T30" id="Seg_1292" s="T29">I-ADES</ta>
            <ta e="T31" id="Seg_1293" s="T30">be-CO-3SG.S</ta>
            <ta e="T32" id="Seg_1294" s="T31">daughter_in_law.[NOM]-1SG</ta>
            <ta e="T33" id="Seg_1295" s="T32">and</ta>
            <ta e="T34" id="Seg_1296" s="T33">son.[NOM]-1SG</ta>
            <ta e="T35" id="Seg_1297" s="T34">(s)he-DU-ADES</ta>
            <ta e="T36" id="Seg_1298" s="T35">one</ta>
            <ta e="T37" id="Seg_1299" s="T36">child.[NOM]-3DU</ta>
            <ta e="T38" id="Seg_1300" s="T37">more</ta>
            <ta e="T39" id="Seg_1301" s="T38">small</ta>
            <ta e="T40" id="Seg_1302" s="T39">be-CO-3SG.S</ta>
            <ta e="T41" id="Seg_1303" s="T40">(s)he-ALL</ta>
            <ta e="T42" id="Seg_1304" s="T41">three</ta>
            <ta e="T43" id="Seg_1305" s="T42">month.[NOM]</ta>
            <ta e="T44" id="Seg_1306" s="T43">four-ORD</ta>
            <ta e="T45" id="Seg_1307" s="T44">month.[NOM]</ta>
            <ta e="T46" id="Seg_1308" s="T45">begin-PST.NAR-3SG.S</ta>
            <ta e="T47" id="Seg_1309" s="T46">I.NOM</ta>
            <ta e="T48" id="Seg_1310" s="T47">son-1SG-COM</ta>
            <ta e="T49" id="Seg_1311" s="T48">NEG</ta>
            <ta e="T50" id="Seg_1312" s="T49">live-1SG.S</ta>
            <ta e="T51" id="Seg_1313" s="T50">(s)he.[NOM]</ta>
            <ta e="T52" id="Seg_1314" s="T51">wife-OBL.3SG-COM</ta>
            <ta e="T53" id="Seg_1315" s="T52">I.ALL</ta>
            <ta e="T54" id="Seg_1316" s="T53">come-PST.NAR-3SG.S</ta>
            <ta e="T55" id="Seg_1317" s="T54">NEG</ta>
            <ta e="T56" id="Seg_1318" s="T55">big-ADVZ</ta>
            <ta e="T57" id="Seg_1319" s="T56">we-ADES</ta>
            <ta e="T58" id="Seg_1320" s="T57">live-IPFV-INF</ta>
            <ta e="T59" id="Seg_1321" s="T58">then</ta>
            <ta e="T60" id="Seg_1322" s="T59">again</ta>
            <ta e="T61" id="Seg_1323" s="T60">leave-FUT-3DU.S</ta>
            <ta e="T62" id="Seg_1324" s="T61">morning-day.[NOM]</ta>
            <ta e="T63" id="Seg_1325" s="T62">or</ta>
            <ta e="T64" id="Seg_1326" s="T63">other-day.[NOM]</ta>
            <ta e="T65" id="Seg_1327" s="T64">I-ADES-%%</ta>
            <ta e="T66" id="Seg_1328" s="T65">heart.[NOM]-EP-1SG</ta>
            <ta e="T67" id="Seg_1329" s="T66">again</ta>
            <ta e="T68" id="Seg_1330" s="T67">press-FUT-3SG.O</ta>
            <ta e="T69" id="Seg_1331" s="T68">(s)he-DU.[NOM]</ta>
            <ta e="T70" id="Seg_1332" s="T69">set.off-FUT-3DU.S</ta>
            <ta e="T71" id="Seg_1333" s="T70">and</ta>
            <ta e="T72" id="Seg_1334" s="T71">I.NOM</ta>
            <ta e="T73" id="Seg_1335" s="T72">again</ta>
            <ta e="T74" id="Seg_1336" s="T73">stay-DUR-FUT-1SG.S</ta>
            <ta e="T75" id="Seg_1337" s="T74">cry-FUT-1SG.S</ta>
            <ta e="T76" id="Seg_1338" s="T75">cry-IMP.2SG.S</ta>
            <ta e="T77" id="Seg_1339" s="T76">NEG.IMP</ta>
            <ta e="T78" id="Seg_1340" s="T77">cry-IMP.2SG.S</ta>
            <ta e="T79" id="Seg_1341" s="T78">what-ACC-EMPH</ta>
            <ta e="T80" id="Seg_1342" s="T79">NEG</ta>
            <ta e="T81" id="Seg_1343" s="T80">do-FUT-1SG.O</ta>
            <ta e="T82" id="Seg_1344" s="T81">(s)he-EP-ACC</ta>
            <ta e="T83" id="Seg_1345" s="T82">feel.sorry-INF</ta>
            <ta e="T84" id="Seg_1346" s="T83">must</ta>
            <ta e="T85" id="Seg_1347" s="T84">and</ta>
            <ta e="T86" id="Seg_1348" s="T85">here-ADV.LOC</ta>
            <ta e="T87" id="Seg_1349" s="T86">feel.sorry-INF</ta>
            <ta e="T88" id="Seg_1350" s="T87">must</ta>
            <ta e="T89" id="Seg_1351" s="T88">now</ta>
            <ta e="T90" id="Seg_1352" s="T89">enough-EP-ADV.LOC</ta>
            <ta e="T91" id="Seg_1353" s="T90">letter-VBLZ-INF</ta>
            <ta e="T92" id="Seg_1354" s="T91">I-ADES</ta>
            <ta e="T93" id="Seg_1355" s="T92">four</ta>
            <ta e="T94" id="Seg_1356" s="T93">daughter-DIM-daughter.[NOM]-1PL</ta>
            <ta e="T95" id="Seg_1357" s="T94">three</ta>
            <ta e="T96" id="Seg_1358" s="T95">daughter-DIM-son.[NOM]-1SG</ta>
            <ta e="T97" id="Seg_1359" s="T96">one</ta>
            <ta e="T98" id="Seg_1360" s="T97">daughter-DIM-daughter.[NOM]-1SG</ta>
            <ta e="T99" id="Seg_1361" s="T98">I-ADES</ta>
            <ta e="T100" id="Seg_1362" s="T99">(s)he-PL.[NOM]</ta>
            <ta e="T101" id="Seg_1363" s="T100">much-ADV.LOC</ta>
            <ta e="T102" id="Seg_1364" s="T101">be-CO-3PL</ta>
            <ta e="T103" id="Seg_1365" s="T102">that.is.why</ta>
            <ta e="T104" id="Seg_1366" s="T103">rag-VBLZ-IPFV-1SG.S</ta>
            <ta e="T105" id="Seg_1367" s="T104">shirt-PL.[NOM]-1DU</ta>
            <ta e="T106" id="Seg_1368" s="T105">rag-VBLZ-PTCP.PST</ta>
            <ta e="T107" id="Seg_1369" s="T106">clothes-EP-ACC</ta>
            <ta e="T108" id="Seg_1370" s="T107">NEG</ta>
            <ta e="T109" id="Seg_1371" s="T108">wear-INF</ta>
            <ta e="T110" id="Seg_1372" s="T109">and</ta>
            <ta e="T111" id="Seg_1373" s="T110">good</ta>
            <ta e="T112" id="Seg_1374" s="T111">clothes-EP-ACC</ta>
            <ta e="T113" id="Seg_1375" s="T112">NEG</ta>
            <ta e="T114" id="Seg_1376" s="T113">put.on-INF</ta>
            <ta e="T115" id="Seg_1377" s="T114">I-ADES</ta>
            <ta e="T116" id="Seg_1378" s="T115">this-day.[NOM]</ta>
            <ta e="T117" id="Seg_1379" s="T116">all</ta>
            <ta e="T118" id="Seg_1380" s="T117">work-INF</ta>
            <ta e="T119" id="Seg_1381" s="T118">leave-CO-3PL</ta>
            <ta e="T120" id="Seg_1382" s="T119">NEG</ta>
            <ta e="T121" id="Seg_1383" s="T120">own.3PL</ta>
            <ta e="T122" id="Seg_1384" s="T121">do-PTCP.NEC-something-ILL.3PL</ta>
            <ta e="T123" id="Seg_1385" s="T122">and</ta>
            <ta e="T124" id="Seg_1386" s="T123">big-ADJZ</ta>
            <ta e="T125" id="Seg_1387" s="T124">human.being-GEN</ta>
            <ta e="T126" id="Seg_1388" s="T125">do-PTCP.NEC-something-ACC</ta>
            <ta e="T127" id="Seg_1389" s="T126">do-INF</ta>
            <ta e="T128" id="Seg_1390" s="T127">now</ta>
            <ta e="T129" id="Seg_1391" s="T128">who.[NOM]</ta>
            <ta e="T130" id="Seg_1392" s="T129">this</ta>
            <ta e="T131" id="Seg_1393" s="T130">human.being.[NOM]</ta>
            <ta e="T132" id="Seg_1394" s="T131">all</ta>
            <ta e="T133" id="Seg_1395" s="T132">human.being-PL-ABL</ta>
            <ta e="T134" id="Seg_1396" s="T133">elder-ADVZ</ta>
            <ta e="T135" id="Seg_1397" s="T134">be-CO-3SG.S</ta>
            <ta e="T136" id="Seg_1398" s="T135">(s)he-ALL</ta>
            <ta e="T137" id="Seg_1399" s="T136">work-3PL</ta>
            <ta e="T138" id="Seg_1400" s="T137">all</ta>
            <ta e="T139" id="Seg_1401" s="T138">human.being-PL.[NOM]</ta>
            <ta e="T140" id="Seg_1402" s="T139">listen.to-DUR-3PL</ta>
            <ta e="T141" id="Seg_1403" s="T140">now</ta>
            <ta e="T142" id="Seg_1404" s="T141">and</ta>
            <ta e="T144" id="Seg_1405" s="T143">work-3PL</ta>
            <ta e="T145" id="Seg_1406" s="T144">all</ta>
            <ta e="T146" id="Seg_1407" s="T145">human.being-PL.[NOM]</ta>
            <ta e="T147" id="Seg_1408" s="T146">(s)he-EP-ALL</ta>
            <ta e="T148" id="Seg_1409" s="T147">now</ta>
            <ta e="T149" id="Seg_1410" s="T148">enough-EP-ADV.LOC</ta>
            <ta e="T150" id="Seg_1411" s="T149">letter-VBLZ-INF</ta>
            <ta e="T151" id="Seg_1412" s="T150">NEG</ta>
            <ta e="T152" id="Seg_1413" s="T151">want-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1414" s="T1">этот</ta>
            <ta e="T3" id="Seg_1415" s="T2">старуха-DIM.[NOM]</ta>
            <ta e="T4" id="Seg_1416" s="T3">что-ACC</ta>
            <ta e="T5" id="Seg_1417" s="T4">ты.ALL</ta>
            <ta e="T6" id="Seg_1418" s="T5">сказать-CO-3SG.O</ta>
            <ta e="T7" id="Seg_1419" s="T6">а</ta>
            <ta e="T8" id="Seg_1420" s="T7">ты.NOM</ta>
            <ta e="T9" id="Seg_1421" s="T8">я.GEN</ta>
            <ta e="T10" id="Seg_1422" s="T9">про</ta>
            <ta e="T11" id="Seg_1423" s="T10">все</ta>
            <ta e="T12" id="Seg_1424" s="T11">сказать-DUR-FUT-2SG.O</ta>
            <ta e="T13" id="Seg_1425" s="T12">я.NOM</ta>
            <ta e="T14" id="Seg_1426" s="T13">жить-1SG.S</ta>
            <ta e="T15" id="Seg_1427" s="T14">дочь-1SG-COM</ta>
            <ta e="T16" id="Seg_1428" s="T15">я-ADES</ta>
            <ta e="T17" id="Seg_1429" s="T16">быть-CO-3SG.S</ta>
            <ta e="T18" id="Seg_1430" s="T17">сын.[NOM]-1SG</ta>
            <ta e="T19" id="Seg_1431" s="T18">он(а)</ta>
            <ta e="T20" id="Seg_1432" s="T19">я-COM</ta>
            <ta e="T21" id="Seg_1433" s="T20">NEG</ta>
            <ta e="T22" id="Seg_1434" s="T21">жить-3SG.S</ta>
            <ta e="T23" id="Seg_1435" s="T22">я-ADES</ta>
            <ta e="T24" id="Seg_1436" s="T23">три</ta>
            <ta e="T25" id="Seg_1437" s="T24">дочь-1SG-GEN</ta>
            <ta e="T26" id="Seg_1438" s="T25">дочь-DIM-сын.[NOM]</ta>
            <ta e="T27" id="Seg_1439" s="T26">и</ta>
            <ta e="T28" id="Seg_1440" s="T27">один</ta>
            <ta e="T29" id="Seg_1441" s="T28">дочь-DIM-дочь.[NOM]-1SG</ta>
            <ta e="T30" id="Seg_1442" s="T29">я-ADES</ta>
            <ta e="T31" id="Seg_1443" s="T30">быть-CO-3SG.S</ta>
            <ta e="T32" id="Seg_1444" s="T31">сноха.[NOM]-1SG</ta>
            <ta e="T33" id="Seg_1445" s="T32">и</ta>
            <ta e="T34" id="Seg_1446" s="T33">сын.[NOM]-1SG</ta>
            <ta e="T35" id="Seg_1447" s="T34">он(а)-DU-ADES</ta>
            <ta e="T36" id="Seg_1448" s="T35">один</ta>
            <ta e="T37" id="Seg_1449" s="T36">ребёнок.[NOM]-3DU</ta>
            <ta e="T38" id="Seg_1450" s="T37">еще</ta>
            <ta e="T39" id="Seg_1451" s="T38">маленький</ta>
            <ta e="T40" id="Seg_1452" s="T39">быть-CO-3SG.S</ta>
            <ta e="T41" id="Seg_1453" s="T40">он(а)-ALL</ta>
            <ta e="T42" id="Seg_1454" s="T41">три</ta>
            <ta e="T43" id="Seg_1455" s="T42">месяц.[NOM]</ta>
            <ta e="T44" id="Seg_1456" s="T43">четыре-ORD</ta>
            <ta e="T45" id="Seg_1457" s="T44">месяц.[NOM]</ta>
            <ta e="T46" id="Seg_1458" s="T45">начать-PST.NAR-3SG.S</ta>
            <ta e="T47" id="Seg_1459" s="T46">я.NOM</ta>
            <ta e="T48" id="Seg_1460" s="T47">сын-1SG-COM</ta>
            <ta e="T49" id="Seg_1461" s="T48">NEG</ta>
            <ta e="T50" id="Seg_1462" s="T49">жить-1SG.S</ta>
            <ta e="T51" id="Seg_1463" s="T50">он(а).[NOM]</ta>
            <ta e="T52" id="Seg_1464" s="T51">жена-OBL.3SG-COM</ta>
            <ta e="T53" id="Seg_1465" s="T52">я.ALL</ta>
            <ta e="T54" id="Seg_1466" s="T53">приехать-PST.NAR-3SG.S</ta>
            <ta e="T55" id="Seg_1467" s="T54">NEG</ta>
            <ta e="T56" id="Seg_1468" s="T55">большой-ADVZ</ta>
            <ta e="T57" id="Seg_1469" s="T56">мы-ADES</ta>
            <ta e="T58" id="Seg_1470" s="T57">жить-IPFV-INF</ta>
            <ta e="T59" id="Seg_1471" s="T58">потом</ta>
            <ta e="T60" id="Seg_1472" s="T59">опять</ta>
            <ta e="T61" id="Seg_1473" s="T60">отправиться-FUT-3DU.S</ta>
            <ta e="T62" id="Seg_1474" s="T61">утро-день.[NOM]</ta>
            <ta e="T63" id="Seg_1475" s="T62">али</ta>
            <ta e="T64" id="Seg_1476" s="T63">другой-день.[NOM]</ta>
            <ta e="T65" id="Seg_1477" s="T64">я-ADES-%%</ta>
            <ta e="T66" id="Seg_1478" s="T65">сердце.[NOM]-EP-1SG</ta>
            <ta e="T67" id="Seg_1479" s="T66">опять</ta>
            <ta e="T68" id="Seg_1480" s="T67">придавить-FUT-3SG.O</ta>
            <ta e="T69" id="Seg_1481" s="T68">он(а)-DU.[NOM]</ta>
            <ta e="T70" id="Seg_1482" s="T69">отправиться-FUT-3DU.S</ta>
            <ta e="T71" id="Seg_1483" s="T70">а</ta>
            <ta e="T72" id="Seg_1484" s="T71">я.NOM</ta>
            <ta e="T73" id="Seg_1485" s="T72">опять</ta>
            <ta e="T74" id="Seg_1486" s="T73">остаться-DUR-FUT-1SG.S</ta>
            <ta e="T75" id="Seg_1487" s="T74">плакать-FUT-1SG.S</ta>
            <ta e="T76" id="Seg_1488" s="T75">плакать-IMP.2SG.S</ta>
            <ta e="T77" id="Seg_1489" s="T76">NEG.IMP</ta>
            <ta e="T78" id="Seg_1490" s="T77">плакать-IMP.2SG.S</ta>
            <ta e="T79" id="Seg_1491" s="T78">что-ACC-EMPH</ta>
            <ta e="T80" id="Seg_1492" s="T79">NEG</ta>
            <ta e="T81" id="Seg_1493" s="T80">сделать-FUT-1SG.O</ta>
            <ta e="T82" id="Seg_1494" s="T81">он(а)-EP-ACC</ta>
            <ta e="T83" id="Seg_1495" s="T82">пожалеть-INF</ta>
            <ta e="T84" id="Seg_1496" s="T83">надо</ta>
            <ta e="T85" id="Seg_1497" s="T84">и</ta>
            <ta e="T86" id="Seg_1498" s="T85">сюда-ADV.LOC</ta>
            <ta e="T87" id="Seg_1499" s="T86">пожалеть-INF</ta>
            <ta e="T88" id="Seg_1500" s="T87">надо</ta>
            <ta e="T89" id="Seg_1501" s="T88">ну</ta>
            <ta e="T90" id="Seg_1502" s="T89">достаточно-EP-ADV.LOC</ta>
            <ta e="T91" id="Seg_1503" s="T90">письмо-VBLZ-INF</ta>
            <ta e="T92" id="Seg_1504" s="T91">я-ADES</ta>
            <ta e="T93" id="Seg_1505" s="T92">четыре</ta>
            <ta e="T94" id="Seg_1506" s="T93">дочь-DIM-дочь.[NOM]-1PL</ta>
            <ta e="T95" id="Seg_1507" s="T94">три</ta>
            <ta e="T96" id="Seg_1508" s="T95">дочь-DIM-сын.[NOM]-1SG</ta>
            <ta e="T97" id="Seg_1509" s="T96">один</ta>
            <ta e="T98" id="Seg_1510" s="T97">дочь-DIM-дочь.[NOM]-1SG</ta>
            <ta e="T99" id="Seg_1511" s="T98">я-ADES</ta>
            <ta e="T100" id="Seg_1512" s="T99">он(а)-PL.[NOM]</ta>
            <ta e="T101" id="Seg_1513" s="T100">много-ADV.LOC</ta>
            <ta e="T102" id="Seg_1514" s="T101">быть-CO-3PL</ta>
            <ta e="T103" id="Seg_1515" s="T102">поэтому</ta>
            <ta e="T104" id="Seg_1516" s="T103">тряпка-VBLZ-IPFV-1SG.S</ta>
            <ta e="T105" id="Seg_1517" s="T104">рубашка-PL.[NOM]-1DU</ta>
            <ta e="T106" id="Seg_1518" s="T105">тряпка-VBLZ-PTCP.PST</ta>
            <ta e="T107" id="Seg_1519" s="T106">одежда-EP-ACC</ta>
            <ta e="T108" id="Seg_1520" s="T107">NEG</ta>
            <ta e="T109" id="Seg_1521" s="T108">носить-INF</ta>
            <ta e="T110" id="Seg_1522" s="T109">и</ta>
            <ta e="T111" id="Seg_1523" s="T110">хороший</ta>
            <ta e="T112" id="Seg_1524" s="T111">одежда-EP-ACC</ta>
            <ta e="T113" id="Seg_1525" s="T112">NEG</ta>
            <ta e="T114" id="Seg_1526" s="T113">надеть-INF</ta>
            <ta e="T115" id="Seg_1527" s="T114">я-ADES</ta>
            <ta e="T116" id="Seg_1528" s="T115">этот-день.[NOM]</ta>
            <ta e="T117" id="Seg_1529" s="T116">все</ta>
            <ta e="T118" id="Seg_1530" s="T117">работать-INF</ta>
            <ta e="T119" id="Seg_1531" s="T118">отправиться-CO-3PL</ta>
            <ta e="T120" id="Seg_1532" s="T119">NEG</ta>
            <ta e="T121" id="Seg_1533" s="T120">свой.3PL</ta>
            <ta e="T122" id="Seg_1534" s="T121">сделать-PTCP.NEC-нечто-ILL.3PL</ta>
            <ta e="T123" id="Seg_1535" s="T122">а</ta>
            <ta e="T124" id="Seg_1536" s="T123">большой-ADJZ</ta>
            <ta e="T125" id="Seg_1537" s="T124">человек-GEN</ta>
            <ta e="T126" id="Seg_1538" s="T125">сделать-PTCP.NEC-нечто-ACC</ta>
            <ta e="T127" id="Seg_1539" s="T126">сделать-INF</ta>
            <ta e="T128" id="Seg_1540" s="T127">ну</ta>
            <ta e="T129" id="Seg_1541" s="T128">кто.[NOM]</ta>
            <ta e="T130" id="Seg_1542" s="T129">этот</ta>
            <ta e="T131" id="Seg_1543" s="T130">человек.[NOM]</ta>
            <ta e="T132" id="Seg_1544" s="T131">все</ta>
            <ta e="T133" id="Seg_1545" s="T132">человек-PL-ABL</ta>
            <ta e="T134" id="Seg_1546" s="T133">старший-ADVZ</ta>
            <ta e="T135" id="Seg_1547" s="T134">быть-CO-3SG.S</ta>
            <ta e="T136" id="Seg_1548" s="T135">он(а)-ALL</ta>
            <ta e="T137" id="Seg_1549" s="T136">работать-3PL</ta>
            <ta e="T138" id="Seg_1550" s="T137">все</ta>
            <ta e="T139" id="Seg_1551" s="T138">человек-PL.[NOM]</ta>
            <ta e="T140" id="Seg_1552" s="T139">послушаться-DUR-3PL</ta>
            <ta e="T141" id="Seg_1553" s="T140">ну</ta>
            <ta e="T142" id="Seg_1554" s="T141">и</ta>
            <ta e="T144" id="Seg_1555" s="T143">работать-3PL</ta>
            <ta e="T145" id="Seg_1556" s="T144">все</ta>
            <ta e="T146" id="Seg_1557" s="T145">человек-PL.[NOM]</ta>
            <ta e="T147" id="Seg_1558" s="T146">он(а)-EP-ALL</ta>
            <ta e="T148" id="Seg_1559" s="T147">ну</ta>
            <ta e="T149" id="Seg_1560" s="T148">достаточно-EP-ADV.LOC</ta>
            <ta e="T150" id="Seg_1561" s="T149">письмо-VBLZ-INF</ta>
            <ta e="T151" id="Seg_1562" s="T150">NEG</ta>
            <ta e="T152" id="Seg_1563" s="T151">хотеть-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1564" s="T1">dem</ta>
            <ta e="T3" id="Seg_1565" s="T2">n-n&gt;n.[n:case]</ta>
            <ta e="T4" id="Seg_1566" s="T3">interrog-n:case</ta>
            <ta e="T5" id="Seg_1567" s="T4">pers</ta>
            <ta e="T6" id="Seg_1568" s="T5">v-v:ins-v:pn</ta>
            <ta e="T7" id="Seg_1569" s="T6">conj</ta>
            <ta e="T8" id="Seg_1570" s="T7">pers</ta>
            <ta e="T9" id="Seg_1571" s="T8">pers</ta>
            <ta e="T10" id="Seg_1572" s="T9">pp</ta>
            <ta e="T11" id="Seg_1573" s="T10">quant</ta>
            <ta e="T12" id="Seg_1574" s="T11">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_1575" s="T12">pers</ta>
            <ta e="T14" id="Seg_1576" s="T13">v-v:pn</ta>
            <ta e="T15" id="Seg_1577" s="T14">n-n:poss-n:case</ta>
            <ta e="T16" id="Seg_1578" s="T15">pers-n:case</ta>
            <ta e="T17" id="Seg_1579" s="T16">v-v:ins-v:pn</ta>
            <ta e="T18" id="Seg_1580" s="T17">n.[n:case]-n:poss</ta>
            <ta e="T19" id="Seg_1581" s="T18">pers</ta>
            <ta e="T20" id="Seg_1582" s="T19">pers-n:case</ta>
            <ta e="T21" id="Seg_1583" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_1584" s="T21">v-v:pn</ta>
            <ta e="T23" id="Seg_1585" s="T22">pers-n:case</ta>
            <ta e="T24" id="Seg_1586" s="T23">num</ta>
            <ta e="T25" id="Seg_1587" s="T24">n-n:poss-n:case</ta>
            <ta e="T26" id="Seg_1588" s="T25">n-n&gt;n-n.[n:case]</ta>
            <ta e="T27" id="Seg_1589" s="T26">conj</ta>
            <ta e="T28" id="Seg_1590" s="T27">num</ta>
            <ta e="T29" id="Seg_1591" s="T28">n-n&gt;n-n.[n:case]-n:poss</ta>
            <ta e="T30" id="Seg_1592" s="T29">pers-n:case</ta>
            <ta e="T31" id="Seg_1593" s="T30">v-v:ins-v:pn</ta>
            <ta e="T32" id="Seg_1594" s="T31">n.[n:case]-n:poss</ta>
            <ta e="T33" id="Seg_1595" s="T32">conj</ta>
            <ta e="T34" id="Seg_1596" s="T33">n.[n:case]-n:poss</ta>
            <ta e="T35" id="Seg_1597" s="T34">pers-n:num-n:case</ta>
            <ta e="T36" id="Seg_1598" s="T35">num</ta>
            <ta e="T37" id="Seg_1599" s="T36">n.[n:case]-n:poss</ta>
            <ta e="T38" id="Seg_1600" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_1601" s="T38">adj</ta>
            <ta e="T40" id="Seg_1602" s="T39">v-v:ins-v:pn</ta>
            <ta e="T41" id="Seg_1603" s="T40">pers-n:case</ta>
            <ta e="T42" id="Seg_1604" s="T41">num</ta>
            <ta e="T43" id="Seg_1605" s="T42">n.[n:case]</ta>
            <ta e="T44" id="Seg_1606" s="T43">num-num&gt;adj</ta>
            <ta e="T45" id="Seg_1607" s="T44">n.[n:case]</ta>
            <ta e="T46" id="Seg_1608" s="T45">v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_1609" s="T46">pers</ta>
            <ta e="T48" id="Seg_1610" s="T47">n-n:poss-n:case</ta>
            <ta e="T49" id="Seg_1611" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_1612" s="T49">v-v:pn</ta>
            <ta e="T51" id="Seg_1613" s="T50">pers.[n:case]</ta>
            <ta e="T52" id="Seg_1614" s="T51">n-n:obl.poss-n:case</ta>
            <ta e="T53" id="Seg_1615" s="T52">pers</ta>
            <ta e="T54" id="Seg_1616" s="T53">v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_1617" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_1618" s="T55">adj-adj&gt;adv</ta>
            <ta e="T57" id="Seg_1619" s="T56">pers-n:case</ta>
            <ta e="T58" id="Seg_1620" s="T57">v-v&gt;v-v:inf</ta>
            <ta e="T59" id="Seg_1621" s="T58">adv</ta>
            <ta e="T60" id="Seg_1622" s="T59">adv</ta>
            <ta e="T61" id="Seg_1623" s="T60">v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_1624" s="T61">n-n.[n:case]</ta>
            <ta e="T63" id="Seg_1625" s="T62">conj</ta>
            <ta e="T64" id="Seg_1626" s="T63">adj-n.[n:case]</ta>
            <ta e="T65" id="Seg_1627" s="T64">pers-n:case-%%</ta>
            <ta e="T66" id="Seg_1628" s="T65">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T67" id="Seg_1629" s="T66">adv</ta>
            <ta e="T68" id="Seg_1630" s="T67">v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_1631" s="T68">pers-n:num.[n:case]</ta>
            <ta e="T70" id="Seg_1632" s="T69">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_1633" s="T70">conj</ta>
            <ta e="T72" id="Seg_1634" s="T71">pers</ta>
            <ta e="T73" id="Seg_1635" s="T72">adv</ta>
            <ta e="T74" id="Seg_1636" s="T73">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_1637" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_1638" s="T75">v-v:mood.pn</ta>
            <ta e="T77" id="Seg_1639" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_1640" s="T77">v-v:mood.pn</ta>
            <ta e="T79" id="Seg_1641" s="T78">interrog-n:case-clit</ta>
            <ta e="T80" id="Seg_1642" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_1643" s="T80">v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_1644" s="T81">pers-n:ins-n:case</ta>
            <ta e="T83" id="Seg_1645" s="T82">v-v:inf</ta>
            <ta e="T84" id="Seg_1646" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_1647" s="T84">conj</ta>
            <ta e="T86" id="Seg_1648" s="T85">adv-adv:case</ta>
            <ta e="T87" id="Seg_1649" s="T86">v-v:inf</ta>
            <ta e="T88" id="Seg_1650" s="T87">ptcl</ta>
            <ta e="T89" id="Seg_1651" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_1652" s="T89">adv-n:ins-adv:case</ta>
            <ta e="T91" id="Seg_1653" s="T90">n-n&gt;v-v:inf</ta>
            <ta e="T92" id="Seg_1654" s="T91">pers-n:case</ta>
            <ta e="T93" id="Seg_1655" s="T92">num</ta>
            <ta e="T94" id="Seg_1656" s="T93">n-n&gt;n-n.[n:case]-n:poss</ta>
            <ta e="T95" id="Seg_1657" s="T94">num</ta>
            <ta e="T96" id="Seg_1658" s="T95">n-n&gt;n-n.[n:case]-n:poss</ta>
            <ta e="T97" id="Seg_1659" s="T96">num</ta>
            <ta e="T98" id="Seg_1660" s="T97">n-n&gt;n-n.[n:case]-n:poss</ta>
            <ta e="T99" id="Seg_1661" s="T98">pers-n:case</ta>
            <ta e="T100" id="Seg_1662" s="T99">pers-n:num.[n:case]</ta>
            <ta e="T101" id="Seg_1663" s="T100">quant-adv:case</ta>
            <ta e="T102" id="Seg_1664" s="T101">v-v:ins-v:pn</ta>
            <ta e="T103" id="Seg_1665" s="T102">conj</ta>
            <ta e="T104" id="Seg_1666" s="T103">n-n&gt;v-v&gt;v-v:pn</ta>
            <ta e="T105" id="Seg_1667" s="T104">n-n:num.[n:case]-n:poss</ta>
            <ta e="T106" id="Seg_1668" s="T105">n-n&gt;v-v&gt;ptcp</ta>
            <ta e="T107" id="Seg_1669" s="T106">n-n:ins-n:case</ta>
            <ta e="T108" id="Seg_1670" s="T107">ptcl</ta>
            <ta e="T109" id="Seg_1671" s="T108">v-v:inf</ta>
            <ta e="T110" id="Seg_1672" s="T109">conj</ta>
            <ta e="T111" id="Seg_1673" s="T110">adj</ta>
            <ta e="T112" id="Seg_1674" s="T111">n-n:ins-n:case</ta>
            <ta e="T113" id="Seg_1675" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_1676" s="T113">v-v:inf</ta>
            <ta e="T115" id="Seg_1677" s="T114">pers-n:case</ta>
            <ta e="T116" id="Seg_1678" s="T115">dem-n.[n:case]</ta>
            <ta e="T117" id="Seg_1679" s="T116">quant</ta>
            <ta e="T118" id="Seg_1680" s="T117">v-v:inf</ta>
            <ta e="T119" id="Seg_1681" s="T118">v-v:ins-v:pn</ta>
            <ta e="T120" id="Seg_1682" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_1683" s="T120">emphpro</ta>
            <ta e="T122" id="Seg_1684" s="T121">v-v&gt;ptcp-n-n:case.poss</ta>
            <ta e="T123" id="Seg_1685" s="T122">conj</ta>
            <ta e="T124" id="Seg_1686" s="T123">adj-n&gt;adj</ta>
            <ta e="T125" id="Seg_1687" s="T124">n-n:case</ta>
            <ta e="T126" id="Seg_1688" s="T125">v-v&gt;ptcp-n-n:case</ta>
            <ta e="T127" id="Seg_1689" s="T126">v-v:inf</ta>
            <ta e="T128" id="Seg_1690" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_1691" s="T128">interrog.[n:case]</ta>
            <ta e="T130" id="Seg_1692" s="T129">dem</ta>
            <ta e="T131" id="Seg_1693" s="T130">n.[n:case]</ta>
            <ta e="T132" id="Seg_1694" s="T131">quant</ta>
            <ta e="T133" id="Seg_1695" s="T132">n-n:num-n:case</ta>
            <ta e="T134" id="Seg_1696" s="T133">adj-adj&gt;adv</ta>
            <ta e="T135" id="Seg_1697" s="T134">v-v:ins-v:pn</ta>
            <ta e="T136" id="Seg_1698" s="T135">pers-n:case</ta>
            <ta e="T137" id="Seg_1699" s="T136">v-v:pn</ta>
            <ta e="T138" id="Seg_1700" s="T137">quant</ta>
            <ta e="T139" id="Seg_1701" s="T138">n-n:num.[n:case]</ta>
            <ta e="T140" id="Seg_1702" s="T139">v-v&gt;v-v:pn</ta>
            <ta e="T141" id="Seg_1703" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_1704" s="T141">conj</ta>
            <ta e="T144" id="Seg_1705" s="T143">v-v:pn</ta>
            <ta e="T145" id="Seg_1706" s="T144">quant</ta>
            <ta e="T146" id="Seg_1707" s="T145">n-n:num.[n:case]</ta>
            <ta e="T147" id="Seg_1708" s="T146">pers-n:ins-n:case</ta>
            <ta e="T148" id="Seg_1709" s="T147">ptcl</ta>
            <ta e="T149" id="Seg_1710" s="T148">adv-n:ins-adv:case</ta>
            <ta e="T150" id="Seg_1711" s="T149">n-n&gt;v-v:inf</ta>
            <ta e="T151" id="Seg_1712" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_1713" s="T151">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1714" s="T1">dem</ta>
            <ta e="T3" id="Seg_1715" s="T2">n</ta>
            <ta e="T4" id="Seg_1716" s="T3">interrog</ta>
            <ta e="T5" id="Seg_1717" s="T4">pers</ta>
            <ta e="T6" id="Seg_1718" s="T5">v</ta>
            <ta e="T7" id="Seg_1719" s="T6">conj</ta>
            <ta e="T8" id="Seg_1720" s="T7">pers</ta>
            <ta e="T9" id="Seg_1721" s="T8">pers</ta>
            <ta e="T10" id="Seg_1722" s="T9">pp</ta>
            <ta e="T11" id="Seg_1723" s="T10">quant</ta>
            <ta e="T12" id="Seg_1724" s="T11">v</ta>
            <ta e="T13" id="Seg_1725" s="T12">pers</ta>
            <ta e="T14" id="Seg_1726" s="T13">v</ta>
            <ta e="T15" id="Seg_1727" s="T14">n</ta>
            <ta e="T16" id="Seg_1728" s="T15">pers</ta>
            <ta e="T17" id="Seg_1729" s="T16">v</ta>
            <ta e="T18" id="Seg_1730" s="T17">n</ta>
            <ta e="T19" id="Seg_1731" s="T18">pers</ta>
            <ta e="T20" id="Seg_1732" s="T19">pers</ta>
            <ta e="T21" id="Seg_1733" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_1734" s="T21">v</ta>
            <ta e="T23" id="Seg_1735" s="T22">pers</ta>
            <ta e="T24" id="Seg_1736" s="T23">num</ta>
            <ta e="T25" id="Seg_1737" s="T24">n</ta>
            <ta e="T26" id="Seg_1738" s="T25">n</ta>
            <ta e="T27" id="Seg_1739" s="T26">conj</ta>
            <ta e="T28" id="Seg_1740" s="T27">num</ta>
            <ta e="T29" id="Seg_1741" s="T28">n</ta>
            <ta e="T30" id="Seg_1742" s="T29">pers</ta>
            <ta e="T31" id="Seg_1743" s="T30">v</ta>
            <ta e="T32" id="Seg_1744" s="T31">n</ta>
            <ta e="T33" id="Seg_1745" s="T32">conj</ta>
            <ta e="T34" id="Seg_1746" s="T33">n</ta>
            <ta e="T35" id="Seg_1747" s="T34">pers</ta>
            <ta e="T36" id="Seg_1748" s="T35">num</ta>
            <ta e="T37" id="Seg_1749" s="T36">n</ta>
            <ta e="T38" id="Seg_1750" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_1751" s="T38">adj</ta>
            <ta e="T40" id="Seg_1752" s="T39">v</ta>
            <ta e="T41" id="Seg_1753" s="T40">pers</ta>
            <ta e="T42" id="Seg_1754" s="T41">num</ta>
            <ta e="T43" id="Seg_1755" s="T42">n</ta>
            <ta e="T44" id="Seg_1756" s="T43">adj</ta>
            <ta e="T45" id="Seg_1757" s="T44">n</ta>
            <ta e="T46" id="Seg_1758" s="T45">v</ta>
            <ta e="T47" id="Seg_1759" s="T46">pers</ta>
            <ta e="T48" id="Seg_1760" s="T47">n</ta>
            <ta e="T49" id="Seg_1761" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_1762" s="T49">v</ta>
            <ta e="T51" id="Seg_1763" s="T50">pers</ta>
            <ta e="T52" id="Seg_1764" s="T51">n</ta>
            <ta e="T53" id="Seg_1765" s="T52">pers</ta>
            <ta e="T54" id="Seg_1766" s="T53">v</ta>
            <ta e="T55" id="Seg_1767" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_1768" s="T55">adv</ta>
            <ta e="T57" id="Seg_1769" s="T56">pers</ta>
            <ta e="T58" id="Seg_1770" s="T57">v</ta>
            <ta e="T59" id="Seg_1771" s="T58">adv</ta>
            <ta e="T60" id="Seg_1772" s="T59">conj</ta>
            <ta e="T61" id="Seg_1773" s="T60">v</ta>
            <ta e="T62" id="Seg_1774" s="T61">n</ta>
            <ta e="T63" id="Seg_1775" s="T62">conj</ta>
            <ta e="T64" id="Seg_1776" s="T63">adj</ta>
            <ta e="T65" id="Seg_1777" s="T64">pers</ta>
            <ta e="T66" id="Seg_1778" s="T65">n</ta>
            <ta e="T67" id="Seg_1779" s="T66">adv</ta>
            <ta e="T68" id="Seg_1780" s="T67">v</ta>
            <ta e="T69" id="Seg_1781" s="T68">pers</ta>
            <ta e="T70" id="Seg_1782" s="T69">v</ta>
            <ta e="T71" id="Seg_1783" s="T70">conj</ta>
            <ta e="T72" id="Seg_1784" s="T71">pers</ta>
            <ta e="T73" id="Seg_1785" s="T72">adv</ta>
            <ta e="T74" id="Seg_1786" s="T73">v</ta>
            <ta e="T75" id="Seg_1787" s="T74">v</ta>
            <ta e="T76" id="Seg_1788" s="T75">v</ta>
            <ta e="T77" id="Seg_1789" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_1790" s="T77">v</ta>
            <ta e="T79" id="Seg_1791" s="T78">pro</ta>
            <ta e="T80" id="Seg_1792" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_1793" s="T80">v</ta>
            <ta e="T82" id="Seg_1794" s="T81">pers</ta>
            <ta e="T83" id="Seg_1795" s="T82">v</ta>
            <ta e="T84" id="Seg_1796" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_1797" s="T84">conj</ta>
            <ta e="T86" id="Seg_1798" s="T85">adv</ta>
            <ta e="T87" id="Seg_1799" s="T86">v</ta>
            <ta e="T88" id="Seg_1800" s="T87">ptcl</ta>
            <ta e="T89" id="Seg_1801" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_1802" s="T89">adv</ta>
            <ta e="T91" id="Seg_1803" s="T90">v</ta>
            <ta e="T92" id="Seg_1804" s="T91">pers</ta>
            <ta e="T93" id="Seg_1805" s="T92">num</ta>
            <ta e="T94" id="Seg_1806" s="T93">n</ta>
            <ta e="T95" id="Seg_1807" s="T94">num</ta>
            <ta e="T96" id="Seg_1808" s="T95">n</ta>
            <ta e="T97" id="Seg_1809" s="T96">num</ta>
            <ta e="T98" id="Seg_1810" s="T97">n</ta>
            <ta e="T99" id="Seg_1811" s="T98">pers</ta>
            <ta e="T100" id="Seg_1812" s="T99">pro</ta>
            <ta e="T101" id="Seg_1813" s="T100">quant</ta>
            <ta e="T102" id="Seg_1814" s="T101">v</ta>
            <ta e="T103" id="Seg_1815" s="T102">conj</ta>
            <ta e="T104" id="Seg_1816" s="T103">v</ta>
            <ta e="T105" id="Seg_1817" s="T104">n</ta>
            <ta e="T106" id="Seg_1818" s="T105">ptcp</ta>
            <ta e="T107" id="Seg_1819" s="T106">n</ta>
            <ta e="T108" id="Seg_1820" s="T107">ptcl</ta>
            <ta e="T109" id="Seg_1821" s="T108">v</ta>
            <ta e="T110" id="Seg_1822" s="T109">conj</ta>
            <ta e="T111" id="Seg_1823" s="T110">adj</ta>
            <ta e="T112" id="Seg_1824" s="T111">n</ta>
            <ta e="T113" id="Seg_1825" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_1826" s="T113">v</ta>
            <ta e="T115" id="Seg_1827" s="T114">pers</ta>
            <ta e="T116" id="Seg_1828" s="T115">n</ta>
            <ta e="T117" id="Seg_1829" s="T116">quant</ta>
            <ta e="T118" id="Seg_1830" s="T117">v</ta>
            <ta e="T119" id="Seg_1831" s="T118">v</ta>
            <ta e="T120" id="Seg_1832" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_1833" s="T120">emphpro</ta>
            <ta e="T122" id="Seg_1834" s="T121">n</ta>
            <ta e="T123" id="Seg_1835" s="T122">conj</ta>
            <ta e="T124" id="Seg_1836" s="T123">adj</ta>
            <ta e="T125" id="Seg_1837" s="T124">n</ta>
            <ta e="T126" id="Seg_1838" s="T125">n</ta>
            <ta e="T127" id="Seg_1839" s="T126">v</ta>
            <ta e="T128" id="Seg_1840" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_1841" s="T128">interrog</ta>
            <ta e="T130" id="Seg_1842" s="T129">dem</ta>
            <ta e="T131" id="Seg_1843" s="T130">n</ta>
            <ta e="T132" id="Seg_1844" s="T131">quant</ta>
            <ta e="T133" id="Seg_1845" s="T132">n</ta>
            <ta e="T134" id="Seg_1846" s="T133">adv</ta>
            <ta e="T135" id="Seg_1847" s="T134">v</ta>
            <ta e="T136" id="Seg_1848" s="T135">pers</ta>
            <ta e="T137" id="Seg_1849" s="T136">v</ta>
            <ta e="T138" id="Seg_1850" s="T137">quant</ta>
            <ta e="T139" id="Seg_1851" s="T138">n</ta>
            <ta e="T140" id="Seg_1852" s="T139">v</ta>
            <ta e="T141" id="Seg_1853" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_1854" s="T141">conj</ta>
            <ta e="T144" id="Seg_1855" s="T143">v</ta>
            <ta e="T145" id="Seg_1856" s="T144">quant</ta>
            <ta e="T146" id="Seg_1857" s="T145">n</ta>
            <ta e="T147" id="Seg_1858" s="T146">pers</ta>
            <ta e="T148" id="Seg_1859" s="T147">ptcl</ta>
            <ta e="T149" id="Seg_1860" s="T148">adv</ta>
            <ta e="T150" id="Seg_1861" s="T149">v</ta>
            <ta e="T151" id="Seg_1862" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_1863" s="T151">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T7" id="Seg_1864" s="T6">RUS:gram</ta>
            <ta e="T11" id="Seg_1865" s="T10">RUS:core</ta>
            <ta e="T27" id="Seg_1866" s="T26">RUS:gram</ta>
            <ta e="T33" id="Seg_1867" s="T32">RUS:gram</ta>
            <ta e="T38" id="Seg_1868" s="T37">RUS:disc</ta>
            <ta e="T63" id="Seg_1869" s="T62">RUS:gram</ta>
            <ta e="T71" id="Seg_1870" s="T70">RUS:gram</ta>
            <ta e="T84" id="Seg_1871" s="T83">TURK:gram</ta>
            <ta e="T85" id="Seg_1872" s="T84">RUS:gram</ta>
            <ta e="T88" id="Seg_1873" s="T87">TURK:gram</ta>
            <ta e="T89" id="Seg_1874" s="T88">RUS:disc</ta>
            <ta e="T110" id="Seg_1875" s="T109">RUS:gram</ta>
            <ta e="T117" id="Seg_1876" s="T116">RUS:core</ta>
            <ta e="T123" id="Seg_1877" s="T122">RUS:gram</ta>
            <ta e="T128" id="Seg_1878" s="T127">RUS:disc</ta>
            <ta e="T132" id="Seg_1879" s="T131">RUS:core</ta>
            <ta e="T138" id="Seg_1880" s="T137">RUS:core</ta>
            <ta e="T141" id="Seg_1881" s="T140">RUS:disc</ta>
            <ta e="T142" id="Seg_1882" s="T141">RUS:gram</ta>
            <ta e="T145" id="Seg_1883" s="T144">RUS:core</ta>
            <ta e="T148" id="Seg_1884" s="T147">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_1885" s="T1">What did this woman tell to you?</ta>
            <ta e="T12" id="Seg_1886" s="T6">Will you tell everything about me?</ta>
            <ta e="T15" id="Seg_1887" s="T12">I live with my daughter.</ta>
            <ta e="T18" id="Seg_1888" s="T15">I have a son.</ta>
            <ta e="T22" id="Seg_1889" s="T18">He doesn't live with me.</ta>
            <ta e="T29" id="Seg_1890" s="T22">I have three grandsons from my daughter and one granddaughter.</ta>
            <ta e="T34" id="Seg_1891" s="T29">I have a daughter-in-law and a son.</ta>
            <ta e="T37" id="Seg_1892" s="T34">They have one child.</ta>
            <ta e="T40" id="Seg_1893" s="T37">It's very small.</ta>
            <ta e="T46" id="Seg_1894" s="T40">It's three months old, the fourth month has begun.</ta>
            <ta e="T50" id="Seg_1895" s="T46">I don't live with my son.</ta>
            <ta e="T58" id="Seg_1896" s="T50">He came to me with his wife to live with us for a while.</ta>
            <ta e="T64" id="Seg_1897" s="T58">Then they'll leave, tomorrow or the day after tomorrow.</ta>
            <ta e="T68" id="Seg_1898" s="T64">My heart will be pressed again.</ta>
            <ta e="T74" id="Seg_1899" s="T68">They'll leave and I'll stay again.</ta>
            <ta e="T75" id="Seg_1900" s="T74">I'll cry.</ta>
            <ta e="T81" id="Seg_1901" s="T75">Cry or not, you can't do anything.</ta>
            <ta e="T88" id="Seg_1902" s="T81">I should feel sorry for him, and I should feel sorry here.</ta>
            <ta e="T91" id="Seg_1903" s="T88">Well, enough writing.</ta>
            <ta e="T98" id="Seg_1904" s="T91">I've four grandchildren: three grandsons and one granddaughter.</ta>
            <ta e="T102" id="Seg_1905" s="T98">I have many of them.</ta>
            <ta e="T105" id="Seg_1906" s="T102">That's why I mend shirts.</ta>
            <ta e="T114" id="Seg_1907" s="T105">If you don't wear mended shirts, you are not to put on a new one.</ta>
            <ta e="T127" id="Seg_1908" s="T114">Today mine have left to work, not to their own job, but to do the job for the chief (=to work in sovkhoz).</ta>
            <ta e="T135" id="Seg_1909" s="T127">Well, the one who is superiour over everyone.</ta>
            <ta e="T140" id="Seg_1910" s="T135">All the people work for him and obey him.</ta>
            <ta e="T147" id="Seg_1911" s="T140">So all the people work for him.</ta>
            <ta e="T152" id="Seg_1912" s="T147">Well, it's enough writing, I don't want [any more].</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_1913" s="T1">Was hat dir diese Frau erzählt?</ta>
            <ta e="T12" id="Seg_1914" s="T6">Wirst du alles über mich erzählen?</ta>
            <ta e="T15" id="Seg_1915" s="T12">Ich lebe mit meiner Tochter.</ta>
            <ta e="T18" id="Seg_1916" s="T15">Ich habe einen Sohn.</ta>
            <ta e="T22" id="Seg_1917" s="T18">Er lebt nicht mit mir.</ta>
            <ta e="T29" id="Seg_1918" s="T22">Ich habe drei Enkel von meiner Tochter und eine Enkelin.</ta>
            <ta e="T34" id="Seg_1919" s="T29">Ich habe eine Schwiegertochter und einen Sohn.</ta>
            <ta e="T37" id="Seg_1920" s="T34">Sie haben ein Kind.</ta>
            <ta e="T40" id="Seg_1921" s="T37">Es ist sehr klein.</ta>
            <ta e="T46" id="Seg_1922" s="T40">Es ist drei Monate alt, der vierte Monat hat begonnen.</ta>
            <ta e="T50" id="Seg_1923" s="T46">Ich lebe nicht mit meinem Sohn zusammen.</ta>
            <ta e="T58" id="Seg_1924" s="T50">Er kam zu mir mit seiner Frau, um einige Zeit bei uns zu leben.</ta>
            <ta e="T64" id="Seg_1925" s="T58">Dann werden sie aufbrechen, morgen oder übermorgen.</ta>
            <ta e="T68" id="Seg_1926" s="T64">Es wird mir wieder das Herz zerdrücken.</ta>
            <ta e="T74" id="Seg_1927" s="T68">Sie gehen und ich werde wieder bleiben.</ta>
            <ta e="T75" id="Seg_1928" s="T74">Ich werde weinen.</ta>
            <ta e="T81" id="Seg_1929" s="T75">Weinen oder nicht, man kann nichts tun.</ta>
            <ta e="T88" id="Seg_1930" s="T81">Ich sollte Mitleid mit ihm haben und ich sollte hier Mitleid haben.</ta>
            <ta e="T91" id="Seg_1931" s="T88">Gut, genug schreiben.</ta>
            <ta e="T98" id="Seg_1932" s="T91">Ich habe vier Enkel: drei Enkel und eine Enkelin.</ta>
            <ta e="T102" id="Seg_1933" s="T98">Ich habe viele von ihnen.</ta>
            <ta e="T105" id="Seg_1934" s="T102">Darum besser ich die Hemden aus.</ta>
            <ta e="T114" id="Seg_1935" s="T105">Wenn du keine geflickten Kleider trägst, sollst du auch kein neues anziehen.</ta>
            <ta e="T127" id="Seg_1936" s="T114">Heute sind die Meinen arbeiten gegangen, nicht in ihrer eigenen Arbeit, sondern um Arbeit für den Chef zu erledigen (=in Sovkhoz arbeiten).</ta>
            <ta e="T135" id="Seg_1937" s="T127">Also, der eine, der über allen anderen steht.</ta>
            <ta e="T140" id="Seg_1938" s="T135">Alle Leute arbeiten für ihn und hören auf ihn.</ta>
            <ta e="T147" id="Seg_1939" s="T140">Also arbeiten alle Leute für ihn.</ta>
            <ta e="T152" id="Seg_1940" s="T147">Gut, es ist genug geschrieben, ich will nicht [mehr].</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_1941" s="T1">Эта старуха что тебе сказала?</ta>
            <ta e="T12" id="Seg_1942" s="T6">А ты про меня все будешь рассказывать?</ta>
            <ta e="T15" id="Seg_1943" s="T12">Я живу со своей дочерью.</ta>
            <ta e="T18" id="Seg_1944" s="T15">У меня есть сын.</ta>
            <ta e="T22" id="Seg_1945" s="T18">Он со мной не живет.</ta>
            <ta e="T29" id="Seg_1946" s="T22">У меня три внучка от дочки и одна внучка.</ta>
            <ta e="T34" id="Seg_1947" s="T29">У меня есть сноха и сын.</ta>
            <ta e="T37" id="Seg_1948" s="T34">У них один ребенок.</ta>
            <ta e="T40" id="Seg_1949" s="T37">Он еще маленький.</ta>
            <ta e="T46" id="Seg_1950" s="T40">Ему три месяца, четвертый месяц пошел.</ta>
            <ta e="T50" id="Seg_1951" s="T46">Я с сыном не живу.</ta>
            <ta e="T58" id="Seg_1952" s="T50">Они с женой ко мне приехали, недолго у нас пожить.</ta>
            <ta e="T64" id="Seg_1953" s="T58">Потом опять поедут, завтра или послезавтра.</ta>
            <ta e="T68" id="Seg_1954" s="T64">Мое сердечко опять зажмет.</ta>
            <ta e="T74" id="Seg_1955" s="T68">Они поедут, а я опять останусь.</ta>
            <ta e="T75" id="Seg_1956" s="T74">Плакать буду.</ta>
            <ta e="T81" id="Seg_1957" s="T75">Плачь не плачь, ничего не сделаешь.</ta>
            <ta e="T88" id="Seg_1958" s="T81">Его жалеть надо, и здесь жалеть надо.</ta>
            <ta e="T91" id="Seg_1959" s="T88">Ну, хватит писать.</ta>
            <ta e="T98" id="Seg_1960" s="T91">У меня четверо внуков: три внука одна внучка.</ta>
            <ta e="T102" id="Seg_1961" s="T98">У меня их много.</ta>
            <ta e="T105" id="Seg_1962" s="T102">Поэтому и починяю рубахи.</ta>
            <ta e="T114" id="Seg_1963" s="T105">Починенные рубахи [если] не носить, и хорошую рубаху не наденешь.</ta>
            <ta e="T127" id="Seg_1964" s="T114">У меня сегодня все работать ушли, не на свою работу, а работу начальника делать (на совхозную работу).</ta>
            <ta e="T135" id="Seg_1965" s="T127">Ну [человек], который над всеми людьми старший.</ta>
            <ta e="T140" id="Seg_1966" s="T135">На него работают все люди, слушаются.</ta>
            <ta e="T147" id="Seg_1967" s="T140">Ну так и работают все люди на него.</ta>
            <ta e="T152" id="Seg_1968" s="T147">Ну хватит писать, не хочу.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_1969" s="T1">эта старуха чего тебе сказала</ta>
            <ta e="T12" id="Seg_1970" s="T6">а про меня все будешь рассказывать</ta>
            <ta e="T15" id="Seg_1971" s="T12">я живу с дочерью</ta>
            <ta e="T18" id="Seg_1972" s="T15">у меня есть сын</ta>
            <ta e="T22" id="Seg_1973" s="T18">он со мной не живет</ta>
            <ta e="T29" id="Seg_1974" s="T22">у меня три дочериных внучка и одна внучка</ta>
            <ta e="T34" id="Seg_1975" s="T29">у меня есть сноха и сын</ta>
            <ta e="T37" id="Seg_1976" s="T34">у них один ребенок</ta>
            <ta e="T40" id="Seg_1977" s="T37">он еще маленький</ta>
            <ta e="T46" id="Seg_1978" s="T40">ему три месяца четвертый месяц пошел</ta>
            <ta e="T50" id="Seg_1979" s="T46">я с сыном не живу</ta>
            <ta e="T58" id="Seg_1980" s="T50">они с женой ко мне приехали не долго у нас пожить</ta>
            <ta e="T64" id="Seg_1981" s="T58">потом опять поедут завтра или послезавтра</ta>
            <ta e="T68" id="Seg_1982" s="T64">мое сердечко опять зажмет</ta>
            <ta e="T74" id="Seg_1983" s="T68">они поедут а я опять останусь</ta>
            <ta e="T75" id="Seg_1984" s="T74">плакать буду</ta>
            <ta e="T81" id="Seg_1985" s="T75">плачь не плачь ничего не сделаешь</ta>
            <ta e="T88" id="Seg_1986" s="T81">его жалеть надо и здесь жалеть надо</ta>
            <ta e="T91" id="Seg_1987" s="T88">ну хватит писать</ta>
            <ta e="T98" id="Seg_1988" s="T91">у меня четыре три внука одна внучка</ta>
            <ta e="T102" id="Seg_1989" s="T98">у меня их много</ta>
            <ta e="T105" id="Seg_1990" s="T102">поэтому и починяю рубахи</ta>
            <ta e="T114" id="Seg_1991" s="T105">починенные рубахи не носить и хорошие рубаху не наденешь</ta>
            <ta e="T127" id="Seg_1992" s="T114">у меня сегодня все работать ушли не на свою работу начальника совхозную (государственную) работу</ta>
            <ta e="T135" id="Seg_1993" s="T127">который этот человек старше всех людей (над всеми старший)</ta>
            <ta e="T140" id="Seg_1994" s="T135">ему работают все люди слушаются</ta>
            <ta e="T147" id="Seg_1995" s="T140">ну так и работают все люди ему</ta>
            <ta e="T152" id="Seg_1996" s="T147">ну и хватит писать не хочу</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T12" id="Seg_1997" s="T6">[BrM:] 'mandʼät' changed to 'man dʼät'. [BrM:] Unusual allomorph of čenčɨ-: 'čeni'.</ta>
            <ta e="T15" id="Seg_1998" s="T12">[KuAI:] Variant: 'neːuze'.</ta>
            <ta e="T58" id="Seg_1999" s="T50">[KuAI:] Variant: 'warkecʼiku'. [BrM:] Tentative analysis of 'warketʼiku'.</ta>
            <ta e="T98" id="Seg_2000" s="T91">[BrM:] 1PL?</ta>
            <ta e="T105" id="Seg_2001" s="T102">[BrM:] 1DU? [BrM:] Tentative analysis of 'kabenǯaŋ'.</ta>
            <ta e="T114" id="Seg_2002" s="T105">[KuAI:] Variants: ' ‎‎kabelbɨdi', 'kaborgɨm'.</ta>
            <ta e="T127" id="Seg_2003" s="T114">[KuAI:] wargulʼe qum – chief. [BrM:] 'meː sodimɨɣɨndɨt' changed to 'meːsodimɨɣɨndɨt'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T127" id="Seg_2004" s="T114">варгулʼе kум - начальник</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
