<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_ADeceivedOldWoman_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_ADeceivedOldWoman_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">225</ud-information>
            <ud-information attribute-name="# HIAT:w">149</ud-information>
            <ud-information attribute-name="# e">149</ud-information>
            <ud-information attribute-name="# HIAT:u">35</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T150" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Igon</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">Qɨngo</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">jedoɣon</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">pajaga</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">jes</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_20" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">Täbnä</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">jes</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">ton</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">pot</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_35" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">Täbɨm</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">qɨːdan</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">aːlɨbukuzattə</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_47" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">Täp</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">qɨːdan</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">tʼäptärgus</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_59" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">Täp</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">tʼäptälam</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">koːcʼin</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">tonustə</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_74" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">Tep</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">soːdʼigan</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">tʼäptärgus</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_86" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">Tebnan</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">tetta</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">iːt</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">jes</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_101" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">Imadlatdänan</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">iːlat</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_109" n="HIAT:w" s="T29">uš</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_112" n="HIAT:w" s="T30">warɣə</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_115" n="HIAT:w" s="T31">jezattə</ts>
                  <nts id="Seg_116" n="HIAT:ip">.</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_119" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_121" n="HIAT:w" s="T32">Täp</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">teblam</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_126" n="HIAT:ip">(</nts>
                  <ts e="T35" id="Seg_128" n="HIAT:w" s="T34">otdə</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_131" n="HIAT:w" s="T35">imadlamdə</ts>
                  <nts id="Seg_132" n="HIAT:ip">)</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_135" n="HIAT:w" s="T36">qajzʼe</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_138" n="HIAT:w" s="T37">popalam</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_141" n="HIAT:w" s="T38">kattelgustə</ts>
                  <nts id="Seg_142" n="HIAT:ip">.</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_145" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_147" n="HIAT:w" s="T39">Tebnan</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_150" n="HIAT:w" s="T40">orɨmdä</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_153" n="HIAT:w" s="T41">jeʒəlʼi</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_156" n="HIAT:w" s="T42">as</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_159" n="HIAT:w" s="T43">meːdekun</ts>
                  <nts id="Seg_160" n="HIAT:ip">,</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_163" n="HIAT:w" s="T44">täp</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_166" n="HIAT:w" s="T45">sabranit</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_169" n="HIAT:w" s="T46">közin</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_173" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_175" n="HIAT:w" s="T47">Sobranʼjetdə</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_178" n="HIAT:w" s="T48">tüːa</ts>
                  <nts id="Seg_179" n="HIAT:ip">,</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_182" n="HIAT:w" s="T49">tʼärɨn</ts>
                  <nts id="Seg_183" n="HIAT:ip">:</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_185" n="HIAT:ip">“</nts>
                  <ts e="T51" id="Seg_187" n="HIAT:w" s="T50">Man</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_190" n="HIAT:w" s="T51">iːw</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_193" n="HIAT:w" s="T52">tʼäkkɨnaltə</ts>
                  <nts id="Seg_194" n="HIAT:ip">.</nts>
                  <nts id="Seg_195" n="HIAT:ip">”</nts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_198" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_200" n="HIAT:w" s="T53">Täbɨm</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_203" n="HIAT:w" s="T54">qudugozʼe</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_206" n="HIAT:w" s="T55">tebla</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_209" n="HIAT:w" s="T56">tʼäkkɨnattə</ts>
                  <nts id="Seg_210" n="HIAT:ip">.</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_213" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_215" n="HIAT:w" s="T57">Patom</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_218" n="HIAT:w" s="T58">iːlat</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_221" n="HIAT:w" s="T59">täbnä</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_224" n="HIAT:w" s="T60">larɨbɨzattə</ts>
                  <nts id="Seg_225" n="HIAT:ip">.</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_228" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_230" n="HIAT:w" s="T61">Täbla</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_233" n="HIAT:w" s="T62">täbnä</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_236" n="HIAT:w" s="T63">as</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_239" n="HIAT:w" s="T64">nasattə</ts>
                  <nts id="Seg_240" n="HIAT:ip">.</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_243" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_245" n="HIAT:w" s="T65">Täp</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_248" n="HIAT:w" s="T66">pajamnen</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_251" n="HIAT:w" s="T67">i</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_254" n="HIAT:w" s="T68">täjdə</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_257" n="HIAT:w" s="T69">tʼäkgus</ts>
                  <nts id="Seg_258" n="HIAT:ip">.</nts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_261" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_263" n="HIAT:w" s="T70">Täbɨm</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_266" n="HIAT:w" s="T71">aːlɨbɨkuzattə</ts>
                  <nts id="Seg_267" n="HIAT:ip">.</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_270" n="HIAT:u" s="T72">
                  <nts id="Seg_271" n="HIAT:ip">“</nts>
                  <ts e="T73" id="Seg_273" n="HIAT:w" s="T72">Nimaː</ts>
                  <nts id="Seg_274" n="HIAT:ip">,</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_277" n="HIAT:w" s="T73">Petra</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_280" n="HIAT:w" s="T74">Petrowičʼen</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_283" n="HIAT:w" s="T75">maːttə</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_286" n="HIAT:w" s="T76">qwannaš</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_289" n="HIAT:w" s="T77">kosti</ts>
                  <nts id="Seg_290" n="HIAT:ip">?</nts>
                  <nts id="Seg_291" n="HIAT:ip">”</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_294" n="HIAT:u" s="T78">
                  <nts id="Seg_295" n="HIAT:ip">“</nts>
                  <ts e="T79" id="Seg_297" n="HIAT:w" s="T78">A</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_300" n="HIAT:w" s="T79">qutdär</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_303" n="HIAT:w" s="T80">man</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_306" n="HIAT:w" s="T81">qwaǯan</ts>
                  <nts id="Seg_307" n="HIAT:ip">?</nts>
                  <nts id="Seg_308" n="HIAT:ip">”</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_311" n="HIAT:u" s="T82">
                  <nts id="Seg_312" n="HIAT:ip">“</nts>
                  <ts e="T83" id="Seg_314" n="HIAT:w" s="T82">Man</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_317" n="HIAT:w" s="T83">tastɨ</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_320" n="HIAT:w" s="T84">qaǯoqaze</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_323" n="HIAT:w" s="T85">qwanneǯan</ts>
                  <nts id="Seg_324" n="HIAT:ip">.</nts>
                  <nts id="Seg_325" n="HIAT:ip">”</nts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_328" n="HIAT:u" s="T86">
                  <nts id="Seg_329" n="HIAT:ip">“</nts>
                  <ts e="T87" id="Seg_331" n="HIAT:w" s="T86">No</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_334" n="HIAT:w" s="T87">qwallaj</ts>
                  <nts id="Seg_335" n="HIAT:ip">.</nts>
                  <nts id="Seg_336" n="HIAT:ip">”</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_339" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_341" n="HIAT:w" s="T88">Pajaga</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_344" n="HIAT:w" s="T89">tʼepbɨnɨn</ts>
                  <nts id="Seg_345" n="HIAT:ip">,</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_348" n="HIAT:w" s="T90">no</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_351" n="HIAT:w" s="T91">qaǯoqat</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_354" n="HIAT:w" s="T92">omdɨn</ts>
                  <nts id="Seg_355" n="HIAT:ip">.</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_358" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_360" n="HIAT:w" s="T93">Täp</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_363" n="HIAT:w" s="T94">täbɨm</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_366" n="HIAT:w" s="T95">madɨm</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_369" n="HIAT:w" s="T96">pʼürɨn</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_372" n="HIAT:w" s="T97">koːjalǯit</ts>
                  <nts id="Seg_373" n="HIAT:ip">.</nts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_376" n="HIAT:u" s="T98">
                  <nts id="Seg_377" n="HIAT:ip">“</nts>
                  <ts e="T99" id="Seg_379" n="HIAT:w" s="T98">Noː</ts>
                  <nts id="Seg_380" n="HIAT:ip">,</nts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_383" n="HIAT:w" s="T99">serlaj</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_386" n="HIAT:w" s="T100">maːttə</ts>
                  <nts id="Seg_387" n="HIAT:ip">.</nts>
                  <nts id="Seg_388" n="HIAT:ip">”</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_391" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_393" n="HIAT:w" s="T101">Pajaga</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_396" n="HIAT:w" s="T102">maːttə</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_399" n="HIAT:w" s="T103">seːrnan</ts>
                  <nts id="Seg_400" n="HIAT:ip">.</nts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_403" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_405" n="HIAT:w" s="T104">Noːmtə</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_408" n="HIAT:w" s="T105">omtə</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_411" n="HIAT:w" s="T106">übəran</ts>
                  <nts id="Seg_412" n="HIAT:ip">.</nts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_415" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_417" n="HIAT:w" s="T107">Tʼärɨn</ts>
                  <nts id="Seg_418" n="HIAT:ip">:</nts>
                  <nts id="Seg_419" n="HIAT:ip">“</nts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_421" n="HIAT:ip">“</nts>
                  <ts e="T109" id="Seg_423" n="HIAT:w" s="T108">Tʼolom</ts>
                  <nts id="Seg_424" n="HIAT:ip">,</nts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_427" n="HIAT:w" s="T109">Petra</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_430" n="HIAT:w" s="T110">Petrowičʼ</ts>
                  <nts id="Seg_431" n="HIAT:ip">!</nts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_434" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_436" n="HIAT:w" s="T111">Qutdär</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_439" n="HIAT:w" s="T112">warkatdə</ts>
                  <nts id="Seg_440" n="HIAT:ip">?</nts>
                  <nts id="Seg_441" n="HIAT:ip">”</nts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_444" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_446" n="HIAT:w" s="T113">Täbɨn</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_449" n="HIAT:w" s="T114">otdə</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_452" n="HIAT:w" s="T115">mat</ts>
                  <nts id="Seg_453" n="HIAT:ip">,</nts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_456" n="HIAT:w" s="T116">a</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_459" n="HIAT:w" s="T117">täp</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_462" n="HIAT:w" s="T118">jas</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_465" n="HIAT:w" s="T119">qostɨqut</ts>
                  <nts id="Seg_466" n="HIAT:ip">.</nts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_469" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_471" n="HIAT:w" s="T120">Tep</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_474" n="HIAT:w" s="T121">täjrɨpaː</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_476" n="HIAT:ip">“</nts>
                  <ts e="T123" id="Seg_478" n="HIAT:w" s="T122">Kosti</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_481" n="HIAT:w" s="T123">tüːpban</ts>
                  <nts id="Seg_482" n="HIAT:ip">.</nts>
                  <nts id="Seg_483" n="HIAT:ip">”</nts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_486" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_488" n="HIAT:w" s="T124">Tabnä</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_491" n="HIAT:w" s="T125">tʼärattə</ts>
                  <nts id="Seg_492" n="HIAT:ip">:</nts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_494" n="HIAT:ip">“</nts>
                  <ts e="T127" id="Seg_496" n="HIAT:w" s="T126">Pötʼe</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_499" n="HIAT:w" s="T127">parɨt</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_502" n="HIAT:w" s="T128">sɨɣɨlga</ts>
                  <nts id="Seg_503" n="HIAT:ip">,</nts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_506" n="HIAT:w" s="T129">qotdaq</ts>
                  <nts id="Seg_507" n="HIAT:ip">.</nts>
                  <nts id="Seg_508" n="HIAT:ip">”</nts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_511" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_513" n="HIAT:w" s="T130">Tep</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_516" n="HIAT:w" s="T131">pötʼe</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_519" n="HIAT:w" s="T132">paroɣɨn</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_522" n="HIAT:w" s="T133">qotdɨs</ts>
                  <nts id="Seg_523" n="HIAT:ip">.</nts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_526" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_528" n="HIAT:w" s="T134">Wes</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_531" n="HIAT:w" s="T135">aulǯɨt</ts>
                  <nts id="Seg_532" n="HIAT:ip">.</nts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_535" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_537" n="HIAT:w" s="T136">Täp</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_540" n="HIAT:w" s="T137">pöcʼi</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_543" n="HIAT:w" s="T138">paroɣɨntɨ</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_546" n="HIAT:w" s="T139">tʼütʼoun</ts>
                  <nts id="Seg_547" n="HIAT:ip">.</nts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_550" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_552" n="HIAT:w" s="T140">Tebla</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_555" n="HIAT:w" s="T141">soɣudʼättə</ts>
                  <nts id="Seg_556" n="HIAT:ip">:</nts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_558" n="HIAT:ip">“</nts>
                  <ts e="T143" id="Seg_560" n="HIAT:w" s="T142">Nima</ts>
                  <nts id="Seg_561" n="HIAT:ip">,</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_564" n="HIAT:w" s="T143">telʼdʼan</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_567" n="HIAT:w" s="T144">Petra</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_570" n="HIAT:w" s="T145">Petrowičʼanä</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_573" n="HIAT:w" s="T146">kostʼi</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_576" n="HIAT:w" s="T147">közʼatda</ts>
                  <nts id="Seg_577" n="HIAT:ip">?</nts>
                  <nts id="Seg_578" n="HIAT:ip">”</nts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_581" n="HIAT:u" s="T148">
                  <nts id="Seg_582" n="HIAT:ip">“</nts>
                  <ts e="T149" id="Seg_584" n="HIAT:w" s="T148">Köːzʼan</ts>
                  <nts id="Seg_585" n="HIAT:ip">”</nts>
                  <nts id="Seg_586" n="HIAT:ip">,</nts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_588" n="HIAT:ip">–</nts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_591" n="HIAT:w" s="T149">tʼarat</ts>
                  <nts id="Seg_592" n="HIAT:ip">.</nts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T150" id="Seg_594" n="sc" s="T1">
               <ts e="T2" id="Seg_596" n="e" s="T1">Igon </ts>
               <ts e="T3" id="Seg_598" n="e" s="T2">Qɨngo </ts>
               <ts e="T4" id="Seg_600" n="e" s="T3">jedoɣon </ts>
               <ts e="T5" id="Seg_602" n="e" s="T4">pajaga </ts>
               <ts e="T6" id="Seg_604" n="e" s="T5">jes. </ts>
               <ts e="T7" id="Seg_606" n="e" s="T6">Täbnä </ts>
               <ts e="T8" id="Seg_608" n="e" s="T7">jes </ts>
               <ts e="T9" id="Seg_610" n="e" s="T8">ton </ts>
               <ts e="T10" id="Seg_612" n="e" s="T9">pot. </ts>
               <ts e="T11" id="Seg_614" n="e" s="T10">Täbɨm </ts>
               <ts e="T12" id="Seg_616" n="e" s="T11">qɨːdan </ts>
               <ts e="T13" id="Seg_618" n="e" s="T12">aːlɨbukuzattə. </ts>
               <ts e="T14" id="Seg_620" n="e" s="T13">Täp </ts>
               <ts e="T15" id="Seg_622" n="e" s="T14">qɨːdan </ts>
               <ts e="T16" id="Seg_624" n="e" s="T15">tʼäptärgus. </ts>
               <ts e="T17" id="Seg_626" n="e" s="T16">Täp </ts>
               <ts e="T18" id="Seg_628" n="e" s="T17">tʼäptälam </ts>
               <ts e="T19" id="Seg_630" n="e" s="T18">koːcʼin </ts>
               <ts e="T20" id="Seg_632" n="e" s="T19">tonustə. </ts>
               <ts e="T21" id="Seg_634" n="e" s="T20">Tep </ts>
               <ts e="T22" id="Seg_636" n="e" s="T21">soːdʼigan </ts>
               <ts e="T23" id="Seg_638" n="e" s="T22">tʼäptärgus. </ts>
               <ts e="T24" id="Seg_640" n="e" s="T23">Tebnan </ts>
               <ts e="T25" id="Seg_642" n="e" s="T24">tetta </ts>
               <ts e="T26" id="Seg_644" n="e" s="T25">iːt </ts>
               <ts e="T27" id="Seg_646" n="e" s="T26">jes. </ts>
               <ts e="T28" id="Seg_648" n="e" s="T27">Imadlatdänan </ts>
               <ts e="T29" id="Seg_650" n="e" s="T28">iːlat </ts>
               <ts e="T30" id="Seg_652" n="e" s="T29">uš </ts>
               <ts e="T31" id="Seg_654" n="e" s="T30">warɣə </ts>
               <ts e="T32" id="Seg_656" n="e" s="T31">jezattə. </ts>
               <ts e="T33" id="Seg_658" n="e" s="T32">Täp </ts>
               <ts e="T34" id="Seg_660" n="e" s="T33">teblam </ts>
               <ts e="T35" id="Seg_662" n="e" s="T34">(otdə </ts>
               <ts e="T36" id="Seg_664" n="e" s="T35">imadlamdə) </ts>
               <ts e="T37" id="Seg_666" n="e" s="T36">qajzʼe </ts>
               <ts e="T38" id="Seg_668" n="e" s="T37">popalam </ts>
               <ts e="T39" id="Seg_670" n="e" s="T38">kattelgustə. </ts>
               <ts e="T40" id="Seg_672" n="e" s="T39">Tebnan </ts>
               <ts e="T41" id="Seg_674" n="e" s="T40">orɨmdä </ts>
               <ts e="T42" id="Seg_676" n="e" s="T41">jeʒəlʼi </ts>
               <ts e="T43" id="Seg_678" n="e" s="T42">as </ts>
               <ts e="T44" id="Seg_680" n="e" s="T43">meːdekun, </ts>
               <ts e="T45" id="Seg_682" n="e" s="T44">täp </ts>
               <ts e="T46" id="Seg_684" n="e" s="T45">sabranit </ts>
               <ts e="T47" id="Seg_686" n="e" s="T46">közin. </ts>
               <ts e="T48" id="Seg_688" n="e" s="T47">Sobranʼjetdə </ts>
               <ts e="T49" id="Seg_690" n="e" s="T48">tüːa, </ts>
               <ts e="T50" id="Seg_692" n="e" s="T49">tʼärɨn: </ts>
               <ts e="T51" id="Seg_694" n="e" s="T50">“Man </ts>
               <ts e="T52" id="Seg_696" n="e" s="T51">iːw </ts>
               <ts e="T53" id="Seg_698" n="e" s="T52">tʼäkkɨnaltə.” </ts>
               <ts e="T54" id="Seg_700" n="e" s="T53">Täbɨm </ts>
               <ts e="T55" id="Seg_702" n="e" s="T54">qudugozʼe </ts>
               <ts e="T56" id="Seg_704" n="e" s="T55">tebla </ts>
               <ts e="T57" id="Seg_706" n="e" s="T56">tʼäkkɨnattə. </ts>
               <ts e="T58" id="Seg_708" n="e" s="T57">Patom </ts>
               <ts e="T59" id="Seg_710" n="e" s="T58">iːlat </ts>
               <ts e="T60" id="Seg_712" n="e" s="T59">täbnä </ts>
               <ts e="T61" id="Seg_714" n="e" s="T60">larɨbɨzattə. </ts>
               <ts e="T62" id="Seg_716" n="e" s="T61">Täbla </ts>
               <ts e="T63" id="Seg_718" n="e" s="T62">täbnä </ts>
               <ts e="T64" id="Seg_720" n="e" s="T63">as </ts>
               <ts e="T65" id="Seg_722" n="e" s="T64">nasattə. </ts>
               <ts e="T66" id="Seg_724" n="e" s="T65">Täp </ts>
               <ts e="T67" id="Seg_726" n="e" s="T66">pajamnen </ts>
               <ts e="T68" id="Seg_728" n="e" s="T67">i </ts>
               <ts e="T69" id="Seg_730" n="e" s="T68">täjdə </ts>
               <ts e="T70" id="Seg_732" n="e" s="T69">tʼäkgus. </ts>
               <ts e="T71" id="Seg_734" n="e" s="T70">Täbɨm </ts>
               <ts e="T72" id="Seg_736" n="e" s="T71">aːlɨbɨkuzattə. </ts>
               <ts e="T73" id="Seg_738" n="e" s="T72">“Nimaː, </ts>
               <ts e="T74" id="Seg_740" n="e" s="T73">Petra </ts>
               <ts e="T75" id="Seg_742" n="e" s="T74">Petrowičʼen </ts>
               <ts e="T76" id="Seg_744" n="e" s="T75">maːttə </ts>
               <ts e="T77" id="Seg_746" n="e" s="T76">qwannaš </ts>
               <ts e="T78" id="Seg_748" n="e" s="T77">kosti?” </ts>
               <ts e="T79" id="Seg_750" n="e" s="T78">“A </ts>
               <ts e="T80" id="Seg_752" n="e" s="T79">qutdär </ts>
               <ts e="T81" id="Seg_754" n="e" s="T80">man </ts>
               <ts e="T82" id="Seg_756" n="e" s="T81">qwaǯan?” </ts>
               <ts e="T83" id="Seg_758" n="e" s="T82">“Man </ts>
               <ts e="T84" id="Seg_760" n="e" s="T83">tastɨ </ts>
               <ts e="T85" id="Seg_762" n="e" s="T84">qaǯoqaze </ts>
               <ts e="T86" id="Seg_764" n="e" s="T85">qwanneǯan.” </ts>
               <ts e="T87" id="Seg_766" n="e" s="T86">“No </ts>
               <ts e="T88" id="Seg_768" n="e" s="T87">qwallaj.” </ts>
               <ts e="T89" id="Seg_770" n="e" s="T88">Pajaga </ts>
               <ts e="T90" id="Seg_772" n="e" s="T89">tʼepbɨnɨn, </ts>
               <ts e="T91" id="Seg_774" n="e" s="T90">no </ts>
               <ts e="T92" id="Seg_776" n="e" s="T91">qaǯoqat </ts>
               <ts e="T93" id="Seg_778" n="e" s="T92">omdɨn. </ts>
               <ts e="T94" id="Seg_780" n="e" s="T93">Täp </ts>
               <ts e="T95" id="Seg_782" n="e" s="T94">täbɨm </ts>
               <ts e="T96" id="Seg_784" n="e" s="T95">madɨm </ts>
               <ts e="T97" id="Seg_786" n="e" s="T96">pʼürɨn </ts>
               <ts e="T98" id="Seg_788" n="e" s="T97">koːjalǯit. </ts>
               <ts e="T99" id="Seg_790" n="e" s="T98">“Noː, </ts>
               <ts e="T100" id="Seg_792" n="e" s="T99">serlaj </ts>
               <ts e="T101" id="Seg_794" n="e" s="T100">maːttə.” </ts>
               <ts e="T102" id="Seg_796" n="e" s="T101">Pajaga </ts>
               <ts e="T103" id="Seg_798" n="e" s="T102">maːttə </ts>
               <ts e="T104" id="Seg_800" n="e" s="T103">seːrnan. </ts>
               <ts e="T105" id="Seg_802" n="e" s="T104">Noːmtə </ts>
               <ts e="T106" id="Seg_804" n="e" s="T105">omtə </ts>
               <ts e="T107" id="Seg_806" n="e" s="T106">übəran. </ts>
               <ts e="T108" id="Seg_808" n="e" s="T107">Tʼärɨn:“ </ts>
               <ts e="T109" id="Seg_810" n="e" s="T108">“Tʼolom, </ts>
               <ts e="T110" id="Seg_812" n="e" s="T109">Petra </ts>
               <ts e="T111" id="Seg_814" n="e" s="T110">Petrowičʼ! </ts>
               <ts e="T112" id="Seg_816" n="e" s="T111">Qutdär </ts>
               <ts e="T113" id="Seg_818" n="e" s="T112">warkatdə?” </ts>
               <ts e="T114" id="Seg_820" n="e" s="T113">Täbɨn </ts>
               <ts e="T115" id="Seg_822" n="e" s="T114">otdə </ts>
               <ts e="T116" id="Seg_824" n="e" s="T115">mat, </ts>
               <ts e="T117" id="Seg_826" n="e" s="T116">a </ts>
               <ts e="T118" id="Seg_828" n="e" s="T117">täp </ts>
               <ts e="T119" id="Seg_830" n="e" s="T118">jas </ts>
               <ts e="T120" id="Seg_832" n="e" s="T119">qostɨqut. </ts>
               <ts e="T121" id="Seg_834" n="e" s="T120">Tep </ts>
               <ts e="T122" id="Seg_836" n="e" s="T121">täjrɨpaː </ts>
               <ts e="T123" id="Seg_838" n="e" s="T122">“Kosti </ts>
               <ts e="T124" id="Seg_840" n="e" s="T123">tüːpban.” </ts>
               <ts e="T125" id="Seg_842" n="e" s="T124">Tabnä </ts>
               <ts e="T126" id="Seg_844" n="e" s="T125">tʼärattə: </ts>
               <ts e="T127" id="Seg_846" n="e" s="T126">“Pötʼe </ts>
               <ts e="T128" id="Seg_848" n="e" s="T127">parɨt </ts>
               <ts e="T129" id="Seg_850" n="e" s="T128">sɨɣɨlga, </ts>
               <ts e="T130" id="Seg_852" n="e" s="T129">qotdaq.” </ts>
               <ts e="T131" id="Seg_854" n="e" s="T130">Tep </ts>
               <ts e="T132" id="Seg_856" n="e" s="T131">pötʼe </ts>
               <ts e="T133" id="Seg_858" n="e" s="T132">paroɣɨn </ts>
               <ts e="T134" id="Seg_860" n="e" s="T133">qotdɨs. </ts>
               <ts e="T135" id="Seg_862" n="e" s="T134">Wes </ts>
               <ts e="T136" id="Seg_864" n="e" s="T135">aulǯɨt. </ts>
               <ts e="T137" id="Seg_866" n="e" s="T136">Täp </ts>
               <ts e="T138" id="Seg_868" n="e" s="T137">pöcʼi </ts>
               <ts e="T139" id="Seg_870" n="e" s="T138">paroɣɨntɨ </ts>
               <ts e="T140" id="Seg_872" n="e" s="T139">tʼütʼoun. </ts>
               <ts e="T141" id="Seg_874" n="e" s="T140">Tebla </ts>
               <ts e="T142" id="Seg_876" n="e" s="T141">soɣudʼättə: </ts>
               <ts e="T143" id="Seg_878" n="e" s="T142">“Nima, </ts>
               <ts e="T144" id="Seg_880" n="e" s="T143">telʼdʼan </ts>
               <ts e="T145" id="Seg_882" n="e" s="T144">Petra </ts>
               <ts e="T146" id="Seg_884" n="e" s="T145">Petrowičʼanä </ts>
               <ts e="T147" id="Seg_886" n="e" s="T146">kostʼi </ts>
               <ts e="T148" id="Seg_888" n="e" s="T147">közʼatda?” </ts>
               <ts e="T149" id="Seg_890" n="e" s="T148">“Köːzʼan”, – </ts>
               <ts e="T150" id="Seg_892" n="e" s="T149">tʼarat. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_893" s="T1">PVD_1964_ADeceivedOldWoman_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_894" s="T6">PVD_1964_ADeceivedOldWoman_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_895" s="T10">PVD_1964_ADeceivedOldWoman_nar.003 (001.003)</ta>
            <ta e="T16" id="Seg_896" s="T13">PVD_1964_ADeceivedOldWoman_nar.004 (001.004)</ta>
            <ta e="T20" id="Seg_897" s="T16">PVD_1964_ADeceivedOldWoman_nar.005 (001.005)</ta>
            <ta e="T23" id="Seg_898" s="T20">PVD_1964_ADeceivedOldWoman_nar.006 (001.006)</ta>
            <ta e="T27" id="Seg_899" s="T23">PVD_1964_ADeceivedOldWoman_nar.007 (001.007)</ta>
            <ta e="T32" id="Seg_900" s="T27">PVD_1964_ADeceivedOldWoman_nar.008 (001.008)</ta>
            <ta e="T39" id="Seg_901" s="T32">PVD_1964_ADeceivedOldWoman_nar.009 (001.009)</ta>
            <ta e="T47" id="Seg_902" s="T39">PVD_1964_ADeceivedOldWoman_nar.010 (001.010)</ta>
            <ta e="T53" id="Seg_903" s="T47">PVD_1964_ADeceivedOldWoman_nar.011 (001.011)</ta>
            <ta e="T57" id="Seg_904" s="T53">PVD_1964_ADeceivedOldWoman_nar.012 (001.012)</ta>
            <ta e="T61" id="Seg_905" s="T57">PVD_1964_ADeceivedOldWoman_nar.013 (001.013)</ta>
            <ta e="T65" id="Seg_906" s="T61">PVD_1964_ADeceivedOldWoman_nar.014 (001.014)</ta>
            <ta e="T70" id="Seg_907" s="T65">PVD_1964_ADeceivedOldWoman_nar.015 (001.015)</ta>
            <ta e="T72" id="Seg_908" s="T70">PVD_1964_ADeceivedOldWoman_nar.016 (001.016)</ta>
            <ta e="T78" id="Seg_909" s="T72">PVD_1964_ADeceivedOldWoman_nar.017 (001.017)</ta>
            <ta e="T82" id="Seg_910" s="T78">PVD_1964_ADeceivedOldWoman_nar.018 (001.018)</ta>
            <ta e="T86" id="Seg_911" s="T82">PVD_1964_ADeceivedOldWoman_nar.019 (001.019)</ta>
            <ta e="T88" id="Seg_912" s="T86">PVD_1964_ADeceivedOldWoman_nar.020 (001.020)</ta>
            <ta e="T93" id="Seg_913" s="T88">PVD_1964_ADeceivedOldWoman_nar.021 (001.021)</ta>
            <ta e="T98" id="Seg_914" s="T93">PVD_1964_ADeceivedOldWoman_nar.022 (001.022)</ta>
            <ta e="T101" id="Seg_915" s="T98">PVD_1964_ADeceivedOldWoman_nar.023 (001.023)</ta>
            <ta e="T104" id="Seg_916" s="T101">PVD_1964_ADeceivedOldWoman_nar.024 (001.024)</ta>
            <ta e="T107" id="Seg_917" s="T104">PVD_1964_ADeceivedOldWoman_nar.025 (001.025)</ta>
            <ta e="T111" id="Seg_918" s="T107">PVD_1964_ADeceivedOldWoman_nar.026 (001.026)</ta>
            <ta e="T113" id="Seg_919" s="T111">PVD_1964_ADeceivedOldWoman_nar.027 (001.027)</ta>
            <ta e="T120" id="Seg_920" s="T113">PVD_1964_ADeceivedOldWoman_nar.028 (001.028)</ta>
            <ta e="T124" id="Seg_921" s="T120">PVD_1964_ADeceivedOldWoman_nar.029 (001.029)</ta>
            <ta e="T130" id="Seg_922" s="T124">PVD_1964_ADeceivedOldWoman_nar.030 (001.030)</ta>
            <ta e="T134" id="Seg_923" s="T130">PVD_1964_ADeceivedOldWoman_nar.031 (001.031)</ta>
            <ta e="T136" id="Seg_924" s="T134">PVD_1964_ADeceivedOldWoman_nar.032 (001.032)</ta>
            <ta e="T140" id="Seg_925" s="T136">PVD_1964_ADeceivedOldWoman_nar.033 (001.033)</ta>
            <ta e="T148" id="Seg_926" s="T140">PVD_1964_ADeceivedOldWoman_nar.034 (001.034)</ta>
            <ta e="T150" id="Seg_927" s="T148">PVD_1964_ADeceivedOldWoman_nar.035 (001.035)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_928" s="T1">и′гон kын′го ′jедоɣон па′jага jес.</ta>
            <ta e="T10" id="Seg_929" s="T6">тӓб′нӓ jес ′тон пот.</ta>
            <ta e="T13" id="Seg_930" s="T10">тӓ′бым ′kы̄дан а̄лыб̂укузаттъ.</ta>
            <ta e="T16" id="Seg_931" s="T13">тӓп ′kы̄дан тʼӓп ′тӓргус.</ta>
            <ta e="T20" id="Seg_932" s="T16">тӓп тʼӓп′тӓлам ′ко̄цʼин ′тонустъ.</ta>
            <ta e="T23" id="Seg_933" s="T20">теп со̄дʼиган тʼӓп′тӓргус.</ta>
            <ta e="T27" id="Seg_934" s="T23">теб′нан ′те̨тта ӣт jес.</ta>
            <ta e="T32" id="Seg_935" s="T27">и′мадлатд̂ӓнан ӣлат уш ′w(в)арɣъ ′jезаттъ.</ta>
            <ta e="T39" id="Seg_936" s="T32">тӓп теб′лам (′отдъ и′мадламдъ) kай′зʼе по′палам кат′телгустъ.</ta>
            <ta e="T47" id="Seg_937" s="T39">теб′нан о′рымдӓ ′jежълʼи ас ме̄де′кун, тӓп са′бранит ′кӧзин.</ta>
            <ta e="T53" id="Seg_938" s="T47">собранʼjетдъ ′тӱ̄а, тʼӓ′рын: ман ӣв тʼӓккы′налтъ.</ta>
            <ta e="T57" id="Seg_939" s="T53">тӓ′бым ′kудугозʼе тебла ′тʼӓккынаттъ.</ta>
            <ta e="T61" id="Seg_940" s="T57">па′том ′ӣлат тӓб′нӓ ′ларыбы′заттъ.</ta>
            <ta e="T65" id="Seg_941" s="T61">тӓб′ла тӓб′нӓ ас на′саттъ.</ta>
            <ta e="T70" id="Seg_942" s="T65">тӓп ′паjамнен и ′тӓйдъ ′тʼӓкгус.</ta>
            <ta e="T72" id="Seg_943" s="T70">′тӓбым ′а̄лыб̂ык(г̂)у‵заттъ.</ta>
            <ta e="T78" id="Seg_944" s="T72">ни′ма̄, Петра Петровичен ′ма̄ттъ kwа′ннаш ′кости?</ta>
            <ta e="T82" id="Seg_945" s="T78">а kут′дӓр ман kwа′джан?</ta>
            <ta e="T86" id="Seg_946" s="T82">ман тас′ты kа′джоkазе kwа′ннеджан.</ta>
            <ta e="T88" id="Seg_947" s="T86">но kwа′ллай.</ta>
            <ta e="T93" id="Seg_948" s="T88">па′jага ′тʼепбынын, но kаджоkат ′омдын.</ta>
            <ta e="T98" id="Seg_949" s="T93">тӓп ′тӓбым ′мадым пʼӱ′рын ко̄′jалджит.</ta>
            <ta e="T101" id="Seg_950" s="T98">но̄, ′серлай ма̄ттъ.</ta>
            <ta e="T104" id="Seg_951" s="T101">па′jага ′ма̄ттъ ′се̄рнан.</ta>
            <ta e="T107" id="Seg_952" s="T104">′но̄мтъ ′омтъ ′ӱбъран.</ta>
            <ta e="T111" id="Seg_953" s="T107">тʼӓ′рын: тʼо′лом, Пет′ра Пет′рович!</ta>
            <ta e="T113" id="Seg_954" s="T111">kут′дӓр вар′катдъ?</ta>
            <ta e="T120" id="Seg_955" s="T113">′тӓбын ′отдъ мат, а тӓп jас ′kосты(у)kут.</ta>
            <ta e="T124" id="Seg_956" s="T120">теп ′тӓйрыпа̄ ′кости ′тӱ̄пбан.</ta>
            <ta e="T130" id="Seg_957" s="T124">таб′нӓ тʼӓ′раттъ: ′пӧтʼе ′парыт ′сыɣылга, kот′даk.</ta>
            <ta e="T134" id="Seg_958" s="T130">теп ′пӧтʼе ′пароɣын ′kотдыс.</ta>
            <ta e="T136" id="Seg_959" s="T134">вес а′улджыт.</ta>
            <ta e="T140" id="Seg_960" s="T136">тӓп ′пӧцʼи ′пароɣынты тʼӱ′тʼоун.</ta>
            <ta e="T148" id="Seg_961" s="T140">теб′ла соɣу′дʼӓттъ: ни′ма, ′те̨лʼдʼа(и)н Петра Петровичанӓ ′костʼи ′кӧзʼатда?</ta>
            <ta e="T150" id="Seg_962" s="T148">′кӧ̄зʼан, тʼа′рат.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_963" s="T1">igon qɨngo jedoɣon pajaga jes.</ta>
            <ta e="T10" id="Seg_964" s="T6">täbnä jes ton pot.</ta>
            <ta e="T13" id="Seg_965" s="T10">täbɨm qɨːdan aːlɨb̂ukuzattə.</ta>
            <ta e="T16" id="Seg_966" s="T13">täp qɨːdan tʼäp tärgus.</ta>
            <ta e="T20" id="Seg_967" s="T16">täp tʼäptälam koːcʼin tonustə.</ta>
            <ta e="T23" id="Seg_968" s="T20">tep soːdʼigan tʼäptärgus.</ta>
            <ta e="T27" id="Seg_969" s="T23">tebnan tetta iːt jes.</ta>
            <ta e="T32" id="Seg_970" s="T27">imadlatd̂änan iːlat uš w(w)arɣə jezattə.</ta>
            <ta e="T39" id="Seg_971" s="T32">täp teblam (otdə imadlamdə) qajzʼe popalam kattelgustə.</ta>
            <ta e="T47" id="Seg_972" s="T39">tebnan orɨmdä jeʒəlʼi as meːdekun, täp sabranit közin.</ta>
            <ta e="T53" id="Seg_973" s="T47">sobranʼjetdə tüːa, tʼärɨn: man iːw tʼäkkɨnaltə.</ta>
            <ta e="T57" id="Seg_974" s="T53">täbɨm qudugozʼe tebla tʼäkkɨnattə.</ta>
            <ta e="T61" id="Seg_975" s="T57">patom iːlat täbnä larɨbɨzattə.</ta>
            <ta e="T65" id="Seg_976" s="T61">täbla täbnä as nasattə.</ta>
            <ta e="T70" id="Seg_977" s="T65">täp pajamnen i täjdə tʼäkgus.</ta>
            <ta e="T72" id="Seg_978" s="T70">täbɨm aːlɨb̂ɨk(ĝ)uzattə.</ta>
            <ta e="T78" id="Seg_979" s="T72">nimaː, Пetra Пetrowičen maːttə qwannaš kosti?</ta>
            <ta e="T82" id="Seg_980" s="T78">a qutdär man qwaǯan?</ta>
            <ta e="T86" id="Seg_981" s="T82">man tastɨ qaǯoqaze qwanneǯan.</ta>
            <ta e="T88" id="Seg_982" s="T86">no qwallaj.</ta>
            <ta e="T93" id="Seg_983" s="T88">pajaga tʼepbɨnɨn, no qaǯoqat omdɨn.</ta>
            <ta e="T98" id="Seg_984" s="T93">täp täbɨm madɨm pʼürɨn koːjalǯit.</ta>
            <ta e="T101" id="Seg_985" s="T98">noː, serlaj maːttə.</ta>
            <ta e="T104" id="Seg_986" s="T101">pajaga maːttə seːrnan.</ta>
            <ta e="T107" id="Seg_987" s="T104">noːmtə omtə übəran.</ta>
            <ta e="T111" id="Seg_988" s="T107">tʼärɨn: tʼolom, Пetra Пetrowič!</ta>
            <ta e="T113" id="Seg_989" s="T111">qutdär warkatdə?</ta>
            <ta e="T120" id="Seg_990" s="T113">täbɨn otdə mat, a täp jas qostɨ(u)qut.</ta>
            <ta e="T124" id="Seg_991" s="T120">tep täjrɨpaː kosti tüːpban.</ta>
            <ta e="T130" id="Seg_992" s="T124">tabnä tʼärattə: pötʼe parɨt sɨɣɨlga, qotdaq.</ta>
            <ta e="T134" id="Seg_993" s="T130">tep pötʼe paroɣɨn qotdɨs.</ta>
            <ta e="T136" id="Seg_994" s="T134">wes aulǯɨt.</ta>
            <ta e="T140" id="Seg_995" s="T136">täp pöcʼi paroɣɨntɨ tʼütʼoun.</ta>
            <ta e="T148" id="Seg_996" s="T140">tebla soɣudʼättə: nima, telʼdʼa(i)n Пetra Пetrowičanä kostʼi közʼatda?</ta>
            <ta e="T150" id="Seg_997" s="T148">köːzʼan, tʼarat.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_998" s="T1">Igon Qɨngo jedoɣon pajaga jes. </ta>
            <ta e="T10" id="Seg_999" s="T6">Täbnä jes ton pot. </ta>
            <ta e="T13" id="Seg_1000" s="T10">Täbɨm qɨːdan aːlɨbukuzattə. </ta>
            <ta e="T16" id="Seg_1001" s="T13">Täp qɨːdan tʼäptärgus. </ta>
            <ta e="T20" id="Seg_1002" s="T16">Täp tʼäptälam koːcʼin tonustə. </ta>
            <ta e="T23" id="Seg_1003" s="T20">Tep soːdʼigan tʼäptärgus. </ta>
            <ta e="T27" id="Seg_1004" s="T23">Tebnan tetta iːt jes. </ta>
            <ta e="T32" id="Seg_1005" s="T27">Imadlatdänan iːlat uš warɣə jezattə. </ta>
            <ta e="T39" id="Seg_1006" s="T32">Täp teblam (otdə imadlamdə) qajzʼe popalam kattelgustə. </ta>
            <ta e="T47" id="Seg_1007" s="T39">Tebnan orɨmdä jeʒəlʼi as meːdekun, täp sabranit közin. </ta>
            <ta e="T53" id="Seg_1008" s="T47">Sobranʼjetdə tüːa, tʼärɨn: “Man iːw tʼäkkɨnaltə.” </ta>
            <ta e="T57" id="Seg_1009" s="T53">Täbɨm qudugozʼe tebla tʼäkkɨnattə. </ta>
            <ta e="T61" id="Seg_1010" s="T57">Patom iːlat täbnä larɨbɨzattə. </ta>
            <ta e="T65" id="Seg_1011" s="T61">Täbla täbnä as nasattə. </ta>
            <ta e="T70" id="Seg_1012" s="T65">Täp pajamnen i täjdə tʼäkgus. </ta>
            <ta e="T72" id="Seg_1013" s="T70">Täbɨm aːlɨbɨkuzattə. </ta>
            <ta e="T78" id="Seg_1014" s="T72">“Nimaː, Petra Petrowičʼen maːttə qwannaš kosti?” </ta>
            <ta e="T82" id="Seg_1015" s="T78">“A qutdär man qwaǯan?” </ta>
            <ta e="T86" id="Seg_1016" s="T82">“Man tastɨ qaǯoqaze qwanneǯan.” </ta>
            <ta e="T88" id="Seg_1017" s="T86">“No qwallaj.” </ta>
            <ta e="T93" id="Seg_1018" s="T88">Pajaga tʼepbɨnɨn, no qaǯoqat omdɨn. </ta>
            <ta e="T98" id="Seg_1019" s="T93">Täp täbɨm madɨm pʼürɨn koːjalǯit. </ta>
            <ta e="T101" id="Seg_1020" s="T98">“Noː, serlaj maːttə.” </ta>
            <ta e="T104" id="Seg_1021" s="T101">Pajaga maːttə seːrnan. </ta>
            <ta e="T107" id="Seg_1022" s="T104">Noːmtə omtə übəran. </ta>
            <ta e="T111" id="Seg_1023" s="T107">Tʼärɨn:“ “Tʼolom, Petra Petrowičʼ! </ta>
            <ta e="T113" id="Seg_1024" s="T111">Qutdär warkatdə?” </ta>
            <ta e="T120" id="Seg_1025" s="T113">Täbɨn otdə mat, a täp jas qostɨqut. </ta>
            <ta e="T124" id="Seg_1026" s="T120">Tep täjrɨpaː “Kosti tüːpban.” </ta>
            <ta e="T130" id="Seg_1027" s="T124">Tabnä tʼärattə: “Pötʼe parɨt sɨɣɨlga, qotdaq.” </ta>
            <ta e="T134" id="Seg_1028" s="T130">Tep pötʼe paroɣɨn qotdɨs. </ta>
            <ta e="T136" id="Seg_1029" s="T134">Wes aulǯɨt. </ta>
            <ta e="T140" id="Seg_1030" s="T136">Täp pöcʼi paroɣɨntɨ tʼütʼoun. </ta>
            <ta e="T148" id="Seg_1031" s="T140">Tebla soɣudʼättə: “Nima, telʼdʼan Petra Petrowičʼanä kostʼi közʼatda?” </ta>
            <ta e="T150" id="Seg_1032" s="T148">“Köːzʼan”, – tʼarat. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_1033" s="T1">igon</ta>
            <ta e="T3" id="Seg_1034" s="T2">Qɨngo</ta>
            <ta e="T4" id="Seg_1035" s="T3">jedo-ɣon</ta>
            <ta e="T5" id="Seg_1036" s="T4">paja-ga</ta>
            <ta e="T6" id="Seg_1037" s="T5">je-s</ta>
            <ta e="T7" id="Seg_1038" s="T6">täb-nä</ta>
            <ta e="T8" id="Seg_1039" s="T7">je-s</ta>
            <ta e="T9" id="Seg_1040" s="T8">ton</ta>
            <ta e="T10" id="Seg_1041" s="T9">po-t</ta>
            <ta e="T11" id="Seg_1042" s="T10">täb-ɨ-m</ta>
            <ta e="T12" id="Seg_1043" s="T11">qɨːdan</ta>
            <ta e="T13" id="Seg_1044" s="T12">aːlɨ-bu-ku-za-ttə</ta>
            <ta e="T14" id="Seg_1045" s="T13">täp</ta>
            <ta e="T15" id="Seg_1046" s="T14">qɨːdan</ta>
            <ta e="T16" id="Seg_1047" s="T15">tʼäptä-r-gu-s</ta>
            <ta e="T17" id="Seg_1048" s="T16">täp</ta>
            <ta e="T18" id="Seg_1049" s="T17">tʼäptä-la-m</ta>
            <ta e="T19" id="Seg_1050" s="T18">koːcʼi-n</ta>
            <ta e="T20" id="Seg_1051" s="T19">tonu-s-tə</ta>
            <ta e="T21" id="Seg_1052" s="T20">tep</ta>
            <ta e="T22" id="Seg_1053" s="T21">soːdʼiga-n</ta>
            <ta e="T23" id="Seg_1054" s="T22">tʼäptä-r-gu-s</ta>
            <ta e="T24" id="Seg_1055" s="T23">teb-nan</ta>
            <ta e="T25" id="Seg_1056" s="T24">tetta</ta>
            <ta e="T26" id="Seg_1057" s="T25">iː-t</ta>
            <ta e="T27" id="Seg_1058" s="T26">je-s</ta>
            <ta e="T28" id="Seg_1059" s="T27">imad-la-tdä-nan</ta>
            <ta e="T29" id="Seg_1060" s="T28">iː-la-t</ta>
            <ta e="T30" id="Seg_1061" s="T29">uš</ta>
            <ta e="T31" id="Seg_1062" s="T30">warɣə</ta>
            <ta e="T32" id="Seg_1063" s="T31">je-za-ttə</ta>
            <ta e="T33" id="Seg_1064" s="T32">täp</ta>
            <ta e="T34" id="Seg_1065" s="T33">teb-la-m</ta>
            <ta e="T35" id="Seg_1066" s="T34">otdə</ta>
            <ta e="T36" id="Seg_1067" s="T35">imad-la-m-də</ta>
            <ta e="T37" id="Seg_1068" s="T36">qaj-zʼe</ta>
            <ta e="T38" id="Seg_1069" s="T37">popalam</ta>
            <ta e="T39" id="Seg_1070" s="T38">katte-l-gu-s-tə</ta>
            <ta e="T40" id="Seg_1071" s="T39">teb-nan</ta>
            <ta e="T41" id="Seg_1072" s="T40">orɨm-dä</ta>
            <ta e="T42" id="Seg_1073" s="T41">jeʒəlʼi</ta>
            <ta e="T43" id="Seg_1074" s="T42">as</ta>
            <ta e="T44" id="Seg_1075" s="T43">meːde-ku-n</ta>
            <ta e="T45" id="Seg_1076" s="T44">täp</ta>
            <ta e="T46" id="Seg_1077" s="T45">sabrani-t</ta>
            <ta e="T47" id="Seg_1078" s="T46">közi-n</ta>
            <ta e="T48" id="Seg_1079" s="T47">sobranʼje-tdə</ta>
            <ta e="T49" id="Seg_1080" s="T48">tüː-a</ta>
            <ta e="T50" id="Seg_1081" s="T49">tʼärɨ-n</ta>
            <ta e="T51" id="Seg_1082" s="T50">Man</ta>
            <ta e="T52" id="Seg_1083" s="T51">iː-w</ta>
            <ta e="T53" id="Seg_1084" s="T52">tʼäkkɨ-naltə</ta>
            <ta e="T54" id="Seg_1085" s="T53">täb-ɨ-m</ta>
            <ta e="T55" id="Seg_1086" s="T54">qudugo-zʼe</ta>
            <ta e="T56" id="Seg_1087" s="T55">teb-la</ta>
            <ta e="T57" id="Seg_1088" s="T56">tʼäkkɨ-na-ttə</ta>
            <ta e="T58" id="Seg_1089" s="T57">patom</ta>
            <ta e="T59" id="Seg_1090" s="T58">iː-la-t</ta>
            <ta e="T60" id="Seg_1091" s="T59">täb-nä</ta>
            <ta e="T61" id="Seg_1092" s="T60">larɨ-bɨ-za-ttə</ta>
            <ta e="T62" id="Seg_1093" s="T61">täb-la</ta>
            <ta e="T63" id="Seg_1094" s="T62">täb-nä</ta>
            <ta e="T64" id="Seg_1095" s="T63">as</ta>
            <ta e="T65" id="Seg_1096" s="T64">nasa-ttə</ta>
            <ta e="T66" id="Seg_1097" s="T65">täp</ta>
            <ta e="T67" id="Seg_1098" s="T66">paja-m-ne-n</ta>
            <ta e="T68" id="Seg_1099" s="T67">i</ta>
            <ta e="T69" id="Seg_1100" s="T68">täj-də</ta>
            <ta e="T70" id="Seg_1101" s="T69">tʼäkgu-s</ta>
            <ta e="T71" id="Seg_1102" s="T70">täb-ɨ-m</ta>
            <ta e="T72" id="Seg_1103" s="T71">aːlɨ-bɨ-ku-za-ttə</ta>
            <ta e="T73" id="Seg_1104" s="T72">nimaː</ta>
            <ta e="T74" id="Seg_1105" s="T73">Petra</ta>
            <ta e="T75" id="Seg_1106" s="T74">Petrowičʼ-e-n</ta>
            <ta e="T76" id="Seg_1107" s="T75">maːt-tə</ta>
            <ta e="T77" id="Seg_1108" s="T76">qwan-naš</ta>
            <ta e="T78" id="Seg_1109" s="T77">kosti</ta>
            <ta e="T79" id="Seg_1110" s="T78">a</ta>
            <ta e="T80" id="Seg_1111" s="T79">qutdär</ta>
            <ta e="T81" id="Seg_1112" s="T80">man</ta>
            <ta e="T82" id="Seg_1113" s="T81">qwa-ǯa-n</ta>
            <ta e="T83" id="Seg_1114" s="T82">Man</ta>
            <ta e="T84" id="Seg_1115" s="T83">tastɨ</ta>
            <ta e="T85" id="Seg_1116" s="T84">qaǯo-qa-ze</ta>
            <ta e="T86" id="Seg_1117" s="T85">qwan-n-eǯa-n</ta>
            <ta e="T87" id="Seg_1118" s="T86">no</ta>
            <ta e="T88" id="Seg_1119" s="T87">qwal-la-j</ta>
            <ta e="T89" id="Seg_1120" s="T88">paja-ga</ta>
            <ta e="T90" id="Seg_1121" s="T89">tʼepbɨ-nɨ-n</ta>
            <ta e="T91" id="Seg_1122" s="T90">no</ta>
            <ta e="T92" id="Seg_1123" s="T91">qaǯo-qa-t</ta>
            <ta e="T93" id="Seg_1124" s="T92">omdɨ-n</ta>
            <ta e="T94" id="Seg_1125" s="T93">täp</ta>
            <ta e="T95" id="Seg_1126" s="T94">täb-ɨ-m</ta>
            <ta e="T96" id="Seg_1127" s="T95">mad-ɨ-m</ta>
            <ta e="T97" id="Seg_1128" s="T96">pʼür-ɨ-n</ta>
            <ta e="T98" id="Seg_1129" s="T97">koːja-lǯi-t</ta>
            <ta e="T99" id="Seg_1130" s="T98">noː</ta>
            <ta e="T100" id="Seg_1131" s="T99">ser-la-j</ta>
            <ta e="T101" id="Seg_1132" s="T100">maːt-tə</ta>
            <ta e="T102" id="Seg_1133" s="T101">paja-ga</ta>
            <ta e="T103" id="Seg_1134" s="T102">maːt-tə</ta>
            <ta e="T104" id="Seg_1135" s="T103">seːr-na-n</ta>
            <ta e="T105" id="Seg_1136" s="T104">noːm-tə</ta>
            <ta e="T106" id="Seg_1137" s="T105">omtə</ta>
            <ta e="T107" id="Seg_1138" s="T106">übə-r-a-n</ta>
            <ta e="T108" id="Seg_1139" s="T107">tʼärɨ-n</ta>
            <ta e="T109" id="Seg_1140" s="T108">tʼolom</ta>
            <ta e="T110" id="Seg_1141" s="T109">Petra</ta>
            <ta e="T111" id="Seg_1142" s="T110">Petrowičʼ</ta>
            <ta e="T112" id="Seg_1143" s="T111">qutdär</ta>
            <ta e="T113" id="Seg_1144" s="T112">warka-tdə</ta>
            <ta e="T114" id="Seg_1145" s="T113">täb-ɨ-n</ta>
            <ta e="T115" id="Seg_1146" s="T114">otdə</ta>
            <ta e="T116" id="Seg_1147" s="T115">mat</ta>
            <ta e="T117" id="Seg_1148" s="T116">a</ta>
            <ta e="T118" id="Seg_1149" s="T117">täp</ta>
            <ta e="T119" id="Seg_1150" s="T118">jas</ta>
            <ta e="T120" id="Seg_1151" s="T119">qostɨ-qu-t</ta>
            <ta e="T121" id="Seg_1152" s="T120">tep</ta>
            <ta e="T122" id="Seg_1153" s="T121">täjrɨpaː</ta>
            <ta e="T123" id="Seg_1154" s="T122">kosti</ta>
            <ta e="T124" id="Seg_1155" s="T123">tüː-pba-n</ta>
            <ta e="T125" id="Seg_1156" s="T124">tab-nä</ta>
            <ta e="T126" id="Seg_1157" s="T125">tʼära-ttə</ta>
            <ta e="T127" id="Seg_1158" s="T126">pötʼe</ta>
            <ta e="T128" id="Seg_1159" s="T127">par-ɨ-t</ta>
            <ta e="T129" id="Seg_1160" s="T128">sɨɣɨ-l-ga</ta>
            <ta e="T130" id="Seg_1161" s="T129">qotda-q</ta>
            <ta e="T131" id="Seg_1162" s="T130">tep</ta>
            <ta e="T132" id="Seg_1163" s="T131">pötʼe</ta>
            <ta e="T133" id="Seg_1164" s="T132">par-o-ɣɨn</ta>
            <ta e="T134" id="Seg_1165" s="T133">qotdɨ-s</ta>
            <ta e="T135" id="Seg_1166" s="T134">wes</ta>
            <ta e="T136" id="Seg_1167" s="T135">aulǯɨ-t</ta>
            <ta e="T137" id="Seg_1168" s="T136">täp</ta>
            <ta e="T138" id="Seg_1169" s="T137">pöcʼi</ta>
            <ta e="T139" id="Seg_1170" s="T138">par-o-ɣɨntɨ</ta>
            <ta e="T140" id="Seg_1171" s="T139">tʼütʼou-n</ta>
            <ta e="T141" id="Seg_1172" s="T140">teb-la</ta>
            <ta e="T142" id="Seg_1173" s="T141">soɣudʼä-ttə</ta>
            <ta e="T143" id="Seg_1174" s="T142">nima</ta>
            <ta e="T144" id="Seg_1175" s="T143">telʼdʼan</ta>
            <ta e="T145" id="Seg_1176" s="T144">Petra</ta>
            <ta e="T146" id="Seg_1177" s="T145">Petrowičʼ-a-nä</ta>
            <ta e="T147" id="Seg_1178" s="T146">kostʼi</ta>
            <ta e="T148" id="Seg_1179" s="T147">közʼa-tda</ta>
            <ta e="T149" id="Seg_1180" s="T148">köːzʼa-n</ta>
            <ta e="T150" id="Seg_1181" s="T149">tʼara-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1182" s="T1">ugon</ta>
            <ta e="T3" id="Seg_1183" s="T2">Qɨngo</ta>
            <ta e="T4" id="Seg_1184" s="T3">eːde-qɨn</ta>
            <ta e="T5" id="Seg_1185" s="T4">paja-ka</ta>
            <ta e="T6" id="Seg_1186" s="T5">eː-sɨ</ta>
            <ta e="T7" id="Seg_1187" s="T6">täp-nä</ta>
            <ta e="T8" id="Seg_1188" s="T7">eː-sɨ</ta>
            <ta e="T9" id="Seg_1189" s="T8">ton</ta>
            <ta e="T10" id="Seg_1190" s="T9">po-tə</ta>
            <ta e="T11" id="Seg_1191" s="T10">täp-ɨ-m</ta>
            <ta e="T12" id="Seg_1192" s="T11">qɨdan</ta>
            <ta e="T13" id="Seg_1193" s="T12">aːlu-mbɨ-ku-sɨ-tɨn</ta>
            <ta e="T14" id="Seg_1194" s="T13">täp</ta>
            <ta e="T15" id="Seg_1195" s="T14">qɨdan</ta>
            <ta e="T16" id="Seg_1196" s="T15">tʼäptä-r-ku-sɨ</ta>
            <ta e="T17" id="Seg_1197" s="T16">täp</ta>
            <ta e="T18" id="Seg_1198" s="T17">tʼäptä-la-m</ta>
            <ta e="T19" id="Seg_1199" s="T18">koːci-ŋ</ta>
            <ta e="T20" id="Seg_1200" s="T19">tonu-sɨ-t</ta>
            <ta e="T21" id="Seg_1201" s="T20">täp</ta>
            <ta e="T22" id="Seg_1202" s="T21">soːdʼiga-ŋ</ta>
            <ta e="T23" id="Seg_1203" s="T22">tʼäptä-r-ku-sɨ</ta>
            <ta e="T24" id="Seg_1204" s="T23">täp-nan</ta>
            <ta e="T25" id="Seg_1205" s="T24">tettɨ</ta>
            <ta e="T26" id="Seg_1206" s="T25">iː-tə</ta>
            <ta e="T27" id="Seg_1207" s="T26">eː-sɨ</ta>
            <ta e="T28" id="Seg_1208" s="T27">ilmat-la-tə-nan</ta>
            <ta e="T29" id="Seg_1209" s="T28">iː-la-tə</ta>
            <ta e="T30" id="Seg_1210" s="T29">uʒ</ta>
            <ta e="T31" id="Seg_1211" s="T30">wargɨ</ta>
            <ta e="T32" id="Seg_1212" s="T31">eː-sɨ-tɨn</ta>
            <ta e="T33" id="Seg_1213" s="T32">täp</ta>
            <ta e="T34" id="Seg_1214" s="T33">täp-la-m</ta>
            <ta e="T35" id="Seg_1215" s="T34">ondə</ta>
            <ta e="T36" id="Seg_1216" s="T35">ilmat-la-m-tə</ta>
            <ta e="T37" id="Seg_1217" s="T36">qaj-se</ta>
            <ta e="T38" id="Seg_1218" s="T37">papal</ta>
            <ta e="T39" id="Seg_1219" s="T38">katɨ-l-ku-sɨ-t</ta>
            <ta e="T40" id="Seg_1220" s="T39">täp-nan</ta>
            <ta e="T41" id="Seg_1221" s="T40">orɨm-tə</ta>
            <ta e="T42" id="Seg_1222" s="T41">jesʼli</ta>
            <ta e="T43" id="Seg_1223" s="T42">asa</ta>
            <ta e="T44" id="Seg_1224" s="T43">medə-ku-n</ta>
            <ta e="T45" id="Seg_1225" s="T44">täp</ta>
            <ta e="T46" id="Seg_1226" s="T45">sabrani-ntə</ta>
            <ta e="T47" id="Seg_1227" s="T46">közi-n</ta>
            <ta e="T48" id="Seg_1228" s="T47">sabrani-ntə</ta>
            <ta e="T49" id="Seg_1229" s="T48">tüː-nɨ</ta>
            <ta e="T50" id="Seg_1230" s="T49">tʼärɨ-n</ta>
            <ta e="T51" id="Seg_1231" s="T50">man</ta>
            <ta e="T52" id="Seg_1232" s="T51">iː-w</ta>
            <ta e="T53" id="Seg_1233" s="T52">tʼäkkut-naltə</ta>
            <ta e="T54" id="Seg_1234" s="T53">täp-ɨ-m</ta>
            <ta e="T55" id="Seg_1235" s="T54">quːdəgo-se</ta>
            <ta e="T56" id="Seg_1236" s="T55">täp-la</ta>
            <ta e="T57" id="Seg_1237" s="T56">tʼäkkut-nɨ-tɨn</ta>
            <ta e="T58" id="Seg_1238" s="T57">patom</ta>
            <ta e="T59" id="Seg_1239" s="T58">iː-la-tə</ta>
            <ta e="T60" id="Seg_1240" s="T59">täp-nä</ta>
            <ta e="T61" id="Seg_1241" s="T60">*larɨ-mbɨ-sɨ-tɨn</ta>
            <ta e="T62" id="Seg_1242" s="T61">täp-la</ta>
            <ta e="T63" id="Seg_1243" s="T62">täp-nä</ta>
            <ta e="T64" id="Seg_1244" s="T63">asa</ta>
            <ta e="T65" id="Seg_1245" s="T64">nasa-tɨn</ta>
            <ta e="T66" id="Seg_1246" s="T65">täp</ta>
            <ta e="T67" id="Seg_1247" s="T66">paja-m-nɨ-n</ta>
            <ta e="T68" id="Seg_1248" s="T67">i</ta>
            <ta e="T69" id="Seg_1249" s="T68">täj-tə</ta>
            <ta e="T70" id="Seg_1250" s="T69">tʼäkku-sɨ</ta>
            <ta e="T71" id="Seg_1251" s="T70">täp-ɨ-m</ta>
            <ta e="T72" id="Seg_1252" s="T71">aːlu-mbɨ-ku-sɨ-tɨn</ta>
            <ta e="T73" id="Seg_1253" s="T72">nima</ta>
            <ta e="T74" id="Seg_1254" s="T73">Petra</ta>
            <ta e="T75" id="Seg_1255" s="T74">Petrowičʼ-ɨ-n</ta>
            <ta e="T76" id="Seg_1256" s="T75">maːt-ntə</ta>
            <ta e="T77" id="Seg_1257" s="T76">qwan-naš</ta>
            <ta e="T78" id="Seg_1258" s="T77">kosti</ta>
            <ta e="T79" id="Seg_1259" s="T78">a</ta>
            <ta e="T80" id="Seg_1260" s="T79">qundar</ta>
            <ta e="T81" id="Seg_1261" s="T80">man</ta>
            <ta e="T82" id="Seg_1262" s="T81">qwan-enǯɨ-ŋ</ta>
            <ta e="T83" id="Seg_1263" s="T82">man</ta>
            <ta e="T84" id="Seg_1264" s="T83">tastɨ</ta>
            <ta e="T85" id="Seg_1265" s="T84">qanǯə-ka-se</ta>
            <ta e="T86" id="Seg_1266" s="T85">qwan-tɨ-enǯɨ-ŋ</ta>
            <ta e="T87" id="Seg_1267" s="T86">nu</ta>
            <ta e="T88" id="Seg_1268" s="T87">qwan-lä-j</ta>
            <ta e="T89" id="Seg_1269" s="T88">paja-ka</ta>
            <ta e="T90" id="Seg_1270" s="T89">tʼipbɨ-nɨ-n</ta>
            <ta e="T91" id="Seg_1271" s="T90">nu</ta>
            <ta e="T92" id="Seg_1272" s="T91">qanǯə-ka-ntə</ta>
            <ta e="T93" id="Seg_1273" s="T92">omdɨ-n</ta>
            <ta e="T94" id="Seg_1274" s="T93">täp</ta>
            <ta e="T95" id="Seg_1275" s="T94">täp-ɨ-m</ta>
            <ta e="T96" id="Seg_1276" s="T95">maːt-ɨ-m</ta>
            <ta e="T97" id="Seg_1277" s="T96">pör-ɨ-ŋ</ta>
            <ta e="T98" id="Seg_1278" s="T97">koja-lǯi-t</ta>
            <ta e="T99" id="Seg_1279" s="T98">nu</ta>
            <ta e="T100" id="Seg_1280" s="T99">ser-lä-j</ta>
            <ta e="T101" id="Seg_1281" s="T100">maːt-ntə</ta>
            <ta e="T102" id="Seg_1282" s="T101">paja-ka</ta>
            <ta e="T103" id="Seg_1283" s="T102">maːt-ntə</ta>
            <ta e="T104" id="Seg_1284" s="T103">ser-nɨ-n</ta>
            <ta e="T105" id="Seg_1285" s="T104">nom-ntə</ta>
            <ta e="T106" id="Seg_1286" s="T105">omtə</ta>
            <ta e="T107" id="Seg_1287" s="T106">übɨ-r-ɨ-n</ta>
            <ta e="T108" id="Seg_1288" s="T107">tʼärɨ-n</ta>
            <ta e="T109" id="Seg_1289" s="T108">tʼolom</ta>
            <ta e="T110" id="Seg_1290" s="T109">Petra</ta>
            <ta e="T111" id="Seg_1291" s="T110">Petrowičʼ</ta>
            <ta e="T112" id="Seg_1292" s="T111">qundar</ta>
            <ta e="T113" id="Seg_1293" s="T112">warkɨ-ntə</ta>
            <ta e="T114" id="Seg_1294" s="T113">täp-ɨ-n</ta>
            <ta e="T115" id="Seg_1295" s="T114">ondə</ta>
            <ta e="T116" id="Seg_1296" s="T115">maːt</ta>
            <ta e="T117" id="Seg_1297" s="T116">a</ta>
            <ta e="T118" id="Seg_1298" s="T117">täp</ta>
            <ta e="T119" id="Seg_1299" s="T118">asa</ta>
            <ta e="T120" id="Seg_1300" s="T119">kostɨ-ku-t</ta>
            <ta e="T121" id="Seg_1301" s="T120">täp</ta>
            <ta e="T122" id="Seg_1302" s="T121">tärba</ta>
            <ta e="T123" id="Seg_1303" s="T122">kosti</ta>
            <ta e="T124" id="Seg_1304" s="T123">tüː-mbɨ-ŋ</ta>
            <ta e="T125" id="Seg_1305" s="T124">täp-nä</ta>
            <ta e="T126" id="Seg_1306" s="T125">tʼärɨ-tɨn</ta>
            <ta e="T127" id="Seg_1307" s="T126">pötte</ta>
            <ta e="T128" id="Seg_1308" s="T127">par-ɨ-ntə</ta>
            <ta e="T129" id="Seg_1309" s="T128">sɨɣə-l-kɨ</ta>
            <ta e="T130" id="Seg_1310" s="T129">qondu-kɨ</ta>
            <ta e="T131" id="Seg_1311" s="T130">täp</ta>
            <ta e="T132" id="Seg_1312" s="T131">pötte</ta>
            <ta e="T133" id="Seg_1313" s="T132">par-ɨ-qɨn</ta>
            <ta e="T134" id="Seg_1314" s="T133">qondu-sɨ</ta>
            <ta e="T135" id="Seg_1315" s="T134">wesʼ</ta>
            <ta e="T136" id="Seg_1316" s="T135">aulǯu-t</ta>
            <ta e="T137" id="Seg_1317" s="T136">täp</ta>
            <ta e="T138" id="Seg_1318" s="T137">pötte</ta>
            <ta e="T139" id="Seg_1319" s="T138">par-ɨ-qɨntɨ</ta>
            <ta e="T140" id="Seg_1320" s="T139">tʼötʼöu-n</ta>
            <ta e="T141" id="Seg_1321" s="T140">täp-la</ta>
            <ta e="T142" id="Seg_1322" s="T141">sogundʼe-tɨn</ta>
            <ta e="T143" id="Seg_1323" s="T142">nima</ta>
            <ta e="T144" id="Seg_1324" s="T143">telʼdʼan</ta>
            <ta e="T145" id="Seg_1325" s="T144">Petra</ta>
            <ta e="T146" id="Seg_1326" s="T145">Petrowičʼ-ɨ-nä</ta>
            <ta e="T147" id="Seg_1327" s="T146">kosti</ta>
            <ta e="T148" id="Seg_1328" s="T147">közi-ntə</ta>
            <ta e="T149" id="Seg_1329" s="T148">közi-ŋ</ta>
            <ta e="T150" id="Seg_1330" s="T149">tʼärɨ-t</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1331" s="T1">earlier</ta>
            <ta e="T3" id="Seg_1332" s="T2">Kostenkino.[NOM]</ta>
            <ta e="T4" id="Seg_1333" s="T3">village-LOC</ta>
            <ta e="T5" id="Seg_1334" s="T4">old.woman-DIM.[NOM]</ta>
            <ta e="T6" id="Seg_1335" s="T5">be-PST.[3SG.S]</ta>
            <ta e="T7" id="Seg_1336" s="T6">(s)he-ALL</ta>
            <ta e="T8" id="Seg_1337" s="T7">be-PST.[3SG.S]</ta>
            <ta e="T9" id="Seg_1338" s="T8">hundred</ta>
            <ta e="T10" id="Seg_1339" s="T9">year.[NOM]-3SG</ta>
            <ta e="T11" id="Seg_1340" s="T10">(s)he-EP-ACC</ta>
            <ta e="T12" id="Seg_1341" s="T11">all.the.time</ta>
            <ta e="T13" id="Seg_1342" s="T12">cheat-DUR-HAB-PST-3PL</ta>
            <ta e="T14" id="Seg_1343" s="T13">(s)he.[NOM]</ta>
            <ta e="T15" id="Seg_1344" s="T14">all.the.time</ta>
            <ta e="T16" id="Seg_1345" s="T15">tale-VBLZ-HAB-PST.[3SG.S]</ta>
            <ta e="T17" id="Seg_1346" s="T16">(s)he.[NOM]</ta>
            <ta e="T18" id="Seg_1347" s="T17">tale-PL-ACC</ta>
            <ta e="T19" id="Seg_1348" s="T18">much-ADVZ</ta>
            <ta e="T20" id="Seg_1349" s="T19">know-PST-3SG.O</ta>
            <ta e="T21" id="Seg_1350" s="T20">(s)he.[NOM]</ta>
            <ta e="T22" id="Seg_1351" s="T21">good-ADVZ</ta>
            <ta e="T23" id="Seg_1352" s="T22">tale-VBLZ-HAB-PST.[3SG.S]</ta>
            <ta e="T24" id="Seg_1353" s="T23">(s)he-ADES</ta>
            <ta e="T25" id="Seg_1354" s="T24">four</ta>
            <ta e="T26" id="Seg_1355" s="T25">son.[NOM]-3SG</ta>
            <ta e="T27" id="Seg_1356" s="T26">be-PST.[3SG.S]</ta>
            <ta e="T28" id="Seg_1357" s="T27">child-PL-3SG-ADES</ta>
            <ta e="T29" id="Seg_1358" s="T28">son-PL.[NOM]-3SG</ta>
            <ta e="T30" id="Seg_1359" s="T29">already</ta>
            <ta e="T31" id="Seg_1360" s="T30">big</ta>
            <ta e="T32" id="Seg_1361" s="T31">be-PST-3PL</ta>
            <ta e="T33" id="Seg_1362" s="T32">(s)he.[NOM]</ta>
            <ta e="T34" id="Seg_1363" s="T33">(s)he-PL-ACC</ta>
            <ta e="T35" id="Seg_1364" s="T34">oneself.3SG</ta>
            <ta e="T36" id="Seg_1365" s="T35">child-PL-ACC-3SG</ta>
            <ta e="T37" id="Seg_1366" s="T36">what-INSTR</ta>
            <ta e="T38" id="Seg_1367" s="T37">how</ta>
            <ta e="T39" id="Seg_1368" s="T38">hit-INCH-HAB-PST-3SG.O</ta>
            <ta e="T40" id="Seg_1369" s="T39">(s)he-ADES</ta>
            <ta e="T41" id="Seg_1370" s="T40">force.[NOM]-3SG</ta>
            <ta e="T42" id="Seg_1371" s="T41">if</ta>
            <ta e="T43" id="Seg_1372" s="T42">NEG</ta>
            <ta e="T44" id="Seg_1373" s="T43">suffice-HAB-3SG.S</ta>
            <ta e="T45" id="Seg_1374" s="T44">(s)he.[NOM]</ta>
            <ta e="T46" id="Seg_1375" s="T45">meeting-ILL</ta>
            <ta e="T47" id="Seg_1376" s="T46">go-3SG.S</ta>
            <ta e="T48" id="Seg_1377" s="T47">meeting-ILL</ta>
            <ta e="T49" id="Seg_1378" s="T48">come-CO.[3SG.S]</ta>
            <ta e="T50" id="Seg_1379" s="T49">say-3SG.S</ta>
            <ta e="T51" id="Seg_1380" s="T50">I.GEN</ta>
            <ta e="T52" id="Seg_1381" s="T51">son.[NOM]-1SG</ta>
            <ta e="T53" id="Seg_1382" s="T52">whip-IMP.2PL.S/O</ta>
            <ta e="T54" id="Seg_1383" s="T53">(s)he-EP-ACC</ta>
            <ta e="T55" id="Seg_1384" s="T54">pull.strap-INSTR</ta>
            <ta e="T56" id="Seg_1385" s="T55">(s)he-PL.[NOM]</ta>
            <ta e="T57" id="Seg_1386" s="T56">whip-CO-3PL</ta>
            <ta e="T58" id="Seg_1387" s="T57">then</ta>
            <ta e="T59" id="Seg_1388" s="T58">son-PL.[NOM]-3SG</ta>
            <ta e="T60" id="Seg_1389" s="T59">(s)he-ALL</ta>
            <ta e="T61" id="Seg_1390" s="T60">get.afraid-DUR-PST-3PL</ta>
            <ta e="T62" id="Seg_1391" s="T61">(s)he-PL.[NOM]</ta>
            <ta e="T63" id="Seg_1392" s="T62">(s)he-ALL</ta>
            <ta e="T64" id="Seg_1393" s="T63">NEG</ta>
            <ta e="T65" id="Seg_1394" s="T64">%%-3PL</ta>
            <ta e="T66" id="Seg_1395" s="T65">(s)he.[NOM]</ta>
            <ta e="T67" id="Seg_1396" s="T66">old.woman-TRL-CO-3SG.S</ta>
            <ta e="T68" id="Seg_1397" s="T67">and</ta>
            <ta e="T69" id="Seg_1398" s="T68">mind.[NOM]-3SG</ta>
            <ta e="T70" id="Seg_1399" s="T69">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T71" id="Seg_1400" s="T70">(s)he-EP-ACC</ta>
            <ta e="T72" id="Seg_1401" s="T71">cheat-DUR-HAB-PST-3PL</ta>
            <ta e="T73" id="Seg_1402" s="T72">grandmother.[NOM]</ta>
            <ta e="T74" id="Seg_1403" s="T73">Pjotr.[NOM]</ta>
            <ta e="T75" id="Seg_1404" s="T74">Petrovich-EP-GEN</ta>
            <ta e="T76" id="Seg_1405" s="T75">house-ILL</ta>
            <ta e="T77" id="Seg_1406" s="T76">leave-POT.FUT.2SG</ta>
            <ta e="T78" id="Seg_1407" s="T77">visiting</ta>
            <ta e="T79" id="Seg_1408" s="T78">and</ta>
            <ta e="T80" id="Seg_1409" s="T79">how</ta>
            <ta e="T81" id="Seg_1410" s="T80">I.NOM</ta>
            <ta e="T82" id="Seg_1411" s="T81">leave-FUT-1SG.S</ta>
            <ta e="T83" id="Seg_1412" s="T82">I.NOM</ta>
            <ta e="T84" id="Seg_1413" s="T83">you.SG.ACC</ta>
            <ta e="T85" id="Seg_1414" s="T84">sledge-DIM-INSTR</ta>
            <ta e="T86" id="Seg_1415" s="T85">leave-TR-FUT-1SG.S</ta>
            <ta e="T87" id="Seg_1416" s="T86">now</ta>
            <ta e="T88" id="Seg_1417" s="T87">leave-OPT-1DU</ta>
            <ta e="T89" id="Seg_1418" s="T88">old.woman-DIM.[NOM]</ta>
            <ta e="T90" id="Seg_1419" s="T89">dress-CO-3SG.S</ta>
            <ta e="T91" id="Seg_1420" s="T90">now</ta>
            <ta e="T92" id="Seg_1421" s="T91">sledge-DIM-ILL</ta>
            <ta e="T93" id="Seg_1422" s="T92">sit.down-3SG.S</ta>
            <ta e="T94" id="Seg_1423" s="T93">(s)he.[NOM]</ta>
            <ta e="T95" id="Seg_1424" s="T94">(s)he-EP-ACC</ta>
            <ta e="T96" id="Seg_1425" s="T95">house-EP-ACC</ta>
            <ta e="T97" id="Seg_1426" s="T96">circle-EP-ADVZ</ta>
            <ta e="T98" id="Seg_1427" s="T97">circle-TR-3SG.O</ta>
            <ta e="T99" id="Seg_1428" s="T98">now</ta>
            <ta e="T100" id="Seg_1429" s="T99">come.in-OPT-1DU</ta>
            <ta e="T101" id="Seg_1430" s="T100">house-ILL</ta>
            <ta e="T102" id="Seg_1431" s="T101">old.woman-DIM.[NOM]</ta>
            <ta e="T103" id="Seg_1432" s="T102">house-ILL</ta>
            <ta e="T104" id="Seg_1433" s="T103">come.in-CO-3SG.S</ta>
            <ta e="T105" id="Seg_1434" s="T104">god-ILL</ta>
            <ta e="T106" id="Seg_1435" s="T105">pray</ta>
            <ta e="T107" id="Seg_1436" s="T106">begin-FRQ-EP-3SG.S</ta>
            <ta e="T108" id="Seg_1437" s="T107">say-3SG.S</ta>
            <ta e="T109" id="Seg_1438" s="T108">hello</ta>
            <ta e="T110" id="Seg_1439" s="T109">Pjotr.[NOM]</ta>
            <ta e="T111" id="Seg_1440" s="T110">Petrovich.[NOM]</ta>
            <ta e="T112" id="Seg_1441" s="T111">how</ta>
            <ta e="T113" id="Seg_1442" s="T112">live-2SG.S</ta>
            <ta e="T114" id="Seg_1443" s="T113">(s)he-EP-GEN</ta>
            <ta e="T115" id="Seg_1444" s="T114">own.3SG</ta>
            <ta e="T116" id="Seg_1445" s="T115">house.[NOM]</ta>
            <ta e="T117" id="Seg_1446" s="T116">and</ta>
            <ta e="T118" id="Seg_1447" s="T117">(s)he.[NOM]</ta>
            <ta e="T119" id="Seg_1448" s="T118">NEG</ta>
            <ta e="T120" id="Seg_1449" s="T119">get.to.know-HAB-3SG.O</ta>
            <ta e="T121" id="Seg_1450" s="T120">(s)he.[NOM]</ta>
            <ta e="T122" id="Seg_1451" s="T121">think.[3SG.S]</ta>
            <ta e="T123" id="Seg_1452" s="T122">visiting</ta>
            <ta e="T124" id="Seg_1453" s="T123">come-PST.NAR-1SG.S</ta>
            <ta e="T125" id="Seg_1454" s="T124">(s)he-ALL</ta>
            <ta e="T126" id="Seg_1455" s="T125">say-3PL</ta>
            <ta e="T127" id="Seg_1456" s="T126">stove.[NOM]</ta>
            <ta e="T128" id="Seg_1457" s="T127">top-EP-ILL</ta>
            <ta e="T129" id="Seg_1458" s="T128">climb-INCH-IMP.2SG.S</ta>
            <ta e="T130" id="Seg_1459" s="T129">sleep-IMP.2SG.S</ta>
            <ta e="T131" id="Seg_1460" s="T130">(s)he.[NOM]</ta>
            <ta e="T132" id="Seg_1461" s="T131">stove.[NOM]</ta>
            <ta e="T133" id="Seg_1462" s="T132">top-EP-LOC</ta>
            <ta e="T134" id="Seg_1463" s="T133">sleep-PST.[3SG.S]</ta>
            <ta e="T135" id="Seg_1464" s="T134">all</ta>
            <ta e="T136" id="Seg_1465" s="T135">forget-3SG.O</ta>
            <ta e="T137" id="Seg_1466" s="T136">(s)he.[NOM]</ta>
            <ta e="T138" id="Seg_1467" s="T137">stove.[NOM]</ta>
            <ta e="T139" id="Seg_1468" s="T138">top-EP-EL.3SG</ta>
            <ta e="T140" id="Seg_1469" s="T139">climb.down-3SG.S</ta>
            <ta e="T141" id="Seg_1470" s="T140">(s)he-PL.[NOM]</ta>
            <ta e="T142" id="Seg_1471" s="T141">ask-3PL</ta>
            <ta e="T143" id="Seg_1472" s="T142">grandmother.[NOM]</ta>
            <ta e="T144" id="Seg_1473" s="T143">yesterday</ta>
            <ta e="T145" id="Seg_1474" s="T144">Pjotr.[NOM]</ta>
            <ta e="T146" id="Seg_1475" s="T145">Petrovich-EP-ALL</ta>
            <ta e="T147" id="Seg_1476" s="T146">visiting</ta>
            <ta e="T148" id="Seg_1477" s="T147">go-2SG.S</ta>
            <ta e="T149" id="Seg_1478" s="T148">go-1SG.S</ta>
            <ta e="T150" id="Seg_1479" s="T149">say-3SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1480" s="T1">раньше</ta>
            <ta e="T3" id="Seg_1481" s="T2">Костенкино.[NOM]</ta>
            <ta e="T4" id="Seg_1482" s="T3">деревня-LOC</ta>
            <ta e="T5" id="Seg_1483" s="T4">старуха-DIM.[NOM]</ta>
            <ta e="T6" id="Seg_1484" s="T5">быть-PST.[3SG.S]</ta>
            <ta e="T7" id="Seg_1485" s="T6">он(а)-ALL</ta>
            <ta e="T8" id="Seg_1486" s="T7">быть-PST.[3SG.S]</ta>
            <ta e="T9" id="Seg_1487" s="T8">сто</ta>
            <ta e="T10" id="Seg_1488" s="T9">год.[NOM]-3SG</ta>
            <ta e="T11" id="Seg_1489" s="T10">он(а)-EP-ACC</ta>
            <ta e="T12" id="Seg_1490" s="T11">все.время</ta>
            <ta e="T13" id="Seg_1491" s="T12">обмануть-DUR-HAB-PST-3PL</ta>
            <ta e="T14" id="Seg_1492" s="T13">он(а).[NOM]</ta>
            <ta e="T15" id="Seg_1493" s="T14">все.время</ta>
            <ta e="T16" id="Seg_1494" s="T15">сказка-VBLZ-HAB-PST.[3SG.S]</ta>
            <ta e="T17" id="Seg_1495" s="T16">он(а).[NOM]</ta>
            <ta e="T18" id="Seg_1496" s="T17">сказка-PL-ACC</ta>
            <ta e="T19" id="Seg_1497" s="T18">много-ADVZ</ta>
            <ta e="T20" id="Seg_1498" s="T19">знать-PST-3SG.O</ta>
            <ta e="T21" id="Seg_1499" s="T20">он(а).[NOM]</ta>
            <ta e="T22" id="Seg_1500" s="T21">хороший-ADVZ</ta>
            <ta e="T23" id="Seg_1501" s="T22">сказка-VBLZ-HAB-PST.[3SG.S]</ta>
            <ta e="T24" id="Seg_1502" s="T23">он(а)-ADES</ta>
            <ta e="T25" id="Seg_1503" s="T24">четыре</ta>
            <ta e="T26" id="Seg_1504" s="T25">сын.[NOM]-3SG</ta>
            <ta e="T27" id="Seg_1505" s="T26">быть-PST.[3SG.S]</ta>
            <ta e="T28" id="Seg_1506" s="T27">ребёнок-PL-3SG-ADES</ta>
            <ta e="T29" id="Seg_1507" s="T28">сын-PL.[NOM]-3SG</ta>
            <ta e="T30" id="Seg_1508" s="T29">уже</ta>
            <ta e="T31" id="Seg_1509" s="T30">большой</ta>
            <ta e="T32" id="Seg_1510" s="T31">быть-PST-3PL</ta>
            <ta e="T33" id="Seg_1511" s="T32">он(а).[NOM]</ta>
            <ta e="T34" id="Seg_1512" s="T33">он(а)-PL-ACC</ta>
            <ta e="T35" id="Seg_1513" s="T34">сам.3SG</ta>
            <ta e="T36" id="Seg_1514" s="T35">ребёнок-PL-ACC-3SG</ta>
            <ta e="T37" id="Seg_1515" s="T36">что-INSTR</ta>
            <ta e="T38" id="Seg_1516" s="T37">попало</ta>
            <ta e="T39" id="Seg_1517" s="T38">ударить-INCH-HAB-PST-3SG.O</ta>
            <ta e="T40" id="Seg_1518" s="T39">он(а)-ADES</ta>
            <ta e="T41" id="Seg_1519" s="T40">сила.[NOM]-3SG</ta>
            <ta e="T42" id="Seg_1520" s="T41">если</ta>
            <ta e="T43" id="Seg_1521" s="T42">NEG</ta>
            <ta e="T44" id="Seg_1522" s="T43">быть.в.достатке-HAB-3SG.S</ta>
            <ta e="T45" id="Seg_1523" s="T44">он(а).[NOM]</ta>
            <ta e="T46" id="Seg_1524" s="T45">собрание-ILL</ta>
            <ta e="T47" id="Seg_1525" s="T46">идти-3SG.S</ta>
            <ta e="T48" id="Seg_1526" s="T47">собрание-ILL</ta>
            <ta e="T49" id="Seg_1527" s="T48">прийти-CO.[3SG.S]</ta>
            <ta e="T50" id="Seg_1528" s="T49">сказать-3SG.S</ta>
            <ta e="T51" id="Seg_1529" s="T50">я.GEN</ta>
            <ta e="T52" id="Seg_1530" s="T51">сын.[NOM]-1SG</ta>
            <ta e="T53" id="Seg_1531" s="T52">отстегать-IMP.2PL.S/O</ta>
            <ta e="T54" id="Seg_1532" s="T53">он(а)-EP-ACC</ta>
            <ta e="T55" id="Seg_1533" s="T54">ремень-INSTR</ta>
            <ta e="T56" id="Seg_1534" s="T55">он(а)-PL.[NOM]</ta>
            <ta e="T57" id="Seg_1535" s="T56">отстегать-CO-3PL</ta>
            <ta e="T58" id="Seg_1536" s="T57">потом</ta>
            <ta e="T59" id="Seg_1537" s="T58">сын-PL.[NOM]-3SG</ta>
            <ta e="T60" id="Seg_1538" s="T59">он(а)-ALL</ta>
            <ta e="T61" id="Seg_1539" s="T60">испугаться-DUR-PST-3PL</ta>
            <ta e="T62" id="Seg_1540" s="T61">он(а)-PL.[NOM]</ta>
            <ta e="T63" id="Seg_1541" s="T62">он(а)-ALL</ta>
            <ta e="T64" id="Seg_1542" s="T63">NEG</ta>
            <ta e="T65" id="Seg_1543" s="T64">%%-3PL</ta>
            <ta e="T66" id="Seg_1544" s="T65">он(а).[NOM]</ta>
            <ta e="T67" id="Seg_1545" s="T66">старуха-TRL-CO-3SG.S</ta>
            <ta e="T68" id="Seg_1546" s="T67">и</ta>
            <ta e="T69" id="Seg_1547" s="T68">ум.[NOM]-3SG</ta>
            <ta e="T70" id="Seg_1548" s="T69">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T71" id="Seg_1549" s="T70">он(а)-EP-ACC</ta>
            <ta e="T72" id="Seg_1550" s="T71">обмануть-DUR-HAB-PST-3PL</ta>
            <ta e="T73" id="Seg_1551" s="T72">бабушка.по.материнской.линии.[NOM]</ta>
            <ta e="T74" id="Seg_1552" s="T73">Петр.[NOM]</ta>
            <ta e="T75" id="Seg_1553" s="T74">Петрович-EP-GEN</ta>
            <ta e="T76" id="Seg_1554" s="T75">дом-ILL</ta>
            <ta e="T77" id="Seg_1555" s="T76">отправиться-POT.FUT.2SG</ta>
            <ta e="T78" id="Seg_1556" s="T77">в.гости</ta>
            <ta e="T79" id="Seg_1557" s="T78">а</ta>
            <ta e="T80" id="Seg_1558" s="T79">как</ta>
            <ta e="T81" id="Seg_1559" s="T80">я.NOM</ta>
            <ta e="T82" id="Seg_1560" s="T81">отправиться-FUT-1SG.S</ta>
            <ta e="T83" id="Seg_1561" s="T82">я.NOM</ta>
            <ta e="T84" id="Seg_1562" s="T83">ты.ACC</ta>
            <ta e="T85" id="Seg_1563" s="T84">нарта-DIM-INSTR</ta>
            <ta e="T86" id="Seg_1564" s="T85">отправиться-TR-FUT-1SG.S</ta>
            <ta e="T87" id="Seg_1565" s="T86">ну</ta>
            <ta e="T88" id="Seg_1566" s="T87">отправиться-OPT-1DU</ta>
            <ta e="T89" id="Seg_1567" s="T88">старуха-DIM.[NOM]</ta>
            <ta e="T90" id="Seg_1568" s="T89">одеться-CO-3SG.S</ta>
            <ta e="T91" id="Seg_1569" s="T90">ну</ta>
            <ta e="T92" id="Seg_1570" s="T91">нарта-DIM-ILL</ta>
            <ta e="T93" id="Seg_1571" s="T92">сесть-3SG.S</ta>
            <ta e="T94" id="Seg_1572" s="T93">он(а).[NOM]</ta>
            <ta e="T95" id="Seg_1573" s="T94">он(а)-EP-ACC</ta>
            <ta e="T96" id="Seg_1574" s="T95">дом-EP-ACC</ta>
            <ta e="T97" id="Seg_1575" s="T96">круг-EP-ADVZ</ta>
            <ta e="T98" id="Seg_1576" s="T97">круг-TR-3SG.O</ta>
            <ta e="T99" id="Seg_1577" s="T98">ну</ta>
            <ta e="T100" id="Seg_1578" s="T99">зайти-OPT-1DU</ta>
            <ta e="T101" id="Seg_1579" s="T100">дом-ILL</ta>
            <ta e="T102" id="Seg_1580" s="T101">старуха-DIM.[NOM]</ta>
            <ta e="T103" id="Seg_1581" s="T102">дом-ILL</ta>
            <ta e="T104" id="Seg_1582" s="T103">зайти-CO-3SG.S</ta>
            <ta e="T105" id="Seg_1583" s="T104">бог-ILL</ta>
            <ta e="T106" id="Seg_1584" s="T105">помолиться</ta>
            <ta e="T107" id="Seg_1585" s="T106">начать-FRQ-EP-3SG.S</ta>
            <ta e="T108" id="Seg_1586" s="T107">сказать-3SG.S</ta>
            <ta e="T109" id="Seg_1587" s="T108">здравствуй</ta>
            <ta e="T110" id="Seg_1588" s="T109">Петр.[NOM]</ta>
            <ta e="T111" id="Seg_1589" s="T110">Петрович.[NOM]</ta>
            <ta e="T112" id="Seg_1590" s="T111">как</ta>
            <ta e="T113" id="Seg_1591" s="T112">жить-2SG.S</ta>
            <ta e="T114" id="Seg_1592" s="T113">он(а)-EP-GEN</ta>
            <ta e="T115" id="Seg_1593" s="T114">свой.3SG</ta>
            <ta e="T116" id="Seg_1594" s="T115">дом.[NOM]</ta>
            <ta e="T117" id="Seg_1595" s="T116">а</ta>
            <ta e="T118" id="Seg_1596" s="T117">он(а).[NOM]</ta>
            <ta e="T119" id="Seg_1597" s="T118">NEG</ta>
            <ta e="T120" id="Seg_1598" s="T119">узнать-HAB-3SG.O</ta>
            <ta e="T121" id="Seg_1599" s="T120">он(а).[NOM]</ta>
            <ta e="T122" id="Seg_1600" s="T121">думать.[3SG.S]</ta>
            <ta e="T123" id="Seg_1601" s="T122">в.гости</ta>
            <ta e="T124" id="Seg_1602" s="T123">прийти-PST.NAR-1SG.S</ta>
            <ta e="T125" id="Seg_1603" s="T124">он(а)-ALL</ta>
            <ta e="T126" id="Seg_1604" s="T125">сказать-3PL</ta>
            <ta e="T127" id="Seg_1605" s="T126">печь.[NOM]</ta>
            <ta e="T128" id="Seg_1606" s="T127">верхняя.часть-EP-ILL</ta>
            <ta e="T129" id="Seg_1607" s="T128">залезть-INCH-IMP.2SG.S</ta>
            <ta e="T130" id="Seg_1608" s="T129">спать-IMP.2SG.S</ta>
            <ta e="T131" id="Seg_1609" s="T130">он(а).[NOM]</ta>
            <ta e="T132" id="Seg_1610" s="T131">печь.[NOM]</ta>
            <ta e="T133" id="Seg_1611" s="T132">верхняя.часть-EP-LOC</ta>
            <ta e="T134" id="Seg_1612" s="T133">спать-PST.[3SG.S]</ta>
            <ta e="T135" id="Seg_1613" s="T134">все</ta>
            <ta e="T136" id="Seg_1614" s="T135">забыть-3SG.O</ta>
            <ta e="T137" id="Seg_1615" s="T136">он(а).[NOM]</ta>
            <ta e="T138" id="Seg_1616" s="T137">печь.[NOM]</ta>
            <ta e="T139" id="Seg_1617" s="T138">верхняя.часть-EP-EL.3SG</ta>
            <ta e="T140" id="Seg_1618" s="T139">слезть-3SG.S</ta>
            <ta e="T141" id="Seg_1619" s="T140">он(а)-PL.[NOM]</ta>
            <ta e="T142" id="Seg_1620" s="T141">спросить-3PL</ta>
            <ta e="T143" id="Seg_1621" s="T142">бабушка.по.материнской.линии.[NOM]</ta>
            <ta e="T144" id="Seg_1622" s="T143">вчера</ta>
            <ta e="T145" id="Seg_1623" s="T144">Петр.[NOM]</ta>
            <ta e="T146" id="Seg_1624" s="T145">Петрович-EP-ALL</ta>
            <ta e="T147" id="Seg_1625" s="T146">в.гости</ta>
            <ta e="T148" id="Seg_1626" s="T147">идти-2SG.S</ta>
            <ta e="T149" id="Seg_1627" s="T148">идти-1SG.S</ta>
            <ta e="T150" id="Seg_1628" s="T149">сказать-3SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1629" s="T1">adv</ta>
            <ta e="T3" id="Seg_1630" s="T2">nprop.[n:case]</ta>
            <ta e="T4" id="Seg_1631" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_1632" s="T4">n-n&gt;n.[n:case]</ta>
            <ta e="T6" id="Seg_1633" s="T5">v-v:tense.[v:pn]</ta>
            <ta e="T7" id="Seg_1634" s="T6">pers-n:case</ta>
            <ta e="T8" id="Seg_1635" s="T7">v-v:tense.[v:pn]</ta>
            <ta e="T9" id="Seg_1636" s="T8">num</ta>
            <ta e="T10" id="Seg_1637" s="T9">n.[n:case]-n:poss</ta>
            <ta e="T11" id="Seg_1638" s="T10">pers-n:ins-n:case</ta>
            <ta e="T12" id="Seg_1639" s="T11">adv</ta>
            <ta e="T13" id="Seg_1640" s="T12">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_1641" s="T13">pers.[n:case]</ta>
            <ta e="T15" id="Seg_1642" s="T14">adv</ta>
            <ta e="T16" id="Seg_1643" s="T15">n-n&gt;v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T17" id="Seg_1644" s="T16">pers.[n:case]</ta>
            <ta e="T18" id="Seg_1645" s="T17">n-n:num-n:case</ta>
            <ta e="T19" id="Seg_1646" s="T18">quant-quant&gt;adv</ta>
            <ta e="T20" id="Seg_1647" s="T19">v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_1648" s="T20">pers.[n:case]</ta>
            <ta e="T22" id="Seg_1649" s="T21">adj-adj&gt;adv</ta>
            <ta e="T23" id="Seg_1650" s="T22">n-n&gt;v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T24" id="Seg_1651" s="T23">pers-n:case</ta>
            <ta e="T25" id="Seg_1652" s="T24">num</ta>
            <ta e="T26" id="Seg_1653" s="T25">n.[n:case]-n:poss</ta>
            <ta e="T27" id="Seg_1654" s="T26">v-v:tense.[v:pn]</ta>
            <ta e="T28" id="Seg_1655" s="T27">n-n:num-n:poss-n:case</ta>
            <ta e="T29" id="Seg_1656" s="T28">n-n:num.[n:case]-n:poss</ta>
            <ta e="T30" id="Seg_1657" s="T29">adv</ta>
            <ta e="T31" id="Seg_1658" s="T30">adj</ta>
            <ta e="T32" id="Seg_1659" s="T31">v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_1660" s="T32">pers.[n:case]</ta>
            <ta e="T34" id="Seg_1661" s="T33">pers-n:num-n:case</ta>
            <ta e="T35" id="Seg_1662" s="T34">emphpro</ta>
            <ta e="T36" id="Seg_1663" s="T35">n-n:num-n:case-n:poss</ta>
            <ta e="T37" id="Seg_1664" s="T36">interrog-n:case</ta>
            <ta e="T38" id="Seg_1665" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_1666" s="T38">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_1667" s="T39">pers-n:case</ta>
            <ta e="T41" id="Seg_1668" s="T40">n.[n:case]-n:poss</ta>
            <ta e="T42" id="Seg_1669" s="T41">conj</ta>
            <ta e="T43" id="Seg_1670" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_1671" s="T43">v-v&gt;v-v:pn</ta>
            <ta e="T45" id="Seg_1672" s="T44">pers.[n:case]</ta>
            <ta e="T46" id="Seg_1673" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_1674" s="T46">v-v:pn</ta>
            <ta e="T48" id="Seg_1675" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_1676" s="T48">v-v:ins.[v:pn]</ta>
            <ta e="T50" id="Seg_1677" s="T49">v-v:pn</ta>
            <ta e="T51" id="Seg_1678" s="T50">pers</ta>
            <ta e="T52" id="Seg_1679" s="T51">n.[n:case]-n:poss</ta>
            <ta e="T53" id="Seg_1680" s="T52">v-v:mood.pn</ta>
            <ta e="T54" id="Seg_1681" s="T53">pers-n:ins-n:case</ta>
            <ta e="T55" id="Seg_1682" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_1683" s="T55">pers-n:num.[n:case]</ta>
            <ta e="T57" id="Seg_1684" s="T56">v-v:ins-v:pn</ta>
            <ta e="T58" id="Seg_1685" s="T57">adv</ta>
            <ta e="T59" id="Seg_1686" s="T58">n-n:num.[n:case]-n:poss</ta>
            <ta e="T60" id="Seg_1687" s="T59">pers-n:case</ta>
            <ta e="T61" id="Seg_1688" s="T60">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_1689" s="T61">pers-n:num.[n:case]</ta>
            <ta e="T63" id="Seg_1690" s="T62">pers-n:case</ta>
            <ta e="T64" id="Seg_1691" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_1692" s="T64">v-v:pn</ta>
            <ta e="T66" id="Seg_1693" s="T65">pers.[n:case]</ta>
            <ta e="T67" id="Seg_1694" s="T66">n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T68" id="Seg_1695" s="T67">conj</ta>
            <ta e="T69" id="Seg_1696" s="T68">n.[n:case]-n:poss</ta>
            <ta e="T70" id="Seg_1697" s="T69">v-v:tense.[v:pn]</ta>
            <ta e="T71" id="Seg_1698" s="T70">pers-n:ins-n:case</ta>
            <ta e="T72" id="Seg_1699" s="T71">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T73" id="Seg_1700" s="T72">n.[n:case]</ta>
            <ta e="T74" id="Seg_1701" s="T73">nprop.[n:case]</ta>
            <ta e="T75" id="Seg_1702" s="T74">nprop-n:ins-n:case</ta>
            <ta e="T76" id="Seg_1703" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_1704" s="T76">v-v:tense</ta>
            <ta e="T78" id="Seg_1705" s="T77">adv</ta>
            <ta e="T79" id="Seg_1706" s="T78">conj</ta>
            <ta e="T80" id="Seg_1707" s="T79">interrog</ta>
            <ta e="T81" id="Seg_1708" s="T80">pers</ta>
            <ta e="T82" id="Seg_1709" s="T81">v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_1710" s="T82">pers</ta>
            <ta e="T84" id="Seg_1711" s="T83">pers</ta>
            <ta e="T85" id="Seg_1712" s="T84">n-n&gt;n-n:case</ta>
            <ta e="T86" id="Seg_1713" s="T85">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_1714" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_1715" s="T87">v-v:mood-v:pn</ta>
            <ta e="T89" id="Seg_1716" s="T88">n-n&gt;n.[n:case]</ta>
            <ta e="T90" id="Seg_1717" s="T89">v-v:ins-v:pn</ta>
            <ta e="T91" id="Seg_1718" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_1719" s="T91">n-n&gt;n-n:case</ta>
            <ta e="T93" id="Seg_1720" s="T92">v-v:pn</ta>
            <ta e="T94" id="Seg_1721" s="T93">pers.[n:case]</ta>
            <ta e="T95" id="Seg_1722" s="T94">pers-n:ins-n:case</ta>
            <ta e="T96" id="Seg_1723" s="T95">n-n:ins-n:case</ta>
            <ta e="T97" id="Seg_1724" s="T96">n-n:ins-n&gt;adv</ta>
            <ta e="T98" id="Seg_1725" s="T97">n-n&gt;v-v:pn</ta>
            <ta e="T99" id="Seg_1726" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_1727" s="T99">v-v:mood-v:pn</ta>
            <ta e="T101" id="Seg_1728" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_1729" s="T101">n-n&gt;n.[n:case]</ta>
            <ta e="T103" id="Seg_1730" s="T102">n-n:case</ta>
            <ta e="T104" id="Seg_1731" s="T103">v-v:ins-v:pn</ta>
            <ta e="T105" id="Seg_1732" s="T104">n-n:case</ta>
            <ta e="T106" id="Seg_1733" s="T105">v</ta>
            <ta e="T107" id="Seg_1734" s="T106">v-v&gt;v-n:ins-v:pn</ta>
            <ta e="T108" id="Seg_1735" s="T107">v-v:pn</ta>
            <ta e="T109" id="Seg_1736" s="T108">interj</ta>
            <ta e="T110" id="Seg_1737" s="T109">nprop.[n:case]</ta>
            <ta e="T111" id="Seg_1738" s="T110">nprop.[n:case]</ta>
            <ta e="T112" id="Seg_1739" s="T111">interrog</ta>
            <ta e="T113" id="Seg_1740" s="T112">v-v:pn</ta>
            <ta e="T114" id="Seg_1741" s="T113">pers-n:ins-n:case</ta>
            <ta e="T115" id="Seg_1742" s="T114">emphpro</ta>
            <ta e="T116" id="Seg_1743" s="T115">n.[n:case]</ta>
            <ta e="T117" id="Seg_1744" s="T116">conj</ta>
            <ta e="T118" id="Seg_1745" s="T117">pers.[n:case]</ta>
            <ta e="T119" id="Seg_1746" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_1747" s="T119">v-v&gt;v-v:pn</ta>
            <ta e="T121" id="Seg_1748" s="T120">pers.[n:case]</ta>
            <ta e="T122" id="Seg_1749" s="T121">v.[v:pn]</ta>
            <ta e="T123" id="Seg_1750" s="T122">adv</ta>
            <ta e="T124" id="Seg_1751" s="T123">v-v:tense-v:pn</ta>
            <ta e="T125" id="Seg_1752" s="T124">pers-n:case</ta>
            <ta e="T126" id="Seg_1753" s="T125">v-v:pn</ta>
            <ta e="T127" id="Seg_1754" s="T126">n.[n:case]</ta>
            <ta e="T128" id="Seg_1755" s="T127">n-n:ins-n:case</ta>
            <ta e="T129" id="Seg_1756" s="T128">v-v&gt;v-v:mood.pn</ta>
            <ta e="T130" id="Seg_1757" s="T129">v-v:mood.pn</ta>
            <ta e="T131" id="Seg_1758" s="T130">pers.[n:case]</ta>
            <ta e="T132" id="Seg_1759" s="T131">n.[n:case]</ta>
            <ta e="T133" id="Seg_1760" s="T132">n-n:ins-n:case</ta>
            <ta e="T134" id="Seg_1761" s="T133">v-v:tense.[v:pn]</ta>
            <ta e="T135" id="Seg_1762" s="T134">quant</ta>
            <ta e="T136" id="Seg_1763" s="T135">v-v:pn</ta>
            <ta e="T137" id="Seg_1764" s="T136">pers.[n:case]</ta>
            <ta e="T138" id="Seg_1765" s="T137">n.[n:case]</ta>
            <ta e="T139" id="Seg_1766" s="T138">n-n:ins-n:case.poss</ta>
            <ta e="T140" id="Seg_1767" s="T139">v-v:pn</ta>
            <ta e="T141" id="Seg_1768" s="T140">pers-n:num.[n:case]</ta>
            <ta e="T142" id="Seg_1769" s="T141">v-v:pn</ta>
            <ta e="T143" id="Seg_1770" s="T142">n.[n:case]</ta>
            <ta e="T144" id="Seg_1771" s="T143">adv</ta>
            <ta e="T145" id="Seg_1772" s="T144">nprop.[n:case]</ta>
            <ta e="T146" id="Seg_1773" s="T145">nprop-n:ins-n:case</ta>
            <ta e="T147" id="Seg_1774" s="T146">adv</ta>
            <ta e="T148" id="Seg_1775" s="T147">v-v:pn</ta>
            <ta e="T149" id="Seg_1776" s="T148">v-v:pn</ta>
            <ta e="T150" id="Seg_1777" s="T149">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1778" s="T1">adv</ta>
            <ta e="T3" id="Seg_1779" s="T2">nprop</ta>
            <ta e="T4" id="Seg_1780" s="T3">n</ta>
            <ta e="T5" id="Seg_1781" s="T4">n</ta>
            <ta e="T6" id="Seg_1782" s="T5">v</ta>
            <ta e="T7" id="Seg_1783" s="T6">pers</ta>
            <ta e="T8" id="Seg_1784" s="T7">v</ta>
            <ta e="T9" id="Seg_1785" s="T8">num</ta>
            <ta e="T10" id="Seg_1786" s="T9">n</ta>
            <ta e="T11" id="Seg_1787" s="T10">pers</ta>
            <ta e="T12" id="Seg_1788" s="T11">adv</ta>
            <ta e="T13" id="Seg_1789" s="T12">v</ta>
            <ta e="T14" id="Seg_1790" s="T13">pers</ta>
            <ta e="T15" id="Seg_1791" s="T14">adv</ta>
            <ta e="T16" id="Seg_1792" s="T15">v</ta>
            <ta e="T17" id="Seg_1793" s="T16">pers</ta>
            <ta e="T18" id="Seg_1794" s="T17">n</ta>
            <ta e="T19" id="Seg_1795" s="T18">adv</ta>
            <ta e="T20" id="Seg_1796" s="T19">v</ta>
            <ta e="T21" id="Seg_1797" s="T20">pers</ta>
            <ta e="T22" id="Seg_1798" s="T21">adv</ta>
            <ta e="T23" id="Seg_1799" s="T22">v</ta>
            <ta e="T24" id="Seg_1800" s="T23">pers</ta>
            <ta e="T25" id="Seg_1801" s="T24">num</ta>
            <ta e="T26" id="Seg_1802" s="T25">n</ta>
            <ta e="T27" id="Seg_1803" s="T26">v</ta>
            <ta e="T28" id="Seg_1804" s="T27">n</ta>
            <ta e="T29" id="Seg_1805" s="T28">n</ta>
            <ta e="T30" id="Seg_1806" s="T29">adv</ta>
            <ta e="T31" id="Seg_1807" s="T30">adj</ta>
            <ta e="T32" id="Seg_1808" s="T31">v</ta>
            <ta e="T33" id="Seg_1809" s="T32">pers</ta>
            <ta e="T34" id="Seg_1810" s="T33">pers</ta>
            <ta e="T35" id="Seg_1811" s="T34">emphpro</ta>
            <ta e="T36" id="Seg_1812" s="T35">n</ta>
            <ta e="T37" id="Seg_1813" s="T36">interrog</ta>
            <ta e="T38" id="Seg_1814" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_1815" s="T38">v</ta>
            <ta e="T40" id="Seg_1816" s="T39">pers</ta>
            <ta e="T41" id="Seg_1817" s="T40">n</ta>
            <ta e="T42" id="Seg_1818" s="T41">conj</ta>
            <ta e="T43" id="Seg_1819" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_1820" s="T43">n</ta>
            <ta e="T45" id="Seg_1821" s="T44">pers</ta>
            <ta e="T46" id="Seg_1822" s="T45">n</ta>
            <ta e="T47" id="Seg_1823" s="T46">v</ta>
            <ta e="T48" id="Seg_1824" s="T47">n</ta>
            <ta e="T49" id="Seg_1825" s="T48">v</ta>
            <ta e="T50" id="Seg_1826" s="T49">v</ta>
            <ta e="T51" id="Seg_1827" s="T50">pers</ta>
            <ta e="T52" id="Seg_1828" s="T51">n</ta>
            <ta e="T53" id="Seg_1829" s="T52">v</ta>
            <ta e="T54" id="Seg_1830" s="T53">pers</ta>
            <ta e="T55" id="Seg_1831" s="T54">n</ta>
            <ta e="T56" id="Seg_1832" s="T55">pers</ta>
            <ta e="T57" id="Seg_1833" s="T56">v</ta>
            <ta e="T58" id="Seg_1834" s="T57">adv</ta>
            <ta e="T59" id="Seg_1835" s="T58">n</ta>
            <ta e="T60" id="Seg_1836" s="T59">pers</ta>
            <ta e="T61" id="Seg_1837" s="T60">v</ta>
            <ta e="T62" id="Seg_1838" s="T61">pers</ta>
            <ta e="T63" id="Seg_1839" s="T62">pers</ta>
            <ta e="T64" id="Seg_1840" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_1841" s="T64">v</ta>
            <ta e="T66" id="Seg_1842" s="T65">pers</ta>
            <ta e="T67" id="Seg_1843" s="T66">v</ta>
            <ta e="T68" id="Seg_1844" s="T67">conj</ta>
            <ta e="T69" id="Seg_1845" s="T68">n</ta>
            <ta e="T70" id="Seg_1846" s="T69">v</ta>
            <ta e="T71" id="Seg_1847" s="T70">pers</ta>
            <ta e="T72" id="Seg_1848" s="T71">v</ta>
            <ta e="T73" id="Seg_1849" s="T72">n</ta>
            <ta e="T74" id="Seg_1850" s="T73">nprop</ta>
            <ta e="T75" id="Seg_1851" s="T74">n</ta>
            <ta e="T76" id="Seg_1852" s="T75">n</ta>
            <ta e="T77" id="Seg_1853" s="T76">v</ta>
            <ta e="T78" id="Seg_1854" s="T77">adv</ta>
            <ta e="T79" id="Seg_1855" s="T78">conj</ta>
            <ta e="T80" id="Seg_1856" s="T79">interrog</ta>
            <ta e="T81" id="Seg_1857" s="T80">pers</ta>
            <ta e="T82" id="Seg_1858" s="T81">v</ta>
            <ta e="T83" id="Seg_1859" s="T82">pers</ta>
            <ta e="T84" id="Seg_1860" s="T83">pers</ta>
            <ta e="T85" id="Seg_1861" s="T84">n</ta>
            <ta e="T86" id="Seg_1862" s="T85">v</ta>
            <ta e="T87" id="Seg_1863" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_1864" s="T87">v</ta>
            <ta e="T89" id="Seg_1865" s="T88">n</ta>
            <ta e="T90" id="Seg_1866" s="T89">v</ta>
            <ta e="T91" id="Seg_1867" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_1868" s="T91">n</ta>
            <ta e="T93" id="Seg_1869" s="T92">v</ta>
            <ta e="T94" id="Seg_1870" s="T93">pers</ta>
            <ta e="T95" id="Seg_1871" s="T94">pers</ta>
            <ta e="T96" id="Seg_1872" s="T95">n</ta>
            <ta e="T97" id="Seg_1873" s="T96">adv</ta>
            <ta e="T98" id="Seg_1874" s="T97">v</ta>
            <ta e="T99" id="Seg_1875" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_1876" s="T99">v</ta>
            <ta e="T101" id="Seg_1877" s="T100">n</ta>
            <ta e="T102" id="Seg_1878" s="T101">n</ta>
            <ta e="T103" id="Seg_1879" s="T102">n</ta>
            <ta e="T104" id="Seg_1880" s="T103">v</ta>
            <ta e="T105" id="Seg_1881" s="T104">n</ta>
            <ta e="T106" id="Seg_1882" s="T105">v</ta>
            <ta e="T107" id="Seg_1883" s="T106">v</ta>
            <ta e="T108" id="Seg_1884" s="T107">v</ta>
            <ta e="T109" id="Seg_1885" s="T108">n</ta>
            <ta e="T110" id="Seg_1886" s="T109">nprop</ta>
            <ta e="T111" id="Seg_1887" s="T110">n</ta>
            <ta e="T112" id="Seg_1888" s="T111">interrog</ta>
            <ta e="T113" id="Seg_1889" s="T112">v</ta>
            <ta e="T114" id="Seg_1890" s="T113">pers</ta>
            <ta e="T115" id="Seg_1891" s="T114">emphpro</ta>
            <ta e="T116" id="Seg_1892" s="T115">n</ta>
            <ta e="T117" id="Seg_1893" s="T116">conj</ta>
            <ta e="T118" id="Seg_1894" s="T117">pers</ta>
            <ta e="T119" id="Seg_1895" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_1896" s="T119">v</ta>
            <ta e="T121" id="Seg_1897" s="T120">pers</ta>
            <ta e="T122" id="Seg_1898" s="T121">v</ta>
            <ta e="T123" id="Seg_1899" s="T122">adv</ta>
            <ta e="T124" id="Seg_1900" s="T123">v</ta>
            <ta e="T125" id="Seg_1901" s="T124">pers</ta>
            <ta e="T126" id="Seg_1902" s="T125">v</ta>
            <ta e="T127" id="Seg_1903" s="T126">n</ta>
            <ta e="T128" id="Seg_1904" s="T127">n</ta>
            <ta e="T129" id="Seg_1905" s="T128">v</ta>
            <ta e="T130" id="Seg_1906" s="T129">v</ta>
            <ta e="T131" id="Seg_1907" s="T130">pers</ta>
            <ta e="T132" id="Seg_1908" s="T131">n</ta>
            <ta e="T133" id="Seg_1909" s="T132">n</ta>
            <ta e="T134" id="Seg_1910" s="T133">v</ta>
            <ta e="T135" id="Seg_1911" s="T134">quant</ta>
            <ta e="T136" id="Seg_1912" s="T135">v</ta>
            <ta e="T137" id="Seg_1913" s="T136">pers</ta>
            <ta e="T138" id="Seg_1914" s="T137">n</ta>
            <ta e="T139" id="Seg_1915" s="T138">n</ta>
            <ta e="T140" id="Seg_1916" s="T139">v</ta>
            <ta e="T141" id="Seg_1917" s="T140">pers</ta>
            <ta e="T142" id="Seg_1918" s="T141">v</ta>
            <ta e="T143" id="Seg_1919" s="T142">n</ta>
            <ta e="T144" id="Seg_1920" s="T143">adv</ta>
            <ta e="T145" id="Seg_1921" s="T144">nprop</ta>
            <ta e="T146" id="Seg_1922" s="T145">nprop</ta>
            <ta e="T147" id="Seg_1923" s="T146">adv</ta>
            <ta e="T148" id="Seg_1924" s="T147">v</ta>
            <ta e="T149" id="Seg_1925" s="T148">v</ta>
            <ta e="T150" id="Seg_1926" s="T149">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1927" s="T1">adv:Time</ta>
            <ta e="T4" id="Seg_1928" s="T3">np:L</ta>
            <ta e="T5" id="Seg_1929" s="T4">np.h:Th</ta>
            <ta e="T10" id="Seg_1930" s="T9">np:Th 0.3.h:Poss</ta>
            <ta e="T11" id="Seg_1931" s="T10">pro.h:P</ta>
            <ta e="T12" id="Seg_1932" s="T11">adv:Time</ta>
            <ta e="T13" id="Seg_1933" s="T12">0.3.h:A</ta>
            <ta e="T14" id="Seg_1934" s="T13">pro.h:A</ta>
            <ta e="T15" id="Seg_1935" s="T14">adv:Time</ta>
            <ta e="T17" id="Seg_1936" s="T16">pro.h:E</ta>
            <ta e="T18" id="Seg_1937" s="T17">np:Th</ta>
            <ta e="T21" id="Seg_1938" s="T20">pro.h:A</ta>
            <ta e="T24" id="Seg_1939" s="T23">pro.h:Poss</ta>
            <ta e="T26" id="Seg_1940" s="T25">np.h:Th</ta>
            <ta e="T28" id="Seg_1941" s="T27">np.h:Poss 0.3.h:Poss</ta>
            <ta e="T29" id="Seg_1942" s="T28">np.h:Th</ta>
            <ta e="T33" id="Seg_1943" s="T32">pro.h:A</ta>
            <ta e="T34" id="Seg_1944" s="T33">pro.h:P</ta>
            <ta e="T37" id="Seg_1945" s="T36">pro:Ins</ta>
            <ta e="T40" id="Seg_1946" s="T39">pro.h:Poss</ta>
            <ta e="T41" id="Seg_1947" s="T40">np:Th</ta>
            <ta e="T45" id="Seg_1948" s="T44">pro.h:A</ta>
            <ta e="T46" id="Seg_1949" s="T45">np:G</ta>
            <ta e="T48" id="Seg_1950" s="T47">np:G</ta>
            <ta e="T49" id="Seg_1951" s="T48">0.3.h:A</ta>
            <ta e="T50" id="Seg_1952" s="T49">0.3.h:A</ta>
            <ta e="T51" id="Seg_1953" s="T50">pro.h:Poss</ta>
            <ta e="T52" id="Seg_1954" s="T51">np.h:P</ta>
            <ta e="T53" id="Seg_1955" s="T52">0.2.h:A</ta>
            <ta e="T54" id="Seg_1956" s="T53">pro.h:P</ta>
            <ta e="T55" id="Seg_1957" s="T54">np:Ins</ta>
            <ta e="T56" id="Seg_1958" s="T55">pro.h:A</ta>
            <ta e="T58" id="Seg_1959" s="T57">adv:Time</ta>
            <ta e="T59" id="Seg_1960" s="T58">np.h:E 0.3.h:Poss</ta>
            <ta e="T62" id="Seg_1961" s="T61">pro.h:A</ta>
            <ta e="T66" id="Seg_1962" s="T65">pro.h:P</ta>
            <ta e="T69" id="Seg_1963" s="T68">np:Th 0.3.h:Poss</ta>
            <ta e="T71" id="Seg_1964" s="T70">pro.h:P</ta>
            <ta e="T72" id="Seg_1965" s="T71">0.3.h:A</ta>
            <ta e="T75" id="Seg_1966" s="T74">np.h:Poss</ta>
            <ta e="T76" id="Seg_1967" s="T75">np:G</ta>
            <ta e="T77" id="Seg_1968" s="T76">0.2.h:A</ta>
            <ta e="T81" id="Seg_1969" s="T80">pro.h:A</ta>
            <ta e="T83" id="Seg_1970" s="T82">pro.h:A</ta>
            <ta e="T84" id="Seg_1971" s="T83">pro.h:Th</ta>
            <ta e="T85" id="Seg_1972" s="T84">np:Ins</ta>
            <ta e="T88" id="Seg_1973" s="T87">0.1.h:A</ta>
            <ta e="T89" id="Seg_1974" s="T88">np.h:A</ta>
            <ta e="T92" id="Seg_1975" s="T91">np:G</ta>
            <ta e="T93" id="Seg_1976" s="T92">0.3.h:A</ta>
            <ta e="T94" id="Seg_1977" s="T93">pro.h:A</ta>
            <ta e="T95" id="Seg_1978" s="T94">pro.h:Th</ta>
            <ta e="T100" id="Seg_1979" s="T99">0.1.h:A</ta>
            <ta e="T101" id="Seg_1980" s="T100">np:G</ta>
            <ta e="T102" id="Seg_1981" s="T101">np.h:A</ta>
            <ta e="T103" id="Seg_1982" s="T102">np:G</ta>
            <ta e="T105" id="Seg_1983" s="T104">np.h:R</ta>
            <ta e="T107" id="Seg_1984" s="T106">0.3.h:A</ta>
            <ta e="T108" id="Seg_1985" s="T107">0.3.h:A</ta>
            <ta e="T113" id="Seg_1986" s="T112">0.2.h:Th</ta>
            <ta e="T114" id="Seg_1987" s="T113">pro.h:Poss</ta>
            <ta e="T116" id="Seg_1988" s="T115">0.3:Th</ta>
            <ta e="T118" id="Seg_1989" s="T117">pro.h:E</ta>
            <ta e="T121" id="Seg_1990" s="T120">pro.h:E</ta>
            <ta e="T124" id="Seg_1991" s="T123">0.1.h:A</ta>
            <ta e="T125" id="Seg_1992" s="T124">pro.h:R</ta>
            <ta e="T126" id="Seg_1993" s="T125">0.3.h:A</ta>
            <ta e="T128" id="Seg_1994" s="T127">np:G</ta>
            <ta e="T129" id="Seg_1995" s="T128">0.2.h:A</ta>
            <ta e="T130" id="Seg_1996" s="T129">0.2.h:Th</ta>
            <ta e="T131" id="Seg_1997" s="T130">pro.h:Th</ta>
            <ta e="T133" id="Seg_1998" s="T132">np:L</ta>
            <ta e="T135" id="Seg_1999" s="T134">pro:Th</ta>
            <ta e="T136" id="Seg_2000" s="T135">0.3.h:E</ta>
            <ta e="T137" id="Seg_2001" s="T136">pro.h:A</ta>
            <ta e="T139" id="Seg_2002" s="T138">np:So</ta>
            <ta e="T141" id="Seg_2003" s="T140">pro.h:A</ta>
            <ta e="T144" id="Seg_2004" s="T143">adv:Time</ta>
            <ta e="T146" id="Seg_2005" s="T145">np:G</ta>
            <ta e="T148" id="Seg_2006" s="T147">0.2.h:A</ta>
            <ta e="T149" id="Seg_2007" s="T148">0.1.h:A</ta>
            <ta e="T150" id="Seg_2008" s="T149">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T5" id="Seg_2009" s="T4">np.h:S</ta>
            <ta e="T6" id="Seg_2010" s="T5">v:pred</ta>
            <ta e="T8" id="Seg_2011" s="T7">v:pred</ta>
            <ta e="T10" id="Seg_2012" s="T9">np:S</ta>
            <ta e="T11" id="Seg_2013" s="T10">pro.h:O</ta>
            <ta e="T13" id="Seg_2014" s="T12">0.3.h:S v:pred</ta>
            <ta e="T14" id="Seg_2015" s="T13">pro.h:S</ta>
            <ta e="T16" id="Seg_2016" s="T15">v:pred</ta>
            <ta e="T17" id="Seg_2017" s="T16">pro.h:S</ta>
            <ta e="T18" id="Seg_2018" s="T17">np:O</ta>
            <ta e="T20" id="Seg_2019" s="T19">v:pred</ta>
            <ta e="T21" id="Seg_2020" s="T20">pro.h:S</ta>
            <ta e="T23" id="Seg_2021" s="T22">v:pred</ta>
            <ta e="T26" id="Seg_2022" s="T25">np.h:S</ta>
            <ta e="T27" id="Seg_2023" s="T26">v:pred</ta>
            <ta e="T29" id="Seg_2024" s="T28">np.h:S</ta>
            <ta e="T31" id="Seg_2025" s="T30">adj:pred</ta>
            <ta e="T32" id="Seg_2026" s="T31">cop</ta>
            <ta e="T33" id="Seg_2027" s="T32">pro.h:S</ta>
            <ta e="T34" id="Seg_2028" s="T33">pro.h:O</ta>
            <ta e="T39" id="Seg_2029" s="T38">v:pred</ta>
            <ta e="T44" id="Seg_2030" s="T39">s:cond</ta>
            <ta e="T45" id="Seg_2031" s="T44">pro.h:S</ta>
            <ta e="T47" id="Seg_2032" s="T46">v:pred</ta>
            <ta e="T49" id="Seg_2033" s="T48">0.3.h:S v:pred</ta>
            <ta e="T50" id="Seg_2034" s="T49">0.3.h:S v:pred</ta>
            <ta e="T52" id="Seg_2035" s="T51">np.h:O</ta>
            <ta e="T53" id="Seg_2036" s="T52">0.2.h:S v:pred</ta>
            <ta e="T54" id="Seg_2037" s="T53">pro.h:O</ta>
            <ta e="T56" id="Seg_2038" s="T55">pro.h:S</ta>
            <ta e="T57" id="Seg_2039" s="T56">v:pred</ta>
            <ta e="T59" id="Seg_2040" s="T58">np.h:S</ta>
            <ta e="T61" id="Seg_2041" s="T60">v:pred</ta>
            <ta e="T62" id="Seg_2042" s="T61">pro.h:S</ta>
            <ta e="T65" id="Seg_2043" s="T64">v:pred</ta>
            <ta e="T66" id="Seg_2044" s="T65">pro.h:S</ta>
            <ta e="T67" id="Seg_2045" s="T66">v:pred</ta>
            <ta e="T69" id="Seg_2046" s="T68">np:S</ta>
            <ta e="T70" id="Seg_2047" s="T69">v:pred</ta>
            <ta e="T71" id="Seg_2048" s="T70">pro.h:O</ta>
            <ta e="T72" id="Seg_2049" s="T71">0.3.h:S v:pred</ta>
            <ta e="T77" id="Seg_2050" s="T76">0.2.h:S v:pred</ta>
            <ta e="T81" id="Seg_2051" s="T80">pro.h:S</ta>
            <ta e="T82" id="Seg_2052" s="T81">v:pred</ta>
            <ta e="T83" id="Seg_2053" s="T82">pro.h:S</ta>
            <ta e="T84" id="Seg_2054" s="T83">pro.h:O</ta>
            <ta e="T86" id="Seg_2055" s="T85">v:pred</ta>
            <ta e="T88" id="Seg_2056" s="T87">0.1.h:S v:pred</ta>
            <ta e="T89" id="Seg_2057" s="T88">np.h:S</ta>
            <ta e="T90" id="Seg_2058" s="T89">v:pred</ta>
            <ta e="T93" id="Seg_2059" s="T92">0.3.h:S v:pred</ta>
            <ta e="T94" id="Seg_2060" s="T93">pro.h:S</ta>
            <ta e="T95" id="Seg_2061" s="T94">pro.h:O</ta>
            <ta e="T98" id="Seg_2062" s="T97">v:pred</ta>
            <ta e="T100" id="Seg_2063" s="T99">0.1.h:S v:pred</ta>
            <ta e="T102" id="Seg_2064" s="T101">np.h:S</ta>
            <ta e="T104" id="Seg_2065" s="T103">v:pred</ta>
            <ta e="T107" id="Seg_2066" s="T106">0.3.h:S v:pred</ta>
            <ta e="T108" id="Seg_2067" s="T107">0.3.h:S v:pred</ta>
            <ta e="T113" id="Seg_2068" s="T112">0.2.h:S v:pred</ta>
            <ta e="T116" id="Seg_2069" s="T115">0.3:S n:pred</ta>
            <ta e="T118" id="Seg_2070" s="T117">pro.h:S</ta>
            <ta e="T120" id="Seg_2071" s="T119">v:pred</ta>
            <ta e="T121" id="Seg_2072" s="T120">pro.h:S</ta>
            <ta e="T122" id="Seg_2073" s="T121">v:pred</ta>
            <ta e="T124" id="Seg_2074" s="T123">0.1.h:S v:pred</ta>
            <ta e="T126" id="Seg_2075" s="T125">0.3.h:S v:pred</ta>
            <ta e="T129" id="Seg_2076" s="T128">0.2.h:S v:pred</ta>
            <ta e="T130" id="Seg_2077" s="T129">0.2.h:S v:pred</ta>
            <ta e="T131" id="Seg_2078" s="T130">pro.h:S</ta>
            <ta e="T134" id="Seg_2079" s="T133">v:pred</ta>
            <ta e="T135" id="Seg_2080" s="T134">pro:O</ta>
            <ta e="T136" id="Seg_2081" s="T135">0.3.h:S v:pred</ta>
            <ta e="T137" id="Seg_2082" s="T136">pro.h:S</ta>
            <ta e="T140" id="Seg_2083" s="T139">v:pred</ta>
            <ta e="T141" id="Seg_2084" s="T140">pro.h:S</ta>
            <ta e="T142" id="Seg_2085" s="T141">v:pred</ta>
            <ta e="T148" id="Seg_2086" s="T147">0.2.h:S v:pred</ta>
            <ta e="T149" id="Seg_2087" s="T148">0.1.h:S v:pred</ta>
            <ta e="T150" id="Seg_2088" s="T149">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T30" id="Seg_2089" s="T29">RUS:disc</ta>
            <ta e="T38" id="Seg_2090" s="T37">RUS:gram</ta>
            <ta e="T42" id="Seg_2091" s="T41">RUS:gram</ta>
            <ta e="T46" id="Seg_2092" s="T45">RUS:cult</ta>
            <ta e="T48" id="Seg_2093" s="T47">RUS:cult</ta>
            <ta e="T58" id="Seg_2094" s="T57">RUS:disc</ta>
            <ta e="T68" id="Seg_2095" s="T67">RUS:gram</ta>
            <ta e="T74" id="Seg_2096" s="T73">RUS:cult</ta>
            <ta e="T78" id="Seg_2097" s="T77">RUS:cult</ta>
            <ta e="T79" id="Seg_2098" s="T78">RUS:gram</ta>
            <ta e="T87" id="Seg_2099" s="T86">RUS:disc</ta>
            <ta e="T91" id="Seg_2100" s="T90">RUS:disc</ta>
            <ta e="T99" id="Seg_2101" s="T98">RUS:disc</ta>
            <ta e="T110" id="Seg_2102" s="T109">RUS:cult</ta>
            <ta e="T117" id="Seg_2103" s="T116">RUS:gram</ta>
            <ta e="T123" id="Seg_2104" s="T122">RUS:cult</ta>
            <ta e="T128" id="Seg_2105" s="T127">WNB Noun or pp</ta>
            <ta e="T133" id="Seg_2106" s="T132">WNB Noun or pp</ta>
            <ta e="T135" id="Seg_2107" s="T134">RUS:core</ta>
            <ta e="T139" id="Seg_2108" s="T138">WNB Noun or pp</ta>
            <ta e="T145" id="Seg_2109" s="T144">RUS:cult</ta>
            <ta e="T147" id="Seg_2110" s="T146">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_2111" s="T1">Earlier in Kostenkino there was an old woman.</ta>
            <ta e="T10" id="Seg_2112" s="T6">She was one hundred years old.</ta>
            <ta e="T13" id="Seg_2113" s="T10">She was constantly deceived.</ta>
            <ta e="T16" id="Seg_2114" s="T13">She was telling fairy tales all the time.</ta>
            <ta e="T20" id="Seg_2115" s="T16">She knew a lot of fairy tales.</ta>
            <ta e="T23" id="Seg_2116" s="T20">She was telling fairy tales very well.</ta>
            <ta e="T27" id="Seg_2117" s="T23">She had four sons.</ta>
            <ta e="T32" id="Seg_2118" s="T27">Her childrens' sons were already grown-up.</ta>
            <ta e="T39" id="Seg_2119" s="T32">She beat them (her children) with whatever [she had].</ta>
            <ta e="T47" id="Seg_2120" s="T39">If she hadn't have enough strength, she went to the [village] meeting.</ta>
            <ta e="T53" id="Seg_2121" s="T47">She came to the meeting and said: “Whip my son.”</ta>
            <ta e="T57" id="Seg_2122" s="T53">They whipped him with a rope.</ta>
            <ta e="T61" id="Seg_2123" s="T57">Then her sons were afraid of her.</ta>
            <ta e="T65" id="Seg_2124" s="T61">They didn't bother her.</ta>
            <ta e="T70" id="Seg_2125" s="T65">She became already old and she had little mind.</ta>
            <ta e="T72" id="Seg_2126" s="T70">[People] used to deceive her.</ta>
            <ta e="T78" id="Seg_2127" s="T72">“Grandmother, do you want to visit Pjotr Petrovitch?”</ta>
            <ta e="T82" id="Seg_2128" s="T78">“And how can I go?”</ta>
            <ta e="T86" id="Seg_2129" s="T82">“I'll take you there on the sled.”</ta>
            <ta e="T88" id="Seg_2130" s="T86">“Well, let's go.”</ta>
            <ta e="T93" id="Seg_2131" s="T88">The old woman dressed and sat onto the sled.</ta>
            <ta e="T98" id="Seg_2132" s="T93">He drove her round her house.</ta>
            <ta e="T101" id="Seg_2133" s="T98">“Well, let's enter the house.”</ta>
            <ta e="T104" id="Seg_2134" s="T101">The old woman entered the house.</ta>
            <ta e="T107" id="Seg_2135" s="T104">She started praying.</ta>
            <ta e="T111" id="Seg_2136" s="T107">She said: “Hello, Pjotr Petrovitch.</ta>
            <ta e="T113" id="Seg_2137" s="T111">How are you?”</ta>
            <ta e="T120" id="Seg_2138" s="T113">It's her own house, and she doesn't recognize it.</ta>
            <ta e="T124" id="Seg_2139" s="T120">She thinks: “I came to visit [Pjotr Petrovitch].”</ta>
            <ta e="T130" id="Seg_2140" s="T124">They said to her: “Climb over the stove and sleep there.”</ta>
            <ta e="T134" id="Seg_2141" s="T130">She slept on the stove.</ta>
            <ta e="T136" id="Seg_2142" s="T134">She forgot everything.</ta>
            <ta e="T140" id="Seg_2143" s="T136">She climbed down from the stove.</ta>
            <ta e="T148" id="Seg_2144" s="T140">They asked: “Grandmother, did you visit Pjotr Petrovitch yesterday?”</ta>
            <ta e="T150" id="Seg_2145" s="T148">“I did” – she said.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_2146" s="T1">Früher gab es in Kostenkino eine alte Frau.</ta>
            <ta e="T10" id="Seg_2147" s="T6">Sie war einhundert Jahre alt.</ta>
            <ta e="T13" id="Seg_2148" s="T10">Sie wurde immer wieder betrogen.</ta>
            <ta e="T16" id="Seg_2149" s="T13">Sie erzählte immer ihre Geschichten.</ta>
            <ta e="T20" id="Seg_2150" s="T16">Sie kannte viele Geschichten.</ta>
            <ta e="T23" id="Seg_2151" s="T20">Sie erzählte die Geschichten sehr gut.</ta>
            <ta e="T27" id="Seg_2152" s="T23">Sie hatte vier Söhne.</ta>
            <ta e="T32" id="Seg_2153" s="T27">Die Söhne ihrer Kinder waren schon erwachsen.</ta>
            <ta e="T39" id="Seg_2154" s="T32">Sie schlug sie (ihre Kinder) mit was auch immer [sie hatte].</ta>
            <ta e="T47" id="Seg_2155" s="T39">Wenn sie nicht genügend Kraft hatte, ging sie zum [Dorf]treffen.</ta>
            <ta e="T53" id="Seg_2156" s="T47">Sie kam zum Treffen und sagte: "Schlagt meinen Sohn."</ta>
            <ta e="T57" id="Seg_2157" s="T53">Sie schlugen ihn mit einem Seil.</ta>
            <ta e="T61" id="Seg_2158" s="T57">Dann fürchteten sich ihre Söhne vor ihr.</ta>
            <ta e="T65" id="Seg_2159" s="T61">Sie störten sie nicht.</ta>
            <ta e="T70" id="Seg_2160" s="T65">Sie wurde alt und vergesslich.</ta>
            <ta e="T72" id="Seg_2161" s="T70">[Die Leute] betrogen sie regelmäßig.</ta>
            <ta e="T78" id="Seg_2162" s="T72">"Großmutter, willst du Pjotr Petrovitch besuchen?"</ta>
            <ta e="T82" id="Seg_2163" s="T78">"Und wie komme ich [dorthin]?"</ta>
            <ta e="T86" id="Seg_2164" s="T82">"Ich bringe dich mit dem Schlitten."</ta>
            <ta e="T88" id="Seg_2165" s="T86">"Gut, lass uns gehen."</ta>
            <ta e="T93" id="Seg_2166" s="T88">Die alte Frau zog sich an und setzte sich auf den Schlitten.</ta>
            <ta e="T98" id="Seg_2167" s="T93">Er fuhr sie um das Haus herum.</ta>
            <ta e="T101" id="Seg_2168" s="T98">"Gut, gehen wir ins Haus."</ta>
            <ta e="T104" id="Seg_2169" s="T101">Die alte Frau ging ins Haus.</ta>
            <ta e="T107" id="Seg_2170" s="T104">Sie begann zu beten.</ta>
            <ta e="T111" id="Seg_2171" s="T107">Sie sagt: "Hallo, Pjotr Petrovitch.</ta>
            <ta e="T113" id="Seg_2172" s="T111">Wie geht es dir?"</ta>
            <ta e="T120" id="Seg_2173" s="T113">Es ist ihr eigenes Haus und sie erkennt es nicht.</ta>
            <ta e="T124" id="Seg_2174" s="T120">Sie denkt: "Ich habe [Pjotr Petrovitch] besucht."</ta>
            <ta e="T130" id="Seg_2175" s="T124">Sie sagten ihr: "Kletter auf den Ofen und schlaf dort."</ta>
            <ta e="T134" id="Seg_2176" s="T130">Sie schlief auf dem Ofen.</ta>
            <ta e="T136" id="Seg_2177" s="T134">Sie vergaß alles.</ta>
            <ta e="T140" id="Seg_2178" s="T136">Sie kletterte vom Ofen herunter.</ta>
            <ta e="T148" id="Seg_2179" s="T140">Sie fragten: "Großmutter, hast du gestern Pjotr Petrovitch besucht?"</ta>
            <ta e="T150" id="Seg_2180" s="T148">"Das habe ich" - sagte sie.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_2181" s="T1">Раньше в Костенкино старуха была.</ta>
            <ta e="T10" id="Seg_2182" s="T6">Ей было сто лет.</ta>
            <ta e="T13" id="Seg_2183" s="T10">Ее все время обманывали.</ta>
            <ta e="T16" id="Seg_2184" s="T13">Она все время сказки рассказывала.</ta>
            <ta e="T20" id="Seg_2185" s="T16">Она сказок много знала.</ta>
            <ta e="T23" id="Seg_2186" s="T20">Она сказки хорошо рассказывала.</ta>
            <ta e="T27" id="Seg_2187" s="T23">У нее было четыре сына.</ta>
            <ta e="T32" id="Seg_2188" s="T27">У детей сыновья уже большие были.</ta>
            <ta e="T39" id="Seg_2189" s="T32">Она их (своих детей) чем попало колотила.</ta>
            <ta e="T47" id="Seg_2190" s="T39">Если у нее силы не хватит, она на собрание шла.</ta>
            <ta e="T53" id="Seg_2191" s="T47">На собрание она пришла, сказала: “Моего сына настегайте”.</ta>
            <ta e="T57" id="Seg_2192" s="T53">Его веревкой они стегали.</ta>
            <ta e="T61" id="Seg_2193" s="T57">Потом ее сыновья ее боялись.</ta>
            <ta e="T65" id="Seg_2194" s="T61">Они к ней не лезли(?).</ta>
            <ta e="T70" id="Seg_2195" s="T65">Она старенькая стала, и ума (уже) не было.</ta>
            <ta e="T72" id="Seg_2196" s="T70">Ее обманывали.</ta>
            <ta e="T78" id="Seg_2197" s="T72">“Бабушка, к Петру Петровичу пойдешь (иди) в гости?”</ta>
            <ta e="T82" id="Seg_2198" s="T78">“А как я пойду?”</ta>
            <ta e="T86" id="Seg_2199" s="T82">“Я тебя на нартах увезу (туда)”.</ta>
            <ta e="T88" id="Seg_2200" s="T86">“Ну пойдем”.</ta>
            <ta e="T93" id="Seg_2201" s="T88">Старуха оделась, ну, на нарты села.</ta>
            <ta e="T98" id="Seg_2202" s="T93">Он ее вокруг дома провез.</ta>
            <ta e="T101" id="Seg_2203" s="T98">“Ну, зайдем в дом”.</ta>
            <ta e="T104" id="Seg_2204" s="T101">Старуха в дом зашла.</ta>
            <ta e="T107" id="Seg_2205" s="T104">Богу молиться стала.</ta>
            <ta e="T111" id="Seg_2206" s="T107">Говорит: “Здорово, Петр Петрович!</ta>
            <ta e="T113" id="Seg_2207" s="T111">Как живешь?”</ta>
            <ta e="T120" id="Seg_2208" s="T113">Ее же дом, а она не узнает.</ta>
            <ta e="T124" id="Seg_2209" s="T120">Она думает: “Я в гости пришла”.</ta>
            <ta e="T130" id="Seg_2210" s="T124">Ей говорят: “На печку залезь, (и там) спи”.</ta>
            <ta e="T134" id="Seg_2211" s="T130">Она на печке спала.</ta>
            <ta e="T136" id="Seg_2212" s="T134">Все забыла.</ta>
            <ta e="T140" id="Seg_2213" s="T136">Она с печки слезла.</ta>
            <ta e="T148" id="Seg_2214" s="T140">Они спрашивали: “Бабушка, ты вчера к Петру Петровичу в гости ходила?”</ta>
            <ta e="T150" id="Seg_2215" s="T148">“Ходила”, – говорит.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_2216" s="T1">раньше в Костенкино старуха была</ta>
            <ta e="T10" id="Seg_2217" s="T6">ей было сто лет</ta>
            <ta e="T13" id="Seg_2218" s="T10">ее все время обманывали</ta>
            <ta e="T16" id="Seg_2219" s="T13">она все сказки рассказывала</ta>
            <ta e="T20" id="Seg_2220" s="T16">она сказок много знала</ta>
            <ta e="T23" id="Seg_2221" s="T20">она сказки хорошо рассказывала</ta>
            <ta e="T27" id="Seg_2222" s="T23">у нее было четыре сына</ta>
            <ta e="T32" id="Seg_2223" s="T27">у сыновей дети уже большие были</ta>
            <ta e="T39" id="Seg_2224" s="T32">она их (своих детей) чем попало колотила</ta>
            <ta e="T47" id="Seg_2225" s="T39">если у нее силы не хватит она на собрание пойдет</ta>
            <ta e="T53" id="Seg_2226" s="T47">на собрание она пришла говорит моего сына настегайте</ta>
            <ta e="T57" id="Seg_2227" s="T53">его (с) веревкой стегали</ta>
            <ta e="T61" id="Seg_2228" s="T57">сыновья ее боялись</ta>
            <ta e="T65" id="Seg_2229" s="T61">они к ней не лезли</ta>
            <ta e="T70" id="Seg_2230" s="T65">она старенькая стала и ума уже не было</ta>
            <ta e="T72" id="Seg_2231" s="T70">ее обманывали</ta>
            <ta e="T78" id="Seg_2232" s="T72">бабушка к Петру Петровичу в гости пойдешь</ta>
            <ta e="T82" id="Seg_2233" s="T78">как я пойду</ta>
            <ta e="T86" id="Seg_2234" s="T82">я тебя на салазках увезу (туда)</ta>
            <ta e="T88" id="Seg_2235" s="T86">ну пойдем</ta>
            <ta e="T93" id="Seg_2236" s="T88">старуха оделась на санки села (она)</ta>
            <ta e="T98" id="Seg_2237" s="T93">он ее вокруг дома возит (провез)</ta>
            <ta e="T101" id="Seg_2238" s="T98">но зайдем в избу</ta>
            <ta e="T104" id="Seg_2239" s="T101">старуха в избу зашла</ta>
            <ta e="T107" id="Seg_2240" s="T104">Богу молиться стала</ta>
            <ta e="T111" id="Seg_2241" s="T107">говорит здорово П.П.</ta>
            <ta e="T113" id="Seg_2242" s="T111">как живете</ta>
            <ta e="T120" id="Seg_2243" s="T113">в свою же избу а она и не узнает</ta>
            <ta e="T124" id="Seg_2244" s="T120">она думает я в гости пришла</ta>
            <ta e="T130" id="Seg_2245" s="T124">ей говорят на печку залезь и там спи </ta>
            <ta e="T134" id="Seg_2246" s="T130">она на печке спала</ta>
            <ta e="T136" id="Seg_2247" s="T134">про все забыла</ta>
            <ta e="T140" id="Seg_2248" s="T136">она с печки слезла</ta>
            <ta e="T148" id="Seg_2249" s="T140">они спросили бабушка вчера к Петру Петровичу в гости ходила</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T32" id="Seg_2250" s="T27">[BrM:] The original translation was: "The sons had already grown-up children".</ta>
            <ta e="T53" id="Seg_2251" s="T47">[BrM:] Maybe 'tʼäkku' is a stem for 'to whip'.</ta>
            <ta e="T57" id="Seg_2252" s="T53">[BrM:] Maybe 'tʼäkku' is a stem for 'to whip'.</ta>
            <ta e="T78" id="Seg_2253" s="T72">[BrM:] An imperative form is used in the text, which was originally transalted with interrogative sentence.</ta>
            <ta e="T98" id="Seg_2254" s="T93">[BrM:] Two Accustaive markers.</ta>
            <ta e="T107" id="Seg_2255" s="T104">[BrM:] Unclear construction 'omtə übəran', the form with converb marker would be expected: 'omtə-lä übəran'</ta>
            <ta e="T120" id="Seg_2256" s="T113">[KuAI:] Variant: 'kostuqut'.</ta>
            <ta e="T130" id="Seg_2257" s="T124">[BrM:] Tentative glossing of 'sɨɣɨlga'.</ta>
            <ta e="T148" id="Seg_2258" s="T140">[KuAI:] Variant: 'telʼdʼin'</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
