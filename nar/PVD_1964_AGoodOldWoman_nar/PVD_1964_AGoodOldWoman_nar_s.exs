<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_AGoodOldWoman_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_AGoodOldWoman_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">24</ud-information>
            <ud-information attribute-name="# HIAT:w">17</ud-information>
            <ud-information attribute-name="# e">17</ud-information>
            <ud-information attribute-name="# HIAT:u">4</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T18" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Tɨtdɨn</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6" n="HIAT:ip">(</nts>
                  <ts e="T3" id="Seg_8" n="HIAT:w" s="T2">warkɨs</ts>
                  <nts id="Seg_9" n="HIAT:ip">)</nts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_12" n="HIAT:w" s="T3">ilɨs</ts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_15" n="HIAT:w" s="T4">okɨr</ts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_18" n="HIAT:w" s="T5">pajaga</ts>
                  <nts id="Seg_19" n="HIAT:ip">.</nts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_22" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_24" n="HIAT:w" s="T6">Soːdʼiga</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_27" n="HIAT:w" s="T7">pajaga</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">jes</ts>
                  <nts id="Seg_31" n="HIAT:ip">.</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_34" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">Man</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_39" n="HIAT:w" s="T10">täbɨm</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">sorɨzau</ts>
                  <nts id="Seg_43" n="HIAT:ip">.</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_46" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">Täper</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">täp</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">jedoɣot</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">qwanba</ts>
                  <nts id="Seg_58" n="HIAT:ip">,</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">Maskwatdə</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">qwanba</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T18" id="Seg_67" n="sc" s="T1">
               <ts e="T2" id="Seg_69" n="e" s="T1">Tɨtdɨn </ts>
               <ts e="T3" id="Seg_71" n="e" s="T2">(warkɨs) </ts>
               <ts e="T4" id="Seg_73" n="e" s="T3">ilɨs </ts>
               <ts e="T5" id="Seg_75" n="e" s="T4">okɨr </ts>
               <ts e="T6" id="Seg_77" n="e" s="T5">pajaga. </ts>
               <ts e="T7" id="Seg_79" n="e" s="T6">Soːdʼiga </ts>
               <ts e="T8" id="Seg_81" n="e" s="T7">pajaga </ts>
               <ts e="T9" id="Seg_83" n="e" s="T8">jes. </ts>
               <ts e="T10" id="Seg_85" n="e" s="T9">Man </ts>
               <ts e="T11" id="Seg_87" n="e" s="T10">täbɨm </ts>
               <ts e="T12" id="Seg_89" n="e" s="T11">sorɨzau. </ts>
               <ts e="T13" id="Seg_91" n="e" s="T12">Täper </ts>
               <ts e="T14" id="Seg_93" n="e" s="T13">täp </ts>
               <ts e="T15" id="Seg_95" n="e" s="T14">jedoɣot </ts>
               <ts e="T16" id="Seg_97" n="e" s="T15">qwanba, </ts>
               <ts e="T17" id="Seg_99" n="e" s="T16">Maskwatdə </ts>
               <ts e="T18" id="Seg_101" n="e" s="T17">qwanba. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_102" s="T1">PVD_1964_AGoodOldWoman_nar.001 (001.001)</ta>
            <ta e="T9" id="Seg_103" s="T6">PVD_1964_AGoodOldWoman_nar.002 (001.002)</ta>
            <ta e="T12" id="Seg_104" s="T9">PVD_1964_AGoodOldWoman_nar.003 (001.003)</ta>
            <ta e="T18" id="Seg_105" s="T12">PVD_1964_AGoodOldWoman_nar.004 (001.004)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_106" s="T1">тыт′дын (вар′кыс) и′лыс о′кыр па′jага.</ta>
            <ta e="T9" id="Seg_107" s="T6">′со̄дʼига па′jага jес.</ta>
            <ta e="T12" id="Seg_108" s="T9">ман ′тӓбым ′сорызау.</ta>
            <ta e="T18" id="Seg_109" s="T12">тӓ′пер тӓп ′jедоɣот kwан′ба, Маск′ватдъ kwан′ба.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_110" s="T1">tɨtdɨn (warkɨs) ilɨs okɨr pajaga.</ta>
            <ta e="T9" id="Seg_111" s="T6">soːdʼiga pajaga jes.</ta>
            <ta e="T12" id="Seg_112" s="T9">man täbɨm sorɨzau.</ta>
            <ta e="T18" id="Seg_113" s="T12">täper täp jedoɣot qwanba, Мaskwatdə qwanba.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_114" s="T1">Tɨtdɨn (warkɨs) ilɨs okɨr pajaga. </ta>
            <ta e="T9" id="Seg_115" s="T6">Soːdʼiga pajaga jes. </ta>
            <ta e="T12" id="Seg_116" s="T9">Man täbɨm sorɨzau. </ta>
            <ta e="T18" id="Seg_117" s="T12">Täper täp jedoɣot qwanba, Maskwatdə qwanba. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_118" s="T1">tɨtdɨ-n</ta>
            <ta e="T3" id="Seg_119" s="T2">warkɨ-s</ta>
            <ta e="T4" id="Seg_120" s="T3">elɨ-s</ta>
            <ta e="T5" id="Seg_121" s="T4">okɨr</ta>
            <ta e="T6" id="Seg_122" s="T5">paja-ga</ta>
            <ta e="T7" id="Seg_123" s="T6">soːdʼiga</ta>
            <ta e="T8" id="Seg_124" s="T7">paja-ga</ta>
            <ta e="T9" id="Seg_125" s="T8">je-s</ta>
            <ta e="T10" id="Seg_126" s="T9">man</ta>
            <ta e="T11" id="Seg_127" s="T10">täb-ɨ-m</ta>
            <ta e="T12" id="Seg_128" s="T11">sorɨ-za-u</ta>
            <ta e="T13" id="Seg_129" s="T12">täper</ta>
            <ta e="T14" id="Seg_130" s="T13">täp</ta>
            <ta e="T15" id="Seg_131" s="T14">jedo-ɣot</ta>
            <ta e="T16" id="Seg_132" s="T15">qwan-ba</ta>
            <ta e="T17" id="Seg_133" s="T16">Maskwa-tdə</ta>
            <ta e="T18" id="Seg_134" s="T17">qwan-ba</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_135" s="T1">tɨtʼa-n</ta>
            <ta e="T3" id="Seg_136" s="T2">warkɨ-sɨ</ta>
            <ta e="T4" id="Seg_137" s="T3">elɨ-sɨ</ta>
            <ta e="T5" id="Seg_138" s="T4">okkɨr</ta>
            <ta e="T6" id="Seg_139" s="T5">paja-ka</ta>
            <ta e="T7" id="Seg_140" s="T6">soːdʼiga</ta>
            <ta e="T8" id="Seg_141" s="T7">paja-ka</ta>
            <ta e="T9" id="Seg_142" s="T8">eː-sɨ</ta>
            <ta e="T10" id="Seg_143" s="T9">man</ta>
            <ta e="T11" id="Seg_144" s="T10">täp-ɨ-m</ta>
            <ta e="T12" id="Seg_145" s="T11">soːrɨ-sɨ-w</ta>
            <ta e="T13" id="Seg_146" s="T12">teper</ta>
            <ta e="T14" id="Seg_147" s="T13">täp</ta>
            <ta e="T15" id="Seg_148" s="T14">eːde-qɨn</ta>
            <ta e="T16" id="Seg_149" s="T15">qwan-mbɨ</ta>
            <ta e="T17" id="Seg_150" s="T16">Maskwa-ntə</ta>
            <ta e="T18" id="Seg_151" s="T17">qwan-mbɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_152" s="T1">here-ADV.LOC</ta>
            <ta e="T3" id="Seg_153" s="T2">live-PST.[3SG.S]</ta>
            <ta e="T4" id="Seg_154" s="T3">live-PST.[3SG.S]</ta>
            <ta e="T5" id="Seg_155" s="T4">one</ta>
            <ta e="T6" id="Seg_156" s="T5">old.woman-DIM.[NOM]</ta>
            <ta e="T7" id="Seg_157" s="T6">good</ta>
            <ta e="T8" id="Seg_158" s="T7">old.woman-DIM.[NOM]</ta>
            <ta e="T9" id="Seg_159" s="T8">be-PST.[3SG.S]</ta>
            <ta e="T10" id="Seg_160" s="T9">I.NOM</ta>
            <ta e="T11" id="Seg_161" s="T10">(s)he-EP-ACC</ta>
            <ta e="T12" id="Seg_162" s="T11">love-PST-1SG.O</ta>
            <ta e="T13" id="Seg_163" s="T12">now</ta>
            <ta e="T14" id="Seg_164" s="T13">(s)he.[NOM]</ta>
            <ta e="T15" id="Seg_165" s="T14">village-LOC</ta>
            <ta e="T16" id="Seg_166" s="T15">leave-PST.NAR.[3SG.S]</ta>
            <ta e="T17" id="Seg_167" s="T16">Moscow-ILL</ta>
            <ta e="T18" id="Seg_168" s="T17">leave-PST.NAR.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_169" s="T1">сюда-ADV.LOC</ta>
            <ta e="T3" id="Seg_170" s="T2">жить-PST.[3SG.S]</ta>
            <ta e="T4" id="Seg_171" s="T3">жить-PST.[3SG.S]</ta>
            <ta e="T5" id="Seg_172" s="T4">один</ta>
            <ta e="T6" id="Seg_173" s="T5">старуха-DIM.[NOM]</ta>
            <ta e="T7" id="Seg_174" s="T6">хороший</ta>
            <ta e="T8" id="Seg_175" s="T7">старуха-DIM.[NOM]</ta>
            <ta e="T9" id="Seg_176" s="T8">быть-PST.[3SG.S]</ta>
            <ta e="T10" id="Seg_177" s="T9">я.NOM</ta>
            <ta e="T11" id="Seg_178" s="T10">он(а)-EP-ACC</ta>
            <ta e="T12" id="Seg_179" s="T11">любить-PST-1SG.O</ta>
            <ta e="T13" id="Seg_180" s="T12">теперь</ta>
            <ta e="T14" id="Seg_181" s="T13">он(а).[NOM]</ta>
            <ta e="T15" id="Seg_182" s="T14">деревня-LOC</ta>
            <ta e="T16" id="Seg_183" s="T15">отправиться-PST.NAR.[3SG.S]</ta>
            <ta e="T17" id="Seg_184" s="T16">Москва-ILL</ta>
            <ta e="T18" id="Seg_185" s="T17">отправиться-PST.NAR.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_186" s="T1">adv-adv:case</ta>
            <ta e="T3" id="Seg_187" s="T2">v-v:tense.[v:pn]</ta>
            <ta e="T4" id="Seg_188" s="T3">v-v:tense.[v:pn]</ta>
            <ta e="T5" id="Seg_189" s="T4">num</ta>
            <ta e="T6" id="Seg_190" s="T5">n-n&gt;n.[n:case]</ta>
            <ta e="T7" id="Seg_191" s="T6">adj</ta>
            <ta e="T8" id="Seg_192" s="T7">n-n&gt;n.[n:case]</ta>
            <ta e="T9" id="Seg_193" s="T8">v-v:tense.[v:pn]</ta>
            <ta e="T10" id="Seg_194" s="T9">pers</ta>
            <ta e="T11" id="Seg_195" s="T10">pers-n:ins-n:case</ta>
            <ta e="T12" id="Seg_196" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_197" s="T12">adv</ta>
            <ta e="T14" id="Seg_198" s="T13">pers.[n:case]</ta>
            <ta e="T15" id="Seg_199" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_200" s="T15">v-v:tense.[v:pn]</ta>
            <ta e="T17" id="Seg_201" s="T16">nprop-n:case</ta>
            <ta e="T18" id="Seg_202" s="T17">v-v:tense.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_203" s="T1">v</ta>
            <ta e="T3" id="Seg_204" s="T2">v</ta>
            <ta e="T4" id="Seg_205" s="T3">v</ta>
            <ta e="T5" id="Seg_206" s="T4">num</ta>
            <ta e="T6" id="Seg_207" s="T5">n</ta>
            <ta e="T7" id="Seg_208" s="T6">adj</ta>
            <ta e="T8" id="Seg_209" s="T7">n</ta>
            <ta e="T9" id="Seg_210" s="T8">v</ta>
            <ta e="T10" id="Seg_211" s="T9">pers</ta>
            <ta e="T11" id="Seg_212" s="T10">pers</ta>
            <ta e="T12" id="Seg_213" s="T11">v</ta>
            <ta e="T13" id="Seg_214" s="T12">adv</ta>
            <ta e="T14" id="Seg_215" s="T13">pers</ta>
            <ta e="T15" id="Seg_216" s="T14">n</ta>
            <ta e="T16" id="Seg_217" s="T15">v</ta>
            <ta e="T17" id="Seg_218" s="T16">nprop</ta>
            <ta e="T18" id="Seg_219" s="T17">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_220" s="T1">adv:L</ta>
            <ta e="T6" id="Seg_221" s="T5">np.h:Th</ta>
            <ta e="T9" id="Seg_222" s="T8">0.3.h:Th</ta>
            <ta e="T10" id="Seg_223" s="T9">pro.h:E</ta>
            <ta e="T11" id="Seg_224" s="T10">pro.h:Th</ta>
            <ta e="T13" id="Seg_225" s="T12">adv:Time</ta>
            <ta e="T14" id="Seg_226" s="T13">pro.h:A</ta>
            <ta e="T17" id="Seg_227" s="T16">np:G</ta>
            <ta e="T18" id="Seg_228" s="T17">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_229" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_230" s="T3">v:pred</ta>
            <ta e="T6" id="Seg_231" s="T5">np.h:S</ta>
            <ta e="T8" id="Seg_232" s="T7">n:pred</ta>
            <ta e="T9" id="Seg_233" s="T8">0.3.h:S cop</ta>
            <ta e="T10" id="Seg_234" s="T9">pro.h:S</ta>
            <ta e="T11" id="Seg_235" s="T10">pro.h:O</ta>
            <ta e="T12" id="Seg_236" s="T11">v:pred</ta>
            <ta e="T14" id="Seg_237" s="T13">pro.h:S</ta>
            <ta e="T16" id="Seg_238" s="T15">v:pred</ta>
            <ta e="T18" id="Seg_239" s="T17">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T13" id="Seg_240" s="T12">RUS:core</ta>
            <ta e="T17" id="Seg_241" s="T16">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T17" id="Seg_242" s="T16">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_243" s="T1">Here lived an old woman.</ta>
            <ta e="T9" id="Seg_244" s="T6">She was a good old woman.</ta>
            <ta e="T12" id="Seg_245" s="T9">I loved her.</ta>
            <ta e="T18" id="Seg_246" s="T12">Now she went home, she went to Moscow.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_247" s="T1">Hier lebte eine alte Frau.</ta>
            <ta e="T9" id="Seg_248" s="T6">Sie war eine gute alte Frau.</ta>
            <ta e="T12" id="Seg_249" s="T9">Ich liebte sie.</ta>
            <ta e="T18" id="Seg_250" s="T12">Jetzt ist sie nach Hause gegangen, sie ist nach Moskau gegangen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_251" s="T1">Здесь жила одна старуха.</ta>
            <ta e="T9" id="Seg_252" s="T6">Хорошая старуха была.</ta>
            <ta e="T12" id="Seg_253" s="T9">Я ее любила.</ta>
            <ta e="T18" id="Seg_254" s="T12">Теперь она домой(?) уехала, в Москву уехала.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_255" s="T1">здесь жила одна старуха</ta>
            <ta e="T9" id="Seg_256" s="T6">хорошая старуха была</ta>
            <ta e="T12" id="Seg_257" s="T9">я ее любила</ta>
            <ta e="T18" id="Seg_258" s="T12">теперь она домой уехала в Москву</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T18" id="Seg_259" s="T12">[BrM:] Unusual usage of Locative case in 'jedoɣot'. Elative?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
