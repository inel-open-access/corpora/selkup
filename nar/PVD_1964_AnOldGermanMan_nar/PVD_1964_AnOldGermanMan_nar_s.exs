<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_AnOldGermanMan_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_AnOldGermanMan_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">45</ud-information>
            <ud-information attribute-name="# HIAT:w">35</ud-information>
            <ud-information attribute-name="# e">35</ud-information>
            <ud-information attribute-name="# HIAT:u">9</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T36" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Menan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">ilɨs</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">äraga</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">nʼemʼes</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Täp</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">Ewangelʼim</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">köt</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">ras</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">toɣulǯupat</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_35" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">Udʼigu</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">qaj</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">grʼexnäj</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">tʼäkgu</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_50" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">Tɨtdɨgu</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">krʼex</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_59" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">Qulam</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">sudapbəgu</ts>
                  <nts id="Seg_65" n="HIAT:ip">,</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">čamɨ</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">müzulʼǯugu</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">krʼex</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_78" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">Täp</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">mannan</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">ilɨs</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">agratkaɣɨn</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_93" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">Man</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">täbnä</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">qɨːdan</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">apsodəmɨlam</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">warukuzan</ts>
                  <nts id="Seg_108" n="HIAT:ip">.</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_111" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">Täp</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_116" n="HIAT:w" s="T31">qɨdan</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">arakaj</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">ärɨs</ts>
                  <nts id="Seg_123" n="HIAT:ip">.</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_126" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_128" n="HIAT:w" s="T34">Täpär</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_131" n="HIAT:w" s="T35">qupba</ts>
                  <nts id="Seg_132" n="HIAT:ip">.</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T36" id="Seg_134" n="sc" s="T1">
               <ts e="T2" id="Seg_136" n="e" s="T1">Menan </ts>
               <ts e="T3" id="Seg_138" n="e" s="T2">ilɨs </ts>
               <ts e="T4" id="Seg_140" n="e" s="T3">äraga </ts>
               <ts e="T5" id="Seg_142" n="e" s="T4">nʼemʼes. </ts>
               <ts e="T6" id="Seg_144" n="e" s="T5">Täp </ts>
               <ts e="T7" id="Seg_146" n="e" s="T6">Ewangelʼim </ts>
               <ts e="T8" id="Seg_148" n="e" s="T7">köt </ts>
               <ts e="T9" id="Seg_150" n="e" s="T8">ras </ts>
               <ts e="T10" id="Seg_152" n="e" s="T9">toɣulǯupat. </ts>
               <ts e="T11" id="Seg_154" n="e" s="T10">Udʼigu </ts>
               <ts e="T12" id="Seg_156" n="e" s="T11">qaj </ts>
               <ts e="T13" id="Seg_158" n="e" s="T12">grʼexnäj </ts>
               <ts e="T14" id="Seg_160" n="e" s="T13">tʼäkgu. </ts>
               <ts e="T15" id="Seg_162" n="e" s="T14">Tɨtdɨgu </ts>
               <ts e="T16" id="Seg_164" n="e" s="T15">krʼex. </ts>
               <ts e="T17" id="Seg_166" n="e" s="T16">Qulam </ts>
               <ts e="T18" id="Seg_168" n="e" s="T17">sudapbəgu, </ts>
               <ts e="T19" id="Seg_170" n="e" s="T18">čamɨ </ts>
               <ts e="T20" id="Seg_172" n="e" s="T19">müzulʼǯugu </ts>
               <ts e="T21" id="Seg_174" n="e" s="T20">krʼex. </ts>
               <ts e="T22" id="Seg_176" n="e" s="T21">Täp </ts>
               <ts e="T23" id="Seg_178" n="e" s="T22">mannan </ts>
               <ts e="T24" id="Seg_180" n="e" s="T23">ilɨs </ts>
               <ts e="T25" id="Seg_182" n="e" s="T24">agratkaɣɨn. </ts>
               <ts e="T26" id="Seg_184" n="e" s="T25">Man </ts>
               <ts e="T27" id="Seg_186" n="e" s="T26">täbnä </ts>
               <ts e="T28" id="Seg_188" n="e" s="T27">qɨːdan </ts>
               <ts e="T29" id="Seg_190" n="e" s="T28">apsodəmɨlam </ts>
               <ts e="T30" id="Seg_192" n="e" s="T29">warukuzan. </ts>
               <ts e="T31" id="Seg_194" n="e" s="T30">Täp </ts>
               <ts e="T32" id="Seg_196" n="e" s="T31">qɨdan </ts>
               <ts e="T33" id="Seg_198" n="e" s="T32">arakaj </ts>
               <ts e="T34" id="Seg_200" n="e" s="T33">ärɨs. </ts>
               <ts e="T35" id="Seg_202" n="e" s="T34">Täpär </ts>
               <ts e="T36" id="Seg_204" n="e" s="T35">qupba. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_205" s="T1">PVD_1964_AnOldGermanMan_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_206" s="T5">PVD_1964_AnOldGermanMan_nar.002 (001.002)</ta>
            <ta e="T14" id="Seg_207" s="T10">PVD_1964_AnOldGermanMan_nar.003 (001.003)</ta>
            <ta e="T16" id="Seg_208" s="T14">PVD_1964_AnOldGermanMan_nar.004 (001.004)</ta>
            <ta e="T21" id="Seg_209" s="T16">PVD_1964_AnOldGermanMan_nar.005 (001.005)</ta>
            <ta e="T25" id="Seg_210" s="T21">PVD_1964_AnOldGermanMan_nar.006 (001.006)</ta>
            <ta e="T30" id="Seg_211" s="T25">PVD_1964_AnOldGermanMan_nar.007 (001.007)</ta>
            <ta e="T34" id="Seg_212" s="T30">PVD_1964_AnOldGermanMan_nar.008 (001.008)</ta>
            <ta e="T36" id="Seg_213" s="T34">PVD_1964_AnOldGermanMan_nar.009 (001.009)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_214" s="T1">ме′нан и′лыс ӓ′рага ′нʼемʼес.</ta>
            <ta e="T10" id="Seg_215" s="T5">тӓп е′вангелʼим кӧт рас ′тоɣуlджупат.</ta>
            <ta e="T14" id="Seg_216" s="T10">′удʼигу kай грʼехнӓй ′тʼӓкгу.</ta>
            <ta e="T16" id="Seg_217" s="T14">′тытдыгу ′крʼех.</ta>
            <ta e="T21" id="Seg_218" s="T16">kу′lам ′судапбъгу, ′тшамы мӱзулʼджугу крʼех.</ta>
            <ta e="T25" id="Seg_219" s="T21">тӓп ман′нан и′лыс аг′раткаɣын.</ta>
            <ta e="T30" id="Seg_220" s="T25">ман тӓб′нӓ ′kы̄дан ап′содъмы′лам ′варукузан.</ta>
            <ta e="T34" id="Seg_221" s="T30">тӓп ′kыдан а′ракай ′ӓрыс.</ta>
            <ta e="T36" id="Seg_222" s="T34">тӓ′пӓр ′kупба.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_223" s="T1">menan ilɨs äraga nʼemʼes.</ta>
            <ta e="T10" id="Seg_224" s="T5">täp ewangelʼim köt ras toɣulǯupat.</ta>
            <ta e="T14" id="Seg_225" s="T10">udʼigu qaj grʼexnäj tʼäkgu.</ta>
            <ta e="T16" id="Seg_226" s="T14">tɨtdɨgu krʼex.</ta>
            <ta e="T21" id="Seg_227" s="T16">qulam sudapbəgu, tšamɨ müzulʼǯugu krʼex.</ta>
            <ta e="T25" id="Seg_228" s="T21">täp mannan ilɨs agratkaɣɨn.</ta>
            <ta e="T30" id="Seg_229" s="T25">man täbnä qɨːdan apsodəmɨlam warukuzan.</ta>
            <ta e="T34" id="Seg_230" s="T30">täp qɨdan arakaj ärɨs.</ta>
            <ta e="T36" id="Seg_231" s="T34">täpär qupba.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_232" s="T1">Menan ilɨs äraga nʼemʼes. </ta>
            <ta e="T10" id="Seg_233" s="T5">Täp Ewangelʼim köt ras toɣulǯupat. </ta>
            <ta e="T14" id="Seg_234" s="T10">Udʼigu qaj grʼexnäj tʼäkgu. </ta>
            <ta e="T16" id="Seg_235" s="T14">Tɨtdɨgu krʼex. </ta>
            <ta e="T21" id="Seg_236" s="T16">Qulam sudapbəgu, čamɨ müzulʼǯugu krʼex. </ta>
            <ta e="T25" id="Seg_237" s="T21">Täp mannan ilɨs agratkaɣɨn. </ta>
            <ta e="T30" id="Seg_238" s="T25">Man täbnä qɨːdan apsodəmɨlam warukuzan. </ta>
            <ta e="T34" id="Seg_239" s="T30">Täp qɨdan arakaj ärɨs. </ta>
            <ta e="T36" id="Seg_240" s="T34">Täpär qupba. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_241" s="T1">me-nan</ta>
            <ta e="T3" id="Seg_242" s="T2">elɨ-s</ta>
            <ta e="T4" id="Seg_243" s="T3">ära-ga</ta>
            <ta e="T5" id="Seg_244" s="T4">nʼemʼes</ta>
            <ta e="T6" id="Seg_245" s="T5">täp</ta>
            <ta e="T7" id="Seg_246" s="T6">Ewangelʼi-m</ta>
            <ta e="T8" id="Seg_247" s="T7">köt</ta>
            <ta e="T9" id="Seg_248" s="T8">ras</ta>
            <ta e="T10" id="Seg_249" s="T9">toɣu-lǯu-pa-t</ta>
            <ta e="T11" id="Seg_250" s="T10">udʼi-gu</ta>
            <ta e="T12" id="Seg_251" s="T11">qaj</ta>
            <ta e="T13" id="Seg_252" s="T12">grʼex-näj</ta>
            <ta e="T14" id="Seg_253" s="T13">tʼäkgu</ta>
            <ta e="T15" id="Seg_254" s="T14">tɨtdɨ-gu</ta>
            <ta e="T16" id="Seg_255" s="T15">krʼex</ta>
            <ta e="T17" id="Seg_256" s="T16">qu-la-m</ta>
            <ta e="T18" id="Seg_257" s="T17">suda-pbə-gu</ta>
            <ta e="T19" id="Seg_258" s="T18">čam-ɨ</ta>
            <ta e="T20" id="Seg_259" s="T19">müzulʼǯu-gu</ta>
            <ta e="T21" id="Seg_260" s="T20">krʼex</ta>
            <ta e="T22" id="Seg_261" s="T21">täp</ta>
            <ta e="T23" id="Seg_262" s="T22">man-nan</ta>
            <ta e="T24" id="Seg_263" s="T23">elɨ-s</ta>
            <ta e="T25" id="Seg_264" s="T24">agratka-ɣɨn</ta>
            <ta e="T26" id="Seg_265" s="T25">man</ta>
            <ta e="T27" id="Seg_266" s="T26">täb-nä</ta>
            <ta e="T28" id="Seg_267" s="T27">qɨːdan</ta>
            <ta e="T29" id="Seg_268" s="T28">apsod-ə-mɨ-la-m</ta>
            <ta e="T30" id="Seg_269" s="T29">waru-ku-za-n</ta>
            <ta e="T31" id="Seg_270" s="T30">täp</ta>
            <ta e="T32" id="Seg_271" s="T31">qɨdan</ta>
            <ta e="T33" id="Seg_272" s="T32">araka-j</ta>
            <ta e="T34" id="Seg_273" s="T33">ärɨ-s</ta>
            <ta e="T35" id="Seg_274" s="T34">täpär</ta>
            <ta e="T36" id="Seg_275" s="T35">qu-pba</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_276" s="T1">me-nan</ta>
            <ta e="T3" id="Seg_277" s="T2">elɨ-sɨ</ta>
            <ta e="T4" id="Seg_278" s="T3">era-ka</ta>
            <ta e="T5" id="Seg_279" s="T4">nemiz</ta>
            <ta e="T6" id="Seg_280" s="T5">täp</ta>
            <ta e="T7" id="Seg_281" s="T6">Ewangelʼi-m</ta>
            <ta e="T8" id="Seg_282" s="T7">köt</ta>
            <ta e="T9" id="Seg_283" s="T8">ras</ta>
            <ta e="T10" id="Seg_284" s="T9">*toːqo-lǯi-mbɨ-t</ta>
            <ta e="T11" id="Seg_285" s="T10">uːdʼi-gu</ta>
            <ta e="T12" id="Seg_286" s="T11">qaj</ta>
            <ta e="T13" id="Seg_287" s="T12">grex-näj</ta>
            <ta e="T14" id="Seg_288" s="T13">tʼäkku</ta>
            <ta e="T15" id="Seg_289" s="T14">tɨtdɨ-gu</ta>
            <ta e="T16" id="Seg_290" s="T15">grex</ta>
            <ta e="T17" id="Seg_291" s="T16">qum-la-m</ta>
            <ta e="T18" id="Seg_292" s="T17">sutto-mbɨ-gu</ta>
            <ta e="T19" id="Seg_293" s="T18">čam-ɨ</ta>
            <ta e="T20" id="Seg_294" s="T19">müzulǯu-gu</ta>
            <ta e="T21" id="Seg_295" s="T20">grex</ta>
            <ta e="T22" id="Seg_296" s="T21">täp</ta>
            <ta e="T23" id="Seg_297" s="T22">man-nan</ta>
            <ta e="T24" id="Seg_298" s="T23">elɨ-sɨ</ta>
            <ta e="T25" id="Seg_299" s="T24">agratka-qɨn</ta>
            <ta e="T26" id="Seg_300" s="T25">man</ta>
            <ta e="T27" id="Seg_301" s="T26">täp-nä</ta>
            <ta e="T28" id="Seg_302" s="T27">qɨdan</ta>
            <ta e="T29" id="Seg_303" s="T28">apsod-ɨ-mɨ-la-m</ta>
            <ta e="T30" id="Seg_304" s="T29">warɨ-ku-sɨ-ŋ</ta>
            <ta e="T31" id="Seg_305" s="T30">täp</ta>
            <ta e="T32" id="Seg_306" s="T31">qɨdan</ta>
            <ta e="T33" id="Seg_307" s="T32">araŋka-lʼ</ta>
            <ta e="T34" id="Seg_308" s="T33">öru-sɨ</ta>
            <ta e="T35" id="Seg_309" s="T34">teper</ta>
            <ta e="T36" id="Seg_310" s="T35">quː-mbɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_311" s="T1">we-ADES</ta>
            <ta e="T3" id="Seg_312" s="T2">live-PST.[3SG.S]</ta>
            <ta e="T4" id="Seg_313" s="T3">old.man-DIM.[NOM]</ta>
            <ta e="T5" id="Seg_314" s="T4">German.[NOM]</ta>
            <ta e="T6" id="Seg_315" s="T5">(s)he.[NOM]</ta>
            <ta e="T7" id="Seg_316" s="T6">Gospel-ACC</ta>
            <ta e="T8" id="Seg_317" s="T7">ten</ta>
            <ta e="T9" id="Seg_318" s="T8">time</ta>
            <ta e="T10" id="Seg_319" s="T9">read-TR-PST.NAR-3SG.O</ta>
            <ta e="T11" id="Seg_320" s="T10">work-INF</ta>
            <ta e="T12" id="Seg_321" s="T11">what.[NOM]</ta>
            <ta e="T13" id="Seg_322" s="T12">sin.[NOM]-EMPH</ta>
            <ta e="T14" id="Seg_323" s="T13">NEG.EX.[3SG.S]</ta>
            <ta e="T15" id="Seg_324" s="T14">swear-INF</ta>
            <ta e="T16" id="Seg_325" s="T15">sin.[3SG.S]</ta>
            <ta e="T17" id="Seg_326" s="T16">human.being-PL-ACC</ta>
            <ta e="T18" id="Seg_327" s="T17">judge-DUR-INF</ta>
            <ta e="T19" id="Seg_328" s="T18">dirt-EP.[NOM]</ta>
            <ta e="T20" id="Seg_329" s="T19">wash-INF</ta>
            <ta e="T21" id="Seg_330" s="T20">sin.[3SG.S]</ta>
            <ta e="T22" id="Seg_331" s="T21">(s)he.[NOM]</ta>
            <ta e="T23" id="Seg_332" s="T22">I-ADES</ta>
            <ta e="T24" id="Seg_333" s="T23">live-PST.[3SG.S]</ta>
            <ta e="T25" id="Seg_334" s="T24">fence-LOC</ta>
            <ta e="T26" id="Seg_335" s="T25">I.NOM</ta>
            <ta e="T27" id="Seg_336" s="T26">(s)he-ALL</ta>
            <ta e="T28" id="Seg_337" s="T27">all.the.time</ta>
            <ta e="T29" id="Seg_338" s="T28">food-EP-something-PL-ACC</ta>
            <ta e="T30" id="Seg_339" s="T29">have-HAB-PST-1SG.S</ta>
            <ta e="T31" id="Seg_340" s="T30">(s)he.[NOM]</ta>
            <ta e="T32" id="Seg_341" s="T31">all.the.time</ta>
            <ta e="T33" id="Seg_342" s="T32">wine-ADJZ</ta>
            <ta e="T34" id="Seg_343" s="T33">drink-PST.[3SG.S]</ta>
            <ta e="T35" id="Seg_344" s="T34">now</ta>
            <ta e="T36" id="Seg_345" s="T35">die-PST.NAR.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_346" s="T1">мы-ADES</ta>
            <ta e="T3" id="Seg_347" s="T2">жить-PST.[3SG.S]</ta>
            <ta e="T4" id="Seg_348" s="T3">старик-DIM.[NOM]</ta>
            <ta e="T5" id="Seg_349" s="T4">немец.[NOM]</ta>
            <ta e="T6" id="Seg_350" s="T5">он(а).[NOM]</ta>
            <ta e="T7" id="Seg_351" s="T6">Евангелие-ACC</ta>
            <ta e="T8" id="Seg_352" s="T7">десять</ta>
            <ta e="T9" id="Seg_353" s="T8">раз</ta>
            <ta e="T10" id="Seg_354" s="T9">читать-TR-PST.NAR-3SG.O</ta>
            <ta e="T11" id="Seg_355" s="T10">работать-INF</ta>
            <ta e="T12" id="Seg_356" s="T11">что.[NOM]</ta>
            <ta e="T13" id="Seg_357" s="T12">грех.[NOM]-EMPH</ta>
            <ta e="T14" id="Seg_358" s="T13">NEG.EX.[3SG.S]</ta>
            <ta e="T15" id="Seg_359" s="T14">поругаться-INF</ta>
            <ta e="T16" id="Seg_360" s="T15">грех.[3SG.S]</ta>
            <ta e="T17" id="Seg_361" s="T16">человек-PL-ACC</ta>
            <ta e="T18" id="Seg_362" s="T17">судить-DUR-INF</ta>
            <ta e="T19" id="Seg_363" s="T18">грязь-EP.[NOM]</ta>
            <ta e="T20" id="Seg_364" s="T19">вымыть-INF</ta>
            <ta e="T21" id="Seg_365" s="T20">грех.[3SG.S]</ta>
            <ta e="T22" id="Seg_366" s="T21">он(а).[NOM]</ta>
            <ta e="T23" id="Seg_367" s="T22">я-ADES</ta>
            <ta e="T24" id="Seg_368" s="T23">жить-PST.[3SG.S]</ta>
            <ta e="T25" id="Seg_369" s="T24">ограда-LOC</ta>
            <ta e="T26" id="Seg_370" s="T25">я.NOM</ta>
            <ta e="T27" id="Seg_371" s="T26">он(а)-ALL</ta>
            <ta e="T28" id="Seg_372" s="T27">все.время</ta>
            <ta e="T29" id="Seg_373" s="T28">еда-EP-нечто-PL-ACC</ta>
            <ta e="T30" id="Seg_374" s="T29">иметь-HAB-PST-1SG.S</ta>
            <ta e="T31" id="Seg_375" s="T30">он(а).[NOM]</ta>
            <ta e="T32" id="Seg_376" s="T31">все.время</ta>
            <ta e="T33" id="Seg_377" s="T32">вино-ADJZ</ta>
            <ta e="T34" id="Seg_378" s="T33">пить-PST.[3SG.S]</ta>
            <ta e="T35" id="Seg_379" s="T34">теперь</ta>
            <ta e="T36" id="Seg_380" s="T35">умереть-PST.NAR.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_381" s="T1">pers-n:case</ta>
            <ta e="T3" id="Seg_382" s="T2">v-v:tense.[v:pn]</ta>
            <ta e="T4" id="Seg_383" s="T3">n-n&gt;n.[n:case]</ta>
            <ta e="T5" id="Seg_384" s="T4">n.[n:case]</ta>
            <ta e="T6" id="Seg_385" s="T5">pers.[n:case]</ta>
            <ta e="T7" id="Seg_386" s="T6">nprop-n:case</ta>
            <ta e="T8" id="Seg_387" s="T7">num</ta>
            <ta e="T9" id="Seg_388" s="T8">n</ta>
            <ta e="T10" id="Seg_389" s="T9">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_390" s="T10">v-v:inf</ta>
            <ta e="T12" id="Seg_391" s="T11">interrog.[n:case]</ta>
            <ta e="T13" id="Seg_392" s="T12">n.[n:case]-clit</ta>
            <ta e="T14" id="Seg_393" s="T13">v.[v:pn]</ta>
            <ta e="T15" id="Seg_394" s="T14">v-v:inf</ta>
            <ta e="T16" id="Seg_395" s="T15">n.[v:pn]</ta>
            <ta e="T17" id="Seg_396" s="T16">n-n:num-n:case</ta>
            <ta e="T18" id="Seg_397" s="T17">v-v&gt;v-v:inf</ta>
            <ta e="T19" id="Seg_398" s="T18">n-n:ins.[n:case]</ta>
            <ta e="T20" id="Seg_399" s="T19">v-v:inf</ta>
            <ta e="T21" id="Seg_400" s="T20">n.[v:pn]</ta>
            <ta e="T22" id="Seg_401" s="T21">pers.[n:case]</ta>
            <ta e="T23" id="Seg_402" s="T22">pers-n:case</ta>
            <ta e="T24" id="Seg_403" s="T23">v-v:tense.[v:pn]</ta>
            <ta e="T25" id="Seg_404" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_405" s="T25">pers</ta>
            <ta e="T27" id="Seg_406" s="T26">pers-n:case</ta>
            <ta e="T28" id="Seg_407" s="T27">adv</ta>
            <ta e="T29" id="Seg_408" s="T28">n-n:ins-n-n:num-n:case</ta>
            <ta e="T30" id="Seg_409" s="T29">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_410" s="T30">pers.[n:case]</ta>
            <ta e="T32" id="Seg_411" s="T31">adv</ta>
            <ta e="T33" id="Seg_412" s="T32">n-n&gt;adj</ta>
            <ta e="T34" id="Seg_413" s="T33">v-v:tense.[v:pn]</ta>
            <ta e="T35" id="Seg_414" s="T34">adv</ta>
            <ta e="T36" id="Seg_415" s="T35">v-v:tense.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_416" s="T1">pers</ta>
            <ta e="T3" id="Seg_417" s="T2">v</ta>
            <ta e="T4" id="Seg_418" s="T3">n</ta>
            <ta e="T5" id="Seg_419" s="T4">nprop</ta>
            <ta e="T6" id="Seg_420" s="T5">pers</ta>
            <ta e="T7" id="Seg_421" s="T6">nprop</ta>
            <ta e="T8" id="Seg_422" s="T7">num</ta>
            <ta e="T9" id="Seg_423" s="T8">n</ta>
            <ta e="T10" id="Seg_424" s="T9">v</ta>
            <ta e="T11" id="Seg_425" s="T10">v</ta>
            <ta e="T12" id="Seg_426" s="T11">interrog</ta>
            <ta e="T13" id="Seg_427" s="T12">n</ta>
            <ta e="T14" id="Seg_428" s="T13">v</ta>
            <ta e="T15" id="Seg_429" s="T14">v</ta>
            <ta e="T16" id="Seg_430" s="T15">n</ta>
            <ta e="T17" id="Seg_431" s="T16">n</ta>
            <ta e="T18" id="Seg_432" s="T17">v</ta>
            <ta e="T19" id="Seg_433" s="T18">n</ta>
            <ta e="T20" id="Seg_434" s="T19">v</ta>
            <ta e="T21" id="Seg_435" s="T20">n</ta>
            <ta e="T22" id="Seg_436" s="T21">pers</ta>
            <ta e="T23" id="Seg_437" s="T22">pers</ta>
            <ta e="T24" id="Seg_438" s="T23">v</ta>
            <ta e="T25" id="Seg_439" s="T24">n</ta>
            <ta e="T26" id="Seg_440" s="T25">pers</ta>
            <ta e="T27" id="Seg_441" s="T26">pers</ta>
            <ta e="T28" id="Seg_442" s="T27">adv</ta>
            <ta e="T29" id="Seg_443" s="T28">n</ta>
            <ta e="T30" id="Seg_444" s="T29">v</ta>
            <ta e="T31" id="Seg_445" s="T30">pers</ta>
            <ta e="T32" id="Seg_446" s="T31">adv</ta>
            <ta e="T33" id="Seg_447" s="T32">adj</ta>
            <ta e="T34" id="Seg_448" s="T33">v</ta>
            <ta e="T35" id="Seg_449" s="T34">adv</ta>
            <ta e="T36" id="Seg_450" s="T35">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_451" s="T1">np.h:L</ta>
            <ta e="T5" id="Seg_452" s="T4">np.h:Th</ta>
            <ta e="T6" id="Seg_453" s="T5">pro.h:A</ta>
            <ta e="T7" id="Seg_454" s="T6">np:Th</ta>
            <ta e="T11" id="Seg_455" s="T10">v:Th</ta>
            <ta e="T15" id="Seg_456" s="T14">v:Th</ta>
            <ta e="T17" id="Seg_457" s="T16">np.h:Th</ta>
            <ta e="T18" id="Seg_458" s="T17">v:Th</ta>
            <ta e="T19" id="Seg_459" s="T18">np:Th</ta>
            <ta e="T20" id="Seg_460" s="T19">v:Th</ta>
            <ta e="T22" id="Seg_461" s="T21">pro.h:Th</ta>
            <ta e="T23" id="Seg_462" s="T22">np.h:L</ta>
            <ta e="T25" id="Seg_463" s="T24">np:L</ta>
            <ta e="T26" id="Seg_464" s="T25">pro.h:A</ta>
            <ta e="T27" id="Seg_465" s="T26">pro.h:R</ta>
            <ta e="T28" id="Seg_466" s="T27">adv:Time</ta>
            <ta e="T29" id="Seg_467" s="T28">np:Th</ta>
            <ta e="T31" id="Seg_468" s="T30">pro.h:A</ta>
            <ta e="T32" id="Seg_469" s="T31">adv:Time</ta>
            <ta e="T33" id="Seg_470" s="T32">np:P</ta>
            <ta e="T35" id="Seg_471" s="T34">adv:Time</ta>
            <ta e="T36" id="Seg_472" s="T35">0.3.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_473" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_474" s="T4">np.h:S</ta>
            <ta e="T6" id="Seg_475" s="T5">pro.h:S</ta>
            <ta e="T7" id="Seg_476" s="T6">np:O</ta>
            <ta e="T10" id="Seg_477" s="T9">v:pred</ta>
            <ta e="T11" id="Seg_478" s="T10">v:S</ta>
            <ta e="T14" id="Seg_479" s="T13">v:pred</ta>
            <ta e="T15" id="Seg_480" s="T14">v:S</ta>
            <ta e="T16" id="Seg_481" s="T15">n:pred</ta>
            <ta e="T18" id="Seg_482" s="T17">v:S</ta>
            <ta e="T20" id="Seg_483" s="T19">v:S</ta>
            <ta e="T21" id="Seg_484" s="T20">n:pred</ta>
            <ta e="T22" id="Seg_485" s="T21">pro.h:S</ta>
            <ta e="T24" id="Seg_486" s="T23">v:pred</ta>
            <ta e="T26" id="Seg_487" s="T25">pro.h:S</ta>
            <ta e="T29" id="Seg_488" s="T28">np:O</ta>
            <ta e="T30" id="Seg_489" s="T29">v:pred</ta>
            <ta e="T31" id="Seg_490" s="T30">pro.h:S</ta>
            <ta e="T33" id="Seg_491" s="T32">np:O</ta>
            <ta e="T34" id="Seg_492" s="T33">v:pred</ta>
            <ta e="T36" id="Seg_493" s="T35">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_494" s="T4">RUS:cult</ta>
            <ta e="T7" id="Seg_495" s="T6">RUS:cult</ta>
            <ta e="T9" id="Seg_496" s="T8">RUS:core</ta>
            <ta e="T13" id="Seg_497" s="T12">RUS:cult</ta>
            <ta e="T16" id="Seg_498" s="T15">RUS:cult</ta>
            <ta e="T21" id="Seg_499" s="T20">RUS:cult</ta>
            <ta e="T25" id="Seg_500" s="T24">RUS:cult</ta>
            <ta e="T33" id="Seg_501" s="T32">TURK:cult</ta>
            <ta e="T35" id="Seg_502" s="T34">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T16" id="Seg_503" s="T15">Csub</ta>
            <ta e="T21" id="Seg_504" s="T20">Csub</ta>
            <ta e="T25" id="Seg_505" s="T24">medCins Csub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T5" id="Seg_506" s="T4">dir:bare</ta>
            <ta e="T13" id="Seg_507" s="T12">dir:infl</ta>
            <ta e="T16" id="Seg_508" s="T15">dir:bare</ta>
            <ta e="T21" id="Seg_509" s="T20">dir:infl</ta>
            <ta e="T25" id="Seg_510" s="T24">dir:infl</ta>
            <ta e="T33" id="Seg_511" s="T32">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_512" s="T1">A German old man used to live with us.</ta>
            <ta e="T10" id="Seg_513" s="T5">He read the Gospel ten times.</ta>
            <ta e="T14" id="Seg_514" s="T10">It's no sin to work.</ta>
            <ta e="T16" id="Seg_515" s="T14">Using foul language is a sin.</ta>
            <ta e="T21" id="Seg_516" s="T16">Judging people, washing dirt is a sin.</ta>
            <ta e="T25" id="Seg_517" s="T21">He lived near me.</ta>
            <ta e="T30" id="Seg_518" s="T25">I used to bring him food.</ta>
            <ta e="T34" id="Seg_519" s="T30">He was drinking all the time.</ta>
            <ta e="T36" id="Seg_520" s="T34">Now he is dead.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_521" s="T1">Bei uns lebte ein alter deutscher Mann.</ta>
            <ta e="T10" id="Seg_522" s="T5">Er las das Evangelium zehn Mal.</ta>
            <ta e="T14" id="Seg_523" s="T10">Zu arbeiten ist keine Sünde.</ta>
            <ta e="T16" id="Seg_524" s="T14">Zu fluchen ist eine Sünde.</ta>
            <ta e="T21" id="Seg_525" s="T16">Über Menschen zu urteilen, Dreck zu waschen, ist Sünde.</ta>
            <ta e="T25" id="Seg_526" s="T21">Er lebte in meiner Nähe.</ta>
            <ta e="T30" id="Seg_527" s="T25">Ich brachte ihm für gewöhnlich Essen.</ta>
            <ta e="T34" id="Seg_528" s="T30">Er trank die ganze Zeit.</ta>
            <ta e="T36" id="Seg_529" s="T34">Jetzt ist er tot.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_530" s="T1">У нас жил старик-немец.</ta>
            <ta e="T10" id="Seg_531" s="T5">Он Евангелие десять раз читал.</ta>
            <ta e="T14" id="Seg_532" s="T10">Работать никакого греха нет.</ta>
            <ta e="T16" id="Seg_533" s="T14">Материться – грех.</ta>
            <ta e="T21" id="Seg_534" s="T16">Людей судить, грязь мыть – грех.</ta>
            <ta e="T25" id="Seg_535" s="T21">Он у меня жил в ограде.</ta>
            <ta e="T30" id="Seg_536" s="T25">Я ему все есть таскала.</ta>
            <ta e="T34" id="Seg_537" s="T30">Он все время вино пил.</ta>
            <ta e="T36" id="Seg_538" s="T34">Теперь он умер.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_539" s="T1">у нас жил старик-немец</ta>
            <ta e="T10" id="Seg_540" s="T5">он Евангелие десять раз читал</ta>
            <ta e="T14" id="Seg_541" s="T10">работать никакого греха нет</ta>
            <ta e="T16" id="Seg_542" s="T14">материться грех</ta>
            <ta e="T21" id="Seg_543" s="T16">людей судить грех грязь мыть грех</ta>
            <ta e="T25" id="Seg_544" s="T21">он у нас жил в ограде</ta>
            <ta e="T30" id="Seg_545" s="T25">я все ему есть таскала</ta>
            <ta e="T34" id="Seg_546" s="T30">он все время вино пил</ta>
            <ta e="T36" id="Seg_547" s="T34">теперь умер</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T21" id="Seg_548" s="T16">[BrM:] Unexpected form 'čamɨ'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
