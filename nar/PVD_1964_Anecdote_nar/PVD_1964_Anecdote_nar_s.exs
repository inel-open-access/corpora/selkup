<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Anecdote_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Anecdote_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">95</ud-information>
            <ud-information attribute-name="# HIAT:w">64</ud-information>
            <ud-information attribute-name="# e">64</ud-information>
            <ud-information attribute-name="# HIAT:u">14</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T65" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Meguntu</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">satdə</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">učʼitʼelʼ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">tʼütdə</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Menan</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">süsögula</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">kocʼin</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">jewattə</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_32" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">A</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">tep</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">tʼarɨn</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">okkɨr</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">nejɣumnä</ts>
                  <nts id="Seg_47" n="HIAT:ip">:</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_49" n="HIAT:ip">“</nts>
                  <ts e="T15" id="Seg_51" n="HIAT:w" s="T14">Mazɨm</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_54" n="HIAT:w" s="T15">oɣulǯoʒaq</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_57" n="HIAT:w" s="T16">tʼolomčugu</ts>
                  <nts id="Seg_58" n="HIAT:ip">.</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_61" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_63" n="HIAT:w" s="T17">Wes</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_66" n="HIAT:w" s="T18">nejgulazʼe</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_69" n="HIAT:w" s="T19">tʼolomčeǯan</ts>
                  <nts id="Seg_70" n="HIAT:ip">.</nts>
                  <nts id="Seg_71" n="HIAT:ip">”</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_74" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">A</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">tep</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">tebɨm</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">oɣulǯoʒɨt</ts>
                  <nts id="Seg_86" n="HIAT:ip">:</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_88" n="HIAT:ip">“</nts>
                  <ts e="T25" id="Seg_90" n="HIAT:w" s="T24">Maːtku</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_93" n="HIAT:w" s="T25">talʼel</ts>
                  <nts id="Seg_94" n="HIAT:ip">.</nts>
                  <nts id="Seg_95" n="HIAT:ip">”</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_98" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_100" n="HIAT:w" s="T26">I</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">tep</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">na</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_109" n="HIAT:w" s="T29">kurolǯa</ts>
                  <nts id="Seg_110" n="HIAT:ip">.</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_113" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">Pajagan</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_118" n="HIAT:w" s="T31">maːttɨ</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_121" n="HIAT:w" s="T32">sernɨn</ts>
                  <nts id="Seg_122" n="HIAT:ip">.</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_125" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">Pajaga</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">bɨlʼinai</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">meʒɨrɨn</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_137" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_139" n="HIAT:w" s="T36">A</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_142" n="HIAT:w" s="T37">učʼitelʼ</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_145" n="HIAT:w" s="T38">tʼarɨn</ts>
                  <nts id="Seg_146" n="HIAT:ip">:</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_148" n="HIAT:ip">“</nts>
                  <ts e="T40" id="Seg_150" n="HIAT:w" s="T39">Pajaga</ts>
                  <nts id="Seg_151" n="HIAT:ip">,</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_154" n="HIAT:w" s="T40">matku</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_157" n="HIAT:w" s="T41">dalʼel</ts>
                  <nts id="Seg_158" n="HIAT:ip">!</nts>
                  <nts id="Seg_159" n="HIAT:ip">”</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_162" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_164" n="HIAT:w" s="T42">Pajaga</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_167" n="HIAT:w" s="T43">kak</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_170" n="HIAT:w" s="T44">qwɛdɨpɨlʼe</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_173" n="HIAT:w" s="T45">jübɨratdə</ts>
                  <nts id="Seg_174" n="HIAT:ip">.</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_177" n="HIAT:u" s="T46">
                  <nts id="Seg_178" n="HIAT:ip">“</nts>
                  <ts e="T47" id="Seg_180" n="HIAT:w" s="T46">Tan</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_183" n="HIAT:w" s="T47">učʼitʼelʼwatdə</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_186" n="HIAT:w" s="T48">ešo</ts>
                  <nts id="Seg_187" n="HIAT:ip">,</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_190" n="HIAT:w" s="T49">manan</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_193" n="HIAT:w" s="T50">maːderlʼe</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_196" n="HIAT:w" s="T51">jübɨrattə</ts>
                  <nts id="Seg_197" n="HIAT:ip">.</nts>
                  <nts id="Seg_198" n="HIAT:ip">”</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_201" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_203" n="HIAT:w" s="T52">Kak</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_206" n="HIAT:w" s="T53">skawarodnikam</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_209" n="HIAT:w" s="T54">meʒalʒit</ts>
                  <nts id="Seg_210" n="HIAT:ip">.</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_213" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_215" n="HIAT:w" s="T55">Täbɨm</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_217" n="HIAT:ip">(</nts>
                  <ts e="T57" id="Seg_219" n="HIAT:w" s="T56">mazɨm</ts>
                  <nts id="Seg_220" n="HIAT:ip">)</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_223" n="HIAT:w" s="T57">qaːlʼe</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_226" n="HIAT:w" s="T58">kwatdɨn</ts>
                  <nts id="Seg_227" n="HIAT:ip">.</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_230" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_232" n="HIAT:w" s="T59">A</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_234" n="HIAT:ip">(</nts>
                  <ts e="T61" id="Seg_236" n="HIAT:w" s="T60">man</ts>
                  <nts id="Seg_237" n="HIAT:ip">)</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_240" n="HIAT:w" s="T61">tet</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_243" n="HIAT:w" s="T62">qɨrsagɨndo</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_246" n="HIAT:w" s="T63">ilʼlʼe</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_249" n="HIAT:w" s="T64">pɨgɨlan</ts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T65" id="Seg_252" n="sc" s="T1">
               <ts e="T2" id="Seg_254" n="e" s="T1">Meguntu </ts>
               <ts e="T3" id="Seg_256" n="e" s="T2">satdə </ts>
               <ts e="T4" id="Seg_258" n="e" s="T3">učʼitʼelʼ </ts>
               <ts e="T5" id="Seg_260" n="e" s="T4">tʼütdə. </ts>
               <ts e="T6" id="Seg_262" n="e" s="T5">Menan </ts>
               <ts e="T7" id="Seg_264" n="e" s="T6">süsögula </ts>
               <ts e="T8" id="Seg_266" n="e" s="T7">kocʼin </ts>
               <ts e="T9" id="Seg_268" n="e" s="T8">jewattə. </ts>
               <ts e="T10" id="Seg_270" n="e" s="T9">A </ts>
               <ts e="T11" id="Seg_272" n="e" s="T10">tep </ts>
               <ts e="T12" id="Seg_274" n="e" s="T11">tʼarɨn </ts>
               <ts e="T13" id="Seg_276" n="e" s="T12">okkɨr </ts>
               <ts e="T14" id="Seg_278" n="e" s="T13">nejɣumnä: </ts>
               <ts e="T15" id="Seg_280" n="e" s="T14">“Mazɨm </ts>
               <ts e="T16" id="Seg_282" n="e" s="T15">oɣulǯoʒaq </ts>
               <ts e="T17" id="Seg_284" n="e" s="T16">tʼolomčugu. </ts>
               <ts e="T18" id="Seg_286" n="e" s="T17">Wes </ts>
               <ts e="T19" id="Seg_288" n="e" s="T18">nejgulazʼe </ts>
               <ts e="T20" id="Seg_290" n="e" s="T19">tʼolomčeǯan.” </ts>
               <ts e="T21" id="Seg_292" n="e" s="T20">A </ts>
               <ts e="T22" id="Seg_294" n="e" s="T21">tep </ts>
               <ts e="T23" id="Seg_296" n="e" s="T22">tebɨm </ts>
               <ts e="T24" id="Seg_298" n="e" s="T23">oɣulǯoʒɨt: </ts>
               <ts e="T25" id="Seg_300" n="e" s="T24">“Maːtku </ts>
               <ts e="T26" id="Seg_302" n="e" s="T25">talʼel.” </ts>
               <ts e="T27" id="Seg_304" n="e" s="T26">I </ts>
               <ts e="T28" id="Seg_306" n="e" s="T27">tep </ts>
               <ts e="T29" id="Seg_308" n="e" s="T28">na </ts>
               <ts e="T30" id="Seg_310" n="e" s="T29">kurolǯa. </ts>
               <ts e="T31" id="Seg_312" n="e" s="T30">Pajagan </ts>
               <ts e="T32" id="Seg_314" n="e" s="T31">maːttɨ </ts>
               <ts e="T33" id="Seg_316" n="e" s="T32">sernɨn. </ts>
               <ts e="T34" id="Seg_318" n="e" s="T33">Pajaga </ts>
               <ts e="T35" id="Seg_320" n="e" s="T34">bɨlʼinai </ts>
               <ts e="T36" id="Seg_322" n="e" s="T35">meʒɨrɨn. </ts>
               <ts e="T37" id="Seg_324" n="e" s="T36">A </ts>
               <ts e="T38" id="Seg_326" n="e" s="T37">učʼitelʼ </ts>
               <ts e="T39" id="Seg_328" n="e" s="T38">tʼarɨn: </ts>
               <ts e="T40" id="Seg_330" n="e" s="T39">“Pajaga, </ts>
               <ts e="T41" id="Seg_332" n="e" s="T40">matku </ts>
               <ts e="T42" id="Seg_334" n="e" s="T41">dalʼel!” </ts>
               <ts e="T43" id="Seg_336" n="e" s="T42">Pajaga </ts>
               <ts e="T44" id="Seg_338" n="e" s="T43">kak </ts>
               <ts e="T45" id="Seg_340" n="e" s="T44">qwɛdɨpɨlʼe </ts>
               <ts e="T46" id="Seg_342" n="e" s="T45">jübɨratdə. </ts>
               <ts e="T47" id="Seg_344" n="e" s="T46">“Tan </ts>
               <ts e="T48" id="Seg_346" n="e" s="T47">učʼitʼelʼwatdə </ts>
               <ts e="T49" id="Seg_348" n="e" s="T48">ešo, </ts>
               <ts e="T50" id="Seg_350" n="e" s="T49">manan </ts>
               <ts e="T51" id="Seg_352" n="e" s="T50">maːderlʼe </ts>
               <ts e="T52" id="Seg_354" n="e" s="T51">jübɨrattə.” </ts>
               <ts e="T53" id="Seg_356" n="e" s="T52">Kak </ts>
               <ts e="T54" id="Seg_358" n="e" s="T53">skawarodnikam </ts>
               <ts e="T55" id="Seg_360" n="e" s="T54">meʒalʒit. </ts>
               <ts e="T56" id="Seg_362" n="e" s="T55">Täbɨm </ts>
               <ts e="T57" id="Seg_364" n="e" s="T56">(mazɨm) </ts>
               <ts e="T58" id="Seg_366" n="e" s="T57">qaːlʼe </ts>
               <ts e="T59" id="Seg_368" n="e" s="T58">kwatdɨn. </ts>
               <ts e="T60" id="Seg_370" n="e" s="T59">A </ts>
               <ts e="T61" id="Seg_372" n="e" s="T60">(man) </ts>
               <ts e="T62" id="Seg_374" n="e" s="T61">tet </ts>
               <ts e="T63" id="Seg_376" n="e" s="T62">qɨrsagɨndo </ts>
               <ts e="T64" id="Seg_378" n="e" s="T63">ilʼlʼe </ts>
               <ts e="T65" id="Seg_380" n="e" s="T64">pɨgɨlan. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_381" s="T1">PVD_1964_Anecdote_nar.001 (001.001)</ta>
            <ta e="T9" id="Seg_382" s="T5">PVD_1964_Anecdote_nar.002 (001.002)</ta>
            <ta e="T17" id="Seg_383" s="T9">PVD_1964_Anecdote_nar.003 (001.003)</ta>
            <ta e="T20" id="Seg_384" s="T17">PVD_1964_Anecdote_nar.004 (001.004)</ta>
            <ta e="T26" id="Seg_385" s="T20">PVD_1964_Anecdote_nar.005 (001.005)</ta>
            <ta e="T30" id="Seg_386" s="T26">PVD_1964_Anecdote_nar.006 (001.006)</ta>
            <ta e="T33" id="Seg_387" s="T30">PVD_1964_Anecdote_nar.007 (001.007)</ta>
            <ta e="T36" id="Seg_388" s="T33">PVD_1964_Anecdote_nar.008 (001.008)</ta>
            <ta e="T42" id="Seg_389" s="T36">PVD_1964_Anecdote_nar.009 (001.009)</ta>
            <ta e="T46" id="Seg_390" s="T42">PVD_1964_Anecdote_nar.010 (001.010)</ta>
            <ta e="T52" id="Seg_391" s="T46">PVD_1964_Anecdote_nar.011 (001.011)</ta>
            <ta e="T55" id="Seg_392" s="T52">PVD_1964_Anecdote_nar.012 (001.012)</ta>
            <ta e="T59" id="Seg_393" s="T55">PVD_1964_Anecdote_nar.013 (001.013)</ta>
            <ta e="T65" id="Seg_394" s="T59">PVD_1964_Anecdote_nar.014 (001.014)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_395" s="T1">′мегунту ′сатдъ учʼитʼелʼ ′тʼӱтдъ.</ta>
            <ta e="T9" id="Seg_396" s="T5">ме′нан сӱ′сӧгула коцʼин ′jеwаттъ.</ta>
            <ta e="T17" id="Seg_397" s="T9">а теп тʼа′рын оккыр ′нейɣумнӓ: ма′зым ′оɣуlджожаk тʼо′ломтшугу.</ta>
            <ta e="T20" id="Seg_398" s="T17">вес нейгу′лазʼе тʼо′ломтшеджан.</ta>
            <ta e="T26" id="Seg_399" s="T20">а теп те′бым ′оɣуlджо‵жыт: ма̄тку та′лʼел? </ta>
            <ta e="T30" id="Seg_400" s="T26">и теп на ку′роlджа.</ta>
            <ta e="T33" id="Seg_401" s="T30">паjа′ган ма̄тты ′сернын.</ta>
            <ta e="T36" id="Seg_402" s="T33">паjага ′былʼинаи ′межырын.</ta>
            <ta e="T42" id="Seg_403" s="T36">а учʼителʼ тʼа′рын: паjага, матку да′лʼел!</ta>
            <ta e="T46" id="Seg_404" s="T42">паjага как ′kwɛдыпылʼе ′jӱбыратдъ.</ta>
            <ta e="T52" id="Seg_405" s="T46">тан учʼитʼелʼ ′ватдъ е′шо, манан ма̄дерлʼе ′jӱбыраттъ.</ta>
            <ta e="T55" id="Seg_406" s="T52">как скавародникам ме′жалжит.</ta>
            <ta e="T59" id="Seg_407" s="T55">тӓбым (ма′зым) kа̄лʼекват′дын.</ta>
            <ta e="T65" id="Seg_408" s="T59">а (ман) тет kырсагын до и′лʼлʼе ′пыгылан.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_409" s="T1">meguntu satdə učʼitʼelʼ tʼütdə.</ta>
            <ta e="T9" id="Seg_410" s="T5">menan süsögula kocʼin jewattə.</ta>
            <ta e="T17" id="Seg_411" s="T9">a tep tʼarɨn okkɨr nejɣumnä: mazɨm oɣulǯoʒaq tʼolomtšugu.</ta>
            <ta e="T20" id="Seg_412" s="T17">wes nejgulazʼe tʼolomtšeǯan.</ta>
            <ta e="T26" id="Seg_413" s="T20">a tep tebɨm oɣulǯoʒɨt: maːtku talʼel? </ta>
            <ta e="T30" id="Seg_414" s="T26">i tep na kurolǯa.</ta>
            <ta e="T33" id="Seg_415" s="T30">pajagan maːttɨ sernɨn.</ta>
            <ta e="T36" id="Seg_416" s="T33">pajaga bɨlʼinai meʒɨrɨn.</ta>
            <ta e="T42" id="Seg_417" s="T36">a učʼitelʼ tʼarɨn: pajaga, matku dalʼel!</ta>
            <ta e="T46" id="Seg_418" s="T42">pajaga kak qwɛdɨpɨlʼe jübɨratdə.</ta>
            <ta e="T52" id="Seg_419" s="T46">tan učʼitʼelʼ watdə ešo, manan maːderlʼe jübɨrattə.</ta>
            <ta e="T55" id="Seg_420" s="T52">kak skawarodnikam meʒalʒit.</ta>
            <ta e="T59" id="Seg_421" s="T55">täbɨm (mazɨm) qaːlʼekwatdɨn.</ta>
            <ta e="T65" id="Seg_422" s="T59">a (man) tet qɨrsagɨn do ilʼlʼe pɨgɨlan.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_423" s="T1">Meguntu satdə učʼitʼelʼ tʼütdə. </ta>
            <ta e="T9" id="Seg_424" s="T5">Menan süsögula kocʼin jewattə. </ta>
            <ta e="T17" id="Seg_425" s="T9">A tep tʼarɨn okkɨr nejɣumnä: “Mazɨm oɣulǯoʒaq tʼolomčugu. </ta>
            <ta e="T20" id="Seg_426" s="T17">Wes nejgulazʼe tʼolomčeǯan.” </ta>
            <ta e="T26" id="Seg_427" s="T20">A tep tebɨm oɣulǯoʒɨt: “Maːtku talʼel.” </ta>
            <ta e="T30" id="Seg_428" s="T26">I tep na kurolǯa. </ta>
            <ta e="T33" id="Seg_429" s="T30">Pajagan maːttɨ sernɨn. </ta>
            <ta e="T36" id="Seg_430" s="T33">Pajaga bɨlʼinai meʒɨrɨn. </ta>
            <ta e="T42" id="Seg_431" s="T36">A učʼitelʼ tʼarɨn: “Pajaga, matku dalʼel!” </ta>
            <ta e="T46" id="Seg_432" s="T42">Pajaga kak qwɛdɨpɨlʼe jübɨratdə. </ta>
            <ta e="T52" id="Seg_433" s="T46">“Tan učʼitʼelʼwatdə ešo, manan maːderlʼe jübɨrattə.” </ta>
            <ta e="T55" id="Seg_434" s="T52">Kak skawarodnikam meʒalʒit. </ta>
            <ta e="T59" id="Seg_435" s="T55">Täbɨm (mazɨm) qaːlʼe kwatdɨn. </ta>
            <ta e="T65" id="Seg_436" s="T59">A (man) tet qɨrsagɨndo ilʼlʼe pɨgɨlan. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_437" s="T1">me-guntu</ta>
            <ta e="T3" id="Seg_438" s="T2">satdə</ta>
            <ta e="T4" id="Seg_439" s="T3">učʼitʼelʼ</ta>
            <ta e="T5" id="Seg_440" s="T4">tʼü-tdə</ta>
            <ta e="T6" id="Seg_441" s="T5">me-nan</ta>
            <ta e="T7" id="Seg_442" s="T6">süsögu-la</ta>
            <ta e="T8" id="Seg_443" s="T7">kocʼi-n</ta>
            <ta e="T9" id="Seg_444" s="T8">je-wa-ttə</ta>
            <ta e="T10" id="Seg_445" s="T9">a</ta>
            <ta e="T11" id="Seg_446" s="T10">tep</ta>
            <ta e="T12" id="Seg_447" s="T11">tʼarɨ-n</ta>
            <ta e="T13" id="Seg_448" s="T12">okkɨr</ta>
            <ta e="T14" id="Seg_449" s="T13">ne-j-ɣum-nä</ta>
            <ta e="T15" id="Seg_450" s="T14">mazɨm</ta>
            <ta e="T16" id="Seg_451" s="T15">oɣulǯo-ʒa-q</ta>
            <ta e="T17" id="Seg_452" s="T16">tʼolom-ču-gu</ta>
            <ta e="T18" id="Seg_453" s="T17">wes</ta>
            <ta e="T19" id="Seg_454" s="T18">ne-j-gu-la-zʼe</ta>
            <ta e="T20" id="Seg_455" s="T19">tʼolom-č-eǯa-n</ta>
            <ta e="T21" id="Seg_456" s="T20">a</ta>
            <ta e="T22" id="Seg_457" s="T21">tep</ta>
            <ta e="T23" id="Seg_458" s="T22">teb-ɨ-m</ta>
            <ta e="T24" id="Seg_459" s="T23">oɣulǯo-ʒɨ-t</ta>
            <ta e="T25" id="Seg_460" s="T24">maːt-ku</ta>
            <ta e="T26" id="Seg_461" s="T25">ta-lʼe-l</ta>
            <ta e="T27" id="Seg_462" s="T26">i</ta>
            <ta e="T28" id="Seg_463" s="T27">tep</ta>
            <ta e="T29" id="Seg_464" s="T28">na</ta>
            <ta e="T30" id="Seg_465" s="T29">kur-ol-ǯa</ta>
            <ta e="T31" id="Seg_466" s="T30">paja-ga-n</ta>
            <ta e="T32" id="Seg_467" s="T31">maːt-tɨ</ta>
            <ta e="T33" id="Seg_468" s="T32">ser-nɨ-n</ta>
            <ta e="T34" id="Seg_469" s="T33">paja-ga</ta>
            <ta e="T35" id="Seg_470" s="T34">bɨlʼinai</ta>
            <ta e="T36" id="Seg_471" s="T35">me-ʒɨ-rɨ-n</ta>
            <ta e="T37" id="Seg_472" s="T36">a</ta>
            <ta e="T38" id="Seg_473" s="T37">učʼitelʼ</ta>
            <ta e="T39" id="Seg_474" s="T38">tʼarɨ-n</ta>
            <ta e="T40" id="Seg_475" s="T39">paja-ga</ta>
            <ta e="T41" id="Seg_476" s="T40">mat-ku</ta>
            <ta e="T42" id="Seg_477" s="T41">da-lʼe-l</ta>
            <ta e="T43" id="Seg_478" s="T42">paja-ga</ta>
            <ta e="T44" id="Seg_479" s="T43">kak</ta>
            <ta e="T45" id="Seg_480" s="T44">qwɛdɨ-pɨ-lʼe</ta>
            <ta e="T46" id="Seg_481" s="T45">jübɨ-ra-tdə</ta>
            <ta e="T47" id="Seg_482" s="T46">Tan</ta>
            <ta e="T48" id="Seg_483" s="T47">učʼitʼelʼ-wa-tdə</ta>
            <ta e="T49" id="Seg_484" s="T48">ešo</ta>
            <ta e="T50" id="Seg_485" s="T49">ma-nan</ta>
            <ta e="T51" id="Seg_486" s="T50">maːder-lʼe</ta>
            <ta e="T52" id="Seg_487" s="T51">jübɨ-ra-ttə</ta>
            <ta e="T53" id="Seg_488" s="T52">kak</ta>
            <ta e="T54" id="Seg_489" s="T53">skawarodnik-a-m</ta>
            <ta e="T55" id="Seg_490" s="T54">meʒal-ʒi-t</ta>
            <ta e="T56" id="Seg_491" s="T55">täb-ɨ-m</ta>
            <ta e="T57" id="Seg_492" s="T56">mazɨm</ta>
            <ta e="T58" id="Seg_493" s="T57">qaː-lʼe</ta>
            <ta e="T59" id="Seg_494" s="T58">kwatdɨ-n</ta>
            <ta e="T60" id="Seg_495" s="T59">a</ta>
            <ta e="T61" id="Seg_496" s="T60">man</ta>
            <ta e="T62" id="Seg_497" s="T61">tet</ta>
            <ta e="T63" id="Seg_498" s="T62">qɨr-sa-gɨndo</ta>
            <ta e="T64" id="Seg_499" s="T63">ilʼlʼe</ta>
            <ta e="T65" id="Seg_500" s="T64">pɨgɨl-a-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_501" s="T1">me-qɨntɨ</ta>
            <ta e="T3" id="Seg_502" s="T2">saddə</ta>
            <ta e="T4" id="Seg_503" s="T3">učʼitʼelʼ</ta>
            <ta e="T5" id="Seg_504" s="T4">tüː-ntɨ</ta>
            <ta e="T6" id="Seg_505" s="T5">me-nan</ta>
            <ta e="T7" id="Seg_506" s="T6">süsögum-la</ta>
            <ta e="T8" id="Seg_507" s="T7">koːci-ŋ</ta>
            <ta e="T9" id="Seg_508" s="T8">eː-nɨ-tɨn</ta>
            <ta e="T10" id="Seg_509" s="T9">a</ta>
            <ta e="T11" id="Seg_510" s="T10">täp</ta>
            <ta e="T12" id="Seg_511" s="T11">tʼärɨ-n</ta>
            <ta e="T13" id="Seg_512" s="T12">okkɨr</ta>
            <ta e="T14" id="Seg_513" s="T13">ne-lʼ-qum-nä</ta>
            <ta e="T15" id="Seg_514" s="T14">mazɨm</ta>
            <ta e="T16" id="Seg_515" s="T15">oɣulǯɨ-ʒu-kɨ</ta>
            <ta e="T17" id="Seg_516" s="T16">tʼolom-ču-gu</ta>
            <ta e="T18" id="Seg_517" s="T17">wesʼ</ta>
            <ta e="T19" id="Seg_518" s="T18">ne-lʼ-qum-la-se</ta>
            <ta e="T20" id="Seg_519" s="T19">tʼolom-ču-enǯɨ-ŋ</ta>
            <ta e="T21" id="Seg_520" s="T20">a</ta>
            <ta e="T22" id="Seg_521" s="T21">täp</ta>
            <ta e="T23" id="Seg_522" s="T22">täp-ɨ-m</ta>
            <ta e="T24" id="Seg_523" s="T23">oɣulǯɨ-ʒu-t</ta>
            <ta e="T25" id="Seg_524" s="T24">mat-gu</ta>
            <ta e="T26" id="Seg_525" s="T25">tat-lä-l</ta>
            <ta e="T27" id="Seg_526" s="T26">i</ta>
            <ta e="T28" id="Seg_527" s="T27">täp</ta>
            <ta e="T29" id="Seg_528" s="T28">na</ta>
            <ta e="T30" id="Seg_529" s="T29">kur-ol-ntɨ</ta>
            <ta e="T31" id="Seg_530" s="T30">paja-ka-n</ta>
            <ta e="T32" id="Seg_531" s="T31">maːt-ntə</ta>
            <ta e="T33" id="Seg_532" s="T32">ser-nɨ-n</ta>
            <ta e="T34" id="Seg_533" s="T33">paja-ka</ta>
            <ta e="T35" id="Seg_534" s="T34">bɨlʼinai</ta>
            <ta e="T36" id="Seg_535" s="T35">meː-ʒu-rɨ-n</ta>
            <ta e="T37" id="Seg_536" s="T36">a</ta>
            <ta e="T38" id="Seg_537" s="T37">učʼitʼelʼ</ta>
            <ta e="T39" id="Seg_538" s="T38">tʼärɨ-n</ta>
            <ta e="T40" id="Seg_539" s="T39">paja-ka</ta>
            <ta e="T41" id="Seg_540" s="T40">mat-gu</ta>
            <ta e="T42" id="Seg_541" s="T41">tat-lä-l</ta>
            <ta e="T43" id="Seg_542" s="T42">paja-ka</ta>
            <ta e="T44" id="Seg_543" s="T43">kak</ta>
            <ta e="T45" id="Seg_544" s="T44">qwɛdɨ-mbɨ-le</ta>
            <ta e="T46" id="Seg_545" s="T45">übɨ-rɨ-ntɨ</ta>
            <ta e="T47" id="Seg_546" s="T46">tan</ta>
            <ta e="T48" id="Seg_547" s="T47">učʼitʼelʼ-nɨ-ntə</ta>
            <ta e="T49" id="Seg_548" s="T48">ešo</ta>
            <ta e="T50" id="Seg_549" s="T49">man-nan</ta>
            <ta e="T51" id="Seg_550" s="T50">madər-le</ta>
            <ta e="T52" id="Seg_551" s="T51">übɨ-rɨ-ntə</ta>
            <ta e="T53" id="Seg_552" s="T52">kak</ta>
            <ta e="T54" id="Seg_553" s="T53">skawarodnik-ɨ-m</ta>
            <ta e="T55" id="Seg_554" s="T54">meʒal-ʒu-t</ta>
            <ta e="T56" id="Seg_555" s="T55">täp-ɨ-m</ta>
            <ta e="T57" id="Seg_556" s="T56">mazɨm</ta>
            <ta e="T58" id="Seg_557" s="T57">qa-le</ta>
            <ta e="T59" id="Seg_558" s="T58">qwattɨ-n</ta>
            <ta e="T60" id="Seg_559" s="T59">a</ta>
            <ta e="T61" id="Seg_560" s="T60">man</ta>
            <ta e="T62" id="Seg_561" s="T61">tet</ta>
            <ta e="T63" id="Seg_562" s="T62">qɨr-se-qɨntɨ</ta>
            <ta e="T64" id="Seg_563" s="T63">ilʼlʼe</ta>
            <ta e="T65" id="Seg_564" s="T64">pɨŋgəl-ɨ-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_565" s="T1">we-ILL.3SG</ta>
            <ta e="T3" id="Seg_566" s="T2">new</ta>
            <ta e="T4" id="Seg_567" s="T3">teacher.[NOM]</ta>
            <ta e="T5" id="Seg_568" s="T4">come-INFER.[3SG.S]</ta>
            <ta e="T6" id="Seg_569" s="T5">we-ADES</ta>
            <ta e="T7" id="Seg_570" s="T6">Selkup-PL.[NOM]</ta>
            <ta e="T8" id="Seg_571" s="T7">much-ADVZ</ta>
            <ta e="T9" id="Seg_572" s="T8">be-CO-3PL</ta>
            <ta e="T10" id="Seg_573" s="T9">and</ta>
            <ta e="T11" id="Seg_574" s="T10">(s)he.[NOM]</ta>
            <ta e="T12" id="Seg_575" s="T11">say-3SG.S</ta>
            <ta e="T13" id="Seg_576" s="T12">one</ta>
            <ta e="T14" id="Seg_577" s="T13">woman-ADJZ-human.being-ALL</ta>
            <ta e="T15" id="Seg_578" s="T14">I.ACC</ta>
            <ta e="T16" id="Seg_579" s="T15">teach-DRV-IMP.2SG.S</ta>
            <ta e="T17" id="Seg_580" s="T16">hello-VBLZ-INF</ta>
            <ta e="T18" id="Seg_581" s="T17">all</ta>
            <ta e="T19" id="Seg_582" s="T18">woman-ADJZ-human.being-PL-COM</ta>
            <ta e="T20" id="Seg_583" s="T19">hello-VBLZ-FUT-1SG.S</ta>
            <ta e="T21" id="Seg_584" s="T20">and</ta>
            <ta e="T22" id="Seg_585" s="T21">(s)he.[NOM]</ta>
            <ta e="T23" id="Seg_586" s="T22">(s)he-EP-ACC</ta>
            <ta e="T24" id="Seg_587" s="T23">teach-DRV-3SG.O</ta>
            <ta e="T25" id="Seg_588" s="T24">fuck-INF</ta>
            <ta e="T26" id="Seg_589" s="T25">give-OPT-2SG.O</ta>
            <ta e="T27" id="Seg_590" s="T26">and</ta>
            <ta e="T28" id="Seg_591" s="T27">(s)he.[NOM]</ta>
            <ta e="T29" id="Seg_592" s="T28">here</ta>
            <ta e="T30" id="Seg_593" s="T29">go-MOM-INFER.[3SG.S]</ta>
            <ta e="T31" id="Seg_594" s="T30">old.woman-DIM-GEN</ta>
            <ta e="T32" id="Seg_595" s="T31">house-ILL</ta>
            <ta e="T33" id="Seg_596" s="T32">come.in-CO-3SG.S</ta>
            <ta e="T34" id="Seg_597" s="T33">old.woman-DIM.[NOM]</ta>
            <ta e="T35" id="Seg_598" s="T34">pancake.PL.[NOM]</ta>
            <ta e="T36" id="Seg_599" s="T35">do-DRV-DRV-3SG.S</ta>
            <ta e="T37" id="Seg_600" s="T36">and</ta>
            <ta e="T38" id="Seg_601" s="T37">teacher.[NOM]</ta>
            <ta e="T39" id="Seg_602" s="T38">say-3SG.S</ta>
            <ta e="T40" id="Seg_603" s="T39">old.woman-DIM.[NOM]</ta>
            <ta e="T41" id="Seg_604" s="T40">fuck-INF</ta>
            <ta e="T42" id="Seg_605" s="T41">bring-OPT-2SG.O</ta>
            <ta e="T43" id="Seg_606" s="T42">old.woman-DIM.[NOM]</ta>
            <ta e="T44" id="Seg_607" s="T43">suddenly</ta>
            <ta e="T45" id="Seg_608" s="T44">swear.at-DUR-CVB</ta>
            <ta e="T46" id="Seg_609" s="T45">begin-DRV-INFER.[3SG.S]</ta>
            <ta e="T47" id="Seg_610" s="T46">you.SG.NOM</ta>
            <ta e="T48" id="Seg_611" s="T47">teacher-CO-2SG.S</ta>
            <ta e="T49" id="Seg_612" s="T48">more</ta>
            <ta e="T50" id="Seg_613" s="T49">I-ADES</ta>
            <ta e="T51" id="Seg_614" s="T50">ask.for-CVB</ta>
            <ta e="T52" id="Seg_615" s="T51">begin-DRV-2SG.S</ta>
            <ta e="T53" id="Seg_616" s="T52">suddenly</ta>
            <ta e="T54" id="Seg_617" s="T53">pan.holder-EP-ACC</ta>
            <ta e="T55" id="Seg_618" s="T54">pull-DRV-3SG.O</ta>
            <ta e="T56" id="Seg_619" s="T55">(s)he-EP-ACC</ta>
            <ta e="T57" id="Seg_620" s="T56">I.ACC</ta>
            <ta e="T58" id="Seg_621" s="T57">pursue-CVB</ta>
            <ta e="T59" id="Seg_622" s="T58">begin-3SG.S</ta>
            <ta e="T60" id="Seg_623" s="T59">and</ta>
            <ta e="T61" id="Seg_624" s="T60">I.NOM</ta>
            <ta e="T62" id="Seg_625" s="T61">%%</ta>
            <ta e="T63" id="Seg_626" s="T62">hole-COM-EL.3SG</ta>
            <ta e="T64" id="Seg_627" s="T63">down</ta>
            <ta e="T65" id="Seg_628" s="T64">fall.down-EP-3SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_629" s="T1">мы-ILL.3SG</ta>
            <ta e="T3" id="Seg_630" s="T2">новый</ta>
            <ta e="T4" id="Seg_631" s="T3">учитель.[NOM]</ta>
            <ta e="T5" id="Seg_632" s="T4">приехать-INFER.[3SG.S]</ta>
            <ta e="T6" id="Seg_633" s="T5">мы-ADES</ta>
            <ta e="T7" id="Seg_634" s="T6">селькуп-PL.[NOM]</ta>
            <ta e="T8" id="Seg_635" s="T7">много-ADVZ</ta>
            <ta e="T9" id="Seg_636" s="T8">быть-CO-3PL</ta>
            <ta e="T10" id="Seg_637" s="T9">а</ta>
            <ta e="T11" id="Seg_638" s="T10">он(а).[NOM]</ta>
            <ta e="T12" id="Seg_639" s="T11">сказать-3SG.S</ta>
            <ta e="T13" id="Seg_640" s="T12">один</ta>
            <ta e="T14" id="Seg_641" s="T13">женщина-ADJZ-человек-ALL</ta>
            <ta e="T15" id="Seg_642" s="T14">я.ACC</ta>
            <ta e="T16" id="Seg_643" s="T15">научить-DRV-IMP.2SG.S</ta>
            <ta e="T17" id="Seg_644" s="T16">здравствуй-VBLZ-INF</ta>
            <ta e="T18" id="Seg_645" s="T17">весь</ta>
            <ta e="T19" id="Seg_646" s="T18">женщина-ADJZ-человек-PL-COM</ta>
            <ta e="T20" id="Seg_647" s="T19">здравствуй-VBLZ-FUT-1SG.S</ta>
            <ta e="T21" id="Seg_648" s="T20">а</ta>
            <ta e="T22" id="Seg_649" s="T21">он(а).[NOM]</ta>
            <ta e="T23" id="Seg_650" s="T22">он(а)-EP-ACC</ta>
            <ta e="T24" id="Seg_651" s="T23">научить-DRV-3SG.O</ta>
            <ta e="T25" id="Seg_652" s="T24">соединяться.с.женщиной-INF</ta>
            <ta e="T26" id="Seg_653" s="T25">подать-OPT-2SG.O</ta>
            <ta e="T27" id="Seg_654" s="T26">и</ta>
            <ta e="T28" id="Seg_655" s="T27">он(а).[NOM]</ta>
            <ta e="T29" id="Seg_656" s="T28">ну</ta>
            <ta e="T30" id="Seg_657" s="T29">ходить-MOM-INFER.[3SG.S]</ta>
            <ta e="T31" id="Seg_658" s="T30">старуха-DIM-GEN</ta>
            <ta e="T32" id="Seg_659" s="T31">дом-ILL</ta>
            <ta e="T33" id="Seg_660" s="T32">зайти-CO-3SG.S</ta>
            <ta e="T34" id="Seg_661" s="T33">старуха-DIM.[NOM]</ta>
            <ta e="T35" id="Seg_662" s="T34">блин.PL.[NOM]</ta>
            <ta e="T36" id="Seg_663" s="T35">сделать-DRV-DRV-3SG.S</ta>
            <ta e="T37" id="Seg_664" s="T36">а</ta>
            <ta e="T38" id="Seg_665" s="T37">учитель.[NOM]</ta>
            <ta e="T39" id="Seg_666" s="T38">сказать-3SG.S</ta>
            <ta e="T40" id="Seg_667" s="T39">старуха-DIM.[NOM]</ta>
            <ta e="T41" id="Seg_668" s="T40">соединяться.с.женщиной-INF</ta>
            <ta e="T42" id="Seg_669" s="T41">принести-OPT-2SG.O</ta>
            <ta e="T43" id="Seg_670" s="T42">старуха-DIM.[NOM]</ta>
            <ta e="T44" id="Seg_671" s="T43">как</ta>
            <ta e="T45" id="Seg_672" s="T44">поругать-DUR-CVB</ta>
            <ta e="T46" id="Seg_673" s="T45">начать-DRV-INFER.[3SG.S]</ta>
            <ta e="T47" id="Seg_674" s="T46">ты.NOM</ta>
            <ta e="T48" id="Seg_675" s="T47">учитель-CO-2SG.S</ta>
            <ta e="T49" id="Seg_676" s="T48">еще</ta>
            <ta e="T50" id="Seg_677" s="T49">я-ADES</ta>
            <ta e="T51" id="Seg_678" s="T50">попросить-CVB</ta>
            <ta e="T52" id="Seg_679" s="T51">начать-DRV-2SG.S</ta>
            <ta e="T53" id="Seg_680" s="T52">как</ta>
            <ta e="T54" id="Seg_681" s="T53">сковородник-EP-ACC</ta>
            <ta e="T55" id="Seg_682" s="T54">вырвать-DRV-3SG.O</ta>
            <ta e="T56" id="Seg_683" s="T55">он(а)-EP-ACC</ta>
            <ta e="T57" id="Seg_684" s="T56">я.ACC</ta>
            <ta e="T58" id="Seg_685" s="T57">преследовать-CVB</ta>
            <ta e="T59" id="Seg_686" s="T58">начать-3SG.S</ta>
            <ta e="T60" id="Seg_687" s="T59">а</ta>
            <ta e="T61" id="Seg_688" s="T60">я.NOM</ta>
            <ta e="T62" id="Seg_689" s="T61">%%</ta>
            <ta e="T63" id="Seg_690" s="T62">дыра-COM-EL.3SG</ta>
            <ta e="T64" id="Seg_691" s="T63">вниз</ta>
            <ta e="T65" id="Seg_692" s="T64">упасть-EP-3SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_693" s="T1">pers-n:case.poss</ta>
            <ta e="T3" id="Seg_694" s="T2">adj</ta>
            <ta e="T4" id="Seg_695" s="T3">n.[n:case]</ta>
            <ta e="T5" id="Seg_696" s="T4">v-v:mood.[v:pn]</ta>
            <ta e="T6" id="Seg_697" s="T5">pers-n:case</ta>
            <ta e="T7" id="Seg_698" s="T6">n-n:num.[n:case]</ta>
            <ta e="T8" id="Seg_699" s="T7">quant-adj&gt;adv</ta>
            <ta e="T9" id="Seg_700" s="T8">v-v:ins-v:pn</ta>
            <ta e="T10" id="Seg_701" s="T9">conj</ta>
            <ta e="T11" id="Seg_702" s="T10">pers.[n:case]</ta>
            <ta e="T12" id="Seg_703" s="T11">v-v:pn</ta>
            <ta e="T13" id="Seg_704" s="T12">num</ta>
            <ta e="T14" id="Seg_705" s="T13">n-n&gt;adj-n-n:case</ta>
            <ta e="T15" id="Seg_706" s="T14">pers</ta>
            <ta e="T16" id="Seg_707" s="T15">v-v&gt;v-v:mood.pn</ta>
            <ta e="T17" id="Seg_708" s="T16">interj-n&gt;v-v:inf</ta>
            <ta e="T18" id="Seg_709" s="T17">quant</ta>
            <ta e="T19" id="Seg_710" s="T18">n-n&gt;adj-n-n:num-n:case</ta>
            <ta e="T20" id="Seg_711" s="T19">interj-n&gt;v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_712" s="T20">conj</ta>
            <ta e="T22" id="Seg_713" s="T21">pers.[n:case]</ta>
            <ta e="T23" id="Seg_714" s="T22">pers-n:ins-n:case</ta>
            <ta e="T24" id="Seg_715" s="T23">v-v&gt;v-v:pn</ta>
            <ta e="T25" id="Seg_716" s="T24">v-v:inf</ta>
            <ta e="T26" id="Seg_717" s="T25">v-v:mood-v:pn</ta>
            <ta e="T27" id="Seg_718" s="T26">conj</ta>
            <ta e="T28" id="Seg_719" s="T27">pers.[n:case]</ta>
            <ta e="T29" id="Seg_720" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_721" s="T29">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T31" id="Seg_722" s="T30">n-n&gt;n-n:case</ta>
            <ta e="T32" id="Seg_723" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_724" s="T32">v-v:ins-v:pn</ta>
            <ta e="T34" id="Seg_725" s="T33">n-n&gt;n.[n:case]</ta>
            <ta e="T35" id="Seg_726" s="T34">n.[n:case]</ta>
            <ta e="T36" id="Seg_727" s="T35">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T37" id="Seg_728" s="T36">conj</ta>
            <ta e="T38" id="Seg_729" s="T37">n.[n:case]</ta>
            <ta e="T39" id="Seg_730" s="T38">v-v:pn</ta>
            <ta e="T40" id="Seg_731" s="T39">n-n&gt;n.[n:case]</ta>
            <ta e="T41" id="Seg_732" s="T40">v-v:inf</ta>
            <ta e="T42" id="Seg_733" s="T41">v-v:mood-v:pn</ta>
            <ta e="T43" id="Seg_734" s="T42">n-n&gt;n.[n:case]</ta>
            <ta e="T44" id="Seg_735" s="T43">adv</ta>
            <ta e="T45" id="Seg_736" s="T44">v-v&gt;v-v&gt;adv</ta>
            <ta e="T46" id="Seg_737" s="T45">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T47" id="Seg_738" s="T46">pers</ta>
            <ta e="T48" id="Seg_739" s="T47">n-v:ins-v:pn</ta>
            <ta e="T49" id="Seg_740" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_741" s="T49">pers-n:case</ta>
            <ta e="T51" id="Seg_742" s="T50">v-v&gt;adv</ta>
            <ta e="T52" id="Seg_743" s="T51">v-v&gt;v-v:pn</ta>
            <ta e="T53" id="Seg_744" s="T52">adv</ta>
            <ta e="T54" id="Seg_745" s="T53">n-n:ins-n:case</ta>
            <ta e="T55" id="Seg_746" s="T54">v-v&gt;v-v:pn</ta>
            <ta e="T56" id="Seg_747" s="T55">pers-n:ins-n:case</ta>
            <ta e="T57" id="Seg_748" s="T56">pers</ta>
            <ta e="T58" id="Seg_749" s="T57">v-v&gt;adv</ta>
            <ta e="T59" id="Seg_750" s="T58">v-v:pn</ta>
            <ta e="T60" id="Seg_751" s="T59">conj</ta>
            <ta e="T61" id="Seg_752" s="T60">pers</ta>
            <ta e="T62" id="Seg_753" s="T61">%%</ta>
            <ta e="T63" id="Seg_754" s="T62">n-n:case-n:case.poss</ta>
            <ta e="T64" id="Seg_755" s="T63">preverb</ta>
            <ta e="T65" id="Seg_756" s="T64">v-n:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_757" s="T1">pers</ta>
            <ta e="T3" id="Seg_758" s="T2">adj</ta>
            <ta e="T4" id="Seg_759" s="T3">n</ta>
            <ta e="T5" id="Seg_760" s="T4">v</ta>
            <ta e="T6" id="Seg_761" s="T5">pers</ta>
            <ta e="T7" id="Seg_762" s="T6">n</ta>
            <ta e="T8" id="Seg_763" s="T7">quant</ta>
            <ta e="T9" id="Seg_764" s="T8">v</ta>
            <ta e="T10" id="Seg_765" s="T9">conj</ta>
            <ta e="T11" id="Seg_766" s="T10">pers</ta>
            <ta e="T12" id="Seg_767" s="T11">v</ta>
            <ta e="T13" id="Seg_768" s="T12">num</ta>
            <ta e="T14" id="Seg_769" s="T13">n</ta>
            <ta e="T15" id="Seg_770" s="T14">pers</ta>
            <ta e="T16" id="Seg_771" s="T15">v</ta>
            <ta e="T17" id="Seg_772" s="T16">v</ta>
            <ta e="T18" id="Seg_773" s="T17">quant</ta>
            <ta e="T19" id="Seg_774" s="T18">n</ta>
            <ta e="T20" id="Seg_775" s="T19">v</ta>
            <ta e="T21" id="Seg_776" s="T20">conj</ta>
            <ta e="T22" id="Seg_777" s="T21">pers</ta>
            <ta e="T23" id="Seg_778" s="T22">pers</ta>
            <ta e="T24" id="Seg_779" s="T23">v</ta>
            <ta e="T25" id="Seg_780" s="T24">v</ta>
            <ta e="T26" id="Seg_781" s="T25">v</ta>
            <ta e="T27" id="Seg_782" s="T26">conj</ta>
            <ta e="T28" id="Seg_783" s="T27">pers</ta>
            <ta e="T29" id="Seg_784" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_785" s="T29">v</ta>
            <ta e="T31" id="Seg_786" s="T30">n</ta>
            <ta e="T32" id="Seg_787" s="T31">n</ta>
            <ta e="T33" id="Seg_788" s="T32">v</ta>
            <ta e="T34" id="Seg_789" s="T33">n</ta>
            <ta e="T35" id="Seg_790" s="T34">n</ta>
            <ta e="T36" id="Seg_791" s="T35">v</ta>
            <ta e="T37" id="Seg_792" s="T36">conj</ta>
            <ta e="T38" id="Seg_793" s="T37">n</ta>
            <ta e="T39" id="Seg_794" s="T38">v</ta>
            <ta e="T40" id="Seg_795" s="T39">n</ta>
            <ta e="T41" id="Seg_796" s="T40">v</ta>
            <ta e="T42" id="Seg_797" s="T41">v</ta>
            <ta e="T43" id="Seg_798" s="T42">n</ta>
            <ta e="T44" id="Seg_799" s="T43">adv</ta>
            <ta e="T45" id="Seg_800" s="T44">adv</ta>
            <ta e="T46" id="Seg_801" s="T45">v</ta>
            <ta e="T47" id="Seg_802" s="T46">pers</ta>
            <ta e="T48" id="Seg_803" s="T47">n</ta>
            <ta e="T49" id="Seg_804" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_805" s="T49">pers</ta>
            <ta e="T51" id="Seg_806" s="T50">adv</ta>
            <ta e="T52" id="Seg_807" s="T51">v</ta>
            <ta e="T53" id="Seg_808" s="T52">adv</ta>
            <ta e="T54" id="Seg_809" s="T53">n</ta>
            <ta e="T55" id="Seg_810" s="T54">v</ta>
            <ta e="T56" id="Seg_811" s="T55">pers</ta>
            <ta e="T57" id="Seg_812" s="T56">pers</ta>
            <ta e="T58" id="Seg_813" s="T57">adv</ta>
            <ta e="T59" id="Seg_814" s="T58">v</ta>
            <ta e="T60" id="Seg_815" s="T59">conj</ta>
            <ta e="T61" id="Seg_816" s="T60">pers</ta>
            <ta e="T63" id="Seg_817" s="T62">n</ta>
            <ta e="T64" id="Seg_818" s="T63">preverb</ta>
            <ta e="T65" id="Seg_819" s="T64">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_820" s="T1">pro.h:G</ta>
            <ta e="T4" id="Seg_821" s="T3">np.h:A</ta>
            <ta e="T6" id="Seg_822" s="T5">pro.h:L</ta>
            <ta e="T7" id="Seg_823" s="T6">np.h:Th</ta>
            <ta e="T11" id="Seg_824" s="T10">pro.h:A</ta>
            <ta e="T14" id="Seg_825" s="T13">np.h:R</ta>
            <ta e="T15" id="Seg_826" s="T14">pro.h:Th</ta>
            <ta e="T16" id="Seg_827" s="T15">0.2.h:A</ta>
            <ta e="T17" id="Seg_828" s="T16">v:Th</ta>
            <ta e="T20" id="Seg_829" s="T19">0.1.h:A</ta>
            <ta e="T22" id="Seg_830" s="T21">pro.h:A</ta>
            <ta e="T23" id="Seg_831" s="T22">pro.h:Th</ta>
            <ta e="T25" id="Seg_832" s="T24">v:Th</ta>
            <ta e="T26" id="Seg_833" s="T25">0.2.h:A</ta>
            <ta e="T28" id="Seg_834" s="T27">pro.h:A</ta>
            <ta e="T31" id="Seg_835" s="T30">np.h:Poss</ta>
            <ta e="T32" id="Seg_836" s="T31">np:G</ta>
            <ta e="T33" id="Seg_837" s="T32">0.3.h:A</ta>
            <ta e="T34" id="Seg_838" s="T33">np.h:A</ta>
            <ta e="T35" id="Seg_839" s="T34">np:P</ta>
            <ta e="T38" id="Seg_840" s="T37">np.h:A</ta>
            <ta e="T41" id="Seg_841" s="T40">v:Th</ta>
            <ta e="T42" id="Seg_842" s="T41">0.2.h:A</ta>
            <ta e="T43" id="Seg_843" s="T42">np.h:A</ta>
            <ta e="T47" id="Seg_844" s="T46">pro.h:Th</ta>
            <ta e="T50" id="Seg_845" s="T49">pro.h:R</ta>
            <ta e="T52" id="Seg_846" s="T51">0.2.h:A</ta>
            <ta e="T54" id="Seg_847" s="T53">np:Th</ta>
            <ta e="T55" id="Seg_848" s="T54">0.3.h:A</ta>
            <ta e="T56" id="Seg_849" s="T55">pro.h:Th</ta>
            <ta e="T59" id="Seg_850" s="T58">0.3.h:A</ta>
            <ta e="T61" id="Seg_851" s="T60">pro.h:P</ta>
            <ta e="T63" id="Seg_852" s="T62">np:So</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_853" s="T3">np.h:S</ta>
            <ta e="T5" id="Seg_854" s="T4">v:pred</ta>
            <ta e="T7" id="Seg_855" s="T6">np.h:S</ta>
            <ta e="T9" id="Seg_856" s="T8">v:pred</ta>
            <ta e="T11" id="Seg_857" s="T10">pro.h:S</ta>
            <ta e="T12" id="Seg_858" s="T11">v:pred</ta>
            <ta e="T15" id="Seg_859" s="T14">pro.h:O</ta>
            <ta e="T16" id="Seg_860" s="T15">0.2.h:S v:pred</ta>
            <ta e="T17" id="Seg_861" s="T16">v:O</ta>
            <ta e="T20" id="Seg_862" s="T19">0.1.h:S v:pred</ta>
            <ta e="T22" id="Seg_863" s="T21">pro.h:S</ta>
            <ta e="T23" id="Seg_864" s="T22">pro.h:O</ta>
            <ta e="T24" id="Seg_865" s="T23">v:pred</ta>
            <ta e="T25" id="Seg_866" s="T24">v:O</ta>
            <ta e="T26" id="Seg_867" s="T25">0.2.h:S v:pred</ta>
            <ta e="T28" id="Seg_868" s="T27">pro.h:S</ta>
            <ta e="T30" id="Seg_869" s="T29">v:pred</ta>
            <ta e="T33" id="Seg_870" s="T32">0.3.h:S v:pred</ta>
            <ta e="T34" id="Seg_871" s="T33">np.h:S</ta>
            <ta e="T35" id="Seg_872" s="T34">np:O</ta>
            <ta e="T36" id="Seg_873" s="T35">v:pred</ta>
            <ta e="T38" id="Seg_874" s="T37">np.h:S</ta>
            <ta e="T39" id="Seg_875" s="T38">v:pred</ta>
            <ta e="T41" id="Seg_876" s="T40">v:O</ta>
            <ta e="T42" id="Seg_877" s="T41">0.2.h:S v:pred</ta>
            <ta e="T43" id="Seg_878" s="T42">np.h:S</ta>
            <ta e="T46" id="Seg_879" s="T45">v:pred</ta>
            <ta e="T47" id="Seg_880" s="T46">pro.h:S</ta>
            <ta e="T48" id="Seg_881" s="T47">n:pred</ta>
            <ta e="T52" id="Seg_882" s="T51">0.2.h:S v:pred</ta>
            <ta e="T54" id="Seg_883" s="T53">np:O</ta>
            <ta e="T55" id="Seg_884" s="T54">0.3.h:S v:pred</ta>
            <ta e="T59" id="Seg_885" s="T58">0.3.h:S v:pred</ta>
            <ta e="T61" id="Seg_886" s="T60">pro.h:S</ta>
            <ta e="T65" id="Seg_887" s="T64">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_888" s="T3">RUS:cult</ta>
            <ta e="T10" id="Seg_889" s="T9">RUS:gram</ta>
            <ta e="T18" id="Seg_890" s="T17">RUS:core</ta>
            <ta e="T21" id="Seg_891" s="T20">RUS:gram</ta>
            <ta e="T27" id="Seg_892" s="T26">RUS:gram</ta>
            <ta e="T35" id="Seg_893" s="T34">RUS:cult</ta>
            <ta e="T37" id="Seg_894" s="T36">RUS:gram</ta>
            <ta e="T38" id="Seg_895" s="T37">RUS:cult</ta>
            <ta e="T44" id="Seg_896" s="T43">RUS:disc</ta>
            <ta e="T48" id="Seg_897" s="T47">RUS:cult</ta>
            <ta e="T49" id="Seg_898" s="T48">RUS:disc</ta>
            <ta e="T53" id="Seg_899" s="T52">RUS:disc</ta>
            <ta e="T54" id="Seg_900" s="T53">RUS:cult</ta>
            <ta e="T60" id="Seg_901" s="T59">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_902" s="T1">A new teacher came to us.</ta>
            <ta e="T9" id="Seg_903" s="T5">We had many Selkups.</ta>
            <ta e="T17" id="Seg_904" s="T9">And he said to one woman: “Teach me to greet [people].</ta>
            <ta e="T20" id="Seg_905" s="T17">I'll greet all women.”</ta>
            <ta e="T26" id="Seg_906" s="T20">And she taught him: “Give me sex.”</ta>
            <ta e="T30" id="Seg_907" s="T26">And he left.</ta>
            <ta e="T33" id="Seg_908" s="T30">He came into a house of one old woman.</ta>
            <ta e="T36" id="Seg_909" s="T33">The old woman was making pancakes.</ta>
            <ta e="T42" id="Seg_910" s="T36">And the teacher said: “Old woman, give me sex.”</ta>
            <ta e="T46" id="Seg_911" s="T42">The old woman began to scold.</ta>
            <ta e="T52" id="Seg_912" s="T46">“You are a teacher and you ask me [for such things].”</ta>
            <ta e="T55" id="Seg_913" s="T52">She started pursuing him with a pan holder.</ta>
            <ta e="T59" id="Seg_914" s="T55">He was pursuing him (me).</ta>
            <ta e="T65" id="Seg_915" s="T59">And I fell down from the stairs.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_916" s="T1">Zu uns kam ein neuer Lehrer.</ta>
            <ta e="T9" id="Seg_917" s="T5">Unter uns waren viele Selkupen.</ta>
            <ta e="T17" id="Seg_918" s="T9">Und er sagte zu einer Frau: "Bring mir bei, [Menschen] zu grüßen.</ta>
            <ta e="T20" id="Seg_919" s="T17">Ich will alle Frauen grüßen."</ta>
            <ta e="T26" id="Seg_920" s="T20">Und sie lehrte ihn: "Gib mir Sex."</ta>
            <ta e="T30" id="Seg_921" s="T26">Und er ging.</ta>
            <ta e="T33" id="Seg_922" s="T30">Er kam in das Haus einer alten Frau.</ta>
            <ta e="T36" id="Seg_923" s="T33">Die alte Frau machte Pfannkuchen.</ta>
            <ta e="T42" id="Seg_924" s="T36">Und der Lehrer sagte: "Alte Frau, gib mir Sex."</ta>
            <ta e="T46" id="Seg_925" s="T42">Die alte Frau begann zu schimpfen.</ta>
            <ta e="T52" id="Seg_926" s="T46">"Du bist ein Lehrer und fragst mich [nach solchen Dingen]."</ta>
            <ta e="T55" id="Seg_927" s="T52">Plötzlich verfolgte sie ihn mit einem Topflappen.</ta>
            <ta e="T59" id="Seg_928" s="T55">Er verfolgte ihn (mich).</ta>
            <ta e="T65" id="Seg_929" s="T59">Und ich fiel die Treppe hinunter.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_930" s="T1">К нам приехал новый учитель.</ta>
            <ta e="T9" id="Seg_931" s="T5">У нас селькупов много было.</ta>
            <ta e="T17" id="Seg_932" s="T9">А он говорит одной женщине: “Меня научи здороваться.</ta>
            <ta e="T20" id="Seg_933" s="T17">Со всеми женщинами здороваться буду”.</ta>
            <ta e="T26" id="Seg_934" s="T20">А она его и научила: “Дай заняться любовью”.</ta>
            <ta e="T30" id="Seg_935" s="T26">И он, ну, пошел.</ta>
            <ta e="T33" id="Seg_936" s="T30">К старухе в избу зашел.</ta>
            <ta e="T36" id="Seg_937" s="T33">Старуха блины печет.</ta>
            <ta e="T42" id="Seg_938" s="T36">А учитель говорит: “Старуха, дай заняться любовью!”</ta>
            <ta e="T46" id="Seg_939" s="T42">Старуха как начала ругаться. </ta>
            <ta e="T52" id="Seg_940" s="T46">“Ты, учитель еще, у меня просить стал”.</ta>
            <ta e="T55" id="Seg_941" s="T52">Как сковородником поймала.</ta>
            <ta e="T59" id="Seg_942" s="T55">Его (меня) погнала.</ta>
            <ta e="T65" id="Seg_943" s="T59">А я с лесенки упал.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_944" s="T1">к нам приехал (Рачковский)</ta>
            <ta e="T9" id="Seg_945" s="T5">у нас остяков много было</ta>
            <ta e="T17" id="Seg_946" s="T9">он говорит одной женщине меня научи здороваться</ta>
            <ta e="T20" id="Seg_947" s="T17">со всеми женщинами здороваться буду</ta>
            <ta e="T26" id="Seg_948" s="T20">а она его и научила поебать дай </ta>
            <ta e="T30" id="Seg_949" s="T26">а он ну и пошел</ta>
            <ta e="T33" id="Seg_950" s="T30">к старухе в избу забежал</ta>
            <ta e="T36" id="Seg_951" s="T33">старуха блины печет</ta>
            <ta e="T42" id="Seg_952" s="T36">старуха поебать дай</ta>
            <ta e="T46" id="Seg_953" s="T42">старуха ругаться начала</ta>
            <ta e="T52" id="Seg_954" s="T46">ты еще учитель у меня еще просить стал</ta>
            <ta e="T55" id="Seg_955" s="T52">поймала</ta>
            <ta e="T59" id="Seg_956" s="T55">меня погнала</ta>
            <ta e="T65" id="Seg_957" s="T59">а я с лесенки упал</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T26" id="Seg_958" s="T20">[BrM:] Tentative analysis of 'talʼel'.</ta>
            <ta e="T42" id="Seg_959" s="T36">[BrM:] Tentative analysis of 'dalʼel'.</ta>
            <ta e="T59" id="Seg_960" s="T55">[BrM:] 'qaːlʼekwatdɨn' changed to 'qaːlʼe kwatdɨn'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
