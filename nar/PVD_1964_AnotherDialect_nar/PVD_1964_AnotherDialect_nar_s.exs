<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_AnotherDialect_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_AnotherDialect_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">23</ud-information>
            <ud-information attribute-name="# HIAT:w">19</ud-information>
            <ud-information attribute-name="# e">19</ud-information>
            <ud-information attribute-name="# HIAT:u">4</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T20" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Menan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">takoj</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">soː</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">sewta</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">A</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">teblanan</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">takoj</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">awaj</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">setta</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_35" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">Man</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">teblan</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">seːm</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">as</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_49" n="HIAT:w" s="T14">sorau</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_53" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">A</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">me</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">seːwtə</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_64" n="HIAT:w" s="T18">wes</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_67" n="HIAT:w" s="T19">soratta</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T20" id="Seg_70" n="sc" s="T1">
               <ts e="T2" id="Seg_72" n="e" s="T1">Menan </ts>
               <ts e="T3" id="Seg_74" n="e" s="T2">takoj </ts>
               <ts e="T4" id="Seg_76" n="e" s="T3">soː </ts>
               <ts e="T5" id="Seg_78" n="e" s="T4">sewta. </ts>
               <ts e="T6" id="Seg_80" n="e" s="T5">A </ts>
               <ts e="T7" id="Seg_82" n="e" s="T6">teblanan </ts>
               <ts e="T8" id="Seg_84" n="e" s="T7">takoj </ts>
               <ts e="T9" id="Seg_86" n="e" s="T8">awaj </ts>
               <ts e="T10" id="Seg_88" n="e" s="T9">setta. </ts>
               <ts e="T11" id="Seg_90" n="e" s="T10">Man </ts>
               <ts e="T12" id="Seg_92" n="e" s="T11">teblan </ts>
               <ts e="T13" id="Seg_94" n="e" s="T12">seːm </ts>
               <ts e="T14" id="Seg_96" n="e" s="T13">as </ts>
               <ts e="T15" id="Seg_98" n="e" s="T14">sorau. </ts>
               <ts e="T16" id="Seg_100" n="e" s="T15">A </ts>
               <ts e="T17" id="Seg_102" n="e" s="T16">me </ts>
               <ts e="T18" id="Seg_104" n="e" s="T17">seːwtə </ts>
               <ts e="T19" id="Seg_106" n="e" s="T18">wes </ts>
               <ts e="T20" id="Seg_108" n="e" s="T19">soratta. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_109" s="T1">PVD_1964_AnotherDialect_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_110" s="T5">PVD_1964_AnotherDialect_nar.002 (001.002)</ta>
            <ta e="T15" id="Seg_111" s="T10">PVD_1964_AnotherDialect_nar.003 (001.003)</ta>
            <ta e="T20" id="Seg_112" s="T15">PVD_1964_AnotherDialect_nar.004 (001.004)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_113" s="T1">ме̨′нан та′кой со̄ ′севта.</ta>
            <ta e="T10" id="Seg_114" s="T5">а тебланан та′кой а′wай ′сетта.</ta>
            <ta e="T15" id="Seg_115" s="T10">ман теб′лан ′се̄м ′ассорау.</ta>
            <ta e="T20" id="Seg_116" s="T15">а ме ′се̄в(ф)тъ вес со′ратта.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_117" s="T1">menan takoj soː sewta.</ta>
            <ta e="T10" id="Seg_118" s="T5">a teblanan takoj awaj setta.</ta>
            <ta e="T15" id="Seg_119" s="T10">man teblan seːm assorau.</ta>
            <ta e="T20" id="Seg_120" s="T15">a me seːw(f)tə wes soratta.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_121" s="T1">Menan takoj soː sewta. </ta>
            <ta e="T10" id="Seg_122" s="T5">A teblanan takoj awaj setta. </ta>
            <ta e="T15" id="Seg_123" s="T10">Man teblan seːm as sorau. </ta>
            <ta e="T20" id="Seg_124" s="T15">A me seːwtə wes soratta. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_125" s="T1">me-nan</ta>
            <ta e="T3" id="Seg_126" s="T2">takoj</ta>
            <ta e="T4" id="Seg_127" s="T3">soː</ta>
            <ta e="T5" id="Seg_128" s="T4">se-wta</ta>
            <ta e="T6" id="Seg_129" s="T5">a</ta>
            <ta e="T7" id="Seg_130" s="T6">teb-la-nan</ta>
            <ta e="T8" id="Seg_131" s="T7">takoj</ta>
            <ta e="T9" id="Seg_132" s="T8">awa-j</ta>
            <ta e="T10" id="Seg_133" s="T9">se-tta</ta>
            <ta e="T11" id="Seg_134" s="T10">man</ta>
            <ta e="T12" id="Seg_135" s="T11">teb-la-n</ta>
            <ta e="T13" id="Seg_136" s="T12">seː-m</ta>
            <ta e="T14" id="Seg_137" s="T13">as</ta>
            <ta e="T15" id="Seg_138" s="T14">sora-u</ta>
            <ta e="T16" id="Seg_139" s="T15">a</ta>
            <ta e="T17" id="Seg_140" s="T16">me</ta>
            <ta e="T18" id="Seg_141" s="T17">seː-wtə</ta>
            <ta e="T19" id="Seg_142" s="T18">wes</ta>
            <ta e="T20" id="Seg_143" s="T19">sora-tta</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_144" s="T1">me-nan</ta>
            <ta e="T3" id="Seg_145" s="T2">takoj</ta>
            <ta e="T4" id="Seg_146" s="T3">soː</ta>
            <ta e="T5" id="Seg_147" s="T4">se-un</ta>
            <ta e="T6" id="Seg_148" s="T5">a</ta>
            <ta e="T7" id="Seg_149" s="T6">täp-la-nan</ta>
            <ta e="T8" id="Seg_150" s="T7">takoj</ta>
            <ta e="T9" id="Seg_151" s="T8">awa-lʼ</ta>
            <ta e="T10" id="Seg_152" s="T9">se-tɨn</ta>
            <ta e="T11" id="Seg_153" s="T10">man</ta>
            <ta e="T12" id="Seg_154" s="T11">täp-la-n</ta>
            <ta e="T13" id="Seg_155" s="T12">se-m</ta>
            <ta e="T14" id="Seg_156" s="T13">asa</ta>
            <ta e="T15" id="Seg_157" s="T14">soːrɨ-w</ta>
            <ta e="T16" id="Seg_158" s="T15">a</ta>
            <ta e="T17" id="Seg_159" s="T16">me</ta>
            <ta e="T18" id="Seg_160" s="T17">se-un</ta>
            <ta e="T19" id="Seg_161" s="T18">wesʼ</ta>
            <ta e="T20" id="Seg_162" s="T19">soːrɨ-tɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_163" s="T1">we-ADES</ta>
            <ta e="T3" id="Seg_164" s="T2">such</ta>
            <ta e="T4" id="Seg_165" s="T3">good</ta>
            <ta e="T5" id="Seg_166" s="T4">language.[NOM]-1PL</ta>
            <ta e="T6" id="Seg_167" s="T5">and</ta>
            <ta e="T7" id="Seg_168" s="T6">(s)he-PL-ADES</ta>
            <ta e="T8" id="Seg_169" s="T7">such</ta>
            <ta e="T9" id="Seg_170" s="T8">bad-DRV</ta>
            <ta e="T10" id="Seg_171" s="T9">language.[NOM]-3PL</ta>
            <ta e="T11" id="Seg_172" s="T10">I.NOM</ta>
            <ta e="T12" id="Seg_173" s="T11">(s)he-PL-GEN</ta>
            <ta e="T13" id="Seg_174" s="T12">language-ACC</ta>
            <ta e="T14" id="Seg_175" s="T13">NEG</ta>
            <ta e="T15" id="Seg_176" s="T14">love-1SG.O</ta>
            <ta e="T16" id="Seg_177" s="T15">and</ta>
            <ta e="T17" id="Seg_178" s="T16">we.GEN</ta>
            <ta e="T18" id="Seg_179" s="T17">language.[NOM]-1PL</ta>
            <ta e="T19" id="Seg_180" s="T18">all</ta>
            <ta e="T20" id="Seg_181" s="T19">love-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_182" s="T1">мы-ADES</ta>
            <ta e="T3" id="Seg_183" s="T2">такой</ta>
            <ta e="T4" id="Seg_184" s="T3">хороший</ta>
            <ta e="T5" id="Seg_185" s="T4">язык.[NOM]-1PL</ta>
            <ta e="T6" id="Seg_186" s="T5">а</ta>
            <ta e="T7" id="Seg_187" s="T6">он(а)-PL-ADES</ta>
            <ta e="T8" id="Seg_188" s="T7">такой</ta>
            <ta e="T9" id="Seg_189" s="T8">плохой-DRV</ta>
            <ta e="T10" id="Seg_190" s="T9">язык.[NOM]-3PL</ta>
            <ta e="T11" id="Seg_191" s="T10">я.NOM</ta>
            <ta e="T12" id="Seg_192" s="T11">он(а)-PL-GEN</ta>
            <ta e="T13" id="Seg_193" s="T12">язык-ACC</ta>
            <ta e="T14" id="Seg_194" s="T13">NEG</ta>
            <ta e="T15" id="Seg_195" s="T14">любить-1SG.O</ta>
            <ta e="T16" id="Seg_196" s="T15">а</ta>
            <ta e="T17" id="Seg_197" s="T16">мы.GEN</ta>
            <ta e="T18" id="Seg_198" s="T17">язык.[NOM]-1PL</ta>
            <ta e="T19" id="Seg_199" s="T18">все</ta>
            <ta e="T20" id="Seg_200" s="T19">любить-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_201" s="T1">pers-n:case</ta>
            <ta e="T3" id="Seg_202" s="T2">dem</ta>
            <ta e="T4" id="Seg_203" s="T3">adj</ta>
            <ta e="T5" id="Seg_204" s="T4">n.[n:case]-n:poss</ta>
            <ta e="T6" id="Seg_205" s="T5">conj</ta>
            <ta e="T7" id="Seg_206" s="T6">pers-n:num-n:case</ta>
            <ta e="T8" id="Seg_207" s="T7">dem</ta>
            <ta e="T9" id="Seg_208" s="T8">adj-adj&gt;adj</ta>
            <ta e="T10" id="Seg_209" s="T9">n.[n:case]-n:poss</ta>
            <ta e="T11" id="Seg_210" s="T10">pers</ta>
            <ta e="T12" id="Seg_211" s="T11">pers-n:num-n:case</ta>
            <ta e="T13" id="Seg_212" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_213" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_214" s="T14">v-v:pn</ta>
            <ta e="T16" id="Seg_215" s="T15">conj</ta>
            <ta e="T17" id="Seg_216" s="T16">pers</ta>
            <ta e="T18" id="Seg_217" s="T17">n.[n:case]-n:poss</ta>
            <ta e="T19" id="Seg_218" s="T18">quant</ta>
            <ta e="T20" id="Seg_219" s="T19">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_220" s="T1">pers</ta>
            <ta e="T3" id="Seg_221" s="T2">dem</ta>
            <ta e="T4" id="Seg_222" s="T3">adj</ta>
            <ta e="T5" id="Seg_223" s="T4">n</ta>
            <ta e="T6" id="Seg_224" s="T5">conj</ta>
            <ta e="T7" id="Seg_225" s="T6">pers</ta>
            <ta e="T8" id="Seg_226" s="T7">dem</ta>
            <ta e="T9" id="Seg_227" s="T8">adj</ta>
            <ta e="T10" id="Seg_228" s="T9">n</ta>
            <ta e="T11" id="Seg_229" s="T10">pers</ta>
            <ta e="T12" id="Seg_230" s="T11">pers</ta>
            <ta e="T13" id="Seg_231" s="T12">n</ta>
            <ta e="T14" id="Seg_232" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_233" s="T14">v</ta>
            <ta e="T16" id="Seg_234" s="T15">conj</ta>
            <ta e="T17" id="Seg_235" s="T16">pers</ta>
            <ta e="T18" id="Seg_236" s="T17">n</ta>
            <ta e="T19" id="Seg_237" s="T18">quant</ta>
            <ta e="T20" id="Seg_238" s="T19">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_239" s="T1">pro.h:Poss</ta>
            <ta e="T5" id="Seg_240" s="T4">np:Th</ta>
            <ta e="T7" id="Seg_241" s="T6">pro.h:Poss</ta>
            <ta e="T10" id="Seg_242" s="T9">np:Th</ta>
            <ta e="T11" id="Seg_243" s="T10">pro.h:E</ta>
            <ta e="T12" id="Seg_244" s="T11">pro.h:Poss</ta>
            <ta e="T13" id="Seg_245" s="T12">np:Th</ta>
            <ta e="T17" id="Seg_246" s="T16">pro.h:Poss</ta>
            <ta e="T18" id="Seg_247" s="T17">np:Th</ta>
            <ta e="T19" id="Seg_248" s="T18">pro.h:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T5" id="Seg_249" s="T4">np:S</ta>
            <ta e="T10" id="Seg_250" s="T9">np:S</ta>
            <ta e="T11" id="Seg_251" s="T10">pro.h:S</ta>
            <ta e="T13" id="Seg_252" s="T12">np:O</ta>
            <ta e="T15" id="Seg_253" s="T14">v:pred</ta>
            <ta e="T18" id="Seg_254" s="T17">np:O</ta>
            <ta e="T19" id="Seg_255" s="T18">pro.h:S</ta>
            <ta e="T20" id="Seg_256" s="T19">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_257" s="T2">RUS:core</ta>
            <ta e="T6" id="Seg_258" s="T5">RUS:gram</ta>
            <ta e="T8" id="Seg_259" s="T7">RUS:core</ta>
            <ta e="T16" id="Seg_260" s="T15">RUS:gram</ta>
            <ta e="T19" id="Seg_261" s="T18">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_262" s="T1">We have such a beautiful language.</ta>
            <ta e="T10" id="Seg_263" s="T5">And their language is so bad.</ta>
            <ta e="T15" id="Seg_264" s="T10">I don't like their language.</ta>
            <ta e="T20" id="Seg_265" s="T15">And our language is loved by everyone.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_266" s="T1">Wir haben so eine schöne Sprache.</ta>
            <ta e="T10" id="Seg_267" s="T5">Und ihre Sprache ist so schlecht.</ta>
            <ta e="T15" id="Seg_268" s="T10">Ich mag ihre Sprache nicht.</ta>
            <ta e="T20" id="Seg_269" s="T15">Und alle lieben unsere Sprache.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_270" s="T1">У нас такой хороший язык.</ta>
            <ta e="T10" id="Seg_271" s="T5">А у них такой плохой язык.</ta>
            <ta e="T15" id="Seg_272" s="T10">Я их язык не люблю.</ta>
            <ta e="T20" id="Seg_273" s="T15">А наш язык все любят.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_274" s="T1">у нас такой хороший язык</ta>
            <ta e="T10" id="Seg_275" s="T5">а у них</ta>
            <ta e="T15" id="Seg_276" s="T10">я их язык не люблю</ta>
            <ta e="T20" id="Seg_277" s="T15">а наш язык все любят</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T15" id="Seg_278" s="T10">[BrM:] 'assorau' changed to 'as sorau'</ta>
            <ta e="T20" id="Seg_279" s="T15">[KuAI:] Variant: 'seːftə'</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
