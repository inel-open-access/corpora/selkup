<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_AtHome_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_AtHome_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">131</ud-information>
            <ud-information attribute-name="# HIAT:w">96</ud-information>
            <ud-information attribute-name="# e">95</ud-information>
            <ud-information attribute-name="# HIAT:u">22</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <timeline-fork end="T13" start="T12">
            <tli id="T12.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T96" id="Seg_0" n="sc" s="T1">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">tap</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">bon</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_12" n="HIAT:ip">(</nts>
                  <ts e="T5" id="Seg_14" n="HIAT:w" s="T4">nonʼčʼe</ts>
                  <nts id="Seg_15" n="HIAT:ip">)</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_18" n="HIAT:w" s="T5">jedot</ts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_21" n="HIAT:w" s="T6">qwatǯan</ts>
                  <nts id="Seg_22" n="HIAT:ip">.</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_25" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_27" n="HIAT:w" s="T7">Menan</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">jedoɣon</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_33" n="HIAT:w" s="T9">qula</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_36" n="HIAT:w" s="T10">koːcʼin</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_39" n="HIAT:w" s="T11">kupbat</ts>
                  <nts id="Seg_40" n="HIAT:ip">.</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_43" n="HIAT:u" s="T12">
                  <ts e="T12.tx.1" id="Seg_45" n="HIAT:w" s="T12">Moʒət</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12.tx.1">bɨtʼ</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">ešʼo</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">kutǯat</ts>
                  <nts id="Seg_55" n="HIAT:ip">.</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_58" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_60" n="HIAT:w" s="T15">Qwatǯan</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_63" n="HIAT:w" s="T16">qwɛlɨsku</ts>
                  <nts id="Seg_64" n="HIAT:ip">.</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_67" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_69" n="HIAT:w" s="T17">Natʼen</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_72" n="HIAT:w" s="T18">qwɛlɨm</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">koːcʼin</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">amǯau</ts>
                  <nts id="Seg_79" n="HIAT:ip">.</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_82" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_84" n="HIAT:w" s="T21">Perwɨj</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_87" n="HIAT:w" s="T22">ras</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_90" n="HIAT:w" s="T23">amǯau</ts>
                  <nts id="Seg_91" n="HIAT:ip">,</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">amǯau</ts>
                  <nts id="Seg_95" n="HIAT:ip">,</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">a</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_101" n="HIAT:w" s="T26">patom</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_104" n="HIAT:w" s="T27">jas</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_107" n="HIAT:w" s="T28">kɨgeǯau</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">amgu</ts>
                  <nts id="Seg_111" n="HIAT:ip">.</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_114" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_116" n="HIAT:w" s="T30">Potdəbon</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_119" n="HIAT:w" s="T31">man</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_122" n="HIAT:w" s="T32">tʼulʼe</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_125" n="HIAT:w" s="T33">medan</ts>
                  <nts id="Seg_126" n="HIAT:ip">.</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_129" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_131" n="HIAT:w" s="T34">Qwɛl</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_134" n="HIAT:w" s="T35">teblanan</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_137" n="HIAT:w" s="T36">koːcʼin</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_140" n="HIAT:w" s="T37">jes</ts>
                  <nts id="Seg_141" n="HIAT:ip">.</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_144" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_146" n="HIAT:w" s="T38">Warɣə</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_149" n="HIAT:w" s="T39">iːɣən</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_152" n="HIAT:w" s="T40">sernan</ts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_156" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_158" n="HIAT:w" s="T41">Tebnan</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_161" n="HIAT:w" s="T42">iːbɨgaj</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_164" n="HIAT:w" s="T43">tudola</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_167" n="HIAT:w" s="T44">jezatdə</ts>
                  <nts id="Seg_168" n="HIAT:ip">.</nts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_171" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_173" n="HIAT:w" s="T45">Patom</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_176" n="HIAT:w" s="T46">sɨdəmdettɨ</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_179" n="HIAT:w" s="T47">iːɣän</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_182" n="HIAT:w" s="T48">qwannan</ts>
                  <nts id="Seg_183" n="HIAT:ip">.</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_186" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_188" n="HIAT:w" s="T49">Täbnan</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_191" n="HIAT:w" s="T50">tudolam</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_194" n="HIAT:w" s="T51">potpat</ts>
                  <nts id="Seg_195" n="HIAT:ip">.</nts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_198" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_200" n="HIAT:w" s="T52">Tebnan</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_203" n="HIAT:w" s="T53">šaribɨdi</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_206" n="HIAT:w" s="T54">tudolat</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_209" n="HIAT:w" s="T55">jezattə</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_212" n="HIAT:w" s="T56">i</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_215" n="HIAT:w" s="T57">wadʼit</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_218" n="HIAT:w" s="T58">čugun</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_221" n="HIAT:w" s="T59">pʼlʼäka</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_224" n="HIAT:w" s="T60">potpədi</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_227" n="HIAT:w" s="T61">jes</ts>
                  <nts id="Seg_228" n="HIAT:ip">.</nts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_231" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_233" n="HIAT:w" s="T62">Man</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_236" n="HIAT:w" s="T63">aj</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_239" n="HIAT:w" s="T64">aurnan</ts>
                  <nts id="Seg_240" n="HIAT:ip">.</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_243" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_245" n="HIAT:w" s="T65">Patom</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_248" n="HIAT:w" s="T66">kuronnan</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_251" n="HIAT:w" s="T67">dʼaja</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_254" n="HIAT:w" s="T68">Mixajlan</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_257" n="HIAT:w" s="T69">maːttə</ts>
                  <nts id="Seg_258" n="HIAT:ip">.</nts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_261" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_263" n="HIAT:w" s="T70">Tebla</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_266" n="HIAT:w" s="T71">tʼärattə</ts>
                  <nts id="Seg_267" n="HIAT:ip">:</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_269" n="HIAT:ip">“</nts>
                  <ts e="T73" id="Seg_271" n="HIAT:w" s="T72">Omdag</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_274" n="HIAT:w" s="T73">aurgu</ts>
                  <nts id="Seg_275" n="HIAT:ip">.</nts>
                  <nts id="Seg_276" n="HIAT:ip">”</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_279" n="HIAT:u" s="T74">
                  <nts id="Seg_280" n="HIAT:ip">“</nts>
                  <ts e="T75" id="Seg_282" n="HIAT:w" s="T74">As</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_285" n="HIAT:w" s="T75">kɨgan</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_288" n="HIAT:w" s="T76">man</ts>
                  <nts id="Seg_289" n="HIAT:ip">.</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_292" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_294" n="HIAT:w" s="T77">Man</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_297" n="HIAT:w" s="T78">sədə</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_300" n="HIAT:w" s="T79">maːtqɨn</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_303" n="HIAT:w" s="T80">aurban</ts>
                  <nts id="Seg_304" n="HIAT:ip">.</nts>
                  <nts id="Seg_305" n="HIAT:ip">”</nts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_308" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_310" n="HIAT:w" s="T81">A</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_313" n="HIAT:w" s="T82">täbla</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_316" n="HIAT:w" s="T83">tʼärattə</ts>
                  <nts id="Seg_317" n="HIAT:ip">:</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_319" n="HIAT:ip">“</nts>
                  <ts e="T85" id="Seg_321" n="HIAT:w" s="T84">Omdaq</ts>
                  <nts id="Seg_322" n="HIAT:ip">.</nts>
                  <nts id="Seg_323" n="HIAT:ip">”</nts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_326" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_328" n="HIAT:w" s="T85">Man</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_331" n="HIAT:w" s="T86">jes</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_334" n="HIAT:w" s="T87">kɨgan</ts>
                  <nts id="Seg_335" n="HIAT:ip">,</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_338" n="HIAT:w" s="T88">nu</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_341" n="HIAT:w" s="T89">aj</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_344" n="HIAT:w" s="T90">omdan</ts>
                  <nts id="Seg_345" n="HIAT:ip">.</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_348" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_350" n="HIAT:w" s="T91">Omdɨgu</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_353" n="HIAT:w" s="T92">nadə</ts>
                  <nts id="Seg_354" n="HIAT:ip">.</nts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_357" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_359" n="HIAT:w" s="T93">Malʼenʼko</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_362" n="HIAT:w" s="T94">aj</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_365" n="HIAT:w" s="T95">aurnan</ts>
                  <nts id="Seg_366" n="HIAT:ip">.</nts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T96" id="Seg_368" n="sc" s="T1">
               <ts e="T2" id="Seg_370" n="e" s="T1">Man </ts>
               <ts e="T3" id="Seg_372" n="e" s="T2">tap </ts>
               <ts e="T4" id="Seg_374" n="e" s="T3">bon </ts>
               <ts e="T5" id="Seg_376" n="e" s="T4">(nonʼčʼe) </ts>
               <ts e="T6" id="Seg_378" n="e" s="T5">jedot </ts>
               <ts e="T7" id="Seg_380" n="e" s="T6">qwatǯan. </ts>
               <ts e="T8" id="Seg_382" n="e" s="T7">Menan </ts>
               <ts e="T9" id="Seg_384" n="e" s="T8">jedoɣon </ts>
               <ts e="T10" id="Seg_386" n="e" s="T9">qula </ts>
               <ts e="T11" id="Seg_388" n="e" s="T10">koːcʼin </ts>
               <ts e="T12" id="Seg_390" n="e" s="T11">kupbat. </ts>
               <ts e="T13" id="Seg_392" n="e" s="T12">Moʒət bɨtʼ </ts>
               <ts e="T14" id="Seg_394" n="e" s="T13">ešʼo </ts>
               <ts e="T15" id="Seg_396" n="e" s="T14">kutǯat. </ts>
               <ts e="T16" id="Seg_398" n="e" s="T15">Qwatǯan </ts>
               <ts e="T17" id="Seg_400" n="e" s="T16">qwɛlɨsku. </ts>
               <ts e="T18" id="Seg_402" n="e" s="T17">Natʼen </ts>
               <ts e="T19" id="Seg_404" n="e" s="T18">qwɛlɨm </ts>
               <ts e="T20" id="Seg_406" n="e" s="T19">koːcʼin </ts>
               <ts e="T21" id="Seg_408" n="e" s="T20">amǯau. </ts>
               <ts e="T22" id="Seg_410" n="e" s="T21">Perwɨj </ts>
               <ts e="T23" id="Seg_412" n="e" s="T22">ras </ts>
               <ts e="T24" id="Seg_414" n="e" s="T23">amǯau, </ts>
               <ts e="T25" id="Seg_416" n="e" s="T24">amǯau, </ts>
               <ts e="T26" id="Seg_418" n="e" s="T25">a </ts>
               <ts e="T27" id="Seg_420" n="e" s="T26">patom </ts>
               <ts e="T28" id="Seg_422" n="e" s="T27">jas </ts>
               <ts e="T29" id="Seg_424" n="e" s="T28">kɨgeǯau </ts>
               <ts e="T30" id="Seg_426" n="e" s="T29">amgu. </ts>
               <ts e="T31" id="Seg_428" n="e" s="T30">Potdəbon </ts>
               <ts e="T32" id="Seg_430" n="e" s="T31">man </ts>
               <ts e="T33" id="Seg_432" n="e" s="T32">tʼulʼe </ts>
               <ts e="T34" id="Seg_434" n="e" s="T33">medan. </ts>
               <ts e="T35" id="Seg_436" n="e" s="T34">Qwɛl </ts>
               <ts e="T36" id="Seg_438" n="e" s="T35">teblanan </ts>
               <ts e="T37" id="Seg_440" n="e" s="T36">koːcʼin </ts>
               <ts e="T38" id="Seg_442" n="e" s="T37">jes. </ts>
               <ts e="T39" id="Seg_444" n="e" s="T38">Warɣə </ts>
               <ts e="T40" id="Seg_446" n="e" s="T39">iːɣən </ts>
               <ts e="T41" id="Seg_448" n="e" s="T40">sernan. </ts>
               <ts e="T42" id="Seg_450" n="e" s="T41">Tebnan </ts>
               <ts e="T43" id="Seg_452" n="e" s="T42">iːbɨgaj </ts>
               <ts e="T44" id="Seg_454" n="e" s="T43">tudola </ts>
               <ts e="T45" id="Seg_456" n="e" s="T44">jezatdə. </ts>
               <ts e="T46" id="Seg_458" n="e" s="T45">Patom </ts>
               <ts e="T47" id="Seg_460" n="e" s="T46">sɨdəmdettɨ </ts>
               <ts e="T48" id="Seg_462" n="e" s="T47">iːɣän </ts>
               <ts e="T49" id="Seg_464" n="e" s="T48">qwannan. </ts>
               <ts e="T50" id="Seg_466" n="e" s="T49">Täbnan </ts>
               <ts e="T51" id="Seg_468" n="e" s="T50">tudolam </ts>
               <ts e="T52" id="Seg_470" n="e" s="T51">potpat. </ts>
               <ts e="T53" id="Seg_472" n="e" s="T52">Tebnan </ts>
               <ts e="T54" id="Seg_474" n="e" s="T53">šaribɨdi </ts>
               <ts e="T55" id="Seg_476" n="e" s="T54">tudolat </ts>
               <ts e="T56" id="Seg_478" n="e" s="T55">jezattə </ts>
               <ts e="T57" id="Seg_480" n="e" s="T56">i </ts>
               <ts e="T58" id="Seg_482" n="e" s="T57">wadʼit </ts>
               <ts e="T59" id="Seg_484" n="e" s="T58">čugun </ts>
               <ts e="T60" id="Seg_486" n="e" s="T59">pʼlʼäka </ts>
               <ts e="T61" id="Seg_488" n="e" s="T60">potpədi </ts>
               <ts e="T62" id="Seg_490" n="e" s="T61">jes. </ts>
               <ts e="T63" id="Seg_492" n="e" s="T62">Man </ts>
               <ts e="T64" id="Seg_494" n="e" s="T63">aj </ts>
               <ts e="T65" id="Seg_496" n="e" s="T64">aurnan. </ts>
               <ts e="T66" id="Seg_498" n="e" s="T65">Patom </ts>
               <ts e="T67" id="Seg_500" n="e" s="T66">kuronnan </ts>
               <ts e="T68" id="Seg_502" n="e" s="T67">dʼaja </ts>
               <ts e="T69" id="Seg_504" n="e" s="T68">Mixajlan </ts>
               <ts e="T70" id="Seg_506" n="e" s="T69">maːttə. </ts>
               <ts e="T71" id="Seg_508" n="e" s="T70">Tebla </ts>
               <ts e="T72" id="Seg_510" n="e" s="T71">tʼärattə: </ts>
               <ts e="T73" id="Seg_512" n="e" s="T72">“Omdag </ts>
               <ts e="T74" id="Seg_514" n="e" s="T73">aurgu.” </ts>
               <ts e="T75" id="Seg_516" n="e" s="T74">“As </ts>
               <ts e="T76" id="Seg_518" n="e" s="T75">kɨgan </ts>
               <ts e="T77" id="Seg_520" n="e" s="T76">man. </ts>
               <ts e="T78" id="Seg_522" n="e" s="T77">Man </ts>
               <ts e="T79" id="Seg_524" n="e" s="T78">sədə </ts>
               <ts e="T80" id="Seg_526" n="e" s="T79">maːtqɨn </ts>
               <ts e="T81" id="Seg_528" n="e" s="T80">aurban.” </ts>
               <ts e="T82" id="Seg_530" n="e" s="T81">A </ts>
               <ts e="T83" id="Seg_532" n="e" s="T82">täbla </ts>
               <ts e="T84" id="Seg_534" n="e" s="T83">tʼärattə: </ts>
               <ts e="T85" id="Seg_536" n="e" s="T84">“Omdaq.” </ts>
               <ts e="T86" id="Seg_538" n="e" s="T85">Man </ts>
               <ts e="T87" id="Seg_540" n="e" s="T86">jes </ts>
               <ts e="T88" id="Seg_542" n="e" s="T87">kɨgan, </ts>
               <ts e="T89" id="Seg_544" n="e" s="T88">nu </ts>
               <ts e="T90" id="Seg_546" n="e" s="T89">aj </ts>
               <ts e="T91" id="Seg_548" n="e" s="T90">omdan. </ts>
               <ts e="T92" id="Seg_550" n="e" s="T91">Omdɨgu </ts>
               <ts e="T93" id="Seg_552" n="e" s="T92">nadə. </ts>
               <ts e="T94" id="Seg_554" n="e" s="T93">Malʼenʼko </ts>
               <ts e="T95" id="Seg_556" n="e" s="T94">aj </ts>
               <ts e="T96" id="Seg_558" n="e" s="T95">aurnan. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_559" s="T1">PVD_1964_AtHome_nar.001 (001.001)</ta>
            <ta e="T12" id="Seg_560" s="T7">PVD_1964_AtHome_nar.002 (001.002)</ta>
            <ta e="T15" id="Seg_561" s="T12">PVD_1964_AtHome_nar.003 (001.003)</ta>
            <ta e="T17" id="Seg_562" s="T15">PVD_1964_AtHome_nar.004 (001.004)</ta>
            <ta e="T21" id="Seg_563" s="T17">PVD_1964_AtHome_nar.005 (001.005)</ta>
            <ta e="T30" id="Seg_564" s="T21">PVD_1964_AtHome_nar.006 (001.006)</ta>
            <ta e="T34" id="Seg_565" s="T30">PVD_1964_AtHome_nar.007 (001.007)</ta>
            <ta e="T38" id="Seg_566" s="T34">PVD_1964_AtHome_nar.008 (001.008)</ta>
            <ta e="T41" id="Seg_567" s="T38">PVD_1964_AtHome_nar.009 (001.009)</ta>
            <ta e="T45" id="Seg_568" s="T41">PVD_1964_AtHome_nar.010 (001.010)</ta>
            <ta e="T49" id="Seg_569" s="T45">PVD_1964_AtHome_nar.011 (001.011)</ta>
            <ta e="T52" id="Seg_570" s="T49">PVD_1964_AtHome_nar.012 (001.012)</ta>
            <ta e="T62" id="Seg_571" s="T52">PVD_1964_AtHome_nar.013 (001.013)</ta>
            <ta e="T65" id="Seg_572" s="T62">PVD_1964_AtHome_nar.014 (001.014)</ta>
            <ta e="T70" id="Seg_573" s="T65">PVD_1964_AtHome_nar.015 (001.015)</ta>
            <ta e="T74" id="Seg_574" s="T70">PVD_1964_AtHome_nar.016 (001.016)</ta>
            <ta e="T77" id="Seg_575" s="T74">PVD_1964_AtHome_nar.017 (001.017)</ta>
            <ta e="T81" id="Seg_576" s="T77">PVD_1964_AtHome_nar.018 (001.018)</ta>
            <ta e="T85" id="Seg_577" s="T81">PVD_1964_AtHome_nar.019 (001.019)</ta>
            <ta e="T91" id="Seg_578" s="T85">PVD_1964_AtHome_nar.020 (001.020)</ta>
            <ta e="T93" id="Seg_579" s="T91">PVD_1964_AtHome_nar.021 (001.021)</ta>
            <ta e="T96" id="Seg_580" s="T93">PVD_1964_AtHome_nar.022 (001.022)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T7" id="Seg_581" s="T1">ман тап′бон (нонʼче) ′jедот kwат′джан.</ta>
            <ta e="T12" id="Seg_582" s="T7">ме′нан ′jедоɣон kу′lа ко̄цʼин ′купбат.</ta>
            <ta e="T15" id="Seg_583" s="T12">′можът бытʼ е′що ′кутджат.</ta>
            <ta e="T17" id="Seg_584" s="T15">kwат′джан ′kwɛл(l)ыску.</ta>
            <ta e="T21" id="Seg_585" s="T17">на′тʼен ′kwɛлым ′ко̄цʼин ам′джау.</ta>
            <ta e="T30" id="Seg_586" s="T21">′первый рас ам′джау, ам′джау, а па′том jас кы′геджау ′амгу.</ta>
            <ta e="T34" id="Seg_587" s="T30">′потдъбон ман ′тʼулʼе ме′дан.</ta>
            <ta e="T38" id="Seg_588" s="T34">kwɛл теб′ланан ко̄цʼин jес.</ta>
            <ta e="T41" id="Seg_589" s="T38">варɣъ ′ӣɣън ′сернан.</ta>
            <ta e="T45" id="Seg_590" s="T41">теб′нан ′ӣбыгай ту′дола ′jезатдъ.</ta>
            <ta e="T49" id="Seg_591" s="T45">па′том ′сыдъм ′детты ′ӣɣӓн kwан′нан.</ta>
            <ta e="T52" id="Seg_592" s="T49">тӓб′нан ту′долам ′потпат.</ta>
            <ta e="T62" id="Seg_593" s="T52">теб′нан ′шарибыди ту′долат ′jезаттъ и вадʼит чу′гун пʼлʼӓ′ка ′потпъди jес.</ta>
            <ta e="T65" id="Seg_594" s="T62">ман ай ′аурнан.</ta>
            <ta e="T70" id="Seg_595" s="T65">па′том ку′роннан дʼаjа Ми′хайлан ′ма̄ттъ(ы).</ta>
            <ta e="T74" id="Seg_596" s="T70">теб′ла тʼӓ′раттъ: ом′даг аур′гу.</ta>
            <ta e="T77" id="Seg_597" s="T74">ас кы′ган ман.</ta>
            <ta e="T81" id="Seg_598" s="T77">ман ′съдъ ′ма̄тkын ′аурбан.</ta>
            <ta e="T85" id="Seg_599" s="T81">а тӓб′ла тʼӓ′раттъ: ом′даk.</ta>
            <ta e="T91" id="Seg_600" s="T85">ман jес кы′ган, ну ай ом′дан.</ta>
            <ta e="T93" id="Seg_601" s="T91">омды′гу надъ.</ta>
            <ta e="T96" id="Seg_602" s="T93">малʼенʼко ′ай ′аурнан.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T7" id="Seg_603" s="T1">man tapbon (nonʼče) jedot qwatǯan.</ta>
            <ta e="T12" id="Seg_604" s="T7">menan jedoɣon qula koːcʼin kupbat.</ta>
            <ta e="T15" id="Seg_605" s="T12">moʒət bɨtʼ ešo kutǯat.</ta>
            <ta e="T17" id="Seg_606" s="T15">qwatǯan qwɛl(l)ɨsku.</ta>
            <ta e="T21" id="Seg_607" s="T17">natʼen qwɛlɨm koːcʼin amǯau.</ta>
            <ta e="T30" id="Seg_608" s="T21">perwɨj ras amǯau, amǯau, a patom jas kɨgeǯau amgu.</ta>
            <ta e="T34" id="Seg_609" s="T30">potdəbon man tʼulʼe medan.</ta>
            <ta e="T38" id="Seg_610" s="T34">qwɛl teblanan koːcʼin jes.</ta>
            <ta e="T41" id="Seg_611" s="T38">warɣə iːɣən sernan.</ta>
            <ta e="T45" id="Seg_612" s="T41">tebnan iːbɨgaj tudola jezatdə.</ta>
            <ta e="T49" id="Seg_613" s="T45">patom sɨdəm dettɨ iːɣän qwannan.</ta>
            <ta e="T52" id="Seg_614" s="T49">täbnan tudolam potpat.</ta>
            <ta e="T62" id="Seg_615" s="T52">tebnan šaribɨdi tudolat jezattə i wadʼit čugun pʼlʼäka potpədi jes.</ta>
            <ta e="T65" id="Seg_616" s="T62">man aj aurnan.</ta>
            <ta e="T70" id="Seg_617" s="T65">patom kuronnan dʼaja Мixajlan maːttə(ɨ).</ta>
            <ta e="T74" id="Seg_618" s="T70">tebla tʼärattə: omdag aurgu.</ta>
            <ta e="T77" id="Seg_619" s="T74">as kɨgan man.</ta>
            <ta e="T81" id="Seg_620" s="T77">man sədə maːtqɨn aurban.</ta>
            <ta e="T85" id="Seg_621" s="T81">a täbla tʼärattə: omdaq.</ta>
            <ta e="T91" id="Seg_622" s="T85">man jes kɨgan, nu aj omdan.</ta>
            <ta e="T93" id="Seg_623" s="T91">omdɨgu nadə.</ta>
            <ta e="T96" id="Seg_624" s="T93">malʼenʼko aj aurnan.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_625" s="T1">Man tap bon (nonʼčʼe) jedot qwatǯan. </ta>
            <ta e="T12" id="Seg_626" s="T7">Menan jedoɣon qula koːcʼin kupbat. </ta>
            <ta e="T15" id="Seg_627" s="T12">Moʒət bɨtʼ ešʼo kutǯat. </ta>
            <ta e="T17" id="Seg_628" s="T15">Qwatǯan qwɛlɨsku. </ta>
            <ta e="T21" id="Seg_629" s="T17">Natʼen qwɛlɨm koːcʼin amǯau. </ta>
            <ta e="T30" id="Seg_630" s="T21">Perwɨj ras amǯau, amǯau, a patom jas kɨgeǯau amgu. </ta>
            <ta e="T34" id="Seg_631" s="T30">Potdəbon man tʼulʼe medan. </ta>
            <ta e="T38" id="Seg_632" s="T34">Qwɛl teblanan koːcʼin jes. </ta>
            <ta e="T41" id="Seg_633" s="T38">Warɣə iːɣən sernan. </ta>
            <ta e="T45" id="Seg_634" s="T41">Tebnan iːbɨgaj tudola jezatdə. </ta>
            <ta e="T49" id="Seg_635" s="T45">Patom sɨdəmdettɨ iːɣän qwannan. </ta>
            <ta e="T52" id="Seg_636" s="T49">Täbnan tudolam potpat. </ta>
            <ta e="T62" id="Seg_637" s="T52">Tebnan šaribɨdi tudolat jezattə i wadʼit čugun pʼlʼäka potpədi jes. </ta>
            <ta e="T65" id="Seg_638" s="T62">Man aj aurnan. </ta>
            <ta e="T70" id="Seg_639" s="T65">Patom kuronnan dʼaja Mixajlan maːttə. </ta>
            <ta e="T74" id="Seg_640" s="T70">Tebla tʼärattə: “Omdag aurgu.” </ta>
            <ta e="T77" id="Seg_641" s="T74">“As kɨgan man. </ta>
            <ta e="T81" id="Seg_642" s="T77">Man sədə maːtqɨn aurban.” </ta>
            <ta e="T85" id="Seg_643" s="T81">A täbla tʼärattə: “Omdaq.” </ta>
            <ta e="T91" id="Seg_644" s="T85">Man jes kɨgan, nu aj omdan. </ta>
            <ta e="T93" id="Seg_645" s="T91">Omdɨgu nadə. </ta>
            <ta e="T96" id="Seg_646" s="T93">Malʼenʼko aj aurnan. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_647" s="T1">man</ta>
            <ta e="T3" id="Seg_648" s="T2">tap</ta>
            <ta e="T4" id="Seg_649" s="T3">bo-n</ta>
            <ta e="T5" id="Seg_650" s="T4">nonʼčʼe</ta>
            <ta e="T6" id="Seg_651" s="T5">jedo-t</ta>
            <ta e="T7" id="Seg_652" s="T6">qwat-ǯa-n</ta>
            <ta e="T8" id="Seg_653" s="T7">me-nan</ta>
            <ta e="T9" id="Seg_654" s="T8">jedo-ɣon</ta>
            <ta e="T10" id="Seg_655" s="T9">qu-la</ta>
            <ta e="T11" id="Seg_656" s="T10">koːcʼi-n</ta>
            <ta e="T12" id="Seg_657" s="T11">ku-pba-t</ta>
            <ta e="T13" id="Seg_658" s="T12">moʒət_bɨtʼ</ta>
            <ta e="T14" id="Seg_659" s="T13">ešʼo</ta>
            <ta e="T15" id="Seg_660" s="T14">ku-t-ǯa-t</ta>
            <ta e="T16" id="Seg_661" s="T15">qwat-ǯa-n</ta>
            <ta e="T17" id="Seg_662" s="T16">qwɛl-ɨ-s-ku</ta>
            <ta e="T18" id="Seg_663" s="T17">natʼen</ta>
            <ta e="T19" id="Seg_664" s="T18">qwɛl-ɨ-m</ta>
            <ta e="T20" id="Seg_665" s="T19">koːcʼi-n</ta>
            <ta e="T21" id="Seg_666" s="T20">am-ǯa-u</ta>
            <ta e="T22" id="Seg_667" s="T21">perwɨj</ta>
            <ta e="T23" id="Seg_668" s="T22">ras</ta>
            <ta e="T24" id="Seg_669" s="T23">am-ǯa-u</ta>
            <ta e="T25" id="Seg_670" s="T24">am-ǯa-u</ta>
            <ta e="T26" id="Seg_671" s="T25">a</ta>
            <ta e="T27" id="Seg_672" s="T26">patom</ta>
            <ta e="T28" id="Seg_673" s="T27">jas</ta>
            <ta e="T29" id="Seg_674" s="T28">kɨg-eǯa-u</ta>
            <ta e="T30" id="Seg_675" s="T29">am-gu</ta>
            <ta e="T31" id="Seg_676" s="T30">po-t-də-bo-n</ta>
            <ta e="T32" id="Seg_677" s="T31">man</ta>
            <ta e="T33" id="Seg_678" s="T32">tʼu-lʼe</ta>
            <ta e="T34" id="Seg_679" s="T33">meda-n</ta>
            <ta e="T35" id="Seg_680" s="T34">qwɛl</ta>
            <ta e="T36" id="Seg_681" s="T35">teb-la-nan</ta>
            <ta e="T37" id="Seg_682" s="T36">koːcʼi-n</ta>
            <ta e="T38" id="Seg_683" s="T37">je-s</ta>
            <ta e="T39" id="Seg_684" s="T38">warɣə</ta>
            <ta e="T40" id="Seg_685" s="T39">iː-ɣən</ta>
            <ta e="T41" id="Seg_686" s="T40">ser-na-n</ta>
            <ta e="T42" id="Seg_687" s="T41">teb-nan</ta>
            <ta e="T43" id="Seg_688" s="T42">iːbɨgaj</ta>
            <ta e="T44" id="Seg_689" s="T43">tudo-la</ta>
            <ta e="T45" id="Seg_690" s="T44">je-za-tdə</ta>
            <ta e="T46" id="Seg_691" s="T45">patom</ta>
            <ta e="T47" id="Seg_692" s="T46">sɨdə-mdettɨ</ta>
            <ta e="T48" id="Seg_693" s="T47">iː-ɣän</ta>
            <ta e="T49" id="Seg_694" s="T48">qwa-na-n</ta>
            <ta e="T50" id="Seg_695" s="T49">täb-nan</ta>
            <ta e="T51" id="Seg_696" s="T50">tudo-la-m</ta>
            <ta e="T52" id="Seg_697" s="T51">pot-pa-t</ta>
            <ta e="T53" id="Seg_698" s="T52">teb-nan</ta>
            <ta e="T54" id="Seg_699" s="T53">šari-bɨdi</ta>
            <ta e="T55" id="Seg_700" s="T54">tudo-la-t</ta>
            <ta e="T56" id="Seg_701" s="T55">je-za-ttə</ta>
            <ta e="T57" id="Seg_702" s="T56">i</ta>
            <ta e="T58" id="Seg_703" s="T57">wadʼi-t</ta>
            <ta e="T59" id="Seg_704" s="T58">čugun</ta>
            <ta e="T60" id="Seg_705" s="T59">pʼlʼäka</ta>
            <ta e="T61" id="Seg_706" s="T60">pot-pədi</ta>
            <ta e="T62" id="Seg_707" s="T61">je-s</ta>
            <ta e="T63" id="Seg_708" s="T62">man</ta>
            <ta e="T64" id="Seg_709" s="T63">aj</ta>
            <ta e="T65" id="Seg_710" s="T64">au-r-na-n</ta>
            <ta e="T66" id="Seg_711" s="T65">patom</ta>
            <ta e="T67" id="Seg_712" s="T66">kur-on-na-n</ta>
            <ta e="T68" id="Seg_713" s="T67">dʼaja</ta>
            <ta e="T69" id="Seg_714" s="T68">Mixajla-n</ta>
            <ta e="T70" id="Seg_715" s="T69">maːt-tə</ta>
            <ta e="T71" id="Seg_716" s="T70">teb-la</ta>
            <ta e="T72" id="Seg_717" s="T71">tʼära-ttə</ta>
            <ta e="T73" id="Seg_718" s="T72">omda-g</ta>
            <ta e="T74" id="Seg_719" s="T73">au-r-gu</ta>
            <ta e="T75" id="Seg_720" s="T74">as</ta>
            <ta e="T76" id="Seg_721" s="T75">kɨga-n</ta>
            <ta e="T77" id="Seg_722" s="T76">man</ta>
            <ta e="T78" id="Seg_723" s="T77">man</ta>
            <ta e="T79" id="Seg_724" s="T78">sədə</ta>
            <ta e="T80" id="Seg_725" s="T79">maːt-qɨn</ta>
            <ta e="T81" id="Seg_726" s="T80">au-r-ba-n</ta>
            <ta e="T82" id="Seg_727" s="T81">a</ta>
            <ta e="T83" id="Seg_728" s="T82">täb-la</ta>
            <ta e="T84" id="Seg_729" s="T83">tʼära-ttə</ta>
            <ta e="T85" id="Seg_730" s="T84">omda-q</ta>
            <ta e="T86" id="Seg_731" s="T85">man</ta>
            <ta e="T87" id="Seg_732" s="T86">jes</ta>
            <ta e="T88" id="Seg_733" s="T87">kɨga-n</ta>
            <ta e="T89" id="Seg_734" s="T88">nu</ta>
            <ta e="T90" id="Seg_735" s="T89">aj</ta>
            <ta e="T91" id="Seg_736" s="T90">omda-n</ta>
            <ta e="T92" id="Seg_737" s="T91">omdɨ-gu</ta>
            <ta e="T93" id="Seg_738" s="T92">nadə</ta>
            <ta e="T94" id="Seg_739" s="T93">malʼenʼko</ta>
            <ta e="T95" id="Seg_740" s="T94">aj</ta>
            <ta e="T96" id="Seg_741" s="T95">au-r-na-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_742" s="T1">man</ta>
            <ta e="T3" id="Seg_743" s="T2">taw</ta>
            <ta e="T4" id="Seg_744" s="T3">po-n</ta>
            <ta e="T5" id="Seg_745" s="T4">nonʼčʼe</ta>
            <ta e="T6" id="Seg_746" s="T5">eːde-ntə</ta>
            <ta e="T7" id="Seg_747" s="T6">qwan-enǯɨ-ŋ</ta>
            <ta e="T8" id="Seg_748" s="T7">me-nan</ta>
            <ta e="T9" id="Seg_749" s="T8">eːde-qɨn</ta>
            <ta e="T10" id="Seg_750" s="T9">qum-la</ta>
            <ta e="T11" id="Seg_751" s="T10">koːci-ŋ</ta>
            <ta e="T12" id="Seg_752" s="T11">quː-mbɨ-tɨn</ta>
            <ta e="T13" id="Seg_753" s="T12">moʒət_bɨtʼ</ta>
            <ta e="T14" id="Seg_754" s="T13">ešo</ta>
            <ta e="T15" id="Seg_755" s="T14">quː-t-enǯɨ-tɨn</ta>
            <ta e="T16" id="Seg_756" s="T15">qwan-enǯɨ-ŋ</ta>
            <ta e="T17" id="Seg_757" s="T16">qwɛl-ɨ-s-gu</ta>
            <ta e="T18" id="Seg_758" s="T17">natʼen</ta>
            <ta e="T19" id="Seg_759" s="T18">qwɛl-ɨ-m</ta>
            <ta e="T20" id="Seg_760" s="T19">koːci-ŋ</ta>
            <ta e="T21" id="Seg_761" s="T20">am-enǯɨ-w</ta>
            <ta e="T22" id="Seg_762" s="T21">perwɨj</ta>
            <ta e="T23" id="Seg_763" s="T22">ras</ta>
            <ta e="T24" id="Seg_764" s="T23">am-enǯɨ-w</ta>
            <ta e="T25" id="Seg_765" s="T24">am-enǯɨ-w</ta>
            <ta e="T26" id="Seg_766" s="T25">a</ta>
            <ta e="T27" id="Seg_767" s="T26">patom</ta>
            <ta e="T28" id="Seg_768" s="T27">asa</ta>
            <ta e="T29" id="Seg_769" s="T28">kɨgɨ-enǯɨ-w</ta>
            <ta e="T30" id="Seg_770" s="T29">am-gu</ta>
            <ta e="T31" id="Seg_771" s="T30">po-n-tə-po-n</ta>
            <ta e="T32" id="Seg_772" s="T31">man</ta>
            <ta e="T33" id="Seg_773" s="T32">tüː-le</ta>
            <ta e="T34" id="Seg_774" s="T33">medə-ŋ</ta>
            <ta e="T35" id="Seg_775" s="T34">qwɛl</ta>
            <ta e="T36" id="Seg_776" s="T35">täp-la-nan</ta>
            <ta e="T37" id="Seg_777" s="T36">koːci-ŋ</ta>
            <ta e="T38" id="Seg_778" s="T37">eː-sɨ</ta>
            <ta e="T39" id="Seg_779" s="T38">wargɨ</ta>
            <ta e="T40" id="Seg_780" s="T39">iː-qɨn</ta>
            <ta e="T41" id="Seg_781" s="T40">ser-nɨ-ŋ</ta>
            <ta e="T42" id="Seg_782" s="T41">täp-nan</ta>
            <ta e="T43" id="Seg_783" s="T42">iːbəgaj</ta>
            <ta e="T44" id="Seg_784" s="T43">tudo-la</ta>
            <ta e="T45" id="Seg_785" s="T44">eː-sɨ-tɨn</ta>
            <ta e="T46" id="Seg_786" s="T45">patom</ta>
            <ta e="T47" id="Seg_787" s="T46">sədə-mtätte</ta>
            <ta e="T48" id="Seg_788" s="T47">iː-qɨn</ta>
            <ta e="T49" id="Seg_789" s="T48">qwan-nɨ-ŋ</ta>
            <ta e="T50" id="Seg_790" s="T49">täp-nan</ta>
            <ta e="T51" id="Seg_791" s="T50">tudo-la-m</ta>
            <ta e="T52" id="Seg_792" s="T51">poːt-mbɨ-tɨn</ta>
            <ta e="T53" id="Seg_793" s="T52">täp-nan</ta>
            <ta e="T54" id="Seg_794" s="T53">šari-mbɨdi</ta>
            <ta e="T55" id="Seg_795" s="T54">tudo-la-tə</ta>
            <ta e="T56" id="Seg_796" s="T55">eː-sɨ-tɨn</ta>
            <ta e="T57" id="Seg_797" s="T56">i</ta>
            <ta e="T58" id="Seg_798" s="T57">wadʼi-n</ta>
            <ta e="T59" id="Seg_799" s="T58">čʼugun</ta>
            <ta e="T60" id="Seg_800" s="T59">plʼäka</ta>
            <ta e="T61" id="Seg_801" s="T60">poːt-mbɨdi</ta>
            <ta e="T62" id="Seg_802" s="T61">eː-sɨ</ta>
            <ta e="T63" id="Seg_803" s="T62">man</ta>
            <ta e="T64" id="Seg_804" s="T63">aj</ta>
            <ta e="T65" id="Seg_805" s="T64">am-r-nɨ-ŋ</ta>
            <ta e="T66" id="Seg_806" s="T65">patom</ta>
            <ta e="T67" id="Seg_807" s="T66">kur-ol-nɨ-ŋ</ta>
            <ta e="T68" id="Seg_808" s="T67">dʼaja</ta>
            <ta e="T69" id="Seg_809" s="T68">Mixajla-n</ta>
            <ta e="T70" id="Seg_810" s="T69">maːt-ntə</ta>
            <ta e="T71" id="Seg_811" s="T70">täp-la</ta>
            <ta e="T72" id="Seg_812" s="T71">tʼärɨ-tɨn</ta>
            <ta e="T73" id="Seg_813" s="T72">omdɨ-kɨ</ta>
            <ta e="T74" id="Seg_814" s="T73">am-r-gu</ta>
            <ta e="T75" id="Seg_815" s="T74">asa</ta>
            <ta e="T76" id="Seg_816" s="T75">kɨgɨ-ŋ</ta>
            <ta e="T77" id="Seg_817" s="T76">man</ta>
            <ta e="T78" id="Seg_818" s="T77">man</ta>
            <ta e="T79" id="Seg_819" s="T78">sədə</ta>
            <ta e="T80" id="Seg_820" s="T79">maːt-qɨn</ta>
            <ta e="T81" id="Seg_821" s="T80">am-r-mbɨ-ŋ</ta>
            <ta e="T82" id="Seg_822" s="T81">a</ta>
            <ta e="T83" id="Seg_823" s="T82">täp-la</ta>
            <ta e="T84" id="Seg_824" s="T83">tʼärɨ-tɨn</ta>
            <ta e="T85" id="Seg_825" s="T84">omdɨ-kɨ</ta>
            <ta e="T86" id="Seg_826" s="T85">man</ta>
            <ta e="T87" id="Seg_827" s="T86">asa</ta>
            <ta e="T88" id="Seg_828" s="T87">kɨgɨ-ŋ</ta>
            <ta e="T89" id="Seg_829" s="T88">nu</ta>
            <ta e="T90" id="Seg_830" s="T89">aj</ta>
            <ta e="T91" id="Seg_831" s="T90">omdɨ-ŋ</ta>
            <ta e="T92" id="Seg_832" s="T91">omdɨ-gu</ta>
            <ta e="T93" id="Seg_833" s="T92">nadə</ta>
            <ta e="T94" id="Seg_834" s="T93">malʼenʼko</ta>
            <ta e="T95" id="Seg_835" s="T94">aj</ta>
            <ta e="T96" id="Seg_836" s="T95">am-r-nɨ-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_837" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_838" s="T2">this</ta>
            <ta e="T4" id="Seg_839" s="T3">year-ADV.LOC</ta>
            <ta e="T5" id="Seg_840" s="T4">now</ta>
            <ta e="T6" id="Seg_841" s="T5">village-ILL</ta>
            <ta e="T7" id="Seg_842" s="T6">leave-FUT-1SG.S</ta>
            <ta e="T8" id="Seg_843" s="T7">we-ADES</ta>
            <ta e="T9" id="Seg_844" s="T8">village-LOC</ta>
            <ta e="T10" id="Seg_845" s="T9">human.being-PL.[NOM]</ta>
            <ta e="T11" id="Seg_846" s="T10">much-ADVZ</ta>
            <ta e="T12" id="Seg_847" s="T11">die-PST.NAR-3PL</ta>
            <ta e="T13" id="Seg_848" s="T12">maybe</ta>
            <ta e="T14" id="Seg_849" s="T13">more</ta>
            <ta e="T15" id="Seg_850" s="T14">die-%%-FUT-3PL</ta>
            <ta e="T16" id="Seg_851" s="T15">leave-FUT-1SG.S</ta>
            <ta e="T17" id="Seg_852" s="T16">fish-EP-CAP-INF</ta>
            <ta e="T18" id="Seg_853" s="T17">then</ta>
            <ta e="T19" id="Seg_854" s="T18">fish-EP-ACC</ta>
            <ta e="T20" id="Seg_855" s="T19">much-ADVZ</ta>
            <ta e="T21" id="Seg_856" s="T20">eat-FUT-1SG.O</ta>
            <ta e="T22" id="Seg_857" s="T21">first</ta>
            <ta e="T23" id="Seg_858" s="T22">time.[NOM]</ta>
            <ta e="T24" id="Seg_859" s="T23">eat-FUT-1SG.O</ta>
            <ta e="T25" id="Seg_860" s="T24">eat-FUT-1SG.O</ta>
            <ta e="T26" id="Seg_861" s="T25">and</ta>
            <ta e="T27" id="Seg_862" s="T26">then</ta>
            <ta e="T28" id="Seg_863" s="T27">NEG</ta>
            <ta e="T29" id="Seg_864" s="T28">want-FUT-1SG.O</ta>
            <ta e="T30" id="Seg_865" s="T29">eat-INF</ta>
            <ta e="T31" id="Seg_866" s="T30">year-GEN-3SG-year-ADV.LOC</ta>
            <ta e="T32" id="Seg_867" s="T31">I.NOM</ta>
            <ta e="T33" id="Seg_868" s="T32">come-CVB</ta>
            <ta e="T34" id="Seg_869" s="T33">achieve-1SG.S</ta>
            <ta e="T35" id="Seg_870" s="T34">fish.[NOM]</ta>
            <ta e="T36" id="Seg_871" s="T35">(s)he-PL-ADES</ta>
            <ta e="T37" id="Seg_872" s="T36">much-ADVZ</ta>
            <ta e="T38" id="Seg_873" s="T37">be-PST.[3SG.S]</ta>
            <ta e="T39" id="Seg_874" s="T38">elder</ta>
            <ta e="T40" id="Seg_875" s="T39">son-LOC</ta>
            <ta e="T41" id="Seg_876" s="T40">come.in-CO-1SG.S</ta>
            <ta e="T42" id="Seg_877" s="T41">(s)he-ADES</ta>
            <ta e="T43" id="Seg_878" s="T42">big</ta>
            <ta e="T44" id="Seg_879" s="T43">crucian-PL.[NOM]</ta>
            <ta e="T45" id="Seg_880" s="T44">be-PST-3PL</ta>
            <ta e="T46" id="Seg_881" s="T45">then</ta>
            <ta e="T47" id="Seg_882" s="T46">two-ORD</ta>
            <ta e="T48" id="Seg_883" s="T47">son-LOC</ta>
            <ta e="T49" id="Seg_884" s="T48">leave-CO-1SG.S</ta>
            <ta e="T50" id="Seg_885" s="T49">(s)he-ADES</ta>
            <ta e="T51" id="Seg_886" s="T50">crucian-PL-ACC</ta>
            <ta e="T52" id="Seg_887" s="T51">cook-RES-3PL</ta>
            <ta e="T53" id="Seg_888" s="T52">(s)he-ADES</ta>
            <ta e="T54" id="Seg_889" s="T53">fry-PTCP.PST</ta>
            <ta e="T55" id="Seg_890" s="T54">crucian-PL.[NOM]-3SG</ta>
            <ta e="T56" id="Seg_891" s="T55">be-PST-3PL</ta>
            <ta e="T57" id="Seg_892" s="T56">and</ta>
            <ta e="T58" id="Seg_893" s="T57">meat-GEN</ta>
            <ta e="T59" id="Seg_894" s="T58">cauldron.[NOM]</ta>
            <ta e="T60" id="Seg_895" s="T59">half.[NOM]</ta>
            <ta e="T61" id="Seg_896" s="T60">cook-PTCP.PST</ta>
            <ta e="T62" id="Seg_897" s="T61">be-PST.[3SG.S]</ta>
            <ta e="T63" id="Seg_898" s="T62">I.NOM</ta>
            <ta e="T64" id="Seg_899" s="T63">again</ta>
            <ta e="T65" id="Seg_900" s="T64">eat-FRQ-CO-1SG.S</ta>
            <ta e="T66" id="Seg_901" s="T65">then</ta>
            <ta e="T67" id="Seg_902" s="T66">go-MOM-CO-1SG.S</ta>
            <ta e="T68" id="Seg_903" s="T67">uncle.[NOM]</ta>
            <ta e="T69" id="Seg_904" s="T68">Mikhailo-GEN</ta>
            <ta e="T70" id="Seg_905" s="T69">house-ILL</ta>
            <ta e="T71" id="Seg_906" s="T70">(s)he-PL</ta>
            <ta e="T72" id="Seg_907" s="T71">say-3PL</ta>
            <ta e="T73" id="Seg_908" s="T72">sit.down-IMP.2SG.S</ta>
            <ta e="T74" id="Seg_909" s="T73">eat-FRQ-INF</ta>
            <ta e="T75" id="Seg_910" s="T74">NEG</ta>
            <ta e="T76" id="Seg_911" s="T75">want-1SG.S</ta>
            <ta e="T77" id="Seg_912" s="T76">I.NOM</ta>
            <ta e="T78" id="Seg_913" s="T77">I.NOM</ta>
            <ta e="T79" id="Seg_914" s="T78">two</ta>
            <ta e="T80" id="Seg_915" s="T79">house-LOC</ta>
            <ta e="T81" id="Seg_916" s="T80">eat-FRQ-PST.NAR-1SG.S</ta>
            <ta e="T82" id="Seg_917" s="T81">and</ta>
            <ta e="T83" id="Seg_918" s="T82">(s)he-PL.[NOM]</ta>
            <ta e="T84" id="Seg_919" s="T83">say-3PL</ta>
            <ta e="T85" id="Seg_920" s="T84">sit.down-IMP.2SG.S</ta>
            <ta e="T86" id="Seg_921" s="T85">I.NOM</ta>
            <ta e="T87" id="Seg_922" s="T86">NEG</ta>
            <ta e="T88" id="Seg_923" s="T87">want-1SG.S</ta>
            <ta e="T89" id="Seg_924" s="T88">now</ta>
            <ta e="T90" id="Seg_925" s="T89">again</ta>
            <ta e="T91" id="Seg_926" s="T90">sit.down-1SG.S</ta>
            <ta e="T92" id="Seg_927" s="T91">sit.down-INF</ta>
            <ta e="T93" id="Seg_928" s="T92">one.should</ta>
            <ta e="T94" id="Seg_929" s="T93">a.bit</ta>
            <ta e="T95" id="Seg_930" s="T94">again</ta>
            <ta e="T96" id="Seg_931" s="T95">eat-FRQ-CO-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_932" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_933" s="T2">этот</ta>
            <ta e="T4" id="Seg_934" s="T3">год-ADV.LOC</ta>
            <ta e="T5" id="Seg_935" s="T4">нынче</ta>
            <ta e="T6" id="Seg_936" s="T5">деревня-ILL</ta>
            <ta e="T7" id="Seg_937" s="T6">отправиться-FUT-1SG.S</ta>
            <ta e="T8" id="Seg_938" s="T7">мы-ADES</ta>
            <ta e="T9" id="Seg_939" s="T8">деревня-LOC</ta>
            <ta e="T10" id="Seg_940" s="T9">человек-PL.[NOM]</ta>
            <ta e="T11" id="Seg_941" s="T10">много-ADVZ</ta>
            <ta e="T12" id="Seg_942" s="T11">умереть-PST.NAR-3PL</ta>
            <ta e="T13" id="Seg_943" s="T12">может.быть</ta>
            <ta e="T14" id="Seg_944" s="T13">еще</ta>
            <ta e="T15" id="Seg_945" s="T14">умереть-%%-FUT-3PL</ta>
            <ta e="T16" id="Seg_946" s="T15">отправиться-FUT-1SG.S</ta>
            <ta e="T17" id="Seg_947" s="T16">рыба-EP-CAP-INF</ta>
            <ta e="T18" id="Seg_948" s="T17">потом</ta>
            <ta e="T19" id="Seg_949" s="T18">рыба-EP-ACC</ta>
            <ta e="T20" id="Seg_950" s="T19">много-ADVZ</ta>
            <ta e="T21" id="Seg_951" s="T20">съесть-FUT-1SG.O</ta>
            <ta e="T22" id="Seg_952" s="T21">первый</ta>
            <ta e="T23" id="Seg_953" s="T22">раз.[NOM]</ta>
            <ta e="T24" id="Seg_954" s="T23">съесть-FUT-1SG.O</ta>
            <ta e="T25" id="Seg_955" s="T24">съесть-FUT-1SG.O</ta>
            <ta e="T26" id="Seg_956" s="T25">а</ta>
            <ta e="T27" id="Seg_957" s="T26">потом</ta>
            <ta e="T28" id="Seg_958" s="T27">NEG</ta>
            <ta e="T29" id="Seg_959" s="T28">хотеть-FUT-1SG.O</ta>
            <ta e="T30" id="Seg_960" s="T29">съесть-INF</ta>
            <ta e="T31" id="Seg_961" s="T30">год-GEN-3SG-год-ADV.LOC</ta>
            <ta e="T32" id="Seg_962" s="T31">я.NOM</ta>
            <ta e="T33" id="Seg_963" s="T32">приехать-CVB</ta>
            <ta e="T34" id="Seg_964" s="T33">достичь-1SG.S</ta>
            <ta e="T35" id="Seg_965" s="T34">рыба.[NOM]</ta>
            <ta e="T36" id="Seg_966" s="T35">он(а)-PL-ADES</ta>
            <ta e="T37" id="Seg_967" s="T36">много-ADVZ</ta>
            <ta e="T38" id="Seg_968" s="T37">быть-PST.[3SG.S]</ta>
            <ta e="T39" id="Seg_969" s="T38">старший</ta>
            <ta e="T40" id="Seg_970" s="T39">сын-LOC</ta>
            <ta e="T41" id="Seg_971" s="T40">зайти-CO-1SG.S</ta>
            <ta e="T42" id="Seg_972" s="T41">он(а)-ADES</ta>
            <ta e="T43" id="Seg_973" s="T42">большой</ta>
            <ta e="T44" id="Seg_974" s="T43">карась-PL.[NOM]</ta>
            <ta e="T45" id="Seg_975" s="T44">быть-PST-3PL</ta>
            <ta e="T46" id="Seg_976" s="T45">потом</ta>
            <ta e="T47" id="Seg_977" s="T46">два-ORD</ta>
            <ta e="T48" id="Seg_978" s="T47">сын-LOC</ta>
            <ta e="T49" id="Seg_979" s="T48">отправиться-CO-1SG.S</ta>
            <ta e="T50" id="Seg_980" s="T49">он(а)-ADES</ta>
            <ta e="T51" id="Seg_981" s="T50">карась-PL-ACC</ta>
            <ta e="T52" id="Seg_982" s="T51">сварить-RES-3PL</ta>
            <ta e="T53" id="Seg_983" s="T52">он(а)-ADES</ta>
            <ta e="T54" id="Seg_984" s="T53">жарить-PTCP.PST</ta>
            <ta e="T55" id="Seg_985" s="T54">карась-PL.[NOM]-3SG</ta>
            <ta e="T56" id="Seg_986" s="T55">быть-PST-3PL</ta>
            <ta e="T57" id="Seg_987" s="T56">и</ta>
            <ta e="T58" id="Seg_988" s="T57">мясо-GEN</ta>
            <ta e="T59" id="Seg_989" s="T58">чугунок.[NOM]</ta>
            <ta e="T60" id="Seg_990" s="T59">половина.[NOM]</ta>
            <ta e="T61" id="Seg_991" s="T60">сварить-PTCP.PST</ta>
            <ta e="T62" id="Seg_992" s="T61">быть-PST.[3SG.S]</ta>
            <ta e="T63" id="Seg_993" s="T62">я.NOM</ta>
            <ta e="T64" id="Seg_994" s="T63">опять</ta>
            <ta e="T65" id="Seg_995" s="T64">съесть-FRQ-CO-1SG.S</ta>
            <ta e="T66" id="Seg_996" s="T65">потом</ta>
            <ta e="T67" id="Seg_997" s="T66">ходить-MOM-CO-1SG.S</ta>
            <ta e="T68" id="Seg_998" s="T67">дядя.[NOM]</ta>
            <ta e="T69" id="Seg_999" s="T68">Михайло-GEN</ta>
            <ta e="T70" id="Seg_1000" s="T69">дом-ILL</ta>
            <ta e="T71" id="Seg_1001" s="T70">он(а)-PL</ta>
            <ta e="T72" id="Seg_1002" s="T71">сказать-3PL</ta>
            <ta e="T73" id="Seg_1003" s="T72">сесть-IMP.2SG.S</ta>
            <ta e="T74" id="Seg_1004" s="T73">съесть-FRQ-INF</ta>
            <ta e="T75" id="Seg_1005" s="T74">NEG</ta>
            <ta e="T76" id="Seg_1006" s="T75">хотеть-1SG.S</ta>
            <ta e="T77" id="Seg_1007" s="T76">я.NOM</ta>
            <ta e="T78" id="Seg_1008" s="T77">я.NOM</ta>
            <ta e="T79" id="Seg_1009" s="T78">два</ta>
            <ta e="T80" id="Seg_1010" s="T79">дом-LOC</ta>
            <ta e="T81" id="Seg_1011" s="T80">съесть-FRQ-PST.NAR-1SG.S</ta>
            <ta e="T82" id="Seg_1012" s="T81">а</ta>
            <ta e="T83" id="Seg_1013" s="T82">он(а)-PL.[NOM]</ta>
            <ta e="T84" id="Seg_1014" s="T83">сказать-3PL</ta>
            <ta e="T85" id="Seg_1015" s="T84">сесть-IMP.2SG.S</ta>
            <ta e="T86" id="Seg_1016" s="T85">я.NOM</ta>
            <ta e="T87" id="Seg_1017" s="T86">NEG</ta>
            <ta e="T88" id="Seg_1018" s="T87">хотеть-1SG.S</ta>
            <ta e="T89" id="Seg_1019" s="T88">ну</ta>
            <ta e="T90" id="Seg_1020" s="T89">опять</ta>
            <ta e="T91" id="Seg_1021" s="T90">сесть-1SG.S</ta>
            <ta e="T92" id="Seg_1022" s="T91">сесть-INF</ta>
            <ta e="T93" id="Seg_1023" s="T92">надо</ta>
            <ta e="T94" id="Seg_1024" s="T93">маленько</ta>
            <ta e="T95" id="Seg_1025" s="T94">опять</ta>
            <ta e="T96" id="Seg_1026" s="T95">съесть-FRQ-CO-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1027" s="T1">pers</ta>
            <ta e="T3" id="Seg_1028" s="T2">dem</ta>
            <ta e="T4" id="Seg_1029" s="T3">n-n&gt;adv</ta>
            <ta e="T5" id="Seg_1030" s="T4">adv</ta>
            <ta e="T6" id="Seg_1031" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_1032" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_1033" s="T7">pers-n:case</ta>
            <ta e="T9" id="Seg_1034" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_1035" s="T9">n-n:num.[n:case]</ta>
            <ta e="T11" id="Seg_1036" s="T10">quant-quant&gt;adv</ta>
            <ta e="T12" id="Seg_1037" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_1038" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_1039" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_1040" s="T14">v-%%-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_1041" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_1042" s="T16">n-n:ins-n&gt;v-v:inf</ta>
            <ta e="T18" id="Seg_1043" s="T17">adv</ta>
            <ta e="T19" id="Seg_1044" s="T18">n-n:ins-n:case</ta>
            <ta e="T20" id="Seg_1045" s="T19">quant-quant&gt;adv</ta>
            <ta e="T21" id="Seg_1046" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_1047" s="T21">adj</ta>
            <ta e="T23" id="Seg_1048" s="T22">n.[n:case]</ta>
            <ta e="T24" id="Seg_1049" s="T23">v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_1050" s="T24">v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_1051" s="T25">conj</ta>
            <ta e="T27" id="Seg_1052" s="T26">adv</ta>
            <ta e="T28" id="Seg_1053" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_1054" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_1055" s="T29">v-v:inf</ta>
            <ta e="T31" id="Seg_1056" s="T30">n-n:case-n:poss-n-n&gt;adv</ta>
            <ta e="T32" id="Seg_1057" s="T31">pers</ta>
            <ta e="T33" id="Seg_1058" s="T32">v-v&gt;adv</ta>
            <ta e="T34" id="Seg_1059" s="T33">v-v:pn</ta>
            <ta e="T35" id="Seg_1060" s="T34">n.[n:case]</ta>
            <ta e="T36" id="Seg_1061" s="T35">pers-n:num-n:case</ta>
            <ta e="T37" id="Seg_1062" s="T36">quant-quant&gt;adv</ta>
            <ta e="T38" id="Seg_1063" s="T37">v-v:tense.[v:pn]</ta>
            <ta e="T39" id="Seg_1064" s="T38">adj</ta>
            <ta e="T40" id="Seg_1065" s="T39">n-n:case</ta>
            <ta e="T41" id="Seg_1066" s="T40">v-v:ins-v:pn</ta>
            <ta e="T42" id="Seg_1067" s="T41">pers-n:case</ta>
            <ta e="T43" id="Seg_1068" s="T42">adj</ta>
            <ta e="T44" id="Seg_1069" s="T43">n-n:num.[n:case]</ta>
            <ta e="T45" id="Seg_1070" s="T44">v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_1071" s="T45">adv</ta>
            <ta e="T47" id="Seg_1072" s="T46">num-num&gt;adj</ta>
            <ta e="T48" id="Seg_1073" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_1074" s="T48">v-v:ins-v:pn</ta>
            <ta e="T50" id="Seg_1075" s="T49">pers-n:case</ta>
            <ta e="T51" id="Seg_1076" s="T50">n-n:num-n:case</ta>
            <ta e="T52" id="Seg_1077" s="T51">v-v&gt;v-v:pn</ta>
            <ta e="T53" id="Seg_1078" s="T52">pers-n:case</ta>
            <ta e="T54" id="Seg_1079" s="T53">v-v&gt;ptcp</ta>
            <ta e="T55" id="Seg_1080" s="T54">n-n:num.[n:case]-n:poss</ta>
            <ta e="T56" id="Seg_1081" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_1082" s="T56">conj</ta>
            <ta e="T58" id="Seg_1083" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_1084" s="T58">n.[n:case]</ta>
            <ta e="T60" id="Seg_1085" s="T59">n.[n:case]</ta>
            <ta e="T61" id="Seg_1086" s="T60">v-v&gt;ptcp</ta>
            <ta e="T62" id="Seg_1087" s="T61">v-v:tense.[v:pn]</ta>
            <ta e="T63" id="Seg_1088" s="T62">pers</ta>
            <ta e="T64" id="Seg_1089" s="T63">adv</ta>
            <ta e="T65" id="Seg_1090" s="T64">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T66" id="Seg_1091" s="T65">adv</ta>
            <ta e="T67" id="Seg_1092" s="T66">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T68" id="Seg_1093" s="T67">n.[n:case]</ta>
            <ta e="T69" id="Seg_1094" s="T68">nprop-n:case</ta>
            <ta e="T70" id="Seg_1095" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_1096" s="T70">pers-n:num</ta>
            <ta e="T72" id="Seg_1097" s="T71">v-v:pn</ta>
            <ta e="T73" id="Seg_1098" s="T72">v-v:mood.pn</ta>
            <ta e="T74" id="Seg_1099" s="T73">v-v&gt;v-v:inf</ta>
            <ta e="T75" id="Seg_1100" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_1101" s="T75">v-v:pn</ta>
            <ta e="T77" id="Seg_1102" s="T76">pers</ta>
            <ta e="T78" id="Seg_1103" s="T77">pers</ta>
            <ta e="T79" id="Seg_1104" s="T78">num</ta>
            <ta e="T80" id="Seg_1105" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_1106" s="T80">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_1107" s="T81">conj</ta>
            <ta e="T83" id="Seg_1108" s="T82">pers-n:num.[n:case]</ta>
            <ta e="T84" id="Seg_1109" s="T83">v-v:pn</ta>
            <ta e="T85" id="Seg_1110" s="T84">v-v:mood.pn</ta>
            <ta e="T86" id="Seg_1111" s="T85">pers</ta>
            <ta e="T87" id="Seg_1112" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_1113" s="T87">v-v:pn</ta>
            <ta e="T89" id="Seg_1114" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_1115" s="T89">adv</ta>
            <ta e="T91" id="Seg_1116" s="T90">v-v:pn</ta>
            <ta e="T92" id="Seg_1117" s="T91">v-v:inf</ta>
            <ta e="T93" id="Seg_1118" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_1119" s="T93">adv</ta>
            <ta e="T95" id="Seg_1120" s="T94">adv</ta>
            <ta e="T96" id="Seg_1121" s="T95">v-v&gt;v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1122" s="T1">pers</ta>
            <ta e="T3" id="Seg_1123" s="T2">dem</ta>
            <ta e="T4" id="Seg_1124" s="T3">adv</ta>
            <ta e="T5" id="Seg_1125" s="T4">adv</ta>
            <ta e="T6" id="Seg_1126" s="T5">n</ta>
            <ta e="T7" id="Seg_1127" s="T6">v</ta>
            <ta e="T8" id="Seg_1128" s="T7">pers</ta>
            <ta e="T9" id="Seg_1129" s="T8">n</ta>
            <ta e="T10" id="Seg_1130" s="T9">n</ta>
            <ta e="T11" id="Seg_1131" s="T10">adv</ta>
            <ta e="T12" id="Seg_1132" s="T11">v</ta>
            <ta e="T13" id="Seg_1133" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_1134" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_1135" s="T14">v</ta>
            <ta e="T16" id="Seg_1136" s="T15">v</ta>
            <ta e="T17" id="Seg_1137" s="T16">v</ta>
            <ta e="T18" id="Seg_1138" s="T17">adv</ta>
            <ta e="T19" id="Seg_1139" s="T18">n</ta>
            <ta e="T20" id="Seg_1140" s="T19">adv</ta>
            <ta e="T21" id="Seg_1141" s="T20">v</ta>
            <ta e="T22" id="Seg_1142" s="T21">adj</ta>
            <ta e="T23" id="Seg_1143" s="T22">n</ta>
            <ta e="T24" id="Seg_1144" s="T23">v</ta>
            <ta e="T25" id="Seg_1145" s="T24">v</ta>
            <ta e="T26" id="Seg_1146" s="T25">conj</ta>
            <ta e="T27" id="Seg_1147" s="T26">adv</ta>
            <ta e="T28" id="Seg_1148" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_1149" s="T28">v</ta>
            <ta e="T30" id="Seg_1150" s="T29">v</ta>
            <ta e="T31" id="Seg_1151" s="T30">n</ta>
            <ta e="T32" id="Seg_1152" s="T31">pers</ta>
            <ta e="T33" id="Seg_1153" s="T32">adv</ta>
            <ta e="T34" id="Seg_1154" s="T33">v</ta>
            <ta e="T35" id="Seg_1155" s="T34">n</ta>
            <ta e="T36" id="Seg_1156" s="T35">pers</ta>
            <ta e="T37" id="Seg_1157" s="T36">adv</ta>
            <ta e="T38" id="Seg_1158" s="T37">v</ta>
            <ta e="T39" id="Seg_1159" s="T38">adj</ta>
            <ta e="T40" id="Seg_1160" s="T39">n</ta>
            <ta e="T41" id="Seg_1161" s="T40">v</ta>
            <ta e="T42" id="Seg_1162" s="T41">pers</ta>
            <ta e="T43" id="Seg_1163" s="T42">adj</ta>
            <ta e="T44" id="Seg_1164" s="T43">n</ta>
            <ta e="T45" id="Seg_1165" s="T44">v</ta>
            <ta e="T46" id="Seg_1166" s="T45">adv</ta>
            <ta e="T47" id="Seg_1167" s="T46">adj</ta>
            <ta e="T48" id="Seg_1168" s="T47">n</ta>
            <ta e="T49" id="Seg_1169" s="T48">v</ta>
            <ta e="T50" id="Seg_1170" s="T49">pers</ta>
            <ta e="T51" id="Seg_1171" s="T50">n</ta>
            <ta e="T52" id="Seg_1172" s="T51">v</ta>
            <ta e="T53" id="Seg_1173" s="T52">pers</ta>
            <ta e="T54" id="Seg_1174" s="T53">ptcp</ta>
            <ta e="T55" id="Seg_1175" s="T54">n</ta>
            <ta e="T56" id="Seg_1176" s="T55">v</ta>
            <ta e="T57" id="Seg_1177" s="T56">conj</ta>
            <ta e="T58" id="Seg_1178" s="T57">n</ta>
            <ta e="T59" id="Seg_1179" s="T58">n</ta>
            <ta e="T60" id="Seg_1180" s="T59">n</ta>
            <ta e="T61" id="Seg_1181" s="T60">ptcp</ta>
            <ta e="T62" id="Seg_1182" s="T61">v</ta>
            <ta e="T63" id="Seg_1183" s="T62">pers</ta>
            <ta e="T64" id="Seg_1184" s="T63">adv</ta>
            <ta e="T65" id="Seg_1185" s="T64">v</ta>
            <ta e="T66" id="Seg_1186" s="T65">adv</ta>
            <ta e="T67" id="Seg_1187" s="T66">v</ta>
            <ta e="T68" id="Seg_1188" s="T67">n</ta>
            <ta e="T69" id="Seg_1189" s="T68">nprop</ta>
            <ta e="T70" id="Seg_1190" s="T69">n</ta>
            <ta e="T71" id="Seg_1191" s="T70">pers</ta>
            <ta e="T72" id="Seg_1192" s="T71">v</ta>
            <ta e="T73" id="Seg_1193" s="T72">v</ta>
            <ta e="T74" id="Seg_1194" s="T73">v</ta>
            <ta e="T75" id="Seg_1195" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_1196" s="T75">v</ta>
            <ta e="T77" id="Seg_1197" s="T76">pers</ta>
            <ta e="T78" id="Seg_1198" s="T77">pers</ta>
            <ta e="T79" id="Seg_1199" s="T78">num</ta>
            <ta e="T80" id="Seg_1200" s="T79">n</ta>
            <ta e="T81" id="Seg_1201" s="T80">v</ta>
            <ta e="T82" id="Seg_1202" s="T81">conj</ta>
            <ta e="T83" id="Seg_1203" s="T82">pers</ta>
            <ta e="T84" id="Seg_1204" s="T83">v</ta>
            <ta e="T85" id="Seg_1205" s="T84">v</ta>
            <ta e="T86" id="Seg_1206" s="T85">pers</ta>
            <ta e="T87" id="Seg_1207" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_1208" s="T87">v</ta>
            <ta e="T89" id="Seg_1209" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_1210" s="T89">adv</ta>
            <ta e="T91" id="Seg_1211" s="T90">v</ta>
            <ta e="T92" id="Seg_1212" s="T91">v</ta>
            <ta e="T93" id="Seg_1213" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_1214" s="T93">adv</ta>
            <ta e="T95" id="Seg_1215" s="T94">adv</ta>
            <ta e="T96" id="Seg_1216" s="T95">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1217" s="T1">pro.h:A</ta>
            <ta e="T4" id="Seg_1218" s="T3">adv:Time</ta>
            <ta e="T6" id="Seg_1219" s="T5">np:G</ta>
            <ta e="T8" id="Seg_1220" s="T7">pro.h:Poss</ta>
            <ta e="T9" id="Seg_1221" s="T8">np:L</ta>
            <ta e="T10" id="Seg_1222" s="T9">np.h:P</ta>
            <ta e="T15" id="Seg_1223" s="T14">0.3.h:P</ta>
            <ta e="T16" id="Seg_1224" s="T15">0.1.h:A</ta>
            <ta e="T18" id="Seg_1225" s="T17">adv:Time</ta>
            <ta e="T19" id="Seg_1226" s="T18">np:P</ta>
            <ta e="T21" id="Seg_1227" s="T20">0.1.h:A</ta>
            <ta e="T23" id="Seg_1228" s="T22">np:Time</ta>
            <ta e="T24" id="Seg_1229" s="T23">0.1.h:A</ta>
            <ta e="T25" id="Seg_1230" s="T24">0.1.h:A</ta>
            <ta e="T27" id="Seg_1231" s="T26">adv:Time</ta>
            <ta e="T29" id="Seg_1232" s="T28">0.1.h:E</ta>
            <ta e="T30" id="Seg_1233" s="T29">v:Th</ta>
            <ta e="T31" id="Seg_1234" s="T30">adv:Time</ta>
            <ta e="T32" id="Seg_1235" s="T31">pro.h:A</ta>
            <ta e="T35" id="Seg_1236" s="T34">np:Th</ta>
            <ta e="T36" id="Seg_1237" s="T35">pro.h:Poss</ta>
            <ta e="T40" id="Seg_1238" s="T39">np.h:G</ta>
            <ta e="T41" id="Seg_1239" s="T40">0.1.h:A</ta>
            <ta e="T42" id="Seg_1240" s="T41">pro.h:Poss</ta>
            <ta e="T44" id="Seg_1241" s="T43">np:Th</ta>
            <ta e="T46" id="Seg_1242" s="T45">adv:Time</ta>
            <ta e="T48" id="Seg_1243" s="T47">np.h:G</ta>
            <ta e="T49" id="Seg_1244" s="T48">0.1.h:A</ta>
            <ta e="T50" id="Seg_1245" s="T49">pro.h:L</ta>
            <ta e="T51" id="Seg_1246" s="T50">np:P</ta>
            <ta e="T52" id="Seg_1247" s="T51">0.3.h:A</ta>
            <ta e="T53" id="Seg_1248" s="T52">pro.h:Poss</ta>
            <ta e="T55" id="Seg_1249" s="T54">np:Th</ta>
            <ta e="T60" id="Seg_1250" s="T59">np:Th</ta>
            <ta e="T63" id="Seg_1251" s="T62">pro.h:A</ta>
            <ta e="T66" id="Seg_1252" s="T65">adv:Time</ta>
            <ta e="T67" id="Seg_1253" s="T66">0.1.h:A</ta>
            <ta e="T69" id="Seg_1254" s="T68">np.h:Poss</ta>
            <ta e="T70" id="Seg_1255" s="T69">np:G</ta>
            <ta e="T71" id="Seg_1256" s="T70">pro.h:A</ta>
            <ta e="T73" id="Seg_1257" s="T72">0.2.h:A</ta>
            <ta e="T77" id="Seg_1258" s="T76">pro.h:E</ta>
            <ta e="T78" id="Seg_1259" s="T77">pro.h:A</ta>
            <ta e="T80" id="Seg_1260" s="T79">np:L</ta>
            <ta e="T83" id="Seg_1261" s="T82">pro.h:A</ta>
            <ta e="T85" id="Seg_1262" s="T84">0.2.h:A</ta>
            <ta e="T86" id="Seg_1263" s="T85">pro.h:E</ta>
            <ta e="T91" id="Seg_1264" s="T90">0.1.h:A</ta>
            <ta e="T92" id="Seg_1265" s="T91">v:Th</ta>
            <ta e="T96" id="Seg_1266" s="T95">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1267" s="T1">pro.h:S</ta>
            <ta e="T7" id="Seg_1268" s="T6">v:pred</ta>
            <ta e="T10" id="Seg_1269" s="T9">np.h:S</ta>
            <ta e="T12" id="Seg_1270" s="T11">v:pred</ta>
            <ta e="T15" id="Seg_1271" s="T14">0.3.h:S v:pred</ta>
            <ta e="T16" id="Seg_1272" s="T15">0.1.h:S v:pred</ta>
            <ta e="T17" id="Seg_1273" s="T16">s:purp</ta>
            <ta e="T19" id="Seg_1274" s="T18">np:O</ta>
            <ta e="T21" id="Seg_1275" s="T20">0.1.h:S v:pred</ta>
            <ta e="T24" id="Seg_1276" s="T23">0.1.h:S v:pred</ta>
            <ta e="T25" id="Seg_1277" s="T24">0.1.h:S v:pred</ta>
            <ta e="T29" id="Seg_1278" s="T28">0.1.h:S v:pred</ta>
            <ta e="T30" id="Seg_1279" s="T29">v:O</ta>
            <ta e="T32" id="Seg_1280" s="T31">pro.h:S</ta>
            <ta e="T33" id="Seg_1281" s="T32">s:temp</ta>
            <ta e="T34" id="Seg_1282" s="T33">v:pred</ta>
            <ta e="T35" id="Seg_1283" s="T34">np:S</ta>
            <ta e="T38" id="Seg_1284" s="T37">v:pred</ta>
            <ta e="T41" id="Seg_1285" s="T40">0.1.h:S v:pred</ta>
            <ta e="T44" id="Seg_1286" s="T43">np:S</ta>
            <ta e="T45" id="Seg_1287" s="T44">v:pred</ta>
            <ta e="T49" id="Seg_1288" s="T48">0.1.h:S v:pred</ta>
            <ta e="T51" id="Seg_1289" s="T50">np:O</ta>
            <ta e="T52" id="Seg_1290" s="T51">0.3.h:S v:pred</ta>
            <ta e="T55" id="Seg_1291" s="T54">np:S</ta>
            <ta e="T56" id="Seg_1292" s="T55">v:pred</ta>
            <ta e="T60" id="Seg_1293" s="T59">np:S</ta>
            <ta e="T62" id="Seg_1294" s="T61">v:pred</ta>
            <ta e="T63" id="Seg_1295" s="T62">pro.h:S</ta>
            <ta e="T65" id="Seg_1296" s="T64">v:pred</ta>
            <ta e="T67" id="Seg_1297" s="T66">0.1.h:S v:pred</ta>
            <ta e="T71" id="Seg_1298" s="T70">pro.h:S</ta>
            <ta e="T72" id="Seg_1299" s="T71">v:pred</ta>
            <ta e="T73" id="Seg_1300" s="T72">0.2.h:S v:pred</ta>
            <ta e="T74" id="Seg_1301" s="T73">s:purp</ta>
            <ta e="T76" id="Seg_1302" s="T75">v:pred</ta>
            <ta e="T77" id="Seg_1303" s="T76">pro.h:S</ta>
            <ta e="T78" id="Seg_1304" s="T77">pro.h:S</ta>
            <ta e="T81" id="Seg_1305" s="T80">v:pred</ta>
            <ta e="T83" id="Seg_1306" s="T82">pro.h:S</ta>
            <ta e="T84" id="Seg_1307" s="T83">v:pred</ta>
            <ta e="T85" id="Seg_1308" s="T84">0.2.h:S v:pred</ta>
            <ta e="T86" id="Seg_1309" s="T85">pro.h:S</ta>
            <ta e="T88" id="Seg_1310" s="T87">v:pred</ta>
            <ta e="T91" id="Seg_1311" s="T90">0.1.h:S v:pred</ta>
            <ta e="T92" id="Seg_1312" s="T91">v:S</ta>
            <ta e="T93" id="Seg_1313" s="T92">ptcl:pred</ta>
            <ta e="T96" id="Seg_1314" s="T95">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_1315" s="T4">RUS:cult</ta>
            <ta e="T13" id="Seg_1316" s="T12">RUS:mod</ta>
            <ta e="T14" id="Seg_1317" s="T13">RUS:disc</ta>
            <ta e="T22" id="Seg_1318" s="T21">RUS:cult</ta>
            <ta e="T23" id="Seg_1319" s="T22">RUS:core</ta>
            <ta e="T26" id="Seg_1320" s="T25">RUS:gram</ta>
            <ta e="T27" id="Seg_1321" s="T26">RUS:disc</ta>
            <ta e="T46" id="Seg_1322" s="T45">RUS:disc</ta>
            <ta e="T54" id="Seg_1323" s="T53">RUS:cult</ta>
            <ta e="T57" id="Seg_1324" s="T56">RUS:gram</ta>
            <ta e="T59" id="Seg_1325" s="T58">RUS:cult</ta>
            <ta e="T66" id="Seg_1326" s="T65">RUS:disc</ta>
            <ta e="T68" id="Seg_1327" s="T67">RUS:cult</ta>
            <ta e="T82" id="Seg_1328" s="T81">RUS:gram</ta>
            <ta e="T89" id="Seg_1329" s="T88">RUS:disc</ta>
            <ta e="T93" id="Seg_1330" s="T92">RUS:mod</ta>
            <ta e="T94" id="Seg_1331" s="T93">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T5" id="Seg_1332" s="T4">Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_1333" s="T1">This year I'll go to my village (home?).</ta>
            <ta e="T12" id="Seg_1334" s="T7">A lot of people died in our village.</ta>
            <ta e="T15" id="Seg_1335" s="T12">Maybe someone else will die.</ta>
            <ta e="T17" id="Seg_1336" s="T15">I'll go fishing.</ta>
            <ta e="T21" id="Seg_1337" s="T17">Then I'll eat lots of fish.</ta>
            <ta e="T30" id="Seg_1338" s="T21">First time I'll be eating and eating, and then I won't want to eat any more.</ta>
            <ta e="T34" id="Seg_1339" s="T30">Last year I came [there].</ta>
            <ta e="T38" id="Seg_1340" s="T34">They had lots of fish.</ta>
            <ta e="T41" id="Seg_1341" s="T38">I came to my elder son.</ta>
            <ta e="T45" id="Seg_1342" s="T41">He had got big crucians.</ta>
            <ta e="T49" id="Seg_1343" s="T45">Then I came to my second son.</ta>
            <ta e="T52" id="Seg_1344" s="T49">With him [I ate] boiled crucians.</ta>
            <ta e="T62" id="Seg_1345" s="T52">He had fried crucians and half of a cauldron with boiled meat.</ta>
            <ta e="T65" id="Seg_1346" s="T62">I ate again.</ta>
            <ta e="T70" id="Seg_1347" s="T65">Then I came home to uncle Mikhaila.</ta>
            <ta e="T74" id="Seg_1348" s="T70">They said: “Sit down and eat.”</ta>
            <ta e="T77" id="Seg_1349" s="T74">“I don't want.</ta>
            <ta e="T81" id="Seg_1350" s="T77">I ate already in two houses.”</ta>
            <ta e="T85" id="Seg_1351" s="T81">And they said: “Sit down.”</ta>
            <ta e="T91" id="Seg_1352" s="T85">I didn't want, well, I sat down again.</ta>
            <ta e="T93" id="Seg_1353" s="T91">One should sit down.</ta>
            <ta e="T96" id="Seg_1354" s="T93">I ate a bit again.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_1355" s="T1">In diesem Jahr gehe ich (nach Hause?) in mein Dorf.</ta>
            <ta e="T12" id="Seg_1356" s="T7">In unserem Dorf starben viele Menschen.</ta>
            <ta e="T15" id="Seg_1357" s="T12">Vielleicht stirbt noch jemand.</ta>
            <ta e="T17" id="Seg_1358" s="T15">Ich gehe fischen.</ta>
            <ta e="T21" id="Seg_1359" s="T17">Dann werde ich viel Fisch essen.</ta>
            <ta e="T30" id="Seg_1360" s="T21">Zuerst werde ich essen und essen und dann werde ich nicht mehr essen wollen.</ta>
            <ta e="T34" id="Seg_1361" s="T30">Letztes Jahr kam ich [dorthin].</ta>
            <ta e="T38" id="Seg_1362" s="T34">Sie hatten viel Fisch.</ta>
            <ta e="T41" id="Seg_1363" s="T38">Ich kam zu meinem älteren Sohn.</ta>
            <ta e="T45" id="Seg_1364" s="T41">Er hatte große Karauschen.</ta>
            <ta e="T49" id="Seg_1365" s="T45">Dann kam ich zu meinem zweiten Sohn.</ta>
            <ta e="T52" id="Seg_1366" s="T49">Bei ihm [aß ich] gekochte Karauschen.</ta>
            <ta e="T62" id="Seg_1367" s="T52">Er hatte gebratene Karauschen und einen halben Kessel mit gekochtem Fleisch.</ta>
            <ta e="T65" id="Seg_1368" s="T62">Ich aß wieder.</ta>
            <ta e="T70" id="Seg_1369" s="T65">Dann kam ich zu Onkel Mikhaila nach Hause.</ta>
            <ta e="T74" id="Seg_1370" s="T70">Sie sagten: "Setz dich und iss."</ta>
            <ta e="T77" id="Seg_1371" s="T74">"Ich will nicht.</ta>
            <ta e="T81" id="Seg_1372" s="T77">Ich habe schon in zwei Häusern gegessen."</ta>
            <ta e="T85" id="Seg_1373" s="T81">Und sie sagten: "Setz dich."</ta>
            <ta e="T91" id="Seg_1374" s="T85">Ich wollte nicht, nun ja, ich setzte mich wieder hin.</ta>
            <ta e="T93" id="Seg_1375" s="T91">Man sollte sich setzen.</ta>
            <ta e="T96" id="Seg_1376" s="T93">Ich aß wieder ein wenig.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_1377" s="T1">Я в этом году в деревню (домой?) поеду.</ta>
            <ta e="T12" id="Seg_1378" s="T7">У нас в деревне людей много умерло.</ta>
            <ta e="T15" id="Seg_1379" s="T12">Может быть, еще умрут.</ta>
            <ta e="T17" id="Seg_1380" s="T15">Я поеду рыбачить.</ta>
            <ta e="T21" id="Seg_1381" s="T17">Потом я рыбы много съем.</ta>
            <ta e="T30" id="Seg_1382" s="T21">В первый раз буду есть, есть, а потом не захочу есть.</ta>
            <ta e="T34" id="Seg_1383" s="T30">В прошлом году я приехала.</ta>
            <ta e="T38" id="Seg_1384" s="T34">Рыбы у них много было.</ta>
            <ta e="T41" id="Seg_1385" s="T38">К старшему сыну я зашла.</ta>
            <ta e="T45" id="Seg_1386" s="T41">У него большие караси были.</ta>
            <ta e="T49" id="Seg_1387" s="T45">Потом я ко второму сыну пошла.</ta>
            <ta e="T52" id="Seg_1388" s="T49">У него карасей вареных.</ta>
            <ta e="T62" id="Seg_1389" s="T52">У него жаренные караси были и мяса половина чугуна вареного было.</ta>
            <ta e="T65" id="Seg_1390" s="T62">Я опять поела.</ta>
            <ta e="T70" id="Seg_1391" s="T65">Потом я пошла домой к дяде Михайле.</ta>
            <ta e="T74" id="Seg_1392" s="T70">Они говорят: “Садись есть”.</ta>
            <ta e="T77" id="Seg_1393" s="T74">“Не хочу я.</ta>
            <ta e="T81" id="Seg_1394" s="T77">Я в двух домах ела”.</ta>
            <ta e="T85" id="Seg_1395" s="T81">А они говорят: “Садись”.</ta>
            <ta e="T91" id="Seg_1396" s="T85">Я не хотела, ну, опять села.</ta>
            <ta e="T93" id="Seg_1397" s="T91">Сесть надо.</ta>
            <ta e="T96" id="Seg_1398" s="T93">Немножко опять поела.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_1399" s="T1">я в этом году домой поеду</ta>
            <ta e="T12" id="Seg_1400" s="T7">у нас в деревне много умерло</ta>
            <ta e="T15" id="Seg_1401" s="T12">может быть еще умрут</ta>
            <ta e="T17" id="Seg_1402" s="T15">пойду (поеду) рыбачить</ta>
            <ta e="T21" id="Seg_1403" s="T17">потом рыбы много съем</ta>
            <ta e="T30" id="Seg_1404" s="T21">вначале буду есть есть а потом не захочу есть</ta>
            <ta e="T34" id="Seg_1405" s="T30">в прошлом году я приехала</ta>
            <ta e="T38" id="Seg_1406" s="T34">рыбы у них много было</ta>
            <ta e="T41" id="Seg_1407" s="T38">к старшему сыну зашла</ta>
            <ta e="T45" id="Seg_1408" s="T41">у него большие караси были </ta>
            <ta e="T49" id="Seg_1409" s="T45">потом ко второму сыну пошла </ta>
            <ta e="T52" id="Seg_1410" s="T49">у него караси вареные</ta>
            <ta e="T62" id="Seg_1411" s="T52">у него жаренные караси были мяса половина чугуна вареного было</ta>
            <ta e="T65" id="Seg_1412" s="T62">я опять поела</ta>
            <ta e="T70" id="Seg_1413" s="T65">потом пошла домой к дяде Михайле</ta>
            <ta e="T74" id="Seg_1414" s="T70">они говорят садись есть</ta>
            <ta e="T77" id="Seg_1415" s="T74">не хочу</ta>
            <ta e="T81" id="Seg_1416" s="T77">я в двух домах ела</ta>
            <ta e="T85" id="Seg_1417" s="T81">а они говорят садись</ta>
            <ta e="T91" id="Seg_1418" s="T85">я не хотела опять села</ta>
            <ta e="T93" id="Seg_1419" s="T91">сесть надо</ta>
            <ta e="T96" id="Seg_1420" s="T93">Немножко опять поела.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T15" id="Seg_1421" s="T12"> || 3) Code switching or borrowing?</ta>
            <ta e="T34" id="Seg_1422" s="T30">[BrM:] Tentative analysis of 'potdəbon'.</ta>
            <ta e="T41" id="Seg_1423" s="T38">[BrM:] Unusual usage of Locative case.</ta>
            <ta e="T49" id="Seg_1424" s="T45"> ‎‎[BrM:] Unusual usage of Locative case.</ta>
            <ta e="T52" id="Seg_1425" s="T49">[BrM:] Unclear syntax.</ta>
            <ta e="T70" id="Seg_1426" s="T65">[KuAI:] Another variant: 'maːttɨ'. || on=ol=al? MOM</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
