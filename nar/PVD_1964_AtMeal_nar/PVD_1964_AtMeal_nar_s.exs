<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_AtMeal_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_AtMeal_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">36</ud-information>
            <ud-information attribute-name="# HIAT:w">24</ud-information>
            <ud-information attribute-name="# e">24</ud-information>
            <ud-information attribute-name="# HIAT:u">8</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T25" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Solan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">pɨkgəlɨt</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_11" n="HIAT:w" s="T3">näjɣum</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_14" n="HIAT:w" s="T4">tʼünaš</ts>
                  <nts id="Seg_15" n="HIAT:ip">.</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_18" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">Maʒelǯi</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">nʼäim</ts>
                  <nts id="Seg_24" n="HIAT:ip">.</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_27" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">Nadə</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">čʼaim</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">ütku</ts>
                  <nts id="Seg_36" n="HIAT:ip">.</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_39" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">Tattɨ</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">nʼaim</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_48" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">Man</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">tʼekka</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">tʼaran</ts>
                  <nts id="Seg_57" n="HIAT:ip">:</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_59" n="HIAT:ip">“</nts>
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">Nʼäin</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">tattɨ</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_68" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_70" n="HIAT:w" s="T17">Qaj</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_73" n="HIAT:w" s="T18">as</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_76" n="HIAT:w" s="T19">ütdədʼal</ts>
                  <nts id="Seg_77" n="HIAT:ip">?</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_80" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_82" n="HIAT:w" s="T20">Noɣolǯə</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_85" n="HIAT:w" s="T21">saxarɨn</ts>
                  <nts id="Seg_86" n="HIAT:ip">.</nts>
                  <nts id="Seg_87" n="HIAT:ip">”</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_90" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_92" n="HIAT:w" s="T22">Noɣolgu</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_95" n="HIAT:w" s="T23">nadə</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_98" n="HIAT:w" s="T24">saxar</ts>
                  <nts id="Seg_99" n="HIAT:ip">.</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T25" id="Seg_101" n="sc" s="T1">
               <ts e="T2" id="Seg_103" n="e" s="T1">Solan </ts>
               <ts e="T3" id="Seg_105" n="e" s="T2">pɨkgəlɨt, </ts>
               <ts e="T4" id="Seg_107" n="e" s="T3">näjɣum </ts>
               <ts e="T5" id="Seg_109" n="e" s="T4">tʼünaš. </ts>
               <ts e="T6" id="Seg_111" n="e" s="T5">Maʒelǯi </ts>
               <ts e="T7" id="Seg_113" n="e" s="T6">nʼäim. </ts>
               <ts e="T8" id="Seg_115" n="e" s="T7">Nadə </ts>
               <ts e="T9" id="Seg_117" n="e" s="T8">čʼaim </ts>
               <ts e="T10" id="Seg_119" n="e" s="T9">ütku. </ts>
               <ts e="T11" id="Seg_121" n="e" s="T10">Tattɨ </ts>
               <ts e="T12" id="Seg_123" n="e" s="T11">nʼaim. </ts>
               <ts e="T13" id="Seg_125" n="e" s="T12">Man </ts>
               <ts e="T14" id="Seg_127" n="e" s="T13">tʼekka </ts>
               <ts e="T15" id="Seg_129" n="e" s="T14">tʼaran: </ts>
               <ts e="T16" id="Seg_131" n="e" s="T15">“Nʼäin </ts>
               <ts e="T17" id="Seg_133" n="e" s="T16">tattɨ. </ts>
               <ts e="T18" id="Seg_135" n="e" s="T17">Qaj </ts>
               <ts e="T19" id="Seg_137" n="e" s="T18">as </ts>
               <ts e="T20" id="Seg_139" n="e" s="T19">ütdədʼal? </ts>
               <ts e="T21" id="Seg_141" n="e" s="T20">Noɣolǯə </ts>
               <ts e="T22" id="Seg_143" n="e" s="T21">saxarɨn.” </ts>
               <ts e="T23" id="Seg_145" n="e" s="T22">Noɣolgu </ts>
               <ts e="T24" id="Seg_147" n="e" s="T23">nadə </ts>
               <ts e="T25" id="Seg_149" n="e" s="T24">saxar. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_150" s="T1">PVD_1964_AtMeal_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_151" s="T5">PVD_1964_AtMeal_nar.002 (001.002)</ta>
            <ta e="T10" id="Seg_152" s="T7">PVD_1964_AtMeal_nar.003 (001.003)</ta>
            <ta e="T12" id="Seg_153" s="T10">PVD_1964_AtMeal_nar.004 (001.004)</ta>
            <ta e="T17" id="Seg_154" s="T12">PVD_1964_AtMeal_nar.005 (001.005)</ta>
            <ta e="T20" id="Seg_155" s="T17">PVD_1964_AtMeal_nar.006 (001.006)</ta>
            <ta e="T22" id="Seg_156" s="T20">PVD_1964_AtMeal_nar.007 (001.007)</ta>
            <ta e="T25" id="Seg_157" s="T22">PVD_1964_AtMeal_nar.008 (001.008)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_158" s="T1">′соlан пыкг̂ълыт, нӓйɣум ′тʼӱнаш.</ta>
            <ta e="T7" id="Seg_159" s="T5">ма′жеlджи ′нʼӓим.</ta>
            <ta e="T10" id="Seg_160" s="T7">надъ ′чʼаим ӱт′ку.</ta>
            <ta e="T12" id="Seg_161" s="T10">′татты ′нʼаим.</ta>
            <ta e="T17" id="Seg_162" s="T12">ман тʼекк(г̂)а тʼа′ра(ы)н: ′нʼӓин ′татты.</ta>
            <ta e="T20" id="Seg_163" s="T17">kай ас ′ӱтдъд̂ʼал?</ta>
            <ta e="T22" id="Seg_164" s="T20">но′ɣоlджъ ′сахарын.</ta>
            <ta e="T25" id="Seg_165" s="T22">но′ɣоlгу надъ сахар.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_166" s="T1">solan pɨkĝəlɨt, näjɣum tʼünaš.</ta>
            <ta e="T7" id="Seg_167" s="T5">maʒelǯi nʼäim.</ta>
            <ta e="T10" id="Seg_168" s="T7">nadə čʼaim ütku.</ta>
            <ta e="T12" id="Seg_169" s="T10">tattɨ nʼaim.</ta>
            <ta e="T17" id="Seg_170" s="T12">man tʼekk(ĝ)a tʼara(ɨ)n: nʼäin tattɨ.</ta>
            <ta e="T20" id="Seg_171" s="T17">qaj as ütdəd̂ʼal?</ta>
            <ta e="T22" id="Seg_172" s="T20">noɣolǯə saxarɨn.</ta>
            <ta e="T25" id="Seg_173" s="T22">noɣolgu nadə saxar.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_174" s="T1">Solan pɨkgəlɨt, näjɣum tʼünaš. </ta>
            <ta e="T7" id="Seg_175" s="T5">Maʒelǯi nʼäim. </ta>
            <ta e="T10" id="Seg_176" s="T7">Nadə čʼaim ütku. </ta>
            <ta e="T12" id="Seg_177" s="T10">Tattɨ nʼaim. </ta>
            <ta e="T17" id="Seg_178" s="T12">Man tʼekka tʼaran: “Nʼäin tattɨ. </ta>
            <ta e="T20" id="Seg_179" s="T17">Qaj as ütdədʼal? </ta>
            <ta e="T22" id="Seg_180" s="T20">Noɣolǯə saxarɨn.” </ta>
            <ta e="T25" id="Seg_181" s="T22">Noɣolgu nadə saxar. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_182" s="T1">solan</ta>
            <ta e="T3" id="Seg_183" s="T2">pɨkgəl-ɨ-t</ta>
            <ta e="T4" id="Seg_184" s="T3">nä-j-ɣum</ta>
            <ta e="T5" id="Seg_185" s="T4">tʼü-naš</ta>
            <ta e="T6" id="Seg_186" s="T5">maʒ-e-l-ǯi</ta>
            <ta e="T7" id="Seg_187" s="T6">nʼäi-m</ta>
            <ta e="T8" id="Seg_188" s="T7">nadə</ta>
            <ta e="T9" id="Seg_189" s="T8">čʼai-m</ta>
            <ta e="T10" id="Seg_190" s="T9">üt-ku</ta>
            <ta e="T11" id="Seg_191" s="T10">tat-tɨ</ta>
            <ta e="T12" id="Seg_192" s="T11">nʼai-m</ta>
            <ta e="T13" id="Seg_193" s="T12">man</ta>
            <ta e="T14" id="Seg_194" s="T13">tʼekka</ta>
            <ta e="T15" id="Seg_195" s="T14">tʼara-n</ta>
            <ta e="T16" id="Seg_196" s="T15">nʼäi-n</ta>
            <ta e="T17" id="Seg_197" s="T16">tat-tɨ</ta>
            <ta e="T18" id="Seg_198" s="T17">qaj</ta>
            <ta e="T19" id="Seg_199" s="T18">as</ta>
            <ta e="T20" id="Seg_200" s="T19">ütdə-dʼa-l</ta>
            <ta e="T21" id="Seg_201" s="T20">noɣol-ǯə</ta>
            <ta e="T22" id="Seg_202" s="T21">saxar-ɨ-n</ta>
            <ta e="T23" id="Seg_203" s="T22">noɣol-gu</ta>
            <ta e="T24" id="Seg_204" s="T23">nadə</ta>
            <ta e="T25" id="Seg_205" s="T24">saxar</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_206" s="T1">solaŋ</ta>
            <ta e="T3" id="Seg_207" s="T2">pɨŋgəl-ɨ-ntɨ</ta>
            <ta e="T4" id="Seg_208" s="T3">ne-lʼ-qum</ta>
            <ta e="T5" id="Seg_209" s="T4">tüː-nɨš</ta>
            <ta e="T6" id="Seg_210" s="T5">maǯə-ɨ-l-ǯi</ta>
            <ta e="T7" id="Seg_211" s="T6">nʼäj-m</ta>
            <ta e="T8" id="Seg_212" s="T7">nadə</ta>
            <ta e="T9" id="Seg_213" s="T8">čʼai-m</ta>
            <ta e="T10" id="Seg_214" s="T9">üt-gu</ta>
            <ta e="T11" id="Seg_215" s="T10">tat-etɨ</ta>
            <ta e="T12" id="Seg_216" s="T11">nʼäj-m</ta>
            <ta e="T13" id="Seg_217" s="T12">man</ta>
            <ta e="T14" id="Seg_218" s="T13">tekka</ta>
            <ta e="T15" id="Seg_219" s="T14">tʼärɨ-ŋ</ta>
            <ta e="T16" id="Seg_220" s="T15">nʼäj-m</ta>
            <ta e="T17" id="Seg_221" s="T16">tat-etɨ</ta>
            <ta e="T18" id="Seg_222" s="T17">qaj</ta>
            <ta e="T19" id="Seg_223" s="T18">asa</ta>
            <ta e="T20" id="Seg_224" s="T19">ündɨ-dʼi-l</ta>
            <ta e="T21" id="Seg_225" s="T20">noɣal-ǯi</ta>
            <ta e="T22" id="Seg_226" s="T21">saxar-ɨ-m</ta>
            <ta e="T23" id="Seg_227" s="T22">noɣal-gu</ta>
            <ta e="T24" id="Seg_228" s="T23">nadə</ta>
            <ta e="T25" id="Seg_229" s="T24">saxar</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_230" s="T1">spoon.[NOM]</ta>
            <ta e="T3" id="Seg_231" s="T2">fall.down-EP-INFER.[3SG.S]</ta>
            <ta e="T4" id="Seg_232" s="T3">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T5" id="Seg_233" s="T4">come-POT.FUT.3SG</ta>
            <ta e="T6" id="Seg_234" s="T5">cut-EP-INCH-IMP.2SG.S</ta>
            <ta e="T7" id="Seg_235" s="T6">bread-ACC</ta>
            <ta e="T8" id="Seg_236" s="T7">one.should</ta>
            <ta e="T9" id="Seg_237" s="T8">tea-ACC</ta>
            <ta e="T10" id="Seg_238" s="T9">drink-INF</ta>
            <ta e="T11" id="Seg_239" s="T10">bring-IMP.2SG.O</ta>
            <ta e="T12" id="Seg_240" s="T11">bread-ACC</ta>
            <ta e="T13" id="Seg_241" s="T12">I.NOM</ta>
            <ta e="T14" id="Seg_242" s="T13">you.ALL</ta>
            <ta e="T15" id="Seg_243" s="T14">say-1SG.S</ta>
            <ta e="T16" id="Seg_244" s="T15">bread-ACC</ta>
            <ta e="T17" id="Seg_245" s="T16">bring-IMP.2SG.O</ta>
            <ta e="T18" id="Seg_246" s="T17">what</ta>
            <ta e="T19" id="Seg_247" s="T18">NEG</ta>
            <ta e="T20" id="Seg_248" s="T19">hear-DRV-2SG.O</ta>
            <ta e="T21" id="Seg_249" s="T20">serve-IMP.2SG.S</ta>
            <ta e="T22" id="Seg_250" s="T21">sugar-EP-ACC</ta>
            <ta e="T23" id="Seg_251" s="T22">serve-INF</ta>
            <ta e="T24" id="Seg_252" s="T23">one.should</ta>
            <ta e="T25" id="Seg_253" s="T24">sugar.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_254" s="T1">ложка.[NOM]</ta>
            <ta e="T3" id="Seg_255" s="T2">упасть-EP-INFER.[3SG.S]</ta>
            <ta e="T4" id="Seg_256" s="T3">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T5" id="Seg_257" s="T4">прийти-POT.FUT.3SG</ta>
            <ta e="T6" id="Seg_258" s="T5">отрезать-EP-INCH-IMP.2SG.S</ta>
            <ta e="T7" id="Seg_259" s="T6">хлеб-ACC</ta>
            <ta e="T8" id="Seg_260" s="T7">надо</ta>
            <ta e="T9" id="Seg_261" s="T8">чай-ACC</ta>
            <ta e="T10" id="Seg_262" s="T9">пить-INF</ta>
            <ta e="T11" id="Seg_263" s="T10">принести-IMP.2SG.O</ta>
            <ta e="T12" id="Seg_264" s="T11">хлеб-ACC</ta>
            <ta e="T13" id="Seg_265" s="T12">я.NOM</ta>
            <ta e="T14" id="Seg_266" s="T13">ты.ALL</ta>
            <ta e="T15" id="Seg_267" s="T14">сказать-1SG.S</ta>
            <ta e="T16" id="Seg_268" s="T15">хлеб-ACC</ta>
            <ta e="T17" id="Seg_269" s="T16">принести-IMP.2SG.O</ta>
            <ta e="T18" id="Seg_270" s="T17">что</ta>
            <ta e="T19" id="Seg_271" s="T18">NEG</ta>
            <ta e="T20" id="Seg_272" s="T19">услышать-DRV-2SG.O</ta>
            <ta e="T21" id="Seg_273" s="T20">подать-IMP.2SG.S</ta>
            <ta e="T22" id="Seg_274" s="T21">сахар-EP-ACC</ta>
            <ta e="T23" id="Seg_275" s="T22">подать-INF</ta>
            <ta e="T24" id="Seg_276" s="T23">надо</ta>
            <ta e="T25" id="Seg_277" s="T24">сахар.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_278" s="T1">n.[n:case]</ta>
            <ta e="T3" id="Seg_279" s="T2">v-v:ins-v:mood.[v:pn]</ta>
            <ta e="T4" id="Seg_280" s="T3">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T5" id="Seg_281" s="T4">v-v:tense</ta>
            <ta e="T6" id="Seg_282" s="T5">v-v:ins-v&gt;v-v:mood.pn</ta>
            <ta e="T7" id="Seg_283" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_284" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_285" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_286" s="T9">v-v:inf</ta>
            <ta e="T11" id="Seg_287" s="T10">v-v:mood.pn</ta>
            <ta e="T12" id="Seg_288" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_289" s="T12">pers</ta>
            <ta e="T14" id="Seg_290" s="T13">pers</ta>
            <ta e="T15" id="Seg_291" s="T14">v-v:pn</ta>
            <ta e="T16" id="Seg_292" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_293" s="T16">v-v:mood.pn</ta>
            <ta e="T18" id="Seg_294" s="T17">interrog</ta>
            <ta e="T19" id="Seg_295" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_296" s="T19">v-v&gt;v-v:pn</ta>
            <ta e="T21" id="Seg_297" s="T20">v-v:mood.pn</ta>
            <ta e="T22" id="Seg_298" s="T21">n-n:ins-n:case</ta>
            <ta e="T23" id="Seg_299" s="T22">v-v:inf</ta>
            <ta e="T24" id="Seg_300" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_301" s="T24">n.[n:case]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_302" s="T1">n</ta>
            <ta e="T3" id="Seg_303" s="T2">v</ta>
            <ta e="T4" id="Seg_304" s="T3">n</ta>
            <ta e="T5" id="Seg_305" s="T4">v</ta>
            <ta e="T6" id="Seg_306" s="T5">v</ta>
            <ta e="T7" id="Seg_307" s="T6">n</ta>
            <ta e="T8" id="Seg_308" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_309" s="T8">n</ta>
            <ta e="T10" id="Seg_310" s="T9">v</ta>
            <ta e="T11" id="Seg_311" s="T10">v</ta>
            <ta e="T12" id="Seg_312" s="T11">n</ta>
            <ta e="T13" id="Seg_313" s="T12">pers</ta>
            <ta e="T14" id="Seg_314" s="T13">pers</ta>
            <ta e="T15" id="Seg_315" s="T14">v</ta>
            <ta e="T16" id="Seg_316" s="T15">n</ta>
            <ta e="T17" id="Seg_317" s="T16">v</ta>
            <ta e="T18" id="Seg_318" s="T17">interrog</ta>
            <ta e="T19" id="Seg_319" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_320" s="T19">v</ta>
            <ta e="T21" id="Seg_321" s="T20">v</ta>
            <ta e="T22" id="Seg_322" s="T21">n</ta>
            <ta e="T23" id="Seg_323" s="T22">v</ta>
            <ta e="T24" id="Seg_324" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_325" s="T24">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_326" s="T1">np:P</ta>
            <ta e="T4" id="Seg_327" s="T3">np.h:A</ta>
            <ta e="T6" id="Seg_328" s="T5">0.2.h:A</ta>
            <ta e="T7" id="Seg_329" s="T6">np:P</ta>
            <ta e="T10" id="Seg_330" s="T9">v:Th</ta>
            <ta e="T11" id="Seg_331" s="T10">0.2.h:A</ta>
            <ta e="T12" id="Seg_332" s="T11">np:Th</ta>
            <ta e="T13" id="Seg_333" s="T12">pro.h:A</ta>
            <ta e="T14" id="Seg_334" s="T13">pro.h:R</ta>
            <ta e="T16" id="Seg_335" s="T15">np:Th</ta>
            <ta e="T17" id="Seg_336" s="T16">0.2.h:A</ta>
            <ta e="T20" id="Seg_337" s="T19">0.2.h:E</ta>
            <ta e="T21" id="Seg_338" s="T20">0.2.h:A</ta>
            <ta e="T22" id="Seg_339" s="T21">np:Th</ta>
            <ta e="T23" id="Seg_340" s="T22">v:Th</ta>
            <ta e="T25" id="Seg_341" s="T24">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_342" s="T1">np:S</ta>
            <ta e="T3" id="Seg_343" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_344" s="T3">np.h:S</ta>
            <ta e="T5" id="Seg_345" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_346" s="T5">0.2.h:S v:pred</ta>
            <ta e="T7" id="Seg_347" s="T6">np:O</ta>
            <ta e="T8" id="Seg_348" s="T7">ptcl:pred</ta>
            <ta e="T10" id="Seg_349" s="T9">v:O</ta>
            <ta e="T11" id="Seg_350" s="T10">0.2.h:S v:pred</ta>
            <ta e="T12" id="Seg_351" s="T11">np:O</ta>
            <ta e="T13" id="Seg_352" s="T12">pro.h:S</ta>
            <ta e="T15" id="Seg_353" s="T14">v:pred</ta>
            <ta e="T16" id="Seg_354" s="T15">np:O</ta>
            <ta e="T17" id="Seg_355" s="T16">0.2.h:S v:pred</ta>
            <ta e="T20" id="Seg_356" s="T19">0.2.h:S v:pred</ta>
            <ta e="T21" id="Seg_357" s="T20">0.2.h:S v:pred</ta>
            <ta e="T22" id="Seg_358" s="T21">np:O</ta>
            <ta e="T23" id="Seg_359" s="T22">v:O</ta>
            <ta e="T24" id="Seg_360" s="T23">ptcl:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T8" id="Seg_361" s="T7">RUS:mod</ta>
            <ta e="T9" id="Seg_362" s="T8">RUS:cult</ta>
            <ta e="T22" id="Seg_363" s="T21">RUS:cult</ta>
            <ta e="T24" id="Seg_364" s="T23">RUS:mod</ta>
            <ta e="T25" id="Seg_365" s="T24">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T8" id="Seg_366" s="T7">Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T22" id="Seg_367" s="T21">dir:infl</ta>
            <ta e="T25" id="Seg_368" s="T24">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_369" s="T1">[If] a spoon falls, a woman comes.</ta>
            <ta e="T7" id="Seg_370" s="T5">Cut the bread.</ta>
            <ta e="T10" id="Seg_371" s="T7">[We] should drink tea.</ta>
            <ta e="T12" id="Seg_372" s="T10">Bring some bread.</ta>
            <ta e="T17" id="Seg_373" s="T12">I told you: “Bring some bread.</ta>
            <ta e="T20" id="Seg_374" s="T17">Don't you hear?</ta>
            <ta e="T22" id="Seg_375" s="T20">Serve some sugar.”</ta>
            <ta e="T25" id="Seg_376" s="T22">Sugar has to be served.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_377" s="T1">[Wenn] ein Löffel fällt, kommt eine Frau.</ta>
            <ta e="T7" id="Seg_378" s="T5">Schneide das Brot.</ta>
            <ta e="T10" id="Seg_379" s="T7">[Wir] sollten Tee trinken.</ta>
            <ta e="T12" id="Seg_380" s="T10">Bring das Brot.</ta>
            <ta e="T17" id="Seg_381" s="T12">Ich sage dir: "Bring mir Brot.</ta>
            <ta e="T20" id="Seg_382" s="T17">Hörst du nicht?</ta>
            <ta e="T22" id="Seg_383" s="T20">Servier den Zucker."</ta>
            <ta e="T25" id="Seg_384" s="T22">Zucker sollte serviert werden.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_385" s="T1">Ложка упала – женщина придет.</ta>
            <ta e="T7" id="Seg_386" s="T5">Нарежь хлеб.</ta>
            <ta e="T10" id="Seg_387" s="T7">Надо чай пить.</ta>
            <ta e="T12" id="Seg_388" s="T10">Принеси хлеб.</ta>
            <ta e="T17" id="Seg_389" s="T12">Я тебе сказала: “Хлеба принеси.</ta>
            <ta e="T20" id="Seg_390" s="T17">Что, не слышишь?</ta>
            <ta e="T22" id="Seg_391" s="T20">Подай сахара”.</ta>
            <ta e="T25" id="Seg_392" s="T22">Подать надо сахар.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_393" s="T1">ложка упала женщина придет</ta>
            <ta e="T7" id="Seg_394" s="T5">нарежь хлеба</ta>
            <ta e="T10" id="Seg_395" s="T7">надо чай пить</ta>
            <ta e="T12" id="Seg_396" s="T10">принеси хлеб</ta>
            <ta e="T17" id="Seg_397" s="T12">я тебе сказала принеси хлеба</ta>
            <ta e="T20" id="Seg_398" s="T17">что не слышишь</ta>
            <ta e="T22" id="Seg_399" s="T20">подай сахар</ta>
            <ta e="T25" id="Seg_400" s="T22">подать надо</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T5" id="Seg_401" s="T1">[BrM:] INFER?</ta>
            <ta e="T17" id="Seg_402" s="T12">[KuAI:] Variants: 'tʼekga, tʼarɨn'</ta>
            <ta e="T22" id="Seg_403" s="T20">[BrM:] Unusual GEN.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
