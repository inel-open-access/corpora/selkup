<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Baptizing_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Baptizing_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">71</ud-information>
            <ud-information attribute-name="# HIAT:w">51</ud-information>
            <ud-information attribute-name="# e">51</ud-information>
            <ud-information attribute-name="# HIAT:u">12</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T52" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">imadlau</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">wes</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">lostässau</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Tannä</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">lostät</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">Svetlanam</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_29" n="HIAT:u" s="T8">
                  <nts id="Seg_30" n="HIAT:ip">“</nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">Ass</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">kɨgan</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">lostägu</ts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip">”</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_43" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_45" n="HIAT:w" s="T11">Man</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">lostässau</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">kɨbamɨla</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">wargɨn</ts>
                  <nts id="Seg_55" n="HIAT:ip">.</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_58" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_60" n="HIAT:w" s="T15">Vanʼäu</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_63" n="HIAT:w" s="T16">čoʒopse</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">tättɨbɨs</ts>
                  <nts id="Seg_67" n="HIAT:ip">,</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">lostugu</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">ass</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">kɨgɨs</ts>
                  <nts id="Seg_77" n="HIAT:ip">.</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_80" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">Täbnä</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">köt</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">pot</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">jes</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_95" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_97" n="HIAT:w" s="T25">Čoʒɨbɨm</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_100" n="HIAT:w" s="T26">umdout</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">kak</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">tʼäbəkutdɨt</ts>
                  <nts id="Seg_107" n="HIAT:ip">,</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">i</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">meʒaut</ts>
                  <nts id="Seg_114" n="HIAT:ip">,</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_117" n="HIAT:w" s="T31">i</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_120" n="HIAT:w" s="T32">meʒaut</ts>
                  <nts id="Seg_121" n="HIAT:ip">.</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_124" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_126" n="HIAT:w" s="T33">Man</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_129" n="HIAT:w" s="T34">täbɨm</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_132" n="HIAT:w" s="T35">kɨgɨzau</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_135" n="HIAT:w" s="T36">tʼäkkɨtku</ts>
                  <nts id="Seg_136" n="HIAT:ip">.</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_139" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_141" n="HIAT:w" s="T37">Čoʒɨp</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_144" n="HIAT:w" s="T38">mekga</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_147" n="HIAT:w" s="T39">ass</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_150" n="HIAT:w" s="T40">meut</ts>
                  <nts id="Seg_151" n="HIAT:ip">.</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_154" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_156" n="HIAT:w" s="T41">Tʼärɨn</ts>
                  <nts id="Seg_157" n="HIAT:ip">:</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_159" n="HIAT:ip">“</nts>
                  <ts e="T43" id="Seg_161" n="HIAT:w" s="T42">Igə</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_164" n="HIAT:w" s="T43">tʼäkkɨtket</ts>
                  <nts id="Seg_165" n="HIAT:ip">”</nts>
                  <nts id="Seg_166" n="HIAT:ip">.</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_169" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_171" n="HIAT:w" s="T44">Man</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_174" n="HIAT:w" s="T45">täbɨm</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_177" n="HIAT:w" s="T46">kojkak</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_180" n="HIAT:w" s="T47">iːou</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_183" n="HIAT:w" s="T48">umdoɣɨndo</ts>
                  <nts id="Seg_184" n="HIAT:ip">.</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_187" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_189" n="HIAT:w" s="T49">Täp</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_192" n="HIAT:w" s="T50">umdɨmdə</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_195" n="HIAT:w" s="T51">oralbɨt</ts>
                  <nts id="Seg_196" n="HIAT:ip">.</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T52" id="Seg_198" n="sc" s="T1">
               <ts e="T2" id="Seg_200" n="e" s="T1">Man </ts>
               <ts e="T3" id="Seg_202" n="e" s="T2">imadlau </ts>
               <ts e="T4" id="Seg_204" n="e" s="T3">wes </ts>
               <ts e="T5" id="Seg_206" n="e" s="T4">lostässau. </ts>
               <ts e="T6" id="Seg_208" n="e" s="T5">Tannä </ts>
               <ts e="T7" id="Seg_210" n="e" s="T6">lostät </ts>
               <ts e="T8" id="Seg_212" n="e" s="T7">Svetlanam. </ts>
               <ts e="T9" id="Seg_214" n="e" s="T8">“Ass </ts>
               <ts e="T10" id="Seg_216" n="e" s="T9">kɨgan </ts>
               <ts e="T11" id="Seg_218" n="e" s="T10">lostägu.” </ts>
               <ts e="T12" id="Seg_220" n="e" s="T11">Man </ts>
               <ts e="T13" id="Seg_222" n="e" s="T12">lostässau </ts>
               <ts e="T14" id="Seg_224" n="e" s="T13">kɨbamɨla </ts>
               <ts e="T15" id="Seg_226" n="e" s="T14">wargɨn. </ts>
               <ts e="T16" id="Seg_228" n="e" s="T15">Vanʼäu </ts>
               <ts e="T17" id="Seg_230" n="e" s="T16">čoʒopse </ts>
               <ts e="T18" id="Seg_232" n="e" s="T17">tättɨbɨs, </ts>
               <ts e="T19" id="Seg_234" n="e" s="T18">lostugu </ts>
               <ts e="T20" id="Seg_236" n="e" s="T19">ass </ts>
               <ts e="T21" id="Seg_238" n="e" s="T20">kɨgɨs. </ts>
               <ts e="T22" id="Seg_240" n="e" s="T21">Täbnä </ts>
               <ts e="T23" id="Seg_242" n="e" s="T22">köt </ts>
               <ts e="T24" id="Seg_244" n="e" s="T23">pot </ts>
               <ts e="T25" id="Seg_246" n="e" s="T24">jes. </ts>
               <ts e="T26" id="Seg_248" n="e" s="T25">Čoʒɨbɨm </ts>
               <ts e="T27" id="Seg_250" n="e" s="T26">umdout </ts>
               <ts e="T28" id="Seg_252" n="e" s="T27">kak </ts>
               <ts e="T29" id="Seg_254" n="e" s="T28">tʼäbəkutdɨt, </ts>
               <ts e="T30" id="Seg_256" n="e" s="T29">i </ts>
               <ts e="T31" id="Seg_258" n="e" s="T30">meʒaut, </ts>
               <ts e="T32" id="Seg_260" n="e" s="T31">i </ts>
               <ts e="T33" id="Seg_262" n="e" s="T32">meʒaut. </ts>
               <ts e="T34" id="Seg_264" n="e" s="T33">Man </ts>
               <ts e="T35" id="Seg_266" n="e" s="T34">täbɨm </ts>
               <ts e="T36" id="Seg_268" n="e" s="T35">kɨgɨzau </ts>
               <ts e="T37" id="Seg_270" n="e" s="T36">tʼäkkɨtku. </ts>
               <ts e="T38" id="Seg_272" n="e" s="T37">Čoʒɨp </ts>
               <ts e="T39" id="Seg_274" n="e" s="T38">mekga </ts>
               <ts e="T40" id="Seg_276" n="e" s="T39">ass </ts>
               <ts e="T41" id="Seg_278" n="e" s="T40">meut. </ts>
               <ts e="T42" id="Seg_280" n="e" s="T41">Tʼärɨn: </ts>
               <ts e="T43" id="Seg_282" n="e" s="T42">“Igə </ts>
               <ts e="T44" id="Seg_284" n="e" s="T43">tʼäkkɨtket”. </ts>
               <ts e="T45" id="Seg_286" n="e" s="T44">Man </ts>
               <ts e="T46" id="Seg_288" n="e" s="T45">täbɨm </ts>
               <ts e="T47" id="Seg_290" n="e" s="T46">kojkak </ts>
               <ts e="T48" id="Seg_292" n="e" s="T47">iːou </ts>
               <ts e="T49" id="Seg_294" n="e" s="T48">umdoɣɨndo. </ts>
               <ts e="T50" id="Seg_296" n="e" s="T49">Täp </ts>
               <ts e="T51" id="Seg_298" n="e" s="T50">umdɨmdə </ts>
               <ts e="T52" id="Seg_300" n="e" s="T51">oralbɨt. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_301" s="T1">PVD_1964_Baptizing_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_302" s="T5">PVD_1964_Baptizing_nar.002 (001.002)</ta>
            <ta e="T11" id="Seg_303" s="T8">PVD_1964_Baptizing_nar.003 (001.003)</ta>
            <ta e="T15" id="Seg_304" s="T11">PVD_1964_Baptizing_nar.004 (001.004)</ta>
            <ta e="T21" id="Seg_305" s="T15">PVD_1964_Baptizing_nar.005 (001.005)</ta>
            <ta e="T25" id="Seg_306" s="T21">PVD_1964_Baptizing_nar.006 (001.006)</ta>
            <ta e="T33" id="Seg_307" s="T25">PVD_1964_Baptizing_nar.007 (001.007)</ta>
            <ta e="T37" id="Seg_308" s="T33">PVD_1964_Baptizing_nar.008 (001.008)</ta>
            <ta e="T41" id="Seg_309" s="T37">PVD_1964_Baptizing_nar.009 (001.009)</ta>
            <ta e="T44" id="Seg_310" s="T41">PVD_1964_Baptizing_nar.010 (001.010)</ta>
            <ta e="T49" id="Seg_311" s="T44">PVD_1964_Baptizing_nar.011 (001.011)</ta>
            <ta e="T52" id="Seg_312" s="T49">PVD_1964_Baptizing_nar.012 (001.012)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_313" s="T1">ман и′мадлау вес ′лостӓссау̹.</ta>
            <ta e="T8" id="Seg_314" s="T5">тан′нӓ ′лостӓт Свет′ланам.</ta>
            <ta e="T11" id="Seg_315" s="T8">асс кы′ган ′лостӓгу.</ta>
            <ta e="T15" id="Seg_316" s="T11">ман ′лостӓссау̹ кы′бамыла вар′гын.</ta>
            <ta e="T21" id="Seg_317" s="T15">Ва′нʼӓу ′тшожопсе тӓтты′быс, ′лостугу асс кы′гыс.</ta>
            <ta e="T25" id="Seg_318" s="T21">тӓб′нӓ кӧт пот jес.</ta>
            <ta e="T33" id="Seg_319" s="T25">′тшожыбым ум′доут как ′тʼӓбъ‵кутдыт, и ме′жаут, и ме′жаут.</ta>
            <ta e="T37" id="Seg_320" s="T33">ман ′тӓбым кыгы′зау тʼӓк′кытку.</ta>
            <ta e="T41" id="Seg_321" s="T37">′тшожып ′мекга асс ме′ут.</ta>
            <ta e="T44" id="Seg_322" s="T41">тʼӓ′рын: ′игъ ′тʼӓккыт′кет.</ta>
            <ta e="T49" id="Seg_323" s="T44">ман тӓ′бым кой-как ′ӣоу̹ у′мдоɣындо.</ta>
            <ta e="T52" id="Seg_324" s="T49">тӓп умдымдъ о′рал′быт.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_325" s="T1">man imadlau wes lostässau̹.</ta>
            <ta e="T8" id="Seg_326" s="T5">tannä lostät Сwetlanam.</ta>
            <ta e="T11" id="Seg_327" s="T8">ass kɨgan lostägu.</ta>
            <ta e="T15" id="Seg_328" s="T11">man lostässau̹ kɨbamɨla wargɨn.</ta>
            <ta e="T21" id="Seg_329" s="T15">Вanʼäu tšoʒopse tättɨbɨs, lostugu ass kɨgɨs.</ta>
            <ta e="T25" id="Seg_330" s="T21">täbnä köt pot jes.</ta>
            <ta e="T33" id="Seg_331" s="T25">tšoʒɨbɨm umdout kak tʼäbəkutdɨt, i meʒaut, i meʒaut.</ta>
            <ta e="T37" id="Seg_332" s="T33">man täbɨm kɨgɨzau tʼäkkɨtku.</ta>
            <ta e="T41" id="Seg_333" s="T37">tšoʒɨp mekga ass meut.</ta>
            <ta e="T44" id="Seg_334" s="T41">tʼärɨn: igə tʼäkkɨtket.</ta>
            <ta e="T49" id="Seg_335" s="T44">man täbɨm koj-kak iːou̹ umdoɣɨndo.</ta>
            <ta e="T52" id="Seg_336" s="T49">täp umdɨmdə oralbɨt.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_337" s="T1">Man imadlau wes lostässau. </ta>
            <ta e="T8" id="Seg_338" s="T5">Tannä lostät Svetlanam. </ta>
            <ta e="T11" id="Seg_339" s="T8">“Ass kɨgan lostägu.” </ta>
            <ta e="T15" id="Seg_340" s="T11">Man lostässau kɨbamɨla wargɨn. </ta>
            <ta e="T21" id="Seg_341" s="T15">Vanʼäu čoʒopse tättɨbɨs, lostugu ass kɨgɨs. </ta>
            <ta e="T25" id="Seg_342" s="T21">Täbnä köt pot jes. </ta>
            <ta e="T33" id="Seg_343" s="T25">Čoʒɨbɨm umdout kak tʼäbəkutdɨt, i meʒaut, i meʒaut. </ta>
            <ta e="T37" id="Seg_344" s="T33">Man täbɨm kɨgɨzau tʼäkkɨtku. </ta>
            <ta e="T41" id="Seg_345" s="T37">Čoʒɨp mekga ass meut. </ta>
            <ta e="T44" id="Seg_346" s="T41">Tʼärɨn: “Igə tʼäkkɨtket”. </ta>
            <ta e="T49" id="Seg_347" s="T44">Man täbɨm kojkak iːou umdoɣɨndo. </ta>
            <ta e="T52" id="Seg_348" s="T49">Täp umdɨmdə oralbɨt. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_349" s="T1">man</ta>
            <ta e="T3" id="Seg_350" s="T2">imad-la-u</ta>
            <ta e="T4" id="Seg_351" s="T3">wes</ta>
            <ta e="T5" id="Seg_352" s="T4">lostä-ssa-u</ta>
            <ta e="T6" id="Seg_353" s="T5">tan-nä</ta>
            <ta e="T7" id="Seg_354" s="T6">lost-ät</ta>
            <ta e="T8" id="Seg_355" s="T7">Svetlana-m</ta>
            <ta e="T9" id="Seg_356" s="T8">ass</ta>
            <ta e="T10" id="Seg_357" s="T9">kɨga-n</ta>
            <ta e="T11" id="Seg_358" s="T10">lostä-gu</ta>
            <ta e="T12" id="Seg_359" s="T11">man</ta>
            <ta e="T13" id="Seg_360" s="T12">lostä-ssa-u</ta>
            <ta e="T14" id="Seg_361" s="T13">kɨba-mɨ-la</ta>
            <ta e="T15" id="Seg_362" s="T14">wargɨ-n</ta>
            <ta e="T16" id="Seg_363" s="T15">Vanʼä-u</ta>
            <ta e="T17" id="Seg_364" s="T16">čoʒop-se</ta>
            <ta e="T18" id="Seg_365" s="T17">tättɨ-bɨ-s</ta>
            <ta e="T19" id="Seg_366" s="T18">lostu-gu</ta>
            <ta e="T20" id="Seg_367" s="T19">ass</ta>
            <ta e="T21" id="Seg_368" s="T20">kɨgɨ-s</ta>
            <ta e="T22" id="Seg_369" s="T21">täb-nä</ta>
            <ta e="T23" id="Seg_370" s="T22">köt</ta>
            <ta e="T24" id="Seg_371" s="T23">po-t</ta>
            <ta e="T25" id="Seg_372" s="T24">je-s</ta>
            <ta e="T26" id="Seg_373" s="T25">čoʒɨb-ɨ-m</ta>
            <ta e="T27" id="Seg_374" s="T26">umdo-ut</ta>
            <ta e="T28" id="Seg_375" s="T27">kak</ta>
            <ta e="T29" id="Seg_376" s="T28">tʼäbə-ku-tdɨ-t</ta>
            <ta e="T30" id="Seg_377" s="T29">i</ta>
            <ta e="T31" id="Seg_378" s="T30">meʒau-t</ta>
            <ta e="T32" id="Seg_379" s="T31">i</ta>
            <ta e="T33" id="Seg_380" s="T32">meʒau-t</ta>
            <ta e="T34" id="Seg_381" s="T33">man</ta>
            <ta e="T35" id="Seg_382" s="T34">täb-ɨ-m</ta>
            <ta e="T36" id="Seg_383" s="T35">kɨgɨ-za-u</ta>
            <ta e="T37" id="Seg_384" s="T36">tʼäkkɨt-ku</ta>
            <ta e="T38" id="Seg_385" s="T37">čoʒɨp</ta>
            <ta e="T39" id="Seg_386" s="T38">mekga</ta>
            <ta e="T40" id="Seg_387" s="T39">ass</ta>
            <ta e="T41" id="Seg_388" s="T40">me-u-t</ta>
            <ta e="T42" id="Seg_389" s="T41">tʼärɨ-n</ta>
            <ta e="T43" id="Seg_390" s="T42">igə</ta>
            <ta e="T44" id="Seg_391" s="T43">tʼäkkɨt-k-et</ta>
            <ta e="T45" id="Seg_392" s="T44">man</ta>
            <ta e="T46" id="Seg_393" s="T45">täb-ɨ-m</ta>
            <ta e="T47" id="Seg_394" s="T46">kojkak</ta>
            <ta e="T48" id="Seg_395" s="T47">iː-o-u</ta>
            <ta e="T49" id="Seg_396" s="T48">umdo-ɣɨndo</ta>
            <ta e="T50" id="Seg_397" s="T49">täp</ta>
            <ta e="T51" id="Seg_398" s="T50">umdɨ-m-də</ta>
            <ta e="T52" id="Seg_399" s="T51">oral-bɨ-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_400" s="T1">man</ta>
            <ta e="T3" id="Seg_401" s="T2">ilmat-la-w</ta>
            <ta e="T4" id="Seg_402" s="T3">wesʼ</ta>
            <ta e="T5" id="Seg_403" s="T4">loːstu-sɨ-w</ta>
            <ta e="T6" id="Seg_404" s="T5">tan-näj</ta>
            <ta e="T7" id="Seg_405" s="T6">loːstu-etɨ</ta>
            <ta e="T8" id="Seg_406" s="T7">Svetlana-m</ta>
            <ta e="T9" id="Seg_407" s="T8">asa</ta>
            <ta e="T10" id="Seg_408" s="T9">kɨgɨ-ŋ</ta>
            <ta e="T11" id="Seg_409" s="T10">loːstu-gu</ta>
            <ta e="T12" id="Seg_410" s="T11">man</ta>
            <ta e="T13" id="Seg_411" s="T12">loːstu-sɨ-w</ta>
            <ta e="T14" id="Seg_412" s="T13">qɨba-mɨ-la</ta>
            <ta e="T15" id="Seg_413" s="T14">wargɨ-ŋ</ta>
            <ta e="T16" id="Seg_414" s="T15">Vanʼä-w</ta>
            <ta e="T17" id="Seg_415" s="T16">čoʒɨp-se</ta>
            <ta e="T18" id="Seg_416" s="T17">tɨtdɨ-mbɨ-sɨ</ta>
            <ta e="T19" id="Seg_417" s="T18">loːstu-gu</ta>
            <ta e="T20" id="Seg_418" s="T19">asa</ta>
            <ta e="T21" id="Seg_419" s="T20">kɨgɨ-sɨ</ta>
            <ta e="T22" id="Seg_420" s="T21">täp-nä</ta>
            <ta e="T23" id="Seg_421" s="T22">köt</ta>
            <ta e="T24" id="Seg_422" s="T23">po-tə</ta>
            <ta e="T25" id="Seg_423" s="T24">eː-sɨ</ta>
            <ta e="T26" id="Seg_424" s="T25">čoʒɨp-ɨ-m</ta>
            <ta e="T27" id="Seg_425" s="T26">umdɨ-un</ta>
            <ta e="T28" id="Seg_426" s="T27">kak</ta>
            <ta e="T29" id="Seg_427" s="T28">tʼabɨ-ku-ntɨ-t</ta>
            <ta e="T30" id="Seg_428" s="T29">i</ta>
            <ta e="T31" id="Seg_429" s="T30">meʒal-t</ta>
            <ta e="T32" id="Seg_430" s="T31">i</ta>
            <ta e="T33" id="Seg_431" s="T32">meʒal-t</ta>
            <ta e="T34" id="Seg_432" s="T33">man</ta>
            <ta e="T35" id="Seg_433" s="T34">täp-ɨ-m</ta>
            <ta e="T36" id="Seg_434" s="T35">kɨgɨ-sɨ-w</ta>
            <ta e="T37" id="Seg_435" s="T36">tʼäkkut-gu</ta>
            <ta e="T38" id="Seg_436" s="T37">čoʒɨp</ta>
            <ta e="T39" id="Seg_437" s="T38">mekka</ta>
            <ta e="T40" id="Seg_438" s="T39">asa</ta>
            <ta e="T41" id="Seg_439" s="T40">me-nɨ-t</ta>
            <ta e="T42" id="Seg_440" s="T41">tʼärɨ-n</ta>
            <ta e="T43" id="Seg_441" s="T42">igə</ta>
            <ta e="T44" id="Seg_442" s="T43">tʼäkkut-ku-etɨ</ta>
            <ta e="T45" id="Seg_443" s="T44">man</ta>
            <ta e="T46" id="Seg_444" s="T45">täp-ɨ-m</ta>
            <ta e="T47" id="Seg_445" s="T46">kojkak</ta>
            <ta e="T48" id="Seg_446" s="T47">iː-ɨ-w</ta>
            <ta e="T49" id="Seg_447" s="T48">umdɨ-qɨntɨ</ta>
            <ta e="T50" id="Seg_448" s="T49">täp</ta>
            <ta e="T51" id="Seg_449" s="T50">umdɨ-m-tə</ta>
            <ta e="T52" id="Seg_450" s="T51">oral-mbɨ-t</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_451" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_452" s="T2">child-PL.[NOM]-1SG</ta>
            <ta e="T4" id="Seg_453" s="T3">all</ta>
            <ta e="T5" id="Seg_454" s="T4">baptize-PST-1SG.O</ta>
            <ta e="T6" id="Seg_455" s="T5">you.SG.NOM-EMPH</ta>
            <ta e="T7" id="Seg_456" s="T6">baptize-IMP.2SG.O</ta>
            <ta e="T8" id="Seg_457" s="T7">Svetlana-ACC</ta>
            <ta e="T9" id="Seg_458" s="T8">NEG</ta>
            <ta e="T10" id="Seg_459" s="T9">want-1SG.S</ta>
            <ta e="T11" id="Seg_460" s="T10">baptize-INF</ta>
            <ta e="T12" id="Seg_461" s="T11">I.NOM</ta>
            <ta e="T13" id="Seg_462" s="T12">baptize-PST-1SG.O</ta>
            <ta e="T14" id="Seg_463" s="T13">small-something-PL.[NOM]</ta>
            <ta e="T15" id="Seg_464" s="T14">big-ADVZ</ta>
            <ta e="T16" id="Seg_465" s="T15">Vanya.[NOM]-1SG</ta>
            <ta e="T17" id="Seg_466" s="T16">priest-COM</ta>
            <ta e="T18" id="Seg_467" s="T17">swear-DUR-PST.[3SG.S]</ta>
            <ta e="T19" id="Seg_468" s="T18">baptize-INF</ta>
            <ta e="T20" id="Seg_469" s="T19">NEG</ta>
            <ta e="T21" id="Seg_470" s="T20">want-PST.[3SG.S]</ta>
            <ta e="T22" id="Seg_471" s="T21">(s)he-ALL</ta>
            <ta e="T23" id="Seg_472" s="T22">ten</ta>
            <ta e="T24" id="Seg_473" s="T23">year.[NOM]-3SG</ta>
            <ta e="T25" id="Seg_474" s="T24">be-PST.[3SG.S]</ta>
            <ta e="T26" id="Seg_475" s="T25">priest-EP-ACC</ta>
            <ta e="T27" id="Seg_476" s="T26">beard-PROL</ta>
            <ta e="T28" id="Seg_477" s="T27">suddenly</ta>
            <ta e="T29" id="Seg_478" s="T28">seize-HAB-INFER-3SG.O</ta>
            <ta e="T30" id="Seg_479" s="T29">and</ta>
            <ta e="T31" id="Seg_480" s="T30">pull-3SG.O</ta>
            <ta e="T32" id="Seg_481" s="T31">and</ta>
            <ta e="T33" id="Seg_482" s="T32">pull-3SG.O</ta>
            <ta e="T34" id="Seg_483" s="T33">I.NOM</ta>
            <ta e="T35" id="Seg_484" s="T34">(s)he-EP-ACC</ta>
            <ta e="T36" id="Seg_485" s="T35">want-PST-1SG.O</ta>
            <ta e="T37" id="Seg_486" s="T36">whip-INF</ta>
            <ta e="T38" id="Seg_487" s="T37">priest.[NOM]</ta>
            <ta e="T39" id="Seg_488" s="T38">I.ALL</ta>
            <ta e="T40" id="Seg_489" s="T39">NEG</ta>
            <ta e="T41" id="Seg_490" s="T40">give-CO-3SG.O</ta>
            <ta e="T42" id="Seg_491" s="T41">say-3SG.S</ta>
            <ta e="T43" id="Seg_492" s="T42">NEG.IMP</ta>
            <ta e="T44" id="Seg_493" s="T43">whip-HAB-IMP.2SG.O</ta>
            <ta e="T45" id="Seg_494" s="T44">I.NOM</ta>
            <ta e="T46" id="Seg_495" s="T45">(s)he-EP-ACC</ta>
            <ta e="T47" id="Seg_496" s="T46">somehow</ta>
            <ta e="T48" id="Seg_497" s="T47">take-EP-1SG.O</ta>
            <ta e="T49" id="Seg_498" s="T48">beard-LOC.3SG</ta>
            <ta e="T50" id="Seg_499" s="T49">(s)he.[NOM]</ta>
            <ta e="T51" id="Seg_500" s="T50">beard-ACC-3SG</ta>
            <ta e="T52" id="Seg_501" s="T51">take-DUR-3SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_502" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_503" s="T2">ребёнок-PL.[NOM]-1SG</ta>
            <ta e="T4" id="Seg_504" s="T3">весь</ta>
            <ta e="T5" id="Seg_505" s="T4">окрестить-PST-1SG.O</ta>
            <ta e="T6" id="Seg_506" s="T5">ты.NOM-EMPH</ta>
            <ta e="T7" id="Seg_507" s="T6">окрестить-IMP.2SG.O</ta>
            <ta e="T8" id="Seg_508" s="T7">Светлана-ACC</ta>
            <ta e="T9" id="Seg_509" s="T8">NEG</ta>
            <ta e="T10" id="Seg_510" s="T9">хотеть-1SG.S</ta>
            <ta e="T11" id="Seg_511" s="T10">окрестить-INF</ta>
            <ta e="T12" id="Seg_512" s="T11">я.NOM</ta>
            <ta e="T13" id="Seg_513" s="T12">окрестить-PST-1SG.O</ta>
            <ta e="T14" id="Seg_514" s="T13">маленький-нечто-PL.[NOM]</ta>
            <ta e="T15" id="Seg_515" s="T14">большой-ADVZ</ta>
            <ta e="T16" id="Seg_516" s="T15">Ваня.[NOM]-1SG</ta>
            <ta e="T17" id="Seg_517" s="T16">поп-COM</ta>
            <ta e="T18" id="Seg_518" s="T17">поругаться-DUR-PST.[3SG.S]</ta>
            <ta e="T19" id="Seg_519" s="T18">окрестить-INF</ta>
            <ta e="T20" id="Seg_520" s="T19">NEG</ta>
            <ta e="T21" id="Seg_521" s="T20">хотеть-PST.[3SG.S]</ta>
            <ta e="T22" id="Seg_522" s="T21">он(а)-ALL</ta>
            <ta e="T23" id="Seg_523" s="T22">десять</ta>
            <ta e="T24" id="Seg_524" s="T23">год.[NOM]-3SG</ta>
            <ta e="T25" id="Seg_525" s="T24">быть-PST.[3SG.S]</ta>
            <ta e="T26" id="Seg_526" s="T25">поп-EP-ACC</ta>
            <ta e="T27" id="Seg_527" s="T26">борода-PROL</ta>
            <ta e="T28" id="Seg_528" s="T27">как</ta>
            <ta e="T29" id="Seg_529" s="T28">схватить-HAB-INFER-3SG.O</ta>
            <ta e="T30" id="Seg_530" s="T29">и</ta>
            <ta e="T31" id="Seg_531" s="T30">вырвать-3SG.O</ta>
            <ta e="T32" id="Seg_532" s="T31">и</ta>
            <ta e="T33" id="Seg_533" s="T32">вырвать-3SG.O</ta>
            <ta e="T34" id="Seg_534" s="T33">я.NOM</ta>
            <ta e="T35" id="Seg_535" s="T34">он(а)-EP-ACC</ta>
            <ta e="T36" id="Seg_536" s="T35">хотеть-PST-1SG.O</ta>
            <ta e="T37" id="Seg_537" s="T36">отстегать-INF</ta>
            <ta e="T38" id="Seg_538" s="T37">поп.[NOM]</ta>
            <ta e="T39" id="Seg_539" s="T38">я.ALL</ta>
            <ta e="T40" id="Seg_540" s="T39">NEG</ta>
            <ta e="T41" id="Seg_541" s="T40">дать-CO-3SG.O</ta>
            <ta e="T42" id="Seg_542" s="T41">сказать-3SG.S</ta>
            <ta e="T43" id="Seg_543" s="T42">NEG.IMP</ta>
            <ta e="T44" id="Seg_544" s="T43">отстегать-HAB-IMP.2SG.O</ta>
            <ta e="T45" id="Seg_545" s="T44">я.NOM</ta>
            <ta e="T46" id="Seg_546" s="T45">он(а)-EP-ACC</ta>
            <ta e="T47" id="Seg_547" s="T46">кое_как</ta>
            <ta e="T48" id="Seg_548" s="T47">взять-EP-1SG.O</ta>
            <ta e="T49" id="Seg_549" s="T48">борода-LOC.3SG</ta>
            <ta e="T50" id="Seg_550" s="T49">он(а).[NOM]</ta>
            <ta e="T51" id="Seg_551" s="T50">борода-ACC-3SG</ta>
            <ta e="T52" id="Seg_552" s="T51">взять-DUR-3SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_553" s="T1">pers</ta>
            <ta e="T3" id="Seg_554" s="T2">n-n:num.[n:case]-n:poss</ta>
            <ta e="T4" id="Seg_555" s="T3">quant</ta>
            <ta e="T5" id="Seg_556" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_557" s="T5">pers-clit</ta>
            <ta e="T7" id="Seg_558" s="T6">v-v:mood.pn</ta>
            <ta e="T8" id="Seg_559" s="T7">nprop-n:case</ta>
            <ta e="T9" id="Seg_560" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_561" s="T9">v-v:pn</ta>
            <ta e="T11" id="Seg_562" s="T10">v-v:inf</ta>
            <ta e="T12" id="Seg_563" s="T11">pers</ta>
            <ta e="T13" id="Seg_564" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_565" s="T13">adj-n-n:num.[n:case]</ta>
            <ta e="T15" id="Seg_566" s="T14">adj-adj&gt;adv</ta>
            <ta e="T16" id="Seg_567" s="T15">nprop.[n:case]-n:poss</ta>
            <ta e="T17" id="Seg_568" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_569" s="T17">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T19" id="Seg_570" s="T18">v-v:inf</ta>
            <ta e="T20" id="Seg_571" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_572" s="T20">v-v:tense.[v:pn]</ta>
            <ta e="T22" id="Seg_573" s="T21">pers-n:case</ta>
            <ta e="T23" id="Seg_574" s="T22">num</ta>
            <ta e="T24" id="Seg_575" s="T23">n.[n:case]-n:poss</ta>
            <ta e="T25" id="Seg_576" s="T24">v-v:tense.[v:pn]</ta>
            <ta e="T26" id="Seg_577" s="T25">n-n:ins-n:case</ta>
            <ta e="T27" id="Seg_578" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_579" s="T27">adv</ta>
            <ta e="T29" id="Seg_580" s="T28">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T30" id="Seg_581" s="T29">conj</ta>
            <ta e="T31" id="Seg_582" s="T30">v-v:pn</ta>
            <ta e="T32" id="Seg_583" s="T31">conj</ta>
            <ta e="T33" id="Seg_584" s="T32">v-v:pn</ta>
            <ta e="T34" id="Seg_585" s="T33">pers</ta>
            <ta e="T35" id="Seg_586" s="T34">pers-n:ins-n:case</ta>
            <ta e="T36" id="Seg_587" s="T35">v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_588" s="T36">v-v:inf</ta>
            <ta e="T38" id="Seg_589" s="T37">n.[n:case]</ta>
            <ta e="T39" id="Seg_590" s="T38">pers</ta>
            <ta e="T40" id="Seg_591" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_592" s="T40">v-v:ins-v:pn</ta>
            <ta e="T42" id="Seg_593" s="T41">v-v:pn</ta>
            <ta e="T43" id="Seg_594" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_595" s="T43">v-v&gt;v-v:mood.pn</ta>
            <ta e="T45" id="Seg_596" s="T44">pers</ta>
            <ta e="T46" id="Seg_597" s="T45">pers-n:ins-n:case</ta>
            <ta e="T47" id="Seg_598" s="T46">adv</ta>
            <ta e="T48" id="Seg_599" s="T47">v-v:ins-v:pn</ta>
            <ta e="T49" id="Seg_600" s="T48">n-n:case.poss</ta>
            <ta e="T50" id="Seg_601" s="T49">pers.[n:case]</ta>
            <ta e="T51" id="Seg_602" s="T50">n-n:case-n:poss</ta>
            <ta e="T52" id="Seg_603" s="T51">v-v&gt;v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_604" s="T1">pers</ta>
            <ta e="T3" id="Seg_605" s="T2">n</ta>
            <ta e="T4" id="Seg_606" s="T3">quant</ta>
            <ta e="T5" id="Seg_607" s="T4">v</ta>
            <ta e="T6" id="Seg_608" s="T5">pers</ta>
            <ta e="T7" id="Seg_609" s="T6">v</ta>
            <ta e="T8" id="Seg_610" s="T7">nprop</ta>
            <ta e="T9" id="Seg_611" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_612" s="T9">v</ta>
            <ta e="T11" id="Seg_613" s="T10">v</ta>
            <ta e="T12" id="Seg_614" s="T11">pers</ta>
            <ta e="T13" id="Seg_615" s="T12">v</ta>
            <ta e="T14" id="Seg_616" s="T13">n</ta>
            <ta e="T15" id="Seg_617" s="T14">adv</ta>
            <ta e="T16" id="Seg_618" s="T15">nprop</ta>
            <ta e="T17" id="Seg_619" s="T16">n</ta>
            <ta e="T18" id="Seg_620" s="T17">v</ta>
            <ta e="T19" id="Seg_621" s="T18">v</ta>
            <ta e="T20" id="Seg_622" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_623" s="T20">v</ta>
            <ta e="T22" id="Seg_624" s="T21">pers</ta>
            <ta e="T23" id="Seg_625" s="T22">num</ta>
            <ta e="T24" id="Seg_626" s="T23">n</ta>
            <ta e="T25" id="Seg_627" s="T24">v</ta>
            <ta e="T26" id="Seg_628" s="T25">n</ta>
            <ta e="T27" id="Seg_629" s="T26">n</ta>
            <ta e="T28" id="Seg_630" s="T27">adv</ta>
            <ta e="T29" id="Seg_631" s="T28">v</ta>
            <ta e="T30" id="Seg_632" s="T29">conj</ta>
            <ta e="T31" id="Seg_633" s="T30">v</ta>
            <ta e="T32" id="Seg_634" s="T31">conj</ta>
            <ta e="T33" id="Seg_635" s="T32">v</ta>
            <ta e="T34" id="Seg_636" s="T33">pers</ta>
            <ta e="T35" id="Seg_637" s="T34">pers</ta>
            <ta e="T36" id="Seg_638" s="T35">v</ta>
            <ta e="T37" id="Seg_639" s="T36">v</ta>
            <ta e="T38" id="Seg_640" s="T37">n</ta>
            <ta e="T39" id="Seg_641" s="T38">pers</ta>
            <ta e="T40" id="Seg_642" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_643" s="T40">v</ta>
            <ta e="T42" id="Seg_644" s="T41">v</ta>
            <ta e="T43" id="Seg_645" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_646" s="T43">v</ta>
            <ta e="T45" id="Seg_647" s="T44">pers</ta>
            <ta e="T46" id="Seg_648" s="T45">pers</ta>
            <ta e="T47" id="Seg_649" s="T46">adv</ta>
            <ta e="T48" id="Seg_650" s="T47">v</ta>
            <ta e="T49" id="Seg_651" s="T48">n</ta>
            <ta e="T50" id="Seg_652" s="T49">pers</ta>
            <ta e="T51" id="Seg_653" s="T50">n</ta>
            <ta e="T52" id="Seg_654" s="T51">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_655" s="T1">pro.h:A</ta>
            <ta e="T3" id="Seg_656" s="T2">np.h:Th 0.1.h:Poss</ta>
            <ta e="T6" id="Seg_657" s="T5">pro.h:A</ta>
            <ta e="T8" id="Seg_658" s="T7">np.h:Th</ta>
            <ta e="T10" id="Seg_659" s="T9">0.1.h:E</ta>
            <ta e="T11" id="Seg_660" s="T10">v:Th</ta>
            <ta e="T12" id="Seg_661" s="T11">pro.h:A</ta>
            <ta e="T14" id="Seg_662" s="T13">np.h:Th</ta>
            <ta e="T16" id="Seg_663" s="T15">np.h:A 0.1.h:Poss</ta>
            <ta e="T17" id="Seg_664" s="T16">np:Com</ta>
            <ta e="T19" id="Seg_665" s="T18">v:Th</ta>
            <ta e="T21" id="Seg_666" s="T20">0.3.h:E</ta>
            <ta e="T24" id="Seg_667" s="T23">np:Th 0.3.h:Poss</ta>
            <ta e="T26" id="Seg_668" s="T25">np.h:P</ta>
            <ta e="T27" id="Seg_669" s="T26">np:L</ta>
            <ta e="T29" id="Seg_670" s="T28">0.3.h:A</ta>
            <ta e="T31" id="Seg_671" s="T30">0.3.h:A</ta>
            <ta e="T33" id="Seg_672" s="T32">0.3.h:A</ta>
            <ta e="T34" id="Seg_673" s="T33">pro.h:E</ta>
            <ta e="T35" id="Seg_674" s="T34">pro.h:P</ta>
            <ta e="T37" id="Seg_675" s="T36">v:Th</ta>
            <ta e="T38" id="Seg_676" s="T37">np.h:A</ta>
            <ta e="T41" id="Seg_677" s="T40">0.3:Th</ta>
            <ta e="T42" id="Seg_678" s="T41">0.3.h:A</ta>
            <ta e="T44" id="Seg_679" s="T43">0.2.h:A 0.3.h:P</ta>
            <ta e="T45" id="Seg_680" s="T44">pro.h:A</ta>
            <ta e="T46" id="Seg_681" s="T45">pro.h:Th</ta>
            <ta e="T49" id="Seg_682" s="T48">np:L</ta>
            <ta e="T50" id="Seg_683" s="T49">pro.h:A</ta>
            <ta e="T51" id="Seg_684" s="T50">np:Th 0.3.h:Poss</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_685" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_686" s="T2">np.h:O</ta>
            <ta e="T5" id="Seg_687" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_688" s="T5">pro.h:S</ta>
            <ta e="T7" id="Seg_689" s="T6">v:pred</ta>
            <ta e="T8" id="Seg_690" s="T7">np.h:O</ta>
            <ta e="T10" id="Seg_691" s="T9">0.1.h:S v:pred</ta>
            <ta e="T11" id="Seg_692" s="T10">v:O</ta>
            <ta e="T12" id="Seg_693" s="T11">pro.h:S</ta>
            <ta e="T13" id="Seg_694" s="T12">v:pred</ta>
            <ta e="T14" id="Seg_695" s="T13">np.h:O</ta>
            <ta e="T16" id="Seg_696" s="T15">np.h:S</ta>
            <ta e="T18" id="Seg_697" s="T17">v:pred</ta>
            <ta e="T19" id="Seg_698" s="T18">v:O</ta>
            <ta e="T21" id="Seg_699" s="T20">0.3.h:S v:pred</ta>
            <ta e="T24" id="Seg_700" s="T23">np:S</ta>
            <ta e="T25" id="Seg_701" s="T24">v:pred</ta>
            <ta e="T26" id="Seg_702" s="T25">np.h:O</ta>
            <ta e="T29" id="Seg_703" s="T28">0.3.h:S v:pred</ta>
            <ta e="T31" id="Seg_704" s="T30">0.3.h:S v:pred</ta>
            <ta e="T33" id="Seg_705" s="T32">0.3.h:S v:pred</ta>
            <ta e="T34" id="Seg_706" s="T33">pro.h:S</ta>
            <ta e="T36" id="Seg_707" s="T35">v:pred</ta>
            <ta e="T37" id="Seg_708" s="T36">v:O</ta>
            <ta e="T38" id="Seg_709" s="T37">np.h:S</ta>
            <ta e="T41" id="Seg_710" s="T40">v:pred 0.3:O</ta>
            <ta e="T42" id="Seg_711" s="T41">0.3.h:S v:pred</ta>
            <ta e="T44" id="Seg_712" s="T43">0.2.h:S v:pred 0.3.h:O</ta>
            <ta e="T45" id="Seg_713" s="T44">pro.h:S</ta>
            <ta e="T46" id="Seg_714" s="T45">pro.h:O</ta>
            <ta e="T48" id="Seg_715" s="T47">v:pred</ta>
            <ta e="T50" id="Seg_716" s="T49">pro.h:S</ta>
            <ta e="T51" id="Seg_717" s="T50">np:O</ta>
            <ta e="T52" id="Seg_718" s="T51">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_719" s="T3">RUS:core</ta>
            <ta e="T28" id="Seg_720" s="T27">RUS:disc</ta>
            <ta e="T30" id="Seg_721" s="T29">RUS:gram</ta>
            <ta e="T32" id="Seg_722" s="T31">RUS:gram</ta>
            <ta e="T47" id="Seg_723" s="T46">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_724" s="T1">I christened all my children.</ta>
            <ta e="T8" id="Seg_725" s="T5">And you should christen Svetlana.</ta>
            <ta e="T11" id="Seg_726" s="T8">“I don't want to christen.”</ta>
            <ta e="T15" id="Seg_727" s="T11">I christened my kids, when they were grown-up.</ta>
            <ta e="T21" id="Seg_728" s="T15">My Vanja was fighting with the priest, he didn't want to be christened.</ta>
            <ta e="T25" id="Seg_729" s="T21">He was ten years old.</ta>
            <ta e="T33" id="Seg_730" s="T25">He caught the priest by his beard and pulled, and pulled.</ta>
            <ta e="T37" id="Seg_731" s="T33">I wanted to whip him.</ta>
            <ta e="T41" id="Seg_732" s="T37">The priest didn't allow me.</ta>
            <ta e="T44" id="Seg_733" s="T41">He said: “Don't whip.”</ta>
            <ta e="T49" id="Seg_734" s="T44">I could hardly take the beard from him.</ta>
            <ta e="T52" id="Seg_735" s="T49">He was holding the beard [tight].</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_736" s="T1">Ich habe all meine Kinder getauft.</ta>
            <ta e="T8" id="Seg_737" s="T5">Du solltest Svetlana taufen.</ta>
            <ta e="T11" id="Seg_738" s="T8">"Ich will nicht taufen."</ta>
            <ta e="T15" id="Seg_739" s="T11">Ich habe meine Kinder getauft, als sie größer waren.</ta>
            <ta e="T21" id="Seg_740" s="T15">Mein Vanja stritt mit dem Priester, er wollte nicht getauft werden.</ta>
            <ta e="T25" id="Seg_741" s="T21">Er war zehn Jahre alt.</ta>
            <ta e="T33" id="Seg_742" s="T25">Er packte den Priester am Bart und zog und zog.</ta>
            <ta e="T37" id="Seg_743" s="T33">Ich wollte ihn schlagen.</ta>
            <ta e="T41" id="Seg_744" s="T37">Der Priester erlaubte es mir nicht.</ta>
            <ta e="T44" id="Seg_745" s="T41">Er sagte: "Schlag [ihn] nicht."</ta>
            <ta e="T49" id="Seg_746" s="T44">Ich konnte ihm kaum den Bart wegnehmen.</ta>
            <ta e="T52" id="Seg_747" s="T49">Er hielt den Bart [fest].</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_748" s="T1">Я крестила всех своих детей.</ta>
            <ta e="T8" id="Seg_749" s="T5">И ты крести Светлану.</ta>
            <ta e="T11" id="Seg_750" s="T8">“Я не хочу крестить”.</ta>
            <ta e="T15" id="Seg_751" s="T11">Я крестила ребятишек большими.</ta>
            <ta e="T21" id="Seg_752" s="T15">Мой Ваня с попом дрался, крестить(ся) не хотел.</ta>
            <ta e="T25" id="Seg_753" s="T21">Ему десять лет было.</ta>
            <ta e="T33" id="Seg_754" s="T25">Попа за бороду как поймал (схватил), и тянет, и тянет.</ta>
            <ta e="T37" id="Seg_755" s="T33">Я его хотела отстегать.</ta>
            <ta e="T41" id="Seg_756" s="T37">Поп мне не дает.</ta>
            <ta e="T44" id="Seg_757" s="T41">Говорит: “Не стегай”.</ta>
            <ta e="T49" id="Seg_758" s="T44">Я у него насилу взяла бороду.</ta>
            <ta e="T52" id="Seg_759" s="T49">Он бороду держит.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_760" s="T1">я крестила всех своих детей</ta>
            <ta e="T8" id="Seg_761" s="T5">и ты крести Светлану</ta>
            <ta e="T11" id="Seg_762" s="T8">не хочу крестить</ta>
            <ta e="T15" id="Seg_763" s="T11">я крестила ребятишек они были большие</ta>
            <ta e="T21" id="Seg_764" s="T15">Ваня с попом дрался креститься не хотел</ta>
            <ta e="T25" id="Seg_765" s="T21">ему было десять лет</ta>
            <ta e="T33" id="Seg_766" s="T25">попа за бороду как поймал (схватил) и тянет и тянет</ta>
            <ta e="T37" id="Seg_767" s="T33">я его хотела выстегать</ta>
            <ta e="T41" id="Seg_768" s="T37">поп мне не дает</ta>
            <ta e="T44" id="Seg_769" s="T41">говорит не стегай</ta>
            <ta e="T49" id="Seg_770" s="T44">Я у него насилу взяла бороду</ta>
            <ta e="T52" id="Seg_771" s="T49">он бороду держит</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T33" id="Seg_772" s="T25">[BrM:] Tentative analysis of 'meʒaut'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
