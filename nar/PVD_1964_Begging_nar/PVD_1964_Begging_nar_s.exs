<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Begging_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Begging_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">34</ud-information>
            <ud-information attribute-name="# HIAT:w">26</ud-information>
            <ud-information attribute-name="# e">26</ud-information>
            <ud-information attribute-name="# HIAT:u">7</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T27" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Awgɨdɨ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">qumdɨ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">awan</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">jen</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Man</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">qoǯirsau</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">augɨdɨ</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">qumɨm</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_32" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">Qusaj</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">maderlʼe</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">qula</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">palʼtʼikuzat</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_47" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">Tebla</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">madurguzattə</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">milosʼtʼinkam</ts>
                  <nts id="Seg_56" n="HIAT:ip">,</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">nʼaim</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">i</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">qomdäm</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_69" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">Man</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">as</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_77" n="HIAT:w" s="T21">mikuzau</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_81" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">Pusʼ</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">uːdʼijemt</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_90" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_92" n="HIAT:w" s="T24">Man</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">qɨdan</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">udʼikan</ts>
                  <nts id="Seg_99" n="HIAT:ip">.</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T27" id="Seg_101" n="sc" s="T1">
               <ts e="T2" id="Seg_103" n="e" s="T1">Awgɨdɨ </ts>
               <ts e="T3" id="Seg_105" n="e" s="T2">qumdɨ </ts>
               <ts e="T4" id="Seg_107" n="e" s="T3">awan </ts>
               <ts e="T5" id="Seg_109" n="e" s="T4">jen. </ts>
               <ts e="T6" id="Seg_111" n="e" s="T5">Man </ts>
               <ts e="T7" id="Seg_113" n="e" s="T6">qoǯirsau </ts>
               <ts e="T8" id="Seg_115" n="e" s="T7">augɨdɨ </ts>
               <ts e="T9" id="Seg_117" n="e" s="T8">qumɨm. </ts>
               <ts e="T10" id="Seg_119" n="e" s="T9">Qusaj </ts>
               <ts e="T11" id="Seg_121" n="e" s="T10">maderlʼe </ts>
               <ts e="T12" id="Seg_123" n="e" s="T11">qula </ts>
               <ts e="T13" id="Seg_125" n="e" s="T12">palʼtʼikuzat. </ts>
               <ts e="T14" id="Seg_127" n="e" s="T13">Tebla </ts>
               <ts e="T15" id="Seg_129" n="e" s="T14">madurguzattə </ts>
               <ts e="T16" id="Seg_131" n="e" s="T15">milosʼtʼinkam, </ts>
               <ts e="T17" id="Seg_133" n="e" s="T16">nʼaim </ts>
               <ts e="T18" id="Seg_135" n="e" s="T17">i </ts>
               <ts e="T19" id="Seg_137" n="e" s="T18">qomdäm. </ts>
               <ts e="T20" id="Seg_139" n="e" s="T19">Man </ts>
               <ts e="T21" id="Seg_141" n="e" s="T20">as </ts>
               <ts e="T22" id="Seg_143" n="e" s="T21">mikuzau. </ts>
               <ts e="T23" id="Seg_145" n="e" s="T22">Pusʼ </ts>
               <ts e="T24" id="Seg_147" n="e" s="T23">uːdʼijemt. </ts>
               <ts e="T25" id="Seg_149" n="e" s="T24">Man </ts>
               <ts e="T26" id="Seg_151" n="e" s="T25">qɨdan </ts>
               <ts e="T27" id="Seg_153" n="e" s="T26">udʼikan. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_154" s="T1">PVD_1964_Begging_nar.001 (001.001)</ta>
            <ta e="T9" id="Seg_155" s="T5">PVD_1964_Begging_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_156" s="T9">PVD_1964_Begging_nar.003 (001.003)</ta>
            <ta e="T19" id="Seg_157" s="T13">PVD_1964_Begging_nar.004 (001.004)</ta>
            <ta e="T22" id="Seg_158" s="T19">PVD_1964_Begging_nar.005 (001.005)</ta>
            <ta e="T24" id="Seg_159" s="T22">PVD_1964_Begging_nar.006 (001.006)</ta>
            <ta e="T27" id="Seg_160" s="T24">PVD_1964_Begging_nar.007 (001.007)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_161" s="T1">′аwгы′ды kум′ды ′аwан jен.</ta>
            <ta e="T9" id="Seg_162" s="T5">ман kоджир′сау̹ ‵ау̹гы′ды kу′мым.</ta>
            <ta e="T13" id="Seg_163" s="T9">kу′сай ′мадерлʼе kу′ла палʼтʼику′зат.</ta>
            <ta e="T19" id="Seg_164" s="T13">те̨б′ла ′мадургу′заттъ ′милосʼтʼинкам, ′нʼаим и kом′дӓм.</ta>
            <ta e="T22" id="Seg_165" s="T19">ман ′ас ′микузау̹.</ta>
            <ta e="T24" id="Seg_166" s="T22">′пусʼ ′ӯдʼиjемт.</ta>
            <ta e="T27" id="Seg_167" s="T24">ман ′kыдан ′удʼикан.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_168" s="T1">awgɨdɨ qumdɨ awan jen.</ta>
            <ta e="T9" id="Seg_169" s="T5">man qoǯirsau̹ au̹gɨdɨ qumɨm.</ta>
            <ta e="T13" id="Seg_170" s="T9">qusaj maderlʼe qula palʼtʼikuzat.</ta>
            <ta e="T19" id="Seg_171" s="T13">tebla madurguzattə milosʼtʼinkam, nʼaim i qomdäm.</ta>
            <ta e="T22" id="Seg_172" s="T19">man as mikuzau̹.</ta>
            <ta e="T24" id="Seg_173" s="T22">pusʼ uːdʼijemt.</ta>
            <ta e="T27" id="Seg_174" s="T24">man qɨdan udʼikan.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_175" s="T1">Awgɨdɨ qumdɨ awan jen. </ta>
            <ta e="T9" id="Seg_176" s="T5">Man qoǯirsau augɨdɨ qumɨm. </ta>
            <ta e="T13" id="Seg_177" s="T9">Qusaj maderlʼe qula palʼtʼikuzat. </ta>
            <ta e="T19" id="Seg_178" s="T13">Tebla madurguzattə milosʼtʼinkam, nʼaim i qomdäm. </ta>
            <ta e="T22" id="Seg_179" s="T19">Man as mikuzau. </ta>
            <ta e="T24" id="Seg_180" s="T22">Pusʼ uːdʼijemt. </ta>
            <ta e="T27" id="Seg_181" s="T24">Man qɨdan udʼikan. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_182" s="T1">aw-gɨdɨ</ta>
            <ta e="T3" id="Seg_183" s="T2">qum-dɨ</ta>
            <ta e="T4" id="Seg_184" s="T3">awa-n</ta>
            <ta e="T5" id="Seg_185" s="T4">je-n</ta>
            <ta e="T6" id="Seg_186" s="T5">man</ta>
            <ta e="T7" id="Seg_187" s="T6">qo-ǯir-sa-u</ta>
            <ta e="T8" id="Seg_188" s="T7">au-gɨdɨ</ta>
            <ta e="T9" id="Seg_189" s="T8">qum-ɨ-m</ta>
            <ta e="T10" id="Seg_190" s="T9">qusaj</ta>
            <ta e="T11" id="Seg_191" s="T10">mader-lʼe</ta>
            <ta e="T12" id="Seg_192" s="T11">qu-la</ta>
            <ta e="T13" id="Seg_193" s="T12">palʼtʼi-ku-za-t</ta>
            <ta e="T14" id="Seg_194" s="T13">teb-la</ta>
            <ta e="T15" id="Seg_195" s="T14">madur-gu-za-ttə</ta>
            <ta e="T16" id="Seg_196" s="T15">milosʼtʼin-ka-m</ta>
            <ta e="T17" id="Seg_197" s="T16">nʼai-m</ta>
            <ta e="T18" id="Seg_198" s="T17">i</ta>
            <ta e="T19" id="Seg_199" s="T18">qomdä-m</ta>
            <ta e="T20" id="Seg_200" s="T19">man</ta>
            <ta e="T21" id="Seg_201" s="T20">as</ta>
            <ta e="T22" id="Seg_202" s="T21">mi-ku-za-u</ta>
            <ta e="T23" id="Seg_203" s="T22">pusʼ</ta>
            <ta e="T24" id="Seg_204" s="T23">uːdʼi-je-mt</ta>
            <ta e="T25" id="Seg_205" s="T24">man</ta>
            <ta e="T26" id="Seg_206" s="T25">qɨdan</ta>
            <ta e="T27" id="Seg_207" s="T26">udʼi-ka-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_208" s="T1">ab-gɨdi</ta>
            <ta e="T3" id="Seg_209" s="T2">qum-ntə</ta>
            <ta e="T4" id="Seg_210" s="T3">awa-ŋ</ta>
            <ta e="T5" id="Seg_211" s="T4">eː-n</ta>
            <ta e="T6" id="Seg_212" s="T5">man</ta>
            <ta e="T7" id="Seg_213" s="T6">qo-nǯir-sɨ-w</ta>
            <ta e="T8" id="Seg_214" s="T7">ab-kattə</ta>
            <ta e="T9" id="Seg_215" s="T8">qum-ɨ-m</ta>
            <ta e="T10" id="Seg_216" s="T9">qusaj</ta>
            <ta e="T11" id="Seg_217" s="T10">madər-le</ta>
            <ta e="T12" id="Seg_218" s="T11">qum-la</ta>
            <ta e="T13" id="Seg_219" s="T12">palʼdʼi-ku-sɨ-tɨn</ta>
            <ta e="T14" id="Seg_220" s="T13">täp-la</ta>
            <ta e="T15" id="Seg_221" s="T14">madər-ku-sɨ-tɨn</ta>
            <ta e="T16" id="Seg_222" s="T15">milosʼtʼin-ka-m</ta>
            <ta e="T17" id="Seg_223" s="T16">nʼäj-m</ta>
            <ta e="T18" id="Seg_224" s="T17">i</ta>
            <ta e="T19" id="Seg_225" s="T18">qomdɛ-m</ta>
            <ta e="T20" id="Seg_226" s="T19">man</ta>
            <ta e="T21" id="Seg_227" s="T20">asa</ta>
            <ta e="T22" id="Seg_228" s="T21">me-ku-sɨ-w</ta>
            <ta e="T23" id="Seg_229" s="T22">pusʼtʼ</ta>
            <ta e="T24" id="Seg_230" s="T23">uːdʼi-je-mt</ta>
            <ta e="T25" id="Seg_231" s="T24">man</ta>
            <ta e="T26" id="Seg_232" s="T25">qɨdan</ta>
            <ta e="T27" id="Seg_233" s="T26">uːdʼi-ku-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_234" s="T1">food-CAR</ta>
            <ta e="T3" id="Seg_235" s="T2">human.being-ILL</ta>
            <ta e="T4" id="Seg_236" s="T3">bad-ADVZ</ta>
            <ta e="T5" id="Seg_237" s="T4">be-3SG.S</ta>
            <ta e="T6" id="Seg_238" s="T5">I.NOM</ta>
            <ta e="T7" id="Seg_239" s="T6">see-DRV-PST-1SG.O</ta>
            <ta e="T8" id="Seg_240" s="T7">food-CAR</ta>
            <ta e="T9" id="Seg_241" s="T8">human.being-EP-ACC</ta>
            <ta e="T10" id="Seg_242" s="T9">how.many</ta>
            <ta e="T11" id="Seg_243" s="T10">ask.for-CVB</ta>
            <ta e="T12" id="Seg_244" s="T11">human.being-PL.[NOM]</ta>
            <ta e="T13" id="Seg_245" s="T12">walk-HAB-PST-3PL</ta>
            <ta e="T14" id="Seg_246" s="T13">(s)he-PL.[NOM]</ta>
            <ta e="T15" id="Seg_247" s="T14">ask.for-HAB-PST-3PL</ta>
            <ta e="T16" id="Seg_248" s="T15">alms-DIM-ACC</ta>
            <ta e="T17" id="Seg_249" s="T16">bread-ACC</ta>
            <ta e="T18" id="Seg_250" s="T17">and</ta>
            <ta e="T19" id="Seg_251" s="T18">money-ACC</ta>
            <ta e="T20" id="Seg_252" s="T19">I.NOM</ta>
            <ta e="T21" id="Seg_253" s="T20">NEG</ta>
            <ta e="T22" id="Seg_254" s="T21">give-HAB-PST-1SG.O</ta>
            <ta e="T23" id="Seg_255" s="T22">JUSS</ta>
            <ta e="T24" id="Seg_256" s="T23">work-IMP-%%3PL</ta>
            <ta e="T25" id="Seg_257" s="T24">I.NOM</ta>
            <ta e="T26" id="Seg_258" s="T25">all.the.time</ta>
            <ta e="T27" id="Seg_259" s="T26">work-HAB-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_260" s="T1">еда-CAR</ta>
            <ta e="T3" id="Seg_261" s="T2">человек-ILL</ta>
            <ta e="T4" id="Seg_262" s="T3">плохой-ADVZ</ta>
            <ta e="T5" id="Seg_263" s="T4">быть-3SG.S</ta>
            <ta e="T6" id="Seg_264" s="T5">я.NOM</ta>
            <ta e="T7" id="Seg_265" s="T6">увидеть-DRV-PST-1SG.O</ta>
            <ta e="T8" id="Seg_266" s="T7">еда-CAR</ta>
            <ta e="T9" id="Seg_267" s="T8">человек-EP-ACC</ta>
            <ta e="T10" id="Seg_268" s="T9">сколько</ta>
            <ta e="T11" id="Seg_269" s="T10">попросить-CVB</ta>
            <ta e="T12" id="Seg_270" s="T11">человек-PL.[NOM]</ta>
            <ta e="T13" id="Seg_271" s="T12">ходить-HAB-PST-3PL</ta>
            <ta e="T14" id="Seg_272" s="T13">он(а)-PL.[NOM]</ta>
            <ta e="T15" id="Seg_273" s="T14">попросить-HAB-PST-3PL</ta>
            <ta e="T16" id="Seg_274" s="T15">милостыня-DIM-ACC</ta>
            <ta e="T17" id="Seg_275" s="T16">хлеб-ACC</ta>
            <ta e="T18" id="Seg_276" s="T17">и</ta>
            <ta e="T19" id="Seg_277" s="T18">деньги-ACC</ta>
            <ta e="T20" id="Seg_278" s="T19">я.NOM</ta>
            <ta e="T21" id="Seg_279" s="T20">NEG</ta>
            <ta e="T22" id="Seg_280" s="T21">дать-HAB-PST-1SG.O</ta>
            <ta e="T23" id="Seg_281" s="T22">JUSS</ta>
            <ta e="T24" id="Seg_282" s="T23">работать-IMP-%%3PL</ta>
            <ta e="T25" id="Seg_283" s="T24">я.NOM</ta>
            <ta e="T26" id="Seg_284" s="T25">все.время</ta>
            <ta e="T27" id="Seg_285" s="T26">работать-HAB-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_286" s="T1">n-n&gt;adj</ta>
            <ta e="T3" id="Seg_287" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_288" s="T3">adj-adj&gt;adv</ta>
            <ta e="T5" id="Seg_289" s="T4">v-v:pn</ta>
            <ta e="T6" id="Seg_290" s="T5">pers</ta>
            <ta e="T7" id="Seg_291" s="T6">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_292" s="T7">n-n&gt;n</ta>
            <ta e="T9" id="Seg_293" s="T8">n-n:ins-n:case</ta>
            <ta e="T10" id="Seg_294" s="T9">interrog</ta>
            <ta e="T11" id="Seg_295" s="T10">v-v&gt;adv</ta>
            <ta e="T12" id="Seg_296" s="T11">n-n:num.[n:case]</ta>
            <ta e="T13" id="Seg_297" s="T12">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_298" s="T13">pers-n:num.[n:case]</ta>
            <ta e="T15" id="Seg_299" s="T14">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_300" s="T15">n-n&gt;n-n:case</ta>
            <ta e="T17" id="Seg_301" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_302" s="T17">conj</ta>
            <ta e="T19" id="Seg_303" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_304" s="T19">pers</ta>
            <ta e="T21" id="Seg_305" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_306" s="T21">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_307" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_308" s="T23">v-v:mood-v:pn</ta>
            <ta e="T25" id="Seg_309" s="T24">pers</ta>
            <ta e="T26" id="Seg_310" s="T25">adv</ta>
            <ta e="T27" id="Seg_311" s="T26">v-v&gt;v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_312" s="T1">adj</ta>
            <ta e="T3" id="Seg_313" s="T2">n</ta>
            <ta e="T4" id="Seg_314" s="T3">adv</ta>
            <ta e="T5" id="Seg_315" s="T4">v</ta>
            <ta e="T6" id="Seg_316" s="T5">pers</ta>
            <ta e="T7" id="Seg_317" s="T6">v</ta>
            <ta e="T8" id="Seg_318" s="T7">n</ta>
            <ta e="T9" id="Seg_319" s="T8">n</ta>
            <ta e="T10" id="Seg_320" s="T9">interrog</ta>
            <ta e="T11" id="Seg_321" s="T10">adv</ta>
            <ta e="T12" id="Seg_322" s="T11">n</ta>
            <ta e="T13" id="Seg_323" s="T12">v</ta>
            <ta e="T14" id="Seg_324" s="T13">pers</ta>
            <ta e="T15" id="Seg_325" s="T14">v</ta>
            <ta e="T16" id="Seg_326" s="T15">n</ta>
            <ta e="T17" id="Seg_327" s="T16">n</ta>
            <ta e="T18" id="Seg_328" s="T17">conj</ta>
            <ta e="T19" id="Seg_329" s="T18">n</ta>
            <ta e="T20" id="Seg_330" s="T19">pers</ta>
            <ta e="T21" id="Seg_331" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_332" s="T21">v</ta>
            <ta e="T23" id="Seg_333" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_334" s="T23">v</ta>
            <ta e="T25" id="Seg_335" s="T24">pers</ta>
            <ta e="T26" id="Seg_336" s="T25">adv</ta>
            <ta e="T27" id="Seg_337" s="T26">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T5" id="Seg_338" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_339" s="T5">pro.h:S</ta>
            <ta e="T7" id="Seg_340" s="T6">v:pred</ta>
            <ta e="T9" id="Seg_341" s="T8">np.h:O</ta>
            <ta e="T11" id="Seg_342" s="T10">s:adv</ta>
            <ta e="T12" id="Seg_343" s="T11">np.h:S</ta>
            <ta e="T13" id="Seg_344" s="T12">v:pred</ta>
            <ta e="T14" id="Seg_345" s="T13">pro.h:S</ta>
            <ta e="T15" id="Seg_346" s="T14">v:pred</ta>
            <ta e="T16" id="Seg_347" s="T15">np:O</ta>
            <ta e="T17" id="Seg_348" s="T16">np:O</ta>
            <ta e="T19" id="Seg_349" s="T18">np:O</ta>
            <ta e="T20" id="Seg_350" s="T19">pro.h:S</ta>
            <ta e="T22" id="Seg_351" s="T21">v:pred</ta>
            <ta e="T24" id="Seg_352" s="T23">0.3.h:S v:pred</ta>
            <ta e="T25" id="Seg_353" s="T24">pro.h:S</ta>
            <ta e="T27" id="Seg_354" s="T26">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_355" s="T2">np.h:E</ta>
            <ta e="T6" id="Seg_356" s="T5">pro.h:E</ta>
            <ta e="T9" id="Seg_357" s="T8">np.h:Th</ta>
            <ta e="T12" id="Seg_358" s="T11">np.h:A</ta>
            <ta e="T14" id="Seg_359" s="T13">pro.h:A</ta>
            <ta e="T16" id="Seg_360" s="T15">np:Th</ta>
            <ta e="T17" id="Seg_361" s="T16">np:Th</ta>
            <ta e="T19" id="Seg_362" s="T18">np:Th</ta>
            <ta e="T20" id="Seg_363" s="T19">pro.h:A</ta>
            <ta e="T24" id="Seg_364" s="T23">0.3.h:A</ta>
            <ta e="T25" id="Seg_365" s="T24">pro.h:A</ta>
            <ta e="T26" id="Seg_366" s="T25">adv:Time</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T16" id="Seg_367" s="T15">RUS:cult</ta>
            <ta e="T18" id="Seg_368" s="T17">RUS:gram</ta>
            <ta e="T23" id="Seg_369" s="T22">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T16" id="Seg_370" s="T15">finVdel Csub</ta>
            <ta e="T23" id="Seg_371" s="T22">finCdel</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T16" id="Seg_372" s="T15">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_373" s="T1">A hungry man feels bad.</ta>
            <ta e="T9" id="Seg_374" s="T5">I saw a hungry man.</ta>
            <ta e="T13" id="Seg_375" s="T9">Many people were walking and begging.</ta>
            <ta e="T19" id="Seg_376" s="T13">They were begging, asking for bread and money.</ta>
            <ta e="T22" id="Seg_377" s="T19">I didn't give [them].</ta>
            <ta e="T24" id="Seg_378" s="T22">They should work.</ta>
            <ta e="T27" id="Seg_379" s="T24">I worked always.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_380" s="T1">Ein hungriger Mann fühlt sich schlecht.</ta>
            <ta e="T9" id="Seg_381" s="T5">Ich sah einen hungrigen Mann.</ta>
            <ta e="T13" id="Seg_382" s="T9">Viele Menschen liefen und bettelten.</ta>
            <ta e="T19" id="Seg_383" s="T13">Sie bettelten um Brot und Geld.</ta>
            <ta e="T22" id="Seg_384" s="T19">Ich gab [ihnen] nichts.</ta>
            <ta e="T24" id="Seg_385" s="T22">Sie sollten arbeiten.</ta>
            <ta e="T27" id="Seg_386" s="T24">Ich habe immer gearbeitet.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_387" s="T1">Голодному человеку плохо.</ta>
            <ta e="T9" id="Seg_388" s="T5">Я видела голодного человека.</ta>
            <ta e="T13" id="Seg_389" s="T9">Люди сколько, прося, ходили.</ta>
            <ta e="T19" id="Seg_390" s="T13">Они просили милостыню, хлеб и денег.</ta>
            <ta e="T22" id="Seg_391" s="T19">Я не давала.</ta>
            <ta e="T24" id="Seg_392" s="T22">Пусть работают.</ta>
            <ta e="T27" id="Seg_393" s="T24">Я всегда работала.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_394" s="T1">голодному человеку плохо</ta>
            <ta e="T9" id="Seg_395" s="T5">я видела голодного человека</ta>
            <ta e="T13" id="Seg_396" s="T9">люди сколько по миру ходили</ta>
            <ta e="T19" id="Seg_397" s="T13">они просили (выпрашивали) милостыню хлеб и денег</ta>
            <ta e="T22" id="Seg_398" s="T19">я не давала</ta>
            <ta e="T24" id="Seg_399" s="T22">пусть работают</ta>
            <ta e="T27" id="Seg_400" s="T24">я всегда работала</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
