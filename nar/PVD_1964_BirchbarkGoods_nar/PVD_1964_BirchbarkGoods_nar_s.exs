<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_BirchbarkGoods_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_BirchbarkGoods_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">13</ud-information>
            <ud-information attribute-name="# HIAT:w">9</ud-information>
            <ud-information attribute-name="# e">9</ud-information>
            <ud-information attribute-name="# HIAT:u">3</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T10" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Qɨba</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">mugola</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_11" n="HIAT:w" s="T3">tebɨm</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_14" n="HIAT:w" s="T4">padʼälqwattɨ</ts>
                  <nts id="Seg_15" n="HIAT:ip">.</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_18" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">Tebnando</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">kemdə</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">mʼekudattə</ts>
                  <nts id="Seg_27" n="HIAT:ip">.</nts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_30" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">Korʒela</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">sütkudattə</ts>
                  <nts id="Seg_36" n="HIAT:ip">.</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T10" id="Seg_38" n="sc" s="T1">
               <ts e="T2" id="Seg_40" n="e" s="T1">Qɨba </ts>
               <ts e="T3" id="Seg_42" n="e" s="T2">mugola, </ts>
               <ts e="T4" id="Seg_44" n="e" s="T3">tebɨm </ts>
               <ts e="T5" id="Seg_46" n="e" s="T4">padʼälqwattɨ. </ts>
               <ts e="T6" id="Seg_48" n="e" s="T5">Tebnando </ts>
               <ts e="T7" id="Seg_50" n="e" s="T6">kemdə </ts>
               <ts e="T8" id="Seg_52" n="e" s="T7">mʼekudattə. </ts>
               <ts e="T9" id="Seg_54" n="e" s="T8">Korʒela </ts>
               <ts e="T10" id="Seg_56" n="e" s="T9">sütkudattə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_57" s="T1">PVD_1964_BirchbarkGoods_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_58" s="T5">PVD_1964_BirchbarkGoods_nar.002 (001.002)</ta>
            <ta e="T10" id="Seg_59" s="T8">PVD_1964_BirchbarkGoods_nar.003 (001.003)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_60" s="T1">′kыба му′гола, ′те̨бым па′дʼӓлkwатты.</ta>
            <ta e="T8" id="Seg_61" s="T5">те̨б′нандо ′кемдъ мʼекудаттъ.</ta>
            <ta e="T10" id="Seg_62" s="T8">кор′же̨ла ′сӱтку‵даттъ.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_63" s="T1">qɨba mugola, tebɨm padʼälqwattɨ.</ta>
            <ta e="T8" id="Seg_64" s="T5">tebnando kemdə mʼekudattə.</ta>
            <ta e="T10" id="Seg_65" s="T8">korʒela sütkudattə.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_66" s="T1">Qɨba mugola, tebɨm padʼälqwattɨ. </ta>
            <ta e="T8" id="Seg_67" s="T5">Tebnando kemdə mʼekudattə. </ta>
            <ta e="T10" id="Seg_68" s="T8">Korʒela sütkudattə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_69" s="T1">qɨba</ta>
            <ta e="T3" id="Seg_70" s="T2">mugo-la</ta>
            <ta e="T4" id="Seg_71" s="T3">teb-ɨ-m</ta>
            <ta e="T5" id="Seg_72" s="T4">padʼäl-qwa-ttɨ</ta>
            <ta e="T6" id="Seg_73" s="T5">teb-nando</ta>
            <ta e="T7" id="Seg_74" s="T6">kemdə</ta>
            <ta e="T8" id="Seg_75" s="T7">mʼe-ku-da-ttə</ta>
            <ta e="T9" id="Seg_76" s="T8">korʒe-la</ta>
            <ta e="T10" id="Seg_77" s="T9">süt-ku-da-ttə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_78" s="T1">qɨba</ta>
            <ta e="T3" id="Seg_79" s="T2">mugo-la</ta>
            <ta e="T4" id="Seg_80" s="T3">täp-ɨ-m</ta>
            <ta e="T5" id="Seg_81" s="T4">padʼal-ku-tɨn</ta>
            <ta e="T6" id="Seg_82" s="T5">täp-nanto</ta>
            <ta e="T7" id="Seg_83" s="T6">kemdə</ta>
            <ta e="T8" id="Seg_84" s="T7">meː-ku-ntɨ-tɨn</ta>
            <ta e="T9" id="Seg_85" s="T8">qorʒe-la</ta>
            <ta e="T10" id="Seg_86" s="T9">süt-ku-ntɨ-tɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_87" s="T1">small</ta>
            <ta e="T3" id="Seg_88" s="T2">bird.cherry-PL.[NOM]</ta>
            <ta e="T4" id="Seg_89" s="T3">(s)he-EP-ACC</ta>
            <ta e="T5" id="Seg_90" s="T4">chop.off-HAB-3PL</ta>
            <ta e="T6" id="Seg_91" s="T5">(s)he-ABL</ta>
            <ta e="T7" id="Seg_92" s="T6">twig.from.bird.cherry.[NOM]</ta>
            <ta e="T8" id="Seg_93" s="T7">do-HAB-INFER-3PL</ta>
            <ta e="T9" id="Seg_94" s="T8">basket-PL.[NOM]</ta>
            <ta e="T10" id="Seg_95" s="T9">sew-HAB-INFER-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_96" s="T1">маленький</ta>
            <ta e="T3" id="Seg_97" s="T2">черемуха-PL.[NOM]</ta>
            <ta e="T4" id="Seg_98" s="T3">он(а)-EP-ACC</ta>
            <ta e="T5" id="Seg_99" s="T4">срубить-HAB-3PL</ta>
            <ta e="T6" id="Seg_100" s="T5">он(а)-ABL</ta>
            <ta e="T7" id="Seg_101" s="T6">черемуховый.прут.[NOM]</ta>
            <ta e="T8" id="Seg_102" s="T7">сделать-HAB-INFER-3PL</ta>
            <ta e="T9" id="Seg_103" s="T8">лукошко-PL.[NOM]</ta>
            <ta e="T10" id="Seg_104" s="T9">сшить-HAB-INFER-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_105" s="T1">adj</ta>
            <ta e="T3" id="Seg_106" s="T2">n-n:num.[n:case]</ta>
            <ta e="T4" id="Seg_107" s="T3">pers-n:ins-n:case</ta>
            <ta e="T5" id="Seg_108" s="T4">v-v&gt;v-v:pn</ta>
            <ta e="T6" id="Seg_109" s="T5">pers-n:case</ta>
            <ta e="T7" id="Seg_110" s="T6">n.[n:case]</ta>
            <ta e="T8" id="Seg_111" s="T7">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T9" id="Seg_112" s="T8">n-n:num.[n:case]</ta>
            <ta e="T10" id="Seg_113" s="T9">v-v&gt;v-v:mood-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_114" s="T1">n</ta>
            <ta e="T3" id="Seg_115" s="T2">n</ta>
            <ta e="T4" id="Seg_116" s="T3">pers</ta>
            <ta e="T5" id="Seg_117" s="T4">v</ta>
            <ta e="T6" id="Seg_118" s="T5">pers</ta>
            <ta e="T7" id="Seg_119" s="T6">n</ta>
            <ta e="T8" id="Seg_120" s="T7">v</ta>
            <ta e="T9" id="Seg_121" s="T8">n</ta>
            <ta e="T10" id="Seg_122" s="T9">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T4" id="Seg_123" s="T3">pro.h:P</ta>
            <ta e="T5" id="Seg_124" s="T4">0.3.h:A</ta>
            <ta e="T6" id="Seg_125" s="T5">pro:So</ta>
            <ta e="T7" id="Seg_126" s="T6">np:P</ta>
            <ta e="T8" id="Seg_127" s="T7">0.3.h:A</ta>
            <ta e="T9" id="Seg_128" s="T8">np:P</ta>
            <ta e="T10" id="Seg_129" s="T9">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_130" s="T3">pro:O</ta>
            <ta e="T5" id="Seg_131" s="T4">0.3.h:S v:pred</ta>
            <ta e="T7" id="Seg_132" s="T6">np:O</ta>
            <ta e="T8" id="Seg_133" s="T7">0.3.h:S v:pred</ta>
            <ta e="T9" id="Seg_134" s="T8">np:O</ta>
            <ta e="T10" id="Seg_135" s="T9">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_136" s="T1">Little branches of bird cherry, they chop them off.</ta>
            <ta e="T8" id="Seg_137" s="T5">They make twigs out of them.</ta>
            <ta e="T10" id="Seg_138" s="T8">They make baskets.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_139" s="T1">Kleine Wildkirschenzweige, sie schneiden sie ab.</ta>
            <ta e="T8" id="Seg_140" s="T5">Aus ihnen machen sie dünne Zweige.</ta>
            <ta e="T10" id="Seg_141" s="T8">Sie weben Körbe.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_142" s="T1">Маленькие черемошники (ветки черемухи), его (их) рубят.</ta>
            <ta e="T8" id="Seg_143" s="T5">Из него сарги делают.</ta>
            <ta e="T10" id="Seg_144" s="T8">Лукошки шьют.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_145" s="T1">маленькие черемошники (от черемухи ветки) его (их) рубят</ta>
            <ta e="T8" id="Seg_146" s="T5">от него (из него)сарги делают</ta>
            <ta e="T10" id="Seg_147" s="T8">лукошки шьют</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto">
            <ta e="T8" id="Seg_148" s="T5">сарга: от черемухи отрывают тонкие прутья, их обдирают и этим шьют кузова</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
