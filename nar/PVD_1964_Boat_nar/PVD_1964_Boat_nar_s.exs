<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Boat_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Boat_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">17</ud-information>
            <ud-information attribute-name="# HIAT:w">13</ud-information>
            <ud-information attribute-name="# e">13</ud-information>
            <ud-information attribute-name="# HIAT:u">3</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T14" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Tau</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">poɣɨndo</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">atdokala</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">soːdʼiga</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">jekudattə</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_20" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">Atdoka</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">tʼöptɨka</ts>
                  <nts id="Seg_26" n="HIAT:ip">,</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">jass</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">pasedʼikun</ts>
                  <nts id="Seg_33" n="HIAT:ip">.</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_36" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">A</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">üdəpi</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">pazelʼlʼe</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">čaʒət</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T14" id="Seg_50" n="sc" s="T1">
               <ts e="T2" id="Seg_52" n="e" s="T1">Tau </ts>
               <ts e="T3" id="Seg_54" n="e" s="T2">poɣɨndo </ts>
               <ts e="T4" id="Seg_56" n="e" s="T3">atdokala </ts>
               <ts e="T5" id="Seg_58" n="e" s="T4">soːdʼiga </ts>
               <ts e="T6" id="Seg_60" n="e" s="T5">jekudattə. </ts>
               <ts e="T7" id="Seg_62" n="e" s="T6">Atdoka </ts>
               <ts e="T8" id="Seg_64" n="e" s="T7">tʼöptɨka, </ts>
               <ts e="T9" id="Seg_66" n="e" s="T8">jass </ts>
               <ts e="T10" id="Seg_68" n="e" s="T9">pasedʼikun. </ts>
               <ts e="T11" id="Seg_70" n="e" s="T10">A </ts>
               <ts e="T12" id="Seg_72" n="e" s="T11">üdəpi </ts>
               <ts e="T13" id="Seg_74" n="e" s="T12">pazelʼlʼe </ts>
               <ts e="T14" id="Seg_76" n="e" s="T13">čaʒət. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_77" s="T1">PVD_1964_Boat_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_78" s="T6">PVD_1964_Boat_nar.002 (001.002)</ta>
            <ta e="T14" id="Seg_79" s="T10">PVD_1964_Boat_nar.003 (001.003)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_80" s="T1">′тау̹ ′поɣындо ат′докала со̄дʼига ′jекудаттъ.</ta>
            <ta e="T10" id="Seg_81" s="T6">ат′дока ′тʼӧптыка, ′jасс па′седʼикун.</ta>
            <ta e="T14" id="Seg_82" s="T10">а ӱдъ′пи па′зелʼлʼе ′тшажът.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_83" s="T1">tau̹ poɣɨndo atdokala soːdʼiga jekudattə.</ta>
            <ta e="T10" id="Seg_84" s="T6">atdoka tʼöptɨka, jass pasedʼikun.</ta>
            <ta e="T14" id="Seg_85" s="T10">a üdəpi pazelʼlʼe tšaʒət.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_86" s="T1">Tau poɣɨndo atdokala soːdʼiga jekudattə. </ta>
            <ta e="T10" id="Seg_87" s="T6">Atdoka tʼöptɨka, jass pasedʼikun. </ta>
            <ta e="T14" id="Seg_88" s="T10">A üdəpi pazelʼlʼe čaʒət. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_89" s="T1">tau</ta>
            <ta e="T3" id="Seg_90" s="T2">po-ɣɨndo</ta>
            <ta e="T4" id="Seg_91" s="T3">atdo-ka-la</ta>
            <ta e="T5" id="Seg_92" s="T4">soːdʼiga</ta>
            <ta e="T6" id="Seg_93" s="T5">je-ku-da-ttə</ta>
            <ta e="T7" id="Seg_94" s="T6">atdo-ka</ta>
            <ta e="T8" id="Seg_95" s="T7">tʼöptɨka</ta>
            <ta e="T9" id="Seg_96" s="T8">jass</ta>
            <ta e="T10" id="Seg_97" s="T9">pasedʼi-ku-n</ta>
            <ta e="T11" id="Seg_98" s="T10">a</ta>
            <ta e="T12" id="Seg_99" s="T11">üdəpi</ta>
            <ta e="T13" id="Seg_100" s="T12">pazelʼ-lʼe</ta>
            <ta e="T14" id="Seg_101" s="T13">čaʒə-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_102" s="T1">taw</ta>
            <ta e="T3" id="Seg_103" s="T2">po-qɨntɨ</ta>
            <ta e="T4" id="Seg_104" s="T3">andǝ-ka-la</ta>
            <ta e="T5" id="Seg_105" s="T4">soːdʼiga</ta>
            <ta e="T6" id="Seg_106" s="T5">eː-ku-ntɨ-tɨn</ta>
            <ta e="T7" id="Seg_107" s="T6">andǝ-ka</ta>
            <ta e="T8" id="Seg_108" s="T7">tʼöptɨka</ta>
            <ta e="T9" id="Seg_109" s="T8">asa</ta>
            <ta e="T10" id="Seg_110" s="T9">pasedʼi-ku-n</ta>
            <ta e="T11" id="Seg_111" s="T10">a</ta>
            <ta e="T12" id="Seg_112" s="T11">üdippi</ta>
            <ta e="T13" id="Seg_113" s="T12">pazel-le</ta>
            <ta e="T14" id="Seg_114" s="T13">čaǯɨ-ntɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_115" s="T1">this</ta>
            <ta e="T3" id="Seg_116" s="T2">tree-EL.3SG</ta>
            <ta e="T4" id="Seg_117" s="T3">boat-DIM-PL.[NOM]</ta>
            <ta e="T5" id="Seg_118" s="T4">good</ta>
            <ta e="T6" id="Seg_119" s="T5">be-HAB-INFER-3PL</ta>
            <ta e="T7" id="Seg_120" s="T6">boat-DIM.[NOM]</ta>
            <ta e="T8" id="Seg_121" s="T7">thin.[3SG.S]</ta>
            <ta e="T9" id="Seg_122" s="T8">NEG</ta>
            <ta e="T10" id="Seg_123" s="T9">split-HAB-3SG.S</ta>
            <ta e="T11" id="Seg_124" s="T10">and</ta>
            <ta e="T12" id="Seg_125" s="T11">poplar.[NOM]</ta>
            <ta e="T13" id="Seg_126" s="T12">crack-CVB</ta>
            <ta e="T14" id="Seg_127" s="T13">go-INFER.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_128" s="T1">этот</ta>
            <ta e="T3" id="Seg_129" s="T2">дерево-EL.3SG</ta>
            <ta e="T4" id="Seg_130" s="T3">обласок-DIM-PL.[NOM]</ta>
            <ta e="T5" id="Seg_131" s="T4">хороший</ta>
            <ta e="T6" id="Seg_132" s="T5">быть-HAB-INFER-3PL</ta>
            <ta e="T7" id="Seg_133" s="T6">обласок-DIM.[NOM]</ta>
            <ta e="T8" id="Seg_134" s="T7">тонкий.[3SG.S]</ta>
            <ta e="T9" id="Seg_135" s="T8">NEG</ta>
            <ta e="T10" id="Seg_136" s="T9">расколоться-HAB-3SG.S</ta>
            <ta e="T11" id="Seg_137" s="T10">а</ta>
            <ta e="T12" id="Seg_138" s="T11">тополь.[NOM]</ta>
            <ta e="T13" id="Seg_139" s="T12">потрескаться-CVB</ta>
            <ta e="T14" id="Seg_140" s="T13">ходить-INFER.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_141" s="T1">dem</ta>
            <ta e="T3" id="Seg_142" s="T2">n-n:case.poss</ta>
            <ta e="T4" id="Seg_143" s="T3">n-n&gt;n-n:num.[n:case]</ta>
            <ta e="T5" id="Seg_144" s="T4">adj</ta>
            <ta e="T6" id="Seg_145" s="T5">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T7" id="Seg_146" s="T6">n-n&gt;n.[n:case]</ta>
            <ta e="T8" id="Seg_147" s="T7">adj.[v:pn]</ta>
            <ta e="T9" id="Seg_148" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_149" s="T9">v-v&gt;v-v:pn</ta>
            <ta e="T11" id="Seg_150" s="T10">conj</ta>
            <ta e="T12" id="Seg_151" s="T11">n.[n:case]</ta>
            <ta e="T13" id="Seg_152" s="T12">v-v&gt;adv</ta>
            <ta e="T14" id="Seg_153" s="T13">v-v:mood.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_154" s="T1">dem</ta>
            <ta e="T3" id="Seg_155" s="T2">n</ta>
            <ta e="T4" id="Seg_156" s="T3">n</ta>
            <ta e="T5" id="Seg_157" s="T4">adj</ta>
            <ta e="T6" id="Seg_158" s="T5">v</ta>
            <ta e="T7" id="Seg_159" s="T6">n</ta>
            <ta e="T8" id="Seg_160" s="T7">adj</ta>
            <ta e="T9" id="Seg_161" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_162" s="T9">v</ta>
            <ta e="T11" id="Seg_163" s="T10">conj</ta>
            <ta e="T12" id="Seg_164" s="T11">n</ta>
            <ta e="T13" id="Seg_165" s="T12">adv</ta>
            <ta e="T14" id="Seg_166" s="T13">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_167" s="T2">np:So</ta>
            <ta e="T4" id="Seg_168" s="T3">np:Th</ta>
            <ta e="T7" id="Seg_169" s="T6">np:Th</ta>
            <ta e="T10" id="Seg_170" s="T9">0.3:P</ta>
            <ta e="T12" id="Seg_171" s="T11">np:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_172" s="T3">np:S</ta>
            <ta e="T5" id="Seg_173" s="T4">adj:pred</ta>
            <ta e="T6" id="Seg_174" s="T5">cop</ta>
            <ta e="T7" id="Seg_175" s="T6">np:S</ta>
            <ta e="T8" id="Seg_176" s="T7">adj:pred</ta>
            <ta e="T10" id="Seg_177" s="T9">0.3:S v:pred</ta>
            <ta e="T12" id="Seg_178" s="T11">np:S</ta>
            <ta e="T14" id="Seg_179" s="T13">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T11" id="Seg_180" s="T10">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_181" s="T1">Boats [made] out of this tree were good.</ta>
            <ta e="T10" id="Seg_182" s="T6">A boat is thin, it doesn't crack.</ta>
            <ta e="T14" id="Seg_183" s="T10">And poplar cracks.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_184" s="T1">Aus diesem Baum [gefertigte] Boote sind gut.</ta>
            <ta e="T10" id="Seg_185" s="T6">Ein Boot ist dünn, es bricht nicht.</ta>
            <ta e="T14" id="Seg_186" s="T10">Und Pappel bricht.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_187" s="T1">Из этого дерева (леса) хорошиие обласки были.</ta>
            <ta e="T10" id="Seg_188" s="T6">Обласок тоненький, не колется.</ta>
            <ta e="T14" id="Seg_189" s="T10">А тополь колется.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_190" s="T1">из этого дерева (леса) хороший обласок был</ta>
            <ta e="T10" id="Seg_191" s="T6">обласок тоненький не колется</ta>
            <ta e="T14" id="Seg_192" s="T10">тополь колется</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T14" id="Seg_193" s="T10">[BrM:] INFER?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
