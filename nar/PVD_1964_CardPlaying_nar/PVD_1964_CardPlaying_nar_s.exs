<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_CardPlaying_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_CardPlaying_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">146</ud-information>
            <ud-information attribute-name="# HIAT:w">99</ud-information>
            <ud-information attribute-name="# e">99</ud-information>
            <ud-information attribute-name="# HIAT:u">44</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="f" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T100" id="Seg_0" n="sc" s="T1">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Tawaj</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">kozɨrčuluttu</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T5" id="Seg_11" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">Nadə</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">kozurčugu</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_20" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">Dama</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">meka</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">tattɨt</ts>
                  <nts id="Seg_29" n="HIAT:ip">!</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_32" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">Paldʼak</ts>
                  <nts id="Seg_35" n="HIAT:ip">!</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_38" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_40" n="HIAT:w" s="T9">Iːow</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_44" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_46" n="HIAT:w" s="T10">Oj</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_49" n="HIAT:w" s="T11">qajlazʼe</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_52" n="HIAT:w" s="T12">palden</ts>
                  <nts id="Seg_53" n="HIAT:ip">!</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_56" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_58" n="HIAT:w" s="T13">Sʼelʼde</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_62" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_64" n="HIAT:w" s="T14">Aj</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_67" n="HIAT:w" s="T15">selʼde</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_71" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_73" n="HIAT:w" s="T16">Metdɨ</ts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_77" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_79" n="HIAT:w" s="T17">Paldʼak</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_82" n="HIAT:w" s="T18">mekka</ts>
                  <nts id="Seg_83" n="HIAT:ip">!</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_86" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_88" n="HIAT:w" s="T19">Muqtɨt</ts>
                  <nts id="Seg_89" n="HIAT:ip">.</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_92" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_94" n="HIAT:w" s="T20">Alɨnɨn</ts>
                  <nts id="Seg_95" n="HIAT:ip">!</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_98" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_100" n="HIAT:w" s="T21">Alɨnɨn</ts>
                  <nts id="Seg_101" n="HIAT:ip">!</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_104" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_106" n="HIAT:w" s="T22">Tʼaggu</ts>
                  <nts id="Seg_107" n="HIAT:ip">!</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_110" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_112" n="HIAT:w" s="T23">Qajno</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_115" n="HIAT:w" s="T24">as</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_118" n="HIAT:w" s="T25">mezal</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_120" n="HIAT:ip">(</nts>
                  <ts e="T27" id="Seg_122" n="HIAT:w" s="T26">qajnos</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_125" n="HIAT:w" s="T27">mezal</ts>
                  <nts id="Seg_126" n="HIAT:ip">)</nts>
                  <nts id="Seg_127" n="HIAT:ip">?</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_130" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_132" n="HIAT:w" s="T28">Man</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_135" n="HIAT:w" s="T29">paldedan</ts>
                  <nts id="Seg_136" n="HIAT:ip">.</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_139" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_141" n="HIAT:w" s="T30">Tep</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_144" n="HIAT:w" s="T31">dura</ts>
                  <nts id="Seg_145" n="HIAT:ip">.</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_148" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_150" n="HIAT:w" s="T32">Tan</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_153" n="HIAT:w" s="T33">lʼišnan</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_156" n="HIAT:w" s="T34">igə</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_159" n="HIAT:w" s="T35">ikʼət</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_163" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_165" n="HIAT:w" s="T36">Igə</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_168" n="HIAT:w" s="T37">idərte</ts>
                  <nts id="Seg_169" n="HIAT:ip">,</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_172" n="HIAT:w" s="T38">metdə</ts>
                  <nts id="Seg_173" n="HIAT:ip">!</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_176" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_178" n="HIAT:w" s="T39">Kozɨrlam</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_181" n="HIAT:w" s="T40">nadə</ts>
                  <nts id="Seg_182" n="HIAT:ip">.</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_185" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_187" n="HIAT:w" s="T41">Idərugu</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_190" n="HIAT:w" s="T42">ne</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_193" n="HIAT:w" s="T43">nado</ts>
                  <nts id="Seg_194" n="HIAT:ip">.</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_197" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_199" n="HIAT:w" s="T44">Man</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_202" n="HIAT:w" s="T45">qalan</ts>
                  <nts id="Seg_203" n="HIAT:ip">.</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_206" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_208" n="HIAT:w" s="T46">Sičas</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_211" n="HIAT:w" s="T47">tan</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_214" n="HIAT:w" s="T48">qalʼinaš</ts>
                  <nts id="Seg_215" n="HIAT:ip">.</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_218" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_220" n="HIAT:w" s="T49">Man</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_223" n="HIAT:w" s="T50">as</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_226" n="HIAT:w" s="T51">qalʼeǯan</ts>
                  <nts id="Seg_227" n="HIAT:ip">.</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_230" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_232" n="HIAT:w" s="T52">Tan</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_235" n="HIAT:w" s="T53">nʼilʼdʼzʼin</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_238" n="HIAT:w" s="T54">paldʼat</ts>
                  <nts id="Seg_239" n="HIAT:ip">?</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_242" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_244" n="HIAT:w" s="T55">Nejɣulam</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_247" n="HIAT:w" s="T56">tanaltə</ts>
                  <nts id="Seg_248" n="HIAT:ip">.</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_251" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_253" n="HIAT:w" s="T57">Warkɨzan</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_256" n="HIAT:w" s="T58">oqqɨr</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_259" n="HIAT:w" s="T59">kozɨrnej</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_262" n="HIAT:w" s="T60">tʼakkus</ts>
                  <nts id="Seg_263" n="HIAT:ip">.</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_266" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_268" n="HIAT:w" s="T61">Manan</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_271" n="HIAT:w" s="T62">sodʼiga</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_274" n="HIAT:w" s="T63">kozɨrla</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_277" n="HIAT:w" s="T64">jezattə</ts>
                  <nts id="Seg_278" n="HIAT:ip">.</nts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_281" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_283" n="HIAT:w" s="T65">Tawaj</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_286" n="HIAT:w" s="T66">qɨbanʼaʒam</ts>
                  <nts id="Seg_287" n="HIAT:ip">!</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_290" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_292" n="HIAT:w" s="T67">Targu</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_295" n="HIAT:w" s="T68">nado</ts>
                  <nts id="Seg_296" n="HIAT:ip">.</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_299" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_301" n="HIAT:w" s="T69">Tartaq</ts>
                  <nts id="Seg_302" n="HIAT:ip">.</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_305" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_307" n="HIAT:w" s="T70">Manan</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_310" n="HIAT:w" s="T71">srodu</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_313" n="HIAT:w" s="T72">tus</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_316" n="HIAT:w" s="T73">tʼakku</ts>
                  <nts id="Seg_317" n="HIAT:ip">.</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_320" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_322" n="HIAT:w" s="T74">Jas</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_325" n="HIAT:w" s="T75">tunou</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_328" n="HIAT:w" s="T76">qajzʼe</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_331" n="HIAT:w" s="T77">krɨtʼ</ts>
                  <nts id="Seg_332" n="HIAT:ip">.</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_335" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_337" n="HIAT:w" s="T78">Swetlana</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_340" n="HIAT:w" s="T79">qalɨn</ts>
                  <nts id="Seg_341" n="HIAT:ip">.</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_344" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_346" n="HIAT:w" s="T80">Qussän</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_349" n="HIAT:w" s="T81">tannan</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_352" n="HIAT:w" s="T82">qozɨr</ts>
                  <nts id="Seg_353" n="HIAT:ip">?</nts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_356" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_358" n="HIAT:w" s="T83">Tan</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_361" n="HIAT:w" s="T84">igə</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_364" n="HIAT:w" s="T85">mannɨpaq</ts>
                  <nts id="Seg_365" n="HIAT:ip">!</nts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_368" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_370" n="HIAT:w" s="T86">Xwatit</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_373" n="HIAT:w" s="T87">warkugu</ts>
                  <nts id="Seg_374" n="HIAT:ip">!</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_377" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_379" n="HIAT:w" s="T88">Dawaj</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_382" n="HIAT:w" s="T89">ješo</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_385" n="HIAT:w" s="T90">warkuluttu</ts>
                  <nts id="Seg_386" n="HIAT:ip">.</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_389" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_391" n="HIAT:w" s="T91">Tilʼdʼiːn</ts>
                  <nts id="Seg_392" n="HIAT:ip">!</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_395" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_397" n="HIAT:w" s="T92">Tildʼiːn</ts>
                  <nts id="Seg_398" n="HIAT:ip">!</nts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_401" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_403" n="HIAT:w" s="T93">Ollou</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_406" n="HIAT:w" s="T94">küzeǯa</ts>
                  <nts id="Seg_407" n="HIAT:ip">.</nts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_410" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_412" n="HIAT:w" s="T95">Nadə</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_415" n="HIAT:w" s="T96">tejerbɨguː</ts>
                  <nts id="Seg_416" n="HIAT:ip">.</nts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_419" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_421" n="HIAT:w" s="T97">Man</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_424" n="HIAT:w" s="T98">tejerban</ts>
                  <nts id="Seg_425" n="HIAT:ip">.</nts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_428" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_430" n="HIAT:w" s="T99">Tejerbaːq</ts>
                  <nts id="Seg_431" n="HIAT:ip">.</nts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T100" id="Seg_433" n="sc" s="T1">
               <ts e="T2" id="Seg_435" n="e" s="T1">Tawaj </ts>
               <ts e="T3" id="Seg_437" n="e" s="T2">kozɨrčuluttu. </ts>
               <ts e="T4" id="Seg_439" n="e" s="T3">Nadə </ts>
               <ts e="T5" id="Seg_441" n="e" s="T4">kozurčugu. </ts>
               <ts e="T6" id="Seg_443" n="e" s="T5">Dama </ts>
               <ts e="T7" id="Seg_445" n="e" s="T6">meka </ts>
               <ts e="T8" id="Seg_447" n="e" s="T7">tattɨt! </ts>
               <ts e="T9" id="Seg_449" n="e" s="T8">Paldʼak! </ts>
               <ts e="T10" id="Seg_451" n="e" s="T9">Iːow. </ts>
               <ts e="T11" id="Seg_453" n="e" s="T10">Oj </ts>
               <ts e="T12" id="Seg_455" n="e" s="T11">qajlazʼe </ts>
               <ts e="T13" id="Seg_457" n="e" s="T12">palden! </ts>
               <ts e="T14" id="Seg_459" n="e" s="T13">Sʼelʼde. </ts>
               <ts e="T15" id="Seg_461" n="e" s="T14">Aj </ts>
               <ts e="T16" id="Seg_463" n="e" s="T15">selʼde. </ts>
               <ts e="T17" id="Seg_465" n="e" s="T16">Metdɨ. </ts>
               <ts e="T18" id="Seg_467" n="e" s="T17">Paldʼak </ts>
               <ts e="T19" id="Seg_469" n="e" s="T18">mekka! </ts>
               <ts e="T20" id="Seg_471" n="e" s="T19">Muqtɨt. </ts>
               <ts e="T21" id="Seg_473" n="e" s="T20">Alɨnɨn! </ts>
               <ts e="T22" id="Seg_475" n="e" s="T21">Alɨnɨn! </ts>
               <ts e="T23" id="Seg_477" n="e" s="T22">Tʼaggu! </ts>
               <ts e="T24" id="Seg_479" n="e" s="T23">Qajno </ts>
               <ts e="T25" id="Seg_481" n="e" s="T24">as </ts>
               <ts e="T26" id="Seg_483" n="e" s="T25">mezal </ts>
               <ts e="T27" id="Seg_485" n="e" s="T26">(qajnos </ts>
               <ts e="T28" id="Seg_487" n="e" s="T27">mezal)? </ts>
               <ts e="T29" id="Seg_489" n="e" s="T28">Man </ts>
               <ts e="T30" id="Seg_491" n="e" s="T29">paldedan. </ts>
               <ts e="T31" id="Seg_493" n="e" s="T30">Tep </ts>
               <ts e="T32" id="Seg_495" n="e" s="T31">dura. </ts>
               <ts e="T33" id="Seg_497" n="e" s="T32">Tan </ts>
               <ts e="T34" id="Seg_499" n="e" s="T33">lʼišnan </ts>
               <ts e="T35" id="Seg_501" n="e" s="T34">igə </ts>
               <ts e="T36" id="Seg_503" n="e" s="T35">ikʼət. </ts>
               <ts e="T37" id="Seg_505" n="e" s="T36">Igə </ts>
               <ts e="T38" id="Seg_507" n="e" s="T37">idərte, </ts>
               <ts e="T39" id="Seg_509" n="e" s="T38">metdə! </ts>
               <ts e="T40" id="Seg_511" n="e" s="T39">Kozɨrlam </ts>
               <ts e="T41" id="Seg_513" n="e" s="T40">nadə. </ts>
               <ts e="T42" id="Seg_515" n="e" s="T41">Idərugu </ts>
               <ts e="T43" id="Seg_517" n="e" s="T42">ne </ts>
               <ts e="T44" id="Seg_519" n="e" s="T43">nado. </ts>
               <ts e="T45" id="Seg_521" n="e" s="T44">Man </ts>
               <ts e="T46" id="Seg_523" n="e" s="T45">qalan. </ts>
               <ts e="T47" id="Seg_525" n="e" s="T46">Sičas </ts>
               <ts e="T48" id="Seg_527" n="e" s="T47">tan </ts>
               <ts e="T49" id="Seg_529" n="e" s="T48">qalʼinaš. </ts>
               <ts e="T50" id="Seg_531" n="e" s="T49">Man </ts>
               <ts e="T51" id="Seg_533" n="e" s="T50">as </ts>
               <ts e="T52" id="Seg_535" n="e" s="T51">qalʼeǯan. </ts>
               <ts e="T53" id="Seg_537" n="e" s="T52">Tan </ts>
               <ts e="T54" id="Seg_539" n="e" s="T53">nʼilʼdʼzʼin </ts>
               <ts e="T55" id="Seg_541" n="e" s="T54">paldʼat? </ts>
               <ts e="T56" id="Seg_543" n="e" s="T55">Nejɣulam </ts>
               <ts e="T57" id="Seg_545" n="e" s="T56">tanaltə. </ts>
               <ts e="T58" id="Seg_547" n="e" s="T57">Warkɨzan </ts>
               <ts e="T59" id="Seg_549" n="e" s="T58">oqqɨr </ts>
               <ts e="T60" id="Seg_551" n="e" s="T59">kozɨrnej </ts>
               <ts e="T61" id="Seg_553" n="e" s="T60">tʼakkus. </ts>
               <ts e="T62" id="Seg_555" n="e" s="T61">Manan </ts>
               <ts e="T63" id="Seg_557" n="e" s="T62">sodʼiga </ts>
               <ts e="T64" id="Seg_559" n="e" s="T63">kozɨrla </ts>
               <ts e="T65" id="Seg_561" n="e" s="T64">jezattə. </ts>
               <ts e="T66" id="Seg_563" n="e" s="T65">Tawaj </ts>
               <ts e="T67" id="Seg_565" n="e" s="T66">qɨbanʼaʒam! </ts>
               <ts e="T68" id="Seg_567" n="e" s="T67">Targu </ts>
               <ts e="T69" id="Seg_569" n="e" s="T68">nado. </ts>
               <ts e="T70" id="Seg_571" n="e" s="T69">Tartaq. </ts>
               <ts e="T71" id="Seg_573" n="e" s="T70">Manan </ts>
               <ts e="T72" id="Seg_575" n="e" s="T71">srodu </ts>
               <ts e="T73" id="Seg_577" n="e" s="T72">tus </ts>
               <ts e="T74" id="Seg_579" n="e" s="T73">tʼakku. </ts>
               <ts e="T75" id="Seg_581" n="e" s="T74">Jas </ts>
               <ts e="T76" id="Seg_583" n="e" s="T75">tunou </ts>
               <ts e="T77" id="Seg_585" n="e" s="T76">qajzʼe </ts>
               <ts e="T78" id="Seg_587" n="e" s="T77">krɨtʼ. </ts>
               <ts e="T79" id="Seg_589" n="e" s="T78">Swetlana </ts>
               <ts e="T80" id="Seg_591" n="e" s="T79">qalɨn. </ts>
               <ts e="T81" id="Seg_593" n="e" s="T80">Qussän </ts>
               <ts e="T82" id="Seg_595" n="e" s="T81">tannan </ts>
               <ts e="T83" id="Seg_597" n="e" s="T82">qozɨr? </ts>
               <ts e="T84" id="Seg_599" n="e" s="T83">Tan </ts>
               <ts e="T85" id="Seg_601" n="e" s="T84">igə </ts>
               <ts e="T86" id="Seg_603" n="e" s="T85">mannɨpaq! </ts>
               <ts e="T87" id="Seg_605" n="e" s="T86">Xwatit </ts>
               <ts e="T88" id="Seg_607" n="e" s="T87">warkugu! </ts>
               <ts e="T89" id="Seg_609" n="e" s="T88">Dawaj </ts>
               <ts e="T90" id="Seg_611" n="e" s="T89">ješo </ts>
               <ts e="T91" id="Seg_613" n="e" s="T90">warkuluttu. </ts>
               <ts e="T92" id="Seg_615" n="e" s="T91">Tilʼdʼiːn! </ts>
               <ts e="T93" id="Seg_617" n="e" s="T92">Tildʼiːn! </ts>
               <ts e="T94" id="Seg_619" n="e" s="T93">Ollou </ts>
               <ts e="T95" id="Seg_621" n="e" s="T94">küzeǯa. </ts>
               <ts e="T96" id="Seg_623" n="e" s="T95">Nadə </ts>
               <ts e="T97" id="Seg_625" n="e" s="T96">tejerbɨguː. </ts>
               <ts e="T98" id="Seg_627" n="e" s="T97">Man </ts>
               <ts e="T99" id="Seg_629" n="e" s="T98">tejerban. </ts>
               <ts e="T100" id="Seg_631" n="e" s="T99">Tejerbaːq. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_632" s="T1">PVD_1964_CardPlaying_nar.001 (001.001)</ta>
            <ta e="T5" id="Seg_633" s="T3">PVD_1964_CardPlaying_nar.002 (001.002)</ta>
            <ta e="T8" id="Seg_634" s="T5">PVD_1964_CardPlaying_nar.003 (001.003)</ta>
            <ta e="T9" id="Seg_635" s="T8">PVD_1964_CardPlaying_nar.004 (001.004)</ta>
            <ta e="T10" id="Seg_636" s="T9">PVD_1964_CardPlaying_nar.005 (001.005)</ta>
            <ta e="T13" id="Seg_637" s="T10">PVD_1964_CardPlaying_nar.006 (001.006)</ta>
            <ta e="T14" id="Seg_638" s="T13">PVD_1964_CardPlaying_nar.007 (001.007)</ta>
            <ta e="T16" id="Seg_639" s="T14">PVD_1964_CardPlaying_nar.008 (001.008)</ta>
            <ta e="T17" id="Seg_640" s="T16">PVD_1964_CardPlaying_nar.009 (001.009)</ta>
            <ta e="T19" id="Seg_641" s="T17">PVD_1964_CardPlaying_nar.010 (001.010)</ta>
            <ta e="T20" id="Seg_642" s="T19">PVD_1964_CardPlaying_nar.011 (001.011)</ta>
            <ta e="T21" id="Seg_643" s="T20">PVD_1964_CardPlaying_nar.012 (001.012)</ta>
            <ta e="T22" id="Seg_644" s="T21">PVD_1964_CardPlaying_nar.013 (001.013)</ta>
            <ta e="T23" id="Seg_645" s="T22">PVD_1964_CardPlaying_nar.014 (001.014)</ta>
            <ta e="T28" id="Seg_646" s="T23">PVD_1964_CardPlaying_nar.015 (001.015)</ta>
            <ta e="T30" id="Seg_647" s="T28">PVD_1964_CardPlaying_nar.016 (001.016)</ta>
            <ta e="T32" id="Seg_648" s="T30">PVD_1964_CardPlaying_nar.017 (001.017)</ta>
            <ta e="T36" id="Seg_649" s="T32">PVD_1964_CardPlaying_nar.018 (001.018)</ta>
            <ta e="T39" id="Seg_650" s="T36">PVD_1964_CardPlaying_nar.019 (001.019)</ta>
            <ta e="T41" id="Seg_651" s="T39">PVD_1964_CardPlaying_nar.020 (001.020)</ta>
            <ta e="T44" id="Seg_652" s="T41">PVD_1964_CardPlaying_nar.021 (001.021)</ta>
            <ta e="T46" id="Seg_653" s="T44">PVD_1964_CardPlaying_nar.022 (001.022)</ta>
            <ta e="T49" id="Seg_654" s="T46">PVD_1964_CardPlaying_nar.023 (001.023)</ta>
            <ta e="T52" id="Seg_655" s="T49">PVD_1964_CardPlaying_nar.024 (001.024)</ta>
            <ta e="T55" id="Seg_656" s="T52">PVD_1964_CardPlaying_nar.025 (001.025)</ta>
            <ta e="T57" id="Seg_657" s="T55">PVD_1964_CardPlaying_nar.026 (001.026)</ta>
            <ta e="T61" id="Seg_658" s="T57">PVD_1964_CardPlaying_nar.027 (001.027)</ta>
            <ta e="T65" id="Seg_659" s="T61">PVD_1964_CardPlaying_nar.028 (001.028)</ta>
            <ta e="T67" id="Seg_660" s="T65">PVD_1964_CardPlaying_nar.029 (001.029)</ta>
            <ta e="T69" id="Seg_661" s="T67">PVD_1964_CardPlaying_nar.030 (001.030)</ta>
            <ta e="T70" id="Seg_662" s="T69">PVD_1964_CardPlaying_nar.031 (001.031)</ta>
            <ta e="T74" id="Seg_663" s="T70">PVD_1964_CardPlaying_nar.032 (001.032)</ta>
            <ta e="T78" id="Seg_664" s="T74">PVD_1964_CardPlaying_nar.033 (001.033)</ta>
            <ta e="T80" id="Seg_665" s="T78">PVD_1964_CardPlaying_nar.034 (001.034)</ta>
            <ta e="T83" id="Seg_666" s="T80">PVD_1964_CardPlaying_nar.035 (001.035)</ta>
            <ta e="T86" id="Seg_667" s="T83">PVD_1964_CardPlaying_nar.036 (001.036)</ta>
            <ta e="T88" id="Seg_668" s="T86">PVD_1964_CardPlaying_nar.037 (001.037)</ta>
            <ta e="T91" id="Seg_669" s="T88">PVD_1964_CardPlaying_nar.038 (001.038)</ta>
            <ta e="T92" id="Seg_670" s="T91">PVD_1964_CardPlaying_nar.039 (001.039)</ta>
            <ta e="T93" id="Seg_671" s="T92">PVD_1964_CardPlaying_nar.040 (001.040)</ta>
            <ta e="T95" id="Seg_672" s="T93">PVD_1964_CardPlaying_nar.041 (001.041)</ta>
            <ta e="T97" id="Seg_673" s="T95">PVD_1964_CardPlaying_nar.042 (001.042)</ta>
            <ta e="T99" id="Seg_674" s="T97">PVD_1964_CardPlaying_nar.043 (001.043)</ta>
            <ta e="T100" id="Seg_675" s="T99">PVD_1964_CardPlaying_nar.044 (001.044)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_676" s="T1">та′вай ′козыртшулутту.</ta>
            <ta e="T5" id="Seg_677" s="T3">надъ ′козуртшугу.</ta>
            <ta e="T8" id="Seg_678" s="T5">д̂ама ′мека таттыт!</ta>
            <ta e="T9" id="Seg_679" s="T8">паlдʼак!</ta>
            <ta e="T10" id="Seg_680" s="T9">′ӣоw.</ta>
            <ta e="T13" id="Seg_681" s="T10">ой ′kайлазʼе ′паlден!</ta>
            <ta e="T14" id="Seg_682" s="T13">сʼелʼде.</ta>
            <ta e="T16" id="Seg_683" s="T14">ай селʼде.</ta>
            <ta e="T17" id="Seg_684" s="T16">мет′ды.</ta>
            <ta e="T19" id="Seg_685" s="T17">паlдʼак мек(г̂)ка!</ta>
            <ta e="T20" id="Seg_686" s="T19">′муkтыт.</ta>
            <ta e="T21" id="Seg_687" s="T20">′алынын!</ta>
            <ta e="T22" id="Seg_688" s="T21">′алынын!</ta>
            <ta e="T23" id="Seg_689" s="T22">′тʼаг̂гу!</ta>
            <ta e="T28" id="Seg_690" s="T23">kай′но ас ме′зал (kайнос мезал)?</ta>
            <ta e="T30" id="Seg_691" s="T28">ман ′паlдедан.</ta>
            <ta e="T32" id="Seg_692" s="T30">теп д̂ура.</ta>
            <ta e="T36" id="Seg_693" s="T32">тан ′лʼишнан ′ига(ъ) икʼът.</ta>
            <ta e="T39" id="Seg_694" s="T36">′игъ ′идърте, ′ме(и)тдъ!</ta>
            <ta e="T41" id="Seg_695" s="T39">козыр′лам надъ.</ta>
            <ta e="T44" id="Seg_696" s="T41">′идъругу не надо.</ta>
            <ta e="T46" id="Seg_697" s="T44">ман kа′лан.</ta>
            <ta e="T49" id="Seg_698" s="T46">си′час тан ′kалʼинаш.</ta>
            <ta e="T52" id="Seg_699" s="T49">ман ′ас kа′лʼеджан.</ta>
            <ta e="T55" id="Seg_700" s="T52">тан ′нʼилʼдʼзʼ(дʼ)ин ′паlдʼат?</ta>
            <ta e="T57" id="Seg_701" s="T55">′нейɣулам та′налтъ.</ta>
            <ta e="T61" id="Seg_702" s="T57">варкызан и о′kkыр козыр′ней ′тʼак(г̂)кус.</ta>
            <ta e="T65" id="Seg_703" s="T61">ма′нан ′содʼига козырла jезаттъ.</ta>
            <ta e="T67" id="Seg_704" s="T65">та′вай kы′банʼажам!</ta>
            <ta e="T69" id="Seg_705" s="T67">′таргу надо.</ta>
            <ta e="T70" id="Seg_706" s="T69">′тартаk.</ta>
            <ta e="T74" id="Seg_707" s="T70">ма′нан ′срод̂у ′тус тʼакку.</ta>
            <ta e="T78" id="Seg_708" s="T74">jас ту′ноу̹ kай′зʼе крыть.</ta>
            <ta e="T80" id="Seg_709" s="T78">Свет′лана kа′лын.</ta>
            <ta e="T83" id="Seg_710" s="T80">kу′ссӓн та′ннан ′kозыр?</ta>
            <ta e="T86" id="Seg_711" s="T83">тан ′игъ ′манны′паk!</ta>
            <ta e="T88" id="Seg_712" s="T86">хватит варку′гу!</ta>
            <ta e="T91" id="Seg_713" s="T88">д̂а′вай jе′шо варкуlутту.</ta>
            <ta e="T92" id="Seg_714" s="T91">ти(ы)лʼдʼӣн!</ta>
            <ta e="T93" id="Seg_715" s="T92">ти(ы)lдʼӣн!</ta>
            <ta e="T95" id="Seg_716" s="T93">о′llоу ′кӱзеджа.</ta>
            <ta e="T97" id="Seg_717" s="T95">′надъ ′те̨jе̨рбы(у)′гӯ.</ta>
            <ta e="T99" id="Seg_718" s="T97">ман ′те̨jербан.</ta>
            <ta e="T100" id="Seg_719" s="T99">′те̨jер′ба̄k.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_720" s="T1">tawaj kozɨrčuluttu.</ta>
            <ta e="T5" id="Seg_721" s="T3">nadə kozurtšugu.</ta>
            <ta e="T8" id="Seg_722" s="T5">d̂ama meka tattɨt!</ta>
            <ta e="T9" id="Seg_723" s="T8">paldʼak!</ta>
            <ta e="T10" id="Seg_724" s="T9">iːow.</ta>
            <ta e="T13" id="Seg_725" s="T10">oj qajlazʼe palden!</ta>
            <ta e="T14" id="Seg_726" s="T13">sʼelʼde.</ta>
            <ta e="T16" id="Seg_727" s="T14">aj selʼde.</ta>
            <ta e="T17" id="Seg_728" s="T16">metdɨ.</ta>
            <ta e="T19" id="Seg_729" s="T17">paldʼak mek(ĝ)ka!</ta>
            <ta e="T20" id="Seg_730" s="T19">muqtɨt.</ta>
            <ta e="T21" id="Seg_731" s="T20">alɨnɨn!</ta>
            <ta e="T22" id="Seg_732" s="T21">alɨnɨn!</ta>
            <ta e="T23" id="Seg_733" s="T22">tʼaĝgu!</ta>
            <ta e="T28" id="Seg_734" s="T23">qajno as mezal (qajnos mezal)?</ta>
            <ta e="T30" id="Seg_735" s="T28">man paldedan.</ta>
            <ta e="T32" id="Seg_736" s="T30">tep d̂ura.</ta>
            <ta e="T36" id="Seg_737" s="T32">tan lʼišnan iga(ə) ikʼət.</ta>
            <ta e="T39" id="Seg_738" s="T36">igə idərte, me(i)tdə!</ta>
            <ta e="T41" id="Seg_739" s="T39">kozɨrlam nadə.</ta>
            <ta e="T44" id="Seg_740" s="T41">idərugu ne nado.</ta>
            <ta e="T46" id="Seg_741" s="T44">man qalan.</ta>
            <ta e="T49" id="Seg_742" s="T46">sičas tan qalʼinaš.</ta>
            <ta e="T52" id="Seg_743" s="T49">man as qalʼeǯan.</ta>
            <ta e="T55" id="Seg_744" s="T52">tan nʼilʼdʼzʼ(dʼ)in paldʼat?</ta>
            <ta e="T57" id="Seg_745" s="T55">nejɣulam tanaltə.</ta>
            <ta e="T61" id="Seg_746" s="T57">warkɨzan i oqqɨr kozɨrnej tʼak(ĝ)kus.</ta>
            <ta e="T65" id="Seg_747" s="T61">manan sodʼiga kozɨrla jezattə.</ta>
            <ta e="T67" id="Seg_748" s="T65">tawaj qɨbanʼaʒam!</ta>
            <ta e="T69" id="Seg_749" s="T67">targu nado.</ta>
            <ta e="T70" id="Seg_750" s="T69">tartaq.</ta>
            <ta e="T74" id="Seg_751" s="T70">manan srod̂u tus tʼakku.</ta>
            <ta e="T78" id="Seg_752" s="T74">jas tunou̹ qajzʼe krɨtʼ.</ta>
            <ta e="T80" id="Seg_753" s="T78">Сwetlana qalɨn.</ta>
            <ta e="T83" id="Seg_754" s="T80">qussän tannan qozɨr?</ta>
            <ta e="T86" id="Seg_755" s="T83">tan igə mannɨpaq!</ta>
            <ta e="T88" id="Seg_756" s="T86">xwatit warkugu!</ta>
            <ta e="T91" id="Seg_757" s="T88">d̂awaj ješo warkuluttu.</ta>
            <ta e="T92" id="Seg_758" s="T91">ti(ɨ)lʼdʼiːn!</ta>
            <ta e="T93" id="Seg_759" s="T92">ti(ɨ)ldʼiːn!</ta>
            <ta e="T95" id="Seg_760" s="T93">ollou küzeǯa.</ta>
            <ta e="T97" id="Seg_761" s="T95">nadə tejerbɨ(u)guː.</ta>
            <ta e="T99" id="Seg_762" s="T97">man tejerban.</ta>
            <ta e="T100" id="Seg_763" s="T99">tejerbaːq.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_764" s="T1">Tawaj kozɨrčuluttu. </ta>
            <ta e="T5" id="Seg_765" s="T3">Nadə kozurčugu. </ta>
            <ta e="T8" id="Seg_766" s="T5">Dama meka tattɨt! </ta>
            <ta e="T9" id="Seg_767" s="T8">Paldʼak! </ta>
            <ta e="T10" id="Seg_768" s="T9">Iːow. </ta>
            <ta e="T13" id="Seg_769" s="T10">Oj qajlazʼe palden! </ta>
            <ta e="T14" id="Seg_770" s="T13">Sʼelʼde. </ta>
            <ta e="T16" id="Seg_771" s="T14">Aj selʼde. </ta>
            <ta e="T17" id="Seg_772" s="T16">Metdɨ. </ta>
            <ta e="T19" id="Seg_773" s="T17">Paldʼak mekka! </ta>
            <ta e="T20" id="Seg_774" s="T19">Muqtɨt. </ta>
            <ta e="T21" id="Seg_775" s="T20">Alɨnɨn! </ta>
            <ta e="T22" id="Seg_776" s="T21">Alɨnɨn! </ta>
            <ta e="T23" id="Seg_777" s="T22">Tʼaggu! </ta>
            <ta e="T28" id="Seg_778" s="T23">Qajno as mezal (qajnos mezal)? </ta>
            <ta e="T30" id="Seg_779" s="T28">Man paldedan. </ta>
            <ta e="T32" id="Seg_780" s="T30">Tep dura. </ta>
            <ta e="T36" id="Seg_781" s="T32">Tan lʼišnan igə ikʼət. </ta>
            <ta e="T39" id="Seg_782" s="T36">Igə idərte, metdə! </ta>
            <ta e="T41" id="Seg_783" s="T39">Kozɨrlam nadə. </ta>
            <ta e="T44" id="Seg_784" s="T41">Idərugu ne nado. </ta>
            <ta e="T46" id="Seg_785" s="T44">Man qalan. </ta>
            <ta e="T49" id="Seg_786" s="T46">Sičas tan qalʼinaš. </ta>
            <ta e="T52" id="Seg_787" s="T49">Man as qalʼeǯan. </ta>
            <ta e="T55" id="Seg_788" s="T52">Tan nʼilʼdʼzʼin paldʼat? </ta>
            <ta e="T57" id="Seg_789" s="T55">Nejɣulam tanaltə. </ta>
            <ta e="T61" id="Seg_790" s="T57">Warkɨzan oqqɨr kozɨrnej tʼakkus. </ta>
            <ta e="T65" id="Seg_791" s="T61">Manan sodʼiga kozɨrla jezattə. </ta>
            <ta e="T67" id="Seg_792" s="T65">Tawaj qɨbanʼaʒam! </ta>
            <ta e="T69" id="Seg_793" s="T67">Targu nado. </ta>
            <ta e="T70" id="Seg_794" s="T69">Tartaq. </ta>
            <ta e="T74" id="Seg_795" s="T70">Manan srodu tus tʼakku. </ta>
            <ta e="T78" id="Seg_796" s="T74">Jas tunou qajzʼe krɨtʼ. </ta>
            <ta e="T80" id="Seg_797" s="T78">Swetlana qalɨn. </ta>
            <ta e="T83" id="Seg_798" s="T80">Qussän tannan qozɨr? </ta>
            <ta e="T86" id="Seg_799" s="T83">Tan igə mannɨpaq! </ta>
            <ta e="T88" id="Seg_800" s="T86">Xwatit warkugu! </ta>
            <ta e="T91" id="Seg_801" s="T88">Dawaj ješo warkuluttu. </ta>
            <ta e="T92" id="Seg_802" s="T91">Tilʼdʼiːn! </ta>
            <ta e="T93" id="Seg_803" s="T92">Tildʼiːn! </ta>
            <ta e="T95" id="Seg_804" s="T93">Ollou küzeǯa. </ta>
            <ta e="T97" id="Seg_805" s="T95">Nadə tejerbɨguː. </ta>
            <ta e="T99" id="Seg_806" s="T97">Man tejerban. </ta>
            <ta e="T100" id="Seg_807" s="T99">Tejerbaːq. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_808" s="T1">tawaj</ta>
            <ta e="T3" id="Seg_809" s="T2">kozɨr-ču-lu-ttu</ta>
            <ta e="T4" id="Seg_810" s="T3">nadə</ta>
            <ta e="T5" id="Seg_811" s="T4">kozur-ču-gu</ta>
            <ta e="T6" id="Seg_812" s="T5">dama</ta>
            <ta e="T7" id="Seg_813" s="T6">meka</ta>
            <ta e="T8" id="Seg_814" s="T7">tat-tɨ-t</ta>
            <ta e="T9" id="Seg_815" s="T8">paldʼa-k</ta>
            <ta e="T10" id="Seg_816" s="T9">iː-o-w</ta>
            <ta e="T11" id="Seg_817" s="T10">oj</ta>
            <ta e="T12" id="Seg_818" s="T11">qaj-la-zʼe</ta>
            <ta e="T13" id="Seg_819" s="T12">palde-n</ta>
            <ta e="T14" id="Seg_820" s="T13">sʼelʼde</ta>
            <ta e="T15" id="Seg_821" s="T14">aj</ta>
            <ta e="T16" id="Seg_822" s="T15">selʼde</ta>
            <ta e="T17" id="Seg_823" s="T16">me-tdɨ</ta>
            <ta e="T18" id="Seg_824" s="T17">paldʼa-k</ta>
            <ta e="T19" id="Seg_825" s="T18">mekka</ta>
            <ta e="T20" id="Seg_826" s="T19">muqtɨt</ta>
            <ta e="T21" id="Seg_827" s="T20">alɨ-nɨ-n</ta>
            <ta e="T22" id="Seg_828" s="T21">alɨ-nɨ-n</ta>
            <ta e="T23" id="Seg_829" s="T22">tʼaggu</ta>
            <ta e="T24" id="Seg_830" s="T23">qaj-no</ta>
            <ta e="T25" id="Seg_831" s="T24">as</ta>
            <ta e="T26" id="Seg_832" s="T25">me-za-l</ta>
            <ta e="T29" id="Seg_833" s="T28">man</ta>
            <ta e="T30" id="Seg_834" s="T29">palde-da-n</ta>
            <ta e="T31" id="Seg_835" s="T30">tep</ta>
            <ta e="T32" id="Seg_836" s="T31">dura</ta>
            <ta e="T33" id="Seg_837" s="T32">tat</ta>
            <ta e="T34" id="Seg_838" s="T33">lʼišna-n</ta>
            <ta e="T35" id="Seg_839" s="T34">igə</ta>
            <ta e="T36" id="Seg_840" s="T35">i-kʼ-ət</ta>
            <ta e="T37" id="Seg_841" s="T36">igə</ta>
            <ta e="T38" id="Seg_842" s="T37">idər-te</ta>
            <ta e="T39" id="Seg_843" s="T38">me-tdə</ta>
            <ta e="T40" id="Seg_844" s="T39">kozɨr-la-m</ta>
            <ta e="T41" id="Seg_845" s="T40">nadə</ta>
            <ta e="T42" id="Seg_846" s="T41">idəru-gu</ta>
            <ta e="T43" id="Seg_847" s="T42">ne</ta>
            <ta e="T44" id="Seg_848" s="T43">nado</ta>
            <ta e="T45" id="Seg_849" s="T44">man</ta>
            <ta e="T46" id="Seg_850" s="T45">qala-n</ta>
            <ta e="T47" id="Seg_851" s="T46">sičas</ta>
            <ta e="T48" id="Seg_852" s="T47">tat</ta>
            <ta e="T49" id="Seg_853" s="T48">qalʼi-naš</ta>
            <ta e="T50" id="Seg_854" s="T49">man</ta>
            <ta e="T51" id="Seg_855" s="T50">as</ta>
            <ta e="T52" id="Seg_856" s="T51">qalʼ-eǯa-n</ta>
            <ta e="T53" id="Seg_857" s="T52">tat</ta>
            <ta e="T54" id="Seg_858" s="T53">nʼilʼdʼzʼi-n</ta>
            <ta e="T55" id="Seg_859" s="T54">paldʼa-t</ta>
            <ta e="T56" id="Seg_860" s="T55">ne-j-ɣu-la-m</ta>
            <ta e="T57" id="Seg_861" s="T56">ta-naltə</ta>
            <ta e="T58" id="Seg_862" s="T57">warkɨ-za-n</ta>
            <ta e="T59" id="Seg_863" s="T58">oqqɨr</ta>
            <ta e="T60" id="Seg_864" s="T59">kozɨr-nej</ta>
            <ta e="T61" id="Seg_865" s="T60">tʼakku-s</ta>
            <ta e="T62" id="Seg_866" s="T61">ma-nan</ta>
            <ta e="T63" id="Seg_867" s="T62">sodʼiga</ta>
            <ta e="T64" id="Seg_868" s="T63">kozɨr-la</ta>
            <ta e="T65" id="Seg_869" s="T64">je-za-ttə</ta>
            <ta e="T66" id="Seg_870" s="T65">tawaj</ta>
            <ta e="T67" id="Seg_871" s="T66">qɨbanʼaʒa-m</ta>
            <ta e="T68" id="Seg_872" s="T67">tar-gu</ta>
            <ta e="T69" id="Seg_873" s="T68">nado</ta>
            <ta e="T70" id="Seg_874" s="T69">tar-ta-q</ta>
            <ta e="T71" id="Seg_875" s="T70">ma-nan</ta>
            <ta e="T72" id="Seg_876" s="T71">srodu</ta>
            <ta e="T73" id="Seg_877" s="T72">tus</ta>
            <ta e="T74" id="Seg_878" s="T73">tʼakku</ta>
            <ta e="T75" id="Seg_879" s="T74">jas</ta>
            <ta e="T76" id="Seg_880" s="T75">tuno-u</ta>
            <ta e="T77" id="Seg_881" s="T76">qaj-zʼe</ta>
            <ta e="T79" id="Seg_882" s="T78">Swetlana</ta>
            <ta e="T80" id="Seg_883" s="T79">qalɨ-n</ta>
            <ta e="T81" id="Seg_884" s="T80">qussän</ta>
            <ta e="T82" id="Seg_885" s="T81">tan-nan</ta>
            <ta e="T83" id="Seg_886" s="T82">qozɨr</ta>
            <ta e="T84" id="Seg_887" s="T83">tat</ta>
            <ta e="T85" id="Seg_888" s="T84">igə</ta>
            <ta e="T86" id="Seg_889" s="T85">mannɨ-pa-q</ta>
            <ta e="T87" id="Seg_890" s="T86">xwatit</ta>
            <ta e="T88" id="Seg_891" s="T87">warku-gu</ta>
            <ta e="T89" id="Seg_892" s="T88">dawaj</ta>
            <ta e="T90" id="Seg_893" s="T89">ješo</ta>
            <ta e="T91" id="Seg_894" s="T90">warku-lu-ttu</ta>
            <ta e="T92" id="Seg_895" s="T91">tilʼdʼiː-n</ta>
            <ta e="T93" id="Seg_896" s="T92">tildʼiː-n</ta>
            <ta e="T94" id="Seg_897" s="T93">ollo-u</ta>
            <ta e="T95" id="Seg_898" s="T94">küz-eǯa</ta>
            <ta e="T96" id="Seg_899" s="T95">nadə</ta>
            <ta e="T97" id="Seg_900" s="T96">tejerbɨ-guː</ta>
            <ta e="T98" id="Seg_901" s="T97">man</ta>
            <ta e="T99" id="Seg_902" s="T98">tejerba-n</ta>
            <ta e="T100" id="Seg_903" s="T99">tejerbaː-q</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_904" s="T1">dawaj</ta>
            <ta e="T3" id="Seg_905" s="T2">kozɨr-ču-lä-un</ta>
            <ta e="T4" id="Seg_906" s="T3">nadə</ta>
            <ta e="T5" id="Seg_907" s="T4">kozɨr-ču-gu</ta>
            <ta e="T6" id="Seg_908" s="T5">dama</ta>
            <ta e="T7" id="Seg_909" s="T6">mekka</ta>
            <ta e="T8" id="Seg_910" s="T7">tat-ntɨ-t</ta>
            <ta e="T9" id="Seg_911" s="T8">palʼdʼi-kɨ</ta>
            <ta e="T10" id="Seg_912" s="T9">iː-nɨ-w</ta>
            <ta e="T11" id="Seg_913" s="T10">oj</ta>
            <ta e="T12" id="Seg_914" s="T11">qaj-la-se</ta>
            <ta e="T13" id="Seg_915" s="T12">palʼdʼi-n</ta>
            <ta e="T14" id="Seg_916" s="T13">selʼdʼi</ta>
            <ta e="T15" id="Seg_917" s="T14">aj</ta>
            <ta e="T16" id="Seg_918" s="T15">selʼdʼi</ta>
            <ta e="T17" id="Seg_919" s="T16">me-etɨ</ta>
            <ta e="T18" id="Seg_920" s="T17">palʼdʼi-kɨ</ta>
            <ta e="T19" id="Seg_921" s="T18">mekka</ta>
            <ta e="T20" id="Seg_922" s="T19">muqtut</ta>
            <ta e="T21" id="Seg_923" s="T20">aːlu-nɨ-n</ta>
            <ta e="T22" id="Seg_924" s="T21">aːlu-nɨ-n</ta>
            <ta e="T23" id="Seg_925" s="T22">tʼäkku</ta>
            <ta e="T24" id="Seg_926" s="T23">qaj-no</ta>
            <ta e="T25" id="Seg_927" s="T24">asa</ta>
            <ta e="T26" id="Seg_928" s="T25">me-sɨ-l</ta>
            <ta e="T29" id="Seg_929" s="T28">man</ta>
            <ta e="T30" id="Seg_930" s="T29">palʼdʼi-ntɨ-ŋ</ta>
            <ta e="T31" id="Seg_931" s="T30">täp</ta>
            <ta e="T32" id="Seg_932" s="T31">dura</ta>
            <ta e="T33" id="Seg_933" s="T32">tan</ta>
            <ta e="T34" id="Seg_934" s="T33">lʼišna-n</ta>
            <ta e="T35" id="Seg_935" s="T34">igə</ta>
            <ta e="T36" id="Seg_936" s="T35">iː-ku-etɨ</ta>
            <ta e="T37" id="Seg_937" s="T36">igə</ta>
            <ta e="T38" id="Seg_938" s="T37">edər-etɨ</ta>
            <ta e="T39" id="Seg_939" s="T38">me-etɨ</ta>
            <ta e="T40" id="Seg_940" s="T39">kozɨr-la-m</ta>
            <ta e="T41" id="Seg_941" s="T40">nadə</ta>
            <ta e="T42" id="Seg_942" s="T41">edər-gu</ta>
            <ta e="T43" id="Seg_943" s="T42">ne</ta>
            <ta e="T44" id="Seg_944" s="T43">nadə</ta>
            <ta e="T45" id="Seg_945" s="T44">man</ta>
            <ta e="T46" id="Seg_946" s="T45">qalɨ-ŋ</ta>
            <ta e="T47" id="Seg_947" s="T46">sičas</ta>
            <ta e="T48" id="Seg_948" s="T47">tan</ta>
            <ta e="T49" id="Seg_949" s="T48">qalɨ-naš</ta>
            <ta e="T50" id="Seg_950" s="T49">man</ta>
            <ta e="T51" id="Seg_951" s="T50">asa</ta>
            <ta e="T52" id="Seg_952" s="T51">qalɨ-enǯɨ-ŋ</ta>
            <ta e="T53" id="Seg_953" s="T52">tan</ta>
            <ta e="T54" id="Seg_954" s="T53">nʼilʼdʼi-ŋ</ta>
            <ta e="T55" id="Seg_955" s="T54">palʼdʼi-ntə</ta>
            <ta e="T56" id="Seg_956" s="T55">ne-lʼ-qum-la-m</ta>
            <ta e="T57" id="Seg_957" s="T56">tat-naltə</ta>
            <ta e="T58" id="Seg_958" s="T57">warkɨ-sɨ-ŋ</ta>
            <ta e="T59" id="Seg_959" s="T58">okkɨr</ta>
            <ta e="T60" id="Seg_960" s="T59">kozɨr-näj</ta>
            <ta e="T61" id="Seg_961" s="T60">tʼäkku-sɨ</ta>
            <ta e="T62" id="Seg_962" s="T61">man-nan</ta>
            <ta e="T63" id="Seg_963" s="T62">soːdʼiga</ta>
            <ta e="T64" id="Seg_964" s="T63">kozɨr-la</ta>
            <ta e="T65" id="Seg_965" s="T64">eː-sɨ-tɨn</ta>
            <ta e="T66" id="Seg_966" s="T65">dawaj</ta>
            <ta e="T67" id="Seg_967" s="T66">qɨbanʼaǯa-m</ta>
            <ta e="T68" id="Seg_968" s="T67">tar-gu</ta>
            <ta e="T69" id="Seg_969" s="T68">nadə</ta>
            <ta e="T70" id="Seg_970" s="T69">tar-tɨ-kɨ</ta>
            <ta e="T71" id="Seg_971" s="T70">man-nan</ta>
            <ta e="T72" id="Seg_972" s="T71">srodu</ta>
            <ta e="T73" id="Seg_973" s="T72">tus</ta>
            <ta e="T74" id="Seg_974" s="T73">tʼäkku</ta>
            <ta e="T75" id="Seg_975" s="T74">asa</ta>
            <ta e="T76" id="Seg_976" s="T75">tonu-w</ta>
            <ta e="T77" id="Seg_977" s="T76">qaj-se</ta>
            <ta e="T79" id="Seg_978" s="T78">Svetlana</ta>
            <ta e="T80" id="Seg_979" s="T79">qalɨ-n</ta>
            <ta e="T81" id="Seg_980" s="T80">qussän</ta>
            <ta e="T82" id="Seg_981" s="T81">tan-nan</ta>
            <ta e="T83" id="Seg_982" s="T82">kozɨr</ta>
            <ta e="T84" id="Seg_983" s="T83">tan</ta>
            <ta e="T85" id="Seg_984" s="T84">igə</ta>
            <ta e="T86" id="Seg_985" s="T85">*mantɨ-mbɨ-kɨ</ta>
            <ta e="T87" id="Seg_986" s="T86">xwatit</ta>
            <ta e="T88" id="Seg_987" s="T87">warkɨ-gu</ta>
            <ta e="T89" id="Seg_988" s="T88">dawaj</ta>
            <ta e="T90" id="Seg_989" s="T89">ešo</ta>
            <ta e="T91" id="Seg_990" s="T90">warkɨ-lä-un</ta>
            <ta e="T92" id="Seg_991" s="T91">tilʼdʼi-ŋ</ta>
            <ta e="T93" id="Seg_992" s="T92">tilʼdʼi-ŋ</ta>
            <ta e="T94" id="Seg_993" s="T93">olə-w</ta>
            <ta e="T95" id="Seg_994" s="T94">küzɨ-enǯɨ</ta>
            <ta e="T96" id="Seg_995" s="T95">nadə</ta>
            <ta e="T97" id="Seg_996" s="T96">tärba-gu</ta>
            <ta e="T98" id="Seg_997" s="T97">man</ta>
            <ta e="T99" id="Seg_998" s="T98">tärba-n</ta>
            <ta e="T100" id="Seg_999" s="T99">tärba-kɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1000" s="T1">HORT</ta>
            <ta e="T3" id="Seg_1001" s="T2">card-VBLZ-OPT-1PL</ta>
            <ta e="T4" id="Seg_1002" s="T3">one.should</ta>
            <ta e="T5" id="Seg_1003" s="T4">card-VBLZ-INF</ta>
            <ta e="T6" id="Seg_1004" s="T5">queen.[NOM]</ta>
            <ta e="T7" id="Seg_1005" s="T6">I.ALL</ta>
            <ta e="T8" id="Seg_1006" s="T7">give-INFER-3SG.O</ta>
            <ta e="T9" id="Seg_1007" s="T8">walk-IMP.2SG.S</ta>
            <ta e="T10" id="Seg_1008" s="T9">take-CO-1SG.O</ta>
            <ta e="T11" id="Seg_1009" s="T10">oh</ta>
            <ta e="T12" id="Seg_1010" s="T11">what-PL-INSTR</ta>
            <ta e="T13" id="Seg_1011" s="T12">walk-3SG.S</ta>
            <ta e="T14" id="Seg_1012" s="T13">seven.[NOM]</ta>
            <ta e="T15" id="Seg_1013" s="T14">also</ta>
            <ta e="T16" id="Seg_1014" s="T15">seven.[NOM]</ta>
            <ta e="T17" id="Seg_1015" s="T16">give-IMP.2SG.O</ta>
            <ta e="T18" id="Seg_1016" s="T17">walk-IMP.2SG.S</ta>
            <ta e="T19" id="Seg_1017" s="T18">I.ALL</ta>
            <ta e="T20" id="Seg_1018" s="T19">six.[NOM]</ta>
            <ta e="T21" id="Seg_1019" s="T20">cheat-CO-3SG.S</ta>
            <ta e="T22" id="Seg_1020" s="T21">cheat-CO-3SG.S</ta>
            <ta e="T23" id="Seg_1021" s="T22">NEG.EX.[3SG.S]</ta>
            <ta e="T24" id="Seg_1022" s="T23">what-TRL</ta>
            <ta e="T25" id="Seg_1023" s="T24">NEG</ta>
            <ta e="T26" id="Seg_1024" s="T25">give-PST-2SG.O</ta>
            <ta e="T29" id="Seg_1025" s="T28">I.NOM</ta>
            <ta e="T30" id="Seg_1026" s="T29">walk-INFER-1SG.S</ta>
            <ta e="T31" id="Seg_1027" s="T30">(s)he.[NOM]</ta>
            <ta e="T32" id="Seg_1028" s="T31">fool.[3SG.S]</ta>
            <ta e="T33" id="Seg_1029" s="T32">you.SG.NOM</ta>
            <ta e="T34" id="Seg_1030" s="T33">redundant-GEN</ta>
            <ta e="T35" id="Seg_1031" s="T34">NEG.IMP</ta>
            <ta e="T36" id="Seg_1032" s="T35">take-HAB-IMP.2SG.O</ta>
            <ta e="T37" id="Seg_1033" s="T36">NEG.IMP</ta>
            <ta e="T38" id="Seg_1034" s="T37">feel.sorry-IMP.2SG.O</ta>
            <ta e="T39" id="Seg_1035" s="T38">give-IMP.2SG.O</ta>
            <ta e="T40" id="Seg_1036" s="T39">trump.card-PL-ACC</ta>
            <ta e="T41" id="Seg_1037" s="T40">one.should</ta>
            <ta e="T42" id="Seg_1038" s="T41">feel.sorry-INF</ta>
            <ta e="T43" id="Seg_1039" s="T42">NEG</ta>
            <ta e="T44" id="Seg_1040" s="T43">one.should</ta>
            <ta e="T45" id="Seg_1041" s="T44">I.NOM</ta>
            <ta e="T46" id="Seg_1042" s="T45">stay-1SG.S</ta>
            <ta e="T47" id="Seg_1043" s="T46">now</ta>
            <ta e="T48" id="Seg_1044" s="T47">you.SG.NOM</ta>
            <ta e="T49" id="Seg_1045" s="T48">stay-POT.FUT.2SG</ta>
            <ta e="T50" id="Seg_1046" s="T49">I.NOM</ta>
            <ta e="T51" id="Seg_1047" s="T50">NEG</ta>
            <ta e="T52" id="Seg_1048" s="T51">stay-FUT-1SG.S</ta>
            <ta e="T53" id="Seg_1049" s="T52">you.SG.NOM</ta>
            <ta e="T54" id="Seg_1050" s="T53">such-ADVZ</ta>
            <ta e="T55" id="Seg_1051" s="T54">walk-2SG.S</ta>
            <ta e="T56" id="Seg_1052" s="T55">woman-ADJZ-human.being-PL-ACC</ta>
            <ta e="T57" id="Seg_1053" s="T56">give-IMP.2PL.S/O</ta>
            <ta e="T58" id="Seg_1054" s="T57">%play-PST-1SG.S</ta>
            <ta e="T59" id="Seg_1055" s="T58">one</ta>
            <ta e="T60" id="Seg_1056" s="T59">trump.card.[NOM]-EMPH</ta>
            <ta e="T61" id="Seg_1057" s="T60">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T62" id="Seg_1058" s="T61">I-ADES</ta>
            <ta e="T63" id="Seg_1059" s="T62">good</ta>
            <ta e="T64" id="Seg_1060" s="T63">card-PL.[NOM]</ta>
            <ta e="T65" id="Seg_1061" s="T64">be-PST-3PL</ta>
            <ta e="T66" id="Seg_1062" s="T65">give.IMP.2SG</ta>
            <ta e="T67" id="Seg_1063" s="T66">child-ACC</ta>
            <ta e="T68" id="Seg_1064" s="T67">share-INF</ta>
            <ta e="T69" id="Seg_1065" s="T68">one.should</ta>
            <ta e="T70" id="Seg_1066" s="T69">share-TR-IMP.2SG.S</ta>
            <ta e="T71" id="Seg_1067" s="T70">I-ADES</ta>
            <ta e="T72" id="Seg_1068" s="T71">never</ta>
            <ta e="T73" id="Seg_1069" s="T72">ace.[NOM]</ta>
            <ta e="T74" id="Seg_1070" s="T73">NEG.EX.[3SG.S]</ta>
            <ta e="T75" id="Seg_1071" s="T74">NEG</ta>
            <ta e="T76" id="Seg_1072" s="T75">know-1SG.O</ta>
            <ta e="T77" id="Seg_1073" s="T76">what-INSTR</ta>
            <ta e="T79" id="Seg_1074" s="T78">Svetlana.[NOM]</ta>
            <ta e="T80" id="Seg_1075" s="T79">stay-3SG.S</ta>
            <ta e="T81" id="Seg_1076" s="T80">how.many</ta>
            <ta e="T82" id="Seg_1077" s="T81">you.SG-ADES</ta>
            <ta e="T83" id="Seg_1078" s="T82">card.[NOM]</ta>
            <ta e="T84" id="Seg_1079" s="T83">you.SG.NOM</ta>
            <ta e="T85" id="Seg_1080" s="T84">NEG.IMP</ta>
            <ta e="T86" id="Seg_1081" s="T85">look-DUR-IMP.2SG.S</ta>
            <ta e="T87" id="Seg_1082" s="T86">enough</ta>
            <ta e="T88" id="Seg_1083" s="T87">%play-INF</ta>
            <ta e="T89" id="Seg_1084" s="T88">HORT</ta>
            <ta e="T90" id="Seg_1085" s="T89">more</ta>
            <ta e="T91" id="Seg_1086" s="T90">%play-OPT-1PL</ta>
            <ta e="T92" id="Seg_1087" s="T91">such-ADVZ</ta>
            <ta e="T93" id="Seg_1088" s="T92">such-ADVZ</ta>
            <ta e="T94" id="Seg_1089" s="T93">head.[NOM]-1SG</ta>
            <ta e="T95" id="Seg_1090" s="T94">hurt-FUT.[3SG.S]</ta>
            <ta e="T96" id="Seg_1091" s="T95">one.should</ta>
            <ta e="T97" id="Seg_1092" s="T96">think-INF</ta>
            <ta e="T98" id="Seg_1093" s="T97">I.NOM</ta>
            <ta e="T99" id="Seg_1094" s="T98">think-3SG.S</ta>
            <ta e="T100" id="Seg_1095" s="T99">think-IMP.2SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1096" s="T1">HORT</ta>
            <ta e="T3" id="Seg_1097" s="T2">карта-VBLZ-OPT-1PL</ta>
            <ta e="T4" id="Seg_1098" s="T3">надо</ta>
            <ta e="T5" id="Seg_1099" s="T4">карта-VBLZ-INF</ta>
            <ta e="T6" id="Seg_1100" s="T5">дама.[NOM]</ta>
            <ta e="T7" id="Seg_1101" s="T6">я.ALL</ta>
            <ta e="T8" id="Seg_1102" s="T7">подать-INFER-3SG.O</ta>
            <ta e="T9" id="Seg_1103" s="T8">ходить-IMP.2SG.S</ta>
            <ta e="T10" id="Seg_1104" s="T9">взять-CO-1SG.O</ta>
            <ta e="T11" id="Seg_1105" s="T10">ой</ta>
            <ta e="T12" id="Seg_1106" s="T11">что-PL-INSTR</ta>
            <ta e="T13" id="Seg_1107" s="T12">ходить-3SG.S</ta>
            <ta e="T14" id="Seg_1108" s="T13">семь.[NOM]</ta>
            <ta e="T15" id="Seg_1109" s="T14">тоже</ta>
            <ta e="T16" id="Seg_1110" s="T15">семь.[NOM]</ta>
            <ta e="T17" id="Seg_1111" s="T16">дать-IMP.2SG.O</ta>
            <ta e="T18" id="Seg_1112" s="T17">ходить-IMP.2SG.S</ta>
            <ta e="T19" id="Seg_1113" s="T18">я.ALL</ta>
            <ta e="T20" id="Seg_1114" s="T19">шесть.[NOM]</ta>
            <ta e="T21" id="Seg_1115" s="T20">обмануть-CO-3SG.S</ta>
            <ta e="T22" id="Seg_1116" s="T21">обмануть-CO-3SG.S</ta>
            <ta e="T23" id="Seg_1117" s="T22">NEG.EX.[3SG.S]</ta>
            <ta e="T24" id="Seg_1118" s="T23">что-TRL</ta>
            <ta e="T25" id="Seg_1119" s="T24">NEG</ta>
            <ta e="T26" id="Seg_1120" s="T25">дать-PST-2SG.O</ta>
            <ta e="T29" id="Seg_1121" s="T28">я.NOM</ta>
            <ta e="T30" id="Seg_1122" s="T29">ходить-INFER-1SG.S</ta>
            <ta e="T31" id="Seg_1123" s="T30">он(а).[NOM]</ta>
            <ta e="T32" id="Seg_1124" s="T31">дура.[3SG.S]</ta>
            <ta e="T33" id="Seg_1125" s="T32">ты.NOM</ta>
            <ta e="T34" id="Seg_1126" s="T33">лишний-GEN</ta>
            <ta e="T35" id="Seg_1127" s="T34">NEG.IMP</ta>
            <ta e="T36" id="Seg_1128" s="T35">взять-HAB-IMP.2SG.O</ta>
            <ta e="T37" id="Seg_1129" s="T36">NEG.IMP</ta>
            <ta e="T38" id="Seg_1130" s="T37">пожалеть-IMP.2SG.O</ta>
            <ta e="T39" id="Seg_1131" s="T38">дать-IMP.2SG.O</ta>
            <ta e="T40" id="Seg_1132" s="T39">козырь-PL-ACC</ta>
            <ta e="T41" id="Seg_1133" s="T40">надо</ta>
            <ta e="T42" id="Seg_1134" s="T41">пожалеть-INF</ta>
            <ta e="T43" id="Seg_1135" s="T42">NEG</ta>
            <ta e="T44" id="Seg_1136" s="T43">надо</ta>
            <ta e="T45" id="Seg_1137" s="T44">я.NOM</ta>
            <ta e="T46" id="Seg_1138" s="T45">остаться-1SG.S</ta>
            <ta e="T47" id="Seg_1139" s="T46">сейчас</ta>
            <ta e="T48" id="Seg_1140" s="T47">ты.NOM</ta>
            <ta e="T49" id="Seg_1141" s="T48">остаться-POT.FUT.2SG</ta>
            <ta e="T50" id="Seg_1142" s="T49">я.NOM</ta>
            <ta e="T51" id="Seg_1143" s="T50">NEG</ta>
            <ta e="T52" id="Seg_1144" s="T51">остаться-FUT-1SG.S</ta>
            <ta e="T53" id="Seg_1145" s="T52">ты.NOM</ta>
            <ta e="T54" id="Seg_1146" s="T53">такой-ADVZ</ta>
            <ta e="T55" id="Seg_1147" s="T54">ходить-2SG.S</ta>
            <ta e="T56" id="Seg_1148" s="T55">женщина-ADJZ-человек-PL-ACC</ta>
            <ta e="T57" id="Seg_1149" s="T56">подать-IMP.2PL.S/O</ta>
            <ta e="T58" id="Seg_1150" s="T57">%играть-PST-1SG.S</ta>
            <ta e="T59" id="Seg_1151" s="T58">один</ta>
            <ta e="T60" id="Seg_1152" s="T59">козырь.[NOM]-EMPH</ta>
            <ta e="T61" id="Seg_1153" s="T60">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T62" id="Seg_1154" s="T61">я-ADES</ta>
            <ta e="T63" id="Seg_1155" s="T62">хороший</ta>
            <ta e="T64" id="Seg_1156" s="T63">карта-PL.[NOM]</ta>
            <ta e="T65" id="Seg_1157" s="T64">быть-PST-3PL</ta>
            <ta e="T66" id="Seg_1158" s="T65">давать.IMP.2SG</ta>
            <ta e="T67" id="Seg_1159" s="T66">ребенок-ACC</ta>
            <ta e="T68" id="Seg_1160" s="T67">поделить-INF</ta>
            <ta e="T69" id="Seg_1161" s="T68">надо</ta>
            <ta e="T70" id="Seg_1162" s="T69">поделить-TR-IMP.2SG.S</ta>
            <ta e="T71" id="Seg_1163" s="T70">я-ADES</ta>
            <ta e="T72" id="Seg_1164" s="T71">сроду</ta>
            <ta e="T73" id="Seg_1165" s="T72">туз.[NOM]</ta>
            <ta e="T74" id="Seg_1166" s="T73">NEG.EX.[3SG.S]</ta>
            <ta e="T75" id="Seg_1167" s="T74">NEG</ta>
            <ta e="T76" id="Seg_1168" s="T75">знать-1SG.O</ta>
            <ta e="T77" id="Seg_1169" s="T76">что-INSTR</ta>
            <ta e="T79" id="Seg_1170" s="T78">Светлана.[NOM]</ta>
            <ta e="T80" id="Seg_1171" s="T79">остаться-3SG.S</ta>
            <ta e="T81" id="Seg_1172" s="T80">сколько</ta>
            <ta e="T82" id="Seg_1173" s="T81">ты-ADES</ta>
            <ta e="T83" id="Seg_1174" s="T82">карта.[NOM]</ta>
            <ta e="T84" id="Seg_1175" s="T83">ты.NOM</ta>
            <ta e="T85" id="Seg_1176" s="T84">NEG.IMP</ta>
            <ta e="T86" id="Seg_1177" s="T85">посмотреть-DUR-IMP.2SG.S</ta>
            <ta e="T87" id="Seg_1178" s="T86">хватит</ta>
            <ta e="T88" id="Seg_1179" s="T87">%играть-INF</ta>
            <ta e="T89" id="Seg_1180" s="T88">HORT</ta>
            <ta e="T90" id="Seg_1181" s="T89">еще</ta>
            <ta e="T91" id="Seg_1182" s="T90">%играть-OPT-1PL</ta>
            <ta e="T92" id="Seg_1183" s="T91">такой-ADVZ</ta>
            <ta e="T93" id="Seg_1184" s="T92">такой-ADVZ</ta>
            <ta e="T94" id="Seg_1185" s="T93">голова.[NOM]-1SG</ta>
            <ta e="T95" id="Seg_1186" s="T94">болеть-FUT.[3SG.S]</ta>
            <ta e="T96" id="Seg_1187" s="T95">надо</ta>
            <ta e="T97" id="Seg_1188" s="T96">думать-INF</ta>
            <ta e="T98" id="Seg_1189" s="T97">я.NOM</ta>
            <ta e="T99" id="Seg_1190" s="T98">думать-3SG.S</ta>
            <ta e="T100" id="Seg_1191" s="T99">думать-IMP.2SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1192" s="T1">ptcl</ta>
            <ta e="T3" id="Seg_1193" s="T2">n-n&gt;v-v:mood-v:pn</ta>
            <ta e="T4" id="Seg_1194" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_1195" s="T4">n-n&gt;v-v:inf</ta>
            <ta e="T6" id="Seg_1196" s="T5">n.[n:case]</ta>
            <ta e="T7" id="Seg_1197" s="T6">pers</ta>
            <ta e="T8" id="Seg_1198" s="T7">v-v:mood-v:pn</ta>
            <ta e="T9" id="Seg_1199" s="T8">v-v:mood.pn</ta>
            <ta e="T10" id="Seg_1200" s="T9">v-v:ins-v:pn</ta>
            <ta e="T11" id="Seg_1201" s="T10">interj</ta>
            <ta e="T12" id="Seg_1202" s="T11">interrog-n:num-n:case</ta>
            <ta e="T13" id="Seg_1203" s="T12">v-v:pn</ta>
            <ta e="T14" id="Seg_1204" s="T13">num.[n:case]</ta>
            <ta e="T15" id="Seg_1205" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_1206" s="T15">num.[n:case]</ta>
            <ta e="T17" id="Seg_1207" s="T16">v-v:mood.pn</ta>
            <ta e="T18" id="Seg_1208" s="T17">v-v:mood.pn</ta>
            <ta e="T19" id="Seg_1209" s="T18">pers</ta>
            <ta e="T20" id="Seg_1210" s="T19">num.[n:case]</ta>
            <ta e="T21" id="Seg_1211" s="T20">v-v:ins-v:pn</ta>
            <ta e="T22" id="Seg_1212" s="T21">v-v:ins-v:pn</ta>
            <ta e="T23" id="Seg_1213" s="T22">v.[v:pn]</ta>
            <ta e="T24" id="Seg_1214" s="T23">interrog-n:case</ta>
            <ta e="T25" id="Seg_1215" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_1216" s="T25">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_1217" s="T28">pers</ta>
            <ta e="T30" id="Seg_1218" s="T29">v-v:mood-v:pn</ta>
            <ta e="T31" id="Seg_1219" s="T30">pers.[n:case]</ta>
            <ta e="T32" id="Seg_1220" s="T31">n.[v:pn]</ta>
            <ta e="T33" id="Seg_1221" s="T32">pers</ta>
            <ta e="T34" id="Seg_1222" s="T33">adj-n:case</ta>
            <ta e="T35" id="Seg_1223" s="T34">ptcl</ta>
            <ta e="T36" id="Seg_1224" s="T35">v-v&gt;v-v:mood.pn</ta>
            <ta e="T37" id="Seg_1225" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_1226" s="T37">v-v:mood.pn</ta>
            <ta e="T39" id="Seg_1227" s="T38">v-v:mood.pn</ta>
            <ta e="T40" id="Seg_1228" s="T39">n-n:num-n:case</ta>
            <ta e="T41" id="Seg_1229" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_1230" s="T41">v-v:inf</ta>
            <ta e="T43" id="Seg_1231" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_1232" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_1233" s="T44">pers</ta>
            <ta e="T46" id="Seg_1234" s="T45">v-v:pn</ta>
            <ta e="T47" id="Seg_1235" s="T46">adv</ta>
            <ta e="T48" id="Seg_1236" s="T47">pers</ta>
            <ta e="T49" id="Seg_1237" s="T48">v-v:tense</ta>
            <ta e="T50" id="Seg_1238" s="T49">pers</ta>
            <ta e="T51" id="Seg_1239" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_1240" s="T51">v-v:tense-v:pn</ta>
            <ta e="T53" id="Seg_1241" s="T52">pers</ta>
            <ta e="T54" id="Seg_1242" s="T53">adj-adj&gt;adv</ta>
            <ta e="T55" id="Seg_1243" s="T54">v-v:pn</ta>
            <ta e="T56" id="Seg_1244" s="T55">n-n&gt;adj-n-n:num-n:case</ta>
            <ta e="T57" id="Seg_1245" s="T56">v-v:mood.pn</ta>
            <ta e="T58" id="Seg_1246" s="T57">v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_1247" s="T58">num</ta>
            <ta e="T60" id="Seg_1248" s="T59">n.[n:case]-clit</ta>
            <ta e="T61" id="Seg_1249" s="T60">v-v:tense.[v:pn]</ta>
            <ta e="T62" id="Seg_1250" s="T61">pers-n:case</ta>
            <ta e="T63" id="Seg_1251" s="T62">adj</ta>
            <ta e="T64" id="Seg_1252" s="T63">n-n:num.[n:case]</ta>
            <ta e="T65" id="Seg_1253" s="T64">v-v:tense-v:pn</ta>
            <ta e="T66" id="Seg_1254" s="T65">v</ta>
            <ta e="T67" id="Seg_1255" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_1256" s="T67">v-v:inf</ta>
            <ta e="T69" id="Seg_1257" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_1258" s="T69">v-v&gt;v-v:mood.pn</ta>
            <ta e="T71" id="Seg_1259" s="T70">pers-n:case</ta>
            <ta e="T72" id="Seg_1260" s="T71">adv</ta>
            <ta e="T73" id="Seg_1261" s="T72">n.[n:case]</ta>
            <ta e="T74" id="Seg_1262" s="T73">v.[v:pn]</ta>
            <ta e="T75" id="Seg_1263" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_1264" s="T75">v-v:pn</ta>
            <ta e="T77" id="Seg_1265" s="T76">interrog-n:case</ta>
            <ta e="T79" id="Seg_1266" s="T78">nprop.[n:case]</ta>
            <ta e="T80" id="Seg_1267" s="T79">v-v:pn</ta>
            <ta e="T81" id="Seg_1268" s="T80">interrog</ta>
            <ta e="T82" id="Seg_1269" s="T81">pers-n:case</ta>
            <ta e="T83" id="Seg_1270" s="T82">n.[n:case]</ta>
            <ta e="T84" id="Seg_1271" s="T83">pers</ta>
            <ta e="T85" id="Seg_1272" s="T84">ptcl</ta>
            <ta e="T86" id="Seg_1273" s="T85">v-v&gt;v-v:mood.pn</ta>
            <ta e="T87" id="Seg_1274" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_1275" s="T87">v-v:inf</ta>
            <ta e="T89" id="Seg_1276" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_1277" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_1278" s="T90">v-v:mood-v:pn</ta>
            <ta e="T92" id="Seg_1279" s="T91">adj-adj&gt;adv</ta>
            <ta e="T93" id="Seg_1280" s="T92">adj-adj&gt;adv</ta>
            <ta e="T94" id="Seg_1281" s="T93">n.[n:case]-n:poss</ta>
            <ta e="T95" id="Seg_1282" s="T94">v-v:tense.[v:pn]</ta>
            <ta e="T96" id="Seg_1283" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_1284" s="T96">v-v:inf</ta>
            <ta e="T98" id="Seg_1285" s="T97">pers</ta>
            <ta e="T99" id="Seg_1286" s="T98">v-v:pn</ta>
            <ta e="T100" id="Seg_1287" s="T99">v-v:mood.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1288" s="T1">v</ta>
            <ta e="T3" id="Seg_1289" s="T2">v</ta>
            <ta e="T4" id="Seg_1290" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_1291" s="T4">v</ta>
            <ta e="T6" id="Seg_1292" s="T5">n</ta>
            <ta e="T7" id="Seg_1293" s="T6">pers</ta>
            <ta e="T8" id="Seg_1294" s="T7">v</ta>
            <ta e="T9" id="Seg_1295" s="T8">v</ta>
            <ta e="T10" id="Seg_1296" s="T9">v</ta>
            <ta e="T11" id="Seg_1297" s="T10">interj</ta>
            <ta e="T12" id="Seg_1298" s="T11">interrog</ta>
            <ta e="T13" id="Seg_1299" s="T12">v</ta>
            <ta e="T14" id="Seg_1300" s="T13">num</ta>
            <ta e="T15" id="Seg_1301" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_1302" s="T15">num</ta>
            <ta e="T17" id="Seg_1303" s="T16">v</ta>
            <ta e="T18" id="Seg_1304" s="T17">v</ta>
            <ta e="T19" id="Seg_1305" s="T18">pers</ta>
            <ta e="T20" id="Seg_1306" s="T19">num</ta>
            <ta e="T21" id="Seg_1307" s="T20">v</ta>
            <ta e="T22" id="Seg_1308" s="T21">v</ta>
            <ta e="T23" id="Seg_1309" s="T22">v</ta>
            <ta e="T24" id="Seg_1310" s="T23">interrog</ta>
            <ta e="T25" id="Seg_1311" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_1312" s="T25">v</ta>
            <ta e="T29" id="Seg_1313" s="T28">pers</ta>
            <ta e="T30" id="Seg_1314" s="T29">v</ta>
            <ta e="T31" id="Seg_1315" s="T30">pers</ta>
            <ta e="T32" id="Seg_1316" s="T31">n</ta>
            <ta e="T33" id="Seg_1317" s="T32">pers</ta>
            <ta e="T34" id="Seg_1318" s="T33">adj</ta>
            <ta e="T35" id="Seg_1319" s="T34">ptcl</ta>
            <ta e="T36" id="Seg_1320" s="T35">v</ta>
            <ta e="T37" id="Seg_1321" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_1322" s="T37">v</ta>
            <ta e="T39" id="Seg_1323" s="T38">v</ta>
            <ta e="T40" id="Seg_1324" s="T39">n</ta>
            <ta e="T41" id="Seg_1325" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_1326" s="T41">v</ta>
            <ta e="T43" id="Seg_1327" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_1328" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_1329" s="T44">pers</ta>
            <ta e="T46" id="Seg_1330" s="T45">v</ta>
            <ta e="T47" id="Seg_1331" s="T46">adv</ta>
            <ta e="T48" id="Seg_1332" s="T47">pers</ta>
            <ta e="T49" id="Seg_1333" s="T48">v</ta>
            <ta e="T50" id="Seg_1334" s="T49">pers</ta>
            <ta e="T51" id="Seg_1335" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_1336" s="T51">v</ta>
            <ta e="T53" id="Seg_1337" s="T52">pers</ta>
            <ta e="T54" id="Seg_1338" s="T53">adv</ta>
            <ta e="T55" id="Seg_1339" s="T54">v</ta>
            <ta e="T56" id="Seg_1340" s="T55">adj</ta>
            <ta e="T57" id="Seg_1341" s="T56">v</ta>
            <ta e="T58" id="Seg_1342" s="T57">v</ta>
            <ta e="T59" id="Seg_1343" s="T58">num</ta>
            <ta e="T60" id="Seg_1344" s="T59">n</ta>
            <ta e="T61" id="Seg_1345" s="T60">v</ta>
            <ta e="T62" id="Seg_1346" s="T61">pers</ta>
            <ta e="T63" id="Seg_1347" s="T62">adj</ta>
            <ta e="T64" id="Seg_1348" s="T63">n</ta>
            <ta e="T65" id="Seg_1349" s="T64">v</ta>
            <ta e="T66" id="Seg_1350" s="T65">v</ta>
            <ta e="T67" id="Seg_1351" s="T66">n</ta>
            <ta e="T68" id="Seg_1352" s="T67">v</ta>
            <ta e="T69" id="Seg_1353" s="T68">v</ta>
            <ta e="T70" id="Seg_1354" s="T69">v</ta>
            <ta e="T71" id="Seg_1355" s="T70">pers</ta>
            <ta e="T72" id="Seg_1356" s="T71">adv</ta>
            <ta e="T73" id="Seg_1357" s="T72">n</ta>
            <ta e="T74" id="Seg_1358" s="T73">v</ta>
            <ta e="T75" id="Seg_1359" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_1360" s="T75">v</ta>
            <ta e="T77" id="Seg_1361" s="T76">interrog</ta>
            <ta e="T79" id="Seg_1362" s="T78">nprop</ta>
            <ta e="T80" id="Seg_1363" s="T79">v</ta>
            <ta e="T81" id="Seg_1364" s="T80">interrog</ta>
            <ta e="T82" id="Seg_1365" s="T81">pers</ta>
            <ta e="T83" id="Seg_1366" s="T82">n</ta>
            <ta e="T84" id="Seg_1367" s="T83">pers</ta>
            <ta e="T85" id="Seg_1368" s="T84">ptcl</ta>
            <ta e="T86" id="Seg_1369" s="T85">v</ta>
            <ta e="T87" id="Seg_1370" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_1371" s="T87">v</ta>
            <ta e="T89" id="Seg_1372" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_1373" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_1374" s="T90">v</ta>
            <ta e="T92" id="Seg_1375" s="T91">adv</ta>
            <ta e="T93" id="Seg_1376" s="T92">adj</ta>
            <ta e="T94" id="Seg_1377" s="T93">n</ta>
            <ta e="T95" id="Seg_1378" s="T94">v</ta>
            <ta e="T96" id="Seg_1379" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_1380" s="T96">v</ta>
            <ta e="T98" id="Seg_1381" s="T97">pers</ta>
            <ta e="T99" id="Seg_1382" s="T98">v</ta>
            <ta e="T100" id="Seg_1383" s="T99">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_1384" s="T2">0.1.h:A</ta>
            <ta e="T5" id="Seg_1385" s="T4">v:Th</ta>
            <ta e="T6" id="Seg_1386" s="T5">np:Th</ta>
            <ta e="T7" id="Seg_1387" s="T6">pro.h:R</ta>
            <ta e="T8" id="Seg_1388" s="T7">0.3.h:A</ta>
            <ta e="T9" id="Seg_1389" s="T8">0.2.h:A</ta>
            <ta e="T10" id="Seg_1390" s="T9">0.1.h:A 0.3:Th</ta>
            <ta e="T12" id="Seg_1391" s="T11">pro:Ins</ta>
            <ta e="T13" id="Seg_1392" s="T12">0.3.h:A</ta>
            <ta e="T17" id="Seg_1393" s="T16">0.2.h:A 0.3:Th</ta>
            <ta e="T18" id="Seg_1394" s="T17">0.2.h:A</ta>
            <ta e="T21" id="Seg_1395" s="T20">0.3.h:A</ta>
            <ta e="T22" id="Seg_1396" s="T21">0.3.h:A</ta>
            <ta e="T23" id="Seg_1397" s="T22">0.3:Th</ta>
            <ta e="T26" id="Seg_1398" s="T25">0.2.h:A</ta>
            <ta e="T29" id="Seg_1399" s="T28">pro.h:A</ta>
            <ta e="T31" id="Seg_1400" s="T30">pro.h:Th</ta>
            <ta e="T33" id="Seg_1401" s="T32">pro.h:A</ta>
            <ta e="T38" id="Seg_1402" s="T37">0.2.h:A</ta>
            <ta e="T39" id="Seg_1403" s="T38">0.2.h:A</ta>
            <ta e="T40" id="Seg_1404" s="T39">np:Th</ta>
            <ta e="T42" id="Seg_1405" s="T41">v:Th</ta>
            <ta e="T45" id="Seg_1406" s="T44">pro.h:Th</ta>
            <ta e="T47" id="Seg_1407" s="T46">adv:Time</ta>
            <ta e="T48" id="Seg_1408" s="T47">pro.h:Th</ta>
            <ta e="T50" id="Seg_1409" s="T49">pro.h:Th</ta>
            <ta e="T53" id="Seg_1410" s="T52">pro.h:A</ta>
            <ta e="T56" id="Seg_1411" s="T55">np:Th</ta>
            <ta e="T57" id="Seg_1412" s="T56">0.2.h:A</ta>
            <ta e="T58" id="Seg_1413" s="T57">0.1.h:A</ta>
            <ta e="T60" id="Seg_1414" s="T59">np:Th</ta>
            <ta e="T62" id="Seg_1415" s="T61">pro.h:Poss</ta>
            <ta e="T64" id="Seg_1416" s="T63">np:Th</ta>
            <ta e="T66" id="Seg_1417" s="T65">0.2.h:A</ta>
            <ta e="T67" id="Seg_1418" s="T66">np:Th</ta>
            <ta e="T68" id="Seg_1419" s="T67">v:Th</ta>
            <ta e="T70" id="Seg_1420" s="T69">0.2.h:A</ta>
            <ta e="T71" id="Seg_1421" s="T70">pro.h:Poss</ta>
            <ta e="T72" id="Seg_1422" s="T71">adv:Time</ta>
            <ta e="T73" id="Seg_1423" s="T72">np:Th</ta>
            <ta e="T76" id="Seg_1424" s="T75">0.1.h:E</ta>
            <ta e="T79" id="Seg_1425" s="T78">np.h:Th</ta>
            <ta e="T82" id="Seg_1426" s="T81">pro.h:Poss</ta>
            <ta e="T83" id="Seg_1427" s="T82">np:Th</ta>
            <ta e="T84" id="Seg_1428" s="T83">pro.h:A</ta>
            <ta e="T91" id="Seg_1429" s="T90">0.1.h:A</ta>
            <ta e="T94" id="Seg_1430" s="T93">np:P 0.1.h:Poss</ta>
            <ta e="T97" id="Seg_1431" s="T96">v:Th</ta>
            <ta e="T98" id="Seg_1432" s="T97">pro.h:E</ta>
            <ta e="T100" id="Seg_1433" s="T99">0.2.h:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_1434" s="T2">0.1.h:S v:pred</ta>
            <ta e="T4" id="Seg_1435" s="T3">ptcl:pred</ta>
            <ta e="T5" id="Seg_1436" s="T4">v:S</ta>
            <ta e="T6" id="Seg_1437" s="T5">np:O</ta>
            <ta e="T8" id="Seg_1438" s="T7">0.3.h:S v:pred</ta>
            <ta e="T9" id="Seg_1439" s="T8">0.2.h:S v:pred</ta>
            <ta e="T10" id="Seg_1440" s="T9">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T13" id="Seg_1441" s="T12">0.3.h:S v:pred</ta>
            <ta e="T17" id="Seg_1442" s="T16">0.2.h:S v:pred 0.3:O</ta>
            <ta e="T18" id="Seg_1443" s="T17">0.2.h:S v:pred</ta>
            <ta e="T21" id="Seg_1444" s="T20">0.3.h:S v:pred</ta>
            <ta e="T22" id="Seg_1445" s="T21">0.3.h:S v:pred</ta>
            <ta e="T23" id="Seg_1446" s="T22">0.3:S v:pred</ta>
            <ta e="T26" id="Seg_1447" s="T25">0.2.h:S v:pred</ta>
            <ta e="T29" id="Seg_1448" s="T28">pro.h:S</ta>
            <ta e="T30" id="Seg_1449" s="T29">v:pred</ta>
            <ta e="T31" id="Seg_1450" s="T30">pro.h:S</ta>
            <ta e="T32" id="Seg_1451" s="T31">n:pred</ta>
            <ta e="T33" id="Seg_1452" s="T32">pro.h:S</ta>
            <ta e="T36" id="Seg_1453" s="T35">v:pred</ta>
            <ta e="T38" id="Seg_1454" s="T37">0.2.h:S v:pred</ta>
            <ta e="T39" id="Seg_1455" s="T38">0.2.h:S v:pred</ta>
            <ta e="T40" id="Seg_1456" s="T39">np:O</ta>
            <ta e="T41" id="Seg_1457" s="T40">ptcl:pred</ta>
            <ta e="T42" id="Seg_1458" s="T41">v:O</ta>
            <ta e="T44" id="Seg_1459" s="T43">ptcl:pred</ta>
            <ta e="T45" id="Seg_1460" s="T44">pro.h:S</ta>
            <ta e="T46" id="Seg_1461" s="T45">v:pred</ta>
            <ta e="T48" id="Seg_1462" s="T47">pro.h:S</ta>
            <ta e="T49" id="Seg_1463" s="T48">v:pred</ta>
            <ta e="T50" id="Seg_1464" s="T49">pro.h:S</ta>
            <ta e="T52" id="Seg_1465" s="T51">v:pred</ta>
            <ta e="T53" id="Seg_1466" s="T52">pro.h:S</ta>
            <ta e="T55" id="Seg_1467" s="T54">v:pred</ta>
            <ta e="T56" id="Seg_1468" s="T55">np:O</ta>
            <ta e="T57" id="Seg_1469" s="T56">0.2.h:S v:pred</ta>
            <ta e="T58" id="Seg_1470" s="T57">0.1.h:S v:pred</ta>
            <ta e="T60" id="Seg_1471" s="T59">np:S</ta>
            <ta e="T61" id="Seg_1472" s="T60">v:pred</ta>
            <ta e="T64" id="Seg_1473" s="T63">np:S</ta>
            <ta e="T65" id="Seg_1474" s="T64">v:pred</ta>
            <ta e="T66" id="Seg_1475" s="T65">0.2.h:S v:pred</ta>
            <ta e="T67" id="Seg_1476" s="T66">np:O</ta>
            <ta e="T68" id="Seg_1477" s="T67">v:O</ta>
            <ta e="T69" id="Seg_1478" s="T68">ptcl:pred</ta>
            <ta e="T70" id="Seg_1479" s="T69">0.2.h:S v:pred</ta>
            <ta e="T73" id="Seg_1480" s="T72">np:S</ta>
            <ta e="T74" id="Seg_1481" s="T73">v:pred</ta>
            <ta e="T76" id="Seg_1482" s="T75">0.1.h:S v:pred</ta>
            <ta e="T79" id="Seg_1483" s="T78">np.h:S</ta>
            <ta e="T80" id="Seg_1484" s="T79">v:pred</ta>
            <ta e="T83" id="Seg_1485" s="T82">np:S</ta>
            <ta e="T84" id="Seg_1486" s="T83">pro.h:S</ta>
            <ta e="T86" id="Seg_1487" s="T85">v:pred</ta>
            <ta e="T91" id="Seg_1488" s="T90">0.1.h:S v:pred</ta>
            <ta e="T94" id="Seg_1489" s="T93">np:S</ta>
            <ta e="T95" id="Seg_1490" s="T94">v:pred</ta>
            <ta e="T96" id="Seg_1491" s="T95">ptcl:pred</ta>
            <ta e="T97" id="Seg_1492" s="T96">v:O</ta>
            <ta e="T98" id="Seg_1493" s="T97">pro.h:S</ta>
            <ta e="T99" id="Seg_1494" s="T98">v:pred</ta>
            <ta e="T100" id="Seg_1495" s="T99">0.2.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_1496" s="T1">RUS:gram</ta>
            <ta e="T3" id="Seg_1497" s="T2">RUS:cult</ta>
            <ta e="T4" id="Seg_1498" s="T3">RUS:mod</ta>
            <ta e="T5" id="Seg_1499" s="T4">RUS:cult</ta>
            <ta e="T6" id="Seg_1500" s="T5">RUS:cult</ta>
            <ta e="T11" id="Seg_1501" s="T10">RUS:disc</ta>
            <ta e="T32" id="Seg_1502" s="T31">RUS:cult</ta>
            <ta e="T34" id="Seg_1503" s="T33">RUS:cult</ta>
            <ta e="T40" id="Seg_1504" s="T39">RUS:cult</ta>
            <ta e="T41" id="Seg_1505" s="T40">RUS:mod</ta>
            <ta e="T43" id="Seg_1506" s="T42">RUS:gram</ta>
            <ta e="T44" id="Seg_1507" s="T43">RUS:mod</ta>
            <ta e="T47" id="Seg_1508" s="T46">RUS:core</ta>
            <ta e="T60" id="Seg_1509" s="T59">RUS:cult</ta>
            <ta e="T64" id="Seg_1510" s="T63">RUS:cult</ta>
            <ta e="T66" id="Seg_1511" s="T65">RUS:core</ta>
            <ta e="T69" id="Seg_1512" s="T68">RUS:mod</ta>
            <ta e="T72" id="Seg_1513" s="T71">RUS:core</ta>
            <ta e="T73" id="Seg_1514" s="T72">RUS:cult</ta>
            <ta e="T83" id="Seg_1515" s="T82">RUS:cult</ta>
            <ta e="T87" id="Seg_1516" s="T86">RUS:core</ta>
            <ta e="T89" id="Seg_1517" s="T88">RUS:gram</ta>
            <ta e="T90" id="Seg_1518" s="T89">RUS:disc</ta>
            <ta e="T96" id="Seg_1519" s="T95">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_1520" s="T1">Let's play cards.</ta>
            <ta e="T5" id="Seg_1521" s="T3">[We] should play cards.</ta>
            <ta e="T8" id="Seg_1522" s="T5">She threw me a queen.</ta>
            <ta e="T9" id="Seg_1523" s="T8">Your turn!</ta>
            <ta e="T10" id="Seg_1524" s="T9">I take.</ta>
            <ta e="T13" id="Seg_1525" s="T10">Oh, what [cards] she is playing!</ta>
            <ta e="T14" id="Seg_1526" s="T13">A seven.</ta>
            <ta e="T16" id="Seg_1527" s="T14">One more seven.</ta>
            <ta e="T17" id="Seg_1528" s="T16">Give [me more].</ta>
            <ta e="T19" id="Seg_1529" s="T17">Play!</ta>
            <ta e="T20" id="Seg_1530" s="T19">A six.</ta>
            <ta e="T21" id="Seg_1531" s="T20">She cheated!</ta>
            <ta e="T22" id="Seg_1532" s="T21">She cheated!</ta>
            <ta e="T23" id="Seg_1533" s="T22">No!</ta>
            <ta e="T28" id="Seg_1534" s="T23">Why didn't you give?</ta>
            <ta e="T30" id="Seg_1535" s="T28">My turn.</ta>
            <ta e="T32" id="Seg_1536" s="T30">She is a fool.</ta>
            <ta e="T36" id="Seg_1537" s="T32">Don't take an extra [card].</ta>
            <ta e="T39" id="Seg_1538" s="T36">Don't spare, come on!</ta>
            <ta e="T41" id="Seg_1539" s="T39">One needs trumps.</ta>
            <ta e="T44" id="Seg_1540" s="T41">One doesn't has to spare.</ta>
            <ta e="T46" id="Seg_1541" s="T44">I lost.</ta>
            <ta e="T49" id="Seg_1542" s="T46">Now you will loose.</ta>
            <ta e="T52" id="Seg_1543" s="T49">I won't loose.</ta>
            <ta e="T55" id="Seg_1544" s="T52">Is that your move?</ta>
            <ta e="T57" id="Seg_1545" s="T55">Play queens!</ta>
            <ta e="T61" id="Seg_1546" s="T57">I lost, I didn't have a single trump.</ta>
            <ta e="T65" id="Seg_1547" s="T61">I had good cards.</ta>
            <ta e="T67" id="Seg_1548" s="T65">Play with Jack!</ta>
            <ta e="T69" id="Seg_1549" s="T67">We need to deal.</ta>
            <ta e="T70" id="Seg_1550" s="T69">Deal!</ta>
            <ta e="T74" id="Seg_1551" s="T70">I never have aces.</ta>
            <ta e="T78" id="Seg_1552" s="T74">I don't know, how to cover.</ta>
            <ta e="T80" id="Seg_1553" s="T78">Svetlana lost.</ta>
            <ta e="T83" id="Seg_1554" s="T80">How many cards do you have?</ta>
            <ta e="T86" id="Seg_1555" s="T83">Don't you look!</ta>
            <ta e="T88" id="Seg_1556" s="T86">Let's stop playing!</ta>
            <ta e="T91" id="Seg_1557" s="T88">Let's play more.</ta>
            <ta e="T92" id="Seg_1558" s="T91">So!</ta>
            <ta e="T93" id="Seg_1559" s="T92">So!</ta>
            <ta e="T95" id="Seg_1560" s="T93">I'll have headache.</ta>
            <ta e="T97" id="Seg_1561" s="T95">[I] need to think.</ta>
            <ta e="T99" id="Seg_1562" s="T97">I am thinking.</ta>
            <ta e="T100" id="Seg_1563" s="T99">Think.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_1564" s="T1">Lasst uns Karten spielen.</ta>
            <ta e="T5" id="Seg_1565" s="T3">[Wir] sollten Karten spielen.</ta>
            <ta e="T8" id="Seg_1566" s="T5">Sie gab mir eine Dame.</ta>
            <ta e="T9" id="Seg_1567" s="T8">Du bist dran!</ta>
            <ta e="T10" id="Seg_1568" s="T9">Ich nehme.</ta>
            <ta e="T13" id="Seg_1569" s="T10">Oh, was [für Karten] spielt sie!</ta>
            <ta e="T14" id="Seg_1570" s="T13">Eine Sieben.</ta>
            <ta e="T16" id="Seg_1571" s="T14">Noch eine Sieben.</ta>
            <ta e="T17" id="Seg_1572" s="T16">Gib [mir mehr].</ta>
            <ta e="T19" id="Seg_1573" s="T17">Spiel!</ta>
            <ta e="T20" id="Seg_1574" s="T19">Eine Sechs.</ta>
            <ta e="T21" id="Seg_1575" s="T20">Sie hat gemogelt!</ta>
            <ta e="T22" id="Seg_1576" s="T21">Sie hat gemogelt!</ta>
            <ta e="T23" id="Seg_1577" s="T22">Nein!</ta>
            <ta e="T28" id="Seg_1578" s="T23">Warum hast du nicht gegeben?</ta>
            <ta e="T30" id="Seg_1579" s="T28">Ich bin dran.</ta>
            <ta e="T32" id="Seg_1580" s="T30">Sie ist ein Trottel.</ta>
            <ta e="T36" id="Seg_1581" s="T32">Nimm nicht noch eine [Karte].</ta>
            <ta e="T39" id="Seg_1582" s="T36">Schon [uns?] nicht, komm schon!</ta>
            <ta e="T41" id="Seg_1583" s="T39">Man braucht Trümpfe.</ta>
            <ta e="T44" id="Seg_1584" s="T41">Man sollte [niemanden] schonen.</ta>
            <ta e="T46" id="Seg_1585" s="T44">Ich habe verloren.</ta>
            <ta e="T49" id="Seg_1586" s="T46">Jetzt wirst du verlieren.</ta>
            <ta e="T52" id="Seg_1587" s="T49">Ich werde nicht verlieren.</ta>
            <ta e="T55" id="Seg_1588" s="T52">Ist das dein Zug?</ta>
            <ta e="T57" id="Seg_1589" s="T55">Spiel Damen!</ta>
            <ta e="T61" id="Seg_1590" s="T57">Ich habe verloren, ich hatte nicht einen Trumpf.</ta>
            <ta e="T65" id="Seg_1591" s="T61">Ich hatte gute Karten.</ta>
            <ta e="T67" id="Seg_1592" s="T65">Spiel den Buben!</ta>
            <ta e="T69" id="Seg_1593" s="T67">Wir müssen austeilen</ta>
            <ta e="T70" id="Seg_1594" s="T69">Teil aus!</ta>
            <ta e="T74" id="Seg_1595" s="T70">Ich habe nie Asse.</ta>
            <ta e="T78" id="Seg_1596" s="T74">Ich weiß nicht, was ich bedienen soll.</ta>
            <ta e="T80" id="Seg_1597" s="T78">Svetlana hat verloren.</ta>
            <ta e="T83" id="Seg_1598" s="T80">Wie viele Karten hast du?</ta>
            <ta e="T86" id="Seg_1599" s="T83">Schau nicht!</ta>
            <ta e="T88" id="Seg_1600" s="T86">Wir haben genug gespielt!</ta>
            <ta e="T91" id="Seg_1601" s="T88">Lass uns noch mehr spielen.</ta>
            <ta e="T92" id="Seg_1602" s="T91">So!</ta>
            <ta e="T93" id="Seg_1603" s="T92">So!</ta>
            <ta e="T95" id="Seg_1604" s="T93">Ich habe Kopfschmerzen.</ta>
            <ta e="T97" id="Seg_1605" s="T95">[Ich] muss nachdenken.</ta>
            <ta e="T99" id="Seg_1606" s="T97">Ich denke nach.</ta>
            <ta e="T100" id="Seg_1607" s="T99">Denk.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_1608" s="T1">Давай в карты играть.</ta>
            <ta e="T5" id="Seg_1609" s="T3">Надо в карты играть.</ta>
            <ta e="T8" id="Seg_1610" s="T5">Она даму мне бросила.</ta>
            <ta e="T9" id="Seg_1611" s="T8">Ходи!</ta>
            <ta e="T10" id="Seg_1612" s="T9">Взяла.</ta>
            <ta e="T13" id="Seg_1613" s="T10">Ой, какими она ходит!</ta>
            <ta e="T14" id="Seg_1614" s="T13">Семерка.</ta>
            <ta e="T16" id="Seg_1615" s="T14">Еще семерка.</ta>
            <ta e="T17" id="Seg_1616" s="T16">Подкидывай.</ta>
            <ta e="T19" id="Seg_1617" s="T17">Ходи под меня.</ta>
            <ta e="T20" id="Seg_1618" s="T19">Шестерка.</ta>
            <ta e="T21" id="Seg_1619" s="T20">Она обманула!</ta>
            <ta e="T22" id="Seg_1620" s="T21">Она обманула!</ta>
            <ta e="T23" id="Seg_1621" s="T22">Нет!</ta>
            <ta e="T28" id="Seg_1622" s="T23">Почему не отдала?</ta>
            <ta e="T30" id="Seg_1623" s="T28">Я хожу.</ta>
            <ta e="T32" id="Seg_1624" s="T30">Она дура.</ta>
            <ta e="T36" id="Seg_1625" s="T32">Ты лишнюю не бери.</ta>
            <ta e="T39" id="Seg_1626" s="T36">Не жалей, давай!</ta>
            <ta e="T41" id="Seg_1627" s="T39">Козыри нужны.</ta>
            <ta e="T44" id="Seg_1628" s="T41">Жалеть не надо.</ta>
            <ta e="T46" id="Seg_1629" s="T44">Я осталась (проиграла).</ta>
            <ta e="T49" id="Seg_1630" s="T46">Сейчас ты останешься (проиграешь).</ta>
            <ta e="T52" id="Seg_1631" s="T49">Я не останусь (не проиграю).</ta>
            <ta e="T55" id="Seg_1632" s="T52">Ты так ходишь?</ta>
            <ta e="T57" id="Seg_1633" s="T55">Женщин (дам) давайте.</ta>
            <ta e="T61" id="Seg_1634" s="T57">Проиграла(?), и ни одного козыря не было.</ta>
            <ta e="T65" id="Seg_1635" s="T61">У меня хорошие карты были.</ta>
            <ta e="T67" id="Seg_1636" s="T65">Давай ребенка (валета)!</ta>
            <ta e="T69" id="Seg_1637" s="T67">Сдавать надо.</ta>
            <ta e="T70" id="Seg_1638" s="T69">Сдавай.</ta>
            <ta e="T74" id="Seg_1639" s="T70">У меня никогда тузов нет.</ta>
            <ta e="T78" id="Seg_1640" s="T74">Я не знаю, чем крыть.</ta>
            <ta e="T80" id="Seg_1641" s="T78">Светлана осталась (проиграла).</ta>
            <ta e="T83" id="Seg_1642" s="T80">Сколько у тебя карт?</ta>
            <ta e="T86" id="Seg_1643" s="T83">Ты не гляди!</ta>
            <ta e="T88" id="Seg_1644" s="T86">Хватит играть!</ta>
            <ta e="T91" id="Seg_1645" s="T88">Давай еще играть.</ta>
            <ta e="T92" id="Seg_1646" s="T91">Так!</ta>
            <ta e="T93" id="Seg_1647" s="T92">Так!</ta>
            <ta e="T95" id="Seg_1648" s="T93">Голова у меня заболит.</ta>
            <ta e="T97" id="Seg_1649" s="T95">Надо подумать.</ta>
            <ta e="T99" id="Seg_1650" s="T97">Я думаю.</ta>
            <ta e="T100" id="Seg_1651" s="T99">Подумай.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_1652" s="T1">давай в карты играть</ta>
            <ta e="T5" id="Seg_1653" s="T3">надо в карты играть</ta>
            <ta e="T8" id="Seg_1654" s="T5">даму мне бросила</ta>
            <ta e="T9" id="Seg_1655" s="T8">ходи</ta>
            <ta e="T10" id="Seg_1656" s="T9">приняла (взяла)</ta>
            <ta e="T13" id="Seg_1657" s="T10">ой какими (с чем) ходит</ta>
            <ta e="T16" id="Seg_1658" s="T14">еще семерка</ta>
            <ta e="T17" id="Seg_1659" s="T16">подкидывай</ta>
            <ta e="T19" id="Seg_1660" s="T17">ходи ко мне</ta>
            <ta e="T20" id="Seg_1661" s="T19">шестерка</ta>
            <ta e="T21" id="Seg_1662" s="T20">обманула</ta>
            <ta e="T23" id="Seg_1663" s="T22">нет</ta>
            <ta e="T28" id="Seg_1664" s="T23">почто не отдала</ta>
            <ta e="T30" id="Seg_1665" s="T28">я хожу</ta>
            <ta e="T36" id="Seg_1666" s="T32">ты лишнюю не бери</ta>
            <ta e="T39" id="Seg_1667" s="T36">не жалей давай</ta>
            <ta e="T44" id="Seg_1668" s="T41">ходить не надо</ta>
            <ta e="T46" id="Seg_1669" s="T44">я осталась (дурочкой)</ta>
            <ta e="T49" id="Seg_1670" s="T46">сейчас ты останешься</ta>
            <ta e="T52" id="Seg_1671" s="T49">я не останусь</ta>
            <ta e="T55" id="Seg_1672" s="T52">ты так ходишь</ta>
            <ta e="T57" id="Seg_1673" s="T55">женщин давайте</ta>
            <ta e="T61" id="Seg_1674" s="T57">проиграла и ни одного козыря не было</ta>
            <ta e="T67" id="Seg_1675" s="T65">давай парнишку (вальта)</ta>
            <ta e="T69" id="Seg_1676" s="T67">сдавать надо</ta>
            <ta e="T70" id="Seg_1677" s="T69">сдавай</ta>
            <ta e="T74" id="Seg_1678" s="T70">у меня никогда тузов</ta>
            <ta e="T78" id="Seg_1679" s="T74">я не знаю чем крыть</ta>
            <ta e="T80" id="Seg_1680" s="T78">Светлана осталась (проиграла)</ta>
            <ta e="T83" id="Seg_1681" s="T80">сколько у тебя карт</ta>
            <ta e="T86" id="Seg_1682" s="T83">ты не гляди</ta>
            <ta e="T88" id="Seg_1683" s="T86">хватит играть</ta>
            <ta e="T92" id="Seg_1684" s="T91">так</ta>
            <ta e="T93" id="Seg_1685" s="T92">так</ta>
            <ta e="T95" id="Seg_1686" s="T93">голова заболит</ta>
            <ta e="T97" id="Seg_1687" s="T95">надо подумать</ta>
            <ta e="T99" id="Seg_1688" s="T97">я думаю</ta>
            <ta e="T100" id="Seg_1689" s="T99">подумай</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T19" id="Seg_1690" s="T17">[KuAI:] Variant: 'megka'.</ta>
            <ta e="T36" id="Seg_1691" s="T32">[KuAI:] Variant: 'iga'. [BrM:] Unclear usage of genitive.</ta>
            <ta e="T39" id="Seg_1692" s="T36">[KuAI:] Variant: 'mitdə'.</ta>
            <ta e="T55" id="Seg_1693" s="T52">[KuAI:] Variant: 'nʼilʼdʼin'.</ta>
            <ta e="T61" id="Seg_1694" s="T57">[KuAI:] Variant: 'tʼagkus'.</ta>
            <ta e="T92" id="Seg_1695" s="T91">[KuAI:] Variant: 'Tɨlʼdʼiːn'.</ta>
            <ta e="T97" id="Seg_1696" s="T95">[KuAI:] Variant: 'tejerbuguː'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
