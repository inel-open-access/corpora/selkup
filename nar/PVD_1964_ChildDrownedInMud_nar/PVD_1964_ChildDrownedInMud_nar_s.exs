<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_ChildDrownedInMud_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_ChildDrownedInMud_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">178</ud-information>
            <ud-information attribute-name="# HIAT:w">129</ud-information>
            <ud-information attribute-name="# e">129</ud-information>
            <ud-information attribute-name="# HIAT:u">34</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T130" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">tamdʼel</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">közizan</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">qwɛlɨm</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">taugu</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_20" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">Küsʼe</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">čaʒan</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">A</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">kɨbanʼäʒa</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">nʼäŋottə</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">qouppa</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_44" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">Tʼüːrɨn</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_50" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">Qula</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">čaʒat</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_59" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">Qudnʼäs</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">qɨbanʼäʒam</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">jassʼ</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">jüɣulgattə</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_74" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_76" n="HIAT:w" s="T19">A</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">mekga</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">ʒalkan</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">azun</ts>
                  <nts id="Seg_86" n="HIAT:ip">.</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_89" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_91" n="HIAT:w" s="T23">Qɨbanʼäʒa</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">tʼürɨn</ts>
                  <nts id="Seg_95" n="HIAT:ip">.</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_98" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_100" n="HIAT:w" s="T25">Man</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_103" n="HIAT:w" s="T26">saːdannan</ts>
                  <nts id="Seg_104" n="HIAT:ip">.</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_107" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_109" n="HIAT:w" s="T27">Qɨbanʼaʒam</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_112" n="HIAT:w" s="T28">üːgunnau</ts>
                  <nts id="Seg_113" n="HIAT:ip">.</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_116" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_118" n="HIAT:w" s="T29">Täbɨn</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_121" n="HIAT:w" s="T30">sapok</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_124" n="HIAT:w" s="T31">natʼen</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_127" n="HIAT:w" s="T32">qalɨn</ts>
                  <nts id="Seg_128" n="HIAT:ip">.</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_131" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_133" n="HIAT:w" s="T33">Qɨbanʼäʒa</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_136" n="HIAT:w" s="T34">watot</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_139" n="HIAT:w" s="T35">kuronnɨn</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_142" n="HIAT:w" s="T36">nelʼdʼzʼä</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_145" n="HIAT:w" s="T37">dobɨn</ts>
                  <nts id="Seg_146" n="HIAT:ip">.</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_149" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_151" n="HIAT:w" s="T38">Man</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_154" n="HIAT:w" s="T39">sapogɨm</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_157" n="HIAT:w" s="T40">ügunnau</ts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_161" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_163" n="HIAT:w" s="T41">Onän</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_166" n="HIAT:w" s="T42">qowan</ts>
                  <nts id="Seg_167" n="HIAT:ip">.</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_170" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_172" n="HIAT:w" s="T43">Okkɨr</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_175" n="HIAT:w" s="T44">tobou</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_178" n="HIAT:w" s="T45">ügulgu</ts>
                  <nts id="Seg_179" n="HIAT:ip">,</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_182" n="HIAT:w" s="T46">au</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_185" n="HIAT:w" s="T47">tobou</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_188" n="HIAT:w" s="T48">qouqun</ts>
                  <nts id="Seg_189" n="HIAT:ip">.</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_192" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_194" n="HIAT:w" s="T49">Nasel</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_197" n="HIAT:w" s="T50">čaǯan</ts>
                  <nts id="Seg_198" n="HIAT:ip">.</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_201" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_203" n="HIAT:w" s="T51">Tobou</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_206" n="HIAT:w" s="T52">innä</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_209" n="HIAT:w" s="T53">watʼükou</ts>
                  <nts id="Seg_210" n="HIAT:ip">.</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_213" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_215" n="HIAT:w" s="T54">A</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_218" n="HIAT:w" s="T55">ɣau</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_221" n="HIAT:w" s="T56">tobou</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_224" n="HIAT:w" s="T57">ilʼlʼe</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_227" n="HIAT:w" s="T58">koukon</ts>
                  <nts id="Seg_228" n="HIAT:ip">.</nts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_231" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_233" n="HIAT:w" s="T59">Naseːl</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_236" n="HIAT:w" s="T60">čaǯan</ts>
                  <nts id="Seg_237" n="HIAT:ip">.</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_240" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_242" n="HIAT:w" s="T61">Udlau</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_245" n="HIAT:w" s="T62">wes</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_248" n="HIAT:w" s="T63">kozɨnnau</ts>
                  <nts id="Seg_249" n="HIAT:ip">.</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_252" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_254" n="HIAT:w" s="T64">Kɨbanaʒan</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_257" n="HIAT:w" s="T65">sapogam</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_260" n="HIAT:w" s="T66">toboɣɨtdə</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_263" n="HIAT:w" s="T67">serčau</ts>
                  <nts id="Seg_264" n="HIAT:ip">.</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_267" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_269" n="HIAT:w" s="T68">A</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_272" n="HIAT:w" s="T69">nɨtdɨn</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_275" n="HIAT:w" s="T70">qɨːbanädäkka</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_278" n="HIAT:w" s="T71">nɨŋɨtda</ts>
                  <nts id="Seg_279" n="HIAT:ip">.</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_282" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_284" n="HIAT:w" s="T72">Man</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_287" n="HIAT:w" s="T73">tʼäran</ts>
                  <nts id="Seg_288" n="HIAT:ip">:</nts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_290" n="HIAT:ip">“</nts>
                  <ts e="T75" id="Seg_292" n="HIAT:w" s="T74">Kɨbanʼäʒan</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_295" n="HIAT:w" s="T75">mattə</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_298" n="HIAT:w" s="T76">qajɣɨn</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_301" n="HIAT:w" s="T77">jen</ts>
                  <nts id="Seg_302" n="HIAT:ip">?</nts>
                  <nts id="Seg_303" n="HIAT:ip">”</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_306" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_308" n="HIAT:w" s="T78">A</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_311" n="HIAT:w" s="T79">qɨba</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_314" n="HIAT:w" s="T80">nädän</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_317" n="HIAT:w" s="T81">tärɨn</ts>
                  <nts id="Seg_318" n="HIAT:ip">:</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_320" n="HIAT:ip">“</nts>
                  <ts e="T83" id="Seg_322" n="HIAT:w" s="T82">A</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_325" n="HIAT:w" s="T83">to</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_328" n="HIAT:w" s="T84">näjɣum</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_331" n="HIAT:w" s="T85">na</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_334" n="HIAT:w" s="T86">lagwatpɨtda</ts>
                  <nts id="Seg_335" n="HIAT:ip">.</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_338" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_340" n="HIAT:w" s="T87">Täbɨn</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_343" n="HIAT:w" s="T88">äutdä</ts>
                  <nts id="Seg_344" n="HIAT:ip">.</nts>
                  <nts id="Seg_345" n="HIAT:ip">”</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_348" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_350" n="HIAT:w" s="T89">Äutdänä</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_353" n="HIAT:w" s="T90">man</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_356" n="HIAT:w" s="T91">datao</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_359" n="HIAT:w" s="T92">tʼüan</ts>
                  <nts id="Seg_360" n="HIAT:ip">.</nts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_363" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_365" n="HIAT:w" s="T93">Otdə</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_368" n="HIAT:w" s="T94">qɨbɨnʼaʒamdə</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_371" n="HIAT:w" s="T95">üɣulgu</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_374" n="HIAT:w" s="T96">ne</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_377" n="HIAT:w" s="T97">moʒət</ts>
                  <nts id="Seg_378" n="HIAT:ip">.</nts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_381" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_383" n="HIAT:w" s="T98">Tabnä</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_386" n="HIAT:w" s="T99">qaj</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_389" n="HIAT:w" s="T100">qɨbanʼaʒam</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_392" n="HIAT:w" s="T101">ne</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_395" n="HIAT:w" s="T102">nada</ts>
                  <nts id="Seg_396" n="HIAT:ip">?</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_399" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_401" n="HIAT:w" s="T103">Aːmda</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_404" n="HIAT:w" s="T104">i</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_407" n="HIAT:w" s="T105">lagwatpa</ts>
                  <nts id="Seg_408" n="HIAT:ip">,</nts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_411" n="HIAT:w" s="T106">akoškaɣon</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_414" n="HIAT:w" s="T107">mannɨppa</ts>
                  <nts id="Seg_415" n="HIAT:ip">.</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_418" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_420" n="HIAT:w" s="T108">Qudämnäss</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_423" n="HIAT:w" s="T109">peldɨmeǯau</ts>
                  <nts id="Seg_424" n="HIAT:ip">.</nts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_427" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_429" n="HIAT:w" s="T110">Udlau</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_432" n="HIAT:w" s="T111">küzatdə</ts>
                  <nts id="Seg_433" n="HIAT:ip">.</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_436" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_438" n="HIAT:w" s="T112">Man</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_441" n="HIAT:w" s="T113">täbɨm</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_444" n="HIAT:w" s="T114">kak</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_447" n="HIAT:w" s="T115">ügulbuzau</ts>
                  <nts id="Seg_448" n="HIAT:ip">,</nts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_451" n="HIAT:w" s="T116">nasel</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_454" n="HIAT:w" s="T117">ügunnau</ts>
                  <nts id="Seg_455" n="HIAT:ip">.</nts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_458" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_460" n="HIAT:w" s="T118">Man</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_463" n="HIAT:w" s="T119">täbnä</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_466" n="HIAT:w" s="T120">tʼäran</ts>
                  <nts id="Seg_467" n="HIAT:ip">:</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_469" n="HIAT:ip">“</nts>
                  <ts e="T122" id="Seg_471" n="HIAT:w" s="T121">Qajno</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_474" n="HIAT:w" s="T122">taftʼet</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_477" n="HIAT:w" s="T123">saːdassat</ts>
                  <nts id="Seg_478" n="HIAT:ip">?</nts>
                  <nts id="Seg_479" n="HIAT:ip">”</nts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_482" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_484" n="HIAT:w" s="T124">A</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_487" n="HIAT:w" s="T125">täp</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_490" n="HIAT:w" s="T126">tʼärɨn</ts>
                  <nts id="Seg_491" n="HIAT:ip">:</nts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_493" n="HIAT:ip">“</nts>
                  <ts e="T128" id="Seg_495" n="HIAT:w" s="T127">Man</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_498" n="HIAT:w" s="T128">koːlɨblam</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_501" n="HIAT:w" s="T129">kaːzan</ts>
                  <nts id="Seg_502" n="HIAT:ip">.</nts>
                  <nts id="Seg_503" n="HIAT:ip">”</nts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T130" id="Seg_505" n="sc" s="T1">
               <ts e="T2" id="Seg_507" n="e" s="T1">Man </ts>
               <ts e="T3" id="Seg_509" n="e" s="T2">tamdʼel </ts>
               <ts e="T4" id="Seg_511" n="e" s="T3">közizan </ts>
               <ts e="T5" id="Seg_513" n="e" s="T4">qwɛlɨm </ts>
               <ts e="T6" id="Seg_515" n="e" s="T5">taugu. </ts>
               <ts e="T7" id="Seg_517" n="e" s="T6">Küsʼe </ts>
               <ts e="T8" id="Seg_519" n="e" s="T7">čaʒan. </ts>
               <ts e="T9" id="Seg_521" n="e" s="T8">A </ts>
               <ts e="T10" id="Seg_523" n="e" s="T9">kɨbanʼäʒa </ts>
               <ts e="T11" id="Seg_525" n="e" s="T10">nʼäŋottə </ts>
               <ts e="T12" id="Seg_527" n="e" s="T11">qouppa. </ts>
               <ts e="T13" id="Seg_529" n="e" s="T12">Tʼüːrɨn. </ts>
               <ts e="T14" id="Seg_531" n="e" s="T13">Qula </ts>
               <ts e="T15" id="Seg_533" n="e" s="T14">čaʒat. </ts>
               <ts e="T16" id="Seg_535" n="e" s="T15">Qudnʼäs </ts>
               <ts e="T17" id="Seg_537" n="e" s="T16">qɨbanʼäʒam </ts>
               <ts e="T18" id="Seg_539" n="e" s="T17">jassʼ </ts>
               <ts e="T19" id="Seg_541" n="e" s="T18">jüɣulgattə. </ts>
               <ts e="T20" id="Seg_543" n="e" s="T19">A </ts>
               <ts e="T21" id="Seg_545" n="e" s="T20">mekga </ts>
               <ts e="T22" id="Seg_547" n="e" s="T21">ʒalkan </ts>
               <ts e="T23" id="Seg_549" n="e" s="T22">azun. </ts>
               <ts e="T24" id="Seg_551" n="e" s="T23">Qɨbanʼäʒa </ts>
               <ts e="T25" id="Seg_553" n="e" s="T24">tʼürɨn. </ts>
               <ts e="T26" id="Seg_555" n="e" s="T25">Man </ts>
               <ts e="T27" id="Seg_557" n="e" s="T26">saːdannan. </ts>
               <ts e="T28" id="Seg_559" n="e" s="T27">Qɨbanʼaʒam </ts>
               <ts e="T29" id="Seg_561" n="e" s="T28">üːgunnau. </ts>
               <ts e="T30" id="Seg_563" n="e" s="T29">Täbɨn </ts>
               <ts e="T31" id="Seg_565" n="e" s="T30">sapok </ts>
               <ts e="T32" id="Seg_567" n="e" s="T31">natʼen </ts>
               <ts e="T33" id="Seg_569" n="e" s="T32">qalɨn. </ts>
               <ts e="T34" id="Seg_571" n="e" s="T33">Qɨbanʼäʒa </ts>
               <ts e="T35" id="Seg_573" n="e" s="T34">watot </ts>
               <ts e="T36" id="Seg_575" n="e" s="T35">kuronnɨn </ts>
               <ts e="T37" id="Seg_577" n="e" s="T36">nelʼdʼzʼä </ts>
               <ts e="T38" id="Seg_579" n="e" s="T37">dobɨn. </ts>
               <ts e="T39" id="Seg_581" n="e" s="T38">Man </ts>
               <ts e="T40" id="Seg_583" n="e" s="T39">sapogɨm </ts>
               <ts e="T41" id="Seg_585" n="e" s="T40">ügunnau. </ts>
               <ts e="T42" id="Seg_587" n="e" s="T41">Onän </ts>
               <ts e="T43" id="Seg_589" n="e" s="T42">qowan. </ts>
               <ts e="T44" id="Seg_591" n="e" s="T43">Okkɨr </ts>
               <ts e="T45" id="Seg_593" n="e" s="T44">tobou </ts>
               <ts e="T46" id="Seg_595" n="e" s="T45">ügulgu, </ts>
               <ts e="T47" id="Seg_597" n="e" s="T46">au </ts>
               <ts e="T48" id="Seg_599" n="e" s="T47">tobou </ts>
               <ts e="T49" id="Seg_601" n="e" s="T48">qouqun. </ts>
               <ts e="T50" id="Seg_603" n="e" s="T49">Nasel </ts>
               <ts e="T51" id="Seg_605" n="e" s="T50">čaǯan. </ts>
               <ts e="T52" id="Seg_607" n="e" s="T51">Tobou </ts>
               <ts e="T53" id="Seg_609" n="e" s="T52">innä </ts>
               <ts e="T54" id="Seg_611" n="e" s="T53">watʼükou. </ts>
               <ts e="T55" id="Seg_613" n="e" s="T54">A </ts>
               <ts e="T56" id="Seg_615" n="e" s="T55">ɣau </ts>
               <ts e="T57" id="Seg_617" n="e" s="T56">tobou </ts>
               <ts e="T58" id="Seg_619" n="e" s="T57">ilʼlʼe </ts>
               <ts e="T59" id="Seg_621" n="e" s="T58">koukon. </ts>
               <ts e="T60" id="Seg_623" n="e" s="T59">Naseːl </ts>
               <ts e="T61" id="Seg_625" n="e" s="T60">čaǯan. </ts>
               <ts e="T62" id="Seg_627" n="e" s="T61">Udlau </ts>
               <ts e="T63" id="Seg_629" n="e" s="T62">wes </ts>
               <ts e="T64" id="Seg_631" n="e" s="T63">kozɨnnau. </ts>
               <ts e="T65" id="Seg_633" n="e" s="T64">Kɨbanaʒan </ts>
               <ts e="T66" id="Seg_635" n="e" s="T65">sapogam </ts>
               <ts e="T67" id="Seg_637" n="e" s="T66">toboɣɨtdə </ts>
               <ts e="T68" id="Seg_639" n="e" s="T67">serčau. </ts>
               <ts e="T69" id="Seg_641" n="e" s="T68">A </ts>
               <ts e="T70" id="Seg_643" n="e" s="T69">nɨtdɨn </ts>
               <ts e="T71" id="Seg_645" n="e" s="T70">qɨːbanädäkka </ts>
               <ts e="T72" id="Seg_647" n="e" s="T71">nɨŋɨtda. </ts>
               <ts e="T73" id="Seg_649" n="e" s="T72">Man </ts>
               <ts e="T74" id="Seg_651" n="e" s="T73">tʼäran: </ts>
               <ts e="T75" id="Seg_653" n="e" s="T74">“Kɨbanʼäʒan </ts>
               <ts e="T76" id="Seg_655" n="e" s="T75">mattə </ts>
               <ts e="T77" id="Seg_657" n="e" s="T76">qajɣɨn </ts>
               <ts e="T78" id="Seg_659" n="e" s="T77">jen?” </ts>
               <ts e="T79" id="Seg_661" n="e" s="T78">A </ts>
               <ts e="T80" id="Seg_663" n="e" s="T79">qɨba </ts>
               <ts e="T81" id="Seg_665" n="e" s="T80">nädän </ts>
               <ts e="T82" id="Seg_667" n="e" s="T81">tärɨn: </ts>
               <ts e="T83" id="Seg_669" n="e" s="T82">“A </ts>
               <ts e="T84" id="Seg_671" n="e" s="T83">to </ts>
               <ts e="T85" id="Seg_673" n="e" s="T84">näjɣum </ts>
               <ts e="T86" id="Seg_675" n="e" s="T85">na </ts>
               <ts e="T87" id="Seg_677" n="e" s="T86">lagwatpɨtda. </ts>
               <ts e="T88" id="Seg_679" n="e" s="T87">Täbɨn </ts>
               <ts e="T89" id="Seg_681" n="e" s="T88">äutdä.” </ts>
               <ts e="T90" id="Seg_683" n="e" s="T89">Äutdänä </ts>
               <ts e="T91" id="Seg_685" n="e" s="T90">man </ts>
               <ts e="T92" id="Seg_687" n="e" s="T91">datao </ts>
               <ts e="T93" id="Seg_689" n="e" s="T92">tʼüan. </ts>
               <ts e="T94" id="Seg_691" n="e" s="T93">Otdə </ts>
               <ts e="T95" id="Seg_693" n="e" s="T94">qɨbɨnʼaʒamdə </ts>
               <ts e="T96" id="Seg_695" n="e" s="T95">üɣulgu </ts>
               <ts e="T97" id="Seg_697" n="e" s="T96">ne </ts>
               <ts e="T98" id="Seg_699" n="e" s="T97">moʒət. </ts>
               <ts e="T99" id="Seg_701" n="e" s="T98">Tabnä </ts>
               <ts e="T100" id="Seg_703" n="e" s="T99">qaj </ts>
               <ts e="T101" id="Seg_705" n="e" s="T100">qɨbanʼaʒam </ts>
               <ts e="T102" id="Seg_707" n="e" s="T101">ne </ts>
               <ts e="T103" id="Seg_709" n="e" s="T102">nada? </ts>
               <ts e="T104" id="Seg_711" n="e" s="T103">Aːmda </ts>
               <ts e="T105" id="Seg_713" n="e" s="T104">i </ts>
               <ts e="T106" id="Seg_715" n="e" s="T105">lagwatpa, </ts>
               <ts e="T107" id="Seg_717" n="e" s="T106">akoškaɣon </ts>
               <ts e="T108" id="Seg_719" n="e" s="T107">mannɨppa. </ts>
               <ts e="T109" id="Seg_721" n="e" s="T108">Qudämnäss </ts>
               <ts e="T110" id="Seg_723" n="e" s="T109">peldɨmeǯau. </ts>
               <ts e="T111" id="Seg_725" n="e" s="T110">Udlau </ts>
               <ts e="T112" id="Seg_727" n="e" s="T111">küzatdə. </ts>
               <ts e="T113" id="Seg_729" n="e" s="T112">Man </ts>
               <ts e="T114" id="Seg_731" n="e" s="T113">täbɨm </ts>
               <ts e="T115" id="Seg_733" n="e" s="T114">kak </ts>
               <ts e="T116" id="Seg_735" n="e" s="T115">ügulbuzau, </ts>
               <ts e="T117" id="Seg_737" n="e" s="T116">nasel </ts>
               <ts e="T118" id="Seg_739" n="e" s="T117">ügunnau. </ts>
               <ts e="T119" id="Seg_741" n="e" s="T118">Man </ts>
               <ts e="T120" id="Seg_743" n="e" s="T119">täbnä </ts>
               <ts e="T121" id="Seg_745" n="e" s="T120">tʼäran: </ts>
               <ts e="T122" id="Seg_747" n="e" s="T121">“Qajno </ts>
               <ts e="T123" id="Seg_749" n="e" s="T122">taftʼet </ts>
               <ts e="T124" id="Seg_751" n="e" s="T123">saːdassat?” </ts>
               <ts e="T125" id="Seg_753" n="e" s="T124">A </ts>
               <ts e="T126" id="Seg_755" n="e" s="T125">täp </ts>
               <ts e="T127" id="Seg_757" n="e" s="T126">tʼärɨn: </ts>
               <ts e="T128" id="Seg_759" n="e" s="T127">“Man </ts>
               <ts e="T129" id="Seg_761" n="e" s="T128">koːlɨblam </ts>
               <ts e="T130" id="Seg_763" n="e" s="T129">kaːzan.” </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_764" s="T1">PVD_1964_ChildDrownedInMud_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_765" s="T6">PVD_1964_ChildDrownedInMud_nar.002 (001.002)</ta>
            <ta e="T12" id="Seg_766" s="T8">PVD_1964_ChildDrownedInMud_nar.003 (001.003)</ta>
            <ta e="T13" id="Seg_767" s="T12">PVD_1964_ChildDrownedInMud_nar.004 (001.004)</ta>
            <ta e="T15" id="Seg_768" s="T13">PVD_1964_ChildDrownedInMud_nar.005 (001.005)</ta>
            <ta e="T19" id="Seg_769" s="T15">PVD_1964_ChildDrownedInMud_nar.006 (001.006)</ta>
            <ta e="T23" id="Seg_770" s="T19">PVD_1964_ChildDrownedInMud_nar.007 (001.007)</ta>
            <ta e="T25" id="Seg_771" s="T23">PVD_1964_ChildDrownedInMud_nar.008 (001.008)</ta>
            <ta e="T27" id="Seg_772" s="T25">PVD_1964_ChildDrownedInMud_nar.009 (001.009)</ta>
            <ta e="T29" id="Seg_773" s="T27">PVD_1964_ChildDrownedInMud_nar.010 (001.010)</ta>
            <ta e="T33" id="Seg_774" s="T29">PVD_1964_ChildDrownedInMud_nar.011 (001.011)</ta>
            <ta e="T38" id="Seg_775" s="T33">PVD_1964_ChildDrownedInMud_nar.012 (001.012)</ta>
            <ta e="T41" id="Seg_776" s="T38">PVD_1964_ChildDrownedInMud_nar.013 (001.013)</ta>
            <ta e="T43" id="Seg_777" s="T41">PVD_1964_ChildDrownedInMud_nar.014 (001.014)</ta>
            <ta e="T49" id="Seg_778" s="T43">PVD_1964_ChildDrownedInMud_nar.015 (001.015)</ta>
            <ta e="T51" id="Seg_779" s="T49">PVD_1964_ChildDrownedInMud_nar.016 (001.016)</ta>
            <ta e="T54" id="Seg_780" s="T51">PVD_1964_ChildDrownedInMud_nar.017 (001.017)</ta>
            <ta e="T59" id="Seg_781" s="T54">PVD_1964_ChildDrownedInMud_nar.018 (001.018)</ta>
            <ta e="T61" id="Seg_782" s="T59">PVD_1964_ChildDrownedInMud_nar.019 (001.019)</ta>
            <ta e="T64" id="Seg_783" s="T61">PVD_1964_ChildDrownedInMud_nar.020 (001.020)</ta>
            <ta e="T68" id="Seg_784" s="T64">PVD_1964_ChildDrownedInMud_nar.021 (001.021)</ta>
            <ta e="T72" id="Seg_785" s="T68">PVD_1964_ChildDrownedInMud_nar.022 (001.022)</ta>
            <ta e="T78" id="Seg_786" s="T72">PVD_1964_ChildDrownedInMud_nar.023 (001.023)</ta>
            <ta e="T87" id="Seg_787" s="T78">PVD_1964_ChildDrownedInMud_nar.024 (001.024)</ta>
            <ta e="T89" id="Seg_788" s="T87">PVD_1964_ChildDrownedInMud_nar.025 (001.025)</ta>
            <ta e="T93" id="Seg_789" s="T89">PVD_1964_ChildDrownedInMud_nar.026 (001.026)</ta>
            <ta e="T98" id="Seg_790" s="T93">PVD_1964_ChildDrownedInMud_nar.027 (001.027)</ta>
            <ta e="T103" id="Seg_791" s="T98">PVD_1964_ChildDrownedInMud_nar.028 (001.028)</ta>
            <ta e="T108" id="Seg_792" s="T103">PVD_1964_ChildDrownedInMud_nar.029 (001.029)</ta>
            <ta e="T110" id="Seg_793" s="T108">PVD_1964_ChildDrownedInMud_nar.030 (001.030)</ta>
            <ta e="T112" id="Seg_794" s="T110">PVD_1964_ChildDrownedInMud_nar.031 (001.031)</ta>
            <ta e="T118" id="Seg_795" s="T112">PVD_1964_ChildDrownedInMud_nar.032 (001.032)</ta>
            <ta e="T124" id="Seg_796" s="T118">PVD_1964_ChildDrownedInMud_nar.033 (001.033)</ta>
            <ta e="T130" id="Seg_797" s="T124">PVD_1964_ChildDrownedInMud_nar.034 (001.034)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_798" s="T1">ман там′дʼел ′кӧ(ъ̊)зизан ′kwɛлым ′таугу.</ta>
            <ta e="T8" id="Seg_799" s="T6">′кӱ(ӧ)сʼе ′тшажан.</ta>
            <ta e="T12" id="Seg_800" s="T8">а кы′банʼӓжа нʼӓ′ңоттъ ′kоуп(б̂)па.</ta>
            <ta e="T13" id="Seg_801" s="T12">′тʼӱ̄рын.</ta>
            <ta e="T15" id="Seg_802" s="T13">kу′lа ′тшажат.</ta>
            <ta e="T19" id="Seg_803" s="T15">kуд′нʼӓс kы′банʼӓжам jассʼ ′jӱɣулгаттъ.</ta>
            <ta e="T23" id="Seg_804" s="T19">а мекга ′жалкан а′зун.</ta>
            <ta e="T25" id="Seg_805" s="T23">kы′банʼӓжа ′тʼӱрын.</ta>
            <ta e="T27" id="Seg_806" s="T25">′ман ′са̄даннан.</ta>
            <ta e="T29" id="Seg_807" s="T27">kыбанʼа′жам ′ӱ̄гуннау.</ta>
            <ta e="T33" id="Seg_808" s="T29">тӓ′бын са′пок на′тʼен ′kалын.</ta>
            <ta e="T38" id="Seg_809" s="T33">kы′банʼӓжа ва′тот ку′роннын нелʼ′дʼзʼӓ′добын.</ta>
            <ta e="T41" id="Seg_810" s="T38">ман ′сапогым ′ӱгуннау.</ta>
            <ta e="T43" id="Seg_811" s="T41">о′нӓн ′kоwан.</ta>
            <ta e="T49" id="Seg_812" s="T43">′оккыр то′боу ′ӱгулгу, ′(ɣ)ау то′боу ′kоуkун.</ta>
            <ta e="T51" id="Seg_813" s="T49">на′сел тша′джан.</ta>
            <ta e="T54" id="Seg_814" s="T51">то′боу ′иннӓ ватʼӱ′коу.</ta>
            <ta e="T59" id="Seg_815" s="T54">а ′ɣау то′боу и′лʼлʼе ′коу‵кон.</ta>
            <ta e="T61" id="Seg_816" s="T59">на′се̄л ′тшаджан.</ta>
            <ta e="T64" id="Seg_817" s="T61">уд′лау вес ′козыннау.</ta>
            <ta e="T68" id="Seg_818" s="T64">кы′банажан са′погам то′боɣытдъ сер′тшау.</ta>
            <ta e="T72" id="Seg_819" s="T68">а ныт′дын ′kы̄банӓ′дӓкка ны′ңытда.</ta>
            <ta e="T78" id="Seg_820" s="T72">ман тʼӓ′ран: кыбанʼӓ′жан ′маттъ kай′ɣын jен?</ta>
            <ta e="T87" id="Seg_821" s="T78">а kы′ба нӓ′дӓн тӓ′рын: а то ′нӓйɣум нала′гватпытда.</ta>
            <ta e="T89" id="Seg_822" s="T87">′тӓбын ′ӓутдӓ.</ta>
            <ta e="T93" id="Seg_823" s="T89">′ӓутдӓнӓ ман д̂а ′тао ′тʼӱан.</ta>
            <ta e="T98" id="Seg_824" s="T93">′отдъ ′kыбынʼа′жамдъ ′ӱɣулгу не ′можът.</ta>
            <ta e="T103" id="Seg_825" s="T98">таб′нӓ kай kы′банʼажам не ′нада.</ta>
            <ta e="T108" id="Seg_826" s="T103">′а̄мда и лагватпа, а′кошкаɣон ′манныппа.</ta>
            <ta e="T110" id="Seg_827" s="T108">′kудӓм нӓсс ′пелдымеджау.</ta>
            <ta e="T112" id="Seg_828" s="T110">уд′лау кӱ′затдъ.</ta>
            <ta e="T118" id="Seg_829" s="T112">ман тӓбым как ′ӱгулбузау, на′сел ′ӱгуннау.</ta>
            <ta e="T124" id="Seg_830" s="T118">ман тӓбнӓ тʼӓ′ран: kай′но таф′тʼет ′са̄дассат.</ta>
            <ta e="T130" id="Seg_831" s="T124">а тӓп тʼӓ′рын: ман ′ко̄лыблам ка̄зан.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_832" s="T1">man tamdʼel kö(ə)zizan qwɛlɨm taugu.</ta>
            <ta e="T8" id="Seg_833" s="T6">kü(ö)sʼe čaʒan.</ta>
            <ta e="T12" id="Seg_834" s="T8">a kɨbanʼäʒa nʼäŋottə qoup(b̂)pa.</ta>
            <ta e="T13" id="Seg_835" s="T12">tʼüːrɨn.</ta>
            <ta e="T15" id="Seg_836" s="T13">qula čaʒat.</ta>
            <ta e="T19" id="Seg_837" s="T15">qudnʼäs qɨbanʼäʒam jassʼ jüɣulgattə.</ta>
            <ta e="T23" id="Seg_838" s="T19">a mekga ʒalkan azun.</ta>
            <ta e="T25" id="Seg_839" s="T23">qɨbanʼäʒa tʼürɨn.</ta>
            <ta e="T27" id="Seg_840" s="T25">man saːdannan.</ta>
            <ta e="T29" id="Seg_841" s="T27">qɨbanʼaʒam üːgunnau.</ta>
            <ta e="T33" id="Seg_842" s="T29">täbɨn sapok natʼen qalɨn.</ta>
            <ta e="T38" id="Seg_843" s="T33">qɨbanʼäʒa watot kuronnɨn nelʼdʼzʼädobɨn.</ta>
            <ta e="T41" id="Seg_844" s="T38">man sapogɨm ügunnau.</ta>
            <ta e="T43" id="Seg_845" s="T41">onän qowan.</ta>
            <ta e="T49" id="Seg_846" s="T43">okkɨr tobou ügulgu, (ɣ)au tobou qouqun.</ta>
            <ta e="T51" id="Seg_847" s="T49">nasel čaǯan.</ta>
            <ta e="T54" id="Seg_848" s="T51">tobou innä watʼükou.</ta>
            <ta e="T59" id="Seg_849" s="T54">a ɣau tobou ilʼlʼe koukon.</ta>
            <ta e="T61" id="Seg_850" s="T59">naseːl tšaǯan.</ta>
            <ta e="T64" id="Seg_851" s="T61">udlau wes kozɨnnau.</ta>
            <ta e="T68" id="Seg_852" s="T64">kɨbanaʒan sapogam toboɣɨtdə sertšau.</ta>
            <ta e="T72" id="Seg_853" s="T68">a nɨtdɨn qɨːbanädäkka nɨŋɨtda.</ta>
            <ta e="T78" id="Seg_854" s="T72">man tʼäran: kɨbanʼäʒan mattə qajɣɨn jen?</ta>
            <ta e="T87" id="Seg_855" s="T78">a qɨba nädän tärɨn: a to näjɣum nalagwatpɨtda.</ta>
            <ta e="T89" id="Seg_856" s="T87">täbɨn äutdä.</ta>
            <ta e="T93" id="Seg_857" s="T89">äutdänä man d̂a tao tʼüan.</ta>
            <ta e="T98" id="Seg_858" s="T93">otdə qɨbɨnʼaʒamdə üɣulgu ne moʒət.</ta>
            <ta e="T103" id="Seg_859" s="T98">tabnä qaj qɨbanʼaʒam ne nada.</ta>
            <ta e="T108" id="Seg_860" s="T103">aːmda i lagwatpa, akoškaɣon mannɨppa.</ta>
            <ta e="T110" id="Seg_861" s="T108">qudäm näss peldɨmeǯau.</ta>
            <ta e="T112" id="Seg_862" s="T110">udlau küzatdə.</ta>
            <ta e="T118" id="Seg_863" s="T112">man täbɨm kak ügulbuzau, nasel ügunnau.</ta>
            <ta e="T124" id="Seg_864" s="T118">man täbnä tʼäran: qajno taftʼet saːdassat.</ta>
            <ta e="T130" id="Seg_865" s="T124">a täp tʼärɨn: man koːlɨblam kaːzan.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_866" s="T1">Man tamdʼel közizan qwɛlɨm taugu. </ta>
            <ta e="T8" id="Seg_867" s="T6">Küsʼe čaʒan. </ta>
            <ta e="T12" id="Seg_868" s="T8">A kɨbanʼäʒa nʼäŋottə qouppa. </ta>
            <ta e="T13" id="Seg_869" s="T12">Tʼüːrɨn. </ta>
            <ta e="T15" id="Seg_870" s="T13">Qula čaʒat. </ta>
            <ta e="T19" id="Seg_871" s="T15">Qudnʼäs qɨbanʼäʒam jassʼ jüɣulgattə. </ta>
            <ta e="T23" id="Seg_872" s="T19">A mekga ʒalkan azun. </ta>
            <ta e="T25" id="Seg_873" s="T23">Qɨbanʼäʒa tʼürɨn. </ta>
            <ta e="T27" id="Seg_874" s="T25">Man saːdannan. </ta>
            <ta e="T29" id="Seg_875" s="T27">Qɨbanʼaʒam üːgunnau. </ta>
            <ta e="T33" id="Seg_876" s="T29">Täbɨn sapok natʼen qalɨn. </ta>
            <ta e="T38" id="Seg_877" s="T33">Qɨbanʼäʒa watot kuronnɨn nelʼdʼzʼä dobɨn. </ta>
            <ta e="T41" id="Seg_878" s="T38">Man sapogɨm ügunnau. </ta>
            <ta e="T43" id="Seg_879" s="T41">Onän qowan. </ta>
            <ta e="T49" id="Seg_880" s="T43">Okkɨr tobou ügulgu, au tobou qouqun. </ta>
            <ta e="T51" id="Seg_881" s="T49">Nasel čaǯan. </ta>
            <ta e="T54" id="Seg_882" s="T51">Tobou innä watʼükou. </ta>
            <ta e="T59" id="Seg_883" s="T54">A ɣau tobou ilʼlʼe koukon. </ta>
            <ta e="T61" id="Seg_884" s="T59">Naseːl čaǯan. </ta>
            <ta e="T64" id="Seg_885" s="T61">Udlau wes kozɨnnau. </ta>
            <ta e="T68" id="Seg_886" s="T64">Kɨbanaʒan sapogam toboɣɨtdə serčau. </ta>
            <ta e="T72" id="Seg_887" s="T68">A nɨtdɨn qɨːbanädäkka nɨŋɨtda. </ta>
            <ta e="T78" id="Seg_888" s="T72">Man tʼäran: “Kɨbanʼäʒan mattə qajɣɨn jen?” </ta>
            <ta e="T87" id="Seg_889" s="T78">A qɨba nädän tärɨn: “A to näjɣum na lagwatpɨtda. </ta>
            <ta e="T89" id="Seg_890" s="T87">Täbɨn äutdä.” </ta>
            <ta e="T93" id="Seg_891" s="T89">Äutdänä man datao tʼüan. </ta>
            <ta e="T98" id="Seg_892" s="T93">Otdə qɨbɨnʼaʒamdə üɣulgu ne moʒət. </ta>
            <ta e="T103" id="Seg_893" s="T98">Tabnä qaj qɨbanʼaʒam ne nada? </ta>
            <ta e="T108" id="Seg_894" s="T103">Aːmda i lagwatpa, akoškaɣon mannɨppa. </ta>
            <ta e="T110" id="Seg_895" s="T108">Qudämnäss peldɨmeǯau. </ta>
            <ta e="T112" id="Seg_896" s="T110">Udlau küzatdə. </ta>
            <ta e="T118" id="Seg_897" s="T112">Man täbɨm kak ügulbuzau, nasel ügunnau. </ta>
            <ta e="T124" id="Seg_898" s="T118">Man täbnä tʼäran: “Qajno taftʼet saːdassat?” </ta>
            <ta e="T130" id="Seg_899" s="T124">A täp tʼärɨn: “Man koːlɨblam kaːzan.” </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_900" s="T1">man</ta>
            <ta e="T3" id="Seg_901" s="T2">tam-dʼel</ta>
            <ta e="T4" id="Seg_902" s="T3">közi-za-n</ta>
            <ta e="T5" id="Seg_903" s="T4">qwɛl-ɨ-m</ta>
            <ta e="T6" id="Seg_904" s="T5">tau-gu</ta>
            <ta e="T7" id="Seg_905" s="T6">küsʼe</ta>
            <ta e="T8" id="Seg_906" s="T7">čaʒa-n</ta>
            <ta e="T9" id="Seg_907" s="T8">a</ta>
            <ta e="T10" id="Seg_908" s="T9">kɨbanʼäʒa</ta>
            <ta e="T11" id="Seg_909" s="T10">nʼäŋo-ttə</ta>
            <ta e="T12" id="Seg_910" s="T11">qou-ppa</ta>
            <ta e="T13" id="Seg_911" s="T12">tʼüːrɨ-n</ta>
            <ta e="T14" id="Seg_912" s="T13">qu-la</ta>
            <ta e="T15" id="Seg_913" s="T14">čaʒa-t</ta>
            <ta e="T16" id="Seg_914" s="T15">qud-nʼä-s</ta>
            <ta e="T17" id="Seg_915" s="T16">qɨbanʼäʒa-m</ta>
            <ta e="T18" id="Seg_916" s="T17">jassʼ</ta>
            <ta e="T19" id="Seg_917" s="T18">jü-ɣul-ga-ttə</ta>
            <ta e="T20" id="Seg_918" s="T19">a</ta>
            <ta e="T21" id="Seg_919" s="T20">mekga</ta>
            <ta e="T22" id="Seg_920" s="T21">ʒalka-n</ta>
            <ta e="T23" id="Seg_921" s="T22">azu-n</ta>
            <ta e="T24" id="Seg_922" s="T23">qɨbanʼäʒa</ta>
            <ta e="T25" id="Seg_923" s="T24">tʼürɨ-n</ta>
            <ta e="T26" id="Seg_924" s="T25">man</ta>
            <ta e="T27" id="Seg_925" s="T26">saːdan-na-n</ta>
            <ta e="T28" id="Seg_926" s="T27">qɨbanʼaʒa-m</ta>
            <ta e="T29" id="Seg_927" s="T28">üː-gu-nna-u</ta>
            <ta e="T30" id="Seg_928" s="T29">täb-ɨ-n</ta>
            <ta e="T31" id="Seg_929" s="T30">sapok</ta>
            <ta e="T32" id="Seg_930" s="T31">natʼe-n</ta>
            <ta e="T33" id="Seg_931" s="T32">qalɨ-n</ta>
            <ta e="T34" id="Seg_932" s="T33">qɨbanʼäʒa</ta>
            <ta e="T35" id="Seg_933" s="T34">wat-o-t</ta>
            <ta e="T36" id="Seg_934" s="T35">kur-on-nɨ-n</ta>
            <ta e="T37" id="Seg_935" s="T36">nelʼdʼzʼä</ta>
            <ta e="T38" id="Seg_936" s="T37">dob-ɨ-n</ta>
            <ta e="T39" id="Seg_937" s="T38">man</ta>
            <ta e="T40" id="Seg_938" s="T39">sapog-ɨ-m</ta>
            <ta e="T41" id="Seg_939" s="T40">ü-gun-na-u</ta>
            <ta e="T42" id="Seg_940" s="T41">onän</ta>
            <ta e="T43" id="Seg_941" s="T42">qow-a-n</ta>
            <ta e="T44" id="Seg_942" s="T43">okkɨr</ta>
            <ta e="T45" id="Seg_943" s="T44">tob-o-u</ta>
            <ta e="T46" id="Seg_944" s="T45">ü-gul-gu</ta>
            <ta e="T47" id="Seg_945" s="T46">au</ta>
            <ta e="T48" id="Seg_946" s="T47">tob-o-u</ta>
            <ta e="T49" id="Seg_947" s="T48">qou-qu-n</ta>
            <ta e="T50" id="Seg_948" s="T49">nasel</ta>
            <ta e="T51" id="Seg_949" s="T50">čaǯa-n</ta>
            <ta e="T52" id="Seg_950" s="T51">tob-o-u</ta>
            <ta e="T53" id="Seg_951" s="T52">innä</ta>
            <ta e="T54" id="Seg_952" s="T53">watʼü-ko-u</ta>
            <ta e="T55" id="Seg_953" s="T54">a</ta>
            <ta e="T56" id="Seg_954" s="T55">ɣau</ta>
            <ta e="T57" id="Seg_955" s="T56">tob-o-u</ta>
            <ta e="T58" id="Seg_956" s="T57">ilʼlʼe</ta>
            <ta e="T59" id="Seg_957" s="T58">kou-ko-n</ta>
            <ta e="T60" id="Seg_958" s="T59">naseːl</ta>
            <ta e="T61" id="Seg_959" s="T60">čaǯa-n</ta>
            <ta e="T62" id="Seg_960" s="T61">ud-la-u</ta>
            <ta e="T63" id="Seg_961" s="T62">wes</ta>
            <ta e="T64" id="Seg_962" s="T63">kozɨn-na-u</ta>
            <ta e="T65" id="Seg_963" s="T64">kɨbanaʒa-n</ta>
            <ta e="T66" id="Seg_964" s="T65">sapog-a-m</ta>
            <ta e="T67" id="Seg_965" s="T66">tob-o-ɣɨtdə</ta>
            <ta e="T68" id="Seg_966" s="T67">ser-ča-u</ta>
            <ta e="T69" id="Seg_967" s="T68">a</ta>
            <ta e="T70" id="Seg_968" s="T69">nɨtdɨ-n</ta>
            <ta e="T71" id="Seg_969" s="T70">qɨːba-nädäk-ka</ta>
            <ta e="T72" id="Seg_970" s="T71">nɨŋɨ-tda</ta>
            <ta e="T73" id="Seg_971" s="T72">man</ta>
            <ta e="T74" id="Seg_972" s="T73">tʼära-n</ta>
            <ta e="T75" id="Seg_973" s="T74">kɨbanʼäʒa-n</ta>
            <ta e="T76" id="Seg_974" s="T75">mat-tə</ta>
            <ta e="T77" id="Seg_975" s="T76">qaj-ɣɨn</ta>
            <ta e="T78" id="Seg_976" s="T77">je-n</ta>
            <ta e="T79" id="Seg_977" s="T78">a</ta>
            <ta e="T80" id="Seg_978" s="T79">qɨba</ta>
            <ta e="T81" id="Seg_979" s="T80">nädän</ta>
            <ta e="T82" id="Seg_980" s="T81">tärɨ-n</ta>
            <ta e="T83" id="Seg_981" s="T82">a</ta>
            <ta e="T84" id="Seg_982" s="T83">to</ta>
            <ta e="T85" id="Seg_983" s="T84">nä-j-ɣum</ta>
            <ta e="T86" id="Seg_984" s="T85">na</ta>
            <ta e="T87" id="Seg_985" s="T86">lagwat-pɨ-tda</ta>
            <ta e="T88" id="Seg_986" s="T87">täb-ɨ-n</ta>
            <ta e="T89" id="Seg_987" s="T88">äu-tdä</ta>
            <ta e="T90" id="Seg_988" s="T89">äu-tdä-nä</ta>
            <ta e="T91" id="Seg_989" s="T90">man</ta>
            <ta e="T92" id="Seg_990" s="T91">datao</ta>
            <ta e="T93" id="Seg_991" s="T92">tʼüa-n</ta>
            <ta e="T94" id="Seg_992" s="T93">otdə</ta>
            <ta e="T95" id="Seg_993" s="T94">qɨbɨnʼaʒa-m-də</ta>
            <ta e="T96" id="Seg_994" s="T95">ü-ɣul-gu</ta>
            <ta e="T99" id="Seg_995" s="T98">tab-nä</ta>
            <ta e="T100" id="Seg_996" s="T99">qaj</ta>
            <ta e="T101" id="Seg_997" s="T100">qɨbanʼaʒa-m</ta>
            <ta e="T102" id="Seg_998" s="T101">ne</ta>
            <ta e="T103" id="Seg_999" s="T102">nada</ta>
            <ta e="T104" id="Seg_1000" s="T103">aːmda</ta>
            <ta e="T105" id="Seg_1001" s="T104">i</ta>
            <ta e="T106" id="Seg_1002" s="T105">lagwat-pa</ta>
            <ta e="T107" id="Seg_1003" s="T106">akoška-ɣon</ta>
            <ta e="T108" id="Seg_1004" s="T107">mannɨ-ppa</ta>
            <ta e="T109" id="Seg_1005" s="T108">qud-ä-m-nä-ss</ta>
            <ta e="T110" id="Seg_1006" s="T109">peldɨ-m-eǯa-u</ta>
            <ta e="T111" id="Seg_1007" s="T110">ud-la-u</ta>
            <ta e="T112" id="Seg_1008" s="T111">küza-tdə</ta>
            <ta e="T113" id="Seg_1009" s="T112">man</ta>
            <ta e="T114" id="Seg_1010" s="T113">täb-ɨ-m</ta>
            <ta e="T115" id="Seg_1011" s="T114">kak</ta>
            <ta e="T116" id="Seg_1012" s="T115">ü-gul-bu-za-u</ta>
            <ta e="T117" id="Seg_1013" s="T116">nasel</ta>
            <ta e="T118" id="Seg_1014" s="T117">ü-gun-na-u</ta>
            <ta e="T119" id="Seg_1015" s="T118">man</ta>
            <ta e="T120" id="Seg_1016" s="T119">täb-nä</ta>
            <ta e="T121" id="Seg_1017" s="T120">tʼära-n</ta>
            <ta e="T122" id="Seg_1018" s="T121">qaj-no</ta>
            <ta e="T123" id="Seg_1019" s="T122">taftʼe-t</ta>
            <ta e="T124" id="Seg_1020" s="T123">saːdas-sa-t</ta>
            <ta e="T125" id="Seg_1021" s="T124">a</ta>
            <ta e="T126" id="Seg_1022" s="T125">täp</ta>
            <ta e="T127" id="Seg_1023" s="T126">tʼärɨ-n</ta>
            <ta e="T128" id="Seg_1024" s="T127">Man</ta>
            <ta e="T129" id="Seg_1025" s="T128">koːlɨb-la-m</ta>
            <ta e="T130" id="Seg_1026" s="T129">kaː-za-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1027" s="T1">man</ta>
            <ta e="T3" id="Seg_1028" s="T2">taw-dʼel</ta>
            <ta e="T4" id="Seg_1029" s="T3">közi-sɨ-ŋ</ta>
            <ta e="T5" id="Seg_1030" s="T4">qwɛl-ɨ-m</ta>
            <ta e="T6" id="Seg_1031" s="T5">taw-gu</ta>
            <ta e="T7" id="Seg_1032" s="T6">kössə</ta>
            <ta e="T8" id="Seg_1033" s="T7">čaǯɨ-ŋ</ta>
            <ta e="T9" id="Seg_1034" s="T8">a</ta>
            <ta e="T10" id="Seg_1035" s="T9">qɨbanʼaǯa</ta>
            <ta e="T11" id="Seg_1036" s="T10">nʼäŋgɨ-ntə</ta>
            <ta e="T12" id="Seg_1037" s="T11">kou-mbɨ</ta>
            <ta e="T13" id="Seg_1038" s="T12">tʼüru-n</ta>
            <ta e="T14" id="Seg_1039" s="T13">qum-la</ta>
            <ta e="T15" id="Seg_1040" s="T14">čaǯɨ-tɨn</ta>
            <ta e="T16" id="Seg_1041" s="T15">kud-näj-s</ta>
            <ta e="T17" id="Seg_1042" s="T16">qɨbanʼaǯa-m</ta>
            <ta e="T18" id="Seg_1043" s="T17">asa</ta>
            <ta e="T19" id="Seg_1044" s="T18">ü-qɨl-ku-tɨn</ta>
            <ta e="T20" id="Seg_1045" s="T19">a</ta>
            <ta e="T21" id="Seg_1046" s="T20">mekka</ta>
            <ta e="T22" id="Seg_1047" s="T21">ʒalka-ŋ</ta>
            <ta e="T23" id="Seg_1048" s="T22">azu-n</ta>
            <ta e="T24" id="Seg_1049" s="T23">qɨbanʼaǯa</ta>
            <ta e="T25" id="Seg_1050" s="T24">tʼüru-n</ta>
            <ta e="T26" id="Seg_1051" s="T25">man</ta>
            <ta e="T27" id="Seg_1052" s="T26">sadɨr-nɨ-ŋ</ta>
            <ta e="T28" id="Seg_1053" s="T27">qɨbanʼaǯa-m</ta>
            <ta e="T29" id="Seg_1054" s="T28">ü-ku-ntɨ-w</ta>
            <ta e="T30" id="Seg_1055" s="T29">täp-ɨ-n</ta>
            <ta e="T31" id="Seg_1056" s="T30">sapog</ta>
            <ta e="T32" id="Seg_1057" s="T31">*natʼe-n</ta>
            <ta e="T33" id="Seg_1058" s="T32">qalɨ-n</ta>
            <ta e="T34" id="Seg_1059" s="T33">qɨbanʼaǯa</ta>
            <ta e="T35" id="Seg_1060" s="T34">watt-ɨ-ntə</ta>
            <ta e="T36" id="Seg_1061" s="T35">kur-ol-nɨ-n</ta>
            <ta e="T37" id="Seg_1062" s="T36">nʼälʼdʼe</ta>
            <ta e="T38" id="Seg_1063" s="T37">tob-ɨ-ŋ</ta>
            <ta e="T39" id="Seg_1064" s="T38">man</ta>
            <ta e="T40" id="Seg_1065" s="T39">sapog-ɨ-m</ta>
            <ta e="T41" id="Seg_1066" s="T40">ü-qɨl-nɨ-w</ta>
            <ta e="T42" id="Seg_1067" s="T41">oneŋ</ta>
            <ta e="T43" id="Seg_1068" s="T42">kou-nɨ-ŋ</ta>
            <ta e="T44" id="Seg_1069" s="T43">okkɨr</ta>
            <ta e="T45" id="Seg_1070" s="T44">tob-ɨ-w</ta>
            <ta e="T46" id="Seg_1071" s="T45">ü-qɨl-ku</ta>
            <ta e="T47" id="Seg_1072" s="T46">au</ta>
            <ta e="T48" id="Seg_1073" s="T47">tob-ɨ-w</ta>
            <ta e="T49" id="Seg_1074" s="T48">kou-ku-n</ta>
            <ta e="T50" id="Seg_1075" s="T49">naseːl</ta>
            <ta e="T51" id="Seg_1076" s="T50">čaǯɨ-n</ta>
            <ta e="T52" id="Seg_1077" s="T51">tob-ɨ-w</ta>
            <ta e="T53" id="Seg_1078" s="T52">innä</ta>
            <ta e="T54" id="Seg_1079" s="T53">watʼtʼi-ku-w</ta>
            <ta e="T55" id="Seg_1080" s="T54">a</ta>
            <ta e="T56" id="Seg_1081" s="T55">au</ta>
            <ta e="T57" id="Seg_1082" s="T56">tob-ɨ-w</ta>
            <ta e="T58" id="Seg_1083" s="T57">ilʼlʼe</ta>
            <ta e="T59" id="Seg_1084" s="T58">kou-ku-n</ta>
            <ta e="T60" id="Seg_1085" s="T59">naseːl</ta>
            <ta e="T61" id="Seg_1086" s="T60">čaǯɨ-ŋ</ta>
            <ta e="T62" id="Seg_1087" s="T61">ut-la-w</ta>
            <ta e="T63" id="Seg_1088" s="T62">wesʼ</ta>
            <ta e="T64" id="Seg_1089" s="T63">kozəl-nɨ-w</ta>
            <ta e="T65" id="Seg_1090" s="T64">qɨbanʼaǯa-n</ta>
            <ta e="T66" id="Seg_1091" s="T65">sapog-ɨ-m</ta>
            <ta e="T67" id="Seg_1092" s="T66">tob-ɨ-qɨntɨ</ta>
            <ta e="T68" id="Seg_1093" s="T67">ser-ču-w</ta>
            <ta e="T69" id="Seg_1094" s="T68">a</ta>
            <ta e="T70" id="Seg_1095" s="T69">*natʼe-n</ta>
            <ta e="T71" id="Seg_1096" s="T70">qɨba-nädek-ka</ta>
            <ta e="T72" id="Seg_1097" s="T71">nɨŋgɨ-ntɨ</ta>
            <ta e="T73" id="Seg_1098" s="T72">man</ta>
            <ta e="T74" id="Seg_1099" s="T73">tʼärɨ-ŋ</ta>
            <ta e="T75" id="Seg_1100" s="T74">qɨbanʼaǯa-n</ta>
            <ta e="T76" id="Seg_1101" s="T75">maːt-tə</ta>
            <ta e="T77" id="Seg_1102" s="T76">qaj-qɨn</ta>
            <ta e="T78" id="Seg_1103" s="T77">eː-n</ta>
            <ta e="T79" id="Seg_1104" s="T78">a</ta>
            <ta e="T80" id="Seg_1105" s="T79">qɨba</ta>
            <ta e="T81" id="Seg_1106" s="T80">nädek</ta>
            <ta e="T82" id="Seg_1107" s="T81">tʼärɨ-n</ta>
            <ta e="T83" id="Seg_1108" s="T82">a</ta>
            <ta e="T84" id="Seg_1109" s="T83">to</ta>
            <ta e="T85" id="Seg_1110" s="T84">ne-lʼ-qum</ta>
            <ta e="T86" id="Seg_1111" s="T85">na</ta>
            <ta e="T87" id="Seg_1112" s="T86">laːɣwat-mbɨ-ntɨ</ta>
            <ta e="T88" id="Seg_1113" s="T87">täp-ɨ-n</ta>
            <ta e="T89" id="Seg_1114" s="T88">awa-tə</ta>
            <ta e="T90" id="Seg_1115" s="T89">awa-tə-nä</ta>
            <ta e="T91" id="Seg_1116" s="T90">man</ta>
            <ta e="T92" id="Seg_1117" s="T91">tatawa</ta>
            <ta e="T93" id="Seg_1118" s="T92">tüu-ŋ</ta>
            <ta e="T94" id="Seg_1119" s="T93">ondə</ta>
            <ta e="T95" id="Seg_1120" s="T94">qɨbanʼaǯa-m-tə</ta>
            <ta e="T96" id="Seg_1121" s="T95">ü-qɨl-gu</ta>
            <ta e="T99" id="Seg_1122" s="T98">täp-nä</ta>
            <ta e="T100" id="Seg_1123" s="T99">qaj</ta>
            <ta e="T101" id="Seg_1124" s="T100">qɨbanʼaǯa-m</ta>
            <ta e="T102" id="Seg_1125" s="T101">ne</ta>
            <ta e="T103" id="Seg_1126" s="T102">nadə</ta>
            <ta e="T104" id="Seg_1127" s="T103">amdɨ</ta>
            <ta e="T105" id="Seg_1128" s="T104">i</ta>
            <ta e="T106" id="Seg_1129" s="T105">laːɣwat-mbɨ</ta>
            <ta e="T107" id="Seg_1130" s="T106">akoška-qɨn</ta>
            <ta e="T108" id="Seg_1131" s="T107">*mantɨ-mbɨ</ta>
            <ta e="T109" id="Seg_1132" s="T108">kud-ɨ-m-näj-s</ta>
            <ta e="T110" id="Seg_1133" s="T109">päldu-mbɨ-enǯɨ-w</ta>
            <ta e="T111" id="Seg_1134" s="T110">ut-la-w</ta>
            <ta e="T112" id="Seg_1135" s="T111">qüzu-tɨn</ta>
            <ta e="T113" id="Seg_1136" s="T112">man</ta>
            <ta e="T114" id="Seg_1137" s="T113">täp-ɨ-m</ta>
            <ta e="T115" id="Seg_1138" s="T114">kak</ta>
            <ta e="T116" id="Seg_1139" s="T115">ü-qɨl-mbɨ-sɨ-w</ta>
            <ta e="T117" id="Seg_1140" s="T116">naseːl</ta>
            <ta e="T118" id="Seg_1141" s="T117">ü-qɨl-nɨ-w</ta>
            <ta e="T119" id="Seg_1142" s="T118">man</ta>
            <ta e="T120" id="Seg_1143" s="T119">täp-nä</ta>
            <ta e="T121" id="Seg_1144" s="T120">tʼärɨ-ŋ</ta>
            <ta e="T122" id="Seg_1145" s="T121">qaj-no</ta>
            <ta e="T123" id="Seg_1146" s="T122">tautʼe-ntə</ta>
            <ta e="T124" id="Seg_1147" s="T123">sadɨr-sɨ-ntə</ta>
            <ta e="T125" id="Seg_1148" s="T124">a</ta>
            <ta e="T126" id="Seg_1149" s="T125">täp</ta>
            <ta e="T127" id="Seg_1150" s="T126">tʼärɨ-n</ta>
            <ta e="T128" id="Seg_1151" s="T127">man</ta>
            <ta e="T129" id="Seg_1152" s="T128">golub-la-m</ta>
            <ta e="T130" id="Seg_1153" s="T129">qa-sɨ-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1154" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_1155" s="T2">this-day.[NOM]</ta>
            <ta e="T4" id="Seg_1156" s="T3">go-PST-1SG.S</ta>
            <ta e="T5" id="Seg_1157" s="T4">fish-EP-ACC</ta>
            <ta e="T6" id="Seg_1158" s="T5">buy-INF</ta>
            <ta e="T7" id="Seg_1159" s="T6">backward</ta>
            <ta e="T8" id="Seg_1160" s="T7">go-1SG.S</ta>
            <ta e="T9" id="Seg_1161" s="T8">and</ta>
            <ta e="T10" id="Seg_1162" s="T9">child.[NOM]</ta>
            <ta e="T11" id="Seg_1163" s="T10">silt-ILL</ta>
            <ta e="T12" id="Seg_1164" s="T11">drown-PST.NAR.[3SG.S]</ta>
            <ta e="T13" id="Seg_1165" s="T12">cry-3SG.S</ta>
            <ta e="T14" id="Seg_1166" s="T13">human.being-PL.[NOM]</ta>
            <ta e="T15" id="Seg_1167" s="T14">go-3PL</ta>
            <ta e="T16" id="Seg_1168" s="T15">who.[NOM]-EMPH-NEG</ta>
            <ta e="T17" id="Seg_1169" s="T16">child-ACC</ta>
            <ta e="T18" id="Seg_1170" s="T17">NEG</ta>
            <ta e="T19" id="Seg_1171" s="T18">pull-DRV-HAB-3PL</ta>
            <ta e="T20" id="Seg_1172" s="T19">and</ta>
            <ta e="T21" id="Seg_1173" s="T20">I.ALL</ta>
            <ta e="T22" id="Seg_1174" s="T21">feel.sorry-ADVZ</ta>
            <ta e="T23" id="Seg_1175" s="T22">become-3SG.S</ta>
            <ta e="T24" id="Seg_1176" s="T23">child.[NOM]</ta>
            <ta e="T25" id="Seg_1177" s="T24">cry-3SG.S</ta>
            <ta e="T26" id="Seg_1178" s="T25">I.NOM</ta>
            <ta e="T27" id="Seg_1179" s="T26">wander-CO-1SG.S</ta>
            <ta e="T28" id="Seg_1180" s="T27">child-ACC</ta>
            <ta e="T29" id="Seg_1181" s="T28">pull-HAB-INFER-1SG.O</ta>
            <ta e="T30" id="Seg_1182" s="T29">(s)he-EP-GEN</ta>
            <ta e="T31" id="Seg_1183" s="T30">boot.[NOM]</ta>
            <ta e="T32" id="Seg_1184" s="T31">there-ADV.LOC</ta>
            <ta e="T33" id="Seg_1185" s="T32">stay-3SG.S</ta>
            <ta e="T34" id="Seg_1186" s="T33">child.[NOM]</ta>
            <ta e="T35" id="Seg_1187" s="T34">road-EP-ILL</ta>
            <ta e="T36" id="Seg_1188" s="T35">run-MOM-CO-3SG.S</ta>
            <ta e="T37" id="Seg_1189" s="T36">bare</ta>
            <ta e="T38" id="Seg_1190" s="T37">leg-EP-ADVZ</ta>
            <ta e="T39" id="Seg_1191" s="T38">I.NOM</ta>
            <ta e="T40" id="Seg_1192" s="T39">boot-EP-ACC</ta>
            <ta e="T41" id="Seg_1193" s="T40">pull-DRV-CO-1SG.O</ta>
            <ta e="T42" id="Seg_1194" s="T41">oneself.1SG</ta>
            <ta e="T43" id="Seg_1195" s="T42">drown-CO-1SG.S</ta>
            <ta e="T44" id="Seg_1196" s="T43">one</ta>
            <ta e="T45" id="Seg_1197" s="T44">leg.[NOM]-EP-1SG</ta>
            <ta e="T46" id="Seg_1198" s="T45">pull-DRV-HAB.[3SG.S]</ta>
            <ta e="T47" id="Seg_1199" s="T46">other</ta>
            <ta e="T48" id="Seg_1200" s="T47">leg.[NOM]-EP-1SG</ta>
            <ta e="T49" id="Seg_1201" s="T48">drown-HAB-3SG.S</ta>
            <ta e="T50" id="Seg_1202" s="T49">hardly</ta>
            <ta e="T51" id="Seg_1203" s="T50">run-3SG.S</ta>
            <ta e="T52" id="Seg_1204" s="T51">leg.[NOM]-EP-1SG</ta>
            <ta e="T53" id="Seg_1205" s="T52">up</ta>
            <ta e="T54" id="Seg_1206" s="T53">lift-HAB-1SG.O</ta>
            <ta e="T55" id="Seg_1207" s="T54">and</ta>
            <ta e="T56" id="Seg_1208" s="T55">other</ta>
            <ta e="T57" id="Seg_1209" s="T56">leg.[NOM]-EP-1SG</ta>
            <ta e="T58" id="Seg_1210" s="T57">down</ta>
            <ta e="T59" id="Seg_1211" s="T58">drown-HAB-3SG.S</ta>
            <ta e="T60" id="Seg_1212" s="T59">hardly</ta>
            <ta e="T61" id="Seg_1213" s="T60">go-1SG.S</ta>
            <ta e="T62" id="Seg_1214" s="T61">hand-PL.[NOM]-1SG</ta>
            <ta e="T63" id="Seg_1215" s="T62">all</ta>
            <ta e="T64" id="Seg_1216" s="T63">make.dirty-CO-1SG.O</ta>
            <ta e="T65" id="Seg_1217" s="T64">child-GEN</ta>
            <ta e="T66" id="Seg_1218" s="T65">boot-EP-ACC</ta>
            <ta e="T67" id="Seg_1219" s="T66">leg-EP-ILL.3SG</ta>
            <ta e="T68" id="Seg_1220" s="T67">put.on-TR-1SG.O</ta>
            <ta e="T69" id="Seg_1221" s="T68">and</ta>
            <ta e="T70" id="Seg_1222" s="T69">there-ADV.LOC</ta>
            <ta e="T71" id="Seg_1223" s="T70">small-girl-DIM.[NOM]</ta>
            <ta e="T72" id="Seg_1224" s="T71">stand-INFER.[3SG.S]</ta>
            <ta e="T73" id="Seg_1225" s="T72">I.NOM</ta>
            <ta e="T74" id="Seg_1226" s="T73">say-1SG.S</ta>
            <ta e="T75" id="Seg_1227" s="T74">child-GEN</ta>
            <ta e="T76" id="Seg_1228" s="T75">tent.[NOM]-3SG</ta>
            <ta e="T77" id="Seg_1229" s="T76">what-LOC</ta>
            <ta e="T78" id="Seg_1230" s="T77">be-3SG.S</ta>
            <ta e="T79" id="Seg_1231" s="T78">and</ta>
            <ta e="T80" id="Seg_1232" s="T79">small</ta>
            <ta e="T81" id="Seg_1233" s="T80">girl.[NOM]</ta>
            <ta e="T82" id="Seg_1234" s="T81">say-3SG.S</ta>
            <ta e="T83" id="Seg_1235" s="T82">and</ta>
            <ta e="T84" id="Seg_1236" s="T83">that</ta>
            <ta e="T85" id="Seg_1237" s="T84">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T86" id="Seg_1238" s="T85">here</ta>
            <ta e="T87" id="Seg_1239" s="T86">begin.to.laugh-DUR-INFER.[3SG.S]</ta>
            <ta e="T88" id="Seg_1240" s="T87">(s)he-EP-GEN</ta>
            <ta e="T89" id="Seg_1241" s="T88">mother.[NOM]-3SG</ta>
            <ta e="T90" id="Seg_1242" s="T89">mother-3SG-ALL</ta>
            <ta e="T91" id="Seg_1243" s="T90">I.NOM</ta>
            <ta e="T92" id="Seg_1244" s="T91">to.such.extent</ta>
            <ta e="T93" id="Seg_1245" s="T92">get.angry-1SG.S</ta>
            <ta e="T94" id="Seg_1246" s="T93">oneself.3SG</ta>
            <ta e="T95" id="Seg_1247" s="T94">child-ACC-3SG</ta>
            <ta e="T96" id="Seg_1248" s="T95">pull-DRV-INF</ta>
            <ta e="T99" id="Seg_1249" s="T98">(s)he-ALL</ta>
            <ta e="T100" id="Seg_1250" s="T99">what</ta>
            <ta e="T101" id="Seg_1251" s="T100">child-ACC</ta>
            <ta e="T102" id="Seg_1252" s="T101">NEG</ta>
            <ta e="T103" id="Seg_1253" s="T102">one.should</ta>
            <ta e="T104" id="Seg_1254" s="T103">sit.[3SG.S]</ta>
            <ta e="T105" id="Seg_1255" s="T104">and</ta>
            <ta e="T106" id="Seg_1256" s="T105">begin.to.laugh-DUR.[3SG.S]</ta>
            <ta e="T107" id="Seg_1257" s="T106">window-LOC</ta>
            <ta e="T108" id="Seg_1258" s="T107">look-DUR.[3SG.S]</ta>
            <ta e="T109" id="Seg_1259" s="T108">who-EP-ACC-EMPH-NEG</ta>
            <ta e="T110" id="Seg_1260" s="T109">help-DUR-FUT-1SG.O</ta>
            <ta e="T111" id="Seg_1261" s="T110">hand-PL.[NOM]-1SG</ta>
            <ta e="T112" id="Seg_1262" s="T111">ache-3PL</ta>
            <ta e="T113" id="Seg_1263" s="T112">I.NOM</ta>
            <ta e="T114" id="Seg_1264" s="T113">(s)he-EP-ACC</ta>
            <ta e="T115" id="Seg_1265" s="T114">how</ta>
            <ta e="T116" id="Seg_1266" s="T115">pull-DRV-DUR-PST-1SG.O</ta>
            <ta e="T117" id="Seg_1267" s="T116">hardly</ta>
            <ta e="T118" id="Seg_1268" s="T117">pull-DRV-CO-1SG.O</ta>
            <ta e="T119" id="Seg_1269" s="T118">I.NOM</ta>
            <ta e="T120" id="Seg_1270" s="T119">(s)he-ALL</ta>
            <ta e="T121" id="Seg_1271" s="T120">say-1SG.S</ta>
            <ta e="T122" id="Seg_1272" s="T121">what-TRL</ta>
            <ta e="T123" id="Seg_1273" s="T122">here-ILL</ta>
            <ta e="T124" id="Seg_1274" s="T123">wander-PST-2SG.S</ta>
            <ta e="T125" id="Seg_1275" s="T124">and</ta>
            <ta e="T126" id="Seg_1276" s="T125">(s)he.[NOM]</ta>
            <ta e="T127" id="Seg_1277" s="T126">say-3SG.S</ta>
            <ta e="T128" id="Seg_1278" s="T127">I.NOM</ta>
            <ta e="T129" id="Seg_1279" s="T128">pigeon-PL-ACC</ta>
            <ta e="T130" id="Seg_1280" s="T129">pursue-PST-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1281" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_1282" s="T2">этот-день.[NOM]</ta>
            <ta e="T4" id="Seg_1283" s="T3">идти-PST-1SG.S</ta>
            <ta e="T5" id="Seg_1284" s="T4">рыба-EP-ACC</ta>
            <ta e="T6" id="Seg_1285" s="T5">купить-INF</ta>
            <ta e="T7" id="Seg_1286" s="T6">назад</ta>
            <ta e="T8" id="Seg_1287" s="T7">ходить-1SG.S</ta>
            <ta e="T9" id="Seg_1288" s="T8">а</ta>
            <ta e="T10" id="Seg_1289" s="T9">ребенок.[NOM]</ta>
            <ta e="T11" id="Seg_1290" s="T10">ил-ILL</ta>
            <ta e="T12" id="Seg_1291" s="T11">утонуть-PST.NAR.[3SG.S]</ta>
            <ta e="T13" id="Seg_1292" s="T12">плакать-3SG.S</ta>
            <ta e="T14" id="Seg_1293" s="T13">человек-PL.[NOM]</ta>
            <ta e="T15" id="Seg_1294" s="T14">ходить-3PL</ta>
            <ta e="T16" id="Seg_1295" s="T15">кто.[NOM]-EMPH-NEG</ta>
            <ta e="T17" id="Seg_1296" s="T16">ребенок-ACC</ta>
            <ta e="T18" id="Seg_1297" s="T17">NEG</ta>
            <ta e="T19" id="Seg_1298" s="T18">тащить-DRV-HAB-3PL</ta>
            <ta e="T20" id="Seg_1299" s="T19">а</ta>
            <ta e="T21" id="Seg_1300" s="T20">я.ALL</ta>
            <ta e="T22" id="Seg_1301" s="T21">жалко-ADVZ</ta>
            <ta e="T23" id="Seg_1302" s="T22">стать-3SG.S</ta>
            <ta e="T24" id="Seg_1303" s="T23">ребенок.[NOM]</ta>
            <ta e="T25" id="Seg_1304" s="T24">плакать-3SG.S</ta>
            <ta e="T26" id="Seg_1305" s="T25">я.NOM</ta>
            <ta e="T27" id="Seg_1306" s="T26">бродить-CO-1SG.S</ta>
            <ta e="T28" id="Seg_1307" s="T27">ребенок-ACC</ta>
            <ta e="T29" id="Seg_1308" s="T28">тащить-HAB-INFER-1SG.O</ta>
            <ta e="T30" id="Seg_1309" s="T29">он(а)-EP-GEN</ta>
            <ta e="T31" id="Seg_1310" s="T30">сапог.[NOM]</ta>
            <ta e="T32" id="Seg_1311" s="T31">туда-ADV.LOC</ta>
            <ta e="T33" id="Seg_1312" s="T32">остаться-3SG.S</ta>
            <ta e="T34" id="Seg_1313" s="T33">ребенок.[NOM]</ta>
            <ta e="T35" id="Seg_1314" s="T34">дорога-EP-ILL</ta>
            <ta e="T36" id="Seg_1315" s="T35">бегать-MOM-CO-3SG.S</ta>
            <ta e="T37" id="Seg_1316" s="T36">голый</ta>
            <ta e="T38" id="Seg_1317" s="T37">нога-EP-ADVZ</ta>
            <ta e="T39" id="Seg_1318" s="T38">я.NOM</ta>
            <ta e="T40" id="Seg_1319" s="T39">сапог-EP-ACC</ta>
            <ta e="T41" id="Seg_1320" s="T40">тащить-DRV-CO-1SG.O</ta>
            <ta e="T42" id="Seg_1321" s="T41">сам.1SG</ta>
            <ta e="T43" id="Seg_1322" s="T42">утонуть-CO-1SG.S</ta>
            <ta e="T44" id="Seg_1323" s="T43">один</ta>
            <ta e="T45" id="Seg_1324" s="T44">нога.[NOM]-EP-1SG</ta>
            <ta e="T46" id="Seg_1325" s="T45">тащить-DRV-HAB.[3SG.S]</ta>
            <ta e="T47" id="Seg_1326" s="T46">другой</ta>
            <ta e="T48" id="Seg_1327" s="T47">нога.[NOM]-EP-1SG</ta>
            <ta e="T49" id="Seg_1328" s="T48">утонуть-HAB-3SG.S</ta>
            <ta e="T50" id="Seg_1329" s="T49">насилу</ta>
            <ta e="T51" id="Seg_1330" s="T50">бегать-3SG.S</ta>
            <ta e="T52" id="Seg_1331" s="T51">нога.[NOM]-EP-1SG</ta>
            <ta e="T53" id="Seg_1332" s="T52">наверх</ta>
            <ta e="T54" id="Seg_1333" s="T53">поднять-HAB-1SG.O</ta>
            <ta e="T55" id="Seg_1334" s="T54">а</ta>
            <ta e="T56" id="Seg_1335" s="T55">другой</ta>
            <ta e="T57" id="Seg_1336" s="T56">нога.[NOM]-EP-1SG</ta>
            <ta e="T58" id="Seg_1337" s="T57">вниз</ta>
            <ta e="T59" id="Seg_1338" s="T58">утонуть-HAB-3SG.S</ta>
            <ta e="T60" id="Seg_1339" s="T59">насилу</ta>
            <ta e="T61" id="Seg_1340" s="T60">ходить-1SG.S</ta>
            <ta e="T62" id="Seg_1341" s="T61">рука-PL.[NOM]-1SG</ta>
            <ta e="T63" id="Seg_1342" s="T62">весь</ta>
            <ta e="T64" id="Seg_1343" s="T63">испачкать-CO-1SG.O</ta>
            <ta e="T65" id="Seg_1344" s="T64">ребенок-GEN</ta>
            <ta e="T66" id="Seg_1345" s="T65">сапог-EP-ACC</ta>
            <ta e="T67" id="Seg_1346" s="T66">нога-EP-ILL.3SG</ta>
            <ta e="T68" id="Seg_1347" s="T67">надеть-TR-1SG.O</ta>
            <ta e="T69" id="Seg_1348" s="T68">а</ta>
            <ta e="T70" id="Seg_1349" s="T69">туда-ADV.LOC</ta>
            <ta e="T71" id="Seg_1350" s="T70">маленький-девушка-DIM.[NOM]</ta>
            <ta e="T72" id="Seg_1351" s="T71">стоять-INFER.[3SG.S]</ta>
            <ta e="T73" id="Seg_1352" s="T72">я.NOM</ta>
            <ta e="T74" id="Seg_1353" s="T73">сказать-1SG.S</ta>
            <ta e="T75" id="Seg_1354" s="T74">ребенок-GEN</ta>
            <ta e="T76" id="Seg_1355" s="T75">чум.[NOM]-3SG</ta>
            <ta e="T77" id="Seg_1356" s="T76">что-LOC</ta>
            <ta e="T78" id="Seg_1357" s="T77">быть-3SG.S</ta>
            <ta e="T79" id="Seg_1358" s="T78">а</ta>
            <ta e="T80" id="Seg_1359" s="T79">маленький</ta>
            <ta e="T81" id="Seg_1360" s="T80">девушка.[NOM]</ta>
            <ta e="T82" id="Seg_1361" s="T81">сказать-3SG.S</ta>
            <ta e="T83" id="Seg_1362" s="T82">а</ta>
            <ta e="T84" id="Seg_1363" s="T83">тот</ta>
            <ta e="T85" id="Seg_1364" s="T84">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T86" id="Seg_1365" s="T85">ну</ta>
            <ta e="T87" id="Seg_1366" s="T86">засмеяться-DUR-INFER.[3SG.S]</ta>
            <ta e="T88" id="Seg_1367" s="T87">он(а)-EP-GEN</ta>
            <ta e="T89" id="Seg_1368" s="T88">мать.[NOM]-3SG</ta>
            <ta e="T90" id="Seg_1369" s="T89">мать-3SG-ALL</ta>
            <ta e="T91" id="Seg_1370" s="T90">я.NOM</ta>
            <ta e="T92" id="Seg_1371" s="T91">до.того</ta>
            <ta e="T93" id="Seg_1372" s="T92">рассердиться-1SG.S</ta>
            <ta e="T94" id="Seg_1373" s="T93">сам.3SG</ta>
            <ta e="T95" id="Seg_1374" s="T94">ребенок-ACC-3SG</ta>
            <ta e="T96" id="Seg_1375" s="T95">тащить-DRV-INF</ta>
            <ta e="T99" id="Seg_1376" s="T98">он(а)-ALL</ta>
            <ta e="T100" id="Seg_1377" s="T99">что</ta>
            <ta e="T101" id="Seg_1378" s="T100">ребенок-ACC</ta>
            <ta e="T102" id="Seg_1379" s="T101">NEG</ta>
            <ta e="T103" id="Seg_1380" s="T102">надо</ta>
            <ta e="T104" id="Seg_1381" s="T103">сидеть.[3SG.S]</ta>
            <ta e="T105" id="Seg_1382" s="T104">и</ta>
            <ta e="T106" id="Seg_1383" s="T105">засмеяться-DUR.[3SG.S]</ta>
            <ta e="T107" id="Seg_1384" s="T106">окно-LOC</ta>
            <ta e="T108" id="Seg_1385" s="T107">посмотреть-DUR.[3SG.S]</ta>
            <ta e="T109" id="Seg_1386" s="T108">кто-EP-ACC-EMPH-NEG</ta>
            <ta e="T110" id="Seg_1387" s="T109">помочь-DUR-FUT-1SG.O</ta>
            <ta e="T111" id="Seg_1388" s="T110">рука-PL.[NOM]-1SG</ta>
            <ta e="T112" id="Seg_1389" s="T111">болеть-3PL</ta>
            <ta e="T113" id="Seg_1390" s="T112">я.NOM</ta>
            <ta e="T114" id="Seg_1391" s="T113">он(а)-EP-ACC</ta>
            <ta e="T115" id="Seg_1392" s="T114">как</ta>
            <ta e="T116" id="Seg_1393" s="T115">тащить-DRV-DUR-PST-1SG.O</ta>
            <ta e="T117" id="Seg_1394" s="T116">насилу</ta>
            <ta e="T118" id="Seg_1395" s="T117">тащить-DRV-CO-1SG.O</ta>
            <ta e="T119" id="Seg_1396" s="T118">я.NOM</ta>
            <ta e="T120" id="Seg_1397" s="T119">он(а)-ALL</ta>
            <ta e="T121" id="Seg_1398" s="T120">сказать-1SG.S</ta>
            <ta e="T122" id="Seg_1399" s="T121">что-TRL</ta>
            <ta e="T123" id="Seg_1400" s="T122">сюда-ILL</ta>
            <ta e="T124" id="Seg_1401" s="T123">бродить-PST-2SG.S</ta>
            <ta e="T125" id="Seg_1402" s="T124">а</ta>
            <ta e="T126" id="Seg_1403" s="T125">он(а).[NOM]</ta>
            <ta e="T127" id="Seg_1404" s="T126">сказать-3SG.S</ta>
            <ta e="T128" id="Seg_1405" s="T127">я.NOM</ta>
            <ta e="T129" id="Seg_1406" s="T128">голубь-PL-ACC</ta>
            <ta e="T130" id="Seg_1407" s="T129">преследовать-PST-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1408" s="T1">pers</ta>
            <ta e="T3" id="Seg_1409" s="T2">dem-n.[n:case]</ta>
            <ta e="T4" id="Seg_1410" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_1411" s="T4">n-n:ins-n:case</ta>
            <ta e="T6" id="Seg_1412" s="T5">v-v:inf</ta>
            <ta e="T7" id="Seg_1413" s="T6">adv</ta>
            <ta e="T8" id="Seg_1414" s="T7">v-v:pn</ta>
            <ta e="T9" id="Seg_1415" s="T8">conj</ta>
            <ta e="T10" id="Seg_1416" s="T9">n.[n:case]</ta>
            <ta e="T11" id="Seg_1417" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_1418" s="T11">v-v:tense.[v:pn]</ta>
            <ta e="T13" id="Seg_1419" s="T12">v-v:pn</ta>
            <ta e="T14" id="Seg_1420" s="T13">n-n:num.[n:case]</ta>
            <ta e="T15" id="Seg_1421" s="T14">v-v:pn</ta>
            <ta e="T16" id="Seg_1422" s="T15">interrog.[n:case]-clit-clit</ta>
            <ta e="T17" id="Seg_1423" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_1424" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_1425" s="T18">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T20" id="Seg_1426" s="T19">conj</ta>
            <ta e="T21" id="Seg_1427" s="T20">pers</ta>
            <ta e="T22" id="Seg_1428" s="T21">adv-adj&gt;adv</ta>
            <ta e="T23" id="Seg_1429" s="T22">v-v:pn</ta>
            <ta e="T24" id="Seg_1430" s="T23">n.[n:case]</ta>
            <ta e="T25" id="Seg_1431" s="T24">v-v:pn</ta>
            <ta e="T26" id="Seg_1432" s="T25">pers</ta>
            <ta e="T27" id="Seg_1433" s="T26">v-v:ins-v:pn</ta>
            <ta e="T28" id="Seg_1434" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_1435" s="T28">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T30" id="Seg_1436" s="T29">pers-n:ins-n:case</ta>
            <ta e="T31" id="Seg_1437" s="T30">n.[n:case]</ta>
            <ta e="T32" id="Seg_1438" s="T31">adv-adv:case</ta>
            <ta e="T33" id="Seg_1439" s="T32">v-v:pn</ta>
            <ta e="T34" id="Seg_1440" s="T33">n.[n:case]</ta>
            <ta e="T35" id="Seg_1441" s="T34">n-n:ins-n:case</ta>
            <ta e="T36" id="Seg_1442" s="T35">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T37" id="Seg_1443" s="T36">adj</ta>
            <ta e="T38" id="Seg_1444" s="T37">n-n:ins-n&gt;adv</ta>
            <ta e="T39" id="Seg_1445" s="T38">pers</ta>
            <ta e="T40" id="Seg_1446" s="T39">n-n:ins-n:case</ta>
            <ta e="T41" id="Seg_1447" s="T40">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T42" id="Seg_1448" s="T41">emphpro</ta>
            <ta e="T43" id="Seg_1449" s="T42">v-v:ins-v:pn</ta>
            <ta e="T44" id="Seg_1450" s="T43">num</ta>
            <ta e="T45" id="Seg_1451" s="T44">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T46" id="Seg_1452" s="T45">v-v&gt;v-v&gt;v.[v:pn]</ta>
            <ta e="T47" id="Seg_1453" s="T46">adj</ta>
            <ta e="T48" id="Seg_1454" s="T47">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T49" id="Seg_1455" s="T48">v-v&gt;v-v:pn</ta>
            <ta e="T50" id="Seg_1456" s="T49">adv</ta>
            <ta e="T51" id="Seg_1457" s="T50">v-v:pn</ta>
            <ta e="T52" id="Seg_1458" s="T51">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T53" id="Seg_1459" s="T52">adv</ta>
            <ta e="T54" id="Seg_1460" s="T53">v-v&gt;v-v:pn</ta>
            <ta e="T55" id="Seg_1461" s="T54">conj</ta>
            <ta e="T56" id="Seg_1462" s="T55">adj</ta>
            <ta e="T57" id="Seg_1463" s="T56">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T58" id="Seg_1464" s="T57">preverb</ta>
            <ta e="T59" id="Seg_1465" s="T58">v-v&gt;v-v:pn</ta>
            <ta e="T60" id="Seg_1466" s="T59">adv</ta>
            <ta e="T61" id="Seg_1467" s="T60">v-v:pn</ta>
            <ta e="T62" id="Seg_1468" s="T61">n-n:num.[n:case]-n:poss</ta>
            <ta e="T63" id="Seg_1469" s="T62">quant</ta>
            <ta e="T64" id="Seg_1470" s="T63">v-v:ins-v:pn</ta>
            <ta e="T65" id="Seg_1471" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_1472" s="T65">n-n:ins-n:case</ta>
            <ta e="T67" id="Seg_1473" s="T66">n-n:ins-n:case.poss</ta>
            <ta e="T68" id="Seg_1474" s="T67">v-v&gt;v-v:pn</ta>
            <ta e="T69" id="Seg_1475" s="T68">conj</ta>
            <ta e="T70" id="Seg_1476" s="T69">adv-adv:case</ta>
            <ta e="T71" id="Seg_1477" s="T70">adj-n-n&gt;n.[n:case]</ta>
            <ta e="T72" id="Seg_1478" s="T71">v-v:mood.[v:pn]</ta>
            <ta e="T73" id="Seg_1479" s="T72">pers</ta>
            <ta e="T74" id="Seg_1480" s="T73">v-v:pn</ta>
            <ta e="T75" id="Seg_1481" s="T74">n-n:case</ta>
            <ta e="T76" id="Seg_1482" s="T75">n.[n:case]-n:poss</ta>
            <ta e="T77" id="Seg_1483" s="T76">interrog-n:case</ta>
            <ta e="T78" id="Seg_1484" s="T77">v-v:pn</ta>
            <ta e="T79" id="Seg_1485" s="T78">conj</ta>
            <ta e="T80" id="Seg_1486" s="T79">adj</ta>
            <ta e="T81" id="Seg_1487" s="T80">n.[n:case]</ta>
            <ta e="T82" id="Seg_1488" s="T81">v-v:pn</ta>
            <ta e="T83" id="Seg_1489" s="T82">conj</ta>
            <ta e="T84" id="Seg_1490" s="T83">dem</ta>
            <ta e="T85" id="Seg_1491" s="T84">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T86" id="Seg_1492" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_1493" s="T86">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T88" id="Seg_1494" s="T87">pers-n:ins-n:case</ta>
            <ta e="T89" id="Seg_1495" s="T88">n.[n:case]-n:poss</ta>
            <ta e="T90" id="Seg_1496" s="T89">n-n:poss-n:case</ta>
            <ta e="T91" id="Seg_1497" s="T90">pers</ta>
            <ta e="T92" id="Seg_1498" s="T91">adv</ta>
            <ta e="T93" id="Seg_1499" s="T92">v-v:pn</ta>
            <ta e="T94" id="Seg_1500" s="T93">emphpro</ta>
            <ta e="T95" id="Seg_1501" s="T94">n-n:case-n:poss</ta>
            <ta e="T96" id="Seg_1502" s="T95">v-v&gt;v-v:inf</ta>
            <ta e="T99" id="Seg_1503" s="T98">pers-n:case</ta>
            <ta e="T100" id="Seg_1504" s="T99">interrog</ta>
            <ta e="T101" id="Seg_1505" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_1506" s="T101">ptcl</ta>
            <ta e="T103" id="Seg_1507" s="T102">ptcl</ta>
            <ta e="T104" id="Seg_1508" s="T103">v.[v:pn]</ta>
            <ta e="T105" id="Seg_1509" s="T104">conj</ta>
            <ta e="T106" id="Seg_1510" s="T105">v-v&gt;v.[v:pn]</ta>
            <ta e="T107" id="Seg_1511" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_1512" s="T107">v-v&gt;v.[v:pn]</ta>
            <ta e="T109" id="Seg_1513" s="T108">interrog-n:ins-n:case-clit-clit</ta>
            <ta e="T110" id="Seg_1514" s="T109">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T111" id="Seg_1515" s="T110">n-n:num.[n:case]-n:poss</ta>
            <ta e="T112" id="Seg_1516" s="T111">v-v:pn</ta>
            <ta e="T113" id="Seg_1517" s="T112">pers</ta>
            <ta e="T114" id="Seg_1518" s="T113">pers-n:ins-n:case</ta>
            <ta e="T115" id="Seg_1519" s="T114">interrog</ta>
            <ta e="T116" id="Seg_1520" s="T115">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T117" id="Seg_1521" s="T116">adv</ta>
            <ta e="T118" id="Seg_1522" s="T117">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T119" id="Seg_1523" s="T118">pers</ta>
            <ta e="T120" id="Seg_1524" s="T119">pers-n:case</ta>
            <ta e="T121" id="Seg_1525" s="T120">v-v:pn</ta>
            <ta e="T122" id="Seg_1526" s="T121">interrog-n:case</ta>
            <ta e="T123" id="Seg_1527" s="T122">adv-n:case</ta>
            <ta e="T124" id="Seg_1528" s="T123">v-v:tense-v:pn</ta>
            <ta e="T125" id="Seg_1529" s="T124">conj</ta>
            <ta e="T126" id="Seg_1530" s="T125">pers.[n:case]</ta>
            <ta e="T127" id="Seg_1531" s="T126">v-v:pn</ta>
            <ta e="T128" id="Seg_1532" s="T127">pers</ta>
            <ta e="T129" id="Seg_1533" s="T128">n-n:num-n:case</ta>
            <ta e="T130" id="Seg_1534" s="T129">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1535" s="T1">pers</ta>
            <ta e="T3" id="Seg_1536" s="T2">adv</ta>
            <ta e="T4" id="Seg_1537" s="T3">v</ta>
            <ta e="T5" id="Seg_1538" s="T4">n</ta>
            <ta e="T6" id="Seg_1539" s="T5">v</ta>
            <ta e="T7" id="Seg_1540" s="T6">adv</ta>
            <ta e="T8" id="Seg_1541" s="T7">v</ta>
            <ta e="T9" id="Seg_1542" s="T8">conj</ta>
            <ta e="T10" id="Seg_1543" s="T9">n</ta>
            <ta e="T11" id="Seg_1544" s="T10">n</ta>
            <ta e="T12" id="Seg_1545" s="T11">v</ta>
            <ta e="T13" id="Seg_1546" s="T12">v</ta>
            <ta e="T14" id="Seg_1547" s="T13">n</ta>
            <ta e="T15" id="Seg_1548" s="T14">v</ta>
            <ta e="T16" id="Seg_1549" s="T15">n</ta>
            <ta e="T17" id="Seg_1550" s="T16">n</ta>
            <ta e="T18" id="Seg_1551" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_1552" s="T18">n</ta>
            <ta e="T20" id="Seg_1553" s="T19">conj</ta>
            <ta e="T21" id="Seg_1554" s="T20">pers</ta>
            <ta e="T22" id="Seg_1555" s="T21">adv</ta>
            <ta e="T23" id="Seg_1556" s="T22">v</ta>
            <ta e="T24" id="Seg_1557" s="T23">n</ta>
            <ta e="T25" id="Seg_1558" s="T24">v</ta>
            <ta e="T26" id="Seg_1559" s="T25">pers</ta>
            <ta e="T27" id="Seg_1560" s="T26">v</ta>
            <ta e="T28" id="Seg_1561" s="T27">n</ta>
            <ta e="T29" id="Seg_1562" s="T28">v</ta>
            <ta e="T30" id="Seg_1563" s="T29">pers</ta>
            <ta e="T31" id="Seg_1564" s="T30">n</ta>
            <ta e="T32" id="Seg_1565" s="T31">adv</ta>
            <ta e="T33" id="Seg_1566" s="T32">v</ta>
            <ta e="T34" id="Seg_1567" s="T33">n</ta>
            <ta e="T35" id="Seg_1568" s="T34">n</ta>
            <ta e="T36" id="Seg_1569" s="T35">v</ta>
            <ta e="T37" id="Seg_1570" s="T36">adj</ta>
            <ta e="T38" id="Seg_1571" s="T37">n</ta>
            <ta e="T39" id="Seg_1572" s="T38">pers</ta>
            <ta e="T40" id="Seg_1573" s="T39">n</ta>
            <ta e="T41" id="Seg_1574" s="T40">v</ta>
            <ta e="T42" id="Seg_1575" s="T41">emphpro</ta>
            <ta e="T43" id="Seg_1576" s="T42">v</ta>
            <ta e="T44" id="Seg_1577" s="T43">num</ta>
            <ta e="T45" id="Seg_1578" s="T44">n</ta>
            <ta e="T46" id="Seg_1579" s="T45">v</ta>
            <ta e="T47" id="Seg_1580" s="T46">adj</ta>
            <ta e="T48" id="Seg_1581" s="T47">n</ta>
            <ta e="T49" id="Seg_1582" s="T48">v</ta>
            <ta e="T50" id="Seg_1583" s="T49">adv</ta>
            <ta e="T51" id="Seg_1584" s="T50">v</ta>
            <ta e="T52" id="Seg_1585" s="T51">n</ta>
            <ta e="T53" id="Seg_1586" s="T52">adv</ta>
            <ta e="T54" id="Seg_1587" s="T53">v</ta>
            <ta e="T55" id="Seg_1588" s="T54">conj</ta>
            <ta e="T56" id="Seg_1589" s="T55">adj</ta>
            <ta e="T57" id="Seg_1590" s="T56">n</ta>
            <ta e="T58" id="Seg_1591" s="T57">preverb</ta>
            <ta e="T59" id="Seg_1592" s="T58">v</ta>
            <ta e="T60" id="Seg_1593" s="T59">adv</ta>
            <ta e="T61" id="Seg_1594" s="T60">v</ta>
            <ta e="T62" id="Seg_1595" s="T61">n</ta>
            <ta e="T63" id="Seg_1596" s="T62">quant</ta>
            <ta e="T64" id="Seg_1597" s="T63">v</ta>
            <ta e="T65" id="Seg_1598" s="T64">n</ta>
            <ta e="T66" id="Seg_1599" s="T65">n</ta>
            <ta e="T67" id="Seg_1600" s="T66">n</ta>
            <ta e="T68" id="Seg_1601" s="T67">v</ta>
            <ta e="T69" id="Seg_1602" s="T68">conj</ta>
            <ta e="T70" id="Seg_1603" s="T69">adv</ta>
            <ta e="T71" id="Seg_1604" s="T70">n</ta>
            <ta e="T72" id="Seg_1605" s="T71">v</ta>
            <ta e="T73" id="Seg_1606" s="T72">pers</ta>
            <ta e="T74" id="Seg_1607" s="T73">v</ta>
            <ta e="T75" id="Seg_1608" s="T74">n</ta>
            <ta e="T76" id="Seg_1609" s="T75">n</ta>
            <ta e="T77" id="Seg_1610" s="T76">interrog</ta>
            <ta e="T78" id="Seg_1611" s="T77">v</ta>
            <ta e="T79" id="Seg_1612" s="T78">conj</ta>
            <ta e="T80" id="Seg_1613" s="T79">n</ta>
            <ta e="T81" id="Seg_1614" s="T80">n</ta>
            <ta e="T82" id="Seg_1615" s="T81">v</ta>
            <ta e="T83" id="Seg_1616" s="T82">conj</ta>
            <ta e="T84" id="Seg_1617" s="T83">n</ta>
            <ta e="T85" id="Seg_1618" s="T84">n</ta>
            <ta e="T86" id="Seg_1619" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_1620" s="T86">v</ta>
            <ta e="T88" id="Seg_1621" s="T87">pers</ta>
            <ta e="T89" id="Seg_1622" s="T88">n</ta>
            <ta e="T90" id="Seg_1623" s="T89">n</ta>
            <ta e="T91" id="Seg_1624" s="T90">pers</ta>
            <ta e="T92" id="Seg_1625" s="T91">adv</ta>
            <ta e="T93" id="Seg_1626" s="T92">v</ta>
            <ta e="T94" id="Seg_1627" s="T93">emphpro</ta>
            <ta e="T95" id="Seg_1628" s="T94">n</ta>
            <ta e="T96" id="Seg_1629" s="T95">v</ta>
            <ta e="T99" id="Seg_1630" s="T98">pers</ta>
            <ta e="T100" id="Seg_1631" s="T99">interrog</ta>
            <ta e="T101" id="Seg_1632" s="T100">n</ta>
            <ta e="T102" id="Seg_1633" s="T101">ptcl</ta>
            <ta e="T103" id="Seg_1634" s="T102">v</ta>
            <ta e="T104" id="Seg_1635" s="T103">v</ta>
            <ta e="T105" id="Seg_1636" s="T104">conj</ta>
            <ta e="T106" id="Seg_1637" s="T105">v</ta>
            <ta e="T107" id="Seg_1638" s="T106">n</ta>
            <ta e="T108" id="Seg_1639" s="T107">v</ta>
            <ta e="T109" id="Seg_1640" s="T108">pro</ta>
            <ta e="T110" id="Seg_1641" s="T109">v</ta>
            <ta e="T111" id="Seg_1642" s="T110">n</ta>
            <ta e="T112" id="Seg_1643" s="T111">v</ta>
            <ta e="T113" id="Seg_1644" s="T112">pers</ta>
            <ta e="T114" id="Seg_1645" s="T113">pers</ta>
            <ta e="T115" id="Seg_1646" s="T114">interrog</ta>
            <ta e="T116" id="Seg_1647" s="T115">v</ta>
            <ta e="T117" id="Seg_1648" s="T116">adv</ta>
            <ta e="T118" id="Seg_1649" s="T117">v</ta>
            <ta e="T119" id="Seg_1650" s="T118">pers</ta>
            <ta e="T120" id="Seg_1651" s="T119">pers</ta>
            <ta e="T121" id="Seg_1652" s="T120">v</ta>
            <ta e="T122" id="Seg_1653" s="T121">interrog</ta>
            <ta e="T123" id="Seg_1654" s="T122">adv</ta>
            <ta e="T124" id="Seg_1655" s="T123">v</ta>
            <ta e="T125" id="Seg_1656" s="T124">conj</ta>
            <ta e="T126" id="Seg_1657" s="T125">pers</ta>
            <ta e="T127" id="Seg_1658" s="T126">v</ta>
            <ta e="T128" id="Seg_1659" s="T127">pers</ta>
            <ta e="T129" id="Seg_1660" s="T128">n</ta>
            <ta e="T130" id="Seg_1661" s="T129">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1662" s="T1">pro.h:A</ta>
            <ta e="T3" id="Seg_1663" s="T2">np:Time</ta>
            <ta e="T5" id="Seg_1664" s="T4">np:Th</ta>
            <ta e="T7" id="Seg_1665" s="T6">adv:G</ta>
            <ta e="T8" id="Seg_1666" s="T7">0.1.h:A</ta>
            <ta e="T10" id="Seg_1667" s="T9">np.h:P</ta>
            <ta e="T11" id="Seg_1668" s="T10">np:G</ta>
            <ta e="T13" id="Seg_1669" s="T12">0.3.h:A</ta>
            <ta e="T14" id="Seg_1670" s="T13">np.h:A</ta>
            <ta e="T16" id="Seg_1671" s="T15">pro.h:A</ta>
            <ta e="T17" id="Seg_1672" s="T16">np.h:Th</ta>
            <ta e="T21" id="Seg_1673" s="T20">pro.h:E</ta>
            <ta e="T23" id="Seg_1674" s="T22">0.3:Th</ta>
            <ta e="T24" id="Seg_1675" s="T23">np.h:A</ta>
            <ta e="T26" id="Seg_1676" s="T25">pro.h:A</ta>
            <ta e="T28" id="Seg_1677" s="T27">np.h:Th</ta>
            <ta e="T29" id="Seg_1678" s="T28">0.1.h:A</ta>
            <ta e="T30" id="Seg_1679" s="T29">pro.h:Poss</ta>
            <ta e="T31" id="Seg_1680" s="T30">np:Th</ta>
            <ta e="T32" id="Seg_1681" s="T31">adv:L</ta>
            <ta e="T34" id="Seg_1682" s="T33">np.h:A</ta>
            <ta e="T35" id="Seg_1683" s="T34">np:G</ta>
            <ta e="T39" id="Seg_1684" s="T38">pro.h:A</ta>
            <ta e="T40" id="Seg_1685" s="T39">np:Th</ta>
            <ta e="T43" id="Seg_1686" s="T42">0.1.h:P</ta>
            <ta e="T45" id="Seg_1687" s="T44">np:A 0.1.h:Poss</ta>
            <ta e="T48" id="Seg_1688" s="T47">np:P 0.1.h:Poss</ta>
            <ta e="T51" id="Seg_1689" s="T50">0.1.h:A</ta>
            <ta e="T52" id="Seg_1690" s="T51">np:Th 0.1.h:Poss</ta>
            <ta e="T53" id="Seg_1691" s="T52">adv:G</ta>
            <ta e="T54" id="Seg_1692" s="T53">0.1.h:A</ta>
            <ta e="T57" id="Seg_1693" s="T56">np:P 0.1.h:Poss</ta>
            <ta e="T61" id="Seg_1694" s="T60">0.1.h:A</ta>
            <ta e="T62" id="Seg_1695" s="T61">np:P 0.1.h:Poss</ta>
            <ta e="T64" id="Seg_1696" s="T63">0.1.h:A</ta>
            <ta e="T65" id="Seg_1697" s="T64">np.h:Poss</ta>
            <ta e="T66" id="Seg_1698" s="T65">np:Th</ta>
            <ta e="T67" id="Seg_1699" s="T66">np:G</ta>
            <ta e="T68" id="Seg_1700" s="T67">0.1.h:A</ta>
            <ta e="T70" id="Seg_1701" s="T69">adv:L</ta>
            <ta e="T71" id="Seg_1702" s="T70">np.h:Th</ta>
            <ta e="T73" id="Seg_1703" s="T72">pro.h:A</ta>
            <ta e="T75" id="Seg_1704" s="T74">np.h:Poss</ta>
            <ta e="T76" id="Seg_1705" s="T75">np:Th</ta>
            <ta e="T77" id="Seg_1706" s="T76">pro:L</ta>
            <ta e="T81" id="Seg_1707" s="T80">np.h:A</ta>
            <ta e="T85" id="Seg_1708" s="T84">np.h:A</ta>
            <ta e="T88" id="Seg_1709" s="T87">pro.h:Poss</ta>
            <ta e="T89" id="Seg_1710" s="T88">np.h:Th</ta>
            <ta e="T90" id="Seg_1711" s="T89">np.h:Th 0.3.h:Poss</ta>
            <ta e="T91" id="Seg_1712" s="T90">pro.h:E</ta>
            <ta e="T101" id="Seg_1713" s="T100">np.h:Th</ta>
            <ta e="T104" id="Seg_1714" s="T103">0.3.h:Th</ta>
            <ta e="T106" id="Seg_1715" s="T105">0.3.h:A</ta>
            <ta e="T107" id="Seg_1716" s="T106">np:Path</ta>
            <ta e="T108" id="Seg_1717" s="T107">0.3.h:A</ta>
            <ta e="T109" id="Seg_1718" s="T108">pro.h:B</ta>
            <ta e="T110" id="Seg_1719" s="T109">0.1.h:A</ta>
            <ta e="T111" id="Seg_1720" s="T110">np:P 0.1.h:Poss</ta>
            <ta e="T113" id="Seg_1721" s="T112">pro.h:A</ta>
            <ta e="T114" id="Seg_1722" s="T113">pro.h:Th</ta>
            <ta e="T118" id="Seg_1723" s="T117">0.1.h:A 0.3.h:Th</ta>
            <ta e="T119" id="Seg_1724" s="T118">pro.h:A</ta>
            <ta e="T120" id="Seg_1725" s="T119">pro.h:R</ta>
            <ta e="T123" id="Seg_1726" s="T122">adv:G</ta>
            <ta e="T124" id="Seg_1727" s="T123">0.2.h:A</ta>
            <ta e="T126" id="Seg_1728" s="T125">pro.h:A</ta>
            <ta e="T128" id="Seg_1729" s="T127">pro.h:A</ta>
            <ta e="T129" id="Seg_1730" s="T128">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1731" s="T1">pro.h:S</ta>
            <ta e="T4" id="Seg_1732" s="T3">v:pred</ta>
            <ta e="T6" id="Seg_1733" s="T4">s:purp</ta>
            <ta e="T8" id="Seg_1734" s="T7">0.1.h:S v:pred</ta>
            <ta e="T10" id="Seg_1735" s="T9">np.h:S</ta>
            <ta e="T12" id="Seg_1736" s="T11">v:pred</ta>
            <ta e="T13" id="Seg_1737" s="T12">0.3.h:S v:pred</ta>
            <ta e="T14" id="Seg_1738" s="T13">np.h:S</ta>
            <ta e="T15" id="Seg_1739" s="T14">v:pred</ta>
            <ta e="T16" id="Seg_1740" s="T15">pro.h:S</ta>
            <ta e="T17" id="Seg_1741" s="T16">np.h:O</ta>
            <ta e="T19" id="Seg_1742" s="T18">v:pred</ta>
            <ta e="T23" id="Seg_1743" s="T22">0.3:S v:pred</ta>
            <ta e="T24" id="Seg_1744" s="T23">np.h:S</ta>
            <ta e="T25" id="Seg_1745" s="T24">v:pred</ta>
            <ta e="T26" id="Seg_1746" s="T25">pro.h:S</ta>
            <ta e="T27" id="Seg_1747" s="T26">v:pred</ta>
            <ta e="T28" id="Seg_1748" s="T27">np.h:O</ta>
            <ta e="T29" id="Seg_1749" s="T28">0.1.h:S v:pred</ta>
            <ta e="T31" id="Seg_1750" s="T30">np:S</ta>
            <ta e="T33" id="Seg_1751" s="T32">v:pred</ta>
            <ta e="T34" id="Seg_1752" s="T33">np.h:S</ta>
            <ta e="T36" id="Seg_1753" s="T35">v:pred</ta>
            <ta e="T39" id="Seg_1754" s="T38">pro.h:S</ta>
            <ta e="T40" id="Seg_1755" s="T39">np:O</ta>
            <ta e="T41" id="Seg_1756" s="T40">v:pred</ta>
            <ta e="T43" id="Seg_1757" s="T42">0.1.h:S v:pred</ta>
            <ta e="T45" id="Seg_1758" s="T44">np:S</ta>
            <ta e="T46" id="Seg_1759" s="T45">v:pred</ta>
            <ta e="T48" id="Seg_1760" s="T47">np:S</ta>
            <ta e="T49" id="Seg_1761" s="T48">v:pred</ta>
            <ta e="T51" id="Seg_1762" s="T50">0.1.h:S v:pred</ta>
            <ta e="T52" id="Seg_1763" s="T51">np:O</ta>
            <ta e="T54" id="Seg_1764" s="T53">0.1.h:S v:pred</ta>
            <ta e="T57" id="Seg_1765" s="T56">np:S</ta>
            <ta e="T59" id="Seg_1766" s="T58">v:pred</ta>
            <ta e="T61" id="Seg_1767" s="T60">0.1.h:S v:pred</ta>
            <ta e="T62" id="Seg_1768" s="T61">np:O</ta>
            <ta e="T64" id="Seg_1769" s="T63">0.1.h:S v:pred</ta>
            <ta e="T66" id="Seg_1770" s="T65">np:O</ta>
            <ta e="T68" id="Seg_1771" s="T67">0.1.h:S v:pred</ta>
            <ta e="T71" id="Seg_1772" s="T70">np.h:S</ta>
            <ta e="T72" id="Seg_1773" s="T71">v:pred</ta>
            <ta e="T73" id="Seg_1774" s="T72">pro.h:S</ta>
            <ta e="T74" id="Seg_1775" s="T73">v:pred</ta>
            <ta e="T76" id="Seg_1776" s="T75">np:S</ta>
            <ta e="T78" id="Seg_1777" s="T77">v:pred</ta>
            <ta e="T81" id="Seg_1778" s="T80">np.h:S</ta>
            <ta e="T82" id="Seg_1779" s="T81">v:pred</ta>
            <ta e="T85" id="Seg_1780" s="T84">np.h:S</ta>
            <ta e="T87" id="Seg_1781" s="T86">v:pred</ta>
            <ta e="T89" id="Seg_1782" s="T88">np.h:S</ta>
            <ta e="T91" id="Seg_1783" s="T90">pro.h:S</ta>
            <ta e="T93" id="Seg_1784" s="T92">v:pred</ta>
            <ta e="T101" id="Seg_1785" s="T100">np.h:O</ta>
            <ta e="T103" id="Seg_1786" s="T102">ptcl:pred</ta>
            <ta e="T104" id="Seg_1787" s="T103">0.3.h:S v:pred</ta>
            <ta e="T106" id="Seg_1788" s="T105">0.3.h:S v:pred</ta>
            <ta e="T108" id="Seg_1789" s="T107">0.3.h:S v:pred</ta>
            <ta e="T109" id="Seg_1790" s="T108">pro.h:O</ta>
            <ta e="T110" id="Seg_1791" s="T109">0.1.h:S v:pred</ta>
            <ta e="T111" id="Seg_1792" s="T110">np:S</ta>
            <ta e="T112" id="Seg_1793" s="T111">v:pred</ta>
            <ta e="T113" id="Seg_1794" s="T112">pro.h:S</ta>
            <ta e="T114" id="Seg_1795" s="T113">pro.h:O</ta>
            <ta e="T116" id="Seg_1796" s="T115">v:pred</ta>
            <ta e="T118" id="Seg_1797" s="T117">0.1.h:S v:pred 0.3.h:O</ta>
            <ta e="T119" id="Seg_1798" s="T118">pro.h:S</ta>
            <ta e="T121" id="Seg_1799" s="T120">v:pred</ta>
            <ta e="T124" id="Seg_1800" s="T123">0.2.h:S v:pred</ta>
            <ta e="T126" id="Seg_1801" s="T125">pro.h:S</ta>
            <ta e="T127" id="Seg_1802" s="T126">v:pred</ta>
            <ta e="T128" id="Seg_1803" s="T127">pro.h:S</ta>
            <ta e="T129" id="Seg_1804" s="T128">np:O</ta>
            <ta e="T130" id="Seg_1805" s="T129">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T9" id="Seg_1806" s="T8">RUS:gram</ta>
            <ta e="T20" id="Seg_1807" s="T19">RUS:gram</ta>
            <ta e="T22" id="Seg_1808" s="T21">RUS:cult</ta>
            <ta e="T31" id="Seg_1809" s="T30">RUS:cult</ta>
            <ta e="T40" id="Seg_1810" s="T39">RUS:cult</ta>
            <ta e="T50" id="Seg_1811" s="T49">RUS:core</ta>
            <ta e="T55" id="Seg_1812" s="T54">RUS:gram</ta>
            <ta e="T60" id="Seg_1813" s="T59">RUS:core</ta>
            <ta e="T63" id="Seg_1814" s="T62">RUS:core</ta>
            <ta e="T66" id="Seg_1815" s="T65">RUS:cult</ta>
            <ta e="T69" id="Seg_1816" s="T68">RUS:gram</ta>
            <ta e="T79" id="Seg_1817" s="T78">RUS:gram</ta>
            <ta e="T83" id="Seg_1818" s="T82">RUS:gram</ta>
            <ta e="T92" id="Seg_1819" s="T91">RUS:core</ta>
            <ta e="T102" id="Seg_1820" s="T101">RUS:gram</ta>
            <ta e="T103" id="Seg_1821" s="T102">RUS:mod</ta>
            <ta e="T105" id="Seg_1822" s="T104">RUS:gram</ta>
            <ta e="T107" id="Seg_1823" s="T106">RUS:cult</ta>
            <ta e="T115" id="Seg_1824" s="T114">RUS:gram</ta>
            <ta e="T117" id="Seg_1825" s="T116">RUS:core</ta>
            <ta e="T125" id="Seg_1826" s="T124">RUS:gram</ta>
            <ta e="T129" id="Seg_1827" s="T128">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_1828" s="T1">Today I went to buy fish.</ta>
            <ta e="T8" id="Seg_1829" s="T6">I was going back.</ta>
            <ta e="T12" id="Seg_1830" s="T8">And a child got stuck in silt.</ta>
            <ta e="T13" id="Seg_1831" s="T12">He was crying.</ta>
            <ta e="T15" id="Seg_1832" s="T13">People were passing by.</ta>
            <ta e="T19" id="Seg_1833" s="T15">Noone was trying to pull the child out.</ta>
            <ta e="T23" id="Seg_1834" s="T19">And I felt sorry for him.</ta>
            <ta e="T25" id="Seg_1835" s="T23">The child was crying.</ta>
            <ta e="T27" id="Seg_1836" s="T25">I went [there].</ta>
            <ta e="T29" id="Seg_1837" s="T27">I pulled the child out.</ta>
            <ta e="T33" id="Seg_1838" s="T29">His boot stayed there.</ta>
            <ta e="T38" id="Seg_1839" s="T33">The child ran onto the road barefoot.</ta>
            <ta e="T41" id="Seg_1840" s="T38">I pulled his boot out.</ta>
            <ta e="T43" id="Seg_1841" s="T41">I got stuck myself.</ta>
            <ta e="T49" id="Seg_1842" s="T43">While I pull one foot out, the other gets stuck.</ta>
            <ta e="T51" id="Seg_1843" s="T49">I could hardly go.</ta>
            <ta e="T54" id="Seg_1844" s="T51">I lift one foot.</ta>
            <ta e="T59" id="Seg_1845" s="T54">The other foot drowns.</ta>
            <ta e="T61" id="Seg_1846" s="T59">‎‎I could hardly go.</ta>
            <ta e="T64" id="Seg_1847" s="T61">My hands got dirty.</ta>
            <ta e="T68" id="Seg_1848" s="T64">I put the boot on the child's foot.</ta>
            <ta e="T72" id="Seg_1849" s="T68">A girl was standing nearby.</ta>
            <ta e="T78" id="Seg_1850" s="T72">I said: “Where is the child's house?”</ta>
            <ta e="T87" id="Seg_1851" s="T78">And the girl said: “Here is a woman laughing.</ta>
            <ta e="T89" id="Seg_1852" s="T87">It's his mother.”</ta>
            <ta e="T93" id="Seg_1853" s="T89">I got so angry with his mother.</ta>
            <ta e="T98" id="Seg_1854" s="T93">Can't she pull her child out herself?</ta>
            <ta e="T103" id="Seg_1855" s="T98">Doesn't she need her child?</ta>
            <ta e="T108" id="Seg_1856" s="T103">She is sitting and laughing, looking through the window.</ta>
            <ta e="T110" id="Seg_1857" s="T108">I am not going to help anyone more.</ta>
            <ta e="T112" id="Seg_1858" s="T110">My hands ache.</ta>
            <ta e="T118" id="Seg_1859" s="T112">I was pulling him so, I hardly managed to pull him out.</ta>
            <ta e="T124" id="Seg_1860" s="T118">I told him: “How did you get here?”</ta>
            <ta e="T130" id="Seg_1861" s="T124">And he said: “I was chasing pigeons.”</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_1862" s="T1">Heute ging ich Fisch kaufen.</ta>
            <ta e="T8" id="Seg_1863" s="T6">Ich ging zurück.</ta>
            <ta e="T12" id="Seg_1864" s="T8">Und ein Kind steckte im Schlamm fest.</ta>
            <ta e="T13" id="Seg_1865" s="T12">Es weinte.</ta>
            <ta e="T15" id="Seg_1866" s="T13">Menschen gingen vorbei.</ta>
            <ta e="T19" id="Seg_1867" s="T15">Niemand versuchte das Kind herauszuziehen.</ta>
            <ta e="T23" id="Seg_1868" s="T19">Und ich hatte Mitleid mit ihm.</ta>
            <ta e="T25" id="Seg_1869" s="T23">Das Kind weinte.</ta>
            <ta e="T27" id="Seg_1870" s="T25">Ich ging [dorthin].</ta>
            <ta e="T29" id="Seg_1871" s="T27">Ich zog das Kind heraus.</ta>
            <ta e="T33" id="Seg_1872" s="T29">Sein Schuh blieb dort.</ta>
            <ta e="T38" id="Seg_1873" s="T33">Das Kind rannte barfuß auf die Straße.</ta>
            <ta e="T41" id="Seg_1874" s="T38">Ich zog seinen Schuh heraus.</ta>
            <ta e="T43" id="Seg_1875" s="T41">Ich steckte selbst fest.</ta>
            <ta e="T49" id="Seg_1876" s="T43">Wenn ich einen Fuß herausziehe, sinkt der andere ein.</ta>
            <ta e="T51" id="Seg_1877" s="T49">Ich konnte kaum laufen.</ta>
            <ta e="T54" id="Seg_1878" s="T51">Ich hob einen Fuß.</ta>
            <ta e="T59" id="Seg_1879" s="T54">Der andere Fuß sinkt ein.</ta>
            <ta e="T61" id="Seg_1880" s="T59">Ich konnte kaum laufen.</ta>
            <ta e="T64" id="Seg_1881" s="T61">Meine Hände wurden dreckig.</ta>
            <ta e="T68" id="Seg_1882" s="T64">Ich zog den Schuh auf den Fuß des Kindes.</ta>
            <ta e="T72" id="Seg_1883" s="T68">Ein Mädchen stand in der Nähe.</ta>
            <ta e="T78" id="Seg_1884" s="T72">Ich sagte: "Wo ist das Haus des Kindes?"</ta>
            <ta e="T87" id="Seg_1885" s="T78">Und das Mädchen sagt: "Hier lacht eine Frau.</ta>
            <ta e="T89" id="Seg_1886" s="T87">Sie ist seine Mutter."</ta>
            <ta e="T93" id="Seg_1887" s="T89">Ich wurde so wütend auf seine Mutter.</ta>
            <ta e="T98" id="Seg_1888" s="T93">Kann sie ihr Kind nicht selbst herausziehen?</ta>
            <ta e="T103" id="Seg_1889" s="T98">Braucht sie ihr Kind nicht?</ta>
            <ta e="T108" id="Seg_1890" s="T103">Sie sitzt und lacht, schaut durchs Fenster.</ta>
            <ta e="T110" id="Seg_1891" s="T108">Ich helfe keinem mehr.</ta>
            <ta e="T112" id="Seg_1892" s="T110">Meine Hände schmerzen.</ta>
            <ta e="T118" id="Seg_1893" s="T112">Ich habe ihn so gezogen, ich schaffte es kaum, ihn herauszuziehen.</ta>
            <ta e="T124" id="Seg_1894" s="T118">Ich sagte ihm: "Wie bist du dahin gekommen?"</ta>
            <ta e="T130" id="Seg_1895" s="T124">Und er sagte: "Ich habe Tauben gejagt."</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_1896" s="T1">Я сегодня ходила рыбу покупать.</ta>
            <ta e="T8" id="Seg_1897" s="T6">Назад иду.</ta>
            <ta e="T12" id="Seg_1898" s="T8">А ребенок в иле завяз.</ta>
            <ta e="T13" id="Seg_1899" s="T12">Плачет.</ta>
            <ta e="T15" id="Seg_1900" s="T13">Люди идут.</ta>
            <ta e="T19" id="Seg_1901" s="T15">Никто ребенка не вытаскивает.</ta>
            <ta e="T23" id="Seg_1902" s="T19">А мне жалко стало.</ta>
            <ta e="T25" id="Seg_1903" s="T23">Ребенок плачет.</ta>
            <ta e="T27" id="Seg_1904" s="T25">Я побрела.</ta>
            <ta e="T29" id="Seg_1905" s="T27">Я ребенка вытащила.</ta>
            <ta e="T33" id="Seg_1906" s="T29">Его сапог там остался.</ta>
            <ta e="T38" id="Seg_1907" s="T33">Ребенок на дорогу побежал босиком.</ta>
            <ta e="T41" id="Seg_1908" s="T38">Я сапог достала.</ta>
            <ta e="T43" id="Seg_1909" s="T41">Я сама увязла.</ta>
            <ta e="T49" id="Seg_1910" s="T43">Одну ногу вытащу, другая (вторая) нога взянет.</ta>
            <ta e="T51" id="Seg_1911" s="T49">Едва вылезла (пошла?).</ta>
            <ta e="T54" id="Seg_1912" s="T51">Ногу наверх поднимаю.</ta>
            <ta e="T59" id="Seg_1913" s="T54">А другая нога тонет.</ta>
            <ta e="T61" id="Seg_1914" s="T59">Насилу вылезла (пошла?).</ta>
            <ta e="T64" id="Seg_1915" s="T61">Руки все измазала.</ta>
            <ta e="T68" id="Seg_1916" s="T64">Ребенку сапог на ногу надела.</ta>
            <ta e="T72" id="Seg_1917" s="T68">А тут девчонка стояла.</ta>
            <ta e="T78" id="Seg_1918" s="T72">Я говорю: “Где дом ребенка?”</ta>
            <ta e="T87" id="Seg_1919" s="T78">А девочка сказала: “А вот женщина хохочет.</ta>
            <ta e="T89" id="Seg_1920" s="T87">Это его мать”.</ta>
            <ta e="T93" id="Seg_1921" s="T89">На его мать я так рассердилась.</ta>
            <ta e="T98" id="Seg_1922" s="T93">Сама своего ребенка вытащить не может.</ta>
            <ta e="T103" id="Seg_1923" s="T98">Ей что, ребенка не надо?</ta>
            <ta e="T108" id="Seg_1924" s="T103">Сидит и хохочет, в окошко смотрит.</ta>
            <ta e="T110" id="Seg_1925" s="T108">Больше никому не буду помогать.</ta>
            <ta e="T112" id="Seg_1926" s="T110">Руки у меня болят.</ta>
            <ta e="T118" id="Seg_1927" s="T112">Я его как тащила, насилу вытащила.</ta>
            <ta e="T124" id="Seg_1928" s="T118">Я ему сказала: “Зачем сюда забрел?”</ta>
            <ta e="T130" id="Seg_1929" s="T124">А он говорит: “Я голубей гонял”.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_1930" s="T1">я сегодня ходила рыбу покупать</ta>
            <ta e="T8" id="Seg_1931" s="T6">назад иду</ta>
            <ta e="T12" id="Seg_1932" s="T8">ребенок в грязи (иле) утонул</ta>
            <ta e="T13" id="Seg_1933" s="T12">плачет</ta>
            <ta e="T15" id="Seg_1934" s="T13">люди идут</ta>
            <ta e="T19" id="Seg_1935" s="T15">никто его не вытаскивает</ta>
            <ta e="T23" id="Seg_1936" s="T19">мне жалко стало</ta>
            <ta e="T25" id="Seg_1937" s="T23">ребенок плачет</ta>
            <ta e="T27" id="Seg_1938" s="T25">я побрела</ta>
            <ta e="T29" id="Seg_1939" s="T27">ребенка вытащила</ta>
            <ta e="T33" id="Seg_1940" s="T29">его сапог там остался</ta>
            <ta e="T38" id="Seg_1941" s="T33">ребенок на дорогу побежал босиком</ta>
            <ta e="T41" id="Seg_1942" s="T38">я сапог достала</ta>
            <ta e="T43" id="Seg_1943" s="T41">сама утонула</ta>
            <ta e="T49" id="Seg_1944" s="T43">одну ногу вытащу другая (вторая) нога тонет</ta>
            <ta e="T51" id="Seg_1945" s="T49">кое-как вылезла</ta>
            <ta e="T54" id="Seg_1946" s="T51">ноги наверх поднимаю</ta>
            <ta e="T59" id="Seg_1947" s="T54">другая нога тонет</ta>
            <ta e="T61" id="Seg_1948" s="T59">насилу вылезла</ta>
            <ta e="T64" id="Seg_1949" s="T61">руки все замазала</ta>
            <ta e="T68" id="Seg_1950" s="T64">ребенку сапог на ногу одела</ta>
            <ta e="T72" id="Seg_1951" s="T68">тут девчонка стояла</ta>
            <ta e="T78" id="Seg_1952" s="T72">я говорю дом ребенка где</ta>
            <ta e="T87" id="Seg_1953" s="T78">девочка сказала а вот женщина хохочет</ta>
            <ta e="T89" id="Seg_1954" s="T87">это его мать</ta>
            <ta e="T93" id="Seg_1955" s="T89">на его мать (его матери) я так рассердилась</ta>
            <ta e="T98" id="Seg_1956" s="T93">сама ребенка вытащить не может</ta>
            <ta e="T103" id="Seg_1957" s="T98">ей что ребенка не надо</ta>
            <ta e="T108" id="Seg_1958" s="T103">сидит хохочет и в окошко смотрит</ta>
            <ta e="T110" id="Seg_1959" s="T108">больше никому не буду помогать</ta>
            <ta e="T112" id="Seg_1960" s="T110">руки болят</ta>
            <ta e="T118" id="Seg_1961" s="T112">я его тащила тащила насилу вытащила</ta>
            <ta e="T124" id="Seg_1962" s="T118">я ему говорила зачем сюда забрел</ta>
            <ta e="T130" id="Seg_1963" s="T124">а он я голубей гонял</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T6" id="Seg_1964" s="T1">[KuAI:] Variant: 'kəzizan'. [BrM:] Unclear suffix -zi in 'közizan'. Cf. simple past forms of this verb in 10, 34, 35. || </ta>
            <ta e="T8" id="Seg_1965" s="T6">[KuAI:] Variant: 'kösʼe'.</ta>
            <ta e="T12" id="Seg_1966" s="T8">[KuAI:] Variant: 'qoubpa'.</ta>
            <ta e="T19" id="Seg_1967" s="T15">[BrM:] Tentative analysis of 'jüɣulgattə'.</ta>
            <ta e="T41" id="Seg_1968" s="T38">[BrM:] Tentative analysis of 'ügunnau'.</ta>
            <ta e="T49" id="Seg_1969" s="T43">[BrM:] Unclear personal ending in 'ügulgu'. Tentative analysis of 'ügulgu'. [KuAI:] Variant: 'ɣau'.</ta>
            <ta e="T72" id="Seg_1970" s="T68">[BrM:] Tentative analysis of 'nɨŋɨtda'.</ta>
            <ta e="T87" id="Seg_1971" s="T78">[BrM:] 'nalagwatpɨtda' changed to 'na lagwatpɨtda'.</ta>
            <ta e="T93" id="Seg_1972" s="T89">[BrM:] 'da tao' changed to 'datao'.</ta>
            <ta e="T98" id="Seg_1973" s="T93">[BrM:] Tentative analysis of 'üɣulgu'.</ta>
            <ta e="T110" id="Seg_1974" s="T108">[BrM:] 'Qudäm näss' changed to 'Qudämnäss'.</ta>
            <ta e="T118" id="Seg_1975" s="T112">[BrM:] Tentative analysis of 'ügulbuzau', 'ügunnau'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T6" id="Seg_1976" s="T1"> || </ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
