<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_ChildrensEating_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_ChildrensEating_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">28</ud-information>
            <ud-information attribute-name="# HIAT:w">23</ud-information>
            <ud-information attribute-name="# e">23</ud-information>
            <ud-information attribute-name="# HIAT:u">5</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T24" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Qɨbanʼaʒam</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">nadə</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">qɨban</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">jettəɣɨt</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">oɣulǯoǯigu</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_20" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">Warɣɨ</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">qɨbanʼaʒam</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">ass</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">oɣulǯoǯeǯal</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_35" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">Puskaj</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">okkɨrmɨɣɨn</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">aurkɨn</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_47" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">Tabnä</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">pelgat</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">qaimnä</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">ne</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">nadə</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_64" n="HIAT:w" s="T18">mekugu</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_68" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">I</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_73" n="HIAT:w" s="T20">puskaj</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">otdə</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">ig</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_82" n="HIAT:w" s="T23">iːkʼkʼimdə</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T24" id="Seg_85" n="sc" s="T1">
               <ts e="T2" id="Seg_87" n="e" s="T1">Qɨbanʼaʒam </ts>
               <ts e="T3" id="Seg_89" n="e" s="T2">nadə </ts>
               <ts e="T4" id="Seg_91" n="e" s="T3">qɨban </ts>
               <ts e="T5" id="Seg_93" n="e" s="T4">jettəɣɨt </ts>
               <ts e="T6" id="Seg_95" n="e" s="T5">oɣulǯoǯigu. </ts>
               <ts e="T7" id="Seg_97" n="e" s="T6">Warɣɨ </ts>
               <ts e="T8" id="Seg_99" n="e" s="T7">qɨbanʼaʒam </ts>
               <ts e="T9" id="Seg_101" n="e" s="T8">ass </ts>
               <ts e="T10" id="Seg_103" n="e" s="T9">oɣulǯoǯeǯal. </ts>
               <ts e="T11" id="Seg_105" n="e" s="T10">Puskaj </ts>
               <ts e="T12" id="Seg_107" n="e" s="T11">okkɨrmɨɣɨn </ts>
               <ts e="T13" id="Seg_109" n="e" s="T12">aurkɨn. </ts>
               <ts e="T14" id="Seg_111" n="e" s="T13">Tabnä </ts>
               <ts e="T15" id="Seg_113" n="e" s="T14">pelgat </ts>
               <ts e="T16" id="Seg_115" n="e" s="T15">qaimnä </ts>
               <ts e="T17" id="Seg_117" n="e" s="T16">ne </ts>
               <ts e="T18" id="Seg_119" n="e" s="T17">nadə </ts>
               <ts e="T19" id="Seg_121" n="e" s="T18">mekugu. </ts>
               <ts e="T20" id="Seg_123" n="e" s="T19">I </ts>
               <ts e="T21" id="Seg_125" n="e" s="T20">puskaj </ts>
               <ts e="T22" id="Seg_127" n="e" s="T21">otdə </ts>
               <ts e="T23" id="Seg_129" n="e" s="T22">ig </ts>
               <ts e="T24" id="Seg_131" n="e" s="T23">iːkʼkʼimdə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_132" s="T1">PVD_1964_ChildrensEating_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_133" s="T6">PVD_1964_ChildrensEating_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_134" s="T10">PVD_1964_ChildrensEating_nar.003 (001.003)</ta>
            <ta e="T19" id="Seg_135" s="T13">PVD_1964_ChildrensEating_nar.004 (001.004)</ta>
            <ta e="T24" id="Seg_136" s="T19">PVD_1964_ChildrensEating_nar.005 (001.005)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_137" s="T1">kыбанʼа′жам надъ ′kыбан ′jеттъ‵ɣыт ′оɣуl‵джоджигу.</ta>
            <ta e="T10" id="Seg_138" s="T6">wар′ɣы kыбанʼа′жам асс ′оɣуlджодже‵джал.</ta>
            <ta e="T13" id="Seg_139" s="T10">пускай ′оккырмыɣын ′ауркын.</ta>
            <ta e="T19" id="Seg_140" s="T13">таб′нӓ пел′гат kаим′нӓ не надъ ме′ку(о)гу.</ta>
            <ta e="T24" id="Seg_141" s="T19">и пус′кай ′отдъ иг̂ ′ӣкʼкʼимдъ.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_142" s="T1">qɨbanʼaʒam nadə qɨban jettəɣɨt oɣulǯoǯigu.</ta>
            <ta e="T10" id="Seg_143" s="T6">warɣɨ qɨbanʼaʒam ass oɣulǯoǯeǯal.</ta>
            <ta e="T13" id="Seg_144" s="T10">puskaj okkɨrmɨɣɨn aurkɨn.</ta>
            <ta e="T19" id="Seg_145" s="T13">tabnä pelgat qaimnä ne nadə meku(o)gu.</ta>
            <ta e="T24" id="Seg_146" s="T19">i puskaj otdə iĝ iːkʼkʼimdə.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_147" s="T1">Qɨbanʼaʒam nadə qɨban jettəɣɨt oɣulǯoǯigu. </ta>
            <ta e="T10" id="Seg_148" s="T6">Warɣɨ qɨbanʼaʒam ass oɣulǯoǯeǯal. </ta>
            <ta e="T13" id="Seg_149" s="T10">Puskaj okkɨrmɨɣɨn aurkɨn. </ta>
            <ta e="T19" id="Seg_150" s="T13">Tabnä pelgat qaimnä ne nadə mekugu. </ta>
            <ta e="T24" id="Seg_151" s="T19">I puskaj otdə ig iːkʼkʼimdə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_152" s="T1">qɨbanʼaʒa-m</ta>
            <ta e="T3" id="Seg_153" s="T2">nadə</ta>
            <ta e="T4" id="Seg_154" s="T3">qɨba-n</ta>
            <ta e="T5" id="Seg_155" s="T4">je-ttə-ɣɨt</ta>
            <ta e="T6" id="Seg_156" s="T5">oɣulǯo-ǯi-gu</ta>
            <ta e="T7" id="Seg_157" s="T6">warɣɨ</ta>
            <ta e="T8" id="Seg_158" s="T7">qɨbanʼaʒa-m</ta>
            <ta e="T9" id="Seg_159" s="T8">ass</ta>
            <ta e="T10" id="Seg_160" s="T9">oɣulǯo-ǯ-eǯa-l</ta>
            <ta e="T11" id="Seg_161" s="T10">puskaj</ta>
            <ta e="T12" id="Seg_162" s="T11">okkɨr-mɨ-ɣɨn</ta>
            <ta e="T13" id="Seg_163" s="T12">au-r-kɨ-n</ta>
            <ta e="T14" id="Seg_164" s="T13">tab-nä</ta>
            <ta e="T15" id="Seg_165" s="T14">pel-gat</ta>
            <ta e="T16" id="Seg_166" s="T15">qai-m-nä</ta>
            <ta e="T17" id="Seg_167" s="T16">ne</ta>
            <ta e="T18" id="Seg_168" s="T17">nadə</ta>
            <ta e="T19" id="Seg_169" s="T18">me-ku-gu</ta>
            <ta e="T20" id="Seg_170" s="T19">i</ta>
            <ta e="T21" id="Seg_171" s="T20">puskaj</ta>
            <ta e="T22" id="Seg_172" s="T21">otdə</ta>
            <ta e="T23" id="Seg_173" s="T22">ig</ta>
            <ta e="T24" id="Seg_174" s="T23">iː-kʼkʼi-mdə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_175" s="T1">qɨbanʼaǯa-m</ta>
            <ta e="T3" id="Seg_176" s="T2">nadə</ta>
            <ta e="T4" id="Seg_177" s="T3">qɨba-ŋ</ta>
            <ta e="T5" id="Seg_178" s="T4">eː-ttə-qɨn</ta>
            <ta e="T6" id="Seg_179" s="T5">oɣulǯɨ-ʒu-gu</ta>
            <ta e="T7" id="Seg_180" s="T6">wargɨ</ta>
            <ta e="T8" id="Seg_181" s="T7">qɨbanʼaǯa-m</ta>
            <ta e="T9" id="Seg_182" s="T8">asa</ta>
            <ta e="T10" id="Seg_183" s="T9">oɣulǯɨ-ǯə-enǯɨ-l</ta>
            <ta e="T11" id="Seg_184" s="T10">puskaj</ta>
            <ta e="T12" id="Seg_185" s="T11">okkɨr-mɨ-qɨn</ta>
            <ta e="T13" id="Seg_186" s="T12">am-r-ku-n</ta>
            <ta e="T14" id="Seg_187" s="T13">täp-nä</ta>
            <ta e="T15" id="Seg_188" s="T14">*pel-kattə</ta>
            <ta e="T16" id="Seg_189" s="T15">qaj-m-näj</ta>
            <ta e="T17" id="Seg_190" s="T16">ne</ta>
            <ta e="T18" id="Seg_191" s="T17">nadə</ta>
            <ta e="T19" id="Seg_192" s="T18">me-ku-gu</ta>
            <ta e="T20" id="Seg_193" s="T19">i</ta>
            <ta e="T21" id="Seg_194" s="T20">puskaj</ta>
            <ta e="T22" id="Seg_195" s="T21">ondə</ta>
            <ta e="T23" id="Seg_196" s="T22">igə</ta>
            <ta e="T24" id="Seg_197" s="T23">iː-ku-mtə</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_198" s="T1">child-ACC</ta>
            <ta e="T3" id="Seg_199" s="T2">one.should</ta>
            <ta e="T4" id="Seg_200" s="T3">small-ADVZ</ta>
            <ta e="T5" id="Seg_201" s="T4">be-ACTN-LOC</ta>
            <ta e="T6" id="Seg_202" s="T5">teach-DRV-INF</ta>
            <ta e="T7" id="Seg_203" s="T6">big</ta>
            <ta e="T8" id="Seg_204" s="T7">child-ACC</ta>
            <ta e="T9" id="Seg_205" s="T8">NEG</ta>
            <ta e="T10" id="Seg_206" s="T9">teach-DRV-FUT-2SG.O</ta>
            <ta e="T11" id="Seg_207" s="T10">JUSS</ta>
            <ta e="T12" id="Seg_208" s="T11">one-something-LOC</ta>
            <ta e="T13" id="Seg_209" s="T12">eat-FRQ-HAB-3SG.S</ta>
            <ta e="T14" id="Seg_210" s="T13">(s)he-ALL</ta>
            <ta e="T15" id="Seg_211" s="T14">friend-CAR</ta>
            <ta e="T16" id="Seg_212" s="T15">what-ACC-EMPH</ta>
            <ta e="T17" id="Seg_213" s="T16">NEG</ta>
            <ta e="T18" id="Seg_214" s="T17">one.should</ta>
            <ta e="T19" id="Seg_215" s="T18">give-HAB-INF</ta>
            <ta e="T20" id="Seg_216" s="T19">and</ta>
            <ta e="T21" id="Seg_217" s="T20">JUSS</ta>
            <ta e="T22" id="Seg_218" s="T21">oneself.3SG</ta>
            <ta e="T23" id="Seg_219" s="T22">NEG.IMP</ta>
            <ta e="T24" id="Seg_220" s="T23">take-HAB-IMP.3SG/PL.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_221" s="T1">ребенок-ACC</ta>
            <ta e="T3" id="Seg_222" s="T2">надо</ta>
            <ta e="T4" id="Seg_223" s="T3">маленький-ADVZ</ta>
            <ta e="T5" id="Seg_224" s="T4">быть-ACTN-LOC</ta>
            <ta e="T6" id="Seg_225" s="T5">научить-DRV-INF</ta>
            <ta e="T7" id="Seg_226" s="T6">большой</ta>
            <ta e="T8" id="Seg_227" s="T7">ребенок-ACC</ta>
            <ta e="T9" id="Seg_228" s="T8">NEG</ta>
            <ta e="T10" id="Seg_229" s="T9">научить-DRV-FUT-2SG.O</ta>
            <ta e="T11" id="Seg_230" s="T10">JUSS</ta>
            <ta e="T12" id="Seg_231" s="T11">один-нечто-LOC</ta>
            <ta e="T13" id="Seg_232" s="T12">съесть-FRQ-HAB-3SG.S</ta>
            <ta e="T14" id="Seg_233" s="T13">он(а)-ALL</ta>
            <ta e="T15" id="Seg_234" s="T14">друг-CAR</ta>
            <ta e="T16" id="Seg_235" s="T15">что-ACC-EMPH</ta>
            <ta e="T17" id="Seg_236" s="T16">NEG</ta>
            <ta e="T18" id="Seg_237" s="T17">надо</ta>
            <ta e="T19" id="Seg_238" s="T18">дать-HAB-INF</ta>
            <ta e="T20" id="Seg_239" s="T19">и</ta>
            <ta e="T21" id="Seg_240" s="T20">JUSS</ta>
            <ta e="T22" id="Seg_241" s="T21">сам.3SG</ta>
            <ta e="T23" id="Seg_242" s="T22">NEG.IMP</ta>
            <ta e="T24" id="Seg_243" s="T23">взять-HAB-IMP.3SG/PL.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_244" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_245" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_246" s="T3">adj-adj&gt;adv</ta>
            <ta e="T5" id="Seg_247" s="T4">v-v&gt;n-n:case</ta>
            <ta e="T6" id="Seg_248" s="T5">v-v&gt;v-v:inf</ta>
            <ta e="T7" id="Seg_249" s="T6">adj</ta>
            <ta e="T8" id="Seg_250" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_251" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_252" s="T9">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_253" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_254" s="T11">num-n-n:case</ta>
            <ta e="T13" id="Seg_255" s="T12">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T14" id="Seg_256" s="T13">pers-n:case</ta>
            <ta e="T15" id="Seg_257" s="T14">n-n&gt;n</ta>
            <ta e="T16" id="Seg_258" s="T15">interrog-n:case-clit</ta>
            <ta e="T17" id="Seg_259" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_260" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_261" s="T18">v-v&gt;v-v:inf</ta>
            <ta e="T20" id="Seg_262" s="T19">conj</ta>
            <ta e="T21" id="Seg_263" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_264" s="T21">emphpro</ta>
            <ta e="T23" id="Seg_265" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_266" s="T23">v-v&gt;v-v:mood.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_267" s="T1">n</ta>
            <ta e="T3" id="Seg_268" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_269" s="T3">adv</ta>
            <ta e="T5" id="Seg_270" s="T4">n</ta>
            <ta e="T6" id="Seg_271" s="T5">v</ta>
            <ta e="T7" id="Seg_272" s="T6">adj</ta>
            <ta e="T8" id="Seg_273" s="T7">n</ta>
            <ta e="T9" id="Seg_274" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_275" s="T9">v</ta>
            <ta e="T11" id="Seg_276" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_277" s="T11">adv</ta>
            <ta e="T13" id="Seg_278" s="T12">v</ta>
            <ta e="T14" id="Seg_279" s="T13">pers</ta>
            <ta e="T15" id="Seg_280" s="T14">n</ta>
            <ta e="T16" id="Seg_281" s="T15">pro</ta>
            <ta e="T17" id="Seg_282" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_283" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_284" s="T18">v</ta>
            <ta e="T20" id="Seg_285" s="T19">conj</ta>
            <ta e="T21" id="Seg_286" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_287" s="T21">emphpro</ta>
            <ta e="T23" id="Seg_288" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_289" s="T23">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_290" s="T1">np.h:Th</ta>
            <ta e="T6" id="Seg_291" s="T5">v:Th</ta>
            <ta e="T8" id="Seg_292" s="T7">np.h:Th</ta>
            <ta e="T10" id="Seg_293" s="T9">0.2.h:A</ta>
            <ta e="T12" id="Seg_294" s="T11">np:L</ta>
            <ta e="T13" id="Seg_295" s="T12">0.3.h:A</ta>
            <ta e="T14" id="Seg_296" s="T13">pro.h:R</ta>
            <ta e="T16" id="Seg_297" s="T15">pro:Th</ta>
            <ta e="T19" id="Seg_298" s="T18">v:Th</ta>
            <ta e="T24" id="Seg_299" s="T23">0.3.h:A 0.3:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_300" s="T2">ptcl:pred</ta>
            <ta e="T5" id="Seg_301" s="T3">s:temp</ta>
            <ta e="T6" id="Seg_302" s="T5">v:O</ta>
            <ta e="T8" id="Seg_303" s="T7">np.h:O</ta>
            <ta e="T10" id="Seg_304" s="T9">0.2.h:S v:pred</ta>
            <ta e="T13" id="Seg_305" s="T12">0.3.h:S v:pred</ta>
            <ta e="T18" id="Seg_306" s="T17">ptcl:pred</ta>
            <ta e="T19" id="Seg_307" s="T18">v:O</ta>
            <ta e="T24" id="Seg_308" s="T23">0.3.h:S v:pred 0.3:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_309" s="T2">RUS:mod</ta>
            <ta e="T11" id="Seg_310" s="T10">RUS:gram</ta>
            <ta e="T17" id="Seg_311" s="T16">RUS:gram</ta>
            <ta e="T18" id="Seg_312" s="T17">RUS:mod</ta>
            <ta e="T20" id="Seg_313" s="T19">RUS:gram</ta>
            <ta e="T21" id="Seg_314" s="T20">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_315" s="T1">A child must be taught, while (s)he is small.</ta>
            <ta e="T10" id="Seg_316" s="T6">One can't teach a grown-up child.</ta>
            <ta e="T13" id="Seg_317" s="T10">Make him eat with all the others.</ta>
            <ta e="T19" id="Seg_318" s="T13">Don't give anything to him only.</ta>
            <ta e="T24" id="Seg_319" s="T19">And don't let him take [anything] himself.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_320" s="T1">Ein Kind muss erzogen werden, solange es klein ist.</ta>
            <ta e="T10" id="Seg_321" s="T6">Man kann kein großes Kind erziehen.</ta>
            <ta e="T13" id="Seg_322" s="T10">Lass es mit den anderen essen.</ta>
            <ta e="T19" id="Seg_323" s="T13">Gib nicht nur ihm etwas.</ta>
            <ta e="T24" id="Seg_324" s="T19">Und lass es nicht [etwas] selbst nehmen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_325" s="T1">Ребенка надо учить маленького.</ta>
            <ta e="T10" id="Seg_326" s="T6">Большого ребенка не выучишь.</ta>
            <ta e="T13" id="Seg_327" s="T10">Пускай вместе (со всеми) ест.</ta>
            <ta e="T19" id="Seg_328" s="T13">Ему одному ничего не надо давать.</ta>
            <ta e="T24" id="Seg_329" s="T19">И пусть сам не берет.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_330" s="T1">ребенка надо учить маленького</ta>
            <ta e="T10" id="Seg_331" s="T6">большого уж не выучишь</ta>
            <ta e="T13" id="Seg_332" s="T10">пускай ест вместе</ta>
            <ta e="T19" id="Seg_333" s="T13">ему одному не надо давать</ta>
            <ta e="T24" id="Seg_334" s="T19">и пусть сам не берет</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
