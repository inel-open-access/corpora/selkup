<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Churches_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Churches_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">24</ud-information>
            <ud-information attribute-name="# HIAT:w">19</ud-information>
            <ud-information attribute-name="# e">19</ud-information>
            <ud-information attribute-name="# HIAT:u">3</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T20" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Nunmat</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">jewan</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">Nowosibirskan</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">alʼi</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">tʼaku</ts>
                  <nts id="Seg_17" n="HIAT:ip">?</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_20" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">Menan</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">nunmadla</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">kotʼin</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">jezatt</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_35" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">Tet</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">nunmat</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">jes</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">Lʼäbatʼöroɣon</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_49" n="HIAT:w" s="T14">i</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_52" n="HIAT:w" s="T15">Satdəqraɣən</ts>
                  <nts id="Seg_53" n="HIAT:ip">,</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_56" n="HIAT:w" s="T16">i</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_59" n="HIAT:w" s="T17">Кalpašəkkan</ts>
                  <nts id="Seg_60" n="HIAT:ip">,</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_63" n="HIAT:w" s="T18">i</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_66" n="HIAT:w" s="T19">Toɣərəɣin</ts>
                  <nts id="Seg_67" n="HIAT:ip">.</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T20" id="Seg_69" n="sc" s="T1">
               <ts e="T2" id="Seg_71" n="e" s="T1">Nunmat </ts>
               <ts e="T3" id="Seg_73" n="e" s="T2">jewan </ts>
               <ts e="T4" id="Seg_75" n="e" s="T3">Nowosibirskan </ts>
               <ts e="T5" id="Seg_77" n="e" s="T4">alʼi </ts>
               <ts e="T6" id="Seg_79" n="e" s="T5">tʼaku? </ts>
               <ts e="T7" id="Seg_81" n="e" s="T6">Menan </ts>
               <ts e="T8" id="Seg_83" n="e" s="T7">nunmadla </ts>
               <ts e="T9" id="Seg_85" n="e" s="T8">kotʼin </ts>
               <ts e="T10" id="Seg_87" n="e" s="T9">jezatt. </ts>
               <ts e="T11" id="Seg_89" n="e" s="T10">Tet </ts>
               <ts e="T12" id="Seg_91" n="e" s="T11">nunmat </ts>
               <ts e="T13" id="Seg_93" n="e" s="T12">jes </ts>
               <ts e="T14" id="Seg_95" n="e" s="T13">Lʼäbatʼöroɣon </ts>
               <ts e="T15" id="Seg_97" n="e" s="T14">i </ts>
               <ts e="T16" id="Seg_99" n="e" s="T15">Satdəqraɣən, </ts>
               <ts e="T17" id="Seg_101" n="e" s="T16">i </ts>
               <ts e="T18" id="Seg_103" n="e" s="T17">Кalpašəkkan, </ts>
               <ts e="T19" id="Seg_105" n="e" s="T18">i </ts>
               <ts e="T20" id="Seg_107" n="e" s="T19">Toɣərəɣin. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_108" s="T1">PVD_1964_Churches_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_109" s="T6">PVD_1964_Churches_nar.002 (001.002)</ta>
            <ta e="T20" id="Seg_110" s="T10">PVD_1964_Churches_nar.003 (001.003)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_111" s="T1">′нунмат ′jеwан Новосибирскан алʼи ′тʼаку?</ta>
            <ta e="T10" id="Seg_112" s="T6">ме′нан ′нунмадла ′котʼин jезатт.</ta>
            <ta e="T20" id="Seg_113" s="T10">′тет нун мат jес Лʼӓ(и)ба′тʼӧроɣон и Сатдъ′kраɣън, и Кал′пашъккан, и ′Тоɣъръɣин.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_114" s="T1">nunmat jewan Нowosibirskan alʼi tʼaku?</ta>
            <ta e="T10" id="Seg_115" s="T6">menan nunmadla kotʼin jezatt.</ta>
            <ta e="T20" id="Seg_116" s="T10">tet nun mat jes Лʼä(i)batʼöroɣon i Сatdəqraɣən, i Кalpašəkkan, i Тoɣərəɣin.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_117" s="T1">Nunmat jewan Nowosibirskan alʼi tʼaku? </ta>
            <ta e="T10" id="Seg_118" s="T6">Menan nunmadla kotʼin jezatt. </ta>
            <ta e="T20" id="Seg_119" s="T10">Tet nunmat jes Lʼäbatʼöroɣon i Satdəqraɣən, i Кalpašəkkan, i Toɣərəɣin. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_120" s="T1">nu-n-mat</ta>
            <ta e="T3" id="Seg_121" s="T2">je-wa-n</ta>
            <ta e="T4" id="Seg_122" s="T3">Nowosibirs-kan</ta>
            <ta e="T5" id="Seg_123" s="T4">alʼi</ta>
            <ta e="T6" id="Seg_124" s="T5">tʼaku</ta>
            <ta e="T7" id="Seg_125" s="T6">me-nan</ta>
            <ta e="T8" id="Seg_126" s="T7">nu-n-mad-la</ta>
            <ta e="T9" id="Seg_127" s="T8">kotʼi-n</ta>
            <ta e="T10" id="Seg_128" s="T9">je-za-tt</ta>
            <ta e="T11" id="Seg_129" s="T10">tet</ta>
            <ta e="T12" id="Seg_130" s="T11">nu-n-mat</ta>
            <ta e="T13" id="Seg_131" s="T12">je-s</ta>
            <ta e="T14" id="Seg_132" s="T13">Lʼäbatʼör-o-ɣon</ta>
            <ta e="T15" id="Seg_133" s="T14">i</ta>
            <ta e="T16" id="Seg_134" s="T15">Satdəqra-ɣən</ta>
            <ta e="T17" id="Seg_135" s="T16">i</ta>
            <ta e="T18" id="Seg_136" s="T17">Кalpašə-kkan</ta>
            <ta e="T19" id="Seg_137" s="T18">i</ta>
            <ta e="T20" id="Seg_138" s="T19">Toɣər-ə-ɣin</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_139" s="T1">nom-n-maːt</ta>
            <ta e="T3" id="Seg_140" s="T2">eː-nɨ-n</ta>
            <ta e="T4" id="Seg_141" s="T3">Nowosibirsk-qɨn</ta>
            <ta e="T5" id="Seg_142" s="T4">alʼi</ta>
            <ta e="T6" id="Seg_143" s="T5">tʼäkku</ta>
            <ta e="T7" id="Seg_144" s="T6">me-nan</ta>
            <ta e="T8" id="Seg_145" s="T7">nom-n-maːt-la</ta>
            <ta e="T9" id="Seg_146" s="T8">koːci-ŋ</ta>
            <ta e="T10" id="Seg_147" s="T9">eː-sɨ-tɨn</ta>
            <ta e="T11" id="Seg_148" s="T10">tettɨ</ta>
            <ta e="T12" id="Seg_149" s="T11">nom-n-maːt</ta>
            <ta e="T13" id="Seg_150" s="T12">eː-sɨ</ta>
            <ta e="T14" id="Seg_151" s="T13">Lʼäbatʼör-ɨ-qɨn</ta>
            <ta e="T15" id="Seg_152" s="T14">i</ta>
            <ta e="T16" id="Seg_153" s="T15">Satdəqra-qɨn</ta>
            <ta e="T17" id="Seg_154" s="T16">i</ta>
            <ta e="T18" id="Seg_155" s="T17">Kolpaše-qɨn</ta>
            <ta e="T19" id="Seg_156" s="T18">i</ta>
            <ta e="T20" id="Seg_157" s="T19">Toɣər-ɨ-qɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_158" s="T1">god-GEN-house.[NOM]</ta>
            <ta e="T3" id="Seg_159" s="T2">be-CO-3SG.S</ta>
            <ta e="T4" id="Seg_160" s="T3">Novosibirsk-LOC</ta>
            <ta e="T5" id="Seg_161" s="T4">or</ta>
            <ta e="T6" id="Seg_162" s="T5">NEG.EX.[3SG.S]</ta>
            <ta e="T7" id="Seg_163" s="T6">we-ADES</ta>
            <ta e="T8" id="Seg_164" s="T7">god-GEN-house-PL.[NOM]</ta>
            <ta e="T9" id="Seg_165" s="T8">much-ADVZ</ta>
            <ta e="T10" id="Seg_166" s="T9">be-PST-3PL</ta>
            <ta e="T11" id="Seg_167" s="T10">four</ta>
            <ta e="T12" id="Seg_168" s="T11">god-GEN-house.[NOM]</ta>
            <ta e="T13" id="Seg_169" s="T12">be-PST.[3SG.S]</ta>
            <ta e="T14" id="Seg_170" s="T13">Libator-EP-LOC</ta>
            <ta e="T15" id="Seg_171" s="T14">and</ta>
            <ta e="T16" id="Seg_172" s="T15">Novoiljinka-LOC</ta>
            <ta e="T17" id="Seg_173" s="T16">and</ta>
            <ta e="T18" id="Seg_174" s="T17">Kolpashevo-LOC</ta>
            <ta e="T19" id="Seg_175" s="T18">and</ta>
            <ta e="T20" id="Seg_176" s="T19">Togur-EP-LOC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_177" s="T1">бог-GEN-дом.[NOM]</ta>
            <ta e="T3" id="Seg_178" s="T2">быть-CO-3SG.S</ta>
            <ta e="T4" id="Seg_179" s="T3">Новосибирск-LOC</ta>
            <ta e="T5" id="Seg_180" s="T4">али</ta>
            <ta e="T6" id="Seg_181" s="T5">NEG.EX.[3SG.S]</ta>
            <ta e="T7" id="Seg_182" s="T6">мы-ADES</ta>
            <ta e="T8" id="Seg_183" s="T7">бог-GEN-дом-PL.[NOM]</ta>
            <ta e="T9" id="Seg_184" s="T8">много-ADVZ</ta>
            <ta e="T10" id="Seg_185" s="T9">быть-PST-3PL</ta>
            <ta e="T11" id="Seg_186" s="T10">четыре</ta>
            <ta e="T12" id="Seg_187" s="T11">бог-GEN-дом.[NOM]</ta>
            <ta e="T13" id="Seg_188" s="T12">быть-PST.[3SG.S]</ta>
            <ta e="T14" id="Seg_189" s="T13">Либатор-EP-LOC</ta>
            <ta e="T15" id="Seg_190" s="T14">и</ta>
            <ta e="T16" id="Seg_191" s="T15">Новоильинка-LOC</ta>
            <ta e="T17" id="Seg_192" s="T16">и</ta>
            <ta e="T18" id="Seg_193" s="T17">Колпашево-LOC</ta>
            <ta e="T19" id="Seg_194" s="T18">и</ta>
            <ta e="T20" id="Seg_195" s="T19">Тогур-EP-LOC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_196" s="T1">n-n:case-n.[n:case]</ta>
            <ta e="T3" id="Seg_197" s="T2">v-v:ins-v:pn</ta>
            <ta e="T4" id="Seg_198" s="T3">nprop-n:case</ta>
            <ta e="T5" id="Seg_199" s="T4">conj</ta>
            <ta e="T6" id="Seg_200" s="T5">v.[v:pn]</ta>
            <ta e="T7" id="Seg_201" s="T6">pers-n:case</ta>
            <ta e="T8" id="Seg_202" s="T7">n-n:case-n-n:num.[n:case]</ta>
            <ta e="T9" id="Seg_203" s="T8">quant-quant&gt;adv</ta>
            <ta e="T10" id="Seg_204" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_205" s="T10">num</ta>
            <ta e="T12" id="Seg_206" s="T11">n-n:case-n.[n:case]</ta>
            <ta e="T13" id="Seg_207" s="T12">v-v:tense.[v:pn]</ta>
            <ta e="T14" id="Seg_208" s="T13">nprop-n:ins-n:case</ta>
            <ta e="T15" id="Seg_209" s="T14">conj</ta>
            <ta e="T16" id="Seg_210" s="T15">nprop-n:case</ta>
            <ta e="T17" id="Seg_211" s="T16">conj</ta>
            <ta e="T18" id="Seg_212" s="T17">nprop-n:case</ta>
            <ta e="T19" id="Seg_213" s="T18">conj</ta>
            <ta e="T20" id="Seg_214" s="T19">nprop-n:ins-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_215" s="T1">n</ta>
            <ta e="T3" id="Seg_216" s="T2">v</ta>
            <ta e="T4" id="Seg_217" s="T3">nprop</ta>
            <ta e="T5" id="Seg_218" s="T4">conj</ta>
            <ta e="T6" id="Seg_219" s="T5">v</ta>
            <ta e="T7" id="Seg_220" s="T6">pers</ta>
            <ta e="T8" id="Seg_221" s="T7">n</ta>
            <ta e="T9" id="Seg_222" s="T8">adv</ta>
            <ta e="T10" id="Seg_223" s="T9">v</ta>
            <ta e="T11" id="Seg_224" s="T10">num</ta>
            <ta e="T12" id="Seg_225" s="T11">n</ta>
            <ta e="T13" id="Seg_226" s="T12">v</ta>
            <ta e="T14" id="Seg_227" s="T13">nprop</ta>
            <ta e="T15" id="Seg_228" s="T14">conj</ta>
            <ta e="T16" id="Seg_229" s="T15">nprop</ta>
            <ta e="T17" id="Seg_230" s="T16">conj</ta>
            <ta e="T18" id="Seg_231" s="T17">nprop</ta>
            <ta e="T19" id="Seg_232" s="T18">conj</ta>
            <ta e="T20" id="Seg_233" s="T19">nprop</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_234" s="T1">np:Th</ta>
            <ta e="T4" id="Seg_235" s="T3">np:L</ta>
            <ta e="T6" id="Seg_236" s="T5">0.3:Th</ta>
            <ta e="T7" id="Seg_237" s="T6">pro.h:Poss</ta>
            <ta e="T8" id="Seg_238" s="T7">np:Th</ta>
            <ta e="T12" id="Seg_239" s="T11">np:Th</ta>
            <ta e="T14" id="Seg_240" s="T13">np:L</ta>
            <ta e="T16" id="Seg_241" s="T15">np:L</ta>
            <ta e="T18" id="Seg_242" s="T17">np:L</ta>
            <ta e="T20" id="Seg_243" s="T19">np:L</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_244" s="T1">np:S</ta>
            <ta e="T3" id="Seg_245" s="T2">v:pred</ta>
            <ta e="T6" id="Seg_246" s="T5">0.3:S v:pred</ta>
            <ta e="T8" id="Seg_247" s="T7">np:S</ta>
            <ta e="T10" id="Seg_248" s="T9">v:pred</ta>
            <ta e="T12" id="Seg_249" s="T11">np:S</ta>
            <ta e="T13" id="Seg_250" s="T12">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_251" s="T4">RUS:gram</ta>
            <ta e="T15" id="Seg_252" s="T14">RUS:gram</ta>
            <ta e="T17" id="Seg_253" s="T16">RUS:gram</ta>
            <ta e="T18" id="Seg_254" s="T17">RUS:cult</ta>
            <ta e="T19" id="Seg_255" s="T18">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_256" s="T1">Is there a church in Novosibirsk or not?</ta>
            <ta e="T10" id="Seg_257" s="T6">We had many churches.</ta>
            <ta e="T20" id="Seg_258" s="T10">We had four churches: in Libator, in Novoiljinka, in Kolpashevo, in Togur.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_259" s="T1">Gibt es eine Kirche in Novosibirsk oder nicht?</ta>
            <ta e="T10" id="Seg_260" s="T6">Wir hatten viele Kirchen.</ta>
            <ta e="T20" id="Seg_261" s="T10">Wir hatten vier Kirchen: in Libator, in Novoiljinka, in Kolpaschevo, in Togur.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_262" s="T1">Церковь есть в Новосибирске или нет?</ta>
            <ta e="T10" id="Seg_263" s="T6">У нас церквей много было.</ta>
            <ta e="T20" id="Seg_264" s="T10">Четыре церкви (у нас) было в Либаторе, и в Новоильинке, и в Колпашево, и в Тогуре.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_265" s="T1">в Новосибирске церковь есть или нет</ta>
            <ta e="T10" id="Seg_266" s="T6">у нас церквей много было</ta>
            <ta e="T20" id="Seg_267" s="T10">четыре церкви у нас было в Либатӧре и в Новоильинке и Колпашево и в Тогере</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T20" id="Seg_268" s="T10">[KuAI:] Variant: Lʼibatʼöroɣon.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
