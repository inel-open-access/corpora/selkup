<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_ClothForShirts_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_ClothForShirts_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">194</ud-information>
            <ud-information attribute-name="# HIAT:w">135</ud-information>
            <ud-information attribute-name="# e">135</ud-information>
            <ud-information attribute-name="# HIAT:u">27</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T136" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Manan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">qomdäu</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">qɨban</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">jes</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Manan</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">sɨt</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">qɨbanʼatau</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">iːgalau</ts>
                  <nts id="Seg_29" n="HIAT:ip">,</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">qɨːbɨn</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_35" n="HIAT:w" s="T10">jezaɣə</ts>
                  <nts id="Seg_36" n="HIAT:ip">.</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_39" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">Man</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">lawkottə</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">qwannan</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">i</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">tawar</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_56" n="HIAT:w" s="T16">iːtan</ts>
                  <nts id="Seg_57" n="HIAT:ip">.</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_60" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">Wanʼänä</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">qaborɣətɨ</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">tawar</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_71" n="HIAT:w" s="T20">iːtdan</ts>
                  <nts id="Seg_72" n="HIAT:ip">.</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_75" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_77" n="HIAT:w" s="T21">Talle</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_80" n="HIAT:w" s="T22">pennau</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_83" n="HIAT:w" s="T23">stol</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_86" n="HIAT:w" s="T24">barot</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_90" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">I</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_95" n="HIAT:w" s="T26">dʼaja</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_98" n="HIAT:w" s="T27">Aleksej</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_101" n="HIAT:w" s="T28">serta</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_104" n="HIAT:w" s="T29">mekka</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_107" n="HIAT:w" s="T30">tʼärɨn</ts>
                  <nts id="Seg_108" n="HIAT:ip">:</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_111" n="HIAT:w" s="T31">qaj</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_114" n="HIAT:w" s="T32">tawar</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_117" n="HIAT:w" s="T33">immɨtdau</ts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_121" n="HIAT:u" s="T34">
                  <nts id="Seg_122" n="HIAT:ip">“</nts>
                  <ts e="T35" id="Seg_124" n="HIAT:w" s="T34">Qajno</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_127" n="HIAT:w" s="T35">qɨban</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_130" n="HIAT:w" s="T36">ippal</ts>
                  <nts id="Seg_131" n="HIAT:ip">?</nts>
                  <nts id="Seg_132" n="HIAT:ip">”</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_135" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_137" n="HIAT:w" s="T37">A</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_140" n="HIAT:w" s="T38">man</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_143" n="HIAT:w" s="T39">tʼäran</ts>
                  <nts id="Seg_144" n="HIAT:ip">:</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_146" n="HIAT:ip">“</nts>
                  <ts e="T41" id="Seg_148" n="HIAT:w" s="T40">Vanʼänä</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_151" n="HIAT:w" s="T41">qaboːrɣətɨ</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_154" n="HIAT:w" s="T42">mɨm</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_157" n="HIAT:w" s="T43">iːzan</ts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip">”</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_162" n="HIAT:u" s="T44">
                  <nts id="Seg_163" n="HIAT:ip">“</nts>
                  <ts e="T45" id="Seg_165" n="HIAT:w" s="T44">A</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_168" n="HIAT:w" s="T45">Kolanä</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_171" n="HIAT:w" s="T46">qajno</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_174" n="HIAT:w" s="T47">ass</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_177" n="HIAT:w" s="T48">iːzau</ts>
                  <nts id="Seg_178" n="HIAT:ip">?</nts>
                  <nts id="Seg_179" n="HIAT:ip">”</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_182" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_184" n="HIAT:w" s="T49">A</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_187" n="HIAT:w" s="T50">man</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_190" n="HIAT:w" s="T51">tʼaran</ts>
                  <nts id="Seg_191" n="HIAT:ip">:</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_193" n="HIAT:ip">“</nts>
                  <ts e="T53" id="Seg_195" n="HIAT:w" s="T52">Qomdʼeu</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_198" n="HIAT:w" s="T53">tʼangut</ts>
                  <nts id="Seg_199" n="HIAT:ip">.</nts>
                  <nts id="Seg_200" n="HIAT:ip">”</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_203" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_205" n="HIAT:w" s="T54">A</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_208" n="HIAT:w" s="T55">tep</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_211" n="HIAT:w" s="T56">tʼarɨn</ts>
                  <nts id="Seg_212" n="HIAT:ip">:</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_214" n="HIAT:ip">“</nts>
                  <ts e="T58" id="Seg_216" n="HIAT:w" s="T57">Man</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_219" n="HIAT:w" s="T58">qwalle</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_222" n="HIAT:w" s="T59">iːǯau</ts>
                  <nts id="Seg_223" n="HIAT:ip">,</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_226" n="HIAT:w" s="T60">Kolʼenä</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_229" n="HIAT:w" s="T61">qaborɣədɨ</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_232" n="HIAT:w" s="T62">mɨm</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_235" n="HIAT:w" s="T63">idǯau</ts>
                  <nts id="Seg_236" n="HIAT:ip">.</nts>
                  <nts id="Seg_237" n="HIAT:ip">”</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_240" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_242" n="HIAT:w" s="T64">I</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_245" n="HIAT:w" s="T65">qwannɨn</ts>
                  <nts id="Seg_246" n="HIAT:ip">.</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_249" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_251" n="HIAT:w" s="T66">Qwalle</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_254" n="HIAT:w" s="T67">iːbbat</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_257" n="HIAT:w" s="T68">i</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_260" n="HIAT:w" s="T69">mʼaɣuntə</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_263" n="HIAT:w" s="T70">jass</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_266" n="HIAT:w" s="T71">tüan</ts>
                  <nts id="Seg_267" n="HIAT:ip">.</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_270" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_272" n="HIAT:w" s="T72">Maːtqətdə</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_275" n="HIAT:w" s="T73">qwanbaː</ts>
                  <nts id="Seg_276" n="HIAT:ip">.</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_279" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_281" n="HIAT:w" s="T74">A</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_284" n="HIAT:w" s="T75">tötka</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_287" n="HIAT:w" s="T76">Marʼina</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_290" n="HIAT:w" s="T77">tʼärɨn</ts>
                  <nts id="Seg_291" n="HIAT:ip">:</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_293" n="HIAT:ip">“</nts>
                  <ts e="T79" id="Seg_295" n="HIAT:w" s="T78">Qaj</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_298" n="HIAT:w" s="T79">tawar</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_301" n="HIAT:w" s="T80">iːmetdau</ts>
                  <nts id="Seg_302" n="HIAT:ip">?</nts>
                  <nts id="Seg_303" n="HIAT:ip">”</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_306" n="HIAT:u" s="T81">
                  <nts id="Seg_307" n="HIAT:ip">“</nts>
                  <ts e="T82" id="Seg_309" n="HIAT:w" s="T81">Man</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_312" n="HIAT:w" s="T82">Kolʼanä</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_315" n="HIAT:w" s="T83">iːzau</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_318" n="HIAT:w" s="T84">qaborgadɨ</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_321" n="HIAT:w" s="T85">mɨm</ts>
                  <nts id="Seg_322" n="HIAT:ip">.</nts>
                  <nts id="Seg_323" n="HIAT:ip">”</nts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_326" n="HIAT:u" s="T86">
                  <nts id="Seg_327" n="HIAT:ip">“</nts>
                  <ts e="T87" id="Seg_329" n="HIAT:w" s="T86">A</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_332" n="HIAT:w" s="T87">man</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_335" n="HIAT:w" s="T88">as</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_338" n="HIAT:w" s="T89">miǯau</ts>
                  <nts id="Seg_339" n="HIAT:ip">.</nts>
                  <nts id="Seg_340" n="HIAT:ip">”</nts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_343" n="HIAT:u" s="T90">
                  <nts id="Seg_344" n="HIAT:ip">“</nts>
                  <ts e="T91" id="Seg_346" n="HIAT:w" s="T90">Vʼera</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_349" n="HIAT:w" s="T91">Vanʼanä</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_352" n="HIAT:w" s="T92">ipbat</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_355" n="HIAT:w" s="T93">qaborgadɨmɨm</ts>
                  <nts id="Seg_356" n="HIAT:ip">.</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_359" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_361" n="HIAT:w" s="T94">I</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_364" n="HIAT:w" s="T95">bolʼše</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_367" n="HIAT:w" s="T96">qomdət</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_370" n="HIAT:w" s="T97">tebnan</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_373" n="HIAT:w" s="T98">tʼäkku</ts>
                  <nts id="Seg_374" n="HIAT:ip">.</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_377" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_379" n="HIAT:w" s="T99">A</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_382" n="HIAT:w" s="T100">man</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_385" n="HIAT:w" s="T101">taow</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_388" n="HIAT:w" s="T102">Kolʼanä</ts>
                  <nts id="Seg_389" n="HIAT:ip">.</nts>
                  <nts id="Seg_390" n="HIAT:ip">”</nts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_393" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_395" n="HIAT:w" s="T103">A</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_398" n="HIAT:w" s="T104">tʼötka</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_401" n="HIAT:w" s="T105">Marʼina</ts>
                  <nts id="Seg_402" n="HIAT:ip">:</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_404" n="HIAT:ip">“</nts>
                  <ts e="T107" id="Seg_406" n="HIAT:w" s="T106">As</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_409" n="HIAT:w" s="T107">meǯau</ts>
                  <nts id="Seg_410" n="HIAT:ip">.</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_413" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_415" n="HIAT:w" s="T108">Panʼanä</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_418" n="HIAT:w" s="T109">sütčau</ts>
                  <nts id="Seg_419" n="HIAT:ip">,</nts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_422" n="HIAT:w" s="T110">ass</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_425" n="HIAT:w" s="T111">medčau</ts>
                  <nts id="Seg_426" n="HIAT:ip">.</nts>
                  <nts id="Seg_427" n="HIAT:ip">”</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_430" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_432" n="HIAT:w" s="T112">A</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_435" n="HIAT:w" s="T113">Kola</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_438" n="HIAT:w" s="T114">adɨlǯɨːn</ts>
                  <nts id="Seg_439" n="HIAT:ip">,</nts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_442" n="HIAT:w" s="T115">adɨlǯɨːn</ts>
                  <nts id="Seg_443" n="HIAT:ip">.</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_446" n="HIAT:u" s="T116">
                  <nts id="Seg_447" n="HIAT:ip">“</nts>
                  <ts e="T117" id="Seg_449" n="HIAT:w" s="T116">Dʼäja</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_452" n="HIAT:w" s="T117">Lʼeksej</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_455" n="HIAT:w" s="T118">mekka</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_458" n="HIAT:w" s="T119">qaborɣədɨ</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_461" n="HIAT:w" s="T120">mɨ</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_464" n="HIAT:w" s="T121">datčitdɨt</ts>
                  <nts id="Seg_465" n="HIAT:ip">.</nts>
                  <nts id="Seg_466" n="HIAT:ip">”</nts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_469" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_471" n="HIAT:w" s="T122">Tak</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_474" n="HIAT:w" s="T123">jass</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_477" n="HIAT:w" s="T124">tannɨt</ts>
                  <nts id="Seg_478" n="HIAT:ip">.</nts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_481" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_483" n="HIAT:w" s="T125">Na</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_486" n="HIAT:w" s="T126">tawaram</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_489" n="HIAT:w" s="T127">wes</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_492" n="HIAT:w" s="T128">maltšattə</ts>
                  <nts id="Seg_493" n="HIAT:ip">.</nts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_496" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_498" n="HIAT:w" s="T129">Man</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_501" n="HIAT:w" s="T130">patom</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_504" n="HIAT:w" s="T131">rozowaj</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_507" n="HIAT:w" s="T132">tawargam</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_510" n="HIAT:w" s="T133">iːwow</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_513" n="HIAT:w" s="T134">Kolʼanä</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_516" n="HIAT:w" s="T135">qaborgadəmɨm</ts>
                  <nts id="Seg_517" n="HIAT:ip">.</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T136" id="Seg_519" n="sc" s="T1">
               <ts e="T2" id="Seg_521" n="e" s="T1">Manan </ts>
               <ts e="T3" id="Seg_523" n="e" s="T2">qomdäu </ts>
               <ts e="T4" id="Seg_525" n="e" s="T3">qɨban </ts>
               <ts e="T5" id="Seg_527" n="e" s="T4">jes. </ts>
               <ts e="T6" id="Seg_529" n="e" s="T5">Manan </ts>
               <ts e="T7" id="Seg_531" n="e" s="T6">sɨt </ts>
               <ts e="T8" id="Seg_533" n="e" s="T7">qɨbanʼatau </ts>
               <ts e="T9" id="Seg_535" n="e" s="T8">iːgalau, </ts>
               <ts e="T10" id="Seg_537" n="e" s="T9">qɨːbɨn </ts>
               <ts e="T11" id="Seg_539" n="e" s="T10">jezaɣə. </ts>
               <ts e="T12" id="Seg_541" n="e" s="T11">Man </ts>
               <ts e="T13" id="Seg_543" n="e" s="T12">lawkottə </ts>
               <ts e="T14" id="Seg_545" n="e" s="T13">qwannan </ts>
               <ts e="T15" id="Seg_547" n="e" s="T14">i </ts>
               <ts e="T16" id="Seg_549" n="e" s="T15">tawar </ts>
               <ts e="T17" id="Seg_551" n="e" s="T16">iːtan. </ts>
               <ts e="T18" id="Seg_553" n="e" s="T17">Wanʼänä </ts>
               <ts e="T19" id="Seg_555" n="e" s="T18">qaborɣətɨ </ts>
               <ts e="T20" id="Seg_557" n="e" s="T19">tawar </ts>
               <ts e="T21" id="Seg_559" n="e" s="T20">iːtdan. </ts>
               <ts e="T22" id="Seg_561" n="e" s="T21">Talle </ts>
               <ts e="T23" id="Seg_563" n="e" s="T22">pennau </ts>
               <ts e="T24" id="Seg_565" n="e" s="T23">stol </ts>
               <ts e="T25" id="Seg_567" n="e" s="T24">barot. </ts>
               <ts e="T26" id="Seg_569" n="e" s="T25">I </ts>
               <ts e="T27" id="Seg_571" n="e" s="T26">dʼaja </ts>
               <ts e="T28" id="Seg_573" n="e" s="T27">Aleksej </ts>
               <ts e="T29" id="Seg_575" n="e" s="T28">serta </ts>
               <ts e="T30" id="Seg_577" n="e" s="T29">mekka </ts>
               <ts e="T31" id="Seg_579" n="e" s="T30">tʼärɨn: </ts>
               <ts e="T32" id="Seg_581" n="e" s="T31">qaj </ts>
               <ts e="T33" id="Seg_583" n="e" s="T32">tawar </ts>
               <ts e="T34" id="Seg_585" n="e" s="T33">immɨtdau. </ts>
               <ts e="T35" id="Seg_587" n="e" s="T34">“Qajno </ts>
               <ts e="T36" id="Seg_589" n="e" s="T35">qɨban </ts>
               <ts e="T37" id="Seg_591" n="e" s="T36">ippal?” </ts>
               <ts e="T38" id="Seg_593" n="e" s="T37">A </ts>
               <ts e="T39" id="Seg_595" n="e" s="T38">man </ts>
               <ts e="T40" id="Seg_597" n="e" s="T39">tʼäran: </ts>
               <ts e="T41" id="Seg_599" n="e" s="T40">“Vanʼänä </ts>
               <ts e="T42" id="Seg_601" n="e" s="T41">qaboːrɣətɨ </ts>
               <ts e="T43" id="Seg_603" n="e" s="T42">mɨm </ts>
               <ts e="T44" id="Seg_605" n="e" s="T43">iːzan.” </ts>
               <ts e="T45" id="Seg_607" n="e" s="T44">“A </ts>
               <ts e="T46" id="Seg_609" n="e" s="T45">Kolanä </ts>
               <ts e="T47" id="Seg_611" n="e" s="T46">qajno </ts>
               <ts e="T48" id="Seg_613" n="e" s="T47">ass </ts>
               <ts e="T49" id="Seg_615" n="e" s="T48">iːzau?” </ts>
               <ts e="T50" id="Seg_617" n="e" s="T49">A </ts>
               <ts e="T51" id="Seg_619" n="e" s="T50">man </ts>
               <ts e="T52" id="Seg_621" n="e" s="T51">tʼaran: </ts>
               <ts e="T53" id="Seg_623" n="e" s="T52">“Qomdʼeu </ts>
               <ts e="T54" id="Seg_625" n="e" s="T53">tʼangut.” </ts>
               <ts e="T55" id="Seg_627" n="e" s="T54">A </ts>
               <ts e="T56" id="Seg_629" n="e" s="T55">tep </ts>
               <ts e="T57" id="Seg_631" n="e" s="T56">tʼarɨn: </ts>
               <ts e="T58" id="Seg_633" n="e" s="T57">“Man </ts>
               <ts e="T59" id="Seg_635" n="e" s="T58">qwalle </ts>
               <ts e="T60" id="Seg_637" n="e" s="T59">iːǯau, </ts>
               <ts e="T61" id="Seg_639" n="e" s="T60">Kolʼenä </ts>
               <ts e="T62" id="Seg_641" n="e" s="T61">qaborɣədɨ </ts>
               <ts e="T63" id="Seg_643" n="e" s="T62">mɨm </ts>
               <ts e="T64" id="Seg_645" n="e" s="T63">idǯau.” </ts>
               <ts e="T65" id="Seg_647" n="e" s="T64">I </ts>
               <ts e="T66" id="Seg_649" n="e" s="T65">qwannɨn. </ts>
               <ts e="T67" id="Seg_651" n="e" s="T66">Qwalle </ts>
               <ts e="T68" id="Seg_653" n="e" s="T67">iːbbat </ts>
               <ts e="T69" id="Seg_655" n="e" s="T68">i </ts>
               <ts e="T70" id="Seg_657" n="e" s="T69">mʼaɣuntə </ts>
               <ts e="T71" id="Seg_659" n="e" s="T70">jass </ts>
               <ts e="T72" id="Seg_661" n="e" s="T71">tüan. </ts>
               <ts e="T73" id="Seg_663" n="e" s="T72">Maːtqətdə </ts>
               <ts e="T74" id="Seg_665" n="e" s="T73">qwanbaː. </ts>
               <ts e="T75" id="Seg_667" n="e" s="T74">A </ts>
               <ts e="T76" id="Seg_669" n="e" s="T75">tötka </ts>
               <ts e="T77" id="Seg_671" n="e" s="T76">Marʼina </ts>
               <ts e="T78" id="Seg_673" n="e" s="T77">tʼärɨn: </ts>
               <ts e="T79" id="Seg_675" n="e" s="T78">“Qaj </ts>
               <ts e="T80" id="Seg_677" n="e" s="T79">tawar </ts>
               <ts e="T81" id="Seg_679" n="e" s="T80">iːmetdau?” </ts>
               <ts e="T82" id="Seg_681" n="e" s="T81">“Man </ts>
               <ts e="T83" id="Seg_683" n="e" s="T82">Kolʼanä </ts>
               <ts e="T84" id="Seg_685" n="e" s="T83">iːzau </ts>
               <ts e="T85" id="Seg_687" n="e" s="T84">qaborgadɨ </ts>
               <ts e="T86" id="Seg_689" n="e" s="T85">mɨm.” </ts>
               <ts e="T87" id="Seg_691" n="e" s="T86">“A </ts>
               <ts e="T88" id="Seg_693" n="e" s="T87">man </ts>
               <ts e="T89" id="Seg_695" n="e" s="T88">as </ts>
               <ts e="T90" id="Seg_697" n="e" s="T89">miǯau.” </ts>
               <ts e="T91" id="Seg_699" n="e" s="T90">“Vʼera </ts>
               <ts e="T92" id="Seg_701" n="e" s="T91">Vanʼanä </ts>
               <ts e="T93" id="Seg_703" n="e" s="T92">ipbat </ts>
               <ts e="T94" id="Seg_705" n="e" s="T93">qaborgadɨmɨm. </ts>
               <ts e="T95" id="Seg_707" n="e" s="T94">I </ts>
               <ts e="T96" id="Seg_709" n="e" s="T95">bolʼše </ts>
               <ts e="T97" id="Seg_711" n="e" s="T96">qomdət </ts>
               <ts e="T98" id="Seg_713" n="e" s="T97">tebnan </ts>
               <ts e="T99" id="Seg_715" n="e" s="T98">tʼäkku. </ts>
               <ts e="T100" id="Seg_717" n="e" s="T99">A </ts>
               <ts e="T101" id="Seg_719" n="e" s="T100">man </ts>
               <ts e="T102" id="Seg_721" n="e" s="T101">taow </ts>
               <ts e="T103" id="Seg_723" n="e" s="T102">Kolʼanä.” </ts>
               <ts e="T104" id="Seg_725" n="e" s="T103">A </ts>
               <ts e="T105" id="Seg_727" n="e" s="T104">tʼötka </ts>
               <ts e="T106" id="Seg_729" n="e" s="T105">Marʼina: </ts>
               <ts e="T107" id="Seg_731" n="e" s="T106">“As </ts>
               <ts e="T108" id="Seg_733" n="e" s="T107">meǯau. </ts>
               <ts e="T109" id="Seg_735" n="e" s="T108">Panʼanä </ts>
               <ts e="T110" id="Seg_737" n="e" s="T109">sütčau, </ts>
               <ts e="T111" id="Seg_739" n="e" s="T110">ass </ts>
               <ts e="T112" id="Seg_741" n="e" s="T111">medčau.” </ts>
               <ts e="T113" id="Seg_743" n="e" s="T112">A </ts>
               <ts e="T114" id="Seg_745" n="e" s="T113">Kola </ts>
               <ts e="T115" id="Seg_747" n="e" s="T114">adɨlǯɨːn, </ts>
               <ts e="T116" id="Seg_749" n="e" s="T115">adɨlǯɨːn. </ts>
               <ts e="T117" id="Seg_751" n="e" s="T116">“Dʼäja </ts>
               <ts e="T118" id="Seg_753" n="e" s="T117">Lʼeksej </ts>
               <ts e="T119" id="Seg_755" n="e" s="T118">mekka </ts>
               <ts e="T120" id="Seg_757" n="e" s="T119">qaborɣədɨ </ts>
               <ts e="T121" id="Seg_759" n="e" s="T120">mɨ </ts>
               <ts e="T122" id="Seg_761" n="e" s="T121">datčitdɨt.” </ts>
               <ts e="T123" id="Seg_763" n="e" s="T122">Tak </ts>
               <ts e="T124" id="Seg_765" n="e" s="T123">jass </ts>
               <ts e="T125" id="Seg_767" n="e" s="T124">tannɨt. </ts>
               <ts e="T126" id="Seg_769" n="e" s="T125">Na </ts>
               <ts e="T127" id="Seg_771" n="e" s="T126">tawaram </ts>
               <ts e="T128" id="Seg_773" n="e" s="T127">wes </ts>
               <ts e="T129" id="Seg_775" n="e" s="T128">maltšattə. </ts>
               <ts e="T130" id="Seg_777" n="e" s="T129">Man </ts>
               <ts e="T131" id="Seg_779" n="e" s="T130">patom </ts>
               <ts e="T132" id="Seg_781" n="e" s="T131">rozowaj </ts>
               <ts e="T133" id="Seg_783" n="e" s="T132">tawargam </ts>
               <ts e="T134" id="Seg_785" n="e" s="T133">iːwow </ts>
               <ts e="T135" id="Seg_787" n="e" s="T134">Kolʼanä </ts>
               <ts e="T136" id="Seg_789" n="e" s="T135">qaborgadəmɨm. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_790" s="T1">PVD_1964_ClothForShirts_nar.001 (001.001)</ta>
            <ta e="T11" id="Seg_791" s="T5">PVD_1964_ClothForShirts_nar.002 (001.002)</ta>
            <ta e="T17" id="Seg_792" s="T11">PVD_1964_ClothForShirts_nar.003 (001.003)</ta>
            <ta e="T21" id="Seg_793" s="T17">PVD_1964_ClothForShirts_nar.004 (001.004)</ta>
            <ta e="T25" id="Seg_794" s="T21">PVD_1964_ClothForShirts_nar.005 (001.005)</ta>
            <ta e="T34" id="Seg_795" s="T25">PVD_1964_ClothForShirts_nar.006 (001.006)</ta>
            <ta e="T37" id="Seg_796" s="T34">PVD_1964_ClothForShirts_nar.007 (001.007)</ta>
            <ta e="T44" id="Seg_797" s="T37">PVD_1964_ClothForShirts_nar.008 (001.008)</ta>
            <ta e="T49" id="Seg_798" s="T44">PVD_1964_ClothForShirts_nar.009 (001.009)</ta>
            <ta e="T54" id="Seg_799" s="T49">PVD_1964_ClothForShirts_nar.010 (001.010)</ta>
            <ta e="T64" id="Seg_800" s="T54">PVD_1964_ClothForShirts_nar.011 (001.011)</ta>
            <ta e="T66" id="Seg_801" s="T64">PVD_1964_ClothForShirts_nar.012 (001.012)</ta>
            <ta e="T72" id="Seg_802" s="T66">PVD_1964_ClothForShirts_nar.013 (001.013)</ta>
            <ta e="T74" id="Seg_803" s="T72">PVD_1964_ClothForShirts_nar.014 (001.014)</ta>
            <ta e="T81" id="Seg_804" s="T74">PVD_1964_ClothForShirts_nar.015 (001.015)</ta>
            <ta e="T86" id="Seg_805" s="T81">PVD_1964_ClothForShirts_nar.016 (001.016)</ta>
            <ta e="T90" id="Seg_806" s="T86">PVD_1964_ClothForShirts_nar.017 (001.017)</ta>
            <ta e="T94" id="Seg_807" s="T90">PVD_1964_ClothForShirts_nar.018 (001.018)</ta>
            <ta e="T99" id="Seg_808" s="T94">PVD_1964_ClothForShirts_nar.019 (001.019)</ta>
            <ta e="T103" id="Seg_809" s="T99">PVD_1964_ClothForShirts_nar.020 (001.020)</ta>
            <ta e="T108" id="Seg_810" s="T103">PVD_1964_ClothForShirts_nar.021 (001.021)</ta>
            <ta e="T112" id="Seg_811" s="T108">PVD_1964_ClothForShirts_nar.022 (001.022)</ta>
            <ta e="T116" id="Seg_812" s="T112">PVD_1964_ClothForShirts_nar.023 (001.023)</ta>
            <ta e="T122" id="Seg_813" s="T116">PVD_1964_ClothForShirts_nar.024 (001.024)</ta>
            <ta e="T125" id="Seg_814" s="T122">PVD_1964_ClothForShirts_nar.025 (001.025)</ta>
            <ta e="T129" id="Seg_815" s="T125">PVD_1964_ClothForShirts_nar.026 (001.026)</ta>
            <ta e="T136" id="Seg_816" s="T129">PVD_1964_ClothForShirts_nar.027 (001.027)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_817" s="T1">ма′нан ′kомдӓу ′kыбан jес.</ta>
            <ta e="T11" id="Seg_818" s="T5">ма′нан сыт kыбанʼа′тау ′ӣгалау, kы̄бын jезаɣъ.</ta>
            <ta e="T17" id="Seg_819" s="T11">ман ′лавкоттъ kwан′нан и та′вар ′ӣтан.</ta>
            <ta e="T21" id="Seg_820" s="T17">ванʼӓнӓ kа′борɣъ(ы)ты та′вар ′ӣтдан.</ta>
            <ta e="T25" id="Seg_821" s="T21">та′llе пе′ннау ′стол ба‵рот.</ta>
            <ta e="T34" id="Seg_822" s="T25">и дʼаjа Аlек′сей ′серта ′мекка тʼӓ′рын: kай та′вар ′иммыт′дау.</ta>
            <ta e="T37" id="Seg_823" s="T34">kай′но ′kыбан ′иппал?</ta>
            <ta e="T44" id="Seg_824" s="T37">а ман тʼӓран: Ванʼӓнӓ kа′бо̄рɣъты мым ′ӣзан.</ta>
            <ta e="T49" id="Seg_825" s="T44">а ′Коланӓ kайно асс ′ӣзау?</ta>
            <ta e="T54" id="Seg_826" s="T49">а ман тʼа′ран: ′kомдʼеу ′тʼангут.</ta>
            <ta e="T64" id="Seg_827" s="T54">а теп тʼа′рын: ман kwа′llе ′ӣд(т)жау̹, ′Колʼенӓ kаборɣъды мым ′ид̂джау.</ta>
            <ta e="T66" id="Seg_828" s="T64">и ′kwаннын.</ta>
            <ta e="T72" id="Seg_829" s="T66">kwа′llе ′ӣб̂бат и мʼаɣунтъ jасс ′тӱан.</ta>
            <ta e="T74" id="Seg_830" s="T72">′ма̄тkътдъ ′kwанба̄.</ta>
            <ta e="T81" id="Seg_831" s="T74">а тӧтка Марʼина тʼӓ′рын: kай та′вар ′ӣметдау?</ta>
            <ta e="T86" id="Seg_832" s="T81">ман ′Колʼанӓ ′ӣзау̹(w) kаборгады мым.</ta>
            <ta e="T90" id="Seg_833" s="T86">а ман ас ми′джау.</ta>
            <ta e="T94" id="Seg_834" s="T90">Вʼера Ванʼанӓ ипб̂ат kа′боргадымым.</ta>
            <ta e="T99" id="Seg_835" s="T94">и болʼше ′kомдът тебнан тʼӓкку.</ta>
            <ta e="T103" id="Seg_836" s="T99">а ман та′оw ′Колʼанӓ.</ta>
            <ta e="T108" id="Seg_837" s="T103">а тʼӧтка Ма′рʼина ас ме′джау.</ta>
            <ta e="T112" id="Seg_838" s="T108">′Панʼанӓ ′сӱттшау̹, асс мед̂′тшау̹.</ta>
            <ta e="T116" id="Seg_839" s="T112">а ко′lа адыl′джы̄н, адыl′джы̄н.</ta>
            <ta e="T122" id="Seg_840" s="T116">дʼӓjа Лʼексей мекка kаборɣъды мы ′даттшитд̂ыт.</ta>
            <ta e="T125" id="Seg_841" s="T122">так jасс тан′ныт.</ta>
            <ta e="T129" id="Seg_842" s="T125">на та′варам вес маl′тшаттъ.</ta>
            <ta e="T136" id="Seg_843" s="T129">ман па′том розовай та′варгам ′ӣwоw Колʼанӓ kа′боргадъ(ы) мым.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_844" s="T1">manan qomdäu qɨban jes.</ta>
            <ta e="T11" id="Seg_845" s="T5">manan sɨt qɨbanʼatau iːgalau, qɨːbɨn jezaɣə.</ta>
            <ta e="T17" id="Seg_846" s="T11">man lawkottə qwannan i tawar iːtan.</ta>
            <ta e="T21" id="Seg_847" s="T17">wanʼänä qaborɣə(ɨ)tɨ tawar iːtdan.</ta>
            <ta e="T25" id="Seg_848" s="T21">talle pennau stol barot.</ta>
            <ta e="T34" id="Seg_849" s="T25">i dʼaja Аleksej serta mekka tʼärɨn: qaj tawar immɨtdau.</ta>
            <ta e="T37" id="Seg_850" s="T34">qajno qɨban ippal?</ta>
            <ta e="T44" id="Seg_851" s="T37">a man tʼäran: Вanʼänä qaboːrɣətɨ mɨm iːzan.</ta>
            <ta e="T49" id="Seg_852" s="T44">a Кolanä qajno ass iːzau?</ta>
            <ta e="T54" id="Seg_853" s="T49">a man tʼaran: qomdʼeu tʼangut.</ta>
            <ta e="T64" id="Seg_854" s="T54">a tep tʼarɨn: man qwalle iːd(t)ʒau̹, Кolʼenä qaborɣədɨ mɨm id̂ǯau.</ta>
            <ta e="T66" id="Seg_855" s="T64">i qwannɨn.</ta>
            <ta e="T72" id="Seg_856" s="T66">qwalle iːb̂bat i mʼaɣuntə jass tüan.</ta>
            <ta e="T74" id="Seg_857" s="T72">maːtqətdə qwanbaː.</ta>
            <ta e="T81" id="Seg_858" s="T74">a tötka Мarʼina tʼärɨn: qaj tawar iːmetdau?</ta>
            <ta e="T86" id="Seg_859" s="T81">man Кolʼanä iːzau̹(w) qaborgadɨ mɨm.</ta>
            <ta e="T90" id="Seg_860" s="T86">a man as miǯau.</ta>
            <ta e="T94" id="Seg_861" s="T90">Вʼera Вanʼanä ipb̂at qaborgadɨmɨm.</ta>
            <ta e="T99" id="Seg_862" s="T94">i bolʼše qomdət tebnan tʼäkku.</ta>
            <ta e="T103" id="Seg_863" s="T99">a man taow Кolʼanä.</ta>
            <ta e="T108" id="Seg_864" s="T103">a tʼötka Мarʼina as meǯau.</ta>
            <ta e="T112" id="Seg_865" s="T108">Пanʼanä süttšau̹, ass med̂tšau̹.</ta>
            <ta e="T116" id="Seg_866" s="T112">a kola adɨlǯɨːn, adɨlǯɨːn.</ta>
            <ta e="T122" id="Seg_867" s="T116">dʼäja Лʼeksej mekka qaborɣədɨ mɨ dattšitd̂ɨt.</ta>
            <ta e="T125" id="Seg_868" s="T122">tak jass tannɨt.</ta>
            <ta e="T129" id="Seg_869" s="T125">na tawaram wes maltšattə.</ta>
            <ta e="T136" id="Seg_870" s="T129">man patom rozowaj tawargam iːwow Кolʼanä qaborgadə(ɨ) mɨm.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_871" s="T1">Manan qomdäu qɨban jes. </ta>
            <ta e="T11" id="Seg_872" s="T5">Manan sɨt qɨbanʼatau iːgalau, qɨːbɨn jezaɣə. </ta>
            <ta e="T17" id="Seg_873" s="T11">Man lawkottə qwannan i tawar iːtan. </ta>
            <ta e="T21" id="Seg_874" s="T17">Wanʼänä qaborɣətɨ tawar iːtdan. </ta>
            <ta e="T25" id="Seg_875" s="T21">Talle pennau stol barot. </ta>
            <ta e="T34" id="Seg_876" s="T25">I dʼaja Aleksej serta mekka tʼärɨn: qaj tawar immɨtdau. </ta>
            <ta e="T37" id="Seg_877" s="T34">“Qajno qɨban ippal?” </ta>
            <ta e="T44" id="Seg_878" s="T37">A man tʼäran: “Vanʼänä qaboːrɣətɨ mɨm iːzan.” </ta>
            <ta e="T49" id="Seg_879" s="T44">“A Kolanä qajno ass iːzau?” </ta>
            <ta e="T54" id="Seg_880" s="T49">A man tʼaran: “Qomdʼeu tʼangut.” </ta>
            <ta e="T64" id="Seg_881" s="T54">A tep tʼarɨn: “Man qwalle iːǯau, Kolʼenä qaborɣədɨ mɨm idǯau.” </ta>
            <ta e="T66" id="Seg_882" s="T64">I qwannɨn. </ta>
            <ta e="T72" id="Seg_883" s="T66">Qwalle iːbbat i mʼaɣuntə jass tüan. </ta>
            <ta e="T74" id="Seg_884" s="T72">Maːtqətdə qwanbaː. </ta>
            <ta e="T81" id="Seg_885" s="T74">A tötka Marʼina tʼärɨn: “Qaj tawar iːmetdau?” </ta>
            <ta e="T86" id="Seg_886" s="T81">“Man Kolʼanä iːzau qaborgadɨ mɨm.” </ta>
            <ta e="T90" id="Seg_887" s="T86">“A man as miǯau.” </ta>
            <ta e="T94" id="Seg_888" s="T90">“Vʼera Vanʼanä ipbat qaborgadɨmɨm. </ta>
            <ta e="T99" id="Seg_889" s="T94">I bolʼše qomdət tebnan tʼäkku. </ta>
            <ta e="T103" id="Seg_890" s="T99">A man taow Kolʼanä.” </ta>
            <ta e="T108" id="Seg_891" s="T103">A tʼötka Marʼina: “As meǯau. </ta>
            <ta e="T112" id="Seg_892" s="T108">Panʼanä sütčau, ass medčau.” </ta>
            <ta e="T116" id="Seg_893" s="T112">A Kola adɨlǯɨːn, adɨlǯɨːn. </ta>
            <ta e="T122" id="Seg_894" s="T116">“Dʼäja Lʼeksej mekka qaborɣədɨ mɨ datčitdɨt.” </ta>
            <ta e="T125" id="Seg_895" s="T122">Tak jass tannɨt. </ta>
            <ta e="T129" id="Seg_896" s="T125">Na tawaram wes maltšattə. </ta>
            <ta e="T136" id="Seg_897" s="T129">Man patom rozowaj tawargam iːwow Kolʼanä qaborgadəmɨm. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_898" s="T1">ma-nan</ta>
            <ta e="T3" id="Seg_899" s="T2">qomdä-u</ta>
            <ta e="T4" id="Seg_900" s="T3">qɨba-n</ta>
            <ta e="T5" id="Seg_901" s="T4">je-s</ta>
            <ta e="T6" id="Seg_902" s="T5">ma-nan</ta>
            <ta e="T7" id="Seg_903" s="T6">sɨt</ta>
            <ta e="T8" id="Seg_904" s="T7">qɨbanʼata-u</ta>
            <ta e="T9" id="Seg_905" s="T8">iː-ga-la-u</ta>
            <ta e="T10" id="Seg_906" s="T9">qɨːbɨ-n</ta>
            <ta e="T11" id="Seg_907" s="T10">je-za-ɣə</ta>
            <ta e="T12" id="Seg_908" s="T11">man</ta>
            <ta e="T13" id="Seg_909" s="T12">lawko-ttə</ta>
            <ta e="T14" id="Seg_910" s="T13">qwan-na-n</ta>
            <ta e="T15" id="Seg_911" s="T14">i</ta>
            <ta e="T16" id="Seg_912" s="T15">tawar</ta>
            <ta e="T17" id="Seg_913" s="T16">iː-ta-n</ta>
            <ta e="T18" id="Seg_914" s="T17">Wanʼä-nä</ta>
            <ta e="T19" id="Seg_915" s="T18">qaborɣ-ə-tɨ</ta>
            <ta e="T20" id="Seg_916" s="T19">tawar</ta>
            <ta e="T21" id="Seg_917" s="T20">iː-tda-n</ta>
            <ta e="T22" id="Seg_918" s="T21">tal-le</ta>
            <ta e="T23" id="Seg_919" s="T22">pen-na-u</ta>
            <ta e="T24" id="Seg_920" s="T23">stol</ta>
            <ta e="T25" id="Seg_921" s="T24">bar-o-t</ta>
            <ta e="T26" id="Seg_922" s="T25">i</ta>
            <ta e="T27" id="Seg_923" s="T26">dʼaja</ta>
            <ta e="T28" id="Seg_924" s="T27">Aleksej</ta>
            <ta e="T29" id="Seg_925" s="T28">ser-ta</ta>
            <ta e="T30" id="Seg_926" s="T29">mekka</ta>
            <ta e="T31" id="Seg_927" s="T30">tʼärɨ-n</ta>
            <ta e="T32" id="Seg_928" s="T31">qaj</ta>
            <ta e="T33" id="Seg_929" s="T32">tawar</ta>
            <ta e="T34" id="Seg_930" s="T33">i-mmɨ-tda-u</ta>
            <ta e="T35" id="Seg_931" s="T34">qaj-no</ta>
            <ta e="T36" id="Seg_932" s="T35">qɨba-n</ta>
            <ta e="T37" id="Seg_933" s="T36">i-ppa-l</ta>
            <ta e="T38" id="Seg_934" s="T37">a</ta>
            <ta e="T39" id="Seg_935" s="T38">man</ta>
            <ta e="T40" id="Seg_936" s="T39">tʼära-n</ta>
            <ta e="T41" id="Seg_937" s="T40">Vanʼä-nä</ta>
            <ta e="T42" id="Seg_938" s="T41">qaboːrɣə-tɨ</ta>
            <ta e="T43" id="Seg_939" s="T42">mɨ-m</ta>
            <ta e="T44" id="Seg_940" s="T43">iː-za-n</ta>
            <ta e="T45" id="Seg_941" s="T44">a</ta>
            <ta e="T46" id="Seg_942" s="T45">Kola-nä</ta>
            <ta e="T47" id="Seg_943" s="T46">qaj-no</ta>
            <ta e="T48" id="Seg_944" s="T47">ass</ta>
            <ta e="T49" id="Seg_945" s="T48">iː-za-u</ta>
            <ta e="T50" id="Seg_946" s="T49">a</ta>
            <ta e="T51" id="Seg_947" s="T50">man</ta>
            <ta e="T52" id="Seg_948" s="T51">tʼara-n</ta>
            <ta e="T53" id="Seg_949" s="T52">qomdʼe-u</ta>
            <ta e="T54" id="Seg_950" s="T53">tʼangu-t</ta>
            <ta e="T55" id="Seg_951" s="T54">a</ta>
            <ta e="T56" id="Seg_952" s="T55">tep</ta>
            <ta e="T57" id="Seg_953" s="T56">tʼarɨ-n</ta>
            <ta e="T58" id="Seg_954" s="T57">Man</ta>
            <ta e="T59" id="Seg_955" s="T58">qwal-le</ta>
            <ta e="T60" id="Seg_956" s="T59">iː-ǯa-u</ta>
            <ta e="T61" id="Seg_957" s="T60">Kolʼe-nä</ta>
            <ta e="T62" id="Seg_958" s="T61">qaborɣ-ə-dɨ</ta>
            <ta e="T63" id="Seg_959" s="T62">mɨ-m</ta>
            <ta e="T64" id="Seg_960" s="T63">i-dǯa-u</ta>
            <ta e="T65" id="Seg_961" s="T64">i</ta>
            <ta e="T66" id="Seg_962" s="T65">qwan-nɨ-n</ta>
            <ta e="T67" id="Seg_963" s="T66">qwal-le</ta>
            <ta e="T68" id="Seg_964" s="T67">iː-bba-t</ta>
            <ta e="T69" id="Seg_965" s="T68">i</ta>
            <ta e="T70" id="Seg_966" s="T69">mʼaɣuntə</ta>
            <ta e="T71" id="Seg_967" s="T70">jass</ta>
            <ta e="T72" id="Seg_968" s="T71">tü-a-n</ta>
            <ta e="T73" id="Seg_969" s="T72">maːt-qətdə</ta>
            <ta e="T74" id="Seg_970" s="T73">qwan-baː</ta>
            <ta e="T75" id="Seg_971" s="T74">a</ta>
            <ta e="T76" id="Seg_972" s="T75">tötka</ta>
            <ta e="T77" id="Seg_973" s="T76">Marʼina</ta>
            <ta e="T78" id="Seg_974" s="T77">tʼärɨ-n</ta>
            <ta e="T79" id="Seg_975" s="T78">Qaj</ta>
            <ta e="T80" id="Seg_976" s="T79">tawar</ta>
            <ta e="T81" id="Seg_977" s="T80">iː-me-tda-u</ta>
            <ta e="T82" id="Seg_978" s="T81">Man</ta>
            <ta e="T83" id="Seg_979" s="T82">Kolʼa-nä</ta>
            <ta e="T84" id="Seg_980" s="T83">iː-za-u</ta>
            <ta e="T85" id="Seg_981" s="T84">qaborg-a-dɨ</ta>
            <ta e="T86" id="Seg_982" s="T85">mɨ-m</ta>
            <ta e="T87" id="Seg_983" s="T86">a</ta>
            <ta e="T88" id="Seg_984" s="T87">man</ta>
            <ta e="T89" id="Seg_985" s="T88">as</ta>
            <ta e="T90" id="Seg_986" s="T89">mi-ǯa-u</ta>
            <ta e="T91" id="Seg_987" s="T90">Vʼera</ta>
            <ta e="T92" id="Seg_988" s="T91">Vanʼa-nä</ta>
            <ta e="T93" id="Seg_989" s="T92">i-pba-t</ta>
            <ta e="T94" id="Seg_990" s="T93">qaborg-a-dɨ-mɨ-m</ta>
            <ta e="T95" id="Seg_991" s="T94">i</ta>
            <ta e="T96" id="Seg_992" s="T95">bolʼše</ta>
            <ta e="T97" id="Seg_993" s="T96">qomdə-t</ta>
            <ta e="T98" id="Seg_994" s="T97">teb-nan</ta>
            <ta e="T99" id="Seg_995" s="T98">tʼäkku</ta>
            <ta e="T100" id="Seg_996" s="T99">a</ta>
            <ta e="T101" id="Seg_997" s="T100">man</ta>
            <ta e="T102" id="Seg_998" s="T101">tao-w</ta>
            <ta e="T103" id="Seg_999" s="T102">Kolʼa-nä</ta>
            <ta e="T104" id="Seg_1000" s="T103">a</ta>
            <ta e="T105" id="Seg_1001" s="T104">tʼötka</ta>
            <ta e="T106" id="Seg_1002" s="T105">Marʼina</ta>
            <ta e="T107" id="Seg_1003" s="T106">as</ta>
            <ta e="T108" id="Seg_1004" s="T107">me-ǯa-u</ta>
            <ta e="T109" id="Seg_1005" s="T108">Panʼa-nä</ta>
            <ta e="T110" id="Seg_1006" s="T109">süt-ča-u</ta>
            <ta e="T111" id="Seg_1007" s="T110">ass</ta>
            <ta e="T112" id="Seg_1008" s="T111">me-dča-u</ta>
            <ta e="T113" id="Seg_1009" s="T112">a</ta>
            <ta e="T114" id="Seg_1010" s="T113">Kola</ta>
            <ta e="T115" id="Seg_1011" s="T114">adɨ-lǯɨː-n</ta>
            <ta e="T116" id="Seg_1012" s="T115">adɨ-lǯɨː-n</ta>
            <ta e="T117" id="Seg_1013" s="T116">dʼäja</ta>
            <ta e="T118" id="Seg_1014" s="T117">Lʼeksej</ta>
            <ta e="T119" id="Seg_1015" s="T118">mekka</ta>
            <ta e="T120" id="Seg_1016" s="T119">qaborɣ-ə-dɨ</ta>
            <ta e="T121" id="Seg_1017" s="T120">mɨ</ta>
            <ta e="T122" id="Seg_1018" s="T121">dat-či-tdɨ-t</ta>
            <ta e="T123" id="Seg_1019" s="T122">tak</ta>
            <ta e="T124" id="Seg_1020" s="T123">jass</ta>
            <ta e="T125" id="Seg_1021" s="T124">tan-nɨ-t</ta>
            <ta e="T126" id="Seg_1022" s="T125">na</ta>
            <ta e="T127" id="Seg_1023" s="T126">tawar-a-m</ta>
            <ta e="T128" id="Seg_1024" s="T127">wes</ta>
            <ta e="T129" id="Seg_1025" s="T128">maltša-ttə</ta>
            <ta e="T130" id="Seg_1026" s="T129">man</ta>
            <ta e="T131" id="Seg_1027" s="T130">patom</ta>
            <ta e="T132" id="Seg_1028" s="T131">rozowaj</ta>
            <ta e="T133" id="Seg_1029" s="T132">tawar-ga-m</ta>
            <ta e="T134" id="Seg_1030" s="T133">iː-wo-w</ta>
            <ta e="T135" id="Seg_1031" s="T134">Kolʼa-nä</ta>
            <ta e="T136" id="Seg_1032" s="T135">qaborg-a-də-mɨ-m</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1033" s="T1">man-nan</ta>
            <ta e="T3" id="Seg_1034" s="T2">qomdɛ-nɨ</ta>
            <ta e="T4" id="Seg_1035" s="T3">qɨba-ŋ</ta>
            <ta e="T5" id="Seg_1036" s="T4">eː-sɨ</ta>
            <ta e="T6" id="Seg_1037" s="T5">man-nan</ta>
            <ta e="T7" id="Seg_1038" s="T6">sədə</ta>
            <ta e="T8" id="Seg_1039" s="T7">qɨbanʼaǯa-w</ta>
            <ta e="T9" id="Seg_1040" s="T8">iː-ka-la-w</ta>
            <ta e="T10" id="Seg_1041" s="T9">qɨba-ŋ</ta>
            <ta e="T11" id="Seg_1042" s="T10">eː-sɨ-qij</ta>
            <ta e="T12" id="Seg_1043" s="T11">man</ta>
            <ta e="T13" id="Seg_1044" s="T12">lawku-ntə</ta>
            <ta e="T14" id="Seg_1045" s="T13">qwan-nɨ-ŋ</ta>
            <ta e="T15" id="Seg_1046" s="T14">i</ta>
            <ta e="T16" id="Seg_1047" s="T15">tawar</ta>
            <ta e="T17" id="Seg_1048" s="T16">iː-ntɨ-ŋ</ta>
            <ta e="T18" id="Seg_1049" s="T17">Vanʼä-nä</ta>
            <ta e="T19" id="Seg_1050" s="T18">qaborɣ-ɨ-tə</ta>
            <ta e="T20" id="Seg_1051" s="T19">tawar</ta>
            <ta e="T21" id="Seg_1052" s="T20">iː-ntɨ-ŋ</ta>
            <ta e="T22" id="Seg_1053" s="T21">tat-le</ta>
            <ta e="T23" id="Seg_1054" s="T22">pen-nɨ-w</ta>
            <ta e="T24" id="Seg_1055" s="T23">istol</ta>
            <ta e="T25" id="Seg_1056" s="T24">par-ɨ-ntə</ta>
            <ta e="T26" id="Seg_1057" s="T25">i</ta>
            <ta e="T27" id="Seg_1058" s="T26">dʼaja</ta>
            <ta e="T28" id="Seg_1059" s="T27">Alʼeksej</ta>
            <ta e="T29" id="Seg_1060" s="T28">ser-ntɨ</ta>
            <ta e="T30" id="Seg_1061" s="T29">mekka</ta>
            <ta e="T31" id="Seg_1062" s="T30">tʼärɨ-n</ta>
            <ta e="T32" id="Seg_1063" s="T31">qaj</ta>
            <ta e="T33" id="Seg_1064" s="T32">tawar</ta>
            <ta e="T34" id="Seg_1065" s="T33">iː-mbɨ-ntɨ-w</ta>
            <ta e="T35" id="Seg_1066" s="T34">qaj-no</ta>
            <ta e="T36" id="Seg_1067" s="T35">qɨba-ŋ</ta>
            <ta e="T37" id="Seg_1068" s="T36">iː-mbɨ-l</ta>
            <ta e="T38" id="Seg_1069" s="T37">a</ta>
            <ta e="T39" id="Seg_1070" s="T38">man</ta>
            <ta e="T40" id="Seg_1071" s="T39">tʼärɨ-ŋ</ta>
            <ta e="T41" id="Seg_1072" s="T40">Vanʼä-nä</ta>
            <ta e="T42" id="Seg_1073" s="T41">qaborɣ-tə</ta>
            <ta e="T43" id="Seg_1074" s="T42">mɨ-m</ta>
            <ta e="T44" id="Seg_1075" s="T43">iː-sɨ-ŋ</ta>
            <ta e="T45" id="Seg_1076" s="T44">a</ta>
            <ta e="T46" id="Seg_1077" s="T45">Kolʼä-nä</ta>
            <ta e="T47" id="Seg_1078" s="T46">qaj-no</ta>
            <ta e="T48" id="Seg_1079" s="T47">asa</ta>
            <ta e="T49" id="Seg_1080" s="T48">iː-sɨ-w</ta>
            <ta e="T50" id="Seg_1081" s="T49">a</ta>
            <ta e="T51" id="Seg_1082" s="T50">man</ta>
            <ta e="T52" id="Seg_1083" s="T51">tʼärɨ-ŋ</ta>
            <ta e="T53" id="Seg_1084" s="T52">qomdɛ-w</ta>
            <ta e="T54" id="Seg_1085" s="T53">tʼäkku-ntɨ</ta>
            <ta e="T55" id="Seg_1086" s="T54">a</ta>
            <ta e="T56" id="Seg_1087" s="T55">täp</ta>
            <ta e="T57" id="Seg_1088" s="T56">tʼärɨ-n</ta>
            <ta e="T58" id="Seg_1089" s="T57">man</ta>
            <ta e="T59" id="Seg_1090" s="T58">qwan-le</ta>
            <ta e="T60" id="Seg_1091" s="T59">iː-enǯɨ-w</ta>
            <ta e="T61" id="Seg_1092" s="T60">Kolʼä-nä</ta>
            <ta e="T62" id="Seg_1093" s="T61">qaborɣ-ɨ-tə</ta>
            <ta e="T63" id="Seg_1094" s="T62">mɨ-m</ta>
            <ta e="T64" id="Seg_1095" s="T63">iː-enǯɨ-w</ta>
            <ta e="T65" id="Seg_1096" s="T64">i</ta>
            <ta e="T66" id="Seg_1097" s="T65">qwan-nɨ-n</ta>
            <ta e="T67" id="Seg_1098" s="T66">qwan-le</ta>
            <ta e="T68" id="Seg_1099" s="T67">iː-mbɨ-t</ta>
            <ta e="T69" id="Seg_1100" s="T68">i</ta>
            <ta e="T70" id="Seg_1101" s="T69">mäɣuntə</ta>
            <ta e="T71" id="Seg_1102" s="T70">asa</ta>
            <ta e="T72" id="Seg_1103" s="T71">tüː-nɨ-n</ta>
            <ta e="T73" id="Seg_1104" s="T72">maːt-qɨntɨ</ta>
            <ta e="T74" id="Seg_1105" s="T73">qwan-mbɨ</ta>
            <ta e="T75" id="Seg_1106" s="T74">a</ta>
            <ta e="T76" id="Seg_1107" s="T75">tʼötka</ta>
            <ta e="T77" id="Seg_1108" s="T76">Marʼina</ta>
            <ta e="T78" id="Seg_1109" s="T77">tʼärɨ-n</ta>
            <ta e="T79" id="Seg_1110" s="T78">qaj</ta>
            <ta e="T80" id="Seg_1111" s="T79">tawar</ta>
            <ta e="T81" id="Seg_1112" s="T80">iː-mbɨ-ntɨ-l</ta>
            <ta e="T82" id="Seg_1113" s="T81">man</ta>
            <ta e="T83" id="Seg_1114" s="T82">Kolʼä-nä</ta>
            <ta e="T84" id="Seg_1115" s="T83">iː-sɨ-w</ta>
            <ta e="T85" id="Seg_1116" s="T84">qaborɣ-ɨ-tə</ta>
            <ta e="T86" id="Seg_1117" s="T85">mɨ-m</ta>
            <ta e="T87" id="Seg_1118" s="T86">a</ta>
            <ta e="T88" id="Seg_1119" s="T87">man</ta>
            <ta e="T89" id="Seg_1120" s="T88">asa</ta>
            <ta e="T90" id="Seg_1121" s="T89">me-enǯɨ-w</ta>
            <ta e="T91" id="Seg_1122" s="T90">Vera</ta>
            <ta e="T92" id="Seg_1123" s="T91">Vanʼä-nä</ta>
            <ta e="T93" id="Seg_1124" s="T92">iː-mbɨ-t</ta>
            <ta e="T94" id="Seg_1125" s="T93">qaborɣ-ɨ-tə-mɨ-m</ta>
            <ta e="T95" id="Seg_1126" s="T94">i</ta>
            <ta e="T96" id="Seg_1127" s="T95">bolʼše</ta>
            <ta e="T97" id="Seg_1128" s="T96">qomdɛ-tə</ta>
            <ta e="T98" id="Seg_1129" s="T97">täp-nan</ta>
            <ta e="T99" id="Seg_1130" s="T98">tʼäkku</ta>
            <ta e="T100" id="Seg_1131" s="T99">a</ta>
            <ta e="T101" id="Seg_1132" s="T100">man</ta>
            <ta e="T102" id="Seg_1133" s="T101">taw-w</ta>
            <ta e="T103" id="Seg_1134" s="T102">Kolʼä-nä</ta>
            <ta e="T104" id="Seg_1135" s="T103">a</ta>
            <ta e="T105" id="Seg_1136" s="T104">tʼötka</ta>
            <ta e="T106" id="Seg_1137" s="T105">Marʼina</ta>
            <ta e="T107" id="Seg_1138" s="T106">asa</ta>
            <ta e="T108" id="Seg_1139" s="T107">meː-enǯɨ-w</ta>
            <ta e="T109" id="Seg_1140" s="T108">Panʼa-nä</ta>
            <ta e="T110" id="Seg_1141" s="T109">süt-enǯɨ-w</ta>
            <ta e="T111" id="Seg_1142" s="T110">asa</ta>
            <ta e="T112" id="Seg_1143" s="T111">me-enǯɨ-w</ta>
            <ta e="T113" id="Seg_1144" s="T112">a</ta>
            <ta e="T114" id="Seg_1145" s="T113">Kolʼä</ta>
            <ta e="T115" id="Seg_1146" s="T114">adɨ-lǯɨ-n</ta>
            <ta e="T116" id="Seg_1147" s="T115">adɨ-lǯɨ-n</ta>
            <ta e="T117" id="Seg_1148" s="T116">dʼaja</ta>
            <ta e="T118" id="Seg_1149" s="T117">Alʼeksej</ta>
            <ta e="T119" id="Seg_1150" s="T118">mekka</ta>
            <ta e="T120" id="Seg_1151" s="T119">qaborɣ-ɨ-tə</ta>
            <ta e="T121" id="Seg_1152" s="T120">mɨ</ta>
            <ta e="T122" id="Seg_1153" s="T121">tat-enǯɨ-ntɨ-t</ta>
            <ta e="T123" id="Seg_1154" s="T122">tak</ta>
            <ta e="T124" id="Seg_1155" s="T123">asa</ta>
            <ta e="T125" id="Seg_1156" s="T124">tat-nɨ-t</ta>
            <ta e="T126" id="Seg_1157" s="T125">na</ta>
            <ta e="T127" id="Seg_1158" s="T126">tawar-ɨ-m</ta>
            <ta e="T128" id="Seg_1159" s="T127">wesʼ</ta>
            <ta e="T129" id="Seg_1160" s="T128">malčə-tɨn</ta>
            <ta e="T130" id="Seg_1161" s="T129">man</ta>
            <ta e="T131" id="Seg_1162" s="T130">patom</ta>
            <ta e="T132" id="Seg_1163" s="T131">rozowaj</ta>
            <ta e="T133" id="Seg_1164" s="T132">tawar-ka-m</ta>
            <ta e="T134" id="Seg_1165" s="T133">iː-nɨ-w</ta>
            <ta e="T135" id="Seg_1166" s="T134">Kolʼä-nä</ta>
            <ta e="T136" id="Seg_1167" s="T135">qaborɣ-ɨ-tə-mɨ-m</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1168" s="T1">I-ADES</ta>
            <ta e="T3" id="Seg_1169" s="T2">money.[NOM]-1SG</ta>
            <ta e="T4" id="Seg_1170" s="T3">small-ADVZ</ta>
            <ta e="T5" id="Seg_1171" s="T4">be-PST.[3SG.S]</ta>
            <ta e="T6" id="Seg_1172" s="T5">I-ADES</ta>
            <ta e="T7" id="Seg_1173" s="T6">two</ta>
            <ta e="T8" id="Seg_1174" s="T7">child.[NOM]-1SG</ta>
            <ta e="T9" id="Seg_1175" s="T8">son-DIM-PL.[NOM]-1SG</ta>
            <ta e="T10" id="Seg_1176" s="T9">small-ADVZ</ta>
            <ta e="T11" id="Seg_1177" s="T10">be-PST-3DU.S</ta>
            <ta e="T12" id="Seg_1178" s="T11">I.NOM</ta>
            <ta e="T13" id="Seg_1179" s="T12">shop-ILL</ta>
            <ta e="T14" id="Seg_1180" s="T13">leave-CO-1SG.S</ta>
            <ta e="T15" id="Seg_1181" s="T14">and</ta>
            <ta e="T16" id="Seg_1182" s="T15">goods.[NOM]</ta>
            <ta e="T17" id="Seg_1183" s="T16">take-INFER-1SG.S</ta>
            <ta e="T18" id="Seg_1184" s="T17">Vanya-ALL</ta>
            <ta e="T19" id="Seg_1185" s="T18">shirt.[NOM]-EP-3SG</ta>
            <ta e="T20" id="Seg_1186" s="T19">goods.[NOM]</ta>
            <ta e="T21" id="Seg_1187" s="T20">take-INFER-1SG.S</ta>
            <ta e="T22" id="Seg_1188" s="T21">bring-CVB</ta>
            <ta e="T23" id="Seg_1189" s="T22">put-CO-1SG.O</ta>
            <ta e="T24" id="Seg_1190" s="T23">table.[NOM]</ta>
            <ta e="T25" id="Seg_1191" s="T24">top-EP-ILL</ta>
            <ta e="T26" id="Seg_1192" s="T25">and</ta>
            <ta e="T27" id="Seg_1193" s="T26">uncle.[NOM]</ta>
            <ta e="T28" id="Seg_1194" s="T27">Alexei.[NOM]</ta>
            <ta e="T29" id="Seg_1195" s="T28">come.in-INFER.[3SG.S]</ta>
            <ta e="T30" id="Seg_1196" s="T29">I.ALL</ta>
            <ta e="T31" id="Seg_1197" s="T30">say-3SG.S</ta>
            <ta e="T32" id="Seg_1198" s="T31">what.[NOM]</ta>
            <ta e="T33" id="Seg_1199" s="T32">goods.[NOM]</ta>
            <ta e="T34" id="Seg_1200" s="T33">take-PST.NAR-INFER-1SG.O</ta>
            <ta e="T35" id="Seg_1201" s="T34">what-TRL</ta>
            <ta e="T36" id="Seg_1202" s="T35">small-ADVZ</ta>
            <ta e="T37" id="Seg_1203" s="T36">take-PST.NAR-2SG.O</ta>
            <ta e="T38" id="Seg_1204" s="T37">and</ta>
            <ta e="T39" id="Seg_1205" s="T38">I.NOM</ta>
            <ta e="T40" id="Seg_1206" s="T39">say-1SG.S</ta>
            <ta e="T41" id="Seg_1207" s="T40">Vanya-ALL</ta>
            <ta e="T42" id="Seg_1208" s="T41">clothes.[NOM]-3SG</ta>
            <ta e="T43" id="Seg_1209" s="T42">something-ACC</ta>
            <ta e="T44" id="Seg_1210" s="T43">take-PST-1SG.S</ta>
            <ta e="T45" id="Seg_1211" s="T44">and</ta>
            <ta e="T46" id="Seg_1212" s="T45">Kolya-ALL</ta>
            <ta e="T47" id="Seg_1213" s="T46">what-TRL</ta>
            <ta e="T48" id="Seg_1214" s="T47">NEG</ta>
            <ta e="T49" id="Seg_1215" s="T48">take-PST-1SG.O</ta>
            <ta e="T50" id="Seg_1216" s="T49">and</ta>
            <ta e="T51" id="Seg_1217" s="T50">I.NOM</ta>
            <ta e="T52" id="Seg_1218" s="T51">say-1SG.S</ta>
            <ta e="T53" id="Seg_1219" s="T52">money.[NOM]-1SG</ta>
            <ta e="T54" id="Seg_1220" s="T53">NEG.EX-INFER.[3SG.S]</ta>
            <ta e="T55" id="Seg_1221" s="T54">and</ta>
            <ta e="T56" id="Seg_1222" s="T55">(s)he.[NOM]</ta>
            <ta e="T57" id="Seg_1223" s="T56">say-3SG.S</ta>
            <ta e="T58" id="Seg_1224" s="T57">I.NOM</ta>
            <ta e="T59" id="Seg_1225" s="T58">leave-CVB</ta>
            <ta e="T60" id="Seg_1226" s="T59">take-FUT-1SG.O</ta>
            <ta e="T61" id="Seg_1227" s="T60">Kolya-ALL</ta>
            <ta e="T62" id="Seg_1228" s="T61">clothes.[NOM]-EP-3SG</ta>
            <ta e="T63" id="Seg_1229" s="T62">something-ACC</ta>
            <ta e="T64" id="Seg_1230" s="T63">take-FUT-1SG.O</ta>
            <ta e="T65" id="Seg_1231" s="T64">and</ta>
            <ta e="T66" id="Seg_1232" s="T65">leave-CO-3SG.S</ta>
            <ta e="T67" id="Seg_1233" s="T66">leave-CVB</ta>
            <ta e="T68" id="Seg_1234" s="T67">take-PST.NAR-3SG.O</ta>
            <ta e="T69" id="Seg_1235" s="T68">and</ta>
            <ta e="T70" id="Seg_1236" s="T69">we.PL.ALL</ta>
            <ta e="T71" id="Seg_1237" s="T70">NEG</ta>
            <ta e="T72" id="Seg_1238" s="T71">come-CO-3SG.S</ta>
            <ta e="T73" id="Seg_1239" s="T72">house-ILL.3SG</ta>
            <ta e="T74" id="Seg_1240" s="T73">leave-PST.NAR.[3SG.S]</ta>
            <ta e="T75" id="Seg_1241" s="T74">and</ta>
            <ta e="T76" id="Seg_1242" s="T75">aunt.[NOM]</ta>
            <ta e="T77" id="Seg_1243" s="T76">Marina.[NOM]</ta>
            <ta e="T78" id="Seg_1244" s="T77">say-3SG.S</ta>
            <ta e="T79" id="Seg_1245" s="T78">what</ta>
            <ta e="T80" id="Seg_1246" s="T79">goods.[NOM]</ta>
            <ta e="T81" id="Seg_1247" s="T80">take-PST.NAR-INFER-2SG.O</ta>
            <ta e="T82" id="Seg_1248" s="T81">I.NOM</ta>
            <ta e="T83" id="Seg_1249" s="T82">Kolya-ALL</ta>
            <ta e="T84" id="Seg_1250" s="T83">take-PST-1SG.O</ta>
            <ta e="T85" id="Seg_1251" s="T84">clothes.[NOM]-EP-3SG</ta>
            <ta e="T86" id="Seg_1252" s="T85">something-ACC</ta>
            <ta e="T87" id="Seg_1253" s="T86">and</ta>
            <ta e="T88" id="Seg_1254" s="T87">I.NOM</ta>
            <ta e="T89" id="Seg_1255" s="T88">NEG</ta>
            <ta e="T90" id="Seg_1256" s="T89">give-FUT-1SG.O</ta>
            <ta e="T91" id="Seg_1257" s="T90">Vera.[NOM]</ta>
            <ta e="T92" id="Seg_1258" s="T91">Vanya-ALL</ta>
            <ta e="T93" id="Seg_1259" s="T92">take-PST.NAR-3SG.O</ta>
            <ta e="T94" id="Seg_1260" s="T93">clothes.[NOM]-EP-3SG-something-ACC</ta>
            <ta e="T95" id="Seg_1261" s="T94">and</ta>
            <ta e="T96" id="Seg_1262" s="T95">more</ta>
            <ta e="T97" id="Seg_1263" s="T96">money.[NOM]-3SG</ta>
            <ta e="T98" id="Seg_1264" s="T97">(s)he-ADES</ta>
            <ta e="T99" id="Seg_1265" s="T98">NEG.EX.[3SG.S]</ta>
            <ta e="T100" id="Seg_1266" s="T99">and</ta>
            <ta e="T101" id="Seg_1267" s="T100">I.NOM</ta>
            <ta e="T102" id="Seg_1268" s="T101">buy-1SG.O</ta>
            <ta e="T103" id="Seg_1269" s="T102">Kolya-ALL</ta>
            <ta e="T104" id="Seg_1270" s="T103">and</ta>
            <ta e="T105" id="Seg_1271" s="T104">aunt.[NOM]</ta>
            <ta e="T106" id="Seg_1272" s="T105">Marina.[NOM]</ta>
            <ta e="T107" id="Seg_1273" s="T106">NEG</ta>
            <ta e="T108" id="Seg_1274" s="T107">do-FUT-1SG.O</ta>
            <ta e="T109" id="Seg_1275" s="T108">Panja-ALL</ta>
            <ta e="T110" id="Seg_1276" s="T109">sew-FUT-1SG.O</ta>
            <ta e="T111" id="Seg_1277" s="T110">NEG</ta>
            <ta e="T112" id="Seg_1278" s="T111">give-FUT-1SG.O</ta>
            <ta e="T113" id="Seg_1279" s="T112">and</ta>
            <ta e="T114" id="Seg_1280" s="T113">Kolya</ta>
            <ta e="T115" id="Seg_1281" s="T114">wait-TR-3SG.S</ta>
            <ta e="T116" id="Seg_1282" s="T115">wait-TR-3SG.S</ta>
            <ta e="T117" id="Seg_1283" s="T116">uncle.[NOM]</ta>
            <ta e="T118" id="Seg_1284" s="T117">Alexei.[NOM]</ta>
            <ta e="T119" id="Seg_1285" s="T118">I.ALL</ta>
            <ta e="T120" id="Seg_1286" s="T119">clothes.[NOM]-EP-3SG</ta>
            <ta e="T121" id="Seg_1287" s="T120">something.[NOM]</ta>
            <ta e="T122" id="Seg_1288" s="T121">bring-FUT-INFER-3SG.O</ta>
            <ta e="T123" id="Seg_1289" s="T122">so</ta>
            <ta e="T124" id="Seg_1290" s="T123">NEG</ta>
            <ta e="T125" id="Seg_1291" s="T124">bring-CO-3SG.O</ta>
            <ta e="T126" id="Seg_1292" s="T125">this</ta>
            <ta e="T127" id="Seg_1293" s="T126">goods-EP-ACC</ta>
            <ta e="T128" id="Seg_1294" s="T127">all</ta>
            <ta e="T129" id="Seg_1295" s="T128">stop-3PL</ta>
            <ta e="T130" id="Seg_1296" s="T129">I.NOM</ta>
            <ta e="T131" id="Seg_1297" s="T130">then</ta>
            <ta e="T132" id="Seg_1298" s="T131">pink</ta>
            <ta e="T133" id="Seg_1299" s="T132">goods-DIM-ACC</ta>
            <ta e="T134" id="Seg_1300" s="T133">take-CO-1SG.O</ta>
            <ta e="T135" id="Seg_1301" s="T134">Kolya-ALL</ta>
            <ta e="T136" id="Seg_1302" s="T135">shirt.[NOM]-EP-3SG-something-ACC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1303" s="T1">я-ADES</ta>
            <ta e="T3" id="Seg_1304" s="T2">деньги.[NOM]-1SG</ta>
            <ta e="T4" id="Seg_1305" s="T3">маленький-ADVZ</ta>
            <ta e="T5" id="Seg_1306" s="T4">быть-PST.[3SG.S]</ta>
            <ta e="T6" id="Seg_1307" s="T5">я-ADES</ta>
            <ta e="T7" id="Seg_1308" s="T6">два</ta>
            <ta e="T8" id="Seg_1309" s="T7">ребенок.[NOM]-1SG</ta>
            <ta e="T9" id="Seg_1310" s="T8">сын-DIM-PL.[NOM]-1SG</ta>
            <ta e="T10" id="Seg_1311" s="T9">маленький-ADVZ</ta>
            <ta e="T11" id="Seg_1312" s="T10">быть-PST-3DU.S</ta>
            <ta e="T12" id="Seg_1313" s="T11">я.NOM</ta>
            <ta e="T13" id="Seg_1314" s="T12">лавка-ILL</ta>
            <ta e="T14" id="Seg_1315" s="T13">отправиться-CO-1SG.S</ta>
            <ta e="T15" id="Seg_1316" s="T14">и</ta>
            <ta e="T16" id="Seg_1317" s="T15">товар.[NOM]</ta>
            <ta e="T17" id="Seg_1318" s="T16">взять-INFER-1SG.S</ta>
            <ta e="T18" id="Seg_1319" s="T17">Ваня-ALL</ta>
            <ta e="T19" id="Seg_1320" s="T18">рубашка.[NOM]-EP-3SG</ta>
            <ta e="T20" id="Seg_1321" s="T19">товар.[NOM]</ta>
            <ta e="T21" id="Seg_1322" s="T20">взять-INFER-1SG.S</ta>
            <ta e="T22" id="Seg_1323" s="T21">принести-CVB</ta>
            <ta e="T23" id="Seg_1324" s="T22">положить-CO-1SG.O</ta>
            <ta e="T24" id="Seg_1325" s="T23">стол.[NOM]</ta>
            <ta e="T25" id="Seg_1326" s="T24">верхняя.часть-EP-ILL</ta>
            <ta e="T26" id="Seg_1327" s="T25">и</ta>
            <ta e="T27" id="Seg_1328" s="T26">дядя.[NOM]</ta>
            <ta e="T28" id="Seg_1329" s="T27">Алексей.[NOM]</ta>
            <ta e="T29" id="Seg_1330" s="T28">зайти-INFER.[3SG.S]</ta>
            <ta e="T30" id="Seg_1331" s="T29">я.ALL</ta>
            <ta e="T31" id="Seg_1332" s="T30">сказать-3SG.S</ta>
            <ta e="T32" id="Seg_1333" s="T31">что.[NOM]</ta>
            <ta e="T33" id="Seg_1334" s="T32">товар.[NOM]</ta>
            <ta e="T34" id="Seg_1335" s="T33">взять-PST.NAR-INFER-1SG.O</ta>
            <ta e="T35" id="Seg_1336" s="T34">что-TRL</ta>
            <ta e="T36" id="Seg_1337" s="T35">маленький-ADVZ</ta>
            <ta e="T37" id="Seg_1338" s="T36">взять-PST.NAR-2SG.O</ta>
            <ta e="T38" id="Seg_1339" s="T37">а</ta>
            <ta e="T39" id="Seg_1340" s="T38">я.NOM</ta>
            <ta e="T40" id="Seg_1341" s="T39">сказать-1SG.S</ta>
            <ta e="T41" id="Seg_1342" s="T40">Ваня-ALL</ta>
            <ta e="T42" id="Seg_1343" s="T41">одежда.[NOM]-3SG</ta>
            <ta e="T43" id="Seg_1344" s="T42">нечто-ACC</ta>
            <ta e="T44" id="Seg_1345" s="T43">взять-PST-1SG.S</ta>
            <ta e="T45" id="Seg_1346" s="T44">а</ta>
            <ta e="T46" id="Seg_1347" s="T45">Коля-ALL</ta>
            <ta e="T47" id="Seg_1348" s="T46">что-TRL</ta>
            <ta e="T48" id="Seg_1349" s="T47">NEG</ta>
            <ta e="T49" id="Seg_1350" s="T48">взять-PST-1SG.O</ta>
            <ta e="T50" id="Seg_1351" s="T49">а</ta>
            <ta e="T51" id="Seg_1352" s="T50">я.NOM</ta>
            <ta e="T52" id="Seg_1353" s="T51">сказать-1SG.S</ta>
            <ta e="T53" id="Seg_1354" s="T52">деньги.[NOM]-1SG</ta>
            <ta e="T54" id="Seg_1355" s="T53">NEG.EX-INFER.[3SG.S]</ta>
            <ta e="T55" id="Seg_1356" s="T54">а</ta>
            <ta e="T56" id="Seg_1357" s="T55">он(а).[NOM]</ta>
            <ta e="T57" id="Seg_1358" s="T56">сказать-3SG.S</ta>
            <ta e="T58" id="Seg_1359" s="T57">я.NOM</ta>
            <ta e="T59" id="Seg_1360" s="T58">отправиться-CVB</ta>
            <ta e="T60" id="Seg_1361" s="T59">взять-FUT-1SG.O</ta>
            <ta e="T61" id="Seg_1362" s="T60">Коля-ALL</ta>
            <ta e="T62" id="Seg_1363" s="T61">одежда.[NOM]-EP-3SG</ta>
            <ta e="T63" id="Seg_1364" s="T62">нечто-ACC</ta>
            <ta e="T64" id="Seg_1365" s="T63">взять-FUT-1SG.O</ta>
            <ta e="T65" id="Seg_1366" s="T64">и</ta>
            <ta e="T66" id="Seg_1367" s="T65">отправиться-CO-3SG.S</ta>
            <ta e="T67" id="Seg_1368" s="T66">отправиться-CVB</ta>
            <ta e="T68" id="Seg_1369" s="T67">взять-PST.NAR-3SG.O</ta>
            <ta e="T69" id="Seg_1370" s="T68">и</ta>
            <ta e="T70" id="Seg_1371" s="T69">мы.PL.ALL</ta>
            <ta e="T71" id="Seg_1372" s="T70">NEG</ta>
            <ta e="T72" id="Seg_1373" s="T71">прийти-CO-3SG.S</ta>
            <ta e="T73" id="Seg_1374" s="T72">дом-ILL.3SG</ta>
            <ta e="T74" id="Seg_1375" s="T73">отправиться-PST.NAR.[3SG.S]</ta>
            <ta e="T75" id="Seg_1376" s="T74">а</ta>
            <ta e="T76" id="Seg_1377" s="T75">тетка.[NOM]</ta>
            <ta e="T77" id="Seg_1378" s="T76">Марина.[NOM]</ta>
            <ta e="T78" id="Seg_1379" s="T77">сказать-3SG.S</ta>
            <ta e="T79" id="Seg_1380" s="T78">что</ta>
            <ta e="T80" id="Seg_1381" s="T79">товар.[NOM]</ta>
            <ta e="T81" id="Seg_1382" s="T80">взять-PST.NAR-INFER-2SG.O</ta>
            <ta e="T82" id="Seg_1383" s="T81">я.NOM</ta>
            <ta e="T83" id="Seg_1384" s="T82">Коля-ALL</ta>
            <ta e="T84" id="Seg_1385" s="T83">взять-PST-1SG.O</ta>
            <ta e="T85" id="Seg_1386" s="T84">одежда.[NOM]-EP-3SG</ta>
            <ta e="T86" id="Seg_1387" s="T85">нечто-ACC</ta>
            <ta e="T87" id="Seg_1388" s="T86">а</ta>
            <ta e="T88" id="Seg_1389" s="T87">я.NOM</ta>
            <ta e="T89" id="Seg_1390" s="T88">NEG</ta>
            <ta e="T90" id="Seg_1391" s="T89">дать-FUT-1SG.O</ta>
            <ta e="T91" id="Seg_1392" s="T90">Вера.[NOM]</ta>
            <ta e="T92" id="Seg_1393" s="T91">Ваня-ALL</ta>
            <ta e="T93" id="Seg_1394" s="T92">взять-PST.NAR-3SG.O</ta>
            <ta e="T94" id="Seg_1395" s="T93">одежда.[NOM]-EP-3SG-нечто-ACC</ta>
            <ta e="T95" id="Seg_1396" s="T94">и</ta>
            <ta e="T96" id="Seg_1397" s="T95">больше</ta>
            <ta e="T97" id="Seg_1398" s="T96">деньги.[NOM]-3SG</ta>
            <ta e="T98" id="Seg_1399" s="T97">он(а)-ADES</ta>
            <ta e="T99" id="Seg_1400" s="T98">NEG.EX.[3SG.S]</ta>
            <ta e="T100" id="Seg_1401" s="T99">а</ta>
            <ta e="T101" id="Seg_1402" s="T100">я.NOM</ta>
            <ta e="T102" id="Seg_1403" s="T101">купить-1SG.O</ta>
            <ta e="T103" id="Seg_1404" s="T102">Коля-ALL</ta>
            <ta e="T104" id="Seg_1405" s="T103">а</ta>
            <ta e="T105" id="Seg_1406" s="T104">тетка.[NOM]</ta>
            <ta e="T106" id="Seg_1407" s="T105">Марина.[NOM]</ta>
            <ta e="T107" id="Seg_1408" s="T106">NEG</ta>
            <ta e="T108" id="Seg_1409" s="T107">сделать-FUT-1SG.O</ta>
            <ta e="T109" id="Seg_1410" s="T108">Паня-ALL</ta>
            <ta e="T110" id="Seg_1411" s="T109">сшить-FUT-1SG.O</ta>
            <ta e="T111" id="Seg_1412" s="T110">NEG</ta>
            <ta e="T112" id="Seg_1413" s="T111">дать-FUT-1SG.O</ta>
            <ta e="T113" id="Seg_1414" s="T112">а</ta>
            <ta e="T114" id="Seg_1415" s="T113">Коля</ta>
            <ta e="T115" id="Seg_1416" s="T114">ждать-TR-3SG.S</ta>
            <ta e="T116" id="Seg_1417" s="T115">ждать-TR-3SG.S</ta>
            <ta e="T117" id="Seg_1418" s="T116">дядя.[NOM]</ta>
            <ta e="T118" id="Seg_1419" s="T117">Алексей.[NOM]</ta>
            <ta e="T119" id="Seg_1420" s="T118">я.ALL</ta>
            <ta e="T120" id="Seg_1421" s="T119">одежда.[NOM]-EP-3SG</ta>
            <ta e="T121" id="Seg_1422" s="T120">нечто.[NOM]</ta>
            <ta e="T122" id="Seg_1423" s="T121">принести-FUT-INFER-3SG.O</ta>
            <ta e="T123" id="Seg_1424" s="T122">так</ta>
            <ta e="T124" id="Seg_1425" s="T123">NEG</ta>
            <ta e="T125" id="Seg_1426" s="T124">принести-CO-3SG.O</ta>
            <ta e="T126" id="Seg_1427" s="T125">этот</ta>
            <ta e="T127" id="Seg_1428" s="T126">товар-EP-ACC</ta>
            <ta e="T128" id="Seg_1429" s="T127">весь</ta>
            <ta e="T129" id="Seg_1430" s="T128">закончить-3PL</ta>
            <ta e="T130" id="Seg_1431" s="T129">я.NOM</ta>
            <ta e="T131" id="Seg_1432" s="T130">потом</ta>
            <ta e="T132" id="Seg_1433" s="T131">розовый</ta>
            <ta e="T133" id="Seg_1434" s="T132">товар-DIM-ACC</ta>
            <ta e="T134" id="Seg_1435" s="T133">взять-CO-1SG.O</ta>
            <ta e="T135" id="Seg_1436" s="T134">Коля-ALL</ta>
            <ta e="T136" id="Seg_1437" s="T135">рубашка.[NOM]-EP-3SG-нечто-ACC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1438" s="T1">pers-n:case</ta>
            <ta e="T3" id="Seg_1439" s="T2">n.[n:case]-n:poss</ta>
            <ta e="T4" id="Seg_1440" s="T3">adj-adj&gt;adv</ta>
            <ta e="T5" id="Seg_1441" s="T4">v-v:tense.[v:pn]</ta>
            <ta e="T6" id="Seg_1442" s="T5">pers-n:case</ta>
            <ta e="T7" id="Seg_1443" s="T6">num</ta>
            <ta e="T8" id="Seg_1444" s="T7">n.[n:case]-n:poss</ta>
            <ta e="T9" id="Seg_1445" s="T8">n-n&gt;n-n:num.[n:case]-n:poss</ta>
            <ta e="T10" id="Seg_1446" s="T9">adj-adj&gt;adv</ta>
            <ta e="T11" id="Seg_1447" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_1448" s="T11">pers</ta>
            <ta e="T13" id="Seg_1449" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_1450" s="T13">v-v:ins-v:pn</ta>
            <ta e="T15" id="Seg_1451" s="T14">conj</ta>
            <ta e="T16" id="Seg_1452" s="T15">n.[n:case]</ta>
            <ta e="T17" id="Seg_1453" s="T16">v-v:mood-v:pn</ta>
            <ta e="T18" id="Seg_1454" s="T17">nprop-n:case</ta>
            <ta e="T19" id="Seg_1455" s="T18">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T20" id="Seg_1456" s="T19">n.[n:case]</ta>
            <ta e="T21" id="Seg_1457" s="T20">v-v:mood-v:pn</ta>
            <ta e="T22" id="Seg_1458" s="T21">v-v&gt;adv</ta>
            <ta e="T23" id="Seg_1459" s="T22">v-v:ins-v:pn</ta>
            <ta e="T24" id="Seg_1460" s="T23">n.[n:case]</ta>
            <ta e="T25" id="Seg_1461" s="T24">n-n:ins-n:case</ta>
            <ta e="T26" id="Seg_1462" s="T25">conj</ta>
            <ta e="T27" id="Seg_1463" s="T26">n.[n:case]</ta>
            <ta e="T28" id="Seg_1464" s="T27">nprop.[n:case]</ta>
            <ta e="T29" id="Seg_1465" s="T28">v-v:mood.[v:pn]</ta>
            <ta e="T30" id="Seg_1466" s="T29">pers</ta>
            <ta e="T31" id="Seg_1467" s="T30">v-v:pn</ta>
            <ta e="T32" id="Seg_1468" s="T31">interrog.[n:case]</ta>
            <ta e="T33" id="Seg_1469" s="T32">n.[n:case]</ta>
            <ta e="T34" id="Seg_1470" s="T33">v-v:tense-v:mood-v:pn</ta>
            <ta e="T35" id="Seg_1471" s="T34">interrog-n:case</ta>
            <ta e="T36" id="Seg_1472" s="T35">adj-adj&gt;adv</ta>
            <ta e="T37" id="Seg_1473" s="T36">v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_1474" s="T37">conj</ta>
            <ta e="T39" id="Seg_1475" s="T38">pers</ta>
            <ta e="T40" id="Seg_1476" s="T39">v-v:pn</ta>
            <ta e="T41" id="Seg_1477" s="T40">nprop-n:case</ta>
            <ta e="T42" id="Seg_1478" s="T41">n.[n:case]-n:poss</ta>
            <ta e="T43" id="Seg_1479" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_1480" s="T43">v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_1481" s="T44">conj</ta>
            <ta e="T46" id="Seg_1482" s="T45">nprop-n:case</ta>
            <ta e="T47" id="Seg_1483" s="T46">interrog-n:case</ta>
            <ta e="T48" id="Seg_1484" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_1485" s="T48">v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_1486" s="T49">conj</ta>
            <ta e="T51" id="Seg_1487" s="T50">pers</ta>
            <ta e="T52" id="Seg_1488" s="T51">v-v:pn</ta>
            <ta e="T53" id="Seg_1489" s="T52">n.[n:case]-n:poss</ta>
            <ta e="T54" id="Seg_1490" s="T53">v-v:mood.[v:pn]</ta>
            <ta e="T55" id="Seg_1491" s="T54">conj</ta>
            <ta e="T56" id="Seg_1492" s="T55">pers.[n:case]</ta>
            <ta e="T57" id="Seg_1493" s="T56">v-v:pn</ta>
            <ta e="T58" id="Seg_1494" s="T57">pers</ta>
            <ta e="T59" id="Seg_1495" s="T58">v-v&gt;adv</ta>
            <ta e="T60" id="Seg_1496" s="T59">v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_1497" s="T60">nprop-n:case</ta>
            <ta e="T62" id="Seg_1498" s="T61">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T63" id="Seg_1499" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_1500" s="T63">v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_1501" s="T64">conj</ta>
            <ta e="T66" id="Seg_1502" s="T65">v-v:ins-v:pn</ta>
            <ta e="T67" id="Seg_1503" s="T66">v-v&gt;adv</ta>
            <ta e="T68" id="Seg_1504" s="T67">v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_1505" s="T68">conj</ta>
            <ta e="T70" id="Seg_1506" s="T69">pers</ta>
            <ta e="T71" id="Seg_1507" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_1508" s="T71">v-v:ins-v:pn</ta>
            <ta e="T73" id="Seg_1509" s="T72">n-n:case.poss</ta>
            <ta e="T74" id="Seg_1510" s="T73">v-v:tense.[v:pn]</ta>
            <ta e="T75" id="Seg_1511" s="T74">conj</ta>
            <ta e="T76" id="Seg_1512" s="T75">n.[n:case]</ta>
            <ta e="T77" id="Seg_1513" s="T76">nprop.[n:case]</ta>
            <ta e="T78" id="Seg_1514" s="T77">v-v:pn</ta>
            <ta e="T79" id="Seg_1515" s="T78">interrog</ta>
            <ta e="T80" id="Seg_1516" s="T79">n.[n:case]</ta>
            <ta e="T81" id="Seg_1517" s="T80">v-v:tense-v:mood-v:pn</ta>
            <ta e="T82" id="Seg_1518" s="T81">pers</ta>
            <ta e="T83" id="Seg_1519" s="T82">nprop-n:case</ta>
            <ta e="T84" id="Seg_1520" s="T83">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_1521" s="T84">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T86" id="Seg_1522" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_1523" s="T86">conj</ta>
            <ta e="T88" id="Seg_1524" s="T87">pers</ta>
            <ta e="T89" id="Seg_1525" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_1526" s="T89">v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_1527" s="T90">nprop.[n:case]</ta>
            <ta e="T92" id="Seg_1528" s="T91">nprop-n:case</ta>
            <ta e="T93" id="Seg_1529" s="T92">v-v:tense-v:pn</ta>
            <ta e="T94" id="Seg_1530" s="T93">n.[n:case]-n:ins-n:poss-n-n:case</ta>
            <ta e="T95" id="Seg_1531" s="T94">conj</ta>
            <ta e="T96" id="Seg_1532" s="T95">adv</ta>
            <ta e="T97" id="Seg_1533" s="T96">n.[n:case]-n:poss</ta>
            <ta e="T98" id="Seg_1534" s="T97">pers-n:case</ta>
            <ta e="T99" id="Seg_1535" s="T98">v.[v:pn]</ta>
            <ta e="T100" id="Seg_1536" s="T99">conj</ta>
            <ta e="T101" id="Seg_1537" s="T100">pers</ta>
            <ta e="T102" id="Seg_1538" s="T101">v-v:pn</ta>
            <ta e="T103" id="Seg_1539" s="T102">nprop-n:case</ta>
            <ta e="T104" id="Seg_1540" s="T103">conj</ta>
            <ta e="T105" id="Seg_1541" s="T104">n.[n:case]</ta>
            <ta e="T106" id="Seg_1542" s="T105">nprop.[n:case]</ta>
            <ta e="T107" id="Seg_1543" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_1544" s="T107">v-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_1545" s="T108">nprop-n:case</ta>
            <ta e="T110" id="Seg_1546" s="T109">v-v:tense-v:pn</ta>
            <ta e="T111" id="Seg_1547" s="T110">ptcl</ta>
            <ta e="T112" id="Seg_1548" s="T111">v-v:tense-v:pn</ta>
            <ta e="T113" id="Seg_1549" s="T112">conj</ta>
            <ta e="T114" id="Seg_1550" s="T113">nprop</ta>
            <ta e="T115" id="Seg_1551" s="T114">v-v&gt;v-v:pn</ta>
            <ta e="T116" id="Seg_1552" s="T115">v-v&gt;v-v:pn</ta>
            <ta e="T117" id="Seg_1553" s="T116">n.[n:case]</ta>
            <ta e="T118" id="Seg_1554" s="T117">nprop.[n:case]</ta>
            <ta e="T119" id="Seg_1555" s="T118">pers</ta>
            <ta e="T120" id="Seg_1556" s="T119">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T121" id="Seg_1557" s="T120">n.[n:case]</ta>
            <ta e="T122" id="Seg_1558" s="T121">v-v:tense-v:mood-v:pn</ta>
            <ta e="T123" id="Seg_1559" s="T122">adv</ta>
            <ta e="T124" id="Seg_1560" s="T123">ptcl</ta>
            <ta e="T125" id="Seg_1561" s="T124">v-v:ins-v:pn</ta>
            <ta e="T126" id="Seg_1562" s="T125">dem</ta>
            <ta e="T127" id="Seg_1563" s="T126">n-n:ins-n:case</ta>
            <ta e="T128" id="Seg_1564" s="T127">quant</ta>
            <ta e="T129" id="Seg_1565" s="T128">v-v:pn</ta>
            <ta e="T130" id="Seg_1566" s="T129">pers</ta>
            <ta e="T131" id="Seg_1567" s="T130">adv</ta>
            <ta e="T132" id="Seg_1568" s="T131">adj</ta>
            <ta e="T133" id="Seg_1569" s="T132">n-n&gt;n-n:case</ta>
            <ta e="T134" id="Seg_1570" s="T133">v-v:ins-v:pn</ta>
            <ta e="T135" id="Seg_1571" s="T134">nprop-n:case</ta>
            <ta e="T136" id="Seg_1572" s="T135">n.[n:case]-n:ins-n:poss-n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1573" s="T1">pers</ta>
            <ta e="T3" id="Seg_1574" s="T2">n</ta>
            <ta e="T4" id="Seg_1575" s="T3">adv</ta>
            <ta e="T5" id="Seg_1576" s="T4">v</ta>
            <ta e="T6" id="Seg_1577" s="T5">pers</ta>
            <ta e="T7" id="Seg_1578" s="T6">num</ta>
            <ta e="T8" id="Seg_1579" s="T7">n</ta>
            <ta e="T9" id="Seg_1580" s="T8">n</ta>
            <ta e="T10" id="Seg_1581" s="T9">adj</ta>
            <ta e="T11" id="Seg_1582" s="T10">v</ta>
            <ta e="T12" id="Seg_1583" s="T11">pers</ta>
            <ta e="T13" id="Seg_1584" s="T12">n</ta>
            <ta e="T14" id="Seg_1585" s="T13">v</ta>
            <ta e="T15" id="Seg_1586" s="T14">conj</ta>
            <ta e="T16" id="Seg_1587" s="T15">n</ta>
            <ta e="T17" id="Seg_1588" s="T16">v</ta>
            <ta e="T18" id="Seg_1589" s="T17">nprop</ta>
            <ta e="T19" id="Seg_1590" s="T18">n</ta>
            <ta e="T20" id="Seg_1591" s="T19">n</ta>
            <ta e="T21" id="Seg_1592" s="T20">v</ta>
            <ta e="T22" id="Seg_1593" s="T21">adv</ta>
            <ta e="T23" id="Seg_1594" s="T22">v</ta>
            <ta e="T24" id="Seg_1595" s="T23">n</ta>
            <ta e="T25" id="Seg_1596" s="T24">n</ta>
            <ta e="T26" id="Seg_1597" s="T25">conj</ta>
            <ta e="T27" id="Seg_1598" s="T26">n</ta>
            <ta e="T28" id="Seg_1599" s="T27">nprop</ta>
            <ta e="T29" id="Seg_1600" s="T28">v</ta>
            <ta e="T30" id="Seg_1601" s="T29">pers</ta>
            <ta e="T31" id="Seg_1602" s="T30">v</ta>
            <ta e="T32" id="Seg_1603" s="T31">interrog</ta>
            <ta e="T33" id="Seg_1604" s="T32">n</ta>
            <ta e="T34" id="Seg_1605" s="T33">v</ta>
            <ta e="T35" id="Seg_1606" s="T34">interrog</ta>
            <ta e="T36" id="Seg_1607" s="T35">adv</ta>
            <ta e="T37" id="Seg_1608" s="T36">v</ta>
            <ta e="T38" id="Seg_1609" s="T37">conj</ta>
            <ta e="T39" id="Seg_1610" s="T38">pers</ta>
            <ta e="T40" id="Seg_1611" s="T39">v</ta>
            <ta e="T41" id="Seg_1612" s="T40">nprop</ta>
            <ta e="T42" id="Seg_1613" s="T41">n</ta>
            <ta e="T43" id="Seg_1614" s="T42">n</ta>
            <ta e="T44" id="Seg_1615" s="T43">v</ta>
            <ta e="T45" id="Seg_1616" s="T44">conj</ta>
            <ta e="T46" id="Seg_1617" s="T45">nprop</ta>
            <ta e="T47" id="Seg_1618" s="T46">interrog</ta>
            <ta e="T48" id="Seg_1619" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_1620" s="T48">v</ta>
            <ta e="T50" id="Seg_1621" s="T49">conj</ta>
            <ta e="T51" id="Seg_1622" s="T50">pers</ta>
            <ta e="T52" id="Seg_1623" s="T51">v</ta>
            <ta e="T53" id="Seg_1624" s="T52">n</ta>
            <ta e="T54" id="Seg_1625" s="T53">v</ta>
            <ta e="T55" id="Seg_1626" s="T54">conj</ta>
            <ta e="T56" id="Seg_1627" s="T55">pers</ta>
            <ta e="T57" id="Seg_1628" s="T56">v</ta>
            <ta e="T58" id="Seg_1629" s="T57">pers</ta>
            <ta e="T59" id="Seg_1630" s="T58">adv</ta>
            <ta e="T60" id="Seg_1631" s="T59">v</ta>
            <ta e="T61" id="Seg_1632" s="T60">nprop</ta>
            <ta e="T62" id="Seg_1633" s="T61">n</ta>
            <ta e="T63" id="Seg_1634" s="T62">n</ta>
            <ta e="T64" id="Seg_1635" s="T63">v</ta>
            <ta e="T65" id="Seg_1636" s="T64">conj</ta>
            <ta e="T66" id="Seg_1637" s="T65">v</ta>
            <ta e="T67" id="Seg_1638" s="T66">adv</ta>
            <ta e="T68" id="Seg_1639" s="T67">v</ta>
            <ta e="T69" id="Seg_1640" s="T68">conj</ta>
            <ta e="T70" id="Seg_1641" s="T69">pers</ta>
            <ta e="T71" id="Seg_1642" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_1643" s="T71">v</ta>
            <ta e="T73" id="Seg_1644" s="T72">n</ta>
            <ta e="T74" id="Seg_1645" s="T73">v</ta>
            <ta e="T75" id="Seg_1646" s="T74">conj</ta>
            <ta e="T76" id="Seg_1647" s="T75">n</ta>
            <ta e="T77" id="Seg_1648" s="T76">nprop</ta>
            <ta e="T78" id="Seg_1649" s="T77">v</ta>
            <ta e="T79" id="Seg_1650" s="T78">interrog</ta>
            <ta e="T80" id="Seg_1651" s="T79">n</ta>
            <ta e="T81" id="Seg_1652" s="T80">v</ta>
            <ta e="T82" id="Seg_1653" s="T81">pers</ta>
            <ta e="T83" id="Seg_1654" s="T82">nprop</ta>
            <ta e="T84" id="Seg_1655" s="T83">v</ta>
            <ta e="T85" id="Seg_1656" s="T84">n</ta>
            <ta e="T86" id="Seg_1657" s="T85">n</ta>
            <ta e="T87" id="Seg_1658" s="T86">conj</ta>
            <ta e="T88" id="Seg_1659" s="T87">pers</ta>
            <ta e="T89" id="Seg_1660" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_1661" s="T89">v</ta>
            <ta e="T91" id="Seg_1662" s="T90">nprop</ta>
            <ta e="T92" id="Seg_1663" s="T91">nprop</ta>
            <ta e="T93" id="Seg_1664" s="T92">v</ta>
            <ta e="T94" id="Seg_1665" s="T93">n</ta>
            <ta e="T95" id="Seg_1666" s="T94">conj</ta>
            <ta e="T96" id="Seg_1667" s="T95">adv</ta>
            <ta e="T97" id="Seg_1668" s="T96">n</ta>
            <ta e="T98" id="Seg_1669" s="T97">pers</ta>
            <ta e="T99" id="Seg_1670" s="T98">v</ta>
            <ta e="T100" id="Seg_1671" s="T99">conj</ta>
            <ta e="T101" id="Seg_1672" s="T100">pers</ta>
            <ta e="T102" id="Seg_1673" s="T101">v</ta>
            <ta e="T103" id="Seg_1674" s="T102">nprop</ta>
            <ta e="T104" id="Seg_1675" s="T103">conj</ta>
            <ta e="T105" id="Seg_1676" s="T104">n</ta>
            <ta e="T106" id="Seg_1677" s="T105">nprop</ta>
            <ta e="T107" id="Seg_1678" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_1679" s="T107">v</ta>
            <ta e="T109" id="Seg_1680" s="T108">nprop</ta>
            <ta e="T110" id="Seg_1681" s="T109">v</ta>
            <ta e="T111" id="Seg_1682" s="T110">ptcl</ta>
            <ta e="T112" id="Seg_1683" s="T111">v</ta>
            <ta e="T113" id="Seg_1684" s="T112">conj</ta>
            <ta e="T114" id="Seg_1685" s="T113">nprop</ta>
            <ta e="T115" id="Seg_1686" s="T114">v</ta>
            <ta e="T116" id="Seg_1687" s="T115">v</ta>
            <ta e="T117" id="Seg_1688" s="T116">n</ta>
            <ta e="T118" id="Seg_1689" s="T117">nprop</ta>
            <ta e="T119" id="Seg_1690" s="T118">pers</ta>
            <ta e="T120" id="Seg_1691" s="T119">n</ta>
            <ta e="T121" id="Seg_1692" s="T120">n</ta>
            <ta e="T122" id="Seg_1693" s="T121">v</ta>
            <ta e="T123" id="Seg_1694" s="T122">adv</ta>
            <ta e="T124" id="Seg_1695" s="T123">ptcl</ta>
            <ta e="T125" id="Seg_1696" s="T124">v</ta>
            <ta e="T126" id="Seg_1697" s="T125">dem</ta>
            <ta e="T127" id="Seg_1698" s="T126">n</ta>
            <ta e="T128" id="Seg_1699" s="T127">quant</ta>
            <ta e="T129" id="Seg_1700" s="T128">v</ta>
            <ta e="T130" id="Seg_1701" s="T129">pers</ta>
            <ta e="T131" id="Seg_1702" s="T130">adv</ta>
            <ta e="T132" id="Seg_1703" s="T131">adj</ta>
            <ta e="T133" id="Seg_1704" s="T132">n</ta>
            <ta e="T134" id="Seg_1705" s="T133">v</ta>
            <ta e="T135" id="Seg_1706" s="T134">nprop</ta>
            <ta e="T136" id="Seg_1707" s="T135">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1708" s="T1">pro.h:Poss</ta>
            <ta e="T3" id="Seg_1709" s="T2">np:Th</ta>
            <ta e="T6" id="Seg_1710" s="T5">pro.h:Poss</ta>
            <ta e="T8" id="Seg_1711" s="T7">np.h:Th </ta>
            <ta e="T9" id="Seg_1712" s="T8">np.h:Th</ta>
            <ta e="T11" id="Seg_1713" s="T10">0.3.h:Th</ta>
            <ta e="T12" id="Seg_1714" s="T11">pro.h:A</ta>
            <ta e="T13" id="Seg_1715" s="T12">np:G</ta>
            <ta e="T16" id="Seg_1716" s="T15">np:Th</ta>
            <ta e="T17" id="Seg_1717" s="T16">0.1.h:A</ta>
            <ta e="T18" id="Seg_1718" s="T17">np.h:B</ta>
            <ta e="T20" id="Seg_1719" s="T19">np:Th</ta>
            <ta e="T21" id="Seg_1720" s="T20">0.1.h:A</ta>
            <ta e="T23" id="Seg_1721" s="T22">0.1.h:A 0.3:Th</ta>
            <ta e="T25" id="Seg_1722" s="T24">np:G</ta>
            <ta e="T28" id="Seg_1723" s="T27">np.h:A</ta>
            <ta e="T30" id="Seg_1724" s="T29">pro.h:R</ta>
            <ta e="T31" id="Seg_1725" s="T30">0.3.h:A</ta>
            <ta e="T33" id="Seg_1726" s="T32">np:Th</ta>
            <ta e="T34" id="Seg_1727" s="T33">0.1.h:A</ta>
            <ta e="T37" id="Seg_1728" s="T36">0.2.h:A 0.3:Th</ta>
            <ta e="T39" id="Seg_1729" s="T38">pro.h:A</ta>
            <ta e="T41" id="Seg_1730" s="T40">np.h:B</ta>
            <ta e="T43" id="Seg_1731" s="T42">np:Th</ta>
            <ta e="T44" id="Seg_1732" s="T43">0.1.h:A</ta>
            <ta e="T46" id="Seg_1733" s="T45">np.h:B</ta>
            <ta e="T49" id="Seg_1734" s="T48">0.1.h:A 0.3:Th</ta>
            <ta e="T51" id="Seg_1735" s="T50">pro.h:A</ta>
            <ta e="T53" id="Seg_1736" s="T52">np:Th 0.1.h:Poss</ta>
            <ta e="T56" id="Seg_1737" s="T55">pro.h:A</ta>
            <ta e="T58" id="Seg_1738" s="T57">pro.h:A</ta>
            <ta e="T60" id="Seg_1739" s="T59">0.3:Th</ta>
            <ta e="T61" id="Seg_1740" s="T60">np.h:B</ta>
            <ta e="T63" id="Seg_1741" s="T62">np:Th</ta>
            <ta e="T64" id="Seg_1742" s="T63">0.1.h:A</ta>
            <ta e="T66" id="Seg_1743" s="T65">0.3.h:A</ta>
            <ta e="T68" id="Seg_1744" s="T67">0.3.h:A 0.3:Th</ta>
            <ta e="T70" id="Seg_1745" s="T69">pro.h:G</ta>
            <ta e="T72" id="Seg_1746" s="T71">0.3.h:A</ta>
            <ta e="T73" id="Seg_1747" s="T72">np:G</ta>
            <ta e="T74" id="Seg_1748" s="T73">0.3.h:A</ta>
            <ta e="T77" id="Seg_1749" s="T76">np.h:A</ta>
            <ta e="T80" id="Seg_1750" s="T79">np:Th</ta>
            <ta e="T81" id="Seg_1751" s="T80">0.2.h:A</ta>
            <ta e="T82" id="Seg_1752" s="T81">pro.h:A</ta>
            <ta e="T83" id="Seg_1753" s="T82">np.h:B</ta>
            <ta e="T86" id="Seg_1754" s="T85">np:Th</ta>
            <ta e="T88" id="Seg_1755" s="T87">pro.h:A</ta>
            <ta e="T90" id="Seg_1756" s="T89">0.3:Th</ta>
            <ta e="T91" id="Seg_1757" s="T90">np.h:A</ta>
            <ta e="T92" id="Seg_1758" s="T91">np.h:B</ta>
            <ta e="T94" id="Seg_1759" s="T93">np:Th</ta>
            <ta e="T97" id="Seg_1760" s="T96">np:Th</ta>
            <ta e="T98" id="Seg_1761" s="T97">pro.h:Poss</ta>
            <ta e="T101" id="Seg_1762" s="T100">pro.h:A</ta>
            <ta e="T102" id="Seg_1763" s="T101">0.3:Th</ta>
            <ta e="T103" id="Seg_1764" s="T102">np.h:B</ta>
            <ta e="T106" id="Seg_1765" s="T105">np.h:A</ta>
            <ta e="T108" id="Seg_1766" s="T107">0.1.h:A 0.3:Th</ta>
            <ta e="T109" id="Seg_1767" s="T108">np.h:B</ta>
            <ta e="T110" id="Seg_1768" s="T109">0.1.h:A 0.3:P</ta>
            <ta e="T112" id="Seg_1769" s="T111">0.1.h:A 0.3:Th</ta>
            <ta e="T114" id="Seg_1770" s="T113">np.h:A</ta>
            <ta e="T116" id="Seg_1771" s="T115">0.3.h:A</ta>
            <ta e="T118" id="Seg_1772" s="T117">np.h:A</ta>
            <ta e="T119" id="Seg_1773" s="T118">pro.h:R</ta>
            <ta e="T121" id="Seg_1774" s="T120">np:Th</ta>
            <ta e="T125" id="Seg_1775" s="T124">0.3.h:A 0.3:Th</ta>
            <ta e="T127" id="Seg_1776" s="T126">np:Th</ta>
            <ta e="T129" id="Seg_1777" s="T128">0.3.h:A</ta>
            <ta e="T130" id="Seg_1778" s="T129">pro.h:A</ta>
            <ta e="T131" id="Seg_1779" s="T130">adv:Time</ta>
            <ta e="T133" id="Seg_1780" s="T132">np:Th</ta>
            <ta e="T135" id="Seg_1781" s="T134">np.h:B</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_1782" s="T2">np:S</ta>
            <ta e="T5" id="Seg_1783" s="T4">v:pred</ta>
            <ta e="T8" id="Seg_1784" s="T7">np.h:S</ta>
            <ta e="T9" id="Seg_1785" s="T8">np.h:S</ta>
            <ta e="T10" id="Seg_1786" s="T9">adj:pred</ta>
            <ta e="T11" id="Seg_1787" s="T10">0.3.h:S cop</ta>
            <ta e="T12" id="Seg_1788" s="T11">pro.h:S</ta>
            <ta e="T14" id="Seg_1789" s="T13">v:pred</ta>
            <ta e="T16" id="Seg_1790" s="T15">np:O</ta>
            <ta e="T17" id="Seg_1791" s="T16">0.1.h:S v:pred</ta>
            <ta e="T20" id="Seg_1792" s="T19">np:O</ta>
            <ta e="T21" id="Seg_1793" s="T20">0.1.h:S v:pred</ta>
            <ta e="T22" id="Seg_1794" s="T21">s:temp</ta>
            <ta e="T23" id="Seg_1795" s="T22">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T28" id="Seg_1796" s="T27">np.h:S</ta>
            <ta e="T29" id="Seg_1797" s="T28">v:pred</ta>
            <ta e="T31" id="Seg_1798" s="T30">0.3.h:S v:pred</ta>
            <ta e="T33" id="Seg_1799" s="T32">np:O</ta>
            <ta e="T34" id="Seg_1800" s="T33">0.1.h:S v:pred</ta>
            <ta e="T37" id="Seg_1801" s="T36">0.2.h:S v:pred 0.3:O</ta>
            <ta e="T39" id="Seg_1802" s="T38">pro.h:S</ta>
            <ta e="T40" id="Seg_1803" s="T39">v:pred</ta>
            <ta e="T43" id="Seg_1804" s="T42">np:O</ta>
            <ta e="T44" id="Seg_1805" s="T43">0.1.h:S v:pred</ta>
            <ta e="T49" id="Seg_1806" s="T48">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T51" id="Seg_1807" s="T50">pro.h:S</ta>
            <ta e="T52" id="Seg_1808" s="T51">v:pred</ta>
            <ta e="T53" id="Seg_1809" s="T52">np:S</ta>
            <ta e="T54" id="Seg_1810" s="T53">v:pred</ta>
            <ta e="T56" id="Seg_1811" s="T55">pro.h:S</ta>
            <ta e="T57" id="Seg_1812" s="T56">v:pred</ta>
            <ta e="T58" id="Seg_1813" s="T57">pro.h:S</ta>
            <ta e="T59" id="Seg_1814" s="T58">s:temp</ta>
            <ta e="T60" id="Seg_1815" s="T59">v:pred 0.3:O</ta>
            <ta e="T63" id="Seg_1816" s="T62">np:O</ta>
            <ta e="T64" id="Seg_1817" s="T63">0.1.h:S v:pred</ta>
            <ta e="T66" id="Seg_1818" s="T65">0.3.h:S v:pred</ta>
            <ta e="T67" id="Seg_1819" s="T66">s:temp</ta>
            <ta e="T68" id="Seg_1820" s="T67">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T72" id="Seg_1821" s="T71">0.3.h:S v:pred</ta>
            <ta e="T74" id="Seg_1822" s="T73">0.3.h:S v:pred</ta>
            <ta e="T77" id="Seg_1823" s="T76">np.h:S</ta>
            <ta e="T78" id="Seg_1824" s="T77">v:pred</ta>
            <ta e="T80" id="Seg_1825" s="T79">np:O</ta>
            <ta e="T81" id="Seg_1826" s="T80">0.2.h:S v:pred</ta>
            <ta e="T82" id="Seg_1827" s="T81">pro.h:S</ta>
            <ta e="T84" id="Seg_1828" s="T83">v:pred</ta>
            <ta e="T86" id="Seg_1829" s="T85">np:O</ta>
            <ta e="T88" id="Seg_1830" s="T87">pro.h:S</ta>
            <ta e="T90" id="Seg_1831" s="T89">v:pred 0.3:O</ta>
            <ta e="T91" id="Seg_1832" s="T90">np.h:S</ta>
            <ta e="T93" id="Seg_1833" s="T92">v:pred</ta>
            <ta e="T94" id="Seg_1834" s="T93">np:O</ta>
            <ta e="T97" id="Seg_1835" s="T96">np:S</ta>
            <ta e="T99" id="Seg_1836" s="T98">v:pred</ta>
            <ta e="T101" id="Seg_1837" s="T100">pro.h:S</ta>
            <ta e="T102" id="Seg_1838" s="T101">v:pred 0.3:O</ta>
            <ta e="T106" id="Seg_1839" s="T105">np.h:S</ta>
            <ta e="T108" id="Seg_1840" s="T107">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T110" id="Seg_1841" s="T109">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T112" id="Seg_1842" s="T111">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T114" id="Seg_1843" s="T113">np.h:S</ta>
            <ta e="T115" id="Seg_1844" s="T114">v:pred</ta>
            <ta e="T116" id="Seg_1845" s="T115">0.3.h:S v:pred</ta>
            <ta e="T118" id="Seg_1846" s="T117">np.h:S</ta>
            <ta e="T121" id="Seg_1847" s="T120">np:O</ta>
            <ta e="T122" id="Seg_1848" s="T121">v:pred</ta>
            <ta e="T125" id="Seg_1849" s="T124">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T127" id="Seg_1850" s="T126">np:O</ta>
            <ta e="T129" id="Seg_1851" s="T128">0.3.h:S v:pred</ta>
            <ta e="T130" id="Seg_1852" s="T129">pro.h:S</ta>
            <ta e="T133" id="Seg_1853" s="T132">np:O</ta>
            <ta e="T134" id="Seg_1854" s="T133">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T13" id="Seg_1855" s="T12">RUS:cult</ta>
            <ta e="T15" id="Seg_1856" s="T14">RUS:gram</ta>
            <ta e="T16" id="Seg_1857" s="T15">RUS:cult</ta>
            <ta e="T20" id="Seg_1858" s="T19">RUS:cult</ta>
            <ta e="T24" id="Seg_1859" s="T23">RUS:cult</ta>
            <ta e="T25" id="Seg_1860" s="T24">WNB Noun or pp</ta>
            <ta e="T26" id="Seg_1861" s="T25">RUS:gram</ta>
            <ta e="T27" id="Seg_1862" s="T26">RUS:cult</ta>
            <ta e="T28" id="Seg_1863" s="T27">RUS:cult</ta>
            <ta e="T33" id="Seg_1864" s="T32">RUS:cult</ta>
            <ta e="T38" id="Seg_1865" s="T37">RUS:gram</ta>
            <ta e="T45" id="Seg_1866" s="T44">RUS:gram</ta>
            <ta e="T50" id="Seg_1867" s="T49">RUS:gram</ta>
            <ta e="T55" id="Seg_1868" s="T54">RUS:gram</ta>
            <ta e="T65" id="Seg_1869" s="T64">RUS:gram</ta>
            <ta e="T69" id="Seg_1870" s="T68">RUS:gram</ta>
            <ta e="T75" id="Seg_1871" s="T74">RUS:gram</ta>
            <ta e="T76" id="Seg_1872" s="T75">RUS:cult</ta>
            <ta e="T80" id="Seg_1873" s="T79">RUS:cult</ta>
            <ta e="T87" id="Seg_1874" s="T86">RUS:gram</ta>
            <ta e="T95" id="Seg_1875" s="T94">RUS:gram</ta>
            <ta e="T96" id="Seg_1876" s="T95">RUS:core</ta>
            <ta e="T100" id="Seg_1877" s="T99">RUS:gram</ta>
            <ta e="T104" id="Seg_1878" s="T103">RUS:gram</ta>
            <ta e="T105" id="Seg_1879" s="T104">RUS:cult</ta>
            <ta e="T113" id="Seg_1880" s="T112">RUS:gram</ta>
            <ta e="T117" id="Seg_1881" s="T116">RUS:cult</ta>
            <ta e="T118" id="Seg_1882" s="T117">RUS:cult</ta>
            <ta e="T123" id="Seg_1883" s="T122">RUS:core</ta>
            <ta e="T127" id="Seg_1884" s="T126">RUS:cult</ta>
            <ta e="T128" id="Seg_1885" s="T127">RUS:core</ta>
            <ta e="T131" id="Seg_1886" s="T130">RUS:disc</ta>
            <ta e="T132" id="Seg_1887" s="T131">RUS:cult</ta>
            <ta e="T133" id="Seg_1888" s="T132">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_1889" s="T1">I had little money.</ta>
            <ta e="T11" id="Seg_1890" s="T5">I had two children, sons, they were small.</ta>
            <ta e="T17" id="Seg_1891" s="T11">I went to the shop and bought some material (=goods).</ta>
            <ta e="T21" id="Seg_1892" s="T17">I bought material for the shirts for Vanja.</ta>
            <ta e="T25" id="Seg_1893" s="T21">I brought it [home] and put it on the table.</ta>
            <ta e="T34" id="Seg_1894" s="T25">And uncle Aleksej came by and asked, what material I bought.</ta>
            <ta e="T37" id="Seg_1895" s="T34">“Why did you buy so little?”</ta>
            <ta e="T44" id="Seg_1896" s="T37">And I said: “I bought [it] for a shirt for Vanja.”</ta>
            <ta e="T49" id="Seg_1897" s="T44">“And why didn't you buy [something] for Kolja?”</ta>
            <ta e="T54" id="Seg_1898" s="T49">And I said: “I don't have money.”</ta>
            <ta e="T64" id="Seg_1899" s="T54">And he said: “I'll go and buy, I'll buy for a shirt for Kolja.”</ta>
            <ta e="T66" id="Seg_1900" s="T64">And he left.</ta>
            <ta e="T72" id="Seg_1901" s="T66">He went and bought [it], but he didn't come to us.</ta>
            <ta e="T74" id="Seg_1902" s="T72">He went home.</ta>
            <ta e="T81" id="Seg_1903" s="T74">And aunt Marina said: “What material did you take?”</ta>
            <ta e="T86" id="Seg_1904" s="T81">“I bought it for a shirt for Kolja.”</ta>
            <ta e="T90" id="Seg_1905" s="T86">“And I Won't give it you.”</ta>
            <ta e="T94" id="Seg_1906" s="T90">“Vera bought [some material] for a shirt for Vanja.</ta>
            <ta e="T99" id="Seg_1907" s="T94">She doesn't have any more money.</ta>
            <ta e="T103" id="Seg_1908" s="T99">And I bought it for Kolja.”</ta>
            <ta e="T108" id="Seg_1909" s="T103">And aunt Marina [said]: “I won't give [it back].</ta>
            <ta e="T112" id="Seg_1910" s="T108">I' ll sew [something] for Panja, I won't give [it back].”</ta>
            <ta e="T116" id="Seg_1911" s="T112">And Kolja was waiting and waiting.</ta>
            <ta e="T122" id="Seg_1912" s="T116">“Uncle Aleksej will bring me [material] for a shirt.”</ta>
            <ta e="T125" id="Seg_1913" s="T122">He never brought it.</ta>
            <ta e="T129" id="Seg_1914" s="T125">This material was sold out.</ta>
            <ta e="T136" id="Seg_1915" s="T129">Afterwards I bought pink material for a shirt for Kolja.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_1916" s="T1">Ich hatte wenig Geld.</ta>
            <ta e="T11" id="Seg_1917" s="T5">Ich hatte zwei Kinder, Söhne, sie waren klein.</ta>
            <ta e="T17" id="Seg_1918" s="T11">Ich ging in den Laden und kaufte Stoff (=Waren).</ta>
            <ta e="T21" id="Seg_1919" s="T17">Ich kaufte Stoff für Hemden für Vanja.</ta>
            <ta e="T25" id="Seg_1920" s="T21">Ich brachte ihn [nach Hause] und legte ihn auf den Tisch.</ta>
            <ta e="T34" id="Seg_1921" s="T25">Und Onkel Aleksej kam vorbei und fragte, was für Stoff ich gekauft hatte.</ta>
            <ta e="T37" id="Seg_1922" s="T34">"Warum hast du so wenig gekauft?"</ta>
            <ta e="T44" id="Seg_1923" s="T37">Und ich sagte: "Ich habe [ihn] für ein Hemd für Vanja gekauft."</ta>
            <ta e="T49" id="Seg_1924" s="T44">"Und warum hast du [nichts] für Kolja gekauft?"</ta>
            <ta e="T54" id="Seg_1925" s="T49">Und ich sagte: "Ich habe kein Geld."</ta>
            <ta e="T64" id="Seg_1926" s="T54">Und er sagte: "Ich werde einkaufen gehen, ich kaufe ein Hemd für Kolja."</ta>
            <ta e="T66" id="Seg_1927" s="T64">Und er ging.</ta>
            <ta e="T72" id="Seg_1928" s="T66">Er ging und kaufte [es], aber er kam nicht zu uns.</ta>
            <ta e="T74" id="Seg_1929" s="T72">Er ging nach Hause.</ta>
            <ta e="T81" id="Seg_1930" s="T74">Und Tante Marina sagte: "Was für Stoff hast du genommen?"</ta>
            <ta e="T86" id="Seg_1931" s="T81">"Ich habe ihn für ein Hemd für Kolja gekauft."</ta>
            <ta e="T90" id="Seg_1932" s="T86">"Und ich gebe ihn dir nicht."</ta>
            <ta e="T94" id="Seg_1933" s="T90">"Vera hat [Stoff] für ein Hemd für Vanja gekauft.</ta>
            <ta e="T99" id="Seg_1934" s="T94">Sie hat nicht mehr Geld.</ta>
            <ta e="T103" id="Seg_1935" s="T99">Und ich habe ihn für Kolja gekauft."</ta>
            <ta e="T108" id="Seg_1936" s="T103">Und Tante Marina [sagte]: "Ich gebe [ihn] nicht [zurück].</ta>
            <ta e="T112" id="Seg_1937" s="T108">Ich will [etwas] für Panja nähen, ich gebe [ihn] nicht [zurück]."</ta>
            <ta e="T116" id="Seg_1938" s="T112">Und Kolja wartete und wartete.</ta>
            <ta e="T122" id="Seg_1939" s="T116">"Onkel Aleksej wird mir [Stoff] für ein Hemd bringen."</ta>
            <ta e="T125" id="Seg_1940" s="T122">Er brachte ihn nie.</ta>
            <ta e="T129" id="Seg_1941" s="T125">Der Stoff war ausverkauft.</ta>
            <ta e="T136" id="Seg_1942" s="T129">Ich habe dann rosafarbenen Stoff für ein Hemd für Kolja gekauft.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_1943" s="T1">У меня денег мало было.</ta>
            <ta e="T11" id="Seg_1944" s="T5">У меня было двое детей, сынишек, они маленькие были.</ta>
            <ta e="T17" id="Seg_1945" s="T11">Я в лавку пошла и товар взяла.</ta>
            <ta e="T21" id="Seg_1946" s="T17">Ване на рубашки товар я взяла.</ta>
            <ta e="T25" id="Seg_1947" s="T21">Принесла и положила на стол.</ta>
            <ta e="T34" id="Seg_1948" s="T25">И дядя Алексей зашел и мне сказал, что за я товар взяла?</ta>
            <ta e="T37" id="Seg_1949" s="T34">“Почему ты мало взяла?”</ta>
            <ta e="T44" id="Seg_1950" s="T37">А я сказала: “Ване на рубашку я взяла”.</ta>
            <ta e="T49" id="Seg_1951" s="T44">“А Коле почему я не взяла?”</ta>
            <ta e="T54" id="Seg_1952" s="T49">А я сказала: “У меня денег нету”.</ta>
            <ta e="T64" id="Seg_1953" s="T54">А он говорит: “Я пойду возьму, Коле на рубашку возьму”.</ta>
            <ta e="T66" id="Seg_1954" s="T64">И ушел.</ta>
            <ta e="T72" id="Seg_1955" s="T66">Пошел взял, а к нам не пришел.</ta>
            <ta e="T74" id="Seg_1956" s="T72">Домой к себе ушел.</ta>
            <ta e="T81" id="Seg_1957" s="T74">А тетка Марина сказала: “Какой товар взял?”</ta>
            <ta e="T86" id="Seg_1958" s="T81">“Я Коле взял на рубашку”.</ta>
            <ta e="T90" id="Seg_1959" s="T86">“А я не дам”.</ta>
            <ta e="T94" id="Seg_1960" s="T90">“Вера Ване взяла на рубашку.</ta>
            <ta e="T99" id="Seg_1961" s="T94">Больше денег у нее нет.</ta>
            <ta e="T103" id="Seg_1962" s="T99">А я купил Коле”.</ta>
            <ta e="T108" id="Seg_1963" s="T103">А тетя Марина: “Не дам.</ta>
            <ta e="T112" id="Seg_1964" s="T108">Паньке сошью, не дам”.</ta>
            <ta e="T116" id="Seg_1965" s="T112">А Коля ждет, ждет.</ta>
            <ta e="T122" id="Seg_1966" s="T116">“Дядя Алексей мне на рубашку принесет”.</ta>
            <ta e="T125" id="Seg_1967" s="T122">Так и не принес.</ta>
            <ta e="T129" id="Seg_1968" s="T125">Этот товар весь закончили (разобрали).</ta>
            <ta e="T136" id="Seg_1969" s="T129">Я потом розовый товар купила Коле на рубашку.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_1970" s="T1">у меня денег мало было</ta>
            <ta e="T11" id="Seg_1971" s="T5">у меня было двое детей сынишки маленькие были</ta>
            <ta e="T17" id="Seg_1972" s="T11">я в лавку пошла товар взяла</ta>
            <ta e="T21" id="Seg_1973" s="T17">Ване на рубашки товар взяла</ta>
            <ta e="T25" id="Seg_1974" s="T21">принесла и положила на стол</ta>
            <ta e="T34" id="Seg_1975" s="T25">дядя Алексей пришел мне говорит что за товар взяла</ta>
            <ta e="T37" id="Seg_1976" s="T34">почему мало взяла</ta>
            <ta e="T44" id="Seg_1977" s="T37">я сказала Ване на рубашку взяла</ta>
            <ta e="T49" id="Seg_1978" s="T44">а Коле почему не взяла</ta>
            <ta e="T54" id="Seg_1979" s="T49">денег нету</ta>
            <ta e="T64" id="Seg_1980" s="T54">а он говорит я пойду возьму Коле на рубашку возьму</ta>
            <ta e="T66" id="Seg_1981" s="T64">и ушел</ta>
            <ta e="T72" id="Seg_1982" s="T66">пошел взял к нам не пришел</ta>
            <ta e="T74" id="Seg_1983" s="T72">домой ушел</ta>
            <ta e="T81" id="Seg_1984" s="T74">сказала какой товар взял</ta>
            <ta e="T86" id="Seg_1985" s="T81">я Коле взял (купил)</ta>
            <ta e="T90" id="Seg_1986" s="T86">а я не дам</ta>
            <ta e="T94" id="Seg_1987" s="T90">Вера Ване взяла на рубашку</ta>
            <ta e="T99" id="Seg_1988" s="T94">больше денег у нее нет</ta>
            <ta e="T103" id="Seg_1989" s="T99">а я купила Коле</ta>
            <ta e="T108" id="Seg_1990" s="T103">а тетя Марина не дам</ta>
            <ta e="T112" id="Seg_1991" s="T108">Пань(ш)ке (сошью?) не дам</ta>
            <ta e="T116" id="Seg_1992" s="T112">а Коля ждет ждет</ta>
            <ta e="T122" id="Seg_1993" s="T116">дядя Алексей мне на рубашку принесет</ta>
            <ta e="T125" id="Seg_1994" s="T122">так и не принес</ta>
            <ta e="T129" id="Seg_1995" s="T125">этот товар весь кончили</ta>
            <ta e="T136" id="Seg_1996" s="T129">я потом розовый товар купила Коле на рубашку</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T21" id="Seg_1997" s="T17">[KuAI:] Variant: 'qaborɣɨtɨ'. [BrM:] POSS 3SG or ILL?</ta>
            <ta e="T34" id="Seg_1998" s="T25">[BrM:] 1SG.O or 2SG?</ta>
            <ta e="T44" id="Seg_1999" s="T37">[BrM:] POSS 3SG or ILL?</ta>
            <ta e="T49" id="Seg_2000" s="T44"> ‎[BrM:] 1SG.O or 2SG?</ta>
            <ta e="T64" id="Seg_2001" s="T54">[KuAI:] Variant: 'iːtʒau'.</ta>
            <ta e="T86" id="Seg_2002" s="T81">[KuAI:] Variant: 'iːzaw'. [BrM:] POSS 3SG or ILL or?</ta>
            <ta e="T94" id="Seg_2003" s="T90">[BrM:] POSS 3SG or ILL or?</ta>
            <ta e="T116" id="Seg_2004" s="T112">[BrM:] TR?</ta>
            <ta e="T122" id="Seg_2005" s="T116">[BrM:] POSS 3SG or ILL or? FUT-INFER?</ta>
            <ta e="T136" id="Seg_2006" s="T129">[KuAI:] Variant: 'qaborgadɨmɨm'. [BrM:] POSS 3SG or ILL or?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
