<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_ConfusionInTheBoat_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_ConfusionInTheBoat_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">97</ud-information>
            <ud-information attribute-name="# HIAT:w">75</ud-information>
            <ud-information attribute-name="# e">75</ud-information>
            <ud-information attribute-name="# HIAT:u">18</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T76" id="Seg_0" n="sc" s="T1">
               <ts e="T9" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Wojnaɣɨn</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">tüːs</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">meɣuntu</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">palnamošnɨj</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">qum</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_19" n="HIAT:w" s="T6">i</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_22" n="HIAT:w" s="T7">sabranʼjem</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_25" n="HIAT:w" s="T8">mess</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_29" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">I</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">qulam</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_37" n="HIAT:w" s="T11">qwɛlɨsku</ts>
                  <nts id="Seg_38" n="HIAT:ip">,</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_41" n="HIAT:w" s="T12">qwɛlɨsku</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_44" n="HIAT:w" s="T13">tʼärɨn</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_47" n="HIAT:w" s="T14">nadə</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_51" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">Täp</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_56" n="HIAT:w" s="T16">mʼänan</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_59" n="HIAT:w" s="T17">seːqqɨn</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_63" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">A</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">qarʼemɨɣɨn</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_71" n="HIAT:w" s="T20">dʼäja</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_74" n="HIAT:w" s="T21">Andrej</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_77" n="HIAT:w" s="T22">täbɨm</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_80" n="HIAT:w" s="T23">qwatdɨt</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_83" n="HIAT:w" s="T24">atdokazʼe</ts>
                  <nts id="Seg_84" n="HIAT:ip">.</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_87" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_89" n="HIAT:w" s="T25">Dʼäa</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_92" n="HIAT:w" s="T26">Andrej</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_95" n="HIAT:w" s="T27">tuwa</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_99" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_101" n="HIAT:w" s="T28">A</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_104" n="HIAT:w" s="T29">täp</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_107" n="HIAT:w" s="T30">aːmda</ts>
                  <nts id="Seg_108" n="HIAT:ip">.</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_111" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">A</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_116" n="HIAT:w" s="T32">man</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_119" n="HIAT:w" s="T33">tʼäkgə</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_122" n="HIAT:w" s="T34">poqqəčelʼe</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_125" n="HIAT:w" s="T35">aːmdɨzan</ts>
                  <nts id="Seg_126" n="HIAT:ip">.</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_129" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_131" n="HIAT:w" s="T36">A</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_134" n="HIAT:w" s="T37">täbɨstaɣə</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_137" n="HIAT:w" s="T38">čaʒaɣə</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_141" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_143" n="HIAT:w" s="T39">Dʼäa</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_146" n="HIAT:w" s="T40">Andrej</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_149" n="HIAT:w" s="T41">tuwa</ts>
                  <nts id="Seg_150" n="HIAT:ip">.</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_153" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_155" n="HIAT:w" s="T42">A</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_158" n="HIAT:w" s="T43">täp</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_161" n="HIAT:w" s="T44">aːmda</ts>
                  <nts id="Seg_162" n="HIAT:ip">.</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_165" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_167" n="HIAT:w" s="T45">A</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_170" n="HIAT:w" s="T46">man</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_173" n="HIAT:w" s="T47">tʼäran</ts>
                  <nts id="Seg_174" n="HIAT:ip">:</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_176" n="HIAT:ip">“</nts>
                  <ts e="T49" id="Seg_178" n="HIAT:w" s="T48">Padʼom</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_181" n="HIAT:w" s="T49">qajno</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_184" n="HIAT:w" s="T50">omdɨlǯibal</ts>
                  <nts id="Seg_185" n="HIAT:ip">?</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_188" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_190" n="HIAT:w" s="T51">Nadə</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_193" n="HIAT:w" s="T52">täbnä</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_196" n="HIAT:w" s="T53">labɨm</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_199" n="HIAT:w" s="T54">megu</ts>
                  <nts id="Seg_200" n="HIAT:ip">.</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_203" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_205" n="HIAT:w" s="T55">I</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_208" n="HIAT:w" s="T56">tuːnän</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_211" n="HIAT:w" s="T57">bə</ts>
                  <nts id="Seg_212" n="HIAT:ip">.</nts>
                  <nts id="Seg_213" n="HIAT:ip">”</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_216" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_218" n="HIAT:w" s="T58">A</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_221" n="HIAT:w" s="T59">täp</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_224" n="HIAT:w" s="T60">süsöko</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_227" n="HIAT:w" s="T61">sʼen</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_230" n="HIAT:w" s="T62">kulupbulʼe</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_233" n="HIAT:w" s="T63">jübɨran</ts>
                  <nts id="Seg_234" n="HIAT:ip">.</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_237" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_239" n="HIAT:w" s="T64">Man</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_242" n="HIAT:w" s="T65">kəcʼiwannan</ts>
                  <nts id="Seg_243" n="HIAT:ip">.</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_246" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_248" n="HIAT:w" s="T66">Menan</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_251" n="HIAT:w" s="T67">padʼola</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_254" n="HIAT:w" s="T68">koːcʼin</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_257" n="HIAT:w" s="T69">jezattə</ts>
                  <nts id="Seg_258" n="HIAT:ip">.</nts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_261" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_263" n="HIAT:w" s="T70">Tebla</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_266" n="HIAT:w" s="T71">qulam</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_269" n="HIAT:w" s="T72">qwatkuzattə</ts>
                  <nts id="Seg_270" n="HIAT:ip">.</nts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_273" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_275" n="HIAT:w" s="T73">Patom</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_278" n="HIAT:w" s="T74">mʼäguntu</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_281" n="HIAT:w" s="T75">üdukkuzattə</ts>
                  <nts id="Seg_282" n="HIAT:ip">.</nts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T76" id="Seg_284" n="sc" s="T1">
               <ts e="T2" id="Seg_286" n="e" s="T1">Wojnaɣɨn </ts>
               <ts e="T3" id="Seg_288" n="e" s="T2">tüːs </ts>
               <ts e="T4" id="Seg_290" n="e" s="T3">meɣuntu </ts>
               <ts e="T5" id="Seg_292" n="e" s="T4">palnamošnɨj </ts>
               <ts e="T6" id="Seg_294" n="e" s="T5">qum </ts>
               <ts e="T7" id="Seg_296" n="e" s="T6">i </ts>
               <ts e="T8" id="Seg_298" n="e" s="T7">sabranʼjem </ts>
               <ts e="T9" id="Seg_300" n="e" s="T8">mess. </ts>
               <ts e="T10" id="Seg_302" n="e" s="T9">I </ts>
               <ts e="T11" id="Seg_304" n="e" s="T10">qulam </ts>
               <ts e="T12" id="Seg_306" n="e" s="T11">qwɛlɨsku, </ts>
               <ts e="T13" id="Seg_308" n="e" s="T12">qwɛlɨsku </ts>
               <ts e="T14" id="Seg_310" n="e" s="T13">tʼärɨn </ts>
               <ts e="T15" id="Seg_312" n="e" s="T14">nadə. </ts>
               <ts e="T16" id="Seg_314" n="e" s="T15">Täp </ts>
               <ts e="T17" id="Seg_316" n="e" s="T16">mʼänan </ts>
               <ts e="T18" id="Seg_318" n="e" s="T17">seːqqɨn. </ts>
               <ts e="T19" id="Seg_320" n="e" s="T18">A </ts>
               <ts e="T20" id="Seg_322" n="e" s="T19">qarʼemɨɣɨn </ts>
               <ts e="T21" id="Seg_324" n="e" s="T20">dʼäja </ts>
               <ts e="T22" id="Seg_326" n="e" s="T21">Andrej </ts>
               <ts e="T23" id="Seg_328" n="e" s="T22">täbɨm </ts>
               <ts e="T24" id="Seg_330" n="e" s="T23">qwatdɨt </ts>
               <ts e="T25" id="Seg_332" n="e" s="T24">atdokazʼe. </ts>
               <ts e="T26" id="Seg_334" n="e" s="T25">Dʼäa </ts>
               <ts e="T27" id="Seg_336" n="e" s="T26">Andrej </ts>
               <ts e="T28" id="Seg_338" n="e" s="T27">tuwa. </ts>
               <ts e="T29" id="Seg_340" n="e" s="T28">A </ts>
               <ts e="T30" id="Seg_342" n="e" s="T29">täp </ts>
               <ts e="T31" id="Seg_344" n="e" s="T30">aːmda. </ts>
               <ts e="T32" id="Seg_346" n="e" s="T31">A </ts>
               <ts e="T33" id="Seg_348" n="e" s="T32">man </ts>
               <ts e="T34" id="Seg_350" n="e" s="T33">tʼäkgə </ts>
               <ts e="T35" id="Seg_352" n="e" s="T34">poqqəčelʼe </ts>
               <ts e="T36" id="Seg_354" n="e" s="T35">aːmdɨzan. </ts>
               <ts e="T37" id="Seg_356" n="e" s="T36">A </ts>
               <ts e="T38" id="Seg_358" n="e" s="T37">täbɨstaɣə </ts>
               <ts e="T39" id="Seg_360" n="e" s="T38">čaʒaɣə. </ts>
               <ts e="T40" id="Seg_362" n="e" s="T39">Dʼäa </ts>
               <ts e="T41" id="Seg_364" n="e" s="T40">Andrej </ts>
               <ts e="T42" id="Seg_366" n="e" s="T41">tuwa. </ts>
               <ts e="T43" id="Seg_368" n="e" s="T42">A </ts>
               <ts e="T44" id="Seg_370" n="e" s="T43">täp </ts>
               <ts e="T45" id="Seg_372" n="e" s="T44">aːmda. </ts>
               <ts e="T46" id="Seg_374" n="e" s="T45">A </ts>
               <ts e="T47" id="Seg_376" n="e" s="T46">man </ts>
               <ts e="T48" id="Seg_378" n="e" s="T47">tʼäran: </ts>
               <ts e="T49" id="Seg_380" n="e" s="T48">“Padʼom </ts>
               <ts e="T50" id="Seg_382" n="e" s="T49">qajno </ts>
               <ts e="T51" id="Seg_384" n="e" s="T50">omdɨlǯibal? </ts>
               <ts e="T52" id="Seg_386" n="e" s="T51">Nadə </ts>
               <ts e="T53" id="Seg_388" n="e" s="T52">täbnä </ts>
               <ts e="T54" id="Seg_390" n="e" s="T53">labɨm </ts>
               <ts e="T55" id="Seg_392" n="e" s="T54">megu. </ts>
               <ts e="T56" id="Seg_394" n="e" s="T55">I </ts>
               <ts e="T57" id="Seg_396" n="e" s="T56">tuːnän </ts>
               <ts e="T58" id="Seg_398" n="e" s="T57">bə.” </ts>
               <ts e="T59" id="Seg_400" n="e" s="T58">A </ts>
               <ts e="T60" id="Seg_402" n="e" s="T59">täp </ts>
               <ts e="T61" id="Seg_404" n="e" s="T60">süsöko </ts>
               <ts e="T62" id="Seg_406" n="e" s="T61">sʼen </ts>
               <ts e="T63" id="Seg_408" n="e" s="T62">kulupbulʼe </ts>
               <ts e="T64" id="Seg_410" n="e" s="T63">jübɨran. </ts>
               <ts e="T65" id="Seg_412" n="e" s="T64">Man </ts>
               <ts e="T66" id="Seg_414" n="e" s="T65">kəcʼiwannan. </ts>
               <ts e="T67" id="Seg_416" n="e" s="T66">Menan </ts>
               <ts e="T68" id="Seg_418" n="e" s="T67">padʼola </ts>
               <ts e="T69" id="Seg_420" n="e" s="T68">koːcʼin </ts>
               <ts e="T70" id="Seg_422" n="e" s="T69">jezattə. </ts>
               <ts e="T71" id="Seg_424" n="e" s="T70">Tebla </ts>
               <ts e="T72" id="Seg_426" n="e" s="T71">qulam </ts>
               <ts e="T73" id="Seg_428" n="e" s="T72">qwatkuzattə. </ts>
               <ts e="T74" id="Seg_430" n="e" s="T73">Patom </ts>
               <ts e="T75" id="Seg_432" n="e" s="T74">mʼäguntu </ts>
               <ts e="T76" id="Seg_434" n="e" s="T75">üdukkuzattə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T9" id="Seg_435" s="T1">PVD_1964_ConfusionInTheBoat_nar.001 (001.001)</ta>
            <ta e="T15" id="Seg_436" s="T9">PVD_1964_ConfusionInTheBoat_nar.002 (001.002)</ta>
            <ta e="T18" id="Seg_437" s="T15">PVD_1964_ConfusionInTheBoat_nar.003 (001.003)</ta>
            <ta e="T25" id="Seg_438" s="T18">PVD_1964_ConfusionInTheBoat_nar.004 (001.004)</ta>
            <ta e="T28" id="Seg_439" s="T25">PVD_1964_ConfusionInTheBoat_nar.005 (001.005)</ta>
            <ta e="T31" id="Seg_440" s="T28">PVD_1964_ConfusionInTheBoat_nar.006 (001.006)</ta>
            <ta e="T36" id="Seg_441" s="T31">PVD_1964_ConfusionInTheBoat_nar.007 (001.007)</ta>
            <ta e="T39" id="Seg_442" s="T36">PVD_1964_ConfusionInTheBoat_nar.008 (001.008)</ta>
            <ta e="T42" id="Seg_443" s="T39">PVD_1964_ConfusionInTheBoat_nar.009 (001.009)</ta>
            <ta e="T45" id="Seg_444" s="T42">PVD_1964_ConfusionInTheBoat_nar.010 (001.010)</ta>
            <ta e="T51" id="Seg_445" s="T45">PVD_1964_ConfusionInTheBoat_nar.011 (001.011)</ta>
            <ta e="T55" id="Seg_446" s="T51">PVD_1964_ConfusionInTheBoat_nar.012 (001.012)</ta>
            <ta e="T58" id="Seg_447" s="T55">PVD_1964_ConfusionInTheBoat_nar.013 (001.013)</ta>
            <ta e="T64" id="Seg_448" s="T58">PVD_1964_ConfusionInTheBoat_nar.014 (001.014)</ta>
            <ta e="T66" id="Seg_449" s="T64">PVD_1964_ConfusionInTheBoat_nar.015 (001.015)</ta>
            <ta e="T70" id="Seg_450" s="T66">PVD_1964_ConfusionInTheBoat_nar.016 (001.016)</ta>
            <ta e="T73" id="Seg_451" s="T70">PVD_1964_ConfusionInTheBoat_nar.017 (001.017)</ta>
            <ta e="T76" id="Seg_452" s="T73">PVD_1964_ConfusionInTheBoat_nar.018 (001.018)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T9" id="Seg_453" s="T1">вой′наɣын тӱ̄с ′меɣунту палнамошный kум и саб′ранʼjем ′месс.</ta>
            <ta e="T15" id="Seg_454" s="T9">и kу′lам ′kwɛлыску, kwɛлыску тʼӓ′рын надъ.</ta>
            <ta e="T18" id="Seg_455" s="T15">тӓп мʼӓ′нан ′се̄kkын.</ta>
            <ta e="T25" id="Seg_456" s="T18">а kа′рʼемыɣын ′дʼӓjа Андрей ′тӓбым ′kwатдыт ат′доказʼе.</ta>
            <ta e="T28" id="Seg_457" s="T25">д̂ʼӓа Андрей ′тува.</ta>
            <ta e="T31" id="Seg_458" s="T28">а тӓп ′а̄мда.</ta>
            <ta e="T36" id="Seg_459" s="T31">а ман тʼӓкгъ ‵поkkътше′лʼе ′а̄мдызан.</ta>
            <ta e="T39" id="Seg_460" s="T36">а тӓбыс′таɣъ тшажаɣъ.</ta>
            <ta e="T42" id="Seg_461" s="T39">′дʼӓа Анд′рей ′тува.</ta>
            <ta e="T45" id="Seg_462" s="T42">а тӓп ′а̄мда.</ta>
            <ta e="T51" id="Seg_463" s="T45">а ман тʼӓ′ран: па′дʼом kайно ′омдыlджиб̂ал.</ta>
            <ta e="T55" id="Seg_464" s="T51">надъ тӓбнӓ ′лабым ′мегу.</ta>
            <ta e="T58" id="Seg_465" s="T55">и ′тӯнӓн бъ.</ta>
            <ta e="T64" id="Seg_466" s="T58">а тӓп сӱ′сӧкосʼен ′куlупбулʼе ′jӱбыран.</ta>
            <ta e="T66" id="Seg_467" s="T64">ман къцʼи′ваннан.</ta>
            <ta e="T70" id="Seg_468" s="T66">ме′нан па′дʼола ′ко̄цʼин ′jезаттъ.</ta>
            <ta e="T73" id="Seg_469" s="T70">теб′ла kу′lам ′kwатку′заттъ.</ta>
            <ta e="T76" id="Seg_470" s="T73">патом мʼӓ′гунту ′ӱдукку‵заттъ.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T9" id="Seg_471" s="T1">wojnaɣɨn tüːs meɣuntu palnamošnɨj qum i sabranʼjem mess.</ta>
            <ta e="T15" id="Seg_472" s="T9">i qulam qwɛlɨsku, qwɛlɨsku tʼärɨn nadə.</ta>
            <ta e="T18" id="Seg_473" s="T15">täp mʼänan seːqqɨn.</ta>
            <ta e="T25" id="Seg_474" s="T18">a qarʼemɨɣɨn dʼäja Аndrej täbɨm qwatdɨt atdokazʼe.</ta>
            <ta e="T28" id="Seg_475" s="T25">d̂ʼäa Аndrej tuwa.</ta>
            <ta e="T31" id="Seg_476" s="T28">a täp aːmda.</ta>
            <ta e="T36" id="Seg_477" s="T31">a man tʼäkgə poqqətšelʼe aːmdɨzan.</ta>
            <ta e="T39" id="Seg_478" s="T36">a täbɨstaɣə tšaʒaɣə.</ta>
            <ta e="T42" id="Seg_479" s="T39">dʼäa Аndrej tuwa.</ta>
            <ta e="T45" id="Seg_480" s="T42">a täp aːmda.</ta>
            <ta e="T51" id="Seg_481" s="T45">a man tʼäran: padʼom qajno omdɨlǯib̂al.</ta>
            <ta e="T55" id="Seg_482" s="T51">nadə täbnä labɨm megu.</ta>
            <ta e="T58" id="Seg_483" s="T55">i tuːnän bə.</ta>
            <ta e="T64" id="Seg_484" s="T58">a täp süsökosʼen kulupbulʼe jübɨran.</ta>
            <ta e="T66" id="Seg_485" s="T64">man kəcʼiwannan.</ta>
            <ta e="T70" id="Seg_486" s="T66">menan padʼola koːcʼin jezattə.</ta>
            <ta e="T73" id="Seg_487" s="T70">tebla qulam qwatkuzattə.</ta>
            <ta e="T76" id="Seg_488" s="T73">patom mʼäguntu üdukkuzattə.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T9" id="Seg_489" s="T1">Wojnaɣɨn tüːs meɣuntu palnamošnɨj qum i sabranʼjem mess. </ta>
            <ta e="T15" id="Seg_490" s="T9">I qulam qwɛlɨsku, qwɛlɨsku tʼärɨn nadə. </ta>
            <ta e="T18" id="Seg_491" s="T15">Täp mʼänan seːqqɨn. </ta>
            <ta e="T25" id="Seg_492" s="T18">A qarʼemɨɣɨn dʼäja Andrej täbɨm qwatdɨt atdokazʼe. </ta>
            <ta e="T28" id="Seg_493" s="T25">Dʼäa Andrej tuwa. </ta>
            <ta e="T31" id="Seg_494" s="T28">A täp aːmda. </ta>
            <ta e="T36" id="Seg_495" s="T31">A man tʼäkgə poqqəčelʼe aːmdɨzan. </ta>
            <ta e="T39" id="Seg_496" s="T36">A täbɨstaɣə čaʒaɣə. </ta>
            <ta e="T42" id="Seg_497" s="T39">Dʼäa Andrej tuwa. </ta>
            <ta e="T45" id="Seg_498" s="T42">A täp aːmda. </ta>
            <ta e="T51" id="Seg_499" s="T45">A man tʼäran: “Padʼom qajno omdɨlǯibal? </ta>
            <ta e="T55" id="Seg_500" s="T51">Nadə täbnä labɨm megu. </ta>
            <ta e="T58" id="Seg_501" s="T55">I tuːnän bə.” </ta>
            <ta e="T64" id="Seg_502" s="T58">A täp süsöko sʼen kulupbulʼe jübɨran. </ta>
            <ta e="T66" id="Seg_503" s="T64">Man kəcʼiwannan. </ta>
            <ta e="T70" id="Seg_504" s="T66">Menan padʼola koːcʼin jezattə. </ta>
            <ta e="T73" id="Seg_505" s="T70">Tebla qulam qwatkuzattə. </ta>
            <ta e="T76" id="Seg_506" s="T73">Patom mʼäguntu üdukkuzattə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_507" s="T1">wojna-ɣɨn</ta>
            <ta e="T3" id="Seg_508" s="T2">tüː-s</ta>
            <ta e="T4" id="Seg_509" s="T3">me-ɣuntu</ta>
            <ta e="T5" id="Seg_510" s="T4">palnamošnɨj</ta>
            <ta e="T6" id="Seg_511" s="T5">qum</ta>
            <ta e="T7" id="Seg_512" s="T6">i</ta>
            <ta e="T8" id="Seg_513" s="T7">sabranʼje-m</ta>
            <ta e="T9" id="Seg_514" s="T8">me-ss</ta>
            <ta e="T10" id="Seg_515" s="T9">i</ta>
            <ta e="T11" id="Seg_516" s="T10">qu-la-m</ta>
            <ta e="T12" id="Seg_517" s="T11">qwɛl-ɨ-s-ku</ta>
            <ta e="T13" id="Seg_518" s="T12">qwɛl-ɨ-s-ku</ta>
            <ta e="T14" id="Seg_519" s="T13">tʼärɨ-n</ta>
            <ta e="T15" id="Seg_520" s="T14">nadə</ta>
            <ta e="T16" id="Seg_521" s="T15">täp</ta>
            <ta e="T17" id="Seg_522" s="T16">mʼä-nan</ta>
            <ta e="T18" id="Seg_523" s="T17">seːqqɨ-n</ta>
            <ta e="T19" id="Seg_524" s="T18">a</ta>
            <ta e="T20" id="Seg_525" s="T19">qarʼe-mɨ-ɣɨn</ta>
            <ta e="T21" id="Seg_526" s="T20">dʼäja</ta>
            <ta e="T22" id="Seg_527" s="T21">Andrej</ta>
            <ta e="T23" id="Seg_528" s="T22">täb-ɨ-m</ta>
            <ta e="T24" id="Seg_529" s="T23">qwat-dɨ-t</ta>
            <ta e="T25" id="Seg_530" s="T24">atdo-ka-zʼe</ta>
            <ta e="T26" id="Seg_531" s="T25">dʼäa</ta>
            <ta e="T27" id="Seg_532" s="T26">Andrej</ta>
            <ta e="T28" id="Seg_533" s="T27">tu-wa</ta>
            <ta e="T29" id="Seg_534" s="T28">a</ta>
            <ta e="T30" id="Seg_535" s="T29">täp</ta>
            <ta e="T31" id="Seg_536" s="T30">aːmda</ta>
            <ta e="T32" id="Seg_537" s="T31">a</ta>
            <ta e="T33" id="Seg_538" s="T32">man</ta>
            <ta e="T34" id="Seg_539" s="T33">tʼäkgə</ta>
            <ta e="T35" id="Seg_540" s="T34">poqqə-če-lʼe</ta>
            <ta e="T36" id="Seg_541" s="T35">aːmdɨ-za-n</ta>
            <ta e="T37" id="Seg_542" s="T36">a</ta>
            <ta e="T38" id="Seg_543" s="T37">täb-ɨ-staɣə</ta>
            <ta e="T39" id="Seg_544" s="T38">čaʒa-ɣə</ta>
            <ta e="T40" id="Seg_545" s="T39">dʼäa</ta>
            <ta e="T41" id="Seg_546" s="T40">Andrej</ta>
            <ta e="T42" id="Seg_547" s="T41">tu-wa</ta>
            <ta e="T43" id="Seg_548" s="T42">a</ta>
            <ta e="T44" id="Seg_549" s="T43">täp</ta>
            <ta e="T45" id="Seg_550" s="T44">aːmda</ta>
            <ta e="T46" id="Seg_551" s="T45">a</ta>
            <ta e="T47" id="Seg_552" s="T46">man</ta>
            <ta e="T48" id="Seg_553" s="T47">tʼära-n</ta>
            <ta e="T49" id="Seg_554" s="T48">padʼo-m</ta>
            <ta e="T50" id="Seg_555" s="T49">qaj-no</ta>
            <ta e="T51" id="Seg_556" s="T50">omdɨ-lǯi-ba-l</ta>
            <ta e="T52" id="Seg_557" s="T51">nadə</ta>
            <ta e="T53" id="Seg_558" s="T52">täb-nä</ta>
            <ta e="T54" id="Seg_559" s="T53">labɨ-m</ta>
            <ta e="T55" id="Seg_560" s="T54">me-gu</ta>
            <ta e="T56" id="Seg_561" s="T55">i</ta>
            <ta e="T57" id="Seg_562" s="T56">tuː-nä-n</ta>
            <ta e="T58" id="Seg_563" s="T57">bə</ta>
            <ta e="T59" id="Seg_564" s="T58">a</ta>
            <ta e="T60" id="Seg_565" s="T59">täp</ta>
            <ta e="T61" id="Seg_566" s="T60">süsöko</ta>
            <ta e="T62" id="Seg_567" s="T61">sʼe-n</ta>
            <ta e="T63" id="Seg_568" s="T62">kulupbu-lʼe</ta>
            <ta e="T64" id="Seg_569" s="T63">jübɨ-ra-n</ta>
            <ta e="T65" id="Seg_570" s="T64">man</ta>
            <ta e="T66" id="Seg_571" s="T65">kəcʼiwan-na-n</ta>
            <ta e="T67" id="Seg_572" s="T66">me-nan</ta>
            <ta e="T68" id="Seg_573" s="T67">padʼo-la</ta>
            <ta e="T69" id="Seg_574" s="T68">koːcʼi-n</ta>
            <ta e="T70" id="Seg_575" s="T69">je-za-ttə</ta>
            <ta e="T71" id="Seg_576" s="T70">teb-la</ta>
            <ta e="T72" id="Seg_577" s="T71">qu-la-m</ta>
            <ta e="T73" id="Seg_578" s="T72">qwat-ku-za-ttə</ta>
            <ta e="T74" id="Seg_579" s="T73">patom</ta>
            <ta e="T75" id="Seg_580" s="T74">mʼä-guntu</ta>
            <ta e="T76" id="Seg_581" s="T75">üdu-kku-za-ttə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_582" s="T1">wajna-qɨn</ta>
            <ta e="T3" id="Seg_583" s="T2">tüː-sɨ</ta>
            <ta e="T4" id="Seg_584" s="T3">me-qɨntɨ</ta>
            <ta e="T5" id="Seg_585" s="T4">palnamošnɨj</ta>
            <ta e="T6" id="Seg_586" s="T5">qum</ta>
            <ta e="T7" id="Seg_587" s="T6">i</ta>
            <ta e="T8" id="Seg_588" s="T7">sabrani-m</ta>
            <ta e="T9" id="Seg_589" s="T8">meː-sɨ</ta>
            <ta e="T10" id="Seg_590" s="T9">i</ta>
            <ta e="T11" id="Seg_591" s="T10">qum-la-m</ta>
            <ta e="T12" id="Seg_592" s="T11">qwɛl-ɨ-s-gu</ta>
            <ta e="T13" id="Seg_593" s="T12">qwɛl-ɨ-s-gu</ta>
            <ta e="T14" id="Seg_594" s="T13">tʼärɨ-n</ta>
            <ta e="T15" id="Seg_595" s="T14">nadə</ta>
            <ta e="T16" id="Seg_596" s="T15">täp</ta>
            <ta e="T17" id="Seg_597" s="T16">me-nan</ta>
            <ta e="T18" id="Seg_598" s="T17">seqqɨ-n</ta>
            <ta e="T19" id="Seg_599" s="T18">a</ta>
            <ta e="T20" id="Seg_600" s="T19">qare-mɨ-qɨn</ta>
            <ta e="T21" id="Seg_601" s="T20">dʼaja</ta>
            <ta e="T22" id="Seg_602" s="T21">Andrej</ta>
            <ta e="T23" id="Seg_603" s="T22">täp-ɨ-m</ta>
            <ta e="T24" id="Seg_604" s="T23">qwan-tɨ-t</ta>
            <ta e="T25" id="Seg_605" s="T24">andǝ-ka-se</ta>
            <ta e="T26" id="Seg_606" s="T25">dʼaja</ta>
            <ta e="T27" id="Seg_607" s="T26">Andrej</ta>
            <ta e="T28" id="Seg_608" s="T27">tuː-nɨ</ta>
            <ta e="T29" id="Seg_609" s="T28">a</ta>
            <ta e="T30" id="Seg_610" s="T29">täp</ta>
            <ta e="T31" id="Seg_611" s="T30">amdɨ</ta>
            <ta e="T32" id="Seg_612" s="T31">a</ta>
            <ta e="T33" id="Seg_613" s="T32">man</ta>
            <ta e="T34" id="Seg_614" s="T33">tʼäkgə</ta>
            <ta e="T35" id="Seg_615" s="T34">poqqɨ-ču-le</ta>
            <ta e="T36" id="Seg_616" s="T35">amdɨ-sɨ-ŋ</ta>
            <ta e="T37" id="Seg_617" s="T36">a</ta>
            <ta e="T38" id="Seg_618" s="T37">täp-ɨ-staɣɨ</ta>
            <ta e="T39" id="Seg_619" s="T38">čaǯɨ-qij</ta>
            <ta e="T40" id="Seg_620" s="T39">dʼaja</ta>
            <ta e="T41" id="Seg_621" s="T40">Andrej</ta>
            <ta e="T42" id="Seg_622" s="T41">tuː-nɨ</ta>
            <ta e="T43" id="Seg_623" s="T42">a</ta>
            <ta e="T44" id="Seg_624" s="T43">täp</ta>
            <ta e="T45" id="Seg_625" s="T44">amdɨ</ta>
            <ta e="T46" id="Seg_626" s="T45">a</ta>
            <ta e="T47" id="Seg_627" s="T46">man</ta>
            <ta e="T48" id="Seg_628" s="T47">tʼärɨ-ŋ</ta>
            <ta e="T49" id="Seg_629" s="T48">padʼo-m</ta>
            <ta e="T50" id="Seg_630" s="T49">qaj-no</ta>
            <ta e="T51" id="Seg_631" s="T50">omdɨ-lǯi-mbɨ-l</ta>
            <ta e="T52" id="Seg_632" s="T51">nadə</ta>
            <ta e="T53" id="Seg_633" s="T52">täp-nä</ta>
            <ta e="T54" id="Seg_634" s="T53">labo-m</ta>
            <ta e="T55" id="Seg_635" s="T54">me-gu</ta>
            <ta e="T56" id="Seg_636" s="T55">i</ta>
            <ta e="T57" id="Seg_637" s="T56">tuː-ne-n</ta>
            <ta e="T58" id="Seg_638" s="T57">bɨ</ta>
            <ta e="T59" id="Seg_639" s="T58">a</ta>
            <ta e="T60" id="Seg_640" s="T59">täp</ta>
            <ta e="T61" id="Seg_641" s="T60">süsögum</ta>
            <ta e="T62" id="Seg_642" s="T61">se-ŋ</ta>
            <ta e="T63" id="Seg_643" s="T62">kulubu-le</ta>
            <ta e="T64" id="Seg_644" s="T63">übɨ-rɨ-n</ta>
            <ta e="T65" id="Seg_645" s="T64">man</ta>
            <ta e="T66" id="Seg_646" s="T65">kɨcʼwat-nɨ-ŋ</ta>
            <ta e="T67" id="Seg_647" s="T66">me-nan</ta>
            <ta e="T68" id="Seg_648" s="T67">padʼo-la</ta>
            <ta e="T69" id="Seg_649" s="T68">koːci-ŋ</ta>
            <ta e="T70" id="Seg_650" s="T69">eː-sɨ-tɨn</ta>
            <ta e="T71" id="Seg_651" s="T70">täp-la</ta>
            <ta e="T72" id="Seg_652" s="T71">qum-la-m</ta>
            <ta e="T73" id="Seg_653" s="T72">qwat-ku-sɨ-tɨn</ta>
            <ta e="T74" id="Seg_654" s="T73">patom</ta>
            <ta e="T75" id="Seg_655" s="T74">me-qɨntɨ</ta>
            <ta e="T76" id="Seg_656" s="T75">üdə-ku-sɨ-tɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_657" s="T1">war-LOC</ta>
            <ta e="T3" id="Seg_658" s="T2">come-PST.[3SG.S]</ta>
            <ta e="T4" id="Seg_659" s="T3">we-ILL.3SG</ta>
            <ta e="T5" id="Seg_660" s="T4">plenipotentiary</ta>
            <ta e="T6" id="Seg_661" s="T5">human.being.[NOM]</ta>
            <ta e="T7" id="Seg_662" s="T6">and</ta>
            <ta e="T8" id="Seg_663" s="T7">meeting-ACC</ta>
            <ta e="T9" id="Seg_664" s="T8">do-PST.[3SG.S]</ta>
            <ta e="T10" id="Seg_665" s="T9">and</ta>
            <ta e="T11" id="Seg_666" s="T10">human.being-PL-ACC</ta>
            <ta e="T12" id="Seg_667" s="T11">fish-EP-CAP-INF</ta>
            <ta e="T13" id="Seg_668" s="T12">fish-EP-CAP-INF</ta>
            <ta e="T14" id="Seg_669" s="T13">say-3SG.S</ta>
            <ta e="T15" id="Seg_670" s="T14">one.should</ta>
            <ta e="T16" id="Seg_671" s="T15">(s)he.[NOM]</ta>
            <ta e="T17" id="Seg_672" s="T16">we-ADES</ta>
            <ta e="T18" id="Seg_673" s="T17">overnight-3SG.S</ta>
            <ta e="T19" id="Seg_674" s="T18">and</ta>
            <ta e="T20" id="Seg_675" s="T19">morning-something-LOC</ta>
            <ta e="T21" id="Seg_676" s="T20">uncle.[NOM]</ta>
            <ta e="T22" id="Seg_677" s="T21">Andrey.[NOM]</ta>
            <ta e="T23" id="Seg_678" s="T22">(s)he-EP-ACC</ta>
            <ta e="T24" id="Seg_679" s="T23">leave-TR-3SG.O</ta>
            <ta e="T25" id="Seg_680" s="T24">boat-DIM-INSTR</ta>
            <ta e="T26" id="Seg_681" s="T25">uncle.[NOM]</ta>
            <ta e="T27" id="Seg_682" s="T26">Andrey.[NOM]</ta>
            <ta e="T28" id="Seg_683" s="T27">row-CO.[3SG.S]</ta>
            <ta e="T29" id="Seg_684" s="T28">and</ta>
            <ta e="T30" id="Seg_685" s="T29">(s)he.[NOM]</ta>
            <ta e="T31" id="Seg_686" s="T30">sit.[3SG.S]</ta>
            <ta e="T32" id="Seg_687" s="T31">and</ta>
            <ta e="T33" id="Seg_688" s="T32">I.NOM</ta>
            <ta e="T34" id="Seg_689" s="T33">%%</ta>
            <ta e="T35" id="Seg_690" s="T34">net-VBLZ-CVB</ta>
            <ta e="T36" id="Seg_691" s="T35">sit-PST-1SG.S</ta>
            <ta e="T37" id="Seg_692" s="T36">and</ta>
            <ta e="T38" id="Seg_693" s="T37">(s)he-EP-DU.[NOM]</ta>
            <ta e="T39" id="Seg_694" s="T38">go-3DU.S</ta>
            <ta e="T40" id="Seg_695" s="T39">uncle.[NOM]</ta>
            <ta e="T41" id="Seg_696" s="T40">Andrey.[NOM]</ta>
            <ta e="T42" id="Seg_697" s="T41">row-CO.[3SG.S]</ta>
            <ta e="T43" id="Seg_698" s="T42">and</ta>
            <ta e="T44" id="Seg_699" s="T43">(s)he.[NOM]</ta>
            <ta e="T45" id="Seg_700" s="T44">sit.[3SG.S]</ta>
            <ta e="T46" id="Seg_701" s="T45">and</ta>
            <ta e="T47" id="Seg_702" s="T46">I.NOM</ta>
            <ta e="T48" id="Seg_703" s="T47">say-1SG.S</ta>
            <ta e="T49" id="Seg_704" s="T48">settler-ACC</ta>
            <ta e="T50" id="Seg_705" s="T49">what-TRL</ta>
            <ta e="T51" id="Seg_706" s="T50">sit.down-TR-PST.NAR-2SG.O</ta>
            <ta e="T52" id="Seg_707" s="T51">one.should</ta>
            <ta e="T53" id="Seg_708" s="T52">(s)he-ALL</ta>
            <ta e="T54" id="Seg_709" s="T53">oar-ACC</ta>
            <ta e="T55" id="Seg_710" s="T54">give-INF</ta>
            <ta e="T56" id="Seg_711" s="T55">and</ta>
            <ta e="T57" id="Seg_712" s="T56">row-CONJ-3SG.S</ta>
            <ta e="T58" id="Seg_713" s="T57">IRREAL</ta>
            <ta e="T59" id="Seg_714" s="T58">and</ta>
            <ta e="T60" id="Seg_715" s="T59">(s)he.[NOM]</ta>
            <ta e="T61" id="Seg_716" s="T60">Selkup.[NOM]</ta>
            <ta e="T62" id="Seg_717" s="T61">language-ADVZ</ta>
            <ta e="T63" id="Seg_718" s="T62">speak-CVB</ta>
            <ta e="T64" id="Seg_719" s="T63">begin-DRV-3SG.S</ta>
            <ta e="T65" id="Seg_720" s="T64">I.NOM</ta>
            <ta e="T66" id="Seg_721" s="T65">get.scared-CO-1SG.S</ta>
            <ta e="T67" id="Seg_722" s="T66">we-ADES</ta>
            <ta e="T68" id="Seg_723" s="T67">settler-PL.[NOM]</ta>
            <ta e="T69" id="Seg_724" s="T68">much-ADVZ</ta>
            <ta e="T70" id="Seg_725" s="T69">be-PST-3PL</ta>
            <ta e="T71" id="Seg_726" s="T70">(s)he-PL.[NOM]</ta>
            <ta e="T72" id="Seg_727" s="T71">human.being-PL-ACC</ta>
            <ta e="T73" id="Seg_728" s="T72">kill-HAB-PST-3PL</ta>
            <ta e="T74" id="Seg_729" s="T73">then</ta>
            <ta e="T75" id="Seg_730" s="T74">we-ILL.3SG</ta>
            <ta e="T76" id="Seg_731" s="T75">send-HAB-PST-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_732" s="T1">война-LOC</ta>
            <ta e="T3" id="Seg_733" s="T2">приехать-PST.[3SG.S]</ta>
            <ta e="T4" id="Seg_734" s="T3">мы-ILL.3SG</ta>
            <ta e="T5" id="Seg_735" s="T4">полномочный</ta>
            <ta e="T6" id="Seg_736" s="T5">человек.[NOM]</ta>
            <ta e="T7" id="Seg_737" s="T6">и</ta>
            <ta e="T8" id="Seg_738" s="T7">собрание-ACC</ta>
            <ta e="T9" id="Seg_739" s="T8">сделать-PST.[3SG.S]</ta>
            <ta e="T10" id="Seg_740" s="T9">и</ta>
            <ta e="T11" id="Seg_741" s="T10">человек-PL-ACC</ta>
            <ta e="T12" id="Seg_742" s="T11">рыба-EP-CAP-INF</ta>
            <ta e="T13" id="Seg_743" s="T12">рыба-EP-CAP-INF</ta>
            <ta e="T14" id="Seg_744" s="T13">сказать-3SG.S</ta>
            <ta e="T15" id="Seg_745" s="T14">надо</ta>
            <ta e="T16" id="Seg_746" s="T15">он(а).[NOM]</ta>
            <ta e="T17" id="Seg_747" s="T16">мы-ADES</ta>
            <ta e="T18" id="Seg_748" s="T17">ночевать-3SG.S</ta>
            <ta e="T19" id="Seg_749" s="T18">а</ta>
            <ta e="T20" id="Seg_750" s="T19">утро-нечто-LOC</ta>
            <ta e="T21" id="Seg_751" s="T20">дядя.[NOM]</ta>
            <ta e="T22" id="Seg_752" s="T21">Андрей.[NOM]</ta>
            <ta e="T23" id="Seg_753" s="T22">он(а)-EP-ACC</ta>
            <ta e="T24" id="Seg_754" s="T23">отправиться-TR-3SG.O</ta>
            <ta e="T25" id="Seg_755" s="T24">обласок-DIM-INSTR</ta>
            <ta e="T26" id="Seg_756" s="T25">дядя.[NOM]</ta>
            <ta e="T27" id="Seg_757" s="T26">Андрей.[NOM]</ta>
            <ta e="T28" id="Seg_758" s="T27">грести-CO.[3SG.S]</ta>
            <ta e="T29" id="Seg_759" s="T28">а</ta>
            <ta e="T30" id="Seg_760" s="T29">он(а).[NOM]</ta>
            <ta e="T31" id="Seg_761" s="T30">сидеть.[3SG.S]</ta>
            <ta e="T32" id="Seg_762" s="T31">а</ta>
            <ta e="T33" id="Seg_763" s="T32">я.NOM</ta>
            <ta e="T34" id="Seg_764" s="T33">%%</ta>
            <ta e="T35" id="Seg_765" s="T34">сеть-VBLZ-CVB</ta>
            <ta e="T36" id="Seg_766" s="T35">сидеть-PST-1SG.S</ta>
            <ta e="T37" id="Seg_767" s="T36">а</ta>
            <ta e="T38" id="Seg_768" s="T37">он(а)-EP-DU.[NOM]</ta>
            <ta e="T39" id="Seg_769" s="T38">ходить-3DU.S</ta>
            <ta e="T40" id="Seg_770" s="T39">дядя.[NOM]</ta>
            <ta e="T41" id="Seg_771" s="T40">Андрей.[NOM]</ta>
            <ta e="T42" id="Seg_772" s="T41">грести-CO.[3SG.S]</ta>
            <ta e="T43" id="Seg_773" s="T42">а</ta>
            <ta e="T44" id="Seg_774" s="T43">он(а).[NOM]</ta>
            <ta e="T45" id="Seg_775" s="T44">сидеть.[3SG.S]</ta>
            <ta e="T46" id="Seg_776" s="T45">а</ta>
            <ta e="T47" id="Seg_777" s="T46">я.NOM</ta>
            <ta e="T48" id="Seg_778" s="T47">сказать-1SG.S</ta>
            <ta e="T49" id="Seg_779" s="T48">поселенец-ACC</ta>
            <ta e="T50" id="Seg_780" s="T49">что-TRL</ta>
            <ta e="T51" id="Seg_781" s="T50">сесть-TR-PST.NAR-2SG.O</ta>
            <ta e="T52" id="Seg_782" s="T51">надо</ta>
            <ta e="T53" id="Seg_783" s="T52">он(а)-ALL</ta>
            <ta e="T54" id="Seg_784" s="T53">весло-ACC</ta>
            <ta e="T55" id="Seg_785" s="T54">дать-INF</ta>
            <ta e="T56" id="Seg_786" s="T55">и</ta>
            <ta e="T57" id="Seg_787" s="T56">грести-CONJ-3SG.S</ta>
            <ta e="T58" id="Seg_788" s="T57">IRREAL</ta>
            <ta e="T59" id="Seg_789" s="T58">а</ta>
            <ta e="T60" id="Seg_790" s="T59">он(а).[NOM]</ta>
            <ta e="T61" id="Seg_791" s="T60">селькуп.[NOM]</ta>
            <ta e="T62" id="Seg_792" s="T61">язык-ADVZ</ta>
            <ta e="T63" id="Seg_793" s="T62">говорить-CVB</ta>
            <ta e="T64" id="Seg_794" s="T63">начать-DRV-3SG.S</ta>
            <ta e="T65" id="Seg_795" s="T64">я.NOM</ta>
            <ta e="T66" id="Seg_796" s="T65">испугаться-CO-1SG.S</ta>
            <ta e="T67" id="Seg_797" s="T66">мы-ADES</ta>
            <ta e="T68" id="Seg_798" s="T67">поселенец-PL.[NOM]</ta>
            <ta e="T69" id="Seg_799" s="T68">много-ADVZ</ta>
            <ta e="T70" id="Seg_800" s="T69">быть-PST-3PL</ta>
            <ta e="T71" id="Seg_801" s="T70">он(а)-PL.[NOM]</ta>
            <ta e="T72" id="Seg_802" s="T71">человек-PL-ACC</ta>
            <ta e="T73" id="Seg_803" s="T72">убить-HAB-PST-3PL</ta>
            <ta e="T74" id="Seg_804" s="T73">потом</ta>
            <ta e="T75" id="Seg_805" s="T74">мы-ILL.3SG</ta>
            <ta e="T76" id="Seg_806" s="T75">посылать-HAB-PST-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_807" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_808" s="T2">v-v:tense.[v:pn]</ta>
            <ta e="T4" id="Seg_809" s="T3">pers-n:case.poss</ta>
            <ta e="T5" id="Seg_810" s="T4">adj</ta>
            <ta e="T6" id="Seg_811" s="T5">n.[n:case]</ta>
            <ta e="T7" id="Seg_812" s="T6">conj</ta>
            <ta e="T8" id="Seg_813" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_814" s="T8">v-v:tense.[v:pn]</ta>
            <ta e="T10" id="Seg_815" s="T9">conj</ta>
            <ta e="T11" id="Seg_816" s="T10">n-n:num-n:case</ta>
            <ta e="T12" id="Seg_817" s="T11">n-n:ins-n&gt;v-v:inf</ta>
            <ta e="T13" id="Seg_818" s="T12">n-n:ins-n&gt;v-v:inf</ta>
            <ta e="T14" id="Seg_819" s="T13">v-v:pn</ta>
            <ta e="T15" id="Seg_820" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_821" s="T15">pers.[n:case]</ta>
            <ta e="T17" id="Seg_822" s="T16">pers-n:case</ta>
            <ta e="T18" id="Seg_823" s="T17">v-v:pn</ta>
            <ta e="T19" id="Seg_824" s="T18">conj</ta>
            <ta e="T20" id="Seg_825" s="T19">n-n-n:case</ta>
            <ta e="T21" id="Seg_826" s="T20">n.[n:case]</ta>
            <ta e="T22" id="Seg_827" s="T21">nprop.[n:case]</ta>
            <ta e="T23" id="Seg_828" s="T22">pers-n:ins-n:case</ta>
            <ta e="T24" id="Seg_829" s="T23">v-v&gt;v-v:pn</ta>
            <ta e="T25" id="Seg_830" s="T24">n-n&gt;n-n:case</ta>
            <ta e="T26" id="Seg_831" s="T25">n.[n:case]</ta>
            <ta e="T27" id="Seg_832" s="T26">nprop.[n:case]</ta>
            <ta e="T28" id="Seg_833" s="T27">v-v:ins.[v:pn]</ta>
            <ta e="T29" id="Seg_834" s="T28">conj</ta>
            <ta e="T30" id="Seg_835" s="T29">pers.[n:case]</ta>
            <ta e="T31" id="Seg_836" s="T30">v.[v:pn]</ta>
            <ta e="T32" id="Seg_837" s="T31">conj</ta>
            <ta e="T33" id="Seg_838" s="T32">pers</ta>
            <ta e="T34" id="Seg_839" s="T33">%%</ta>
            <ta e="T35" id="Seg_840" s="T34">n-n&gt;v-v&gt;adv</ta>
            <ta e="T36" id="Seg_841" s="T35">v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_842" s="T36">conj</ta>
            <ta e="T38" id="Seg_843" s="T37">pers-n:ins-n:num.[n:case]</ta>
            <ta e="T39" id="Seg_844" s="T38">v-v:pn</ta>
            <ta e="T40" id="Seg_845" s="T39">n.[n:case]</ta>
            <ta e="T41" id="Seg_846" s="T40">nprop.[n:case]</ta>
            <ta e="T42" id="Seg_847" s="T41">v-v:ins.[v:pn]</ta>
            <ta e="T43" id="Seg_848" s="T42">conj</ta>
            <ta e="T44" id="Seg_849" s="T43">pers.[n:case]</ta>
            <ta e="T45" id="Seg_850" s="T44">v.[v:pn]</ta>
            <ta e="T46" id="Seg_851" s="T45">conj</ta>
            <ta e="T47" id="Seg_852" s="T46">pers</ta>
            <ta e="T48" id="Seg_853" s="T47">v-v:pn</ta>
            <ta e="T49" id="Seg_854" s="T48">n-n:case</ta>
            <ta e="T50" id="Seg_855" s="T49">interrog-n:case</ta>
            <ta e="T51" id="Seg_856" s="T50">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_857" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_858" s="T52">pers-n:case</ta>
            <ta e="T54" id="Seg_859" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_860" s="T54">v-v:inf</ta>
            <ta e="T56" id="Seg_861" s="T55">conj</ta>
            <ta e="T57" id="Seg_862" s="T56">v-v:mood-v:pn</ta>
            <ta e="T58" id="Seg_863" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_864" s="T58">conj</ta>
            <ta e="T60" id="Seg_865" s="T59">pers.[n:case]</ta>
            <ta e="T61" id="Seg_866" s="T60">n.[n:case]</ta>
            <ta e="T62" id="Seg_867" s="T61">n-n&gt;adv</ta>
            <ta e="T63" id="Seg_868" s="T62">v-v&gt;adv</ta>
            <ta e="T64" id="Seg_869" s="T63">v-v&gt;v-v:pn</ta>
            <ta e="T65" id="Seg_870" s="T64">pers</ta>
            <ta e="T66" id="Seg_871" s="T65">v-v:ins-v:pn</ta>
            <ta e="T67" id="Seg_872" s="T66">pers-n:case</ta>
            <ta e="T68" id="Seg_873" s="T67">n-n:num.[n:case]</ta>
            <ta e="T69" id="Seg_874" s="T68">quant-quant&gt;adv</ta>
            <ta e="T70" id="Seg_875" s="T69">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_876" s="T70">pers-n:num.[n:case]</ta>
            <ta e="T72" id="Seg_877" s="T71">n-n:num-n:case</ta>
            <ta e="T73" id="Seg_878" s="T72">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_879" s="T73">adv</ta>
            <ta e="T75" id="Seg_880" s="T74">pers-n:case.poss</ta>
            <ta e="T76" id="Seg_881" s="T75">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_882" s="T1">n</ta>
            <ta e="T3" id="Seg_883" s="T2">v</ta>
            <ta e="T4" id="Seg_884" s="T3">pers</ta>
            <ta e="T5" id="Seg_885" s="T4">adj</ta>
            <ta e="T6" id="Seg_886" s="T5">n</ta>
            <ta e="T7" id="Seg_887" s="T6">conj</ta>
            <ta e="T8" id="Seg_888" s="T7">n</ta>
            <ta e="T9" id="Seg_889" s="T8">v</ta>
            <ta e="T10" id="Seg_890" s="T9">conj</ta>
            <ta e="T11" id="Seg_891" s="T10">n</ta>
            <ta e="T12" id="Seg_892" s="T11">v</ta>
            <ta e="T13" id="Seg_893" s="T12">v</ta>
            <ta e="T14" id="Seg_894" s="T13">v</ta>
            <ta e="T15" id="Seg_895" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_896" s="T15">pers</ta>
            <ta e="T17" id="Seg_897" s="T16">pers</ta>
            <ta e="T18" id="Seg_898" s="T17">v</ta>
            <ta e="T19" id="Seg_899" s="T18">conj</ta>
            <ta e="T20" id="Seg_900" s="T19">n</ta>
            <ta e="T21" id="Seg_901" s="T20">n</ta>
            <ta e="T22" id="Seg_902" s="T21">nprop</ta>
            <ta e="T23" id="Seg_903" s="T22">pers</ta>
            <ta e="T24" id="Seg_904" s="T23">v</ta>
            <ta e="T25" id="Seg_905" s="T24">n</ta>
            <ta e="T26" id="Seg_906" s="T25">n</ta>
            <ta e="T27" id="Seg_907" s="T26">nprop</ta>
            <ta e="T28" id="Seg_908" s="T27">v</ta>
            <ta e="T29" id="Seg_909" s="T28">conj</ta>
            <ta e="T30" id="Seg_910" s="T29">pers</ta>
            <ta e="T31" id="Seg_911" s="T30">v</ta>
            <ta e="T32" id="Seg_912" s="T31">conj</ta>
            <ta e="T33" id="Seg_913" s="T32">pers</ta>
            <ta e="T35" id="Seg_914" s="T34">adv</ta>
            <ta e="T36" id="Seg_915" s="T35">v</ta>
            <ta e="T37" id="Seg_916" s="T36">conj</ta>
            <ta e="T38" id="Seg_917" s="T37">pers</ta>
            <ta e="T39" id="Seg_918" s="T38">v</ta>
            <ta e="T40" id="Seg_919" s="T39">n</ta>
            <ta e="T41" id="Seg_920" s="T40">nprop</ta>
            <ta e="T42" id="Seg_921" s="T41">v</ta>
            <ta e="T43" id="Seg_922" s="T42">conj</ta>
            <ta e="T44" id="Seg_923" s="T43">pers</ta>
            <ta e="T45" id="Seg_924" s="T44">v</ta>
            <ta e="T46" id="Seg_925" s="T45">conj</ta>
            <ta e="T47" id="Seg_926" s="T46">pers</ta>
            <ta e="T48" id="Seg_927" s="T47">v</ta>
            <ta e="T49" id="Seg_928" s="T48">n</ta>
            <ta e="T50" id="Seg_929" s="T49">interrog</ta>
            <ta e="T51" id="Seg_930" s="T50">v</ta>
            <ta e="T52" id="Seg_931" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_932" s="T52">pers</ta>
            <ta e="T54" id="Seg_933" s="T53">n</ta>
            <ta e="T55" id="Seg_934" s="T54">v</ta>
            <ta e="T56" id="Seg_935" s="T55">conj</ta>
            <ta e="T57" id="Seg_936" s="T56">v</ta>
            <ta e="T58" id="Seg_937" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_938" s="T58">conj</ta>
            <ta e="T60" id="Seg_939" s="T59">pers</ta>
            <ta e="T61" id="Seg_940" s="T60">n</ta>
            <ta e="T62" id="Seg_941" s="T61">n</ta>
            <ta e="T63" id="Seg_942" s="T62">adv</ta>
            <ta e="T64" id="Seg_943" s="T63">v</ta>
            <ta e="T65" id="Seg_944" s="T64">pers</ta>
            <ta e="T66" id="Seg_945" s="T65">v</ta>
            <ta e="T67" id="Seg_946" s="T66">pers</ta>
            <ta e="T68" id="Seg_947" s="T67">n</ta>
            <ta e="T69" id="Seg_948" s="T68">adv</ta>
            <ta e="T70" id="Seg_949" s="T69">v</ta>
            <ta e="T71" id="Seg_950" s="T70">pers</ta>
            <ta e="T72" id="Seg_951" s="T71">n</ta>
            <ta e="T73" id="Seg_952" s="T72">v</ta>
            <ta e="T74" id="Seg_953" s="T73">adv</ta>
            <ta e="T75" id="Seg_954" s="T74">pers</ta>
            <ta e="T76" id="Seg_955" s="T75">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_956" s="T1">np:Time</ta>
            <ta e="T6" id="Seg_957" s="T5">np.h:A</ta>
            <ta e="T8" id="Seg_958" s="T7">np:Th</ta>
            <ta e="T9" id="Seg_959" s="T8">0.3.h:A</ta>
            <ta e="T11" id="Seg_960" s="T10">np.h:Th</ta>
            <ta e="T14" id="Seg_961" s="T13">0.3.h:A</ta>
            <ta e="T16" id="Seg_962" s="T15">pro.h:Th</ta>
            <ta e="T17" id="Seg_963" s="T16">pro.h:L</ta>
            <ta e="T20" id="Seg_964" s="T19">np:Time</ta>
            <ta e="T22" id="Seg_965" s="T21">np.h:A</ta>
            <ta e="T23" id="Seg_966" s="T22">pro.h:Th</ta>
            <ta e="T25" id="Seg_967" s="T24">np:Ins</ta>
            <ta e="T27" id="Seg_968" s="T26">np.h:A</ta>
            <ta e="T30" id="Seg_969" s="T29">pro.h:Th</ta>
            <ta e="T33" id="Seg_970" s="T32">pro.h:Th</ta>
            <ta e="T38" id="Seg_971" s="T37">pro.h:A</ta>
            <ta e="T41" id="Seg_972" s="T40">np.h:A</ta>
            <ta e="T44" id="Seg_973" s="T43">pro.h:Th</ta>
            <ta e="T47" id="Seg_974" s="T46">pro.h:A</ta>
            <ta e="T49" id="Seg_975" s="T48">np.h:Th</ta>
            <ta e="T51" id="Seg_976" s="T50">0.2.h:A</ta>
            <ta e="T53" id="Seg_977" s="T52">pro.h:R</ta>
            <ta e="T54" id="Seg_978" s="T53">np:Th</ta>
            <ta e="T55" id="Seg_979" s="T54">v:Th</ta>
            <ta e="T57" id="Seg_980" s="T56">0.3.h:A</ta>
            <ta e="T60" id="Seg_981" s="T59">pro.h:A</ta>
            <ta e="T65" id="Seg_982" s="T64">pro.h:E</ta>
            <ta e="T67" id="Seg_983" s="T66">pro.h:Poss</ta>
            <ta e="T68" id="Seg_984" s="T67">np.h:Th</ta>
            <ta e="T71" id="Seg_985" s="T70">pro.h:A</ta>
            <ta e="T72" id="Seg_986" s="T71">np.h:P</ta>
            <ta e="T74" id="Seg_987" s="T73">adv:Time</ta>
            <ta e="T75" id="Seg_988" s="T74">pro.h:G</ta>
            <ta e="T76" id="Seg_989" s="T75">0.3.h:A 0.3.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_990" s="T2">v:pred</ta>
            <ta e="T6" id="Seg_991" s="T5">np.h:S</ta>
            <ta e="T8" id="Seg_992" s="T7">np:O</ta>
            <ta e="T9" id="Seg_993" s="T8">0.3.h:S v:pred</ta>
            <ta e="T11" id="Seg_994" s="T10">np.h:O</ta>
            <ta e="T12" id="Seg_995" s="T11">s:purp</ta>
            <ta e="T13" id="Seg_996" s="T12">s:purp</ta>
            <ta e="T14" id="Seg_997" s="T13">0.3.h:S v:pred</ta>
            <ta e="T15" id="Seg_998" s="T14">ptcl:pred</ta>
            <ta e="T16" id="Seg_999" s="T15">pro.h:S</ta>
            <ta e="T18" id="Seg_1000" s="T17">v:pred</ta>
            <ta e="T22" id="Seg_1001" s="T21">np.h:S</ta>
            <ta e="T23" id="Seg_1002" s="T22">pro.h:O</ta>
            <ta e="T24" id="Seg_1003" s="T23">v:pred</ta>
            <ta e="T27" id="Seg_1004" s="T26">np.h:S</ta>
            <ta e="T28" id="Seg_1005" s="T27">v:pred</ta>
            <ta e="T30" id="Seg_1006" s="T29">pro.h:S</ta>
            <ta e="T31" id="Seg_1007" s="T30">v:pred</ta>
            <ta e="T33" id="Seg_1008" s="T32">pro.h:S</ta>
            <ta e="T35" id="Seg_1009" s="T34">s:temp</ta>
            <ta e="T36" id="Seg_1010" s="T35">v:pred</ta>
            <ta e="T38" id="Seg_1011" s="T37">pro.h:S</ta>
            <ta e="T39" id="Seg_1012" s="T38">v:pred</ta>
            <ta e="T41" id="Seg_1013" s="T40">np.h:S</ta>
            <ta e="T42" id="Seg_1014" s="T41">v:pred</ta>
            <ta e="T44" id="Seg_1015" s="T43">pro.h:S</ta>
            <ta e="T45" id="Seg_1016" s="T44">v:pred</ta>
            <ta e="T47" id="Seg_1017" s="T46">pro.h:S</ta>
            <ta e="T48" id="Seg_1018" s="T47">v:pred</ta>
            <ta e="T49" id="Seg_1019" s="T48">np.h:O</ta>
            <ta e="T51" id="Seg_1020" s="T50">0.2.h:S v:pred</ta>
            <ta e="T52" id="Seg_1021" s="T51">ptcl:pred</ta>
            <ta e="T55" id="Seg_1022" s="T54">v:O</ta>
            <ta e="T57" id="Seg_1023" s="T56">0.3.h:S v:pred</ta>
            <ta e="T60" id="Seg_1024" s="T59">pro.h:S</ta>
            <ta e="T64" id="Seg_1025" s="T63">v:pred</ta>
            <ta e="T65" id="Seg_1026" s="T64">pro.h:S</ta>
            <ta e="T66" id="Seg_1027" s="T65">v:pred</ta>
            <ta e="T68" id="Seg_1028" s="T67">np.h:S</ta>
            <ta e="T70" id="Seg_1029" s="T69">v:pred</ta>
            <ta e="T71" id="Seg_1030" s="T70">pro.h:S</ta>
            <ta e="T72" id="Seg_1031" s="T71">np.h:O</ta>
            <ta e="T73" id="Seg_1032" s="T72">v:pred</ta>
            <ta e="T76" id="Seg_1033" s="T75">0.3.h:S v:pred 0.3.h:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_1034" s="T1">RUS:cult</ta>
            <ta e="T5" id="Seg_1035" s="T4">RUS:cult</ta>
            <ta e="T7" id="Seg_1036" s="T6">RUS:gram</ta>
            <ta e="T8" id="Seg_1037" s="T7">RUS:cult</ta>
            <ta e="T10" id="Seg_1038" s="T9">RUS:gram</ta>
            <ta e="T15" id="Seg_1039" s="T14">RUS:mod</ta>
            <ta e="T19" id="Seg_1040" s="T18">RUS:gram</ta>
            <ta e="T21" id="Seg_1041" s="T20">RUS:cult</ta>
            <ta e="T22" id="Seg_1042" s="T21">RUS:cult</ta>
            <ta e="T26" id="Seg_1043" s="T25">RUS:cult</ta>
            <ta e="T27" id="Seg_1044" s="T26">RUS:cult</ta>
            <ta e="T29" id="Seg_1045" s="T28">RUS:gram</ta>
            <ta e="T32" id="Seg_1046" s="T31">RUS:gram</ta>
            <ta e="T37" id="Seg_1047" s="T36">RUS:gram</ta>
            <ta e="T40" id="Seg_1048" s="T39">RUS:cult</ta>
            <ta e="T41" id="Seg_1049" s="T40">RUS:cult</ta>
            <ta e="T43" id="Seg_1050" s="T42">RUS:gram</ta>
            <ta e="T46" id="Seg_1051" s="T45">RUS:gram</ta>
            <ta e="T52" id="Seg_1052" s="T51">RUS:mod</ta>
            <ta e="T56" id="Seg_1053" s="T55">RUS:gram</ta>
            <ta e="T58" id="Seg_1054" s="T57">RUS:gram</ta>
            <ta e="T59" id="Seg_1055" s="T58">RUS:gram</ta>
            <ta e="T74" id="Seg_1056" s="T73">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T9" id="Seg_1057" s="T1">During the war a plenipotentiary came to us and gathered a meeting.</ta>
            <ta e="T15" id="Seg_1058" s="T9">[I] need people to go fishing.</ta>
            <ta e="T18" id="Seg_1059" s="T15">He was spending the night in our house.</ta>
            <ta e="T25" id="Seg_1060" s="T18">And in the morning uncle Andrej drove him away on a boat. </ta>
            <ta e="T28" id="Seg_1061" s="T25">Uncle Andrej was rowing.</ta>
            <ta e="T31" id="Seg_1062" s="T28">And he was sitting.</ta>
            <ta e="T36" id="Seg_1063" s="T31">And I was (fishing with a net?).</ta>
            <ta e="T39" id="Seg_1064" s="T36">And they were going(?).</ta>
            <ta e="T42" id="Seg_1065" s="T39">Uncle Andrej was rowing.</ta>
            <ta e="T45" id="Seg_1066" s="T42">And he was sitting.</ta>
            <ta e="T51" id="Seg_1067" s="T45">And I said: “Why did you seat the settler?</ta>
            <ta e="T55" id="Seg_1068" s="T51">One should give him an oar.</ta>
            <ta e="T58" id="Seg_1069" s="T55">And he would row.”</ta>
            <ta e="T64" id="Seg_1070" s="T58">And he began speaking Selkup.</ta>
            <ta e="T66" id="Seg_1071" s="T64">I got afraid.</ta>
            <ta e="T70" id="Seg_1072" s="T66">We had many settlers.</ta>
            <ta e="T73" id="Seg_1073" s="T70">They used to kill people.</ta>
            <ta e="T76" id="Seg_1074" s="T73">Then they sent them to us.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T9" id="Seg_1075" s="T1">Während des Krieges kam ein Generalbevollmächtigter zu uns und hielt ein Treffen ab.</ta>
            <ta e="T15" id="Seg_1076" s="T9">[Ich] brauche Leute zum Fischen.</ta>
            <ta e="T18" id="Seg_1077" s="T15">Er übernachtete bei uns (im Haus).</ta>
            <ta e="T25" id="Seg_1078" s="T18">Und am Morgen fuhr Onkel Andrej ihn mit dem Boot hinaus.</ta>
            <ta e="T28" id="Seg_1079" s="T25">Onkel Andrej ruderte.</ta>
            <ta e="T31" id="Seg_1080" s="T28">Und er saß.</ta>
            <ta e="T36" id="Seg_1081" s="T31">Und ich (fischte mit dem Netz?).</ta>
            <ta e="T39" id="Seg_1082" s="T36">Und sie gingen(?).</ta>
            <ta e="T42" id="Seg_1083" s="T39">Onkel Andrej ruderte.</ta>
            <ta e="T45" id="Seg_1084" s="T42">Und er saß.</ta>
            <ta e="T51" id="Seg_1085" s="T45">Und ich sagte: "Warum hast du den Siedler hingesetzt?</ta>
            <ta e="T55" id="Seg_1086" s="T51">Man sollte ihm ein Ruder geben.</ta>
            <ta e="T58" id="Seg_1087" s="T55">Und er würde rudern."</ta>
            <ta e="T64" id="Seg_1088" s="T58">Und er begann Selkupisch zu sprechen.</ta>
            <ta e="T66" id="Seg_1089" s="T64">Ich bekam Angst.</ta>
            <ta e="T70" id="Seg_1090" s="T66">Wir hatten viele Siedler.</ta>
            <ta e="T73" id="Seg_1091" s="T70">Sie töteten für gewöhnlich Menschen.</ta>
            <ta e="T76" id="Seg_1092" s="T73">Dann schickten sie sie zu uns.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T9" id="Seg_1093" s="T1">Во время войны приехал к нам уполномоченный и устроил собрание.</ta>
            <ta e="T15" id="Seg_1094" s="T9">И (людей?) рыбачить, рыбачить, говорит, надо.</ta>
            <ta e="T18" id="Seg_1095" s="T15">Он у нас ночевал.</ta>
            <ta e="T25" id="Seg_1096" s="T18">А утром дядя Андрей его увез в обласке.</ta>
            <ta e="T28" id="Seg_1097" s="T25">Дядя Андрей гребет.</ta>
            <ta e="T31" id="Seg_1098" s="T28">А он сидит.</ta>
            <ta e="T36" id="Seg_1099" s="T31">А я куревой сидела.</ta>
            <ta e="T39" id="Seg_1100" s="T36">А они идут(?).</ta>
            <ta e="T42" id="Seg_1101" s="T39">Дядя Андрей гребет.</ta>
            <ta e="T45" id="Seg_1102" s="T42">А он сидит.</ta>
            <ta e="T51" id="Seg_1103" s="T45">А я говорю: “Поселенца зачем посадил?</ta>
            <ta e="T55" id="Seg_1104" s="T51">Надо ему весло дать.</ta>
            <ta e="T58" id="Seg_1105" s="T55">И греб бы он”.</ta>
            <ta e="T64" id="Seg_1106" s="T58">А он по-селькупски стал разговаривать.</ta>
            <ta e="T66" id="Seg_1107" s="T64">Я испугалась.</ta>
            <ta e="T70" id="Seg_1108" s="T66">У нас поселенцев много было.</ta>
            <ta e="T73" id="Seg_1109" s="T70">Они людей убивали.</ta>
            <ta e="T76" id="Seg_1110" s="T73">Потом к нам посылали их.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T9" id="Seg_1111" s="T1">во время войны приехал к нам полномочный уполномоченный, собранье провел (сделал).</ta>
            <ta e="T15" id="Seg_1112" s="T9">и говорит надо рыбачить и рыбачить</ta>
            <ta e="T18" id="Seg_1113" s="T15">он у нас ночевал</ta>
            <ta e="T25" id="Seg_1114" s="T18">а утром дядя его увез в обласке</ta>
            <ta e="T28" id="Seg_1115" s="T25">дядя гребет</ta>
            <ta e="T31" id="Seg_1116" s="T28">а он сидит</ta>
            <ta e="T36" id="Seg_1117" s="T31">а я куревой сидела</ta>
            <ta e="T42" id="Seg_1118" s="T39">дядя Андрей гребет</ta>
            <ta e="T45" id="Seg_1119" s="T42">а он сидит</ta>
            <ta e="T51" id="Seg_1120" s="T45">я говорю поселенца зачем посадил</ta>
            <ta e="T55" id="Seg_1121" s="T51">надо бы ему весло дать</ta>
            <ta e="T58" id="Seg_1122" s="T55">и греб бы он</ta>
            <ta e="T64" id="Seg_1123" s="T58">а он по-остяцки стал разговаривать</ta>
            <ta e="T66" id="Seg_1124" s="T64">я испугалась</ta>
            <ta e="T70" id="Seg_1125" s="T66">у нас поселенцев много было</ta>
            <ta e="T73" id="Seg_1126" s="T70">они людей убивали</ta>
            <ta e="T76" id="Seg_1127" s="T73">потом (к) нам посылали их</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto">
            <ta e="T36" id="Seg_1128" s="T31">тʼӓкгъ поkkъ - куревой</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
