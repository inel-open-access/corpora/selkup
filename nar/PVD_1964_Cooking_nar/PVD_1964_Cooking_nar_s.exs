<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Cooking_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Cooking_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">93</ud-information>
            <ud-information attribute-name="# HIAT:w">70</ud-information>
            <ud-information attribute-name="# e">70</ud-information>
            <ud-information attribute-name="# HIAT:u">20</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T71" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">A</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">qaːrdʼen</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">man</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">qaj</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">poːčʼitan</ts>
                  <nts id="Seg_17" n="HIAT:ip">?</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_20" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">Taːn</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">mega</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">ketket</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_32" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">Jeʒlʼe</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">awan</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">mekou</ts>
                  <nts id="Seg_41" n="HIAT:ip">,</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">tan</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">mazɨm</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">oːɣulʒəʒaq</ts>
                  <nts id="Seg_51" n="HIAT:ip">.</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_54" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">Ato</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">man</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">megu</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">aːs</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">tonan</ts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_72" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">Man</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_77" n="HIAT:w" s="T21">jedoɣon</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_80" n="HIAT:w" s="T22">eləzan</ts>
                  <nts id="Seg_81" n="HIAT:ip">.</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_84" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">Menan</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">tolʼko</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">qwɨl</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_96" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">Me</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">tebəm</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">poːtkowtə</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">i</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">ʒarəkoftə</ts>
                  <nts id="Seg_111" n="HIAT:ip">.</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_114" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_116" n="HIAT:w" s="T31">Wadʼzʼe</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">jen</ts>
                  <nts id="Seg_120" n="HIAT:ip">,</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_123" n="HIAT:w" s="T33">man</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_126" n="HIAT:w" s="T34">tebɨm</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_129" n="HIAT:w" s="T35">potkow</ts>
                  <nts id="Seg_130" n="HIAT:ip">.</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_133" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_135" n="HIAT:w" s="T36">Bolʼše</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_138" n="HIAT:w" s="T37">man</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_141" n="HIAT:w" s="T38">qaimnə</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_144" n="HIAT:w" s="T39">megu</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_147" n="HIAT:w" s="T40">as</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_150" n="HIAT:w" s="T41">tonan</ts>
                  <nts id="Seg_151" n="HIAT:ip">.</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_154" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_156" n="HIAT:w" s="T42">Qwəlɨsku</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_159" n="HIAT:w" s="T43">tunan</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_163" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_165" n="HIAT:w" s="T44">Koːcʼin</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_168" n="HIAT:w" s="T45">tudoːlam</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_171" n="HIAT:w" s="T46">koːcʼit</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_174" n="HIAT:w" s="T47">qwatquzau</ts>
                  <nts id="Seg_175" n="HIAT:ip">.</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_178" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_180" n="HIAT:w" s="T48">Tan</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_183" n="HIAT:w" s="T49">onetdə</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_186" n="HIAT:w" s="T50">koǯirsal</ts>
                  <nts id="Seg_187" n="HIAT:ip">.</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_190" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_192" n="HIAT:w" s="T51">Müzɨpba</ts>
                  <nts id="Seg_193" n="HIAT:ip">?</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_196" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_198" n="HIAT:w" s="T52">As</ts>
                  <nts id="Seg_199" n="HIAT:ip">.</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_202" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_204" n="HIAT:w" s="T53">Müzɨpba</ts>
                  <nts id="Seg_205" n="HIAT:ip">?</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_208" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_210" n="HIAT:w" s="T54">Nu</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_213" n="HIAT:w" s="T55">itdə</ts>
                  <nts id="Seg_214" n="HIAT:ip">.</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_217" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_219" n="HIAT:w" s="T56">Pusʼ</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_222" n="HIAT:w" s="T57">ešo</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_225" n="HIAT:w" s="T58">perčʼa</ts>
                  <nts id="Seg_226" n="HIAT:ip">,</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_229" n="HIAT:w" s="T59">kunan</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_232" n="HIAT:w" s="T60">perčʼan</ts>
                  <nts id="Seg_233" n="HIAT:ip">.</nts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_236" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_238" n="HIAT:w" s="T61">Qwalle</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_241" n="HIAT:w" s="T62">Swetam</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_244" n="HIAT:w" s="T63">sədetǯau</ts>
                  <nts id="Seg_245" n="HIAT:ip">.</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_248" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_250" n="HIAT:w" s="T64">Man</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_253" n="HIAT:w" s="T65">tebɨm</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_256" n="HIAT:w" s="T66">pozno</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_259" n="HIAT:w" s="T67">sədau</ts>
                  <nts id="Seg_260" n="HIAT:ip">.</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_263" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_265" n="HIAT:w" s="T68">Tep</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_268" n="HIAT:w" s="T69">krepkan</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_271" n="HIAT:w" s="T70">qotda</ts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T71" id="Seg_274" n="sc" s="T1">
               <ts e="T2" id="Seg_276" n="e" s="T1">A </ts>
               <ts e="T3" id="Seg_278" n="e" s="T2">qaːrdʼen </ts>
               <ts e="T4" id="Seg_280" n="e" s="T3">man </ts>
               <ts e="T5" id="Seg_282" n="e" s="T4">qaj </ts>
               <ts e="T6" id="Seg_284" n="e" s="T5">poːčʼitan? </ts>
               <ts e="T7" id="Seg_286" n="e" s="T6">Taːn </ts>
               <ts e="T8" id="Seg_288" n="e" s="T7">mega </ts>
               <ts e="T9" id="Seg_290" n="e" s="T8">ketket. </ts>
               <ts e="T10" id="Seg_292" n="e" s="T9">Jeʒlʼe </ts>
               <ts e="T11" id="Seg_294" n="e" s="T10">awan </ts>
               <ts e="T12" id="Seg_296" n="e" s="T11">mekou, </ts>
               <ts e="T13" id="Seg_298" n="e" s="T12">tan </ts>
               <ts e="T14" id="Seg_300" n="e" s="T13">mazɨm </ts>
               <ts e="T15" id="Seg_302" n="e" s="T14">oːɣulʒəʒaq. </ts>
               <ts e="T16" id="Seg_304" n="e" s="T15">Ato </ts>
               <ts e="T17" id="Seg_306" n="e" s="T16">man </ts>
               <ts e="T18" id="Seg_308" n="e" s="T17">megu </ts>
               <ts e="T19" id="Seg_310" n="e" s="T18">aːs </ts>
               <ts e="T20" id="Seg_312" n="e" s="T19">tonan. </ts>
               <ts e="T21" id="Seg_314" n="e" s="T20">Man </ts>
               <ts e="T22" id="Seg_316" n="e" s="T21">jedoɣon </ts>
               <ts e="T23" id="Seg_318" n="e" s="T22">eləzan. </ts>
               <ts e="T24" id="Seg_320" n="e" s="T23">Menan </ts>
               <ts e="T25" id="Seg_322" n="e" s="T24">tolʼko </ts>
               <ts e="T26" id="Seg_324" n="e" s="T25">qwɨl. </ts>
               <ts e="T27" id="Seg_326" n="e" s="T26">Me </ts>
               <ts e="T28" id="Seg_328" n="e" s="T27">tebəm </ts>
               <ts e="T29" id="Seg_330" n="e" s="T28">poːtkowtə </ts>
               <ts e="T30" id="Seg_332" n="e" s="T29">i </ts>
               <ts e="T31" id="Seg_334" n="e" s="T30">ʒarəkoftə. </ts>
               <ts e="T32" id="Seg_336" n="e" s="T31">Wadʼzʼe </ts>
               <ts e="T33" id="Seg_338" n="e" s="T32">jen, </ts>
               <ts e="T34" id="Seg_340" n="e" s="T33">man </ts>
               <ts e="T35" id="Seg_342" n="e" s="T34">tebɨm </ts>
               <ts e="T36" id="Seg_344" n="e" s="T35">potkow. </ts>
               <ts e="T37" id="Seg_346" n="e" s="T36">Bolʼše </ts>
               <ts e="T38" id="Seg_348" n="e" s="T37">man </ts>
               <ts e="T39" id="Seg_350" n="e" s="T38">qaimnə </ts>
               <ts e="T40" id="Seg_352" n="e" s="T39">megu </ts>
               <ts e="T41" id="Seg_354" n="e" s="T40">as </ts>
               <ts e="T42" id="Seg_356" n="e" s="T41">tonan. </ts>
               <ts e="T43" id="Seg_358" n="e" s="T42">Qwəlɨsku </ts>
               <ts e="T44" id="Seg_360" n="e" s="T43">tunan. </ts>
               <ts e="T45" id="Seg_362" n="e" s="T44">Koːcʼin </ts>
               <ts e="T46" id="Seg_364" n="e" s="T45">tudoːlam </ts>
               <ts e="T47" id="Seg_366" n="e" s="T46">koːcʼit </ts>
               <ts e="T48" id="Seg_368" n="e" s="T47">qwatquzau. </ts>
               <ts e="T49" id="Seg_370" n="e" s="T48">Tan </ts>
               <ts e="T50" id="Seg_372" n="e" s="T49">onetdə </ts>
               <ts e="T51" id="Seg_374" n="e" s="T50">koǯirsal. </ts>
               <ts e="T52" id="Seg_376" n="e" s="T51">Müzɨpba? </ts>
               <ts e="T53" id="Seg_378" n="e" s="T52">As. </ts>
               <ts e="T54" id="Seg_380" n="e" s="T53">Müzɨpba? </ts>
               <ts e="T55" id="Seg_382" n="e" s="T54">Nu </ts>
               <ts e="T56" id="Seg_384" n="e" s="T55">itdə. </ts>
               <ts e="T57" id="Seg_386" n="e" s="T56">Pusʼ </ts>
               <ts e="T58" id="Seg_388" n="e" s="T57">ešo </ts>
               <ts e="T59" id="Seg_390" n="e" s="T58">perčʼa, </ts>
               <ts e="T60" id="Seg_392" n="e" s="T59">kunan </ts>
               <ts e="T61" id="Seg_394" n="e" s="T60">perčʼan. </ts>
               <ts e="T62" id="Seg_396" n="e" s="T61">Qwalle </ts>
               <ts e="T63" id="Seg_398" n="e" s="T62">Swetam </ts>
               <ts e="T64" id="Seg_400" n="e" s="T63">sədetǯau. </ts>
               <ts e="T65" id="Seg_402" n="e" s="T64">Man </ts>
               <ts e="T66" id="Seg_404" n="e" s="T65">tebɨm </ts>
               <ts e="T67" id="Seg_406" n="e" s="T66">pozno </ts>
               <ts e="T68" id="Seg_408" n="e" s="T67">sədau. </ts>
               <ts e="T69" id="Seg_410" n="e" s="T68">Tep </ts>
               <ts e="T70" id="Seg_412" n="e" s="T69">krepkan </ts>
               <ts e="T71" id="Seg_414" n="e" s="T70">qotda. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_415" s="T1">PVD_1964_Cooking_nar.001 (001.001)</ta>
            <ta e="T9" id="Seg_416" s="T6">PVD_1964_Cooking_nar.002 (001.002)</ta>
            <ta e="T15" id="Seg_417" s="T9">PVD_1964_Cooking_nar.003 (001.003)</ta>
            <ta e="T20" id="Seg_418" s="T15">PVD_1964_Cooking_nar.004 (001.004)</ta>
            <ta e="T23" id="Seg_419" s="T20">PVD_1964_Cooking_nar.005 (001.005)</ta>
            <ta e="T26" id="Seg_420" s="T23">PVD_1964_Cooking_nar.006 (001.006)</ta>
            <ta e="T31" id="Seg_421" s="T26">PVD_1964_Cooking_nar.007 (001.007)</ta>
            <ta e="T36" id="Seg_422" s="T31">PVD_1964_Cooking_nar.008 (001.008)</ta>
            <ta e="T42" id="Seg_423" s="T36">PVD_1964_Cooking_nar.009 (001.009)</ta>
            <ta e="T44" id="Seg_424" s="T42">PVD_1964_Cooking_nar.010 (001.010)</ta>
            <ta e="T48" id="Seg_425" s="T44">PVD_1964_Cooking_nar.011 (001.011)</ta>
            <ta e="T51" id="Seg_426" s="T48">PVD_1964_Cooking_nar.012 (001.012)</ta>
            <ta e="T52" id="Seg_427" s="T51">PVD_1964_Cooking_nar.013 (001.013)</ta>
            <ta e="T53" id="Seg_428" s="T52">PVD_1964_Cooking_nar.014 (001.014)</ta>
            <ta e="T54" id="Seg_429" s="T53">PVD_1964_Cooking_nar.015 (001.015)</ta>
            <ta e="T56" id="Seg_430" s="T54">PVD_1964_Cooking_nar.016 (001.016)</ta>
            <ta e="T61" id="Seg_431" s="T56">PVD_1964_Cooking_nar.017 (001.017)</ta>
            <ta e="T64" id="Seg_432" s="T61">PVD_1964_Cooking_nar.018 (001.018)</ta>
            <ta e="T68" id="Seg_433" s="T64">PVD_1964_Cooking_nar.019 (001.019)</ta>
            <ta e="T71" id="Seg_434" s="T68">PVD_1964_Cooking_nar.020 (001.020)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_435" s="T1">а kа̄р ′дʼен ман kай ′п(б)о̄читан?</ta>
            <ta e="T9" id="Seg_436" s="T6">та̄н ′мега кет′кет.</ta>
            <ta e="T15" id="Seg_437" s="T9">′jежлʼе а′ван ′мекоу̹, тан ′мазым ′о̄ɣулжъжаk.</ta>
            <ta e="T20" id="Seg_438" s="T15">а то ман ′мегу ′а̄сто(у)нан.</ta>
            <ta e="T23" id="Seg_439" s="T20">ман ′jедоɣон ′елъ(ы)зан.</ta>
            <ta e="T26" id="Seg_440" s="T23">ме′нан ′толʼко kwыл.</ta>
            <ta e="T31" id="Seg_441" s="T26">′ме ′тебъм п(б̂)о̄ткоw(ф)тъ и ′ж̂аръкофтъ.</ta>
            <ta e="T36" id="Seg_442" s="T31">′вадʼзʼе jен, ман ′тебым ′поткоw.</ta>
            <ta e="T42" id="Seg_443" s="T36">болʼше ман ′kаимнъ ′мегу ′асто‵нан.</ta>
            <ta e="T44" id="Seg_444" s="T42">kwъ(ɛ)лыс′ку ту′нан.</ta>
            <ta e="T48" id="Seg_445" s="T44">ко̄цʼин ′ту′до̄лам ко̄цʼит kwатkузау̹.</ta>
            <ta e="T51" id="Seg_446" s="T48">тан о′не(ɛ)тдъ ′коджир‵сал.</ta>
            <ta e="T52" id="Seg_447" s="T51">мӱ′зыпба?</ta>
            <ta e="T53" id="Seg_448" s="T52">ас.</ta>
            <ta e="T54" id="Seg_449" s="T53">мӱ′зыпб̂а?</ta>
            <ta e="T56" id="Seg_450" s="T54">ну ′итдъ.</ta>
            <ta e="T61" id="Seg_451" s="T56">пусʼ е′шо ′перча, ′кунан перчан.</ta>
            <ta e="T64" id="Seg_452" s="T61">kwаllе Сwетам съ′де̹тджау.</ta>
            <ta e="T68" id="Seg_453" s="T64">ман тебым ′позно съ′дау̹.</ta>
            <ta e="T71" id="Seg_454" s="T68">теп крепкан ′kотда.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_455" s="T1">a qaːr dʼen man qaj p(b)oːčitan?</ta>
            <ta e="T9" id="Seg_456" s="T6">taːn mega ketket.</ta>
            <ta e="T15" id="Seg_457" s="T9">jeʒlʼe awan mekou̹, tan mazɨm oːɣulʒəʒaq.</ta>
            <ta e="T20" id="Seg_458" s="T15">a to man megu aːsto(u)nan.</ta>
            <ta e="T23" id="Seg_459" s="T20">man jedoɣon elə(ɨ)zan.</ta>
            <ta e="T26" id="Seg_460" s="T23">menan tolʼko qwɨl.</ta>
            <ta e="T31" id="Seg_461" s="T26">me tebəm p(b̂)oːtkow(f)tə i ʒ̂arəkoftə.</ta>
            <ta e="T36" id="Seg_462" s="T31">wadʼzʼe jen, man tebɨm potkow.</ta>
            <ta e="T42" id="Seg_463" s="T36">bolʼše man qaimnə megu astonan.</ta>
            <ta e="T44" id="Seg_464" s="T42">qwə(ɛ)lɨsku tunan.</ta>
            <ta e="T48" id="Seg_465" s="T44">koːcʼin tudoːlam koːcʼit qwatquzau̹.</ta>
            <ta e="T51" id="Seg_466" s="T48">tan one(ɛ)tdə koǯirsal.</ta>
            <ta e="T52" id="Seg_467" s="T51">müzɨpba?</ta>
            <ta e="T53" id="Seg_468" s="T52">as.</ta>
            <ta e="T54" id="Seg_469" s="T53">müzɨpb̂a?</ta>
            <ta e="T56" id="Seg_470" s="T54">nu itdə.</ta>
            <ta e="T61" id="Seg_471" s="T56">pusʼ ešo perča, kunan perčan.</ta>
            <ta e="T64" id="Seg_472" s="T61">qwalle Сwetam səde̹tǯau.</ta>
            <ta e="T68" id="Seg_473" s="T64">man tebɨm pozno sədau̹.</ta>
            <ta e="T71" id="Seg_474" s="T68">tep krepkan qotda.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_475" s="T1">A qaːrdʼen man qaj poːčʼitan? </ta>
            <ta e="T9" id="Seg_476" s="T6">Taːn mega ketket. </ta>
            <ta e="T15" id="Seg_477" s="T9">Jeʒlʼe awan mekou, tan mazɨm oːɣulʒəʒaq. </ta>
            <ta e="T20" id="Seg_478" s="T15">Ato man megu aːs tonan. </ta>
            <ta e="T23" id="Seg_479" s="T20">Man jedoɣon eləzan. </ta>
            <ta e="T26" id="Seg_480" s="T23">Menan tolʼko qwɨl. </ta>
            <ta e="T31" id="Seg_481" s="T26">Me tebəm poːtkowtə i ʒarəkoftə. </ta>
            <ta e="T36" id="Seg_482" s="T31">Wadʼzʼe jen, man tebɨm potkow. </ta>
            <ta e="T42" id="Seg_483" s="T36">Bolʼše man qaimnə megu as tonan. </ta>
            <ta e="T44" id="Seg_484" s="T42">Qwəlɨsku tunan. </ta>
            <ta e="T48" id="Seg_485" s="T44">Koːcʼin tudoːlam koːcʼit qwatquzau. </ta>
            <ta e="T51" id="Seg_486" s="T48">Tan onetdə koǯirsal. </ta>
            <ta e="T52" id="Seg_487" s="T51">Müzɨpba? </ta>
            <ta e="T53" id="Seg_488" s="T52">As. </ta>
            <ta e="T54" id="Seg_489" s="T53">Müzɨpba? </ta>
            <ta e="T56" id="Seg_490" s="T54">Nu itdə. </ta>
            <ta e="T61" id="Seg_491" s="T56">Pusʼ ešo perčʼa, kunan perčʼan. </ta>
            <ta e="T64" id="Seg_492" s="T61">Qwalle Swetam sədetǯau. </ta>
            <ta e="T68" id="Seg_493" s="T64">Man tebɨm pozno sədau. </ta>
            <ta e="T71" id="Seg_494" s="T68">Tep krepkan qotda. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_495" s="T1">a</ta>
            <ta e="T3" id="Seg_496" s="T2">qaːrdʼe-n</ta>
            <ta e="T4" id="Seg_497" s="T3">man</ta>
            <ta e="T5" id="Seg_498" s="T4">qaj</ta>
            <ta e="T6" id="Seg_499" s="T5">poː-čʼi-ta-n</ta>
            <ta e="T7" id="Seg_500" s="T6">taːn</ta>
            <ta e="T8" id="Seg_501" s="T7">mega</ta>
            <ta e="T9" id="Seg_502" s="T8">ket-k-et</ta>
            <ta e="T10" id="Seg_503" s="T9">jeʒlʼe</ta>
            <ta e="T11" id="Seg_504" s="T10">awa-n</ta>
            <ta e="T12" id="Seg_505" s="T11">me-ko-u</ta>
            <ta e="T13" id="Seg_506" s="T12">tat</ta>
            <ta e="T14" id="Seg_507" s="T13">mazɨm</ta>
            <ta e="T15" id="Seg_508" s="T14">oːɣulʒə-ʒa-q</ta>
            <ta e="T16" id="Seg_509" s="T15">ato</ta>
            <ta e="T17" id="Seg_510" s="T16">man</ta>
            <ta e="T18" id="Seg_511" s="T17">me-gu</ta>
            <ta e="T19" id="Seg_512" s="T18">aːs</ta>
            <ta e="T20" id="Seg_513" s="T19">tona-n</ta>
            <ta e="T21" id="Seg_514" s="T20">man</ta>
            <ta e="T22" id="Seg_515" s="T21">jedo-ɣon</ta>
            <ta e="T23" id="Seg_516" s="T22">elə-za-n</ta>
            <ta e="T24" id="Seg_517" s="T23">me-nan</ta>
            <ta e="T25" id="Seg_518" s="T24">tolʼko</ta>
            <ta e="T26" id="Seg_519" s="T25">qwɨl</ta>
            <ta e="T27" id="Seg_520" s="T26">me</ta>
            <ta e="T28" id="Seg_521" s="T27">teb-ə-m</ta>
            <ta e="T29" id="Seg_522" s="T28">poːt-ko-wtə</ta>
            <ta e="T30" id="Seg_523" s="T29">i</ta>
            <ta e="T31" id="Seg_524" s="T30">ʒarə-ko-ftə</ta>
            <ta e="T32" id="Seg_525" s="T31">wadʼzʼe</ta>
            <ta e="T33" id="Seg_526" s="T32">je-n</ta>
            <ta e="T34" id="Seg_527" s="T33">man</ta>
            <ta e="T35" id="Seg_528" s="T34">teb-ɨ-m</ta>
            <ta e="T36" id="Seg_529" s="T35">pot-ko-w</ta>
            <ta e="T37" id="Seg_530" s="T36">bolʼše</ta>
            <ta e="T38" id="Seg_531" s="T37">man</ta>
            <ta e="T39" id="Seg_532" s="T38">qai-m-nə</ta>
            <ta e="T40" id="Seg_533" s="T39">me-gu</ta>
            <ta e="T41" id="Seg_534" s="T40">as</ta>
            <ta e="T42" id="Seg_535" s="T41">tona-n</ta>
            <ta e="T43" id="Seg_536" s="T42">qwəl-ɨ-s-ku</ta>
            <ta e="T44" id="Seg_537" s="T43">tuna-n</ta>
            <ta e="T45" id="Seg_538" s="T44">koːcʼi-n</ta>
            <ta e="T46" id="Seg_539" s="T45">tudoː-la-m</ta>
            <ta e="T47" id="Seg_540" s="T46">koːcʼi-t</ta>
            <ta e="T48" id="Seg_541" s="T47">qwat-qu-za-u</ta>
            <ta e="T49" id="Seg_542" s="T48">tat</ta>
            <ta e="T50" id="Seg_543" s="T49">onetdə</ta>
            <ta e="T51" id="Seg_544" s="T50">ko-ǯir-sa-l</ta>
            <ta e="T52" id="Seg_545" s="T51">müzɨ-pba</ta>
            <ta e="T53" id="Seg_546" s="T52">as</ta>
            <ta e="T54" id="Seg_547" s="T53">müzɨ-pba</ta>
            <ta e="T55" id="Seg_548" s="T54">nu</ta>
            <ta e="T56" id="Seg_549" s="T55">i-tdə</ta>
            <ta e="T57" id="Seg_550" s="T56">pusʼ</ta>
            <ta e="T58" id="Seg_551" s="T57">ešo</ta>
            <ta e="T59" id="Seg_552" s="T58">perčʼa</ta>
            <ta e="T60" id="Seg_553" s="T59">kun-a-n</ta>
            <ta e="T61" id="Seg_554" s="T60">perčʼa-n</ta>
            <ta e="T62" id="Seg_555" s="T61">qwal-le</ta>
            <ta e="T63" id="Seg_556" s="T62">Sweta-m</ta>
            <ta e="T64" id="Seg_557" s="T63">səde-tǯa-u</ta>
            <ta e="T65" id="Seg_558" s="T64">man</ta>
            <ta e="T66" id="Seg_559" s="T65">teb-ɨ-m</ta>
            <ta e="T67" id="Seg_560" s="T66">pozno</ta>
            <ta e="T68" id="Seg_561" s="T67">səda-u</ta>
            <ta e="T69" id="Seg_562" s="T68">tep</ta>
            <ta e="T70" id="Seg_563" s="T69">krepka-n</ta>
            <ta e="T71" id="Seg_564" s="T70">qotda</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_565" s="T1">a</ta>
            <ta e="T3" id="Seg_566" s="T2">qardʼe-n</ta>
            <ta e="T4" id="Seg_567" s="T3">man</ta>
            <ta e="T5" id="Seg_568" s="T4">qaj</ta>
            <ta e="T6" id="Seg_569" s="T5">poːt-ntɨ-enǯɨ-ŋ</ta>
            <ta e="T7" id="Seg_570" s="T6">tan</ta>
            <ta e="T8" id="Seg_571" s="T7">mekka</ta>
            <ta e="T9" id="Seg_572" s="T8">ket-ku-etɨ</ta>
            <ta e="T10" id="Seg_573" s="T9">jeʒlʼe</ta>
            <ta e="T11" id="Seg_574" s="T10">awa-ŋ</ta>
            <ta e="T12" id="Seg_575" s="T11">meː-ku-w</ta>
            <ta e="T13" id="Seg_576" s="T12">tan</ta>
            <ta e="T14" id="Seg_577" s="T13">mazɨm</ta>
            <ta e="T15" id="Seg_578" s="T14">oɣulǯɨ-ʒu-kɨ</ta>
            <ta e="T16" id="Seg_579" s="T15">ato</ta>
            <ta e="T17" id="Seg_580" s="T16">man</ta>
            <ta e="T18" id="Seg_581" s="T17">meː-gu</ta>
            <ta e="T19" id="Seg_582" s="T18">asa</ta>
            <ta e="T20" id="Seg_583" s="T19">tonu-ŋ</ta>
            <ta e="T21" id="Seg_584" s="T20">man</ta>
            <ta e="T22" id="Seg_585" s="T21">eːde-qɨn</ta>
            <ta e="T23" id="Seg_586" s="T22">elɨ-sɨ-ŋ</ta>
            <ta e="T24" id="Seg_587" s="T23">me-nan</ta>
            <ta e="T25" id="Seg_588" s="T24">tolʼko</ta>
            <ta e="T26" id="Seg_589" s="T25">qwɛl</ta>
            <ta e="T27" id="Seg_590" s="T26">me</ta>
            <ta e="T28" id="Seg_591" s="T27">täp-ɨ-m</ta>
            <ta e="T29" id="Seg_592" s="T28">poːt-ku-un</ta>
            <ta e="T30" id="Seg_593" s="T29">i</ta>
            <ta e="T31" id="Seg_594" s="T30">šari-ku-un</ta>
            <ta e="T32" id="Seg_595" s="T31">wadʼi</ta>
            <ta e="T33" id="Seg_596" s="T32">eː-n</ta>
            <ta e="T34" id="Seg_597" s="T33">man</ta>
            <ta e="T35" id="Seg_598" s="T34">täp-ɨ-m</ta>
            <ta e="T36" id="Seg_599" s="T35">poːt-ku-w</ta>
            <ta e="T37" id="Seg_600" s="T36">bolʼše</ta>
            <ta e="T38" id="Seg_601" s="T37">man</ta>
            <ta e="T39" id="Seg_602" s="T38">qaj-m-näj</ta>
            <ta e="T40" id="Seg_603" s="T39">meː-gu</ta>
            <ta e="T41" id="Seg_604" s="T40">asa</ta>
            <ta e="T42" id="Seg_605" s="T41">tonu-ŋ</ta>
            <ta e="T43" id="Seg_606" s="T42">qwɛl-ɨ-s-gu</ta>
            <ta e="T44" id="Seg_607" s="T43">tonu-ŋ</ta>
            <ta e="T45" id="Seg_608" s="T44">koːci-n</ta>
            <ta e="T46" id="Seg_609" s="T45">tudo-la-m</ta>
            <ta e="T47" id="Seg_610" s="T46">koːci-n</ta>
            <ta e="T48" id="Seg_611" s="T47">qwat-ku-sɨ-w</ta>
            <ta e="T49" id="Seg_612" s="T48">tan</ta>
            <ta e="T50" id="Seg_613" s="T49">onendǝ</ta>
            <ta e="T51" id="Seg_614" s="T50">qo-nǯir-sɨ-l</ta>
            <ta e="T52" id="Seg_615" s="T51">mözu-mbɨ</ta>
            <ta e="T53" id="Seg_616" s="T52">asa</ta>
            <ta e="T54" id="Seg_617" s="T53">mözu-mbɨ</ta>
            <ta e="T55" id="Seg_618" s="T54">nu</ta>
            <ta e="T56" id="Seg_619" s="T55">iː-etɨ</ta>
            <ta e="T57" id="Seg_620" s="T56">pusʼtʼ</ta>
            <ta e="T58" id="Seg_621" s="T57">ešo</ta>
            <ta e="T59" id="Seg_622" s="T58">pärču</ta>
            <ta e="T60" id="Seg_623" s="T59">kun-ɨ-n</ta>
            <ta e="T61" id="Seg_624" s="T60">pärču-n</ta>
            <ta e="T62" id="Seg_625" s="T61">qwan-le</ta>
            <ta e="T63" id="Seg_626" s="T62">Sveta-m</ta>
            <ta e="T64" id="Seg_627" s="T63">söde-enǯɨ-w</ta>
            <ta e="T65" id="Seg_628" s="T64">man</ta>
            <ta e="T66" id="Seg_629" s="T65">täp-ɨ-m</ta>
            <ta e="T67" id="Seg_630" s="T66">pozno</ta>
            <ta e="T68" id="Seg_631" s="T67">söde-w</ta>
            <ta e="T69" id="Seg_632" s="T68">täp</ta>
            <ta e="T70" id="Seg_633" s="T69">krepka-ŋ</ta>
            <ta e="T71" id="Seg_634" s="T70">qondu</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_635" s="T1">and</ta>
            <ta e="T3" id="Seg_636" s="T2">tomorrow-ADV.LOC</ta>
            <ta e="T4" id="Seg_637" s="T3">I.NOM</ta>
            <ta e="T5" id="Seg_638" s="T4">what.[NOM]</ta>
            <ta e="T6" id="Seg_639" s="T5">cook-INFER-FUT-1SG.S</ta>
            <ta e="T7" id="Seg_640" s="T6">you.SG.NOM</ta>
            <ta e="T8" id="Seg_641" s="T7">I.ALL</ta>
            <ta e="T9" id="Seg_642" s="T8">say-HAB-IMP.2SG.O</ta>
            <ta e="T10" id="Seg_643" s="T9">if</ta>
            <ta e="T11" id="Seg_644" s="T10">bad-ADVZ</ta>
            <ta e="T12" id="Seg_645" s="T11">do-HAB-1SG.O</ta>
            <ta e="T13" id="Seg_646" s="T12">you.SG.NOM</ta>
            <ta e="T14" id="Seg_647" s="T13">I.ACC</ta>
            <ta e="T15" id="Seg_648" s="T14">teach-DRV-IMP.2SG.S</ta>
            <ta e="T16" id="Seg_649" s="T15">otherwise</ta>
            <ta e="T17" id="Seg_650" s="T16">I.NOM</ta>
            <ta e="T18" id="Seg_651" s="T17">do-INF</ta>
            <ta e="T19" id="Seg_652" s="T18">NEG</ta>
            <ta e="T20" id="Seg_653" s="T19">can-1SG.S</ta>
            <ta e="T21" id="Seg_654" s="T20">I.NOM</ta>
            <ta e="T22" id="Seg_655" s="T21">village-LOC</ta>
            <ta e="T23" id="Seg_656" s="T22">live-PST-1SG.S</ta>
            <ta e="T24" id="Seg_657" s="T23">we-ADES</ta>
            <ta e="T25" id="Seg_658" s="T24">only</ta>
            <ta e="T26" id="Seg_659" s="T25">fish.[NOM]</ta>
            <ta e="T27" id="Seg_660" s="T26">we.[NOM]</ta>
            <ta e="T28" id="Seg_661" s="T27">(s)he-EP-ACC</ta>
            <ta e="T29" id="Seg_662" s="T28">cook-HAB-1PL</ta>
            <ta e="T30" id="Seg_663" s="T29">and</ta>
            <ta e="T31" id="Seg_664" s="T30">fry-HAB-1PL</ta>
            <ta e="T32" id="Seg_665" s="T31">meat.[NOM]</ta>
            <ta e="T33" id="Seg_666" s="T32">be-3SG.S</ta>
            <ta e="T34" id="Seg_667" s="T33">I.NOM</ta>
            <ta e="T35" id="Seg_668" s="T34">(s)he-EP-ACC</ta>
            <ta e="T36" id="Seg_669" s="T35">cook-HAB-1SG.O</ta>
            <ta e="T37" id="Seg_670" s="T36">more</ta>
            <ta e="T38" id="Seg_671" s="T37">I.NOM</ta>
            <ta e="T39" id="Seg_672" s="T38">what-ACC-EMPH</ta>
            <ta e="T40" id="Seg_673" s="T39">do-INF</ta>
            <ta e="T41" id="Seg_674" s="T40">NEG</ta>
            <ta e="T42" id="Seg_675" s="T41">can-1SG.S</ta>
            <ta e="T43" id="Seg_676" s="T42">fish-EP-CAP-INF</ta>
            <ta e="T44" id="Seg_677" s="T43">can-1SG.S</ta>
            <ta e="T45" id="Seg_678" s="T44">much-ADV.LOC</ta>
            <ta e="T46" id="Seg_679" s="T45">crucian-PL-ACC</ta>
            <ta e="T47" id="Seg_680" s="T46">much-ADV.LOC</ta>
            <ta e="T48" id="Seg_681" s="T47">catch-HAB-PST-1SG.O</ta>
            <ta e="T49" id="Seg_682" s="T48">you.SG.NOM</ta>
            <ta e="T50" id="Seg_683" s="T49">oneself.2SG</ta>
            <ta e="T51" id="Seg_684" s="T50">see-DRV-PST-2SG.O</ta>
            <ta e="T52" id="Seg_685" s="T51">cook-RES.[3SG.S]</ta>
            <ta e="T53" id="Seg_686" s="T52">NEG</ta>
            <ta e="T54" id="Seg_687" s="T53">cook-RES.[3SG.S]</ta>
            <ta e="T55" id="Seg_688" s="T54">now</ta>
            <ta e="T56" id="Seg_689" s="T55">take-IMP.2SG.O</ta>
            <ta e="T57" id="Seg_690" s="T56">JUSS</ta>
            <ta e="T58" id="Seg_691" s="T57">more</ta>
            <ta e="T59" id="Seg_692" s="T58">boil.[3SG.S]</ta>
            <ta e="T60" id="Seg_693" s="T59">where-EP-ADV.LOC</ta>
            <ta e="T61" id="Seg_694" s="T60">boil-3SG.S</ta>
            <ta e="T62" id="Seg_695" s="T61">leave-CVB</ta>
            <ta e="T63" id="Seg_696" s="T62">Sveta-ACC</ta>
            <ta e="T64" id="Seg_697" s="T63">awake-FUT-1SG.O</ta>
            <ta e="T65" id="Seg_698" s="T64">I.NOM</ta>
            <ta e="T66" id="Seg_699" s="T65">(s)he-EP-ACC</ta>
            <ta e="T67" id="Seg_700" s="T66">late</ta>
            <ta e="T68" id="Seg_701" s="T67">awake-1SG.O</ta>
            <ta e="T69" id="Seg_702" s="T68">(s)he.[NOM]</ta>
            <ta e="T70" id="Seg_703" s="T69">strong-ADVZ</ta>
            <ta e="T71" id="Seg_704" s="T70">sleep.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_705" s="T1">а</ta>
            <ta e="T3" id="Seg_706" s="T2">завтра-ADV.LOC</ta>
            <ta e="T4" id="Seg_707" s="T3">я.NOM</ta>
            <ta e="T5" id="Seg_708" s="T4">что.[NOM]</ta>
            <ta e="T6" id="Seg_709" s="T5">сварить-INFER-FUT-1SG.S</ta>
            <ta e="T7" id="Seg_710" s="T6">ты.NOM</ta>
            <ta e="T8" id="Seg_711" s="T7">я.ALL</ta>
            <ta e="T9" id="Seg_712" s="T8">сказать-HAB-IMP.2SG.O</ta>
            <ta e="T10" id="Seg_713" s="T9">ежели</ta>
            <ta e="T11" id="Seg_714" s="T10">плохой-ADVZ</ta>
            <ta e="T12" id="Seg_715" s="T11">сделать-HAB-1SG.O</ta>
            <ta e="T13" id="Seg_716" s="T12">ты.NOM</ta>
            <ta e="T14" id="Seg_717" s="T13">я.ACC</ta>
            <ta e="T15" id="Seg_718" s="T14">научить-DRV-IMP.2SG.S</ta>
            <ta e="T16" id="Seg_719" s="T15">а.то</ta>
            <ta e="T17" id="Seg_720" s="T16">я.NOM</ta>
            <ta e="T18" id="Seg_721" s="T17">сделать-INF</ta>
            <ta e="T19" id="Seg_722" s="T18">NEG</ta>
            <ta e="T20" id="Seg_723" s="T19">уметь-1SG.S</ta>
            <ta e="T21" id="Seg_724" s="T20">я.NOM</ta>
            <ta e="T22" id="Seg_725" s="T21">деревня-LOC</ta>
            <ta e="T23" id="Seg_726" s="T22">жить-PST-1SG.S</ta>
            <ta e="T24" id="Seg_727" s="T23">мы-ADES</ta>
            <ta e="T25" id="Seg_728" s="T24">только</ta>
            <ta e="T26" id="Seg_729" s="T25">рыба.[NOM]</ta>
            <ta e="T27" id="Seg_730" s="T26">мы.[NOM]</ta>
            <ta e="T28" id="Seg_731" s="T27">он(а)-EP-ACC</ta>
            <ta e="T29" id="Seg_732" s="T28">сварить-HAB-1PL</ta>
            <ta e="T30" id="Seg_733" s="T29">и</ta>
            <ta e="T31" id="Seg_734" s="T30">жарить-HAB-1PL</ta>
            <ta e="T32" id="Seg_735" s="T31">мясо.[NOM]</ta>
            <ta e="T33" id="Seg_736" s="T32">быть-3SG.S</ta>
            <ta e="T34" id="Seg_737" s="T33">я.NOM</ta>
            <ta e="T35" id="Seg_738" s="T34">он(а)-EP-ACC</ta>
            <ta e="T36" id="Seg_739" s="T35">сварить-HAB-1SG.O</ta>
            <ta e="T37" id="Seg_740" s="T36">больше</ta>
            <ta e="T38" id="Seg_741" s="T37">я.NOM</ta>
            <ta e="T39" id="Seg_742" s="T38">что-ACC-EMPH</ta>
            <ta e="T40" id="Seg_743" s="T39">сделать-INF</ta>
            <ta e="T41" id="Seg_744" s="T40">NEG</ta>
            <ta e="T42" id="Seg_745" s="T41">уметь-1SG.S</ta>
            <ta e="T43" id="Seg_746" s="T42">рыба-EP-CAP-INF</ta>
            <ta e="T44" id="Seg_747" s="T43">уметь-1SG.S</ta>
            <ta e="T45" id="Seg_748" s="T44">много-ADV.LOC</ta>
            <ta e="T46" id="Seg_749" s="T45">карась-PL-ACC</ta>
            <ta e="T47" id="Seg_750" s="T46">много-ADV.LOC</ta>
            <ta e="T48" id="Seg_751" s="T47">поймать-HAB-PST-1SG.O</ta>
            <ta e="T49" id="Seg_752" s="T48">ты.NOM</ta>
            <ta e="T50" id="Seg_753" s="T49">сам.2SG</ta>
            <ta e="T51" id="Seg_754" s="T50">увидеть-DRV-PST-2SG.O</ta>
            <ta e="T52" id="Seg_755" s="T51">сварить-RES.[3SG.S]</ta>
            <ta e="T53" id="Seg_756" s="T52">NEG</ta>
            <ta e="T54" id="Seg_757" s="T53">сварить-RES.[3SG.S]</ta>
            <ta e="T55" id="Seg_758" s="T54">ну</ta>
            <ta e="T56" id="Seg_759" s="T55">взять-IMP.2SG.O</ta>
            <ta e="T57" id="Seg_760" s="T56">JUSS</ta>
            <ta e="T58" id="Seg_761" s="T57">еще</ta>
            <ta e="T59" id="Seg_762" s="T58">кипеть.[3SG.S]</ta>
            <ta e="T60" id="Seg_763" s="T59">где-EP-ADV.LOC</ta>
            <ta e="T61" id="Seg_764" s="T60">кипеть-3SG.S</ta>
            <ta e="T62" id="Seg_765" s="T61">отправиться-CVB</ta>
            <ta e="T63" id="Seg_766" s="T62">Света-ACC</ta>
            <ta e="T64" id="Seg_767" s="T63">разбудить-FUT-1SG.O</ta>
            <ta e="T65" id="Seg_768" s="T64">я.NOM</ta>
            <ta e="T66" id="Seg_769" s="T65">он(а)-EP-ACC</ta>
            <ta e="T67" id="Seg_770" s="T66">поздно</ta>
            <ta e="T68" id="Seg_771" s="T67">разбудить-1SG.O</ta>
            <ta e="T69" id="Seg_772" s="T68">он(а).[NOM]</ta>
            <ta e="T70" id="Seg_773" s="T69">крепкий-ADVZ</ta>
            <ta e="T71" id="Seg_774" s="T70">спать.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_775" s="T1">conj</ta>
            <ta e="T3" id="Seg_776" s="T2">adv-adv:case</ta>
            <ta e="T4" id="Seg_777" s="T3">pers</ta>
            <ta e="T5" id="Seg_778" s="T4">interrog.[n:case]</ta>
            <ta e="T6" id="Seg_779" s="T5">v-v:mood-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_780" s="T6">pers</ta>
            <ta e="T8" id="Seg_781" s="T7">pers</ta>
            <ta e="T9" id="Seg_782" s="T8">v-v&gt;v-v:mood.pn</ta>
            <ta e="T10" id="Seg_783" s="T9">conj</ta>
            <ta e="T11" id="Seg_784" s="T10">adj-adj&gt;adv</ta>
            <ta e="T12" id="Seg_785" s="T11">v-v&gt;v-v:pn</ta>
            <ta e="T13" id="Seg_786" s="T12">pers</ta>
            <ta e="T14" id="Seg_787" s="T13">pers</ta>
            <ta e="T15" id="Seg_788" s="T14">v-v&gt;v-v:mood.pn</ta>
            <ta e="T16" id="Seg_789" s="T15">conj</ta>
            <ta e="T17" id="Seg_790" s="T16">pers</ta>
            <ta e="T18" id="Seg_791" s="T17">v-v:inf</ta>
            <ta e="T19" id="Seg_792" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_793" s="T19">v-v:pn</ta>
            <ta e="T21" id="Seg_794" s="T20">pers</ta>
            <ta e="T22" id="Seg_795" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_796" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_797" s="T23">pers-n:case</ta>
            <ta e="T25" id="Seg_798" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_799" s="T25">n.[n:case]</ta>
            <ta e="T27" id="Seg_800" s="T26">pers.[n:case]</ta>
            <ta e="T28" id="Seg_801" s="T27">pers-n:ins-n:case</ta>
            <ta e="T29" id="Seg_802" s="T28">v-v&gt;v-v:pn</ta>
            <ta e="T30" id="Seg_803" s="T29">conj</ta>
            <ta e="T31" id="Seg_804" s="T30">v-v&gt;v-v:pn</ta>
            <ta e="T32" id="Seg_805" s="T31">n.[n:case]</ta>
            <ta e="T33" id="Seg_806" s="T32">v-v:pn</ta>
            <ta e="T34" id="Seg_807" s="T33">pers</ta>
            <ta e="T35" id="Seg_808" s="T34">pers-n:ins-n:case</ta>
            <ta e="T36" id="Seg_809" s="T35">v-v&gt;v-v:pn</ta>
            <ta e="T37" id="Seg_810" s="T36">adv</ta>
            <ta e="T38" id="Seg_811" s="T37">pers</ta>
            <ta e="T39" id="Seg_812" s="T38">interrog-n:case-clit</ta>
            <ta e="T40" id="Seg_813" s="T39">v-v:inf</ta>
            <ta e="T41" id="Seg_814" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_815" s="T41">v-v:pn</ta>
            <ta e="T43" id="Seg_816" s="T42">n-n:ins-n&gt;v-v:inf</ta>
            <ta e="T44" id="Seg_817" s="T43">v-v:pn</ta>
            <ta e="T45" id="Seg_818" s="T44">quant-n&gt;adv</ta>
            <ta e="T46" id="Seg_819" s="T45">n-n:num-n:case</ta>
            <ta e="T47" id="Seg_820" s="T46">quant-adv:case</ta>
            <ta e="T48" id="Seg_821" s="T47">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_822" s="T48">pers</ta>
            <ta e="T50" id="Seg_823" s="T49">emphpro</ta>
            <ta e="T51" id="Seg_824" s="T50">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_825" s="T51">v-v&gt;v.[v:pn]</ta>
            <ta e="T53" id="Seg_826" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_827" s="T53">v-v&gt;v.[v:pn]</ta>
            <ta e="T55" id="Seg_828" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_829" s="T55">v-v:mood.pn</ta>
            <ta e="T57" id="Seg_830" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_831" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_832" s="T58">v.[v:pn]</ta>
            <ta e="T60" id="Seg_833" s="T59">interrog-n:ins-adv:case</ta>
            <ta e="T61" id="Seg_834" s="T60">v-v:pn</ta>
            <ta e="T62" id="Seg_835" s="T61">v-v&gt;adv</ta>
            <ta e="T63" id="Seg_836" s="T62">nprop-n:case</ta>
            <ta e="T64" id="Seg_837" s="T63">v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_838" s="T64">pers</ta>
            <ta e="T66" id="Seg_839" s="T65">pers-n:ins-n:case</ta>
            <ta e="T67" id="Seg_840" s="T66">adv</ta>
            <ta e="T68" id="Seg_841" s="T67">v-v:pn</ta>
            <ta e="T69" id="Seg_842" s="T68">pers.[n:case]</ta>
            <ta e="T70" id="Seg_843" s="T69">adj-adj&gt;adv</ta>
            <ta e="T71" id="Seg_844" s="T70">v.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_845" s="T1">conj</ta>
            <ta e="T3" id="Seg_846" s="T2">adv</ta>
            <ta e="T4" id="Seg_847" s="T3">pers</ta>
            <ta e="T5" id="Seg_848" s="T4">interrog</ta>
            <ta e="T6" id="Seg_849" s="T5">v</ta>
            <ta e="T7" id="Seg_850" s="T6">pers</ta>
            <ta e="T8" id="Seg_851" s="T7">pers</ta>
            <ta e="T9" id="Seg_852" s="T8">v</ta>
            <ta e="T10" id="Seg_853" s="T9">conj</ta>
            <ta e="T11" id="Seg_854" s="T10">adv</ta>
            <ta e="T12" id="Seg_855" s="T11">v</ta>
            <ta e="T13" id="Seg_856" s="T12">pers</ta>
            <ta e="T14" id="Seg_857" s="T13">pers</ta>
            <ta e="T15" id="Seg_858" s="T14">v</ta>
            <ta e="T16" id="Seg_859" s="T15">conj</ta>
            <ta e="T17" id="Seg_860" s="T16">pers</ta>
            <ta e="T18" id="Seg_861" s="T17">v</ta>
            <ta e="T19" id="Seg_862" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_863" s="T19">v</ta>
            <ta e="T21" id="Seg_864" s="T20">pers</ta>
            <ta e="T22" id="Seg_865" s="T21">n</ta>
            <ta e="T23" id="Seg_866" s="T22">v</ta>
            <ta e="T24" id="Seg_867" s="T23">pers</ta>
            <ta e="T25" id="Seg_868" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_869" s="T25">n</ta>
            <ta e="T27" id="Seg_870" s="T26">pers</ta>
            <ta e="T28" id="Seg_871" s="T27">pers</ta>
            <ta e="T29" id="Seg_872" s="T28">v</ta>
            <ta e="T30" id="Seg_873" s="T29">conj</ta>
            <ta e="T31" id="Seg_874" s="T30">v</ta>
            <ta e="T32" id="Seg_875" s="T31">n</ta>
            <ta e="T33" id="Seg_876" s="T32">v</ta>
            <ta e="T34" id="Seg_877" s="T33">pers</ta>
            <ta e="T35" id="Seg_878" s="T34">pers</ta>
            <ta e="T36" id="Seg_879" s="T35">v</ta>
            <ta e="T37" id="Seg_880" s="T36">adv</ta>
            <ta e="T38" id="Seg_881" s="T37">pers</ta>
            <ta e="T39" id="Seg_882" s="T38">pro</ta>
            <ta e="T40" id="Seg_883" s="T39">v</ta>
            <ta e="T41" id="Seg_884" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_885" s="T41">v</ta>
            <ta e="T43" id="Seg_886" s="T42">v</ta>
            <ta e="T44" id="Seg_887" s="T43">v</ta>
            <ta e="T45" id="Seg_888" s="T44">adv</ta>
            <ta e="T46" id="Seg_889" s="T45">n</ta>
            <ta e="T47" id="Seg_890" s="T46">quant</ta>
            <ta e="T48" id="Seg_891" s="T47">v</ta>
            <ta e="T49" id="Seg_892" s="T48">pers</ta>
            <ta e="T50" id="Seg_893" s="T49">emphpro</ta>
            <ta e="T51" id="Seg_894" s="T50">v</ta>
            <ta e="T52" id="Seg_895" s="T51">v</ta>
            <ta e="T53" id="Seg_896" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_897" s="T53">v</ta>
            <ta e="T55" id="Seg_898" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_899" s="T55">v</ta>
            <ta e="T57" id="Seg_900" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_901" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_902" s="T58">v</ta>
            <ta e="T60" id="Seg_903" s="T59">interrog</ta>
            <ta e="T61" id="Seg_904" s="T60">v</ta>
            <ta e="T62" id="Seg_905" s="T61">adv</ta>
            <ta e="T63" id="Seg_906" s="T62">nprop</ta>
            <ta e="T64" id="Seg_907" s="T63">v</ta>
            <ta e="T65" id="Seg_908" s="T64">pers</ta>
            <ta e="T66" id="Seg_909" s="T65">pers</ta>
            <ta e="T67" id="Seg_910" s="T66">adv</ta>
            <ta e="T68" id="Seg_911" s="T67">v</ta>
            <ta e="T69" id="Seg_912" s="T68">pers</ta>
            <ta e="T70" id="Seg_913" s="T69">adv</ta>
            <ta e="T71" id="Seg_914" s="T70">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_915" s="T2">adv:Time</ta>
            <ta e="T4" id="Seg_916" s="T3">pro.h:A</ta>
            <ta e="T7" id="Seg_917" s="T6">pro.h:A</ta>
            <ta e="T8" id="Seg_918" s="T7">pro.h:R</ta>
            <ta e="T12" id="Seg_919" s="T11">0.1.h:A</ta>
            <ta e="T13" id="Seg_920" s="T12">pro.h:A</ta>
            <ta e="T14" id="Seg_921" s="T13">pro.h:Th</ta>
            <ta e="T17" id="Seg_922" s="T16">pro.h:A</ta>
            <ta e="T18" id="Seg_923" s="T17">v:Th</ta>
            <ta e="T21" id="Seg_924" s="T20">pro.h:Th</ta>
            <ta e="T22" id="Seg_925" s="T21">np:L</ta>
            <ta e="T24" id="Seg_926" s="T23">pro.h:Poss</ta>
            <ta e="T26" id="Seg_927" s="T25">np:Th</ta>
            <ta e="T27" id="Seg_928" s="T26">pro.h:A</ta>
            <ta e="T28" id="Seg_929" s="T27">pro:P</ta>
            <ta e="T31" id="Seg_930" s="T30">0.1.h:A </ta>
            <ta e="T32" id="Seg_931" s="T31">np:Th</ta>
            <ta e="T34" id="Seg_932" s="T33">pro.h:A</ta>
            <ta e="T35" id="Seg_933" s="T34">pro:P</ta>
            <ta e="T38" id="Seg_934" s="T37">pro.h:A</ta>
            <ta e="T39" id="Seg_935" s="T38">pro:P</ta>
            <ta e="T40" id="Seg_936" s="T39">v:Th</ta>
            <ta e="T43" id="Seg_937" s="T42">v:Th</ta>
            <ta e="T44" id="Seg_938" s="T43">0.1.h:A</ta>
            <ta e="T46" id="Seg_939" s="T45">np:P</ta>
            <ta e="T48" id="Seg_940" s="T47">0.1.h:A</ta>
            <ta e="T49" id="Seg_941" s="T48">pro.h:E</ta>
            <ta e="T51" id="Seg_942" s="T50">0.3:Th</ta>
            <ta e="T52" id="Seg_943" s="T51">0.3:P</ta>
            <ta e="T54" id="Seg_944" s="T53">0.3:P</ta>
            <ta e="T56" id="Seg_945" s="T55">0.2.h:A 0.3:Th</ta>
            <ta e="T59" id="Seg_946" s="T58">0.3:P</ta>
            <ta e="T61" id="Seg_947" s="T60">0.3:P</ta>
            <ta e="T63" id="Seg_948" s="T62">np.h:P</ta>
            <ta e="T64" id="Seg_949" s="T63">0.1.h:A</ta>
            <ta e="T65" id="Seg_950" s="T64">pro.h:A</ta>
            <ta e="T66" id="Seg_951" s="T65">pro.h:P</ta>
            <ta e="T69" id="Seg_952" s="T68">pro.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_953" s="T3">pro.h:S</ta>
            <ta e="T6" id="Seg_954" s="T5">v:pred</ta>
            <ta e="T7" id="Seg_955" s="T6">pro.h:S</ta>
            <ta e="T9" id="Seg_956" s="T8">v:pred</ta>
            <ta e="T12" id="Seg_957" s="T9">s:cond</ta>
            <ta e="T13" id="Seg_958" s="T12">pro.h:S</ta>
            <ta e="T14" id="Seg_959" s="T13">pro.h:O</ta>
            <ta e="T15" id="Seg_960" s="T14">v:pred</ta>
            <ta e="T17" id="Seg_961" s="T16">pro.h:S</ta>
            <ta e="T18" id="Seg_962" s="T17">v:O</ta>
            <ta e="T20" id="Seg_963" s="T19">v:pred</ta>
            <ta e="T21" id="Seg_964" s="T20">pro.h:S</ta>
            <ta e="T23" id="Seg_965" s="T22">v:pred</ta>
            <ta e="T26" id="Seg_966" s="T25">np:S</ta>
            <ta e="T27" id="Seg_967" s="T26">pro.h:S</ta>
            <ta e="T28" id="Seg_968" s="T27">pro:O</ta>
            <ta e="T29" id="Seg_969" s="T28">v:pred</ta>
            <ta e="T31" id="Seg_970" s="T30">0.1.h:S v:pred </ta>
            <ta e="T32" id="Seg_971" s="T31">np:S</ta>
            <ta e="T33" id="Seg_972" s="T32">v:pred</ta>
            <ta e="T34" id="Seg_973" s="T33">pro.h:S</ta>
            <ta e="T35" id="Seg_974" s="T34">pro:O</ta>
            <ta e="T36" id="Seg_975" s="T35">v:pred</ta>
            <ta e="T38" id="Seg_976" s="T37">pro.h:S</ta>
            <ta e="T40" id="Seg_977" s="T39">v:O</ta>
            <ta e="T42" id="Seg_978" s="T41">v:pred</ta>
            <ta e="T43" id="Seg_979" s="T42">v:O</ta>
            <ta e="T44" id="Seg_980" s="T43">0.1.h:S v:pred</ta>
            <ta e="T46" id="Seg_981" s="T45">np:O</ta>
            <ta e="T48" id="Seg_982" s="T47">0.1.h:S v:pred</ta>
            <ta e="T49" id="Seg_983" s="T48">pro.h:S</ta>
            <ta e="T51" id="Seg_984" s="T50">v:pred 0.3:O</ta>
            <ta e="T52" id="Seg_985" s="T51">0.3:S v:pred</ta>
            <ta e="T54" id="Seg_986" s="T53">0.3:S v:pred</ta>
            <ta e="T56" id="Seg_987" s="T55">0.2.h:S v:pred 0.3:O</ta>
            <ta e="T59" id="Seg_988" s="T58">0.3:S v:pred</ta>
            <ta e="T61" id="Seg_989" s="T60">0.3:S v:pred</ta>
            <ta e="T62" id="Seg_990" s="T61">s:temp</ta>
            <ta e="T63" id="Seg_991" s="T62">np.h:O</ta>
            <ta e="T64" id="Seg_992" s="T63">0.1.h:S v:pred</ta>
            <ta e="T65" id="Seg_993" s="T64">pro.h:S</ta>
            <ta e="T66" id="Seg_994" s="T65">pro.h:O</ta>
            <ta e="T68" id="Seg_995" s="T67">v:pred</ta>
            <ta e="T69" id="Seg_996" s="T68">pro.h:S</ta>
            <ta e="T71" id="Seg_997" s="T70">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_998" s="T1">RUS:gram</ta>
            <ta e="T10" id="Seg_999" s="T9">RUS:gram</ta>
            <ta e="T16" id="Seg_1000" s="T15">RUS:gram</ta>
            <ta e="T25" id="Seg_1001" s="T24">RUS:disc</ta>
            <ta e="T30" id="Seg_1002" s="T29">RUS:gram</ta>
            <ta e="T31" id="Seg_1003" s="T30">RUS:cult</ta>
            <ta e="T37" id="Seg_1004" s="T36">RUS:core</ta>
            <ta e="T55" id="Seg_1005" s="T54">RUS:disc</ta>
            <ta e="T57" id="Seg_1006" s="T56">RUS:gram</ta>
            <ta e="T58" id="Seg_1007" s="T57">RUS:disc</ta>
            <ta e="T63" id="Seg_1008" s="T62">RUS:cult</ta>
            <ta e="T67" id="Seg_1009" s="T66">RUS:core</ta>
            <ta e="T70" id="Seg_1010" s="T69">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_1011" s="T1">What shall I cook tomorrow?</ta>
            <ta e="T9" id="Seg_1012" s="T6">You tell me.</ta>
            <ta e="T15" id="Seg_1013" s="T9">If I make something wrong, teach me.</ta>
            <ta e="T20" id="Seg_1014" s="T15">Because I don't know how to do things.</ta>
            <ta e="T23" id="Seg_1015" s="T20">I lived in a village.</ta>
            <ta e="T26" id="Seg_1016" s="T23">We only have fish.</ta>
            <ta e="T31" id="Seg_1017" s="T26">We boil and fry it.</ta>
            <ta e="T36" id="Seg_1018" s="T31">There is meat, I'll cook it.</ta>
            <ta e="T42" id="Seg_1019" s="T36">I can't do anything else.</ta>
            <ta e="T44" id="Seg_1020" s="T42">I can fish.</ta>
            <ta e="T48" id="Seg_1021" s="T44">I used to catch crucians.</ta>
            <ta e="T51" id="Seg_1022" s="T48">You saw it yourself.</ta>
            <ta e="T52" id="Seg_1023" s="T51">Is it ready?</ta>
            <ta e="T53" id="Seg_1024" s="T52">No-</ta>
            <ta e="T54" id="Seg_1025" s="T53">Is it ready?</ta>
            <ta e="T56" id="Seg_1026" s="T54">Well, take it [from the fire].</ta>
            <ta e="T61" id="Seg_1027" s="T56">Let it boil a bit more (where is it boiling?).</ta>
            <ta e="T64" id="Seg_1028" s="T61">I'll go and wake up Sveta.</ta>
            <ta e="T68" id="Seg_1029" s="T64">I woke her up late.</ta>
            <ta e="T71" id="Seg_1030" s="T68">She is deep asleep.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_1031" s="T1">Was soll ich morgen kochen?</ta>
            <ta e="T9" id="Seg_1032" s="T6">Sag du es mir.</ta>
            <ta e="T15" id="Seg_1033" s="T9">Wenn ich etwas falsch mache, bring es mir bei.</ta>
            <ta e="T20" id="Seg_1034" s="T15">Denn ich kann es nicht machen.</ta>
            <ta e="T23" id="Seg_1035" s="T20">Ich lebte in einem Dorf.</ta>
            <ta e="T26" id="Seg_1036" s="T23">Wir hatten nur Fisch.</ta>
            <ta e="T31" id="Seg_1037" s="T26">Wir kochen und braten ihn.</ta>
            <ta e="T36" id="Seg_1038" s="T31">Es gibt Fleisch, ich koche es.</ta>
            <ta e="T42" id="Seg_1039" s="T36">Ich kann nichts anderes (machen).</ta>
            <ta e="T44" id="Seg_1040" s="T42">Ich kann fischen.</ta>
            <ta e="T48" id="Seg_1041" s="T44">Ich habe regelmäßig Karauschen gefangen.</ta>
            <ta e="T51" id="Seg_1042" s="T48">Du hast es selbst gesehen.</ta>
            <ta e="T52" id="Seg_1043" s="T51">Ist es fertig?</ta>
            <ta e="T53" id="Seg_1044" s="T52">Nein.</ta>
            <ta e="T54" id="Seg_1045" s="T53">Ist es fertig?</ta>
            <ta e="T56" id="Seg_1046" s="T54">Gut, nimm es [vom Feuer].</ta>
            <ta e="T61" id="Seg_1047" s="T56">Lass es noch ein wenig kochen (wo kocht es?)</ta>
            <ta e="T64" id="Seg_1048" s="T61">Ich gehe und wecke Sveta.</ta>
            <ta e="T68" id="Seg_1049" s="T64">Ich habe sie spät geweckt.</ta>
            <ta e="T71" id="Seg_1050" s="T68">Sie schläft tief.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_1051" s="T1">А завтра я что сварю?</ta>
            <ta e="T9" id="Seg_1052" s="T6">Ты мне говори.</ta>
            <ta e="T15" id="Seg_1053" s="T9">Если я плохо делаю, ты меня учи.</ta>
            <ta e="T20" id="Seg_1054" s="T15">А то я делать ничего не умею.</ta>
            <ta e="T23" id="Seg_1055" s="T20">Я в деревне жила.</ta>
            <ta e="T26" id="Seg_1056" s="T23">У нас только рыба.</ta>
            <ta e="T31" id="Seg_1057" s="T26">Мы ее (рыбу) жарим и варим.</ta>
            <ta e="T36" id="Seg_1058" s="T31">Мясо есть, я его варю.</ta>
            <ta e="T42" id="Seg_1059" s="T36">Больше я ничего не умею делать.</ta>
            <ta e="T44" id="Seg_1060" s="T42">Промышлять умею.</ta>
            <ta e="T48" id="Seg_1061" s="T44">Карасей много ловила.</ta>
            <ta e="T51" id="Seg_1062" s="T48">Ты сама видела.</ta>
            <ta e="T52" id="Seg_1063" s="T51">Сварилась?</ta>
            <ta e="T53" id="Seg_1064" s="T52">Нет.</ta>
            <ta e="T54" id="Seg_1065" s="T53">Сварилось?</ta>
            <ta e="T56" id="Seg_1066" s="T54">Ну снимай.</ta>
            <ta e="T61" id="Seg_1067" s="T56">Пусть еще покипит, (где кипит?).</ta>
            <ta e="T64" id="Seg_1068" s="T61">Пойду Свету разбужу.</ta>
            <ta e="T68" id="Seg_1069" s="T64">Я ее поздно разбудила.</ta>
            <ta e="T71" id="Seg_1070" s="T68">Она крепко спит.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_1071" s="T1">завтра я кого (что) сварю</ta>
            <ta e="T9" id="Seg_1072" s="T6">ты мне говори</ta>
            <ta e="T15" id="Seg_1073" s="T9">если я плохо делаю ты меня учи</ta>
            <ta e="T20" id="Seg_1074" s="T15">а то я делать ничего не умею</ta>
            <ta e="T23" id="Seg_1075" s="T20">я в деревне жила</ta>
            <ta e="T26" id="Seg_1076" s="T23">у нас только рыба</ta>
            <ta e="T31" id="Seg_1077" s="T26">мы ее (рыбу) жарим и варим</ta>
            <ta e="T36" id="Seg_1078" s="T31">мясо есть я его варю</ta>
            <ta e="T42" id="Seg_1079" s="T36">больше я ничего не умею делать</ta>
            <ta e="T44" id="Seg_1080" s="T42">промышлять умею</ta>
            <ta e="T48" id="Seg_1081" s="T44">карасей много добывала</ta>
            <ta e="T51" id="Seg_1082" s="T48">ты сама видела</ta>
            <ta e="T52" id="Seg_1083" s="T51">сварилась</ta>
            <ta e="T53" id="Seg_1084" s="T52">нет</ta>
            <ta e="T54" id="Seg_1085" s="T53">сварилось</ta>
            <ta e="T56" id="Seg_1086" s="T54">ну снимай</ta>
            <ta e="T61" id="Seg_1087" s="T56">пусть еще покипит</ta>
            <ta e="T64" id="Seg_1088" s="T61">пойду Свету будить</ta>
            <ta e="T68" id="Seg_1089" s="T64">я ее поздно разбудила</ta>
            <ta e="T71" id="Seg_1090" s="T68">она крепко спит</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T6" id="Seg_1091" s="T1">[KuAI:] Variant: 'boːčʼitan'. [BrM:] Tentative analysis of 'poːčʼitan'.</ta>
            <ta e="T20" id="Seg_1092" s="T15">[KuAI:] Variant: 'aːstunan'. [BrM:] 'aːstonan' changed to 'aːs tonan'.</ta>
            <ta e="T23" id="Seg_1093" s="T20">[KuAI:] Variant: 'elɨzan'.</ta>
            <ta e="T31" id="Seg_1094" s="T26">[KuAI:] Variant: 'boːtkoftə'.</ta>
            <ta e="T42" id="Seg_1095" s="T36">[BrM:] 'astonan' changed to 'as tonan'.</ta>
            <ta e="T44" id="Seg_1096" s="T42">[KuAI:] Variant: 'Qwɛlɨsku'.</ta>
            <ta e="T51" id="Seg_1097" s="T48"> ‎‎[KuAI:] Variant: 'oneɛtdə'.</ta>
            <ta e="T52" id="Seg_1098" s="T51">[BrM:] Another fragment starts.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
