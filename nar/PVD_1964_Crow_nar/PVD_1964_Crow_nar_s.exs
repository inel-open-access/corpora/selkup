<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Crow_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Crow_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">28</ud-information>
            <ud-information attribute-name="# HIAT:w">19</ud-information>
            <ud-information attribute-name="# e">19</ud-information>
            <ud-information attribute-name="# HIAT:u">3</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T20" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Qwɨrʼä</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">as</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">wazʼedʼikun</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_12" n="HIAT:ip">(</nts>
                  <ts e="T5" id="Seg_14" n="HIAT:w" s="T4">pʼötta</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_17" n="HIAT:w" s="T5">qadarta</ts>
                  <nts id="Seg_18" n="HIAT:ip">)</nts>
                  <nts id="Seg_19" n="HIAT:ip">.</nts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_22" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_24" n="HIAT:w" s="T6">Qwɛrʼila</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_27" n="HIAT:w" s="T7">tʼütǯattə</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">aprʼelʼiɣɨn</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_33" n="HIAT:w" s="T9">i</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_36" n="HIAT:w" s="T10">tʼekkəlaː</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_39" n="HIAT:w" s="T11">tʼütǯattə</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_41" n="HIAT:ip">(</nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">tʼünnɨtǯattə</ts>
                  <nts id="Seg_44" n="HIAT:ip">)</nts>
                  <nts id="Seg_45" n="HIAT:ip">,</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">i</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_51" n="HIAT:w" s="T14">säɣaboɣala</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_54" n="HIAT:w" s="T15">tʼünnɨtǯattə</ts>
                  <nts id="Seg_55" n="HIAT:ip">.</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_58" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">Tebla</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_63" n="HIAT:w" s="T17">čaʒeːkan</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_66" n="HIAT:w" s="T18">jewawattə</ts>
                  <nts id="Seg_67" n="HIAT:ip">,</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">Šikargaɣɨn</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T20" id="Seg_73" n="sc" s="T1">
               <ts e="T2" id="Seg_75" n="e" s="T1">Qwɨrʼä </ts>
               <ts e="T3" id="Seg_77" n="e" s="T2">as </ts>
               <ts e="T4" id="Seg_79" n="e" s="T3">wazʼedʼikun </ts>
               <ts e="T5" id="Seg_81" n="e" s="T4">(pʼötta </ts>
               <ts e="T6" id="Seg_83" n="e" s="T5">qadarta). </ts>
               <ts e="T7" id="Seg_85" n="e" s="T6">Qwɛrʼila </ts>
               <ts e="T8" id="Seg_87" n="e" s="T7">tʼütǯattə </ts>
               <ts e="T9" id="Seg_89" n="e" s="T8">aprʼelʼiɣɨn </ts>
               <ts e="T10" id="Seg_91" n="e" s="T9">i </ts>
               <ts e="T11" id="Seg_93" n="e" s="T10">tʼekkəlaː </ts>
               <ts e="T12" id="Seg_95" n="e" s="T11">tʼütǯattə </ts>
               <ts e="T13" id="Seg_97" n="e" s="T12">(tʼünnɨtǯattə), </ts>
               <ts e="T14" id="Seg_99" n="e" s="T13">i </ts>
               <ts e="T15" id="Seg_101" n="e" s="T14">säɣaboɣala </ts>
               <ts e="T16" id="Seg_103" n="e" s="T15">tʼünnɨtǯattə. </ts>
               <ts e="T17" id="Seg_105" n="e" s="T16">Tebla </ts>
               <ts e="T18" id="Seg_107" n="e" s="T17">čaʒeːkan </ts>
               <ts e="T19" id="Seg_109" n="e" s="T18">jewawattə, </ts>
               <ts e="T20" id="Seg_111" n="e" s="T19">Šikargaɣɨn. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_112" s="T1">PVD_1964_Crow_nar.001 (001.001)</ta>
            <ta e="T16" id="Seg_113" s="T6">PVD_1964_Crow_nar.002 (001.002)</ta>
            <ta e="T20" id="Seg_114" s="T16">PVD_1964_Crow_nar.003 (001.003)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_115" s="T1">′kwырʼӓ ас wа′зʼедʼикун (пʼӧтта ′kадарта).</ta>
            <ta e="T16" id="Seg_116" s="T6">′kwɛрʼила ′тʼӱтджаттъ ап′рʼелʼиɣын и тʼеккъ′ла̄ тʼӱтджаттъ (тʼӱннытджаттъ), и ′сӓɣа′боɣа′ла ′тʼӱннытджатта̹(ъ).</ta>
            <ta e="T20" id="Seg_117" s="T16">тебла ′тша′же̄кан jеwаwаттъ, Ши′кар′гаɣын.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_118" s="T1">qwɨrʼä as wazʼedʼikun (pʼötta qadarta).</ta>
            <ta e="T16" id="Seg_119" s="T6">qwɛrʼila tʼütǯattə aprʼelʼiɣɨn i tʼekkəlaː tʼütǯattə (tʼünnɨtǯattə), i säɣaboɣala tʼünnɨtǯatta̹(ə).</ta>
            <ta e="T20" id="Seg_120" s="T16">tebla tšaʒeːkan jewawattə, Шikargaɣɨn.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_121" s="T1">Qwɨrʼä as wazʼedʼikun (pʼötta qadarta). </ta>
            <ta e="T16" id="Seg_122" s="T6">Qwɛrʼila tʼütǯattə aprʼelʼiɣɨn i tʼekkəlaː tʼütǯattə (tʼünnɨtǯattə), i säɣaboɣala tʼünnɨtǯattə. </ta>
            <ta e="T20" id="Seg_123" s="T16">Tebla čaʒeːkan jewawattə, Šikargaɣɨn. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_124" s="T1">qwɨrʼä</ta>
            <ta e="T3" id="Seg_125" s="T2">as</ta>
            <ta e="T4" id="Seg_126" s="T3">wazʼe-dʼi-ku-n</ta>
            <ta e="T5" id="Seg_127" s="T4">pʼötta</ta>
            <ta e="T6" id="Seg_128" s="T5">qadar-ta</ta>
            <ta e="T7" id="Seg_129" s="T6">qwɛrʼi-la</ta>
            <ta e="T8" id="Seg_130" s="T7">tʼü-tǯa-ttə</ta>
            <ta e="T9" id="Seg_131" s="T8">aprʼelʼ-i-ɣɨn</ta>
            <ta e="T10" id="Seg_132" s="T9">i</ta>
            <ta e="T11" id="Seg_133" s="T10">tʼekkə-laː</ta>
            <ta e="T12" id="Seg_134" s="T11">tʼü-tǯa-ttə</ta>
            <ta e="T13" id="Seg_135" s="T12">tʼü-nnɨ-tǯa-ttə</ta>
            <ta e="T14" id="Seg_136" s="T13">i</ta>
            <ta e="T15" id="Seg_137" s="T14">säɣaboɣa-la</ta>
            <ta e="T16" id="Seg_138" s="T15">tʼü-nnɨ-tǯa-ttə</ta>
            <ta e="T17" id="Seg_139" s="T16">teb-la</ta>
            <ta e="T18" id="Seg_140" s="T17">čaʒeːk-a-n</ta>
            <ta e="T19" id="Seg_141" s="T18">jewa-wa-ttə</ta>
            <ta e="T20" id="Seg_142" s="T19">Šikarga-ɣɨn</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_143" s="T1">qwərä</ta>
            <ta e="T3" id="Seg_144" s="T2">asa</ta>
            <ta e="T4" id="Seg_145" s="T3">wazʼe-dʼi-ku-n</ta>
            <ta e="T5" id="Seg_146" s="T4">pʼötta</ta>
            <ta e="T6" id="Seg_147" s="T5">kadar-ntə</ta>
            <ta e="T7" id="Seg_148" s="T6">qwərä-la</ta>
            <ta e="T8" id="Seg_149" s="T7">tüː-enǯɨ-tɨn</ta>
            <ta e="T9" id="Seg_150" s="T8">aprʼelʼ-ɨ-qɨn</ta>
            <ta e="T10" id="Seg_151" s="T9">i</ta>
            <ta e="T11" id="Seg_152" s="T10">čeŋgə-la</ta>
            <ta e="T12" id="Seg_153" s="T11">tüː-enǯɨ-tɨn</ta>
            <ta e="T13" id="Seg_154" s="T12">tüː-ntɨ-enǯɨ-tɨn</ta>
            <ta e="T14" id="Seg_155" s="T13">i</ta>
            <ta e="T15" id="Seg_156" s="T14">seɣaboɣa-la</ta>
            <ta e="T16" id="Seg_157" s="T15">tüː-ntɨ-enǯɨ-tɨn</ta>
            <ta e="T17" id="Seg_158" s="T16">täp-la</ta>
            <ta e="T18" id="Seg_159" s="T17">čaček-ɨ-n</ta>
            <ta e="T19" id="Seg_160" s="T18">elɨ-nɨ-tɨn</ta>
            <ta e="T20" id="Seg_161" s="T19">Šikarga-qɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_162" s="T1">crow.[NOM]</ta>
            <ta e="T3" id="Seg_163" s="T2">NEG</ta>
            <ta e="T4" id="Seg_164" s="T3">fly-DRV-HAB-3SG.S</ta>
            <ta e="T5" id="Seg_165" s="T4">hot</ta>
            <ta e="T6" id="Seg_166" s="T5">side-ILL</ta>
            <ta e="T7" id="Seg_167" s="T6">crow-PL.[NOM]</ta>
            <ta e="T8" id="Seg_168" s="T7">come.flying-FUT-3PL</ta>
            <ta e="T9" id="Seg_169" s="T8">April-EP-LOC</ta>
            <ta e="T10" id="Seg_170" s="T9">and</ta>
            <ta e="T11" id="Seg_171" s="T10">swan-PL.[NOM]</ta>
            <ta e="T12" id="Seg_172" s="T11">come.flying-FUT-3PL</ta>
            <ta e="T13" id="Seg_173" s="T12">come.flying-INFER-FUT-3PL</ta>
            <ta e="T14" id="Seg_174" s="T13">and</ta>
            <ta e="T15" id="Seg_175" s="T14">starling-PL.[NOM]</ta>
            <ta e="T16" id="Seg_176" s="T15">come.flying-INFER-FUT-3PL</ta>
            <ta e="T17" id="Seg_177" s="T16">(s)he-PL.[NOM]</ta>
            <ta e="T18" id="Seg_178" s="T17">near-EP-ADV.LOC</ta>
            <ta e="T19" id="Seg_179" s="T18">live-CO-3PL</ta>
            <ta e="T20" id="Seg_180" s="T19">Shegarka-LOC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_181" s="T1">ворона.[NOM]</ta>
            <ta e="T3" id="Seg_182" s="T2">NEG</ta>
            <ta e="T4" id="Seg_183" s="T3">полететь-DRV-HAB-3SG.S</ta>
            <ta e="T5" id="Seg_184" s="T4">горячий</ta>
            <ta e="T6" id="Seg_185" s="T5">сторона-ILL</ta>
            <ta e="T7" id="Seg_186" s="T6">ворона-PL.[NOM]</ta>
            <ta e="T8" id="Seg_187" s="T7">прилететь-FUT-3PL</ta>
            <ta e="T9" id="Seg_188" s="T8">апрель-EP-LOC</ta>
            <ta e="T10" id="Seg_189" s="T9">и</ta>
            <ta e="T11" id="Seg_190" s="T10">лебедь-PL.[NOM]</ta>
            <ta e="T12" id="Seg_191" s="T11">прилететь-FUT-3PL</ta>
            <ta e="T13" id="Seg_192" s="T12">прилететь-INFER-FUT-3PL</ta>
            <ta e="T14" id="Seg_193" s="T13">и</ta>
            <ta e="T15" id="Seg_194" s="T14">скворец-PL.[NOM]</ta>
            <ta e="T16" id="Seg_195" s="T15">прилететь-INFER-FUT-3PL</ta>
            <ta e="T17" id="Seg_196" s="T16">он(а)-PL.[NOM]</ta>
            <ta e="T18" id="Seg_197" s="T17">близко-EP-ADV.LOC</ta>
            <ta e="T19" id="Seg_198" s="T18">жить-CO-3PL</ta>
            <ta e="T20" id="Seg_199" s="T19">Шегарка-LOC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_200" s="T1">n.[n:case]</ta>
            <ta e="T3" id="Seg_201" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_202" s="T3">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T5" id="Seg_203" s="T4">adj</ta>
            <ta e="T6" id="Seg_204" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_205" s="T6">n-n:num.[n:case]</ta>
            <ta e="T8" id="Seg_206" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_207" s="T8">n-n:ins-n:case</ta>
            <ta e="T10" id="Seg_208" s="T9">conj</ta>
            <ta e="T11" id="Seg_209" s="T10">n-n:num.[n:case]</ta>
            <ta e="T12" id="Seg_210" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_211" s="T12">v-v:mood-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_212" s="T13">conj</ta>
            <ta e="T15" id="Seg_213" s="T14">n-n:num.[n:case]</ta>
            <ta e="T16" id="Seg_214" s="T15">v-v:mood-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_215" s="T16">pers-n:num.[n:case]</ta>
            <ta e="T18" id="Seg_216" s="T17">adv-n:ins-adv:case</ta>
            <ta e="T19" id="Seg_217" s="T18">v-v:ins-v:pn</ta>
            <ta e="T20" id="Seg_218" s="T19">nprop-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_219" s="T1">n</ta>
            <ta e="T3" id="Seg_220" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_221" s="T3">v</ta>
            <ta e="T5" id="Seg_222" s="T4">adj</ta>
            <ta e="T6" id="Seg_223" s="T5">n</ta>
            <ta e="T7" id="Seg_224" s="T6">n</ta>
            <ta e="T8" id="Seg_225" s="T7">v</ta>
            <ta e="T9" id="Seg_226" s="T8">n</ta>
            <ta e="T10" id="Seg_227" s="T9">conj</ta>
            <ta e="T11" id="Seg_228" s="T10">v</ta>
            <ta e="T12" id="Seg_229" s="T11">v</ta>
            <ta e="T13" id="Seg_230" s="T12">v</ta>
            <ta e="T14" id="Seg_231" s="T13">conj</ta>
            <ta e="T15" id="Seg_232" s="T14">n</ta>
            <ta e="T16" id="Seg_233" s="T15">v</ta>
            <ta e="T17" id="Seg_234" s="T16">pers</ta>
            <ta e="T18" id="Seg_235" s="T17">adv</ta>
            <ta e="T19" id="Seg_236" s="T18">v</ta>
            <ta e="T20" id="Seg_237" s="T19">nprop</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_238" s="T1">np:A</ta>
            <ta e="T6" id="Seg_239" s="T5">np:G</ta>
            <ta e="T7" id="Seg_240" s="T6">np:A</ta>
            <ta e="T9" id="Seg_241" s="T8">np:Time</ta>
            <ta e="T11" id="Seg_242" s="T10">np:A</ta>
            <ta e="T15" id="Seg_243" s="T14">np:A</ta>
            <ta e="T17" id="Seg_244" s="T16">pro:Th</ta>
            <ta e="T18" id="Seg_245" s="T17">adv:L</ta>
            <ta e="T20" id="Seg_246" s="T19">np:L</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_247" s="T1">np:S</ta>
            <ta e="T4" id="Seg_248" s="T3">v:pred</ta>
            <ta e="T7" id="Seg_249" s="T6">np:S</ta>
            <ta e="T8" id="Seg_250" s="T7">v:pred</ta>
            <ta e="T11" id="Seg_251" s="T10">np:S</ta>
            <ta e="T12" id="Seg_252" s="T11">v:pred</ta>
            <ta e="T13" id="Seg_253" s="T12">0.3:S v:pred</ta>
            <ta e="T15" id="Seg_254" s="T14">np:S</ta>
            <ta e="T16" id="Seg_255" s="T15">v:pred</ta>
            <ta e="T17" id="Seg_256" s="T16">pro:S</ta>
            <ta e="T19" id="Seg_257" s="T18">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T6" id="Seg_258" s="T5">TURK:core</ta>
            <ta e="T9" id="Seg_259" s="T8">RUS:cult</ta>
            <ta e="T10" id="Seg_260" s="T9">RUS:gram</ta>
            <ta e="T14" id="Seg_261" s="T13">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T9" id="Seg_262" s="T8">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_263" s="T1">A crow doesn't fly (to the warm lands).</ta>
            <ta e="T16" id="Seg_264" s="T6">Crows will come here in April, and swans will come, and starlings will come.</ta>
            <ta e="T20" id="Seg_265" s="T16">They live nearby, in Shegarka.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_266" s="T1">Eine Krähe fliegt nicht in warme Länder.</ta>
            <ta e="T16" id="Seg_267" s="T6">Krähen kommen hierher im April und die Schwäne werden kommen und die Stare werden kommen.</ta>
            <ta e="T20" id="Seg_268" s="T16">Sie leben in der Nähe, in Schegarka.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_269" s="T1">Ворона не летает (в теплые края).</ta>
            <ta e="T16" id="Seg_270" s="T6">Вороны прилетят в апреле, и лебеди прилетят, и скворцы прилетят.</ta>
            <ta e="T20" id="Seg_271" s="T16">Они близко живут, в Шегарке.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_272" s="T1">ворона не летает (в теплые края)</ta>
            <ta e="T16" id="Seg_273" s="T6">вороны прилетят в апреле лебеди прилетят и скворцы прилетят</ta>
            <ta e="T20" id="Seg_274" s="T16">они близко (живут) в Шигарке</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto">
            <ta e="T6" id="Seg_275" s="T1">на теплую сторону</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
