<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_DarkSkinnedPeople_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_DarkSkinnedPeople_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">66</ud-information>
            <ud-information attribute-name="# HIAT:w">48</ud-information>
            <ud-information attribute-name="# e">48</ud-information>
            <ud-information attribute-name="# HIAT:u">10</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T49" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">telʼdʼän</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">qoǯirsau</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">täbeɣum</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">šeːɣadʼiga</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_20" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">Man</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">täerbɨzan</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">sɨgan</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">ass</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_35" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">Täp</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">ass</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">sɨgan</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_47" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">A</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">pajat</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">nawerno</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">näj</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">šäɣa</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_65" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">Man</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">qoǯirsau</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_73" n="HIAT:w" s="T20">kitajzlam</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">i</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">karʼejzlam</ts>
                  <nts id="Seg_80" n="HIAT:ip">.</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_83" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">Täbla</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_88" n="HIAT:w" s="T24">šäɣən</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_91" n="HIAT:w" s="T25">jewattə</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_95" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">Ollattə</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_100" n="HIAT:w" s="T27">kruglan</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_103" n="HIAT:w" s="T28">az</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_106" n="HIAT:w" s="T29">jewattə</ts>
                  <nts id="Seg_107" n="HIAT:ip">,</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">tʼüpbokon</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">jewattə</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_117" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">Qɨbanʼäʒa</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">tʼelɨmgun</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">wes</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">šäɣən</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_132" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_134" n="HIAT:w" s="T36">A</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_137" n="HIAT:w" s="T37">man</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_140" n="HIAT:w" s="T38">soɣɨdʼän</ts>
                  <nts id="Seg_141" n="HIAT:ip">:</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_143" n="HIAT:ip">“</nts>
                  <ts e="T40" id="Seg_145" n="HIAT:w" s="T39">Qajɣɨn</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_148" n="HIAT:w" s="T40">qwatpat</ts>
                  <nts id="Seg_149" n="HIAT:ip">,</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_152" n="HIAT:w" s="T41">qaj</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_155" n="HIAT:w" s="T42">pɨgɨlɨppa</ts>
                  <nts id="Seg_156" n="HIAT:ip">?</nts>
                  <nts id="Seg_157" n="HIAT:ip">”</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_160" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_162" n="HIAT:w" s="T43">A</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_165" n="HIAT:w" s="T44">täp</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_168" n="HIAT:w" s="T45">tʼärɨn</ts>
                  <nts id="Seg_169" n="HIAT:ip">:</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_171" n="HIAT:ip">“</nts>
                  <ts e="T47" id="Seg_173" n="HIAT:w" s="T46">Menap</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_176" n="HIAT:w" s="T47">nildʼzʼin</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_179" n="HIAT:w" s="T48">tʼelɨmqwattə</ts>
                  <nts id="Seg_180" n="HIAT:ip">.</nts>
                  <nts id="Seg_181" n="HIAT:ip">”</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T49" id="Seg_183" n="sc" s="T1">
               <ts e="T2" id="Seg_185" n="e" s="T1">Man </ts>
               <ts e="T3" id="Seg_187" n="e" s="T2">telʼdʼän </ts>
               <ts e="T4" id="Seg_189" n="e" s="T3">qoǯirsau </ts>
               <ts e="T5" id="Seg_191" n="e" s="T4">täbeɣum </ts>
               <ts e="T6" id="Seg_193" n="e" s="T5">šeːɣadʼiga. </ts>
               <ts e="T7" id="Seg_195" n="e" s="T6">Man </ts>
               <ts e="T8" id="Seg_197" n="e" s="T7">täerbɨzan </ts>
               <ts e="T9" id="Seg_199" n="e" s="T8">sɨgan </ts>
               <ts e="T10" id="Seg_201" n="e" s="T9">ass. </ts>
               <ts e="T11" id="Seg_203" n="e" s="T10">Täp </ts>
               <ts e="T12" id="Seg_205" n="e" s="T11">ass </ts>
               <ts e="T13" id="Seg_207" n="e" s="T12">sɨgan. </ts>
               <ts e="T14" id="Seg_209" n="e" s="T13">A </ts>
               <ts e="T15" id="Seg_211" n="e" s="T14">pajat </ts>
               <ts e="T16" id="Seg_213" n="e" s="T15">nawerno </ts>
               <ts e="T17" id="Seg_215" n="e" s="T16">näj </ts>
               <ts e="T18" id="Seg_217" n="e" s="T17">šäɣa. </ts>
               <ts e="T19" id="Seg_219" n="e" s="T18">Man </ts>
               <ts e="T20" id="Seg_221" n="e" s="T19">qoǯirsau </ts>
               <ts e="T21" id="Seg_223" n="e" s="T20">kitajzlam </ts>
               <ts e="T22" id="Seg_225" n="e" s="T21">i </ts>
               <ts e="T23" id="Seg_227" n="e" s="T22">karʼejzlam. </ts>
               <ts e="T24" id="Seg_229" n="e" s="T23">Täbla </ts>
               <ts e="T25" id="Seg_231" n="e" s="T24">šäɣən </ts>
               <ts e="T26" id="Seg_233" n="e" s="T25">jewattə. </ts>
               <ts e="T27" id="Seg_235" n="e" s="T26">Ollattə </ts>
               <ts e="T28" id="Seg_237" n="e" s="T27">kruglan </ts>
               <ts e="T29" id="Seg_239" n="e" s="T28">az </ts>
               <ts e="T30" id="Seg_241" n="e" s="T29">jewattə, </ts>
               <ts e="T31" id="Seg_243" n="e" s="T30">tʼüpbokon </ts>
               <ts e="T32" id="Seg_245" n="e" s="T31">jewattə. </ts>
               <ts e="T33" id="Seg_247" n="e" s="T32">Qɨbanʼäʒa </ts>
               <ts e="T34" id="Seg_249" n="e" s="T33">tʼelɨmgun </ts>
               <ts e="T35" id="Seg_251" n="e" s="T34">wes </ts>
               <ts e="T36" id="Seg_253" n="e" s="T35">šäɣən. </ts>
               <ts e="T37" id="Seg_255" n="e" s="T36">A </ts>
               <ts e="T38" id="Seg_257" n="e" s="T37">man </ts>
               <ts e="T39" id="Seg_259" n="e" s="T38">soɣɨdʼän: </ts>
               <ts e="T40" id="Seg_261" n="e" s="T39">“Qajɣɨn </ts>
               <ts e="T41" id="Seg_263" n="e" s="T40">qwatpat, </ts>
               <ts e="T42" id="Seg_265" n="e" s="T41">qaj </ts>
               <ts e="T43" id="Seg_267" n="e" s="T42">pɨgɨlɨppa?” </ts>
               <ts e="T44" id="Seg_269" n="e" s="T43">A </ts>
               <ts e="T45" id="Seg_271" n="e" s="T44">täp </ts>
               <ts e="T46" id="Seg_273" n="e" s="T45">tʼärɨn: </ts>
               <ts e="T47" id="Seg_275" n="e" s="T46">“Menap </ts>
               <ts e="T48" id="Seg_277" n="e" s="T47">nildʼzʼin </ts>
               <ts e="T49" id="Seg_279" n="e" s="T48">tʼelɨmqwattə.” </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_280" s="T1">PVD_1964_DarkSkinnedPeople_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_281" s="T6">PVD_1964_DarkSkinnedPeople_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_282" s="T10">PVD_1964_DarkSkinnedPeople_nar.003 (001.003)</ta>
            <ta e="T18" id="Seg_283" s="T13">PVD_1964_DarkSkinnedPeople_nar.004 (001.004)</ta>
            <ta e="T23" id="Seg_284" s="T18">PVD_1964_DarkSkinnedPeople_nar.005 (001.005)</ta>
            <ta e="T26" id="Seg_285" s="T23">PVD_1964_DarkSkinnedPeople_nar.006 (001.006)</ta>
            <ta e="T32" id="Seg_286" s="T26">PVD_1964_DarkSkinnedPeople_nar.007 (001.007)</ta>
            <ta e="T36" id="Seg_287" s="T32">PVD_1964_DarkSkinnedPeople_nar.008 (001.008)</ta>
            <ta e="T43" id="Seg_288" s="T36">PVD_1964_DarkSkinnedPeople_nar.009 (001.009)</ta>
            <ta e="T49" id="Seg_289" s="T43">PVD_1964_DarkSkinnedPeople_nar.010 (001.010)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_290" s="T1">ман ′телʼдʼӓн ′kоджирсау тӓ′беɣум ′ше̄ɣад̂ʼига.</ta>
            <ta e="T10" id="Seg_291" s="T6">ман ′тӓербы‵зан сы ′ган асс.</ta>
            <ta e="T13" id="Seg_292" s="T10">тӓп асс сы ′ган.</ta>
            <ta e="T18" id="Seg_293" s="T13">а ′паjат на′верно ′нӓй ′шӓɣа.</ta>
            <ta e="T23" id="Seg_294" s="T18">ман kоджир′сау ки′тайзлам и ка′рʼейзлам.</ta>
            <ta e="T26" id="Seg_295" s="T23">тӓб′ла ′шӓɣън ′jеwаттъ.</ta>
            <ta e="T32" id="Seg_296" s="T26">оl′lаттъ ′круглан аз ′jеwаттъ, ′тʼӱпбокон ′jеwаттъ.</ta>
            <ta e="T36" id="Seg_297" s="T32">kыбанʼӓжа ′тʼелымгун вес ′шӓɣън.</ta>
            <ta e="T43" id="Seg_298" s="T36">а ман соɣы′дʼ(дʼзʼ)ӓн: kай′ɣын kwат′пат, kай ′пыгылыппа?</ta>
            <ta e="T49" id="Seg_299" s="T43">а тӓп тʼӓ′рын: ме′нап ниl′дʼзʼин ′тʼелым‵kwаттъ.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_300" s="T1">man telʼdʼän qoǯirsau täbeɣum šeːɣad̂ʼiga.</ta>
            <ta e="T10" id="Seg_301" s="T6">man täerbɨzan sɨ gan ass.</ta>
            <ta e="T13" id="Seg_302" s="T10">täp ass sɨ gan.</ta>
            <ta e="T18" id="Seg_303" s="T13">a pajat nawerno näj šäɣa.</ta>
            <ta e="T23" id="Seg_304" s="T18">man qoǯirsau kitajzlam i karʼejzlam.</ta>
            <ta e="T26" id="Seg_305" s="T23">täbla šäɣən jewattə.</ta>
            <ta e="T32" id="Seg_306" s="T26">ollattə kruglan az jewattə, tʼüpbokon jewattə.</ta>
            <ta e="T36" id="Seg_307" s="T32">qɨbanʼäʒa tʼelɨmgun wes šäɣən.</ta>
            <ta e="T43" id="Seg_308" s="T36">a man soɣɨdʼ(dʼzʼ)än: qajɣɨn qwatpat, qaj pɨgɨlɨppa?</ta>
            <ta e="T49" id="Seg_309" s="T43">a täp tʼärɨn: menap nildʼzʼin tʼelɨmqwattə.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_310" s="T1">Man telʼdʼän qoǯirsau täbeɣum šeːɣadʼiga. </ta>
            <ta e="T10" id="Seg_311" s="T6">Man täerbɨzan sɨgan ass. </ta>
            <ta e="T13" id="Seg_312" s="T10">Täp ass sɨgan. </ta>
            <ta e="T18" id="Seg_313" s="T13">A pajat nawerno näj šäɣa. </ta>
            <ta e="T23" id="Seg_314" s="T18">Man qoǯirsau kitajzlam i karʼejzlam. </ta>
            <ta e="T26" id="Seg_315" s="T23">Täbla šäɣən jewattə. </ta>
            <ta e="T32" id="Seg_316" s="T26">Ollattə kruglan az jewattə, tʼüpbokon jewattə. </ta>
            <ta e="T36" id="Seg_317" s="T32">Qɨbanʼäʒa tʼelɨmgun wes šäɣən. </ta>
            <ta e="T43" id="Seg_318" s="T36">A man soɣɨdʼän: “Qajɣɨn qwatpat, qaj pɨgɨlɨppa?” </ta>
            <ta e="T49" id="Seg_319" s="T43">A täp tʼärɨn: “Menap nildʼzʼin tʼelɨmqwattə.” </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_320" s="T1">man</ta>
            <ta e="T3" id="Seg_321" s="T2">telʼdʼän</ta>
            <ta e="T4" id="Seg_322" s="T3">qo-ǯir-sa-u</ta>
            <ta e="T5" id="Seg_323" s="T4">täbe-ɣum</ta>
            <ta e="T6" id="Seg_324" s="T5">šeːɣa-dʼiga</ta>
            <ta e="T7" id="Seg_325" s="T6">man</ta>
            <ta e="T8" id="Seg_326" s="T7">täerbɨ-za-n</ta>
            <ta e="T9" id="Seg_327" s="T8">sɨgan</ta>
            <ta e="T10" id="Seg_328" s="T9">ass</ta>
            <ta e="T11" id="Seg_329" s="T10">täp</ta>
            <ta e="T12" id="Seg_330" s="T11">ass</ta>
            <ta e="T13" id="Seg_331" s="T12">sɨgan</ta>
            <ta e="T14" id="Seg_332" s="T13">a</ta>
            <ta e="T15" id="Seg_333" s="T14">paja-t</ta>
            <ta e="T16" id="Seg_334" s="T15">nawerno</ta>
            <ta e="T17" id="Seg_335" s="T16">näj</ta>
            <ta e="T18" id="Seg_336" s="T17">šäɣa</ta>
            <ta e="T19" id="Seg_337" s="T18">man</ta>
            <ta e="T20" id="Seg_338" s="T19">qo-ǯir-sa-u</ta>
            <ta e="T21" id="Seg_339" s="T20">kitajz-la-m</ta>
            <ta e="T22" id="Seg_340" s="T21">i</ta>
            <ta e="T23" id="Seg_341" s="T22">karʼejz-la-m</ta>
            <ta e="T24" id="Seg_342" s="T23">täb-la</ta>
            <ta e="T25" id="Seg_343" s="T24">šäɣə-n</ta>
            <ta e="T26" id="Seg_344" s="T25">je-wa-ttə</ta>
            <ta e="T27" id="Seg_345" s="T26">olla-ttə</ta>
            <ta e="T28" id="Seg_346" s="T27">krugla-n</ta>
            <ta e="T29" id="Seg_347" s="T28">az</ta>
            <ta e="T30" id="Seg_348" s="T29">je-wa-ttə</ta>
            <ta e="T31" id="Seg_349" s="T30">tʼüpbo-ko-n</ta>
            <ta e="T32" id="Seg_350" s="T31">je-wa-ttə</ta>
            <ta e="T33" id="Seg_351" s="T32">qɨbanʼäʒa</ta>
            <ta e="T34" id="Seg_352" s="T33">tʼelɨm-gu-n</ta>
            <ta e="T35" id="Seg_353" s="T34">wes</ta>
            <ta e="T36" id="Seg_354" s="T35">šäɣə-n</ta>
            <ta e="T37" id="Seg_355" s="T36">a</ta>
            <ta e="T38" id="Seg_356" s="T37">man</ta>
            <ta e="T39" id="Seg_357" s="T38">soɣɨdʼä-n</ta>
            <ta e="T40" id="Seg_358" s="T39">qaj-ɣɨn</ta>
            <ta e="T41" id="Seg_359" s="T40">qwat-pa-t</ta>
            <ta e="T42" id="Seg_360" s="T41">qaj</ta>
            <ta e="T43" id="Seg_361" s="T42">pɨgɨl-ɨ-ppa</ta>
            <ta e="T44" id="Seg_362" s="T43">a</ta>
            <ta e="T45" id="Seg_363" s="T44">täp</ta>
            <ta e="T46" id="Seg_364" s="T45">tʼärɨ-n</ta>
            <ta e="T47" id="Seg_365" s="T46">me-nap</ta>
            <ta e="T48" id="Seg_366" s="T47">nildʼzʼi-n</ta>
            <ta e="T49" id="Seg_367" s="T48">tʼelɨm-qwa-ttə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_368" s="T1">man</ta>
            <ta e="T3" id="Seg_369" s="T2">telʼdʼan</ta>
            <ta e="T4" id="Seg_370" s="T3">qo-nǯir-sɨ-w</ta>
            <ta e="T5" id="Seg_371" s="T4">täbe-qum</ta>
            <ta e="T6" id="Seg_372" s="T5">šeːɣa-dʼiga</ta>
            <ta e="T7" id="Seg_373" s="T6">man</ta>
            <ta e="T8" id="Seg_374" s="T7">tärba-sɨ-ŋ</ta>
            <ta e="T9" id="Seg_375" s="T8">sɨgan</ta>
            <ta e="T10" id="Seg_376" s="T9">asa</ta>
            <ta e="T11" id="Seg_377" s="T10">täp</ta>
            <ta e="T12" id="Seg_378" s="T11">asa</ta>
            <ta e="T13" id="Seg_379" s="T12">sɨgan</ta>
            <ta e="T14" id="Seg_380" s="T13">a</ta>
            <ta e="T15" id="Seg_381" s="T14">paja-tə</ta>
            <ta e="T16" id="Seg_382" s="T15">nawerna</ta>
            <ta e="T17" id="Seg_383" s="T16">naj</ta>
            <ta e="T18" id="Seg_384" s="T17">šeːɣa</ta>
            <ta e="T19" id="Seg_385" s="T18">man</ta>
            <ta e="T20" id="Seg_386" s="T19">qo-nǯir-sɨ-w</ta>
            <ta e="T21" id="Seg_387" s="T20">kitajz-la-m</ta>
            <ta e="T22" id="Seg_388" s="T21">i</ta>
            <ta e="T23" id="Seg_389" s="T22">karʼejz-la-m</ta>
            <ta e="T24" id="Seg_390" s="T23">täp-la</ta>
            <ta e="T25" id="Seg_391" s="T24">šeːɣa-ŋ</ta>
            <ta e="T26" id="Seg_392" s="T25">eː-nɨ-tɨn</ta>
            <ta e="T27" id="Seg_393" s="T26">olə-tə</ta>
            <ta e="T28" id="Seg_394" s="T27">krugla-ŋ</ta>
            <ta e="T29" id="Seg_395" s="T28">asa</ta>
            <ta e="T30" id="Seg_396" s="T29">eː-nɨ-tɨn</ta>
            <ta e="T31" id="Seg_397" s="T30">tʼümbɨ-ka-ŋ</ta>
            <ta e="T32" id="Seg_398" s="T31">eː-nɨ-tɨn</ta>
            <ta e="T33" id="Seg_399" s="T32">qɨbanʼaǯa</ta>
            <ta e="T34" id="Seg_400" s="T33">tʼelɨm-ku-n</ta>
            <ta e="T35" id="Seg_401" s="T34">wesʼ</ta>
            <ta e="T36" id="Seg_402" s="T35">šeːɣa-ŋ</ta>
            <ta e="T37" id="Seg_403" s="T36">a</ta>
            <ta e="T38" id="Seg_404" s="T37">man</ta>
            <ta e="T39" id="Seg_405" s="T38">sogandʼe-ŋ</ta>
            <ta e="T40" id="Seg_406" s="T39">qaj-qɨn</ta>
            <ta e="T41" id="Seg_407" s="T40">qwat-mbɨ-t</ta>
            <ta e="T42" id="Seg_408" s="T41">qaj</ta>
            <ta e="T43" id="Seg_409" s="T42">pɨŋgəl-ɨ-mbɨ</ta>
            <ta e="T44" id="Seg_410" s="T43">a</ta>
            <ta e="T45" id="Seg_411" s="T44">täp</ta>
            <ta e="T46" id="Seg_412" s="T45">tʼärɨ-n</ta>
            <ta e="T47" id="Seg_413" s="T46">me-nan</ta>
            <ta e="T48" id="Seg_414" s="T47">nʼilʼdʼi-ŋ</ta>
            <ta e="T49" id="Seg_415" s="T48">tʼelɨm-ku-tɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_416" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_417" s="T2">yesterday</ta>
            <ta e="T4" id="Seg_418" s="T3">see-DRV-PST-1SG.O</ta>
            <ta e="T5" id="Seg_419" s="T4">man-human.being.[NOM]</ta>
            <ta e="T6" id="Seg_420" s="T5">black-%%</ta>
            <ta e="T7" id="Seg_421" s="T6">I.NOM</ta>
            <ta e="T8" id="Seg_422" s="T7">think-PST-1SG.S</ta>
            <ta e="T9" id="Seg_423" s="T8">Gipsy.[NOM]</ta>
            <ta e="T10" id="Seg_424" s="T9">NEG</ta>
            <ta e="T11" id="Seg_425" s="T10">(s)he.[NOM]</ta>
            <ta e="T12" id="Seg_426" s="T11">NEG</ta>
            <ta e="T13" id="Seg_427" s="T12">Gipsy.[NOM]</ta>
            <ta e="T14" id="Seg_428" s="T13">and</ta>
            <ta e="T15" id="Seg_429" s="T14">wife.[NOM]-3SG</ta>
            <ta e="T16" id="Seg_430" s="T15">probably</ta>
            <ta e="T17" id="Seg_431" s="T16">also</ta>
            <ta e="T18" id="Seg_432" s="T17">black.[3SG.S]</ta>
            <ta e="T19" id="Seg_433" s="T18">I.NOM</ta>
            <ta e="T20" id="Seg_434" s="T19">see-DRV-PST-1SG.O</ta>
            <ta e="T21" id="Seg_435" s="T20">Chinese-PL-ACC</ta>
            <ta e="T22" id="Seg_436" s="T21">and</ta>
            <ta e="T23" id="Seg_437" s="T22">Korean-PL-ACC</ta>
            <ta e="T24" id="Seg_438" s="T23">(s)he-PL.[NOM]</ta>
            <ta e="T25" id="Seg_439" s="T24">black-ADVZ</ta>
            <ta e="T26" id="Seg_440" s="T25">be-CO-3PL</ta>
            <ta e="T27" id="Seg_441" s="T26">head.[NOM]-3SG</ta>
            <ta e="T28" id="Seg_442" s="T27">round-ADVZ</ta>
            <ta e="T29" id="Seg_443" s="T28">NEG</ta>
            <ta e="T30" id="Seg_444" s="T29">be-CO-3PL</ta>
            <ta e="T31" id="Seg_445" s="T30">long-DIM-ADVZ</ta>
            <ta e="T32" id="Seg_446" s="T31">be-CO-3PL</ta>
            <ta e="T33" id="Seg_447" s="T32">child.[NOM]</ta>
            <ta e="T34" id="Seg_448" s="T33">be.born-TEMPN-ADV.LOC</ta>
            <ta e="T35" id="Seg_449" s="T34">all</ta>
            <ta e="T36" id="Seg_450" s="T35">black-ADVZ</ta>
            <ta e="T37" id="Seg_451" s="T36">and</ta>
            <ta e="T38" id="Seg_452" s="T37">I.NOM</ta>
            <ta e="T39" id="Seg_453" s="T38">ask-1SG.S</ta>
            <ta e="T40" id="Seg_454" s="T39">what-LOC</ta>
            <ta e="T41" id="Seg_455" s="T40">beat-RES-3SG.O</ta>
            <ta e="T42" id="Seg_456" s="T41">either</ta>
            <ta e="T43" id="Seg_457" s="T42">fall.down-EP-PST.NAR.[3SG.S]</ta>
            <ta e="T44" id="Seg_458" s="T43">and</ta>
            <ta e="T45" id="Seg_459" s="T44">(s)he.[NOM]</ta>
            <ta e="T46" id="Seg_460" s="T45">say-3SG.S</ta>
            <ta e="T47" id="Seg_461" s="T46">we-ADES</ta>
            <ta e="T48" id="Seg_462" s="T47">so-ADVZ</ta>
            <ta e="T49" id="Seg_463" s="T48">give.birth-HAB-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_464" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_465" s="T2">вчера</ta>
            <ta e="T4" id="Seg_466" s="T3">увидеть-DRV-PST-1SG.O</ta>
            <ta e="T5" id="Seg_467" s="T4">мужчина-человек.[NOM]</ta>
            <ta e="T6" id="Seg_468" s="T5">чёрный-%%</ta>
            <ta e="T7" id="Seg_469" s="T6">я.NOM</ta>
            <ta e="T8" id="Seg_470" s="T7">думать-PST-1SG.S</ta>
            <ta e="T9" id="Seg_471" s="T8">цыган.[NOM]</ta>
            <ta e="T10" id="Seg_472" s="T9">NEG</ta>
            <ta e="T11" id="Seg_473" s="T10">он(а).[NOM]</ta>
            <ta e="T12" id="Seg_474" s="T11">NEG</ta>
            <ta e="T13" id="Seg_475" s="T12">цыган.[NOM]</ta>
            <ta e="T14" id="Seg_476" s="T13">а</ta>
            <ta e="T15" id="Seg_477" s="T14">жена.[NOM]-3SG</ta>
            <ta e="T16" id="Seg_478" s="T15">наверное</ta>
            <ta e="T17" id="Seg_479" s="T16">тоже</ta>
            <ta e="T18" id="Seg_480" s="T17">чёрный.[3SG.S]</ta>
            <ta e="T19" id="Seg_481" s="T18">я.NOM</ta>
            <ta e="T20" id="Seg_482" s="T19">увидеть-DRV-PST-1SG.O</ta>
            <ta e="T21" id="Seg_483" s="T20">китаец-PL-ACC</ta>
            <ta e="T22" id="Seg_484" s="T21">и</ta>
            <ta e="T23" id="Seg_485" s="T22">кореец-PL-ACC</ta>
            <ta e="T24" id="Seg_486" s="T23">он(а)-PL.[NOM]</ta>
            <ta e="T25" id="Seg_487" s="T24">чёрный-ADVZ</ta>
            <ta e="T26" id="Seg_488" s="T25">быть-CO-3PL</ta>
            <ta e="T27" id="Seg_489" s="T26">голова.[NOM]-3SG</ta>
            <ta e="T28" id="Seg_490" s="T27">круглый-ADVZ</ta>
            <ta e="T29" id="Seg_491" s="T28">NEG</ta>
            <ta e="T30" id="Seg_492" s="T29">быть-CO-3PL</ta>
            <ta e="T31" id="Seg_493" s="T30">длинный-DIM-ADVZ</ta>
            <ta e="T32" id="Seg_494" s="T31">быть-CO-3PL</ta>
            <ta e="T33" id="Seg_495" s="T32">ребенок.[NOM]</ta>
            <ta e="T34" id="Seg_496" s="T33">родиться-TEMPN-ADV.LOC</ta>
            <ta e="T35" id="Seg_497" s="T34">весь</ta>
            <ta e="T36" id="Seg_498" s="T35">чёрный-ADVZ</ta>
            <ta e="T37" id="Seg_499" s="T36">а</ta>
            <ta e="T38" id="Seg_500" s="T37">я.NOM</ta>
            <ta e="T39" id="Seg_501" s="T38">спросить-1SG.S</ta>
            <ta e="T40" id="Seg_502" s="T39">что-LOC</ta>
            <ta e="T41" id="Seg_503" s="T40">побить-RES-3SG.O</ta>
            <ta e="T42" id="Seg_504" s="T41">ли</ta>
            <ta e="T43" id="Seg_505" s="T42">упасть-EP-PST.NAR.[3SG.S]</ta>
            <ta e="T44" id="Seg_506" s="T43">а</ta>
            <ta e="T45" id="Seg_507" s="T44">он(а).[NOM]</ta>
            <ta e="T46" id="Seg_508" s="T45">сказать-3SG.S</ta>
            <ta e="T47" id="Seg_509" s="T46">мы-ADES</ta>
            <ta e="T48" id="Seg_510" s="T47">так-ADVZ</ta>
            <ta e="T49" id="Seg_511" s="T48">родить-HAB-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_512" s="T1">pers</ta>
            <ta e="T3" id="Seg_513" s="T2">adv</ta>
            <ta e="T4" id="Seg_514" s="T3">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_515" s="T4">n-n.[n:case]</ta>
            <ta e="T6" id="Seg_516" s="T5">adj-adj&gt;???</ta>
            <ta e="T7" id="Seg_517" s="T6">pers</ta>
            <ta e="T8" id="Seg_518" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_519" s="T8">n.[n:case]</ta>
            <ta e="T10" id="Seg_520" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_521" s="T10">pers.[n:case]</ta>
            <ta e="T12" id="Seg_522" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_523" s="T12">n.[n:case]</ta>
            <ta e="T14" id="Seg_524" s="T13">conj</ta>
            <ta e="T15" id="Seg_525" s="T14">n.[n:case]-n:poss</ta>
            <ta e="T16" id="Seg_526" s="T15">adv</ta>
            <ta e="T17" id="Seg_527" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_528" s="T17">adj.[v:pn]</ta>
            <ta e="T19" id="Seg_529" s="T18">pers</ta>
            <ta e="T20" id="Seg_530" s="T19">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_531" s="T20">n-n:num-n:case</ta>
            <ta e="T22" id="Seg_532" s="T21">conj</ta>
            <ta e="T23" id="Seg_533" s="T22">n-n:num-n:case</ta>
            <ta e="T24" id="Seg_534" s="T23">pers-n:num.[n:case]</ta>
            <ta e="T25" id="Seg_535" s="T24">adj-adj&gt;adv</ta>
            <ta e="T26" id="Seg_536" s="T25">v-v:ins-v:pn</ta>
            <ta e="T27" id="Seg_537" s="T26">n.[n:case]-n:poss</ta>
            <ta e="T28" id="Seg_538" s="T27">adj-adj&gt;adv</ta>
            <ta e="T29" id="Seg_539" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_540" s="T29">v-v:ins-v:pn</ta>
            <ta e="T31" id="Seg_541" s="T30">adj-n&gt;n-adj&gt;adv</ta>
            <ta e="T32" id="Seg_542" s="T31">v-v:ins-v:pn</ta>
            <ta e="T33" id="Seg_543" s="T32">n.[n:case]</ta>
            <ta e="T34" id="Seg_544" s="T33">v-v&gt;n-n&gt;adv</ta>
            <ta e="T35" id="Seg_545" s="T34">quant</ta>
            <ta e="T36" id="Seg_546" s="T35">adj-adj&gt;adv</ta>
            <ta e="T37" id="Seg_547" s="T36">conj</ta>
            <ta e="T38" id="Seg_548" s="T37">pers</ta>
            <ta e="T39" id="Seg_549" s="T38">v-v:pn</ta>
            <ta e="T40" id="Seg_550" s="T39">interrog-n:case</ta>
            <ta e="T41" id="Seg_551" s="T40">v-v&gt;v-v:pn</ta>
            <ta e="T42" id="Seg_552" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_553" s="T42">v-v:ins-v:tense.[v:pn]</ta>
            <ta e="T44" id="Seg_554" s="T43">conj</ta>
            <ta e="T45" id="Seg_555" s="T44">pers.[n:case]</ta>
            <ta e="T46" id="Seg_556" s="T45">v-v:pn</ta>
            <ta e="T47" id="Seg_557" s="T46">pers-n:case</ta>
            <ta e="T48" id="Seg_558" s="T47">adv-adj&gt;adv</ta>
            <ta e="T49" id="Seg_559" s="T48">v-v&gt;v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_560" s="T1">pers</ta>
            <ta e="T3" id="Seg_561" s="T2">adv</ta>
            <ta e="T4" id="Seg_562" s="T3">v</ta>
            <ta e="T5" id="Seg_563" s="T4">n</ta>
            <ta e="T6" id="Seg_564" s="T5">adj</ta>
            <ta e="T7" id="Seg_565" s="T6">pers</ta>
            <ta e="T8" id="Seg_566" s="T7">v</ta>
            <ta e="T9" id="Seg_567" s="T8">n</ta>
            <ta e="T10" id="Seg_568" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_569" s="T10">pers</ta>
            <ta e="T12" id="Seg_570" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_571" s="T12">n</ta>
            <ta e="T14" id="Seg_572" s="T13">conj</ta>
            <ta e="T15" id="Seg_573" s="T14">n</ta>
            <ta e="T16" id="Seg_574" s="T15">adv</ta>
            <ta e="T17" id="Seg_575" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_576" s="T17">adj</ta>
            <ta e="T19" id="Seg_577" s="T18">pers</ta>
            <ta e="T20" id="Seg_578" s="T19">v</ta>
            <ta e="T21" id="Seg_579" s="T20">n</ta>
            <ta e="T22" id="Seg_580" s="T21">conj</ta>
            <ta e="T23" id="Seg_581" s="T22">n</ta>
            <ta e="T24" id="Seg_582" s="T23">pers</ta>
            <ta e="T25" id="Seg_583" s="T24">adj</ta>
            <ta e="T26" id="Seg_584" s="T25">v</ta>
            <ta e="T27" id="Seg_585" s="T26">n</ta>
            <ta e="T28" id="Seg_586" s="T27">adj</ta>
            <ta e="T29" id="Seg_587" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_588" s="T29">v</ta>
            <ta e="T31" id="Seg_589" s="T30">adv</ta>
            <ta e="T32" id="Seg_590" s="T31">v</ta>
            <ta e="T33" id="Seg_591" s="T32">n</ta>
            <ta e="T34" id="Seg_592" s="T33">n</ta>
            <ta e="T35" id="Seg_593" s="T34">quant</ta>
            <ta e="T36" id="Seg_594" s="T35">adj</ta>
            <ta e="T37" id="Seg_595" s="T36">conj</ta>
            <ta e="T38" id="Seg_596" s="T37">pers</ta>
            <ta e="T39" id="Seg_597" s="T38">v</ta>
            <ta e="T40" id="Seg_598" s="T39">interrog</ta>
            <ta e="T41" id="Seg_599" s="T40">v</ta>
            <ta e="T42" id="Seg_600" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_601" s="T42">v</ta>
            <ta e="T44" id="Seg_602" s="T43">conj</ta>
            <ta e="T45" id="Seg_603" s="T44">pers</ta>
            <ta e="T46" id="Seg_604" s="T45">v</ta>
            <ta e="T47" id="Seg_605" s="T46">pers</ta>
            <ta e="T48" id="Seg_606" s="T47">adv</ta>
            <ta e="T49" id="Seg_607" s="T48">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_608" s="T1">pro.h:S</ta>
            <ta e="T4" id="Seg_609" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_610" s="T4">np.h:O</ta>
            <ta e="T7" id="Seg_611" s="T6">pro.h:S</ta>
            <ta e="T8" id="Seg_612" s="T7">v:pred</ta>
            <ta e="T11" id="Seg_613" s="T10">pro.h:S</ta>
            <ta e="T13" id="Seg_614" s="T12">n:pred</ta>
            <ta e="T15" id="Seg_615" s="T14">np.h:S</ta>
            <ta e="T18" id="Seg_616" s="T17">adj:pred</ta>
            <ta e="T19" id="Seg_617" s="T18">pro.h:S</ta>
            <ta e="T20" id="Seg_618" s="T19">v:pred</ta>
            <ta e="T21" id="Seg_619" s="T20">np.h:O</ta>
            <ta e="T23" id="Seg_620" s="T22">np.h:O</ta>
            <ta e="T24" id="Seg_621" s="T23">pro.h:S</ta>
            <ta e="T26" id="Seg_622" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_623" s="T26">np:S</ta>
            <ta e="T30" id="Seg_624" s="T29">v:pred</ta>
            <ta e="T32" id="Seg_625" s="T31">0.3:S v:pred</ta>
            <ta e="T34" id="Seg_626" s="T32">s:temp</ta>
            <ta e="T38" id="Seg_627" s="T37">pro.h:S</ta>
            <ta e="T39" id="Seg_628" s="T38">v:pred</ta>
            <ta e="T41" id="Seg_629" s="T40">0.3.h:S v:pred</ta>
            <ta e="T43" id="Seg_630" s="T42">0.3.h:S v:pred</ta>
            <ta e="T45" id="Seg_631" s="T44">pro.h:S</ta>
            <ta e="T46" id="Seg_632" s="T45">v:pred</ta>
            <ta e="T49" id="Seg_633" s="T48">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_634" s="T1">pro.h:E</ta>
            <ta e="T3" id="Seg_635" s="T2">adv:Time</ta>
            <ta e="T5" id="Seg_636" s="T4">np.h:Th</ta>
            <ta e="T7" id="Seg_637" s="T6">pro.h:E</ta>
            <ta e="T11" id="Seg_638" s="T10">pro.h:Th</ta>
            <ta e="T15" id="Seg_639" s="T14">np.h:Th 0.3.h:Poss</ta>
            <ta e="T19" id="Seg_640" s="T18">pro.h:E</ta>
            <ta e="T21" id="Seg_641" s="T20">np.h:Th</ta>
            <ta e="T23" id="Seg_642" s="T22">np.h:Th</ta>
            <ta e="T24" id="Seg_643" s="T23">pro.h:Th</ta>
            <ta e="T27" id="Seg_644" s="T26">np:Th 0.3.h:Poss</ta>
            <ta e="T32" id="Seg_645" s="T31">0.3:Th</ta>
            <ta e="T33" id="Seg_646" s="T32">np.h:P</ta>
            <ta e="T38" id="Seg_647" s="T37">pro.h:A</ta>
            <ta e="T40" id="Seg_648" s="T39">pro:L</ta>
            <ta e="T41" id="Seg_649" s="T40">0.3.h:Th</ta>
            <ta e="T43" id="Seg_650" s="T42">0.3.h:P</ta>
            <ta e="T45" id="Seg_651" s="T44">pro.h:A</ta>
            <ta e="T47" id="Seg_652" s="T46">pro:L</ta>
            <ta e="T49" id="Seg_653" s="T48">0.3.h:P</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T9" id="Seg_654" s="T8">RUS:cult</ta>
            <ta e="T13" id="Seg_655" s="T12">RUS:cult</ta>
            <ta e="T14" id="Seg_656" s="T13">RUS:gram</ta>
            <ta e="T16" id="Seg_657" s="T15">RUS:mod</ta>
            <ta e="T21" id="Seg_658" s="T20">RUS:cult</ta>
            <ta e="T22" id="Seg_659" s="T21">RUS:gram</ta>
            <ta e="T23" id="Seg_660" s="T22">RUS:cult</ta>
            <ta e="T28" id="Seg_661" s="T27">RUS:core</ta>
            <ta e="T35" id="Seg_662" s="T34">RUS:core</ta>
            <ta e="T37" id="Seg_663" s="T36">RUS:gram</ta>
            <ta e="T44" id="Seg_664" s="T43">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T9" id="Seg_665" s="T8">Csub</ta>
            <ta e="T13" id="Seg_666" s="T12">Csub</ta>
            <ta e="T21" id="Seg_667" s="T20">medVdel Csub</ta>
            <ta e="T23" id="Seg_668" s="T22">Csub medVdel</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T9" id="Seg_669" s="T8">dir:bare</ta>
            <ta e="T13" id="Seg_670" s="T12">dir:bare</ta>
            <ta e="T21" id="Seg_671" s="T20">dir:infl</ta>
            <ta e="T23" id="Seg_672" s="T22">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_673" s="T1">Yesterday I saw a black man.</ta>
            <ta e="T10" id="Seg_674" s="T6">I thought – is he Gipsy or not?</ta>
            <ta e="T13" id="Seg_675" s="T10">He isn't Gipsy.</ta>
            <ta e="T18" id="Seg_676" s="T13">And his wife is probably black, too.</ta>
            <ta e="T23" id="Seg_677" s="T18">I saw Chinese and Korean people.</ta>
            <ta e="T26" id="Seg_678" s="T23">They were black.</ta>
            <ta e="T32" id="Seg_679" s="T26">Their heads were not round, they were long.</ta>
            <ta e="T36" id="Seg_680" s="T32">When a child is born, it's all black.</ta>
            <ta e="T43" id="Seg_681" s="T36">And I asked: “Where did he hit, had he fallen down?”</ta>
            <ta e="T49" id="Seg_682" s="T43">And she said: “They are born so among us.”</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_683" s="T1">Ich sah gestern einen Schwarzen.</ta>
            <ta e="T10" id="Seg_684" s="T6">Ich überlegte - ist er ein Zigeuner oder nicht?</ta>
            <ta e="T13" id="Seg_685" s="T10">Er ist kein Zigeuner.</ta>
            <ta e="T18" id="Seg_686" s="T13">Und seine Frau ist wahrscheinlich auch schwarz.</ta>
            <ta e="T23" id="Seg_687" s="T18">Ich sah Chinesen und Koreaner.</ta>
            <ta e="T26" id="Seg_688" s="T23">Sie waren schwarz.</ta>
            <ta e="T32" id="Seg_689" s="T26">Ihre Köpfe waren nicht rund, sie waren lang.</ta>
            <ta e="T36" id="Seg_690" s="T32">Wenn ein Kind geboren wird, ist es ganz schwarz.</ta>
            <ta e="T43" id="Seg_691" s="T36">Und ich habe gefragt: "Wo hat es sich gestoßen, wo ist es heruntergefallen?"</ta>
            <ta e="T49" id="Seg_692" s="T43">Und sie sagte: "Bei uns werden sie so geboren."</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_693" s="T1">Я вчера видала мужчину черного.</ta>
            <ta e="T10" id="Seg_694" s="T6">Я думала – цыган, нет?</ta>
            <ta e="T13" id="Seg_695" s="T10">Он не цыган.</ta>
            <ta e="T18" id="Seg_696" s="T13">А жена у него, наверное, тоже черная.</ta>
            <ta e="T23" id="Seg_697" s="T18">Я видала китайцев и корейцев.</ta>
            <ta e="T26" id="Seg_698" s="T23">Они черными были.</ta>
            <ta e="T32" id="Seg_699" s="T26">Головы у них не круглые были, длинные были.</ta>
            <ta e="T36" id="Seg_700" s="T32">Ребенок как родится, весь черный.</ta>
            <ta e="T43" id="Seg_701" s="T36">А я спросила: “Где зашибся, он упал что ли?”</ta>
            <ta e="T49" id="Seg_702" s="T43">А она говорит: “У нас так родятся”.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_703" s="T1">я вчера видала мужчину черного</ta>
            <ta e="T10" id="Seg_704" s="T6">There is no Russian translation of this sentence.</ta>
            <ta e="T13" id="Seg_705" s="T10">There is no Russian translation of this sentence.</ta>
            <ta e="T18" id="Seg_706" s="T13">а жена наверное тоже черная</ta>
            <ta e="T23" id="Seg_707" s="T18">я видала китайцев и корейцев</ta>
            <ta e="T26" id="Seg_708" s="T23">они черные</ta>
            <ta e="T32" id="Seg_709" s="T26">головы у них не круглые длинные головы</ta>
            <ta e="T36" id="Seg_710" s="T32">ребенок как родится весь черный</ta>
            <ta e="T43" id="Seg_711" s="T36">а я спросила где зашибся он упал что ли</ta>
            <ta e="T49" id="Seg_712" s="T43">а она говорит у нас так родятся</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T10" id="Seg_713" s="T6">[BrM:] 'sɨ gan' changed to 'sɨgan'.</ta>
            <ta e="T13" id="Seg_714" s="T10">[BrM:] 'sɨ gan' changed to 'sɨgan'.</ta>
            <ta e="T43" id="Seg_715" s="T36">[KuAI:] Variant: 'soɣɨdʼzʼän'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
