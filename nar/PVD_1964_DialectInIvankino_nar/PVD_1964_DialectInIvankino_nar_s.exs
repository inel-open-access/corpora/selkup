<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_DialectInIvankino_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_DialectInIvankino_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">17</ud-information>
            <ud-information attribute-name="# HIAT:w">14</ud-information>
            <ud-information attribute-name="# e">14</ud-information>
            <ud-information attribute-name="# HIAT:u">3</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T15" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Iwankinaɣən</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">arɨn</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">kulubattə</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Man</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">teblan</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">äʒɨm</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">as</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">sorau</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_32" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">A</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">tebla</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">me</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">ɨʒəwtə</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">aːs</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_49" n="HIAT:w" s="T14">sorat</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T15" id="Seg_52" n="sc" s="T1">
               <ts e="T2" id="Seg_54" n="e" s="T1">Iwankinaɣən </ts>
               <ts e="T3" id="Seg_56" n="e" s="T2">arɨn </ts>
               <ts e="T4" id="Seg_58" n="e" s="T3">kulubattə. </ts>
               <ts e="T5" id="Seg_60" n="e" s="T4">Man </ts>
               <ts e="T6" id="Seg_62" n="e" s="T5">teblan </ts>
               <ts e="T7" id="Seg_64" n="e" s="T6">äʒɨm </ts>
               <ts e="T8" id="Seg_66" n="e" s="T7">as </ts>
               <ts e="T9" id="Seg_68" n="e" s="T8">sorau. </ts>
               <ts e="T10" id="Seg_70" n="e" s="T9">A </ts>
               <ts e="T11" id="Seg_72" n="e" s="T10">tebla </ts>
               <ts e="T12" id="Seg_74" n="e" s="T11">me </ts>
               <ts e="T13" id="Seg_76" n="e" s="T12">ɨʒəwtə </ts>
               <ts e="T14" id="Seg_78" n="e" s="T13">aːs </ts>
               <ts e="T15" id="Seg_80" n="e" s="T14">sorat. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_81" s="T1">PVD_1964_DialectInIvankino_nar.001 (001.001)</ta>
            <ta e="T9" id="Seg_82" s="T4">PVD_1964_DialectInIvankino_nar.002 (001.002)</ta>
            <ta e="T15" id="Seg_83" s="T9">PVD_1964_DialectInIvankino_nar.003 (001.003)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_84" s="T1">И′ванкинаɣън ′арын кулу′баттъ.</ta>
            <ta e="T9" id="Seg_85" s="T4">ман теб′лан ′ӓжым ′ассорау̹.</ta>
            <ta e="T15" id="Seg_86" s="T9">а теб′ла ме (ɣ)′ыжъwтъ а̄′ссорат.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_87" s="T1">Иwankinaɣən arɨn kulubattə.</ta>
            <ta e="T9" id="Seg_88" s="T4">man teblan äʒɨm assorau̹.</ta>
            <ta e="T15" id="Seg_89" s="T9">a tebla me (ɣ)ɨʒəwtə aːssorat.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_90" s="T1">Iwankinaɣən arɨn kulubattə. </ta>
            <ta e="T9" id="Seg_91" s="T4">Man teblan äʒɨm as sorau. </ta>
            <ta e="T15" id="Seg_92" s="T9">A tebla me ɨʒəwtə aːs sorat. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_93" s="T1">Iwankina-ɣən</ta>
            <ta e="T3" id="Seg_94" s="T2">ar-ɨ-n</ta>
            <ta e="T4" id="Seg_95" s="T3">kuluba-ttə</ta>
            <ta e="T5" id="Seg_96" s="T4">man</ta>
            <ta e="T6" id="Seg_97" s="T5">teb-la-n</ta>
            <ta e="T7" id="Seg_98" s="T6">äʒɨ-m</ta>
            <ta e="T8" id="Seg_99" s="T7">as</ta>
            <ta e="T9" id="Seg_100" s="T8">sora-u</ta>
            <ta e="T10" id="Seg_101" s="T9">a</ta>
            <ta e="T11" id="Seg_102" s="T10">teb-la</ta>
            <ta e="T12" id="Seg_103" s="T11">me</ta>
            <ta e="T13" id="Seg_104" s="T12">ɨʒə-wtə</ta>
            <ta e="T14" id="Seg_105" s="T13">aːs</ta>
            <ta e="T15" id="Seg_106" s="T14">sora-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_107" s="T1">Iwankina-qɨn</ta>
            <ta e="T3" id="Seg_108" s="T2">aːr-ɨ-ŋ</ta>
            <ta e="T4" id="Seg_109" s="T3">kulubu-tɨn</ta>
            <ta e="T5" id="Seg_110" s="T4">man</ta>
            <ta e="T6" id="Seg_111" s="T5">täp-la-n</ta>
            <ta e="T7" id="Seg_112" s="T6">əǯə-m</ta>
            <ta e="T8" id="Seg_113" s="T7">asa</ta>
            <ta e="T9" id="Seg_114" s="T8">soːrɨ-w</ta>
            <ta e="T10" id="Seg_115" s="T9">a</ta>
            <ta e="T11" id="Seg_116" s="T10">täp-la</ta>
            <ta e="T12" id="Seg_117" s="T11">me</ta>
            <ta e="T13" id="Seg_118" s="T12">əǯə-un</ta>
            <ta e="T14" id="Seg_119" s="T13">asa</ta>
            <ta e="T15" id="Seg_120" s="T14">soːrɨ-tɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_121" s="T1">Ivankino-LOC</ta>
            <ta e="T3" id="Seg_122" s="T2">other-EP-ADVZ</ta>
            <ta e="T4" id="Seg_123" s="T3">speak-3PL</ta>
            <ta e="T5" id="Seg_124" s="T4">I.NOM</ta>
            <ta e="T6" id="Seg_125" s="T5">(s)he-PL-GEN</ta>
            <ta e="T7" id="Seg_126" s="T6">speech-ACC</ta>
            <ta e="T8" id="Seg_127" s="T7">NEG</ta>
            <ta e="T9" id="Seg_128" s="T8">love-1SG.O</ta>
            <ta e="T10" id="Seg_129" s="T9">and</ta>
            <ta e="T11" id="Seg_130" s="T10">(s)he-PL.[NOM]</ta>
            <ta e="T12" id="Seg_131" s="T11">we.GEN</ta>
            <ta e="T13" id="Seg_132" s="T12">word.[NOM]-1PL</ta>
            <ta e="T14" id="Seg_133" s="T13">NEG</ta>
            <ta e="T15" id="Seg_134" s="T14">love-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_135" s="T1">Иванкино-LOC</ta>
            <ta e="T3" id="Seg_136" s="T2">другой-EP-ADVZ</ta>
            <ta e="T4" id="Seg_137" s="T3">говорить-3PL</ta>
            <ta e="T5" id="Seg_138" s="T4">я.NOM</ta>
            <ta e="T6" id="Seg_139" s="T5">он(а)-PL-GEN</ta>
            <ta e="T7" id="Seg_140" s="T6">речь-ACC</ta>
            <ta e="T8" id="Seg_141" s="T7">NEG</ta>
            <ta e="T9" id="Seg_142" s="T8">любить-1SG.O</ta>
            <ta e="T10" id="Seg_143" s="T9">а</ta>
            <ta e="T11" id="Seg_144" s="T10">он(а)-PL.[NOM]</ta>
            <ta e="T12" id="Seg_145" s="T11">мы.GEN</ta>
            <ta e="T13" id="Seg_146" s="T12">слово.[NOM]-1PL</ta>
            <ta e="T14" id="Seg_147" s="T13">NEG</ta>
            <ta e="T15" id="Seg_148" s="T14">любить-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_149" s="T1">nprop-n:case</ta>
            <ta e="T3" id="Seg_150" s="T2">adj-n:ins-adj&gt;adv</ta>
            <ta e="T4" id="Seg_151" s="T3">v-v:pn</ta>
            <ta e="T5" id="Seg_152" s="T4">pers</ta>
            <ta e="T6" id="Seg_153" s="T5">pers-n:num-n:case</ta>
            <ta e="T7" id="Seg_154" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_155" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_156" s="T8">v-v:pn</ta>
            <ta e="T10" id="Seg_157" s="T9">conj</ta>
            <ta e="T11" id="Seg_158" s="T10">pers-n:num.[n:case]</ta>
            <ta e="T12" id="Seg_159" s="T11">pers</ta>
            <ta e="T13" id="Seg_160" s="T12">n.[n:case]-n:poss</ta>
            <ta e="T14" id="Seg_161" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_162" s="T14">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_163" s="T1">nprop</ta>
            <ta e="T3" id="Seg_164" s="T2">adv</ta>
            <ta e="T4" id="Seg_165" s="T3">v</ta>
            <ta e="T5" id="Seg_166" s="T4">pers</ta>
            <ta e="T6" id="Seg_167" s="T5">pers</ta>
            <ta e="T7" id="Seg_168" s="T6">n</ta>
            <ta e="T8" id="Seg_169" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_170" s="T8">v</ta>
            <ta e="T10" id="Seg_171" s="T9">conj</ta>
            <ta e="T11" id="Seg_172" s="T10">pers</ta>
            <ta e="T12" id="Seg_173" s="T11">pers</ta>
            <ta e="T13" id="Seg_174" s="T12">n</ta>
            <ta e="T14" id="Seg_175" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_176" s="T14">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_177" s="T1">np:L</ta>
            <ta e="T4" id="Seg_178" s="T3">0.3.h:A</ta>
            <ta e="T5" id="Seg_179" s="T4">pro.h:E</ta>
            <ta e="T6" id="Seg_180" s="T5">pro.h:Poss</ta>
            <ta e="T7" id="Seg_181" s="T6">np:Th</ta>
            <ta e="T11" id="Seg_182" s="T10">pro.h:E</ta>
            <ta e="T12" id="Seg_183" s="T11">pro.h:Poss</ta>
            <ta e="T13" id="Seg_184" s="T12">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_185" s="T3">0.3.h:S v:pred</ta>
            <ta e="T5" id="Seg_186" s="T4">pro.h:S</ta>
            <ta e="T7" id="Seg_187" s="T6">np:O</ta>
            <ta e="T9" id="Seg_188" s="T8">v:pred</ta>
            <ta e="T11" id="Seg_189" s="T10">pro.h:S</ta>
            <ta e="T13" id="Seg_190" s="T12">np:O</ta>
            <ta e="T15" id="Seg_191" s="T14">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T10" id="Seg_192" s="T9">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_193" s="T1">They speak otherwise in Ivankino.</ta>
            <ta e="T9" id="Seg_194" s="T4">I don't like their speech.</ta>
            <ta e="T15" id="Seg_195" s="T9">And they don't like our speech.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_196" s="T1">In Ivankino sprechen sie anders.</ta>
            <ta e="T9" id="Seg_197" s="T4">Ich mag ihre Sprache nicht.</ta>
            <ta e="T15" id="Seg_198" s="T9">Und sie mögen unsere Sprache nicht.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_199" s="T1">В Иванкино по-другому говорят.</ta>
            <ta e="T9" id="Seg_200" s="T4">Я их речь не люблю.</ta>
            <ta e="T15" id="Seg_201" s="T9">А они нашу речь не любят.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_202" s="T1">в Иванкино по-другому говорят</ta>
            <ta e="T9" id="Seg_203" s="T4">я ихний разговор не люблю</ta>
            <ta e="T15" id="Seg_204" s="T9">а они наш разговор не любят</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T9" id="Seg_205" s="T4">[BrM:] 'assorau' changed to 'as sorau'.</ta>
            <ta e="T15" id="Seg_206" s="T9"> ‎‎[KuAI:] Variant: ɣɨʒəwtə. [BrM:] 'aːssorat' changed to 'aːs sorat'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
