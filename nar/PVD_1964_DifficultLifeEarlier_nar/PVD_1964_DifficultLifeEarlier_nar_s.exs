<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_DifficultLifeEarlier_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_DifficultLifeEarlier_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">81</ud-information>
            <ud-information attribute-name="# HIAT:w">65</ud-information>
            <ud-information attribute-name="# e">65</ud-information>
            <ud-information attribute-name="# HIAT:u">11</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T66" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Taper</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">qula</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">jass</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">tuːattə</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Omduqwattə</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">saozniktə</ts>
                  <nts id="Seg_23" n="HIAT:ip">,</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">matoram</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">praikut</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">i</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_35" n="HIAT:w" s="T10">koimutčelʼe</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_38" n="HIAT:w" s="T11">čaʒən</ts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_42" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">Man</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">tʼaran</ts>
                  <nts id="Seg_48" n="HIAT:ip">:</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_50" n="HIAT:ip">“</nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">Teper</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">kak</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">soː</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">jen</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_65" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">Amdalta</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">i</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_73" n="HIAT:w" s="T20">kojmɨčaltə</ts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_77" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">A</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">me</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">ugon</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_88" n="HIAT:w" s="T24">qɨdan</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_91" n="HIAT:w" s="T25">moglapčuquzawtə</ts>
                  <nts id="Seg_92" n="HIAT:ip">,</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_95" n="HIAT:w" s="T26">moglapčuquzawtə</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_99" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">Teper</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">udlau</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">i</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">toblau</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">küzattə</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_117" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">Man</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">wes</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">mekuzau</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">i</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_131" n="HIAT:w" s="T36">qɨdan</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_134" n="HIAT:w" s="T37">i</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_137" n="HIAT:w" s="T38">nʼüʒə</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_140" n="HIAT:w" s="T39">tutdəkuzan</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_143" n="HIAT:w" s="T40">i</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_146" n="HIAT:w" s="T41">poj</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_149" n="HIAT:w" s="T42">tutdəkuzan</ts>
                  <nts id="Seg_150" n="HIAT:ip">.</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_153" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_155" n="HIAT:w" s="T43">A</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_158" n="HIAT:w" s="T44">teper</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_161" n="HIAT:w" s="T45">traxtorse</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_164" n="HIAT:w" s="T46">tä</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_167" n="HIAT:w" s="T47">nʼüʒə</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_170" n="HIAT:w" s="T48">tutdaltə</ts>
                  <nts id="Seg_171" n="HIAT:ip">.</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_174" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_176" n="HIAT:w" s="T49">Srazu</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_179" n="HIAT:w" s="T50">nagur</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_182" n="HIAT:w" s="T51">mäktɨm</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_185" n="HIAT:w" s="T52">tadɨrɨt</ts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_189" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_191" n="HIAT:w" s="T53">A</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_194" n="HIAT:w" s="T54">mäguntu</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_197" n="HIAT:w" s="T55">ugon</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_200" n="HIAT:w" s="T56">nagur</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_203" n="HIAT:w" s="T57">dʼel</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_206" n="HIAT:w" s="T58">nadə</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_209" n="HIAT:w" s="T59">tuddugu</ts>
                  <nts id="Seg_210" n="HIAT:ip">.</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_213" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_215" n="HIAT:w" s="T60">A</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_218" n="HIAT:w" s="T61">täper</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_221" n="HIAT:w" s="T62">srazu</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_224" n="HIAT:w" s="T63">nagur</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_227" n="HIAT:w" s="T64">mʼäktə</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_230" n="HIAT:w" s="T65">tatkut</ts>
                  <nts id="Seg_231" n="HIAT:ip">.</nts>
                  <nts id="Seg_232" n="HIAT:ip">”</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T66" id="Seg_234" n="sc" s="T1">
               <ts e="T2" id="Seg_236" n="e" s="T1">Taper </ts>
               <ts e="T3" id="Seg_238" n="e" s="T2">qula </ts>
               <ts e="T4" id="Seg_240" n="e" s="T3">jass </ts>
               <ts e="T5" id="Seg_242" n="e" s="T4">tuːattə. </ts>
               <ts e="T6" id="Seg_244" n="e" s="T5">Omduqwattə </ts>
               <ts e="T7" id="Seg_246" n="e" s="T6">saozniktə, </ts>
               <ts e="T8" id="Seg_248" n="e" s="T7">matoram </ts>
               <ts e="T9" id="Seg_250" n="e" s="T8">praikut </ts>
               <ts e="T10" id="Seg_252" n="e" s="T9">i </ts>
               <ts e="T11" id="Seg_254" n="e" s="T10">koimutčelʼe </ts>
               <ts e="T12" id="Seg_256" n="e" s="T11">čaʒən. </ts>
               <ts e="T13" id="Seg_258" n="e" s="T12">Man </ts>
               <ts e="T14" id="Seg_260" n="e" s="T13">tʼaran: </ts>
               <ts e="T15" id="Seg_262" n="e" s="T14">“Teper </ts>
               <ts e="T16" id="Seg_264" n="e" s="T15">kak </ts>
               <ts e="T17" id="Seg_266" n="e" s="T16">soː </ts>
               <ts e="T18" id="Seg_268" n="e" s="T17">jen. </ts>
               <ts e="T19" id="Seg_270" n="e" s="T18">Amdalta </ts>
               <ts e="T20" id="Seg_272" n="e" s="T19">i </ts>
               <ts e="T21" id="Seg_274" n="e" s="T20">kojmɨčaltə. </ts>
               <ts e="T22" id="Seg_276" n="e" s="T21">A </ts>
               <ts e="T23" id="Seg_278" n="e" s="T22">me </ts>
               <ts e="T24" id="Seg_280" n="e" s="T23">ugon </ts>
               <ts e="T25" id="Seg_282" n="e" s="T24">qɨdan </ts>
               <ts e="T26" id="Seg_284" n="e" s="T25">moglapčuquzawtə, </ts>
               <ts e="T27" id="Seg_286" n="e" s="T26">moglapčuquzawtə. </ts>
               <ts e="T28" id="Seg_288" n="e" s="T27">Teper </ts>
               <ts e="T29" id="Seg_290" n="e" s="T28">udlau </ts>
               <ts e="T30" id="Seg_292" n="e" s="T29">i </ts>
               <ts e="T31" id="Seg_294" n="e" s="T30">toblau </ts>
               <ts e="T32" id="Seg_296" n="e" s="T31">küzattə. </ts>
               <ts e="T33" id="Seg_298" n="e" s="T32">Man </ts>
               <ts e="T34" id="Seg_300" n="e" s="T33">wes </ts>
               <ts e="T35" id="Seg_302" n="e" s="T34">mekuzau </ts>
               <ts e="T36" id="Seg_304" n="e" s="T35">i </ts>
               <ts e="T37" id="Seg_306" n="e" s="T36">qɨdan </ts>
               <ts e="T38" id="Seg_308" n="e" s="T37">i </ts>
               <ts e="T39" id="Seg_310" n="e" s="T38">nʼüʒə </ts>
               <ts e="T40" id="Seg_312" n="e" s="T39">tutdəkuzan </ts>
               <ts e="T41" id="Seg_314" n="e" s="T40">i </ts>
               <ts e="T42" id="Seg_316" n="e" s="T41">poj </ts>
               <ts e="T43" id="Seg_318" n="e" s="T42">tutdəkuzan. </ts>
               <ts e="T44" id="Seg_320" n="e" s="T43">A </ts>
               <ts e="T45" id="Seg_322" n="e" s="T44">teper </ts>
               <ts e="T46" id="Seg_324" n="e" s="T45">traxtorse </ts>
               <ts e="T47" id="Seg_326" n="e" s="T46">tä </ts>
               <ts e="T48" id="Seg_328" n="e" s="T47">nʼüʒə </ts>
               <ts e="T49" id="Seg_330" n="e" s="T48">tutdaltə. </ts>
               <ts e="T50" id="Seg_332" n="e" s="T49">Srazu </ts>
               <ts e="T51" id="Seg_334" n="e" s="T50">nagur </ts>
               <ts e="T52" id="Seg_336" n="e" s="T51">mäktɨm </ts>
               <ts e="T53" id="Seg_338" n="e" s="T52">tadɨrɨt. </ts>
               <ts e="T54" id="Seg_340" n="e" s="T53">A </ts>
               <ts e="T55" id="Seg_342" n="e" s="T54">mäguntu </ts>
               <ts e="T56" id="Seg_344" n="e" s="T55">ugon </ts>
               <ts e="T57" id="Seg_346" n="e" s="T56">nagur </ts>
               <ts e="T58" id="Seg_348" n="e" s="T57">dʼel </ts>
               <ts e="T59" id="Seg_350" n="e" s="T58">nadə </ts>
               <ts e="T60" id="Seg_352" n="e" s="T59">tuddugu. </ts>
               <ts e="T61" id="Seg_354" n="e" s="T60">A </ts>
               <ts e="T62" id="Seg_356" n="e" s="T61">täper </ts>
               <ts e="T63" id="Seg_358" n="e" s="T62">srazu </ts>
               <ts e="T64" id="Seg_360" n="e" s="T63">nagur </ts>
               <ts e="T65" id="Seg_362" n="e" s="T64">mʼäktə </ts>
               <ts e="T66" id="Seg_364" n="e" s="T65">tatkut.” </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_365" s="T1">PVD_1964_DifficultLifeEarlier_nar.001 (001.001)</ta>
            <ta e="T12" id="Seg_366" s="T5">PVD_1964_DifficultLifeEarlier_nar.002 (001.002)</ta>
            <ta e="T18" id="Seg_367" s="T12">PVD_1964_DifficultLifeEarlier_nar.003 (001.003)</ta>
            <ta e="T21" id="Seg_368" s="T18">PVD_1964_DifficultLifeEarlier_nar.004 (001.004)</ta>
            <ta e="T27" id="Seg_369" s="T21">PVD_1964_DifficultLifeEarlier_nar.005 (001.005)</ta>
            <ta e="T32" id="Seg_370" s="T27">PVD_1964_DifficultLifeEarlier_nar.006 (001.006)</ta>
            <ta e="T43" id="Seg_371" s="T32">PVD_1964_DifficultLifeEarlier_nar.007 (001.007)</ta>
            <ta e="T49" id="Seg_372" s="T43">PVD_1964_DifficultLifeEarlier_nar.008 (001.008)</ta>
            <ta e="T53" id="Seg_373" s="T49">PVD_1964_DifficultLifeEarlier_nar.009 (001.009)</ta>
            <ta e="T60" id="Seg_374" s="T53">PVD_1964_DifficultLifeEarlier_nar.010 (001.010)</ta>
            <ta e="T66" id="Seg_375" s="T60">PVD_1964_DifficultLifeEarlier_nar.011 (001.011)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_376" s="T1">та′пе̨р kу′lа jасс ′тӯаттъ.</ta>
            <ta e="T12" id="Seg_377" s="T5">омду′kwаттъ са′озниктъ, ма′торам ′праикут и ′кои ′муттшелʼе тшажън.</ta>
            <ta e="T18" id="Seg_378" s="T12">ман тʼа′ран: те̨пер как со̄ jен.</ta>
            <ta e="T21" id="Seg_379" s="T18">′амдалта и кой мы′тшалтъ.</ta>
            <ta e="T27" id="Seg_380" s="T21">а ме у′гон ′kыдан мог′лаптшуkу′завтъ, мог′лаптшуkу′завтъ.</ta>
            <ta e="T32" id="Seg_381" s="T27">тепер уд′лау̹ и тоб′лау кӱ′заттъ.</ta>
            <ta e="T43" id="Seg_382" s="T32">ман вес ′мекузау̹ и ′kыдан и нʼӱжъ′тутдъ ку′зан и пой ′тутдъку′зан.</ta>
            <ta e="T49" id="Seg_383" s="T43">а те′пер тра′хто(ъ)рсе тӓ нʼӱжъ тут′далтъ.</ta>
            <ta e="T53" id="Seg_384" s="T49">сразу ′нагур ′мӓктым ′тадырыт.</ta>
            <ta e="T60" id="Seg_385" s="T53">а мӓ′гунту у′гон ′нагур ′дʼел ′надъ ′туд̂дугу.</ta>
            <ta e="T66" id="Seg_386" s="T60">а тӓ′пер сразу ′нагур ′мʼӓктъ ′таткут.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_387" s="T1">taper qula jass tuːattə.</ta>
            <ta e="T12" id="Seg_388" s="T5">omduqwattə saozniktə, matoram praikut i koi muttšelʼe tšaʒən.</ta>
            <ta e="T18" id="Seg_389" s="T12">man tʼaran: teper kak soː jen.</ta>
            <ta e="T21" id="Seg_390" s="T18">amdalta i koj mɨtšaltə.</ta>
            <ta e="T27" id="Seg_391" s="T21">a me ugon qɨdan moglaptšuquzawtə, moglaptšuquzawtə.</ta>
            <ta e="T32" id="Seg_392" s="T27">teper udlau̹ i toblau küzattə.</ta>
            <ta e="T43" id="Seg_393" s="T32">man wes mekuzau̹ i qɨdan i nʼüʒətutdə kuzan i poj tutdəkuzan.</ta>
            <ta e="T49" id="Seg_394" s="T43">a teper traxto(ə)rse tä nʼüʒə tutdaltə.</ta>
            <ta e="T53" id="Seg_395" s="T49">srazu nagur mäktɨm tadɨrɨt.</ta>
            <ta e="T60" id="Seg_396" s="T53">a mäguntu ugon nagur dʼel nadə tud̂dugu.</ta>
            <ta e="T66" id="Seg_397" s="T60">a täper srazu nagur mʼäktə tatkut.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_398" s="T1">Taper qula jass tuːattə. </ta>
            <ta e="T12" id="Seg_399" s="T5">Omduqwattə saozniktə, matoram praikut i koimutčelʼe čaʒən. </ta>
            <ta e="T18" id="Seg_400" s="T12">Man tʼaran: “Teper kak soː jen. </ta>
            <ta e="T21" id="Seg_401" s="T18">Amdalta i kojmɨčaltə. </ta>
            <ta e="T27" id="Seg_402" s="T21">A me ugon qɨdan moglapčuquzawtə, moglapčuquzawtə. </ta>
            <ta e="T32" id="Seg_403" s="T27">Teper udlau i toblau küzattə. </ta>
            <ta e="T43" id="Seg_404" s="T32">Man wes mekuzau i qɨdan i nʼüʒə tutdəkuzan i poj tutdəkuzan. </ta>
            <ta e="T49" id="Seg_405" s="T43">A teper traxtorse tä nʼüʒə tutdaltə. </ta>
            <ta e="T53" id="Seg_406" s="T49">Srazu nagur mäktɨm tadɨrɨt. </ta>
            <ta e="T60" id="Seg_407" s="T53">A mäguntu ugon nagur dʼel nadə tuddugu. </ta>
            <ta e="T66" id="Seg_408" s="T60">A täper srazu nagur mʼäktə tatkut.” </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_409" s="T1">taper</ta>
            <ta e="T3" id="Seg_410" s="T2">qu-la</ta>
            <ta e="T4" id="Seg_411" s="T3">jass</ta>
            <ta e="T5" id="Seg_412" s="T4">tuː-a-ttə</ta>
            <ta e="T6" id="Seg_413" s="T5">omdu-qwa-ttə</ta>
            <ta e="T7" id="Seg_414" s="T6">saoznik-tə</ta>
            <ta e="T8" id="Seg_415" s="T7">mator-a-m</ta>
            <ta e="T9" id="Seg_416" s="T8">prai-ku-t</ta>
            <ta e="T10" id="Seg_417" s="T9">i</ta>
            <ta e="T11" id="Seg_418" s="T10">koimu-tče-lʼe</ta>
            <ta e="T12" id="Seg_419" s="T11">čaʒə-n</ta>
            <ta e="T13" id="Seg_420" s="T12">man</ta>
            <ta e="T14" id="Seg_421" s="T13">tʼara-n</ta>
            <ta e="T15" id="Seg_422" s="T14">teper</ta>
            <ta e="T16" id="Seg_423" s="T15">kak</ta>
            <ta e="T17" id="Seg_424" s="T16">soː</ta>
            <ta e="T18" id="Seg_425" s="T17">je-n</ta>
            <ta e="T19" id="Seg_426" s="T18">amda-lta</ta>
            <ta e="T20" id="Seg_427" s="T19">i</ta>
            <ta e="T21" id="Seg_428" s="T20">kojmɨ-ča-ltə</ta>
            <ta e="T22" id="Seg_429" s="T21">a</ta>
            <ta e="T23" id="Seg_430" s="T22">me</ta>
            <ta e="T24" id="Seg_431" s="T23">ugon</ta>
            <ta e="T25" id="Seg_432" s="T24">qɨdan</ta>
            <ta e="T26" id="Seg_433" s="T25">moglapču-qu-za-wtə</ta>
            <ta e="T27" id="Seg_434" s="T26">moglapču-qu-za-wtə</ta>
            <ta e="T28" id="Seg_435" s="T27">teper</ta>
            <ta e="T29" id="Seg_436" s="T28">ud-la-u</ta>
            <ta e="T30" id="Seg_437" s="T29">i</ta>
            <ta e="T31" id="Seg_438" s="T30">tob-la-u</ta>
            <ta e="T32" id="Seg_439" s="T31">küza-ttə</ta>
            <ta e="T33" id="Seg_440" s="T32">man</ta>
            <ta e="T34" id="Seg_441" s="T33">wes</ta>
            <ta e="T35" id="Seg_442" s="T34">me-ku-za-u</ta>
            <ta e="T36" id="Seg_443" s="T35">i</ta>
            <ta e="T37" id="Seg_444" s="T36">qɨdan</ta>
            <ta e="T38" id="Seg_445" s="T37">i</ta>
            <ta e="T39" id="Seg_446" s="T38">nʼüʒə</ta>
            <ta e="T40" id="Seg_447" s="T39">tutdə-ku-za-n</ta>
            <ta e="T41" id="Seg_448" s="T40">i</ta>
            <ta e="T42" id="Seg_449" s="T41">po-j</ta>
            <ta e="T43" id="Seg_450" s="T42">tutdə-ku-za-n</ta>
            <ta e="T44" id="Seg_451" s="T43">a</ta>
            <ta e="T45" id="Seg_452" s="T44">teper</ta>
            <ta e="T46" id="Seg_453" s="T45">traxtor-se</ta>
            <ta e="T47" id="Seg_454" s="T46">tä</ta>
            <ta e="T48" id="Seg_455" s="T47">nʼüʒə</ta>
            <ta e="T49" id="Seg_456" s="T48">tutda-ltə</ta>
            <ta e="T50" id="Seg_457" s="T49">srazu</ta>
            <ta e="T51" id="Seg_458" s="T50">nagur</ta>
            <ta e="T52" id="Seg_459" s="T51">mäkt-ɨ-m</ta>
            <ta e="T53" id="Seg_460" s="T52">tad-ɨ-r-ɨ-t</ta>
            <ta e="T54" id="Seg_461" s="T53">a</ta>
            <ta e="T55" id="Seg_462" s="T54">mä-guntu</ta>
            <ta e="T56" id="Seg_463" s="T55">ugon</ta>
            <ta e="T57" id="Seg_464" s="T56">nagur</ta>
            <ta e="T58" id="Seg_465" s="T57">dʼel</ta>
            <ta e="T59" id="Seg_466" s="T58">nadə</ta>
            <ta e="T60" id="Seg_467" s="T59">tuddu-gu</ta>
            <ta e="T61" id="Seg_468" s="T60">a</ta>
            <ta e="T62" id="Seg_469" s="T61">täper</ta>
            <ta e="T63" id="Seg_470" s="T62">srazu</ta>
            <ta e="T64" id="Seg_471" s="T63">nagur</ta>
            <ta e="T65" id="Seg_472" s="T64">mʼäktə</ta>
            <ta e="T66" id="Seg_473" s="T65">tat-ku-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_474" s="T1">teper</ta>
            <ta e="T3" id="Seg_475" s="T2">qum-la</ta>
            <ta e="T4" id="Seg_476" s="T3">asa</ta>
            <ta e="T5" id="Seg_477" s="T4">tuː-nɨ-tɨn</ta>
            <ta e="T6" id="Seg_478" s="T5">omdɨ-ku-tɨn</ta>
            <ta e="T7" id="Seg_479" s="T6">zawoznʼik-ntə</ta>
            <ta e="T8" id="Seg_480" s="T7">mator-ɨ-m</ta>
            <ta e="T9" id="Seg_481" s="T8">prai-ku-tɨn</ta>
            <ta e="T10" id="Seg_482" s="T9">i</ta>
            <ta e="T11" id="Seg_483" s="T10">qojmɨ-ču-le</ta>
            <ta e="T12" id="Seg_484" s="T11">čaǯɨ-n</ta>
            <ta e="T13" id="Seg_485" s="T12">man</ta>
            <ta e="T14" id="Seg_486" s="T13">tʼärɨ-ŋ</ta>
            <ta e="T15" id="Seg_487" s="T14">teper</ta>
            <ta e="T16" id="Seg_488" s="T15">kak</ta>
            <ta e="T17" id="Seg_489" s="T16">soː</ta>
            <ta e="T18" id="Seg_490" s="T17">eː-n</ta>
            <ta e="T19" id="Seg_491" s="T18">amdɨ-lɨn</ta>
            <ta e="T20" id="Seg_492" s="T19">i</ta>
            <ta e="T21" id="Seg_493" s="T20">qojmɨ-ču-lɨn</ta>
            <ta e="T22" id="Seg_494" s="T21">a</ta>
            <ta e="T23" id="Seg_495" s="T22">me</ta>
            <ta e="T24" id="Seg_496" s="T23">ugon</ta>
            <ta e="T25" id="Seg_497" s="T24">qɨdan</ta>
            <ta e="T26" id="Seg_498" s="T25">moglapču-ku-sɨ-un</ta>
            <ta e="T27" id="Seg_499" s="T26">moglapču-ku-sɨ-un</ta>
            <ta e="T28" id="Seg_500" s="T27">teper</ta>
            <ta e="T29" id="Seg_501" s="T28">ut-la-w</ta>
            <ta e="T30" id="Seg_502" s="T29">i</ta>
            <ta e="T31" id="Seg_503" s="T30">tob-la-w</ta>
            <ta e="T32" id="Seg_504" s="T31">qüzu-tɨn</ta>
            <ta e="T33" id="Seg_505" s="T32">man</ta>
            <ta e="T34" id="Seg_506" s="T33">wesʼ</ta>
            <ta e="T35" id="Seg_507" s="T34">meː-ku-sɨ-w</ta>
            <ta e="T36" id="Seg_508" s="T35">i</ta>
            <ta e="T37" id="Seg_509" s="T36">qɨdan</ta>
            <ta e="T38" id="Seg_510" s="T37">i</ta>
            <ta e="T39" id="Seg_511" s="T38">nʼüʒə</ta>
            <ta e="T40" id="Seg_512" s="T39">tundɨ-ku-sɨ-ŋ</ta>
            <ta e="T41" id="Seg_513" s="T40">i</ta>
            <ta e="T42" id="Seg_514" s="T41">po-lʼ</ta>
            <ta e="T43" id="Seg_515" s="T42">tundɨ-ku-sɨ-ŋ</ta>
            <ta e="T44" id="Seg_516" s="T43">a</ta>
            <ta e="T45" id="Seg_517" s="T44">teper</ta>
            <ta e="T46" id="Seg_518" s="T45">traxtor-se</ta>
            <ta e="T47" id="Seg_519" s="T46">te</ta>
            <ta e="T48" id="Seg_520" s="T47">nʼüʒə</ta>
            <ta e="T49" id="Seg_521" s="T48">tundɨ-lɨn</ta>
            <ta e="T50" id="Seg_522" s="T49">srazu</ta>
            <ta e="T51" id="Seg_523" s="T50">nagur</ta>
            <ta e="T52" id="Seg_524" s="T51">mäkt-ɨ-m</ta>
            <ta e="T53" id="Seg_525" s="T52">tat-ɨ-r-ɨ-t</ta>
            <ta e="T54" id="Seg_526" s="T53">a</ta>
            <ta e="T55" id="Seg_527" s="T54">me-quntu</ta>
            <ta e="T56" id="Seg_528" s="T55">ugon</ta>
            <ta e="T57" id="Seg_529" s="T56">nagur</ta>
            <ta e="T58" id="Seg_530" s="T57">dʼel</ta>
            <ta e="T59" id="Seg_531" s="T58">nadə</ta>
            <ta e="T60" id="Seg_532" s="T59">tundɨ-gu</ta>
            <ta e="T61" id="Seg_533" s="T60">a</ta>
            <ta e="T62" id="Seg_534" s="T61">teper</ta>
            <ta e="T63" id="Seg_535" s="T62">srazu</ta>
            <ta e="T64" id="Seg_536" s="T63">nagur</ta>
            <ta e="T65" id="Seg_537" s="T64">mäkt</ta>
            <ta e="T66" id="Seg_538" s="T65">tat-ku-t</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_539" s="T1">now</ta>
            <ta e="T3" id="Seg_540" s="T2">human.being-PL.[NOM]</ta>
            <ta e="T4" id="Seg_541" s="T3">NEG</ta>
            <ta e="T5" id="Seg_542" s="T4">row-CO-3PL</ta>
            <ta e="T6" id="Seg_543" s="T5">sit.down-HAB-3PL</ta>
            <ta e="T7" id="Seg_544" s="T6">boat-ILL</ta>
            <ta e="T8" id="Seg_545" s="T7">motor-EP-ACC</ta>
            <ta e="T9" id="Seg_546" s="T8">steer-HAB-3PL</ta>
            <ta e="T10" id="Seg_547" s="T9">and</ta>
            <ta e="T11" id="Seg_548" s="T10">song-VBLZ-CVB</ta>
            <ta e="T12" id="Seg_549" s="T11">go-3SG.S</ta>
            <ta e="T13" id="Seg_550" s="T12">I.NOM</ta>
            <ta e="T14" id="Seg_551" s="T13">say-1SG.S</ta>
            <ta e="T15" id="Seg_552" s="T14">now</ta>
            <ta e="T16" id="Seg_553" s="T15">how</ta>
            <ta e="T17" id="Seg_554" s="T16">good</ta>
            <ta e="T18" id="Seg_555" s="T17">be-3SG.S</ta>
            <ta e="T19" id="Seg_556" s="T18">sit-2PL</ta>
            <ta e="T20" id="Seg_557" s="T19">and</ta>
            <ta e="T21" id="Seg_558" s="T20">song-VBLZ-2PL</ta>
            <ta e="T22" id="Seg_559" s="T21">and</ta>
            <ta e="T23" id="Seg_560" s="T22">we.[NOM]</ta>
            <ta e="T24" id="Seg_561" s="T23">earlier</ta>
            <ta e="T25" id="Seg_562" s="T24">all.the.time</ta>
            <ta e="T26" id="Seg_563" s="T25">row-HAB-PST-1PL</ta>
            <ta e="T27" id="Seg_564" s="T26">row-HAB-PST-1PL</ta>
            <ta e="T28" id="Seg_565" s="T27">now</ta>
            <ta e="T29" id="Seg_566" s="T28">hand-PL.[NOM]-1SG</ta>
            <ta e="T30" id="Seg_567" s="T29">and</ta>
            <ta e="T31" id="Seg_568" s="T30">leg-PL.[NOM]-1SG</ta>
            <ta e="T32" id="Seg_569" s="T31">ache-3PL</ta>
            <ta e="T33" id="Seg_570" s="T32">I.NOM</ta>
            <ta e="T34" id="Seg_571" s="T33">all</ta>
            <ta e="T35" id="Seg_572" s="T34">do-HAB-PST-1SG.O</ta>
            <ta e="T36" id="Seg_573" s="T35">and</ta>
            <ta e="T37" id="Seg_574" s="T36">all.the.time</ta>
            <ta e="T38" id="Seg_575" s="T37">and</ta>
            <ta e="T39" id="Seg_576" s="T38">hay.[NOM]</ta>
            <ta e="T40" id="Seg_577" s="T39">transport-HAB-PST-1SG.S</ta>
            <ta e="T41" id="Seg_578" s="T40">and</ta>
            <ta e="T42" id="Seg_579" s="T41">wood-ADJZ</ta>
            <ta e="T43" id="Seg_580" s="T42">transport-HAB-PST-1SG.S</ta>
            <ta e="T44" id="Seg_581" s="T43">and</ta>
            <ta e="T45" id="Seg_582" s="T44">now</ta>
            <ta e="T46" id="Seg_583" s="T45">tractor-INSTR</ta>
            <ta e="T47" id="Seg_584" s="T46">you.PL.NOM</ta>
            <ta e="T48" id="Seg_585" s="T47">hay.[NOM]</ta>
            <ta e="T49" id="Seg_586" s="T48">transport-2PL</ta>
            <ta e="T50" id="Seg_587" s="T49">at.once</ta>
            <ta e="T51" id="Seg_588" s="T50">three</ta>
            <ta e="T52" id="Seg_589" s="T51">rick-EP-ACC</ta>
            <ta e="T53" id="Seg_590" s="T52">bring-EP-FRQ-EP-3SG.O</ta>
            <ta e="T54" id="Seg_591" s="T53">and</ta>
            <ta e="T55" id="Seg_592" s="T54">we-LOC.1PL</ta>
            <ta e="T56" id="Seg_593" s="T55">earlier</ta>
            <ta e="T57" id="Seg_594" s="T56">three</ta>
            <ta e="T58" id="Seg_595" s="T57">day.[NOM]</ta>
            <ta e="T59" id="Seg_596" s="T58">one.should</ta>
            <ta e="T60" id="Seg_597" s="T59">transport-INF</ta>
            <ta e="T61" id="Seg_598" s="T60">and</ta>
            <ta e="T62" id="Seg_599" s="T61">now</ta>
            <ta e="T63" id="Seg_600" s="T62">at.once</ta>
            <ta e="T64" id="Seg_601" s="T63">three</ta>
            <ta e="T65" id="Seg_602" s="T64">rick.[NOM]</ta>
            <ta e="T66" id="Seg_603" s="T65">bring-HAB-3SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_604" s="T1">теперь</ta>
            <ta e="T3" id="Seg_605" s="T2">человек-PL.[NOM]</ta>
            <ta e="T4" id="Seg_606" s="T3">NEG</ta>
            <ta e="T5" id="Seg_607" s="T4">грести-CO-3PL</ta>
            <ta e="T6" id="Seg_608" s="T5">сесть-HAB-3PL</ta>
            <ta e="T7" id="Seg_609" s="T6">лодка-ILL</ta>
            <ta e="T8" id="Seg_610" s="T7">мотор-EP-ACC</ta>
            <ta e="T9" id="Seg_611" s="T8">направлять-HAB-3PL</ta>
            <ta e="T10" id="Seg_612" s="T9">и</ta>
            <ta e="T11" id="Seg_613" s="T10">песня-VBLZ-CVB</ta>
            <ta e="T12" id="Seg_614" s="T11">ходить-3SG.S</ta>
            <ta e="T13" id="Seg_615" s="T12">я.NOM</ta>
            <ta e="T14" id="Seg_616" s="T13">сказать-1SG.S</ta>
            <ta e="T15" id="Seg_617" s="T14">теперь</ta>
            <ta e="T16" id="Seg_618" s="T15">как</ta>
            <ta e="T17" id="Seg_619" s="T16">хороший</ta>
            <ta e="T18" id="Seg_620" s="T17">быть-3SG.S</ta>
            <ta e="T19" id="Seg_621" s="T18">сидеть-2PL</ta>
            <ta e="T20" id="Seg_622" s="T19">и</ta>
            <ta e="T21" id="Seg_623" s="T20">песня-VBLZ-2PL</ta>
            <ta e="T22" id="Seg_624" s="T21">а</ta>
            <ta e="T23" id="Seg_625" s="T22">мы.[NOM]</ta>
            <ta e="T24" id="Seg_626" s="T23">раньше</ta>
            <ta e="T25" id="Seg_627" s="T24">все.время</ta>
            <ta e="T26" id="Seg_628" s="T25">грести-HAB-PST-1PL</ta>
            <ta e="T27" id="Seg_629" s="T26">грести-HAB-PST-1PL</ta>
            <ta e="T28" id="Seg_630" s="T27">теперь</ta>
            <ta e="T29" id="Seg_631" s="T28">рука-PL.[NOM]-1SG</ta>
            <ta e="T30" id="Seg_632" s="T29">и</ta>
            <ta e="T31" id="Seg_633" s="T30">нога-PL.[NOM]-1SG</ta>
            <ta e="T32" id="Seg_634" s="T31">болеть-3PL</ta>
            <ta e="T33" id="Seg_635" s="T32">я.NOM</ta>
            <ta e="T34" id="Seg_636" s="T33">весь</ta>
            <ta e="T35" id="Seg_637" s="T34">сделать-HAB-PST-1SG.O</ta>
            <ta e="T36" id="Seg_638" s="T35">и</ta>
            <ta e="T37" id="Seg_639" s="T36">все.время</ta>
            <ta e="T38" id="Seg_640" s="T37">и</ta>
            <ta e="T39" id="Seg_641" s="T38">сено.[NOM]</ta>
            <ta e="T40" id="Seg_642" s="T39">возить-HAB-PST-1SG.S</ta>
            <ta e="T41" id="Seg_643" s="T40">и</ta>
            <ta e="T42" id="Seg_644" s="T41">дрова-ADJZ</ta>
            <ta e="T43" id="Seg_645" s="T42">возить-HAB-PST-1SG.S</ta>
            <ta e="T44" id="Seg_646" s="T43">а</ta>
            <ta e="T45" id="Seg_647" s="T44">теперь</ta>
            <ta e="T46" id="Seg_648" s="T45">трактор-INSTR</ta>
            <ta e="T47" id="Seg_649" s="T46">вы.PL.NOM</ta>
            <ta e="T48" id="Seg_650" s="T47">сено.[NOM]</ta>
            <ta e="T49" id="Seg_651" s="T48">возить-2PL</ta>
            <ta e="T50" id="Seg_652" s="T49">сразу</ta>
            <ta e="T51" id="Seg_653" s="T50">три</ta>
            <ta e="T52" id="Seg_654" s="T51">стог-EP-ACC</ta>
            <ta e="T53" id="Seg_655" s="T52">привезти-EP-FRQ-EP-3SG.O</ta>
            <ta e="T54" id="Seg_656" s="T53">а</ta>
            <ta e="T55" id="Seg_657" s="T54">мы-LOC.1PL</ta>
            <ta e="T56" id="Seg_658" s="T55">раньше</ta>
            <ta e="T57" id="Seg_659" s="T56">три</ta>
            <ta e="T58" id="Seg_660" s="T57">день.[NOM]</ta>
            <ta e="T59" id="Seg_661" s="T58">надо</ta>
            <ta e="T60" id="Seg_662" s="T59">возить-INF</ta>
            <ta e="T61" id="Seg_663" s="T60">а</ta>
            <ta e="T62" id="Seg_664" s="T61">теперь</ta>
            <ta e="T63" id="Seg_665" s="T62">сразу</ta>
            <ta e="T64" id="Seg_666" s="T63">три</ta>
            <ta e="T65" id="Seg_667" s="T64">стог.[NOM]</ta>
            <ta e="T66" id="Seg_668" s="T65">привезти-HAB-3SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_669" s="T1">adv</ta>
            <ta e="T3" id="Seg_670" s="T2">n-n:num.[n:case]</ta>
            <ta e="T4" id="Seg_671" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_672" s="T4">v-v:ins-v:pn</ta>
            <ta e="T6" id="Seg_673" s="T5">v-v&gt;v-v:pn</ta>
            <ta e="T7" id="Seg_674" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_675" s="T7">n-n:ins-n:case</ta>
            <ta e="T9" id="Seg_676" s="T8">v-v&gt;v-v:pn</ta>
            <ta e="T10" id="Seg_677" s="T9">conj</ta>
            <ta e="T11" id="Seg_678" s="T10">n-n&gt;v-v&gt;adv</ta>
            <ta e="T12" id="Seg_679" s="T11">v-v:pn</ta>
            <ta e="T13" id="Seg_680" s="T12">pers</ta>
            <ta e="T14" id="Seg_681" s="T13">v-v:pn</ta>
            <ta e="T15" id="Seg_682" s="T14">adv</ta>
            <ta e="T16" id="Seg_683" s="T15">interrog</ta>
            <ta e="T17" id="Seg_684" s="T16">adj</ta>
            <ta e="T18" id="Seg_685" s="T17">v-v:pn</ta>
            <ta e="T19" id="Seg_686" s="T18">v-v:pn</ta>
            <ta e="T20" id="Seg_687" s="T19">conj</ta>
            <ta e="T21" id="Seg_688" s="T20">n-n&gt;v-v:pn</ta>
            <ta e="T22" id="Seg_689" s="T21">conj</ta>
            <ta e="T23" id="Seg_690" s="T22">pers.[n:case]</ta>
            <ta e="T24" id="Seg_691" s="T23">adv</ta>
            <ta e="T25" id="Seg_692" s="T24">adv</ta>
            <ta e="T26" id="Seg_693" s="T25">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_694" s="T26">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_695" s="T27">adv</ta>
            <ta e="T29" id="Seg_696" s="T28">n-n:num.[n:case]-n:poss</ta>
            <ta e="T30" id="Seg_697" s="T29">conj</ta>
            <ta e="T31" id="Seg_698" s="T30">n-n:num.[n:case]-n:poss</ta>
            <ta e="T32" id="Seg_699" s="T31">v-v:pn</ta>
            <ta e="T33" id="Seg_700" s="T32">pers</ta>
            <ta e="T34" id="Seg_701" s="T33">quant</ta>
            <ta e="T35" id="Seg_702" s="T34">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_703" s="T35">conj</ta>
            <ta e="T37" id="Seg_704" s="T36">adv</ta>
            <ta e="T38" id="Seg_705" s="T37">conj</ta>
            <ta e="T39" id="Seg_706" s="T38">n.[n:case]</ta>
            <ta e="T40" id="Seg_707" s="T39">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_708" s="T40">conj</ta>
            <ta e="T42" id="Seg_709" s="T41">n-n&gt;adj</ta>
            <ta e="T43" id="Seg_710" s="T42">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_711" s="T43">conj</ta>
            <ta e="T45" id="Seg_712" s="T44">adv</ta>
            <ta e="T46" id="Seg_713" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_714" s="T46">pers</ta>
            <ta e="T48" id="Seg_715" s="T47">n.[n:case]</ta>
            <ta e="T49" id="Seg_716" s="T48">v-v:pn</ta>
            <ta e="T50" id="Seg_717" s="T49">adv</ta>
            <ta e="T51" id="Seg_718" s="T50">num</ta>
            <ta e="T52" id="Seg_719" s="T51">n-n:ins-n:case</ta>
            <ta e="T53" id="Seg_720" s="T52">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T54" id="Seg_721" s="T53">conj</ta>
            <ta e="T55" id="Seg_722" s="T54">pers-n:case.poss</ta>
            <ta e="T56" id="Seg_723" s="T55">adv</ta>
            <ta e="T57" id="Seg_724" s="T56">num</ta>
            <ta e="T58" id="Seg_725" s="T57">n.[n:case]</ta>
            <ta e="T59" id="Seg_726" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_727" s="T59">v-v:inf</ta>
            <ta e="T61" id="Seg_728" s="T60">conj</ta>
            <ta e="T62" id="Seg_729" s="T61">adv</ta>
            <ta e="T63" id="Seg_730" s="T62">adv</ta>
            <ta e="T64" id="Seg_731" s="T63">num</ta>
            <ta e="T65" id="Seg_732" s="T64">n.[n:case]</ta>
            <ta e="T66" id="Seg_733" s="T65">v-v&gt;v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_734" s="T1">adv</ta>
            <ta e="T3" id="Seg_735" s="T2">n</ta>
            <ta e="T4" id="Seg_736" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_737" s="T4">v</ta>
            <ta e="T6" id="Seg_738" s="T5">v</ta>
            <ta e="T7" id="Seg_739" s="T6">n</ta>
            <ta e="T8" id="Seg_740" s="T7">n</ta>
            <ta e="T9" id="Seg_741" s="T8">v</ta>
            <ta e="T10" id="Seg_742" s="T9">conj</ta>
            <ta e="T11" id="Seg_743" s="T10">adv</ta>
            <ta e="T12" id="Seg_744" s="T11">v</ta>
            <ta e="T13" id="Seg_745" s="T12">pers</ta>
            <ta e="T14" id="Seg_746" s="T13">v</ta>
            <ta e="T15" id="Seg_747" s="T14">adv</ta>
            <ta e="T16" id="Seg_748" s="T15">interrog</ta>
            <ta e="T17" id="Seg_749" s="T16">adj</ta>
            <ta e="T18" id="Seg_750" s="T17">v</ta>
            <ta e="T19" id="Seg_751" s="T18">v</ta>
            <ta e="T20" id="Seg_752" s="T19">conj</ta>
            <ta e="T21" id="Seg_753" s="T20">n</ta>
            <ta e="T22" id="Seg_754" s="T21">conj</ta>
            <ta e="T23" id="Seg_755" s="T22">pers</ta>
            <ta e="T24" id="Seg_756" s="T23">adv</ta>
            <ta e="T25" id="Seg_757" s="T24">adv</ta>
            <ta e="T26" id="Seg_758" s="T25">v</ta>
            <ta e="T27" id="Seg_759" s="T26">v</ta>
            <ta e="T28" id="Seg_760" s="T27">adv</ta>
            <ta e="T29" id="Seg_761" s="T28">n</ta>
            <ta e="T30" id="Seg_762" s="T29">conj</ta>
            <ta e="T31" id="Seg_763" s="T30">n</ta>
            <ta e="T32" id="Seg_764" s="T31">v</ta>
            <ta e="T33" id="Seg_765" s="T32">pers</ta>
            <ta e="T34" id="Seg_766" s="T33">quant</ta>
            <ta e="T35" id="Seg_767" s="T34">v</ta>
            <ta e="T36" id="Seg_768" s="T35">conj</ta>
            <ta e="T37" id="Seg_769" s="T36">adv</ta>
            <ta e="T38" id="Seg_770" s="T37">conj</ta>
            <ta e="T39" id="Seg_771" s="T38">n</ta>
            <ta e="T40" id="Seg_772" s="T39">v</ta>
            <ta e="T41" id="Seg_773" s="T40">conj</ta>
            <ta e="T42" id="Seg_774" s="T41">adj</ta>
            <ta e="T43" id="Seg_775" s="T42">v</ta>
            <ta e="T44" id="Seg_776" s="T43">conj</ta>
            <ta e="T45" id="Seg_777" s="T44">adv</ta>
            <ta e="T46" id="Seg_778" s="T45">n</ta>
            <ta e="T47" id="Seg_779" s="T46">pers</ta>
            <ta e="T48" id="Seg_780" s="T47">n</ta>
            <ta e="T49" id="Seg_781" s="T48">v</ta>
            <ta e="T50" id="Seg_782" s="T49">adv</ta>
            <ta e="T51" id="Seg_783" s="T50">num</ta>
            <ta e="T52" id="Seg_784" s="T51">n</ta>
            <ta e="T53" id="Seg_785" s="T52">v</ta>
            <ta e="T54" id="Seg_786" s="T53">conj</ta>
            <ta e="T55" id="Seg_787" s="T54">pers</ta>
            <ta e="T56" id="Seg_788" s="T55">adv</ta>
            <ta e="T57" id="Seg_789" s="T56">num</ta>
            <ta e="T58" id="Seg_790" s="T57">n</ta>
            <ta e="T59" id="Seg_791" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_792" s="T59">v</ta>
            <ta e="T61" id="Seg_793" s="T60">conj</ta>
            <ta e="T62" id="Seg_794" s="T61">adv</ta>
            <ta e="T63" id="Seg_795" s="T62">adv</ta>
            <ta e="T64" id="Seg_796" s="T63">num</ta>
            <ta e="T65" id="Seg_797" s="T64">n</ta>
            <ta e="T66" id="Seg_798" s="T65">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_799" s="T1">adv:Time</ta>
            <ta e="T3" id="Seg_800" s="T2">np.h:A</ta>
            <ta e="T6" id="Seg_801" s="T5">0.3.h:A</ta>
            <ta e="T7" id="Seg_802" s="T6">np:G</ta>
            <ta e="T8" id="Seg_803" s="T7">np:Th</ta>
            <ta e="T9" id="Seg_804" s="T8">0.3.h:A</ta>
            <ta e="T12" id="Seg_805" s="T11">0.3.h:A</ta>
            <ta e="T13" id="Seg_806" s="T12">pro.h:A</ta>
            <ta e="T15" id="Seg_807" s="T14">adv:Time</ta>
            <ta e="T18" id="Seg_808" s="T17">0.3:Th</ta>
            <ta e="T19" id="Seg_809" s="T18">0.2.h:A</ta>
            <ta e="T21" id="Seg_810" s="T20">0.2.h:A</ta>
            <ta e="T23" id="Seg_811" s="T22">pro.h:A</ta>
            <ta e="T24" id="Seg_812" s="T23">adv:Time</ta>
            <ta e="T27" id="Seg_813" s="T26">0.1.h:A</ta>
            <ta e="T28" id="Seg_814" s="T27">adv:Time</ta>
            <ta e="T29" id="Seg_815" s="T28">np:P 0.1.h:Poss</ta>
            <ta e="T31" id="Seg_816" s="T30">np:P 0.1.h:Poss</ta>
            <ta e="T33" id="Seg_817" s="T32">pro.h:A</ta>
            <ta e="T34" id="Seg_818" s="T33">pro:Th</ta>
            <ta e="T39" id="Seg_819" s="T38">np:Th</ta>
            <ta e="T40" id="Seg_820" s="T39">0.1.h:A</ta>
            <ta e="T42" id="Seg_821" s="T41">np:Th</ta>
            <ta e="T43" id="Seg_822" s="T42">0.1.h:A</ta>
            <ta e="T45" id="Seg_823" s="T44">adv:Time</ta>
            <ta e="T46" id="Seg_824" s="T45">np:Ins</ta>
            <ta e="T47" id="Seg_825" s="T46">pro.h:A</ta>
            <ta e="T48" id="Seg_826" s="T47">np:Th</ta>
            <ta e="T52" id="Seg_827" s="T51">np:Th</ta>
            <ta e="T53" id="Seg_828" s="T52">0.3:A</ta>
            <ta e="T58" id="Seg_829" s="T57">np:Th</ta>
            <ta e="T62" id="Seg_830" s="T61">adv:Time</ta>
            <ta e="T65" id="Seg_831" s="T64">np:Th</ta>
            <ta e="T66" id="Seg_832" s="T65">0.3:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_833" s="T2">np.h:S</ta>
            <ta e="T5" id="Seg_834" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_835" s="T5">0.3.h:S v:pred</ta>
            <ta e="T8" id="Seg_836" s="T7">np:O</ta>
            <ta e="T9" id="Seg_837" s="T8">0.3.h:S v:pred</ta>
            <ta e="T12" id="Seg_838" s="T11">0.3.h:S v:pred</ta>
            <ta e="T13" id="Seg_839" s="T12">pro.h:S</ta>
            <ta e="T14" id="Seg_840" s="T13">v:pred</ta>
            <ta e="T17" id="Seg_841" s="T16">adj:pred</ta>
            <ta e="T18" id="Seg_842" s="T17">0.3:S cop</ta>
            <ta e="T19" id="Seg_843" s="T18">0.2.h:S v:pred</ta>
            <ta e="T21" id="Seg_844" s="T20">0.2.h:S v:pred</ta>
            <ta e="T23" id="Seg_845" s="T22">pro.h:S</ta>
            <ta e="T26" id="Seg_846" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_847" s="T26">0.1.h:S v:pred</ta>
            <ta e="T29" id="Seg_848" s="T28">np:S</ta>
            <ta e="T31" id="Seg_849" s="T30">np:S</ta>
            <ta e="T32" id="Seg_850" s="T31">v:pred</ta>
            <ta e="T33" id="Seg_851" s="T32">pro.h:S</ta>
            <ta e="T34" id="Seg_852" s="T33">pro:O</ta>
            <ta e="T35" id="Seg_853" s="T34">v:pred</ta>
            <ta e="T39" id="Seg_854" s="T38">np:O</ta>
            <ta e="T40" id="Seg_855" s="T39">0.1.h:S v:pred</ta>
            <ta e="T42" id="Seg_856" s="T41">np:O</ta>
            <ta e="T43" id="Seg_857" s="T42">0.1.h:S v:pred</ta>
            <ta e="T47" id="Seg_858" s="T46">pro.h:S</ta>
            <ta e="T48" id="Seg_859" s="T47">np:O</ta>
            <ta e="T49" id="Seg_860" s="T48">v:pred</ta>
            <ta e="T52" id="Seg_861" s="T51">np:O</ta>
            <ta e="T53" id="Seg_862" s="T52">0.3:S v:pred</ta>
            <ta e="T58" id="Seg_863" s="T57">np:O</ta>
            <ta e="T59" id="Seg_864" s="T58">ptcl:pred</ta>
            <ta e="T60" id="Seg_865" s="T59">s:purp</ta>
            <ta e="T65" id="Seg_866" s="T64">np:O</ta>
            <ta e="T66" id="Seg_867" s="T65">0.3:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_868" s="T1">RUS:core</ta>
            <ta e="T7" id="Seg_869" s="T6">RUS:cult</ta>
            <ta e="T8" id="Seg_870" s="T7">RUS:cult</ta>
            <ta e="T9" id="Seg_871" s="T8">RUS:cult</ta>
            <ta e="T10" id="Seg_872" s="T9">RUS:gram</ta>
            <ta e="T15" id="Seg_873" s="T14">RUS:core</ta>
            <ta e="T16" id="Seg_874" s="T15">RUS:gram</ta>
            <ta e="T20" id="Seg_875" s="T19">RUS:gram</ta>
            <ta e="T22" id="Seg_876" s="T21">RUS:gram</ta>
            <ta e="T28" id="Seg_877" s="T27">RUS:core</ta>
            <ta e="T30" id="Seg_878" s="T29">RUS:gram</ta>
            <ta e="T34" id="Seg_879" s="T33">RUS:core</ta>
            <ta e="T36" id="Seg_880" s="T35">RUS:gram</ta>
            <ta e="T38" id="Seg_881" s="T37">RUS:gram</ta>
            <ta e="T41" id="Seg_882" s="T40">RUS:gram</ta>
            <ta e="T44" id="Seg_883" s="T43">RUS:gram</ta>
            <ta e="T45" id="Seg_884" s="T44">RUS:core</ta>
            <ta e="T46" id="Seg_885" s="T45">RUS:cult</ta>
            <ta e="T50" id="Seg_886" s="T49">RUS:core</ta>
            <ta e="T54" id="Seg_887" s="T53">RUS:gram</ta>
            <ta e="T59" id="Seg_888" s="T58">RUS:mod</ta>
            <ta e="T61" id="Seg_889" s="T60">RUS:gram</ta>
            <ta e="T62" id="Seg_890" s="T61">RUS:core</ta>
            <ta e="T63" id="Seg_891" s="T62">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_892" s="T1">Now people don't row.</ta>
            <ta e="T12" id="Seg_893" s="T5">They get into a boat, one steers the motor and sings songs.</ta>
            <ta e="T18" id="Seg_894" s="T12">I said: “Now it's so easy.</ta>
            <ta e="T21" id="Seg_895" s="T18">You board and sing.</ta>
            <ta e="T27" id="Seg_896" s="T21">And earlier we had to row all the time.</ta>
            <ta e="T32" id="Seg_897" s="T27">My hands and legs hurt.</ta>
            <ta e="T43" id="Seg_898" s="T32">I used to do everything, I used to transport hay, I used to transport firewood.</ta>
            <ta e="T49" id="Seg_899" s="T43">And now you transport hay with a tractor.</ta>
            <ta e="T53" id="Seg_900" s="T49">It transports three haystacks at once. </ta>
            <ta e="T60" id="Seg_901" s="T53">And earlier we needed three days to transport [it].</ta>
            <ta e="T66" id="Seg_902" s="T60">And now it transports three haystacks at once.”</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_903" s="T1">Heutzutage rudern die Leute nicht.</ta>
            <ta e="T12" id="Seg_904" s="T5">Sie gehen aufs Boot, einer steuert den Motor und singt Lieder.</ta>
            <ta e="T18" id="Seg_905" s="T12">Ich sagte: "Jetzt ist es so leicht.</ta>
            <ta e="T21" id="Seg_906" s="T18">Ihr geht an Bord und singt.</ta>
            <ta e="T27" id="Seg_907" s="T21">Und wir mussten früher immer rudern.</ta>
            <ta e="T32" id="Seg_908" s="T27">Jetzt schmerzen meine Hände und Beine.</ta>
            <ta e="T43" id="Seg_909" s="T32">Ich habe für gewöhnlich alles gemacht, ich habe Heu transportiert, ich habe Feuerholz transportiert.</ta>
            <ta e="T49" id="Seg_910" s="T43">Und jetzt transportiert ihr Heu mit dem Traktor.</ta>
            <ta e="T53" id="Seg_911" s="T49">Er transportiert drei Heuballen gleichzeitig.</ta>
            <ta e="T60" id="Seg_912" s="T53">Und wir brauchten früher drei Tage um [sie] zu transportieren.</ta>
            <ta e="T66" id="Seg_913" s="T60">Und jetzt transportiert er drei Heuballen gleichzeitig.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_914" s="T1">Теперь люди не гребут.</ta>
            <ta e="T12" id="Seg_915" s="T5">Садятся в лодку, мотор направляют (направляет?) и песенки поет.</ta>
            <ta e="T18" id="Seg_916" s="T12">Я сказала: “Теперь как хорошо.</ta>
            <ta e="T21" id="Seg_917" s="T18">Садитесь и песни поете.</ta>
            <ta e="T27" id="Seg_918" s="T21">А мы раньше все гребли, гребли.</ta>
            <ta e="T32" id="Seg_919" s="T27">Теперь руки и ноги болят.</ta>
            <ta e="T43" id="Seg_920" s="T32">Я все делала и все время и сено возила, и дрова возила.</ta>
            <ta e="T49" id="Seg_921" s="T43">А теперь трактором вы сено возите.</ta>
            <ta e="T53" id="Seg_922" s="T49">Сразу три стога везет.</ta>
            <ta e="T60" id="Seg_923" s="T53">А нам раньше три дня надо было возить.</ta>
            <ta e="T66" id="Seg_924" s="T60">А теперь сразу три стога везет.”</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_925" s="T1">теперь люди не гребут</ta>
            <ta e="T12" id="Seg_926" s="T5">садятся в лодку, мотор направляют и песенки поют</ta>
            <ta e="T18" id="Seg_927" s="T12">я сказала: теперь как хорошо</ta>
            <ta e="T21" id="Seg_928" s="T18">садитесь и песни поете</ta>
            <ta e="T27" id="Seg_929" s="T21">а мы раньше все гребли, гребли</ta>
            <ta e="T32" id="Seg_930" s="T27">теперь руки и ноги болят</ta>
            <ta e="T43" id="Seg_931" s="T32">я все делала и все время сено и дрова возила</ta>
            <ta e="T49" id="Seg_932" s="T43">а теперь трактором сено возите</ta>
            <ta e="T53" id="Seg_933" s="T49">сразу три стога везет</ta>
            <ta e="T60" id="Seg_934" s="T53">а нам раньше три дня надо было возить</ta>
            <ta e="T66" id="Seg_935" s="T60">а теперь сразу везет три стога</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T12" id="Seg_936" s="T5">[BrM:] 'koi mutčelʼe' changed to 'koimutčelʼe'</ta>
            <ta e="T21" id="Seg_937" s="T18">[BrM:] 'koj mɨčaltə' changed to 'kojmɨčaltə'.</ta>
            <ta e="T43" id="Seg_938" s="T32">[BrM:] 'nʼüʒətutdə kuzan' changed to 'nʼüʒə tutdəkuzan'.</ta>
            <ta e="T49" id="Seg_939" s="T43">[KuAI:] Variant: 'traxtərse'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
