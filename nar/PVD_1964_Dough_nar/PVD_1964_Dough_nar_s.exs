<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Dough_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Dough_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">36</ud-information>
            <ud-information attribute-name="# HIAT:w">28</ud-information>
            <ud-information attribute-name="# e">28</ud-information>
            <ud-information attribute-name="# HIAT:u">7</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T29" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Qɨgaim</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">nadə</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">panalgu</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Testam</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">toqtugu</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">nadə</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">Man</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">testaj</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">kɨdan</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">doqtuquʒəquzan</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_41" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">A</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">täpar</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">aulǯupu</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_53" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">Kak</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">omdelǯeǯal</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">pötöt</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">nʼäjlam</ts>
                  <nts id="Seg_65" n="HIAT:ip">,</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">tebla</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">jass</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">müzeǯattə</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_78" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">Oqkɨr</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">qwašnʼäm</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">portinɨǯal</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_90" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_92" n="HIAT:w" s="T24">A</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">sədəmdettə</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">qwašnʼäm</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">soːdigan</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">meǯal</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T29" id="Seg_107" n="sc" s="T1">
               <ts e="T2" id="Seg_109" n="e" s="T1">Qɨgaim </ts>
               <ts e="T3" id="Seg_111" n="e" s="T2">nadə </ts>
               <ts e="T4" id="Seg_113" n="e" s="T3">panalgu. </ts>
               <ts e="T5" id="Seg_115" n="e" s="T4">Testam </ts>
               <ts e="T6" id="Seg_117" n="e" s="T5">toqtugu </ts>
               <ts e="T7" id="Seg_119" n="e" s="T6">nadə. </ts>
               <ts e="T8" id="Seg_121" n="e" s="T7">Man </ts>
               <ts e="T9" id="Seg_123" n="e" s="T8">testaj </ts>
               <ts e="T10" id="Seg_125" n="e" s="T9">kɨdan </ts>
               <ts e="T11" id="Seg_127" n="e" s="T10">doqtuquʒəquzan. </ts>
               <ts e="T12" id="Seg_129" n="e" s="T11">A </ts>
               <ts e="T13" id="Seg_131" n="e" s="T12">täpar </ts>
               <ts e="T14" id="Seg_133" n="e" s="T13">aulǯupu. </ts>
               <ts e="T15" id="Seg_135" n="e" s="T14">Kak </ts>
               <ts e="T16" id="Seg_137" n="e" s="T15">omdelǯeǯal </ts>
               <ts e="T17" id="Seg_139" n="e" s="T16">pötöt </ts>
               <ts e="T18" id="Seg_141" n="e" s="T17">nʼäjlam, </ts>
               <ts e="T19" id="Seg_143" n="e" s="T18">tebla </ts>
               <ts e="T20" id="Seg_145" n="e" s="T19">jass </ts>
               <ts e="T21" id="Seg_147" n="e" s="T20">müzeǯattə. </ts>
               <ts e="T22" id="Seg_149" n="e" s="T21">Oqkɨr </ts>
               <ts e="T23" id="Seg_151" n="e" s="T22">qwašnʼäm </ts>
               <ts e="T24" id="Seg_153" n="e" s="T23">portinɨǯal. </ts>
               <ts e="T25" id="Seg_155" n="e" s="T24">A </ts>
               <ts e="T26" id="Seg_157" n="e" s="T25">sədəmdettə </ts>
               <ts e="T27" id="Seg_159" n="e" s="T26">qwašnʼäm </ts>
               <ts e="T28" id="Seg_161" n="e" s="T27">soːdigan </ts>
               <ts e="T29" id="Seg_163" n="e" s="T28">meǯal. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_164" s="T1">PVD_1964_Dough_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_165" s="T4">PVD_1964_Dough_nar.002 (001.002)</ta>
            <ta e="T11" id="Seg_166" s="T7">PVD_1964_Dough_nar.003 (001.003)</ta>
            <ta e="T14" id="Seg_167" s="T11">PVD_1964_Dough_nar.004 (001.004)</ta>
            <ta e="T21" id="Seg_168" s="T14">PVD_1964_Dough_nar.005 (001.005)</ta>
            <ta e="T24" id="Seg_169" s="T21">PVD_1964_Dough_nar.006 (001.006)</ta>
            <ta e="T29" id="Seg_170" s="T24">PVD_1964_Dough_nar.007 (001.007)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_171" s="T1">k(к)ы′гаим надъ па′налгу.</ta>
            <ta e="T7" id="Seg_172" s="T4">′тестам ′тоkтугу надъ.</ta>
            <ta e="T11" id="Seg_173" s="T7">ман ′тестай ′кыдан д̂оkтуkужъkу′зан.</ta>
            <ta e="T14" id="Seg_174" s="T11">а тӓпар ′ауlджупу.</ta>
            <ta e="T21" id="Seg_175" s="T14">как омдел′джеджал ′пӧтӧт нʼӓйлам, теб′ла jасс мӱ′зеджаттъ.</ta>
            <ta e="T24" id="Seg_176" s="T21">оk′кыр kwашнʼӓм ′портиныджал.</ta>
            <ta e="T29" id="Seg_177" s="T24">а съдъмдеттъ kwашнʼӓм ′со̄ди′ган ′меджал.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_178" s="T1">q(k)ɨgaim nadə panalgu.</ta>
            <ta e="T7" id="Seg_179" s="T4">testam toqtugu nadə.</ta>
            <ta e="T11" id="Seg_180" s="T7">man testaj kɨdan d̂oqtuquʒəquzan.</ta>
            <ta e="T14" id="Seg_181" s="T11">a täpar aulǯupu.</ta>
            <ta e="T21" id="Seg_182" s="T14">kak omdelǯeǯal pötöt nʼäjlam, tebla jass müzeǯattə.</ta>
            <ta e="T24" id="Seg_183" s="T21">oqkɨr qwašnʼäm portinɨǯal.</ta>
            <ta e="T29" id="Seg_184" s="T24">a sədəmdettə qwašnʼäm soːdigan meǯal.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_185" s="T1">Qɨgaim nadə panalgu. </ta>
            <ta e="T7" id="Seg_186" s="T4">Testam toqtugu nadə. </ta>
            <ta e="T11" id="Seg_187" s="T7">Man testaj kɨdan doqtuquʒəquzan. </ta>
            <ta e="T14" id="Seg_188" s="T11">A täpar aulǯupu. </ta>
            <ta e="T21" id="Seg_189" s="T14">Kak omdelǯeǯal pötöt nʼäjlam, tebla jass müzeǯattə. </ta>
            <ta e="T24" id="Seg_190" s="T21">Oqkɨr qwašnʼäm portinɨǯal. </ta>
            <ta e="T29" id="Seg_191" s="T24">A sədəmdettə qwašnʼäm soːdigan meǯal. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_192" s="T1">qɨgai-m</ta>
            <ta e="T3" id="Seg_193" s="T2">nadə</ta>
            <ta e="T4" id="Seg_194" s="T3">pan-al-gu</ta>
            <ta e="T5" id="Seg_195" s="T4">testa-m</ta>
            <ta e="T6" id="Seg_196" s="T5">toqtu-gu</ta>
            <ta e="T7" id="Seg_197" s="T6">nadə</ta>
            <ta e="T8" id="Seg_198" s="T7">man</ta>
            <ta e="T9" id="Seg_199" s="T8">testa-j</ta>
            <ta e="T10" id="Seg_200" s="T9">kɨdan</ta>
            <ta e="T11" id="Seg_201" s="T10">doqtu-qu-ʒə-qu-za-n</ta>
            <ta e="T12" id="Seg_202" s="T11">a</ta>
            <ta e="T13" id="Seg_203" s="T12">täpar</ta>
            <ta e="T14" id="Seg_204" s="T13">aulǯu-pu</ta>
            <ta e="T15" id="Seg_205" s="T14">kak</ta>
            <ta e="T16" id="Seg_206" s="T15">omde-lǯ-eǯa-l</ta>
            <ta e="T17" id="Seg_207" s="T16">pötö-t</ta>
            <ta e="T18" id="Seg_208" s="T17">nʼäj-la-m</ta>
            <ta e="T19" id="Seg_209" s="T18">teb-la</ta>
            <ta e="T20" id="Seg_210" s="T19">jass</ta>
            <ta e="T21" id="Seg_211" s="T20">müz-eǯa-ttə</ta>
            <ta e="T22" id="Seg_212" s="T21">oqkɨr</ta>
            <ta e="T23" id="Seg_213" s="T22">qwašnʼä-m</ta>
            <ta e="T24" id="Seg_214" s="T23">portinɨ-ǯa-l</ta>
            <ta e="T25" id="Seg_215" s="T24">a</ta>
            <ta e="T26" id="Seg_216" s="T25">sədə-mdettə</ta>
            <ta e="T27" id="Seg_217" s="T26">qwašnʼä-m</ta>
            <ta e="T28" id="Seg_218" s="T27">soːdiga-n</ta>
            <ta e="T29" id="Seg_219" s="T28">me-ǯa-l</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_220" s="T1">qɨgaj-m</ta>
            <ta e="T3" id="Seg_221" s="T2">nadə</ta>
            <ta e="T4" id="Seg_222" s="T3">pan-ol-gu</ta>
            <ta e="T5" id="Seg_223" s="T4">testa-m</ta>
            <ta e="T6" id="Seg_224" s="T5">toqtu-gu</ta>
            <ta e="T7" id="Seg_225" s="T6">nadə</ta>
            <ta e="T8" id="Seg_226" s="T7">man</ta>
            <ta e="T9" id="Seg_227" s="T8">testa-lʼ</ta>
            <ta e="T10" id="Seg_228" s="T9">qɨdan</ta>
            <ta e="T11" id="Seg_229" s="T10">toqtu-ku-ʒə-ku-sɨ-ŋ</ta>
            <ta e="T12" id="Seg_230" s="T11">a</ta>
            <ta e="T13" id="Seg_231" s="T12">teper</ta>
            <ta e="T14" id="Seg_232" s="T13">aulǯu-mbɨ</ta>
            <ta e="T15" id="Seg_233" s="T14">kak</ta>
            <ta e="T16" id="Seg_234" s="T15">omdɨ-lǯi-enǯɨ-l</ta>
            <ta e="T17" id="Seg_235" s="T16">pötte-ntə</ta>
            <ta e="T18" id="Seg_236" s="T17">nʼäj-la-m</ta>
            <ta e="T19" id="Seg_237" s="T18">täp-la</ta>
            <ta e="T20" id="Seg_238" s="T19">asa</ta>
            <ta e="T21" id="Seg_239" s="T20">müzɨ-enǯɨ-tɨn</ta>
            <ta e="T22" id="Seg_240" s="T21">okkɨr</ta>
            <ta e="T23" id="Seg_241" s="T22">qwašnʼä-m</ta>
            <ta e="T24" id="Seg_242" s="T23">portinɨ-enǯɨ-l</ta>
            <ta e="T25" id="Seg_243" s="T24">a</ta>
            <ta e="T26" id="Seg_244" s="T25">sədə-mǯʼeːli</ta>
            <ta e="T27" id="Seg_245" s="T26">qwašnʼä-m</ta>
            <ta e="T28" id="Seg_246" s="T27">soːdʼiga-ŋ</ta>
            <ta e="T29" id="Seg_247" s="T28">meː-enǯɨ-l</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_248" s="T1">egg-ACC</ta>
            <ta e="T3" id="Seg_249" s="T2">one.should</ta>
            <ta e="T4" id="Seg_250" s="T3">twist-MOM-INF</ta>
            <ta e="T5" id="Seg_251" s="T4">dough-ACC</ta>
            <ta e="T6" id="Seg_252" s="T5">knead-INF</ta>
            <ta e="T7" id="Seg_253" s="T6">one.should</ta>
            <ta e="T8" id="Seg_254" s="T7">I.NOM</ta>
            <ta e="T9" id="Seg_255" s="T8">dough-ADJZ</ta>
            <ta e="T10" id="Seg_256" s="T9">all.the.time</ta>
            <ta e="T11" id="Seg_257" s="T10">knead-HAB-DRV-HAB-PST-1SG.S</ta>
            <ta e="T12" id="Seg_258" s="T11">and</ta>
            <ta e="T13" id="Seg_259" s="T12">now</ta>
            <ta e="T14" id="Seg_260" s="T13">forget-PST.NAR.[3SG.S]</ta>
            <ta e="T15" id="Seg_261" s="T14">how</ta>
            <ta e="T16" id="Seg_262" s="T15">sit.down-TR-FUT-2SG.O</ta>
            <ta e="T17" id="Seg_263" s="T16">stove-ILL</ta>
            <ta e="T18" id="Seg_264" s="T17">bread-PL-ACC</ta>
            <ta e="T19" id="Seg_265" s="T18">(s)he-PL.[NOM]</ta>
            <ta e="T20" id="Seg_266" s="T19">NEG</ta>
            <ta e="T21" id="Seg_267" s="T20">be.done-FUT-3PL</ta>
            <ta e="T22" id="Seg_268" s="T21">one</ta>
            <ta e="T23" id="Seg_269" s="T22">dough-ACC</ta>
            <ta e="T24" id="Seg_270" s="T23">spoil-FUT-2SG.O</ta>
            <ta e="T25" id="Seg_271" s="T24">and</ta>
            <ta e="T26" id="Seg_272" s="T25">two-ORD</ta>
            <ta e="T27" id="Seg_273" s="T26">dough-ACC</ta>
            <ta e="T28" id="Seg_274" s="T27">good-ADVZ</ta>
            <ta e="T29" id="Seg_275" s="T28">do-FUT-2SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_276" s="T1">яйцо-ACC</ta>
            <ta e="T3" id="Seg_277" s="T2">надо</ta>
            <ta e="T4" id="Seg_278" s="T3">скрутить-MOM-INF</ta>
            <ta e="T5" id="Seg_279" s="T4">тесто-ACC</ta>
            <ta e="T6" id="Seg_280" s="T5">замесить-INF</ta>
            <ta e="T7" id="Seg_281" s="T6">надо</ta>
            <ta e="T8" id="Seg_282" s="T7">я.NOM</ta>
            <ta e="T9" id="Seg_283" s="T8">тесто-ADJZ</ta>
            <ta e="T10" id="Seg_284" s="T9">все.время</ta>
            <ta e="T11" id="Seg_285" s="T10">замесить-HAB-DRV-HAB-PST-1SG.S</ta>
            <ta e="T12" id="Seg_286" s="T11">а</ta>
            <ta e="T13" id="Seg_287" s="T12">теперь</ta>
            <ta e="T14" id="Seg_288" s="T13">забыть-PST.NAR.[3SG.S]</ta>
            <ta e="T15" id="Seg_289" s="T14">как</ta>
            <ta e="T16" id="Seg_290" s="T15">сесть-TR-FUT-2SG.O</ta>
            <ta e="T17" id="Seg_291" s="T16">печь-ILL</ta>
            <ta e="T18" id="Seg_292" s="T17">хлеб-PL-ACC</ta>
            <ta e="T19" id="Seg_293" s="T18">он(а)-PL.[NOM]</ta>
            <ta e="T20" id="Seg_294" s="T19">NEG</ta>
            <ta e="T21" id="Seg_295" s="T20">поспеть-FUT-3PL</ta>
            <ta e="T22" id="Seg_296" s="T21">один</ta>
            <ta e="T23" id="Seg_297" s="T22">квашня-ACC</ta>
            <ta e="T24" id="Seg_298" s="T23">испортить-FUT-2SG.O</ta>
            <ta e="T25" id="Seg_299" s="T24">а</ta>
            <ta e="T26" id="Seg_300" s="T25">два-ORD</ta>
            <ta e="T27" id="Seg_301" s="T26">квашня-ACC</ta>
            <ta e="T28" id="Seg_302" s="T27">хороший-ADVZ</ta>
            <ta e="T29" id="Seg_303" s="T28">сделать-FUT-2SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_304" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_305" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_306" s="T3">v-v&gt;v-v:inf</ta>
            <ta e="T5" id="Seg_307" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_308" s="T5">v-v:inf</ta>
            <ta e="T7" id="Seg_309" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_310" s="T7">pers</ta>
            <ta e="T9" id="Seg_311" s="T8">n-n&gt;adj</ta>
            <ta e="T10" id="Seg_312" s="T9">adv</ta>
            <ta e="T11" id="Seg_313" s="T10">v-v&gt;v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_314" s="T11">conj</ta>
            <ta e="T13" id="Seg_315" s="T12">adv</ta>
            <ta e="T14" id="Seg_316" s="T13">v-v:tense.[v:pn]</ta>
            <ta e="T15" id="Seg_317" s="T14">interrog</ta>
            <ta e="T16" id="Seg_318" s="T15">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_319" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_320" s="T17">n-n:num-n:case</ta>
            <ta e="T19" id="Seg_321" s="T18">pers-n:num.[n:case]</ta>
            <ta e="T20" id="Seg_322" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_323" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_324" s="T21">num</ta>
            <ta e="T23" id="Seg_325" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_326" s="T23">v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_327" s="T24">conj</ta>
            <ta e="T26" id="Seg_328" s="T25">num-num&gt;adj</ta>
            <ta e="T27" id="Seg_329" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_330" s="T27">adj-adj&gt;adv</ta>
            <ta e="T29" id="Seg_331" s="T28">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_332" s="T1">n</ta>
            <ta e="T3" id="Seg_333" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_334" s="T3">v</ta>
            <ta e="T5" id="Seg_335" s="T4">n</ta>
            <ta e="T6" id="Seg_336" s="T5">v</ta>
            <ta e="T7" id="Seg_337" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_338" s="T7">pers</ta>
            <ta e="T9" id="Seg_339" s="T8">adj</ta>
            <ta e="T10" id="Seg_340" s="T9">adv</ta>
            <ta e="T11" id="Seg_341" s="T10">v</ta>
            <ta e="T12" id="Seg_342" s="T11">conj</ta>
            <ta e="T13" id="Seg_343" s="T12">adv</ta>
            <ta e="T14" id="Seg_344" s="T13">v</ta>
            <ta e="T15" id="Seg_345" s="T14">interrog</ta>
            <ta e="T16" id="Seg_346" s="T15">v</ta>
            <ta e="T17" id="Seg_347" s="T16">n</ta>
            <ta e="T18" id="Seg_348" s="T17">n</ta>
            <ta e="T19" id="Seg_349" s="T18">pers</ta>
            <ta e="T20" id="Seg_350" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_351" s="T20">v</ta>
            <ta e="T22" id="Seg_352" s="T21">num</ta>
            <ta e="T23" id="Seg_353" s="T22">n</ta>
            <ta e="T24" id="Seg_354" s="T23">v</ta>
            <ta e="T25" id="Seg_355" s="T24">conj</ta>
            <ta e="T26" id="Seg_356" s="T25">adj</ta>
            <ta e="T27" id="Seg_357" s="T26">n</ta>
            <ta e="T28" id="Seg_358" s="T27">adv</ta>
            <ta e="T29" id="Seg_359" s="T28">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_360" s="T1">np:P</ta>
            <ta e="T4" id="Seg_361" s="T3">v:Th</ta>
            <ta e="T5" id="Seg_362" s="T4">np:P</ta>
            <ta e="T6" id="Seg_363" s="T5">v:Th</ta>
            <ta e="T8" id="Seg_364" s="T7">pro.h:A</ta>
            <ta e="T9" id="Seg_365" s="T8">np:P</ta>
            <ta e="T10" id="Seg_366" s="T9">adv:Time</ta>
            <ta e="T13" id="Seg_367" s="T12">adv:Time</ta>
            <ta e="T14" id="Seg_368" s="T13">0.1.h:E</ta>
            <ta e="T16" id="Seg_369" s="T15">0.2.h:A</ta>
            <ta e="T17" id="Seg_370" s="T16">np:G</ta>
            <ta e="T18" id="Seg_371" s="T17">np:Th</ta>
            <ta e="T19" id="Seg_372" s="T18">np:P</ta>
            <ta e="T23" id="Seg_373" s="T22">np:P</ta>
            <ta e="T24" id="Seg_374" s="T23">0.2.h:A</ta>
            <ta e="T27" id="Seg_375" s="T26">np:P</ta>
            <ta e="T29" id="Seg_376" s="T28">0.2.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_377" s="T2">ptcl:pred</ta>
            <ta e="T4" id="Seg_378" s="T3">v:O</ta>
            <ta e="T6" id="Seg_379" s="T5">v:O</ta>
            <ta e="T7" id="Seg_380" s="T6">ptcl:pred</ta>
            <ta e="T8" id="Seg_381" s="T7">pro.h:S</ta>
            <ta e="T9" id="Seg_382" s="T8">np:O</ta>
            <ta e="T11" id="Seg_383" s="T10">v:pred</ta>
            <ta e="T14" id="Seg_384" s="T13">0.1.h:S v:pred</ta>
            <ta e="T16" id="Seg_385" s="T15">0.2.h:S v:pred</ta>
            <ta e="T18" id="Seg_386" s="T17">np:O</ta>
            <ta e="T19" id="Seg_387" s="T18">np:S</ta>
            <ta e="T21" id="Seg_388" s="T20">v:pred</ta>
            <ta e="T23" id="Seg_389" s="T22">np:O</ta>
            <ta e="T24" id="Seg_390" s="T23">0.2.h:S v:pred</ta>
            <ta e="T27" id="Seg_391" s="T26">np:O</ta>
            <ta e="T29" id="Seg_392" s="T28">0.2.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_393" s="T2">RUS:mod</ta>
            <ta e="T5" id="Seg_394" s="T4">RUS:cult</ta>
            <ta e="T7" id="Seg_395" s="T6">RUS:mod</ta>
            <ta e="T9" id="Seg_396" s="T8">RUS:cult</ta>
            <ta e="T12" id="Seg_397" s="T11">RUS:gram</ta>
            <ta e="T13" id="Seg_398" s="T12">RUS:core</ta>
            <ta e="T15" id="Seg_399" s="T14">RUS:gram</ta>
            <ta e="T23" id="Seg_400" s="T22">RUS:cult</ta>
            <ta e="T24" id="Seg_401" s="T23">RUS:cult</ta>
            <ta e="T25" id="Seg_402" s="T24">RUS:gram</ta>
            <ta e="T27" id="Seg_403" s="T26">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T3" id="Seg_404" s="T2">Vsub</ta>
            <ta e="T7" id="Seg_405" s="T6">Vsub</ta>
            <ta e="T13" id="Seg_406" s="T12">Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T5" id="Seg_407" s="T4">dir:infl</ta>
            <ta e="T9" id="Seg_408" s="T8">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_409" s="T1">One should beat the eggs up.</ta>
            <ta e="T7" id="Seg_410" s="T4">One should knead dough.</ta>
            <ta e="T11" id="Seg_411" s="T7">[Earlier] I used to knead dough all the time.</ta>
            <ta e="T14" id="Seg_412" s="T11">And now I forgot it.</ta>
            <ta e="T21" id="Seg_413" s="T14">You put loaves of bread into the oven, and they don't get ready.</ta>
            <ta e="T24" id="Seg_414" s="T21">You spoil one dough.</ta>
            <ta e="T29" id="Seg_415" s="T24">And the second dough you make well.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_416" s="T1">Man sollte die Eier aufschlagen.</ta>
            <ta e="T7" id="Seg_417" s="T4">Man sollte den Teig kneten.</ta>
            <ta e="T11" id="Seg_418" s="T7">[Früher] habe ich für gewöhnlich immer den Teig geknetet.</ta>
            <ta e="T14" id="Seg_419" s="T11">Und jetzt habe ich es vergessen.</ta>
            <ta e="T21" id="Seg_420" s="T14">Du legst die Brote in den Ofen und sie werden nicht fertig.</ta>
            <ta e="T24" id="Seg_421" s="T21">Du verdirbst einen Teig.</ta>
            <ta e="T29" id="Seg_422" s="T24">Und den zweiten Teig machst du gut.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_423" s="T1">Яйцо надо взбить.</ta>
            <ta e="T7" id="Seg_424" s="T4">Тесто надо месить.</ta>
            <ta e="T11" id="Seg_425" s="T7">Я тесто всегда месила.</ta>
            <ta e="T14" id="Seg_426" s="T11">А теперь забыла.</ta>
            <ta e="T21" id="Seg_427" s="T14">Посадишь в печку хлебы, они и не поспеют.</ta>
            <ta e="T24" id="Seg_428" s="T21">Одну квашню испортишь.</ta>
            <ta e="T29" id="Seg_429" s="T24">А вторую квашню хорошо сделаешь.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_430" s="T1">яйцо надо взбить</ta>
            <ta e="T7" id="Seg_431" s="T4">тесто надо месить</ta>
            <ta e="T11" id="Seg_432" s="T7">я тесто всегда бы месила</ta>
            <ta e="T14" id="Seg_433" s="T11">а теперь забыла</ta>
            <ta e="T21" id="Seg_434" s="T14">посадишь в печку хлебы они и не поспеют</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T4" id="Seg_435" s="T1">[KuAI:] Variant: 'kɨgam'. [BrM:] Tentative analysis of 'panalgu'.</ta>
            <ta e="T11" id="Seg_436" s="T7">[BrM:] Tentative analysis of 'doqtuquʒəquzan'.</ta>
            <ta e="T14" id="Seg_437" s="T11">[BrM:] 3SG.S?</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T24" id="Seg_438" s="T21">There is no Russian translation of this sentence.</ta>
            <ta e="T29" id="Seg_439" s="T24">There is no Russian translation of this sentence.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
