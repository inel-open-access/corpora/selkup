<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Dreams_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Dreams_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">230</ud-information>
            <ud-information attribute-name="# HIAT:w">164</ud-information>
            <ud-information attribute-name="# e">164</ud-information>
            <ud-information attribute-name="# HIAT:u">41</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T165" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">tamdʼel</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">küːdeptɨbɨzan</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">son</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Staraj</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">jedom</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">qoǯirsau</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">Tʼai</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">Кonom</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">qoǯirsau</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_41" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">I</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">tʼötka</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">Paraskowja</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">qoǯirsau</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">küdeptɨbɨlʼe</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_59" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">Man</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">qoǯirsau</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">sɨrʼi</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">pajan</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_74" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">Qɨgɨzau</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">paːrgɨlɨgu</ts>
                  <nts id="Seg_80" n="HIAT:ip">,</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">a</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">tep</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">mʼekga</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">jas</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_95" n="HIAT:w" s="T26">mewaː</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_99" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">Man</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">na</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">qoǯirquddau</ts>
                  <nts id="Seg_108" n="HIAT:ip">,</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_111" n="HIAT:w" s="T30">qajamɨ</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_114" n="HIAT:w" s="T31">qum</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_117" n="HIAT:w" s="T32">qunɨš</ts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_121" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_123" n="HIAT:w" s="T33">Man</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_126" n="HIAT:w" s="T34">sɨdɨdʼän</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_129" n="HIAT:w" s="T35">i</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_132" n="HIAT:w" s="T36">tʼürlʼe</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_135" n="HIAT:w" s="T37">jübəran</ts>
                  <nts id="Seg_136" n="HIAT:ip">.</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_139" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_141" n="HIAT:w" s="T38">Man</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_144" n="HIAT:w" s="T39">tʼäran</ts>
                  <nts id="Seg_145" n="HIAT:ip">:</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_147" n="HIAT:ip">“</nts>
                  <ts e="T41" id="Seg_149" n="HIAT:w" s="T40">Qajamə</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_152" n="HIAT:w" s="T41">quǯin</ts>
                  <nts id="Seg_153" n="HIAT:ip">”</nts>
                  <nts id="Seg_154" n="HIAT:ip">,</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_157" n="HIAT:w" s="T42">i</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_160" n="HIAT:w" s="T43">sɨdɨdʼän</ts>
                  <nts id="Seg_161" n="HIAT:ip">.</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_164" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_166" n="HIAT:w" s="T44">Na</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_169" n="HIAT:w" s="T45">küdrʼim</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_172" n="HIAT:w" s="T46">wes</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_175" n="HIAT:w" s="T47">kennau</ts>
                  <nts id="Seg_176" n="HIAT:ip">.</nts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_179" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_181" n="HIAT:w" s="T48">Aj</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_184" n="HIAT:w" s="T49">qotdolban</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_186" n="HIAT:ip">(</nts>
                  <ts e="T51" id="Seg_188" n="HIAT:w" s="T50">qotdonnan</ts>
                  <nts id="Seg_189" n="HIAT:ip">)</nts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_193" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_195" n="HIAT:w" s="T51">Man</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_198" n="HIAT:w" s="T52">jeʒlʼi</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_201" n="HIAT:w" s="T53">qoǯirnau</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_204" n="HIAT:w" s="T54">staroj</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_207" n="HIAT:w" s="T55">jedom</ts>
                  <nts id="Seg_208" n="HIAT:ip">,</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_211" n="HIAT:w" s="T56">qajam</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_214" n="HIAT:w" s="T57">qum</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_217" n="HIAT:w" s="T58">kunɨš</ts>
                  <nts id="Seg_218" n="HIAT:ip">.</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_221" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_223" n="HIAT:w" s="T59">Aj</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_226" n="HIAT:w" s="T60">küdeptan</ts>
                  <nts id="Seg_227" n="HIAT:ip">,</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_230" n="HIAT:w" s="T61">matqən</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_233" n="HIAT:w" s="T62">onʼen</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_236" n="HIAT:w" s="T63">amdan</ts>
                  <nts id="Seg_237" n="HIAT:ip">.</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_240" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_242" n="HIAT:w" s="T64">Qajda</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_245" n="HIAT:w" s="T65">stukajčelʼe</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_248" n="HIAT:w" s="T66">übəran</ts>
                  <nts id="Seg_249" n="HIAT:ip">.</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_252" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_254" n="HIAT:w" s="T67">Man</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_257" n="HIAT:w" s="T68">koronnan</ts>
                  <nts id="Seg_258" n="HIAT:ip">.</nts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_261" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_263" n="HIAT:w" s="T69">Kudɨ</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_266" n="HIAT:w" s="T70">natʼen</ts>
                  <nts id="Seg_267" n="HIAT:ip">?</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_270" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_272" n="HIAT:w" s="T71">A</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_275" n="HIAT:w" s="T72">tep</ts>
                  <nts id="Seg_276" n="HIAT:ip">:</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_278" n="HIAT:ip">“</nts>
                  <ts e="T74" id="Seg_280" n="HIAT:w" s="T73">Maɣn</ts>
                  <nts id="Seg_281" n="HIAT:ip">,</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_284" n="HIAT:w" s="T74">maɣn</ts>
                  <nts id="Seg_285" n="HIAT:ip">,</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_288" n="HIAT:w" s="T75">maɣn</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_291" n="HIAT:w" s="T76">ɛto</ts>
                  <nts id="Seg_292" n="HIAT:ip">.</nts>
                  <nts id="Seg_293" n="HIAT:ip">”</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_296" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_298" n="HIAT:w" s="T77">Man</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_301" n="HIAT:w" s="T78">asʼ</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_304" n="HIAT:w" s="T79">üːdəku</ts>
                  <nts id="Seg_305" n="HIAT:ip">.</nts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_308" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_310" n="HIAT:w" s="T80">A</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_313" n="HIAT:w" s="T81">tep</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_316" n="HIAT:w" s="T82">innä</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_319" n="HIAT:w" s="T83">sɨɣənnɨn</ts>
                  <nts id="Seg_320" n="HIAT:ip">.</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_323" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_325" n="HIAT:w" s="T84">Madan</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_328" n="HIAT:w" s="T85">oːlloɣɨn</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_331" n="HIAT:w" s="T86">akoškaɣa</ts>
                  <nts id="Seg_332" n="HIAT:ip">.</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_335" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_337" n="HIAT:w" s="T87">Tep</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_340" n="HIAT:w" s="T88">na</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_343" n="HIAT:w" s="T89">akoškaon</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_346" n="HIAT:w" s="T90">nʼaːpbodin</ts>
                  <nts id="Seg_347" n="HIAT:ip">.</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_350" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_352" n="HIAT:w" s="T91">Man</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_355" n="HIAT:w" s="T92">paroɣɨn</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_358" n="HIAT:w" s="T93">pɨkgəlan</ts>
                  <nts id="Seg_359" n="HIAT:ip">.</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_362" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_364" n="HIAT:w" s="T94">Man</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_367" n="HIAT:w" s="T95">datawa</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_370" n="HIAT:w" s="T96">kətʼewaːnnan</ts>
                  <nts id="Seg_371" n="HIAT:ip">.</nts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_374" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_376" n="HIAT:w" s="T97">Man</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_379" n="HIAT:w" s="T98">maǯedidan</ts>
                  <nts id="Seg_380" n="HIAT:ip">,</nts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_383" n="HIAT:w" s="T99">a</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_386" n="HIAT:w" s="T100">Вalotʼka</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_389" n="HIAT:w" s="T101">ippaː</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_392" n="HIAT:w" s="T102">i</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_395" n="HIAT:w" s="T103">lagwaːtpa</ts>
                  <nts id="Seg_396" n="HIAT:ip">.</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_399" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_401" n="HIAT:w" s="T104">Man</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_404" n="HIAT:w" s="T105">tʼäran</ts>
                  <nts id="Seg_405" n="HIAT:ip">:</nts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_407" n="HIAT:ip">“</nts>
                  <ts e="T107" id="Seg_409" n="HIAT:w" s="T106">Qajno</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_412" n="HIAT:w" s="T107">zɨm</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_415" n="HIAT:w" s="T108">qɨtʼölǯat</ts>
                  <nts id="Seg_416" n="HIAT:ip">?</nts>
                  <nts id="Seg_417" n="HIAT:ip">”</nts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_420" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_422" n="HIAT:w" s="T109">I</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_425" n="HIAT:w" s="T110">sɨdedʼan</ts>
                  <nts id="Seg_426" n="HIAT:ip">.</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_429" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_431" n="HIAT:w" s="T111">Man</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_434" n="HIAT:w" s="T112">tamdʼel</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_437" n="HIAT:w" s="T113">Filatam</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_440" n="HIAT:w" s="T114">qoǯirsan</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_443" n="HIAT:w" s="T115">küdeptəbəlʼe</ts>
                  <nts id="Seg_444" n="HIAT:ip">.</nts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_447" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_449" n="HIAT:w" s="T116">Neldʼä</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_452" n="HIAT:w" s="T117">toːbɨn</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_455" n="HIAT:w" s="T118">amdɨs</ts>
                  <nts id="Seg_456" n="HIAT:ip">.</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_459" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_461" n="HIAT:w" s="T119">Mekka</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_464" n="HIAT:w" s="T120">tʼarɨn</ts>
                  <nts id="Seg_465" n="HIAT:ip">:</nts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_467" n="HIAT:ip">“</nts>
                  <ts e="T122" id="Seg_469" n="HIAT:w" s="T121">Man</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_472" n="HIAT:w" s="T122">toblau</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_475" n="HIAT:w" s="T123">qannɨbaːt</ts>
                  <nts id="Seg_476" n="HIAT:ip">.</nts>
                  <nts id="Seg_477" n="HIAT:ip">”</nts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_480" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_482" n="HIAT:w" s="T124">Man</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_485" n="HIAT:w" s="T125">tebɨn</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_488" n="HIAT:w" s="T126">tobɨm</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_491" n="HIAT:w" s="T127">orannau</ts>
                  <nts id="Seg_492" n="HIAT:ip">.</nts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_495" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_497" n="HIAT:w" s="T128">Tebnan</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_500" n="HIAT:w" s="T129">qannɨbaːt</ts>
                  <nts id="Seg_501" n="HIAT:ip">.</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_504" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_506" n="HIAT:w" s="T130">Imattə</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_509" n="HIAT:w" s="T131">tʼäkku</ts>
                  <nts id="Seg_510" n="HIAT:ip">,</nts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_513" n="HIAT:w" s="T132">qudnä</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_516" n="HIAT:w" s="T133">nušno</ts>
                  <nts id="Seg_517" n="HIAT:ip">.</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_520" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_522" n="HIAT:w" s="T134">Tebnan</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_525" n="HIAT:w" s="T135">agat</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_528" n="HIAT:w" s="T136">tɨmnʼat</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_531" n="HIAT:w" s="T137">jetta</ts>
                  <nts id="Seg_532" n="HIAT:ip">.</nts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_535" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_537" n="HIAT:w" s="T138">Karmonʼäčukus</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_540" n="HIAT:w" s="T139">kɨːdan</ts>
                  <nts id="Seg_541" n="HIAT:ip">.</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_544" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_546" n="HIAT:w" s="T140">Tamdʼel</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_549" n="HIAT:w" s="T141">man</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_552" n="HIAT:w" s="T142">küdeptɨbɨlʼe</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_555" n="HIAT:w" s="T143">tödɨn</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_558" n="HIAT:w" s="T144">qoǯirsau</ts>
                  <nts id="Seg_559" n="HIAT:ip">.</nts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_562" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_564" n="HIAT:w" s="T145">Koška</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_567" n="HIAT:w" s="T146">tötpa</ts>
                  <nts id="Seg_568" n="HIAT:ip">,</nts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_571" n="HIAT:w" s="T147">müzelǯɨ</ts>
                  <nts id="Seg_572" n="HIAT:ip">,</nts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_575" n="HIAT:w" s="T148">tan</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_578" n="HIAT:w" s="T149">koškal</ts>
                  <nts id="Seg_579" n="HIAT:ip">.</nts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_582" n="HIAT:u" s="T150">
                  <ts e="T151" id="Seg_584" n="HIAT:w" s="T150">Tamdʼel</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_587" n="HIAT:w" s="T151">man</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_590" n="HIAT:w" s="T152">küːdrʼem</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_593" n="HIAT:w" s="T153">qoǯərsan</ts>
                  <nts id="Seg_594" n="HIAT:ip">.</nts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_597" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_599" n="HIAT:w" s="T154">Qəːnnolottə</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_602" n="HIAT:w" s="T155">tʼötʼöuan</ts>
                  <nts id="Seg_603" n="HIAT:ip">.</nts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T159" id="Seg_606" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_608" n="HIAT:w" s="T156">Meka</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_611" n="HIAT:w" s="T157">soːn</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_614" n="HIAT:w" s="T158">jekon</ts>
                  <nts id="Seg_615" n="HIAT:ip">.</nts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_618" n="HIAT:u" s="T159">
                  <ts e="T160" id="Seg_620" n="HIAT:w" s="T159">Teper</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_623" n="HIAT:w" s="T160">adɨlčan</ts>
                  <nts id="Seg_624" n="HIAT:ip">.</nts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_627" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_629" n="HIAT:w" s="T161">Qaj</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_632" n="HIAT:w" s="T162">jenɨš</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_635" n="HIAT:w" s="T163">as</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_638" n="HIAT:w" s="T164">tunou</ts>
                  <nts id="Seg_639" n="HIAT:ip">.</nts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T165" id="Seg_641" n="sc" s="T1">
               <ts e="T2" id="Seg_643" n="e" s="T1">Man </ts>
               <ts e="T3" id="Seg_645" n="e" s="T2">tamdʼel </ts>
               <ts e="T4" id="Seg_647" n="e" s="T3">küːdeptɨbɨzan </ts>
               <ts e="T5" id="Seg_649" n="e" s="T4">son. </ts>
               <ts e="T6" id="Seg_651" n="e" s="T5">Staraj </ts>
               <ts e="T7" id="Seg_653" n="e" s="T6">jedom </ts>
               <ts e="T8" id="Seg_655" n="e" s="T7">qoǯirsau. </ts>
               <ts e="T9" id="Seg_657" n="e" s="T8">Tʼai </ts>
               <ts e="T10" id="Seg_659" n="e" s="T9">Кonom </ts>
               <ts e="T11" id="Seg_661" n="e" s="T10">qoǯirsau. </ts>
               <ts e="T12" id="Seg_663" n="e" s="T11">I </ts>
               <ts e="T13" id="Seg_665" n="e" s="T12">tʼötka </ts>
               <ts e="T14" id="Seg_667" n="e" s="T13">Paraskowja </ts>
               <ts e="T15" id="Seg_669" n="e" s="T14">qoǯirsau </ts>
               <ts e="T16" id="Seg_671" n="e" s="T15">küdeptɨbɨlʼe. </ts>
               <ts e="T17" id="Seg_673" n="e" s="T16">Man </ts>
               <ts e="T18" id="Seg_675" n="e" s="T17">qoǯirsau </ts>
               <ts e="T19" id="Seg_677" n="e" s="T18">sɨrʼi </ts>
               <ts e="T20" id="Seg_679" n="e" s="T19">pajan. </ts>
               <ts e="T21" id="Seg_681" n="e" s="T20">Qɨgɨzau </ts>
               <ts e="T22" id="Seg_683" n="e" s="T21">paːrgɨlɨgu, </ts>
               <ts e="T23" id="Seg_685" n="e" s="T22">a </ts>
               <ts e="T24" id="Seg_687" n="e" s="T23">tep </ts>
               <ts e="T25" id="Seg_689" n="e" s="T24">mʼekga </ts>
               <ts e="T26" id="Seg_691" n="e" s="T25">jas </ts>
               <ts e="T27" id="Seg_693" n="e" s="T26">mewaː. </ts>
               <ts e="T28" id="Seg_695" n="e" s="T27">Man </ts>
               <ts e="T29" id="Seg_697" n="e" s="T28">na </ts>
               <ts e="T30" id="Seg_699" n="e" s="T29">qoǯirquddau, </ts>
               <ts e="T31" id="Seg_701" n="e" s="T30">qajamɨ </ts>
               <ts e="T32" id="Seg_703" n="e" s="T31">qum </ts>
               <ts e="T33" id="Seg_705" n="e" s="T32">qunɨš. </ts>
               <ts e="T34" id="Seg_707" n="e" s="T33">Man </ts>
               <ts e="T35" id="Seg_709" n="e" s="T34">sɨdɨdʼän </ts>
               <ts e="T36" id="Seg_711" n="e" s="T35">i </ts>
               <ts e="T37" id="Seg_713" n="e" s="T36">tʼürlʼe </ts>
               <ts e="T38" id="Seg_715" n="e" s="T37">jübəran. </ts>
               <ts e="T39" id="Seg_717" n="e" s="T38">Man </ts>
               <ts e="T40" id="Seg_719" n="e" s="T39">tʼäran: </ts>
               <ts e="T41" id="Seg_721" n="e" s="T40">“Qajamə </ts>
               <ts e="T42" id="Seg_723" n="e" s="T41">quǯin”, </ts>
               <ts e="T43" id="Seg_725" n="e" s="T42">i </ts>
               <ts e="T44" id="Seg_727" n="e" s="T43">sɨdɨdʼän. </ts>
               <ts e="T45" id="Seg_729" n="e" s="T44">Na </ts>
               <ts e="T46" id="Seg_731" n="e" s="T45">küdrʼim </ts>
               <ts e="T47" id="Seg_733" n="e" s="T46">wes </ts>
               <ts e="T48" id="Seg_735" n="e" s="T47">kennau. </ts>
               <ts e="T49" id="Seg_737" n="e" s="T48">Aj </ts>
               <ts e="T50" id="Seg_739" n="e" s="T49">qotdolban </ts>
               <ts e="T51" id="Seg_741" n="e" s="T50">(qotdonnan). </ts>
               <ts e="T52" id="Seg_743" n="e" s="T51">Man </ts>
               <ts e="T53" id="Seg_745" n="e" s="T52">jeʒlʼi </ts>
               <ts e="T54" id="Seg_747" n="e" s="T53">qoǯirnau </ts>
               <ts e="T55" id="Seg_749" n="e" s="T54">staroj </ts>
               <ts e="T56" id="Seg_751" n="e" s="T55">jedom, </ts>
               <ts e="T57" id="Seg_753" n="e" s="T56">qajam </ts>
               <ts e="T58" id="Seg_755" n="e" s="T57">qum </ts>
               <ts e="T59" id="Seg_757" n="e" s="T58">kunɨš. </ts>
               <ts e="T60" id="Seg_759" n="e" s="T59">Aj </ts>
               <ts e="T61" id="Seg_761" n="e" s="T60">küdeptan, </ts>
               <ts e="T62" id="Seg_763" n="e" s="T61">matqən </ts>
               <ts e="T63" id="Seg_765" n="e" s="T62">onʼen </ts>
               <ts e="T64" id="Seg_767" n="e" s="T63">amdan. </ts>
               <ts e="T65" id="Seg_769" n="e" s="T64">Qajda </ts>
               <ts e="T66" id="Seg_771" n="e" s="T65">stukajčelʼe </ts>
               <ts e="T67" id="Seg_773" n="e" s="T66">übəran. </ts>
               <ts e="T68" id="Seg_775" n="e" s="T67">Man </ts>
               <ts e="T69" id="Seg_777" n="e" s="T68">koronnan. </ts>
               <ts e="T70" id="Seg_779" n="e" s="T69">Kudɨ </ts>
               <ts e="T71" id="Seg_781" n="e" s="T70">natʼen? </ts>
               <ts e="T72" id="Seg_783" n="e" s="T71">A </ts>
               <ts e="T73" id="Seg_785" n="e" s="T72">tep: </ts>
               <ts e="T74" id="Seg_787" n="e" s="T73">“Maɣn, </ts>
               <ts e="T75" id="Seg_789" n="e" s="T74">maɣn, </ts>
               <ts e="T76" id="Seg_791" n="e" s="T75">maɣn </ts>
               <ts e="T77" id="Seg_793" n="e" s="T76">ɛto.” </ts>
               <ts e="T78" id="Seg_795" n="e" s="T77">Man </ts>
               <ts e="T79" id="Seg_797" n="e" s="T78">asʼ </ts>
               <ts e="T80" id="Seg_799" n="e" s="T79">üːdəku. </ts>
               <ts e="T81" id="Seg_801" n="e" s="T80">A </ts>
               <ts e="T82" id="Seg_803" n="e" s="T81">tep </ts>
               <ts e="T83" id="Seg_805" n="e" s="T82">innä </ts>
               <ts e="T84" id="Seg_807" n="e" s="T83">sɨɣənnɨn. </ts>
               <ts e="T85" id="Seg_809" n="e" s="T84">Madan </ts>
               <ts e="T86" id="Seg_811" n="e" s="T85">oːlloɣɨn </ts>
               <ts e="T87" id="Seg_813" n="e" s="T86">akoškaɣa. </ts>
               <ts e="T88" id="Seg_815" n="e" s="T87">Tep </ts>
               <ts e="T89" id="Seg_817" n="e" s="T88">na </ts>
               <ts e="T90" id="Seg_819" n="e" s="T89">akoškaon </ts>
               <ts e="T91" id="Seg_821" n="e" s="T90">nʼaːpbodin. </ts>
               <ts e="T92" id="Seg_823" n="e" s="T91">Man </ts>
               <ts e="T93" id="Seg_825" n="e" s="T92">paroɣɨn </ts>
               <ts e="T94" id="Seg_827" n="e" s="T93">pɨkgəlan. </ts>
               <ts e="T95" id="Seg_829" n="e" s="T94">Man </ts>
               <ts e="T96" id="Seg_831" n="e" s="T95">datawa </ts>
               <ts e="T97" id="Seg_833" n="e" s="T96">kətʼewaːnnan. </ts>
               <ts e="T98" id="Seg_835" n="e" s="T97">Man </ts>
               <ts e="T99" id="Seg_837" n="e" s="T98">maǯedidan, </ts>
               <ts e="T100" id="Seg_839" n="e" s="T99">a </ts>
               <ts e="T101" id="Seg_841" n="e" s="T100">Вalotʼka </ts>
               <ts e="T102" id="Seg_843" n="e" s="T101">ippaː </ts>
               <ts e="T103" id="Seg_845" n="e" s="T102">i </ts>
               <ts e="T104" id="Seg_847" n="e" s="T103">lagwaːtpa. </ts>
               <ts e="T105" id="Seg_849" n="e" s="T104">Man </ts>
               <ts e="T106" id="Seg_851" n="e" s="T105">tʼäran: </ts>
               <ts e="T107" id="Seg_853" n="e" s="T106">“Qajno </ts>
               <ts e="T108" id="Seg_855" n="e" s="T107">zɨm </ts>
               <ts e="T109" id="Seg_857" n="e" s="T108">qɨtʼölǯat?” </ts>
               <ts e="T110" id="Seg_859" n="e" s="T109">I </ts>
               <ts e="T111" id="Seg_861" n="e" s="T110">sɨdedʼan. </ts>
               <ts e="T112" id="Seg_863" n="e" s="T111">Man </ts>
               <ts e="T113" id="Seg_865" n="e" s="T112">tamdʼel </ts>
               <ts e="T114" id="Seg_867" n="e" s="T113">Filatam </ts>
               <ts e="T115" id="Seg_869" n="e" s="T114">qoǯirsan </ts>
               <ts e="T116" id="Seg_871" n="e" s="T115">küdeptəbəlʼe. </ts>
               <ts e="T117" id="Seg_873" n="e" s="T116">Neldʼä </ts>
               <ts e="T118" id="Seg_875" n="e" s="T117">toːbɨn </ts>
               <ts e="T119" id="Seg_877" n="e" s="T118">amdɨs. </ts>
               <ts e="T120" id="Seg_879" n="e" s="T119">Mekka </ts>
               <ts e="T121" id="Seg_881" n="e" s="T120">tʼarɨn: </ts>
               <ts e="T122" id="Seg_883" n="e" s="T121">“Man </ts>
               <ts e="T123" id="Seg_885" n="e" s="T122">toblau </ts>
               <ts e="T124" id="Seg_887" n="e" s="T123">qannɨbaːt.” </ts>
               <ts e="T125" id="Seg_889" n="e" s="T124">Man </ts>
               <ts e="T126" id="Seg_891" n="e" s="T125">tebɨn </ts>
               <ts e="T127" id="Seg_893" n="e" s="T126">tobɨm </ts>
               <ts e="T128" id="Seg_895" n="e" s="T127">orannau. </ts>
               <ts e="T129" id="Seg_897" n="e" s="T128">Tebnan </ts>
               <ts e="T130" id="Seg_899" n="e" s="T129">qannɨbaːt. </ts>
               <ts e="T131" id="Seg_901" n="e" s="T130">Imattə </ts>
               <ts e="T132" id="Seg_903" n="e" s="T131">tʼäkku, </ts>
               <ts e="T133" id="Seg_905" n="e" s="T132">qudnä </ts>
               <ts e="T134" id="Seg_907" n="e" s="T133">nušno. </ts>
               <ts e="T135" id="Seg_909" n="e" s="T134">Tebnan </ts>
               <ts e="T136" id="Seg_911" n="e" s="T135">agat </ts>
               <ts e="T137" id="Seg_913" n="e" s="T136">tɨmnʼat </ts>
               <ts e="T138" id="Seg_915" n="e" s="T137">jetta. </ts>
               <ts e="T139" id="Seg_917" n="e" s="T138">Karmonʼäčukus </ts>
               <ts e="T140" id="Seg_919" n="e" s="T139">kɨːdan. </ts>
               <ts e="T141" id="Seg_921" n="e" s="T140">Tamdʼel </ts>
               <ts e="T142" id="Seg_923" n="e" s="T141">man </ts>
               <ts e="T143" id="Seg_925" n="e" s="T142">küdeptɨbɨlʼe </ts>
               <ts e="T144" id="Seg_927" n="e" s="T143">tödɨn </ts>
               <ts e="T145" id="Seg_929" n="e" s="T144">qoǯirsau. </ts>
               <ts e="T146" id="Seg_931" n="e" s="T145">Koška </ts>
               <ts e="T147" id="Seg_933" n="e" s="T146">tötpa, </ts>
               <ts e="T148" id="Seg_935" n="e" s="T147">müzelǯɨ, </ts>
               <ts e="T149" id="Seg_937" n="e" s="T148">tan </ts>
               <ts e="T150" id="Seg_939" n="e" s="T149">koškal. </ts>
               <ts e="T151" id="Seg_941" n="e" s="T150">Tamdʼel </ts>
               <ts e="T152" id="Seg_943" n="e" s="T151">man </ts>
               <ts e="T153" id="Seg_945" n="e" s="T152">küːdrʼem </ts>
               <ts e="T154" id="Seg_947" n="e" s="T153">qoǯərsan. </ts>
               <ts e="T155" id="Seg_949" n="e" s="T154">Qəːnnolottə </ts>
               <ts e="T156" id="Seg_951" n="e" s="T155">tʼötʼöuan. </ts>
               <ts e="T157" id="Seg_953" n="e" s="T156">Meka </ts>
               <ts e="T158" id="Seg_955" n="e" s="T157">soːn </ts>
               <ts e="T159" id="Seg_957" n="e" s="T158">jekon. </ts>
               <ts e="T160" id="Seg_959" n="e" s="T159">Teper </ts>
               <ts e="T161" id="Seg_961" n="e" s="T160">adɨlčan. </ts>
               <ts e="T162" id="Seg_963" n="e" s="T161">Qaj </ts>
               <ts e="T163" id="Seg_965" n="e" s="T162">jenɨš </ts>
               <ts e="T164" id="Seg_967" n="e" s="T163">as </ts>
               <ts e="T165" id="Seg_969" n="e" s="T164">tunou. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_970" s="T1">PVD_1964_Dreams_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_971" s="T5">PVD_1964_Dreams_nar.002 (001.002)</ta>
            <ta e="T11" id="Seg_972" s="T8">PVD_1964_Dreams_nar.003 (001.003)</ta>
            <ta e="T16" id="Seg_973" s="T11">PVD_1964_Dreams_nar.004 (001.004)</ta>
            <ta e="T20" id="Seg_974" s="T16">PVD_1964_Dreams_nar.005 (001.005)</ta>
            <ta e="T27" id="Seg_975" s="T20">PVD_1964_Dreams_nar.006 (001.006)</ta>
            <ta e="T33" id="Seg_976" s="T27">PVD_1964_Dreams_nar.007 (001.007)</ta>
            <ta e="T38" id="Seg_977" s="T33">PVD_1964_Dreams_nar.008 (001.008)</ta>
            <ta e="T44" id="Seg_978" s="T38">PVD_1964_Dreams_nar.009 (001.009)</ta>
            <ta e="T48" id="Seg_979" s="T44">PVD_1964_Dreams_nar.010 (001.010)</ta>
            <ta e="T51" id="Seg_980" s="T48">PVD_1964_Dreams_nar.011 (001.011)</ta>
            <ta e="T59" id="Seg_981" s="T51">PVD_1964_Dreams_nar.012 (001.012)</ta>
            <ta e="T64" id="Seg_982" s="T59">PVD_1964_Dreams_nar.013 (001.013)</ta>
            <ta e="T67" id="Seg_983" s="T64">PVD_1964_Dreams_nar.014 (001.014)</ta>
            <ta e="T69" id="Seg_984" s="T67">PVD_1964_Dreams_nar.015 (001.015)</ta>
            <ta e="T71" id="Seg_985" s="T69">PVD_1964_Dreams_nar.016 (001.016)</ta>
            <ta e="T77" id="Seg_986" s="T71">PVD_1964_Dreams_nar.017 (001.017)</ta>
            <ta e="T80" id="Seg_987" s="T77">PVD_1964_Dreams_nar.018 (001.018)</ta>
            <ta e="T84" id="Seg_988" s="T80">PVD_1964_Dreams_nar.019 (001.019)</ta>
            <ta e="T87" id="Seg_989" s="T84">PVD_1964_Dreams_nar.020 (001.020)</ta>
            <ta e="T91" id="Seg_990" s="T87">PVD_1964_Dreams_nar.021 (001.021)</ta>
            <ta e="T94" id="Seg_991" s="T91">PVD_1964_Dreams_nar.022 (001.022)</ta>
            <ta e="T97" id="Seg_992" s="T94">PVD_1964_Dreams_nar.023 (001.023)</ta>
            <ta e="T104" id="Seg_993" s="T97">PVD_1964_Dreams_nar.024 (001.024)</ta>
            <ta e="T109" id="Seg_994" s="T104">PVD_1964_Dreams_nar.025 (001.025)</ta>
            <ta e="T111" id="Seg_995" s="T109">PVD_1964_Dreams_nar.026 (001.026)</ta>
            <ta e="T116" id="Seg_996" s="T111">PVD_1964_Dreams_nar.027 (001.027)</ta>
            <ta e="T119" id="Seg_997" s="T116">PVD_1964_Dreams_nar.028 (001.028)</ta>
            <ta e="T124" id="Seg_998" s="T119">PVD_1964_Dreams_nar.029 (001.029)</ta>
            <ta e="T128" id="Seg_999" s="T124">PVD_1964_Dreams_nar.030 (001.030)</ta>
            <ta e="T130" id="Seg_1000" s="T128">PVD_1964_Dreams_nar.031 (001.031)</ta>
            <ta e="T134" id="Seg_1001" s="T130">PVD_1964_Dreams_nar.032 (001.032)</ta>
            <ta e="T138" id="Seg_1002" s="T134">PVD_1964_Dreams_nar.033 (001.033)</ta>
            <ta e="T140" id="Seg_1003" s="T138">PVD_1964_Dreams_nar.034 (001.034)</ta>
            <ta e="T145" id="Seg_1004" s="T140">PVD_1964_Dreams_nar.035 (001.035)</ta>
            <ta e="T150" id="Seg_1005" s="T145">PVD_1964_Dreams_nar.036 (001.036)</ta>
            <ta e="T154" id="Seg_1006" s="T150">PVD_1964_Dreams_nar.037 (001.037)</ta>
            <ta e="T156" id="Seg_1007" s="T154">PVD_1964_Dreams_nar.038 (001.038)</ta>
            <ta e="T159" id="Seg_1008" s="T156">PVD_1964_Dreams_nar.039 (001.039)</ta>
            <ta e="T161" id="Seg_1009" s="T159">PVD_1964_Dreams_nar.040 (001.040)</ta>
            <ta e="T165" id="Seg_1010" s="T161">PVD_1964_Dreams_nar.041 (001.041)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_1011" s="T1">ман там′дʼел кӱ̄′де̨птыб̂ызан сон.</ta>
            <ta e="T8" id="Seg_1012" s="T5">старай ′jедом ′kоджир′сау̹.</ta>
            <ta e="T11" id="Seg_1013" s="T8">′тʼаи ′Коном kоджир̹′сау̹.</ta>
            <ta e="T16" id="Seg_1014" s="T11">и тʼӧтка Парасковjа kоджир̹′сау̹ кӱ′де̨птыбылʼе.</ta>
            <ta e="T20" id="Seg_1015" s="T16">ман kоджир′сау̹ ′сырʼи па′jан.</ta>
            <ta e="T27" id="Seg_1016" s="T20">kыгы′зау ′па̄ргылыг(ɣ)у, а те̨п мʼекг̂а jас ме′ва̄.</ta>
            <ta e="T33" id="Seg_1017" s="T27">ман ′на ′kоджирkуд̂′дау̹, kа′jамы kум ′kуныш.</ta>
            <ta e="T38" id="Seg_1018" s="T33">ман сыды′дʼӓн и тʼӱрлʼе ′jӱбъран.</ta>
            <ta e="T44" id="Seg_1019" s="T38">ман тʼӓ′ран kа′jамъ ′kуджин и ′сыды‵дʼӓн.</ta>
            <ta e="T48" id="Seg_1020" s="T44">на кӱдрʼим вес ′кеннау̹.</ta>
            <ta e="T51" id="Seg_1021" s="T48">ай ′kот′долбан (kот′доннан).</ta>
            <ta e="T59" id="Seg_1022" s="T51">ман jежлʼи kоджир′нау ′старой ′jедом, kа′jам kум ′куныш.</ta>
            <ta e="T64" id="Seg_1023" s="T59">ай кӱ′де̨птан, ′матkън о′нʼен ′амдан.</ta>
            <ta e="T67" id="Seg_1024" s="T64">kай′да ′стукайтше′лʼе ′ӱбъран.</ta>
            <ta e="T69" id="Seg_1025" s="T67">ман ко′роннан.</ta>
            <ta e="T71" id="Seg_1026" s="T69">ку′ды на′тʼен?</ta>
            <ta e="T77" id="Seg_1027" s="T71">а те̨п: маɣн, маɣн, маɣн это.</ta>
            <ta e="T80" id="Seg_1028" s="T77">ман асʼ ′ӱ̄дъку.</ta>
            <ta e="T84" id="Seg_1029" s="T80">а теп ин′нӓ ′сыɣъннын.</ta>
            <ta e="T87" id="Seg_1030" s="T84">′мадан о̄′llоɣын а′кошкаɣа.</ta>
            <ta e="T91" id="Seg_1031" s="T87">те̨п на а′кошкаон нʼа̄п′б̂од̂ин.</ta>
            <ta e="T94" id="Seg_1032" s="T91">ман ′пароɣын ′пыкг̂ълан.</ta>
            <ta e="T97" id="Seg_1033" s="T94">ман д̂а ′тава ′къ(ы)тʼ(цʼ)е′ва̄ннан.</ta>
            <ta e="T104" id="Seg_1034" s="T97">ман ма′джед̂ид̂ан, а Ваlотʼка ип′па̄ и лаг′ва̄тпа.</ta>
            <ta e="T109" id="Seg_1035" s="T104">ман тʼӓ′ран: kай′нозым kы′тʼӧlджат?</ta>
            <ta e="T111" id="Seg_1036" s="T109">и ′сыде‵дʼан.</ta>
            <ta e="T116" id="Seg_1037" s="T111">ман тамдʼел Фи′латам ′kоджирсан кӱ′де̨птъбълʼе.</ta>
            <ta e="T119" id="Seg_1038" s="T116">′неlдʼӓ ′то̄бын ′амдыс.</ta>
            <ta e="T124" id="Seg_1039" s="T119">′мекка тʼа′рын: ман тоб′лау ′kанны‵ба̄т.</ta>
            <ta e="T128" id="Seg_1040" s="T124">ман ′тебын то′бым о′раннау̹.</ta>
            <ta e="T130" id="Seg_1041" s="T128">теб′нан kанны′ба̄т.</ta>
            <ta e="T134" id="Seg_1042" s="T130">и′маттъ ′тʼӓкку, kуд′нӓ ′нушно.</ta>
            <ta e="T138" id="Seg_1043" s="T134">теб′нан а′гат тым′нʼат ′jетта.</ta>
            <ta e="T140" id="Seg_1044" s="T138">кар′монʼӓтшукус кы̄дан.</ta>
            <ta e="T145" id="Seg_1045" s="T140">тамдʼел ман кӱ′де̨птыбылʼе ′тӧдын ′kоджирсау.</ta>
            <ta e="T150" id="Seg_1046" s="T145">кошка тӧтпа, мӱ′зеlджы, тан ′кошкал.</ta>
            <ta e="T154" id="Seg_1047" s="T150">там′дʼел ман ′кӱ̄дрʼе̨м ′kоджърсан.</ta>
            <ta e="T156" id="Seg_1048" s="T154">kъ̄(ӓ)нноlоттъ тʼӧ′тʼӧу̹ан.</ta>
            <ta e="T159" id="Seg_1049" s="T156">′мек(г̂)а ′со̄н ′jе‵кон.</ta>
            <ta e="T161" id="Seg_1050" s="T159">те̨пе̨р ′адыlтшан.</ta>
            <ta e="T165" id="Seg_1051" s="T161">′kай ′jеныш ′ас ту′ноу̹.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_1052" s="T1">man tamdʼel küːdeptɨb̂ɨzan son.</ta>
            <ta e="T8" id="Seg_1053" s="T5">staraj jedom qoǯirsau̹.</ta>
            <ta e="T11" id="Seg_1054" s="T8">tʼai Кonom qoǯir̹sau̹.</ta>
            <ta e="T16" id="Seg_1055" s="T11">i tʼötka Пaraskowja qoǯir̹sau̹ küdeptɨbɨlʼe.</ta>
            <ta e="T20" id="Seg_1056" s="T16">man qoǯirsau̹ sɨrʼi pajan.</ta>
            <ta e="T27" id="Seg_1057" s="T20">qɨgɨzau paːrgɨlɨg(ɣ)u, a tep mʼekĝa jas mewaː.</ta>
            <ta e="T33" id="Seg_1058" s="T27">man na qoǯirqud̂dau̹, qajamɨ qum qunɨš.</ta>
            <ta e="T38" id="Seg_1059" s="T33">man sɨdɨdʼän i tʼürlʼe jübəran.</ta>
            <ta e="T44" id="Seg_1060" s="T38">man tʼäran qajamə quǯin i sɨdɨdʼän.</ta>
            <ta e="T48" id="Seg_1061" s="T44">na küdrʼim wes kennau̹.</ta>
            <ta e="T51" id="Seg_1062" s="T48">aj qotdolban (qotdonnan).</ta>
            <ta e="T59" id="Seg_1063" s="T51">man jeʒlʼi qoǯirnau staroj jedom, qajam qum kunɨš.</ta>
            <ta e="T64" id="Seg_1064" s="T59">aj küdeptan, matqən onʼen amdan.</ta>
            <ta e="T67" id="Seg_1065" s="T64">qajda stukajtšelʼe übəran.</ta>
            <ta e="T69" id="Seg_1066" s="T67">man koronnan.</ta>
            <ta e="T71" id="Seg_1067" s="T69">kudɨ natʼen?</ta>
            <ta e="T77" id="Seg_1068" s="T71">a tep: maɣn, maɣn, maɣn ɛto.</ta>
            <ta e="T80" id="Seg_1069" s="T77">man asʼ üːdəku.</ta>
            <ta e="T84" id="Seg_1070" s="T80">a tep innä sɨɣənnɨn.</ta>
            <ta e="T87" id="Seg_1071" s="T84">madan oːlloɣɨn akoškaɣa.</ta>
            <ta e="T91" id="Seg_1072" s="T87">tep na akoškaon nʼaːpb̂od̂in.</ta>
            <ta e="T94" id="Seg_1073" s="T91">man paroɣɨn pɨkĝəlan.</ta>
            <ta e="T97" id="Seg_1074" s="T94">man d̂a tawa kə(ɨ)tʼ(cʼ)ewaːnnan.</ta>
            <ta e="T104" id="Seg_1075" s="T97">man maǯed̂id̂an, a Вalotʼka ippaː i lagwaːtpa.</ta>
            <ta e="T109" id="Seg_1076" s="T104">man tʼäran: qajnozɨm qɨtʼölǯat?</ta>
            <ta e="T111" id="Seg_1077" s="T109">i sɨdedʼan.</ta>
            <ta e="T116" id="Seg_1078" s="T111">man tamdʼel Фilatam qoǯirsan küdeptəbəlʼe.</ta>
            <ta e="T119" id="Seg_1079" s="T116">neldʼä toːbɨn amdɨs.</ta>
            <ta e="T124" id="Seg_1080" s="T119">mekka tʼarɨn: man toblau qannɨbaːt.</ta>
            <ta e="T128" id="Seg_1081" s="T124">man tebɨn tobɨm orannau̹.</ta>
            <ta e="T130" id="Seg_1082" s="T128">tebnan qannɨbaːt.</ta>
            <ta e="T134" id="Seg_1083" s="T130">imattə tʼäkku, qudnä nušno.</ta>
            <ta e="T138" id="Seg_1084" s="T134">tebnan agat tɨmnʼat jetta.</ta>
            <ta e="T140" id="Seg_1085" s="T138">karmonʼätšukus kɨːdan.</ta>
            <ta e="T145" id="Seg_1086" s="T140">tamdʼel man küdeptɨbɨlʼe tödɨn qoǯirsau.</ta>
            <ta e="T150" id="Seg_1087" s="T145">koška tötpa, müzelǯɨ, tan koškal.</ta>
            <ta e="T154" id="Seg_1088" s="T150">tamdʼel man küːdrʼem qoǯərsan.</ta>
            <ta e="T156" id="Seg_1089" s="T154">qəː(ä)nnolottə tʼötʼöu̹an.</ta>
            <ta e="T159" id="Seg_1090" s="T156">mek(ĝ)a soːn jekon.</ta>
            <ta e="T161" id="Seg_1091" s="T159">teper adɨltšan.</ta>
            <ta e="T165" id="Seg_1092" s="T161">qaj jenɨš as tunou̹.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_1093" s="T1">Man tamdʼel küːdeptɨbɨzan son. </ta>
            <ta e="T8" id="Seg_1094" s="T5">Staraj jedom qoǯirsau. </ta>
            <ta e="T11" id="Seg_1095" s="T8">Tʼai Кonom qoǯirsau. </ta>
            <ta e="T16" id="Seg_1096" s="T11">I tʼötka Paraskowja qoǯirsau küdeptɨbɨlʼe. </ta>
            <ta e="T20" id="Seg_1097" s="T16">Man qoǯirsau sɨrʼi pajan. </ta>
            <ta e="T27" id="Seg_1098" s="T20">Qɨgɨzau paːrgɨlɨgu, a tep mʼekga jas mewaː. </ta>
            <ta e="T33" id="Seg_1099" s="T27">Man na qoǯirquddau, qajamɨ qum qunɨš. </ta>
            <ta e="T38" id="Seg_1100" s="T33">Man sɨdɨdʼän i tʼürlʼe jübəran. </ta>
            <ta e="T44" id="Seg_1101" s="T38">Man tʼäran: “Qajamə quǯin”, i sɨdɨdʼän. </ta>
            <ta e="T48" id="Seg_1102" s="T44">Na küdrʼim wes kennau. </ta>
            <ta e="T51" id="Seg_1103" s="T48">Aj qotdolban (qotdonnan). </ta>
            <ta e="T59" id="Seg_1104" s="T51">Man jeʒlʼi qoǯirnau staroj jedom, qajam qum kunɨš. </ta>
            <ta e="T64" id="Seg_1105" s="T59">Aj küdeptan, matqən onʼen amdan. </ta>
            <ta e="T67" id="Seg_1106" s="T64">Qajda stukajčelʼe übəran. </ta>
            <ta e="T69" id="Seg_1107" s="T67">Man koronnan. </ta>
            <ta e="T71" id="Seg_1108" s="T69">Kudɨ natʼen? </ta>
            <ta e="T77" id="Seg_1109" s="T71">A tep: “Maɣn, maɣn, maɣn ɛto.” </ta>
            <ta e="T80" id="Seg_1110" s="T77">Man asʼ üːdəku. </ta>
            <ta e="T84" id="Seg_1111" s="T80">A tep innä sɨɣənnɨn. </ta>
            <ta e="T87" id="Seg_1112" s="T84">Madan oːlloɣɨn akoškaɣa. </ta>
            <ta e="T91" id="Seg_1113" s="T87">Tep na akoškaon nʼaːpbodin. </ta>
            <ta e="T94" id="Seg_1114" s="T91">Man paroɣɨn pɨkgəlan. </ta>
            <ta e="T97" id="Seg_1115" s="T94">Man datawa kətʼewaːnnan. </ta>
            <ta e="T104" id="Seg_1116" s="T97">Man maǯedidan, a Вalotʼka ippaː i lagwaːtpa. </ta>
            <ta e="T109" id="Seg_1117" s="T104">Man tʼäran: “Qajno zɨm qɨtʼölǯat?” </ta>
            <ta e="T111" id="Seg_1118" s="T109">I sɨdedʼan. </ta>
            <ta e="T116" id="Seg_1119" s="T111">Man tamdʼel Filatam qoǯirsan küdeptəbəlʼe. </ta>
            <ta e="T119" id="Seg_1120" s="T116">Neldʼä toːbɨn amdɨs. </ta>
            <ta e="T124" id="Seg_1121" s="T119">Mekka tʼarɨn: “Man toblau qannɨbaːt.” </ta>
            <ta e="T128" id="Seg_1122" s="T124">Man tebɨn tobɨm orannau. </ta>
            <ta e="T130" id="Seg_1123" s="T128">Tebnan qannɨbaːt. </ta>
            <ta e="T134" id="Seg_1124" s="T130">Imattə tʼäkku, qudnä nušno. </ta>
            <ta e="T138" id="Seg_1125" s="T134">Tebnan agat tɨmnʼat jetta. </ta>
            <ta e="T140" id="Seg_1126" s="T138">Karmonʼäčukus kɨːdan. </ta>
            <ta e="T145" id="Seg_1127" s="T140">Tamdʼel man küdeptɨbɨlʼe tödɨn qoǯirsau. </ta>
            <ta e="T150" id="Seg_1128" s="T145">Koška tötpa, müzelǯɨ, tan koškal. </ta>
            <ta e="T154" id="Seg_1129" s="T150">Tamdʼel man küːdrʼem qoǯərsan. </ta>
            <ta e="T156" id="Seg_1130" s="T154">Qəːnnolottə tʼötʼöuan. </ta>
            <ta e="T159" id="Seg_1131" s="T156">Meka soːn jekon. </ta>
            <ta e="T161" id="Seg_1132" s="T159">Teper adɨlčan. </ta>
            <ta e="T165" id="Seg_1133" s="T161">Qaj jenɨš as tunou. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_1134" s="T1">man</ta>
            <ta e="T3" id="Seg_1135" s="T2">tam-dʼel</ta>
            <ta e="T4" id="Seg_1136" s="T3">küːde-ptɨ-bɨ-za-n</ta>
            <ta e="T5" id="Seg_1137" s="T4">son</ta>
            <ta e="T6" id="Seg_1138" s="T5">staraj</ta>
            <ta e="T7" id="Seg_1139" s="T6">jedo-m</ta>
            <ta e="T8" id="Seg_1140" s="T7">qo-ǯir-sa-u</ta>
            <ta e="T9" id="Seg_1141" s="T8">tʼai</ta>
            <ta e="T10" id="Seg_1142" s="T9">Кon-o-m</ta>
            <ta e="T11" id="Seg_1143" s="T10">qo-ǯir-sa-u</ta>
            <ta e="T12" id="Seg_1144" s="T11">i</ta>
            <ta e="T13" id="Seg_1145" s="T12">tʼötka</ta>
            <ta e="T14" id="Seg_1146" s="T13">Paraskowja</ta>
            <ta e="T15" id="Seg_1147" s="T14">qo-ǯir-sa-u</ta>
            <ta e="T16" id="Seg_1148" s="T15">küde-ptɨ-bɨ-lʼe</ta>
            <ta e="T17" id="Seg_1149" s="T16">man</ta>
            <ta e="T18" id="Seg_1150" s="T17">qo-ǯir-sa-u</ta>
            <ta e="T19" id="Seg_1151" s="T18">sɨrʼi</ta>
            <ta e="T20" id="Seg_1152" s="T19">paja-n</ta>
            <ta e="T21" id="Seg_1153" s="T20">qɨgɨ-za-u</ta>
            <ta e="T22" id="Seg_1154" s="T21">paːrgɨ-lɨ-gu</ta>
            <ta e="T23" id="Seg_1155" s="T22">a</ta>
            <ta e="T24" id="Seg_1156" s="T23">tep</ta>
            <ta e="T25" id="Seg_1157" s="T24">mʼekga</ta>
            <ta e="T26" id="Seg_1158" s="T25">jas</ta>
            <ta e="T27" id="Seg_1159" s="T26">me-waː</ta>
            <ta e="T28" id="Seg_1160" s="T27">man</ta>
            <ta e="T29" id="Seg_1161" s="T28">na</ta>
            <ta e="T30" id="Seg_1162" s="T29">qo-ǯir-qu-dda-u</ta>
            <ta e="T31" id="Seg_1163" s="T30">qaj-amɨ</ta>
            <ta e="T32" id="Seg_1164" s="T31">qum</ta>
            <ta e="T33" id="Seg_1165" s="T32">qu-nɨš</ta>
            <ta e="T34" id="Seg_1166" s="T33">man</ta>
            <ta e="T35" id="Seg_1167" s="T34">sɨdɨ-dʼä-n</ta>
            <ta e="T36" id="Seg_1168" s="T35">i</ta>
            <ta e="T37" id="Seg_1169" s="T36">tʼür-lʼe</ta>
            <ta e="T38" id="Seg_1170" s="T37">jübə-ra-n</ta>
            <ta e="T39" id="Seg_1171" s="T38">man</ta>
            <ta e="T40" id="Seg_1172" s="T39">tʼära-n</ta>
            <ta e="T41" id="Seg_1173" s="T40">qaj-amə</ta>
            <ta e="T42" id="Seg_1174" s="T41">qu-ǯi-n</ta>
            <ta e="T43" id="Seg_1175" s="T42">i</ta>
            <ta e="T44" id="Seg_1176" s="T43">sɨdɨ-dʼä-n</ta>
            <ta e="T45" id="Seg_1177" s="T44">na</ta>
            <ta e="T46" id="Seg_1178" s="T45">küd-rʼ-i-m</ta>
            <ta e="T47" id="Seg_1179" s="T46">wes</ta>
            <ta e="T48" id="Seg_1180" s="T47">ken-na-u</ta>
            <ta e="T49" id="Seg_1181" s="T48">aj</ta>
            <ta e="T50" id="Seg_1182" s="T49">qotdo-l-ba-n</ta>
            <ta e="T51" id="Seg_1183" s="T50">qotdo-nna-n</ta>
            <ta e="T52" id="Seg_1184" s="T51">man</ta>
            <ta e="T53" id="Seg_1185" s="T52">jeʒlʼi</ta>
            <ta e="T54" id="Seg_1186" s="T53">qo-ǯir-na-u</ta>
            <ta e="T55" id="Seg_1187" s="T54">staroj</ta>
            <ta e="T56" id="Seg_1188" s="T55">jedo-m</ta>
            <ta e="T57" id="Seg_1189" s="T56">qaj-am</ta>
            <ta e="T58" id="Seg_1190" s="T57">qum</ta>
            <ta e="T59" id="Seg_1191" s="T58">ku-nɨš</ta>
            <ta e="T60" id="Seg_1192" s="T59">aj</ta>
            <ta e="T61" id="Seg_1193" s="T60">küde-pta-n</ta>
            <ta e="T62" id="Seg_1194" s="T61">mat-qən</ta>
            <ta e="T63" id="Seg_1195" s="T62">onʼen</ta>
            <ta e="T64" id="Seg_1196" s="T63">amda-n</ta>
            <ta e="T65" id="Seg_1197" s="T64">qaj-da</ta>
            <ta e="T66" id="Seg_1198" s="T65">stukaj-če-lʼe</ta>
            <ta e="T67" id="Seg_1199" s="T66">übə-r-a-n</ta>
            <ta e="T68" id="Seg_1200" s="T67">man</ta>
            <ta e="T69" id="Seg_1201" s="T68">kor-on-na-n</ta>
            <ta e="T70" id="Seg_1202" s="T69">kudɨ</ta>
            <ta e="T71" id="Seg_1203" s="T70">natʼe-n</ta>
            <ta e="T72" id="Seg_1204" s="T71">a</ta>
            <ta e="T73" id="Seg_1205" s="T72">tep</ta>
            <ta e="T74" id="Seg_1206" s="T73">maɣn</ta>
            <ta e="T75" id="Seg_1207" s="T74">maɣn</ta>
            <ta e="T76" id="Seg_1208" s="T75">maɣn</ta>
            <ta e="T78" id="Seg_1209" s="T77">man</ta>
            <ta e="T79" id="Seg_1210" s="T78">asʼ</ta>
            <ta e="T80" id="Seg_1211" s="T79">üːdə-ku</ta>
            <ta e="T81" id="Seg_1212" s="T80">a</ta>
            <ta e="T82" id="Seg_1213" s="T81">tep</ta>
            <ta e="T83" id="Seg_1214" s="T82">innä</ta>
            <ta e="T84" id="Seg_1215" s="T83">sɨɣə-nnɨ-n</ta>
            <ta e="T85" id="Seg_1216" s="T84">mada-n</ta>
            <ta e="T86" id="Seg_1217" s="T85">oːllo-ɣɨn</ta>
            <ta e="T87" id="Seg_1218" s="T86">akoška-ɣa</ta>
            <ta e="T88" id="Seg_1219" s="T87">tep</ta>
            <ta e="T89" id="Seg_1220" s="T88">na</ta>
            <ta e="T90" id="Seg_1221" s="T89">akoška-on</ta>
            <ta e="T91" id="Seg_1222" s="T90">nʼaːpbo-di-n</ta>
            <ta e="T92" id="Seg_1223" s="T91">man</ta>
            <ta e="T93" id="Seg_1224" s="T92">par-o-ɣɨn</ta>
            <ta e="T94" id="Seg_1225" s="T93">pɨkgəl-a-n</ta>
            <ta e="T95" id="Seg_1226" s="T94">man</ta>
            <ta e="T96" id="Seg_1227" s="T95">datawa</ta>
            <ta e="T97" id="Seg_1228" s="T96">kətʼe-waːn-na-n</ta>
            <ta e="T98" id="Seg_1229" s="T97">man</ta>
            <ta e="T99" id="Seg_1230" s="T98">maǯe-di-da-n</ta>
            <ta e="T100" id="Seg_1231" s="T99">a</ta>
            <ta e="T101" id="Seg_1232" s="T100">Вalotʼka</ta>
            <ta e="T102" id="Seg_1233" s="T101">ippaː</ta>
            <ta e="T103" id="Seg_1234" s="T102">i</ta>
            <ta e="T104" id="Seg_1235" s="T103">lagwaːt-pa</ta>
            <ta e="T105" id="Seg_1236" s="T104">man</ta>
            <ta e="T106" id="Seg_1237" s="T105">tʼära-n</ta>
            <ta e="T107" id="Seg_1238" s="T106">qaj-no</ta>
            <ta e="T108" id="Seg_1239" s="T107">zɨm</ta>
            <ta e="T109" id="Seg_1240" s="T108">qɨtʼö-lǯa-t</ta>
            <ta e="T110" id="Seg_1241" s="T109">i</ta>
            <ta e="T111" id="Seg_1242" s="T110">sɨde-dʼa-n</ta>
            <ta e="T112" id="Seg_1243" s="T111">man</ta>
            <ta e="T113" id="Seg_1244" s="T112">tam-dʼel</ta>
            <ta e="T114" id="Seg_1245" s="T113">Filat-a-m</ta>
            <ta e="T115" id="Seg_1246" s="T114">qo-ǯir-sa-n</ta>
            <ta e="T116" id="Seg_1247" s="T115">küde-ptə-bə-lʼe</ta>
            <ta e="T117" id="Seg_1248" s="T116">neldʼä</ta>
            <ta e="T118" id="Seg_1249" s="T117">toːb-ɨ-n</ta>
            <ta e="T119" id="Seg_1250" s="T118">amdɨ-s</ta>
            <ta e="T120" id="Seg_1251" s="T119">mekka</ta>
            <ta e="T121" id="Seg_1252" s="T120">tʼarɨ-n</ta>
            <ta e="T122" id="Seg_1253" s="T121">Man</ta>
            <ta e="T123" id="Seg_1254" s="T122">tob-la-u</ta>
            <ta e="T124" id="Seg_1255" s="T123">qannɨ-baː-t</ta>
            <ta e="T125" id="Seg_1256" s="T124">man</ta>
            <ta e="T126" id="Seg_1257" s="T125">teb-ɨ-n</ta>
            <ta e="T127" id="Seg_1258" s="T126">tob-ɨ-m</ta>
            <ta e="T128" id="Seg_1259" s="T127">oran-na-u</ta>
            <ta e="T129" id="Seg_1260" s="T128">teb-nan</ta>
            <ta e="T130" id="Seg_1261" s="T129">qannɨ-baː-t</ta>
            <ta e="T131" id="Seg_1262" s="T130">imat-tə</ta>
            <ta e="T132" id="Seg_1263" s="T131">tʼäkku</ta>
            <ta e="T133" id="Seg_1264" s="T132">qud-nä</ta>
            <ta e="T134" id="Seg_1265" s="T133">nušno</ta>
            <ta e="T135" id="Seg_1266" s="T134">teb-nan</ta>
            <ta e="T136" id="Seg_1267" s="T135">aga-t</ta>
            <ta e="T137" id="Seg_1268" s="T136">tɨmnʼa-t</ta>
            <ta e="T138" id="Seg_1269" s="T137">je-tta</ta>
            <ta e="T139" id="Seg_1270" s="T138">karmonʼ-ä-ču-ku-s</ta>
            <ta e="T140" id="Seg_1271" s="T139">kɨːdan</ta>
            <ta e="T141" id="Seg_1272" s="T140">tam-dʼel</ta>
            <ta e="T142" id="Seg_1273" s="T141">man</ta>
            <ta e="T143" id="Seg_1274" s="T142">küde-ptɨ-bɨ-lʼe</ta>
            <ta e="T144" id="Seg_1275" s="T143">töd-ɨ-n</ta>
            <ta e="T145" id="Seg_1276" s="T144">qo-ǯir-sa-u</ta>
            <ta e="T146" id="Seg_1277" s="T145">koška</ta>
            <ta e="T147" id="Seg_1278" s="T146">töt-pa</ta>
            <ta e="T148" id="Seg_1279" s="T147">müzel-ǯɨ</ta>
            <ta e="T149" id="Seg_1280" s="T148">tat</ta>
            <ta e="T150" id="Seg_1281" s="T149">koška-l</ta>
            <ta e="T151" id="Seg_1282" s="T150">tam-dʼel</ta>
            <ta e="T152" id="Seg_1283" s="T151">man</ta>
            <ta e="T153" id="Seg_1284" s="T152">küːd-rʼ-e-m</ta>
            <ta e="T154" id="Seg_1285" s="T153">qo-ǯər-sa-n</ta>
            <ta e="T155" id="Seg_1286" s="T154">qəː-n-nolo-ttə</ta>
            <ta e="T156" id="Seg_1287" s="T155">tʼötʼöu-a-n</ta>
            <ta e="T157" id="Seg_1288" s="T156">meka</ta>
            <ta e="T158" id="Seg_1289" s="T157">soːn</ta>
            <ta e="T159" id="Seg_1290" s="T158">je-ko-n</ta>
            <ta e="T160" id="Seg_1291" s="T159">teper</ta>
            <ta e="T161" id="Seg_1292" s="T160">adɨ-lča-n</ta>
            <ta e="T162" id="Seg_1293" s="T161">qaj</ta>
            <ta e="T163" id="Seg_1294" s="T162">je-nɨš</ta>
            <ta e="T164" id="Seg_1295" s="T163">as</ta>
            <ta e="T165" id="Seg_1296" s="T164">tuno-u</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1297" s="T1">man</ta>
            <ta e="T3" id="Seg_1298" s="T2">taw-dʼel</ta>
            <ta e="T4" id="Seg_1299" s="T3">ködɨ-ptɨ-mbɨ-sɨ-ŋ</ta>
            <ta e="T5" id="Seg_1300" s="T4">son</ta>
            <ta e="T6" id="Seg_1301" s="T5">staːrəj</ta>
            <ta e="T7" id="Seg_1302" s="T6">eːde-m</ta>
            <ta e="T8" id="Seg_1303" s="T7">qo-nǯir-sɨ-w</ta>
            <ta e="T9" id="Seg_1304" s="T8">dʼaja</ta>
            <ta e="T10" id="Seg_1305" s="T9">Кon-ɨ-m</ta>
            <ta e="T11" id="Seg_1306" s="T10">qo-nǯir-sɨ-w</ta>
            <ta e="T12" id="Seg_1307" s="T11">i</ta>
            <ta e="T13" id="Seg_1308" s="T12">tʼötka</ta>
            <ta e="T14" id="Seg_1309" s="T13">Paraskowja</ta>
            <ta e="T15" id="Seg_1310" s="T14">qo-nǯir-sɨ-w</ta>
            <ta e="T16" id="Seg_1311" s="T15">ködɨ-ptɨ-mbɨ-le</ta>
            <ta e="T17" id="Seg_1312" s="T16">man</ta>
            <ta e="T18" id="Seg_1313" s="T17">qo-nǯir-sɨ-w</ta>
            <ta e="T19" id="Seg_1314" s="T18">sɨr</ta>
            <ta e="T20" id="Seg_1315" s="T19">paja-n</ta>
            <ta e="T21" id="Seg_1316" s="T20">kɨgɨ-sɨ-w</ta>
            <ta e="T22" id="Seg_1317" s="T21">pargəl-lɨ-gu</ta>
            <ta e="T23" id="Seg_1318" s="T22">a</ta>
            <ta e="T24" id="Seg_1319" s="T23">täp</ta>
            <ta e="T25" id="Seg_1320" s="T24">mekka</ta>
            <ta e="T26" id="Seg_1321" s="T25">asa</ta>
            <ta e="T27" id="Seg_1322" s="T26">meː-nɨ</ta>
            <ta e="T28" id="Seg_1323" s="T27">man</ta>
            <ta e="T29" id="Seg_1324" s="T28">na</ta>
            <ta e="T30" id="Seg_1325" s="T29">qo-nǯir-ku-ntɨ-w</ta>
            <ta e="T31" id="Seg_1326" s="T30">qaj-amə</ta>
            <ta e="T32" id="Seg_1327" s="T31">qum</ta>
            <ta e="T33" id="Seg_1328" s="T32">quː-nɨš</ta>
            <ta e="T34" id="Seg_1329" s="T33">man</ta>
            <ta e="T35" id="Seg_1330" s="T34">söde-či-ŋ</ta>
            <ta e="T36" id="Seg_1331" s="T35">i</ta>
            <ta e="T37" id="Seg_1332" s="T36">tʼüru-le</ta>
            <ta e="T38" id="Seg_1333" s="T37">übɨ-rɨ-ŋ</ta>
            <ta e="T39" id="Seg_1334" s="T38">man</ta>
            <ta e="T40" id="Seg_1335" s="T39">tʼärɨ-ŋ</ta>
            <ta e="T41" id="Seg_1336" s="T40">qaj-amə</ta>
            <ta e="T42" id="Seg_1337" s="T41">quː-enǯɨ-n</ta>
            <ta e="T43" id="Seg_1338" s="T42">i</ta>
            <ta e="T44" id="Seg_1339" s="T43">söde-či-ŋ</ta>
            <ta e="T45" id="Seg_1340" s="T44">na</ta>
            <ta e="T46" id="Seg_1341" s="T45">ködɨ-r-ptä-m</ta>
            <ta e="T47" id="Seg_1342" s="T46">wesʼ</ta>
            <ta e="T48" id="Seg_1343" s="T47">ket-nɨ-w</ta>
            <ta e="T49" id="Seg_1344" s="T48">aj</ta>
            <ta e="T50" id="Seg_1345" s="T49">qondu-l-mbɨ-ŋ</ta>
            <ta e="T51" id="Seg_1346" s="T50">qondu-ntɨ-ŋ</ta>
            <ta e="T52" id="Seg_1347" s="T51">man</ta>
            <ta e="T53" id="Seg_1348" s="T52">jeʒlʼe</ta>
            <ta e="T54" id="Seg_1349" s="T53">qo-nǯir-nɨ-w</ta>
            <ta e="T55" id="Seg_1350" s="T54">staːrəj</ta>
            <ta e="T56" id="Seg_1351" s="T55">eːde-m</ta>
            <ta e="T57" id="Seg_1352" s="T56">qaj-amə</ta>
            <ta e="T58" id="Seg_1353" s="T57">qum</ta>
            <ta e="T59" id="Seg_1354" s="T58">quː-nɨš</ta>
            <ta e="T60" id="Seg_1355" s="T59">aj</ta>
            <ta e="T61" id="Seg_1356" s="T60">ködɨ-ptɨ-ŋ</ta>
            <ta e="T62" id="Seg_1357" s="T61">maːt-qɨn</ta>
            <ta e="T63" id="Seg_1358" s="T62">oneŋ</ta>
            <ta e="T64" id="Seg_1359" s="T63">amdɨ-ŋ</ta>
            <ta e="T65" id="Seg_1360" s="T64">qaj-ta</ta>
            <ta e="T66" id="Seg_1361" s="T65">stukaj-ču-le</ta>
            <ta e="T67" id="Seg_1362" s="T66">übɨ-r-ɨ-n</ta>
            <ta e="T68" id="Seg_1363" s="T67">man</ta>
            <ta e="T69" id="Seg_1364" s="T68">kur-ol-nɨ-ŋ</ta>
            <ta e="T70" id="Seg_1365" s="T69">kud</ta>
            <ta e="T71" id="Seg_1366" s="T70">*natʼe-n</ta>
            <ta e="T72" id="Seg_1367" s="T71">a</ta>
            <ta e="T73" id="Seg_1368" s="T72">täp</ta>
            <ta e="T74" id="Seg_1369" s="T73">man</ta>
            <ta e="T75" id="Seg_1370" s="T74">man</ta>
            <ta e="T76" id="Seg_1371" s="T75">man</ta>
            <ta e="T78" id="Seg_1372" s="T77">man</ta>
            <ta e="T79" id="Seg_1373" s="T78">asa</ta>
            <ta e="T80" id="Seg_1374" s="T79">üdə-ku</ta>
            <ta e="T81" id="Seg_1375" s="T80">a</ta>
            <ta e="T82" id="Seg_1376" s="T81">täp</ta>
            <ta e="T83" id="Seg_1377" s="T82">innä</ta>
            <ta e="T84" id="Seg_1378" s="T83">sɨɣə-nɨ-n</ta>
            <ta e="T85" id="Seg_1379" s="T84">maːda-n</ta>
            <ta e="T86" id="Seg_1380" s="T85">olə-qɨn</ta>
            <ta e="T87" id="Seg_1381" s="T86">akoška-ka</ta>
            <ta e="T88" id="Seg_1382" s="T87">täp</ta>
            <ta e="T89" id="Seg_1383" s="T88">na</ta>
            <ta e="T90" id="Seg_1384" s="T89">akoška-un</ta>
            <ta e="T91" id="Seg_1385" s="T90">nʼampɛː-dʼi-n</ta>
            <ta e="T92" id="Seg_1386" s="T91">man</ta>
            <ta e="T93" id="Seg_1387" s="T92">par-ɨ-qɨn</ta>
            <ta e="T94" id="Seg_1388" s="T93">pɨŋgəl-nɨ-n</ta>
            <ta e="T95" id="Seg_1389" s="T94">man</ta>
            <ta e="T96" id="Seg_1390" s="T95">tatawa</ta>
            <ta e="T97" id="Seg_1391" s="T96">*qɨčo-wat-nɨ-ŋ</ta>
            <ta e="T98" id="Seg_1392" s="T97">man</ta>
            <ta e="T99" id="Seg_1393" s="T98">manǯu-dʼi-ntɨ-ŋ</ta>
            <ta e="T100" id="Seg_1394" s="T99">a</ta>
            <ta e="T101" id="Seg_1395" s="T100">Вalotʼka</ta>
            <ta e="T102" id="Seg_1396" s="T101">ippɨ</ta>
            <ta e="T103" id="Seg_1397" s="T102">i</ta>
            <ta e="T104" id="Seg_1398" s="T103">laːɣwat-mbɨ</ta>
            <ta e="T105" id="Seg_1399" s="T104">man</ta>
            <ta e="T106" id="Seg_1400" s="T105">tʼärɨ-ŋ</ta>
            <ta e="T107" id="Seg_1401" s="T106">qaj-no</ta>
            <ta e="T108" id="Seg_1402" s="T107">mazɨm</ta>
            <ta e="T109" id="Seg_1403" s="T108">*qɨčo-lǯi-ntə</ta>
            <ta e="T110" id="Seg_1404" s="T109">i</ta>
            <ta e="T111" id="Seg_1405" s="T110">söde-či-ŋ</ta>
            <ta e="T112" id="Seg_1406" s="T111">man</ta>
            <ta e="T113" id="Seg_1407" s="T112">taw-dʼel</ta>
            <ta e="T114" id="Seg_1408" s="T113">Filat-ɨ-m</ta>
            <ta e="T115" id="Seg_1409" s="T114">qo-nǯir-sɨ-ŋ</ta>
            <ta e="T116" id="Seg_1410" s="T115">ködɨ-ptɨ-mbɨ-le</ta>
            <ta e="T117" id="Seg_1411" s="T116">nʼälʼdʼe</ta>
            <ta e="T118" id="Seg_1412" s="T117">tob-ɨ-ŋ</ta>
            <ta e="T119" id="Seg_1413" s="T118">amdɨ-sɨ</ta>
            <ta e="T120" id="Seg_1414" s="T119">mekka</ta>
            <ta e="T121" id="Seg_1415" s="T120">tʼärɨ-n</ta>
            <ta e="T122" id="Seg_1416" s="T121">man</ta>
            <ta e="T123" id="Seg_1417" s="T122">tob-la-w</ta>
            <ta e="T124" id="Seg_1418" s="T123">qandɨ-mbɨ-tɨn</ta>
            <ta e="T125" id="Seg_1419" s="T124">man</ta>
            <ta e="T126" id="Seg_1420" s="T125">täp-ɨ-n</ta>
            <ta e="T127" id="Seg_1421" s="T126">tob-ɨ-m</ta>
            <ta e="T128" id="Seg_1422" s="T127">oral-nɨ-w</ta>
            <ta e="T129" id="Seg_1423" s="T128">täp-nan</ta>
            <ta e="T130" id="Seg_1424" s="T129">qandɨ-mbɨ-tɨn</ta>
            <ta e="T131" id="Seg_1425" s="T130">imat-tə</ta>
            <ta e="T132" id="Seg_1426" s="T131">tʼäkku</ta>
            <ta e="T133" id="Seg_1427" s="T132">kud-nä</ta>
            <ta e="T134" id="Seg_1428" s="T133">nušno</ta>
            <ta e="T135" id="Seg_1429" s="T134">täp-nan</ta>
            <ta e="T136" id="Seg_1430" s="T135">agaː-tə</ta>
            <ta e="T137" id="Seg_1431" s="T136">tɨmnʼa-tə</ta>
            <ta e="T138" id="Seg_1432" s="T137">eː-ntɨ</ta>
            <ta e="T139" id="Seg_1433" s="T138">karmonʼ-ɨ-ču-ku-sɨ</ta>
            <ta e="T140" id="Seg_1434" s="T139">qɨdan</ta>
            <ta e="T141" id="Seg_1435" s="T140">taw-dʼel</ta>
            <ta e="T142" id="Seg_1436" s="T141">man</ta>
            <ta e="T143" id="Seg_1437" s="T142">ködɨ-ptɨ-mbɨ-le</ta>
            <ta e="T144" id="Seg_1438" s="T143">töt-ɨ-n</ta>
            <ta e="T145" id="Seg_1439" s="T144">qo-nǯir-sɨ-w</ta>
            <ta e="T146" id="Seg_1440" s="T145">koška</ta>
            <ta e="T147" id="Seg_1441" s="T146">töt-mbɨ</ta>
            <ta e="T148" id="Seg_1442" s="T147">müzel-ǯi</ta>
            <ta e="T149" id="Seg_1443" s="T148">tan</ta>
            <ta e="T150" id="Seg_1444" s="T149">koška-l</ta>
            <ta e="T151" id="Seg_1445" s="T150">taw-dʼel</ta>
            <ta e="T152" id="Seg_1446" s="T151">man</ta>
            <ta e="T153" id="Seg_1447" s="T152">ködɨ-r-ptä-m</ta>
            <ta e="T154" id="Seg_1448" s="T153">qo-nǯir-sɨ-ŋ</ta>
            <ta e="T155" id="Seg_1449" s="T154">qä-n-nolo-ntə</ta>
            <ta e="T156" id="Seg_1450" s="T155">tʼötʼöu-nɨ-ŋ</ta>
            <ta e="T157" id="Seg_1451" s="T156">mekka</ta>
            <ta e="T158" id="Seg_1452" s="T157">soŋ</ta>
            <ta e="T159" id="Seg_1453" s="T158">eː-ku-n</ta>
            <ta e="T160" id="Seg_1454" s="T159">teper</ta>
            <ta e="T161" id="Seg_1455" s="T160">adɨ-lǯi-ŋ</ta>
            <ta e="T162" id="Seg_1456" s="T161">qaj</ta>
            <ta e="T163" id="Seg_1457" s="T162">eː-nɨš</ta>
            <ta e="T164" id="Seg_1458" s="T163">asa</ta>
            <ta e="T165" id="Seg_1459" s="T164">tonu-w</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1460" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_1461" s="T2">this-day.[NOM]</ta>
            <ta e="T4" id="Seg_1462" s="T3">dream-VBLZ-DUR-PST-1SG.S</ta>
            <ta e="T5" id="Seg_1463" s="T4">dream.[NOM]</ta>
            <ta e="T6" id="Seg_1464" s="T5">old</ta>
            <ta e="T7" id="Seg_1465" s="T6">village-ACC</ta>
            <ta e="T8" id="Seg_1466" s="T7">see-DRV-PST-1SG.O</ta>
            <ta e="T9" id="Seg_1467" s="T8">uncle.[NOM]</ta>
            <ta e="T10" id="Seg_1468" s="T9">Kon-EP-ACC</ta>
            <ta e="T11" id="Seg_1469" s="T10">see-DRV-PST-1SG.O</ta>
            <ta e="T12" id="Seg_1470" s="T11">and</ta>
            <ta e="T13" id="Seg_1471" s="T12">aunt.[NOM]</ta>
            <ta e="T14" id="Seg_1472" s="T13">Praskovja.[NOM]</ta>
            <ta e="T15" id="Seg_1473" s="T14">see-DRV-PST-1SG.O</ta>
            <ta e="T16" id="Seg_1474" s="T15">dream-VBLZ-DUR-CVB</ta>
            <ta e="T17" id="Seg_1475" s="T16">I.NOM</ta>
            <ta e="T18" id="Seg_1476" s="T17">see-DRV-PST-1SG.O</ta>
            <ta e="T19" id="Seg_1477" s="T18">cow.[NOM]</ta>
            <ta e="T20" id="Seg_1478" s="T19">female-GEN</ta>
            <ta e="T21" id="Seg_1479" s="T20">want-PST-1SG.O</ta>
            <ta e="T22" id="Seg_1480" s="T21">milk-RES-INF</ta>
            <ta e="T23" id="Seg_1481" s="T22">and</ta>
            <ta e="T24" id="Seg_1482" s="T23">(s)he.[NOM]</ta>
            <ta e="T25" id="Seg_1483" s="T24">I.ALL</ta>
            <ta e="T26" id="Seg_1484" s="T25">NEG</ta>
            <ta e="T27" id="Seg_1485" s="T26">do-CO.[3SG.S]</ta>
            <ta e="T28" id="Seg_1486" s="T27">I.NOM</ta>
            <ta e="T29" id="Seg_1487" s="T28">this.[NOM]</ta>
            <ta e="T30" id="Seg_1488" s="T29">see-DRV-HAB-INFER-1SG.O</ta>
            <ta e="T31" id="Seg_1489" s="T30">what-INDEF2.[NOM]</ta>
            <ta e="T32" id="Seg_1490" s="T31">human.being.[NOM]</ta>
            <ta e="T33" id="Seg_1491" s="T32">die-POT.FUT.3SG</ta>
            <ta e="T34" id="Seg_1492" s="T33">I.NOM</ta>
            <ta e="T35" id="Seg_1493" s="T34">awake-RFL-1SG.S</ta>
            <ta e="T36" id="Seg_1494" s="T35">and</ta>
            <ta e="T37" id="Seg_1495" s="T36">cry-CVB</ta>
            <ta e="T38" id="Seg_1496" s="T37">begin-DRV-1SG.S</ta>
            <ta e="T39" id="Seg_1497" s="T38">I.NOM</ta>
            <ta e="T40" id="Seg_1498" s="T39">say-1SG.S</ta>
            <ta e="T41" id="Seg_1499" s="T40">what-INDEF2.[NOM]</ta>
            <ta e="T42" id="Seg_1500" s="T41">die-FUT-3SG.S</ta>
            <ta e="T43" id="Seg_1501" s="T42">and</ta>
            <ta e="T44" id="Seg_1502" s="T43">awake-RFL-1SG.S</ta>
            <ta e="T45" id="Seg_1503" s="T44">this</ta>
            <ta e="T46" id="Seg_1504" s="T45">dream-VBLZ-ACTN-ACC</ta>
            <ta e="T47" id="Seg_1505" s="T46">all</ta>
            <ta e="T48" id="Seg_1506" s="T47">say-CO-1SG.O</ta>
            <ta e="T49" id="Seg_1507" s="T48">again</ta>
            <ta e="T50" id="Seg_1508" s="T49">sleep-INCH-PST.NAR-1SG.S</ta>
            <ta e="T51" id="Seg_1509" s="T50">sleep-INFER-1SG.S</ta>
            <ta e="T52" id="Seg_1510" s="T51">I.NOM</ta>
            <ta e="T53" id="Seg_1511" s="T52">if</ta>
            <ta e="T54" id="Seg_1512" s="T53">see-DRV-CO-1SG.O</ta>
            <ta e="T55" id="Seg_1513" s="T54">old</ta>
            <ta e="T56" id="Seg_1514" s="T55">village-ACC</ta>
            <ta e="T57" id="Seg_1515" s="T56">what-INDEF2</ta>
            <ta e="T58" id="Seg_1516" s="T57">human.being.[NOM]</ta>
            <ta e="T59" id="Seg_1517" s="T58">die-POT.FUT.3SG</ta>
            <ta e="T60" id="Seg_1518" s="T59">again</ta>
            <ta e="T61" id="Seg_1519" s="T60">dream-VBLZ-1SG.S</ta>
            <ta e="T62" id="Seg_1520" s="T61">house-LOC</ta>
            <ta e="T63" id="Seg_1521" s="T62">oneself.1SG</ta>
            <ta e="T64" id="Seg_1522" s="T63">sit-1SG.S</ta>
            <ta e="T65" id="Seg_1523" s="T64">what-INDEF.[NOM]</ta>
            <ta e="T66" id="Seg_1524" s="T65">knock-TR-CVB</ta>
            <ta e="T67" id="Seg_1525" s="T66">begin-FRQ-EP-3SG.S</ta>
            <ta e="T68" id="Seg_1526" s="T67">I.NOM</ta>
            <ta e="T69" id="Seg_1527" s="T68">run-MOM-CO-1SG.S</ta>
            <ta e="T70" id="Seg_1528" s="T69">who.[NOM]</ta>
            <ta e="T71" id="Seg_1529" s="T70">there-ADV.LOC</ta>
            <ta e="T72" id="Seg_1530" s="T71">and</ta>
            <ta e="T73" id="Seg_1531" s="T72">(s)he.[NOM]</ta>
            <ta e="T74" id="Seg_1532" s="T73">I.NOM</ta>
            <ta e="T75" id="Seg_1533" s="T74">I.NOM</ta>
            <ta e="T76" id="Seg_1534" s="T75">I.NOM</ta>
            <ta e="T78" id="Seg_1535" s="T77">I.NOM</ta>
            <ta e="T79" id="Seg_1536" s="T78">NEG</ta>
            <ta e="T80" id="Seg_1537" s="T79">let.go-HAB.[3SG.S]</ta>
            <ta e="T81" id="Seg_1538" s="T80">and</ta>
            <ta e="T82" id="Seg_1539" s="T81">(s)he.[NOM]</ta>
            <ta e="T83" id="Seg_1540" s="T82">up</ta>
            <ta e="T84" id="Seg_1541" s="T83">climb-CO-3SG.S</ta>
            <ta e="T85" id="Seg_1542" s="T84">door-GEN</ta>
            <ta e="T86" id="Seg_1543" s="T85">top-LOC</ta>
            <ta e="T87" id="Seg_1544" s="T86">window-DIM.[3SG.S]</ta>
            <ta e="T88" id="Seg_1545" s="T87">(s)he.[NOM]</ta>
            <ta e="T89" id="Seg_1546" s="T88">this</ta>
            <ta e="T90" id="Seg_1547" s="T89">window-PROL</ta>
            <ta e="T91" id="Seg_1548" s="T90">push.away-RFL-3SG.S</ta>
            <ta e="T92" id="Seg_1549" s="T91">I.GEN</ta>
            <ta e="T93" id="Seg_1550" s="T92">top-EP-LOC</ta>
            <ta e="T94" id="Seg_1551" s="T93">fall.down-CO-3SG.S</ta>
            <ta e="T95" id="Seg_1552" s="T94">I.NOM</ta>
            <ta e="T96" id="Seg_1553" s="T95">to.such.extent</ta>
            <ta e="T97" id="Seg_1554" s="T96">frighten-DETR-CO-1SG.S</ta>
            <ta e="T98" id="Seg_1555" s="T97">I.NOM</ta>
            <ta e="T99" id="Seg_1556" s="T98">look.at-DRV-INFER-1SG.S</ta>
            <ta e="T100" id="Seg_1557" s="T99">and</ta>
            <ta e="T101" id="Seg_1558" s="T100">Volodjka.[NOM]</ta>
            <ta e="T102" id="Seg_1559" s="T101">lie.[3SG.S]</ta>
            <ta e="T103" id="Seg_1560" s="T102">and</ta>
            <ta e="T104" id="Seg_1561" s="T103">begin.to.laugh-DUR.[3SG.S]</ta>
            <ta e="T105" id="Seg_1562" s="T104">I.NOM</ta>
            <ta e="T106" id="Seg_1563" s="T105">say-1SG.S</ta>
            <ta e="T107" id="Seg_1564" s="T106">what-TRL</ta>
            <ta e="T108" id="Seg_1565" s="T107">I.ACC</ta>
            <ta e="T109" id="Seg_1566" s="T108">frighten-PFV-2SG.S</ta>
            <ta e="T110" id="Seg_1567" s="T109">and</ta>
            <ta e="T111" id="Seg_1568" s="T110">awake-RFL-1SG.S</ta>
            <ta e="T112" id="Seg_1569" s="T111">I.NOM</ta>
            <ta e="T113" id="Seg_1570" s="T112">this-day.[NOM]</ta>
            <ta e="T114" id="Seg_1571" s="T113">Filat-EP-ACC</ta>
            <ta e="T115" id="Seg_1572" s="T114">see-DRV-PST-1SG.S</ta>
            <ta e="T116" id="Seg_1573" s="T115">dream-VBLZ-DUR-CVB</ta>
            <ta e="T117" id="Seg_1574" s="T116">bare</ta>
            <ta e="T118" id="Seg_1575" s="T117">leg-EP-ADVZ</ta>
            <ta e="T119" id="Seg_1576" s="T118">sit-PST.[3SG.S]</ta>
            <ta e="T120" id="Seg_1577" s="T119">I.ALL</ta>
            <ta e="T121" id="Seg_1578" s="T120">say-3SG.S</ta>
            <ta e="T122" id="Seg_1579" s="T121">I.NOM</ta>
            <ta e="T123" id="Seg_1580" s="T122">leg-PL.[NOM]-1SG</ta>
            <ta e="T124" id="Seg_1581" s="T123">freeze-PST.NAR-3PL</ta>
            <ta e="T125" id="Seg_1582" s="T124">I.NOM</ta>
            <ta e="T126" id="Seg_1583" s="T125">(s)he-EP-GEN</ta>
            <ta e="T127" id="Seg_1584" s="T126">leg-EP-ACC</ta>
            <ta e="T128" id="Seg_1585" s="T127">take-CO-1SG.O</ta>
            <ta e="T129" id="Seg_1586" s="T128">(s)he-ADES</ta>
            <ta e="T130" id="Seg_1587" s="T129">freeze-RES-3PL</ta>
            <ta e="T131" id="Seg_1588" s="T130">son.[NOM]-3SG</ta>
            <ta e="T132" id="Seg_1589" s="T131">NEG.EX.[3SG]</ta>
            <ta e="T133" id="Seg_1590" s="T132">who-ALL</ta>
            <ta e="T134" id="Seg_1591" s="T133">one.needs</ta>
            <ta e="T135" id="Seg_1592" s="T134">(s)he-ADES</ta>
            <ta e="T136" id="Seg_1593" s="T135">brother.[NOM]-3SG</ta>
            <ta e="T137" id="Seg_1594" s="T136">brother.[NOM]-3SG</ta>
            <ta e="T138" id="Seg_1595" s="T137">be-INFER.[3SG.S]</ta>
            <ta e="T139" id="Seg_1596" s="T138">accordion-EP-VBLZ-HAB-PST.[3SG.S]</ta>
            <ta e="T140" id="Seg_1597" s="T139">all.the.time</ta>
            <ta e="T141" id="Seg_1598" s="T140">this-day.[NOM]</ta>
            <ta e="T142" id="Seg_1599" s="T141">I.NOM</ta>
            <ta e="T143" id="Seg_1600" s="T142">dream-VBLZ-DUR-CVB</ta>
            <ta e="T144" id="Seg_1601" s="T143">excrements-EP-GEN</ta>
            <ta e="T145" id="Seg_1602" s="T144">see-DRV-PST-1SG.O</ta>
            <ta e="T146" id="Seg_1603" s="T145">cat.[NOM]</ta>
            <ta e="T147" id="Seg_1604" s="T146">defecate-PST.NAR.[3SG.S]</ta>
            <ta e="T148" id="Seg_1605" s="T147">wipe-IMP.2SG.S</ta>
            <ta e="T149" id="Seg_1606" s="T148">you.SG.GEN</ta>
            <ta e="T150" id="Seg_1607" s="T149">cat.[NOM]-2SG</ta>
            <ta e="T151" id="Seg_1608" s="T150">this-day.[NOM]</ta>
            <ta e="T152" id="Seg_1609" s="T151">I.NOM</ta>
            <ta e="T153" id="Seg_1610" s="T152">dream-VBLZ-ACTN-ACC</ta>
            <ta e="T154" id="Seg_1611" s="T153">see-DRV-PST-1SG.S</ta>
            <ta e="T155" id="Seg_1612" s="T154">hill-GEN-%%-ILL</ta>
            <ta e="T156" id="Seg_1613" s="T155">climb.down-CO-1SG.S</ta>
            <ta e="T157" id="Seg_1614" s="T156">I.ALL</ta>
            <ta e="T158" id="Seg_1615" s="T157">good</ta>
            <ta e="T159" id="Seg_1616" s="T158">be-HAB-3SG.S</ta>
            <ta e="T160" id="Seg_1617" s="T159">now</ta>
            <ta e="T161" id="Seg_1618" s="T160">wait-TR-1SG.S</ta>
            <ta e="T162" id="Seg_1619" s="T161">what.[NOM]</ta>
            <ta e="T163" id="Seg_1620" s="T162">be-POT.FUT.3SG</ta>
            <ta e="T164" id="Seg_1621" s="T163">NEG</ta>
            <ta e="T165" id="Seg_1622" s="T164">know-1SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1623" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_1624" s="T2">этот-день.[NOM]</ta>
            <ta e="T4" id="Seg_1625" s="T3">сон-VBLZ-DUR-PST-1SG.S</ta>
            <ta e="T5" id="Seg_1626" s="T4">сон.[NOM]</ta>
            <ta e="T6" id="Seg_1627" s="T5">старый</ta>
            <ta e="T7" id="Seg_1628" s="T6">деревня-ACC</ta>
            <ta e="T8" id="Seg_1629" s="T7">увидеть-DRV-PST-1SG.O</ta>
            <ta e="T9" id="Seg_1630" s="T8">дядя.[NOM]</ta>
            <ta e="T10" id="Seg_1631" s="T9">Кон-EP-ACC</ta>
            <ta e="T11" id="Seg_1632" s="T10">увидеть-DRV-PST-1SG.O</ta>
            <ta e="T12" id="Seg_1633" s="T11">и</ta>
            <ta e="T13" id="Seg_1634" s="T12">тетка.[NOM]</ta>
            <ta e="T14" id="Seg_1635" s="T13">Прасковья.[NOM]</ta>
            <ta e="T15" id="Seg_1636" s="T14">увидеть-DRV-PST-1SG.O</ta>
            <ta e="T16" id="Seg_1637" s="T15">сон-VBLZ-DUR-CVB</ta>
            <ta e="T17" id="Seg_1638" s="T16">я.NOM</ta>
            <ta e="T18" id="Seg_1639" s="T17">увидеть-DRV-PST-1SG.O</ta>
            <ta e="T19" id="Seg_1640" s="T18">корова.[NOM]</ta>
            <ta e="T20" id="Seg_1641" s="T19">самка-GEN</ta>
            <ta e="T21" id="Seg_1642" s="T20">хотеть-PST-1SG.O</ta>
            <ta e="T22" id="Seg_1643" s="T21">подоить-RES-INF</ta>
            <ta e="T23" id="Seg_1644" s="T22">а</ta>
            <ta e="T24" id="Seg_1645" s="T23">он(а).[NOM]</ta>
            <ta e="T25" id="Seg_1646" s="T24">я.ALL</ta>
            <ta e="T26" id="Seg_1647" s="T25">NEG</ta>
            <ta e="T27" id="Seg_1648" s="T26">сделать-CO.[3SG.S]</ta>
            <ta e="T28" id="Seg_1649" s="T27">я.NOM</ta>
            <ta e="T29" id="Seg_1650" s="T28">этот.[NOM]</ta>
            <ta e="T30" id="Seg_1651" s="T29">увидеть-DRV-HAB-INFER-1SG.O</ta>
            <ta e="T31" id="Seg_1652" s="T30">что-INDEF2.[NOM]</ta>
            <ta e="T32" id="Seg_1653" s="T31">человек.[NOM]</ta>
            <ta e="T33" id="Seg_1654" s="T32">умереть-POT.FUT.3SG</ta>
            <ta e="T34" id="Seg_1655" s="T33">я.NOM</ta>
            <ta e="T35" id="Seg_1656" s="T34">разбудить-RFL-1SG.S</ta>
            <ta e="T36" id="Seg_1657" s="T35">и</ta>
            <ta e="T37" id="Seg_1658" s="T36">плакать-CVB</ta>
            <ta e="T38" id="Seg_1659" s="T37">начать-DRV-1SG.S</ta>
            <ta e="T39" id="Seg_1660" s="T38">я.NOM</ta>
            <ta e="T40" id="Seg_1661" s="T39">сказать-1SG.S</ta>
            <ta e="T41" id="Seg_1662" s="T40">что-INDEF2.[NOM]</ta>
            <ta e="T42" id="Seg_1663" s="T41">умереть-FUT-3SG.S</ta>
            <ta e="T43" id="Seg_1664" s="T42">и</ta>
            <ta e="T44" id="Seg_1665" s="T43">разбудить-RFL-1SG.S</ta>
            <ta e="T45" id="Seg_1666" s="T44">этот</ta>
            <ta e="T46" id="Seg_1667" s="T45">сон-VBLZ-ACTN-ACC</ta>
            <ta e="T47" id="Seg_1668" s="T46">весь</ta>
            <ta e="T48" id="Seg_1669" s="T47">сказать-CO-1SG.O</ta>
            <ta e="T49" id="Seg_1670" s="T48">опять</ta>
            <ta e="T50" id="Seg_1671" s="T49">спать-INCH-PST.NAR-1SG.S</ta>
            <ta e="T51" id="Seg_1672" s="T50">спать-INFER-1SG.S</ta>
            <ta e="T52" id="Seg_1673" s="T51">я.NOM</ta>
            <ta e="T53" id="Seg_1674" s="T52">ежели</ta>
            <ta e="T54" id="Seg_1675" s="T53">увидеть-DRV-CO-1SG.O</ta>
            <ta e="T55" id="Seg_1676" s="T54">старый</ta>
            <ta e="T56" id="Seg_1677" s="T55">деревня-ACC</ta>
            <ta e="T57" id="Seg_1678" s="T56">что-INDEF2</ta>
            <ta e="T58" id="Seg_1679" s="T57">человек.[NOM]</ta>
            <ta e="T59" id="Seg_1680" s="T58">умереть-POT.FUT.3SG</ta>
            <ta e="T60" id="Seg_1681" s="T59">опять</ta>
            <ta e="T61" id="Seg_1682" s="T60">сон-VBLZ-1SG.S</ta>
            <ta e="T62" id="Seg_1683" s="T61">дом-LOC</ta>
            <ta e="T63" id="Seg_1684" s="T62">сам.1SG</ta>
            <ta e="T64" id="Seg_1685" s="T63">сидеть-1SG.S</ta>
            <ta e="T65" id="Seg_1686" s="T64">что-INDEF.[NOM]</ta>
            <ta e="T66" id="Seg_1687" s="T65">стучать-TR-CVB</ta>
            <ta e="T67" id="Seg_1688" s="T66">начать-FRQ-EP-3SG.S</ta>
            <ta e="T68" id="Seg_1689" s="T67">я.NOM</ta>
            <ta e="T69" id="Seg_1690" s="T68">бегать-MOM-CO-1SG.S</ta>
            <ta e="T70" id="Seg_1691" s="T69">кто.[NOM]</ta>
            <ta e="T71" id="Seg_1692" s="T70">туда-ADV.LOC</ta>
            <ta e="T72" id="Seg_1693" s="T71">а</ta>
            <ta e="T73" id="Seg_1694" s="T72">он(а).[NOM]</ta>
            <ta e="T74" id="Seg_1695" s="T73">я.NOM</ta>
            <ta e="T75" id="Seg_1696" s="T74">я.NOM</ta>
            <ta e="T76" id="Seg_1697" s="T75">я.NOM</ta>
            <ta e="T78" id="Seg_1698" s="T77">я.NOM</ta>
            <ta e="T79" id="Seg_1699" s="T78">NEG</ta>
            <ta e="T80" id="Seg_1700" s="T79">пустить-HAB.[3SG.S]</ta>
            <ta e="T81" id="Seg_1701" s="T80">а</ta>
            <ta e="T82" id="Seg_1702" s="T81">он(а).[NOM]</ta>
            <ta e="T83" id="Seg_1703" s="T82">наверх</ta>
            <ta e="T84" id="Seg_1704" s="T83">залезть-CO-3SG.S</ta>
            <ta e="T85" id="Seg_1705" s="T84">дверь-GEN</ta>
            <ta e="T86" id="Seg_1706" s="T85">верхушка-LOC</ta>
            <ta e="T87" id="Seg_1707" s="T86">окно-DIM.[3SG.S]</ta>
            <ta e="T88" id="Seg_1708" s="T87">он(а).[NOM]</ta>
            <ta e="T89" id="Seg_1709" s="T88">этот</ta>
            <ta e="T90" id="Seg_1710" s="T89">окно-PROL</ta>
            <ta e="T91" id="Seg_1711" s="T90">оттолкнуть-RFL-3SG.S</ta>
            <ta e="T92" id="Seg_1712" s="T91">я.GEN</ta>
            <ta e="T93" id="Seg_1713" s="T92">верхняя.часть-EP-LOC</ta>
            <ta e="T94" id="Seg_1714" s="T93">упасть-CO-3SG.S</ta>
            <ta e="T95" id="Seg_1715" s="T94">я.NOM</ta>
            <ta e="T96" id="Seg_1716" s="T95">до.того</ta>
            <ta e="T97" id="Seg_1717" s="T96">пугать-DETR-CO-1SG.S</ta>
            <ta e="T98" id="Seg_1718" s="T97">я.NOM</ta>
            <ta e="T99" id="Seg_1719" s="T98">посмотреть-DRV-INFER-1SG.S</ta>
            <ta e="T100" id="Seg_1720" s="T99">а</ta>
            <ta e="T101" id="Seg_1721" s="T100">Володька.[NOM]</ta>
            <ta e="T102" id="Seg_1722" s="T101">лежать.[3SG.S]</ta>
            <ta e="T103" id="Seg_1723" s="T102">и</ta>
            <ta e="T104" id="Seg_1724" s="T103">засмеяться-DUR.[3SG.S]</ta>
            <ta e="T105" id="Seg_1725" s="T104">я.NOM</ta>
            <ta e="T106" id="Seg_1726" s="T105">сказать-1SG.S</ta>
            <ta e="T107" id="Seg_1727" s="T106">что-TRL</ta>
            <ta e="T108" id="Seg_1728" s="T107">я.ACC</ta>
            <ta e="T109" id="Seg_1729" s="T108">пугать-PFV-2SG.S</ta>
            <ta e="T110" id="Seg_1730" s="T109">и</ta>
            <ta e="T111" id="Seg_1731" s="T110">разбудить-RFL-1SG.S</ta>
            <ta e="T112" id="Seg_1732" s="T111">я.NOM</ta>
            <ta e="T113" id="Seg_1733" s="T112">этот-день.[NOM]</ta>
            <ta e="T114" id="Seg_1734" s="T113">Филат-EP-ACC</ta>
            <ta e="T115" id="Seg_1735" s="T114">увидеть-DRV-PST-1SG.S</ta>
            <ta e="T116" id="Seg_1736" s="T115">сон-VBLZ-DUR-CVB</ta>
            <ta e="T117" id="Seg_1737" s="T116">голый</ta>
            <ta e="T118" id="Seg_1738" s="T117">нога-EP-ADVZ</ta>
            <ta e="T119" id="Seg_1739" s="T118">сидеть-PST.[3SG.S]</ta>
            <ta e="T120" id="Seg_1740" s="T119">я.ALL</ta>
            <ta e="T121" id="Seg_1741" s="T120">сказать-3SG.S</ta>
            <ta e="T122" id="Seg_1742" s="T121">я.NOM</ta>
            <ta e="T123" id="Seg_1743" s="T122">нога-PL.[NOM]-1SG</ta>
            <ta e="T124" id="Seg_1744" s="T123">замерзнуть-PST.NAR-3PL</ta>
            <ta e="T125" id="Seg_1745" s="T124">я.NOM</ta>
            <ta e="T126" id="Seg_1746" s="T125">он(а)-EP-GEN</ta>
            <ta e="T127" id="Seg_1747" s="T126">нога-EP-ACC</ta>
            <ta e="T128" id="Seg_1748" s="T127">взять-CO-1SG.O</ta>
            <ta e="T129" id="Seg_1749" s="T128">он(а)-ADES</ta>
            <ta e="T130" id="Seg_1750" s="T129">замерзнуть-RES-3PL</ta>
            <ta e="T131" id="Seg_1751" s="T130">сын.[NOM]-3SG</ta>
            <ta e="T132" id="Seg_1752" s="T131">NEG.EX.[3SG]</ta>
            <ta e="T133" id="Seg_1753" s="T132">кто-ALL</ta>
            <ta e="T134" id="Seg_1754" s="T133">нужно</ta>
            <ta e="T135" id="Seg_1755" s="T134">он(а)-ADES</ta>
            <ta e="T136" id="Seg_1756" s="T135">брат.[NOM]-3SG</ta>
            <ta e="T137" id="Seg_1757" s="T136">брат.[NOM]-3SG</ta>
            <ta e="T138" id="Seg_1758" s="T137">быть-INFER.[3SG.S]</ta>
            <ta e="T139" id="Seg_1759" s="T138">гармонь-EP-VBLZ-HAB-PST.[3SG.S]</ta>
            <ta e="T140" id="Seg_1760" s="T139">все.время</ta>
            <ta e="T141" id="Seg_1761" s="T140">этот-день.[NOM]</ta>
            <ta e="T142" id="Seg_1762" s="T141">я.NOM</ta>
            <ta e="T143" id="Seg_1763" s="T142">сон-VBLZ-DUR-CVB</ta>
            <ta e="T144" id="Seg_1764" s="T143">кал-EP-GEN</ta>
            <ta e="T145" id="Seg_1765" s="T144">увидеть-DRV-PST-1SG.O</ta>
            <ta e="T146" id="Seg_1766" s="T145">кошка.[NOM]</ta>
            <ta e="T147" id="Seg_1767" s="T146">испражниться-PST.NAR.[3SG.S]</ta>
            <ta e="T148" id="Seg_1768" s="T147">вытереть-IMP.2SG.S</ta>
            <ta e="T149" id="Seg_1769" s="T148">ты.GEN</ta>
            <ta e="T150" id="Seg_1770" s="T149">кошка.[NOM]-2SG</ta>
            <ta e="T151" id="Seg_1771" s="T150">этот-день.[NOM]</ta>
            <ta e="T152" id="Seg_1772" s="T151">я.NOM</ta>
            <ta e="T153" id="Seg_1773" s="T152">сон-VBLZ-ACTN-ACC</ta>
            <ta e="T154" id="Seg_1774" s="T153">увидеть-DRV-PST-1SG.S</ta>
            <ta e="T155" id="Seg_1775" s="T154">гора-GEN-%%-ILL</ta>
            <ta e="T156" id="Seg_1776" s="T155">слезть-CO-1SG.S</ta>
            <ta e="T157" id="Seg_1777" s="T156">я.ALL</ta>
            <ta e="T158" id="Seg_1778" s="T157">хорошо</ta>
            <ta e="T159" id="Seg_1779" s="T158">быть-HAB-3SG.S</ta>
            <ta e="T160" id="Seg_1780" s="T159">теперь</ta>
            <ta e="T161" id="Seg_1781" s="T160">ждать-TR-1SG.S</ta>
            <ta e="T162" id="Seg_1782" s="T161">что.[NOM]</ta>
            <ta e="T163" id="Seg_1783" s="T162">быть-POT.FUT.3SG</ta>
            <ta e="T164" id="Seg_1784" s="T163">NEG</ta>
            <ta e="T165" id="Seg_1785" s="T164">знать-1SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1786" s="T1">pers</ta>
            <ta e="T3" id="Seg_1787" s="T2">dem-n.[n:case]</ta>
            <ta e="T4" id="Seg_1788" s="T3">n-n&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_1789" s="T4">n.[n:case]</ta>
            <ta e="T6" id="Seg_1790" s="T5">adj</ta>
            <ta e="T7" id="Seg_1791" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_1792" s="T7">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_1793" s="T8">n.[n:case]</ta>
            <ta e="T10" id="Seg_1794" s="T9">nprop-n:ins-n:case</ta>
            <ta e="T11" id="Seg_1795" s="T10">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_1796" s="T11">conj</ta>
            <ta e="T13" id="Seg_1797" s="T12">n.[n:case]</ta>
            <ta e="T14" id="Seg_1798" s="T13">nprop.[n:case]</ta>
            <ta e="T15" id="Seg_1799" s="T14">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_1800" s="T15">n-n&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T17" id="Seg_1801" s="T16">pers</ta>
            <ta e="T18" id="Seg_1802" s="T17">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_1803" s="T18">n.[n:case]</ta>
            <ta e="T20" id="Seg_1804" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_1805" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_1806" s="T21">v-v&gt;v-v:inf</ta>
            <ta e="T23" id="Seg_1807" s="T22">conj</ta>
            <ta e="T24" id="Seg_1808" s="T23">pers.[n:case]</ta>
            <ta e="T25" id="Seg_1809" s="T24">pers</ta>
            <ta e="T26" id="Seg_1810" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_1811" s="T26">v-v:ins.[v:pn]</ta>
            <ta e="T28" id="Seg_1812" s="T27">pers</ta>
            <ta e="T29" id="Seg_1813" s="T28">dem.[n:case]</ta>
            <ta e="T30" id="Seg_1814" s="T29">v-v&gt;v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T31" id="Seg_1815" s="T30">interrog-clit.[n:case]</ta>
            <ta e="T32" id="Seg_1816" s="T31">n.[n:case]</ta>
            <ta e="T33" id="Seg_1817" s="T32">v-v:tense</ta>
            <ta e="T34" id="Seg_1818" s="T33">pers</ta>
            <ta e="T35" id="Seg_1819" s="T34">v-v&gt;v-v:pn</ta>
            <ta e="T36" id="Seg_1820" s="T35">conj</ta>
            <ta e="T37" id="Seg_1821" s="T36">v-v&gt;adv</ta>
            <ta e="T38" id="Seg_1822" s="T37">v-v&gt;v-v:pn</ta>
            <ta e="T39" id="Seg_1823" s="T38">pers</ta>
            <ta e="T40" id="Seg_1824" s="T39">v-v:pn</ta>
            <ta e="T41" id="Seg_1825" s="T40">interrog-clit.[n:case]</ta>
            <ta e="T42" id="Seg_1826" s="T41">v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_1827" s="T42">conj</ta>
            <ta e="T44" id="Seg_1828" s="T43">v-v&gt;v-v:pn</ta>
            <ta e="T45" id="Seg_1829" s="T44">dem</ta>
            <ta e="T46" id="Seg_1830" s="T45">n-n&gt;v-v&gt;n-n:case</ta>
            <ta e="T47" id="Seg_1831" s="T46">quant</ta>
            <ta e="T48" id="Seg_1832" s="T47">v-v:ins-v:pn</ta>
            <ta e="T49" id="Seg_1833" s="T48">adv</ta>
            <ta e="T50" id="Seg_1834" s="T49">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_1835" s="T50">v-v:mood-v:pn</ta>
            <ta e="T52" id="Seg_1836" s="T51">pers</ta>
            <ta e="T53" id="Seg_1837" s="T52">conj</ta>
            <ta e="T54" id="Seg_1838" s="T53">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T55" id="Seg_1839" s="T54">adj</ta>
            <ta e="T56" id="Seg_1840" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_1841" s="T56">interrog-clit</ta>
            <ta e="T58" id="Seg_1842" s="T57">n.[n:case]</ta>
            <ta e="T59" id="Seg_1843" s="T58">v-v:tense</ta>
            <ta e="T60" id="Seg_1844" s="T59">adv</ta>
            <ta e="T61" id="Seg_1845" s="T60">n-n&gt;v-v:pn</ta>
            <ta e="T62" id="Seg_1846" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_1847" s="T62">emphpro</ta>
            <ta e="T64" id="Seg_1848" s="T63">v-v:pn</ta>
            <ta e="T65" id="Seg_1849" s="T64">interrog-clit.[n:case]</ta>
            <ta e="T66" id="Seg_1850" s="T65">v-v&gt;v-v&gt;adv</ta>
            <ta e="T67" id="Seg_1851" s="T66">v-v&gt;v-n:ins-v:pn</ta>
            <ta e="T68" id="Seg_1852" s="T67">pers</ta>
            <ta e="T69" id="Seg_1853" s="T68">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T70" id="Seg_1854" s="T69">interrog.[n:case]</ta>
            <ta e="T71" id="Seg_1855" s="T70">adv-adv:case</ta>
            <ta e="T72" id="Seg_1856" s="T71">conj</ta>
            <ta e="T73" id="Seg_1857" s="T72">pers.[n:case]</ta>
            <ta e="T74" id="Seg_1858" s="T73">pers</ta>
            <ta e="T75" id="Seg_1859" s="T74">pers</ta>
            <ta e="T76" id="Seg_1860" s="T75">pers</ta>
            <ta e="T78" id="Seg_1861" s="T77">pers</ta>
            <ta e="T79" id="Seg_1862" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_1863" s="T79">v-v&gt;v.[v:pn]</ta>
            <ta e="T81" id="Seg_1864" s="T80">conj</ta>
            <ta e="T82" id="Seg_1865" s="T81">pers.[n:case]</ta>
            <ta e="T83" id="Seg_1866" s="T82">adv</ta>
            <ta e="T84" id="Seg_1867" s="T83">v-v:ins-v:pn</ta>
            <ta e="T85" id="Seg_1868" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_1869" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_1870" s="T86">n-n&gt;n.[v:pn]</ta>
            <ta e="T88" id="Seg_1871" s="T87">pers.[n:case]</ta>
            <ta e="T89" id="Seg_1872" s="T88">dem</ta>
            <ta e="T90" id="Seg_1873" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_1874" s="T90">v-v&gt;v-v:pn</ta>
            <ta e="T92" id="Seg_1875" s="T91">pers</ta>
            <ta e="T93" id="Seg_1876" s="T92">n-n:ins-n:case</ta>
            <ta e="T94" id="Seg_1877" s="T93">v-v:ins-v:pn</ta>
            <ta e="T95" id="Seg_1878" s="T94">pers</ta>
            <ta e="T96" id="Seg_1879" s="T95">adv</ta>
            <ta e="T97" id="Seg_1880" s="T96">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T98" id="Seg_1881" s="T97">pers</ta>
            <ta e="T99" id="Seg_1882" s="T98">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T100" id="Seg_1883" s="T99">conj</ta>
            <ta e="T101" id="Seg_1884" s="T100">nprop.[n:case]</ta>
            <ta e="T102" id="Seg_1885" s="T101">v.[v:pn]</ta>
            <ta e="T103" id="Seg_1886" s="T102">conj</ta>
            <ta e="T104" id="Seg_1887" s="T103">v-v&gt;v.[v:pn]</ta>
            <ta e="T105" id="Seg_1888" s="T104">pers</ta>
            <ta e="T106" id="Seg_1889" s="T105">v-v:pn</ta>
            <ta e="T107" id="Seg_1890" s="T106">interrog-n:case</ta>
            <ta e="T108" id="Seg_1891" s="T107">pers</ta>
            <ta e="T109" id="Seg_1892" s="T108">v-v&gt;v-v:pn</ta>
            <ta e="T110" id="Seg_1893" s="T109">conj</ta>
            <ta e="T111" id="Seg_1894" s="T110">v-v&gt;v-v:pn</ta>
            <ta e="T112" id="Seg_1895" s="T111">pers</ta>
            <ta e="T113" id="Seg_1896" s="T112">dem-n.[n:case]</ta>
            <ta e="T114" id="Seg_1897" s="T113">nprop-n:ins-n:case</ta>
            <ta e="T115" id="Seg_1898" s="T114">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T116" id="Seg_1899" s="T115">n-n&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T117" id="Seg_1900" s="T116">adj</ta>
            <ta e="T118" id="Seg_1901" s="T117">n-n:ins-n&gt;adv</ta>
            <ta e="T119" id="Seg_1902" s="T118">v-v:tense.[v:pn]</ta>
            <ta e="T120" id="Seg_1903" s="T119">pers</ta>
            <ta e="T121" id="Seg_1904" s="T120">v-v:pn</ta>
            <ta e="T122" id="Seg_1905" s="T121">pers</ta>
            <ta e="T123" id="Seg_1906" s="T122">n-n:num.[n:case]-n:poss</ta>
            <ta e="T124" id="Seg_1907" s="T123">v-v:tense-v:pn</ta>
            <ta e="T125" id="Seg_1908" s="T124">pers</ta>
            <ta e="T126" id="Seg_1909" s="T125">pers-n:ins-n:case</ta>
            <ta e="T127" id="Seg_1910" s="T126">n-n:ins-n:case</ta>
            <ta e="T128" id="Seg_1911" s="T127">v-v:ins-v:pn</ta>
            <ta e="T129" id="Seg_1912" s="T128">pers-n:case</ta>
            <ta e="T130" id="Seg_1913" s="T129">v-v&gt;v-v:pn</ta>
            <ta e="T131" id="Seg_1914" s="T130">n.[n:case]-n:poss</ta>
            <ta e="T132" id="Seg_1915" s="T131">v.[v:pn]</ta>
            <ta e="T133" id="Seg_1916" s="T132">interrog-n:case</ta>
            <ta e="T134" id="Seg_1917" s="T133">v</ta>
            <ta e="T135" id="Seg_1918" s="T134">pers-n:case</ta>
            <ta e="T136" id="Seg_1919" s="T135">n.[n:case]-n:poss</ta>
            <ta e="T137" id="Seg_1920" s="T136">n.[n:case]-n:poss</ta>
            <ta e="T138" id="Seg_1921" s="T137">v-v:mood.[v:pn]</ta>
            <ta e="T139" id="Seg_1922" s="T138">n-n:ins-n&gt;v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T140" id="Seg_1923" s="T139">adv</ta>
            <ta e="T141" id="Seg_1924" s="T140">dem-n.[n:case]</ta>
            <ta e="T142" id="Seg_1925" s="T141">pers</ta>
            <ta e="T143" id="Seg_1926" s="T142">n-n&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T144" id="Seg_1927" s="T143">n-n:ins-n:case</ta>
            <ta e="T145" id="Seg_1928" s="T144">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T146" id="Seg_1929" s="T145">n.[n:case]</ta>
            <ta e="T147" id="Seg_1930" s="T146">v-v:tense.[v:pn]</ta>
            <ta e="T148" id="Seg_1931" s="T147">v-v:mood.pn</ta>
            <ta e="T149" id="Seg_1932" s="T148">pers</ta>
            <ta e="T150" id="Seg_1933" s="T149">n.[n:case]-n:poss</ta>
            <ta e="T151" id="Seg_1934" s="T150">dem-n.[n:case]</ta>
            <ta e="T152" id="Seg_1935" s="T151">pers</ta>
            <ta e="T153" id="Seg_1936" s="T152">n-n&gt;v-v&gt;n-n:case</ta>
            <ta e="T154" id="Seg_1937" s="T153">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T155" id="Seg_1938" s="T154">n-n:case-%%-n:case</ta>
            <ta e="T156" id="Seg_1939" s="T155">v-v:ins-v:pn</ta>
            <ta e="T157" id="Seg_1940" s="T156">pers</ta>
            <ta e="T158" id="Seg_1941" s="T157">adv</ta>
            <ta e="T159" id="Seg_1942" s="T158">v-v&gt;v-v:pn</ta>
            <ta e="T160" id="Seg_1943" s="T159">adv</ta>
            <ta e="T161" id="Seg_1944" s="T160">v-v&gt;v-v:pn</ta>
            <ta e="T162" id="Seg_1945" s="T161">interrog.[n:case]</ta>
            <ta e="T163" id="Seg_1946" s="T162">v-v:tense</ta>
            <ta e="T164" id="Seg_1947" s="T163">ptcl</ta>
            <ta e="T165" id="Seg_1948" s="T164">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1949" s="T1">pers</ta>
            <ta e="T3" id="Seg_1950" s="T2">adv</ta>
            <ta e="T4" id="Seg_1951" s="T3">v</ta>
            <ta e="T5" id="Seg_1952" s="T4">n</ta>
            <ta e="T6" id="Seg_1953" s="T5">adj</ta>
            <ta e="T7" id="Seg_1954" s="T6">n</ta>
            <ta e="T8" id="Seg_1955" s="T7">v</ta>
            <ta e="T9" id="Seg_1956" s="T8">n</ta>
            <ta e="T10" id="Seg_1957" s="T9">nprop</ta>
            <ta e="T11" id="Seg_1958" s="T10">v</ta>
            <ta e="T12" id="Seg_1959" s="T11">conj</ta>
            <ta e="T13" id="Seg_1960" s="T12">n</ta>
            <ta e="T14" id="Seg_1961" s="T13">nprop</ta>
            <ta e="T15" id="Seg_1962" s="T14">v</ta>
            <ta e="T16" id="Seg_1963" s="T15">adv</ta>
            <ta e="T17" id="Seg_1964" s="T16">pers</ta>
            <ta e="T18" id="Seg_1965" s="T17">v</ta>
            <ta e="T19" id="Seg_1966" s="T18">n</ta>
            <ta e="T20" id="Seg_1967" s="T19">n</ta>
            <ta e="T21" id="Seg_1968" s="T20">v</ta>
            <ta e="T22" id="Seg_1969" s="T21">v</ta>
            <ta e="T23" id="Seg_1970" s="T22">conj</ta>
            <ta e="T24" id="Seg_1971" s="T23">pers</ta>
            <ta e="T25" id="Seg_1972" s="T24">pers</ta>
            <ta e="T26" id="Seg_1973" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_1974" s="T26">v</ta>
            <ta e="T28" id="Seg_1975" s="T27">pers</ta>
            <ta e="T29" id="Seg_1976" s="T28">dem</ta>
            <ta e="T30" id="Seg_1977" s="T29">v</ta>
            <ta e="T31" id="Seg_1978" s="T30">pro</ta>
            <ta e="T32" id="Seg_1979" s="T31">n</ta>
            <ta e="T33" id="Seg_1980" s="T32">v</ta>
            <ta e="T34" id="Seg_1981" s="T33">pers</ta>
            <ta e="T35" id="Seg_1982" s="T34">v</ta>
            <ta e="T36" id="Seg_1983" s="T35">conj</ta>
            <ta e="T37" id="Seg_1984" s="T36">adv</ta>
            <ta e="T38" id="Seg_1985" s="T37">v</ta>
            <ta e="T39" id="Seg_1986" s="T38">pers</ta>
            <ta e="T40" id="Seg_1987" s="T39">v</ta>
            <ta e="T41" id="Seg_1988" s="T40">pro</ta>
            <ta e="T42" id="Seg_1989" s="T41">n</ta>
            <ta e="T43" id="Seg_1990" s="T42">conj</ta>
            <ta e="T44" id="Seg_1991" s="T43">v</ta>
            <ta e="T45" id="Seg_1992" s="T44">dem</ta>
            <ta e="T46" id="Seg_1993" s="T45">n</ta>
            <ta e="T47" id="Seg_1994" s="T46">quant</ta>
            <ta e="T48" id="Seg_1995" s="T47">v</ta>
            <ta e="T49" id="Seg_1996" s="T48">adv</ta>
            <ta e="T50" id="Seg_1997" s="T49">v</ta>
            <ta e="T51" id="Seg_1998" s="T50">v</ta>
            <ta e="T52" id="Seg_1999" s="T51">pers</ta>
            <ta e="T53" id="Seg_2000" s="T52">conj</ta>
            <ta e="T54" id="Seg_2001" s="T53">v</ta>
            <ta e="T55" id="Seg_2002" s="T54">adj</ta>
            <ta e="T56" id="Seg_2003" s="T55">n</ta>
            <ta e="T57" id="Seg_2004" s="T56">pro</ta>
            <ta e="T58" id="Seg_2005" s="T57">n</ta>
            <ta e="T59" id="Seg_2006" s="T58">v</ta>
            <ta e="T60" id="Seg_2007" s="T59">adv</ta>
            <ta e="T61" id="Seg_2008" s="T60">v</ta>
            <ta e="T62" id="Seg_2009" s="T61">n</ta>
            <ta e="T63" id="Seg_2010" s="T62">emphpro</ta>
            <ta e="T64" id="Seg_2011" s="T63">v</ta>
            <ta e="T65" id="Seg_2012" s="T64">pro</ta>
            <ta e="T66" id="Seg_2013" s="T65">adv</ta>
            <ta e="T67" id="Seg_2014" s="T66">v</ta>
            <ta e="T68" id="Seg_2015" s="T67">pers</ta>
            <ta e="T69" id="Seg_2016" s="T68">v</ta>
            <ta e="T70" id="Seg_2017" s="T69">interrog</ta>
            <ta e="T71" id="Seg_2018" s="T70">adv</ta>
            <ta e="T72" id="Seg_2019" s="T71">conj</ta>
            <ta e="T73" id="Seg_2020" s="T72">pers</ta>
            <ta e="T74" id="Seg_2021" s="T73">pers</ta>
            <ta e="T75" id="Seg_2022" s="T74">pers</ta>
            <ta e="T76" id="Seg_2023" s="T75">pers</ta>
            <ta e="T78" id="Seg_2024" s="T77">pers</ta>
            <ta e="T79" id="Seg_2025" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_2026" s="T79">v</ta>
            <ta e="T81" id="Seg_2027" s="T80">conj</ta>
            <ta e="T82" id="Seg_2028" s="T81">pers</ta>
            <ta e="T83" id="Seg_2029" s="T82">adv</ta>
            <ta e="T84" id="Seg_2030" s="T83">v</ta>
            <ta e="T85" id="Seg_2031" s="T84">n</ta>
            <ta e="T86" id="Seg_2032" s="T85">n</ta>
            <ta e="T87" id="Seg_2033" s="T86">n</ta>
            <ta e="T88" id="Seg_2034" s="T87">pers</ta>
            <ta e="T89" id="Seg_2035" s="T88">dem</ta>
            <ta e="T90" id="Seg_2036" s="T89">n</ta>
            <ta e="T91" id="Seg_2037" s="T90">v</ta>
            <ta e="T92" id="Seg_2038" s="T91">pers</ta>
            <ta e="T93" id="Seg_2039" s="T92">n</ta>
            <ta e="T94" id="Seg_2040" s="T93">v</ta>
            <ta e="T95" id="Seg_2041" s="T94">pers</ta>
            <ta e="T96" id="Seg_2042" s="T95">adv</ta>
            <ta e="T97" id="Seg_2043" s="T96">v</ta>
            <ta e="T98" id="Seg_2044" s="T97">pers</ta>
            <ta e="T99" id="Seg_2045" s="T98">v</ta>
            <ta e="T100" id="Seg_2046" s="T99">conj</ta>
            <ta e="T101" id="Seg_2047" s="T100">nprop</ta>
            <ta e="T102" id="Seg_2048" s="T101">v</ta>
            <ta e="T103" id="Seg_2049" s="T102">conj</ta>
            <ta e="T104" id="Seg_2050" s="T103">v</ta>
            <ta e="T105" id="Seg_2051" s="T104">pers</ta>
            <ta e="T106" id="Seg_2052" s="T105">v</ta>
            <ta e="T107" id="Seg_2053" s="T106">interrog</ta>
            <ta e="T108" id="Seg_2054" s="T107">pers</ta>
            <ta e="T109" id="Seg_2055" s="T108">v</ta>
            <ta e="T110" id="Seg_2056" s="T109">conj</ta>
            <ta e="T111" id="Seg_2057" s="T110">v</ta>
            <ta e="T112" id="Seg_2058" s="T111">pers</ta>
            <ta e="T113" id="Seg_2059" s="T112">adv</ta>
            <ta e="T114" id="Seg_2060" s="T113">nprop</ta>
            <ta e="T115" id="Seg_2061" s="T114">v</ta>
            <ta e="T116" id="Seg_2062" s="T115">v</ta>
            <ta e="T117" id="Seg_2063" s="T116">adj</ta>
            <ta e="T118" id="Seg_2064" s="T117">adv</ta>
            <ta e="T119" id="Seg_2065" s="T118">v</ta>
            <ta e="T120" id="Seg_2066" s="T119">pers</ta>
            <ta e="T121" id="Seg_2067" s="T120">v</ta>
            <ta e="T122" id="Seg_2068" s="T121">pers</ta>
            <ta e="T123" id="Seg_2069" s="T122">n</ta>
            <ta e="T124" id="Seg_2070" s="T123">v</ta>
            <ta e="T125" id="Seg_2071" s="T124">pers</ta>
            <ta e="T126" id="Seg_2072" s="T125">pers</ta>
            <ta e="T127" id="Seg_2073" s="T126">n</ta>
            <ta e="T128" id="Seg_2074" s="T127">v</ta>
            <ta e="T129" id="Seg_2075" s="T128">pers</ta>
            <ta e="T130" id="Seg_2076" s="T129">v</ta>
            <ta e="T131" id="Seg_2077" s="T130">n</ta>
            <ta e="T132" id="Seg_2078" s="T131">v</ta>
            <ta e="T133" id="Seg_2079" s="T132">interrog</ta>
            <ta e="T134" id="Seg_2080" s="T133">v</ta>
            <ta e="T135" id="Seg_2081" s="T134">pers</ta>
            <ta e="T136" id="Seg_2082" s="T135">n</ta>
            <ta e="T137" id="Seg_2083" s="T136">n</ta>
            <ta e="T138" id="Seg_2084" s="T137">v</ta>
            <ta e="T139" id="Seg_2085" s="T138">v</ta>
            <ta e="T140" id="Seg_2086" s="T139">adv</ta>
            <ta e="T141" id="Seg_2087" s="T140">adv</ta>
            <ta e="T142" id="Seg_2088" s="T141">pers</ta>
            <ta e="T143" id="Seg_2089" s="T142">adv</ta>
            <ta e="T144" id="Seg_2090" s="T143">n</ta>
            <ta e="T145" id="Seg_2091" s="T144">v</ta>
            <ta e="T146" id="Seg_2092" s="T145">n</ta>
            <ta e="T147" id="Seg_2093" s="T146">v</ta>
            <ta e="T148" id="Seg_2094" s="T147">v</ta>
            <ta e="T149" id="Seg_2095" s="T148">pers</ta>
            <ta e="T150" id="Seg_2096" s="T149">n</ta>
            <ta e="T151" id="Seg_2097" s="T150">adv</ta>
            <ta e="T152" id="Seg_2098" s="T151">pers</ta>
            <ta e="T153" id="Seg_2099" s="T152">v</ta>
            <ta e="T154" id="Seg_2100" s="T153">v</ta>
            <ta e="T155" id="Seg_2101" s="T154">n</ta>
            <ta e="T156" id="Seg_2102" s="T155">v</ta>
            <ta e="T157" id="Seg_2103" s="T156">pers</ta>
            <ta e="T158" id="Seg_2104" s="T157">adv</ta>
            <ta e="T159" id="Seg_2105" s="T158">v</ta>
            <ta e="T160" id="Seg_2106" s="T159">adv</ta>
            <ta e="T161" id="Seg_2107" s="T160">v</ta>
            <ta e="T162" id="Seg_2108" s="T161">interrog</ta>
            <ta e="T163" id="Seg_2109" s="T162">v</ta>
            <ta e="T164" id="Seg_2110" s="T163">ptcl</ta>
            <ta e="T165" id="Seg_2111" s="T164">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_2112" s="T1">pro.h:E</ta>
            <ta e="T3" id="Seg_2113" s="T2">np:Time</ta>
            <ta e="T5" id="Seg_2114" s="T4">np:Th</ta>
            <ta e="T7" id="Seg_2115" s="T6">np:Th</ta>
            <ta e="T8" id="Seg_2116" s="T7">0.1.h:E</ta>
            <ta e="T10" id="Seg_2117" s="T9">np.h:Th</ta>
            <ta e="T11" id="Seg_2118" s="T10">0.1.h:E</ta>
            <ta e="T14" id="Seg_2119" s="T13">np.h:Th</ta>
            <ta e="T15" id="Seg_2120" s="T14">0.1.h:E</ta>
            <ta e="T17" id="Seg_2121" s="T16">pro.h:E</ta>
            <ta e="T19" id="Seg_2122" s="T18">np:Th</ta>
            <ta e="T21" id="Seg_2123" s="T20">0.1.h:E</ta>
            <ta e="T22" id="Seg_2124" s="T21">v:Th</ta>
            <ta e="T24" id="Seg_2125" s="T23">pro:A</ta>
            <ta e="T25" id="Seg_2126" s="T24">pro.h:B</ta>
            <ta e="T28" id="Seg_2127" s="T27">pro.h:E</ta>
            <ta e="T29" id="Seg_2128" s="T28">pro:Th</ta>
            <ta e="T32" id="Seg_2129" s="T31">np.h:P</ta>
            <ta e="T34" id="Seg_2130" s="T33">pro.h:P</ta>
            <ta e="T38" id="Seg_2131" s="T37">0.1.h:A</ta>
            <ta e="T39" id="Seg_2132" s="T38">pro.h:A</ta>
            <ta e="T41" id="Seg_2133" s="T40">pro.h:P</ta>
            <ta e="T44" id="Seg_2134" s="T43">0.1.h:P</ta>
            <ta e="T46" id="Seg_2135" s="T45">np:Th</ta>
            <ta e="T48" id="Seg_2136" s="T47">0.1.h:A</ta>
            <ta e="T50" id="Seg_2137" s="T49">0.1.h:P</ta>
            <ta e="T52" id="Seg_2138" s="T51">pro.h:E</ta>
            <ta e="T56" id="Seg_2139" s="T55">np:Th</ta>
            <ta e="T58" id="Seg_2140" s="T57">np.h:P</ta>
            <ta e="T61" id="Seg_2141" s="T60">0.1.h:E</ta>
            <ta e="T62" id="Seg_2142" s="T61">np:L</ta>
            <ta e="T64" id="Seg_2143" s="T63">0.1.h:Th</ta>
            <ta e="T65" id="Seg_2144" s="T64">pro.h:A</ta>
            <ta e="T68" id="Seg_2145" s="T67">pro.h:A</ta>
            <ta e="T70" id="Seg_2146" s="T69">pro.h:Th</ta>
            <ta e="T71" id="Seg_2147" s="T70">adv:L</ta>
            <ta e="T74" id="Seg_2148" s="T73">pro.h:Th</ta>
            <ta e="T75" id="Seg_2149" s="T74">pro.h:Th</ta>
            <ta e="T76" id="Seg_2150" s="T75">pro.h:Th</ta>
            <ta e="T78" id="Seg_2151" s="T77">pro.h:A</ta>
            <ta e="T82" id="Seg_2152" s="T81">pro.h:A</ta>
            <ta e="T83" id="Seg_2153" s="T82">adv:G</ta>
            <ta e="T85" id="Seg_2154" s="T84">np:Poss</ta>
            <ta e="T86" id="Seg_2155" s="T85">np:L</ta>
            <ta e="T87" id="Seg_2156" s="T86">np:Th</ta>
            <ta e="T88" id="Seg_2157" s="T87">pro.h:A</ta>
            <ta e="T90" id="Seg_2158" s="T89">np:Path</ta>
            <ta e="T92" id="Seg_2159" s="T91">pro.h:Poss</ta>
            <ta e="T93" id="Seg_2160" s="T92">np:G</ta>
            <ta e="T94" id="Seg_2161" s="T93">0.3.h:P</ta>
            <ta e="T95" id="Seg_2162" s="T94">pro.h:E</ta>
            <ta e="T98" id="Seg_2163" s="T97">pro.h:A</ta>
            <ta e="T101" id="Seg_2164" s="T100">np.h:Th</ta>
            <ta e="T104" id="Seg_2165" s="T103">0.3.h:A</ta>
            <ta e="T105" id="Seg_2166" s="T104">pro.h:A</ta>
            <ta e="T108" id="Seg_2167" s="T107">pro.h:P</ta>
            <ta e="T109" id="Seg_2168" s="T108">0.2.h:A</ta>
            <ta e="T111" id="Seg_2169" s="T110">0.1.h:P</ta>
            <ta e="T112" id="Seg_2170" s="T111">pro.h:E</ta>
            <ta e="T113" id="Seg_2171" s="T112">np:Time</ta>
            <ta e="T114" id="Seg_2172" s="T113">np.h:Th</ta>
            <ta e="T119" id="Seg_2173" s="T118">0.3.h:Th</ta>
            <ta e="T120" id="Seg_2174" s="T119">pro.h:R</ta>
            <ta e="T121" id="Seg_2175" s="T120">0.3.h:A</ta>
            <ta e="T122" id="Seg_2176" s="T121">pro.h:Poss</ta>
            <ta e="T123" id="Seg_2177" s="T122">np:P</ta>
            <ta e="T125" id="Seg_2178" s="T124">pro.h:A</ta>
            <ta e="T126" id="Seg_2179" s="T125">pro.h:Poss</ta>
            <ta e="T127" id="Seg_2180" s="T126">np:Th</ta>
            <ta e="T130" id="Seg_2181" s="T129">0.3:P</ta>
            <ta e="T131" id="Seg_2182" s="T130">np.h:Th 0.3.h:Poss</ta>
            <ta e="T133" id="Seg_2183" s="T132">pro.h:B</ta>
            <ta e="T134" id="Seg_2184" s="T133">0.3.h:Th</ta>
            <ta e="T135" id="Seg_2185" s="T134">pro.h:Poss</ta>
            <ta e="T136" id="Seg_2186" s="T135">np.h:Th</ta>
            <ta e="T137" id="Seg_2187" s="T136">np.h:Th</ta>
            <ta e="T139" id="Seg_2188" s="T138">0.3.h:A</ta>
            <ta e="T141" id="Seg_2189" s="T140">np:Time</ta>
            <ta e="T142" id="Seg_2190" s="T141">pro.h:E</ta>
            <ta e="T144" id="Seg_2191" s="T143">np:Th</ta>
            <ta e="T146" id="Seg_2192" s="T145">np:A</ta>
            <ta e="T148" id="Seg_2193" s="T147">0.2.h:A</ta>
            <ta e="T149" id="Seg_2194" s="T148">pro.h:Poss</ta>
            <ta e="T150" id="Seg_2195" s="T149">0.3:Th</ta>
            <ta e="T151" id="Seg_2196" s="T150">np:Time</ta>
            <ta e="T152" id="Seg_2197" s="T151">pro.h:E</ta>
            <ta e="T153" id="Seg_2198" s="T152">np:Th</ta>
            <ta e="T155" id="Seg_2199" s="T154">np:G</ta>
            <ta e="T156" id="Seg_2200" s="T155">0.1.h:A</ta>
            <ta e="T157" id="Seg_2201" s="T156">pro.h:E</ta>
            <ta e="T159" id="Seg_2202" s="T158">0.3:Th</ta>
            <ta e="T160" id="Seg_2203" s="T159">adv:Time</ta>
            <ta e="T161" id="Seg_2204" s="T160">0.1.h:A</ta>
            <ta e="T162" id="Seg_2205" s="T161">pro:Th</ta>
            <ta e="T165" id="Seg_2206" s="T164">0.1.h:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_2207" s="T1">pro.h:S</ta>
            <ta e="T4" id="Seg_2208" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_2209" s="T4">np:O</ta>
            <ta e="T7" id="Seg_2210" s="T6">np:O</ta>
            <ta e="T8" id="Seg_2211" s="T7">0.1.h:S v:pred</ta>
            <ta e="T10" id="Seg_2212" s="T9">np.h:O</ta>
            <ta e="T11" id="Seg_2213" s="T10">0.1.h:S v:pred</ta>
            <ta e="T14" id="Seg_2214" s="T13">np.h:O</ta>
            <ta e="T15" id="Seg_2215" s="T14">0.1.h:S v:pred</ta>
            <ta e="T16" id="Seg_2216" s="T15">s:temp</ta>
            <ta e="T17" id="Seg_2217" s="T16">pro.h:S</ta>
            <ta e="T18" id="Seg_2218" s="T17">v:pred</ta>
            <ta e="T19" id="Seg_2219" s="T18">np:O</ta>
            <ta e="T21" id="Seg_2220" s="T20">0.1.h:S v:pred</ta>
            <ta e="T22" id="Seg_2221" s="T21">v:O</ta>
            <ta e="T24" id="Seg_2222" s="T23">pro:S</ta>
            <ta e="T27" id="Seg_2223" s="T26">v:pred</ta>
            <ta e="T28" id="Seg_2224" s="T27">pro.h:S</ta>
            <ta e="T29" id="Seg_2225" s="T28">pro:O</ta>
            <ta e="T30" id="Seg_2226" s="T29">v:pred</ta>
            <ta e="T32" id="Seg_2227" s="T31">np.h:S</ta>
            <ta e="T33" id="Seg_2228" s="T32">v:pred</ta>
            <ta e="T34" id="Seg_2229" s="T33">pro.h:S</ta>
            <ta e="T35" id="Seg_2230" s="T34">v:pred</ta>
            <ta e="T38" id="Seg_2231" s="T37">0.1.h:S v:pred</ta>
            <ta e="T39" id="Seg_2232" s="T38">pro.h:S</ta>
            <ta e="T40" id="Seg_2233" s="T39">v:pred</ta>
            <ta e="T41" id="Seg_2234" s="T40">pro.h:S</ta>
            <ta e="T42" id="Seg_2235" s="T41">v:pred</ta>
            <ta e="T44" id="Seg_2236" s="T43">0.1.h:S v:pred</ta>
            <ta e="T46" id="Seg_2237" s="T45">np:O</ta>
            <ta e="T48" id="Seg_2238" s="T47">0.1.h:S v:pred</ta>
            <ta e="T50" id="Seg_2239" s="T49">0.1.h:S v:pred</ta>
            <ta e="T56" id="Seg_2240" s="T51">s:cond</ta>
            <ta e="T58" id="Seg_2241" s="T57">np.h:S</ta>
            <ta e="T59" id="Seg_2242" s="T58">v:pred</ta>
            <ta e="T61" id="Seg_2243" s="T60">0.1.h:S v:pred</ta>
            <ta e="T64" id="Seg_2244" s="T63">0.1.h:S v:pred</ta>
            <ta e="T65" id="Seg_2245" s="T64">pro.h:S</ta>
            <ta e="T67" id="Seg_2246" s="T66">v:pred</ta>
            <ta e="T68" id="Seg_2247" s="T67">pro.h:S</ta>
            <ta e="T69" id="Seg_2248" s="T68">v:pred</ta>
            <ta e="T70" id="Seg_2249" s="T69">pro.h:S</ta>
            <ta e="T74" id="Seg_2250" s="T73">pro.h:S</ta>
            <ta e="T75" id="Seg_2251" s="T74">pro.h:S</ta>
            <ta e="T76" id="Seg_2252" s="T75">pro.h:S</ta>
            <ta e="T78" id="Seg_2253" s="T77">pro.h:S</ta>
            <ta e="T80" id="Seg_2254" s="T79">v:pred</ta>
            <ta e="T82" id="Seg_2255" s="T81">pro.h:S</ta>
            <ta e="T84" id="Seg_2256" s="T83">v:pred</ta>
            <ta e="T87" id="Seg_2257" s="T86">np:S</ta>
            <ta e="T88" id="Seg_2258" s="T87">pro.h:S</ta>
            <ta e="T91" id="Seg_2259" s="T90">v:pred</ta>
            <ta e="T94" id="Seg_2260" s="T93">0.3.h:S v:pred</ta>
            <ta e="T95" id="Seg_2261" s="T94">pro.h:S</ta>
            <ta e="T97" id="Seg_2262" s="T96">v:pred</ta>
            <ta e="T98" id="Seg_2263" s="T97">pro.h:S</ta>
            <ta e="T99" id="Seg_2264" s="T98">v:pred</ta>
            <ta e="T101" id="Seg_2265" s="T100">np.h:S</ta>
            <ta e="T102" id="Seg_2266" s="T101">v:pred</ta>
            <ta e="T104" id="Seg_2267" s="T103">0.3.h:S v:pred</ta>
            <ta e="T105" id="Seg_2268" s="T104">pro.h:S</ta>
            <ta e="T106" id="Seg_2269" s="T105">v:pred</ta>
            <ta e="T108" id="Seg_2270" s="T107">pro.h:O</ta>
            <ta e="T109" id="Seg_2271" s="T108">0.2.h:S v:pred</ta>
            <ta e="T111" id="Seg_2272" s="T110">0.1.h:S v:pred</ta>
            <ta e="T112" id="Seg_2273" s="T111">pro.h:S</ta>
            <ta e="T114" id="Seg_2274" s="T113">np.h:O</ta>
            <ta e="T115" id="Seg_2275" s="T114">v:pred</ta>
            <ta e="T116" id="Seg_2276" s="T115">s:temp</ta>
            <ta e="T119" id="Seg_2277" s="T118">0.3.h:S v:pred</ta>
            <ta e="T121" id="Seg_2278" s="T120">0.3.h:S v:pred</ta>
            <ta e="T123" id="Seg_2279" s="T122">np:S</ta>
            <ta e="T124" id="Seg_2280" s="T123">v:pred</ta>
            <ta e="T125" id="Seg_2281" s="T124">pro.h:S</ta>
            <ta e="T127" id="Seg_2282" s="T126">np:O</ta>
            <ta e="T128" id="Seg_2283" s="T127">v:pred</ta>
            <ta e="T130" id="Seg_2284" s="T129">0.3:S v:pred</ta>
            <ta e="T131" id="Seg_2285" s="T130">np.h:S</ta>
            <ta e="T132" id="Seg_2286" s="T131">v:pred</ta>
            <ta e="T134" id="Seg_2287" s="T133">0.3.h:S v:pred</ta>
            <ta e="T136" id="Seg_2288" s="T135">np.h:S</ta>
            <ta e="T137" id="Seg_2289" s="T136">np.h:S</ta>
            <ta e="T138" id="Seg_2290" s="T137">v:pred</ta>
            <ta e="T139" id="Seg_2291" s="T138">0.3.h:S v:pred</ta>
            <ta e="T142" id="Seg_2292" s="T141">pro.h:S</ta>
            <ta e="T143" id="Seg_2293" s="T142">s:temp</ta>
            <ta e="T145" id="Seg_2294" s="T144">v:pred</ta>
            <ta e="T146" id="Seg_2295" s="T145">np:S</ta>
            <ta e="T147" id="Seg_2296" s="T146">v:pred</ta>
            <ta e="T148" id="Seg_2297" s="T147">0.2.h:S v:pred</ta>
            <ta e="T150" id="Seg_2298" s="T149">0.3:S n:pred</ta>
            <ta e="T152" id="Seg_2299" s="T151">pro.h:S</ta>
            <ta e="T153" id="Seg_2300" s="T152">np:O</ta>
            <ta e="T154" id="Seg_2301" s="T153">v:pred</ta>
            <ta e="T156" id="Seg_2302" s="T155">0.1.h:S v:pred</ta>
            <ta e="T159" id="Seg_2303" s="T158">0.3:S v:pred</ta>
            <ta e="T161" id="Seg_2304" s="T160">0.1.h:S v:pred</ta>
            <ta e="T163" id="Seg_2305" s="T161">s:compl</ta>
            <ta e="T165" id="Seg_2306" s="T164">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_2307" s="T4">RUS:core</ta>
            <ta e="T6" id="Seg_2308" s="T5">RUS:core</ta>
            <ta e="T9" id="Seg_2309" s="T8">RUS:cult</ta>
            <ta e="T12" id="Seg_2310" s="T11">RUS:gram</ta>
            <ta e="T13" id="Seg_2311" s="T12">RUS:cult</ta>
            <ta e="T23" id="Seg_2312" s="T22">RUS:gram</ta>
            <ta e="T36" id="Seg_2313" s="T35">RUS:gram</ta>
            <ta e="T43" id="Seg_2314" s="T42">RUS:gram</ta>
            <ta e="T47" id="Seg_2315" s="T46">RUS:core</ta>
            <ta e="T53" id="Seg_2316" s="T52">RUS:gram</ta>
            <ta e="T55" id="Seg_2317" s="T54">RUS:core</ta>
            <ta e="T65" id="Seg_2318" s="T64">RUS:gram</ta>
            <ta e="T66" id="Seg_2319" s="T65">RUS:core</ta>
            <ta e="T72" id="Seg_2320" s="T71">RUS:gram</ta>
            <ta e="T81" id="Seg_2321" s="T80">RUS:gram</ta>
            <ta e="T87" id="Seg_2322" s="T86">RUS:cult</ta>
            <ta e="T90" id="Seg_2323" s="T89">RUS:cult</ta>
            <ta e="T93" id="Seg_2324" s="T92">WNB Noun or pp</ta>
            <ta e="T96" id="Seg_2325" s="T95">RUS:core</ta>
            <ta e="T100" id="Seg_2326" s="T99">RUS:gram</ta>
            <ta e="T103" id="Seg_2327" s="T102">RUS:gram</ta>
            <ta e="T110" id="Seg_2328" s="T109">RUS:gram</ta>
            <ta e="T134" id="Seg_2329" s="T133">RUS:gram</ta>
            <ta e="T139" id="Seg_2330" s="T138">RUS:cult</ta>
            <ta e="T146" id="Seg_2331" s="T145">RUS:cult</ta>
            <ta e="T150" id="Seg_2332" s="T149">RUS:cult</ta>
            <ta e="T160" id="Seg_2333" s="T159">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_2334" s="T1">I saw a dream today.</ta>
            <ta e="T8" id="Seg_2335" s="T5">I saw Starosondorovo (= old village).</ta>
            <ta e="T11" id="Seg_2336" s="T8">I saw uncle Kon.</ta>
            <ta e="T16" id="Seg_2337" s="T11">And I saw aunt Praskovja in my dream.</ta>
            <ta e="T20" id="Seg_2338" s="T16">I saw my cow.</ta>
            <ta e="T27" id="Seg_2339" s="T20">I wanted to milk it, and it didn't allow [me to milk it].</ta>
            <ta e="T33" id="Seg_2340" s="T27">If I see it in my dream, (s)he will die.</ta>
            <ta e="T38" id="Seg_2341" s="T33">I woke up and began crying.</ta>
            <ta e="T44" id="Seg_2342" s="T38">I said : “Someone will die”, – and I woke up.</ta>
            <ta e="T48" id="Seg_2343" s="T44">I told this dream.</ta>
            <ta e="T51" id="Seg_2344" s="T48">I fell asleep (I am sleeping) again.</ta>
            <ta e="T59" id="Seg_2345" s="T51">If I saw Starosonodorovo (=old village) in my dream, someone will die.</ta>
            <ta e="T64" id="Seg_2346" s="T59">I saw again in my dream: I'm home alone.</ta>
            <ta e="T67" id="Seg_2347" s="T64">Someone is knocking.</ta>
            <ta e="T69" id="Seg_2348" s="T67">I ran [to the door].</ta>
            <ta e="T71" id="Seg_2349" s="T69">Who is there?</ta>
            <ta e="T77" id="Seg_2350" s="T71">And he: “It's me, it's me.”</ta>
            <ta e="T80" id="Seg_2351" s="T77">I don't let him in.</ta>
            <ta e="T84" id="Seg_2352" s="T80">And he climbed up.</ta>
            <ta e="T87" id="Seg_2353" s="T84">There was a window above the door.</ta>
            <ta e="T91" id="Seg_2354" s="T87">He climbed down through that window.</ta>
            <ta e="T94" id="Seg_2355" s="T91">He fell over me.</ta>
            <ta e="T97" id="Seg_2356" s="T94">I got so afraid.</ta>
            <ta e="T104" id="Seg_2357" s="T97">I looked at him, and Volodjka is lying and laughing.</ta>
            <ta e="T109" id="Seg_2358" s="T104">I said: “Why did you frighten me?”</ta>
            <ta e="T111" id="Seg_2359" s="T109">And I woke up.</ta>
            <ta e="T116" id="Seg_2360" s="T111">Today I saw Filat in my dream.</ta>
            <ta e="T119" id="Seg_2361" s="T116">He was sitting barefoot.</ta>
            <ta e="T124" id="Seg_2362" s="T119">He said to me: “My feet are frozen.”</ta>
            <ta e="T128" id="Seg_2363" s="T124">I touched his feet.</ta>
            <ta e="T130" id="Seg_2364" s="T128">They were frozen.</ta>
            <ta e="T134" id="Seg_2365" s="T130">He has no children, who needs him.</ta>
            <ta e="T138" id="Seg_2366" s="T134">He has got a younger brother.</ta>
            <ta e="T140" id="Seg_2367" s="T138">He was playing accordion all the time.</ta>
            <ta e="T145" id="Seg_2368" s="T140">Today I saw excrements in my dream.</ta>
            <ta e="T150" id="Seg_2369" s="T145">A cat pooped, wipe it, it's your cat.</ta>
            <ta e="T154" id="Seg_2370" s="T150">Today I saw a dream.</ta>
            <ta e="T156" id="Seg_2371" s="T154">I came downhill.</ta>
            <ta e="T159" id="Seg_2372" s="T156">I feel well.</ta>
            <ta e="T161" id="Seg_2373" s="T159">Now I wait.</ta>
            <ta e="T165" id="Seg_2374" s="T161">I don't know what happens.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_2375" s="T1">Ich habe heute einen Traum geträumt.</ta>
            <ta e="T8" id="Seg_2376" s="T5">Ich habe Starosondorovo (= altes Dorf) gesehen.</ta>
            <ta e="T11" id="Seg_2377" s="T8">Ich habe Onkel Kon gesehen.</ta>
            <ta e="T16" id="Seg_2378" s="T11">Und ich habe Tante Praskovja in meinem Traum gesehen.</ta>
            <ta e="T20" id="Seg_2379" s="T16">Ich habe meine Kuh gesehen.</ta>
            <ta e="T27" id="Seg_2380" s="T20">Ich wollte sie melken und sie erlaubte [mir nicht sie zu melken].</ta>
            <ta e="T33" id="Seg_2381" s="T27">Sehe ich es in meinem Traum, wird er/sie sterben.</ta>
            <ta e="T38" id="Seg_2382" s="T33">Ich wachte auf und begann zu weinen.</ta>
            <ta e="T44" id="Seg_2383" s="T38">Ich sagte: "Jemand wird sterben", - und wachte auf.</ta>
            <ta e="T48" id="Seg_2384" s="T44">Ich erzählte meinen Traum.</ta>
            <ta e="T51" id="Seg_2385" s="T48">Ich schlief wieder ein (ich schlafe).</ta>
            <ta e="T59" id="Seg_2386" s="T51">Wenn ich Starosondorovo (=altes Dorf) im Traum sah, starb jemand.</ta>
            <ta e="T64" id="Seg_2387" s="T59">Ich träumte wieder, ich bin alleine zu Hause.</ta>
            <ta e="T67" id="Seg_2388" s="T64">Jemand klopft an.</ta>
            <ta e="T69" id="Seg_2389" s="T67">Ich rannte [zur Tür].</ta>
            <ta e="T71" id="Seg_2390" s="T69">Wer ist da?</ta>
            <ta e="T77" id="Seg_2391" s="T71">Und er: "Ich bin es, ich bin es."</ta>
            <ta e="T80" id="Seg_2392" s="T77">Ich lasse ihn nicht herein.</ta>
            <ta e="T84" id="Seg_2393" s="T80">Und er kletterte hinauf.</ta>
            <ta e="T87" id="Seg_2394" s="T84">Da ist ein Fenster über der Tür.</ta>
            <ta e="T91" id="Seg_2395" s="T87">Er kletterte durch dieses Fenster herunter.</ta>
            <ta e="T94" id="Seg_2396" s="T91">Er fiel auf mich.</ta>
            <ta e="T97" id="Seg_2397" s="T94">Ich hatte solche Angst.</ta>
            <ta e="T104" id="Seg_2398" s="T97">Ich sah ihn an und Volodjka liegt und lacht.</ta>
            <ta e="T109" id="Seg_2399" s="T104">Ich sagte: "Warum hast du mich erschreckt?"</ta>
            <ta e="T111" id="Seg_2400" s="T109">Und ich wachte auf.</ta>
            <ta e="T116" id="Seg_2401" s="T111">Ich habe heute Filat in meinem Traum gesehen.</ta>
            <ta e="T119" id="Seg_2402" s="T116">Er saß barfuß.</ta>
            <ta e="T124" id="Seg_2403" s="T119">Er sagte mir: "Meine Füße sind erfroren."</ta>
            <ta e="T128" id="Seg_2404" s="T124">Ich berührte seine Füße.</ta>
            <ta e="T130" id="Seg_2405" s="T128">Sie waren erfroren.</ta>
            <ta e="T134" id="Seg_2406" s="T130">Er hat keine Kinder, wer braucht ihn.</ta>
            <ta e="T138" id="Seg_2407" s="T134">Er hat einen jüngeren Bruder.</ta>
            <ta e="T140" id="Seg_2408" s="T138">Er spielte die ganze Zeit Akkordion.</ta>
            <ta e="T145" id="Seg_2409" s="T140">Heute habe ich im Traum Kot gesehen.</ta>
            <ta e="T150" id="Seg_2410" s="T145">Eine Katze hat geschissen, wisch sie ab, es ist deine Katze.</ta>
            <ta e="T154" id="Seg_2411" s="T150">Heute sah ich einen Traum.</ta>
            <ta e="T156" id="Seg_2412" s="T154">Ich lief den Hügel hinab.</ta>
            <ta e="T159" id="Seg_2413" s="T156">Ich fühle mich gut.</ta>
            <ta e="T161" id="Seg_2414" s="T159">Jetzt warte ich.</ta>
            <ta e="T165" id="Seg_2415" s="T161">Ich weiß nicht, was passiert.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_2416" s="T1">Я сегодня видела сон.</ta>
            <ta e="T8" id="Seg_2417" s="T5">Старое Сондрово (= старую деревню) видела.</ta>
            <ta e="T11" id="Seg_2418" s="T8">Дядю Кона видела.</ta>
            <ta e="T16" id="Seg_2419" s="T11">И тетку Прасковью видела во сне.</ta>
            <ta e="T20" id="Seg_2420" s="T16">Я видела свою корову.</ta>
            <ta e="T27" id="Seg_2421" s="T20">Я ее доить хотела, она мне не далась доить.</ta>
            <ta e="T33" id="Seg_2422" s="T27">(Если) Это вижу [во сне], кто-нибудь помрет.</ta>
            <ta e="T38" id="Seg_2423" s="T33">Я проснулась и плакать стала.</ta>
            <ta e="T44" id="Seg_2424" s="T38">Я говорю: “Кто-то помрет”, – и проснулась.</ta>
            <ta e="T48" id="Seg_2425" s="T44">Этот сон я весь рассказала.</ta>
            <ta e="T51" id="Seg_2426" s="T48">Опять уснула (сплю).</ta>
            <ta e="T59" id="Seg_2427" s="T51">Если я вижу во сне Старое Сондорово (= старую деревню), кто-нибудь умрет.</ta>
            <ta e="T64" id="Seg_2428" s="T59">Опять во сне видела: дома одна сижу.</ta>
            <ta e="T67" id="Seg_2429" s="T64">Кто-то стучится (стучать начал).</ta>
            <ta e="T69" id="Seg_2430" s="T67">Я побежала.</ta>
            <ta e="T71" id="Seg_2431" s="T69">Кто там?</ta>
            <ta e="T77" id="Seg_2432" s="T71">А он: “Я, я, я это”.</ta>
            <ta e="T80" id="Seg_2433" s="T77">Я не пускаю.</ta>
            <ta e="T84" id="Seg_2434" s="T80">А он наверх залез.</ta>
            <ta e="T87" id="Seg_2435" s="T84">На двери там (над дверью наверху) окошко было.</ta>
            <ta e="T91" id="Seg_2436" s="T87">Он по этому окошку спустился.</ta>
            <ta e="T94" id="Seg_2437" s="T91">На меня упал.</ta>
            <ta e="T97" id="Seg_2438" s="T94">Я до того испугалась.</ta>
            <ta e="T104" id="Seg_2439" s="T97">Я взглянула, а Володька лежит и хохочет.</ta>
            <ta e="T109" id="Seg_2440" s="T104">Я сказала: “Зачем ты меня испугал?”</ta>
            <ta e="T111" id="Seg_2441" s="T109">И проснулась (=разбудилась).</ta>
            <ta e="T116" id="Seg_2442" s="T111">Я сегодня Филата видела во сне.</ta>
            <ta e="T119" id="Seg_2443" s="T116">Он босиком сидел.</ta>
            <ta e="T124" id="Seg_2444" s="T119">Он мне говорит: “Мои ноги застыли”.</ta>
            <ta e="T128" id="Seg_2445" s="T124">Я схватила его ноги.</ta>
            <ta e="T130" id="Seg_2446" s="T128">Они у него замерзшие.</ta>
            <ta e="T134" id="Seg_2447" s="T130">Детей нет, кому он нужен.</ta>
            <ta e="T138" id="Seg_2448" s="T134">У него есть младший брат.</ta>
            <ta e="T140" id="Seg_2449" s="T138">Он все время в гармонь (на гармони?) играл.</ta>
            <ta e="T145" id="Seg_2450" s="T140">Сегодня я во сне кал видала.</ta>
            <ta e="T150" id="Seg_2451" s="T145">Кошка накакала, вытри, твоя кошка.</ta>
            <ta e="T154" id="Seg_2452" s="T150">Сегодня я сон видела.</ta>
            <ta e="T156" id="Seg_2453" s="T154">Я под гору спустилась.</ta>
            <ta e="T159" id="Seg_2454" s="T156">Мне хорошо.</ta>
            <ta e="T161" id="Seg_2455" s="T159">Теперь жду.</ta>
            <ta e="T165" id="Seg_2456" s="T161">Что будет, не знаю.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_2457" s="T1">я сегодня видала сон</ta>
            <ta e="T8" id="Seg_2458" s="T5">старое Сондрово видела</ta>
            <ta e="T11" id="Seg_2459" s="T8">дядю Кона</ta>
            <ta e="T16" id="Seg_2460" s="T11">и тетку Прасковью видела во сне</ta>
            <ta e="T20" id="Seg_2461" s="T16">я видела свою корову</ta>
            <ta e="T27" id="Seg_2462" s="T20">доить хотела она мне не далась доить</ta>
            <ta e="T33" id="Seg_2463" s="T27">если это вижу во сне кто-нибудь помрет</ta>
            <ta e="T38" id="Seg_2464" s="T33">я разбудилась (проснулась) и плакать стала</ta>
            <ta e="T44" id="Seg_2465" s="T38">я рассказала (сон) и говорю кто-то помрет разбудилась</ta>
            <ta e="T48" id="Seg_2466" s="T44">этот сон весь рассказала</ta>
            <ta e="T51" id="Seg_2467" s="T48">опять сплю (уснула)</ta>
            <ta e="T59" id="Seg_2468" s="T51">если я вижу во сне старое сондорово кто-нибудь помрет</ta>
            <ta e="T64" id="Seg_2469" s="T59">опять во сне видала дома одна сижу</ta>
            <ta e="T67" id="Seg_2470" s="T64">кто-то стукается (стучать начал)</ta>
            <ta e="T69" id="Seg_2471" s="T67">я побежала</ta>
            <ta e="T71" id="Seg_2472" s="T69">кто там</ta>
            <ta e="T77" id="Seg_2473" s="T71">а он я я я это</ta>
            <ta e="T80" id="Seg_2474" s="T77">я не пускаю</ta>
            <ta e="T84" id="Seg_2475" s="T80">а он наверх залез</ta>
            <ta e="T87" id="Seg_2476" s="T84">на двери там (над дверью наверху) окошко было</ta>
            <ta e="T91" id="Seg_2477" s="T87">он с этого окошка и спустился</ta>
            <ta e="T94" id="Seg_2478" s="T91">прямо на меня упал</ta>
            <ta e="T97" id="Seg_2479" s="T94">я до того испугалась</ta>
            <ta e="T104" id="Seg_2480" s="T97">я взглянула в Володька лежит и хохочет</ta>
            <ta e="T109" id="Seg_2481" s="T104">я сказала зачем ты меня испугал</ta>
            <ta e="T111" id="Seg_2482" s="T109">и проснулась (разбудилась)</ta>
            <ta e="T116" id="Seg_2483" s="T111">я сегодня Филата видела во сне</ta>
            <ta e="T119" id="Seg_2484" s="T116">босиком сидел</ta>
            <ta e="T124" id="Seg_2485" s="T119">мне говорит мои ноги застыли</ta>
            <ta e="T128" id="Seg_2486" s="T124">я поймала его ноги</ta>
            <ta e="T130" id="Seg_2487" s="T128">у него холодные (и правда как лед)</ta>
            <ta e="T134" id="Seg_2488" s="T130">детей нет кому он нужен</ta>
            <ta e="T138" id="Seg_2489" s="T134">у него есть младший брат</ta>
            <ta e="T140" id="Seg_2490" s="T138">в гармонь всегда играл</ta>
            <ta e="T145" id="Seg_2491" s="T140">сегодня я во сне кал видала</ta>
            <ta e="T150" id="Seg_2492" s="T145">кошка накакала вытри твоя кошка</ta>
            <ta e="T154" id="Seg_2493" s="T150">сегодня я сон видела</ta>
            <ta e="T156" id="Seg_2494" s="T154">под гору спустилась</ta>
            <ta e="T159" id="Seg_2495" s="T156">мне хорошо</ta>
            <ta e="T161" id="Seg_2496" s="T159">теперь жду</ta>
            <ta e="T165" id="Seg_2497" s="T161">что будет не знаю</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T5" id="Seg_2498" s="T1">[OSV:] First dream.</ta>
            <ta e="T20" id="Seg_2499" s="T16">[BrM:] GEN instead of ACC?</ta>
            <ta e="T27" id="Seg_2500" s="T20">[KuAI:] Variant: 'paːrgɨlɨɣu'. [BrM:] Tentative analysis of 'paːrgɨlɨgu'.</ta>
            <ta e="T48" id="Seg_2501" s="T44">[BrM:] Tentative analysis of 'küdrʼim'.</ta>
            <ta e="T64" id="Seg_2502" s="T59">[OSV:] Second dream.</ta>
            <ta e="T80" id="Seg_2503" s="T77">[BrM:] 3SG.S?</ta>
            <ta e="T97" id="Seg_2504" s="T94">[KuAI:] Variants: 'kɨtʼewaːnnan', 'kəcʼewaːnnan', 'kətʼewaːnnan'. [BrM:] 'da tawa' changed to 'datawa'.</ta>
            <ta e="T109" id="Seg_2505" s="T104">[BrM:] 'Qajnozɨm' changed to 'Qajno zɨm'.</ta>
            <ta e="T116" id="Seg_2506" s="T111">[OSV:] Third dream.</ta>
            <ta e="T145" id="Seg_2507" s="T140">[OSV:] Fourth dream. [BrM:] GEN?</ta>
            <ta e="T154" id="Seg_2508" s="T150">[OSV:] Fifth dream.</ta>
            <ta e="T156" id="Seg_2509" s="T154">[KuAI:] Variant: 'qännolottə'. [BrM:] nolo &lt;&lt; 'lɨ - 'under'?</ta>
            <ta e="T159" id="Seg_2510" s="T156">[KuAI:] Variant: 'mega' or 'mekga'?</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T5" id="Seg_2511" s="T1">первый сон</ta>
            <ta e="T64" id="Seg_2512" s="T59">второй сон</ta>
            <ta e="T116" id="Seg_2513" s="T111">третий сон</ta>
            <ta e="T145" id="Seg_2514" s="T140">четвертый сон</ta>
            <ta e="T154" id="Seg_2515" s="T150">пятый сон</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
