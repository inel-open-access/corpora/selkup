<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_DuckWithDucklings_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_DuckWithDucklings_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">35</ud-information>
            <ud-information attribute-name="# HIAT:w">27</ud-information>
            <ud-information attribute-name="# e">27</ud-information>
            <ud-information attribute-name="# HIAT:u">8</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T28" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">amdəzan</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">matqan</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Man</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">maǯedʼiddan</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">pone</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">A</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">nʼap</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">paldän</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_38" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">Patom</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">tep</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">kösse</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">qwanən</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_53" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">Patom</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">čaʒən</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">sibakalazʼe</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_65" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">Man</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">toɣulǯʼau</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">köt</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">sibaka</ts>
                  <nts id="Seg_77" n="HIAT:ip">.</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_80" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">Patom</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">aj</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">sədəjaq</ts>
                  <nts id="Seg_89" n="HIAT:ip">.</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_92" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">Tebla</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_97" n="HIAT:w" s="T25">wes</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_100" n="HIAT:w" s="T26">qwajdə</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">qwannat</ts>
                  <nts id="Seg_104" n="HIAT:ip">.</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T28" id="Seg_106" n="sc" s="T1">
               <ts e="T2" id="Seg_108" n="e" s="T1">Man </ts>
               <ts e="T3" id="Seg_110" n="e" s="T2">amdəzan </ts>
               <ts e="T4" id="Seg_112" n="e" s="T3">matqan. </ts>
               <ts e="T5" id="Seg_114" n="e" s="T4">Man </ts>
               <ts e="T6" id="Seg_116" n="e" s="T5">maǯedʼiddan </ts>
               <ts e="T7" id="Seg_118" n="e" s="T6">pone. </ts>
               <ts e="T8" id="Seg_120" n="e" s="T7">A </ts>
               <ts e="T9" id="Seg_122" n="e" s="T8">nʼap </ts>
               <ts e="T10" id="Seg_124" n="e" s="T9">paldän. </ts>
               <ts e="T11" id="Seg_126" n="e" s="T10">Patom </ts>
               <ts e="T12" id="Seg_128" n="e" s="T11">tep </ts>
               <ts e="T13" id="Seg_130" n="e" s="T12">kösse </ts>
               <ts e="T14" id="Seg_132" n="e" s="T13">qwanən. </ts>
               <ts e="T15" id="Seg_134" n="e" s="T14">Patom </ts>
               <ts e="T16" id="Seg_136" n="e" s="T15">čaʒən </ts>
               <ts e="T17" id="Seg_138" n="e" s="T16">sibakalazʼe. </ts>
               <ts e="T18" id="Seg_140" n="e" s="T17">Man </ts>
               <ts e="T19" id="Seg_142" n="e" s="T18">toɣulǯʼau </ts>
               <ts e="T20" id="Seg_144" n="e" s="T19">köt </ts>
               <ts e="T21" id="Seg_146" n="e" s="T20">sibaka. </ts>
               <ts e="T22" id="Seg_148" n="e" s="T21">Patom </ts>
               <ts e="T23" id="Seg_150" n="e" s="T22">aj </ts>
               <ts e="T24" id="Seg_152" n="e" s="T23">sədəjaq. </ts>
               <ts e="T25" id="Seg_154" n="e" s="T24">Tebla </ts>
               <ts e="T26" id="Seg_156" n="e" s="T25">wes </ts>
               <ts e="T27" id="Seg_158" n="e" s="T26">qwajdə </ts>
               <ts e="T28" id="Seg_160" n="e" s="T27">qwannat. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_161" s="T1">PVD_1964_DuckWithDucklings_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_162" s="T4">PVD_1964_DuckWithDucklings_nar.002 (001.002)</ta>
            <ta e="T10" id="Seg_163" s="T7">PVD_1964_DuckWithDucklings_nar.003 (001.003)</ta>
            <ta e="T14" id="Seg_164" s="T10">PVD_1964_DuckWithDucklings_nar.004 (001.004)</ta>
            <ta e="T17" id="Seg_165" s="T14">PVD_1964_DuckWithDucklings_nar.005 (001.005)</ta>
            <ta e="T21" id="Seg_166" s="T17">PVD_1964_DuckWithDucklings_nar.006 (001.006)</ta>
            <ta e="T24" id="Seg_167" s="T21">PVD_1964_DuckWithDucklings_nar.007 (001.007)</ta>
            <ta e="T28" id="Seg_168" s="T24">PVD_1964_DuckWithDucklings_nar.008 (001.008)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_169" s="T1">ман ′амдъзан ′матkан.</ta>
            <ta e="T7" id="Seg_170" s="T4">ман ма′джедʼиддан ′поне̨.</ta>
            <ta e="T10" id="Seg_171" s="T7">а нʼап ′паlдӓн.</ta>
            <ta e="T14" id="Seg_172" s="T10">па′том ′теп ′кӧссе ′kwанън.</ta>
            <ta e="T17" id="Seg_173" s="T14">па′том ′тшажън ′сибакалазʼе.</ta>
            <ta e="T21" id="Seg_174" s="T17">ман ′тоɣуlдʼжʼау̹ кӧт ′сибака.</ta>
            <ta e="T24" id="Seg_175" s="T21">патом ′ай ′съдъ jаk.</ta>
            <ta e="T28" id="Seg_176" s="T24">теб′ла вес ′kwайдъ ′kwаннат.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_177" s="T1">man amdəzan matqan.</ta>
            <ta e="T7" id="Seg_178" s="T4">man maǯedʼiddan pone.</ta>
            <ta e="T10" id="Seg_179" s="T7">a nʼap paldän.</ta>
            <ta e="T14" id="Seg_180" s="T10">patom tep kösse qwanən.</ta>
            <ta e="T17" id="Seg_181" s="T14">patom tšaʒən sibakalazʼe.</ta>
            <ta e="T21" id="Seg_182" s="T17">man toɣulǯʼau̹ köt sibaka.</ta>
            <ta e="T24" id="Seg_183" s="T21">patom aj sədə jaq.</ta>
            <ta e="T28" id="Seg_184" s="T24">tebla wes qwajdə qwannat.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_185" s="T1">Man amdəzan matqan. </ta>
            <ta e="T7" id="Seg_186" s="T4">Man maǯedʼiddan pone. </ta>
            <ta e="T10" id="Seg_187" s="T7">A nʼap paldän. </ta>
            <ta e="T14" id="Seg_188" s="T10">Patom tep kösse qwanən. </ta>
            <ta e="T17" id="Seg_189" s="T14">Patom čaʒən sibakalazʼe. </ta>
            <ta e="T21" id="Seg_190" s="T17">Man toɣulǯʼau köt sibaka. </ta>
            <ta e="T24" id="Seg_191" s="T21">Patom aj sədəjaq. </ta>
            <ta e="T28" id="Seg_192" s="T24">Tebla wes qwajdə qwannat. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_193" s="T1">man</ta>
            <ta e="T3" id="Seg_194" s="T2">amdə-za-n</ta>
            <ta e="T4" id="Seg_195" s="T3">mat-qan</ta>
            <ta e="T5" id="Seg_196" s="T4">man</ta>
            <ta e="T6" id="Seg_197" s="T5">maǯe-dʼi-dda-n</ta>
            <ta e="T7" id="Seg_198" s="T6">pone</ta>
            <ta e="T8" id="Seg_199" s="T7">a</ta>
            <ta e="T9" id="Seg_200" s="T8">nʼap</ta>
            <ta e="T10" id="Seg_201" s="T9">paldä-n</ta>
            <ta e="T11" id="Seg_202" s="T10">patom</ta>
            <ta e="T12" id="Seg_203" s="T11">tep</ta>
            <ta e="T13" id="Seg_204" s="T12">kösse</ta>
            <ta e="T14" id="Seg_205" s="T13">qwan-ə-n</ta>
            <ta e="T15" id="Seg_206" s="T14">patom</ta>
            <ta e="T16" id="Seg_207" s="T15">čaʒə-n</ta>
            <ta e="T17" id="Seg_208" s="T16">siba-ka-la-zʼe</ta>
            <ta e="T18" id="Seg_209" s="T17">man</ta>
            <ta e="T19" id="Seg_210" s="T18">toɣu-lǯʼa-u</ta>
            <ta e="T20" id="Seg_211" s="T19">köt</ta>
            <ta e="T21" id="Seg_212" s="T20">siba-ka</ta>
            <ta e="T22" id="Seg_213" s="T21">patom</ta>
            <ta e="T23" id="Seg_214" s="T22">aj</ta>
            <ta e="T24" id="Seg_215" s="T23">sədə-ja-q</ta>
            <ta e="T25" id="Seg_216" s="T24">teb-la</ta>
            <ta e="T26" id="Seg_217" s="T25">wes</ta>
            <ta e="T27" id="Seg_218" s="T26">qwaj-də</ta>
            <ta e="T28" id="Seg_219" s="T27">qwan-na-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_220" s="T1">man</ta>
            <ta e="T3" id="Seg_221" s="T2">amdɨ-sɨ-ŋ</ta>
            <ta e="T4" id="Seg_222" s="T3">maːt-qɨn</ta>
            <ta e="T5" id="Seg_223" s="T4">man</ta>
            <ta e="T6" id="Seg_224" s="T5">manǯu-dʼi-ntɨ-ŋ</ta>
            <ta e="T7" id="Seg_225" s="T6">poːne</ta>
            <ta e="T8" id="Seg_226" s="T7">a</ta>
            <ta e="T9" id="Seg_227" s="T8">nʼaːb</ta>
            <ta e="T10" id="Seg_228" s="T9">palʼdʼi-n</ta>
            <ta e="T11" id="Seg_229" s="T10">patom</ta>
            <ta e="T12" id="Seg_230" s="T11">täp</ta>
            <ta e="T13" id="Seg_231" s="T12">kössə</ta>
            <ta e="T14" id="Seg_232" s="T13">qwan-ɨ-n</ta>
            <ta e="T15" id="Seg_233" s="T14">patom</ta>
            <ta e="T16" id="Seg_234" s="T15">čaǯɨ-n</ta>
            <ta e="T17" id="Seg_235" s="T16">siba-ka-la-se</ta>
            <ta e="T18" id="Seg_236" s="T17">man</ta>
            <ta e="T19" id="Seg_237" s="T18">*toːqo-lǯi-w</ta>
            <ta e="T20" id="Seg_238" s="T19">köt</ta>
            <ta e="T21" id="Seg_239" s="T20">siba-ka</ta>
            <ta e="T22" id="Seg_240" s="T21">patom</ta>
            <ta e="T23" id="Seg_241" s="T22">aj</ta>
            <ta e="T24" id="Seg_242" s="T23">sədə-ja-qi</ta>
            <ta e="T25" id="Seg_243" s="T24">täp-la</ta>
            <ta e="T26" id="Seg_244" s="T25">wesʼ</ta>
            <ta e="T27" id="Seg_245" s="T26">kwaj-ntə</ta>
            <ta e="T28" id="Seg_246" s="T27">qwan-nɨ-tɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_247" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_248" s="T2">sit-PST-1SG.S</ta>
            <ta e="T4" id="Seg_249" s="T3">house-LOC</ta>
            <ta e="T5" id="Seg_250" s="T4">I.NOM</ta>
            <ta e="T6" id="Seg_251" s="T5">look.at-DRV-INFER-1SG.S</ta>
            <ta e="T7" id="Seg_252" s="T6">outward(s)</ta>
            <ta e="T8" id="Seg_253" s="T7">and</ta>
            <ta e="T9" id="Seg_254" s="T8">duck.[NOM]</ta>
            <ta e="T10" id="Seg_255" s="T9">walk-3SG.S</ta>
            <ta e="T11" id="Seg_256" s="T10">then</ta>
            <ta e="T12" id="Seg_257" s="T11">(s)he.[NOM]</ta>
            <ta e="T13" id="Seg_258" s="T12">backward</ta>
            <ta e="T14" id="Seg_259" s="T13">leave-EP-3SG.S</ta>
            <ta e="T15" id="Seg_260" s="T14">then</ta>
            <ta e="T16" id="Seg_261" s="T15">go-3SG.S</ta>
            <ta e="T17" id="Seg_262" s="T16">duckling-DIM-PL-COM</ta>
            <ta e="T18" id="Seg_263" s="T17">I.NOM</ta>
            <ta e="T19" id="Seg_264" s="T18">count-PFV-1SG.O</ta>
            <ta e="T20" id="Seg_265" s="T19">ten</ta>
            <ta e="T21" id="Seg_266" s="T20">duckling-DIM.[NOM]</ta>
            <ta e="T22" id="Seg_267" s="T21">then</ta>
            <ta e="T23" id="Seg_268" s="T22">again</ta>
            <ta e="T24" id="Seg_269" s="T23">two-DU-DU.[NOM]</ta>
            <ta e="T25" id="Seg_270" s="T24">(s)he-PL.[NOM]</ta>
            <ta e="T26" id="Seg_271" s="T25">all</ta>
            <ta e="T27" id="Seg_272" s="T26">big.river-ILL</ta>
            <ta e="T28" id="Seg_273" s="T27">leave-CO-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_274" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_275" s="T2">сидеть-PST-1SG.S</ta>
            <ta e="T4" id="Seg_276" s="T3">дом-LOC</ta>
            <ta e="T5" id="Seg_277" s="T4">я.NOM</ta>
            <ta e="T6" id="Seg_278" s="T5">посмотреть-DRV-INFER-1SG.S</ta>
            <ta e="T7" id="Seg_279" s="T6">наружу</ta>
            <ta e="T8" id="Seg_280" s="T7">а</ta>
            <ta e="T9" id="Seg_281" s="T8">утка.[NOM]</ta>
            <ta e="T10" id="Seg_282" s="T9">ходить-3SG.S</ta>
            <ta e="T11" id="Seg_283" s="T10">потом</ta>
            <ta e="T12" id="Seg_284" s="T11">он(а).[NOM]</ta>
            <ta e="T13" id="Seg_285" s="T12">назад</ta>
            <ta e="T14" id="Seg_286" s="T13">отправиться-EP-3SG.S</ta>
            <ta e="T15" id="Seg_287" s="T14">потом</ta>
            <ta e="T16" id="Seg_288" s="T15">ходить-3SG.S</ta>
            <ta e="T17" id="Seg_289" s="T16">утенок-DIM-PL-COM</ta>
            <ta e="T18" id="Seg_290" s="T17">я.NOM</ta>
            <ta e="T19" id="Seg_291" s="T18">считать-PFV-1SG.O</ta>
            <ta e="T20" id="Seg_292" s="T19">десять</ta>
            <ta e="T21" id="Seg_293" s="T20">утенок-DIM.[NOM]</ta>
            <ta e="T22" id="Seg_294" s="T21">потом</ta>
            <ta e="T23" id="Seg_295" s="T22">опять</ta>
            <ta e="T24" id="Seg_296" s="T23">два-DU-DU.[NOM]</ta>
            <ta e="T25" id="Seg_297" s="T24">он(а)-PL.[NOM]</ta>
            <ta e="T26" id="Seg_298" s="T25">весь</ta>
            <ta e="T27" id="Seg_299" s="T26">большая.река-ILL</ta>
            <ta e="T28" id="Seg_300" s="T27">отправиться-CO-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_301" s="T1">pers</ta>
            <ta e="T3" id="Seg_302" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_303" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_304" s="T4">pers</ta>
            <ta e="T6" id="Seg_305" s="T5">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T7" id="Seg_306" s="T6">adv</ta>
            <ta e="T8" id="Seg_307" s="T7">conj</ta>
            <ta e="T9" id="Seg_308" s="T8">n.[n:case]</ta>
            <ta e="T10" id="Seg_309" s="T9">v-v:pn</ta>
            <ta e="T11" id="Seg_310" s="T10">adv</ta>
            <ta e="T12" id="Seg_311" s="T11">pers.[n:case]</ta>
            <ta e="T13" id="Seg_312" s="T12">adv</ta>
            <ta e="T14" id="Seg_313" s="T13">v-v:ins-v:pn</ta>
            <ta e="T15" id="Seg_314" s="T14">adv</ta>
            <ta e="T16" id="Seg_315" s="T15">v-v:pn</ta>
            <ta e="T17" id="Seg_316" s="T16">n-n&gt;n-n:num-n:case</ta>
            <ta e="T18" id="Seg_317" s="T17">pers</ta>
            <ta e="T19" id="Seg_318" s="T18">v-v&gt;v-v:pn</ta>
            <ta e="T20" id="Seg_319" s="T19">num</ta>
            <ta e="T21" id="Seg_320" s="T20">n-n&gt;n.[n:case]</ta>
            <ta e="T22" id="Seg_321" s="T21">adv</ta>
            <ta e="T23" id="Seg_322" s="T22">adv</ta>
            <ta e="T24" id="Seg_323" s="T23">num-n:num-n:num.[n:case]</ta>
            <ta e="T25" id="Seg_324" s="T24">pers-n:num.[n:case]</ta>
            <ta e="T26" id="Seg_325" s="T25">quant</ta>
            <ta e="T27" id="Seg_326" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_327" s="T27">v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_328" s="T1">pers</ta>
            <ta e="T3" id="Seg_329" s="T2">v</ta>
            <ta e="T4" id="Seg_330" s="T3">n</ta>
            <ta e="T5" id="Seg_331" s="T4">pers</ta>
            <ta e="T6" id="Seg_332" s="T5">v</ta>
            <ta e="T7" id="Seg_333" s="T6">adv</ta>
            <ta e="T8" id="Seg_334" s="T7">conj</ta>
            <ta e="T9" id="Seg_335" s="T8">n</ta>
            <ta e="T10" id="Seg_336" s="T9">v</ta>
            <ta e="T11" id="Seg_337" s="T10">adv</ta>
            <ta e="T12" id="Seg_338" s="T11">pers</ta>
            <ta e="T13" id="Seg_339" s="T12">adv</ta>
            <ta e="T14" id="Seg_340" s="T13">v</ta>
            <ta e="T15" id="Seg_341" s="T14">adv</ta>
            <ta e="T16" id="Seg_342" s="T15">v</ta>
            <ta e="T17" id="Seg_343" s="T16">n</ta>
            <ta e="T18" id="Seg_344" s="T17">pers</ta>
            <ta e="T19" id="Seg_345" s="T18">v</ta>
            <ta e="T20" id="Seg_346" s="T19">num</ta>
            <ta e="T21" id="Seg_347" s="T20">n</ta>
            <ta e="T22" id="Seg_348" s="T21">adv</ta>
            <ta e="T23" id="Seg_349" s="T22">conj</ta>
            <ta e="T24" id="Seg_350" s="T23">num</ta>
            <ta e="T25" id="Seg_351" s="T24">pers</ta>
            <ta e="T26" id="Seg_352" s="T25">quant</ta>
            <ta e="T27" id="Seg_353" s="T26">n</ta>
            <ta e="T28" id="Seg_354" s="T27">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_355" s="T1">pro.h:Th</ta>
            <ta e="T4" id="Seg_356" s="T3">np:L</ta>
            <ta e="T5" id="Seg_357" s="T4">pro.h:A</ta>
            <ta e="T7" id="Seg_358" s="T6">adv:G</ta>
            <ta e="T9" id="Seg_359" s="T8">np:A</ta>
            <ta e="T11" id="Seg_360" s="T10">adv:Time</ta>
            <ta e="T12" id="Seg_361" s="T11">pro:A</ta>
            <ta e="T13" id="Seg_362" s="T12">adv:G</ta>
            <ta e="T15" id="Seg_363" s="T14">adv:Time</ta>
            <ta e="T16" id="Seg_364" s="T15">0.3:A</ta>
            <ta e="T17" id="Seg_365" s="T16">np:Com</ta>
            <ta e="T18" id="Seg_366" s="T17">pro.h:A</ta>
            <ta e="T21" id="Seg_367" s="T20">np:Th</ta>
            <ta e="T25" id="Seg_368" s="T24">pro:A</ta>
            <ta e="T27" id="Seg_369" s="T26">np:G</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_370" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_371" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_372" s="T4">pro.h:S</ta>
            <ta e="T6" id="Seg_373" s="T5">v:pred</ta>
            <ta e="T9" id="Seg_374" s="T8">np:S</ta>
            <ta e="T10" id="Seg_375" s="T9">v:pred</ta>
            <ta e="T12" id="Seg_376" s="T11">pro:S</ta>
            <ta e="T14" id="Seg_377" s="T13">v:pred</ta>
            <ta e="T16" id="Seg_378" s="T15">0.3:S v:pred</ta>
            <ta e="T18" id="Seg_379" s="T17">pro.h:S</ta>
            <ta e="T19" id="Seg_380" s="T18">v:pred</ta>
            <ta e="T21" id="Seg_381" s="T20">np:O</ta>
            <ta e="T25" id="Seg_382" s="T24">pro:S</ta>
            <ta e="T28" id="Seg_383" s="T27">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T8" id="Seg_384" s="T7">RUS:gram</ta>
            <ta e="T11" id="Seg_385" s="T10">RUS:disc</ta>
            <ta e="T15" id="Seg_386" s="T14">RUS:disc</ta>
            <ta e="T22" id="Seg_387" s="T21">RUS:disc</ta>
            <ta e="T26" id="Seg_388" s="T25">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_389" s="T1">I was sitting at home.</ta>
            <ta e="T7" id="Seg_390" s="T4">I glanced onto the street.</ta>
            <ta e="T10" id="Seg_391" s="T7">A duck was passing by.</ta>
            <ta e="T14" id="Seg_392" s="T10">Then she went back.</ta>
            <ta e="T17" id="Seg_393" s="T14">Then she was walking with her ducklings.</ta>
            <ta e="T21" id="Seg_394" s="T17">I counted ten ducklings.</ta>
            <ta e="T24" id="Seg_395" s="T21">Then two more.</ta>
            <ta e="T28" id="Seg_396" s="T24">All of them went to the river.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_397" s="T1">Ich saß zu Hause.</ta>
            <ta e="T7" id="Seg_398" s="T4">Ich sah nach draußen auf die Straße.</ta>
            <ta e="T10" id="Seg_399" s="T7">Eine Ente ging vorbei.</ta>
            <ta e="T14" id="Seg_400" s="T10">Dann ging sie zurück.</ta>
            <ta e="T17" id="Seg_401" s="T14">Dann ging sie mit ihren Entenküken.</ta>
            <ta e="T21" id="Seg_402" s="T17">Ich zählte zehn Entenküken.</ta>
            <ta e="T24" id="Seg_403" s="T21">Dann noch zwei.</ta>
            <ta e="T28" id="Seg_404" s="T24">Sie alle gingen zum Fluss.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_405" s="T1">Я сидела дома.</ta>
            <ta e="T7" id="Seg_406" s="T4">Я взглянула на улицу.</ta>
            <ta e="T10" id="Seg_407" s="T7">А утка ходит.</ta>
            <ta e="T14" id="Seg_408" s="T10">Потом она назад пошла.</ta>
            <ta e="T17" id="Seg_409" s="T14">Потом она идет с утятами.</ta>
            <ta e="T21" id="Seg_410" s="T17">Я сосчитала десять утят.</ta>
            <ta e="T24" id="Seg_411" s="T21">Потом еще два.</ta>
            <ta e="T28" id="Seg_412" s="T24">Они все на реку отправились.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_413" s="T1">я сидела дома</ta>
            <ta e="T7" id="Seg_414" s="T4">я взглянула на улицу</ta>
            <ta e="T10" id="Seg_415" s="T7">а утка ходит</ta>
            <ta e="T14" id="Seg_416" s="T10">потом она назад пошла</ta>
            <ta e="T17" id="Seg_417" s="T14">потом идет с утятами</ta>
            <ta e="T21" id="Seg_418" s="T17">я сосчитала десять утят</ta>
            <ta e="T24" id="Seg_419" s="T21">потом еще два</ta>
            <ta e="T28" id="Seg_420" s="T24">они все на реку отправились</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T24" id="Seg_421" s="T21">[BrM:] 'sədə jaq' changed to 'sədəjaq'. Tentative analysis of 'sədəjaq', the last -q could be an ADVZ -k.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
