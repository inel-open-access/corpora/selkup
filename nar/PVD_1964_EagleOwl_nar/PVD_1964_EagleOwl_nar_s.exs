<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_EagleOwl_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_EagleOwl_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">44</ud-information>
            <ud-information attribute-name="# HIAT:w">30</ud-information>
            <ud-information attribute-name="# e">30</ud-information>
            <ud-information attribute-name="# HIAT:u">7</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T31" id="Seg_0" n="sc" s="T1">
               <ts e="T9" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Püalǯiga</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">nʼidernɨ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">i</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">tʼürɨn</ts>
                  <nts id="Seg_14" n="HIAT:ip">,</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_17" n="HIAT:w" s="T5">tawladdɨzʼe</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_20" n="HIAT:w" s="T6">kɨʒəppa</ts>
                  <nts id="Seg_21" n="HIAT:ip">,</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_24" n="HIAT:w" s="T7">čaɣɨlʼe</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_27" n="HIAT:w" s="T8">üːburkun</ts>
                  <nts id="Seg_28" n="HIAT:ip">.</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_31" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_33" n="HIAT:w" s="T9">Man</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_36" n="HIAT:w" s="T10">tatawa</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_39" n="HIAT:w" s="T11">kocʼcʼiwanan</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_42" n="HIAT:w" s="T12">i</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_45" n="HIAT:w" s="T13">quronnan</ts>
                  <nts id="Seg_46" n="HIAT:ip">.</nts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_49" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_51" n="HIAT:w" s="T14">Tʼaran</ts>
                  <nts id="Seg_52" n="HIAT:ip">:</nts>
                  <nts id="Seg_53" n="HIAT:ip">“</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_55" n="HIAT:ip">“</nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">Qajda</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">čaɣɨːn</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_64" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">Qwallutu</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">mannɨbulutu</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">qaj</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">čaɣɨtda</ts>
                  <nts id="Seg_76" n="HIAT:ip">.</nts>
                  <nts id="Seg_77" n="HIAT:ip">”</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_80" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">Qwannaftə</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_86" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_88" n="HIAT:w" s="T22">Natʼet</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_91" n="HIAT:w" s="T23">tülʼe</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">medautə</ts>
                  <nts id="Seg_95" n="HIAT:ip">,</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">a</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_101" n="HIAT:w" s="T26">püalǯiga</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_104" n="HIAT:w" s="T27">wazeːdʼzʼan</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_108" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_110" n="HIAT:w" s="T28">Tep</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_113" n="HIAT:w" s="T29">mazɨm</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_116" n="HIAT:w" s="T30">kɨtʼölǯiba</ts>
                  <nts id="Seg_117" n="HIAT:ip">.</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T31" id="Seg_119" n="sc" s="T1">
               <ts e="T2" id="Seg_121" n="e" s="T1">Püalǯiga </ts>
               <ts e="T3" id="Seg_123" n="e" s="T2">nʼidernɨ </ts>
               <ts e="T4" id="Seg_125" n="e" s="T3">i </ts>
               <ts e="T5" id="Seg_127" n="e" s="T4">tʼürɨn, </ts>
               <ts e="T6" id="Seg_129" n="e" s="T5">tawladdɨzʼe </ts>
               <ts e="T7" id="Seg_131" n="e" s="T6">kɨʒəppa, </ts>
               <ts e="T8" id="Seg_133" n="e" s="T7">čaɣɨlʼe </ts>
               <ts e="T9" id="Seg_135" n="e" s="T8">üːburkun. </ts>
               <ts e="T10" id="Seg_137" n="e" s="T9">Man </ts>
               <ts e="T11" id="Seg_139" n="e" s="T10">tatawa </ts>
               <ts e="T12" id="Seg_141" n="e" s="T11">kocʼcʼiwanan </ts>
               <ts e="T13" id="Seg_143" n="e" s="T12">i </ts>
               <ts e="T14" id="Seg_145" n="e" s="T13">quronnan. </ts>
               <ts e="T15" id="Seg_147" n="e" s="T14">Tʼaran:“ </ts>
               <ts e="T16" id="Seg_149" n="e" s="T15">“Qajda </ts>
               <ts e="T17" id="Seg_151" n="e" s="T16">čaɣɨːn. </ts>
               <ts e="T18" id="Seg_153" n="e" s="T17">Qwallutu </ts>
               <ts e="T19" id="Seg_155" n="e" s="T18">mannɨbulutu </ts>
               <ts e="T20" id="Seg_157" n="e" s="T19">qaj </ts>
               <ts e="T21" id="Seg_159" n="e" s="T20">čaɣɨtda.” </ts>
               <ts e="T22" id="Seg_161" n="e" s="T21">Qwannaftə. </ts>
               <ts e="T23" id="Seg_163" n="e" s="T22">Natʼet </ts>
               <ts e="T24" id="Seg_165" n="e" s="T23">tülʼe </ts>
               <ts e="T25" id="Seg_167" n="e" s="T24">medautə, </ts>
               <ts e="T26" id="Seg_169" n="e" s="T25">a </ts>
               <ts e="T27" id="Seg_171" n="e" s="T26">püalǯiga </ts>
               <ts e="T28" id="Seg_173" n="e" s="T27">wazeːdʼzʼan. </ts>
               <ts e="T29" id="Seg_175" n="e" s="T28">Tep </ts>
               <ts e="T30" id="Seg_177" n="e" s="T29">mazɨm </ts>
               <ts e="T31" id="Seg_179" n="e" s="T30">kɨtʼölǯiba. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T9" id="Seg_180" s="T1">PVD_1964_EagleOwl_nar.001 (001.001)</ta>
            <ta e="T14" id="Seg_181" s="T9">PVD_1964_EagleOwl_nar.002 (001.002)</ta>
            <ta e="T17" id="Seg_182" s="T14">PVD_1964_EagleOwl_nar.003 (001.003)</ta>
            <ta e="T21" id="Seg_183" s="T17">PVD_1964_EagleOwl_nar.004 (001.004)</ta>
            <ta e="T22" id="Seg_184" s="T21">PVD_1964_EagleOwl_nar.005 (001.005)</ta>
            <ta e="T28" id="Seg_185" s="T22">PVD_1964_EagleOwl_nar.006 (001.006)</ta>
            <ta e="T31" id="Seg_186" s="T28">PVD_1964_EagleOwl_nar.007 (001.007)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T9" id="Seg_187" s="T1">пӱ′аlджига ′нʼидерны(н) и ′тʼӱры(у)н, таw(у)′ладдызʼе кы′жъппа, ′тшаɣылʼе ′ӱ̄бу(е)ркун.</ta>
            <ta e="T14" id="Seg_188" s="T9">ман та ′тава ′коцʼцʼи′ванан и kу′роннан.</ta>
            <ta e="T17" id="Seg_189" s="T14">тʼа′ран: kай′да тша′ɣы̄н.</ta>
            <ta e="T21" id="Seg_190" s="T17">kwаллу′ту ′манныб̂улу′ту kай тшаɣытда.</ta>
            <ta e="T22" id="Seg_191" s="T21">kwа′ннафт̂ъ.</ta>
            <ta e="T28" id="Seg_192" s="T22">на′тʼет ′тӱлʼе ме′даутъ, а пӱаlджига ′ва′зе̄дʼзʼан.</ta>
            <ta e="T31" id="Seg_193" s="T28">теп ма′зым кытʼӧlджиб̂а.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T9" id="Seg_194" s="T1">püalǯiga nʼidernɨ(n) i tʼürɨ(u)n, taw(u)laddɨzʼe kɨʒəppa, tšaɣɨlʼe üːbu(e)rkun.</ta>
            <ta e="T14" id="Seg_195" s="T9">man ta tawa kocʼcʼiwanan i quronnan.</ta>
            <ta e="T17" id="Seg_196" s="T14">tʼaran: qajda tšaɣɨːn.</ta>
            <ta e="T21" id="Seg_197" s="T17">qwallutu mannɨb̂ulutu qaj tšaɣɨtda.</ta>
            <ta e="T22" id="Seg_198" s="T21">qwannaft̂ə.</ta>
            <ta e="T28" id="Seg_199" s="T22">natʼet tülʼe medautə, a püalǯiga wazeːdʼzʼan.</ta>
            <ta e="T31" id="Seg_200" s="T28">tep mazɨm kɨtʼölǯib̂a.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T9" id="Seg_201" s="T1">Püalǯiga nʼidernɨ i tʼürɨn, tawladdɨzʼe kɨʒəppa, čaɣɨlʼe üːburkun. </ta>
            <ta e="T14" id="Seg_202" s="T9">Man tatawa kocʼcʼiwanan i quronnan. </ta>
            <ta e="T17" id="Seg_203" s="T14">Tʼaran:“ “Qajda čaɣɨːn. </ta>
            <ta e="T21" id="Seg_204" s="T17">Qwallutu mannɨbulutu qaj čaɣɨtda.” </ta>
            <ta e="T22" id="Seg_205" s="T21">Qwannaftə. </ta>
            <ta e="T28" id="Seg_206" s="T22">Natʼet tülʼe medautə, a püalǯiga wazeːdʼzʼan. </ta>
            <ta e="T31" id="Seg_207" s="T28">Tep mazɨm kɨtʼölǯiba. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_208" s="T1">püalǯiga</ta>
            <ta e="T3" id="Seg_209" s="T2">nʼider-nɨ</ta>
            <ta e="T4" id="Seg_210" s="T3">i</ta>
            <ta e="T5" id="Seg_211" s="T4">tʼürɨ-n</ta>
            <ta e="T6" id="Seg_212" s="T5">taw-la-ddɨ-zʼe</ta>
            <ta e="T7" id="Seg_213" s="T6">kɨʒəppa</ta>
            <ta e="T8" id="Seg_214" s="T7">čaɣɨ-lʼe</ta>
            <ta e="T9" id="Seg_215" s="T8">üːbu-r-ku-n</ta>
            <ta e="T10" id="Seg_216" s="T9">man</ta>
            <ta e="T11" id="Seg_217" s="T10">tatawa</ta>
            <ta e="T12" id="Seg_218" s="T11">kocʼcʼiwan-a-n</ta>
            <ta e="T13" id="Seg_219" s="T12">i</ta>
            <ta e="T14" id="Seg_220" s="T13">qur-on-na-n</ta>
            <ta e="T15" id="Seg_221" s="T14">tʼara-n</ta>
            <ta e="T16" id="Seg_222" s="T15">qaj-da</ta>
            <ta e="T17" id="Seg_223" s="T16">čaɣɨː-n</ta>
            <ta e="T18" id="Seg_224" s="T17">qwal-lu-tu</ta>
            <ta e="T19" id="Seg_225" s="T18">mannɨ-bu-lu-tu</ta>
            <ta e="T20" id="Seg_226" s="T19">qaj</ta>
            <ta e="T21" id="Seg_227" s="T20">čaɣɨ-tda</ta>
            <ta e="T22" id="Seg_228" s="T21">qwan-na-ftə</ta>
            <ta e="T23" id="Seg_229" s="T22">natʼe-t</ta>
            <ta e="T24" id="Seg_230" s="T23">tü-lʼe</ta>
            <ta e="T25" id="Seg_231" s="T24">meda-utə</ta>
            <ta e="T26" id="Seg_232" s="T25">a</ta>
            <ta e="T27" id="Seg_233" s="T26">püalǯiga</ta>
            <ta e="T28" id="Seg_234" s="T27">wazeː-dʼzʼa-n</ta>
            <ta e="T29" id="Seg_235" s="T28">tep</ta>
            <ta e="T30" id="Seg_236" s="T29">mazɨm</ta>
            <ta e="T31" id="Seg_237" s="T30">kɨtʼö-lǯi-ba</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_238" s="T1">piolǯiga</ta>
            <ta e="T3" id="Seg_239" s="T2">nʼider-nɨ</ta>
            <ta e="T4" id="Seg_240" s="T3">i</ta>
            <ta e="T5" id="Seg_241" s="T4">tʼüru-n</ta>
            <ta e="T6" id="Seg_242" s="T5">taw-la-tə-se</ta>
            <ta e="T7" id="Seg_243" s="T6">kɨʒɨbu</ta>
            <ta e="T8" id="Seg_244" s="T7">čaɣɨ-le</ta>
            <ta e="T9" id="Seg_245" s="T8">übɨ-r-ku-n</ta>
            <ta e="T10" id="Seg_246" s="T9">man</ta>
            <ta e="T11" id="Seg_247" s="T10">tatawa</ta>
            <ta e="T12" id="Seg_248" s="T11">kɨcʼwat-nɨ-ŋ</ta>
            <ta e="T13" id="Seg_249" s="T12">i</ta>
            <ta e="T14" id="Seg_250" s="T13">kur-ol-nɨ-ŋ</ta>
            <ta e="T15" id="Seg_251" s="T14">tʼärɨ-ŋ</ta>
            <ta e="T16" id="Seg_252" s="T15">qaj-ta</ta>
            <ta e="T17" id="Seg_253" s="T16">čaɣɨ-n</ta>
            <ta e="T18" id="Seg_254" s="T17">qwan-lä-un</ta>
            <ta e="T19" id="Seg_255" s="T18">*mantɨ-mbɨ-lä-un</ta>
            <ta e="T20" id="Seg_256" s="T19">qaj</ta>
            <ta e="T21" id="Seg_257" s="T20">čaɣɨ-ntɨ</ta>
            <ta e="T22" id="Seg_258" s="T21">qwan-nɨ-un</ta>
            <ta e="T23" id="Seg_259" s="T22">*natʼe-n</ta>
            <ta e="T24" id="Seg_260" s="T23">tüː-le</ta>
            <ta e="T25" id="Seg_261" s="T24">medə-un</ta>
            <ta e="T26" id="Seg_262" s="T25">a</ta>
            <ta e="T27" id="Seg_263" s="T26">piolǯiga</ta>
            <ta e="T28" id="Seg_264" s="T27">wazʼe-ǯə-n</ta>
            <ta e="T29" id="Seg_265" s="T28">täp</ta>
            <ta e="T30" id="Seg_266" s="T29">mazɨm</ta>
            <ta e="T31" id="Seg_267" s="T30">*qɨčo-lǯi-mbɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_268" s="T1">eagle.owl.[NOM]</ta>
            <ta e="T3" id="Seg_269" s="T2">%whistle-CO.[3SG.S]</ta>
            <ta e="T4" id="Seg_270" s="T3">and</ta>
            <ta e="T5" id="Seg_271" s="T4">cry-3SG.S</ta>
            <ta e="T6" id="Seg_272" s="T5">tooth-PL-3SG-COM</ta>
            <ta e="T7" id="Seg_273" s="T6">chatter.with.teeth.[3SG.S]</ta>
            <ta e="T8" id="Seg_274" s="T7">groan-CVB</ta>
            <ta e="T9" id="Seg_275" s="T8">begin-DRV-HAB-3SG.S</ta>
            <ta e="T10" id="Seg_276" s="T9">I.NOM</ta>
            <ta e="T11" id="Seg_277" s="T10">to.such.extent</ta>
            <ta e="T12" id="Seg_278" s="T11">get.scared-CO-1SG.S</ta>
            <ta e="T13" id="Seg_279" s="T12">and</ta>
            <ta e="T14" id="Seg_280" s="T13">run-MOM-CO-1SG.S</ta>
            <ta e="T15" id="Seg_281" s="T14">say-1SG.S</ta>
            <ta e="T16" id="Seg_282" s="T15">what-INDEF.[NOM]</ta>
            <ta e="T17" id="Seg_283" s="T16">groan-3SG.S</ta>
            <ta e="T18" id="Seg_284" s="T17">leave-OPT-1PL</ta>
            <ta e="T19" id="Seg_285" s="T18">look-DUR-OPT-1PL</ta>
            <ta e="T20" id="Seg_286" s="T19">what.[NOM]</ta>
            <ta e="T21" id="Seg_287" s="T20">groan-INFER.[3SG.S]</ta>
            <ta e="T22" id="Seg_288" s="T21">leave-CO-1PL</ta>
            <ta e="T23" id="Seg_289" s="T22">there-ADV.LOC</ta>
            <ta e="T24" id="Seg_290" s="T23">come-CVB</ta>
            <ta e="T25" id="Seg_291" s="T24">achieve-1PL</ta>
            <ta e="T26" id="Seg_292" s="T25">and</ta>
            <ta e="T27" id="Seg_293" s="T26">eagle.owl.[NOM]</ta>
            <ta e="T28" id="Seg_294" s="T27">fly-DRV-3SG.S</ta>
            <ta e="T29" id="Seg_295" s="T28">(s)he.[NOM]</ta>
            <ta e="T30" id="Seg_296" s="T29">I.ACC</ta>
            <ta e="T31" id="Seg_297" s="T30">frighten-TR-PST.NAR.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_298" s="T1">филин.[NOM]</ta>
            <ta e="T3" id="Seg_299" s="T2">%свистеть-CO.[3SG.S]</ta>
            <ta e="T4" id="Seg_300" s="T3">и</ta>
            <ta e="T5" id="Seg_301" s="T4">плакать-3SG.S</ta>
            <ta e="T6" id="Seg_302" s="T5">зуб-PL-3SG-COM</ta>
            <ta e="T7" id="Seg_303" s="T6">стучать.зубами.[3SG.S]</ta>
            <ta e="T8" id="Seg_304" s="T7">стонать-CVB</ta>
            <ta e="T9" id="Seg_305" s="T8">начать-DRV-HAB-3SG.S</ta>
            <ta e="T10" id="Seg_306" s="T9">я.NOM</ta>
            <ta e="T11" id="Seg_307" s="T10">до.того</ta>
            <ta e="T12" id="Seg_308" s="T11">испугаться-CO-1SG.S</ta>
            <ta e="T13" id="Seg_309" s="T12">и</ta>
            <ta e="T14" id="Seg_310" s="T13">бегать-MOM-CO-1SG.S</ta>
            <ta e="T15" id="Seg_311" s="T14">сказать-1SG.S</ta>
            <ta e="T16" id="Seg_312" s="T15">что-INDEF.[NOM]</ta>
            <ta e="T17" id="Seg_313" s="T16">стонать-3SG.S</ta>
            <ta e="T18" id="Seg_314" s="T17">отправиться-OPT-1PL</ta>
            <ta e="T19" id="Seg_315" s="T18">посмотреть-DUR-OPT-1PL</ta>
            <ta e="T20" id="Seg_316" s="T19">что.[NOM]</ta>
            <ta e="T21" id="Seg_317" s="T20">стонать-INFER.[3SG.S]</ta>
            <ta e="T22" id="Seg_318" s="T21">отправиться-CO-1PL</ta>
            <ta e="T23" id="Seg_319" s="T22">туда-ADV.LOC</ta>
            <ta e="T24" id="Seg_320" s="T23">прийти-CVB</ta>
            <ta e="T25" id="Seg_321" s="T24">достичь-1PL</ta>
            <ta e="T26" id="Seg_322" s="T25">а</ta>
            <ta e="T27" id="Seg_323" s="T26">филин.[NOM]</ta>
            <ta e="T28" id="Seg_324" s="T27">полететь-DRV-3SG.S</ta>
            <ta e="T29" id="Seg_325" s="T28">он(а).[NOM]</ta>
            <ta e="T30" id="Seg_326" s="T29">я.ACC</ta>
            <ta e="T31" id="Seg_327" s="T30">пугать-TR-PST.NAR.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_328" s="T1">n.[n:case]</ta>
            <ta e="T3" id="Seg_329" s="T2">v-v:ins.[v:pn]</ta>
            <ta e="T4" id="Seg_330" s="T3">conj</ta>
            <ta e="T5" id="Seg_331" s="T4">v-v:pn</ta>
            <ta e="T6" id="Seg_332" s="T5">n-n:num-n:poss-n:case</ta>
            <ta e="T7" id="Seg_333" s="T6">v.[v:pn]</ta>
            <ta e="T8" id="Seg_334" s="T7">v-v&gt;adv</ta>
            <ta e="T9" id="Seg_335" s="T8">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T10" id="Seg_336" s="T9">pers</ta>
            <ta e="T11" id="Seg_337" s="T10">adv</ta>
            <ta e="T12" id="Seg_338" s="T11">v-v:ins-v:pn</ta>
            <ta e="T13" id="Seg_339" s="T12">conj</ta>
            <ta e="T14" id="Seg_340" s="T13">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T15" id="Seg_341" s="T14">v-v:pn</ta>
            <ta e="T16" id="Seg_342" s="T15">interrog-clit.[n:case]</ta>
            <ta e="T17" id="Seg_343" s="T16">v-v:pn</ta>
            <ta e="T18" id="Seg_344" s="T17">v-v:mood-v:pn</ta>
            <ta e="T19" id="Seg_345" s="T18">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T20" id="Seg_346" s="T19">interrog.[n:case]</ta>
            <ta e="T21" id="Seg_347" s="T20">v-v:mood.[v:pn]</ta>
            <ta e="T22" id="Seg_348" s="T21">v-v:ins-v:pn</ta>
            <ta e="T23" id="Seg_349" s="T22">adv-adv:case</ta>
            <ta e="T24" id="Seg_350" s="T23">v-v&gt;adv</ta>
            <ta e="T25" id="Seg_351" s="T24">v-v:pn</ta>
            <ta e="T26" id="Seg_352" s="T25">conj</ta>
            <ta e="T27" id="Seg_353" s="T26">n.[n:case]</ta>
            <ta e="T28" id="Seg_354" s="T27">v-v&gt;v-v:pn</ta>
            <ta e="T29" id="Seg_355" s="T28">pers.[n:case]</ta>
            <ta e="T30" id="Seg_356" s="T29">pers</ta>
            <ta e="T31" id="Seg_357" s="T30">v-v&gt;v-v:tense.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_358" s="T1">n</ta>
            <ta e="T3" id="Seg_359" s="T2">v</ta>
            <ta e="T4" id="Seg_360" s="T3">conj</ta>
            <ta e="T5" id="Seg_361" s="T4">v</ta>
            <ta e="T6" id="Seg_362" s="T5">n</ta>
            <ta e="T7" id="Seg_363" s="T6">v</ta>
            <ta e="T8" id="Seg_364" s="T7">adv</ta>
            <ta e="T9" id="Seg_365" s="T8">v</ta>
            <ta e="T10" id="Seg_366" s="T9">pers</ta>
            <ta e="T11" id="Seg_367" s="T10">adv</ta>
            <ta e="T12" id="Seg_368" s="T11">v</ta>
            <ta e="T13" id="Seg_369" s="T12">conj</ta>
            <ta e="T14" id="Seg_370" s="T13">v</ta>
            <ta e="T15" id="Seg_371" s="T14">v</ta>
            <ta e="T16" id="Seg_372" s="T15">pro</ta>
            <ta e="T17" id="Seg_373" s="T16">v</ta>
            <ta e="T18" id="Seg_374" s="T17">v</ta>
            <ta e="T19" id="Seg_375" s="T18">v</ta>
            <ta e="T20" id="Seg_376" s="T19">interrog</ta>
            <ta e="T21" id="Seg_377" s="T20">v</ta>
            <ta e="T22" id="Seg_378" s="T21">v</ta>
            <ta e="T23" id="Seg_379" s="T22">adv</ta>
            <ta e="T24" id="Seg_380" s="T23">adv</ta>
            <ta e="T25" id="Seg_381" s="T24">v</ta>
            <ta e="T26" id="Seg_382" s="T25">conj</ta>
            <ta e="T27" id="Seg_383" s="T26">n</ta>
            <ta e="T28" id="Seg_384" s="T27">v</ta>
            <ta e="T29" id="Seg_385" s="T28">pers</ta>
            <ta e="T30" id="Seg_386" s="T29">pers</ta>
            <ta e="T31" id="Seg_387" s="T30">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_388" s="T1">np:A</ta>
            <ta e="T5" id="Seg_389" s="T4">0.3:A</ta>
            <ta e="T6" id="Seg_390" s="T5">0.3:Poss np:Ins</ta>
            <ta e="T7" id="Seg_391" s="T6">0.3:A</ta>
            <ta e="T9" id="Seg_392" s="T8">0.3:A</ta>
            <ta e="T10" id="Seg_393" s="T9">pro.h:E</ta>
            <ta e="T14" id="Seg_394" s="T13">0.1.h:A</ta>
            <ta e="T15" id="Seg_395" s="T14">0.1.h:A</ta>
            <ta e="T16" id="Seg_396" s="T15">pro:A</ta>
            <ta e="T18" id="Seg_397" s="T17">0.1.h:A</ta>
            <ta e="T19" id="Seg_398" s="T18">0.1.h:A</ta>
            <ta e="T20" id="Seg_399" s="T19">pro.h:A</ta>
            <ta e="T22" id="Seg_400" s="T21">0.1.h:A</ta>
            <ta e="T23" id="Seg_401" s="T22">adv:G</ta>
            <ta e="T25" id="Seg_402" s="T24">0.1.h:A</ta>
            <ta e="T27" id="Seg_403" s="T26">np:A</ta>
            <ta e="T29" id="Seg_404" s="T28">pro:A</ta>
            <ta e="T30" id="Seg_405" s="T29">pro.h:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_406" s="T1">np:S</ta>
            <ta e="T3" id="Seg_407" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_408" s="T4">0.3:S v:pred</ta>
            <ta e="T7" id="Seg_409" s="T6">0.3:S v:pred</ta>
            <ta e="T9" id="Seg_410" s="T8">0.3:S v:pred</ta>
            <ta e="T10" id="Seg_411" s="T9">pro.h:S</ta>
            <ta e="T12" id="Seg_412" s="T11">v:pred</ta>
            <ta e="T14" id="Seg_413" s="T13">0.1.h:S v:pred</ta>
            <ta e="T15" id="Seg_414" s="T14">0.1.h:S v:pred</ta>
            <ta e="T16" id="Seg_415" s="T15">pro:S</ta>
            <ta e="T17" id="Seg_416" s="T16">v:pred</ta>
            <ta e="T18" id="Seg_417" s="T17">0.1.h:S v:pred</ta>
            <ta e="T19" id="Seg_418" s="T18">0.1.h:S v:pred</ta>
            <ta e="T21" id="Seg_419" s="T19">s:compl</ta>
            <ta e="T22" id="Seg_420" s="T21">0.1.h:S v:pred</ta>
            <ta e="T24" id="Seg_421" s="T23">s:temp</ta>
            <ta e="T25" id="Seg_422" s="T24">0.1.h:S v:pred</ta>
            <ta e="T27" id="Seg_423" s="T26">np:S</ta>
            <ta e="T28" id="Seg_424" s="T27">v:pred</ta>
            <ta e="T29" id="Seg_425" s="T28">pro:S</ta>
            <ta e="T30" id="Seg_426" s="T29">pro.h:O</ta>
            <ta e="T31" id="Seg_427" s="T30">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_428" s="T3">RUS:gram</ta>
            <ta e="T11" id="Seg_429" s="T10">RUS:core</ta>
            <ta e="T13" id="Seg_430" s="T12">RUS:gram</ta>
            <ta e="T16" id="Seg_431" s="T15">RUS:gram</ta>
            <ta e="T26" id="Seg_432" s="T25">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T9" id="Seg_433" s="T1">An eagle-owl whistles and cries, chatter with his teeth, begins to groan.</ta>
            <ta e="T14" id="Seg_434" s="T9">I was so frightened, and I ran away.</ta>
            <ta e="T17" id="Seg_435" s="T14">I said: “Someone is groaning.</ta>
            <ta e="T21" id="Seg_436" s="T17">Let's go and have a look, who is groaning.”</ta>
            <ta e="T22" id="Seg_437" s="T21">We went.</ta>
            <ta e="T28" id="Seg_438" s="T22">We came there, and the eagle-owl flew away.</ta>
            <ta e="T31" id="Seg_439" s="T28">It frightened me.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T9" id="Seg_440" s="T1">Ein Uhu pfeift und schreit, klappert mit den Zähnen, beginnt zu stöhnen.</ta>
            <ta e="T14" id="Seg_441" s="T9">Ich bekam solche Angst und rannte weg.</ta>
            <ta e="T17" id="Seg_442" s="T14">Ich sagte: "Jemand stöhnt.</ta>
            <ta e="T21" id="Seg_443" s="T17">Lass uns gehen und nachsehen, wer stöhnt."</ta>
            <ta e="T22" id="Seg_444" s="T21">Wir gingen.</ta>
            <ta e="T28" id="Seg_445" s="T22">Wir kamen dorthin und der Uhu flog davon.</ta>
            <ta e="T31" id="Seg_446" s="T28">Er machte mir Angst.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T9" id="Seg_447" s="T1">Филин свистит и плачет, зубами скрипит, стонать начинает.</ta>
            <ta e="T14" id="Seg_448" s="T9">Я до того испугалась и убежала.</ta>
            <ta e="T17" id="Seg_449" s="T14">Я говорю: “Кто-то стонет.</ta>
            <ta e="T21" id="Seg_450" s="T17">Пойдем посмотрим, кто стонет”.</ta>
            <ta e="T22" id="Seg_451" s="T21">Мы пошли.</ta>
            <ta e="T28" id="Seg_452" s="T22">Туда пришли, а филин полетел.</ta>
            <ta e="T31" id="Seg_453" s="T28">Он меня испугал.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T9" id="Seg_454" s="T1">филин свистит плачет зубами скрипит стонать начинает</ta>
            <ta e="T14" id="Seg_455" s="T9">я до того испугалась и убежала</ta>
            <ta e="T17" id="Seg_456" s="T14">говорю кто-то стонет</ta>
            <ta e="T21" id="Seg_457" s="T17">пойдем посмотрим кто стонет</ta>
            <ta e="T22" id="Seg_458" s="T21">пошли</ta>
            <ta e="T28" id="Seg_459" s="T22">туда пришли а филин полетел</ta>
            <ta e="T31" id="Seg_460" s="T28">он меня испугал</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T9" id="Seg_461" s="T1">[KuAI:] Variants: 'nʼidernɨn', 'tʼürun', 'tauladdɨzʼe', 'üːberkun'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
