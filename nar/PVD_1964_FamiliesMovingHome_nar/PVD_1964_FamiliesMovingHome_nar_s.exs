<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_FamiliesMovingHome_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_FamiliesMovingHome_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">29</ud-information>
            <ud-information attribute-name="# HIAT:w">23</ud-information>
            <ud-information attribute-name="# e">23</ud-information>
            <ud-information attribute-name="# HIAT:u">6</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T24" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Tamdʼel</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">qula</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">tʼütdattə</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Kocʼin</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">tʼüattə</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">tawtʼedelʼe</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">Menan</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">toʒa</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">sɨt</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">semja</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">tʼümatdə</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">mʼäɣuntu</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_47" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">Menan</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">tapbon</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">koːcʼin</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">qula</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">qudattə</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_65" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">Qudnäs</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">qopbat</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_74" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">A</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">tätdɨn</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">koːcʼin</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">qopbattə</ts>
                  <nts id="Seg_86" n="HIAT:ip">.</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T24" id="Seg_88" n="sc" s="T1">
               <ts e="T2" id="Seg_90" n="e" s="T1">Tamdʼel </ts>
               <ts e="T3" id="Seg_92" n="e" s="T2">qula </ts>
               <ts e="T4" id="Seg_94" n="e" s="T3">tʼütdattə. </ts>
               <ts e="T5" id="Seg_96" n="e" s="T4">Kocʼin </ts>
               <ts e="T6" id="Seg_98" n="e" s="T5">tʼüattə </ts>
               <ts e="T7" id="Seg_100" n="e" s="T6">tawtʼedelʼe. </ts>
               <ts e="T8" id="Seg_102" n="e" s="T7">Menan </ts>
               <ts e="T9" id="Seg_104" n="e" s="T8">toʒa </ts>
               <ts e="T10" id="Seg_106" n="e" s="T9">sɨt </ts>
               <ts e="T11" id="Seg_108" n="e" s="T10">semja </ts>
               <ts e="T12" id="Seg_110" n="e" s="T11">tʼümatdə </ts>
               <ts e="T13" id="Seg_112" n="e" s="T12">mʼäɣuntu. </ts>
               <ts e="T14" id="Seg_114" n="e" s="T13">Menan </ts>
               <ts e="T15" id="Seg_116" n="e" s="T14">tapbon </ts>
               <ts e="T16" id="Seg_118" n="e" s="T15">koːcʼin </ts>
               <ts e="T17" id="Seg_120" n="e" s="T16">qula </ts>
               <ts e="T18" id="Seg_122" n="e" s="T17">qudattə. </ts>
               <ts e="T19" id="Seg_124" n="e" s="T18">Qudnäs </ts>
               <ts e="T20" id="Seg_126" n="e" s="T19">qopbat. </ts>
               <ts e="T21" id="Seg_128" n="e" s="T20">A </ts>
               <ts e="T22" id="Seg_130" n="e" s="T21">tätdɨn </ts>
               <ts e="T23" id="Seg_132" n="e" s="T22">koːcʼin </ts>
               <ts e="T24" id="Seg_134" n="e" s="T23">qopbattə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_135" s="T1">PVD_1964_FamiliesMovingHome_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_136" s="T4">PVD_1964_FamiliesMovingHome_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_137" s="T7">PVD_1964_FamiliesMovingHome_nar.003 (001.003)</ta>
            <ta e="T18" id="Seg_138" s="T13">PVD_1964_FamiliesMovingHome_nar.004 (001.004)</ta>
            <ta e="T20" id="Seg_139" s="T18">PVD_1964_FamiliesMovingHome_nar.005 (001.005)</ta>
            <ta e="T24" id="Seg_140" s="T20">PVD_1964_FamiliesMovingHome_nar.006 (001.006)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_141" s="T1">там′дʼел kу′ла ′тʼӱт‵даттъ.</ta>
            <ta e="T7" id="Seg_142" s="T4">′коцʼин ′тʼӱаттъ таw(ф)′тʼеде̨(ы)лʼе.</ta>
            <ta e="T13" id="Seg_143" s="T7">ме′нан ′тожа сыт ′семjа ′тʼӱматдъ мʼӓ′ɣунту.</ta>
            <ta e="T18" id="Seg_144" s="T13">ме′нан тап′бон ′ко̄цʼин kу′lа ′kудаттъ.</ta>
            <ta e="T20" id="Seg_145" s="T18">kуд′нӓс kоп′бат.</ta>
            <ta e="T24" id="Seg_146" s="T20">а тӓт′дын ′ко̄цʼин kоп′баттъ.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_147" s="T1">tamdʼel qula tʼütdattə.</ta>
            <ta e="T7" id="Seg_148" s="T4">kocʼin tʼüattə taw(f)tʼede(ɨ)lʼe.</ta>
            <ta e="T13" id="Seg_149" s="T7">menan toʒa sɨt semja tʼümatdə mʼäɣuntu.</ta>
            <ta e="T18" id="Seg_150" s="T13">menan tapbon koːcʼin qula qudattə.</ta>
            <ta e="T20" id="Seg_151" s="T18">qudnäs qopbat.</ta>
            <ta e="T24" id="Seg_152" s="T20">a tätdɨn koːcʼin qopbattə.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_153" s="T1">Tamdʼel qula tʼütdattə. </ta>
            <ta e="T7" id="Seg_154" s="T4">Kocʼin tʼüattə tawtʼedelʼe. </ta>
            <ta e="T13" id="Seg_155" s="T7">Menan toʒa sɨt semja tʼümatdə mʼäɣuntu. </ta>
            <ta e="T18" id="Seg_156" s="T13">Menan tapbon koːcʼin qula qudattə. </ta>
            <ta e="T20" id="Seg_157" s="T18">Qudnäs qopbat. </ta>
            <ta e="T24" id="Seg_158" s="T20">A tätdɨn koːcʼin qopbattə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_159" s="T1">tam-dʼel</ta>
            <ta e="T3" id="Seg_160" s="T2">qu-la</ta>
            <ta e="T4" id="Seg_161" s="T3">tʼü-tda-ttə</ta>
            <ta e="T5" id="Seg_162" s="T4">kocʼi-n</ta>
            <ta e="T6" id="Seg_163" s="T5">tʼü-a-ttə</ta>
            <ta e="T7" id="Seg_164" s="T6">tawtʼedelʼe</ta>
            <ta e="T8" id="Seg_165" s="T7">me-nan</ta>
            <ta e="T9" id="Seg_166" s="T8">toʒa</ta>
            <ta e="T10" id="Seg_167" s="T9">sɨt</ta>
            <ta e="T11" id="Seg_168" s="T10">semja</ta>
            <ta e="T12" id="Seg_169" s="T11">tʼü-ma-tdə</ta>
            <ta e="T13" id="Seg_170" s="T12">mʼä-ɣuntu</ta>
            <ta e="T14" id="Seg_171" s="T13">me-nan</ta>
            <ta e="T15" id="Seg_172" s="T14">tap-bo-n</ta>
            <ta e="T16" id="Seg_173" s="T15">koːcʼi-n</ta>
            <ta e="T17" id="Seg_174" s="T16">qu-la</ta>
            <ta e="T18" id="Seg_175" s="T17">qu-da-ttə</ta>
            <ta e="T19" id="Seg_176" s="T18">qud-nä-s</ta>
            <ta e="T20" id="Seg_177" s="T19">qo-pba-t</ta>
            <ta e="T21" id="Seg_178" s="T20">a</ta>
            <ta e="T22" id="Seg_179" s="T21">tätdɨ-n</ta>
            <ta e="T23" id="Seg_180" s="T22">koːcʼi-n</ta>
            <ta e="T24" id="Seg_181" s="T23">qo-pba-ttə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_182" s="T1">taw-dʼel</ta>
            <ta e="T3" id="Seg_183" s="T2">qum-la</ta>
            <ta e="T4" id="Seg_184" s="T3">tüː-ntɨ-tɨn</ta>
            <ta e="T5" id="Seg_185" s="T4">koːci-ŋ</ta>
            <ta e="T6" id="Seg_186" s="T5">tüː-nɨ-tɨn</ta>
            <ta e="T7" id="Seg_187" s="T6">tawtʼidelʼe</ta>
            <ta e="T8" id="Seg_188" s="T7">me-nan</ta>
            <ta e="T9" id="Seg_189" s="T8">toʒa</ta>
            <ta e="T10" id="Seg_190" s="T9">sədə</ta>
            <ta e="T11" id="Seg_191" s="T10">semja</ta>
            <ta e="T12" id="Seg_192" s="T11">tüː-mbɨ-ntɨ</ta>
            <ta e="T13" id="Seg_193" s="T12">me-qɨntɨ</ta>
            <ta e="T14" id="Seg_194" s="T13">me-nan</ta>
            <ta e="T15" id="Seg_195" s="T14">taw-po-n</ta>
            <ta e="T16" id="Seg_196" s="T15">koːci-ŋ</ta>
            <ta e="T17" id="Seg_197" s="T16">qum-la</ta>
            <ta e="T18" id="Seg_198" s="T17">quː-ntɨ-tɨn</ta>
            <ta e="T19" id="Seg_199" s="T18">kud-näj-s</ta>
            <ta e="T20" id="Seg_200" s="T19">qo-mbɨ-t</ta>
            <ta e="T21" id="Seg_201" s="T20">a</ta>
            <ta e="T22" id="Seg_202" s="T21">tɨtʼa-n</ta>
            <ta e="T23" id="Seg_203" s="T22">koːci-ŋ</ta>
            <ta e="T24" id="Seg_204" s="T23">qo-mbɨ-tɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_205" s="T1">this-day.[NOM]</ta>
            <ta e="T3" id="Seg_206" s="T2">human.being-PL.[NOM]</ta>
            <ta e="T4" id="Seg_207" s="T3">come-INFER-3PL</ta>
            <ta e="T5" id="Seg_208" s="T4">much-ADVZ</ta>
            <ta e="T6" id="Seg_209" s="T5">come-CO-3PL</ta>
            <ta e="T7" id="Seg_210" s="T6">here</ta>
            <ta e="T8" id="Seg_211" s="T7">we-ADES</ta>
            <ta e="T9" id="Seg_212" s="T8">also</ta>
            <ta e="T10" id="Seg_213" s="T9">two</ta>
            <ta e="T11" id="Seg_214" s="T10">family.[NOM]</ta>
            <ta e="T12" id="Seg_215" s="T11">come-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T13" id="Seg_216" s="T12">we-ILL.3SG</ta>
            <ta e="T14" id="Seg_217" s="T13">we-ADES</ta>
            <ta e="T15" id="Seg_218" s="T14">this-year-ADV.LOC</ta>
            <ta e="T16" id="Seg_219" s="T15">much-ADVZ</ta>
            <ta e="T17" id="Seg_220" s="T16">human.being-PL.[NOM]</ta>
            <ta e="T18" id="Seg_221" s="T17">die-INFER-3PL</ta>
            <ta e="T19" id="Seg_222" s="T18">who.[NOM]-EMPH-NEG</ta>
            <ta e="T20" id="Seg_223" s="T19">give.birth-PST.NAR-3SG.O</ta>
            <ta e="T21" id="Seg_224" s="T20">and</ta>
            <ta e="T22" id="Seg_225" s="T21">here-ADV.LOC</ta>
            <ta e="T23" id="Seg_226" s="T22">much-ADVZ</ta>
            <ta e="T24" id="Seg_227" s="T23">give.birth-DUR-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_228" s="T1">этот-день.[NOM]</ta>
            <ta e="T3" id="Seg_229" s="T2">человек-PL.[NOM]</ta>
            <ta e="T4" id="Seg_230" s="T3">приехать-INFER-3PL</ta>
            <ta e="T5" id="Seg_231" s="T4">много-ADVZ</ta>
            <ta e="T6" id="Seg_232" s="T5">приехать-CO-3PL</ta>
            <ta e="T7" id="Seg_233" s="T6">сюда</ta>
            <ta e="T8" id="Seg_234" s="T7">мы-ADES</ta>
            <ta e="T9" id="Seg_235" s="T8">тоже</ta>
            <ta e="T10" id="Seg_236" s="T9">два</ta>
            <ta e="T11" id="Seg_237" s="T10">семья.[NOM]</ta>
            <ta e="T12" id="Seg_238" s="T11">приехать-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T13" id="Seg_239" s="T12">мы-ILL.3SG</ta>
            <ta e="T14" id="Seg_240" s="T13">мы-ADES</ta>
            <ta e="T15" id="Seg_241" s="T14">этот-год-ADV.LOC</ta>
            <ta e="T16" id="Seg_242" s="T15">много-ADVZ</ta>
            <ta e="T17" id="Seg_243" s="T16">человек-PL.[NOM]</ta>
            <ta e="T18" id="Seg_244" s="T17">умереть-INFER-3PL</ta>
            <ta e="T19" id="Seg_245" s="T18">кто.[NOM]-EMPH-NEG</ta>
            <ta e="T20" id="Seg_246" s="T19">родить-PST.NAR-3SG.O</ta>
            <ta e="T21" id="Seg_247" s="T20">а</ta>
            <ta e="T22" id="Seg_248" s="T21">сюда-ADV.LOC</ta>
            <ta e="T23" id="Seg_249" s="T22">много-ADVZ</ta>
            <ta e="T24" id="Seg_250" s="T23">родить-DUR-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_251" s="T1">dem-n.[n:case]</ta>
            <ta e="T3" id="Seg_252" s="T2">n-n:num.[n:case]</ta>
            <ta e="T4" id="Seg_253" s="T3">v-v:mood-v:pn</ta>
            <ta e="T5" id="Seg_254" s="T4">quant-adj&gt;adv</ta>
            <ta e="T6" id="Seg_255" s="T5">v-v:ins-v:pn</ta>
            <ta e="T7" id="Seg_256" s="T6">adv</ta>
            <ta e="T8" id="Seg_257" s="T7">pers-n:case</ta>
            <ta e="T9" id="Seg_258" s="T8">adv</ta>
            <ta e="T10" id="Seg_259" s="T9">num</ta>
            <ta e="T11" id="Seg_260" s="T10">n.[n:case]</ta>
            <ta e="T12" id="Seg_261" s="T11">v-v:tense-v:mood.[v:pn]</ta>
            <ta e="T13" id="Seg_262" s="T12">pers-n:case.poss</ta>
            <ta e="T14" id="Seg_263" s="T13">pers-n:case</ta>
            <ta e="T15" id="Seg_264" s="T14">dem-n-n&gt;adv</ta>
            <ta e="T16" id="Seg_265" s="T15">quant-quant&gt;adv</ta>
            <ta e="T17" id="Seg_266" s="T16">n-n:num.[n:case]</ta>
            <ta e="T18" id="Seg_267" s="T17">v-v:mood-v:pn</ta>
            <ta e="T19" id="Seg_268" s="T18">interrog.[n:case]-clit-clit</ta>
            <ta e="T20" id="Seg_269" s="T19">v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_270" s="T20">conj</ta>
            <ta e="T22" id="Seg_271" s="T21">adv-adv:case</ta>
            <ta e="T23" id="Seg_272" s="T22">quant-quant&gt;adv</ta>
            <ta e="T24" id="Seg_273" s="T23">v-v&gt;v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_274" s="T1">adv</ta>
            <ta e="T3" id="Seg_275" s="T2">n</ta>
            <ta e="T4" id="Seg_276" s="T3">v</ta>
            <ta e="T5" id="Seg_277" s="T4">quant</ta>
            <ta e="T6" id="Seg_278" s="T5">v</ta>
            <ta e="T7" id="Seg_279" s="T6">adv</ta>
            <ta e="T8" id="Seg_280" s="T7">pers</ta>
            <ta e="T9" id="Seg_281" s="T8">adv</ta>
            <ta e="T10" id="Seg_282" s="T9">num</ta>
            <ta e="T11" id="Seg_283" s="T10">n</ta>
            <ta e="T12" id="Seg_284" s="T11">v</ta>
            <ta e="T13" id="Seg_285" s="T12">pers</ta>
            <ta e="T14" id="Seg_286" s="T13">pers</ta>
            <ta e="T15" id="Seg_287" s="T14">adv</ta>
            <ta e="T16" id="Seg_288" s="T15">adv</ta>
            <ta e="T17" id="Seg_289" s="T16">n</ta>
            <ta e="T18" id="Seg_290" s="T17">pro</ta>
            <ta e="T19" id="Seg_291" s="T18">interrog</ta>
            <ta e="T20" id="Seg_292" s="T19">v</ta>
            <ta e="T21" id="Seg_293" s="T20">conj</ta>
            <ta e="T22" id="Seg_294" s="T21">adv</ta>
            <ta e="T23" id="Seg_295" s="T22">adv</ta>
            <ta e="T24" id="Seg_296" s="T23">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_297" s="T1">np:Time</ta>
            <ta e="T3" id="Seg_298" s="T2">np.h:A</ta>
            <ta e="T5" id="Seg_299" s="T4">pro.h:A</ta>
            <ta e="T7" id="Seg_300" s="T6">adv:G</ta>
            <ta e="T11" id="Seg_301" s="T10">np.h:A</ta>
            <ta e="T13" id="Seg_302" s="T12">pro.h:G</ta>
            <ta e="T14" id="Seg_303" s="T13">pro.h:L</ta>
            <ta e="T15" id="Seg_304" s="T14">adv:Time</ta>
            <ta e="T17" id="Seg_305" s="T16">np.h:P</ta>
            <ta e="T19" id="Seg_306" s="T18">pro.h:A</ta>
            <ta e="T22" id="Seg_307" s="T21">adv:L</ta>
            <ta e="T24" id="Seg_308" s="T23">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_309" s="T2">np.h:S</ta>
            <ta e="T4" id="Seg_310" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_311" s="T4">pro.h:S</ta>
            <ta e="T6" id="Seg_312" s="T5">v:pred</ta>
            <ta e="T11" id="Seg_313" s="T10">np.h:S</ta>
            <ta e="T12" id="Seg_314" s="T11">v:pred</ta>
            <ta e="T17" id="Seg_315" s="T16">np.h:S</ta>
            <ta e="T18" id="Seg_316" s="T17">v:pred</ta>
            <ta e="T19" id="Seg_317" s="T18">pro.h:S</ta>
            <ta e="T20" id="Seg_318" s="T19">v:pred</ta>
            <ta e="T24" id="Seg_319" s="T23">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T9" id="Seg_320" s="T8">RUS:core</ta>
            <ta e="T11" id="Seg_321" s="T10">RUS:cult</ta>
            <ta e="T21" id="Seg_322" s="T20">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_323" s="T1">Some people came (=moved) today.</ta>
            <ta e="T7" id="Seg_324" s="T4">Many [people] came (=moved) here.</ta>
            <ta e="T13" id="Seg_325" s="T7">We also had two families, which came (=moved) to us.</ta>
            <ta e="T18" id="Seg_326" s="T13">Many peolpe among us died this year.</ta>
            <ta e="T20" id="Seg_327" s="T18">Noone gave birth.</ta>
            <ta e="T24" id="Seg_328" s="T20">And here many gave birth.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_329" s="T1">Heute kamen (=zogen ein) einige Leute.</ta>
            <ta e="T7" id="Seg_330" s="T4">Viele [Menschen] kamen (zogen) her.</ta>
            <ta e="T13" id="Seg_331" s="T7">Wir hatten auch zwei Familien, die zu uns kamen (=zogen).</ta>
            <ta e="T18" id="Seg_332" s="T13">Dieses Jahr starben viele Leute unter uns.</ta>
            <ta e="T20" id="Seg_333" s="T18">Keiner bekam ein Kind.</ta>
            <ta e="T24" id="Seg_334" s="T20">Und hier bekamen viele Kinder.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_335" s="T1">Сегодня люди переехали.</ta>
            <ta e="T7" id="Seg_336" s="T4">Много сюда переехало.</ta>
            <ta e="T13" id="Seg_337" s="T7">У нас тоже две семьи переехали к нам.</ta>
            <ta e="T18" id="Seg_338" s="T13">У нас нынче много людей умерло.</ta>
            <ta e="T20" id="Seg_339" s="T18">Никто не родил.</ta>
            <ta e="T24" id="Seg_340" s="T20">А тут много рожают.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_341" s="T1">сегодня люди переехали опять</ta>
            <ta e="T7" id="Seg_342" s="T4">много сюда переехало</ta>
            <ta e="T13" id="Seg_343" s="T7">у нас тоже две семьи переехали к нам</ta>
            <ta e="T18" id="Seg_344" s="T13">у нас нынче много людей померли</ta>
            <ta e="T20" id="Seg_345" s="T18">никто не родился</ta>
            <ta e="T24" id="Seg_346" s="T20">а тут много рожают</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T7" id="Seg_347" s="T4">[KuAI:] Variant: 'taftʼedɨlʼe'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
