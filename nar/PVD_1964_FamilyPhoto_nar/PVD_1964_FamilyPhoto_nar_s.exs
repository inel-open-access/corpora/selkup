<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_FamilyPhoto_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_FamilyPhoto_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">64</ud-information>
            <ud-information attribute-name="# HIAT:w">49</ud-information>
            <ud-information attribute-name="# e">49</ud-information>
            <ud-information attribute-name="# HIAT:u">12</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T50" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Manan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">sobla</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">aunupqau</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">jes</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Teper</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">wes</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">qupbattə</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">Awnupkalau</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">manan</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">kartɨčkaɣɨn</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">amdat</ts>
                  <nts id="Seg_41" n="HIAT:ip">,</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">i</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">mamu</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">nɨtdɨn</ts>
                  <nts id="Seg_51" n="HIAT:ip">.</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_54" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">Oqqɨr</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">aunupkau</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">pöjzʼe</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">amda</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_69" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">A</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">ɣau</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_77" n="HIAT:w" s="T21">aunupkalau</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_80" n="HIAT:w" s="T22">batʼinkazʼe</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_83" n="HIAT:w" s="T23">amdattə</ts>
                  <nts id="Seg_84" n="HIAT:ip">.</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_87" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">Padurla</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_91" n="HIAT:ip">(</nts>
                  <ts e="T26" id="Seg_93" n="HIAT:w" s="T25">qadlan</ts>
                  <nts id="Seg_94" n="HIAT:ip">)</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">sarelǯəmɨtdɨt</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_101" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">Fartuk</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">sarɨmɨtɨt</ts>
                  <nts id="Seg_107" n="HIAT:ip">.</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_110" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_112" n="HIAT:w" s="T29">I</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">tɨmnʼau</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_118" n="HIAT:w" s="T31">näldʼzʼä</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_121" n="HIAT:w" s="T32">tobɨn</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">nɨkka</ts>
                  <nts id="Seg_125" n="HIAT:ip">.</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_128" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">Täp</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">qɨːban</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_136" n="HIAT:w" s="T36">jes</ts>
                  <nts id="Seg_137" n="HIAT:ip">.</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_140" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_142" n="HIAT:w" s="T37">Toboɣɨtdə</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_145" n="HIAT:w" s="T38">munlat</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_148" n="HIAT:w" s="T39">wes</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_151" n="HIAT:w" s="T40">aduattə</ts>
                  <nts id="Seg_152" n="HIAT:ip">.</nts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_155" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_157" n="HIAT:w" s="T41">Täp</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_160" n="HIAT:w" s="T42">qursa</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_163" n="HIAT:w" s="T43">i</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_166" n="HIAT:w" s="T44">teblanä</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_169" n="HIAT:w" s="T45">nɨːlʼadʼzʼen</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_173" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_175" n="HIAT:w" s="T46">Tabnä</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_178" n="HIAT:w" s="T47">sɨdədʼet</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_181" n="HIAT:w" s="T48">pot</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_184" n="HIAT:w" s="T49">jes</ts>
                  <nts id="Seg_185" n="HIAT:ip">.</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T50" id="Seg_187" n="sc" s="T1">
               <ts e="T2" id="Seg_189" n="e" s="T1">Manan </ts>
               <ts e="T3" id="Seg_191" n="e" s="T2">sobla </ts>
               <ts e="T4" id="Seg_193" n="e" s="T3">aunupqau </ts>
               <ts e="T5" id="Seg_195" n="e" s="T4">jes. </ts>
               <ts e="T6" id="Seg_197" n="e" s="T5">Teper </ts>
               <ts e="T7" id="Seg_199" n="e" s="T6">wes </ts>
               <ts e="T8" id="Seg_201" n="e" s="T7">qupbattə. </ts>
               <ts e="T9" id="Seg_203" n="e" s="T8">Awnupkalau </ts>
               <ts e="T10" id="Seg_205" n="e" s="T9">manan </ts>
               <ts e="T11" id="Seg_207" n="e" s="T10">kartɨčkaɣɨn </ts>
               <ts e="T12" id="Seg_209" n="e" s="T11">amdat, </ts>
               <ts e="T13" id="Seg_211" n="e" s="T12">i </ts>
               <ts e="T14" id="Seg_213" n="e" s="T13">mamu </ts>
               <ts e="T15" id="Seg_215" n="e" s="T14">nɨtdɨn. </ts>
               <ts e="T16" id="Seg_217" n="e" s="T15">Oqqɨr </ts>
               <ts e="T17" id="Seg_219" n="e" s="T16">aunupkau </ts>
               <ts e="T18" id="Seg_221" n="e" s="T17">pöjzʼe </ts>
               <ts e="T19" id="Seg_223" n="e" s="T18">amda. </ts>
               <ts e="T20" id="Seg_225" n="e" s="T19">A </ts>
               <ts e="T21" id="Seg_227" n="e" s="T20">ɣau </ts>
               <ts e="T22" id="Seg_229" n="e" s="T21">aunupkalau </ts>
               <ts e="T23" id="Seg_231" n="e" s="T22">batʼinkazʼe </ts>
               <ts e="T24" id="Seg_233" n="e" s="T23">amdattə. </ts>
               <ts e="T25" id="Seg_235" n="e" s="T24">Padurla </ts>
               <ts e="T26" id="Seg_237" n="e" s="T25">(qadlan) </ts>
               <ts e="T27" id="Seg_239" n="e" s="T26">sarelǯəmɨtdɨt. </ts>
               <ts e="T28" id="Seg_241" n="e" s="T27">Fartuk </ts>
               <ts e="T29" id="Seg_243" n="e" s="T28">sarɨmɨtɨt. </ts>
               <ts e="T30" id="Seg_245" n="e" s="T29">I </ts>
               <ts e="T31" id="Seg_247" n="e" s="T30">tɨmnʼau </ts>
               <ts e="T32" id="Seg_249" n="e" s="T31">näldʼzʼä </ts>
               <ts e="T33" id="Seg_251" n="e" s="T32">tobɨn </ts>
               <ts e="T34" id="Seg_253" n="e" s="T33">nɨkka. </ts>
               <ts e="T35" id="Seg_255" n="e" s="T34">Täp </ts>
               <ts e="T36" id="Seg_257" n="e" s="T35">qɨːban </ts>
               <ts e="T37" id="Seg_259" n="e" s="T36">jes. </ts>
               <ts e="T38" id="Seg_261" n="e" s="T37">Toboɣɨtdə </ts>
               <ts e="T39" id="Seg_263" n="e" s="T38">munlat </ts>
               <ts e="T40" id="Seg_265" n="e" s="T39">wes </ts>
               <ts e="T41" id="Seg_267" n="e" s="T40">aduattə. </ts>
               <ts e="T42" id="Seg_269" n="e" s="T41">Täp </ts>
               <ts e="T43" id="Seg_271" n="e" s="T42">qursa </ts>
               <ts e="T44" id="Seg_273" n="e" s="T43">i </ts>
               <ts e="T45" id="Seg_275" n="e" s="T44">teblanä </ts>
               <ts e="T46" id="Seg_277" n="e" s="T45">nɨːlʼadʼzʼen. </ts>
               <ts e="T47" id="Seg_279" n="e" s="T46">Tabnä </ts>
               <ts e="T48" id="Seg_281" n="e" s="T47">sɨdədʼet </ts>
               <ts e="T49" id="Seg_283" n="e" s="T48">pot </ts>
               <ts e="T50" id="Seg_285" n="e" s="T49">jes. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_286" s="T1">PVD_1964_FamilyPhoto_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_287" s="T5">PVD_1964_FamilyPhoto_nar.002 (001.002)</ta>
            <ta e="T15" id="Seg_288" s="T8">PVD_1964_FamilyPhoto_nar.003 (001.003)</ta>
            <ta e="T19" id="Seg_289" s="T15">PVD_1964_FamilyPhoto_nar.004 (001.004)</ta>
            <ta e="T24" id="Seg_290" s="T19">PVD_1964_FamilyPhoto_nar.005 (001.005)</ta>
            <ta e="T27" id="Seg_291" s="T24">PVD_1964_FamilyPhoto_nar.006 (001.006)</ta>
            <ta e="T29" id="Seg_292" s="T27">PVD_1964_FamilyPhoto_nar.007 (001.007)</ta>
            <ta e="T34" id="Seg_293" s="T29">PVD_1964_FamilyPhoto_nar.008 (001.008)</ta>
            <ta e="T37" id="Seg_294" s="T34">PVD_1964_FamilyPhoto_nar.009 (001.009)</ta>
            <ta e="T41" id="Seg_295" s="T37">PVD_1964_FamilyPhoto_nar.010 (001.010)</ta>
            <ta e="T46" id="Seg_296" s="T41">PVD_1964_FamilyPhoto_nar.011 (001.011)</ta>
            <ta e="T50" id="Seg_297" s="T46">PVD_1964_FamilyPhoto_nar.012 (001.012)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_298" s="T1">манан ′собла ау′нупkау jес.</ta>
            <ta e="T8" id="Seg_299" s="T5">тепер вес ′kупбаттъ.</ta>
            <ta e="T15" id="Seg_300" s="T8">ав′нупкалау ма′нан ′картычкаɣын ′амдат, и маму ныт′дын.</ta>
            <ta e="T19" id="Seg_301" s="T15">оk′kыр ау′нупкау пӧй′зʼе ′амда.</ta>
            <ta e="T24" id="Seg_302" s="T19">а ɣау ау′нупкалау ба′тʼинказʼе ′амдаттъ.</ta>
            <ta e="T27" id="Seg_303" s="T24">′падурла kад′лан са′реlджъмыт′дыт.</ta>
            <ta e="T29" id="Seg_304" s="T27">′фартук ′сарымытыт.</ta>
            <ta e="T34" id="Seg_305" s="T29">и тым′нʼау ′нӓlдʼзʼӓ ′тобын нык′ка.</ta>
            <ta e="T37" id="Seg_306" s="T34">тӓп kы̄бан jес.</ta>
            <ta e="T41" id="Seg_307" s="T37">тобоɣыт′дъ ′мунлат вес аду′аттъ.</ta>
            <ta e="T46" id="Seg_308" s="T41">тӓп kур′са и теб′ланӓ ′ны̄лʼадʼзʼен.</ta>
            <ta e="T50" id="Seg_309" s="T46">таб′нӓ ′сыдъ′дʼет пот jес.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_310" s="T1">manan sobla aunupqau jes.</ta>
            <ta e="T8" id="Seg_311" s="T5">teper wes qupbattə.</ta>
            <ta e="T15" id="Seg_312" s="T8">awnupkalau manan kartɨčkaɣɨn amdat, i mamu nɨtdɨn.</ta>
            <ta e="T19" id="Seg_313" s="T15">oqqɨr aunupkau pöjzʼe amda.</ta>
            <ta e="T24" id="Seg_314" s="T19">a ɣau aunupkalau batʼinkazʼe amdattə.</ta>
            <ta e="T27" id="Seg_315" s="T24">padurla qadlan sarelǯəmɨtdɨt.</ta>
            <ta e="T29" id="Seg_316" s="T27">fartuk sarɨmɨtɨt.</ta>
            <ta e="T34" id="Seg_317" s="T29">i tɨmnʼau näldʼzʼä tobɨn nɨkka.</ta>
            <ta e="T37" id="Seg_318" s="T34">täp qɨːban jes.</ta>
            <ta e="T41" id="Seg_319" s="T37">toboɣɨtdə munlat wes aduattə.</ta>
            <ta e="T46" id="Seg_320" s="T41">täp qursa i teblanä nɨːlʼadʼzʼen.</ta>
            <ta e="T50" id="Seg_321" s="T46">tabnä sɨdədʼet pot jes.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_322" s="T1">Manan sobla aunupqau jes. </ta>
            <ta e="T8" id="Seg_323" s="T5">Teper wes qupbattə. </ta>
            <ta e="T15" id="Seg_324" s="T8">Awnupkalau manan kartɨčkaɣɨn amdat, i mamu nɨtdɨn. </ta>
            <ta e="T19" id="Seg_325" s="T15">Oqqɨr aunupkau pöjzʼe amda. </ta>
            <ta e="T24" id="Seg_326" s="T19">A ɣau aunupkalau batʼinkazʼe amdattə. </ta>
            <ta e="T27" id="Seg_327" s="T24">Padurla (qadlan) sarelǯəmɨtdɨt. </ta>
            <ta e="T29" id="Seg_328" s="T27">Fartuk sarɨmɨtɨt. </ta>
            <ta e="T34" id="Seg_329" s="T29">I tɨmnʼau näldʼzʼä tobɨn nɨkka. </ta>
            <ta e="T37" id="Seg_330" s="T34">Täp qɨːban jes. </ta>
            <ta e="T41" id="Seg_331" s="T37">Toboɣɨtdə munlat wes aduattə. </ta>
            <ta e="T46" id="Seg_332" s="T41">Täp qursa i teblanä nɨːlʼadʼzʼen. </ta>
            <ta e="T50" id="Seg_333" s="T46">Tabnä sɨdədʼet pot jes. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_334" s="T1">ma-nan</ta>
            <ta e="T3" id="Seg_335" s="T2">sobla</ta>
            <ta e="T4" id="Seg_336" s="T3">aunupqa-u</ta>
            <ta e="T5" id="Seg_337" s="T4">je-s</ta>
            <ta e="T6" id="Seg_338" s="T5">teper</ta>
            <ta e="T7" id="Seg_339" s="T6">wes</ta>
            <ta e="T8" id="Seg_340" s="T7">qu-pba-ttə</ta>
            <ta e="T9" id="Seg_341" s="T8">awnupka-la-u</ta>
            <ta e="T10" id="Seg_342" s="T9">ma-nan</ta>
            <ta e="T11" id="Seg_343" s="T10">kartɨčka-ɣɨn</ta>
            <ta e="T12" id="Seg_344" s="T11">amda-t</ta>
            <ta e="T13" id="Seg_345" s="T12">i</ta>
            <ta e="T14" id="Seg_346" s="T13">mam-u</ta>
            <ta e="T15" id="Seg_347" s="T14">nɨtdɨ-n</ta>
            <ta e="T16" id="Seg_348" s="T15">oqqɨr</ta>
            <ta e="T17" id="Seg_349" s="T16">aunupka-u</ta>
            <ta e="T18" id="Seg_350" s="T17">pöj-zʼe</ta>
            <ta e="T19" id="Seg_351" s="T18">amda</ta>
            <ta e="T20" id="Seg_352" s="T19">a</ta>
            <ta e="T21" id="Seg_353" s="T20">ɣau</ta>
            <ta e="T22" id="Seg_354" s="T21">aunupka-la-u</ta>
            <ta e="T23" id="Seg_355" s="T22">batʼinka-zʼe</ta>
            <ta e="T24" id="Seg_356" s="T23">amda-ttə</ta>
            <ta e="T25" id="Seg_357" s="T24">padur-la</ta>
            <ta e="T26" id="Seg_358" s="T25">qadlan</ta>
            <ta e="T27" id="Seg_359" s="T26">sare-lǯə-mɨ-tdɨ-t</ta>
            <ta e="T28" id="Seg_360" s="T27">fartuk</ta>
            <ta e="T29" id="Seg_361" s="T28">sarɨ-mɨ-tɨt</ta>
            <ta e="T30" id="Seg_362" s="T29">i</ta>
            <ta e="T31" id="Seg_363" s="T30">tɨmnʼa-u</ta>
            <ta e="T32" id="Seg_364" s="T31">näldʼzʼä</ta>
            <ta e="T33" id="Seg_365" s="T32">tob-ɨ-n</ta>
            <ta e="T34" id="Seg_366" s="T33">nɨkka</ta>
            <ta e="T35" id="Seg_367" s="T34">täp</ta>
            <ta e="T36" id="Seg_368" s="T35">qɨːba-n</ta>
            <ta e="T37" id="Seg_369" s="T36">je-s</ta>
            <ta e="T38" id="Seg_370" s="T37">tob-o-ɣɨtdə</ta>
            <ta e="T39" id="Seg_371" s="T38">mun-la-t</ta>
            <ta e="T40" id="Seg_372" s="T39">wes</ta>
            <ta e="T41" id="Seg_373" s="T40">adu-a-ttə</ta>
            <ta e="T42" id="Seg_374" s="T41">täp</ta>
            <ta e="T43" id="Seg_375" s="T42">qur-sa</ta>
            <ta e="T44" id="Seg_376" s="T43">i</ta>
            <ta e="T45" id="Seg_377" s="T44">teb-la-nä</ta>
            <ta e="T46" id="Seg_378" s="T45">nɨː-lʼ-a-dʼzʼe-n</ta>
            <ta e="T47" id="Seg_379" s="T46">tab-nä</ta>
            <ta e="T48" id="Seg_380" s="T47">sɨdədʼet</ta>
            <ta e="T49" id="Seg_381" s="T48">po-t</ta>
            <ta e="T50" id="Seg_382" s="T49">je-s</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_383" s="T1">man-nan</ta>
            <ta e="T3" id="Seg_384" s="T2">sobla</ta>
            <ta e="T4" id="Seg_385" s="T3">aunopqa-w</ta>
            <ta e="T5" id="Seg_386" s="T4">eː-sɨ</ta>
            <ta e="T6" id="Seg_387" s="T5">teper</ta>
            <ta e="T7" id="Seg_388" s="T6">wesʼ</ta>
            <ta e="T8" id="Seg_389" s="T7">quː-mbɨ-tɨn</ta>
            <ta e="T9" id="Seg_390" s="T8">aunopqa-la-w</ta>
            <ta e="T10" id="Seg_391" s="T9">man-nan</ta>
            <ta e="T11" id="Seg_392" s="T10">kartɨčka-qɨn</ta>
            <ta e="T12" id="Seg_393" s="T11">amdɨ-tɨn</ta>
            <ta e="T13" id="Seg_394" s="T12">i</ta>
            <ta e="T14" id="Seg_395" s="T13">mamo-ɨ</ta>
            <ta e="T15" id="Seg_396" s="T14">*natʼe-n</ta>
            <ta e="T16" id="Seg_397" s="T15">okkɨr</ta>
            <ta e="T17" id="Seg_398" s="T16">aunopqa-w</ta>
            <ta e="T18" id="Seg_399" s="T17">pöw-se</ta>
            <ta e="T19" id="Seg_400" s="T18">amdɨ</ta>
            <ta e="T20" id="Seg_401" s="T19">a</ta>
            <ta e="T21" id="Seg_402" s="T20">au</ta>
            <ta e="T22" id="Seg_403" s="T21">aunopqa-la-w</ta>
            <ta e="T23" id="Seg_404" s="T22">batʼinka-se</ta>
            <ta e="T24" id="Seg_405" s="T23">amdɨ-tɨn</ta>
            <ta e="T25" id="Seg_406" s="T24">padur-la</ta>
            <ta e="T26" id="Seg_407" s="T25">qadlan</ta>
            <ta e="T27" id="Seg_408" s="T26">saːrə-lǯi-mbɨ-ntɨ-tɨn</ta>
            <ta e="T28" id="Seg_409" s="T27">fartuk</ta>
            <ta e="T29" id="Seg_410" s="T28">saːrə-mbɨ-tɨn</ta>
            <ta e="T30" id="Seg_411" s="T29">i</ta>
            <ta e="T31" id="Seg_412" s="T30">tɨmnʼa-nɨ</ta>
            <ta e="T32" id="Seg_413" s="T31">nʼälʼdʼe</ta>
            <ta e="T33" id="Seg_414" s="T32">tob-ɨ-ŋ</ta>
            <ta e="T34" id="Seg_415" s="T33">nɨŋgɨ</ta>
            <ta e="T35" id="Seg_416" s="T34">täp</ta>
            <ta e="T36" id="Seg_417" s="T35">qɨba-ŋ</ta>
            <ta e="T37" id="Seg_418" s="T36">eː-sɨ</ta>
            <ta e="T38" id="Seg_419" s="T37">tob-ɨ-qɨntɨ</ta>
            <ta e="T39" id="Seg_420" s="T38">mun-la-tə</ta>
            <ta e="T40" id="Seg_421" s="T39">wesʼ</ta>
            <ta e="T41" id="Seg_422" s="T40">aːdu-ɨ-tɨn</ta>
            <ta e="T42" id="Seg_423" s="T41">täp</ta>
            <ta e="T43" id="Seg_424" s="T42">kur-sɨ</ta>
            <ta e="T44" id="Seg_425" s="T43">i</ta>
            <ta e="T45" id="Seg_426" s="T44">täp-la-nä</ta>
            <ta e="T46" id="Seg_427" s="T45">nɨ-l-ɨ-ǯə-n</ta>
            <ta e="T47" id="Seg_428" s="T46">täp-nä</ta>
            <ta e="T48" id="Seg_429" s="T47">sɨdədʼet</ta>
            <ta e="T49" id="Seg_430" s="T48">po-tə</ta>
            <ta e="T50" id="Seg_431" s="T49">eː-sɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_432" s="T1">I-ADES</ta>
            <ta e="T3" id="Seg_433" s="T2">five</ta>
            <ta e="T4" id="Seg_434" s="T3">mother's.sister.[NOM]-1SG</ta>
            <ta e="T5" id="Seg_435" s="T4">be-PST.[3SG.S]</ta>
            <ta e="T6" id="Seg_436" s="T5">now</ta>
            <ta e="T7" id="Seg_437" s="T6">all.[NOM]</ta>
            <ta e="T8" id="Seg_438" s="T7">die-PST.NAR-3PL</ta>
            <ta e="T9" id="Seg_439" s="T8">mother's.sister-PL.[NOM]-1SG</ta>
            <ta e="T10" id="Seg_440" s="T9">I-ADES</ta>
            <ta e="T11" id="Seg_441" s="T10">photo-LOC</ta>
            <ta e="T12" id="Seg_442" s="T11">sit-3PL</ta>
            <ta e="T13" id="Seg_443" s="T12">and</ta>
            <ta e="T14" id="Seg_444" s="T13">mum.[NOM]-EP</ta>
            <ta e="T15" id="Seg_445" s="T14">there-ADV.LOC</ta>
            <ta e="T16" id="Seg_446" s="T15">one</ta>
            <ta e="T17" id="Seg_447" s="T16">mother's.sister.[NOM]-1SG</ta>
            <ta e="T18" id="Seg_448" s="T17">boots-COM</ta>
            <ta e="T19" id="Seg_449" s="T18">sit.[3SG.S]</ta>
            <ta e="T20" id="Seg_450" s="T19">and</ta>
            <ta e="T21" id="Seg_451" s="T20">other</ta>
            <ta e="T22" id="Seg_452" s="T21">mother's.sister-PL.[NOM]-1SG</ta>
            <ta e="T23" id="Seg_453" s="T22">boots-INSTR</ta>
            <ta e="T24" id="Seg_454" s="T23">sit-3PL</ta>
            <ta e="T25" id="Seg_455" s="T24">%%-PL.[NOM]</ta>
            <ta e="T26" id="Seg_456" s="T25">%%</ta>
            <ta e="T27" id="Seg_457" s="T26">bind-PFV-RES-INFER-3PL</ta>
            <ta e="T28" id="Seg_458" s="T27">apron.[NOM]</ta>
            <ta e="T29" id="Seg_459" s="T28">bind-RES-3PL</ta>
            <ta e="T30" id="Seg_460" s="T29">and</ta>
            <ta e="T31" id="Seg_461" s="T30">brother.[NOM]-1SG</ta>
            <ta e="T32" id="Seg_462" s="T31">bare</ta>
            <ta e="T33" id="Seg_463" s="T32">leg-EP-ADVZ</ta>
            <ta e="T34" id="Seg_464" s="T33">stand.[3SG.S]</ta>
            <ta e="T35" id="Seg_465" s="T34">(s)he.[NOM]</ta>
            <ta e="T36" id="Seg_466" s="T35">small-ADVZ</ta>
            <ta e="T37" id="Seg_467" s="T36">be-PST.[3SG.S]</ta>
            <ta e="T38" id="Seg_468" s="T37">leg-EP-LOC.3SG</ta>
            <ta e="T39" id="Seg_469" s="T38">finger-PL.[NOM]-3SG</ta>
            <ta e="T40" id="Seg_470" s="T39">all</ta>
            <ta e="T41" id="Seg_471" s="T40">be.visible-EP-3PL</ta>
            <ta e="T42" id="Seg_472" s="T41">(s)he.[NOM]</ta>
            <ta e="T43" id="Seg_473" s="T42">run-PST.[3SG.S]</ta>
            <ta e="T44" id="Seg_474" s="T43">and</ta>
            <ta e="T45" id="Seg_475" s="T44">(s)he-PL-ALL</ta>
            <ta e="T46" id="Seg_476" s="T45">stand-INCH-EP-DRV-3SG.S</ta>
            <ta e="T47" id="Seg_477" s="T46">(s)he-ALL</ta>
            <ta e="T48" id="Seg_478" s="T47">eight</ta>
            <ta e="T49" id="Seg_479" s="T48">year.[NOM]-3SG</ta>
            <ta e="T50" id="Seg_480" s="T49">be-PST.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_481" s="T1">я-ADES</ta>
            <ta e="T3" id="Seg_482" s="T2">пять</ta>
            <ta e="T4" id="Seg_483" s="T3">сестра.матери.[NOM]-1SG</ta>
            <ta e="T5" id="Seg_484" s="T4">быть-PST.[3SG.S]</ta>
            <ta e="T6" id="Seg_485" s="T5">теперь</ta>
            <ta e="T7" id="Seg_486" s="T6">весь.[NOM]</ta>
            <ta e="T8" id="Seg_487" s="T7">умереть-PST.NAR-3PL</ta>
            <ta e="T9" id="Seg_488" s="T8">сестра.матери-PL.[NOM]-1SG</ta>
            <ta e="T10" id="Seg_489" s="T9">я-ADES</ta>
            <ta e="T11" id="Seg_490" s="T10">фотокарточка-LOC</ta>
            <ta e="T12" id="Seg_491" s="T11">сидеть-3PL</ta>
            <ta e="T13" id="Seg_492" s="T12">и</ta>
            <ta e="T14" id="Seg_493" s="T13">мама.[NOM]-EP</ta>
            <ta e="T15" id="Seg_494" s="T14">туда-ADV.LOC</ta>
            <ta e="T16" id="Seg_495" s="T15">один</ta>
            <ta e="T17" id="Seg_496" s="T16">сестра.матери.[NOM]-1SG</ta>
            <ta e="T18" id="Seg_497" s="T17">чарки-COM</ta>
            <ta e="T19" id="Seg_498" s="T18">сидеть.[3SG.S]</ta>
            <ta e="T20" id="Seg_499" s="T19">а</ta>
            <ta e="T21" id="Seg_500" s="T20">другой</ta>
            <ta e="T22" id="Seg_501" s="T21">сестра.матери-PL.[NOM]-1SG</ta>
            <ta e="T23" id="Seg_502" s="T22">ботинки-INSTR</ta>
            <ta e="T24" id="Seg_503" s="T23">сидеть-3PL</ta>
            <ta e="T25" id="Seg_504" s="T24">%%-PL.[NOM]</ta>
            <ta e="T26" id="Seg_505" s="T25">%%</ta>
            <ta e="T27" id="Seg_506" s="T26">привязать-PFV-RES-INFER-3PL</ta>
            <ta e="T28" id="Seg_507" s="T27">фартук.[NOM]</ta>
            <ta e="T29" id="Seg_508" s="T28">привязать-RES-3PL</ta>
            <ta e="T30" id="Seg_509" s="T29">и</ta>
            <ta e="T31" id="Seg_510" s="T30">брат.[NOM]-1SG</ta>
            <ta e="T32" id="Seg_511" s="T31">голый</ta>
            <ta e="T33" id="Seg_512" s="T32">нога-EP-ADVZ</ta>
            <ta e="T34" id="Seg_513" s="T33">стоять.[3SG.S]</ta>
            <ta e="T35" id="Seg_514" s="T34">он(а).[NOM]</ta>
            <ta e="T36" id="Seg_515" s="T35">маленький-ADVZ</ta>
            <ta e="T37" id="Seg_516" s="T36">быть-PST.[3SG.S]</ta>
            <ta e="T38" id="Seg_517" s="T37">нога-EP-LOC.3SG</ta>
            <ta e="T39" id="Seg_518" s="T38">палец-PL.[NOM]-3SG</ta>
            <ta e="T40" id="Seg_519" s="T39">весь</ta>
            <ta e="T41" id="Seg_520" s="T40">виднеться-EP-3PL</ta>
            <ta e="T42" id="Seg_521" s="T41">он(а).[NOM]</ta>
            <ta e="T43" id="Seg_522" s="T42">бегать-PST.[3SG.S]</ta>
            <ta e="T44" id="Seg_523" s="T43">и</ta>
            <ta e="T45" id="Seg_524" s="T44">он(а)-PL-ALL</ta>
            <ta e="T46" id="Seg_525" s="T45">стоять-INCH-EP-DRV-3SG.S</ta>
            <ta e="T47" id="Seg_526" s="T46">он(а)-ALL</ta>
            <ta e="T48" id="Seg_527" s="T47">восемь</ta>
            <ta e="T49" id="Seg_528" s="T48">год.[NOM]-3SG</ta>
            <ta e="T50" id="Seg_529" s="T49">быть-PST.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_530" s="T1">pers-n:case</ta>
            <ta e="T3" id="Seg_531" s="T2">num</ta>
            <ta e="T4" id="Seg_532" s="T3">n.[n:case]-n:poss</ta>
            <ta e="T5" id="Seg_533" s="T4">v-v:tense.[v:pn]</ta>
            <ta e="T6" id="Seg_534" s="T5">adv</ta>
            <ta e="T7" id="Seg_535" s="T6">quant.[n:case]</ta>
            <ta e="T8" id="Seg_536" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_537" s="T8">n-n:num.[n:case]-n:poss</ta>
            <ta e="T10" id="Seg_538" s="T9">pers-n:case</ta>
            <ta e="T11" id="Seg_539" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_540" s="T11">v-v:pn</ta>
            <ta e="T13" id="Seg_541" s="T12">conj</ta>
            <ta e="T14" id="Seg_542" s="T13">n.[n:case]-n:ins</ta>
            <ta e="T15" id="Seg_543" s="T14">adv-adv:case</ta>
            <ta e="T16" id="Seg_544" s="T15">num</ta>
            <ta e="T17" id="Seg_545" s="T16">n.[n:case]-n:poss</ta>
            <ta e="T18" id="Seg_546" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_547" s="T18">v.[v:pn]</ta>
            <ta e="T20" id="Seg_548" s="T19">conj</ta>
            <ta e="T21" id="Seg_549" s="T20">adj</ta>
            <ta e="T22" id="Seg_550" s="T21">n-n:num.[n:case]-n:poss</ta>
            <ta e="T23" id="Seg_551" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_552" s="T23">v-v:pn</ta>
            <ta e="T25" id="Seg_553" s="T24">n-n:num.[n:case]</ta>
            <ta e="T26" id="Seg_554" s="T25">%%</ta>
            <ta e="T27" id="Seg_555" s="T26">v-v&gt;v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T28" id="Seg_556" s="T27">n.[n:case]</ta>
            <ta e="T29" id="Seg_557" s="T28">v-v&gt;v-v:pn</ta>
            <ta e="T30" id="Seg_558" s="T29">conj</ta>
            <ta e="T31" id="Seg_559" s="T30">n.[n:case]-n:poss</ta>
            <ta e="T32" id="Seg_560" s="T31">adj</ta>
            <ta e="T33" id="Seg_561" s="T32">n-n:ins-n&gt;adv</ta>
            <ta e="T34" id="Seg_562" s="T33">v.[v:pn]</ta>
            <ta e="T35" id="Seg_563" s="T34">pers.[n:case]</ta>
            <ta e="T36" id="Seg_564" s="T35">adj-adj&gt;adv</ta>
            <ta e="T37" id="Seg_565" s="T36">v-v:tense.[v:pn]</ta>
            <ta e="T38" id="Seg_566" s="T37">n-n:ins-n:case.poss</ta>
            <ta e="T39" id="Seg_567" s="T38">n-n:num.[n:case]-n:poss</ta>
            <ta e="T40" id="Seg_568" s="T39">quant</ta>
            <ta e="T41" id="Seg_569" s="T40">v-v:ins-v:pn</ta>
            <ta e="T42" id="Seg_570" s="T41">pers.[n:case]</ta>
            <ta e="T43" id="Seg_571" s="T42">v-v:tense.[v:pn]</ta>
            <ta e="T44" id="Seg_572" s="T43">conj</ta>
            <ta e="T45" id="Seg_573" s="T44">pers-n:num-n:case</ta>
            <ta e="T46" id="Seg_574" s="T45">v-v&gt;v-v:ins-v&gt;v-v:pn</ta>
            <ta e="T47" id="Seg_575" s="T46">pers-n:case</ta>
            <ta e="T48" id="Seg_576" s="T47">num</ta>
            <ta e="T49" id="Seg_577" s="T48">n.[n:case]-n:poss</ta>
            <ta e="T50" id="Seg_578" s="T49">v-v:tense.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_579" s="T1">pers</ta>
            <ta e="T3" id="Seg_580" s="T2">num</ta>
            <ta e="T4" id="Seg_581" s="T3">n</ta>
            <ta e="T5" id="Seg_582" s="T4">v</ta>
            <ta e="T6" id="Seg_583" s="T5">adv</ta>
            <ta e="T7" id="Seg_584" s="T6">quant</ta>
            <ta e="T8" id="Seg_585" s="T7">v</ta>
            <ta e="T9" id="Seg_586" s="T8">n</ta>
            <ta e="T10" id="Seg_587" s="T9">pers</ta>
            <ta e="T11" id="Seg_588" s="T10">n</ta>
            <ta e="T12" id="Seg_589" s="T11">v</ta>
            <ta e="T13" id="Seg_590" s="T12">conj</ta>
            <ta e="T14" id="Seg_591" s="T13">n</ta>
            <ta e="T15" id="Seg_592" s="T14">adv</ta>
            <ta e="T16" id="Seg_593" s="T15">num</ta>
            <ta e="T17" id="Seg_594" s="T16">n</ta>
            <ta e="T18" id="Seg_595" s="T17">n</ta>
            <ta e="T19" id="Seg_596" s="T18">v</ta>
            <ta e="T20" id="Seg_597" s="T19">conj</ta>
            <ta e="T21" id="Seg_598" s="T20">adj</ta>
            <ta e="T22" id="Seg_599" s="T21">n</ta>
            <ta e="T23" id="Seg_600" s="T22">n</ta>
            <ta e="T24" id="Seg_601" s="T23">v</ta>
            <ta e="T25" id="Seg_602" s="T24">n</ta>
            <ta e="T27" id="Seg_603" s="T26">v</ta>
            <ta e="T28" id="Seg_604" s="T27">n</ta>
            <ta e="T29" id="Seg_605" s="T28">v</ta>
            <ta e="T30" id="Seg_606" s="T29">conj</ta>
            <ta e="T31" id="Seg_607" s="T30">n</ta>
            <ta e="T32" id="Seg_608" s="T31">adj</ta>
            <ta e="T33" id="Seg_609" s="T32">adv</ta>
            <ta e="T34" id="Seg_610" s="T33">v</ta>
            <ta e="T35" id="Seg_611" s="T34">pers</ta>
            <ta e="T36" id="Seg_612" s="T35">adv</ta>
            <ta e="T37" id="Seg_613" s="T36">v</ta>
            <ta e="T38" id="Seg_614" s="T37">n</ta>
            <ta e="T39" id="Seg_615" s="T38">n</ta>
            <ta e="T40" id="Seg_616" s="T39">quant</ta>
            <ta e="T41" id="Seg_617" s="T40">v</ta>
            <ta e="T42" id="Seg_618" s="T41">pers</ta>
            <ta e="T43" id="Seg_619" s="T42">v</ta>
            <ta e="T44" id="Seg_620" s="T43">conj</ta>
            <ta e="T45" id="Seg_621" s="T44">pers</ta>
            <ta e="T46" id="Seg_622" s="T45">v</ta>
            <ta e="T47" id="Seg_623" s="T46">pers</ta>
            <ta e="T48" id="Seg_624" s="T47">num</ta>
            <ta e="T49" id="Seg_625" s="T48">n</ta>
            <ta e="T50" id="Seg_626" s="T49">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_627" s="T1">pro.h:Poss</ta>
            <ta e="T4" id="Seg_628" s="T3">np.h:Th</ta>
            <ta e="T6" id="Seg_629" s="T5">adv:Time</ta>
            <ta e="T7" id="Seg_630" s="T6">pro.h:P</ta>
            <ta e="T9" id="Seg_631" s="T8">np.h:Th</ta>
            <ta e="T10" id="Seg_632" s="T9">pro.h:Poss</ta>
            <ta e="T11" id="Seg_633" s="T10">np:L</ta>
            <ta e="T14" id="Seg_634" s="T13">np.h:Th</ta>
            <ta e="T15" id="Seg_635" s="T14">adv:L</ta>
            <ta e="T17" id="Seg_636" s="T16">np.h:Th 0.1.h:Poss</ta>
            <ta e="T18" id="Seg_637" s="T17">np:Com</ta>
            <ta e="T22" id="Seg_638" s="T21">np.h:Th 0.1.h:Poss</ta>
            <ta e="T23" id="Seg_639" s="T22">np:Com</ta>
            <ta e="T25" id="Seg_640" s="T24">np:Th</ta>
            <ta e="T28" id="Seg_641" s="T27">np:Th</ta>
            <ta e="T31" id="Seg_642" s="T30">np.h:Th 0.1.h:Poss</ta>
            <ta e="T35" id="Seg_643" s="T34">pro.h:Th</ta>
            <ta e="T38" id="Seg_644" s="T37">np:L 0.3.h:Poss</ta>
            <ta e="T39" id="Seg_645" s="T38">np:Th 0.3.h:Poss</ta>
            <ta e="T42" id="Seg_646" s="T41">pro.h:A</ta>
            <ta e="T45" id="Seg_647" s="T44">pro.h:L</ta>
            <ta e="T46" id="Seg_648" s="T45">0.3.h:Th</ta>
            <ta e="T49" id="Seg_649" s="T48">np:Th 0.3.h:Poss</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_650" s="T3">np.h:S</ta>
            <ta e="T5" id="Seg_651" s="T4">v:pred</ta>
            <ta e="T7" id="Seg_652" s="T6">pro.h:S</ta>
            <ta e="T8" id="Seg_653" s="T7">v:pred</ta>
            <ta e="T9" id="Seg_654" s="T8">np.h:S</ta>
            <ta e="T12" id="Seg_655" s="T11">v:pred</ta>
            <ta e="T14" id="Seg_656" s="T13">np.h:S</ta>
            <ta e="T17" id="Seg_657" s="T16">np.h:S</ta>
            <ta e="T19" id="Seg_658" s="T18">v:pred</ta>
            <ta e="T22" id="Seg_659" s="T21">np.h:S</ta>
            <ta e="T24" id="Seg_660" s="T23">v:pred</ta>
            <ta e="T25" id="Seg_661" s="T24">np:S</ta>
            <ta e="T27" id="Seg_662" s="T26">v:pred</ta>
            <ta e="T28" id="Seg_663" s="T27">np:S</ta>
            <ta e="T29" id="Seg_664" s="T28">v:pred</ta>
            <ta e="T31" id="Seg_665" s="T30">np.h:S</ta>
            <ta e="T34" id="Seg_666" s="T33">v:pred</ta>
            <ta e="T35" id="Seg_667" s="T34">pro.h:S</ta>
            <ta e="T36" id="Seg_668" s="T35">adj:pred</ta>
            <ta e="T37" id="Seg_669" s="T36">cop</ta>
            <ta e="T39" id="Seg_670" s="T38">np:S</ta>
            <ta e="T41" id="Seg_671" s="T40">v:pred</ta>
            <ta e="T42" id="Seg_672" s="T41">pro.h:S</ta>
            <ta e="T43" id="Seg_673" s="T42">v:pred</ta>
            <ta e="T46" id="Seg_674" s="T45">0.3.h:S v:pred</ta>
            <ta e="T49" id="Seg_675" s="T48">np:S</ta>
            <ta e="T50" id="Seg_676" s="T49">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T6" id="Seg_677" s="T5">RUS:core</ta>
            <ta e="T7" id="Seg_678" s="T6">RUS:core</ta>
            <ta e="T11" id="Seg_679" s="T10">RUS:cult</ta>
            <ta e="T13" id="Seg_680" s="T12">RUS:gram</ta>
            <ta e="T14" id="Seg_681" s="T13">RUS:core</ta>
            <ta e="T20" id="Seg_682" s="T19">RUS:gram</ta>
            <ta e="T23" id="Seg_683" s="T22">RUS:cult</ta>
            <ta e="T28" id="Seg_684" s="T27">RUS:cult</ta>
            <ta e="T30" id="Seg_685" s="T29">RUS:gram</ta>
            <ta e="T40" id="Seg_686" s="T39">RUS:core</ta>
            <ta e="T44" id="Seg_687" s="T43">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_688" s="T1">I had five aunts (my mother's sisters).</ta>
            <ta e="T8" id="Seg_689" s="T5">They all died.</ta>
            <ta e="T15" id="Seg_690" s="T8">I have a photo of my mothers's sisters, and my mother is there.</ta>
            <ta e="T19" id="Seg_691" s="T15">One of my aunts is sitting wearing traditional boots.</ta>
            <ta e="T24" id="Seg_692" s="T19">The other aunts are sitting wearing (Russian)? boots.</ta>
            <ta e="T27" id="Seg_693" s="T24">There are garters tied to (the boots?).</ta>
            <ta e="T29" id="Seg_694" s="T27">An apron is tied up.</ta>
            <ta e="T34" id="Seg_695" s="T29">And my younger brother is standing nearby barefoot.</ta>
            <ta e="T37" id="Seg_696" s="T34">He was small.</ta>
            <ta e="T41" id="Seg_697" s="T37">One can see all the toes of his feet.</ta>
            <ta e="T46" id="Seg_698" s="T41">He was running around and he joined them.</ta>
            <ta e="T50" id="Seg_699" s="T46">He was eight years old.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_700" s="T1">Ich hatte fünf Tanten (die Schwestern meiner Mutter).</ta>
            <ta e="T8" id="Seg_701" s="T5">Sie alle sind gestorben.</ta>
            <ta e="T15" id="Seg_702" s="T8">Ich habe ein Foto von den Schwestern meiner Mutter, meine Mutter ist dort.</ta>
            <ta e="T19" id="Seg_703" s="T15">Eine meiner Tanten sitzt mit traditionellen Schuhen.</ta>
            <ta e="T24" id="Seg_704" s="T19">Die anderen Tanten sitzen mit (russischen?) Schuhen.</ta>
            <ta e="T27" id="Seg_705" s="T24">Sie sind mit Strumpfbändern gebunden.</ta>
            <ta e="T29" id="Seg_706" s="T27">Eine Schürze ist gebunden.</ta>
            <ta e="T34" id="Seg_707" s="T29">Und mein kleiner Bruder steht barfuß daneben.</ta>
            <ta e="T37" id="Seg_708" s="T34">Er war klein.</ta>
            <ta e="T41" id="Seg_709" s="T37">Man kann all seine Zehen an den Füßen sehen.</ta>
            <ta e="T46" id="Seg_710" s="T41">Er rannte umher und stand bei ihnen.</ta>
            <ta e="T50" id="Seg_711" s="T46">Er war acht Jahre alt.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_712" s="T1">У меня было пять теть (сестер матери).</ta>
            <ta e="T8" id="Seg_713" s="T5">Теперь все умерли.</ta>
            <ta e="T15" id="Seg_714" s="T8">Сестры матери у меня на фотографии сидят, и мама моя там.</ta>
            <ta e="T19" id="Seg_715" s="T15">Одна моя тетя в чарках сидит.</ta>
            <ta e="T24" id="Seg_716" s="T19">А другие мои тетки в ботинках сидят.</ta>
            <ta e="T27" id="Seg_717" s="T24">(От чирков) подвязки завязаны.</ta>
            <ta e="T29" id="Seg_718" s="T27">Фартук подвязан.</ta>
            <ta e="T34" id="Seg_719" s="T29">И братишка мой стоит тут же босиком.</ta>
            <ta e="T37" id="Seg_720" s="T34">Он маленький был.</ta>
            <ta e="T41" id="Seg_721" s="T37">У него на ногах все пальцы видны.</ta>
            <ta e="T46" id="Seg_722" s="T41">Он бегал и к ним встал.</ta>
            <ta e="T50" id="Seg_723" s="T46">Ему было восемь лет.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_724" s="T1">у меня было пять материных сестер</ta>
            <ta e="T8" id="Seg_725" s="T5">теперь все умерли</ta>
            <ta e="T15" id="Seg_726" s="T8">сестры матери у меня на карточке сидят и мама с ними</ta>
            <ta e="T19" id="Seg_727" s="T15">одна авнупка в чарках сидит</ta>
            <ta e="T24" id="Seg_728" s="T19">а те тетки в ботинках сидят</ta>
            <ta e="T27" id="Seg_729" s="T24">от чарков подвязки завязаны</ta>
            <ta e="T29" id="Seg_730" s="T27">фартук подвязан</ta>
            <ta e="T34" id="Seg_731" s="T29">а братишка мой стоит тут же босиком</ta>
            <ta e="T37" id="Seg_732" s="T34">он маленький был</ta>
            <ta e="T41" id="Seg_733" s="T37">на ногах все пальцы видать</ta>
            <ta e="T46" id="Seg_734" s="T41">он бегал и с ними встал</ta>
            <ta e="T50" id="Seg_735" s="T46">ему было восемь лет</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T27" id="Seg_736" s="T24">[BrM:] Tentative analysis of 'sarelǯəmɨtdɨt'.</ta>
            <ta e="T29" id="Seg_737" s="T27">[BrM:] 3PL?</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T19" id="Seg_738" s="T15">пʼӧй - чарок</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
