<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_FishingNet_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_FishingNet_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">18</ud-information>
            <ud-information attribute-name="# HIAT:w">14</ud-information>
            <ud-information attribute-name="# e">14</ud-information>
            <ud-information attribute-name="# HIAT:u">4</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T15" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Maːdɨn</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">qɨɣɨn</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">ippa</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">poqgɨ</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Puskaj</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">ippi</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">Man</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">sičas</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">qwatǯan</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">poqqɨm</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">potku</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_44" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">Qardʼen</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">qwɛl</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">amǯidawt</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T15" id="Seg_55" n="sc" s="T1">
               <ts e="T2" id="Seg_57" n="e" s="T1">Maːdɨn </ts>
               <ts e="T3" id="Seg_59" n="e" s="T2">qɨɣɨn </ts>
               <ts e="T4" id="Seg_61" n="e" s="T3">ippa </ts>
               <ts e="T5" id="Seg_63" n="e" s="T4">poqgɨ. </ts>
               <ts e="T6" id="Seg_65" n="e" s="T5">Puskaj </ts>
               <ts e="T7" id="Seg_67" n="e" s="T6">ippi. </ts>
               <ts e="T8" id="Seg_69" n="e" s="T7">Man </ts>
               <ts e="T9" id="Seg_71" n="e" s="T8">sičas </ts>
               <ts e="T10" id="Seg_73" n="e" s="T9">qwatǯan </ts>
               <ts e="T11" id="Seg_75" n="e" s="T10">poqqɨm </ts>
               <ts e="T12" id="Seg_77" n="e" s="T11">potku. </ts>
               <ts e="T13" id="Seg_79" n="e" s="T12">Qardʼen </ts>
               <ts e="T14" id="Seg_81" n="e" s="T13">qwɛl </ts>
               <ts e="T15" id="Seg_83" n="e" s="T14">amǯidawt. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_84" s="T1">PVD_1964_FishingNet_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_85" s="T5">PVD_1964_FishingNet_nar.002 (001.002)</ta>
            <ta e="T12" id="Seg_86" s="T7">PVD_1964_FishingNet_nar.003 (001.003)</ta>
            <ta e="T15" id="Seg_87" s="T12">PVD_1964_FishingNet_nar.004 (001.004)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_88" s="T1">′ма̄дын ′kыɣын иппа ′поkгы.</ta>
            <ta e="T7" id="Seg_89" s="T5">пускай ′иппи.</ta>
            <ta e="T12" id="Seg_90" s="T7">ман си′час kwат′джан ′поkkым ′потку.</ta>
            <ta e="T15" id="Seg_91" s="T12">kар′дʼен kwɛл ′амджи‵даwт.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_92" s="T1">maːdɨn qɨɣɨn ippa poqgɨ.</ta>
            <ta e="T7" id="Seg_93" s="T5">puskaj ippi.</ta>
            <ta e="T12" id="Seg_94" s="T7">man sičas qwatǯan poqqɨm potku.</ta>
            <ta e="T15" id="Seg_95" s="T12">qardʼen qwɛl amǯidawt.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_96" s="T1">Maːdɨn qɨɣɨn ippa poqgɨ. </ta>
            <ta e="T7" id="Seg_97" s="T5">Puskaj ippi. </ta>
            <ta e="T12" id="Seg_98" s="T7">Man sičas qwatǯan poqqɨm potku. </ta>
            <ta e="T15" id="Seg_99" s="T12">Qardʼen qwɛl amǯidawt. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_100" s="T1">maːd-ɨ-n</ta>
            <ta e="T3" id="Seg_101" s="T2">qɨ-ɣɨn</ta>
            <ta e="T4" id="Seg_102" s="T3">ippa</ta>
            <ta e="T5" id="Seg_103" s="T4">poqgɨ</ta>
            <ta e="T6" id="Seg_104" s="T5">puskaj</ta>
            <ta e="T7" id="Seg_105" s="T6">ippi</ta>
            <ta e="T8" id="Seg_106" s="T7">man</ta>
            <ta e="T9" id="Seg_107" s="T8">sičas</ta>
            <ta e="T10" id="Seg_108" s="T9">qwat-ǯa-n</ta>
            <ta e="T11" id="Seg_109" s="T10">poqqɨ-m</ta>
            <ta e="T12" id="Seg_110" s="T11">pot-ku</ta>
            <ta e="T13" id="Seg_111" s="T12">qar-dʼe-n</ta>
            <ta e="T14" id="Seg_112" s="T13">qwɛl</ta>
            <ta e="T15" id="Seg_113" s="T14">am-ǯi-da-wt</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_114" s="T1">maːt-ɨ-n</ta>
            <ta e="T3" id="Seg_115" s="T2">qɨ-qɨn</ta>
            <ta e="T4" id="Seg_116" s="T3">ippɨ</ta>
            <ta e="T5" id="Seg_117" s="T4">poqqɨ</ta>
            <ta e="T6" id="Seg_118" s="T5">puskaj</ta>
            <ta e="T7" id="Seg_119" s="T6">ippɨ</ta>
            <ta e="T8" id="Seg_120" s="T7">man</ta>
            <ta e="T9" id="Seg_121" s="T8">sičas</ta>
            <ta e="T10" id="Seg_122" s="T9">qwan-enǯɨ-ŋ</ta>
            <ta e="T11" id="Seg_123" s="T10">poqqɨ-m</ta>
            <ta e="T12" id="Seg_124" s="T11">pot-gu</ta>
            <ta e="T13" id="Seg_125" s="T12">qare-dʼel-n</ta>
            <ta e="T14" id="Seg_126" s="T13">qwɛl</ta>
            <ta e="T15" id="Seg_127" s="T14">am-ǯə-enǯɨ-un</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_128" s="T1">tent-EP-GEN</ta>
            <ta e="T3" id="Seg_129" s="T2">middle-LOC</ta>
            <ta e="T4" id="Seg_130" s="T3">lie.[3SG.S]</ta>
            <ta e="T5" id="Seg_131" s="T4">net.[NOM]</ta>
            <ta e="T6" id="Seg_132" s="T5">JUSS</ta>
            <ta e="T7" id="Seg_133" s="T6">lie.[3SG.S]</ta>
            <ta e="T8" id="Seg_134" s="T7">I.NOM</ta>
            <ta e="T9" id="Seg_135" s="T8">now</ta>
            <ta e="T10" id="Seg_136" s="T9">leave-FUT-1SG.S</ta>
            <ta e="T11" id="Seg_137" s="T10">net-ACC</ta>
            <ta e="T12" id="Seg_138" s="T11">settle.net-INF</ta>
            <ta e="T13" id="Seg_139" s="T12">morning-day-ADV.LOC</ta>
            <ta e="T14" id="Seg_140" s="T13">fish.[NOM]</ta>
            <ta e="T15" id="Seg_141" s="T14">eat-DRV-FUT-1PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_142" s="T1">чум-EP-GEN</ta>
            <ta e="T3" id="Seg_143" s="T2">середина-LOC</ta>
            <ta e="T4" id="Seg_144" s="T3">лежать.[3SG.S]</ta>
            <ta e="T5" id="Seg_145" s="T4">сеть.[NOM]</ta>
            <ta e="T6" id="Seg_146" s="T5">JUSS</ta>
            <ta e="T7" id="Seg_147" s="T6">лежать.[3SG.S]</ta>
            <ta e="T8" id="Seg_148" s="T7">я.NOM</ta>
            <ta e="T9" id="Seg_149" s="T8">сейчас</ta>
            <ta e="T10" id="Seg_150" s="T9">отправиться-FUT-1SG.S</ta>
            <ta e="T11" id="Seg_151" s="T10">сеть-ACC</ta>
            <ta e="T12" id="Seg_152" s="T11">поставить.сеть-INF</ta>
            <ta e="T13" id="Seg_153" s="T12">утро-день-ADV.LOC</ta>
            <ta e="T14" id="Seg_154" s="T13">рыба.[NOM]</ta>
            <ta e="T15" id="Seg_155" s="T14">съесть-DRV-FUT-1PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_156" s="T1">n-n:ins-n:case</ta>
            <ta e="T3" id="Seg_157" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_158" s="T3">v.[v:pn]</ta>
            <ta e="T5" id="Seg_159" s="T4">n.[n:case]</ta>
            <ta e="T6" id="Seg_160" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_161" s="T6">v.[v:pn]</ta>
            <ta e="T8" id="Seg_162" s="T7">pers</ta>
            <ta e="T9" id="Seg_163" s="T8">adv</ta>
            <ta e="T10" id="Seg_164" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_165" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_166" s="T11">v-v:inf</ta>
            <ta e="T13" id="Seg_167" s="T12">n-n-adv:case</ta>
            <ta e="T14" id="Seg_168" s="T13">n.[n:case]</ta>
            <ta e="T15" id="Seg_169" s="T14">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_170" s="T1">n</ta>
            <ta e="T3" id="Seg_171" s="T2">n</ta>
            <ta e="T4" id="Seg_172" s="T3">v</ta>
            <ta e="T5" id="Seg_173" s="T4">n</ta>
            <ta e="T6" id="Seg_174" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_175" s="T6">v</ta>
            <ta e="T8" id="Seg_176" s="T7">pers</ta>
            <ta e="T9" id="Seg_177" s="T8">adv</ta>
            <ta e="T10" id="Seg_178" s="T9">v</ta>
            <ta e="T11" id="Seg_179" s="T10">n</ta>
            <ta e="T12" id="Seg_180" s="T11">v</ta>
            <ta e="T13" id="Seg_181" s="T12">n</ta>
            <ta e="T14" id="Seg_182" s="T13">n</ta>
            <ta e="T15" id="Seg_183" s="T14">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_184" s="T1">np:Poss</ta>
            <ta e="T3" id="Seg_185" s="T2">np:L</ta>
            <ta e="T5" id="Seg_186" s="T4">np:Th</ta>
            <ta e="T7" id="Seg_187" s="T6">0.3:Th</ta>
            <ta e="T8" id="Seg_188" s="T7">pro.h:A</ta>
            <ta e="T9" id="Seg_189" s="T8">adv:Time</ta>
            <ta e="T11" id="Seg_190" s="T10">np:Th</ta>
            <ta e="T13" id="Seg_191" s="T12">np:Time</ta>
            <ta e="T14" id="Seg_192" s="T13">np:P</ta>
            <ta e="T15" id="Seg_193" s="T14">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_194" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_195" s="T4">np:S</ta>
            <ta e="T7" id="Seg_196" s="T6">0.3:S v:pred</ta>
            <ta e="T8" id="Seg_197" s="T7">pro.h:S</ta>
            <ta e="T10" id="Seg_198" s="T9">v:pred</ta>
            <ta e="T12" id="Seg_199" s="T10">s:purp</ta>
            <ta e="T14" id="Seg_200" s="T13">np:O</ta>
            <ta e="T15" id="Seg_201" s="T14">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T6" id="Seg_202" s="T5">RUS:gram</ta>
            <ta e="T9" id="Seg_203" s="T8">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_204" s="T1">A fishing net lies in the middle of a house.</ta>
            <ta e="T7" id="Seg_205" s="T5">Let it lie.</ta>
            <ta e="T12" id="Seg_206" s="T7">Now I'll go and settle the fishing net.</ta>
            <ta e="T15" id="Seg_207" s="T12">Tomorrow we'll be eating fish.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_208" s="T1">Mitten im Haus liegt ein Fischernetz.</ta>
            <ta e="T7" id="Seg_209" s="T5">Lass es liegen.</ta>
            <ta e="T12" id="Seg_210" s="T7">Ich gehe jetzt das Netz auswerfen.</ta>
            <ta e="T15" id="Seg_211" s="T12">Morgen essen wir Fisch.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_212" s="T1">Посередине избы лежит сеть.</ta>
            <ta e="T7" id="Seg_213" s="T5">Пусть лежит.</ta>
            <ta e="T12" id="Seg_214" s="T7">Я сейчас пойду сеть ставить.</ta>
            <ta e="T15" id="Seg_215" s="T12">Завтра рыбу есть будем.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_216" s="T1">средь избы лежит сетка</ta>
            <ta e="T7" id="Seg_217" s="T5">пусть лежит</ta>
            <ta e="T12" id="Seg_218" s="T7">я сейчас пойду сетку ставить</ta>
            <ta e="T15" id="Seg_219" s="T12">а завтра рыбу есть будем</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
