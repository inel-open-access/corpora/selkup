<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_FurCoat_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_FurCoat_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">16</ud-information>
            <ud-information attribute-name="# HIAT:w">13</ud-information>
            <ud-information attribute-name="# e">13</ud-information>
            <ud-information attribute-name="# HIAT:u">3</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T14" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Qobla</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">taumɨdɨt</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">porɣɨm</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">setqu</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Nadə</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">koːcʼin</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">taugu</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">qoblam</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_32" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">Okkɨr</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">qoboɣɨnto</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">ass</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">sütšal</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">porəɣɨm</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T14" id="Seg_49" n="sc" s="T1">
               <ts e="T2" id="Seg_51" n="e" s="T1">Qobla </ts>
               <ts e="T3" id="Seg_53" n="e" s="T2">taumɨdɨt </ts>
               <ts e="T4" id="Seg_55" n="e" s="T3">porɣɨm </ts>
               <ts e="T5" id="Seg_57" n="e" s="T4">setqu. </ts>
               <ts e="T6" id="Seg_59" n="e" s="T5">Nadə </ts>
               <ts e="T7" id="Seg_61" n="e" s="T6">koːcʼin </ts>
               <ts e="T8" id="Seg_63" n="e" s="T7">taugu </ts>
               <ts e="T9" id="Seg_65" n="e" s="T8">qoblam. </ts>
               <ts e="T10" id="Seg_67" n="e" s="T9">Okkɨr </ts>
               <ts e="T11" id="Seg_69" n="e" s="T10">qoboɣɨnto </ts>
               <ts e="T12" id="Seg_71" n="e" s="T11">ass </ts>
               <ts e="T13" id="Seg_73" n="e" s="T12">sütšal </ts>
               <ts e="T14" id="Seg_75" n="e" s="T13">porəɣɨm. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_76" s="T1">PVD_1964_FurCoat_nar.001 (001.001)</ta>
            <ta e="T9" id="Seg_77" s="T5">PVD_1964_FurCoat_nar.002 (001.002)</ta>
            <ta e="T14" id="Seg_78" s="T9">PVD_1964_FurCoat_nar.003 (001.003)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_79" s="T1">kоб′ла таумыдыт ′порɣым ′сетkу.</ta>
            <ta e="T9" id="Seg_80" s="T5">надъ ′ко̄цʼин ′таугу ′kоблам.</ta>
            <ta e="T14" id="Seg_81" s="T9">о′ккыр kо′боɣынто асс сӱ′тшал ′поръɣым.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_82" s="T1">qobla taumɨdɨt porɣɨm setqu.</ta>
            <ta e="T9" id="Seg_83" s="T5">nadə koːcʼin taugu qoblam.</ta>
            <ta e="T14" id="Seg_84" s="T9">okkɨr qoboɣɨnto ass sütšal porəɣɨm.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_85" s="T1">Qobla taumɨdɨt porɣɨm setqu. </ta>
            <ta e="T9" id="Seg_86" s="T5">Nadə koːcʼin taugu qoblam. </ta>
            <ta e="T14" id="Seg_87" s="T9">Okkɨr qoboɣɨnto ass sütšal porəɣɨm. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_88" s="T1">qob-la</ta>
            <ta e="T3" id="Seg_89" s="T2">tau-mɨ-dɨ-t</ta>
            <ta e="T4" id="Seg_90" s="T3">porɣɨ-m</ta>
            <ta e="T5" id="Seg_91" s="T4">set-qu</ta>
            <ta e="T6" id="Seg_92" s="T5">nadə</ta>
            <ta e="T7" id="Seg_93" s="T6">koːcʼi-n</ta>
            <ta e="T8" id="Seg_94" s="T7">tau-gu</ta>
            <ta e="T9" id="Seg_95" s="T8">qob-la-m</ta>
            <ta e="T10" id="Seg_96" s="T9">okkɨr</ta>
            <ta e="T11" id="Seg_97" s="T10">qob-o-ɣɨnto</ta>
            <ta e="T12" id="Seg_98" s="T11">ass</ta>
            <ta e="T13" id="Seg_99" s="T12">süt-ša-l</ta>
            <ta e="T14" id="Seg_100" s="T13">porəɣɨ-m</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_101" s="T1">qob-la</ta>
            <ta e="T3" id="Seg_102" s="T2">taw-mbɨ-ntɨ-t</ta>
            <ta e="T4" id="Seg_103" s="T3">porɣə-m</ta>
            <ta e="T5" id="Seg_104" s="T4">süt-gu</ta>
            <ta e="T6" id="Seg_105" s="T5">nadə</ta>
            <ta e="T7" id="Seg_106" s="T6">koːci-ŋ</ta>
            <ta e="T8" id="Seg_107" s="T7">taw-gu</ta>
            <ta e="T9" id="Seg_108" s="T8">qob-la-m</ta>
            <ta e="T10" id="Seg_109" s="T9">okkɨr</ta>
            <ta e="T11" id="Seg_110" s="T10">qob-ɨ-qɨntɨ</ta>
            <ta e="T12" id="Seg_111" s="T11">asa</ta>
            <ta e="T13" id="Seg_112" s="T12">süt-enǯɨ-l</ta>
            <ta e="T14" id="Seg_113" s="T13">porɣə-m</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_114" s="T1">skin-PL.[NOM]</ta>
            <ta e="T3" id="Seg_115" s="T2">buy-PST.NAR-INFER-3SG.O</ta>
            <ta e="T4" id="Seg_116" s="T3">fur.coat-ACC</ta>
            <ta e="T5" id="Seg_117" s="T4">sew-INF</ta>
            <ta e="T6" id="Seg_118" s="T5">one.should</ta>
            <ta e="T7" id="Seg_119" s="T6">much-ADVZ</ta>
            <ta e="T8" id="Seg_120" s="T7">buy-INF</ta>
            <ta e="T9" id="Seg_121" s="T8">skin-PL-ACC</ta>
            <ta e="T10" id="Seg_122" s="T9">one</ta>
            <ta e="T11" id="Seg_123" s="T10">skin-EP-EL.3SG</ta>
            <ta e="T12" id="Seg_124" s="T11">NEG</ta>
            <ta e="T13" id="Seg_125" s="T12">sew-FUT-2SG.O</ta>
            <ta e="T14" id="Seg_126" s="T13">fur.coat-ACC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_127" s="T1">шкура-PL.[NOM]</ta>
            <ta e="T3" id="Seg_128" s="T2">купить-PST.NAR-INFER-3SG.O</ta>
            <ta e="T4" id="Seg_129" s="T3">шуба-ACC</ta>
            <ta e="T5" id="Seg_130" s="T4">сшить-INF</ta>
            <ta e="T6" id="Seg_131" s="T5">надо</ta>
            <ta e="T7" id="Seg_132" s="T6">много-ADVZ</ta>
            <ta e="T8" id="Seg_133" s="T7">купить-INF</ta>
            <ta e="T9" id="Seg_134" s="T8">шкура-PL-ACC</ta>
            <ta e="T10" id="Seg_135" s="T9">один</ta>
            <ta e="T11" id="Seg_136" s="T10">шкура-EP-EL.3SG</ta>
            <ta e="T12" id="Seg_137" s="T11">NEG</ta>
            <ta e="T13" id="Seg_138" s="T12">сшить-FUT-2SG.O</ta>
            <ta e="T14" id="Seg_139" s="T13">шуба-ACC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_140" s="T1">n-n:num.[n:case]</ta>
            <ta e="T3" id="Seg_141" s="T2">v-v:tense-v:mood-v:pn</ta>
            <ta e="T4" id="Seg_142" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_143" s="T4">v-v:inf</ta>
            <ta e="T6" id="Seg_144" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_145" s="T6">quant-quant&gt;adv</ta>
            <ta e="T8" id="Seg_146" s="T7">v-v:inf</ta>
            <ta e="T9" id="Seg_147" s="T8">n-n:num-n:case</ta>
            <ta e="T10" id="Seg_148" s="T9">num</ta>
            <ta e="T11" id="Seg_149" s="T10">n-n:ins-n:case.poss</ta>
            <ta e="T12" id="Seg_150" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_151" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_152" s="T13">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_153" s="T1">n</ta>
            <ta e="T3" id="Seg_154" s="T2">n</ta>
            <ta e="T4" id="Seg_155" s="T3">n</ta>
            <ta e="T5" id="Seg_156" s="T4">v</ta>
            <ta e="T6" id="Seg_157" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_158" s="T6">adv</ta>
            <ta e="T8" id="Seg_159" s="T7">v</ta>
            <ta e="T9" id="Seg_160" s="T8">n</ta>
            <ta e="T10" id="Seg_161" s="T9">num</ta>
            <ta e="T11" id="Seg_162" s="T10">n</ta>
            <ta e="T12" id="Seg_163" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_164" s="T12">v</ta>
            <ta e="T14" id="Seg_165" s="T13">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_166" s="T1">np:Th</ta>
            <ta e="T3" id="Seg_167" s="T2">0.3.h:A</ta>
            <ta e="T4" id="Seg_168" s="T3">np:P</ta>
            <ta e="T8" id="Seg_169" s="T7">v:Th</ta>
            <ta e="T9" id="Seg_170" s="T8">np:Th</ta>
            <ta e="T11" id="Seg_171" s="T10">np:So</ta>
            <ta e="T13" id="Seg_172" s="T12">0.2.h:A</ta>
            <ta e="T14" id="Seg_173" s="T13">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_174" s="T1">np:O</ta>
            <ta e="T3" id="Seg_175" s="T2">0.3.h:S v:pred</ta>
            <ta e="T5" id="Seg_176" s="T3">s:purp</ta>
            <ta e="T6" id="Seg_177" s="T5">ptcl:pred</ta>
            <ta e="T8" id="Seg_178" s="T7">v:O</ta>
            <ta e="T13" id="Seg_179" s="T12">0.2.h:S v:pred</ta>
            <ta e="T14" id="Seg_180" s="T13">np:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T6" id="Seg_181" s="T5">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_182" s="T1">He bought a (sheep)skin to sew a fur coat.</ta>
            <ta e="T9" id="Seg_183" s="T5">One should buy many skins.</ta>
            <ta e="T14" id="Seg_184" s="T9">You can't sew a fur coat out of one skin.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_185" s="T1">Er kaufte einen (Schafs)pelz um einen Fellmantel zu nähen.</ta>
            <ta e="T9" id="Seg_186" s="T5">Man sollte viele Pelze kaufen.</ta>
            <ta e="T14" id="Seg_187" s="T9">Du kannst aus einem einzelnen Pelz keinen Fellmantel nähen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_188" s="T1">Он купил шкуру (овчину), чтобы шубу сшить.</ta>
            <ta e="T9" id="Seg_189" s="T5">Надо много купить шкур.</ta>
            <ta e="T14" id="Seg_190" s="T9">Из одной шкуры не сошьешь шубу.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_191" s="T1">овчины (шкур) купил шубу сшить (парку)</ta>
            <ta e="T9" id="Seg_192" s="T5">надо много купить шкур</ta>
            <ta e="T14" id="Seg_193" s="T9">из одной шкуры не сошьешь шубу</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
