<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Hares_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Hares_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">49</ud-information>
            <ud-information attribute-name="# HIAT:w">38</ud-information>
            <ud-information attribute-name="# e">38</ud-information>
            <ud-information attribute-name="# HIAT:u">9</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T39" id="Seg_0" n="sc" s="T1">
               <ts e="T12" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Menan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">loɣəla</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">i</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">warɣə</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">surəmla</ts>
                  <nts id="Seg_17" n="HIAT:ip">,</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_20" n="HIAT:w" s="T6">i</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_23" n="HIAT:w" s="T7">tʼübbənɛla</ts>
                  <nts id="Seg_24" n="HIAT:ip">,</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_27" n="HIAT:w" s="T8">i</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_30" n="HIAT:w" s="T9">malǯala</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_33" n="HIAT:w" s="T10">kocin</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_36" n="HIAT:w" s="T11">jewatdə</ts>
                  <nts id="Seg_37" n="HIAT:ip">.</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_40" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_42" n="HIAT:w" s="T12">Man</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_45" n="HIAT:w" s="T13">malǯalam</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_48" n="HIAT:w" s="T14">qwatkuzau</ts>
                  <nts id="Seg_49" n="HIAT:ip">.</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_52" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_54" n="HIAT:w" s="T15">Me</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_57" n="HIAT:w" s="T16">nürgunt</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_60" n="HIAT:w" s="T17">koːcin</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_63" n="HIAT:w" s="T18">jewattə</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_66" n="HIAT:w" s="T19">malǯala</ts>
                  <nts id="Seg_67" n="HIAT:ip">.</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_70" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_72" n="HIAT:w" s="T20">Tebla</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_75" n="HIAT:w" s="T21">tʼeːɣən</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_78" n="HIAT:w" s="T22">jewatdə</ts>
                  <nts id="Seg_79" n="HIAT:ip">.</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_82" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_84" n="HIAT:w" s="T23">Me</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_87" n="HIAT:w" s="T24">teblam</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_90" n="HIAT:w" s="T25">awkoft</ts>
                  <nts id="Seg_91" n="HIAT:ip">.</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_94" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_96" n="HIAT:w" s="T26">Tebla</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_99" n="HIAT:w" s="T27">nʼüːjettə</ts>
                  <nts id="Seg_100" n="HIAT:ip">.</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_103" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_105" n="HIAT:w" s="T28">Potku</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_108" n="HIAT:w" s="T29">nadə</ts>
                  <nts id="Seg_109" n="HIAT:ip">.</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_112" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_114" n="HIAT:w" s="T30">Man</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_117" n="HIAT:w" s="T31">sorau</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_120" n="HIAT:w" s="T32">malǯən</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_123" n="HIAT:w" s="T33">tobəm</ts>
                  <nts id="Seg_124" n="HIAT:ip">.</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_127" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_129" n="HIAT:w" s="T34">Ranʼše</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_132" n="HIAT:w" s="T35">erau</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_135" n="HIAT:w" s="T36">malǯam</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_138" n="HIAT:w" s="T37">koːcin</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_141" n="HIAT:w" s="T38">qwatkustʼ</ts>
                  <nts id="Seg_142" n="HIAT:ip">.</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T39" id="Seg_144" n="sc" s="T1">
               <ts e="T2" id="Seg_146" n="e" s="T1">Menan </ts>
               <ts e="T3" id="Seg_148" n="e" s="T2">loɣəla </ts>
               <ts e="T4" id="Seg_150" n="e" s="T3">i </ts>
               <ts e="T5" id="Seg_152" n="e" s="T4">warɣə </ts>
               <ts e="T6" id="Seg_154" n="e" s="T5">surəmla, </ts>
               <ts e="T7" id="Seg_156" n="e" s="T6">i </ts>
               <ts e="T8" id="Seg_158" n="e" s="T7">tʼübbənɛla, </ts>
               <ts e="T9" id="Seg_160" n="e" s="T8">i </ts>
               <ts e="T10" id="Seg_162" n="e" s="T9">malǯala </ts>
               <ts e="T11" id="Seg_164" n="e" s="T10">kocin </ts>
               <ts e="T12" id="Seg_166" n="e" s="T11">jewatdə. </ts>
               <ts e="T13" id="Seg_168" n="e" s="T12">Man </ts>
               <ts e="T14" id="Seg_170" n="e" s="T13">malǯalam </ts>
               <ts e="T15" id="Seg_172" n="e" s="T14">qwatkuzau. </ts>
               <ts e="T16" id="Seg_174" n="e" s="T15">Me </ts>
               <ts e="T17" id="Seg_176" n="e" s="T16">nürgunt </ts>
               <ts e="T18" id="Seg_178" n="e" s="T17">koːcin </ts>
               <ts e="T19" id="Seg_180" n="e" s="T18">jewattə </ts>
               <ts e="T20" id="Seg_182" n="e" s="T19">malǯala. </ts>
               <ts e="T21" id="Seg_184" n="e" s="T20">Tebla </ts>
               <ts e="T22" id="Seg_186" n="e" s="T21">tʼeːɣən </ts>
               <ts e="T23" id="Seg_188" n="e" s="T22">jewatdə. </ts>
               <ts e="T24" id="Seg_190" n="e" s="T23">Me </ts>
               <ts e="T25" id="Seg_192" n="e" s="T24">teblam </ts>
               <ts e="T26" id="Seg_194" n="e" s="T25">awkoft. </ts>
               <ts e="T27" id="Seg_196" n="e" s="T26">Tebla </ts>
               <ts e="T28" id="Seg_198" n="e" s="T27">nʼüːjettə. </ts>
               <ts e="T29" id="Seg_200" n="e" s="T28">Potku </ts>
               <ts e="T30" id="Seg_202" n="e" s="T29">nadə. </ts>
               <ts e="T31" id="Seg_204" n="e" s="T30">Man </ts>
               <ts e="T32" id="Seg_206" n="e" s="T31">sorau </ts>
               <ts e="T33" id="Seg_208" n="e" s="T32">malǯən </ts>
               <ts e="T34" id="Seg_210" n="e" s="T33">tobəm. </ts>
               <ts e="T35" id="Seg_212" n="e" s="T34">Ranʼše </ts>
               <ts e="T36" id="Seg_214" n="e" s="T35">erau </ts>
               <ts e="T37" id="Seg_216" n="e" s="T36">malǯam </ts>
               <ts e="T38" id="Seg_218" n="e" s="T37">koːcin </ts>
               <ts e="T39" id="Seg_220" n="e" s="T38">qwatkustʼ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T12" id="Seg_221" s="T1">PVD_1964_Hares_nar.001 (001.001)</ta>
            <ta e="T15" id="Seg_222" s="T12">PVD_1964_Hares_nar.002 (001.002)</ta>
            <ta e="T20" id="Seg_223" s="T15">PVD_1964_Hares_nar.003 (001.003)</ta>
            <ta e="T23" id="Seg_224" s="T20">PVD_1964_Hares_nar.004 (001.004)</ta>
            <ta e="T26" id="Seg_225" s="T23">PVD_1964_Hares_nar.005 (001.005)</ta>
            <ta e="T28" id="Seg_226" s="T26">PVD_1964_Hares_nar.006 (001.006)</ta>
            <ta e="T30" id="Seg_227" s="T28">PVD_1964_Hares_nar.007 (001.007)</ta>
            <ta e="T34" id="Seg_228" s="T30">PVD_1964_Hares_nar.008 (001.008)</ta>
            <ta e="T39" id="Seg_229" s="T34">PVD_1964_Hares_nar.009 (001.009)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T12" id="Seg_230" s="T1">ме′нан ′лоɣъла и ′варɣъ ′суръ(ы)мла, и тʼӱб̂бъ(у)нɛла, и ′малджала ′коцин ′jеватдъ.</ta>
            <ta e="T15" id="Seg_231" s="T12">ман малджалам ′kwатку‵зау̹.</ta>
            <ta e="T20" id="Seg_232" s="T15">ме нӱр′гунт ′ко̄цин ′jеwаттъ ′малджала.</ta>
            <ta e="T23" id="Seg_233" s="T20">теб′ла ′тʼе̄ɣън ′jеwатд̂ъ.</ta>
            <ta e="T26" id="Seg_234" s="T23">ме теб′лам аw′кофт.</ta>
            <ta e="T28" id="Seg_235" s="T26">теб′ла нʼӱ̄ ′jеттъ.</ta>
            <ta e="T30" id="Seg_236" s="T28">′потку надъ.</ta>
            <ta e="T34" id="Seg_237" s="T30">ман ′сорау̹ ′малджъ(а)н ′тобъм.</ta>
            <ta e="T39" id="Seg_238" s="T34">ранʼше е′рау̹ ′малджам ко̄цин kwат′кусть.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T12" id="Seg_239" s="T1">menan loɣəla i warɣə surə(ɨ)mla, i tʼüb̂bə(u)nɛla, i malǯala kocin jewatdə.</ta>
            <ta e="T15" id="Seg_240" s="T12">man malǯalam qwatkuzau̹.</ta>
            <ta e="T20" id="Seg_241" s="T15">me nürgunt koːcin jewattə malǯala.</ta>
            <ta e="T23" id="Seg_242" s="T20">tebla tʼeːɣən jewatd̂ə.</ta>
            <ta e="T26" id="Seg_243" s="T23">me teblam awkoft.</ta>
            <ta e="T28" id="Seg_244" s="T26">tebla nʼüː jettə.</ta>
            <ta e="T30" id="Seg_245" s="T28">potku nadə.</ta>
            <ta e="T34" id="Seg_246" s="T30">man sorau̹ malǯə(a)n tobəm.</ta>
            <ta e="T39" id="Seg_247" s="T34">ranʼše erau̹ malǯam koːcin qwatkustʼ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T12" id="Seg_248" s="T1">Menan loɣəla i warɣə surəmla, i tʼübbənɛla, i malǯala kocin jewatdə. </ta>
            <ta e="T15" id="Seg_249" s="T12">Man malǯalam qwatkuzau. </ta>
            <ta e="T20" id="Seg_250" s="T15">Me nürgunt koːcin jewattə malǯala. </ta>
            <ta e="T23" id="Seg_251" s="T20">Tebla tʼeːɣən jewatdə. </ta>
            <ta e="T26" id="Seg_252" s="T23">Me teblam awkoft. </ta>
            <ta e="T28" id="Seg_253" s="T26">Tebla nʼüːjettə. </ta>
            <ta e="T30" id="Seg_254" s="T28">Potku nadə. </ta>
            <ta e="T34" id="Seg_255" s="T30">Man sorau malǯən tobəm. </ta>
            <ta e="T39" id="Seg_256" s="T34">Ranʼše erau malǯam koːcin qwatkustʼ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_257" s="T1">me-nan</ta>
            <ta e="T3" id="Seg_258" s="T2">loɣə-la</ta>
            <ta e="T4" id="Seg_259" s="T3">i</ta>
            <ta e="T5" id="Seg_260" s="T4">warɣə</ta>
            <ta e="T6" id="Seg_261" s="T5">surəm-la</ta>
            <ta e="T7" id="Seg_262" s="T6">i</ta>
            <ta e="T8" id="Seg_263" s="T7">tʼübbənɛ-la</ta>
            <ta e="T9" id="Seg_264" s="T8">i</ta>
            <ta e="T10" id="Seg_265" s="T9">malǯa-la</ta>
            <ta e="T11" id="Seg_266" s="T10">koci-n</ta>
            <ta e="T12" id="Seg_267" s="T11">je-wa-tdə</ta>
            <ta e="T13" id="Seg_268" s="T12">man</ta>
            <ta e="T14" id="Seg_269" s="T13">malǯa-la-m</ta>
            <ta e="T15" id="Seg_270" s="T14">qwat-ku-za-u</ta>
            <ta e="T16" id="Seg_271" s="T15">me</ta>
            <ta e="T17" id="Seg_272" s="T16">nür-gunt</ta>
            <ta e="T18" id="Seg_273" s="T17">koːci-n</ta>
            <ta e="T19" id="Seg_274" s="T18">je-wa-ttə</ta>
            <ta e="T20" id="Seg_275" s="T19">malǯa-la</ta>
            <ta e="T21" id="Seg_276" s="T20">teb-la</ta>
            <ta e="T22" id="Seg_277" s="T21">tʼeːɣə-n</ta>
            <ta e="T23" id="Seg_278" s="T22">je-wa-tdə</ta>
            <ta e="T24" id="Seg_279" s="T23">me</ta>
            <ta e="T25" id="Seg_280" s="T24">teb-la-m</ta>
            <ta e="T26" id="Seg_281" s="T25">aw-ko-ft</ta>
            <ta e="T27" id="Seg_282" s="T26">teb-la</ta>
            <ta e="T28" id="Seg_283" s="T27">nʼüːje-ttə</ta>
            <ta e="T29" id="Seg_284" s="T28">pot-ku</ta>
            <ta e="T30" id="Seg_285" s="T29">nadə</ta>
            <ta e="T31" id="Seg_286" s="T30">man</ta>
            <ta e="T32" id="Seg_287" s="T31">sora-u</ta>
            <ta e="T33" id="Seg_288" s="T32">malǯə-n</ta>
            <ta e="T34" id="Seg_289" s="T33">tob-ə-m</ta>
            <ta e="T35" id="Seg_290" s="T34">ranʼše</ta>
            <ta e="T36" id="Seg_291" s="T35">era-u</ta>
            <ta e="T37" id="Seg_292" s="T36">malǯa-m</ta>
            <ta e="T38" id="Seg_293" s="T37">koːci-n</ta>
            <ta e="T39" id="Seg_294" s="T38">qwat-ku-s-tʼ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_295" s="T1">me-nan</ta>
            <ta e="T3" id="Seg_296" s="T2">loɣa-la</ta>
            <ta e="T4" id="Seg_297" s="T3">i</ta>
            <ta e="T5" id="Seg_298" s="T4">wargɨ</ta>
            <ta e="T6" id="Seg_299" s="T5">suːrum-la</ta>
            <ta e="T7" id="Seg_300" s="T6">i</ta>
            <ta e="T8" id="Seg_301" s="T7">tʼübbəne-la</ta>
            <ta e="T9" id="Seg_302" s="T8">i</ta>
            <ta e="T10" id="Seg_303" s="T9">malǯa-la</ta>
            <ta e="T11" id="Seg_304" s="T10">koːci-ŋ</ta>
            <ta e="T12" id="Seg_305" s="T11">eː-nɨ-tɨn</ta>
            <ta e="T13" id="Seg_306" s="T12">man</ta>
            <ta e="T14" id="Seg_307" s="T13">malǯa-la-m</ta>
            <ta e="T15" id="Seg_308" s="T14">qwat-ku-sɨ-w</ta>
            <ta e="T16" id="Seg_309" s="T15">me</ta>
            <ta e="T17" id="Seg_310" s="T16">nür-qɨntɨ</ta>
            <ta e="T18" id="Seg_311" s="T17">koːci-ŋ</ta>
            <ta e="T19" id="Seg_312" s="T18">eː-nɨ-tɨn</ta>
            <ta e="T20" id="Seg_313" s="T19">malǯa-la</ta>
            <ta e="T21" id="Seg_314" s="T20">täp-la</ta>
            <ta e="T22" id="Seg_315" s="T21">tʼeɣə-ŋ</ta>
            <ta e="T23" id="Seg_316" s="T22">eː-nɨ-tɨn</ta>
            <ta e="T24" id="Seg_317" s="T23">me</ta>
            <ta e="T25" id="Seg_318" s="T24">täp-la-m</ta>
            <ta e="T26" id="Seg_319" s="T25">am-ku-un</ta>
            <ta e="T27" id="Seg_320" s="T26">täp-la</ta>
            <ta e="T28" id="Seg_321" s="T27">nʼuːi-tɨn</ta>
            <ta e="T29" id="Seg_322" s="T28">poːt-gu</ta>
            <ta e="T30" id="Seg_323" s="T29">nadə</ta>
            <ta e="T31" id="Seg_324" s="T30">man</ta>
            <ta e="T32" id="Seg_325" s="T31">soːrɨ-w</ta>
            <ta e="T33" id="Seg_326" s="T32">malǯa-n</ta>
            <ta e="T34" id="Seg_327" s="T33">tob-ɨ-m</ta>
            <ta e="T35" id="Seg_328" s="T34">ranʼše</ta>
            <ta e="T36" id="Seg_329" s="T35">era-nɨ</ta>
            <ta e="T37" id="Seg_330" s="T36">malǯa-m</ta>
            <ta e="T38" id="Seg_331" s="T37">koːci-ŋ</ta>
            <ta e="T39" id="Seg_332" s="T38">qwat-ku-sɨ-t</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_333" s="T1">we-ADES</ta>
            <ta e="T3" id="Seg_334" s="T2">fox-PL.[NOM]</ta>
            <ta e="T4" id="Seg_335" s="T3">and</ta>
            <ta e="T5" id="Seg_336" s="T4">big</ta>
            <ta e="T6" id="Seg_337" s="T5">wild.animal-PL.[NOM]</ta>
            <ta e="T7" id="Seg_338" s="T6">and</ta>
            <ta e="T8" id="Seg_339" s="T7">wolf-PL.[NOM]</ta>
            <ta e="T9" id="Seg_340" s="T8">and</ta>
            <ta e="T10" id="Seg_341" s="T9">hare-PL.[NOM]</ta>
            <ta e="T11" id="Seg_342" s="T10">much-ADVZ</ta>
            <ta e="T12" id="Seg_343" s="T11">be-CO-3PL</ta>
            <ta e="T13" id="Seg_344" s="T12">I.NOM</ta>
            <ta e="T14" id="Seg_345" s="T13">hare-PL-ACC</ta>
            <ta e="T15" id="Seg_346" s="T14">catch-HAB-PST-1SG.O</ta>
            <ta e="T16" id="Seg_347" s="T15">we.GEN</ta>
            <ta e="T17" id="Seg_348" s="T16">field-LOC.3SG</ta>
            <ta e="T18" id="Seg_349" s="T17">much-ADVZ</ta>
            <ta e="T19" id="Seg_350" s="T18">be-CO-3PL</ta>
            <ta e="T20" id="Seg_351" s="T19">hare-PL.[NOM]</ta>
            <ta e="T21" id="Seg_352" s="T20">(s)he-PL.[NOM]</ta>
            <ta e="T22" id="Seg_353" s="T21">white-ADVZ</ta>
            <ta e="T23" id="Seg_354" s="T22">be-CO-3PL</ta>
            <ta e="T24" id="Seg_355" s="T23">we.GEN</ta>
            <ta e="T25" id="Seg_356" s="T24">(s)he-PL-ACC</ta>
            <ta e="T26" id="Seg_357" s="T25">eat-HAB-1PL</ta>
            <ta e="T27" id="Seg_358" s="T26">(s)he-PL.[NOM]</ta>
            <ta e="T28" id="Seg_359" s="T27">be.sweet-3PL</ta>
            <ta e="T29" id="Seg_360" s="T28">cook-INF</ta>
            <ta e="T30" id="Seg_361" s="T29">one.should</ta>
            <ta e="T31" id="Seg_362" s="T30">I.NOM</ta>
            <ta e="T32" id="Seg_363" s="T31">love-1SG.O</ta>
            <ta e="T33" id="Seg_364" s="T32">hare-GEN</ta>
            <ta e="T34" id="Seg_365" s="T33">leg-EP-ACC</ta>
            <ta e="T35" id="Seg_366" s="T34">earlier</ta>
            <ta e="T36" id="Seg_367" s="T35">husband.[NOM]-1SG</ta>
            <ta e="T37" id="Seg_368" s="T36">hare-ACC</ta>
            <ta e="T38" id="Seg_369" s="T37">much-ADVZ</ta>
            <ta e="T39" id="Seg_370" s="T38">kill-HAB-PST-3SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_371" s="T1">мы-ADES</ta>
            <ta e="T3" id="Seg_372" s="T2">лиса-PL.[NOM]</ta>
            <ta e="T4" id="Seg_373" s="T3">и</ta>
            <ta e="T5" id="Seg_374" s="T4">большой</ta>
            <ta e="T6" id="Seg_375" s="T5">зверь-PL.[NOM]</ta>
            <ta e="T7" id="Seg_376" s="T6">и</ta>
            <ta e="T8" id="Seg_377" s="T7">волк-PL.[NOM]</ta>
            <ta e="T9" id="Seg_378" s="T8">и</ta>
            <ta e="T10" id="Seg_379" s="T9">заяц-PL.[NOM]</ta>
            <ta e="T11" id="Seg_380" s="T10">много-ADVZ</ta>
            <ta e="T12" id="Seg_381" s="T11">быть-CO-3PL</ta>
            <ta e="T13" id="Seg_382" s="T12">я.NOM</ta>
            <ta e="T14" id="Seg_383" s="T13">заяц-PL-ACC</ta>
            <ta e="T15" id="Seg_384" s="T14">поймать-HAB-PST-1SG.O</ta>
            <ta e="T16" id="Seg_385" s="T15">мы.GEN</ta>
            <ta e="T17" id="Seg_386" s="T16">поле-LOC.3SG</ta>
            <ta e="T18" id="Seg_387" s="T17">много-ADVZ</ta>
            <ta e="T19" id="Seg_388" s="T18">быть-CO-3PL</ta>
            <ta e="T20" id="Seg_389" s="T19">заяц-PL.[NOM]</ta>
            <ta e="T21" id="Seg_390" s="T20">он(а)-PL.[NOM]</ta>
            <ta e="T22" id="Seg_391" s="T21">белый-ADVZ</ta>
            <ta e="T23" id="Seg_392" s="T22">быть-CO-3PL</ta>
            <ta e="T24" id="Seg_393" s="T23">мы.GEN</ta>
            <ta e="T25" id="Seg_394" s="T24">он(а)-PL-ACC</ta>
            <ta e="T26" id="Seg_395" s="T25">съесть-HAB-1PL</ta>
            <ta e="T27" id="Seg_396" s="T26">он(а)-PL.[NOM]</ta>
            <ta e="T28" id="Seg_397" s="T27">быть.сладким-3PL</ta>
            <ta e="T29" id="Seg_398" s="T28">сварить-INF</ta>
            <ta e="T30" id="Seg_399" s="T29">надо</ta>
            <ta e="T31" id="Seg_400" s="T30">я.NOM</ta>
            <ta e="T32" id="Seg_401" s="T31">любить-1SG.O</ta>
            <ta e="T33" id="Seg_402" s="T32">заяц-GEN</ta>
            <ta e="T34" id="Seg_403" s="T33">нога-EP-ACC</ta>
            <ta e="T35" id="Seg_404" s="T34">раньше</ta>
            <ta e="T36" id="Seg_405" s="T35">муж.[NOM]-1SG</ta>
            <ta e="T37" id="Seg_406" s="T36">заяц-ACC</ta>
            <ta e="T38" id="Seg_407" s="T37">много-ADVZ</ta>
            <ta e="T39" id="Seg_408" s="T38">убить-HAB-PST-3SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_409" s="T1">pers-n:case</ta>
            <ta e="T3" id="Seg_410" s="T2">n-n:num.[n:case]</ta>
            <ta e="T4" id="Seg_411" s="T3">conj</ta>
            <ta e="T5" id="Seg_412" s="T4">adj</ta>
            <ta e="T6" id="Seg_413" s="T5">n-n:num.[n:case]</ta>
            <ta e="T7" id="Seg_414" s="T6">conj</ta>
            <ta e="T8" id="Seg_415" s="T7">n-n:num.[n:case]</ta>
            <ta e="T9" id="Seg_416" s="T8">conj</ta>
            <ta e="T10" id="Seg_417" s="T9">n-n:num.[n:case]</ta>
            <ta e="T11" id="Seg_418" s="T10">quant-adj&gt;adv</ta>
            <ta e="T12" id="Seg_419" s="T11">v-v:ins-v:pn</ta>
            <ta e="T13" id="Seg_420" s="T12">pers</ta>
            <ta e="T14" id="Seg_421" s="T13">n-n:num-n:case</ta>
            <ta e="T15" id="Seg_422" s="T14">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_423" s="T15">pers</ta>
            <ta e="T17" id="Seg_424" s="T16">n-n:case.poss</ta>
            <ta e="T18" id="Seg_425" s="T17">quant-quant&gt;adv</ta>
            <ta e="T19" id="Seg_426" s="T18">v-v:ins-v:pn</ta>
            <ta e="T20" id="Seg_427" s="T19">n-n:num.[n:case]</ta>
            <ta e="T21" id="Seg_428" s="T20">pers-n:num.[n:case]</ta>
            <ta e="T22" id="Seg_429" s="T21">adj-adj&gt;adv</ta>
            <ta e="T23" id="Seg_430" s="T22">v-v:ins-v:pn</ta>
            <ta e="T24" id="Seg_431" s="T23">pers</ta>
            <ta e="T25" id="Seg_432" s="T24">pers-n:num-n:case</ta>
            <ta e="T26" id="Seg_433" s="T25">v-v&gt;v-v:pn</ta>
            <ta e="T27" id="Seg_434" s="T26">pers-n:num.[n:case]</ta>
            <ta e="T28" id="Seg_435" s="T27">v-v:pn</ta>
            <ta e="T29" id="Seg_436" s="T28">v-v:inf</ta>
            <ta e="T30" id="Seg_437" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_438" s="T30">pers</ta>
            <ta e="T32" id="Seg_439" s="T31">v-v:pn</ta>
            <ta e="T33" id="Seg_440" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_441" s="T33">n-n:ins-n:case</ta>
            <ta e="T35" id="Seg_442" s="T34">adv</ta>
            <ta e="T36" id="Seg_443" s="T35">n.[n:case]-n:poss</ta>
            <ta e="T37" id="Seg_444" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_445" s="T37">quant-quant&gt;adv</ta>
            <ta e="T39" id="Seg_446" s="T38">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_447" s="T1">pers</ta>
            <ta e="T3" id="Seg_448" s="T2">n</ta>
            <ta e="T4" id="Seg_449" s="T3">conj</ta>
            <ta e="T5" id="Seg_450" s="T4">adj</ta>
            <ta e="T6" id="Seg_451" s="T5">n</ta>
            <ta e="T7" id="Seg_452" s="T6">conj</ta>
            <ta e="T8" id="Seg_453" s="T7">n</ta>
            <ta e="T9" id="Seg_454" s="T8">conj</ta>
            <ta e="T10" id="Seg_455" s="T9">n</ta>
            <ta e="T11" id="Seg_456" s="T10">quant</ta>
            <ta e="T12" id="Seg_457" s="T11">v</ta>
            <ta e="T13" id="Seg_458" s="T12">pers</ta>
            <ta e="T14" id="Seg_459" s="T13">n</ta>
            <ta e="T15" id="Seg_460" s="T14">v</ta>
            <ta e="T16" id="Seg_461" s="T15">pers</ta>
            <ta e="T17" id="Seg_462" s="T16">n</ta>
            <ta e="T18" id="Seg_463" s="T17">quant</ta>
            <ta e="T19" id="Seg_464" s="T18">v</ta>
            <ta e="T20" id="Seg_465" s="T19">n</ta>
            <ta e="T21" id="Seg_466" s="T20">pers</ta>
            <ta e="T22" id="Seg_467" s="T21">adv</ta>
            <ta e="T23" id="Seg_468" s="T22">v</ta>
            <ta e="T24" id="Seg_469" s="T23">pers</ta>
            <ta e="T25" id="Seg_470" s="T24">pers</ta>
            <ta e="T26" id="Seg_471" s="T25">v</ta>
            <ta e="T27" id="Seg_472" s="T26">pers</ta>
            <ta e="T28" id="Seg_473" s="T27">v</ta>
            <ta e="T29" id="Seg_474" s="T28">v</ta>
            <ta e="T30" id="Seg_475" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_476" s="T30">pers</ta>
            <ta e="T32" id="Seg_477" s="T31">v</ta>
            <ta e="T33" id="Seg_478" s="T32">n</ta>
            <ta e="T34" id="Seg_479" s="T33">n</ta>
            <ta e="T35" id="Seg_480" s="T34">adv</ta>
            <ta e="T36" id="Seg_481" s="T35">n</ta>
            <ta e="T37" id="Seg_482" s="T36">n</ta>
            <ta e="T38" id="Seg_483" s="T37">quant</ta>
            <ta e="T39" id="Seg_484" s="T38">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_485" s="T1">pro.h:Poss</ta>
            <ta e="T3" id="Seg_486" s="T2">np:Th</ta>
            <ta e="T6" id="Seg_487" s="T5">np:Th</ta>
            <ta e="T8" id="Seg_488" s="T7">np:Th</ta>
            <ta e="T10" id="Seg_489" s="T9">np:Th</ta>
            <ta e="T13" id="Seg_490" s="T12">pro.h:A</ta>
            <ta e="T14" id="Seg_491" s="T13">np:Th</ta>
            <ta e="T16" id="Seg_492" s="T15">pro.h:Poss</ta>
            <ta e="T17" id="Seg_493" s="T16">np:L</ta>
            <ta e="T20" id="Seg_494" s="T19">np:Th</ta>
            <ta e="T21" id="Seg_495" s="T20">pro.h:Th</ta>
            <ta e="T24" id="Seg_496" s="T23">pro.h:A</ta>
            <ta e="T25" id="Seg_497" s="T24">pro:P</ta>
            <ta e="T27" id="Seg_498" s="T26">pro:Th</ta>
            <ta e="T29" id="Seg_499" s="T28">v:Th</ta>
            <ta e="T31" id="Seg_500" s="T30">pro.h:E</ta>
            <ta e="T33" id="Seg_501" s="T32">np:Poss</ta>
            <ta e="T34" id="Seg_502" s="T33">np:Th</ta>
            <ta e="T35" id="Seg_503" s="T34">adv:Time</ta>
            <ta e="T36" id="Seg_504" s="T35">np.h:A 0.1.h:Poss</ta>
            <ta e="T37" id="Seg_505" s="T36">np:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_506" s="T2">np:S</ta>
            <ta e="T6" id="Seg_507" s="T5">np:S</ta>
            <ta e="T8" id="Seg_508" s="T7">np:S</ta>
            <ta e="T10" id="Seg_509" s="T9">np:S</ta>
            <ta e="T12" id="Seg_510" s="T11">v:pred</ta>
            <ta e="T13" id="Seg_511" s="T12">pro.h:S</ta>
            <ta e="T14" id="Seg_512" s="T13">np:O</ta>
            <ta e="T15" id="Seg_513" s="T14">v:pred</ta>
            <ta e="T19" id="Seg_514" s="T18">v:pred</ta>
            <ta e="T20" id="Seg_515" s="T19">np:S</ta>
            <ta e="T21" id="Seg_516" s="T20">pro.h:S</ta>
            <ta e="T23" id="Seg_517" s="T22">v:pred</ta>
            <ta e="T24" id="Seg_518" s="T23">pro.h:S</ta>
            <ta e="T25" id="Seg_519" s="T24">pro:O</ta>
            <ta e="T26" id="Seg_520" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_521" s="T26">pro:S</ta>
            <ta e="T28" id="Seg_522" s="T27">v:pred</ta>
            <ta e="T29" id="Seg_523" s="T28">v:O</ta>
            <ta e="T30" id="Seg_524" s="T29">ptcl:pred</ta>
            <ta e="T31" id="Seg_525" s="T30">pro.h:S</ta>
            <ta e="T32" id="Seg_526" s="T31">v:pred</ta>
            <ta e="T34" id="Seg_527" s="T33">np:O</ta>
            <ta e="T36" id="Seg_528" s="T35">np.h:S</ta>
            <ta e="T37" id="Seg_529" s="T36">np:O</ta>
            <ta e="T39" id="Seg_530" s="T38">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_531" s="T3">RUS:gram</ta>
            <ta e="T7" id="Seg_532" s="T6">RUS:gram</ta>
            <ta e="T9" id="Seg_533" s="T8">RUS:gram</ta>
            <ta e="T30" id="Seg_534" s="T29">RUS:mod</ta>
            <ta e="T35" id="Seg_535" s="T34">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T12" id="Seg_536" s="T1">We have foxes, and big animals, and wolves, and many hares.</ta>
            <ta e="T15" id="Seg_537" s="T12">I used to hunt hares.</ta>
            <ta e="T20" id="Seg_538" s="T15">There are many hares in our fields.</ta>
            <ta e="T23" id="Seg_539" s="T20">They are white.</ta>
            <ta e="T26" id="Seg_540" s="T23">We eat them.</ta>
            <ta e="T28" id="Seg_541" s="T26">They are tasty.</ta>
            <ta e="T30" id="Seg_542" s="T28">One should boil them.</ta>
            <ta e="T34" id="Seg_543" s="T30">I like hare's legs.</ta>
            <ta e="T39" id="Seg_544" s="T34">My husband used to kill many hares.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T12" id="Seg_545" s="T1">Wir haben Füchse und große Tiere und Wölfe und viele Hasen.</ta>
            <ta e="T15" id="Seg_546" s="T12">Ich habe immer Hasen gejagt.</ta>
            <ta e="T20" id="Seg_547" s="T15">In unseren Feldern sind viele Hasen.</ta>
            <ta e="T23" id="Seg_548" s="T20">Sie sind weiß.</ta>
            <ta e="T26" id="Seg_549" s="T23">Wir essen sie.</ta>
            <ta e="T28" id="Seg_550" s="T26">Sie sind lecker.</ta>
            <ta e="T30" id="Seg_551" s="T28">Man sollte sie kochen.</ta>
            <ta e="T34" id="Seg_552" s="T30">Ich mag Hasenkeulen.</ta>
            <ta e="T39" id="Seg_553" s="T34">Mein Mann hat früher immer viele Hasen getötet.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T12" id="Seg_554" s="T1">У нас лисы есть, и большие звери, и волки, и зайцев много.</ta>
            <ta e="T15" id="Seg_555" s="T12">Я зайцев добывала.</ta>
            <ta e="T20" id="Seg_556" s="T15">В нашем поле много зайцев.</ta>
            <ta e="T23" id="Seg_557" s="T20">Они белые.</ta>
            <ta e="T26" id="Seg_558" s="T23">Мы их едим.</ta>
            <ta e="T28" id="Seg_559" s="T26">Они вкусные.</ta>
            <ta e="T30" id="Seg_560" s="T28">Варить надо.</ta>
            <ta e="T34" id="Seg_561" s="T30">Я люблю зайчью ножку.</ta>
            <ta e="T39" id="Seg_562" s="T34">Раньше мой муж зайцев много добывал.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T12" id="Seg_563" s="T1">у нас лисы и большие звери и волки и зайцев много</ta>
            <ta e="T15" id="Seg_564" s="T12">я зайцев добывала (убивала)</ta>
            <ta e="T20" id="Seg_565" s="T15">в нашем лесу много зайцев</ta>
            <ta e="T23" id="Seg_566" s="T20">они белые</ta>
            <ta e="T26" id="Seg_567" s="T23">мы их едим</ta>
            <ta e="T28" id="Seg_568" s="T26">они вкусные</ta>
            <ta e="T30" id="Seg_569" s="T28">варить надо</ta>
            <ta e="T34" id="Seg_570" s="T30">я люблю зайцев ножку</ta>
            <ta e="T39" id="Seg_571" s="T34">раньше мой муж зайцев много добывал</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T12" id="Seg_572" s="T1">[KuAI:] Variants: 'surɨmla', 'tʼübbunɛla'.</ta>
            <ta e="T20" id="Seg_573" s="T15">[BrM:] In original translation: "In our forest…".</ta>
            <ta e="T28" id="Seg_574" s="T26">[BrM:] 'nʼüː jettə' changed to 'nʼüːjettə'.</ta>
            <ta e="T34" id="Seg_575" s="T30">[KuAI:] Variant: 'malǯan'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
