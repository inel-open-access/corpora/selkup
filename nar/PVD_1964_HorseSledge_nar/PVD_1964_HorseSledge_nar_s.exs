<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_HorseSledge_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_HorseSledge_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">54</ud-information>
            <ud-information attribute-name="# HIAT:w">43</ud-information>
            <ud-information attribute-name="# e">43</ud-information>
            <ud-information attribute-name="# HIAT:u">9</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T44" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">A</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">man</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">tromniksʼe</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">qwaǯan</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Satdʼijedoɣon</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">kütdəlam</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">oɣulǯoʒelʼe</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">taːderət</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">taɣɨn</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_35" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">Tromniktə</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">taqtolǯuqwattə</ts>
                  <nts id="Seg_41" n="HIAT:ip">,</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">nildʼzʼin</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">i</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">oɣulǯoʒuqwattə</ts>
                  <nts id="Seg_51" n="HIAT:ip">.</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_54" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">Ranʼše</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">menan</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">kan</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">oɣulǯoʒelʼe</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">taːdərkuzattə</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_71" n="HIAT:w" s="T20">kütdəlam</ts>
                  <nts id="Seg_72" n="HIAT:ip">.</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_75" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_77" n="HIAT:w" s="T21">Oqqɨr</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_80" n="HIAT:w" s="T22">teböɣum</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_83" n="HIAT:w" s="T23">Uːdʼarʼedɨ</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_86" n="HIAT:w" s="T24">qütʼdɨmdə</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_89" n="HIAT:w" s="T25">oɣulǯoʒelʼe</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_92" n="HIAT:w" s="T26">tadərest</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_96" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_98" n="HIAT:w" s="T27">Qötdɨt</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_101" n="HIAT:w" s="T28">kak</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_104" n="HIAT:w" s="T29">kurolǯa</ts>
                  <nts id="Seg_105" n="HIAT:ip">,</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_108" n="HIAT:w" s="T30">šerdʼälan</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_111" n="HIAT:w" s="T31">parmun</ts>
                  <nts id="Seg_112" n="HIAT:ip">.</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_115" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_117" n="HIAT:w" s="T32">I</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_120" n="HIAT:w" s="T33">šerdʼa</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_123" n="HIAT:w" s="T34">qutder</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_126" n="HIAT:w" s="T35">tʼöqolǯut</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_129" n="HIAT:w" s="T36">naǯout</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_132" n="HIAT:w" s="T37">i</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_135" n="HIAT:w" s="T38">moɣolmət</ts>
                  <nts id="Seg_136" n="HIAT:ip">.</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_139" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_141" n="HIAT:w" s="T39">Tiː</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_144" n="HIAT:w" s="T40">qončit</ts>
                  <nts id="Seg_145" n="HIAT:ip">.</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_148" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_150" n="HIAT:w" s="T41">Tep</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_153" n="HIAT:w" s="T42">i</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_156" n="HIAT:w" s="T43">kuːwa</ts>
                  <nts id="Seg_157" n="HIAT:ip">.</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T44" id="Seg_159" n="sc" s="T1">
               <ts e="T2" id="Seg_161" n="e" s="T1">A </ts>
               <ts e="T3" id="Seg_163" n="e" s="T2">man </ts>
               <ts e="T4" id="Seg_165" n="e" s="T3">tromniksʼe </ts>
               <ts e="T5" id="Seg_167" n="e" s="T4">qwaǯan. </ts>
               <ts e="T6" id="Seg_169" n="e" s="T5">Satdʼijedoɣon </ts>
               <ts e="T7" id="Seg_171" n="e" s="T6">kütdəlam </ts>
               <ts e="T8" id="Seg_173" n="e" s="T7">oɣulǯoʒelʼe </ts>
               <ts e="T9" id="Seg_175" n="e" s="T8">taːderət </ts>
               <ts e="T10" id="Seg_177" n="e" s="T9">taɣɨn. </ts>
               <ts e="T11" id="Seg_179" n="e" s="T10">Tromniktə </ts>
               <ts e="T12" id="Seg_181" n="e" s="T11">taqtolǯuqwattə, </ts>
               <ts e="T13" id="Seg_183" n="e" s="T12">nildʼzʼin </ts>
               <ts e="T14" id="Seg_185" n="e" s="T13">i </ts>
               <ts e="T15" id="Seg_187" n="e" s="T14">oɣulǯoʒuqwattə. </ts>
               <ts e="T16" id="Seg_189" n="e" s="T15">Ranʼše </ts>
               <ts e="T17" id="Seg_191" n="e" s="T16">menan </ts>
               <ts e="T18" id="Seg_193" n="e" s="T17">kan </ts>
               <ts e="T19" id="Seg_195" n="e" s="T18">oɣulǯoʒelʼe </ts>
               <ts e="T20" id="Seg_197" n="e" s="T19">taːdərkuzattə </ts>
               <ts e="T21" id="Seg_199" n="e" s="T20">kütdəlam. </ts>
               <ts e="T22" id="Seg_201" n="e" s="T21">Oqqɨr </ts>
               <ts e="T23" id="Seg_203" n="e" s="T22">teböɣum </ts>
               <ts e="T24" id="Seg_205" n="e" s="T23">Uːdʼarʼedɨ </ts>
               <ts e="T25" id="Seg_207" n="e" s="T24">qütʼdɨmdə </ts>
               <ts e="T26" id="Seg_209" n="e" s="T25">oɣulǯoʒelʼe </ts>
               <ts e="T27" id="Seg_211" n="e" s="T26">tadərest. </ts>
               <ts e="T28" id="Seg_213" n="e" s="T27">Qötdɨt </ts>
               <ts e="T29" id="Seg_215" n="e" s="T28">kak </ts>
               <ts e="T30" id="Seg_217" n="e" s="T29">kurolǯa, </ts>
               <ts e="T31" id="Seg_219" n="e" s="T30">šerdʼälan </ts>
               <ts e="T32" id="Seg_221" n="e" s="T31">parmun. </ts>
               <ts e="T33" id="Seg_223" n="e" s="T32">I </ts>
               <ts e="T34" id="Seg_225" n="e" s="T33">šerdʼa </ts>
               <ts e="T35" id="Seg_227" n="e" s="T34">qutder </ts>
               <ts e="T36" id="Seg_229" n="e" s="T35">tʼöqolǯut </ts>
               <ts e="T37" id="Seg_231" n="e" s="T36">naǯout </ts>
               <ts e="T38" id="Seg_233" n="e" s="T37">i </ts>
               <ts e="T39" id="Seg_235" n="e" s="T38">moɣolmət. </ts>
               <ts e="T40" id="Seg_237" n="e" s="T39">Tiː </ts>
               <ts e="T41" id="Seg_239" n="e" s="T40">qončit. </ts>
               <ts e="T42" id="Seg_241" n="e" s="T41">Tep </ts>
               <ts e="T43" id="Seg_243" n="e" s="T42">i </ts>
               <ts e="T44" id="Seg_245" n="e" s="T43">kuːwa. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_246" s="T1">PVD_1964_HorseSledge_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_247" s="T5">PVD_1964_HorseSledge_nar.002 (001.002)</ta>
            <ta e="T15" id="Seg_248" s="T10">PVD_1964_HorseSledge_nar.003 (001.003)</ta>
            <ta e="T21" id="Seg_249" s="T15">PVD_1964_HorseSledge_nar.004 (001.004)</ta>
            <ta e="T27" id="Seg_250" s="T21">PVD_1964_HorseSledge_nar.005 (001.005)</ta>
            <ta e="T32" id="Seg_251" s="T27">PVD_1964_HorseSledge_nar.006 (001.006)</ta>
            <ta e="T39" id="Seg_252" s="T32">PVD_1964_HorseSledge_nar.007 (001.007)</ta>
            <ta e="T41" id="Seg_253" s="T39">PVD_1964_HorseSledge_nar.008 (001.008)</ta>
            <ta e="T44" id="Seg_254" s="T41">PVD_1964_HorseSledge_nar.009 (001.009)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_255" s="T1">а ман ′тромниксʼе kwа′джан.</ta>
            <ta e="T10" id="Seg_256" s="T5">′сатдʼи ′jедоɣон кӱтдълам ′оɣуlджожелʼе ′та̄дерът та′ɣын.</ta>
            <ta e="T15" id="Seg_257" s="T10">тромниктъ таk′тоlджуkwаттъ, ниl′дʼзʼин и ′оɣуlджожуkwаттъ.</ta>
            <ta e="T21" id="Seg_258" s="T15">ранʼше ме′нан кан ′оɣуlджожелʼе ′та̄дъркузаттъ кӱтдълам.</ta>
            <ta e="T27" id="Seg_259" s="T21">оk′kыр те′бӧɣум ′ӯдʼа‵рʼеды kӱтʼдымдъ ′оɣуlджожелʼе ′тадъре̨ст.</ta>
            <ta e="T32" id="Seg_260" s="T27">kӧт′дыт как ку′роlджа, шер′дʼӓлан ′пармун.</ta>
            <ta e="T39" id="Seg_261" s="T32">и шер′дʼа kут′де̨р тʼӧ′kоlджут ′наджоут и мо′ɣолмът.</ta>
            <ta e="T41" id="Seg_262" s="T39">′тӣ ′kонтшит.</ta>
            <ta e="T44" id="Seg_263" s="T41">теп и кӯwа.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_264" s="T1">a man tromniksʼe qwaǯan.</ta>
            <ta e="T10" id="Seg_265" s="T5">satdʼi jedoɣon kütdəlam oɣulǯoʒelʼe taːderət taɣɨn.</ta>
            <ta e="T15" id="Seg_266" s="T10">tromniktə taqtolǯuqwattə, nildʼzʼin i oɣulǯoʒuqwattə.</ta>
            <ta e="T21" id="Seg_267" s="T15">ranʼše menan kan oɣulǯoʒelʼe taːdərkuzattə kütdəlam.</ta>
            <ta e="T27" id="Seg_268" s="T21">oqqɨr teböɣum uːdʼarʼedɨ qütʼdɨmdə oɣulǯoʒelʼe tadərest.</ta>
            <ta e="T32" id="Seg_269" s="T27">qötdɨt kak kurolǯa, šerdʼälan parmun.</ta>
            <ta e="T39" id="Seg_270" s="T32">i šerdʼa qutder tʼöqolǯut naǯout i moɣolmət.</ta>
            <ta e="T41" id="Seg_271" s="T39">tiː qontšit.</ta>
            <ta e="T44" id="Seg_272" s="T41">tep i kuːwa.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_273" s="T1">A man tromniksʼe qwaǯan. </ta>
            <ta e="T10" id="Seg_274" s="T5">Satdʼijedoɣon kütdəlam oɣulǯoʒelʼe taːderət taɣɨn. </ta>
            <ta e="T15" id="Seg_275" s="T10">Tromniktə taqtolǯuqwattə, nildʼzʼin i oɣulǯoʒuqwattə. </ta>
            <ta e="T21" id="Seg_276" s="T15">Ranʼše menan kan oɣulǯoʒelʼe taːdərkuzattə kütdəlam. </ta>
            <ta e="T27" id="Seg_277" s="T21">Oqqɨr teböɣum Uːdʼarʼedɨ qütʼdɨmdə oɣulǯoʒelʼe tadərest. </ta>
            <ta e="T32" id="Seg_278" s="T27">Qötdɨt kak kurolǯa, šerdʼälan parmun. </ta>
            <ta e="T39" id="Seg_279" s="T32">I šerdʼa qutder tʼöqolǯut naǯout i moɣolmət. </ta>
            <ta e="T41" id="Seg_280" s="T39">Tiː qončit. </ta>
            <ta e="T44" id="Seg_281" s="T41">Tep i kuːwa. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_282" s="T1">a</ta>
            <ta e="T3" id="Seg_283" s="T2">man</ta>
            <ta e="T4" id="Seg_284" s="T3">tromnik-sʼe</ta>
            <ta e="T5" id="Seg_285" s="T4">qwa-ǯa-n</ta>
            <ta e="T6" id="Seg_286" s="T5">Satdʼijedo-ɣon</ta>
            <ta e="T7" id="Seg_287" s="T6">kütdə-la-m</ta>
            <ta e="T8" id="Seg_288" s="T7">oɣulǯo-ʒe-lʼe</ta>
            <ta e="T9" id="Seg_289" s="T8">taːd-e-r-ə-t</ta>
            <ta e="T10" id="Seg_290" s="T9">taɣ-ɨ-n</ta>
            <ta e="T11" id="Seg_291" s="T10">tromnik-tə</ta>
            <ta e="T12" id="Seg_292" s="T11">taqtolǯu-qwa-ttə</ta>
            <ta e="T13" id="Seg_293" s="T12">nildʼzʼi-n</ta>
            <ta e="T14" id="Seg_294" s="T13">i</ta>
            <ta e="T15" id="Seg_295" s="T14">oɣulǯo-ʒu-qwa-ttə</ta>
            <ta e="T16" id="Seg_296" s="T15">ranʼše</ta>
            <ta e="T17" id="Seg_297" s="T16">me-nan</ta>
            <ta e="T18" id="Seg_298" s="T17">ka-n</ta>
            <ta e="T19" id="Seg_299" s="T18">oɣulǯo-ʒe-lʼe</ta>
            <ta e="T20" id="Seg_300" s="T19">taːd-ə-r-ku-za-ttə</ta>
            <ta e="T21" id="Seg_301" s="T20">kütdə-la-m</ta>
            <ta e="T22" id="Seg_302" s="T21">oqqɨr</ta>
            <ta e="T23" id="Seg_303" s="T22">tebö-ɣum</ta>
            <ta e="T24" id="Seg_304" s="T23">Uːdʼarʼedɨ</ta>
            <ta e="T25" id="Seg_305" s="T24">qütʼdɨ-m-də</ta>
            <ta e="T26" id="Seg_306" s="T25">oɣulǯo-ʒe-lʼe</ta>
            <ta e="T27" id="Seg_307" s="T26">tad-ə-r-e-s-t</ta>
            <ta e="T28" id="Seg_308" s="T27">qötdɨ-t</ta>
            <ta e="T29" id="Seg_309" s="T28">kak</ta>
            <ta e="T30" id="Seg_310" s="T29">kur-ol-ǯa</ta>
            <ta e="T31" id="Seg_311" s="T30">šerdʼä-la-n</ta>
            <ta e="T32" id="Seg_312" s="T31">par-mun</ta>
            <ta e="T33" id="Seg_313" s="T32">i</ta>
            <ta e="T34" id="Seg_314" s="T33">šerdʼa</ta>
            <ta e="T35" id="Seg_315" s="T34">qutder</ta>
            <ta e="T36" id="Seg_316" s="T35">tʼöqol-ǯu-t</ta>
            <ta e="T37" id="Seg_317" s="T36">naǯo-ut</ta>
            <ta e="T38" id="Seg_318" s="T37">i</ta>
            <ta e="T39" id="Seg_319" s="T38">moɣo-l-mə-t</ta>
            <ta e="T40" id="Seg_320" s="T39">tiː</ta>
            <ta e="T41" id="Seg_321" s="T40">qonči-t</ta>
            <ta e="T42" id="Seg_322" s="T41">tep</ta>
            <ta e="T43" id="Seg_323" s="T42">i</ta>
            <ta e="T44" id="Seg_324" s="T43">kuː-wa</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_325" s="T1">a</ta>
            <ta e="T3" id="Seg_326" s="T2">man</ta>
            <ta e="T4" id="Seg_327" s="T3">tromnik-se</ta>
            <ta e="T5" id="Seg_328" s="T4">qwan-enǯɨ-ŋ</ta>
            <ta e="T6" id="Seg_329" s="T5">Satdʼijedo-qɨn</ta>
            <ta e="T7" id="Seg_330" s="T6">kütdə-la-m</ta>
            <ta e="T8" id="Seg_331" s="T7">oɣulǯɨ-ʒu-le</ta>
            <ta e="T9" id="Seg_332" s="T8">tat-ɨ-r-ɨ-tɨn</ta>
            <ta e="T10" id="Seg_333" s="T9">taɣ-ɨ-ŋ</ta>
            <ta e="T11" id="Seg_334" s="T10">tromnik-ntə</ta>
            <ta e="T12" id="Seg_335" s="T11">taqtolǯu-ku-tɨn</ta>
            <ta e="T13" id="Seg_336" s="T12">nʼilʼdʼi-ŋ</ta>
            <ta e="T14" id="Seg_337" s="T13">i</ta>
            <ta e="T15" id="Seg_338" s="T14">oɣulǯɨ-ʒu-ku-tɨn</ta>
            <ta e="T16" id="Seg_339" s="T15">ranʼše</ta>
            <ta e="T17" id="Seg_340" s="T16">me-nan</ta>
            <ta e="T18" id="Seg_341" s="T17">ka-ŋ</ta>
            <ta e="T19" id="Seg_342" s="T18">oɣulǯɨ-ʒu-le</ta>
            <ta e="T20" id="Seg_343" s="T19">tat-ɨ-r-ku-sɨ-tɨn</ta>
            <ta e="T21" id="Seg_344" s="T20">kütdə-la-m</ta>
            <ta e="T22" id="Seg_345" s="T21">okkɨr</ta>
            <ta e="T23" id="Seg_346" s="T22">täbe-qum</ta>
            <ta e="T24" id="Seg_347" s="T23">Uːdʼarʼedɨ</ta>
            <ta e="T25" id="Seg_348" s="T24">kütdə-m-tə</ta>
            <ta e="T26" id="Seg_349" s="T25">oɣulǯɨ-ʒu-le</ta>
            <ta e="T27" id="Seg_350" s="T26">tat-ɨ-r-ɨ-sɨ-t</ta>
            <ta e="T28" id="Seg_351" s="T27">kütdə-tə</ta>
            <ta e="T29" id="Seg_352" s="T28">kak</ta>
            <ta e="T30" id="Seg_353" s="T29">kur-ol-ntɨ</ta>
            <ta e="T31" id="Seg_354" s="T30">šerdʼä-la-n</ta>
            <ta e="T32" id="Seg_355" s="T31">par-un</ta>
            <ta e="T33" id="Seg_356" s="T32">i</ta>
            <ta e="T34" id="Seg_357" s="T33">šerdʼä</ta>
            <ta e="T35" id="Seg_358" s="T34">qundar</ta>
            <ta e="T36" id="Seg_359" s="T35">tökkol-či-ntɨ</ta>
            <ta e="T37" id="Seg_360" s="T36">naːǯə-un</ta>
            <ta e="T38" id="Seg_361" s="T37">i</ta>
            <ta e="T39" id="Seg_362" s="T38">moqə-lʼ-mɨ-ntə</ta>
            <ta e="T40" id="Seg_363" s="T39">tiː</ta>
            <ta e="T41" id="Seg_364" s="T40">qonču-ntɨ</ta>
            <ta e="T42" id="Seg_365" s="T41">täp</ta>
            <ta e="T43" id="Seg_366" s="T42">i</ta>
            <ta e="T44" id="Seg_367" s="T43">quː-nɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_368" s="T1">and</ta>
            <ta e="T3" id="Seg_369" s="T2">I.NOM</ta>
            <ta e="T4" id="Seg_370" s="T3">wood_sledge-INSTR</ta>
            <ta e="T5" id="Seg_371" s="T4">leave-FUT-1SG.S</ta>
            <ta e="T6" id="Seg_372" s="T5">Sondorovo-LOC</ta>
            <ta e="T7" id="Seg_373" s="T6">horse-PL-ACC</ta>
            <ta e="T8" id="Seg_374" s="T7">teach-DRV-CVB</ta>
            <ta e="T9" id="Seg_375" s="T8">bring-EP-FRQ-EP-3PL</ta>
            <ta e="T10" id="Seg_376" s="T9">summer-EP-ADVZ</ta>
            <ta e="T11" id="Seg_377" s="T10">wood_sledge-ILL</ta>
            <ta e="T12" id="Seg_378" s="T11">harness-HAB-3PL</ta>
            <ta e="T13" id="Seg_379" s="T12">so-ADVZ</ta>
            <ta e="T14" id="Seg_380" s="T13">and</ta>
            <ta e="T15" id="Seg_381" s="T14">teach-DRV-HAB-3PL</ta>
            <ta e="T16" id="Seg_382" s="T15">earlier</ta>
            <ta e="T17" id="Seg_383" s="T16">we-ADES</ta>
            <ta e="T18" id="Seg_384" s="T17">winter-ADVZ</ta>
            <ta e="T19" id="Seg_385" s="T18">teach-DRV-CVB</ta>
            <ta e="T20" id="Seg_386" s="T19">bring-EP-FRQ-HAB-PST-3PL</ta>
            <ta e="T21" id="Seg_387" s="T20">horse-PL-ACC</ta>
            <ta e="T22" id="Seg_388" s="T21">one</ta>
            <ta e="T23" id="Seg_389" s="T22">man-human.being.[NOM]</ta>
            <ta e="T24" id="Seg_390" s="T23">Tiskino</ta>
            <ta e="T25" id="Seg_391" s="T24">horse-ACC-3SG</ta>
            <ta e="T26" id="Seg_392" s="T25">teach-DRV-CVB</ta>
            <ta e="T27" id="Seg_393" s="T26">bring-EP-FRQ-EP-PST-3SG.O</ta>
            <ta e="T28" id="Seg_394" s="T27">horse.[NOM]-3SG</ta>
            <ta e="T29" id="Seg_395" s="T28">suddenly</ta>
            <ta e="T30" id="Seg_396" s="T29">go-MOM-INFER.[3SG.S]</ta>
            <ta e="T31" id="Seg_397" s="T30">pole-PL-GEN</ta>
            <ta e="T32" id="Seg_398" s="T31">top-PROL</ta>
            <ta e="T33" id="Seg_399" s="T32">and</ta>
            <ta e="T34" id="Seg_400" s="T33">pole.[NOM]</ta>
            <ta e="T35" id="Seg_401" s="T34">suddenly</ta>
            <ta e="T36" id="Seg_402" s="T35">prick-RFL-INFER.[3SG.S]</ta>
            <ta e="T37" id="Seg_403" s="T36">paunch-PROL</ta>
            <ta e="T38" id="Seg_404" s="T37">and</ta>
            <ta e="T39" id="Seg_405" s="T38">back-ADJZ-something-ILL</ta>
            <ta e="T40" id="Seg_406" s="T39">here</ta>
            <ta e="T41" id="Seg_407" s="T40">appear-INFER.[3SG.S]</ta>
            <ta e="T42" id="Seg_408" s="T41">(s)he.[NOM]</ta>
            <ta e="T43" id="Seg_409" s="T42">and</ta>
            <ta e="T44" id="Seg_410" s="T43">die-CO.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_411" s="T1">а</ta>
            <ta e="T3" id="Seg_412" s="T2">я.NOM</ta>
            <ta e="T4" id="Seg_413" s="T3">дровни-INSTR</ta>
            <ta e="T5" id="Seg_414" s="T4">отправиться-FUT-1SG.S</ta>
            <ta e="T6" id="Seg_415" s="T5">Сондорово-LOC</ta>
            <ta e="T7" id="Seg_416" s="T6">лошадь-PL-ACC</ta>
            <ta e="T8" id="Seg_417" s="T7">научить-DRV-CVB</ta>
            <ta e="T9" id="Seg_418" s="T8">принести-EP-FRQ-EP-3PL</ta>
            <ta e="T10" id="Seg_419" s="T9">лето-EP-ADVZ</ta>
            <ta e="T11" id="Seg_420" s="T10">дровни-ILL</ta>
            <ta e="T12" id="Seg_421" s="T11">запрячь-HAB-3PL</ta>
            <ta e="T13" id="Seg_422" s="T12">так-ADVZ</ta>
            <ta e="T14" id="Seg_423" s="T13">и</ta>
            <ta e="T15" id="Seg_424" s="T14">научить-DRV-HAB-3PL</ta>
            <ta e="T16" id="Seg_425" s="T15">раньше</ta>
            <ta e="T17" id="Seg_426" s="T16">мы-ADES</ta>
            <ta e="T18" id="Seg_427" s="T17">зима-ADVZ</ta>
            <ta e="T19" id="Seg_428" s="T18">научить-DRV-CVB</ta>
            <ta e="T20" id="Seg_429" s="T19">принести-EP-FRQ-HAB-PST-3PL</ta>
            <ta e="T21" id="Seg_430" s="T20">лошадь-PL-ACC</ta>
            <ta e="T22" id="Seg_431" s="T21">один</ta>
            <ta e="T23" id="Seg_432" s="T22">мужчина-человек.[NOM]</ta>
            <ta e="T24" id="Seg_433" s="T23">Тискино</ta>
            <ta e="T25" id="Seg_434" s="T24">лошадь-ACC-3SG</ta>
            <ta e="T26" id="Seg_435" s="T25">научить-DRV-CVB</ta>
            <ta e="T27" id="Seg_436" s="T26">принести-EP-FRQ-EP-PST-3SG.O</ta>
            <ta e="T28" id="Seg_437" s="T27">лошадь.[NOM]-3SG</ta>
            <ta e="T29" id="Seg_438" s="T28">как</ta>
            <ta e="T30" id="Seg_439" s="T29">ходить-MOM-INFER.[3SG.S]</ta>
            <ta e="T31" id="Seg_440" s="T30">жердь-PL-GEN</ta>
            <ta e="T32" id="Seg_441" s="T31">верхняя.часть-PROL</ta>
            <ta e="T33" id="Seg_442" s="T32">и</ta>
            <ta e="T34" id="Seg_443" s="T33">жердь.[NOM]</ta>
            <ta e="T35" id="Seg_444" s="T34">как</ta>
            <ta e="T36" id="Seg_445" s="T35">воткнуть-RFL-INFER.[3SG.S]</ta>
            <ta e="T37" id="Seg_446" s="T36">живот-PROL</ta>
            <ta e="T38" id="Seg_447" s="T37">и</ta>
            <ta e="T39" id="Seg_448" s="T38">спина-ADJZ-нечто-ILL</ta>
            <ta e="T40" id="Seg_449" s="T39">сюда</ta>
            <ta e="T41" id="Seg_450" s="T40">появиться-INFER.[3SG.S]</ta>
            <ta e="T42" id="Seg_451" s="T41">он(а).[NOM]</ta>
            <ta e="T43" id="Seg_452" s="T42">и</ta>
            <ta e="T44" id="Seg_453" s="T43">умереть-CO.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_454" s="T1">conj</ta>
            <ta e="T3" id="Seg_455" s="T2">pers</ta>
            <ta e="T4" id="Seg_456" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_457" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_458" s="T5">nprop-n:case</ta>
            <ta e="T7" id="Seg_459" s="T6">n-n:num-n:case</ta>
            <ta e="T8" id="Seg_460" s="T7">v-v&gt;v-v&gt;adv</ta>
            <ta e="T9" id="Seg_461" s="T8">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T10" id="Seg_462" s="T9">n-n:ins-n&gt;adv</ta>
            <ta e="T11" id="Seg_463" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_464" s="T11">v-v&gt;v-v:pn</ta>
            <ta e="T13" id="Seg_465" s="T12">adv-adj&gt;adv</ta>
            <ta e="T14" id="Seg_466" s="T13">conj</ta>
            <ta e="T15" id="Seg_467" s="T14">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T16" id="Seg_468" s="T15">adv</ta>
            <ta e="T17" id="Seg_469" s="T16">pers-n:case</ta>
            <ta e="T18" id="Seg_470" s="T17">n-n&gt;adv</ta>
            <ta e="T19" id="Seg_471" s="T18">v-v&gt;v-v&gt;adv</ta>
            <ta e="T20" id="Seg_472" s="T19">v-v:ins-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_473" s="T20">n-n:num-n:case</ta>
            <ta e="T22" id="Seg_474" s="T21">num</ta>
            <ta e="T23" id="Seg_475" s="T22">n-n.[n:case]</ta>
            <ta e="T24" id="Seg_476" s="T23">nprop</ta>
            <ta e="T25" id="Seg_477" s="T24">n-n:case-n:poss</ta>
            <ta e="T26" id="Seg_478" s="T25">v-v&gt;v-v&gt;adv</ta>
            <ta e="T27" id="Seg_479" s="T26">v-v:ins-v&gt;v-v:ins-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_480" s="T27">n.[n:case]-n:poss</ta>
            <ta e="T29" id="Seg_481" s="T28">adv</ta>
            <ta e="T30" id="Seg_482" s="T29">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T31" id="Seg_483" s="T30">n-n:num-n:case</ta>
            <ta e="T32" id="Seg_484" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_485" s="T32">conj</ta>
            <ta e="T34" id="Seg_486" s="T33">n.[n:case]</ta>
            <ta e="T35" id="Seg_487" s="T34">adv</ta>
            <ta e="T36" id="Seg_488" s="T35">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T37" id="Seg_489" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_490" s="T37">conj</ta>
            <ta e="T39" id="Seg_491" s="T38">n-n&gt;adj-n-n:case</ta>
            <ta e="T40" id="Seg_492" s="T39">adv</ta>
            <ta e="T41" id="Seg_493" s="T40">v-v:mood.[v:pn]</ta>
            <ta e="T42" id="Seg_494" s="T41">pers.[n:case]</ta>
            <ta e="T43" id="Seg_495" s="T42">conj</ta>
            <ta e="T44" id="Seg_496" s="T43">v-v:ins.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_497" s="T1">conj</ta>
            <ta e="T3" id="Seg_498" s="T2">pers</ta>
            <ta e="T4" id="Seg_499" s="T3">n</ta>
            <ta e="T5" id="Seg_500" s="T4">v</ta>
            <ta e="T6" id="Seg_501" s="T5">nprop</ta>
            <ta e="T7" id="Seg_502" s="T6">n</ta>
            <ta e="T8" id="Seg_503" s="T7">v</ta>
            <ta e="T9" id="Seg_504" s="T8">v</ta>
            <ta e="T10" id="Seg_505" s="T9">adv</ta>
            <ta e="T11" id="Seg_506" s="T10">n</ta>
            <ta e="T12" id="Seg_507" s="T11">v</ta>
            <ta e="T13" id="Seg_508" s="T12">adv</ta>
            <ta e="T14" id="Seg_509" s="T13">conj</ta>
            <ta e="T15" id="Seg_510" s="T14">v</ta>
            <ta e="T16" id="Seg_511" s="T15">adv</ta>
            <ta e="T17" id="Seg_512" s="T16">pers</ta>
            <ta e="T18" id="Seg_513" s="T17">adv</ta>
            <ta e="T19" id="Seg_514" s="T18">v</ta>
            <ta e="T20" id="Seg_515" s="T19">v</ta>
            <ta e="T21" id="Seg_516" s="T20">n</ta>
            <ta e="T22" id="Seg_517" s="T21">num</ta>
            <ta e="T23" id="Seg_518" s="T22">n</ta>
            <ta e="T24" id="Seg_519" s="T23">adj</ta>
            <ta e="T25" id="Seg_520" s="T24">n</ta>
            <ta e="T26" id="Seg_521" s="T25">v</ta>
            <ta e="T27" id="Seg_522" s="T26">v</ta>
            <ta e="T28" id="Seg_523" s="T27">n</ta>
            <ta e="T29" id="Seg_524" s="T28">adv</ta>
            <ta e="T30" id="Seg_525" s="T29">v</ta>
            <ta e="T31" id="Seg_526" s="T30">n</ta>
            <ta e="T32" id="Seg_527" s="T31">n</ta>
            <ta e="T33" id="Seg_528" s="T32">conj</ta>
            <ta e="T34" id="Seg_529" s="T33">n</ta>
            <ta e="T35" id="Seg_530" s="T34">adv</ta>
            <ta e="T36" id="Seg_531" s="T35">v</ta>
            <ta e="T37" id="Seg_532" s="T36">n</ta>
            <ta e="T38" id="Seg_533" s="T37">conj</ta>
            <ta e="T39" id="Seg_534" s="T38">n</ta>
            <ta e="T40" id="Seg_535" s="T39">adv</ta>
            <ta e="T41" id="Seg_536" s="T40">v</ta>
            <ta e="T42" id="Seg_537" s="T41">pers</ta>
            <ta e="T43" id="Seg_538" s="T42">conj</ta>
            <ta e="T44" id="Seg_539" s="T43">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_540" s="T2">pro.h:A</ta>
            <ta e="T4" id="Seg_541" s="T3">np:Ins</ta>
            <ta e="T6" id="Seg_542" s="T5">np:L</ta>
            <ta e="T7" id="Seg_543" s="T6">np:Th</ta>
            <ta e="T9" id="Seg_544" s="T8">0.3.h:A</ta>
            <ta e="T10" id="Seg_545" s="T9">adv:Time</ta>
            <ta e="T11" id="Seg_546" s="T10">np:G</ta>
            <ta e="T12" id="Seg_547" s="T11">0.3.h:A 0.3:Th</ta>
            <ta e="T15" id="Seg_548" s="T14">0.3.h:A 0.3:Th</ta>
            <ta e="T16" id="Seg_549" s="T15">adv:Time</ta>
            <ta e="T18" id="Seg_550" s="T17">adv:Time</ta>
            <ta e="T20" id="Seg_551" s="T19">0.3.h:A</ta>
            <ta e="T21" id="Seg_552" s="T20">np:Th</ta>
            <ta e="T23" id="Seg_553" s="T22">np.h:A</ta>
            <ta e="T25" id="Seg_554" s="T24">np:Th 0.3.h:Poss</ta>
            <ta e="T28" id="Seg_555" s="T27">np:A 0.3.h:Poss</ta>
            <ta e="T31" id="Seg_556" s="T30">np:Poss</ta>
            <ta e="T32" id="Seg_557" s="T31">np:Path</ta>
            <ta e="T34" id="Seg_558" s="T33">np:A</ta>
            <ta e="T37" id="Seg_559" s="T36">np:Path</ta>
            <ta e="T39" id="Seg_560" s="T38">np:G</ta>
            <ta e="T41" id="Seg_561" s="T40">0.3:Th</ta>
            <ta e="T42" id="Seg_562" s="T41">pro:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_563" s="T2">pro.h:S</ta>
            <ta e="T5" id="Seg_564" s="T4">v:pred</ta>
            <ta e="T7" id="Seg_565" s="T6">np:O</ta>
            <ta e="T8" id="Seg_566" s="T7">s:temp</ta>
            <ta e="T9" id="Seg_567" s="T8">0.3.h:S v:pred</ta>
            <ta e="T12" id="Seg_568" s="T11">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T15" id="Seg_569" s="T14">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T19" id="Seg_570" s="T18">s:temp</ta>
            <ta e="T20" id="Seg_571" s="T19">0.3.h:S v:pred</ta>
            <ta e="T21" id="Seg_572" s="T20">np:O</ta>
            <ta e="T23" id="Seg_573" s="T22">np.h:S</ta>
            <ta e="T25" id="Seg_574" s="T24">np:O</ta>
            <ta e="T26" id="Seg_575" s="T25">s:temp</ta>
            <ta e="T27" id="Seg_576" s="T26">v:pred</ta>
            <ta e="T28" id="Seg_577" s="T27">np:S</ta>
            <ta e="T30" id="Seg_578" s="T29">v:pred</ta>
            <ta e="T34" id="Seg_579" s="T33">np:S</ta>
            <ta e="T36" id="Seg_580" s="T35">v:pred</ta>
            <ta e="T41" id="Seg_581" s="T40">0.3:S v:pred</ta>
            <ta e="T42" id="Seg_582" s="T41">pro:S</ta>
            <ta e="T44" id="Seg_583" s="T43">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_584" s="T1">RUS:gram</ta>
            <ta e="T4" id="Seg_585" s="T3">RUS:cult</ta>
            <ta e="T11" id="Seg_586" s="T10">RUS:cult</ta>
            <ta e="T14" id="Seg_587" s="T13">RUS:gram</ta>
            <ta e="T16" id="Seg_588" s="T15">RUS:core</ta>
            <ta e="T29" id="Seg_589" s="T28">RUS:disc</ta>
            <ta e="T31" id="Seg_590" s="T30">RUS:cult</ta>
            <ta e="T32" id="Seg_591" s="T31">WNB Noun or pp</ta>
            <ta e="T33" id="Seg_592" s="T32">RUS:gram</ta>
            <ta e="T34" id="Seg_593" s="T33">RUS:cult</ta>
            <ta e="T38" id="Seg_594" s="T37">RUS:gram</ta>
            <ta e="T43" id="Seg_595" s="T42">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_596" s="T1">I'll go on wooden sleds.</ta>
            <ta e="T10" id="Seg_597" s="T5">In Sondorovo horses are being taught in summer.</ta>
            <ta e="T15" id="Seg_598" s="T10">They are harnessed in wooden sleds, and so they are taught.</ta>
            <ta e="T21" id="Seg_599" s="T15">Earlier our horses were taught in winter.</ta>
            <ta e="T27" id="Seg_600" s="T21">One man from Tiskino was teaching a horse.</ta>
            <ta e="T32" id="Seg_601" s="T27">His horse started running over poles.</ta>
            <ta e="T39" id="Seg_602" s="T32">And a pole stick into his paunch and into his back.</ta>
            <ta e="T41" id="Seg_603" s="T39">It came out there.</ta>
            <ta e="T44" id="Seg_604" s="T41">So he died.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_605" s="T1">Ich fahre auf Holzschlitten.</ta>
            <ta e="T10" id="Seg_606" s="T5">In Sondorovo werden die Pferde im Sommer ausgebildet.</ta>
            <ta e="T15" id="Seg_607" s="T10">Sie werden vor Holzschlitten gespannt und so werden sie ausgebildet.</ta>
            <ta e="T21" id="Seg_608" s="T15">Früher wurden unsere Pferde im Winter ausgebildet.</ta>
            <ta e="T27" id="Seg_609" s="T21">Ein Mann aus Tiskino bildete ein Pferd aus.</ta>
            <ta e="T32" id="Seg_610" s="T27">Sein Pferd lief plötzlich über die Pfähle.</ta>
            <ta e="T39" id="Seg_611" s="T32">Und ein Pfahl bohrte sich durch seinen Bauch und in seinen Rücken.</ta>
            <ta e="T41" id="Seg_612" s="T39">Er kam hier heraus.</ta>
            <ta e="T44" id="Seg_613" s="T41">So starb es.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_614" s="T1">А я на дровнях поеду.</ta>
            <ta e="T10" id="Seg_615" s="T5">В Сондрово лошадей учат летом.</ta>
            <ta e="T15" id="Seg_616" s="T10">На дровни запрягают, так и учат.</ta>
            <ta e="T21" id="Seg_617" s="T15">Раньше у нас зимой учили коней.</ta>
            <ta e="T27" id="Seg_618" s="T21">Один мужик Тискинский (из деревни Тискино) лошадь учил.</ta>
            <ta e="T32" id="Seg_619" s="T27">Лошадь его как побежала, по жердям.</ta>
            <ta e="T39" id="Seg_620" s="T32">И жердь как залетела, в живот и в спину.</ta>
            <ta e="T41" id="Seg_621" s="T39">Сюда и высунулась (вышла).</ta>
            <ta e="T44" id="Seg_622" s="T41">Он и помер.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_623" s="T1">а я на дровнях поеду</ta>
            <ta e="T10" id="Seg_624" s="T5">в Сондрово лошадей учат летом</ta>
            <ta e="T15" id="Seg_625" s="T10">на дровни запрягают так и учат</ta>
            <ta e="T21" id="Seg_626" s="T15">раньше у нас зимой учили коней</ta>
            <ta e="T27" id="Seg_627" s="T21">один мужик Тискинский (из деревни Тискино) лошадь учил</ta>
            <ta e="T32" id="Seg_628" s="T27">лошадь как побежала по жердям пошла</ta>
            <ta e="T39" id="Seg_629" s="T32">и жердь как залетела в живот в спину</ta>
            <ta e="T41" id="Seg_630" s="T39">сюда и высунулась (вышла)</ta>
            <ta e="T44" id="Seg_631" s="T41">он и помер</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T39" id="Seg_632" s="T32">[BrM:] RFL? ‎‎PROL or ILL? 3SG.O or INFER? || </ta>
            <ta e="T41" id="Seg_633" s="T39">[BrM:] 3SG.O or INFER? || </ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
