<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_HouseholdActivities_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_HouseholdActivities_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">82</ud-information>
            <ud-information attribute-name="# HIAT:w">66</ud-information>
            <ud-information attribute-name="# e">66</ud-information>
            <ud-information attribute-name="# HIAT:u">14</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T67" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Nejgulam</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">qwɛːreǯau</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">urgu</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">A</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">patom</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">tajtɨlʼe</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">qwaǯaftə</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">Man</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">koːtʼen</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">kɛːtau</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_41" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">A</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">tebla</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">az</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">wɨːrkan</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">kɛtat</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_59" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">Man</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">teblazʼe</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">aːs</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">tajteǯan</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_74" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">Qardʼzʼen</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">oːnen</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">tajteǯan</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_86" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">Tebla</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">mega</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">tʼüːotə</ts>
                  <nts id="Seg_95" n="HIAT:ip">.</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_98" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_100" n="HIAT:w" s="T26">Tebla</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">mega</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">tʼüːbatə</ts>
                  <nts id="Seg_107" n="HIAT:ip">.</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_110" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_112" n="HIAT:w" s="T29">Nu</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">i</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_118" n="HIAT:w" s="T31">puskaj</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_121" n="HIAT:w" s="T32">tʼüːbiamtə</ts>
                  <nts id="Seg_122" n="HIAT:ip">.</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_125" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">Man</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">qɛːtəzau</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">35</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_136" n="HIAT:w" s="T36">sotək</ts>
                  <nts id="Seg_137" n="HIAT:ip">,</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_140" n="HIAT:w" s="T37">a</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_143" n="HIAT:w" s="T38">tebla</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_146" n="HIAT:w" s="T39">tolʼko</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_149" n="HIAT:w" s="T40">15</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_152" n="HIAT:w" s="T41">sotək</ts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_156" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_158" n="HIAT:w" s="T42">A</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_161" n="HIAT:w" s="T43">teper</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_164" n="HIAT:w" s="T44">meː</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_167" n="HIAT:w" s="T45">nʼuʒəm</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_170" n="HIAT:w" s="T46">maltčʼawtə</ts>
                  <nts id="Seg_171" n="HIAT:ip">.</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_174" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_176" n="HIAT:w" s="T47">Təper</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_179" n="HIAT:w" s="T48">me</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_182" n="HIAT:w" s="T49">kartoškaj</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_185" n="HIAT:w" s="T50">baɣatəlʼe</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_188" n="HIAT:w" s="T51">juːbəretčtu</ts>
                  <nts id="Seg_189" n="HIAT:ip">.</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_192" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_194" n="HIAT:w" s="T52">I</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_197" n="HIAT:w" s="T53">me</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_200" n="HIAT:w" s="T54">taːrnawtə</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_203" n="HIAT:w" s="T55">küt</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_206" n="HIAT:w" s="T56">sotka</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_209" n="HIAT:w" s="T57">dʼar</ts>
                  <nts id="Seg_210" n="HIAT:ip">.</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_213" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_215" n="HIAT:w" s="T58">Man</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_218" n="HIAT:w" s="T59">sɨt</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_221" n="HIAT:w" s="T60">tʼel</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_224" n="HIAT:w" s="T61">bakənau</ts>
                  <nts id="Seg_225" n="HIAT:ip">,</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_228" n="HIAT:w" s="T62">a</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_231" n="HIAT:w" s="T63">tebla</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_234" n="HIAT:w" s="T64">sobla</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_237" n="HIAT:w" s="T65">tel</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_240" n="HIAT:w" s="T66">baɣatəzat</ts>
                  <nts id="Seg_241" n="HIAT:ip">.</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T67" id="Seg_243" n="sc" s="T1">
               <ts e="T2" id="Seg_245" n="e" s="T1">Nejgulam </ts>
               <ts e="T3" id="Seg_247" n="e" s="T2">qwɛːreǯau </ts>
               <ts e="T4" id="Seg_249" n="e" s="T3">urgu. </ts>
               <ts e="T5" id="Seg_251" n="e" s="T4">A </ts>
               <ts e="T6" id="Seg_253" n="e" s="T5">patom </ts>
               <ts e="T7" id="Seg_255" n="e" s="T6">tajtɨlʼe </ts>
               <ts e="T8" id="Seg_257" n="e" s="T7">qwaǯaftə. </ts>
               <ts e="T9" id="Seg_259" n="e" s="T8">Man </ts>
               <ts e="T10" id="Seg_261" n="e" s="T9">koːtʼen </ts>
               <ts e="T11" id="Seg_263" n="e" s="T10">kɛːtau. </ts>
               <ts e="T12" id="Seg_265" n="e" s="T11">A </ts>
               <ts e="T13" id="Seg_267" n="e" s="T12">tebla </ts>
               <ts e="T14" id="Seg_269" n="e" s="T13">az </ts>
               <ts e="T15" id="Seg_271" n="e" s="T14">wɨːrkan </ts>
               <ts e="T16" id="Seg_273" n="e" s="T15">kɛtat. </ts>
               <ts e="T17" id="Seg_275" n="e" s="T16">Man </ts>
               <ts e="T18" id="Seg_277" n="e" s="T17">teblazʼe </ts>
               <ts e="T19" id="Seg_279" n="e" s="T18">aːs </ts>
               <ts e="T20" id="Seg_281" n="e" s="T19">tajteǯan. </ts>
               <ts e="T21" id="Seg_283" n="e" s="T20">Qardʼzʼen </ts>
               <ts e="T22" id="Seg_285" n="e" s="T21">oːnen </ts>
               <ts e="T23" id="Seg_287" n="e" s="T22">tajteǯan. </ts>
               <ts e="T24" id="Seg_289" n="e" s="T23">Tebla </ts>
               <ts e="T25" id="Seg_291" n="e" s="T24">mega </ts>
               <ts e="T26" id="Seg_293" n="e" s="T25">tʼüːotə. </ts>
               <ts e="T27" id="Seg_295" n="e" s="T26">Tebla </ts>
               <ts e="T28" id="Seg_297" n="e" s="T27">mega </ts>
               <ts e="T29" id="Seg_299" n="e" s="T28">tʼüːbatə. </ts>
               <ts e="T30" id="Seg_301" n="e" s="T29">Nu </ts>
               <ts e="T31" id="Seg_303" n="e" s="T30">i </ts>
               <ts e="T32" id="Seg_305" n="e" s="T31">puskaj </ts>
               <ts e="T33" id="Seg_307" n="e" s="T32">tʼüːbiamtə. </ts>
               <ts e="T34" id="Seg_309" n="e" s="T33">Man </ts>
               <ts e="T35" id="Seg_311" n="e" s="T34">qɛːtəzau </ts>
               <ts e="T36" id="Seg_313" n="e" s="T35">35 </ts>
               <ts e="T37" id="Seg_315" n="e" s="T36">sotək, </ts>
               <ts e="T38" id="Seg_317" n="e" s="T37">a </ts>
               <ts e="T39" id="Seg_319" n="e" s="T38">tebla </ts>
               <ts e="T40" id="Seg_321" n="e" s="T39">tolʼko </ts>
               <ts e="T41" id="Seg_323" n="e" s="T40">15 </ts>
               <ts e="T42" id="Seg_325" n="e" s="T41">sotək. </ts>
               <ts e="T43" id="Seg_327" n="e" s="T42">A </ts>
               <ts e="T44" id="Seg_329" n="e" s="T43">teper </ts>
               <ts e="T45" id="Seg_331" n="e" s="T44">meː </ts>
               <ts e="T46" id="Seg_333" n="e" s="T45">nʼuʒəm </ts>
               <ts e="T47" id="Seg_335" n="e" s="T46">maltčʼawtə. </ts>
               <ts e="T48" id="Seg_337" n="e" s="T47">Təper </ts>
               <ts e="T49" id="Seg_339" n="e" s="T48">me </ts>
               <ts e="T50" id="Seg_341" n="e" s="T49">kartoškaj </ts>
               <ts e="T51" id="Seg_343" n="e" s="T50">baɣatəlʼe </ts>
               <ts e="T52" id="Seg_345" n="e" s="T51">juːbəretčtu. </ts>
               <ts e="T53" id="Seg_347" n="e" s="T52">I </ts>
               <ts e="T54" id="Seg_349" n="e" s="T53">me </ts>
               <ts e="T55" id="Seg_351" n="e" s="T54">taːrnawtə </ts>
               <ts e="T56" id="Seg_353" n="e" s="T55">küt </ts>
               <ts e="T57" id="Seg_355" n="e" s="T56">sotka </ts>
               <ts e="T58" id="Seg_357" n="e" s="T57">dʼar. </ts>
               <ts e="T59" id="Seg_359" n="e" s="T58">Man </ts>
               <ts e="T60" id="Seg_361" n="e" s="T59">sɨt </ts>
               <ts e="T61" id="Seg_363" n="e" s="T60">tʼel </ts>
               <ts e="T62" id="Seg_365" n="e" s="T61">bakənau, </ts>
               <ts e="T63" id="Seg_367" n="e" s="T62">a </ts>
               <ts e="T64" id="Seg_369" n="e" s="T63">tebla </ts>
               <ts e="T65" id="Seg_371" n="e" s="T64">sobla </ts>
               <ts e="T66" id="Seg_373" n="e" s="T65">tel </ts>
               <ts e="T67" id="Seg_375" n="e" s="T66">baɣatəzat. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_376" s="T1">PVD_1964_HouseholdActivities_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_377" s="T4">PVD_1964_HouseholdActivities_nar.002 (001.002)</ta>
            <ta e="T11" id="Seg_378" s="T8">PVD_1964_HouseholdActivities_nar.003 (001.003)</ta>
            <ta e="T16" id="Seg_379" s="T11">PVD_1964_HouseholdActivities_nar.004 (001.004)</ta>
            <ta e="T20" id="Seg_380" s="T16">PVD_1964_HouseholdActivities_nar.005 (001.005)</ta>
            <ta e="T23" id="Seg_381" s="T20">PVD_1964_HouseholdActivities_nar.006 (001.006)</ta>
            <ta e="T26" id="Seg_382" s="T23">PVD_1964_HouseholdActivities_nar.007 (001.007)</ta>
            <ta e="T29" id="Seg_383" s="T26">PVD_1964_HouseholdActivities_nar.008 (001.008)</ta>
            <ta e="T33" id="Seg_384" s="T29">PVD_1964_HouseholdActivities_nar.009 (001.009)</ta>
            <ta e="T42" id="Seg_385" s="T33">PVD_1964_HouseholdActivities_nar.010 (001.010)</ta>
            <ta e="T47" id="Seg_386" s="T42">PVD_1964_HouseholdActivities_nar.011 (001.011)</ta>
            <ta e="T52" id="Seg_387" s="T47">PVD_1964_HouseholdActivities_nar.012 (001.012)</ta>
            <ta e="T58" id="Seg_388" s="T52">PVD_1964_HouseholdActivities_nar.013 (001.013)</ta>
            <ta e="T67" id="Seg_389" s="T58">PVD_1964_HouseholdActivities_nar.014 (001.014)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_390" s="T1">′нейг̂улам ′kwɛ̄реджау̹ ′ургу.</ta>
            <ta e="T8" id="Seg_391" s="T4">а па′том тайты′лʼе kwа′джафтъ.</ta>
            <ta e="T11" id="Seg_392" s="T8">ман ко̄тʼ(ц)ен ′кɛ̄тау̹(w).</ta>
            <ta e="T16" id="Seg_393" s="T11">а теб′ла аз ′вы̄ркан ′кɛтат.</ta>
            <ta e="T20" id="Seg_394" s="T16">ман теб′лазʼе а̄с ′тай′теджан.</ta>
            <ta e="T23" id="Seg_395" s="T20">kар′дʼзʼен о̄′нен тай′тет(д̂)жан.</ta>
            <ta e="T26" id="Seg_396" s="T23">теб′ла ′мега ′тʼӱ̄о̂тъ.</ta>
            <ta e="T29" id="Seg_397" s="T26">теб′ла ′мега ′тʼӱ̄батъ.</ta>
            <ta e="T33" id="Seg_398" s="T29">ну и пускай тʼӱ̄би̹′амтъ.</ta>
            <ta e="T42" id="Seg_399" s="T33">ман ′kɛ̄тъзау̹(w) 35 сотък, а теб′ла толʼко 15 сотък.</ta>
            <ta e="T47" id="Seg_400" s="T42">а те′пер ме̄ ′нʼужъм мал′тчʼавтъ.</ta>
            <ta e="T52" id="Seg_401" s="T47">тъ′пер ме кар′тошкай б̂а′ɣ̂атълʼе ′jӯбъретчту.</ta>
            <ta e="T58" id="Seg_402" s="T52">и ме ′та̄рнаw̹(ф)тъ кӱт ′соткадʼар.</ta>
            <ta e="T67" id="Seg_403" s="T58">ман сы(ъ)′тʼел ′б̂акънау̹, а ′тебла ′собла тел б̂а′ɣатъзат.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_404" s="T1">nejĝulam qwɛːreǯau̹ urgu.</ta>
            <ta e="T8" id="Seg_405" s="T4">a patom tajtɨlʼe qwaǯaftə.</ta>
            <ta e="T11" id="Seg_406" s="T8">man koːtʼ(c)en kɛːtau̹(w).</ta>
            <ta e="T16" id="Seg_407" s="T11">a tebla az wɨːrkan kɛtat.</ta>
            <ta e="T20" id="Seg_408" s="T16">man teblazʼe aːs tajteǯan.</ta>
            <ta e="T23" id="Seg_409" s="T20">qardʼzʼen oːnen tajtet(d̂)ʒan.</ta>
            <ta e="T26" id="Seg_410" s="T23">tebla mega tʼüːôtə.</ta>
            <ta e="T29" id="Seg_411" s="T26">tebla mega tʼüːbatə.</ta>
            <ta e="T33" id="Seg_412" s="T29">nu i puskaj tʼüːbi̹amtə.</ta>
            <ta e="T42" id="Seg_413" s="T33">man qɛːtəzau̹(w) 35 sotək, a tebla tolʼko 15 sotək.</ta>
            <ta e="T47" id="Seg_414" s="T42">a teper meː nʼuʒəm maltčʼawtə.</ta>
            <ta e="T52" id="Seg_415" s="T47">təper me kartoškaj b̂aɣ̂atəlʼe juːbəretčtu.</ta>
            <ta e="T58" id="Seg_416" s="T52">i me taːrnaw̹(f)tə küt sotkadʼar.</ta>
            <ta e="T67" id="Seg_417" s="T58">man sɨ(ə)tʼel b̂akənau̹, a tebla sobla tel b̂aɣatəzat.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_418" s="T1">Nejgulam qwɛːreǯau urgu. </ta>
            <ta e="T8" id="Seg_419" s="T4">A patom tajtɨlʼe qwaǯaftə. </ta>
            <ta e="T11" id="Seg_420" s="T8">Man koːtʼen kɛːtau. </ta>
            <ta e="T16" id="Seg_421" s="T11">A tebla az wɨːrkan kɛtat. </ta>
            <ta e="T20" id="Seg_422" s="T16">Man teblazʼe aːs tajteǯan. </ta>
            <ta e="T23" id="Seg_423" s="T20">Qardʼzʼen oːnen tajteǯan. </ta>
            <ta e="T26" id="Seg_424" s="T23">Tebla mega tʼüːotə. </ta>
            <ta e="T29" id="Seg_425" s="T26">Tebla mega tʼüːbatə. </ta>
            <ta e="T33" id="Seg_426" s="T29">Nu i puskaj tʼüːbiamtə. </ta>
            <ta e="T42" id="Seg_427" s="T33">Man qɛːtəzau 35 sotək, a tebla tolʼko 15 sotək. </ta>
            <ta e="T47" id="Seg_428" s="T42">A teper meː nʼuʒəm maltčʼawtə. </ta>
            <ta e="T52" id="Seg_429" s="T47">Təper me kartoškaj baɣatəlʼe juːbəretčtu. </ta>
            <ta e="T58" id="Seg_430" s="T52">I me taːrnawtə küt sotka dʼar. </ta>
            <ta e="T67" id="Seg_431" s="T58">Man sɨt tʼel bakənau, a tebla sobla tel baɣatəzat. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_432" s="T1">ne-j-gu-la-m</ta>
            <ta e="T3" id="Seg_433" s="T2">qwɛːr-eǯa-u</ta>
            <ta e="T4" id="Seg_434" s="T3">ur-gu</ta>
            <ta e="T5" id="Seg_435" s="T4">a</ta>
            <ta e="T6" id="Seg_436" s="T5">patom</ta>
            <ta e="T7" id="Seg_437" s="T6">tajtɨ-lʼe</ta>
            <ta e="T8" id="Seg_438" s="T7">qwa-ǯa-ftə</ta>
            <ta e="T9" id="Seg_439" s="T8">man</ta>
            <ta e="T10" id="Seg_440" s="T9">koːtʼe-n</ta>
            <ta e="T11" id="Seg_441" s="T10">kɛːta-u</ta>
            <ta e="T12" id="Seg_442" s="T11">a</ta>
            <ta e="T13" id="Seg_443" s="T12">teb-la</ta>
            <ta e="T14" id="Seg_444" s="T13">az</ta>
            <ta e="T15" id="Seg_445" s="T14">wɨːrka-n</ta>
            <ta e="T16" id="Seg_446" s="T15">kɛta-t</ta>
            <ta e="T17" id="Seg_447" s="T16">man</ta>
            <ta e="T18" id="Seg_448" s="T17">teb-la-zʼe</ta>
            <ta e="T19" id="Seg_449" s="T18">aːs</ta>
            <ta e="T20" id="Seg_450" s="T19">tajt-eǯa-n</ta>
            <ta e="T21" id="Seg_451" s="T20">qardʼzʼe-n</ta>
            <ta e="T22" id="Seg_452" s="T21">oːnen</ta>
            <ta e="T23" id="Seg_453" s="T22">tajt-eǯa-n</ta>
            <ta e="T24" id="Seg_454" s="T23">teb-la</ta>
            <ta e="T25" id="Seg_455" s="T24">mega</ta>
            <ta e="T26" id="Seg_456" s="T25">tʼüːo-tə</ta>
            <ta e="T27" id="Seg_457" s="T26">teb-la</ta>
            <ta e="T28" id="Seg_458" s="T27">mega</ta>
            <ta e="T29" id="Seg_459" s="T28">tʼüː-ba-tə</ta>
            <ta e="T30" id="Seg_460" s="T29">nu</ta>
            <ta e="T31" id="Seg_461" s="T30">i</ta>
            <ta e="T32" id="Seg_462" s="T31">puskaj</ta>
            <ta e="T33" id="Seg_463" s="T32">tʼüː-biam-tə</ta>
            <ta e="T34" id="Seg_464" s="T33">man</ta>
            <ta e="T35" id="Seg_465" s="T34">qɛːtə-za-u</ta>
            <ta e="T38" id="Seg_466" s="T37">a</ta>
            <ta e="T39" id="Seg_467" s="T38">teb-la</ta>
            <ta e="T40" id="Seg_468" s="T39">tolʼko</ta>
            <ta e="T43" id="Seg_469" s="T42">a</ta>
            <ta e="T44" id="Seg_470" s="T43">teper</ta>
            <ta e="T45" id="Seg_471" s="T44">meː</ta>
            <ta e="T46" id="Seg_472" s="T45">nʼuʒə-m</ta>
            <ta e="T47" id="Seg_473" s="T46">maltčʼa-wtə</ta>
            <ta e="T48" id="Seg_474" s="T47">təper</ta>
            <ta e="T49" id="Seg_475" s="T48">me</ta>
            <ta e="T50" id="Seg_476" s="T49">kartoška-j</ta>
            <ta e="T51" id="Seg_477" s="T50">baɣa-tə-lʼe</ta>
            <ta e="T52" id="Seg_478" s="T51">juːbə-r-etč-tu</ta>
            <ta e="T53" id="Seg_479" s="T52">i</ta>
            <ta e="T54" id="Seg_480" s="T53">me</ta>
            <ta e="T55" id="Seg_481" s="T54">taːr-na-wtə</ta>
            <ta e="T56" id="Seg_482" s="T55">küt</ta>
            <ta e="T57" id="Seg_483" s="T56">sotka</ta>
            <ta e="T58" id="Seg_484" s="T57">dʼar</ta>
            <ta e="T59" id="Seg_485" s="T58">man</ta>
            <ta e="T60" id="Seg_486" s="T59">sɨt</ta>
            <ta e="T61" id="Seg_487" s="T60">tʼel</ta>
            <ta e="T62" id="Seg_488" s="T61">bakə-na-u</ta>
            <ta e="T63" id="Seg_489" s="T62">a</ta>
            <ta e="T64" id="Seg_490" s="T63">teb-la</ta>
            <ta e="T65" id="Seg_491" s="T64">sobla</ta>
            <ta e="T66" id="Seg_492" s="T65">tel</ta>
            <ta e="T67" id="Seg_493" s="T66">baɣa-tə-za-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_494" s="T1">ne-lʼ-qum-la-m</ta>
            <ta e="T3" id="Seg_495" s="T2">qwɨrɨ-enǯɨ-w</ta>
            <ta e="T4" id="Seg_496" s="T3">uːr-gu</ta>
            <ta e="T5" id="Seg_497" s="T4">a</ta>
            <ta e="T6" id="Seg_498" s="T5">patom</ta>
            <ta e="T7" id="Seg_499" s="T6">tajtɨ-le</ta>
            <ta e="T8" id="Seg_500" s="T7">qwan-enǯɨ-un</ta>
            <ta e="T9" id="Seg_501" s="T8">man</ta>
            <ta e="T10" id="Seg_502" s="T9">koːci-ŋ</ta>
            <ta e="T11" id="Seg_503" s="T10">qättɨ-w</ta>
            <ta e="T12" id="Seg_504" s="T11">a</ta>
            <ta e="T13" id="Seg_505" s="T12">täp-la</ta>
            <ta e="T14" id="Seg_506" s="T13">asa</ta>
            <ta e="T15" id="Seg_507" s="T14">wargɨ-ŋ</ta>
            <ta e="T16" id="Seg_508" s="T15">qättɨ-tɨn</ta>
            <ta e="T17" id="Seg_509" s="T16">man</ta>
            <ta e="T18" id="Seg_510" s="T17">täp-la-se</ta>
            <ta e="T19" id="Seg_511" s="T18">asa</ta>
            <ta e="T20" id="Seg_512" s="T19">tajtɨ-enǯɨ-ŋ</ta>
            <ta e="T21" id="Seg_513" s="T20">qardʼe-n</ta>
            <ta e="T22" id="Seg_514" s="T21">oneŋ</ta>
            <ta e="T23" id="Seg_515" s="T22">tajtɨ-enǯɨ-ŋ</ta>
            <ta e="T24" id="Seg_516" s="T23">täp-la</ta>
            <ta e="T25" id="Seg_517" s="T24">mekka</ta>
            <ta e="T26" id="Seg_518" s="T25">tüu-tɨn</ta>
            <ta e="T27" id="Seg_519" s="T26">täp-la</ta>
            <ta e="T28" id="Seg_520" s="T27">mekka</ta>
            <ta e="T29" id="Seg_521" s="T28">tüu-mbɨ-tɨn</ta>
            <ta e="T30" id="Seg_522" s="T29">nu</ta>
            <ta e="T31" id="Seg_523" s="T30">i</ta>
            <ta e="T32" id="Seg_524" s="T31">puskaj</ta>
            <ta e="T33" id="Seg_525" s="T32">tüu-biam-tɨn</ta>
            <ta e="T34" id="Seg_526" s="T33">man</ta>
            <ta e="T35" id="Seg_527" s="T34">qättɨ-sɨ-w</ta>
            <ta e="T38" id="Seg_528" s="T37">a</ta>
            <ta e="T39" id="Seg_529" s="T38">täp-la</ta>
            <ta e="T40" id="Seg_530" s="T39">tolʼko</ta>
            <ta e="T43" id="Seg_531" s="T42">a</ta>
            <ta e="T44" id="Seg_532" s="T43">teper</ta>
            <ta e="T45" id="Seg_533" s="T44">me</ta>
            <ta e="T46" id="Seg_534" s="T45">nʼüʒə-m</ta>
            <ta e="T47" id="Seg_535" s="T46">malčə-un</ta>
            <ta e="T48" id="Seg_536" s="T47">teper</ta>
            <ta e="T49" id="Seg_537" s="T48">me</ta>
            <ta e="T50" id="Seg_538" s="T49">kartoška-lʼ</ta>
            <ta e="T51" id="Seg_539" s="T50">paqə-ntɨ-le</ta>
            <ta e="T52" id="Seg_540" s="T51">übɨ-r-enǯɨ-un</ta>
            <ta e="T53" id="Seg_541" s="T52">i</ta>
            <ta e="T54" id="Seg_542" s="T53">me</ta>
            <ta e="T55" id="Seg_543" s="T54">tar-nɨ-un</ta>
            <ta e="T56" id="Seg_544" s="T55">köt</ta>
            <ta e="T57" id="Seg_545" s="T56">sotka</ta>
            <ta e="T58" id="Seg_546" s="T57">dʼar</ta>
            <ta e="T59" id="Seg_547" s="T58">man</ta>
            <ta e="T60" id="Seg_548" s="T59">sədə</ta>
            <ta e="T61" id="Seg_549" s="T60">dʼel</ta>
            <ta e="T62" id="Seg_550" s="T61">paqə-nɨ-w</ta>
            <ta e="T63" id="Seg_551" s="T62">a</ta>
            <ta e="T64" id="Seg_552" s="T63">täp-la</ta>
            <ta e="T65" id="Seg_553" s="T64">sobla</ta>
            <ta e="T66" id="Seg_554" s="T65">dʼel</ta>
            <ta e="T67" id="Seg_555" s="T66">paqə-ntɨ-sɨ-tɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_556" s="T1">woman-ADJZ-human.being-PL-ACC</ta>
            <ta e="T3" id="Seg_557" s="T2">call-FUT-1SG.O</ta>
            <ta e="T4" id="Seg_558" s="T3">swim-INF</ta>
            <ta e="T5" id="Seg_559" s="T4">and</ta>
            <ta e="T6" id="Seg_560" s="T5">then</ta>
            <ta e="T7" id="Seg_561" s="T6">mow-CVB</ta>
            <ta e="T8" id="Seg_562" s="T7">leave-FUT-1PL</ta>
            <ta e="T9" id="Seg_563" s="T8">I.NOM</ta>
            <ta e="T10" id="Seg_564" s="T9">much-ADVZ</ta>
            <ta e="T11" id="Seg_565" s="T10">mow-1SG.O</ta>
            <ta e="T12" id="Seg_566" s="T11">and</ta>
            <ta e="T13" id="Seg_567" s="T12">(s)he-PL.[NOM]</ta>
            <ta e="T14" id="Seg_568" s="T13">NEG</ta>
            <ta e="T15" id="Seg_569" s="T14">big-ADVZ</ta>
            <ta e="T16" id="Seg_570" s="T15">mow-3PL</ta>
            <ta e="T17" id="Seg_571" s="T16">I.NOM</ta>
            <ta e="T18" id="Seg_572" s="T17">(s)he-PL-COM</ta>
            <ta e="T19" id="Seg_573" s="T18">NEG</ta>
            <ta e="T20" id="Seg_574" s="T19">mow-FUT-1SG.S</ta>
            <ta e="T21" id="Seg_575" s="T20">tomorrow-ADV.LOC</ta>
            <ta e="T22" id="Seg_576" s="T21">oneself.1SG</ta>
            <ta e="T23" id="Seg_577" s="T22">mow-FUT-1SG.S</ta>
            <ta e="T24" id="Seg_578" s="T23">(s)he-PL.[NOM]</ta>
            <ta e="T25" id="Seg_579" s="T24">I.ALL</ta>
            <ta e="T26" id="Seg_580" s="T25">get.angry-3PL</ta>
            <ta e="T27" id="Seg_581" s="T26">(s)he-PL.[NOM]</ta>
            <ta e="T28" id="Seg_582" s="T27">I.ALL</ta>
            <ta e="T29" id="Seg_583" s="T28">get.angry-DUR-3PL</ta>
            <ta e="T30" id="Seg_584" s="T29">now</ta>
            <ta e="T31" id="Seg_585" s="T30">and</ta>
            <ta e="T32" id="Seg_586" s="T31">JUSS</ta>
            <ta e="T33" id="Seg_587" s="T32">get.angry-%%-3PL</ta>
            <ta e="T34" id="Seg_588" s="T33">I.NOM</ta>
            <ta e="T35" id="Seg_589" s="T34">mow-PST-1SG.O</ta>
            <ta e="T38" id="Seg_590" s="T37">and</ta>
            <ta e="T39" id="Seg_591" s="T38">(s)he-PL.[NOM]</ta>
            <ta e="T40" id="Seg_592" s="T39">only</ta>
            <ta e="T43" id="Seg_593" s="T42">and</ta>
            <ta e="T44" id="Seg_594" s="T43">now</ta>
            <ta e="T45" id="Seg_595" s="T44">we.[NOM]</ta>
            <ta e="T46" id="Seg_596" s="T45">hay-ACC</ta>
            <ta e="T47" id="Seg_597" s="T46">stop-1PL</ta>
            <ta e="T48" id="Seg_598" s="T47">now</ta>
            <ta e="T49" id="Seg_599" s="T48">we.[NOM]</ta>
            <ta e="T50" id="Seg_600" s="T49">potato-ADJZ</ta>
            <ta e="T51" id="Seg_601" s="T50">dig.out-IPFV-CVB</ta>
            <ta e="T52" id="Seg_602" s="T51">begin-DRV-FUT-1PL</ta>
            <ta e="T53" id="Seg_603" s="T52">and</ta>
            <ta e="T54" id="Seg_604" s="T53">we.[NOM]</ta>
            <ta e="T55" id="Seg_605" s="T54">share-CO-1PL</ta>
            <ta e="T56" id="Seg_606" s="T55">ten</ta>
            <ta e="T57" id="Seg_607" s="T56">100.square.meters.[NOM]</ta>
            <ta e="T58" id="Seg_608" s="T57">(each)</ta>
            <ta e="T59" id="Seg_609" s="T58">I.NOM</ta>
            <ta e="T60" id="Seg_610" s="T59">two</ta>
            <ta e="T61" id="Seg_611" s="T60">day.[NOM]</ta>
            <ta e="T62" id="Seg_612" s="T61">dig.out-CO-1SG.O</ta>
            <ta e="T63" id="Seg_613" s="T62">and</ta>
            <ta e="T64" id="Seg_614" s="T63">(s)he-PL.[NOM]</ta>
            <ta e="T65" id="Seg_615" s="T64">five</ta>
            <ta e="T66" id="Seg_616" s="T65">day.[NOM]</ta>
            <ta e="T67" id="Seg_617" s="T66">dig.out-IPFV-PST-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_618" s="T1">женщина-ADJZ-человек-PL-ACC</ta>
            <ta e="T3" id="Seg_619" s="T2">позвать-FUT-1SG.O</ta>
            <ta e="T4" id="Seg_620" s="T3">купаться-INF</ta>
            <ta e="T5" id="Seg_621" s="T4">а</ta>
            <ta e="T6" id="Seg_622" s="T5">потом</ta>
            <ta e="T7" id="Seg_623" s="T6">косить-CVB</ta>
            <ta e="T8" id="Seg_624" s="T7">отправиться-FUT-1PL</ta>
            <ta e="T9" id="Seg_625" s="T8">я.NOM</ta>
            <ta e="T10" id="Seg_626" s="T9">много-ADVZ</ta>
            <ta e="T11" id="Seg_627" s="T10">скосить-1SG.O</ta>
            <ta e="T12" id="Seg_628" s="T11">а</ta>
            <ta e="T13" id="Seg_629" s="T12">он(а)-PL.[NOM]</ta>
            <ta e="T14" id="Seg_630" s="T13">NEG</ta>
            <ta e="T15" id="Seg_631" s="T14">большой-ADVZ</ta>
            <ta e="T16" id="Seg_632" s="T15">скосить-3PL</ta>
            <ta e="T17" id="Seg_633" s="T16">я.NOM</ta>
            <ta e="T18" id="Seg_634" s="T17">он(а)-PL-COM</ta>
            <ta e="T19" id="Seg_635" s="T18">NEG</ta>
            <ta e="T20" id="Seg_636" s="T19">косить-FUT-1SG.S</ta>
            <ta e="T21" id="Seg_637" s="T20">завтра-ADV.LOC</ta>
            <ta e="T22" id="Seg_638" s="T21">сам.1SG</ta>
            <ta e="T23" id="Seg_639" s="T22">косить-FUT-1SG.S</ta>
            <ta e="T24" id="Seg_640" s="T23">он(а)-PL.[NOM]</ta>
            <ta e="T25" id="Seg_641" s="T24">я.ALL</ta>
            <ta e="T26" id="Seg_642" s="T25">рассердиться-3PL</ta>
            <ta e="T27" id="Seg_643" s="T26">он(а)-PL.[NOM]</ta>
            <ta e="T28" id="Seg_644" s="T27">я.ALL</ta>
            <ta e="T29" id="Seg_645" s="T28">рассердиться-DUR-3PL</ta>
            <ta e="T30" id="Seg_646" s="T29">ну</ta>
            <ta e="T31" id="Seg_647" s="T30">и</ta>
            <ta e="T32" id="Seg_648" s="T31">JUSS</ta>
            <ta e="T33" id="Seg_649" s="T32">рассердиться-%%-3PL</ta>
            <ta e="T34" id="Seg_650" s="T33">я.NOM</ta>
            <ta e="T35" id="Seg_651" s="T34">скосить-PST-1SG.O</ta>
            <ta e="T38" id="Seg_652" s="T37">а</ta>
            <ta e="T39" id="Seg_653" s="T38">он(а)-PL.[NOM]</ta>
            <ta e="T40" id="Seg_654" s="T39">только</ta>
            <ta e="T43" id="Seg_655" s="T42">а</ta>
            <ta e="T44" id="Seg_656" s="T43">теперь</ta>
            <ta e="T45" id="Seg_657" s="T44">мы.[NOM]</ta>
            <ta e="T46" id="Seg_658" s="T45">сено-ACC</ta>
            <ta e="T47" id="Seg_659" s="T46">закончить-1PL</ta>
            <ta e="T48" id="Seg_660" s="T47">теперь</ta>
            <ta e="T49" id="Seg_661" s="T48">мы.[NOM]</ta>
            <ta e="T50" id="Seg_662" s="T49">картошка-ADJZ</ta>
            <ta e="T51" id="Seg_663" s="T50">выкопать-IPFV-CVB</ta>
            <ta e="T52" id="Seg_664" s="T51">начать-DRV-FUT-1PL</ta>
            <ta e="T53" id="Seg_665" s="T52">и</ta>
            <ta e="T54" id="Seg_666" s="T53">мы.[NOM]</ta>
            <ta e="T55" id="Seg_667" s="T54">поделить-CO-1PL</ta>
            <ta e="T56" id="Seg_668" s="T55">десять</ta>
            <ta e="T57" id="Seg_669" s="T56">сотка.[NOM]</ta>
            <ta e="T58" id="Seg_670" s="T57">по</ta>
            <ta e="T59" id="Seg_671" s="T58">я.NOM</ta>
            <ta e="T60" id="Seg_672" s="T59">два</ta>
            <ta e="T61" id="Seg_673" s="T60">день.[NOM]</ta>
            <ta e="T62" id="Seg_674" s="T61">выкопать-CO-1SG.O</ta>
            <ta e="T63" id="Seg_675" s="T62">а</ta>
            <ta e="T64" id="Seg_676" s="T63">он(а)-PL.[NOM]</ta>
            <ta e="T65" id="Seg_677" s="T64">пять</ta>
            <ta e="T66" id="Seg_678" s="T65">день.[NOM]</ta>
            <ta e="T67" id="Seg_679" s="T66">выкопать-IPFV-PST-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_680" s="T1">n-n&gt;adj-n-n:num-n:case</ta>
            <ta e="T3" id="Seg_681" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_682" s="T3">v-v:inf</ta>
            <ta e="T5" id="Seg_683" s="T4">conj</ta>
            <ta e="T6" id="Seg_684" s="T5">adv</ta>
            <ta e="T7" id="Seg_685" s="T6">v-v&gt;adv</ta>
            <ta e="T8" id="Seg_686" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_687" s="T8">pers</ta>
            <ta e="T10" id="Seg_688" s="T9">quant-adj&gt;adv</ta>
            <ta e="T11" id="Seg_689" s="T10">v-v:pn</ta>
            <ta e="T12" id="Seg_690" s="T11">conj</ta>
            <ta e="T13" id="Seg_691" s="T12">pers-n:num.[n:case]</ta>
            <ta e="T14" id="Seg_692" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_693" s="T14">adj-n&gt;adv</ta>
            <ta e="T16" id="Seg_694" s="T15">v-v:pn</ta>
            <ta e="T17" id="Seg_695" s="T16">pers</ta>
            <ta e="T18" id="Seg_696" s="T17">pers-n:num-n:case</ta>
            <ta e="T19" id="Seg_697" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_698" s="T19">v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_699" s="T20">adv-adv:case</ta>
            <ta e="T22" id="Seg_700" s="T21">emphpro</ta>
            <ta e="T23" id="Seg_701" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_702" s="T23">pers-n:num.[n:case]</ta>
            <ta e="T25" id="Seg_703" s="T24">pers</ta>
            <ta e="T26" id="Seg_704" s="T25">v-v:pn</ta>
            <ta e="T27" id="Seg_705" s="T26">pers-n:num.[n:case]</ta>
            <ta e="T28" id="Seg_706" s="T27">pers</ta>
            <ta e="T29" id="Seg_707" s="T28">v-v&gt;v-v:pn</ta>
            <ta e="T30" id="Seg_708" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_709" s="T30">conj</ta>
            <ta e="T32" id="Seg_710" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_711" s="T32">v-%%-v:pn</ta>
            <ta e="T34" id="Seg_712" s="T33">pers</ta>
            <ta e="T35" id="Seg_713" s="T34">v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_714" s="T37">conj</ta>
            <ta e="T39" id="Seg_715" s="T38">pers-n:num.[n:case]</ta>
            <ta e="T40" id="Seg_716" s="T39">ptcl</ta>
            <ta e="T43" id="Seg_717" s="T42">conj</ta>
            <ta e="T44" id="Seg_718" s="T43">adv</ta>
            <ta e="T45" id="Seg_719" s="T44">pers.[n:case]</ta>
            <ta e="T46" id="Seg_720" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_721" s="T46">v-v:pn</ta>
            <ta e="T48" id="Seg_722" s="T47">adv</ta>
            <ta e="T49" id="Seg_723" s="T48">pers.[n:case]</ta>
            <ta e="T50" id="Seg_724" s="T49">n-n&gt;adj</ta>
            <ta e="T51" id="Seg_725" s="T50">v-v&gt;v-v&gt;adv</ta>
            <ta e="T52" id="Seg_726" s="T51">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T53" id="Seg_727" s="T52">conj</ta>
            <ta e="T54" id="Seg_728" s="T53">pers.[n:case]</ta>
            <ta e="T55" id="Seg_729" s="T54">v-v:ins-v:pn</ta>
            <ta e="T56" id="Seg_730" s="T55">num</ta>
            <ta e="T57" id="Seg_731" s="T56">n.[n:case]</ta>
            <ta e="T58" id="Seg_732" s="T57">pp</ta>
            <ta e="T59" id="Seg_733" s="T58">pers</ta>
            <ta e="T60" id="Seg_734" s="T59">num</ta>
            <ta e="T61" id="Seg_735" s="T60">n.[n:case]</ta>
            <ta e="T62" id="Seg_736" s="T61">v-v:ins-v:pn</ta>
            <ta e="T63" id="Seg_737" s="T62">conj</ta>
            <ta e="T64" id="Seg_738" s="T63">pers-n:num.[n:case]</ta>
            <ta e="T65" id="Seg_739" s="T64">num</ta>
            <ta e="T66" id="Seg_740" s="T65">n.[n:case]</ta>
            <ta e="T67" id="Seg_741" s="T66">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_742" s="T1">adj</ta>
            <ta e="T3" id="Seg_743" s="T2">v</ta>
            <ta e="T4" id="Seg_744" s="T3">v</ta>
            <ta e="T5" id="Seg_745" s="T4">conj</ta>
            <ta e="T6" id="Seg_746" s="T5">adv</ta>
            <ta e="T7" id="Seg_747" s="T6">adv</ta>
            <ta e="T8" id="Seg_748" s="T7">v</ta>
            <ta e="T9" id="Seg_749" s="T8">pers</ta>
            <ta e="T10" id="Seg_750" s="T9">quant</ta>
            <ta e="T11" id="Seg_751" s="T10">v</ta>
            <ta e="T12" id="Seg_752" s="T11">conj</ta>
            <ta e="T13" id="Seg_753" s="T12">pers</ta>
            <ta e="T14" id="Seg_754" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_755" s="T14">adv</ta>
            <ta e="T16" id="Seg_756" s="T15">v</ta>
            <ta e="T17" id="Seg_757" s="T16">pers</ta>
            <ta e="T18" id="Seg_758" s="T17">pers</ta>
            <ta e="T19" id="Seg_759" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_760" s="T19">v</ta>
            <ta e="T21" id="Seg_761" s="T20">adv</ta>
            <ta e="T22" id="Seg_762" s="T21">emphpro</ta>
            <ta e="T23" id="Seg_763" s="T22">v</ta>
            <ta e="T24" id="Seg_764" s="T23">pers</ta>
            <ta e="T25" id="Seg_765" s="T24">pers</ta>
            <ta e="T26" id="Seg_766" s="T25">v</ta>
            <ta e="T27" id="Seg_767" s="T26">pers</ta>
            <ta e="T28" id="Seg_768" s="T27">pers</ta>
            <ta e="T29" id="Seg_769" s="T28">v</ta>
            <ta e="T30" id="Seg_770" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_771" s="T30">conj</ta>
            <ta e="T32" id="Seg_772" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_773" s="T32">v</ta>
            <ta e="T34" id="Seg_774" s="T33">pers</ta>
            <ta e="T35" id="Seg_775" s="T34">v</ta>
            <ta e="T38" id="Seg_776" s="T37">conj</ta>
            <ta e="T39" id="Seg_777" s="T38">pers</ta>
            <ta e="T40" id="Seg_778" s="T39">ptcl</ta>
            <ta e="T43" id="Seg_779" s="T42">conj</ta>
            <ta e="T44" id="Seg_780" s="T43">adv</ta>
            <ta e="T45" id="Seg_781" s="T44">pers</ta>
            <ta e="T46" id="Seg_782" s="T45">n</ta>
            <ta e="T47" id="Seg_783" s="T46">v</ta>
            <ta e="T48" id="Seg_784" s="T47">adv</ta>
            <ta e="T49" id="Seg_785" s="T48">pers</ta>
            <ta e="T50" id="Seg_786" s="T49">adj</ta>
            <ta e="T51" id="Seg_787" s="T50">adv</ta>
            <ta e="T52" id="Seg_788" s="T51">v</ta>
            <ta e="T53" id="Seg_789" s="T52">conj</ta>
            <ta e="T54" id="Seg_790" s="T53">pers</ta>
            <ta e="T55" id="Seg_791" s="T54">v</ta>
            <ta e="T56" id="Seg_792" s="T55">num</ta>
            <ta e="T57" id="Seg_793" s="T56">n</ta>
            <ta e="T58" id="Seg_794" s="T57">pp</ta>
            <ta e="T59" id="Seg_795" s="T58">pers</ta>
            <ta e="T60" id="Seg_796" s="T59">num</ta>
            <ta e="T61" id="Seg_797" s="T60">n</ta>
            <ta e="T62" id="Seg_798" s="T61">v</ta>
            <ta e="T63" id="Seg_799" s="T62">conj</ta>
            <ta e="T64" id="Seg_800" s="T63">pers</ta>
            <ta e="T65" id="Seg_801" s="T64">num</ta>
            <ta e="T66" id="Seg_802" s="T65">v</ta>
            <ta e="T67" id="Seg_803" s="T66">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_804" s="T1">np.h:Th</ta>
            <ta e="T3" id="Seg_805" s="T2">0.1.h:A</ta>
            <ta e="T6" id="Seg_806" s="T5">adv:Time</ta>
            <ta e="T8" id="Seg_807" s="T7">0.1.h:A</ta>
            <ta e="T9" id="Seg_808" s="T8">pro.h:A</ta>
            <ta e="T11" id="Seg_809" s="T10">0.3:P</ta>
            <ta e="T13" id="Seg_810" s="T12">pro.h:A</ta>
            <ta e="T16" id="Seg_811" s="T15">0.3:P</ta>
            <ta e="T17" id="Seg_812" s="T16">pro.h:A</ta>
            <ta e="T18" id="Seg_813" s="T17">pro:Com</ta>
            <ta e="T21" id="Seg_814" s="T20">adv:Time</ta>
            <ta e="T23" id="Seg_815" s="T22">0.1.h:A</ta>
            <ta e="T24" id="Seg_816" s="T23">pro.h:E</ta>
            <ta e="T25" id="Seg_817" s="T24">pro.h:R</ta>
            <ta e="T27" id="Seg_818" s="T26">pro.h:E</ta>
            <ta e="T28" id="Seg_819" s="T27">pro.h:R</ta>
            <ta e="T33" id="Seg_820" s="T32">0.3.h:E</ta>
            <ta e="T34" id="Seg_821" s="T33">pro.h:A</ta>
            <ta e="T39" id="Seg_822" s="T38">pro.h:A</ta>
            <ta e="T44" id="Seg_823" s="T43">adv:Time</ta>
            <ta e="T45" id="Seg_824" s="T44">pro.h:A</ta>
            <ta e="T46" id="Seg_825" s="T45">np:Th</ta>
            <ta e="T48" id="Seg_826" s="T47">adv:Time</ta>
            <ta e="T49" id="Seg_827" s="T48">pro.h:A</ta>
            <ta e="T50" id="Seg_828" s="T49">np:Th</ta>
            <ta e="T54" id="Seg_829" s="T53">pro.h:A</ta>
            <ta e="T59" id="Seg_830" s="T58">pro.h:A</ta>
            <ta e="T61" id="Seg_831" s="T60">np:Time</ta>
            <ta e="T62" id="Seg_832" s="T61">0.3:Th</ta>
            <ta e="T64" id="Seg_833" s="T63">pro.h:A</ta>
            <ta e="T66" id="Seg_834" s="T65">np:Time</ta>
            <ta e="T67" id="Seg_835" s="T66">0.3:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_836" s="T1">np.h:O</ta>
            <ta e="T3" id="Seg_837" s="T2">0.1.h:S v:pred</ta>
            <ta e="T4" id="Seg_838" s="T3">s:purp</ta>
            <ta e="T7" id="Seg_839" s="T6">s:purp</ta>
            <ta e="T8" id="Seg_840" s="T7">0.1.h:S v:pred</ta>
            <ta e="T9" id="Seg_841" s="T8">pro.h:S</ta>
            <ta e="T11" id="Seg_842" s="T10">v:pred 0.3:O</ta>
            <ta e="T13" id="Seg_843" s="T12">pro.h:S</ta>
            <ta e="T16" id="Seg_844" s="T15">v:pred 0.3:O</ta>
            <ta e="T17" id="Seg_845" s="T16">pro.h:S</ta>
            <ta e="T20" id="Seg_846" s="T19">v:pred</ta>
            <ta e="T23" id="Seg_847" s="T22">0.1.h:S v:pred</ta>
            <ta e="T24" id="Seg_848" s="T23">pro.h:S</ta>
            <ta e="T26" id="Seg_849" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_850" s="T26">pro.h:S</ta>
            <ta e="T29" id="Seg_851" s="T28">v:pred</ta>
            <ta e="T33" id="Seg_852" s="T32">0.3.h:S v:pred</ta>
            <ta e="T34" id="Seg_853" s="T33">pro.h:S</ta>
            <ta e="T35" id="Seg_854" s="T34">v:pred</ta>
            <ta e="T39" id="Seg_855" s="T38">pro.h:S</ta>
            <ta e="T45" id="Seg_856" s="T44">pro.h:S</ta>
            <ta e="T46" id="Seg_857" s="T45">np:O</ta>
            <ta e="T47" id="Seg_858" s="T46">v:pred</ta>
            <ta e="T49" id="Seg_859" s="T48">pro.h:S</ta>
            <ta e="T52" id="Seg_860" s="T51">v:pred</ta>
            <ta e="T54" id="Seg_861" s="T53">pro.h:S</ta>
            <ta e="T55" id="Seg_862" s="T54">v:pred</ta>
            <ta e="T59" id="Seg_863" s="T58">pro.h:S</ta>
            <ta e="T62" id="Seg_864" s="T61">v:pred 0.3:O</ta>
            <ta e="T64" id="Seg_865" s="T63">pro.h:S</ta>
            <ta e="T67" id="Seg_866" s="T66">v:pred 0.3:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_867" s="T4">RUS:gram</ta>
            <ta e="T6" id="Seg_868" s="T5">RUS:disc</ta>
            <ta e="T12" id="Seg_869" s="T11">RUS:gram</ta>
            <ta e="T30" id="Seg_870" s="T29">RUS:disc</ta>
            <ta e="T31" id="Seg_871" s="T30">RUS:gram</ta>
            <ta e="T32" id="Seg_872" s="T31">RUS:gram</ta>
            <ta e="T38" id="Seg_873" s="T37">RUS:gram</ta>
            <ta e="T40" id="Seg_874" s="T39">RUS:disc</ta>
            <ta e="T43" id="Seg_875" s="T42">RUS:gram</ta>
            <ta e="T44" id="Seg_876" s="T43">RUS:core</ta>
            <ta e="T48" id="Seg_877" s="T47">RUS:core</ta>
            <ta e="T50" id="Seg_878" s="T49">RUS:cult</ta>
            <ta e="T53" id="Seg_879" s="T52">RUS:gram</ta>
            <ta e="T57" id="Seg_880" s="T56">RUS:cult</ta>
            <ta e="T63" id="Seg_881" s="T62">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_882" s="T1">I'll call women to go swimming.</ta>
            <ta e="T8" id="Seg_883" s="T4">And then we'll go mowing.</ta>
            <ta e="T11" id="Seg_884" s="T8">I mowed a lot [of hay].</ta>
            <ta e="T16" id="Seg_885" s="T11">And they mowed little [hay].</ta>
            <ta e="T20" id="Seg_886" s="T16">I won't mow with them.</ta>
            <ta e="T23" id="Seg_887" s="T20">Tomorrow I'll mow myself.</ta>
            <ta e="T26" id="Seg_888" s="T23">They got angry with me.</ta>
            <ta e="T29" id="Seg_889" s="T26">They get angry with me.</ta>
            <ta e="T33" id="Seg_890" s="T29">They can be angry.</ta>
            <ta e="T42" id="Seg_891" s="T33">I mowed 0.35 hectares, and they [mowed] only 0.15 hectares.</ta>
            <ta e="T47" id="Seg_892" s="T42">And now we finished mowing.</ta>
            <ta e="T52" id="Seg_893" s="T47">Now we start digging out potatoes.</ta>
            <ta e="T58" id="Seg_894" s="T52">And we divided it, 0.1 hectares for each of us.</ta>
            <ta e="T67" id="Seg_895" s="T58">I digged it out in two days, and they were digging five days.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_896" s="T1">Ich rufe die Frauen zum Schwimmen.</ta>
            <ta e="T8" id="Seg_897" s="T4">Und dann gehen wir mähen.</ta>
            <ta e="T11" id="Seg_898" s="T8">Ich mähte viel [Heu].</ta>
            <ta e="T16" id="Seg_899" s="T11">Und sie mähten wenig [Heu].</ta>
            <ta e="T20" id="Seg_900" s="T16">Ich mähe nicht mehr mit ihnen.</ta>
            <ta e="T23" id="Seg_901" s="T20">Morgen mähe ich alleine.</ta>
            <ta e="T26" id="Seg_902" s="T23">Sie wurden wütend auf mich.</ta>
            <ta e="T29" id="Seg_903" s="T26">Sie werden wütend auf mich.</ta>
            <ta e="T33" id="Seg_904" s="T29">Sie können wütend sein.</ta>
            <ta e="T42" id="Seg_905" s="T33">Ich mähte 35 Ar (0,35 Hektar) und sie [mähten] nur 15 Ar (0,15 Hektar).</ta>
            <ta e="T47" id="Seg_906" s="T42">Und jetzt haben wir aufgehört zu mähen.</ta>
            <ta e="T52" id="Seg_907" s="T47">Jetzt fangen wir an, Kartoffeln auszugraben.</ta>
            <ta e="T58" id="Seg_908" s="T52">Und wir teilten es auf, 10 Ar (0,1 Hektar) für jeden von uns.</ta>
            <ta e="T67" id="Seg_909" s="T58">Ich grub sie in zwei Tagen aus und sie gruben fünf Tage lang.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_910" s="T1">Женщин я буду звать купаться.</ta>
            <ta e="T8" id="Seg_911" s="T4">А потом мы пойдем косить.</ta>
            <ta e="T11" id="Seg_912" s="T8">Я много накосила [сена].</ta>
            <ta e="T16" id="Seg_913" s="T11">А они мало накосили.</ta>
            <ta e="T20" id="Seg_914" s="T16">Я с ними не буду косить.</ta>
            <ta e="T23" id="Seg_915" s="T20">Завтра я сама буду косить.</ta>
            <ta e="T26" id="Seg_916" s="T23">Они на меня рассердились.</ta>
            <ta e="T29" id="Seg_917" s="T26">Они на меня сердиться будут.</ta>
            <ta e="T33" id="Seg_918" s="T29">Ну и пускай сердятся.</ta>
            <ta e="T42" id="Seg_919" s="T33">Я выкосила 35 соток, а они только 15 соток.</ta>
            <ta e="T47" id="Seg_920" s="T42">А теперь мы сено кончили [косить].</ta>
            <ta e="T52" id="Seg_921" s="T47">Мы теперь картошку копать начинаем.</ta>
            <ta e="T58" id="Seg_922" s="T52">И по десять соток мы разделили.</ta>
            <ta e="T67" id="Seg_923" s="T58">Я за два дня выкопала, а они пять дней копали.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_924" s="T1">звать буду я их (женщин) купаться</ta>
            <ta e="T8" id="Seg_925" s="T4">а потом пойдем косить</ta>
            <ta e="T11" id="Seg_926" s="T8">я много накосила сена</ta>
            <ta e="T16" id="Seg_927" s="T11">они мало накосили</ta>
            <ta e="T20" id="Seg_928" s="T16">я с ними не буду косить</ta>
            <ta e="T23" id="Seg_929" s="T20">завтра я сама буду косить</ta>
            <ta e="T26" id="Seg_930" s="T23">они на меня рассердились</ta>
            <ta e="T29" id="Seg_931" s="T26">они на меня рассердятся</ta>
            <ta e="T33" id="Seg_932" s="T29">пускай сердятся</ta>
            <ta e="T42" id="Seg_933" s="T33">я выкосила 35 соток а они 15 соток</ta>
            <ta e="T47" id="Seg_934" s="T42">а теперь мы сено кончили</ta>
            <ta e="T52" id="Seg_935" s="T47">мы теперь картошку копать начинаем</ta>
            <ta e="T58" id="Seg_936" s="T52">и по десять соток мы разделили</ta>
            <ta e="T67" id="Seg_937" s="T58">я за два дня выкопала а они пять дней</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T11" id="Seg_938" s="T8">[KuAI:] Variants: 'koːcen', 'kɛːtaw'.</ta>
            <ta e="T23" id="Seg_939" s="T20"> ‎‎[KuAI:] Variant: 'tajtetʒan'.</ta>
            <ta e="T42" id="Seg_940" s="T33">[KuAI:] Variant: 'qɛːtəzaw'.</ta>
            <ta e="T52" id="Seg_941" s="T47">[BrM:] Tentative analysis of 'juːbəretčtu'.</ta>
            <ta e="T58" id="Seg_942" s="T52">[KuAI:] Variant: 'taːrnaftə'. [BrM:] 'sotkadʼar' changed to 'sotkadʼar'.</ta>
            <ta e="T67" id="Seg_943" s="T58">[KuAI:] Variant: 'sətʼel'. [BrM:] 'sɨtʼel' changed to 'sɨt tʼel'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
