<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_HusbandClosedWife_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_HusbandClosedWife_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">27</ud-information>
            <ud-information attribute-name="# HIAT:w">22</ud-information>
            <ud-information attribute-name="# e">22</ud-information>
            <ud-information attribute-name="# HIAT:u">5</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T23" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Täp</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">aːmda</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">qättɨpbɨlʼe</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Täbɨm</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">ärat</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">qättɨbat</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">Krigarʼenko</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">Ilʼenamdə</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">kɨdan</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">qättukustə</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">maːttə</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">poze</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">čänčukustə</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_50" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">A</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">qaj</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">pom</ts>
                  <nts id="Seg_59" n="HIAT:ip">?</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_62" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">Pom</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">innä</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">watʼeǯɨt</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_73" n="HIAT:w" s="T20">i</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">maːttə</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">serčɨn</ts>
                  <nts id="Seg_80" n="HIAT:ip">.</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T23" id="Seg_82" n="sc" s="T1">
               <ts e="T2" id="Seg_84" n="e" s="T1">Täp </ts>
               <ts e="T3" id="Seg_86" n="e" s="T2">aːmda </ts>
               <ts e="T4" id="Seg_88" n="e" s="T3">qättɨpbɨlʼe. </ts>
               <ts e="T5" id="Seg_90" n="e" s="T4">Täbɨm </ts>
               <ts e="T6" id="Seg_92" n="e" s="T5">ärat </ts>
               <ts e="T7" id="Seg_94" n="e" s="T6">qättɨbat. </ts>
               <ts e="T8" id="Seg_96" n="e" s="T7">Krigarʼenko </ts>
               <ts e="T9" id="Seg_98" n="e" s="T8">Ilʼenamdə </ts>
               <ts e="T10" id="Seg_100" n="e" s="T9">kɨdan </ts>
               <ts e="T11" id="Seg_102" n="e" s="T10">qättukustə </ts>
               <ts e="T12" id="Seg_104" n="e" s="T11">maːttə </ts>
               <ts e="T13" id="Seg_106" n="e" s="T12">poze </ts>
               <ts e="T14" id="Seg_108" n="e" s="T13">čänčukustə. </ts>
               <ts e="T15" id="Seg_110" n="e" s="T14">A </ts>
               <ts e="T16" id="Seg_112" n="e" s="T15">qaj </ts>
               <ts e="T17" id="Seg_114" n="e" s="T16">pom? </ts>
               <ts e="T18" id="Seg_116" n="e" s="T17">Pom </ts>
               <ts e="T19" id="Seg_118" n="e" s="T18">innä </ts>
               <ts e="T20" id="Seg_120" n="e" s="T19">watʼeǯɨt </ts>
               <ts e="T21" id="Seg_122" n="e" s="T20">i </ts>
               <ts e="T22" id="Seg_124" n="e" s="T21">maːttə </ts>
               <ts e="T23" id="Seg_126" n="e" s="T22">serčɨn. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_127" s="T1">PVD_1964_HusbandClosedWife_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_128" s="T4">PVD_1964_HusbandClosedWife_nar.002 (001.002)</ta>
            <ta e="T14" id="Seg_129" s="T7">PVD_1964_HusbandClosedWife_nar.003 (001.003)</ta>
            <ta e="T17" id="Seg_130" s="T14">PVD_1964_HusbandClosedWife_nar.004 (001.004)</ta>
            <ta e="T23" id="Seg_131" s="T17">PVD_1964_HusbandClosedWife_nar.005 (001.005)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_132" s="T1">тӓп а̄мда ′kӓттыпбылʼе.</ta>
            <ta e="T7" id="Seg_133" s="T4">′тӓбым ӓ′рат ′kӓттыбат.</ta>
            <ta e="T14" id="Seg_134" s="T7">Крига′рʼенко и′лʼенамдъ ′кыдан kӓттукустъ ма̄ттъ по′зе ′тшӓнтшу′кустъ.</ta>
            <ta e="T17" id="Seg_135" s="T14">а kай пом?</ta>
            <ta e="T23" id="Seg_136" s="T17">пом ин′нӓ ва′тʼеджыт и ма̄ттъ ′сертшын.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_137" s="T1">täp aːmda qättɨpbɨlʼe.</ta>
            <ta e="T7" id="Seg_138" s="T4">täbɨm ärat qättɨbat.</ta>
            <ta e="T14" id="Seg_139" s="T7">Кrigarʼenko ilʼenamdə kɨdan qättukustə maːttə poze tšäntšukustə.</ta>
            <ta e="T17" id="Seg_140" s="T14">a qaj pom?</ta>
            <ta e="T23" id="Seg_141" s="T17">pom innä watʼeǯɨt i maːttə sertšɨn.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_142" s="T1">Täp aːmda qättɨpbɨlʼe. </ta>
            <ta e="T7" id="Seg_143" s="T4">Täbɨm ärat qättɨbat. </ta>
            <ta e="T14" id="Seg_144" s="T7">Krigarʼenko Ilʼenamdə kɨdan qättukustə maːttə poze čänčukustə. </ta>
            <ta e="T17" id="Seg_145" s="T14">A qaj pom? </ta>
            <ta e="T23" id="Seg_146" s="T17">Pom innä watʼeǯɨt i maːttə serčɨn. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_147" s="T1">täp</ta>
            <ta e="T3" id="Seg_148" s="T2">aːmda</ta>
            <ta e="T4" id="Seg_149" s="T3">qättɨ-pbɨ-lʼe</ta>
            <ta e="T5" id="Seg_150" s="T4">täb-ɨ-m</ta>
            <ta e="T6" id="Seg_151" s="T5">ära-t</ta>
            <ta e="T7" id="Seg_152" s="T6">qättɨ-ba-t</ta>
            <ta e="T8" id="Seg_153" s="T7">Krigarʼenko</ta>
            <ta e="T9" id="Seg_154" s="T8">Ilʼena-m-də</ta>
            <ta e="T10" id="Seg_155" s="T9">kɨdan</ta>
            <ta e="T11" id="Seg_156" s="T10">qättu-ku-s-tə</ta>
            <ta e="T12" id="Seg_157" s="T11">maːt-tə</ta>
            <ta e="T13" id="Seg_158" s="T12">po-ze</ta>
            <ta e="T14" id="Seg_159" s="T13">čänču-ku-s-tə</ta>
            <ta e="T15" id="Seg_160" s="T14">a</ta>
            <ta e="T16" id="Seg_161" s="T15">qaj</ta>
            <ta e="T17" id="Seg_162" s="T16">po-m</ta>
            <ta e="T18" id="Seg_163" s="T17">po-m</ta>
            <ta e="T19" id="Seg_164" s="T18">innä</ta>
            <ta e="T20" id="Seg_165" s="T19">watʼ-eǯɨ-t</ta>
            <ta e="T21" id="Seg_166" s="T20">i</ta>
            <ta e="T22" id="Seg_167" s="T21">maːt-tə</ta>
            <ta e="T23" id="Seg_168" s="T22">ser-čɨ-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_169" s="T1">täp</ta>
            <ta e="T3" id="Seg_170" s="T2">amdɨ</ta>
            <ta e="T4" id="Seg_171" s="T3">qättɨ-mbɨ-le</ta>
            <ta e="T5" id="Seg_172" s="T4">täp-ɨ-m</ta>
            <ta e="T6" id="Seg_173" s="T5">era-tə</ta>
            <ta e="T7" id="Seg_174" s="T6">qättɨ-mbɨ-t</ta>
            <ta e="T8" id="Seg_175" s="T7">Krigarʼenko</ta>
            <ta e="T9" id="Seg_176" s="T8">Ilʼena-m-tə</ta>
            <ta e="T10" id="Seg_177" s="T9">qɨdan</ta>
            <ta e="T11" id="Seg_178" s="T10">qättɨ-ku-sɨ-t</ta>
            <ta e="T12" id="Seg_179" s="T11">maːt-ntə</ta>
            <ta e="T13" id="Seg_180" s="T12">po-se</ta>
            <ta e="T14" id="Seg_181" s="T13">čänču-ku-sɨ-t</ta>
            <ta e="T15" id="Seg_182" s="T14">a</ta>
            <ta e="T16" id="Seg_183" s="T15">qaj</ta>
            <ta e="T17" id="Seg_184" s="T16">po-m</ta>
            <ta e="T18" id="Seg_185" s="T17">po-m</ta>
            <ta e="T19" id="Seg_186" s="T18">innä</ta>
            <ta e="T20" id="Seg_187" s="T19">watʼtʼi-enǯɨ-t</ta>
            <ta e="T21" id="Seg_188" s="T20">i</ta>
            <ta e="T22" id="Seg_189" s="T21">maːt-ntə</ta>
            <ta e="T23" id="Seg_190" s="T22">ser-enǯɨ-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_191" s="T1">(s)he.[NOM]</ta>
            <ta e="T3" id="Seg_192" s="T2">sit.[3SG.S]</ta>
            <ta e="T4" id="Seg_193" s="T3">close-RES-CVB</ta>
            <ta e="T5" id="Seg_194" s="T4">(s)he-EP-ACC</ta>
            <ta e="T6" id="Seg_195" s="T5">husband.[NOM]-3SG</ta>
            <ta e="T7" id="Seg_196" s="T6">close-PST.NAR-3SG.O</ta>
            <ta e="T8" id="Seg_197" s="T7">Grigorenko.[NOM]</ta>
            <ta e="T9" id="Seg_198" s="T8">Elena-ACC-3SG</ta>
            <ta e="T10" id="Seg_199" s="T9">all.the.time</ta>
            <ta e="T11" id="Seg_200" s="T10">close-HAB-PST-3SG.O</ta>
            <ta e="T12" id="Seg_201" s="T11">house-ILL</ta>
            <ta e="T13" id="Seg_202" s="T12">stick-COM</ta>
            <ta e="T14" id="Seg_203" s="T13">%close-HAB-PST-3SG.O</ta>
            <ta e="T15" id="Seg_204" s="T14">and</ta>
            <ta e="T16" id="Seg_205" s="T15">what.[NOM]</ta>
            <ta e="T17" id="Seg_206" s="T16">stick-ACC</ta>
            <ta e="T18" id="Seg_207" s="T17">stick-ACC</ta>
            <ta e="T19" id="Seg_208" s="T18">up</ta>
            <ta e="T20" id="Seg_209" s="T19">lift-FUT-3SG.O</ta>
            <ta e="T21" id="Seg_210" s="T20">and</ta>
            <ta e="T22" id="Seg_211" s="T21">house-ILL</ta>
            <ta e="T23" id="Seg_212" s="T22">come.in-FUT-3SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_213" s="T1">он(а).[NOM]</ta>
            <ta e="T3" id="Seg_214" s="T2">сидеть.[3SG.S]</ta>
            <ta e="T4" id="Seg_215" s="T3">закрыть-RES-CVB</ta>
            <ta e="T5" id="Seg_216" s="T4">он(а)-EP-ACC</ta>
            <ta e="T6" id="Seg_217" s="T5">муж.[NOM]-3SG</ta>
            <ta e="T7" id="Seg_218" s="T6">закрыть-PST.NAR-3SG.O</ta>
            <ta e="T8" id="Seg_219" s="T7">Григоренко.[NOM]</ta>
            <ta e="T9" id="Seg_220" s="T8">Елена-ACC-3SG</ta>
            <ta e="T10" id="Seg_221" s="T9">все.время</ta>
            <ta e="T11" id="Seg_222" s="T10">закрыть-HAB-PST-3SG.O</ta>
            <ta e="T12" id="Seg_223" s="T11">дом-ILL</ta>
            <ta e="T13" id="Seg_224" s="T12">палка-COM</ta>
            <ta e="T14" id="Seg_225" s="T13">%закрыть-HAB-PST-3SG.O</ta>
            <ta e="T15" id="Seg_226" s="T14">а</ta>
            <ta e="T16" id="Seg_227" s="T15">что.[NOM]</ta>
            <ta e="T17" id="Seg_228" s="T16">палка-ACC</ta>
            <ta e="T18" id="Seg_229" s="T17">палка-ACC</ta>
            <ta e="T19" id="Seg_230" s="T18">наверх</ta>
            <ta e="T20" id="Seg_231" s="T19">поднять-FUT-3SG.O</ta>
            <ta e="T21" id="Seg_232" s="T20">и</ta>
            <ta e="T22" id="Seg_233" s="T21">дом-ILL</ta>
            <ta e="T23" id="Seg_234" s="T22">зайти-FUT-3SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_235" s="T1">pers.[n:case]</ta>
            <ta e="T3" id="Seg_236" s="T2">v.[v:pn]</ta>
            <ta e="T4" id="Seg_237" s="T3">v-v&gt;v-v&gt;adv</ta>
            <ta e="T5" id="Seg_238" s="T4">pers-n:ins-n:case</ta>
            <ta e="T6" id="Seg_239" s="T5">n.[n:case]-n:poss</ta>
            <ta e="T7" id="Seg_240" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_241" s="T7">nprop.[n:case]</ta>
            <ta e="T9" id="Seg_242" s="T8">nprop-n:case-n:poss</ta>
            <ta e="T10" id="Seg_243" s="T9">adv</ta>
            <ta e="T11" id="Seg_244" s="T10">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_245" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_246" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_247" s="T13">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_248" s="T14">conj</ta>
            <ta e="T16" id="Seg_249" s="T15">interrog.[n:case]</ta>
            <ta e="T17" id="Seg_250" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_251" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_252" s="T18">adv</ta>
            <ta e="T20" id="Seg_253" s="T19">v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_254" s="T20">conj</ta>
            <ta e="T22" id="Seg_255" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_256" s="T22">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_257" s="T1">pers</ta>
            <ta e="T3" id="Seg_258" s="T2">v</ta>
            <ta e="T4" id="Seg_259" s="T3">v</ta>
            <ta e="T5" id="Seg_260" s="T4">pers</ta>
            <ta e="T6" id="Seg_261" s="T5">n</ta>
            <ta e="T7" id="Seg_262" s="T6">v</ta>
            <ta e="T8" id="Seg_263" s="T7">nprop</ta>
            <ta e="T9" id="Seg_264" s="T8">nprop</ta>
            <ta e="T10" id="Seg_265" s="T9">adv</ta>
            <ta e="T11" id="Seg_266" s="T10">v</ta>
            <ta e="T12" id="Seg_267" s="T11">n</ta>
            <ta e="T13" id="Seg_268" s="T12">n</ta>
            <ta e="T14" id="Seg_269" s="T13">v</ta>
            <ta e="T15" id="Seg_270" s="T14">conj</ta>
            <ta e="T16" id="Seg_271" s="T15">interrog</ta>
            <ta e="T17" id="Seg_272" s="T16">n</ta>
            <ta e="T18" id="Seg_273" s="T17">n</ta>
            <ta e="T19" id="Seg_274" s="T18">adv</ta>
            <ta e="T20" id="Seg_275" s="T19">v</ta>
            <ta e="T21" id="Seg_276" s="T20">conj</ta>
            <ta e="T22" id="Seg_277" s="T21">n</ta>
            <ta e="T23" id="Seg_278" s="T22">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_279" s="T1">pro.h:Th</ta>
            <ta e="T5" id="Seg_280" s="T4">pro.h:P</ta>
            <ta e="T6" id="Seg_281" s="T5">np.h:A 0.3.h:Poss</ta>
            <ta e="T8" id="Seg_282" s="T7">np.h:A</ta>
            <ta e="T9" id="Seg_283" s="T8">np.h:P 0.3.h:Poss</ta>
            <ta e="T10" id="Seg_284" s="T9">adv:Time</ta>
            <ta e="T12" id="Seg_285" s="T11">np:G</ta>
            <ta e="T13" id="Seg_286" s="T12">np:Ins</ta>
            <ta e="T14" id="Seg_287" s="T13">0.3.h:A 0.3.h:P</ta>
            <ta e="T18" id="Seg_288" s="T17">np:Th</ta>
            <ta e="T19" id="Seg_289" s="T18">adv:G</ta>
            <ta e="T20" id="Seg_290" s="T19">0.3.h:A</ta>
            <ta e="T22" id="Seg_291" s="T21">np:G</ta>
            <ta e="T23" id="Seg_292" s="T22">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_293" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_294" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_295" s="T3">s:adv</ta>
            <ta e="T5" id="Seg_296" s="T4">pro.h:O</ta>
            <ta e="T6" id="Seg_297" s="T5">np.h:S</ta>
            <ta e="T7" id="Seg_298" s="T6">v:pred</ta>
            <ta e="T8" id="Seg_299" s="T7">np.h:S</ta>
            <ta e="T9" id="Seg_300" s="T8">np.h:O</ta>
            <ta e="T11" id="Seg_301" s="T10">v:pred</ta>
            <ta e="T14" id="Seg_302" s="T13">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T18" id="Seg_303" s="T17">np:O</ta>
            <ta e="T20" id="Seg_304" s="T19">0.3.h:S v:pred</ta>
            <ta e="T23" id="Seg_305" s="T22">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T15" id="Seg_306" s="T14">RUS:gram</ta>
            <ta e="T21" id="Seg_307" s="T20">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_308" s="T1">She is locked up.</ta>
            <ta e="T7" id="Seg_309" s="T4">Her husband locked her up.</ta>
            <ta e="T14" id="Seg_310" s="T7">Grigorenko always locked up Elena at home with a stick.</ta>
            <ta e="T17" id="Seg_311" s="T14">And why with a stick?</ta>
            <ta e="T23" id="Seg_312" s="T17">He lifts the stick and comes into the house.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_313" s="T1">Sie ist eingesperrt.</ta>
            <ta e="T7" id="Seg_314" s="T4">Ihr Mann hat sie eingesperrt.</ta>
            <ta e="T14" id="Seg_315" s="T7">Grigorenko sperrt Elena immer zu Hause mit einem Stock ein.</ta>
            <ta e="T17" id="Seg_316" s="T14">Und warum mit einem Stock?</ta>
            <ta e="T23" id="Seg_317" s="T17">Er hebt den Stock an und kommt ins Haus.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_318" s="T1">Она сидит взаперти.</ta>
            <ta e="T7" id="Seg_319" s="T4">Ее закрыл муж.</ta>
            <ta e="T14" id="Seg_320" s="T7">Григоренко всегда Елену в избе закрывал на палку.</ta>
            <ta e="T17" id="Seg_321" s="T14">А чего палку?</ta>
            <ta e="T23" id="Seg_322" s="T17">Палку поднимет и в избу войдет.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_323" s="T1">она сидит взаперти</ta>
            <ta e="T7" id="Seg_324" s="T4">ее закрыл муж</ta>
            <ta e="T14" id="Seg_325" s="T7">Григоренко всегда Елену в избу (избе) закрывал на палку (бревно)</ta>
            <ta e="T17" id="Seg_326" s="T14">а чего палку</ta>
            <ta e="T23" id="Seg_327" s="T17">палку поднимет и в избу войдет</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
