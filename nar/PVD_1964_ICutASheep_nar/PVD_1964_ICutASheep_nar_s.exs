<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_ICutASheep_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_ICutASheep_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">69</ud-information>
            <ud-information attribute-name="# HIAT:w">53</ud-information>
            <ud-information attribute-name="# e">53</ud-information>
            <ud-information attribute-name="# HIAT:u">11</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T54" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">depkaɣän</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">jezan</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Ugon</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">konärlaj</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">pičelʼe</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">übəran</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">Okqɨr</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">konärkam</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">maːttə</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">tulǯukou</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_44" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">Peʒɨkou</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">i</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">konäram</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">ponä</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">qwatdəqou</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_62" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">Tulǯau</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">paranam</ts>
                  <nts id="Seg_68" n="HIAT:ip">,</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">nʼäpbodʼäu</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">maːttə</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_78" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">Täp</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">kak</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">kurolǯa</ts>
                  <nts id="Seg_87" n="HIAT:ip">,</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_90" n="HIAT:w" s="T24">akoškat</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_93" n="HIAT:w" s="T25">kak</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_96" n="HIAT:w" s="T26">paqtädʼit</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_99" n="HIAT:w" s="T27">i</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_102" n="HIAT:w" s="T28">tokkuwanän</ts>
                  <nts id="Seg_103" n="HIAT:ip">.</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_106" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_108" n="HIAT:w" s="T29">Sət</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_111" n="HIAT:w" s="T30">klʼetkam</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_114" n="HIAT:w" s="T31">panannɨt</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_117" n="HIAT:w" s="T32">i</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_120" n="HIAT:w" s="T33">tokkuwatpɨkun</ts>
                  <nts id="Seg_121" n="HIAT:ip">.</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_124" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_126" n="HIAT:w" s="T34">Man</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_129" n="HIAT:w" s="T35">ügulgou</ts>
                  <nts id="Seg_130" n="HIAT:ip">,</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_133" n="HIAT:w" s="T36">ügulgou</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_137" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_139" n="HIAT:w" s="T37">Qudnäj</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_142" n="HIAT:w" s="T38">tʼakgus</ts>
                  <nts id="Seg_143" n="HIAT:ip">,</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_146" n="HIAT:w" s="T39">xotʼ</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_149" n="HIAT:w" s="T40">tʼürak</ts>
                  <nts id="Seg_150" n="HIAT:ip">,</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_153" n="HIAT:w" s="T41">man</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_156" n="HIAT:w" s="T42">pelgattə</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_159" n="HIAT:w" s="T43">jewan</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_163" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_165" n="HIAT:w" s="T44">Nasel</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_168" n="HIAT:w" s="T45">ügunnau</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_171" n="HIAT:w" s="T46">i</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_174" n="HIAT:w" s="T47">saːrau</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_177" n="HIAT:w" s="T48">kudugozʼe</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_180" n="HIAT:w" s="T49">i</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_183" n="HIAT:w" s="T50">peʒau</ts>
                  <nts id="Seg_184" n="HIAT:ip">.</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_187" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_189" n="HIAT:w" s="T51">I</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_192" n="HIAT:w" s="T52">ponä</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_195" n="HIAT:w" s="T53">nʼäpbodʼäu</ts>
                  <nts id="Seg_196" n="HIAT:ip">.</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T54" id="Seg_198" n="sc" s="T1">
               <ts e="T2" id="Seg_200" n="e" s="T1">Man </ts>
               <ts e="T3" id="Seg_202" n="e" s="T2">depkaɣän </ts>
               <ts e="T4" id="Seg_204" n="e" s="T3">jezan. </ts>
               <ts e="T5" id="Seg_206" n="e" s="T4">Ugon </ts>
               <ts e="T6" id="Seg_208" n="e" s="T5">konärlaj </ts>
               <ts e="T7" id="Seg_210" n="e" s="T6">pičelʼe </ts>
               <ts e="T8" id="Seg_212" n="e" s="T7">übəran. </ts>
               <ts e="T9" id="Seg_214" n="e" s="T8">Okqɨr </ts>
               <ts e="T10" id="Seg_216" n="e" s="T9">konärkam </ts>
               <ts e="T11" id="Seg_218" n="e" s="T10">maːttə </ts>
               <ts e="T12" id="Seg_220" n="e" s="T11">tulǯukou. </ts>
               <ts e="T13" id="Seg_222" n="e" s="T12">Peʒɨkou </ts>
               <ts e="T14" id="Seg_224" n="e" s="T13">i </ts>
               <ts e="T15" id="Seg_226" n="e" s="T14">konäram </ts>
               <ts e="T16" id="Seg_228" n="e" s="T15">ponä </ts>
               <ts e="T17" id="Seg_230" n="e" s="T16">qwatdəqou. </ts>
               <ts e="T18" id="Seg_232" n="e" s="T17">Tulǯau </ts>
               <ts e="T19" id="Seg_234" n="e" s="T18">paranam, </ts>
               <ts e="T20" id="Seg_236" n="e" s="T19">nʼäpbodʼäu </ts>
               <ts e="T21" id="Seg_238" n="e" s="T20">maːttə. </ts>
               <ts e="T22" id="Seg_240" n="e" s="T21">Täp </ts>
               <ts e="T23" id="Seg_242" n="e" s="T22">kak </ts>
               <ts e="T24" id="Seg_244" n="e" s="T23">kurolǯa, </ts>
               <ts e="T25" id="Seg_246" n="e" s="T24">akoškat </ts>
               <ts e="T26" id="Seg_248" n="e" s="T25">kak </ts>
               <ts e="T27" id="Seg_250" n="e" s="T26">paqtädʼit </ts>
               <ts e="T28" id="Seg_252" n="e" s="T27">i </ts>
               <ts e="T29" id="Seg_254" n="e" s="T28">tokkuwanän. </ts>
               <ts e="T30" id="Seg_256" n="e" s="T29">Sət </ts>
               <ts e="T31" id="Seg_258" n="e" s="T30">klʼetkam </ts>
               <ts e="T32" id="Seg_260" n="e" s="T31">panannɨt </ts>
               <ts e="T33" id="Seg_262" n="e" s="T32">i </ts>
               <ts e="T34" id="Seg_264" n="e" s="T33">tokkuwatpɨkun. </ts>
               <ts e="T35" id="Seg_266" n="e" s="T34">Man </ts>
               <ts e="T36" id="Seg_268" n="e" s="T35">ügulgou, </ts>
               <ts e="T37" id="Seg_270" n="e" s="T36">ügulgou. </ts>
               <ts e="T38" id="Seg_272" n="e" s="T37">Qudnäj </ts>
               <ts e="T39" id="Seg_274" n="e" s="T38">tʼakgus, </ts>
               <ts e="T40" id="Seg_276" n="e" s="T39">xotʼ </ts>
               <ts e="T41" id="Seg_278" n="e" s="T40">tʼürak, </ts>
               <ts e="T42" id="Seg_280" n="e" s="T41">man </ts>
               <ts e="T43" id="Seg_282" n="e" s="T42">pelgattə </ts>
               <ts e="T44" id="Seg_284" n="e" s="T43">jewan. </ts>
               <ts e="T45" id="Seg_286" n="e" s="T44">Nasel </ts>
               <ts e="T46" id="Seg_288" n="e" s="T45">ügunnau </ts>
               <ts e="T47" id="Seg_290" n="e" s="T46">i </ts>
               <ts e="T48" id="Seg_292" n="e" s="T47">saːrau </ts>
               <ts e="T49" id="Seg_294" n="e" s="T48">kudugozʼe </ts>
               <ts e="T50" id="Seg_296" n="e" s="T49">i </ts>
               <ts e="T51" id="Seg_298" n="e" s="T50">peʒau. </ts>
               <ts e="T52" id="Seg_300" n="e" s="T51">I </ts>
               <ts e="T53" id="Seg_302" n="e" s="T52">ponä </ts>
               <ts e="T54" id="Seg_304" n="e" s="T53">nʼäpbodʼäu. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_305" s="T1">PVD_1964_ICutASheep_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_306" s="T4">PVD_1964_ICutASheep_nar.002 (001.002)</ta>
            <ta e="T12" id="Seg_307" s="T8">PVD_1964_ICutASheep_nar.003 (001.003)</ta>
            <ta e="T17" id="Seg_308" s="T12">PVD_1964_ICutASheep_nar.004 (001.004)</ta>
            <ta e="T21" id="Seg_309" s="T17">PVD_1964_ICutASheep_nar.005 (001.005)</ta>
            <ta e="T29" id="Seg_310" s="T21">PVD_1964_ICutASheep_nar.006 (001.006)</ta>
            <ta e="T34" id="Seg_311" s="T29">PVD_1964_ICutASheep_nar.007 (001.007)</ta>
            <ta e="T37" id="Seg_312" s="T34">PVD_1964_ICutASheep_nar.008 (001.008)</ta>
            <ta e="T44" id="Seg_313" s="T37">PVD_1964_ICutASheep_nar.009 (001.009)</ta>
            <ta e="T51" id="Seg_314" s="T44">PVD_1964_ICutASheep_nar.010 (001.010)</ta>
            <ta e="T54" id="Seg_315" s="T51">PVD_1964_ICutASheep_nar.011 (001.011)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_316" s="T1">ман ′д̂епкаɣӓн ′jезан.</ta>
            <ta e="T8" id="Seg_317" s="T4">у′гон ко′нӓрлай ′питшелʼе ′ӱбъран.</ta>
            <ta e="T12" id="Seg_318" s="T8">ок′kыр ко′нӓркам ′ма̄ттъ туlджу′коу̹.</ta>
            <ta e="T17" id="Seg_319" s="T12">пежы′коу̹ и ко′нӓрам ′понӓ ′kwатдъkоу̹.</ta>
            <ta e="T21" id="Seg_320" s="T17">туl′джау па′ранам, нʼӓп′бодʼӓу̹ ′ма̄ттъ.</ta>
            <ta e="T29" id="Seg_321" s="T21">тӓп как ку′роlджа, а′кошкат как пах(k)′тӓдʼит и ′токкуванӓн.</ta>
            <ta e="T34" id="Seg_322" s="T29">сът ′клʼеткам па′нанныт и токку′ватпыкун.</ta>
            <ta e="T37" id="Seg_323" s="T34">ман ′ӱгулгоу̹, ′ӱгулгоу̹.</ta>
            <ta e="T44" id="Seg_324" s="T37">kуд′нӓй тʼакгус, хотʼ ′тʼӱрак, ман пел′гаттъ jеwан.</ta>
            <ta e="T51" id="Seg_325" s="T44">на′сел ′ӱгуннау̹ и са̄рау̹ ′кудугозʼе и пе′жау̹.</ta>
            <ta e="T54" id="Seg_326" s="T51">и ′понӓ ′нʼӓпбо′дʼӓу̹.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_327" s="T1">man d̂epkaɣän jezan.</ta>
            <ta e="T8" id="Seg_328" s="T4">ugon konärlaj pitšelʼe übəran.</ta>
            <ta e="T12" id="Seg_329" s="T8">okqɨr konärkam maːttə tulǯukou̹.</ta>
            <ta e="T17" id="Seg_330" s="T12">peʒɨkou̹ i konäram ponä qwatdəqou̹.</ta>
            <ta e="T21" id="Seg_331" s="T17">tulǯau paranam, nʼäpbodʼäu̹ maːttə.</ta>
            <ta e="T29" id="Seg_332" s="T21">täp kak kurolǯa, akoškat kak pax(q)tädʼit i tokkuwanän.</ta>
            <ta e="T34" id="Seg_333" s="T29">sət klʼetkam panannɨt i tokkuwatpɨkun.</ta>
            <ta e="T37" id="Seg_334" s="T34">man ügulgou̹, ügulgou̹.</ta>
            <ta e="T44" id="Seg_335" s="T37">qudnäj tʼakgus, xotʼ tʼürak, man pelgattə jewan.</ta>
            <ta e="T51" id="Seg_336" s="T44">nasel ügunnau̹ i saːrau̹ kudugozʼe i peʒau̹.</ta>
            <ta e="T54" id="Seg_337" s="T51">i ponä nʼäpbodʼäu̹.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_338" s="T1">Man depkaɣän jezan. </ta>
            <ta e="T8" id="Seg_339" s="T4">Ugon konärlaj pičelʼe übəran. </ta>
            <ta e="T12" id="Seg_340" s="T8">Okqɨr konärkam maːttə tulǯukou. </ta>
            <ta e="T17" id="Seg_341" s="T12">Peʒɨkou i konäram ponä qwatdəqou. </ta>
            <ta e="T21" id="Seg_342" s="T17">Tulǯau paranam, nʼäpbodʼäu maːttə. </ta>
            <ta e="T29" id="Seg_343" s="T21">Täp kak kurolǯa, akoškat kak paqtädʼit i tokkuwanän. </ta>
            <ta e="T34" id="Seg_344" s="T29">Sət klʼetkam panannɨt i tokkuwatpɨkun. </ta>
            <ta e="T37" id="Seg_345" s="T34">Man ügulgou, ügulgou. </ta>
            <ta e="T44" id="Seg_346" s="T37">Qudnäj tʼakgus, xotʼ tʼürak, man pelgattə jewan. </ta>
            <ta e="T51" id="Seg_347" s="T44">Nasel ügunnau i saːrau kudugozʼe i peʒau. </ta>
            <ta e="T54" id="Seg_348" s="T51">I ponä nʼäpbodʼäu. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_349" s="T1">man</ta>
            <ta e="T3" id="Seg_350" s="T2">depka-ɣän</ta>
            <ta e="T4" id="Seg_351" s="T3">je-za-n</ta>
            <ta e="T5" id="Seg_352" s="T4">ugon</ta>
            <ta e="T6" id="Seg_353" s="T5">konär-la-j</ta>
            <ta e="T7" id="Seg_354" s="T6">piče-lʼe</ta>
            <ta e="T8" id="Seg_355" s="T7">übə-r-a-n</ta>
            <ta e="T9" id="Seg_356" s="T8">okqɨr</ta>
            <ta e="T10" id="Seg_357" s="T9">konär-ka-m</ta>
            <ta e="T11" id="Seg_358" s="T10">maːt-tə</ta>
            <ta e="T12" id="Seg_359" s="T11">tulǯu-ko-u</ta>
            <ta e="T13" id="Seg_360" s="T12">peʒɨ-ko-u</ta>
            <ta e="T14" id="Seg_361" s="T13">i</ta>
            <ta e="T15" id="Seg_362" s="T14">konär-a-m</ta>
            <ta e="T16" id="Seg_363" s="T15">ponä</ta>
            <ta e="T17" id="Seg_364" s="T16">qwat-də-qo-u</ta>
            <ta e="T18" id="Seg_365" s="T17">tulǯa-u</ta>
            <ta e="T19" id="Seg_366" s="T18">paran-a-m</ta>
            <ta e="T20" id="Seg_367" s="T19">nʼäpbo-dʼä-u</ta>
            <ta e="T21" id="Seg_368" s="T20">maːt-tə</ta>
            <ta e="T22" id="Seg_369" s="T21">täp</ta>
            <ta e="T23" id="Seg_370" s="T22">kak</ta>
            <ta e="T24" id="Seg_371" s="T23">kur-ol-ǯa</ta>
            <ta e="T25" id="Seg_372" s="T24">akoška-t</ta>
            <ta e="T26" id="Seg_373" s="T25">kak</ta>
            <ta e="T27" id="Seg_374" s="T26">paqtä-dʼi-t</ta>
            <ta e="T28" id="Seg_375" s="T27">i</ta>
            <ta e="T29" id="Seg_376" s="T28">tokkuwa-nä-n</ta>
            <ta e="T30" id="Seg_377" s="T29">sət</ta>
            <ta e="T31" id="Seg_378" s="T30">klʼetka-m</ta>
            <ta e="T32" id="Seg_379" s="T31">pan-a-nnɨ-t</ta>
            <ta e="T33" id="Seg_380" s="T32">i</ta>
            <ta e="T34" id="Seg_381" s="T33">tokkuwat-pɨ-ku-n</ta>
            <ta e="T35" id="Seg_382" s="T34">man</ta>
            <ta e="T36" id="Seg_383" s="T35">ü-gul-go-u</ta>
            <ta e="T37" id="Seg_384" s="T36">ü-gul-go-u</ta>
            <ta e="T38" id="Seg_385" s="T37">qud-näj</ta>
            <ta e="T39" id="Seg_386" s="T38">tʼakgu-s</ta>
            <ta e="T40" id="Seg_387" s="T39">xotʼ</ta>
            <ta e="T41" id="Seg_388" s="T40">tʼür-ak</ta>
            <ta e="T42" id="Seg_389" s="T41">man</ta>
            <ta e="T43" id="Seg_390" s="T42">pel-gattə</ta>
            <ta e="T44" id="Seg_391" s="T43">je-wa-n</ta>
            <ta e="T45" id="Seg_392" s="T44">nasel</ta>
            <ta e="T46" id="Seg_393" s="T45">ü-gun-na-u</ta>
            <ta e="T47" id="Seg_394" s="T46">i</ta>
            <ta e="T48" id="Seg_395" s="T47">saːra-u</ta>
            <ta e="T49" id="Seg_396" s="T48">kudugo-zʼe</ta>
            <ta e="T50" id="Seg_397" s="T49">i</ta>
            <ta e="T51" id="Seg_398" s="T50">peʒa-u</ta>
            <ta e="T52" id="Seg_399" s="T51">i</ta>
            <ta e="T53" id="Seg_400" s="T52">ponä</ta>
            <ta e="T54" id="Seg_401" s="T53">nʼäpbo-dʼä-u</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_402" s="T1">man</ta>
            <ta e="T3" id="Seg_403" s="T2">dʼepka-qɨn</ta>
            <ta e="T4" id="Seg_404" s="T3">eː-sɨ-ŋ</ta>
            <ta e="T5" id="Seg_405" s="T4">ugon</ta>
            <ta e="T6" id="Seg_406" s="T5">qoner-la-lʼ</ta>
            <ta e="T7" id="Seg_407" s="T6">piʒu-le</ta>
            <ta e="T8" id="Seg_408" s="T7">übɨ-r-ɨ-ŋ</ta>
            <ta e="T9" id="Seg_409" s="T8">okkɨr</ta>
            <ta e="T10" id="Seg_410" s="T9">qoner-ka-m</ta>
            <ta e="T11" id="Seg_411" s="T10">maːt-ntə</ta>
            <ta e="T12" id="Seg_412" s="T11">tulǯu-ku-w</ta>
            <ta e="T13" id="Seg_413" s="T12">piʒu-ku-w</ta>
            <ta e="T14" id="Seg_414" s="T13">i</ta>
            <ta e="T15" id="Seg_415" s="T14">qoner-ɨ-m</ta>
            <ta e="T16" id="Seg_416" s="T15">poːne</ta>
            <ta e="T17" id="Seg_417" s="T16">qwat-ntɨ-ku-w</ta>
            <ta e="T18" id="Seg_418" s="T17">tulǯu-w</ta>
            <ta e="T19" id="Seg_419" s="T18">paran-ɨ-m</ta>
            <ta e="T20" id="Seg_420" s="T19">nʼampɛː-dʼi-w</ta>
            <ta e="T21" id="Seg_421" s="T20">maːt-ntə</ta>
            <ta e="T22" id="Seg_422" s="T21">täp</ta>
            <ta e="T23" id="Seg_423" s="T22">kak</ta>
            <ta e="T24" id="Seg_424" s="T23">kur-ol-ntɨ</ta>
            <ta e="T25" id="Seg_425" s="T24">akoška-ntə</ta>
            <ta e="T26" id="Seg_426" s="T25">kak</ta>
            <ta e="T27" id="Seg_427" s="T26">paktɨ-dʼi-t</ta>
            <ta e="T28" id="Seg_428" s="T27">i</ta>
            <ta e="T29" id="Seg_429" s="T28">toqwat-nɨ-n</ta>
            <ta e="T30" id="Seg_430" s="T29">sədə</ta>
            <ta e="T31" id="Seg_431" s="T30">klʼetka-m</ta>
            <ta e="T32" id="Seg_432" s="T31">pan-ɨ-ntɨ-t</ta>
            <ta e="T33" id="Seg_433" s="T32">i</ta>
            <ta e="T34" id="Seg_434" s="T33">toqwat-mbɨ-ku-n</ta>
            <ta e="T35" id="Seg_435" s="T34">man</ta>
            <ta e="T36" id="Seg_436" s="T35">ü-qɨl-ku-w</ta>
            <ta e="T37" id="Seg_437" s="T36">ü-qɨl-ku-w</ta>
            <ta e="T38" id="Seg_438" s="T37">kud-näj</ta>
            <ta e="T39" id="Seg_439" s="T38">tʼäkku-sɨ</ta>
            <ta e="T40" id="Seg_440" s="T39">xotʼ</ta>
            <ta e="T41" id="Seg_441" s="T40">tʼüru-kɨ</ta>
            <ta e="T42" id="Seg_442" s="T41">man</ta>
            <ta e="T43" id="Seg_443" s="T42">*pel-kattə</ta>
            <ta e="T44" id="Seg_444" s="T43">eː-nɨ-ŋ</ta>
            <ta e="T45" id="Seg_445" s="T44">naseːl</ta>
            <ta e="T46" id="Seg_446" s="T45">ü-qɨl-nɨ-w</ta>
            <ta e="T47" id="Seg_447" s="T46">i</ta>
            <ta e="T48" id="Seg_448" s="T47">saːrə-w</ta>
            <ta e="T49" id="Seg_449" s="T48">quːdəgo-se</ta>
            <ta e="T50" id="Seg_450" s="T49">i</ta>
            <ta e="T51" id="Seg_451" s="T50">piʒu-w</ta>
            <ta e="T52" id="Seg_452" s="T51">i</ta>
            <ta e="T53" id="Seg_453" s="T52">poːne</ta>
            <ta e="T54" id="Seg_454" s="T53">nʼampɛː-dʼi-w</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_455" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_456" s="T2">girl-LOC</ta>
            <ta e="T4" id="Seg_457" s="T3">be-PST-1SG.S</ta>
            <ta e="T5" id="Seg_458" s="T4">earlier</ta>
            <ta e="T6" id="Seg_459" s="T5">sheep-PL-ADJZ</ta>
            <ta e="T7" id="Seg_460" s="T6">shear-CVB</ta>
            <ta e="T8" id="Seg_461" s="T7">begin-DRV-EP-1SG.S</ta>
            <ta e="T9" id="Seg_462" s="T8">one</ta>
            <ta e="T10" id="Seg_463" s="T9">sheep-DIM-ACC</ta>
            <ta e="T11" id="Seg_464" s="T10">house-ILL</ta>
            <ta e="T12" id="Seg_465" s="T11">bring.in-HAB-1SG.O</ta>
            <ta e="T13" id="Seg_466" s="T12">shear-HAB-1SG.O</ta>
            <ta e="T14" id="Seg_467" s="T13">and</ta>
            <ta e="T15" id="Seg_468" s="T14">sheep-EP-ACC</ta>
            <ta e="T16" id="Seg_469" s="T15">outward(s)</ta>
            <ta e="T17" id="Seg_470" s="T16">catch-IPFV-HAB-1SG.O</ta>
            <ta e="T18" id="Seg_471" s="T17">bring.in-1SG.O</ta>
            <ta e="T19" id="Seg_472" s="T18">ram-EP-ACC</ta>
            <ta e="T20" id="Seg_473" s="T19">push-DRV-1SG.O</ta>
            <ta e="T21" id="Seg_474" s="T20">house-ILL</ta>
            <ta e="T22" id="Seg_475" s="T21">(s)he.[NOM]</ta>
            <ta e="T23" id="Seg_476" s="T22">suddenly</ta>
            <ta e="T24" id="Seg_477" s="T23">go-MOM-INFER.[3SG.S]</ta>
            <ta e="T25" id="Seg_478" s="T24">window-ILL</ta>
            <ta e="T26" id="Seg_479" s="T25">suddenly</ta>
            <ta e="T27" id="Seg_480" s="T26">jump-DRV-3SG.O</ta>
            <ta e="T28" id="Seg_481" s="T27">and</ta>
            <ta e="T29" id="Seg_482" s="T28">stick.in-CO-3SG.S</ta>
            <ta e="T30" id="Seg_483" s="T29">two</ta>
            <ta e="T31" id="Seg_484" s="T30">cage-ACC</ta>
            <ta e="T32" id="Seg_485" s="T31">shatter-EP-INFER-3SG.O</ta>
            <ta e="T33" id="Seg_486" s="T32">and</ta>
            <ta e="T34" id="Seg_487" s="T33">stick.in-RES-HAB-3SG.S</ta>
            <ta e="T35" id="Seg_488" s="T34">I.NOM</ta>
            <ta e="T36" id="Seg_489" s="T35">pull-DRV-HAB-1SG.O</ta>
            <ta e="T37" id="Seg_490" s="T36">pull-DRV-HAB-1SG.O</ta>
            <ta e="T38" id="Seg_491" s="T37">who-EMPH</ta>
            <ta e="T39" id="Seg_492" s="T38">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T40" id="Seg_493" s="T39">for.example</ta>
            <ta e="T41" id="Seg_494" s="T40">cry-IMP.2SG.S</ta>
            <ta e="T42" id="Seg_495" s="T41">I.NOM</ta>
            <ta e="T43" id="Seg_496" s="T42">friend-CAR</ta>
            <ta e="T44" id="Seg_497" s="T43">be-CO-1SG.S</ta>
            <ta e="T45" id="Seg_498" s="T44">hardly</ta>
            <ta e="T46" id="Seg_499" s="T45">pull-DRV-CO-1SG.O</ta>
            <ta e="T47" id="Seg_500" s="T46">and</ta>
            <ta e="T48" id="Seg_501" s="T47">bind-1SG.O</ta>
            <ta e="T49" id="Seg_502" s="T48">pull.strap-INSTR</ta>
            <ta e="T50" id="Seg_503" s="T49">and</ta>
            <ta e="T51" id="Seg_504" s="T50">shear-1SG.O</ta>
            <ta e="T52" id="Seg_505" s="T51">and</ta>
            <ta e="T53" id="Seg_506" s="T52">outward(s)</ta>
            <ta e="T54" id="Seg_507" s="T53">push-DRV-1SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_508" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_509" s="T2">девка-LOC</ta>
            <ta e="T4" id="Seg_510" s="T3">быть-PST-1SG.S</ta>
            <ta e="T5" id="Seg_511" s="T4">раньше</ta>
            <ta e="T6" id="Seg_512" s="T5">овца-PL-ADJZ</ta>
            <ta e="T7" id="Seg_513" s="T6">остричь-CVB</ta>
            <ta e="T8" id="Seg_514" s="T7">начать-DRV-EP-1SG.S</ta>
            <ta e="T9" id="Seg_515" s="T8">один</ta>
            <ta e="T10" id="Seg_516" s="T9">овца-DIM-ACC</ta>
            <ta e="T11" id="Seg_517" s="T10">дом-ILL</ta>
            <ta e="T12" id="Seg_518" s="T11">внести-HAB-1SG.O</ta>
            <ta e="T13" id="Seg_519" s="T12">остричь-HAB-1SG.O</ta>
            <ta e="T14" id="Seg_520" s="T13">и</ta>
            <ta e="T15" id="Seg_521" s="T14">овца-EP-ACC</ta>
            <ta e="T16" id="Seg_522" s="T15">наружу</ta>
            <ta e="T17" id="Seg_523" s="T16">поймать-IPFV-HAB-1SG.O</ta>
            <ta e="T18" id="Seg_524" s="T17">внести-1SG.O</ta>
            <ta e="T19" id="Seg_525" s="T18">баран-EP-ACC</ta>
            <ta e="T20" id="Seg_526" s="T19">вытолкнуть-DRV-1SG.O</ta>
            <ta e="T21" id="Seg_527" s="T20">дом-ILL</ta>
            <ta e="T22" id="Seg_528" s="T21">он(а).[NOM]</ta>
            <ta e="T23" id="Seg_529" s="T22">как</ta>
            <ta e="T24" id="Seg_530" s="T23">ходить-MOM-INFER.[3SG.S]</ta>
            <ta e="T25" id="Seg_531" s="T24">окно-ILL</ta>
            <ta e="T26" id="Seg_532" s="T25">как</ta>
            <ta e="T27" id="Seg_533" s="T26">прыгнуть-DRV-3SG.O</ta>
            <ta e="T28" id="Seg_534" s="T27">и</ta>
            <ta e="T29" id="Seg_535" s="T28">увязнуть-CO-3SG.S</ta>
            <ta e="T30" id="Seg_536" s="T29">два</ta>
            <ta e="T31" id="Seg_537" s="T30">клетка-ACC</ta>
            <ta e="T32" id="Seg_538" s="T31">расколоть-EP-INFER-3SG.O</ta>
            <ta e="T33" id="Seg_539" s="T32">и</ta>
            <ta e="T34" id="Seg_540" s="T33">увязнуть-RES-HAB-3SG.S</ta>
            <ta e="T35" id="Seg_541" s="T34">я.NOM</ta>
            <ta e="T36" id="Seg_542" s="T35">тащить-DRV-HAB-1SG.O</ta>
            <ta e="T37" id="Seg_543" s="T36">тащить-DRV-HAB-1SG.O</ta>
            <ta e="T38" id="Seg_544" s="T37">кто-EMPH</ta>
            <ta e="T39" id="Seg_545" s="T38">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T40" id="Seg_546" s="T39">хоть</ta>
            <ta e="T41" id="Seg_547" s="T40">плакать-IMP.2SG.S</ta>
            <ta e="T42" id="Seg_548" s="T41">я.NOM</ta>
            <ta e="T43" id="Seg_549" s="T42">друг-CAR</ta>
            <ta e="T44" id="Seg_550" s="T43">быть-CO-1SG.S</ta>
            <ta e="T45" id="Seg_551" s="T44">насилу</ta>
            <ta e="T46" id="Seg_552" s="T45">тащить-DRV-CO-1SG.O</ta>
            <ta e="T47" id="Seg_553" s="T46">и</ta>
            <ta e="T48" id="Seg_554" s="T47">привязать-1SG.O</ta>
            <ta e="T49" id="Seg_555" s="T48">ремень-INSTR</ta>
            <ta e="T50" id="Seg_556" s="T49">и</ta>
            <ta e="T51" id="Seg_557" s="T50">остричь-1SG.O</ta>
            <ta e="T52" id="Seg_558" s="T51">и</ta>
            <ta e="T53" id="Seg_559" s="T52">наружу</ta>
            <ta e="T54" id="Seg_560" s="T53">вытолкнуть-DRV-1SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_561" s="T1">pers</ta>
            <ta e="T3" id="Seg_562" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_563" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_564" s="T4">adv</ta>
            <ta e="T6" id="Seg_565" s="T5">n-n:num-n&gt;adj</ta>
            <ta e="T7" id="Seg_566" s="T6">v-v&gt;adv</ta>
            <ta e="T8" id="Seg_567" s="T7">v-v&gt;v-n:ins-v:pn</ta>
            <ta e="T9" id="Seg_568" s="T8">num</ta>
            <ta e="T10" id="Seg_569" s="T9">n-n&gt;n-n:case</ta>
            <ta e="T11" id="Seg_570" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_571" s="T11">v-v&gt;v-v:pn</ta>
            <ta e="T13" id="Seg_572" s="T12">v-v&gt;v-v:pn</ta>
            <ta e="T14" id="Seg_573" s="T13">conj</ta>
            <ta e="T15" id="Seg_574" s="T14">n-n:ins-n:case</ta>
            <ta e="T16" id="Seg_575" s="T15">adv</ta>
            <ta e="T17" id="Seg_576" s="T16">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T18" id="Seg_577" s="T17">v-v:pn</ta>
            <ta e="T19" id="Seg_578" s="T18">n-n:ins-n:case</ta>
            <ta e="T20" id="Seg_579" s="T19">v-v&gt;v-v:pn</ta>
            <ta e="T21" id="Seg_580" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_581" s="T21">pers.[n:case]</ta>
            <ta e="T23" id="Seg_582" s="T22">adv</ta>
            <ta e="T24" id="Seg_583" s="T23">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T25" id="Seg_584" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_585" s="T25">adv</ta>
            <ta e="T27" id="Seg_586" s="T26">v-v&gt;v-v:pn</ta>
            <ta e="T28" id="Seg_587" s="T27">conj</ta>
            <ta e="T29" id="Seg_588" s="T28">v-v:ins-v:pn</ta>
            <ta e="T30" id="Seg_589" s="T29">num</ta>
            <ta e="T31" id="Seg_590" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_591" s="T31">v-v:ins-v:mood-v:pn</ta>
            <ta e="T33" id="Seg_592" s="T32">conj</ta>
            <ta e="T34" id="Seg_593" s="T33">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T35" id="Seg_594" s="T34">pers</ta>
            <ta e="T36" id="Seg_595" s="T35">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T37" id="Seg_596" s="T36">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T38" id="Seg_597" s="T37">interrog-clit</ta>
            <ta e="T39" id="Seg_598" s="T38">v-v:tense.[v:pn]</ta>
            <ta e="T40" id="Seg_599" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_600" s="T40">v-v:mood.pn</ta>
            <ta e="T42" id="Seg_601" s="T41">pers</ta>
            <ta e="T43" id="Seg_602" s="T42">n-n&gt;n</ta>
            <ta e="T44" id="Seg_603" s="T43">v-v:ins-v:pn</ta>
            <ta e="T45" id="Seg_604" s="T44">adv</ta>
            <ta e="T46" id="Seg_605" s="T45">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T47" id="Seg_606" s="T46">conj</ta>
            <ta e="T48" id="Seg_607" s="T47">v-v:pn</ta>
            <ta e="T49" id="Seg_608" s="T48">n-n:case</ta>
            <ta e="T50" id="Seg_609" s="T49">conj</ta>
            <ta e="T51" id="Seg_610" s="T50">v-v:pn</ta>
            <ta e="T52" id="Seg_611" s="T51">conj</ta>
            <ta e="T53" id="Seg_612" s="T52">adv</ta>
            <ta e="T54" id="Seg_613" s="T53">v-v&gt;v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_614" s="T1">pers</ta>
            <ta e="T3" id="Seg_615" s="T2">n</ta>
            <ta e="T4" id="Seg_616" s="T3">v</ta>
            <ta e="T5" id="Seg_617" s="T4">adv</ta>
            <ta e="T6" id="Seg_618" s="T5">adj</ta>
            <ta e="T7" id="Seg_619" s="T6">adv</ta>
            <ta e="T8" id="Seg_620" s="T7">v</ta>
            <ta e="T9" id="Seg_621" s="T8">num</ta>
            <ta e="T10" id="Seg_622" s="T9">n</ta>
            <ta e="T11" id="Seg_623" s="T10">n</ta>
            <ta e="T12" id="Seg_624" s="T11">v</ta>
            <ta e="T13" id="Seg_625" s="T12">v</ta>
            <ta e="T14" id="Seg_626" s="T13">conj</ta>
            <ta e="T15" id="Seg_627" s="T14">n</ta>
            <ta e="T16" id="Seg_628" s="T15">adv</ta>
            <ta e="T17" id="Seg_629" s="T16">v</ta>
            <ta e="T18" id="Seg_630" s="T17">v</ta>
            <ta e="T19" id="Seg_631" s="T18">n</ta>
            <ta e="T20" id="Seg_632" s="T19">v</ta>
            <ta e="T21" id="Seg_633" s="T20">n</ta>
            <ta e="T22" id="Seg_634" s="T21">pers</ta>
            <ta e="T23" id="Seg_635" s="T22">adv</ta>
            <ta e="T24" id="Seg_636" s="T23">v</ta>
            <ta e="T25" id="Seg_637" s="T24">n</ta>
            <ta e="T26" id="Seg_638" s="T25">adv</ta>
            <ta e="T27" id="Seg_639" s="T26">v</ta>
            <ta e="T28" id="Seg_640" s="T27">conj</ta>
            <ta e="T29" id="Seg_641" s="T28">v</ta>
            <ta e="T30" id="Seg_642" s="T29">num</ta>
            <ta e="T31" id="Seg_643" s="T30">n</ta>
            <ta e="T32" id="Seg_644" s="T31">v</ta>
            <ta e="T33" id="Seg_645" s="T32">conj</ta>
            <ta e="T34" id="Seg_646" s="T33">v</ta>
            <ta e="T35" id="Seg_647" s="T34">pers</ta>
            <ta e="T36" id="Seg_648" s="T35">v</ta>
            <ta e="T37" id="Seg_649" s="T36">v</ta>
            <ta e="T38" id="Seg_650" s="T37">pro</ta>
            <ta e="T39" id="Seg_651" s="T38">v</ta>
            <ta e="T40" id="Seg_652" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_653" s="T40">v</ta>
            <ta e="T42" id="Seg_654" s="T41">pers</ta>
            <ta e="T43" id="Seg_655" s="T42">n</ta>
            <ta e="T44" id="Seg_656" s="T43">v</ta>
            <ta e="T45" id="Seg_657" s="T44">adv</ta>
            <ta e="T46" id="Seg_658" s="T45">v</ta>
            <ta e="T47" id="Seg_659" s="T46">conj</ta>
            <ta e="T48" id="Seg_660" s="T47">v</ta>
            <ta e="T49" id="Seg_661" s="T48">n</ta>
            <ta e="T50" id="Seg_662" s="T49">conj</ta>
            <ta e="T51" id="Seg_663" s="T50">v</ta>
            <ta e="T52" id="Seg_664" s="T51">conj</ta>
            <ta e="T53" id="Seg_665" s="T52">adv</ta>
            <ta e="T54" id="Seg_666" s="T53">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_667" s="T1">pro.h:Th</ta>
            <ta e="T5" id="Seg_668" s="T4">adv:Time</ta>
            <ta e="T6" id="Seg_669" s="T5">np:P</ta>
            <ta e="T8" id="Seg_670" s="T7">0.1.h:A</ta>
            <ta e="T10" id="Seg_671" s="T9">np:Th</ta>
            <ta e="T11" id="Seg_672" s="T10">np:G</ta>
            <ta e="T12" id="Seg_673" s="T11">0.1.h:A</ta>
            <ta e="T13" id="Seg_674" s="T12">0.1.h:A 0.3:P</ta>
            <ta e="T15" id="Seg_675" s="T14">np:Th</ta>
            <ta e="T16" id="Seg_676" s="T15">adv:G</ta>
            <ta e="T17" id="Seg_677" s="T16">0.1.h:A</ta>
            <ta e="T18" id="Seg_678" s="T17">0.1.h:A</ta>
            <ta e="T19" id="Seg_679" s="T18">np:Th</ta>
            <ta e="T20" id="Seg_680" s="T19">0.1.h:A 0.3:Th</ta>
            <ta e="T21" id="Seg_681" s="T20">np:G</ta>
            <ta e="T22" id="Seg_682" s="T21">pro:A</ta>
            <ta e="T25" id="Seg_683" s="T24">np:G</ta>
            <ta e="T27" id="Seg_684" s="T26">0.3:A</ta>
            <ta e="T29" id="Seg_685" s="T28">0.3:P</ta>
            <ta e="T31" id="Seg_686" s="T30">np:P</ta>
            <ta e="T32" id="Seg_687" s="T31">0.3:A</ta>
            <ta e="T34" id="Seg_688" s="T33">0.3:P</ta>
            <ta e="T35" id="Seg_689" s="T34">pro.h:A</ta>
            <ta e="T36" id="Seg_690" s="T35">0.3:Th</ta>
            <ta e="T37" id="Seg_691" s="T36">0.1.h:A 0.3:Th</ta>
            <ta e="T38" id="Seg_692" s="T37">pro.h:Th</ta>
            <ta e="T41" id="Seg_693" s="T40">0.2.h:A</ta>
            <ta e="T42" id="Seg_694" s="T41">pro.h:Th</ta>
            <ta e="T46" id="Seg_695" s="T45">0.1.h:A 0.3:Th</ta>
            <ta e="T48" id="Seg_696" s="T47">0.1.h:A 0.3:Th</ta>
            <ta e="T49" id="Seg_697" s="T48">np:Ins</ta>
            <ta e="T51" id="Seg_698" s="T50">0.1.h:A 0.3:P</ta>
            <ta e="T53" id="Seg_699" s="T52">adv:G</ta>
            <ta e="T54" id="Seg_700" s="T53">0.1.h:A 0.3:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_701" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_702" s="T2">n:pred</ta>
            <ta e="T4" id="Seg_703" s="T3">cop</ta>
            <ta e="T8" id="Seg_704" s="T7">0.1.h:S v:pred</ta>
            <ta e="T10" id="Seg_705" s="T9">np:O</ta>
            <ta e="T12" id="Seg_706" s="T11">0.1.h:S v:pred</ta>
            <ta e="T13" id="Seg_707" s="T12">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T15" id="Seg_708" s="T14">np:O</ta>
            <ta e="T17" id="Seg_709" s="T16">0.1.h:S v:pred</ta>
            <ta e="T18" id="Seg_710" s="T17">0.1.h:S v:pred</ta>
            <ta e="T19" id="Seg_711" s="T18">np:O</ta>
            <ta e="T20" id="Seg_712" s="T19">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T22" id="Seg_713" s="T21">pro:S</ta>
            <ta e="T24" id="Seg_714" s="T23">v:pred</ta>
            <ta e="T27" id="Seg_715" s="T26">0.3:S v:pred</ta>
            <ta e="T29" id="Seg_716" s="T28">0.3:S v:pred</ta>
            <ta e="T31" id="Seg_717" s="T30">np:O</ta>
            <ta e="T32" id="Seg_718" s="T31">0.3:S v:pred</ta>
            <ta e="T34" id="Seg_719" s="T33">0.3:S v:pred</ta>
            <ta e="T35" id="Seg_720" s="T34">pro.h:S</ta>
            <ta e="T36" id="Seg_721" s="T35">v:pred 0.3:O</ta>
            <ta e="T37" id="Seg_722" s="T36">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T38" id="Seg_723" s="T37">pro.h:S</ta>
            <ta e="T39" id="Seg_724" s="T38">v:pred</ta>
            <ta e="T41" id="Seg_725" s="T40">0.2.h:S v:pred</ta>
            <ta e="T42" id="Seg_726" s="T41">pro.h:S</ta>
            <ta e="T43" id="Seg_727" s="T42">n:pred</ta>
            <ta e="T44" id="Seg_728" s="T43">cop</ta>
            <ta e="T46" id="Seg_729" s="T45">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T48" id="Seg_730" s="T47">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T51" id="Seg_731" s="T50">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T54" id="Seg_732" s="T53">0.1.h:S v:pred 0.3:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_733" s="T2">RUS:core</ta>
            <ta e="T14" id="Seg_734" s="T13">RUS:gram</ta>
            <ta e="T19" id="Seg_735" s="T18">RUS:cult</ta>
            <ta e="T23" id="Seg_736" s="T22">RUS:disc</ta>
            <ta e="T25" id="Seg_737" s="T24">RUS:cult</ta>
            <ta e="T26" id="Seg_738" s="T25">RUS:disc</ta>
            <ta e="T28" id="Seg_739" s="T27">RUS:gram</ta>
            <ta e="T31" id="Seg_740" s="T30">RUS:cult</ta>
            <ta e="T33" id="Seg_741" s="T32">RUS:gram</ta>
            <ta e="T40" id="Seg_742" s="T39">RUS:disc</ta>
            <ta e="T45" id="Seg_743" s="T44">RUS:core</ta>
            <ta e="T47" id="Seg_744" s="T46">RUS:gram</ta>
            <ta e="T50" id="Seg_745" s="T49">RUS:gram</ta>
            <ta e="T52" id="Seg_746" s="T51">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_747" s="T1">I was a young girl.</ta>
            <ta e="T8" id="Seg_748" s="T4">Once I began to cut sheep.</ta>
            <ta e="T12" id="Seg_749" s="T8">I took them one by one into the house.</ta>
            <ta e="T17" id="Seg_750" s="T12">I cut them and pulled them outside.</ta>
            <ta e="T21" id="Seg_751" s="T17">I dragged a ram, and pushed it into the house. </ta>
            <ta e="T29" id="Seg_752" s="T21">And he broke into run, jumped onto the window and got stuck.</ta>
            <ta e="T34" id="Seg_753" s="T29">He broke two cages and got stuck.</ta>
            <ta e="T37" id="Seg_754" s="T34">I pulled and pulled.</ta>
            <ta e="T44" id="Seg_755" s="T37">And there was noone [to help], it could make me cry, I was alone.</ta>
            <ta e="T51" id="Seg_756" s="T44">I hardly managed to pull him out, and to bind him and to cut him.</ta>
            <ta e="T54" id="Seg_757" s="T51">And [then] I pushed it outside.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_758" s="T1">Ich war ein junges Mädchen.</ta>
            <ta e="T8" id="Seg_759" s="T4">Einmal begann ich Schafe zu scheren.</ta>
            <ta e="T12" id="Seg_760" s="T8">Ich brachte sie eins nach dem anderen ins Haus.</ta>
            <ta e="T17" id="Seg_761" s="T12">Ich schor sie und trieb sie nach draußen.</ta>
            <ta e="T21" id="Seg_762" s="T17">Ich zerrte einen Bock und schob ihn ins Haus.</ta>
            <ta e="T29" id="Seg_763" s="T21">Und er rannte plötzlich los, sprang aufs Fenster und steckte fest.</ta>
            <ta e="T34" id="Seg_764" s="T29">Er zerbrach zwei Käfige und steckte fest.</ta>
            <ta e="T37" id="Seg_765" s="T34">Ich zog und zog.</ta>
            <ta e="T44" id="Seg_766" s="T37">Und es gab niemanden [um zu helfen], es war zum Heulen, ich war alleine.</ta>
            <ta e="T51" id="Seg_767" s="T44">Ich schaffte es kaum ihn herauszuziehen und ihn mit Seilen festzubinden und ihn zu scheren.</ta>
            <ta e="T54" id="Seg_768" s="T51">Und [dann] schob ich ihn nach draußen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_769" s="T1">Я еще в девках была.</ta>
            <ta e="T8" id="Seg_770" s="T4">Однажды я овечек стричь начала.</ta>
            <ta e="T12" id="Seg_771" s="T8">По одной овечке в избу заношу.</ta>
            <ta e="T17" id="Seg_772" s="T12">Остригу и овечку на улицу тащу.</ta>
            <ta e="T21" id="Seg_773" s="T17">Притащила барана, втолкнула в избу.</ta>
            <ta e="T29" id="Seg_774" s="T21">Он как побежал, на окошко как скакнул и застрял.</ta>
            <ta e="T34" id="Seg_775" s="T29">Две клетки сломал и застрял.</ta>
            <ta e="T37" id="Seg_776" s="T34">Я тяну, тяну.</ta>
            <ta e="T44" id="Seg_777" s="T37">И никого не было, хоть плачь, я одна.</ta>
            <ta e="T51" id="Seg_778" s="T44">Насилу вытащила, привязала веревочкой и обстригла.</ta>
            <ta e="T54" id="Seg_779" s="T51">И на улицу вытолкала.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_780" s="T1">я еще девкой была</ta>
            <ta e="T8" id="Seg_781" s="T4">раньше овечек стричь начал(а)</ta>
            <ta e="T12" id="Seg_782" s="T8">по одной овечке в избу заношу</ta>
            <ta e="T17" id="Seg_783" s="T12">остригу и овечку на улицу тащу</ta>
            <ta e="T21" id="Seg_784" s="T17">притащила барана пустила в избу</ta>
            <ta e="T29" id="Seg_785" s="T21">он как побежал на окошко как скакнул засел</ta>
            <ta e="T34" id="Seg_786" s="T29">две клетки сломал и засел (застрял)</ta>
            <ta e="T37" id="Seg_787" s="T34">я тяну тяну</ta>
            <ta e="T44" id="Seg_788" s="T37">и никого не было хоть плачь я одна</ta>
            <ta e="T51" id="Seg_789" s="T44">насилу вытащила завязала веревочкой (на веревочку) и обстригла</ta>
            <ta e="T54" id="Seg_790" s="T51">и на улицу отпустила</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T29" id="Seg_791" s="T21">[KuAI:] Variant: 'paxtädʼit'. [BrM:] Objective conjugation in 'paqtädʼit'?</ta>
            <ta e="T37" id="Seg_792" s="T34">[BrM:] Tentative analysis of 'ügulgou'.</ta>
            <ta e="T51" id="Seg_793" s="T44">[BrM:] Tentative analysis of 'ügunnau'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
