<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_IHadNoHouse_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_IHadNoHouse_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">107</ud-information>
            <ud-information attribute-name="# HIAT:w">82</ud-information>
            <ud-information attribute-name="# e">82</ud-information>
            <ud-information attribute-name="# HIAT:u">18</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T83" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Mannan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">maːdmo</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">tʼakkus</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Man</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">elɨzan</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">qullannan</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">Patom</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">megunɨ</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">maːt</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_36" n="HIAT:ip">(</nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">maːdɨm</ts>
                  <nts id="Seg_39" n="HIAT:ip">)</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">mewattɨ</ts>
                  <nts id="Seg_43" n="HIAT:ip">.</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_46" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">A</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">man</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">tʼüan</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">na</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">mattə</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_64" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">A</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">na</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">tʼedaɣɨn</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">takaja</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">čam</ts>
                  <nts id="Seg_79" n="HIAT:ip">.</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_82" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">Man</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_87" n="HIAT:w" s="T23">tʼettɨ</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_90" n="HIAT:w" s="T24">dʼel</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_93" n="HIAT:w" s="T25">ubirajčan</ts>
                  <nts id="Seg_94" n="HIAT:ip">.</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_97" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_99" n="HIAT:w" s="T26">Qutder</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_102" n="HIAT:w" s="T27">qula</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_105" n="HIAT:w" s="T28">illəpbattɨ</ts>
                  <nts id="Seg_106" n="HIAT:ip">.</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_109" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_111" n="HIAT:w" s="T29">A</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_114" n="HIAT:w" s="T30">man</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_117" n="HIAT:w" s="T31">iːw</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_120" n="HIAT:w" s="T32">tolʼko</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_123" n="HIAT:w" s="T33">ödi</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_126" n="HIAT:w" s="T34">tuddokun</ts>
                  <nts id="Seg_127" n="HIAT:ip">.</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_130" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_132" n="HIAT:w" s="T35">Man</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_135" n="HIAT:w" s="T36">müzulǯuguʒukan</ts>
                  <nts id="Seg_136" n="HIAT:ip">,</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_139" n="HIAT:w" s="T37">müzulǯuguʒukan</ts>
                  <nts id="Seg_140" n="HIAT:ip">.</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_143" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_145" n="HIAT:w" s="T38">Patom</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_148" n="HIAT:w" s="T39">manan</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_151" n="HIAT:w" s="T40">soːdʼiga</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_154" n="HIAT:w" s="T41">azun</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_157" n="HIAT:w" s="T42">maːdən</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_160" n="HIAT:w" s="T43">sʼütʼdʼe</ts>
                  <nts id="Seg_161" n="HIAT:ip">.</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_164" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_166" n="HIAT:w" s="T44">A</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_169" n="HIAT:w" s="T45">man</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_172" n="HIAT:w" s="T46">as</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_175" n="HIAT:w" s="T47">sorau</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_178" n="HIAT:w" s="T48">awan</ts>
                  <nts id="Seg_179" n="HIAT:ip">,</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_182" n="HIAT:w" s="T49">štobɨ</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_185" n="HIAT:w" s="T50">soːdʼigan</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_188" n="HIAT:w" s="T51">jegi</ts>
                  <nts id="Seg_189" n="HIAT:ip">.</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_192" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_194" n="HIAT:w" s="T52">A</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_197" n="HIAT:w" s="T53">teperʼ</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_200" n="HIAT:w" s="T54">aj</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_203" n="HIAT:w" s="T55">maːdmu</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_206" n="HIAT:w" s="T56">tʼäkku</ts>
                  <nts id="Seg_207" n="HIAT:ip">.</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_210" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_212" n="HIAT:w" s="T57">Aj</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_215" n="HIAT:w" s="T58">qullannan</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_218" n="HIAT:w" s="T59">warkan</ts>
                  <nts id="Seg_219" n="HIAT:ip">.</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_222" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_224" n="HIAT:w" s="T60">Tɨtdɨn</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_227" n="HIAT:w" s="T61">soːn</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_230" n="HIAT:w" s="T62">warkan</ts>
                  <nts id="Seg_231" n="HIAT:ip">.</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_234" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_236" n="HIAT:w" s="T63">Neuɣum</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_239" n="HIAT:w" s="T64">soːdiga</ts>
                  <nts id="Seg_240" n="HIAT:ip">,</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_243" n="HIAT:w" s="T65">mekka</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_246" n="HIAT:w" s="T66">as</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_249" n="HIAT:w" s="T67">qwɛdɨbukkɨn</ts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_253" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_255" n="HIAT:w" s="T68">Man</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_258" n="HIAT:w" s="T69">oːnen</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_261" n="HIAT:w" s="T70">qaim</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_264" n="HIAT:w" s="T71">kɨgelam</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_267" n="HIAT:w" s="T72">naugam</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_270" n="HIAT:w" s="T73">melʼe</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_273" n="HIAT:w" s="T74">tadərəku</ts>
                  <nts id="Seg_274" n="HIAT:ip">.</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_277" n="HIAT:u" s="T75">
                  <nts id="Seg_278" n="HIAT:ip">“</nts>
                  <ts e="T76" id="Seg_280" n="HIAT:w" s="T75">Maːdɨm</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_283" n="HIAT:w" s="T76">kaʒnɨj</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_286" n="HIAT:w" s="T77">dʼel</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_289" n="HIAT:w" s="T78">mʼuzulǯuku</ts>
                  <nts id="Seg_290" n="HIAT:ip">.</nts>
                  <nts id="Seg_291" n="HIAT:ip">”</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_294" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_296" n="HIAT:w" s="T79">Saltšəbo</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_299" n="HIAT:w" s="T80">kašnɨj</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_302" n="HIAT:w" s="T81">dʼel</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_305" n="HIAT:w" s="T82">mʼuzulǯɨku</ts>
                  <nts id="Seg_306" n="HIAT:ip">.</nts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T83" id="Seg_308" n="sc" s="T1">
               <ts e="T2" id="Seg_310" n="e" s="T1">Mannan </ts>
               <ts e="T3" id="Seg_312" n="e" s="T2">maːdmo </ts>
               <ts e="T4" id="Seg_314" n="e" s="T3">tʼakkus. </ts>
               <ts e="T5" id="Seg_316" n="e" s="T4">Man </ts>
               <ts e="T6" id="Seg_318" n="e" s="T5">elɨzan </ts>
               <ts e="T7" id="Seg_320" n="e" s="T6">qullannan. </ts>
               <ts e="T8" id="Seg_322" n="e" s="T7">Patom </ts>
               <ts e="T9" id="Seg_324" n="e" s="T8">megunɨ </ts>
               <ts e="T10" id="Seg_326" n="e" s="T9">maːt </ts>
               <ts e="T11" id="Seg_328" n="e" s="T10">(maːdɨm) </ts>
               <ts e="T12" id="Seg_330" n="e" s="T11">mewattɨ. </ts>
               <ts e="T13" id="Seg_332" n="e" s="T12">A </ts>
               <ts e="T14" id="Seg_334" n="e" s="T13">man </ts>
               <ts e="T15" id="Seg_336" n="e" s="T14">tʼüan </ts>
               <ts e="T16" id="Seg_338" n="e" s="T15">na </ts>
               <ts e="T17" id="Seg_340" n="e" s="T16">mattə. </ts>
               <ts e="T18" id="Seg_342" n="e" s="T17">A </ts>
               <ts e="T19" id="Seg_344" n="e" s="T18">na </ts>
               <ts e="T20" id="Seg_346" n="e" s="T19">tʼedaɣɨn </ts>
               <ts e="T21" id="Seg_348" n="e" s="T20">takaja </ts>
               <ts e="T22" id="Seg_350" n="e" s="T21">čam. </ts>
               <ts e="T23" id="Seg_352" n="e" s="T22">Man </ts>
               <ts e="T24" id="Seg_354" n="e" s="T23">tʼettɨ </ts>
               <ts e="T25" id="Seg_356" n="e" s="T24">dʼel </ts>
               <ts e="T26" id="Seg_358" n="e" s="T25">ubirajčan. </ts>
               <ts e="T27" id="Seg_360" n="e" s="T26">Qutder </ts>
               <ts e="T28" id="Seg_362" n="e" s="T27">qula </ts>
               <ts e="T29" id="Seg_364" n="e" s="T28">illəpbattɨ. </ts>
               <ts e="T30" id="Seg_366" n="e" s="T29">A </ts>
               <ts e="T31" id="Seg_368" n="e" s="T30">man </ts>
               <ts e="T32" id="Seg_370" n="e" s="T31">iːw </ts>
               <ts e="T33" id="Seg_372" n="e" s="T32">tolʼko </ts>
               <ts e="T34" id="Seg_374" n="e" s="T33">ödi </ts>
               <ts e="T35" id="Seg_376" n="e" s="T34">tuddokun. </ts>
               <ts e="T36" id="Seg_378" n="e" s="T35">Man </ts>
               <ts e="T37" id="Seg_380" n="e" s="T36">müzulǯuguʒukan, </ts>
               <ts e="T38" id="Seg_382" n="e" s="T37">müzulǯuguʒukan. </ts>
               <ts e="T39" id="Seg_384" n="e" s="T38">Patom </ts>
               <ts e="T40" id="Seg_386" n="e" s="T39">manan </ts>
               <ts e="T41" id="Seg_388" n="e" s="T40">soːdʼiga </ts>
               <ts e="T42" id="Seg_390" n="e" s="T41">azun </ts>
               <ts e="T43" id="Seg_392" n="e" s="T42">maːdən </ts>
               <ts e="T44" id="Seg_394" n="e" s="T43">sʼütʼdʼe. </ts>
               <ts e="T45" id="Seg_396" n="e" s="T44">A </ts>
               <ts e="T46" id="Seg_398" n="e" s="T45">man </ts>
               <ts e="T47" id="Seg_400" n="e" s="T46">as </ts>
               <ts e="T48" id="Seg_402" n="e" s="T47">sorau </ts>
               <ts e="T49" id="Seg_404" n="e" s="T48">awan, </ts>
               <ts e="T50" id="Seg_406" n="e" s="T49">štobɨ </ts>
               <ts e="T51" id="Seg_408" n="e" s="T50">soːdʼigan </ts>
               <ts e="T52" id="Seg_410" n="e" s="T51">jegi. </ts>
               <ts e="T53" id="Seg_412" n="e" s="T52">A </ts>
               <ts e="T54" id="Seg_414" n="e" s="T53">teperʼ </ts>
               <ts e="T55" id="Seg_416" n="e" s="T54">aj </ts>
               <ts e="T56" id="Seg_418" n="e" s="T55">maːdmu </ts>
               <ts e="T57" id="Seg_420" n="e" s="T56">tʼäkku. </ts>
               <ts e="T58" id="Seg_422" n="e" s="T57">Aj </ts>
               <ts e="T59" id="Seg_424" n="e" s="T58">qullannan </ts>
               <ts e="T60" id="Seg_426" n="e" s="T59">warkan. </ts>
               <ts e="T61" id="Seg_428" n="e" s="T60">Tɨtdɨn </ts>
               <ts e="T62" id="Seg_430" n="e" s="T61">soːn </ts>
               <ts e="T63" id="Seg_432" n="e" s="T62">warkan. </ts>
               <ts e="T64" id="Seg_434" n="e" s="T63">Neuɣum </ts>
               <ts e="T65" id="Seg_436" n="e" s="T64">soːdiga, </ts>
               <ts e="T66" id="Seg_438" n="e" s="T65">mekka </ts>
               <ts e="T67" id="Seg_440" n="e" s="T66">as </ts>
               <ts e="T68" id="Seg_442" n="e" s="T67">qwɛdɨbukkɨn. </ts>
               <ts e="T69" id="Seg_444" n="e" s="T68">Man </ts>
               <ts e="T70" id="Seg_446" n="e" s="T69">oːnen </ts>
               <ts e="T71" id="Seg_448" n="e" s="T70">qaim </ts>
               <ts e="T72" id="Seg_450" n="e" s="T71">kɨgelam </ts>
               <ts e="T73" id="Seg_452" n="e" s="T72">naugam </ts>
               <ts e="T74" id="Seg_454" n="e" s="T73">melʼe </ts>
               <ts e="T75" id="Seg_456" n="e" s="T74">tadərəku. </ts>
               <ts e="T76" id="Seg_458" n="e" s="T75">“Maːdɨm </ts>
               <ts e="T77" id="Seg_460" n="e" s="T76">kaʒnɨj </ts>
               <ts e="T78" id="Seg_462" n="e" s="T77">dʼel </ts>
               <ts e="T79" id="Seg_464" n="e" s="T78">mʼuzulǯuku.” </ts>
               <ts e="T80" id="Seg_466" n="e" s="T79">Saltšəbo </ts>
               <ts e="T81" id="Seg_468" n="e" s="T80">kašnɨj </ts>
               <ts e="T82" id="Seg_470" n="e" s="T81">dʼel </ts>
               <ts e="T83" id="Seg_472" n="e" s="T82">mʼuzulǯɨku. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_473" s="T1">PVD_1964_IHadNoHouse_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_474" s="T4">PVD_1964_IHadNoHouse_nar.002 (001.002)</ta>
            <ta e="T12" id="Seg_475" s="T7">PVD_1964_IHadNoHouse_nar.003 (001.003)</ta>
            <ta e="T17" id="Seg_476" s="T12">PVD_1964_IHadNoHouse_nar.004 (001.004)</ta>
            <ta e="T22" id="Seg_477" s="T17">PVD_1964_IHadNoHouse_nar.005 (001.005)</ta>
            <ta e="T26" id="Seg_478" s="T22">PVD_1964_IHadNoHouse_nar.006 (001.006)</ta>
            <ta e="T29" id="Seg_479" s="T26">PVD_1964_IHadNoHouse_nar.007 (001.007)</ta>
            <ta e="T35" id="Seg_480" s="T29">PVD_1964_IHadNoHouse_nar.008 (001.008)</ta>
            <ta e="T38" id="Seg_481" s="T35">PVD_1964_IHadNoHouse_nar.009 (001.009)</ta>
            <ta e="T44" id="Seg_482" s="T38">PVD_1964_IHadNoHouse_nar.010 (001.010)</ta>
            <ta e="T52" id="Seg_483" s="T44">PVD_1964_IHadNoHouse_nar.011 (001.011)</ta>
            <ta e="T57" id="Seg_484" s="T52">PVD_1964_IHadNoHouse_nar.012 (001.012)</ta>
            <ta e="T60" id="Seg_485" s="T57">PVD_1964_IHadNoHouse_nar.013 (001.013)</ta>
            <ta e="T63" id="Seg_486" s="T60">PVD_1964_IHadNoHouse_nar.014 (001.014)</ta>
            <ta e="T68" id="Seg_487" s="T63">PVD_1964_IHadNoHouse_nar.015 (001.015)</ta>
            <ta e="T75" id="Seg_488" s="T68">PVD_1964_IHadNoHouse_nar.016 (001.016)</ta>
            <ta e="T79" id="Seg_489" s="T75">PVD_1964_IHadNoHouse_nar.017 (001.017)</ta>
            <ta e="T83" id="Seg_490" s="T79">PVD_1964_IHadNoHouse_nar.018 (001.018)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_491" s="T1">маннан ′ма̄дмо тʼаккус.</ta>
            <ta e="T7" id="Seg_492" s="T4">ман елы′зан kу′llаннан.</ta>
            <ta e="T12" id="Seg_493" s="T7">патом мегу′ны ма̄т (ма̄дым) ме′ватты.</ta>
            <ta e="T17" id="Seg_494" s="T12">а ман ′тʼӱан на ′маттъ.</ta>
            <ta e="T22" id="Seg_495" s="T17">а на ′тʼедаɣын та′каjа тшам.</ta>
            <ta e="T26" id="Seg_496" s="T22">ман ′тʼетты дʼел уби′райтшан.</ta>
            <ta e="T29" id="Seg_497" s="T26">kут′де̨р kу′lа ′иллъпбатты.</ta>
            <ta e="T35" id="Seg_498" s="T29">а ман ӣw ′толʼко ӧди ′туд̂докун.</ta>
            <ta e="T38" id="Seg_499" s="T35">ман мӱ′зуlджугу′жукан, мӱ′зуlджугужукан.</ta>
            <ta e="T44" id="Seg_500" s="T38">па′том ма′нан ′со̄дʼига а′зун ′ма̄дън ′сʼӱтʼдʼе.</ta>
            <ta e="T52" id="Seg_501" s="T44">а ман ас ′сорау ′аwан, штобы ′со̄дʼиган ′jеги.</ta>
            <ta e="T57" id="Seg_502" s="T52">а те̨пе̨рʼ ай ′ма̄дму тʼӓкку.</ta>
            <ta e="T60" id="Seg_503" s="T57">ай ′kуllаннан вар′кан.</ta>
            <ta e="T63" id="Seg_504" s="T60">тытдын со̄н вар′кан.</ta>
            <ta e="T68" id="Seg_505" s="T63">′не̨уɣум ′со̄дига, мекка ас ′kwɛдыб̂уккын.</ta>
            <ta e="T75" id="Seg_506" s="T68">ман ′о̄нен ′kаим кы′гелам нау′гам ′мелʼе ′тадъръку.</ta>
            <ta e="T79" id="Seg_507" s="T75">ма̄дым ′кажный дʼеl ′мʼу′зулджуку.</ta>
            <ta e="T83" id="Seg_508" s="T79">′саlтшъбо ′кашный дʼеl мʼузулджу(ы)ку.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_509" s="T1">mannan maːdmo tʼakkus.</ta>
            <ta e="T7" id="Seg_510" s="T4">man elɨzan qullannan.</ta>
            <ta e="T12" id="Seg_511" s="T7">patom megunɨ maːt (maːdɨm) mewattɨ.</ta>
            <ta e="T17" id="Seg_512" s="T12">a man tʼüan na mattə.</ta>
            <ta e="T22" id="Seg_513" s="T17">a na tʼedaɣɨn takaja tšam.</ta>
            <ta e="T26" id="Seg_514" s="T22">man tʼettɨ dʼel ubirajtšan.</ta>
            <ta e="T29" id="Seg_515" s="T26">qutder qula illəpbattɨ.</ta>
            <ta e="T35" id="Seg_516" s="T29">a man iːw tolʼko ödi tud̂dokun.</ta>
            <ta e="T38" id="Seg_517" s="T35">man müzulǯuguʒukan, müzulǯuguʒukan.</ta>
            <ta e="T44" id="Seg_518" s="T38">patom manan soːdʼiga azun maːdən sʼütʼdʼe.</ta>
            <ta e="T52" id="Seg_519" s="T44">a man as sorau awan, štobɨ soːdʼigan jegi.</ta>
            <ta e="T57" id="Seg_520" s="T52">a teperʼ aj maːdmu tʼäkku.</ta>
            <ta e="T60" id="Seg_521" s="T57">aj qullannan warkan.</ta>
            <ta e="T63" id="Seg_522" s="T60">tɨtdɨn soːn warkan.</ta>
            <ta e="T68" id="Seg_523" s="T63">neuɣum soːdiga, mekka as qwɛdɨb̂ukkɨn.</ta>
            <ta e="T75" id="Seg_524" s="T68">man oːnen qaim kɨgelam naugam melʼe tadərəku.</ta>
            <ta e="T79" id="Seg_525" s="T75">maːdɨm kaʒnɨj dʼel mʼuzulǯuku.</ta>
            <ta e="T83" id="Seg_526" s="T79">saltšəbo kašnɨj dʼel mʼuzulǯu(ɨ)ku.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_527" s="T1">Mannan maːdmo tʼakkus. </ta>
            <ta e="T7" id="Seg_528" s="T4">Man elɨzan qullannan. </ta>
            <ta e="T12" id="Seg_529" s="T7">Patom megunɨ maːt (maːdɨm) mewattɨ. </ta>
            <ta e="T17" id="Seg_530" s="T12">A man tʼüan na mattə. </ta>
            <ta e="T22" id="Seg_531" s="T17">A na tʼedaɣɨn takaja čam. </ta>
            <ta e="T26" id="Seg_532" s="T22">Man tʼettɨ dʼel ubirajčan. </ta>
            <ta e="T29" id="Seg_533" s="T26">Qutder qula illəpbattɨ. </ta>
            <ta e="T35" id="Seg_534" s="T29">A man iːw tolʼko ödi tuddokun. </ta>
            <ta e="T38" id="Seg_535" s="T35">Man müzulǯuguʒukan, müzulǯuguʒukan. </ta>
            <ta e="T44" id="Seg_536" s="T38">Patom manan soːdʼiga azun maːdən sʼütʼdʼe. </ta>
            <ta e="T52" id="Seg_537" s="T44">A man as sorau awan, štobɨ soːdʼigan jegi. </ta>
            <ta e="T57" id="Seg_538" s="T52">A teperʼ aj maːdmu tʼäkku. </ta>
            <ta e="T60" id="Seg_539" s="T57">Aj qullannan warkan. </ta>
            <ta e="T63" id="Seg_540" s="T60">Tɨtdɨn soːn warkan. </ta>
            <ta e="T68" id="Seg_541" s="T63">Neuɣum soːdiga, mekka as qwɛdɨbukkɨn. </ta>
            <ta e="T75" id="Seg_542" s="T68">Man oːnen qaim kɨgelam naugam melʼe tadərəku. </ta>
            <ta e="T79" id="Seg_543" s="T75">“Maːdɨm kaʒnɨj dʼel mʼuzulǯuku.” </ta>
            <ta e="T83" id="Seg_544" s="T79">Saltšəbo kašnɨj dʼel mʼuzulǯɨku. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_545" s="T1">man-nan</ta>
            <ta e="T3" id="Seg_546" s="T2">maːd-mo</ta>
            <ta e="T4" id="Seg_547" s="T3">tʼakku-s</ta>
            <ta e="T5" id="Seg_548" s="T4">man</ta>
            <ta e="T6" id="Seg_549" s="T5">elɨ-za-n</ta>
            <ta e="T7" id="Seg_550" s="T6">qul-la-nnan</ta>
            <ta e="T8" id="Seg_551" s="T7">patom</ta>
            <ta e="T9" id="Seg_552" s="T8">megunɨ</ta>
            <ta e="T10" id="Seg_553" s="T9">maːt</ta>
            <ta e="T11" id="Seg_554" s="T10">maːd-ɨ-m</ta>
            <ta e="T12" id="Seg_555" s="T11">me-wa-ttɨ</ta>
            <ta e="T13" id="Seg_556" s="T12">a</ta>
            <ta e="T14" id="Seg_557" s="T13">man</ta>
            <ta e="T15" id="Seg_558" s="T14">tʼü-a-n</ta>
            <ta e="T16" id="Seg_559" s="T15">na</ta>
            <ta e="T17" id="Seg_560" s="T16">mat-tə</ta>
            <ta e="T18" id="Seg_561" s="T17">a</ta>
            <ta e="T19" id="Seg_562" s="T18">na</ta>
            <ta e="T20" id="Seg_563" s="T19">tʼeda-ɣɨn</ta>
            <ta e="T22" id="Seg_564" s="T21">čam</ta>
            <ta e="T23" id="Seg_565" s="T22">man</ta>
            <ta e="T24" id="Seg_566" s="T23">tʼettɨ</ta>
            <ta e="T25" id="Seg_567" s="T24">dʼel</ta>
            <ta e="T26" id="Seg_568" s="T25">ubiraj-ča-n</ta>
            <ta e="T27" id="Seg_569" s="T26">qutder</ta>
            <ta e="T28" id="Seg_570" s="T27">qu-la</ta>
            <ta e="T29" id="Seg_571" s="T28">illə-pba-ttɨ</ta>
            <ta e="T30" id="Seg_572" s="T29">a</ta>
            <ta e="T31" id="Seg_573" s="T30">man</ta>
            <ta e="T32" id="Seg_574" s="T31">iː-w</ta>
            <ta e="T33" id="Seg_575" s="T32">tolʼko</ta>
            <ta e="T34" id="Seg_576" s="T33">ödi</ta>
            <ta e="T35" id="Seg_577" s="T34">tuddo-ku-n</ta>
            <ta e="T36" id="Seg_578" s="T35">man</ta>
            <ta e="T37" id="Seg_579" s="T36">müzulǯu-gu-ʒu-ka-n</ta>
            <ta e="T38" id="Seg_580" s="T37">müzulǯu-gu-ʒu-ka-n</ta>
            <ta e="T39" id="Seg_581" s="T38">patom</ta>
            <ta e="T40" id="Seg_582" s="T39">ma-nan</ta>
            <ta e="T41" id="Seg_583" s="T40">soːdʼiga</ta>
            <ta e="T42" id="Seg_584" s="T41">azu-n</ta>
            <ta e="T43" id="Seg_585" s="T42">maːd-ə-n</ta>
            <ta e="T44" id="Seg_586" s="T43">sʼütʼdʼe</ta>
            <ta e="T45" id="Seg_587" s="T44">a</ta>
            <ta e="T46" id="Seg_588" s="T45">man</ta>
            <ta e="T47" id="Seg_589" s="T46">as</ta>
            <ta e="T48" id="Seg_590" s="T47">sora-u</ta>
            <ta e="T49" id="Seg_591" s="T48">awa-n</ta>
            <ta e="T50" id="Seg_592" s="T49">štobɨ</ta>
            <ta e="T51" id="Seg_593" s="T50">soːdʼiga-n</ta>
            <ta e="T52" id="Seg_594" s="T51">je-gi</ta>
            <ta e="T53" id="Seg_595" s="T52">a</ta>
            <ta e="T54" id="Seg_596" s="T53">teperʼ</ta>
            <ta e="T55" id="Seg_597" s="T54">aj</ta>
            <ta e="T56" id="Seg_598" s="T55">maːd-mu</ta>
            <ta e="T57" id="Seg_599" s="T56">tʼäkku</ta>
            <ta e="T58" id="Seg_600" s="T57">aj</ta>
            <ta e="T59" id="Seg_601" s="T58">qul-la-nnan</ta>
            <ta e="T60" id="Seg_602" s="T59">warka-n</ta>
            <ta e="T61" id="Seg_603" s="T60">tɨtdɨ-n</ta>
            <ta e="T62" id="Seg_604" s="T61">soːn</ta>
            <ta e="T63" id="Seg_605" s="T62">warka-n</ta>
            <ta e="T64" id="Seg_606" s="T63">ne-u-ɣum</ta>
            <ta e="T65" id="Seg_607" s="T64">soːdiga</ta>
            <ta e="T66" id="Seg_608" s="T65">mekka</ta>
            <ta e="T67" id="Seg_609" s="T66">as</ta>
            <ta e="T68" id="Seg_610" s="T67">qwɛdɨ-bu-kkɨ-n</ta>
            <ta e="T69" id="Seg_611" s="T68">man</ta>
            <ta e="T70" id="Seg_612" s="T69">oːnen</ta>
            <ta e="T71" id="Seg_613" s="T70">qai-m</ta>
            <ta e="T72" id="Seg_614" s="T71">kɨge-la-m</ta>
            <ta e="T73" id="Seg_615" s="T72">nauga-m</ta>
            <ta e="T74" id="Seg_616" s="T73">me-lʼe</ta>
            <ta e="T75" id="Seg_617" s="T74">tad-ə-r-ə-ku</ta>
            <ta e="T76" id="Seg_618" s="T75">maːd-ɨ-m</ta>
            <ta e="T77" id="Seg_619" s="T76">kaʒnɨj</ta>
            <ta e="T78" id="Seg_620" s="T77">dʼel</ta>
            <ta e="T79" id="Seg_621" s="T78">mʼuzulǯu-ku</ta>
            <ta e="T80" id="Seg_622" s="T79">saltšəbo</ta>
            <ta e="T81" id="Seg_623" s="T80">kašnɨj</ta>
            <ta e="T82" id="Seg_624" s="T81">dʼel</ta>
            <ta e="T83" id="Seg_625" s="T82">mʼuzulǯɨ-ku</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_626" s="T1">man-nan</ta>
            <ta e="T3" id="Seg_627" s="T2">maːt-w</ta>
            <ta e="T4" id="Seg_628" s="T3">tʼäkku-sɨ</ta>
            <ta e="T5" id="Seg_629" s="T4">man</ta>
            <ta e="T6" id="Seg_630" s="T5">elɨ-sɨ-ŋ</ta>
            <ta e="T7" id="Seg_631" s="T6">qum-la-nan</ta>
            <ta e="T8" id="Seg_632" s="T7">patom</ta>
            <ta e="T9" id="Seg_633" s="T8">meguni</ta>
            <ta e="T10" id="Seg_634" s="T9">maːt</ta>
            <ta e="T11" id="Seg_635" s="T10">maːt-ɨ-m</ta>
            <ta e="T12" id="Seg_636" s="T11">me-nɨ-tɨn</ta>
            <ta e="T13" id="Seg_637" s="T12">a</ta>
            <ta e="T14" id="Seg_638" s="T13">man</ta>
            <ta e="T15" id="Seg_639" s="T14">tüː-ɨ-ŋ</ta>
            <ta e="T16" id="Seg_640" s="T15">na</ta>
            <ta e="T17" id="Seg_641" s="T16">maːt-ntə</ta>
            <ta e="T18" id="Seg_642" s="T17">a</ta>
            <ta e="T19" id="Seg_643" s="T18">na</ta>
            <ta e="T20" id="Seg_644" s="T19">tɨtʼa-qɨn</ta>
            <ta e="T22" id="Seg_645" s="T21">čam</ta>
            <ta e="T23" id="Seg_646" s="T22">man</ta>
            <ta e="T24" id="Seg_647" s="T23">tettɨ</ta>
            <ta e="T25" id="Seg_648" s="T24">dʼel</ta>
            <ta e="T26" id="Seg_649" s="T25">ubiraj-ču-ŋ</ta>
            <ta e="T27" id="Seg_650" s="T26">qundar</ta>
            <ta e="T28" id="Seg_651" s="T27">qum-la</ta>
            <ta e="T29" id="Seg_652" s="T28">elɨ-mbɨ-tɨn</ta>
            <ta e="T30" id="Seg_653" s="T29">a</ta>
            <ta e="T31" id="Seg_654" s="T30">man</ta>
            <ta e="T32" id="Seg_655" s="T31">iː-w</ta>
            <ta e="T33" id="Seg_656" s="T32">tolʼko</ta>
            <ta e="T34" id="Seg_657" s="T33">üt</ta>
            <ta e="T35" id="Seg_658" s="T34">tundɨ-ku-n</ta>
            <ta e="T36" id="Seg_659" s="T35">man</ta>
            <ta e="T37" id="Seg_660" s="T36">müzulǯu-ku-ʒu-ku-ŋ</ta>
            <ta e="T38" id="Seg_661" s="T37">müzulǯu-ku-ʒu-ku-ŋ</ta>
            <ta e="T39" id="Seg_662" s="T38">patom</ta>
            <ta e="T40" id="Seg_663" s="T39">man-nan</ta>
            <ta e="T41" id="Seg_664" s="T40">soːdʼiga</ta>
            <ta e="T42" id="Seg_665" s="T41">azu-n</ta>
            <ta e="T43" id="Seg_666" s="T42">maːt-ɨ-n</ta>
            <ta e="T44" id="Seg_667" s="T43">sʼütdʼe</ta>
            <ta e="T45" id="Seg_668" s="T44">a</ta>
            <ta e="T46" id="Seg_669" s="T45">man</ta>
            <ta e="T47" id="Seg_670" s="T46">asa</ta>
            <ta e="T48" id="Seg_671" s="T47">soːrɨ-w</ta>
            <ta e="T49" id="Seg_672" s="T48">awa-ŋ</ta>
            <ta e="T50" id="Seg_673" s="T49">štobɨ</ta>
            <ta e="T51" id="Seg_674" s="T50">soːdʼiga-ŋ</ta>
            <ta e="T52" id="Seg_675" s="T51">eː-ku</ta>
            <ta e="T53" id="Seg_676" s="T52">a</ta>
            <ta e="T54" id="Seg_677" s="T53">teper</ta>
            <ta e="T55" id="Seg_678" s="T54">aj</ta>
            <ta e="T56" id="Seg_679" s="T55">maːt-w</ta>
            <ta e="T57" id="Seg_680" s="T56">tʼäkku</ta>
            <ta e="T58" id="Seg_681" s="T57">aj</ta>
            <ta e="T59" id="Seg_682" s="T58">qum-la-nan</ta>
            <ta e="T60" id="Seg_683" s="T59">warkɨ-ŋ</ta>
            <ta e="T61" id="Seg_684" s="T60">tɨtʼa-n</ta>
            <ta e="T62" id="Seg_685" s="T61">soŋ</ta>
            <ta e="T63" id="Seg_686" s="T62">warkɨ-ŋ</ta>
            <ta e="T64" id="Seg_687" s="T63">ne-ɨ-qum</ta>
            <ta e="T65" id="Seg_688" s="T64">soːdʼiga</ta>
            <ta e="T66" id="Seg_689" s="T65">mekka</ta>
            <ta e="T67" id="Seg_690" s="T66">asa</ta>
            <ta e="T68" id="Seg_691" s="T67">qwɛdɨ-mbɨ-ku-n</ta>
            <ta e="T69" id="Seg_692" s="T68">man</ta>
            <ta e="T70" id="Seg_693" s="T69">oneŋ</ta>
            <ta e="T71" id="Seg_694" s="T70">qaj-m</ta>
            <ta e="T72" id="Seg_695" s="T71">kɨgɨ-lä-w</ta>
            <ta e="T73" id="Seg_696" s="T72">nauga-m</ta>
            <ta e="T74" id="Seg_697" s="T73">meː-le</ta>
            <ta e="T75" id="Seg_698" s="T74">tat-ɨ-r-ɨ-ku</ta>
            <ta e="T76" id="Seg_699" s="T75">maːt-ɨ-m</ta>
            <ta e="T77" id="Seg_700" s="T76">kaʒnaj</ta>
            <ta e="T78" id="Seg_701" s="T77">dʼel</ta>
            <ta e="T79" id="Seg_702" s="T78">müzulǯu-kɨ</ta>
            <ta e="T80" id="Seg_703" s="T79">saltšibo</ta>
            <ta e="T81" id="Seg_704" s="T80">kaʒnaj</ta>
            <ta e="T82" id="Seg_705" s="T81">dʼel</ta>
            <ta e="T83" id="Seg_706" s="T82">müzulǯu-ku</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_707" s="T1">I-ADES</ta>
            <ta e="T3" id="Seg_708" s="T2">house.[NOM]-1SG</ta>
            <ta e="T4" id="Seg_709" s="T3">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T5" id="Seg_710" s="T4">I.NOM</ta>
            <ta e="T6" id="Seg_711" s="T5">live-PST-1SG.S</ta>
            <ta e="T7" id="Seg_712" s="T6">human.being-PL-ADES</ta>
            <ta e="T8" id="Seg_713" s="T7">then</ta>
            <ta e="T9" id="Seg_714" s="T8">we.DU.ALL</ta>
            <ta e="T10" id="Seg_715" s="T9">house.[NOM]</ta>
            <ta e="T11" id="Seg_716" s="T10">house-EP-ACC</ta>
            <ta e="T12" id="Seg_717" s="T11">give-CO-3PL</ta>
            <ta e="T13" id="Seg_718" s="T12">and</ta>
            <ta e="T14" id="Seg_719" s="T13">I.NOM</ta>
            <ta e="T15" id="Seg_720" s="T14">come-EP-1SG.S</ta>
            <ta e="T16" id="Seg_721" s="T15">this</ta>
            <ta e="T17" id="Seg_722" s="T16">house-ILL</ta>
            <ta e="T18" id="Seg_723" s="T17">and</ta>
            <ta e="T19" id="Seg_724" s="T18">this</ta>
            <ta e="T20" id="Seg_725" s="T19">here-LOC</ta>
            <ta e="T22" id="Seg_726" s="T21">dirt.[NOM]</ta>
            <ta e="T23" id="Seg_727" s="T22">I.NOM</ta>
            <ta e="T24" id="Seg_728" s="T23">four</ta>
            <ta e="T25" id="Seg_729" s="T24">day.[NOM]</ta>
            <ta e="T26" id="Seg_730" s="T25">clean-TR-1SG.S</ta>
            <ta e="T27" id="Seg_731" s="T26">how</ta>
            <ta e="T28" id="Seg_732" s="T27">human.being-PL.[NOM]</ta>
            <ta e="T29" id="Seg_733" s="T28">live-PST.NAR-3PL</ta>
            <ta e="T30" id="Seg_734" s="T29">and</ta>
            <ta e="T31" id="Seg_735" s="T30">I.GEN</ta>
            <ta e="T32" id="Seg_736" s="T31">son.[NOM]-1SG</ta>
            <ta e="T33" id="Seg_737" s="T32">only</ta>
            <ta e="T34" id="Seg_738" s="T33">water.[NOM]</ta>
            <ta e="T35" id="Seg_739" s="T34">carry-HAB-3SG.S</ta>
            <ta e="T36" id="Seg_740" s="T35">I.NOM</ta>
            <ta e="T37" id="Seg_741" s="T36">wash-HAB-DRV-HAB-1SG.S</ta>
            <ta e="T38" id="Seg_742" s="T37">wash-HAB-DRV-HAB-1SG.S</ta>
            <ta e="T39" id="Seg_743" s="T38">then</ta>
            <ta e="T40" id="Seg_744" s="T39">I-ADES</ta>
            <ta e="T41" id="Seg_745" s="T40">good</ta>
            <ta e="T42" id="Seg_746" s="T41">become-3SG.S</ta>
            <ta e="T43" id="Seg_747" s="T42">house-EP-GEN</ta>
            <ta e="T44" id="Seg_748" s="T43">inside.[NOM]</ta>
            <ta e="T45" id="Seg_749" s="T44">and</ta>
            <ta e="T46" id="Seg_750" s="T45">I.NOM</ta>
            <ta e="T47" id="Seg_751" s="T46">NEG</ta>
            <ta e="T48" id="Seg_752" s="T47">love-1SG.O</ta>
            <ta e="T49" id="Seg_753" s="T48">bad-ADVZ</ta>
            <ta e="T50" id="Seg_754" s="T49">so.that</ta>
            <ta e="T51" id="Seg_755" s="T50">good-ADVZ</ta>
            <ta e="T52" id="Seg_756" s="T51">be-HAB.[3SG.S]</ta>
            <ta e="T53" id="Seg_757" s="T52">and</ta>
            <ta e="T54" id="Seg_758" s="T53">now</ta>
            <ta e="T55" id="Seg_759" s="T54">again</ta>
            <ta e="T56" id="Seg_760" s="T55">house.[NOM]-1SG</ta>
            <ta e="T57" id="Seg_761" s="T56">NEG.EX.[3SG.S]</ta>
            <ta e="T58" id="Seg_762" s="T57">again</ta>
            <ta e="T59" id="Seg_763" s="T58">human.being-PL-ADES</ta>
            <ta e="T60" id="Seg_764" s="T59">live-1SG.S</ta>
            <ta e="T61" id="Seg_765" s="T60">here-ADV.LOC</ta>
            <ta e="T62" id="Seg_766" s="T61">good</ta>
            <ta e="T63" id="Seg_767" s="T62">live-1SG.S</ta>
            <ta e="T64" id="Seg_768" s="T63">woman-EP-human.being.[NOM]</ta>
            <ta e="T65" id="Seg_769" s="T64">good.[3SG.S]</ta>
            <ta e="T66" id="Seg_770" s="T65">I.ALL</ta>
            <ta e="T67" id="Seg_771" s="T66">NEG</ta>
            <ta e="T68" id="Seg_772" s="T67">swear.at-DUR-HAB-3SG.S</ta>
            <ta e="T69" id="Seg_773" s="T68">I.NOM</ta>
            <ta e="T70" id="Seg_774" s="T69">oneself.1SG</ta>
            <ta e="T71" id="Seg_775" s="T70">what-ACC</ta>
            <ta e="T72" id="Seg_776" s="T71">want-OPT-1SG.O</ta>
            <ta e="T73" id="Seg_777" s="T72">that-ACC</ta>
            <ta e="T74" id="Seg_778" s="T73">do-CVB</ta>
            <ta e="T75" id="Seg_779" s="T74">bring-EP-FRQ-EP-HAB.[3SG.S]</ta>
            <ta e="T76" id="Seg_780" s="T75">house-EP-ACC</ta>
            <ta e="T77" id="Seg_781" s="T76">every</ta>
            <ta e="T78" id="Seg_782" s="T77">day.[NOM]</ta>
            <ta e="T79" id="Seg_783" s="T78">wash-IMP.2SG.S</ta>
            <ta e="T80" id="Seg_784" s="T79">floor.[NOM]</ta>
            <ta e="T81" id="Seg_785" s="T80">every</ta>
            <ta e="T82" id="Seg_786" s="T81">day.[NOM]</ta>
            <ta e="T83" id="Seg_787" s="T82">wash-HAB.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_788" s="T1">я-ADES</ta>
            <ta e="T3" id="Seg_789" s="T2">дом.[NOM]-1SG</ta>
            <ta e="T4" id="Seg_790" s="T3">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T5" id="Seg_791" s="T4">я.NOM</ta>
            <ta e="T6" id="Seg_792" s="T5">жить-PST-1SG.S</ta>
            <ta e="T7" id="Seg_793" s="T6">человек-PL-ADES</ta>
            <ta e="T8" id="Seg_794" s="T7">потом</ta>
            <ta e="T9" id="Seg_795" s="T8">мы.DU.ALL</ta>
            <ta e="T10" id="Seg_796" s="T9">дом.[NOM]</ta>
            <ta e="T11" id="Seg_797" s="T10">дом-EP-ACC</ta>
            <ta e="T12" id="Seg_798" s="T11">дать-CO-3PL</ta>
            <ta e="T13" id="Seg_799" s="T12">а</ta>
            <ta e="T14" id="Seg_800" s="T13">я.NOM</ta>
            <ta e="T15" id="Seg_801" s="T14">прийти-EP-1SG.S</ta>
            <ta e="T16" id="Seg_802" s="T15">этот</ta>
            <ta e="T17" id="Seg_803" s="T16">дом-ILL</ta>
            <ta e="T18" id="Seg_804" s="T17">а</ta>
            <ta e="T19" id="Seg_805" s="T18">этот</ta>
            <ta e="T20" id="Seg_806" s="T19">сюда-LOC</ta>
            <ta e="T22" id="Seg_807" s="T21">грязь.[NOM]</ta>
            <ta e="T23" id="Seg_808" s="T22">я.NOM</ta>
            <ta e="T24" id="Seg_809" s="T23">четыре</ta>
            <ta e="T25" id="Seg_810" s="T24">день.[NOM]</ta>
            <ta e="T26" id="Seg_811" s="T25">убрать-TR-1SG.S</ta>
            <ta e="T27" id="Seg_812" s="T26">как</ta>
            <ta e="T28" id="Seg_813" s="T27">человек-PL.[NOM]</ta>
            <ta e="T29" id="Seg_814" s="T28">жить-PST.NAR-3PL</ta>
            <ta e="T30" id="Seg_815" s="T29">а</ta>
            <ta e="T31" id="Seg_816" s="T30">я.GEN</ta>
            <ta e="T32" id="Seg_817" s="T31">сын.[NOM]-1SG</ta>
            <ta e="T33" id="Seg_818" s="T32">только</ta>
            <ta e="T34" id="Seg_819" s="T33">вода.[NOM]</ta>
            <ta e="T35" id="Seg_820" s="T34">таскать-HAB-3SG.S</ta>
            <ta e="T36" id="Seg_821" s="T35">я.NOM</ta>
            <ta e="T37" id="Seg_822" s="T36">вымыть-HAB-DRV-HAB-1SG.S</ta>
            <ta e="T38" id="Seg_823" s="T37">вымыть-HAB-DRV-HAB-1SG.S</ta>
            <ta e="T39" id="Seg_824" s="T38">потом</ta>
            <ta e="T40" id="Seg_825" s="T39">я-ADES</ta>
            <ta e="T41" id="Seg_826" s="T40">хороший</ta>
            <ta e="T42" id="Seg_827" s="T41">стать-3SG.S</ta>
            <ta e="T43" id="Seg_828" s="T42">дом-EP-GEN</ta>
            <ta e="T44" id="Seg_829" s="T43">нутро.[NOM]</ta>
            <ta e="T45" id="Seg_830" s="T44">а</ta>
            <ta e="T46" id="Seg_831" s="T45">я.NOM</ta>
            <ta e="T47" id="Seg_832" s="T46">NEG</ta>
            <ta e="T48" id="Seg_833" s="T47">любить-1SG.O</ta>
            <ta e="T49" id="Seg_834" s="T48">плохой-ADVZ</ta>
            <ta e="T50" id="Seg_835" s="T49">чтобы</ta>
            <ta e="T51" id="Seg_836" s="T50">хороший-ADVZ</ta>
            <ta e="T52" id="Seg_837" s="T51">быть-HAB.[3SG.S]</ta>
            <ta e="T53" id="Seg_838" s="T52">а</ta>
            <ta e="T54" id="Seg_839" s="T53">теперь</ta>
            <ta e="T55" id="Seg_840" s="T54">опять</ta>
            <ta e="T56" id="Seg_841" s="T55">дом.[NOM]-1SG</ta>
            <ta e="T57" id="Seg_842" s="T56">NEG.EX.[3SG.S]</ta>
            <ta e="T58" id="Seg_843" s="T57">опять</ta>
            <ta e="T59" id="Seg_844" s="T58">человек-PL-ADES</ta>
            <ta e="T60" id="Seg_845" s="T59">жить-1SG.S</ta>
            <ta e="T61" id="Seg_846" s="T60">сюда-ADV.LOC</ta>
            <ta e="T62" id="Seg_847" s="T61">хорошо</ta>
            <ta e="T63" id="Seg_848" s="T62">жить-1SG.S</ta>
            <ta e="T64" id="Seg_849" s="T63">женщина-EP-человек.[NOM]</ta>
            <ta e="T65" id="Seg_850" s="T64">хороший.[3SG.S]</ta>
            <ta e="T66" id="Seg_851" s="T65">я.ALL</ta>
            <ta e="T67" id="Seg_852" s="T66">NEG</ta>
            <ta e="T68" id="Seg_853" s="T67">поругать-DUR-HAB-3SG.S</ta>
            <ta e="T69" id="Seg_854" s="T68">я.NOM</ta>
            <ta e="T70" id="Seg_855" s="T69">сам.1SG</ta>
            <ta e="T71" id="Seg_856" s="T70">что-ACC</ta>
            <ta e="T72" id="Seg_857" s="T71">хотеть-OPT-1SG.O</ta>
            <ta e="T73" id="Seg_858" s="T72">тот-ACC</ta>
            <ta e="T74" id="Seg_859" s="T73">сделать-CVB</ta>
            <ta e="T75" id="Seg_860" s="T74">принести-EP-FRQ-EP-HAB.[3SG.S]</ta>
            <ta e="T76" id="Seg_861" s="T75">дом-EP-ACC</ta>
            <ta e="T77" id="Seg_862" s="T76">каждый</ta>
            <ta e="T78" id="Seg_863" s="T77">день.[NOM]</ta>
            <ta e="T79" id="Seg_864" s="T78">вымыть-IMP.2SG.S</ta>
            <ta e="T80" id="Seg_865" s="T79">пол.[NOM]</ta>
            <ta e="T81" id="Seg_866" s="T80">каждый</ta>
            <ta e="T82" id="Seg_867" s="T81">день.[NOM]</ta>
            <ta e="T83" id="Seg_868" s="T82">вымыть-HAB.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_869" s="T1">pers-n:case</ta>
            <ta e="T3" id="Seg_870" s="T2">n.[n:case]-n:poss</ta>
            <ta e="T4" id="Seg_871" s="T3">v-v:tense.[v:pn]</ta>
            <ta e="T5" id="Seg_872" s="T4">pers</ta>
            <ta e="T6" id="Seg_873" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_874" s="T6">n-n:num-n:case</ta>
            <ta e="T8" id="Seg_875" s="T7">adv</ta>
            <ta e="T9" id="Seg_876" s="T8">pers</ta>
            <ta e="T10" id="Seg_877" s="T9">n.[n:case]</ta>
            <ta e="T11" id="Seg_878" s="T10">n-n:ins-n:case</ta>
            <ta e="T12" id="Seg_879" s="T11">v-v:ins-v:pn</ta>
            <ta e="T13" id="Seg_880" s="T12">conj</ta>
            <ta e="T14" id="Seg_881" s="T13">pers</ta>
            <ta e="T15" id="Seg_882" s="T14">v-n:ins-v:pn</ta>
            <ta e="T16" id="Seg_883" s="T15">dem</ta>
            <ta e="T17" id="Seg_884" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_885" s="T17">conj</ta>
            <ta e="T19" id="Seg_886" s="T18">dem</ta>
            <ta e="T20" id="Seg_887" s="T19">adv-n:case</ta>
            <ta e="T22" id="Seg_888" s="T21">n.[n:case]</ta>
            <ta e="T23" id="Seg_889" s="T22">pers</ta>
            <ta e="T24" id="Seg_890" s="T23">num</ta>
            <ta e="T25" id="Seg_891" s="T24">n.[n:case]</ta>
            <ta e="T26" id="Seg_892" s="T25">v-v&gt;v-v:pn</ta>
            <ta e="T27" id="Seg_893" s="T26">interrog</ta>
            <ta e="T28" id="Seg_894" s="T27">n-n:num.[n:case]</ta>
            <ta e="T29" id="Seg_895" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_896" s="T29">conj</ta>
            <ta e="T31" id="Seg_897" s="T30">pers</ta>
            <ta e="T32" id="Seg_898" s="T31">n.[n:case]-n:poss</ta>
            <ta e="T33" id="Seg_899" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_900" s="T33">n.[n:case]</ta>
            <ta e="T35" id="Seg_901" s="T34">v-v&gt;v-v:pn</ta>
            <ta e="T36" id="Seg_902" s="T35">pers</ta>
            <ta e="T37" id="Seg_903" s="T36">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T38" id="Seg_904" s="T37">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T39" id="Seg_905" s="T38">adv</ta>
            <ta e="T40" id="Seg_906" s="T39">pers-n:case</ta>
            <ta e="T41" id="Seg_907" s="T40">adj</ta>
            <ta e="T42" id="Seg_908" s="T41">v-v:pn</ta>
            <ta e="T43" id="Seg_909" s="T42">n-n:ins-n:case</ta>
            <ta e="T44" id="Seg_910" s="T43">n.[n:case]</ta>
            <ta e="T45" id="Seg_911" s="T44">conj</ta>
            <ta e="T46" id="Seg_912" s="T45">pers</ta>
            <ta e="T47" id="Seg_913" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_914" s="T47">v-v:pn</ta>
            <ta e="T49" id="Seg_915" s="T48">adj-adj&gt;adv</ta>
            <ta e="T50" id="Seg_916" s="T49">conj</ta>
            <ta e="T51" id="Seg_917" s="T50">adj-adj&gt;adv</ta>
            <ta e="T52" id="Seg_918" s="T51">v-v&gt;v.[v:pn]</ta>
            <ta e="T53" id="Seg_919" s="T52">conj</ta>
            <ta e="T54" id="Seg_920" s="T53">adv</ta>
            <ta e="T55" id="Seg_921" s="T54">adv</ta>
            <ta e="T56" id="Seg_922" s="T55">n.[n:case]-n:poss</ta>
            <ta e="T57" id="Seg_923" s="T56">v.[v:pn]</ta>
            <ta e="T58" id="Seg_924" s="T57">adv</ta>
            <ta e="T59" id="Seg_925" s="T58">n-n:num-n:case</ta>
            <ta e="T60" id="Seg_926" s="T59">v-v:pn</ta>
            <ta e="T61" id="Seg_927" s="T60">adv-adv:case</ta>
            <ta e="T62" id="Seg_928" s="T61">adv</ta>
            <ta e="T63" id="Seg_929" s="T62">v-v:pn</ta>
            <ta e="T64" id="Seg_930" s="T63">n-n:ins-n.[n:case]</ta>
            <ta e="T65" id="Seg_931" s="T64">adj.[v:pn]</ta>
            <ta e="T66" id="Seg_932" s="T65">pers</ta>
            <ta e="T67" id="Seg_933" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_934" s="T67">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T69" id="Seg_935" s="T68">pers</ta>
            <ta e="T70" id="Seg_936" s="T69">emphpro</ta>
            <ta e="T71" id="Seg_937" s="T70">interrog-n:case</ta>
            <ta e="T72" id="Seg_938" s="T71">v-v:mood-v:pn</ta>
            <ta e="T73" id="Seg_939" s="T72">dem-n:case</ta>
            <ta e="T74" id="Seg_940" s="T73">v-v&gt;adv</ta>
            <ta e="T75" id="Seg_941" s="T74">v-v:ins-v&gt;v-v:ins-v&gt;v.[v:pn]</ta>
            <ta e="T76" id="Seg_942" s="T75">n-n:ins-n:case</ta>
            <ta e="T77" id="Seg_943" s="T76">adj</ta>
            <ta e="T78" id="Seg_944" s="T77">n.[n:case]</ta>
            <ta e="T79" id="Seg_945" s="T78">v-v:mood.pn</ta>
            <ta e="T80" id="Seg_946" s="T79">n.[n:case]</ta>
            <ta e="T81" id="Seg_947" s="T80">adj</ta>
            <ta e="T82" id="Seg_948" s="T81">n.[n:case]</ta>
            <ta e="T83" id="Seg_949" s="T82">v-v&gt;v.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_950" s="T1">pers</ta>
            <ta e="T3" id="Seg_951" s="T2">n</ta>
            <ta e="T4" id="Seg_952" s="T3">v</ta>
            <ta e="T5" id="Seg_953" s="T4">pers</ta>
            <ta e="T6" id="Seg_954" s="T5">v</ta>
            <ta e="T7" id="Seg_955" s="T6">n</ta>
            <ta e="T8" id="Seg_956" s="T7">adv</ta>
            <ta e="T9" id="Seg_957" s="T8">pers</ta>
            <ta e="T10" id="Seg_958" s="T9">n</ta>
            <ta e="T11" id="Seg_959" s="T10">n</ta>
            <ta e="T12" id="Seg_960" s="T11">v</ta>
            <ta e="T13" id="Seg_961" s="T12">conj</ta>
            <ta e="T14" id="Seg_962" s="T13">pers</ta>
            <ta e="T15" id="Seg_963" s="T14">v</ta>
            <ta e="T16" id="Seg_964" s="T15">dem</ta>
            <ta e="T17" id="Seg_965" s="T16">n</ta>
            <ta e="T18" id="Seg_966" s="T17">conj</ta>
            <ta e="T19" id="Seg_967" s="T18">dem</ta>
            <ta e="T20" id="Seg_968" s="T19">adv</ta>
            <ta e="T22" id="Seg_969" s="T21">n</ta>
            <ta e="T23" id="Seg_970" s="T22">pers</ta>
            <ta e="T24" id="Seg_971" s="T23">num</ta>
            <ta e="T25" id="Seg_972" s="T24">n</ta>
            <ta e="T26" id="Seg_973" s="T25">v</ta>
            <ta e="T27" id="Seg_974" s="T26">interrog</ta>
            <ta e="T28" id="Seg_975" s="T27">n</ta>
            <ta e="T29" id="Seg_976" s="T28">v</ta>
            <ta e="T30" id="Seg_977" s="T29">conj</ta>
            <ta e="T31" id="Seg_978" s="T30">pers</ta>
            <ta e="T32" id="Seg_979" s="T31">n</ta>
            <ta e="T33" id="Seg_980" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_981" s="T33">n</ta>
            <ta e="T35" id="Seg_982" s="T34">v</ta>
            <ta e="T36" id="Seg_983" s="T35">pers</ta>
            <ta e="T37" id="Seg_984" s="T36">v</ta>
            <ta e="T38" id="Seg_985" s="T37">v</ta>
            <ta e="T39" id="Seg_986" s="T38">adv</ta>
            <ta e="T40" id="Seg_987" s="T39">pers</ta>
            <ta e="T41" id="Seg_988" s="T40">adj</ta>
            <ta e="T42" id="Seg_989" s="T41">v</ta>
            <ta e="T43" id="Seg_990" s="T42">n</ta>
            <ta e="T44" id="Seg_991" s="T43">n</ta>
            <ta e="T45" id="Seg_992" s="T44">conj</ta>
            <ta e="T46" id="Seg_993" s="T45">pers</ta>
            <ta e="T47" id="Seg_994" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_995" s="T47">v</ta>
            <ta e="T49" id="Seg_996" s="T48">adv</ta>
            <ta e="T50" id="Seg_997" s="T49">conj</ta>
            <ta e="T51" id="Seg_998" s="T50">adv</ta>
            <ta e="T52" id="Seg_999" s="T51">v</ta>
            <ta e="T53" id="Seg_1000" s="T52">conj</ta>
            <ta e="T54" id="Seg_1001" s="T53">adv</ta>
            <ta e="T55" id="Seg_1002" s="T54">adv</ta>
            <ta e="T56" id="Seg_1003" s="T55">n</ta>
            <ta e="T57" id="Seg_1004" s="T56">v</ta>
            <ta e="T58" id="Seg_1005" s="T57">adv</ta>
            <ta e="T59" id="Seg_1006" s="T58">n</ta>
            <ta e="T60" id="Seg_1007" s="T59">v</ta>
            <ta e="T61" id="Seg_1008" s="T60">adv</ta>
            <ta e="T62" id="Seg_1009" s="T61">adv</ta>
            <ta e="T63" id="Seg_1010" s="T62">v</ta>
            <ta e="T64" id="Seg_1011" s="T63">n</ta>
            <ta e="T65" id="Seg_1012" s="T64">adj</ta>
            <ta e="T66" id="Seg_1013" s="T65">pers</ta>
            <ta e="T67" id="Seg_1014" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_1015" s="T67">v</ta>
            <ta e="T69" id="Seg_1016" s="T68">pers</ta>
            <ta e="T70" id="Seg_1017" s="T69">emphpro</ta>
            <ta e="T71" id="Seg_1018" s="T70">interrog</ta>
            <ta e="T72" id="Seg_1019" s="T71">v</ta>
            <ta e="T73" id="Seg_1020" s="T72">dem</ta>
            <ta e="T74" id="Seg_1021" s="T73">adv</ta>
            <ta e="T75" id="Seg_1022" s="T74">v</ta>
            <ta e="T76" id="Seg_1023" s="T75">n</ta>
            <ta e="T77" id="Seg_1024" s="T76">adj</ta>
            <ta e="T78" id="Seg_1025" s="T77">n</ta>
            <ta e="T79" id="Seg_1026" s="T78">v</ta>
            <ta e="T80" id="Seg_1027" s="T79">n</ta>
            <ta e="T81" id="Seg_1028" s="T80">adj</ta>
            <ta e="T82" id="Seg_1029" s="T81">n</ta>
            <ta e="T83" id="Seg_1030" s="T82">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1031" s="T1">pro.h:Poss</ta>
            <ta e="T3" id="Seg_1032" s="T2">np:Th</ta>
            <ta e="T5" id="Seg_1033" s="T4">pro.h:Th</ta>
            <ta e="T7" id="Seg_1034" s="T6">np.h:L</ta>
            <ta e="T8" id="Seg_1035" s="T7">adv:Time</ta>
            <ta e="T9" id="Seg_1036" s="T8">pro.h:R</ta>
            <ta e="T10" id="Seg_1037" s="T9">np:Th</ta>
            <ta e="T12" id="Seg_1038" s="T11">0.3.h:A</ta>
            <ta e="T14" id="Seg_1039" s="T13">pro.h:A</ta>
            <ta e="T17" id="Seg_1040" s="T16">np:G</ta>
            <ta e="T20" id="Seg_1041" s="T19">adv:L</ta>
            <ta e="T22" id="Seg_1042" s="T21">np:Th</ta>
            <ta e="T23" id="Seg_1043" s="T22">pro.h:A</ta>
            <ta e="T25" id="Seg_1044" s="T24">np:Time</ta>
            <ta e="T28" id="Seg_1045" s="T27">np.h:Th</ta>
            <ta e="T31" id="Seg_1046" s="T30">pro.h:Poss</ta>
            <ta e="T32" id="Seg_1047" s="T31">np.h:A</ta>
            <ta e="T34" id="Seg_1048" s="T33">np:Th</ta>
            <ta e="T36" id="Seg_1049" s="T35">pro.h:A</ta>
            <ta e="T38" id="Seg_1050" s="T37">0.1.h:A</ta>
            <ta e="T39" id="Seg_1051" s="T38">adv:Time</ta>
            <ta e="T40" id="Seg_1052" s="T39">pro.h:Poss</ta>
            <ta e="T43" id="Seg_1053" s="T42">np:Poss</ta>
            <ta e="T44" id="Seg_1054" s="T43">np:Th</ta>
            <ta e="T46" id="Seg_1055" s="T45">pro.h:E</ta>
            <ta e="T52" id="Seg_1056" s="T51">0.3:Th</ta>
            <ta e="T54" id="Seg_1057" s="T53">adv:Time</ta>
            <ta e="T56" id="Seg_1058" s="T55">np:Th 0.1.h:Poss</ta>
            <ta e="T59" id="Seg_1059" s="T58">np.h:L</ta>
            <ta e="T60" id="Seg_1060" s="T59">0.1.h:Th</ta>
            <ta e="T61" id="Seg_1061" s="T60">adv:L</ta>
            <ta e="T63" id="Seg_1062" s="T62">0.1.h:Th</ta>
            <ta e="T64" id="Seg_1063" s="T63">np.h:Th</ta>
            <ta e="T66" id="Seg_1064" s="T65">pro.h:R</ta>
            <ta e="T68" id="Seg_1065" s="T67">0.3.h:A</ta>
            <ta e="T69" id="Seg_1066" s="T68">pro.h:E</ta>
            <ta e="T71" id="Seg_1067" s="T70">pro:Th</ta>
            <ta e="T73" id="Seg_1068" s="T72">pro:Th</ta>
            <ta e="T75" id="Seg_1069" s="T74">0.1.h:A</ta>
            <ta e="T76" id="Seg_1070" s="T75">np:Th</ta>
            <ta e="T78" id="Seg_1071" s="T77">np:Time</ta>
            <ta e="T79" id="Seg_1072" s="T78">0.2.h:A</ta>
            <ta e="T80" id="Seg_1073" s="T79">np:Th</ta>
            <ta e="T82" id="Seg_1074" s="T81">np:Time</ta>
            <ta e="T83" id="Seg_1075" s="T82">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_1076" s="T2">np:S</ta>
            <ta e="T4" id="Seg_1077" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_1078" s="T4">pro.h:S</ta>
            <ta e="T6" id="Seg_1079" s="T5">v:pred</ta>
            <ta e="T10" id="Seg_1080" s="T9">np:O</ta>
            <ta e="T12" id="Seg_1081" s="T11">0.3.h:S v:pred</ta>
            <ta e="T14" id="Seg_1082" s="T13">pro.h:S</ta>
            <ta e="T15" id="Seg_1083" s="T14">v:pred</ta>
            <ta e="T22" id="Seg_1084" s="T21">np:S</ta>
            <ta e="T23" id="Seg_1085" s="T22">pro.h:S</ta>
            <ta e="T26" id="Seg_1086" s="T25">v:pred</ta>
            <ta e="T28" id="Seg_1087" s="T27">np.h:S</ta>
            <ta e="T29" id="Seg_1088" s="T28">v:pred</ta>
            <ta e="T32" id="Seg_1089" s="T31">np.h:S</ta>
            <ta e="T34" id="Seg_1090" s="T33">np:O</ta>
            <ta e="T35" id="Seg_1091" s="T34">v:pred</ta>
            <ta e="T36" id="Seg_1092" s="T35">pro.h:S</ta>
            <ta e="T37" id="Seg_1093" s="T36">v:pred</ta>
            <ta e="T38" id="Seg_1094" s="T37">0.1.h:S v:pred</ta>
            <ta e="T41" id="Seg_1095" s="T40">adj:pred</ta>
            <ta e="T42" id="Seg_1096" s="T41">cop</ta>
            <ta e="T44" id="Seg_1097" s="T43">np:S</ta>
            <ta e="T46" id="Seg_1098" s="T45">pro.h:S</ta>
            <ta e="T48" id="Seg_1099" s="T47">v:pred</ta>
            <ta e="T52" id="Seg_1100" s="T51">0.3:S v:pred</ta>
            <ta e="T56" id="Seg_1101" s="T55">np:S</ta>
            <ta e="T57" id="Seg_1102" s="T56">v:pred</ta>
            <ta e="T60" id="Seg_1103" s="T59">0.1.h:S v:pred</ta>
            <ta e="T63" id="Seg_1104" s="T62">0.1.h:S v:pred</ta>
            <ta e="T64" id="Seg_1105" s="T63">np.h:S</ta>
            <ta e="T65" id="Seg_1106" s="T64">adj:pred</ta>
            <ta e="T68" id="Seg_1107" s="T67">0.3.h:S v:pred</ta>
            <ta e="T69" id="Seg_1108" s="T68">pro.h:S</ta>
            <ta e="T71" id="Seg_1109" s="T70">pro:O</ta>
            <ta e="T72" id="Seg_1110" s="T71">v:pred</ta>
            <ta e="T73" id="Seg_1111" s="T72">pro:O</ta>
            <ta e="T75" id="Seg_1112" s="T74">0.1.h:S v:pred</ta>
            <ta e="T76" id="Seg_1113" s="T75">np:O</ta>
            <ta e="T79" id="Seg_1114" s="T78">0.2.h:S v:pred</ta>
            <ta e="T80" id="Seg_1115" s="T79">np:O</ta>
            <ta e="T83" id="Seg_1116" s="T82">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T8" id="Seg_1117" s="T7">RUS:disc</ta>
            <ta e="T13" id="Seg_1118" s="T12">RUS:gram</ta>
            <ta e="T18" id="Seg_1119" s="T17">RUS:gram</ta>
            <ta e="T26" id="Seg_1120" s="T25">RUS:cult</ta>
            <ta e="T30" id="Seg_1121" s="T29">RUS:gram</ta>
            <ta e="T33" id="Seg_1122" s="T32">RUS:disc</ta>
            <ta e="T39" id="Seg_1123" s="T38">RUS:disc</ta>
            <ta e="T45" id="Seg_1124" s="T44">RUS:gram</ta>
            <ta e="T50" id="Seg_1125" s="T49">RUS:gram</ta>
            <ta e="T53" id="Seg_1126" s="T52">RUS:gram</ta>
            <ta e="T54" id="Seg_1127" s="T53">RUS:core</ta>
            <ta e="T77" id="Seg_1128" s="T76">RUS:core</ta>
            <ta e="T81" id="Seg_1129" s="T80">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_1130" s="T1">I didn't have my own home.</ta>
            <ta e="T7" id="Seg_1131" s="T4">I lived in the houses of other people.</ta>
            <ta e="T12" id="Seg_1132" s="T7">Then they gave two of us a house.</ta>
            <ta e="T17" id="Seg_1133" s="T12">And I came to this house.</ta>
            <ta e="T22" id="Seg_1134" s="T17">And it was so dirty here.</ta>
            <ta e="T26" id="Seg_1135" s="T22">I was cleaning four days long.</ta>
            <ta e="T29" id="Seg_1136" s="T26">How people could only live [here].</ta>
            <ta e="T35" id="Seg_1137" s="T29">And my son was bringing water all the time.</ta>
            <ta e="T38" id="Seg_1138" s="T35">I washed and washed.</ta>
            <ta e="T44" id="Seg_1139" s="T38">Then it became nice inside the house.</ta>
            <ta e="T52" id="Seg_1140" s="T44">And I don't like it bad (dirty), I like it to be nice.</ta>
            <ta e="T57" id="Seg_1141" s="T52">And now I don't have home again.</ta>
            <ta e="T60" id="Seg_1142" s="T57">Again I live by the others.</ta>
            <ta e="T63" id="Seg_1143" s="T60">I live well here.</ta>
            <ta e="T68" id="Seg_1144" s="T63">The woman is nice, she doesn't scold at me.</ta>
            <ta e="T75" id="Seg_1145" s="T68">I do myself what I want.</ta>
            <ta e="T79" id="Seg_1146" s="T75">“Wash the house every day.”</ta>
            <ta e="T83" id="Seg_1147" s="T79">I wash the floor every day.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_1148" s="T1">Ich hatte kein eigenes Zuhause.</ta>
            <ta e="T7" id="Seg_1149" s="T4">Ich wohnte in den Häusern anderer Leute.</ta>
            <ta e="T12" id="Seg_1150" s="T7">Dann gaben sie uns (beiden) ein Haus.</ta>
            <ta e="T17" id="Seg_1151" s="T12">Und ich kam in dieses Haus.</ta>
            <ta e="T22" id="Seg_1152" s="T17">Und es war so dreckig hier.</ta>
            <ta e="T26" id="Seg_1153" s="T22">Ich habe vier Tage lang geputzt.</ta>
            <ta e="T29" id="Seg_1154" s="T26">Wie konnten [hier] Menschen leben.</ta>
            <ta e="T35" id="Seg_1155" s="T29">Und mein Sohn brachte die ganze Zeit Wasser.</ta>
            <ta e="T38" id="Seg_1156" s="T35">Ich putzte und putzte.</ta>
            <ta e="T44" id="Seg_1157" s="T38">Dann wurde es schön im Inneren des Hauses.</ta>
            <ta e="T52" id="Seg_1158" s="T44">Und ich mag es nicht schlecht (dreckig), ich mag es schön.</ta>
            <ta e="T57" id="Seg_1159" s="T52">Und jetzt habe ich wieder kein Zuhause.</ta>
            <ta e="T60" id="Seg_1160" s="T57">Wieder wohne ich bei anderen.</ta>
            <ta e="T63" id="Seg_1161" s="T60">Hier lebe ich gut.</ta>
            <ta e="T68" id="Seg_1162" s="T63">Die Frau ist nett, sie schimpft nicht mit mir.</ta>
            <ta e="T75" id="Seg_1163" s="T68">Ich mache, was ich will.</ta>
            <ta e="T79" id="Seg_1164" s="T75">"Putz das Haus jeden Tag."</ta>
            <ta e="T83" id="Seg_1165" s="T79">Ich wische jeden Tag den Boden.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_1166" s="T1">У меня не было дома.</ta>
            <ta e="T7" id="Seg_1167" s="T4">Я жила у людей.</ta>
            <ta e="T12" id="Seg_1168" s="T7">Потом нам двоим дом дали.</ta>
            <ta e="T17" id="Seg_1169" s="T12">И я пришла в этот дом.</ta>
            <ta e="T22" id="Seg_1170" s="T17">А там такая грязь.</ta>
            <ta e="T26" id="Seg_1171" s="T22">Я четыре дня убирала.</ta>
            <ta e="T29" id="Seg_1172" s="T26">Как только люди жили.</ta>
            <ta e="T35" id="Seg_1173" s="T29">А мой сын только воды таскал.</ta>
            <ta e="T38" id="Seg_1174" s="T35">Я мою, мою.</ta>
            <ta e="T44" id="Seg_1175" s="T38">Потом у меня хорошо стало в избе.</ta>
            <ta e="T52" id="Seg_1176" s="T44">А я не люблю плохо, чтобы хорошо было бы.</ta>
            <ta e="T57" id="Seg_1177" s="T52">А теперь опять у меня дома нет.</ta>
            <ta e="T60" id="Seg_1178" s="T57">Опять у людей живу.</ta>
            <ta e="T63" id="Seg_1179" s="T60">Тут хорошо живу.</ta>
            <ta e="T68" id="Seg_1180" s="T63">Женщина хорошая, на меня не ругается.</ta>
            <ta e="T75" id="Seg_1181" s="T68">Я сама что хочу, то и делаю.</ta>
            <ta e="T79" id="Seg_1182" s="T75">“Избу каждый день мой”.</ta>
            <ta e="T83" id="Seg_1183" s="T79">Пол каждый день мою.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_1184" s="T1">у меня не было дома</ta>
            <ta e="T7" id="Seg_1185" s="T4">я жила у людей</ta>
            <ta e="T12" id="Seg_1186" s="T7">нам (мы двое) дом дали</ta>
            <ta e="T17" id="Seg_1187" s="T12">я пришла в этот дом</ta>
            <ta e="T22" id="Seg_1188" s="T17">а у них такая грязь</ta>
            <ta e="T26" id="Seg_1189" s="T22">я четыре дня убирала</ta>
            <ta e="T29" id="Seg_1190" s="T26">как только люди жили</ta>
            <ta e="T35" id="Seg_1191" s="T29">а мой сын только воды таскал</ta>
            <ta e="T38" id="Seg_1192" s="T35">я только мою</ta>
            <ta e="T44" id="Seg_1193" s="T38">потом у меня хорошо стало в избе</ta>
            <ta e="T52" id="Seg_1194" s="T44">я не люблю плохо чтобы хорошо было бы</ta>
            <ta e="T57" id="Seg_1195" s="T52">а теперь опять дома нет</ta>
            <ta e="T60" id="Seg_1196" s="T57">опять у людей живу</ta>
            <ta e="T63" id="Seg_1197" s="T60">тут хорошо живу</ta>
            <ta e="T68" id="Seg_1198" s="T63">женщина хорошая меня не ругает (не ругается)</ta>
            <ta e="T75" id="Seg_1199" s="T68">я сама чего хочу то и делаю</ta>
            <ta e="T79" id="Seg_1200" s="T75">в избе каждый день мой</ta>
            <ta e="T83" id="Seg_1201" s="T79">пол каждый день мою</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T38" id="Seg_1202" s="T35">[BrM:] Tentative analysis of 'müzulǯuguʒukan'.</ta>
            <ta e="T44" id="Seg_1203" s="T38">[BrM:] Locative form of 'sʼütʼdʼe' would be expected.</ta>
            <ta e="T52" id="Seg_1204" s="T44">[BrM:] Tentative analysis of 'jegi'.</ta>
            <ta e="T75" id="Seg_1205" s="T68">[BrM:] 3SG.S instead of 1SG.S? || 1SG?</ta>
            <ta e="T83" id="Seg_1206" s="T79">[BrM:] 3SG.S instead of 1SG.S? [KuAI:] Variant: 'mʼuzulǯɨku'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
