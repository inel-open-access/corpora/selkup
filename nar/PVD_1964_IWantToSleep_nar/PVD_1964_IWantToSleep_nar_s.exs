<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_IWantToSleep_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_IWantToSleep_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">34</ud-information>
            <ud-information attribute-name="# HIAT:w">24</ud-information>
            <ud-information attribute-name="# e">24</ud-information>
            <ud-information attribute-name="# HIAT:u">10</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T25" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Tan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">iːgɨ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">moltɨčaq</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Megga</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">nadə</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">qotduɣu</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">Ütdɨdʼal</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">alʼi</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">as</ts>
                  <nts id="Seg_35" n="HIAT:ip">?</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_38" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">Üsʼedʼat</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_44" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">Teper</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">as</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">moltɨčin</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_56" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_58" n="HIAT:w" s="T14">Qotdugu</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">nadə</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_65" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_67" n="HIAT:w" s="T16">Nu</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_70" n="HIAT:w" s="T17">qotdaq</ts>
                  <nts id="Seg_71" n="HIAT:ip">!</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_74" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_76" n="HIAT:w" s="T18">I</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_79" n="HIAT:w" s="T19">man</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_82" n="HIAT:w" s="T20">nej</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_85" n="HIAT:w" s="T21">qonneǯan</ts>
                  <nts id="Seg_86" n="HIAT:ip">.</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_89" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_91" n="HIAT:w" s="T22">Qotdatt</ts>
                  <nts id="Seg_92" n="HIAT:ip">?</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_95" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_97" n="HIAT:w" s="T23">Man</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_100" n="HIAT:w" s="T24">qotdan</ts>
                  <nts id="Seg_101" n="HIAT:ip">.</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T25" id="Seg_103" n="sc" s="T1">
               <ts e="T2" id="Seg_105" n="e" s="T1">Tan </ts>
               <ts e="T3" id="Seg_107" n="e" s="T2">iːgɨ </ts>
               <ts e="T4" id="Seg_109" n="e" s="T3">moltɨčaq. </ts>
               <ts e="T5" id="Seg_111" n="e" s="T4">Megga </ts>
               <ts e="T6" id="Seg_113" n="e" s="T5">nadə </ts>
               <ts e="T7" id="Seg_115" n="e" s="T6">qotduɣu. </ts>
               <ts e="T8" id="Seg_117" n="e" s="T7">Ütdɨdʼal </ts>
               <ts e="T9" id="Seg_119" n="e" s="T8">alʼi </ts>
               <ts e="T10" id="Seg_121" n="e" s="T9">as? </ts>
               <ts e="T11" id="Seg_123" n="e" s="T10">Üsʼedʼat. </ts>
               <ts e="T12" id="Seg_125" n="e" s="T11">Teper </ts>
               <ts e="T13" id="Seg_127" n="e" s="T12">as </ts>
               <ts e="T14" id="Seg_129" n="e" s="T13">moltɨčin. </ts>
               <ts e="T15" id="Seg_131" n="e" s="T14">Qotdugu </ts>
               <ts e="T16" id="Seg_133" n="e" s="T15">nadə. </ts>
               <ts e="T17" id="Seg_135" n="e" s="T16">Nu </ts>
               <ts e="T18" id="Seg_137" n="e" s="T17">qotdaq! </ts>
               <ts e="T19" id="Seg_139" n="e" s="T18">I </ts>
               <ts e="T20" id="Seg_141" n="e" s="T19">man </ts>
               <ts e="T21" id="Seg_143" n="e" s="T20">nej </ts>
               <ts e="T22" id="Seg_145" n="e" s="T21">qonneǯan. </ts>
               <ts e="T23" id="Seg_147" n="e" s="T22">Qotdatt? </ts>
               <ts e="T24" id="Seg_149" n="e" s="T23">Man </ts>
               <ts e="T25" id="Seg_151" n="e" s="T24">qotdan. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_152" s="T1">PVD_1964_IWantToSleep_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_153" s="T4">PVD_1964_IWantToSleep_nar.002 (001.002)</ta>
            <ta e="T10" id="Seg_154" s="T7">PVD_1964_IWantToSleep_nar.003 (001.003)</ta>
            <ta e="T11" id="Seg_155" s="T10">PVD_1964_IWantToSleep_nar.004 (001.004)</ta>
            <ta e="T14" id="Seg_156" s="T11">PVD_1964_IWantToSleep_nar.005 (001.005)</ta>
            <ta e="T16" id="Seg_157" s="T14">PVD_1964_IWantToSleep_nar.006 (001.006)</ta>
            <ta e="T18" id="Seg_158" s="T16">PVD_1964_IWantToSleep_nar.007 (001.007)</ta>
            <ta e="T22" id="Seg_159" s="T18">PVD_1964_IWantToSleep_nar.008 (001.008)</ta>
            <ta e="T23" id="Seg_160" s="T22">PVD_1964_IWantToSleep_nar.009 (001.009)</ta>
            <ta e="T25" id="Seg_161" s="T23">PVD_1964_IWantToSleep_nar.010 (001.010)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_162" s="T1">тан ӣ′гы ′молтытшаk.</ta>
            <ta e="T7" id="Seg_163" s="T4">мег̂га надъ ′kотдуɣу.</ta>
            <ta e="T10" id="Seg_164" s="T7">ӱ(ӧ)тды′дʼал алʼи ас?</ta>
            <ta e="T11" id="Seg_165" s="T10">ӱ′сʼедʼат.</ta>
            <ta e="T14" id="Seg_166" s="T11">те′пе̨р ас ′молтытшин.</ta>
            <ta e="T16" id="Seg_167" s="T14">′kотдугу надъ.</ta>
            <ta e="T18" id="Seg_168" s="T16">ну kот′даk!</ta>
            <ta e="T22" id="Seg_169" s="T18">и ма′н ней kонне′джан.</ta>
            <ta e="T23" id="Seg_170" s="T22">kот′дат(д̂)т?</ta>
            <ta e="T25" id="Seg_171" s="T23">ман ′kотдан.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_172" s="T1">tan iːgɨ moltɨtšaq.</ta>
            <ta e="T7" id="Seg_173" s="T4">meĝga nadə qotduɣu.</ta>
            <ta e="T10" id="Seg_174" s="T7">ü(ö)tdɨdʼal alʼi as?</ta>
            <ta e="T11" id="Seg_175" s="T10">üsʼedʼat.</ta>
            <ta e="T14" id="Seg_176" s="T11">teper as moltɨtšin.</ta>
            <ta e="T16" id="Seg_177" s="T14">qotdugu nadə.</ta>
            <ta e="T18" id="Seg_178" s="T16">nu qotdaq!</ta>
            <ta e="T22" id="Seg_179" s="T18">i man nej qonneǯan.</ta>
            <ta e="T23" id="Seg_180" s="T22">qotdat(d̂)t?</ta>
            <ta e="T25" id="Seg_181" s="T23">man qotdan.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_182" s="T1">Tan iːgɨ moltɨčaq. </ta>
            <ta e="T7" id="Seg_183" s="T4">Megga nadə qotduɣu. </ta>
            <ta e="T10" id="Seg_184" s="T7">Ütdɨdʼal alʼi as? </ta>
            <ta e="T11" id="Seg_185" s="T10">Üsʼedʼat. </ta>
            <ta e="T14" id="Seg_186" s="T11">Teper as moltɨčin. </ta>
            <ta e="T16" id="Seg_187" s="T14">Qotdugu nadə. </ta>
            <ta e="T18" id="Seg_188" s="T16">Nu qotdaq! </ta>
            <ta e="T22" id="Seg_189" s="T18">I man nej qonneǯan. </ta>
            <ta e="T23" id="Seg_190" s="T22">Qotdatt? </ta>
            <ta e="T25" id="Seg_191" s="T23">Man qotdan. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_192" s="T1">tat</ta>
            <ta e="T3" id="Seg_193" s="T2">iːgɨ</ta>
            <ta e="T4" id="Seg_194" s="T3">moltɨča-q</ta>
            <ta e="T5" id="Seg_195" s="T4">megga</ta>
            <ta e="T6" id="Seg_196" s="T5">nadə</ta>
            <ta e="T7" id="Seg_197" s="T6">qotdu-ɣu</ta>
            <ta e="T8" id="Seg_198" s="T7">ütdɨ-dʼa-l</ta>
            <ta e="T9" id="Seg_199" s="T8">alʼi</ta>
            <ta e="T10" id="Seg_200" s="T9">as</ta>
            <ta e="T11" id="Seg_201" s="T10">üsʼe-dʼa-t</ta>
            <ta e="T12" id="Seg_202" s="T11">teper</ta>
            <ta e="T13" id="Seg_203" s="T12">as</ta>
            <ta e="T14" id="Seg_204" s="T13">moltɨči-n</ta>
            <ta e="T15" id="Seg_205" s="T14">qotdu-gu</ta>
            <ta e="T16" id="Seg_206" s="T15">nadə</ta>
            <ta e="T17" id="Seg_207" s="T16">nu</ta>
            <ta e="T18" id="Seg_208" s="T17">qotda-q</ta>
            <ta e="T19" id="Seg_209" s="T18">i</ta>
            <ta e="T20" id="Seg_210" s="T19">man</ta>
            <ta e="T21" id="Seg_211" s="T20">nej</ta>
            <ta e="T22" id="Seg_212" s="T21">qonn-eǯa-n</ta>
            <ta e="T23" id="Seg_213" s="T22">qotda-tt</ta>
            <ta e="T24" id="Seg_214" s="T23">man</ta>
            <ta e="T25" id="Seg_215" s="T24">qotda-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_216" s="T1">tan</ta>
            <ta e="T3" id="Seg_217" s="T2">igə</ta>
            <ta e="T4" id="Seg_218" s="T3">moltoččə-kɨ</ta>
            <ta e="T5" id="Seg_219" s="T4">mekka</ta>
            <ta e="T6" id="Seg_220" s="T5">nadə</ta>
            <ta e="T7" id="Seg_221" s="T6">qondu-gu</ta>
            <ta e="T8" id="Seg_222" s="T7">ündɨ-dʼi-l</ta>
            <ta e="T9" id="Seg_223" s="T8">alʼi</ta>
            <ta e="T10" id="Seg_224" s="T9">asa</ta>
            <ta e="T11" id="Seg_225" s="T10">üsʼe-dʼi-t</ta>
            <ta e="T12" id="Seg_226" s="T11">teper</ta>
            <ta e="T13" id="Seg_227" s="T12">asa</ta>
            <ta e="T14" id="Seg_228" s="T13">moltoččə-n</ta>
            <ta e="T15" id="Seg_229" s="T14">qondu-gu</ta>
            <ta e="T16" id="Seg_230" s="T15">nadə</ta>
            <ta e="T17" id="Seg_231" s="T16">nu</ta>
            <ta e="T18" id="Seg_232" s="T17">qondu-kɨ</ta>
            <ta e="T19" id="Seg_233" s="T18">i</ta>
            <ta e="T20" id="Seg_234" s="T19">man</ta>
            <ta e="T21" id="Seg_235" s="T20">naj</ta>
            <ta e="T22" id="Seg_236" s="T21">qondu-enǯɨ-ŋ</ta>
            <ta e="T23" id="Seg_237" s="T22">qondu-ntə</ta>
            <ta e="T24" id="Seg_238" s="T23">man</ta>
            <ta e="T25" id="Seg_239" s="T24">qondu-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_240" s="T1">you.SG.NOM</ta>
            <ta e="T3" id="Seg_241" s="T2">NEG.IMP</ta>
            <ta e="T4" id="Seg_242" s="T3">tap-IMP.2SG.S</ta>
            <ta e="T5" id="Seg_243" s="T4">I.ALL</ta>
            <ta e="T6" id="Seg_244" s="T5">one.should</ta>
            <ta e="T7" id="Seg_245" s="T6">sleep-INF</ta>
            <ta e="T8" id="Seg_246" s="T7">hear-DRV-2SG.O</ta>
            <ta e="T9" id="Seg_247" s="T8">or</ta>
            <ta e="T10" id="Seg_248" s="T9">NEG</ta>
            <ta e="T11" id="Seg_249" s="T10">hear-DRV-3SG.O</ta>
            <ta e="T12" id="Seg_250" s="T11">now</ta>
            <ta e="T13" id="Seg_251" s="T12">NEG</ta>
            <ta e="T14" id="Seg_252" s="T13">tap-3SG.S</ta>
            <ta e="T15" id="Seg_253" s="T14">sleep-INF</ta>
            <ta e="T16" id="Seg_254" s="T15">one.should</ta>
            <ta e="T17" id="Seg_255" s="T16">now</ta>
            <ta e="T18" id="Seg_256" s="T17">sleep-IMP.2SG.S</ta>
            <ta e="T19" id="Seg_257" s="T18">and</ta>
            <ta e="T20" id="Seg_258" s="T19">I.NOM</ta>
            <ta e="T21" id="Seg_259" s="T20">also</ta>
            <ta e="T22" id="Seg_260" s="T21">sleep-FUT-1SG.S</ta>
            <ta e="T23" id="Seg_261" s="T22">sleep-2SG.S</ta>
            <ta e="T24" id="Seg_262" s="T23">I.NOM</ta>
            <ta e="T25" id="Seg_263" s="T24">sleep-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_264" s="T1">ты.NOM</ta>
            <ta e="T3" id="Seg_265" s="T2">NEG.IMP</ta>
            <ta e="T4" id="Seg_266" s="T3">постучать-IMP.2SG.S</ta>
            <ta e="T5" id="Seg_267" s="T4">я.ALL</ta>
            <ta e="T6" id="Seg_268" s="T5">надо</ta>
            <ta e="T7" id="Seg_269" s="T6">спать-INF</ta>
            <ta e="T8" id="Seg_270" s="T7">услышать-DRV-2SG.O</ta>
            <ta e="T9" id="Seg_271" s="T8">али</ta>
            <ta e="T10" id="Seg_272" s="T9">NEG</ta>
            <ta e="T11" id="Seg_273" s="T10">услышать-DRV-3SG.O</ta>
            <ta e="T12" id="Seg_274" s="T11">теперь</ta>
            <ta e="T13" id="Seg_275" s="T12">NEG</ta>
            <ta e="T14" id="Seg_276" s="T13">постучать-3SG.S</ta>
            <ta e="T15" id="Seg_277" s="T14">спать-INF</ta>
            <ta e="T16" id="Seg_278" s="T15">надо</ta>
            <ta e="T17" id="Seg_279" s="T16">ну</ta>
            <ta e="T18" id="Seg_280" s="T17">спать-IMP.2SG.S</ta>
            <ta e="T19" id="Seg_281" s="T18">и</ta>
            <ta e="T20" id="Seg_282" s="T19">я.NOM</ta>
            <ta e="T21" id="Seg_283" s="T20">тоже</ta>
            <ta e="T22" id="Seg_284" s="T21">спать-FUT-1SG.S</ta>
            <ta e="T23" id="Seg_285" s="T22">спать-2SG.S</ta>
            <ta e="T24" id="Seg_286" s="T23">я.NOM</ta>
            <ta e="T25" id="Seg_287" s="T24">спать-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_288" s="T1">pers</ta>
            <ta e="T3" id="Seg_289" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_290" s="T3">v-v:mood.pn</ta>
            <ta e="T5" id="Seg_291" s="T4">pers</ta>
            <ta e="T6" id="Seg_292" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_293" s="T6">v-v:inf</ta>
            <ta e="T8" id="Seg_294" s="T7">v-v&gt;v-v:pn</ta>
            <ta e="T9" id="Seg_295" s="T8">conj</ta>
            <ta e="T10" id="Seg_296" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_297" s="T10">v-v&gt;v-v:pn</ta>
            <ta e="T12" id="Seg_298" s="T11">adv</ta>
            <ta e="T13" id="Seg_299" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_300" s="T13">v-v:pn</ta>
            <ta e="T15" id="Seg_301" s="T14">v-v:inf</ta>
            <ta e="T16" id="Seg_302" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_303" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_304" s="T17">v-v:mood.pn</ta>
            <ta e="T19" id="Seg_305" s="T18">conj</ta>
            <ta e="T20" id="Seg_306" s="T19">pers</ta>
            <ta e="T21" id="Seg_307" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_308" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_309" s="T22">v-v:pn</ta>
            <ta e="T24" id="Seg_310" s="T23">pers</ta>
            <ta e="T25" id="Seg_311" s="T24">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_312" s="T1">pers</ta>
            <ta e="T3" id="Seg_313" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_314" s="T3">v</ta>
            <ta e="T5" id="Seg_315" s="T4">pers</ta>
            <ta e="T6" id="Seg_316" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_317" s="T6">v</ta>
            <ta e="T8" id="Seg_318" s="T7">v</ta>
            <ta e="T9" id="Seg_319" s="T8">conj</ta>
            <ta e="T10" id="Seg_320" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_321" s="T10">v</ta>
            <ta e="T12" id="Seg_322" s="T11">adv</ta>
            <ta e="T13" id="Seg_323" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_324" s="T13">v</ta>
            <ta e="T15" id="Seg_325" s="T14">v</ta>
            <ta e="T16" id="Seg_326" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_327" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_328" s="T17">v</ta>
            <ta e="T19" id="Seg_329" s="T18">conj</ta>
            <ta e="T20" id="Seg_330" s="T19">pers</ta>
            <ta e="T21" id="Seg_331" s="T20">n</ta>
            <ta e="T22" id="Seg_332" s="T21">v</ta>
            <ta e="T23" id="Seg_333" s="T22">v</ta>
            <ta e="T24" id="Seg_334" s="T23">pers</ta>
            <ta e="T25" id="Seg_335" s="T24">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_336" s="T1">pro.h:A</ta>
            <ta e="T7" id="Seg_337" s="T6">v:Th</ta>
            <ta e="T8" id="Seg_338" s="T7">0.2.h:E</ta>
            <ta e="T11" id="Seg_339" s="T10">0.3.h:E</ta>
            <ta e="T12" id="Seg_340" s="T11">adv:Time</ta>
            <ta e="T14" id="Seg_341" s="T13">0.3.h:A</ta>
            <ta e="T15" id="Seg_342" s="T14">v:Th</ta>
            <ta e="T18" id="Seg_343" s="T17">0.2.h:Th</ta>
            <ta e="T20" id="Seg_344" s="T19">pro.h:Th</ta>
            <ta e="T23" id="Seg_345" s="T22">0.2.h:Th</ta>
            <ta e="T24" id="Seg_346" s="T23">pro.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_347" s="T1">pro.h:S</ta>
            <ta e="T4" id="Seg_348" s="T3">v:pred</ta>
            <ta e="T6" id="Seg_349" s="T5">ptcl:pred</ta>
            <ta e="T7" id="Seg_350" s="T6">v:O</ta>
            <ta e="T8" id="Seg_351" s="T7">0.2.h:S v:pred</ta>
            <ta e="T11" id="Seg_352" s="T10">0.3.h:S v:pred</ta>
            <ta e="T14" id="Seg_353" s="T13">0.3.h:S v:pred</ta>
            <ta e="T15" id="Seg_354" s="T14">v:O</ta>
            <ta e="T16" id="Seg_355" s="T15">ptcl:pred</ta>
            <ta e="T18" id="Seg_356" s="T17">0.2.h:S v:pred</ta>
            <ta e="T20" id="Seg_357" s="T19">pro.h:S</ta>
            <ta e="T22" id="Seg_358" s="T21">v:pred</ta>
            <ta e="T23" id="Seg_359" s="T22">0.2.h:S v:pred</ta>
            <ta e="T24" id="Seg_360" s="T23">pro.h:S</ta>
            <ta e="T25" id="Seg_361" s="T24">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T6" id="Seg_362" s="T5">RUS:mod</ta>
            <ta e="T9" id="Seg_363" s="T8">RUS:gram</ta>
            <ta e="T12" id="Seg_364" s="T11">RUS:core</ta>
            <ta e="T16" id="Seg_365" s="T15">RUS:mod</ta>
            <ta e="T17" id="Seg_366" s="T16">RUS:disc</ta>
            <ta e="T19" id="Seg_367" s="T18">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_368" s="T1">Don't you tap.</ta>
            <ta e="T7" id="Seg_369" s="T4">I need to sleep.</ta>
            <ta e="T10" id="Seg_370" s="T7">Do you hear or not?</ta>
            <ta e="T11" id="Seg_371" s="T10">He heard me.</ta>
            <ta e="T14" id="Seg_372" s="T11">He doesn't tap any more.</ta>
            <ta e="T16" id="Seg_373" s="T14">One should sleep.</ta>
            <ta e="T18" id="Seg_374" s="T16">Well, sleep!</ta>
            <ta e="T22" id="Seg_375" s="T18">And I'll sleep as well.</ta>
            <ta e="T23" id="Seg_376" s="T22">Are you sleeping?</ta>
            <ta e="T25" id="Seg_377" s="T23">I am sleeping.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_378" s="T1">Klopf nicht.</ta>
            <ta e="T7" id="Seg_379" s="T4">Ich muss schlafen.</ta>
            <ta e="T10" id="Seg_380" s="T7">Hörst du oder nicht?</ta>
            <ta e="T11" id="Seg_381" s="T10">Er hat mich gehört.</ta>
            <ta e="T14" id="Seg_382" s="T11">Jetzt klopft er nicht mehr.</ta>
            <ta e="T16" id="Seg_383" s="T14">Man sollte schlafen.</ta>
            <ta e="T18" id="Seg_384" s="T16">Jetzt schlaf!</ta>
            <ta e="T22" id="Seg_385" s="T18">Und ich schlafe auch.</ta>
            <ta e="T23" id="Seg_386" s="T22">Schläfst du?</ta>
            <ta e="T25" id="Seg_387" s="T23">Ich schlafe.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_388" s="T1">Ты не стучи.</ta>
            <ta e="T7" id="Seg_389" s="T4">Мне надо спать.</ta>
            <ta e="T10" id="Seg_390" s="T7">Ты слышишь или нет?</ta>
            <ta e="T11" id="Seg_391" s="T10">Услышал.</ta>
            <ta e="T14" id="Seg_392" s="T11">Теперь не стучит.</ta>
            <ta e="T16" id="Seg_393" s="T14">Спать надо.</ta>
            <ta e="T18" id="Seg_394" s="T16">Ну спи!</ta>
            <ta e="T22" id="Seg_395" s="T18">И я тоже спать буду.</ta>
            <ta e="T23" id="Seg_396" s="T22">Спишь?</ta>
            <ta e="T25" id="Seg_397" s="T23">Я сплю.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_398" s="T1">ты не стукай там</ta>
            <ta e="T7" id="Seg_399" s="T4">мне надо спать</ta>
            <ta e="T10" id="Seg_400" s="T7">ты слышишь или нет</ta>
            <ta e="T11" id="Seg_401" s="T10">услыхал</ta>
            <ta e="T14" id="Seg_402" s="T11">теперь не стучит</ta>
            <ta e="T16" id="Seg_403" s="T14">спать надо</ta>
            <ta e="T18" id="Seg_404" s="T16">ну спи</ta>
            <ta e="T22" id="Seg_405" s="T18">и я тоже спать буду</ta>
            <ta e="T23" id="Seg_406" s="T22">спишь</ta>
            <ta e="T25" id="Seg_407" s="T23">я сплю</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T10" id="Seg_408" s="T7">[KuAI:] Variant: 'ötdɨdʼal'.</ta>
            <ta e="T23" id="Seg_409" s="T22">[KuAI:] Variant: 'qotdadt'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
