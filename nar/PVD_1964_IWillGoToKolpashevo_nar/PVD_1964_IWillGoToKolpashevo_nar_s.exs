<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_IWillGoToKolpashevo_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_IWillGoToKolpashevo_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">29</ud-information>
            <ud-information attribute-name="# HIAT:w">23</ud-information>
            <ud-information attribute-name="# e">23</ud-information>
            <ud-information attribute-name="# HIAT:u">5</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T24" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Täjeptugu</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">nadə</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">qardʼen</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">ku</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">qwanku</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_20" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">Man</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">qardʼen</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">qwaǯan</ts>
                  <nts id="Seg_29" n="HIAT:ip">,</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">Кalpašitdə</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_35" n="HIAT:w" s="T10">qwaǯan</ts>
                  <nts id="Seg_36" n="HIAT:ip">.</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_39" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">Qoːnär</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">qwaneǯan</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">mergu</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">i</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">merinäǯau</ts>
                  <nts id="Seg_54" n="HIAT:ip">.</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_57" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">Na</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">mesta</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">barana</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">i</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_71" n="HIAT:w" s="T20">meːrnau</ts>
                  <nts id="Seg_72" n="HIAT:ip">.</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_75" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_77" n="HIAT:w" s="T21">Man</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_80" n="HIAT:w" s="T22">teblam</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_83" n="HIAT:w" s="T23">aːlənnau</ts>
                  <nts id="Seg_84" n="HIAT:ip">.</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T24" id="Seg_86" n="sc" s="T1">
               <ts e="T2" id="Seg_88" n="e" s="T1">Täjeptugu </ts>
               <ts e="T3" id="Seg_90" n="e" s="T2">nadə </ts>
               <ts e="T4" id="Seg_92" n="e" s="T3">qardʼen </ts>
               <ts e="T5" id="Seg_94" n="e" s="T4">ku </ts>
               <ts e="T6" id="Seg_96" n="e" s="T5">qwanku. </ts>
               <ts e="T7" id="Seg_98" n="e" s="T6">Man </ts>
               <ts e="T8" id="Seg_100" n="e" s="T7">qardʼen </ts>
               <ts e="T9" id="Seg_102" n="e" s="T8">qwaǯan, </ts>
               <ts e="T10" id="Seg_104" n="e" s="T9">Кalpašitdə </ts>
               <ts e="T11" id="Seg_106" n="e" s="T10">qwaǯan. </ts>
               <ts e="T12" id="Seg_108" n="e" s="T11">Qoːnär </ts>
               <ts e="T13" id="Seg_110" n="e" s="T12">qwaneǯan </ts>
               <ts e="T14" id="Seg_112" n="e" s="T13">mergu </ts>
               <ts e="T15" id="Seg_114" n="e" s="T14">i </ts>
               <ts e="T16" id="Seg_116" n="e" s="T15">merinäǯau. </ts>
               <ts e="T17" id="Seg_118" n="e" s="T16">Na </ts>
               <ts e="T18" id="Seg_120" n="e" s="T17">mesta </ts>
               <ts e="T19" id="Seg_122" n="e" s="T18">barana </ts>
               <ts e="T20" id="Seg_124" n="e" s="T19">i </ts>
               <ts e="T21" id="Seg_126" n="e" s="T20">meːrnau. </ts>
               <ts e="T22" id="Seg_128" n="e" s="T21">Man </ts>
               <ts e="T23" id="Seg_130" n="e" s="T22">teblam </ts>
               <ts e="T24" id="Seg_132" n="e" s="T23">aːlənnau. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_133" s="T1">PVD_1964_IWillGoToKolpashevo_nar.001 (001.001)</ta>
            <ta e="T11" id="Seg_134" s="T6">PVD_1964_IWillGoToKolpashevo_nar.002 (001.002)</ta>
            <ta e="T16" id="Seg_135" s="T11">PVD_1964_IWillGoToKolpashevo_nar.003 (001.003)</ta>
            <ta e="T21" id="Seg_136" s="T16">PVD_1964_IWillGoToKolpashevo_nar.004 (001.004)</ta>
            <ta e="T24" id="Seg_137" s="T21">PVD_1964_IWillGoToKolpashevo_nar.005 (001.005)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_138" s="T1">тӓ′jептугу надъ kар′дʼен ку kwан′ку.</ta>
            <ta e="T11" id="Seg_139" s="T6">ман kар′дʼен kwа′джан, Калпашитдъ(ы) kwа′джан.</ta>
            <ta e="T16" id="Seg_140" s="T11">kо̄′нӓр kwа′не̨джан мер′г̂у и ′ме̨ри′нӓджау.</ta>
            <ta e="T21" id="Seg_141" s="T16">на ′места б̂а′рана и ме̄р′нау̹.</ta>
            <ta e="T24" id="Seg_142" s="T21">ман теб′лам ′а̄лъннау̹.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_143" s="T1">täjeptugu nadə qardʼen ku qwanku.</ta>
            <ta e="T11" id="Seg_144" s="T6">man qardʼen qwaǯan, Кalpašitdə(ɨ) qwaǯan.</ta>
            <ta e="T16" id="Seg_145" s="T11">qoːnär qwaneǯan merĝu i merinäǯau.</ta>
            <ta e="T21" id="Seg_146" s="T16">na mesta b̂arana i meːrnau̹.</ta>
            <ta e="T24" id="Seg_147" s="T21">man teblam aːlənnau̹.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_148" s="T1">Täjeptugu nadə qardʼen ku qwanku. </ta>
            <ta e="T11" id="Seg_149" s="T6">Man qardʼen qwaǯan, Кalpašitdə qwaǯan. </ta>
            <ta e="T16" id="Seg_150" s="T11">Qoːnär qwaneǯan mergu i merinäǯau. </ta>
            <ta e="T21" id="Seg_151" s="T16">Na mesta barana i meːrnau. </ta>
            <ta e="T24" id="Seg_152" s="T21">Man teblam aːlənnau. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_153" s="T1">täje-ptu-gu</ta>
            <ta e="T3" id="Seg_154" s="T2">nadə</ta>
            <ta e="T4" id="Seg_155" s="T3">qar-dʼe-n</ta>
            <ta e="T5" id="Seg_156" s="T4">ku</ta>
            <ta e="T6" id="Seg_157" s="T5">qwan-ku</ta>
            <ta e="T7" id="Seg_158" s="T6">man</ta>
            <ta e="T8" id="Seg_159" s="T7">qar-dʼe-n</ta>
            <ta e="T9" id="Seg_160" s="T8">qwa-ǯa-n</ta>
            <ta e="T10" id="Seg_161" s="T9">Кalpaši-tdə</ta>
            <ta e="T11" id="Seg_162" s="T10">qwa-ǯa-n</ta>
            <ta e="T12" id="Seg_163" s="T11">qoːnär</ta>
            <ta e="T13" id="Seg_164" s="T12">qwan-eǯa-n</ta>
            <ta e="T14" id="Seg_165" s="T13">mer-gu</ta>
            <ta e="T15" id="Seg_166" s="T14">i</ta>
            <ta e="T16" id="Seg_167" s="T15">merin-äǯa-u</ta>
            <ta e="T21" id="Seg_168" s="T20">meːr-na-u</ta>
            <ta e="T22" id="Seg_169" s="T21">man</ta>
            <ta e="T23" id="Seg_170" s="T22">teb-la-m</ta>
            <ta e="T24" id="Seg_171" s="T23">aːlə-nna-u</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_172" s="T1">teje-ptɨ-gu</ta>
            <ta e="T3" id="Seg_173" s="T2">nadə</ta>
            <ta e="T4" id="Seg_174" s="T3">qare-dʼel-n</ta>
            <ta e="T5" id="Seg_175" s="T4">kuː</ta>
            <ta e="T6" id="Seg_176" s="T5">qwan-gu</ta>
            <ta e="T7" id="Seg_177" s="T6">man</ta>
            <ta e="T8" id="Seg_178" s="T7">qare-dʼel-n</ta>
            <ta e="T9" id="Seg_179" s="T8">qwan-enǯɨ-ŋ</ta>
            <ta e="T10" id="Seg_180" s="T9">Kolpaše-ntə</ta>
            <ta e="T11" id="Seg_181" s="T10">qwan-enǯɨ-ŋ</ta>
            <ta e="T12" id="Seg_182" s="T11">qoner</ta>
            <ta e="T13" id="Seg_183" s="T12">qwan-enǯɨ-ŋ</ta>
            <ta e="T14" id="Seg_184" s="T13">mer-gu</ta>
            <ta e="T15" id="Seg_185" s="T14">i</ta>
            <ta e="T16" id="Seg_186" s="T15">merɨŋ-enǯɨ-w</ta>
            <ta e="T21" id="Seg_187" s="T20">mer-nɨ-w</ta>
            <ta e="T22" id="Seg_188" s="T21">man</ta>
            <ta e="T23" id="Seg_189" s="T22">täp-la-m</ta>
            <ta e="T24" id="Seg_190" s="T23">aːlu-nɨ-w</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_191" s="T1">think-CAUS-INF</ta>
            <ta e="T3" id="Seg_192" s="T2">one.should</ta>
            <ta e="T4" id="Seg_193" s="T3">morning-day-ADV.LOC</ta>
            <ta e="T5" id="Seg_194" s="T4">where</ta>
            <ta e="T6" id="Seg_195" s="T5">leave-INF</ta>
            <ta e="T7" id="Seg_196" s="T6">I.NOM</ta>
            <ta e="T8" id="Seg_197" s="T7">morning-day-ADV.LOC</ta>
            <ta e="T9" id="Seg_198" s="T8">leave-FUT-1SG.S</ta>
            <ta e="T10" id="Seg_199" s="T9">Kolpashevo-ILL</ta>
            <ta e="T11" id="Seg_200" s="T10">leave-FUT-1SG.S</ta>
            <ta e="T12" id="Seg_201" s="T11">sheep.[NOM]</ta>
            <ta e="T13" id="Seg_202" s="T12">leave-FUT-1SG.S</ta>
            <ta e="T14" id="Seg_203" s="T13">sell-INF</ta>
            <ta e="T15" id="Seg_204" s="T14">and</ta>
            <ta e="T16" id="Seg_205" s="T15">sell-FUT-1SG.O</ta>
            <ta e="T21" id="Seg_206" s="T20">sell-CO-1SG.O</ta>
            <ta e="T22" id="Seg_207" s="T21">I.NOM</ta>
            <ta e="T23" id="Seg_208" s="T22">(s)he-PL-ACC</ta>
            <ta e="T24" id="Seg_209" s="T23">cheat-CO-1SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_210" s="T1">думать-CAUS-INF</ta>
            <ta e="T3" id="Seg_211" s="T2">надо</ta>
            <ta e="T4" id="Seg_212" s="T3">утро-день-ADV.LOC</ta>
            <ta e="T5" id="Seg_213" s="T4">куда</ta>
            <ta e="T6" id="Seg_214" s="T5">отправиться-INF</ta>
            <ta e="T7" id="Seg_215" s="T6">я.NOM</ta>
            <ta e="T8" id="Seg_216" s="T7">утро-день-ADV.LOC</ta>
            <ta e="T9" id="Seg_217" s="T8">отправиться-FUT-1SG.S</ta>
            <ta e="T10" id="Seg_218" s="T9">Колпашево-ILL</ta>
            <ta e="T11" id="Seg_219" s="T10">отправиться-FUT-1SG.S</ta>
            <ta e="T12" id="Seg_220" s="T11">овца.[NOM]</ta>
            <ta e="T13" id="Seg_221" s="T12">отправиться-FUT-1SG.S</ta>
            <ta e="T14" id="Seg_222" s="T13">продать-INF</ta>
            <ta e="T15" id="Seg_223" s="T14">и</ta>
            <ta e="T16" id="Seg_224" s="T15">продать-FUT-1SG.O</ta>
            <ta e="T21" id="Seg_225" s="T20">продать-CO-1SG.O</ta>
            <ta e="T22" id="Seg_226" s="T21">я.NOM</ta>
            <ta e="T23" id="Seg_227" s="T22">он(а)-PL-ACC</ta>
            <ta e="T24" id="Seg_228" s="T23">обмануть-CO-1SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_229" s="T1">v-v&gt;v-v:inf</ta>
            <ta e="T3" id="Seg_230" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_231" s="T3">n-n-adv:case</ta>
            <ta e="T5" id="Seg_232" s="T4">interrog</ta>
            <ta e="T6" id="Seg_233" s="T5">v-v:inf</ta>
            <ta e="T7" id="Seg_234" s="T6">pers</ta>
            <ta e="T8" id="Seg_235" s="T7">n-n-adv:case</ta>
            <ta e="T9" id="Seg_236" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_237" s="T9">nprop-n:case</ta>
            <ta e="T11" id="Seg_238" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_239" s="T11">n.[n:case]</ta>
            <ta e="T13" id="Seg_240" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_241" s="T13">v-v:inf</ta>
            <ta e="T15" id="Seg_242" s="T14">conj</ta>
            <ta e="T16" id="Seg_243" s="T15">v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_244" s="T20">v-v:ins-v:pn</ta>
            <ta e="T22" id="Seg_245" s="T21">pers</ta>
            <ta e="T23" id="Seg_246" s="T22">pers-n:num-n:case</ta>
            <ta e="T24" id="Seg_247" s="T23">v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_248" s="T1">v</ta>
            <ta e="T3" id="Seg_249" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_250" s="T3">adv</ta>
            <ta e="T5" id="Seg_251" s="T4">interrog</ta>
            <ta e="T6" id="Seg_252" s="T5">v</ta>
            <ta e="T7" id="Seg_253" s="T6">pers</ta>
            <ta e="T8" id="Seg_254" s="T7">adv</ta>
            <ta e="T9" id="Seg_255" s="T8">v</ta>
            <ta e="T10" id="Seg_256" s="T9">nprop</ta>
            <ta e="T11" id="Seg_257" s="T10">v</ta>
            <ta e="T12" id="Seg_258" s="T11">n</ta>
            <ta e="T13" id="Seg_259" s="T12">v</ta>
            <ta e="T14" id="Seg_260" s="T13">v</ta>
            <ta e="T15" id="Seg_261" s="T14">conj</ta>
            <ta e="T16" id="Seg_262" s="T15">v</ta>
            <ta e="T21" id="Seg_263" s="T20">v</ta>
            <ta e="T22" id="Seg_264" s="T21">pers</ta>
            <ta e="T23" id="Seg_265" s="T22">pers</ta>
            <ta e="T24" id="Seg_266" s="T23">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_267" s="T1">v:Th</ta>
            <ta e="T4" id="Seg_268" s="T3">adv:Time</ta>
            <ta e="T5" id="Seg_269" s="T4">adv:G</ta>
            <ta e="T7" id="Seg_270" s="T6">pro.h:A</ta>
            <ta e="T8" id="Seg_271" s="T7">adv:Time</ta>
            <ta e="T10" id="Seg_272" s="T9">np:G</ta>
            <ta e="T11" id="Seg_273" s="T10">0.1.h:A</ta>
            <ta e="T12" id="Seg_274" s="T11">np:Th</ta>
            <ta e="T13" id="Seg_275" s="T12">0.1.h:A</ta>
            <ta e="T16" id="Seg_276" s="T15">0.1.h:A 0.3:Th</ta>
            <ta e="T21" id="Seg_277" s="T20">0.1.h:A 0.3:Th</ta>
            <ta e="T22" id="Seg_278" s="T21">pro.h:A</ta>
            <ta e="T23" id="Seg_279" s="T22">pro.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_280" s="T1">v:O</ta>
            <ta e="T3" id="Seg_281" s="T2">ptcl:pred</ta>
            <ta e="T6" id="Seg_282" s="T3">s:compl</ta>
            <ta e="T7" id="Seg_283" s="T6">pro.h:S</ta>
            <ta e="T9" id="Seg_284" s="T8">v:pred</ta>
            <ta e="T11" id="Seg_285" s="T10">0.1.h:S v:pred</ta>
            <ta e="T13" id="Seg_286" s="T12">0.1.h:S v:pred</ta>
            <ta e="T14" id="Seg_287" s="T13">s:purp</ta>
            <ta e="T16" id="Seg_288" s="T15">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T21" id="Seg_289" s="T20">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T22" id="Seg_290" s="T21">pro.h:S</ta>
            <ta e="T23" id="Seg_291" s="T22">pro.h:O</ta>
            <ta e="T24" id="Seg_292" s="T23">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_293" s="T2">RUS:mod</ta>
            <ta e="T10" id="Seg_294" s="T9">RUS:cult</ta>
            <ta e="T15" id="Seg_295" s="T14">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_296" s="T1">I have to think, where I should go tomorrow.</ta>
            <ta e="T11" id="Seg_297" s="T6">Tomorrow I'll go to Kolpashevo.</ta>
            <ta e="T16" id="Seg_298" s="T11">I'll take a sheep away to sell it and I'll sell it.</ta>
            <ta e="T21" id="Seg_299" s="T16">I sold [the sheep] as a ram.</ta>
            <ta e="T24" id="Seg_300" s="T21">I deceived them.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_301" s="T1">Ich muss überlegen, wo ich morgen hingehe.</ta>
            <ta e="T11" id="Seg_302" s="T6">Morgen gehe ich nach Kolpashevo.</ta>
            <ta e="T16" id="Seg_303" s="T11">Ich nehme ein Schaf zum Verkaufen mit und werde es verkaufen.</ta>
            <ta e="T21" id="Seg_304" s="T16">Ich verkaufte [das Schaf] als Bock.</ta>
            <ta e="T24" id="Seg_305" s="T21">Ich habe sie betrogen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_306" s="T1">Подумать надо, завтра куда пойти.</ta>
            <ta e="T11" id="Seg_307" s="T6">Я завтра поеду, в Колпашево поеду.</ta>
            <ta e="T16" id="Seg_308" s="T11">Овечку увезу продавать и продам.</ta>
            <ta e="T21" id="Seg_309" s="T16">Вместо барана и продала [овечку].</ta>
            <ta e="T24" id="Seg_310" s="T21">Я их обманула.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_311" s="T1">завтра надо подумать куда пойти</ta>
            <ta e="T11" id="Seg_312" s="T6">я завтра поеду в Колпашево поеду</ta>
            <ta e="T16" id="Seg_313" s="T11">овечку увезу продавать и продам</ta>
            <ta e="T21" id="Seg_314" s="T16">и вместо барана продала (овечку)</ta>
            <ta e="T24" id="Seg_315" s="T21">я обманула их</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T11" id="Seg_316" s="T6">[KuAI:] Variant: 'Кalpašitdɨ'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
