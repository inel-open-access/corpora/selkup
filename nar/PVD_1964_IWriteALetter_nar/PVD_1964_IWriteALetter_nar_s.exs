<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_IWriteALetter_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_IWriteALetter_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">18</ud-information>
            <ud-information attribute-name="# HIAT:w">14</ud-information>
            <ud-information attribute-name="# e">14</ud-information>
            <ud-information attribute-name="# HIAT:u">4</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T15" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Nagɨrgu</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">nadə</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">nagɨrɨm</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Man</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">nagɨrčeǯau</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">nanʼnʼaɣən</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">Puskaj</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">kartopkam</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">wes</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">moːrenɨmdə</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">roskalɨm</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_44" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">Ato</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">ibagan</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">orɨpbattɨ</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T15" id="Seg_55" n="sc" s="T1">
               <ts e="T2" id="Seg_57" n="e" s="T1">Nagɨrgu </ts>
               <ts e="T3" id="Seg_59" n="e" s="T2">nadə </ts>
               <ts e="T4" id="Seg_61" n="e" s="T3">nagɨrɨm. </ts>
               <ts e="T5" id="Seg_63" n="e" s="T4">Man </ts>
               <ts e="T6" id="Seg_65" n="e" s="T5">nagɨrčeǯau </ts>
               <ts e="T7" id="Seg_67" n="e" s="T6">nanʼnʼaɣən. </ts>
               <ts e="T8" id="Seg_69" n="e" s="T7">Puskaj </ts>
               <ts e="T9" id="Seg_71" n="e" s="T8">kartopkam </ts>
               <ts e="T10" id="Seg_73" n="e" s="T9">wes </ts>
               <ts e="T11" id="Seg_75" n="e" s="T10">moːrenɨmdə </ts>
               <ts e="T12" id="Seg_77" n="e" s="T11">roskalɨm. </ts>
               <ts e="T13" id="Seg_79" n="e" s="T12">Ato </ts>
               <ts e="T14" id="Seg_81" n="e" s="T13">ibagan </ts>
               <ts e="T15" id="Seg_83" n="e" s="T14">orɨpbattɨ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_84" s="T1">PVD_1964_IWriteALetter_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_85" s="T4">PVD_1964_IWriteALetter_nar.002 (001.002)</ta>
            <ta e="T12" id="Seg_86" s="T7">PVD_1964_IWriteALetter_nar.003 (001.003)</ta>
            <ta e="T15" id="Seg_87" s="T12">PVD_1964_IWriteALetter_nar.004 (001.004)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_88" s="T1">нагы(у)р′гу надъ ′нагырым.</ta>
            <ta e="T7" id="Seg_89" s="T4">ман ′нагыр тшеджау на′нʼнʼаɣън.</ta>
            <ta e="T12" id="Seg_90" s="T7">пу′скай кар′топкам вес мо̄ре̨нымдъ ′рос(с)калым.</ta>
            <ta e="T15" id="Seg_91" s="T12">а то ′иба(ъ)ган орыпбатты.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_92" s="T1">nagɨ(u)rgu nadə nagɨrɨm.</ta>
            <ta e="T7" id="Seg_93" s="T4">man nagɨr tšeǯau nanʼnʼaɣən.</ta>
            <ta e="T12" id="Seg_94" s="T7">puskaj kartopkam wes moːrenɨmdə ros(s)kalɨm.</ta>
            <ta e="T15" id="Seg_95" s="T12">a to iba(ə)gan orɨpbattɨ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_96" s="T1">Nagɨrgu nadə nagɨrɨm. </ta>
            <ta e="T7" id="Seg_97" s="T4">Man nagɨrčeǯau nanʼnʼaɣən. </ta>
            <ta e="T12" id="Seg_98" s="T7">Puskaj kartopkam wes moːrenɨmdə roskalɨm. </ta>
            <ta e="T15" id="Seg_99" s="T12">Ato ibagan orɨpbattɨ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_100" s="T1">nagɨr-gu</ta>
            <ta e="T3" id="Seg_101" s="T2">nadə</ta>
            <ta e="T4" id="Seg_102" s="T3">nagɨr-ɨ-m</ta>
            <ta e="T5" id="Seg_103" s="T4">man</ta>
            <ta e="T6" id="Seg_104" s="T5">nagɨr-č-eǯa-u</ta>
            <ta e="T7" id="Seg_105" s="T6">nanʼnʼa-ɣən</ta>
            <ta e="T8" id="Seg_106" s="T7">puskaj</ta>
            <ta e="T9" id="Seg_107" s="T8">kartopka-m</ta>
            <ta e="T10" id="Seg_108" s="T9">wes</ta>
            <ta e="T11" id="Seg_109" s="T10">moːre-nɨ-mdə</ta>
            <ta e="T12" id="Seg_110" s="T11">roska-lɨ-m</ta>
            <ta e="T13" id="Seg_111" s="T12">ato</ta>
            <ta e="T14" id="Seg_112" s="T13">ibaga-n</ta>
            <ta e="T15" id="Seg_113" s="T14">orɨp-ba-ttɨ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_114" s="T1">nagər-gu</ta>
            <ta e="T3" id="Seg_115" s="T2">nadə</ta>
            <ta e="T4" id="Seg_116" s="T3">nagər-ɨ-m</ta>
            <ta e="T5" id="Seg_117" s="T4">man</ta>
            <ta e="T6" id="Seg_118" s="T5">nagər-ču-enǯɨ-w</ta>
            <ta e="T7" id="Seg_119" s="T6">nanʼa-qɨn</ta>
            <ta e="T8" id="Seg_120" s="T7">puskaj</ta>
            <ta e="T9" id="Seg_121" s="T8">kartoška-m</ta>
            <ta e="T10" id="Seg_122" s="T9">wesʼ</ta>
            <ta e="T11" id="Seg_123" s="T10">morel-nɨ-mtə</ta>
            <ta e="T12" id="Seg_124" s="T11">roska-la-m</ta>
            <ta e="T13" id="Seg_125" s="T12">ato</ta>
            <ta e="T14" id="Seg_126" s="T13">iːbəgaj-ŋ</ta>
            <ta e="T15" id="Seg_127" s="T14">orɨm-mbɨ-tɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_128" s="T1">write-INF</ta>
            <ta e="T3" id="Seg_129" s="T2">one.should</ta>
            <ta e="T4" id="Seg_130" s="T3">letter-EP-ACC</ta>
            <ta e="T5" id="Seg_131" s="T4">I.NOM</ta>
            <ta e="T6" id="Seg_132" s="T5">letter-VBLZ-FUT-1SG.O</ta>
            <ta e="T7" id="Seg_133" s="T6">sister-LOC</ta>
            <ta e="T8" id="Seg_134" s="T7">JUSS</ta>
            <ta e="T9" id="Seg_135" s="T8">potato-ACC</ta>
            <ta e="T10" id="Seg_136" s="T9">all</ta>
            <ta e="T11" id="Seg_137" s="T10">break.off-CO-IMP.3SG/PL.O</ta>
            <ta e="T12" id="Seg_138" s="T11">sprout-PL-ACC</ta>
            <ta e="T13" id="Seg_139" s="T12">otherwise</ta>
            <ta e="T14" id="Seg_140" s="T13">big-ADVZ</ta>
            <ta e="T15" id="Seg_141" s="T14">grow.up-PST.NAR-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_142" s="T1">написать-INF</ta>
            <ta e="T3" id="Seg_143" s="T2">надо</ta>
            <ta e="T4" id="Seg_144" s="T3">письмо-EP-ACC</ta>
            <ta e="T5" id="Seg_145" s="T4">я.NOM</ta>
            <ta e="T6" id="Seg_146" s="T5">письмо-VBLZ-FUT-1SG.O</ta>
            <ta e="T7" id="Seg_147" s="T6">сестра-LOC</ta>
            <ta e="T8" id="Seg_148" s="T7">JUSS</ta>
            <ta e="T9" id="Seg_149" s="T8">картошка-ACC</ta>
            <ta e="T10" id="Seg_150" s="T9">весь</ta>
            <ta e="T11" id="Seg_151" s="T10">наломать-CO-IMP.3SG/PL.O</ta>
            <ta e="T12" id="Seg_152" s="T11">росток-PL-ACC</ta>
            <ta e="T13" id="Seg_153" s="T12">а.то</ta>
            <ta e="T14" id="Seg_154" s="T13">большой-ADVZ</ta>
            <ta e="T15" id="Seg_155" s="T14">вырасти-PST.NAR-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_156" s="T1">v-v:inf</ta>
            <ta e="T3" id="Seg_157" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_158" s="T3">n-n:ins-n:case</ta>
            <ta e="T5" id="Seg_159" s="T4">pers</ta>
            <ta e="T6" id="Seg_160" s="T5">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_161" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_162" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_163" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_164" s="T9">quant</ta>
            <ta e="T11" id="Seg_165" s="T10">v-v:ins-v:mood.pn</ta>
            <ta e="T12" id="Seg_166" s="T11">n-n:num-n:case</ta>
            <ta e="T13" id="Seg_167" s="T12">conj</ta>
            <ta e="T14" id="Seg_168" s="T13">adj-adj&gt;adv</ta>
            <ta e="T15" id="Seg_169" s="T14">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_170" s="T1">v</ta>
            <ta e="T3" id="Seg_171" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_172" s="T3">n</ta>
            <ta e="T5" id="Seg_173" s="T4">pers</ta>
            <ta e="T6" id="Seg_174" s="T5">v</ta>
            <ta e="T7" id="Seg_175" s="T6">n</ta>
            <ta e="T8" id="Seg_176" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_177" s="T8">n</ta>
            <ta e="T10" id="Seg_178" s="T9">quant</ta>
            <ta e="T11" id="Seg_179" s="T10">v</ta>
            <ta e="T12" id="Seg_180" s="T11">n</ta>
            <ta e="T13" id="Seg_181" s="T12">conj</ta>
            <ta e="T14" id="Seg_182" s="T13">adj</ta>
            <ta e="T15" id="Seg_183" s="T14">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_184" s="T1">v:Th</ta>
            <ta e="T4" id="Seg_185" s="T3">np:Th</ta>
            <ta e="T5" id="Seg_186" s="T4">pro.h:A</ta>
            <ta e="T6" id="Seg_187" s="T5">0.3:Th</ta>
            <ta e="T7" id="Seg_188" s="T6">np.h:R</ta>
            <ta e="T11" id="Seg_189" s="T10">0.3.h:A</ta>
            <ta e="T12" id="Seg_190" s="T11">np:P</ta>
            <ta e="T15" id="Seg_191" s="T14">0.3:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_192" s="T1">v:O</ta>
            <ta e="T3" id="Seg_193" s="T2">ptcl:pred</ta>
            <ta e="T5" id="Seg_194" s="T4">pro.h:S</ta>
            <ta e="T6" id="Seg_195" s="T5">v:pred 0.3:O</ta>
            <ta e="T11" id="Seg_196" s="T10">0.3.h:S v:pred</ta>
            <ta e="T12" id="Seg_197" s="T11">np:O</ta>
            <ta e="T15" id="Seg_198" s="T14">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_199" s="T2">RUS:mod</ta>
            <ta e="T8" id="Seg_200" s="T7">RUS:gram</ta>
            <ta e="T9" id="Seg_201" s="T8">RUS:cult</ta>
            <ta e="T10" id="Seg_202" s="T9">RUS:core</ta>
            <ta e="T12" id="Seg_203" s="T11">RUS:cult</ta>
            <ta e="T13" id="Seg_204" s="T12">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_205" s="T1">[I] need to write a letter.</ta>
            <ta e="T7" id="Seg_206" s="T4">I'll write to my younger sister.</ta>
            <ta e="T12" id="Seg_207" s="T7">She should break off all the sprouts.</ta>
            <ta e="T15" id="Seg_208" s="T12">Because they grew big.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_209" s="T1">[Ich] muss einen Brief schreiben.</ta>
            <ta e="T7" id="Seg_210" s="T4">Ich schreibe meiner jüngeren Schwester.</ta>
            <ta e="T12" id="Seg_211" s="T7">Sie muss alle Triebe abbrechen.</ta>
            <ta e="T15" id="Seg_212" s="T12">Weil sie sonst groß werden.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_213" s="T1">Написать надо письмо.</ta>
            <ta e="T7" id="Seg_214" s="T4">Я напишу младшей сестре.</ta>
            <ta e="T12" id="Seg_215" s="T7">Пускай у картошки все обломает ростки.</ta>
            <ta e="T15" id="Seg_216" s="T12">А то большими выросли.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_217" s="T1">написать надо письмо</ta>
            <ta e="T7" id="Seg_218" s="T4">я напишу младшей сестре</ta>
            <ta e="T12" id="Seg_219" s="T7">пусть картошки все обломает ростки</ta>
            <ta e="T15" id="Seg_220" s="T12">а то большие (ростки) выросли</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T4" id="Seg_221" s="T1">[KuAI:] Variant: 'nagurgu'.</ta>
            <ta e="T7" id="Seg_222" s="T4">[KuAI:] na′nʼnʼä - younger sister. [BrM:] 1) 'nagɨr čeǯau' changed to 'nagɨrčeǯau'. 2) LOC?</ta>
            <ta e="T12" id="Seg_223" s="T7">[KuAI:] Variant: 'rosskalɨm'.</ta>
            <ta e="T15" id="Seg_224" s="T12">[KuAI:] Variant: 'ibəgan'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T7" id="Seg_225" s="T4">на′нʼнʼӓ - младшая сестра</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
