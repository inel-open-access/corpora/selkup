<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Ignashka_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Ignashka_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">24</ud-information>
            <ud-information attribute-name="# HIAT:w">15</ud-information>
            <ud-information attribute-name="# e">15</ud-information>
            <ud-information attribute-name="# HIAT:u">4</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T16" id="Seg_0" n="sc" s="T1">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">A</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">Ignaška</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">as</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">kɨgan</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">artʼelʼaɣɨn</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_19" n="HIAT:w" s="T6">jegu</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_23" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">Tebɨm</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">to</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">jelʼe</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">wɨklʼüčʼilʼi</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_38" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">Tʼäratta</ts>
                  <nts id="Seg_41" n="HIAT:ip">:</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_43" n="HIAT:ip">“</nts>
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">Кalpašɨt</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">qwaǯɨːn</ts>
                  <nts id="Seg_49" n="HIAT:ip">.</nts>
                  <nts id="Seg_50" n="HIAT:ip">”</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_53" n="HIAT:u" s="T14">
                  <nts id="Seg_54" n="HIAT:ip">“</nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">Toː</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">qwaǯan</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip">”</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T16" id="Seg_63" n="sc" s="T1">
               <ts e="T2" id="Seg_65" n="e" s="T1">A </ts>
               <ts e="T3" id="Seg_67" n="e" s="T2">Ignaška </ts>
               <ts e="T4" id="Seg_69" n="e" s="T3">as </ts>
               <ts e="T5" id="Seg_71" n="e" s="T4">kɨgan </ts>
               <ts e="T6" id="Seg_73" n="e" s="T5">artʼelʼaɣɨn </ts>
               <ts e="T7" id="Seg_75" n="e" s="T6">jegu. </ts>
               <ts e="T8" id="Seg_77" n="e" s="T7">Tebɨm </ts>
               <ts e="T9" id="Seg_79" n="e" s="T8">to </ts>
               <ts e="T10" id="Seg_81" n="e" s="T9">jelʼe </ts>
               <ts e="T11" id="Seg_83" n="e" s="T10">wɨklʼüčʼilʼi. </ts>
               <ts e="T12" id="Seg_85" n="e" s="T11">Tʼäratta: </ts>
               <ts e="T13" id="Seg_87" n="e" s="T12">“Кalpašɨt </ts>
               <ts e="T14" id="Seg_89" n="e" s="T13">qwaǯɨːn.” </ts>
               <ts e="T15" id="Seg_91" n="e" s="T14">“Toː </ts>
               <ts e="T16" id="Seg_93" n="e" s="T15">qwaǯan.” </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_94" s="T1">PVD_1964_Ignashka_nar.001 (001.001)</ta>
            <ta e="T11" id="Seg_95" s="T7">PVD_1964_Ignashka_nar.002 (001.002)</ta>
            <ta e="T14" id="Seg_96" s="T11">PVD_1964_Ignashka_nar.003 (001.003)</ta>
            <ta e="T16" id="Seg_97" s="T14">PVD_1964_Ignashka_nar.004 (001.004)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T7" id="Seg_98" s="T1">а Иг′нашка ас кы′ган артʼелʼаɣын ′jегу.</ta>
            <ta e="T11" id="Seg_99" s="T7">тебым ′тоjелʼе выклʼючʼилʼи.</ta>
            <ta e="T14" id="Seg_100" s="T11">тʼӓ′ратта, Калпашыт ′kwаджы̄н.</ta>
            <ta e="T16" id="Seg_101" s="T14">то̄ kwа′джан.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T7" id="Seg_102" s="T1">a Иgnaška as kɨgan artʼelʼaɣɨn jegu.</ta>
            <ta e="T11" id="Seg_103" s="T7">tebɨm tojelʼe wɨklʼюčʼilʼi.</ta>
            <ta e="T14" id="Seg_104" s="T11">tʼäratta, Кalpašɨt qwaǯɨːn.</ta>
            <ta e="T16" id="Seg_105" s="T14">toː qwaǯan.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_106" s="T1">A Ignaška as kɨgan artʼelʼaɣɨn jegu. </ta>
            <ta e="T11" id="Seg_107" s="T7">Tebɨm to jelʼe wɨklʼüčʼilʼi. </ta>
            <ta e="T14" id="Seg_108" s="T11">Tʼäratta: “Кalpašɨt qwaǯɨːn.” </ta>
            <ta e="T16" id="Seg_109" s="T14">“Toː qwaǯan.” </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_110" s="T1">a</ta>
            <ta e="T3" id="Seg_111" s="T2">Ignaška</ta>
            <ta e="T4" id="Seg_112" s="T3">as</ta>
            <ta e="T5" id="Seg_113" s="T4">kɨga-n</ta>
            <ta e="T6" id="Seg_114" s="T5">artʼelʼ-a-ɣɨn</ta>
            <ta e="T7" id="Seg_115" s="T6">je-gu</ta>
            <ta e="T8" id="Seg_116" s="T7">teb-ɨ-m</ta>
            <ta e="T9" id="Seg_117" s="T8">to</ta>
            <ta e="T10" id="Seg_118" s="T9">je-lʼe</ta>
            <ta e="T12" id="Seg_119" s="T11">tʼära-tta</ta>
            <ta e="T13" id="Seg_120" s="T12">Кalpašɨ-t</ta>
            <ta e="T14" id="Seg_121" s="T13">qwa-ǯɨː-n</ta>
            <ta e="T15" id="Seg_122" s="T14">toː</ta>
            <ta e="T16" id="Seg_123" s="T15">qwa-ǯa-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_124" s="T1">a</ta>
            <ta e="T3" id="Seg_125" s="T2">Ignaška</ta>
            <ta e="T4" id="Seg_126" s="T3">asa</ta>
            <ta e="T5" id="Seg_127" s="T4">kɨgɨ-ŋ</ta>
            <ta e="T6" id="Seg_128" s="T5">artʼelʼ-ɨ-qɨn</ta>
            <ta e="T7" id="Seg_129" s="T6">eː-gu</ta>
            <ta e="T8" id="Seg_130" s="T7">täp-ɨ-m</ta>
            <ta e="T9" id="Seg_131" s="T8">to</ta>
            <ta e="T10" id="Seg_132" s="T9">eː-le</ta>
            <ta e="T12" id="Seg_133" s="T11">tʼärɨ-tɨn</ta>
            <ta e="T13" id="Seg_134" s="T12">Kolpaše-ntə</ta>
            <ta e="T14" id="Seg_135" s="T13">qwan-enǯɨ-n</ta>
            <ta e="T15" id="Seg_136" s="T14">to</ta>
            <ta e="T16" id="Seg_137" s="T15">qwan-enǯɨ-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_138" s="T1">and</ta>
            <ta e="T3" id="Seg_139" s="T2">Ignashka.[NOM]</ta>
            <ta e="T4" id="Seg_140" s="T3">NEG</ta>
            <ta e="T5" id="Seg_141" s="T4">want-1SG.S</ta>
            <ta e="T6" id="Seg_142" s="T5">artel-EP-LOC</ta>
            <ta e="T7" id="Seg_143" s="T6">be-INF</ta>
            <ta e="T8" id="Seg_144" s="T7">(s)he-EP-ACC</ta>
            <ta e="T9" id="Seg_145" s="T8">away</ta>
            <ta e="T10" id="Seg_146" s="T9">be-CVB</ta>
            <ta e="T12" id="Seg_147" s="T11">say-3PL</ta>
            <ta e="T13" id="Seg_148" s="T12">Kolpashevo-ILL</ta>
            <ta e="T14" id="Seg_149" s="T13">leave-FUT-3SG.S</ta>
            <ta e="T15" id="Seg_150" s="T14">away</ta>
            <ta e="T16" id="Seg_151" s="T15">leave-FUT-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_152" s="T1">а</ta>
            <ta e="T3" id="Seg_153" s="T2">Игнашка.[NOM]</ta>
            <ta e="T4" id="Seg_154" s="T3">NEG</ta>
            <ta e="T5" id="Seg_155" s="T4">хотеть-1SG.S</ta>
            <ta e="T6" id="Seg_156" s="T5">артель-EP-LOC</ta>
            <ta e="T7" id="Seg_157" s="T6">быть-INF</ta>
            <ta e="T8" id="Seg_158" s="T7">он(а)-EP-ACC</ta>
            <ta e="T9" id="Seg_159" s="T8">прочь</ta>
            <ta e="T10" id="Seg_160" s="T9">быть-CVB</ta>
            <ta e="T12" id="Seg_161" s="T11">сказать-3PL</ta>
            <ta e="T13" id="Seg_162" s="T12">Колпашево-ILL</ta>
            <ta e="T14" id="Seg_163" s="T13">отправиться-FUT-3SG.S</ta>
            <ta e="T15" id="Seg_164" s="T14">прочь</ta>
            <ta e="T16" id="Seg_165" s="T15">отправиться-FUT-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_166" s="T1">conj</ta>
            <ta e="T3" id="Seg_167" s="T2">nprop.[n:case]</ta>
            <ta e="T4" id="Seg_168" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_169" s="T4">v-v:pn</ta>
            <ta e="T6" id="Seg_170" s="T5">n-n:ins-n:case</ta>
            <ta e="T7" id="Seg_171" s="T6">v-v:inf</ta>
            <ta e="T8" id="Seg_172" s="T7">pers-n:ins-n:case</ta>
            <ta e="T9" id="Seg_173" s="T8">preverb</ta>
            <ta e="T10" id="Seg_174" s="T9">v-v&gt;adv</ta>
            <ta e="T12" id="Seg_175" s="T11">v-v:pn</ta>
            <ta e="T13" id="Seg_176" s="T12">nprop-n:case</ta>
            <ta e="T14" id="Seg_177" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_178" s="T14">preverb</ta>
            <ta e="T16" id="Seg_179" s="T15">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_180" s="T1">conj</ta>
            <ta e="T3" id="Seg_181" s="T2">nprop</ta>
            <ta e="T4" id="Seg_182" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_183" s="T4">v</ta>
            <ta e="T6" id="Seg_184" s="T5">n</ta>
            <ta e="T7" id="Seg_185" s="T6">v</ta>
            <ta e="T8" id="Seg_186" s="T7">pers</ta>
            <ta e="T9" id="Seg_187" s="T8">preverb</ta>
            <ta e="T10" id="Seg_188" s="T9">v</ta>
            <ta e="T12" id="Seg_189" s="T11">v</ta>
            <ta e="T13" id="Seg_190" s="T12">nprop</ta>
            <ta e="T14" id="Seg_191" s="T13">v</ta>
            <ta e="T15" id="Seg_192" s="T14">preverb</ta>
            <ta e="T16" id="Seg_193" s="T15">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_194" s="T2">np.h:E</ta>
            <ta e="T6" id="Seg_195" s="T5">np:L</ta>
            <ta e="T7" id="Seg_196" s="T6">v:Th</ta>
            <ta e="T12" id="Seg_197" s="T11">0.3.h:A</ta>
            <ta e="T13" id="Seg_198" s="T12">np:G</ta>
            <ta e="T14" id="Seg_199" s="T13">0.3.h:A</ta>
            <ta e="T16" id="Seg_200" s="T15">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_201" s="T2">np.h:S</ta>
            <ta e="T5" id="Seg_202" s="T4">v:pred</ta>
            <ta e="T7" id="Seg_203" s="T6">v:O</ta>
            <ta e="T12" id="Seg_204" s="T11">0.3.h:S v:pred</ta>
            <ta e="T14" id="Seg_205" s="T13">0.3.h:S v:pred</ta>
            <ta e="T16" id="Seg_206" s="T15">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_207" s="T1">RUS:gram</ta>
            <ta e="T6" id="Seg_208" s="T5">RUS:cult</ta>
            <ta e="T13" id="Seg_209" s="T12">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_210" s="T1">And Ignashka didn't want to be in the artel.</ta>
            <ta e="T11" id="Seg_211" s="T7">He was expelled.</ta>
            <ta e="T14" id="Seg_212" s="T11">They said: “He'll go to Kolpashevo”.</ta>
            <ta e="T16" id="Seg_213" s="T14">“I'll go away”.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_214" s="T1">Und Ignaschka wollte nicht in der Artel sein.</ta>
            <ta e="T11" id="Seg_215" s="T7">Er wurde rausgeworfen.</ta>
            <ta e="T14" id="Seg_216" s="T11">Sie sagten: "Er wird nach Kolpashevo gehen."</ta>
            <ta e="T16" id="Seg_217" s="T14">"Ich gehe fort."</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_218" s="T1">А Игнашка не хочет в артели быть.</ta>
            <ta e="T11" id="Seg_219" s="T7">Его исключили.</ta>
            <ta e="T14" id="Seg_220" s="T11">Говорят: “В Колпашево пойдет”.</ta>
            <ta e="T16" id="Seg_221" s="T14">“Уйду я”.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_222" s="T1">а Игнашка не хочет в артели быть</ta>
            <ta e="T11" id="Seg_223" s="T7">его прочь выключили</ta>
            <ta e="T16" id="Seg_224" s="T14">уйду я</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T11" id="Seg_225" s="T7">[BrM:] 'tojelʼe' changed to 'to jelʼe'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
