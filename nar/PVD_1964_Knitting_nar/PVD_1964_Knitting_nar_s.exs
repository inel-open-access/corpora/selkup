<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Knitting_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Knitting_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">20</ud-information>
            <ud-information attribute-name="# HIAT:w">15</ud-information>
            <ud-information attribute-name="# e">15</ud-information>
            <ud-information attribute-name="# HIAT:u">4</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T16" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Oj</ts>
                  <nts id="Seg_5" n="HIAT:ip">,</nts>
                  <nts id="Seg_6" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_8" n="HIAT:w" s="T2">man</ts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_11" n="HIAT:w" s="T3">kotʼin</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_14" n="HIAT:w" s="T4">sormennau</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_17" n="HIAT:w" s="T5">spotkalam</ts>
                  <nts id="Seg_18" n="HIAT:ip">.</nts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_21" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">Teblam</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">qadǯizʼe</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">ass</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">qwannaǯal</ts>
                  <nts id="Seg_33" n="HIAT:ip">.</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_36" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">I</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">poqɨlaj</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">sormeǯəzan</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_48" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">Nagur</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">poqɨ</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">sormeddan</ts>
                  <nts id="Seg_57" n="HIAT:ip">.</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T16" id="Seg_59" n="sc" s="T1">
               <ts e="T2" id="Seg_61" n="e" s="T1">Oj, </ts>
               <ts e="T3" id="Seg_63" n="e" s="T2">man </ts>
               <ts e="T4" id="Seg_65" n="e" s="T3">kotʼin </ts>
               <ts e="T5" id="Seg_67" n="e" s="T4">sormennau </ts>
               <ts e="T6" id="Seg_69" n="e" s="T5">spotkalam. </ts>
               <ts e="T7" id="Seg_71" n="e" s="T6">Teblam </ts>
               <ts e="T8" id="Seg_73" n="e" s="T7">qadǯizʼe </ts>
               <ts e="T9" id="Seg_75" n="e" s="T8">ass </ts>
               <ts e="T10" id="Seg_77" n="e" s="T9">qwannaǯal. </ts>
               <ts e="T11" id="Seg_79" n="e" s="T10">I </ts>
               <ts e="T12" id="Seg_81" n="e" s="T11">poqɨlaj </ts>
               <ts e="T13" id="Seg_83" n="e" s="T12">sormeǯəzan. </ts>
               <ts e="T14" id="Seg_85" n="e" s="T13">Nagur </ts>
               <ts e="T15" id="Seg_87" n="e" s="T14">poqɨ </ts>
               <ts e="T16" id="Seg_89" n="e" s="T15">sormeddan. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_90" s="T1">PVD_1964_Knitting_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_91" s="T6">PVD_1964_Knitting_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_92" s="T10">PVD_1964_Knitting_nar.003 (001.003)</ta>
            <ta e="T16" id="Seg_93" s="T13">PVD_1964_Knitting_nar.004 (001.004)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_94" s="T1">ой, ман котʼин ′сорменнау споткалам.</ta>
            <ta e="T10" id="Seg_95" s="T6">теблам kадджи′зʼе асс kwа′нна(ӓ)‵джал.</ta>
            <ta e="T13" id="Seg_96" s="T10">и поkы′лай сормеджъзан.</ta>
            <ta e="T16" id="Seg_97" s="T13">′нагур ′поkы ′сормед̂дан.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_98" s="T1">oj, man kotʼin sormennau spotkalam.</ta>
            <ta e="T10" id="Seg_99" s="T6">teblam qadǯizʼe ass qwanna(ä)ǯal.</ta>
            <ta e="T13" id="Seg_100" s="T10">i poqɨlaj sormeǯəzan.</ta>
            <ta e="T16" id="Seg_101" s="T13">nagur poqɨ sormed̂dan.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_102" s="T1">Oj, man kotʼin sormennau spotkalam. </ta>
            <ta e="T10" id="Seg_103" s="T6">Teblam qadǯizʼe ass qwannaǯal. </ta>
            <ta e="T13" id="Seg_104" s="T10">I poqɨlaj sormeǯəzan. </ta>
            <ta e="T16" id="Seg_105" s="T13">Nagur poqɨ sormeddan. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_106" s="T1">oj</ta>
            <ta e="T3" id="Seg_107" s="T2">man</ta>
            <ta e="T4" id="Seg_108" s="T3">kotʼi-n</ta>
            <ta e="T5" id="Seg_109" s="T4">sorme-nna-u</ta>
            <ta e="T6" id="Seg_110" s="T5">spotka-la-m</ta>
            <ta e="T7" id="Seg_111" s="T6">teb-la-m</ta>
            <ta e="T8" id="Seg_112" s="T7">qadǯi-zʼe</ta>
            <ta e="T9" id="Seg_113" s="T8">ass</ta>
            <ta e="T10" id="Seg_114" s="T9">qwan-na-ǯa-l</ta>
            <ta e="T11" id="Seg_115" s="T10">i</ta>
            <ta e="T12" id="Seg_116" s="T11">poqɨ-la-j</ta>
            <ta e="T13" id="Seg_117" s="T12">sorme-ǯə-za-n</ta>
            <ta e="T14" id="Seg_118" s="T13">nagur</ta>
            <ta e="T15" id="Seg_119" s="T14">poqɨ</ta>
            <ta e="T16" id="Seg_120" s="T15">sorme-dda-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_121" s="T1">oj</ta>
            <ta e="T3" id="Seg_122" s="T2">man</ta>
            <ta e="T4" id="Seg_123" s="T3">koːci-ŋ</ta>
            <ta e="T5" id="Seg_124" s="T4">sormu-nɨ-w</ta>
            <ta e="T6" id="Seg_125" s="T5">spotka-la-m</ta>
            <ta e="T7" id="Seg_126" s="T6">täp-la-m</ta>
            <ta e="T8" id="Seg_127" s="T7">qaǯɨ-se</ta>
            <ta e="T9" id="Seg_128" s="T8">asa</ta>
            <ta e="T10" id="Seg_129" s="T9">qwan-tɨ-enǯɨ-l</ta>
            <ta e="T11" id="Seg_130" s="T10">i</ta>
            <ta e="T12" id="Seg_131" s="T11">poqqɨ-la-lʼ</ta>
            <ta e="T13" id="Seg_132" s="T12">sormu-ǯə-sɨ-ŋ</ta>
            <ta e="T14" id="Seg_133" s="T13">nagur</ta>
            <ta e="T15" id="Seg_134" s="T14">poqqɨ</ta>
            <ta e="T16" id="Seg_135" s="T15">sormu-ntɨ-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_136" s="T1">oh</ta>
            <ta e="T3" id="Seg_137" s="T2">I.NOM</ta>
            <ta e="T4" id="Seg_138" s="T3">much-ADVZ</ta>
            <ta e="T5" id="Seg_139" s="T4">knit-CO-1SG.O</ta>
            <ta e="T6" id="Seg_140" s="T5">knitted.mitten-PL-ACC</ta>
            <ta e="T7" id="Seg_141" s="T6">(s)he-PL-ACC</ta>
            <ta e="T8" id="Seg_142" s="T7">cart-COM</ta>
            <ta e="T9" id="Seg_143" s="T8">NEG</ta>
            <ta e="T10" id="Seg_144" s="T9">leave-TR-FUT-2SG.O</ta>
            <ta e="T11" id="Seg_145" s="T10">and</ta>
            <ta e="T12" id="Seg_146" s="T11">net-PL-ADJZ</ta>
            <ta e="T13" id="Seg_147" s="T12">knit-DRV-PST-1SG.S</ta>
            <ta e="T14" id="Seg_148" s="T13">three</ta>
            <ta e="T15" id="Seg_149" s="T14">net.[NOM]</ta>
            <ta e="T16" id="Seg_150" s="T15">knit-INFER-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_151" s="T1">ой</ta>
            <ta e="T3" id="Seg_152" s="T2">я.NOM</ta>
            <ta e="T4" id="Seg_153" s="T3">много-ADVZ</ta>
            <ta e="T5" id="Seg_154" s="T4">связать-CO-1SG.O</ta>
            <ta e="T6" id="Seg_155" s="T5">вязаная.рукавица-PL-ACC</ta>
            <ta e="T7" id="Seg_156" s="T6">он(а)-PL-ACC</ta>
            <ta e="T8" id="Seg_157" s="T7">воз-COM</ta>
            <ta e="T9" id="Seg_158" s="T8">NEG</ta>
            <ta e="T10" id="Seg_159" s="T9">отправиться-TR-FUT-2SG.O</ta>
            <ta e="T11" id="Seg_160" s="T10">и</ta>
            <ta e="T12" id="Seg_161" s="T11">сеть-PL-ADJZ</ta>
            <ta e="T13" id="Seg_162" s="T12">связать-DRV-PST-1SG.S</ta>
            <ta e="T14" id="Seg_163" s="T13">три</ta>
            <ta e="T15" id="Seg_164" s="T14">сеть.[NOM]</ta>
            <ta e="T16" id="Seg_165" s="T15">связать-INFER-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_166" s="T1">interj</ta>
            <ta e="T3" id="Seg_167" s="T2">pers</ta>
            <ta e="T4" id="Seg_168" s="T3">quant-quant&gt;adv</ta>
            <ta e="T5" id="Seg_169" s="T4">v-v:ins-v:pn</ta>
            <ta e="T6" id="Seg_170" s="T5">n-n:num-n:case</ta>
            <ta e="T7" id="Seg_171" s="T6">pers-n:num-n:case</ta>
            <ta e="T8" id="Seg_172" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_173" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_174" s="T9">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_175" s="T10">conj</ta>
            <ta e="T12" id="Seg_176" s="T11">n-n:num-n&gt;adj</ta>
            <ta e="T13" id="Seg_177" s="T12">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_178" s="T13">num</ta>
            <ta e="T15" id="Seg_179" s="T14">n.[n:case]</ta>
            <ta e="T16" id="Seg_180" s="T15">v-v:mood-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_181" s="T1">interj</ta>
            <ta e="T3" id="Seg_182" s="T2">pers</ta>
            <ta e="T4" id="Seg_183" s="T3">adv</ta>
            <ta e="T5" id="Seg_184" s="T4">v</ta>
            <ta e="T6" id="Seg_185" s="T5">n</ta>
            <ta e="T7" id="Seg_186" s="T6">pers</ta>
            <ta e="T8" id="Seg_187" s="T7">n</ta>
            <ta e="T9" id="Seg_188" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_189" s="T9">v</ta>
            <ta e="T11" id="Seg_190" s="T10">conj</ta>
            <ta e="T12" id="Seg_191" s="T11">adj</ta>
            <ta e="T13" id="Seg_192" s="T12">v</ta>
            <ta e="T14" id="Seg_193" s="T13">num</ta>
            <ta e="T15" id="Seg_194" s="T14">n</ta>
            <ta e="T16" id="Seg_195" s="T15">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_196" s="T2">pro.h:A</ta>
            <ta e="T6" id="Seg_197" s="T5">np:P</ta>
            <ta e="T7" id="Seg_198" s="T6">pro:Th</ta>
            <ta e="T8" id="Seg_199" s="T7">np:Ins</ta>
            <ta e="T10" id="Seg_200" s="T9">0.2.h:A</ta>
            <ta e="T12" id="Seg_201" s="T11">np:P</ta>
            <ta e="T13" id="Seg_202" s="T12">0.1.h:A</ta>
            <ta e="T15" id="Seg_203" s="T14">np:P</ta>
            <ta e="T16" id="Seg_204" s="T15">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_205" s="T2">pro.h:S</ta>
            <ta e="T5" id="Seg_206" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_207" s="T5">np:O</ta>
            <ta e="T7" id="Seg_208" s="T6">pro:O</ta>
            <ta e="T10" id="Seg_209" s="T9">0.2.h:S v:pred</ta>
            <ta e="T12" id="Seg_210" s="T11">np:O</ta>
            <ta e="T13" id="Seg_211" s="T12">0.1.h:S v:pred</ta>
            <ta e="T15" id="Seg_212" s="T14">np:O</ta>
            <ta e="T16" id="Seg_213" s="T15">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_214" s="T1">RUS:disc</ta>
            <ta e="T6" id="Seg_215" s="T5">((unknown))</ta>
            <ta e="T11" id="Seg_216" s="T10">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_217" s="T1">Oh, I knitted many mittens.</ta>
            <ta e="T10" id="Seg_218" s="T6">One can't carry them with a whole cart.</ta>
            <ta e="T13" id="Seg_219" s="T10">And I knitted nets.</ta>
            <ta e="T16" id="Seg_220" s="T13">I knitted three nets.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_221" s="T1">Oh, ich habe viele Handschuhe gestrickt.</ta>
            <ta e="T10" id="Seg_222" s="T6">Man kann sie nicht [alle] mit einem Wagen befördern.</ta>
            <ta e="T13" id="Seg_223" s="T10">Und ich habe Netze geknüpft.</ta>
            <ta e="T16" id="Seg_224" s="T13">Drei Netze habe ich geknüpft.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_225" s="T1">Ой, я много навязала рукавиц.</ta>
            <ta e="T10" id="Seg_226" s="T6">Их возом не увезешь.</ta>
            <ta e="T13" id="Seg_227" s="T10">И сетки (частушки) вязала.</ta>
            <ta e="T16" id="Seg_228" s="T13">Три сети связала.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_229" s="T1">ой я много навязала рукавиц</ta>
            <ta e="T10" id="Seg_230" s="T6">возом не увезешь</ta>
            <ta e="T13" id="Seg_231" s="T10">и сетки (частушки) вязала</ta>
            <ta e="T16" id="Seg_232" s="T13">три частушки связала</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T10" id="Seg_233" s="T6">[KuAI:] Variant: 'qwannäǯal'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
