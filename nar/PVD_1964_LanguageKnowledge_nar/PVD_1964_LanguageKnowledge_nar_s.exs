<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_LanguageKnowledge_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_LanguageKnowledge_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">33</ud-information>
            <ud-information attribute-name="# HIAT:w">25</ud-information>
            <ud-information attribute-name="# e">25</ud-information>
            <ud-information attribute-name="# HIAT:u">5</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T26" id="Seg_0" n="sc" s="T1">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Meː</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">dari</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">toʒa</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">soːdigan</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">kollupbugu</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_19" n="HIAT:w" s="T6">nadə</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_23" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">Awan</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">kulumennaš</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">biqwɛdʒattə</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_35" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">Biskugu</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">ne</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">nadə</ts>
                  <nts id="Seg_44" n="HIAT:ip">,</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">me</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">dari</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">eʒeli</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_56" n="HIAT:w" s="T16">as</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_59" n="HIAT:w" s="T17">tunon</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_62" n="HIAT:w" s="T18">kulupbugu</ts>
                  <nts id="Seg_63" n="HIAT:ip">.</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_66" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">A</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_71" n="HIAT:w" s="T20">dʼaja</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_74" n="HIAT:w" s="T21">Mixajla</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_77" n="HIAT:w" s="T22">lagwatpa</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip">“</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_82" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_84" n="HIAT:w" s="T23">Qaj</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_87" n="HIAT:w" s="T24">nʼöpla</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_90" n="HIAT:w" s="T25">jes</ts>
                  <nts id="Seg_91" n="HIAT:ip">.</nts>
                  <nts id="Seg_92" n="HIAT:ip">”</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T26" id="Seg_94" n="sc" s="T1">
               <ts e="T2" id="Seg_96" n="e" s="T1">Meː </ts>
               <ts e="T3" id="Seg_98" n="e" s="T2">dari </ts>
               <ts e="T4" id="Seg_100" n="e" s="T3">toʒa </ts>
               <ts e="T5" id="Seg_102" n="e" s="T4">soːdigan </ts>
               <ts e="T6" id="Seg_104" n="e" s="T5">kollupbugu </ts>
               <ts e="T7" id="Seg_106" n="e" s="T6">nadə. </ts>
               <ts e="T8" id="Seg_108" n="e" s="T7">Awan </ts>
               <ts e="T9" id="Seg_110" n="e" s="T8">kulumennaš </ts>
               <ts e="T10" id="Seg_112" n="e" s="T9">biqwɛdʒattə. </ts>
               <ts e="T11" id="Seg_114" n="e" s="T10">Biskugu </ts>
               <ts e="T12" id="Seg_116" n="e" s="T11">ne </ts>
               <ts e="T13" id="Seg_118" n="e" s="T12">nadə, </ts>
               <ts e="T14" id="Seg_120" n="e" s="T13">me </ts>
               <ts e="T15" id="Seg_122" n="e" s="T14">dari </ts>
               <ts e="T16" id="Seg_124" n="e" s="T15">eʒeli </ts>
               <ts e="T17" id="Seg_126" n="e" s="T16">as </ts>
               <ts e="T18" id="Seg_128" n="e" s="T17">tunon </ts>
               <ts e="T19" id="Seg_130" n="e" s="T18">kulupbugu. </ts>
               <ts e="T20" id="Seg_132" n="e" s="T19">A </ts>
               <ts e="T21" id="Seg_134" n="e" s="T20">dʼaja </ts>
               <ts e="T22" id="Seg_136" n="e" s="T21">Mixajla </ts>
               <ts e="T23" id="Seg_138" n="e" s="T22">lagwatpa.“ </ts>
               <ts e="T24" id="Seg_140" n="e" s="T23">Qaj </ts>
               <ts e="T25" id="Seg_142" n="e" s="T24">nʼöpla </ts>
               <ts e="T26" id="Seg_144" n="e" s="T25">jes.” </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_145" s="T1">PVD_1964_LanguageKnowledge_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_146" s="T7">PVD_1964_LanguageKnowledge_nar.002 (001.002)</ta>
            <ta e="T19" id="Seg_147" s="T10">PVD_1964_LanguageKnowledge_nar.003 (001.003)</ta>
            <ta e="T23" id="Seg_148" s="T19">PVD_1964_LanguageKnowledge_nar.004 (001.004)</ta>
            <ta e="T26" id="Seg_149" s="T23">PVD_1964_LanguageKnowledge_nar.005 (001.005)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T7" id="Seg_150" s="T1">ме̄дари ′тожа со̄диган коll(лл)упбугу надъ.</ta>
            <ta e="T10" id="Seg_151" s="T7">а′wан ′куlу ′меннаш б̂иkwɛд̂жаттъ.</ta>
            <ta e="T19" id="Seg_152" s="T10">′б̂искугу не надъ, ме′дари ежели ас ту′но(у)н ку′лупбугу.</ta>
            <ta e="T23" id="Seg_153" s="T19">а ′дʼаjа Ми′хайла ′лагватпа.</ta>
            <ta e="T26" id="Seg_154" s="T23">kай ′нʼӧпла jес.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T7" id="Seg_155" s="T1">meːdari toʒa soːdigan koll(ll)upbugu nadə.</ta>
            <ta e="T10" id="Seg_156" s="T7">awan kulu mennaš b̂iqwɛd̂ʒattə.</ta>
            <ta e="T19" id="Seg_157" s="T10">b̂iskugu ne nadə, medari eʒeli as tuno(u)n kulupbugu.</ta>
            <ta e="T23" id="Seg_158" s="T19">a dʼaja Мixajla lagwatpa.</ta>
            <ta e="T26" id="Seg_159" s="T23">qaj nʼöpla jes.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_160" s="T1">Meː dari toʒa soːdigan kollupbugu nadə. </ta>
            <ta e="T10" id="Seg_161" s="T7">Awan kulumennaš biqwɛdʒattə. </ta>
            <ta e="T19" id="Seg_162" s="T10">Biskugu ne nadə, me dari eʒeli as tunon kulupbugu. </ta>
            <ta e="T23" id="Seg_163" s="T19">A dʼaja Mixajla lagwatpa.“ </ta>
            <ta e="T26" id="Seg_164" s="T23">Qaj nʼöpla jes.” </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_165" s="T1">meː</ta>
            <ta e="T3" id="Seg_166" s="T2">dari</ta>
            <ta e="T4" id="Seg_167" s="T3">toʒa</ta>
            <ta e="T5" id="Seg_168" s="T4">soːdiga-n</ta>
            <ta e="T6" id="Seg_169" s="T5">kollupbu-gu</ta>
            <ta e="T7" id="Seg_170" s="T6">nadə</ta>
            <ta e="T8" id="Seg_171" s="T7">awa-n</ta>
            <ta e="T9" id="Seg_172" s="T8">kulu-me-nnaš</ta>
            <ta e="T10" id="Seg_173" s="T9">bi-qw-ɛdʒa-ttə</ta>
            <ta e="T11" id="Seg_174" s="T10">bis-ku-gu</ta>
            <ta e="T12" id="Seg_175" s="T11">ne</ta>
            <ta e="T13" id="Seg_176" s="T12">nadə</ta>
            <ta e="T14" id="Seg_177" s="T13">me</ta>
            <ta e="T15" id="Seg_178" s="T14">dari</ta>
            <ta e="T16" id="Seg_179" s="T15">eʒeli</ta>
            <ta e="T17" id="Seg_180" s="T16">as</ta>
            <ta e="T18" id="Seg_181" s="T17">tuno-n</ta>
            <ta e="T19" id="Seg_182" s="T18">kulupbu-gu</ta>
            <ta e="T20" id="Seg_183" s="T19">a</ta>
            <ta e="T21" id="Seg_184" s="T20">dʼaja</ta>
            <ta e="T22" id="Seg_185" s="T21">Mixajla</ta>
            <ta e="T23" id="Seg_186" s="T22">lagwat-pa</ta>
            <ta e="T24" id="Seg_187" s="T23">qaj</ta>
            <ta e="T25" id="Seg_188" s="T24">nʼöpla</ta>
            <ta e="T26" id="Seg_189" s="T25">je-s</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_190" s="T1">me</ta>
            <ta e="T3" id="Seg_191" s="T2">dari</ta>
            <ta e="T4" id="Seg_192" s="T3">toʒa</ta>
            <ta e="T5" id="Seg_193" s="T4">soːdʼiga-ŋ</ta>
            <ta e="T6" id="Seg_194" s="T5">kulubu-gu</ta>
            <ta e="T7" id="Seg_195" s="T6">nadə</ta>
            <ta e="T8" id="Seg_196" s="T7">awa-ŋ</ta>
            <ta e="T9" id="Seg_197" s="T8">kulubu-me-naš</ta>
            <ta e="T10" id="Seg_198" s="T9">pissɨ-ku-enǯɨ-tɨn</ta>
            <ta e="T11" id="Seg_199" s="T10">pissɨ-ku-gu</ta>
            <ta e="T12" id="Seg_200" s="T11">ne</ta>
            <ta e="T13" id="Seg_201" s="T12">nadə</ta>
            <ta e="T14" id="Seg_202" s="T13">me</ta>
            <ta e="T15" id="Seg_203" s="T14">dari</ta>
            <ta e="T16" id="Seg_204" s="T15">jeʒlʼe</ta>
            <ta e="T17" id="Seg_205" s="T16">asa</ta>
            <ta e="T18" id="Seg_206" s="T17">tonu-n</ta>
            <ta e="T19" id="Seg_207" s="T18">kulubu-gu</ta>
            <ta e="T20" id="Seg_208" s="T19">a</ta>
            <ta e="T21" id="Seg_209" s="T20">dʼaja</ta>
            <ta e="T22" id="Seg_210" s="T21">Mixajla</ta>
            <ta e="T23" id="Seg_211" s="T22">laːɣwat-mbɨ</ta>
            <ta e="T24" id="Seg_212" s="T23">qaj</ta>
            <ta e="T25" id="Seg_213" s="T24">nʼöpla</ta>
            <ta e="T26" id="Seg_214" s="T25">eː-sɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_215" s="T1">we.GEN</ta>
            <ta e="T3" id="Seg_216" s="T2">like</ta>
            <ta e="T4" id="Seg_217" s="T3">also</ta>
            <ta e="T5" id="Seg_218" s="T4">good-ADVZ</ta>
            <ta e="T6" id="Seg_219" s="T5">speak-INF</ta>
            <ta e="T7" id="Seg_220" s="T6">one.should</ta>
            <ta e="T8" id="Seg_221" s="T7">bad-ADVZ</ta>
            <ta e="T9" id="Seg_222" s="T8">speak-COND-POT.FUT.2SG</ta>
            <ta e="T10" id="Seg_223" s="T9">laugh-HAB-FUT-3PL</ta>
            <ta e="T11" id="Seg_224" s="T10">laugh-HAB-INF</ta>
            <ta e="T12" id="Seg_225" s="T11">NEG</ta>
            <ta e="T13" id="Seg_226" s="T12">one.should</ta>
            <ta e="T14" id="Seg_227" s="T13">we.GEN</ta>
            <ta e="T15" id="Seg_228" s="T14">like</ta>
            <ta e="T16" id="Seg_229" s="T15">if</ta>
            <ta e="T17" id="Seg_230" s="T16">NEG</ta>
            <ta e="T18" id="Seg_231" s="T17">can-3SG.S</ta>
            <ta e="T19" id="Seg_232" s="T18">speak-INF</ta>
            <ta e="T20" id="Seg_233" s="T19">and</ta>
            <ta e="T21" id="Seg_234" s="T20">uncle.[NOM]</ta>
            <ta e="T22" id="Seg_235" s="T21">Mikhailo.[NOM]</ta>
            <ta e="T23" id="Seg_236" s="T22">begin.to.laugh-DUR.[3SG.S]</ta>
            <ta e="T24" id="Seg_237" s="T23">either</ta>
            <ta e="T25" id="Seg_238" s="T24">%very.bad</ta>
            <ta e="T26" id="Seg_239" s="T25">be-PST.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_240" s="T1">мы.GEN</ta>
            <ta e="T3" id="Seg_241" s="T2">как</ta>
            <ta e="T4" id="Seg_242" s="T3">тоже</ta>
            <ta e="T5" id="Seg_243" s="T4">хороший-ADVZ</ta>
            <ta e="T6" id="Seg_244" s="T5">говорить-INF</ta>
            <ta e="T7" id="Seg_245" s="T6">надо</ta>
            <ta e="T8" id="Seg_246" s="T7">плохой-ADVZ</ta>
            <ta e="T9" id="Seg_247" s="T8">говорить-COND-POT.FUT.2SG</ta>
            <ta e="T10" id="Seg_248" s="T9">смеяться-HAB-FUT-3PL</ta>
            <ta e="T11" id="Seg_249" s="T10">смеяться-HAB-INF</ta>
            <ta e="T12" id="Seg_250" s="T11">NEG</ta>
            <ta e="T13" id="Seg_251" s="T12">надо</ta>
            <ta e="T14" id="Seg_252" s="T13">мы.GEN</ta>
            <ta e="T15" id="Seg_253" s="T14">как</ta>
            <ta e="T16" id="Seg_254" s="T15">ежели</ta>
            <ta e="T17" id="Seg_255" s="T16">NEG</ta>
            <ta e="T18" id="Seg_256" s="T17">уметь-3SG.S</ta>
            <ta e="T19" id="Seg_257" s="T18">говорить-INF</ta>
            <ta e="T20" id="Seg_258" s="T19">а</ta>
            <ta e="T21" id="Seg_259" s="T20">дядя.[NOM]</ta>
            <ta e="T22" id="Seg_260" s="T21">Михайло.[NOM]</ta>
            <ta e="T23" id="Seg_261" s="T22">засмеяться-DUR.[3SG.S]</ta>
            <ta e="T24" id="Seg_262" s="T23">ли</ta>
            <ta e="T25" id="Seg_263" s="T24">%очень.плохой</ta>
            <ta e="T26" id="Seg_264" s="T25">быть-PST.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_265" s="T1">pers</ta>
            <ta e="T3" id="Seg_266" s="T2">pp</ta>
            <ta e="T4" id="Seg_267" s="T3">adv</ta>
            <ta e="T5" id="Seg_268" s="T4">adj-adj&gt;adv</ta>
            <ta e="T6" id="Seg_269" s="T5">v-v:inf</ta>
            <ta e="T7" id="Seg_270" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_271" s="T7">adj-adj&gt;adv</ta>
            <ta e="T9" id="Seg_272" s="T8">v-v:mood-v:tense</ta>
            <ta e="T10" id="Seg_273" s="T9">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_274" s="T10">v-v&gt;v-v:inf</ta>
            <ta e="T12" id="Seg_275" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_276" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_277" s="T13">pers</ta>
            <ta e="T15" id="Seg_278" s="T14">pp</ta>
            <ta e="T16" id="Seg_279" s="T15">conj</ta>
            <ta e="T17" id="Seg_280" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_281" s="T17">v-v:pn</ta>
            <ta e="T19" id="Seg_282" s="T18">v-v:inf</ta>
            <ta e="T20" id="Seg_283" s="T19">conj</ta>
            <ta e="T21" id="Seg_284" s="T20">n.[n:case]</ta>
            <ta e="T22" id="Seg_285" s="T21">nprop.[n:case]</ta>
            <ta e="T23" id="Seg_286" s="T22">v-v&gt;v.[v:pn]</ta>
            <ta e="T24" id="Seg_287" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_288" s="T24">adj</ta>
            <ta e="T26" id="Seg_289" s="T25">v-v:tense.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_290" s="T1">pers</ta>
            <ta e="T3" id="Seg_291" s="T2">pp</ta>
            <ta e="T4" id="Seg_292" s="T3">adv</ta>
            <ta e="T5" id="Seg_293" s="T4">adv</ta>
            <ta e="T6" id="Seg_294" s="T5">v</ta>
            <ta e="T7" id="Seg_295" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_296" s="T7">adv</ta>
            <ta e="T9" id="Seg_297" s="T8">v</ta>
            <ta e="T10" id="Seg_298" s="T9">v</ta>
            <ta e="T11" id="Seg_299" s="T10">v</ta>
            <ta e="T12" id="Seg_300" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_301" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_302" s="T13">pers</ta>
            <ta e="T15" id="Seg_303" s="T14">pp</ta>
            <ta e="T16" id="Seg_304" s="T15">conj</ta>
            <ta e="T17" id="Seg_305" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_306" s="T17">v</ta>
            <ta e="T19" id="Seg_307" s="T18">v</ta>
            <ta e="T20" id="Seg_308" s="T19">conj</ta>
            <ta e="T21" id="Seg_309" s="T20">n</ta>
            <ta e="T22" id="Seg_310" s="T21">nprop</ta>
            <ta e="T23" id="Seg_311" s="T22">v</ta>
            <ta e="T24" id="Seg_312" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_313" s="T24">adj</ta>
            <ta e="T26" id="Seg_314" s="T25">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T6" id="Seg_315" s="T5">v:Th</ta>
            <ta e="T9" id="Seg_316" s="T8">0.2.h:A</ta>
            <ta e="T10" id="Seg_317" s="T9">0.3.h:A</ta>
            <ta e="T11" id="Seg_318" s="T10">v:Th</ta>
            <ta e="T18" id="Seg_319" s="T17">0.3.h:E</ta>
            <ta e="T19" id="Seg_320" s="T18">v:Th</ta>
            <ta e="T22" id="Seg_321" s="T21">np.h:A</ta>
            <ta e="T26" id="Seg_322" s="T25">0.3.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T6" id="Seg_323" s="T5">v:O</ta>
            <ta e="T7" id="Seg_324" s="T6">ptcl:pred</ta>
            <ta e="T9" id="Seg_325" s="T7">s:cond</ta>
            <ta e="T10" id="Seg_326" s="T9">0.3.h:S v:pred</ta>
            <ta e="T11" id="Seg_327" s="T10">v:O</ta>
            <ta e="T13" id="Seg_328" s="T12">ptcl:pred</ta>
            <ta e="T19" id="Seg_329" s="T13">s:cond</ta>
            <ta e="T22" id="Seg_330" s="T21">np.h:S</ta>
            <ta e="T23" id="Seg_331" s="T22">v:pred</ta>
            <ta e="T25" id="Seg_332" s="T24">adj:pred</ta>
            <ta e="T26" id="Seg_333" s="T25">0.3.h:S cop</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_334" s="T3">RUS:core</ta>
            <ta e="T7" id="Seg_335" s="T6">RUS:mod</ta>
            <ta e="T12" id="Seg_336" s="T11">RUS:gram</ta>
            <ta e="T13" id="Seg_337" s="T12">RUS:mod</ta>
            <ta e="T16" id="Seg_338" s="T15">RUS:gram</ta>
            <ta e="T20" id="Seg_339" s="T19">RUS:gram</ta>
            <ta e="T21" id="Seg_340" s="T20">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_341" s="T1">One should also speak well our language.</ta>
            <ta e="T10" id="Seg_342" s="T7">If you speak bad, they will laugh.</ta>
            <ta e="T19" id="Seg_343" s="T10">One shouldn't laugh, if one can't speak our language.</ta>
            <ta e="T23" id="Seg_344" s="T19">And uncle Mikhailo laughs.</ta>
            <ta e="T26" id="Seg_345" s="T23">“What a (bad man)?.”</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_346" s="T1">Man sollte auch unsere Sprache gut sprechen.</ta>
            <ta e="T10" id="Seg_347" s="T7">Wenn du schlecht sprichst, lachen sie.</ta>
            <ta e="T19" id="Seg_348" s="T10">Man sollte nicht lachen, wenn jemand unsere Sprache nicht sprechen kann.</ta>
            <ta e="T23" id="Seg_349" s="T19">Und Onkel Mikhailo lacht.</ta>
            <ta e="T26" id="Seg_350" s="T23">"Was für ein (schlechter Mann)?"</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_351" s="T1">По-нашему тоже надо хорошо разговаривать.</ta>
            <ta e="T10" id="Seg_352" s="T7">Плохо будешь говорить – смеяться будут.</ta>
            <ta e="T19" id="Seg_353" s="T10">Смеяться не надо, если по-нашему (человек) не умеет говорить.</ta>
            <ta e="T23" id="Seg_354" s="T19">А дядя Михайла смеется.</ta>
            <ta e="T26" id="Seg_355" s="T23">“Что за плохой.”</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_356" s="T1">по-нашему тоже надо хорошо разговаривать</ta>
            <ta e="T10" id="Seg_357" s="T7">плохо будешь говорить смеяться будут</ta>
            <ta e="T19" id="Seg_358" s="T10">смеяться не надо если по-нашему не умеет говорит</ta>
            <ta e="T23" id="Seg_359" s="T19">а дядя Михайла смеется</ta>
            <ta e="T26" id="Seg_360" s="T23">очень плохой</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T10" id="Seg_361" s="T7">[BrM:] Tentative analysis of 'biqwɛdʒattə'.</ta>
            <ta e="T19" id="Seg_362" s="T10">[KuAI:] Variant: 'tunun'.</ta>
            <ta e="T26" id="Seg_363" s="T23">[KuAI:] 'nʼöpla' is a very bad word.</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T26" id="Seg_364" s="T23">если человек не умеет по-нашему говорить, то дядя Михайла говорит: ′нʼӧпла - это очень плохое слово, очень плохой</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
