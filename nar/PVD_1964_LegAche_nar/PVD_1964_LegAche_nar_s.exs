<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_LegAche_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_LegAche_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">13</ud-information>
            <ud-information attribute-name="# HIAT:w">9</ud-information>
            <ud-information attribute-name="# e">9</ud-information>
            <ud-information attribute-name="# HIAT:u">4</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T10" id="Seg_0" n="sc" s="T1">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Tobou</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">közit</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T5" id="Seg_11" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">Pulbo</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">omnɨmɨt</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_20" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">Man</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">täbɨm</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">nagawarinau</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_32" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">Täp</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">čagɨn</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T10" id="Seg_40" n="sc" s="T1">
               <ts e="T2" id="Seg_42" n="e" s="T1">Tobou </ts>
               <ts e="T3" id="Seg_44" n="e" s="T2">közit. </ts>
               <ts e="T4" id="Seg_46" n="e" s="T3">Pulbo </ts>
               <ts e="T5" id="Seg_48" n="e" s="T4">omnɨmɨt. </ts>
               <ts e="T6" id="Seg_50" n="e" s="T5">Man </ts>
               <ts e="T7" id="Seg_52" n="e" s="T6">täbɨm </ts>
               <ts e="T8" id="Seg_54" n="e" s="T7">nagawarinau. </ts>
               <ts e="T9" id="Seg_56" n="e" s="T8">Täp </ts>
               <ts e="T10" id="Seg_58" n="e" s="T9">čagɨn. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_59" s="T1">PVD_1964_LegAche_nar.001 (001.001)</ta>
            <ta e="T5" id="Seg_60" s="T3">PVD_1964_LegAche_nar.002 (001.002)</ta>
            <ta e="T8" id="Seg_61" s="T5">PVD_1964_LegAche_nar.003 (001.003)</ta>
            <ta e="T10" id="Seg_62" s="T8">PVD_1964_LegAche_nar.004 (001.004)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_63" s="T1">то′боу ′кӧзит.</ta>
            <ta e="T5" id="Seg_64" s="T3">′пулбо ′омнымыт.</ta>
            <ta e="T8" id="Seg_65" s="T5">ман ′тӓбым нага′вари‵нау.</ta>
            <ta e="T10" id="Seg_66" s="T8">тӓп ′тшагын.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_67" s="T1">tobou közit.</ta>
            <ta e="T5" id="Seg_68" s="T3">pulbo omnɨmɨt.</ta>
            <ta e="T8" id="Seg_69" s="T5">man täbɨm nagawarinau.</ta>
            <ta e="T10" id="Seg_70" s="T8">täp tšagɨn.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_71" s="T1">Tobou közit. </ta>
            <ta e="T5" id="Seg_72" s="T3">Pulbo omnɨmɨt. </ta>
            <ta e="T8" id="Seg_73" s="T5">Man täbɨm nagawarinau. </ta>
            <ta e="T10" id="Seg_74" s="T8">Täp čagɨn. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_75" s="T1">tob-o-u</ta>
            <ta e="T3" id="Seg_76" s="T2">közi-t</ta>
            <ta e="T4" id="Seg_77" s="T3">pulbo</ta>
            <ta e="T5" id="Seg_78" s="T4">omnɨ-mɨ-t</ta>
            <ta e="T6" id="Seg_79" s="T5">man</ta>
            <ta e="T7" id="Seg_80" s="T6">täb-ɨ-m</ta>
            <ta e="T8" id="Seg_81" s="T7">nagawari-na-u</ta>
            <ta e="T9" id="Seg_82" s="T8">täp</ta>
            <ta e="T10" id="Seg_83" s="T9">čagɨ-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_84" s="T1">tob-ɨ-w</ta>
            <ta e="T3" id="Seg_85" s="T2">qüzu-ntɨ</ta>
            <ta e="T4" id="Seg_86" s="T3">pulbo</ta>
            <ta e="T5" id="Seg_87" s="T4">omdɨ-mbɨ-ntɨ</ta>
            <ta e="T6" id="Seg_88" s="T5">man</ta>
            <ta e="T7" id="Seg_89" s="T6">täp-ɨ-m</ta>
            <ta e="T8" id="Seg_90" s="T7">nagawari-nɨ-w</ta>
            <ta e="T9" id="Seg_91" s="T8">täp</ta>
            <ta e="T10" id="Seg_92" s="T9">čagɨ-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_93" s="T1">leg.[NOM]-EP-1SG</ta>
            <ta e="T3" id="Seg_94" s="T2">ache-INFER.[3SG.S]</ta>
            <ta e="T4" id="Seg_95" s="T3">furuncle.[NOM]</ta>
            <ta e="T5" id="Seg_96" s="T4">sit.down-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T6" id="Seg_97" s="T5">I.NOM</ta>
            <ta e="T7" id="Seg_98" s="T6">(s)he-EP-ACC</ta>
            <ta e="T8" id="Seg_99" s="T7">cast.a.spell-CO-1SG.O</ta>
            <ta e="T9" id="Seg_100" s="T8">(s)he.[NOM]</ta>
            <ta e="T10" id="Seg_101" s="T9">dry-3SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_102" s="T1">нога.[NOM]-EP-1SG</ta>
            <ta e="T3" id="Seg_103" s="T2">болеть-INFER.[3SG.S]</ta>
            <ta e="T4" id="Seg_104" s="T3">чирей.[NOM]</ta>
            <ta e="T5" id="Seg_105" s="T4">сесть-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T6" id="Seg_106" s="T5">я.NOM</ta>
            <ta e="T7" id="Seg_107" s="T6">он(а)-EP-ACC</ta>
            <ta e="T8" id="Seg_108" s="T7">наговаривать-CO-1SG.O</ta>
            <ta e="T9" id="Seg_109" s="T8">он(а).[NOM]</ta>
            <ta e="T10" id="Seg_110" s="T9">высохнуть-3SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_111" s="T1">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T3" id="Seg_112" s="T2">v-v:mood.[v:pn]</ta>
            <ta e="T4" id="Seg_113" s="T3">n.[n:case]</ta>
            <ta e="T5" id="Seg_114" s="T4">v-v:tense-v:mood.[v:pn]</ta>
            <ta e="T6" id="Seg_115" s="T5">pers</ta>
            <ta e="T7" id="Seg_116" s="T6">pers-n:ins-n:case</ta>
            <ta e="T8" id="Seg_117" s="T7">v-v:ins-v:pn</ta>
            <ta e="T9" id="Seg_118" s="T8">pers.[n:case]</ta>
            <ta e="T10" id="Seg_119" s="T9">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_120" s="T1">n</ta>
            <ta e="T3" id="Seg_121" s="T2">v</ta>
            <ta e="T4" id="Seg_122" s="T3">n</ta>
            <ta e="T5" id="Seg_123" s="T4">v</ta>
            <ta e="T6" id="Seg_124" s="T5">pers</ta>
            <ta e="T7" id="Seg_125" s="T6">pers</ta>
            <ta e="T8" id="Seg_126" s="T7">v</ta>
            <ta e="T9" id="Seg_127" s="T8">pers</ta>
            <ta e="T10" id="Seg_128" s="T9">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_129" s="T1">np:P 0.1.h:Poss</ta>
            <ta e="T4" id="Seg_130" s="T3">np:P</ta>
            <ta e="T6" id="Seg_131" s="T5">pro.h:A</ta>
            <ta e="T7" id="Seg_132" s="T6">pro:P</ta>
            <ta e="T9" id="Seg_133" s="T8">pro:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_134" s="T1">np:S</ta>
            <ta e="T3" id="Seg_135" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_136" s="T3">np:S</ta>
            <ta e="T5" id="Seg_137" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_138" s="T5">pro.h:S</ta>
            <ta e="T7" id="Seg_139" s="T6">pro:O</ta>
            <ta e="T8" id="Seg_140" s="T7">v:pred</ta>
            <ta e="T9" id="Seg_141" s="T8">pro:S</ta>
            <ta e="T10" id="Seg_142" s="T9">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T8" id="Seg_143" s="T7">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_144" s="T1">My leg hurts.</ta>
            <ta e="T5" id="Seg_145" s="T3">A furuncle appeared.</ta>
            <ta e="T8" id="Seg_146" s="T5">I casted a spell onto it.</ta>
            <ta e="T10" id="Seg_147" s="T8">It dried.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_148" s="T1">Mein Bein tut weh.</ta>
            <ta e="T5" id="Seg_149" s="T3">Eine Furunkel bildete sich.</ta>
            <ta e="T8" id="Seg_150" s="T5">Ich habe einen Zauber auf sie gelegt.</ta>
            <ta e="T10" id="Seg_151" s="T8">Sie ist ausgetrocknet.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_152" s="T1">У меня нога болит.</ta>
            <ta e="T5" id="Seg_153" s="T3">Чирий вскочил.</ta>
            <ta e="T8" id="Seg_154" s="T5">Я его заговариваю.</ta>
            <ta e="T10" id="Seg_155" s="T8">Он высох.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_156" s="T1">нога болит</ta>
            <ta e="T5" id="Seg_157" s="T3">чирий сел</ta>
            <ta e="T8" id="Seg_158" s="T5">я его наговариваю</ta>
            <ta e="T10" id="Seg_159" s="T8">он высох</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T3" id="Seg_160" s="T1">[BrM:] INFER or 3SG.O?</ta>
            <ta e="T5" id="Seg_161" s="T3"> ‎‎[BrM:] INFER or 3SG.O?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
