<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_LetterToMPP_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_LetterToMPP_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">759</ud-information>
            <ud-information attribute-name="# HIAT:w">566</ud-information>
            <ud-information attribute-name="# e">566</ud-information>
            <ud-information attribute-name="# HIAT:u">123</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
         <tli id="T243" />
         <tli id="T244" />
         <tli id="T245" />
         <tli id="T246" />
         <tli id="T247" />
         <tli id="T248" />
         <tli id="T249" />
         <tli id="T250" />
         <tli id="T251" />
         <tli id="T252" />
         <tli id="T253" />
         <tli id="T254" />
         <tli id="T255" />
         <tli id="T256" />
         <tli id="T257" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" />
         <tli id="T261" />
         <tli id="T262" />
         <tli id="T263" />
         <tli id="T264" />
         <tli id="T265" />
         <tli id="T266" />
         <tli id="T267" />
         <tli id="T268" />
         <tli id="T269" />
         <tli id="T270" />
         <tli id="T271" />
         <tli id="T272" />
         <tli id="T273" />
         <tli id="T274" />
         <tli id="T275" />
         <tli id="T276" />
         <tli id="T277" />
         <tli id="T278" />
         <tli id="T279" />
         <tli id="T280" />
         <tli id="T281" />
         <tli id="T282" />
         <tli id="T283" />
         <tli id="T284" />
         <tli id="T285" />
         <tli id="T286" />
         <tli id="T287" />
         <tli id="T288" />
         <tli id="T289" />
         <tli id="T290" />
         <tli id="T291" />
         <tli id="T292" />
         <tli id="T293" />
         <tli id="T294" />
         <tli id="T295" />
         <tli id="T296" />
         <tli id="T297" />
         <tli id="T298" />
         <tli id="T299" />
         <tli id="T300" />
         <tli id="T301" />
         <tli id="T302" />
         <tli id="T303" />
         <tli id="T304" />
         <tli id="T305" />
         <tli id="T306" />
         <tli id="T307" />
         <tli id="T308" />
         <tli id="T309" />
         <tli id="T310" />
         <tli id="T311" />
         <tli id="T312" />
         <tli id="T313" />
         <tli id="T314" />
         <tli id="T315" />
         <tli id="T316" />
         <tli id="T317" />
         <tli id="T318" />
         <tli id="T319" />
         <tli id="T320" />
         <tli id="T321" />
         <tli id="T322" />
         <tli id="T323" />
         <tli id="T324" />
         <tli id="T325" />
         <tli id="T326" />
         <tli id="T327" />
         <tli id="T328" />
         <tli id="T329" />
         <tli id="T330" />
         <tli id="T331" />
         <tli id="T332" />
         <tli id="T333" />
         <tli id="T334" />
         <tli id="T335" />
         <tli id="T336" />
         <tli id="T337" />
         <tli id="T338" />
         <tli id="T339" />
         <tli id="T340" />
         <tli id="T341" />
         <tli id="T342" />
         <tli id="T343" />
         <tli id="T344" />
         <tli id="T345" />
         <tli id="T346" />
         <tli id="T347" />
         <tli id="T348" />
         <tli id="T349" />
         <tli id="T350" />
         <tli id="T351" />
         <tli id="T352" />
         <tli id="T353" />
         <tli id="T354" />
         <tli id="T355" />
         <tli id="T356" />
         <tli id="T357" />
         <tli id="T358" />
         <tli id="T359" />
         <tli id="T360" />
         <tli id="T361" />
         <tli id="T362" />
         <tli id="T363" />
         <tli id="T364" />
         <tli id="T365" />
         <tli id="T366" />
         <tli id="T367" />
         <tli id="T368" />
         <tli id="T369" />
         <tli id="T370" />
         <tli id="T371" />
         <tli id="T372" />
         <tli id="T373" />
         <tli id="T374" />
         <tli id="T375" />
         <tli id="T376" />
         <tli id="T377" />
         <tli id="T378" />
         <tli id="T379" />
         <tli id="T380" />
         <tli id="T381" />
         <tli id="T382" />
         <tli id="T383" />
         <tli id="T384" />
         <tli id="T385" />
         <tli id="T386" />
         <tli id="T387" />
         <tli id="T388" />
         <tli id="T389" />
         <tli id="T390" />
         <tli id="T391" />
         <tli id="T392" />
         <tli id="T393" />
         <tli id="T394" />
         <tli id="T395" />
         <tli id="T396" />
         <tli id="T397" />
         <tli id="T398" />
         <tli id="T399" />
         <tli id="T400" />
         <tli id="T401" />
         <tli id="T402" />
         <tli id="T403" />
         <tli id="T404" />
         <tli id="T405" />
         <tli id="T406" />
         <tli id="T407" />
         <tli id="T408" />
         <tli id="T409" />
         <tli id="T410" />
         <tli id="T411" />
         <tli id="T412" />
         <tli id="T413" />
         <tli id="T414" />
         <tli id="T415" />
         <tli id="T416" />
         <tli id="T417" />
         <tli id="T418" />
         <tli id="T419" />
         <tli id="T420" />
         <tli id="T421" />
         <tli id="T422" />
         <tli id="T423" />
         <tli id="T424" />
         <tli id="T425" />
         <tli id="T426" />
         <tli id="T427" />
         <tli id="T428" />
         <tli id="T429" />
         <tli id="T430" />
         <tli id="T431" />
         <tli id="T432" />
         <tli id="T433" />
         <tli id="T434" />
         <tli id="T435" />
         <tli id="T436" />
         <tli id="T437" />
         <tli id="T438" />
         <tli id="T439" />
         <tli id="T440" />
         <tli id="T441" />
         <tli id="T442" />
         <tli id="T443" />
         <tli id="T444" />
         <tli id="T445" />
         <tli id="T446" />
         <tli id="T447" />
         <tli id="T448" />
         <tli id="T449" />
         <tli id="T450" />
         <tli id="T451" />
         <tli id="T452" />
         <tli id="T453" />
         <tli id="T454" />
         <tli id="T455" />
         <tli id="T456" />
         <tli id="T457" />
         <tli id="T458" />
         <tli id="T459" />
         <tli id="T460" />
         <tli id="T461" />
         <tli id="T462" />
         <tli id="T463" />
         <tli id="T464" />
         <tli id="T465" />
         <tli id="T466" />
         <tli id="T467" />
         <tli id="T468" />
         <tli id="T469" />
         <tli id="T470" />
         <tli id="T471" />
         <tli id="T472" />
         <tli id="T473" />
         <tli id="T474" />
         <tli id="T475" />
         <tli id="T476" />
         <tli id="T477" />
         <tli id="T478" />
         <tli id="T479" />
         <tli id="T480" />
         <tli id="T481" />
         <tli id="T482" />
         <tli id="T483" />
         <tli id="T484" />
         <tli id="T485" />
         <tli id="T486" />
         <tli id="T487" />
         <tli id="T488" />
         <tli id="T489" />
         <tli id="T490" />
         <tli id="T491" />
         <tli id="T492" />
         <tli id="T493" />
         <tli id="T494" />
         <tli id="T495" />
         <tli id="T496" />
         <tli id="T497" />
         <tli id="T498" />
         <tli id="T499" />
         <tli id="T500" />
         <tli id="T501" />
         <tli id="T502" />
         <tli id="T503" />
         <tli id="T504" />
         <tli id="T505" />
         <tli id="T506" />
         <tli id="T507" />
         <tli id="T508" />
         <tli id="T509" />
         <tli id="T510" />
         <tli id="T511" />
         <tli id="T512" />
         <tli id="T513" />
         <tli id="T514" />
         <tli id="T515" />
         <tli id="T516" />
         <tli id="T517" />
         <tli id="T518" />
         <tli id="T519" />
         <tli id="T520" />
         <tli id="T521" />
         <tli id="T522" />
         <tli id="T523" />
         <tli id="T524" />
         <tli id="T525" />
         <tli id="T526" />
         <tli id="T527" />
         <tli id="T528" />
         <tli id="T529" />
         <tli id="T530" />
         <tli id="T531" />
         <tli id="T532" />
         <tli id="T533" />
         <tli id="T534" />
         <tli id="T535" />
         <tli id="T536" />
         <tli id="T537" />
         <tli id="T538" />
         <tli id="T539" />
         <tli id="T540" />
         <tli id="T541" />
         <tli id="T542" />
         <tli id="T543" />
         <tli id="T544" />
         <tli id="T545" />
         <tli id="T546" />
         <tli id="T547" />
         <tli id="T548" />
         <tli id="T549" />
         <tli id="T550" />
         <tli id="T551" />
         <tli id="T552" />
         <tli id="T553" />
         <tli id="T554" />
         <tli id="T555" />
         <tli id="T556" />
         <tli id="T557" />
         <tli id="T558" />
         <tli id="T559" />
         <tli id="T560" />
         <tli id="T561" />
         <tli id="T562" />
         <tli id="T563" />
         <tli id="T564" />
         <tli id="T565" />
         <tli id="T566" />
         <tli id="T567" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T567" id="Seg_0" n="sc" s="T1">
               <ts e="T14" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Me</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">tamdʼel</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">amdaj</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">Angelina</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">Iwanownaze</ts>
                  <nts id="Seg_17" n="HIAT:ip">,</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_20" n="HIAT:w" s="T6">qulupbaj</ts>
                  <nts id="Seg_21" n="HIAT:ip">,</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_24" n="HIAT:w" s="T7">tan</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_27" n="HIAT:w" s="T8">dʼat</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_30" n="HIAT:w" s="T9">qulupbaj</ts>
                  <nts id="Seg_31" n="HIAT:ip">,</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">qutdär</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_37" n="HIAT:w" s="T11">warkatdə</ts>
                  <nts id="Seg_38" n="HIAT:ip">,</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_41" n="HIAT:w" s="T12">qajla</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_44" n="HIAT:w" s="T13">meːkutdal</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_48" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">Man</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">tebnä</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_56" n="HIAT:w" s="T16">kelʼlʼe</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_59" n="HIAT:w" s="T17">taːdərau</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_62" n="HIAT:w" s="T18">tan</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_65" n="HIAT:w" s="T19">dʼat</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_69" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_71" n="HIAT:w" s="T20">Man</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_74" n="HIAT:w" s="T21">tʼäran</ts>
                  <nts id="Seg_75" n="HIAT:ip">:</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_77" n="HIAT:ip">“</nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">Täp</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_82" n="HIAT:w" s="T23">pajäpba</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip">”</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_87" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">Man</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">täbnä</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_95" n="HIAT:w" s="T26">kessau</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_98" n="HIAT:w" s="T27">tan</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_101" n="HIAT:w" s="T28">mezuwi</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_104" n="HIAT:w" s="T29">Мarʼijaze</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_107" n="HIAT:w" s="T30">apstɨzat</ts>
                  <nts id="Seg_108" n="HIAT:ip">,</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_111" n="HIAT:w" s="T31">me</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_114" n="HIAT:w" s="T32">tezʼe</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_117" n="HIAT:w" s="T33">qutdär</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_120" n="HIAT:w" s="T34">ärzawtə</ts>
                  <nts id="Seg_121" n="HIAT:ip">.</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_124" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_126" n="HIAT:w" s="T35">Man</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_129" n="HIAT:w" s="T36">tebnä</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_132" n="HIAT:w" s="T37">wes</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_135" n="HIAT:w" s="T38">kessau</ts>
                  <nts id="Seg_136" n="HIAT:ip">.</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_139" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_141" n="HIAT:w" s="T39">Man</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_144" n="HIAT:w" s="T40">qulɨpbɨzan</ts>
                  <nts id="Seg_145" n="HIAT:ip">:</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_147" n="HIAT:ip">“</nts>
                  <ts e="T42" id="Seg_149" n="HIAT:w" s="T41">Tʼotka</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_152" n="HIAT:w" s="T42">Marʼina</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_155" n="HIAT:w" s="T43">nagɨr</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_158" n="HIAT:w" s="T44">nagɨrčeǯan</ts>
                  <nts id="Seg_159" n="HIAT:ip">.</nts>
                  <nts id="Seg_160" n="HIAT:ip">”</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_163" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_165" n="HIAT:w" s="T45">Puskaj</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_168" n="HIAT:w" s="T46">togulǯɨmdə</ts>
                  <nts id="Seg_169" n="HIAT:ip">.</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_172" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_174" n="HIAT:w" s="T47">Tʼolom</ts>
                  <nts id="Seg_175" n="HIAT:ip">,</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_178" n="HIAT:w" s="T48">Marʼina</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_181" n="HIAT:w" s="T49">Pawlowna</ts>
                  <nts id="Seg_182" n="HIAT:ip">!</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_185" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_187" n="HIAT:w" s="T50">Man</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_190" n="HIAT:w" s="T51">tekga</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_193" n="HIAT:w" s="T52">süsögə</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_196" n="HIAT:w" s="T53">nagɨr</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_199" n="HIAT:w" s="T54">nagɨrčeǯan</ts>
                  <nts id="Seg_200" n="HIAT:ip">.</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_203" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_205" n="HIAT:w" s="T55">Kazaxsen</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_208" n="HIAT:w" s="T56">man</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_211" n="HIAT:w" s="T57">ass</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_214" n="HIAT:w" s="T58">tunan</ts>
                  <nts id="Seg_215" n="HIAT:ip">.</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_218" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_220" n="HIAT:w" s="T59">Man</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_223" n="HIAT:w" s="T60">ilan</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_226" n="HIAT:w" s="T61">täpär</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_229" n="HIAT:w" s="T62">qwačoɣɨn</ts>
                  <nts id="Seg_230" n="HIAT:ip">,</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_233" n="HIAT:w" s="T63">Nowosibirskaɣɨn</ts>
                  <nts id="Seg_234" n="HIAT:ip">.</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_237" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_239" n="HIAT:w" s="T64">Qwačə</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_242" n="HIAT:w" s="T65">iːbɨɣaj</ts>
                  <nts id="Seg_243" n="HIAT:ip">,</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_246" n="HIAT:w" s="T66">soːdigaj</ts>
                  <nts id="Seg_247" n="HIAT:ip">.</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_250" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_252" n="HIAT:w" s="T67">Qwačoɣɨndə</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_255" n="HIAT:w" s="T68">Nowosibirskat</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_258" n="HIAT:w" s="T69">poisse</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_261" n="HIAT:w" s="T70">čaːʒɨzan</ts>
                  <nts id="Seg_262" n="HIAT:ip">.</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_265" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_267" n="HIAT:w" s="T71">Poiskan</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_270" n="HIAT:w" s="T72">čaʒəle</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_273" n="HIAT:w" s="T73">qozɨrčezawtə</ts>
                  <nts id="Seg_274" n="HIAT:ip">.</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_277" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_279" n="HIAT:w" s="T74">Tajoʒnɨj</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_282" n="HIAT:w" s="T75">stancɨɣɨn</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_285" n="HIAT:w" s="T76">pois</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_288" n="HIAT:w" s="T77">kuːnnɨn</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_291" n="HIAT:w" s="T78">nɨkgɨs</ts>
                  <nts id="Seg_292" n="HIAT:ip">.</nts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_295" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_297" n="HIAT:w" s="T79">Man</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_300" n="HIAT:w" s="T80">mannɨpɨzan</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_303" n="HIAT:w" s="T81">i</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_306" n="HIAT:w" s="T82">Nikinɨm</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_309" n="HIAT:w" s="T83">qajɣɨn</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_312" n="HIAT:w" s="T84">äss</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_315" n="HIAT:w" s="T85">qoǯirsau</ts>
                  <nts id="Seg_316" n="HIAT:ip">.</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_319" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_321" n="HIAT:w" s="T86">Patom</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_324" n="HIAT:w" s="T87">pois</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_327" n="HIAT:w" s="T88">üːban</ts>
                  <nts id="Seg_328" n="HIAT:ip">.</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_331" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_333" n="HIAT:w" s="T89">Man</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_336" n="HIAT:w" s="T90">qɨdan</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_339" n="HIAT:w" s="T91">akoškaɣɨn</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_342" n="HIAT:w" s="T92">mannɨpɨzan</ts>
                  <nts id="Seg_343" n="HIAT:ip">.</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_346" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_348" n="HIAT:w" s="T93">Jedla</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_351" n="HIAT:w" s="T94">koːcʼin</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_354" n="HIAT:w" s="T95">kalʼelʼlʼe</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_357" n="HIAT:w" s="T96">čaːʒɨzattə</ts>
                  <nts id="Seg_358" n="HIAT:ip">.</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_361" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_363" n="HIAT:w" s="T97">Qwačɨn</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_366" n="HIAT:w" s="T98">krajɣɨn</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_369" n="HIAT:w" s="T99">pola</ts>
                  <nts id="Seg_370" n="HIAT:ip">,</nts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_373" n="HIAT:w" s="T100">madʼila</ts>
                  <nts id="Seg_374" n="HIAT:ip">.</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_377" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_379" n="HIAT:w" s="T101">Krugom</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_382" n="HIAT:w" s="T102">sər</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_385" n="HIAT:w" s="T103">ippɨtda</ts>
                  <nts id="Seg_386" n="HIAT:ip">.</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_389" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_391" n="HIAT:w" s="T104">Man</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_394" n="HIAT:w" s="T105">poisqɨn</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_397" n="HIAT:w" s="T106">ass</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_400" n="HIAT:w" s="T107">warɨpɨzan</ts>
                  <nts id="Seg_401" n="HIAT:ip">.</nts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_404" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_406" n="HIAT:w" s="T108">Qɨːban</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_409" n="HIAT:w" s="T109">kogɨrɨt</ts>
                  <nts id="Seg_410" n="HIAT:ip">.</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_413" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_415" n="HIAT:w" s="T110">Ugon</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_418" n="HIAT:w" s="T111">man</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_421" n="HIAT:w" s="T112">ass</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_424" n="HIAT:w" s="T113">paldükuzan</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_427" n="HIAT:w" s="T114">poezlaze</ts>
                  <nts id="Seg_428" n="HIAT:ip">.</nts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_431" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_433" n="HIAT:w" s="T115">Man</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_436" n="HIAT:w" s="T116">elan</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_439" n="HIAT:w" s="T117">näjɣunnan</ts>
                  <nts id="Seg_440" n="HIAT:ip">,</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_443" n="HIAT:w" s="T118">mäɣuntə</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_446" n="HIAT:w" s="T119">na</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_449" n="HIAT:w" s="T120">tükkutda</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_452" n="HIAT:w" s="T121">süsöga</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_455" n="HIAT:w" s="T122">selam</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_458" n="HIAT:w" s="T123">nagɨrlʼe</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_461" n="HIAT:w" s="T124">taːdərɨst</ts>
                  <nts id="Seg_462" n="HIAT:ip">.</nts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_465" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_467" n="HIAT:w" s="T125">Qwatčɨze</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_470" n="HIAT:w" s="T126">näjɣum</ts>
                  <nts id="Seg_471" n="HIAT:ip">,</nts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_474" n="HIAT:w" s="T127">na</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_477" n="HIAT:w" s="T128">tükkɨt</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_480" n="HIAT:w" s="T129">da</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_483" n="HIAT:w" s="T130">Satdijedotdə</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_486" n="HIAT:w" s="T131">süsöɣə</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_489" n="HIAT:w" s="T132">äʒlam</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_492" n="HIAT:w" s="T133">nagɨrle</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_495" n="HIAT:w" s="T134">taːdɨrɨstə</ts>
                  <nts id="Seg_496" n="HIAT:ip">,</nts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_499" n="HIAT:w" s="T135">tekga</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_502" n="HIAT:w" s="T136">balʼdʼukus</ts>
                  <nts id="Seg_503" n="HIAT:ip">,</nts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_506" n="HIAT:w" s="T137">tazʼe</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_509" n="HIAT:w" s="T138">qɨːdan</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_512" n="HIAT:w" s="T139">nagɨrčukus</ts>
                  <nts id="Seg_513" n="HIAT:ip">.</nts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_516" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_518" n="HIAT:w" s="T140">Man</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_521" n="HIAT:w" s="T141">na</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_524" n="HIAT:w" s="T142">näjɣunnan</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_527" n="HIAT:w" s="T143">warkan</ts>
                  <nts id="Seg_528" n="HIAT:ip">,</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_531" n="HIAT:w" s="T144">Nowosibʼirskan</ts>
                  <nts id="Seg_532" n="HIAT:ip">.</nts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_535" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_537" n="HIAT:w" s="T145">Täbɨn</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_540" n="HIAT:w" s="T146">nimdə</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_543" n="HIAT:w" s="T147">Anglʼina</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_546" n="HIAT:w" s="T148">Iwanɨwna</ts>
                  <nts id="Seg_547" n="HIAT:ip">.</nts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_550" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_552" n="HIAT:w" s="T149">Man</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_555" n="HIAT:w" s="T150">tebnan</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_558" n="HIAT:w" s="T151">warkan</ts>
                  <nts id="Seg_559" n="HIAT:ip">.</nts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_562" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_564" n="HIAT:w" s="T152">Mazɨm</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_567" n="HIAT:w" s="T153">warɨn</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_570" n="HIAT:w" s="T154">soːn</ts>
                  <nts id="Seg_571" n="HIAT:ip">,</nts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_574" n="HIAT:w" s="T155">jas</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_577" n="HIAT:w" s="T156">qwɛdɨpɨqun</ts>
                  <nts id="Seg_578" n="HIAT:ip">.</nts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_581" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_583" n="HIAT:w" s="T157">Man</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_586" n="HIAT:w" s="T158">oːnän</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_589" n="HIAT:w" s="T159">melʼe</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_592" n="HIAT:w" s="T160">taːdərəkou</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_595" n="HIAT:w" s="T161">qaim</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_598" n="HIAT:w" s="T162">kɨgan</ts>
                  <nts id="Seg_599" n="HIAT:ip">.</nts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_602" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_604" n="HIAT:w" s="T163">Tebnan</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_607" n="HIAT:w" s="T164">nagur</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_610" n="HIAT:w" s="T165">sütʼdʼe</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_613" n="HIAT:w" s="T166">maːttə</ts>
                  <nts id="Seg_614" n="HIAT:ip">.</nts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_617" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_619" n="HIAT:w" s="T167">Maːdɨn</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_622" n="HIAT:w" s="T168">sütʼdʼila</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_625" n="HIAT:w" s="T169">warɣɨ</ts>
                  <nts id="Seg_626" n="HIAT:ip">.</nts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_629" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_631" n="HIAT:w" s="T170">Maːt</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_634" n="HIAT:w" s="T171">pötpa</ts>
                  <nts id="Seg_635" n="HIAT:ip">,</nts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_638" n="HIAT:w" s="T172">parawoje</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_641" n="HIAT:w" s="T173">ataplʼenʼje</ts>
                  <nts id="Seg_642" n="HIAT:ip">.</nts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_645" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_647" n="HIAT:w" s="T174">Üt</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_650" n="HIAT:w" s="T175">maːtqɨn</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_653" n="HIAT:w" s="T176">jen</ts>
                  <nts id="Seg_654" n="HIAT:ip">:</nts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_657" n="HIAT:w" s="T177">pärčɨtdə</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_660" n="HIAT:w" s="T178">üt</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_663" n="HIAT:w" s="T179">i</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_666" n="HIAT:w" s="T180">qannəbädi</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_669" n="HIAT:w" s="T181">üt</ts>
                  <nts id="Seg_670" n="HIAT:ip">.</nts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_673" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_675" n="HIAT:w" s="T182">Üt</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_678" n="HIAT:w" s="T183">wes</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_681" n="HIAT:w" s="T184">maːtqɨn</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_684" n="HIAT:w" s="T185">jewat</ts>
                  <nts id="Seg_685" n="HIAT:ip">.</nts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_688" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_690" n="HIAT:w" s="T186">Okkɨr</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_693" n="HIAT:w" s="T187">sütʼdʼäɣɨn</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_696" n="HIAT:w" s="T188">uːrgoftə</ts>
                  <nts id="Seg_697" n="HIAT:ip">.</nts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T195" id="Seg_700" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_702" n="HIAT:w" s="T189">Natʼen</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_705" n="HIAT:w" s="T190">pärčɨd</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_708" n="HIAT:w" s="T191">üt</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_711" n="HIAT:w" s="T192">i</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_714" n="HIAT:w" s="T193">qannäbädi</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_717" n="HIAT:w" s="T194">üt</ts>
                  <nts id="Seg_718" n="HIAT:ip">.</nts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_721" n="HIAT:u" s="T195">
                  <ts e="T196" id="Seg_723" n="HIAT:w" s="T195">Matqɨn</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_726" n="HIAT:w" s="T196">tütpukowtə</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_729" n="HIAT:w" s="T197">i</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_732" n="HIAT:w" s="T198">küzepukowtə</ts>
                  <nts id="Seg_733" n="HIAT:ip">.</nts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_736" n="HIAT:u" s="T199">
                  <ts e="T200" id="Seg_738" n="HIAT:w" s="T199">Wes</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_741" n="HIAT:w" s="T200">müzulʼǯɨlʼe</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_744" n="HIAT:w" s="T201">qwatdəqut</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_747" n="HIAT:w" s="T202">i</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_750" n="HIAT:w" s="T203">qaj</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_753" n="HIAT:w" s="T204">aptänäj</ts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_756" n="HIAT:w" s="T205">tʼäkku</ts>
                  <nts id="Seg_757" n="HIAT:ip">.</nts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T211" id="Seg_760" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_762" n="HIAT:w" s="T206">Cɨpočkam</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_765" n="HIAT:w" s="T207">nameʒalǯal</ts>
                  <nts id="Seg_766" n="HIAT:ip">,</nts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_769" n="HIAT:w" s="T208">üt</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_772" n="HIAT:w" s="T209">šoɣərlʼe</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_775" n="HIAT:w" s="T210">qalukun</ts>
                  <nts id="Seg_776" n="HIAT:ip">.</nts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_779" n="HIAT:u" s="T211">
                  <ts e="T212" id="Seg_781" n="HIAT:w" s="T211">Wes</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_784" n="HIAT:w" s="T212">mezulʼǯulʼe</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_787" n="HIAT:w" s="T213">meʒalgut</ts>
                  <nts id="Seg_788" n="HIAT:ip">.</nts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_791" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_793" n="HIAT:w" s="T214">Maːdla</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_796" n="HIAT:w" s="T215">tɨtdɨn</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_799" n="HIAT:w" s="T216">iːbɨgaj</ts>
                  <nts id="Seg_800" n="HIAT:ip">,</nts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_803" n="HIAT:w" s="T217">teːttə</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_806" n="HIAT:w" s="T218">ätaš</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_809" n="HIAT:w" s="T219">maːdla</ts>
                  <nts id="Seg_810" n="HIAT:ip">,</nts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_813" n="HIAT:w" s="T220">wes</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_816" n="HIAT:w" s="T221">okkɨr</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_819" n="HIAT:w" s="T222">saj</ts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_822" n="HIAT:w" s="T223">maːdla</ts>
                  <nts id="Seg_823" n="HIAT:ip">.</nts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_826" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_828" n="HIAT:w" s="T224">Maːdla</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_831" n="HIAT:w" s="T225">tüpba</ts>
                  <nts id="Seg_832" n="HIAT:ip">.</nts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_835" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_837" n="HIAT:w" s="T226">Ass</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_840" n="HIAT:w" s="T227">qutdoqɨn</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_843" n="HIAT:w" s="T228">pola</ts>
                  <nts id="Seg_844" n="HIAT:ip">:</nts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_847" n="HIAT:w" s="T229">kəla</ts>
                  <nts id="Seg_848" n="HIAT:ip">,</nts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_851" n="HIAT:w" s="T230">üdəpiːla</ts>
                  <nts id="Seg_852" n="HIAT:ip">,</nts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_855" n="HIAT:w" s="T231">jolkala</ts>
                  <nts id="Seg_856" n="HIAT:ip">,</nts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_859" n="HIAT:w" s="T232">sosnala</ts>
                  <nts id="Seg_860" n="HIAT:ip">,</nts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_863" n="HIAT:w" s="T233">tʼeunpola</ts>
                  <nts id="Seg_864" n="HIAT:ip">,</nts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_867" n="HIAT:w" s="T234">pila</ts>
                  <nts id="Seg_868" n="HIAT:ip">.</nts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T240" id="Seg_871" n="HIAT:u" s="T235">
                  <ts e="T236" id="Seg_873" n="HIAT:w" s="T235">Tautʼen</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_876" n="HIAT:w" s="T236">taɣɨn</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_879" n="HIAT:w" s="T237">koːcʼin</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_882" n="HIAT:w" s="T238">krɨbɨla</ts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_885" n="HIAT:w" s="T239">tʼelɨmkun</ts>
                  <nts id="Seg_886" n="HIAT:ip">.</nts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_889" n="HIAT:u" s="T240">
                  <ts e="T241" id="Seg_891" n="HIAT:w" s="T240">Me</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_894" n="HIAT:w" s="T241">taɣɨn</ts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_897" n="HIAT:w" s="T242">krɨbɨ</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_900" n="HIAT:w" s="T243">taktätǯutu</ts>
                  <nts id="Seg_901" n="HIAT:ip">.</nts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T248" id="Seg_904" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_906" n="HIAT:w" s="T244">Üdɨn</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_909" n="HIAT:w" s="T245">kocʼin</ts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_912" n="HIAT:w" s="T246">jeqwattə</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_915" n="HIAT:w" s="T247">swetoɣɨla</ts>
                  <nts id="Seg_916" n="HIAT:ip">.</nts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_919" n="HIAT:u" s="T248">
                  <ts e="T249" id="Seg_921" n="HIAT:w" s="T248">Qaːn</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_924" n="HIAT:w" s="T249">tolʼdʼizʼe</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_927" n="HIAT:w" s="T250">paldʼättə</ts>
                  <nts id="Seg_928" n="HIAT:ip">.</nts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T254" id="Seg_931" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_933" n="HIAT:w" s="T251">Qɨbanʼäʒala</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_936" n="HIAT:w" s="T252">salaskaze</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_939" n="HIAT:w" s="T253">nezarnattə</ts>
                  <nts id="Seg_940" n="HIAT:ip">.</nts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_943" n="HIAT:u" s="T254">
                  <ts e="T255" id="Seg_945" n="HIAT:w" s="T254">Aran</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_948" n="HIAT:w" s="T255">pon</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_951" n="HIAT:w" s="T256">tʼäbla</ts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_954" n="HIAT:w" s="T257">wes</ts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_957" n="HIAT:w" s="T258">šoltana</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_960" n="HIAT:w" s="T259">azölʼeǯattə</ts>
                  <nts id="Seg_961" n="HIAT:ip">.</nts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T267" id="Seg_964" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_966" n="HIAT:w" s="T260">Qusaqɨn</ts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_969" n="HIAT:w" s="T261">me</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_972" n="HIAT:w" s="T262">tüzawtə</ts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_975" n="HIAT:w" s="T263">Nowosibirskat</ts>
                  <nts id="Seg_976" n="HIAT:ip">,</nts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_979" n="HIAT:w" s="T264">seqɨzawtə</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_982" n="HIAT:w" s="T265">Nowosibirskaɣɨn</ts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_985" n="HIAT:w" s="T266">wakzalɣɨn</ts>
                  <nts id="Seg_986" n="HIAT:ip">.</nts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T270" id="Seg_989" n="HIAT:u" s="T267">
                  <ts e="T268" id="Seg_991" n="HIAT:w" s="T267">Innäj</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_994" n="HIAT:w" s="T268">ətašqɨn</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_997" n="HIAT:w" s="T269">seqɨzawtə</ts>
                  <nts id="Seg_998" n="HIAT:ip">.</nts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T273" id="Seg_1001" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_1003" n="HIAT:w" s="T270">Soblamtätte</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1006" n="HIAT:w" s="T271">ätašxɨn</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1009" n="HIAT:w" s="T272">qotdɨzawtə</ts>
                  <nts id="Seg_1010" n="HIAT:ip">.</nts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T279" id="Seg_1013" n="HIAT:u" s="T273">
                  <ts e="T274" id="Seg_1015" n="HIAT:w" s="T273">Mʼäɣuntu</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1018" n="HIAT:w" s="T274">wes</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1021" n="HIAT:w" s="T275">mewattə</ts>
                  <nts id="Seg_1022" n="HIAT:ip">:</nts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1025" n="HIAT:w" s="T276">matrazlam</ts>
                  <nts id="Seg_1026" n="HIAT:ip">,</nts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1029" n="HIAT:w" s="T277">adʼejallam</ts>
                  <nts id="Seg_1030" n="HIAT:ip">,</nts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1033" n="HIAT:w" s="T278">paduškalam</ts>
                  <nts id="Seg_1034" n="HIAT:ip">.</nts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T281" id="Seg_1037" n="HIAT:u" s="T279">
                  <ts e="T280" id="Seg_1039" n="HIAT:w" s="T279">Qotdəzawtə</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1042" n="HIAT:w" s="T280">soːdʼigan</ts>
                  <nts id="Seg_1043" n="HIAT:ip">.</nts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T284" id="Seg_1046" n="HIAT:u" s="T281">
                  <ts e="T282" id="Seg_1048" n="HIAT:w" s="T281">Qula</ts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1051" n="HIAT:w" s="T282">koːcʼin</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1054" n="HIAT:w" s="T283">jezattə</ts>
                  <nts id="Seg_1055" n="HIAT:ip">.</nts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_1058" n="HIAT:u" s="T284">
                  <ts e="T285" id="Seg_1060" n="HIAT:w" s="T284">Iːbɨɣaj</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1063" n="HIAT:w" s="T285">wakzal</ts>
                  <nts id="Seg_1064" n="HIAT:ip">.</nts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T290" id="Seg_1067" n="HIAT:u" s="T286">
                  <ts e="T287" id="Seg_1069" n="HIAT:w" s="T286">Natʼen</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1072" n="HIAT:w" s="T287">maːɣɨlǯənnaš</ts>
                  <nts id="Seg_1073" n="HIAT:ip">,</nts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1076" n="HIAT:w" s="T288">jaš</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1079" n="HIAT:w" s="T289">čannännaš</ts>
                  <nts id="Seg_1080" n="HIAT:ip">.</nts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T293" id="Seg_1083" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_1085" n="HIAT:w" s="T290">Qarʼemɨɣɨn</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1088" n="HIAT:w" s="T291">qwannawtə</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1091" n="HIAT:w" s="T292">stalowajtə</ts>
                  <nts id="Seg_1092" n="HIAT:ip">.</nts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T296" id="Seg_1095" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_1097" n="HIAT:w" s="T293">Tebla</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1100" n="HIAT:w" s="T294">ketqwattə</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1103" n="HIAT:w" s="T295">restoran</ts>
                  <nts id="Seg_1104" n="HIAT:ip">.</nts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T301" id="Seg_1107" n="HIAT:u" s="T296">
                  <ts e="T297" id="Seg_1109" n="HIAT:w" s="T296">Natʼen</ts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1112" n="HIAT:w" s="T297">soːdigan</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1115" n="HIAT:w" s="T298">jen</ts>
                  <nts id="Seg_1116" n="HIAT:ip">,</nts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1119" n="HIAT:w" s="T299">rajin</ts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1122" n="HIAT:w" s="T300">sütdə</ts>
                  <nts id="Seg_1123" n="HIAT:ip">.</nts>
                  <nts id="Seg_1124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T304" id="Seg_1126" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_1128" n="HIAT:w" s="T301">Natʼen</ts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1131" n="HIAT:w" s="T302">i</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1134" n="HIAT:w" s="T303">aursawtə</ts>
                  <nts id="Seg_1135" n="HIAT:ip">.</nts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_1138" n="HIAT:u" s="T304">
                  <ts e="T305" id="Seg_1140" n="HIAT:w" s="T304">Menan</ts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1143" n="HIAT:w" s="T305">more</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1146" n="HIAT:w" s="T306">čaʒekan</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1149" n="HIAT:w" s="T307">jen</ts>
                  <nts id="Seg_1150" n="HIAT:ip">.</nts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T312" id="Seg_1153" n="HIAT:u" s="T308">
                  <ts e="T309" id="Seg_1155" n="HIAT:w" s="T308">Natʼen</ts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1158" n="HIAT:w" s="T309">qula</ts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1161" n="HIAT:w" s="T310">qwɛlɨnʼättə</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1164" n="HIAT:w" s="T311">sɨbɨqse</ts>
                  <nts id="Seg_1165" n="HIAT:ip">.</nts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T316" id="Seg_1168" n="HIAT:u" s="T312">
                  <ts e="T313" id="Seg_1170" n="HIAT:w" s="T312">Nɨrsala</ts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1173" n="HIAT:w" s="T313">qwatqudattə</ts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1176" n="HIAT:w" s="T314">i</ts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1179" n="HIAT:w" s="T315">pʼedʼela</ts>
                  <nts id="Seg_1180" n="HIAT:ip">.</nts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T322" id="Seg_1183" n="HIAT:u" s="T316">
                  <ts e="T317" id="Seg_1185" n="HIAT:w" s="T316">Qɨbanädäkqa</ts>
                  <nts id="Seg_1186" n="HIAT:ip">,</nts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1189" n="HIAT:w" s="T317">xazʼäjkan</ts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1192" n="HIAT:w" s="T318">nägat</ts>
                  <nts id="Seg_1193" n="HIAT:ip">,</nts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1196" n="HIAT:w" s="T319">tätkustə</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1199" n="HIAT:w" s="T320">nɨrsan</ts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1202" n="HIAT:w" s="T321">tassɨt</ts>
                  <nts id="Seg_1203" n="HIAT:ip">.</nts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T324" id="Seg_1206" n="HIAT:u" s="T322">
                  <ts e="T323" id="Seg_1208" n="HIAT:w" s="T322">Nɨrsa</ts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1211" n="HIAT:w" s="T323">qattädʼipba</ts>
                  <nts id="Seg_1212" n="HIAT:ip">.</nts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_1215" n="HIAT:u" s="T324">
                  <ts e="T325" id="Seg_1217" n="HIAT:w" s="T324">Koška</ts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1220" n="HIAT:w" s="T325">täbɨm</ts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1223" n="HIAT:w" s="T326">apsɨt</ts>
                  <nts id="Seg_1224" n="HIAT:ip">.</nts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T331" id="Seg_1227" n="HIAT:u" s="T327">
                  <ts e="T328" id="Seg_1229" n="HIAT:w" s="T327">Mannan</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1232" n="HIAT:w" s="T328">sət</ts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1235" n="HIAT:w" s="T329">padrugau</ts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1238" n="HIAT:w" s="T330">jewa</ts>
                  <nts id="Seg_1239" n="HIAT:ip">.</nts>
                  <nts id="Seg_1240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T334" id="Seg_1242" n="HIAT:u" s="T331">
                  <ts e="T332" id="Seg_1244" n="HIAT:w" s="T331">Man</ts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1247" n="HIAT:w" s="T332">teblanä</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1250" n="HIAT:w" s="T333">palʼdʼüqwan</ts>
                  <nts id="Seg_1251" n="HIAT:ip">.</nts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T338" id="Seg_1254" n="HIAT:u" s="T334">
                  <ts e="T335" id="Seg_1256" n="HIAT:w" s="T334">I</ts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1259" n="HIAT:w" s="T335">tebla</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1262" n="HIAT:w" s="T336">mekka</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1265" n="HIAT:w" s="T337">palʼdʼüqwattə</ts>
                  <nts id="Seg_1266" n="HIAT:ip">.</nts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T341" id="Seg_1269" n="HIAT:u" s="T338">
                  <ts e="T339" id="Seg_1271" n="HIAT:w" s="T338">Man</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1274" n="HIAT:w" s="T339">sormeǯan</ts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1277" n="HIAT:w" s="T340">poqqɨlaj</ts>
                  <nts id="Seg_1278" n="HIAT:ip">.</nts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T345" id="Seg_1281" n="HIAT:u" s="T341">
                  <ts e="T342" id="Seg_1283" n="HIAT:w" s="T341">Ola</ts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1286" n="HIAT:w" s="T342">amdɨgu</ts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1289" n="HIAT:w" s="T343">ass</ts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1292" n="HIAT:w" s="T344">kɨgan</ts>
                  <nts id="Seg_1293" n="HIAT:ip">.</nts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T349" id="Seg_1296" n="HIAT:u" s="T345">
                  <ts e="T346" id="Seg_1298" n="HIAT:w" s="T345">A</ts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1301" n="HIAT:w" s="T346">onän</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1304" n="HIAT:w" s="T347">kɨdan</ts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1307" n="HIAT:w" s="T348">qojmutčukan</ts>
                  <nts id="Seg_1308" n="HIAT:ip">.</nts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T356" id="Seg_1311" n="HIAT:u" s="T349">
                  <ts e="T350" id="Seg_1313" n="HIAT:w" s="T349">Man</ts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1316" n="HIAT:w" s="T350">üdɨn</ts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1319" n="HIAT:w" s="T351">Satdijedot</ts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1322" n="HIAT:w" s="T352">tütǯan</ts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1325" n="HIAT:w" s="T353">qwälɨsku</ts>
                  <nts id="Seg_1326" n="HIAT:ip">,</nts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1329" n="HIAT:w" s="T354">čobərɨm</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1332" n="HIAT:w" s="T355">qwadəgu</ts>
                  <nts id="Seg_1333" n="HIAT:ip">.</nts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T359" id="Seg_1336" n="HIAT:u" s="T356">
                  <ts e="T357" id="Seg_1338" n="HIAT:w" s="T356">Qwače</ts>
                  <nts id="Seg_1339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1341" n="HIAT:w" s="T357">iːbɨgan</ts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1344" n="HIAT:w" s="T358">jen</ts>
                  <nts id="Seg_1345" n="HIAT:ip">.</nts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T366" id="Seg_1348" n="HIAT:u" s="T359">
                  <ts e="T360" id="Seg_1350" n="HIAT:w" s="T359">Man</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1353" n="HIAT:w" s="T360">na</ts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1356" n="HIAT:w" s="T361">tünɨǯan</ts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1359" n="HIAT:w" s="T362">i</ts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1362" n="HIAT:w" s="T363">tekga</ts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1365" n="HIAT:w" s="T364">aːdulǯeǯau</ts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1368" n="HIAT:w" s="T365">qwačɨn</ts>
                  <nts id="Seg_1369" n="HIAT:ip">.</nts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T370" id="Seg_1372" n="HIAT:u" s="T366">
                  <ts e="T367" id="Seg_1374" n="HIAT:w" s="T366">Tekga</ts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1377" n="HIAT:w" s="T367">na</ts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1380" n="HIAT:w" s="T368">qwatčɨn</ts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1383" n="HIAT:w" s="T369">tatčau</ts>
                  <nts id="Seg_1384" n="HIAT:ip">.</nts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T372" id="Seg_1387" n="HIAT:u" s="T370">
                  <ts e="T371" id="Seg_1389" n="HIAT:w" s="T370">Tan</ts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1392" n="HIAT:w" s="T371">mannämeːǯal</ts>
                  <nts id="Seg_1393" n="HIAT:ip">.</nts>
                  <nts id="Seg_1394" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_1396" n="HIAT:u" s="T372">
                  <ts e="T373" id="Seg_1398" n="HIAT:w" s="T372">Tawtʼen</ts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1401" n="HIAT:w" s="T373">wsʼäkij</ts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1404" n="HIAT:w" s="T374">mašinalazʼe</ts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1407" n="HIAT:w" s="T375">palʼdʼizan</ts>
                  <nts id="Seg_1408" n="HIAT:ip">.</nts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T379" id="Seg_1411" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_1413" n="HIAT:w" s="T376">Perwɨj</ts>
                  <nts id="Seg_1414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1416" n="HIAT:w" s="T377">ras</ts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1419" n="HIAT:w" s="T378">larɨpɨzan</ts>
                  <nts id="Seg_1420" n="HIAT:ip">.</nts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T383" id="Seg_1423" n="HIAT:u" s="T379">
                  <ts e="T380" id="Seg_1425" n="HIAT:w" s="T379">Mazɨm</ts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1428" n="HIAT:w" s="T380">Valodʼä</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1431" n="HIAT:w" s="T381">qwačoɣɨn</ts>
                  <nts id="Seg_1432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1434" n="HIAT:w" s="T382">warɨs</ts>
                  <nts id="Seg_1435" n="HIAT:ip">.</nts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T385" id="Seg_1438" n="HIAT:u" s="T383">
                  <ts e="T384" id="Seg_1440" n="HIAT:w" s="T383">Wes</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1443" n="HIAT:w" s="T384">aːdulǯɨɣɨsɨt</ts>
                  <nts id="Seg_1444" n="HIAT:ip">.</nts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_1447" n="HIAT:u" s="T385">
                  <ts e="T386" id="Seg_1449" n="HIAT:w" s="T385">Onät</ts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1452" n="HIAT:w" s="T386">də</ts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1455" n="HIAT:w" s="T387">qu</ts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1458" n="HIAT:w" s="T388">qwannaš</ts>
                  <nts id="Seg_1459" n="HIAT:ip">,</nts>
                  <nts id="Seg_1460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1462" n="HIAT:w" s="T389">maɣɨlʼǯənnaš</ts>
                  <nts id="Seg_1463" n="HIAT:ip">.</nts>
                  <nts id="Seg_1464" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1466" n="HIAT:u" s="T390">
                  <ts e="T391" id="Seg_1468" n="HIAT:w" s="T390">Man</ts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1471" n="HIAT:w" s="T391">küssukumnan</ts>
                  <nts id="Seg_1472" n="HIAT:ip">,</nts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1475" n="HIAT:w" s="T392">tʼäran</ts>
                  <nts id="Seg_1476" n="HIAT:ip">:</nts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1478" n="HIAT:ip">“</nts>
                  <ts e="T394" id="Seg_1480" n="HIAT:w" s="T393">Küzugu</ts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1483" n="HIAT:w" s="T394">nado</ts>
                  <nts id="Seg_1484" n="HIAT:ip">.</nts>
                  <nts id="Seg_1485" n="HIAT:ip">”</nts>
                  <nts id="Seg_1486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T401" id="Seg_1488" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_1490" n="HIAT:w" s="T395">A</ts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1493" n="HIAT:w" s="T396">Valodʼa</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1496" n="HIAT:w" s="T397">mekga</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1499" n="HIAT:w" s="T398">tʼärɨn</ts>
                  <nts id="Seg_1500" n="HIAT:ip">:</nts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1502" n="HIAT:ip">“</nts>
                  <ts e="T400" id="Seg_1504" n="HIAT:w" s="T399">Qu</ts>
                  <nts id="Seg_1505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1507" n="HIAT:w" s="T400">küzenaš</ts>
                  <nts id="Seg_1508" n="HIAT:ip">?</nts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T404" id="Seg_1511" n="HIAT:u" s="T401">
                  <ts e="T402" id="Seg_1513" n="HIAT:w" s="T401">Küzəpbədi</ts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1516" n="HIAT:w" s="T402">mɨ</ts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1519" n="HIAT:w" s="T403">tʼäkgu</ts>
                  <nts id="Seg_1520" n="HIAT:ip">.</nts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T410" id="Seg_1523" n="HIAT:u" s="T404">
                  <ts e="T405" id="Seg_1525" n="HIAT:w" s="T404">Küzəpbədi</ts>
                  <nts id="Seg_1526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1528" n="HIAT:w" s="T405">mɨla</ts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1531" n="HIAT:w" s="T406">wes</ts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1534" n="HIAT:w" s="T407">madɨn</ts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1537" n="HIAT:w" s="T408">sütʼdʼäɣɨn</ts>
                  <nts id="Seg_1538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1540" n="HIAT:w" s="T409">jewattə</ts>
                  <nts id="Seg_1541" n="HIAT:ip">.</nts>
                  <nts id="Seg_1542" n="HIAT:ip">”</nts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T412" id="Seg_1545" n="HIAT:u" s="T410">
                  <ts e="T411" id="Seg_1547" n="HIAT:w" s="T410">Pazarɣɨn</ts>
                  <nts id="Seg_1548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1550" n="HIAT:w" s="T411">jezaj</ts>
                  <nts id="Seg_1551" n="HIAT:ip">.</nts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T419" id="Seg_1554" n="HIAT:u" s="T412">
                  <ts e="T413" id="Seg_1556" n="HIAT:w" s="T412">Täp</ts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1559" n="HIAT:w" s="T413">mekga</ts>
                  <nts id="Seg_1560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1562" n="HIAT:w" s="T414">tʼärɨn</ts>
                  <nts id="Seg_1563" n="HIAT:ip">:</nts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1565" n="HIAT:ip">“</nts>
                  <ts e="T416" id="Seg_1567" n="HIAT:w" s="T415">Natʼent</ts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1570" n="HIAT:w" s="T416">qajno</ts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1573" n="HIAT:w" s="T417">as</ts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1576" n="HIAT:w" s="T418">küzəzat</ts>
                  <nts id="Seg_1577" n="HIAT:ip">?</nts>
                  <nts id="Seg_1578" n="HIAT:ip">”</nts>
                  <nts id="Seg_1579" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T423" id="Seg_1581" n="HIAT:u" s="T419">
                  <ts e="T420" id="Seg_1583" n="HIAT:w" s="T419">A</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1586" n="HIAT:w" s="T420">man</ts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1589" n="HIAT:w" s="T421">ass</ts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1592" n="HIAT:w" s="T422">küzɨzaːn</ts>
                  <nts id="Seg_1593" n="HIAT:ip">.</nts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T426" id="Seg_1596" n="HIAT:u" s="T423">
                  <ts e="T424" id="Seg_1598" n="HIAT:w" s="T423">Tak</ts>
                  <nts id="Seg_1599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1601" n="HIAT:w" s="T424">man</ts>
                  <nts id="Seg_1602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1604" n="HIAT:w" s="T425">tʼärpipɨzau</ts>
                  <nts id="Seg_1605" n="HIAT:ip">.</nts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T436" id="Seg_1608" n="HIAT:u" s="T426">
                  <ts e="T427" id="Seg_1610" n="HIAT:w" s="T426">Qusakɨn</ts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1613" n="HIAT:w" s="T427">mašinam</ts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1616" n="HIAT:w" s="T428">aːdɨzaj</ts>
                  <nts id="Seg_1617" n="HIAT:ip">,</nts>
                  <nts id="Seg_1618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1620" n="HIAT:w" s="T429">man</ts>
                  <nts id="Seg_1621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1623" n="HIAT:w" s="T430">okkɨr</ts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1626" n="HIAT:w" s="T431">näjɣumnä</ts>
                  <nts id="Seg_1627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1629" n="HIAT:w" s="T432">soɣutdʼan</ts>
                  <nts id="Seg_1630" n="HIAT:ip">:</nts>
                  <nts id="Seg_1631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1632" n="HIAT:ip">“</nts>
                  <ts e="T434" id="Seg_1634" n="HIAT:w" s="T433">Qutʼen</ts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1637" n="HIAT:w" s="T434">küzəpbədi</ts>
                  <nts id="Seg_1638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1640" n="HIAT:w" s="T435">maːt</ts>
                  <nts id="Seg_1641" n="HIAT:ip">?</nts>
                  <nts id="Seg_1642" n="HIAT:ip">”</nts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T440" id="Seg_1645" n="HIAT:u" s="T436">
                  <ts e="T437" id="Seg_1647" n="HIAT:w" s="T436">Tep</ts>
                  <nts id="Seg_1648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1650" n="HIAT:w" s="T437">mekga</ts>
                  <nts id="Seg_1651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1653" n="HIAT:w" s="T438">aːdulǯɨt</ts>
                  <nts id="Seg_1654" n="HIAT:ip">:</nts>
                  <nts id="Seg_1655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1656" n="HIAT:ip">“</nts>
                  <ts e="T440" id="Seg_1658" n="HIAT:w" s="T439">Tutʼetdo</ts>
                  <nts id="Seg_1659" n="HIAT:ip">.</nts>
                  <nts id="Seg_1660" n="HIAT:ip">”</nts>
                  <nts id="Seg_1661" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T444" id="Seg_1663" n="HIAT:u" s="T440">
                  <ts e="T441" id="Seg_1665" n="HIAT:w" s="T440">Man</ts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1668" n="HIAT:w" s="T441">kuronnan</ts>
                  <nts id="Seg_1669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1671" n="HIAT:w" s="T442">i</ts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1674" n="HIAT:w" s="T443">küzan</ts>
                  <nts id="Seg_1675" n="HIAT:ip">.</nts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T449" id="Seg_1678" n="HIAT:u" s="T444">
                  <ts e="T445" id="Seg_1680" n="HIAT:w" s="T444">Man</ts>
                  <nts id="Seg_1681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1683" n="HIAT:w" s="T445">täjerbɨzan</ts>
                  <nts id="Seg_1684" n="HIAT:ip">,</nts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1687" n="HIAT:w" s="T446">küzeu</ts>
                  <nts id="Seg_1688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1690" n="HIAT:w" s="T447">wal</ts>
                  <nts id="Seg_1691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1693" n="HIAT:w" s="T448">waǯən</ts>
                  <nts id="Seg_1694" n="HIAT:ip">.</nts>
                  <nts id="Seg_1695" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_1697" n="HIAT:u" s="T449">
                  <ts e="T450" id="Seg_1699" n="HIAT:w" s="T449">Küzan</ts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1702" n="HIAT:w" s="T450">i</ts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1705" n="HIAT:w" s="T451">soːn</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1708" n="HIAT:w" s="T452">azun</ts>
                  <nts id="Seg_1709" n="HIAT:ip">.</nts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T457" id="Seg_1712" n="HIAT:u" s="T453">
                  <ts e="T454" id="Seg_1714" n="HIAT:w" s="T453">Magazinla</ts>
                  <nts id="Seg_1715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1717" n="HIAT:w" s="T454">serkaj</ts>
                  <nts id="Seg_1718" n="HIAT:ip">,</nts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1721" n="HIAT:w" s="T455">iːbɨgaj</ts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1724" n="HIAT:w" s="T456">magazilla</ts>
                  <nts id="Seg_1725" n="HIAT:ip">.</nts>
                  <nts id="Seg_1726" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T459" id="Seg_1728" n="HIAT:u" s="T457">
                  <ts e="T458" id="Seg_1730" n="HIAT:w" s="T457">Natʼen</ts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1733" n="HIAT:w" s="T458">maɣulǯennaš</ts>
                  <nts id="Seg_1734" n="HIAT:ip">.</nts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T463" id="Seg_1737" n="HIAT:u" s="T459">
                  <ts e="T460" id="Seg_1739" n="HIAT:w" s="T459">Man</ts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1742" n="HIAT:w" s="T460">nagɨrɨm</ts>
                  <nts id="Seg_1743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1745" n="HIAT:w" s="T461">ass</ts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1748" n="HIAT:w" s="T462">tunou</ts>
                  <nts id="Seg_1749" n="HIAT:ip">.</nts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T469" id="Seg_1752" n="HIAT:u" s="T463">
                  <ts e="T464" id="Seg_1754" n="HIAT:w" s="T463">Oːnän</ts>
                  <nts id="Seg_1755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1757" n="HIAT:w" s="T464">ass</ts>
                  <nts id="Seg_1758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1760" n="HIAT:w" s="T465">paldʼiqwan</ts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1763" n="HIAT:w" s="T466">qwatčoɣɨn</ts>
                  <nts id="Seg_1764" n="HIAT:ip">,</nts>
                  <nts id="Seg_1765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1767" n="HIAT:w" s="T467">ato</ts>
                  <nts id="Seg_1768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1770" n="HIAT:w" s="T468">maɣɨlǯəǯan</ts>
                  <nts id="Seg_1771" n="HIAT:ip">.</nts>
                  <nts id="Seg_1772" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T473" id="Seg_1774" n="HIAT:u" s="T469">
                  <ts e="T470" id="Seg_1776" n="HIAT:w" s="T469">Man</ts>
                  <nts id="Seg_1777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1779" n="HIAT:w" s="T470">täpär</ts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1782" n="HIAT:w" s="T471">mašinalaze</ts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1785" n="HIAT:w" s="T472">paldʼän</ts>
                  <nts id="Seg_1786" n="HIAT:ip">.</nts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T479" id="Seg_1789" n="HIAT:u" s="T473">
                  <ts e="T474" id="Seg_1791" n="HIAT:w" s="T473">Ilɨzan</ts>
                  <nts id="Seg_1792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1794" n="HIAT:w" s="T474">qwatčoɣɨn</ts>
                  <nts id="Seg_1795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1797" n="HIAT:w" s="T475">ČʼornajaRečʼkaɣɨn</ts>
                  <nts id="Seg_1798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1800" n="HIAT:w" s="T476">i</ts>
                  <nts id="Seg_1801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1803" n="HIAT:w" s="T477">Taxtamɨšowaɣɨn</ts>
                  <nts id="Seg_1804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1806" n="HIAT:w" s="T478">ilɨzan</ts>
                  <nts id="Seg_1807" n="HIAT:ip">.</nts>
                  <nts id="Seg_1808" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T482" id="Seg_1810" n="HIAT:u" s="T479">
                  <ts e="T480" id="Seg_1812" n="HIAT:w" s="T479">Dunʼa</ts>
                  <nts id="Seg_1813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1815" n="HIAT:w" s="T480">tʼüs</ts>
                  <nts id="Seg_1816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1818" n="HIAT:w" s="T481">ČʼornajaRʼečʼkat</ts>
                  <nts id="Seg_1819" n="HIAT:ip">.</nts>
                  <nts id="Seg_1820" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T485" id="Seg_1822" n="HIAT:u" s="T482">
                  <ts e="T483" id="Seg_1824" n="HIAT:w" s="T482">Iːtdɨzʼe</ts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1827" n="HIAT:w" s="T483">täbɨstaɣə</ts>
                  <nts id="Seg_1828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1830" n="HIAT:w" s="T484">warkaɣə</ts>
                  <nts id="Seg_1831" n="HIAT:ip">.</nts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T489" id="Seg_1834" n="HIAT:u" s="T485">
                  <ts e="T486" id="Seg_1836" n="HIAT:w" s="T485">Mikalaj</ts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1839" n="HIAT:w" s="T486">Lazarɨčʼin</ts>
                  <nts id="Seg_1840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1842" n="HIAT:w" s="T487">pajagat</ts>
                  <nts id="Seg_1843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1845" n="HIAT:w" s="T488">qua</ts>
                  <nts id="Seg_1846" n="HIAT:ip">.</nts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T492" id="Seg_1849" n="HIAT:u" s="T489">
                  <ts e="T490" id="Seg_1851" n="HIAT:w" s="T489">ČʼornajaRʼečʼkaɣɨn</ts>
                  <nts id="Seg_1852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1854" n="HIAT:w" s="T490">warkɨss</ts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1857" n="HIAT:w" s="T491">nätdanan</ts>
                  <nts id="Seg_1858" n="HIAT:ip">.</nts>
                  <nts id="Seg_1859" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T497" id="Seg_1861" n="HIAT:u" s="T492">
                  <ts e="T493" id="Seg_1863" n="HIAT:w" s="T492">A</ts>
                  <nts id="Seg_1864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1866" n="HIAT:w" s="T493">man</ts>
                  <nts id="Seg_1867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1869" n="HIAT:w" s="T494">qwannan</ts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1872" n="HIAT:w" s="T495">qwačilam</ts>
                  <nts id="Seg_1873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1875" n="HIAT:w" s="T496">mannɨpugu</ts>
                  <nts id="Seg_1876" n="HIAT:ip">.</nts>
                  <nts id="Seg_1877" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T508" id="Seg_1879" n="HIAT:u" s="T497">
                  <ts e="T498" id="Seg_1881" n="HIAT:w" s="T497">Marina</ts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1884" n="HIAT:w" s="T498">Pawlowna</ts>
                  <nts id="Seg_1885" n="HIAT:ip">,</nts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1888" n="HIAT:w" s="T499">qutdär</ts>
                  <nts id="Seg_1889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1891" n="HIAT:w" s="T500">man</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1894" n="HIAT:w" s="T501">nagɨrmoː</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1897" n="HIAT:w" s="T502">tütǯɨn</ts>
                  <nts id="Seg_1898" n="HIAT:ip">,</nts>
                  <nts id="Seg_1899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1901" n="HIAT:w" s="T503">i</ts>
                  <nts id="Seg_1902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1904" n="HIAT:w" s="T504">mekga</ts>
                  <nts id="Seg_1905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1907" n="HIAT:w" s="T505">nagɨrɨm</ts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1910" n="HIAT:w" s="T506">srazu</ts>
                  <nts id="Seg_1911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1913" n="HIAT:w" s="T507">nagɨrtə</ts>
                  <nts id="Seg_1914" n="HIAT:ip">.</nts>
                  <nts id="Seg_1915" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T510" id="Seg_1917" n="HIAT:u" s="T508">
                  <ts e="T509" id="Seg_1919" n="HIAT:w" s="T508">Man</ts>
                  <nts id="Seg_1920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1922" n="HIAT:w" s="T509">aːdelǯeǯan</ts>
                  <nts id="Seg_1923" n="HIAT:ip">.</nts>
                  <nts id="Seg_1924" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T516" id="Seg_1926" n="HIAT:u" s="T510">
                  <ts e="T511" id="Seg_1928" n="HIAT:w" s="T510">Wes</ts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1931" n="HIAT:w" s="T511">nagɨrtə</ts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1934" n="HIAT:w" s="T512">qutdär</ts>
                  <nts id="Seg_1935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1937" n="HIAT:w" s="T513">man</ts>
                  <nts id="Seg_1938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1940" n="HIAT:w" s="T514">qulau</ts>
                  <nts id="Seg_1941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1943" n="HIAT:w" s="T515">warkattə</ts>
                  <nts id="Seg_1944" n="HIAT:ip">.</nts>
                  <nts id="Seg_1945" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T522" id="Seg_1947" n="HIAT:u" s="T516">
                  <ts e="T517" id="Seg_1949" n="HIAT:w" s="T516">Kolʼän</ts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1952" n="HIAT:w" s="T517">tʼät</ts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1955" n="HIAT:w" s="T518">qajjamɨm</ts>
                  <nts id="Seg_1956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1958" n="HIAT:w" s="T519">üːtdətʼükal</ts>
                  <nts id="Seg_1959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1961" n="HIAT:w" s="T520">alʼ</ts>
                  <nts id="Seg_1962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1964" n="HIAT:w" s="T521">ass</ts>
                  <nts id="Seg_1965" n="HIAT:ip">?</nts>
                  <nts id="Seg_1966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T525" id="Seg_1968" n="HIAT:u" s="T522">
                  <ts e="T523" id="Seg_1970" n="HIAT:w" s="T522">Wes</ts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1973" n="HIAT:w" s="T523">mekga</ts>
                  <nts id="Seg_1974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1976" n="HIAT:w" s="T524">nagɨrɨt</ts>
                  <nts id="Seg_1977" n="HIAT:ip">.</nts>
                  <nts id="Seg_1978" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T528" id="Seg_1980" n="HIAT:u" s="T525">
                  <ts e="T526" id="Seg_1982" n="HIAT:w" s="T525">Man</ts>
                  <nts id="Seg_1983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1985" n="HIAT:w" s="T526">putʼöm</ts>
                  <nts id="Seg_1986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1988" n="HIAT:w" s="T527">aːdɨlǯəǯan</ts>
                  <nts id="Seg_1989" n="HIAT:ip">.</nts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T534" id="Seg_1992" n="HIAT:u" s="T528">
                  <ts e="T529" id="Seg_1994" n="HIAT:w" s="T528">Täɣɨtdɨltə</ts>
                  <nts id="Seg_1995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1997" n="HIAT:w" s="T529">nʼäjzʼe</ts>
                  <nts id="Seg_1998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2000" n="HIAT:w" s="T530">meqwattə</ts>
                  <nts id="Seg_2001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_2003" n="HIAT:w" s="T531">alʼi</ts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_2006" n="HIAT:w" s="T532">mogazʼe</ts>
                  <nts id="Seg_2007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_2009" n="HIAT:w" s="T533">meqwattə</ts>
                  <nts id="Seg_2010" n="HIAT:ip">?</nts>
                  <nts id="Seg_2011" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T540" id="Seg_2013" n="HIAT:u" s="T534">
                  <ts e="T535" id="Seg_2015" n="HIAT:w" s="T534">A</ts>
                  <nts id="Seg_2016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2018" n="HIAT:w" s="T535">magazʼinɣɨn</ts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_2021" n="HIAT:w" s="T536">qajla</ts>
                  <nts id="Seg_2022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2024" n="HIAT:w" s="T537">jetattə</ts>
                  <nts id="Seg_2025" n="HIAT:ip">,</nts>
                  <nts id="Seg_2026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2028" n="HIAT:w" s="T538">wes</ts>
                  <nts id="Seg_2029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2031" n="HIAT:w" s="T539">nagɨrtə</ts>
                  <nts id="Seg_2032" n="HIAT:ip">.</nts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T545" id="Seg_2035" n="HIAT:u" s="T540">
                  <ts e="T541" id="Seg_2037" n="HIAT:w" s="T540">Ämnäl</ts>
                  <nts id="Seg_2038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2040" n="HIAT:w" s="T541">qaj</ts>
                  <nts id="Seg_2041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2043" n="HIAT:w" s="T542">iːlɨn</ts>
                  <nts id="Seg_2044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2046" n="HIAT:w" s="T543">alʼ</ts>
                  <nts id="Seg_2047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2049" n="HIAT:w" s="T544">ass</ts>
                  <nts id="Seg_2050" n="HIAT:ip">?</nts>
                  <nts id="Seg_2051" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T548" id="Seg_2053" n="HIAT:u" s="T545">
                  <ts e="T546" id="Seg_2055" n="HIAT:w" s="T545">Tʼärzattə</ts>
                  <nts id="Seg_2056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2058" n="HIAT:w" s="T546">to</ts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2061" n="HIAT:w" s="T547">qwanpa</ts>
                  <nts id="Seg_2062" n="HIAT:ip">.</nts>
                  <nts id="Seg_2063" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T554" id="Seg_2065" n="HIAT:u" s="T548">
                  <ts e="T549" id="Seg_2067" n="HIAT:w" s="T548">Sinkan</ts>
                  <nts id="Seg_2068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2070" n="HIAT:w" s="T549">dʼät</ts>
                  <nts id="Seg_2071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2073" n="HIAT:w" s="T550">nagɨrət</ts>
                  <nts id="Seg_2074" n="HIAT:ip">,</nts>
                  <nts id="Seg_2075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2077" n="HIAT:w" s="T551">qutdär</ts>
                  <nts id="Seg_2078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2080" n="HIAT:w" s="T552">ilɨkqun</ts>
                  <nts id="Seg_2081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2082" n="HIAT:ip">(</nts>
                  <ts e="T554" id="Seg_2084" n="HIAT:w" s="T553">warkɨqun</ts>
                  <nts id="Seg_2085" n="HIAT:ip">)</nts>
                  <nts id="Seg_2086" n="HIAT:ip">.</nts>
                  <nts id="Seg_2087" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T558" id="Seg_2089" n="HIAT:u" s="T554">
                  <ts e="T555" id="Seg_2091" n="HIAT:w" s="T554">Man</ts>
                  <nts id="Seg_2092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2094" n="HIAT:w" s="T555">täbnä</ts>
                  <nts id="Seg_2095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2097" n="HIAT:w" s="T556">nagɨrɨm</ts>
                  <nts id="Seg_2098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2100" n="HIAT:w" s="T557">nagurzan</ts>
                  <nts id="Seg_2101" n="HIAT:ip">.</nts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T562" id="Seg_2104" n="HIAT:u" s="T558">
                  <ts e="T559" id="Seg_2106" n="HIAT:w" s="T558">Täp</ts>
                  <nts id="Seg_2107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2109" n="HIAT:w" s="T559">mekga</ts>
                  <nts id="Seg_2110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2112" n="HIAT:w" s="T560">jass</ts>
                  <nts id="Seg_2113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2115" n="HIAT:w" s="T561">nagurgut</ts>
                  <nts id="Seg_2116" n="HIAT:ip">.</nts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T567" id="Seg_2119" n="HIAT:u" s="T562">
                  <ts e="T563" id="Seg_2121" n="HIAT:w" s="T562">Tetʼa</ts>
                  <nts id="Seg_2122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2124" n="HIAT:w" s="T563">Marina</ts>
                  <nts id="Seg_2125" n="HIAT:ip">,</nts>
                  <nts id="Seg_2126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2128" n="HIAT:w" s="T564">perwɨj</ts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2131" n="HIAT:w" s="T565">majzʼe</ts>
                  <nts id="Seg_2132" n="HIAT:ip">,</nts>
                  <nts id="Seg_2133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2135" n="HIAT:w" s="T566">praznikze</ts>
                  <nts id="Seg_2136" n="HIAT:ip">!</nts>
                  <nts id="Seg_2137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T567" id="Seg_2138" n="sc" s="T1">
               <ts e="T2" id="Seg_2140" n="e" s="T1">Me </ts>
               <ts e="T3" id="Seg_2142" n="e" s="T2">tamdʼel </ts>
               <ts e="T4" id="Seg_2144" n="e" s="T3">amdaj </ts>
               <ts e="T5" id="Seg_2146" n="e" s="T4">Angelina </ts>
               <ts e="T6" id="Seg_2148" n="e" s="T5">Iwanownaze, </ts>
               <ts e="T7" id="Seg_2150" n="e" s="T6">qulupbaj, </ts>
               <ts e="T8" id="Seg_2152" n="e" s="T7">tan </ts>
               <ts e="T9" id="Seg_2154" n="e" s="T8">dʼat </ts>
               <ts e="T10" id="Seg_2156" n="e" s="T9">qulupbaj, </ts>
               <ts e="T11" id="Seg_2158" n="e" s="T10">qutdär </ts>
               <ts e="T12" id="Seg_2160" n="e" s="T11">warkatdə, </ts>
               <ts e="T13" id="Seg_2162" n="e" s="T12">qajla </ts>
               <ts e="T14" id="Seg_2164" n="e" s="T13">meːkutdal. </ts>
               <ts e="T15" id="Seg_2166" n="e" s="T14">Man </ts>
               <ts e="T16" id="Seg_2168" n="e" s="T15">tebnä </ts>
               <ts e="T17" id="Seg_2170" n="e" s="T16">kelʼlʼe </ts>
               <ts e="T18" id="Seg_2172" n="e" s="T17">taːdərau </ts>
               <ts e="T19" id="Seg_2174" n="e" s="T18">tan </ts>
               <ts e="T20" id="Seg_2176" n="e" s="T19">dʼat. </ts>
               <ts e="T21" id="Seg_2178" n="e" s="T20">Man </ts>
               <ts e="T22" id="Seg_2180" n="e" s="T21">tʼäran: </ts>
               <ts e="T23" id="Seg_2182" n="e" s="T22">“Täp </ts>
               <ts e="T24" id="Seg_2184" n="e" s="T23">pajäpba.” </ts>
               <ts e="T25" id="Seg_2186" n="e" s="T24">Man </ts>
               <ts e="T26" id="Seg_2188" n="e" s="T25">täbnä </ts>
               <ts e="T27" id="Seg_2190" n="e" s="T26">kessau </ts>
               <ts e="T28" id="Seg_2192" n="e" s="T27">tan </ts>
               <ts e="T29" id="Seg_2194" n="e" s="T28">mezuwi </ts>
               <ts e="T30" id="Seg_2196" n="e" s="T29">Мarʼijaze </ts>
               <ts e="T31" id="Seg_2198" n="e" s="T30">apstɨzat, </ts>
               <ts e="T32" id="Seg_2200" n="e" s="T31">me </ts>
               <ts e="T33" id="Seg_2202" n="e" s="T32">tezʼe </ts>
               <ts e="T34" id="Seg_2204" n="e" s="T33">qutdär </ts>
               <ts e="T35" id="Seg_2206" n="e" s="T34">ärzawtə. </ts>
               <ts e="T36" id="Seg_2208" n="e" s="T35">Man </ts>
               <ts e="T37" id="Seg_2210" n="e" s="T36">tebnä </ts>
               <ts e="T38" id="Seg_2212" n="e" s="T37">wes </ts>
               <ts e="T39" id="Seg_2214" n="e" s="T38">kessau. </ts>
               <ts e="T40" id="Seg_2216" n="e" s="T39">Man </ts>
               <ts e="T41" id="Seg_2218" n="e" s="T40">qulɨpbɨzan: </ts>
               <ts e="T42" id="Seg_2220" n="e" s="T41">“Tʼotka </ts>
               <ts e="T43" id="Seg_2222" n="e" s="T42">Marʼina </ts>
               <ts e="T44" id="Seg_2224" n="e" s="T43">nagɨr </ts>
               <ts e="T45" id="Seg_2226" n="e" s="T44">nagɨrčeǯan.” </ts>
               <ts e="T46" id="Seg_2228" n="e" s="T45">Puskaj </ts>
               <ts e="T47" id="Seg_2230" n="e" s="T46">togulǯɨmdə. </ts>
               <ts e="T48" id="Seg_2232" n="e" s="T47">Tʼolom, </ts>
               <ts e="T49" id="Seg_2234" n="e" s="T48">Marʼina </ts>
               <ts e="T50" id="Seg_2236" n="e" s="T49">Pawlowna! </ts>
               <ts e="T51" id="Seg_2238" n="e" s="T50">Man </ts>
               <ts e="T52" id="Seg_2240" n="e" s="T51">tekga </ts>
               <ts e="T53" id="Seg_2242" n="e" s="T52">süsögə </ts>
               <ts e="T54" id="Seg_2244" n="e" s="T53">nagɨr </ts>
               <ts e="T55" id="Seg_2246" n="e" s="T54">nagɨrčeǯan. </ts>
               <ts e="T56" id="Seg_2248" n="e" s="T55">Kazaxsen </ts>
               <ts e="T57" id="Seg_2250" n="e" s="T56">man </ts>
               <ts e="T58" id="Seg_2252" n="e" s="T57">ass </ts>
               <ts e="T59" id="Seg_2254" n="e" s="T58">tunan. </ts>
               <ts e="T60" id="Seg_2256" n="e" s="T59">Man </ts>
               <ts e="T61" id="Seg_2258" n="e" s="T60">ilan </ts>
               <ts e="T62" id="Seg_2260" n="e" s="T61">täpär </ts>
               <ts e="T63" id="Seg_2262" n="e" s="T62">qwačoɣɨn, </ts>
               <ts e="T64" id="Seg_2264" n="e" s="T63">Nowosibirskaɣɨn. </ts>
               <ts e="T65" id="Seg_2266" n="e" s="T64">Qwačə </ts>
               <ts e="T66" id="Seg_2268" n="e" s="T65">iːbɨɣaj, </ts>
               <ts e="T67" id="Seg_2270" n="e" s="T66">soːdigaj. </ts>
               <ts e="T68" id="Seg_2272" n="e" s="T67">Qwačoɣɨndə </ts>
               <ts e="T69" id="Seg_2274" n="e" s="T68">Nowosibirskat </ts>
               <ts e="T70" id="Seg_2276" n="e" s="T69">poisse </ts>
               <ts e="T71" id="Seg_2278" n="e" s="T70">čaːʒɨzan. </ts>
               <ts e="T72" id="Seg_2280" n="e" s="T71">Poiskan </ts>
               <ts e="T73" id="Seg_2282" n="e" s="T72">čaʒəle </ts>
               <ts e="T74" id="Seg_2284" n="e" s="T73">qozɨrčezawtə. </ts>
               <ts e="T75" id="Seg_2286" n="e" s="T74">Tajoʒnɨj </ts>
               <ts e="T76" id="Seg_2288" n="e" s="T75">stancɨɣɨn </ts>
               <ts e="T77" id="Seg_2290" n="e" s="T76">pois </ts>
               <ts e="T78" id="Seg_2292" n="e" s="T77">kuːnnɨn </ts>
               <ts e="T79" id="Seg_2294" n="e" s="T78">nɨkgɨs. </ts>
               <ts e="T80" id="Seg_2296" n="e" s="T79">Man </ts>
               <ts e="T81" id="Seg_2298" n="e" s="T80">mannɨpɨzan </ts>
               <ts e="T82" id="Seg_2300" n="e" s="T81">i </ts>
               <ts e="T83" id="Seg_2302" n="e" s="T82">Nikinɨm </ts>
               <ts e="T84" id="Seg_2304" n="e" s="T83">qajɣɨn </ts>
               <ts e="T85" id="Seg_2306" n="e" s="T84">äss </ts>
               <ts e="T86" id="Seg_2308" n="e" s="T85">qoǯirsau. </ts>
               <ts e="T87" id="Seg_2310" n="e" s="T86">Patom </ts>
               <ts e="T88" id="Seg_2312" n="e" s="T87">pois </ts>
               <ts e="T89" id="Seg_2314" n="e" s="T88">üːban. </ts>
               <ts e="T90" id="Seg_2316" n="e" s="T89">Man </ts>
               <ts e="T91" id="Seg_2318" n="e" s="T90">qɨdan </ts>
               <ts e="T92" id="Seg_2320" n="e" s="T91">akoškaɣɨn </ts>
               <ts e="T93" id="Seg_2322" n="e" s="T92">mannɨpɨzan. </ts>
               <ts e="T94" id="Seg_2324" n="e" s="T93">Jedla </ts>
               <ts e="T95" id="Seg_2326" n="e" s="T94">koːcʼin </ts>
               <ts e="T96" id="Seg_2328" n="e" s="T95">kalʼelʼlʼe </ts>
               <ts e="T97" id="Seg_2330" n="e" s="T96">čaːʒɨzattə. </ts>
               <ts e="T98" id="Seg_2332" n="e" s="T97">Qwačɨn </ts>
               <ts e="T99" id="Seg_2334" n="e" s="T98">krajɣɨn </ts>
               <ts e="T100" id="Seg_2336" n="e" s="T99">pola, </ts>
               <ts e="T101" id="Seg_2338" n="e" s="T100">madʼila. </ts>
               <ts e="T102" id="Seg_2340" n="e" s="T101">Krugom </ts>
               <ts e="T103" id="Seg_2342" n="e" s="T102">sər </ts>
               <ts e="T104" id="Seg_2344" n="e" s="T103">ippɨtda. </ts>
               <ts e="T105" id="Seg_2346" n="e" s="T104">Man </ts>
               <ts e="T106" id="Seg_2348" n="e" s="T105">poisqɨn </ts>
               <ts e="T107" id="Seg_2350" n="e" s="T106">ass </ts>
               <ts e="T108" id="Seg_2352" n="e" s="T107">warɨpɨzan. </ts>
               <ts e="T109" id="Seg_2354" n="e" s="T108">Qɨːban </ts>
               <ts e="T110" id="Seg_2356" n="e" s="T109">kogɨrɨt. </ts>
               <ts e="T111" id="Seg_2358" n="e" s="T110">Ugon </ts>
               <ts e="T112" id="Seg_2360" n="e" s="T111">man </ts>
               <ts e="T113" id="Seg_2362" n="e" s="T112">ass </ts>
               <ts e="T114" id="Seg_2364" n="e" s="T113">paldükuzan </ts>
               <ts e="T115" id="Seg_2366" n="e" s="T114">poezlaze. </ts>
               <ts e="T116" id="Seg_2368" n="e" s="T115">Man </ts>
               <ts e="T117" id="Seg_2370" n="e" s="T116">elan </ts>
               <ts e="T118" id="Seg_2372" n="e" s="T117">näjɣunnan, </ts>
               <ts e="T119" id="Seg_2374" n="e" s="T118">mäɣuntə </ts>
               <ts e="T120" id="Seg_2376" n="e" s="T119">na </ts>
               <ts e="T121" id="Seg_2378" n="e" s="T120">tükkutda </ts>
               <ts e="T122" id="Seg_2380" n="e" s="T121">süsöga </ts>
               <ts e="T123" id="Seg_2382" n="e" s="T122">selam </ts>
               <ts e="T124" id="Seg_2384" n="e" s="T123">nagɨrlʼe </ts>
               <ts e="T125" id="Seg_2386" n="e" s="T124">taːdərɨst. </ts>
               <ts e="T126" id="Seg_2388" n="e" s="T125">Qwatčɨze </ts>
               <ts e="T127" id="Seg_2390" n="e" s="T126">näjɣum, </ts>
               <ts e="T128" id="Seg_2392" n="e" s="T127">na </ts>
               <ts e="T129" id="Seg_2394" n="e" s="T128">tükkɨt </ts>
               <ts e="T130" id="Seg_2396" n="e" s="T129">da </ts>
               <ts e="T131" id="Seg_2398" n="e" s="T130">Satdijedotdə </ts>
               <ts e="T132" id="Seg_2400" n="e" s="T131">süsöɣə </ts>
               <ts e="T133" id="Seg_2402" n="e" s="T132">äʒlam </ts>
               <ts e="T134" id="Seg_2404" n="e" s="T133">nagɨrle </ts>
               <ts e="T135" id="Seg_2406" n="e" s="T134">taːdɨrɨstə, </ts>
               <ts e="T136" id="Seg_2408" n="e" s="T135">tekga </ts>
               <ts e="T137" id="Seg_2410" n="e" s="T136">balʼdʼukus, </ts>
               <ts e="T138" id="Seg_2412" n="e" s="T137">tazʼe </ts>
               <ts e="T139" id="Seg_2414" n="e" s="T138">qɨːdan </ts>
               <ts e="T140" id="Seg_2416" n="e" s="T139">nagɨrčukus. </ts>
               <ts e="T141" id="Seg_2418" n="e" s="T140">Man </ts>
               <ts e="T142" id="Seg_2420" n="e" s="T141">na </ts>
               <ts e="T143" id="Seg_2422" n="e" s="T142">näjɣunnan </ts>
               <ts e="T144" id="Seg_2424" n="e" s="T143">warkan, </ts>
               <ts e="T145" id="Seg_2426" n="e" s="T144">Nowosibʼirskan. </ts>
               <ts e="T146" id="Seg_2428" n="e" s="T145">Täbɨn </ts>
               <ts e="T147" id="Seg_2430" n="e" s="T146">nimdə </ts>
               <ts e="T148" id="Seg_2432" n="e" s="T147">Anglʼina </ts>
               <ts e="T149" id="Seg_2434" n="e" s="T148">Iwanɨwna. </ts>
               <ts e="T150" id="Seg_2436" n="e" s="T149">Man </ts>
               <ts e="T151" id="Seg_2438" n="e" s="T150">tebnan </ts>
               <ts e="T152" id="Seg_2440" n="e" s="T151">warkan. </ts>
               <ts e="T153" id="Seg_2442" n="e" s="T152">Mazɨm </ts>
               <ts e="T154" id="Seg_2444" n="e" s="T153">warɨn </ts>
               <ts e="T155" id="Seg_2446" n="e" s="T154">soːn, </ts>
               <ts e="T156" id="Seg_2448" n="e" s="T155">jas </ts>
               <ts e="T157" id="Seg_2450" n="e" s="T156">qwɛdɨpɨqun. </ts>
               <ts e="T158" id="Seg_2452" n="e" s="T157">Man </ts>
               <ts e="T159" id="Seg_2454" n="e" s="T158">oːnän </ts>
               <ts e="T160" id="Seg_2456" n="e" s="T159">melʼe </ts>
               <ts e="T161" id="Seg_2458" n="e" s="T160">taːdərəkou </ts>
               <ts e="T162" id="Seg_2460" n="e" s="T161">qaim </ts>
               <ts e="T163" id="Seg_2462" n="e" s="T162">kɨgan. </ts>
               <ts e="T164" id="Seg_2464" n="e" s="T163">Tebnan </ts>
               <ts e="T165" id="Seg_2466" n="e" s="T164">nagur </ts>
               <ts e="T166" id="Seg_2468" n="e" s="T165">sütʼdʼe </ts>
               <ts e="T167" id="Seg_2470" n="e" s="T166">maːttə. </ts>
               <ts e="T168" id="Seg_2472" n="e" s="T167">Maːdɨn </ts>
               <ts e="T169" id="Seg_2474" n="e" s="T168">sütʼdʼila </ts>
               <ts e="T170" id="Seg_2476" n="e" s="T169">warɣɨ. </ts>
               <ts e="T171" id="Seg_2478" n="e" s="T170">Maːt </ts>
               <ts e="T172" id="Seg_2480" n="e" s="T171">pötpa, </ts>
               <ts e="T173" id="Seg_2482" n="e" s="T172">parawoje </ts>
               <ts e="T174" id="Seg_2484" n="e" s="T173">ataplʼenʼje. </ts>
               <ts e="T175" id="Seg_2486" n="e" s="T174">Üt </ts>
               <ts e="T176" id="Seg_2488" n="e" s="T175">maːtqɨn </ts>
               <ts e="T177" id="Seg_2490" n="e" s="T176">jen: </ts>
               <ts e="T178" id="Seg_2492" n="e" s="T177">pärčɨtdə </ts>
               <ts e="T179" id="Seg_2494" n="e" s="T178">üt </ts>
               <ts e="T180" id="Seg_2496" n="e" s="T179">i </ts>
               <ts e="T181" id="Seg_2498" n="e" s="T180">qannəbädi </ts>
               <ts e="T182" id="Seg_2500" n="e" s="T181">üt. </ts>
               <ts e="T183" id="Seg_2502" n="e" s="T182">Üt </ts>
               <ts e="T184" id="Seg_2504" n="e" s="T183">wes </ts>
               <ts e="T185" id="Seg_2506" n="e" s="T184">maːtqɨn </ts>
               <ts e="T186" id="Seg_2508" n="e" s="T185">jewat. </ts>
               <ts e="T187" id="Seg_2510" n="e" s="T186">Okkɨr </ts>
               <ts e="T188" id="Seg_2512" n="e" s="T187">sütʼdʼäɣɨn </ts>
               <ts e="T189" id="Seg_2514" n="e" s="T188">uːrgoftə. </ts>
               <ts e="T190" id="Seg_2516" n="e" s="T189">Natʼen </ts>
               <ts e="T191" id="Seg_2518" n="e" s="T190">pärčɨd </ts>
               <ts e="T192" id="Seg_2520" n="e" s="T191">üt </ts>
               <ts e="T193" id="Seg_2522" n="e" s="T192">i </ts>
               <ts e="T194" id="Seg_2524" n="e" s="T193">qannäbädi </ts>
               <ts e="T195" id="Seg_2526" n="e" s="T194">üt. </ts>
               <ts e="T196" id="Seg_2528" n="e" s="T195">Matqɨn </ts>
               <ts e="T197" id="Seg_2530" n="e" s="T196">tütpukowtə </ts>
               <ts e="T198" id="Seg_2532" n="e" s="T197">i </ts>
               <ts e="T199" id="Seg_2534" n="e" s="T198">küzepukowtə. </ts>
               <ts e="T200" id="Seg_2536" n="e" s="T199">Wes </ts>
               <ts e="T201" id="Seg_2538" n="e" s="T200">müzulʼǯɨlʼe </ts>
               <ts e="T202" id="Seg_2540" n="e" s="T201">qwatdəqut </ts>
               <ts e="T203" id="Seg_2542" n="e" s="T202">i </ts>
               <ts e="T204" id="Seg_2544" n="e" s="T203">qaj </ts>
               <ts e="T205" id="Seg_2546" n="e" s="T204">aptänäj </ts>
               <ts e="T206" id="Seg_2548" n="e" s="T205">tʼäkku. </ts>
               <ts e="T207" id="Seg_2550" n="e" s="T206">Cɨpočkam </ts>
               <ts e="T208" id="Seg_2552" n="e" s="T207">nameʒalǯal, </ts>
               <ts e="T209" id="Seg_2554" n="e" s="T208">üt </ts>
               <ts e="T210" id="Seg_2556" n="e" s="T209">šoɣərlʼe </ts>
               <ts e="T211" id="Seg_2558" n="e" s="T210">qalukun. </ts>
               <ts e="T212" id="Seg_2560" n="e" s="T211">Wes </ts>
               <ts e="T213" id="Seg_2562" n="e" s="T212">mezulʼǯulʼe </ts>
               <ts e="T214" id="Seg_2564" n="e" s="T213">meʒalgut. </ts>
               <ts e="T215" id="Seg_2566" n="e" s="T214">Maːdla </ts>
               <ts e="T216" id="Seg_2568" n="e" s="T215">tɨtdɨn </ts>
               <ts e="T217" id="Seg_2570" n="e" s="T216">iːbɨgaj, </ts>
               <ts e="T218" id="Seg_2572" n="e" s="T217">teːttə </ts>
               <ts e="T219" id="Seg_2574" n="e" s="T218">ätaš </ts>
               <ts e="T220" id="Seg_2576" n="e" s="T219">maːdla, </ts>
               <ts e="T221" id="Seg_2578" n="e" s="T220">wes </ts>
               <ts e="T222" id="Seg_2580" n="e" s="T221">okkɨr </ts>
               <ts e="T223" id="Seg_2582" n="e" s="T222">saj </ts>
               <ts e="T224" id="Seg_2584" n="e" s="T223">maːdla. </ts>
               <ts e="T225" id="Seg_2586" n="e" s="T224">Maːdla </ts>
               <ts e="T226" id="Seg_2588" n="e" s="T225">tüpba. </ts>
               <ts e="T227" id="Seg_2590" n="e" s="T226">Ass </ts>
               <ts e="T228" id="Seg_2592" n="e" s="T227">qutdoqɨn </ts>
               <ts e="T229" id="Seg_2594" n="e" s="T228">pola: </ts>
               <ts e="T230" id="Seg_2596" n="e" s="T229">kəla, </ts>
               <ts e="T231" id="Seg_2598" n="e" s="T230">üdəpiːla, </ts>
               <ts e="T232" id="Seg_2600" n="e" s="T231">jolkala, </ts>
               <ts e="T233" id="Seg_2602" n="e" s="T232">sosnala, </ts>
               <ts e="T234" id="Seg_2604" n="e" s="T233">tʼeunpola, </ts>
               <ts e="T235" id="Seg_2606" n="e" s="T234">pila. </ts>
               <ts e="T236" id="Seg_2608" n="e" s="T235">Tautʼen </ts>
               <ts e="T237" id="Seg_2610" n="e" s="T236">taɣɨn </ts>
               <ts e="T238" id="Seg_2612" n="e" s="T237">koːcʼin </ts>
               <ts e="T239" id="Seg_2614" n="e" s="T238">krɨbɨla </ts>
               <ts e="T240" id="Seg_2616" n="e" s="T239">tʼelɨmkun. </ts>
               <ts e="T241" id="Seg_2618" n="e" s="T240">Me </ts>
               <ts e="T242" id="Seg_2620" n="e" s="T241">taɣɨn </ts>
               <ts e="T243" id="Seg_2622" n="e" s="T242">krɨbɨ </ts>
               <ts e="T244" id="Seg_2624" n="e" s="T243">taktätǯutu. </ts>
               <ts e="T245" id="Seg_2626" n="e" s="T244">Üdɨn </ts>
               <ts e="T246" id="Seg_2628" n="e" s="T245">kocʼin </ts>
               <ts e="T247" id="Seg_2630" n="e" s="T246">jeqwattə </ts>
               <ts e="T248" id="Seg_2632" n="e" s="T247">swetoɣɨla. </ts>
               <ts e="T249" id="Seg_2634" n="e" s="T248">Qaːn </ts>
               <ts e="T250" id="Seg_2636" n="e" s="T249">tolʼdʼizʼe </ts>
               <ts e="T251" id="Seg_2638" n="e" s="T250">paldʼättə. </ts>
               <ts e="T252" id="Seg_2640" n="e" s="T251">Qɨbanʼäʒala </ts>
               <ts e="T253" id="Seg_2642" n="e" s="T252">salaskaze </ts>
               <ts e="T254" id="Seg_2644" n="e" s="T253">nezarnattə. </ts>
               <ts e="T255" id="Seg_2646" n="e" s="T254">Aran </ts>
               <ts e="T256" id="Seg_2648" n="e" s="T255">pon </ts>
               <ts e="T257" id="Seg_2650" n="e" s="T256">tʼäbla </ts>
               <ts e="T258" id="Seg_2652" n="e" s="T257">wes </ts>
               <ts e="T259" id="Seg_2654" n="e" s="T258">šoltana </ts>
               <ts e="T260" id="Seg_2656" n="e" s="T259">azölʼeǯattə. </ts>
               <ts e="T261" id="Seg_2658" n="e" s="T260">Qusaqɨn </ts>
               <ts e="T262" id="Seg_2660" n="e" s="T261">me </ts>
               <ts e="T263" id="Seg_2662" n="e" s="T262">tüzawtə </ts>
               <ts e="T264" id="Seg_2664" n="e" s="T263">Nowosibirskat, </ts>
               <ts e="T265" id="Seg_2666" n="e" s="T264">seqɨzawtə </ts>
               <ts e="T266" id="Seg_2668" n="e" s="T265">Nowosibirskaɣɨn </ts>
               <ts e="T267" id="Seg_2670" n="e" s="T266">wakzalɣɨn. </ts>
               <ts e="T268" id="Seg_2672" n="e" s="T267">Innäj </ts>
               <ts e="T269" id="Seg_2674" n="e" s="T268">ətašqɨn </ts>
               <ts e="T270" id="Seg_2676" n="e" s="T269">seqɨzawtə. </ts>
               <ts e="T271" id="Seg_2678" n="e" s="T270">Soblamtätte </ts>
               <ts e="T272" id="Seg_2680" n="e" s="T271">ätašxɨn </ts>
               <ts e="T273" id="Seg_2682" n="e" s="T272">qotdɨzawtə. </ts>
               <ts e="T274" id="Seg_2684" n="e" s="T273">Mʼäɣuntu </ts>
               <ts e="T275" id="Seg_2686" n="e" s="T274">wes </ts>
               <ts e="T276" id="Seg_2688" n="e" s="T275">mewattə: </ts>
               <ts e="T277" id="Seg_2690" n="e" s="T276">matrazlam, </ts>
               <ts e="T278" id="Seg_2692" n="e" s="T277">adʼejallam, </ts>
               <ts e="T279" id="Seg_2694" n="e" s="T278">paduškalam. </ts>
               <ts e="T280" id="Seg_2696" n="e" s="T279">Qotdəzawtə </ts>
               <ts e="T281" id="Seg_2698" n="e" s="T280">soːdʼigan. </ts>
               <ts e="T282" id="Seg_2700" n="e" s="T281">Qula </ts>
               <ts e="T283" id="Seg_2702" n="e" s="T282">koːcʼin </ts>
               <ts e="T284" id="Seg_2704" n="e" s="T283">jezattə. </ts>
               <ts e="T285" id="Seg_2706" n="e" s="T284">Iːbɨɣaj </ts>
               <ts e="T286" id="Seg_2708" n="e" s="T285">wakzal. </ts>
               <ts e="T287" id="Seg_2710" n="e" s="T286">Natʼen </ts>
               <ts e="T288" id="Seg_2712" n="e" s="T287">maːɣɨlǯənnaš, </ts>
               <ts e="T289" id="Seg_2714" n="e" s="T288">jaš </ts>
               <ts e="T290" id="Seg_2716" n="e" s="T289">čannännaš. </ts>
               <ts e="T291" id="Seg_2718" n="e" s="T290">Qarʼemɨɣɨn </ts>
               <ts e="T292" id="Seg_2720" n="e" s="T291">qwannawtə </ts>
               <ts e="T293" id="Seg_2722" n="e" s="T292">stalowajtə. </ts>
               <ts e="T294" id="Seg_2724" n="e" s="T293">Tebla </ts>
               <ts e="T295" id="Seg_2726" n="e" s="T294">ketqwattə </ts>
               <ts e="T296" id="Seg_2728" n="e" s="T295">restoran. </ts>
               <ts e="T297" id="Seg_2730" n="e" s="T296">Natʼen </ts>
               <ts e="T298" id="Seg_2732" n="e" s="T297">soːdigan </ts>
               <ts e="T299" id="Seg_2734" n="e" s="T298">jen, </ts>
               <ts e="T300" id="Seg_2736" n="e" s="T299">rajin </ts>
               <ts e="T301" id="Seg_2738" n="e" s="T300">sütdə. </ts>
               <ts e="T302" id="Seg_2740" n="e" s="T301">Natʼen </ts>
               <ts e="T303" id="Seg_2742" n="e" s="T302">i </ts>
               <ts e="T304" id="Seg_2744" n="e" s="T303">aursawtə. </ts>
               <ts e="T305" id="Seg_2746" n="e" s="T304">Menan </ts>
               <ts e="T306" id="Seg_2748" n="e" s="T305">more </ts>
               <ts e="T307" id="Seg_2750" n="e" s="T306">čaʒekan </ts>
               <ts e="T308" id="Seg_2752" n="e" s="T307">jen. </ts>
               <ts e="T309" id="Seg_2754" n="e" s="T308">Natʼen </ts>
               <ts e="T310" id="Seg_2756" n="e" s="T309">qula </ts>
               <ts e="T311" id="Seg_2758" n="e" s="T310">qwɛlɨnʼättə </ts>
               <ts e="T312" id="Seg_2760" n="e" s="T311">sɨbɨqse. </ts>
               <ts e="T313" id="Seg_2762" n="e" s="T312">Nɨrsala </ts>
               <ts e="T314" id="Seg_2764" n="e" s="T313">qwatqudattə </ts>
               <ts e="T315" id="Seg_2766" n="e" s="T314">i </ts>
               <ts e="T316" id="Seg_2768" n="e" s="T315">pʼedʼela. </ts>
               <ts e="T317" id="Seg_2770" n="e" s="T316">Qɨbanädäkqa, </ts>
               <ts e="T318" id="Seg_2772" n="e" s="T317">xazʼäjkan </ts>
               <ts e="T319" id="Seg_2774" n="e" s="T318">nägat, </ts>
               <ts e="T320" id="Seg_2776" n="e" s="T319">tätkustə </ts>
               <ts e="T321" id="Seg_2778" n="e" s="T320">nɨrsan </ts>
               <ts e="T322" id="Seg_2780" n="e" s="T321">tassɨt. </ts>
               <ts e="T323" id="Seg_2782" n="e" s="T322">Nɨrsa </ts>
               <ts e="T324" id="Seg_2784" n="e" s="T323">qattädʼipba. </ts>
               <ts e="T325" id="Seg_2786" n="e" s="T324">Koška </ts>
               <ts e="T326" id="Seg_2788" n="e" s="T325">täbɨm </ts>
               <ts e="T327" id="Seg_2790" n="e" s="T326">apsɨt. </ts>
               <ts e="T328" id="Seg_2792" n="e" s="T327">Mannan </ts>
               <ts e="T329" id="Seg_2794" n="e" s="T328">sət </ts>
               <ts e="T330" id="Seg_2796" n="e" s="T329">padrugau </ts>
               <ts e="T331" id="Seg_2798" n="e" s="T330">jewa. </ts>
               <ts e="T332" id="Seg_2800" n="e" s="T331">Man </ts>
               <ts e="T333" id="Seg_2802" n="e" s="T332">teblanä </ts>
               <ts e="T334" id="Seg_2804" n="e" s="T333">palʼdʼüqwan. </ts>
               <ts e="T335" id="Seg_2806" n="e" s="T334">I </ts>
               <ts e="T336" id="Seg_2808" n="e" s="T335">tebla </ts>
               <ts e="T337" id="Seg_2810" n="e" s="T336">mekka </ts>
               <ts e="T338" id="Seg_2812" n="e" s="T337">palʼdʼüqwattə. </ts>
               <ts e="T339" id="Seg_2814" n="e" s="T338">Man </ts>
               <ts e="T340" id="Seg_2816" n="e" s="T339">sormeǯan </ts>
               <ts e="T341" id="Seg_2818" n="e" s="T340">poqqɨlaj. </ts>
               <ts e="T342" id="Seg_2820" n="e" s="T341">Ola </ts>
               <ts e="T343" id="Seg_2822" n="e" s="T342">amdɨgu </ts>
               <ts e="T344" id="Seg_2824" n="e" s="T343">ass </ts>
               <ts e="T345" id="Seg_2826" n="e" s="T344">kɨgan. </ts>
               <ts e="T346" id="Seg_2828" n="e" s="T345">A </ts>
               <ts e="T347" id="Seg_2830" n="e" s="T346">onän </ts>
               <ts e="T348" id="Seg_2832" n="e" s="T347">kɨdan </ts>
               <ts e="T349" id="Seg_2834" n="e" s="T348">qojmutčukan. </ts>
               <ts e="T350" id="Seg_2836" n="e" s="T349">Man </ts>
               <ts e="T351" id="Seg_2838" n="e" s="T350">üdɨn </ts>
               <ts e="T352" id="Seg_2840" n="e" s="T351">Satdijedot </ts>
               <ts e="T353" id="Seg_2842" n="e" s="T352">tütǯan </ts>
               <ts e="T354" id="Seg_2844" n="e" s="T353">qwälɨsku, </ts>
               <ts e="T355" id="Seg_2846" n="e" s="T354">čobərɨm </ts>
               <ts e="T356" id="Seg_2848" n="e" s="T355">qwadəgu. </ts>
               <ts e="T357" id="Seg_2850" n="e" s="T356">Qwače </ts>
               <ts e="T358" id="Seg_2852" n="e" s="T357">iːbɨgan </ts>
               <ts e="T359" id="Seg_2854" n="e" s="T358">jen. </ts>
               <ts e="T360" id="Seg_2856" n="e" s="T359">Man </ts>
               <ts e="T361" id="Seg_2858" n="e" s="T360">na </ts>
               <ts e="T362" id="Seg_2860" n="e" s="T361">tünɨǯan </ts>
               <ts e="T363" id="Seg_2862" n="e" s="T362">i </ts>
               <ts e="T364" id="Seg_2864" n="e" s="T363">tekga </ts>
               <ts e="T365" id="Seg_2866" n="e" s="T364">aːdulǯeǯau </ts>
               <ts e="T366" id="Seg_2868" n="e" s="T365">qwačɨn. </ts>
               <ts e="T367" id="Seg_2870" n="e" s="T366">Tekga </ts>
               <ts e="T368" id="Seg_2872" n="e" s="T367">na </ts>
               <ts e="T369" id="Seg_2874" n="e" s="T368">qwatčɨn </ts>
               <ts e="T370" id="Seg_2876" n="e" s="T369">tatčau. </ts>
               <ts e="T371" id="Seg_2878" n="e" s="T370">Tan </ts>
               <ts e="T372" id="Seg_2880" n="e" s="T371">mannämeːǯal. </ts>
               <ts e="T373" id="Seg_2882" n="e" s="T372">Tawtʼen </ts>
               <ts e="T374" id="Seg_2884" n="e" s="T373">wsʼäkij </ts>
               <ts e="T375" id="Seg_2886" n="e" s="T374">mašinalazʼe </ts>
               <ts e="T376" id="Seg_2888" n="e" s="T375">palʼdʼizan. </ts>
               <ts e="T377" id="Seg_2890" n="e" s="T376">Perwɨj </ts>
               <ts e="T378" id="Seg_2892" n="e" s="T377">ras </ts>
               <ts e="T379" id="Seg_2894" n="e" s="T378">larɨpɨzan. </ts>
               <ts e="T380" id="Seg_2896" n="e" s="T379">Mazɨm </ts>
               <ts e="T381" id="Seg_2898" n="e" s="T380">Valodʼä </ts>
               <ts e="T382" id="Seg_2900" n="e" s="T381">qwačoɣɨn </ts>
               <ts e="T383" id="Seg_2902" n="e" s="T382">warɨs. </ts>
               <ts e="T384" id="Seg_2904" n="e" s="T383">Wes </ts>
               <ts e="T385" id="Seg_2906" n="e" s="T384">aːdulǯɨɣɨsɨt. </ts>
               <ts e="T386" id="Seg_2908" n="e" s="T385">Onät </ts>
               <ts e="T387" id="Seg_2910" n="e" s="T386">də </ts>
               <ts e="T388" id="Seg_2912" n="e" s="T387">qu </ts>
               <ts e="T389" id="Seg_2914" n="e" s="T388">qwannaš, </ts>
               <ts e="T390" id="Seg_2916" n="e" s="T389">maɣɨlʼǯənnaš. </ts>
               <ts e="T391" id="Seg_2918" n="e" s="T390">Man </ts>
               <ts e="T392" id="Seg_2920" n="e" s="T391">küssukumnan, </ts>
               <ts e="T393" id="Seg_2922" n="e" s="T392">tʼäran: </ts>
               <ts e="T394" id="Seg_2924" n="e" s="T393">“Küzugu </ts>
               <ts e="T395" id="Seg_2926" n="e" s="T394">nado.” </ts>
               <ts e="T396" id="Seg_2928" n="e" s="T395">A </ts>
               <ts e="T397" id="Seg_2930" n="e" s="T396">Valodʼa </ts>
               <ts e="T398" id="Seg_2932" n="e" s="T397">mekga </ts>
               <ts e="T399" id="Seg_2934" n="e" s="T398">tʼärɨn: </ts>
               <ts e="T400" id="Seg_2936" n="e" s="T399">“Qu </ts>
               <ts e="T401" id="Seg_2938" n="e" s="T400">küzenaš? </ts>
               <ts e="T402" id="Seg_2940" n="e" s="T401">Küzəpbədi </ts>
               <ts e="T403" id="Seg_2942" n="e" s="T402">mɨ </ts>
               <ts e="T404" id="Seg_2944" n="e" s="T403">tʼäkgu. </ts>
               <ts e="T405" id="Seg_2946" n="e" s="T404">Küzəpbədi </ts>
               <ts e="T406" id="Seg_2948" n="e" s="T405">mɨla </ts>
               <ts e="T407" id="Seg_2950" n="e" s="T406">wes </ts>
               <ts e="T408" id="Seg_2952" n="e" s="T407">madɨn </ts>
               <ts e="T409" id="Seg_2954" n="e" s="T408">sütʼdʼäɣɨn </ts>
               <ts e="T410" id="Seg_2956" n="e" s="T409">jewattə.” </ts>
               <ts e="T411" id="Seg_2958" n="e" s="T410">Pazarɣɨn </ts>
               <ts e="T412" id="Seg_2960" n="e" s="T411">jezaj. </ts>
               <ts e="T413" id="Seg_2962" n="e" s="T412">Täp </ts>
               <ts e="T414" id="Seg_2964" n="e" s="T413">mekga </ts>
               <ts e="T415" id="Seg_2966" n="e" s="T414">tʼärɨn: </ts>
               <ts e="T416" id="Seg_2968" n="e" s="T415">“Natʼent </ts>
               <ts e="T417" id="Seg_2970" n="e" s="T416">qajno </ts>
               <ts e="T418" id="Seg_2972" n="e" s="T417">as </ts>
               <ts e="T419" id="Seg_2974" n="e" s="T418">küzəzat?” </ts>
               <ts e="T420" id="Seg_2976" n="e" s="T419">A </ts>
               <ts e="T421" id="Seg_2978" n="e" s="T420">man </ts>
               <ts e="T422" id="Seg_2980" n="e" s="T421">ass </ts>
               <ts e="T423" id="Seg_2982" n="e" s="T422">küzɨzaːn. </ts>
               <ts e="T424" id="Seg_2984" n="e" s="T423">Tak </ts>
               <ts e="T425" id="Seg_2986" n="e" s="T424">man </ts>
               <ts e="T426" id="Seg_2988" n="e" s="T425">tʼärpipɨzau. </ts>
               <ts e="T427" id="Seg_2990" n="e" s="T426">Qusakɨn </ts>
               <ts e="T428" id="Seg_2992" n="e" s="T427">mašinam </ts>
               <ts e="T429" id="Seg_2994" n="e" s="T428">aːdɨzaj, </ts>
               <ts e="T430" id="Seg_2996" n="e" s="T429">man </ts>
               <ts e="T431" id="Seg_2998" n="e" s="T430">okkɨr </ts>
               <ts e="T432" id="Seg_3000" n="e" s="T431">näjɣumnä </ts>
               <ts e="T433" id="Seg_3002" n="e" s="T432">soɣutdʼan: </ts>
               <ts e="T434" id="Seg_3004" n="e" s="T433">“Qutʼen </ts>
               <ts e="T435" id="Seg_3006" n="e" s="T434">küzəpbədi </ts>
               <ts e="T436" id="Seg_3008" n="e" s="T435">maːt?” </ts>
               <ts e="T437" id="Seg_3010" n="e" s="T436">Tep </ts>
               <ts e="T438" id="Seg_3012" n="e" s="T437">mekga </ts>
               <ts e="T439" id="Seg_3014" n="e" s="T438">aːdulǯɨt: </ts>
               <ts e="T440" id="Seg_3016" n="e" s="T439">“Tutʼetdo.” </ts>
               <ts e="T441" id="Seg_3018" n="e" s="T440">Man </ts>
               <ts e="T442" id="Seg_3020" n="e" s="T441">kuronnan </ts>
               <ts e="T443" id="Seg_3022" n="e" s="T442">i </ts>
               <ts e="T444" id="Seg_3024" n="e" s="T443">küzan. </ts>
               <ts e="T445" id="Seg_3026" n="e" s="T444">Man </ts>
               <ts e="T446" id="Seg_3028" n="e" s="T445">täjerbɨzan, </ts>
               <ts e="T447" id="Seg_3030" n="e" s="T446">küzeu </ts>
               <ts e="T448" id="Seg_3032" n="e" s="T447">wal </ts>
               <ts e="T449" id="Seg_3034" n="e" s="T448">waǯən. </ts>
               <ts e="T450" id="Seg_3036" n="e" s="T449">Küzan </ts>
               <ts e="T451" id="Seg_3038" n="e" s="T450">i </ts>
               <ts e="T452" id="Seg_3040" n="e" s="T451">soːn </ts>
               <ts e="T453" id="Seg_3042" n="e" s="T452">azun. </ts>
               <ts e="T454" id="Seg_3044" n="e" s="T453">Magazinla </ts>
               <ts e="T455" id="Seg_3046" n="e" s="T454">serkaj, </ts>
               <ts e="T456" id="Seg_3048" n="e" s="T455">iːbɨgaj </ts>
               <ts e="T457" id="Seg_3050" n="e" s="T456">magazilla. </ts>
               <ts e="T458" id="Seg_3052" n="e" s="T457">Natʼen </ts>
               <ts e="T459" id="Seg_3054" n="e" s="T458">maɣulǯennaš. </ts>
               <ts e="T460" id="Seg_3056" n="e" s="T459">Man </ts>
               <ts e="T461" id="Seg_3058" n="e" s="T460">nagɨrɨm </ts>
               <ts e="T462" id="Seg_3060" n="e" s="T461">ass </ts>
               <ts e="T463" id="Seg_3062" n="e" s="T462">tunou. </ts>
               <ts e="T464" id="Seg_3064" n="e" s="T463">Oːnän </ts>
               <ts e="T465" id="Seg_3066" n="e" s="T464">ass </ts>
               <ts e="T466" id="Seg_3068" n="e" s="T465">paldʼiqwan </ts>
               <ts e="T467" id="Seg_3070" n="e" s="T466">qwatčoɣɨn, </ts>
               <ts e="T468" id="Seg_3072" n="e" s="T467">ato </ts>
               <ts e="T469" id="Seg_3074" n="e" s="T468">maɣɨlǯəǯan. </ts>
               <ts e="T470" id="Seg_3076" n="e" s="T469">Man </ts>
               <ts e="T471" id="Seg_3078" n="e" s="T470">täpär </ts>
               <ts e="T472" id="Seg_3080" n="e" s="T471">mašinalaze </ts>
               <ts e="T473" id="Seg_3082" n="e" s="T472">paldʼän. </ts>
               <ts e="T474" id="Seg_3084" n="e" s="T473">Ilɨzan </ts>
               <ts e="T475" id="Seg_3086" n="e" s="T474">qwatčoɣɨn </ts>
               <ts e="T476" id="Seg_3088" n="e" s="T475">ČʼornajaRečʼkaɣɨn </ts>
               <ts e="T477" id="Seg_3090" n="e" s="T476">i </ts>
               <ts e="T478" id="Seg_3092" n="e" s="T477">Taxtamɨšowaɣɨn </ts>
               <ts e="T479" id="Seg_3094" n="e" s="T478">ilɨzan. </ts>
               <ts e="T480" id="Seg_3096" n="e" s="T479">Dunʼa </ts>
               <ts e="T481" id="Seg_3098" n="e" s="T480">tʼüs </ts>
               <ts e="T482" id="Seg_3100" n="e" s="T481">ČʼornajaRʼečʼkat. </ts>
               <ts e="T483" id="Seg_3102" n="e" s="T482">Iːtdɨzʼe </ts>
               <ts e="T484" id="Seg_3104" n="e" s="T483">täbɨstaɣə </ts>
               <ts e="T485" id="Seg_3106" n="e" s="T484">warkaɣə. </ts>
               <ts e="T486" id="Seg_3108" n="e" s="T485">Mikalaj </ts>
               <ts e="T487" id="Seg_3110" n="e" s="T486">Lazarɨčʼin </ts>
               <ts e="T488" id="Seg_3112" n="e" s="T487">pajagat </ts>
               <ts e="T489" id="Seg_3114" n="e" s="T488">qua. </ts>
               <ts e="T490" id="Seg_3116" n="e" s="T489">ČʼornajaRʼečʼkaɣɨn </ts>
               <ts e="T491" id="Seg_3118" n="e" s="T490">warkɨss </ts>
               <ts e="T492" id="Seg_3120" n="e" s="T491">nätdanan. </ts>
               <ts e="T493" id="Seg_3122" n="e" s="T492">A </ts>
               <ts e="T494" id="Seg_3124" n="e" s="T493">man </ts>
               <ts e="T495" id="Seg_3126" n="e" s="T494">qwannan </ts>
               <ts e="T496" id="Seg_3128" n="e" s="T495">qwačilam </ts>
               <ts e="T497" id="Seg_3130" n="e" s="T496">mannɨpugu. </ts>
               <ts e="T498" id="Seg_3132" n="e" s="T497">Marina </ts>
               <ts e="T499" id="Seg_3134" n="e" s="T498">Pawlowna, </ts>
               <ts e="T500" id="Seg_3136" n="e" s="T499">qutdär </ts>
               <ts e="T501" id="Seg_3138" n="e" s="T500">man </ts>
               <ts e="T502" id="Seg_3140" n="e" s="T501">nagɨrmoː </ts>
               <ts e="T503" id="Seg_3142" n="e" s="T502">tütǯɨn, </ts>
               <ts e="T504" id="Seg_3144" n="e" s="T503">i </ts>
               <ts e="T505" id="Seg_3146" n="e" s="T504">mekga </ts>
               <ts e="T506" id="Seg_3148" n="e" s="T505">nagɨrɨm </ts>
               <ts e="T507" id="Seg_3150" n="e" s="T506">srazu </ts>
               <ts e="T508" id="Seg_3152" n="e" s="T507">nagɨrtə. </ts>
               <ts e="T509" id="Seg_3154" n="e" s="T508">Man </ts>
               <ts e="T510" id="Seg_3156" n="e" s="T509">aːdelǯeǯan. </ts>
               <ts e="T511" id="Seg_3158" n="e" s="T510">Wes </ts>
               <ts e="T512" id="Seg_3160" n="e" s="T511">nagɨrtə </ts>
               <ts e="T513" id="Seg_3162" n="e" s="T512">qutdär </ts>
               <ts e="T514" id="Seg_3164" n="e" s="T513">man </ts>
               <ts e="T515" id="Seg_3166" n="e" s="T514">qulau </ts>
               <ts e="T516" id="Seg_3168" n="e" s="T515">warkattə. </ts>
               <ts e="T517" id="Seg_3170" n="e" s="T516">Kolʼän </ts>
               <ts e="T518" id="Seg_3172" n="e" s="T517">tʼät </ts>
               <ts e="T519" id="Seg_3174" n="e" s="T518">qajjamɨm </ts>
               <ts e="T520" id="Seg_3176" n="e" s="T519">üːtdətʼükal </ts>
               <ts e="T521" id="Seg_3178" n="e" s="T520">alʼ </ts>
               <ts e="T522" id="Seg_3180" n="e" s="T521">ass? </ts>
               <ts e="T523" id="Seg_3182" n="e" s="T522">Wes </ts>
               <ts e="T524" id="Seg_3184" n="e" s="T523">mekga </ts>
               <ts e="T525" id="Seg_3186" n="e" s="T524">nagɨrɨt. </ts>
               <ts e="T526" id="Seg_3188" n="e" s="T525">Man </ts>
               <ts e="T527" id="Seg_3190" n="e" s="T526">putʼöm </ts>
               <ts e="T528" id="Seg_3192" n="e" s="T527">aːdɨlǯəǯan. </ts>
               <ts e="T529" id="Seg_3194" n="e" s="T528">Täɣɨtdɨltə </ts>
               <ts e="T530" id="Seg_3196" n="e" s="T529">nʼäjzʼe </ts>
               <ts e="T531" id="Seg_3198" n="e" s="T530">meqwattə </ts>
               <ts e="T532" id="Seg_3200" n="e" s="T531">alʼi </ts>
               <ts e="T533" id="Seg_3202" n="e" s="T532">mogazʼe </ts>
               <ts e="T534" id="Seg_3204" n="e" s="T533">meqwattə? </ts>
               <ts e="T535" id="Seg_3206" n="e" s="T534">A </ts>
               <ts e="T536" id="Seg_3208" n="e" s="T535">magazʼinɣɨn </ts>
               <ts e="T537" id="Seg_3210" n="e" s="T536">qajla </ts>
               <ts e="T538" id="Seg_3212" n="e" s="T537">jetattə, </ts>
               <ts e="T539" id="Seg_3214" n="e" s="T538">wes </ts>
               <ts e="T540" id="Seg_3216" n="e" s="T539">nagɨrtə. </ts>
               <ts e="T541" id="Seg_3218" n="e" s="T540">Ämnäl </ts>
               <ts e="T542" id="Seg_3220" n="e" s="T541">qaj </ts>
               <ts e="T543" id="Seg_3222" n="e" s="T542">iːlɨn </ts>
               <ts e="T544" id="Seg_3224" n="e" s="T543">alʼ </ts>
               <ts e="T545" id="Seg_3226" n="e" s="T544">ass? </ts>
               <ts e="T546" id="Seg_3228" n="e" s="T545">Tʼärzattə </ts>
               <ts e="T547" id="Seg_3230" n="e" s="T546">to </ts>
               <ts e="T548" id="Seg_3232" n="e" s="T547">qwanpa. </ts>
               <ts e="T549" id="Seg_3234" n="e" s="T548">Sinkan </ts>
               <ts e="T550" id="Seg_3236" n="e" s="T549">dʼät </ts>
               <ts e="T551" id="Seg_3238" n="e" s="T550">nagɨrət, </ts>
               <ts e="T552" id="Seg_3240" n="e" s="T551">qutdär </ts>
               <ts e="T553" id="Seg_3242" n="e" s="T552">ilɨkqun </ts>
               <ts e="T554" id="Seg_3244" n="e" s="T553">(warkɨqun). </ts>
               <ts e="T555" id="Seg_3246" n="e" s="T554">Man </ts>
               <ts e="T556" id="Seg_3248" n="e" s="T555">täbnä </ts>
               <ts e="T557" id="Seg_3250" n="e" s="T556">nagɨrɨm </ts>
               <ts e="T558" id="Seg_3252" n="e" s="T557">nagurzan. </ts>
               <ts e="T559" id="Seg_3254" n="e" s="T558">Täp </ts>
               <ts e="T560" id="Seg_3256" n="e" s="T559">mekga </ts>
               <ts e="T561" id="Seg_3258" n="e" s="T560">jass </ts>
               <ts e="T562" id="Seg_3260" n="e" s="T561">nagurgut. </ts>
               <ts e="T563" id="Seg_3262" n="e" s="T562">Tetʼa </ts>
               <ts e="T564" id="Seg_3264" n="e" s="T563">Marina, </ts>
               <ts e="T565" id="Seg_3266" n="e" s="T564">perwɨj </ts>
               <ts e="T566" id="Seg_3268" n="e" s="T565">majzʼe, </ts>
               <ts e="T567" id="Seg_3270" n="e" s="T566">praznikze! </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T14" id="Seg_3271" s="T1">PVD_1964_LetterToMPP_nar.001 (001.001)</ta>
            <ta e="T20" id="Seg_3272" s="T14">PVD_1964_LetterToMPP_nar.002 (001.002)</ta>
            <ta e="T24" id="Seg_3273" s="T20">PVD_1964_LetterToMPP_nar.003 (001.003)</ta>
            <ta e="T35" id="Seg_3274" s="T24">PVD_1964_LetterToMPP_nar.004 (001.004)</ta>
            <ta e="T39" id="Seg_3275" s="T35">PVD_1964_LetterToMPP_nar.005 (001.005)</ta>
            <ta e="T45" id="Seg_3276" s="T39">PVD_1964_LetterToMPP_nar.006 (001.006)</ta>
            <ta e="T47" id="Seg_3277" s="T45">PVD_1964_LetterToMPP_nar.007 (001.007)</ta>
            <ta e="T50" id="Seg_3278" s="T47">PVD_1964_LetterToMPP_nar.008 (001.008)</ta>
            <ta e="T55" id="Seg_3279" s="T50">PVD_1964_LetterToMPP_nar.009 (001.009)</ta>
            <ta e="T59" id="Seg_3280" s="T55">PVD_1964_LetterToMPP_nar.010 (001.010)</ta>
            <ta e="T64" id="Seg_3281" s="T59">PVD_1964_LetterToMPP_nar.011 (001.011)</ta>
            <ta e="T67" id="Seg_3282" s="T64">PVD_1964_LetterToMPP_nar.012 (001.012)</ta>
            <ta e="T71" id="Seg_3283" s="T67">PVD_1964_LetterToMPP_nar.013 (001.013)</ta>
            <ta e="T74" id="Seg_3284" s="T71">PVD_1964_LetterToMPP_nar.014 (001.014)</ta>
            <ta e="T79" id="Seg_3285" s="T74">PVD_1964_LetterToMPP_nar.015 (001.015)</ta>
            <ta e="T86" id="Seg_3286" s="T79">PVD_1964_LetterToMPP_nar.016 (001.016)</ta>
            <ta e="T89" id="Seg_3287" s="T86">PVD_1964_LetterToMPP_nar.017 (001.017)</ta>
            <ta e="T93" id="Seg_3288" s="T89">PVD_1964_LetterToMPP_nar.018 (001.018)</ta>
            <ta e="T97" id="Seg_3289" s="T93">PVD_1964_LetterToMPP_nar.019 (001.019)</ta>
            <ta e="T101" id="Seg_3290" s="T97">PVD_1964_LetterToMPP_nar.020 (001.020)</ta>
            <ta e="T104" id="Seg_3291" s="T101">PVD_1964_LetterToMPP_nar.021 (001.021)</ta>
            <ta e="T108" id="Seg_3292" s="T104">PVD_1964_LetterToMPP_nar.022 (001.022)</ta>
            <ta e="T110" id="Seg_3293" s="T108">PVD_1964_LetterToMPP_nar.023 (001.023)</ta>
            <ta e="T115" id="Seg_3294" s="T110">PVD_1964_LetterToMPP_nar.024 (001.024)</ta>
            <ta e="T125" id="Seg_3295" s="T115">PVD_1964_LetterToMPP_nar.025 (001.025)</ta>
            <ta e="T140" id="Seg_3296" s="T125">PVD_1964_LetterToMPP_nar.026 (001.026)</ta>
            <ta e="T145" id="Seg_3297" s="T140">PVD_1964_LetterToMPP_nar.027 (001.027)</ta>
            <ta e="T149" id="Seg_3298" s="T145">PVD_1964_LetterToMPP_nar.028 (001.028)</ta>
            <ta e="T152" id="Seg_3299" s="T149">PVD_1964_LetterToMPP_nar.029 (001.029)</ta>
            <ta e="T157" id="Seg_3300" s="T152">PVD_1964_LetterToMPP_nar.030 (001.030)</ta>
            <ta e="T163" id="Seg_3301" s="T157">PVD_1964_LetterToMPP_nar.031 (001.031)</ta>
            <ta e="T167" id="Seg_3302" s="T163">PVD_1964_LetterToMPP_nar.032 (001.032)</ta>
            <ta e="T170" id="Seg_3303" s="T167">PVD_1964_LetterToMPP_nar.033 (001.033)</ta>
            <ta e="T174" id="Seg_3304" s="T170">PVD_1964_LetterToMPP_nar.034 (001.034)</ta>
            <ta e="T182" id="Seg_3305" s="T174">PVD_1964_LetterToMPP_nar.035 (001.035)</ta>
            <ta e="T186" id="Seg_3306" s="T182">PVD_1964_LetterToMPP_nar.036 (001.036)</ta>
            <ta e="T189" id="Seg_3307" s="T186">PVD_1964_LetterToMPP_nar.037 (001.037)</ta>
            <ta e="T195" id="Seg_3308" s="T189">PVD_1964_LetterToMPP_nar.038 (001.038)</ta>
            <ta e="T199" id="Seg_3309" s="T195">PVD_1964_LetterToMPP_nar.039 (001.039)</ta>
            <ta e="T206" id="Seg_3310" s="T199">PVD_1964_LetterToMPP_nar.040 (001.040)</ta>
            <ta e="T211" id="Seg_3311" s="T206">PVD_1964_LetterToMPP_nar.041 (001.041)</ta>
            <ta e="T214" id="Seg_3312" s="T211">PVD_1964_LetterToMPP_nar.042 (001.042)</ta>
            <ta e="T224" id="Seg_3313" s="T214">PVD_1964_LetterToMPP_nar.043 (001.043)</ta>
            <ta e="T226" id="Seg_3314" s="T224">PVD_1964_LetterToMPP_nar.044 (001.044)</ta>
            <ta e="T235" id="Seg_3315" s="T226">PVD_1964_LetterToMPP_nar.045 (001.045)</ta>
            <ta e="T240" id="Seg_3316" s="T235">PVD_1964_LetterToMPP_nar.046 (001.046)</ta>
            <ta e="T244" id="Seg_3317" s="T240">PVD_1964_LetterToMPP_nar.047 (001.047)</ta>
            <ta e="T248" id="Seg_3318" s="T244">PVD_1964_LetterToMPP_nar.048 (001.048)</ta>
            <ta e="T251" id="Seg_3319" s="T248">PVD_1964_LetterToMPP_nar.049 (001.049)</ta>
            <ta e="T254" id="Seg_3320" s="T251">PVD_1964_LetterToMPP_nar.050 (001.050)</ta>
            <ta e="T260" id="Seg_3321" s="T254">PVD_1964_LetterToMPP_nar.051 (001.051)</ta>
            <ta e="T267" id="Seg_3322" s="T260">PVD_1964_LetterToMPP_nar.052 (001.052)</ta>
            <ta e="T270" id="Seg_3323" s="T267">PVD_1964_LetterToMPP_nar.053 (001.053)</ta>
            <ta e="T273" id="Seg_3324" s="T270">PVD_1964_LetterToMPP_nar.054 (001.054)</ta>
            <ta e="T279" id="Seg_3325" s="T273">PVD_1964_LetterToMPP_nar.055 (001.055)</ta>
            <ta e="T281" id="Seg_3326" s="T279">PVD_1964_LetterToMPP_nar.056 (001.056)</ta>
            <ta e="T284" id="Seg_3327" s="T281">PVD_1964_LetterToMPP_nar.057 (001.057)</ta>
            <ta e="T286" id="Seg_3328" s="T284">PVD_1964_LetterToMPP_nar.058 (001.058)</ta>
            <ta e="T290" id="Seg_3329" s="T286">PVD_1964_LetterToMPP_nar.059 (001.059)</ta>
            <ta e="T293" id="Seg_3330" s="T290">PVD_1964_LetterToMPP_nar.060 (001.060)</ta>
            <ta e="T296" id="Seg_3331" s="T293">PVD_1964_LetterToMPP_nar.061 (001.061)</ta>
            <ta e="T301" id="Seg_3332" s="T296">PVD_1964_LetterToMPP_nar.062 (001.062)</ta>
            <ta e="T304" id="Seg_3333" s="T301">PVD_1964_LetterToMPP_nar.063 (001.063)</ta>
            <ta e="T308" id="Seg_3334" s="T304">PVD_1964_LetterToMPP_nar.064 (001.064)</ta>
            <ta e="T312" id="Seg_3335" s="T308">PVD_1964_LetterToMPP_nar.065 (001.065)</ta>
            <ta e="T316" id="Seg_3336" s="T312">PVD_1964_LetterToMPP_nar.066 (001.066)</ta>
            <ta e="T322" id="Seg_3337" s="T316">PVD_1964_LetterToMPP_nar.067 (001.067)</ta>
            <ta e="T324" id="Seg_3338" s="T322">PVD_1964_LetterToMPP_nar.068 (001.068)</ta>
            <ta e="T327" id="Seg_3339" s="T324">PVD_1964_LetterToMPP_nar.069 (001.069)</ta>
            <ta e="T331" id="Seg_3340" s="T327">PVD_1964_LetterToMPP_nar.070 (001.070)</ta>
            <ta e="T334" id="Seg_3341" s="T331">PVD_1964_LetterToMPP_nar.071 (001.071)</ta>
            <ta e="T338" id="Seg_3342" s="T334">PVD_1964_LetterToMPP_nar.072 (001.072)</ta>
            <ta e="T341" id="Seg_3343" s="T338">PVD_1964_LetterToMPP_nar.073 (001.073)</ta>
            <ta e="T345" id="Seg_3344" s="T341">PVD_1964_LetterToMPP_nar.074 (001.074)</ta>
            <ta e="T349" id="Seg_3345" s="T345">PVD_1964_LetterToMPP_nar.075 (001.075)</ta>
            <ta e="T356" id="Seg_3346" s="T349">PVD_1964_LetterToMPP_nar.076 (001.076)</ta>
            <ta e="T359" id="Seg_3347" s="T356">PVD_1964_LetterToMPP_nar.077 (001.077)</ta>
            <ta e="T366" id="Seg_3348" s="T359">PVD_1964_LetterToMPP_nar.078 (001.078)</ta>
            <ta e="T370" id="Seg_3349" s="T366">PVD_1964_LetterToMPP_nar.079 (001.079)</ta>
            <ta e="T372" id="Seg_3350" s="T370">PVD_1964_LetterToMPP_nar.080 (001.080)</ta>
            <ta e="T376" id="Seg_3351" s="T372">PVD_1964_LetterToMPP_nar.081 (001.081)</ta>
            <ta e="T379" id="Seg_3352" s="T376">PVD_1964_LetterToMPP_nar.082 (001.082)</ta>
            <ta e="T383" id="Seg_3353" s="T379">PVD_1964_LetterToMPP_nar.083 (001.083)</ta>
            <ta e="T385" id="Seg_3354" s="T383">PVD_1964_LetterToMPP_nar.084 (001.084)</ta>
            <ta e="T390" id="Seg_3355" s="T385">PVD_1964_LetterToMPP_nar.085 (001.085)</ta>
            <ta e="T395" id="Seg_3356" s="T390">PVD_1964_LetterToMPP_nar.086 (001.086)</ta>
            <ta e="T401" id="Seg_3357" s="T395">PVD_1964_LetterToMPP_nar.087 (001.087)</ta>
            <ta e="T404" id="Seg_3358" s="T401">PVD_1964_LetterToMPP_nar.088 (001.088)</ta>
            <ta e="T410" id="Seg_3359" s="T404">PVD_1964_LetterToMPP_nar.089 (001.089)</ta>
            <ta e="T412" id="Seg_3360" s="T410">PVD_1964_LetterToMPP_nar.090 (001.090)</ta>
            <ta e="T419" id="Seg_3361" s="T412">PVD_1964_LetterToMPP_nar.091 (001.091)</ta>
            <ta e="T423" id="Seg_3362" s="T419">PVD_1964_LetterToMPP_nar.092 (001.092)</ta>
            <ta e="T426" id="Seg_3363" s="T423">PVD_1964_LetterToMPP_nar.093 (001.093)</ta>
            <ta e="T436" id="Seg_3364" s="T426">PVD_1964_LetterToMPP_nar.094 (001.094)</ta>
            <ta e="T440" id="Seg_3365" s="T436">PVD_1964_LetterToMPP_nar.095 (001.095)</ta>
            <ta e="T444" id="Seg_3366" s="T440">PVD_1964_LetterToMPP_nar.096 (001.096)</ta>
            <ta e="T449" id="Seg_3367" s="T444">PVD_1964_LetterToMPP_nar.097 (001.097)</ta>
            <ta e="T453" id="Seg_3368" s="T449">PVD_1964_LetterToMPP_nar.098 (001.098)</ta>
            <ta e="T457" id="Seg_3369" s="T453">PVD_1964_LetterToMPP_nar.099 (001.099)</ta>
            <ta e="T459" id="Seg_3370" s="T457">PVD_1964_LetterToMPP_nar.100 (001.100)</ta>
            <ta e="T463" id="Seg_3371" s="T459">PVD_1964_LetterToMPP_nar.101 (001.101)</ta>
            <ta e="T469" id="Seg_3372" s="T463">PVD_1964_LetterToMPP_nar.102 (001.102)</ta>
            <ta e="T473" id="Seg_3373" s="T469">PVD_1964_LetterToMPP_nar.103 (001.103)</ta>
            <ta e="T479" id="Seg_3374" s="T473">PVD_1964_LetterToMPP_nar.104 (001.104)</ta>
            <ta e="T482" id="Seg_3375" s="T479">PVD_1964_LetterToMPP_nar.105 (001.105)</ta>
            <ta e="T485" id="Seg_3376" s="T482">PVD_1964_LetterToMPP_nar.106 (001.106)</ta>
            <ta e="T489" id="Seg_3377" s="T485">PVD_1964_LetterToMPP_nar.107 (001.107)</ta>
            <ta e="T492" id="Seg_3378" s="T489">PVD_1964_LetterToMPP_nar.108 (001.108)</ta>
            <ta e="T497" id="Seg_3379" s="T492">PVD_1964_LetterToMPP_nar.109 (001.109)</ta>
            <ta e="T508" id="Seg_3380" s="T497">PVD_1964_LetterToMPP_nar.110 (001.110)</ta>
            <ta e="T510" id="Seg_3381" s="T508">PVD_1964_LetterToMPP_nar.111 (001.111)</ta>
            <ta e="T516" id="Seg_3382" s="T510">PVD_1964_LetterToMPP_nar.112 (001.112)</ta>
            <ta e="T522" id="Seg_3383" s="T516">PVD_1964_LetterToMPP_nar.113 (001.113)</ta>
            <ta e="T525" id="Seg_3384" s="T522">PVD_1964_LetterToMPP_nar.114 (001.114)</ta>
            <ta e="T528" id="Seg_3385" s="T525">PVD_1964_LetterToMPP_nar.115 (001.115)</ta>
            <ta e="T534" id="Seg_3386" s="T528">PVD_1964_LetterToMPP_nar.116 (001.116)</ta>
            <ta e="T540" id="Seg_3387" s="T534">PVD_1964_LetterToMPP_nar.117 (001.117)</ta>
            <ta e="T545" id="Seg_3388" s="T540">PVD_1964_LetterToMPP_nar.118 (001.118)</ta>
            <ta e="T548" id="Seg_3389" s="T545">PVD_1964_LetterToMPP_nar.119 (001.119)</ta>
            <ta e="T554" id="Seg_3390" s="T548">PVD_1964_LetterToMPP_nar.120 (001.120)</ta>
            <ta e="T558" id="Seg_3391" s="T554">PVD_1964_LetterToMPP_nar.121 (001.121)</ta>
            <ta e="T562" id="Seg_3392" s="T558">PVD_1964_LetterToMPP_nar.122 (001.122)</ta>
            <ta e="T567" id="Seg_3393" s="T562">PVD_1964_LetterToMPP_nar.123 (001.123)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T14" id="Seg_3394" s="T1">ме там′дʼел ′амдай Анге′лина И′вановна′зе, kуlуп′бай, тан′дʼат kуlуп′б̂ай, kут′дӓр вар′катдъ, kай′ла ′ме̄кутдал.</ta>
            <ta e="T20" id="Seg_3395" s="T14">ман теб′нӓ ке′лʼлʼе ′та̄дърау̹ тан′дʼат.</ta>
            <ta e="T24" id="Seg_3396" s="T20">ман тʼӓ′ран: тӓп па′jӓпба.</ta>
            <ta e="T35" id="Seg_3397" s="T24">ман тӓб′нӓ ке′ссау тан мезу′ви Ма′рʼиjазе апстызат, ме те′зʼе kут′дӓр ′ӓрзаwтъ.</ta>
            <ta e="T39" id="Seg_3398" s="T35">ман теб′нӓ вес ке′ссау.</ta>
            <ta e="T45" id="Seg_3399" s="T39">ман kу′лыпб̂ы′зан: ′тʼотка Ма′рʼина ′нагыр ′нагыртшеджан.</ta>
            <ta e="T47" id="Seg_3400" s="T45">пус′кай ′тогулджымдъ.</ta>
            <ta e="T50" id="Seg_3401" s="T47">тʼо′лом, Ма′рʼина Павловна!</ta>
            <ta e="T55" id="Seg_3402" s="T50">ман ′текг̂а сӱ′сӧгъ ′нагыр нагыр′тшеджан.</ta>
            <ta e="T59" id="Seg_3403" s="T55">ка′зах′сен ман асс ту′нан.</ta>
            <ta e="T64" id="Seg_3404" s="T59">ман и′лан тӓ′пӓр kwа′тшоɣын, Новоси′бирскаɣын.</ta>
            <ta e="T67" id="Seg_3405" s="T64">kwа′тшъ ′ӣбыɣай, ′со̄дигай.</ta>
            <ta e="T71" id="Seg_3406" s="T67">kwа′тшоɣындъ Новосибирскат ′поиссе ′тша̄жызан.</ta>
            <ta e="T74" id="Seg_3407" s="T71">′поискан ′тшажъл′е ′kозыртшезаwтъ.</ta>
            <ta e="T79" id="Seg_3408" s="T74">та′jожный ′станцыɣын ′поис ′кӯннын нык′г̂ыс.</ta>
            <ta e="T86" id="Seg_3409" s="T79">ман ‵манныпы′зан и ′Никиным kай‵ɣы′н ӓсс ′kоджирсау̹.</ta>
            <ta e="T89" id="Seg_3410" s="T86">па′том ′поис ′ӱ̄бан.</ta>
            <ta e="T93" id="Seg_3411" s="T89">ман ′kыдан а′кошкаɣын ′манныпызан.</ta>
            <ta e="T97" id="Seg_3412" s="T93">′jедла ко̄цʼин ка′лʼелʼлʼе ′тша̄жызаттъ.</ta>
            <ta e="T101" id="Seg_3413" s="T97">kwа′тшын ′крайɣын ′пола, ′мадʼила.</ta>
            <ta e="T104" id="Seg_3414" s="T101">кру′гом сър ′иппытда.</ta>
            <ta e="T108" id="Seg_3415" s="T104">ман ′поисkын асс ′варыпызан.</ta>
            <ta e="T110" id="Seg_3416" s="T108">kы̄бан когы′рыт.</ta>
            <ta e="T115" id="Seg_3417" s="T110">у′гон ман асс ′паlдӱку′зан ′поезла‵зе.</ta>
            <ta e="T125" id="Seg_3418" s="T115">ман е′лан нӓйɣун′нан, м′ӓ′ɣунтъ на ′тӱккутда сӱ′сӧга ′селам нагыр′лʼе ′та̄дърыст.</ta>
            <ta e="T140" id="Seg_3419" s="T125">kwаттшы′зе нӓйɣум, на ′тӱккыт да ′сатди′jедотдъ сӱ′сӧɣъ ′ӓжлам ′нагыр′ле ′та̄дырыстъ, ′текг̂а ′б̂алʼдʼукус, та′зʼе ′kы̄дан ′нагыртшукус.</ta>
            <ta e="T145" id="Seg_3420" s="T140">ман на′нӓйɣуннан вар′кан, Новоси′бʼирскан.</ta>
            <ta e="T149" id="Seg_3421" s="T145">тӓбын ′нимдъ Анг′лʼина И′ванывна.</ta>
            <ta e="T152" id="Seg_3422" s="T149">ман теб′нан вар′кан.</ta>
            <ta e="T157" id="Seg_3423" s="T152">ма′зым ва′рын со̄н, jас ′kwɛдыпыkун.</ta>
            <ta e="T163" id="Seg_3424" s="T157">ман о̄′нӓн ′мелʼе ′та̄дъръкоу kа′им кы′ган.</ta>
            <ta e="T167" id="Seg_3425" s="T163">теб′нан ′нагур ′сӱтʼдʼе(и) ′ма̄ттъ.</ta>
            <ta e="T170" id="Seg_3426" s="T167">ма̄дын ′сӱтʼдʼила ′wарɣы.</ta>
            <ta e="T174" id="Seg_3427" s="T170">′ма̄т ′пӧтпа, пара′воjе ата′плʼенʼjе.</ta>
            <ta e="T182" id="Seg_3428" s="T174">′ӱт ′ма̄тkын jен: ′пӓртшытдъ ӱт и ′kаннъб̂ӓ‵ди ′ӱт.</ta>
            <ta e="T186" id="Seg_3429" s="T182">′ӱт вес ′ма̄тkын ′jе̨wат.</ta>
            <ta e="T189" id="Seg_3430" s="T186">о′ккыр сӱтʼ′дʼӓɣын ′ӯр‵гофтъ.</ta>
            <ta e="T195" id="Seg_3431" s="T189">на′тʼен пӓр′тшыд ′ӱт и ′kаннӓб̂ӓди ӱт.</ta>
            <ta e="T199" id="Seg_3432" s="T195">′матkын ′тӱтпукоwтъ и ′кӱзепу′коwтъ.</ta>
            <ta e="T206" id="Seg_3433" s="T199">вес мӱ′зулʼджылʼе kwатдъkут и kай ′аптӓнӓй ′тʼӓкку.</ta>
            <ta e="T211" id="Seg_3434" s="T206">цы′почкам наме′жалджал, ӱт ′шоɣърлʼе ′kалукун.</ta>
            <ta e="T214" id="Seg_3435" s="T211">вес ме′зулʼджулʼе ме′жалгут.</ta>
            <ta e="T224" id="Seg_3436" s="T214">ма̄дла тыт′дын ′ӣбыгай, ′те̨̄ттъ ӓташ ′ма̄дла, вес о′ккыр ′сай ′ма̄дла.</ta>
            <ta e="T226" id="Seg_3437" s="T224">′ма̄дла ′тӱпба.</ta>
            <ta e="T235" id="Seg_3438" s="T226">асс kут′доkын ′пола: ′къ̊ла, ′ӱдъ ′пӣла, ′jолкала, ′сос′нала, ′тʼеун′пола, ′пила.</ta>
            <ta e="T240" id="Seg_3439" s="T235">тау(ф)′тʼен та′ɣын ко̄цʼин кры′была ′тʼелымкун.</ta>
            <ta e="T244" id="Seg_3440" s="T240">ме та′ɣын крыбы так′тӓтджу′ту.</ta>
            <ta e="T248" id="Seg_3441" s="T244">ӱ′дын ′коцʼин ′jеkwаттъ све′тоɣыла.</ta>
            <ta e="T251" id="Seg_3442" s="T248">kа̄н ′толʼдʼизʼе паl′дʼӓттъ.</ta>
            <ta e="T254" id="Seg_3443" s="T251">kыбанʼӓжала са′ласка‵зе н′езар′наттъ.</ta>
            <ta e="T260" id="Seg_3444" s="T254">а′ран пон ′тʼӓбла вес ′шолтана а′зӧлʼе джа‵ттъ. (ман а′зӧджан).</ta>
            <ta e="T267" id="Seg_3445" s="T260">kу′саkын ме ′тӱзаwтъ Новоси′бирскат, ′сеkызаwтъ Новосибирскаɣын вак′залɣын.</ta>
            <ta e="T270" id="Seg_3446" s="T267">ин′нӓй ə′ташkын ′сеkызаwтъ.</ta>
            <ta e="T273" id="Seg_3447" s="T270">′соблам ′тӓтте ӓ′ташхын ′kотды‵заwтъ.</ta>
            <ta e="T279" id="Seg_3448" s="T273">мʼӓɣун′ту вес ме′ваттъ: мат′разлам, адʼе′jаллам, па′душкалам.</ta>
            <ta e="T281" id="Seg_3449" s="T279">kотдъ′заwтъ ′со̄дʼиган.</ta>
            <ta e="T284" id="Seg_3450" s="T281">kу′lа ′ко̄цʼин ′jезаттъ.</ta>
            <ta e="T286" id="Seg_3451" s="T284">′ӣбыɣай вакзал.</ta>
            <ta e="T290" id="Seg_3452" s="T286">на′тʼен ′ма̄ɣылджъннаш, jаш тшан′нӓннаш.</ta>
            <ta e="T293" id="Seg_3453" s="T290">kа′рʼемыɣын kwа′ннаwтъ ста′ловайтъ.</ta>
            <ta e="T296" id="Seg_3454" s="T293">теб′ла кет′kwаттъ ресто′ран.</ta>
            <ta e="T301" id="Seg_3455" s="T296">на′тʼен ′со̄диган jен, ′райин ′сӱтдъ.</ta>
            <ta e="T304" id="Seg_3456" s="T301">на′тʼен и аур′саwтъ.</ta>
            <ta e="T308" id="Seg_3457" s="T304">ме′нан ′море тша′жекан jен.</ta>
            <ta e="T312" id="Seg_3458" s="T308">на′тʼен kу′lа kwɛлы′нʼӓттъ ′сыбыkсе.</ta>
            <ta e="T316" id="Seg_3459" s="T312">ныр′сала ‵kwатkу′даттъ и пʼе′дʼела.</ta>
            <ta e="T322" id="Seg_3460" s="T316">kы′банӓ′дӓкkа, ха′зʼӓйкан ′нӓгат, тӓт′кустъ ныр′сан тассыт.</ta>
            <ta e="T324" id="Seg_3461" s="T322">ныр′са kат′тӓдʼипб̂а.</ta>
            <ta e="T327" id="Seg_3462" s="T324">′кошка тӓбым ′апсыт.</ta>
            <ta e="T331" id="Seg_3463" s="T327">ма′ннан сът па′другау ′jеwа.</ta>
            <ta e="T334" id="Seg_3464" s="T331">ман теб′ланӓ ′палʼдʼӱkwан.</ta>
            <ta e="T338" id="Seg_3465" s="T334">и теб′ла ′мекка палʼдʼӱ′kwаттъ.</ta>
            <ta e="T341" id="Seg_3466" s="T338">ман сор′меджан ′поkkылай.</ta>
            <ta e="T345" id="Seg_3467" s="T341">о′ла ′амдыгу асс кы′ган.</ta>
            <ta e="T349" id="Seg_3468" s="T345">а о′нӓн ′кыдан ′kоймут тшу′кан.</ta>
            <ta e="T356" id="Seg_3469" s="T349">ман ′ӱдын ′сатдиjедот ′тӱтджан ′kwӓлыску, ′тшобърым ′kwадъгу.</ta>
            <ta e="T359" id="Seg_3470" s="T356">kwатше ′ӣбыган ′jен.</ta>
            <ta e="T366" id="Seg_3471" s="T359">ман на ′тӱныджан и ′текга ′а̄дуlджеджау̹ kwа′тшын.</ta>
            <ta e="T370" id="Seg_3472" s="T366">′текга на kwат′тшын тат′тшау.</ta>
            <ta e="T372" id="Seg_3473" s="T370">тан ′маннӓ′ме̄джал.</ta>
            <ta e="T376" id="Seg_3474" s="T372">таw′тʼен ′всʼӓкий ма′шиналазʼе палʼдʼи′зан.</ta>
            <ta e="T379" id="Seg_3475" s="T376">′первый рас ′ларыпы′зан.</ta>
            <ta e="T383" id="Seg_3476" s="T379">ма′зым Ва′лодʼӓ kwа′тшоɣын ва′рыс.</ta>
            <ta e="T385" id="Seg_3477" s="T383">вес ′а̄дуlджыɣы‵сыт.</ta>
            <ta e="T390" id="Seg_3478" s="T385">о′нӓт дъ kу kwа′ннаш, ′маɣылʼджъннаш.</ta>
            <ta e="T395" id="Seg_3479" s="T390">ман кӱс′су ′кумнан, тʼӓ′ран: кӱзу′гу надо.</ta>
            <ta e="T401" id="Seg_3480" s="T395">а Ва′лодʼа ′мекга тʼӓ′рын: kу кӱ′зенаш?</ta>
            <ta e="T404" id="Seg_3481" s="T401">′кӱзъпбъди мы ′тʼӓкгу.</ta>
            <ta e="T410" id="Seg_3482" s="T404">′кӱзъпб̂ъди мыла вес ′мадын сӱтʼ′д̂ʼӓɣын ′jеwаттъ.</ta>
            <ta e="T412" id="Seg_3483" s="T410">па′зарɣын ′jезай.</ta>
            <ta e="T419" id="Seg_3484" s="T412">тӓп ′мекга тʼӓ′рын: на′тʼент kай′но ас кӱзъ′зат?</ta>
            <ta e="T423" id="Seg_3485" s="T419">а ман асс ′кӱзыза̄н.</ta>
            <ta e="T426" id="Seg_3486" s="T423">так ман тʼӓрпипы′зау.</ta>
            <ta e="T436" id="Seg_3487" s="T426">kу′сакын ма′шинам а̄ды′зай, ман о′ккыр нӓйɣум′нӓ ′соɣутдʼан: kу′тʼен ′кӱзъпбъди ма̄т?</ta>
            <ta e="T440" id="Seg_3488" s="T436">теп ′мекга ′а̄дуlджыт: ту′тʼетдо.</ta>
            <ta e="T444" id="Seg_3489" s="T440">ман ку′роннан и ′кӱзан.</ta>
            <ta e="T449" id="Seg_3490" s="T444">ман ′тӓйербызан: кӱ′зеу̹ вал′ваджън.</ta>
            <ta e="T453" id="Seg_3491" s="T449">кӱ′зан и со̄н а′зун.</ta>
            <ta e="T457" id="Seg_3492" s="T453">мага′зинла ′серкай, ′ӣбыгай мага′зилла.</ta>
            <ta e="T459" id="Seg_3493" s="T457">на′тʼен ′маɣуlдженнаш.</ta>
            <ta e="T463" id="Seg_3494" s="T459">ман ′нагырым асс ту′ноу̹.</ta>
            <ta e="T469" id="Seg_3495" s="T463">о̄′нӓн асс ′паlдʼиkwан kwат′тшоɣын, а то ′маɣыlджъджан.</ta>
            <ta e="T473" id="Seg_3496" s="T469">ман тӓ′пӓр ма′шиналаз′е паl′дʼӓн.</ta>
            <ta e="T479" id="Seg_3497" s="T473">илы′зан kwат′тшоɣын ′Чʼорнаjа ′речʼкаɣын и Тахта′мышоваɣын илы′зан.</ta>
            <ta e="T482" id="Seg_3498" s="T479">′Д̂унʼа ′тʼӱс чʼорнаjа ′рʼечʼкат.</ta>
            <ta e="T485" id="Seg_3499" s="T482">′ӣтдызʼе ′тӓбыстаɣъ вар′каɣъ.</ta>
            <ta e="T489" id="Seg_3500" s="T485">Мика′лай Ла′зары′чʼин па′jагат ′kуа.</ta>
            <ta e="T492" id="Seg_3501" s="T489">′Чʼорнаjа ′рʼечʼкаɣын ′вар′кысс ′нӓтда‵нан.</ta>
            <ta e="T497" id="Seg_3502" s="T492">а ман kwаннан kwатшилам ′манныпугу.</ta>
            <ta e="T508" id="Seg_3503" s="T497">Марина Павловна, kут′дӓр ман ′нагыр‵мо̄ ′тӱтджын, и ′мекга ′нагырым сразу ′нагыртъ.</ta>
            <ta e="T510" id="Seg_3504" s="T508">ман ′а̄дел′джеджан.</ta>
            <ta e="T516" id="Seg_3505" s="T510">вес ′нагыртъ kут′дӓр ман kу′lау̹ вар′каттъ.</ta>
            <ta e="T522" id="Seg_3506" s="T516">′Колʼӓнтʼӓт kай′jамым ′ӱ̄тдътʼӱкал (ӱтдътӱ′кал) алʼ асс.</ta>
            <ta e="T525" id="Seg_3507" s="T522">вес мекга ′нагырыт.</ta>
            <ta e="T528" id="Seg_3508" s="T525">ман пу′тʼӧм а̄дыл′джъджан.</ta>
            <ta e="T534" id="Seg_3509" s="T528">′тӓɣыт′дылтъ ′нʼӓйзʼе ме′kwаттъ ′алʼи ′могазʼе ме′kwаттъ?</ta>
            <ta e="T540" id="Seg_3510" s="T534">а мага′зʼинɣын kай′ла ′jетаттъ, вес ′нагыртъ.</ta>
            <ta e="T545" id="Seg_3511" s="T540">′ӓмнӓл kай ′ӣлын алʼ асс?</ta>
            <ta e="T548" id="Seg_3512" s="T545">тʼӓр′заттъ ′то kwан′па.</ta>
            <ta e="T554" id="Seg_3513" s="T548">′С(з̂)инкандʼӓт ′нагырът, kут′дӓр илык′kун (варкы′kун).</ta>
            <ta e="T558" id="Seg_3514" s="T554">ман тӓб′нӓ ′нагырым ′нагурзан.</ta>
            <ta e="T562" id="Seg_3515" s="T558">тӓп ′мекга jасс ′нагургут.</ta>
            <ta e="T567" id="Seg_3516" s="T562">тетя Марина, первый ′майзʼе, ′празникзе!</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T14" id="Seg_3517" s="T1">me tamdʼel amdaj Аngelina Иwanownaze, qulupbaj, tandʼat qulupb̂aj, qutdär warkatdə, qajla meːkutdal.</ta>
            <ta e="T20" id="Seg_3518" s="T14">man tebnä kelʼlʼe taːdərau̹ tandʼat.</ta>
            <ta e="T24" id="Seg_3519" s="T20">man tʼäran: täp pajäpba.</ta>
            <ta e="T35" id="Seg_3520" s="T24">man täbnä kessau tan mezuwi Мarʼijaze apstɨzat, me tezʼe qutdär ärzawtə.</ta>
            <ta e="T39" id="Seg_3521" s="T35">man tebnä wes kessau.</ta>
            <ta e="T45" id="Seg_3522" s="T39">man qulɨpb̂ɨzan: tʼotka Мarʼina nagɨr nagɨrtšeǯan.</ta>
            <ta e="T47" id="Seg_3523" s="T45">puskaj togulǯɨmdə.</ta>
            <ta e="T50" id="Seg_3524" s="T47">tʼolom, Мarʼina Пawlowna!</ta>
            <ta e="T55" id="Seg_3525" s="T50">man tekĝa süsögə nagɨr nagɨrtšeǯan.</ta>
            <ta e="T59" id="Seg_3526" s="T55">kazaxsen man ass tunan.</ta>
            <ta e="T64" id="Seg_3527" s="T59">man ilan täpär qwatšoɣɨn, Нowosibirskaɣɨn.</ta>
            <ta e="T67" id="Seg_3528" s="T64">qwatšə iːbɨɣaj, soːdigaj.</ta>
            <ta e="T71" id="Seg_3529" s="T67">qwatšoɣɨndə Нowosibirskat poisse tšaːʒɨzan.</ta>
            <ta e="T74" id="Seg_3530" s="T71">poiskan tšaʒəle qozɨrtšezawtə.</ta>
            <ta e="T79" id="Seg_3531" s="T74">tajoʒnɨj stancɨɣɨn pois kuːnnɨn nɨkĝɨs.</ta>
            <ta e="T86" id="Seg_3532" s="T79">man mannɨpɨzan i Нikinɨm qajɣɨn äss qoǯirsau̹.</ta>
            <ta e="T89" id="Seg_3533" s="T86">patom pois üːban.</ta>
            <ta e="T93" id="Seg_3534" s="T89">man qɨdan akoškaɣɨn mannɨpɨzan.</ta>
            <ta e="T97" id="Seg_3535" s="T93">jedla koːcʼin kalʼelʼlʼe tšaːʒɨzattə.</ta>
            <ta e="T101" id="Seg_3536" s="T97">qwatšɨn krajɣɨn pola, madʼila.</ta>
            <ta e="T104" id="Seg_3537" s="T101">krugom sər ippɨtda.</ta>
            <ta e="T108" id="Seg_3538" s="T104">man poisqɨn ass warɨpɨzan.</ta>
            <ta e="T110" id="Seg_3539" s="T108">qɨːban kogɨrɨt.</ta>
            <ta e="T115" id="Seg_3540" s="T110">ugon man ass paldükuzan poezlaze.</ta>
            <ta e="T125" id="Seg_3541" s="T115">man elan näjɣunnan, mäɣuntə na tükkutda süsöga selam nagɨrlʼe taːdərɨst.</ta>
            <ta e="T140" id="Seg_3542" s="T125">qwattšɨze näjɣum, na tükkɨt da satdijedotdə süsöɣə äʒlam nagɨrle taːdɨrɨstə, tekĝa b̂alʼdʼukus, tazʼe qɨːdan nagɨrtšukus.</ta>
            <ta e="T145" id="Seg_3543" s="T140">man nanäjɣunnan warkan, Нowosibʼirskan.</ta>
            <ta e="T149" id="Seg_3544" s="T145">täbɨn nimdə Аnglʼina Иwanɨwna.</ta>
            <ta e="T152" id="Seg_3545" s="T149">man tebnan warkan.</ta>
            <ta e="T157" id="Seg_3546" s="T152">mazɨm warɨn soːn, jas qwɛdɨpɨqun.</ta>
            <ta e="T163" id="Seg_3547" s="T157">man oːnän melʼe taːdərəkou qaim kɨgan.</ta>
            <ta e="T167" id="Seg_3548" s="T163">tebnan nagur sütʼdʼe(i) maːttə.</ta>
            <ta e="T170" id="Seg_3549" s="T167">maːdɨn sütʼdʼila warɣɨ.</ta>
            <ta e="T174" id="Seg_3550" s="T170">maːt pötpa, parawoje ataplʼenʼje.</ta>
            <ta e="T182" id="Seg_3551" s="T174">üt maːtqɨn jen: pärtšɨtdə üt i qannəb̂ädi üt.</ta>
            <ta e="T186" id="Seg_3552" s="T182">üt wes maːtqɨn jewat.</ta>
            <ta e="T189" id="Seg_3553" s="T186">okkɨr sütʼdʼäɣɨn uːrgoftə.</ta>
            <ta e="T195" id="Seg_3554" s="T189">natʼen pärtšɨd üt i qannäb̂ädi üt.</ta>
            <ta e="T199" id="Seg_3555" s="T195">matqɨn tütpukowtə i küzepukowtə.</ta>
            <ta e="T206" id="Seg_3556" s="T199">wes müzulʼǯɨlʼe qwatdəqut i qaj aptänäj tʼäkku.</ta>
            <ta e="T211" id="Seg_3557" s="T206">cɨpočkam nameʒalǯal, üt šoɣərlʼe qalukun.</ta>
            <ta e="T214" id="Seg_3558" s="T211">wes mezulʼǯulʼe meʒalgut.</ta>
            <ta e="T224" id="Seg_3559" s="T214">maːdla tɨtdɨn iːbɨgaj, teːttə ätaš maːdla, wes okkɨr saj maːdla.</ta>
            <ta e="T226" id="Seg_3560" s="T224">maːdla tüpba.</ta>
            <ta e="T235" id="Seg_3561" s="T226">ass qutdoqɨn pola: kəla, üdə piːla, jolkala, sosnala, tʼeunpola, pila.</ta>
            <ta e="T240" id="Seg_3562" s="T235">tau(f)tʼen taɣɨn koːcʼin krɨbɨla tʼelɨmkun.</ta>
            <ta e="T244" id="Seg_3563" s="T240">me taɣɨn krɨbɨ taktätǯutu.</ta>
            <ta e="T248" id="Seg_3564" s="T244">üdɨn kocʼin jeqwattə swetoɣɨla.</ta>
            <ta e="T251" id="Seg_3565" s="T248">qaːn tolʼdʼizʼe paldʼättə.</ta>
            <ta e="T254" id="Seg_3566" s="T251">qɨbanʼäʒala salaskaze nezarnattə.</ta>
            <ta e="T260" id="Seg_3567" s="T254">aran pon tʼäbla wes šoltana azölʼe ǯattə. (man azöǯan).</ta>
            <ta e="T267" id="Seg_3568" s="T260">qusaqɨn me tüzawtə Нowosibirskat, seqɨzawtə Нowosibirskaɣɨn wakzalɣɨn.</ta>
            <ta e="T270" id="Seg_3569" s="T267">innäj ətašqɨn seqɨzawtə.</ta>
            <ta e="T273" id="Seg_3570" s="T270">soblam tätte ätašxɨn qotdɨzawtə.</ta>
            <ta e="T279" id="Seg_3571" s="T273">mʼäɣuntu wes mewattə: matrazlam, adʼejallam, paduškalam.</ta>
            <ta e="T281" id="Seg_3572" s="T279">qotdəzawtə soːdʼigan.</ta>
            <ta e="T284" id="Seg_3573" s="T281">qula koːcʼin jezattə.</ta>
            <ta e="T286" id="Seg_3574" s="T284">iːbɨɣaj wakzal.</ta>
            <ta e="T290" id="Seg_3575" s="T286">natʼen maːɣɨlǯənnaš, jaš tšannännaš.</ta>
            <ta e="T293" id="Seg_3576" s="T290">qarʼemɨɣɨn qwannawtə stalowajtə.</ta>
            <ta e="T296" id="Seg_3577" s="T293">tebla ketqwattə restoran.</ta>
            <ta e="T301" id="Seg_3578" s="T296">natʼen soːdigan jen, rajin sütdə.</ta>
            <ta e="T304" id="Seg_3579" s="T301">natʼen i aursawtə.</ta>
            <ta e="T308" id="Seg_3580" s="T304">menan more tšaʒekan jen.</ta>
            <ta e="T312" id="Seg_3581" s="T308">natʼen qula qwɛlɨnʼättə sɨbɨqse.</ta>
            <ta e="T316" id="Seg_3582" s="T312">nɨrsala qwatqudattə i pʼedʼela.</ta>
            <ta e="T322" id="Seg_3583" s="T316">qɨbanädäkqa, xazʼäjkan nägat, tätkustə nɨrsan tassɨt.</ta>
            <ta e="T324" id="Seg_3584" s="T322">nɨrsa qattädʼipb̂a.</ta>
            <ta e="T327" id="Seg_3585" s="T324">koška täbɨm apsɨt.</ta>
            <ta e="T331" id="Seg_3586" s="T327">mannan sət padrugau jewa.</ta>
            <ta e="T334" id="Seg_3587" s="T331">man teblanä palʼdʼüqwan.</ta>
            <ta e="T338" id="Seg_3588" s="T334">i tebla mekka palʼdʼüqwattə.</ta>
            <ta e="T341" id="Seg_3589" s="T338">man sormeǯan poqqɨlaj.</ta>
            <ta e="T345" id="Seg_3590" s="T341">ola amdɨgu ass kɨgan.</ta>
            <ta e="T349" id="Seg_3591" s="T345">a onän kɨdan qojmut tšukan.</ta>
            <ta e="T356" id="Seg_3592" s="T349">man üdɨn satdijedot tütǯan qwälɨsku, tšobərɨm qwadəgu.</ta>
            <ta e="T359" id="Seg_3593" s="T356">qwatše iːbɨgan jen.</ta>
            <ta e="T366" id="Seg_3594" s="T359">man na tünɨǯan i tekga aːdulǯeǯau̹ qwatšɨn.</ta>
            <ta e="T370" id="Seg_3595" s="T366">tekga na qwattšɨn tattšau.</ta>
            <ta e="T372" id="Seg_3596" s="T370">tan mannämeːǯal.</ta>
            <ta e="T376" id="Seg_3597" s="T372">tawtʼen wsʼäkij mašinalazʼe palʼdʼizan.</ta>
            <ta e="T379" id="Seg_3598" s="T376">perwɨj ras larɨpɨzan.</ta>
            <ta e="T383" id="Seg_3599" s="T379">mazɨm Вalodʼä qwatšoɣɨn warɨs.</ta>
            <ta e="T385" id="Seg_3600" s="T383">wes aːdulǯɨɣɨsɨt.</ta>
            <ta e="T390" id="Seg_3601" s="T385">onät də qu qwannaš, maɣɨlʼǯənnaš.</ta>
            <ta e="T395" id="Seg_3602" s="T390">man küssu kumnan, tʼäran: küzugu nado.</ta>
            <ta e="T401" id="Seg_3603" s="T395">a Вalodʼa mekga tʼärɨn: qu küzenaš?</ta>
            <ta e="T404" id="Seg_3604" s="T401">küzəpbədi mɨ tʼäkgu.</ta>
            <ta e="T410" id="Seg_3605" s="T404">küzəpb̂ədi mɨla wes madɨn sütʼd̂ʼäɣɨn jewattə.</ta>
            <ta e="T412" id="Seg_3606" s="T410">pazarɣɨn jezaj.</ta>
            <ta e="T419" id="Seg_3607" s="T412">täp mekga tʼärɨn: natʼent qajno as küzəzat?</ta>
            <ta e="T423" id="Seg_3608" s="T419">a man ass küzɨzaːn.</ta>
            <ta e="T426" id="Seg_3609" s="T423">tak man tʼärpipɨzau.</ta>
            <ta e="T436" id="Seg_3610" s="T426">qusakɨn mašinam aːdɨzaj, man okkɨr näjɣumnä soɣutdʼan: qutʼen küzəpbədi maːt?</ta>
            <ta e="T440" id="Seg_3611" s="T436">tep mekga aːdulǯɨt: tutʼetdo.</ta>
            <ta e="T444" id="Seg_3612" s="T440">man kuronnan i küzan.</ta>
            <ta e="T449" id="Seg_3613" s="T444">man täjerbɨzan: küzeu̹ walwaǯən.</ta>
            <ta e="T453" id="Seg_3614" s="T449">küzan i soːn azun.</ta>
            <ta e="T457" id="Seg_3615" s="T453">magazinla serkaj, iːbɨgaj magazilla.</ta>
            <ta e="T459" id="Seg_3616" s="T457">natʼen maɣulǯennaš.</ta>
            <ta e="T463" id="Seg_3617" s="T459">man nagɨrɨm ass tunou̹.</ta>
            <ta e="T469" id="Seg_3618" s="T463">oːnän ass paldʼiqwan qwattšoɣɨn, a to maɣɨlǯəǯan.</ta>
            <ta e="T473" id="Seg_3619" s="T469">man täpär mašinalaze paldʼän.</ta>
            <ta e="T479" id="Seg_3620" s="T473">ilɨzan qwattšoɣɨn Чʼornaja rečʼkaɣɨn i Тaxtamɨšowaɣɨn ilɨzan.</ta>
            <ta e="T482" id="Seg_3621" s="T479">Д̂unʼa tʼüs čʼornaja rʼečʼkat.</ta>
            <ta e="T485" id="Seg_3622" s="T482">iːtdɨzʼe täbɨstaɣə warkaɣə.</ta>
            <ta e="T489" id="Seg_3623" s="T485">Мikalaj Лazarɨčʼin pajagat qua.</ta>
            <ta e="T492" id="Seg_3624" s="T489">Чʼornaja rʼečʼkaɣɨn warkɨss nätdanan.</ta>
            <ta e="T497" id="Seg_3625" s="T492">a man qwannan qwatšilam mannɨpugu.</ta>
            <ta e="T508" id="Seg_3626" s="T497">Мarina Пawlowna, qutdär man nagɨrmoː tütǯɨn, i mekga nagɨrɨm srazu nagɨrtə.</ta>
            <ta e="T510" id="Seg_3627" s="T508">man aːdelǯeǯan.</ta>
            <ta e="T516" id="Seg_3628" s="T510">wes nagɨrtə qutdär man qulau̹ warkattə.</ta>
            <ta e="T522" id="Seg_3629" s="T516">Кolʼäntʼät qajjamɨm üːtdətʼükal (ütdətükal) alʼ ass.</ta>
            <ta e="T525" id="Seg_3630" s="T522">wes mekga nagɨrɨt.</ta>
            <ta e="T528" id="Seg_3631" s="T525">man putʼöm aːdɨlǯəǯan.</ta>
            <ta e="T534" id="Seg_3632" s="T528">täɣɨtdɨltə nʼäjzʼe meqwattə alʼi mogazʼe meqwattə?</ta>
            <ta e="T540" id="Seg_3633" s="T534">a magazʼinɣɨn qajla jetattə, wes nagɨrtə.</ta>
            <ta e="T545" id="Seg_3634" s="T540">ämnäl qaj iːlɨn alʼ ass?</ta>
            <ta e="T548" id="Seg_3635" s="T545">tʼärzattə to qwanpa.</ta>
            <ta e="T554" id="Seg_3636" s="T548">С(ẑ)inkandʼät nagɨrət, qutdär ilɨkqun (warkɨqun).</ta>
            <ta e="T558" id="Seg_3637" s="T554">man täbnä nagɨrɨm nagurzan.</ta>
            <ta e="T562" id="Seg_3638" s="T558">täp mekga jass nagurgut.</ta>
            <ta e="T567" id="Seg_3639" s="T562">tetʼa Мarina, perwɨj majzʼe, praznikze!</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T14" id="Seg_3640" s="T1">Me tamdʼel amdaj Angelina Iwanownaze, qulupbaj, tan dʼat qulupbaj, qutdär warkatdə, qajla meːkutdal. </ta>
            <ta e="T20" id="Seg_3641" s="T14">Man tebnä kelʼlʼe taːdərau tan dʼat. </ta>
            <ta e="T24" id="Seg_3642" s="T20">Man tʼäran: “Täp pajäpba.” </ta>
            <ta e="T35" id="Seg_3643" s="T24">Man täbnä kessau tan mezuwi Мarʼijaze apstɨzat, me tezʼe qutdär ärzawtə. </ta>
            <ta e="T39" id="Seg_3644" s="T35">Man tebnä wes kessau. </ta>
            <ta e="T45" id="Seg_3645" s="T39">Man qulɨpbɨzan: “Tʼotka Marʼina nagɨr nagɨrčeǯan.” </ta>
            <ta e="T47" id="Seg_3646" s="T45">Puskaj togulǯɨmdə. </ta>
            <ta e="T50" id="Seg_3647" s="T47">Tʼolom, Marʼina Pawlowna! </ta>
            <ta e="T55" id="Seg_3648" s="T50">Man tekga süsögə nagɨr nagɨrčeǯan. </ta>
            <ta e="T59" id="Seg_3649" s="T55">Kazaxsen man ass tunan. </ta>
            <ta e="T64" id="Seg_3650" s="T59">Man ilan täpär qwačoɣɨn, Nowosibirskaɣɨn. </ta>
            <ta e="T67" id="Seg_3651" s="T64">Qwačə iːbɨɣaj, soːdigaj. </ta>
            <ta e="T71" id="Seg_3652" s="T67">Qwačoɣɨndə Nowosibirskat poisse čaːʒɨzan. </ta>
            <ta e="T74" id="Seg_3653" s="T71">Poiskan čaʒəle qozɨrčezawtə. </ta>
            <ta e="T79" id="Seg_3654" s="T74">Tajoʒnɨj stancɨɣɨn pois kuːnnɨn nɨkgɨs. </ta>
            <ta e="T86" id="Seg_3655" s="T79">Man mannɨpɨzan i Nikinɨm qajɣɨn äss qoǯirsau. </ta>
            <ta e="T89" id="Seg_3656" s="T86">Patom pois üːban. </ta>
            <ta e="T93" id="Seg_3657" s="T89">Man qɨdan akoškaɣɨn mannɨpɨzan. </ta>
            <ta e="T97" id="Seg_3658" s="T93">Jedla koːcʼin kalʼelʼlʼe čaːʒɨzattə. </ta>
            <ta e="T101" id="Seg_3659" s="T97">Qwačɨn krajɣɨn pola, madʼila. </ta>
            <ta e="T104" id="Seg_3660" s="T101">Krugom sər ippɨtda. </ta>
            <ta e="T108" id="Seg_3661" s="T104">Man poisqɨn ass warɨpɨzan. </ta>
            <ta e="T110" id="Seg_3662" s="T108">Qɨːban kogɨrɨt. </ta>
            <ta e="T115" id="Seg_3663" s="T110">Ugon man ass paldükuzan poezlaze. </ta>
            <ta e="T125" id="Seg_3664" s="T115">Man elan näjɣunnan, mäɣuntə na tükkutda süsöga selam nagɨrlʼe taːdərɨst. </ta>
            <ta e="T140" id="Seg_3665" s="T125">Qwatčɨze näjɣum, na tükkɨt da Satdijedotdə süsöɣə äʒlam nagɨrle taːdɨrɨstə, tekga balʼdʼukus, tazʼe qɨːdan nagɨrčukus. </ta>
            <ta e="T145" id="Seg_3666" s="T140">Man na näjɣunnan warkan, Nowosibʼirskan. </ta>
            <ta e="T149" id="Seg_3667" s="T145">Täbɨn nimdə Anglʼina Iwanɨwna. </ta>
            <ta e="T152" id="Seg_3668" s="T149">Man tebnan warkan. </ta>
            <ta e="T157" id="Seg_3669" s="T152">Mazɨm warɨn soːn, jas qwɛdɨpɨqun. </ta>
            <ta e="T163" id="Seg_3670" s="T157">Man oːnän melʼe taːdərəkou qaim kɨgan. </ta>
            <ta e="T167" id="Seg_3671" s="T163">Tebnan nagur sütʼdʼe maːttə. </ta>
            <ta e="T170" id="Seg_3672" s="T167">Maːdɨn sütʼdʼila warɣɨ. </ta>
            <ta e="T174" id="Seg_3673" s="T170">Maːt pötpa, parawoje ataplʼenʼje. </ta>
            <ta e="T182" id="Seg_3674" s="T174">Üt maːtqɨn jen: pärčɨtdə üt i qannəbädi üt. </ta>
            <ta e="T186" id="Seg_3675" s="T182">Üt wes maːtqɨn jewat. </ta>
            <ta e="T189" id="Seg_3676" s="T186">Okkɨr sütʼdʼäɣɨn uːrgoftə. </ta>
            <ta e="T195" id="Seg_3677" s="T189">Natʼen pärčɨd üt i qannäbädi üt. </ta>
            <ta e="T199" id="Seg_3678" s="T195">Matqɨn tütpukowtə i küzepukowtə. </ta>
            <ta e="T206" id="Seg_3679" s="T199">Wes müzulʼǯɨlʼe qwatdəqut i qaj aptänäj tʼäkku. </ta>
            <ta e="T211" id="Seg_3680" s="T206">Cɨpočkam nameʒalǯal, üt šoɣərlʼe qalukun. </ta>
            <ta e="T214" id="Seg_3681" s="T211">Wes mezulʼǯulʼe meʒalgut. </ta>
            <ta e="T224" id="Seg_3682" s="T214">Maːdla tɨtdɨn iːbɨgaj, teːttə ätaš maːdla, wes okkɨr saj maːdla. </ta>
            <ta e="T226" id="Seg_3683" s="T224">Maːdla tüpba. </ta>
            <ta e="T235" id="Seg_3684" s="T226">Ass qutdoqɨn pola: kəla, üdəpiːla, jolkala, sosnala, tʼeunpola, pila. </ta>
            <ta e="T240" id="Seg_3685" s="T235">Tautʼen taɣɨn koːcʼin krɨbɨla tʼelɨmkun. </ta>
            <ta e="T244" id="Seg_3686" s="T240">Me taɣɨn krɨbɨ taktätǯutu. </ta>
            <ta e="T248" id="Seg_3687" s="T244">Üdɨn kocʼin jeqwattə swetoɣɨla. </ta>
            <ta e="T251" id="Seg_3688" s="T248">Qaːn tolʼdʼizʼe paldʼättə. </ta>
            <ta e="T254" id="Seg_3689" s="T251">Qɨbanʼäʒala salaskaze nezarnattə. </ta>
            <ta e="T260" id="Seg_3690" s="T254">Aran pon tʼäbla wes šoltana azölʼeǯattə. </ta>
            <ta e="T267" id="Seg_3691" s="T260">Qusaqɨn me tüzawtə Nowosibirskat, seqɨzawtə Nowosibirskaɣɨn wakzalɣɨn. </ta>
            <ta e="T270" id="Seg_3692" s="T267">Innäj ətašqɨn seqɨzawtə. </ta>
            <ta e="T273" id="Seg_3693" s="T270">Soblamtätte ätašxɨn qotdɨzawtə. </ta>
            <ta e="T279" id="Seg_3694" s="T273">Mʼäɣuntu wes mewattə: matrazlam, adʼejallam, paduškalam. </ta>
            <ta e="T281" id="Seg_3695" s="T279">Qotdəzawtə soːdʼigan. </ta>
            <ta e="T284" id="Seg_3696" s="T281">Qula koːcʼin jezattə. </ta>
            <ta e="T286" id="Seg_3697" s="T284">Iːbɨɣaj wakzal. </ta>
            <ta e="T290" id="Seg_3698" s="T286">Natʼen maːɣɨlǯənnaš, jaš čannännaš. </ta>
            <ta e="T293" id="Seg_3699" s="T290">Qarʼemɨɣɨn qwannawtə stalowajtə. </ta>
            <ta e="T296" id="Seg_3700" s="T293">Tebla ketqwattə restoran. </ta>
            <ta e="T301" id="Seg_3701" s="T296">Natʼen soːdigan jen, rajin sütdə. </ta>
            <ta e="T304" id="Seg_3702" s="T301">Natʼen i aursawtə. </ta>
            <ta e="T308" id="Seg_3703" s="T304">Menan more čaʒekan jen. </ta>
            <ta e="T312" id="Seg_3704" s="T308">Natʼen qula qwɛlɨnʼättə sɨbɨqse. </ta>
            <ta e="T316" id="Seg_3705" s="T312">Nɨrsala qwatqudattə i pʼedʼela. </ta>
            <ta e="T322" id="Seg_3706" s="T316">Qɨbanädäkqa, xazʼäjkan nägat, tätkustə nɨrsan tassɨt. </ta>
            <ta e="T324" id="Seg_3707" s="T322">Nɨrsa qattädʼipba. </ta>
            <ta e="T327" id="Seg_3708" s="T324">Koška täbɨm apsɨt. </ta>
            <ta e="T331" id="Seg_3709" s="T327">Mannan sət padrugau jewa. </ta>
            <ta e="T334" id="Seg_3710" s="T331">Man teblanä palʼdʼüqwan. </ta>
            <ta e="T338" id="Seg_3711" s="T334">I tebla mekka palʼdʼüqwattə. </ta>
            <ta e="T341" id="Seg_3712" s="T338">Man sormeǯan poqqɨlaj. </ta>
            <ta e="T345" id="Seg_3713" s="T341">Ola amdɨgu ass kɨgan. </ta>
            <ta e="T349" id="Seg_3714" s="T345">A onän kɨdan qojmutčukan. </ta>
            <ta e="T356" id="Seg_3715" s="T349">Man üdɨn Satdijedot tütǯan qwälɨsku, čobərɨm qwadəgu. </ta>
            <ta e="T359" id="Seg_3716" s="T356">Qwače iːbɨgan jen. </ta>
            <ta e="T366" id="Seg_3717" s="T359">Man na tünɨǯan i tekga aːdulǯeǯau qwačɨn. </ta>
            <ta e="T370" id="Seg_3718" s="T366">Tekga na qwatčɨn tatčau. </ta>
            <ta e="T372" id="Seg_3719" s="T370">Tan mannämeːǯal. </ta>
            <ta e="T376" id="Seg_3720" s="T372">Tawtʼen wsʼäkij mašinalazʼe palʼdʼizan. </ta>
            <ta e="T379" id="Seg_3721" s="T376">Perwɨj ras larɨpɨzan. </ta>
            <ta e="T383" id="Seg_3722" s="T379">Mazɨm Valodʼä qwačoɣɨn warɨs. </ta>
            <ta e="T385" id="Seg_3723" s="T383">Wes aːdulǯɨɣɨsɨt. </ta>
            <ta e="T390" id="Seg_3724" s="T385">Onät də qu qwannaš, maɣɨlʼǯənnaš. </ta>
            <ta e="T395" id="Seg_3725" s="T390">Man küssukumnan, tʼäran: “Küzugu nado.” </ta>
            <ta e="T401" id="Seg_3726" s="T395">A Valodʼa mekga tʼärɨn: “Qu küzenaš? </ta>
            <ta e="T404" id="Seg_3727" s="T401">Küzəpbədi mɨ tʼäkgu. </ta>
            <ta e="T410" id="Seg_3728" s="T404">Küzəpbədi mɨla wes madɨn sütʼdʼäɣɨn jewattə.” </ta>
            <ta e="T412" id="Seg_3729" s="T410">Pazarɣɨn jezaj. </ta>
            <ta e="T419" id="Seg_3730" s="T412">Täp mekga tʼärɨn: “Natʼent qajno as küzəzat?” </ta>
            <ta e="T423" id="Seg_3731" s="T419">A man ass küzɨzaːn. </ta>
            <ta e="T426" id="Seg_3732" s="T423">Tak man tʼärpipɨzau. </ta>
            <ta e="T436" id="Seg_3733" s="T426">Qusakɨn mašinam aːdɨzaj, man okkɨr näjɣumnä soɣutdʼan: “Qutʼen küzəpbədi maːt?” </ta>
            <ta e="T440" id="Seg_3734" s="T436">Tep mekga aːdulǯɨt: “Tutʼetdo.” </ta>
            <ta e="T444" id="Seg_3735" s="T440">Man kuronnan i küzan. </ta>
            <ta e="T449" id="Seg_3736" s="T444">Man täjerbɨzan, küzeu wal waǯən. </ta>
            <ta e="T453" id="Seg_3737" s="T449">Küzan i soːn azun. </ta>
            <ta e="T457" id="Seg_3738" s="T453">Magazinla serkaj, iːbɨgaj magazilla. </ta>
            <ta e="T459" id="Seg_3739" s="T457">Natʼen maɣulǯennaš. </ta>
            <ta e="T463" id="Seg_3740" s="T459">Man nagɨrɨm ass tunou. </ta>
            <ta e="T469" id="Seg_3741" s="T463">Oːnän ass paldʼiqwan qwatčoɣɨn, ato maɣɨlǯəǯan. </ta>
            <ta e="T473" id="Seg_3742" s="T469">Man täpär mašinalaze paldʼän. </ta>
            <ta e="T479" id="Seg_3743" s="T473">Ilɨzan qwatčoɣɨn ČʼornajaRečʼkaɣɨn i Taxtamɨšowaɣɨn ilɨzan. </ta>
            <ta e="T482" id="Seg_3744" s="T479">Dunʼa tʼüs ČʼornajaRʼečʼkat. </ta>
            <ta e="T485" id="Seg_3745" s="T482">Iːtdɨzʼe täbɨstaɣə warkaɣə. </ta>
            <ta e="T489" id="Seg_3746" s="T485">Mikalaj Lazarɨčʼin pajagat qua. </ta>
            <ta e="T492" id="Seg_3747" s="T489">ČʼornajaRʼečʼkaɣɨn warkɨss nätdanan. </ta>
            <ta e="T497" id="Seg_3748" s="T492">A man qwannan qwačilam mannɨpugu. </ta>
            <ta e="T508" id="Seg_3749" s="T497">Marina Pawlowna, qutdär man nagɨrmoː tütǯɨn, i mekga nagɨrɨm srazu nagɨrtə. </ta>
            <ta e="T510" id="Seg_3750" s="T508">Man aːdelǯeǯan. </ta>
            <ta e="T516" id="Seg_3751" s="T510">Wes nagɨrtə qutdär man qulau warkattə. </ta>
            <ta e="T522" id="Seg_3752" s="T516">Kolʼän tʼät qajjamɨm üːtdətʼükal alʼ ass? </ta>
            <ta e="T525" id="Seg_3753" s="T522">Wes mekga nagɨrɨt. </ta>
            <ta e="T528" id="Seg_3754" s="T525">Man putʼöm aːdɨlǯəǯan. </ta>
            <ta e="T534" id="Seg_3755" s="T528">Täɣɨtdɨltə nʼäjzʼe meqwattə alʼi mogazʼe meqwattə? </ta>
            <ta e="T540" id="Seg_3756" s="T534">A magazʼinɣɨn qajla jetattə, wes nagɨrtə. </ta>
            <ta e="T545" id="Seg_3757" s="T540">Ämnäl qaj iːlɨn alʼ ass? </ta>
            <ta e="T548" id="Seg_3758" s="T545">Tʼärzattə to qwanpa. </ta>
            <ta e="T554" id="Seg_3759" s="T548">Sinkan dʼät nagɨrət, qutdär ilɨkqun (warkɨqun). </ta>
            <ta e="T558" id="Seg_3760" s="T554">Man täbnä nagɨrɨm nagurzan. </ta>
            <ta e="T562" id="Seg_3761" s="T558">Täp mekga jass nagurgut. </ta>
            <ta e="T567" id="Seg_3762" s="T562">Tetʼa Marina, perwɨj majzʼe, praznikze! </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_3763" s="T1">me</ta>
            <ta e="T3" id="Seg_3764" s="T2">tam-dʼel</ta>
            <ta e="T4" id="Seg_3765" s="T3">amda-j</ta>
            <ta e="T5" id="Seg_3766" s="T4">Angelina</ta>
            <ta e="T6" id="Seg_3767" s="T5">Iwanowna-ze</ta>
            <ta e="T7" id="Seg_3768" s="T6">qulupba-j</ta>
            <ta e="T8" id="Seg_3769" s="T7">tat</ta>
            <ta e="T9" id="Seg_3770" s="T8">dʼat</ta>
            <ta e="T10" id="Seg_3771" s="T9">qulupba-j</ta>
            <ta e="T11" id="Seg_3772" s="T10">qutdär</ta>
            <ta e="T12" id="Seg_3773" s="T11">warka-tdə</ta>
            <ta e="T13" id="Seg_3774" s="T12">qaj-la</ta>
            <ta e="T14" id="Seg_3775" s="T13">meː-ku-tda-l</ta>
            <ta e="T15" id="Seg_3776" s="T14">man</ta>
            <ta e="T16" id="Seg_3777" s="T15">teb-nä</ta>
            <ta e="T17" id="Seg_3778" s="T16">kelʼ-lʼe</ta>
            <ta e="T18" id="Seg_3779" s="T17">taːd-ə-r-a-u</ta>
            <ta e="T19" id="Seg_3780" s="T18">tat</ta>
            <ta e="T20" id="Seg_3781" s="T19">dʼat</ta>
            <ta e="T21" id="Seg_3782" s="T20">man</ta>
            <ta e="T22" id="Seg_3783" s="T21">tʼära-n</ta>
            <ta e="T23" id="Seg_3784" s="T22">täp</ta>
            <ta e="T24" id="Seg_3785" s="T23">pajä-p-ba</ta>
            <ta e="T25" id="Seg_3786" s="T24">man</ta>
            <ta e="T26" id="Seg_3787" s="T25">täb-nä</ta>
            <ta e="T27" id="Seg_3788" s="T26">kes-sa-u</ta>
            <ta e="T28" id="Seg_3789" s="T27">tat</ta>
            <ta e="T29" id="Seg_3790" s="T28">mezuwi</ta>
            <ta e="T30" id="Seg_3791" s="T29">Мarʼija-ze</ta>
            <ta e="T31" id="Seg_3792" s="T30">aps-tɨ-za-t</ta>
            <ta e="T32" id="Seg_3793" s="T31">me</ta>
            <ta e="T33" id="Seg_3794" s="T32">te-zʼe</ta>
            <ta e="T34" id="Seg_3795" s="T33">qutdär</ta>
            <ta e="T35" id="Seg_3796" s="T34">är-za-wtə</ta>
            <ta e="T36" id="Seg_3797" s="T35">man</ta>
            <ta e="T37" id="Seg_3798" s="T36">teb-nä</ta>
            <ta e="T38" id="Seg_3799" s="T37">wes</ta>
            <ta e="T39" id="Seg_3800" s="T38">kes-sa-u</ta>
            <ta e="T40" id="Seg_3801" s="T39">man</ta>
            <ta e="T41" id="Seg_3802" s="T40">qulɨpbɨ-za-n</ta>
            <ta e="T42" id="Seg_3803" s="T41">tʼotka</ta>
            <ta e="T43" id="Seg_3804" s="T42">Marʼina</ta>
            <ta e="T44" id="Seg_3805" s="T43">nagɨr</ta>
            <ta e="T45" id="Seg_3806" s="T44">nagɨr-č-eǯa-n</ta>
            <ta e="T46" id="Seg_3807" s="T45">puskaj</ta>
            <ta e="T47" id="Seg_3808" s="T46">togu-lǯɨ-mdə</ta>
            <ta e="T48" id="Seg_3809" s="T47">tʼolom</ta>
            <ta e="T49" id="Seg_3810" s="T48">Marʼina</ta>
            <ta e="T50" id="Seg_3811" s="T49">Pawlowna</ta>
            <ta e="T51" id="Seg_3812" s="T50">man</ta>
            <ta e="T52" id="Seg_3813" s="T51">tekga</ta>
            <ta e="T53" id="Seg_3814" s="T52">süsögə</ta>
            <ta e="T54" id="Seg_3815" s="T53">nagɨr</ta>
            <ta e="T55" id="Seg_3816" s="T54">nagɨr-č-eǯa-n</ta>
            <ta e="T56" id="Seg_3817" s="T55">kazax-se-n</ta>
            <ta e="T57" id="Seg_3818" s="T56">man</ta>
            <ta e="T58" id="Seg_3819" s="T57">ass</ta>
            <ta e="T59" id="Seg_3820" s="T58">tuna-n</ta>
            <ta e="T60" id="Seg_3821" s="T59">man</ta>
            <ta e="T61" id="Seg_3822" s="T60">ila-n</ta>
            <ta e="T62" id="Seg_3823" s="T61">täpär</ta>
            <ta e="T63" id="Seg_3824" s="T62">qwačo-ɣɨn</ta>
            <ta e="T64" id="Seg_3825" s="T63">Nowosibirsk-a-ɣɨn</ta>
            <ta e="T65" id="Seg_3826" s="T64">qwačə</ta>
            <ta e="T66" id="Seg_3827" s="T65">iːbɨɣaj</ta>
            <ta e="T67" id="Seg_3828" s="T66">soːdiga-j</ta>
            <ta e="T68" id="Seg_3829" s="T67">qwačo-ɣɨndə</ta>
            <ta e="T69" id="Seg_3830" s="T68">Nowosibirsk-a-t</ta>
            <ta e="T70" id="Seg_3831" s="T69">pois-se</ta>
            <ta e="T71" id="Seg_3832" s="T70">čaːʒɨ-za-n</ta>
            <ta e="T72" id="Seg_3833" s="T71">pois-kan</ta>
            <ta e="T73" id="Seg_3834" s="T72">čaʒə-le</ta>
            <ta e="T74" id="Seg_3835" s="T73">qozɨr-če-za-wtə</ta>
            <ta e="T75" id="Seg_3836" s="T74">Tajoʒnɨj</ta>
            <ta e="T76" id="Seg_3837" s="T75">stancɨ-ɣɨn</ta>
            <ta e="T77" id="Seg_3838" s="T76">pois</ta>
            <ta e="T78" id="Seg_3839" s="T77">kuːnnɨ-n</ta>
            <ta e="T79" id="Seg_3840" s="T78">nɨ-kgɨ-s</ta>
            <ta e="T80" id="Seg_3841" s="T79">man</ta>
            <ta e="T81" id="Seg_3842" s="T80">mannɨ-pɨ-za-n</ta>
            <ta e="T82" id="Seg_3843" s="T81">i</ta>
            <ta e="T83" id="Seg_3844" s="T82">Nikin-ɨ-m</ta>
            <ta e="T84" id="Seg_3845" s="T83">qaj-ɣɨn</ta>
            <ta e="T85" id="Seg_3846" s="T84">äss</ta>
            <ta e="T86" id="Seg_3847" s="T85">qo-ǯir-sa-u</ta>
            <ta e="T87" id="Seg_3848" s="T86">patom</ta>
            <ta e="T88" id="Seg_3849" s="T87">pois</ta>
            <ta e="T89" id="Seg_3850" s="T88">üːba-n</ta>
            <ta e="T90" id="Seg_3851" s="T89">man</ta>
            <ta e="T91" id="Seg_3852" s="T90">qɨdan</ta>
            <ta e="T92" id="Seg_3853" s="T91">akoška-ɣɨn</ta>
            <ta e="T93" id="Seg_3854" s="T92">mannɨ-pɨ-za-n</ta>
            <ta e="T94" id="Seg_3855" s="T93">jed-la</ta>
            <ta e="T95" id="Seg_3856" s="T94">koːcʼi-n</ta>
            <ta e="T96" id="Seg_3857" s="T95">kalʼe-lʼlʼe</ta>
            <ta e="T97" id="Seg_3858" s="T96">čaːʒɨ-za-ttə</ta>
            <ta e="T98" id="Seg_3859" s="T97">qwačɨ-n</ta>
            <ta e="T99" id="Seg_3860" s="T98">kraj-ɣɨn</ta>
            <ta e="T100" id="Seg_3861" s="T99">po-la</ta>
            <ta e="T101" id="Seg_3862" s="T100">madʼ-i-la</ta>
            <ta e="T102" id="Seg_3863" s="T101">krugom</ta>
            <ta e="T103" id="Seg_3864" s="T102">sər</ta>
            <ta e="T104" id="Seg_3865" s="T103">ippɨ-tda</ta>
            <ta e="T105" id="Seg_3866" s="T104">man</ta>
            <ta e="T106" id="Seg_3867" s="T105">pois-qɨn</ta>
            <ta e="T107" id="Seg_3868" s="T106">ass</ta>
            <ta e="T108" id="Seg_3869" s="T107">warɨ-pɨ-za-n</ta>
            <ta e="T109" id="Seg_3870" s="T108">qɨːba-n</ta>
            <ta e="T110" id="Seg_3871" s="T109">kogɨr-ɨ-t</ta>
            <ta e="T111" id="Seg_3872" s="T110">ugon</ta>
            <ta e="T112" id="Seg_3873" s="T111">man</ta>
            <ta e="T113" id="Seg_3874" s="T112">ass</ta>
            <ta e="T114" id="Seg_3875" s="T113">paldü-ku-za-n</ta>
            <ta e="T115" id="Seg_3876" s="T114">poez-la-ze</ta>
            <ta e="T116" id="Seg_3877" s="T115">man</ta>
            <ta e="T117" id="Seg_3878" s="T116">ela-n</ta>
            <ta e="T118" id="Seg_3879" s="T117">nä-j-ɣun-nan</ta>
            <ta e="T119" id="Seg_3880" s="T118">mäɣuntə</ta>
            <ta e="T120" id="Seg_3881" s="T119">na</ta>
            <ta e="T121" id="Seg_3882" s="T120">tü-kku-tda</ta>
            <ta e="T122" id="Seg_3883" s="T121">süsöga</ta>
            <ta e="T123" id="Seg_3884" s="T122">se-la-m</ta>
            <ta e="T124" id="Seg_3885" s="T123">nagɨr-lʼe</ta>
            <ta e="T125" id="Seg_3886" s="T124">taːd-ə-r-ɨ-s-t</ta>
            <ta e="T126" id="Seg_3887" s="T125">qwatčɨ-ze</ta>
            <ta e="T127" id="Seg_3888" s="T126">nä-j-ɣum</ta>
            <ta e="T128" id="Seg_3889" s="T127">na</ta>
            <ta e="T129" id="Seg_3890" s="T128">tü-kkɨ-t</ta>
            <ta e="T130" id="Seg_3891" s="T129">da</ta>
            <ta e="T131" id="Seg_3892" s="T130">Satdijedo-tdə</ta>
            <ta e="T132" id="Seg_3893" s="T131">süsöɣə</ta>
            <ta e="T133" id="Seg_3894" s="T132">äʒ-la-m</ta>
            <ta e="T134" id="Seg_3895" s="T133">nagɨr-le</ta>
            <ta e="T135" id="Seg_3896" s="T134">taːd-ɨ-r-ɨ-s-tə</ta>
            <ta e="T136" id="Seg_3897" s="T135">tekga</ta>
            <ta e="T137" id="Seg_3898" s="T136">balʼdʼu-ku-s</ta>
            <ta e="T138" id="Seg_3899" s="T137">ta-zʼe</ta>
            <ta e="T139" id="Seg_3900" s="T138">qɨːdan</ta>
            <ta e="T140" id="Seg_3901" s="T139">nagɨr-ču-ku-s</ta>
            <ta e="T141" id="Seg_3902" s="T140">man</ta>
            <ta e="T142" id="Seg_3903" s="T141">na</ta>
            <ta e="T143" id="Seg_3904" s="T142">nä-j-ɣun-nan</ta>
            <ta e="T144" id="Seg_3905" s="T143">warka-n</ta>
            <ta e="T145" id="Seg_3906" s="T144">Nowosibʼirsk-a-n</ta>
            <ta e="T146" id="Seg_3907" s="T145">täb-ɨ-n</ta>
            <ta e="T147" id="Seg_3908" s="T146">nim-də</ta>
            <ta e="T148" id="Seg_3909" s="T147">Anglʼina</ta>
            <ta e="T149" id="Seg_3910" s="T148">Iwanɨwna</ta>
            <ta e="T150" id="Seg_3911" s="T149">man</ta>
            <ta e="T151" id="Seg_3912" s="T150">teb-nan</ta>
            <ta e="T152" id="Seg_3913" s="T151">warka-n</ta>
            <ta e="T153" id="Seg_3914" s="T152">mazɨm</ta>
            <ta e="T154" id="Seg_3915" s="T153">warɨ-n</ta>
            <ta e="T155" id="Seg_3916" s="T154">soːn</ta>
            <ta e="T156" id="Seg_3917" s="T155">jas</ta>
            <ta e="T157" id="Seg_3918" s="T156">qwɛdɨ-pɨ-qu-n</ta>
            <ta e="T158" id="Seg_3919" s="T157">man</ta>
            <ta e="T159" id="Seg_3920" s="T158">oːnän</ta>
            <ta e="T160" id="Seg_3921" s="T159">me-lʼe</ta>
            <ta e="T161" id="Seg_3922" s="T160">taːd-ə-r-ə-ko-u</ta>
            <ta e="T162" id="Seg_3923" s="T161">qai-m</ta>
            <ta e="T163" id="Seg_3924" s="T162">kɨga-n</ta>
            <ta e="T164" id="Seg_3925" s="T163">teb-nan</ta>
            <ta e="T165" id="Seg_3926" s="T164">nagur</ta>
            <ta e="T166" id="Seg_3927" s="T165">sütʼdʼe</ta>
            <ta e="T167" id="Seg_3928" s="T166">maːt-tə</ta>
            <ta e="T168" id="Seg_3929" s="T167">maːd-ɨ-n</ta>
            <ta e="T169" id="Seg_3930" s="T168">sütʼdʼi-la</ta>
            <ta e="T170" id="Seg_3931" s="T169">warɣɨ</ta>
            <ta e="T171" id="Seg_3932" s="T170">maːt</ta>
            <ta e="T172" id="Seg_3933" s="T171">pötpa</ta>
            <ta e="T175" id="Seg_3934" s="T174">üt</ta>
            <ta e="T176" id="Seg_3935" s="T175">maːt-qɨn</ta>
            <ta e="T177" id="Seg_3936" s="T176">je-n</ta>
            <ta e="T178" id="Seg_3937" s="T177">pärčɨ-tdə</ta>
            <ta e="T179" id="Seg_3938" s="T178">üt</ta>
            <ta e="T180" id="Seg_3939" s="T179">i</ta>
            <ta e="T181" id="Seg_3940" s="T180">qannə-bädi</ta>
            <ta e="T182" id="Seg_3941" s="T181">üt</ta>
            <ta e="T183" id="Seg_3942" s="T182">üt</ta>
            <ta e="T184" id="Seg_3943" s="T183">wes</ta>
            <ta e="T185" id="Seg_3944" s="T184">maːt-qɨn</ta>
            <ta e="T186" id="Seg_3945" s="T185">je-wa-t</ta>
            <ta e="T187" id="Seg_3946" s="T186">okkɨr</ta>
            <ta e="T188" id="Seg_3947" s="T187">sütʼdʼä-ɣɨn</ta>
            <ta e="T189" id="Seg_3948" s="T188">uːr-go-ftə</ta>
            <ta e="T190" id="Seg_3949" s="T189">natʼe-n</ta>
            <ta e="T191" id="Seg_3950" s="T190">pärčɨ-d</ta>
            <ta e="T192" id="Seg_3951" s="T191">üt</ta>
            <ta e="T193" id="Seg_3952" s="T192">i</ta>
            <ta e="T194" id="Seg_3953" s="T193">qannä-bädi</ta>
            <ta e="T195" id="Seg_3954" s="T194">üt</ta>
            <ta e="T196" id="Seg_3955" s="T195">mat-qɨn</ta>
            <ta e="T197" id="Seg_3956" s="T196">tüt-pu-ko-wtə</ta>
            <ta e="T198" id="Seg_3957" s="T197">i</ta>
            <ta e="T199" id="Seg_3958" s="T198">küze-pu-ko-wtə</ta>
            <ta e="T200" id="Seg_3959" s="T199">wes</ta>
            <ta e="T201" id="Seg_3960" s="T200">müzulʼǯɨ-lʼe</ta>
            <ta e="T202" id="Seg_3961" s="T201">qwatdə-qu-t</ta>
            <ta e="T203" id="Seg_3962" s="T202">i</ta>
            <ta e="T204" id="Seg_3963" s="T203">qaj</ta>
            <ta e="T205" id="Seg_3964" s="T204">aptä-näj</ta>
            <ta e="T206" id="Seg_3965" s="T205">tʼäkku</ta>
            <ta e="T207" id="Seg_3966" s="T206">cɨpočka-m</ta>
            <ta e="T208" id="Seg_3967" s="T207">nameʒal-ǯa-l</ta>
            <ta e="T209" id="Seg_3968" s="T208">üt</ta>
            <ta e="T210" id="Seg_3969" s="T209">šoɣər-lʼe</ta>
            <ta e="T211" id="Seg_3970" s="T210">qalu-ku-n</ta>
            <ta e="T212" id="Seg_3971" s="T211">wes</ta>
            <ta e="T213" id="Seg_3972" s="T212">mezulʼǯu-lʼe</ta>
            <ta e="T214" id="Seg_3973" s="T213">meʒal-gu-t</ta>
            <ta e="T215" id="Seg_3974" s="T214">maːd-la</ta>
            <ta e="T216" id="Seg_3975" s="T215">tɨtdɨ-n</ta>
            <ta e="T217" id="Seg_3976" s="T216">iːbɨgaj</ta>
            <ta e="T218" id="Seg_3977" s="T217">teːttə</ta>
            <ta e="T219" id="Seg_3978" s="T218">ätaš</ta>
            <ta e="T220" id="Seg_3979" s="T219">maːd-la</ta>
            <ta e="T221" id="Seg_3980" s="T220">wes</ta>
            <ta e="T222" id="Seg_3981" s="T221">okkɨr</ta>
            <ta e="T223" id="Seg_3982" s="T222">saj</ta>
            <ta e="T224" id="Seg_3983" s="T223">maːd-la</ta>
            <ta e="T225" id="Seg_3984" s="T224">maːd-la</ta>
            <ta e="T226" id="Seg_3985" s="T225">tüpba</ta>
            <ta e="T227" id="Seg_3986" s="T226">ass</ta>
            <ta e="T228" id="Seg_3987" s="T227">qutdoqɨn</ta>
            <ta e="T229" id="Seg_3988" s="T228">po-la</ta>
            <ta e="T230" id="Seg_3989" s="T229">kə-la</ta>
            <ta e="T231" id="Seg_3990" s="T230">üdəpiː-la</ta>
            <ta e="T232" id="Seg_3991" s="T231">jolka-la</ta>
            <ta e="T233" id="Seg_3992" s="T232">sosna-la</ta>
            <ta e="T234" id="Seg_3993" s="T233">tʼeu-n-po-la</ta>
            <ta e="T235" id="Seg_3994" s="T234">pi-la</ta>
            <ta e="T236" id="Seg_3995" s="T235">tautʼe-n</ta>
            <ta e="T237" id="Seg_3996" s="T236">taɣ-ɨ-n</ta>
            <ta e="T238" id="Seg_3997" s="T237">koːcʼi-n</ta>
            <ta e="T239" id="Seg_3998" s="T238">krɨbɨ-la</ta>
            <ta e="T240" id="Seg_3999" s="T239">tʼelɨm-ku-n</ta>
            <ta e="T241" id="Seg_4000" s="T240">me</ta>
            <ta e="T242" id="Seg_4001" s="T241">taɣ-ɨ-n</ta>
            <ta e="T243" id="Seg_4002" s="T242">krɨbɨ</ta>
            <ta e="T244" id="Seg_4003" s="T243">tak-t-ätǯu-tu</ta>
            <ta e="T245" id="Seg_4004" s="T244">üdɨn</ta>
            <ta e="T246" id="Seg_4005" s="T245">kocʼi-n</ta>
            <ta e="T247" id="Seg_4006" s="T246">je-q-wa-ttə</ta>
            <ta e="T248" id="Seg_4007" s="T247">swetoɣ-ɨ-la</ta>
            <ta e="T249" id="Seg_4008" s="T248">qaː-n</ta>
            <ta e="T250" id="Seg_4009" s="T249">tolʼdʼ-i-zʼe</ta>
            <ta e="T251" id="Seg_4010" s="T250">paldʼä-ttə</ta>
            <ta e="T252" id="Seg_4011" s="T251">qɨbanʼäʒa-la</ta>
            <ta e="T253" id="Seg_4012" s="T252">salaska-ze</ta>
            <ta e="T254" id="Seg_4013" s="T253">nez-a-r-na-ttə</ta>
            <ta e="T255" id="Seg_4014" s="T254">aran</ta>
            <ta e="T256" id="Seg_4015" s="T255">po-n</ta>
            <ta e="T257" id="Seg_4016" s="T256">tʼäb-la</ta>
            <ta e="T258" id="Seg_4017" s="T257">wes</ta>
            <ta e="T259" id="Seg_4018" s="T258">šolta-na</ta>
            <ta e="T260" id="Seg_4019" s="T259">azö-lʼ-eǯa-ttə</ta>
            <ta e="T261" id="Seg_4020" s="T260">qusaqɨn</ta>
            <ta e="T262" id="Seg_4021" s="T261">me</ta>
            <ta e="T263" id="Seg_4022" s="T262">tü-za-wtə</ta>
            <ta e="T264" id="Seg_4023" s="T263">Nowosibirsk-a-t</ta>
            <ta e="T265" id="Seg_4024" s="T264">seqɨ-za-wtə</ta>
            <ta e="T266" id="Seg_4025" s="T265">Nowosibirsk-a-ɣɨn</ta>
            <ta e="T267" id="Seg_4026" s="T266">wakzal-ɣɨn</ta>
            <ta e="T268" id="Seg_4027" s="T267">innä-j</ta>
            <ta e="T269" id="Seg_4028" s="T268">ətaš-qɨn</ta>
            <ta e="T270" id="Seg_4029" s="T269">seqɨ-za-wtə</ta>
            <ta e="T271" id="Seg_4030" s="T270">sobla-mtätte</ta>
            <ta e="T272" id="Seg_4031" s="T271">ätaš-xɨn</ta>
            <ta e="T273" id="Seg_4032" s="T272">qotdɨ-za-wtə</ta>
            <ta e="T274" id="Seg_4033" s="T273">mʼäɣuntu</ta>
            <ta e="T275" id="Seg_4034" s="T274">wes</ta>
            <ta e="T276" id="Seg_4035" s="T275">me-wa-ttə</ta>
            <ta e="T277" id="Seg_4036" s="T276">matraz-la-m</ta>
            <ta e="T278" id="Seg_4037" s="T277">adʼejal-la-m</ta>
            <ta e="T279" id="Seg_4038" s="T278">paduška-la-m</ta>
            <ta e="T280" id="Seg_4039" s="T279">qotdə-za-wtə</ta>
            <ta e="T281" id="Seg_4040" s="T280">soːdʼiga-n</ta>
            <ta e="T282" id="Seg_4041" s="T281">qu-la</ta>
            <ta e="T283" id="Seg_4042" s="T282">koːcʼi-n</ta>
            <ta e="T284" id="Seg_4043" s="T283">je-za-ttə</ta>
            <ta e="T285" id="Seg_4044" s="T284">iːbɨɣaj</ta>
            <ta e="T286" id="Seg_4045" s="T285">wakzal</ta>
            <ta e="T287" id="Seg_4046" s="T286">natʼe-n</ta>
            <ta e="T288" id="Seg_4047" s="T287">maːɣɨlǯə-nnaš</ta>
            <ta e="T289" id="Seg_4048" s="T288">jaš</ta>
            <ta e="T290" id="Seg_4049" s="T289">čannä-nnaš</ta>
            <ta e="T291" id="Seg_4050" s="T290">qarʼe-mɨ-ɣɨn</ta>
            <ta e="T292" id="Seg_4051" s="T291">qwan-na-wtə</ta>
            <ta e="T293" id="Seg_4052" s="T292">stalowaj-tə</ta>
            <ta e="T294" id="Seg_4053" s="T293">teb-la</ta>
            <ta e="T295" id="Seg_4054" s="T294">ket-qwa-ttə</ta>
            <ta e="T296" id="Seg_4055" s="T295">restoran</ta>
            <ta e="T297" id="Seg_4056" s="T296">natʼe-n</ta>
            <ta e="T298" id="Seg_4057" s="T297">soːdiga-n</ta>
            <ta e="T299" id="Seg_4058" s="T298">je-n</ta>
            <ta e="T300" id="Seg_4059" s="T299">raj-i-n</ta>
            <ta e="T301" id="Seg_4060" s="T300">sütdə</ta>
            <ta e="T302" id="Seg_4061" s="T301">natʼe-n</ta>
            <ta e="T303" id="Seg_4062" s="T302">i</ta>
            <ta e="T304" id="Seg_4063" s="T303">au-r-sa-wtə</ta>
            <ta e="T305" id="Seg_4064" s="T304">me-nan</ta>
            <ta e="T306" id="Seg_4065" s="T305">more</ta>
            <ta e="T307" id="Seg_4066" s="T306">čaʒek-a-n</ta>
            <ta e="T308" id="Seg_4067" s="T307">je-n</ta>
            <ta e="T309" id="Seg_4068" s="T308">natʼe-n</ta>
            <ta e="T310" id="Seg_4069" s="T309">qu-la</ta>
            <ta e="T311" id="Seg_4070" s="T310">qwɛlɨ-j-nʼä-ttə</ta>
            <ta e="T312" id="Seg_4071" s="T311">sɨbɨq-se</ta>
            <ta e="T313" id="Seg_4072" s="T312">nɨrsa-la</ta>
            <ta e="T314" id="Seg_4073" s="T313">qwat-qu-da-ttə</ta>
            <ta e="T315" id="Seg_4074" s="T314">i</ta>
            <ta e="T316" id="Seg_4075" s="T315">pʼedʼe-la</ta>
            <ta e="T317" id="Seg_4076" s="T316">qɨba-nädäk-qa</ta>
            <ta e="T318" id="Seg_4077" s="T317">xazʼäjka-n</ta>
            <ta e="T319" id="Seg_4078" s="T318">nä-ga-t</ta>
            <ta e="T320" id="Seg_4079" s="T319">tät-ku-s-tə</ta>
            <ta e="T321" id="Seg_4080" s="T320">nɨrsa-n</ta>
            <ta e="T322" id="Seg_4081" s="T321">tas-sɨ-t</ta>
            <ta e="T323" id="Seg_4082" s="T322">nɨrsa</ta>
            <ta e="T324" id="Seg_4083" s="T323">qattä-dʼi-pba</ta>
            <ta e="T325" id="Seg_4084" s="T324">koška</ta>
            <ta e="T326" id="Seg_4085" s="T325">täb-ɨ-m</ta>
            <ta e="T327" id="Seg_4086" s="T326">ap-sɨ-t</ta>
            <ta e="T328" id="Seg_4087" s="T327">man-nan</ta>
            <ta e="T329" id="Seg_4088" s="T328">sət</ta>
            <ta e="T330" id="Seg_4089" s="T329">padruga-u</ta>
            <ta e="T331" id="Seg_4090" s="T330">je-wa</ta>
            <ta e="T332" id="Seg_4091" s="T331">man</ta>
            <ta e="T333" id="Seg_4092" s="T332">teb-la-nä</ta>
            <ta e="T334" id="Seg_4093" s="T333">palʼdʼü-q-wa-n</ta>
            <ta e="T335" id="Seg_4094" s="T334">i</ta>
            <ta e="T336" id="Seg_4095" s="T335">teb-la</ta>
            <ta e="T337" id="Seg_4096" s="T336">mekka</ta>
            <ta e="T338" id="Seg_4097" s="T337">palʼdʼü-q-wa-ttə</ta>
            <ta e="T339" id="Seg_4098" s="T338">man</ta>
            <ta e="T340" id="Seg_4099" s="T339">sorm-eǯa-n</ta>
            <ta e="T341" id="Seg_4100" s="T340">poqqɨ-la-j</ta>
            <ta e="T342" id="Seg_4101" s="T341">ola</ta>
            <ta e="T343" id="Seg_4102" s="T342">amdɨ-gu</ta>
            <ta e="T344" id="Seg_4103" s="T343">ass</ta>
            <ta e="T345" id="Seg_4104" s="T344">kɨga-n</ta>
            <ta e="T346" id="Seg_4105" s="T345">a</ta>
            <ta e="T347" id="Seg_4106" s="T346">onän</ta>
            <ta e="T348" id="Seg_4107" s="T347">kɨdan</ta>
            <ta e="T349" id="Seg_4108" s="T348">qojmu-tču-ka-n</ta>
            <ta e="T350" id="Seg_4109" s="T349">man</ta>
            <ta e="T351" id="Seg_4110" s="T350">üdɨn</ta>
            <ta e="T352" id="Seg_4111" s="T351">Satdijedo-t</ta>
            <ta e="T353" id="Seg_4112" s="T352">tü-tǯa-n</ta>
            <ta e="T354" id="Seg_4113" s="T353">qwälɨ-s-ku</ta>
            <ta e="T355" id="Seg_4114" s="T354">čobər-ɨ-m</ta>
            <ta e="T356" id="Seg_4115" s="T355">qwad-ə-gu</ta>
            <ta e="T357" id="Seg_4116" s="T356">qwače</ta>
            <ta e="T358" id="Seg_4117" s="T357">iːbɨga-n</ta>
            <ta e="T359" id="Seg_4118" s="T358">je-n</ta>
            <ta e="T360" id="Seg_4119" s="T359">man</ta>
            <ta e="T361" id="Seg_4120" s="T360">na</ta>
            <ta e="T362" id="Seg_4121" s="T361">tü-nɨ-ǯa-n</ta>
            <ta e="T363" id="Seg_4122" s="T362">i</ta>
            <ta e="T364" id="Seg_4123" s="T363">tekga</ta>
            <ta e="T365" id="Seg_4124" s="T364">aːdu-lǯ-eǯa-u</ta>
            <ta e="T366" id="Seg_4125" s="T365">qwačɨ-n</ta>
            <ta e="T367" id="Seg_4126" s="T366">tekga</ta>
            <ta e="T368" id="Seg_4127" s="T367">na</ta>
            <ta e="T369" id="Seg_4128" s="T368">qwatčɨ-n</ta>
            <ta e="T370" id="Seg_4129" s="T369">tat-ča-u</ta>
            <ta e="T371" id="Seg_4130" s="T370">tat</ta>
            <ta e="T372" id="Seg_4131" s="T371">mannä-m-eːǯa-l</ta>
            <ta e="T373" id="Seg_4132" s="T372">tawtʼe-n</ta>
            <ta e="T374" id="Seg_4133" s="T373">wsʼäkij</ta>
            <ta e="T375" id="Seg_4134" s="T374">mašina-la-zʼe</ta>
            <ta e="T376" id="Seg_4135" s="T375">palʼdʼi-za-n</ta>
            <ta e="T379" id="Seg_4136" s="T378">larɨ-pɨ-za-n</ta>
            <ta e="T380" id="Seg_4137" s="T379">mazɨm</ta>
            <ta e="T381" id="Seg_4138" s="T380">Valodʼä</ta>
            <ta e="T382" id="Seg_4139" s="T381">qwačo-ɣɨn</ta>
            <ta e="T383" id="Seg_4140" s="T382">warɨ-s</ta>
            <ta e="T384" id="Seg_4141" s="T383">wes</ta>
            <ta e="T385" id="Seg_4142" s="T384">aːdu-lǯɨ-ɣɨ-sɨ-t</ta>
            <ta e="T386" id="Seg_4143" s="T385">onät</ta>
            <ta e="T387" id="Seg_4144" s="T386">də</ta>
            <ta e="T388" id="Seg_4145" s="T387">qu</ta>
            <ta e="T389" id="Seg_4146" s="T388">qwan-naš</ta>
            <ta e="T390" id="Seg_4147" s="T389">maɣɨlʼǯə-nnaš</ta>
            <ta e="T391" id="Seg_4148" s="T390">man</ta>
            <ta e="T392" id="Seg_4149" s="T391">küssu-ku-m-na-n</ta>
            <ta e="T393" id="Seg_4150" s="T392">tʼära-n</ta>
            <ta e="T394" id="Seg_4151" s="T393">küzu-gu</ta>
            <ta e="T395" id="Seg_4152" s="T394">nado</ta>
            <ta e="T396" id="Seg_4153" s="T395">a</ta>
            <ta e="T397" id="Seg_4154" s="T396">Valodʼa</ta>
            <ta e="T398" id="Seg_4155" s="T397">mekga</ta>
            <ta e="T399" id="Seg_4156" s="T398">tʼärɨ-n</ta>
            <ta e="T400" id="Seg_4157" s="T399">qu</ta>
            <ta e="T401" id="Seg_4158" s="T400">küze-naš</ta>
            <ta e="T402" id="Seg_4159" s="T401">küzə-pbədi</ta>
            <ta e="T403" id="Seg_4160" s="T402">mɨ</ta>
            <ta e="T404" id="Seg_4161" s="T403">tʼäkgu</ta>
            <ta e="T405" id="Seg_4162" s="T404">küzə-pbədi</ta>
            <ta e="T406" id="Seg_4163" s="T405">mɨ-la</ta>
            <ta e="T407" id="Seg_4164" s="T406">wes</ta>
            <ta e="T408" id="Seg_4165" s="T407">mad-ɨ-n</ta>
            <ta e="T409" id="Seg_4166" s="T408">sütʼdʼä-ɣɨn</ta>
            <ta e="T410" id="Seg_4167" s="T409">je-wa-ttə</ta>
            <ta e="T411" id="Seg_4168" s="T410">pazar-ɣɨn</ta>
            <ta e="T412" id="Seg_4169" s="T411">je-za-j</ta>
            <ta e="T413" id="Seg_4170" s="T412">täp</ta>
            <ta e="T414" id="Seg_4171" s="T413">mekga</ta>
            <ta e="T415" id="Seg_4172" s="T414">tʼärɨ-n</ta>
            <ta e="T416" id="Seg_4173" s="T415">natʼe-n-t</ta>
            <ta e="T417" id="Seg_4174" s="T416">qaj-no</ta>
            <ta e="T418" id="Seg_4175" s="T417">as</ta>
            <ta e="T419" id="Seg_4176" s="T418">küzə-za-t</ta>
            <ta e="T420" id="Seg_4177" s="T419">a</ta>
            <ta e="T421" id="Seg_4178" s="T420">man</ta>
            <ta e="T422" id="Seg_4179" s="T421">ass</ta>
            <ta e="T423" id="Seg_4180" s="T422">küzɨ-zaː-n</ta>
            <ta e="T424" id="Seg_4181" s="T423">tak</ta>
            <ta e="T425" id="Seg_4182" s="T424">man</ta>
            <ta e="T426" id="Seg_4183" s="T425">tʼärpi-pɨ-za-u</ta>
            <ta e="T427" id="Seg_4184" s="T426">qusakɨn</ta>
            <ta e="T428" id="Seg_4185" s="T427">mašina-m</ta>
            <ta e="T429" id="Seg_4186" s="T428">aːdɨ-za-j</ta>
            <ta e="T430" id="Seg_4187" s="T429">man</ta>
            <ta e="T431" id="Seg_4188" s="T430">okkɨr</ta>
            <ta e="T432" id="Seg_4189" s="T431">nä-j-ɣum-nä</ta>
            <ta e="T433" id="Seg_4190" s="T432">soɣutdʼa-n</ta>
            <ta e="T434" id="Seg_4191" s="T433">qutʼen</ta>
            <ta e="T435" id="Seg_4192" s="T434">küzə-pbədi</ta>
            <ta e="T436" id="Seg_4193" s="T435">maːt</ta>
            <ta e="T437" id="Seg_4194" s="T436">tep</ta>
            <ta e="T438" id="Seg_4195" s="T437">mekga</ta>
            <ta e="T439" id="Seg_4196" s="T438">aːdu-lǯɨ-t</ta>
            <ta e="T440" id="Seg_4197" s="T439">tutʼetdo</ta>
            <ta e="T441" id="Seg_4198" s="T440">man</ta>
            <ta e="T442" id="Seg_4199" s="T441">kur-on-na-n</ta>
            <ta e="T443" id="Seg_4200" s="T442">i</ta>
            <ta e="T444" id="Seg_4201" s="T443">küza-n</ta>
            <ta e="T445" id="Seg_4202" s="T444">man</ta>
            <ta e="T446" id="Seg_4203" s="T445">täjerbɨ-za-n</ta>
            <ta e="T447" id="Seg_4204" s="T446">küze-u</ta>
            <ta e="T448" id="Seg_4205" s="T447">wal</ta>
            <ta e="T449" id="Seg_4206" s="T448">waǯə-n</ta>
            <ta e="T450" id="Seg_4207" s="T449">küza-n</ta>
            <ta e="T451" id="Seg_4208" s="T450">i</ta>
            <ta e="T452" id="Seg_4209" s="T451">soːn</ta>
            <ta e="T453" id="Seg_4210" s="T452">azu-n</ta>
            <ta e="T454" id="Seg_4211" s="T453">magazin-la</ta>
            <ta e="T455" id="Seg_4212" s="T454">ser-ka-j</ta>
            <ta e="T456" id="Seg_4213" s="T455">iːbɨgaj</ta>
            <ta e="T457" id="Seg_4214" s="T456">magazil-la</ta>
            <ta e="T458" id="Seg_4215" s="T457">natʼe-n</ta>
            <ta e="T459" id="Seg_4216" s="T458">maɣulǯe-nnaš</ta>
            <ta e="T460" id="Seg_4217" s="T459">man</ta>
            <ta e="T461" id="Seg_4218" s="T460">nagɨr-ɨ-m</ta>
            <ta e="T462" id="Seg_4219" s="T461">ass</ta>
            <ta e="T463" id="Seg_4220" s="T462">tuno-u</ta>
            <ta e="T464" id="Seg_4221" s="T463">oːnän</ta>
            <ta e="T465" id="Seg_4222" s="T464">ass</ta>
            <ta e="T466" id="Seg_4223" s="T465">paldʼi-q-wa-n</ta>
            <ta e="T467" id="Seg_4224" s="T466">qwatčo-ɣɨn</ta>
            <ta e="T468" id="Seg_4225" s="T467">ato</ta>
            <ta e="T469" id="Seg_4226" s="T468">maɣɨlǯə-ǯa-n</ta>
            <ta e="T470" id="Seg_4227" s="T469">man</ta>
            <ta e="T471" id="Seg_4228" s="T470">täpär</ta>
            <ta e="T472" id="Seg_4229" s="T471">mašina-la-ze</ta>
            <ta e="T473" id="Seg_4230" s="T472">paldʼä-n</ta>
            <ta e="T474" id="Seg_4231" s="T473">ilɨ-za-n</ta>
            <ta e="T475" id="Seg_4232" s="T474">qwatčo-ɣɨn</ta>
            <ta e="T476" id="Seg_4233" s="T475">ČʼornajaRečʼka-ɣɨn</ta>
            <ta e="T477" id="Seg_4234" s="T476">i</ta>
            <ta e="T478" id="Seg_4235" s="T477">Taxtamɨšowa-ɣɨn</ta>
            <ta e="T479" id="Seg_4236" s="T478">ilɨ-za-n</ta>
            <ta e="T480" id="Seg_4237" s="T479">Dunʼa</ta>
            <ta e="T481" id="Seg_4238" s="T480">tʼü-s</ta>
            <ta e="T482" id="Seg_4239" s="T481">ČʼornajaRʼečʼka-t</ta>
            <ta e="T483" id="Seg_4240" s="T482">iː-tdɨ-zʼe</ta>
            <ta e="T484" id="Seg_4241" s="T483">täb-ɨ-staɣə</ta>
            <ta e="T485" id="Seg_4242" s="T484">warka-ɣə</ta>
            <ta e="T486" id="Seg_4243" s="T485">Mikalaj</ta>
            <ta e="T487" id="Seg_4244" s="T486">Lazarɨčʼ-i-n</ta>
            <ta e="T488" id="Seg_4245" s="T487">paja-ga-t</ta>
            <ta e="T489" id="Seg_4246" s="T488">qu-a</ta>
            <ta e="T490" id="Seg_4247" s="T489">ČʼornajaRʼečʼka-ɣɨn</ta>
            <ta e="T491" id="Seg_4248" s="T490">warkɨ-ss</ta>
            <ta e="T492" id="Seg_4249" s="T491">nä-tda-nan</ta>
            <ta e="T493" id="Seg_4250" s="T492">a</ta>
            <ta e="T494" id="Seg_4251" s="T493">man</ta>
            <ta e="T495" id="Seg_4252" s="T494">qwa-na-n</ta>
            <ta e="T496" id="Seg_4253" s="T495">qwači-la-m</ta>
            <ta e="T497" id="Seg_4254" s="T496">mannɨ-pu-gu</ta>
            <ta e="T498" id="Seg_4255" s="T497">Marina</ta>
            <ta e="T499" id="Seg_4256" s="T498">Pawlowna</ta>
            <ta e="T500" id="Seg_4257" s="T499">qutdär</ta>
            <ta e="T501" id="Seg_4258" s="T500">man</ta>
            <ta e="T502" id="Seg_4259" s="T501">nagɨr-moː</ta>
            <ta e="T503" id="Seg_4260" s="T502">tü-tǯɨ-n</ta>
            <ta e="T504" id="Seg_4261" s="T503">i</ta>
            <ta e="T505" id="Seg_4262" s="T504">mekga</ta>
            <ta e="T506" id="Seg_4263" s="T505">nagɨr-ɨ-m</ta>
            <ta e="T507" id="Seg_4264" s="T506">srazu</ta>
            <ta e="T508" id="Seg_4265" s="T507">nagɨr-tə</ta>
            <ta e="T509" id="Seg_4266" s="T508">man</ta>
            <ta e="T510" id="Seg_4267" s="T509">aːde-lǯe-ǯa-n</ta>
            <ta e="T511" id="Seg_4268" s="T510">wes</ta>
            <ta e="T512" id="Seg_4269" s="T511">nagɨr-tə</ta>
            <ta e="T513" id="Seg_4270" s="T512">qutdär</ta>
            <ta e="T514" id="Seg_4271" s="T513">man</ta>
            <ta e="T515" id="Seg_4272" s="T514">qu-la-u</ta>
            <ta e="T516" id="Seg_4273" s="T515">warka-ttə</ta>
            <ta e="T517" id="Seg_4274" s="T516">Kolʼä-n</ta>
            <ta e="T518" id="Seg_4275" s="T517">tʼät</ta>
            <ta e="T519" id="Seg_4276" s="T518">qaj-j-a-mɨ-m</ta>
            <ta e="T520" id="Seg_4277" s="T519">üːtdə-tʼü-ka-l</ta>
            <ta e="T521" id="Seg_4278" s="T520">alʼ</ta>
            <ta e="T522" id="Seg_4279" s="T521">ass</ta>
            <ta e="T523" id="Seg_4280" s="T522">wes</ta>
            <ta e="T524" id="Seg_4281" s="T523">mekga</ta>
            <ta e="T525" id="Seg_4282" s="T524">nagɨr-ɨ-t</ta>
            <ta e="T526" id="Seg_4283" s="T525">man</ta>
            <ta e="T527" id="Seg_4284" s="T526">putʼöm</ta>
            <ta e="T528" id="Seg_4285" s="T527">aːdɨ-lǯə-ǯa-n</ta>
            <ta e="T529" id="Seg_4286" s="T528">täɣɨtdɨltə</ta>
            <ta e="T530" id="Seg_4287" s="T529">nʼäj-zʼe</ta>
            <ta e="T531" id="Seg_4288" s="T530">me-q-wa-ttə</ta>
            <ta e="T532" id="Seg_4289" s="T531">alʼi</ta>
            <ta e="T533" id="Seg_4290" s="T532">moga-zʼe</ta>
            <ta e="T534" id="Seg_4291" s="T533">me-q-wa-ttə</ta>
            <ta e="T535" id="Seg_4292" s="T534">a</ta>
            <ta e="T536" id="Seg_4293" s="T535">magazʼin-ɣɨn</ta>
            <ta e="T537" id="Seg_4294" s="T536">qaj-la</ta>
            <ta e="T538" id="Seg_4295" s="T537">je-ta-ttə</ta>
            <ta e="T539" id="Seg_4296" s="T538">wes</ta>
            <ta e="T540" id="Seg_4297" s="T539">nagɨr-tə</ta>
            <ta e="T541" id="Seg_4298" s="T540">ämnä-l</ta>
            <ta e="T542" id="Seg_4299" s="T541">qaj</ta>
            <ta e="T543" id="Seg_4300" s="T542">iːlɨ-n</ta>
            <ta e="T544" id="Seg_4301" s="T543">alʼ</ta>
            <ta e="T545" id="Seg_4302" s="T544">ass</ta>
            <ta e="T546" id="Seg_4303" s="T545">tʼär-za-ttə</ta>
            <ta e="T547" id="Seg_4304" s="T546">to</ta>
            <ta e="T548" id="Seg_4305" s="T547">qwan-pa</ta>
            <ta e="T549" id="Seg_4306" s="T548">Sinka-n</ta>
            <ta e="T550" id="Seg_4307" s="T549">dʼät</ta>
            <ta e="T551" id="Seg_4308" s="T550">nagɨr-ət</ta>
            <ta e="T552" id="Seg_4309" s="T551">qutdär</ta>
            <ta e="T553" id="Seg_4310" s="T552">ilɨ-kqu-n</ta>
            <ta e="T554" id="Seg_4311" s="T553">warkɨ-qu-n</ta>
            <ta e="T555" id="Seg_4312" s="T554">man</ta>
            <ta e="T556" id="Seg_4313" s="T555">täb-nä</ta>
            <ta e="T557" id="Seg_4314" s="T556">nagɨr-ɨ-m</ta>
            <ta e="T558" id="Seg_4315" s="T557">nagur-za-n</ta>
            <ta e="T559" id="Seg_4316" s="T558">täp</ta>
            <ta e="T560" id="Seg_4317" s="T559">mekga</ta>
            <ta e="T561" id="Seg_4318" s="T560">jass</ta>
            <ta e="T562" id="Seg_4319" s="T561">nagur-gu-t</ta>
            <ta e="T563" id="Seg_4320" s="T562">tetʼa</ta>
            <ta e="T564" id="Seg_4321" s="T563">Marina</ta>
            <ta e="T565" id="Seg_4322" s="T564">perwɨj</ta>
            <ta e="T566" id="Seg_4323" s="T565">maj-zʼe</ta>
            <ta e="T567" id="Seg_4324" s="T566">praznik-ze</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_4325" s="T1">me</ta>
            <ta e="T3" id="Seg_4326" s="T2">taw-dʼel</ta>
            <ta e="T4" id="Seg_4327" s="T3">amdɨ-j</ta>
            <ta e="T5" id="Seg_4328" s="T4">Angelina</ta>
            <ta e="T6" id="Seg_4329" s="T5">Iwanɨwna-se</ta>
            <ta e="T7" id="Seg_4330" s="T6">kulubu-j</ta>
            <ta e="T8" id="Seg_4331" s="T7">tan</ta>
            <ta e="T9" id="Seg_4332" s="T8">tʼаːt</ta>
            <ta e="T10" id="Seg_4333" s="T9">kulubu-j</ta>
            <ta e="T11" id="Seg_4334" s="T10">qundar</ta>
            <ta e="T12" id="Seg_4335" s="T11">warkɨ-ntə</ta>
            <ta e="T13" id="Seg_4336" s="T12">qaj-la</ta>
            <ta e="T14" id="Seg_4337" s="T13">meː-ku-ntɨ-l</ta>
            <ta e="T15" id="Seg_4338" s="T14">man</ta>
            <ta e="T16" id="Seg_4339" s="T15">täp-nä</ta>
            <ta e="T17" id="Seg_4340" s="T16">ket-le</ta>
            <ta e="T18" id="Seg_4341" s="T17">tat-ɨ-r-nɨ-w</ta>
            <ta e="T19" id="Seg_4342" s="T18">tan</ta>
            <ta e="T20" id="Seg_4343" s="T19">tʼаːt</ta>
            <ta e="T21" id="Seg_4344" s="T20">man</ta>
            <ta e="T22" id="Seg_4345" s="T21">tʼärɨ-ŋ</ta>
            <ta e="T23" id="Seg_4346" s="T22">täp</ta>
            <ta e="T24" id="Seg_4347" s="T23">paja-m-mbɨ</ta>
            <ta e="T25" id="Seg_4348" s="T24">man</ta>
            <ta e="T26" id="Seg_4349" s="T25">täp-nä</ta>
            <ta e="T27" id="Seg_4350" s="T26">ket-sɨ-w</ta>
            <ta e="T28" id="Seg_4351" s="T27">tan</ta>
            <ta e="T29" id="Seg_4352" s="T28">mezuwi</ta>
            <ta e="T30" id="Seg_4353" s="T29">Мarʼija-se</ta>
            <ta e="T31" id="Seg_4354" s="T30">apsǝ-tɨ-sɨ-t</ta>
            <ta e="T32" id="Seg_4355" s="T31">me</ta>
            <ta e="T33" id="Seg_4356" s="T32">tan-se</ta>
            <ta e="T34" id="Seg_4357" s="T33">qundar</ta>
            <ta e="T35" id="Seg_4358" s="T34">öru-sɨ-un</ta>
            <ta e="T36" id="Seg_4359" s="T35">man</ta>
            <ta e="T37" id="Seg_4360" s="T36">täp-nä</ta>
            <ta e="T38" id="Seg_4361" s="T37">wesʼ</ta>
            <ta e="T39" id="Seg_4362" s="T38">ket-sɨ-w</ta>
            <ta e="T40" id="Seg_4363" s="T39">man</ta>
            <ta e="T41" id="Seg_4364" s="T40">kulubu-sɨ-ŋ</ta>
            <ta e="T42" id="Seg_4365" s="T41">tʼötka</ta>
            <ta e="T43" id="Seg_4366" s="T42">Marʼina</ta>
            <ta e="T44" id="Seg_4367" s="T43">nagər</ta>
            <ta e="T45" id="Seg_4368" s="T44">nagər-ču-enǯɨ-ŋ</ta>
            <ta e="T46" id="Seg_4369" s="T45">puskaj</ta>
            <ta e="T47" id="Seg_4370" s="T46">*toːqo-lǯi-mtə</ta>
            <ta e="T48" id="Seg_4371" s="T47">tʼolom</ta>
            <ta e="T49" id="Seg_4372" s="T48">Marʼina</ta>
            <ta e="T50" id="Seg_4373" s="T49">Pawlowna</ta>
            <ta e="T51" id="Seg_4374" s="T50">man</ta>
            <ta e="T52" id="Seg_4375" s="T51">tekka</ta>
            <ta e="T53" id="Seg_4376" s="T52">süsögi</ta>
            <ta e="T54" id="Seg_4377" s="T53">nagər</ta>
            <ta e="T55" id="Seg_4378" s="T54">nagər-ču-enǯɨ-ŋ</ta>
            <ta e="T56" id="Seg_4379" s="T55">qazaq-se-ŋ</ta>
            <ta e="T57" id="Seg_4380" s="T56">man</ta>
            <ta e="T58" id="Seg_4381" s="T57">asa</ta>
            <ta e="T59" id="Seg_4382" s="T58">tonu-ŋ</ta>
            <ta e="T60" id="Seg_4383" s="T59">man</ta>
            <ta e="T61" id="Seg_4384" s="T60">elɨ-ŋ</ta>
            <ta e="T62" id="Seg_4385" s="T61">teper</ta>
            <ta e="T63" id="Seg_4386" s="T62">qwačə-qɨn</ta>
            <ta e="T64" id="Seg_4387" s="T63">Nowosibirsk-ɨ-qɨn</ta>
            <ta e="T65" id="Seg_4388" s="T64">qwačə</ta>
            <ta e="T66" id="Seg_4389" s="T65">iːbəgaj</ta>
            <ta e="T67" id="Seg_4390" s="T66">soːdʼiga-lʼ</ta>
            <ta e="T68" id="Seg_4391" s="T67">qwačə-qɨntɨ</ta>
            <ta e="T69" id="Seg_4392" s="T68">Nowosibirsk-ɨ-ntə</ta>
            <ta e="T70" id="Seg_4393" s="T69">pois-se</ta>
            <ta e="T71" id="Seg_4394" s="T70">čaǯɨ-sɨ-ŋ</ta>
            <ta e="T72" id="Seg_4395" s="T71">pois-qɨn</ta>
            <ta e="T73" id="Seg_4396" s="T72">čaǯɨ-le</ta>
            <ta e="T74" id="Seg_4397" s="T73">kozɨr-ču-sɨ-un</ta>
            <ta e="T75" id="Seg_4398" s="T74">Tajoʒnɨj</ta>
            <ta e="T76" id="Seg_4399" s="T75">stancɨ-qɨn</ta>
            <ta e="T77" id="Seg_4400" s="T76">pois</ta>
            <ta e="T78" id="Seg_4401" s="T77">kundɨ-n</ta>
            <ta e="T79" id="Seg_4402" s="T78">nɨ-ku-sɨ</ta>
            <ta e="T80" id="Seg_4403" s="T79">man</ta>
            <ta e="T81" id="Seg_4404" s="T80">*mantɨ-mbɨ-sɨ-ŋ</ta>
            <ta e="T82" id="Seg_4405" s="T81">i</ta>
            <ta e="T83" id="Seg_4406" s="T82">Nikin-ɨ-m</ta>
            <ta e="T84" id="Seg_4407" s="T83">qaj-qɨn</ta>
            <ta e="T85" id="Seg_4408" s="T84">asa</ta>
            <ta e="T86" id="Seg_4409" s="T85">qo-nǯir-sɨ-w</ta>
            <ta e="T87" id="Seg_4410" s="T86">patom</ta>
            <ta e="T88" id="Seg_4411" s="T87">pois</ta>
            <ta e="T89" id="Seg_4412" s="T88">übɨ-n</ta>
            <ta e="T90" id="Seg_4413" s="T89">man</ta>
            <ta e="T91" id="Seg_4414" s="T90">qɨdan</ta>
            <ta e="T92" id="Seg_4415" s="T91">akoška-qɨn</ta>
            <ta e="T93" id="Seg_4416" s="T92">*mantɨ-mbɨ-sɨ-ŋ</ta>
            <ta e="T94" id="Seg_4417" s="T93">eːde-la</ta>
            <ta e="T95" id="Seg_4418" s="T94">koːci-ŋ</ta>
            <ta e="T96" id="Seg_4419" s="T95">qalɨ-le</ta>
            <ta e="T97" id="Seg_4420" s="T96">čaǯɨ-sɨ-tɨn</ta>
            <ta e="T98" id="Seg_4421" s="T97">qwačə-n</ta>
            <ta e="T99" id="Seg_4422" s="T98">kraj-qɨn</ta>
            <ta e="T100" id="Seg_4423" s="T99">po-la</ta>
            <ta e="T101" id="Seg_4424" s="T100">madʼ-ɨ-la</ta>
            <ta e="T102" id="Seg_4425" s="T101">krugom</ta>
            <ta e="T103" id="Seg_4426" s="T102">sər</ta>
            <ta e="T104" id="Seg_4427" s="T103">ippɨ-ntɨ</ta>
            <ta e="T105" id="Seg_4428" s="T104">man</ta>
            <ta e="T106" id="Seg_4429" s="T105">pois-qɨn</ta>
            <ta e="T107" id="Seg_4430" s="T106">asa</ta>
            <ta e="T108" id="Seg_4431" s="T107">*larɨ-mbɨ-sɨ-ŋ</ta>
            <ta e="T109" id="Seg_4432" s="T108">qɨba-ŋ</ta>
            <ta e="T110" id="Seg_4433" s="T109">qugur-ɨ-t</ta>
            <ta e="T111" id="Seg_4434" s="T110">ugon</ta>
            <ta e="T112" id="Seg_4435" s="T111">man</ta>
            <ta e="T113" id="Seg_4436" s="T112">asa</ta>
            <ta e="T114" id="Seg_4437" s="T113">palʼdʼi-ku-sɨ-ŋ</ta>
            <ta e="T115" id="Seg_4438" s="T114">pois-la-se</ta>
            <ta e="T116" id="Seg_4439" s="T115">man</ta>
            <ta e="T117" id="Seg_4440" s="T116">elɨ-ŋ</ta>
            <ta e="T118" id="Seg_4441" s="T117">ne-lʼ-qum-nan</ta>
            <ta e="T119" id="Seg_4442" s="T118">mäɣuntə</ta>
            <ta e="T120" id="Seg_4443" s="T119">na</ta>
            <ta e="T121" id="Seg_4444" s="T120">tüː-ku-ntɨ</ta>
            <ta e="T122" id="Seg_4445" s="T121">süsögi</ta>
            <ta e="T123" id="Seg_4446" s="T122">se-la-m</ta>
            <ta e="T124" id="Seg_4447" s="T123">nagər-le</ta>
            <ta e="T125" id="Seg_4448" s="T124">tat-ɨ-r-ɨ-sɨ-t</ta>
            <ta e="T126" id="Seg_4449" s="T125">qwačə-se</ta>
            <ta e="T127" id="Seg_4450" s="T126">ne-lʼ-qum</ta>
            <ta e="T128" id="Seg_4451" s="T127">na</ta>
            <ta e="T129" id="Seg_4452" s="T128">tüː-ku-ntɨ</ta>
            <ta e="T130" id="Seg_4453" s="T129">da</ta>
            <ta e="T131" id="Seg_4454" s="T130">Satdijedo-ntə</ta>
            <ta e="T132" id="Seg_4455" s="T131">süsögi</ta>
            <ta e="T133" id="Seg_4456" s="T132">əǯə-la-m</ta>
            <ta e="T134" id="Seg_4457" s="T133">nagər-le</ta>
            <ta e="T135" id="Seg_4458" s="T134">tat-ɨ-r-ɨ-sɨ-t</ta>
            <ta e="T136" id="Seg_4459" s="T135">tekka</ta>
            <ta e="T137" id="Seg_4460" s="T136">palʼdʼi-ku-sɨ</ta>
            <ta e="T138" id="Seg_4461" s="T137">tan-se</ta>
            <ta e="T139" id="Seg_4462" s="T138">qɨdan</ta>
            <ta e="T140" id="Seg_4463" s="T139">nagər-ču-ku-sɨ</ta>
            <ta e="T141" id="Seg_4464" s="T140">man</ta>
            <ta e="T142" id="Seg_4465" s="T141">na</ta>
            <ta e="T143" id="Seg_4466" s="T142">ne-lʼ-qum-nan</ta>
            <ta e="T144" id="Seg_4467" s="T143">warkɨ-ŋ</ta>
            <ta e="T145" id="Seg_4468" s="T144">Nowosibirsk-ɨ-n</ta>
            <ta e="T146" id="Seg_4469" s="T145">täp-ɨ-n</ta>
            <ta e="T147" id="Seg_4470" s="T146">nim-tə</ta>
            <ta e="T148" id="Seg_4471" s="T147">Angelina</ta>
            <ta e="T149" id="Seg_4472" s="T148">Iwanɨwna</ta>
            <ta e="T150" id="Seg_4473" s="T149">man</ta>
            <ta e="T151" id="Seg_4474" s="T150">täp-nan</ta>
            <ta e="T152" id="Seg_4475" s="T151">warkɨ-ŋ</ta>
            <ta e="T153" id="Seg_4476" s="T152">mazɨm</ta>
            <ta e="T154" id="Seg_4477" s="T153">warɨ-n</ta>
            <ta e="T155" id="Seg_4478" s="T154">soŋ</ta>
            <ta e="T156" id="Seg_4479" s="T155">asa</ta>
            <ta e="T157" id="Seg_4480" s="T156">qwɛdɨ-mbɨ-ku-n</ta>
            <ta e="T158" id="Seg_4481" s="T157">man</ta>
            <ta e="T159" id="Seg_4482" s="T158">oneŋ</ta>
            <ta e="T160" id="Seg_4483" s="T159">meː-le</ta>
            <ta e="T161" id="Seg_4484" s="T160">tat-ɨ-r-ɨ-ku-w</ta>
            <ta e="T162" id="Seg_4485" s="T161">qaj-m</ta>
            <ta e="T163" id="Seg_4486" s="T162">kɨgɨ-ŋ</ta>
            <ta e="T164" id="Seg_4487" s="T163">täp-nan</ta>
            <ta e="T165" id="Seg_4488" s="T164">nagur</ta>
            <ta e="T166" id="Seg_4489" s="T165">sʼütdʼe</ta>
            <ta e="T167" id="Seg_4490" s="T166">maːt-ntə</ta>
            <ta e="T168" id="Seg_4491" s="T167">maːt-ɨ-n</ta>
            <ta e="T169" id="Seg_4492" s="T168">sʼütdʼe-la</ta>
            <ta e="T171" id="Seg_4493" s="T170">maːt</ta>
            <ta e="T172" id="Seg_4494" s="T171">pödpə</ta>
            <ta e="T175" id="Seg_4495" s="T174">üt</ta>
            <ta e="T176" id="Seg_4496" s="T175">maːt-qɨn</ta>
            <ta e="T177" id="Seg_4497" s="T176">eː-n</ta>
            <ta e="T178" id="Seg_4498" s="T177">pärču-ntɨ</ta>
            <ta e="T179" id="Seg_4499" s="T178">üt</ta>
            <ta e="T180" id="Seg_4500" s="T179">i</ta>
            <ta e="T181" id="Seg_4501" s="T180">qandɨ-mbɨdi</ta>
            <ta e="T182" id="Seg_4502" s="T181">üt</ta>
            <ta e="T183" id="Seg_4503" s="T182">üt</ta>
            <ta e="T184" id="Seg_4504" s="T183">wesʼ</ta>
            <ta e="T185" id="Seg_4505" s="T184">maːt-qɨn</ta>
            <ta e="T186" id="Seg_4506" s="T185">eː-nɨ-tɨn</ta>
            <ta e="T187" id="Seg_4507" s="T186">okkɨr</ta>
            <ta e="T188" id="Seg_4508" s="T187">sʼütdʼe-qɨn</ta>
            <ta e="T189" id="Seg_4509" s="T188">uːr-ku-un</ta>
            <ta e="T190" id="Seg_4510" s="T189">*natʼe-n</ta>
            <ta e="T191" id="Seg_4511" s="T190">pärču-ntɨ</ta>
            <ta e="T192" id="Seg_4512" s="T191">üt</ta>
            <ta e="T193" id="Seg_4513" s="T192">i</ta>
            <ta e="T194" id="Seg_4514" s="T193">qandɨ-mbɨdi</ta>
            <ta e="T195" id="Seg_4515" s="T194">üt</ta>
            <ta e="T196" id="Seg_4516" s="T195">maːt-qɨn</ta>
            <ta e="T197" id="Seg_4517" s="T196">töt-mbɨ-ku-un</ta>
            <ta e="T198" id="Seg_4518" s="T197">i</ta>
            <ta e="T199" id="Seg_4519" s="T198">qözu-mbɨ-ku-un</ta>
            <ta e="T200" id="Seg_4520" s="T199">wesʼ</ta>
            <ta e="T201" id="Seg_4521" s="T200">müzulǯu-le</ta>
            <ta e="T202" id="Seg_4522" s="T201">qwattɨ-ku-t</ta>
            <ta e="T203" id="Seg_4523" s="T202">i</ta>
            <ta e="T204" id="Seg_4524" s="T203">qaj</ta>
            <ta e="T205" id="Seg_4525" s="T204">apti-näj</ta>
            <ta e="T206" id="Seg_4526" s="T205">tʼäkku</ta>
            <ta e="T207" id="Seg_4527" s="T206">cɨpočka-m</ta>
            <ta e="T208" id="Seg_4528" s="T207">nameʒal-enǯɨ-l</ta>
            <ta e="T209" id="Seg_4529" s="T208">üt</ta>
            <ta e="T210" id="Seg_4530" s="T209">šoɣər-le</ta>
            <ta e="T211" id="Seg_4531" s="T210">qalɨ-ku-n</ta>
            <ta e="T212" id="Seg_4532" s="T211">wesʼ</ta>
            <ta e="T213" id="Seg_4533" s="T212">müzulǯu-le</ta>
            <ta e="T214" id="Seg_4534" s="T213">meʒal-ku-t</ta>
            <ta e="T215" id="Seg_4535" s="T214">maːt-la</ta>
            <ta e="T216" id="Seg_4536" s="T215">tɨtʼa-n</ta>
            <ta e="T217" id="Seg_4537" s="T216">iːbəgaj</ta>
            <ta e="T218" id="Seg_4538" s="T217">tettɨ</ta>
            <ta e="T219" id="Seg_4539" s="T218">ätaš</ta>
            <ta e="T220" id="Seg_4540" s="T219">maːt-la</ta>
            <ta e="T221" id="Seg_4541" s="T220">wesʼ</ta>
            <ta e="T222" id="Seg_4542" s="T221">okkɨr</ta>
            <ta e="T223" id="Seg_4543" s="T222">saŋ</ta>
            <ta e="T224" id="Seg_4544" s="T223">maːt-la</ta>
            <ta e="T225" id="Seg_4545" s="T224">maːt-la</ta>
            <ta e="T226" id="Seg_4546" s="T225">tʼümbɨ</ta>
            <ta e="T227" id="Seg_4547" s="T226">asa</ta>
            <ta e="T228" id="Seg_4548" s="T227">qundaqɨn</ta>
            <ta e="T229" id="Seg_4549" s="T228">po-la</ta>
            <ta e="T230" id="Seg_4550" s="T229">kə-la</ta>
            <ta e="T231" id="Seg_4551" s="T230">üdippi-la</ta>
            <ta e="T232" id="Seg_4552" s="T231">jolka-la</ta>
            <ta e="T233" id="Seg_4553" s="T232">sosna-la</ta>
            <ta e="T234" id="Seg_4554" s="T233">tivə-n-po-la</ta>
            <ta e="T235" id="Seg_4555" s="T234">pi-la</ta>
            <ta e="T236" id="Seg_4556" s="T235">tautʼe-n</ta>
            <ta e="T237" id="Seg_4557" s="T236">taɣ-ɨ-ŋ</ta>
            <ta e="T238" id="Seg_4558" s="T237">koːci-ŋ</ta>
            <ta e="T239" id="Seg_4559" s="T238">krɨbɨ-la</ta>
            <ta e="T240" id="Seg_4560" s="T239">tʼelɨm-ku-n</ta>
            <ta e="T241" id="Seg_4561" s="T240">me</ta>
            <ta e="T242" id="Seg_4562" s="T241">taɣ-ɨ-ŋ</ta>
            <ta e="T243" id="Seg_4563" s="T242">krɨbɨ</ta>
            <ta e="T244" id="Seg_4564" s="T243">taqqə-ntɨ-enǯɨ-un</ta>
            <ta e="T245" id="Seg_4565" s="T244">üdɨn</ta>
            <ta e="T246" id="Seg_4566" s="T245">koːci-ŋ</ta>
            <ta e="T247" id="Seg_4567" s="T246">eː-ku-nɨ-tɨn</ta>
            <ta e="T248" id="Seg_4568" s="T247">swetoɣ-ɨ-la</ta>
            <ta e="T249" id="Seg_4569" s="T248">ka-ŋ</ta>
            <ta e="T250" id="Seg_4570" s="T249">tolʼdʼ-ɨ-se</ta>
            <ta e="T251" id="Seg_4571" s="T250">palʼdʼi-tɨn</ta>
            <ta e="T252" id="Seg_4572" s="T251">qɨbanʼaǯa-la</ta>
            <ta e="T253" id="Seg_4573" s="T252">salaska-se</ta>
            <ta e="T254" id="Seg_4574" s="T253">*nʼäz-ɨ-r-nɨ-tɨn</ta>
            <ta e="T255" id="Seg_4575" s="T254">aran</ta>
            <ta e="T256" id="Seg_4576" s="T255">po-n</ta>
            <ta e="T257" id="Seg_4577" s="T256">tʼäb-la</ta>
            <ta e="T258" id="Seg_4578" s="T257">wesʼ</ta>
            <ta e="T259" id="Seg_4579" s="T258">šolta-na</ta>
            <ta e="T260" id="Seg_4580" s="T259">azu-l-enǯɨ-tɨn</ta>
            <ta e="T261" id="Seg_4581" s="T260">qussaqɨn</ta>
            <ta e="T262" id="Seg_4582" s="T261">me</ta>
            <ta e="T263" id="Seg_4583" s="T262">tüː-sɨ-un</ta>
            <ta e="T264" id="Seg_4584" s="T263">Nowosibirsk-ɨ-ntə</ta>
            <ta e="T265" id="Seg_4585" s="T264">seqqɨ-sɨ-un</ta>
            <ta e="T266" id="Seg_4586" s="T265">Nowosibirsk-ɨ-qɨn</ta>
            <ta e="T267" id="Seg_4587" s="T266">vokzal-qɨn</ta>
            <ta e="T268" id="Seg_4588" s="T267">innä-lʼ</ta>
            <ta e="T269" id="Seg_4589" s="T268">ätaš-qɨn</ta>
            <ta e="T270" id="Seg_4590" s="T269">seqqɨ-sɨ-un</ta>
            <ta e="T271" id="Seg_4591" s="T270">sobla-mtätte</ta>
            <ta e="T272" id="Seg_4592" s="T271">ätaš-qɨn</ta>
            <ta e="T273" id="Seg_4593" s="T272">qondu-sɨ-un</ta>
            <ta e="T274" id="Seg_4594" s="T273">mäɣuntə</ta>
            <ta e="T275" id="Seg_4595" s="T274">wesʼ</ta>
            <ta e="T276" id="Seg_4596" s="T275">me-nɨ-tɨn</ta>
            <ta e="T277" id="Seg_4597" s="T276">matraz-la-m</ta>
            <ta e="T278" id="Seg_4598" s="T277">adʼejal-la-m</ta>
            <ta e="T279" id="Seg_4599" s="T278">paduška-la-m</ta>
            <ta e="T280" id="Seg_4600" s="T279">qondu-sɨ-un</ta>
            <ta e="T281" id="Seg_4601" s="T280">soːdʼiga-ŋ</ta>
            <ta e="T282" id="Seg_4602" s="T281">qum-la</ta>
            <ta e="T283" id="Seg_4603" s="T282">koːci-ŋ</ta>
            <ta e="T284" id="Seg_4604" s="T283">eː-sɨ-tɨn</ta>
            <ta e="T285" id="Seg_4605" s="T284">iːbəgaj</ta>
            <ta e="T286" id="Seg_4606" s="T285">vokzal</ta>
            <ta e="T287" id="Seg_4607" s="T286">*natʼe-n</ta>
            <ta e="T288" id="Seg_4608" s="T287">maɣɨlǯə-naš</ta>
            <ta e="T289" id="Seg_4609" s="T288">asa</ta>
            <ta e="T290" id="Seg_4610" s="T289">čanǯu-naš</ta>
            <ta e="T291" id="Seg_4611" s="T290">qare-mɨ-qɨn</ta>
            <ta e="T292" id="Seg_4612" s="T291">qwan-nɨ-un</ta>
            <ta e="T293" id="Seg_4613" s="T292">stalowaj-ntə</ta>
            <ta e="T294" id="Seg_4614" s="T293">täp-la</ta>
            <ta e="T295" id="Seg_4615" s="T294">ket-ku-tɨn</ta>
            <ta e="T296" id="Seg_4616" s="T295">restoran</ta>
            <ta e="T297" id="Seg_4617" s="T296">*natʼe-n</ta>
            <ta e="T298" id="Seg_4618" s="T297">soːdʼiga-ŋ</ta>
            <ta e="T299" id="Seg_4619" s="T298">eː-n</ta>
            <ta e="T300" id="Seg_4620" s="T299">raj-ɨ-n</ta>
            <ta e="T301" id="Seg_4621" s="T300">sʼütdʼe</ta>
            <ta e="T302" id="Seg_4622" s="T301">*natʼe-n</ta>
            <ta e="T303" id="Seg_4623" s="T302">i</ta>
            <ta e="T304" id="Seg_4624" s="T303">am-r-sɨ-un</ta>
            <ta e="T305" id="Seg_4625" s="T304">me-nan</ta>
            <ta e="T306" id="Seg_4626" s="T305">more</ta>
            <ta e="T307" id="Seg_4627" s="T306">čaček-ɨ-n</ta>
            <ta e="T308" id="Seg_4628" s="T307">eː-n</ta>
            <ta e="T309" id="Seg_4629" s="T308">*natʼe-n</ta>
            <ta e="T310" id="Seg_4630" s="T309">qum-la</ta>
            <ta e="T311" id="Seg_4631" s="T310">qwäːlɨj-lʼ-nɨ-tɨn</ta>
            <ta e="T312" id="Seg_4632" s="T311">sɨbɨq-se</ta>
            <ta e="T313" id="Seg_4633" s="T312">nɨrsa-la</ta>
            <ta e="T314" id="Seg_4634" s="T313">qwat-ku-ntɨ-tɨn</ta>
            <ta e="T315" id="Seg_4635" s="T314">i</ta>
            <ta e="T316" id="Seg_4636" s="T315">pʼetʼe-la</ta>
            <ta e="T317" id="Seg_4637" s="T316">qɨba-nädek-ka</ta>
            <ta e="T318" id="Seg_4638" s="T317">xazʼäjka-n</ta>
            <ta e="T319" id="Seg_4639" s="T318">ne-ka-tə</ta>
            <ta e="T320" id="Seg_4640" s="T319">tat-ku-sɨ-t</ta>
            <ta e="T321" id="Seg_4641" s="T320">nɨrsa-m</ta>
            <ta e="T322" id="Seg_4642" s="T321">tat-sɨ-t</ta>
            <ta e="T323" id="Seg_4643" s="T322">nɨrsa</ta>
            <ta e="T324" id="Seg_4644" s="T323">qandɨ-dʼi-mbɨ</ta>
            <ta e="T325" id="Seg_4645" s="T324">koška</ta>
            <ta e="T326" id="Seg_4646" s="T325">täp-ɨ-m</ta>
            <ta e="T327" id="Seg_4647" s="T326">am-sɨ-t</ta>
            <ta e="T328" id="Seg_4648" s="T327">man-nan</ta>
            <ta e="T329" id="Seg_4649" s="T328">sədə</ta>
            <ta e="T330" id="Seg_4650" s="T329">padruga-nɨ</ta>
            <ta e="T331" id="Seg_4651" s="T330">eː-nɨ</ta>
            <ta e="T332" id="Seg_4652" s="T331">man</ta>
            <ta e="T333" id="Seg_4653" s="T332">täp-la-nä</ta>
            <ta e="T334" id="Seg_4654" s="T333">palʼdʼi-ku-nɨ-ŋ</ta>
            <ta e="T335" id="Seg_4655" s="T334">i</ta>
            <ta e="T336" id="Seg_4656" s="T335">täp-la</ta>
            <ta e="T337" id="Seg_4657" s="T336">mekka</ta>
            <ta e="T338" id="Seg_4658" s="T337">palʼdʼi-ku-nɨ-tɨn</ta>
            <ta e="T339" id="Seg_4659" s="T338">man</ta>
            <ta e="T340" id="Seg_4660" s="T339">sormu-enǯɨ-ŋ</ta>
            <ta e="T341" id="Seg_4661" s="T340">poqqɨ-la-lʼ</ta>
            <ta e="T342" id="Seg_4662" s="T341">ola</ta>
            <ta e="T343" id="Seg_4663" s="T342">amdɨ-gu</ta>
            <ta e="T344" id="Seg_4664" s="T343">asa</ta>
            <ta e="T345" id="Seg_4665" s="T344">kɨgɨ-ŋ</ta>
            <ta e="T346" id="Seg_4666" s="T345">a</ta>
            <ta e="T347" id="Seg_4667" s="T346">oneŋ</ta>
            <ta e="T348" id="Seg_4668" s="T347">qɨdan</ta>
            <ta e="T349" id="Seg_4669" s="T348">qojmɨ-ču-ku-ŋ</ta>
            <ta e="T350" id="Seg_4670" s="T349">man</ta>
            <ta e="T351" id="Seg_4671" s="T350">üdɨn</ta>
            <ta e="T352" id="Seg_4672" s="T351">Satdijedo-ntə</ta>
            <ta e="T353" id="Seg_4673" s="T352">tüː-enǯɨ-ŋ</ta>
            <ta e="T354" id="Seg_4674" s="T353">qwɛl-s-gu</ta>
            <ta e="T355" id="Seg_4675" s="T354">čobər-ɨ-m</ta>
            <ta e="T356" id="Seg_4676" s="T355">qwat-ɨ-gu</ta>
            <ta e="T357" id="Seg_4677" s="T356">qwačə</ta>
            <ta e="T358" id="Seg_4678" s="T357">iːbəgaj-ŋ</ta>
            <ta e="T359" id="Seg_4679" s="T358">eː-n</ta>
            <ta e="T360" id="Seg_4680" s="T359">man</ta>
            <ta e="T361" id="Seg_4681" s="T360">na</ta>
            <ta e="T362" id="Seg_4682" s="T361">tüː-ntɨ-enǯɨ-ŋ</ta>
            <ta e="T363" id="Seg_4683" s="T362">i</ta>
            <ta e="T364" id="Seg_4684" s="T363">tekka</ta>
            <ta e="T365" id="Seg_4685" s="T364">aːdu-lǯi-enǯɨ-w</ta>
            <ta e="T366" id="Seg_4686" s="T365">qwačə-m</ta>
            <ta e="T367" id="Seg_4687" s="T366">tekka</ta>
            <ta e="T368" id="Seg_4688" s="T367">na</ta>
            <ta e="T369" id="Seg_4689" s="T368">qwačə-m</ta>
            <ta e="T370" id="Seg_4690" s="T369">tat-enǯɨ-w</ta>
            <ta e="T371" id="Seg_4691" s="T370">tan</ta>
            <ta e="T372" id="Seg_4692" s="T371">*mantɨ-mbɨ-enǯɨ-l</ta>
            <ta e="T373" id="Seg_4693" s="T372">tautʼe-n</ta>
            <ta e="T374" id="Seg_4694" s="T373">wsʼäkij</ta>
            <ta e="T375" id="Seg_4695" s="T374">mašina-la-se</ta>
            <ta e="T376" id="Seg_4696" s="T375">palʼdʼi-sɨ-ŋ</ta>
            <ta e="T379" id="Seg_4697" s="T378">*larɨ-mbɨ-sɨ-ŋ</ta>
            <ta e="T380" id="Seg_4698" s="T379">mazɨm</ta>
            <ta e="T381" id="Seg_4699" s="T380">Wolodʼa</ta>
            <ta e="T382" id="Seg_4700" s="T381">qwačə-qɨn</ta>
            <ta e="T383" id="Seg_4701" s="T382">warɨ-sɨ</ta>
            <ta e="T384" id="Seg_4702" s="T383">wesʼ</ta>
            <ta e="T385" id="Seg_4703" s="T384">aːdu-lǯɨ-ku-sɨ-t</ta>
            <ta e="T386" id="Seg_4704" s="T385">ondə</ta>
            <ta e="T387" id="Seg_4705" s="T386">də</ta>
            <ta e="T388" id="Seg_4706" s="T387">kuː</ta>
            <ta e="T389" id="Seg_4707" s="T388">qwan-naš</ta>
            <ta e="T390" id="Seg_4708" s="T389">maɣɨlǯə-naš</ta>
            <ta e="T391" id="Seg_4709" s="T390">man</ta>
            <ta e="T392" id="Seg_4710" s="T391">qözu-ku-m-nɨ-ŋ</ta>
            <ta e="T393" id="Seg_4711" s="T392">tʼärɨ-ŋ</ta>
            <ta e="T394" id="Seg_4712" s="T393">qözu-gu</ta>
            <ta e="T395" id="Seg_4713" s="T394">nadə</ta>
            <ta e="T396" id="Seg_4714" s="T395">a</ta>
            <ta e="T397" id="Seg_4715" s="T396">Wolodʼa</ta>
            <ta e="T398" id="Seg_4716" s="T397">mekka</ta>
            <ta e="T399" id="Seg_4717" s="T398">tʼärɨ-n</ta>
            <ta e="T400" id="Seg_4718" s="T399">kuː</ta>
            <ta e="T401" id="Seg_4719" s="T400">qözu-naš</ta>
            <ta e="T402" id="Seg_4720" s="T401">qözu-mbɨdi</ta>
            <ta e="T403" id="Seg_4721" s="T402">mɨ</ta>
            <ta e="T404" id="Seg_4722" s="T403">tʼäkku</ta>
            <ta e="T405" id="Seg_4723" s="T404">qözu-mbɨdi</ta>
            <ta e="T406" id="Seg_4724" s="T405">mɨ-la</ta>
            <ta e="T407" id="Seg_4725" s="T406">wesʼ</ta>
            <ta e="T408" id="Seg_4726" s="T407">maːt-ɨ-n</ta>
            <ta e="T409" id="Seg_4727" s="T408">sʼütdʼe-qɨn</ta>
            <ta e="T410" id="Seg_4728" s="T409">eː-nɨ-tɨn</ta>
            <ta e="T411" id="Seg_4729" s="T410">pazar-qɨn</ta>
            <ta e="T412" id="Seg_4730" s="T411">eː-sɨ-j</ta>
            <ta e="T413" id="Seg_4731" s="T412">täp</ta>
            <ta e="T414" id="Seg_4732" s="T413">mekka</ta>
            <ta e="T415" id="Seg_4733" s="T414">tʼärɨ-n</ta>
            <ta e="T416" id="Seg_4734" s="T415">*natʼe-n-t</ta>
            <ta e="T417" id="Seg_4735" s="T416">qaj-no</ta>
            <ta e="T418" id="Seg_4736" s="T417">asa</ta>
            <ta e="T419" id="Seg_4737" s="T418">qözu-sɨ-ntə</ta>
            <ta e="T420" id="Seg_4738" s="T419">a</ta>
            <ta e="T421" id="Seg_4739" s="T420">man</ta>
            <ta e="T422" id="Seg_4740" s="T421">asa</ta>
            <ta e="T423" id="Seg_4741" s="T422">qözu-sɨ-ŋ</ta>
            <ta e="T424" id="Seg_4742" s="T423">tak</ta>
            <ta e="T425" id="Seg_4743" s="T424">man</ta>
            <ta e="T426" id="Seg_4744" s="T425">tʼärpi-mbɨ-sɨ-w</ta>
            <ta e="T427" id="Seg_4745" s="T426">qussaqɨn</ta>
            <ta e="T428" id="Seg_4746" s="T427">mašina-m</ta>
            <ta e="T429" id="Seg_4747" s="T428">adɨ-sɨ-j</ta>
            <ta e="T430" id="Seg_4748" s="T429">man</ta>
            <ta e="T431" id="Seg_4749" s="T430">okkɨr</ta>
            <ta e="T432" id="Seg_4750" s="T431">ne-lʼ-qum-nä</ta>
            <ta e="T433" id="Seg_4751" s="T432">sogundʼe-ŋ</ta>
            <ta e="T434" id="Seg_4752" s="T433">qutʼet</ta>
            <ta e="T435" id="Seg_4753" s="T434">qözu-mbɨdi</ta>
            <ta e="T436" id="Seg_4754" s="T435">maːt</ta>
            <ta e="T437" id="Seg_4755" s="T436">täp</ta>
            <ta e="T438" id="Seg_4756" s="T437">mekka</ta>
            <ta e="T439" id="Seg_4757" s="T438">aːdu-lǯɨ-t</ta>
            <ta e="T440" id="Seg_4758" s="T439">tutʼetdo</ta>
            <ta e="T441" id="Seg_4759" s="T440">man</ta>
            <ta e="T442" id="Seg_4760" s="T441">kur-ol-nɨ-ŋ</ta>
            <ta e="T443" id="Seg_4761" s="T442">i</ta>
            <ta e="T444" id="Seg_4762" s="T443">qözu-ŋ</ta>
            <ta e="T445" id="Seg_4763" s="T444">man</ta>
            <ta e="T446" id="Seg_4764" s="T445">tärba-sɨ-ŋ</ta>
            <ta e="T447" id="Seg_4765" s="T446">qözu-w</ta>
            <ta e="T448" id="Seg_4766" s="T447">mal</ta>
            <ta e="T449" id="Seg_4767" s="T448">waᴣǝ-n</ta>
            <ta e="T450" id="Seg_4768" s="T449">qözu-ŋ</ta>
            <ta e="T451" id="Seg_4769" s="T450">i</ta>
            <ta e="T452" id="Seg_4770" s="T451">soŋ</ta>
            <ta e="T453" id="Seg_4771" s="T452">azu-n</ta>
            <ta e="T454" id="Seg_4772" s="T453">magazin-la</ta>
            <ta e="T455" id="Seg_4773" s="T454">ser-ku-j</ta>
            <ta e="T456" id="Seg_4774" s="T455">iːbəgaj</ta>
            <ta e="T457" id="Seg_4775" s="T456">magazin-la</ta>
            <ta e="T458" id="Seg_4776" s="T457">*natʼe-n</ta>
            <ta e="T459" id="Seg_4777" s="T458">maɣɨlǯə-naš</ta>
            <ta e="T460" id="Seg_4778" s="T459">man</ta>
            <ta e="T461" id="Seg_4779" s="T460">nagər-ɨ-m</ta>
            <ta e="T462" id="Seg_4780" s="T461">asa</ta>
            <ta e="T463" id="Seg_4781" s="T462">tonu-w</ta>
            <ta e="T464" id="Seg_4782" s="T463">oneŋ</ta>
            <ta e="T465" id="Seg_4783" s="T464">asa</ta>
            <ta e="T466" id="Seg_4784" s="T465">palʼdʼi-ku-nɨ-ŋ</ta>
            <ta e="T467" id="Seg_4785" s="T466">qwačə-qɨn</ta>
            <ta e="T468" id="Seg_4786" s="T467">ato</ta>
            <ta e="T469" id="Seg_4787" s="T468">maɣɨlǯə-enǯɨ-ŋ</ta>
            <ta e="T470" id="Seg_4788" s="T469">man</ta>
            <ta e="T471" id="Seg_4789" s="T470">teper</ta>
            <ta e="T472" id="Seg_4790" s="T471">mašina-la-se</ta>
            <ta e="T473" id="Seg_4791" s="T472">palʼdʼi-ŋ</ta>
            <ta e="T474" id="Seg_4792" s="T473">elɨ-sɨ-ŋ</ta>
            <ta e="T475" id="Seg_4793" s="T474">qwačə-qɨn</ta>
            <ta e="T476" id="Seg_4794" s="T475">Čʼornaja_rečka-qɨn</ta>
            <ta e="T477" id="Seg_4795" s="T476">i</ta>
            <ta e="T478" id="Seg_4796" s="T477">Taxtamɨšowa-qɨn</ta>
            <ta e="T479" id="Seg_4797" s="T478">elɨ-sɨ-ŋ</ta>
            <ta e="T480" id="Seg_4798" s="T479">Dunʼa</ta>
            <ta e="T481" id="Seg_4799" s="T480">tüː-sɨ</ta>
            <ta e="T482" id="Seg_4800" s="T481">Čʼornaja_rečka-ntə</ta>
            <ta e="T483" id="Seg_4801" s="T482">iː-ntɨ-se</ta>
            <ta e="T484" id="Seg_4802" s="T483">täp-ɨ-staɣɨ</ta>
            <ta e="T485" id="Seg_4803" s="T484">warkɨ-qij</ta>
            <ta e="T486" id="Seg_4804" s="T485">Mikola</ta>
            <ta e="T487" id="Seg_4805" s="T486">Lazarɨčʼ-ɨ-n</ta>
            <ta e="T488" id="Seg_4806" s="T487">paja-ka-tə</ta>
            <ta e="T489" id="Seg_4807" s="T488">quː-nɨ</ta>
            <ta e="T490" id="Seg_4808" s="T489">Čʼornaja_rečka-qɨn</ta>
            <ta e="T491" id="Seg_4809" s="T490">warkɨ-sɨ</ta>
            <ta e="T492" id="Seg_4810" s="T491">ne-ntɨ-nan</ta>
            <ta e="T493" id="Seg_4811" s="T492">a</ta>
            <ta e="T494" id="Seg_4812" s="T493">man</ta>
            <ta e="T495" id="Seg_4813" s="T494">qwan-nɨ-ŋ</ta>
            <ta e="T496" id="Seg_4814" s="T495">qwačə-la-m</ta>
            <ta e="T497" id="Seg_4815" s="T496">*mantɨ-mbɨ-gu</ta>
            <ta e="T498" id="Seg_4816" s="T497">Marʼina</ta>
            <ta e="T499" id="Seg_4817" s="T498">Pawlowna</ta>
            <ta e="T500" id="Seg_4818" s="T499">qundar</ta>
            <ta e="T501" id="Seg_4819" s="T500">man</ta>
            <ta e="T502" id="Seg_4820" s="T501">nagər-w</ta>
            <ta e="T503" id="Seg_4821" s="T502">tüː-enǯɨ-n</ta>
            <ta e="T504" id="Seg_4822" s="T503">i</ta>
            <ta e="T505" id="Seg_4823" s="T504">mekka</ta>
            <ta e="T506" id="Seg_4824" s="T505">nagər-ɨ-m</ta>
            <ta e="T507" id="Seg_4825" s="T506">srazu</ta>
            <ta e="T508" id="Seg_4826" s="T507">nagər-etɨ</ta>
            <ta e="T509" id="Seg_4827" s="T508">man</ta>
            <ta e="T510" id="Seg_4828" s="T509">adɨ-lǯi-enǯɨ-ŋ</ta>
            <ta e="T511" id="Seg_4829" s="T510">wesʼ</ta>
            <ta e="T512" id="Seg_4830" s="T511">nagər-etɨ</ta>
            <ta e="T513" id="Seg_4831" s="T512">qundar</ta>
            <ta e="T514" id="Seg_4832" s="T513">man</ta>
            <ta e="T515" id="Seg_4833" s="T514">qum-la-w</ta>
            <ta e="T516" id="Seg_4834" s="T515">warkɨ-tɨn</ta>
            <ta e="T517" id="Seg_4835" s="T516">Kolʼä-n</ta>
            <ta e="T518" id="Seg_4836" s="T517">tʼаːt</ta>
            <ta e="T519" id="Seg_4837" s="T518">qaj-lʼ-ɨ-mɨ-m</ta>
            <ta e="T520" id="Seg_4838" s="T519">ündɨ-ču-ku-l</ta>
            <ta e="T521" id="Seg_4839" s="T520">alʼi</ta>
            <ta e="T522" id="Seg_4840" s="T521">asa</ta>
            <ta e="T523" id="Seg_4841" s="T522">wesʼ</ta>
            <ta e="T524" id="Seg_4842" s="T523">mekka</ta>
            <ta e="T525" id="Seg_4843" s="T524">nagər-ɨ-etɨ</ta>
            <ta e="T526" id="Seg_4844" s="T525">man</ta>
            <ta e="T527" id="Seg_4845" s="T526">putʼom</ta>
            <ta e="T528" id="Seg_4846" s="T527">adɨ-lǯɨ-enǯɨ-ŋ</ta>
            <ta e="T529" id="Seg_4847" s="T528">täɣɨtdɨltə</ta>
            <ta e="T530" id="Seg_4848" s="T529">nʼäj-se</ta>
            <ta e="T531" id="Seg_4849" s="T530">me-ku-nɨ-tɨn</ta>
            <ta e="T532" id="Seg_4850" s="T531">alʼi</ta>
            <ta e="T533" id="Seg_4851" s="T532">moga-se</ta>
            <ta e="T534" id="Seg_4852" s="T533">me-ku-nɨ-tɨn</ta>
            <ta e="T535" id="Seg_4853" s="T534">a</ta>
            <ta e="T536" id="Seg_4854" s="T535">magazin-qɨn</ta>
            <ta e="T537" id="Seg_4855" s="T536">qaj-la</ta>
            <ta e="T538" id="Seg_4856" s="T537">eː-ntɨ-tɨn</ta>
            <ta e="T539" id="Seg_4857" s="T538">wesʼ</ta>
            <ta e="T540" id="Seg_4858" s="T539">nagər-etɨ</ta>
            <ta e="T541" id="Seg_4859" s="T540">ämnä-l</ta>
            <ta e="T542" id="Seg_4860" s="T541">qaj</ta>
            <ta e="T543" id="Seg_4861" s="T542">elɨ-n</ta>
            <ta e="T544" id="Seg_4862" s="T543">alʼi</ta>
            <ta e="T545" id="Seg_4863" s="T544">asa</ta>
            <ta e="T546" id="Seg_4864" s="T545">tʼärɨ-sɨ-tɨn</ta>
            <ta e="T547" id="Seg_4865" s="T546">to</ta>
            <ta e="T548" id="Seg_4866" s="T547">qwan-mbɨ</ta>
            <ta e="T549" id="Seg_4867" s="T548">Sinka-n</ta>
            <ta e="T550" id="Seg_4868" s="T549">tʼаːt</ta>
            <ta e="T551" id="Seg_4869" s="T550">nagər-etɨ</ta>
            <ta e="T552" id="Seg_4870" s="T551">qundar</ta>
            <ta e="T553" id="Seg_4871" s="T552">elɨ-ku-n</ta>
            <ta e="T554" id="Seg_4872" s="T553">warkɨ-ku-n</ta>
            <ta e="T555" id="Seg_4873" s="T554">man</ta>
            <ta e="T556" id="Seg_4874" s="T555">täp-nä</ta>
            <ta e="T557" id="Seg_4875" s="T556">nagər-ɨ-m</ta>
            <ta e="T558" id="Seg_4876" s="T557">nagər-sɨ-ŋ</ta>
            <ta e="T559" id="Seg_4877" s="T558">täp</ta>
            <ta e="T560" id="Seg_4878" s="T559">mekka</ta>
            <ta e="T561" id="Seg_4879" s="T560">asa</ta>
            <ta e="T562" id="Seg_4880" s="T561">nagər-ku-t</ta>
            <ta e="T563" id="Seg_4881" s="T562">tetʼa</ta>
            <ta e="T564" id="Seg_4882" s="T563">Marʼina</ta>
            <ta e="T565" id="Seg_4883" s="T564">perwɨj</ta>
            <ta e="T566" id="Seg_4884" s="T565">maj-se</ta>
            <ta e="T567" id="Seg_4885" s="T566">praznik-se</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_4886" s="T1">we.[NOM]</ta>
            <ta e="T3" id="Seg_4887" s="T2">this-day.[NOM]</ta>
            <ta e="T4" id="Seg_4888" s="T3">sit-1DU</ta>
            <ta e="T5" id="Seg_4889" s="T4">Angelina.[NOM]</ta>
            <ta e="T6" id="Seg_4890" s="T5">Ivanovna-COM</ta>
            <ta e="T7" id="Seg_4891" s="T6">speak-1DU</ta>
            <ta e="T8" id="Seg_4892" s="T7">you.SG.GEN</ta>
            <ta e="T9" id="Seg_4893" s="T8">about</ta>
            <ta e="T10" id="Seg_4894" s="T9">speak-1DU</ta>
            <ta e="T11" id="Seg_4895" s="T10">how</ta>
            <ta e="T12" id="Seg_4896" s="T11">live-2SG.S</ta>
            <ta e="T13" id="Seg_4897" s="T12">what-PL.[NOM]</ta>
            <ta e="T14" id="Seg_4898" s="T13">do-HAB-INFER-2SG.O</ta>
            <ta e="T15" id="Seg_4899" s="T14">I.NOM</ta>
            <ta e="T16" id="Seg_4900" s="T15">(s)he-ALL</ta>
            <ta e="T17" id="Seg_4901" s="T16">say-CVB</ta>
            <ta e="T18" id="Seg_4902" s="T17">bring-EP-FRQ-CO-1SG.O</ta>
            <ta e="T19" id="Seg_4903" s="T18">you.SG.GEN</ta>
            <ta e="T20" id="Seg_4904" s="T19">about</ta>
            <ta e="T21" id="Seg_4905" s="T20">I.NOM</ta>
            <ta e="T22" id="Seg_4906" s="T21">say-1SG.S</ta>
            <ta e="T23" id="Seg_4907" s="T22">(s)he.[NOM]</ta>
            <ta e="T24" id="Seg_4908" s="T23">old.woman-TRL-PST.NAR.[3SG.S]</ta>
            <ta e="T25" id="Seg_4909" s="T24">I.NOM</ta>
            <ta e="T26" id="Seg_4910" s="T25">(s)he-ALL</ta>
            <ta e="T27" id="Seg_4911" s="T26">say-PST-1SG.O</ta>
            <ta e="T28" id="Seg_4912" s="T27">you.SG.NOM</ta>
            <ta e="T29" id="Seg_4913" s="T28">we.ACC</ta>
            <ta e="T30" id="Seg_4914" s="T29">Maria-COM</ta>
            <ta e="T31" id="Seg_4915" s="T30">food-VBLZ-PST-3SG.O</ta>
            <ta e="T32" id="Seg_4916" s="T31">we.[NOM]</ta>
            <ta e="T33" id="Seg_4917" s="T32">you.SG-COM</ta>
            <ta e="T34" id="Seg_4918" s="T33">how</ta>
            <ta e="T35" id="Seg_4919" s="T34">drink-PST-1PL</ta>
            <ta e="T36" id="Seg_4920" s="T35">I.NOM</ta>
            <ta e="T37" id="Seg_4921" s="T36">(s)he-ALL</ta>
            <ta e="T38" id="Seg_4922" s="T37">all</ta>
            <ta e="T39" id="Seg_4923" s="T38">say-PST-1SG.O</ta>
            <ta e="T40" id="Seg_4924" s="T39">I.NOM</ta>
            <ta e="T41" id="Seg_4925" s="T40">speak-PST-1SG.S</ta>
            <ta e="T42" id="Seg_4926" s="T41">aunt.[NOM]</ta>
            <ta e="T43" id="Seg_4927" s="T42">Marina.[NOM]</ta>
            <ta e="T44" id="Seg_4928" s="T43">letter.[NOM]</ta>
            <ta e="T45" id="Seg_4929" s="T44">letter-VBLZ-FUT-1SG.S</ta>
            <ta e="T46" id="Seg_4930" s="T45">JUSS</ta>
            <ta e="T47" id="Seg_4931" s="T46">read-PFV-IMP.3SG/PL.O</ta>
            <ta e="T48" id="Seg_4932" s="T47">hello</ta>
            <ta e="T49" id="Seg_4933" s="T48">Marina.[NOM]</ta>
            <ta e="T50" id="Seg_4934" s="T49">Pawlowna.[NOM]</ta>
            <ta e="T51" id="Seg_4935" s="T50">I.NOM</ta>
            <ta e="T52" id="Seg_4936" s="T51">you.ALL</ta>
            <ta e="T53" id="Seg_4937" s="T52">Selkup</ta>
            <ta e="T54" id="Seg_4938" s="T53">letter.[NOM]</ta>
            <ta e="T55" id="Seg_4939" s="T54">letter-VBLZ-FUT-1SG.S</ta>
            <ta e="T56" id="Seg_4940" s="T55">Russian-language-ADVZ</ta>
            <ta e="T57" id="Seg_4941" s="T56">I.NOM</ta>
            <ta e="T58" id="Seg_4942" s="T57">NEG</ta>
            <ta e="T59" id="Seg_4943" s="T58">can-1SG.S</ta>
            <ta e="T60" id="Seg_4944" s="T59">I.NOM</ta>
            <ta e="T61" id="Seg_4945" s="T60">live-1SG.S</ta>
            <ta e="T62" id="Seg_4946" s="T61">now</ta>
            <ta e="T63" id="Seg_4947" s="T62">city-LOC</ta>
            <ta e="T64" id="Seg_4948" s="T63">Novosibirsk-EP-LOC</ta>
            <ta e="T65" id="Seg_4949" s="T64">city.[NOM]</ta>
            <ta e="T66" id="Seg_4950" s="T65">big</ta>
            <ta e="T67" id="Seg_4951" s="T66">good-DRV</ta>
            <ta e="T68" id="Seg_4952" s="T67">city-EL.3SG</ta>
            <ta e="T69" id="Seg_4953" s="T68">Novosibirsk-EP-ILL</ta>
            <ta e="T70" id="Seg_4954" s="T69">train-INSTR</ta>
            <ta e="T71" id="Seg_4955" s="T70">go-PST-1SG.S</ta>
            <ta e="T72" id="Seg_4956" s="T71">train-LOC</ta>
            <ta e="T73" id="Seg_4957" s="T72">go-CVB</ta>
            <ta e="T74" id="Seg_4958" s="T73">card-VBLZ-PST-1PL</ta>
            <ta e="T75" id="Seg_4959" s="T74">Tajozhnyj.[NOM]</ta>
            <ta e="T76" id="Seg_4960" s="T75">station-LOC</ta>
            <ta e="T77" id="Seg_4961" s="T76">train.[NOM]</ta>
            <ta e="T78" id="Seg_4962" s="T77">long-ADV.LOC</ta>
            <ta e="T79" id="Seg_4963" s="T78">stand-HAB-PST.[3SG.S]</ta>
            <ta e="T80" id="Seg_4964" s="T79">I.NOM</ta>
            <ta e="T81" id="Seg_4965" s="T80">look-DUR-PST-1SG.S</ta>
            <ta e="T82" id="Seg_4966" s="T81">and</ta>
            <ta e="T83" id="Seg_4967" s="T82">Nikin-EP-ACC</ta>
            <ta e="T84" id="Seg_4968" s="T83">what-LOC</ta>
            <ta e="T85" id="Seg_4969" s="T84">NEG</ta>
            <ta e="T86" id="Seg_4970" s="T85">see-DRV-PST-1SG.O</ta>
            <ta e="T87" id="Seg_4971" s="T86">then</ta>
            <ta e="T88" id="Seg_4972" s="T87">train.[NOM]</ta>
            <ta e="T89" id="Seg_4973" s="T88">set.off-3SG.S</ta>
            <ta e="T90" id="Seg_4974" s="T89">I.NOM</ta>
            <ta e="T91" id="Seg_4975" s="T90">all.the.time</ta>
            <ta e="T92" id="Seg_4976" s="T91">window-LOC</ta>
            <ta e="T93" id="Seg_4977" s="T92">look-DUR-PST-1SG.S</ta>
            <ta e="T94" id="Seg_4978" s="T93">village-PL.[NOM]</ta>
            <ta e="T95" id="Seg_4979" s="T94">much-ADVZ</ta>
            <ta e="T96" id="Seg_4980" s="T95">stay-CVB</ta>
            <ta e="T97" id="Seg_4981" s="T96">go-PST-3PL</ta>
            <ta e="T98" id="Seg_4982" s="T97">city-GEN</ta>
            <ta e="T99" id="Seg_4983" s="T98">border-LOC</ta>
            <ta e="T100" id="Seg_4984" s="T99">tree-PL.[NOM]</ta>
            <ta e="T101" id="Seg_4985" s="T100">taiga-EP-PL.[NOM]</ta>
            <ta e="T102" id="Seg_4986" s="T101">all.around</ta>
            <ta e="T103" id="Seg_4987" s="T102">snow.[NOM]</ta>
            <ta e="T104" id="Seg_4988" s="T103">lie-INFER.[3SG.S]</ta>
            <ta e="T105" id="Seg_4989" s="T104">I.NOM</ta>
            <ta e="T106" id="Seg_4990" s="T105">train-LOC</ta>
            <ta e="T107" id="Seg_4991" s="T106">NEG</ta>
            <ta e="T108" id="Seg_4992" s="T107">get.afraid-DUR-PST-1SG.S</ta>
            <ta e="T109" id="Seg_4993" s="T108">small-ADVZ</ta>
            <ta e="T110" id="Seg_4994" s="T109">shake-EP-3SG.O</ta>
            <ta e="T111" id="Seg_4995" s="T110">earlier</ta>
            <ta e="T112" id="Seg_4996" s="T111">I.NOM</ta>
            <ta e="T113" id="Seg_4997" s="T112">NEG</ta>
            <ta e="T114" id="Seg_4998" s="T113">walk-HAB-PST-1SG.S</ta>
            <ta e="T115" id="Seg_4999" s="T114">train-PL-INSTR</ta>
            <ta e="T116" id="Seg_5000" s="T115">I.NOM</ta>
            <ta e="T117" id="Seg_5001" s="T116">live-1SG.S</ta>
            <ta e="T118" id="Seg_5002" s="T117">woman-ADJZ-human.being-ADES</ta>
            <ta e="T119" id="Seg_5003" s="T118">we.PL.ALL</ta>
            <ta e="T120" id="Seg_5004" s="T119">this.[NOM]</ta>
            <ta e="T121" id="Seg_5005" s="T120">come-HAB-INFER.[3SG.S]</ta>
            <ta e="T122" id="Seg_5006" s="T121">Selkup</ta>
            <ta e="T123" id="Seg_5007" s="T122">language-PL-ACC</ta>
            <ta e="T124" id="Seg_5008" s="T123">write-CVB</ta>
            <ta e="T125" id="Seg_5009" s="T124">bring-EP-FRQ-EP-PST-3SG.O</ta>
            <ta e="T126" id="Seg_5010" s="T125">city-COM</ta>
            <ta e="T127" id="Seg_5011" s="T126">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T128" id="Seg_5012" s="T127">this.[NOM]</ta>
            <ta e="T129" id="Seg_5013" s="T128">come-HAB-INFER.[3SG.S]</ta>
            <ta e="T130" id="Seg_5014" s="T129">and</ta>
            <ta e="T131" id="Seg_5015" s="T130">Starosondorovo-ILL</ta>
            <ta e="T132" id="Seg_5016" s="T131">Selkup</ta>
            <ta e="T133" id="Seg_5017" s="T132">word-PL-ACC</ta>
            <ta e="T134" id="Seg_5018" s="T133">write-CVB</ta>
            <ta e="T135" id="Seg_5019" s="T134">bring-EP-FRQ-EP-PST-3SG.O</ta>
            <ta e="T136" id="Seg_5020" s="T135">you.ALL</ta>
            <ta e="T137" id="Seg_5021" s="T136">walk-HAB-PST.[3SG.S]</ta>
            <ta e="T138" id="Seg_5022" s="T137">you.SG-COM</ta>
            <ta e="T139" id="Seg_5023" s="T138">all.the.time</ta>
            <ta e="T140" id="Seg_5024" s="T139">letter-VBLZ-HAB-PST.[3SG.S]</ta>
            <ta e="T141" id="Seg_5025" s="T140">I.NOM</ta>
            <ta e="T142" id="Seg_5026" s="T141">this.[NOM]</ta>
            <ta e="T143" id="Seg_5027" s="T142">woman-ADJZ-human.being-ADES</ta>
            <ta e="T144" id="Seg_5028" s="T143">live-1SG.S</ta>
            <ta e="T145" id="Seg_5029" s="T144">Novosibirsk-EP-ADV.LOC</ta>
            <ta e="T146" id="Seg_5030" s="T145">(s)he-EP-GEN</ta>
            <ta e="T147" id="Seg_5031" s="T146">name.[NOM]-3SG</ta>
            <ta e="T148" id="Seg_5032" s="T147">Angelina.[NOM]</ta>
            <ta e="T149" id="Seg_5033" s="T148">Ivanovna.[NOM]</ta>
            <ta e="T150" id="Seg_5034" s="T149">I.NOM</ta>
            <ta e="T151" id="Seg_5035" s="T150">(s)he-ADES</ta>
            <ta e="T152" id="Seg_5036" s="T151">live-1SG.S</ta>
            <ta e="T153" id="Seg_5037" s="T152">I.ACC</ta>
            <ta e="T154" id="Seg_5038" s="T153">have-3SG.S</ta>
            <ta e="T155" id="Seg_5039" s="T154">good</ta>
            <ta e="T156" id="Seg_5040" s="T155">NEG</ta>
            <ta e="T157" id="Seg_5041" s="T156">swear.at-DUR-HAB-3SG.S</ta>
            <ta e="T158" id="Seg_5042" s="T157">I.NOM</ta>
            <ta e="T159" id="Seg_5043" s="T158">oneself.1SG</ta>
            <ta e="T160" id="Seg_5044" s="T159">do-CVB</ta>
            <ta e="T161" id="Seg_5045" s="T160">bring-EP-FRQ-EP-HAB-1SG.O</ta>
            <ta e="T162" id="Seg_5046" s="T161">what-ACC</ta>
            <ta e="T163" id="Seg_5047" s="T162">want-1SG.S</ta>
            <ta e="T164" id="Seg_5048" s="T163">(s)he-ADES</ta>
            <ta e="T165" id="Seg_5049" s="T164">three</ta>
            <ta e="T166" id="Seg_5050" s="T165">room.[NOM]</ta>
            <ta e="T167" id="Seg_5051" s="T166">house-ILL</ta>
            <ta e="T168" id="Seg_5052" s="T167">house-EP-GEN</ta>
            <ta e="T169" id="Seg_5053" s="T168">room-PL.[NOM]</ta>
            <ta e="T170" id="Seg_5054" s="T169">big.[3SG.S]</ta>
            <ta e="T171" id="Seg_5055" s="T170">house.[NOM]</ta>
            <ta e="T172" id="Seg_5056" s="T171">hot.[3SG.S]</ta>
            <ta e="T175" id="Seg_5057" s="T174">water.[NOM]</ta>
            <ta e="T176" id="Seg_5058" s="T175">house-LOC</ta>
            <ta e="T177" id="Seg_5059" s="T176">be-3SG.S</ta>
            <ta e="T178" id="Seg_5060" s="T177">boil-PTCP.PRS</ta>
            <ta e="T179" id="Seg_5061" s="T178">water.[NOM]</ta>
            <ta e="T180" id="Seg_5062" s="T179">and</ta>
            <ta e="T181" id="Seg_5063" s="T180">freeze-PTCP.PST</ta>
            <ta e="T182" id="Seg_5064" s="T181">water.[NOM]</ta>
            <ta e="T183" id="Seg_5065" s="T182">water.[NOM]</ta>
            <ta e="T184" id="Seg_5066" s="T183">all.[NOM]</ta>
            <ta e="T185" id="Seg_5067" s="T184">house-LOC</ta>
            <ta e="T186" id="Seg_5068" s="T185">be-CO-3PL</ta>
            <ta e="T187" id="Seg_5069" s="T186">one</ta>
            <ta e="T188" id="Seg_5070" s="T187">room-LOC</ta>
            <ta e="T189" id="Seg_5071" s="T188">wash.oneself-HAB-1PL</ta>
            <ta e="T190" id="Seg_5072" s="T189">there-ADV.LOC</ta>
            <ta e="T191" id="Seg_5073" s="T190">boil-PTCP.PRS</ta>
            <ta e="T192" id="Seg_5074" s="T191">water.[NOM]</ta>
            <ta e="T193" id="Seg_5075" s="T192">and</ta>
            <ta e="T194" id="Seg_5076" s="T193">freeze-PTCP.PST</ta>
            <ta e="T195" id="Seg_5077" s="T194">water.[NOM]</ta>
            <ta e="T196" id="Seg_5078" s="T195">house-LOC</ta>
            <ta e="T197" id="Seg_5079" s="T196">defecate-DUR-HAB-1PL</ta>
            <ta e="T198" id="Seg_5080" s="T197">and</ta>
            <ta e="T199" id="Seg_5081" s="T198">piss-DUR-HAB-1PL</ta>
            <ta e="T200" id="Seg_5082" s="T199">all</ta>
            <ta e="T201" id="Seg_5083" s="T200">wash-CVB</ta>
            <ta e="T202" id="Seg_5084" s="T201">begin-HAB-3SG.O</ta>
            <ta e="T203" id="Seg_5085" s="T202">and</ta>
            <ta e="T204" id="Seg_5086" s="T203">what</ta>
            <ta e="T205" id="Seg_5087" s="T204">smell.[NOM]-EMPH</ta>
            <ta e="T206" id="Seg_5088" s="T205">NEG.EX.[3SG.S]</ta>
            <ta e="T207" id="Seg_5089" s="T206">chain-ACC</ta>
            <ta e="T208" id="Seg_5090" s="T207">%%-FUT-2SG.O</ta>
            <ta e="T209" id="Seg_5091" s="T208">water.[NOM]</ta>
            <ta e="T210" id="Seg_5092" s="T209">%seethe-CVB</ta>
            <ta e="T211" id="Seg_5093" s="T210">stay-HAB-3SG.S</ta>
            <ta e="T212" id="Seg_5094" s="T211">all</ta>
            <ta e="T213" id="Seg_5095" s="T212">wash-CVB</ta>
            <ta e="T214" id="Seg_5096" s="T213">pull-HAB-3SG.O</ta>
            <ta e="T215" id="Seg_5097" s="T214">house-PL.[NOM]</ta>
            <ta e="T216" id="Seg_5098" s="T215">here-ADV.LOC</ta>
            <ta e="T217" id="Seg_5099" s="T216">big</ta>
            <ta e="T218" id="Seg_5100" s="T217">four</ta>
            <ta e="T219" id="Seg_5101" s="T218">floor.[NOM]</ta>
            <ta e="T220" id="Seg_5102" s="T219">house-PL.[NOM]</ta>
            <ta e="T221" id="Seg_5103" s="T220">all</ta>
            <ta e="T222" id="Seg_5104" s="T221">one</ta>
            <ta e="T223" id="Seg_5105" s="T222">approximately.as</ta>
            <ta e="T224" id="Seg_5106" s="T223">house-PL.[NOM]</ta>
            <ta e="T225" id="Seg_5107" s="T224">house-PL.[NOM]</ta>
            <ta e="T226" id="Seg_5108" s="T225">long</ta>
            <ta e="T227" id="Seg_5109" s="T226">NEG</ta>
            <ta e="T228" id="Seg_5110" s="T227">far</ta>
            <ta e="T229" id="Seg_5111" s="T228">tree-PL.[NOM]</ta>
            <ta e="T230" id="Seg_5112" s="T229">birch-PL.[NOM]</ta>
            <ta e="T231" id="Seg_5113" s="T230">poplar-PL.[NOM]</ta>
            <ta e="T232" id="Seg_5114" s="T231">fir.tree-PL.[NOM]</ta>
            <ta e="T233" id="Seg_5115" s="T232">pine_tree-PL.[NOM]</ta>
            <ta e="T234" id="Seg_5116" s="T233">bird.cherry-GEN-tree-PL.[NOM]</ta>
            <ta e="T235" id="Seg_5117" s="T234">aspen-PL.[NOM]</ta>
            <ta e="T236" id="Seg_5118" s="T235">here-ADV.LOC</ta>
            <ta e="T237" id="Seg_5119" s="T236">summer-EP-ADVZ</ta>
            <ta e="T238" id="Seg_5120" s="T237">much-ADVZ</ta>
            <ta e="T239" id="Seg_5121" s="T238">mushrooms-PL.[NOM]</ta>
            <ta e="T240" id="Seg_5122" s="T239">be.born-HAB-3SG.S</ta>
            <ta e="T241" id="Seg_5123" s="T240">we.[NOM]</ta>
            <ta e="T242" id="Seg_5124" s="T241">summer-EP-ADVZ</ta>
            <ta e="T243" id="Seg_5125" s="T242">mushrooms.[NOM]</ta>
            <ta e="T244" id="Seg_5126" s="T243">gather-IPFV-FUT-1PL</ta>
            <ta e="T245" id="Seg_5127" s="T244">in.spring</ta>
            <ta e="T246" id="Seg_5128" s="T245">much-ADVZ</ta>
            <ta e="T247" id="Seg_5129" s="T246">be-HAB-CO-3PL</ta>
            <ta e="T248" id="Seg_5130" s="T247">flower-EP-PL.[NOM]</ta>
            <ta e="T249" id="Seg_5131" s="T248">winter-ADVZ</ta>
            <ta e="T250" id="Seg_5132" s="T249">ski-EP-INSTR</ta>
            <ta e="T251" id="Seg_5133" s="T250">walk-3PL</ta>
            <ta e="T252" id="Seg_5134" s="T251">child-PL.[NOM]</ta>
            <ta e="T253" id="Seg_5135" s="T252">sled-INSTR</ta>
            <ta e="T254" id="Seg_5136" s="T253">roll-EP-FRQ-CO-3PL</ta>
            <ta e="T255" id="Seg_5137" s="T254">autumn.[NOM]</ta>
            <ta e="T256" id="Seg_5138" s="T255">year-ADV.LOC</ta>
            <ta e="T257" id="Seg_5139" s="T256">leaf-PL.[NOM]</ta>
            <ta e="T258" id="Seg_5140" s="T257">all</ta>
            <ta e="T259" id="Seg_5141" s="T258">yellow-%%</ta>
            <ta e="T260" id="Seg_5142" s="T259">become-INCH-FUT-3PL</ta>
            <ta e="T261" id="Seg_5143" s="T260">when</ta>
            <ta e="T262" id="Seg_5144" s="T261">we.[NOM]</ta>
            <ta e="T263" id="Seg_5145" s="T262">come-PST-1PL</ta>
            <ta e="T264" id="Seg_5146" s="T263">Novosibirsk-EP-ILL</ta>
            <ta e="T265" id="Seg_5147" s="T264">overnight-PST-1PL</ta>
            <ta e="T266" id="Seg_5148" s="T265">Novosibirsk-EP-LOC</ta>
            <ta e="T267" id="Seg_5149" s="T266">station-LOC</ta>
            <ta e="T268" id="Seg_5150" s="T267">up-ADJZ</ta>
            <ta e="T269" id="Seg_5151" s="T268">floor-LOC</ta>
            <ta e="T270" id="Seg_5152" s="T269">overnight-PST-1PL</ta>
            <ta e="T271" id="Seg_5153" s="T270">five-ORD</ta>
            <ta e="T272" id="Seg_5154" s="T271">floor-LOC</ta>
            <ta e="T273" id="Seg_5155" s="T272">sleep-PST-1PL</ta>
            <ta e="T274" id="Seg_5156" s="T273">we.PL.ALL</ta>
            <ta e="T275" id="Seg_5157" s="T274">all</ta>
            <ta e="T276" id="Seg_5158" s="T275">give-CO-3PL</ta>
            <ta e="T277" id="Seg_5159" s="T276">mattress-PL-ACC</ta>
            <ta e="T278" id="Seg_5160" s="T277">blanket-PL-ACC</ta>
            <ta e="T279" id="Seg_5161" s="T278">pillow-PL-ACC</ta>
            <ta e="T280" id="Seg_5162" s="T279">sleep-PST-1PL</ta>
            <ta e="T281" id="Seg_5163" s="T280">good-ADVZ</ta>
            <ta e="T282" id="Seg_5164" s="T281">human.being-PL.[NOM]</ta>
            <ta e="T283" id="Seg_5165" s="T282">much-ADVZ</ta>
            <ta e="T284" id="Seg_5166" s="T283">be-PST-3PL</ta>
            <ta e="T285" id="Seg_5167" s="T284">big</ta>
            <ta e="T286" id="Seg_5168" s="T285">station.[NOM]</ta>
            <ta e="T287" id="Seg_5169" s="T286">there-ADV.LOC</ta>
            <ta e="T288" id="Seg_5170" s="T287">get.lost-POT.FUT.2SG</ta>
            <ta e="T289" id="Seg_5171" s="T288">NEG</ta>
            <ta e="T290" id="Seg_5172" s="T289">go.out-POT.FUT.2SG</ta>
            <ta e="T291" id="Seg_5173" s="T290">morning-something-LOC</ta>
            <ta e="T292" id="Seg_5174" s="T291">leave-CO-1PL</ta>
            <ta e="T293" id="Seg_5175" s="T292">canteen-ILL</ta>
            <ta e="T294" id="Seg_5176" s="T293">(s)he-PL.[NOM]</ta>
            <ta e="T295" id="Seg_5177" s="T294">say-HAB-3PL</ta>
            <ta e="T296" id="Seg_5178" s="T295">restaurant.[NOM]</ta>
            <ta e="T297" id="Seg_5179" s="T296">there-ADV.LOC</ta>
            <ta e="T298" id="Seg_5180" s="T297">good-ADVZ</ta>
            <ta e="T299" id="Seg_5181" s="T298">be-3SG.S</ta>
            <ta e="T300" id="Seg_5182" s="T299">paradise-EP-GEN</ta>
            <ta e="T301" id="Seg_5183" s="T300">inside.[NOM]</ta>
            <ta e="T302" id="Seg_5184" s="T301">there-ADV.LOC</ta>
            <ta e="T303" id="Seg_5185" s="T302">and</ta>
            <ta e="T304" id="Seg_5186" s="T303">eat-FRQ-PST-1PL</ta>
            <ta e="T305" id="Seg_5187" s="T304">we-ADES</ta>
            <ta e="T306" id="Seg_5188" s="T305">sea.[NOM]</ta>
            <ta e="T307" id="Seg_5189" s="T306">near-EP-ADV.LOC</ta>
            <ta e="T308" id="Seg_5190" s="T307">be-3SG.S</ta>
            <ta e="T309" id="Seg_5191" s="T308">there-ADV.LOC</ta>
            <ta e="T310" id="Seg_5192" s="T309">human.being-PL.[NOM]</ta>
            <ta e="T311" id="Seg_5193" s="T310">fish-ADJZ-CO-3PL</ta>
            <ta e="T312" id="Seg_5194" s="T311">fishing.rod-INSTR</ta>
            <ta e="T313" id="Seg_5195" s="T312">ruff-PL.[NOM]</ta>
            <ta e="T314" id="Seg_5196" s="T313">catch-HAB-INFER-3PL</ta>
            <ta e="T315" id="Seg_5197" s="T314">and</ta>
            <ta e="T316" id="Seg_5198" s="T315">roach-PL.[NOM]</ta>
            <ta e="T317" id="Seg_5199" s="T316">small-girl-DIM.[NOM]</ta>
            <ta e="T318" id="Seg_5200" s="T317">hostess-GEN</ta>
            <ta e="T319" id="Seg_5201" s="T318">daughter-DIM.[NOM]-3SG</ta>
            <ta e="T320" id="Seg_5202" s="T319">bring-HAB-PST-3SG.O</ta>
            <ta e="T321" id="Seg_5203" s="T320">ruff-ACC</ta>
            <ta e="T322" id="Seg_5204" s="T321">bring-PST-3SG.O</ta>
            <ta e="T323" id="Seg_5205" s="T322">ruff.[NOM]</ta>
            <ta e="T324" id="Seg_5206" s="T323">freeze-RFL-PST.NAR.[3SG.S]</ta>
            <ta e="T325" id="Seg_5207" s="T324">cat.[NOM]</ta>
            <ta e="T326" id="Seg_5208" s="T325">(s)he-EP-ACC</ta>
            <ta e="T327" id="Seg_5209" s="T326">eat-PST-3SG.O</ta>
            <ta e="T328" id="Seg_5210" s="T327">I.NOM-ADES</ta>
            <ta e="T329" id="Seg_5211" s="T328">two</ta>
            <ta e="T330" id="Seg_5212" s="T329">female.friend.[NOM]-1SG</ta>
            <ta e="T331" id="Seg_5213" s="T330">be-CO.[3SG.S]</ta>
            <ta e="T332" id="Seg_5214" s="T331">I.NOM</ta>
            <ta e="T333" id="Seg_5215" s="T332">(s)he-PL-ALL</ta>
            <ta e="T334" id="Seg_5216" s="T333">walk-HAB-CO-1SG.S</ta>
            <ta e="T335" id="Seg_5217" s="T334">and</ta>
            <ta e="T336" id="Seg_5218" s="T335">(s)he-PL.[NOM]</ta>
            <ta e="T337" id="Seg_5219" s="T336">I.ALL</ta>
            <ta e="T338" id="Seg_5220" s="T337">walk-HAB-CO-3PL</ta>
            <ta e="T339" id="Seg_5221" s="T338">I.NOM</ta>
            <ta e="T340" id="Seg_5222" s="T339">knit-FUT-1SG.S</ta>
            <ta e="T341" id="Seg_5223" s="T340">net-PL-ADJZ</ta>
            <ta e="T342" id="Seg_5224" s="T341">simply</ta>
            <ta e="T343" id="Seg_5225" s="T342">sit-INF</ta>
            <ta e="T344" id="Seg_5226" s="T343">NEG</ta>
            <ta e="T345" id="Seg_5227" s="T344">want-1SG.S</ta>
            <ta e="T346" id="Seg_5228" s="T345">and</ta>
            <ta e="T347" id="Seg_5229" s="T346">oneself.1SG</ta>
            <ta e="T348" id="Seg_5230" s="T347">all.the.time</ta>
            <ta e="T349" id="Seg_5231" s="T348">song-VBLZ-HAB-1SG.S</ta>
            <ta e="T350" id="Seg_5232" s="T349">I.NOM</ta>
            <ta e="T351" id="Seg_5233" s="T350">in.spring</ta>
            <ta e="T352" id="Seg_5234" s="T351">Starosondorovo-ILL</ta>
            <ta e="T353" id="Seg_5235" s="T352">come-FUT-1SG.S</ta>
            <ta e="T354" id="Seg_5236" s="T353">fish-CAP-INF</ta>
            <ta e="T355" id="Seg_5237" s="T354">berry-EP-ACC</ta>
            <ta e="T356" id="Seg_5238" s="T355">manage.to.get-EP-INF</ta>
            <ta e="T357" id="Seg_5239" s="T356">city.[NOM]</ta>
            <ta e="T358" id="Seg_5240" s="T357">big-ADVZ</ta>
            <ta e="T359" id="Seg_5241" s="T358">be-3SG.S</ta>
            <ta e="T360" id="Seg_5242" s="T359">I.NOM</ta>
            <ta e="T361" id="Seg_5243" s="T360">here</ta>
            <ta e="T362" id="Seg_5244" s="T361">come-INFER-FUT-1SG.S</ta>
            <ta e="T363" id="Seg_5245" s="T362">and</ta>
            <ta e="T364" id="Seg_5246" s="T363">you.ALL</ta>
            <ta e="T365" id="Seg_5247" s="T364">be.visible-TR-FUT-1SG.O</ta>
            <ta e="T366" id="Seg_5248" s="T365">city-ACC</ta>
            <ta e="T367" id="Seg_5249" s="T366">you.ALL</ta>
            <ta e="T368" id="Seg_5250" s="T367">this</ta>
            <ta e="T369" id="Seg_5251" s="T368">city-ACC</ta>
            <ta e="T370" id="Seg_5252" s="T369">bring-FUT-1SG.O</ta>
            <ta e="T371" id="Seg_5253" s="T370">you.SG.NOM</ta>
            <ta e="T372" id="Seg_5254" s="T371">look-DUR-FUT-2SG.O</ta>
            <ta e="T373" id="Seg_5255" s="T372">here-ADV.LOC</ta>
            <ta e="T374" id="Seg_5256" s="T373">any</ta>
            <ta e="T375" id="Seg_5257" s="T374">car-PL-INSTR</ta>
            <ta e="T376" id="Seg_5258" s="T375">walk-PST-1SG.S</ta>
            <ta e="T379" id="Seg_5259" s="T378">get.afraid-DUR-PST-1SG.S</ta>
            <ta e="T380" id="Seg_5260" s="T379">I.ACC</ta>
            <ta e="T381" id="Seg_5261" s="T380">Volodya.[NOM]</ta>
            <ta e="T382" id="Seg_5262" s="T381">city-LOC</ta>
            <ta e="T383" id="Seg_5263" s="T382">guide-PST.[NOM]</ta>
            <ta e="T384" id="Seg_5264" s="T383">all</ta>
            <ta e="T385" id="Seg_5265" s="T384">be.visible-TR-HAB-PST-3SG.O</ta>
            <ta e="T386" id="Seg_5266" s="T385">oneself.3SG</ta>
            <ta e="T387" id="Seg_5267" s="T386">EMPH</ta>
            <ta e="T388" id="Seg_5268" s="T387">where</ta>
            <ta e="T389" id="Seg_5269" s="T388">leave-POT.FUT.2SG</ta>
            <ta e="T390" id="Seg_5270" s="T389">get.lost-POT.FUT.2SG</ta>
            <ta e="T391" id="Seg_5271" s="T390">I.NOM</ta>
            <ta e="T392" id="Seg_5272" s="T391">piss-TEMPN-DRV-CO-1SG.S</ta>
            <ta e="T393" id="Seg_5273" s="T392">say-1SG.S</ta>
            <ta e="T394" id="Seg_5274" s="T393">piss-INF</ta>
            <ta e="T395" id="Seg_5275" s="T394">one.should</ta>
            <ta e="T396" id="Seg_5276" s="T395">and</ta>
            <ta e="T397" id="Seg_5277" s="T396">Volodya.[NOM]</ta>
            <ta e="T398" id="Seg_5278" s="T397">I.ALL</ta>
            <ta e="T399" id="Seg_5279" s="T398">say-3SG.S</ta>
            <ta e="T400" id="Seg_5280" s="T399">where</ta>
            <ta e="T401" id="Seg_5281" s="T400">piss-POT.FUT.2SG</ta>
            <ta e="T402" id="Seg_5282" s="T401">piss-PTCP.PST</ta>
            <ta e="T403" id="Seg_5283" s="T402">something.[NOM]</ta>
            <ta e="T404" id="Seg_5284" s="T403">NEG.EX.[3SG.S]</ta>
            <ta e="T405" id="Seg_5285" s="T404">piss-PTCP.PST</ta>
            <ta e="T406" id="Seg_5286" s="T405">something-PL.[NOM]</ta>
            <ta e="T407" id="Seg_5287" s="T406">all</ta>
            <ta e="T408" id="Seg_5288" s="T407">house-EP-GEN</ta>
            <ta e="T409" id="Seg_5289" s="T408">inside-LOC</ta>
            <ta e="T410" id="Seg_5290" s="T409">be-CO-3PL</ta>
            <ta e="T411" id="Seg_5291" s="T410">market-LOC</ta>
            <ta e="T412" id="Seg_5292" s="T411">be-PST-1DU</ta>
            <ta e="T413" id="Seg_5293" s="T412">(s)he.[NOM]</ta>
            <ta e="T414" id="Seg_5294" s="T413">I.ALL</ta>
            <ta e="T415" id="Seg_5295" s="T414">say-3SG.S</ta>
            <ta e="T416" id="Seg_5296" s="T415">there-ADV.LOC-%%</ta>
            <ta e="T417" id="Seg_5297" s="T416">what-TRL</ta>
            <ta e="T418" id="Seg_5298" s="T417">NEG</ta>
            <ta e="T419" id="Seg_5299" s="T418">piss-PST-2SG.S</ta>
            <ta e="T420" id="Seg_5300" s="T419">and</ta>
            <ta e="T421" id="Seg_5301" s="T420">I.NOM</ta>
            <ta e="T422" id="Seg_5302" s="T421">NEG</ta>
            <ta e="T423" id="Seg_5303" s="T422">piss-PST-1SG.S</ta>
            <ta e="T424" id="Seg_5304" s="T423">so</ta>
            <ta e="T425" id="Seg_5305" s="T424">I.NOM</ta>
            <ta e="T426" id="Seg_5306" s="T425">endure-DUR-PST-1SG.O</ta>
            <ta e="T427" id="Seg_5307" s="T426">when</ta>
            <ta e="T428" id="Seg_5308" s="T427">car-ACC</ta>
            <ta e="T429" id="Seg_5309" s="T428">wait-PST-1DU</ta>
            <ta e="T430" id="Seg_5310" s="T429">I.NOM</ta>
            <ta e="T431" id="Seg_5311" s="T430">one</ta>
            <ta e="T432" id="Seg_5312" s="T431">woman-ADJZ-human.being-ALL</ta>
            <ta e="T433" id="Seg_5313" s="T432">ask-1SG.S</ta>
            <ta e="T434" id="Seg_5314" s="T433">where</ta>
            <ta e="T435" id="Seg_5315" s="T434">piss-PTCP.PST</ta>
            <ta e="T436" id="Seg_5316" s="T435">house.[NOM]</ta>
            <ta e="T437" id="Seg_5317" s="T436">(s)he.[NOM]</ta>
            <ta e="T438" id="Seg_5318" s="T437">I.ALL</ta>
            <ta e="T439" id="Seg_5319" s="T438">be.visible-TR-3SG.O</ta>
            <ta e="T440" id="Seg_5320" s="T439">%there</ta>
            <ta e="T441" id="Seg_5321" s="T440">I.NOM</ta>
            <ta e="T442" id="Seg_5322" s="T441">go-MOM-CO-1SG.S</ta>
            <ta e="T443" id="Seg_5323" s="T442">and</ta>
            <ta e="T444" id="Seg_5324" s="T443">piss-1SG.S</ta>
            <ta e="T445" id="Seg_5325" s="T444">I.NOM</ta>
            <ta e="T446" id="Seg_5326" s="T445">think-PST-1SG.S</ta>
            <ta e="T447" id="Seg_5327" s="T446">urine.[NOM]-1SG</ta>
            <ta e="T448" id="Seg_5328" s="T447">separately</ta>
            <ta e="T449" id="Seg_5329" s="T448">get.up-3SG.S</ta>
            <ta e="T450" id="Seg_5330" s="T449">piss-1SG.S</ta>
            <ta e="T451" id="Seg_5331" s="T450">and</ta>
            <ta e="T452" id="Seg_5332" s="T451">good</ta>
            <ta e="T453" id="Seg_5333" s="T452">become-3SG.S</ta>
            <ta e="T454" id="Seg_5334" s="T453">shop-PL.[NOM]</ta>
            <ta e="T455" id="Seg_5335" s="T454">come.in-HAB-1DU</ta>
            <ta e="T456" id="Seg_5336" s="T455">big</ta>
            <ta e="T457" id="Seg_5337" s="T456">shop-PL.[NOM]</ta>
            <ta e="T458" id="Seg_5338" s="T457">there-ADV.LOC</ta>
            <ta e="T459" id="Seg_5339" s="T458">get.lost-POT.FUT.2SG</ta>
            <ta e="T460" id="Seg_5340" s="T459">I.NOM</ta>
            <ta e="T461" id="Seg_5341" s="T460">writing-EP-ACC</ta>
            <ta e="T462" id="Seg_5342" s="T461">NEG</ta>
            <ta e="T463" id="Seg_5343" s="T462">can-1SG.O</ta>
            <ta e="T464" id="Seg_5344" s="T463">oneself.1SG</ta>
            <ta e="T465" id="Seg_5345" s="T464">NEG</ta>
            <ta e="T466" id="Seg_5346" s="T465">walk-HAB-CO-1SG.S</ta>
            <ta e="T467" id="Seg_5347" s="T466">city-LOC</ta>
            <ta e="T468" id="Seg_5348" s="T467">otherwise</ta>
            <ta e="T469" id="Seg_5349" s="T468">get.lost-FUT-1SG.S</ta>
            <ta e="T470" id="Seg_5350" s="T469">I.NOM</ta>
            <ta e="T471" id="Seg_5351" s="T470">now</ta>
            <ta e="T472" id="Seg_5352" s="T471">car-PL-INSTR</ta>
            <ta e="T473" id="Seg_5353" s="T472">walk-1SG.S</ta>
            <ta e="T474" id="Seg_5354" s="T473">live-PST-1SG.S</ta>
            <ta e="T475" id="Seg_5355" s="T474">city-LOC</ta>
            <ta e="T476" id="Seg_5356" s="T475">Chornaja_rechka-LOC</ta>
            <ta e="T477" id="Seg_5357" s="T476">and</ta>
            <ta e="T478" id="Seg_5358" s="T477">Taxtamyshevo-LOC</ta>
            <ta e="T479" id="Seg_5359" s="T478">live-PST-1SG.S</ta>
            <ta e="T480" id="Seg_5360" s="T479">Dunya.[NOM]</ta>
            <ta e="T481" id="Seg_5361" s="T480">come-PST.[3SG.S]</ta>
            <ta e="T482" id="Seg_5362" s="T481">Chornaja_rechka-ILL</ta>
            <ta e="T483" id="Seg_5363" s="T482">son-OBL.3SG-COM</ta>
            <ta e="T484" id="Seg_5364" s="T483">(s)he-EP-DU.[NOM]</ta>
            <ta e="T485" id="Seg_5365" s="T484">live-3DU.S</ta>
            <ta e="T486" id="Seg_5366" s="T485">Nikolay.[NOM]</ta>
            <ta e="T487" id="Seg_5367" s="T486">Lazarych-EP-GEN</ta>
            <ta e="T488" id="Seg_5368" s="T487">wife-DIM.[NOM]-3SG</ta>
            <ta e="T489" id="Seg_5369" s="T488">die-CO.[3SG.S]</ta>
            <ta e="T490" id="Seg_5370" s="T489">Chornaja_rechka-LOC</ta>
            <ta e="T491" id="Seg_5371" s="T490">live-PST.[3SG.S]</ta>
            <ta e="T492" id="Seg_5372" s="T491">daughter-OBL.3SG-ADES</ta>
            <ta e="T493" id="Seg_5373" s="T492">and</ta>
            <ta e="T494" id="Seg_5374" s="T493">I.NOM</ta>
            <ta e="T495" id="Seg_5375" s="T494">leave-CO-1SG.S</ta>
            <ta e="T496" id="Seg_5376" s="T495">city-PL-ACC</ta>
            <ta e="T497" id="Seg_5377" s="T496">look-DUR-INF</ta>
            <ta e="T498" id="Seg_5378" s="T497">Marina.[NOM]</ta>
            <ta e="T499" id="Seg_5379" s="T498">Pawlowna.[NOM]</ta>
            <ta e="T500" id="Seg_5380" s="T499">how</ta>
            <ta e="T501" id="Seg_5381" s="T500">I.NOM</ta>
            <ta e="T502" id="Seg_5382" s="T501">letter.[NOM]-1SG</ta>
            <ta e="T503" id="Seg_5383" s="T502">come-FUT-3SG.S</ta>
            <ta e="T504" id="Seg_5384" s="T503">and</ta>
            <ta e="T505" id="Seg_5385" s="T504">I.ALL</ta>
            <ta e="T506" id="Seg_5386" s="T505">letter-EP-ACC</ta>
            <ta e="T507" id="Seg_5387" s="T506">at.once</ta>
            <ta e="T508" id="Seg_5388" s="T507">letter-IMP.2SG.O</ta>
            <ta e="T509" id="Seg_5389" s="T508">I.NOM</ta>
            <ta e="T510" id="Seg_5390" s="T509">wait-TR-FUT-1SG.S</ta>
            <ta e="T511" id="Seg_5391" s="T510">all</ta>
            <ta e="T512" id="Seg_5392" s="T511">write-IMP.2SG.O</ta>
            <ta e="T513" id="Seg_5393" s="T512">how</ta>
            <ta e="T514" id="Seg_5394" s="T513">I.GEN</ta>
            <ta e="T515" id="Seg_5395" s="T514">human.being-PL.[NOM]-1SG</ta>
            <ta e="T516" id="Seg_5396" s="T515">live-3PL</ta>
            <ta e="T517" id="Seg_5397" s="T516">Kolya-GEN</ta>
            <ta e="T518" id="Seg_5398" s="T517">about</ta>
            <ta e="T519" id="Seg_5399" s="T518">what-ADJZ-EP-something-ACC</ta>
            <ta e="T520" id="Seg_5400" s="T519">hear-TR-HAB-2SG.O</ta>
            <ta e="T521" id="Seg_5401" s="T520">or</ta>
            <ta e="T522" id="Seg_5402" s="T521">NEG</ta>
            <ta e="T523" id="Seg_5403" s="T522">all</ta>
            <ta e="T524" id="Seg_5404" s="T523">I.ALL</ta>
            <ta e="T525" id="Seg_5405" s="T524">write-EP-IMP.2SG.O</ta>
            <ta e="T526" id="Seg_5406" s="T525">I.NOM</ta>
            <ta e="T527" id="Seg_5407" s="T526">well</ta>
            <ta e="T528" id="Seg_5408" s="T527">wait-TR-FUT-1SG.S</ta>
            <ta e="T529" id="Seg_5409" s="T528">you.PL.ALL</ta>
            <ta e="T530" id="Seg_5410" s="T529">bread-INSTR</ta>
            <ta e="T531" id="Seg_5411" s="T530">give-HAB-CO-3PL</ta>
            <ta e="T532" id="Seg_5412" s="T531">or</ta>
            <ta e="T533" id="Seg_5413" s="T532">flour-INSTR</ta>
            <ta e="T534" id="Seg_5414" s="T533">give-HAB-CO-3PL</ta>
            <ta e="T535" id="Seg_5415" s="T534">and</ta>
            <ta e="T536" id="Seg_5416" s="T535">shop-LOC</ta>
            <ta e="T537" id="Seg_5417" s="T536">what-PL.[NOM]</ta>
            <ta e="T538" id="Seg_5418" s="T537">be-INFER-3PL</ta>
            <ta e="T539" id="Seg_5419" s="T538">all</ta>
            <ta e="T540" id="Seg_5420" s="T539">letter-IMP.2SG.O</ta>
            <ta e="T541" id="Seg_5421" s="T540">son_in_law.[NOM]-2SG</ta>
            <ta e="T542" id="Seg_5422" s="T541">either</ta>
            <ta e="T543" id="Seg_5423" s="T542">live-3SG.S</ta>
            <ta e="T544" id="Seg_5424" s="T543">or</ta>
            <ta e="T545" id="Seg_5425" s="T544">NEG</ta>
            <ta e="T546" id="Seg_5426" s="T545">say-PST-3PL</ta>
            <ta e="T547" id="Seg_5427" s="T546">away</ta>
            <ta e="T548" id="Seg_5428" s="T547">leave-PST.NAR.[3SG.S]</ta>
            <ta e="T549" id="Seg_5429" s="T548">Zinka-GEN</ta>
            <ta e="T550" id="Seg_5430" s="T549">about</ta>
            <ta e="T551" id="Seg_5431" s="T550">write-IMP.2SG.O</ta>
            <ta e="T552" id="Seg_5432" s="T551">how</ta>
            <ta e="T553" id="Seg_5433" s="T552">live-HAB-3SG.S</ta>
            <ta e="T554" id="Seg_5434" s="T553">live-HAB-3SG.S</ta>
            <ta e="T555" id="Seg_5435" s="T554">I.NOM</ta>
            <ta e="T556" id="Seg_5436" s="T555">(s)he-ALL</ta>
            <ta e="T557" id="Seg_5437" s="T556">letter-EP-ACC</ta>
            <ta e="T558" id="Seg_5438" s="T557">write-PST-1SG.S</ta>
            <ta e="T559" id="Seg_5439" s="T558">(s)he.[NOM]</ta>
            <ta e="T560" id="Seg_5440" s="T559">I.ALL</ta>
            <ta e="T561" id="Seg_5441" s="T560">NEG</ta>
            <ta e="T562" id="Seg_5442" s="T561">write-HAB-3SG.O</ta>
            <ta e="T563" id="Seg_5443" s="T562">aunt.[NOM]</ta>
            <ta e="T564" id="Seg_5444" s="T563">Marina.[NOM]</ta>
            <ta e="T565" id="Seg_5445" s="T564">first</ta>
            <ta e="T566" id="Seg_5446" s="T565">May-INSTR</ta>
            <ta e="T567" id="Seg_5447" s="T566">holiday-INSTR</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_5448" s="T1">мы.[NOM]</ta>
            <ta e="T3" id="Seg_5449" s="T2">этот-день.[NOM]</ta>
            <ta e="T4" id="Seg_5450" s="T3">сидеть-1DU</ta>
            <ta e="T5" id="Seg_5451" s="T4">Ангелина.[NOM]</ta>
            <ta e="T6" id="Seg_5452" s="T5">Ивановна-COM</ta>
            <ta e="T7" id="Seg_5453" s="T6">говорить-1DU</ta>
            <ta e="T8" id="Seg_5454" s="T7">ты.GEN</ta>
            <ta e="T9" id="Seg_5455" s="T8">про</ta>
            <ta e="T10" id="Seg_5456" s="T9">говорить-1DU</ta>
            <ta e="T11" id="Seg_5457" s="T10">как</ta>
            <ta e="T12" id="Seg_5458" s="T11">жить-2SG.S</ta>
            <ta e="T13" id="Seg_5459" s="T12">что-PL.[NOM]</ta>
            <ta e="T14" id="Seg_5460" s="T13">сделать-HAB-INFER-2SG.O</ta>
            <ta e="T15" id="Seg_5461" s="T14">я.NOM</ta>
            <ta e="T16" id="Seg_5462" s="T15">он(а)-ALL</ta>
            <ta e="T17" id="Seg_5463" s="T16">сказать-CVB</ta>
            <ta e="T18" id="Seg_5464" s="T17">принести-EP-FRQ-CO-1SG.O</ta>
            <ta e="T19" id="Seg_5465" s="T18">ты.GEN</ta>
            <ta e="T20" id="Seg_5466" s="T19">про</ta>
            <ta e="T21" id="Seg_5467" s="T20">я.NOM</ta>
            <ta e="T22" id="Seg_5468" s="T21">сказать-1SG.S</ta>
            <ta e="T23" id="Seg_5469" s="T22">он(а).[NOM]</ta>
            <ta e="T24" id="Seg_5470" s="T23">старуха-TRL-PST.NAR.[3SG.S]</ta>
            <ta e="T25" id="Seg_5471" s="T24">я.NOM</ta>
            <ta e="T26" id="Seg_5472" s="T25">он(а)-ALL</ta>
            <ta e="T27" id="Seg_5473" s="T26">сказать-PST-1SG.O</ta>
            <ta e="T28" id="Seg_5474" s="T27">ты.NOM</ta>
            <ta e="T29" id="Seg_5475" s="T28">мы.ACC</ta>
            <ta e="T30" id="Seg_5476" s="T29">Мария-COM</ta>
            <ta e="T31" id="Seg_5477" s="T30">еда-VBLZ-PST-3SG.O</ta>
            <ta e="T32" id="Seg_5478" s="T31">мы.[NOM]</ta>
            <ta e="T33" id="Seg_5479" s="T32">ты-COM</ta>
            <ta e="T34" id="Seg_5480" s="T33">как</ta>
            <ta e="T35" id="Seg_5481" s="T34">пить-PST-1PL</ta>
            <ta e="T36" id="Seg_5482" s="T35">я.NOM</ta>
            <ta e="T37" id="Seg_5483" s="T36">он(а)-ALL</ta>
            <ta e="T38" id="Seg_5484" s="T37">все</ta>
            <ta e="T39" id="Seg_5485" s="T38">сказать-PST-1SG.O</ta>
            <ta e="T40" id="Seg_5486" s="T39">я.NOM</ta>
            <ta e="T41" id="Seg_5487" s="T40">говорить-PST-1SG.S</ta>
            <ta e="T42" id="Seg_5488" s="T41">тетка.[NOM]</ta>
            <ta e="T43" id="Seg_5489" s="T42">Марина.[NOM]</ta>
            <ta e="T44" id="Seg_5490" s="T43">письмо.[NOM]</ta>
            <ta e="T45" id="Seg_5491" s="T44">письмо-VBLZ-FUT-1SG.S</ta>
            <ta e="T46" id="Seg_5492" s="T45">JUSS</ta>
            <ta e="T47" id="Seg_5493" s="T46">читать-PFV-IMP.3SG/PL.O</ta>
            <ta e="T48" id="Seg_5494" s="T47">здравствуй</ta>
            <ta e="T49" id="Seg_5495" s="T48">Марина.[NOM]</ta>
            <ta e="T50" id="Seg_5496" s="T49">Павловна.[NOM]</ta>
            <ta e="T51" id="Seg_5497" s="T50">я.NOM</ta>
            <ta e="T52" id="Seg_5498" s="T51">ты.ALL</ta>
            <ta e="T53" id="Seg_5499" s="T52">селькупский</ta>
            <ta e="T54" id="Seg_5500" s="T53">письмо.[NOM]</ta>
            <ta e="T55" id="Seg_5501" s="T54">письмо-VBLZ-FUT-1SG.S</ta>
            <ta e="T56" id="Seg_5502" s="T55">русский-язык-ADVZ</ta>
            <ta e="T57" id="Seg_5503" s="T56">я.NOM</ta>
            <ta e="T58" id="Seg_5504" s="T57">NEG</ta>
            <ta e="T59" id="Seg_5505" s="T58">уметь-1SG.S</ta>
            <ta e="T60" id="Seg_5506" s="T59">я.NOM</ta>
            <ta e="T61" id="Seg_5507" s="T60">жить-1SG.S</ta>
            <ta e="T62" id="Seg_5508" s="T61">теперь</ta>
            <ta e="T63" id="Seg_5509" s="T62">город-LOC</ta>
            <ta e="T64" id="Seg_5510" s="T63">Новосибирск-EP-LOC</ta>
            <ta e="T65" id="Seg_5511" s="T64">город.[NOM]</ta>
            <ta e="T66" id="Seg_5512" s="T65">большой</ta>
            <ta e="T67" id="Seg_5513" s="T66">хороший-DRV</ta>
            <ta e="T68" id="Seg_5514" s="T67">город-EL.3SG</ta>
            <ta e="T69" id="Seg_5515" s="T68">Новосибирск-EP-ILL</ta>
            <ta e="T70" id="Seg_5516" s="T69">поезд-INSTR</ta>
            <ta e="T71" id="Seg_5517" s="T70">ходить-PST-1SG.S</ta>
            <ta e="T72" id="Seg_5518" s="T71">поезд-LOC</ta>
            <ta e="T73" id="Seg_5519" s="T72">ходить-CVB</ta>
            <ta e="T74" id="Seg_5520" s="T73">карта-VBLZ-PST-1PL</ta>
            <ta e="T75" id="Seg_5521" s="T74">Таёжный.[NOM]</ta>
            <ta e="T76" id="Seg_5522" s="T75">станция-LOC</ta>
            <ta e="T77" id="Seg_5523" s="T76">поезд.[NOM]</ta>
            <ta e="T78" id="Seg_5524" s="T77">долго-ADV.LOC</ta>
            <ta e="T79" id="Seg_5525" s="T78">стоять-HAB-PST.[3SG.S]</ta>
            <ta e="T80" id="Seg_5526" s="T79">я.NOM</ta>
            <ta e="T81" id="Seg_5527" s="T80">посмотреть-DUR-PST-1SG.S</ta>
            <ta e="T82" id="Seg_5528" s="T81">и</ta>
            <ta e="T83" id="Seg_5529" s="T82">Никин-EP-ACC</ta>
            <ta e="T84" id="Seg_5530" s="T83">что-LOC</ta>
            <ta e="T85" id="Seg_5531" s="T84">NEG</ta>
            <ta e="T86" id="Seg_5532" s="T85">увидеть-DRV-PST-1SG.O</ta>
            <ta e="T87" id="Seg_5533" s="T86">потом</ta>
            <ta e="T88" id="Seg_5534" s="T87">поезд.[NOM]</ta>
            <ta e="T89" id="Seg_5535" s="T88">отправиться-3SG.S</ta>
            <ta e="T90" id="Seg_5536" s="T89">я.NOM</ta>
            <ta e="T91" id="Seg_5537" s="T90">все.время</ta>
            <ta e="T92" id="Seg_5538" s="T91">окно-LOC</ta>
            <ta e="T93" id="Seg_5539" s="T92">посмотреть-DUR-PST-1SG.S</ta>
            <ta e="T94" id="Seg_5540" s="T93">деревня-PL.[NOM]</ta>
            <ta e="T95" id="Seg_5541" s="T94">много-ADVZ</ta>
            <ta e="T96" id="Seg_5542" s="T95">остаться-CVB</ta>
            <ta e="T97" id="Seg_5543" s="T96">ходить-PST-3PL</ta>
            <ta e="T98" id="Seg_5544" s="T97">город-GEN</ta>
            <ta e="T99" id="Seg_5545" s="T98">край-LOC</ta>
            <ta e="T100" id="Seg_5546" s="T99">дерево-PL.[NOM]</ta>
            <ta e="T101" id="Seg_5547" s="T100">тайга-EP-PL.[NOM]</ta>
            <ta e="T102" id="Seg_5548" s="T101">кругом</ta>
            <ta e="T103" id="Seg_5549" s="T102">снег.[NOM]</ta>
            <ta e="T104" id="Seg_5550" s="T103">лежать-INFER.[3SG.S]</ta>
            <ta e="T105" id="Seg_5551" s="T104">я.NOM</ta>
            <ta e="T106" id="Seg_5552" s="T105">поезд-LOC</ta>
            <ta e="T107" id="Seg_5553" s="T106">NEG</ta>
            <ta e="T108" id="Seg_5554" s="T107">испугаться-DUR-PST-1SG.S</ta>
            <ta e="T109" id="Seg_5555" s="T108">маленький-ADVZ</ta>
            <ta e="T110" id="Seg_5556" s="T109">качать-EP-3SG.O</ta>
            <ta e="T111" id="Seg_5557" s="T110">раньше</ta>
            <ta e="T112" id="Seg_5558" s="T111">я.NOM</ta>
            <ta e="T113" id="Seg_5559" s="T112">NEG</ta>
            <ta e="T114" id="Seg_5560" s="T113">ходить-HAB-PST-1SG.S</ta>
            <ta e="T115" id="Seg_5561" s="T114">поезд-PL-INSTR</ta>
            <ta e="T116" id="Seg_5562" s="T115">я.NOM</ta>
            <ta e="T117" id="Seg_5563" s="T116">жить-1SG.S</ta>
            <ta e="T118" id="Seg_5564" s="T117">женщина-ADJZ-человек-ADES</ta>
            <ta e="T119" id="Seg_5565" s="T118">мы.PL.ALL</ta>
            <ta e="T120" id="Seg_5566" s="T119">этот.[NOM]</ta>
            <ta e="T121" id="Seg_5567" s="T120">приехать-HAB-INFER.[3SG.S]</ta>
            <ta e="T122" id="Seg_5568" s="T121">селькупский</ta>
            <ta e="T123" id="Seg_5569" s="T122">язык-PL-ACC</ta>
            <ta e="T124" id="Seg_5570" s="T123">написать-CVB</ta>
            <ta e="T125" id="Seg_5571" s="T124">принести-EP-FRQ-EP-PST-3SG.O</ta>
            <ta e="T126" id="Seg_5572" s="T125">город-COM</ta>
            <ta e="T127" id="Seg_5573" s="T126">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T128" id="Seg_5574" s="T127">этот.[NOM]</ta>
            <ta e="T129" id="Seg_5575" s="T128">приехать-HAB-INFER.[3SG.S]</ta>
            <ta e="T130" id="Seg_5576" s="T129">и</ta>
            <ta e="T131" id="Seg_5577" s="T130">Старосондорово-ILL</ta>
            <ta e="T132" id="Seg_5578" s="T131">селькупский</ta>
            <ta e="T133" id="Seg_5579" s="T132">слово-PL-ACC</ta>
            <ta e="T134" id="Seg_5580" s="T133">написать-CVB</ta>
            <ta e="T135" id="Seg_5581" s="T134">принести-EP-FRQ-EP-PST-3SG.O</ta>
            <ta e="T136" id="Seg_5582" s="T135">ты.ALL</ta>
            <ta e="T137" id="Seg_5583" s="T136">ходить-HAB-PST.[3SG.S]</ta>
            <ta e="T138" id="Seg_5584" s="T137">ты-COM</ta>
            <ta e="T139" id="Seg_5585" s="T138">все.время</ta>
            <ta e="T140" id="Seg_5586" s="T139">письмо-VBLZ-HAB-PST.[3SG.S]</ta>
            <ta e="T141" id="Seg_5587" s="T140">я.NOM</ta>
            <ta e="T142" id="Seg_5588" s="T141">этот.[NOM]</ta>
            <ta e="T143" id="Seg_5589" s="T142">женщина-ADJZ-человек-ADES</ta>
            <ta e="T144" id="Seg_5590" s="T143">жить-1SG.S</ta>
            <ta e="T145" id="Seg_5591" s="T144">Новосибирск-EP-ADV.LOC</ta>
            <ta e="T146" id="Seg_5592" s="T145">он(а)-EP-GEN</ta>
            <ta e="T147" id="Seg_5593" s="T146">имя.[NOM]-3SG</ta>
            <ta e="T148" id="Seg_5594" s="T147">Ангелина.[NOM]</ta>
            <ta e="T149" id="Seg_5595" s="T148">Ивановна.[NOM]</ta>
            <ta e="T150" id="Seg_5596" s="T149">я.NOM</ta>
            <ta e="T151" id="Seg_5597" s="T150">он(а)-ADES</ta>
            <ta e="T152" id="Seg_5598" s="T151">жить-1SG.S</ta>
            <ta e="T153" id="Seg_5599" s="T152">я.ACC</ta>
            <ta e="T154" id="Seg_5600" s="T153">иметь-3SG.S</ta>
            <ta e="T155" id="Seg_5601" s="T154">хорошо</ta>
            <ta e="T156" id="Seg_5602" s="T155">NEG</ta>
            <ta e="T157" id="Seg_5603" s="T156">поругать-DUR-HAB-3SG.S</ta>
            <ta e="T158" id="Seg_5604" s="T157">я.NOM</ta>
            <ta e="T159" id="Seg_5605" s="T158">сам.1SG</ta>
            <ta e="T160" id="Seg_5606" s="T159">сделать-CVB</ta>
            <ta e="T161" id="Seg_5607" s="T160">принести-EP-FRQ-EP-HAB-1SG.O</ta>
            <ta e="T162" id="Seg_5608" s="T161">что-ACC</ta>
            <ta e="T163" id="Seg_5609" s="T162">хотеть-1SG.S</ta>
            <ta e="T164" id="Seg_5610" s="T163">он(а)-ADES</ta>
            <ta e="T165" id="Seg_5611" s="T164">три</ta>
            <ta e="T166" id="Seg_5612" s="T165">комната.[NOM]</ta>
            <ta e="T167" id="Seg_5613" s="T166">дом-ILL</ta>
            <ta e="T168" id="Seg_5614" s="T167">дом-EP-GEN</ta>
            <ta e="T169" id="Seg_5615" s="T168">комната-PL.[NOM]</ta>
            <ta e="T170" id="Seg_5616" s="T169">большой.[3SG.S]</ta>
            <ta e="T171" id="Seg_5617" s="T170">дом.[NOM]</ta>
            <ta e="T172" id="Seg_5618" s="T171">горячий.[3SG.S]</ta>
            <ta e="T175" id="Seg_5619" s="T174">вода.[NOM]</ta>
            <ta e="T176" id="Seg_5620" s="T175">дом-LOC</ta>
            <ta e="T177" id="Seg_5621" s="T176">быть-3SG.S</ta>
            <ta e="T178" id="Seg_5622" s="T177">кипеть-PTCP.PRS</ta>
            <ta e="T179" id="Seg_5623" s="T178">вода.[NOM]</ta>
            <ta e="T180" id="Seg_5624" s="T179">и</ta>
            <ta e="T181" id="Seg_5625" s="T180">замерзнуть-PTCP.PST</ta>
            <ta e="T182" id="Seg_5626" s="T181">вода.[NOM]</ta>
            <ta e="T183" id="Seg_5627" s="T182">вода.[NOM]</ta>
            <ta e="T184" id="Seg_5628" s="T183">все.[NOM]</ta>
            <ta e="T185" id="Seg_5629" s="T184">дом-LOC</ta>
            <ta e="T186" id="Seg_5630" s="T185">быть-CO-3PL</ta>
            <ta e="T187" id="Seg_5631" s="T186">один</ta>
            <ta e="T188" id="Seg_5632" s="T187">комната-LOC</ta>
            <ta e="T189" id="Seg_5633" s="T188">мыться-HAB-1PL</ta>
            <ta e="T190" id="Seg_5634" s="T189">туда-ADV.LOC</ta>
            <ta e="T191" id="Seg_5635" s="T190">кипеть-PTCP.PRS</ta>
            <ta e="T192" id="Seg_5636" s="T191">вода.[NOM]</ta>
            <ta e="T193" id="Seg_5637" s="T192">и</ta>
            <ta e="T194" id="Seg_5638" s="T193">замерзнуть-PTCP.PST</ta>
            <ta e="T195" id="Seg_5639" s="T194">вода.[NOM]</ta>
            <ta e="T196" id="Seg_5640" s="T195">дом-LOC</ta>
            <ta e="T197" id="Seg_5641" s="T196">испражниться-DUR-HAB-1PL</ta>
            <ta e="T198" id="Seg_5642" s="T197">и</ta>
            <ta e="T199" id="Seg_5643" s="T198">помочиться-DUR-HAB-1PL</ta>
            <ta e="T200" id="Seg_5644" s="T199">весь</ta>
            <ta e="T201" id="Seg_5645" s="T200">вымыть-CVB</ta>
            <ta e="T202" id="Seg_5646" s="T201">начать-HAB-3SG.O</ta>
            <ta e="T203" id="Seg_5647" s="T202">и</ta>
            <ta e="T204" id="Seg_5648" s="T203">что</ta>
            <ta e="T205" id="Seg_5649" s="T204">запах.[NOM]-EMPH</ta>
            <ta e="T206" id="Seg_5650" s="T205">NEG.EX.[3SG.S]</ta>
            <ta e="T207" id="Seg_5651" s="T206">цепочка-ACC</ta>
            <ta e="T208" id="Seg_5652" s="T207">%%-FUT-2SG.O</ta>
            <ta e="T209" id="Seg_5653" s="T208">вода.[NOM]</ta>
            <ta e="T210" id="Seg_5654" s="T209">%бурлить-CVB</ta>
            <ta e="T211" id="Seg_5655" s="T210">остаться-HAB-3SG.S</ta>
            <ta e="T212" id="Seg_5656" s="T211">все</ta>
            <ta e="T213" id="Seg_5657" s="T212">вымыть-CVB</ta>
            <ta e="T214" id="Seg_5658" s="T213">вырвать-HAB-3SG.O</ta>
            <ta e="T215" id="Seg_5659" s="T214">дом-PL.[NOM]</ta>
            <ta e="T216" id="Seg_5660" s="T215">сюда-ADV.LOC</ta>
            <ta e="T217" id="Seg_5661" s="T216">большой</ta>
            <ta e="T218" id="Seg_5662" s="T217">четыре</ta>
            <ta e="T219" id="Seg_5663" s="T218">этаж.[NOM]</ta>
            <ta e="T220" id="Seg_5664" s="T219">дом-PL.[NOM]</ta>
            <ta e="T221" id="Seg_5665" s="T220">все</ta>
            <ta e="T222" id="Seg_5666" s="T221">один</ta>
            <ta e="T223" id="Seg_5667" s="T222">приблизительно.как</ta>
            <ta e="T224" id="Seg_5668" s="T223">дом-PL.[NOM]</ta>
            <ta e="T225" id="Seg_5669" s="T224">дом-PL.[NOM]</ta>
            <ta e="T226" id="Seg_5670" s="T225">длинный</ta>
            <ta e="T227" id="Seg_5671" s="T226">NEG</ta>
            <ta e="T228" id="Seg_5672" s="T227">далеко</ta>
            <ta e="T229" id="Seg_5673" s="T228">дерево-PL.[NOM]</ta>
            <ta e="T230" id="Seg_5674" s="T229">береза-PL.[NOM]</ta>
            <ta e="T231" id="Seg_5675" s="T230">тополь-PL.[NOM]</ta>
            <ta e="T232" id="Seg_5676" s="T231">елка-PL.[NOM]</ta>
            <ta e="T233" id="Seg_5677" s="T232">сосна-PL.[NOM]</ta>
            <ta e="T234" id="Seg_5678" s="T233">черемуха-GEN-дерево-PL.[NOM]</ta>
            <ta e="T235" id="Seg_5679" s="T234">осина-PL.[NOM]</ta>
            <ta e="T236" id="Seg_5680" s="T235">сюда-ADV.LOC</ta>
            <ta e="T237" id="Seg_5681" s="T236">лето-EP-ADVZ</ta>
            <ta e="T238" id="Seg_5682" s="T237">много-ADVZ</ta>
            <ta e="T239" id="Seg_5683" s="T238">грибы-PL.[NOM]</ta>
            <ta e="T240" id="Seg_5684" s="T239">родиться-HAB-3SG.S</ta>
            <ta e="T241" id="Seg_5685" s="T240">мы.[NOM]</ta>
            <ta e="T242" id="Seg_5686" s="T241">лето-EP-ADVZ</ta>
            <ta e="T243" id="Seg_5687" s="T242">грибы.[NOM]</ta>
            <ta e="T244" id="Seg_5688" s="T243">собрать-IPFV-FUT-1PL</ta>
            <ta e="T245" id="Seg_5689" s="T244">весной</ta>
            <ta e="T246" id="Seg_5690" s="T245">много-ADVZ</ta>
            <ta e="T247" id="Seg_5691" s="T246">быть-HAB-CO-3PL</ta>
            <ta e="T248" id="Seg_5692" s="T247">цветок-EP-PL.[NOM]</ta>
            <ta e="T249" id="Seg_5693" s="T248">зима-ADVZ</ta>
            <ta e="T250" id="Seg_5694" s="T249">лыжи-EP-INSTR</ta>
            <ta e="T251" id="Seg_5695" s="T250">ходить-3PL</ta>
            <ta e="T252" id="Seg_5696" s="T251">ребенок-PL.[NOM]</ta>
            <ta e="T253" id="Seg_5697" s="T252">салазки-INSTR</ta>
            <ta e="T254" id="Seg_5698" s="T253">покатиться-EP-FRQ-CO-3PL</ta>
            <ta e="T255" id="Seg_5699" s="T254">осень.[NOM]</ta>
            <ta e="T256" id="Seg_5700" s="T255">год-ADV.LOC</ta>
            <ta e="T257" id="Seg_5701" s="T256">лист-PL.[NOM]</ta>
            <ta e="T258" id="Seg_5702" s="T257">все</ta>
            <ta e="T259" id="Seg_5703" s="T258">желтый-%%</ta>
            <ta e="T260" id="Seg_5704" s="T259">стать-INCH-FUT-3PL</ta>
            <ta e="T261" id="Seg_5705" s="T260">когда</ta>
            <ta e="T262" id="Seg_5706" s="T261">мы.[NOM]</ta>
            <ta e="T263" id="Seg_5707" s="T262">приехать-PST-1PL</ta>
            <ta e="T264" id="Seg_5708" s="T263">Новосибирск-EP-ILL</ta>
            <ta e="T265" id="Seg_5709" s="T264">ночевать-PST-1PL</ta>
            <ta e="T266" id="Seg_5710" s="T265">Новосибирск-EP-LOC</ta>
            <ta e="T267" id="Seg_5711" s="T266">вокзал-LOC</ta>
            <ta e="T268" id="Seg_5712" s="T267">наверх-ADJZ</ta>
            <ta e="T269" id="Seg_5713" s="T268">этаж-LOC</ta>
            <ta e="T270" id="Seg_5714" s="T269">ночевать-PST-1PL</ta>
            <ta e="T271" id="Seg_5715" s="T270">пять-ORD</ta>
            <ta e="T272" id="Seg_5716" s="T271">этаж-LOC</ta>
            <ta e="T273" id="Seg_5717" s="T272">спать-PST-1PL</ta>
            <ta e="T274" id="Seg_5718" s="T273">мы.PL.ALL</ta>
            <ta e="T275" id="Seg_5719" s="T274">все</ta>
            <ta e="T276" id="Seg_5720" s="T275">дать-CO-3PL</ta>
            <ta e="T277" id="Seg_5721" s="T276">матрац-PL-ACC</ta>
            <ta e="T278" id="Seg_5722" s="T277">одеяло-PL-ACC</ta>
            <ta e="T279" id="Seg_5723" s="T278">подушка-PL-ACC</ta>
            <ta e="T280" id="Seg_5724" s="T279">спать-PST-1PL</ta>
            <ta e="T281" id="Seg_5725" s="T280">хороший-ADVZ</ta>
            <ta e="T282" id="Seg_5726" s="T281">человек-PL.[NOM]</ta>
            <ta e="T283" id="Seg_5727" s="T282">много-ADVZ</ta>
            <ta e="T284" id="Seg_5728" s="T283">быть-PST-3PL</ta>
            <ta e="T285" id="Seg_5729" s="T284">большой</ta>
            <ta e="T286" id="Seg_5730" s="T285">вокзал.[NOM]</ta>
            <ta e="T287" id="Seg_5731" s="T286">туда-ADV.LOC</ta>
            <ta e="T288" id="Seg_5732" s="T287">заблудиться-POT.FUT.2SG</ta>
            <ta e="T289" id="Seg_5733" s="T288">NEG</ta>
            <ta e="T290" id="Seg_5734" s="T289">выйти-POT.FUT.2SG</ta>
            <ta e="T291" id="Seg_5735" s="T290">утро-нечто-LOC</ta>
            <ta e="T292" id="Seg_5736" s="T291">отправиться-CO-1PL</ta>
            <ta e="T293" id="Seg_5737" s="T292">столовая-ILL</ta>
            <ta e="T294" id="Seg_5738" s="T293">он(а)-PL.[NOM]</ta>
            <ta e="T295" id="Seg_5739" s="T294">сказать-HAB-3PL</ta>
            <ta e="T296" id="Seg_5740" s="T295">ресторан.[NOM]</ta>
            <ta e="T297" id="Seg_5741" s="T296">туда-ADV.LOC</ta>
            <ta e="T298" id="Seg_5742" s="T297">хороший-ADVZ</ta>
            <ta e="T299" id="Seg_5743" s="T298">быть-3SG.S</ta>
            <ta e="T300" id="Seg_5744" s="T299">рай-EP-GEN</ta>
            <ta e="T301" id="Seg_5745" s="T300">нутро.[NOM]</ta>
            <ta e="T302" id="Seg_5746" s="T301">туда-ADV.LOC</ta>
            <ta e="T303" id="Seg_5747" s="T302">и</ta>
            <ta e="T304" id="Seg_5748" s="T303">съесть-FRQ-PST-1PL</ta>
            <ta e="T305" id="Seg_5749" s="T304">мы-ADES</ta>
            <ta e="T306" id="Seg_5750" s="T305">море.[NOM]</ta>
            <ta e="T307" id="Seg_5751" s="T306">близко-EP-ADV.LOC</ta>
            <ta e="T308" id="Seg_5752" s="T307">быть-3SG.S</ta>
            <ta e="T309" id="Seg_5753" s="T308">туда-ADV.LOC</ta>
            <ta e="T310" id="Seg_5754" s="T309">человек-PL.[NOM]</ta>
            <ta e="T311" id="Seg_5755" s="T310">рыбачить-ADJZ-CO-3PL</ta>
            <ta e="T312" id="Seg_5756" s="T311">удочка-INSTR</ta>
            <ta e="T313" id="Seg_5757" s="T312">ёрш-PL.[NOM]</ta>
            <ta e="T314" id="Seg_5758" s="T313">поймать-HAB-INFER-3PL</ta>
            <ta e="T315" id="Seg_5759" s="T314">и</ta>
            <ta e="T316" id="Seg_5760" s="T315">плотва-PL.[NOM]</ta>
            <ta e="T317" id="Seg_5761" s="T316">маленький-девушка-DIM.[NOM]</ta>
            <ta e="T318" id="Seg_5762" s="T317">хозяйка-GEN</ta>
            <ta e="T319" id="Seg_5763" s="T318">дочь-DIM.[NOM]-3SG</ta>
            <ta e="T320" id="Seg_5764" s="T319">принести-HAB-PST-3SG.O</ta>
            <ta e="T321" id="Seg_5765" s="T320">ёрш-ACC</ta>
            <ta e="T322" id="Seg_5766" s="T321">принести-PST-3SG.O</ta>
            <ta e="T323" id="Seg_5767" s="T322">ёрш.[NOM]</ta>
            <ta e="T324" id="Seg_5768" s="T323">замерзнуть-RFL-PST.NAR.[3SG.S]</ta>
            <ta e="T325" id="Seg_5769" s="T324">кошка.[NOM]</ta>
            <ta e="T326" id="Seg_5770" s="T325">он(а)-EP-ACC</ta>
            <ta e="T327" id="Seg_5771" s="T326">съесть-PST-3SG.O</ta>
            <ta e="T328" id="Seg_5772" s="T327">я.NOM-ADES</ta>
            <ta e="T329" id="Seg_5773" s="T328">два</ta>
            <ta e="T330" id="Seg_5774" s="T329">подруга.[NOM]-1SG</ta>
            <ta e="T331" id="Seg_5775" s="T330">быть-CO.[3SG.S]</ta>
            <ta e="T332" id="Seg_5776" s="T331">я.NOM</ta>
            <ta e="T333" id="Seg_5777" s="T332">он(а)-PL-ALL</ta>
            <ta e="T334" id="Seg_5778" s="T333">ходить-HAB-CO-1SG.S</ta>
            <ta e="T335" id="Seg_5779" s="T334">и</ta>
            <ta e="T336" id="Seg_5780" s="T335">он(а)-PL.[NOM]</ta>
            <ta e="T337" id="Seg_5781" s="T336">я.ALL</ta>
            <ta e="T338" id="Seg_5782" s="T337">ходить-HAB-CO-3PL</ta>
            <ta e="T339" id="Seg_5783" s="T338">я.NOM</ta>
            <ta e="T340" id="Seg_5784" s="T339">связать-FUT-1SG.S</ta>
            <ta e="T341" id="Seg_5785" s="T340">сеть-PL-ADJZ</ta>
            <ta e="T342" id="Seg_5786" s="T341">просто.так</ta>
            <ta e="T343" id="Seg_5787" s="T342">сидеть-INF</ta>
            <ta e="T344" id="Seg_5788" s="T343">NEG</ta>
            <ta e="T345" id="Seg_5789" s="T344">хотеть-1SG.S</ta>
            <ta e="T346" id="Seg_5790" s="T345">а</ta>
            <ta e="T347" id="Seg_5791" s="T346">сам.1SG</ta>
            <ta e="T348" id="Seg_5792" s="T347">все.время</ta>
            <ta e="T349" id="Seg_5793" s="T348">песня-VBLZ-HAB-1SG.S</ta>
            <ta e="T350" id="Seg_5794" s="T349">я.NOM</ta>
            <ta e="T351" id="Seg_5795" s="T350">весной</ta>
            <ta e="T352" id="Seg_5796" s="T351">Старосондорово-ILL</ta>
            <ta e="T353" id="Seg_5797" s="T352">приехать-FUT-1SG.S</ta>
            <ta e="T354" id="Seg_5798" s="T353">рыба-CAP-INF</ta>
            <ta e="T355" id="Seg_5799" s="T354">ягода-EP-ACC</ta>
            <ta e="T356" id="Seg_5800" s="T355">добыть-EP-INF</ta>
            <ta e="T357" id="Seg_5801" s="T356">город.[NOM]</ta>
            <ta e="T358" id="Seg_5802" s="T357">большой-ADVZ</ta>
            <ta e="T359" id="Seg_5803" s="T358">быть-3SG.S</ta>
            <ta e="T360" id="Seg_5804" s="T359">я.NOM</ta>
            <ta e="T361" id="Seg_5805" s="T360">ну</ta>
            <ta e="T362" id="Seg_5806" s="T361">приехать-INFER-FUT-1SG.S</ta>
            <ta e="T363" id="Seg_5807" s="T362">и</ta>
            <ta e="T364" id="Seg_5808" s="T363">ты.ALL</ta>
            <ta e="T365" id="Seg_5809" s="T364">виднеться-TR-FUT-1SG.O</ta>
            <ta e="T366" id="Seg_5810" s="T365">город-ACC</ta>
            <ta e="T367" id="Seg_5811" s="T366">ты.ALL</ta>
            <ta e="T368" id="Seg_5812" s="T367">этот</ta>
            <ta e="T369" id="Seg_5813" s="T368">город-ACC</ta>
            <ta e="T370" id="Seg_5814" s="T369">привезти-FUT-1SG.O</ta>
            <ta e="T371" id="Seg_5815" s="T370">ты.NOM</ta>
            <ta e="T372" id="Seg_5816" s="T371">посмотреть-DUR-FUT-2SG.O</ta>
            <ta e="T373" id="Seg_5817" s="T372">сюда-ADV.LOC</ta>
            <ta e="T374" id="Seg_5818" s="T373">всякий</ta>
            <ta e="T375" id="Seg_5819" s="T374">машина-PL-INSTR</ta>
            <ta e="T376" id="Seg_5820" s="T375">ходить-PST-1SG.S</ta>
            <ta e="T379" id="Seg_5821" s="T378">испугаться-DUR-PST-1SG.S</ta>
            <ta e="T380" id="Seg_5822" s="T379">я.ACC</ta>
            <ta e="T381" id="Seg_5823" s="T380">Володя.[NOM]</ta>
            <ta e="T382" id="Seg_5824" s="T381">город-LOC</ta>
            <ta e="T383" id="Seg_5825" s="T382">водить-PST.[NOM]</ta>
            <ta e="T384" id="Seg_5826" s="T383">все</ta>
            <ta e="T385" id="Seg_5827" s="T384">виднеться-TR-HAB-PST-3SG.O</ta>
            <ta e="T386" id="Seg_5828" s="T385">сам.3SG</ta>
            <ta e="T387" id="Seg_5829" s="T386">то</ta>
            <ta e="T388" id="Seg_5830" s="T387">куда</ta>
            <ta e="T389" id="Seg_5831" s="T388">отправиться-POT.FUT.2SG</ta>
            <ta e="T390" id="Seg_5832" s="T389">заблудиться-POT.FUT.2SG</ta>
            <ta e="T391" id="Seg_5833" s="T390">я.NOM</ta>
            <ta e="T392" id="Seg_5834" s="T391">помочиться-TEMPN-DRV-CO-1SG.S</ta>
            <ta e="T393" id="Seg_5835" s="T392">сказать-1SG.S</ta>
            <ta e="T394" id="Seg_5836" s="T393">помочиться-INF</ta>
            <ta e="T395" id="Seg_5837" s="T394">надо</ta>
            <ta e="T396" id="Seg_5838" s="T395">а</ta>
            <ta e="T397" id="Seg_5839" s="T396">Володя.[NOM]</ta>
            <ta e="T398" id="Seg_5840" s="T397">я.ALL</ta>
            <ta e="T399" id="Seg_5841" s="T398">сказать-3SG.S</ta>
            <ta e="T400" id="Seg_5842" s="T399">куда</ta>
            <ta e="T401" id="Seg_5843" s="T400">помочиться-POT.FUT.2SG</ta>
            <ta e="T402" id="Seg_5844" s="T401">помочиться-PTCP.PST</ta>
            <ta e="T403" id="Seg_5845" s="T402">нечто.[NOM]</ta>
            <ta e="T404" id="Seg_5846" s="T403">NEG.EX.[3SG.S]</ta>
            <ta e="T405" id="Seg_5847" s="T404">помочиться-PTCP.PST</ta>
            <ta e="T406" id="Seg_5848" s="T405">нечто-PL.[NOM]</ta>
            <ta e="T407" id="Seg_5849" s="T406">все</ta>
            <ta e="T408" id="Seg_5850" s="T407">дом-EP-GEN</ta>
            <ta e="T409" id="Seg_5851" s="T408">нутро-LOC</ta>
            <ta e="T410" id="Seg_5852" s="T409">быть-CO-3PL</ta>
            <ta e="T411" id="Seg_5853" s="T410">базар-LOC</ta>
            <ta e="T412" id="Seg_5854" s="T411">быть-PST-1DU</ta>
            <ta e="T413" id="Seg_5855" s="T412">он(а).[NOM]</ta>
            <ta e="T414" id="Seg_5856" s="T413">я.ALL</ta>
            <ta e="T415" id="Seg_5857" s="T414">сказать-3SG.S</ta>
            <ta e="T416" id="Seg_5858" s="T415">туда-ADV.LOC-%%</ta>
            <ta e="T417" id="Seg_5859" s="T416">что-TRL</ta>
            <ta e="T418" id="Seg_5860" s="T417">NEG</ta>
            <ta e="T419" id="Seg_5861" s="T418">помочиться-PST-2SG.S</ta>
            <ta e="T420" id="Seg_5862" s="T419">а</ta>
            <ta e="T421" id="Seg_5863" s="T420">я.NOM</ta>
            <ta e="T422" id="Seg_5864" s="T421">NEG</ta>
            <ta e="T423" id="Seg_5865" s="T422">помочиться-PST-1SG.S</ta>
            <ta e="T424" id="Seg_5866" s="T423">так</ta>
            <ta e="T425" id="Seg_5867" s="T424">я.NOM</ta>
            <ta e="T426" id="Seg_5868" s="T425">терпеть-DUR-PST-1SG.O</ta>
            <ta e="T427" id="Seg_5869" s="T426">когда</ta>
            <ta e="T428" id="Seg_5870" s="T427">машина-ACC</ta>
            <ta e="T429" id="Seg_5871" s="T428">ждать-PST-1DU</ta>
            <ta e="T430" id="Seg_5872" s="T429">я.NOM</ta>
            <ta e="T431" id="Seg_5873" s="T430">один</ta>
            <ta e="T432" id="Seg_5874" s="T431">женщина-ADJZ-человек-ALL</ta>
            <ta e="T433" id="Seg_5875" s="T432">спросить-1SG.S</ta>
            <ta e="T434" id="Seg_5876" s="T433">куда</ta>
            <ta e="T435" id="Seg_5877" s="T434">помочиться-PTCP.PST</ta>
            <ta e="T436" id="Seg_5878" s="T435">дом.[NOM]</ta>
            <ta e="T437" id="Seg_5879" s="T436">он(а).[NOM]</ta>
            <ta e="T438" id="Seg_5880" s="T437">я.ALL</ta>
            <ta e="T439" id="Seg_5881" s="T438">виднеться-TR-3SG.O</ta>
            <ta e="T440" id="Seg_5882" s="T439">%туда</ta>
            <ta e="T441" id="Seg_5883" s="T440">я.NOM</ta>
            <ta e="T442" id="Seg_5884" s="T441">ходить-MOM-CO-1SG.S</ta>
            <ta e="T443" id="Seg_5885" s="T442">и</ta>
            <ta e="T444" id="Seg_5886" s="T443">помочиться-1SG.S</ta>
            <ta e="T445" id="Seg_5887" s="T444">я.NOM</ta>
            <ta e="T446" id="Seg_5888" s="T445">думать-PST-1SG.S</ta>
            <ta e="T447" id="Seg_5889" s="T446">моча.[NOM]-1SG</ta>
            <ta e="T448" id="Seg_5890" s="T447">раздельно</ta>
            <ta e="T449" id="Seg_5891" s="T448">встать-3SG.S</ta>
            <ta e="T450" id="Seg_5892" s="T449">помочиться-1SG.S</ta>
            <ta e="T451" id="Seg_5893" s="T450">и</ta>
            <ta e="T452" id="Seg_5894" s="T451">хорошо</ta>
            <ta e="T453" id="Seg_5895" s="T452">стать-3SG.S</ta>
            <ta e="T454" id="Seg_5896" s="T453">магазин-PL.[NOM]</ta>
            <ta e="T455" id="Seg_5897" s="T454">зайти-HAB-1DU</ta>
            <ta e="T456" id="Seg_5898" s="T455">большой</ta>
            <ta e="T457" id="Seg_5899" s="T456">магазин-PL.[NOM]</ta>
            <ta e="T458" id="Seg_5900" s="T457">туда-ADV.LOC</ta>
            <ta e="T459" id="Seg_5901" s="T458">заблудиться-POT.FUT.2SG</ta>
            <ta e="T460" id="Seg_5902" s="T459">я.NOM</ta>
            <ta e="T461" id="Seg_5903" s="T460">грамота-EP-ACC</ta>
            <ta e="T462" id="Seg_5904" s="T461">NEG</ta>
            <ta e="T463" id="Seg_5905" s="T462">уметь-1SG.O</ta>
            <ta e="T464" id="Seg_5906" s="T463">сам.1SG</ta>
            <ta e="T465" id="Seg_5907" s="T464">NEG</ta>
            <ta e="T466" id="Seg_5908" s="T465">ходить-HAB-CO-1SG.S</ta>
            <ta e="T467" id="Seg_5909" s="T466">город-LOC</ta>
            <ta e="T468" id="Seg_5910" s="T467">а.то</ta>
            <ta e="T469" id="Seg_5911" s="T468">заблудиться-FUT-1SG.S</ta>
            <ta e="T470" id="Seg_5912" s="T469">я.NOM</ta>
            <ta e="T471" id="Seg_5913" s="T470">теперь</ta>
            <ta e="T472" id="Seg_5914" s="T471">машина-PL-INSTR</ta>
            <ta e="T473" id="Seg_5915" s="T472">ходить-1SG.S</ta>
            <ta e="T474" id="Seg_5916" s="T473">жить-PST-1SG.S</ta>
            <ta e="T475" id="Seg_5917" s="T474">город-LOC</ta>
            <ta e="T476" id="Seg_5918" s="T475">Чёрная_речка-LOC</ta>
            <ta e="T477" id="Seg_5919" s="T476">и</ta>
            <ta e="T478" id="Seg_5920" s="T477">Тахтамышево-LOC</ta>
            <ta e="T479" id="Seg_5921" s="T478">жить-PST-1SG.S</ta>
            <ta e="T480" id="Seg_5922" s="T479">Дуня.[NOM]</ta>
            <ta e="T481" id="Seg_5923" s="T480">приехать-PST.[3SG.S]</ta>
            <ta e="T482" id="Seg_5924" s="T481">Чёрная_речка-ILL</ta>
            <ta e="T483" id="Seg_5925" s="T482">сын-OBL.3SG-COM</ta>
            <ta e="T484" id="Seg_5926" s="T483">он(а)-EP-DU.[NOM]</ta>
            <ta e="T485" id="Seg_5927" s="T484">жить-3DU.S</ta>
            <ta e="T486" id="Seg_5928" s="T485">Николай.[NOM]</ta>
            <ta e="T487" id="Seg_5929" s="T486">Лазарыч-EP-GEN</ta>
            <ta e="T488" id="Seg_5930" s="T487">жена-DIM.[NOM]-3SG</ta>
            <ta e="T489" id="Seg_5931" s="T488">умереть-CO.[3SG.S]</ta>
            <ta e="T490" id="Seg_5932" s="T489">Чёрная_речка-LOC</ta>
            <ta e="T491" id="Seg_5933" s="T490">жить-PST.[3SG.S]</ta>
            <ta e="T492" id="Seg_5934" s="T491">дочь-OBL.3SG-ADES</ta>
            <ta e="T493" id="Seg_5935" s="T492">а</ta>
            <ta e="T494" id="Seg_5936" s="T493">я.NOM</ta>
            <ta e="T495" id="Seg_5937" s="T494">отправиться-CO-1SG.S</ta>
            <ta e="T496" id="Seg_5938" s="T495">город-PL-ACC</ta>
            <ta e="T497" id="Seg_5939" s="T496">посмотреть-DUR-INF</ta>
            <ta e="T498" id="Seg_5940" s="T497">Марина.[NOM]</ta>
            <ta e="T499" id="Seg_5941" s="T498">Павловна.[NOM]</ta>
            <ta e="T500" id="Seg_5942" s="T499">как</ta>
            <ta e="T501" id="Seg_5943" s="T500">я.NOM</ta>
            <ta e="T502" id="Seg_5944" s="T501">письмо.[NOM]-1SG</ta>
            <ta e="T503" id="Seg_5945" s="T502">прийти-FUT-3SG.S</ta>
            <ta e="T504" id="Seg_5946" s="T503">и</ta>
            <ta e="T505" id="Seg_5947" s="T504">я.ALL</ta>
            <ta e="T506" id="Seg_5948" s="T505">письмо-EP-ACC</ta>
            <ta e="T507" id="Seg_5949" s="T506">сразу</ta>
            <ta e="T508" id="Seg_5950" s="T507">письмо-IMP.2SG.O</ta>
            <ta e="T509" id="Seg_5951" s="T508">я.NOM</ta>
            <ta e="T510" id="Seg_5952" s="T509">ждать-TR-FUT-1SG.S</ta>
            <ta e="T511" id="Seg_5953" s="T510">все</ta>
            <ta e="T512" id="Seg_5954" s="T511">написать-IMP.2SG.O</ta>
            <ta e="T513" id="Seg_5955" s="T512">как</ta>
            <ta e="T514" id="Seg_5956" s="T513">я.GEN</ta>
            <ta e="T515" id="Seg_5957" s="T514">человек-PL.[NOM]-1SG</ta>
            <ta e="T516" id="Seg_5958" s="T515">жить-3PL</ta>
            <ta e="T517" id="Seg_5959" s="T516">Коля-GEN</ta>
            <ta e="T518" id="Seg_5960" s="T517">про</ta>
            <ta e="T519" id="Seg_5961" s="T518">что-ADJZ-EP-нечто-ACC</ta>
            <ta e="T520" id="Seg_5962" s="T519">услышать-TR-HAB-2SG.O</ta>
            <ta e="T521" id="Seg_5963" s="T520">али</ta>
            <ta e="T522" id="Seg_5964" s="T521">NEG</ta>
            <ta e="T523" id="Seg_5965" s="T522">все</ta>
            <ta e="T524" id="Seg_5966" s="T523">я.ALL</ta>
            <ta e="T525" id="Seg_5967" s="T524">написать-EP-IMP.2SG.O</ta>
            <ta e="T526" id="Seg_5968" s="T525">я.NOM</ta>
            <ta e="T527" id="Seg_5969" s="T526">путём</ta>
            <ta e="T528" id="Seg_5970" s="T527">ждать-TR-FUT-1SG.S</ta>
            <ta e="T529" id="Seg_5971" s="T528">вы.PL.ALL</ta>
            <ta e="T530" id="Seg_5972" s="T529">хлеб-INSTR</ta>
            <ta e="T531" id="Seg_5973" s="T530">дать-HAB-CO-3PL</ta>
            <ta e="T532" id="Seg_5974" s="T531">али</ta>
            <ta e="T533" id="Seg_5975" s="T532">мука-INSTR</ta>
            <ta e="T534" id="Seg_5976" s="T533">дать-HAB-CO-3PL</ta>
            <ta e="T535" id="Seg_5977" s="T534">а</ta>
            <ta e="T536" id="Seg_5978" s="T535">магазин-LOC</ta>
            <ta e="T537" id="Seg_5979" s="T536">что-PL.[NOM]</ta>
            <ta e="T538" id="Seg_5980" s="T537">быть-INFER-3PL</ta>
            <ta e="T539" id="Seg_5981" s="T538">все</ta>
            <ta e="T540" id="Seg_5982" s="T539">письмо-IMP.2SG.O</ta>
            <ta e="T541" id="Seg_5983" s="T540">зять.[NOM]-2SG</ta>
            <ta e="T542" id="Seg_5984" s="T541">ли</ta>
            <ta e="T543" id="Seg_5985" s="T542">жить-3SG.S</ta>
            <ta e="T544" id="Seg_5986" s="T543">али</ta>
            <ta e="T545" id="Seg_5987" s="T544">NEG</ta>
            <ta e="T546" id="Seg_5988" s="T545">сказать-PST-3PL</ta>
            <ta e="T547" id="Seg_5989" s="T546">прочь</ta>
            <ta e="T548" id="Seg_5990" s="T547">отправиться-PST.NAR.[3SG.S]</ta>
            <ta e="T549" id="Seg_5991" s="T548">Зинка-GEN</ta>
            <ta e="T550" id="Seg_5992" s="T549">про</ta>
            <ta e="T551" id="Seg_5993" s="T550">написать-IMP.2SG.O</ta>
            <ta e="T552" id="Seg_5994" s="T551">как</ta>
            <ta e="T553" id="Seg_5995" s="T552">жить-HAB-3SG.S</ta>
            <ta e="T554" id="Seg_5996" s="T553">жить-HAB-3SG.S</ta>
            <ta e="T555" id="Seg_5997" s="T554">я.NOM</ta>
            <ta e="T556" id="Seg_5998" s="T555">он(а)-ALL</ta>
            <ta e="T557" id="Seg_5999" s="T556">письмо-EP-ACC</ta>
            <ta e="T558" id="Seg_6000" s="T557">написать-PST-1SG.S</ta>
            <ta e="T559" id="Seg_6001" s="T558">он(а).[NOM]</ta>
            <ta e="T560" id="Seg_6002" s="T559">я.ALL</ta>
            <ta e="T561" id="Seg_6003" s="T560">NEG</ta>
            <ta e="T562" id="Seg_6004" s="T561">написать-HAB-3SG.O</ta>
            <ta e="T563" id="Seg_6005" s="T562">тетя.[NOM]</ta>
            <ta e="T564" id="Seg_6006" s="T563">Марина.[NOM]</ta>
            <ta e="T565" id="Seg_6007" s="T564">первый</ta>
            <ta e="T566" id="Seg_6008" s="T565">май-INSTR</ta>
            <ta e="T567" id="Seg_6009" s="T566">праздник-INSTR</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_6010" s="T1">pers.[n:case]</ta>
            <ta e="T3" id="Seg_6011" s="T2">dem-n.[n:case]</ta>
            <ta e="T4" id="Seg_6012" s="T3">v-v:pn</ta>
            <ta e="T5" id="Seg_6013" s="T4">nprop.[n:case]</ta>
            <ta e="T6" id="Seg_6014" s="T5">nprop-n:case</ta>
            <ta e="T7" id="Seg_6015" s="T6">v-v:pn</ta>
            <ta e="T8" id="Seg_6016" s="T7">pers</ta>
            <ta e="T9" id="Seg_6017" s="T8">pp</ta>
            <ta e="T10" id="Seg_6018" s="T9">v-v:pn</ta>
            <ta e="T11" id="Seg_6019" s="T10">conj</ta>
            <ta e="T12" id="Seg_6020" s="T11">v-v:pn</ta>
            <ta e="T13" id="Seg_6021" s="T12">interrog-n:num.[n:case]</ta>
            <ta e="T14" id="Seg_6022" s="T13">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T15" id="Seg_6023" s="T14">pers</ta>
            <ta e="T16" id="Seg_6024" s="T15">pers-n:case</ta>
            <ta e="T17" id="Seg_6025" s="T16">v-v&gt;adv</ta>
            <ta e="T18" id="Seg_6026" s="T17">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T19" id="Seg_6027" s="T18">pers</ta>
            <ta e="T20" id="Seg_6028" s="T19">pp</ta>
            <ta e="T21" id="Seg_6029" s="T20">pers</ta>
            <ta e="T22" id="Seg_6030" s="T21">v-v:pn</ta>
            <ta e="T23" id="Seg_6031" s="T22">pers.[n:case]</ta>
            <ta e="T24" id="Seg_6032" s="T23">n-n&gt;v-v:tense.[v:pn]</ta>
            <ta e="T25" id="Seg_6033" s="T24">pers</ta>
            <ta e="T26" id="Seg_6034" s="T25">pers-n:case</ta>
            <ta e="T27" id="Seg_6035" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_6036" s="T27">pers</ta>
            <ta e="T29" id="Seg_6037" s="T28">pers</ta>
            <ta e="T30" id="Seg_6038" s="T29">nprop-n:case</ta>
            <ta e="T31" id="Seg_6039" s="T30">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_6040" s="T31">pers.[n:case]</ta>
            <ta e="T33" id="Seg_6041" s="T32">pers-n:case</ta>
            <ta e="T34" id="Seg_6042" s="T33">conj</ta>
            <ta e="T35" id="Seg_6043" s="T34">v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_6044" s="T35">pers</ta>
            <ta e="T37" id="Seg_6045" s="T36">pers-n:case</ta>
            <ta e="T38" id="Seg_6046" s="T37">quant</ta>
            <ta e="T39" id="Seg_6047" s="T38">v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_6048" s="T39">pers</ta>
            <ta e="T41" id="Seg_6049" s="T40">v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_6050" s="T41">n.[n:case]</ta>
            <ta e="T43" id="Seg_6051" s="T42">nprop.[n:case]</ta>
            <ta e="T44" id="Seg_6052" s="T43">n.[n:case]</ta>
            <ta e="T45" id="Seg_6053" s="T44">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_6054" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_6055" s="T46">v-v&gt;v-v:mood.pn</ta>
            <ta e="T48" id="Seg_6056" s="T47">interj</ta>
            <ta e="T49" id="Seg_6057" s="T48">nprop.[n:case]</ta>
            <ta e="T50" id="Seg_6058" s="T49">nprop.[n:case]</ta>
            <ta e="T51" id="Seg_6059" s="T50">pers</ta>
            <ta e="T52" id="Seg_6060" s="T51">pers</ta>
            <ta e="T53" id="Seg_6061" s="T52">adj</ta>
            <ta e="T54" id="Seg_6062" s="T53">n.[n:case]</ta>
            <ta e="T55" id="Seg_6063" s="T54">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_6064" s="T55">adj-n-n&gt;adv</ta>
            <ta e="T57" id="Seg_6065" s="T56">pers</ta>
            <ta e="T58" id="Seg_6066" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_6067" s="T58">v-v:pn</ta>
            <ta e="T60" id="Seg_6068" s="T59">pers</ta>
            <ta e="T61" id="Seg_6069" s="T60">v-v:pn</ta>
            <ta e="T62" id="Seg_6070" s="T61">adv</ta>
            <ta e="T63" id="Seg_6071" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_6072" s="T63">nprop-n:ins-n:case</ta>
            <ta e="T65" id="Seg_6073" s="T64">n.[n:case]</ta>
            <ta e="T66" id="Seg_6074" s="T65">adj</ta>
            <ta e="T67" id="Seg_6075" s="T66">adj-adj&gt;adj</ta>
            <ta e="T68" id="Seg_6076" s="T67">n-n:case.poss</ta>
            <ta e="T69" id="Seg_6077" s="T68">nprop-n:ins-n:case</ta>
            <ta e="T70" id="Seg_6078" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_6079" s="T70">v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_6080" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_6081" s="T72">v-v&gt;adv</ta>
            <ta e="T74" id="Seg_6082" s="T73">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_6083" s="T74">nprop.[n:case]</ta>
            <ta e="T76" id="Seg_6084" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_6085" s="T76">n.[n:case]</ta>
            <ta e="T78" id="Seg_6086" s="T77">adv-adv:case</ta>
            <ta e="T79" id="Seg_6087" s="T78">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T80" id="Seg_6088" s="T79">pers</ta>
            <ta e="T81" id="Seg_6089" s="T80">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_6090" s="T81">conj</ta>
            <ta e="T83" id="Seg_6091" s="T82">nprop-n:ins-n:case</ta>
            <ta e="T84" id="Seg_6092" s="T83">interrog-n:case</ta>
            <ta e="T85" id="Seg_6093" s="T84">ptcl</ta>
            <ta e="T86" id="Seg_6094" s="T85">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_6095" s="T86">adv</ta>
            <ta e="T88" id="Seg_6096" s="T87">n.[n:case]</ta>
            <ta e="T89" id="Seg_6097" s="T88">v-v:pn</ta>
            <ta e="T90" id="Seg_6098" s="T89">pers</ta>
            <ta e="T91" id="Seg_6099" s="T90">adv</ta>
            <ta e="T92" id="Seg_6100" s="T91">n-n:case</ta>
            <ta e="T93" id="Seg_6101" s="T92">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T94" id="Seg_6102" s="T93">n-n:num.[n:case]</ta>
            <ta e="T95" id="Seg_6103" s="T94">quant-quant&gt;adv</ta>
            <ta e="T96" id="Seg_6104" s="T95">v-v&gt;adv</ta>
            <ta e="T97" id="Seg_6105" s="T96">v-v:tense-v:pn</ta>
            <ta e="T98" id="Seg_6106" s="T97">n-n:case</ta>
            <ta e="T99" id="Seg_6107" s="T98">n-n:case</ta>
            <ta e="T100" id="Seg_6108" s="T99">n-n:num.[n:case]</ta>
            <ta e="T101" id="Seg_6109" s="T100">n-n:ins-n:num.[n:case]</ta>
            <ta e="T102" id="Seg_6110" s="T101">adv</ta>
            <ta e="T103" id="Seg_6111" s="T102">n.[n:case]</ta>
            <ta e="T104" id="Seg_6112" s="T103">v-v:mood.[v:pn]</ta>
            <ta e="T105" id="Seg_6113" s="T104">pers</ta>
            <ta e="T106" id="Seg_6114" s="T105">n-n:case</ta>
            <ta e="T107" id="Seg_6115" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_6116" s="T107">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_6117" s="T108">adj-adj&gt;adv</ta>
            <ta e="T110" id="Seg_6118" s="T109">v-n:ins-v:pn</ta>
            <ta e="T111" id="Seg_6119" s="T110">adv</ta>
            <ta e="T112" id="Seg_6120" s="T111">pers</ta>
            <ta e="T113" id="Seg_6121" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_6122" s="T113">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T115" id="Seg_6123" s="T114">n-n:num-n:case</ta>
            <ta e="T116" id="Seg_6124" s="T115">pers</ta>
            <ta e="T117" id="Seg_6125" s="T116">v-v:pn</ta>
            <ta e="T118" id="Seg_6126" s="T117">n-n&gt;adj-n-n:case</ta>
            <ta e="T119" id="Seg_6127" s="T118">pers</ta>
            <ta e="T120" id="Seg_6128" s="T119">dem.[n:case]</ta>
            <ta e="T121" id="Seg_6129" s="T120">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T122" id="Seg_6130" s="T121">adj</ta>
            <ta e="T123" id="Seg_6131" s="T122">n-n:num-n:case</ta>
            <ta e="T124" id="Seg_6132" s="T123">v-v&gt;adv</ta>
            <ta e="T125" id="Seg_6133" s="T124">v-v:ins-v&gt;v-v:ins-v:tense-v:pn</ta>
            <ta e="T126" id="Seg_6134" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_6135" s="T126">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T128" id="Seg_6136" s="T127">dem.[n:case]</ta>
            <ta e="T129" id="Seg_6137" s="T128">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T130" id="Seg_6138" s="T129">conj</ta>
            <ta e="T131" id="Seg_6139" s="T130">nprop-n:case</ta>
            <ta e="T132" id="Seg_6140" s="T131">adj</ta>
            <ta e="T133" id="Seg_6141" s="T132">n-n:num-n:case</ta>
            <ta e="T134" id="Seg_6142" s="T133">v-v&gt;adv</ta>
            <ta e="T135" id="Seg_6143" s="T134">v-v:ins-v&gt;v-v:ins-v:tense-v:pn</ta>
            <ta e="T136" id="Seg_6144" s="T135">pers</ta>
            <ta e="T137" id="Seg_6145" s="T136">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T138" id="Seg_6146" s="T137">pers-n:case</ta>
            <ta e="T139" id="Seg_6147" s="T138">adv</ta>
            <ta e="T140" id="Seg_6148" s="T139">n-n&gt;v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T141" id="Seg_6149" s="T140">pers</ta>
            <ta e="T142" id="Seg_6150" s="T141">dem.[n:case]</ta>
            <ta e="T143" id="Seg_6151" s="T142">n-n&gt;adj-n-n:case</ta>
            <ta e="T144" id="Seg_6152" s="T143">v-v:pn</ta>
            <ta e="T145" id="Seg_6153" s="T144">nprop-n:ins-adv:case</ta>
            <ta e="T146" id="Seg_6154" s="T145">pers-n:ins-n:case</ta>
            <ta e="T147" id="Seg_6155" s="T146">n.[n:case]-n:poss</ta>
            <ta e="T148" id="Seg_6156" s="T147">nprop.[n:case]</ta>
            <ta e="T149" id="Seg_6157" s="T148">nprop.[n:case]</ta>
            <ta e="T150" id="Seg_6158" s="T149">pers</ta>
            <ta e="T151" id="Seg_6159" s="T150">pers-n:case</ta>
            <ta e="T152" id="Seg_6160" s="T151">v-v:pn</ta>
            <ta e="T153" id="Seg_6161" s="T152">pers</ta>
            <ta e="T154" id="Seg_6162" s="T153">v-v:pn</ta>
            <ta e="T155" id="Seg_6163" s="T154">adv</ta>
            <ta e="T156" id="Seg_6164" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_6165" s="T156">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T158" id="Seg_6166" s="T157">pers</ta>
            <ta e="T159" id="Seg_6167" s="T158">emphpro</ta>
            <ta e="T160" id="Seg_6168" s="T159">v-v&gt;adv</ta>
            <ta e="T161" id="Seg_6169" s="T160">v-v:ins-v&gt;v-v:ins-v&gt;v-v:pn</ta>
            <ta e="T162" id="Seg_6170" s="T161">interrog-n:case</ta>
            <ta e="T163" id="Seg_6171" s="T162">v-v:pn</ta>
            <ta e="T164" id="Seg_6172" s="T163">pers-n:case</ta>
            <ta e="T165" id="Seg_6173" s="T164">num</ta>
            <ta e="T166" id="Seg_6174" s="T165">n.[n:case]</ta>
            <ta e="T167" id="Seg_6175" s="T166">n-n:case</ta>
            <ta e="T168" id="Seg_6176" s="T167">n-n:ins-n:case</ta>
            <ta e="T169" id="Seg_6177" s="T168">n-n:num.[n:case]</ta>
            <ta e="T170" id="Seg_6178" s="T169">adj.[v:pn]</ta>
            <ta e="T171" id="Seg_6179" s="T170">n.[n:case]</ta>
            <ta e="T172" id="Seg_6180" s="T171">adj.[v:pn]</ta>
            <ta e="T175" id="Seg_6181" s="T174">n.[n:case]</ta>
            <ta e="T176" id="Seg_6182" s="T175">n-n:case</ta>
            <ta e="T177" id="Seg_6183" s="T176">v-v:pn</ta>
            <ta e="T178" id="Seg_6184" s="T177">v-v&gt;ptcp</ta>
            <ta e="T179" id="Seg_6185" s="T178">n.[n:case]</ta>
            <ta e="T180" id="Seg_6186" s="T179">conj</ta>
            <ta e="T181" id="Seg_6187" s="T180">v-v&gt;ptcp</ta>
            <ta e="T182" id="Seg_6188" s="T181">n.[n:case]</ta>
            <ta e="T183" id="Seg_6189" s="T182">n.[n:case]</ta>
            <ta e="T184" id="Seg_6190" s="T183">quant.[n:case]</ta>
            <ta e="T185" id="Seg_6191" s="T184">n-n:case</ta>
            <ta e="T186" id="Seg_6192" s="T185">v-v:ins-v:pn</ta>
            <ta e="T187" id="Seg_6193" s="T186">num</ta>
            <ta e="T188" id="Seg_6194" s="T187">n-n:case</ta>
            <ta e="T189" id="Seg_6195" s="T188">v-v&gt;v-v:pn</ta>
            <ta e="T190" id="Seg_6196" s="T189">adv-adv:case</ta>
            <ta e="T191" id="Seg_6197" s="T190">v-v&gt;ptcp</ta>
            <ta e="T192" id="Seg_6198" s="T191">n.[n:case]</ta>
            <ta e="T193" id="Seg_6199" s="T192">conj</ta>
            <ta e="T194" id="Seg_6200" s="T193">v-v&gt;ptcp</ta>
            <ta e="T195" id="Seg_6201" s="T194">n.[n:case]</ta>
            <ta e="T196" id="Seg_6202" s="T195">n-n:case</ta>
            <ta e="T197" id="Seg_6203" s="T196">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T198" id="Seg_6204" s="T197">conj</ta>
            <ta e="T199" id="Seg_6205" s="T198">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T200" id="Seg_6206" s="T199">quant</ta>
            <ta e="T201" id="Seg_6207" s="T200">v-v&gt;adv</ta>
            <ta e="T202" id="Seg_6208" s="T201">v-v&gt;v-v:pn</ta>
            <ta e="T203" id="Seg_6209" s="T202">conj</ta>
            <ta e="T204" id="Seg_6210" s="T203">interrog</ta>
            <ta e="T205" id="Seg_6211" s="T204">n.[n:case]-clit</ta>
            <ta e="T206" id="Seg_6212" s="T205">v.[v:pn]</ta>
            <ta e="T207" id="Seg_6213" s="T206">n-n:case</ta>
            <ta e="T208" id="Seg_6214" s="T207">v-v:tense-v:pn</ta>
            <ta e="T209" id="Seg_6215" s="T208">n.[n:case]</ta>
            <ta e="T210" id="Seg_6216" s="T209">v-v&gt;adv</ta>
            <ta e="T211" id="Seg_6217" s="T210">v-v&gt;v-v:pn</ta>
            <ta e="T212" id="Seg_6218" s="T211">quant</ta>
            <ta e="T213" id="Seg_6219" s="T212">v-v&gt;adv</ta>
            <ta e="T214" id="Seg_6220" s="T213">v-v&gt;v-v:pn</ta>
            <ta e="T215" id="Seg_6221" s="T214">n-n:num.[n:case]</ta>
            <ta e="T216" id="Seg_6222" s="T215">adv-adv:case</ta>
            <ta e="T217" id="Seg_6223" s="T216">adj</ta>
            <ta e="T218" id="Seg_6224" s="T217">num</ta>
            <ta e="T219" id="Seg_6225" s="T218">n.[n:case]</ta>
            <ta e="T220" id="Seg_6226" s="T219">n-n:num.[n:case]</ta>
            <ta e="T221" id="Seg_6227" s="T220">quant</ta>
            <ta e="T222" id="Seg_6228" s="T221">num</ta>
            <ta e="T223" id="Seg_6229" s="T222">pp</ta>
            <ta e="T224" id="Seg_6230" s="T223">n-n:num.[n:case]</ta>
            <ta e="T225" id="Seg_6231" s="T224">n-n:num.[n:case]</ta>
            <ta e="T226" id="Seg_6232" s="T225">adj</ta>
            <ta e="T227" id="Seg_6233" s="T226">ptcl</ta>
            <ta e="T228" id="Seg_6234" s="T227">adv</ta>
            <ta e="T229" id="Seg_6235" s="T228">n-n:num.[n:case]</ta>
            <ta e="T230" id="Seg_6236" s="T229">n-n:num.[n:case]</ta>
            <ta e="T231" id="Seg_6237" s="T230">n-n:num.[n:case]</ta>
            <ta e="T232" id="Seg_6238" s="T231">n-n:num.[n:case]</ta>
            <ta e="T233" id="Seg_6239" s="T232">n-n:num.[n:case]</ta>
            <ta e="T234" id="Seg_6240" s="T233">n-n:case-n-n:num.[n:case]</ta>
            <ta e="T235" id="Seg_6241" s="T234">n-n:num.[n:case]</ta>
            <ta e="T236" id="Seg_6242" s="T235">adv-adv:case</ta>
            <ta e="T237" id="Seg_6243" s="T236">n-n:ins-n&gt;adv</ta>
            <ta e="T238" id="Seg_6244" s="T237">quant-quant&gt;adv</ta>
            <ta e="T239" id="Seg_6245" s="T238">n-n:num.[n:case]</ta>
            <ta e="T240" id="Seg_6246" s="T239">v-v&gt;v-v:pn</ta>
            <ta e="T241" id="Seg_6247" s="T240">pers.[n:case]</ta>
            <ta e="T242" id="Seg_6248" s="T241">n-n:ins-n&gt;adv</ta>
            <ta e="T243" id="Seg_6249" s="T242">n.[n:case]</ta>
            <ta e="T244" id="Seg_6250" s="T243">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T245" id="Seg_6251" s="T244">adv</ta>
            <ta e="T246" id="Seg_6252" s="T245">quant-adj&gt;adv</ta>
            <ta e="T247" id="Seg_6253" s="T246">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T248" id="Seg_6254" s="T247">n-n:ins-n:num.[n:case]</ta>
            <ta e="T249" id="Seg_6255" s="T248">n-n&gt;adv</ta>
            <ta e="T250" id="Seg_6256" s="T249">n-n:ins-n:case</ta>
            <ta e="T251" id="Seg_6257" s="T250">v-v:pn</ta>
            <ta e="T252" id="Seg_6258" s="T251">n-n:num.[n:case]</ta>
            <ta e="T253" id="Seg_6259" s="T252">n-n:case</ta>
            <ta e="T254" id="Seg_6260" s="T253">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T255" id="Seg_6261" s="T254">n.[n:case]</ta>
            <ta e="T256" id="Seg_6262" s="T255">n-adv:case</ta>
            <ta e="T257" id="Seg_6263" s="T256">n-n:num.[n:case]</ta>
            <ta e="T258" id="Seg_6264" s="T257">quant</ta>
            <ta e="T259" id="Seg_6265" s="T258">adj-%%</ta>
            <ta e="T260" id="Seg_6266" s="T259">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T261" id="Seg_6267" s="T260">conj</ta>
            <ta e="T262" id="Seg_6268" s="T261">pers.[n:case]</ta>
            <ta e="T263" id="Seg_6269" s="T262">v-v:tense-v:pn</ta>
            <ta e="T264" id="Seg_6270" s="T263">nprop-n:ins-n:case</ta>
            <ta e="T265" id="Seg_6271" s="T264">v-v:tense-v:pn</ta>
            <ta e="T266" id="Seg_6272" s="T265">nprop-n:ins-n:case</ta>
            <ta e="T267" id="Seg_6273" s="T266">n-n:case</ta>
            <ta e="T268" id="Seg_6274" s="T267">adv-adv&gt;adj</ta>
            <ta e="T269" id="Seg_6275" s="T268">n-n:case</ta>
            <ta e="T270" id="Seg_6276" s="T269">v-v:tense-v:pn</ta>
            <ta e="T271" id="Seg_6277" s="T270">num-num&gt;adj</ta>
            <ta e="T272" id="Seg_6278" s="T271">n-n:case</ta>
            <ta e="T273" id="Seg_6279" s="T272">v-v:tense-v:pn</ta>
            <ta e="T274" id="Seg_6280" s="T273">pers</ta>
            <ta e="T275" id="Seg_6281" s="T274">quant</ta>
            <ta e="T276" id="Seg_6282" s="T275">v-v:ins-v:pn</ta>
            <ta e="T277" id="Seg_6283" s="T276">n-n:num-n:case</ta>
            <ta e="T278" id="Seg_6284" s="T277">n-n:num-n:case</ta>
            <ta e="T279" id="Seg_6285" s="T278">n-n:num-n:case</ta>
            <ta e="T280" id="Seg_6286" s="T279">v-v:tense-v:pn</ta>
            <ta e="T281" id="Seg_6287" s="T280">adj-adj&gt;adv</ta>
            <ta e="T282" id="Seg_6288" s="T281">n-n:num.[n:case]</ta>
            <ta e="T283" id="Seg_6289" s="T282">quant-quant&gt;adv</ta>
            <ta e="T284" id="Seg_6290" s="T283">v-v:tense-v:pn</ta>
            <ta e="T285" id="Seg_6291" s="T284">adj</ta>
            <ta e="T286" id="Seg_6292" s="T285">n.[n:case]</ta>
            <ta e="T287" id="Seg_6293" s="T286">adv-adv:case</ta>
            <ta e="T288" id="Seg_6294" s="T287">v-v:tense</ta>
            <ta e="T289" id="Seg_6295" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_6296" s="T289">v-v:tense</ta>
            <ta e="T291" id="Seg_6297" s="T290">n-n-n:case</ta>
            <ta e="T292" id="Seg_6298" s="T291">v-v:ins-v:pn</ta>
            <ta e="T293" id="Seg_6299" s="T292">n-n:case</ta>
            <ta e="T294" id="Seg_6300" s="T293">pers-n:num.[n:case]</ta>
            <ta e="T295" id="Seg_6301" s="T294">v-v&gt;v-v:pn</ta>
            <ta e="T296" id="Seg_6302" s="T295">n.[n:case]</ta>
            <ta e="T297" id="Seg_6303" s="T296">adv-adv:case</ta>
            <ta e="T298" id="Seg_6304" s="T297">adj-adj&gt;adv</ta>
            <ta e="T299" id="Seg_6305" s="T298">v-v:pn</ta>
            <ta e="T300" id="Seg_6306" s="T299">n-n:ins-n:case</ta>
            <ta e="T301" id="Seg_6307" s="T300">n.[n:case]</ta>
            <ta e="T302" id="Seg_6308" s="T301">adv-adv:case</ta>
            <ta e="T303" id="Seg_6309" s="T302">conj</ta>
            <ta e="T304" id="Seg_6310" s="T303">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T305" id="Seg_6311" s="T304">pers-n:case</ta>
            <ta e="T306" id="Seg_6312" s="T305">n.[n:case]</ta>
            <ta e="T307" id="Seg_6313" s="T306">adv-n:ins-adv:case</ta>
            <ta e="T308" id="Seg_6314" s="T307">v-v:pn</ta>
            <ta e="T309" id="Seg_6315" s="T308">adv-adv:case</ta>
            <ta e="T310" id="Seg_6316" s="T309">n-n:num.[n:case]</ta>
            <ta e="T311" id="Seg_6317" s="T310">v-n&gt;adj-v:ins-v:pn</ta>
            <ta e="T312" id="Seg_6318" s="T311">n-n:case</ta>
            <ta e="T313" id="Seg_6319" s="T312">n-n:num.[n:case]</ta>
            <ta e="T314" id="Seg_6320" s="T313">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T315" id="Seg_6321" s="T314">conj</ta>
            <ta e="T316" id="Seg_6322" s="T315">n-n:num.[n:case]</ta>
            <ta e="T317" id="Seg_6323" s="T316">adj-n-n&gt;n.[n:case]</ta>
            <ta e="T318" id="Seg_6324" s="T317">n-n:case</ta>
            <ta e="T319" id="Seg_6325" s="T318">n-n&gt;n.[n:case]-n:poss</ta>
            <ta e="T320" id="Seg_6326" s="T319">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T321" id="Seg_6327" s="T320">n-n:case</ta>
            <ta e="T322" id="Seg_6328" s="T321">v-v:tense-v:pn</ta>
            <ta e="T323" id="Seg_6329" s="T322">n.[n:case]</ta>
            <ta e="T324" id="Seg_6330" s="T323">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T325" id="Seg_6331" s="T324">n.[n:case]</ta>
            <ta e="T326" id="Seg_6332" s="T325">pers-n:ins-n:case</ta>
            <ta e="T327" id="Seg_6333" s="T326">v-v:tense-v:pn</ta>
            <ta e="T328" id="Seg_6334" s="T327">pers-n:case</ta>
            <ta e="T329" id="Seg_6335" s="T328">num</ta>
            <ta e="T330" id="Seg_6336" s="T329">n.[n:case]-n:poss</ta>
            <ta e="T331" id="Seg_6337" s="T330">v-v:ins.[v:pn]</ta>
            <ta e="T332" id="Seg_6338" s="T331">pers</ta>
            <ta e="T333" id="Seg_6339" s="T332">pers-n:num-n:case</ta>
            <ta e="T334" id="Seg_6340" s="T333">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T335" id="Seg_6341" s="T334">conj</ta>
            <ta e="T336" id="Seg_6342" s="T335">pers-n:num.[n:case]</ta>
            <ta e="T337" id="Seg_6343" s="T336">pers</ta>
            <ta e="T338" id="Seg_6344" s="T337">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T339" id="Seg_6345" s="T338">pers</ta>
            <ta e="T340" id="Seg_6346" s="T339">v-v:tense-v:pn</ta>
            <ta e="T341" id="Seg_6347" s="T340">n-n:num-n&gt;adj</ta>
            <ta e="T342" id="Seg_6348" s="T341">adv</ta>
            <ta e="T343" id="Seg_6349" s="T342">v-v:inf</ta>
            <ta e="T344" id="Seg_6350" s="T343">ptcl</ta>
            <ta e="T345" id="Seg_6351" s="T344">v-v:pn</ta>
            <ta e="T346" id="Seg_6352" s="T345">conj</ta>
            <ta e="T347" id="Seg_6353" s="T346">emphpro</ta>
            <ta e="T348" id="Seg_6354" s="T347">adv</ta>
            <ta e="T349" id="Seg_6355" s="T348">n-n&gt;v-v&gt;v-v:pn</ta>
            <ta e="T350" id="Seg_6356" s="T349">pers</ta>
            <ta e="T351" id="Seg_6357" s="T350">adv</ta>
            <ta e="T352" id="Seg_6358" s="T351">nprop-n:case</ta>
            <ta e="T353" id="Seg_6359" s="T352">v-v:tense-v:pn</ta>
            <ta e="T354" id="Seg_6360" s="T353">n-n&gt;v-v:inf</ta>
            <ta e="T355" id="Seg_6361" s="T354">n-n:ins-n:case</ta>
            <ta e="T356" id="Seg_6362" s="T355">v-n:ins-v:inf</ta>
            <ta e="T357" id="Seg_6363" s="T356">n.[n:case]</ta>
            <ta e="T358" id="Seg_6364" s="T357">adj-adj&gt;adv</ta>
            <ta e="T359" id="Seg_6365" s="T358">v-v:pn</ta>
            <ta e="T360" id="Seg_6366" s="T359">pers</ta>
            <ta e="T361" id="Seg_6367" s="T360">ptcl</ta>
            <ta e="T362" id="Seg_6368" s="T361">v-v:mood-v:tense-v:pn</ta>
            <ta e="T363" id="Seg_6369" s="T362">conj</ta>
            <ta e="T364" id="Seg_6370" s="T363">pers</ta>
            <ta e="T365" id="Seg_6371" s="T364">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T366" id="Seg_6372" s="T365">n-n:case</ta>
            <ta e="T367" id="Seg_6373" s="T366">pers</ta>
            <ta e="T368" id="Seg_6374" s="T367">dem</ta>
            <ta e="T369" id="Seg_6375" s="T368">n-n:case</ta>
            <ta e="T370" id="Seg_6376" s="T369">v-v:tense-v:pn</ta>
            <ta e="T371" id="Seg_6377" s="T370">pers</ta>
            <ta e="T372" id="Seg_6378" s="T371">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T373" id="Seg_6379" s="T372">adv-adv:case</ta>
            <ta e="T374" id="Seg_6380" s="T373">adj</ta>
            <ta e="T375" id="Seg_6381" s="T374">n-n:num-n:case</ta>
            <ta e="T376" id="Seg_6382" s="T375">v-v:tense-v:pn</ta>
            <ta e="T379" id="Seg_6383" s="T378">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T380" id="Seg_6384" s="T379">pers</ta>
            <ta e="T381" id="Seg_6385" s="T380">nprop.[n:case]</ta>
            <ta e="T382" id="Seg_6386" s="T381">n-n:case</ta>
            <ta e="T383" id="Seg_6387" s="T382">v-v:tense.[n:case]</ta>
            <ta e="T384" id="Seg_6388" s="T383">quant</ta>
            <ta e="T385" id="Seg_6389" s="T384">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T386" id="Seg_6390" s="T385">emphpro</ta>
            <ta e="T387" id="Seg_6391" s="T386">ptcl</ta>
            <ta e="T388" id="Seg_6392" s="T387">interrog</ta>
            <ta e="T389" id="Seg_6393" s="T388">v-v:tense</ta>
            <ta e="T390" id="Seg_6394" s="T389">v-v:tense</ta>
            <ta e="T391" id="Seg_6395" s="T390">pers</ta>
            <ta e="T392" id="Seg_6396" s="T391">v-v&gt;n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T393" id="Seg_6397" s="T392">v-v:pn</ta>
            <ta e="T394" id="Seg_6398" s="T393">v-v:inf</ta>
            <ta e="T395" id="Seg_6399" s="T394">ptcl</ta>
            <ta e="T396" id="Seg_6400" s="T395">conj</ta>
            <ta e="T397" id="Seg_6401" s="T396">nprop.[n:case]</ta>
            <ta e="T398" id="Seg_6402" s="T397">pers</ta>
            <ta e="T399" id="Seg_6403" s="T398">v-v:pn</ta>
            <ta e="T400" id="Seg_6404" s="T399">interrog</ta>
            <ta e="T401" id="Seg_6405" s="T400">v-v:tense</ta>
            <ta e="T402" id="Seg_6406" s="T401">v-v&gt;ptcp</ta>
            <ta e="T403" id="Seg_6407" s="T402">n.[n:case]</ta>
            <ta e="T404" id="Seg_6408" s="T403">v.[v:pn]</ta>
            <ta e="T405" id="Seg_6409" s="T404">v-v&gt;ptcp</ta>
            <ta e="T406" id="Seg_6410" s="T405">n-n:num.[n:case]</ta>
            <ta e="T407" id="Seg_6411" s="T406">quant</ta>
            <ta e="T408" id="Seg_6412" s="T407">n-n:ins-n:case</ta>
            <ta e="T409" id="Seg_6413" s="T408">n-n:case</ta>
            <ta e="T410" id="Seg_6414" s="T409">v-v:ins-v:pn</ta>
            <ta e="T411" id="Seg_6415" s="T410">n-n:case</ta>
            <ta e="T412" id="Seg_6416" s="T411">v-v:tense-v:pn</ta>
            <ta e="T413" id="Seg_6417" s="T412">pers.[n:case]</ta>
            <ta e="T414" id="Seg_6418" s="T413">pers</ta>
            <ta e="T415" id="Seg_6419" s="T414">v-v:pn</ta>
            <ta e="T416" id="Seg_6420" s="T415">adv-adv:case-%%</ta>
            <ta e="T417" id="Seg_6421" s="T416">interrog-n:case</ta>
            <ta e="T418" id="Seg_6422" s="T417">ptcl</ta>
            <ta e="T419" id="Seg_6423" s="T418">v-v:tense-v:pn</ta>
            <ta e="T420" id="Seg_6424" s="T419">conj</ta>
            <ta e="T421" id="Seg_6425" s="T420">pers</ta>
            <ta e="T422" id="Seg_6426" s="T421">ptcl</ta>
            <ta e="T423" id="Seg_6427" s="T422">v-v:tense-v:pn</ta>
            <ta e="T424" id="Seg_6428" s="T423">adv</ta>
            <ta e="T425" id="Seg_6429" s="T424">pers</ta>
            <ta e="T426" id="Seg_6430" s="T425">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T427" id="Seg_6431" s="T426">conj</ta>
            <ta e="T428" id="Seg_6432" s="T427">n-n:case</ta>
            <ta e="T429" id="Seg_6433" s="T428">v-v:tense-v:pn</ta>
            <ta e="T430" id="Seg_6434" s="T429">pers</ta>
            <ta e="T431" id="Seg_6435" s="T430">num</ta>
            <ta e="T432" id="Seg_6436" s="T431">n-n&gt;adj-n-n:case</ta>
            <ta e="T433" id="Seg_6437" s="T432">v-v:pn</ta>
            <ta e="T434" id="Seg_6438" s="T433">interrog</ta>
            <ta e="T435" id="Seg_6439" s="T434">v-v&gt;ptcp</ta>
            <ta e="T436" id="Seg_6440" s="T435">n.[n:case]</ta>
            <ta e="T437" id="Seg_6441" s="T436">pers.[n:case]</ta>
            <ta e="T438" id="Seg_6442" s="T437">pers</ta>
            <ta e="T439" id="Seg_6443" s="T438">v-v&gt;v-v:pn</ta>
            <ta e="T440" id="Seg_6444" s="T439">adv</ta>
            <ta e="T441" id="Seg_6445" s="T440">pers</ta>
            <ta e="T442" id="Seg_6446" s="T441">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T443" id="Seg_6447" s="T442">conj</ta>
            <ta e="T444" id="Seg_6448" s="T443">v-v:pn</ta>
            <ta e="T445" id="Seg_6449" s="T444">pers</ta>
            <ta e="T446" id="Seg_6450" s="T445">v-v:tense-v:pn</ta>
            <ta e="T447" id="Seg_6451" s="T446">n.[n:case]-n:poss</ta>
            <ta e="T448" id="Seg_6452" s="T447">preverb</ta>
            <ta e="T449" id="Seg_6453" s="T448">v-v:pn</ta>
            <ta e="T450" id="Seg_6454" s="T449">v-v:pn</ta>
            <ta e="T451" id="Seg_6455" s="T450">conj</ta>
            <ta e="T452" id="Seg_6456" s="T451">adv</ta>
            <ta e="T453" id="Seg_6457" s="T452">v-v:pn</ta>
            <ta e="T454" id="Seg_6458" s="T453">n-n:num.[n:case]</ta>
            <ta e="T455" id="Seg_6459" s="T454">v-v&gt;v-v:pn</ta>
            <ta e="T456" id="Seg_6460" s="T455">adj</ta>
            <ta e="T457" id="Seg_6461" s="T456">n-n:num.[n:case]</ta>
            <ta e="T458" id="Seg_6462" s="T457">adv-adv:case</ta>
            <ta e="T459" id="Seg_6463" s="T458">v-v:tense</ta>
            <ta e="T460" id="Seg_6464" s="T459">pers</ta>
            <ta e="T461" id="Seg_6465" s="T460">n-n:ins-n:case</ta>
            <ta e="T462" id="Seg_6466" s="T461">ptcl</ta>
            <ta e="T463" id="Seg_6467" s="T462">v-v:pn</ta>
            <ta e="T464" id="Seg_6468" s="T463">emphpro</ta>
            <ta e="T465" id="Seg_6469" s="T464">ptcl</ta>
            <ta e="T466" id="Seg_6470" s="T465">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T467" id="Seg_6471" s="T466">n-n:case</ta>
            <ta e="T468" id="Seg_6472" s="T467">conj</ta>
            <ta e="T469" id="Seg_6473" s="T468">v-v:tense-v:pn</ta>
            <ta e="T470" id="Seg_6474" s="T469">pers</ta>
            <ta e="T471" id="Seg_6475" s="T470">adv</ta>
            <ta e="T472" id="Seg_6476" s="T471">n-n:num-n:case</ta>
            <ta e="T473" id="Seg_6477" s="T472">v-v:pn</ta>
            <ta e="T474" id="Seg_6478" s="T473">v-v:tense-v:pn</ta>
            <ta e="T475" id="Seg_6479" s="T474">n-n:case</ta>
            <ta e="T476" id="Seg_6480" s="T475">nprop-n:case</ta>
            <ta e="T477" id="Seg_6481" s="T476">conj</ta>
            <ta e="T478" id="Seg_6482" s="T477">nprop-n:case</ta>
            <ta e="T479" id="Seg_6483" s="T478">v-v:tense-v:pn</ta>
            <ta e="T480" id="Seg_6484" s="T479">nprop.[n:case]</ta>
            <ta e="T481" id="Seg_6485" s="T480">v-v:tense.[v:pn]</ta>
            <ta e="T482" id="Seg_6486" s="T481">nprop-n:case</ta>
            <ta e="T483" id="Seg_6487" s="T482">n-n:obl.poss-n:case</ta>
            <ta e="T484" id="Seg_6488" s="T483">pers-n:ins-n:num.[n:case]</ta>
            <ta e="T485" id="Seg_6489" s="T484">v-v:pn</ta>
            <ta e="T486" id="Seg_6490" s="T485">nprop.[n:case]</ta>
            <ta e="T487" id="Seg_6491" s="T486">nprop-n:ins-n:case</ta>
            <ta e="T488" id="Seg_6492" s="T487">n-n&gt;n.[n:case]-n:poss</ta>
            <ta e="T489" id="Seg_6493" s="T488">v-v:ins.[v:pn]</ta>
            <ta e="T490" id="Seg_6494" s="T489">nprop-n:case</ta>
            <ta e="T491" id="Seg_6495" s="T490">v-v:tense.[v:pn]</ta>
            <ta e="T492" id="Seg_6496" s="T491">n-n:obl.poss-n:case</ta>
            <ta e="T493" id="Seg_6497" s="T492">conj</ta>
            <ta e="T494" id="Seg_6498" s="T493">pers</ta>
            <ta e="T495" id="Seg_6499" s="T494">v-v:ins-v:pn</ta>
            <ta e="T496" id="Seg_6500" s="T495">n-n:num-n:case</ta>
            <ta e="T497" id="Seg_6501" s="T496">v-v&gt;v-v:inf</ta>
            <ta e="T498" id="Seg_6502" s="T497">nprop.[n:case]</ta>
            <ta e="T499" id="Seg_6503" s="T498">nprop.[n:case]</ta>
            <ta e="T500" id="Seg_6504" s="T499">conj</ta>
            <ta e="T501" id="Seg_6505" s="T500">pers</ta>
            <ta e="T502" id="Seg_6506" s="T501">n.[n:case]-n:poss</ta>
            <ta e="T503" id="Seg_6507" s="T502">v-v:tense-v:pn</ta>
            <ta e="T504" id="Seg_6508" s="T503">conj</ta>
            <ta e="T505" id="Seg_6509" s="T504">pers</ta>
            <ta e="T506" id="Seg_6510" s="T505">n-n:ins-n:case</ta>
            <ta e="T507" id="Seg_6511" s="T506">adv</ta>
            <ta e="T508" id="Seg_6512" s="T507">n-v:mood.pn</ta>
            <ta e="T509" id="Seg_6513" s="T508">pers</ta>
            <ta e="T510" id="Seg_6514" s="T509">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T511" id="Seg_6515" s="T510">quant</ta>
            <ta e="T512" id="Seg_6516" s="T511">v-v:mood.pn</ta>
            <ta e="T513" id="Seg_6517" s="T512">conj</ta>
            <ta e="T514" id="Seg_6518" s="T513">pers</ta>
            <ta e="T515" id="Seg_6519" s="T514">n-n:num.[n:case]-n:poss</ta>
            <ta e="T516" id="Seg_6520" s="T515">v-v:pn</ta>
            <ta e="T517" id="Seg_6521" s="T516">nprop-n:case</ta>
            <ta e="T518" id="Seg_6522" s="T517">pp</ta>
            <ta e="T519" id="Seg_6523" s="T518">interrog-n&gt;adj-n:ins-n-n:case</ta>
            <ta e="T520" id="Seg_6524" s="T519">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T521" id="Seg_6525" s="T520">conj</ta>
            <ta e="T522" id="Seg_6526" s="T521">ptcl</ta>
            <ta e="T523" id="Seg_6527" s="T522">quant</ta>
            <ta e="T524" id="Seg_6528" s="T523">pers</ta>
            <ta e="T525" id="Seg_6529" s="T524">v-v:ins-v:mood.pn</ta>
            <ta e="T526" id="Seg_6530" s="T525">pers</ta>
            <ta e="T527" id="Seg_6531" s="T526">adv</ta>
            <ta e="T528" id="Seg_6532" s="T527">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T529" id="Seg_6533" s="T528">pers</ta>
            <ta e="T530" id="Seg_6534" s="T529">n-n:case</ta>
            <ta e="T531" id="Seg_6535" s="T530">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T532" id="Seg_6536" s="T531">conj</ta>
            <ta e="T533" id="Seg_6537" s="T532">n-n:case</ta>
            <ta e="T534" id="Seg_6538" s="T533">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T535" id="Seg_6539" s="T534">conj</ta>
            <ta e="T536" id="Seg_6540" s="T535">n-n:case</ta>
            <ta e="T537" id="Seg_6541" s="T536">interrog-n:num.[n:case]</ta>
            <ta e="T538" id="Seg_6542" s="T537">v-v:mood-v:pn</ta>
            <ta e="T539" id="Seg_6543" s="T538">quant</ta>
            <ta e="T540" id="Seg_6544" s="T539">n-v:mood.pn</ta>
            <ta e="T541" id="Seg_6545" s="T540">n.[n:case]-n:poss</ta>
            <ta e="T542" id="Seg_6546" s="T541">ptcl</ta>
            <ta e="T543" id="Seg_6547" s="T542">v-v:pn</ta>
            <ta e="T544" id="Seg_6548" s="T543">conj</ta>
            <ta e="T545" id="Seg_6549" s="T544">ptcl</ta>
            <ta e="T546" id="Seg_6550" s="T545">v-v:tense-v:pn</ta>
            <ta e="T547" id="Seg_6551" s="T546">preverb</ta>
            <ta e="T548" id="Seg_6552" s="T547">v-v:tense.[v:pn]</ta>
            <ta e="T549" id="Seg_6553" s="T548">nprop-n:case</ta>
            <ta e="T550" id="Seg_6554" s="T549">pp</ta>
            <ta e="T551" id="Seg_6555" s="T550">v-v:mood.pn</ta>
            <ta e="T552" id="Seg_6556" s="T551">conj</ta>
            <ta e="T553" id="Seg_6557" s="T552">v-v&gt;v-v:pn</ta>
            <ta e="T554" id="Seg_6558" s="T553">v-v&gt;v-v:pn</ta>
            <ta e="T555" id="Seg_6559" s="T554">pers</ta>
            <ta e="T556" id="Seg_6560" s="T555">pers-n:case</ta>
            <ta e="T557" id="Seg_6561" s="T556">n-n:ins-n:case</ta>
            <ta e="T558" id="Seg_6562" s="T557">v-v:tense-v:pn</ta>
            <ta e="T559" id="Seg_6563" s="T558">pers.[n:case]</ta>
            <ta e="T560" id="Seg_6564" s="T559">pers</ta>
            <ta e="T561" id="Seg_6565" s="T560">ptcl</ta>
            <ta e="T562" id="Seg_6566" s="T561">v-v&gt;v-v:pn</ta>
            <ta e="T563" id="Seg_6567" s="T562">n.[n:case]</ta>
            <ta e="T564" id="Seg_6568" s="T563">nprop.[n:case]</ta>
            <ta e="T565" id="Seg_6569" s="T564">adj</ta>
            <ta e="T566" id="Seg_6570" s="T565">n-n:case</ta>
            <ta e="T567" id="Seg_6571" s="T566">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_6572" s="T1">pers</ta>
            <ta e="T3" id="Seg_6573" s="T2">adv</ta>
            <ta e="T4" id="Seg_6574" s="T3">v</ta>
            <ta e="T5" id="Seg_6575" s="T4">nprop</ta>
            <ta e="T6" id="Seg_6576" s="T5">nprop</ta>
            <ta e="T7" id="Seg_6577" s="T6">v</ta>
            <ta e="T8" id="Seg_6578" s="T7">pers</ta>
            <ta e="T9" id="Seg_6579" s="T8">pp</ta>
            <ta e="T10" id="Seg_6580" s="T9">v</ta>
            <ta e="T11" id="Seg_6581" s="T10">conj</ta>
            <ta e="T12" id="Seg_6582" s="T11">v</ta>
            <ta e="T13" id="Seg_6583" s="T12">interrog</ta>
            <ta e="T14" id="Seg_6584" s="T13">v</ta>
            <ta e="T15" id="Seg_6585" s="T14">pers</ta>
            <ta e="T16" id="Seg_6586" s="T15">pers</ta>
            <ta e="T17" id="Seg_6587" s="T16">adv</ta>
            <ta e="T18" id="Seg_6588" s="T17">v</ta>
            <ta e="T19" id="Seg_6589" s="T18">pers</ta>
            <ta e="T20" id="Seg_6590" s="T19">pp</ta>
            <ta e="T21" id="Seg_6591" s="T20">pers</ta>
            <ta e="T22" id="Seg_6592" s="T21">v</ta>
            <ta e="T23" id="Seg_6593" s="T22">pers</ta>
            <ta e="T24" id="Seg_6594" s="T23">n</ta>
            <ta e="T25" id="Seg_6595" s="T24">pers</ta>
            <ta e="T26" id="Seg_6596" s="T25">pers</ta>
            <ta e="T27" id="Seg_6597" s="T26">v</ta>
            <ta e="T28" id="Seg_6598" s="T27">pers</ta>
            <ta e="T29" id="Seg_6599" s="T28">pers</ta>
            <ta e="T30" id="Seg_6600" s="T29">nprop</ta>
            <ta e="T31" id="Seg_6601" s="T30">v</ta>
            <ta e="T32" id="Seg_6602" s="T31">pers</ta>
            <ta e="T33" id="Seg_6603" s="T32">pers</ta>
            <ta e="T34" id="Seg_6604" s="T33">conj</ta>
            <ta e="T35" id="Seg_6605" s="T34">v</ta>
            <ta e="T36" id="Seg_6606" s="T35">pers</ta>
            <ta e="T37" id="Seg_6607" s="T36">pers</ta>
            <ta e="T38" id="Seg_6608" s="T37">quant</ta>
            <ta e="T39" id="Seg_6609" s="T38">v</ta>
            <ta e="T40" id="Seg_6610" s="T39">pers</ta>
            <ta e="T41" id="Seg_6611" s="T40">v</ta>
            <ta e="T42" id="Seg_6612" s="T41">n</ta>
            <ta e="T43" id="Seg_6613" s="T42">nprop</ta>
            <ta e="T44" id="Seg_6614" s="T43">n</ta>
            <ta e="T45" id="Seg_6615" s="T44">v</ta>
            <ta e="T46" id="Seg_6616" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_6617" s="T46">v</ta>
            <ta e="T48" id="Seg_6618" s="T47">interj</ta>
            <ta e="T49" id="Seg_6619" s="T48">nprop</ta>
            <ta e="T50" id="Seg_6620" s="T49">nprop</ta>
            <ta e="T51" id="Seg_6621" s="T50">pers</ta>
            <ta e="T52" id="Seg_6622" s="T51">pers</ta>
            <ta e="T53" id="Seg_6623" s="T52">adj</ta>
            <ta e="T54" id="Seg_6624" s="T53">n</ta>
            <ta e="T55" id="Seg_6625" s="T54">v</ta>
            <ta e="T56" id="Seg_6626" s="T55">adv</ta>
            <ta e="T57" id="Seg_6627" s="T56">pers</ta>
            <ta e="T58" id="Seg_6628" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_6629" s="T58">v</ta>
            <ta e="T60" id="Seg_6630" s="T59">pers</ta>
            <ta e="T61" id="Seg_6631" s="T60">v</ta>
            <ta e="T62" id="Seg_6632" s="T61">adv</ta>
            <ta e="T63" id="Seg_6633" s="T62">n</ta>
            <ta e="T64" id="Seg_6634" s="T63">n</ta>
            <ta e="T65" id="Seg_6635" s="T64">n</ta>
            <ta e="T66" id="Seg_6636" s="T65">adj</ta>
            <ta e="T67" id="Seg_6637" s="T66">adj</ta>
            <ta e="T68" id="Seg_6638" s="T67">n</ta>
            <ta e="T69" id="Seg_6639" s="T68">nprop</ta>
            <ta e="T70" id="Seg_6640" s="T69">n</ta>
            <ta e="T71" id="Seg_6641" s="T70">v</ta>
            <ta e="T72" id="Seg_6642" s="T71">n</ta>
            <ta e="T73" id="Seg_6643" s="T72">adv</ta>
            <ta e="T74" id="Seg_6644" s="T73">v</ta>
            <ta e="T75" id="Seg_6645" s="T74">nprop</ta>
            <ta e="T76" id="Seg_6646" s="T75">n</ta>
            <ta e="T77" id="Seg_6647" s="T76">n</ta>
            <ta e="T78" id="Seg_6648" s="T77">adv</ta>
            <ta e="T79" id="Seg_6649" s="T78">v</ta>
            <ta e="T80" id="Seg_6650" s="T79">pers</ta>
            <ta e="T81" id="Seg_6651" s="T80">v</ta>
            <ta e="T82" id="Seg_6652" s="T81">conj</ta>
            <ta e="T83" id="Seg_6653" s="T82">nprop</ta>
            <ta e="T84" id="Seg_6654" s="T83">interrog</ta>
            <ta e="T85" id="Seg_6655" s="T84">ptcl</ta>
            <ta e="T86" id="Seg_6656" s="T85">v</ta>
            <ta e="T87" id="Seg_6657" s="T86">adv</ta>
            <ta e="T88" id="Seg_6658" s="T87">n</ta>
            <ta e="T89" id="Seg_6659" s="T88">v</ta>
            <ta e="T90" id="Seg_6660" s="T89">pers</ta>
            <ta e="T91" id="Seg_6661" s="T90">adv</ta>
            <ta e="T92" id="Seg_6662" s="T91">n</ta>
            <ta e="T93" id="Seg_6663" s="T92">v</ta>
            <ta e="T94" id="Seg_6664" s="T93">n</ta>
            <ta e="T95" id="Seg_6665" s="T94">adv</ta>
            <ta e="T96" id="Seg_6666" s="T95">adv</ta>
            <ta e="T97" id="Seg_6667" s="T96">v</ta>
            <ta e="T98" id="Seg_6668" s="T97">n</ta>
            <ta e="T99" id="Seg_6669" s="T98">n</ta>
            <ta e="T100" id="Seg_6670" s="T99">n</ta>
            <ta e="T101" id="Seg_6671" s="T100">n</ta>
            <ta e="T102" id="Seg_6672" s="T101">adv</ta>
            <ta e="T103" id="Seg_6673" s="T102">n</ta>
            <ta e="T104" id="Seg_6674" s="T103">v</ta>
            <ta e="T105" id="Seg_6675" s="T104">pers</ta>
            <ta e="T106" id="Seg_6676" s="T105">n</ta>
            <ta e="T107" id="Seg_6677" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_6678" s="T107">v</ta>
            <ta e="T109" id="Seg_6679" s="T108">adv</ta>
            <ta e="T110" id="Seg_6680" s="T109">v</ta>
            <ta e="T111" id="Seg_6681" s="T110">adv</ta>
            <ta e="T112" id="Seg_6682" s="T111">pers</ta>
            <ta e="T113" id="Seg_6683" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_6684" s="T113">v</ta>
            <ta e="T115" id="Seg_6685" s="T114">n</ta>
            <ta e="T116" id="Seg_6686" s="T115">pers</ta>
            <ta e="T117" id="Seg_6687" s="T116">v</ta>
            <ta e="T118" id="Seg_6688" s="T117">n</ta>
            <ta e="T119" id="Seg_6689" s="T118">pers</ta>
            <ta e="T120" id="Seg_6690" s="T119">dem</ta>
            <ta e="T121" id="Seg_6691" s="T120">v</ta>
            <ta e="T122" id="Seg_6692" s="T121">adj</ta>
            <ta e="T123" id="Seg_6693" s="T122">n</ta>
            <ta e="T124" id="Seg_6694" s="T123">adv</ta>
            <ta e="T125" id="Seg_6695" s="T124">v</ta>
            <ta e="T126" id="Seg_6696" s="T125">n</ta>
            <ta e="T127" id="Seg_6697" s="T126">n</ta>
            <ta e="T128" id="Seg_6698" s="T127">dem</ta>
            <ta e="T129" id="Seg_6699" s="T128">v</ta>
            <ta e="T130" id="Seg_6700" s="T129">conj</ta>
            <ta e="T131" id="Seg_6701" s="T130">nprop</ta>
            <ta e="T132" id="Seg_6702" s="T131">adj</ta>
            <ta e="T133" id="Seg_6703" s="T132">n</ta>
            <ta e="T134" id="Seg_6704" s="T133">adv</ta>
            <ta e="T135" id="Seg_6705" s="T134">v</ta>
            <ta e="T136" id="Seg_6706" s="T135">pers</ta>
            <ta e="T137" id="Seg_6707" s="T136">v</ta>
            <ta e="T138" id="Seg_6708" s="T137">pers</ta>
            <ta e="T139" id="Seg_6709" s="T138">adv</ta>
            <ta e="T140" id="Seg_6710" s="T139">v</ta>
            <ta e="T141" id="Seg_6711" s="T140">pers</ta>
            <ta e="T142" id="Seg_6712" s="T141">dem</ta>
            <ta e="T143" id="Seg_6713" s="T142">n</ta>
            <ta e="T144" id="Seg_6714" s="T143">v</ta>
            <ta e="T145" id="Seg_6715" s="T144">nprop</ta>
            <ta e="T146" id="Seg_6716" s="T145">pers</ta>
            <ta e="T147" id="Seg_6717" s="T146">n</ta>
            <ta e="T148" id="Seg_6718" s="T147">nprop</ta>
            <ta e="T149" id="Seg_6719" s="T148">nprop</ta>
            <ta e="T150" id="Seg_6720" s="T149">pers</ta>
            <ta e="T151" id="Seg_6721" s="T150">pers</ta>
            <ta e="T152" id="Seg_6722" s="T151">v</ta>
            <ta e="T153" id="Seg_6723" s="T152">pers</ta>
            <ta e="T154" id="Seg_6724" s="T153">v</ta>
            <ta e="T155" id="Seg_6725" s="T154">adv</ta>
            <ta e="T156" id="Seg_6726" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_6727" s="T156">v</ta>
            <ta e="T158" id="Seg_6728" s="T157">pers</ta>
            <ta e="T159" id="Seg_6729" s="T158">emphpro</ta>
            <ta e="T160" id="Seg_6730" s="T159">adv</ta>
            <ta e="T161" id="Seg_6731" s="T160">v</ta>
            <ta e="T162" id="Seg_6732" s="T161">interrog</ta>
            <ta e="T163" id="Seg_6733" s="T162">v</ta>
            <ta e="T164" id="Seg_6734" s="T163">pers</ta>
            <ta e="T165" id="Seg_6735" s="T164">num</ta>
            <ta e="T166" id="Seg_6736" s="T165">n</ta>
            <ta e="T167" id="Seg_6737" s="T166">n</ta>
            <ta e="T168" id="Seg_6738" s="T167">n</ta>
            <ta e="T169" id="Seg_6739" s="T168">n</ta>
            <ta e="T170" id="Seg_6740" s="T169">adj</ta>
            <ta e="T171" id="Seg_6741" s="T170">n</ta>
            <ta e="T172" id="Seg_6742" s="T171">adj</ta>
            <ta e="T175" id="Seg_6743" s="T174">n</ta>
            <ta e="T176" id="Seg_6744" s="T175">n</ta>
            <ta e="T177" id="Seg_6745" s="T176">v</ta>
            <ta e="T178" id="Seg_6746" s="T177">ptcp</ta>
            <ta e="T179" id="Seg_6747" s="T178">n</ta>
            <ta e="T180" id="Seg_6748" s="T179">conj</ta>
            <ta e="T181" id="Seg_6749" s="T180">ptcp</ta>
            <ta e="T182" id="Seg_6750" s="T181">n</ta>
            <ta e="T183" id="Seg_6751" s="T182">n</ta>
            <ta e="T184" id="Seg_6752" s="T183">quant</ta>
            <ta e="T185" id="Seg_6753" s="T184">n</ta>
            <ta e="T186" id="Seg_6754" s="T185">v</ta>
            <ta e="T187" id="Seg_6755" s="T186">num</ta>
            <ta e="T188" id="Seg_6756" s="T187">n</ta>
            <ta e="T189" id="Seg_6757" s="T188">v</ta>
            <ta e="T190" id="Seg_6758" s="T189">adv</ta>
            <ta e="T191" id="Seg_6759" s="T190">ptcp</ta>
            <ta e="T192" id="Seg_6760" s="T191">n</ta>
            <ta e="T193" id="Seg_6761" s="T192">conj</ta>
            <ta e="T194" id="Seg_6762" s="T193">ptcp</ta>
            <ta e="T195" id="Seg_6763" s="T194">n</ta>
            <ta e="T196" id="Seg_6764" s="T195">n</ta>
            <ta e="T197" id="Seg_6765" s="T196">v</ta>
            <ta e="T198" id="Seg_6766" s="T197">conj</ta>
            <ta e="T199" id="Seg_6767" s="T198">v</ta>
            <ta e="T200" id="Seg_6768" s="T199">quant</ta>
            <ta e="T201" id="Seg_6769" s="T200">v</ta>
            <ta e="T202" id="Seg_6770" s="T201">v</ta>
            <ta e="T203" id="Seg_6771" s="T202">conj</ta>
            <ta e="T204" id="Seg_6772" s="T203">interrog</ta>
            <ta e="T205" id="Seg_6773" s="T204">n</ta>
            <ta e="T206" id="Seg_6774" s="T205">v</ta>
            <ta e="T207" id="Seg_6775" s="T206">n</ta>
            <ta e="T208" id="Seg_6776" s="T207">v</ta>
            <ta e="T209" id="Seg_6777" s="T208">n</ta>
            <ta e="T210" id="Seg_6778" s="T209">adv</ta>
            <ta e="T211" id="Seg_6779" s="T210">v</ta>
            <ta e="T212" id="Seg_6780" s="T211">quant</ta>
            <ta e="T213" id="Seg_6781" s="T212">v</ta>
            <ta e="T214" id="Seg_6782" s="T213">v</ta>
            <ta e="T215" id="Seg_6783" s="T214">n</ta>
            <ta e="T216" id="Seg_6784" s="T215">adv</ta>
            <ta e="T217" id="Seg_6785" s="T216">adj</ta>
            <ta e="T218" id="Seg_6786" s="T217">num</ta>
            <ta e="T219" id="Seg_6787" s="T218">n</ta>
            <ta e="T220" id="Seg_6788" s="T219">n</ta>
            <ta e="T221" id="Seg_6789" s="T220">quant</ta>
            <ta e="T222" id="Seg_6790" s="T221">num</ta>
            <ta e="T223" id="Seg_6791" s="T222">pp</ta>
            <ta e="T224" id="Seg_6792" s="T223">n</ta>
            <ta e="T225" id="Seg_6793" s="T224">n</ta>
            <ta e="T226" id="Seg_6794" s="T225">adj</ta>
            <ta e="T227" id="Seg_6795" s="T226">ptcl</ta>
            <ta e="T228" id="Seg_6796" s="T227">adv</ta>
            <ta e="T229" id="Seg_6797" s="T228">n</ta>
            <ta e="T230" id="Seg_6798" s="T229">n</ta>
            <ta e="T231" id="Seg_6799" s="T230">n</ta>
            <ta e="T232" id="Seg_6800" s="T231">n</ta>
            <ta e="T233" id="Seg_6801" s="T232">n</ta>
            <ta e="T234" id="Seg_6802" s="T233">n</ta>
            <ta e="T235" id="Seg_6803" s="T234">n</ta>
            <ta e="T236" id="Seg_6804" s="T235">adv</ta>
            <ta e="T237" id="Seg_6805" s="T236">adv</ta>
            <ta e="T238" id="Seg_6806" s="T237">adv</ta>
            <ta e="T239" id="Seg_6807" s="T238">n</ta>
            <ta e="T240" id="Seg_6808" s="T239">v</ta>
            <ta e="T241" id="Seg_6809" s="T240">pers</ta>
            <ta e="T242" id="Seg_6810" s="T241">adv</ta>
            <ta e="T243" id="Seg_6811" s="T242">n</ta>
            <ta e="T244" id="Seg_6812" s="T243">v</ta>
            <ta e="T245" id="Seg_6813" s="T244">adv</ta>
            <ta e="T246" id="Seg_6814" s="T245">quant</ta>
            <ta e="T247" id="Seg_6815" s="T246">v</ta>
            <ta e="T248" id="Seg_6816" s="T247">n</ta>
            <ta e="T249" id="Seg_6817" s="T248">n</ta>
            <ta e="T250" id="Seg_6818" s="T249">n</ta>
            <ta e="T251" id="Seg_6819" s="T250">v</ta>
            <ta e="T252" id="Seg_6820" s="T251">n</ta>
            <ta e="T253" id="Seg_6821" s="T252">n</ta>
            <ta e="T254" id="Seg_6822" s="T253">v</ta>
            <ta e="T255" id="Seg_6823" s="T254">n</ta>
            <ta e="T256" id="Seg_6824" s="T255">n</ta>
            <ta e="T257" id="Seg_6825" s="T256">n</ta>
            <ta e="T258" id="Seg_6826" s="T257">quant</ta>
            <ta e="T259" id="Seg_6827" s="T258">adj</ta>
            <ta e="T260" id="Seg_6828" s="T259">v</ta>
            <ta e="T261" id="Seg_6829" s="T260">conj</ta>
            <ta e="T262" id="Seg_6830" s="T261">pers</ta>
            <ta e="T263" id="Seg_6831" s="T262">v</ta>
            <ta e="T264" id="Seg_6832" s="T263">nprop</ta>
            <ta e="T265" id="Seg_6833" s="T264">v</ta>
            <ta e="T266" id="Seg_6834" s="T265">n</ta>
            <ta e="T267" id="Seg_6835" s="T266">n</ta>
            <ta e="T268" id="Seg_6836" s="T267">adj</ta>
            <ta e="T269" id="Seg_6837" s="T268">n</ta>
            <ta e="T270" id="Seg_6838" s="T269">v</ta>
            <ta e="T271" id="Seg_6839" s="T270">adj</ta>
            <ta e="T272" id="Seg_6840" s="T271">n</ta>
            <ta e="T273" id="Seg_6841" s="T272">v</ta>
            <ta e="T274" id="Seg_6842" s="T273">pers</ta>
            <ta e="T275" id="Seg_6843" s="T274">quant</ta>
            <ta e="T276" id="Seg_6844" s="T275">v</ta>
            <ta e="T277" id="Seg_6845" s="T276">n</ta>
            <ta e="T278" id="Seg_6846" s="T277">n</ta>
            <ta e="T279" id="Seg_6847" s="T278">n</ta>
            <ta e="T280" id="Seg_6848" s="T279">v</ta>
            <ta e="T281" id="Seg_6849" s="T280">adv</ta>
            <ta e="T282" id="Seg_6850" s="T281">n</ta>
            <ta e="T283" id="Seg_6851" s="T282">adv</ta>
            <ta e="T284" id="Seg_6852" s="T283">v</ta>
            <ta e="T285" id="Seg_6853" s="T284">adj</ta>
            <ta e="T286" id="Seg_6854" s="T285">n</ta>
            <ta e="T287" id="Seg_6855" s="T286">adv</ta>
            <ta e="T288" id="Seg_6856" s="T287">v</ta>
            <ta e="T289" id="Seg_6857" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_6858" s="T289">v</ta>
            <ta e="T291" id="Seg_6859" s="T290">n</ta>
            <ta e="T292" id="Seg_6860" s="T291">v</ta>
            <ta e="T293" id="Seg_6861" s="T292">n</ta>
            <ta e="T294" id="Seg_6862" s="T293">pers</ta>
            <ta e="T295" id="Seg_6863" s="T294">v</ta>
            <ta e="T296" id="Seg_6864" s="T295">n</ta>
            <ta e="T297" id="Seg_6865" s="T296">adv</ta>
            <ta e="T298" id="Seg_6866" s="T297">adv</ta>
            <ta e="T299" id="Seg_6867" s="T298">v</ta>
            <ta e="T300" id="Seg_6868" s="T299">n</ta>
            <ta e="T301" id="Seg_6869" s="T300">n</ta>
            <ta e="T302" id="Seg_6870" s="T301">adv</ta>
            <ta e="T303" id="Seg_6871" s="T302">conj</ta>
            <ta e="T304" id="Seg_6872" s="T303">v</ta>
            <ta e="T305" id="Seg_6873" s="T304">pers</ta>
            <ta e="T306" id="Seg_6874" s="T305">n</ta>
            <ta e="T307" id="Seg_6875" s="T306">adv</ta>
            <ta e="T308" id="Seg_6876" s="T307">v</ta>
            <ta e="T309" id="Seg_6877" s="T308">adv</ta>
            <ta e="T310" id="Seg_6878" s="T309">n</ta>
            <ta e="T311" id="Seg_6879" s="T310">v</ta>
            <ta e="T312" id="Seg_6880" s="T311">n</ta>
            <ta e="T313" id="Seg_6881" s="T312">n</ta>
            <ta e="T314" id="Seg_6882" s="T313">v</ta>
            <ta e="T315" id="Seg_6883" s="T314">conj</ta>
            <ta e="T316" id="Seg_6884" s="T315">n</ta>
            <ta e="T317" id="Seg_6885" s="T316">n</ta>
            <ta e="T318" id="Seg_6886" s="T317">n</ta>
            <ta e="T319" id="Seg_6887" s="T318">n</ta>
            <ta e="T320" id="Seg_6888" s="T319">v</ta>
            <ta e="T321" id="Seg_6889" s="T320">n</ta>
            <ta e="T322" id="Seg_6890" s="T321">pers</ta>
            <ta e="T323" id="Seg_6891" s="T322">n</ta>
            <ta e="T324" id="Seg_6892" s="T323">v</ta>
            <ta e="T325" id="Seg_6893" s="T324">n</ta>
            <ta e="T326" id="Seg_6894" s="T325">pers</ta>
            <ta e="T327" id="Seg_6895" s="T326">v</ta>
            <ta e="T328" id="Seg_6896" s="T327">pers</ta>
            <ta e="T329" id="Seg_6897" s="T328">num</ta>
            <ta e="T330" id="Seg_6898" s="T329">n</ta>
            <ta e="T331" id="Seg_6899" s="T330">v</ta>
            <ta e="T332" id="Seg_6900" s="T331">pers</ta>
            <ta e="T333" id="Seg_6901" s="T332">pers</ta>
            <ta e="T334" id="Seg_6902" s="T333">v</ta>
            <ta e="T335" id="Seg_6903" s="T334">conj</ta>
            <ta e="T336" id="Seg_6904" s="T335">pers</ta>
            <ta e="T337" id="Seg_6905" s="T336">pers</ta>
            <ta e="T338" id="Seg_6906" s="T337">v</ta>
            <ta e="T339" id="Seg_6907" s="T338">pers</ta>
            <ta e="T340" id="Seg_6908" s="T339">v</ta>
            <ta e="T341" id="Seg_6909" s="T340">adj</ta>
            <ta e="T342" id="Seg_6910" s="T341">adv</ta>
            <ta e="T343" id="Seg_6911" s="T342">v</ta>
            <ta e="T344" id="Seg_6912" s="T343">ptcl</ta>
            <ta e="T345" id="Seg_6913" s="T344">v</ta>
            <ta e="T346" id="Seg_6914" s="T345">conj</ta>
            <ta e="T347" id="Seg_6915" s="T346">emphpro</ta>
            <ta e="T348" id="Seg_6916" s="T347">adv</ta>
            <ta e="T349" id="Seg_6917" s="T348">n</ta>
            <ta e="T350" id="Seg_6918" s="T349">pers</ta>
            <ta e="T351" id="Seg_6919" s="T350">adv</ta>
            <ta e="T352" id="Seg_6920" s="T351">nprop</ta>
            <ta e="T353" id="Seg_6921" s="T352">v</ta>
            <ta e="T354" id="Seg_6922" s="T353">v</ta>
            <ta e="T355" id="Seg_6923" s="T354">n</ta>
            <ta e="T356" id="Seg_6924" s="T355">v</ta>
            <ta e="T357" id="Seg_6925" s="T356">n</ta>
            <ta e="T358" id="Seg_6926" s="T357">adj</ta>
            <ta e="T359" id="Seg_6927" s="T358">v</ta>
            <ta e="T360" id="Seg_6928" s="T359">pers</ta>
            <ta e="T361" id="Seg_6929" s="T360">ptcl</ta>
            <ta e="T362" id="Seg_6930" s="T361">v</ta>
            <ta e="T363" id="Seg_6931" s="T362">conj</ta>
            <ta e="T364" id="Seg_6932" s="T363">pers</ta>
            <ta e="T365" id="Seg_6933" s="T364">v</ta>
            <ta e="T366" id="Seg_6934" s="T365">n</ta>
            <ta e="T367" id="Seg_6935" s="T366">pers</ta>
            <ta e="T368" id="Seg_6936" s="T367">dem</ta>
            <ta e="T369" id="Seg_6937" s="T368">n</ta>
            <ta e="T370" id="Seg_6938" s="T369">v</ta>
            <ta e="T371" id="Seg_6939" s="T370">pers</ta>
            <ta e="T372" id="Seg_6940" s="T371">v</ta>
            <ta e="T373" id="Seg_6941" s="T372">adv</ta>
            <ta e="T374" id="Seg_6942" s="T373">adj</ta>
            <ta e="T375" id="Seg_6943" s="T374">n</ta>
            <ta e="T376" id="Seg_6944" s="T375">v</ta>
            <ta e="T379" id="Seg_6945" s="T378">v</ta>
            <ta e="T380" id="Seg_6946" s="T379">pers</ta>
            <ta e="T381" id="Seg_6947" s="T380">nprop</ta>
            <ta e="T382" id="Seg_6948" s="T381">n</ta>
            <ta e="T383" id="Seg_6949" s="T382">v</ta>
            <ta e="T384" id="Seg_6950" s="T383">quant</ta>
            <ta e="T385" id="Seg_6951" s="T384">v</ta>
            <ta e="T386" id="Seg_6952" s="T385">emphpro</ta>
            <ta e="T387" id="Seg_6953" s="T386">ptcl</ta>
            <ta e="T388" id="Seg_6954" s="T387">interrog</ta>
            <ta e="T389" id="Seg_6955" s="T388">v</ta>
            <ta e="T390" id="Seg_6956" s="T389">v</ta>
            <ta e="T391" id="Seg_6957" s="T390">pers</ta>
            <ta e="T392" id="Seg_6958" s="T391">v</ta>
            <ta e="T393" id="Seg_6959" s="T392">v</ta>
            <ta e="T394" id="Seg_6960" s="T393">v</ta>
            <ta e="T395" id="Seg_6961" s="T394">v</ta>
            <ta e="T396" id="Seg_6962" s="T395">conj</ta>
            <ta e="T397" id="Seg_6963" s="T396">nprop</ta>
            <ta e="T398" id="Seg_6964" s="T397">pers</ta>
            <ta e="T399" id="Seg_6965" s="T398">v</ta>
            <ta e="T400" id="Seg_6966" s="T399">interrog</ta>
            <ta e="T401" id="Seg_6967" s="T400">v</ta>
            <ta e="T402" id="Seg_6968" s="T401">v</ta>
            <ta e="T403" id="Seg_6969" s="T402">n</ta>
            <ta e="T404" id="Seg_6970" s="T403">v</ta>
            <ta e="T405" id="Seg_6971" s="T404">v</ta>
            <ta e="T406" id="Seg_6972" s="T405">n</ta>
            <ta e="T407" id="Seg_6973" s="T406">quant</ta>
            <ta e="T408" id="Seg_6974" s="T407">n</ta>
            <ta e="T409" id="Seg_6975" s="T408">n</ta>
            <ta e="T410" id="Seg_6976" s="T409">v</ta>
            <ta e="T411" id="Seg_6977" s="T410">n</ta>
            <ta e="T412" id="Seg_6978" s="T411">v</ta>
            <ta e="T413" id="Seg_6979" s="T412">pers</ta>
            <ta e="T414" id="Seg_6980" s="T413">pers</ta>
            <ta e="T415" id="Seg_6981" s="T414">v</ta>
            <ta e="T416" id="Seg_6982" s="T415">adv</ta>
            <ta e="T417" id="Seg_6983" s="T416">interrog</ta>
            <ta e="T418" id="Seg_6984" s="T417">ptcl</ta>
            <ta e="T419" id="Seg_6985" s="T418">v</ta>
            <ta e="T420" id="Seg_6986" s="T419">conj</ta>
            <ta e="T421" id="Seg_6987" s="T420">pers</ta>
            <ta e="T422" id="Seg_6988" s="T421">ptcl</ta>
            <ta e="T423" id="Seg_6989" s="T422">v</ta>
            <ta e="T424" id="Seg_6990" s="T423">adv</ta>
            <ta e="T425" id="Seg_6991" s="T424">pers</ta>
            <ta e="T426" id="Seg_6992" s="T425">v</ta>
            <ta e="T427" id="Seg_6993" s="T426">conj</ta>
            <ta e="T428" id="Seg_6994" s="T427">n</ta>
            <ta e="T429" id="Seg_6995" s="T428">v</ta>
            <ta e="T430" id="Seg_6996" s="T429">pers</ta>
            <ta e="T431" id="Seg_6997" s="T430">num</ta>
            <ta e="T432" id="Seg_6998" s="T431">adj</ta>
            <ta e="T433" id="Seg_6999" s="T432">v</ta>
            <ta e="T434" id="Seg_7000" s="T433">interrog</ta>
            <ta e="T435" id="Seg_7001" s="T434">v</ta>
            <ta e="T436" id="Seg_7002" s="T435">n</ta>
            <ta e="T437" id="Seg_7003" s="T436">pers</ta>
            <ta e="T438" id="Seg_7004" s="T437">pers</ta>
            <ta e="T439" id="Seg_7005" s="T438">v</ta>
            <ta e="T440" id="Seg_7006" s="T439">adv</ta>
            <ta e="T441" id="Seg_7007" s="T440">pers</ta>
            <ta e="T442" id="Seg_7008" s="T441">v</ta>
            <ta e="T443" id="Seg_7009" s="T442">conj</ta>
            <ta e="T444" id="Seg_7010" s="T443">v</ta>
            <ta e="T445" id="Seg_7011" s="T444">pers</ta>
            <ta e="T446" id="Seg_7012" s="T445">v</ta>
            <ta e="T447" id="Seg_7013" s="T446">n</ta>
            <ta e="T448" id="Seg_7014" s="T447">preverb</ta>
            <ta e="T449" id="Seg_7015" s="T448">v</ta>
            <ta e="T450" id="Seg_7016" s="T449">v</ta>
            <ta e="T451" id="Seg_7017" s="T450">conj</ta>
            <ta e="T452" id="Seg_7018" s="T451">adv</ta>
            <ta e="T453" id="Seg_7019" s="T452">v</ta>
            <ta e="T454" id="Seg_7020" s="T453">n</ta>
            <ta e="T455" id="Seg_7021" s="T454">v</ta>
            <ta e="T456" id="Seg_7022" s="T455">adj</ta>
            <ta e="T457" id="Seg_7023" s="T456">n</ta>
            <ta e="T458" id="Seg_7024" s="T457">adv</ta>
            <ta e="T459" id="Seg_7025" s="T458">v</ta>
            <ta e="T460" id="Seg_7026" s="T459">pers</ta>
            <ta e="T461" id="Seg_7027" s="T460">n</ta>
            <ta e="T462" id="Seg_7028" s="T461">ptcl</ta>
            <ta e="T463" id="Seg_7029" s="T462">v</ta>
            <ta e="T464" id="Seg_7030" s="T463">emphpro</ta>
            <ta e="T465" id="Seg_7031" s="T464">ptcl</ta>
            <ta e="T466" id="Seg_7032" s="T465">v</ta>
            <ta e="T467" id="Seg_7033" s="T466">n</ta>
            <ta e="T468" id="Seg_7034" s="T467">conj</ta>
            <ta e="T469" id="Seg_7035" s="T468">v</ta>
            <ta e="T470" id="Seg_7036" s="T469">pers</ta>
            <ta e="T471" id="Seg_7037" s="T470">adv</ta>
            <ta e="T472" id="Seg_7038" s="T471">n</ta>
            <ta e="T473" id="Seg_7039" s="T472">v</ta>
            <ta e="T474" id="Seg_7040" s="T473">v</ta>
            <ta e="T475" id="Seg_7041" s="T474">n</ta>
            <ta e="T476" id="Seg_7042" s="T475">nprop</ta>
            <ta e="T477" id="Seg_7043" s="T476">conj</ta>
            <ta e="T478" id="Seg_7044" s="T477">nprop</ta>
            <ta e="T479" id="Seg_7045" s="T478">v</ta>
            <ta e="T480" id="Seg_7046" s="T479">nprop</ta>
            <ta e="T481" id="Seg_7047" s="T480">v</ta>
            <ta e="T482" id="Seg_7048" s="T481">nprop</ta>
            <ta e="T483" id="Seg_7049" s="T482">n</ta>
            <ta e="T484" id="Seg_7050" s="T483">pers</ta>
            <ta e="T485" id="Seg_7051" s="T484">v</ta>
            <ta e="T486" id="Seg_7052" s="T485">nprop</ta>
            <ta e="T487" id="Seg_7053" s="T486">nprop</ta>
            <ta e="T488" id="Seg_7054" s="T487">n</ta>
            <ta e="T489" id="Seg_7055" s="T488">v</ta>
            <ta e="T490" id="Seg_7056" s="T489">n</ta>
            <ta e="T491" id="Seg_7057" s="T490">v</ta>
            <ta e="T492" id="Seg_7058" s="T491">n</ta>
            <ta e="T493" id="Seg_7059" s="T492">conj</ta>
            <ta e="T494" id="Seg_7060" s="T493">pers</ta>
            <ta e="T495" id="Seg_7061" s="T494">v</ta>
            <ta e="T496" id="Seg_7062" s="T495">n</ta>
            <ta e="T497" id="Seg_7063" s="T496">v</ta>
            <ta e="T498" id="Seg_7064" s="T497">nprop</ta>
            <ta e="T499" id="Seg_7065" s="T498">nprop</ta>
            <ta e="T500" id="Seg_7066" s="T499">conj</ta>
            <ta e="T501" id="Seg_7067" s="T500">pers</ta>
            <ta e="T502" id="Seg_7068" s="T501">n</ta>
            <ta e="T503" id="Seg_7069" s="T502">v</ta>
            <ta e="T504" id="Seg_7070" s="T503">conj</ta>
            <ta e="T505" id="Seg_7071" s="T504">pers</ta>
            <ta e="T506" id="Seg_7072" s="T505">n</ta>
            <ta e="T507" id="Seg_7073" s="T506">adv</ta>
            <ta e="T508" id="Seg_7074" s="T507">n</ta>
            <ta e="T509" id="Seg_7075" s="T508">pers</ta>
            <ta e="T510" id="Seg_7076" s="T509">v</ta>
            <ta e="T511" id="Seg_7077" s="T510">quant</ta>
            <ta e="T512" id="Seg_7078" s="T511">v</ta>
            <ta e="T513" id="Seg_7079" s="T512">conj</ta>
            <ta e="T514" id="Seg_7080" s="T513">pers</ta>
            <ta e="T515" id="Seg_7081" s="T514">n</ta>
            <ta e="T516" id="Seg_7082" s="T515">v</ta>
            <ta e="T517" id="Seg_7083" s="T516">nprop</ta>
            <ta e="T518" id="Seg_7084" s="T517">pp</ta>
            <ta e="T519" id="Seg_7085" s="T518">n</ta>
            <ta e="T520" id="Seg_7086" s="T519">v</ta>
            <ta e="T521" id="Seg_7087" s="T520">conj</ta>
            <ta e="T522" id="Seg_7088" s="T521">ptcl</ta>
            <ta e="T523" id="Seg_7089" s="T522">quant</ta>
            <ta e="T524" id="Seg_7090" s="T523">pers</ta>
            <ta e="T525" id="Seg_7091" s="T524">v</ta>
            <ta e="T526" id="Seg_7092" s="T525">pers</ta>
            <ta e="T527" id="Seg_7093" s="T526">adv</ta>
            <ta e="T528" id="Seg_7094" s="T527">v</ta>
            <ta e="T529" id="Seg_7095" s="T528">pers</ta>
            <ta e="T530" id="Seg_7096" s="T529">n</ta>
            <ta e="T531" id="Seg_7097" s="T530">v</ta>
            <ta e="T532" id="Seg_7098" s="T531">conj</ta>
            <ta e="T533" id="Seg_7099" s="T532">n</ta>
            <ta e="T534" id="Seg_7100" s="T533">v</ta>
            <ta e="T535" id="Seg_7101" s="T534">conj</ta>
            <ta e="T536" id="Seg_7102" s="T535">n</ta>
            <ta e="T537" id="Seg_7103" s="T536">interrog</ta>
            <ta e="T538" id="Seg_7104" s="T537">v</ta>
            <ta e="T539" id="Seg_7105" s="T538">quant</ta>
            <ta e="T540" id="Seg_7106" s="T539">n</ta>
            <ta e="T541" id="Seg_7107" s="T540">n</ta>
            <ta e="T542" id="Seg_7108" s="T541">ptcl</ta>
            <ta e="T543" id="Seg_7109" s="T542">v</ta>
            <ta e="T544" id="Seg_7110" s="T543">conj</ta>
            <ta e="T545" id="Seg_7111" s="T544">ptcl</ta>
            <ta e="T546" id="Seg_7112" s="T545">v</ta>
            <ta e="T547" id="Seg_7113" s="T546">preverb</ta>
            <ta e="T548" id="Seg_7114" s="T547">v</ta>
            <ta e="T549" id="Seg_7115" s="T548">nprop</ta>
            <ta e="T550" id="Seg_7116" s="T549">pp</ta>
            <ta e="T551" id="Seg_7117" s="T550">v</ta>
            <ta e="T552" id="Seg_7118" s="T551">conj</ta>
            <ta e="T553" id="Seg_7119" s="T552">v</ta>
            <ta e="T554" id="Seg_7120" s="T553">v</ta>
            <ta e="T555" id="Seg_7121" s="T554">pers</ta>
            <ta e="T556" id="Seg_7122" s="T555">pers</ta>
            <ta e="T557" id="Seg_7123" s="T556">n</ta>
            <ta e="T558" id="Seg_7124" s="T557">v</ta>
            <ta e="T559" id="Seg_7125" s="T558">pers</ta>
            <ta e="T560" id="Seg_7126" s="T559">pers</ta>
            <ta e="T561" id="Seg_7127" s="T560">ptcl</ta>
            <ta e="T562" id="Seg_7128" s="T561">v</ta>
            <ta e="T563" id="Seg_7129" s="T562">n</ta>
            <ta e="T564" id="Seg_7130" s="T563">nprop</ta>
            <ta e="T565" id="Seg_7131" s="T564">adj</ta>
            <ta e="T566" id="Seg_7132" s="T565">n</ta>
            <ta e="T567" id="Seg_7133" s="T566">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_7134" s="T1">pro.h:Th</ta>
            <ta e="T3" id="Seg_7135" s="T2">np:Time</ta>
            <ta e="T6" id="Seg_7136" s="T5">np:Com</ta>
            <ta e="T7" id="Seg_7137" s="T6">0.1.h:A</ta>
            <ta e="T8" id="Seg_7138" s="T7">pp:Th</ta>
            <ta e="T10" id="Seg_7139" s="T9">0.1.h:A</ta>
            <ta e="T12" id="Seg_7140" s="T11">0.2.h:Th</ta>
            <ta e="T14" id="Seg_7141" s="T13">0.2.h:A</ta>
            <ta e="T15" id="Seg_7142" s="T14">pro.h:A</ta>
            <ta e="T16" id="Seg_7143" s="T15">pro.h:R</ta>
            <ta e="T19" id="Seg_7144" s="T18">pp:Th</ta>
            <ta e="T21" id="Seg_7145" s="T20">pro.h:A</ta>
            <ta e="T23" id="Seg_7146" s="T22">pro.h:P</ta>
            <ta e="T25" id="Seg_7147" s="T24">pro.h:A</ta>
            <ta e="T26" id="Seg_7148" s="T25">pro.h:R</ta>
            <ta e="T28" id="Seg_7149" s="T27">pro.h:A</ta>
            <ta e="T29" id="Seg_7150" s="T28">pro.h:B</ta>
            <ta e="T30" id="Seg_7151" s="T29">np:Com</ta>
            <ta e="T32" id="Seg_7152" s="T31">pro.h:A</ta>
            <ta e="T33" id="Seg_7153" s="T32">pro:Com</ta>
            <ta e="T36" id="Seg_7154" s="T35">pro.h:A</ta>
            <ta e="T37" id="Seg_7155" s="T36">pro.h:R</ta>
            <ta e="T38" id="Seg_7156" s="T37">np:Th</ta>
            <ta e="T40" id="Seg_7157" s="T39">pro.h:A</ta>
            <ta e="T44" id="Seg_7158" s="T43">np:Th</ta>
            <ta e="T45" id="Seg_7159" s="T44">0.1.h:A</ta>
            <ta e="T47" id="Seg_7160" s="T46">0.3.h:A 0.3:Th</ta>
            <ta e="T51" id="Seg_7161" s="T50">pro.h:A</ta>
            <ta e="T52" id="Seg_7162" s="T51">pro.h:R</ta>
            <ta e="T54" id="Seg_7163" s="T53">np:Th</ta>
            <ta e="T57" id="Seg_7164" s="T56">pro.h:A</ta>
            <ta e="T60" id="Seg_7165" s="T59">pro.h:Th</ta>
            <ta e="T62" id="Seg_7166" s="T61">adv:Time</ta>
            <ta e="T63" id="Seg_7167" s="T62">np:L</ta>
            <ta e="T64" id="Seg_7168" s="T63">np:L</ta>
            <ta e="T65" id="Seg_7169" s="T64">np:Th</ta>
            <ta e="T68" id="Seg_7170" s="T67">np:So</ta>
            <ta e="T69" id="Seg_7171" s="T68">np:G</ta>
            <ta e="T70" id="Seg_7172" s="T69">np:Ins</ta>
            <ta e="T71" id="Seg_7173" s="T70">0.1.h:A</ta>
            <ta e="T72" id="Seg_7174" s="T71">np:L</ta>
            <ta e="T73" id="Seg_7175" s="T72">0.1.h:A</ta>
            <ta e="T74" id="Seg_7176" s="T73">0.1.h:A</ta>
            <ta e="T76" id="Seg_7177" s="T75">np:L</ta>
            <ta e="T77" id="Seg_7178" s="T76">np:Th</ta>
            <ta e="T78" id="Seg_7179" s="T77">adv:Time</ta>
            <ta e="T80" id="Seg_7180" s="T79">pro.h:A</ta>
            <ta e="T83" id="Seg_7181" s="T82">np.h:Th</ta>
            <ta e="T84" id="Seg_7182" s="T83">adv:L</ta>
            <ta e="T86" id="Seg_7183" s="T85">0.1.h:E</ta>
            <ta e="T87" id="Seg_7184" s="T86">adv:Time</ta>
            <ta e="T88" id="Seg_7185" s="T87">np:A</ta>
            <ta e="T90" id="Seg_7186" s="T89">pro.h:A</ta>
            <ta e="T91" id="Seg_7187" s="T90">adv:Time</ta>
            <ta e="T92" id="Seg_7188" s="T91">np:Path</ta>
            <ta e="T94" id="Seg_7189" s="T93">np:Th</ta>
            <ta e="T98" id="Seg_7190" s="T97">np:Poss</ta>
            <ta e="T99" id="Seg_7191" s="T98">np:L</ta>
            <ta e="T100" id="Seg_7192" s="T99">np:Th</ta>
            <ta e="T101" id="Seg_7193" s="T100">np:Th</ta>
            <ta e="T102" id="Seg_7194" s="T101">adv:L</ta>
            <ta e="T103" id="Seg_7195" s="T102">np:Th</ta>
            <ta e="T105" id="Seg_7196" s="T104">pro.h:E</ta>
            <ta e="T106" id="Seg_7197" s="T105">np:L</ta>
            <ta e="T110" id="Seg_7198" s="T109">0.3:A</ta>
            <ta e="T111" id="Seg_7199" s="T110">adv:Time</ta>
            <ta e="T112" id="Seg_7200" s="T111">pro.h:A</ta>
            <ta e="T115" id="Seg_7201" s="T114">np:Ins</ta>
            <ta e="T116" id="Seg_7202" s="T115">pro.h:Th</ta>
            <ta e="T118" id="Seg_7203" s="T117">np.h:L</ta>
            <ta e="T119" id="Seg_7204" s="T118">np.h:G</ta>
            <ta e="T121" id="Seg_7205" s="T120">0.3.h:A</ta>
            <ta e="T123" id="Seg_7206" s="T122">np:Th</ta>
            <ta e="T124" id="Seg_7207" s="T123">0.3.h:A</ta>
            <ta e="T125" id="Seg_7208" s="T124">0.3.h:A</ta>
            <ta e="T129" id="Seg_7209" s="T128">0.3.h:A</ta>
            <ta e="T131" id="Seg_7210" s="T130">np:G</ta>
            <ta e="T133" id="Seg_7211" s="T132">np:Th</ta>
            <ta e="T134" id="Seg_7212" s="T133">0.3.h:A</ta>
            <ta e="T135" id="Seg_7213" s="T134">0.3.h:A</ta>
            <ta e="T136" id="Seg_7214" s="T135">pro.h:G</ta>
            <ta e="T137" id="Seg_7215" s="T136">0.3.h:A</ta>
            <ta e="T138" id="Seg_7216" s="T137">pro:Com</ta>
            <ta e="T139" id="Seg_7217" s="T138">adv:Time</ta>
            <ta e="T140" id="Seg_7218" s="T139">0.3.h:A</ta>
            <ta e="T141" id="Seg_7219" s="T140">pro.h:Th</ta>
            <ta e="T143" id="Seg_7220" s="T142">np.h:L</ta>
            <ta e="T145" id="Seg_7221" s="T144">adv:L</ta>
            <ta e="T146" id="Seg_7222" s="T145">pro.h:Poss</ta>
            <ta e="T147" id="Seg_7223" s="T146">np:Th</ta>
            <ta e="T150" id="Seg_7224" s="T149">pro.h:Th</ta>
            <ta e="T151" id="Seg_7225" s="T150">pro.h:L</ta>
            <ta e="T153" id="Seg_7226" s="T152">pro.h:Th</ta>
            <ta e="T154" id="Seg_7227" s="T153">0.3.h:A</ta>
            <ta e="T157" id="Seg_7228" s="T156">0.3.h:A</ta>
            <ta e="T158" id="Seg_7229" s="T157">pro.h:A</ta>
            <ta e="T163" id="Seg_7230" s="T162">0.1.h:E</ta>
            <ta e="T164" id="Seg_7231" s="T163">pro.h:Poss</ta>
            <ta e="T166" id="Seg_7232" s="T165">np:Th</ta>
            <ta e="T167" id="Seg_7233" s="T166">np:L</ta>
            <ta e="T168" id="Seg_7234" s="T167">np:Poss</ta>
            <ta e="T169" id="Seg_7235" s="T168">np:Th</ta>
            <ta e="T171" id="Seg_7236" s="T170">np:Th</ta>
            <ta e="T175" id="Seg_7237" s="T174">np:Th</ta>
            <ta e="T176" id="Seg_7238" s="T175">np:L</ta>
            <ta e="T183" id="Seg_7239" s="T182">np:Th</ta>
            <ta e="T184" id="Seg_7240" s="T183">np:Th</ta>
            <ta e="T185" id="Seg_7241" s="T184">np:L</ta>
            <ta e="T188" id="Seg_7242" s="T187">np:L</ta>
            <ta e="T189" id="Seg_7243" s="T188">0.1.h:A</ta>
            <ta e="T190" id="Seg_7244" s="T189">adv:L</ta>
            <ta e="T192" id="Seg_7245" s="T191">np:Th</ta>
            <ta e="T195" id="Seg_7246" s="T194">np:Th</ta>
            <ta e="T196" id="Seg_7247" s="T195">np:L</ta>
            <ta e="T197" id="Seg_7248" s="T196">0.1.h:A</ta>
            <ta e="T199" id="Seg_7249" s="T198">0.1.h:A</ta>
            <ta e="T200" id="Seg_7250" s="T199">np:Th</ta>
            <ta e="T202" id="Seg_7251" s="T201">0.3:A</ta>
            <ta e="T205" id="Seg_7252" s="T204">np:Th</ta>
            <ta e="T207" id="Seg_7253" s="T206">np:Th</ta>
            <ta e="T208" id="Seg_7254" s="T207">0.2.h:A</ta>
            <ta e="T209" id="Seg_7255" s="T208">np:Th</ta>
            <ta e="T212" id="Seg_7256" s="T211">pro:Th</ta>
            <ta e="T214" id="Seg_7257" s="T213">0.3:A</ta>
            <ta e="T215" id="Seg_7258" s="T214">np:Th</ta>
            <ta e="T216" id="Seg_7259" s="T215">adv:L</ta>
            <ta e="T225" id="Seg_7260" s="T224">np:Th</ta>
            <ta e="T228" id="Seg_7261" s="T227">adv:L</ta>
            <ta e="T229" id="Seg_7262" s="T228">np:Th</ta>
            <ta e="T236" id="Seg_7263" s="T235">adv:L</ta>
            <ta e="T237" id="Seg_7264" s="T236">adv:Time</ta>
            <ta e="T239" id="Seg_7265" s="T238">np:P</ta>
            <ta e="T241" id="Seg_7266" s="T240">pro.h:A</ta>
            <ta e="T242" id="Seg_7267" s="T241">adv:Time</ta>
            <ta e="T243" id="Seg_7268" s="T242">np:Th</ta>
            <ta e="T245" id="Seg_7269" s="T244">adv:Time</ta>
            <ta e="T248" id="Seg_7270" s="T247">np:Th</ta>
            <ta e="T249" id="Seg_7271" s="T248">adv:Time</ta>
            <ta e="T250" id="Seg_7272" s="T249">np:Ins</ta>
            <ta e="T251" id="Seg_7273" s="T250">0.3.h:A</ta>
            <ta e="T252" id="Seg_7274" s="T251">np.h:A</ta>
            <ta e="T253" id="Seg_7275" s="T252">np:Ins</ta>
            <ta e="T255" id="Seg_7276" s="T254">np:Time</ta>
            <ta e="T257" id="Seg_7277" s="T256">np:Th</ta>
            <ta e="T262" id="Seg_7278" s="T261">pro.h:A</ta>
            <ta e="T264" id="Seg_7279" s="T263">np:G</ta>
            <ta e="T265" id="Seg_7280" s="T264">0.1.h:Th</ta>
            <ta e="T266" id="Seg_7281" s="T265">np:L</ta>
            <ta e="T267" id="Seg_7282" s="T266">np:L</ta>
            <ta e="T269" id="Seg_7283" s="T268">np:L</ta>
            <ta e="T270" id="Seg_7284" s="T269">0.1.h:Th</ta>
            <ta e="T272" id="Seg_7285" s="T271">np:L</ta>
            <ta e="T273" id="Seg_7286" s="T272">0.1.h:Th</ta>
            <ta e="T274" id="Seg_7287" s="T273">pro.h:R</ta>
            <ta e="T275" id="Seg_7288" s="T274">pro:Th</ta>
            <ta e="T276" id="Seg_7289" s="T275">0.3.h:A</ta>
            <ta e="T280" id="Seg_7290" s="T279">0.1.h:Th</ta>
            <ta e="T282" id="Seg_7291" s="T281">np.h:Th</ta>
            <ta e="T287" id="Seg_7292" s="T286">adv:L</ta>
            <ta e="T288" id="Seg_7293" s="T287">0.2.h:P</ta>
            <ta e="T290" id="Seg_7294" s="T289">0.2.h:A</ta>
            <ta e="T291" id="Seg_7295" s="T290">np:Time</ta>
            <ta e="T292" id="Seg_7296" s="T291">0.1.h:A</ta>
            <ta e="T293" id="Seg_7297" s="T292">np:G</ta>
            <ta e="T294" id="Seg_7298" s="T293">pro.h:A</ta>
            <ta e="T296" id="Seg_7299" s="T295">np:Th</ta>
            <ta e="T297" id="Seg_7300" s="T296">adv:L</ta>
            <ta e="T299" id="Seg_7301" s="T298">0.3:Th</ta>
            <ta e="T302" id="Seg_7302" s="T301">adv:L</ta>
            <ta e="T304" id="Seg_7303" s="T303">0.1.h:A</ta>
            <ta e="T306" id="Seg_7304" s="T305">np:Th</ta>
            <ta e="T307" id="Seg_7305" s="T306">adv:L</ta>
            <ta e="T309" id="Seg_7306" s="T308">adv:L</ta>
            <ta e="T310" id="Seg_7307" s="T309">np.h:A</ta>
            <ta e="T312" id="Seg_7308" s="T311">np:Ins</ta>
            <ta e="T313" id="Seg_7309" s="T312">np:P</ta>
            <ta e="T314" id="Seg_7310" s="T313">0.3.h:A</ta>
            <ta e="T316" id="Seg_7311" s="T315">np:P</ta>
            <ta e="T317" id="Seg_7312" s="T316">np.h:A</ta>
            <ta e="T318" id="Seg_7313" s="T317">np.h:Poss</ta>
            <ta e="T320" id="Seg_7314" s="T319">0.3:Th</ta>
            <ta e="T321" id="Seg_7315" s="T320">np:Th</ta>
            <ta e="T322" id="Seg_7316" s="T321">0.3.h:A</ta>
            <ta e="T323" id="Seg_7317" s="T322">np:P</ta>
            <ta e="T325" id="Seg_7318" s="T324">np:A</ta>
            <ta e="T326" id="Seg_7319" s="T325">pro:P</ta>
            <ta e="T328" id="Seg_7320" s="T327">pro.h:Poss</ta>
            <ta e="T330" id="Seg_7321" s="T329">np.h:Th</ta>
            <ta e="T332" id="Seg_7322" s="T331">pro.h:A</ta>
            <ta e="T333" id="Seg_7323" s="T332">pro.h:G</ta>
            <ta e="T336" id="Seg_7324" s="T335">pro.h:A</ta>
            <ta e="T337" id="Seg_7325" s="T336">pro.h:G</ta>
            <ta e="T339" id="Seg_7326" s="T338">pro.h:A</ta>
            <ta e="T341" id="Seg_7327" s="T340">np:P</ta>
            <ta e="T343" id="Seg_7328" s="T342">v:Th</ta>
            <ta e="T345" id="Seg_7329" s="T344">0.1.h:E</ta>
            <ta e="T348" id="Seg_7330" s="T347">adv:Time</ta>
            <ta e="T349" id="Seg_7331" s="T348">0.1.h:A</ta>
            <ta e="T350" id="Seg_7332" s="T349">pro.h:A</ta>
            <ta e="T351" id="Seg_7333" s="T350">adv:Time</ta>
            <ta e="T352" id="Seg_7334" s="T351">np:G</ta>
            <ta e="T354" id="Seg_7335" s="T353">0.1.h:A</ta>
            <ta e="T355" id="Seg_7336" s="T354">np:Th</ta>
            <ta e="T356" id="Seg_7337" s="T355">0.1.h:A</ta>
            <ta e="T357" id="Seg_7338" s="T356">np:Th</ta>
            <ta e="T360" id="Seg_7339" s="T359">pro.h:A</ta>
            <ta e="T364" id="Seg_7340" s="T363">pro.h:E</ta>
            <ta e="T365" id="Seg_7341" s="T364">0.1.h:A</ta>
            <ta e="T366" id="Seg_7342" s="T365">np:Th</ta>
            <ta e="T367" id="Seg_7343" s="T366">pro.h:R</ta>
            <ta e="T369" id="Seg_7344" s="T368">np:Th</ta>
            <ta e="T370" id="Seg_7345" s="T369">0.1.h:A</ta>
            <ta e="T371" id="Seg_7346" s="T370">pro.h:A</ta>
            <ta e="T373" id="Seg_7347" s="T372">adv:L</ta>
            <ta e="T375" id="Seg_7348" s="T374">np:Ins</ta>
            <ta e="T376" id="Seg_7349" s="T375">0.1.h:A</ta>
            <ta e="T379" id="Seg_7350" s="T378">0.1.h:E</ta>
            <ta e="T380" id="Seg_7351" s="T379">pro.h:Th</ta>
            <ta e="T381" id="Seg_7352" s="T380">np.h:A</ta>
            <ta e="T382" id="Seg_7353" s="T381">np:Path</ta>
            <ta e="T384" id="Seg_7354" s="T383">pro:Th</ta>
            <ta e="T385" id="Seg_7355" s="T384">0.3.h:A</ta>
            <ta e="T389" id="Seg_7356" s="T388">0.2.h:A</ta>
            <ta e="T390" id="Seg_7357" s="T389">0.2.h:P</ta>
            <ta e="T391" id="Seg_7358" s="T390">pro.h:A</ta>
            <ta e="T393" id="Seg_7359" s="T392">0.1.h:A</ta>
            <ta e="T394" id="Seg_7360" s="T393">v:Th</ta>
            <ta e="T397" id="Seg_7361" s="T396">np.h:A</ta>
            <ta e="T398" id="Seg_7362" s="T397">pro.h:R</ta>
            <ta e="T400" id="Seg_7363" s="T399">pro:L</ta>
            <ta e="T401" id="Seg_7364" s="T400">0.2.h:A</ta>
            <ta e="T403" id="Seg_7365" s="T402">np:Th</ta>
            <ta e="T406" id="Seg_7366" s="T405">np:Th</ta>
            <ta e="T408" id="Seg_7367" s="T407">np:Poss</ta>
            <ta e="T409" id="Seg_7368" s="T408">np:L</ta>
            <ta e="T411" id="Seg_7369" s="T410">np:L</ta>
            <ta e="T412" id="Seg_7370" s="T411">0.1.h:Th</ta>
            <ta e="T413" id="Seg_7371" s="T412">pro.h:A</ta>
            <ta e="T414" id="Seg_7372" s="T413">pro.h:R</ta>
            <ta e="T416" id="Seg_7373" s="T415">adv:L</ta>
            <ta e="T419" id="Seg_7374" s="T418">0.2.h:A</ta>
            <ta e="T421" id="Seg_7375" s="T420">pro.h:A</ta>
            <ta e="T425" id="Seg_7376" s="T424">pro.h:E</ta>
            <ta e="T429" id="Seg_7377" s="T428">0.1.h:A</ta>
            <ta e="T430" id="Seg_7378" s="T429">pro.h:A</ta>
            <ta e="T432" id="Seg_7379" s="T431">np.h:R</ta>
            <ta e="T434" id="Seg_7380" s="T433">pro:L</ta>
            <ta e="T436" id="Seg_7381" s="T435">np:Th</ta>
            <ta e="T437" id="Seg_7382" s="T436">pro.h:A</ta>
            <ta e="T438" id="Seg_7383" s="T437">pro.h:E</ta>
            <ta e="T439" id="Seg_7384" s="T438">0.3:Th</ta>
            <ta e="T441" id="Seg_7385" s="T440">pro.h:A</ta>
            <ta e="T444" id="Seg_7386" s="T443">0.1.h:A</ta>
            <ta e="T445" id="Seg_7387" s="T444">pro.h:E</ta>
            <ta e="T447" id="Seg_7388" s="T446">np:Th 0.1.h:Poss</ta>
            <ta e="T450" id="Seg_7389" s="T449">0.1.h:A</ta>
            <ta e="T453" id="Seg_7390" s="T452">0.3:Th</ta>
            <ta e="T454" id="Seg_7391" s="T453">np:G</ta>
            <ta e="T455" id="Seg_7392" s="T454">0.1.h:A</ta>
            <ta e="T458" id="Seg_7393" s="T457">adv:L</ta>
            <ta e="T459" id="Seg_7394" s="T458">0.2.h:P</ta>
            <ta e="T460" id="Seg_7395" s="T459">pro.h:A</ta>
            <ta e="T461" id="Seg_7396" s="T460">np:Th</ta>
            <ta e="T466" id="Seg_7397" s="T465">0.1.h:A</ta>
            <ta e="T467" id="Seg_7398" s="T466">np:Path</ta>
            <ta e="T469" id="Seg_7399" s="T468">0.1.h:P</ta>
            <ta e="T470" id="Seg_7400" s="T469">pro.h:A</ta>
            <ta e="T471" id="Seg_7401" s="T470">adv:Time</ta>
            <ta e="T472" id="Seg_7402" s="T471">np:Ins</ta>
            <ta e="T474" id="Seg_7403" s="T473">0.1.h:Th</ta>
            <ta e="T475" id="Seg_7404" s="T474">np:L</ta>
            <ta e="T476" id="Seg_7405" s="T475">np:L</ta>
            <ta e="T478" id="Seg_7406" s="T477">np:L</ta>
            <ta e="T479" id="Seg_7407" s="T478">0.1.h:Th</ta>
            <ta e="T480" id="Seg_7408" s="T479">np.h:A</ta>
            <ta e="T482" id="Seg_7409" s="T481">np:G</ta>
            <ta e="T483" id="Seg_7410" s="T482">np:Com 0.3.h:Poss</ta>
            <ta e="T484" id="Seg_7411" s="T483">pro.h:Th</ta>
            <ta e="T487" id="Seg_7412" s="T486">np.h:Poss</ta>
            <ta e="T488" id="Seg_7413" s="T487">np.h:P</ta>
            <ta e="T490" id="Seg_7414" s="T489">np:L</ta>
            <ta e="T491" id="Seg_7415" s="T490">0.3.h:Th</ta>
            <ta e="T492" id="Seg_7416" s="T491">np.h:L 0.3.h:Poss</ta>
            <ta e="T494" id="Seg_7417" s="T493">pro.h:A</ta>
            <ta e="T496" id="Seg_7418" s="T495">np:Th</ta>
            <ta e="T497" id="Seg_7419" s="T496">0.1.h:A</ta>
            <ta e="T501" id="Seg_7420" s="T500">pro.h:Poss</ta>
            <ta e="T502" id="Seg_7421" s="T501">np:A</ta>
            <ta e="T505" id="Seg_7422" s="T504">pro.h:R</ta>
            <ta e="T506" id="Seg_7423" s="T505">np:Th</ta>
            <ta e="T508" id="Seg_7424" s="T507">0.2.h:A</ta>
            <ta e="T509" id="Seg_7425" s="T508">pro.h:A</ta>
            <ta e="T512" id="Seg_7426" s="T511">0.2.h:A</ta>
            <ta e="T514" id="Seg_7427" s="T513">pro.h:Poss</ta>
            <ta e="T515" id="Seg_7428" s="T514">np.h:Th</ta>
            <ta e="T519" id="Seg_7429" s="T518">np:Th</ta>
            <ta e="T520" id="Seg_7430" s="T519">0.2.h:E</ta>
            <ta e="T523" id="Seg_7431" s="T522">pro:Th</ta>
            <ta e="T524" id="Seg_7432" s="T523">pro.h:R</ta>
            <ta e="T525" id="Seg_7433" s="T524">0.2.h:A</ta>
            <ta e="T526" id="Seg_7434" s="T525">pro.h:A</ta>
            <ta e="T529" id="Seg_7435" s="T528">pro.h:R</ta>
            <ta e="T530" id="Seg_7436" s="T529">np:Th</ta>
            <ta e="T531" id="Seg_7437" s="T530">0.3.h:A</ta>
            <ta e="T533" id="Seg_7438" s="T532">np:Th</ta>
            <ta e="T534" id="Seg_7439" s="T533">0.3.h:A</ta>
            <ta e="T536" id="Seg_7440" s="T535">np:L</ta>
            <ta e="T537" id="Seg_7441" s="T536">pro:Th</ta>
            <ta e="T539" id="Seg_7442" s="T538">pro:Th</ta>
            <ta e="T540" id="Seg_7443" s="T539">0.2.h:A</ta>
            <ta e="T541" id="Seg_7444" s="T540">np.h:Th 0.2.h:Poss</ta>
            <ta e="T546" id="Seg_7445" s="T545">0.3.h:A</ta>
            <ta e="T548" id="Seg_7446" s="T547">0.3.h:A</ta>
            <ta e="T549" id="Seg_7447" s="T548">pp:Th</ta>
            <ta e="T551" id="Seg_7448" s="T550">0.2.h:A</ta>
            <ta e="T553" id="Seg_7449" s="T552">0.3.h:Th</ta>
            <ta e="T555" id="Seg_7450" s="T554">pro.h:A</ta>
            <ta e="T556" id="Seg_7451" s="T555">pro.h:R</ta>
            <ta e="T557" id="Seg_7452" s="T556">np:Th</ta>
            <ta e="T559" id="Seg_7453" s="T558">pro.h:A</ta>
            <ta e="T560" id="Seg_7454" s="T559">pro.h:R</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_7455" s="T1">pro.h:S</ta>
            <ta e="T4" id="Seg_7456" s="T3">v:pred</ta>
            <ta e="T7" id="Seg_7457" s="T6">0.1.h:S v:pred</ta>
            <ta e="T10" id="Seg_7458" s="T9">0.1.h:S v:pred</ta>
            <ta e="T12" id="Seg_7459" s="T10">s:compl</ta>
            <ta e="T14" id="Seg_7460" s="T12">s:compl</ta>
            <ta e="T15" id="Seg_7461" s="T14">pro.h:S</ta>
            <ta e="T17" id="Seg_7462" s="T15">s:temp</ta>
            <ta e="T18" id="Seg_7463" s="T17">v:pred</ta>
            <ta e="T21" id="Seg_7464" s="T20">pro.h:S</ta>
            <ta e="T22" id="Seg_7465" s="T21">v:pred</ta>
            <ta e="T23" id="Seg_7466" s="T22">pro.h:S</ta>
            <ta e="T24" id="Seg_7467" s="T23">v:pred</ta>
            <ta e="T25" id="Seg_7468" s="T24">pro.h:S</ta>
            <ta e="T27" id="Seg_7469" s="T26">v:pred</ta>
            <ta e="T28" id="Seg_7470" s="T27">pro.h:S</ta>
            <ta e="T29" id="Seg_7471" s="T28">pro.h:O</ta>
            <ta e="T31" id="Seg_7472" s="T30">v:pred</ta>
            <ta e="T35" id="Seg_7473" s="T31">s:compl</ta>
            <ta e="T36" id="Seg_7474" s="T35">pro.h:S</ta>
            <ta e="T38" id="Seg_7475" s="T37">np:O</ta>
            <ta e="T39" id="Seg_7476" s="T38">v:pred</ta>
            <ta e="T40" id="Seg_7477" s="T39">pro.h:S</ta>
            <ta e="T41" id="Seg_7478" s="T40">v:pred</ta>
            <ta e="T44" id="Seg_7479" s="T43">np:O</ta>
            <ta e="T45" id="Seg_7480" s="T44">0.1.h:S v:pred</ta>
            <ta e="T47" id="Seg_7481" s="T46">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T51" id="Seg_7482" s="T50">pro.h:S</ta>
            <ta e="T54" id="Seg_7483" s="T53">np:O</ta>
            <ta e="T55" id="Seg_7484" s="T54">v:pred</ta>
            <ta e="T57" id="Seg_7485" s="T56">pro.h:S</ta>
            <ta e="T59" id="Seg_7486" s="T58">v:pred</ta>
            <ta e="T60" id="Seg_7487" s="T59">pro.h:S</ta>
            <ta e="T61" id="Seg_7488" s="T60">v:pred</ta>
            <ta e="T65" id="Seg_7489" s="T64">np:S</ta>
            <ta e="T66" id="Seg_7490" s="T65">adj:pred</ta>
            <ta e="T67" id="Seg_7491" s="T66">adj:pred</ta>
            <ta e="T71" id="Seg_7492" s="T70">0.1.h:S v:pred</ta>
            <ta e="T73" id="Seg_7493" s="T71">s:temp</ta>
            <ta e="T74" id="Seg_7494" s="T73">0.1.h:S v:pred</ta>
            <ta e="T77" id="Seg_7495" s="T76">np:S</ta>
            <ta e="T79" id="Seg_7496" s="T78">v:pred</ta>
            <ta e="T80" id="Seg_7497" s="T79">pro.h:S</ta>
            <ta e="T81" id="Seg_7498" s="T80">v:pred</ta>
            <ta e="T83" id="Seg_7499" s="T82">np.h:O</ta>
            <ta e="T86" id="Seg_7500" s="T85">0.1.h:S v:pred</ta>
            <ta e="T88" id="Seg_7501" s="T87">np:S</ta>
            <ta e="T89" id="Seg_7502" s="T88">v:pred</ta>
            <ta e="T90" id="Seg_7503" s="T89">pro.h:S</ta>
            <ta e="T93" id="Seg_7504" s="T92">v:pred</ta>
            <ta e="T94" id="Seg_7505" s="T93">np:S</ta>
            <ta e="T97" id="Seg_7506" s="T96">v:pred</ta>
            <ta e="T100" id="Seg_7507" s="T99">np:S</ta>
            <ta e="T101" id="Seg_7508" s="T100">np:S</ta>
            <ta e="T103" id="Seg_7509" s="T102">np:S</ta>
            <ta e="T104" id="Seg_7510" s="T103">v:pred</ta>
            <ta e="T105" id="Seg_7511" s="T104">pro.h:S</ta>
            <ta e="T108" id="Seg_7512" s="T107">v:pred</ta>
            <ta e="T110" id="Seg_7513" s="T109">0.3:S v:pred</ta>
            <ta e="T112" id="Seg_7514" s="T111">pro.h:S</ta>
            <ta e="T114" id="Seg_7515" s="T113">v:pred</ta>
            <ta e="T116" id="Seg_7516" s="T115">pro.h:S</ta>
            <ta e="T117" id="Seg_7517" s="T116">v:pred</ta>
            <ta e="T121" id="Seg_7518" s="T120">0.3.h:S v:pred</ta>
            <ta e="T124" id="Seg_7519" s="T121">s:purp</ta>
            <ta e="T125" id="Seg_7520" s="T124">0.3.h:S v:pred</ta>
            <ta e="T129" id="Seg_7521" s="T128">0.3.h:S v:pred</ta>
            <ta e="T134" id="Seg_7522" s="T131">s:temp</ta>
            <ta e="T135" id="Seg_7523" s="T134">0.3.h:S v:pred</ta>
            <ta e="T137" id="Seg_7524" s="T136">0.3.h:S v:pred</ta>
            <ta e="T140" id="Seg_7525" s="T139">0.3.h:S v:pred</ta>
            <ta e="T141" id="Seg_7526" s="T140">pro.h:S</ta>
            <ta e="T144" id="Seg_7527" s="T143">v:pred</ta>
            <ta e="T147" id="Seg_7528" s="T146">np:S</ta>
            <ta e="T149" id="Seg_7529" s="T148">n:pred</ta>
            <ta e="T150" id="Seg_7530" s="T149">pro.h:S</ta>
            <ta e="T152" id="Seg_7531" s="T151">v:pred</ta>
            <ta e="T153" id="Seg_7532" s="T152">pro.h:O</ta>
            <ta e="T154" id="Seg_7533" s="T153">0.3.h:S v:pred</ta>
            <ta e="T157" id="Seg_7534" s="T156">0.3.h:S v:pred</ta>
            <ta e="T158" id="Seg_7535" s="T157">pro.h:S</ta>
            <ta e="T160" id="Seg_7536" s="T159">s:temp</ta>
            <ta e="T161" id="Seg_7537" s="T160">v:pred</ta>
            <ta e="T163" id="Seg_7538" s="T161">s:compl</ta>
            <ta e="T166" id="Seg_7539" s="T165">np:S</ta>
            <ta e="T169" id="Seg_7540" s="T168">np:S</ta>
            <ta e="T170" id="Seg_7541" s="T169">adj:pred</ta>
            <ta e="T171" id="Seg_7542" s="T170">np:S</ta>
            <ta e="T172" id="Seg_7543" s="T171">adj:pred</ta>
            <ta e="T175" id="Seg_7544" s="T174">np:S</ta>
            <ta e="T177" id="Seg_7545" s="T176">v:pred</ta>
            <ta e="T183" id="Seg_7546" s="T182">np:S</ta>
            <ta e="T184" id="Seg_7547" s="T183">np:S</ta>
            <ta e="T186" id="Seg_7548" s="T185">v:pred</ta>
            <ta e="T189" id="Seg_7549" s="T188">0.1.h:S v:pred</ta>
            <ta e="T192" id="Seg_7550" s="T191">np:S</ta>
            <ta e="T195" id="Seg_7551" s="T194">np:S</ta>
            <ta e="T197" id="Seg_7552" s="T196">0.1.h:S v:pred</ta>
            <ta e="T199" id="Seg_7553" s="T198">0.1.h:S v:pred</ta>
            <ta e="T200" id="Seg_7554" s="T199">np:O</ta>
            <ta e="T202" id="Seg_7555" s="T201">0.3:S v:pred</ta>
            <ta e="T205" id="Seg_7556" s="T204">np:S</ta>
            <ta e="T206" id="Seg_7557" s="T205">v:pred</ta>
            <ta e="T207" id="Seg_7558" s="T206">np:O</ta>
            <ta e="T208" id="Seg_7559" s="T207">0.2.h:S v:pred</ta>
            <ta e="T209" id="Seg_7560" s="T208">np:S</ta>
            <ta e="T211" id="Seg_7561" s="T210">v:pred</ta>
            <ta e="T212" id="Seg_7562" s="T211">pro:O</ta>
            <ta e="T213" id="Seg_7563" s="T212">s:temp</ta>
            <ta e="T214" id="Seg_7564" s="T213">0.3:S v:pred</ta>
            <ta e="T215" id="Seg_7565" s="T214">np:S</ta>
            <ta e="T217" id="Seg_7566" s="T216">adj:pred</ta>
            <ta e="T225" id="Seg_7567" s="T224">np:S</ta>
            <ta e="T226" id="Seg_7568" s="T225">adj:pred</ta>
            <ta e="T229" id="Seg_7569" s="T228">np:S</ta>
            <ta e="T239" id="Seg_7570" s="T238">np:S</ta>
            <ta e="T240" id="Seg_7571" s="T239">v:pred</ta>
            <ta e="T241" id="Seg_7572" s="T240">pro.h:S</ta>
            <ta e="T243" id="Seg_7573" s="T242">np:O</ta>
            <ta e="T244" id="Seg_7574" s="T243">v:pred</ta>
            <ta e="T247" id="Seg_7575" s="T246">v:pred</ta>
            <ta e="T248" id="Seg_7576" s="T247">np:S</ta>
            <ta e="T251" id="Seg_7577" s="T250">0.3.h:S v:pred</ta>
            <ta e="T252" id="Seg_7578" s="T251">np.h:S</ta>
            <ta e="T254" id="Seg_7579" s="T253">v:pred</ta>
            <ta e="T257" id="Seg_7580" s="T256">np:S</ta>
            <ta e="T259" id="Seg_7581" s="T258">adj:pred</ta>
            <ta e="T260" id="Seg_7582" s="T259">cop</ta>
            <ta e="T264" id="Seg_7583" s="T260">s:temp</ta>
            <ta e="T265" id="Seg_7584" s="T264">0.1.h:S v:pred</ta>
            <ta e="T270" id="Seg_7585" s="T269">0.1.h:S v:pred</ta>
            <ta e="T273" id="Seg_7586" s="T272">0.1.h:S v:pred</ta>
            <ta e="T275" id="Seg_7587" s="T274">pro:O</ta>
            <ta e="T276" id="Seg_7588" s="T275">0.3.h:S v:pred</ta>
            <ta e="T280" id="Seg_7589" s="T279">0.1.h:S v:pred</ta>
            <ta e="T282" id="Seg_7590" s="T281">np.h:S</ta>
            <ta e="T284" id="Seg_7591" s="T283">v:pred</ta>
            <ta e="T288" id="Seg_7592" s="T287">0.2.h:S v:pred</ta>
            <ta e="T290" id="Seg_7593" s="T289">0.2.h:S v:pred</ta>
            <ta e="T292" id="Seg_7594" s="T291">0.1.h:S v:pred</ta>
            <ta e="T294" id="Seg_7595" s="T293">pro.h:S</ta>
            <ta e="T295" id="Seg_7596" s="T294">v:pred</ta>
            <ta e="T296" id="Seg_7597" s="T295">np:O</ta>
            <ta e="T299" id="Seg_7598" s="T298">0.3:S v.pred</ta>
            <ta e="T304" id="Seg_7599" s="T303">0.1.h:S v:pred</ta>
            <ta e="T306" id="Seg_7600" s="T305">np:S</ta>
            <ta e="T308" id="Seg_7601" s="T307">v:pred</ta>
            <ta e="T310" id="Seg_7602" s="T309">np.h:S</ta>
            <ta e="T311" id="Seg_7603" s="T310">v:pred</ta>
            <ta e="T313" id="Seg_7604" s="T312">np:O</ta>
            <ta e="T314" id="Seg_7605" s="T313">0.3.h:S v:pred</ta>
            <ta e="T316" id="Seg_7606" s="T315">np:O</ta>
            <ta e="T317" id="Seg_7607" s="T316">np.h:S</ta>
            <ta e="T320" id="Seg_7608" s="T319">v:pred 0.3:O</ta>
            <ta e="T321" id="Seg_7609" s="T320">np:O</ta>
            <ta e="T322" id="Seg_7610" s="T321">0.3.h:S v:pred</ta>
            <ta e="T323" id="Seg_7611" s="T322">np:S</ta>
            <ta e="T324" id="Seg_7612" s="T323">v:pred</ta>
            <ta e="T325" id="Seg_7613" s="T324">np:S</ta>
            <ta e="T326" id="Seg_7614" s="T325">pro:O</ta>
            <ta e="T327" id="Seg_7615" s="T326">v:pred</ta>
            <ta e="T330" id="Seg_7616" s="T329">np.h:S</ta>
            <ta e="T331" id="Seg_7617" s="T330">v:pred</ta>
            <ta e="T332" id="Seg_7618" s="T331">pro.h:S</ta>
            <ta e="T334" id="Seg_7619" s="T333">v:pred</ta>
            <ta e="T336" id="Seg_7620" s="T335">pro.h:S</ta>
            <ta e="T338" id="Seg_7621" s="T337">v:pred</ta>
            <ta e="T339" id="Seg_7622" s="T338">pro.h:S</ta>
            <ta e="T340" id="Seg_7623" s="T339">v:pred</ta>
            <ta e="T341" id="Seg_7624" s="T340">np:O</ta>
            <ta e="T343" id="Seg_7625" s="T342">v:O</ta>
            <ta e="T345" id="Seg_7626" s="T344">0.1.h:S v:pred</ta>
            <ta e="T349" id="Seg_7627" s="T348">0.1.h:S v:pred</ta>
            <ta e="T350" id="Seg_7628" s="T349">pro.h:S</ta>
            <ta e="T353" id="Seg_7629" s="T352">v:pred</ta>
            <ta e="T354" id="Seg_7630" s="T353">s:purp</ta>
            <ta e="T356" id="Seg_7631" s="T354">s:purp</ta>
            <ta e="T357" id="Seg_7632" s="T356">np:S</ta>
            <ta e="T358" id="Seg_7633" s="T357">adj:pred</ta>
            <ta e="T359" id="Seg_7634" s="T358">cop</ta>
            <ta e="T360" id="Seg_7635" s="T359">pro.h:S</ta>
            <ta e="T362" id="Seg_7636" s="T361">v:pred</ta>
            <ta e="T365" id="Seg_7637" s="T364">0.1.h:S v:pred</ta>
            <ta e="T366" id="Seg_7638" s="T365">np:O</ta>
            <ta e="T369" id="Seg_7639" s="T368">np:O</ta>
            <ta e="T370" id="Seg_7640" s="T369">0.1.h:S v:pred</ta>
            <ta e="T371" id="Seg_7641" s="T370">pro.h:S</ta>
            <ta e="T372" id="Seg_7642" s="T371">v:pred</ta>
            <ta e="T376" id="Seg_7643" s="T375">0.1.h:S v:pred</ta>
            <ta e="T379" id="Seg_7644" s="T378">0.1.h:S v:pred</ta>
            <ta e="T380" id="Seg_7645" s="T379">pro.h:O</ta>
            <ta e="T381" id="Seg_7646" s="T380">np.h:S</ta>
            <ta e="T383" id="Seg_7647" s="T382">v:pred</ta>
            <ta e="T384" id="Seg_7648" s="T383">pro:O</ta>
            <ta e="T385" id="Seg_7649" s="T384">0.3.h:S v:pred</ta>
            <ta e="T389" id="Seg_7650" s="T388">0.2.h:S v:pred</ta>
            <ta e="T390" id="Seg_7651" s="T389">0.2.h:S v:pred</ta>
            <ta e="T391" id="Seg_7652" s="T390">pro.h:S</ta>
            <ta e="T392" id="Seg_7653" s="T391">v:pred</ta>
            <ta e="T393" id="Seg_7654" s="T392">0.1.h:S v:pred</ta>
            <ta e="T394" id="Seg_7655" s="T393">v:O</ta>
            <ta e="T395" id="Seg_7656" s="T394">ptcl:pred</ta>
            <ta e="T397" id="Seg_7657" s="T396">np.h:S</ta>
            <ta e="T399" id="Seg_7658" s="T398">v:pred</ta>
            <ta e="T401" id="Seg_7659" s="T400">0.2.h:S v:pred</ta>
            <ta e="T403" id="Seg_7660" s="T402">np:S</ta>
            <ta e="T404" id="Seg_7661" s="T403">v:pred</ta>
            <ta e="T406" id="Seg_7662" s="T405">np:S</ta>
            <ta e="T410" id="Seg_7663" s="T409">v:pred</ta>
            <ta e="T412" id="Seg_7664" s="T411">0.1.h:S v:pred</ta>
            <ta e="T413" id="Seg_7665" s="T412">pro.h:S</ta>
            <ta e="T415" id="Seg_7666" s="T414">v:pred</ta>
            <ta e="T419" id="Seg_7667" s="T418">0.2.h:S v:pred</ta>
            <ta e="T421" id="Seg_7668" s="T420">pro.h:S</ta>
            <ta e="T423" id="Seg_7669" s="T422">v:pred</ta>
            <ta e="T425" id="Seg_7670" s="T424">pro.h:S</ta>
            <ta e="T426" id="Seg_7671" s="T425">v:pred</ta>
            <ta e="T429" id="Seg_7672" s="T426">s:temp</ta>
            <ta e="T430" id="Seg_7673" s="T429">pro.h:S</ta>
            <ta e="T433" id="Seg_7674" s="T432">v:pred</ta>
            <ta e="T436" id="Seg_7675" s="T435">np:S</ta>
            <ta e="T437" id="Seg_7676" s="T436">pro.h:S</ta>
            <ta e="T439" id="Seg_7677" s="T438">v:pred 0.3:O</ta>
            <ta e="T441" id="Seg_7678" s="T440">pro.h:S</ta>
            <ta e="T442" id="Seg_7679" s="T441">v:pred</ta>
            <ta e="T444" id="Seg_7680" s="T443">0.1.h:S v:pred</ta>
            <ta e="T445" id="Seg_7681" s="T444">pro.h:S</ta>
            <ta e="T446" id="Seg_7682" s="T445">v:pred</ta>
            <ta e="T447" id="Seg_7683" s="T446">np:S</ta>
            <ta e="T449" id="Seg_7684" s="T448">v:pred</ta>
            <ta e="T450" id="Seg_7685" s="T449">0.1.h:S v:pred</ta>
            <ta e="T453" id="Seg_7686" s="T452">0.3:S v:pred</ta>
            <ta e="T455" id="Seg_7687" s="T454">0.1.h:S v:pred</ta>
            <ta e="T459" id="Seg_7688" s="T458">0.2.h:S v:pred</ta>
            <ta e="T460" id="Seg_7689" s="T459">pro.h:S</ta>
            <ta e="T461" id="Seg_7690" s="T460">np:O</ta>
            <ta e="T463" id="Seg_7691" s="T462">v:pred</ta>
            <ta e="T466" id="Seg_7692" s="T465">0.1.h:S v:pred</ta>
            <ta e="T469" id="Seg_7693" s="T468">0.1.h:S v:pred</ta>
            <ta e="T470" id="Seg_7694" s="T469">pro.h:S</ta>
            <ta e="T473" id="Seg_7695" s="T472">v:pred</ta>
            <ta e="T474" id="Seg_7696" s="T473">0.1.h:S v:pred</ta>
            <ta e="T479" id="Seg_7697" s="T478">0.1.h:S v:pred</ta>
            <ta e="T480" id="Seg_7698" s="T479">np.h:S</ta>
            <ta e="T481" id="Seg_7699" s="T480">v:pred</ta>
            <ta e="T484" id="Seg_7700" s="T483">pro.h:S</ta>
            <ta e="T485" id="Seg_7701" s="T484">v:pred</ta>
            <ta e="T488" id="Seg_7702" s="T487">np.h:S</ta>
            <ta e="T489" id="Seg_7703" s="T488">v:pred</ta>
            <ta e="T491" id="Seg_7704" s="T490">0.3.h:S v:pred</ta>
            <ta e="T494" id="Seg_7705" s="T493">pro.h:S</ta>
            <ta e="T495" id="Seg_7706" s="T494">v:pred</ta>
            <ta e="T497" id="Seg_7707" s="T495">s:purp</ta>
            <ta e="T503" id="Seg_7708" s="T499">s:temp</ta>
            <ta e="T506" id="Seg_7709" s="T505">np:O</ta>
            <ta e="T508" id="Seg_7710" s="T507">0.2.h:S v:pred</ta>
            <ta e="T509" id="Seg_7711" s="T508">pro.h:S</ta>
            <ta e="T510" id="Seg_7712" s="T509">v:pred</ta>
            <ta e="T512" id="Seg_7713" s="T511">0.2.h:S v:pred</ta>
            <ta e="T516" id="Seg_7714" s="T512">s:compl</ta>
            <ta e="T519" id="Seg_7715" s="T518">np:O</ta>
            <ta e="T520" id="Seg_7716" s="T519">0.2.h:S v:pred</ta>
            <ta e="T523" id="Seg_7717" s="T522">pro:O</ta>
            <ta e="T525" id="Seg_7718" s="T524">0.2.h:S v:pred</ta>
            <ta e="T526" id="Seg_7719" s="T525">pro.h:S</ta>
            <ta e="T528" id="Seg_7720" s="T527">v:pred</ta>
            <ta e="T531" id="Seg_7721" s="T530">0.3.h:S v:pred</ta>
            <ta e="T534" id="Seg_7722" s="T533">0.3.h:S v:pred</ta>
            <ta e="T538" id="Seg_7723" s="T535">s:rel</ta>
            <ta e="T539" id="Seg_7724" s="T538">pro:O</ta>
            <ta e="T540" id="Seg_7725" s="T539">0.2.h:S v:pred</ta>
            <ta e="T541" id="Seg_7726" s="T540">np.h:S</ta>
            <ta e="T543" id="Seg_7727" s="T542">v:pred</ta>
            <ta e="T546" id="Seg_7728" s="T545">0.3.h:S v:pred</ta>
            <ta e="T548" id="Seg_7729" s="T547">0.3.h:S v:pred</ta>
            <ta e="T551" id="Seg_7730" s="T550">0.2.h:S v:pred</ta>
            <ta e="T554" id="Seg_7731" s="T551">s:compl</ta>
            <ta e="T555" id="Seg_7732" s="T554">pro.h:S</ta>
            <ta e="T557" id="Seg_7733" s="T556">np:O</ta>
            <ta e="T558" id="Seg_7734" s="T557">v:pred</ta>
            <ta e="T559" id="Seg_7735" s="T558">pro.h:S</ta>
            <ta e="T562" id="Seg_7736" s="T561">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T38" id="Seg_7737" s="T37">RUS:core</ta>
            <ta e="T42" id="Seg_7738" s="T41">RUS:cult</ta>
            <ta e="T46" id="Seg_7739" s="T45">RUS:gram</ta>
            <ta e="T62" id="Seg_7740" s="T61">RUS:core</ta>
            <ta e="T70" id="Seg_7741" s="T69">RUS:cult</ta>
            <ta e="T72" id="Seg_7742" s="T71">RUS:cult</ta>
            <ta e="T74" id="Seg_7743" s="T73">RUS:cult</ta>
            <ta e="T76" id="Seg_7744" s="T75">RUS:cult</ta>
            <ta e="T77" id="Seg_7745" s="T76">RUS:cult</ta>
            <ta e="T82" id="Seg_7746" s="T81">RUS:gram</ta>
            <ta e="T87" id="Seg_7747" s="T86">RUS:disc</ta>
            <ta e="T88" id="Seg_7748" s="T87">RUS:cult</ta>
            <ta e="T92" id="Seg_7749" s="T91">RUS:cult</ta>
            <ta e="T99" id="Seg_7750" s="T98">RUS:core</ta>
            <ta e="T102" id="Seg_7751" s="T101">RUS:core</ta>
            <ta e="T106" id="Seg_7752" s="T105">RUS:cult</ta>
            <ta e="T115" id="Seg_7753" s="T114">RUS:cult</ta>
            <ta e="T130" id="Seg_7754" s="T129">RUS:gram</ta>
            <ta e="T180" id="Seg_7755" s="T179">RUS:gram</ta>
            <ta e="T184" id="Seg_7756" s="T183">RUS:core</ta>
            <ta e="T193" id="Seg_7757" s="T192">RUS:gram</ta>
            <ta e="T198" id="Seg_7758" s="T197">RUS:gram</ta>
            <ta e="T200" id="Seg_7759" s="T199">RUS:core</ta>
            <ta e="T203" id="Seg_7760" s="T202">RUS:gram</ta>
            <ta e="T207" id="Seg_7761" s="T206">RUS:cult</ta>
            <ta e="T212" id="Seg_7762" s="T211">RUS:core</ta>
            <ta e="T219" id="Seg_7763" s="T218">RUS:cult</ta>
            <ta e="T221" id="Seg_7764" s="T220">RUS:core</ta>
            <ta e="T232" id="Seg_7765" s="T231">RUS:core</ta>
            <ta e="T233" id="Seg_7766" s="T232">RUS:core</ta>
            <ta e="T239" id="Seg_7767" s="T238">RUS:core</ta>
            <ta e="T243" id="Seg_7768" s="T242">RUS:core</ta>
            <ta e="T248" id="Seg_7769" s="T247">RUS:core</ta>
            <ta e="T253" id="Seg_7770" s="T252">RUS:cult</ta>
            <ta e="T258" id="Seg_7771" s="T257">RUS:core</ta>
            <ta e="T259" id="Seg_7772" s="T258">RUS:core</ta>
            <ta e="T267" id="Seg_7773" s="T266">RUS:cult</ta>
            <ta e="T269" id="Seg_7774" s="T268">RUS:cult</ta>
            <ta e="T272" id="Seg_7775" s="T271">RUS:cult</ta>
            <ta e="T275" id="Seg_7776" s="T274">RUS:core</ta>
            <ta e="T277" id="Seg_7777" s="T276">RUS:cult</ta>
            <ta e="T278" id="Seg_7778" s="T277">RUS:cult</ta>
            <ta e="T279" id="Seg_7779" s="T278">RUS:cult</ta>
            <ta e="T286" id="Seg_7780" s="T285">RUS:cult</ta>
            <ta e="T293" id="Seg_7781" s="T292">RUS:cult</ta>
            <ta e="T296" id="Seg_7782" s="T295">RUS:cult</ta>
            <ta e="T300" id="Seg_7783" s="T299">RUS:cult</ta>
            <ta e="T303" id="Seg_7784" s="T302">RUS:gram</ta>
            <ta e="T306" id="Seg_7785" s="T305">RUS:core</ta>
            <ta e="T315" id="Seg_7786" s="T314">RUS:gram</ta>
            <ta e="T318" id="Seg_7787" s="T317">RUS:cult</ta>
            <ta e="T325" id="Seg_7788" s="T324">RUS:cult</ta>
            <ta e="T330" id="Seg_7789" s="T329">RUS:core</ta>
            <ta e="T335" id="Seg_7790" s="T334">RUS:gram</ta>
            <ta e="T346" id="Seg_7791" s="T345">RUS:gram</ta>
            <ta e="T363" id="Seg_7792" s="T362">RUS:gram</ta>
            <ta e="T374" id="Seg_7793" s="T373">RUS:core</ta>
            <ta e="T375" id="Seg_7794" s="T374">RUS:cult</ta>
            <ta e="T381" id="Seg_7795" s="T380">RUS:cult</ta>
            <ta e="T384" id="Seg_7796" s="T383">RUS:core</ta>
            <ta e="T387" id="Seg_7797" s="T386">RUS:disc</ta>
            <ta e="T395" id="Seg_7798" s="T394">RUS:mod</ta>
            <ta e="T396" id="Seg_7799" s="T395">RUS:gram</ta>
            <ta e="T397" id="Seg_7800" s="T396">RUS:cult</ta>
            <ta e="T407" id="Seg_7801" s="T406">RUS:core</ta>
            <ta e="T411" id="Seg_7802" s="T410">RUS:cult</ta>
            <ta e="T420" id="Seg_7803" s="T419">RUS:gram</ta>
            <ta e="T424" id="Seg_7804" s="T423">RUS:core</ta>
            <ta e="T426" id="Seg_7805" s="T425">RUS:core</ta>
            <ta e="T428" id="Seg_7806" s="T427">RUS:cult</ta>
            <ta e="T443" id="Seg_7807" s="T442">RUS:gram</ta>
            <ta e="T451" id="Seg_7808" s="T450">RUS:gram</ta>
            <ta e="T454" id="Seg_7809" s="T453">RUS:cult</ta>
            <ta e="T457" id="Seg_7810" s="T456">RUS:cult</ta>
            <ta e="T468" id="Seg_7811" s="T467">RUS:gram</ta>
            <ta e="T471" id="Seg_7812" s="T470">RUS:core</ta>
            <ta e="T472" id="Seg_7813" s="T471">RUS:cult</ta>
            <ta e="T477" id="Seg_7814" s="T476">RUS:gram</ta>
            <ta e="T486" id="Seg_7815" s="T485">RUS:cult</ta>
            <ta e="T493" id="Seg_7816" s="T492">RUS:gram</ta>
            <ta e="T504" id="Seg_7817" s="T503">RUS:gram</ta>
            <ta e="T507" id="Seg_7818" s="T506">RUS:core</ta>
            <ta e="T511" id="Seg_7819" s="T510">RUS:core</ta>
            <ta e="T521" id="Seg_7820" s="T520">RUS:gram</ta>
            <ta e="T523" id="Seg_7821" s="T522">RUS:core</ta>
            <ta e="T527" id="Seg_7822" s="T526">RUS:core</ta>
            <ta e="T532" id="Seg_7823" s="T531">RUS:gram</ta>
            <ta e="T533" id="Seg_7824" s="T532">RUS:cult</ta>
            <ta e="T535" id="Seg_7825" s="T534">RUS:gram</ta>
            <ta e="T536" id="Seg_7826" s="T535">RUS:cult</ta>
            <ta e="T539" id="Seg_7827" s="T538">RUS:core</ta>
            <ta e="T544" id="Seg_7828" s="T543">RUS:gram</ta>
            <ta e="T563" id="Seg_7829" s="T562">RUS:cult</ta>
            <ta e="T565" id="Seg_7830" s="T564">RUS:cult</ta>
            <ta e="T566" id="Seg_7831" s="T565">RUS:cult</ta>
            <ta e="T567" id="Seg_7832" s="T566">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T14" id="Seg_7833" s="T1">We were sitting today with Angelina Ivanovna, talking, talking about you, how you were living, what you were doing.</ta>
            <ta e="T20" id="Seg_7834" s="T14">I told her about you.</ta>
            <ta e="T24" id="Seg_7835" s="T20">I said: “She grew old.”</ta>
            <ta e="T35" id="Seg_7836" s="T24">I told her, that you had fed Maria and me, how we had been drinking with you.</ta>
            <ta e="T39" id="Seg_7837" s="T35">I told her everything.</ta>
            <ta e="T45" id="Seg_7838" s="T39">I said: “I'll write a letter to aunt Marina.”</ta>
            <ta e="T47" id="Seg_7839" s="T45">She'll read.</ta>
            <ta e="T50" id="Seg_7840" s="T47">Hallo, Marina Pavlovna.</ta>
            <ta e="T55" id="Seg_7841" s="T50">I'll write you a letter in Selkup.</ta>
            <ta e="T59" id="Seg_7842" s="T55">I can't do it in Russian.</ta>
            <ta e="T64" id="Seg_7843" s="T59">Now I live in a city, in Novosibirsk.</ta>
            <ta e="T67" id="Seg_7844" s="T64">The city is big and beautiful.</ta>
            <ta e="T71" id="Seg_7845" s="T67">I travelled from the city (Tomsk) to Novosibirsk by train.</ta>
            <ta e="T74" id="Seg_7846" s="T71">When we were travelling by train, we were playing cards.</ta>
            <ta e="T79" id="Seg_7847" s="T74">On the “Taiga” station the train stopped for a long time.</ta>
            <ta e="T86" id="Seg_7848" s="T79">I was looking but I didn't see Nikin(/Nikon) anywhere.</ta>
            <ta e="T89" id="Seg_7849" s="T86">Then the train set off.</ta>
            <ta e="T93" id="Seg_7850" s="T89">I was looking through the window all the time.</ta>
            <ta e="T97" id="Seg_7851" s="T93">Many villages stayed [behind].</ta>
            <ta e="T101" id="Seg_7852" s="T97">On the edge of the city there were woods, taiga.</ta>
            <ta e="T104" id="Seg_7853" s="T101">The snow lay everywhere.</ta>
            <ta e="T108" id="Seg_7854" s="T104">I wasn't afraid [travelling] by train.</ta>
            <ta e="T110" id="Seg_7855" s="T108">It swang a little.</ta>
            <ta e="T115" id="Seg_7856" s="T110">Earlier I didn't travel by train.</ta>
            <ta e="T125" id="Seg_7857" s="T115">I live with that woman, who had come to us to write the Selkup language.</ta>
            <ta e="T140" id="Seg_7858" s="T125">The woman from the city (Tomsk), who had come to Starosondorovo, she wrote words in Selkup, she used to come to you and to write with you.</ta>
            <ta e="T145" id="Seg_7859" s="T140">I live with this woman, in Novosibirsk.</ta>
            <ta e="T149" id="Seg_7860" s="T145">Her name is Angelina Ivanovna.</ta>
            <ta e="T152" id="Seg_7861" s="T149">I live in her house.</ta>
            <ta e="T157" id="Seg_7862" s="T152">She treats me good, she doesn't scold.</ta>
            <ta e="T163" id="Seg_7863" s="T157">I do myself what I want.</ta>
            <ta e="T167" id="Seg_7864" s="T163">She has three rooms.</ta>
            <ta e="T170" id="Seg_7865" s="T167">The rooms are big.</ta>
            <ta e="T174" id="Seg_7866" s="T170">It's warm in the house, there is central heating.</ta>
            <ta e="T182" id="Seg_7867" s="T174">There is water in the house: hot water and cold water.</ta>
            <ta e="T186" id="Seg_7868" s="T182">There is water and everything in the house.</ta>
            <ta e="T189" id="Seg_7869" s="T186">We wash ourselves in a separate room.</ta>
            <ta e="T195" id="Seg_7870" s="T189">There is hot and cold water there.</ta>
            <ta e="T199" id="Seg_7871" s="T195">We piss and defecate at home.</ta>
            <ta e="T206" id="Seg_7872" s="T199">Everything is washed out, and there is no smell.</ta>
            <ta e="T211" id="Seg_7873" s="T206">You pull by the chain and water flows.</ta>
            <ta e="T214" id="Seg_7874" s="T211">Everything is washed out completley.</ta>
            <ta e="T224" id="Seg_7875" s="T214">Houses are big here, four-storied, they are all alike.</ta>
            <ta e="T226" id="Seg_7876" s="T224">Houses are long.</ta>
            <ta e="T235" id="Seg_7877" s="T226">There are trees nearby: birches, poplars, firtrees, pine trees, bird cherry trees, aspens.</ta>
            <ta e="T240" id="Seg_7878" s="T235">There are many mushrooms here in summer (=many mushrooms are born here in summer).</ta>
            <ta e="T244" id="Seg_7879" s="T240">In summer we'll pick mushrooms.</ta>
            <ta e="T248" id="Seg_7880" s="T244">In spring there are many flowers.</ta>
            <ta e="T251" id="Seg_7881" s="T248">In winter people go skiing.</ta>
            <ta e="T254" id="Seg_7882" s="T251">Children ride sleds.</ta>
            <ta e="T260" id="Seg_7883" s="T254">In autumn all leaves will become yellow.</ta>
            <ta e="T267" id="Seg_7884" s="T260">When we had arrived to Novosibirsk, we spent a night at the Novosibirsk railway station.</ta>
            <ta e="T270" id="Seg_7885" s="T267">We spent the night on the top floor.</ta>
            <ta e="T273" id="Seg_7886" s="T270">We slept on the fifth floor.</ta>
            <ta e="T279" id="Seg_7887" s="T273">We were given everything: mattresses, blankets, pillows.</ta>
            <ta e="T281" id="Seg_7888" s="T279">We slept well.</ta>
            <ta e="T284" id="Seg_7889" s="T281">There were many people.</ta>
            <ta e="T286" id="Seg_7890" s="T284">A big railway station.</ta>
            <ta e="T290" id="Seg_7891" s="T286">One can get lost there and don't be able to find the exit.</ta>
            <ta e="T293" id="Seg_7892" s="T290">In the morning we went to a canteen.</ta>
            <ta e="T296" id="Seg_7893" s="T293">They use to say “restaurant”.</ta>
            <ta e="T301" id="Seg_7894" s="T296">It's good there, as in paradise.</ta>
            <ta e="T304" id="Seg_7895" s="T301">We ate there.</ta>
            <ta e="T308" id="Seg_7896" s="T304">There is sea nearby.</ta>
            <ta e="T312" id="Seg_7897" s="T308">People fish there using fishing rods. </ta>
            <ta e="T316" id="Seg_7898" s="T312">They catch ruffes and roaches(?) .</ta>
            <ta e="T322" id="Seg_7899" s="T316">A girl, the daughter of the hostess used to bring [fish], she brought a ruff.</ta>
            <ta e="T324" id="Seg_7900" s="T322">The ruff was frozen.</ta>
            <ta e="T327" id="Seg_7901" s="T324">The cat ate it.</ta>
            <ta e="T331" id="Seg_7902" s="T327">I have two friends.</ta>
            <ta e="T334" id="Seg_7903" s="T331">I go to visit them.</ta>
            <ta e="T338" id="Seg_7904" s="T334">And they come to me.</ta>
            <ta e="T341" id="Seg_7905" s="T338">I knit fishing nets.</ta>
            <ta e="T345" id="Seg_7906" s="T341">I don't want to sit doing nothing.</ta>
            <ta e="T349" id="Seg_7907" s="T345">And I am singing songs all the time.</ta>
            <ta e="T356" id="Seg_7908" s="T349">In spring I'll go to Starosondorovo to fish and to pick berries.</ta>
            <ta e="T359" id="Seg_7909" s="T356">The city is big.</ta>
            <ta e="T366" id="Seg_7910" s="T359">I'll come and show you the city.</ta>
            <ta e="T370" id="Seg_7911" s="T366">I'll bring this city to you.</ta>
            <ta e="T372" id="Seg_7912" s="T370">You'll look.</ta>
            <ta e="T376" id="Seg_7913" s="T372">I used to go by different cars here.</ta>
            <ta e="T379" id="Seg_7914" s="T376">First time I was afraid.</ta>
            <ta e="T383" id="Seg_7915" s="T379">Volodya drove me around.</ta>
            <ta e="T385" id="Seg_7916" s="T383">He showed me everything.</ta>
            <ta e="T390" id="Seg_7917" s="T385">If you go somewhere by yourself, you'll get lost.</ta>
            <ta e="T395" id="Seg_7918" s="T390">I wanted to piss and said: “[I] need to piss.”</ta>
            <ta e="T401" id="Seg_7919" s="T395">And Volodya said to me: “Where are you going to piss?</ta>
            <ta e="T404" id="Seg_7920" s="T401">There is no WC here.</ta>
            <ta e="T410" id="Seg_7921" s="T404">All toilets are in the houses.”</ta>
            <ta e="T412" id="Seg_7922" s="T410">We were at the market place.</ta>
            <ta e="T419" id="Seg_7923" s="T412">He said to me: “Why couldn't you piss there?”</ta>
            <ta e="T423" id="Seg_7924" s="T419">And I didn't piss.</ta>
            <ta e="T426" id="Seg_7925" s="T423">So I suffered.</ta>
            <ta e="T436" id="Seg_7926" s="T426">When we were waiting for a car, I asked one woman: “Where is a toilet?”</ta>
            <ta e="T440" id="Seg_7927" s="T436">She showed me: “There.”</ta>
            <ta e="T444" id="Seg_7928" s="T440">I ran [there] and pissed.</ta>
            <ta e="T449" id="Seg_7929" s="T444">I thought, the urine would flow out.</ta>
            <ta e="T453" id="Seg_7930" s="T449">I pissed and felt happy.</ta>
            <ta e="T457" id="Seg_7931" s="T453">We went shopping, [to] the big shops.</ta>
            <ta e="T459" id="Seg_7932" s="T457">One can get lost there.</ta>
            <ta e="T463" id="Seg_7933" s="T459">I cannot write.</ta>
            <ta e="T469" id="Seg_7934" s="T463">I don't go round the city myself, otherwise I'll get lost.</ta>
            <ta e="T473" id="Seg_7935" s="T469">I go by cars.</ta>
            <ta e="T479" id="Seg_7936" s="T473">I lived in the city (Tomsk), in Chornaja Rechka and in Takhtomyshevo.</ta>
            <ta e="T482" id="Seg_7937" s="T479">Dunja moved to Chornaja Rechka.</ta>
            <ta e="T485" id="Seg_7938" s="T482">She lives together with her son.</ta>
            <ta e="T489" id="Seg_7939" s="T485">Nikolaj Lazarych's wife died.</ta>
            <ta e="T492" id="Seg_7940" s="T489">She lived in Chornaja Rechka with her daughter.</ta>
            <ta e="T497" id="Seg_7941" s="T492">And I went to visit cities.</ta>
            <ta e="T508" id="Seg_7942" s="T497">Marina Pavlovna, when my letter comes, write me an answer straight away.</ta>
            <ta e="T510" id="Seg_7943" s="T508">I'll be waiting.</ta>
            <ta e="T516" id="Seg_7944" s="T510">Write everything, how my people are doing.</ta>
            <ta e="T522" id="Seg_7945" s="T516">Have you heard something about Kolya or not?</ta>
            <ta e="T525" id="Seg_7946" s="T522">Write me everything.</ta>
            <ta e="T528" id="Seg_7947" s="T525">I'll be waiting very much.</ta>
            <ta e="T534" id="Seg_7948" s="T528">Do they sell bread or flour?</ta>
            <ta e="T540" id="Seg_7949" s="T534">And write me about everything, that there is in the shop.</ta>
            <ta e="T545" id="Seg_7950" s="T540">Does your son-in-law lives [with you] or not?</ta>
            <ta e="T548" id="Seg_7951" s="T545">He was said to have left.</ta>
            <ta e="T554" id="Seg_7952" s="T548">Write about Zinka, how she is doing.</ta>
            <ta e="T558" id="Seg_7953" s="T554">I wrote her a letter.</ta>
            <ta e="T562" id="Seg_7954" s="T558">She didn't answer to me.</ta>
            <ta e="T567" id="Seg_7955" s="T562">Aunt Marina, happy First of May!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T14" id="Seg_7956" s="T1">Wir saßen heute beisammen mit Angelina Ivanovna, wir haben geredet, redeten über dich, wie du lebst, was du machst.</ta>
            <ta e="T20" id="Seg_7957" s="T14">Ich habe ihr von dir erzählt.</ta>
            <ta e="T24" id="Seg_7958" s="T20">Ich habe gesagt: "Sie ist alt geworden."</ta>
            <ta e="T35" id="Seg_7959" s="T24">Ich habe ihr erzählt, dass du Maria und mich bewirtet hast, wie wir mit dir getrunken haben.</ta>
            <ta e="T39" id="Seg_7960" s="T35">Ich habe ihr alles erzählt.</ta>
            <ta e="T45" id="Seg_7961" s="T39">Ich sagte: "Ich werde Tante Marina einen Brief schreiben."</ta>
            <ta e="T47" id="Seg_7962" s="T45">Sie wird ihn lesen.</ta>
            <ta e="T50" id="Seg_7963" s="T47">Hallo Marina Pavlovna.</ta>
            <ta e="T55" id="Seg_7964" s="T50">Ich schreibe dir einen Brief auf Selkupisch.</ta>
            <ta e="T59" id="Seg_7965" s="T55">Auf Russisch kann ich es nicht.</ta>
            <ta e="T64" id="Seg_7966" s="T59">Ich lebe jetzt in einer Stadt, in Novosibirsk.</ta>
            <ta e="T67" id="Seg_7967" s="T64">Die Stadt ist groß und schön.</ta>
            <ta e="T71" id="Seg_7968" s="T67">Aus der Stadt (Tomsk) nach Novosibirsk fuhr ich mit dem Zug.</ta>
            <ta e="T74" id="Seg_7969" s="T71">Während der Zugfahrt spielten wir Karten.</ta>
            <ta e="T79" id="Seg_7970" s="T74">An der "Taiga"-Station hielt der Zug lange.</ta>
            <ta e="T86" id="Seg_7971" s="T79">Ich schaute, aber ich konnte Nikin(/Nikon) nirgends sehen.</ta>
            <ta e="T89" id="Seg_7972" s="T86">Dann fuhr der Zug los.</ta>
            <ta e="T93" id="Seg_7973" s="T89">Ich schaute die ganze Zeit aus dem Fenster.</ta>
            <ta e="T97" id="Seg_7974" s="T93">Viele Dörfer blieben [zurück].</ta>
            <ta e="T101" id="Seg_7975" s="T97">Am Stadtrand gab es Wälder, Taiga.</ta>
            <ta e="T104" id="Seg_7976" s="T101">Überall lag Schnee.</ta>
            <ta e="T108" id="Seg_7977" s="T104">Ich hatte keine Angst Zug [zu fahren].</ta>
            <ta e="T110" id="Seg_7978" s="T108">Es schwankte ein wenig.</ta>
            <ta e="T115" id="Seg_7979" s="T110">Früher bin ich nicht Zug gefahren.</ta>
            <ta e="T125" id="Seg_7980" s="T115">Ich wohne mit dieser Frau zusammen, die zu uns gekommen ist, um die selkupische Sprache aufzuschreiben.</ta>
            <ta e="T140" id="Seg_7981" s="T125">Die Frau aus der Stadt (Tomsk), die nach Starosondorovo gekommen ist, sie hat selkupische Wörter aufgeschrieben, sie ist immer zu dir gekommen und hat mit dir geschrieben.</ta>
            <ta e="T145" id="Seg_7982" s="T140">Ich wohne mit dieser Frau zusammen, in Novosibirsk.</ta>
            <ta e="T149" id="Seg_7983" s="T145">Ihr Name ist Angelina Ivanovna.</ta>
            <ta e="T152" id="Seg_7984" s="T149">Ich wohne in ihrem Haus.</ta>
            <ta e="T157" id="Seg_7985" s="T152">Sie behandelt mich gut, sie schimpft nicht.</ta>
            <ta e="T163" id="Seg_7986" s="T157">Ich mache, was ich will.</ta>
            <ta e="T167" id="Seg_7987" s="T163">Sie hat drei Zimmer.</ta>
            <ta e="T170" id="Seg_7988" s="T167">Die Zimmer sind groß.</ta>
            <ta e="T174" id="Seg_7989" s="T170">Im Haus ist es warm, es gibt eine Zentralheizung.</ta>
            <ta e="T182" id="Seg_7990" s="T174">Es gibt Wasser im Haus: heißes Wasser und kaltes Wasser.</ta>
            <ta e="T186" id="Seg_7991" s="T182">Es gibt Wasser und alles im Haus.</ta>
            <ta e="T189" id="Seg_7992" s="T186">Wir waschen uns in einem eigenen Raum.</ta>
            <ta e="T195" id="Seg_7993" s="T189">Dort gibt es heißes und kaltes Wasser.</ta>
            <ta e="T199" id="Seg_7994" s="T195">Wir scheißen und pinkeln im Haus.</ta>
            <ta e="T206" id="Seg_7995" s="T199">Alles wird rausgespült und es gibt keinen Gestank.</ta>
            <ta e="T211" id="Seg_7996" s="T206">Du ziehst an der Kette und Wasser fließt.</ta>
            <ta e="T214" id="Seg_7997" s="T211">Alles wird komplett rausgespült.</ta>
            <ta e="T224" id="Seg_7998" s="T214">Die Häuser sind hier groß, vierstöckig, sie sind alle gleich.</ta>
            <ta e="T226" id="Seg_7999" s="T224">Die Häuser sind lang.</ta>
            <ta e="T235" id="Seg_8000" s="T226">Nicht weit gibt es viele Bäume: Birken, Pappeln, Tannen, Kiefern, Wildkirsche, Espen.</ta>
            <ta e="T240" id="Seg_8001" s="T235">Hier gibt es im Sommer viele Pilze (=Viele Pilze werden hier im Sommer geboren).</ta>
            <ta e="T244" id="Seg_8002" s="T240">Im Sommer sammeln wir Pilze.</ta>
            <ta e="T248" id="Seg_8003" s="T244">Im Frühling gibt es viele Blumen.</ta>
            <ta e="T251" id="Seg_8004" s="T248">Im Winter gehen die Leute Skilaufen.</ta>
            <ta e="T254" id="Seg_8005" s="T251">Die Kinder gehen rodeln (mit den Schlitten).</ta>
            <ta e="T260" id="Seg_8006" s="T254">Im Herbst werden alle Blätter gelb.</ta>
            <ta e="T267" id="Seg_8007" s="T260">Als wir in Novosibirsk ankamen, übernachteten wir im Bahnhof von Novosibirsk.</ta>
            <ta e="T270" id="Seg_8008" s="T267">Wir übernachteten im obersten Stockwerk.</ta>
            <ta e="T273" id="Seg_8009" s="T270">Im fünften Stock schliefen wir.</ta>
            <ta e="T279" id="Seg_8010" s="T273">Man hat uns alles gegeben: Matratzen, Decken, Kissen.</ta>
            <ta e="T281" id="Seg_8011" s="T279">Wir haben gut geschlafen.</ta>
            <ta e="T284" id="Seg_8012" s="T281">Da waren viele Menschen.</ta>
            <ta e="T286" id="Seg_8013" s="T284">Ein großer Bahnhof.</ta>
            <ta e="T290" id="Seg_8014" s="T286">Man kann sich verlaufen und den Ausgang nicht finden.</ta>
            <ta e="T293" id="Seg_8015" s="T290">Am Morgen gingen wir in eine Kantine.</ta>
            <ta e="T296" id="Seg_8016" s="T293">Sie sagten immer "Restaurant".</ta>
            <ta e="T301" id="Seg_8017" s="T296">Dort ist es gut, wie im Paradies.</ta>
            <ta e="T304" id="Seg_8018" s="T301">Dort haben wir gegessen.</ta>
            <ta e="T308" id="Seg_8019" s="T304">In der Nähe ist das Meer.</ta>
            <ta e="T312" id="Seg_8020" s="T308">Dort fischen die Menschen mit Angeln.</ta>
            <ta e="T316" id="Seg_8021" s="T312">Sie fingen Barsche und Rotaugen(?).</ta>
            <ta e="T322" id="Seg_8022" s="T316">Ein Mädchen, die Tochter der Wirtin brachte für gewöhnlich [Fisch], sie brachte einen Barsch.</ta>
            <ta e="T324" id="Seg_8023" s="T322">Der Barsch war gefroren.</ta>
            <ta e="T327" id="Seg_8024" s="T324">Die Katze aß ihn.</ta>
            <ta e="T331" id="Seg_8025" s="T327">Ich habe zwei Freundinnen.</ta>
            <ta e="T334" id="Seg_8026" s="T331">Ich gehe sie besuchen.</ta>
            <ta e="T338" id="Seg_8027" s="T334">Und sie kommen zu mir.</ta>
            <ta e="T341" id="Seg_8028" s="T338">Ich knüpfe Fischernetze.</ta>
            <ta e="T345" id="Seg_8029" s="T341">Ich will nicht nur sitzen und nichts tun.</ta>
            <ta e="T349" id="Seg_8030" s="T345">Und ich singe die ganze Zeit Lieder.</ta>
            <ta e="T356" id="Seg_8031" s="T349">Im Frühling gehe ich nach Starosondorovo um zu fischen und Beeren zu sammeln.</ta>
            <ta e="T359" id="Seg_8032" s="T356">Die Stadt ist groß.</ta>
            <ta e="T366" id="Seg_8033" s="T359">Ich komme und zeige dir die Stadt.</ta>
            <ta e="T370" id="Seg_8034" s="T366">Ich werde dir die Stadt bringen.</ta>
            <ta e="T372" id="Seg_8035" s="T370">Du wirst sehen.</ta>
            <ta e="T376" id="Seg_8036" s="T372">Ich fuhr hier mit verschiedenen Autos.</ta>
            <ta e="T379" id="Seg_8037" s="T376">Beim ersten Mal hatte ich Angst.</ta>
            <ta e="T383" id="Seg_8038" s="T379">Volodya fuhr mich in der Stadt herum.</ta>
            <ta e="T385" id="Seg_8039" s="T383">Er zeigte mir alles.</ta>
            <ta e="T390" id="Seg_8040" s="T385">Wenn du irgendwohin alleine hingehst, verläufst du dich.</ta>
            <ta e="T395" id="Seg_8041" s="T390">Ich musste pinkeln und sagte: "[Ich] muss mal."</ta>
            <ta e="T401" id="Seg_8042" s="T395">Und Volodya sagte zu mir: "Wohin willst du pinkeln?</ta>
            <ta e="T404" id="Seg_8043" s="T401">Es gibt hier kein Klo.</ta>
            <ta e="T410" id="Seg_8044" s="T404">Alle Klos sind in den Häusern."</ta>
            <ta e="T412" id="Seg_8045" s="T410">Wir waren auf dem Markt.</ta>
            <ta e="T419" id="Seg_8046" s="T412">Er sagte zu mir: "Warum konntest du nicht dort pinkeln?"</ta>
            <ta e="T423" id="Seg_8047" s="T419">Und ich habe ich nicht gepinkelt.</ta>
            <ta e="T426" id="Seg_8048" s="T423">Also hielt ich es durch.</ta>
            <ta e="T436" id="Seg_8049" s="T426">Als wir auf ein Auto warteten, fragte ich eine Frau: "Wo gibt es ein Klo?"</ta>
            <ta e="T440" id="Seg_8050" s="T436">Sie zeigte es mir: "Dort."</ta>
            <ta e="T444" id="Seg_8051" s="T440">Ich lief [dorthin] und pinkelte.</ta>
            <ta e="T449" id="Seg_8052" s="T444">Ich dachte, der Urin würde abfließen.</ta>
            <ta e="T453" id="Seg_8053" s="T449">Ich pinkelte und war glücklich.</ta>
            <ta e="T457" id="Seg_8054" s="T453">Wir gingen einkaufen, [in] die großen Läden.</ta>
            <ta e="T459" id="Seg_8055" s="T457">Dort kann man sich verlaufen.</ta>
            <ta e="T463" id="Seg_8056" s="T459">Ich kann nicht schreiben.</ta>
            <ta e="T469" id="Seg_8057" s="T463">Ich laufe nicht alleine in der Stadt umher, sonst verlaufe ich mich.</ta>
            <ta e="T473" id="Seg_8058" s="T469">Ich fahre mit den Autos.</ta>
            <ta e="T479" id="Seg_8059" s="T473">Ich lebte in der Stadt (Tomsk), in Chornaja Rechka und in Takhtomyshevo.</ta>
            <ta e="T482" id="Seg_8060" s="T479">Dunja zog nach Chornaja Rechka.</ta>
            <ta e="T485" id="Seg_8061" s="T482">Sie lebt gemeinsam mit ihrem Sohn.</ta>
            <ta e="T489" id="Seg_8062" s="T485">Nikolaj Lazarychs Frau starb.</ta>
            <ta e="T492" id="Seg_8063" s="T489">Sie lebte in Chornaja Rechka mit ihrer Tochter.</ta>
            <ta e="T497" id="Seg_8064" s="T492">Und ich ging Städte besuchen.</ta>
            <ta e="T508" id="Seg_8065" s="T497">Marina Pavlovna, wenn mein Brief ankommt, schreib mir gleich eine Antwort.</ta>
            <ta e="T510" id="Seg_8066" s="T508">Ich werde warten.</ta>
            <ta e="T516" id="Seg_8067" s="T510">Schreib alles darüber, wie es meinen Leuten geht.</ta>
            <ta e="T522" id="Seg_8068" s="T516">Hast du etwas von Kolya gehört oder nicht?</ta>
            <ta e="T525" id="Seg_8069" s="T522">Schreib mir alles.</ta>
            <ta e="T528" id="Seg_8070" s="T525">Ich werde gespannt warten.</ta>
            <ta e="T534" id="Seg_8071" s="T528">Verkaufen sie euch Brot oder Mehl?</ta>
            <ta e="T540" id="Seg_8072" s="T534">Und schreib mir über alles, was im Laden ist.</ta>
            <ta e="T545" id="Seg_8073" s="T540">Lebt dein Schwiegersohn [bei dir] oder nicht?</ta>
            <ta e="T548" id="Seg_8074" s="T545">Er sagte, er würde gehen.</ta>
            <ta e="T554" id="Seg_8075" s="T548">Schreib von Zinka, wie es ihr geht.</ta>
            <ta e="T558" id="Seg_8076" s="T554">Ich habe ihr einen Brief geschrieben.</ta>
            <ta e="T562" id="Seg_8077" s="T558">Sie antwortete mir nicht.</ta>
            <ta e="T567" id="Seg_8078" s="T562">Tante Marina, frohen ersten Mai!</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T14" id="Seg_8079" s="T1">Сидим мы сегодня с Ангелиной Ивановной, разговариваем, о тебе говорим, как живешь, что делаешь.</ta>
            <ta e="T20" id="Seg_8080" s="T14">Я ей говорю о тебе.</ta>
            <ta e="T24" id="Seg_8081" s="T20">Я говорю: “Она состарилась”.</ta>
            <ta e="T35" id="Seg_8082" s="T24">Я ей рассказала, (как) ты нас с Марией кормила, как мы с тобой выпивали.</ta>
            <ta e="T39" id="Seg_8083" s="T35">Я ей все рассказала.</ta>
            <ta e="T45" id="Seg_8084" s="T39">Я говорила: “Тетке Марине письмо напишу”.</ta>
            <ta e="T47" id="Seg_8085" s="T45">Пусть читает.</ta>
            <ta e="T50" id="Seg_8086" s="T47">Здравствуй, Марина Павловна!</ta>
            <ta e="T55" id="Seg_8087" s="T50">Я тебе по-селькупски письмо напишу.</ta>
            <ta e="T59" id="Seg_8088" s="T55">По-русски я не умею.</ta>
            <ta e="T64" id="Seg_8089" s="T59">Я живу теперь в городе, в Новосибирске.</ta>
            <ta e="T67" id="Seg_8090" s="T64">Город большой, красивый.</ta>
            <ta e="T71" id="Seg_8091" s="T67">Из города (Томска) в Новосибирск я на поезде ехала.</ta>
            <ta e="T74" id="Seg_8092" s="T71">Когда мы ехали на поезде, мы играли в карты.</ta>
            <ta e="T79" id="Seg_8093" s="T74">На станции «Тайга» поезд долго стоял.</ta>
            <ta e="T86" id="Seg_8094" s="T79">Я смотрела и Никина(/Никона) нигде не видела.</ta>
            <ta e="T89" id="Seg_8095" s="T86">Потом поезд тронулся.</ta>
            <ta e="T93" id="Seg_8096" s="T89">Я все в окошко смотрела.</ta>
            <ta e="T97" id="Seg_8097" s="T93">Деревней много оставалось.</ta>
            <ta e="T101" id="Seg_8098" s="T97">На краю города деревья (леса), тайга.</ta>
            <ta e="T104" id="Seg_8099" s="T101">Кругом снег лежит.</ta>
            <ta e="T108" id="Seg_8100" s="T104">Я на поезде не боялась.</ta>
            <ta e="T110" id="Seg_8101" s="T108">Немножко качает.</ta>
            <ta e="T115" id="Seg_8102" s="T110">Раньше я не ездила на поездах.</ta>
            <ta e="T125" id="Seg_8103" s="T115">Я живу у той женщины, которая к нам приезжала, селькупский язык писала.</ta>
            <ta e="T140" id="Seg_8104" s="T125">Из города (из Томска) женщина, которая приезжала в Старосондорово, по-селькупски слова все записывала, к тебе ходила, с тобой все писала.</ta>
            <ta e="T145" id="Seg_8105" s="T140">Я у этой женщины живу, в Новосибирске.</ta>
            <ta e="T149" id="Seg_8106" s="T145">Ее имя Ангелина Ивановна.</ta>
            <ta e="T152" id="Seg_8107" s="T149">У нее я живу.</ta>
            <ta e="T157" id="Seg_8108" s="T152">Меня держит хорошо, не ругается.</ta>
            <ta e="T163" id="Seg_8109" s="T157">Я сама делаю, что хочу.</ta>
            <ta e="T167" id="Seg_8110" s="T163">У нее три комнаты.</ta>
            <ta e="T170" id="Seg_8111" s="T167">Комнаты большие.</ta>
            <ta e="T174" id="Seg_8112" s="T170">В доме тепло, паровое отпление.</ta>
            <ta e="T182" id="Seg_8113" s="T174">Вода в доме есть: теплая вода и холодная вода.</ta>
            <ta e="T186" id="Seg_8114" s="T182">Вода, всё в доме есть.</ta>
            <ta e="T189" id="Seg_8115" s="T186">В одной комнате моемся.</ta>
            <ta e="T195" id="Seg_8116" s="T189">Там горячая вода и холодная вода.</ta>
            <ta e="T199" id="Seg_8117" s="T195">Дома какаем и писаем.</ta>
            <ta e="T206" id="Seg_8118" s="T199">Все вымывается и никакого запаха нет.</ta>
            <ta e="T211" id="Seg_8119" s="T206">Цепочку как вздернешь, вода льется (бурлить осталась).</ta>
            <ta e="T214" id="Seg_8120" s="T211">Все чисто вымывается.</ta>
            <ta e="T224" id="Seg_8121" s="T214">Дома здесь большие, четырехэтажные, все одинаковые (как один) дома.</ta>
            <ta e="T226" id="Seg_8122" s="T224">Дома длинные.</ta>
            <ta e="T235" id="Seg_8123" s="T226">Недалеко деревья: березы, тополя, елки, сосны, черемухи, осины.</ta>
            <ta e="T240" id="Seg_8124" s="T235">Здесь летом много грибов родится.</ta>
            <ta e="T244" id="Seg_8125" s="T240">Мы летом грибы собирать будем.</ta>
            <ta e="T248" id="Seg_8126" s="T244">Весной много цветов.</ta>
            <ta e="T251" id="Seg_8127" s="T248">Зимой на лыжах ходят.</ta>
            <ta e="T254" id="Seg_8128" s="T251">Дети катаются на салазках.</ta>
            <ta e="T260" id="Seg_8129" s="T254">Осенью все листья пожелтеют.</ta>
            <ta e="T267" id="Seg_8130" s="T260">Когда мы прибыли в Новосибирск, ночевали в Новосибирске на вокзале.</ta>
            <ta e="T270" id="Seg_8131" s="T267">На верхнем этаже ночевали.</ta>
            <ta e="T273" id="Seg_8132" s="T270">На пятом этаже спали.</ta>
            <ta e="T279" id="Seg_8133" s="T273">Нам все дали: матрасы, одеяла, подушки.</ta>
            <ta e="T281" id="Seg_8134" s="T279">Мы спали хорошо.</ta>
            <ta e="T284" id="Seg_8135" s="T281">Людей много было.</ta>
            <ta e="T286" id="Seg_8136" s="T284">Большой вокзал.</ta>
            <ta e="T290" id="Seg_8137" s="T286">Там заблудишься и не выйдешь.</ta>
            <ta e="T293" id="Seg_8138" s="T290">Утром пошли в столовую.</ta>
            <ta e="T296" id="Seg_8139" s="T293">Они говорят «Ресторан».</ta>
            <ta e="T301" id="Seg_8140" s="T296">Там хорошо, как в раю.</ta>
            <ta e="T304" id="Seg_8141" s="T301">Там и ели.</ta>
            <ta e="T308" id="Seg_8142" s="T304">У нас море близко.</ta>
            <ta e="T312" id="Seg_8143" s="T308">Там люди рыбачат удочкой.</ta>
            <ta e="T316" id="Seg_8144" s="T312">Ершей добывают и чераков (плотву?).</ta>
            <ta e="T322" id="Seg_8145" s="T316">Девочка, дочка хозяйки, приносила, ерша принесла.</ta>
            <ta e="T324" id="Seg_8146" s="T322">Ерш замерз.</ta>
            <ta e="T327" id="Seg_8147" s="T324">Кошка его съела.</ta>
            <ta e="T331" id="Seg_8148" s="T327">У меня две подруги.</ta>
            <ta e="T334" id="Seg_8149" s="T331">Я к ним хожу.</ta>
            <ta e="T338" id="Seg_8150" s="T334">И они ко мне ходят.</ta>
            <ta e="T341" id="Seg_8151" s="T338">Я вяжу сетки.</ta>
            <ta e="T345" id="Seg_8152" s="T341">Просто так сидеть не хочу.</ta>
            <ta e="T349" id="Seg_8153" s="T345">А сама все время песни пою.</ta>
            <ta e="T356" id="Seg_8154" s="T349">Весной я приеду в Старосондрово рыбачить и ягоду собирать.</ta>
            <ta e="T359" id="Seg_8155" s="T356">Город большой.</ta>
            <ta e="T366" id="Seg_8156" s="T359">Я как приеду и покажу тебе город.</ta>
            <ta e="T370" id="Seg_8157" s="T366">Тебе этот город привезу.</ta>
            <ta e="T372" id="Seg_8158" s="T370">Ты посмотришь.</ta>
            <ta e="T376" id="Seg_8159" s="T372">Здесь на всяких машинах ездила.</ta>
            <ta e="T379" id="Seg_8160" s="T376">Первый раз я боялась.</ta>
            <ta e="T383" id="Seg_8161" s="T379">Меня Володя по городу водил.</ta>
            <ta e="T385" id="Seg_8162" s="T383">Все показывал.</ta>
            <ta e="T390" id="Seg_8163" s="T385">Сама-то куда пойдешь, заблудишься.</ta>
            <ta e="T395" id="Seg_8164" s="T390">Я писать захотела, говорю: “Писать надо”.</ta>
            <ta e="T401" id="Seg_8165" s="T395">А Володя мне говорит: “Где писать будешь?</ta>
            <ta e="T404" id="Seg_8166" s="T401">Уборной нет.</ta>
            <ta e="T410" id="Seg_8167" s="T404">Туалеты все в доме находятся”.</ta>
            <ta e="T412" id="Seg_8168" s="T410">На базаре были.</ta>
            <ta e="T419" id="Seg_8169" s="T412">Он мне сказал: “Там почему не писала?”</ta>
            <ta e="T423" id="Seg_8170" s="T419">А я не пописала.</ta>
            <ta e="T426" id="Seg_8171" s="T423">Так я и терпела.</ta>
            <ta e="T436" id="Seg_8172" s="T426">Когда машину ждали, я у одной женщины спросила: “Где уборная?”</ta>
            <ta e="T440" id="Seg_8173" s="T436">Она мне показала: “Туда”.</ta>
            <ta e="T444" id="Seg_8174" s="T440">Я побежала и пописала.</ta>
            <ta e="T449" id="Seg_8175" s="T444">Я думала, у меня моча польется.</ta>
            <ta e="T453" id="Seg_8176" s="T449">Я пописала и хорошо стало.</ta>
            <ta e="T457" id="Seg_8177" s="T453">В магазины заходили, большие магазины.</ta>
            <ta e="T459" id="Seg_8178" s="T457">Там и заблудишься.</ta>
            <ta e="T463" id="Seg_8179" s="T459">Я неграмотная.</ta>
            <ta e="T469" id="Seg_8180" s="T463">Сама не хожу по городу, а то заблужусь.</ta>
            <ta e="T473" id="Seg_8181" s="T469">Я теперь на машинах езжу.</ta>
            <ta e="T479" id="Seg_8182" s="T473">Жила в городе (Томске) в Черной речке и в Тахтомышево жила.</ta>
            <ta e="T482" id="Seg_8183" s="T479">Дуня приехала в Черную речку.</ta>
            <ta e="T485" id="Seg_8184" s="T482">С сыном вдвоем живут.</ta>
            <ta e="T489" id="Seg_8185" s="T485">Жена Николая Лазарыча умерла.</ta>
            <ta e="T492" id="Seg_8186" s="T489">Она в Черной речке жила у дочери.</ta>
            <ta e="T497" id="Seg_8187" s="T492">А я поехала города смотреть.</ta>
            <ta e="T508" id="Seg_8188" s="T497">Марина Павловна, когда мое письмо придет, и мне письмо сразу напиши.</ta>
            <ta e="T510" id="Seg_8189" s="T508">Я буду ждать.</ta>
            <ta e="T516" id="Seg_8190" s="T510">Все напиши, как мои люди живут.</ta>
            <ta e="T522" id="Seg_8191" s="T516">О Коле что-то слышала или нет?</ta>
            <ta e="T525" id="Seg_8192" s="T522">Все мне напиши.</ta>
            <ta e="T528" id="Seg_8193" s="T525">Я буду сильно ждать.</ta>
            <ta e="T534" id="Seg_8194" s="T528">Вам хлебом дают (продают) или мукой?</ta>
            <ta e="T540" id="Seg_8195" s="T534">И в магазине что есть, все напиши.</ta>
            <ta e="T545" id="Seg_8196" s="T540">Зять твой живет или нет?</ta>
            <ta e="T548" id="Seg_8197" s="T545">Говорили, уехал (он).</ta>
            <ta e="T554" id="Seg_8198" s="T548">О Зинке напиши, как живет.</ta>
            <ta e="T558" id="Seg_8199" s="T554">Я ей письмо написала.</ta>
            <ta e="T562" id="Seg_8200" s="T558">Она мне не написала.</ta>
            <ta e="T567" id="Seg_8201" s="T562">Тетя Марина, с Первым маем!</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T14" id="Seg_8202" s="T1">Сидим мы сегодня с Ангелиной Ивановной, разговариваем, о тебе говорим, как живешь что делаешь.</ta>
            <ta e="T20" id="Seg_8203" s="T14">Я ей говорю о тебе.</ta>
            <ta e="T24" id="Seg_8204" s="T20">Я говорю: она старенькая (теперь).</ta>
            <ta e="T35" id="Seg_8205" s="T24">Я ей рассказала (как) ты нас с Марией кормила, как мы с тобой выпивали.</ta>
            <ta e="T39" id="Seg_8206" s="T35">Я ей все рассказала.</ta>
            <ta e="T45" id="Seg_8207" s="T39">Я говорю: тетке Марине письмо напишу.</ta>
            <ta e="T47" id="Seg_8208" s="T45">Пусть читает.</ta>
            <ta e="T50" id="Seg_8209" s="T47">Здравствуй, Марина Павловна!</ta>
            <ta e="T55" id="Seg_8210" s="T50">Я тебе по-остяцки письмо напишу.</ta>
            <ta e="T59" id="Seg_8211" s="T55">По-русски я не умею.</ta>
            <ta e="T64" id="Seg_8212" s="T59">Я живу теперь в городе Новосибирске.</ta>
            <ta e="T67" id="Seg_8213" s="T64">Город большой, красивый.</ta>
            <ta e="T71" id="Seg_8214" s="T67">Из города (Томска) в Новосибирск я на поезде ехала.</ta>
            <ta e="T74" id="Seg_8215" s="T71">Когда мы ехали на поезде, мы играли в карты.</ta>
            <ta e="T79" id="Seg_8216" s="T74">На станции «Тайга» поезд долго стоял.</ta>
            <ta e="T86" id="Seg_8217" s="T79">Я смотрела и Ники(о)на нигде не видела.</ta>
            <ta e="T89" id="Seg_8218" s="T86">Потом поезд тронулся.</ta>
            <ta e="T93" id="Seg_8219" s="T89">Я все в окошко смотрела.</ta>
            <ta e="T97" id="Seg_8220" s="T93">Деревней много оставало(и)сь.</ta>
            <ta e="T101" id="Seg_8221" s="T97">На краю города деревья (леса), тайга.</ta>
            <ta e="T104" id="Seg_8222" s="T101">Кругом снег лежит.</ta>
            <ta e="T108" id="Seg_8223" s="T104">Я на поезде не боялась.</ta>
            <ta e="T110" id="Seg_8224" s="T108">Немножко качает.</ta>
            <ta e="T115" id="Seg_8225" s="T110">Раньше я не ездила на поездах.</ta>
            <ta e="T125" id="Seg_8226" s="T115">Я живу у той женщины, которая к нам приезжала, остяцкий язык писала.</ta>
            <ta e="T140" id="Seg_8227" s="T125">С города (из Томска) женщина, которая приезжала в Старосондорово, по-остяцки слова все записывала, к тебе ходила, с тобой все писала.</ta>
            <ta e="T145" id="Seg_8228" s="T140">Я у этой женщины живу, в Новосибирске.</ta>
            <ta e="T149" id="Seg_8229" s="T145">Ее имя Ангелина Ивановна.</ta>
            <ta e="T152" id="Seg_8230" s="T149">У нее я живу.</ta>
            <ta e="T157" id="Seg_8231" s="T152">Меня держит хорошо, не ругается.</ta>
            <ta e="T163" id="Seg_8232" s="T157">Я сама делаю, что хочу.</ta>
            <ta e="T167" id="Seg_8233" s="T163">У нее три комнаты.</ta>
            <ta e="T170" id="Seg_8234" s="T167">Комнаты большие.</ta>
            <ta e="T174" id="Seg_8235" s="T170">В избе тепло, паровое отпление.</ta>
            <ta e="T182" id="Seg_8236" s="T174">Вода в доме есть: теплая вода и холодная вода.</ta>
            <ta e="T186" id="Seg_8237" s="T182">Все в доме есть.</ta>
            <ta e="T189" id="Seg_8238" s="T186">В одной комнате моемся.</ta>
            <ta e="T195" id="Seg_8239" s="T189">Там горячая и холодная вода.</ta>
            <ta e="T199" id="Seg_8240" s="T195">Дома какаем и писаем.</ta>
            <ta e="T206" id="Seg_8241" s="T199">Все вымывается и никакого запаха нет.</ta>
            <ta e="T211" id="Seg_8242" s="T206">Цепочку как вздернешь, вода льется (бурлить осталась).</ta>
            <ta e="T214" id="Seg_8243" s="T211">Все чисто вымывается.</ta>
            <ta e="T224" id="Seg_8244" s="T214">Дома здесь большие, четырехэтажные, все одинаковые (как один) дома.</ta>
            <ta e="T226" id="Seg_8245" s="T224">Дома длинные.</ta>
            <ta e="T235" id="Seg_8246" s="T226">Недалеко деревья: березы, тополя, елки, сосны, черемуха, осина.</ta>
            <ta e="T240" id="Seg_8247" s="T235">Летом здесь много грибов (растет) родится.</ta>
            <ta e="T244" id="Seg_8248" s="T240">Мы летом грибы собирать будем.</ta>
            <ta e="T248" id="Seg_8249" s="T244">Весной много цветов.</ta>
            <ta e="T251" id="Seg_8250" s="T248">Зимой на лыжах ходят.</ta>
            <ta e="T254" id="Seg_8251" s="T251">Дети катаются на салазках.</ta>
            <ta e="T260" id="Seg_8252" s="T254">Осенью все листья пожелтеют (Я желтая буду).</ta>
            <ta e="T267" id="Seg_8253" s="T260">Когда мы прибыли в Новосибирск, ночевали в Новосибирском вокзале.</ta>
            <ta e="T270" id="Seg_8254" s="T267">На верхнем этаже ночевали.</ta>
            <ta e="T273" id="Seg_8255" s="T270">На пятом этаже ночевали (спали).</ta>
            <ta e="T279" id="Seg_8256" s="T273">Нам все дали: матрасы, одеяла, подушки.</ta>
            <ta e="T281" id="Seg_8257" s="T279">Ночевали хорошо.</ta>
            <ta e="T284" id="Seg_8258" s="T281">Людей много было.</ta>
            <ta e="T286" id="Seg_8259" s="T284">Большой вокзал.</ta>
            <ta e="T290" id="Seg_8260" s="T286">Там заблудишься и не выйдешь.</ta>
            <ta e="T293" id="Seg_8261" s="T290">Утром пошли в столовую.</ta>
            <ta e="T296" id="Seg_8262" s="T293">Они говорят «Ресторан».</ta>
            <ta e="T301" id="Seg_8263" s="T296">Там хорошо, как в раю.</ta>
            <ta e="T304" id="Seg_8264" s="T301">Там и ели.</ta>
            <ta e="T308" id="Seg_8265" s="T304">У нас море близко.</ta>
            <ta e="T312" id="Seg_8266" s="T308">Там люди рыбачат удочкой.</ta>
            <ta e="T316" id="Seg_8267" s="T312">Ершей добывают и чераков.</ta>
            <ta e="T322" id="Seg_8268" s="T316">Девочка, дочка хозяйки, ерша принесла.</ta>
            <ta e="T324" id="Seg_8269" s="T322">Ерш замерз.</ta>
            <ta e="T327" id="Seg_8270" s="T324">Кошка его съела.</ta>
            <ta e="T331" id="Seg_8271" s="T327">У меня две подруги.</ta>
            <ta e="T334" id="Seg_8272" s="T331">Я к ним хожу.</ta>
            <ta e="T338" id="Seg_8273" s="T334">Они ко мне ходят.</ta>
            <ta e="T341" id="Seg_8274" s="T338">Я вяжу сетки.</ta>
            <ta e="T345" id="Seg_8275" s="T341">Так сидеть не хочу.</ta>
            <ta e="T349" id="Seg_8276" s="T345">А сама все время песни пою.</ta>
            <ta e="T356" id="Seg_8277" s="T349">Весной я приеду в Сондрово рыбачить и ягоду брать.</ta>
            <ta e="T359" id="Seg_8278" s="T356">Город большой.</ta>
            <ta e="T366" id="Seg_8279" s="T359">Я как приеду и покажу тебе город.</ta>
            <ta e="T370" id="Seg_8280" s="T366">Тебе этот город привезу.</ta>
            <ta e="T372" id="Seg_8281" s="T370">Ты посмотришь (смотреть будешь).</ta>
            <ta e="T376" id="Seg_8282" s="T372">Здесь на всяких машинах ездила.</ta>
            <ta e="T379" id="Seg_8283" s="T376">Первый раз боялась.</ta>
            <ta e="T383" id="Seg_8284" s="T379">Меня Володя по городу водил.</ta>
            <ta e="T385" id="Seg_8285" s="T383">Все показывал.</ta>
            <ta e="T390" id="Seg_8286" s="T385">Сама-то куда пойдешь, заблудишься.</ta>
            <ta e="T395" id="Seg_8287" s="T390">Я ′писать захотела, говорю: ′Писать надо.</ta>
            <ta e="T401" id="Seg_8288" s="T395">А Володя мне говорит: Где ′писать будешь?</ta>
            <ta e="T404" id="Seg_8289" s="T401">Уборной нет.</ta>
            <ta e="T410" id="Seg_8290" s="T404">Туалеты все в избе находятся.</ta>
            <ta e="T412" id="Seg_8291" s="T410">На базаре были.</ta>
            <ta e="T419" id="Seg_8292" s="T412">Он мне сказал: Там почему не ′писала?</ta>
            <ta e="T423" id="Seg_8293" s="T419">А я не хотела ′писать.</ta>
            <ta e="T426" id="Seg_8294" s="T423">Так я и терпела.</ta>
            <ta e="T436" id="Seg_8295" s="T426">Когда машину ждали, я у одной женщины спросила: Где уборная?</ta>
            <ta e="T440" id="Seg_8296" s="T436">Она мне показала: Там.</ta>
            <ta e="T444" id="Seg_8297" s="T440">Я побежала и пописала.</ta>
            <ta e="T449" id="Seg_8298" s="T444">Я думала: У меня ссаки побегут.</ta>
            <ta e="T453" id="Seg_8299" s="T449">Пописала и хорошо стало.</ta>
            <ta e="T457" id="Seg_8300" s="T453">По магазинам ходили, большие магазины.</ta>
            <ta e="T459" id="Seg_8301" s="T457">Там и заблудишься.</ta>
            <ta e="T463" id="Seg_8302" s="T459">Я неграмотная (писать не умею).</ta>
            <ta e="T469" id="Seg_8303" s="T463">Сама не хожу по городу, а то заблужусь.</ta>
            <ta e="T473" id="Seg_8304" s="T469">Я теперь на машинах езжу.</ta>
            <ta e="T479" id="Seg_8305" s="T473">Жила в городе (Томске) в Черной речке и в Тахтомышево жила.</ta>
            <ta e="T482" id="Seg_8306" s="T479">Дуня приехала в Черную речку.</ta>
            <ta e="T485" id="Seg_8307" s="T482">С сыном вдвоем живут.</ta>
            <ta e="T489" id="Seg_8308" s="T485">Жена Николая Лазарыча умерла.</ta>
            <ta e="T492" id="Seg_8309" s="T489">В Черной речке жила у дочери.</ta>
            <ta e="T497" id="Seg_8310" s="T492">А я поехала города смотреть.</ta>
            <ta e="T508" id="Seg_8311" s="T497">Марина Павловна, когда мое письмо придет, мне напиши сразу письмо.</ta>
            <ta e="T510" id="Seg_8312" s="T508">Я буду ждать.</ta>
            <ta e="T516" id="Seg_8313" s="T510">Все напиши, как мои (люди) живут.</ta>
            <ta e="T522" id="Seg_8314" s="T516">о Коле</ta>
            <ta e="T525" id="Seg_8315" s="T522">Все мне напиши.</ta>
            <ta e="T528" id="Seg_8316" s="T525">Я буду сильно ждать.</ta>
            <ta e="T534" id="Seg_8317" s="T528">Вам хлебом дают (продают) или мукой?</ta>
            <ta e="T540" id="Seg_8318" s="T534">И в магазине что есть, все напиши.</ta>
            <ta e="T545" id="Seg_8319" s="T540">Зять живет или нет?</ta>
            <ta e="T548" id="Seg_8320" s="T545">Говорят, уехал (он).</ta>
            <ta e="T554" id="Seg_8321" s="T548">О Зинке напиши, как живет.</ta>
            <ta e="T558" id="Seg_8322" s="T554">Я ей письмо написала.</ta>
            <ta e="T562" id="Seg_8323" s="T558">Она мне не написала.</ta>
            <ta e="T567" id="Seg_8324" s="T562">Тетя Марина, с Первым маем!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T14" id="Seg_8325" s="T1"> || Here is the introduction to the letter of Vera Demidovna Pidogina to Marina Pavlovna Pidogina (to Novosondorovo village). Novosibirsk, 24.04.1964.</ta>
            <ta e="T50" id="Seg_8326" s="T47">Here is the beginning of the letter.</ta>
            <ta e="T145" id="Seg_8327" s="T140">[BrM:] 'nanäjɣunnan' changed to 'na näjɣunnan'.</ta>
            <ta e="T167" id="Seg_8328" s="T163">[KuAI:] Variant: 'sütʼdʼi'.</ta>
            <ta e="T186" id="Seg_8329" s="T182">[BrM:] 3PL or INFER?</ta>
            <ta e="T206" id="Seg_8330" s="T199">[BrM:] qwatdəqut 3SG.O? Water washes everithing out?</ta>
            <ta e="T211" id="Seg_8331" s="T206">[KuAI:] (Üt šoɣɨpʼlʼe na ɣalɨtda). </ta>
            <ta e="T214" id="Seg_8332" s="T211">[BrM:] Tentative analysis. || Tentative analysis.</ta>
            <ta e="T235" id="Seg_8333" s="T226">[BrM:] 'üdə piːla' changed to 'üdəpiːla'.</ta>
            <ta e="T240" id="Seg_8334" s="T235">[KuAI:] Variant: 'Taftʼen'.</ta>
            <ta e="T260" id="Seg_8335" s="T254">[KuAI:] Man azöǯan – Я желтая буду. [BrM:] 'azölʼe ǯattə' changed to 'azölʼeǯattə'. [BrM:] šolta-na TRL or EMPH?</ta>
            <ta e="T273" id="Seg_8336" s="T270">[BrM:] 'Soblam tätte' changed to 'Soblamtätte'.</ta>
            <ta e="T349" id="Seg_8337" s="T345">[BrM:] 'qojmut čukan' changed to 'qojmutčukan'.</ta>
            <ta e="T366" id="Seg_8338" s="T359">[BrM:] ACC?</ta>
            <ta e="T395" id="Seg_8339" s="T390">[BrM:] 'küssu kumnan' changed to 'küssukumnan'. [BrM:] Tentative analysis of 'küssukumnan'.</ta>
            <ta e="T423" id="Seg_8340" s="T419">[BrM:] The original translation is: I didn't want to piss.</ta>
            <ta e="T449" id="Seg_8341" s="T444">[BrM:] 'walwaǯən' changed to 'wal waǯən'. Tentative analysis of ''wal waǯən'.</ta>
            <ta e="T522" id="Seg_8342" s="T516">[KuAI:] Variant: 'ütdətükal'. [BrM:] 'Kolʼäntʼät' changed to 'Kolʼän tʼät'. [BrM:] TR -ču?</ta>
            <ta e="T528" id="Seg_8343" s="T525">[BrM:] TR?</ta>
            <ta e="T554" id="Seg_8344" s="T548"> ‎‎[KuAI:] Variant: 'Zinkandʼät'. [BrM:] 'Sinkandʼät' changed to 'Sinkan dʼät'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T14" id="Seg_8345" s="T1"> || Письмо Веры Демидовны Пидогиной в деревню Новосондорово Марине Павловне Пидогиной. Новосибирск, Академгородок, 24.IV.1964</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
