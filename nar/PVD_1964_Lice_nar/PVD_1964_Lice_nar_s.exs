<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Lice_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Lice_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">12</ud-information>
            <ud-information attribute-name="# HIAT:w">9</ud-information>
            <ud-information attribute-name="# e">9</ud-information>
            <ud-information attribute-name="# HIAT:u">3</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T10" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Manan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">olou</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">kazəmɨt</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Nawerno</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">uǯila</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_23" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">Tawaj</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">man</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">olou</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">mannebet</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T10" id="Seg_37" n="sc" s="T1">
               <ts e="T2" id="Seg_39" n="e" s="T1">Manan </ts>
               <ts e="T3" id="Seg_41" n="e" s="T2">olou </ts>
               <ts e="T4" id="Seg_43" n="e" s="T3">kazəmɨt. </ts>
               <ts e="T5" id="Seg_45" n="e" s="T4">Nawerno </ts>
               <ts e="T6" id="Seg_47" n="e" s="T5">uǯila. </ts>
               <ts e="T7" id="Seg_49" n="e" s="T6">Tawaj </ts>
               <ts e="T8" id="Seg_51" n="e" s="T7">man </ts>
               <ts e="T9" id="Seg_53" n="e" s="T8">olou </ts>
               <ts e="T10" id="Seg_55" n="e" s="T9">mannebet. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_56" s="T1">PVD_1964_Lice_nar.001 (001.001)</ta>
            <ta e="T6" id="Seg_57" s="T4">PVD_1964_Lice_nar.002 (001.002)</ta>
            <ta e="T10" id="Seg_58" s="T6">PVD_1964_Lice_nar.003 (001.003)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_59" s="T1">ма′нан о′lоу̹ казъ′мыт.</ta>
            <ta e="T6" id="Seg_60" s="T4">наверно уджи′ла.</ta>
            <ta e="T10" id="Seg_61" s="T6">тавай ман о′lоу̹ ′манне′б̂ет.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_62" s="T1">manan olou̹ kazəmɨt.</ta>
            <ta e="T6" id="Seg_63" s="T4">nawerno uǯila.</ta>
            <ta e="T10" id="Seg_64" s="T6">tawaj man olou̹ manneb̂et.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_65" s="T1">Manan olou kazəmɨt. </ta>
            <ta e="T6" id="Seg_66" s="T4">Nawerno uǯila. </ta>
            <ta e="T10" id="Seg_67" s="T6">Tawaj man olou mannebet. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_68" s="T1">ma-nan</ta>
            <ta e="T3" id="Seg_69" s="T2">olo-u</ta>
            <ta e="T4" id="Seg_70" s="T3">kazəmɨ-t</ta>
            <ta e="T5" id="Seg_71" s="T4">nawerno</ta>
            <ta e="T6" id="Seg_72" s="T5">uǯi-la</ta>
            <ta e="T7" id="Seg_73" s="T6">tawaj</ta>
            <ta e="T8" id="Seg_74" s="T7">man</ta>
            <ta e="T9" id="Seg_75" s="T8">olo-u</ta>
            <ta e="T10" id="Seg_76" s="T9">manne-b-et</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_77" s="T1">man-nan</ta>
            <ta e="T3" id="Seg_78" s="T2">olə-w</ta>
            <ta e="T4" id="Seg_79" s="T3">qəzəmbɨ-tɨn</ta>
            <ta e="T5" id="Seg_80" s="T4">nawerna</ta>
            <ta e="T6" id="Seg_81" s="T5">uǯi-la</ta>
            <ta e="T7" id="Seg_82" s="T6">dawaj</ta>
            <ta e="T8" id="Seg_83" s="T7">man</ta>
            <ta e="T9" id="Seg_84" s="T8">olə-w</ta>
            <ta e="T10" id="Seg_85" s="T9">*mantɨ-mbɨ-etɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_86" s="T1">I-ADES</ta>
            <ta e="T3" id="Seg_87" s="T2">head.[NOM]-1SG</ta>
            <ta e="T4" id="Seg_88" s="T3">itch-3PL</ta>
            <ta e="T5" id="Seg_89" s="T4">probably</ta>
            <ta e="T6" id="Seg_90" s="T5">louse-PL.[NOM]</ta>
            <ta e="T7" id="Seg_91" s="T6">HORT</ta>
            <ta e="T8" id="Seg_92" s="T7">I.GEN</ta>
            <ta e="T9" id="Seg_93" s="T8">head.[NOM]-1SG</ta>
            <ta e="T10" id="Seg_94" s="T9">look-DUR-IMP.2SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_95" s="T1">я-ADES</ta>
            <ta e="T3" id="Seg_96" s="T2">голова.[NOM]-1SG</ta>
            <ta e="T4" id="Seg_97" s="T3">чесаться-3PL</ta>
            <ta e="T5" id="Seg_98" s="T4">наверное</ta>
            <ta e="T6" id="Seg_99" s="T5">вошь-PL.[NOM]</ta>
            <ta e="T7" id="Seg_100" s="T6">HORT</ta>
            <ta e="T8" id="Seg_101" s="T7">я.GEN</ta>
            <ta e="T9" id="Seg_102" s="T8">голова.[NOM]-1SG</ta>
            <ta e="T10" id="Seg_103" s="T9">посмотреть-DUR-IMP.2SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_104" s="T1">pers-n:case</ta>
            <ta e="T3" id="Seg_105" s="T2">n.[n:case]-n:poss</ta>
            <ta e="T4" id="Seg_106" s="T3">v-v:pn</ta>
            <ta e="T5" id="Seg_107" s="T4">adv</ta>
            <ta e="T6" id="Seg_108" s="T5">n-n:num.[n:case]</ta>
            <ta e="T7" id="Seg_109" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_110" s="T7">pers</ta>
            <ta e="T9" id="Seg_111" s="T8">n.[n:case]-n:poss</ta>
            <ta e="T10" id="Seg_112" s="T9">v-v&gt;v-v:mood.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_113" s="T1">pers</ta>
            <ta e="T3" id="Seg_114" s="T2">n</ta>
            <ta e="T4" id="Seg_115" s="T3">v</ta>
            <ta e="T5" id="Seg_116" s="T4">adv</ta>
            <ta e="T6" id="Seg_117" s="T5">n</ta>
            <ta e="T7" id="Seg_118" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_119" s="T7">pers</ta>
            <ta e="T9" id="Seg_120" s="T8">n</ta>
            <ta e="T10" id="Seg_121" s="T9">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_122" s="T1">pro.h:Poss</ta>
            <ta e="T3" id="Seg_123" s="T2">np:P</ta>
            <ta e="T8" id="Seg_124" s="T7">pro.h:Poss</ta>
            <ta e="T9" id="Seg_125" s="T8">np:Th</ta>
            <ta e="T10" id="Seg_126" s="T9">0.2.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_127" s="T2">np:S</ta>
            <ta e="T4" id="Seg_128" s="T3">v:pred</ta>
            <ta e="T9" id="Seg_129" s="T8">np:O</ta>
            <ta e="T10" id="Seg_130" s="T9">0.2.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_131" s="T4">RUS:mod</ta>
            <ta e="T7" id="Seg_132" s="T6">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_133" s="T1">My head itches.</ta>
            <ta e="T6" id="Seg_134" s="T4">Probably, lice.</ta>
            <ta e="T10" id="Seg_135" s="T6">Come on, inspect my head.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_136" s="T1">Mein Kopf juckt.</ta>
            <ta e="T6" id="Seg_137" s="T4">Wahrscheinlich Läuse.</ta>
            <ta e="T10" id="Seg_138" s="T6">Los, untersuche meinen Kopf.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_139" s="T1">У меня голова чешется.</ta>
            <ta e="T6" id="Seg_140" s="T4">Наверное, вши.</ta>
            <ta e="T10" id="Seg_141" s="T6">Давай мою голову посмотри.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_142" s="T1">у меня голова чешется</ta>
            <ta e="T6" id="Seg_143" s="T4">наверно вши стали</ta>
            <ta e="T10" id="Seg_144" s="T6">давай мне в голове ищи</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T4" id="Seg_145" s="T1">[BrM:] 3PL?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
