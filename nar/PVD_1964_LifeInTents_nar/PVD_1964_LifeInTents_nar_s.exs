<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_LifeInTents_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_LifeInTents_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">46</ud-information>
            <ud-information attribute-name="# HIAT:w">37</ud-information>
            <ud-information attribute-name="# e">37</ud-information>
            <ud-information attribute-name="# HIAT:u">9</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T38" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Na</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">čorta</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">man</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">tɨldʼzʼen</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">ilʼeǯan</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_20" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">Qajno</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">nildʼzʼin</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">elattə</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">qaːzɨrbɨlʼe</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_35" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">Nawerno</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">uǯirbaːtt</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_44" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">Čamgɨn</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">uǯila</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">jekudattə</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_56" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">Čʼum</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">man</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">as</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">tonou</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_71" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">Menan</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">balaganla</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">jezattə</ts>
                  <nts id="Seg_80" n="HIAT:ip">.</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_83" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">Pakoskən</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">jekuzattə</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">palaganla</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">i</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">qwelɨtʼtʼe</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_100" n="HIAT:w" s="T27">mɨɣɨn</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_103" n="HIAT:w" s="T28">jekuzattə</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_106" n="HIAT:w" s="T29">palaganla</ts>
                  <nts id="Seg_107" n="HIAT:ip">.</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_110" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_112" n="HIAT:w" s="T30">Čʼumqan</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_115" n="HIAT:w" s="T31">man</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_118" n="HIAT:w" s="T32">aːs</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_121" n="HIAT:w" s="T33">warkukuzan</ts>
                  <nts id="Seg_122" n="HIAT:ip">.</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_125" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_127" n="HIAT:w" s="T34">Taŋɨːn</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_130" n="HIAT:w" s="T35">čʼumlaɣɨn</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_133" n="HIAT:w" s="T36">warkattə</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_136" n="HIAT:w" s="T37">süsögula</ts>
                  <nts id="Seg_137" n="HIAT:ip">.</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T38" id="Seg_139" n="sc" s="T1">
               <ts e="T2" id="Seg_141" n="e" s="T1">Na </ts>
               <ts e="T3" id="Seg_143" n="e" s="T2">čorta </ts>
               <ts e="T4" id="Seg_145" n="e" s="T3">man </ts>
               <ts e="T5" id="Seg_147" n="e" s="T4">tɨldʼzʼen </ts>
               <ts e="T6" id="Seg_149" n="e" s="T5">ilʼeǯan. </ts>
               <ts e="T7" id="Seg_151" n="e" s="T6">Qajno </ts>
               <ts e="T8" id="Seg_153" n="e" s="T7">nildʼzʼin </ts>
               <ts e="T9" id="Seg_155" n="e" s="T8">elattə </ts>
               <ts e="T10" id="Seg_157" n="e" s="T9">qaːzɨrbɨlʼe. </ts>
               <ts e="T11" id="Seg_159" n="e" s="T10">Nawerno </ts>
               <ts e="T12" id="Seg_161" n="e" s="T11">uǯirbaːtt. </ts>
               <ts e="T13" id="Seg_163" n="e" s="T12">Čamgɨn </ts>
               <ts e="T14" id="Seg_165" n="e" s="T13">uǯila </ts>
               <ts e="T15" id="Seg_167" n="e" s="T14">jekudattə. </ts>
               <ts e="T16" id="Seg_169" n="e" s="T15">Čʼum </ts>
               <ts e="T17" id="Seg_171" n="e" s="T16">man </ts>
               <ts e="T18" id="Seg_173" n="e" s="T17">as </ts>
               <ts e="T19" id="Seg_175" n="e" s="T18">tonou. </ts>
               <ts e="T20" id="Seg_177" n="e" s="T19">Menan </ts>
               <ts e="T21" id="Seg_179" n="e" s="T20">balaganla </ts>
               <ts e="T22" id="Seg_181" n="e" s="T21">jezattə. </ts>
               <ts e="T23" id="Seg_183" n="e" s="T22">Pakoskən </ts>
               <ts e="T24" id="Seg_185" n="e" s="T23">jekuzattə </ts>
               <ts e="T25" id="Seg_187" n="e" s="T24">palaganla </ts>
               <ts e="T26" id="Seg_189" n="e" s="T25">i </ts>
               <ts e="T27" id="Seg_191" n="e" s="T26">qwelɨtʼtʼe </ts>
               <ts e="T28" id="Seg_193" n="e" s="T27">mɨɣɨn </ts>
               <ts e="T29" id="Seg_195" n="e" s="T28">jekuzattə </ts>
               <ts e="T30" id="Seg_197" n="e" s="T29">palaganla. </ts>
               <ts e="T31" id="Seg_199" n="e" s="T30">Čʼumqan </ts>
               <ts e="T32" id="Seg_201" n="e" s="T31">man </ts>
               <ts e="T33" id="Seg_203" n="e" s="T32">aːs </ts>
               <ts e="T34" id="Seg_205" n="e" s="T33">warkukuzan. </ts>
               <ts e="T35" id="Seg_207" n="e" s="T34">Taŋɨːn </ts>
               <ts e="T36" id="Seg_209" n="e" s="T35">čʼumlaɣɨn </ts>
               <ts e="T37" id="Seg_211" n="e" s="T36">warkattə </ts>
               <ts e="T38" id="Seg_213" n="e" s="T37">süsögula. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_214" s="T1">PVD_1964_LifeInTents_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_215" s="T6">PVD_1964_LifeInTents_nar.002 (001.002)</ta>
            <ta e="T12" id="Seg_216" s="T10">PVD_1964_LifeInTents_nar.003 (001.003)</ta>
            <ta e="T15" id="Seg_217" s="T12">PVD_1964_LifeInTents_nar.004 (001.004)</ta>
            <ta e="T19" id="Seg_218" s="T15">PVD_1964_LifeInTents_nar.005 (001.005)</ta>
            <ta e="T22" id="Seg_219" s="T19">PVD_1964_LifeInTents_nar.006 (001.006)</ta>
            <ta e="T30" id="Seg_220" s="T22">PVD_1964_LifeInTents_nar.007 (001.007)</ta>
            <ta e="T34" id="Seg_221" s="T30">PVD_1964_LifeInTents_nar.008 (001.008)</ta>
            <ta e="T38" id="Seg_222" s="T34">PVD_1964_LifeInTents_nar.009 (001.009)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_223" s="T1">на чор′та ман ′тыlдʼзʼен илʼеджан.</ta>
            <ta e="T10" id="Seg_224" s="T6">′kайно ниlдʼзʼин е′латтъ̹ ′kа̄зырбылʼе.</ta>
            <ta e="T12" id="Seg_225" s="T10">наверно уджир′ба̄тт.</ta>
            <ta e="T15" id="Seg_226" s="T12">тшам′гын уджи′ла ′jекудаттъ.</ta>
            <ta e="T19" id="Seg_227" s="T15">чʼум ман ′ас то′ноу.</ta>
            <ta e="T22" id="Seg_228" s="T19">ме′нан б̂ала′ганла ′jезаттъ.</ta>
            <ta e="T30" id="Seg_229" s="T22">па′коскън ′jекузаттъ палаганла и kwе̨лытʼтʼе мы′ɣын ′jекузаттъ пала′ганла.</ta>
            <ta e="T34" id="Seg_230" s="T30">чʼумkан ман а̄с варкукузан.</ta>
            <ta e="T38" id="Seg_231" s="T34">та′ңы̄н ′чʼумлаɣын вар′каттъ сӱ′сӧгула.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_232" s="T1">na čorta man tɨldʼzʼen ilʼeǯan.</ta>
            <ta e="T10" id="Seg_233" s="T6">qajno nildʼzʼin elattə̹ qaːzɨrbɨlʼe.</ta>
            <ta e="T12" id="Seg_234" s="T10">nawerno uǯirbaːtt.</ta>
            <ta e="T15" id="Seg_235" s="T12">tšamgɨn uǯila jekudattə.</ta>
            <ta e="T19" id="Seg_236" s="T15">čʼum man as tonou.</ta>
            <ta e="T22" id="Seg_237" s="T19">menan b̂alaganla jezattə.</ta>
            <ta e="T30" id="Seg_238" s="T22">pakoskən jekuzattə palaganla i qwelɨtʼtʼe mɨɣɨn jekuzattə palaganla.</ta>
            <ta e="T34" id="Seg_239" s="T30">čʼumqan man aːs warkukuzan.</ta>
            <ta e="T38" id="Seg_240" s="T34">taŋɨːn čʼumlaɣɨn warkattə süsögula.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_241" s="T1">Na čorta man tɨldʼzʼen ilʼeǯan. </ta>
            <ta e="T10" id="Seg_242" s="T6">Qajno nildʼzʼin elattə qaːzɨrbɨlʼe. </ta>
            <ta e="T12" id="Seg_243" s="T10">Nawerno uǯirbaːtt. </ta>
            <ta e="T15" id="Seg_244" s="T12">Čamgɨn uǯila jekudattə. </ta>
            <ta e="T19" id="Seg_245" s="T15">Čʼum man as tonou. </ta>
            <ta e="T22" id="Seg_246" s="T19">Menan balaganla jezattə. </ta>
            <ta e="T30" id="Seg_247" s="T22">Pakoskən jekuzattə palaganla i qwelɨtʼtʼe mɨɣɨn jekuzattə palaganla. </ta>
            <ta e="T34" id="Seg_248" s="T30">Čʼumqan man aːs warkukuzan. </ta>
            <ta e="T38" id="Seg_249" s="T34">Taŋɨːn čʼumlaɣɨn warkattə süsögula. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T4" id="Seg_250" s="T3">man</ta>
            <ta e="T5" id="Seg_251" s="T4">tɨldʼzʼe-n</ta>
            <ta e="T6" id="Seg_252" s="T5">ilʼ-eǯa-n</ta>
            <ta e="T7" id="Seg_253" s="T6">qaj-no</ta>
            <ta e="T8" id="Seg_254" s="T7">nildʼzʼi-n</ta>
            <ta e="T9" id="Seg_255" s="T8">ela-ttə</ta>
            <ta e="T10" id="Seg_256" s="T9">qaːzɨr-bɨ-lʼe</ta>
            <ta e="T11" id="Seg_257" s="T10">nawerno</ta>
            <ta e="T12" id="Seg_258" s="T11">uǯi-r-baː-tt</ta>
            <ta e="T13" id="Seg_259" s="T12">čam-gɨn</ta>
            <ta e="T14" id="Seg_260" s="T13">uǯi-la</ta>
            <ta e="T15" id="Seg_261" s="T14">je-ku-da-ttə</ta>
            <ta e="T16" id="Seg_262" s="T15">čʼum</ta>
            <ta e="T17" id="Seg_263" s="T16">man</ta>
            <ta e="T18" id="Seg_264" s="T17">as</ta>
            <ta e="T19" id="Seg_265" s="T18">tono-u</ta>
            <ta e="T20" id="Seg_266" s="T19">me-nan</ta>
            <ta e="T21" id="Seg_267" s="T20">balagan-la</ta>
            <ta e="T22" id="Seg_268" s="T21">je-za-ttə</ta>
            <ta e="T23" id="Seg_269" s="T22">pakos-kən</ta>
            <ta e="T24" id="Seg_270" s="T23">je-ku-za-ttə</ta>
            <ta e="T25" id="Seg_271" s="T24">palagan-la</ta>
            <ta e="T26" id="Seg_272" s="T25">i</ta>
            <ta e="T27" id="Seg_273" s="T26">qwel-ɨ-tʼtʼe</ta>
            <ta e="T28" id="Seg_274" s="T27">mɨ-ɣɨn</ta>
            <ta e="T29" id="Seg_275" s="T28">je-ku-za-ttə</ta>
            <ta e="T30" id="Seg_276" s="T29">palagan-la</ta>
            <ta e="T31" id="Seg_277" s="T30">čʼum-qan</ta>
            <ta e="T32" id="Seg_278" s="T31">man</ta>
            <ta e="T33" id="Seg_279" s="T32">aːs</ta>
            <ta e="T34" id="Seg_280" s="T33">warku-ku-za-n</ta>
            <ta e="T35" id="Seg_281" s="T34">taŋɨː-n</ta>
            <ta e="T36" id="Seg_282" s="T35">čʼum-la-ɣɨn</ta>
            <ta e="T37" id="Seg_283" s="T36">warka-ttə</ta>
            <ta e="T38" id="Seg_284" s="T37">süsögu-la</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T4" id="Seg_285" s="T3">man</ta>
            <ta e="T5" id="Seg_286" s="T4">tilʼdʼi-ŋ</ta>
            <ta e="T6" id="Seg_287" s="T5">elɨ-enǯɨ-ŋ</ta>
            <ta e="T7" id="Seg_288" s="T6">qaj-no</ta>
            <ta e="T8" id="Seg_289" s="T7">nʼilʼdʼi-ŋ</ta>
            <ta e="T9" id="Seg_290" s="T8">elɨ-tɨn</ta>
            <ta e="T10" id="Seg_291" s="T9">kazɨr-mbɨ-le</ta>
            <ta e="T11" id="Seg_292" s="T10">nawerna</ta>
            <ta e="T12" id="Seg_293" s="T11">uǯi-r-mbɨ-tɨn</ta>
            <ta e="T13" id="Seg_294" s="T12">čam-qɨn</ta>
            <ta e="T14" id="Seg_295" s="T13">uǯi-la</ta>
            <ta e="T15" id="Seg_296" s="T14">eː-ku-ntɨ-tɨn</ta>
            <ta e="T16" id="Seg_297" s="T15">čʼum</ta>
            <ta e="T17" id="Seg_298" s="T16">man</ta>
            <ta e="T18" id="Seg_299" s="T17">asa</ta>
            <ta e="T19" id="Seg_300" s="T18">tonu-w</ta>
            <ta e="T20" id="Seg_301" s="T19">me-nan</ta>
            <ta e="T21" id="Seg_302" s="T20">palagan-la</ta>
            <ta e="T22" id="Seg_303" s="T21">eː-sɨ-tɨn</ta>
            <ta e="T23" id="Seg_304" s="T22">pakos-qɨn</ta>
            <ta e="T24" id="Seg_305" s="T23">eː-ku-sɨ-tɨn</ta>
            <ta e="T25" id="Seg_306" s="T24">palagan-la</ta>
            <ta e="T26" id="Seg_307" s="T25">i</ta>
            <ta e="T27" id="Seg_308" s="T26">qwäːlɨj-ɨ-ttə</ta>
            <ta e="T28" id="Seg_309" s="T27">mɨ-qɨn</ta>
            <ta e="T29" id="Seg_310" s="T28">eː-ku-sɨ-tɨn</ta>
            <ta e="T30" id="Seg_311" s="T29">palagan-la</ta>
            <ta e="T31" id="Seg_312" s="T30">čʼum-qɨn</ta>
            <ta e="T32" id="Seg_313" s="T31">man</ta>
            <ta e="T33" id="Seg_314" s="T32">asa</ta>
            <ta e="T34" id="Seg_315" s="T33">warkɨ-ku-sɨ-ŋ</ta>
            <ta e="T35" id="Seg_316" s="T34">taŋi-ŋ</ta>
            <ta e="T36" id="Seg_317" s="T35">čʼum-la-qɨn</ta>
            <ta e="T37" id="Seg_318" s="T36">warkɨ-tɨn</ta>
            <ta e="T38" id="Seg_319" s="T37">süsögum-la</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T4" id="Seg_320" s="T3">I.NOM</ta>
            <ta e="T5" id="Seg_321" s="T4">such-ADVZ</ta>
            <ta e="T6" id="Seg_322" s="T5">live-FUT-1SG.S</ta>
            <ta e="T7" id="Seg_323" s="T6">what-TRL</ta>
            <ta e="T8" id="Seg_324" s="T7">so-ADVZ</ta>
            <ta e="T9" id="Seg_325" s="T8">live-3PL</ta>
            <ta e="T10" id="Seg_326" s="T9">get.dirty-DUR-CVB</ta>
            <ta e="T11" id="Seg_327" s="T10">probably</ta>
            <ta e="T12" id="Seg_328" s="T11">louse-VBLZ-DUR-3PL</ta>
            <ta e="T13" id="Seg_329" s="T12">dirt-LOC</ta>
            <ta e="T14" id="Seg_330" s="T13">louse-PL.[NOM]</ta>
            <ta e="T15" id="Seg_331" s="T14">be-HAB-INFER-3PL</ta>
            <ta e="T16" id="Seg_332" s="T15">tent.[NOM]</ta>
            <ta e="T17" id="Seg_333" s="T16">I.NOM</ta>
            <ta e="T18" id="Seg_334" s="T17">NEG</ta>
            <ta e="T19" id="Seg_335" s="T18">know-1SG.O</ta>
            <ta e="T20" id="Seg_336" s="T19">we-ADES</ta>
            <ta e="T21" id="Seg_337" s="T20">tent-PL.[NOM]</ta>
            <ta e="T22" id="Seg_338" s="T21">be-PST-3PL</ta>
            <ta e="T23" id="Seg_339" s="T22">haymaking-LOC</ta>
            <ta e="T24" id="Seg_340" s="T23">be-HAB-PST-3PL</ta>
            <ta e="T25" id="Seg_341" s="T24">tent-PL.[NOM]</ta>
            <ta e="T26" id="Seg_342" s="T25">and</ta>
            <ta e="T27" id="Seg_343" s="T26">fish-EP-ACTN.[NOM]</ta>
            <ta e="T28" id="Seg_344" s="T27">something-LOC</ta>
            <ta e="T29" id="Seg_345" s="T28">be-HAB-PST-3PL</ta>
            <ta e="T30" id="Seg_346" s="T29">tent-PL.[NOM]</ta>
            <ta e="T31" id="Seg_347" s="T30">tent-LOC</ta>
            <ta e="T32" id="Seg_348" s="T31">I.NOM</ta>
            <ta e="T33" id="Seg_349" s="T32">NEG</ta>
            <ta e="T34" id="Seg_350" s="T33">live-HAB-PST-1SG.S</ta>
            <ta e="T35" id="Seg_351" s="T34">north-ADVZ</ta>
            <ta e="T36" id="Seg_352" s="T35">tent-PL-LOC</ta>
            <ta e="T37" id="Seg_353" s="T36">live-3PL</ta>
            <ta e="T38" id="Seg_354" s="T37">Selkup-PL.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T4" id="Seg_355" s="T3">я.NOM</ta>
            <ta e="T5" id="Seg_356" s="T4">такой-ADVZ</ta>
            <ta e="T6" id="Seg_357" s="T5">жить-FUT-1SG.S</ta>
            <ta e="T7" id="Seg_358" s="T6">что-TRL</ta>
            <ta e="T8" id="Seg_359" s="T7">так-ADVZ</ta>
            <ta e="T9" id="Seg_360" s="T8">жить-3PL</ta>
            <ta e="T10" id="Seg_361" s="T9">испачкаться-DUR-CVB</ta>
            <ta e="T11" id="Seg_362" s="T10">наверное</ta>
            <ta e="T12" id="Seg_363" s="T11">вошь-VBLZ-DUR-3PL</ta>
            <ta e="T13" id="Seg_364" s="T12">грязь-LOC</ta>
            <ta e="T14" id="Seg_365" s="T13">вошь-PL.[NOM]</ta>
            <ta e="T15" id="Seg_366" s="T14">быть-HAB-INFER-3PL</ta>
            <ta e="T16" id="Seg_367" s="T15">чум.[NOM]</ta>
            <ta e="T17" id="Seg_368" s="T16">я.NOM</ta>
            <ta e="T18" id="Seg_369" s="T17">NEG</ta>
            <ta e="T19" id="Seg_370" s="T18">знать-1SG.O</ta>
            <ta e="T20" id="Seg_371" s="T19">мы-ADES</ta>
            <ta e="T21" id="Seg_372" s="T20">балаган-PL.[NOM]</ta>
            <ta e="T22" id="Seg_373" s="T21">быть-PST-3PL</ta>
            <ta e="T23" id="Seg_374" s="T22">покос-LOC</ta>
            <ta e="T24" id="Seg_375" s="T23">быть-HAB-PST-3PL</ta>
            <ta e="T25" id="Seg_376" s="T24">балаган-PL.[NOM]</ta>
            <ta e="T26" id="Seg_377" s="T25">и</ta>
            <ta e="T27" id="Seg_378" s="T26">рыбачить-EP-ACTN.[NOM]</ta>
            <ta e="T28" id="Seg_379" s="T27">нечто-LOC</ta>
            <ta e="T29" id="Seg_380" s="T28">быть-HAB-PST-3PL</ta>
            <ta e="T30" id="Seg_381" s="T29">балаган-PL.[NOM]</ta>
            <ta e="T31" id="Seg_382" s="T30">чум-LOC</ta>
            <ta e="T32" id="Seg_383" s="T31">я.NOM</ta>
            <ta e="T33" id="Seg_384" s="T32">NEG</ta>
            <ta e="T34" id="Seg_385" s="T33">жить-HAB-PST-1SG.S</ta>
            <ta e="T35" id="Seg_386" s="T34">северный-ADVZ</ta>
            <ta e="T36" id="Seg_387" s="T35">чум-PL-LOC</ta>
            <ta e="T37" id="Seg_388" s="T36">жить-3PL</ta>
            <ta e="T38" id="Seg_389" s="T37">селькуп-PL.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T4" id="Seg_390" s="T3">pers</ta>
            <ta e="T5" id="Seg_391" s="T4">adj-adj&gt;adv</ta>
            <ta e="T6" id="Seg_392" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_393" s="T6">interrog-n:case</ta>
            <ta e="T8" id="Seg_394" s="T7">adv-adj&gt;adv</ta>
            <ta e="T9" id="Seg_395" s="T8">v-v:pn</ta>
            <ta e="T10" id="Seg_396" s="T9">v-v&gt;v-v&gt;adv</ta>
            <ta e="T11" id="Seg_397" s="T10">adv</ta>
            <ta e="T12" id="Seg_398" s="T11">n-n&gt;v-v&gt;v-v:pn</ta>
            <ta e="T13" id="Seg_399" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_400" s="T13">n-n:num.[n:case]</ta>
            <ta e="T15" id="Seg_401" s="T14">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T16" id="Seg_402" s="T15">n.[n:case]</ta>
            <ta e="T17" id="Seg_403" s="T16">pers</ta>
            <ta e="T18" id="Seg_404" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_405" s="T18">v-v:pn</ta>
            <ta e="T20" id="Seg_406" s="T19">pers-n:case</ta>
            <ta e="T21" id="Seg_407" s="T20">n-n:num.[n:case]</ta>
            <ta e="T22" id="Seg_408" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_409" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_410" s="T23">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_411" s="T24">n-n:num.[n:case]</ta>
            <ta e="T26" id="Seg_412" s="T25">conj</ta>
            <ta e="T27" id="Seg_413" s="T26">v-v:ins-v&gt;n.[n:case]</ta>
            <ta e="T28" id="Seg_414" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_415" s="T28">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_416" s="T29">n-n:num.[n:case]</ta>
            <ta e="T31" id="Seg_417" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_418" s="T31">pers</ta>
            <ta e="T33" id="Seg_419" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_420" s="T33">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_421" s="T34">adj-adj&gt;adv</ta>
            <ta e="T36" id="Seg_422" s="T35">n-n:num-n:case</ta>
            <ta e="T37" id="Seg_423" s="T36">v-v:pn</ta>
            <ta e="T38" id="Seg_424" s="T37">n-n:num.[n:case]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T4" id="Seg_425" s="T3">pers</ta>
            <ta e="T5" id="Seg_426" s="T4">adj</ta>
            <ta e="T6" id="Seg_427" s="T5">v</ta>
            <ta e="T7" id="Seg_428" s="T6">interrog</ta>
            <ta e="T8" id="Seg_429" s="T7">adv</ta>
            <ta e="T9" id="Seg_430" s="T8">v</ta>
            <ta e="T10" id="Seg_431" s="T9">adv</ta>
            <ta e="T11" id="Seg_432" s="T10">adv</ta>
            <ta e="T12" id="Seg_433" s="T11">v</ta>
            <ta e="T13" id="Seg_434" s="T12">n</ta>
            <ta e="T14" id="Seg_435" s="T13">n</ta>
            <ta e="T15" id="Seg_436" s="T14">v</ta>
            <ta e="T16" id="Seg_437" s="T15">n</ta>
            <ta e="T17" id="Seg_438" s="T16">pers</ta>
            <ta e="T18" id="Seg_439" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_440" s="T18">v</ta>
            <ta e="T20" id="Seg_441" s="T19">pers</ta>
            <ta e="T21" id="Seg_442" s="T20">n</ta>
            <ta e="T22" id="Seg_443" s="T21">v</ta>
            <ta e="T23" id="Seg_444" s="T22">n</ta>
            <ta e="T24" id="Seg_445" s="T23">v</ta>
            <ta e="T25" id="Seg_446" s="T24">n</ta>
            <ta e="T26" id="Seg_447" s="T25">conj</ta>
            <ta e="T27" id="Seg_448" s="T26">n</ta>
            <ta e="T28" id="Seg_449" s="T27">n</ta>
            <ta e="T29" id="Seg_450" s="T28">v</ta>
            <ta e="T30" id="Seg_451" s="T29">n</ta>
            <ta e="T31" id="Seg_452" s="T30">n</ta>
            <ta e="T32" id="Seg_453" s="T31">pers</ta>
            <ta e="T33" id="Seg_454" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_455" s="T33">v</ta>
            <ta e="T35" id="Seg_456" s="T34">adv</ta>
            <ta e="T36" id="Seg_457" s="T35">n</ta>
            <ta e="T37" id="Seg_458" s="T36">v</ta>
            <ta e="T38" id="Seg_459" s="T37">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T4" id="Seg_460" s="T3">pro.h:Th</ta>
            <ta e="T9" id="Seg_461" s="T8">0.3.h:Th</ta>
            <ta e="T12" id="Seg_462" s="T11">0.3.h:Th</ta>
            <ta e="T13" id="Seg_463" s="T12">np:L</ta>
            <ta e="T14" id="Seg_464" s="T13">np:Th</ta>
            <ta e="T16" id="Seg_465" s="T15">np:Th</ta>
            <ta e="T17" id="Seg_466" s="T16">pro.h:E</ta>
            <ta e="T20" id="Seg_467" s="T19">pro.h:Poss</ta>
            <ta e="T21" id="Seg_468" s="T20">np:Th</ta>
            <ta e="T23" id="Seg_469" s="T22">np:Time</ta>
            <ta e="T25" id="Seg_470" s="T24">np:Th</ta>
            <ta e="T28" id="Seg_471" s="T27">np:Time</ta>
            <ta e="T30" id="Seg_472" s="T29">np:Th</ta>
            <ta e="T31" id="Seg_473" s="T30">np:L</ta>
            <ta e="T32" id="Seg_474" s="T31">pro.h:Th</ta>
            <ta e="T36" id="Seg_475" s="T35">np:L</ta>
            <ta e="T38" id="Seg_476" s="T37">np.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_477" s="T3">pro.h:S</ta>
            <ta e="T6" id="Seg_478" s="T5">v:pred</ta>
            <ta e="T9" id="Seg_479" s="T8">0.3.h:S v:pred</ta>
            <ta e="T10" id="Seg_480" s="T9">s:adv</ta>
            <ta e="T12" id="Seg_481" s="T11">0.3.h:S v:pred</ta>
            <ta e="T14" id="Seg_482" s="T13">np:S</ta>
            <ta e="T15" id="Seg_483" s="T14">v:pred</ta>
            <ta e="T16" id="Seg_484" s="T15">np:O</ta>
            <ta e="T17" id="Seg_485" s="T16">pro.h:S</ta>
            <ta e="T19" id="Seg_486" s="T18">v:pred</ta>
            <ta e="T21" id="Seg_487" s="T20">np:S</ta>
            <ta e="T22" id="Seg_488" s="T21">v:pred</ta>
            <ta e="T24" id="Seg_489" s="T23">v:pred</ta>
            <ta e="T25" id="Seg_490" s="T24">np:S</ta>
            <ta e="T29" id="Seg_491" s="T28">v:pred</ta>
            <ta e="T30" id="Seg_492" s="T29">np:S</ta>
            <ta e="T32" id="Seg_493" s="T31">pro.h:S</ta>
            <ta e="T34" id="Seg_494" s="T33">v:pred</ta>
            <ta e="T37" id="Seg_495" s="T36">v:pred</ta>
            <ta e="T38" id="Seg_496" s="T37">np.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T11" id="Seg_497" s="T10">RUS:mod</ta>
            <ta e="T16" id="Seg_498" s="T15">RUS:core</ta>
            <ta e="T21" id="Seg_499" s="T20">RUS:core</ta>
            <ta e="T23" id="Seg_500" s="T22">RUS:cult</ta>
            <ta e="T25" id="Seg_501" s="T24">RUS:core</ta>
            <ta e="T26" id="Seg_502" s="T25">RUS:gram</ta>
            <ta e="T30" id="Seg_503" s="T29">RUS:core</ta>
            <ta e="T31" id="Seg_504" s="T30">RUS:core</ta>
            <ta e="T36" id="Seg_505" s="T35">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_506" s="T1">What the hell, why should I live like this.</ta>
            <ta e="T10" id="Seg_507" s="T6">Why do they live so in the dirt.</ta>
            <ta e="T12" id="Seg_508" s="T10">Probably, they have lice.</ta>
            <ta e="T15" id="Seg_509" s="T12">There are always lice in the dirt.</ta>
            <ta e="T19" id="Seg_510" s="T15">I don't know (reindeer skin) tents.</ta>
            <ta e="T22" id="Seg_511" s="T19">We used to have wooden(?) tents.</ta>
            <ta e="T30" id="Seg_512" s="T22">In the time of haymaking we had wooden(?) tents and in the time of fishing we had wooden(?) tents.</ta>
            <ta e="T34" id="Seg_513" s="T30">I never used to live in a skin tent.</ta>
            <ta e="T38" id="Seg_514" s="T34">Selkups to the North live in skin tents.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_515" s="T1">Was zur Hölle, warum sollte ich so leben.</ta>
            <ta e="T10" id="Seg_516" s="T6">Warum leben sie so im Dreck.</ta>
            <ta e="T12" id="Seg_517" s="T10">Wahrscheinlich haben sie Läuse.</ta>
            <ta e="T15" id="Seg_518" s="T12">Im Dreck sind immer Läuse.</ta>
            <ta e="T19" id="Seg_519" s="T15">Ich kenne keine (Rentierhaut)zelte.</ta>
            <ta e="T22" id="Seg_520" s="T19">Wir hatten Zelte aus Holz(?).</ta>
            <ta e="T30" id="Seg_521" s="T22">Während der Heuernte hatten wir Holz(?)zelte und beim Fischen hatten wir Holz(?)zelte.</ta>
            <ta e="T34" id="Seg_522" s="T30">In einem Lederzelt habe ich nie gelebt.</ta>
            <ta e="T38" id="Seg_523" s="T34">Selkupen im Norden leben in Lederzelten.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_524" s="T1">На черта я так жить буду.</ta>
            <ta e="T10" id="Seg_525" s="T6">Зачем они так живут в грязи.</ta>
            <ta e="T12" id="Seg_526" s="T10">Наверное, вши у них.</ta>
            <ta e="T15" id="Seg_527" s="T12">В грязи всегда вши бывают.</ta>
            <ta e="T19" id="Seg_528" s="T15">Чума я не знаю.</ta>
            <ta e="T22" id="Seg_529" s="T19">У нас были балаганы.</ta>
            <ta e="T30" id="Seg_530" s="T22">На покосе были балаганы и на рыбалке бывали балаганы.</ta>
            <ta e="T34" id="Seg_531" s="T30">В чуме я никогда не жила.</ta>
            <ta e="T38" id="Seg_532" s="T34">Внизу в чумах живут селькупы.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_533" s="T1">на черта я так жить буду</ta>
            <ta e="T10" id="Seg_534" s="T6">зачем они так живут в грязи</ta>
            <ta e="T12" id="Seg_535" s="T10">наверно вши там</ta>
            <ta e="T15" id="Seg_536" s="T12">в грязи всегда вши бывают</ta>
            <ta e="T19" id="Seg_537" s="T15">чум я не знаю</ta>
            <ta e="T22" id="Seg_538" s="T19">у нас были балаганы</ta>
            <ta e="T30" id="Seg_539" s="T22">на покосе были балаганы и на рыбалке бывали балаганы</ta>
            <ta e="T34" id="Seg_540" s="T30">в чуме я никогда не жила</ta>
            <ta e="T38" id="Seg_541" s="T34">внизу в чумах живут остяки</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
