<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_LittleMice_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_LittleMice_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">89</ud-information>
            <ud-information attribute-name="# HIAT:w">70</ud-information>
            <ud-information attribute-name="# e">70</ud-information>
            <ud-information attribute-name="# HIAT:u">15</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T71" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Manan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">qɨbanʼaʒau</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">kuza</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Me</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">tebɨm</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">xaranisaftə</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">Tebɨn</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">pilʼonkalam</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">taqkəlʼeː</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">pʼötʼe</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">paːrod</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">pessau</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_47" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">Tebla</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">natʼen</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">ipɨzattə</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_59" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">Man</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">pʼötʼe</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">parod</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">sɨɣəsan</ts>
                  <nts id="Seg_71" n="HIAT:ip">,</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">teblam</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_77" n="HIAT:w" s="T21">wes</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_80" n="HIAT:w" s="T22">kuču</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_83" n="HIAT:w" s="T23">čuːlǯau</ts>
                  <nts id="Seg_84" n="HIAT:ip">.</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_87" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">Saltšiboddə</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">ilʼe</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_95" n="HIAT:w" s="T26">jübərau</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_99" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">A</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">tawa</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">qobbat</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">natʼen</ts>
                  <nts id="Seg_111" n="HIAT:ip">.</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_114" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_116" n="HIAT:w" s="T31">I</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">tebla</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">megga</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">mugutqen</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">wes</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_131" n="HIAT:w" s="T36">pɨggelat</ts>
                  <nts id="Seg_132" n="HIAT:ip">.</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_135" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_137" n="HIAT:w" s="T37">Man</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_140" n="HIAT:w" s="T38">dawaj</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_143" n="HIAT:w" s="T39">qaːrɨlʼeː</ts>
                  <nts id="Seg_144" n="HIAT:ip">,</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_147" n="HIAT:w" s="T40">qaːrɨlʼeː</ts>
                  <nts id="Seg_148" n="HIAT:ip">.</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_151" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_153" n="HIAT:w" s="T41">Platjo</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_156" n="HIAT:w" s="T42">wes</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_159" n="HIAT:w" s="T43">niʒarnau</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_163" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_165" n="HIAT:w" s="T44">Nʼalaj</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_168" n="HIAT:w" s="T45">qobɨn</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_171" n="HIAT:w" s="T46">kurkan</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_174" n="HIAT:w" s="T47">madɨn</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_177" n="HIAT:w" s="T48">sʼutʼdʼäɣən</ts>
                  <nts id="Seg_178" n="HIAT:ip">.</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_181" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_183" n="HIAT:w" s="T49">A</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_186" n="HIAT:w" s="T50">man</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_189" n="HIAT:w" s="T51">maǯʼezʼidan</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_192" n="HIAT:w" s="T52">qɨːba</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_195" n="HIAT:w" s="T53">tawakala</ts>
                  <nts id="Seg_196" n="HIAT:ip">.</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_199" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_201" n="HIAT:w" s="T54">Mʼaguntɨ</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_204" n="HIAT:w" s="T55">kɨdan</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_207" n="HIAT:w" s="T56">kula</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_210" n="HIAT:w" s="T57">serkott</ts>
                  <nts id="Seg_211" n="HIAT:ip">.</nts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_214" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_216" n="HIAT:w" s="T58">Kusaqen</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_219" n="HIAT:w" s="T59">man</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_222" n="HIAT:w" s="T60">nʼalaj</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_225" n="HIAT:w" s="T61">qobɨn</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_228" n="HIAT:w" s="T62">jezan</ts>
                  <nts id="Seg_229" n="HIAT:ip">,</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_232" n="HIAT:w" s="T63">kak</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_235" n="HIAT:w" s="T64">bɨ</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_238" n="HIAT:w" s="T65">nɨtdɨn</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_241" n="HIAT:w" s="T66">sʼernʼatt</ts>
                  <nts id="Seg_242" n="HIAT:ip">.</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_245" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_247" n="HIAT:w" s="T67">Ot</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_250" n="HIAT:w" s="T68">man</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_253" n="HIAT:w" s="T69">qarɨsan</ts>
                  <nts id="Seg_254" n="HIAT:ip">,</nts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_257" n="HIAT:w" s="T70">qarɨsan</ts>
                  <nts id="Seg_258" n="HIAT:ip">.</nts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T71" id="Seg_260" n="sc" s="T1">
               <ts e="T2" id="Seg_262" n="e" s="T1">Manan </ts>
               <ts e="T3" id="Seg_264" n="e" s="T2">qɨbanʼaʒau </ts>
               <ts e="T4" id="Seg_266" n="e" s="T3">kuza. </ts>
               <ts e="T5" id="Seg_268" n="e" s="T4">Me </ts>
               <ts e="T6" id="Seg_270" n="e" s="T5">tebɨm </ts>
               <ts e="T7" id="Seg_272" n="e" s="T6">xaranisaftə. </ts>
               <ts e="T8" id="Seg_274" n="e" s="T7">Tebɨn </ts>
               <ts e="T9" id="Seg_276" n="e" s="T8">pilʼonkalam </ts>
               <ts e="T10" id="Seg_278" n="e" s="T9">taqkəlʼeː </ts>
               <ts e="T11" id="Seg_280" n="e" s="T10">pʼötʼe </ts>
               <ts e="T12" id="Seg_282" n="e" s="T11">paːrod </ts>
               <ts e="T13" id="Seg_284" n="e" s="T12">pessau. </ts>
               <ts e="T14" id="Seg_286" n="e" s="T13">Tebla </ts>
               <ts e="T15" id="Seg_288" n="e" s="T14">natʼen </ts>
               <ts e="T16" id="Seg_290" n="e" s="T15">ipɨzattə. </ts>
               <ts e="T17" id="Seg_292" n="e" s="T16">Man </ts>
               <ts e="T18" id="Seg_294" n="e" s="T17">pʼötʼe </ts>
               <ts e="T19" id="Seg_296" n="e" s="T18">parod </ts>
               <ts e="T20" id="Seg_298" n="e" s="T19">sɨɣəsan, </ts>
               <ts e="T21" id="Seg_300" n="e" s="T20">teblam </ts>
               <ts e="T22" id="Seg_302" n="e" s="T21">wes </ts>
               <ts e="T23" id="Seg_304" n="e" s="T22">kuču </ts>
               <ts e="T24" id="Seg_306" n="e" s="T23">čuːlǯau. </ts>
               <ts e="T25" id="Seg_308" n="e" s="T24">Saltšiboddə </ts>
               <ts e="T26" id="Seg_310" n="e" s="T25">ilʼe </ts>
               <ts e="T27" id="Seg_312" n="e" s="T26">jübərau. </ts>
               <ts e="T28" id="Seg_314" n="e" s="T27">A </ts>
               <ts e="T29" id="Seg_316" n="e" s="T28">tawa </ts>
               <ts e="T30" id="Seg_318" n="e" s="T29">qobbat </ts>
               <ts e="T31" id="Seg_320" n="e" s="T30">natʼen. </ts>
               <ts e="T32" id="Seg_322" n="e" s="T31">I </ts>
               <ts e="T33" id="Seg_324" n="e" s="T32">tebla </ts>
               <ts e="T34" id="Seg_326" n="e" s="T33">megga </ts>
               <ts e="T35" id="Seg_328" n="e" s="T34">mugutqen </ts>
               <ts e="T36" id="Seg_330" n="e" s="T35">wes </ts>
               <ts e="T37" id="Seg_332" n="e" s="T36">pɨggelat. </ts>
               <ts e="T38" id="Seg_334" n="e" s="T37">Man </ts>
               <ts e="T39" id="Seg_336" n="e" s="T38">dawaj </ts>
               <ts e="T40" id="Seg_338" n="e" s="T39">qaːrɨlʼeː, </ts>
               <ts e="T41" id="Seg_340" n="e" s="T40">qaːrɨlʼeː. </ts>
               <ts e="T42" id="Seg_342" n="e" s="T41">Platjo </ts>
               <ts e="T43" id="Seg_344" n="e" s="T42">wes </ts>
               <ts e="T44" id="Seg_346" n="e" s="T43">niʒarnau. </ts>
               <ts e="T45" id="Seg_348" n="e" s="T44">Nʼalaj </ts>
               <ts e="T46" id="Seg_350" n="e" s="T45">qobɨn </ts>
               <ts e="T47" id="Seg_352" n="e" s="T46">kurkan </ts>
               <ts e="T48" id="Seg_354" n="e" s="T47">madɨn </ts>
               <ts e="T49" id="Seg_356" n="e" s="T48">sʼutʼdʼäɣən. </ts>
               <ts e="T50" id="Seg_358" n="e" s="T49">A </ts>
               <ts e="T51" id="Seg_360" n="e" s="T50">man </ts>
               <ts e="T52" id="Seg_362" n="e" s="T51">maǯʼezʼidan </ts>
               <ts e="T53" id="Seg_364" n="e" s="T52">qɨːba </ts>
               <ts e="T54" id="Seg_366" n="e" s="T53">tawakala. </ts>
               <ts e="T55" id="Seg_368" n="e" s="T54">Mʼaguntɨ </ts>
               <ts e="T56" id="Seg_370" n="e" s="T55">kɨdan </ts>
               <ts e="T57" id="Seg_372" n="e" s="T56">kula </ts>
               <ts e="T58" id="Seg_374" n="e" s="T57">serkott. </ts>
               <ts e="T59" id="Seg_376" n="e" s="T58">Kusaqen </ts>
               <ts e="T60" id="Seg_378" n="e" s="T59">man </ts>
               <ts e="T61" id="Seg_380" n="e" s="T60">nʼalaj </ts>
               <ts e="T62" id="Seg_382" n="e" s="T61">qobɨn </ts>
               <ts e="T63" id="Seg_384" n="e" s="T62">jezan, </ts>
               <ts e="T64" id="Seg_386" n="e" s="T63">kak </ts>
               <ts e="T65" id="Seg_388" n="e" s="T64">bɨ </ts>
               <ts e="T66" id="Seg_390" n="e" s="T65">nɨtdɨn </ts>
               <ts e="T67" id="Seg_392" n="e" s="T66">sʼernʼatt. </ts>
               <ts e="T68" id="Seg_394" n="e" s="T67">Ot </ts>
               <ts e="T69" id="Seg_396" n="e" s="T68">man </ts>
               <ts e="T70" id="Seg_398" n="e" s="T69">qarɨsan, </ts>
               <ts e="T71" id="Seg_400" n="e" s="T70">qarɨsan. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_401" s="T1">PVD_1964_LittleMice_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_402" s="T4">PVD_1964_LittleMice_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_403" s="T7">PVD_1964_LittleMice_nar.003 (001.003)</ta>
            <ta e="T16" id="Seg_404" s="T13">PVD_1964_LittleMice_nar.004 (001.004)</ta>
            <ta e="T24" id="Seg_405" s="T16">PVD_1964_LittleMice_nar.005 (001.005)</ta>
            <ta e="T27" id="Seg_406" s="T24">PVD_1964_LittleMice_nar.006 (001.006)</ta>
            <ta e="T31" id="Seg_407" s="T27">PVD_1964_LittleMice_nar.007 (001.007)</ta>
            <ta e="T37" id="Seg_408" s="T31">PVD_1964_LittleMice_nar.008 (001.008)</ta>
            <ta e="T41" id="Seg_409" s="T37">PVD_1964_LittleMice_nar.009 (001.009)</ta>
            <ta e="T44" id="Seg_410" s="T41">PVD_1964_LittleMice_nar.010 (001.010)</ta>
            <ta e="T49" id="Seg_411" s="T44">PVD_1964_LittleMice_nar.011 (001.011)</ta>
            <ta e="T54" id="Seg_412" s="T49">PVD_1964_LittleMice_nar.012 (001.012)</ta>
            <ta e="T58" id="Seg_413" s="T54">PVD_1964_LittleMice_nar.013 (001.013)</ta>
            <ta e="T67" id="Seg_414" s="T58">PVD_1964_LittleMice_nar.014 (001.014)</ta>
            <ta e="T71" id="Seg_415" s="T67">PVD_1964_LittleMice_nar.015 (001.015)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_416" s="T1">а′нан kы′банʼажау ′куза.</ta>
            <ta e="T7" id="Seg_417" s="T4">ме ′тебым хара′ни′сафтъ.</ta>
            <ta e="T13" id="Seg_418" s="T7">′те̨бын пи′лʼонкалам ‵таkкъ′лʼе̄ ′пʼӧтʼе ′па̄род пе′ссау̹.</ta>
            <ta e="T16" id="Seg_419" s="T13">теб′ла натʼен и пы′заттъ.</ta>
            <ta e="T24" id="Seg_420" s="T16">ман пʼӧтʼе ′парод ′сыɣъ‵сан, ′теблам вес ′кучу ′тшӯlджау̹.</ta>
            <ta e="T27" id="Seg_421" s="T24">′салтши‵боддъ ′илʼе jӱбърау̹.</ta>
            <ta e="T31" id="Seg_422" s="T27">а ′тава kо′ббат на′тʼен.</ta>
            <ta e="T37" id="Seg_423" s="T31">и теб′ла ′мег̂га ‵мугут′kе̨н вес пыг̂′ге′лат.</ta>
            <ta e="T41" id="Seg_424" s="T37">ман д̂а′вай ′kа̄рылʼе̄, ′kа̄рылʼе̄.</ta>
            <ta e="T44" id="Seg_425" s="T41">′платjо ′вес ни′жарнау̹.</ta>
            <ta e="T49" id="Seg_426" s="T44">нʼа′лай kо′бын кур′кан ′мадын сʼутʼ′дʼӓɣън.</ta>
            <ta e="T54" id="Seg_427" s="T49">а ман ма′джʼе̨зʼидан kы̄ба ′тавакала.</ta>
            <ta e="T58" id="Seg_428" s="T54">мʼа′гунты ′кыдан ку′ла ′серкотт.</ta>
            <ta e="T67" id="Seg_429" s="T58">ку′саkе(ӓ)н ман нʼа′лай ′kобын ′jезан, как бы ныт′дын сʼернʼатт.</ta>
            <ta e="T71" id="Seg_430" s="T67">от ман ′kарысан, ′kарысан.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_431" s="T1">anan qɨbanʼaʒau kuza.</ta>
            <ta e="T7" id="Seg_432" s="T4">me tebɨm xaranisaftə.</ta>
            <ta e="T13" id="Seg_433" s="T7">tebɨn pilʼonkalam taqkəlʼeː pʼötʼe paːrod pessau̹.</ta>
            <ta e="T16" id="Seg_434" s="T13">tebla natʼen i pɨzattə.</ta>
            <ta e="T24" id="Seg_435" s="T16">man pʼötʼe parod sɨɣəsan, teblam wes kuču tšuːlǯau̹.</ta>
            <ta e="T27" id="Seg_436" s="T24">saltšiboddə ilʼe jübərau̹.</ta>
            <ta e="T31" id="Seg_437" s="T27">a tawa qobbat natʼen.</ta>
            <ta e="T37" id="Seg_438" s="T31">i tebla meĝga mugutqen wes pɨĝgelat.</ta>
            <ta e="T41" id="Seg_439" s="T37">man d̂awaj qaːrɨlʼeː, qaːrɨlʼeː.</ta>
            <ta e="T44" id="Seg_440" s="T41">platjo wes niʒarnau̹.</ta>
            <ta e="T49" id="Seg_441" s="T44">nʼalaj qobɨn kurkan madɨn sʼutʼdʼäɣən.</ta>
            <ta e="T54" id="Seg_442" s="T49">a man maǯʼezʼidan qɨːba tawakala.</ta>
            <ta e="T58" id="Seg_443" s="T54">mʼaguntɨ kɨdan kula serkott.</ta>
            <ta e="T67" id="Seg_444" s="T58">kusaqe(ä)n man nʼalaj qobɨn jezan, kak bɨ nɨtdɨn sʼernʼatt.</ta>
            <ta e="T71" id="Seg_445" s="T67">ot man qarɨsan, qarɨsan.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_446" s="T1">Manan qɨbanʼaʒau kuza. </ta>
            <ta e="T7" id="Seg_447" s="T4">Me tebɨm xaranisaftə. </ta>
            <ta e="T13" id="Seg_448" s="T7">Tebɨn pilʼonkalam taqkəlʼeː pʼötʼe paːrod pessau. </ta>
            <ta e="T16" id="Seg_449" s="T13">Tebla natʼen ipɨzattə. </ta>
            <ta e="T24" id="Seg_450" s="T16">Man pʼötʼe parod sɨɣəsan, teblam wes kuču čuːlǯau. </ta>
            <ta e="T27" id="Seg_451" s="T24">Saltšiboddə ilʼe jübərau. </ta>
            <ta e="T31" id="Seg_452" s="T27">A tawa qobbat natʼen. </ta>
            <ta e="T37" id="Seg_453" s="T31">I tebla megga mugutqen wes pɨggelat. </ta>
            <ta e="T41" id="Seg_454" s="T37">Man dawaj qaːrɨlʼeː, qaːrɨlʼeː. </ta>
            <ta e="T44" id="Seg_455" s="T41">Platjo wes niʒarnau. </ta>
            <ta e="T49" id="Seg_456" s="T44">Nʼalaj qobɨn kurkan madɨn sʼutʼdʼäɣən. </ta>
            <ta e="T54" id="Seg_457" s="T49">A man maǯʼezʼidan qɨːba tawakala. </ta>
            <ta e="T58" id="Seg_458" s="T54">Mʼaguntɨ kɨdan kula serkott. </ta>
            <ta e="T67" id="Seg_459" s="T58">Kusaqen man nʼalaj qobɨn jezan, kak bɨ nɨtdɨn sʼernʼatt. </ta>
            <ta e="T71" id="Seg_460" s="T67">Ot man qarɨsan, qarɨsan. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_461" s="T1">ma-nan</ta>
            <ta e="T3" id="Seg_462" s="T2">qɨbanʼaʒa-u</ta>
            <ta e="T4" id="Seg_463" s="T3">ku-za</ta>
            <ta e="T5" id="Seg_464" s="T4">me</ta>
            <ta e="T6" id="Seg_465" s="T5">teb-ɨ-m</ta>
            <ta e="T7" id="Seg_466" s="T6">xarani-sa-ftə</ta>
            <ta e="T8" id="Seg_467" s="T7">teb-ɨ-n</ta>
            <ta e="T9" id="Seg_468" s="T8">pilʼonka-la-m</ta>
            <ta e="T10" id="Seg_469" s="T9">taqkə-lʼeː</ta>
            <ta e="T11" id="Seg_470" s="T10">pʼötʼe</ta>
            <ta e="T12" id="Seg_471" s="T11">paːr-o-d</ta>
            <ta e="T13" id="Seg_472" s="T12">pes-sa-u</ta>
            <ta e="T14" id="Seg_473" s="T13">teb-la</ta>
            <ta e="T15" id="Seg_474" s="T14">natʼe-n</ta>
            <ta e="T16" id="Seg_475" s="T15">ipɨ-za-ttə</ta>
            <ta e="T17" id="Seg_476" s="T16">man</ta>
            <ta e="T18" id="Seg_477" s="T17">pʼötʼe</ta>
            <ta e="T19" id="Seg_478" s="T18">par-o-d</ta>
            <ta e="T20" id="Seg_479" s="T19">sɨɣə-sa-n</ta>
            <ta e="T21" id="Seg_480" s="T20">teb-la-m</ta>
            <ta e="T22" id="Seg_481" s="T21">wes</ta>
            <ta e="T23" id="Seg_482" s="T22">kuču</ta>
            <ta e="T24" id="Seg_483" s="T23">čuːlǯa-u</ta>
            <ta e="T25" id="Seg_484" s="T24">saltšibo-ddə</ta>
            <ta e="T26" id="Seg_485" s="T25">ilʼe</ta>
            <ta e="T27" id="Seg_486" s="T26">jübə-ra-u</ta>
            <ta e="T28" id="Seg_487" s="T27">a</ta>
            <ta e="T29" id="Seg_488" s="T28">tawa</ta>
            <ta e="T30" id="Seg_489" s="T29">qo-bba-t</ta>
            <ta e="T31" id="Seg_490" s="T30">natʼe-n</ta>
            <ta e="T32" id="Seg_491" s="T31">i</ta>
            <ta e="T33" id="Seg_492" s="T32">teb-la</ta>
            <ta e="T34" id="Seg_493" s="T33">megga</ta>
            <ta e="T35" id="Seg_494" s="T34">mugut-qen</ta>
            <ta e="T36" id="Seg_495" s="T35">wes</ta>
            <ta e="T37" id="Seg_496" s="T36">pɨggel-a-t</ta>
            <ta e="T38" id="Seg_497" s="T37">man</ta>
            <ta e="T39" id="Seg_498" s="T38">dawaj</ta>
            <ta e="T40" id="Seg_499" s="T39">qaːrɨ-lʼeː</ta>
            <ta e="T41" id="Seg_500" s="T40">qaːrɨ-lʼeː</ta>
            <ta e="T42" id="Seg_501" s="T41">platjo</ta>
            <ta e="T43" id="Seg_502" s="T42">wes</ta>
            <ta e="T44" id="Seg_503" s="T43">niʒar-na-u</ta>
            <ta e="T45" id="Seg_504" s="T44">nʼalaj</ta>
            <ta e="T46" id="Seg_505" s="T45">qob-ɨ-n</ta>
            <ta e="T47" id="Seg_506" s="T46">kur-ka-n</ta>
            <ta e="T48" id="Seg_507" s="T47">mad-ɨ-n</ta>
            <ta e="T49" id="Seg_508" s="T48">sʼutʼdʼä-ɣən</ta>
            <ta e="T50" id="Seg_509" s="T49">a</ta>
            <ta e="T51" id="Seg_510" s="T50">man</ta>
            <ta e="T52" id="Seg_511" s="T51">maǯʼe-zʼi-da-n</ta>
            <ta e="T53" id="Seg_512" s="T52">qɨːba</ta>
            <ta e="T54" id="Seg_513" s="T53">tawa-ka-la</ta>
            <ta e="T55" id="Seg_514" s="T54">mʼa-guntɨ</ta>
            <ta e="T56" id="Seg_515" s="T55">kɨdan</ta>
            <ta e="T57" id="Seg_516" s="T56">ku-la</ta>
            <ta e="T58" id="Seg_517" s="T57">ser-ko-tt</ta>
            <ta e="T59" id="Seg_518" s="T58">kusaqen</ta>
            <ta e="T60" id="Seg_519" s="T59">man</ta>
            <ta e="T61" id="Seg_520" s="T60">nʼalaj</ta>
            <ta e="T62" id="Seg_521" s="T61">qob-ɨ-n</ta>
            <ta e="T63" id="Seg_522" s="T62">je-za-n</ta>
            <ta e="T64" id="Seg_523" s="T63">kak</ta>
            <ta e="T65" id="Seg_524" s="T64">bɨ</ta>
            <ta e="T66" id="Seg_525" s="T65">nɨtdɨ-n</ta>
            <ta e="T67" id="Seg_526" s="T66">sʼer-nʼa-tt</ta>
            <ta e="T68" id="Seg_527" s="T67">ot</ta>
            <ta e="T69" id="Seg_528" s="T68">man</ta>
            <ta e="T70" id="Seg_529" s="T69">qarɨ-sa-n</ta>
            <ta e="T71" id="Seg_530" s="T70">qarɨ-sa-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_531" s="T1">man-nan</ta>
            <ta e="T3" id="Seg_532" s="T2">qɨbanʼaǯa-w</ta>
            <ta e="T4" id="Seg_533" s="T3">quː-sɨ</ta>
            <ta e="T5" id="Seg_534" s="T4">me</ta>
            <ta e="T6" id="Seg_535" s="T5">täp-ɨ-m</ta>
            <ta e="T7" id="Seg_536" s="T6">horonʼi-sɨ-un</ta>
            <ta e="T8" id="Seg_537" s="T7">täp-ɨ-n</ta>
            <ta e="T9" id="Seg_538" s="T8">pilʼonka-la-m</ta>
            <ta e="T10" id="Seg_539" s="T9">taqqə-le</ta>
            <ta e="T11" id="Seg_540" s="T10">pötte</ta>
            <ta e="T12" id="Seg_541" s="T11">par-ɨ-ntə</ta>
            <ta e="T13" id="Seg_542" s="T12">pen-sɨ-w</ta>
            <ta e="T14" id="Seg_543" s="T13">täp-la</ta>
            <ta e="T15" id="Seg_544" s="T14">*natʼe-n</ta>
            <ta e="T16" id="Seg_545" s="T15">ippɨ-sɨ-tɨn</ta>
            <ta e="T17" id="Seg_546" s="T16">man</ta>
            <ta e="T18" id="Seg_547" s="T17">pötte</ta>
            <ta e="T19" id="Seg_548" s="T18">par-ɨ-ntə</ta>
            <ta e="T20" id="Seg_549" s="T19">sɨɣə-sɨ-ŋ</ta>
            <ta e="T21" id="Seg_550" s="T20">täp-la-m</ta>
            <ta e="T22" id="Seg_551" s="T21">wesʼ</ta>
            <ta e="T23" id="Seg_552" s="T22">kuču</ta>
            <ta e="T24" id="Seg_553" s="T23">čulǯu-w</ta>
            <ta e="T25" id="Seg_554" s="T24">saltšibo-ntə</ta>
            <ta e="T26" id="Seg_555" s="T25">ilʼlʼe</ta>
            <ta e="T27" id="Seg_556" s="T26">übɨ-rɨ-w</ta>
            <ta e="T28" id="Seg_557" s="T27">a</ta>
            <ta e="T29" id="Seg_558" s="T28">tawa</ta>
            <ta e="T30" id="Seg_559" s="T29">qo-mbɨ-t</ta>
            <ta e="T31" id="Seg_560" s="T30">*natʼe-n</ta>
            <ta e="T32" id="Seg_561" s="T31">i</ta>
            <ta e="T33" id="Seg_562" s="T32">täp-la</ta>
            <ta e="T34" id="Seg_563" s="T33">mekka</ta>
            <ta e="T35" id="Seg_564" s="T34">mugut-qɨn</ta>
            <ta e="T36" id="Seg_565" s="T35">wesʼ</ta>
            <ta e="T37" id="Seg_566" s="T36">pɨŋgəl-ɨ-tɨn</ta>
            <ta e="T38" id="Seg_567" s="T37">man</ta>
            <ta e="T39" id="Seg_568" s="T38">dawaj</ta>
            <ta e="T40" id="Seg_569" s="T39">qarɨ-le</ta>
            <ta e="T41" id="Seg_570" s="T40">qarɨ-le</ta>
            <ta e="T42" id="Seg_571" s="T41">platjo</ta>
            <ta e="T43" id="Seg_572" s="T42">wesʼ</ta>
            <ta e="T44" id="Seg_573" s="T43">niʒar-nɨ-w</ta>
            <ta e="T45" id="Seg_574" s="T44">nʼalal</ta>
            <ta e="T46" id="Seg_575" s="T45">qob-ɨ-ŋ</ta>
            <ta e="T47" id="Seg_576" s="T46">kur-ku-ŋ</ta>
            <ta e="T48" id="Seg_577" s="T47">maːt-ɨ-n</ta>
            <ta e="T49" id="Seg_578" s="T48">sʼütdʼe-qɨn</ta>
            <ta e="T50" id="Seg_579" s="T49">a</ta>
            <ta e="T51" id="Seg_580" s="T50">man</ta>
            <ta e="T52" id="Seg_581" s="T51">manǯu-sɨ-ntɨ-ŋ</ta>
            <ta e="T53" id="Seg_582" s="T52">qɨba</ta>
            <ta e="T54" id="Seg_583" s="T53">tawa-ka-la</ta>
            <ta e="T55" id="Seg_584" s="T54">me-qɨntɨ</ta>
            <ta e="T56" id="Seg_585" s="T55">qɨdan</ta>
            <ta e="T57" id="Seg_586" s="T56">qum-la</ta>
            <ta e="T58" id="Seg_587" s="T57">ser-ku-tɨn</ta>
            <ta e="T59" id="Seg_588" s="T58">qussaqɨn</ta>
            <ta e="T60" id="Seg_589" s="T59">man</ta>
            <ta e="T61" id="Seg_590" s="T60">nʼalal</ta>
            <ta e="T62" id="Seg_591" s="T61">qob-ɨ-ŋ</ta>
            <ta e="T63" id="Seg_592" s="T62">eː-sɨ-ŋ</ta>
            <ta e="T64" id="Seg_593" s="T63">kak</ta>
            <ta e="T65" id="Seg_594" s="T64">bɨ</ta>
            <ta e="T66" id="Seg_595" s="T65">*natʼe-n</ta>
            <ta e="T67" id="Seg_596" s="T66">ser-ne-tɨn</ta>
            <ta e="T68" id="Seg_597" s="T67">ot</ta>
            <ta e="T69" id="Seg_598" s="T68">man</ta>
            <ta e="T70" id="Seg_599" s="T69">qarɨ-sɨ-ŋ</ta>
            <ta e="T71" id="Seg_600" s="T70">qarɨ-sɨ-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_601" s="T1">I-ADES</ta>
            <ta e="T3" id="Seg_602" s="T2">child.[NOM]-1SG</ta>
            <ta e="T4" id="Seg_603" s="T3">die-PST.[3SG.S]</ta>
            <ta e="T5" id="Seg_604" s="T4">we.GEN</ta>
            <ta e="T6" id="Seg_605" s="T5">(s)he-EP-ACC</ta>
            <ta e="T7" id="Seg_606" s="T6">bury-PST-1PL</ta>
            <ta e="T8" id="Seg_607" s="T7">(s)he-EP-GEN</ta>
            <ta e="T9" id="Seg_608" s="T8">nappy-PL-ACC</ta>
            <ta e="T10" id="Seg_609" s="T9">gather-CVB</ta>
            <ta e="T11" id="Seg_610" s="T10">stove.[NOM]</ta>
            <ta e="T12" id="Seg_611" s="T11">top-EP-ILL</ta>
            <ta e="T13" id="Seg_612" s="T12">put-PST-1SG.O</ta>
            <ta e="T14" id="Seg_613" s="T13">(s)he-PL.[NOM]</ta>
            <ta e="T15" id="Seg_614" s="T14">there-ADV.LOC</ta>
            <ta e="T16" id="Seg_615" s="T15">lie-PST-3PL</ta>
            <ta e="T17" id="Seg_616" s="T16">I.NOM</ta>
            <ta e="T18" id="Seg_617" s="T17">stove.[NOM]</ta>
            <ta e="T19" id="Seg_618" s="T18">top-EP-ILL</ta>
            <ta e="T20" id="Seg_619" s="T19">climb-PST-1SG.S</ta>
            <ta e="T21" id="Seg_620" s="T20">(s)he-PL-ACC</ta>
            <ta e="T22" id="Seg_621" s="T21">all</ta>
            <ta e="T23" id="Seg_622" s="T22">into.a.pile</ta>
            <ta e="T24" id="Seg_623" s="T23">rake.up-1SG.O</ta>
            <ta e="T25" id="Seg_624" s="T24">floor-ILL</ta>
            <ta e="T26" id="Seg_625" s="T25">down</ta>
            <ta e="T27" id="Seg_626" s="T26">begin-DRV-1SG.O</ta>
            <ta e="T28" id="Seg_627" s="T27">and</ta>
            <ta e="T29" id="Seg_628" s="T28">mouse.[NOM]</ta>
            <ta e="T30" id="Seg_629" s="T29">give.birth-PST.NAR-3SG.O</ta>
            <ta e="T31" id="Seg_630" s="T30">there-ADV.LOC</ta>
            <ta e="T32" id="Seg_631" s="T31">and</ta>
            <ta e="T33" id="Seg_632" s="T32">(s)he-PL.[NOM]</ta>
            <ta e="T34" id="Seg_633" s="T33">I.ALL</ta>
            <ta e="T35" id="Seg_634" s="T34">bosom-LOC</ta>
            <ta e="T36" id="Seg_635" s="T35">all</ta>
            <ta e="T37" id="Seg_636" s="T36">fall.down-EP-3PL</ta>
            <ta e="T38" id="Seg_637" s="T37">I.NOM</ta>
            <ta e="T39" id="Seg_638" s="T38">INCH</ta>
            <ta e="T40" id="Seg_639" s="T39">shout-CVB</ta>
            <ta e="T41" id="Seg_640" s="T40">shout-CVB</ta>
            <ta e="T42" id="Seg_641" s="T41">dress.[NOM]</ta>
            <ta e="T43" id="Seg_642" s="T42">all</ta>
            <ta e="T44" id="Seg_643" s="T43">tear-CO-1SG.O</ta>
            <ta e="T45" id="Seg_644" s="T44">naked</ta>
            <ta e="T46" id="Seg_645" s="T45">skin-EP-ADVZ</ta>
            <ta e="T47" id="Seg_646" s="T46">run-HAB-1SG.S</ta>
            <ta e="T48" id="Seg_647" s="T47">house-EP-GEN</ta>
            <ta e="T49" id="Seg_648" s="T48">inside-LOC</ta>
            <ta e="T50" id="Seg_649" s="T49">and</ta>
            <ta e="T51" id="Seg_650" s="T50">I.NOM</ta>
            <ta e="T52" id="Seg_651" s="T51">look.at-PST-INFER-1SG.S</ta>
            <ta e="T53" id="Seg_652" s="T52">small</ta>
            <ta e="T54" id="Seg_653" s="T53">mouse-DIM-PL.[NOM]</ta>
            <ta e="T55" id="Seg_654" s="T54">we-ILL.3SG</ta>
            <ta e="T56" id="Seg_655" s="T55">all.the.time</ta>
            <ta e="T57" id="Seg_656" s="T56">human.being-PL.[NOM]</ta>
            <ta e="T58" id="Seg_657" s="T57">come.in-HAB-3PL</ta>
            <ta e="T59" id="Seg_658" s="T58">when</ta>
            <ta e="T60" id="Seg_659" s="T59">I.NOM</ta>
            <ta e="T61" id="Seg_660" s="T60">naked</ta>
            <ta e="T62" id="Seg_661" s="T61">skin-EP-ADVZ</ta>
            <ta e="T63" id="Seg_662" s="T62">be-PST-1SG.S</ta>
            <ta e="T64" id="Seg_663" s="T63">how</ta>
            <ta e="T65" id="Seg_664" s="T64">IRREAL</ta>
            <ta e="T66" id="Seg_665" s="T65">there-ADV.LOC</ta>
            <ta e="T67" id="Seg_666" s="T66">come.in-CONJ-3PL</ta>
            <ta e="T68" id="Seg_667" s="T67">look.here</ta>
            <ta e="T69" id="Seg_668" s="T68">I.NOM</ta>
            <ta e="T70" id="Seg_669" s="T69">shout-PST-1SG.S</ta>
            <ta e="T71" id="Seg_670" s="T70">shout-PST-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_671" s="T1">я-ADES</ta>
            <ta e="T3" id="Seg_672" s="T2">ребенок.[NOM]-1SG</ta>
            <ta e="T4" id="Seg_673" s="T3">умереть-PST.[3SG.S]</ta>
            <ta e="T5" id="Seg_674" s="T4">мы.GEN</ta>
            <ta e="T6" id="Seg_675" s="T5">он(а)-EP-ACC</ta>
            <ta e="T7" id="Seg_676" s="T6">похоронить-PST-1PL</ta>
            <ta e="T8" id="Seg_677" s="T7">он(а)-EP-GEN</ta>
            <ta e="T9" id="Seg_678" s="T8">пеленка-PL-ACC</ta>
            <ta e="T10" id="Seg_679" s="T9">собрать-CVB</ta>
            <ta e="T11" id="Seg_680" s="T10">печь.[NOM]</ta>
            <ta e="T12" id="Seg_681" s="T11">верхняя.часть-EP-ILL</ta>
            <ta e="T13" id="Seg_682" s="T12">положить-PST-1SG.O</ta>
            <ta e="T14" id="Seg_683" s="T13">он(а)-PL.[NOM]</ta>
            <ta e="T15" id="Seg_684" s="T14">туда-ADV.LOC</ta>
            <ta e="T16" id="Seg_685" s="T15">лежать-PST-3PL</ta>
            <ta e="T17" id="Seg_686" s="T16">я.NOM</ta>
            <ta e="T18" id="Seg_687" s="T17">печь.[NOM]</ta>
            <ta e="T19" id="Seg_688" s="T18">верхняя.часть-EP-ILL</ta>
            <ta e="T20" id="Seg_689" s="T19">залезть-PST-1SG.S</ta>
            <ta e="T21" id="Seg_690" s="T20">он(а)-PL-ACC</ta>
            <ta e="T22" id="Seg_691" s="T21">весь</ta>
            <ta e="T23" id="Seg_692" s="T22">в.кучу</ta>
            <ta e="T24" id="Seg_693" s="T23">сгрести-1SG.O</ta>
            <ta e="T25" id="Seg_694" s="T24">пол-ILL</ta>
            <ta e="T26" id="Seg_695" s="T25">вниз</ta>
            <ta e="T27" id="Seg_696" s="T26">начать-DRV-1SG.O</ta>
            <ta e="T28" id="Seg_697" s="T27">а</ta>
            <ta e="T29" id="Seg_698" s="T28">мышь.[NOM]</ta>
            <ta e="T30" id="Seg_699" s="T29">родить-PST.NAR-3SG.O</ta>
            <ta e="T31" id="Seg_700" s="T30">туда-ADV.LOC</ta>
            <ta e="T32" id="Seg_701" s="T31">и</ta>
            <ta e="T33" id="Seg_702" s="T32">он(а)-PL.[NOM]</ta>
            <ta e="T34" id="Seg_703" s="T33">я.ALL</ta>
            <ta e="T35" id="Seg_704" s="T34">пазуха-LOC</ta>
            <ta e="T36" id="Seg_705" s="T35">все</ta>
            <ta e="T37" id="Seg_706" s="T36">упасть-EP-3PL</ta>
            <ta e="T38" id="Seg_707" s="T37">я.NOM</ta>
            <ta e="T39" id="Seg_708" s="T38">INCH</ta>
            <ta e="T40" id="Seg_709" s="T39">кричать-CVB</ta>
            <ta e="T41" id="Seg_710" s="T40">кричать-CVB</ta>
            <ta e="T42" id="Seg_711" s="T41">платье.[NOM]</ta>
            <ta e="T43" id="Seg_712" s="T42">весь</ta>
            <ta e="T44" id="Seg_713" s="T43">порвать-CO-1SG.O</ta>
            <ta e="T45" id="Seg_714" s="T44">голый</ta>
            <ta e="T46" id="Seg_715" s="T45">кожа-EP-ADVZ</ta>
            <ta e="T47" id="Seg_716" s="T46">бегать-HAB-1SG.S</ta>
            <ta e="T48" id="Seg_717" s="T47">дом-EP-GEN</ta>
            <ta e="T49" id="Seg_718" s="T48">нутро-LOC</ta>
            <ta e="T50" id="Seg_719" s="T49">а</ta>
            <ta e="T51" id="Seg_720" s="T50">я.NOM</ta>
            <ta e="T52" id="Seg_721" s="T51">посмотреть-PST-INFER-1SG.S</ta>
            <ta e="T53" id="Seg_722" s="T52">маленький</ta>
            <ta e="T54" id="Seg_723" s="T53">мышь-DIM-PL.[NOM]</ta>
            <ta e="T55" id="Seg_724" s="T54">мы-ILL.3SG</ta>
            <ta e="T56" id="Seg_725" s="T55">все.время</ta>
            <ta e="T57" id="Seg_726" s="T56">человек-PL.[NOM]</ta>
            <ta e="T58" id="Seg_727" s="T57">зайти-HAB-3PL</ta>
            <ta e="T59" id="Seg_728" s="T58">когда</ta>
            <ta e="T60" id="Seg_729" s="T59">я.NOM</ta>
            <ta e="T61" id="Seg_730" s="T60">голый</ta>
            <ta e="T62" id="Seg_731" s="T61">кожа-EP-ADVZ</ta>
            <ta e="T63" id="Seg_732" s="T62">быть-PST-1SG.S</ta>
            <ta e="T64" id="Seg_733" s="T63">как</ta>
            <ta e="T65" id="Seg_734" s="T64">IRREAL</ta>
            <ta e="T66" id="Seg_735" s="T65">туда-ADV.LOC</ta>
            <ta e="T67" id="Seg_736" s="T66">зайти-CONJ-3PL</ta>
            <ta e="T68" id="Seg_737" s="T67">вот</ta>
            <ta e="T69" id="Seg_738" s="T68">я.NOM</ta>
            <ta e="T70" id="Seg_739" s="T69">кричать-PST-1SG.S</ta>
            <ta e="T71" id="Seg_740" s="T70">кричать-PST-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_741" s="T1">pers-n:case</ta>
            <ta e="T3" id="Seg_742" s="T2">n.[n:case]-n:poss</ta>
            <ta e="T4" id="Seg_743" s="T3">v-v:tense.[v:pn]</ta>
            <ta e="T5" id="Seg_744" s="T4">pers</ta>
            <ta e="T6" id="Seg_745" s="T5">pers-n:ins-n:case</ta>
            <ta e="T7" id="Seg_746" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_747" s="T7">pers-n:ins-n:case</ta>
            <ta e="T9" id="Seg_748" s="T8">n-n:num-n:case</ta>
            <ta e="T10" id="Seg_749" s="T9">v-v&gt;adv</ta>
            <ta e="T11" id="Seg_750" s="T10">n.[n:case]</ta>
            <ta e="T12" id="Seg_751" s="T11">n-n:ins-n:case</ta>
            <ta e="T13" id="Seg_752" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_753" s="T13">pers-n:num.[n:case]</ta>
            <ta e="T15" id="Seg_754" s="T14">adv-adv:case</ta>
            <ta e="T16" id="Seg_755" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_756" s="T16">pers</ta>
            <ta e="T18" id="Seg_757" s="T17">n.[n:case]</ta>
            <ta e="T19" id="Seg_758" s="T18">n-n:ins-n:case</ta>
            <ta e="T20" id="Seg_759" s="T19">v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_760" s="T20">pers-n:num-n:case</ta>
            <ta e="T22" id="Seg_761" s="T21">quant</ta>
            <ta e="T23" id="Seg_762" s="T22">adv</ta>
            <ta e="T24" id="Seg_763" s="T23">v-v:pn</ta>
            <ta e="T25" id="Seg_764" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_765" s="T25">adv</ta>
            <ta e="T27" id="Seg_766" s="T26">v-v&gt;v-v:pn</ta>
            <ta e="T28" id="Seg_767" s="T27">conj</ta>
            <ta e="T29" id="Seg_768" s="T28">n.[n:case]</ta>
            <ta e="T30" id="Seg_769" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_770" s="T30">adv-adv:case</ta>
            <ta e="T32" id="Seg_771" s="T31">conj</ta>
            <ta e="T33" id="Seg_772" s="T32">pers-n:num.[n:case]</ta>
            <ta e="T34" id="Seg_773" s="T33">pers</ta>
            <ta e="T35" id="Seg_774" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_775" s="T35">quant</ta>
            <ta e="T37" id="Seg_776" s="T36">v-v:ins-v:pn</ta>
            <ta e="T38" id="Seg_777" s="T37">pers</ta>
            <ta e="T39" id="Seg_778" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_779" s="T39">v-v&gt;adv</ta>
            <ta e="T41" id="Seg_780" s="T40">v-v&gt;adv</ta>
            <ta e="T42" id="Seg_781" s="T41">n.[n:case]</ta>
            <ta e="T43" id="Seg_782" s="T42">quant</ta>
            <ta e="T44" id="Seg_783" s="T43">v-v:ins-v:pn</ta>
            <ta e="T45" id="Seg_784" s="T44">adj</ta>
            <ta e="T46" id="Seg_785" s="T45">n-n:ins-n&gt;adv</ta>
            <ta e="T47" id="Seg_786" s="T46">v-v&gt;v-v:pn</ta>
            <ta e="T48" id="Seg_787" s="T47">n-n:ins-n:case</ta>
            <ta e="T49" id="Seg_788" s="T48">n-n:case</ta>
            <ta e="T50" id="Seg_789" s="T49">conj</ta>
            <ta e="T51" id="Seg_790" s="T50">pers</ta>
            <ta e="T52" id="Seg_791" s="T51">v-v:tense-v:mood-v:pn</ta>
            <ta e="T53" id="Seg_792" s="T52">adj</ta>
            <ta e="T54" id="Seg_793" s="T53">n-n&gt;n-n:num.[n:case]</ta>
            <ta e="T55" id="Seg_794" s="T54">pers-n:case.poss</ta>
            <ta e="T56" id="Seg_795" s="T55">adv</ta>
            <ta e="T57" id="Seg_796" s="T56">n-n:num.[n:case]</ta>
            <ta e="T58" id="Seg_797" s="T57">v-v&gt;v-v:pn</ta>
            <ta e="T59" id="Seg_798" s="T58">conj</ta>
            <ta e="T60" id="Seg_799" s="T59">pers</ta>
            <ta e="T61" id="Seg_800" s="T60">adj</ta>
            <ta e="T62" id="Seg_801" s="T61">n-n:ins-n&gt;adv</ta>
            <ta e="T63" id="Seg_802" s="T62">v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_803" s="T63">interrog</ta>
            <ta e="T65" id="Seg_804" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_805" s="T65">adv-adv:case</ta>
            <ta e="T67" id="Seg_806" s="T66">v-v:mood-v:pn</ta>
            <ta e="T68" id="Seg_807" s="T67">interj</ta>
            <ta e="T69" id="Seg_808" s="T68">pers</ta>
            <ta e="T70" id="Seg_809" s="T69">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_810" s="T70">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_811" s="T1">pers</ta>
            <ta e="T3" id="Seg_812" s="T2">n</ta>
            <ta e="T4" id="Seg_813" s="T3">v</ta>
            <ta e="T5" id="Seg_814" s="T4">pers</ta>
            <ta e="T6" id="Seg_815" s="T5">pers</ta>
            <ta e="T7" id="Seg_816" s="T6">v</ta>
            <ta e="T8" id="Seg_817" s="T7">pers</ta>
            <ta e="T9" id="Seg_818" s="T8">n</ta>
            <ta e="T10" id="Seg_819" s="T9">adv</ta>
            <ta e="T11" id="Seg_820" s="T10">n</ta>
            <ta e="T12" id="Seg_821" s="T11">n</ta>
            <ta e="T13" id="Seg_822" s="T12">v</ta>
            <ta e="T14" id="Seg_823" s="T13">pers</ta>
            <ta e="T15" id="Seg_824" s="T14">adv</ta>
            <ta e="T16" id="Seg_825" s="T15">v</ta>
            <ta e="T17" id="Seg_826" s="T16">pers</ta>
            <ta e="T18" id="Seg_827" s="T17">n</ta>
            <ta e="T19" id="Seg_828" s="T18">n</ta>
            <ta e="T20" id="Seg_829" s="T19">v</ta>
            <ta e="T21" id="Seg_830" s="T20">pers</ta>
            <ta e="T22" id="Seg_831" s="T21">quant</ta>
            <ta e="T23" id="Seg_832" s="T22">adv</ta>
            <ta e="T24" id="Seg_833" s="T23">v</ta>
            <ta e="T25" id="Seg_834" s="T24">n</ta>
            <ta e="T26" id="Seg_835" s="T25">adv</ta>
            <ta e="T27" id="Seg_836" s="T26">v</ta>
            <ta e="T28" id="Seg_837" s="T27">conj</ta>
            <ta e="T29" id="Seg_838" s="T28">n</ta>
            <ta e="T30" id="Seg_839" s="T29">v</ta>
            <ta e="T31" id="Seg_840" s="T30">adv</ta>
            <ta e="T32" id="Seg_841" s="T31">conj</ta>
            <ta e="T33" id="Seg_842" s="T32">pers</ta>
            <ta e="T34" id="Seg_843" s="T33">pers</ta>
            <ta e="T35" id="Seg_844" s="T34">n</ta>
            <ta e="T36" id="Seg_845" s="T35">quant</ta>
            <ta e="T37" id="Seg_846" s="T36">v</ta>
            <ta e="T38" id="Seg_847" s="T37">pers</ta>
            <ta e="T39" id="Seg_848" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_849" s="T39">adv</ta>
            <ta e="T41" id="Seg_850" s="T40">adv</ta>
            <ta e="T42" id="Seg_851" s="T41">n</ta>
            <ta e="T43" id="Seg_852" s="T42">quant</ta>
            <ta e="T44" id="Seg_853" s="T43">v</ta>
            <ta e="T45" id="Seg_854" s="T44">adj</ta>
            <ta e="T46" id="Seg_855" s="T45">adv</ta>
            <ta e="T47" id="Seg_856" s="T46">v</ta>
            <ta e="T48" id="Seg_857" s="T47">n</ta>
            <ta e="T49" id="Seg_858" s="T48">n</ta>
            <ta e="T50" id="Seg_859" s="T49">conj</ta>
            <ta e="T51" id="Seg_860" s="T50">pers</ta>
            <ta e="T52" id="Seg_861" s="T51">v</ta>
            <ta e="T53" id="Seg_862" s="T52">adj</ta>
            <ta e="T54" id="Seg_863" s="T53">n</ta>
            <ta e="T55" id="Seg_864" s="T54">pers</ta>
            <ta e="T56" id="Seg_865" s="T55">adv</ta>
            <ta e="T57" id="Seg_866" s="T56">n</ta>
            <ta e="T58" id="Seg_867" s="T57">v</ta>
            <ta e="T59" id="Seg_868" s="T58">conj</ta>
            <ta e="T60" id="Seg_869" s="T59">pers</ta>
            <ta e="T61" id="Seg_870" s="T60">adj</ta>
            <ta e="T62" id="Seg_871" s="T61">adv</ta>
            <ta e="T63" id="Seg_872" s="T62">v</ta>
            <ta e="T64" id="Seg_873" s="T63">interrog</ta>
            <ta e="T65" id="Seg_874" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_875" s="T65">adv</ta>
            <ta e="T67" id="Seg_876" s="T66">v</ta>
            <ta e="T68" id="Seg_877" s="T67">interj</ta>
            <ta e="T69" id="Seg_878" s="T68">pers</ta>
            <ta e="T70" id="Seg_879" s="T69">v</ta>
            <ta e="T71" id="Seg_880" s="T70">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_881" s="T1">pro.h:Poss</ta>
            <ta e="T3" id="Seg_882" s="T2">np.h:P</ta>
            <ta e="T5" id="Seg_883" s="T4">pro.h:A</ta>
            <ta e="T6" id="Seg_884" s="T5">pro.h:P</ta>
            <ta e="T8" id="Seg_885" s="T7">pro.h:Poss</ta>
            <ta e="T9" id="Seg_886" s="T8">np:Th</ta>
            <ta e="T12" id="Seg_887" s="T11">np:G</ta>
            <ta e="T13" id="Seg_888" s="T12">0.1.h:A</ta>
            <ta e="T14" id="Seg_889" s="T13">pro.h:Th</ta>
            <ta e="T15" id="Seg_890" s="T14">adv:L</ta>
            <ta e="T17" id="Seg_891" s="T16">pro.h:A</ta>
            <ta e="T19" id="Seg_892" s="T18">np:G</ta>
            <ta e="T21" id="Seg_893" s="T20">pro:Th</ta>
            <ta e="T24" id="Seg_894" s="T23">0.1.h:A</ta>
            <ta e="T25" id="Seg_895" s="T24">np:G</ta>
            <ta e="T27" id="Seg_896" s="T26">0.1.h:A</ta>
            <ta e="T29" id="Seg_897" s="T28">np:P</ta>
            <ta e="T31" id="Seg_898" s="T30">adv:L</ta>
            <ta e="T33" id="Seg_899" s="T32">pro:P</ta>
            <ta e="T35" id="Seg_900" s="T34">np:G</ta>
            <ta e="T42" id="Seg_901" s="T41">np:P</ta>
            <ta e="T44" id="Seg_902" s="T43">0.1.h:A</ta>
            <ta e="T47" id="Seg_903" s="T46">0.1.h:A</ta>
            <ta e="T48" id="Seg_904" s="T47">np:Poss</ta>
            <ta e="T49" id="Seg_905" s="T48">np:L</ta>
            <ta e="T51" id="Seg_906" s="T50">pro.h:A</ta>
            <ta e="T54" id="Seg_907" s="T53">np:Th</ta>
            <ta e="T55" id="Seg_908" s="T54">pro.h:G</ta>
            <ta e="T56" id="Seg_909" s="T55">adv:Time</ta>
            <ta e="T57" id="Seg_910" s="T56">np.h:A</ta>
            <ta e="T60" id="Seg_911" s="T59">pro.h:Th</ta>
            <ta e="T66" id="Seg_912" s="T65">adv:G</ta>
            <ta e="T67" id="Seg_913" s="T66">0.3.h:A</ta>
            <ta e="T69" id="Seg_914" s="T68">pro.h:A</ta>
            <ta e="T71" id="Seg_915" s="T70">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_916" s="T2">np.h:S</ta>
            <ta e="T4" id="Seg_917" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_918" s="T4">pro.h:S</ta>
            <ta e="T6" id="Seg_919" s="T5">pro.h:O</ta>
            <ta e="T7" id="Seg_920" s="T6">v:pred</ta>
            <ta e="T9" id="Seg_921" s="T8">np:O</ta>
            <ta e="T10" id="Seg_922" s="T9">s:temp</ta>
            <ta e="T13" id="Seg_923" s="T12">0.1.h:S v:pred</ta>
            <ta e="T14" id="Seg_924" s="T13">pro.h:S</ta>
            <ta e="T16" id="Seg_925" s="T15">v:pred</ta>
            <ta e="T17" id="Seg_926" s="T16">pro.h:S</ta>
            <ta e="T20" id="Seg_927" s="T19">v:pred</ta>
            <ta e="T21" id="Seg_928" s="T20">pro:O</ta>
            <ta e="T24" id="Seg_929" s="T23">0.1.h:S v:pred</ta>
            <ta e="T27" id="Seg_930" s="T26">0.1.h:S v:pred</ta>
            <ta e="T29" id="Seg_931" s="T28">np:S</ta>
            <ta e="T30" id="Seg_932" s="T29">v:pred</ta>
            <ta e="T33" id="Seg_933" s="T32">pro:S</ta>
            <ta e="T37" id="Seg_934" s="T36">v:pred</ta>
            <ta e="T42" id="Seg_935" s="T41">np:O</ta>
            <ta e="T44" id="Seg_936" s="T43">0.1.h:S v:pred</ta>
            <ta e="T47" id="Seg_937" s="T46">0.1.h:S v:pred</ta>
            <ta e="T51" id="Seg_938" s="T50">pro.h:S</ta>
            <ta e="T52" id="Seg_939" s="T51">v:pred</ta>
            <ta e="T54" id="Seg_940" s="T53">np:O</ta>
            <ta e="T57" id="Seg_941" s="T56">np.h:S</ta>
            <ta e="T58" id="Seg_942" s="T57">v:pred</ta>
            <ta e="T63" id="Seg_943" s="T58">s:temp</ta>
            <ta e="T67" id="Seg_944" s="T66">0.3.h:S v:pred</ta>
            <ta e="T69" id="Seg_945" s="T68">pro.h:S</ta>
            <ta e="T70" id="Seg_946" s="T69">v:pred</ta>
            <ta e="T71" id="Seg_947" s="T70">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T7" id="Seg_948" s="T6">RUS:core</ta>
            <ta e="T9" id="Seg_949" s="T8">RUS:cult</ta>
            <ta e="T12" id="Seg_950" s="T11">WNB Noun or pp</ta>
            <ta e="T19" id="Seg_951" s="T18">WNB Noun or pp</ta>
            <ta e="T22" id="Seg_952" s="T21">RUS:core</ta>
            <ta e="T23" id="Seg_953" s="T22">RUS:core</ta>
            <ta e="T28" id="Seg_954" s="T27">RUS:gram</ta>
            <ta e="T32" id="Seg_955" s="T31">RUS:gram</ta>
            <ta e="T36" id="Seg_956" s="T35">RUS:core</ta>
            <ta e="T39" id="Seg_957" s="T38">RUS:gram</ta>
            <ta e="T42" id="Seg_958" s="T41">RUS:cult</ta>
            <ta e="T43" id="Seg_959" s="T42">RUS:core</ta>
            <ta e="T50" id="Seg_960" s="T49">RUS:gram</ta>
            <ta e="T64" id="Seg_961" s="T63">RUS:gram</ta>
            <ta e="T65" id="Seg_962" s="T64">RUS:gram</ta>
            <ta e="T68" id="Seg_963" s="T67">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_964" s="T1">My child died.</ta>
            <ta e="T7" id="Seg_965" s="T4">We buried him.</ta>
            <ta e="T13" id="Seg_966" s="T7">I gathered his nappies and put them onto the stove.</ta>
            <ta e="T16" id="Seg_967" s="T13">They were lying there.</ta>
            <ta e="T24" id="Seg_968" s="T16">I climbed onto the stove and gathered them into a pile.</ta>
            <ta e="T27" id="Seg_969" s="T24">I began [taking] them down.</ta>
            <ta e="T31" id="Seg_970" s="T27">And a mouse had [her baby mice] there.</ta>
            <ta e="T37" id="Seg_971" s="T31">And all of them fell down in the bossom (of my dress).</ta>
            <ta e="T41" id="Seg_972" s="T37">And I started crying.</ta>
            <ta e="T44" id="Seg_973" s="T41">I tore my dress completely.</ta>
            <ta e="T49" id="Seg_974" s="T44">I was running naked in the house.</ta>
            <ta e="T54" id="Seg_975" s="T49">And I had a look: little mice.</ta>
            <ta e="T58" id="Seg_976" s="T54">People use to come to us.</ta>
            <ta e="T67" id="Seg_977" s="T58">And when I had been naked, what if they had come?</ta>
            <ta e="T71" id="Seg_978" s="T67">So I cried and cried.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_979" s="T1">Mein Kind ist gestorben.</ta>
            <ta e="T7" id="Seg_980" s="T4">Wir haben ihn begraben.</ta>
            <ta e="T13" id="Seg_981" s="T7">Ich sammelte seine Windeln und legte sie auf den Ofen.</ta>
            <ta e="T16" id="Seg_982" s="T13">Sie lagen dort.</ta>
            <ta e="T24" id="Seg_983" s="T16">Ich kletterte auf den Ofen und sammelte sie zu einem Haufen.</ta>
            <ta e="T27" id="Seg_984" s="T24">Ich begann sie herunter [zu nehmen].</ta>
            <ta e="T31" id="Seg_985" s="T27">Und eine Maus bekam dort [ihre Mäusebabies].</ta>
            <ta e="T37" id="Seg_986" s="T31">Und alle fielen in den Ausschnitt (von meinem Kleid).</ta>
            <ta e="T41" id="Seg_987" s="T37">Und ich begann zu schreien.</ta>
            <ta e="T44" id="Seg_988" s="T41">Ich habe mein Kleid komplett zerrissen.</ta>
            <ta e="T49" id="Seg_989" s="T44">Ich rannte nackt durchs Haus.</ta>
            <ta e="T54" id="Seg_990" s="T49">Und ich sah: Kleine Mäuschen.</ta>
            <ta e="T58" id="Seg_991" s="T54">Zu uns kamen oft Leute.</ta>
            <ta e="T67" id="Seg_992" s="T58">Und als ich nackt war, was wäre wenn sie gekommen wären?</ta>
            <ta e="T71" id="Seg_993" s="T67">So schrie und schrie ich.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_994" s="T1">У меня ребенок умер.</ta>
            <ta e="T7" id="Seg_995" s="T4">Мы его похоронили.</ta>
            <ta e="T13" id="Seg_996" s="T7">Я его пеленки, собрав, на печку положила.</ta>
            <ta e="T16" id="Seg_997" s="T13">Они там лежали.</ta>
            <ta e="T24" id="Seg_998" s="T16">Я на печку залезла, их все в кучу собрала.</ta>
            <ta e="T27" id="Seg_999" s="T24">Я (их) на пол вниз начала (спускать).</ta>
            <ta e="T31" id="Seg_1000" s="T27">А мышка родила там.</ta>
            <ta e="T37" id="Seg_1001" s="T31">И они мне за пазуху все посыпались.</ta>
            <ta e="T41" id="Seg_1002" s="T37">Я давай кричать, кричать.</ta>
            <ta e="T44" id="Seg_1003" s="T41">Платье все порвала.</ta>
            <ta e="T49" id="Seg_1004" s="T44">Нагишом по избе бегаю.</ta>
            <ta e="T54" id="Seg_1005" s="T49">А я взглянула: это мышонки маленькие.</ta>
            <ta e="T58" id="Seg_1006" s="T54">К нам люди всегда заходят.</ta>
            <ta e="T67" id="Seg_1007" s="T58">Ой, когда я нагишом была, если бы там зашли.</ta>
            <ta e="T71" id="Seg_1008" s="T67">Вот я кричала, кричала.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_1009" s="T1">меня ребенок помер</ta>
            <ta e="T7" id="Seg_1010" s="T4">мы его похоронили</ta>
            <ta e="T13" id="Seg_1011" s="T7">его пеленки я забрала на печку положила</ta>
            <ta e="T16" id="Seg_1012" s="T13">они там и лежали</ta>
            <ta e="T24" id="Seg_1013" s="T16">я на печку залезла их все в кучу собрала</ta>
            <ta e="T27" id="Seg_1014" s="T24">я их на пол спускать стала</ta>
            <ta e="T31" id="Seg_1015" s="T27">а мышка окотилась там</ta>
            <ta e="T37" id="Seg_1016" s="T31">они мне за пазуху все посыпались</ta>
            <ta e="T41" id="Seg_1017" s="T37">я давай кричать (реветь) кричать</ta>
            <ta e="T44" id="Seg_1018" s="T41">платье все порвала</ta>
            <ta e="T49" id="Seg_1019" s="T44">нагишом по избе бегаю</ta>
            <ta e="T54" id="Seg_1020" s="T49">а я взглянула а это мышонки маленькие</ta>
            <ta e="T58" id="Seg_1021" s="T54">к нам люди всегда заходят</ta>
            <ta e="T67" id="Seg_1022" s="T58">ой когда я нагишом была как бы зашли ко мне</ta>
            <ta e="T71" id="Seg_1023" s="T67">вот я кричала так кричала</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T16" id="Seg_1024" s="T13">[BrM:] ' i pɨzattə' changed to ' ipɨzattə'.</ta>
            <ta e="T54" id="Seg_1025" s="T49">[BrM:] INFER?</ta>
            <ta e="T58" id="Seg_1026" s="T54">[BrM:] ILL.3SG?</ta>
            <ta e="T67" id="Seg_1027" s="T58">[KuAI:] Variant: 'Kusaqän'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T37" id="Seg_1028" s="T31"> || </ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
