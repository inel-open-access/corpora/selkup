<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_MannersOfWalking_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_MannersOfWalking_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">28</ud-information>
            <ud-information attribute-name="# HIAT:w">22</ud-information>
            <ud-information attribute-name="# e">22</ud-information>
            <ud-information attribute-name="# HIAT:u">5</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T23" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Qum</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">naː</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">quroːlǯa</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">A</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">nejqum</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">tʼötǯiqan</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">čaːʒɨn</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">A</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">oqqɨr</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">nejɣum</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">qɨːbanʼaʒa</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">üːgɨlmɨtɨt</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_47" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">Modʼitdə</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">tebeɣum</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">čaːʒətdə</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_59" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">A</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">oqqɨr</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">pajaga</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">takaja</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_73" n="HIAT:w" s="T20">tʼarɨm</ts>
                  <nts id="Seg_74" n="HIAT:ip">,</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_77" n="HIAT:w" s="T21">nassel</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_80" n="HIAT:w" s="T22">palʼdin</ts>
                  <nts id="Seg_81" n="HIAT:ip">.</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T23" id="Seg_83" n="sc" s="T1">
               <ts e="T2" id="Seg_85" n="e" s="T1">Qum </ts>
               <ts e="T3" id="Seg_87" n="e" s="T2">naː </ts>
               <ts e="T4" id="Seg_89" n="e" s="T3">quroːlǯa. </ts>
               <ts e="T5" id="Seg_91" n="e" s="T4">A </ts>
               <ts e="T6" id="Seg_93" n="e" s="T5">nejqum </ts>
               <ts e="T7" id="Seg_95" n="e" s="T6">tʼötǯiqan </ts>
               <ts e="T8" id="Seg_97" n="e" s="T7">čaːʒɨn. </ts>
               <ts e="T9" id="Seg_99" n="e" s="T8">A </ts>
               <ts e="T10" id="Seg_101" n="e" s="T9">oqqɨr </ts>
               <ts e="T11" id="Seg_103" n="e" s="T10">nejɣum </ts>
               <ts e="T12" id="Seg_105" n="e" s="T11">qɨːbanʼaʒa </ts>
               <ts e="T13" id="Seg_107" n="e" s="T12">üːgɨlmɨtɨt. </ts>
               <ts e="T14" id="Seg_109" n="e" s="T13">Modʼitdə </ts>
               <ts e="T15" id="Seg_111" n="e" s="T14">tebeɣum </ts>
               <ts e="T16" id="Seg_113" n="e" s="T15">čaːʒətdə. </ts>
               <ts e="T17" id="Seg_115" n="e" s="T16">A </ts>
               <ts e="T18" id="Seg_117" n="e" s="T17">oqqɨr </ts>
               <ts e="T19" id="Seg_119" n="e" s="T18">pajaga </ts>
               <ts e="T20" id="Seg_121" n="e" s="T19">takaja </ts>
               <ts e="T21" id="Seg_123" n="e" s="T20">tʼarɨm, </ts>
               <ts e="T22" id="Seg_125" n="e" s="T21">nassel </ts>
               <ts e="T23" id="Seg_127" n="e" s="T22">palʼdin. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_128" s="T1">PVD_1964_MannersOfWalking_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_129" s="T4">PVD_1964_MannersOfWalking_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_130" s="T8">PVD_1964_MannersOfWalking_nar.003 (001.003)</ta>
            <ta e="T16" id="Seg_131" s="T13">PVD_1964_MannersOfWalking_nar.004 (001.004)</ta>
            <ta e="T23" id="Seg_132" s="T16">PVD_1964_MannersOfWalking_nar.005 (001.005)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_133" s="T1">kумна̄ kу′ро̄lджа.</ta>
            <ta e="T8" id="Seg_134" s="T4">а не̹й′kум ′тʼӧтджиkан ′тша̄жын.</ta>
            <ta e="T13" id="Seg_135" s="T8">а о′kkыр ′не̹йɣум ′kы̄′б̂анʼажа ′ӱ̄гылмытыт.</ta>
            <ta e="T16" id="Seg_136" s="T13">′модʼитдъ тебе′ɣум ′тша̄жътдъ.</ta>
            <ta e="T23" id="Seg_137" s="T16">а оkkыр па′jага та′каjа ′тʼарым, на′ссел ′палʼдин.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_138" s="T1">qumnaː quroːlǯa.</ta>
            <ta e="T8" id="Seg_139" s="T4">a ne̹jqum tʼötǯiqan tšaːʒɨn.</ta>
            <ta e="T13" id="Seg_140" s="T8">a oqqɨr ne̹jɣum qɨːb̂anʼaʒa üːgɨlmɨtɨt.</ta>
            <ta e="T16" id="Seg_141" s="T13">modʼitdə tebeɣum tšaːʒətdə.</ta>
            <ta e="T23" id="Seg_142" s="T16">a oqqɨr pajaga takaja tʼarɨm, nassel palʼdin.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_143" s="T1">Qum naː quroːlǯa. </ta>
            <ta e="T8" id="Seg_144" s="T4">A nejqum tʼötǯiqan čaːʒɨn. </ta>
            <ta e="T13" id="Seg_145" s="T8">A oqqɨr nejɣum qɨːbanʼaʒa üːgɨlmɨtɨt. </ta>
            <ta e="T16" id="Seg_146" s="T13">Modʼitdə tebeɣum čaːʒətdə. </ta>
            <ta e="T23" id="Seg_147" s="T16">A oqqɨr pajaga takaja tʼarɨm, nassel palʼdin. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_148" s="T1">qum</ta>
            <ta e="T3" id="Seg_149" s="T2">naː</ta>
            <ta e="T4" id="Seg_150" s="T3">qur-oːl-ǯa</ta>
            <ta e="T5" id="Seg_151" s="T4">a</ta>
            <ta e="T6" id="Seg_152" s="T5">ne-j-qum</ta>
            <ta e="T7" id="Seg_153" s="T6">tʼötǯiqan</ta>
            <ta e="T8" id="Seg_154" s="T7">čaːʒɨ-n</ta>
            <ta e="T9" id="Seg_155" s="T8">a</ta>
            <ta e="T10" id="Seg_156" s="T9">oqqɨr</ta>
            <ta e="T11" id="Seg_157" s="T10">ne-j-ɣum</ta>
            <ta e="T12" id="Seg_158" s="T11">qɨːbanʼaʒa</ta>
            <ta e="T13" id="Seg_159" s="T12">üː-gɨl-mɨ-tɨ-t</ta>
            <ta e="T14" id="Seg_160" s="T13">modʼi-tdə</ta>
            <ta e="T15" id="Seg_161" s="T14">tebe-ɣum</ta>
            <ta e="T16" id="Seg_162" s="T15">čaːʒə-tdə</ta>
            <ta e="T17" id="Seg_163" s="T16">a</ta>
            <ta e="T18" id="Seg_164" s="T17">oqqɨr</ta>
            <ta e="T19" id="Seg_165" s="T18">paja-ga</ta>
            <ta e="T21" id="Seg_166" s="T20">tʼarɨm</ta>
            <ta e="T22" id="Seg_167" s="T21">nassel</ta>
            <ta e="T23" id="Seg_168" s="T22">palʼdi-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_169" s="T1">qum</ta>
            <ta e="T3" id="Seg_170" s="T2">na</ta>
            <ta e="T4" id="Seg_171" s="T3">kur-ol-ntɨ</ta>
            <ta e="T5" id="Seg_172" s="T4">a</ta>
            <ta e="T6" id="Seg_173" s="T5">ne-lʼ-qum</ta>
            <ta e="T7" id="Seg_174" s="T6">tʼötʒɨkaŋ</ta>
            <ta e="T8" id="Seg_175" s="T7">čaǯɨ-n</ta>
            <ta e="T9" id="Seg_176" s="T8">a</ta>
            <ta e="T10" id="Seg_177" s="T9">okkɨr</ta>
            <ta e="T11" id="Seg_178" s="T10">ne-lʼ-qum</ta>
            <ta e="T12" id="Seg_179" s="T11">qɨbanʼaǯa</ta>
            <ta e="T13" id="Seg_180" s="T12">ü-qɨl-mbɨ-ntɨ-t</ta>
            <ta e="T14" id="Seg_181" s="T13">moːdʼi-ntɨ</ta>
            <ta e="T15" id="Seg_182" s="T14">täbe-qum</ta>
            <ta e="T16" id="Seg_183" s="T15">čaǯɨ-ntɨ</ta>
            <ta e="T17" id="Seg_184" s="T16">a</ta>
            <ta e="T18" id="Seg_185" s="T17">okkɨr</ta>
            <ta e="T19" id="Seg_186" s="T18">paja-ka</ta>
            <ta e="T21" id="Seg_187" s="T20">tʼarɨm</ta>
            <ta e="T22" id="Seg_188" s="T21">naseːl</ta>
            <ta e="T23" id="Seg_189" s="T22">palʼdʼi-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_190" s="T1">human.being.[NOM]</ta>
            <ta e="T3" id="Seg_191" s="T2">here</ta>
            <ta e="T4" id="Seg_192" s="T3">run-MOM-INFER.[3SG.S]</ta>
            <ta e="T5" id="Seg_193" s="T4">and</ta>
            <ta e="T6" id="Seg_194" s="T5">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T7" id="Seg_195" s="T6">slowly</ta>
            <ta e="T8" id="Seg_196" s="T7">go-3SG.S</ta>
            <ta e="T9" id="Seg_197" s="T8">and</ta>
            <ta e="T10" id="Seg_198" s="T9">one</ta>
            <ta e="T11" id="Seg_199" s="T10">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T12" id="Seg_200" s="T11">child.[NOM]</ta>
            <ta e="T13" id="Seg_201" s="T12">pull-DRV-DUR-INFER-3SG.O</ta>
            <ta e="T14" id="Seg_202" s="T13">limp-PTCP.PRS.[NOM]</ta>
            <ta e="T15" id="Seg_203" s="T14">man-human.being.[NOM]</ta>
            <ta e="T16" id="Seg_204" s="T15">go-INFER.[3SG.S]</ta>
            <ta e="T17" id="Seg_205" s="T16">and</ta>
            <ta e="T18" id="Seg_206" s="T17">one</ta>
            <ta e="T19" id="Seg_207" s="T18">old.woman-DIM.[NOM]</ta>
            <ta e="T21" id="Seg_208" s="T20">thick</ta>
            <ta e="T22" id="Seg_209" s="T21">hardly</ta>
            <ta e="T23" id="Seg_210" s="T22">walk-3SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_211" s="T1">человек.[NOM]</ta>
            <ta e="T3" id="Seg_212" s="T2">ну</ta>
            <ta e="T4" id="Seg_213" s="T3">бегать-MOM-INFER.[3SG.S]</ta>
            <ta e="T5" id="Seg_214" s="T4">а</ta>
            <ta e="T6" id="Seg_215" s="T5">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T7" id="Seg_216" s="T6">медленно</ta>
            <ta e="T8" id="Seg_217" s="T7">ходить-3SG.S</ta>
            <ta e="T9" id="Seg_218" s="T8">а</ta>
            <ta e="T10" id="Seg_219" s="T9">один</ta>
            <ta e="T11" id="Seg_220" s="T10">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T12" id="Seg_221" s="T11">ребенок.[NOM]</ta>
            <ta e="T13" id="Seg_222" s="T12">тащить-DRV-DUR-INFER-3SG.O</ta>
            <ta e="T14" id="Seg_223" s="T13">хромать-PTCP.PRS.[NOM]</ta>
            <ta e="T15" id="Seg_224" s="T14">мужчина-человек.[NOM]</ta>
            <ta e="T16" id="Seg_225" s="T15">ходить-INFER.[3SG.S]</ta>
            <ta e="T17" id="Seg_226" s="T16">а</ta>
            <ta e="T18" id="Seg_227" s="T17">один</ta>
            <ta e="T19" id="Seg_228" s="T18">старуха-DIM.[NOM]</ta>
            <ta e="T21" id="Seg_229" s="T20">толстый</ta>
            <ta e="T22" id="Seg_230" s="T21">насилу</ta>
            <ta e="T23" id="Seg_231" s="T22">ходить-3SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_232" s="T1">n.[n:case]</ta>
            <ta e="T3" id="Seg_233" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_234" s="T3">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T5" id="Seg_235" s="T4">conj</ta>
            <ta e="T6" id="Seg_236" s="T5">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T7" id="Seg_237" s="T6">adv</ta>
            <ta e="T8" id="Seg_238" s="T7">v-v:pn</ta>
            <ta e="T9" id="Seg_239" s="T8">conj</ta>
            <ta e="T10" id="Seg_240" s="T9">num</ta>
            <ta e="T11" id="Seg_241" s="T10">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T12" id="Seg_242" s="T11">n.[n:case]</ta>
            <ta e="T13" id="Seg_243" s="T12">v-v&gt;v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T14" id="Seg_244" s="T13">v-v&gt;ptcp.[n:case]</ta>
            <ta e="T15" id="Seg_245" s="T14">n-n.[n:case]</ta>
            <ta e="T16" id="Seg_246" s="T15">v-v:mood.[v:pn]</ta>
            <ta e="T17" id="Seg_247" s="T16">conj</ta>
            <ta e="T18" id="Seg_248" s="T17">num</ta>
            <ta e="T19" id="Seg_249" s="T18">n-n&gt;n.[n:case]</ta>
            <ta e="T21" id="Seg_250" s="T20">adj</ta>
            <ta e="T22" id="Seg_251" s="T21">adv</ta>
            <ta e="T23" id="Seg_252" s="T22">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_253" s="T1">n</ta>
            <ta e="T3" id="Seg_254" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_255" s="T3">v</ta>
            <ta e="T5" id="Seg_256" s="T4">conj</ta>
            <ta e="T6" id="Seg_257" s="T5">n</ta>
            <ta e="T7" id="Seg_258" s="T6">adv</ta>
            <ta e="T8" id="Seg_259" s="T7">v</ta>
            <ta e="T9" id="Seg_260" s="T8">conj</ta>
            <ta e="T10" id="Seg_261" s="T9">num</ta>
            <ta e="T11" id="Seg_262" s="T10">n</ta>
            <ta e="T12" id="Seg_263" s="T11">n</ta>
            <ta e="T13" id="Seg_264" s="T12">v</ta>
            <ta e="T14" id="Seg_265" s="T13">ptcp</ta>
            <ta e="T15" id="Seg_266" s="T14">n</ta>
            <ta e="T16" id="Seg_267" s="T15">v</ta>
            <ta e="T17" id="Seg_268" s="T16">conj</ta>
            <ta e="T18" id="Seg_269" s="T17">num</ta>
            <ta e="T19" id="Seg_270" s="T18">n</ta>
            <ta e="T21" id="Seg_271" s="T20">adj</ta>
            <ta e="T22" id="Seg_272" s="T21">adv</ta>
            <ta e="T23" id="Seg_273" s="T22">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_274" s="T1">np.h:A</ta>
            <ta e="T6" id="Seg_275" s="T5">np.h:A</ta>
            <ta e="T11" id="Seg_276" s="T10">np.h:A</ta>
            <ta e="T12" id="Seg_277" s="T11">np.h:Th</ta>
            <ta e="T15" id="Seg_278" s="T14">np.h:A</ta>
            <ta e="T19" id="Seg_279" s="T18">np.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_280" s="T1">np.h:S</ta>
            <ta e="T4" id="Seg_281" s="T3">v:pred</ta>
            <ta e="T6" id="Seg_282" s="T5">np.h:S</ta>
            <ta e="T8" id="Seg_283" s="T7">v:pred</ta>
            <ta e="T11" id="Seg_284" s="T10">np.h:S</ta>
            <ta e="T12" id="Seg_285" s="T11">np.h:O</ta>
            <ta e="T13" id="Seg_286" s="T12">v:pred</ta>
            <ta e="T15" id="Seg_287" s="T14">np.h:S</ta>
            <ta e="T16" id="Seg_288" s="T15">v:pred</ta>
            <ta e="T19" id="Seg_289" s="T18">np.h:S</ta>
            <ta e="T23" id="Seg_290" s="T22">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_291" s="T4">RUS:gram</ta>
            <ta e="T9" id="Seg_292" s="T8">RUS:gram</ta>
            <ta e="T17" id="Seg_293" s="T16">RUS:gram</ta>
            <ta e="T22" id="Seg_294" s="T21">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_295" s="T1">A man set off running.</ta>
            <ta e="T8" id="Seg_296" s="T4">And a woman walked slowly.</ta>
            <ta e="T13" id="Seg_297" s="T8">And one woman is carrying a child.</ta>
            <ta e="T16" id="Seg_298" s="T13">A lame man is coming.</ta>
            <ta e="T23" id="Seg_299" s="T16">And one thick old man is hardly walking.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_300" s="T1">Ein Mann rannte los.</ta>
            <ta e="T8" id="Seg_301" s="T4">Und eine Frau ging langsam.</ta>
            <ta e="T13" id="Seg_302" s="T8">Und eine Frau trägt ein Kind.</ta>
            <ta e="T16" id="Seg_303" s="T13">Ein lahmer Mann geht.</ta>
            <ta e="T23" id="Seg_304" s="T16">Und ein dicker alter Mann läuft kaum.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_305" s="T1">Человек побежал.</ta>
            <ta e="T8" id="Seg_306" s="T4">А женщина помаленьку пошла.</ta>
            <ta e="T13" id="Seg_307" s="T8">А одна женщина ребенка несет.</ta>
            <ta e="T16" id="Seg_308" s="T13">Хромой человек идет.</ta>
            <ta e="T23" id="Seg_309" s="T16">А одна старуха, такая толстая, кое-как ходит.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_310" s="T1">человек побежал</ta>
            <ta e="T8" id="Seg_311" s="T4">а женщина помаленьку пошла</ta>
            <ta e="T13" id="Seg_312" s="T8">одна женщина ребенка несет</ta>
            <ta e="T16" id="Seg_313" s="T13">хромой человек идет</ta>
            <ta e="T23" id="Seg_314" s="T16">а одна старуха такая толстая кое-как ходит</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T4" id="Seg_315" s="T1">[BrM:] 'Qumnaː' changed to 'Qum naː'. [BrM:] INFER?</ta>
            <ta e="T13" id="Seg_316" s="T8">[BrM:] Tentative analysis of 'üːgɨlmɨtɨt'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
