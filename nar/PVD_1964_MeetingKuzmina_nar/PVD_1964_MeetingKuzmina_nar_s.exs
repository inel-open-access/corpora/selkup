<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_MeetingKuzmina_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_MeetingKuzmina_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">165</ud-information>
            <ud-information attribute-name="# HIAT:w">118</ud-information>
            <ud-information attribute-name="# e">118</ud-information>
            <ud-information attribute-name="# HIAT:u">25</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T119" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">amdɨzan</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">sormeǯele</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Qajda</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">nejɣum</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">mennɨtə</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">Mekga</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">tʼäratta</ts>
                  <nts id="Seg_32" n="HIAT:ip">,</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">tau</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">Jewlaškan</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">pajat</ts>
                  <nts id="Seg_42" n="HIAT:ip">.</nts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_45" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">Man</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">tʼütdə</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">kuroːnnan</ts>
                  <nts id="Seg_54" n="HIAT:ip">.</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_57" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">Tʼüwan</ts>
                  <nts id="Seg_60" n="HIAT:ip">,</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_63" n="HIAT:w" s="T16">tʼölomčan</ts>
                  <nts id="Seg_64" n="HIAT:ip">.</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_67" n="HIAT:u" s="T17">
                  <nts id="Seg_68" n="HIAT:ip">(</nts>
                  <ts e="T18" id="Seg_70" n="HIAT:w" s="T17">Man</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_73" n="HIAT:w" s="T18">tepse</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_76" n="HIAT:w" s="T19">tʼölomčezan</ts>
                  <nts id="Seg_77" n="HIAT:ip">)</nts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_81" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_83" n="HIAT:w" s="T20">Man</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_86" n="HIAT:w" s="T21">tʼäran</ts>
                  <nts id="Seg_87" n="HIAT:ip">:</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_89" n="HIAT:ip">“</nts>
                  <ts e="T23" id="Seg_91" n="HIAT:w" s="T22">Tau</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_94" n="HIAT:w" s="T23">qaj</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_97" n="HIAT:w" s="T24">Jewlaškan</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_100" n="HIAT:w" s="T25">pajat</ts>
                  <nts id="Seg_101" n="HIAT:ip">?</nts>
                  <nts id="Seg_102" n="HIAT:ip">”</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_105" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_107" n="HIAT:w" s="T26">A</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_110" n="HIAT:w" s="T27">tebla</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_113" n="HIAT:w" s="T28">mekga</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_116" n="HIAT:w" s="T29">tʼärattə</ts>
                  <nts id="Seg_117" n="HIAT:ip">:</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_119" n="HIAT:ip">“</nts>
                  <ts e="T31" id="Seg_121" n="HIAT:w" s="T30">As</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_124" n="HIAT:w" s="T31">Jewlaškan</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_127" n="HIAT:w" s="T32">pajat</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_130" n="HIAT:w" s="T33">tʼütdə</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip">”</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_135" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_137" n="HIAT:w" s="T34">A</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_140" n="HIAT:w" s="T35">mekga</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_143" n="HIAT:w" s="T36">tʼärzat</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_146" n="HIAT:w" s="T37">što</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_149" n="HIAT:w" s="T38">Jewlaškin</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_152" n="HIAT:w" s="T39">pajat</ts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_156" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_158" n="HIAT:w" s="T40">I</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_161" n="HIAT:w" s="T41">man</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_164" n="HIAT:w" s="T42">i</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_167" n="HIAT:w" s="T43">kurlʼe</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_170" n="HIAT:w" s="T44">koššedʼän</ts>
                  <nts id="Seg_171" n="HIAT:ip">.</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_174" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_176" n="HIAT:w" s="T45">Tebla</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_179" n="HIAT:w" s="T46">mekga</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_182" n="HIAT:w" s="T47">sedʼäptəbat</ts>
                  <nts id="Seg_183" n="HIAT:ip">.</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_186" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_188" n="HIAT:w" s="T48">A</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_191" n="HIAT:w" s="T49">tau</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_194" n="HIAT:w" s="T50">Angelina</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_197" n="HIAT:w" s="T51">Iwanowna</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_200" n="HIAT:w" s="T52">tʼümatda</ts>
                  <nts id="Seg_201" n="HIAT:ip">.</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_204" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_206" n="HIAT:w" s="T53">Amda</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_209" n="HIAT:w" s="T54">qwɛlə</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_212" n="HIAT:w" s="T55">auərlʼe</ts>
                  <nts id="Seg_213" n="HIAT:ip">.</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_216" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_218" n="HIAT:w" s="T56">A</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_221" n="HIAT:w" s="T57">man</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_224" n="HIAT:w" s="T58">tebne</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_227" n="HIAT:w" s="T59">soːgədʼzʼan</ts>
                  <nts id="Seg_228" n="HIAT:ip">:</nts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_230" n="HIAT:ip">“</nts>
                  <ts e="T61" id="Seg_232" n="HIAT:w" s="T60">Tan</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_235" n="HIAT:w" s="T61">kuː</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_238" n="HIAT:w" s="T62">čaʒatdə</ts>
                  <nts id="Seg_239" n="HIAT:ip">?</nts>
                  <nts id="Seg_240" n="HIAT:ip">”</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_243" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_245" n="HIAT:w" s="T63">A</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_248" n="HIAT:w" s="T64">tep</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_251" n="HIAT:w" s="T65">tʼarɨn</ts>
                  <nts id="Seg_252" n="HIAT:ip">:</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_254" n="HIAT:ip">“</nts>
                  <ts e="T67" id="Seg_256" n="HIAT:w" s="T66">Man</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_259" n="HIAT:w" s="T67">taftʼet</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_262" n="HIAT:w" s="T68">tʼüan</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_265" n="HIAT:w" s="T69">täɣɨtdɨltə</ts>
                  <nts id="Seg_266" n="HIAT:ip">,</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_269" n="HIAT:w" s="T70">eretdə</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_272" n="HIAT:w" s="T71">qutdə</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_275" n="HIAT:w" s="T72">warkeǯan</ts>
                  <nts id="Seg_276" n="HIAT:ip">.</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_279" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_281" n="HIAT:w" s="T73">Man</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_284" n="HIAT:w" s="T74">teɣɨtdɨltə</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_287" n="HIAT:w" s="T75">tʼüan</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_290" n="HIAT:w" s="T76">sʼüsöɣɨ</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_293" n="HIAT:w" s="T77">ɣeʒlam</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_296" n="HIAT:w" s="T78">nagɨrlʼe</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_299" n="HIAT:w" s="T79">tadɨreǯau</ts>
                  <nts id="Seg_300" n="HIAT:ip">.</nts>
                  <nts id="Seg_301" n="HIAT:ip">”</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_304" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_306" n="HIAT:w" s="T80">A</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_309" n="HIAT:w" s="T81">man</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_312" n="HIAT:w" s="T82">tʼaran</ts>
                  <nts id="Seg_313" n="HIAT:ip">,</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_316" n="HIAT:w" s="T83">mekga</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_319" n="HIAT:w" s="T84">dʼäja</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_322" n="HIAT:w" s="T85">Mixajla</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_325" n="HIAT:w" s="T86">tʼärɨn</ts>
                  <nts id="Seg_326" n="HIAT:ip">:</nts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_328" n="HIAT:ip">“</nts>
                  <ts e="T88" id="Seg_330" n="HIAT:w" s="T87">Piːrʼeɣətdə</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_333" n="HIAT:w" s="T88">qwɛːrət</ts>
                  <nts id="Seg_334" n="HIAT:ip">.</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_337" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_339" n="HIAT:w" s="T89">Tan</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_342" n="HIAT:w" s="T90">onetdə</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_345" n="HIAT:w" s="T91">jewatdə</ts>
                  <nts id="Seg_346" n="HIAT:ip">.</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_349" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_351" n="HIAT:w" s="T92">Tan</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_354" n="HIAT:w" s="T93">pelkattə</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_357" n="HIAT:w" s="T94">jewat</ts>
                  <nts id="Seg_358" n="HIAT:ip">.</nts>
                  <nts id="Seg_359" n="HIAT:ip">”</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_362" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_364" n="HIAT:w" s="T95">Man</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_367" n="HIAT:w" s="T96">tebɨm</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_370" n="HIAT:w" s="T97">pʼirʼäɣän</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_373" n="HIAT:w" s="T98">qwɛrau</ts>
                  <nts id="Seg_374" n="HIAT:ip">.</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_377" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_379" n="HIAT:w" s="T99">Täp</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_382" n="HIAT:w" s="T100">mekga</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_385" n="HIAT:w" s="T101">tʼüa</ts>
                  <nts id="Seg_386" n="HIAT:ip">,</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_389" n="HIAT:w" s="T102">manan</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_392" n="HIAT:w" s="T103">warkɨs</ts>
                  <nts id="Seg_393" n="HIAT:ip">.</nts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_396" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_398" n="HIAT:w" s="T104">A</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_401" n="HIAT:w" s="T105">teper</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_404" n="HIAT:w" s="T106">man</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_407" n="HIAT:w" s="T107">tebnan</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_410" n="HIAT:w" s="T108">warkan</ts>
                  <nts id="Seg_411" n="HIAT:ip">.</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_414" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_416" n="HIAT:w" s="T109">Man</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_419" n="HIAT:w" s="T110">kwaǯan</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_422" n="HIAT:w" s="T111">Satdəjedotdə</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_425" n="HIAT:w" s="T112">i</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_428" n="HIAT:w" s="T113">qwälɨtʼeǯan</ts>
                  <nts id="Seg_429" n="HIAT:ip">.</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_432" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_434" n="HIAT:w" s="T114">Tʼeːkga</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_437" n="HIAT:w" s="T115">qwɛl</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_440" n="HIAT:w" s="T116">üdetʒan</ts>
                  <nts id="Seg_441" n="HIAT:ip">.</nts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_444" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_446" n="HIAT:w" s="T117">Tan</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_449" n="HIAT:w" s="T118">amǯal</ts>
                  <nts id="Seg_450" n="HIAT:ip">.</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T119" id="Seg_452" n="sc" s="T1">
               <ts e="T2" id="Seg_454" n="e" s="T1">Man </ts>
               <ts e="T3" id="Seg_456" n="e" s="T2">amdɨzan </ts>
               <ts e="T4" id="Seg_458" n="e" s="T3">sormeǯele. </ts>
               <ts e="T5" id="Seg_460" n="e" s="T4">Qajda </ts>
               <ts e="T6" id="Seg_462" n="e" s="T5">nejɣum </ts>
               <ts e="T7" id="Seg_464" n="e" s="T6">mennɨtə. </ts>
               <ts e="T8" id="Seg_466" n="e" s="T7">Mekga </ts>
               <ts e="T9" id="Seg_468" n="e" s="T8">tʼäratta, </ts>
               <ts e="T10" id="Seg_470" n="e" s="T9">tau </ts>
               <ts e="T11" id="Seg_472" n="e" s="T10">Jewlaškan </ts>
               <ts e="T12" id="Seg_474" n="e" s="T11">pajat. </ts>
               <ts e="T13" id="Seg_476" n="e" s="T12">Man </ts>
               <ts e="T14" id="Seg_478" n="e" s="T13">tʼütdə </ts>
               <ts e="T15" id="Seg_480" n="e" s="T14">kuroːnnan. </ts>
               <ts e="T16" id="Seg_482" n="e" s="T15">Tʼüwan, </ts>
               <ts e="T17" id="Seg_484" n="e" s="T16">tʼölomčan. </ts>
               <ts e="T18" id="Seg_486" n="e" s="T17">(Man </ts>
               <ts e="T19" id="Seg_488" n="e" s="T18">tepse </ts>
               <ts e="T20" id="Seg_490" n="e" s="T19">tʼölomčezan). </ts>
               <ts e="T21" id="Seg_492" n="e" s="T20">Man </ts>
               <ts e="T22" id="Seg_494" n="e" s="T21">tʼäran: </ts>
               <ts e="T23" id="Seg_496" n="e" s="T22">“Tau </ts>
               <ts e="T24" id="Seg_498" n="e" s="T23">qaj </ts>
               <ts e="T25" id="Seg_500" n="e" s="T24">Jewlaškan </ts>
               <ts e="T26" id="Seg_502" n="e" s="T25">pajat?” </ts>
               <ts e="T27" id="Seg_504" n="e" s="T26">A </ts>
               <ts e="T28" id="Seg_506" n="e" s="T27">tebla </ts>
               <ts e="T29" id="Seg_508" n="e" s="T28">mekga </ts>
               <ts e="T30" id="Seg_510" n="e" s="T29">tʼärattə: </ts>
               <ts e="T31" id="Seg_512" n="e" s="T30">“As </ts>
               <ts e="T32" id="Seg_514" n="e" s="T31">Jewlaškan </ts>
               <ts e="T33" id="Seg_516" n="e" s="T32">pajat </ts>
               <ts e="T34" id="Seg_518" n="e" s="T33">tʼütdə.” </ts>
               <ts e="T35" id="Seg_520" n="e" s="T34">A </ts>
               <ts e="T36" id="Seg_522" n="e" s="T35">mekga </ts>
               <ts e="T37" id="Seg_524" n="e" s="T36">tʼärzat </ts>
               <ts e="T38" id="Seg_526" n="e" s="T37">što </ts>
               <ts e="T39" id="Seg_528" n="e" s="T38">Jewlaškin </ts>
               <ts e="T40" id="Seg_530" n="e" s="T39">pajat. </ts>
               <ts e="T41" id="Seg_532" n="e" s="T40">I </ts>
               <ts e="T42" id="Seg_534" n="e" s="T41">man </ts>
               <ts e="T43" id="Seg_536" n="e" s="T42">i </ts>
               <ts e="T44" id="Seg_538" n="e" s="T43">kurlʼe </ts>
               <ts e="T45" id="Seg_540" n="e" s="T44">koššedʼän. </ts>
               <ts e="T46" id="Seg_542" n="e" s="T45">Tebla </ts>
               <ts e="T47" id="Seg_544" n="e" s="T46">mekga </ts>
               <ts e="T48" id="Seg_546" n="e" s="T47">sedʼäptəbat. </ts>
               <ts e="T49" id="Seg_548" n="e" s="T48">A </ts>
               <ts e="T50" id="Seg_550" n="e" s="T49">tau </ts>
               <ts e="T51" id="Seg_552" n="e" s="T50">Angelina </ts>
               <ts e="T52" id="Seg_554" n="e" s="T51">Iwanowna </ts>
               <ts e="T53" id="Seg_556" n="e" s="T52">tʼümatda. </ts>
               <ts e="T54" id="Seg_558" n="e" s="T53">Amda </ts>
               <ts e="T55" id="Seg_560" n="e" s="T54">qwɛlə </ts>
               <ts e="T56" id="Seg_562" n="e" s="T55">auərlʼe. </ts>
               <ts e="T57" id="Seg_564" n="e" s="T56">A </ts>
               <ts e="T58" id="Seg_566" n="e" s="T57">man </ts>
               <ts e="T59" id="Seg_568" n="e" s="T58">tebne </ts>
               <ts e="T60" id="Seg_570" n="e" s="T59">soːgədʼzʼan: </ts>
               <ts e="T61" id="Seg_572" n="e" s="T60">“Tan </ts>
               <ts e="T62" id="Seg_574" n="e" s="T61">kuː </ts>
               <ts e="T63" id="Seg_576" n="e" s="T62">čaʒatdə?” </ts>
               <ts e="T64" id="Seg_578" n="e" s="T63">A </ts>
               <ts e="T65" id="Seg_580" n="e" s="T64">tep </ts>
               <ts e="T66" id="Seg_582" n="e" s="T65">tʼarɨn: </ts>
               <ts e="T67" id="Seg_584" n="e" s="T66">“Man </ts>
               <ts e="T68" id="Seg_586" n="e" s="T67">taftʼet </ts>
               <ts e="T69" id="Seg_588" n="e" s="T68">tʼüan </ts>
               <ts e="T70" id="Seg_590" n="e" s="T69">täɣɨtdɨltə, </ts>
               <ts e="T71" id="Seg_592" n="e" s="T70">eretdə </ts>
               <ts e="T72" id="Seg_594" n="e" s="T71">qutdə </ts>
               <ts e="T73" id="Seg_596" n="e" s="T72">warkeǯan. </ts>
               <ts e="T74" id="Seg_598" n="e" s="T73">Man </ts>
               <ts e="T75" id="Seg_600" n="e" s="T74">teɣɨtdɨltə </ts>
               <ts e="T76" id="Seg_602" n="e" s="T75">tʼüan </ts>
               <ts e="T77" id="Seg_604" n="e" s="T76">sʼüsöɣɨ </ts>
               <ts e="T78" id="Seg_606" n="e" s="T77">ɣeʒlam </ts>
               <ts e="T79" id="Seg_608" n="e" s="T78">nagɨrlʼe </ts>
               <ts e="T80" id="Seg_610" n="e" s="T79">tadɨreǯau.” </ts>
               <ts e="T81" id="Seg_612" n="e" s="T80">A </ts>
               <ts e="T82" id="Seg_614" n="e" s="T81">man </ts>
               <ts e="T83" id="Seg_616" n="e" s="T82">tʼaran, </ts>
               <ts e="T84" id="Seg_618" n="e" s="T83">mekga </ts>
               <ts e="T85" id="Seg_620" n="e" s="T84">dʼäja </ts>
               <ts e="T86" id="Seg_622" n="e" s="T85">Mixajla </ts>
               <ts e="T87" id="Seg_624" n="e" s="T86">tʼärɨn: </ts>
               <ts e="T88" id="Seg_626" n="e" s="T87">“Piːrʼeɣətdə </ts>
               <ts e="T89" id="Seg_628" n="e" s="T88">qwɛːrət. </ts>
               <ts e="T90" id="Seg_630" n="e" s="T89">Tan </ts>
               <ts e="T91" id="Seg_632" n="e" s="T90">onetdə </ts>
               <ts e="T92" id="Seg_634" n="e" s="T91">jewatdə. </ts>
               <ts e="T93" id="Seg_636" n="e" s="T92">Tan </ts>
               <ts e="T94" id="Seg_638" n="e" s="T93">pelkattə </ts>
               <ts e="T95" id="Seg_640" n="e" s="T94">jewat.” </ts>
               <ts e="T96" id="Seg_642" n="e" s="T95">Man </ts>
               <ts e="T97" id="Seg_644" n="e" s="T96">tebɨm </ts>
               <ts e="T98" id="Seg_646" n="e" s="T97">pʼirʼäɣän </ts>
               <ts e="T99" id="Seg_648" n="e" s="T98">qwɛrau. </ts>
               <ts e="T100" id="Seg_650" n="e" s="T99">Täp </ts>
               <ts e="T101" id="Seg_652" n="e" s="T100">mekga </ts>
               <ts e="T102" id="Seg_654" n="e" s="T101">tʼüa, </ts>
               <ts e="T103" id="Seg_656" n="e" s="T102">manan </ts>
               <ts e="T104" id="Seg_658" n="e" s="T103">warkɨs. </ts>
               <ts e="T105" id="Seg_660" n="e" s="T104">A </ts>
               <ts e="T106" id="Seg_662" n="e" s="T105">teper </ts>
               <ts e="T107" id="Seg_664" n="e" s="T106">man </ts>
               <ts e="T108" id="Seg_666" n="e" s="T107">tebnan </ts>
               <ts e="T109" id="Seg_668" n="e" s="T108">warkan. </ts>
               <ts e="T110" id="Seg_670" n="e" s="T109">Man </ts>
               <ts e="T111" id="Seg_672" n="e" s="T110">kwaǯan </ts>
               <ts e="T112" id="Seg_674" n="e" s="T111">Satdəjedotdə </ts>
               <ts e="T113" id="Seg_676" n="e" s="T112">i </ts>
               <ts e="T114" id="Seg_678" n="e" s="T113">qwälɨtʼeǯan. </ts>
               <ts e="T115" id="Seg_680" n="e" s="T114">Tʼeːkga </ts>
               <ts e="T116" id="Seg_682" n="e" s="T115">qwɛl </ts>
               <ts e="T117" id="Seg_684" n="e" s="T116">üdetʒan. </ts>
               <ts e="T118" id="Seg_686" n="e" s="T117">Tan </ts>
               <ts e="T119" id="Seg_688" n="e" s="T118">amǯal. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_689" s="T1">PVD_1964_MeetingKuzmina_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_690" s="T4">PVD_1964_MeetingKuzmina_nar.002 (001.002)</ta>
            <ta e="T12" id="Seg_691" s="T7">PVD_1964_MeetingKuzmina_nar.003 (001.003)</ta>
            <ta e="T15" id="Seg_692" s="T12">PVD_1964_MeetingKuzmina_nar.004 (001.004)</ta>
            <ta e="T17" id="Seg_693" s="T15">PVD_1964_MeetingKuzmina_nar.005 (001.005)</ta>
            <ta e="T20" id="Seg_694" s="T17">PVD_1964_MeetingKuzmina_nar.006 (001.006)</ta>
            <ta e="T26" id="Seg_695" s="T20">PVD_1964_MeetingKuzmina_nar.007 (001.007)</ta>
            <ta e="T34" id="Seg_696" s="T26">PVD_1964_MeetingKuzmina_nar.008 (001.008)</ta>
            <ta e="T40" id="Seg_697" s="T34">PVD_1964_MeetingKuzmina_nar.009 (001.009)</ta>
            <ta e="T45" id="Seg_698" s="T40">PVD_1964_MeetingKuzmina_nar.010 (001.010)</ta>
            <ta e="T48" id="Seg_699" s="T45">PVD_1964_MeetingKuzmina_nar.011 (001.011)</ta>
            <ta e="T53" id="Seg_700" s="T48">PVD_1964_MeetingKuzmina_nar.012 (001.012)</ta>
            <ta e="T56" id="Seg_701" s="T53">PVD_1964_MeetingKuzmina_nar.013 (001.013)</ta>
            <ta e="T63" id="Seg_702" s="T56">PVD_1964_MeetingKuzmina_nar.014 (001.014)</ta>
            <ta e="T73" id="Seg_703" s="T63">PVD_1964_MeetingKuzmina_nar.015 (001.015)</ta>
            <ta e="T80" id="Seg_704" s="T73">PVD_1964_MeetingKuzmina_nar.016 (001.016)</ta>
            <ta e="T89" id="Seg_705" s="T80">PVD_1964_MeetingKuzmina_nar.017 (001.017)</ta>
            <ta e="T92" id="Seg_706" s="T89">PVD_1964_MeetingKuzmina_nar.018 (001.018)</ta>
            <ta e="T95" id="Seg_707" s="T92">PVD_1964_MeetingKuzmina_nar.019 (001.019)</ta>
            <ta e="T99" id="Seg_708" s="T95">PVD_1964_MeetingKuzmina_nar.020 (001.020)</ta>
            <ta e="T104" id="Seg_709" s="T99">PVD_1964_MeetingKuzmina_nar.021 (001.021)</ta>
            <ta e="T109" id="Seg_710" s="T104">PVD_1964_MeetingKuzmina_nar.022 (001.022)</ta>
            <ta e="T114" id="Seg_711" s="T109">PVD_1964_MeetingKuzmina_nar.023 (001.023)</ta>
            <ta e="T117" id="Seg_712" s="T114">PVD_1964_MeetingKuzmina_nar.024 (001.024)</ta>
            <ta e="T119" id="Seg_713" s="T117">PVD_1964_MeetingKuzmina_nar.025 (001.025)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_714" s="T1">ман ′амдызан сор′меджеlе.</ta>
            <ta e="T7" id="Seg_715" s="T4">kайда ′не̨й‵ɣум ′меннытъ(а).</ta>
            <ta e="T12" id="Seg_716" s="T7">′мекг̂а тʼӓ′ратта, ′тау jеw′лашкан па′jат.</ta>
            <ta e="T15" id="Seg_717" s="T12">ман ′тʼӱтдъ ку′ро̄ннан.</ta>
            <ta e="T17" id="Seg_718" s="T15">′тʼӱwан, тʼӧ′ломтшан.</ta>
            <ta e="T20" id="Seg_719" s="T17">(ман теп′се тʼӧ′ломтше‵зан.)</ta>
            <ta e="T26" id="Seg_720" s="T20">ман тʼӓ′ран, ′тау kай jеw′лашка(ы)н па′jат.</ta>
            <ta e="T34" id="Seg_721" s="T26">а теб′ла мекг̂а тʼӓ′раттъ, ас jеw′лашкан паjат тʼӱтдъ.</ta>
            <ta e="T40" id="Seg_722" s="T34">а мекг̂а тʼӓр′зат што jеw′лашкин па′jат.</ta>
            <ta e="T45" id="Seg_723" s="T40">и ман и кур′лʼе ′кошʼшʼе‵дʼӓн.</ta>
            <ta e="T48" id="Seg_724" s="T45">теб′ла ′мекг̂а се′дʼӓптъбат.</ta>
            <ta e="T53" id="Seg_725" s="T48">а ′тау Ангелина Ивановна ′тʼӱматда.</ta>
            <ta e="T56" id="Seg_726" s="T53">амда kwɛ̨lə ′ауəрлʼе.</ta>
            <ta e="T63" id="Seg_727" s="T56">а ман те̨б′не ′со̄гъдʼзʼан: тан кӯ ′тшажатд̂ъ?</ta>
            <ta e="T73" id="Seg_728" s="T63">а те̨п тʼа′рын: ман таф′тʼет тʼӱ′ан ′тӓɣытдылтъ, е′ретдъ ′kутдъ ′варкед̂жан.</ta>
            <ta e="T80" id="Seg_729" s="T73">ман ′теɣытдылтъ тʼӱан сʼӱ′сӧɣы ɣе̨жлам ′нагыр‵лʼе ′тадыре′джау.</ta>
            <ta e="T89" id="Seg_730" s="T80">а ман тʼа′ран, ′мекг̂а д̂ʼӓjа Михайла тʼӓ′рын, ′пӣрʼе(ӓ)ɣътд̂ъ ′kwɛ̄ръ(и)т.</ta>
            <ta e="T92" id="Seg_731" s="T89">тан о′не̨тд̂ъ jеwатд̂ъ.</ta>
            <ta e="T95" id="Seg_732" s="T92">тан пел′к(г̂)аттъ ′jеwат.</ta>
            <ta e="T99" id="Seg_733" s="T95">ман ′те̨бым ′пʼирʼӓɣӓн ′kwɛрау̹.</ta>
            <ta e="T104" id="Seg_734" s="T99">тӓп мекг̂а ′тʼӱа, ма′нан ′w(в)аркыс.</ta>
            <ta e="T109" id="Seg_735" s="T104">а тепер ман те̨б′нан вар′кан.</ta>
            <ta e="T114" id="Seg_736" s="T109">ман кwа′джан ′сатдъ′jедотд̂ъ и kwӓлытʼед̂жан.</ta>
            <ta e="T117" id="Seg_737" s="T114">′тʼе̄кга ′kwɛл ′ӱд̂ет(д̂)жан.</ta>
            <ta e="T119" id="Seg_738" s="T117">′тан ам′джал.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_739" s="T1">man amdɨzan sormeǯele.</ta>
            <ta e="T7" id="Seg_740" s="T4">qajda nejɣum mennɨtə(a).</ta>
            <ta e="T12" id="Seg_741" s="T7">mekĝa tʼäratta, tau jewlaškan pajat.</ta>
            <ta e="T15" id="Seg_742" s="T12">man tʼütdə kuroːnnan.</ta>
            <ta e="T17" id="Seg_743" s="T15">tʼüwan, tʼölomtšan.</ta>
            <ta e="T20" id="Seg_744" s="T17">(man tepse tʼölomtšezan.)</ta>
            <ta e="T26" id="Seg_745" s="T20">man tʼäran, tau qaj jewlaška(ɨ)n pajat.</ta>
            <ta e="T34" id="Seg_746" s="T26">a tebla mekĝa tʼärattə, as jewlaškan pajat tʼütdə.</ta>
            <ta e="T40" id="Seg_747" s="T34">a mekĝa tʼärzat što jewlaškin pajat.</ta>
            <ta e="T45" id="Seg_748" s="T40">i man i kurlʼe koššedʼän.</ta>
            <ta e="T48" id="Seg_749" s="T45">tebla mekĝa sedʼäptəbat.</ta>
            <ta e="T53" id="Seg_750" s="T48">a tau Аngelina Иwanowna tʼümatda.</ta>
            <ta e="T56" id="Seg_751" s="T53">amda qwɛlə auərlʼe.</ta>
            <ta e="T63" id="Seg_752" s="T56">a man tebne soːgədʼzʼan: tan kuː tšaʒatd̂ə?</ta>
            <ta e="T73" id="Seg_753" s="T63">a tep tʼarɨn: man taftʼet tʼüan täɣɨtdɨltə, eretdə qutdə warked̂ʒan.</ta>
            <ta e="T80" id="Seg_754" s="T73">man teɣɨtdɨltə tʼüan sʼüsöɣɨ ɣeʒlam nagɨrlʼe tadɨreǯau.</ta>
            <ta e="T89" id="Seg_755" s="T80">a man tʼaran, mekĝa d̂ʼäja Мixajla tʼärɨn, piːrʼe(ä)ɣətd̂ə qwɛːrə(i)t.</ta>
            <ta e="T92" id="Seg_756" s="T89">tan onetd̂ə jewatd̂ə.</ta>
            <ta e="T95" id="Seg_757" s="T92">tan pelk(ĝ)attə jewat.</ta>
            <ta e="T99" id="Seg_758" s="T95">man tebɨm pʼirʼäɣän qwɛrau̹.</ta>
            <ta e="T104" id="Seg_759" s="T99">täp mekĝa tʼüa, manan w(w)arkɨs.</ta>
            <ta e="T109" id="Seg_760" s="T104">a teper man tebnan warkan.</ta>
            <ta e="T114" id="Seg_761" s="T109">man kwaǯan satdəjedotd̂ə i qwälɨtʼed̂ʒan.</ta>
            <ta e="T117" id="Seg_762" s="T114">tʼeːkga qwɛl üd̂et(d̂)ʒan.</ta>
            <ta e="T119" id="Seg_763" s="T117">tan amǯal.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_764" s="T1">Man amdɨzan sormeǯele. </ta>
            <ta e="T7" id="Seg_765" s="T4">Qajda nejɣum mennɨtə. </ta>
            <ta e="T12" id="Seg_766" s="T7">Mekga tʼäratta, tau Jewlaškan pajat. </ta>
            <ta e="T15" id="Seg_767" s="T12">Man tʼütdə kuroːnnan. </ta>
            <ta e="T17" id="Seg_768" s="T15">Tʼüwan, tʼölomčan. </ta>
            <ta e="T20" id="Seg_769" s="T17">(Man tepse tʼölomčezan). </ta>
            <ta e="T26" id="Seg_770" s="T20">Man tʼäran: “Tau qaj Jewlaškan pajat?” </ta>
            <ta e="T34" id="Seg_771" s="T26">A tebla mekga tʼärattə: “As Jewlaškan pajat tʼütdə.” </ta>
            <ta e="T40" id="Seg_772" s="T34">A mekga tʼärzat što Jewlaškin pajat. </ta>
            <ta e="T45" id="Seg_773" s="T40">I man i kurlʼe koššedʼän. </ta>
            <ta e="T48" id="Seg_774" s="T45">Tebla mekga sedʼäptəbat. </ta>
            <ta e="T53" id="Seg_775" s="T48">A tau Angelina Iwanowna tʼümatda. </ta>
            <ta e="T56" id="Seg_776" s="T53">Amda qwɛlə auərlʼe. </ta>
            <ta e="T63" id="Seg_777" s="T56">A man tebne soːgədʼzʼan: “Tan kuː čaʒatdə?” </ta>
            <ta e="T73" id="Seg_778" s="T63">A tep tʼarɨn: “Man taftʼet tʼüan täɣɨtdɨltə, eretdə qutdə warkeǯan. </ta>
            <ta e="T80" id="Seg_779" s="T73">Man teɣɨtdɨltə tʼüan sʼüsöɣɨ ɣeʒlam nagɨrlʼe tadɨreǯau.” </ta>
            <ta e="T89" id="Seg_780" s="T80">A man tʼaran, mekga dʼäja Mixajla tʼärɨn: “Piːrʼeɣətdə qwɛːrət. </ta>
            <ta e="T92" id="Seg_781" s="T89">Tan onetdə jewatdə. </ta>
            <ta e="T95" id="Seg_782" s="T92">Tan pelkattə jewat.” </ta>
            <ta e="T99" id="Seg_783" s="T95">Man tebɨm pʼirʼäɣän qwɛrau. </ta>
            <ta e="T104" id="Seg_784" s="T99">Täp mekga tʼüa, manan warkɨs. </ta>
            <ta e="T109" id="Seg_785" s="T104">A teper man tebnan warkan. </ta>
            <ta e="T114" id="Seg_786" s="T109">Man kwaǯan Satdəjedotdə i qwälɨtʼeǯan. </ta>
            <ta e="T117" id="Seg_787" s="T114">Tʼeːkga qwɛl üdetʒan. </ta>
            <ta e="T119" id="Seg_788" s="T117">Tan amǯal. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_789" s="T1">man</ta>
            <ta e="T3" id="Seg_790" s="T2">amdɨ-za-n</ta>
            <ta e="T4" id="Seg_791" s="T3">sorme-ǯe-le</ta>
            <ta e="T5" id="Seg_792" s="T4">qaj-da</ta>
            <ta e="T6" id="Seg_793" s="T5">ne-j-ɣum</ta>
            <ta e="T7" id="Seg_794" s="T6">mennɨ-tə</ta>
            <ta e="T8" id="Seg_795" s="T7">mekga</ta>
            <ta e="T9" id="Seg_796" s="T8">tʼära-tta</ta>
            <ta e="T10" id="Seg_797" s="T9">tau</ta>
            <ta e="T11" id="Seg_798" s="T10">Jewlaška-n</ta>
            <ta e="T12" id="Seg_799" s="T11">paja-t</ta>
            <ta e="T13" id="Seg_800" s="T12">man</ta>
            <ta e="T14" id="Seg_801" s="T13">tʼütdə</ta>
            <ta e="T15" id="Seg_802" s="T14">kur-oːn-na-n</ta>
            <ta e="T16" id="Seg_803" s="T15">tʼü-wa-n</ta>
            <ta e="T17" id="Seg_804" s="T16">tʼölom-ča-n</ta>
            <ta e="T18" id="Seg_805" s="T17">man</ta>
            <ta e="T19" id="Seg_806" s="T18">tep-se</ta>
            <ta e="T20" id="Seg_807" s="T19">tʼölom-če-za-n</ta>
            <ta e="T21" id="Seg_808" s="T20">man</ta>
            <ta e="T22" id="Seg_809" s="T21">tʼära-n</ta>
            <ta e="T23" id="Seg_810" s="T22">tau</ta>
            <ta e="T24" id="Seg_811" s="T23">qaj</ta>
            <ta e="T25" id="Seg_812" s="T24">Jewlaška-n</ta>
            <ta e="T26" id="Seg_813" s="T25">paja-t</ta>
            <ta e="T27" id="Seg_814" s="T26">a</ta>
            <ta e="T28" id="Seg_815" s="T27">teb-la</ta>
            <ta e="T29" id="Seg_816" s="T28">mekga</ta>
            <ta e="T30" id="Seg_817" s="T29">tʼära-ttə</ta>
            <ta e="T31" id="Seg_818" s="T30">as</ta>
            <ta e="T32" id="Seg_819" s="T31">Jewlaška-n</ta>
            <ta e="T33" id="Seg_820" s="T32">paja-t</ta>
            <ta e="T34" id="Seg_821" s="T33">tʼü-tdə</ta>
            <ta e="T35" id="Seg_822" s="T34">a</ta>
            <ta e="T36" id="Seg_823" s="T35">mekga</ta>
            <ta e="T37" id="Seg_824" s="T36">tʼär-za-t</ta>
            <ta e="T38" id="Seg_825" s="T37">što</ta>
            <ta e="T39" id="Seg_826" s="T38">Jewlaški-n</ta>
            <ta e="T40" id="Seg_827" s="T39">paja-t</ta>
            <ta e="T41" id="Seg_828" s="T40">i</ta>
            <ta e="T42" id="Seg_829" s="T41">man</ta>
            <ta e="T43" id="Seg_830" s="T42">i</ta>
            <ta e="T44" id="Seg_831" s="T43">kur-lʼe</ta>
            <ta e="T45" id="Seg_832" s="T44">košše-dʼä-n</ta>
            <ta e="T46" id="Seg_833" s="T45">teb-la</ta>
            <ta e="T47" id="Seg_834" s="T46">mekga</ta>
            <ta e="T48" id="Seg_835" s="T47">sedʼäptə-ba-t</ta>
            <ta e="T49" id="Seg_836" s="T48">a</ta>
            <ta e="T50" id="Seg_837" s="T49">tau</ta>
            <ta e="T51" id="Seg_838" s="T50">Angelina</ta>
            <ta e="T52" id="Seg_839" s="T51">Iwanowna</ta>
            <ta e="T53" id="Seg_840" s="T52">tʼü-ma-tda</ta>
            <ta e="T54" id="Seg_841" s="T53">amda</ta>
            <ta e="T55" id="Seg_842" s="T54">qwɛlə</ta>
            <ta e="T56" id="Seg_843" s="T55">au-ə-r-lʼe</ta>
            <ta e="T57" id="Seg_844" s="T56">a</ta>
            <ta e="T58" id="Seg_845" s="T57">man</ta>
            <ta e="T59" id="Seg_846" s="T58">teb-ne</ta>
            <ta e="T60" id="Seg_847" s="T59">soːgədʼzʼa-n</ta>
            <ta e="T61" id="Seg_848" s="T60">Tan</ta>
            <ta e="T62" id="Seg_849" s="T61">kuː</ta>
            <ta e="T63" id="Seg_850" s="T62">čaʒa-tdə</ta>
            <ta e="T64" id="Seg_851" s="T63">a</ta>
            <ta e="T65" id="Seg_852" s="T64">tep</ta>
            <ta e="T66" id="Seg_853" s="T65">tʼarɨ-n</ta>
            <ta e="T67" id="Seg_854" s="T66">man</ta>
            <ta e="T68" id="Seg_855" s="T67">taftʼe-t</ta>
            <ta e="T69" id="Seg_856" s="T68">tʼü-a-n</ta>
            <ta e="T70" id="Seg_857" s="T69">täɣɨtdɨltə</ta>
            <ta e="T71" id="Seg_858" s="T70">eret-də</ta>
            <ta e="T72" id="Seg_859" s="T71">qutdə</ta>
            <ta e="T73" id="Seg_860" s="T72">wark-eǯa-n</ta>
            <ta e="T74" id="Seg_861" s="T73">man</ta>
            <ta e="T75" id="Seg_862" s="T74">teɣɨtdɨltə</ta>
            <ta e="T76" id="Seg_863" s="T75">tʼü-a-n</ta>
            <ta e="T77" id="Seg_864" s="T76">sʼüsöɣɨ</ta>
            <ta e="T78" id="Seg_865" s="T77">ɣeʒ-la-m</ta>
            <ta e="T79" id="Seg_866" s="T78">nagɨr-lʼe</ta>
            <ta e="T80" id="Seg_867" s="T79">tad-ɨ-r-eǯa-u</ta>
            <ta e="T81" id="Seg_868" s="T80">a</ta>
            <ta e="T82" id="Seg_869" s="T81">man</ta>
            <ta e="T83" id="Seg_870" s="T82">tʼara-n</ta>
            <ta e="T84" id="Seg_871" s="T83">mekga</ta>
            <ta e="T85" id="Seg_872" s="T84">dʼäja</ta>
            <ta e="T86" id="Seg_873" s="T85">Mixajla</ta>
            <ta e="T87" id="Seg_874" s="T86">tʼärɨ-n</ta>
            <ta e="T88" id="Seg_875" s="T87">piːrʼe-ɣətdə</ta>
            <ta e="T89" id="Seg_876" s="T88">qwɛːrə-t</ta>
            <ta e="T90" id="Seg_877" s="T89">tat</ta>
            <ta e="T91" id="Seg_878" s="T90">onetdə</ta>
            <ta e="T92" id="Seg_879" s="T91">je-wa-tdə</ta>
            <ta e="T93" id="Seg_880" s="T92">tat</ta>
            <ta e="T94" id="Seg_881" s="T93">pel-kattə</ta>
            <ta e="T95" id="Seg_882" s="T94">je-wa-t</ta>
            <ta e="T96" id="Seg_883" s="T95">man</ta>
            <ta e="T97" id="Seg_884" s="T96">teb-ɨ-m</ta>
            <ta e="T98" id="Seg_885" s="T97">pʼirʼä-ɣän</ta>
            <ta e="T99" id="Seg_886" s="T98">qwɛra-u</ta>
            <ta e="T100" id="Seg_887" s="T99">täp</ta>
            <ta e="T101" id="Seg_888" s="T100">mekga</ta>
            <ta e="T102" id="Seg_889" s="T101">tʼü-a</ta>
            <ta e="T103" id="Seg_890" s="T102">ma-nan</ta>
            <ta e="T104" id="Seg_891" s="T103">warkɨ-s</ta>
            <ta e="T105" id="Seg_892" s="T104">a</ta>
            <ta e="T106" id="Seg_893" s="T105">teper</ta>
            <ta e="T107" id="Seg_894" s="T106">man</ta>
            <ta e="T108" id="Seg_895" s="T107">teb-nan</ta>
            <ta e="T109" id="Seg_896" s="T108">warka-n</ta>
            <ta e="T110" id="Seg_897" s="T109">man</ta>
            <ta e="T111" id="Seg_898" s="T110">kwa-ǯa-n</ta>
            <ta e="T112" id="Seg_899" s="T111">Satdəjedo-tdə</ta>
            <ta e="T113" id="Seg_900" s="T112">i</ta>
            <ta e="T114" id="Seg_901" s="T113">qwälɨ-tʼ-eǯa-n</ta>
            <ta e="T115" id="Seg_902" s="T114">tʼeːkga</ta>
            <ta e="T116" id="Seg_903" s="T115">qwɛl</ta>
            <ta e="T117" id="Seg_904" s="T116">üd-etʒa-n</ta>
            <ta e="T118" id="Seg_905" s="T117">tat</ta>
            <ta e="T119" id="Seg_906" s="T118">am-ǯa-l</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_907" s="T1">man</ta>
            <ta e="T3" id="Seg_908" s="T2">amdɨ-sɨ-ŋ</ta>
            <ta e="T4" id="Seg_909" s="T3">sormu-ǯə-le</ta>
            <ta e="T5" id="Seg_910" s="T4">qaj-ta</ta>
            <ta e="T6" id="Seg_911" s="T5">ne-lʼ-qum</ta>
            <ta e="T7" id="Seg_912" s="T6">mendɨ-ntɨ</ta>
            <ta e="T8" id="Seg_913" s="T7">mekka</ta>
            <ta e="T9" id="Seg_914" s="T8">tʼärɨ-tɨn</ta>
            <ta e="T10" id="Seg_915" s="T9">taw</ta>
            <ta e="T11" id="Seg_916" s="T10">Jewlaška-n</ta>
            <ta e="T12" id="Seg_917" s="T11">paja-tə</ta>
            <ta e="T13" id="Seg_918" s="T12">man</ta>
            <ta e="T14" id="Seg_919" s="T13">tɨtʼa</ta>
            <ta e="T15" id="Seg_920" s="T14">kur-ol-nɨ-ŋ</ta>
            <ta e="T16" id="Seg_921" s="T15">tüː-nɨ-ŋ</ta>
            <ta e="T17" id="Seg_922" s="T16">tʼolom-ču-ŋ</ta>
            <ta e="T18" id="Seg_923" s="T17">man</ta>
            <ta e="T19" id="Seg_924" s="T18">täp-se</ta>
            <ta e="T20" id="Seg_925" s="T19">tʼolom-ču-sɨ-ŋ</ta>
            <ta e="T21" id="Seg_926" s="T20">man</ta>
            <ta e="T22" id="Seg_927" s="T21">tʼärɨ-ŋ</ta>
            <ta e="T23" id="Seg_928" s="T22">taw</ta>
            <ta e="T24" id="Seg_929" s="T23">qaj</ta>
            <ta e="T25" id="Seg_930" s="T24">Jewlaška-n</ta>
            <ta e="T26" id="Seg_931" s="T25">paja-tə</ta>
            <ta e="T27" id="Seg_932" s="T26">a</ta>
            <ta e="T28" id="Seg_933" s="T27">täp-la</ta>
            <ta e="T29" id="Seg_934" s="T28">mekka</ta>
            <ta e="T30" id="Seg_935" s="T29">tʼärɨ-tɨn</ta>
            <ta e="T31" id="Seg_936" s="T30">asa</ta>
            <ta e="T32" id="Seg_937" s="T31">Jewlaška-n</ta>
            <ta e="T33" id="Seg_938" s="T32">paja-tə</ta>
            <ta e="T34" id="Seg_939" s="T33">tüː-ntɨ</ta>
            <ta e="T35" id="Seg_940" s="T34">a</ta>
            <ta e="T36" id="Seg_941" s="T35">mekka</ta>
            <ta e="T37" id="Seg_942" s="T36">tʼärɨ-sɨ-tɨn</ta>
            <ta e="T38" id="Seg_943" s="T37">što</ta>
            <ta e="T39" id="Seg_944" s="T38">Jewlaška-n</ta>
            <ta e="T40" id="Seg_945" s="T39">paja-tə</ta>
            <ta e="T41" id="Seg_946" s="T40">i</ta>
            <ta e="T42" id="Seg_947" s="T41">man</ta>
            <ta e="T43" id="Seg_948" s="T42">i</ta>
            <ta e="T44" id="Seg_949" s="T43">kur-le</ta>
            <ta e="T45" id="Seg_950" s="T44">kössə-dʼi-ŋ</ta>
            <ta e="T46" id="Seg_951" s="T45">täp-la</ta>
            <ta e="T47" id="Seg_952" s="T46">mekka</ta>
            <ta e="T48" id="Seg_953" s="T47">sedʼäptə-mbɨ-tɨn</ta>
            <ta e="T49" id="Seg_954" s="T48">a</ta>
            <ta e="T50" id="Seg_955" s="T49">taw</ta>
            <ta e="T51" id="Seg_956" s="T50">Angelina</ta>
            <ta e="T52" id="Seg_957" s="T51">Iwanɨwna</ta>
            <ta e="T53" id="Seg_958" s="T52">tüː-mbɨ-ntɨ</ta>
            <ta e="T54" id="Seg_959" s="T53">amdɨ</ta>
            <ta e="T55" id="Seg_960" s="T54">qwɛl</ta>
            <ta e="T56" id="Seg_961" s="T55">am-ɨ-r-le</ta>
            <ta e="T57" id="Seg_962" s="T56">a</ta>
            <ta e="T58" id="Seg_963" s="T57">man</ta>
            <ta e="T59" id="Seg_964" s="T58">täp-nä</ta>
            <ta e="T60" id="Seg_965" s="T59">sogundʼe-ŋ</ta>
            <ta e="T61" id="Seg_966" s="T60">tan</ta>
            <ta e="T62" id="Seg_967" s="T61">kuː</ta>
            <ta e="T63" id="Seg_968" s="T62">čaǯɨ-ntə</ta>
            <ta e="T64" id="Seg_969" s="T63">a</ta>
            <ta e="T65" id="Seg_970" s="T64">täp</ta>
            <ta e="T66" id="Seg_971" s="T65">tʼärɨ-n</ta>
            <ta e="T67" id="Seg_972" s="T66">man</ta>
            <ta e="T68" id="Seg_973" s="T67">tautʼe-ntə</ta>
            <ta e="T69" id="Seg_974" s="T68">tüː-ɨ-ŋ</ta>
            <ta e="T70" id="Seg_975" s="T69">täɣɨtdɨltə</ta>
            <ta e="T71" id="Seg_976" s="T70">eret-tə</ta>
            <ta e="T72" id="Seg_977" s="T71">kundɨ</ta>
            <ta e="T73" id="Seg_978" s="T72">warkɨ-enǯɨ-ŋ</ta>
            <ta e="T74" id="Seg_979" s="T73">man</ta>
            <ta e="T75" id="Seg_980" s="T74">täɣɨtdɨltə</ta>
            <ta e="T76" id="Seg_981" s="T75">tüː-ɨ-ŋ</ta>
            <ta e="T77" id="Seg_982" s="T76">süsögi</ta>
            <ta e="T78" id="Seg_983" s="T77">əǯə-la-m</ta>
            <ta e="T79" id="Seg_984" s="T78">nagər-le</ta>
            <ta e="T80" id="Seg_985" s="T79">tat-ɨ-r-enǯɨ-w</ta>
            <ta e="T81" id="Seg_986" s="T80">a</ta>
            <ta e="T82" id="Seg_987" s="T81">man</ta>
            <ta e="T83" id="Seg_988" s="T82">tʼärɨ-ŋ</ta>
            <ta e="T84" id="Seg_989" s="T83">mekka</ta>
            <ta e="T85" id="Seg_990" s="T84">dʼaja</ta>
            <ta e="T86" id="Seg_991" s="T85">Mixajla</ta>
            <ta e="T87" id="Seg_992" s="T86">tʼärɨ-n</ta>
            <ta e="T88" id="Seg_993" s="T87">pirä-qɨntɨ</ta>
            <ta e="T89" id="Seg_994" s="T88">qwɨrɨ-t</ta>
            <ta e="T90" id="Seg_995" s="T89">tan</ta>
            <ta e="T91" id="Seg_996" s="T90">onendǝ</ta>
            <ta e="T92" id="Seg_997" s="T91">eː-nɨ-ntə</ta>
            <ta e="T93" id="Seg_998" s="T92">tan</ta>
            <ta e="T94" id="Seg_999" s="T93">*pel-kattə</ta>
            <ta e="T95" id="Seg_1000" s="T94">eː-nɨ-ntɨ</ta>
            <ta e="T96" id="Seg_1001" s="T95">man</ta>
            <ta e="T97" id="Seg_1002" s="T96">täp-ɨ-m</ta>
            <ta e="T98" id="Seg_1003" s="T97">pirä-qɨn</ta>
            <ta e="T99" id="Seg_1004" s="T98">qwɨrɨ-w</ta>
            <ta e="T100" id="Seg_1005" s="T99">täp</ta>
            <ta e="T101" id="Seg_1006" s="T100">mekka</ta>
            <ta e="T102" id="Seg_1007" s="T101">tüː-nɨ</ta>
            <ta e="T103" id="Seg_1008" s="T102">man-nan</ta>
            <ta e="T104" id="Seg_1009" s="T103">warkɨ-sɨ</ta>
            <ta e="T105" id="Seg_1010" s="T104">a</ta>
            <ta e="T106" id="Seg_1011" s="T105">teper</ta>
            <ta e="T107" id="Seg_1012" s="T106">man</ta>
            <ta e="T108" id="Seg_1013" s="T107">täp-nan</ta>
            <ta e="T109" id="Seg_1014" s="T108">warkɨ-ŋ</ta>
            <ta e="T110" id="Seg_1015" s="T109">man</ta>
            <ta e="T111" id="Seg_1016" s="T110">qwan-enǯɨ-ŋ</ta>
            <ta e="T112" id="Seg_1017" s="T111">Satdʼijedo-ntə</ta>
            <ta e="T113" id="Seg_1018" s="T112">i</ta>
            <ta e="T114" id="Seg_1019" s="T113">qwäːlɨj-ntɨ-enǯɨ-ŋ</ta>
            <ta e="T115" id="Seg_1020" s="T114">tekka</ta>
            <ta e="T116" id="Seg_1021" s="T115">qwɛl</ta>
            <ta e="T117" id="Seg_1022" s="T116">üdə-enǯɨ-ŋ</ta>
            <ta e="T118" id="Seg_1023" s="T117">tan</ta>
            <ta e="T119" id="Seg_1024" s="T118">am-enǯɨ-l</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1025" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_1026" s="T2">sit-PST-1SG.S</ta>
            <ta e="T4" id="Seg_1027" s="T3">knit-DRV-CVB</ta>
            <ta e="T5" id="Seg_1028" s="T4">what-INDEF.[NOM]</ta>
            <ta e="T6" id="Seg_1029" s="T5">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T7" id="Seg_1030" s="T6">pass-INFER.[3SG.S]</ta>
            <ta e="T8" id="Seg_1031" s="T7">I.ALL</ta>
            <ta e="T9" id="Seg_1032" s="T8">say-3PL</ta>
            <ta e="T10" id="Seg_1033" s="T9">this</ta>
            <ta e="T11" id="Seg_1034" s="T10">Evlashka-GEN</ta>
            <ta e="T12" id="Seg_1035" s="T11">woman.[NOM]-3SG</ta>
            <ta e="T13" id="Seg_1036" s="T12">I.NOM</ta>
            <ta e="T14" id="Seg_1037" s="T13">here</ta>
            <ta e="T15" id="Seg_1038" s="T14">run-MOM-CO-1SG.S</ta>
            <ta e="T16" id="Seg_1039" s="T15">come-CO-1SG.S</ta>
            <ta e="T17" id="Seg_1040" s="T16">hello-VBLZ-1SG.S</ta>
            <ta e="T18" id="Seg_1041" s="T17">I.NOM</ta>
            <ta e="T19" id="Seg_1042" s="T18">(s)he-COM</ta>
            <ta e="T20" id="Seg_1043" s="T19">hello-VBLZ-PST-1SG.S</ta>
            <ta e="T21" id="Seg_1044" s="T20">I.NOM</ta>
            <ta e="T22" id="Seg_1045" s="T21">say-1SG.S</ta>
            <ta e="T23" id="Seg_1046" s="T22">this</ta>
            <ta e="T24" id="Seg_1047" s="T23">either</ta>
            <ta e="T25" id="Seg_1048" s="T24">Evlashka-GEN</ta>
            <ta e="T26" id="Seg_1049" s="T25">wife.[NOM]-3SG</ta>
            <ta e="T27" id="Seg_1050" s="T26">and</ta>
            <ta e="T28" id="Seg_1051" s="T27">(s)he-PL.[NOM]</ta>
            <ta e="T29" id="Seg_1052" s="T28">I.ALL</ta>
            <ta e="T30" id="Seg_1053" s="T29">say-3PL</ta>
            <ta e="T31" id="Seg_1054" s="T30">NEG</ta>
            <ta e="T32" id="Seg_1055" s="T31">Evlashka-GEN</ta>
            <ta e="T33" id="Seg_1056" s="T32">wife.[NOM]-3SG</ta>
            <ta e="T34" id="Seg_1057" s="T33">come-INFER.[3SG.S]</ta>
            <ta e="T35" id="Seg_1058" s="T34">and</ta>
            <ta e="T36" id="Seg_1059" s="T35">I.ALL</ta>
            <ta e="T37" id="Seg_1060" s="T36">say-PST-3PL</ta>
            <ta e="T38" id="Seg_1061" s="T37">that</ta>
            <ta e="T39" id="Seg_1062" s="T38">Evlashka-GEN</ta>
            <ta e="T40" id="Seg_1063" s="T39">wife.[NOM]-3SG</ta>
            <ta e="T41" id="Seg_1064" s="T40">and</ta>
            <ta e="T42" id="Seg_1065" s="T41">I.NOM</ta>
            <ta e="T43" id="Seg_1066" s="T42">and</ta>
            <ta e="T44" id="Seg_1067" s="T43">run-CVB</ta>
            <ta e="T45" id="Seg_1068" s="T44">backward-VBLZ-1SG.S</ta>
            <ta e="T46" id="Seg_1069" s="T45">(s)he-PL.[NOM]</ta>
            <ta e="T47" id="Seg_1070" s="T46">I.ALL</ta>
            <ta e="T48" id="Seg_1071" s="T47">tell.a.lie-PST.NAR-3PL</ta>
            <ta e="T49" id="Seg_1072" s="T48">and</ta>
            <ta e="T50" id="Seg_1073" s="T49">this</ta>
            <ta e="T51" id="Seg_1074" s="T50">Angelina.[NOM]</ta>
            <ta e="T52" id="Seg_1075" s="T51">Ivanovna.[NOM]</ta>
            <ta e="T53" id="Seg_1076" s="T52">come-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T54" id="Seg_1077" s="T53">sit.[3SG.S]</ta>
            <ta e="T55" id="Seg_1078" s="T54">fish.[NOM]</ta>
            <ta e="T56" id="Seg_1079" s="T55">eat-EP-FRQ-CVB</ta>
            <ta e="T57" id="Seg_1080" s="T56">and</ta>
            <ta e="T58" id="Seg_1081" s="T57">I.NOM</ta>
            <ta e="T59" id="Seg_1082" s="T58">(s)he-ALL</ta>
            <ta e="T60" id="Seg_1083" s="T59">ask-1SG.S</ta>
            <ta e="T61" id="Seg_1084" s="T60">you.SG.NOM</ta>
            <ta e="T62" id="Seg_1085" s="T61">where</ta>
            <ta e="T63" id="Seg_1086" s="T62">go-2SG.S</ta>
            <ta e="T64" id="Seg_1087" s="T63">and</ta>
            <ta e="T65" id="Seg_1088" s="T64">(s)he.[NOM]</ta>
            <ta e="T66" id="Seg_1089" s="T65">say-3SG.S</ta>
            <ta e="T67" id="Seg_1090" s="T66">I.NOM</ta>
            <ta e="T68" id="Seg_1091" s="T67">here-ILL</ta>
            <ta e="T69" id="Seg_1092" s="T68">come-EP-1SG.S</ta>
            <ta e="T70" id="Seg_1093" s="T69">you.PL.ALL</ta>
            <ta e="T71" id="Seg_1094" s="T70">month.[NOM]-3SG</ta>
            <ta e="T72" id="Seg_1095" s="T71">during</ta>
            <ta e="T73" id="Seg_1096" s="T72">live-FUT-1SG.S</ta>
            <ta e="T74" id="Seg_1097" s="T73">I.NOM</ta>
            <ta e="T75" id="Seg_1098" s="T74">you.PL.ALL</ta>
            <ta e="T76" id="Seg_1099" s="T75">come-EP-1SG.S</ta>
            <ta e="T77" id="Seg_1100" s="T76">Selkup</ta>
            <ta e="T78" id="Seg_1101" s="T77">word-PL-ACC</ta>
            <ta e="T79" id="Seg_1102" s="T78">write-CVB</ta>
            <ta e="T80" id="Seg_1103" s="T79">bring-EP-FRQ-FUT-1SG.O</ta>
            <ta e="T81" id="Seg_1104" s="T80">and</ta>
            <ta e="T82" id="Seg_1105" s="T81">I.NOM</ta>
            <ta e="T83" id="Seg_1106" s="T82">say-1SG.S</ta>
            <ta e="T84" id="Seg_1107" s="T83">I.ALL</ta>
            <ta e="T85" id="Seg_1108" s="T84">uncle.[NOM]</ta>
            <ta e="T86" id="Seg_1109" s="T85">Mikhailo.[NOM]</ta>
            <ta e="T87" id="Seg_1110" s="T86">say-3SG.S</ta>
            <ta e="T88" id="Seg_1111" s="T87">self-LOC.3SG</ta>
            <ta e="T89" id="Seg_1112" s="T88">call-3SG.O</ta>
            <ta e="T90" id="Seg_1113" s="T89">you.SG.NOM</ta>
            <ta e="T91" id="Seg_1114" s="T90">oneself.2SG</ta>
            <ta e="T92" id="Seg_1115" s="T91">be-CO-2SG.S</ta>
            <ta e="T93" id="Seg_1116" s="T92">you.SG.NOM</ta>
            <ta e="T94" id="Seg_1117" s="T93">friend-CAR.[NOM]</ta>
            <ta e="T95" id="Seg_1118" s="T94">be-CO-INFER.[3SG.S]</ta>
            <ta e="T96" id="Seg_1119" s="T95">I.NOM</ta>
            <ta e="T97" id="Seg_1120" s="T96">(s)he-EP-ACC</ta>
            <ta e="T98" id="Seg_1121" s="T97">self-LOC</ta>
            <ta e="T99" id="Seg_1122" s="T98">call-1SG.O</ta>
            <ta e="T100" id="Seg_1123" s="T99">(s)he.[NOM]</ta>
            <ta e="T101" id="Seg_1124" s="T100">I.ALL</ta>
            <ta e="T102" id="Seg_1125" s="T101">come-CO.[3SG.S]</ta>
            <ta e="T103" id="Seg_1126" s="T102">I-ADES</ta>
            <ta e="T104" id="Seg_1127" s="T103">live-PST.[3SG.S]</ta>
            <ta e="T105" id="Seg_1128" s="T104">and</ta>
            <ta e="T106" id="Seg_1129" s="T105">now</ta>
            <ta e="T107" id="Seg_1130" s="T106">I.NOM</ta>
            <ta e="T108" id="Seg_1131" s="T107">(s)he-ADES</ta>
            <ta e="T109" id="Seg_1132" s="T108">live-1SG.S</ta>
            <ta e="T110" id="Seg_1133" s="T109">I.NOM</ta>
            <ta e="T111" id="Seg_1134" s="T110">leave-FUT-1SG.S</ta>
            <ta e="T112" id="Seg_1135" s="T111">Sondorovo-ILL</ta>
            <ta e="T113" id="Seg_1136" s="T112">and</ta>
            <ta e="T114" id="Seg_1137" s="T113">fish-IPFV-FUT-1SG.S</ta>
            <ta e="T115" id="Seg_1138" s="T114">you.ALL</ta>
            <ta e="T116" id="Seg_1139" s="T115">fish.[NOM]</ta>
            <ta e="T117" id="Seg_1140" s="T116">send-FUT-1SG.S</ta>
            <ta e="T118" id="Seg_1141" s="T117">you.SG.NOM</ta>
            <ta e="T119" id="Seg_1142" s="T118">eat-FUT-2SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1143" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_1144" s="T2">сидеть-PST-1SG.S</ta>
            <ta e="T4" id="Seg_1145" s="T3">связать-DRV-CVB</ta>
            <ta e="T5" id="Seg_1146" s="T4">что-INDEF.[NOM]</ta>
            <ta e="T6" id="Seg_1147" s="T5">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T7" id="Seg_1148" s="T6">пройти-INFER.[3SG.S]</ta>
            <ta e="T8" id="Seg_1149" s="T7">я.ALL</ta>
            <ta e="T9" id="Seg_1150" s="T8">сказать-3PL</ta>
            <ta e="T10" id="Seg_1151" s="T9">этот</ta>
            <ta e="T11" id="Seg_1152" s="T10">Евлашка-GEN</ta>
            <ta e="T12" id="Seg_1153" s="T11">женщина.[NOM]-3SG</ta>
            <ta e="T13" id="Seg_1154" s="T12">я.NOM</ta>
            <ta e="T14" id="Seg_1155" s="T13">сюда</ta>
            <ta e="T15" id="Seg_1156" s="T14">бегать-MOM-CO-1SG.S</ta>
            <ta e="T16" id="Seg_1157" s="T15">прийти-CO-1SG.S</ta>
            <ta e="T17" id="Seg_1158" s="T16">здравствуй-VBLZ-1SG.S</ta>
            <ta e="T18" id="Seg_1159" s="T17">я.NOM</ta>
            <ta e="T19" id="Seg_1160" s="T18">он(а)-COM</ta>
            <ta e="T20" id="Seg_1161" s="T19">здравствуй-VBLZ-PST-1SG.S</ta>
            <ta e="T21" id="Seg_1162" s="T20">я.NOM</ta>
            <ta e="T22" id="Seg_1163" s="T21">сказать-1SG.S</ta>
            <ta e="T23" id="Seg_1164" s="T22">этот</ta>
            <ta e="T24" id="Seg_1165" s="T23">ли</ta>
            <ta e="T25" id="Seg_1166" s="T24">Евлашка-GEN</ta>
            <ta e="T26" id="Seg_1167" s="T25">жена.[NOM]-3SG</ta>
            <ta e="T27" id="Seg_1168" s="T26">а</ta>
            <ta e="T28" id="Seg_1169" s="T27">он(а)-PL.[NOM]</ta>
            <ta e="T29" id="Seg_1170" s="T28">я.ALL</ta>
            <ta e="T30" id="Seg_1171" s="T29">сказать-3PL</ta>
            <ta e="T31" id="Seg_1172" s="T30">NEG</ta>
            <ta e="T32" id="Seg_1173" s="T31">Евлашка-GEN</ta>
            <ta e="T33" id="Seg_1174" s="T32">жена.[NOM]-3SG</ta>
            <ta e="T34" id="Seg_1175" s="T33">прийти-INFER.[3SG.S]</ta>
            <ta e="T35" id="Seg_1176" s="T34">а</ta>
            <ta e="T36" id="Seg_1177" s="T35">я.ALL</ta>
            <ta e="T37" id="Seg_1178" s="T36">сказать-PST-3PL</ta>
            <ta e="T38" id="Seg_1179" s="T37">что</ta>
            <ta e="T39" id="Seg_1180" s="T38">Евлашка-GEN</ta>
            <ta e="T40" id="Seg_1181" s="T39">жена.[NOM]-3SG</ta>
            <ta e="T41" id="Seg_1182" s="T40">и</ta>
            <ta e="T42" id="Seg_1183" s="T41">я.NOM</ta>
            <ta e="T43" id="Seg_1184" s="T42">и</ta>
            <ta e="T44" id="Seg_1185" s="T43">бегать-CVB</ta>
            <ta e="T45" id="Seg_1186" s="T44">назад-VBLZ-1SG.S</ta>
            <ta e="T46" id="Seg_1187" s="T45">он(а)-PL.[NOM]</ta>
            <ta e="T47" id="Seg_1188" s="T46">я.ALL</ta>
            <ta e="T48" id="Seg_1189" s="T47">соврать-PST.NAR-3PL</ta>
            <ta e="T49" id="Seg_1190" s="T48">а</ta>
            <ta e="T50" id="Seg_1191" s="T49">этот</ta>
            <ta e="T51" id="Seg_1192" s="T50">Ангелина.[NOM]</ta>
            <ta e="T52" id="Seg_1193" s="T51">Ивановна.[NOM]</ta>
            <ta e="T53" id="Seg_1194" s="T52">прийти-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T54" id="Seg_1195" s="T53">сидеть.[3SG.S]</ta>
            <ta e="T55" id="Seg_1196" s="T54">рыба.[NOM]</ta>
            <ta e="T56" id="Seg_1197" s="T55">съесть-EP-FRQ-CVB</ta>
            <ta e="T57" id="Seg_1198" s="T56">а</ta>
            <ta e="T58" id="Seg_1199" s="T57">я.NOM</ta>
            <ta e="T59" id="Seg_1200" s="T58">он(а)-ALL</ta>
            <ta e="T60" id="Seg_1201" s="T59">спросить-1SG.S</ta>
            <ta e="T61" id="Seg_1202" s="T60">ты.NOM</ta>
            <ta e="T62" id="Seg_1203" s="T61">куда</ta>
            <ta e="T63" id="Seg_1204" s="T62">ходить-2SG.S</ta>
            <ta e="T64" id="Seg_1205" s="T63">а</ta>
            <ta e="T65" id="Seg_1206" s="T64">он(а).[NOM]</ta>
            <ta e="T66" id="Seg_1207" s="T65">сказать-3SG.S</ta>
            <ta e="T67" id="Seg_1208" s="T66">я.NOM</ta>
            <ta e="T68" id="Seg_1209" s="T67">сюда-ILL</ta>
            <ta e="T69" id="Seg_1210" s="T68">приехать-EP-1SG.S</ta>
            <ta e="T70" id="Seg_1211" s="T69">вы.PL.ALL</ta>
            <ta e="T71" id="Seg_1212" s="T70">месяц.[NOM]-3SG</ta>
            <ta e="T72" id="Seg_1213" s="T71">в.течение</ta>
            <ta e="T73" id="Seg_1214" s="T72">жить-FUT-1SG.S</ta>
            <ta e="T74" id="Seg_1215" s="T73">я.NOM</ta>
            <ta e="T75" id="Seg_1216" s="T74">вы.PL.ALL</ta>
            <ta e="T76" id="Seg_1217" s="T75">приехать-EP-1SG.S</ta>
            <ta e="T77" id="Seg_1218" s="T76">селькупский</ta>
            <ta e="T78" id="Seg_1219" s="T77">слово-PL-ACC</ta>
            <ta e="T79" id="Seg_1220" s="T78">написать-CVB</ta>
            <ta e="T80" id="Seg_1221" s="T79">принести-EP-FRQ-FUT-1SG.O</ta>
            <ta e="T81" id="Seg_1222" s="T80">а</ta>
            <ta e="T82" id="Seg_1223" s="T81">я.NOM</ta>
            <ta e="T83" id="Seg_1224" s="T82">сказать-1SG.S</ta>
            <ta e="T84" id="Seg_1225" s="T83">я.ALL</ta>
            <ta e="T85" id="Seg_1226" s="T84">дядя.[NOM]</ta>
            <ta e="T86" id="Seg_1227" s="T85">Михайло.[NOM]</ta>
            <ta e="T87" id="Seg_1228" s="T86">сказать-3SG.S</ta>
            <ta e="T88" id="Seg_1229" s="T87">себя-LOC.3SG</ta>
            <ta e="T89" id="Seg_1230" s="T88">позвать-3SG.O</ta>
            <ta e="T90" id="Seg_1231" s="T89">ты.NOM</ta>
            <ta e="T91" id="Seg_1232" s="T90">сам.2SG</ta>
            <ta e="T92" id="Seg_1233" s="T91">быть-CO-2SG.S</ta>
            <ta e="T93" id="Seg_1234" s="T92">ты.NOM</ta>
            <ta e="T94" id="Seg_1235" s="T93">друг-CAR.[NOM]</ta>
            <ta e="T95" id="Seg_1236" s="T94">быть-CO-INFER.[3SG.S]</ta>
            <ta e="T96" id="Seg_1237" s="T95">я.NOM</ta>
            <ta e="T97" id="Seg_1238" s="T96">он(а)-EP-ACC</ta>
            <ta e="T98" id="Seg_1239" s="T97">себя-LOC</ta>
            <ta e="T99" id="Seg_1240" s="T98">позвать-1SG.O</ta>
            <ta e="T100" id="Seg_1241" s="T99">он(а).[NOM]</ta>
            <ta e="T101" id="Seg_1242" s="T100">я.ALL</ta>
            <ta e="T102" id="Seg_1243" s="T101">прийти-CO.[3SG.S]</ta>
            <ta e="T103" id="Seg_1244" s="T102">я-ADES</ta>
            <ta e="T104" id="Seg_1245" s="T103">жить-PST.[3SG.S]</ta>
            <ta e="T105" id="Seg_1246" s="T104">а</ta>
            <ta e="T106" id="Seg_1247" s="T105">теперь</ta>
            <ta e="T107" id="Seg_1248" s="T106">я.NOM</ta>
            <ta e="T108" id="Seg_1249" s="T107">он(а)-ADES</ta>
            <ta e="T109" id="Seg_1250" s="T108">жить-1SG.S</ta>
            <ta e="T110" id="Seg_1251" s="T109">я.NOM</ta>
            <ta e="T111" id="Seg_1252" s="T110">отправиться-FUT-1SG.S</ta>
            <ta e="T112" id="Seg_1253" s="T111">Сондорово-ILL</ta>
            <ta e="T113" id="Seg_1254" s="T112">и</ta>
            <ta e="T114" id="Seg_1255" s="T113">рыбачить-IPFV-FUT-1SG.S</ta>
            <ta e="T115" id="Seg_1256" s="T114">ты.ALL</ta>
            <ta e="T116" id="Seg_1257" s="T115">рыба.[NOM]</ta>
            <ta e="T117" id="Seg_1258" s="T116">посылать-FUT-1SG.S</ta>
            <ta e="T118" id="Seg_1259" s="T117">ты.NOM</ta>
            <ta e="T119" id="Seg_1260" s="T118">съесть-FUT-2SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1261" s="T1">pers</ta>
            <ta e="T3" id="Seg_1262" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_1263" s="T3">v-v&gt;v-v&gt;adv</ta>
            <ta e="T5" id="Seg_1264" s="T4">interrog-clit.[n:case]</ta>
            <ta e="T6" id="Seg_1265" s="T5">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T7" id="Seg_1266" s="T6">v-v:mood.[v:pn]</ta>
            <ta e="T8" id="Seg_1267" s="T7">pers</ta>
            <ta e="T9" id="Seg_1268" s="T8">v-v:pn</ta>
            <ta e="T10" id="Seg_1269" s="T9">dem</ta>
            <ta e="T11" id="Seg_1270" s="T10">nprop-n:case</ta>
            <ta e="T12" id="Seg_1271" s="T11">n.[n:case]-n:poss</ta>
            <ta e="T13" id="Seg_1272" s="T12">pers</ta>
            <ta e="T14" id="Seg_1273" s="T13">adv</ta>
            <ta e="T15" id="Seg_1274" s="T14">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T16" id="Seg_1275" s="T15">v-v:ins-v:pn</ta>
            <ta e="T17" id="Seg_1276" s="T16">interj-n&gt;v-v:pn</ta>
            <ta e="T18" id="Seg_1277" s="T17">pers</ta>
            <ta e="T19" id="Seg_1278" s="T18">pers-n:case</ta>
            <ta e="T20" id="Seg_1279" s="T19">interj-n&gt;v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_1280" s="T20">pers</ta>
            <ta e="T22" id="Seg_1281" s="T21">v-v:pn</ta>
            <ta e="T23" id="Seg_1282" s="T22">dem</ta>
            <ta e="T24" id="Seg_1283" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_1284" s="T24">nprop-n:case</ta>
            <ta e="T26" id="Seg_1285" s="T25">n.[n:case]-n:poss</ta>
            <ta e="T27" id="Seg_1286" s="T26">conj</ta>
            <ta e="T28" id="Seg_1287" s="T27">pers-n:num.[n:case]</ta>
            <ta e="T29" id="Seg_1288" s="T28">pers</ta>
            <ta e="T30" id="Seg_1289" s="T29">v-v:pn</ta>
            <ta e="T31" id="Seg_1290" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_1291" s="T31">nprop-n:case</ta>
            <ta e="T33" id="Seg_1292" s="T32">n.[n:case]-n:poss</ta>
            <ta e="T34" id="Seg_1293" s="T33">v-v:mood.[v:pn]</ta>
            <ta e="T35" id="Seg_1294" s="T34">conj</ta>
            <ta e="T36" id="Seg_1295" s="T35">pers</ta>
            <ta e="T37" id="Seg_1296" s="T36">v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_1297" s="T37">conj</ta>
            <ta e="T39" id="Seg_1298" s="T38">nprop-n:case</ta>
            <ta e="T40" id="Seg_1299" s="T39">n.[n:case]-n:poss</ta>
            <ta e="T41" id="Seg_1300" s="T40">conj</ta>
            <ta e="T42" id="Seg_1301" s="T41">pers</ta>
            <ta e="T43" id="Seg_1302" s="T42">conj</ta>
            <ta e="T44" id="Seg_1303" s="T43">v-v&gt;adv</ta>
            <ta e="T45" id="Seg_1304" s="T44">adv-n&gt;v-v:pn</ta>
            <ta e="T46" id="Seg_1305" s="T45">pers-n:num.[n:case]</ta>
            <ta e="T47" id="Seg_1306" s="T46">pers</ta>
            <ta e="T48" id="Seg_1307" s="T47">v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_1308" s="T48">conj</ta>
            <ta e="T50" id="Seg_1309" s="T49">dem</ta>
            <ta e="T51" id="Seg_1310" s="T50">nprop.[n:case]</ta>
            <ta e="T52" id="Seg_1311" s="T51">nprop.[n:case]</ta>
            <ta e="T53" id="Seg_1312" s="T52">v-v:tense-v:mood.[v:pn]</ta>
            <ta e="T54" id="Seg_1313" s="T53">v.[v:pn]</ta>
            <ta e="T55" id="Seg_1314" s="T54">n.[n:case]</ta>
            <ta e="T56" id="Seg_1315" s="T55">v-v:ins-v&gt;v-v&gt;adv</ta>
            <ta e="T57" id="Seg_1316" s="T56">conj</ta>
            <ta e="T58" id="Seg_1317" s="T57">pers</ta>
            <ta e="T59" id="Seg_1318" s="T58">pers-n:case</ta>
            <ta e="T60" id="Seg_1319" s="T59">v-v:pn</ta>
            <ta e="T61" id="Seg_1320" s="T60">pers</ta>
            <ta e="T62" id="Seg_1321" s="T61">interrog</ta>
            <ta e="T63" id="Seg_1322" s="T62">v-v:pn</ta>
            <ta e="T64" id="Seg_1323" s="T63">conj</ta>
            <ta e="T65" id="Seg_1324" s="T64">pers.[n:case]</ta>
            <ta e="T66" id="Seg_1325" s="T65">v-v:pn</ta>
            <ta e="T67" id="Seg_1326" s="T66">pers</ta>
            <ta e="T68" id="Seg_1327" s="T67">adv-n:case</ta>
            <ta e="T69" id="Seg_1328" s="T68">v-n:ins-v:pn</ta>
            <ta e="T70" id="Seg_1329" s="T69">pers</ta>
            <ta e="T71" id="Seg_1330" s="T70">n.[n:case]-n:poss</ta>
            <ta e="T72" id="Seg_1331" s="T71">pp</ta>
            <ta e="T73" id="Seg_1332" s="T72">v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_1333" s="T73">pers</ta>
            <ta e="T75" id="Seg_1334" s="T74">pers</ta>
            <ta e="T76" id="Seg_1335" s="T75">v-n:ins-v:pn</ta>
            <ta e="T77" id="Seg_1336" s="T76">adj</ta>
            <ta e="T78" id="Seg_1337" s="T77">n-n:num-n:case</ta>
            <ta e="T79" id="Seg_1338" s="T78">v-v&gt;adv</ta>
            <ta e="T80" id="Seg_1339" s="T79">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_1340" s="T80">conj</ta>
            <ta e="T82" id="Seg_1341" s="T81">pers</ta>
            <ta e="T83" id="Seg_1342" s="T82">v-v:pn</ta>
            <ta e="T84" id="Seg_1343" s="T83">pers</ta>
            <ta e="T85" id="Seg_1344" s="T84">n.[n:case]</ta>
            <ta e="T86" id="Seg_1345" s="T85">nprop.[n:case]</ta>
            <ta e="T87" id="Seg_1346" s="T86">v-v:pn</ta>
            <ta e="T88" id="Seg_1347" s="T87">pro-n:case.poss</ta>
            <ta e="T89" id="Seg_1348" s="T88">v-v:pn</ta>
            <ta e="T90" id="Seg_1349" s="T89">pers</ta>
            <ta e="T91" id="Seg_1350" s="T90">emphpro</ta>
            <ta e="T92" id="Seg_1351" s="T91">v-v:ins-v:pn</ta>
            <ta e="T93" id="Seg_1352" s="T92">pers</ta>
            <ta e="T94" id="Seg_1353" s="T93">n-n&gt;n.[n:case]</ta>
            <ta e="T95" id="Seg_1354" s="T94">v-v:ins-v:mood.[v:pn]</ta>
            <ta e="T96" id="Seg_1355" s="T95">pers</ta>
            <ta e="T97" id="Seg_1356" s="T96">pers-n:ins-n:case</ta>
            <ta e="T98" id="Seg_1357" s="T97">pro-n:case</ta>
            <ta e="T99" id="Seg_1358" s="T98">v-v:pn</ta>
            <ta e="T100" id="Seg_1359" s="T99">pers.[n:case]</ta>
            <ta e="T101" id="Seg_1360" s="T100">pers</ta>
            <ta e="T102" id="Seg_1361" s="T101">v-v:ins.[v:pn]</ta>
            <ta e="T103" id="Seg_1362" s="T102">pers-n:case</ta>
            <ta e="T104" id="Seg_1363" s="T103">v-v:tense.[v:pn]</ta>
            <ta e="T105" id="Seg_1364" s="T104">conj</ta>
            <ta e="T106" id="Seg_1365" s="T105">adv</ta>
            <ta e="T107" id="Seg_1366" s="T106">pers</ta>
            <ta e="T108" id="Seg_1367" s="T107">pers-n:case</ta>
            <ta e="T109" id="Seg_1368" s="T108">v-v:pn</ta>
            <ta e="T110" id="Seg_1369" s="T109">pers</ta>
            <ta e="T111" id="Seg_1370" s="T110">v-v:tense-v:pn</ta>
            <ta e="T112" id="Seg_1371" s="T111">nprop-n:case</ta>
            <ta e="T113" id="Seg_1372" s="T112">conj</ta>
            <ta e="T114" id="Seg_1373" s="T113">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T115" id="Seg_1374" s="T114">pers</ta>
            <ta e="T116" id="Seg_1375" s="T115">n.[n:case]</ta>
            <ta e="T117" id="Seg_1376" s="T116">v-v:tense-v:pn</ta>
            <ta e="T118" id="Seg_1377" s="T117">pers</ta>
            <ta e="T119" id="Seg_1378" s="T118">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1379" s="T1">pers</ta>
            <ta e="T3" id="Seg_1380" s="T2">v</ta>
            <ta e="T4" id="Seg_1381" s="T3">adv</ta>
            <ta e="T5" id="Seg_1382" s="T4">pro</ta>
            <ta e="T6" id="Seg_1383" s="T5">n</ta>
            <ta e="T7" id="Seg_1384" s="T6">v</ta>
            <ta e="T8" id="Seg_1385" s="T7">pers</ta>
            <ta e="T9" id="Seg_1386" s="T8">v</ta>
            <ta e="T10" id="Seg_1387" s="T9">dem</ta>
            <ta e="T11" id="Seg_1388" s="T10">nprop</ta>
            <ta e="T12" id="Seg_1389" s="T11">n</ta>
            <ta e="T13" id="Seg_1390" s="T12">pers</ta>
            <ta e="T14" id="Seg_1391" s="T13">adv</ta>
            <ta e="T15" id="Seg_1392" s="T14">v</ta>
            <ta e="T16" id="Seg_1393" s="T15">v</ta>
            <ta e="T17" id="Seg_1394" s="T16">interj</ta>
            <ta e="T18" id="Seg_1395" s="T17">pers</ta>
            <ta e="T19" id="Seg_1396" s="T18">pers</ta>
            <ta e="T20" id="Seg_1397" s="T19">v</ta>
            <ta e="T21" id="Seg_1398" s="T20">pers</ta>
            <ta e="T22" id="Seg_1399" s="T21">v</ta>
            <ta e="T23" id="Seg_1400" s="T22">dem</ta>
            <ta e="T24" id="Seg_1401" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_1402" s="T24">nprop</ta>
            <ta e="T26" id="Seg_1403" s="T25">n</ta>
            <ta e="T27" id="Seg_1404" s="T26">conj</ta>
            <ta e="T28" id="Seg_1405" s="T27">pers</ta>
            <ta e="T29" id="Seg_1406" s="T28">pers</ta>
            <ta e="T30" id="Seg_1407" s="T29">v</ta>
            <ta e="T31" id="Seg_1408" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_1409" s="T31">nprop</ta>
            <ta e="T33" id="Seg_1410" s="T32">n</ta>
            <ta e="T34" id="Seg_1411" s="T33">v</ta>
            <ta e="T35" id="Seg_1412" s="T34">conj</ta>
            <ta e="T36" id="Seg_1413" s="T35">pers</ta>
            <ta e="T37" id="Seg_1414" s="T36">v</ta>
            <ta e="T38" id="Seg_1415" s="T37">conj</ta>
            <ta e="T39" id="Seg_1416" s="T38">nprop</ta>
            <ta e="T40" id="Seg_1417" s="T39">n</ta>
            <ta e="T41" id="Seg_1418" s="T40">conj</ta>
            <ta e="T42" id="Seg_1419" s="T41">pers</ta>
            <ta e="T43" id="Seg_1420" s="T42">conj</ta>
            <ta e="T44" id="Seg_1421" s="T43">adv</ta>
            <ta e="T45" id="Seg_1422" s="T44">v</ta>
            <ta e="T46" id="Seg_1423" s="T45">pers</ta>
            <ta e="T47" id="Seg_1424" s="T46">pers</ta>
            <ta e="T48" id="Seg_1425" s="T47">v</ta>
            <ta e="T49" id="Seg_1426" s="T48">conj</ta>
            <ta e="T50" id="Seg_1427" s="T49">dem</ta>
            <ta e="T51" id="Seg_1428" s="T50">nprop</ta>
            <ta e="T52" id="Seg_1429" s="T51">nprop</ta>
            <ta e="T53" id="Seg_1430" s="T52">v</ta>
            <ta e="T54" id="Seg_1431" s="T53">v</ta>
            <ta e="T55" id="Seg_1432" s="T54">n</ta>
            <ta e="T56" id="Seg_1433" s="T55">adv</ta>
            <ta e="T57" id="Seg_1434" s="T56">conj</ta>
            <ta e="T58" id="Seg_1435" s="T57">pers</ta>
            <ta e="T59" id="Seg_1436" s="T58">pers</ta>
            <ta e="T60" id="Seg_1437" s="T59">v</ta>
            <ta e="T61" id="Seg_1438" s="T60">pers</ta>
            <ta e="T62" id="Seg_1439" s="T61">interrog</ta>
            <ta e="T63" id="Seg_1440" s="T62">v</ta>
            <ta e="T64" id="Seg_1441" s="T63">conj</ta>
            <ta e="T65" id="Seg_1442" s="T64">pers</ta>
            <ta e="T66" id="Seg_1443" s="T65">v</ta>
            <ta e="T67" id="Seg_1444" s="T66">pers</ta>
            <ta e="T68" id="Seg_1445" s="T67">adv</ta>
            <ta e="T69" id="Seg_1446" s="T68">v</ta>
            <ta e="T70" id="Seg_1447" s="T69">pers</ta>
            <ta e="T71" id="Seg_1448" s="T70">n</ta>
            <ta e="T72" id="Seg_1449" s="T71">pp</ta>
            <ta e="T73" id="Seg_1450" s="T72">v</ta>
            <ta e="T74" id="Seg_1451" s="T73">pers</ta>
            <ta e="T75" id="Seg_1452" s="T74">pers</ta>
            <ta e="T76" id="Seg_1453" s="T75">v</ta>
            <ta e="T77" id="Seg_1454" s="T76">adj</ta>
            <ta e="T78" id="Seg_1455" s="T77">n</ta>
            <ta e="T79" id="Seg_1456" s="T78">adv</ta>
            <ta e="T80" id="Seg_1457" s="T79">v</ta>
            <ta e="T81" id="Seg_1458" s="T80">conj</ta>
            <ta e="T82" id="Seg_1459" s="T81">pers</ta>
            <ta e="T83" id="Seg_1460" s="T82">v</ta>
            <ta e="T84" id="Seg_1461" s="T83">pers</ta>
            <ta e="T85" id="Seg_1462" s="T84">n</ta>
            <ta e="T86" id="Seg_1463" s="T85">nprop</ta>
            <ta e="T87" id="Seg_1464" s="T86">v</ta>
            <ta e="T88" id="Seg_1465" s="T87">pro</ta>
            <ta e="T89" id="Seg_1466" s="T88">v</ta>
            <ta e="T90" id="Seg_1467" s="T89">pers</ta>
            <ta e="T91" id="Seg_1468" s="T90">emphpro</ta>
            <ta e="T92" id="Seg_1469" s="T91">v</ta>
            <ta e="T93" id="Seg_1470" s="T92">pers</ta>
            <ta e="T94" id="Seg_1471" s="T93">n</ta>
            <ta e="T95" id="Seg_1472" s="T94">v</ta>
            <ta e="T96" id="Seg_1473" s="T95">pers</ta>
            <ta e="T97" id="Seg_1474" s="T96">pers</ta>
            <ta e="T98" id="Seg_1475" s="T97">pro</ta>
            <ta e="T99" id="Seg_1476" s="T98">v</ta>
            <ta e="T100" id="Seg_1477" s="T99">pers</ta>
            <ta e="T101" id="Seg_1478" s="T100">pers</ta>
            <ta e="T102" id="Seg_1479" s="T101">v</ta>
            <ta e="T103" id="Seg_1480" s="T102">pers</ta>
            <ta e="T104" id="Seg_1481" s="T103">v</ta>
            <ta e="T105" id="Seg_1482" s="T104">conj</ta>
            <ta e="T106" id="Seg_1483" s="T105">adv</ta>
            <ta e="T107" id="Seg_1484" s="T106">pers</ta>
            <ta e="T108" id="Seg_1485" s="T107">pers</ta>
            <ta e="T109" id="Seg_1486" s="T108">v</ta>
            <ta e="T110" id="Seg_1487" s="T109">pers</ta>
            <ta e="T111" id="Seg_1488" s="T110">v</ta>
            <ta e="T112" id="Seg_1489" s="T111">nprop</ta>
            <ta e="T113" id="Seg_1490" s="T112">conj</ta>
            <ta e="T114" id="Seg_1491" s="T113">v</ta>
            <ta e="T115" id="Seg_1492" s="T114">pers</ta>
            <ta e="T116" id="Seg_1493" s="T115">n</ta>
            <ta e="T117" id="Seg_1494" s="T116">v</ta>
            <ta e="T118" id="Seg_1495" s="T117">pers</ta>
            <ta e="T119" id="Seg_1496" s="T118">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1497" s="T1">pro.h:Th</ta>
            <ta e="T6" id="Seg_1498" s="T5">np.h:A</ta>
            <ta e="T8" id="Seg_1499" s="T7">pro.h:R</ta>
            <ta e="T9" id="Seg_1500" s="T8">0.3.h:A</ta>
            <ta e="T10" id="Seg_1501" s="T9">pro.h:Th</ta>
            <ta e="T11" id="Seg_1502" s="T10">np.h:Poss</ta>
            <ta e="T13" id="Seg_1503" s="T12">pro.h:A</ta>
            <ta e="T14" id="Seg_1504" s="T13">adv:G</ta>
            <ta e="T16" id="Seg_1505" s="T15">0.1.h:A</ta>
            <ta e="T17" id="Seg_1506" s="T16">0.1.h:A</ta>
            <ta e="T18" id="Seg_1507" s="T17">pro.h:A</ta>
            <ta e="T19" id="Seg_1508" s="T18">pro:Com</ta>
            <ta e="T21" id="Seg_1509" s="T20">pro.h:A</ta>
            <ta e="T23" id="Seg_1510" s="T22">pro.h:Th</ta>
            <ta e="T25" id="Seg_1511" s="T24">np.h:Poss</ta>
            <ta e="T28" id="Seg_1512" s="T27">pro.h:A</ta>
            <ta e="T29" id="Seg_1513" s="T28">pro.h:R</ta>
            <ta e="T32" id="Seg_1514" s="T31">np.h:Poss</ta>
            <ta e="T33" id="Seg_1515" s="T32">np.h:A</ta>
            <ta e="T36" id="Seg_1516" s="T35">pro.h:R</ta>
            <ta e="T37" id="Seg_1517" s="T36">0.3.h:A</ta>
            <ta e="T39" id="Seg_1518" s="T38">np.h:Poss</ta>
            <ta e="T40" id="Seg_1519" s="T39">0.3.h:Th</ta>
            <ta e="T42" id="Seg_1520" s="T41">pro.h:A</ta>
            <ta e="T46" id="Seg_1521" s="T45">pro.h:A</ta>
            <ta e="T47" id="Seg_1522" s="T46">pro.h:R</ta>
            <ta e="T51" id="Seg_1523" s="T50">np.h:A</ta>
            <ta e="T54" id="Seg_1524" s="T53">0.3.h:Th</ta>
            <ta e="T55" id="Seg_1525" s="T54">np:P</ta>
            <ta e="T58" id="Seg_1526" s="T57">pro.h:A</ta>
            <ta e="T59" id="Seg_1527" s="T58">pro.h:R</ta>
            <ta e="T61" id="Seg_1528" s="T60">pro.h:A</ta>
            <ta e="T62" id="Seg_1529" s="T61">pro:G</ta>
            <ta e="T65" id="Seg_1530" s="T64">pro.h:A</ta>
            <ta e="T67" id="Seg_1531" s="T66">pro.h:A</ta>
            <ta e="T68" id="Seg_1532" s="T67">adv:G</ta>
            <ta e="T70" id="Seg_1533" s="T69">pro.h:G</ta>
            <ta e="T71" id="Seg_1534" s="T70">pp:Time</ta>
            <ta e="T73" id="Seg_1535" s="T72">0.1.h:Th</ta>
            <ta e="T74" id="Seg_1536" s="T73">pro.h:A</ta>
            <ta e="T75" id="Seg_1537" s="T74">pro.h:G</ta>
            <ta e="T78" id="Seg_1538" s="T77">np:Th</ta>
            <ta e="T80" id="Seg_1539" s="T79">0.1.h:A</ta>
            <ta e="T82" id="Seg_1540" s="T81">pro.h:A</ta>
            <ta e="T84" id="Seg_1541" s="T83">pro.h:R</ta>
            <ta e="T86" id="Seg_1542" s="T85">np.h:A</ta>
            <ta e="T89" id="Seg_1543" s="T88">0.3.h:A 0.3.h:Th</ta>
            <ta e="T90" id="Seg_1544" s="T89">pro.h:Th</ta>
            <ta e="T93" id="Seg_1545" s="T92">pro.h:Th</ta>
            <ta e="T96" id="Seg_1546" s="T95">pro.h:A</ta>
            <ta e="T97" id="Seg_1547" s="T96">pro.h:Th</ta>
            <ta e="T100" id="Seg_1548" s="T99">pro.h:A</ta>
            <ta e="T101" id="Seg_1549" s="T100">pro.h:G</ta>
            <ta e="T103" id="Seg_1550" s="T102">pro.h:L</ta>
            <ta e="T104" id="Seg_1551" s="T103">0.3.h:Th</ta>
            <ta e="T106" id="Seg_1552" s="T105">adv:Time</ta>
            <ta e="T107" id="Seg_1553" s="T106">pro.h:Th</ta>
            <ta e="T108" id="Seg_1554" s="T107">pro.h:L</ta>
            <ta e="T110" id="Seg_1555" s="T109">pro.h:A</ta>
            <ta e="T112" id="Seg_1556" s="T111">np:G</ta>
            <ta e="T114" id="Seg_1557" s="T113">0.1.h:A</ta>
            <ta e="T115" id="Seg_1558" s="T114">pro.h:R</ta>
            <ta e="T116" id="Seg_1559" s="T115">np:Th</ta>
            <ta e="T117" id="Seg_1560" s="T116">0.1.h:A</ta>
            <ta e="T118" id="Seg_1561" s="T117">pro.h:A</ta>
            <ta e="T119" id="Seg_1562" s="T118">0.3:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1563" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_1564" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_1565" s="T3">s:adv</ta>
            <ta e="T6" id="Seg_1566" s="T5">np.h:S</ta>
            <ta e="T7" id="Seg_1567" s="T6">v:pred</ta>
            <ta e="T9" id="Seg_1568" s="T8">0.3.h:S v:pred</ta>
            <ta e="T10" id="Seg_1569" s="T9">pro.h:S</ta>
            <ta e="T12" id="Seg_1570" s="T11">n:pred</ta>
            <ta e="T13" id="Seg_1571" s="T12">pro.h:S</ta>
            <ta e="T15" id="Seg_1572" s="T14">v:pred</ta>
            <ta e="T16" id="Seg_1573" s="T15">0.1.h:S v:pred</ta>
            <ta e="T17" id="Seg_1574" s="T16">0.1.h:S v:pred</ta>
            <ta e="T18" id="Seg_1575" s="T17">pro.h:S</ta>
            <ta e="T20" id="Seg_1576" s="T19">v:pred</ta>
            <ta e="T21" id="Seg_1577" s="T20">pro.h:S</ta>
            <ta e="T22" id="Seg_1578" s="T21">v:pred</ta>
            <ta e="T23" id="Seg_1579" s="T22">pro.h:S</ta>
            <ta e="T26" id="Seg_1580" s="T25">n:pred</ta>
            <ta e="T28" id="Seg_1581" s="T27">pro.h:S</ta>
            <ta e="T30" id="Seg_1582" s="T29">v:pred</ta>
            <ta e="T33" id="Seg_1583" s="T32">np.h:S</ta>
            <ta e="T34" id="Seg_1584" s="T33">v:pred</ta>
            <ta e="T37" id="Seg_1585" s="T36">0.3.h:S v:pred</ta>
            <ta e="T40" id="Seg_1586" s="T39">0.3.h:S n:pred</ta>
            <ta e="T42" id="Seg_1587" s="T41">pro.h:S</ta>
            <ta e="T44" id="Seg_1588" s="T43">s:adv</ta>
            <ta e="T45" id="Seg_1589" s="T44">v:pred</ta>
            <ta e="T46" id="Seg_1590" s="T45">pro.h:S</ta>
            <ta e="T48" id="Seg_1591" s="T47">v:pred</ta>
            <ta e="T51" id="Seg_1592" s="T50">np.h:S</ta>
            <ta e="T53" id="Seg_1593" s="T52">v:pred</ta>
            <ta e="T54" id="Seg_1594" s="T53">0.3.h:S v:pred</ta>
            <ta e="T56" id="Seg_1595" s="T54">s:temp</ta>
            <ta e="T58" id="Seg_1596" s="T57">pro.h:S</ta>
            <ta e="T60" id="Seg_1597" s="T59">v:pred</ta>
            <ta e="T61" id="Seg_1598" s="T60">pro.h:S</ta>
            <ta e="T63" id="Seg_1599" s="T62">v:pred</ta>
            <ta e="T65" id="Seg_1600" s="T64">pro.h:S</ta>
            <ta e="T66" id="Seg_1601" s="T65">v:pred</ta>
            <ta e="T67" id="Seg_1602" s="T66">pro.h:S</ta>
            <ta e="T69" id="Seg_1603" s="T68">v:pred</ta>
            <ta e="T73" id="Seg_1604" s="T72">0.1.h:S v:pred</ta>
            <ta e="T74" id="Seg_1605" s="T73">pro.h:S</ta>
            <ta e="T76" id="Seg_1606" s="T75">v:pred</ta>
            <ta e="T80" id="Seg_1607" s="T79">0.1.h:S v:pred</ta>
            <ta e="T82" id="Seg_1608" s="T81">pro.h:S</ta>
            <ta e="T83" id="Seg_1609" s="T82">v:pred</ta>
            <ta e="T86" id="Seg_1610" s="T85">np.h:S</ta>
            <ta e="T87" id="Seg_1611" s="T86">v:pred</ta>
            <ta e="T89" id="Seg_1612" s="T88">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T90" id="Seg_1613" s="T89">pro.h:S</ta>
            <ta e="T91" id="Seg_1614" s="T90">pro:pred</ta>
            <ta e="T92" id="Seg_1615" s="T91">cop</ta>
            <ta e="T93" id="Seg_1616" s="T92">pro.h:S</ta>
            <ta e="T94" id="Seg_1617" s="T93">n:pred</ta>
            <ta e="T95" id="Seg_1618" s="T94">cop</ta>
            <ta e="T96" id="Seg_1619" s="T95">pro.h:S</ta>
            <ta e="T97" id="Seg_1620" s="T96">pro.h:O</ta>
            <ta e="T99" id="Seg_1621" s="T98">v:pred</ta>
            <ta e="T100" id="Seg_1622" s="T99">pro.h:S</ta>
            <ta e="T102" id="Seg_1623" s="T101">v:pred</ta>
            <ta e="T104" id="Seg_1624" s="T103">0.3.h:S v:pred</ta>
            <ta e="T107" id="Seg_1625" s="T106">pro.h:S</ta>
            <ta e="T109" id="Seg_1626" s="T108">v:pred</ta>
            <ta e="T110" id="Seg_1627" s="T109">pro.h:S</ta>
            <ta e="T111" id="Seg_1628" s="T110">v:pred</ta>
            <ta e="T114" id="Seg_1629" s="T113">0.1.h:S v:pred</ta>
            <ta e="T116" id="Seg_1630" s="T115">np:O</ta>
            <ta e="T117" id="Seg_1631" s="T116">0.1.h:S v:pred</ta>
            <ta e="T118" id="Seg_1632" s="T117">pro.h:S</ta>
            <ta e="T119" id="Seg_1633" s="T118">v:pred 0.3:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_1634" s="T4">RUS:gram</ta>
            <ta e="T27" id="Seg_1635" s="T26">RUS:gram</ta>
            <ta e="T35" id="Seg_1636" s="T34">RUS:gram</ta>
            <ta e="T38" id="Seg_1637" s="T37">RUS:gram</ta>
            <ta e="T41" id="Seg_1638" s="T40">RUS:gram</ta>
            <ta e="T43" id="Seg_1639" s="T42">RUS:gram</ta>
            <ta e="T49" id="Seg_1640" s="T48">RUS:gram</ta>
            <ta e="T57" id="Seg_1641" s="T56">RUS:gram</ta>
            <ta e="T64" id="Seg_1642" s="T63">RUS:gram</ta>
            <ta e="T81" id="Seg_1643" s="T80">RUS:gram</ta>
            <ta e="T85" id="Seg_1644" s="T84">RUS:cult</ta>
            <ta e="T105" id="Seg_1645" s="T104">RUS:gram</ta>
            <ta e="T106" id="Seg_1646" s="T105">RUS:core</ta>
            <ta e="T113" id="Seg_1647" s="T112">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_1648" s="T1">I was sitting and knitting.</ta>
            <ta e="T7" id="Seg_1649" s="T4">A woman passed by.</ta>
            <ta e="T12" id="Seg_1650" s="T7">I was told that was Evlashka's wife.</ta>
            <ta e="T15" id="Seg_1651" s="T12">I came here.</ta>
            <ta e="T17" id="Seg_1652" s="T15">I came and greeted [her].</ta>
            <ta e="T20" id="Seg_1653" s="T17">(I greeted her.)</ta>
            <ta e="T26" id="Seg_1654" s="T20">I said: “Is that Evlashka's wife?”</ta>
            <ta e="T34" id="Seg_1655" s="T26">And they said to me: “It's not Evlashka's wife, who had come.”</ta>
            <ta e="T40" id="Seg_1656" s="T34">And I was told it was Evlashka's wife.</ta>
            <ta e="T45" id="Seg_1657" s="T40">And I came running.</ta>
            <ta e="T48" id="Seg_1658" s="T45">They lied to me.</ta>
            <ta e="T53" id="Seg_1659" s="T48">And it was Angelina Ivanovna, who had come.</ta>
            <ta e="T56" id="Seg_1660" s="T53">She was sitting and eating fish.</ta>
            <ta e="T63" id="Seg_1661" s="T56">And I asked her: “Where are you going?”</ta>
            <ta e="T73" id="Seg_1662" s="T63">And she said: “I came here to you, I'll spend a whole month [here].</ta>
            <ta e="T80" id="Seg_1663" s="T73">I came to you, I'll be writing down Selkup words.”</ta>
            <ta e="T89" id="Seg_1664" s="T80">And I said, uncle Mikhajla told me: “Call her [to live] with you.</ta>
            <ta e="T92" id="Seg_1665" s="T89">You are alone.</ta>
            <ta e="T95" id="Seg_1666" s="T92">You are alone.</ta>
            <ta e="T99" id="Seg_1667" s="T95">I called her [to live] with me.</ta>
            <ta e="T104" id="Seg_1668" s="T99">She came to me, she lived in my house.</ta>
            <ta e="T109" id="Seg_1669" s="T104">And now I live in her house.</ta>
            <ta e="T114" id="Seg_1670" s="T109">I'll go to Novosondorovo and I'll go fishing.</ta>
            <ta e="T117" id="Seg_1671" s="T114">I'll send you fish.</ta>
            <ta e="T119" id="Seg_1672" s="T117">You'll eat it.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_1673" s="T1">Ich saß und strickte.</ta>
            <ta e="T7" id="Seg_1674" s="T4">Eine Frau kam vorbei.</ta>
            <ta e="T12" id="Seg_1675" s="T7">Mir wurde gesagt, das sei Evlashkas Frau.</ta>
            <ta e="T15" id="Seg_1676" s="T12">Ich lief hierher.</ta>
            <ta e="T17" id="Seg_1677" s="T15">Ich kam und grüßte [sie].</ta>
            <ta e="T20" id="Seg_1678" s="T17">(Ich grüßte sie.)</ta>
            <ta e="T26" id="Seg_1679" s="T20">Ich sage: "Ist das Evlashkas Frau?"</ta>
            <ta e="T34" id="Seg_1680" s="T26">Und sie sagen mir: "Nicht Evlashkas Frau ist gekommen."</ta>
            <ta e="T40" id="Seg_1681" s="T34">Und mir wurde gesagt, es sei Evlashkas Frau.</ta>
            <ta e="T45" id="Seg_1682" s="T40">Und ich kam angelaufen.</ta>
            <ta e="T48" id="Seg_1683" s="T45">Sie haben mich angelogen.</ta>
            <ta e="T53" id="Seg_1684" s="T48">Und diese Angelina Ivanovna ist gekommen.</ta>
            <ta e="T56" id="Seg_1685" s="T53">Sie saß und aß Fisch.</ta>
            <ta e="T63" id="Seg_1686" s="T56">Und ich frage sie: "Wohin gehst du?"</ta>
            <ta e="T73" id="Seg_1687" s="T63">Und sie sagt: "Ich kam hierher zu euch, ich bleibe einen ganzen Monat.</ta>
            <ta e="T80" id="Seg_1688" s="T73">Ich kam zu euch, ich werde selkupische Wörter aufschreiben."</ta>
            <ta e="T89" id="Seg_1689" s="T80">Und ich sagte, Onkel Mikhaila hat mir gesagt: "Ruf sie zu dir [damit sie bei dir wohnt].</ta>
            <ta e="T92" id="Seg_1690" s="T89">Du bist allein.</ta>
            <ta e="T95" id="Seg_1691" s="T92">Du bist allein.</ta>
            <ta e="T99" id="Seg_1692" s="T95">Ich rief sie zu mir [damit sie bei mir wohnen kann].</ta>
            <ta e="T104" id="Seg_1693" s="T99">Sie kam zu mir, sie lebte bei mir.</ta>
            <ta e="T109" id="Seg_1694" s="T104">Und jetzt wohne ich bei ihr.</ta>
            <ta e="T114" id="Seg_1695" s="T109">Ich gehe nach Novosondorovo und ich gehe fischen.</ta>
            <ta e="T117" id="Seg_1696" s="T114">Ich schicke dir Fisch.</ta>
            <ta e="T119" id="Seg_1697" s="T117">Du isst ihn.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_1698" s="T1">Я сидела вязала.</ta>
            <ta e="T7" id="Seg_1699" s="T4">Какая-то женщина прошла.</ta>
            <ta e="T12" id="Seg_1700" s="T7">Мне сказали, что это Евлашкина жена.</ta>
            <ta e="T15" id="Seg_1701" s="T12">Я сюда прибежала.</ta>
            <ta e="T17" id="Seg_1702" s="T15">Я пришла, поздоровалась.</ta>
            <ta e="T20" id="Seg_1703" s="T17">(Я с ней поздоровалась.)</ta>
            <ta e="T26" id="Seg_1704" s="T20">Я говорю: “Чего это, Евлашкина жена?”</ta>
            <ta e="T34" id="Seg_1705" s="T26">А они мне говорят: “Это не Евлашкина жена пришла”.</ta>
            <ta e="T40" id="Seg_1706" s="T34">А мне говорили, что Евлашкина жена.</ta>
            <ta e="T45" id="Seg_1707" s="T40">А я и рысью прибежала.</ta>
            <ta e="T48" id="Seg_1708" s="T45">Они мне соврали.</ta>
            <ta e="T53" id="Seg_1709" s="T48">А это Ангелина Ивановна пришла.</ta>
            <ta e="T56" id="Seg_1710" s="T53">Сидит, рыбу ест.</ta>
            <ta e="T63" id="Seg_1711" s="T56">А я у нее спросила: “Ты куда идешь?”</ta>
            <ta e="T73" id="Seg_1712" s="T63">А она сказала: “Я сюда приехала к вам, целый месяц жить буду.</ta>
            <ta e="T80" id="Seg_1713" s="T73">Я к вам приехала, селькупские слова писать буду”.</ta>
            <ta e="T89" id="Seg_1714" s="T80">А я сказала, мне дядя Михайла сказал: “Зови ее к себе.</ta>
            <ta e="T92" id="Seg_1715" s="T89">Ты одна.</ta>
            <ta e="T95" id="Seg_1716" s="T92">Ты одна”.</ta>
            <ta e="T99" id="Seg_1717" s="T95">Я ее к себе зазвала.</ta>
            <ta e="T104" id="Seg_1718" s="T99">Она ко мне пришла, у меня она жила.</ta>
            <ta e="T109" id="Seg_1719" s="T104">А теперь я у нее живу.</ta>
            <ta e="T114" id="Seg_1720" s="T109">Я поеду в Новосондорово, рыбу промышлять буду.</ta>
            <ta e="T117" id="Seg_1721" s="T114">Я тебе рыбу пришлю.</ta>
            <ta e="T119" id="Seg_1722" s="T117">Ты ее есть будешь.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_1723" s="T1">я сидела вязала</ta>
            <ta e="T7" id="Seg_1724" s="T4">какая-то женщина прошла</ta>
            <ta e="T12" id="Seg_1725" s="T7">мне сказали это Евлантиева жена</ta>
            <ta e="T15" id="Seg_1726" s="T12">я побежала</ta>
            <ta e="T17" id="Seg_1727" s="T15">пришла поздоровалась</ta>
            <ta e="T20" id="Seg_1728" s="T17">я с ней поздоровалась</ta>
            <ta e="T26" id="Seg_1729" s="T20">я говорю чего это Евлашкина баба</ta>
            <ta e="T34" id="Seg_1730" s="T26">мне</ta>
            <ta e="T40" id="Seg_1731" s="T34">There is no Russian translation of this sentence.</ta>
            <ta e="T45" id="Seg_1732" s="T40">я рысью прибежала</ta>
            <ta e="T48" id="Seg_1733" s="T45">они мне соврали</ta>
            <ta e="T53" id="Seg_1734" s="T48">а это пришла</ta>
            <ta e="T56" id="Seg_1735" s="T53">сидит рыбу ест</ta>
            <ta e="T63" id="Seg_1736" s="T56">а я у ней спросила ты куда идешь</ta>
            <ta e="T73" id="Seg_1737" s="T63">а она сказала я сюда приехала к вам на целый месяц (приехала) жить буду</ta>
            <ta e="T80" id="Seg_1738" s="T73">я к вам приехала остяцкие (по-остцки) слова писать буду</ta>
            <ta e="T89" id="Seg_1739" s="T80">мне дядя Михайла сказал зови к себе</ta>
            <ta e="T92" id="Seg_1740" s="T89">ты одна</ta>
            <ta e="T95" id="Seg_1741" s="T92">ты одна</ta>
            <ta e="T99" id="Seg_1742" s="T95">я и к себе зазвала</ta>
            <ta e="T104" id="Seg_1743" s="T99">она ко мне пришла у меня она жила</ta>
            <ta e="T109" id="Seg_1744" s="T104">я теперь я у нее живу</ta>
            <ta e="T114" id="Seg_1745" s="T109">я поеду в Новосондорово рыбу промышлять буду</ta>
            <ta e="T117" id="Seg_1746" s="T114">я тебе рыбку пришлю</ta>
            <ta e="T119" id="Seg_1747" s="T117">ты кушать (есть) будешь</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T7" id="Seg_1748" s="T4">[KuAI:] Variant: 'mennɨta'.</ta>
            <ta e="T15" id="Seg_1749" s="T12">[BrM:] Tentative analysis of 'tʼütdə'.</ta>
            <ta e="T26" id="Seg_1750" s="T20">[KuAI:] Variant: 'Jewlaškɨn'.</ta>
            <ta e="T34" id="Seg_1751" s="T26">[BrM:] Tentative analysis of 'tʼütdə'.</ta>
            <ta e="T45" id="Seg_1752" s="T40">[BrM:] Tentative analysis of 'koššedʼän'.</ta>
            <ta e="T89" id="Seg_1753" s="T80">[KuAI:] Variants: 'piːrʼäɣətdə', 'qwɛːrit'.</ta>
            <ta e="T95" id="Seg_1754" s="T92">[KuAI:] Variant: 'pelgattə'.</ta>
            <ta e="T104" id="Seg_1755" s="T99">[KuAI:] Variant: ''.</ta>
            <ta e="T117" id="Seg_1756" s="T114">[KuAI:] Variant: 'üdedʒan'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
