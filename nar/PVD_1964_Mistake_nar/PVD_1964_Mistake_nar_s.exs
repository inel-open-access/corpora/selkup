<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Mistake_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Mistake_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">69</ud-information>
            <ud-information attribute-name="# HIAT:w">51</ud-information>
            <ud-information attribute-name="# e">51</ud-information>
            <ud-information attribute-name="# HIAT:u">11</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T52" id="Seg_0" n="sc" s="T1">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Kɨdan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">paldʼükus</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">okkɨr</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">tebəɣum</ts>
                  <nts id="Seg_14" n="HIAT:ip">,</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_17" n="HIAT:w" s="T5">semʼelam</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_20" n="HIAT:w" s="T6">warkus</ts>
                  <nts id="Seg_21" n="HIAT:ip">.</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_24" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">Tepnan</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">qɨːʒaj</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">tufajkat</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_35" n="HIAT:w" s="T10">jes</ts>
                  <nts id="Seg_36" n="HIAT:ip">.</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_39" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">A</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">otdə</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">čaːorɨpɨs</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_51" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">A</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">patom</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">tüua</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_63" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">Satdə</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">porɣɨt</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">i</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">satdə</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_77" n="HIAT:w" s="T21">ügut</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_81" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">Man</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">tebɨm</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">as</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">kostau</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_96" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">Man</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">tʼaran</ts>
                  <nts id="Seg_102" n="HIAT:ip">:</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_104" n="HIAT:ip">“</nts>
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">Au</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_109" n="HIAT:w" s="T29">tebeɣum</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_112" n="HIAT:w" s="T30">qaj</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_115" n="HIAT:w" s="T31">to</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_118" n="HIAT:w" s="T32">üdɨbattə</ts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_122" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">Nawerna</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_127" n="HIAT:w" s="T34">arakaj</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_130" n="HIAT:w" s="T35">ärɨppa</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip">”</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_135" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_137" n="HIAT:w" s="T36">A</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_140" n="HIAT:w" s="T37">tep</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_143" n="HIAT:w" s="T38">tʼarɨn</ts>
                  <nts id="Seg_144" n="HIAT:ip">:</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_146" n="HIAT:ip">“</nts>
                  <ts e="T40" id="Seg_148" n="HIAT:w" s="T39">Na</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_151" n="HIAT:w" s="T40">man</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_154" n="HIAT:w" s="T41">jezan</ts>
                  <nts id="Seg_155" n="HIAT:ip">.</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_158" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_160" n="HIAT:w" s="T42">Tan</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_163" n="HIAT:w" s="T43">mazɨm</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_166" n="HIAT:w" s="T44">jas</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_169" n="HIAT:w" s="T45">kostat</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_173" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_175" n="HIAT:w" s="T46">Na</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_178" n="HIAT:w" s="T47">saqqən</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_181" n="HIAT:w" s="T48">manan</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_184" n="HIAT:w" s="T49">lačaj</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_187" n="HIAT:w" s="T50">tufajqaw</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_190" n="HIAT:w" s="T51">jes</ts>
                  <nts id="Seg_191" n="HIAT:ip">.</nts>
                  <nts id="Seg_192" n="HIAT:ip">”</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T52" id="Seg_194" n="sc" s="T1">
               <ts e="T2" id="Seg_196" n="e" s="T1">Kɨdan </ts>
               <ts e="T3" id="Seg_198" n="e" s="T2">paldʼükus </ts>
               <ts e="T4" id="Seg_200" n="e" s="T3">okkɨr </ts>
               <ts e="T5" id="Seg_202" n="e" s="T4">tebəɣum, </ts>
               <ts e="T6" id="Seg_204" n="e" s="T5">semʼelam </ts>
               <ts e="T7" id="Seg_206" n="e" s="T6">warkus. </ts>
               <ts e="T8" id="Seg_208" n="e" s="T7">Tepnan </ts>
               <ts e="T9" id="Seg_210" n="e" s="T8">qɨːʒaj </ts>
               <ts e="T10" id="Seg_212" n="e" s="T9">tufajkat </ts>
               <ts e="T11" id="Seg_214" n="e" s="T10">jes. </ts>
               <ts e="T12" id="Seg_216" n="e" s="T11">A </ts>
               <ts e="T13" id="Seg_218" n="e" s="T12">otdə </ts>
               <ts e="T14" id="Seg_220" n="e" s="T13">čaːorɨpɨs. </ts>
               <ts e="T15" id="Seg_222" n="e" s="T14">A </ts>
               <ts e="T16" id="Seg_224" n="e" s="T15">patom </ts>
               <ts e="T17" id="Seg_226" n="e" s="T16">tüua. </ts>
               <ts e="T18" id="Seg_228" n="e" s="T17">Satdə </ts>
               <ts e="T19" id="Seg_230" n="e" s="T18">porɣɨt </ts>
               <ts e="T20" id="Seg_232" n="e" s="T19">i </ts>
               <ts e="T21" id="Seg_234" n="e" s="T20">satdə </ts>
               <ts e="T22" id="Seg_236" n="e" s="T21">ügut. </ts>
               <ts e="T23" id="Seg_238" n="e" s="T22">Man </ts>
               <ts e="T24" id="Seg_240" n="e" s="T23">tebɨm </ts>
               <ts e="T25" id="Seg_242" n="e" s="T24">as </ts>
               <ts e="T26" id="Seg_244" n="e" s="T25">kostau. </ts>
               <ts e="T27" id="Seg_246" n="e" s="T26">Man </ts>
               <ts e="T28" id="Seg_248" n="e" s="T27">tʼaran: </ts>
               <ts e="T29" id="Seg_250" n="e" s="T28">“Au </ts>
               <ts e="T30" id="Seg_252" n="e" s="T29">tebeɣum </ts>
               <ts e="T31" id="Seg_254" n="e" s="T30">qaj </ts>
               <ts e="T32" id="Seg_256" n="e" s="T31">to </ts>
               <ts e="T33" id="Seg_258" n="e" s="T32">üdɨbattə. </ts>
               <ts e="T34" id="Seg_260" n="e" s="T33">Nawerna </ts>
               <ts e="T35" id="Seg_262" n="e" s="T34">arakaj </ts>
               <ts e="T36" id="Seg_264" n="e" s="T35">ärɨppa.” </ts>
               <ts e="T37" id="Seg_266" n="e" s="T36">A </ts>
               <ts e="T38" id="Seg_268" n="e" s="T37">tep </ts>
               <ts e="T39" id="Seg_270" n="e" s="T38">tʼarɨn: </ts>
               <ts e="T40" id="Seg_272" n="e" s="T39">“Na </ts>
               <ts e="T41" id="Seg_274" n="e" s="T40">man </ts>
               <ts e="T42" id="Seg_276" n="e" s="T41">jezan. </ts>
               <ts e="T43" id="Seg_278" n="e" s="T42">Tan </ts>
               <ts e="T44" id="Seg_280" n="e" s="T43">mazɨm </ts>
               <ts e="T45" id="Seg_282" n="e" s="T44">jas </ts>
               <ts e="T46" id="Seg_284" n="e" s="T45">kostat. </ts>
               <ts e="T47" id="Seg_286" n="e" s="T46">Na </ts>
               <ts e="T48" id="Seg_288" n="e" s="T47">saqqən </ts>
               <ts e="T49" id="Seg_290" n="e" s="T48">manan </ts>
               <ts e="T50" id="Seg_292" n="e" s="T49">lačaj </ts>
               <ts e="T51" id="Seg_294" n="e" s="T50">tufajqaw </ts>
               <ts e="T52" id="Seg_296" n="e" s="T51">jes.” </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_297" s="T1">PVD_1964_Mistake_nar.001 (001.001)</ta>
            <ta e="T11" id="Seg_298" s="T7">PVD_1964_Mistake_nar.002 (001.002)</ta>
            <ta e="T14" id="Seg_299" s="T11">PVD_1964_Mistake_nar.003 (001.003)</ta>
            <ta e="T17" id="Seg_300" s="T14">PVD_1964_Mistake_nar.004 (001.004)</ta>
            <ta e="T22" id="Seg_301" s="T17">PVD_1964_Mistake_nar.005 (001.005)</ta>
            <ta e="T26" id="Seg_302" s="T22">PVD_1964_Mistake_nar.006 (001.006)</ta>
            <ta e="T33" id="Seg_303" s="T26">PVD_1964_Mistake_nar.007 (001.007)</ta>
            <ta e="T36" id="Seg_304" s="T33">PVD_1964_Mistake_nar.008 (001.008)</ta>
            <ta e="T42" id="Seg_305" s="T36">PVD_1964_Mistake_nar.009 (001.009)</ta>
            <ta e="T46" id="Seg_306" s="T42">PVD_1964_Mistake_nar.010 (001.010)</ta>
            <ta e="T52" id="Seg_307" s="T46">PVD_1964_Mistake_nar.011 (001.011)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T7" id="Seg_308" s="T1">′кыдан ′паlдʼӱкус оккыр ′тебъɣум, ′семʼелам ′варкус.</ta>
            <ta e="T11" id="Seg_309" s="T7">тепнан kы̄′жай ту′файкат jес.</ta>
            <ta e="T14" id="Seg_310" s="T11">а ′отдъ ′тша̄орыпыс.</ta>
            <ta e="T17" id="Seg_311" s="T14">а па′том тӱу(w)а.</ta>
            <ta e="T22" id="Seg_312" s="T17">′сатдъ ′порɣыт и ′сатдъ ′ӱгут.</ta>
            <ta e="T26" id="Seg_313" s="T22">ман те′бым ас кос′тау.</ta>
            <ta e="T33" id="Seg_314" s="T26">ман тʼа′ран: ау ′те̨беɣум kай то ′ӱдыбаттъ.</ta>
            <ta e="T36" id="Seg_315" s="T33">на′верна а′ракай ӓрыппа.</ta>
            <ta e="T42" id="Seg_316" s="T36">а теп тʼарын: на ман ′jезан.</ta>
            <ta e="T46" id="Seg_317" s="T42">тан ма′зым jас кос′тат.</ta>
            <ta e="T52" id="Seg_318" s="T46">на ′саккън ма′нан ла′тшай туфайкав jес.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T7" id="Seg_319" s="T1">kɨdan paldʼükus okkɨr tebəɣum, semʼelam warkus.</ta>
            <ta e="T11" id="Seg_320" s="T7">tepnan qɨːʒaj tufajkat jes.</ta>
            <ta e="T14" id="Seg_321" s="T11">a otdə tšaːorɨpɨs.</ta>
            <ta e="T17" id="Seg_322" s="T14">a patom tüu(w)a.</ta>
            <ta e="T22" id="Seg_323" s="T17">satdə porɣɨt i satdə ügut.</ta>
            <ta e="T26" id="Seg_324" s="T22">man tebɨm as kostau.</ta>
            <ta e="T33" id="Seg_325" s="T26">man tʼaran: au tebeɣum qaj to üdɨbattə.</ta>
            <ta e="T36" id="Seg_326" s="T33">nawerna arakaj ärɨppa.</ta>
            <ta e="T42" id="Seg_327" s="T36">a tep tʼarɨn: na man jezan.</ta>
            <ta e="T46" id="Seg_328" s="T42">tan mazɨm jas kostat.</ta>
            <ta e="T52" id="Seg_329" s="T46">na saqqən manan latšaj tufajqaw jes.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_330" s="T1">Kɨdan paldʼükus okkɨr tebəɣum, semʼelam warkus. </ta>
            <ta e="T11" id="Seg_331" s="T7">Tepnan qɨːʒaj tufajkat jes. </ta>
            <ta e="T14" id="Seg_332" s="T11">A otdə čaːorɨpɨs. </ta>
            <ta e="T17" id="Seg_333" s="T14">A patom tüua. </ta>
            <ta e="T22" id="Seg_334" s="T17">Satdə porɣɨt i satdə ügut. </ta>
            <ta e="T26" id="Seg_335" s="T22">Man tebɨm as kostau. </ta>
            <ta e="T33" id="Seg_336" s="T26">Man tʼaran: “Au tebeɣum qaj to üdɨbattə. </ta>
            <ta e="T36" id="Seg_337" s="T33">Nawerna arakaj ärɨppa.” </ta>
            <ta e="T42" id="Seg_338" s="T36">A tep tʼarɨn: “Na man jezan. </ta>
            <ta e="T46" id="Seg_339" s="T42">Tan mazɨm jas kostat. </ta>
            <ta e="T52" id="Seg_340" s="T46">Na saqqən manan lačaj tufajqaw jes.” </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_341" s="T1">kɨdan</ta>
            <ta e="T3" id="Seg_342" s="T2">paldʼü-ku-s</ta>
            <ta e="T4" id="Seg_343" s="T3">okkɨr</ta>
            <ta e="T5" id="Seg_344" s="T4">tebə-ɣum</ta>
            <ta e="T6" id="Seg_345" s="T5">semʼe-la-m</ta>
            <ta e="T7" id="Seg_346" s="T6">warku-s</ta>
            <ta e="T8" id="Seg_347" s="T7">tep-nan</ta>
            <ta e="T9" id="Seg_348" s="T8">qɨːʒaj</ta>
            <ta e="T10" id="Seg_349" s="T9">tufajka-t</ta>
            <ta e="T11" id="Seg_350" s="T10">je-s</ta>
            <ta e="T12" id="Seg_351" s="T11">a</ta>
            <ta e="T13" id="Seg_352" s="T12">otdə</ta>
            <ta e="T14" id="Seg_353" s="T13">čaːo-r-ɨ-pɨ-s</ta>
            <ta e="T15" id="Seg_354" s="T14">a</ta>
            <ta e="T16" id="Seg_355" s="T15">patom</ta>
            <ta e="T17" id="Seg_356" s="T16">tü-ua</ta>
            <ta e="T18" id="Seg_357" s="T17">satdə</ta>
            <ta e="T19" id="Seg_358" s="T18">porɣɨ-t</ta>
            <ta e="T20" id="Seg_359" s="T19">i</ta>
            <ta e="T21" id="Seg_360" s="T20">satdə</ta>
            <ta e="T22" id="Seg_361" s="T21">ügu-t</ta>
            <ta e="T23" id="Seg_362" s="T22">man</ta>
            <ta e="T24" id="Seg_363" s="T23">teb-ɨ-m</ta>
            <ta e="T25" id="Seg_364" s="T24">as</ta>
            <ta e="T26" id="Seg_365" s="T25">kosta-u</ta>
            <ta e="T27" id="Seg_366" s="T26">man</ta>
            <ta e="T28" id="Seg_367" s="T27">tʼara-n</ta>
            <ta e="T29" id="Seg_368" s="T28">au</ta>
            <ta e="T30" id="Seg_369" s="T29">tebe-ɣum</ta>
            <ta e="T31" id="Seg_370" s="T30">qaj</ta>
            <ta e="T32" id="Seg_371" s="T31">to</ta>
            <ta e="T33" id="Seg_372" s="T32">üdɨ-ba-ttə</ta>
            <ta e="T34" id="Seg_373" s="T33">nawerna</ta>
            <ta e="T35" id="Seg_374" s="T34">araka-j</ta>
            <ta e="T36" id="Seg_375" s="T35">ärɨ-ppa</ta>
            <ta e="T37" id="Seg_376" s="T36">a</ta>
            <ta e="T38" id="Seg_377" s="T37">tep</ta>
            <ta e="T39" id="Seg_378" s="T38">tʼarɨ-n</ta>
            <ta e="T40" id="Seg_379" s="T39">na</ta>
            <ta e="T41" id="Seg_380" s="T40">man</ta>
            <ta e="T42" id="Seg_381" s="T41">je-za-n</ta>
            <ta e="T43" id="Seg_382" s="T42">tat</ta>
            <ta e="T44" id="Seg_383" s="T43">mazɨm</ta>
            <ta e="T45" id="Seg_384" s="T44">jas</ta>
            <ta e="T46" id="Seg_385" s="T45">kosta-t</ta>
            <ta e="T47" id="Seg_386" s="T46">na</ta>
            <ta e="T48" id="Seg_387" s="T47">saq-qən</ta>
            <ta e="T49" id="Seg_388" s="T48">ma-nan</ta>
            <ta e="T50" id="Seg_389" s="T49">lačaj</ta>
            <ta e="T51" id="Seg_390" s="T50">tufajqa-w</ta>
            <ta e="T52" id="Seg_391" s="T51">je-s</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_392" s="T1">qɨdan</ta>
            <ta e="T3" id="Seg_393" s="T2">palʼdʼi-ku-sɨ</ta>
            <ta e="T4" id="Seg_394" s="T3">okkɨr</ta>
            <ta e="T5" id="Seg_395" s="T4">täbe-qum</ta>
            <ta e="T6" id="Seg_396" s="T5">sʼemʼa-la-m</ta>
            <ta e="T7" id="Seg_397" s="T6">warkɨ-sɨ</ta>
            <ta e="T8" id="Seg_398" s="T7">täp-nan</ta>
            <ta e="T9" id="Seg_399" s="T8">qɨːʒaj</ta>
            <ta e="T10" id="Seg_400" s="T9">tufajka-tə</ta>
            <ta e="T11" id="Seg_401" s="T10">eː-sɨ</ta>
            <ta e="T12" id="Seg_402" s="T11">a</ta>
            <ta e="T13" id="Seg_403" s="T12">ondə</ta>
            <ta e="T14" id="Seg_404" s="T13">čam-r-ɨ-mbɨ-sɨ</ta>
            <ta e="T15" id="Seg_405" s="T14">a</ta>
            <ta e="T16" id="Seg_406" s="T15">patom</ta>
            <ta e="T17" id="Seg_407" s="T16">tüː-nɨ</ta>
            <ta e="T18" id="Seg_408" s="T17">saddə</ta>
            <ta e="T19" id="Seg_409" s="T18">porɣə-tə</ta>
            <ta e="T20" id="Seg_410" s="T19">i</ta>
            <ta e="T21" id="Seg_411" s="T20">saddə</ta>
            <ta e="T22" id="Seg_412" s="T21">ügu-tə</ta>
            <ta e="T23" id="Seg_413" s="T22">man</ta>
            <ta e="T24" id="Seg_414" s="T23">täp-ɨ-m</ta>
            <ta e="T25" id="Seg_415" s="T24">asa</ta>
            <ta e="T26" id="Seg_416" s="T25">kostɨ-w</ta>
            <ta e="T27" id="Seg_417" s="T26">man</ta>
            <ta e="T28" id="Seg_418" s="T27">tʼärɨ-ŋ</ta>
            <ta e="T29" id="Seg_419" s="T28">au</ta>
            <ta e="T30" id="Seg_420" s="T29">täbe-qum</ta>
            <ta e="T31" id="Seg_421" s="T30">qaj</ta>
            <ta e="T32" id="Seg_422" s="T31">to</ta>
            <ta e="T33" id="Seg_423" s="T32">üdə-mbɨ-t</ta>
            <ta e="T34" id="Seg_424" s="T33">nawerna</ta>
            <ta e="T35" id="Seg_425" s="T34">araŋka-lʼ</ta>
            <ta e="T36" id="Seg_426" s="T35">öru-mbɨ</ta>
            <ta e="T37" id="Seg_427" s="T36">a</ta>
            <ta e="T38" id="Seg_428" s="T37">täp</ta>
            <ta e="T39" id="Seg_429" s="T38">tʼärɨ-n</ta>
            <ta e="T40" id="Seg_430" s="T39">na</ta>
            <ta e="T41" id="Seg_431" s="T40">man</ta>
            <ta e="T42" id="Seg_432" s="T41">eː-sɨ-ŋ</ta>
            <ta e="T43" id="Seg_433" s="T42">tan</ta>
            <ta e="T44" id="Seg_434" s="T43">mazɨm</ta>
            <ta e="T45" id="Seg_435" s="T44">asa</ta>
            <ta e="T46" id="Seg_436" s="T45">kostɨ-ntə</ta>
            <ta e="T47" id="Seg_437" s="T46">na</ta>
            <ta e="T48" id="Seg_438" s="T47">saŋ-qɨn</ta>
            <ta e="T49" id="Seg_439" s="T48">man-nan</ta>
            <ta e="T50" id="Seg_440" s="T49">lačaj</ta>
            <ta e="T51" id="Seg_441" s="T50">tufajka-w</ta>
            <ta e="T52" id="Seg_442" s="T51">eː-sɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_443" s="T1">all.the.time</ta>
            <ta e="T3" id="Seg_444" s="T2">walk-HAB-PST.[3SG.S]</ta>
            <ta e="T4" id="Seg_445" s="T3">one</ta>
            <ta e="T5" id="Seg_446" s="T4">man-human.being.[NOM]</ta>
            <ta e="T6" id="Seg_447" s="T5">seed-PL-ACC</ta>
            <ta e="T7" id="Seg_448" s="T6">%%-PST.[3SG.S]</ta>
            <ta e="T8" id="Seg_449" s="T7">(s)he-ADES</ta>
            <ta e="T9" id="Seg_450" s="T8">%torn</ta>
            <ta e="T10" id="Seg_451" s="T9">sweater.[NOM]-3SG</ta>
            <ta e="T11" id="Seg_452" s="T10">be-PST.[3SG.S]</ta>
            <ta e="T12" id="Seg_453" s="T11">and</ta>
            <ta e="T13" id="Seg_454" s="T12">oneself.3SG</ta>
            <ta e="T14" id="Seg_455" s="T13">dirt-VBLZ-EP-RES-PST.[3SG.S]</ta>
            <ta e="T15" id="Seg_456" s="T14">and</ta>
            <ta e="T16" id="Seg_457" s="T15">then</ta>
            <ta e="T17" id="Seg_458" s="T16">come-CO.[3SG.S]</ta>
            <ta e="T18" id="Seg_459" s="T17">new</ta>
            <ta e="T19" id="Seg_460" s="T18">fur.coat.[NOM]-3SG</ta>
            <ta e="T20" id="Seg_461" s="T19">and</ta>
            <ta e="T21" id="Seg_462" s="T20">new</ta>
            <ta e="T22" id="Seg_463" s="T21">cap.[NOM]-3SG</ta>
            <ta e="T23" id="Seg_464" s="T22">I.NOM</ta>
            <ta e="T24" id="Seg_465" s="T23">(s)he-EP-ACC</ta>
            <ta e="T25" id="Seg_466" s="T24">NEG</ta>
            <ta e="T26" id="Seg_467" s="T25">get.to.know-1SG.O</ta>
            <ta e="T27" id="Seg_468" s="T26">I.NOM</ta>
            <ta e="T28" id="Seg_469" s="T27">say-1SG.S</ta>
            <ta e="T29" id="Seg_470" s="T28">other</ta>
            <ta e="T30" id="Seg_471" s="T29">man-human.being.[NOM]</ta>
            <ta e="T31" id="Seg_472" s="T30">either</ta>
            <ta e="T32" id="Seg_473" s="T31">that.[NOM]</ta>
            <ta e="T33" id="Seg_474" s="T32">send-PST.NAR-3SG.O</ta>
            <ta e="T34" id="Seg_475" s="T33">probably</ta>
            <ta e="T35" id="Seg_476" s="T34">vodka-ADJZ</ta>
            <ta e="T36" id="Seg_477" s="T35">drink-PST.NAR.[3SG.S]</ta>
            <ta e="T37" id="Seg_478" s="T36">and</ta>
            <ta e="T38" id="Seg_479" s="T37">(s)he.[NOM]</ta>
            <ta e="T39" id="Seg_480" s="T38">say-3SG.S</ta>
            <ta e="T40" id="Seg_481" s="T39">this</ta>
            <ta e="T41" id="Seg_482" s="T40">I.NOM</ta>
            <ta e="T42" id="Seg_483" s="T41">be-PST-1SG.S</ta>
            <ta e="T43" id="Seg_484" s="T42">you.SG.NOM</ta>
            <ta e="T44" id="Seg_485" s="T43">I.ACC</ta>
            <ta e="T45" id="Seg_486" s="T44">NEG</ta>
            <ta e="T46" id="Seg_487" s="T45">get.to.know-2SG.S</ta>
            <ta e="T47" id="Seg_488" s="T46">this.[NOM]</ta>
            <ta e="T48" id="Seg_489" s="T47">approximately.as-LOC</ta>
            <ta e="T49" id="Seg_490" s="T48">I-ADES</ta>
            <ta e="T50" id="Seg_491" s="T49">%torn</ta>
            <ta e="T51" id="Seg_492" s="T50">sweater.[NOM]-1SG</ta>
            <ta e="T52" id="Seg_493" s="T51">be-PST.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_494" s="T1">все.время</ta>
            <ta e="T3" id="Seg_495" s="T2">ходить-HAB-PST.[3SG.S]</ta>
            <ta e="T4" id="Seg_496" s="T3">один</ta>
            <ta e="T5" id="Seg_497" s="T4">мужчина-человек.[NOM]</ta>
            <ta e="T6" id="Seg_498" s="T5">семя-PL-ACC</ta>
            <ta e="T7" id="Seg_499" s="T6">%%-PST.[3SG.S]</ta>
            <ta e="T8" id="Seg_500" s="T7">он(а)-ADES</ta>
            <ta e="T9" id="Seg_501" s="T8">%рваный</ta>
            <ta e="T10" id="Seg_502" s="T9">фуфайка.[NOM]-3SG</ta>
            <ta e="T11" id="Seg_503" s="T10">быть-PST.[3SG.S]</ta>
            <ta e="T12" id="Seg_504" s="T11">а</ta>
            <ta e="T13" id="Seg_505" s="T12">сам.3SG</ta>
            <ta e="T14" id="Seg_506" s="T13">грязь-VBLZ-EP-RES-PST.[3SG.S]</ta>
            <ta e="T15" id="Seg_507" s="T14">а</ta>
            <ta e="T16" id="Seg_508" s="T15">потом</ta>
            <ta e="T17" id="Seg_509" s="T16">приехать-CO.[3SG.S]</ta>
            <ta e="T18" id="Seg_510" s="T17">новый</ta>
            <ta e="T19" id="Seg_511" s="T18">шуба.[NOM]-3SG</ta>
            <ta e="T20" id="Seg_512" s="T19">и</ta>
            <ta e="T21" id="Seg_513" s="T20">новый</ta>
            <ta e="T22" id="Seg_514" s="T21">шапка.[NOM]-3SG</ta>
            <ta e="T23" id="Seg_515" s="T22">я.NOM</ta>
            <ta e="T24" id="Seg_516" s="T23">он(а)-EP-ACC</ta>
            <ta e="T25" id="Seg_517" s="T24">NEG</ta>
            <ta e="T26" id="Seg_518" s="T25">узнать-1SG.O</ta>
            <ta e="T27" id="Seg_519" s="T26">я.NOM</ta>
            <ta e="T28" id="Seg_520" s="T27">сказать-1SG.S</ta>
            <ta e="T29" id="Seg_521" s="T28">другой</ta>
            <ta e="T30" id="Seg_522" s="T29">мужчина-человек.[NOM]</ta>
            <ta e="T31" id="Seg_523" s="T30">ли</ta>
            <ta e="T32" id="Seg_524" s="T31">тот.[NOM]</ta>
            <ta e="T33" id="Seg_525" s="T32">посылать-PST.NAR-3SG.O</ta>
            <ta e="T34" id="Seg_526" s="T33">наверное</ta>
            <ta e="T35" id="Seg_527" s="T34">водка-ADJZ</ta>
            <ta e="T36" id="Seg_528" s="T35">пить-PST.NAR.[3SG.S]</ta>
            <ta e="T37" id="Seg_529" s="T36">а</ta>
            <ta e="T38" id="Seg_530" s="T37">он(а).[NOM]</ta>
            <ta e="T39" id="Seg_531" s="T38">сказать-3SG.S</ta>
            <ta e="T40" id="Seg_532" s="T39">этот</ta>
            <ta e="T41" id="Seg_533" s="T40">я.NOM</ta>
            <ta e="T42" id="Seg_534" s="T41">быть-PST-1SG.S</ta>
            <ta e="T43" id="Seg_535" s="T42">ты.NOM</ta>
            <ta e="T44" id="Seg_536" s="T43">я.ACC</ta>
            <ta e="T45" id="Seg_537" s="T44">NEG</ta>
            <ta e="T46" id="Seg_538" s="T45">узнать-2SG.S</ta>
            <ta e="T47" id="Seg_539" s="T46">этот.[NOM]</ta>
            <ta e="T48" id="Seg_540" s="T47">приблизительно.как-LOC</ta>
            <ta e="T49" id="Seg_541" s="T48">я-ADES</ta>
            <ta e="T50" id="Seg_542" s="T49">%рваный</ta>
            <ta e="T51" id="Seg_543" s="T50">фуфайка.[NOM]-1SG</ta>
            <ta e="T52" id="Seg_544" s="T51">быть-PST.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_545" s="T1">adv</ta>
            <ta e="T3" id="Seg_546" s="T2">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T4" id="Seg_547" s="T3">num</ta>
            <ta e="T5" id="Seg_548" s="T4">n-n.[n:case]</ta>
            <ta e="T6" id="Seg_549" s="T5">n-n:num-n:case</ta>
            <ta e="T7" id="Seg_550" s="T6">v-v:tense.[v:pn]</ta>
            <ta e="T8" id="Seg_551" s="T7">pers-n:case</ta>
            <ta e="T9" id="Seg_552" s="T8">adj</ta>
            <ta e="T10" id="Seg_553" s="T9">n.[n:case]-n:poss</ta>
            <ta e="T11" id="Seg_554" s="T10">v-v:tense.[v:pn]</ta>
            <ta e="T12" id="Seg_555" s="T11">conj</ta>
            <ta e="T13" id="Seg_556" s="T12">emphpro</ta>
            <ta e="T14" id="Seg_557" s="T13">n-n&gt;v-v:ins-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T15" id="Seg_558" s="T14">conj</ta>
            <ta e="T16" id="Seg_559" s="T15">adv</ta>
            <ta e="T17" id="Seg_560" s="T16">v-v:ins.[v:pn]</ta>
            <ta e="T18" id="Seg_561" s="T17">adj</ta>
            <ta e="T19" id="Seg_562" s="T18">n.[n:case]-n:poss</ta>
            <ta e="T20" id="Seg_563" s="T19">conj</ta>
            <ta e="T21" id="Seg_564" s="T20">adj</ta>
            <ta e="T22" id="Seg_565" s="T21">n.[n:case]-n:poss</ta>
            <ta e="T23" id="Seg_566" s="T22">pers</ta>
            <ta e="T24" id="Seg_567" s="T23">pers-n:ins-n:case</ta>
            <ta e="T25" id="Seg_568" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_569" s="T25">v-v:pn</ta>
            <ta e="T27" id="Seg_570" s="T26">pers</ta>
            <ta e="T28" id="Seg_571" s="T27">v-v:pn</ta>
            <ta e="T29" id="Seg_572" s="T28">adj</ta>
            <ta e="T30" id="Seg_573" s="T29">n-n.[n:case]</ta>
            <ta e="T31" id="Seg_574" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_575" s="T31">dem.[n:case]</ta>
            <ta e="T33" id="Seg_576" s="T32">v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_577" s="T33">adv</ta>
            <ta e="T35" id="Seg_578" s="T34">n-n&gt;adj</ta>
            <ta e="T36" id="Seg_579" s="T35">v-v:tense.[v:pn]</ta>
            <ta e="T37" id="Seg_580" s="T36">conj</ta>
            <ta e="T38" id="Seg_581" s="T37">pers.[n:case]</ta>
            <ta e="T39" id="Seg_582" s="T38">v-v:pn</ta>
            <ta e="T40" id="Seg_583" s="T39">dem</ta>
            <ta e="T41" id="Seg_584" s="T40">pers</ta>
            <ta e="T42" id="Seg_585" s="T41">v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_586" s="T42">pers</ta>
            <ta e="T44" id="Seg_587" s="T43">pers</ta>
            <ta e="T45" id="Seg_588" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_589" s="T45">v-v:pn</ta>
            <ta e="T47" id="Seg_590" s="T46">dem.[n:case]</ta>
            <ta e="T48" id="Seg_591" s="T47">pp-n:case</ta>
            <ta e="T49" id="Seg_592" s="T48">pers-n:case</ta>
            <ta e="T50" id="Seg_593" s="T49">adj</ta>
            <ta e="T51" id="Seg_594" s="T50">n.[n:case]-n:poss</ta>
            <ta e="T52" id="Seg_595" s="T51">v-v:tense.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_596" s="T1">adv</ta>
            <ta e="T3" id="Seg_597" s="T2">v</ta>
            <ta e="T4" id="Seg_598" s="T3">num</ta>
            <ta e="T5" id="Seg_599" s="T4">n</ta>
            <ta e="T6" id="Seg_600" s="T5">n</ta>
            <ta e="T7" id="Seg_601" s="T6">v</ta>
            <ta e="T8" id="Seg_602" s="T7">pers</ta>
            <ta e="T9" id="Seg_603" s="T8">adj</ta>
            <ta e="T10" id="Seg_604" s="T9">n</ta>
            <ta e="T11" id="Seg_605" s="T10">v</ta>
            <ta e="T12" id="Seg_606" s="T11">conj</ta>
            <ta e="T13" id="Seg_607" s="T12">emphpro</ta>
            <ta e="T14" id="Seg_608" s="T13">v</ta>
            <ta e="T15" id="Seg_609" s="T14">conj</ta>
            <ta e="T16" id="Seg_610" s="T15">adv</ta>
            <ta e="T17" id="Seg_611" s="T16">v</ta>
            <ta e="T18" id="Seg_612" s="T17">adj</ta>
            <ta e="T19" id="Seg_613" s="T18">n</ta>
            <ta e="T20" id="Seg_614" s="T19">conj</ta>
            <ta e="T21" id="Seg_615" s="T20">adj</ta>
            <ta e="T22" id="Seg_616" s="T21">n</ta>
            <ta e="T23" id="Seg_617" s="T22">pers</ta>
            <ta e="T24" id="Seg_618" s="T23">pers</ta>
            <ta e="T25" id="Seg_619" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_620" s="T25">v</ta>
            <ta e="T27" id="Seg_621" s="T26">pers</ta>
            <ta e="T28" id="Seg_622" s="T27">v</ta>
            <ta e="T29" id="Seg_623" s="T28">adj</ta>
            <ta e="T30" id="Seg_624" s="T29">n</ta>
            <ta e="T31" id="Seg_625" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_626" s="T31">dem</ta>
            <ta e="T33" id="Seg_627" s="T32">v</ta>
            <ta e="T34" id="Seg_628" s="T33">adv</ta>
            <ta e="T35" id="Seg_629" s="T34">adj</ta>
            <ta e="T36" id="Seg_630" s="T35">v</ta>
            <ta e="T37" id="Seg_631" s="T36">conj</ta>
            <ta e="T38" id="Seg_632" s="T37">pers</ta>
            <ta e="T39" id="Seg_633" s="T38">v</ta>
            <ta e="T40" id="Seg_634" s="T39">dem</ta>
            <ta e="T41" id="Seg_635" s="T40">pers</ta>
            <ta e="T42" id="Seg_636" s="T41">v</ta>
            <ta e="T43" id="Seg_637" s="T42">pers</ta>
            <ta e="T44" id="Seg_638" s="T43">pers</ta>
            <ta e="T45" id="Seg_639" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_640" s="T45">v</ta>
            <ta e="T47" id="Seg_641" s="T46">dem</ta>
            <ta e="T48" id="Seg_642" s="T47">pp</ta>
            <ta e="T49" id="Seg_643" s="T48">pers</ta>
            <ta e="T50" id="Seg_644" s="T49">adj</ta>
            <ta e="T51" id="Seg_645" s="T50">n</ta>
            <ta e="T52" id="Seg_646" s="T51">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_647" s="T1">adv:Time</ta>
            <ta e="T5" id="Seg_648" s="T4">np.h:A</ta>
            <ta e="T6" id="Seg_649" s="T5">np:Th</ta>
            <ta e="T7" id="Seg_650" s="T6">0.3.h:A</ta>
            <ta e="T8" id="Seg_651" s="T7">pro.h:Poss</ta>
            <ta e="T10" id="Seg_652" s="T9">np:Th</ta>
            <ta e="T14" id="Seg_653" s="T13">0.3.h:Th</ta>
            <ta e="T16" id="Seg_654" s="T15">adv:Time</ta>
            <ta e="T17" id="Seg_655" s="T16">0.3.h:A</ta>
            <ta e="T19" id="Seg_656" s="T18">np:Th 0.3.h:Poss</ta>
            <ta e="T22" id="Seg_657" s="T21">np:Th 0.3.h:Poss</ta>
            <ta e="T23" id="Seg_658" s="T22">pro.h:E</ta>
            <ta e="T24" id="Seg_659" s="T23">pro.h:Th</ta>
            <ta e="T27" id="Seg_660" s="T26">pro.h:A</ta>
            <ta e="T30" id="Seg_661" s="T29">np.h:Th</ta>
            <ta e="T33" id="Seg_662" s="T32">0.3.h:A</ta>
            <ta e="T35" id="Seg_663" s="T34">np:P</ta>
            <ta e="T36" id="Seg_664" s="T35">0.3.h:A</ta>
            <ta e="T38" id="Seg_665" s="T37">pro.h:A</ta>
            <ta e="T41" id="Seg_666" s="T40">pro.h:Th</ta>
            <ta e="T43" id="Seg_667" s="T42">pro.h:E</ta>
            <ta e="T44" id="Seg_668" s="T43">pro.h:Th</ta>
            <ta e="T49" id="Seg_669" s="T48">pro.h:Poss</ta>
            <ta e="T51" id="Seg_670" s="T50">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_671" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_672" s="T4">np.h:S</ta>
            <ta e="T6" id="Seg_673" s="T5">np:O</ta>
            <ta e="T7" id="Seg_674" s="T6">0.3.h:S v:pred</ta>
            <ta e="T10" id="Seg_675" s="T9">np:S</ta>
            <ta e="T11" id="Seg_676" s="T10">v:pred</ta>
            <ta e="T14" id="Seg_677" s="T13">0.3.h:S v:pred</ta>
            <ta e="T17" id="Seg_678" s="T16">0.3.h:S v:pred</ta>
            <ta e="T19" id="Seg_679" s="T18">np:S</ta>
            <ta e="T22" id="Seg_680" s="T21">np:S</ta>
            <ta e="T23" id="Seg_681" s="T22">pro.h:S</ta>
            <ta e="T24" id="Seg_682" s="T23">pro.h:O</ta>
            <ta e="T26" id="Seg_683" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_684" s="T26">pro.h:S</ta>
            <ta e="T28" id="Seg_685" s="T27">v:pred</ta>
            <ta e="T30" id="Seg_686" s="T29">np.h:O</ta>
            <ta e="T33" id="Seg_687" s="T32">0.3.h:S v:pred</ta>
            <ta e="T35" id="Seg_688" s="T34">np:O</ta>
            <ta e="T36" id="Seg_689" s="T35">0.3.h:S v:pred</ta>
            <ta e="T38" id="Seg_690" s="T37">pro.h:S</ta>
            <ta e="T39" id="Seg_691" s="T38">v:pred</ta>
            <ta e="T41" id="Seg_692" s="T40">pro.h:S</ta>
            <ta e="T42" id="Seg_693" s="T41">v:pred</ta>
            <ta e="T43" id="Seg_694" s="T42">pro.h:S</ta>
            <ta e="T44" id="Seg_695" s="T43">pro.h:O</ta>
            <ta e="T46" id="Seg_696" s="T45">v:pred</ta>
            <ta e="T51" id="Seg_697" s="T50">np:S</ta>
            <ta e="T52" id="Seg_698" s="T51">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T6" id="Seg_699" s="T5">RUS:core</ta>
            <ta e="T10" id="Seg_700" s="T9">RUS:cult</ta>
            <ta e="T12" id="Seg_701" s="T11">RUS:gram</ta>
            <ta e="T15" id="Seg_702" s="T14">RUS:gram</ta>
            <ta e="T16" id="Seg_703" s="T15">RUS:disc</ta>
            <ta e="T20" id="Seg_704" s="T19">RUS:gram</ta>
            <ta e="T34" id="Seg_705" s="T33">RUS:mod</ta>
            <ta e="T37" id="Seg_706" s="T36">RUS:gram</ta>
            <ta e="T51" id="Seg_707" s="T50">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_708" s="T1">One man was coming all the time, bringing(?) seeds.</ta>
            <ta e="T11" id="Seg_709" s="T7">He was wearing a ragged sweater.</ta>
            <ta e="T14" id="Seg_710" s="T11">And he himself was dirty.</ta>
            <ta e="T17" id="Seg_711" s="T14">And then he came.</ta>
            <ta e="T22" id="Seg_712" s="T17">He had a new fur coat and a new hat.</ta>
            <ta e="T26" id="Seg_713" s="T22">I didn't recognize him.</ta>
            <ta e="T33" id="Seg_714" s="T26">I said: “Probably he sent another man.</ta>
            <ta e="T36" id="Seg_715" s="T33">Probably, he had been drinking vodka.”</ta>
            <ta e="T42" id="Seg_716" s="T36">And he said: “That was me.</ta>
            <ta e="T46" id="Seg_717" s="T42">You didn't recognize me.</ta>
            <ta e="T52" id="Seg_718" s="T46">I was wearing such a ragged sweater.”</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_719" s="T1">Ein Mann kam immer wieder, er brachte(?) Samen.</ta>
            <ta e="T11" id="Seg_720" s="T7">Er trug einen zerrissenen Pullover.</ta>
            <ta e="T14" id="Seg_721" s="T11">Und er selbst war dreckig.</ta>
            <ta e="T17" id="Seg_722" s="T14">Und dann kam er.</ta>
            <ta e="T22" id="Seg_723" s="T17">Er hatte einen neuen Pelzmantel und einen neuen Hut.</ta>
            <ta e="T26" id="Seg_724" s="T22">Ich erkannte ihn nicht.</ta>
            <ta e="T33" id="Seg_725" s="T26">Ich sagte: "Vielleicht hat er einen anderen Mann geschickt.</ta>
            <ta e="T36" id="Seg_726" s="T33">Vielleicht hat er Wodka getrunken."</ta>
            <ta e="T42" id="Seg_727" s="T36">Und er sagte: "Das war ich.</ta>
            <ta e="T46" id="Seg_728" s="T42">Du hast mich nicht erkannt.</ta>
            <ta e="T52" id="Seg_729" s="T46">Ich habe so einen zerrissenen Pullover getragen."</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_730" s="T1">Все время ездил один мужик, семена возил(?).</ta>
            <ta e="T11" id="Seg_731" s="T7">У него порванная(?) фуфайка была.</ta>
            <ta e="T14" id="Seg_732" s="T11">А сам грязный был.</ta>
            <ta e="T17" id="Seg_733" s="T14">А потом приехал.</ta>
            <ta e="T22" id="Seg_734" s="T17">У него новая шуба и новая шапка.</ta>
            <ta e="T26" id="Seg_735" s="T22">Я его не узнала.</ta>
            <ta e="T33" id="Seg_736" s="T26">Я сказала: “Другого человека, наверное, тот послал.</ta>
            <ta e="T36" id="Seg_737" s="T33">Наверное, водки выпил.”</ta>
            <ta e="T42" id="Seg_738" s="T36">А он сказал: “Это я был.</ta>
            <ta e="T46" id="Seg_739" s="T42">Ты меня не узнала.</ta>
            <ta e="T52" id="Seg_740" s="T46">Такая у меня рваная фуфайка была”.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_741" s="T1">все время ездил один мужик семена возил</ta>
            <ta e="T11" id="Seg_742" s="T7">у него порванная фуфайка была</ta>
            <ta e="T14" id="Seg_743" s="T11">а сам грязный</ta>
            <ta e="T17" id="Seg_744" s="T14">потом приехал</ta>
            <ta e="T22" id="Seg_745" s="T17">у него новая шуба и новая шапка</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T17" id="Seg_746" s="T14">[KuAI:] Variant: 'tüwa'.</ta>
            <ta e="T26" id="Seg_747" s="T22">[OSV:] In this and next sentences there is no original Russian translation.</ta>
            <ta e="T33" id="Seg_748" s="T26">[BrM:] Tentative analysis and translation.</ta>
            <ta e="T36" id="Seg_749" s="T33"> ‎‎[BrM:] Tentative analysis and translation.</ta>
            <ta e="T52" id="Seg_750" s="T46"> ‎‎[BrM:] Tentative analysis and translation.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
