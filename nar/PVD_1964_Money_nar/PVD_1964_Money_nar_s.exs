<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Money_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Money_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">16</ud-information>
            <ud-information attribute-name="# HIAT:w">13</ud-information>
            <ud-information attribute-name="# e">13</ud-information>
            <ud-information attribute-name="# HIAT:u">3</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T14" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Qomdäzʼe</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">nadə</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">tauqu</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">qajamu</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Ato</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">manan</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">qomdäu</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">koːcʼin</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">jen</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_35" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">Jass</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">tunou</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">qaim</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">taugu</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T14" id="Seg_49" n="sc" s="T1">
               <ts e="T2" id="Seg_51" n="e" s="T1">Qomdäzʼe </ts>
               <ts e="T3" id="Seg_53" n="e" s="T2">nadə </ts>
               <ts e="T4" id="Seg_55" n="e" s="T3">tauqu </ts>
               <ts e="T5" id="Seg_57" n="e" s="T4">qajamu. </ts>
               <ts e="T6" id="Seg_59" n="e" s="T5">Ato </ts>
               <ts e="T7" id="Seg_61" n="e" s="T6">manan </ts>
               <ts e="T8" id="Seg_63" n="e" s="T7">qomdäu </ts>
               <ts e="T9" id="Seg_65" n="e" s="T8">koːcʼin </ts>
               <ts e="T10" id="Seg_67" n="e" s="T9">jen. </ts>
               <ts e="T11" id="Seg_69" n="e" s="T10">Jass </ts>
               <ts e="T12" id="Seg_71" n="e" s="T11">tunou </ts>
               <ts e="T13" id="Seg_73" n="e" s="T12">qaim </ts>
               <ts e="T14" id="Seg_75" n="e" s="T13">taugu. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_76" s="T1">PVD_1964_Money_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_77" s="T5">PVD_1964_Money_nar.002 (001.002)</ta>
            <ta e="T14" id="Seg_78" s="T10">PVD_1964_Money_nar.003 (001.003)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_79" s="T1">kом′дӓзʼе надъ ′тауkу kа′jаму.</ta>
            <ta e="T10" id="Seg_80" s="T5">а то ма′нан kом′дӓу ко̄цʼин jен.</ta>
            <ta e="T14" id="Seg_81" s="T10">jасс ту′ноу kа′им ′таугу.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_82" s="T1">qomdäzʼe nadə tauqu qajamu.</ta>
            <ta e="T10" id="Seg_83" s="T5">a to manan qomdäu koːcʼin jen.</ta>
            <ta e="T14" id="Seg_84" s="T10">jass tunou qaim taugu.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_85" s="T1">Qomdäzʼe nadə tauqu qajamu. </ta>
            <ta e="T10" id="Seg_86" s="T5">Ato manan qomdäu koːcʼin jen. </ta>
            <ta e="T14" id="Seg_87" s="T10">Jass tunou qaim taugu. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_88" s="T1">qomdä-zʼe</ta>
            <ta e="T3" id="Seg_89" s="T2">nadə</ta>
            <ta e="T4" id="Seg_90" s="T3">tau-qu</ta>
            <ta e="T5" id="Seg_91" s="T4">qaj-a-mu</ta>
            <ta e="T6" id="Seg_92" s="T5">ato</ta>
            <ta e="T7" id="Seg_93" s="T6">ma-nan</ta>
            <ta e="T8" id="Seg_94" s="T7">qomdä-u</ta>
            <ta e="T9" id="Seg_95" s="T8">koːcʼi-n</ta>
            <ta e="T10" id="Seg_96" s="T9">je-n</ta>
            <ta e="T11" id="Seg_97" s="T10">jass</ta>
            <ta e="T12" id="Seg_98" s="T11">tuno-u</ta>
            <ta e="T13" id="Seg_99" s="T12">qai-m</ta>
            <ta e="T14" id="Seg_100" s="T13">tau-gu</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_101" s="T1">qomdɛ-se</ta>
            <ta e="T3" id="Seg_102" s="T2">nadə</ta>
            <ta e="T4" id="Seg_103" s="T3">taw-gu</ta>
            <ta e="T5" id="Seg_104" s="T4">qaj-ɨ-mɨ</ta>
            <ta e="T6" id="Seg_105" s="T5">ato</ta>
            <ta e="T7" id="Seg_106" s="T6">man-nan</ta>
            <ta e="T8" id="Seg_107" s="T7">qomdɛ-nɨ</ta>
            <ta e="T9" id="Seg_108" s="T8">koːci-ŋ</ta>
            <ta e="T10" id="Seg_109" s="T9">eː-n</ta>
            <ta e="T11" id="Seg_110" s="T10">asa</ta>
            <ta e="T12" id="Seg_111" s="T11">tonu-w</ta>
            <ta e="T13" id="Seg_112" s="T12">qaj-m</ta>
            <ta e="T14" id="Seg_113" s="T13">taw-gu</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_114" s="T1">money-INSTR</ta>
            <ta e="T3" id="Seg_115" s="T2">one.should</ta>
            <ta e="T4" id="Seg_116" s="T3">buy-INF</ta>
            <ta e="T5" id="Seg_117" s="T4">what-EP-something.[NOM]</ta>
            <ta e="T6" id="Seg_118" s="T5">otherwise</ta>
            <ta e="T7" id="Seg_119" s="T6">I-ADES</ta>
            <ta e="T8" id="Seg_120" s="T7">money.[NOM]-1SG</ta>
            <ta e="T9" id="Seg_121" s="T8">much-ADVZ</ta>
            <ta e="T10" id="Seg_122" s="T9">be-3SG.S</ta>
            <ta e="T11" id="Seg_123" s="T10">NEG</ta>
            <ta e="T12" id="Seg_124" s="T11">know-1SG.O</ta>
            <ta e="T13" id="Seg_125" s="T12">what-ACC</ta>
            <ta e="T14" id="Seg_126" s="T13">buy-INF</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_127" s="T1">деньги-INSTR</ta>
            <ta e="T3" id="Seg_128" s="T2">надо</ta>
            <ta e="T4" id="Seg_129" s="T3">купить-INF</ta>
            <ta e="T5" id="Seg_130" s="T4">что-EP-нечто.[NOM]</ta>
            <ta e="T6" id="Seg_131" s="T5">а.то</ta>
            <ta e="T7" id="Seg_132" s="T6">я-ADES</ta>
            <ta e="T8" id="Seg_133" s="T7">деньги.[NOM]-1SG</ta>
            <ta e="T9" id="Seg_134" s="T8">много-ADVZ</ta>
            <ta e="T10" id="Seg_135" s="T9">быть-3SG.S</ta>
            <ta e="T11" id="Seg_136" s="T10">NEG</ta>
            <ta e="T12" id="Seg_137" s="T11">знать-1SG.O</ta>
            <ta e="T13" id="Seg_138" s="T12">что-ACC</ta>
            <ta e="T14" id="Seg_139" s="T13">купить-INF</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_140" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_141" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_142" s="T3">v-v:inf</ta>
            <ta e="T5" id="Seg_143" s="T4">interrog-n:ins-n.[n:case]</ta>
            <ta e="T6" id="Seg_144" s="T5">conj</ta>
            <ta e="T7" id="Seg_145" s="T6">pers-n:case</ta>
            <ta e="T8" id="Seg_146" s="T7">n.[n:case]-n:poss</ta>
            <ta e="T9" id="Seg_147" s="T8">quant-quant&gt;adv</ta>
            <ta e="T10" id="Seg_148" s="T9">v-v:pn</ta>
            <ta e="T11" id="Seg_149" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_150" s="T11">v-v:pn</ta>
            <ta e="T13" id="Seg_151" s="T12">interrog-n:case</ta>
            <ta e="T14" id="Seg_152" s="T13">v-v:inf</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_153" s="T1">n</ta>
            <ta e="T3" id="Seg_154" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_155" s="T3">v</ta>
            <ta e="T5" id="Seg_156" s="T4">n</ta>
            <ta e="T6" id="Seg_157" s="T5">conj</ta>
            <ta e="T7" id="Seg_158" s="T6">pers</ta>
            <ta e="T8" id="Seg_159" s="T7">n</ta>
            <ta e="T9" id="Seg_160" s="T8">adv</ta>
            <ta e="T10" id="Seg_161" s="T9">v</ta>
            <ta e="T11" id="Seg_162" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_163" s="T11">v</ta>
            <ta e="T13" id="Seg_164" s="T12">interrog</ta>
            <ta e="T14" id="Seg_165" s="T13">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_166" s="T1">np:Ins</ta>
            <ta e="T4" id="Seg_167" s="T3">v:Th</ta>
            <ta e="T5" id="Seg_168" s="T4">np:Th</ta>
            <ta e="T7" id="Seg_169" s="T6">pro.h:Poss</ta>
            <ta e="T8" id="Seg_170" s="T7">np:Th</ta>
            <ta e="T12" id="Seg_171" s="T11">0.1.h:E</ta>
            <ta e="T13" id="Seg_172" s="T12">pro:Th</ta>
            <ta e="T14" id="Seg_173" s="T13">v:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_174" s="T2">ptcl:pred</ta>
            <ta e="T4" id="Seg_175" s="T3">v:O</ta>
            <ta e="T8" id="Seg_176" s="T7">np:S</ta>
            <ta e="T10" id="Seg_177" s="T9">v:pred</ta>
            <ta e="T12" id="Seg_178" s="T11">0.1.h:S v:pred</ta>
            <ta e="T14" id="Seg_179" s="T13">v:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_180" s="T2">RUS:mod</ta>
            <ta e="T6" id="Seg_181" s="T5">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_182" s="T1">[I] should buy something using my money.</ta>
            <ta e="T10" id="Seg_183" s="T5">Because I've got much money.</ta>
            <ta e="T14" id="Seg_184" s="T10">I don't know what to buy.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_185" s="T1">[Ich] sollte etwas mit meinem Geld kaufen.</ta>
            <ta e="T10" id="Seg_186" s="T5">Denn ich habe viel Geld.</ta>
            <ta e="T14" id="Seg_187" s="T10">Ich weiß nicht, was ich kaufen [soll].</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_188" s="T1">На деньги чего-нибудь надо купить.</ta>
            <ta e="T10" id="Seg_189" s="T5">А то у меня денег много.</ta>
            <ta e="T14" id="Seg_190" s="T10">Я не знаю, что купить.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_191" s="T1">на деньги чего-нибудь надо купить</ta>
            <ta e="T10" id="Seg_192" s="T5">а то у меня денег много</ta>
            <ta e="T14" id="Seg_193" s="T10">не знаю что купить</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
