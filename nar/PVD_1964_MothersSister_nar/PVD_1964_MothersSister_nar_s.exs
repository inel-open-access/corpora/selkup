<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_MothersSister_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_MothersSister_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">47</ud-information>
            <ud-information attribute-name="# HIAT:w">35</ud-information>
            <ud-information attribute-name="# e">35</ud-information>
            <ud-information attribute-name="# HIAT:u">8</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T36" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Mamounan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">jes</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">abat</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Täbnan</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">tobɨt</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">küzɨs</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">Täbnan</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">tolʼko</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">okɨr</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">čandə</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">qaːlɨmɨt</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">toboɣət</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_47" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">Tobɨt</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">nɨlʼdʼzʼin</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">laergus</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_59" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">Täp</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">äragatdänä</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">tʼärɨn</ts>
                  <nts id="Seg_68" n="HIAT:ip">:</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_70" n="HIAT:ip">“</nts>
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">Tau</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">čanɨm</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">maʒät</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">paze</ts>
                  <nts id="Seg_82" n="HIAT:ip">.</nts>
                  <nts id="Seg_83" n="HIAT:ip">”</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_86" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">Täp</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">paːm</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">sɨlɨt</ts>
                  <nts id="Seg_95" n="HIAT:ip">,</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">pam</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">lɨːbərən</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">səlɨt</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_108" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">I</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">tobɨmdə</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_116" n="HIAT:w" s="T31">maːʒɨt</ts>
                  <nts id="Seg_117" n="HIAT:ip">.</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_120" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_122" n="HIAT:w" s="T32">Täp</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_125" n="HIAT:w" s="T33">nɨtdɨn</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_128" n="HIAT:w" s="T34">ʒa</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_131" n="HIAT:w" s="T35">qua</ts>
                  <nts id="Seg_132" n="HIAT:ip">.</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T36" id="Seg_134" n="sc" s="T1">
               <ts e="T2" id="Seg_136" n="e" s="T1">Mamounan </ts>
               <ts e="T3" id="Seg_138" n="e" s="T2">jes </ts>
               <ts e="T4" id="Seg_140" n="e" s="T3">abat. </ts>
               <ts e="T5" id="Seg_142" n="e" s="T4">Täbnan </ts>
               <ts e="T6" id="Seg_144" n="e" s="T5">tobɨt </ts>
               <ts e="T7" id="Seg_146" n="e" s="T6">küzɨs. </ts>
               <ts e="T8" id="Seg_148" n="e" s="T7">Täbnan </ts>
               <ts e="T9" id="Seg_150" n="e" s="T8">tolʼko </ts>
               <ts e="T10" id="Seg_152" n="e" s="T9">okɨr </ts>
               <ts e="T11" id="Seg_154" n="e" s="T10">čandə </ts>
               <ts e="T12" id="Seg_156" n="e" s="T11">qaːlɨmɨt </ts>
               <ts e="T13" id="Seg_158" n="e" s="T12">toboɣət. </ts>
               <ts e="T14" id="Seg_160" n="e" s="T13">Tobɨt </ts>
               <ts e="T15" id="Seg_162" n="e" s="T14">nɨlʼdʼzʼin </ts>
               <ts e="T16" id="Seg_164" n="e" s="T15">laergus. </ts>
               <ts e="T17" id="Seg_166" n="e" s="T16">Täp </ts>
               <ts e="T18" id="Seg_168" n="e" s="T17">äragatdänä </ts>
               <ts e="T19" id="Seg_170" n="e" s="T18">tʼärɨn: </ts>
               <ts e="T20" id="Seg_172" n="e" s="T19">“Tau </ts>
               <ts e="T21" id="Seg_174" n="e" s="T20">čanɨm </ts>
               <ts e="T22" id="Seg_176" n="e" s="T21">maʒät </ts>
               <ts e="T23" id="Seg_178" n="e" s="T22">paze.” </ts>
               <ts e="T24" id="Seg_180" n="e" s="T23">Täp </ts>
               <ts e="T25" id="Seg_182" n="e" s="T24">paːm </ts>
               <ts e="T26" id="Seg_184" n="e" s="T25">sɨlɨt, </ts>
               <ts e="T27" id="Seg_186" n="e" s="T26">pam </ts>
               <ts e="T28" id="Seg_188" n="e" s="T27">lɨːbərən </ts>
               <ts e="T29" id="Seg_190" n="e" s="T28">səlɨt. </ts>
               <ts e="T30" id="Seg_192" n="e" s="T29">I </ts>
               <ts e="T31" id="Seg_194" n="e" s="T30">tobɨmdə </ts>
               <ts e="T32" id="Seg_196" n="e" s="T31">maːʒɨt. </ts>
               <ts e="T33" id="Seg_198" n="e" s="T32">Täp </ts>
               <ts e="T34" id="Seg_200" n="e" s="T33">nɨtdɨn </ts>
               <ts e="T35" id="Seg_202" n="e" s="T34">ʒa </ts>
               <ts e="T36" id="Seg_204" n="e" s="T35">qua. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_205" s="T1">PVD_1964_MothersSister_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_206" s="T4">PVD_1964_MothersSister_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_207" s="T7">PVD_1964_MothersSister_nar.003 (001.003)</ta>
            <ta e="T16" id="Seg_208" s="T13">PVD_1964_MothersSister_nar.004 (001.004)</ta>
            <ta e="T23" id="Seg_209" s="T16">PVD_1964_MothersSister_nar.005 (001.005)</ta>
            <ta e="T29" id="Seg_210" s="T23">PVD_1964_MothersSister_nar.006 (001.006)</ta>
            <ta e="T32" id="Seg_211" s="T29">PVD_1964_MothersSister_nar.007 (001.007)</ta>
            <ta e="T36" id="Seg_212" s="T32">PVD_1964_MothersSister_nar.008 (001.008)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_213" s="T1">′мамоунан jес а′бат.</ta>
            <ta e="T7" id="Seg_214" s="T4">тӓб′нан ′тобыт ′кӱзыс.</ta>
            <ta e="T13" id="Seg_215" s="T7">тӓб′нан толʼко о′кыр тшандъ kа̄лымыт то′боɣът.</ta>
            <ta e="T16" id="Seg_216" s="T13">′тобыт нылʼ′дʼзʼин ′лаергус.</ta>
            <ta e="T23" id="Seg_217" s="T16">тӓп ӓ′рагатдӓнӓ тʼӓрын: ′тау тша′ным ма′жӓт ′пазе.</ta>
            <ta e="T29" id="Seg_218" s="T23">тӓп па̄м сы(ъ)′лыт, пам ′лы̄бърън съ′лыт.</ta>
            <ta e="T32" id="Seg_219" s="T29">и ′тобымдъ ′ма̄жыт.</ta>
            <ta e="T36" id="Seg_220" s="T32">тӓп ныт′дын ж̂а ′kуа.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_221" s="T1">mamounan jes abat.</ta>
            <ta e="T7" id="Seg_222" s="T4">täbnan tobɨt küzɨs.</ta>
            <ta e="T13" id="Seg_223" s="T7">täbnan tolʼko okɨr tšandə qaːlɨmɨt toboɣət.</ta>
            <ta e="T16" id="Seg_224" s="T13">tobɨt nɨlʼdʼzʼin laergus.</ta>
            <ta e="T23" id="Seg_225" s="T16">täp äragatdänä tʼärɨn: tau tšanɨm maʒät paze.</ta>
            <ta e="T29" id="Seg_226" s="T23">täp paːm sɨ(ə)lɨt, pam lɨːbərən səlɨt.</ta>
            <ta e="T32" id="Seg_227" s="T29">i tobɨmdə maːʒɨt.</ta>
            <ta e="T36" id="Seg_228" s="T32">täp nɨtdɨn ʒ̂a qua.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_229" s="T1">Mamounan jes abat. </ta>
            <ta e="T7" id="Seg_230" s="T4">Täbnan tobɨt küzɨs. </ta>
            <ta e="T13" id="Seg_231" s="T7">Täbnan tolʼko okɨr čandə qaːlɨmɨt toboɣət. </ta>
            <ta e="T16" id="Seg_232" s="T13">Tobɨt nɨlʼdʼzʼin laergus. </ta>
            <ta e="T23" id="Seg_233" s="T16">Täp äragatdänä tʼärɨn: “Tau čanɨm maʒät paze.” </ta>
            <ta e="T29" id="Seg_234" s="T23">Täp paːm sɨlɨt, pam lɨːbərən səlɨt. </ta>
            <ta e="T32" id="Seg_235" s="T29">I tobɨmdə maːʒɨt. </ta>
            <ta e="T36" id="Seg_236" s="T32">Täp nɨtdɨn ʒa qua. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_237" s="T1">mamo-u-nan</ta>
            <ta e="T3" id="Seg_238" s="T2">je-s</ta>
            <ta e="T4" id="Seg_239" s="T3">apa-t</ta>
            <ta e="T5" id="Seg_240" s="T4">täb-nan</ta>
            <ta e="T6" id="Seg_241" s="T5">tob-ɨ-t</ta>
            <ta e="T7" id="Seg_242" s="T6">küzɨ-s</ta>
            <ta e="T8" id="Seg_243" s="T7">täb-nan</ta>
            <ta e="T9" id="Seg_244" s="T8">tolʼko</ta>
            <ta e="T10" id="Seg_245" s="T9">okɨr</ta>
            <ta e="T11" id="Seg_246" s="T10">čan-də</ta>
            <ta e="T12" id="Seg_247" s="T11">qaːlɨ-mɨ-t</ta>
            <ta e="T13" id="Seg_248" s="T12">tob-o-ɣət</ta>
            <ta e="T14" id="Seg_249" s="T13">tob-ɨ-t</ta>
            <ta e="T15" id="Seg_250" s="T14">nɨlʼdʼzʼi-n</ta>
            <ta e="T16" id="Seg_251" s="T15">lae-r-gu-s</ta>
            <ta e="T17" id="Seg_252" s="T16">täp</ta>
            <ta e="T18" id="Seg_253" s="T17">ära-ga-tdä-nä</ta>
            <ta e="T19" id="Seg_254" s="T18">tʼärɨ-n</ta>
            <ta e="T20" id="Seg_255" s="T19">tau</ta>
            <ta e="T21" id="Seg_256" s="T20">čan-ɨ-m</ta>
            <ta e="T22" id="Seg_257" s="T21">maʒ-ät</ta>
            <ta e="T23" id="Seg_258" s="T22">pa-ze</ta>
            <ta e="T24" id="Seg_259" s="T23">täp</ta>
            <ta e="T25" id="Seg_260" s="T24">paː-m</ta>
            <ta e="T26" id="Seg_261" s="T25">sɨlɨ-t</ta>
            <ta e="T27" id="Seg_262" s="T26">pa-m</ta>
            <ta e="T28" id="Seg_263" s="T27">lɨːbərə-n</ta>
            <ta e="T29" id="Seg_264" s="T28">səlɨ-t</ta>
            <ta e="T30" id="Seg_265" s="T29">i</ta>
            <ta e="T31" id="Seg_266" s="T30">tob-ɨ-m-də</ta>
            <ta e="T32" id="Seg_267" s="T31">maːʒɨ-t</ta>
            <ta e="T33" id="Seg_268" s="T32">täp</ta>
            <ta e="T34" id="Seg_269" s="T33">nɨtdɨ-n</ta>
            <ta e="T35" id="Seg_270" s="T34">-ʒa</ta>
            <ta e="T36" id="Seg_271" s="T35">qu-a</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_272" s="T1">mamo-w-nan</ta>
            <ta e="T3" id="Seg_273" s="T2">eː-sɨ</ta>
            <ta e="T4" id="Seg_274" s="T3">aba-tə</ta>
            <ta e="T5" id="Seg_275" s="T4">täp-nan</ta>
            <ta e="T6" id="Seg_276" s="T5">tob-ɨ-tə</ta>
            <ta e="T7" id="Seg_277" s="T6">küzɨ-sɨ</ta>
            <ta e="T8" id="Seg_278" s="T7">täp-nan</ta>
            <ta e="T9" id="Seg_279" s="T8">tolʼko</ta>
            <ta e="T10" id="Seg_280" s="T9">okkɨr</ta>
            <ta e="T11" id="Seg_281" s="T10">čan-tə</ta>
            <ta e="T12" id="Seg_282" s="T11">qalɨ-mbɨ-ntɨ</ta>
            <ta e="T13" id="Seg_283" s="T12">tob-ɨ-qɨntɨ</ta>
            <ta e="T14" id="Seg_284" s="T13">tob-ɨ-tə</ta>
            <ta e="T15" id="Seg_285" s="T14">nʼilʼdʼi-ŋ</ta>
            <ta e="T16" id="Seg_286" s="T15">laqǝ-r-gu-sɨ</ta>
            <ta e="T17" id="Seg_287" s="T16">täp</ta>
            <ta e="T18" id="Seg_288" s="T17">era-ka-tə-nä</ta>
            <ta e="T19" id="Seg_289" s="T18">tʼärɨ-n</ta>
            <ta e="T20" id="Seg_290" s="T19">taw</ta>
            <ta e="T21" id="Seg_291" s="T20">čan-ɨ-m</ta>
            <ta e="T22" id="Seg_292" s="T21">maǯə-etɨ</ta>
            <ta e="T23" id="Seg_293" s="T22">paː-se</ta>
            <ta e="T24" id="Seg_294" s="T23">täp</ta>
            <ta e="T25" id="Seg_295" s="T24">paː-m</ta>
            <ta e="T26" id="Seg_296" s="T25">sɨlɨ-t</ta>
            <ta e="T27" id="Seg_297" s="T26">paː-m</ta>
            <ta e="T28" id="Seg_298" s="T27">lɨbraj-ŋ</ta>
            <ta e="T29" id="Seg_299" s="T28">sɨlɨ-t</ta>
            <ta e="T30" id="Seg_300" s="T29">i</ta>
            <ta e="T31" id="Seg_301" s="T30">tob-ɨ-m-tə</ta>
            <ta e="T32" id="Seg_302" s="T31">maǯə-t</ta>
            <ta e="T33" id="Seg_303" s="T32">täp</ta>
            <ta e="T34" id="Seg_304" s="T33">*natʼe-n</ta>
            <ta e="T35" id="Seg_305" s="T34">-ʒe</ta>
            <ta e="T36" id="Seg_306" s="T35">quː-nɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_307" s="T1">mum-1SG-ADES</ta>
            <ta e="T3" id="Seg_308" s="T2">be-PST.[3SG.S]</ta>
            <ta e="T4" id="Seg_309" s="T3">older.sister.[NOM]-3SG</ta>
            <ta e="T5" id="Seg_310" s="T4">(s)he-ADES</ta>
            <ta e="T6" id="Seg_311" s="T5">leg.[NOM]-EP-3SG</ta>
            <ta e="T7" id="Seg_312" s="T6">hurt-PST.[3SG.S]</ta>
            <ta e="T8" id="Seg_313" s="T7">(s)he-ADES</ta>
            <ta e="T9" id="Seg_314" s="T8">only</ta>
            <ta e="T10" id="Seg_315" s="T9">one</ta>
            <ta e="T11" id="Seg_316" s="T10">sinew.[NOM]-3SG</ta>
            <ta e="T12" id="Seg_317" s="T11">stay-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T13" id="Seg_318" s="T12">leg-EP-LOC.3SG</ta>
            <ta e="T14" id="Seg_319" s="T13">leg.[NOM]-EP-3SG</ta>
            <ta e="T15" id="Seg_320" s="T14">such-ADVZ</ta>
            <ta e="T16" id="Seg_321" s="T15">move-FRQ-INF-PST.[NOM]</ta>
            <ta e="T17" id="Seg_322" s="T16">(s)he.[NOM]</ta>
            <ta e="T18" id="Seg_323" s="T17">husband-DIM-3SG-ALL</ta>
            <ta e="T19" id="Seg_324" s="T18">say-3SG.S</ta>
            <ta e="T20" id="Seg_325" s="T19">this</ta>
            <ta e="T21" id="Seg_326" s="T20">sinew-EP-ACC</ta>
            <ta e="T22" id="Seg_327" s="T21">cut-IMP.2SG.O</ta>
            <ta e="T23" id="Seg_328" s="T22">knife-INSTR</ta>
            <ta e="T24" id="Seg_329" s="T23">(s)he.[NOM]</ta>
            <ta e="T25" id="Seg_330" s="T24">knife-ACC</ta>
            <ta e="T26" id="Seg_331" s="T25">sharpen-3SG.O</ta>
            <ta e="T27" id="Seg_332" s="T26">knife-ACC</ta>
            <ta e="T28" id="Seg_333" s="T27">sharp-ADVZ</ta>
            <ta e="T29" id="Seg_334" s="T28">sharpen-3SG.O</ta>
            <ta e="T30" id="Seg_335" s="T29">and</ta>
            <ta e="T31" id="Seg_336" s="T30">leg-EP-ACC-3SG</ta>
            <ta e="T32" id="Seg_337" s="T31">cut-3SG.O</ta>
            <ta e="T33" id="Seg_338" s="T32">(s)he.[NOM]</ta>
            <ta e="T34" id="Seg_339" s="T33">there-ADV.LOC</ta>
            <ta e="T35" id="Seg_340" s="T34">-EMPH</ta>
            <ta e="T36" id="Seg_341" s="T35">die-CO.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_342" s="T1">мама-1SG-ADES</ta>
            <ta e="T3" id="Seg_343" s="T2">быть-PST.[3SG.S]</ta>
            <ta e="T4" id="Seg_344" s="T3">старшая.сестра.[NOM]-3SG</ta>
            <ta e="T5" id="Seg_345" s="T4">он(а)-ADES</ta>
            <ta e="T6" id="Seg_346" s="T5">нога.[NOM]-EP-3SG</ta>
            <ta e="T7" id="Seg_347" s="T6">болеть-PST.[3SG.S]</ta>
            <ta e="T8" id="Seg_348" s="T7">он(а)-ADES</ta>
            <ta e="T9" id="Seg_349" s="T8">только</ta>
            <ta e="T10" id="Seg_350" s="T9">один</ta>
            <ta e="T11" id="Seg_351" s="T10">жила.[NOM]-3SG</ta>
            <ta e="T12" id="Seg_352" s="T11">остаться-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T13" id="Seg_353" s="T12">нога-EP-LOC.3SG</ta>
            <ta e="T14" id="Seg_354" s="T13">нога.[NOM]-EP-3SG</ta>
            <ta e="T15" id="Seg_355" s="T14">такой-ADVZ</ta>
            <ta e="T16" id="Seg_356" s="T15">шевелиться-FRQ-INF-PST.[NOM]</ta>
            <ta e="T17" id="Seg_357" s="T16">он(а).[NOM]</ta>
            <ta e="T18" id="Seg_358" s="T17">муж-DIM-3SG-ALL</ta>
            <ta e="T19" id="Seg_359" s="T18">сказать-3SG.S</ta>
            <ta e="T20" id="Seg_360" s="T19">этот</ta>
            <ta e="T21" id="Seg_361" s="T20">жила-EP-ACC</ta>
            <ta e="T22" id="Seg_362" s="T21">отрезать-IMP.2SG.O</ta>
            <ta e="T23" id="Seg_363" s="T22">нож-INSTR</ta>
            <ta e="T24" id="Seg_364" s="T23">он(а).[NOM]</ta>
            <ta e="T25" id="Seg_365" s="T24">нож-ACC</ta>
            <ta e="T26" id="Seg_366" s="T25">наточить-3SG.O</ta>
            <ta e="T27" id="Seg_367" s="T26">нож-ACC</ta>
            <ta e="T28" id="Seg_368" s="T27">острый-ADVZ</ta>
            <ta e="T29" id="Seg_369" s="T28">наточить-3SG.O</ta>
            <ta e="T30" id="Seg_370" s="T29">и</ta>
            <ta e="T31" id="Seg_371" s="T30">нога-EP-ACC-3SG</ta>
            <ta e="T32" id="Seg_372" s="T31">отрезать-3SG.O</ta>
            <ta e="T33" id="Seg_373" s="T32">он(а).[NOM]</ta>
            <ta e="T34" id="Seg_374" s="T33">туда-ADV.LOC</ta>
            <ta e="T35" id="Seg_375" s="T34">-же</ta>
            <ta e="T36" id="Seg_376" s="T35">умереть-CO.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_377" s="T1">n-n:poss-n:case</ta>
            <ta e="T3" id="Seg_378" s="T2">v-v:tense.[v:pn]</ta>
            <ta e="T4" id="Seg_379" s="T3">n.[n:case]-n:poss</ta>
            <ta e="T5" id="Seg_380" s="T4">pers-n:case</ta>
            <ta e="T6" id="Seg_381" s="T5">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T7" id="Seg_382" s="T6">v-v:tense.[v:pn]</ta>
            <ta e="T8" id="Seg_383" s="T7">pers-n:case</ta>
            <ta e="T9" id="Seg_384" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_385" s="T9">num</ta>
            <ta e="T11" id="Seg_386" s="T10">n.[n:case]-n:poss</ta>
            <ta e="T12" id="Seg_387" s="T11">v-v:tense-v:mood.[v:pn]</ta>
            <ta e="T13" id="Seg_388" s="T12">n-n:ins-n:case.poss</ta>
            <ta e="T14" id="Seg_389" s="T13">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T15" id="Seg_390" s="T14">adj-adj&gt;adv</ta>
            <ta e="T16" id="Seg_391" s="T15">v-v&gt;v-v:inf-v:tense.[n:case]</ta>
            <ta e="T17" id="Seg_392" s="T16">pers.[n:case]</ta>
            <ta e="T18" id="Seg_393" s="T17">n-n&gt;n-n:poss-n:case</ta>
            <ta e="T19" id="Seg_394" s="T18">v-v:pn</ta>
            <ta e="T20" id="Seg_395" s="T19">dem</ta>
            <ta e="T21" id="Seg_396" s="T20">n-n:ins-n:case</ta>
            <ta e="T22" id="Seg_397" s="T21">v-v:mood.pn</ta>
            <ta e="T23" id="Seg_398" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_399" s="T23">pers.[n:case]</ta>
            <ta e="T25" id="Seg_400" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_401" s="T25">v-v:pn</ta>
            <ta e="T27" id="Seg_402" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_403" s="T27">adj-adj&gt;adv</ta>
            <ta e="T29" id="Seg_404" s="T28">v-v:pn</ta>
            <ta e="T30" id="Seg_405" s="T29">conj</ta>
            <ta e="T31" id="Seg_406" s="T30">n-n:ins-n:case-n:poss</ta>
            <ta e="T32" id="Seg_407" s="T31">v-v:pn</ta>
            <ta e="T33" id="Seg_408" s="T32">pers.[n:case]</ta>
            <ta e="T34" id="Seg_409" s="T33">adv-adv:case</ta>
            <ta e="T35" id="Seg_410" s="T34">-ptcl</ta>
            <ta e="T36" id="Seg_411" s="T35">v-v:ins.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_412" s="T1">n</ta>
            <ta e="T3" id="Seg_413" s="T2">v</ta>
            <ta e="T4" id="Seg_414" s="T3">n</ta>
            <ta e="T5" id="Seg_415" s="T4">pers</ta>
            <ta e="T6" id="Seg_416" s="T5">n</ta>
            <ta e="T7" id="Seg_417" s="T6">v</ta>
            <ta e="T8" id="Seg_418" s="T7">pers</ta>
            <ta e="T9" id="Seg_419" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_420" s="T9">num</ta>
            <ta e="T11" id="Seg_421" s="T10">n</ta>
            <ta e="T12" id="Seg_422" s="T11">v</ta>
            <ta e="T13" id="Seg_423" s="T12">n</ta>
            <ta e="T14" id="Seg_424" s="T13">n</ta>
            <ta e="T15" id="Seg_425" s="T14">adj</ta>
            <ta e="T16" id="Seg_426" s="T15">v</ta>
            <ta e="T17" id="Seg_427" s="T16">pers</ta>
            <ta e="T18" id="Seg_428" s="T17">n</ta>
            <ta e="T19" id="Seg_429" s="T18">v</ta>
            <ta e="T20" id="Seg_430" s="T19">dem</ta>
            <ta e="T21" id="Seg_431" s="T20">n</ta>
            <ta e="T22" id="Seg_432" s="T21">v</ta>
            <ta e="T23" id="Seg_433" s="T22">n</ta>
            <ta e="T24" id="Seg_434" s="T23">pers</ta>
            <ta e="T25" id="Seg_435" s="T24">n</ta>
            <ta e="T26" id="Seg_436" s="T25">v</ta>
            <ta e="T27" id="Seg_437" s="T26">n</ta>
            <ta e="T28" id="Seg_438" s="T27">adv</ta>
            <ta e="T29" id="Seg_439" s="T28">v</ta>
            <ta e="T30" id="Seg_440" s="T29">conj</ta>
            <ta e="T31" id="Seg_441" s="T30">n</ta>
            <ta e="T32" id="Seg_442" s="T31">v</ta>
            <ta e="T33" id="Seg_443" s="T32">pers</ta>
            <ta e="T34" id="Seg_444" s="T33">adv</ta>
            <ta e="T35" id="Seg_445" s="T34">clit</ta>
            <ta e="T36" id="Seg_446" s="T35">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_447" s="T1">np.h:Poss 0.1.h:Poss</ta>
            <ta e="T4" id="Seg_448" s="T3">np.h:Th</ta>
            <ta e="T5" id="Seg_449" s="T4">pro.h:Poss</ta>
            <ta e="T6" id="Seg_450" s="T5">np:P</ta>
            <ta e="T8" id="Seg_451" s="T7">pro.h:Poss</ta>
            <ta e="T11" id="Seg_452" s="T10">np:Th</ta>
            <ta e="T13" id="Seg_453" s="T12">np:L 0.3.h:Poss</ta>
            <ta e="T14" id="Seg_454" s="T13">np:Th 0.3.h:Poss</ta>
            <ta e="T17" id="Seg_455" s="T16">pro.h:A</ta>
            <ta e="T18" id="Seg_456" s="T17">np.h:R 0.3.h:Poss</ta>
            <ta e="T21" id="Seg_457" s="T20">np:P</ta>
            <ta e="T22" id="Seg_458" s="T21">0.2.h:A</ta>
            <ta e="T23" id="Seg_459" s="T22">np:Ins</ta>
            <ta e="T24" id="Seg_460" s="T23">pro.h:A</ta>
            <ta e="T25" id="Seg_461" s="T24">np:P</ta>
            <ta e="T27" id="Seg_462" s="T26">np:P</ta>
            <ta e="T29" id="Seg_463" s="T28">0.3.h:A</ta>
            <ta e="T31" id="Seg_464" s="T30">np:P 0.3.h:Poss</ta>
            <ta e="T32" id="Seg_465" s="T31">0.3.h:A</ta>
            <ta e="T33" id="Seg_466" s="T32">pro.h:P</ta>
            <ta e="T34" id="Seg_467" s="T33">adv:L</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_468" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_469" s="T3">np.h:S</ta>
            <ta e="T6" id="Seg_470" s="T5">np:S</ta>
            <ta e="T7" id="Seg_471" s="T6">v:pred</ta>
            <ta e="T11" id="Seg_472" s="T10">np:S</ta>
            <ta e="T12" id="Seg_473" s="T11">v:pred</ta>
            <ta e="T14" id="Seg_474" s="T13">np:S</ta>
            <ta e="T16" id="Seg_475" s="T15">v:pred</ta>
            <ta e="T17" id="Seg_476" s="T16">pro.h:S</ta>
            <ta e="T19" id="Seg_477" s="T18">v:pred</ta>
            <ta e="T21" id="Seg_478" s="T20">np:O</ta>
            <ta e="T22" id="Seg_479" s="T21">0.2.h:S v:pred</ta>
            <ta e="T24" id="Seg_480" s="T23">pro.h:S</ta>
            <ta e="T25" id="Seg_481" s="T24">np:O</ta>
            <ta e="T26" id="Seg_482" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_483" s="T26">np:O</ta>
            <ta e="T29" id="Seg_484" s="T28">0.3.h:S v:pred</ta>
            <ta e="T31" id="Seg_485" s="T30">np:O</ta>
            <ta e="T32" id="Seg_486" s="T31">0.3.h:S v:pred</ta>
            <ta e="T33" id="Seg_487" s="T32">pro.h:S</ta>
            <ta e="T36" id="Seg_488" s="T35">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_489" s="T1">RUS:core</ta>
            <ta e="T9" id="Seg_490" s="T8">RUS:disc</ta>
            <ta e="T30" id="Seg_491" s="T29">RUS:gram</ta>
            <ta e="T35" id="Seg_492" s="T34">=RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_493" s="T1">My mother had an elder sister.</ta>
            <ta e="T7" id="Seg_494" s="T4">Her leg hurt.</ta>
            <ta e="T13" id="Seg_495" s="T7">She had only one sinew left in her leg.</ta>
            <ta e="T16" id="Seg_496" s="T13">Her leg was simply whipping back and forth.</ta>
            <ta e="T23" id="Seg_497" s="T16">She said to her husband: “Cut this sinew with a knife.”</ta>
            <ta e="T29" id="Seg_498" s="T23">He sharpened a knife, he sharpened a sharp kinfe.</ta>
            <ta e="T32" id="Seg_499" s="T29">And he cut her leg.</ta>
            <ta e="T36" id="Seg_500" s="T32">She died right there.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_501" s="T1">Meine Mama hatte eine große Schwester.</ta>
            <ta e="T7" id="Seg_502" s="T4">Ihr Bein tat weh.</ta>
            <ta e="T13" id="Seg_503" s="T7">Ihr blieb nur noch eine Sehne in ihrem Bein.</ta>
            <ta e="T16" id="Seg_504" s="T13">Ihr Bein schwang einfach hin und her.</ta>
            <ta e="T23" id="Seg_505" s="T16">Sie sagte zu ihrem Mann: "Schneide diese Sehne mit dem Messer durch."</ta>
            <ta e="T29" id="Seg_506" s="T23">Er schärfte ein Messer, er schärfte das Messer scharf.</ta>
            <ta e="T32" id="Seg_507" s="T29">Und er schnitt in ihr Bein.</ta>
            <ta e="T36" id="Seg_508" s="T32">Sie starb dort sofort.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_509" s="T1">У моей мамы была старшая сестра.</ta>
            <ta e="T7" id="Seg_510" s="T4">У нее болела нога.</ta>
            <ta e="T13" id="Seg_511" s="T7">У нее только одна жила осталась в ноге.</ta>
            <ta e="T16" id="Seg_512" s="T13">Нога так и болталась.</ta>
            <ta e="T23" id="Seg_513" s="T16">Она мужу своему сказала: “Эту жилу разрежь ножом”.</ta>
            <ta e="T29" id="Seg_514" s="T23">Он нож наточил, нож острый наточил.</ta>
            <ta e="T32" id="Seg_515" s="T29">И ногу ее отрезал.</ta>
            <ta e="T36" id="Seg_516" s="T32">Она тут же умерла.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_517" s="T1">у мамы была сестра</ta>
            <ta e="T7" id="Seg_518" s="T4">у нее болела нога</ta>
            <ta e="T13" id="Seg_519" s="T7">у нее только одна жила осталась в ноге</ta>
            <ta e="T16" id="Seg_520" s="T13">нога так и болталась</ta>
            <ta e="T23" id="Seg_521" s="T16">она старику (мужу) сказала эту жилу разрежь ножом</ta>
            <ta e="T29" id="Seg_522" s="T23">он нож наточил ножик острый наточил</ta>
            <ta e="T32" id="Seg_523" s="T29">ногу отрезал</ta>
            <ta e="T36" id="Seg_524" s="T32">она тут же умерла</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T29" id="Seg_525" s="T23">[KuAI:] Variant: 'səlɨt'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
