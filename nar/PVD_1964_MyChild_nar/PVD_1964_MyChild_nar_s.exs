<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_MyChild_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_MyChild_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">46</ud-information>
            <ud-information attribute-name="# HIAT:w">35</ud-information>
            <ud-information attribute-name="# e">35</ud-information>
            <ud-information attribute-name="# HIAT:u">9</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T36" id="Seg_0" n="sc" s="T1">
               <ts e="T2" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Kuɣuret</ts>
                  <nts id="Seg_5" n="HIAT:ip">.</nts>
                  <nts id="Seg_6" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T4" id="Seg_8" n="HIAT:u" s="T2">
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">As</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">koɣuretǯau</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Megga</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">nadojelo</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">nʼanʼkačugu</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">Kak</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">tʼöpsɨm</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">napoddewajkudau</ts>
                  <nts id="Seg_38" n="HIAT:ip">,</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">kɨbanʼaʒa</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">ərgass</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">ilʼe</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">pɨkkɨluqun</ts>
                  <nts id="Seg_51" n="HIAT:ip">.</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_54" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">Kɨbanaʒəzʼe</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">kudnäsʼ</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">nʼanʼkačis</ts>
                  <nts id="Seg_63" n="HIAT:ip">.</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_66" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">Tep</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">otdə</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">oromsa</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_78" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">A</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">patom</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">tep</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_89" n="HIAT:w" s="T23">orɨmsa</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_92" n="HIAT:w" s="T24">warɣɨlaq</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_96" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">Patom</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_101" n="HIAT:w" s="T26">təbne</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_104" n="HIAT:w" s="T27">man</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_107" n="HIAT:w" s="T28">molokam</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">qwɛdʼäkou</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">senakən</ts>
                  <nts id="Seg_114" n="HIAT:ip">,</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_117" n="HIAT:w" s="T31">nʼaj</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_120" n="HIAT:w" s="T32">qwɛdʼikudan</ts>
                  <nts id="Seg_121" n="HIAT:ip">.</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_124" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_126" n="HIAT:w" s="T33">Tep</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_129" n="HIAT:w" s="T34">otdə</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_132" n="HIAT:w" s="T35">aurgun</ts>
                  <nts id="Seg_133" n="HIAT:ip">.</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T36" id="Seg_135" n="sc" s="T1">
               <ts e="T2" id="Seg_137" n="e" s="T1">Kuɣuret. </ts>
               <ts e="T3" id="Seg_139" n="e" s="T2">As </ts>
               <ts e="T4" id="Seg_141" n="e" s="T3">koɣuretǯau. </ts>
               <ts e="T5" id="Seg_143" n="e" s="T4">Megga </ts>
               <ts e="T6" id="Seg_145" n="e" s="T5">nadojelo </ts>
               <ts e="T7" id="Seg_147" n="e" s="T6">nʼanʼkačugu. </ts>
               <ts e="T8" id="Seg_149" n="e" s="T7">Kak </ts>
               <ts e="T9" id="Seg_151" n="e" s="T8">tʼöpsɨm </ts>
               <ts e="T10" id="Seg_153" n="e" s="T9">napoddewajkudau, </ts>
               <ts e="T11" id="Seg_155" n="e" s="T10">kɨbanʼaʒa </ts>
               <ts e="T12" id="Seg_157" n="e" s="T11">ərgass </ts>
               <ts e="T13" id="Seg_159" n="e" s="T12">ilʼe </ts>
               <ts e="T14" id="Seg_161" n="e" s="T13">pɨkkɨluqun. </ts>
               <ts e="T15" id="Seg_163" n="e" s="T14">Kɨbanaʒəzʼe </ts>
               <ts e="T16" id="Seg_165" n="e" s="T15">kudnäsʼ </ts>
               <ts e="T17" id="Seg_167" n="e" s="T16">nʼanʼkačis. </ts>
               <ts e="T18" id="Seg_169" n="e" s="T17">Tep </ts>
               <ts e="T19" id="Seg_171" n="e" s="T18">otdə </ts>
               <ts e="T20" id="Seg_173" n="e" s="T19">oromsa. </ts>
               <ts e="T21" id="Seg_175" n="e" s="T20">A </ts>
               <ts e="T22" id="Seg_177" n="e" s="T21">patom </ts>
               <ts e="T23" id="Seg_179" n="e" s="T22">tep </ts>
               <ts e="T24" id="Seg_181" n="e" s="T23">orɨmsa </ts>
               <ts e="T25" id="Seg_183" n="e" s="T24">warɣɨlaq. </ts>
               <ts e="T26" id="Seg_185" n="e" s="T25">Patom </ts>
               <ts e="T27" id="Seg_187" n="e" s="T26">təbne </ts>
               <ts e="T28" id="Seg_189" n="e" s="T27">man </ts>
               <ts e="T29" id="Seg_191" n="e" s="T28">molokam </ts>
               <ts e="T30" id="Seg_193" n="e" s="T29">qwɛdʼäkou </ts>
               <ts e="T31" id="Seg_195" n="e" s="T30">senakən, </ts>
               <ts e="T32" id="Seg_197" n="e" s="T31">nʼaj </ts>
               <ts e="T33" id="Seg_199" n="e" s="T32">qwɛdʼikudan. </ts>
               <ts e="T34" id="Seg_201" n="e" s="T33">Tep </ts>
               <ts e="T35" id="Seg_203" n="e" s="T34">otdə </ts>
               <ts e="T36" id="Seg_205" n="e" s="T35">aurgun. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2" id="Seg_206" s="T1">PVD_1964_MyChild_nar.001 (001.001)</ta>
            <ta e="T4" id="Seg_207" s="T2">PVD_1964_MyChild_nar.002 (001.002)</ta>
            <ta e="T7" id="Seg_208" s="T4">PVD_1964_MyChild_nar.003 (001.003)</ta>
            <ta e="T14" id="Seg_209" s="T7">PVD_1964_MyChild_nar.004 (001.004)</ta>
            <ta e="T17" id="Seg_210" s="T14">PVD_1964_MyChild_nar.005 (001.005)</ta>
            <ta e="T20" id="Seg_211" s="T17">PVD_1964_MyChild_nar.006 (001.006)</ta>
            <ta e="T25" id="Seg_212" s="T20">PVD_1964_MyChild_nar.007 (001.007)</ta>
            <ta e="T33" id="Seg_213" s="T25">PVD_1964_MyChild_nar.008 (001.008)</ta>
            <ta e="T36" id="Seg_214" s="T33">PVD_1964_MyChild_nar.009 (001.009)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T2" id="Seg_215" s="T1">куɣу′рет.</ta>
            <ta e="T4" id="Seg_216" s="T2">ас ко′ɣу′ре̨тджау̹.</ta>
            <ta e="T7" id="Seg_217" s="T4">′мег̂га надо′jело ′нʼанʼкатшу′гу.</ta>
            <ta e="T14" id="Seg_218" s="T7">как ′тʼӧпсым напод′д′е′wайку′дау̹, кы′банʼажа ър′гас(з̂)с и′лʼе ′пыккылуkун.</ta>
            <ta e="T17" id="Seg_219" s="T14">кы′банажъзʼе куд′нӓсʼ ′нʼанʼкатшис.</ta>
            <ta e="T20" id="Seg_220" s="T17">те̨п ′отдоромса (отдъ оромса).</ta>
            <ta e="T25" id="Seg_221" s="T20">а патом теп ′орымса ′wарɣылаk.</ta>
            <ta e="T33" id="Seg_222" s="T25">па′том тъб′не̨ ман мо′lокам ′kwɛдʼӓкоу̹ се′накън, нʼай ′kwɛдʼикуд̂ан.</ta>
            <ta e="T36" id="Seg_223" s="T33">те̨п ′отдъ аур′гун.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T2" id="Seg_224" s="T1">kuɣuret.</ta>
            <ta e="T4" id="Seg_225" s="T2">as koɣuretǯau̹.</ta>
            <ta e="T7" id="Seg_226" s="T4">meĝga nadojelo nʼanʼkatšugu.</ta>
            <ta e="T14" id="Seg_227" s="T7">kak tʼöpsɨm napoddewajkudau̹, kɨbanʼaʒa ərgas(ẑ)s ilʼe pɨkkɨluqun.</ta>
            <ta e="T17" id="Seg_228" s="T14">kɨbanaʒəzʼe kudnäsʼ nʼanʼkatšis.</ta>
            <ta e="T20" id="Seg_229" s="T17">tep otdoromsa (otdə oromsa).</ta>
            <ta e="T25" id="Seg_230" s="T20">a patom tep orɨmsa warɣɨlaq.</ta>
            <ta e="T33" id="Seg_231" s="T25">patom təbne man molokam qwɛdʼäkou̹ senakən, nʼaj qwɛdʼikud̂an.</ta>
            <ta e="T36" id="Seg_232" s="T33">tep otdə aurgun.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2" id="Seg_233" s="T1">Kuɣuret. </ta>
            <ta e="T4" id="Seg_234" s="T2">As koɣuretǯau. </ta>
            <ta e="T7" id="Seg_235" s="T4">Megga nadojelo nʼanʼkačugu. </ta>
            <ta e="T14" id="Seg_236" s="T7">Kak tʼöpsɨm napoddewajkudau, kɨbanʼaʒa ərgass ilʼe pɨkkɨluqun. </ta>
            <ta e="T17" id="Seg_237" s="T14">Kɨbanaʒəzʼe kudnäsʼ nʼanʼkačis. </ta>
            <ta e="T20" id="Seg_238" s="T17">Tep otdə oromsa. </ta>
            <ta e="T25" id="Seg_239" s="T20">A patom tep orɨmsa warɣɨlaq. </ta>
            <ta e="T33" id="Seg_240" s="T25">Patom təbne man molokam qwɛdʼäkou senakən, nʼaj qwɛdʼikudan. </ta>
            <ta e="T36" id="Seg_241" s="T33">Tep otdə aurgun. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_242" s="T1">kuɣur-et</ta>
            <ta e="T3" id="Seg_243" s="T2">as</ta>
            <ta e="T4" id="Seg_244" s="T3">koɣur-etǯa-u</ta>
            <ta e="T5" id="Seg_245" s="T4">megga</ta>
            <ta e="T7" id="Seg_246" s="T6">nʼanʼka-ču-gu</ta>
            <ta e="T8" id="Seg_247" s="T7">kak</ta>
            <ta e="T9" id="Seg_248" s="T8">tʼöps-ɨ-m</ta>
            <ta e="T10" id="Seg_249" s="T9">napoddewaj-ku-da-u</ta>
            <ta e="T11" id="Seg_250" s="T10">kɨbanʼaʒa</ta>
            <ta e="T12" id="Seg_251" s="T11">ərgass</ta>
            <ta e="T13" id="Seg_252" s="T12">ilʼe</ta>
            <ta e="T14" id="Seg_253" s="T13">pɨkkɨl-u-qu-n</ta>
            <ta e="T15" id="Seg_254" s="T14">kɨbanaʒə-zʼe</ta>
            <ta e="T16" id="Seg_255" s="T15">kud-nä-sʼ</ta>
            <ta e="T17" id="Seg_256" s="T16">nʼanʼka-či-s</ta>
            <ta e="T18" id="Seg_257" s="T17">tep</ta>
            <ta e="T19" id="Seg_258" s="T18">otdə</ta>
            <ta e="T20" id="Seg_259" s="T19">orom-sa</ta>
            <ta e="T21" id="Seg_260" s="T20">a</ta>
            <ta e="T22" id="Seg_261" s="T21">patom</ta>
            <ta e="T23" id="Seg_262" s="T22">tep</ta>
            <ta e="T24" id="Seg_263" s="T23">orɨm-sa</ta>
            <ta e="T25" id="Seg_264" s="T24">warɣɨ-laq</ta>
            <ta e="T26" id="Seg_265" s="T25">patom</ta>
            <ta e="T27" id="Seg_266" s="T26">təb-ne</ta>
            <ta e="T28" id="Seg_267" s="T27">man</ta>
            <ta e="T29" id="Seg_268" s="T28">moloka-m</ta>
            <ta e="T30" id="Seg_269" s="T29">qwɛdʼä-ko-u</ta>
            <ta e="T31" id="Seg_270" s="T30">sena-kən</ta>
            <ta e="T32" id="Seg_271" s="T31">nʼaːj</ta>
            <ta e="T33" id="Seg_272" s="T32">qwɛdʼi-ku-da-n</ta>
            <ta e="T34" id="Seg_273" s="T33">tep</ta>
            <ta e="T35" id="Seg_274" s="T34">otdə</ta>
            <ta e="T36" id="Seg_275" s="T35">au-r-gu-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_276" s="T1">qugur-etɨ</ta>
            <ta e="T3" id="Seg_277" s="T2">asa</ta>
            <ta e="T4" id="Seg_278" s="T3">qugur-enǯɨ-w</ta>
            <ta e="T5" id="Seg_279" s="T4">mekka</ta>
            <ta e="T7" id="Seg_280" s="T6">nʼanʼka-ču-gu</ta>
            <ta e="T8" id="Seg_281" s="T7">kak</ta>
            <ta e="T9" id="Seg_282" s="T8">tʼöps-ɨ-m</ta>
            <ta e="T10" id="Seg_283" s="T9">napoddewaj-ku-ntɨ-w</ta>
            <ta e="T11" id="Seg_284" s="T10">qɨbanʼaǯa</ta>
            <ta e="T12" id="Seg_285" s="T11">ərgas</ta>
            <ta e="T13" id="Seg_286" s="T12">ilʼlʼe</ta>
            <ta e="T14" id="Seg_287" s="T13">pɨŋgəl-ɨ-ku-n</ta>
            <ta e="T15" id="Seg_288" s="T14">qɨbanʼaǯa-se</ta>
            <ta e="T16" id="Seg_289" s="T15">kud-näj-s</ta>
            <ta e="T17" id="Seg_290" s="T16">nʼanʼka-ču-sɨ</ta>
            <ta e="T18" id="Seg_291" s="T17">täp</ta>
            <ta e="T19" id="Seg_292" s="T18">ondə</ta>
            <ta e="T20" id="Seg_293" s="T19">orɨm-sɨ</ta>
            <ta e="T21" id="Seg_294" s="T20">a</ta>
            <ta e="T22" id="Seg_295" s="T21">patom</ta>
            <ta e="T23" id="Seg_296" s="T22">täp</ta>
            <ta e="T24" id="Seg_297" s="T23">orɨm-sɨ</ta>
            <ta e="T25" id="Seg_298" s="T24">wargɨ-laq</ta>
            <ta e="T26" id="Seg_299" s="T25">patom</ta>
            <ta e="T27" id="Seg_300" s="T26">täp-nä</ta>
            <ta e="T28" id="Seg_301" s="T27">man</ta>
            <ta e="T29" id="Seg_302" s="T28">moloka-m</ta>
            <ta e="T30" id="Seg_303" s="T29">qwɛdʼi-ku-w</ta>
            <ta e="T31" id="Seg_304" s="T30">sena-qɨn</ta>
            <ta e="T32" id="Seg_305" s="T31">nʼäj</ta>
            <ta e="T33" id="Seg_306" s="T32">qwɛdʼi-ku-ntɨ-ŋ</ta>
            <ta e="T34" id="Seg_307" s="T33">täp</ta>
            <ta e="T35" id="Seg_308" s="T34">ondə</ta>
            <ta e="T36" id="Seg_309" s="T35">am-r-ku-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_310" s="T1">swing-IMP.2SG.O</ta>
            <ta e="T3" id="Seg_311" s="T2">NEG</ta>
            <ta e="T4" id="Seg_312" s="T3">swing-FUT-1SG.O</ta>
            <ta e="T5" id="Seg_313" s="T4">I.ALL</ta>
            <ta e="T7" id="Seg_314" s="T6">nanny-VBLZ-INF</ta>
            <ta e="T8" id="Seg_315" s="T7">suddenly</ta>
            <ta e="T9" id="Seg_316" s="T8">cradle-EP-ACC</ta>
            <ta e="T10" id="Seg_317" s="T9">give.a.punch-HAB-IPFV-1SG.O</ta>
            <ta e="T11" id="Seg_318" s="T10">child.[NOM]</ta>
            <ta e="T12" id="Seg_319" s="T11">almost</ta>
            <ta e="T13" id="Seg_320" s="T12">down</ta>
            <ta e="T14" id="Seg_321" s="T13">fall.down-EP-HAB-3SG.S</ta>
            <ta e="T15" id="Seg_322" s="T14">child-COM</ta>
            <ta e="T16" id="Seg_323" s="T15">who.[NOM]-EMPH-NEG</ta>
            <ta e="T17" id="Seg_324" s="T16">nanny-VBLZ-PST.[3SG.S]</ta>
            <ta e="T18" id="Seg_325" s="T17">(s)he.[NOM]</ta>
            <ta e="T19" id="Seg_326" s="T18">oneself.3SG</ta>
            <ta e="T20" id="Seg_327" s="T19">grow.up-PST.[3SG.S]</ta>
            <ta e="T21" id="Seg_328" s="T20">and</ta>
            <ta e="T22" id="Seg_329" s="T21">then</ta>
            <ta e="T23" id="Seg_330" s="T22">(s)he.[NOM]</ta>
            <ta e="T24" id="Seg_331" s="T23">grow.up-PST.[3SG.S]</ta>
            <ta e="T25" id="Seg_332" s="T24">big-ATTEN</ta>
            <ta e="T26" id="Seg_333" s="T25">then</ta>
            <ta e="T27" id="Seg_334" s="T26">(s)he-ALL</ta>
            <ta e="T28" id="Seg_335" s="T27">I.NOM</ta>
            <ta e="T29" id="Seg_336" s="T28">milk-ACC</ta>
            <ta e="T30" id="Seg_337" s="T29">leave-HAB-1SG.O</ta>
            <ta e="T31" id="Seg_338" s="T30">outer.entrance.room-LOC</ta>
            <ta e="T32" id="Seg_339" s="T31">bread.[NOM]</ta>
            <ta e="T33" id="Seg_340" s="T32">leave-HAB-IPFV-1SG.S</ta>
            <ta e="T34" id="Seg_341" s="T33">(s)he.[NOM]</ta>
            <ta e="T35" id="Seg_342" s="T34">oneself.3SG</ta>
            <ta e="T36" id="Seg_343" s="T35">eat-FRQ-HAB-3SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_344" s="T1">качать-IMP.2SG.O</ta>
            <ta e="T3" id="Seg_345" s="T2">NEG</ta>
            <ta e="T4" id="Seg_346" s="T3">качать-FUT-1SG.O</ta>
            <ta e="T5" id="Seg_347" s="T4">я.ALL</ta>
            <ta e="T7" id="Seg_348" s="T6">нянька-VBLZ-INF</ta>
            <ta e="T8" id="Seg_349" s="T7">как</ta>
            <ta e="T9" id="Seg_350" s="T8">люлька-EP-ACC</ta>
            <ta e="T10" id="Seg_351" s="T9">наподдать-HAB-IPFV-1SG.O</ta>
            <ta e="T11" id="Seg_352" s="T10">ребенок.[NOM]</ta>
            <ta e="T12" id="Seg_353" s="T11">чуть.не</ta>
            <ta e="T13" id="Seg_354" s="T12">вниз</ta>
            <ta e="T14" id="Seg_355" s="T13">упасть-EP-HAB-3SG.S</ta>
            <ta e="T15" id="Seg_356" s="T14">ребенок-COM</ta>
            <ta e="T16" id="Seg_357" s="T15">кто.[NOM]-EMPH-NEG</ta>
            <ta e="T17" id="Seg_358" s="T16">нянька-VBLZ-PST.[3SG.S]</ta>
            <ta e="T18" id="Seg_359" s="T17">он(а).[NOM]</ta>
            <ta e="T19" id="Seg_360" s="T18">сам.3SG</ta>
            <ta e="T20" id="Seg_361" s="T19">вырасти-PST.[3SG.S]</ta>
            <ta e="T21" id="Seg_362" s="T20">а</ta>
            <ta e="T22" id="Seg_363" s="T21">потом</ta>
            <ta e="T23" id="Seg_364" s="T22">он(а).[NOM]</ta>
            <ta e="T24" id="Seg_365" s="T23">вырасти-PST.[3SG.S]</ta>
            <ta e="T25" id="Seg_366" s="T24">большой-ATTEN</ta>
            <ta e="T26" id="Seg_367" s="T25">потом</ta>
            <ta e="T27" id="Seg_368" s="T26">он(а)-ALL</ta>
            <ta e="T28" id="Seg_369" s="T27">я.NOM</ta>
            <ta e="T29" id="Seg_370" s="T28">молоко-ACC</ta>
            <ta e="T30" id="Seg_371" s="T29">оставить-HAB-1SG.O</ta>
            <ta e="T31" id="Seg_372" s="T30">сени-LOC</ta>
            <ta e="T32" id="Seg_373" s="T31">хлеб.[NOM]</ta>
            <ta e="T33" id="Seg_374" s="T32">оставить-HAB-IPFV-1SG.S</ta>
            <ta e="T34" id="Seg_375" s="T33">он(а).[NOM]</ta>
            <ta e="T35" id="Seg_376" s="T34">сам.3SG</ta>
            <ta e="T36" id="Seg_377" s="T35">съесть-FRQ-HAB-3SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_378" s="T1">v-v:mood.pn</ta>
            <ta e="T3" id="Seg_379" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_380" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_381" s="T4">pers</ta>
            <ta e="T7" id="Seg_382" s="T6">n-n&gt;v-v:inf</ta>
            <ta e="T8" id="Seg_383" s="T7">adv</ta>
            <ta e="T9" id="Seg_384" s="T8">n-n:ins-n:case</ta>
            <ta e="T10" id="Seg_385" s="T9">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T11" id="Seg_386" s="T10">n.[n:case]</ta>
            <ta e="T12" id="Seg_387" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_388" s="T12">preverb</ta>
            <ta e="T14" id="Seg_389" s="T13">v-v:ins-v&gt;v-v:pn</ta>
            <ta e="T15" id="Seg_390" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_391" s="T15">interrog.[n:case]-clit-clit</ta>
            <ta e="T17" id="Seg_392" s="T16">n-n&gt;v-v:tense.[v:pn]</ta>
            <ta e="T18" id="Seg_393" s="T17">pers.[n:case]</ta>
            <ta e="T19" id="Seg_394" s="T18">emphpro</ta>
            <ta e="T20" id="Seg_395" s="T19">v-v:tense.[v:pn]</ta>
            <ta e="T21" id="Seg_396" s="T20">conj</ta>
            <ta e="T22" id="Seg_397" s="T21">adv</ta>
            <ta e="T23" id="Seg_398" s="T22">pers.[n:case]</ta>
            <ta e="T24" id="Seg_399" s="T23">v-v:tense.[v:pn]</ta>
            <ta e="T25" id="Seg_400" s="T24">adj-adj&gt;adj</ta>
            <ta e="T26" id="Seg_401" s="T25">adv</ta>
            <ta e="T27" id="Seg_402" s="T26">pers-n:case</ta>
            <ta e="T28" id="Seg_403" s="T27">pers</ta>
            <ta e="T29" id="Seg_404" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_405" s="T29">v-v&gt;v-v:pn</ta>
            <ta e="T31" id="Seg_406" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_407" s="T31">n.[n:case]</ta>
            <ta e="T33" id="Seg_408" s="T32">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T34" id="Seg_409" s="T33">pers.[n:case]</ta>
            <ta e="T35" id="Seg_410" s="T34">emphpro</ta>
            <ta e="T36" id="Seg_411" s="T35">v-v&gt;v-v&gt;v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_412" s="T1">v</ta>
            <ta e="T3" id="Seg_413" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_414" s="T3">v</ta>
            <ta e="T5" id="Seg_415" s="T4">pers</ta>
            <ta e="T7" id="Seg_416" s="T6">v</ta>
            <ta e="T8" id="Seg_417" s="T7">adv</ta>
            <ta e="T9" id="Seg_418" s="T8">n</ta>
            <ta e="T10" id="Seg_419" s="T9">v</ta>
            <ta e="T11" id="Seg_420" s="T10">n</ta>
            <ta e="T12" id="Seg_421" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_422" s="T12">preverb</ta>
            <ta e="T14" id="Seg_423" s="T13">v</ta>
            <ta e="T15" id="Seg_424" s="T14">n</ta>
            <ta e="T16" id="Seg_425" s="T15">pro</ta>
            <ta e="T17" id="Seg_426" s="T16">v</ta>
            <ta e="T18" id="Seg_427" s="T17">pers</ta>
            <ta e="T19" id="Seg_428" s="T18">emphpro</ta>
            <ta e="T20" id="Seg_429" s="T19">v</ta>
            <ta e="T21" id="Seg_430" s="T20">conj</ta>
            <ta e="T22" id="Seg_431" s="T21">adv</ta>
            <ta e="T23" id="Seg_432" s="T22">pers</ta>
            <ta e="T24" id="Seg_433" s="T23">v</ta>
            <ta e="T25" id="Seg_434" s="T24">adj</ta>
            <ta e="T26" id="Seg_435" s="T25">adv</ta>
            <ta e="T27" id="Seg_436" s="T26">pers</ta>
            <ta e="T28" id="Seg_437" s="T27">pers</ta>
            <ta e="T29" id="Seg_438" s="T28">n</ta>
            <ta e="T30" id="Seg_439" s="T29">v</ta>
            <ta e="T31" id="Seg_440" s="T30">n</ta>
            <ta e="T32" id="Seg_441" s="T31">n</ta>
            <ta e="T33" id="Seg_442" s="T32">v</ta>
            <ta e="T34" id="Seg_443" s="T33">pers</ta>
            <ta e="T35" id="Seg_444" s="T34">emphpro</ta>
            <ta e="T36" id="Seg_445" s="T35">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_446" s="T1">0.2.h:A 0.3.h:Th</ta>
            <ta e="T4" id="Seg_447" s="T3">0.1.h:A 0.3.h:Th</ta>
            <ta e="T9" id="Seg_448" s="T8">np:Th</ta>
            <ta e="T10" id="Seg_449" s="T9">0.1.h:A</ta>
            <ta e="T11" id="Seg_450" s="T10">np.h:P</ta>
            <ta e="T15" id="Seg_451" s="T14">np.h:Th</ta>
            <ta e="T16" id="Seg_452" s="T15">pro.h:A</ta>
            <ta e="T18" id="Seg_453" s="T17">pro.h:P</ta>
            <ta e="T22" id="Seg_454" s="T21">adv:Time</ta>
            <ta e="T23" id="Seg_455" s="T22">pro.h:P</ta>
            <ta e="T26" id="Seg_456" s="T25">adv:Time</ta>
            <ta e="T27" id="Seg_457" s="T26">pro.h:B</ta>
            <ta e="T28" id="Seg_458" s="T27">pro.h:A</ta>
            <ta e="T29" id="Seg_459" s="T28">np:Th</ta>
            <ta e="T31" id="Seg_460" s="T30">np:L</ta>
            <ta e="T32" id="Seg_461" s="T31">np:Th</ta>
            <ta e="T33" id="Seg_462" s="T32">0.1.h:A</ta>
            <ta e="T34" id="Seg_463" s="T33">pro.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_464" s="T1">0.2.h:S v:pred 0.3.h:O</ta>
            <ta e="T4" id="Seg_465" s="T3">0.1.h:S v:pred 0.3.h:O</ta>
            <ta e="T10" id="Seg_466" s="T7">s:temp</ta>
            <ta e="T11" id="Seg_467" s="T10">np.h:S</ta>
            <ta e="T14" id="Seg_468" s="T13">v:pred</ta>
            <ta e="T16" id="Seg_469" s="T15">pro.h:S</ta>
            <ta e="T17" id="Seg_470" s="T16">v:pred</ta>
            <ta e="T18" id="Seg_471" s="T17">pro.h:S</ta>
            <ta e="T20" id="Seg_472" s="T19">v:pred</ta>
            <ta e="T23" id="Seg_473" s="T22">pro.h:S</ta>
            <ta e="T24" id="Seg_474" s="T23">v:pred</ta>
            <ta e="T28" id="Seg_475" s="T27">pro.h:S</ta>
            <ta e="T29" id="Seg_476" s="T28">np:O</ta>
            <ta e="T30" id="Seg_477" s="T29">v:pred</ta>
            <ta e="T32" id="Seg_478" s="T31">np:O</ta>
            <ta e="T33" id="Seg_479" s="T32">0.1.h:S v:pred</ta>
            <ta e="T34" id="Seg_480" s="T33">pro.h:S</ta>
            <ta e="T36" id="Seg_481" s="T35">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T7" id="Seg_482" s="T6">RUS:cult</ta>
            <ta e="T8" id="Seg_483" s="T7">RUS:disc</ta>
            <ta e="T10" id="Seg_484" s="T9">RUS:core</ta>
            <ta e="T17" id="Seg_485" s="T16">RUS:cult</ta>
            <ta e="T21" id="Seg_486" s="T20">RUS:gram</ta>
            <ta e="T22" id="Seg_487" s="T21">RUS:disc</ta>
            <ta e="T26" id="Seg_488" s="T25">RUS:disc</ta>
            <ta e="T29" id="Seg_489" s="T28">RUS:cult</ta>
            <ta e="T31" id="Seg_490" s="T30">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T2" id="Seg_491" s="T1">Swing [the child].</ta>
            <ta e="T4" id="Seg_492" s="T2">I won't swing.</ta>
            <ta e="T7" id="Seg_493" s="T4">I am tired of nursing.</ta>
            <ta e="T14" id="Seg_494" s="T7">When I swing the craddle, the child is almost falling out.</ta>
            <ta e="T17" id="Seg_495" s="T14">Noone was nursing the child.</ta>
            <ta e="T20" id="Seg_496" s="T17">He grew up himself.</ta>
            <ta e="T25" id="Seg_497" s="T20">And then he grew a bit more.</ta>
            <ta e="T33" id="Seg_498" s="T25">Then I leave him milk in the outer entrance room, I leave him bread.</ta>
            <ta e="T36" id="Seg_499" s="T33">He eats himself.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2" id="Seg_500" s="T1">Wiege [das Kind].</ta>
            <ta e="T4" id="Seg_501" s="T2">Ich werde [es] nicht wiegen.</ta>
            <ta e="T7" id="Seg_502" s="T4">Ich habe keine Lust [es] zu pflegen.</ta>
            <ta e="T14" id="Seg_503" s="T7">Wenn ich die Wiege bewege, fällt das Kind beinahe heraus.</ta>
            <ta e="T17" id="Seg_504" s="T14">Niemand kümmerte sich um das Kind.</ta>
            <ta e="T20" id="Seg_505" s="T17">Es wuchs allein auf.</ta>
            <ta e="T25" id="Seg_506" s="T20">Und dann wuchs es noch ein wenig mehr.</ta>
            <ta e="T33" id="Seg_507" s="T25">Dann lasse ich ihm Milch im Flur, ich lasse ihm Brot da.</ta>
            <ta e="T36" id="Seg_508" s="T33">Er isst selbst.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T2" id="Seg_509" s="T1">Качай (ребенка).</ta>
            <ta e="T4" id="Seg_510" s="T2">Не буду качать.</ta>
            <ta e="T7" id="Seg_511" s="T4">Мне надоело нянчить.</ta>
            <ta e="T14" id="Seg_512" s="T7">Как люльку поддену, ребенок чуть на пол не падает.</ta>
            <ta e="T17" id="Seg_513" s="T14">С ребенком никто не нянчился.</ta>
            <ta e="T20" id="Seg_514" s="T17">Он сам вырос.</ta>
            <ta e="T25" id="Seg_515" s="T20">А потом вырос немного побольше.</ta>
            <ta e="T33" id="Seg_516" s="T25">Потом я ему молоко оставляю в сенках, хлеб оставляю.</ta>
            <ta e="T36" id="Seg_517" s="T33">Он сам ест.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T2" id="Seg_518" s="T1">качай (ребенка)</ta>
            <ta e="T4" id="Seg_519" s="T2">не буду качать</ta>
            <ta e="T7" id="Seg_520" s="T4">мне надоело нянчить (ребенка)</ta>
            <ta e="T14" id="Seg_521" s="T7">как люльку поддену ребенок чуть на пол не падает</ta>
            <ta e="T17" id="Seg_522" s="T14">с ребенком никто не водился</ta>
            <ta e="T20" id="Seg_523" s="T17">он сам вырос</ta>
            <ta e="T25" id="Seg_524" s="T20">а потом вырос немного побольше</ta>
            <ta e="T33" id="Seg_525" s="T25">я ему молоко оставляю хлеб оставляю</ta>
            <ta e="T36" id="Seg_526" s="T33">он сам ест</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T14" id="Seg_527" s="T7">[KuAI:] Variant: 'ərgazs'. [BrM:] Tentative analysis of 'napoddewajkudau'.</ta>
            <ta e="T20" id="Seg_528" s="T17">[KuAI:] Variant: 'otdoromsa'.</ta>
            <ta e="T33" id="Seg_529" s="T25">[BrM:] Tentative analysis of 'qwɛdʼikudan'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
