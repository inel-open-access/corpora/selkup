<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_MyDream_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_MyDream_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">81</ud-information>
            <ud-information attribute-name="# HIAT:w">55</ud-information>
            <ud-information attribute-name="# e">55</ud-information>
            <ud-information attribute-name="# HIAT:u">14</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T56" id="Seg_0" n="sc" s="T1">
               <ts e="T10" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">taben</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_9" n="HIAT:ip">(</nts>
                  <ts e="T4" id="Seg_11" n="HIAT:w" s="T3">tapben</ts>
                  <nts id="Seg_12" n="HIAT:ip">)</nts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_15" n="HIAT:w" s="T4">qoǯirsau</ts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_18" n="HIAT:w" s="T5">kudäptɨpɨlʼe</ts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_21" n="HIAT:w" s="T6">dʼaja</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_24" n="HIAT:w" s="T7">Lʼekseim</ts>
                  <nts id="Seg_25" n="HIAT:ip">,</nts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">Lʼönʼam</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">qoǯirsau</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_35" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">Man</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">teblanä</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">sernan</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_47" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">Tebla</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">aurnattə</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_56" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">Mazɨm</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">omdɨlǯattə</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">aurgu</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_68" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">Dʼaja</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">Lʼeksʼej</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">mekga</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">tʼärɨn</ts>
                  <nts id="Seg_80" n="HIAT:ip">:</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_82" n="HIAT:ip">“</nts>
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">Tan</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_87" n="HIAT:w" s="T23">pragam</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_90" n="HIAT:w" s="T24">ütqwal</ts>
                  <nts id="Seg_91" n="HIAT:ip">?</nts>
                  <nts id="Seg_92" n="HIAT:ip">”</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_95" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_97" n="HIAT:w" s="T25">A</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_100" n="HIAT:w" s="T26">man</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">tʼaran</ts>
                  <nts id="Seg_104" n="HIAT:ip">:</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_106" n="HIAT:ip">“</nts>
                  <ts e="T29" id="Seg_108" n="HIAT:w" s="T28">As</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_111" n="HIAT:w" s="T29">ütqou</ts>
                  <nts id="Seg_112" n="HIAT:ip">.</nts>
                  <nts id="Seg_113" n="HIAT:ip">”</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_116" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_118" n="HIAT:w" s="T30">Täbɨstaɣə</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_121" n="HIAT:w" s="T31">otdə</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_124" n="HIAT:w" s="T32">äraɣə</ts>
                  <nts id="Seg_125" n="HIAT:ip">.</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_128" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_130" n="HIAT:w" s="T33">Qajno</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_133" n="HIAT:w" s="T34">soːgutʼügu</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_137" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_139" n="HIAT:w" s="T35">Man</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_142" n="HIAT:w" s="T36">wetʼ</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_145" n="HIAT:w" s="T37">kosti</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_148" n="HIAT:w" s="T38">tüpban</ts>
                  <nts id="Seg_149" n="HIAT:ip">.</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_152" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_154" n="HIAT:w" s="T39">Puskaj</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_157" n="HIAT:w" s="T40">qamǯenädəba</ts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_161" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_163" n="HIAT:w" s="T41">Ato</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_166" n="HIAT:w" s="T42">soːɣutʼükun</ts>
                  <nts id="Seg_167" n="HIAT:ip">.</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_170" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_172" n="HIAT:w" s="T43">Man</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_175" n="HIAT:w" s="T44">nʼarɣa</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_178" n="HIAT:w" s="T45">arakam</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_181" n="HIAT:w" s="T46">ütkou</ts>
                  <nts id="Seg_182" n="HIAT:ip">.</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_185" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_187" n="HIAT:w" s="T47">Man</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_190" n="HIAT:w" s="T48">tʼaran</ts>
                  <nts id="Seg_191" n="HIAT:ip">:</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_193" n="HIAT:ip">“</nts>
                  <ts e="T50" id="Seg_195" n="HIAT:w" s="T49">Man</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_198" n="HIAT:w" s="T50">asʼ</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_201" n="HIAT:w" s="T51">ütkou</ts>
                  <nts id="Seg_202" n="HIAT:ip">.</nts>
                  <nts id="Seg_203" n="HIAT:ip">”</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_206" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_208" n="HIAT:w" s="T52">A</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_211" n="HIAT:w" s="T53">onän</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_214" n="HIAT:w" s="T54">kɨgɨzau</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_217" n="HIAT:w" s="T55">ötku</ts>
                  <nts id="Seg_218" n="HIAT:ip">.</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T56" id="Seg_220" n="sc" s="T1">
               <ts e="T2" id="Seg_222" n="e" s="T1">Man </ts>
               <ts e="T3" id="Seg_224" n="e" s="T2">taben </ts>
               <ts e="T4" id="Seg_226" n="e" s="T3">(tapben) </ts>
               <ts e="T5" id="Seg_228" n="e" s="T4">qoǯirsau </ts>
               <ts e="T6" id="Seg_230" n="e" s="T5">kudäptɨpɨlʼe </ts>
               <ts e="T7" id="Seg_232" n="e" s="T6">dʼaja </ts>
               <ts e="T8" id="Seg_234" n="e" s="T7">Lʼekseim, </ts>
               <ts e="T9" id="Seg_236" n="e" s="T8">Lʼönʼam </ts>
               <ts e="T10" id="Seg_238" n="e" s="T9">qoǯirsau. </ts>
               <ts e="T11" id="Seg_240" n="e" s="T10">Man </ts>
               <ts e="T12" id="Seg_242" n="e" s="T11">teblanä </ts>
               <ts e="T13" id="Seg_244" n="e" s="T12">sernan. </ts>
               <ts e="T14" id="Seg_246" n="e" s="T13">Tebla </ts>
               <ts e="T15" id="Seg_248" n="e" s="T14">aurnattə. </ts>
               <ts e="T16" id="Seg_250" n="e" s="T15">Mazɨm </ts>
               <ts e="T17" id="Seg_252" n="e" s="T16">omdɨlǯattə </ts>
               <ts e="T18" id="Seg_254" n="e" s="T17">aurgu. </ts>
               <ts e="T19" id="Seg_256" n="e" s="T18">Dʼaja </ts>
               <ts e="T20" id="Seg_258" n="e" s="T19">Lʼeksʼej </ts>
               <ts e="T21" id="Seg_260" n="e" s="T20">mekga </ts>
               <ts e="T22" id="Seg_262" n="e" s="T21">tʼärɨn: </ts>
               <ts e="T23" id="Seg_264" n="e" s="T22">“Tan </ts>
               <ts e="T24" id="Seg_266" n="e" s="T23">pragam </ts>
               <ts e="T25" id="Seg_268" n="e" s="T24">ütqwal?” </ts>
               <ts e="T26" id="Seg_270" n="e" s="T25">A </ts>
               <ts e="T27" id="Seg_272" n="e" s="T26">man </ts>
               <ts e="T28" id="Seg_274" n="e" s="T27">tʼaran: </ts>
               <ts e="T29" id="Seg_276" n="e" s="T28">“As </ts>
               <ts e="T30" id="Seg_278" n="e" s="T29">ütqou.” </ts>
               <ts e="T31" id="Seg_280" n="e" s="T30">Täbɨstaɣə </ts>
               <ts e="T32" id="Seg_282" n="e" s="T31">otdə </ts>
               <ts e="T33" id="Seg_284" n="e" s="T32">äraɣə. </ts>
               <ts e="T34" id="Seg_286" n="e" s="T33">Qajno </ts>
               <ts e="T35" id="Seg_288" n="e" s="T34">soːgutʼügu. </ts>
               <ts e="T36" id="Seg_290" n="e" s="T35">Man </ts>
               <ts e="T37" id="Seg_292" n="e" s="T36">wetʼ </ts>
               <ts e="T38" id="Seg_294" n="e" s="T37">kosti </ts>
               <ts e="T39" id="Seg_296" n="e" s="T38">tüpban. </ts>
               <ts e="T40" id="Seg_298" n="e" s="T39">Puskaj </ts>
               <ts e="T41" id="Seg_300" n="e" s="T40">qamǯenädəba. </ts>
               <ts e="T42" id="Seg_302" n="e" s="T41">Ato </ts>
               <ts e="T43" id="Seg_304" n="e" s="T42">soːɣutʼükun. </ts>
               <ts e="T44" id="Seg_306" n="e" s="T43">Man </ts>
               <ts e="T45" id="Seg_308" n="e" s="T44">nʼarɣa </ts>
               <ts e="T46" id="Seg_310" n="e" s="T45">arakam </ts>
               <ts e="T47" id="Seg_312" n="e" s="T46">ütkou. </ts>
               <ts e="T48" id="Seg_314" n="e" s="T47">Man </ts>
               <ts e="T49" id="Seg_316" n="e" s="T48">tʼaran: </ts>
               <ts e="T50" id="Seg_318" n="e" s="T49">“Man </ts>
               <ts e="T51" id="Seg_320" n="e" s="T50">asʼ </ts>
               <ts e="T52" id="Seg_322" n="e" s="T51">ütkou.” </ts>
               <ts e="T53" id="Seg_324" n="e" s="T52">A </ts>
               <ts e="T54" id="Seg_326" n="e" s="T53">onän </ts>
               <ts e="T55" id="Seg_328" n="e" s="T54">kɨgɨzau </ts>
               <ts e="T56" id="Seg_330" n="e" s="T55">ötku. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T10" id="Seg_331" s="T1">PVD_1964_MyDream_nar.001 (001.001)</ta>
            <ta e="T13" id="Seg_332" s="T10">PVD_1964_MyDream_nar.002 (001.002)</ta>
            <ta e="T15" id="Seg_333" s="T13">PVD_1964_MyDream_nar.003 (001.003)</ta>
            <ta e="T18" id="Seg_334" s="T15">PVD_1964_MyDream_nar.004 (001.004)</ta>
            <ta e="T25" id="Seg_335" s="T18">PVD_1964_MyDream_nar.005 (001.005)</ta>
            <ta e="T30" id="Seg_336" s="T25">PVD_1964_MyDream_nar.006 (001.006)</ta>
            <ta e="T33" id="Seg_337" s="T30">PVD_1964_MyDream_nar.007 (001.007)</ta>
            <ta e="T35" id="Seg_338" s="T33">PVD_1964_MyDream_nar.008 (001.008)</ta>
            <ta e="T39" id="Seg_339" s="T35">PVD_1964_MyDream_nar.009 (001.009)</ta>
            <ta e="T41" id="Seg_340" s="T39">PVD_1964_MyDream_nar.010 (001.010)</ta>
            <ta e="T43" id="Seg_341" s="T41">PVD_1964_MyDream_nar.011 (001.011)</ta>
            <ta e="T47" id="Seg_342" s="T43">PVD_1964_MyDream_nar.012 (001.012)</ta>
            <ta e="T52" id="Seg_343" s="T47">PVD_1964_MyDream_nar.013 (001.013)</ta>
            <ta e="T56" id="Seg_344" s="T52">PVD_1964_MyDream_nar.014 (001.014)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T10" id="Seg_345" s="T1">ман та′б̂ен (тапбен) kоджир′сау ку′дӓптыпылʼе дʼаjа Лʼексеим, Лʼӧнʼам kоджир′сау.</ta>
            <ta e="T13" id="Seg_346" s="T10">ман теб′ланӓ ′сернан.</ta>
            <ta e="T15" id="Seg_347" s="T13">теб′ла аур′наттъ.</ta>
            <ta e="T18" id="Seg_348" s="T15">ма′зым ′омдыlджаттъ ′аургу.</ta>
            <ta e="T30" id="Seg_349" s="T25"> а ман тʼа′ран: ас ӱт′kоу.</ta>
            <ta e="T33" id="Seg_350" s="T30">тӓбыстаɣъ ′отдъ(ы) ′ӓраɣъ (′ӓрак).</ta>
            <ta e="T35" id="Seg_351" s="T33">kайно ′со̄г(ɣ)утʼӱгу.</ta>
            <ta e="T39" id="Seg_352" s="T35">ман ветʼ ′кости ′тӱпбан.</ta>
            <ta e="T41" id="Seg_353" s="T39">пускай ′kамдже′нӓдъба.</ta>
            <ta e="T43" id="Seg_354" s="T41">а то ′со̄ɣутʼӱкун.</ta>
            <ta e="T47" id="Seg_355" s="T43">ман нʼарɣа ара′кам ӱт′коу.</ta>
            <ta e="T52" id="Seg_356" s="T47">ман тʼа′ран: ман ′асʼ ӱт′коу.</ta>
            <ta e="T56" id="Seg_357" s="T52">а ′онӓн кыгы′зау ӧт′ку.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T10" id="Seg_358" s="T1">man tab̂en (tapben) qoǯirsau kudäptɨpɨlʼe dʼaja Лʼekseim, Лʼönʼam qoǯirsau.</ta>
            <ta e="T13" id="Seg_359" s="T10">man teblanä sernan.</ta>
            <ta e="T15" id="Seg_360" s="T13">tebla aurnattə.</ta>
            <ta e="T18" id="Seg_361" s="T15">mazɨm omdɨlǯattə aurgu.</ta>
            <ta e="T25" id="Seg_362" s="T18">dʼaja Лʼeksʼej mekga tʼärɨn: tan pragam ütqwal. </ta>
            <ta e="T30" id="Seg_363" s="T25">a man tʼaran: as ütqou.</ta>
            <ta e="T33" id="Seg_364" s="T30">täbɨstaɣə otdə(ɨ) äraɣə (ärak).</ta>
            <ta e="T35" id="Seg_365" s="T33">qajno soːg(ɣ)utʼügu.</ta>
            <ta e="T39" id="Seg_366" s="T35">man wetʼ kosti tüpban.</ta>
            <ta e="T41" id="Seg_367" s="T39">puskaj qamǯenädəba.</ta>
            <ta e="T43" id="Seg_368" s="T41">a to soːɣutʼükun.</ta>
            <ta e="T47" id="Seg_369" s="T43">man nʼarɣa arakam ütkou.</ta>
            <ta e="T52" id="Seg_370" s="T47">man tʼaran: man asʼ ütkou.</ta>
            <ta e="T56" id="Seg_371" s="T52">a onän kɨgɨzau ötku.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T10" id="Seg_372" s="T1">Man taben (tapben) qoǯirsau kudäptɨpɨlʼe dʼaja Lʼekseim, Lʼönʼam qoǯirsau. </ta>
            <ta e="T13" id="Seg_373" s="T10">Man teblanä sernan. </ta>
            <ta e="T15" id="Seg_374" s="T13">Tebla aurnattə. </ta>
            <ta e="T18" id="Seg_375" s="T15">Mazɨm omdɨlǯattə aurgu. </ta>
            <ta e="T25" id="Seg_376" s="T18">Dʼaja Lʼeksʼej mekga tʼärɨn: “Tan pragam ütqwal?” </ta>
            <ta e="T30" id="Seg_377" s="T25">A man tʼaran: “As ütqou.” </ta>
            <ta e="T33" id="Seg_378" s="T30">Täbɨstaɣə otdə äraɣə. </ta>
            <ta e="T35" id="Seg_379" s="T33">Qajno soːgutʼügu. </ta>
            <ta e="T39" id="Seg_380" s="T35">Man wetʼ kosti tüpban. </ta>
            <ta e="T41" id="Seg_381" s="T39">Puskaj qamǯenädəba. </ta>
            <ta e="T43" id="Seg_382" s="T41">Ato soːɣutʼükun. </ta>
            <ta e="T47" id="Seg_383" s="T43">Man nʼarɣa arakam ütkou. </ta>
            <ta e="T52" id="Seg_384" s="T47">Man tʼaran: “Man asʼ ütkou.” </ta>
            <ta e="T56" id="Seg_385" s="T52">A onän kɨgɨzau ötku. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_386" s="T1">man</ta>
            <ta e="T3" id="Seg_387" s="T2">ta-be-n</ta>
            <ta e="T4" id="Seg_388" s="T3">tap-be-n</ta>
            <ta e="T5" id="Seg_389" s="T4">qo-ǯir-sa-u</ta>
            <ta e="T6" id="Seg_390" s="T5">kudä-ptɨ-pɨ-lʼe</ta>
            <ta e="T7" id="Seg_391" s="T6">dʼaja</ta>
            <ta e="T8" id="Seg_392" s="T7">Lʼeksei-m</ta>
            <ta e="T9" id="Seg_393" s="T8">Lʼönʼa-m</ta>
            <ta e="T10" id="Seg_394" s="T9">qo-ǯir-sa-u</ta>
            <ta e="T11" id="Seg_395" s="T10">man</ta>
            <ta e="T12" id="Seg_396" s="T11">teb-la-nä</ta>
            <ta e="T13" id="Seg_397" s="T12">ser-na-n</ta>
            <ta e="T14" id="Seg_398" s="T13">teb-la</ta>
            <ta e="T15" id="Seg_399" s="T14">au-r-na-ttə</ta>
            <ta e="T16" id="Seg_400" s="T15">mazɨm</ta>
            <ta e="T17" id="Seg_401" s="T16">omdɨ-lǯa-ttə</ta>
            <ta e="T18" id="Seg_402" s="T17">au-r-gu</ta>
            <ta e="T19" id="Seg_403" s="T18">dʼaja</ta>
            <ta e="T20" id="Seg_404" s="T19">Lʼeksʼej</ta>
            <ta e="T21" id="Seg_405" s="T20">mekga</ta>
            <ta e="T22" id="Seg_406" s="T21">tʼärɨ-n</ta>
            <ta e="T23" id="Seg_407" s="T22">Tan</ta>
            <ta e="T24" id="Seg_408" s="T23">praga-m</ta>
            <ta e="T25" id="Seg_409" s="T24">üt-qwa-l</ta>
            <ta e="T26" id="Seg_410" s="T25">a</ta>
            <ta e="T27" id="Seg_411" s="T26">man</ta>
            <ta e="T28" id="Seg_412" s="T27">tʼara-n</ta>
            <ta e="T29" id="Seg_413" s="T28">as</ta>
            <ta e="T30" id="Seg_414" s="T29">üt-qo-u</ta>
            <ta e="T31" id="Seg_415" s="T30">täb-ɨ-staɣə</ta>
            <ta e="T32" id="Seg_416" s="T31">otdə</ta>
            <ta e="T33" id="Seg_417" s="T32">ära-ɣə</ta>
            <ta e="T34" id="Seg_418" s="T33">qaj-no</ta>
            <ta e="T35" id="Seg_419" s="T34">soːgutʼü-gu</ta>
            <ta e="T36" id="Seg_420" s="T35">man</ta>
            <ta e="T37" id="Seg_421" s="T36">wetʼ</ta>
            <ta e="T38" id="Seg_422" s="T37">kosti</ta>
            <ta e="T39" id="Seg_423" s="T38">tü-pba-n</ta>
            <ta e="T40" id="Seg_424" s="T39">puskaj</ta>
            <ta e="T41" id="Seg_425" s="T40">qamǯe-nä-də-ba</ta>
            <ta e="T42" id="Seg_426" s="T41">ato</ta>
            <ta e="T43" id="Seg_427" s="T42">soːɣutʼü-ku-n</ta>
            <ta e="T44" id="Seg_428" s="T43">man</ta>
            <ta e="T45" id="Seg_429" s="T44">nʼarɣa</ta>
            <ta e="T46" id="Seg_430" s="T45">araka-m</ta>
            <ta e="T47" id="Seg_431" s="T46">üt-ko-u</ta>
            <ta e="T48" id="Seg_432" s="T47">man</ta>
            <ta e="T49" id="Seg_433" s="T48">tʼara-n</ta>
            <ta e="T50" id="Seg_434" s="T49">Man</ta>
            <ta e="T51" id="Seg_435" s="T50">asʼ</ta>
            <ta e="T52" id="Seg_436" s="T51">üt-ko-u</ta>
            <ta e="T53" id="Seg_437" s="T52">a</ta>
            <ta e="T54" id="Seg_438" s="T53">onän</ta>
            <ta e="T55" id="Seg_439" s="T54">kɨgɨ-za-u</ta>
            <ta e="T56" id="Seg_440" s="T55">öt-ku</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_441" s="T1">man</ta>
            <ta e="T3" id="Seg_442" s="T2">taw-pi-ŋ</ta>
            <ta e="T4" id="Seg_443" s="T3">taw-pi-ŋ</ta>
            <ta e="T5" id="Seg_444" s="T4">qo-nǯir-sɨ-w</ta>
            <ta e="T6" id="Seg_445" s="T5">ködɨ-ptɨ-mbɨ-le</ta>
            <ta e="T7" id="Seg_446" s="T6">dʼaja</ta>
            <ta e="T8" id="Seg_447" s="T7">Alʼeksej-m</ta>
            <ta e="T9" id="Seg_448" s="T8">Lʼönʼa-m</ta>
            <ta e="T10" id="Seg_449" s="T9">qo-nǯir-sɨ-w</ta>
            <ta e="T11" id="Seg_450" s="T10">man</ta>
            <ta e="T12" id="Seg_451" s="T11">täp-la-nä</ta>
            <ta e="T13" id="Seg_452" s="T12">ser-nɨ-ŋ</ta>
            <ta e="T14" id="Seg_453" s="T13">täp-la</ta>
            <ta e="T15" id="Seg_454" s="T14">am-r-nɨ-tɨn</ta>
            <ta e="T16" id="Seg_455" s="T15">mazɨm</ta>
            <ta e="T17" id="Seg_456" s="T16">omdɨ-lǯi-tɨn</ta>
            <ta e="T18" id="Seg_457" s="T17">am-r-gu</ta>
            <ta e="T19" id="Seg_458" s="T18">dʼaja</ta>
            <ta e="T20" id="Seg_459" s="T19">Alʼeksej</ta>
            <ta e="T21" id="Seg_460" s="T20">mekka</ta>
            <ta e="T22" id="Seg_461" s="T21">tʼärɨ-n</ta>
            <ta e="T23" id="Seg_462" s="T22">tan</ta>
            <ta e="T24" id="Seg_463" s="T23">praga-m</ta>
            <ta e="T25" id="Seg_464" s="T24">üt-ku-l</ta>
            <ta e="T26" id="Seg_465" s="T25">a</ta>
            <ta e="T27" id="Seg_466" s="T26">man</ta>
            <ta e="T28" id="Seg_467" s="T27">tʼärɨ-ŋ</ta>
            <ta e="T29" id="Seg_468" s="T28">asa</ta>
            <ta e="T30" id="Seg_469" s="T29">üt-ku-w</ta>
            <ta e="T31" id="Seg_470" s="T30">täp-ɨ-staɣɨ</ta>
            <ta e="T32" id="Seg_471" s="T31">ondə</ta>
            <ta e="T33" id="Seg_472" s="T32">öru-qij</ta>
            <ta e="T34" id="Seg_473" s="T33">qaj-no</ta>
            <ta e="T35" id="Seg_474" s="T34">sogundʼe-gu</ta>
            <ta e="T36" id="Seg_475" s="T35">man</ta>
            <ta e="T37" id="Seg_476" s="T36">wetʼ</ta>
            <ta e="T38" id="Seg_477" s="T37">kosti</ta>
            <ta e="T39" id="Seg_478" s="T38">tüː-mbɨ-ŋ</ta>
            <ta e="T40" id="Seg_479" s="T39">puskaj</ta>
            <ta e="T41" id="Seg_480" s="T40">qamǯu-ne-tɨn-bɨ</ta>
            <ta e="T42" id="Seg_481" s="T41">ato</ta>
            <ta e="T43" id="Seg_482" s="T42">sogundʼe-ku-n</ta>
            <ta e="T44" id="Seg_483" s="T43">man</ta>
            <ta e="T45" id="Seg_484" s="T44">nʼarɣə</ta>
            <ta e="T46" id="Seg_485" s="T45">araŋka-m</ta>
            <ta e="T47" id="Seg_486" s="T46">üt-ku-w</ta>
            <ta e="T48" id="Seg_487" s="T47">man</ta>
            <ta e="T49" id="Seg_488" s="T48">tʼärɨ-ŋ</ta>
            <ta e="T50" id="Seg_489" s="T49">man</ta>
            <ta e="T51" id="Seg_490" s="T50">asa</ta>
            <ta e="T52" id="Seg_491" s="T51">üt-ku-w</ta>
            <ta e="T53" id="Seg_492" s="T52">a</ta>
            <ta e="T54" id="Seg_493" s="T53">oneŋ</ta>
            <ta e="T55" id="Seg_494" s="T54">kɨgɨ-sɨ-w</ta>
            <ta e="T56" id="Seg_495" s="T55">öru-gu</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_496" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_497" s="T2">this-night-ADVZ</ta>
            <ta e="T4" id="Seg_498" s="T3">this-night-ADVZ</ta>
            <ta e="T5" id="Seg_499" s="T4">see-DRV-PST-1SG.O</ta>
            <ta e="T6" id="Seg_500" s="T5">dream-VBLZ-DUR-CVB</ta>
            <ta e="T7" id="Seg_501" s="T6">uncle.[NOM]</ta>
            <ta e="T8" id="Seg_502" s="T7">Alexei-ACC</ta>
            <ta e="T9" id="Seg_503" s="T8">Ljonya-ACC</ta>
            <ta e="T10" id="Seg_504" s="T9">see-DRV-PST-1SG.O</ta>
            <ta e="T11" id="Seg_505" s="T10">I.NOM</ta>
            <ta e="T12" id="Seg_506" s="T11">(s)he-PL-ALL</ta>
            <ta e="T13" id="Seg_507" s="T12">come.in-CO-1SG.S</ta>
            <ta e="T14" id="Seg_508" s="T13">(s)he-PL.[NOM]</ta>
            <ta e="T15" id="Seg_509" s="T14">eat-FRQ-CO-3PL</ta>
            <ta e="T16" id="Seg_510" s="T15">I.ACC</ta>
            <ta e="T17" id="Seg_511" s="T16">sit.down-TR-3PL</ta>
            <ta e="T18" id="Seg_512" s="T17">eat-FRQ-INF</ta>
            <ta e="T19" id="Seg_513" s="T18">uncle.[NOM]</ta>
            <ta e="T20" id="Seg_514" s="T19">Alexei.[NOM]</ta>
            <ta e="T21" id="Seg_515" s="T20">I.ALL</ta>
            <ta e="T22" id="Seg_516" s="T21">say-3SG.S</ta>
            <ta e="T23" id="Seg_517" s="T22">you.SG.NOM</ta>
            <ta e="T24" id="Seg_518" s="T23">moonshine-ACC</ta>
            <ta e="T25" id="Seg_519" s="T24">drink-HAB-2SG.O</ta>
            <ta e="T26" id="Seg_520" s="T25">and</ta>
            <ta e="T27" id="Seg_521" s="T26">I.NOM</ta>
            <ta e="T28" id="Seg_522" s="T27">say-1SG.S</ta>
            <ta e="T29" id="Seg_523" s="T28">NEG</ta>
            <ta e="T30" id="Seg_524" s="T29">drink-HAB-1SG.O</ta>
            <ta e="T31" id="Seg_525" s="T30">(s)he-EP-DU.[NOM]</ta>
            <ta e="T32" id="Seg_526" s="T31">oneself.3SG</ta>
            <ta e="T33" id="Seg_527" s="T32">drink-3DU.S</ta>
            <ta e="T34" id="Seg_528" s="T33">what-TRL</ta>
            <ta e="T35" id="Seg_529" s="T34">ask-INF</ta>
            <ta e="T36" id="Seg_530" s="T35">I.NOM</ta>
            <ta e="T37" id="Seg_531" s="T36">still</ta>
            <ta e="T38" id="Seg_532" s="T37">visiting</ta>
            <ta e="T39" id="Seg_533" s="T38">come-PST.NAR-1SG.S</ta>
            <ta e="T40" id="Seg_534" s="T39">JUSS</ta>
            <ta e="T41" id="Seg_535" s="T40">pour-CONJ-3PL-IRREAL</ta>
            <ta e="T42" id="Seg_536" s="T41">otherwise</ta>
            <ta e="T43" id="Seg_537" s="T42">ask-HAB-3SG.S</ta>
            <ta e="T44" id="Seg_538" s="T43">I.NOM</ta>
            <ta e="T45" id="Seg_539" s="T44">red</ta>
            <ta e="T46" id="Seg_540" s="T45">vodka-ACC</ta>
            <ta e="T47" id="Seg_541" s="T46">water-HAB-1SG.O</ta>
            <ta e="T48" id="Seg_542" s="T47">I.NOM</ta>
            <ta e="T49" id="Seg_543" s="T48">say-1SG.S</ta>
            <ta e="T50" id="Seg_544" s="T49">I.NOM</ta>
            <ta e="T51" id="Seg_545" s="T50">NEG</ta>
            <ta e="T52" id="Seg_546" s="T51">water-HAB-1SG.O</ta>
            <ta e="T53" id="Seg_547" s="T52">and</ta>
            <ta e="T54" id="Seg_548" s="T53">oneself.1SG</ta>
            <ta e="T55" id="Seg_549" s="T54">want-PST-1SG.O</ta>
            <ta e="T56" id="Seg_550" s="T55">drink-INF</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_551" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_552" s="T2">этот-ночь-ADVZ</ta>
            <ta e="T4" id="Seg_553" s="T3">этот-ночь-ADVZ</ta>
            <ta e="T5" id="Seg_554" s="T4">увидеть-DRV-PST-1SG.O</ta>
            <ta e="T6" id="Seg_555" s="T5">сон-VBLZ-DUR-CVB</ta>
            <ta e="T7" id="Seg_556" s="T6">дядя.[NOM]</ta>
            <ta e="T8" id="Seg_557" s="T7">Алексей-ACC</ta>
            <ta e="T9" id="Seg_558" s="T8">Лёня-ACC</ta>
            <ta e="T10" id="Seg_559" s="T9">увидеть-DRV-PST-1SG.O</ta>
            <ta e="T11" id="Seg_560" s="T10">я.NOM</ta>
            <ta e="T12" id="Seg_561" s="T11">он(а)-PL-ALL</ta>
            <ta e="T13" id="Seg_562" s="T12">зайти-CO-1SG.S</ta>
            <ta e="T14" id="Seg_563" s="T13">он(а)-PL.[NOM]</ta>
            <ta e="T15" id="Seg_564" s="T14">съесть-FRQ-CO-3PL</ta>
            <ta e="T16" id="Seg_565" s="T15">я.ACC</ta>
            <ta e="T17" id="Seg_566" s="T16">сесть-TR-3PL</ta>
            <ta e="T18" id="Seg_567" s="T17">съесть-FRQ-INF</ta>
            <ta e="T19" id="Seg_568" s="T18">дядя.[NOM]</ta>
            <ta e="T20" id="Seg_569" s="T19">Алексей.[NOM]</ta>
            <ta e="T21" id="Seg_570" s="T20">я.ALL</ta>
            <ta e="T22" id="Seg_571" s="T21">сказать-3SG.S</ta>
            <ta e="T23" id="Seg_572" s="T22">ты.NOM</ta>
            <ta e="T24" id="Seg_573" s="T23">брага-ACC</ta>
            <ta e="T25" id="Seg_574" s="T24">пить-HAB-2SG.O</ta>
            <ta e="T26" id="Seg_575" s="T25">а</ta>
            <ta e="T27" id="Seg_576" s="T26">я.NOM</ta>
            <ta e="T28" id="Seg_577" s="T27">сказать-1SG.S</ta>
            <ta e="T29" id="Seg_578" s="T28">NEG</ta>
            <ta e="T30" id="Seg_579" s="T29">пить-HAB-1SG.O</ta>
            <ta e="T31" id="Seg_580" s="T30">он(а)-EP-DU.[NOM]</ta>
            <ta e="T32" id="Seg_581" s="T31">сам.3SG</ta>
            <ta e="T33" id="Seg_582" s="T32">пить-3DU.S</ta>
            <ta e="T34" id="Seg_583" s="T33">что-TRL</ta>
            <ta e="T35" id="Seg_584" s="T34">спросить-INF</ta>
            <ta e="T36" id="Seg_585" s="T35">я.NOM</ta>
            <ta e="T37" id="Seg_586" s="T36">ведь</ta>
            <ta e="T38" id="Seg_587" s="T37">в.гости</ta>
            <ta e="T39" id="Seg_588" s="T38">прийти-PST.NAR-1SG.S</ta>
            <ta e="T40" id="Seg_589" s="T39">JUSS</ta>
            <ta e="T41" id="Seg_590" s="T40">налить-CONJ-3PL-IRREAL</ta>
            <ta e="T42" id="Seg_591" s="T41">а.то</ta>
            <ta e="T43" id="Seg_592" s="T42">спросить-HAB-3SG.S</ta>
            <ta e="T44" id="Seg_593" s="T43">я.NOM</ta>
            <ta e="T45" id="Seg_594" s="T44">красный</ta>
            <ta e="T46" id="Seg_595" s="T45">водка-ACC</ta>
            <ta e="T47" id="Seg_596" s="T46">вода-HAB-1SG.O</ta>
            <ta e="T48" id="Seg_597" s="T47">я.NOM</ta>
            <ta e="T49" id="Seg_598" s="T48">сказать-1SG.S</ta>
            <ta e="T50" id="Seg_599" s="T49">я.NOM</ta>
            <ta e="T51" id="Seg_600" s="T50">NEG</ta>
            <ta e="T52" id="Seg_601" s="T51">вода-HAB-1SG.O</ta>
            <ta e="T53" id="Seg_602" s="T52">а</ta>
            <ta e="T54" id="Seg_603" s="T53">сам.1SG</ta>
            <ta e="T55" id="Seg_604" s="T54">хотеть-PST-1SG.O</ta>
            <ta e="T56" id="Seg_605" s="T55">пить-INF</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_606" s="T1">pers</ta>
            <ta e="T3" id="Seg_607" s="T2">dem-n-n&gt;adv</ta>
            <ta e="T4" id="Seg_608" s="T3">dem-n-n&gt;adv</ta>
            <ta e="T5" id="Seg_609" s="T4">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_610" s="T5">n-n&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T7" id="Seg_611" s="T6">n.[n:case]</ta>
            <ta e="T8" id="Seg_612" s="T7">nprop-n:case</ta>
            <ta e="T9" id="Seg_613" s="T8">nprop-n:case</ta>
            <ta e="T10" id="Seg_614" s="T9">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_615" s="T10">pers</ta>
            <ta e="T12" id="Seg_616" s="T11">pers-n:num-n:case</ta>
            <ta e="T13" id="Seg_617" s="T12">v-v:ins-v:pn</ta>
            <ta e="T14" id="Seg_618" s="T13">pers-n:num.[n:case]</ta>
            <ta e="T15" id="Seg_619" s="T14">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T16" id="Seg_620" s="T15">pers</ta>
            <ta e="T17" id="Seg_621" s="T16">v-v&gt;v-v:pn</ta>
            <ta e="T18" id="Seg_622" s="T17">v-v&gt;v-v:inf</ta>
            <ta e="T19" id="Seg_623" s="T18">n.[n:case]</ta>
            <ta e="T20" id="Seg_624" s="T19">nprop.[n:case]</ta>
            <ta e="T21" id="Seg_625" s="T20">pers</ta>
            <ta e="T22" id="Seg_626" s="T21">v-v:pn</ta>
            <ta e="T23" id="Seg_627" s="T22">pers</ta>
            <ta e="T24" id="Seg_628" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_629" s="T24">v-v&gt;v-v:pn</ta>
            <ta e="T26" id="Seg_630" s="T25">conj</ta>
            <ta e="T27" id="Seg_631" s="T26">pers</ta>
            <ta e="T28" id="Seg_632" s="T27">v-v:pn</ta>
            <ta e="T29" id="Seg_633" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_634" s="T29">v-v&gt;v-v:pn</ta>
            <ta e="T31" id="Seg_635" s="T30">pers-n:ins-n:num.[n:case]</ta>
            <ta e="T32" id="Seg_636" s="T31">emphpro</ta>
            <ta e="T33" id="Seg_637" s="T32">v-v:pn</ta>
            <ta e="T34" id="Seg_638" s="T33">interrog-n:case</ta>
            <ta e="T35" id="Seg_639" s="T34">v-v:inf</ta>
            <ta e="T36" id="Seg_640" s="T35">pers</ta>
            <ta e="T37" id="Seg_641" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_642" s="T37">adv</ta>
            <ta e="T39" id="Seg_643" s="T38">v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_644" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_645" s="T40">v-v:mood-v:pn-ptcl</ta>
            <ta e="T42" id="Seg_646" s="T41">conj</ta>
            <ta e="T43" id="Seg_647" s="T42">v-v&gt;v-v:pn</ta>
            <ta e="T44" id="Seg_648" s="T43">pers</ta>
            <ta e="T45" id="Seg_649" s="T44">adj</ta>
            <ta e="T46" id="Seg_650" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_651" s="T46">n-v&gt;v-v:pn</ta>
            <ta e="T48" id="Seg_652" s="T47">pers</ta>
            <ta e="T49" id="Seg_653" s="T48">v-v:pn</ta>
            <ta e="T50" id="Seg_654" s="T49">pers</ta>
            <ta e="T51" id="Seg_655" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_656" s="T51">n-v&gt;v-v:pn</ta>
            <ta e="T53" id="Seg_657" s="T52">conj</ta>
            <ta e="T54" id="Seg_658" s="T53">emphpro</ta>
            <ta e="T55" id="Seg_659" s="T54">v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_660" s="T55">v-v:inf</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_661" s="T1">pers</ta>
            <ta e="T3" id="Seg_662" s="T2">adv</ta>
            <ta e="T4" id="Seg_663" s="T3">adv</ta>
            <ta e="T5" id="Seg_664" s="T4">v</ta>
            <ta e="T6" id="Seg_665" s="T5">adv</ta>
            <ta e="T7" id="Seg_666" s="T6">n</ta>
            <ta e="T8" id="Seg_667" s="T7">nprop</ta>
            <ta e="T9" id="Seg_668" s="T8">nprop</ta>
            <ta e="T10" id="Seg_669" s="T9">v</ta>
            <ta e="T11" id="Seg_670" s="T10">pers</ta>
            <ta e="T12" id="Seg_671" s="T11">pers</ta>
            <ta e="T13" id="Seg_672" s="T12">v</ta>
            <ta e="T14" id="Seg_673" s="T13">pers</ta>
            <ta e="T15" id="Seg_674" s="T14">v</ta>
            <ta e="T16" id="Seg_675" s="T15">pers</ta>
            <ta e="T17" id="Seg_676" s="T16">v</ta>
            <ta e="T18" id="Seg_677" s="T17">v</ta>
            <ta e="T19" id="Seg_678" s="T18">n</ta>
            <ta e="T20" id="Seg_679" s="T19">nprop</ta>
            <ta e="T21" id="Seg_680" s="T20">pers</ta>
            <ta e="T22" id="Seg_681" s="T21">v</ta>
            <ta e="T23" id="Seg_682" s="T22">pers</ta>
            <ta e="T24" id="Seg_683" s="T23">n</ta>
            <ta e="T25" id="Seg_684" s="T24">v</ta>
            <ta e="T26" id="Seg_685" s="T25">conj</ta>
            <ta e="T27" id="Seg_686" s="T26">pers</ta>
            <ta e="T28" id="Seg_687" s="T27">v</ta>
            <ta e="T29" id="Seg_688" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_689" s="T29">v</ta>
            <ta e="T31" id="Seg_690" s="T30">pers</ta>
            <ta e="T32" id="Seg_691" s="T31">emphpro</ta>
            <ta e="T33" id="Seg_692" s="T32">v</ta>
            <ta e="T34" id="Seg_693" s="T33">interrog</ta>
            <ta e="T35" id="Seg_694" s="T34">v</ta>
            <ta e="T36" id="Seg_695" s="T35">pers</ta>
            <ta e="T37" id="Seg_696" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_697" s="T37">adv</ta>
            <ta e="T39" id="Seg_698" s="T38">v</ta>
            <ta e="T40" id="Seg_699" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_700" s="T40">v</ta>
            <ta e="T42" id="Seg_701" s="T41">conj</ta>
            <ta e="T43" id="Seg_702" s="T42">v</ta>
            <ta e="T44" id="Seg_703" s="T43">pers</ta>
            <ta e="T45" id="Seg_704" s="T44">adj</ta>
            <ta e="T46" id="Seg_705" s="T45">n</ta>
            <ta e="T47" id="Seg_706" s="T46">v</ta>
            <ta e="T48" id="Seg_707" s="T47">pers</ta>
            <ta e="T49" id="Seg_708" s="T48">v</ta>
            <ta e="T50" id="Seg_709" s="T49">pers</ta>
            <ta e="T51" id="Seg_710" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_711" s="T51">v</ta>
            <ta e="T53" id="Seg_712" s="T52">conj</ta>
            <ta e="T54" id="Seg_713" s="T53">emphpro</ta>
            <ta e="T55" id="Seg_714" s="T54">v</ta>
            <ta e="T56" id="Seg_715" s="T55">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_716" s="T1">pro.h:E</ta>
            <ta e="T3" id="Seg_717" s="T2">adv:Time</ta>
            <ta e="T8" id="Seg_718" s="T7">np.h:Th</ta>
            <ta e="T9" id="Seg_719" s="T8">np.h:Th</ta>
            <ta e="T10" id="Seg_720" s="T9">0.1.h:E</ta>
            <ta e="T11" id="Seg_721" s="T10">pro.h:A</ta>
            <ta e="T12" id="Seg_722" s="T11">pro.h:G</ta>
            <ta e="T14" id="Seg_723" s="T13">pro.h:A</ta>
            <ta e="T16" id="Seg_724" s="T15">pro.h:Th</ta>
            <ta e="T17" id="Seg_725" s="T16">0.3.h:A</ta>
            <ta e="T20" id="Seg_726" s="T19">np.h:A</ta>
            <ta e="T21" id="Seg_727" s="T20">pro.h:R</ta>
            <ta e="T23" id="Seg_728" s="T22">pro.h:A</ta>
            <ta e="T24" id="Seg_729" s="T23">np:P</ta>
            <ta e="T27" id="Seg_730" s="T26">pro.h:A</ta>
            <ta e="T30" id="Seg_731" s="T29">0.1.h:A 0.3:Th</ta>
            <ta e="T31" id="Seg_732" s="T30">pro.h:A</ta>
            <ta e="T36" id="Seg_733" s="T35">pro.h:A</ta>
            <ta e="T41" id="Seg_734" s="T40">0.3.h:A</ta>
            <ta e="T43" id="Seg_735" s="T42">0.3.h:A</ta>
            <ta e="T44" id="Seg_736" s="T43">pro.h:A</ta>
            <ta e="T46" id="Seg_737" s="T45">np:P</ta>
            <ta e="T48" id="Seg_738" s="T47">pro.h:A</ta>
            <ta e="T50" id="Seg_739" s="T49">pro.h:A</ta>
            <ta e="T55" id="Seg_740" s="T54">0.1.h:E</ta>
            <ta e="T56" id="Seg_741" s="T55">v:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_742" s="T1">pro.h:S</ta>
            <ta e="T5" id="Seg_743" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_744" s="T5">s:temp</ta>
            <ta e="T8" id="Seg_745" s="T7">np.h:O</ta>
            <ta e="T9" id="Seg_746" s="T8">np.h:O</ta>
            <ta e="T10" id="Seg_747" s="T9">0.1.h:S v:pred</ta>
            <ta e="T11" id="Seg_748" s="T10">pro.h:S</ta>
            <ta e="T13" id="Seg_749" s="T12">v:pred</ta>
            <ta e="T14" id="Seg_750" s="T13">pro.h:S</ta>
            <ta e="T15" id="Seg_751" s="T14">v:pred</ta>
            <ta e="T16" id="Seg_752" s="T15">pro.h:O</ta>
            <ta e="T17" id="Seg_753" s="T16">0.3.h:S v:pred</ta>
            <ta e="T18" id="Seg_754" s="T17">s:purp</ta>
            <ta e="T20" id="Seg_755" s="T19">np.h:S</ta>
            <ta e="T22" id="Seg_756" s="T21">v:pred</ta>
            <ta e="T23" id="Seg_757" s="T22">pro.h:S</ta>
            <ta e="T24" id="Seg_758" s="T23">np:O</ta>
            <ta e="T25" id="Seg_759" s="T24">v:pred</ta>
            <ta e="T27" id="Seg_760" s="T26">pro.h:S</ta>
            <ta e="T28" id="Seg_761" s="T27">v:pred</ta>
            <ta e="T30" id="Seg_762" s="T29">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T31" id="Seg_763" s="T30">pro.h:S</ta>
            <ta e="T33" id="Seg_764" s="T32">v:pred</ta>
            <ta e="T36" id="Seg_765" s="T35">pro.h:S</ta>
            <ta e="T39" id="Seg_766" s="T38">v:pred</ta>
            <ta e="T41" id="Seg_767" s="T40">0.3.h:S v:pred</ta>
            <ta e="T43" id="Seg_768" s="T42">0.3.h:S v:pred</ta>
            <ta e="T44" id="Seg_769" s="T43">pro.h:S</ta>
            <ta e="T46" id="Seg_770" s="T45">np:O</ta>
            <ta e="T47" id="Seg_771" s="T46">v:pred</ta>
            <ta e="T48" id="Seg_772" s="T47">pro.h:S</ta>
            <ta e="T49" id="Seg_773" s="T48">v:pred</ta>
            <ta e="T50" id="Seg_774" s="T49">pro.h:S</ta>
            <ta e="T52" id="Seg_775" s="T51">v:pred</ta>
            <ta e="T55" id="Seg_776" s="T54">0.1.h:S v:pred</ta>
            <ta e="T56" id="Seg_777" s="T55">v:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T7" id="Seg_778" s="T6">RUS:cult</ta>
            <ta e="T8" id="Seg_779" s="T7">RUS:cult</ta>
            <ta e="T19" id="Seg_780" s="T18">RUS:cult</ta>
            <ta e="T20" id="Seg_781" s="T19">RUS:cult</ta>
            <ta e="T24" id="Seg_782" s="T23">RUS:cult</ta>
            <ta e="T26" id="Seg_783" s="T25">RUS:gram</ta>
            <ta e="T37" id="Seg_784" s="T36">RUS:disc</ta>
            <ta e="T38" id="Seg_785" s="T37">RUS:cult</ta>
            <ta e="T40" id="Seg_786" s="T39">RUS:gram</ta>
            <ta e="T41" id="Seg_787" s="T40">RUS:gram</ta>
            <ta e="T42" id="Seg_788" s="T41">RUS:gram</ta>
            <ta e="T53" id="Seg_789" s="T52">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T10" id="Seg_790" s="T1">Yesterday night I saw uncle Aleksej in my dream, and I saw Ljonjka.</ta>
            <ta e="T13" id="Seg_791" s="T10">I came to them.</ta>
            <ta e="T15" id="Seg_792" s="T13">They were eating.</ta>
            <ta e="T18" id="Seg_793" s="T15">They invited me to have dinner.</ta>
            <ta e="T25" id="Seg_794" s="T18">Uncle Aleksej said to me: “Do you drink moonshine?”</ta>
            <ta e="T30" id="Seg_795" s="T25">And I said: “No, I don't.”</ta>
            <ta e="T33" id="Seg_796" s="T30">The two of them were drinking.</ta>
            <ta e="T35" id="Seg_797" s="T33">And why would one be asking.</ta>
            <ta e="T39" id="Seg_798" s="T35">I came to visit them.</ta>
            <ta e="T41" id="Seg_799" s="T39">They could have given me something to drink.</ta>
            <ta e="T43" id="Seg_800" s="T41">And they were asking.</ta>
            <ta e="T47" id="Seg_801" s="T43">I drink red wine.</ta>
            <ta e="T52" id="Seg_802" s="T47">I said: “I don't drink”.</ta>
            <ta e="T56" id="Seg_803" s="T52">And in fact I wanted to get a drink.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T10" id="Seg_804" s="T1">Gestern nacht sah ich in meinem Traum Onkel Alexej, und ich sah Ljonka.</ta>
            <ta e="T13" id="Seg_805" s="T10">Ich kam zu ihnen.</ta>
            <ta e="T15" id="Seg_806" s="T13">Sie haben gegessen.</ta>
            <ta e="T18" id="Seg_807" s="T15">Sie luden mich ein [mit ihnen] zu essen.</ta>
            <ta e="T25" id="Seg_808" s="T18">Onkel Alexej sagte zu mir: "Trinkst du selbstgebrannten Schnaps?"</ta>
            <ta e="T30" id="Seg_809" s="T25">Und ich sage: "Nein, trinke ich nicht."</ta>
            <ta e="T33" id="Seg_810" s="T30">Die beiden tranken.</ta>
            <ta e="T35" id="Seg_811" s="T33">Und warum sollte man fragen.</ta>
            <ta e="T39" id="Seg_812" s="T35">Ich kam sie besuchen.</ta>
            <ta e="T41" id="Seg_813" s="T39">Sie hätten mir was zu trinken geben können.</ta>
            <ta e="T43" id="Seg_814" s="T41">Und sie haben gefragt.</ta>
            <ta e="T47" id="Seg_815" s="T43">Ich trinke Rotwein.</ta>
            <ta e="T52" id="Seg_816" s="T47">Ich sagte: "Ich trinke nicht."</ta>
            <ta e="T56" id="Seg_817" s="T52">Und eigentlich wollte ich trinken.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T10" id="Seg_818" s="T1">Я сегодня ночью видала во сне дядю Алексея, и Лёньку видела.</ta>
            <ta e="T13" id="Seg_819" s="T10">Я к ним зашла.</ta>
            <ta e="T15" id="Seg_820" s="T13">Они едят.</ta>
            <ta e="T18" id="Seg_821" s="T15">Меня посадили обедать.</ta>
            <ta e="T25" id="Seg_822" s="T18">Дядя Алексей мне говорит: “Ты брагу пьешь?” </ta>
            <ta e="T30" id="Seg_823" s="T25">А я сказала: “Я не пью”.</ta>
            <ta e="T33" id="Seg_824" s="T30">Они двое пьют.</ta>
            <ta e="T35" id="Seg_825" s="T33">А зачем спрашивать.</ta>
            <ta e="T39" id="Seg_826" s="T35">Я ведь в гости пришла.</ta>
            <ta e="T41" id="Seg_827" s="T39">Пускай налили бы.</ta>
            <ta e="T43" id="Seg_828" s="T41">А то спрашивает.</ta>
            <ta e="T47" id="Seg_829" s="T43">Я красную водку (вино?) пью.</ta>
            <ta e="T52" id="Seg_830" s="T47">Я сказала: “Я не пью”.</ta>
            <ta e="T56" id="Seg_831" s="T52">А сама хотела выпить.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T10" id="Seg_832" s="T1">я сегодня ночью видала во сне дядю Алексея и Лёньку видела</ta>
            <ta e="T13" id="Seg_833" s="T10">я к ним зашла</ta>
            <ta e="T15" id="Seg_834" s="T13">они едят</ta>
            <ta e="T18" id="Seg_835" s="T15">меня посадили обедать</ta>
            <ta e="T25" id="Seg_836" s="T18">дядя Алексей мне говорит ты брагу пьешь </ta>
            <ta e="T30" id="Seg_837" s="T25">а я сказала я не пью</ta>
            <ta e="T33" id="Seg_838" s="T30">они двое пьют</ta>
            <ta e="T35" id="Seg_839" s="T33">а зачем спрашивать</ta>
            <ta e="T39" id="Seg_840" s="T35">я ведь в гости пришла</ta>
            <ta e="T41" id="Seg_841" s="T39">налили бы</ta>
            <ta e="T43" id="Seg_842" s="T41">а то спрашивают</ta>
            <ta e="T47" id="Seg_843" s="T43">я красную водку пью</ta>
            <ta e="T52" id="Seg_844" s="T47">я сказала я не пью</ta>
            <ta e="T56" id="Seg_845" s="T52">а сама хотела выпить</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T33" id="Seg_846" s="T30">[KuAI:] Variants: 'otdɨ', 'ärak'.</ta>
            <ta e="T35" id="Seg_847" s="T33">[KuAI:] Variant: 'soːɣutʼügu'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
