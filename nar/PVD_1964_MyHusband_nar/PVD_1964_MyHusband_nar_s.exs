<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_MyHusband_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_MyHusband_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">93</ud-information>
            <ud-information attribute-name="# HIAT:w">74</ud-information>
            <ud-information attribute-name="# e">74</ud-information>
            <ud-information attribute-name="# HIAT:u">17</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T75" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">täbɨtdɨzan</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">maladon</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">jezan</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Mekga</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">sɨsari</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">pot</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">tʼäkgus</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_32" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">Man</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">ärau</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">ugon</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">surumsa</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">i</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_49" n="HIAT:w" s="T14">qwɛllɨssa</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_53" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">Me</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">täpse</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">okkɨr</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_64" n="HIAT:w" s="T18">poj</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_66" n="HIAT:ip">(</nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">sədäwi</ts>
                  <nts id="Seg_69" n="HIAT:ip">)</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_72" n="HIAT:w" s="T20">jezaj</ts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_76" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">Man</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">täbɨm</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_84" n="HIAT:w" s="T23">soːrɨzau</ts>
                  <nts id="Seg_85" n="HIAT:ip">.</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_88" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_90" n="HIAT:w" s="T24">Soːdiga</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_93" n="HIAT:w" s="T25">qum</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_96" n="HIAT:w" s="T26">jes</ts>
                  <nts id="Seg_97" n="HIAT:ip">.</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_100" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_102" n="HIAT:w" s="T27">Täp</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_105" n="HIAT:w" s="T28">ass</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_108" n="HIAT:w" s="T29">tättəbɨs</ts>
                  <nts id="Seg_109" n="HIAT:ip">.</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_112" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_114" n="HIAT:w" s="T30">Mazɨm</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_117" n="HIAT:w" s="T31">ass</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_120" n="HIAT:w" s="T32">qwatkus</ts>
                  <nts id="Seg_121" n="HIAT:ip">.</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_124" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_126" n="HIAT:w" s="T33">Täp</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_129" n="HIAT:w" s="T34">surɨmsa</ts>
                  <nts id="Seg_130" n="HIAT:ip">.</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_133" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_135" n="HIAT:w" s="T35">Surɨmlam</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_138" n="HIAT:w" s="T36">koːtʼin</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_141" n="HIAT:w" s="T37">qwatkustə</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_144" n="HIAT:w" s="T38">i</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_147" n="HIAT:w" s="T39">qwɛlɨm</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_150" n="HIAT:w" s="T40">kocʼin</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_153" n="HIAT:w" s="T41">qwatkustə</ts>
                  <nts id="Seg_154" n="HIAT:ip">.</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_157" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_159" n="HIAT:w" s="T42">Täp</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_162" n="HIAT:w" s="T43">kaʒnɨj</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_165" n="HIAT:w" s="T44">dʼel</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_168" n="HIAT:w" s="T45">nagur</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_171" n="HIAT:w" s="T46">tatkudət</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_174" n="HIAT:w" s="T47">i</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_177" n="HIAT:w" s="T48">sədəjagla</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_180" n="HIAT:w" s="T49">i</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_183" n="HIAT:w" s="T50">okkɨr</ts>
                  <nts id="Seg_184" n="HIAT:ip">.</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_187" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_189" n="HIAT:w" s="T51">Sʼütʼdʼibɨn</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_192" n="HIAT:w" s="T52">ass</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_195" n="HIAT:w" s="T53">tükkus</ts>
                  <nts id="Seg_196" n="HIAT:ip">.</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_199" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_201" n="HIAT:w" s="T54">Man</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_204" n="HIAT:w" s="T55">kaʒnɨj</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_207" n="HIAT:w" s="T56">dʼel</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_210" n="HIAT:w" s="T57">tutdɨzan</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_213" n="HIAT:w" s="T58">qwɛlʼe</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_216" n="HIAT:w" s="T59">Kudəmargedot</ts>
                  <nts id="Seg_217" n="HIAT:ip">.</nts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_220" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_222" n="HIAT:w" s="T60">Täp</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_225" n="HIAT:w" s="T61">tʼelɨpba</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_228" n="HIAT:w" s="T62">üguzʼä</ts>
                  <nts id="Seg_229" n="HIAT:ip">.</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_232" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_234" n="HIAT:w" s="T63">A</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_237" n="HIAT:w" s="T64">na</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_240" n="HIAT:w" s="T65">jugum</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_243" n="HIAT:w" s="T66">eudə</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_246" n="HIAT:w" s="T67">ürčubat</ts>
                  <nts id="Seg_247" n="HIAT:ip">.</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_250" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_252" n="HIAT:w" s="T68">Täp</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_255" n="HIAT:w" s="T69">ešo</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_258" n="HIAT:w" s="T70">kocʼin</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_261" n="HIAT:w" s="T71">qwatkunet</ts>
                  <nts id="Seg_262" n="HIAT:ip">.</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_265" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_267" n="HIAT:w" s="T72">Man</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_270" n="HIAT:w" s="T73">arau</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_273" n="HIAT:w" s="T74">Qɨngojessä</ts>
                  <nts id="Seg_274" n="HIAT:ip">.</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T75" id="Seg_276" n="sc" s="T1">
               <ts e="T2" id="Seg_278" n="e" s="T1">Man </ts>
               <ts e="T3" id="Seg_280" n="e" s="T2">täbɨtdɨzan </ts>
               <ts e="T4" id="Seg_282" n="e" s="T3">maladon </ts>
               <ts e="T5" id="Seg_284" n="e" s="T4">jezan. </ts>
               <ts e="T6" id="Seg_286" n="e" s="T5">Mekga </ts>
               <ts e="T7" id="Seg_288" n="e" s="T6">sɨsari </ts>
               <ts e="T8" id="Seg_290" n="e" s="T7">pot </ts>
               <ts e="T9" id="Seg_292" n="e" s="T8">tʼäkgus. </ts>
               <ts e="T10" id="Seg_294" n="e" s="T9">Man </ts>
               <ts e="T11" id="Seg_296" n="e" s="T10">ärau </ts>
               <ts e="T12" id="Seg_298" n="e" s="T11">ugon </ts>
               <ts e="T13" id="Seg_300" n="e" s="T12">surumsa </ts>
               <ts e="T14" id="Seg_302" n="e" s="T13">i </ts>
               <ts e="T15" id="Seg_304" n="e" s="T14">qwɛllɨssa. </ts>
               <ts e="T16" id="Seg_306" n="e" s="T15">Me </ts>
               <ts e="T17" id="Seg_308" n="e" s="T16">täpse </ts>
               <ts e="T18" id="Seg_310" n="e" s="T17">okkɨr </ts>
               <ts e="T19" id="Seg_312" n="e" s="T18">poj </ts>
               <ts e="T20" id="Seg_314" n="e" s="T19">(sədäwi) </ts>
               <ts e="T21" id="Seg_316" n="e" s="T20">jezaj. </ts>
               <ts e="T22" id="Seg_318" n="e" s="T21">Man </ts>
               <ts e="T23" id="Seg_320" n="e" s="T22">täbɨm </ts>
               <ts e="T24" id="Seg_322" n="e" s="T23">soːrɨzau. </ts>
               <ts e="T25" id="Seg_324" n="e" s="T24">Soːdiga </ts>
               <ts e="T26" id="Seg_326" n="e" s="T25">qum </ts>
               <ts e="T27" id="Seg_328" n="e" s="T26">jes. </ts>
               <ts e="T28" id="Seg_330" n="e" s="T27">Täp </ts>
               <ts e="T29" id="Seg_332" n="e" s="T28">ass </ts>
               <ts e="T30" id="Seg_334" n="e" s="T29">tättəbɨs. </ts>
               <ts e="T31" id="Seg_336" n="e" s="T30">Mazɨm </ts>
               <ts e="T32" id="Seg_338" n="e" s="T31">ass </ts>
               <ts e="T33" id="Seg_340" n="e" s="T32">qwatkus. </ts>
               <ts e="T34" id="Seg_342" n="e" s="T33">Täp </ts>
               <ts e="T35" id="Seg_344" n="e" s="T34">surɨmsa. </ts>
               <ts e="T36" id="Seg_346" n="e" s="T35">Surɨmlam </ts>
               <ts e="T37" id="Seg_348" n="e" s="T36">koːtʼin </ts>
               <ts e="T38" id="Seg_350" n="e" s="T37">qwatkustə </ts>
               <ts e="T39" id="Seg_352" n="e" s="T38">i </ts>
               <ts e="T40" id="Seg_354" n="e" s="T39">qwɛlɨm </ts>
               <ts e="T41" id="Seg_356" n="e" s="T40">kocʼin </ts>
               <ts e="T42" id="Seg_358" n="e" s="T41">qwatkustə. </ts>
               <ts e="T43" id="Seg_360" n="e" s="T42">Täp </ts>
               <ts e="T44" id="Seg_362" n="e" s="T43">kaʒnɨj </ts>
               <ts e="T45" id="Seg_364" n="e" s="T44">dʼel </ts>
               <ts e="T46" id="Seg_366" n="e" s="T45">nagur </ts>
               <ts e="T47" id="Seg_368" n="e" s="T46">tatkudət </ts>
               <ts e="T48" id="Seg_370" n="e" s="T47">i </ts>
               <ts e="T49" id="Seg_372" n="e" s="T48">sədəjagla </ts>
               <ts e="T50" id="Seg_374" n="e" s="T49">i </ts>
               <ts e="T51" id="Seg_376" n="e" s="T50">okkɨr. </ts>
               <ts e="T52" id="Seg_378" n="e" s="T51">Sʼütʼdʼibɨn </ts>
               <ts e="T53" id="Seg_380" n="e" s="T52">ass </ts>
               <ts e="T54" id="Seg_382" n="e" s="T53">tükkus. </ts>
               <ts e="T55" id="Seg_384" n="e" s="T54">Man </ts>
               <ts e="T56" id="Seg_386" n="e" s="T55">kaʒnɨj </ts>
               <ts e="T57" id="Seg_388" n="e" s="T56">dʼel </ts>
               <ts e="T58" id="Seg_390" n="e" s="T57">tutdɨzan </ts>
               <ts e="T59" id="Seg_392" n="e" s="T58">qwɛlʼe </ts>
               <ts e="T60" id="Seg_394" n="e" s="T59">Kudəmargedot. </ts>
               <ts e="T61" id="Seg_396" n="e" s="T60">Täp </ts>
               <ts e="T62" id="Seg_398" n="e" s="T61">tʼelɨpba </ts>
               <ts e="T63" id="Seg_400" n="e" s="T62">üguzʼä. </ts>
               <ts e="T64" id="Seg_402" n="e" s="T63">A </ts>
               <ts e="T65" id="Seg_404" n="e" s="T64">na </ts>
               <ts e="T66" id="Seg_406" n="e" s="T65">jugum </ts>
               <ts e="T67" id="Seg_408" n="e" s="T66">eudə </ts>
               <ts e="T68" id="Seg_410" n="e" s="T67">ürčubat. </ts>
               <ts e="T69" id="Seg_412" n="e" s="T68">Täp </ts>
               <ts e="T70" id="Seg_414" n="e" s="T69">ešo </ts>
               <ts e="T71" id="Seg_416" n="e" s="T70">kocʼin </ts>
               <ts e="T72" id="Seg_418" n="e" s="T71">qwatkunet. </ts>
               <ts e="T73" id="Seg_420" n="e" s="T72">Man </ts>
               <ts e="T74" id="Seg_422" n="e" s="T73">arau </ts>
               <ts e="T75" id="Seg_424" n="e" s="T74">Qɨngojessä. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_425" s="T1">PVD_1964_MyHusband_nar.001 (001.001)</ta>
            <ta e="T9" id="Seg_426" s="T5">PVD_1964_MyHusband_nar.002 (001.002)</ta>
            <ta e="T15" id="Seg_427" s="T9">PVD_1964_MyHusband_nar.003 (001.003)</ta>
            <ta e="T21" id="Seg_428" s="T15">PVD_1964_MyHusband_nar.004 (001.004)</ta>
            <ta e="T24" id="Seg_429" s="T21">PVD_1964_MyHusband_nar.005 (001.005)</ta>
            <ta e="T27" id="Seg_430" s="T24">PVD_1964_MyHusband_nar.006 (001.006)</ta>
            <ta e="T30" id="Seg_431" s="T27">PVD_1964_MyHusband_nar.007 (001.007)</ta>
            <ta e="T33" id="Seg_432" s="T30">PVD_1964_MyHusband_nar.008 (001.008)</ta>
            <ta e="T35" id="Seg_433" s="T33">PVD_1964_MyHusband_nar.009 (001.009)</ta>
            <ta e="T42" id="Seg_434" s="T35">PVD_1964_MyHusband_nar.010 (001.010)</ta>
            <ta e="T51" id="Seg_435" s="T42">PVD_1964_MyHusband_nar.011 (001.011)</ta>
            <ta e="T54" id="Seg_436" s="T51">PVD_1964_MyHusband_nar.012 (001.012)</ta>
            <ta e="T60" id="Seg_437" s="T54">PVD_1964_MyHusband_nar.013 (001.013)</ta>
            <ta e="T63" id="Seg_438" s="T60">PVD_1964_MyHusband_nar.014 (001.014)</ta>
            <ta e="T68" id="Seg_439" s="T63">PVD_1964_MyHusband_nar.015 (001.015)</ta>
            <ta e="T72" id="Seg_440" s="T68">PVD_1964_MyHusband_nar.016 (001.016)</ta>
            <ta e="T75" id="Seg_441" s="T72">PVD_1964_MyHusband_nar.017 (001.017)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_442" s="T1">ман тӓ′бытды′зан мала′дон jе′зан.</ta>
            <ta e="T9" id="Seg_443" s="T5">′мекга сы′сари пот ′тʼӓкгус.</ta>
            <ta e="T15" id="Seg_444" s="T9">ман ӓ′рау у′гон ′суру(ы)мса и ′kwɛллысса.</ta>
            <ta e="T21" id="Seg_445" s="T15">ме тӓп′се ок′кыр ′пой (съ′дӓви) jезай.</ta>
            <ta e="T24" id="Seg_446" s="T21">ман ′тӓбым ′со̄рызау.</ta>
            <ta e="T27" id="Seg_447" s="T24">со̄дига kум jес.</ta>
            <ta e="T30" id="Seg_448" s="T27">тӓп асс ′тӓттъ‵быс.</ta>
            <ta e="T33" id="Seg_449" s="T30">мазым асс kwаткус.</ta>
            <ta e="T35" id="Seg_450" s="T33">тӓп ′сурымса.</ta>
            <ta e="T42" id="Seg_451" s="T35">′сурымлам ′ко̄тʼ(цʼ)ин kwат′кустъ и ′kwɛлым коцʼин kwат′кустъ.</ta>
            <ta e="T51" id="Seg_452" s="T42">тӓп кажный ′дʼел ′нагур тат′кудът и съдъjагла и ок′кыр.</ta>
            <ta e="T60" id="Seg_453" s="T54">ман ′кажный дʼел тутдызан kwɛлʼе ′кудъ′маргедот.</ta>
            <ta e="T63" id="Seg_454" s="T60">тӓп ′тʼелыпба ′ӱгузʼӓ.</ta>
            <ta e="T68" id="Seg_455" s="T63">а ′на jу′гум ′еу′дъ ′ӱртшубат.</ta>
            <ta e="T72" id="Seg_456" s="T68">тӓп е′що ′коцʼин kwат′ку не̨т.</ta>
            <ta e="T75" id="Seg_457" s="T72">ман а′рау̹ kын′го′jессӓ.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_458" s="T1">man täbɨtdɨzan maladon jezan.</ta>
            <ta e="T9" id="Seg_459" s="T5">mekga sɨsari pot tʼäkgus.</ta>
            <ta e="T15" id="Seg_460" s="T9">man ärau ugon suru(ɨ)msa i qwɛllɨssa.</ta>
            <ta e="T21" id="Seg_461" s="T15">me täpse okkɨr poj (sədäwi) jezaj.</ta>
            <ta e="T24" id="Seg_462" s="T21">man täbɨm soːrɨzau.</ta>
            <ta e="T27" id="Seg_463" s="T24">soːdiga qum jes.</ta>
            <ta e="T30" id="Seg_464" s="T27">täp ass tättəbɨs.</ta>
            <ta e="T33" id="Seg_465" s="T30">mazɨm ass qwatkus.</ta>
            <ta e="T35" id="Seg_466" s="T33">täp surɨmsa.</ta>
            <ta e="T42" id="Seg_467" s="T35">surɨmlam koːtʼ(cʼ)in qwatkustə i qwɛlɨm kocʼin qwatkustə.</ta>
            <ta e="T51" id="Seg_468" s="T42">täp kaʒnɨj dʼel nagur tatkudət i sədəjagla i okkɨr.</ta>
            <ta e="T63" id="Seg_469" s="T60">täp tʼelɨpba üguzʼä.</ta>
            <ta e="T68" id="Seg_470" s="T63">a na jugum eudə ürtšubat.</ta>
            <ta e="T72" id="Seg_471" s="T68">täp ešo kocʼin qwatku net.</ta>
            <ta e="T75" id="Seg_472" s="T72">man arau̹ qɨngojessä.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_473" s="T1">Man täbɨtdɨzan maladon jezan. </ta>
            <ta e="T9" id="Seg_474" s="T5">Mekga sɨsari pot tʼäkgus. </ta>
            <ta e="T15" id="Seg_475" s="T9">Man ärau ugon surumsa i qwɛllɨssa. </ta>
            <ta e="T21" id="Seg_476" s="T15">Me täpse okkɨr poj (sədäwi) jezaj. </ta>
            <ta e="T24" id="Seg_477" s="T21">Man täbɨm soːrɨzau. </ta>
            <ta e="T27" id="Seg_478" s="T24">Soːdiga qum jes. </ta>
            <ta e="T30" id="Seg_479" s="T27">Täp ass tättəbɨs. </ta>
            <ta e="T33" id="Seg_480" s="T30">Mazɨm ass qwatkus. </ta>
            <ta e="T35" id="Seg_481" s="T33">Täp surɨmsa. </ta>
            <ta e="T42" id="Seg_482" s="T35">Surɨmlam koːtʼin qwatkustə i qwɛlɨm kocʼin qwatkustə. </ta>
            <ta e="T51" id="Seg_483" s="T42">Täp kaʒnɨj dʼel nagur tatkudət i sədəjagla i okkɨr. </ta>
            <ta e="T54" id="Seg_484" s="T51">Sʼütʼdʼibɨn ass tükkus. </ta>
            <ta e="T60" id="Seg_485" s="T54">Man kaʒnɨj dʼel tutdɨzan qwɛlʼe Kudəmargedot. </ta>
            <ta e="T63" id="Seg_486" s="T60">Täp tʼelɨpba üguzʼä. </ta>
            <ta e="T68" id="Seg_487" s="T63">A na jugum eudə ürčubat. </ta>
            <ta e="T72" id="Seg_488" s="T68">Täp ešo kocʼin qwatkunet. </ta>
            <ta e="T75" id="Seg_489" s="T72">Man arau Qɨngojessä. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_490" s="T1">man</ta>
            <ta e="T3" id="Seg_491" s="T2">täbɨ-tdɨ-za-n</ta>
            <ta e="T4" id="Seg_492" s="T3">malado-n</ta>
            <ta e="T5" id="Seg_493" s="T4">je-za-n</ta>
            <ta e="T6" id="Seg_494" s="T5">mekga</ta>
            <ta e="T7" id="Seg_495" s="T6">sɨsari</ta>
            <ta e="T8" id="Seg_496" s="T7">po-t</ta>
            <ta e="T9" id="Seg_497" s="T8">tʼäkgu-s</ta>
            <ta e="T10" id="Seg_498" s="T9">man</ta>
            <ta e="T11" id="Seg_499" s="T10">ära-u</ta>
            <ta e="T12" id="Seg_500" s="T11">ugon</ta>
            <ta e="T13" id="Seg_501" s="T12">surum-sa</ta>
            <ta e="T14" id="Seg_502" s="T13">i</ta>
            <ta e="T15" id="Seg_503" s="T14">qwɛll-ɨ-s-sa</ta>
            <ta e="T16" id="Seg_504" s="T15">me</ta>
            <ta e="T17" id="Seg_505" s="T16">täp-se</ta>
            <ta e="T18" id="Seg_506" s="T17">okkɨr</ta>
            <ta e="T19" id="Seg_507" s="T18">po-j</ta>
            <ta e="T20" id="Seg_508" s="T19">sədäwi</ta>
            <ta e="T21" id="Seg_509" s="T20">je-za-j</ta>
            <ta e="T22" id="Seg_510" s="T21">man</ta>
            <ta e="T23" id="Seg_511" s="T22">täb-ɨ-m</ta>
            <ta e="T24" id="Seg_512" s="T23">soːrɨ-za-u</ta>
            <ta e="T25" id="Seg_513" s="T24">soːdiga</ta>
            <ta e="T26" id="Seg_514" s="T25">qum</ta>
            <ta e="T27" id="Seg_515" s="T26">je-s</ta>
            <ta e="T28" id="Seg_516" s="T27">täp</ta>
            <ta e="T29" id="Seg_517" s="T28">ass</ta>
            <ta e="T30" id="Seg_518" s="T29">tättə-bɨ-s</ta>
            <ta e="T31" id="Seg_519" s="T30">mazɨm</ta>
            <ta e="T32" id="Seg_520" s="T31">ass</ta>
            <ta e="T33" id="Seg_521" s="T32">qwat-ku-s</ta>
            <ta e="T34" id="Seg_522" s="T33">täp</ta>
            <ta e="T35" id="Seg_523" s="T34">surɨm-sa</ta>
            <ta e="T36" id="Seg_524" s="T35">surɨm-la-m</ta>
            <ta e="T37" id="Seg_525" s="T36">koːtʼi-n</ta>
            <ta e="T38" id="Seg_526" s="T37">qwat-ku-s-tə</ta>
            <ta e="T39" id="Seg_527" s="T38">i</ta>
            <ta e="T40" id="Seg_528" s="T39">qwɛl-ɨ-m</ta>
            <ta e="T41" id="Seg_529" s="T40">kocʼi-n</ta>
            <ta e="T42" id="Seg_530" s="T41">qwat-ku-s-tə</ta>
            <ta e="T43" id="Seg_531" s="T42">täp</ta>
            <ta e="T44" id="Seg_532" s="T43">kaʒnɨj</ta>
            <ta e="T45" id="Seg_533" s="T44">dʼel</ta>
            <ta e="T46" id="Seg_534" s="T45">nagur</ta>
            <ta e="T47" id="Seg_535" s="T46">tat-ku-də-t</ta>
            <ta e="T48" id="Seg_536" s="T47">i</ta>
            <ta e="T49" id="Seg_537" s="T48">sədəjag-la</ta>
            <ta e="T50" id="Seg_538" s="T49">i</ta>
            <ta e="T51" id="Seg_539" s="T50">okkɨr</ta>
            <ta e="T52" id="Seg_540" s="T51">sʼütʼdʼi-bɨ-n</ta>
            <ta e="T53" id="Seg_541" s="T52">ass</ta>
            <ta e="T54" id="Seg_542" s="T53">tü-kku-s</ta>
            <ta e="T55" id="Seg_543" s="T54">man</ta>
            <ta e="T56" id="Seg_544" s="T55">kaʒnɨj</ta>
            <ta e="T57" id="Seg_545" s="T56">dʼel</ta>
            <ta e="T58" id="Seg_546" s="T57">tutdɨ-za-n</ta>
            <ta e="T59" id="Seg_547" s="T58">qwɛlʼe</ta>
            <ta e="T60" id="Seg_548" s="T59">Kudəmargedo-t</ta>
            <ta e="T61" id="Seg_549" s="T60">täp</ta>
            <ta e="T62" id="Seg_550" s="T61">tʼelɨp-ba</ta>
            <ta e="T63" id="Seg_551" s="T62">ügu-zʼä</ta>
            <ta e="T64" id="Seg_552" s="T63">a</ta>
            <ta e="T65" id="Seg_553" s="T64">na</ta>
            <ta e="T66" id="Seg_554" s="T65">jugu-m</ta>
            <ta e="T67" id="Seg_555" s="T66">eu-də</ta>
            <ta e="T68" id="Seg_556" s="T67">ür-ču-ba-t</ta>
            <ta e="T69" id="Seg_557" s="T68">täp</ta>
            <ta e="T70" id="Seg_558" s="T69">ešo</ta>
            <ta e="T71" id="Seg_559" s="T70">kocʼi-n</ta>
            <ta e="T72" id="Seg_560" s="T71">qwat-ku-ne-t</ta>
            <ta e="T73" id="Seg_561" s="T72">man</ta>
            <ta e="T74" id="Seg_562" s="T73">ara-u</ta>
            <ta e="T75" id="Seg_563" s="T74">Qɨngo-jes-sä</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_564" s="T1">man</ta>
            <ta e="T3" id="Seg_565" s="T2">täbe-tɨ-sɨ-ŋ</ta>
            <ta e="T4" id="Seg_566" s="T3">malado-ŋ</ta>
            <ta e="T5" id="Seg_567" s="T4">eː-sɨ-ŋ</ta>
            <ta e="T6" id="Seg_568" s="T5">mekka</ta>
            <ta e="T7" id="Seg_569" s="T6">sɨsari</ta>
            <ta e="T8" id="Seg_570" s="T7">po-tə</ta>
            <ta e="T9" id="Seg_571" s="T8">tʼäkku-sɨ</ta>
            <ta e="T10" id="Seg_572" s="T9">man</ta>
            <ta e="T11" id="Seg_573" s="T10">era-w</ta>
            <ta e="T12" id="Seg_574" s="T11">ugon</ta>
            <ta e="T13" id="Seg_575" s="T12">suːrum-sɨ</ta>
            <ta e="T14" id="Seg_576" s="T13">i</ta>
            <ta e="T15" id="Seg_577" s="T14">qwɛl-ɨ-s-sɨ</ta>
            <ta e="T16" id="Seg_578" s="T15">me</ta>
            <ta e="T17" id="Seg_579" s="T16">täp-se</ta>
            <ta e="T18" id="Seg_580" s="T17">okkɨr</ta>
            <ta e="T19" id="Seg_581" s="T18">po-lʼ</ta>
            <ta e="T20" id="Seg_582" s="T19">sədäwi</ta>
            <ta e="T21" id="Seg_583" s="T20">eː-sɨ-j</ta>
            <ta e="T22" id="Seg_584" s="T21">man</ta>
            <ta e="T23" id="Seg_585" s="T22">täp-ɨ-m</ta>
            <ta e="T24" id="Seg_586" s="T23">soːrɨ-sɨ-w</ta>
            <ta e="T25" id="Seg_587" s="T24">soːdʼiga</ta>
            <ta e="T26" id="Seg_588" s="T25">qum</ta>
            <ta e="T27" id="Seg_589" s="T26">eː-sɨ</ta>
            <ta e="T28" id="Seg_590" s="T27">täp</ta>
            <ta e="T29" id="Seg_591" s="T28">asa</ta>
            <ta e="T30" id="Seg_592" s="T29">tɨtdɨ-mbɨ-sɨ</ta>
            <ta e="T31" id="Seg_593" s="T30">mazɨm</ta>
            <ta e="T32" id="Seg_594" s="T31">asa</ta>
            <ta e="T33" id="Seg_595" s="T32">qwat-ku-sɨ</ta>
            <ta e="T34" id="Seg_596" s="T33">täp</ta>
            <ta e="T35" id="Seg_597" s="T34">suːrum-sɨ</ta>
            <ta e="T36" id="Seg_598" s="T35">suːrum-la-m</ta>
            <ta e="T37" id="Seg_599" s="T36">koːci-ŋ</ta>
            <ta e="T38" id="Seg_600" s="T37">qwat-ku-sɨ-t</ta>
            <ta e="T39" id="Seg_601" s="T38">i</ta>
            <ta e="T40" id="Seg_602" s="T39">qwɛl-ɨ-m</ta>
            <ta e="T41" id="Seg_603" s="T40">koːci-ŋ</ta>
            <ta e="T42" id="Seg_604" s="T41">qwat-ku-sɨ-t</ta>
            <ta e="T43" id="Seg_605" s="T42">täp</ta>
            <ta e="T44" id="Seg_606" s="T43">kaʒnaj</ta>
            <ta e="T45" id="Seg_607" s="T44">dʼel</ta>
            <ta e="T46" id="Seg_608" s="T45">nagur</ta>
            <ta e="T47" id="Seg_609" s="T46">tat-ku-ntɨ-t</ta>
            <ta e="T48" id="Seg_610" s="T47">i</ta>
            <ta e="T49" id="Seg_611" s="T48">sədəjag-la</ta>
            <ta e="T50" id="Seg_612" s="T49">i</ta>
            <ta e="T51" id="Seg_613" s="T50">okkɨr</ta>
            <ta e="T52" id="Seg_614" s="T51">sʼütdʼe-bɨ-ŋ</ta>
            <ta e="T53" id="Seg_615" s="T52">asa</ta>
            <ta e="T54" id="Seg_616" s="T53">tüː-ku-sɨ</ta>
            <ta e="T55" id="Seg_617" s="T54">man</ta>
            <ta e="T56" id="Seg_618" s="T55">kaʒnaj</ta>
            <ta e="T57" id="Seg_619" s="T56">dʼel</ta>
            <ta e="T58" id="Seg_620" s="T57">tundɨ-sɨ-ŋ</ta>
            <ta e="T59" id="Seg_621" s="T58">qwɛl</ta>
            <ta e="T60" id="Seg_622" s="T59">Kudəmargedo-ntə</ta>
            <ta e="T61" id="Seg_623" s="T60">täp</ta>
            <ta e="T62" id="Seg_624" s="T61">tʼelɨm-mbɨ</ta>
            <ta e="T63" id="Seg_625" s="T62">ügu-se</ta>
            <ta e="T64" id="Seg_626" s="T63">a</ta>
            <ta e="T65" id="Seg_627" s="T64">na</ta>
            <ta e="T66" id="Seg_628" s="T65">ügu-m</ta>
            <ta e="T67" id="Seg_629" s="T66">awa-tə</ta>
            <ta e="T68" id="Seg_630" s="T67">ör-ču-mbɨ-t</ta>
            <ta e="T69" id="Seg_631" s="T68">täp</ta>
            <ta e="T70" id="Seg_632" s="T69">ešo</ta>
            <ta e="T71" id="Seg_633" s="T70">koːci-ŋ</ta>
            <ta e="T72" id="Seg_634" s="T71">qwat-ku-ne-t</ta>
            <ta e="T73" id="Seg_635" s="T72">man</ta>
            <ta e="T74" id="Seg_636" s="T73">era-w</ta>
            <ta e="T75" id="Seg_637" s="T74">Qɨngo-eːde-se</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_638" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_639" s="T2">man-VBLZ-PST-1SG.S</ta>
            <ta e="T4" id="Seg_640" s="T3">young-ADVZ</ta>
            <ta e="T5" id="Seg_641" s="T4">be-PST-1SG.S</ta>
            <ta e="T6" id="Seg_642" s="T5">I.ALL</ta>
            <ta e="T7" id="Seg_643" s="T6">twenty</ta>
            <ta e="T8" id="Seg_644" s="T7">year.[NOM]-3SG</ta>
            <ta e="T9" id="Seg_645" s="T8">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T10" id="Seg_646" s="T9">I.GEN</ta>
            <ta e="T11" id="Seg_647" s="T10">husband.[NOM]-1SG</ta>
            <ta e="T12" id="Seg_648" s="T11">earlier</ta>
            <ta e="T13" id="Seg_649" s="T12">hunt-PST.[3SG.S]</ta>
            <ta e="T14" id="Seg_650" s="T13">and</ta>
            <ta e="T15" id="Seg_651" s="T14">fish-EP-CAP-PST.[3SG.S]</ta>
            <ta e="T16" id="Seg_652" s="T15">we.[NOM]</ta>
            <ta e="T17" id="Seg_653" s="T16">(s)he-COM</ta>
            <ta e="T18" id="Seg_654" s="T17">one</ta>
            <ta e="T19" id="Seg_655" s="T18">year-ADJZ</ta>
            <ta e="T20" id="Seg_656" s="T19">%%</ta>
            <ta e="T21" id="Seg_657" s="T20">be-PST-1DU</ta>
            <ta e="T22" id="Seg_658" s="T21">I.NOM</ta>
            <ta e="T23" id="Seg_659" s="T22">(s)he-EP-ACC</ta>
            <ta e="T24" id="Seg_660" s="T23">love-PST-1SG.O</ta>
            <ta e="T25" id="Seg_661" s="T24">good.[3SG.S]</ta>
            <ta e="T26" id="Seg_662" s="T25">human.being.[NOM]</ta>
            <ta e="T27" id="Seg_663" s="T26">be-PST.[3SG.S]</ta>
            <ta e="T28" id="Seg_664" s="T27">(s)he.[NOM]</ta>
            <ta e="T29" id="Seg_665" s="T28">NEG</ta>
            <ta e="T30" id="Seg_666" s="T29">scuffle-DUR-PST.[3SG.S]</ta>
            <ta e="T31" id="Seg_667" s="T30">I.ACC</ta>
            <ta e="T32" id="Seg_668" s="T31">NEG</ta>
            <ta e="T33" id="Seg_669" s="T32">beat-HAB-PST.[3SG.S]</ta>
            <ta e="T34" id="Seg_670" s="T33">(s)he.[NOM]</ta>
            <ta e="T35" id="Seg_671" s="T34">hunt-PST.[3SG.S]</ta>
            <ta e="T36" id="Seg_672" s="T35">wild.animal-PL-ACC</ta>
            <ta e="T37" id="Seg_673" s="T36">much-ADVZ</ta>
            <ta e="T38" id="Seg_674" s="T37">kill-HAB-PST-3SG.O</ta>
            <ta e="T39" id="Seg_675" s="T38">and</ta>
            <ta e="T40" id="Seg_676" s="T39">fish-EP-ACC</ta>
            <ta e="T41" id="Seg_677" s="T40">much-ADVZ</ta>
            <ta e="T42" id="Seg_678" s="T41">kill-HAB-PST-3SG.O</ta>
            <ta e="T43" id="Seg_679" s="T42">(s)he.[NOM]</ta>
            <ta e="T44" id="Seg_680" s="T43">every</ta>
            <ta e="T45" id="Seg_681" s="T44">day.[NOM]</ta>
            <ta e="T46" id="Seg_682" s="T45">three</ta>
            <ta e="T47" id="Seg_683" s="T46">bring-HAB-IPFV-3SG.O</ta>
            <ta e="T48" id="Seg_684" s="T47">and</ta>
            <ta e="T49" id="Seg_685" s="T48">the.two.together-PL.[NOM]</ta>
            <ta e="T50" id="Seg_686" s="T49">and</ta>
            <ta e="T51" id="Seg_687" s="T50">one</ta>
            <ta e="T52" id="Seg_688" s="T51">become.empty-PTCP.PST-ADVZ</ta>
            <ta e="T53" id="Seg_689" s="T52">NEG</ta>
            <ta e="T54" id="Seg_690" s="T53">come-HAB-PST.[3SG.S]</ta>
            <ta e="T55" id="Seg_691" s="T54">I.NOM</ta>
            <ta e="T56" id="Seg_692" s="T55">every</ta>
            <ta e="T57" id="Seg_693" s="T56">day.[NOM]</ta>
            <ta e="T58" id="Seg_694" s="T57">transport-PST-1SG.S</ta>
            <ta e="T59" id="Seg_695" s="T58">fish.[NOM]</ta>
            <ta e="T60" id="Seg_696" s="T59">Tajzakovo-ILL</ta>
            <ta e="T61" id="Seg_697" s="T60">(s)he.[NOM]</ta>
            <ta e="T62" id="Seg_698" s="T61">be.born-PST.NAR.[3SG.S]</ta>
            <ta e="T63" id="Seg_699" s="T62">cap-COM</ta>
            <ta e="T64" id="Seg_700" s="T63">and</ta>
            <ta e="T65" id="Seg_701" s="T64">this</ta>
            <ta e="T66" id="Seg_702" s="T65">cap-ACC</ta>
            <ta e="T67" id="Seg_703" s="T66">mother.[NOM]-3SG</ta>
            <ta e="T68" id="Seg_704" s="T67">get.lost-TR-PST.NAR-3SG.O</ta>
            <ta e="T69" id="Seg_705" s="T68">(s)he.[NOM]</ta>
            <ta e="T70" id="Seg_706" s="T69">more</ta>
            <ta e="T71" id="Seg_707" s="T70">much-ADVZ</ta>
            <ta e="T72" id="Seg_708" s="T71">manage.to.get-HAB-CONJ-3SG.O</ta>
            <ta e="T73" id="Seg_709" s="T72">I.GEN</ta>
            <ta e="T74" id="Seg_710" s="T73">husband.[NOM]-1SG</ta>
            <ta e="T75" id="Seg_711" s="T74">Kostenkino-village-COM</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_712" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_713" s="T2">мужчина-VBLZ-PST-1SG.S</ta>
            <ta e="T4" id="Seg_714" s="T3">молодой-ADVZ</ta>
            <ta e="T5" id="Seg_715" s="T4">быть-PST-1SG.S</ta>
            <ta e="T6" id="Seg_716" s="T5">я.ALL</ta>
            <ta e="T7" id="Seg_717" s="T6">двадцать</ta>
            <ta e="T8" id="Seg_718" s="T7">год.[NOM]-3SG</ta>
            <ta e="T9" id="Seg_719" s="T8">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T10" id="Seg_720" s="T9">я.GEN</ta>
            <ta e="T11" id="Seg_721" s="T10">муж.[NOM]-1SG</ta>
            <ta e="T12" id="Seg_722" s="T11">раньше</ta>
            <ta e="T13" id="Seg_723" s="T12">охотиться-PST.[3SG.S]</ta>
            <ta e="T14" id="Seg_724" s="T13">и</ta>
            <ta e="T15" id="Seg_725" s="T14">рыба-EP-CAP-PST.[3SG.S]</ta>
            <ta e="T16" id="Seg_726" s="T15">мы.[NOM]</ta>
            <ta e="T17" id="Seg_727" s="T16">он(а)-COM</ta>
            <ta e="T18" id="Seg_728" s="T17">один</ta>
            <ta e="T19" id="Seg_729" s="T18">год-ADJZ</ta>
            <ta e="T20" id="Seg_730" s="T19">%%</ta>
            <ta e="T21" id="Seg_731" s="T20">быть-PST-1DU</ta>
            <ta e="T22" id="Seg_732" s="T21">я.NOM</ta>
            <ta e="T23" id="Seg_733" s="T22">он(а)-EP-ACC</ta>
            <ta e="T24" id="Seg_734" s="T23">любить-PST-1SG.O</ta>
            <ta e="T25" id="Seg_735" s="T24">хороший.[3SG.S]</ta>
            <ta e="T26" id="Seg_736" s="T25">человек.[NOM]</ta>
            <ta e="T27" id="Seg_737" s="T26">быть-PST.[3SG.S]</ta>
            <ta e="T28" id="Seg_738" s="T27">он(а).[NOM]</ta>
            <ta e="T29" id="Seg_739" s="T28">NEG</ta>
            <ta e="T30" id="Seg_740" s="T29">подраться-DUR-PST.[3SG.S]</ta>
            <ta e="T31" id="Seg_741" s="T30">я.ACC</ta>
            <ta e="T32" id="Seg_742" s="T31">NEG</ta>
            <ta e="T33" id="Seg_743" s="T32">побить-HAB-PST.[3SG.S]</ta>
            <ta e="T34" id="Seg_744" s="T33">он(а).[NOM]</ta>
            <ta e="T35" id="Seg_745" s="T34">охотиться-PST.[3SG.S]</ta>
            <ta e="T36" id="Seg_746" s="T35">зверь-PL-ACC</ta>
            <ta e="T37" id="Seg_747" s="T36">много-ADVZ</ta>
            <ta e="T38" id="Seg_748" s="T37">убить-HAB-PST-3SG.O</ta>
            <ta e="T39" id="Seg_749" s="T38">и</ta>
            <ta e="T40" id="Seg_750" s="T39">рыба-EP-ACC</ta>
            <ta e="T41" id="Seg_751" s="T40">много-ADVZ</ta>
            <ta e="T42" id="Seg_752" s="T41">убить-HAB-PST-3SG.O</ta>
            <ta e="T43" id="Seg_753" s="T42">он(а).[NOM]</ta>
            <ta e="T44" id="Seg_754" s="T43">каждый</ta>
            <ta e="T45" id="Seg_755" s="T44">день.[NOM]</ta>
            <ta e="T46" id="Seg_756" s="T45">три</ta>
            <ta e="T47" id="Seg_757" s="T46">принести-HAB-IPFV-3SG.O</ta>
            <ta e="T48" id="Seg_758" s="T47">и</ta>
            <ta e="T49" id="Seg_759" s="T48">вдвоем-PL.[NOM]</ta>
            <ta e="T50" id="Seg_760" s="T49">и</ta>
            <ta e="T51" id="Seg_761" s="T50">один</ta>
            <ta e="T52" id="Seg_762" s="T51">опустеть-PTCP.PST-ADVZ</ta>
            <ta e="T53" id="Seg_763" s="T52">NEG</ta>
            <ta e="T54" id="Seg_764" s="T53">прийти-HAB-PST.[3SG.S]</ta>
            <ta e="T55" id="Seg_765" s="T54">я.NOM</ta>
            <ta e="T56" id="Seg_766" s="T55">каждый</ta>
            <ta e="T57" id="Seg_767" s="T56">день.[NOM]</ta>
            <ta e="T58" id="Seg_768" s="T57">возить-PST-1SG.S</ta>
            <ta e="T59" id="Seg_769" s="T58">рыба.[NOM]</ta>
            <ta e="T60" id="Seg_770" s="T59">Тайзаково-ILL</ta>
            <ta e="T61" id="Seg_771" s="T60">он(а).[NOM]</ta>
            <ta e="T62" id="Seg_772" s="T61">родиться-PST.NAR.[3SG.S]</ta>
            <ta e="T63" id="Seg_773" s="T62">шапка-COM</ta>
            <ta e="T64" id="Seg_774" s="T63">а</ta>
            <ta e="T65" id="Seg_775" s="T64">этот</ta>
            <ta e="T66" id="Seg_776" s="T65">шапка-ACC</ta>
            <ta e="T67" id="Seg_777" s="T66">мать.[NOM]-3SG</ta>
            <ta e="T68" id="Seg_778" s="T67">потеряться-TR-PST.NAR-3SG.O</ta>
            <ta e="T69" id="Seg_779" s="T68">он(а).[NOM]</ta>
            <ta e="T70" id="Seg_780" s="T69">еще</ta>
            <ta e="T71" id="Seg_781" s="T70">много-ADVZ</ta>
            <ta e="T72" id="Seg_782" s="T71">добыть-HAB-CONJ-3SG.O</ta>
            <ta e="T73" id="Seg_783" s="T72">я.GEN</ta>
            <ta e="T74" id="Seg_784" s="T73">муж.[NOM]-1SG</ta>
            <ta e="T75" id="Seg_785" s="T74">Костенкино-деревня-COM</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_786" s="T1">pers</ta>
            <ta e="T3" id="Seg_787" s="T2">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_788" s="T3">adj-adj&gt;adv</ta>
            <ta e="T5" id="Seg_789" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_790" s="T5">pers</ta>
            <ta e="T7" id="Seg_791" s="T6">num</ta>
            <ta e="T8" id="Seg_792" s="T7">n.[n:case]-n:poss</ta>
            <ta e="T9" id="Seg_793" s="T8">v-v:tense.[v:pn]</ta>
            <ta e="T10" id="Seg_794" s="T9">pers</ta>
            <ta e="T11" id="Seg_795" s="T10">n.[n:case]-n:poss</ta>
            <ta e="T12" id="Seg_796" s="T11">adv</ta>
            <ta e="T13" id="Seg_797" s="T12">v-v:tense.[v:pn]</ta>
            <ta e="T14" id="Seg_798" s="T13">conj</ta>
            <ta e="T15" id="Seg_799" s="T14">n-n:ins-n&gt;v-v:tense.[v:pn]</ta>
            <ta e="T16" id="Seg_800" s="T15">pers.[n:case]</ta>
            <ta e="T17" id="Seg_801" s="T16">pers-n:case</ta>
            <ta e="T18" id="Seg_802" s="T17">num</ta>
            <ta e="T19" id="Seg_803" s="T18">n-n&gt;adj</ta>
            <ta e="T20" id="Seg_804" s="T19">%%</ta>
            <ta e="T21" id="Seg_805" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_806" s="T21">pers</ta>
            <ta e="T23" id="Seg_807" s="T22">pers-n:ins-n:case</ta>
            <ta e="T24" id="Seg_808" s="T23">v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_809" s="T24">adj.[v:pn]</ta>
            <ta e="T26" id="Seg_810" s="T25">n.[n:case]</ta>
            <ta e="T27" id="Seg_811" s="T26">v-v:tense.[v:pn]</ta>
            <ta e="T28" id="Seg_812" s="T27">pers.[n:case]</ta>
            <ta e="T29" id="Seg_813" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_814" s="T29">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T31" id="Seg_815" s="T30">pers</ta>
            <ta e="T32" id="Seg_816" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_817" s="T32">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T34" id="Seg_818" s="T33">pers.[n:case]</ta>
            <ta e="T35" id="Seg_819" s="T34">v-v:tense.[v:pn]</ta>
            <ta e="T36" id="Seg_820" s="T35">n-n:num-n:case</ta>
            <ta e="T37" id="Seg_821" s="T36">quant-quant&gt;adv</ta>
            <ta e="T38" id="Seg_822" s="T37">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_823" s="T38">conj</ta>
            <ta e="T40" id="Seg_824" s="T39">n-n:ins-n:case</ta>
            <ta e="T41" id="Seg_825" s="T40">quant-adj&gt;adv</ta>
            <ta e="T42" id="Seg_826" s="T41">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_827" s="T42">pers.[n:case]</ta>
            <ta e="T44" id="Seg_828" s="T43">adj</ta>
            <ta e="T45" id="Seg_829" s="T44">n.[n:case]</ta>
            <ta e="T46" id="Seg_830" s="T45">num</ta>
            <ta e="T47" id="Seg_831" s="T46">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T48" id="Seg_832" s="T47">conj</ta>
            <ta e="T49" id="Seg_833" s="T48">adv-n:num.[n:case]</ta>
            <ta e="T50" id="Seg_834" s="T49">conj</ta>
            <ta e="T51" id="Seg_835" s="T50">num</ta>
            <ta e="T52" id="Seg_836" s="T51">v-v&gt;ptcp-adj&gt;adv</ta>
            <ta e="T53" id="Seg_837" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_838" s="T53">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T55" id="Seg_839" s="T54">pers</ta>
            <ta e="T56" id="Seg_840" s="T55">adj</ta>
            <ta e="T57" id="Seg_841" s="T56">n.[n:case]</ta>
            <ta e="T58" id="Seg_842" s="T57">v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_843" s="T58">n.[n:case]</ta>
            <ta e="T60" id="Seg_844" s="T59">nprop-n:case</ta>
            <ta e="T61" id="Seg_845" s="T60">pers.[n:case]</ta>
            <ta e="T62" id="Seg_846" s="T61">v-v:tense.[v:pn]</ta>
            <ta e="T63" id="Seg_847" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_848" s="T63">conj</ta>
            <ta e="T65" id="Seg_849" s="T64">dem</ta>
            <ta e="T66" id="Seg_850" s="T65">n-n:case</ta>
            <ta e="T67" id="Seg_851" s="T66">n.[n:case]-n:poss</ta>
            <ta e="T68" id="Seg_852" s="T67">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_853" s="T68">pers.[n:case]</ta>
            <ta e="T70" id="Seg_854" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_855" s="T70">quant-adj&gt;adv</ta>
            <ta e="T72" id="Seg_856" s="T71">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T73" id="Seg_857" s="T72">pers</ta>
            <ta e="T74" id="Seg_858" s="T73">n.[n:case]-n:poss</ta>
            <ta e="T75" id="Seg_859" s="T74">nprop-n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_860" s="T1">pers</ta>
            <ta e="T3" id="Seg_861" s="T2">v</ta>
            <ta e="T4" id="Seg_862" s="T3">adv</ta>
            <ta e="T5" id="Seg_863" s="T4">v</ta>
            <ta e="T6" id="Seg_864" s="T5">pers</ta>
            <ta e="T7" id="Seg_865" s="T6">num</ta>
            <ta e="T8" id="Seg_866" s="T7">n</ta>
            <ta e="T9" id="Seg_867" s="T8">v</ta>
            <ta e="T10" id="Seg_868" s="T9">pers</ta>
            <ta e="T11" id="Seg_869" s="T10">n</ta>
            <ta e="T12" id="Seg_870" s="T11">adv</ta>
            <ta e="T13" id="Seg_871" s="T12">v</ta>
            <ta e="T14" id="Seg_872" s="T13">conj</ta>
            <ta e="T15" id="Seg_873" s="T14">n</ta>
            <ta e="T16" id="Seg_874" s="T15">pers</ta>
            <ta e="T17" id="Seg_875" s="T16">pers</ta>
            <ta e="T18" id="Seg_876" s="T17">num</ta>
            <ta e="T19" id="Seg_877" s="T18">adj</ta>
            <ta e="T21" id="Seg_878" s="T20">v</ta>
            <ta e="T22" id="Seg_879" s="T21">pers</ta>
            <ta e="T23" id="Seg_880" s="T22">pers</ta>
            <ta e="T24" id="Seg_881" s="T23">v</ta>
            <ta e="T25" id="Seg_882" s="T24">adj</ta>
            <ta e="T26" id="Seg_883" s="T25">n</ta>
            <ta e="T27" id="Seg_884" s="T26">v</ta>
            <ta e="T28" id="Seg_885" s="T27">pers</ta>
            <ta e="T29" id="Seg_886" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_887" s="T29">v</ta>
            <ta e="T31" id="Seg_888" s="T30">pers</ta>
            <ta e="T32" id="Seg_889" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_890" s="T32">v</ta>
            <ta e="T34" id="Seg_891" s="T33">pers</ta>
            <ta e="T35" id="Seg_892" s="T34">v</ta>
            <ta e="T36" id="Seg_893" s="T35">n</ta>
            <ta e="T37" id="Seg_894" s="T36">adv</ta>
            <ta e="T38" id="Seg_895" s="T37">v</ta>
            <ta e="T39" id="Seg_896" s="T38">conj</ta>
            <ta e="T40" id="Seg_897" s="T39">n</ta>
            <ta e="T41" id="Seg_898" s="T40">quant</ta>
            <ta e="T42" id="Seg_899" s="T41">v</ta>
            <ta e="T43" id="Seg_900" s="T42">pers</ta>
            <ta e="T44" id="Seg_901" s="T43">adj</ta>
            <ta e="T45" id="Seg_902" s="T44">n</ta>
            <ta e="T46" id="Seg_903" s="T45">num</ta>
            <ta e="T47" id="Seg_904" s="T46">v</ta>
            <ta e="T48" id="Seg_905" s="T47">conj</ta>
            <ta e="T49" id="Seg_906" s="T48">adv</ta>
            <ta e="T50" id="Seg_907" s="T49">conj</ta>
            <ta e="T51" id="Seg_908" s="T50">num</ta>
            <ta e="T52" id="Seg_909" s="T51">v</ta>
            <ta e="T53" id="Seg_910" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_911" s="T53">v</ta>
            <ta e="T55" id="Seg_912" s="T54">pers</ta>
            <ta e="T56" id="Seg_913" s="T55">adj</ta>
            <ta e="T57" id="Seg_914" s="T56">n</ta>
            <ta e="T58" id="Seg_915" s="T57">v</ta>
            <ta e="T59" id="Seg_916" s="T58">n</ta>
            <ta e="T60" id="Seg_917" s="T59">n</ta>
            <ta e="T61" id="Seg_918" s="T60">pers</ta>
            <ta e="T62" id="Seg_919" s="T61">v</ta>
            <ta e="T63" id="Seg_920" s="T62">n</ta>
            <ta e="T64" id="Seg_921" s="T63">conj</ta>
            <ta e="T65" id="Seg_922" s="T64">dem</ta>
            <ta e="T66" id="Seg_923" s="T65">n</ta>
            <ta e="T67" id="Seg_924" s="T66">n</ta>
            <ta e="T68" id="Seg_925" s="T67">v</ta>
            <ta e="T69" id="Seg_926" s="T68">pers</ta>
            <ta e="T70" id="Seg_927" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_928" s="T70">quant</ta>
            <ta e="T72" id="Seg_929" s="T71">v</ta>
            <ta e="T73" id="Seg_930" s="T72">pers</ta>
            <ta e="T74" id="Seg_931" s="T73">n</ta>
            <ta e="T75" id="Seg_932" s="T74">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_933" s="T1">pro.h:A</ta>
            <ta e="T5" id="Seg_934" s="T4">0.1.h:Th</ta>
            <ta e="T6" id="Seg_935" s="T5">pro.h:Poss</ta>
            <ta e="T8" id="Seg_936" s="T7">np:Th</ta>
            <ta e="T10" id="Seg_937" s="T9">pro.h:Poss</ta>
            <ta e="T11" id="Seg_938" s="T10">np.h:A</ta>
            <ta e="T12" id="Seg_939" s="T11">adv:Time</ta>
            <ta e="T15" id="Seg_940" s="T14">0.3.h:A</ta>
            <ta e="T16" id="Seg_941" s="T15">pro.h:Th</ta>
            <ta e="T22" id="Seg_942" s="T21">pro.h:E</ta>
            <ta e="T23" id="Seg_943" s="T22">pro.h:Th</ta>
            <ta e="T27" id="Seg_944" s="T26">0.3.h:Th</ta>
            <ta e="T28" id="Seg_945" s="T27">pro.h:A</ta>
            <ta e="T31" id="Seg_946" s="T30">pro.h:P</ta>
            <ta e="T33" id="Seg_947" s="T32">0.3.h:A</ta>
            <ta e="T34" id="Seg_948" s="T33">pro.h:A</ta>
            <ta e="T36" id="Seg_949" s="T35">np:P</ta>
            <ta e="T38" id="Seg_950" s="T37">0.3.h:A</ta>
            <ta e="T40" id="Seg_951" s="T39">np:P</ta>
            <ta e="T42" id="Seg_952" s="T41">0.3.h:A</ta>
            <ta e="T43" id="Seg_953" s="T42">pro.h:A</ta>
            <ta e="T45" id="Seg_954" s="T44">np:Time</ta>
            <ta e="T46" id="Seg_955" s="T45">np:Th</ta>
            <ta e="T54" id="Seg_956" s="T53">0.3.h:A</ta>
            <ta e="T55" id="Seg_957" s="T54">pro.h:A</ta>
            <ta e="T57" id="Seg_958" s="T56">np:Time</ta>
            <ta e="T59" id="Seg_959" s="T58">np:Th</ta>
            <ta e="T60" id="Seg_960" s="T59">np:G</ta>
            <ta e="T61" id="Seg_961" s="T60">pro.h:P</ta>
            <ta e="T63" id="Seg_962" s="T62">np:Com</ta>
            <ta e="T66" id="Seg_963" s="T65">np:Th</ta>
            <ta e="T67" id="Seg_964" s="T66">np.h:B</ta>
            <ta e="T69" id="Seg_965" s="T68">pro.h:Th</ta>
            <ta e="T73" id="Seg_966" s="T72">pro.h:Poss</ta>
            <ta e="T74" id="Seg_967" s="T73">np.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_968" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_969" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_970" s="T4">0.1.h:S v:pred</ta>
            <ta e="T8" id="Seg_971" s="T7">np:S</ta>
            <ta e="T9" id="Seg_972" s="T8">v:pred</ta>
            <ta e="T11" id="Seg_973" s="T10">np.h:S</ta>
            <ta e="T13" id="Seg_974" s="T12">v:pred</ta>
            <ta e="T15" id="Seg_975" s="T14">0.3.h:S v:pred</ta>
            <ta e="T16" id="Seg_976" s="T15">pro.h:S</ta>
            <ta e="T19" id="Seg_977" s="T18">adj:pred</ta>
            <ta e="T21" id="Seg_978" s="T20">cop</ta>
            <ta e="T22" id="Seg_979" s="T21">pro.h:S</ta>
            <ta e="T23" id="Seg_980" s="T22">pro.h:O</ta>
            <ta e="T24" id="Seg_981" s="T23">v:pred</ta>
            <ta e="T26" id="Seg_982" s="T25">n:pred</ta>
            <ta e="T27" id="Seg_983" s="T26">0.3.h:S cop</ta>
            <ta e="T28" id="Seg_984" s="T27">pro.h:S</ta>
            <ta e="T30" id="Seg_985" s="T29">v:pred</ta>
            <ta e="T31" id="Seg_986" s="T30">pro.h:O</ta>
            <ta e="T33" id="Seg_987" s="T32">0.3.h:S v:pred</ta>
            <ta e="T34" id="Seg_988" s="T33">pro.h:S</ta>
            <ta e="T35" id="Seg_989" s="T34">v:pred</ta>
            <ta e="T36" id="Seg_990" s="T35">np:O</ta>
            <ta e="T38" id="Seg_991" s="T37">0.3.h:S v:pred</ta>
            <ta e="T40" id="Seg_992" s="T39">np:O</ta>
            <ta e="T42" id="Seg_993" s="T41">0.3.h:S v:pred</ta>
            <ta e="T43" id="Seg_994" s="T42">pro.h:S</ta>
            <ta e="T46" id="Seg_995" s="T45">np:O</ta>
            <ta e="T47" id="Seg_996" s="T46">v:pred</ta>
            <ta e="T54" id="Seg_997" s="T53">0.3.h:S v:pred</ta>
            <ta e="T55" id="Seg_998" s="T54">pro.h:S</ta>
            <ta e="T58" id="Seg_999" s="T57">v:pred</ta>
            <ta e="T59" id="Seg_1000" s="T58">np:O</ta>
            <ta e="T61" id="Seg_1001" s="T60">pro.h:S</ta>
            <ta e="T62" id="Seg_1002" s="T61">v:pred</ta>
            <ta e="T66" id="Seg_1003" s="T65">np:O</ta>
            <ta e="T67" id="Seg_1004" s="T66">np.h:S</ta>
            <ta e="T68" id="Seg_1005" s="T67">v:pred</ta>
            <ta e="T69" id="Seg_1006" s="T68">pro.h:S</ta>
            <ta e="T72" id="Seg_1007" s="T71">v:pred</ta>
            <ta e="T74" id="Seg_1008" s="T73">np.h:S</ta>
            <ta e="T75" id="Seg_1009" s="T74">n:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_1010" s="T3">RUS:core</ta>
            <ta e="T14" id="Seg_1011" s="T13">RUS:gram</ta>
            <ta e="T39" id="Seg_1012" s="T38">RUS:gram</ta>
            <ta e="T44" id="Seg_1013" s="T43">RUS:core</ta>
            <ta e="T48" id="Seg_1014" s="T47">RUS:gram</ta>
            <ta e="T50" id="Seg_1015" s="T49">RUS:gram</ta>
            <ta e="T56" id="Seg_1016" s="T55">RUS:core</ta>
            <ta e="T64" id="Seg_1017" s="T63">RUS:gram</ta>
            <ta e="T70" id="Seg_1018" s="T69">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_1019" s="T1">I married when I was young.</ta>
            <ta e="T9" id="Seg_1020" s="T5">I wasn't even twenty years old.</ta>
            <ta e="T15" id="Seg_1021" s="T9">Earlier my husband used to hunt and fish.</ta>
            <ta e="T21" id="Seg_1022" s="T15">We were [born] in the same year.</ta>
            <ta e="T24" id="Seg_1023" s="T21">I loved him.</ta>
            <ta e="T27" id="Seg_1024" s="T24">He was a good man.</ta>
            <ta e="T30" id="Seg_1025" s="T27">He didn't scuffle.</ta>
            <ta e="T33" id="Seg_1026" s="T30">He didn't beat me.</ta>
            <ta e="T35" id="Seg_1027" s="T33">He went hunting.</ta>
            <ta e="T42" id="Seg_1028" s="T35">He killed many animals and he caught much fish.</ta>
            <ta e="T51" id="Seg_1029" s="T42">Every day he brought [something], three or two (of them)? and (alone?).</ta>
            <ta e="T54" id="Seg_1030" s="T51">He never came empty.</ta>
            <ta e="T60" id="Seg_1031" s="T54">Every day I took fish to Tajzakovo.</ta>
            <ta e="T63" id="Seg_1032" s="T60">He was born in a cap.</ta>
            <ta e="T68" id="Seg_1033" s="T63">And his mother lost this cap.</ta>
            <ta e="T72" id="Seg_1034" s="T68">He could have been more successful in his hunting.</ta>
            <ta e="T75" id="Seg_1035" s="T72">My husband comes from Kostenkino.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_1036" s="T1">Ich habe geheiratet, als ich jung war.</ta>
            <ta e="T9" id="Seg_1037" s="T5">Ich war nicht einmal zwanzig Jahre alt.</ta>
            <ta e="T15" id="Seg_1038" s="T9">Mein Mann ging früher regelmäßig jagen und fischen.</ta>
            <ta e="T21" id="Seg_1039" s="T15">Wir waren im selben Jahr [geboren].</ta>
            <ta e="T24" id="Seg_1040" s="T21">Ich liebte ihn.</ta>
            <ta e="T27" id="Seg_1041" s="T24">Er war ein guter Mann.</ta>
            <ta e="T30" id="Seg_1042" s="T27">Er prügelte sich nicht.</ta>
            <ta e="T33" id="Seg_1043" s="T30">Mich schlug er nicht.</ta>
            <ta e="T35" id="Seg_1044" s="T33">Er ging auf die Jagd.</ta>
            <ta e="T42" id="Seg_1045" s="T35">Er erlegte viele Tiere und er fing viele Fische.</ta>
            <ta e="T51" id="Seg_1046" s="T42">Jeden Tag brachte er [etwas], drei oder zwei (von ihnen)? und einen?</ta>
            <ta e="T54" id="Seg_1047" s="T51">Er kam nie mit leeren Händen zurück.</ta>
            <ta e="T60" id="Seg_1048" s="T54">Jeden Tag brachte ich Fisch nach Tajzakovo.</ta>
            <ta e="T63" id="Seg_1049" s="T60">Er wurde mit einer Mütze geboren.</ta>
            <ta e="T68" id="Seg_1050" s="T63">Und seine Mutter verlor diese Mütze.</ta>
            <ta e="T72" id="Seg_1051" s="T68">Er hätte besser in der Jagd sein können.</ta>
            <ta e="T75" id="Seg_1052" s="T72">Mein Mann kommt aus Kostenkino.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_1053" s="T1">Я выходила замуж, молоденькой была.</ta>
            <ta e="T9" id="Seg_1054" s="T5">Мне двадцать лет не было.</ta>
            <ta e="T15" id="Seg_1055" s="T9">Мой муж раньше охотился и рыбачил.</ta>
            <ta e="T21" id="Seg_1056" s="T15">Мы с ним одного года (?) были.</ta>
            <ta e="T24" id="Seg_1057" s="T21">Я его любила.</ta>
            <ta e="T27" id="Seg_1058" s="T24">Он хороший человек был.</ta>
            <ta e="T30" id="Seg_1059" s="T27">Он не дрался.</ta>
            <ta e="T33" id="Seg_1060" s="T30">Меня он не бил.</ta>
            <ta e="T35" id="Seg_1061" s="T33">Он охотился.</ta>
            <ta e="T42" id="Seg_1062" s="T35">Зверей много добывал и рыбы много добывал.</ta>
            <ta e="T51" id="Seg_1063" s="T42">Он каждый день (три приносил)? и (вдвоем)? и один(?).</ta>
            <ta e="T54" id="Seg_1064" s="T51">‎‎Пустой он не приходил.</ta>
            <ta e="T60" id="Seg_1065" s="T54">Я каждый день возила рыбу в Тайзаково.</ta>
            <ta e="T63" id="Seg_1066" s="T60">Он родился в шапочке.</ta>
            <ta e="T68" id="Seg_1067" s="T63">А эту шапочку мать потеряла.</ta>
            <ta e="T72" id="Seg_1068" s="T68">Он еще больше добывал бы.</ta>
            <ta e="T75" id="Seg_1069" s="T72">Мой муж костенькинский.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_1070" s="T1">я выходила замуж молоденькой была</ta>
            <ta e="T9" id="Seg_1071" s="T5">мне двадцать лет не было</ta>
            <ta e="T21" id="Seg_1072" s="T15">мы с ним одного года были</ta>
            <ta e="T24" id="Seg_1073" s="T21">я его любила</ta>
            <ta e="T27" id="Seg_1074" s="T24">хороший человек был</ta>
            <ta e="T30" id="Seg_1075" s="T27">он не дрался</ta>
            <ta e="T33" id="Seg_1076" s="T30">меня не бил</ta>
            <ta e="T35" id="Seg_1077" s="T33">он охотился</ta>
            <ta e="T42" id="Seg_1078" s="T35">зверей много добывал</ta>
            <ta e="T51" id="Seg_1079" s="T42">и рыбы много добывал</ta>
            <ta e="T54" id="Seg_1080" s="T51">пустой не приходил</ta>
            <ta e="T60" id="Seg_1081" s="T54">я каждый день возила рыбу в Тайзаково</ta>
            <ta e="T63" id="Seg_1082" s="T60">он родился в шапочке</ta>
            <ta e="T68" id="Seg_1083" s="T63">а эту шапочку мать потеряла</ta>
            <ta e="T72" id="Seg_1084" s="T68">он еще больше добывал бы</ta>
            <ta e="T75" id="Seg_1085" s="T72">мой муж Костенькинский</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T15" id="Seg_1086" s="T9">There is no Russian translation of this sentence. [KuAI:] Variant: 'surɨmsa'.</ta>
            <ta e="T42" id="Seg_1087" s="T35">[KuAI:] Variant: 'koːcʼin'.</ta>
            <ta e="T51" id="Seg_1088" s="T42">[BrM:] No original translation. [BrM:] Tentative analysis of 'tatkudət'.</ta>
            <ta e="T54" id="Seg_1089" s="T51"> ‎‎[BrM:] Tentative analysis of 'sʼütʼdʼibɨn'. || ‎‎sʼütʼd̂ʼibɨn</ta>
            <ta e="T72" id="Seg_1090" s="T68">[BrM:] 'qwatku net' changed to 'qwatkunet'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T75" id="Seg_1091" s="T72">kын′гоjет - деревня Костенькино</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
