<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_MyKnowledgeOfRussian_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_MyKnowledgeOfRussian_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">128</ud-information>
            <ud-information attribute-name="# HIAT:w">100</ud-information>
            <ud-information attribute-name="# e">100</ud-information>
            <ud-information attribute-name="# HIAT:u">20</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T101" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Marusʼa</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">awdaon</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">qalɨn</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Tebnan</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">kɨːbanʼaʒalat</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">koːcʼin</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">jezat</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">A</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">mamuun</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">agat</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">sɨtdə</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">qɨbanʼaʒa</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">iːut</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_50" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">Oqkɨr</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">tebäkü</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">qɨbanʼaʒa</ts>
                  <nts id="Seg_59" n="HIAT:ip">,</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">a</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">oqqɨr</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">näjkü</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_71" n="HIAT:w" s="T20">qɨbanʼaʒa</ts>
                  <nts id="Seg_72" n="HIAT:ip">.</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_75" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_77" n="HIAT:w" s="T21">Tep</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_80" n="HIAT:w" s="T22">pʼirʼaɣətdə</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_83" n="HIAT:w" s="T23">iːstə</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_86" n="HIAT:w" s="T24">orɨmǯelʼe</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_89" n="HIAT:w" s="T25">tadərɨt</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_93" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_95" n="HIAT:w" s="T26">A</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_98" n="HIAT:w" s="T27">teblan</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_101" n="HIAT:w" s="T28">euddɨ</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_104" n="HIAT:w" s="T29">čaːʒis</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_107" n="HIAT:w" s="T30">teblanʼe</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_110" n="HIAT:w" s="T31">mʼekga</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_113" n="HIAT:w" s="T32">udɨrɨn</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_117" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_119" n="HIAT:w" s="T33">Man</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_122" n="HIAT:w" s="T34">matqən</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_125" n="HIAT:w" s="T35">pʼelgattə</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_128" n="HIAT:w" s="T36">jezan</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_132" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_134" n="HIAT:w" s="T37">Man</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_137" n="HIAT:w" s="T38">čʼaim</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_140" n="HIAT:w" s="T39">pöčezan</ts>
                  <nts id="Seg_141" n="HIAT:ip">,</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_144" n="HIAT:w" s="T40">täbɨm</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_147" n="HIAT:w" s="T41">čʼajzʼe</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_150" n="HIAT:w" s="T42">äːrčezau</ts>
                  <nts id="Seg_151" n="HIAT:ip">.</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_154" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_156" n="HIAT:w" s="T43">Mekga</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_159" n="HIAT:w" s="T44">sɨdəkojat</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_162" n="HIAT:w" s="T45">pot</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_165" n="HIAT:w" s="T46">jes</ts>
                  <nts id="Seg_166" n="HIAT:ip">.</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_169" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_171" n="HIAT:w" s="T47">Tep</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_174" n="HIAT:w" s="T48">mʼekga</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_177" n="HIAT:w" s="T49">soɣɨdʼzʼen</ts>
                  <nts id="Seg_178" n="HIAT:ip">:</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_180" n="HIAT:ip">“</nts>
                  <ts e="T51" id="Seg_182" n="HIAT:w" s="T50">Qutder</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_185" n="HIAT:w" s="T51">warkattə</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_188" n="HIAT:w" s="T52">Denis</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_191" n="HIAT:w" s="T53">Petrowič</ts>
                  <nts id="Seg_192" n="HIAT:ip">?</nts>
                  <nts id="Seg_193" n="HIAT:ip">”</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_196" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_198" n="HIAT:w" s="T54">A</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_201" n="HIAT:w" s="T55">man</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_204" n="HIAT:w" s="T56">tʼäran</ts>
                  <nts id="Seg_205" n="HIAT:ip">:</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_207" n="HIAT:ip">“</nts>
                  <ts e="T58" id="Seg_209" n="HIAT:w" s="T57">Tebɨm</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_212" n="HIAT:w" s="T58">wes</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_215" n="HIAT:w" s="T59">tü</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_218" n="HIAT:w" s="T60">abbat</ts>
                  <nts id="Seg_219" n="HIAT:ip">.</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_222" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_224" n="HIAT:w" s="T61">Porɨɣɨmdə</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_227" n="HIAT:w" s="T62">tʼü</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_230" n="HIAT:w" s="T63">abbat</ts>
                  <nts id="Seg_231" n="HIAT:ip">.</nts>
                  <nts id="Seg_232" n="HIAT:ip">”</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_235" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_237" n="HIAT:w" s="T64">Nu</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_240" n="HIAT:w" s="T65">i</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_243" n="HIAT:w" s="T66">tep</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_246" n="HIAT:w" s="T67">patom</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_249" n="HIAT:w" s="T68">qwannɨn</ts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_253" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_255" n="HIAT:w" s="T69">Man</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_258" n="HIAT:w" s="T70">taɣdɨ</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_261" n="HIAT:w" s="T71">kazaqsʼem</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_264" n="HIAT:w" s="T72">as</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_267" n="HIAT:w" s="T73">tonuzau</ts>
                  <nts id="Seg_268" n="HIAT:ip">.</nts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_271" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_273" n="HIAT:w" s="T74">Man</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_276" n="HIAT:w" s="T75">nano</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_279" n="HIAT:w" s="T76">tʼarzan</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_282" n="HIAT:w" s="T77">što</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_285" n="HIAT:w" s="T78">tʼü</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_288" n="HIAT:w" s="T79">abbat</ts>
                  <nts id="Seg_289" n="HIAT:ip">.</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_292" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_294" n="HIAT:w" s="T80">A</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_297" n="HIAT:w" s="T81">tep</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_300" n="HIAT:w" s="T82">qɨdan</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_303" n="HIAT:w" s="T83">lagwatpɨs</ts>
                  <nts id="Seg_304" n="HIAT:ip">.</nts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_307" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_309" n="HIAT:w" s="T84">A</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_312" n="HIAT:w" s="T85">qajno</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_315" n="HIAT:w" s="T86">lagwatpugu</ts>
                  <nts id="Seg_316" n="HIAT:ip">.</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_319" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_321" n="HIAT:w" s="T87">Ras</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_324" n="HIAT:w" s="T88">mʼe</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_327" n="HIAT:w" s="T89">süsögulaftə</ts>
                  <nts id="Seg_328" n="HIAT:ip">.</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_331" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_333" n="HIAT:w" s="T90">A</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_336" n="HIAT:w" s="T91">tep</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_339" n="HIAT:w" s="T92">toʒa</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_342" n="HIAT:w" s="T93">aːs</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_345" n="HIAT:w" s="T94">tonun</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_348" n="HIAT:w" s="T95">süsögusen</ts>
                  <nts id="Seg_349" n="HIAT:ip">.</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_352" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_354" n="HIAT:w" s="T96">Taper</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_357" n="HIAT:w" s="T97">man</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_360" n="HIAT:w" s="T98">qazaqsen</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_363" n="HIAT:w" s="T99">soːn</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_366" n="HIAT:w" s="T100">qulupban</ts>
                  <nts id="Seg_367" n="HIAT:ip">.</nts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T101" id="Seg_369" n="sc" s="T1">
               <ts e="T2" id="Seg_371" n="e" s="T1">Marusʼa </ts>
               <ts e="T3" id="Seg_373" n="e" s="T2">awdaon </ts>
               <ts e="T4" id="Seg_375" n="e" s="T3">qalɨn. </ts>
               <ts e="T5" id="Seg_377" n="e" s="T4">Tebnan </ts>
               <ts e="T6" id="Seg_379" n="e" s="T5">kɨːbanʼaʒalat </ts>
               <ts e="T7" id="Seg_381" n="e" s="T6">koːcʼin </ts>
               <ts e="T8" id="Seg_383" n="e" s="T7">jezat. </ts>
               <ts e="T9" id="Seg_385" n="e" s="T8">A </ts>
               <ts e="T10" id="Seg_387" n="e" s="T9">mamuun </ts>
               <ts e="T11" id="Seg_389" n="e" s="T10">agat </ts>
               <ts e="T12" id="Seg_391" n="e" s="T11">sɨtdə </ts>
               <ts e="T13" id="Seg_393" n="e" s="T12">qɨbanʼaʒa </ts>
               <ts e="T14" id="Seg_395" n="e" s="T13">iːut. </ts>
               <ts e="T15" id="Seg_397" n="e" s="T14">Oqkɨr </ts>
               <ts e="T16" id="Seg_399" n="e" s="T15">tebäkü </ts>
               <ts e="T17" id="Seg_401" n="e" s="T16">qɨbanʼaʒa, </ts>
               <ts e="T18" id="Seg_403" n="e" s="T17">a </ts>
               <ts e="T19" id="Seg_405" n="e" s="T18">oqqɨr </ts>
               <ts e="T20" id="Seg_407" n="e" s="T19">näjkü </ts>
               <ts e="T21" id="Seg_409" n="e" s="T20">qɨbanʼaʒa. </ts>
               <ts e="T22" id="Seg_411" n="e" s="T21">Tep </ts>
               <ts e="T23" id="Seg_413" n="e" s="T22">pʼirʼaɣətdə </ts>
               <ts e="T24" id="Seg_415" n="e" s="T23">iːstə </ts>
               <ts e="T25" id="Seg_417" n="e" s="T24">orɨmǯelʼe </ts>
               <ts e="T26" id="Seg_419" n="e" s="T25">tadərɨt. </ts>
               <ts e="T27" id="Seg_421" n="e" s="T26">A </ts>
               <ts e="T28" id="Seg_423" n="e" s="T27">teblan </ts>
               <ts e="T29" id="Seg_425" n="e" s="T28">euddɨ </ts>
               <ts e="T30" id="Seg_427" n="e" s="T29">čaːʒis </ts>
               <ts e="T31" id="Seg_429" n="e" s="T30">teblanʼe </ts>
               <ts e="T32" id="Seg_431" n="e" s="T31">mʼekga </ts>
               <ts e="T33" id="Seg_433" n="e" s="T32">udɨrɨn. </ts>
               <ts e="T34" id="Seg_435" n="e" s="T33">Man </ts>
               <ts e="T35" id="Seg_437" n="e" s="T34">matqən </ts>
               <ts e="T36" id="Seg_439" n="e" s="T35">pʼelgattə </ts>
               <ts e="T37" id="Seg_441" n="e" s="T36">jezan. </ts>
               <ts e="T38" id="Seg_443" n="e" s="T37">Man </ts>
               <ts e="T39" id="Seg_445" n="e" s="T38">čʼaim </ts>
               <ts e="T40" id="Seg_447" n="e" s="T39">pöčezan, </ts>
               <ts e="T41" id="Seg_449" n="e" s="T40">täbɨm </ts>
               <ts e="T42" id="Seg_451" n="e" s="T41">čʼajzʼe </ts>
               <ts e="T43" id="Seg_453" n="e" s="T42">äːrčezau. </ts>
               <ts e="T44" id="Seg_455" n="e" s="T43">Mekga </ts>
               <ts e="T45" id="Seg_457" n="e" s="T44">sɨdəkojat </ts>
               <ts e="T46" id="Seg_459" n="e" s="T45">pot </ts>
               <ts e="T47" id="Seg_461" n="e" s="T46">jes. </ts>
               <ts e="T48" id="Seg_463" n="e" s="T47">Tep </ts>
               <ts e="T49" id="Seg_465" n="e" s="T48">mʼekga </ts>
               <ts e="T50" id="Seg_467" n="e" s="T49">soɣɨdʼzʼen: </ts>
               <ts e="T51" id="Seg_469" n="e" s="T50">“Qutder </ts>
               <ts e="T52" id="Seg_471" n="e" s="T51">warkattə </ts>
               <ts e="T53" id="Seg_473" n="e" s="T52">Denis </ts>
               <ts e="T54" id="Seg_475" n="e" s="T53">Petrowič?” </ts>
               <ts e="T55" id="Seg_477" n="e" s="T54">A </ts>
               <ts e="T56" id="Seg_479" n="e" s="T55">man </ts>
               <ts e="T57" id="Seg_481" n="e" s="T56">tʼäran: </ts>
               <ts e="T58" id="Seg_483" n="e" s="T57">“Tebɨm </ts>
               <ts e="T59" id="Seg_485" n="e" s="T58">wes </ts>
               <ts e="T60" id="Seg_487" n="e" s="T59">tü </ts>
               <ts e="T61" id="Seg_489" n="e" s="T60">abbat. </ts>
               <ts e="T62" id="Seg_491" n="e" s="T61">Porɨɣɨmdə </ts>
               <ts e="T63" id="Seg_493" n="e" s="T62">tʼü </ts>
               <ts e="T64" id="Seg_495" n="e" s="T63">abbat.” </ts>
               <ts e="T65" id="Seg_497" n="e" s="T64">Nu </ts>
               <ts e="T66" id="Seg_499" n="e" s="T65">i </ts>
               <ts e="T67" id="Seg_501" n="e" s="T66">tep </ts>
               <ts e="T68" id="Seg_503" n="e" s="T67">patom </ts>
               <ts e="T69" id="Seg_505" n="e" s="T68">qwannɨn. </ts>
               <ts e="T70" id="Seg_507" n="e" s="T69">Man </ts>
               <ts e="T71" id="Seg_509" n="e" s="T70">taɣdɨ </ts>
               <ts e="T72" id="Seg_511" n="e" s="T71">kazaqsʼem </ts>
               <ts e="T73" id="Seg_513" n="e" s="T72">as </ts>
               <ts e="T74" id="Seg_515" n="e" s="T73">tonuzau. </ts>
               <ts e="T75" id="Seg_517" n="e" s="T74">Man </ts>
               <ts e="T76" id="Seg_519" n="e" s="T75">nano </ts>
               <ts e="T77" id="Seg_521" n="e" s="T76">tʼarzan </ts>
               <ts e="T78" id="Seg_523" n="e" s="T77">što </ts>
               <ts e="T79" id="Seg_525" n="e" s="T78">tʼü </ts>
               <ts e="T80" id="Seg_527" n="e" s="T79">abbat. </ts>
               <ts e="T81" id="Seg_529" n="e" s="T80">A </ts>
               <ts e="T82" id="Seg_531" n="e" s="T81">tep </ts>
               <ts e="T83" id="Seg_533" n="e" s="T82">qɨdan </ts>
               <ts e="T84" id="Seg_535" n="e" s="T83">lagwatpɨs. </ts>
               <ts e="T85" id="Seg_537" n="e" s="T84">A </ts>
               <ts e="T86" id="Seg_539" n="e" s="T85">qajno </ts>
               <ts e="T87" id="Seg_541" n="e" s="T86">lagwatpugu. </ts>
               <ts e="T88" id="Seg_543" n="e" s="T87">Ras </ts>
               <ts e="T89" id="Seg_545" n="e" s="T88">mʼe </ts>
               <ts e="T90" id="Seg_547" n="e" s="T89">süsögulaftə. </ts>
               <ts e="T91" id="Seg_549" n="e" s="T90">A </ts>
               <ts e="T92" id="Seg_551" n="e" s="T91">tep </ts>
               <ts e="T93" id="Seg_553" n="e" s="T92">toʒa </ts>
               <ts e="T94" id="Seg_555" n="e" s="T93">aːs </ts>
               <ts e="T95" id="Seg_557" n="e" s="T94">tonun </ts>
               <ts e="T96" id="Seg_559" n="e" s="T95">süsögusen. </ts>
               <ts e="T97" id="Seg_561" n="e" s="T96">Taper </ts>
               <ts e="T98" id="Seg_563" n="e" s="T97">man </ts>
               <ts e="T99" id="Seg_565" n="e" s="T98">qazaqsen </ts>
               <ts e="T100" id="Seg_567" n="e" s="T99">soːn </ts>
               <ts e="T101" id="Seg_569" n="e" s="T100">qulupban. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_570" s="T1">PVD_1964_MyKnowledgeOfRussian_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_571" s="T4">PVD_1964_MyKnowledgeOfRussian_nar.002 (001.002)</ta>
            <ta e="T14" id="Seg_572" s="T8">PVD_1964_MyKnowledgeOfRussian_nar.003 (001.003)</ta>
            <ta e="T21" id="Seg_573" s="T14">PVD_1964_MyKnowledgeOfRussian_nar.004 (001.004)</ta>
            <ta e="T26" id="Seg_574" s="T21">PVD_1964_MyKnowledgeOfRussian_nar.005 (001.005)</ta>
            <ta e="T33" id="Seg_575" s="T26">PVD_1964_MyKnowledgeOfRussian_nar.006 (001.006)</ta>
            <ta e="T37" id="Seg_576" s="T33">PVD_1964_MyKnowledgeOfRussian_nar.007 (001.007)</ta>
            <ta e="T43" id="Seg_577" s="T37">PVD_1964_MyKnowledgeOfRussian_nar.008 (001.008)</ta>
            <ta e="T47" id="Seg_578" s="T43">PVD_1964_MyKnowledgeOfRussian_nar.009 (001.009)</ta>
            <ta e="T54" id="Seg_579" s="T47">PVD_1964_MyKnowledgeOfRussian_nar.010 (001.010)</ta>
            <ta e="T61" id="Seg_580" s="T54">PVD_1964_MyKnowledgeOfRussian_nar.011 (001.011)</ta>
            <ta e="T64" id="Seg_581" s="T61">PVD_1964_MyKnowledgeOfRussian_nar.012 (001.012)</ta>
            <ta e="T69" id="Seg_582" s="T64">PVD_1964_MyKnowledgeOfRussian_nar.013 (001.013)</ta>
            <ta e="T74" id="Seg_583" s="T69">PVD_1964_MyKnowledgeOfRussian_nar.014 (001.014)</ta>
            <ta e="T80" id="Seg_584" s="T74">PVD_1964_MyKnowledgeOfRussian_nar.015 (001.015)</ta>
            <ta e="T84" id="Seg_585" s="T80">PVD_1964_MyKnowledgeOfRussian_nar.016 (001.016)</ta>
            <ta e="T87" id="Seg_586" s="T84">PVD_1964_MyKnowledgeOfRussian_nar.017 (001.017)</ta>
            <ta e="T90" id="Seg_587" s="T87">PVD_1964_MyKnowledgeOfRussian_nar.018 (001.018)</ta>
            <ta e="T96" id="Seg_588" s="T90">PVD_1964_MyKnowledgeOfRussian_nar.019 (001.019)</ta>
            <ta e="T101" id="Seg_589" s="T96">PVD_1964_MyKnowledgeOfRussian_nar.020 (001.020)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_590" s="T1">Ма′русʼа ′аwдаон ′kалын.</ta>
            <ta e="T8" id="Seg_591" s="T4">тебнан ′кы̄банʼажалат ко̄цʼин jезат.</ta>
            <ta e="T14" id="Seg_592" s="T8">а ′маму ун а′гат сы(ъ)тдъ kы′банʼажа ′ӣут.</ta>
            <ta e="T21" id="Seg_593" s="T14">оk′кыр те′бӓ кӱ kы′банʼажа, а оk′kыр ′нӓй кӱ kы′банʼажа.</ta>
            <ta e="T26" id="Seg_594" s="T21">те̨п ′пʼирʼаɣътдъ ӣстъ ′орымдже‵лʼе ′тадърыт.</ta>
            <ta e="T33" id="Seg_595" s="T26">а теб′лан ′еу̨д′ды ′тша̄жис те̨б′ланʼе ′мʼекга ′удыры̹н.</ta>
            <ta e="T37" id="Seg_596" s="T33">ман ′матkън пʼел′гаттъ ′jезан.</ta>
            <ta e="T43" id="Seg_597" s="T37">ман ′чʼаим ′пӧтше(и)зан, тӓ′бым ′чʼайзʼе ′ӓ̄ртшезау̹.</ta>
            <ta e="T47" id="Seg_598" s="T43">′мекга ′сыдъкоjат пот jес.</ta>
            <ta e="T54" id="Seg_599" s="T47">те̨п ′мʼекга ′соɣыдʼзʼен: ′kутдер wар′каттъ Де′нис Пет′рович?</ta>
            <ta e="T61" id="Seg_600" s="T54">а ман тʼӓ′ран: те̨бым вес ′тӱ аб′бат.</ta>
            <ta e="T64" id="Seg_601" s="T61">′порыɣымдъ тʼӱ аб′бат.</ta>
            <ta e="T69" id="Seg_602" s="T64">ну и те̨п па′том ′kwаннын.</ta>
            <ta e="T74" id="Seg_603" s="T69">ман таɣ′ды казаk′сʼем ас ′тонузау̹.</ta>
            <ta e="T80" id="Seg_604" s="T74">ман на′но тʼарзан што тʼӱ ′аббат.</ta>
            <ta e="T84" id="Seg_605" s="T80">а те̨п ′kыдан лаг′wатпыс.</ta>
            <ta e="T87" id="Seg_606" s="T84">а kай′но лаг′wатпугу.</ta>
            <ta e="T90" id="Seg_607" s="T87">рас мʼе сӱ′сӧгулафтъ.</ta>
            <ta e="T96" id="Seg_608" s="T90">а те̨п ′тожа ′а̄сто′нун сӱ′сӧгусен.</ta>
            <ta e="T101" id="Seg_609" s="T96">та′пер ман kазаk′сен со̄н kулуп′б̂ан.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_610" s="T1">Мarusʼa awdaon qalɨn.</ta>
            <ta e="T8" id="Seg_611" s="T4">tebnan kɨːbanʼaʒalat koːcʼin jezat.</ta>
            <ta e="T14" id="Seg_612" s="T8">a mamu un agat sɨ(ə)tdə qɨbanʼaʒa iːut.</ta>
            <ta e="T21" id="Seg_613" s="T14">oqkɨr tebä kü qɨbanʼaʒa, a oqqɨr näj kü qɨbanʼaʒa.</ta>
            <ta e="T26" id="Seg_614" s="T21">tep pʼirʼaɣətdə iːstə orɨmǯelʼe tadərɨt.</ta>
            <ta e="T33" id="Seg_615" s="T26">a teblan euddɨ tšaːʒis teblanʼe mʼekga udɨrɨ̹n.</ta>
            <ta e="T37" id="Seg_616" s="T33">man matqən pʼelgattə jezan.</ta>
            <ta e="T43" id="Seg_617" s="T37">man čʼaim pötše(i)zan, täbɨm čʼajzʼe äːrtšezau̹.</ta>
            <ta e="T47" id="Seg_618" s="T43">mekga sɨdəkojat pot jes.</ta>
            <ta e="T54" id="Seg_619" s="T47">tep mʼekga soɣɨdʼzʼen: qutder warkattə Дenis Пetrowič?</ta>
            <ta e="T61" id="Seg_620" s="T54">a man tʼäran: tebɨm wes tü abbat.</ta>
            <ta e="T64" id="Seg_621" s="T61">porɨɣɨmdə tʼü abbat.</ta>
            <ta e="T69" id="Seg_622" s="T64">nu i tep patom qwannɨn.</ta>
            <ta e="T74" id="Seg_623" s="T69">man taɣdɨ kazaqsʼem as tonuzau̹.</ta>
            <ta e="T80" id="Seg_624" s="T74">man nano tʼarzan što tʼü abbat.</ta>
            <ta e="T84" id="Seg_625" s="T80">a tep qɨdan lagwatpɨs.</ta>
            <ta e="T87" id="Seg_626" s="T84">a qajno lagwatpugu.</ta>
            <ta e="T90" id="Seg_627" s="T87">ras mʼe süsögulaftə.</ta>
            <ta e="T96" id="Seg_628" s="T90">a tep toʒa aːstonun süsögusen.</ta>
            <ta e="T101" id="Seg_629" s="T96">taper man qazaqsen soːn qulupb̂an.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_630" s="T1">Marusʼa awdaon qalɨn. </ta>
            <ta e="T8" id="Seg_631" s="T4">Tebnan kɨːbanʼaʒalat koːcʼin jezat. </ta>
            <ta e="T14" id="Seg_632" s="T8">A mamuun agat sɨtdə qɨbanʼaʒa iːut. </ta>
            <ta e="T21" id="Seg_633" s="T14">Oqkɨr tebäkü qɨbanʼaʒa, a oqqɨr näjkü qɨbanʼaʒa. </ta>
            <ta e="T26" id="Seg_634" s="T21">Tep pʼirʼaɣətdə iːstə orɨmǯelʼe tadərɨt. </ta>
            <ta e="T33" id="Seg_635" s="T26">A teblan euddɨ čaːʒis teblanʼe mʼekga udɨrɨn. </ta>
            <ta e="T37" id="Seg_636" s="T33">Man matqən pʼelgattə jezan. </ta>
            <ta e="T43" id="Seg_637" s="T37">Man čʼaim pöčezan, täbɨm čʼajzʼe äːrčezau. </ta>
            <ta e="T47" id="Seg_638" s="T43">Mekga sɨdəkojat pot jes. </ta>
            <ta e="T54" id="Seg_639" s="T47">Tep mʼekga soɣɨdʼzʼen: “Qutder warkattə Denis Petrowič?” </ta>
            <ta e="T61" id="Seg_640" s="T54">A man tʼäran: “Tebɨm wes tü abbat. </ta>
            <ta e="T64" id="Seg_641" s="T61">Porɨɣɨmdə tʼü abbat.” </ta>
            <ta e="T69" id="Seg_642" s="T64">Nu i tep patom qwannɨn. </ta>
            <ta e="T74" id="Seg_643" s="T69">Man taɣdɨ kazaqsʼem as tonuzau. </ta>
            <ta e="T80" id="Seg_644" s="T74">Man nano tʼarzan što tʼü abbat. </ta>
            <ta e="T84" id="Seg_645" s="T80">A tep qɨdan lagwatpɨs. </ta>
            <ta e="T87" id="Seg_646" s="T84">A qajno lagwatpugu. </ta>
            <ta e="T90" id="Seg_647" s="T87">Ras mʼe süsögulaftə. </ta>
            <ta e="T96" id="Seg_648" s="T90">A tep toʒa aːs tonun süsögusen. </ta>
            <ta e="T101" id="Seg_649" s="T96">Taper man qazaqsen soːn qulupban. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_650" s="T1">Marusʼa</ta>
            <ta e="T3" id="Seg_651" s="T2">awdao-n</ta>
            <ta e="T4" id="Seg_652" s="T3">qalɨ-n</ta>
            <ta e="T5" id="Seg_653" s="T4">teb-nan</ta>
            <ta e="T6" id="Seg_654" s="T5">kɨːbanʼaʒa-la-t</ta>
            <ta e="T7" id="Seg_655" s="T6">koːcʼi-n</ta>
            <ta e="T8" id="Seg_656" s="T7">je-za-t</ta>
            <ta e="T9" id="Seg_657" s="T8">a</ta>
            <ta e="T10" id="Seg_658" s="T9">mamu-u-n</ta>
            <ta e="T11" id="Seg_659" s="T10">aga-t</ta>
            <ta e="T12" id="Seg_660" s="T11">sɨtdə</ta>
            <ta e="T13" id="Seg_661" s="T12">qɨbanʼaʒa</ta>
            <ta e="T14" id="Seg_662" s="T13">iː-u-t</ta>
            <ta e="T15" id="Seg_663" s="T14">oqkɨr</ta>
            <ta e="T16" id="Seg_664" s="T15">tebä-kü</ta>
            <ta e="T17" id="Seg_665" s="T16">qɨbanʼaʒa</ta>
            <ta e="T18" id="Seg_666" s="T17">a</ta>
            <ta e="T19" id="Seg_667" s="T18">oqqɨr</ta>
            <ta e="T20" id="Seg_668" s="T19">näj-kü</ta>
            <ta e="T21" id="Seg_669" s="T20">qɨbanʼaʒa</ta>
            <ta e="T22" id="Seg_670" s="T21">tep</ta>
            <ta e="T23" id="Seg_671" s="T22">pʼirʼa-ɣətdə</ta>
            <ta e="T24" id="Seg_672" s="T23">iː-s-tə</ta>
            <ta e="T25" id="Seg_673" s="T24">orɨm-ǯe-lʼe</ta>
            <ta e="T26" id="Seg_674" s="T25">tad-ə-r-ɨ-t</ta>
            <ta e="T27" id="Seg_675" s="T26">a</ta>
            <ta e="T28" id="Seg_676" s="T27">teb-la-n</ta>
            <ta e="T29" id="Seg_677" s="T28">eu-ddɨ</ta>
            <ta e="T30" id="Seg_678" s="T29">čaːʒi-s</ta>
            <ta e="T31" id="Seg_679" s="T30">teb-la-nʼe</ta>
            <ta e="T32" id="Seg_680" s="T31">mʼekga</ta>
            <ta e="T33" id="Seg_681" s="T32">udɨr-ɨ-n</ta>
            <ta e="T34" id="Seg_682" s="T33">man</ta>
            <ta e="T35" id="Seg_683" s="T34">mat-qən</ta>
            <ta e="T36" id="Seg_684" s="T35">pʼel-gattə</ta>
            <ta e="T37" id="Seg_685" s="T36">je-za-n</ta>
            <ta e="T38" id="Seg_686" s="T37">man</ta>
            <ta e="T39" id="Seg_687" s="T38">čʼai-m</ta>
            <ta e="T40" id="Seg_688" s="T39">pöče-za-n</ta>
            <ta e="T41" id="Seg_689" s="T40">täb-ɨ-m</ta>
            <ta e="T42" id="Seg_690" s="T41">čʼaj-zʼe</ta>
            <ta e="T43" id="Seg_691" s="T42">äːr-če-za-u</ta>
            <ta e="T44" id="Seg_692" s="T43">mekga</ta>
            <ta e="T45" id="Seg_693" s="T44">sɨdəkojat</ta>
            <ta e="T46" id="Seg_694" s="T45">po-t</ta>
            <ta e="T47" id="Seg_695" s="T46">je-s</ta>
            <ta e="T48" id="Seg_696" s="T47">tep</ta>
            <ta e="T49" id="Seg_697" s="T48">mʼekga</ta>
            <ta e="T50" id="Seg_698" s="T49">soɣɨdʼzʼe-n</ta>
            <ta e="T51" id="Seg_699" s="T50">qutder</ta>
            <ta e="T52" id="Seg_700" s="T51">warka-ttə</ta>
            <ta e="T53" id="Seg_701" s="T52">Denis</ta>
            <ta e="T54" id="Seg_702" s="T53">petrowič</ta>
            <ta e="T55" id="Seg_703" s="T54">a</ta>
            <ta e="T56" id="Seg_704" s="T55">man</ta>
            <ta e="T57" id="Seg_705" s="T56">tʼära-n</ta>
            <ta e="T58" id="Seg_706" s="T57">teb-ɨ-m</ta>
            <ta e="T59" id="Seg_707" s="T58">wes</ta>
            <ta e="T60" id="Seg_708" s="T59">tü</ta>
            <ta e="T61" id="Seg_709" s="T60">ab-ba-t</ta>
            <ta e="T62" id="Seg_710" s="T61">porɨɣɨ-m-də</ta>
            <ta e="T63" id="Seg_711" s="T62">tʼü</ta>
            <ta e="T64" id="Seg_712" s="T63">ab-ba-t</ta>
            <ta e="T65" id="Seg_713" s="T64">nu</ta>
            <ta e="T66" id="Seg_714" s="T65">i</ta>
            <ta e="T67" id="Seg_715" s="T66">tep</ta>
            <ta e="T68" id="Seg_716" s="T67">patom</ta>
            <ta e="T69" id="Seg_717" s="T68">qwan-nɨ-n</ta>
            <ta e="T70" id="Seg_718" s="T69">man</ta>
            <ta e="T71" id="Seg_719" s="T70">taɣdɨ</ta>
            <ta e="T72" id="Seg_720" s="T71">kazaq-sʼe-m</ta>
            <ta e="T73" id="Seg_721" s="T72">as</ta>
            <ta e="T74" id="Seg_722" s="T73">tonu-za-u</ta>
            <ta e="T75" id="Seg_723" s="T74">man</ta>
            <ta e="T76" id="Seg_724" s="T75">nano</ta>
            <ta e="T77" id="Seg_725" s="T76">tʼar-za-n</ta>
            <ta e="T78" id="Seg_726" s="T77">što</ta>
            <ta e="T79" id="Seg_727" s="T78">tʼü</ta>
            <ta e="T80" id="Seg_728" s="T79">ab-ba-t</ta>
            <ta e="T81" id="Seg_729" s="T80">a</ta>
            <ta e="T82" id="Seg_730" s="T81">tep</ta>
            <ta e="T83" id="Seg_731" s="T82">qɨdan</ta>
            <ta e="T84" id="Seg_732" s="T83">lagwat-pɨ-s</ta>
            <ta e="T85" id="Seg_733" s="T84">a</ta>
            <ta e="T86" id="Seg_734" s="T85">qaj-no</ta>
            <ta e="T87" id="Seg_735" s="T86">lagwat-pu-gu</ta>
            <ta e="T88" id="Seg_736" s="T87">ras</ta>
            <ta e="T89" id="Seg_737" s="T88">mʼe</ta>
            <ta e="T90" id="Seg_738" s="T89">süsögu-la-ftə</ta>
            <ta e="T91" id="Seg_739" s="T90">a</ta>
            <ta e="T92" id="Seg_740" s="T91">tep</ta>
            <ta e="T93" id="Seg_741" s="T92">toʒa</ta>
            <ta e="T94" id="Seg_742" s="T93">aːs</ta>
            <ta e="T95" id="Seg_743" s="T94">tonu-n</ta>
            <ta e="T96" id="Seg_744" s="T95">süsögu-se-n</ta>
            <ta e="T97" id="Seg_745" s="T96">taper</ta>
            <ta e="T98" id="Seg_746" s="T97">man</ta>
            <ta e="T99" id="Seg_747" s="T98">qazaq-se-n</ta>
            <ta e="T100" id="Seg_748" s="T99">soːn</ta>
            <ta e="T101" id="Seg_749" s="T100">qulupba-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_750" s="T1">Marusʼa</ta>
            <ta e="T3" id="Seg_751" s="T2">awdao-ŋ</ta>
            <ta e="T4" id="Seg_752" s="T3">qalɨ-n</ta>
            <ta e="T5" id="Seg_753" s="T4">täp-nan</ta>
            <ta e="T6" id="Seg_754" s="T5">qɨbanʼaǯa-la-tə</ta>
            <ta e="T7" id="Seg_755" s="T6">koːci-ŋ</ta>
            <ta e="T8" id="Seg_756" s="T7">eː-sɨ-tɨn</ta>
            <ta e="T9" id="Seg_757" s="T8">a</ta>
            <ta e="T10" id="Seg_758" s="T9">mamo-w-n</ta>
            <ta e="T11" id="Seg_759" s="T10">agaː-tə</ta>
            <ta e="T12" id="Seg_760" s="T11">sədə</ta>
            <ta e="T13" id="Seg_761" s="T12">qɨbanʼaǯa</ta>
            <ta e="T14" id="Seg_762" s="T13">iː-nɨ-t</ta>
            <ta e="T15" id="Seg_763" s="T14">okkɨr</ta>
            <ta e="T16" id="Seg_764" s="T15">täbe-ka</ta>
            <ta e="T17" id="Seg_765" s="T16">qɨbanʼaǯa</ta>
            <ta e="T18" id="Seg_766" s="T17">a</ta>
            <ta e="T19" id="Seg_767" s="T18">okkɨr</ta>
            <ta e="T20" id="Seg_768" s="T19">ne-ka</ta>
            <ta e="T21" id="Seg_769" s="T20">qɨbanʼaǯa</ta>
            <ta e="T22" id="Seg_770" s="T21">täp</ta>
            <ta e="T23" id="Seg_771" s="T22">pirä-qɨntɨ</ta>
            <ta e="T24" id="Seg_772" s="T23">iː-sɨ-t</ta>
            <ta e="T25" id="Seg_773" s="T24">orɨm-lǯɨ-le</ta>
            <ta e="T26" id="Seg_774" s="T25">tat-ɨ-r-ɨ-t</ta>
            <ta e="T27" id="Seg_775" s="T26">a</ta>
            <ta e="T28" id="Seg_776" s="T27">täp-la-n</ta>
            <ta e="T29" id="Seg_777" s="T28">awa-tə</ta>
            <ta e="T30" id="Seg_778" s="T29">čaǯɨ-sɨ</ta>
            <ta e="T31" id="Seg_779" s="T30">täp-la-nä</ta>
            <ta e="T32" id="Seg_780" s="T31">mekka</ta>
            <ta e="T33" id="Seg_781" s="T32">udɨr-ɨ-n</ta>
            <ta e="T34" id="Seg_782" s="T33">man</ta>
            <ta e="T35" id="Seg_783" s="T34">maːt-qɨn</ta>
            <ta e="T36" id="Seg_784" s="T35">*pel-kattə</ta>
            <ta e="T37" id="Seg_785" s="T36">eː-sɨ-ŋ</ta>
            <ta e="T38" id="Seg_786" s="T37">man</ta>
            <ta e="T39" id="Seg_787" s="T38">čʼai-m</ta>
            <ta e="T40" id="Seg_788" s="T39">pöču-sɨ-ŋ</ta>
            <ta e="T41" id="Seg_789" s="T40">täp-ɨ-m</ta>
            <ta e="T42" id="Seg_790" s="T41">čʼai-se</ta>
            <ta e="T43" id="Seg_791" s="T42">öru-ču-sɨ-w</ta>
            <ta e="T44" id="Seg_792" s="T43">mekka</ta>
            <ta e="T45" id="Seg_793" s="T44">sɨdəkojat</ta>
            <ta e="T46" id="Seg_794" s="T45">po-tə</ta>
            <ta e="T47" id="Seg_795" s="T46">eː-sɨ</ta>
            <ta e="T48" id="Seg_796" s="T47">täp</ta>
            <ta e="T49" id="Seg_797" s="T48">mekka</ta>
            <ta e="T50" id="Seg_798" s="T49">sogundʼe-n</ta>
            <ta e="T51" id="Seg_799" s="T50">qundar</ta>
            <ta e="T52" id="Seg_800" s="T51">warkɨ-tɨn</ta>
            <ta e="T53" id="Seg_801" s="T52">Denis</ta>
            <ta e="T54" id="Seg_802" s="T53">Petrowičʼ</ta>
            <ta e="T55" id="Seg_803" s="T54">a</ta>
            <ta e="T56" id="Seg_804" s="T55">man</ta>
            <ta e="T57" id="Seg_805" s="T56">tʼärɨ-ŋ</ta>
            <ta e="T58" id="Seg_806" s="T57">täp-ɨ-m</ta>
            <ta e="T59" id="Seg_807" s="T58">wesʼ</ta>
            <ta e="T60" id="Seg_808" s="T59">tü</ta>
            <ta e="T61" id="Seg_809" s="T60">am-mbɨ-t</ta>
            <ta e="T62" id="Seg_810" s="T61">porɣə-m-tə</ta>
            <ta e="T63" id="Seg_811" s="T62">tü</ta>
            <ta e="T64" id="Seg_812" s="T63">am-mbɨ-t</ta>
            <ta e="T65" id="Seg_813" s="T64">nu</ta>
            <ta e="T66" id="Seg_814" s="T65">i</ta>
            <ta e="T67" id="Seg_815" s="T66">täp</ta>
            <ta e="T68" id="Seg_816" s="T67">patom</ta>
            <ta e="T69" id="Seg_817" s="T68">qwan-nɨ-n</ta>
            <ta e="T70" id="Seg_818" s="T69">man</ta>
            <ta e="T71" id="Seg_819" s="T70">tagdɨ</ta>
            <ta e="T72" id="Seg_820" s="T71">qazaq-se-m</ta>
            <ta e="T73" id="Seg_821" s="T72">asa</ta>
            <ta e="T74" id="Seg_822" s="T73">tonu-sɨ-w</ta>
            <ta e="T75" id="Seg_823" s="T74">man</ta>
            <ta e="T76" id="Seg_824" s="T75">nanoː</ta>
            <ta e="T77" id="Seg_825" s="T76">tʼärɨ-sɨ-n</ta>
            <ta e="T78" id="Seg_826" s="T77">što</ta>
            <ta e="T79" id="Seg_827" s="T78">tü</ta>
            <ta e="T80" id="Seg_828" s="T79">am-mbɨ-t</ta>
            <ta e="T81" id="Seg_829" s="T80">a</ta>
            <ta e="T82" id="Seg_830" s="T81">täp</ta>
            <ta e="T83" id="Seg_831" s="T82">qɨdan</ta>
            <ta e="T84" id="Seg_832" s="T83">laːɣwat-mbɨ-sɨ</ta>
            <ta e="T85" id="Seg_833" s="T84">a</ta>
            <ta e="T86" id="Seg_834" s="T85">qaj-no</ta>
            <ta e="T87" id="Seg_835" s="T86">laːɣwat-mbɨ-gu</ta>
            <ta e="T88" id="Seg_836" s="T87">ras</ta>
            <ta e="T89" id="Seg_837" s="T88">me</ta>
            <ta e="T90" id="Seg_838" s="T89">süsögum-la-un</ta>
            <ta e="T91" id="Seg_839" s="T90">a</ta>
            <ta e="T92" id="Seg_840" s="T91">täp</ta>
            <ta e="T93" id="Seg_841" s="T92">toʒa</ta>
            <ta e="T94" id="Seg_842" s="T93">asa</ta>
            <ta e="T95" id="Seg_843" s="T94">tonu-n</ta>
            <ta e="T96" id="Seg_844" s="T95">süsögum-se-ŋ</ta>
            <ta e="T97" id="Seg_845" s="T96">teper</ta>
            <ta e="T98" id="Seg_846" s="T97">man</ta>
            <ta e="T99" id="Seg_847" s="T98">qazaq-se-ŋ</ta>
            <ta e="T100" id="Seg_848" s="T99">soŋ</ta>
            <ta e="T101" id="Seg_849" s="T100">kulubu-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_850" s="T1">Marusya.[NOM]</ta>
            <ta e="T3" id="Seg_851" s="T2">%widow-ADVZ</ta>
            <ta e="T4" id="Seg_852" s="T3">stay-3SG.S</ta>
            <ta e="T5" id="Seg_853" s="T4">(s)he-ADES</ta>
            <ta e="T6" id="Seg_854" s="T5">child-PL.[NOM]-3SG</ta>
            <ta e="T7" id="Seg_855" s="T6">much-ADVZ</ta>
            <ta e="T8" id="Seg_856" s="T7">be-PST-3PL</ta>
            <ta e="T9" id="Seg_857" s="T8">and</ta>
            <ta e="T10" id="Seg_858" s="T9">mum-1SG-GEN</ta>
            <ta e="T11" id="Seg_859" s="T10">brother.[NOM]-3SG</ta>
            <ta e="T12" id="Seg_860" s="T11">two</ta>
            <ta e="T13" id="Seg_861" s="T12">child.[NOM]</ta>
            <ta e="T14" id="Seg_862" s="T13">take-CO-3SG.O</ta>
            <ta e="T15" id="Seg_863" s="T14">one</ta>
            <ta e="T16" id="Seg_864" s="T15">man-DIM.[NOM]</ta>
            <ta e="T17" id="Seg_865" s="T16">child.[NOM]</ta>
            <ta e="T18" id="Seg_866" s="T17">and</ta>
            <ta e="T19" id="Seg_867" s="T18">one</ta>
            <ta e="T20" id="Seg_868" s="T19">woman-DIM.[NOM]</ta>
            <ta e="T21" id="Seg_869" s="T20">child.[NOM]</ta>
            <ta e="T22" id="Seg_870" s="T21">(s)he.[NOM]</ta>
            <ta e="T23" id="Seg_871" s="T22">self-ILL.3SG</ta>
            <ta e="T24" id="Seg_872" s="T23">take-PST-3SG.O</ta>
            <ta e="T25" id="Seg_873" s="T24">grow.up-TR-CVB</ta>
            <ta e="T26" id="Seg_874" s="T25">bring-EP-FRQ-EP-3SG.O</ta>
            <ta e="T27" id="Seg_875" s="T26">and</ta>
            <ta e="T28" id="Seg_876" s="T27">(s)he-PL-GEN</ta>
            <ta e="T29" id="Seg_877" s="T28">mother.[NOM]-3SG</ta>
            <ta e="T30" id="Seg_878" s="T29">go-PST.[NOM]</ta>
            <ta e="T31" id="Seg_879" s="T30">(s)he-PL-ALL</ta>
            <ta e="T32" id="Seg_880" s="T31">I.ALL</ta>
            <ta e="T33" id="Seg_881" s="T32">stop.in-EP-3SG.S</ta>
            <ta e="T34" id="Seg_882" s="T33">I.NOM</ta>
            <ta e="T35" id="Seg_883" s="T34">house-LOC</ta>
            <ta e="T36" id="Seg_884" s="T35">friend-CAR</ta>
            <ta e="T37" id="Seg_885" s="T36">be-PST-1SG.S</ta>
            <ta e="T38" id="Seg_886" s="T37">I.NOM</ta>
            <ta e="T39" id="Seg_887" s="T38">tea-ACC</ta>
            <ta e="T40" id="Seg_888" s="T39">heat.up-PST-1SG.S</ta>
            <ta e="T41" id="Seg_889" s="T40">(s)he-EP-ACC</ta>
            <ta e="T42" id="Seg_890" s="T41">tea-INSTR</ta>
            <ta e="T43" id="Seg_891" s="T42">drink-TR-PST-1SG.O</ta>
            <ta e="T44" id="Seg_892" s="T43">I.ALL</ta>
            <ta e="T45" id="Seg_893" s="T44">twelve</ta>
            <ta e="T46" id="Seg_894" s="T45">year.[NOM]-3SG</ta>
            <ta e="T47" id="Seg_895" s="T46">be-PST.[3SG.S]</ta>
            <ta e="T48" id="Seg_896" s="T47">(s)he.[NOM]</ta>
            <ta e="T49" id="Seg_897" s="T48">I.ALL</ta>
            <ta e="T50" id="Seg_898" s="T49">ask-3SG.S</ta>
            <ta e="T51" id="Seg_899" s="T50">how</ta>
            <ta e="T52" id="Seg_900" s="T51">live-3PL</ta>
            <ta e="T53" id="Seg_901" s="T52">Denis.[NOM]</ta>
            <ta e="T54" id="Seg_902" s="T53">Petrovich.[NOM]</ta>
            <ta e="T55" id="Seg_903" s="T54">and</ta>
            <ta e="T56" id="Seg_904" s="T55">I.NOM</ta>
            <ta e="T57" id="Seg_905" s="T56">say-1SG.S</ta>
            <ta e="T58" id="Seg_906" s="T57">(s)he-EP-ACC</ta>
            <ta e="T59" id="Seg_907" s="T58">all</ta>
            <ta e="T60" id="Seg_908" s="T59">fire.[NOM]</ta>
            <ta e="T61" id="Seg_909" s="T60">eat-PST.NAR-3SG.O</ta>
            <ta e="T62" id="Seg_910" s="T61">fur.coat-ACC-3SG</ta>
            <ta e="T63" id="Seg_911" s="T62">fire.[NOM]</ta>
            <ta e="T64" id="Seg_912" s="T63">eat-PST.NAR-3SG.O</ta>
            <ta e="T65" id="Seg_913" s="T64">now</ta>
            <ta e="T66" id="Seg_914" s="T65">and</ta>
            <ta e="T67" id="Seg_915" s="T66">(s)he.[NOM]</ta>
            <ta e="T68" id="Seg_916" s="T67">then</ta>
            <ta e="T69" id="Seg_917" s="T68">leave-CO-3SG.S</ta>
            <ta e="T70" id="Seg_918" s="T69">I.NOM</ta>
            <ta e="T71" id="Seg_919" s="T70">then</ta>
            <ta e="T72" id="Seg_920" s="T71">Russian-language-ACC</ta>
            <ta e="T73" id="Seg_921" s="T72">NEG</ta>
            <ta e="T74" id="Seg_922" s="T73">know-PST-1SG.O</ta>
            <ta e="T75" id="Seg_923" s="T74">I.NOM</ta>
            <ta e="T76" id="Seg_924" s="T75">that.is.why</ta>
            <ta e="T77" id="Seg_925" s="T76">say-PST-3SG.O</ta>
            <ta e="T78" id="Seg_926" s="T77">that</ta>
            <ta e="T79" id="Seg_927" s="T78">fire.[NOM]</ta>
            <ta e="T80" id="Seg_928" s="T79">eat-PST.NAR-3SG.O</ta>
            <ta e="T81" id="Seg_929" s="T80">and</ta>
            <ta e="T82" id="Seg_930" s="T81">(s)he.[NOM]</ta>
            <ta e="T83" id="Seg_931" s="T82">all.the.time</ta>
            <ta e="T84" id="Seg_932" s="T83">begin.to.laugh-DUR-PST.[3SG.S]</ta>
            <ta e="T85" id="Seg_933" s="T84">and</ta>
            <ta e="T86" id="Seg_934" s="T85">what-TRL</ta>
            <ta e="T87" id="Seg_935" s="T86">begin.to.laugh-DUR-INF</ta>
            <ta e="T88" id="Seg_936" s="T87">since</ta>
            <ta e="T89" id="Seg_937" s="T88">we.[NOM]</ta>
            <ta e="T90" id="Seg_938" s="T89">Selkup-PL-1PL</ta>
            <ta e="T91" id="Seg_939" s="T90">and</ta>
            <ta e="T92" id="Seg_940" s="T91">(s)he.[NOM]</ta>
            <ta e="T93" id="Seg_941" s="T92">also</ta>
            <ta e="T94" id="Seg_942" s="T93">NEG</ta>
            <ta e="T95" id="Seg_943" s="T94">can-3SG.S</ta>
            <ta e="T96" id="Seg_944" s="T95">Selkup-language-ADVZ</ta>
            <ta e="T97" id="Seg_945" s="T96">now</ta>
            <ta e="T98" id="Seg_946" s="T97">I.NOM</ta>
            <ta e="T99" id="Seg_947" s="T98">Russian-language-ADVZ</ta>
            <ta e="T100" id="Seg_948" s="T99">good</ta>
            <ta e="T101" id="Seg_949" s="T100">speak-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_950" s="T1">Маруся.[NOM]</ta>
            <ta e="T3" id="Seg_951" s="T2">%вдова-ADVZ</ta>
            <ta e="T4" id="Seg_952" s="T3">остаться-3SG.S</ta>
            <ta e="T5" id="Seg_953" s="T4">он(а)-ADES</ta>
            <ta e="T6" id="Seg_954" s="T5">ребенок-PL.[NOM]-3SG</ta>
            <ta e="T7" id="Seg_955" s="T6">много-ADVZ</ta>
            <ta e="T8" id="Seg_956" s="T7">быть-PST-3PL</ta>
            <ta e="T9" id="Seg_957" s="T8">а</ta>
            <ta e="T10" id="Seg_958" s="T9">мама-1SG-GEN</ta>
            <ta e="T11" id="Seg_959" s="T10">брат.[NOM]-3SG</ta>
            <ta e="T12" id="Seg_960" s="T11">два</ta>
            <ta e="T13" id="Seg_961" s="T12">ребенок.[NOM]</ta>
            <ta e="T14" id="Seg_962" s="T13">взять-CO-3SG.O</ta>
            <ta e="T15" id="Seg_963" s="T14">один</ta>
            <ta e="T16" id="Seg_964" s="T15">мужчина-DIM.[NOM]</ta>
            <ta e="T17" id="Seg_965" s="T16">ребенок.[NOM]</ta>
            <ta e="T18" id="Seg_966" s="T17">а</ta>
            <ta e="T19" id="Seg_967" s="T18">один</ta>
            <ta e="T20" id="Seg_968" s="T19">женщина-DIM.[NOM]</ta>
            <ta e="T21" id="Seg_969" s="T20">ребенок.[NOM]</ta>
            <ta e="T22" id="Seg_970" s="T21">он(а).[NOM]</ta>
            <ta e="T23" id="Seg_971" s="T22">себя-ILL.3SG</ta>
            <ta e="T24" id="Seg_972" s="T23">взять-PST-3SG.O</ta>
            <ta e="T25" id="Seg_973" s="T24">вырасти-TR-CVB</ta>
            <ta e="T26" id="Seg_974" s="T25">принести-EP-FRQ-EP-3SG.O</ta>
            <ta e="T27" id="Seg_975" s="T26">а</ta>
            <ta e="T28" id="Seg_976" s="T27">он(а)-PL-GEN</ta>
            <ta e="T29" id="Seg_977" s="T28">мать.[NOM]-3SG</ta>
            <ta e="T30" id="Seg_978" s="T29">ходить-PST.[NOM]</ta>
            <ta e="T31" id="Seg_979" s="T30">он(а)-PL-ALL</ta>
            <ta e="T32" id="Seg_980" s="T31">я.ALL</ta>
            <ta e="T33" id="Seg_981" s="T32">заехать-EP-3SG.S</ta>
            <ta e="T34" id="Seg_982" s="T33">я.NOM</ta>
            <ta e="T35" id="Seg_983" s="T34">дом-LOC</ta>
            <ta e="T36" id="Seg_984" s="T35">друг-CAR</ta>
            <ta e="T37" id="Seg_985" s="T36">быть-PST-1SG.S</ta>
            <ta e="T38" id="Seg_986" s="T37">я.NOM</ta>
            <ta e="T39" id="Seg_987" s="T38">чай-ACC</ta>
            <ta e="T40" id="Seg_988" s="T39">разогреть-PST-1SG.S</ta>
            <ta e="T41" id="Seg_989" s="T40">он(а)-EP-ACC</ta>
            <ta e="T42" id="Seg_990" s="T41">чай-INSTR</ta>
            <ta e="T43" id="Seg_991" s="T42">пить-TR-PST-1SG.O</ta>
            <ta e="T44" id="Seg_992" s="T43">я.ALL</ta>
            <ta e="T45" id="Seg_993" s="T44">двенадцать</ta>
            <ta e="T46" id="Seg_994" s="T45">год.[NOM]-3SG</ta>
            <ta e="T47" id="Seg_995" s="T46">быть-PST.[3SG.S]</ta>
            <ta e="T48" id="Seg_996" s="T47">он(а).[NOM]</ta>
            <ta e="T49" id="Seg_997" s="T48">я.ALL</ta>
            <ta e="T50" id="Seg_998" s="T49">спросить-3SG.S</ta>
            <ta e="T51" id="Seg_999" s="T50">как</ta>
            <ta e="T52" id="Seg_1000" s="T51">жить-3PL</ta>
            <ta e="T53" id="Seg_1001" s="T52">Денис.[NOM]</ta>
            <ta e="T54" id="Seg_1002" s="T53">Петрович.[NOM]</ta>
            <ta e="T55" id="Seg_1003" s="T54">а</ta>
            <ta e="T56" id="Seg_1004" s="T55">я.NOM</ta>
            <ta e="T57" id="Seg_1005" s="T56">сказать-1SG.S</ta>
            <ta e="T58" id="Seg_1006" s="T57">он(а)-EP-ACC</ta>
            <ta e="T59" id="Seg_1007" s="T58">все</ta>
            <ta e="T60" id="Seg_1008" s="T59">огонь.[NOM]</ta>
            <ta e="T61" id="Seg_1009" s="T60">съесть-PST.NAR-3SG.O</ta>
            <ta e="T62" id="Seg_1010" s="T61">шуба-ACC-3SG</ta>
            <ta e="T63" id="Seg_1011" s="T62">огонь.[NOM]</ta>
            <ta e="T64" id="Seg_1012" s="T63">съесть-PST.NAR-3SG.O</ta>
            <ta e="T65" id="Seg_1013" s="T64">ну</ta>
            <ta e="T66" id="Seg_1014" s="T65">и</ta>
            <ta e="T67" id="Seg_1015" s="T66">он(а).[NOM]</ta>
            <ta e="T68" id="Seg_1016" s="T67">потом</ta>
            <ta e="T69" id="Seg_1017" s="T68">отправиться-CO-3SG.S</ta>
            <ta e="T70" id="Seg_1018" s="T69">я.NOM</ta>
            <ta e="T71" id="Seg_1019" s="T70">тогда</ta>
            <ta e="T72" id="Seg_1020" s="T71">русский-язык-ACC</ta>
            <ta e="T73" id="Seg_1021" s="T72">NEG</ta>
            <ta e="T74" id="Seg_1022" s="T73">знать-PST-1SG.O</ta>
            <ta e="T75" id="Seg_1023" s="T74">я.NOM</ta>
            <ta e="T76" id="Seg_1024" s="T75">поэтому</ta>
            <ta e="T77" id="Seg_1025" s="T76">сказать-PST-3SG.O</ta>
            <ta e="T78" id="Seg_1026" s="T77">что</ta>
            <ta e="T79" id="Seg_1027" s="T78">огонь.[NOM]</ta>
            <ta e="T80" id="Seg_1028" s="T79">съесть-PST.NAR-3SG.O</ta>
            <ta e="T81" id="Seg_1029" s="T80">а</ta>
            <ta e="T82" id="Seg_1030" s="T81">он(а).[NOM]</ta>
            <ta e="T83" id="Seg_1031" s="T82">все.время</ta>
            <ta e="T84" id="Seg_1032" s="T83">засмеяться-DUR-PST.[3SG.S]</ta>
            <ta e="T85" id="Seg_1033" s="T84">а</ta>
            <ta e="T86" id="Seg_1034" s="T85">что-TRL</ta>
            <ta e="T87" id="Seg_1035" s="T86">засмеяться-DUR-INF</ta>
            <ta e="T88" id="Seg_1036" s="T87">раз</ta>
            <ta e="T89" id="Seg_1037" s="T88">мы.[NOM]</ta>
            <ta e="T90" id="Seg_1038" s="T89">селькуп-PL-1PL</ta>
            <ta e="T91" id="Seg_1039" s="T90">а</ta>
            <ta e="T92" id="Seg_1040" s="T91">он(а).[NOM]</ta>
            <ta e="T93" id="Seg_1041" s="T92">тоже</ta>
            <ta e="T94" id="Seg_1042" s="T93">NEG</ta>
            <ta e="T95" id="Seg_1043" s="T94">уметь-3SG.S</ta>
            <ta e="T96" id="Seg_1044" s="T95">селькуп-язык-ADVZ</ta>
            <ta e="T97" id="Seg_1045" s="T96">теперь</ta>
            <ta e="T98" id="Seg_1046" s="T97">я.NOM</ta>
            <ta e="T99" id="Seg_1047" s="T98">русский-язык-ADVZ</ta>
            <ta e="T100" id="Seg_1048" s="T99">хорошо</ta>
            <ta e="T101" id="Seg_1049" s="T100">говорить-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1050" s="T1">nprop.[n:case]</ta>
            <ta e="T3" id="Seg_1051" s="T2">n-n&gt;adv</ta>
            <ta e="T4" id="Seg_1052" s="T3">v-v:pn</ta>
            <ta e="T5" id="Seg_1053" s="T4">pers-n:case</ta>
            <ta e="T6" id="Seg_1054" s="T5">n-n:num.[n:case]-n:poss</ta>
            <ta e="T7" id="Seg_1055" s="T6">quant-quant&gt;adv</ta>
            <ta e="T8" id="Seg_1056" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_1057" s="T8">conj</ta>
            <ta e="T10" id="Seg_1058" s="T9">n-n:poss-n:case</ta>
            <ta e="T11" id="Seg_1059" s="T10">n.[n:case]-n:poss</ta>
            <ta e="T12" id="Seg_1060" s="T11">num</ta>
            <ta e="T13" id="Seg_1061" s="T12">n.[n:case]</ta>
            <ta e="T14" id="Seg_1062" s="T13">v-v:ins-v:pn</ta>
            <ta e="T15" id="Seg_1063" s="T14">num</ta>
            <ta e="T16" id="Seg_1064" s="T15">n-n&gt;n.[n:case]</ta>
            <ta e="T17" id="Seg_1065" s="T16">n.[n:case]</ta>
            <ta e="T18" id="Seg_1066" s="T17">conj</ta>
            <ta e="T19" id="Seg_1067" s="T18">num</ta>
            <ta e="T20" id="Seg_1068" s="T19">n-n&gt;n.[n:case]</ta>
            <ta e="T21" id="Seg_1069" s="T20">n.[n:case]</ta>
            <ta e="T22" id="Seg_1070" s="T21">pers.[n:case]</ta>
            <ta e="T23" id="Seg_1071" s="T22">pro-n:case.poss</ta>
            <ta e="T24" id="Seg_1072" s="T23">v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_1073" s="T24">v-v&gt;v-v&gt;adv</ta>
            <ta e="T26" id="Seg_1074" s="T25">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T27" id="Seg_1075" s="T26">conj</ta>
            <ta e="T28" id="Seg_1076" s="T27">pers-n:num-n:case</ta>
            <ta e="T29" id="Seg_1077" s="T28">n.[n:case]-n:poss</ta>
            <ta e="T30" id="Seg_1078" s="T29">v-v:tense.[n:case]</ta>
            <ta e="T31" id="Seg_1079" s="T30">pers-n:num-n:case</ta>
            <ta e="T32" id="Seg_1080" s="T31">pers</ta>
            <ta e="T33" id="Seg_1081" s="T32">v-v:ins-v:pn</ta>
            <ta e="T34" id="Seg_1082" s="T33">pers</ta>
            <ta e="T35" id="Seg_1083" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_1084" s="T35">n-n&gt;n</ta>
            <ta e="T37" id="Seg_1085" s="T36">v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_1086" s="T37">pers</ta>
            <ta e="T39" id="Seg_1087" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_1088" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_1089" s="T40">pers-n:ins-n:case</ta>
            <ta e="T42" id="Seg_1090" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_1091" s="T42">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_1092" s="T43">pers</ta>
            <ta e="T45" id="Seg_1093" s="T44">num</ta>
            <ta e="T46" id="Seg_1094" s="T45">n.[n:case]-n:poss</ta>
            <ta e="T47" id="Seg_1095" s="T46">v-v:tense.[v:pn]</ta>
            <ta e="T48" id="Seg_1096" s="T47">pers.[n:case]</ta>
            <ta e="T49" id="Seg_1097" s="T48">pers</ta>
            <ta e="T50" id="Seg_1098" s="T49">v-v:pn</ta>
            <ta e="T51" id="Seg_1099" s="T50">interrog</ta>
            <ta e="T52" id="Seg_1100" s="T51">v-v:pn</ta>
            <ta e="T53" id="Seg_1101" s="T52">nprop.[n:case]</ta>
            <ta e="T54" id="Seg_1102" s="T53">nprop.[n:case]</ta>
            <ta e="T55" id="Seg_1103" s="T54">conj</ta>
            <ta e="T56" id="Seg_1104" s="T55">pers</ta>
            <ta e="T57" id="Seg_1105" s="T56">v-v:pn</ta>
            <ta e="T58" id="Seg_1106" s="T57">pers-n:ins-n:case</ta>
            <ta e="T59" id="Seg_1107" s="T58">quant</ta>
            <ta e="T60" id="Seg_1108" s="T59">n.[n:case]</ta>
            <ta e="T61" id="Seg_1109" s="T60">v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_1110" s="T61">n-n:case-n:poss</ta>
            <ta e="T63" id="Seg_1111" s="T62">n.[n:case]</ta>
            <ta e="T64" id="Seg_1112" s="T63">v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_1113" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_1114" s="T65">conj</ta>
            <ta e="T67" id="Seg_1115" s="T66">pers.[n:case]</ta>
            <ta e="T68" id="Seg_1116" s="T67">adv</ta>
            <ta e="T69" id="Seg_1117" s="T68">v-v:ins-v:pn</ta>
            <ta e="T70" id="Seg_1118" s="T69">pers</ta>
            <ta e="T71" id="Seg_1119" s="T70">adv</ta>
            <ta e="T72" id="Seg_1120" s="T71">adj-n-n:case</ta>
            <ta e="T73" id="Seg_1121" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_1122" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_1123" s="T74">pers</ta>
            <ta e="T76" id="Seg_1124" s="T75">conj</ta>
            <ta e="T77" id="Seg_1125" s="T76">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_1126" s="T77">conj</ta>
            <ta e="T79" id="Seg_1127" s="T78">n.[n:case]</ta>
            <ta e="T80" id="Seg_1128" s="T79">v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_1129" s="T80">conj</ta>
            <ta e="T82" id="Seg_1130" s="T81">pers.[n:case]</ta>
            <ta e="T83" id="Seg_1131" s="T82">adv</ta>
            <ta e="T84" id="Seg_1132" s="T83">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T85" id="Seg_1133" s="T84">conj</ta>
            <ta e="T86" id="Seg_1134" s="T85">interrog-n:case</ta>
            <ta e="T87" id="Seg_1135" s="T86">v-v&gt;v-v:inf</ta>
            <ta e="T88" id="Seg_1136" s="T87">conj</ta>
            <ta e="T89" id="Seg_1137" s="T88">pers.[n:case]</ta>
            <ta e="T90" id="Seg_1138" s="T89">n-n:num-v:pn</ta>
            <ta e="T91" id="Seg_1139" s="T90">conj</ta>
            <ta e="T92" id="Seg_1140" s="T91">pers.[n:case]</ta>
            <ta e="T93" id="Seg_1141" s="T92">adv</ta>
            <ta e="T94" id="Seg_1142" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_1143" s="T94">v-v:pn</ta>
            <ta e="T96" id="Seg_1144" s="T95">n-n-n&gt;adv</ta>
            <ta e="T97" id="Seg_1145" s="T96">adv</ta>
            <ta e="T98" id="Seg_1146" s="T97">pers</ta>
            <ta e="T99" id="Seg_1147" s="T98">adj-n-n&gt;adv</ta>
            <ta e="T100" id="Seg_1148" s="T99">adv</ta>
            <ta e="T101" id="Seg_1149" s="T100">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1150" s="T1">nprop</ta>
            <ta e="T3" id="Seg_1151" s="T2">n</ta>
            <ta e="T4" id="Seg_1152" s="T3">v</ta>
            <ta e="T5" id="Seg_1153" s="T4">pers</ta>
            <ta e="T6" id="Seg_1154" s="T5">n</ta>
            <ta e="T7" id="Seg_1155" s="T6">adv</ta>
            <ta e="T8" id="Seg_1156" s="T7">v</ta>
            <ta e="T9" id="Seg_1157" s="T8">conj</ta>
            <ta e="T10" id="Seg_1158" s="T9">n</ta>
            <ta e="T11" id="Seg_1159" s="T10">n</ta>
            <ta e="T12" id="Seg_1160" s="T11">num</ta>
            <ta e="T13" id="Seg_1161" s="T12">n</ta>
            <ta e="T14" id="Seg_1162" s="T13">v</ta>
            <ta e="T15" id="Seg_1163" s="T14">num</ta>
            <ta e="T16" id="Seg_1164" s="T15">n</ta>
            <ta e="T17" id="Seg_1165" s="T16">n</ta>
            <ta e="T18" id="Seg_1166" s="T17">conj</ta>
            <ta e="T19" id="Seg_1167" s="T18">num</ta>
            <ta e="T20" id="Seg_1168" s="T19">n</ta>
            <ta e="T21" id="Seg_1169" s="T20">n</ta>
            <ta e="T22" id="Seg_1170" s="T21">pers</ta>
            <ta e="T23" id="Seg_1171" s="T22">pro</ta>
            <ta e="T24" id="Seg_1172" s="T23">v</ta>
            <ta e="T25" id="Seg_1173" s="T24">v</ta>
            <ta e="T26" id="Seg_1174" s="T25">v</ta>
            <ta e="T27" id="Seg_1175" s="T26">conj</ta>
            <ta e="T28" id="Seg_1176" s="T27">pers</ta>
            <ta e="T29" id="Seg_1177" s="T28">n</ta>
            <ta e="T30" id="Seg_1178" s="T29">v</ta>
            <ta e="T31" id="Seg_1179" s="T30">pers</ta>
            <ta e="T32" id="Seg_1180" s="T31">pers</ta>
            <ta e="T33" id="Seg_1181" s="T32">v</ta>
            <ta e="T34" id="Seg_1182" s="T33">pers</ta>
            <ta e="T35" id="Seg_1183" s="T34">n</ta>
            <ta e="T36" id="Seg_1184" s="T35">n</ta>
            <ta e="T37" id="Seg_1185" s="T36">v</ta>
            <ta e="T38" id="Seg_1186" s="T37">pers</ta>
            <ta e="T39" id="Seg_1187" s="T38">n</ta>
            <ta e="T40" id="Seg_1188" s="T39">v</ta>
            <ta e="T41" id="Seg_1189" s="T40">pers</ta>
            <ta e="T42" id="Seg_1190" s="T41">n</ta>
            <ta e="T43" id="Seg_1191" s="T42">v</ta>
            <ta e="T44" id="Seg_1192" s="T43">pers</ta>
            <ta e="T45" id="Seg_1193" s="T44">num</ta>
            <ta e="T46" id="Seg_1194" s="T45">n</ta>
            <ta e="T47" id="Seg_1195" s="T46">v</ta>
            <ta e="T48" id="Seg_1196" s="T47">pers</ta>
            <ta e="T49" id="Seg_1197" s="T48">pers</ta>
            <ta e="T50" id="Seg_1198" s="T49">v</ta>
            <ta e="T51" id="Seg_1199" s="T50">interrog</ta>
            <ta e="T52" id="Seg_1200" s="T51">v</ta>
            <ta e="T53" id="Seg_1201" s="T52">nprop</ta>
            <ta e="T54" id="Seg_1202" s="T53">nprop</ta>
            <ta e="T55" id="Seg_1203" s="T54">conj</ta>
            <ta e="T56" id="Seg_1204" s="T55">pers</ta>
            <ta e="T57" id="Seg_1205" s="T56">v</ta>
            <ta e="T58" id="Seg_1206" s="T57">pers</ta>
            <ta e="T59" id="Seg_1207" s="T58">quant</ta>
            <ta e="T60" id="Seg_1208" s="T59">n</ta>
            <ta e="T61" id="Seg_1209" s="T60">v</ta>
            <ta e="T62" id="Seg_1210" s="T61">n</ta>
            <ta e="T63" id="Seg_1211" s="T62">n</ta>
            <ta e="T64" id="Seg_1212" s="T63">v</ta>
            <ta e="T65" id="Seg_1213" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_1214" s="T65">conj</ta>
            <ta e="T67" id="Seg_1215" s="T66">pers</ta>
            <ta e="T68" id="Seg_1216" s="T67">adv</ta>
            <ta e="T69" id="Seg_1217" s="T68">v</ta>
            <ta e="T70" id="Seg_1218" s="T69">pers</ta>
            <ta e="T71" id="Seg_1219" s="T70">adv</ta>
            <ta e="T72" id="Seg_1220" s="T71">n</ta>
            <ta e="T73" id="Seg_1221" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_1222" s="T73">v</ta>
            <ta e="T75" id="Seg_1223" s="T74">pers</ta>
            <ta e="T76" id="Seg_1224" s="T75">conj</ta>
            <ta e="T77" id="Seg_1225" s="T76">v</ta>
            <ta e="T78" id="Seg_1226" s="T77">conj</ta>
            <ta e="T79" id="Seg_1227" s="T78">n</ta>
            <ta e="T80" id="Seg_1228" s="T79">v</ta>
            <ta e="T81" id="Seg_1229" s="T80">conj</ta>
            <ta e="T82" id="Seg_1230" s="T81">pers</ta>
            <ta e="T83" id="Seg_1231" s="T82">adv</ta>
            <ta e="T84" id="Seg_1232" s="T83">v</ta>
            <ta e="T85" id="Seg_1233" s="T84">conj</ta>
            <ta e="T86" id="Seg_1234" s="T85">interrog</ta>
            <ta e="T87" id="Seg_1235" s="T86">v</ta>
            <ta e="T88" id="Seg_1236" s="T87">conj</ta>
            <ta e="T89" id="Seg_1237" s="T88">pers</ta>
            <ta e="T90" id="Seg_1238" s="T89">n</ta>
            <ta e="T91" id="Seg_1239" s="T90">conj</ta>
            <ta e="T92" id="Seg_1240" s="T91">pers</ta>
            <ta e="T93" id="Seg_1241" s="T92">adv</ta>
            <ta e="T94" id="Seg_1242" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_1243" s="T94">v</ta>
            <ta e="T96" id="Seg_1244" s="T95">adv</ta>
            <ta e="T97" id="Seg_1245" s="T96">adv</ta>
            <ta e="T98" id="Seg_1246" s="T97">pers</ta>
            <ta e="T99" id="Seg_1247" s="T98">adv</ta>
            <ta e="T100" id="Seg_1248" s="T99">adv</ta>
            <ta e="T101" id="Seg_1249" s="T100">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1250" s="T1">np.h:Th</ta>
            <ta e="T5" id="Seg_1251" s="T4">pro.h:Poss</ta>
            <ta e="T6" id="Seg_1252" s="T5">np.h:Th</ta>
            <ta e="T10" id="Seg_1253" s="T9">np.h:Poss 0.1.h:Poss</ta>
            <ta e="T11" id="Seg_1254" s="T10">np.h:A</ta>
            <ta e="T13" id="Seg_1255" s="T12">np.h:Th</ta>
            <ta e="T22" id="Seg_1256" s="T21">pro.h:A</ta>
            <ta e="T23" id="Seg_1257" s="T22">pro.h:G</ta>
            <ta e="T24" id="Seg_1258" s="T23">0.3.h:Th</ta>
            <ta e="T26" id="Seg_1259" s="T25">0.3.h:A 0.3.h:Th</ta>
            <ta e="T28" id="Seg_1260" s="T27">pro.h:Poss</ta>
            <ta e="T29" id="Seg_1261" s="T28">np.h:A</ta>
            <ta e="T31" id="Seg_1262" s="T30">pro.h:G</ta>
            <ta e="T32" id="Seg_1263" s="T31">pro.h:G</ta>
            <ta e="T33" id="Seg_1264" s="T32">0.3.h:A</ta>
            <ta e="T34" id="Seg_1265" s="T33">pro.h:Th</ta>
            <ta e="T35" id="Seg_1266" s="T34">np:L</ta>
            <ta e="T38" id="Seg_1267" s="T37">pro.h:A</ta>
            <ta e="T39" id="Seg_1268" s="T38">np:P</ta>
            <ta e="T41" id="Seg_1269" s="T40">pro.h:B</ta>
            <ta e="T42" id="Seg_1270" s="T41">np:Ins</ta>
            <ta e="T43" id="Seg_1271" s="T42">0.1.h:A</ta>
            <ta e="T44" id="Seg_1272" s="T43">pro.h:Poss</ta>
            <ta e="T46" id="Seg_1273" s="T45">np:Th</ta>
            <ta e="T48" id="Seg_1274" s="T47">pro.h:A</ta>
            <ta e="T49" id="Seg_1275" s="T48">pro.h:R</ta>
            <ta e="T53" id="Seg_1276" s="T52">np.h:Th</ta>
            <ta e="T56" id="Seg_1277" s="T55">pro.h:A</ta>
            <ta e="T58" id="Seg_1278" s="T57">pro.h:P</ta>
            <ta e="T60" id="Seg_1279" s="T59">np:Cau</ta>
            <ta e="T62" id="Seg_1280" s="T61">np:Cau 0.3.h:Poss</ta>
            <ta e="T63" id="Seg_1281" s="T62">np:A</ta>
            <ta e="T67" id="Seg_1282" s="T66">pro.h:A</ta>
            <ta e="T68" id="Seg_1283" s="T67">adv:Time</ta>
            <ta e="T70" id="Seg_1284" s="T69">pro.h:E</ta>
            <ta e="T71" id="Seg_1285" s="T70">adv:Time</ta>
            <ta e="T72" id="Seg_1286" s="T71">np:Th</ta>
            <ta e="T75" id="Seg_1287" s="T74">pro.h:A</ta>
            <ta e="T79" id="Seg_1288" s="T78">np:Cau</ta>
            <ta e="T80" id="Seg_1289" s="T79">0.3:P</ta>
            <ta e="T82" id="Seg_1290" s="T81">pro.h:A</ta>
            <ta e="T89" id="Seg_1291" s="T88">pro.h:Th</ta>
            <ta e="T92" id="Seg_1292" s="T91">pro.h:E</ta>
            <ta e="T97" id="Seg_1293" s="T96">adv:Time</ta>
            <ta e="T98" id="Seg_1294" s="T97">pro.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1295" s="T1">np.h:S</ta>
            <ta e="T4" id="Seg_1296" s="T3">v:pred</ta>
            <ta e="T6" id="Seg_1297" s="T5">np.h:S</ta>
            <ta e="T8" id="Seg_1298" s="T7">v:pred</ta>
            <ta e="T11" id="Seg_1299" s="T10">np.h:S</ta>
            <ta e="T13" id="Seg_1300" s="T12">np.h:O</ta>
            <ta e="T14" id="Seg_1301" s="T13">v:pred</ta>
            <ta e="T22" id="Seg_1302" s="T21">pro.h:S</ta>
            <ta e="T24" id="Seg_1303" s="T23">v:pred 0.3.h:O</ta>
            <ta e="T25" id="Seg_1304" s="T24">s:purp</ta>
            <ta e="T26" id="Seg_1305" s="T25">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T29" id="Seg_1306" s="T28">np.h:S</ta>
            <ta e="T30" id="Seg_1307" s="T29">v:pred</ta>
            <ta e="T33" id="Seg_1308" s="T32">0.3.h:S v:pred</ta>
            <ta e="T34" id="Seg_1309" s="T33">pro.h:S</ta>
            <ta e="T36" id="Seg_1310" s="T35">n:pred</ta>
            <ta e="T37" id="Seg_1311" s="T36">cop</ta>
            <ta e="T38" id="Seg_1312" s="T37">pro.h:S</ta>
            <ta e="T39" id="Seg_1313" s="T38">np:O</ta>
            <ta e="T40" id="Seg_1314" s="T39">v:pred</ta>
            <ta e="T41" id="Seg_1315" s="T40">pro.h:O</ta>
            <ta e="T43" id="Seg_1316" s="T42">0.1.h:S v:pred</ta>
            <ta e="T46" id="Seg_1317" s="T45">np:S</ta>
            <ta e="T47" id="Seg_1318" s="T46">v:pred</ta>
            <ta e="T48" id="Seg_1319" s="T47">pro.h:S</ta>
            <ta e="T50" id="Seg_1320" s="T49">v:pred</ta>
            <ta e="T52" id="Seg_1321" s="T51">v:pred</ta>
            <ta e="T53" id="Seg_1322" s="T52">np.h:S</ta>
            <ta e="T56" id="Seg_1323" s="T55">pro.h:S</ta>
            <ta e="T57" id="Seg_1324" s="T56">v:pred</ta>
            <ta e="T58" id="Seg_1325" s="T57">pro.h:O</ta>
            <ta e="T60" id="Seg_1326" s="T59">np:S</ta>
            <ta e="T61" id="Seg_1327" s="T60">v:pred</ta>
            <ta e="T62" id="Seg_1328" s="T61">np:O</ta>
            <ta e="T63" id="Seg_1329" s="T62">np:S</ta>
            <ta e="T64" id="Seg_1330" s="T63">v:pred</ta>
            <ta e="T67" id="Seg_1331" s="T66">pro.h:S</ta>
            <ta e="T69" id="Seg_1332" s="T68">v:pred</ta>
            <ta e="T70" id="Seg_1333" s="T69">pro.h:S</ta>
            <ta e="T72" id="Seg_1334" s="T71">np:O</ta>
            <ta e="T74" id="Seg_1335" s="T73">v:pred</ta>
            <ta e="T75" id="Seg_1336" s="T74">pro.h:S</ta>
            <ta e="T77" id="Seg_1337" s="T76">v:pred</ta>
            <ta e="T80" id="Seg_1338" s="T77">s:compl</ta>
            <ta e="T82" id="Seg_1339" s="T81">pro.h:S</ta>
            <ta e="T84" id="Seg_1340" s="T83">v:pred</ta>
            <ta e="T89" id="Seg_1341" s="T88">pro.h:S</ta>
            <ta e="T90" id="Seg_1342" s="T89">n:pred</ta>
            <ta e="T92" id="Seg_1343" s="T91">pro.h:S</ta>
            <ta e="T95" id="Seg_1344" s="T94">v:pred</ta>
            <ta e="T98" id="Seg_1345" s="T97">pro.h:S</ta>
            <ta e="T101" id="Seg_1346" s="T100">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T9" id="Seg_1347" s="T8">RUS:gram</ta>
            <ta e="T10" id="Seg_1348" s="T9">RUS:core</ta>
            <ta e="T18" id="Seg_1349" s="T17">RUS:gram</ta>
            <ta e="T27" id="Seg_1350" s="T26">RUS:gram</ta>
            <ta e="T39" id="Seg_1351" s="T38">RUS:cult</ta>
            <ta e="T42" id="Seg_1352" s="T41">RUS:cult</ta>
            <ta e="T55" id="Seg_1353" s="T54">RUS:gram</ta>
            <ta e="T59" id="Seg_1354" s="T58">RUS:core</ta>
            <ta e="T65" id="Seg_1355" s="T64">RUS:disc</ta>
            <ta e="T66" id="Seg_1356" s="T65">RUS:gram</ta>
            <ta e="T68" id="Seg_1357" s="T67">RUS:disc</ta>
            <ta e="T71" id="Seg_1358" s="T70">RUS:disc</ta>
            <ta e="T78" id="Seg_1359" s="T77">RUS:gram</ta>
            <ta e="T81" id="Seg_1360" s="T80">RUS:gram</ta>
            <ta e="T85" id="Seg_1361" s="T84">RUS:gram</ta>
            <ta e="T88" id="Seg_1362" s="T87">RUS:gram</ta>
            <ta e="T91" id="Seg_1363" s="T90">RUS:gram</ta>
            <ta e="T93" id="Seg_1364" s="T92">RUS:core</ta>
            <ta e="T97" id="Seg_1365" s="T96">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_1366" s="T1">Marusya became a widow.</ta>
            <ta e="T8" id="Seg_1367" s="T4">She had many children.</ta>
            <ta e="T14" id="Seg_1368" s="T8">And my mother's brother took two children to him.</ta>
            <ta e="T21" id="Seg_1369" s="T14">One boy and one girl.</ta>
            <ta e="T26" id="Seg_1370" s="T21">He took them to his house to supervise them.</ta>
            <ta e="T33" id="Seg_1371" s="T26">And their mother came to (them?) me.</ta>
            <ta e="T37" id="Seg_1372" s="T33">I was home alone.</ta>
            <ta e="T43" id="Seg_1373" s="T37">I made tea, I gave her tea.</ta>
            <ta e="T47" id="Seg_1374" s="T43">I was twelve years old.</ta>
            <ta e="T54" id="Seg_1375" s="T47">She began asking me: “How is Denis Petrovitch?”</ta>
            <ta e="T61" id="Seg_1376" s="T54">And I said: “He was eaten by fire.</ta>
            <ta e="T64" id="Seg_1377" s="T61">His fur clothing was eaten by fire.”</ta>
            <ta e="T69" id="Seg_1378" s="T64">And then she left.</ta>
            <ta e="T74" id="Seg_1379" s="T69">I couldn't speak Russian at that time.</ta>
            <ta e="T80" id="Seg_1380" s="T74">That's why I said, that he had been eaten by fire.</ta>
            <ta e="T84" id="Seg_1381" s="T80">And she was laughing.</ta>
            <ta e="T87" id="Seg_1382" s="T84">And why should one laugh.</ta>
            <ta e="T90" id="Seg_1383" s="T87">Since we are Selkup.</ta>
            <ta e="T96" id="Seg_1384" s="T90">She also couldn't speak Selkup.</ta>
            <ta e="T101" id="Seg_1385" s="T96">Now I speak Russian well.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_1386" s="T1">Marusya wurde Witwe.</ta>
            <ta e="T8" id="Seg_1387" s="T4">Sie hatte viele Kinder.</ta>
            <ta e="T14" id="Seg_1388" s="T8">Und der Bruder meiner Mutter nahm zwei Kinder (bei sich auf).</ta>
            <ta e="T21" id="Seg_1389" s="T14">Ein Junge und ein Mädchen.</ta>
            <ta e="T26" id="Seg_1390" s="T21">Er nahm sie in sein Haus um sie großzuziehen.</ta>
            <ta e="T33" id="Seg_1391" s="T26">Und ihre Mutter kam zu (ihnen?) mir.</ta>
            <ta e="T37" id="Seg_1392" s="T33">Ich war allein im Haus.</ta>
            <ta e="T43" id="Seg_1393" s="T37">Ich kochte Tee, ich gab ihr Tee.</ta>
            <ta e="T47" id="Seg_1394" s="T43">Ich war zwölf Jahre alt.</ta>
            <ta e="T54" id="Seg_1395" s="T47">Sie begann mich zu fragen: "Wie geht es Denis Petrovitch?"</ta>
            <ta e="T61" id="Seg_1396" s="T54">Und ich sagte: "Er wurde vom Feuer gegessen.</ta>
            <ta e="T64" id="Seg_1397" s="T61">Seine Pelzkleidung wurde vom Feuer gegessen."</ta>
            <ta e="T69" id="Seg_1398" s="T64">Und dann ging sie.</ta>
            <ta e="T74" id="Seg_1399" s="T69">Ich konnte damals noch kein Russisch.</ta>
            <ta e="T80" id="Seg_1400" s="T74">Deswegen habe ich gesagt, er wurde vom Feuer gegessen.</ta>
            <ta e="T84" id="Seg_1401" s="T80">Und sie lachte.</ta>
            <ta e="T87" id="Seg_1402" s="T84">Und warum sollte man lachen.</ta>
            <ta e="T90" id="Seg_1403" s="T87">Denn wir sind Selkupen.</ta>
            <ta e="T96" id="Seg_1404" s="T90">Und sie konnte auch kein Selkupisch.</ta>
            <ta e="T101" id="Seg_1405" s="T96">Jetzt spreche ich Russisch gut.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_1406" s="T1">Маруся овдовела.</ta>
            <ta e="T8" id="Seg_1407" s="T4">У нее ребятишек много было.</ta>
            <ta e="T14" id="Seg_1408" s="T8">А мамин брат двоих ребятишек взял.</ta>
            <ta e="T21" id="Seg_1409" s="T14">Одного парнишку и одну девочку.</ta>
            <ta e="T26" id="Seg_1410" s="T21">Он к себе их взял растить.</ta>
            <ta e="T33" id="Seg_1411" s="T26">А их мать пошла, к ним(?) ко мне заехала.</ta>
            <ta e="T37" id="Seg_1412" s="T33">Я дома одна была.</ta>
            <ta e="T43" id="Seg_1413" s="T37">Я чай согрела, ее чаем напоила.</ta>
            <ta e="T47" id="Seg_1414" s="T43">Мне двенадцать лет было.</ta>
            <ta e="T54" id="Seg_1415" s="T47">Она у меня стала спрашивать: “Как живут Денис Петрович?”</ta>
            <ta e="T61" id="Seg_1416" s="T54">А я сказала: “Его огонь всего съел.</ta>
            <ta e="T64" id="Seg_1417" s="T61">Шубу его огонь съел”.</ta>
            <ta e="T69" id="Seg_1418" s="T64">Ну и она потом уехала.</ta>
            <ta e="T74" id="Seg_1419" s="T69">Я тогда по-русски не умела говорить.</ta>
            <ta e="T80" id="Seg_1420" s="T74">Я поэтому и сказала, что огонь съел.</ta>
            <ta e="T84" id="Seg_1421" s="T80">А она все смеялась.</ta>
            <ta e="T87" id="Seg_1422" s="T84">А зачем смеяться.</ta>
            <ta e="T90" id="Seg_1423" s="T87">Раз мы селькупы.</ta>
            <ta e="T96" id="Seg_1424" s="T90">А она тоже по-селькупски не умеет (говорить).</ta>
            <ta e="T101" id="Seg_1425" s="T96">Теперь я хорошо по-русски говорю.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_1426" s="T1">Маруся овдовела (вдовой осталась)</ta>
            <ta e="T8" id="Seg_1427" s="T4">у нее ребятишек много</ta>
            <ta e="T14" id="Seg_1428" s="T8">мамин брат двоих ребятишек взял</ta>
            <ta e="T21" id="Seg_1429" s="T14">одного парнишку другая девчонка</ta>
            <ta e="T26" id="Seg_1430" s="T21">он себе взял растить</ta>
            <ta e="T33" id="Seg_1431" s="T26">а ихняя мать уехала ко мне заехала</ta>
            <ta e="T37" id="Seg_1432" s="T33">я дома одна была</ta>
            <ta e="T43" id="Seg_1433" s="T37">я чай согрела ее чаем напоила</ta>
            <ta e="T47" id="Seg_1434" s="T43">мне двенадцать лет было</ta>
            <ta e="T54" id="Seg_1435" s="T47">она у меня стала спрашивать как живут Денис Петрович</ta>
            <ta e="T61" id="Seg_1436" s="T54">а я сказала его огонь все съел</ta>
            <ta e="T64" id="Seg_1437" s="T61">шубу огонь съел</ta>
            <ta e="T69" id="Seg_1438" s="T64">ну она потом уехала</ta>
            <ta e="T74" id="Seg_1439" s="T69">я тогда по-русски не умела говорить</ta>
            <ta e="T80" id="Seg_1440" s="T74">я поэтому и сказала что огонь съел</ta>
            <ta e="T84" id="Seg_1441" s="T80">а она потом все смеялась</ta>
            <ta e="T87" id="Seg_1442" s="T84">а зачем смеяться</ta>
            <ta e="T90" id="Seg_1443" s="T87">раз мы остяки</ta>
            <ta e="T96" id="Seg_1444" s="T90">а она тоже по-остяцки не знает</ta>
            <ta e="T101" id="Seg_1445" s="T96">а я теперь хорошо по-русски говорю</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T14" id="Seg_1446" s="T8">[BrM:] 'mamu un' changed to 'mamuun'. [KuAI:] Variant: 'sətdə'.</ta>
            <ta e="T43" id="Seg_1447" s="T37">[KuAI:] Variant: 'pöčizan'.</ta>
            <ta e="T96" id="Seg_1448" s="T90">[BrM:] 'aːstonun' changed to 'aːs tonun'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
