<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_MyShoes_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_MyShoes_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">45</ud-information>
            <ud-information attribute-name="# HIAT:w">36</ud-information>
            <ud-information attribute-name="# e">36</ud-information>
            <ud-information attribute-name="# HIAT:u">7</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T37" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Ugon</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">man</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">kɨːdɨn</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">pöilam</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">warɨkuzan</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_20" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">Bolʼše</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">qajnä</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">tʼäkgus</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_32" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">A</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">täpär</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">wsʼäkij</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">pöjla</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">jetat</ts>
                  <nts id="Seg_47" n="HIAT:ip">,</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">i</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">batʼinkala</ts>
                  <nts id="Seg_54" n="HIAT:ip">,</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_57" n="HIAT:w" s="T16">i</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_60" n="HIAT:w" s="T17">tapočkala</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_64" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_66" n="HIAT:w" s="T18">A</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_69" n="HIAT:w" s="T19">ugon</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_72" n="HIAT:w" s="T20">qajnäj</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_75" n="HIAT:w" s="T21">tʼäkgus</ts>
                  <nts id="Seg_76" n="HIAT:ip">.</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_79" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">Ugon</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_84" n="HIAT:w" s="T23">tapɨčkalam</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_87" n="HIAT:w" s="T24">me</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_90" n="HIAT:w" s="T25">sormelguzawtə</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_93" n="HIAT:w" s="T26">ontutu</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_96" n="HIAT:w" s="T27">i</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_99" n="HIAT:w" s="T28">warzawtə</ts>
                  <nts id="Seg_100" n="HIAT:ip">.</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_103" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_105" n="HIAT:w" s="T29">Sorɨmkuzau</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_108" n="HIAT:w" s="T30">kɨrgakalam</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_111" n="HIAT:w" s="T31">što</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_114" n="HIAT:w" s="T32">soːdʼigan</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_117" n="HIAT:w" s="T33">jegi</ts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_121" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_123" n="HIAT:w" s="T34">I</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_126" n="HIAT:w" s="T35">lʼübkam</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_129" n="HIAT:w" s="T36">sormukuzan</ts>
                  <nts id="Seg_130" n="HIAT:ip">.</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T37" id="Seg_132" n="sc" s="T1">
               <ts e="T2" id="Seg_134" n="e" s="T1">Ugon </ts>
               <ts e="T3" id="Seg_136" n="e" s="T2">man </ts>
               <ts e="T4" id="Seg_138" n="e" s="T3">kɨːdɨn </ts>
               <ts e="T5" id="Seg_140" n="e" s="T4">pöilam </ts>
               <ts e="T6" id="Seg_142" n="e" s="T5">warɨkuzan. </ts>
               <ts e="T7" id="Seg_144" n="e" s="T6">Bolʼše </ts>
               <ts e="T8" id="Seg_146" n="e" s="T7">qajnä </ts>
               <ts e="T9" id="Seg_148" n="e" s="T8">tʼäkgus. </ts>
               <ts e="T10" id="Seg_150" n="e" s="T9">A </ts>
               <ts e="T11" id="Seg_152" n="e" s="T10">täpär </ts>
               <ts e="T12" id="Seg_154" n="e" s="T11">wsʼäkij </ts>
               <ts e="T13" id="Seg_156" n="e" s="T12">pöjla </ts>
               <ts e="T14" id="Seg_158" n="e" s="T13">jetat, </ts>
               <ts e="T15" id="Seg_160" n="e" s="T14">i </ts>
               <ts e="T16" id="Seg_162" n="e" s="T15">batʼinkala, </ts>
               <ts e="T17" id="Seg_164" n="e" s="T16">i </ts>
               <ts e="T18" id="Seg_166" n="e" s="T17">tapočkala. </ts>
               <ts e="T19" id="Seg_168" n="e" s="T18">A </ts>
               <ts e="T20" id="Seg_170" n="e" s="T19">ugon </ts>
               <ts e="T21" id="Seg_172" n="e" s="T20">qajnäj </ts>
               <ts e="T22" id="Seg_174" n="e" s="T21">tʼäkgus. </ts>
               <ts e="T23" id="Seg_176" n="e" s="T22">Ugon </ts>
               <ts e="T24" id="Seg_178" n="e" s="T23">tapɨčkalam </ts>
               <ts e="T25" id="Seg_180" n="e" s="T24">me </ts>
               <ts e="T26" id="Seg_182" n="e" s="T25">sormelguzawtə </ts>
               <ts e="T27" id="Seg_184" n="e" s="T26">ontutu </ts>
               <ts e="T28" id="Seg_186" n="e" s="T27">i </ts>
               <ts e="T29" id="Seg_188" n="e" s="T28">warzawtə. </ts>
               <ts e="T30" id="Seg_190" n="e" s="T29">Sorɨmkuzau </ts>
               <ts e="T31" id="Seg_192" n="e" s="T30">kɨrgakalam </ts>
               <ts e="T32" id="Seg_194" n="e" s="T31">što </ts>
               <ts e="T33" id="Seg_196" n="e" s="T32">soːdʼigan </ts>
               <ts e="T34" id="Seg_198" n="e" s="T33">jegi. </ts>
               <ts e="T35" id="Seg_200" n="e" s="T34">I </ts>
               <ts e="T36" id="Seg_202" n="e" s="T35">lʼübkam </ts>
               <ts e="T37" id="Seg_204" n="e" s="T36">sormukuzan. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_205" s="T1">PVD_1964_MyShoes_nar.001 (001.001)</ta>
            <ta e="T9" id="Seg_206" s="T6">PVD_1964_MyShoes_nar.002 (001.002)</ta>
            <ta e="T18" id="Seg_207" s="T9">PVD_1964_MyShoes_nar.003 (001.003)</ta>
            <ta e="T22" id="Seg_208" s="T18">PVD_1964_MyShoes_nar.004 (001.004)</ta>
            <ta e="T29" id="Seg_209" s="T22">PVD_1964_MyShoes_nar.005 (001.005)</ta>
            <ta e="T34" id="Seg_210" s="T29">PVD_1964_MyShoes_nar.006 (001.006)</ta>
            <ta e="T37" id="Seg_211" s="T34">PVD_1964_MyShoes_nar.007 (001.007)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_212" s="T1">у′гон ман ′кы̄дын пӧи′лам ′варыкузан.</ta>
            <ta e="T9" id="Seg_213" s="T6">′б̂олʼше kай′нӓ ′тʼӓкгус.</ta>
            <ta e="T18" id="Seg_214" s="T9">а тӓ′пӓр всʼӓкий пӧй′ла ′jетат, и б̂а′тʼинкала, и ′тапотшкала.</ta>
            <ta e="T22" id="Seg_215" s="T18">а у′гон kай′нӓй ′тʼӓкгус.</ta>
            <ta e="T29" id="Seg_216" s="T22">у′гон ′тапычкалам ме сор′мелгузаwтъ ′онтуту и вар′заwтъ.</ta>
            <ta e="T34" id="Seg_217" s="T29">со′рымкузау кыр′гакалам што со̄дʼиган ′jеги.</ta>
            <ta e="T37" id="Seg_218" s="T34">и лʼю′бкам сор′муку′зан.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_219" s="T1">ugon man kɨːdɨn pöilam warɨkuzan.</ta>
            <ta e="T9" id="Seg_220" s="T6">b̂olʼše qajnä tʼäkgus.</ta>
            <ta e="T18" id="Seg_221" s="T9">a täpär wsʼäkij pöjla jetat, i b̂atʼinkala, i tapotškala.</ta>
            <ta e="T22" id="Seg_222" s="T18">a ugon qajnäj tʼäkgus.</ta>
            <ta e="T29" id="Seg_223" s="T22">ugon tapɨčkalam me sormelguzawtə ontutu i warzawtə.</ta>
            <ta e="T34" id="Seg_224" s="T29">sorɨmkuzau kɨrgakalam što soːdʼigan jegi.</ta>
            <ta e="T37" id="Seg_225" s="T34">i lʼюbkam sormukuzan.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_226" s="T1">Ugon man kɨːdɨn pöilam warɨkuzan. </ta>
            <ta e="T9" id="Seg_227" s="T6">Bolʼše qajnä tʼäkgus. </ta>
            <ta e="T18" id="Seg_228" s="T9">A täpär wsʼäkij pöjla jetat, i batʼinkala, i tapočkala. </ta>
            <ta e="T22" id="Seg_229" s="T18">A ugon qajnäj tʼäkgus. </ta>
            <ta e="T29" id="Seg_230" s="T22">Ugon tapɨčkalam me sormelguzawtə ontutu i warzawtə. </ta>
            <ta e="T34" id="Seg_231" s="T29">Sorɨmkuzau kɨrgakalam što soːdʼigan jegi. </ta>
            <ta e="T37" id="Seg_232" s="T34">I lʼübkam sormukuzan. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_233" s="T1">ugon</ta>
            <ta e="T3" id="Seg_234" s="T2">man</ta>
            <ta e="T4" id="Seg_235" s="T3">kɨːdɨn</ta>
            <ta e="T5" id="Seg_236" s="T4">pöi-la-m</ta>
            <ta e="T6" id="Seg_237" s="T5">warɨ-ku-za-n</ta>
            <ta e="T7" id="Seg_238" s="T6">bolʼše</ta>
            <ta e="T8" id="Seg_239" s="T7">qaj-nä</ta>
            <ta e="T9" id="Seg_240" s="T8">tʼäkgu-s</ta>
            <ta e="T10" id="Seg_241" s="T9">a</ta>
            <ta e="T11" id="Seg_242" s="T10">täpär</ta>
            <ta e="T12" id="Seg_243" s="T11">wsʼäkij</ta>
            <ta e="T13" id="Seg_244" s="T12">pöj-la</ta>
            <ta e="T14" id="Seg_245" s="T13">je-ta-t</ta>
            <ta e="T15" id="Seg_246" s="T14">i</ta>
            <ta e="T16" id="Seg_247" s="T15">batʼinka-la</ta>
            <ta e="T17" id="Seg_248" s="T16">i</ta>
            <ta e="T18" id="Seg_249" s="T17">tapočka-la</ta>
            <ta e="T19" id="Seg_250" s="T18">a</ta>
            <ta e="T20" id="Seg_251" s="T19">ugon</ta>
            <ta e="T21" id="Seg_252" s="T20">qaj-näj</ta>
            <ta e="T22" id="Seg_253" s="T21">tʼäkgu-s</ta>
            <ta e="T23" id="Seg_254" s="T22">ugon</ta>
            <ta e="T24" id="Seg_255" s="T23">tapɨčka-la-m</ta>
            <ta e="T25" id="Seg_256" s="T24">me</ta>
            <ta e="T26" id="Seg_257" s="T25">sorme-l-gu-za-wtə</ta>
            <ta e="T27" id="Seg_258" s="T26">ontutu</ta>
            <ta e="T28" id="Seg_259" s="T27">i</ta>
            <ta e="T29" id="Seg_260" s="T28">war-za-wtə</ta>
            <ta e="T30" id="Seg_261" s="T29">sorɨm-ku-za-u</ta>
            <ta e="T31" id="Seg_262" s="T30">kɨr-ga-ka-la-m</ta>
            <ta e="T32" id="Seg_263" s="T31">što</ta>
            <ta e="T33" id="Seg_264" s="T32">soːdʼiga-n</ta>
            <ta e="T34" id="Seg_265" s="T33">je-gi</ta>
            <ta e="T35" id="Seg_266" s="T34">i</ta>
            <ta e="T36" id="Seg_267" s="T35">lʼübka-m</ta>
            <ta e="T37" id="Seg_268" s="T36">sormu-ku-za-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_269" s="T1">ugon</ta>
            <ta e="T3" id="Seg_270" s="T2">man</ta>
            <ta e="T4" id="Seg_271" s="T3">qɨdan</ta>
            <ta e="T5" id="Seg_272" s="T4">pöw-la-m</ta>
            <ta e="T6" id="Seg_273" s="T5">warɨ-ku-sɨ-ŋ</ta>
            <ta e="T7" id="Seg_274" s="T6">bolʼše</ta>
            <ta e="T8" id="Seg_275" s="T7">qaj-näj</ta>
            <ta e="T9" id="Seg_276" s="T8">tʼäkku-sɨ</ta>
            <ta e="T10" id="Seg_277" s="T9">a</ta>
            <ta e="T11" id="Seg_278" s="T10">teper</ta>
            <ta e="T12" id="Seg_279" s="T11">wsʼäkij</ta>
            <ta e="T13" id="Seg_280" s="T12">pöw-la</ta>
            <ta e="T14" id="Seg_281" s="T13">eː-ntɨ-tɨn</ta>
            <ta e="T15" id="Seg_282" s="T14">i</ta>
            <ta e="T16" id="Seg_283" s="T15">batʼinka-la</ta>
            <ta e="T17" id="Seg_284" s="T16">i</ta>
            <ta e="T18" id="Seg_285" s="T17">tapočka-la</ta>
            <ta e="T19" id="Seg_286" s="T18">a</ta>
            <ta e="T20" id="Seg_287" s="T19">ugon</ta>
            <ta e="T21" id="Seg_288" s="T20">qaj-näj</ta>
            <ta e="T22" id="Seg_289" s="T21">tʼäkku-sɨ</ta>
            <ta e="T23" id="Seg_290" s="T22">ugon</ta>
            <ta e="T24" id="Seg_291" s="T23">tapočka-la-m</ta>
            <ta e="T25" id="Seg_292" s="T24">me</ta>
            <ta e="T26" id="Seg_293" s="T25">sormu-l-ku-sɨ-un</ta>
            <ta e="T27" id="Seg_294" s="T26">ontutu</ta>
            <ta e="T28" id="Seg_295" s="T27">i</ta>
            <ta e="T29" id="Seg_296" s="T28">warɨ-sɨ-un</ta>
            <ta e="T30" id="Seg_297" s="T29">sormu-ku-sɨ-w</ta>
            <ta e="T31" id="Seg_298" s="T30">qɨr-ka-ka-la-m</ta>
            <ta e="T32" id="Seg_299" s="T31">što</ta>
            <ta e="T33" id="Seg_300" s="T32">soːdʼiga-ŋ</ta>
            <ta e="T34" id="Seg_301" s="T33">eː-ku</ta>
            <ta e="T35" id="Seg_302" s="T34">i</ta>
            <ta e="T36" id="Seg_303" s="T35">lʼübka-m</ta>
            <ta e="T37" id="Seg_304" s="T36">sormu-ku-sɨ-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_305" s="T1">earlier</ta>
            <ta e="T3" id="Seg_306" s="T2">I.NOM</ta>
            <ta e="T4" id="Seg_307" s="T3">all.the.time</ta>
            <ta e="T5" id="Seg_308" s="T4">boots-PL-ACC</ta>
            <ta e="T6" id="Seg_309" s="T5">wear-HAB-PST-1SG.S</ta>
            <ta e="T7" id="Seg_310" s="T6">more</ta>
            <ta e="T8" id="Seg_311" s="T7">what.[NOM]-EMPH</ta>
            <ta e="T9" id="Seg_312" s="T8">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T10" id="Seg_313" s="T9">and</ta>
            <ta e="T11" id="Seg_314" s="T10">now</ta>
            <ta e="T12" id="Seg_315" s="T11">any</ta>
            <ta e="T13" id="Seg_316" s="T12">shoes-PL.[NOM]</ta>
            <ta e="T14" id="Seg_317" s="T13">be-INFER-3PL</ta>
            <ta e="T15" id="Seg_318" s="T14">and</ta>
            <ta e="T16" id="Seg_319" s="T15">boots-PL.[NOM]</ta>
            <ta e="T17" id="Seg_320" s="T16">and</ta>
            <ta e="T18" id="Seg_321" s="T17">slippers-PL.[NOM]</ta>
            <ta e="T19" id="Seg_322" s="T18">and</ta>
            <ta e="T20" id="Seg_323" s="T19">earlier</ta>
            <ta e="T21" id="Seg_324" s="T20">what.[NOM]-EMPH</ta>
            <ta e="T22" id="Seg_325" s="T21">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T23" id="Seg_326" s="T22">earlier</ta>
            <ta e="T24" id="Seg_327" s="T23">slippers-PL-ACC</ta>
            <ta e="T25" id="Seg_328" s="T24">we.[NOM]</ta>
            <ta e="T26" id="Seg_329" s="T25">knit-INCH-HAB-PST-1PL</ta>
            <ta e="T27" id="Seg_330" s="T26">oneself.1PL.[NOM]</ta>
            <ta e="T28" id="Seg_331" s="T27">and</ta>
            <ta e="T29" id="Seg_332" s="T28">wear-PST-1PL</ta>
            <ta e="T30" id="Seg_333" s="T29">knit-HAB-PST-1SG.O</ta>
            <ta e="T31" id="Seg_334" s="T30">hole-DIM-DIM-PL-ACC</ta>
            <ta e="T32" id="Seg_335" s="T31">that</ta>
            <ta e="T33" id="Seg_336" s="T32">good-ADVZ</ta>
            <ta e="T34" id="Seg_337" s="T33">be-HAB.[3SG.S]</ta>
            <ta e="T35" id="Seg_338" s="T34">and</ta>
            <ta e="T36" id="Seg_339" s="T35">skirt-ACC</ta>
            <ta e="T37" id="Seg_340" s="T36">knit-HAB-PST-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_341" s="T1">раньше</ta>
            <ta e="T3" id="Seg_342" s="T2">я.NOM</ta>
            <ta e="T4" id="Seg_343" s="T3">все.время</ta>
            <ta e="T5" id="Seg_344" s="T4">чарки-PL-ACC</ta>
            <ta e="T6" id="Seg_345" s="T5">носить-HAB-PST-1SG.S</ta>
            <ta e="T7" id="Seg_346" s="T6">больше</ta>
            <ta e="T8" id="Seg_347" s="T7">что.[NOM]-EMPH</ta>
            <ta e="T9" id="Seg_348" s="T8">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T10" id="Seg_349" s="T9">а</ta>
            <ta e="T11" id="Seg_350" s="T10">теперь</ta>
            <ta e="T12" id="Seg_351" s="T11">всякий</ta>
            <ta e="T13" id="Seg_352" s="T12">обувь-PL.[NOM]</ta>
            <ta e="T14" id="Seg_353" s="T13">быть-INFER-3PL</ta>
            <ta e="T15" id="Seg_354" s="T14">и</ta>
            <ta e="T16" id="Seg_355" s="T15">ботинки-PL.[NOM]</ta>
            <ta e="T17" id="Seg_356" s="T16">и</ta>
            <ta e="T18" id="Seg_357" s="T17">тапочки-PL.[NOM]</ta>
            <ta e="T19" id="Seg_358" s="T18">а</ta>
            <ta e="T20" id="Seg_359" s="T19">раньше</ta>
            <ta e="T21" id="Seg_360" s="T20">что.[NOM]-EMPH</ta>
            <ta e="T22" id="Seg_361" s="T21">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T23" id="Seg_362" s="T22">раньше</ta>
            <ta e="T24" id="Seg_363" s="T23">тапочки-PL-ACC</ta>
            <ta e="T25" id="Seg_364" s="T24">мы.[NOM]</ta>
            <ta e="T26" id="Seg_365" s="T25">связать-INCH-HAB-PST-1PL</ta>
            <ta e="T27" id="Seg_366" s="T26">сам.1PL.[NOM]</ta>
            <ta e="T28" id="Seg_367" s="T27">и</ta>
            <ta e="T29" id="Seg_368" s="T28">носить-PST-1PL</ta>
            <ta e="T30" id="Seg_369" s="T29">связать-HAB-PST-1SG.O</ta>
            <ta e="T31" id="Seg_370" s="T30">дыра-DIM-DIM-PL-ACC</ta>
            <ta e="T32" id="Seg_371" s="T31">что</ta>
            <ta e="T33" id="Seg_372" s="T32">хороший-ADVZ</ta>
            <ta e="T34" id="Seg_373" s="T33">быть-HAB.[3SG.S]</ta>
            <ta e="T35" id="Seg_374" s="T34">и</ta>
            <ta e="T36" id="Seg_375" s="T35">юбка-ACC</ta>
            <ta e="T37" id="Seg_376" s="T36">связать-HAB-PST-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_377" s="T1">adv</ta>
            <ta e="T3" id="Seg_378" s="T2">pers</ta>
            <ta e="T4" id="Seg_379" s="T3">adv</ta>
            <ta e="T5" id="Seg_380" s="T4">n-n:num-n:case</ta>
            <ta e="T6" id="Seg_381" s="T5">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_382" s="T6">adv</ta>
            <ta e="T8" id="Seg_383" s="T7">interrog.[n:case]-clit</ta>
            <ta e="T9" id="Seg_384" s="T8">v-v:tense.[v:pn]</ta>
            <ta e="T10" id="Seg_385" s="T9">conj</ta>
            <ta e="T11" id="Seg_386" s="T10">adv</ta>
            <ta e="T12" id="Seg_387" s="T11">adj</ta>
            <ta e="T13" id="Seg_388" s="T12">n-n:num.[n:case]</ta>
            <ta e="T14" id="Seg_389" s="T13">v-v:mood-v:pn</ta>
            <ta e="T15" id="Seg_390" s="T14">conj</ta>
            <ta e="T16" id="Seg_391" s="T15">n-n:num.[n:case]</ta>
            <ta e="T17" id="Seg_392" s="T16">conj</ta>
            <ta e="T18" id="Seg_393" s="T17">n-n:num.[n:case]</ta>
            <ta e="T19" id="Seg_394" s="T18">conj</ta>
            <ta e="T20" id="Seg_395" s="T19">adv</ta>
            <ta e="T21" id="Seg_396" s="T20">interrog.[n:case]-clit</ta>
            <ta e="T22" id="Seg_397" s="T21">v-v:tense.[v:pn]</ta>
            <ta e="T23" id="Seg_398" s="T22">adv</ta>
            <ta e="T24" id="Seg_399" s="T23">n-n:num-n:case</ta>
            <ta e="T25" id="Seg_400" s="T24">pers.[n:case]</ta>
            <ta e="T26" id="Seg_401" s="T25">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_402" s="T26">emphpro.[n:case]</ta>
            <ta e="T28" id="Seg_403" s="T27">conj</ta>
            <ta e="T29" id="Seg_404" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_405" s="T29">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_406" s="T30">n-n&gt;n-n&gt;n-n:num-n:case</ta>
            <ta e="T32" id="Seg_407" s="T31">conj</ta>
            <ta e="T33" id="Seg_408" s="T32">adj-adj&gt;adv</ta>
            <ta e="T34" id="Seg_409" s="T33">v-v&gt;v.[v:pn]</ta>
            <ta e="T35" id="Seg_410" s="T34">conj</ta>
            <ta e="T36" id="Seg_411" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_412" s="T36">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_413" s="T1">adv</ta>
            <ta e="T3" id="Seg_414" s="T2">pers</ta>
            <ta e="T4" id="Seg_415" s="T3">adv</ta>
            <ta e="T5" id="Seg_416" s="T4">n</ta>
            <ta e="T6" id="Seg_417" s="T5">v</ta>
            <ta e="T7" id="Seg_418" s="T6">adv</ta>
            <ta e="T8" id="Seg_419" s="T7">pro</ta>
            <ta e="T9" id="Seg_420" s="T8">v</ta>
            <ta e="T10" id="Seg_421" s="T9">conj</ta>
            <ta e="T11" id="Seg_422" s="T10">adv</ta>
            <ta e="T12" id="Seg_423" s="T11">adj</ta>
            <ta e="T13" id="Seg_424" s="T12">n</ta>
            <ta e="T14" id="Seg_425" s="T13">v</ta>
            <ta e="T15" id="Seg_426" s="T14">conj</ta>
            <ta e="T16" id="Seg_427" s="T15">n</ta>
            <ta e="T17" id="Seg_428" s="T16">conj</ta>
            <ta e="T18" id="Seg_429" s="T17">n</ta>
            <ta e="T19" id="Seg_430" s="T18">conj</ta>
            <ta e="T20" id="Seg_431" s="T19">adv</ta>
            <ta e="T21" id="Seg_432" s="T20">pro</ta>
            <ta e="T22" id="Seg_433" s="T21">v</ta>
            <ta e="T23" id="Seg_434" s="T22">adv</ta>
            <ta e="T24" id="Seg_435" s="T23">n</ta>
            <ta e="T25" id="Seg_436" s="T24">pers</ta>
            <ta e="T26" id="Seg_437" s="T25">v</ta>
            <ta e="T27" id="Seg_438" s="T26">emphpro</ta>
            <ta e="T28" id="Seg_439" s="T27">conj</ta>
            <ta e="T29" id="Seg_440" s="T28">v</ta>
            <ta e="T30" id="Seg_441" s="T29">v</ta>
            <ta e="T31" id="Seg_442" s="T30">n</ta>
            <ta e="T32" id="Seg_443" s="T31">conj</ta>
            <ta e="T33" id="Seg_444" s="T32">adv</ta>
            <ta e="T34" id="Seg_445" s="T33">v</ta>
            <ta e="T35" id="Seg_446" s="T34">conj</ta>
            <ta e="T36" id="Seg_447" s="T35">n</ta>
            <ta e="T37" id="Seg_448" s="T36">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_449" s="T1">adv:Time</ta>
            <ta e="T3" id="Seg_450" s="T2">pro.h:A</ta>
            <ta e="T5" id="Seg_451" s="T4">np:Th</ta>
            <ta e="T8" id="Seg_452" s="T7">pro:Th</ta>
            <ta e="T11" id="Seg_453" s="T10">adv:Time</ta>
            <ta e="T13" id="Seg_454" s="T12">np:Th</ta>
            <ta e="T20" id="Seg_455" s="T19">adv:Time</ta>
            <ta e="T21" id="Seg_456" s="T20">pro:Th</ta>
            <ta e="T23" id="Seg_457" s="T22">adv:Time</ta>
            <ta e="T24" id="Seg_458" s="T23">np:P</ta>
            <ta e="T25" id="Seg_459" s="T24">pro.h:A</ta>
            <ta e="T29" id="Seg_460" s="T28">0.1.h:A 0.3:Th</ta>
            <ta e="T30" id="Seg_461" s="T29">0.1.h:A</ta>
            <ta e="T31" id="Seg_462" s="T30">np:P</ta>
            <ta e="T34" id="Seg_463" s="T33">0.3:Th</ta>
            <ta e="T36" id="Seg_464" s="T35">np:P</ta>
            <ta e="T37" id="Seg_465" s="T36">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_466" s="T2">pro.h:S</ta>
            <ta e="T5" id="Seg_467" s="T4">np:O</ta>
            <ta e="T6" id="Seg_468" s="T5">v:pred</ta>
            <ta e="T8" id="Seg_469" s="T7">pro:S</ta>
            <ta e="T9" id="Seg_470" s="T8">v:pred</ta>
            <ta e="T13" id="Seg_471" s="T12">np:S</ta>
            <ta e="T14" id="Seg_472" s="T13">v:pred</ta>
            <ta e="T21" id="Seg_473" s="T20">pro:S</ta>
            <ta e="T22" id="Seg_474" s="T21">v:pred</ta>
            <ta e="T24" id="Seg_475" s="T23">np:O</ta>
            <ta e="T25" id="Seg_476" s="T24">pro.h:S</ta>
            <ta e="T26" id="Seg_477" s="T25">v:pred</ta>
            <ta e="T29" id="Seg_478" s="T28">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T30" id="Seg_479" s="T29">0.1.h:S v:pred</ta>
            <ta e="T31" id="Seg_480" s="T30">np:O</ta>
            <ta e="T34" id="Seg_481" s="T31">s:purp</ta>
            <ta e="T36" id="Seg_482" s="T35">np:O</ta>
            <ta e="T37" id="Seg_483" s="T36">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T7" id="Seg_484" s="T6">RUS:core</ta>
            <ta e="T10" id="Seg_485" s="T9">RUS:gram</ta>
            <ta e="T11" id="Seg_486" s="T10">RUS:core</ta>
            <ta e="T12" id="Seg_487" s="T11">RUS:core</ta>
            <ta e="T15" id="Seg_488" s="T14">RUS:gram</ta>
            <ta e="T16" id="Seg_489" s="T15">RUS:cult</ta>
            <ta e="T17" id="Seg_490" s="T16">RUS:gram</ta>
            <ta e="T18" id="Seg_491" s="T17">RUS:cult</ta>
            <ta e="T19" id="Seg_492" s="T18">RUS:gram</ta>
            <ta e="T24" id="Seg_493" s="T23">RUS:cult</ta>
            <ta e="T28" id="Seg_494" s="T27">RUS:gram</ta>
            <ta e="T32" id="Seg_495" s="T31">RUS:gram</ta>
            <ta e="T35" id="Seg_496" s="T34">RUS:gram</ta>
            <ta e="T36" id="Seg_497" s="T35">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_498" s="T1">Earlier I was wearing traditional boots all the time.</ta>
            <ta e="T9" id="Seg_499" s="T6">I didn't have anything else.</ta>
            <ta e="T18" id="Seg_500" s="T9">Now there are different shoes: boots, slippers.</ta>
            <ta e="T22" id="Seg_501" s="T18">And earlier there was nothing.</ta>
            <ta e="T29" id="Seg_502" s="T22">Earlier we used to knit slippers ourselves and wore them.</ta>
            <ta e="T34" id="Seg_503" s="T29">I used to knit holes, so that they looked good.</ta>
            <ta e="T37" id="Seg_504" s="T34">And I used to knit skirt(s).</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_505" s="T1">Früher trug ich immer traditionelle Stiefel.</ta>
            <ta e="T9" id="Seg_506" s="T6">Ich hatte nichts anderes.</ta>
            <ta e="T18" id="Seg_507" s="T9">Und jetzt gibt es verschiedene Schuhe: Stiefel, Pantoffeln.</ta>
            <ta e="T22" id="Seg_508" s="T18">Und früher gab es nichts.</ta>
            <ta e="T29" id="Seg_509" s="T22">Früher haben wir Pantoffeln selbst gestrickt und sie getragen.</ta>
            <ta e="T34" id="Seg_510" s="T29">Ich habe immer Löcher hineingestrickt, sodass sie gut aussahen.</ta>
            <ta e="T37" id="Seg_511" s="T34">Und ich habe oft Röcke gestrickt.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_512" s="T1">Раньше я все время чарки носила.</ta>
            <ta e="T9" id="Seg_513" s="T6">Больше ничего не было.</ta>
            <ta e="T18" id="Seg_514" s="T9">А теперь всякая обувь есть: и ботинки, и тапочки.</ta>
            <ta e="T22" id="Seg_515" s="T18">А раньше ничего не было.</ta>
            <ta e="T29" id="Seg_516" s="T22">Раньше тапочки мы сами вязали и носили.</ta>
            <ta e="T34" id="Seg_517" s="T29">Я вязала дырочки, чтобы хорошо были.</ta>
            <ta e="T37" id="Seg_518" s="T34">И юбку вязала.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_519" s="T1">раньше я все время чарки носила</ta>
            <ta e="T9" id="Seg_520" s="T6">больше ничего не было</ta>
            <ta e="T18" id="Seg_521" s="T9">а теперь всякие обувки есть</ta>
            <ta e="T22" id="Seg_522" s="T18">а раньше ничего не было</ta>
            <ta e="T29" id="Seg_523" s="T22">раньше тапочки мы сами вязали и носили</ta>
            <ta e="T34" id="Seg_524" s="T29">вязала дырочки чтобы хорошо были</ta>
            <ta e="T37" id="Seg_525" s="T34">и юбку вязала</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T29" id="Seg_526" s="T22">[BrM:] INCH?</ta>
            <ta e="T34" id="Seg_527" s="T29">[BrM:] Two DIMs?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
