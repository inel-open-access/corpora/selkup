<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_MySister_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_MySister_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">30</ud-information>
            <ud-information attribute-name="# HIAT:w">24</ud-information>
            <ud-information attribute-name="# e">22</ud-information>
            <ud-information attribute-name="# HIAT:u">6</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <timeline-fork end="T9" start="T8">
            <tli id="T8.tx.1" />
         </timeline-fork>
         <timeline-fork end="T20" start="T19">
            <tli id="T19.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T23" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Ugon</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">Wdʼäredoɣon</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">man</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">nannʼau</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">warkɨs</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_20" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">Täpär</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">tüpba</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8.tx.1" id="Seg_28" n="HIAT:w" s="T8">Čʼornaja</ts>
                  <nts id="Seg_29" n="HIAT:ip">_</nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8.tx.1">rečkatda</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_35" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">Maːdɨmdə</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">merɨpbat</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">i</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">sɨrəmdə</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">merɨpbat</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_53" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">Täper</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">täbnan</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">qajdänä</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">tʼakgu</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_68" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">Tüpba</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19.tx.1" id="Seg_73" n="HIAT:w" s="T19">Čʼornaja</ts>
                  <nts id="Seg_74" n="HIAT:ip">_</nts>
                  <ts e="T20" id="Seg_76" n="HIAT:w" s="T19.tx.1">rečkatda</ts>
                  <nts id="Seg_77" n="HIAT:ip">.</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_80" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_82" n="HIAT:w" s="T20">Nɨtdɨn</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_85" n="HIAT:w" s="T21">warka</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_88" n="HIAT:w" s="T22">iːtdəze</ts>
                  <nts id="Seg_89" n="HIAT:ip">.</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T23" id="Seg_91" n="sc" s="T1">
               <ts e="T2" id="Seg_93" n="e" s="T1">Ugon </ts>
               <ts e="T3" id="Seg_95" n="e" s="T2">Wdʼäredoɣon </ts>
               <ts e="T4" id="Seg_97" n="e" s="T3">man </ts>
               <ts e="T5" id="Seg_99" n="e" s="T4">nannʼau </ts>
               <ts e="T6" id="Seg_101" n="e" s="T5">warkɨs. </ts>
               <ts e="T7" id="Seg_103" n="e" s="T6">Täpär </ts>
               <ts e="T8" id="Seg_105" n="e" s="T7">tüpba </ts>
               <ts e="T9" id="Seg_107" n="e" s="T8">Čʼornaja_rečkatda. </ts>
               <ts e="T10" id="Seg_109" n="e" s="T9">Maːdɨmdə </ts>
               <ts e="T11" id="Seg_111" n="e" s="T10">merɨpbat </ts>
               <ts e="T12" id="Seg_113" n="e" s="T11">i </ts>
               <ts e="T13" id="Seg_115" n="e" s="T12">sɨrəmdə </ts>
               <ts e="T14" id="Seg_117" n="e" s="T13">merɨpbat. </ts>
               <ts e="T15" id="Seg_119" n="e" s="T14">Täper </ts>
               <ts e="T16" id="Seg_121" n="e" s="T15">täbnan </ts>
               <ts e="T17" id="Seg_123" n="e" s="T16">qajdänä </ts>
               <ts e="T18" id="Seg_125" n="e" s="T17">tʼakgu. </ts>
               <ts e="T19" id="Seg_127" n="e" s="T18">Tüpba </ts>
               <ts e="T20" id="Seg_129" n="e" s="T19">Čʼornaja_rečkatda. </ts>
               <ts e="T21" id="Seg_131" n="e" s="T20">Nɨtdɨn </ts>
               <ts e="T22" id="Seg_133" n="e" s="T21">warka </ts>
               <ts e="T23" id="Seg_135" n="e" s="T22">iːtdəze. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_136" s="T1">PVD_1964_MySister_nar.001 (001.001)</ta>
            <ta e="T9" id="Seg_137" s="T6">PVD_1964_MySister_nar.002 (001.002)</ta>
            <ta e="T14" id="Seg_138" s="T9">PVD_1964_MySister_nar.003 (001.003)</ta>
            <ta e="T18" id="Seg_139" s="T14">PVD_1964_MySister_nar.004 (001.004)</ta>
            <ta e="T20" id="Seg_140" s="T18">PVD_1964_MySister_nar.005 (001.005)</ta>
            <ta e="T23" id="Seg_141" s="T20">PVD_1964_MySister_nar.006 (001.006)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_142" s="T1">у′гон вдʼӓ′редоɣон ман наннʼау̹ варкыс.</ta>
            <ta e="T9" id="Seg_143" s="T6">тӓ′пӓр ′тӱпба ′Чʼорнаjа ′речкатда.</ta>
            <ta e="T14" id="Seg_144" s="T9">′ма̄дымдъ ′мерыпб̂ат и ′сыръмдъ ′мерыпбат.</ta>
            <ta e="T18" id="Seg_145" s="T14">тӓ′пер тӓб′нан ′kайдӓнӓ ′тʼакгу.</ta>
            <ta e="T20" id="Seg_146" s="T18">′тӱпба ′Чорнаjа ′речкатда.</ta>
            <ta e="T23" id="Seg_147" s="T20">ныт′дын вар′ка ′ӣтдъзе.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_148" s="T1">ugon wdʼäredoɣon man nannʼau̹ warkɨs.</ta>
            <ta e="T9" id="Seg_149" s="T6">täpär tüpba Чʼornaja rečkatda.</ta>
            <ta e="T14" id="Seg_150" s="T9">maːdɨmdə merɨpb̂at i sɨrəmdə merɨpbat.</ta>
            <ta e="T18" id="Seg_151" s="T14">täper täbnan qajdänä tʼakgu.</ta>
            <ta e="T20" id="Seg_152" s="T18">tüpba Чornaja rečkatda.</ta>
            <ta e="T23" id="Seg_153" s="T20">nɨtdɨn warka iːtdəze.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_154" s="T1">Ugon Wdʼäredoɣon man nannʼau warkɨs. </ta>
            <ta e="T9" id="Seg_155" s="T6">Täpär tüpba Čʼornaja_rečkatda. </ta>
            <ta e="T14" id="Seg_156" s="T9">Maːdɨmdə merɨpbat i sɨrəmdə merɨpbat. </ta>
            <ta e="T18" id="Seg_157" s="T14">Täper täbnan qajdänä tʼakgu. </ta>
            <ta e="T20" id="Seg_158" s="T18">Tüpba Čʼornaja_rečkatda. </ta>
            <ta e="T23" id="Seg_159" s="T20">Nɨtdɨn warka iːtdəze. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_160" s="T1">ugon</ta>
            <ta e="T3" id="Seg_161" s="T2">Wdʼäredo-ɣon</ta>
            <ta e="T4" id="Seg_162" s="T3">man</ta>
            <ta e="T5" id="Seg_163" s="T4">nannʼa-u</ta>
            <ta e="T6" id="Seg_164" s="T5">warkɨ-s</ta>
            <ta e="T7" id="Seg_165" s="T6">täpär</ta>
            <ta e="T8" id="Seg_166" s="T7">tü-pba</ta>
            <ta e="T9" id="Seg_167" s="T8">Čʼornaja_rečka-tda</ta>
            <ta e="T10" id="Seg_168" s="T9">maːd-ɨ-m-də</ta>
            <ta e="T11" id="Seg_169" s="T10">merɨp-ba-t</ta>
            <ta e="T12" id="Seg_170" s="T11">i</ta>
            <ta e="T13" id="Seg_171" s="T12">sɨr-ə-m-də</ta>
            <ta e="T14" id="Seg_172" s="T13">merɨp-ba-t</ta>
            <ta e="T15" id="Seg_173" s="T14">täper</ta>
            <ta e="T16" id="Seg_174" s="T15">täb-nan</ta>
            <ta e="T17" id="Seg_175" s="T16">qaj-dä-nä</ta>
            <ta e="T18" id="Seg_176" s="T17">tʼakgu</ta>
            <ta e="T19" id="Seg_177" s="T18">tü-pba</ta>
            <ta e="T20" id="Seg_178" s="T19">Čʼornaja_rečka-tda</ta>
            <ta e="T21" id="Seg_179" s="T20">nɨtdɨ-n</ta>
            <ta e="T22" id="Seg_180" s="T21">warka</ta>
            <ta e="T23" id="Seg_181" s="T22">iː-tdə-ze</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_182" s="T1">ugon</ta>
            <ta e="T3" id="Seg_183" s="T2">Uːdʼarʼedɨ-qɨn</ta>
            <ta e="T4" id="Seg_184" s="T3">man</ta>
            <ta e="T5" id="Seg_185" s="T4">nanʼa-w</ta>
            <ta e="T6" id="Seg_186" s="T5">warkɨ-sɨ</ta>
            <ta e="T7" id="Seg_187" s="T6">teper</ta>
            <ta e="T8" id="Seg_188" s="T7">tüː-mbɨ</ta>
            <ta e="T9" id="Seg_189" s="T8">Čʼornaja_rečka-ntə</ta>
            <ta e="T10" id="Seg_190" s="T9">maːt-ɨ-m-tə</ta>
            <ta e="T11" id="Seg_191" s="T10">merɨŋ-mbɨ-t</ta>
            <ta e="T12" id="Seg_192" s="T11">i</ta>
            <ta e="T13" id="Seg_193" s="T12">sɨr-ɨ-m-tə</ta>
            <ta e="T14" id="Seg_194" s="T13">merɨŋ-mbɨ-t</ta>
            <ta e="T15" id="Seg_195" s="T14">teper</ta>
            <ta e="T16" id="Seg_196" s="T15">täp-nan</ta>
            <ta e="T17" id="Seg_197" s="T16">qaj-ta-näj</ta>
            <ta e="T18" id="Seg_198" s="T17">tʼäkku</ta>
            <ta e="T19" id="Seg_199" s="T18">tüː-mbɨ</ta>
            <ta e="T20" id="Seg_200" s="T19">Čʼornaja_rečka-ntə</ta>
            <ta e="T21" id="Seg_201" s="T20">*natʼe-n</ta>
            <ta e="T22" id="Seg_202" s="T21">warkɨ</ta>
            <ta e="T23" id="Seg_203" s="T22">iː-tə-se</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_204" s="T1">earlier</ta>
            <ta e="T3" id="Seg_205" s="T2">Tiskino-LOC</ta>
            <ta e="T4" id="Seg_206" s="T3">I.GEN</ta>
            <ta e="T5" id="Seg_207" s="T4">sister.[NOM]-1SG</ta>
            <ta e="T6" id="Seg_208" s="T5">live-PST.[3SG.S]</ta>
            <ta e="T7" id="Seg_209" s="T6">now</ta>
            <ta e="T8" id="Seg_210" s="T7">come-PST.NAR.[3SG.S]</ta>
            <ta e="T9" id="Seg_211" s="T8">Chornaya_rechka-ILL</ta>
            <ta e="T10" id="Seg_212" s="T9">house-EP-ACC-3SG</ta>
            <ta e="T11" id="Seg_213" s="T10">sell-PST.NAR-3SG.O</ta>
            <ta e="T12" id="Seg_214" s="T11">and</ta>
            <ta e="T13" id="Seg_215" s="T12">cow-EP-ACC-3SG</ta>
            <ta e="T14" id="Seg_216" s="T13">sell-PST.NAR-3SG.O</ta>
            <ta e="T15" id="Seg_217" s="T14">now</ta>
            <ta e="T16" id="Seg_218" s="T15">(s)he-ADES</ta>
            <ta e="T17" id="Seg_219" s="T16">what-INDEF-EMPH</ta>
            <ta e="T18" id="Seg_220" s="T17">NEG.EX.[3SG.S]</ta>
            <ta e="T19" id="Seg_221" s="T18">come-PST.NAR.[3SG.S]</ta>
            <ta e="T20" id="Seg_222" s="T19">Chornaya_rechka-ILL</ta>
            <ta e="T21" id="Seg_223" s="T20">there-ADV.LOC</ta>
            <ta e="T22" id="Seg_224" s="T21">live.[3SG.S]</ta>
            <ta e="T23" id="Seg_225" s="T22">son-3SG-COM</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_226" s="T1">раньше</ta>
            <ta e="T3" id="Seg_227" s="T2">Тискино-LOC</ta>
            <ta e="T4" id="Seg_228" s="T3">я.GEN</ta>
            <ta e="T5" id="Seg_229" s="T4">сестра.[NOM]-1SG</ta>
            <ta e="T6" id="Seg_230" s="T5">жить-PST.[3SG.S]</ta>
            <ta e="T7" id="Seg_231" s="T6">теперь</ta>
            <ta e="T8" id="Seg_232" s="T7">приехать-PST.NAR.[3SG.S]</ta>
            <ta e="T9" id="Seg_233" s="T8">Чёрная_речка-ILL</ta>
            <ta e="T10" id="Seg_234" s="T9">дом-EP-ACC-3SG</ta>
            <ta e="T11" id="Seg_235" s="T10">продать-PST.NAR-3SG.O</ta>
            <ta e="T12" id="Seg_236" s="T11">и</ta>
            <ta e="T13" id="Seg_237" s="T12">корова-EP-ACC-3SG</ta>
            <ta e="T14" id="Seg_238" s="T13">продать-PST.NAR-3SG.O</ta>
            <ta e="T15" id="Seg_239" s="T14">теперь</ta>
            <ta e="T16" id="Seg_240" s="T15">он(а)-ADES</ta>
            <ta e="T17" id="Seg_241" s="T16">что-INDEF-EMPH</ta>
            <ta e="T18" id="Seg_242" s="T17">NEG.EX.[3SG.S]</ta>
            <ta e="T19" id="Seg_243" s="T18">приехать-PST.NAR.[3SG.S]</ta>
            <ta e="T20" id="Seg_244" s="T19">Чёрная_речка-ILL</ta>
            <ta e="T21" id="Seg_245" s="T20">туда-ADV.LOC</ta>
            <ta e="T22" id="Seg_246" s="T21">жить.[3SG.S]</ta>
            <ta e="T23" id="Seg_247" s="T22">сын-3SG-COM</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_248" s="T1">adv</ta>
            <ta e="T3" id="Seg_249" s="T2">nprop-n:case</ta>
            <ta e="T4" id="Seg_250" s="T3">pers</ta>
            <ta e="T5" id="Seg_251" s="T4">n.[n:case]-n:poss</ta>
            <ta e="T6" id="Seg_252" s="T5">v-v:tense.[v:pn]</ta>
            <ta e="T7" id="Seg_253" s="T6">adv</ta>
            <ta e="T8" id="Seg_254" s="T7">v-v:tense.[v:pn]</ta>
            <ta e="T9" id="Seg_255" s="T8">nprop-n:case</ta>
            <ta e="T10" id="Seg_256" s="T9">n-n:ins-n:case-n:poss</ta>
            <ta e="T11" id="Seg_257" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_258" s="T11">conj</ta>
            <ta e="T13" id="Seg_259" s="T12">n-n:ins-n:case-n:poss</ta>
            <ta e="T14" id="Seg_260" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_261" s="T14">adv</ta>
            <ta e="T16" id="Seg_262" s="T15">pers-n:case</ta>
            <ta e="T17" id="Seg_263" s="T16">interrog-clit-clit</ta>
            <ta e="T18" id="Seg_264" s="T17">v.[v:pn]</ta>
            <ta e="T19" id="Seg_265" s="T18">v-v:tense.[v:pn]</ta>
            <ta e="T20" id="Seg_266" s="T19">nprop-n:case</ta>
            <ta e="T21" id="Seg_267" s="T20">adv-adv:case</ta>
            <ta e="T22" id="Seg_268" s="T21">v.[v:pn]</ta>
            <ta e="T23" id="Seg_269" s="T22">n-n:poss-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_270" s="T1">adv</ta>
            <ta e="T3" id="Seg_271" s="T2">nprop</ta>
            <ta e="T4" id="Seg_272" s="T3">pers</ta>
            <ta e="T5" id="Seg_273" s="T4">n</ta>
            <ta e="T6" id="Seg_274" s="T5">v</ta>
            <ta e="T7" id="Seg_275" s="T6">adv</ta>
            <ta e="T8" id="Seg_276" s="T7">v</ta>
            <ta e="T9" id="Seg_277" s="T8">nprop</ta>
            <ta e="T10" id="Seg_278" s="T9">n</ta>
            <ta e="T11" id="Seg_279" s="T10">v</ta>
            <ta e="T12" id="Seg_280" s="T11">conj</ta>
            <ta e="T13" id="Seg_281" s="T12">n</ta>
            <ta e="T14" id="Seg_282" s="T13">v</ta>
            <ta e="T15" id="Seg_283" s="T14">adv</ta>
            <ta e="T16" id="Seg_284" s="T15">pers</ta>
            <ta e="T17" id="Seg_285" s="T16">pro</ta>
            <ta e="T18" id="Seg_286" s="T17">v</ta>
            <ta e="T19" id="Seg_287" s="T18">v</ta>
            <ta e="T20" id="Seg_288" s="T19">nprop</ta>
            <ta e="T21" id="Seg_289" s="T20">adv</ta>
            <ta e="T22" id="Seg_290" s="T21">v</ta>
            <ta e="T23" id="Seg_291" s="T22">n</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T5" id="Seg_292" s="T4">np.h:S</ta>
            <ta e="T6" id="Seg_293" s="T5">v:pred</ta>
            <ta e="T8" id="Seg_294" s="T7">0.3.h:S v:pred</ta>
            <ta e="T10" id="Seg_295" s="T9">np:O</ta>
            <ta e="T11" id="Seg_296" s="T10">0.3.h:S v:pred</ta>
            <ta e="T13" id="Seg_297" s="T12">np:O</ta>
            <ta e="T14" id="Seg_298" s="T13">0.3.h:S v:pred</ta>
            <ta e="T17" id="Seg_299" s="T16">np:S</ta>
            <ta e="T18" id="Seg_300" s="T17">v:pred</ta>
            <ta e="T19" id="Seg_301" s="T18">0.3.h:S v:pred</ta>
            <ta e="T22" id="Seg_302" s="T21">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_303" s="T1">adv:Time</ta>
            <ta e="T3" id="Seg_304" s="T2">np:L</ta>
            <ta e="T4" id="Seg_305" s="T3">pro.h:Poss</ta>
            <ta e="T5" id="Seg_306" s="T4">np.h:Th</ta>
            <ta e="T7" id="Seg_307" s="T6">adv:Time</ta>
            <ta e="T8" id="Seg_308" s="T7">0.3.h:A</ta>
            <ta e="T9" id="Seg_309" s="T8">np:G</ta>
            <ta e="T10" id="Seg_310" s="T9">0.3.h:Poss np:Th</ta>
            <ta e="T11" id="Seg_311" s="T10">0.3.h:A</ta>
            <ta e="T13" id="Seg_312" s="T12">0.3.h:Poss np:Th</ta>
            <ta e="T14" id="Seg_313" s="T13">0.3.h:A</ta>
            <ta e="T15" id="Seg_314" s="T14">adv:Time</ta>
            <ta e="T16" id="Seg_315" s="T15">pro.h:Poss</ta>
            <ta e="T17" id="Seg_316" s="T16">np:Th</ta>
            <ta e="T19" id="Seg_317" s="T18">0.3.h:A</ta>
            <ta e="T20" id="Seg_318" s="T19">np:G</ta>
            <ta e="T21" id="Seg_319" s="T20">adv:L</ta>
            <ta e="T22" id="Seg_320" s="T21">0.3.h:Th</ta>
            <ta e="T23" id="Seg_321" s="T22">np:Com 0.3.h:Poss</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T7" id="Seg_322" s="T6">RUS:core</ta>
            <ta e="T12" id="Seg_323" s="T11">RUS:gram</ta>
            <ta e="T15" id="Seg_324" s="T14">RUS:core</ta>
            <ta e="T17" id="Seg_325" s="T16">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_326" s="T1">Earlier my younger sister lived in Tiskino.</ta>
            <ta e="T9" id="Seg_327" s="T6">Now she moved to Chornaja Rechka.</ta>
            <ta e="T14" id="Seg_328" s="T9">She sold her house and her cow.</ta>
            <ta e="T18" id="Seg_329" s="T14">Now she doesn't have anything.</ta>
            <ta e="T20" id="Seg_330" s="T18">She moved to Chornaja Rechka.</ta>
            <ta e="T23" id="Seg_331" s="T20">She lives there with her son.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_332" s="T1">Früher lebte meine kleine Schwester in Tiskino.</ta>
            <ta e="T9" id="Seg_333" s="T6">Jetzt ist sie nach Chornaja Rechka gezogen.</ta>
            <ta e="T14" id="Seg_334" s="T9">Sie hat ihr Haus und ihre Kuh verkauft.</ta>
            <ta e="T18" id="Seg_335" s="T14">Jetzt hat sie gar nichts.</ta>
            <ta e="T20" id="Seg_336" s="T18">Sie ist nach Chornaja Rechka gezogen.</ta>
            <ta e="T23" id="Seg_337" s="T20">Sie lebt dort mit ihrem Sohn.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_338" s="T1">Раньше в Тискино моя младшая сестра жила.</ta>
            <ta e="T9" id="Seg_339" s="T6">Теперь приехала в Черную речку.</ta>
            <ta e="T14" id="Seg_340" s="T9">Дом свой продала и корову продала.</ta>
            <ta e="T18" id="Seg_341" s="T14">Теперь у нее ничего нет.</ta>
            <ta e="T20" id="Seg_342" s="T18">Приехала в Черную Речку.</ta>
            <ta e="T23" id="Seg_343" s="T20">Там живет с сыном.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_344" s="T1">раньше в Тискино моя младшая сестра жила</ta>
            <ta e="T9" id="Seg_345" s="T6">теперь приехала в Черную речку</ta>
            <ta e="T14" id="Seg_346" s="T9">дом (ее) продала и корову продала</ta>
            <ta e="T18" id="Seg_347" s="T14">теперь у нее ничего нет</ta>
            <ta e="T23" id="Seg_348" s="T20">тут живет с сыном</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
