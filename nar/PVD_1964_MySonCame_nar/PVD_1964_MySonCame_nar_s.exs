<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_MySonCame_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_MySonCame_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">61</ud-information>
            <ud-information attribute-name="# HIAT:w">41</ud-information>
            <ud-information attribute-name="# e">41</ud-information>
            <ud-information attribute-name="# HIAT:u">8</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T42" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Mekga</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">iːw</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">tükkus</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Nagur</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">pin</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">sekɨn</ts>
                  <nts id="Seg_23" n="HIAT:ip">,</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">tamdʼel</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">qwannɨn</ts>
                  <nts id="Seg_30" n="HIAT:ip">.</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_33" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">No</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">man</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">as</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">türɨzan</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_48" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">A</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">qajno</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">tʼürugu</ts>
                  <nts id="Seg_57" n="HIAT:ip">?</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_60" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">A</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">qussaqɨn</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">iːw</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">tüːs</ts>
                  <nts id="Seg_72" n="HIAT:ip">,</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">man</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">tʼürəzan</ts>
                  <nts id="Seg_79" n="HIAT:ip">.</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_82" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">A</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_87" n="HIAT:w" s="T23">qɨbanädäkka</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_90" n="HIAT:w" s="T24">tʼarɨn</ts>
                  <nts id="Seg_91" n="HIAT:ip">:</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_93" n="HIAT:ip">“</nts>
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">Igə</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">tʼüːraq</ts>
                  <nts id="Seg_99" n="HIAT:ip">,</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_102" n="HIAT:w" s="T27">igə</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_105" n="HIAT:w" s="T28">tʼüraq</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_108" n="HIAT:w" s="T29">tötka</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_111" n="HIAT:w" s="T30">Vera</ts>
                  <nts id="Seg_112" n="HIAT:ip">.</nts>
                  <nts id="Seg_113" n="HIAT:ip">”</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_116" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_118" n="HIAT:w" s="T31">Iːw</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_121" n="HIAT:w" s="T32">tʼarɨn</ts>
                  <nts id="Seg_122" n="HIAT:ip">:</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_124" n="HIAT:ip">“</nts>
                  <ts e="T34" id="Seg_126" n="HIAT:w" s="T33">Igə</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_129" n="HIAT:w" s="T34">tʼüraq</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_132" n="HIAT:w" s="T35">mamou</ts>
                  <nts id="Seg_133" n="HIAT:ip">.</nts>
                  <nts id="Seg_134" n="HIAT:ip">”</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_137" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_139" n="HIAT:w" s="T36">I</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_142" n="HIAT:w" s="T37">pajaga</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_145" n="HIAT:w" s="T38">megka</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_148" n="HIAT:w" s="T39">tʼarɨn</ts>
                  <nts id="Seg_149" n="HIAT:ip">:</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_151" n="HIAT:ip">“</nts>
                  <ts e="T41" id="Seg_153" n="HIAT:w" s="T40">Igə</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_156" n="HIAT:w" s="T41">tʼüːrlʼet</ts>
                  <nts id="Seg_157" n="HIAT:ip">.</nts>
                  <nts id="Seg_158" n="HIAT:ip">”</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T42" id="Seg_160" n="sc" s="T1">
               <ts e="T2" id="Seg_162" n="e" s="T1">Mekga </ts>
               <ts e="T3" id="Seg_164" n="e" s="T2">iːw </ts>
               <ts e="T4" id="Seg_166" n="e" s="T3">tükkus. </ts>
               <ts e="T5" id="Seg_168" n="e" s="T4">Nagur </ts>
               <ts e="T6" id="Seg_170" n="e" s="T5">pin </ts>
               <ts e="T7" id="Seg_172" n="e" s="T6">sekɨn, </ts>
               <ts e="T8" id="Seg_174" n="e" s="T7">tamdʼel </ts>
               <ts e="T9" id="Seg_176" n="e" s="T8">qwannɨn. </ts>
               <ts e="T10" id="Seg_178" n="e" s="T9">No </ts>
               <ts e="T11" id="Seg_180" n="e" s="T10">man </ts>
               <ts e="T12" id="Seg_182" n="e" s="T11">as </ts>
               <ts e="T13" id="Seg_184" n="e" s="T12">türɨzan. </ts>
               <ts e="T14" id="Seg_186" n="e" s="T13">A </ts>
               <ts e="T15" id="Seg_188" n="e" s="T14">qajno </ts>
               <ts e="T16" id="Seg_190" n="e" s="T15">tʼürugu? </ts>
               <ts e="T17" id="Seg_192" n="e" s="T16">A </ts>
               <ts e="T18" id="Seg_194" n="e" s="T17">qussaqɨn </ts>
               <ts e="T19" id="Seg_196" n="e" s="T18">iːw </ts>
               <ts e="T20" id="Seg_198" n="e" s="T19">tüːs, </ts>
               <ts e="T21" id="Seg_200" n="e" s="T20">man </ts>
               <ts e="T22" id="Seg_202" n="e" s="T21">tʼürəzan. </ts>
               <ts e="T23" id="Seg_204" n="e" s="T22">A </ts>
               <ts e="T24" id="Seg_206" n="e" s="T23">qɨbanädäkka </ts>
               <ts e="T25" id="Seg_208" n="e" s="T24">tʼarɨn: </ts>
               <ts e="T26" id="Seg_210" n="e" s="T25">“Igə </ts>
               <ts e="T27" id="Seg_212" n="e" s="T26">tʼüːraq, </ts>
               <ts e="T28" id="Seg_214" n="e" s="T27">igə </ts>
               <ts e="T29" id="Seg_216" n="e" s="T28">tʼüraq </ts>
               <ts e="T30" id="Seg_218" n="e" s="T29">tötka </ts>
               <ts e="T31" id="Seg_220" n="e" s="T30">Vera.” </ts>
               <ts e="T32" id="Seg_222" n="e" s="T31">Iːw </ts>
               <ts e="T33" id="Seg_224" n="e" s="T32">tʼarɨn: </ts>
               <ts e="T34" id="Seg_226" n="e" s="T33">“Igə </ts>
               <ts e="T35" id="Seg_228" n="e" s="T34">tʼüraq </ts>
               <ts e="T36" id="Seg_230" n="e" s="T35">mamou.” </ts>
               <ts e="T37" id="Seg_232" n="e" s="T36">I </ts>
               <ts e="T38" id="Seg_234" n="e" s="T37">pajaga </ts>
               <ts e="T39" id="Seg_236" n="e" s="T38">megka </ts>
               <ts e="T40" id="Seg_238" n="e" s="T39">tʼarɨn: </ts>
               <ts e="T41" id="Seg_240" n="e" s="T40">“Igə </ts>
               <ts e="T42" id="Seg_242" n="e" s="T41">tʼüːrlʼet.” </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_243" s="T1">PVD_1964_MySonCame_nar.001 (001.001)</ta>
            <ta e="T9" id="Seg_244" s="T4">PVD_1964_MySonCame_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_245" s="T9">PVD_1964_MySonCame_nar.003 (001.003)</ta>
            <ta e="T16" id="Seg_246" s="T13">PVD_1964_MySonCame_nar.004 (001.004)</ta>
            <ta e="T22" id="Seg_247" s="T16">PVD_1964_MySonCame_nar.005 (001.005)</ta>
            <ta e="T31" id="Seg_248" s="T22">PVD_1964_MySonCame_nar.006 (001.006)</ta>
            <ta e="T36" id="Seg_249" s="T31">PVD_1964_MySonCame_nar.007 (001.007)</ta>
            <ta e="T42" id="Seg_250" s="T36">PVD_1964_MySonCame_nar.008 (001.008)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_251" s="T1">′мекга ӣw ′тӱккус.</ta>
            <ta e="T9" id="Seg_252" s="T4">′нагур ′пин ′секын, там′дʼел ′kwаннын.</ta>
            <ta e="T13" id="Seg_253" s="T9">но ман ас ′тӱрызан.</ta>
            <ta e="T16" id="Seg_254" s="T13">а kай′но ′тʼӱругу.</ta>
            <ta e="T22" id="Seg_255" s="T16">а kу′ссаkын ӣw тӱ̄с, ман ′тʼӱръзан.</ta>
            <ta e="T31" id="Seg_256" s="T22">а kы′банӓ′дӓкка тʼа′рын: ′игъ ′тʼӱ̄′раk, ′игъ ′тʼӱраk тӧтка Ве′ра.</ta>
            <ta e="T36" id="Seg_257" s="T31">ӣw тʼа′рын: ′игъ тʼӱраk ′мамоу.</ta>
            <ta e="T42" id="Seg_258" s="T36">и па′jага мегка тʼа′рын: игъ ′тʼӱ̄рлʼет.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_259" s="T1">mekga iːw tükkus.</ta>
            <ta e="T9" id="Seg_260" s="T4">nagur pin sekɨn, tamdʼel qwannɨn.</ta>
            <ta e="T13" id="Seg_261" s="T9">no man as türɨzan.</ta>
            <ta e="T16" id="Seg_262" s="T13">a qajno tʼürugu.</ta>
            <ta e="T22" id="Seg_263" s="T16">a qussaqɨn iːw tüːs, man tʼürəzan.</ta>
            <ta e="T31" id="Seg_264" s="T22">a qɨbanädäkka tʼarɨn: igə tʼüːraq, igə tʼüraq tötka Вera.</ta>
            <ta e="T36" id="Seg_265" s="T31">iːw tʼarɨn: igə tʼüraq mamou.</ta>
            <ta e="T42" id="Seg_266" s="T36">i pajaga megka tʼarɨn: igə tʼüːrlʼet.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_267" s="T1">Mekga iːw tükkus. </ta>
            <ta e="T9" id="Seg_268" s="T4">Nagur pin sekɨn, tamdʼel qwannɨn. </ta>
            <ta e="T13" id="Seg_269" s="T9">No man as türɨzan. </ta>
            <ta e="T16" id="Seg_270" s="T13">A qajno tʼürugu? </ta>
            <ta e="T22" id="Seg_271" s="T16">A qussaqɨn iːw tüːs, man tʼürəzan. </ta>
            <ta e="T31" id="Seg_272" s="T22">A qɨbanädäkka tʼarɨn: “Igə tʼüːraq, igə tʼüraq tötka Vera.” </ta>
            <ta e="T36" id="Seg_273" s="T31">Iːw tʼarɨn: “Igə tʼüraq mamou.” </ta>
            <ta e="T42" id="Seg_274" s="T36">I pajaga megka tʼarɨn: “Igə tʼüːrlʼet.” </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_275" s="T1">mekga</ta>
            <ta e="T3" id="Seg_276" s="T2">iː-w</ta>
            <ta e="T4" id="Seg_277" s="T3">tü-kku-s</ta>
            <ta e="T5" id="Seg_278" s="T4">nagur</ta>
            <ta e="T6" id="Seg_279" s="T5">pi-n</ta>
            <ta e="T7" id="Seg_280" s="T6">sekɨ-n</ta>
            <ta e="T8" id="Seg_281" s="T7">tam-dʼel</ta>
            <ta e="T9" id="Seg_282" s="T8">qwan-nɨ-n</ta>
            <ta e="T10" id="Seg_283" s="T9">no</ta>
            <ta e="T11" id="Seg_284" s="T10">man</ta>
            <ta e="T12" id="Seg_285" s="T11">as</ta>
            <ta e="T13" id="Seg_286" s="T12">türɨ-za-n</ta>
            <ta e="T14" id="Seg_287" s="T13">a</ta>
            <ta e="T15" id="Seg_288" s="T14">qaj-no</ta>
            <ta e="T16" id="Seg_289" s="T15">tʼüru-gu</ta>
            <ta e="T17" id="Seg_290" s="T16">a</ta>
            <ta e="T18" id="Seg_291" s="T17">qussaqɨn</ta>
            <ta e="T19" id="Seg_292" s="T18">iː-w</ta>
            <ta e="T20" id="Seg_293" s="T19">tüː-s</ta>
            <ta e="T21" id="Seg_294" s="T20">man</ta>
            <ta e="T22" id="Seg_295" s="T21">tʼürə-za-n</ta>
            <ta e="T23" id="Seg_296" s="T22">a</ta>
            <ta e="T24" id="Seg_297" s="T23">qɨba-nädäk-ka</ta>
            <ta e="T25" id="Seg_298" s="T24">tʼarɨ-n</ta>
            <ta e="T26" id="Seg_299" s="T25">igə</ta>
            <ta e="T27" id="Seg_300" s="T26">tʼüːra-q</ta>
            <ta e="T28" id="Seg_301" s="T27">igə</ta>
            <ta e="T29" id="Seg_302" s="T28">tʼüra-q</ta>
            <ta e="T30" id="Seg_303" s="T29">tötka</ta>
            <ta e="T31" id="Seg_304" s="T30">Vera</ta>
            <ta e="T32" id="Seg_305" s="T31">iː-w</ta>
            <ta e="T33" id="Seg_306" s="T32">tʼarɨ-n</ta>
            <ta e="T34" id="Seg_307" s="T33">igə</ta>
            <ta e="T35" id="Seg_308" s="T34">tʼüra-q</ta>
            <ta e="T36" id="Seg_309" s="T35">mamo-u</ta>
            <ta e="T37" id="Seg_310" s="T36">i</ta>
            <ta e="T38" id="Seg_311" s="T37">paja-ga</ta>
            <ta e="T39" id="Seg_312" s="T38">megka</ta>
            <ta e="T40" id="Seg_313" s="T39">tʼarɨ-n</ta>
            <ta e="T41" id="Seg_314" s="T40">igə</ta>
            <ta e="T42" id="Seg_315" s="T41">tʼüːr-lʼe-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_316" s="T1">mekka</ta>
            <ta e="T3" id="Seg_317" s="T2">iː-w</ta>
            <ta e="T4" id="Seg_318" s="T3">tüː-ku-sɨ</ta>
            <ta e="T5" id="Seg_319" s="T4">nagur</ta>
            <ta e="T6" id="Seg_320" s="T5">pi-n</ta>
            <ta e="T7" id="Seg_321" s="T6">seqqɨ-n</ta>
            <ta e="T8" id="Seg_322" s="T7">taw-dʼel</ta>
            <ta e="T9" id="Seg_323" s="T8">qwan-nɨ-n</ta>
            <ta e="T10" id="Seg_324" s="T9">no</ta>
            <ta e="T11" id="Seg_325" s="T10">man</ta>
            <ta e="T12" id="Seg_326" s="T11">asa</ta>
            <ta e="T13" id="Seg_327" s="T12">tʼüru-sɨ-ŋ</ta>
            <ta e="T14" id="Seg_328" s="T13">a</ta>
            <ta e="T15" id="Seg_329" s="T14">qaj-no</ta>
            <ta e="T16" id="Seg_330" s="T15">tʼüru-gu</ta>
            <ta e="T17" id="Seg_331" s="T16">a</ta>
            <ta e="T18" id="Seg_332" s="T17">qussaqɨn</ta>
            <ta e="T19" id="Seg_333" s="T18">iː-w</ta>
            <ta e="T20" id="Seg_334" s="T19">tüː-sɨ</ta>
            <ta e="T21" id="Seg_335" s="T20">man</ta>
            <ta e="T22" id="Seg_336" s="T21">tʼüru-sɨ-ŋ</ta>
            <ta e="T23" id="Seg_337" s="T22">a</ta>
            <ta e="T24" id="Seg_338" s="T23">qɨba-nädek-ka</ta>
            <ta e="T25" id="Seg_339" s="T24">tʼärɨ-n</ta>
            <ta e="T26" id="Seg_340" s="T25">igə</ta>
            <ta e="T27" id="Seg_341" s="T26">tʼüru-kɨ</ta>
            <ta e="T28" id="Seg_342" s="T27">igə</ta>
            <ta e="T29" id="Seg_343" s="T28">tʼüru-kɨ</ta>
            <ta e="T30" id="Seg_344" s="T29">tʼötka</ta>
            <ta e="T31" id="Seg_345" s="T30">Vera</ta>
            <ta e="T32" id="Seg_346" s="T31">iː-w</ta>
            <ta e="T33" id="Seg_347" s="T32">tʼärɨ-n</ta>
            <ta e="T34" id="Seg_348" s="T33">igə</ta>
            <ta e="T35" id="Seg_349" s="T34">tʼüru-kɨ</ta>
            <ta e="T36" id="Seg_350" s="T35">mamo-w</ta>
            <ta e="T37" id="Seg_351" s="T36">i</ta>
            <ta e="T38" id="Seg_352" s="T37">paja-ka</ta>
            <ta e="T39" id="Seg_353" s="T38">mekka</ta>
            <ta e="T40" id="Seg_354" s="T39">tʼärɨ-n</ta>
            <ta e="T41" id="Seg_355" s="T40">igə</ta>
            <ta e="T42" id="Seg_356" s="T41">tʼüru-lä-ntə</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_357" s="T1">I.ALL</ta>
            <ta e="T3" id="Seg_358" s="T2">son.[NOM]-1SG</ta>
            <ta e="T4" id="Seg_359" s="T3">come-HAB-PST.[3SG.S]</ta>
            <ta e="T5" id="Seg_360" s="T4">three</ta>
            <ta e="T6" id="Seg_361" s="T5">night-ADV.LOC</ta>
            <ta e="T7" id="Seg_362" s="T6">overnight-3SG.S</ta>
            <ta e="T8" id="Seg_363" s="T7">this-day.[NOM]</ta>
            <ta e="T9" id="Seg_364" s="T8">leave-CO-3SG.S</ta>
            <ta e="T10" id="Seg_365" s="T9">but</ta>
            <ta e="T11" id="Seg_366" s="T10">I.NOM</ta>
            <ta e="T12" id="Seg_367" s="T11">NEG</ta>
            <ta e="T13" id="Seg_368" s="T12">cry-PST-1SG.S</ta>
            <ta e="T14" id="Seg_369" s="T13">and</ta>
            <ta e="T15" id="Seg_370" s="T14">what-TRL</ta>
            <ta e="T16" id="Seg_371" s="T15">cry-INF</ta>
            <ta e="T17" id="Seg_372" s="T16">and</ta>
            <ta e="T18" id="Seg_373" s="T17">when</ta>
            <ta e="T19" id="Seg_374" s="T18">son.[NOM]-1SG</ta>
            <ta e="T20" id="Seg_375" s="T19">come-PST.[3SG.S]</ta>
            <ta e="T21" id="Seg_376" s="T20">I.NOM</ta>
            <ta e="T22" id="Seg_377" s="T21">cry-PST-1SG.S</ta>
            <ta e="T23" id="Seg_378" s="T22">and</ta>
            <ta e="T24" id="Seg_379" s="T23">small-girl-DIM.[NOM]</ta>
            <ta e="T25" id="Seg_380" s="T24">say-3SG.S</ta>
            <ta e="T26" id="Seg_381" s="T25">NEG.IMP</ta>
            <ta e="T27" id="Seg_382" s="T26">cry-IMP.2SG.S</ta>
            <ta e="T28" id="Seg_383" s="T27">NEG.IMP</ta>
            <ta e="T29" id="Seg_384" s="T28">cry-IMP.2SG.S</ta>
            <ta e="T30" id="Seg_385" s="T29">aunt.[NOM]</ta>
            <ta e="T31" id="Seg_386" s="T30">Vera.[NOM]</ta>
            <ta e="T32" id="Seg_387" s="T31">son.[NOM]-1SG</ta>
            <ta e="T33" id="Seg_388" s="T32">say-3SG.S</ta>
            <ta e="T34" id="Seg_389" s="T33">NEG.IMP</ta>
            <ta e="T35" id="Seg_390" s="T34">cry-IMP.2SG.S</ta>
            <ta e="T36" id="Seg_391" s="T35">mum.[NOM]-1SG</ta>
            <ta e="T37" id="Seg_392" s="T36">and</ta>
            <ta e="T38" id="Seg_393" s="T37">old.woman-DIM.[NOM]</ta>
            <ta e="T39" id="Seg_394" s="T38">I.ALL</ta>
            <ta e="T40" id="Seg_395" s="T39">say-3SG.S</ta>
            <ta e="T41" id="Seg_396" s="T40">NEG.IMP</ta>
            <ta e="T42" id="Seg_397" s="T41">cry-OPT-2SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_398" s="T1">я.ALL</ta>
            <ta e="T3" id="Seg_399" s="T2">сын.[NOM]-1SG</ta>
            <ta e="T4" id="Seg_400" s="T3">приехать-HAB-PST.[3SG.S]</ta>
            <ta e="T5" id="Seg_401" s="T4">три</ta>
            <ta e="T6" id="Seg_402" s="T5">ночь-ADV.LOC</ta>
            <ta e="T7" id="Seg_403" s="T6">ночевать-3SG.S</ta>
            <ta e="T8" id="Seg_404" s="T7">этот-день.[NOM]</ta>
            <ta e="T9" id="Seg_405" s="T8">отправиться-CO-3SG.S</ta>
            <ta e="T10" id="Seg_406" s="T9">но</ta>
            <ta e="T11" id="Seg_407" s="T10">я.NOM</ta>
            <ta e="T12" id="Seg_408" s="T11">NEG</ta>
            <ta e="T13" id="Seg_409" s="T12">плакать-PST-1SG.S</ta>
            <ta e="T14" id="Seg_410" s="T13">а</ta>
            <ta e="T15" id="Seg_411" s="T14">что-TRL</ta>
            <ta e="T16" id="Seg_412" s="T15">плакать-INF</ta>
            <ta e="T17" id="Seg_413" s="T16">а</ta>
            <ta e="T18" id="Seg_414" s="T17">когда</ta>
            <ta e="T19" id="Seg_415" s="T18">сын.[NOM]-1SG</ta>
            <ta e="T20" id="Seg_416" s="T19">приехать-PST.[3SG.S]</ta>
            <ta e="T21" id="Seg_417" s="T20">я.NOM</ta>
            <ta e="T22" id="Seg_418" s="T21">плакать-PST-1SG.S</ta>
            <ta e="T23" id="Seg_419" s="T22">а</ta>
            <ta e="T24" id="Seg_420" s="T23">маленький-девушка-DIM.[NOM]</ta>
            <ta e="T25" id="Seg_421" s="T24">сказать-3SG.S</ta>
            <ta e="T26" id="Seg_422" s="T25">NEG.IMP</ta>
            <ta e="T27" id="Seg_423" s="T26">плакать-IMP.2SG.S</ta>
            <ta e="T28" id="Seg_424" s="T27">NEG.IMP</ta>
            <ta e="T29" id="Seg_425" s="T28">плакать-IMP.2SG.S</ta>
            <ta e="T30" id="Seg_426" s="T29">тетка.[NOM]</ta>
            <ta e="T31" id="Seg_427" s="T30">Вера.[NOM]</ta>
            <ta e="T32" id="Seg_428" s="T31">сын.[NOM]-1SG</ta>
            <ta e="T33" id="Seg_429" s="T32">сказать-3SG.S</ta>
            <ta e="T34" id="Seg_430" s="T33">NEG.IMP</ta>
            <ta e="T35" id="Seg_431" s="T34">плакать-IMP.2SG.S</ta>
            <ta e="T36" id="Seg_432" s="T35">мама.[NOM]-1SG</ta>
            <ta e="T37" id="Seg_433" s="T36">и</ta>
            <ta e="T38" id="Seg_434" s="T37">старуха-DIM.[NOM]</ta>
            <ta e="T39" id="Seg_435" s="T38">я.ALL</ta>
            <ta e="T40" id="Seg_436" s="T39">сказать-3SG.S</ta>
            <ta e="T41" id="Seg_437" s="T40">NEG.IMP</ta>
            <ta e="T42" id="Seg_438" s="T41">плакать-OPT-2SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_439" s="T1">pers</ta>
            <ta e="T3" id="Seg_440" s="T2">n.[n:case]-n:poss</ta>
            <ta e="T4" id="Seg_441" s="T3">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T5" id="Seg_442" s="T4">num</ta>
            <ta e="T6" id="Seg_443" s="T5">n-adv:case</ta>
            <ta e="T7" id="Seg_444" s="T6">v-v:pn</ta>
            <ta e="T8" id="Seg_445" s="T7">dem-n.[n:case]</ta>
            <ta e="T9" id="Seg_446" s="T8">v-v:ins-v:pn</ta>
            <ta e="T10" id="Seg_447" s="T9">conj</ta>
            <ta e="T11" id="Seg_448" s="T10">pers</ta>
            <ta e="T12" id="Seg_449" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_450" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_451" s="T13">conj</ta>
            <ta e="T15" id="Seg_452" s="T14">interrog-n:case</ta>
            <ta e="T16" id="Seg_453" s="T15">v-v:inf</ta>
            <ta e="T17" id="Seg_454" s="T16">conj</ta>
            <ta e="T18" id="Seg_455" s="T17">conj</ta>
            <ta e="T19" id="Seg_456" s="T18">n.[n:case]-n:poss</ta>
            <ta e="T20" id="Seg_457" s="T19">v-v:tense.[v:pn]</ta>
            <ta e="T21" id="Seg_458" s="T20">pers</ta>
            <ta e="T22" id="Seg_459" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_460" s="T22">conj</ta>
            <ta e="T24" id="Seg_461" s="T23">adj-n-n&gt;n.[n:case]</ta>
            <ta e="T25" id="Seg_462" s="T24">v-v:pn</ta>
            <ta e="T26" id="Seg_463" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_464" s="T26">v-v:mood.pn</ta>
            <ta e="T28" id="Seg_465" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_466" s="T28">v-v:mood.pn</ta>
            <ta e="T30" id="Seg_467" s="T29">n.[n:case]</ta>
            <ta e="T31" id="Seg_468" s="T30">nprop.[n:case]</ta>
            <ta e="T32" id="Seg_469" s="T31">n.[n:case]-n:poss</ta>
            <ta e="T33" id="Seg_470" s="T32">v-v:pn</ta>
            <ta e="T34" id="Seg_471" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_472" s="T34">v-v:mood.pn</ta>
            <ta e="T36" id="Seg_473" s="T35">n.[n:case]-n:poss</ta>
            <ta e="T37" id="Seg_474" s="T36">conj</ta>
            <ta e="T38" id="Seg_475" s="T37">n-n&gt;n.[n:case]</ta>
            <ta e="T39" id="Seg_476" s="T38">pers</ta>
            <ta e="T40" id="Seg_477" s="T39">v-v:pn</ta>
            <ta e="T41" id="Seg_478" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_479" s="T41">v-v:mood-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_480" s="T1">pers</ta>
            <ta e="T3" id="Seg_481" s="T2">n</ta>
            <ta e="T4" id="Seg_482" s="T3">v</ta>
            <ta e="T5" id="Seg_483" s="T4">num</ta>
            <ta e="T6" id="Seg_484" s="T5">n</ta>
            <ta e="T7" id="Seg_485" s="T6">v</ta>
            <ta e="T8" id="Seg_486" s="T7">adv</ta>
            <ta e="T9" id="Seg_487" s="T8">v</ta>
            <ta e="T10" id="Seg_488" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_489" s="T10">pers</ta>
            <ta e="T12" id="Seg_490" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_491" s="T12">v</ta>
            <ta e="T14" id="Seg_492" s="T13">conj</ta>
            <ta e="T15" id="Seg_493" s="T14">interrog</ta>
            <ta e="T16" id="Seg_494" s="T15">v</ta>
            <ta e="T17" id="Seg_495" s="T16">conj</ta>
            <ta e="T18" id="Seg_496" s="T17">conj</ta>
            <ta e="T19" id="Seg_497" s="T18">n</ta>
            <ta e="T20" id="Seg_498" s="T19">v</ta>
            <ta e="T21" id="Seg_499" s="T20">pers</ta>
            <ta e="T22" id="Seg_500" s="T21">v</ta>
            <ta e="T23" id="Seg_501" s="T22">conj</ta>
            <ta e="T24" id="Seg_502" s="T23">n</ta>
            <ta e="T25" id="Seg_503" s="T24">v</ta>
            <ta e="T26" id="Seg_504" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_505" s="T26">v</ta>
            <ta e="T28" id="Seg_506" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_507" s="T28">v</ta>
            <ta e="T30" id="Seg_508" s="T29">n</ta>
            <ta e="T31" id="Seg_509" s="T30">nprop</ta>
            <ta e="T32" id="Seg_510" s="T31">n</ta>
            <ta e="T33" id="Seg_511" s="T32">v</ta>
            <ta e="T34" id="Seg_512" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_513" s="T34">v</ta>
            <ta e="T36" id="Seg_514" s="T35">n</ta>
            <ta e="T37" id="Seg_515" s="T36">conj</ta>
            <ta e="T38" id="Seg_516" s="T37">n</ta>
            <ta e="T39" id="Seg_517" s="T38">pers</ta>
            <ta e="T40" id="Seg_518" s="T39">v</ta>
            <ta e="T41" id="Seg_519" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_520" s="T41">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_521" s="T1">pro.h:G</ta>
            <ta e="T3" id="Seg_522" s="T2">np.h:A 0.1.h:Poss</ta>
            <ta e="T6" id="Seg_523" s="T5">adv:Time</ta>
            <ta e="T7" id="Seg_524" s="T6">0.3.h:Th</ta>
            <ta e="T8" id="Seg_525" s="T7">np:Time</ta>
            <ta e="T9" id="Seg_526" s="T8">0.3.h:A</ta>
            <ta e="T11" id="Seg_527" s="T10">pro.h:A</ta>
            <ta e="T19" id="Seg_528" s="T18">np.h:A 0.1.h:Poss</ta>
            <ta e="T21" id="Seg_529" s="T20">pro.h:A</ta>
            <ta e="T24" id="Seg_530" s="T23">np.h:A</ta>
            <ta e="T27" id="Seg_531" s="T26">0.2.h:A</ta>
            <ta e="T29" id="Seg_532" s="T28">0.2.h:A</ta>
            <ta e="T32" id="Seg_533" s="T31">np.h:A 0.1.h:Poss</ta>
            <ta e="T35" id="Seg_534" s="T34">0.2.h:A</ta>
            <ta e="T38" id="Seg_535" s="T37">np.h:A</ta>
            <ta e="T39" id="Seg_536" s="T38">pro.h:R</ta>
            <ta e="T42" id="Seg_537" s="T41">0.2.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_538" s="T2">np.h:S</ta>
            <ta e="T4" id="Seg_539" s="T3">v:pred</ta>
            <ta e="T7" id="Seg_540" s="T6">0.3.h:S v:pred</ta>
            <ta e="T9" id="Seg_541" s="T8">0.3.h:S v:pred</ta>
            <ta e="T11" id="Seg_542" s="T10">pro.h:S</ta>
            <ta e="T13" id="Seg_543" s="T12">v:pred</ta>
            <ta e="T20" id="Seg_544" s="T16">s:temp</ta>
            <ta e="T21" id="Seg_545" s="T20">pro.h:S</ta>
            <ta e="T22" id="Seg_546" s="T21">v:pred</ta>
            <ta e="T24" id="Seg_547" s="T23">np.h:S</ta>
            <ta e="T25" id="Seg_548" s="T24">v:pred</ta>
            <ta e="T27" id="Seg_549" s="T26">0.2.h:S v:pred</ta>
            <ta e="T29" id="Seg_550" s="T28">0.2.h:S v:pred</ta>
            <ta e="T32" id="Seg_551" s="T31">np.h:S</ta>
            <ta e="T33" id="Seg_552" s="T32">v:pred</ta>
            <ta e="T35" id="Seg_553" s="T34">0.2.h:S v:pred</ta>
            <ta e="T38" id="Seg_554" s="T37">np.h:S</ta>
            <ta e="T40" id="Seg_555" s="T39">v:pred</ta>
            <ta e="T42" id="Seg_556" s="T41">0.2.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T10" id="Seg_557" s="T9">RUS:gram</ta>
            <ta e="T14" id="Seg_558" s="T13">RUS:gram</ta>
            <ta e="T17" id="Seg_559" s="T16">RUS:gram</ta>
            <ta e="T23" id="Seg_560" s="T22">RUS:gram</ta>
            <ta e="T30" id="Seg_561" s="T29">RUS:cult</ta>
            <ta e="T36" id="Seg_562" s="T35">RUS:core</ta>
            <ta e="T37" id="Seg_563" s="T36">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_564" s="T1">My son came to me.</ta>
            <ta e="T9" id="Seg_565" s="T4">He spent three night here, he left today.</ta>
            <ta e="T13" id="Seg_566" s="T9">But I didn't cry.</ta>
            <ta e="T16" id="Seg_567" s="T13">Why would one cry?</ta>
            <ta e="T22" id="Seg_568" s="T16">And when my son had come, I cried.</ta>
            <ta e="T31" id="Seg_569" s="T22">And a little girl said: “Don't cry, aunt Vera, don't cry.”</ta>
            <ta e="T36" id="Seg_570" s="T31">My son said to me: “Don't cry, mom.”</ta>
            <ta e="T42" id="Seg_571" s="T36">And an old woman said to me: “You don't have to cry.”</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_572" s="T1">Mein Sohn kam zu mir.</ta>
            <ta e="T9" id="Seg_573" s="T4">Drei Nächte verbrachte er hier, er ist heute gegangen.</ta>
            <ta e="T13" id="Seg_574" s="T9">Aber ich habe nicht geweint.</ta>
            <ta e="T16" id="Seg_575" s="T13">Warum sollte man weinen?</ta>
            <ta e="T22" id="Seg_576" s="T16">Und als mein Sohn kam, habe ich geweint.</ta>
            <ta e="T31" id="Seg_577" s="T22">Und ein kleines Mädchen sagte: "Weine nicht, Tante Vera, weine nicht."</ta>
            <ta e="T36" id="Seg_578" s="T31">Mein Sohn sagte zu mir: "Weine nicht, Mama."</ta>
            <ta e="T42" id="Seg_579" s="T36">Und eine alte Frau sagte zu mir: "Du musst nicht weinen."</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_580" s="T1">Ко мне сын приезжал.</ta>
            <ta e="T9" id="Seg_581" s="T4">Три ночи ночевал, сегодня уехал.</ta>
            <ta e="T13" id="Seg_582" s="T9">Но я не плакала.</ta>
            <ta e="T16" id="Seg_583" s="T13">А зачем плакать?</ta>
            <ta e="T22" id="Seg_584" s="T16">А когда сын приехал, я плакала.</ta>
            <ta e="T31" id="Seg_585" s="T22">А маленькая девочка сказала: “Не плачь, не плачь, тетя Вера”.</ta>
            <ta e="T36" id="Seg_586" s="T31">Мой сын сказал: “Не плачь, мама”.</ta>
            <ta e="T42" id="Seg_587" s="T36">И старуха мне сказала: “Плакать тебе не надо”.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_588" s="T1">у меня сын приезжал</ta>
            <ta e="T9" id="Seg_589" s="T4">три ночи ночевал сегодня уехал</ta>
            <ta e="T13" id="Seg_590" s="T9">а я не плакала</ta>
            <ta e="T16" id="Seg_591" s="T13">а зачем плакать</ta>
            <ta e="T22" id="Seg_592" s="T16">а когда сын приехал я плакала</ta>
            <ta e="T31" id="Seg_593" s="T22">девочка сказала не плачь</ta>
            <ta e="T42" id="Seg_594" s="T36">плакать не надо</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
