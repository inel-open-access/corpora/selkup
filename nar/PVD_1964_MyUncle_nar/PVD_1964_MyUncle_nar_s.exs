<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_MyUncle_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_MyUncle_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">82</ud-information>
            <ud-information attribute-name="# HIAT:w">62</ud-information>
            <ud-information attribute-name="# e">62</ud-information>
            <ud-information attribute-name="# HIAT:u">14</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T63" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Megga</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">nalam</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_9" n="HIAT:ip">(</nts>
                  <ts e="T4" id="Seg_11" n="HIAT:w" s="T3">mamu</ts>
                  <nts id="Seg_12" n="HIAT:ip">)</nts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_15" n="HIAT:w" s="T4">awaw</ts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_18" n="HIAT:w" s="T5">kessɨt</ts>
                  <nts id="Seg_19" n="HIAT:ip">.</nts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_22" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_24" n="HIAT:w" s="T6">Man</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_27" n="HIAT:w" s="T7">dʼädʼau</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">nʼedɨs</ts>
                  <nts id="Seg_31" n="HIAT:ip">.</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_34" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">Tep</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_39" n="HIAT:w" s="T10">čoʒimnə</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">qwannɨn</ts>
                  <nts id="Seg_43" n="HIAT:ip">.</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_46" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">Tebne</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">nadə</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">wʼetʼättogu</ts>
                  <nts id="Seg_55" n="HIAT:ip">.</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_58" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_60" n="HIAT:w" s="T15">A</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_63" n="HIAT:w" s="T16">čoʒop</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">tʼarɨn</ts>
                  <nts id="Seg_67" n="HIAT:ip">:</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_69" n="HIAT:ip">“</nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">Mekga</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">kötdə</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">soːdʼiga</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">tattɨ</ts>
                  <nts id="Seg_81" n="HIAT:ip">,</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">ato</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_87" n="HIAT:w" s="T23">as</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_90" n="HIAT:w" s="T24">tə</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_93" n="HIAT:w" s="T25">wʼetʼätätʒan</ts>
                  <nts id="Seg_94" n="HIAT:ip">.</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_97" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_99" n="HIAT:w" s="T26">Aːwaj</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_102" n="HIAT:w" s="T27">kötdɨm</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_105" n="HIAT:w" s="T28">as</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_108" n="HIAT:w" s="T29">iǯau</ts>
                  <nts id="Seg_109" n="HIAT:ip">.</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_112" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_114" n="HIAT:w" s="T30">Qwaǯan</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_117" n="HIAT:w" s="T31">onen</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_120" n="HIAT:w" s="T32">qwalle</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_123" n="HIAT:w" s="T33">mannɨmeǯau</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_126" n="HIAT:w" s="T34">soːdʼiga</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_129" n="HIAT:w" s="T35">kötdə</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_132" n="HIAT:w" s="T36">inətʒan</ts>
                  <nts id="Seg_133" n="HIAT:ip">.</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_136" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_138" n="HIAT:w" s="T37">Awaj</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_141" n="HIAT:w" s="T38">kötdə</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_144" n="HIAT:w" s="T39">mekga</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_147" n="HIAT:w" s="T40">ne</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_150" n="HIAT:w" s="T41">nadə</ts>
                  <nts id="Seg_151" n="HIAT:ip">.</nts>
                  <nts id="Seg_152" n="HIAT:ip">”</nts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_155" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_157" n="HIAT:w" s="T42">Tak</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_160" n="HIAT:w" s="T43">i</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_163" n="HIAT:w" s="T44">meut</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_166" n="HIAT:w" s="T45">soːdʼiga</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_169" n="HIAT:w" s="T46">kötdɨm</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_173" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_175" n="HIAT:w" s="T47">Tep</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_178" n="HIAT:w" s="T48">tettɨ</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_181" n="HIAT:w" s="T49">nedukkus</ts>
                  <nts id="Seg_182" n="HIAT:ip">.</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_185" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_187" n="HIAT:w" s="T50">Pajalat</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_190" n="HIAT:w" s="T51">kulʼe</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_193" n="HIAT:w" s="T52">čaʒəzattə</ts>
                  <nts id="Seg_194" n="HIAT:ip">.</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_197" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_199" n="HIAT:w" s="T53">Oqkɨrɨn</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_202" n="HIAT:w" s="T54">wʼetʼatɨs</ts>
                  <nts id="Seg_203" n="HIAT:ip">.</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_206" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_208" n="HIAT:w" s="T55">Man</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_211" n="HIAT:w" s="T56">bɨ</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_214" n="HIAT:w" s="T57">nizašto</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_217" n="HIAT:w" s="T58">as</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_220" n="HIAT:w" s="T59">meneu</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_223" n="HIAT:w" s="T60">kötdɨm</ts>
                  <nts id="Seg_224" n="HIAT:ip">.</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_227" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_229" n="HIAT:w" s="T61">Čoʒɨbla</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_232" n="HIAT:w" s="T62">awaj</ts>
                  <nts id="Seg_233" n="HIAT:ip">.</nts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T63" id="Seg_235" n="sc" s="T1">
               <ts e="T2" id="Seg_237" n="e" s="T1">Megga </ts>
               <ts e="T3" id="Seg_239" n="e" s="T2">nalam </ts>
               <ts e="T4" id="Seg_241" n="e" s="T3">(mamu) </ts>
               <ts e="T5" id="Seg_243" n="e" s="T4">awaw </ts>
               <ts e="T6" id="Seg_245" n="e" s="T5">kessɨt. </ts>
               <ts e="T7" id="Seg_247" n="e" s="T6">Man </ts>
               <ts e="T8" id="Seg_249" n="e" s="T7">dʼädʼau </ts>
               <ts e="T9" id="Seg_251" n="e" s="T8">nʼedɨs. </ts>
               <ts e="T10" id="Seg_253" n="e" s="T9">Tep </ts>
               <ts e="T11" id="Seg_255" n="e" s="T10">čoʒimnə </ts>
               <ts e="T12" id="Seg_257" n="e" s="T11">qwannɨn. </ts>
               <ts e="T13" id="Seg_259" n="e" s="T12">Tebne </ts>
               <ts e="T14" id="Seg_261" n="e" s="T13">nadə </ts>
               <ts e="T15" id="Seg_263" n="e" s="T14">wʼetʼättogu. </ts>
               <ts e="T16" id="Seg_265" n="e" s="T15">A </ts>
               <ts e="T17" id="Seg_267" n="e" s="T16">čoʒop </ts>
               <ts e="T18" id="Seg_269" n="e" s="T17">tʼarɨn: </ts>
               <ts e="T19" id="Seg_271" n="e" s="T18">“Mekga </ts>
               <ts e="T20" id="Seg_273" n="e" s="T19">kötdə </ts>
               <ts e="T21" id="Seg_275" n="e" s="T20">soːdʼiga </ts>
               <ts e="T22" id="Seg_277" n="e" s="T21">tattɨ, </ts>
               <ts e="T23" id="Seg_279" n="e" s="T22">ato </ts>
               <ts e="T24" id="Seg_281" n="e" s="T23">as </ts>
               <ts e="T25" id="Seg_283" n="e" s="T24">tə </ts>
               <ts e="T26" id="Seg_285" n="e" s="T25">wʼetʼätätʒan. </ts>
               <ts e="T27" id="Seg_287" n="e" s="T26">Aːwaj </ts>
               <ts e="T28" id="Seg_289" n="e" s="T27">kötdɨm </ts>
               <ts e="T29" id="Seg_291" n="e" s="T28">as </ts>
               <ts e="T30" id="Seg_293" n="e" s="T29">iǯau. </ts>
               <ts e="T31" id="Seg_295" n="e" s="T30">Qwaǯan </ts>
               <ts e="T32" id="Seg_297" n="e" s="T31">onen </ts>
               <ts e="T33" id="Seg_299" n="e" s="T32">qwalle </ts>
               <ts e="T34" id="Seg_301" n="e" s="T33">mannɨmeǯau </ts>
               <ts e="T35" id="Seg_303" n="e" s="T34">soːdʼiga </ts>
               <ts e="T36" id="Seg_305" n="e" s="T35">kötdə </ts>
               <ts e="T37" id="Seg_307" n="e" s="T36">inətʒan. </ts>
               <ts e="T38" id="Seg_309" n="e" s="T37">Awaj </ts>
               <ts e="T39" id="Seg_311" n="e" s="T38">kötdə </ts>
               <ts e="T40" id="Seg_313" n="e" s="T39">mekga </ts>
               <ts e="T41" id="Seg_315" n="e" s="T40">ne </ts>
               <ts e="T42" id="Seg_317" n="e" s="T41">nadə.” </ts>
               <ts e="T43" id="Seg_319" n="e" s="T42">Tak </ts>
               <ts e="T44" id="Seg_321" n="e" s="T43">i </ts>
               <ts e="T45" id="Seg_323" n="e" s="T44">meut </ts>
               <ts e="T46" id="Seg_325" n="e" s="T45">soːdʼiga </ts>
               <ts e="T47" id="Seg_327" n="e" s="T46">kötdɨm. </ts>
               <ts e="T48" id="Seg_329" n="e" s="T47">Tep </ts>
               <ts e="T49" id="Seg_331" n="e" s="T48">tettɨ </ts>
               <ts e="T50" id="Seg_333" n="e" s="T49">nedukkus. </ts>
               <ts e="T51" id="Seg_335" n="e" s="T50">Pajalat </ts>
               <ts e="T52" id="Seg_337" n="e" s="T51">kulʼe </ts>
               <ts e="T53" id="Seg_339" n="e" s="T52">čaʒəzattə. </ts>
               <ts e="T54" id="Seg_341" n="e" s="T53">Oqkɨrɨn </ts>
               <ts e="T55" id="Seg_343" n="e" s="T54">wʼetʼatɨs. </ts>
               <ts e="T56" id="Seg_345" n="e" s="T55">Man </ts>
               <ts e="T57" id="Seg_347" n="e" s="T56">bɨ </ts>
               <ts e="T58" id="Seg_349" n="e" s="T57">nizašto </ts>
               <ts e="T59" id="Seg_351" n="e" s="T58">as </ts>
               <ts e="T60" id="Seg_353" n="e" s="T59">meneu </ts>
               <ts e="T61" id="Seg_355" n="e" s="T60">kötdɨm. </ts>
               <ts e="T62" id="Seg_357" n="e" s="T61">Čoʒɨbla </ts>
               <ts e="T63" id="Seg_359" n="e" s="T62">awaj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_360" s="T1">PVD_1964_MyUncle_nar.001 (001.001)</ta>
            <ta e="T9" id="Seg_361" s="T6">PVD_1964_MyUncle_nar.002 (001.002)</ta>
            <ta e="T12" id="Seg_362" s="T9">PVD_1964_MyUncle_nar.003 (001.003)</ta>
            <ta e="T15" id="Seg_363" s="T12">PVD_1964_MyUncle_nar.004 (001.004)</ta>
            <ta e="T26" id="Seg_364" s="T15">PVD_1964_MyUncle_nar.005 (001.005)</ta>
            <ta e="T30" id="Seg_365" s="T26">PVD_1964_MyUncle_nar.006 (001.006)</ta>
            <ta e="T37" id="Seg_366" s="T30">PVD_1964_MyUncle_nar.007 (001.007)</ta>
            <ta e="T42" id="Seg_367" s="T37">PVD_1964_MyUncle_nar.008 (001.008)</ta>
            <ta e="T47" id="Seg_368" s="T42">PVD_1964_MyUncle_nar.009 (001.009)</ta>
            <ta e="T50" id="Seg_369" s="T47">PVD_1964_MyUncle_nar.010 (001.010)</ta>
            <ta e="T53" id="Seg_370" s="T50">PVD_1964_MyUncle_nar.011 (001.011)</ta>
            <ta e="T55" id="Seg_371" s="T53">PVD_1964_MyUncle_nar.012 (001.012)</ta>
            <ta e="T61" id="Seg_372" s="T55">PVD_1964_MyUncle_nar.013 (001.013)</ta>
            <ta e="T63" id="Seg_373" s="T61">PVD_1964_MyUncle_nar.014 (001.014)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_374" s="T1">мег̂га на′лам (′маму) а′wаw кессыт.</ta>
            <ta e="T9" id="Seg_375" s="T6">ман ′дʼӓ′дʼау̹ нʼе′дыс.</ta>
            <ta e="T12" id="Seg_376" s="T9">теп тшожимнъ ′kwаннын.</ta>
            <ta e="T15" id="Seg_377" s="T12">теб′не(ӓ) ′надъ вʼе′тʼӓттогу.</ta>
            <ta e="T26" id="Seg_378" s="T15">а ′тшожоп тʼарын: ′мекга кӧтдъ ′со̄дʼига ′татты, а то ′ас тъ ′вʼетʼӓтӓт(д̂)жан.</ta>
            <ta e="T30" id="Seg_379" s="T26">а̄′wай кӧт′дым ас ′иджау̹.</ta>
            <ta e="T37" id="Seg_380" s="T30">kwад′жан о′не̨н kwаl′lе ‵манны′меджау̹ со̄дʼига кӧт дъ ′инътжан.</ta>
            <ta e="T42" id="Seg_381" s="T37">а′wай кӧтдъ ′мекга не ′надъ.</ta>
            <ta e="T47" id="Seg_382" s="T42">так и ме′ут со̄дʼига кӧтдым.</ta>
            <ta e="T50" id="Seg_383" s="T47">те̨п ′тетты ′не̨дук‵кус.</ta>
            <ta e="T53" id="Seg_384" s="T50">′паjалат ′кулʼе ′тшажъзаттъ.</ta>
            <ta e="T55" id="Seg_385" s="T53">′оkкырын ′вʼетʼатыс.</ta>
            <ta e="T61" id="Seg_386" s="T55">ман бы низаш′то ас менеу̹ кӧтдым.</ta>
            <ta e="T63" id="Seg_387" s="T61">′тшожыбла ′аwай.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_388" s="T1">meĝga nalam (mamu) awaw kessɨt.</ta>
            <ta e="T9" id="Seg_389" s="T6">man dʼädʼau̹ nʼedɨs.</ta>
            <ta e="T12" id="Seg_390" s="T9">tep tšoʒimnə qwannɨn.</ta>
            <ta e="T15" id="Seg_391" s="T12">tebne(ä) nadə wʼetʼättogu.</ta>
            <ta e="T26" id="Seg_392" s="T15">a tšoʒop tʼarɨn: mekga kötdə soːdʼiga tattɨ, a to as tə wʼetʼätät(d̂)ʒan.</ta>
            <ta e="T30" id="Seg_393" s="T26">aːwaj kötdɨm as iǯau̹.</ta>
            <ta e="T37" id="Seg_394" s="T30">qwaǯan onen qwalle mannɨmeǯau̹ soːdʼiga köt də inətʒan.</ta>
            <ta e="T42" id="Seg_395" s="T37">awaj kötdə mekga ne nadə.</ta>
            <ta e="T47" id="Seg_396" s="T42">tak i meut soːdʼiga kötdɨm.</ta>
            <ta e="T50" id="Seg_397" s="T47">tep tettɨ nedukkus.</ta>
            <ta e="T53" id="Seg_398" s="T50">pajalat kulʼe tšaʒəzattə.</ta>
            <ta e="T55" id="Seg_399" s="T53">oqkɨrɨn wʼetʼatɨs.</ta>
            <ta e="T61" id="Seg_400" s="T55">man bɨ nizašto as meneu̹ kötdɨm.</ta>
            <ta e="T63" id="Seg_401" s="T61">tšoʒɨbla awaj.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_402" s="T1">Megga nalam (mamu) awaw kessɨt. </ta>
            <ta e="T9" id="Seg_403" s="T6">Man dʼädʼau nʼedɨs. </ta>
            <ta e="T12" id="Seg_404" s="T9">Tep čoʒimnə qwannɨn. </ta>
            <ta e="T15" id="Seg_405" s="T12">Tebne nadə wʼetʼättogu. </ta>
            <ta e="T26" id="Seg_406" s="T15">A čoʒop tʼarɨn: “Mekga kötdə soːdʼiga tattɨ, ato as tə wʼetʼätätʒan. </ta>
            <ta e="T30" id="Seg_407" s="T26">Aːwaj kötdɨm as iǯau. </ta>
            <ta e="T37" id="Seg_408" s="T30">Qwaǯan onen qwalle mannɨmeǯau soːdʼiga kötdə inətʒan. </ta>
            <ta e="T42" id="Seg_409" s="T37">Awaj kötdə mekga ne nadə.” </ta>
            <ta e="T47" id="Seg_410" s="T42">Tak i meut soːdʼiga kötdɨm. </ta>
            <ta e="T50" id="Seg_411" s="T47">Tep tettɨ nedukkus. </ta>
            <ta e="T53" id="Seg_412" s="T50">Pajalat kulʼe čaʒəzattə. </ta>
            <ta e="T55" id="Seg_413" s="T53">Oqkɨrɨn wʼetʼatɨs. </ta>
            <ta e="T61" id="Seg_414" s="T55">Man bɨ nizašto as meneu kötdɨm. </ta>
            <ta e="T63" id="Seg_415" s="T61">Čoʒɨbla awaj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_416" s="T1">megga</ta>
            <ta e="T3" id="Seg_417" s="T2">na-la-m</ta>
            <ta e="T4" id="Seg_418" s="T3">mam-u</ta>
            <ta e="T5" id="Seg_419" s="T4">awa-w</ta>
            <ta e="T6" id="Seg_420" s="T5">kes-sɨ-t</ta>
            <ta e="T7" id="Seg_421" s="T6">man</ta>
            <ta e="T8" id="Seg_422" s="T7">dʼädʼa-u</ta>
            <ta e="T9" id="Seg_423" s="T8">nʼedɨ-s</ta>
            <ta e="T10" id="Seg_424" s="T9">tep</ta>
            <ta e="T11" id="Seg_425" s="T10">čoʒim-nə</ta>
            <ta e="T12" id="Seg_426" s="T11">qwan-nɨ-n</ta>
            <ta e="T13" id="Seg_427" s="T12">teb-ne</ta>
            <ta e="T14" id="Seg_428" s="T13">nadə</ta>
            <ta e="T15" id="Seg_429" s="T14">wʼetʼät-to-gu</ta>
            <ta e="T16" id="Seg_430" s="T15">a</ta>
            <ta e="T17" id="Seg_431" s="T16">čoʒop</ta>
            <ta e="T18" id="Seg_432" s="T17">tʼarɨ-n</ta>
            <ta e="T19" id="Seg_433" s="T18">mekga</ta>
            <ta e="T20" id="Seg_434" s="T19">kötdə</ta>
            <ta e="T21" id="Seg_435" s="T20">soːdʼiga</ta>
            <ta e="T22" id="Seg_436" s="T21">tat-tɨ</ta>
            <ta e="T23" id="Seg_437" s="T22">ato</ta>
            <ta e="T24" id="Seg_438" s="T23">as</ta>
            <ta e="T25" id="Seg_439" s="T24">tə</ta>
            <ta e="T26" id="Seg_440" s="T25">wʼetʼät-ätʒa-n</ta>
            <ta e="T27" id="Seg_441" s="T26">aːwa-j</ta>
            <ta e="T28" id="Seg_442" s="T27">kötdɨ-m</ta>
            <ta e="T29" id="Seg_443" s="T28">as</ta>
            <ta e="T30" id="Seg_444" s="T29">i-ǯa-u</ta>
            <ta e="T31" id="Seg_445" s="T30">qwa-ǯa-n</ta>
            <ta e="T32" id="Seg_446" s="T31">onen</ta>
            <ta e="T33" id="Seg_447" s="T32">qwal-le</ta>
            <ta e="T34" id="Seg_448" s="T33">mannɨ-m-eǯa-u</ta>
            <ta e="T35" id="Seg_449" s="T34">soːdʼiga</ta>
            <ta e="T36" id="Seg_450" s="T35">kötdə</ta>
            <ta e="T37" id="Seg_451" s="T36">i-n-ətʒa-n</ta>
            <ta e="T38" id="Seg_452" s="T37">awa-j</ta>
            <ta e="T39" id="Seg_453" s="T38">kötdə</ta>
            <ta e="T40" id="Seg_454" s="T39">mekga</ta>
            <ta e="T41" id="Seg_455" s="T40">ne</ta>
            <ta e="T42" id="Seg_456" s="T41">nadə</ta>
            <ta e="T43" id="Seg_457" s="T42">tak</ta>
            <ta e="T44" id="Seg_458" s="T43">i</ta>
            <ta e="T45" id="Seg_459" s="T44">me-u-t</ta>
            <ta e="T46" id="Seg_460" s="T45">soːdʼiga</ta>
            <ta e="T47" id="Seg_461" s="T46">kötdɨ-m</ta>
            <ta e="T48" id="Seg_462" s="T47">tep</ta>
            <ta e="T49" id="Seg_463" s="T48">tettɨ</ta>
            <ta e="T50" id="Seg_464" s="T49">nedu-kku-s</ta>
            <ta e="T51" id="Seg_465" s="T50">paja-la-t</ta>
            <ta e="T52" id="Seg_466" s="T51">ku-lʼe</ta>
            <ta e="T53" id="Seg_467" s="T52">čaʒə-za-ttə</ta>
            <ta e="T54" id="Seg_468" s="T53">oqkɨr-ɨ-n</ta>
            <ta e="T55" id="Seg_469" s="T54">wʼetʼa-tɨ-s</ta>
            <ta e="T56" id="Seg_470" s="T55">man</ta>
            <ta e="T57" id="Seg_471" s="T56">bɨ</ta>
            <ta e="T58" id="Seg_472" s="T57">nizašto</ta>
            <ta e="T59" id="Seg_473" s="T58">as</ta>
            <ta e="T60" id="Seg_474" s="T59">me-ne-u</ta>
            <ta e="T61" id="Seg_475" s="T60">kötdɨ-m</ta>
            <ta e="T62" id="Seg_476" s="T61">čoʒɨb-la</ta>
            <ta e="T63" id="Seg_477" s="T62">awa-j</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_478" s="T1">mekka</ta>
            <ta e="T3" id="Seg_479" s="T2">na-la-m</ta>
            <ta e="T4" id="Seg_480" s="T3">mamo-ɨ</ta>
            <ta e="T5" id="Seg_481" s="T4">awa-w</ta>
            <ta e="T6" id="Seg_482" s="T5">ket-sɨ-t</ta>
            <ta e="T7" id="Seg_483" s="T6">man</ta>
            <ta e="T8" id="Seg_484" s="T7">dʼaja-w</ta>
            <ta e="T9" id="Seg_485" s="T8">nedɨ-sɨ</ta>
            <ta e="T10" id="Seg_486" s="T9">täp</ta>
            <ta e="T11" id="Seg_487" s="T10">čoʒɨp-nä</ta>
            <ta e="T12" id="Seg_488" s="T11">qwan-nɨ-n</ta>
            <ta e="T13" id="Seg_489" s="T12">täp-nä</ta>
            <ta e="T14" id="Seg_490" s="T13">nadə</ta>
            <ta e="T15" id="Seg_491" s="T14">wʼetʼät-tɨ-gu</ta>
            <ta e="T16" id="Seg_492" s="T15">a</ta>
            <ta e="T17" id="Seg_493" s="T16">čoʒɨp</ta>
            <ta e="T18" id="Seg_494" s="T17">tʼärɨ-n</ta>
            <ta e="T19" id="Seg_495" s="T18">mekka</ta>
            <ta e="T20" id="Seg_496" s="T19">kütdə</ta>
            <ta e="T21" id="Seg_497" s="T20">soːdʼiga</ta>
            <ta e="T22" id="Seg_498" s="T21">tat-etɨ</ta>
            <ta e="T23" id="Seg_499" s="T22">ato</ta>
            <ta e="T24" id="Seg_500" s="T23">asa</ta>
            <ta e="T25" id="Seg_501" s="T24">te</ta>
            <ta e="T26" id="Seg_502" s="T25">wʼetʼät-enǯɨ-ŋ</ta>
            <ta e="T27" id="Seg_503" s="T26">awa-lʼ</ta>
            <ta e="T28" id="Seg_504" s="T27">kütdə-m</ta>
            <ta e="T29" id="Seg_505" s="T28">asa</ta>
            <ta e="T30" id="Seg_506" s="T29">iː-enǯɨ-w</ta>
            <ta e="T31" id="Seg_507" s="T30">qwan-enǯɨ-ŋ</ta>
            <ta e="T32" id="Seg_508" s="T31">oneŋ</ta>
            <ta e="T33" id="Seg_509" s="T32">qwan-le</ta>
            <ta e="T34" id="Seg_510" s="T33">*mantɨ-mbɨ-enǯɨ-w</ta>
            <ta e="T35" id="Seg_511" s="T34">soːdʼiga</ta>
            <ta e="T36" id="Seg_512" s="T35">kütdə</ta>
            <ta e="T37" id="Seg_513" s="T36">iː-ntɨ-enǯɨ-ŋ</ta>
            <ta e="T38" id="Seg_514" s="T37">awa-lʼ</ta>
            <ta e="T39" id="Seg_515" s="T38">kütdə</ta>
            <ta e="T40" id="Seg_516" s="T39">mekka</ta>
            <ta e="T41" id="Seg_517" s="T40">ne</ta>
            <ta e="T42" id="Seg_518" s="T41">nadə</ta>
            <ta e="T43" id="Seg_519" s="T42">tak</ta>
            <ta e="T44" id="Seg_520" s="T43">i</ta>
            <ta e="T45" id="Seg_521" s="T44">me-nɨ-t</ta>
            <ta e="T46" id="Seg_522" s="T45">soːdʼiga</ta>
            <ta e="T47" id="Seg_523" s="T46">kütdə-m</ta>
            <ta e="T48" id="Seg_524" s="T47">täp</ta>
            <ta e="T49" id="Seg_525" s="T48">tettɨ</ta>
            <ta e="T50" id="Seg_526" s="T49">nedɨ-ku-sɨ</ta>
            <ta e="T51" id="Seg_527" s="T50">paja-la-tə</ta>
            <ta e="T52" id="Seg_528" s="T51">quː-le</ta>
            <ta e="T53" id="Seg_529" s="T52">čaǯɨ-sɨ-tɨn</ta>
            <ta e="T54" id="Seg_530" s="T53">okkɨr-ɨ-ŋ</ta>
            <ta e="T55" id="Seg_531" s="T54">wʼetʼät-tɨ-sɨ</ta>
            <ta e="T56" id="Seg_532" s="T55">man</ta>
            <ta e="T57" id="Seg_533" s="T56">bɨ</ta>
            <ta e="T58" id="Seg_534" s="T57">nizašto</ta>
            <ta e="T59" id="Seg_535" s="T58">asa</ta>
            <ta e="T60" id="Seg_536" s="T59">me-ne-w</ta>
            <ta e="T61" id="Seg_537" s="T60">kütdə-m</ta>
            <ta e="T62" id="Seg_538" s="T61">čoʒɨp-la</ta>
            <ta e="T63" id="Seg_539" s="T62">awa-lʼ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_540" s="T1">I.ALL</ta>
            <ta e="T3" id="Seg_541" s="T2">this-PL-ACC</ta>
            <ta e="T4" id="Seg_542" s="T3">mum.[NOM]-EP</ta>
            <ta e="T5" id="Seg_543" s="T4">mother.[NOM]-1SG</ta>
            <ta e="T6" id="Seg_544" s="T5">say-PST-3SG.O</ta>
            <ta e="T7" id="Seg_545" s="T6">I.NOM</ta>
            <ta e="T8" id="Seg_546" s="T7">uncle.[NOM]-1SG</ta>
            <ta e="T9" id="Seg_547" s="T8">get.married-PST.[3SG.S]</ta>
            <ta e="T10" id="Seg_548" s="T9">(s)he.[NOM]</ta>
            <ta e="T11" id="Seg_549" s="T10">priest-ALL</ta>
            <ta e="T12" id="Seg_550" s="T11">leave-CO-3SG.S</ta>
            <ta e="T13" id="Seg_551" s="T12">(s)he-ALL</ta>
            <ta e="T14" id="Seg_552" s="T13">one.should</ta>
            <ta e="T15" id="Seg_553" s="T14">get.married.in.church-TR-INF</ta>
            <ta e="T16" id="Seg_554" s="T15">and</ta>
            <ta e="T17" id="Seg_555" s="T16">priest.[NOM]</ta>
            <ta e="T18" id="Seg_556" s="T17">say-3SG.S</ta>
            <ta e="T19" id="Seg_557" s="T18">I.ALL</ta>
            <ta e="T20" id="Seg_558" s="T19">horse.[NOM]</ta>
            <ta e="T21" id="Seg_559" s="T20">good</ta>
            <ta e="T22" id="Seg_560" s="T21">give-IMP.2SG.O</ta>
            <ta e="T23" id="Seg_561" s="T22">otherwise</ta>
            <ta e="T24" id="Seg_562" s="T23">NEG</ta>
            <ta e="T25" id="Seg_563" s="T24">you.DU.NOM</ta>
            <ta e="T26" id="Seg_564" s="T25">get.married.in.church-FUT-1SG.S</ta>
            <ta e="T27" id="Seg_565" s="T26">bad-DRV</ta>
            <ta e="T28" id="Seg_566" s="T27">horse-ACC</ta>
            <ta e="T29" id="Seg_567" s="T28">NEG</ta>
            <ta e="T30" id="Seg_568" s="T29">take-FUT-1SG.O</ta>
            <ta e="T31" id="Seg_569" s="T30">leave-FUT-1SG.S</ta>
            <ta e="T32" id="Seg_570" s="T31">oneself.1SG</ta>
            <ta e="T33" id="Seg_571" s="T32">leave-CVB</ta>
            <ta e="T34" id="Seg_572" s="T33">look-DUR-FUT-1SG.O</ta>
            <ta e="T35" id="Seg_573" s="T34">good</ta>
            <ta e="T36" id="Seg_574" s="T35">horse.[NOM]</ta>
            <ta e="T37" id="Seg_575" s="T36">take-INFER-FUT-1SG.S</ta>
            <ta e="T38" id="Seg_576" s="T37">bad-ADJZ</ta>
            <ta e="T39" id="Seg_577" s="T38">horse.[NOM]</ta>
            <ta e="T40" id="Seg_578" s="T39">I.ALL</ta>
            <ta e="T41" id="Seg_579" s="T40">NEG</ta>
            <ta e="T42" id="Seg_580" s="T41">one.should</ta>
            <ta e="T43" id="Seg_581" s="T42">so</ta>
            <ta e="T44" id="Seg_582" s="T43">and</ta>
            <ta e="T45" id="Seg_583" s="T44">give-CO-3SG.O</ta>
            <ta e="T46" id="Seg_584" s="T45">good</ta>
            <ta e="T47" id="Seg_585" s="T46">horse-ACC</ta>
            <ta e="T48" id="Seg_586" s="T47">(s)he.[NOM]</ta>
            <ta e="T49" id="Seg_587" s="T48">four</ta>
            <ta e="T50" id="Seg_588" s="T49">get.married-HAB-PST.[3SG.S]</ta>
            <ta e="T51" id="Seg_589" s="T50">wife-PL.[NOM]-3SG</ta>
            <ta e="T52" id="Seg_590" s="T51">die-CVB</ta>
            <ta e="T53" id="Seg_591" s="T52">go-PST-3PL</ta>
            <ta e="T54" id="Seg_592" s="T53">one-EP-ADVZ</ta>
            <ta e="T55" id="Seg_593" s="T54">get.married.in.church-TR-PST.[3SG.S]</ta>
            <ta e="T56" id="Seg_594" s="T55">I.NOM</ta>
            <ta e="T57" id="Seg_595" s="T56">IRREAL</ta>
            <ta e="T58" id="Seg_596" s="T57">by.no.means</ta>
            <ta e="T59" id="Seg_597" s="T58">NEG</ta>
            <ta e="T60" id="Seg_598" s="T59">give-CONJ-1SG.O</ta>
            <ta e="T61" id="Seg_599" s="T60">horse-ACC</ta>
            <ta e="T62" id="Seg_600" s="T61">priest-PL.[NOM]</ta>
            <ta e="T63" id="Seg_601" s="T62">bad-DRV</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_602" s="T1">я.ALL</ta>
            <ta e="T3" id="Seg_603" s="T2">этот-PL-ACC</ta>
            <ta e="T4" id="Seg_604" s="T3">мама.[NOM]-EP</ta>
            <ta e="T5" id="Seg_605" s="T4">мать.[NOM]-1SG</ta>
            <ta e="T6" id="Seg_606" s="T5">сказать-PST-3SG.O</ta>
            <ta e="T7" id="Seg_607" s="T6">я.NOM</ta>
            <ta e="T8" id="Seg_608" s="T7">дядя.[NOM]-1SG</ta>
            <ta e="T9" id="Seg_609" s="T8">жениться-PST.[3SG.S]</ta>
            <ta e="T10" id="Seg_610" s="T9">он(а).[NOM]</ta>
            <ta e="T11" id="Seg_611" s="T10">поп-ALL</ta>
            <ta e="T12" id="Seg_612" s="T11">отправиться-CO-3SG.S</ta>
            <ta e="T13" id="Seg_613" s="T12">он(а)-ALL</ta>
            <ta e="T14" id="Seg_614" s="T13">надо</ta>
            <ta e="T15" id="Seg_615" s="T14">венчаться-TR-INF</ta>
            <ta e="T16" id="Seg_616" s="T15">а</ta>
            <ta e="T17" id="Seg_617" s="T16">поп.[NOM]</ta>
            <ta e="T18" id="Seg_618" s="T17">сказать-3SG.S</ta>
            <ta e="T19" id="Seg_619" s="T18">я.ALL</ta>
            <ta e="T20" id="Seg_620" s="T19">лошадь.[NOM]</ta>
            <ta e="T21" id="Seg_621" s="T20">хороший</ta>
            <ta e="T22" id="Seg_622" s="T21">подать-IMP.2SG.O</ta>
            <ta e="T23" id="Seg_623" s="T22">а.то</ta>
            <ta e="T24" id="Seg_624" s="T23">NEG</ta>
            <ta e="T25" id="Seg_625" s="T24">вы.DU.NOM</ta>
            <ta e="T26" id="Seg_626" s="T25">венчаться-FUT-1SG.S</ta>
            <ta e="T27" id="Seg_627" s="T26">плохой-DRV</ta>
            <ta e="T28" id="Seg_628" s="T27">лошадь-ACC</ta>
            <ta e="T29" id="Seg_629" s="T28">NEG</ta>
            <ta e="T30" id="Seg_630" s="T29">взять-FUT-1SG.O</ta>
            <ta e="T31" id="Seg_631" s="T30">отправиться-FUT-1SG.S</ta>
            <ta e="T32" id="Seg_632" s="T31">сам.1SG</ta>
            <ta e="T33" id="Seg_633" s="T32">отправиться-CVB</ta>
            <ta e="T34" id="Seg_634" s="T33">посмотреть-DUR-FUT-1SG.O</ta>
            <ta e="T35" id="Seg_635" s="T34">хороший</ta>
            <ta e="T36" id="Seg_636" s="T35">лошадь.[NOM]</ta>
            <ta e="T37" id="Seg_637" s="T36">взять-INFER-FUT-1SG.S</ta>
            <ta e="T38" id="Seg_638" s="T37">плохой-ADJZ</ta>
            <ta e="T39" id="Seg_639" s="T38">лошадь.[NOM]</ta>
            <ta e="T40" id="Seg_640" s="T39">я.ALL</ta>
            <ta e="T41" id="Seg_641" s="T40">NEG</ta>
            <ta e="T42" id="Seg_642" s="T41">надо</ta>
            <ta e="T43" id="Seg_643" s="T42">так</ta>
            <ta e="T44" id="Seg_644" s="T43">и</ta>
            <ta e="T45" id="Seg_645" s="T44">дать-CO-3SG.O</ta>
            <ta e="T46" id="Seg_646" s="T45">хороший</ta>
            <ta e="T47" id="Seg_647" s="T46">лошадь-ACC</ta>
            <ta e="T48" id="Seg_648" s="T47">он(а).[NOM]</ta>
            <ta e="T49" id="Seg_649" s="T48">четыре</ta>
            <ta e="T50" id="Seg_650" s="T49">жениться-HAB-PST.[3SG.S]</ta>
            <ta e="T51" id="Seg_651" s="T50">жена-PL.[NOM]-3SG</ta>
            <ta e="T52" id="Seg_652" s="T51">умереть-CVB</ta>
            <ta e="T53" id="Seg_653" s="T52">ходить-PST-3PL</ta>
            <ta e="T54" id="Seg_654" s="T53">один-EP-ADVZ</ta>
            <ta e="T55" id="Seg_655" s="T54">венчаться-TR-PST.[3SG.S]</ta>
            <ta e="T56" id="Seg_656" s="T55">я.NOM</ta>
            <ta e="T57" id="Seg_657" s="T56">IRREAL</ta>
            <ta e="T58" id="Seg_658" s="T57">ни.за.что</ta>
            <ta e="T59" id="Seg_659" s="T58">NEG</ta>
            <ta e="T60" id="Seg_660" s="T59">дать-CONJ-1SG.O</ta>
            <ta e="T61" id="Seg_661" s="T60">лошадь-ACC</ta>
            <ta e="T62" id="Seg_662" s="T61">поп-PL.[NOM]</ta>
            <ta e="T63" id="Seg_663" s="T62">плохой-DRV</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_664" s="T1">pers</ta>
            <ta e="T3" id="Seg_665" s="T2">dem-n:num-n:case</ta>
            <ta e="T4" id="Seg_666" s="T3">n.[n:case]-n:ins</ta>
            <ta e="T5" id="Seg_667" s="T4">n.[n:case]-n:poss</ta>
            <ta e="T6" id="Seg_668" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_669" s="T6">pers</ta>
            <ta e="T8" id="Seg_670" s="T7">n.[n:case]-n:poss</ta>
            <ta e="T9" id="Seg_671" s="T8">v-v:tense.[v:pn]</ta>
            <ta e="T10" id="Seg_672" s="T9">pers.[n:case]</ta>
            <ta e="T11" id="Seg_673" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_674" s="T11">v-v:ins-v:pn</ta>
            <ta e="T13" id="Seg_675" s="T12">pers-n:case</ta>
            <ta e="T14" id="Seg_676" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_677" s="T14">v-v&gt;v-v:inf</ta>
            <ta e="T16" id="Seg_678" s="T15">conj</ta>
            <ta e="T17" id="Seg_679" s="T16">n.[n:case]</ta>
            <ta e="T18" id="Seg_680" s="T17">v-v:pn</ta>
            <ta e="T19" id="Seg_681" s="T18">pers</ta>
            <ta e="T20" id="Seg_682" s="T19">n.[n:case]</ta>
            <ta e="T21" id="Seg_683" s="T20">adj</ta>
            <ta e="T22" id="Seg_684" s="T21">v-v:mood.pn</ta>
            <ta e="T23" id="Seg_685" s="T22">conj</ta>
            <ta e="T24" id="Seg_686" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_687" s="T24">pers</ta>
            <ta e="T26" id="Seg_688" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_689" s="T26">adj-adj&gt;adj</ta>
            <ta e="T28" id="Seg_690" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_691" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_692" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_693" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_694" s="T31">emphpro</ta>
            <ta e="T33" id="Seg_695" s="T32">v-v&gt;adv</ta>
            <ta e="T34" id="Seg_696" s="T33">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_697" s="T34">adj</ta>
            <ta e="T36" id="Seg_698" s="T35">n.[n:case]</ta>
            <ta e="T37" id="Seg_699" s="T36">v-v:mood-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_700" s="T37">adj-n&gt;adj</ta>
            <ta e="T39" id="Seg_701" s="T38">n.[n:case]</ta>
            <ta e="T40" id="Seg_702" s="T39">pers</ta>
            <ta e="T41" id="Seg_703" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_704" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_705" s="T42">adv</ta>
            <ta e="T44" id="Seg_706" s="T43">conj</ta>
            <ta e="T45" id="Seg_707" s="T44">v-v:ins-v:pn</ta>
            <ta e="T46" id="Seg_708" s="T45">adj</ta>
            <ta e="T47" id="Seg_709" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_710" s="T47">pers.[n:case]</ta>
            <ta e="T49" id="Seg_711" s="T48">num</ta>
            <ta e="T50" id="Seg_712" s="T49">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T51" id="Seg_713" s="T50">n-n:num.[n:case]-n:poss</ta>
            <ta e="T52" id="Seg_714" s="T51">v-v&gt;adv</ta>
            <ta e="T53" id="Seg_715" s="T52">v-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_716" s="T53">num-n:ins-quant&gt;adv</ta>
            <ta e="T55" id="Seg_717" s="T54">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T56" id="Seg_718" s="T55">pers</ta>
            <ta e="T57" id="Seg_719" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_720" s="T57">adv</ta>
            <ta e="T59" id="Seg_721" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_722" s="T59">v-v:mood-v:pn</ta>
            <ta e="T61" id="Seg_723" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_724" s="T61">n-n:num.[n:case]</ta>
            <ta e="T63" id="Seg_725" s="T62">adj-adj&gt;adj</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_726" s="T1">pers</ta>
            <ta e="T3" id="Seg_727" s="T2">dem</ta>
            <ta e="T4" id="Seg_728" s="T3">n</ta>
            <ta e="T5" id="Seg_729" s="T4">n</ta>
            <ta e="T6" id="Seg_730" s="T5">v</ta>
            <ta e="T7" id="Seg_731" s="T6">pers</ta>
            <ta e="T8" id="Seg_732" s="T7">n</ta>
            <ta e="T9" id="Seg_733" s="T8">v</ta>
            <ta e="T10" id="Seg_734" s="T9">pers</ta>
            <ta e="T11" id="Seg_735" s="T10">n</ta>
            <ta e="T12" id="Seg_736" s="T11">v</ta>
            <ta e="T13" id="Seg_737" s="T12">pers</ta>
            <ta e="T14" id="Seg_738" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_739" s="T14">v</ta>
            <ta e="T16" id="Seg_740" s="T15">conj</ta>
            <ta e="T17" id="Seg_741" s="T16">n</ta>
            <ta e="T18" id="Seg_742" s="T17">v</ta>
            <ta e="T19" id="Seg_743" s="T18">pers</ta>
            <ta e="T20" id="Seg_744" s="T19">n</ta>
            <ta e="T21" id="Seg_745" s="T20">adj</ta>
            <ta e="T22" id="Seg_746" s="T21">v</ta>
            <ta e="T23" id="Seg_747" s="T22">conj</ta>
            <ta e="T24" id="Seg_748" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_749" s="T24">pers</ta>
            <ta e="T26" id="Seg_750" s="T25">v</ta>
            <ta e="T27" id="Seg_751" s="T26">adj</ta>
            <ta e="T28" id="Seg_752" s="T27">n</ta>
            <ta e="T29" id="Seg_753" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_754" s="T29">v</ta>
            <ta e="T31" id="Seg_755" s="T30">v</ta>
            <ta e="T32" id="Seg_756" s="T31">emphpro</ta>
            <ta e="T33" id="Seg_757" s="T32">adv</ta>
            <ta e="T34" id="Seg_758" s="T33">v</ta>
            <ta e="T35" id="Seg_759" s="T34">adj</ta>
            <ta e="T36" id="Seg_760" s="T35">n</ta>
            <ta e="T37" id="Seg_761" s="T36">v</ta>
            <ta e="T38" id="Seg_762" s="T37">adj</ta>
            <ta e="T39" id="Seg_763" s="T38">n</ta>
            <ta e="T40" id="Seg_764" s="T39">pers</ta>
            <ta e="T41" id="Seg_765" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_766" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_767" s="T42">adv</ta>
            <ta e="T44" id="Seg_768" s="T43">conj</ta>
            <ta e="T45" id="Seg_769" s="T44">v</ta>
            <ta e="T46" id="Seg_770" s="T45">adj</ta>
            <ta e="T47" id="Seg_771" s="T46">n</ta>
            <ta e="T48" id="Seg_772" s="T47">pers</ta>
            <ta e="T49" id="Seg_773" s="T48">num</ta>
            <ta e="T50" id="Seg_774" s="T49">v</ta>
            <ta e="T51" id="Seg_775" s="T50">n</ta>
            <ta e="T52" id="Seg_776" s="T51">adv</ta>
            <ta e="T53" id="Seg_777" s="T52">v</ta>
            <ta e="T54" id="Seg_778" s="T53">adv</ta>
            <ta e="T55" id="Seg_779" s="T54">v</ta>
            <ta e="T56" id="Seg_780" s="T55">pers</ta>
            <ta e="T57" id="Seg_781" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_782" s="T57">pro</ta>
            <ta e="T59" id="Seg_783" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_784" s="T59">v</ta>
            <ta e="T61" id="Seg_785" s="T60">n</ta>
            <ta e="T62" id="Seg_786" s="T61">n</ta>
            <ta e="T63" id="Seg_787" s="T62">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_788" s="T1">pro.h:R</ta>
            <ta e="T3" id="Seg_789" s="T2">pro:Th</ta>
            <ta e="T5" id="Seg_790" s="T4">np.h:A 0.1.h:Poss</ta>
            <ta e="T7" id="Seg_791" s="T6">pro.h:Poss</ta>
            <ta e="T8" id="Seg_792" s="T7">np.h:A</ta>
            <ta e="T10" id="Seg_793" s="T9">pro.h:A</ta>
            <ta e="T11" id="Seg_794" s="T10">np.h:G</ta>
            <ta e="T13" id="Seg_795" s="T12">np.h:G</ta>
            <ta e="T15" id="Seg_796" s="T14">v:Th</ta>
            <ta e="T17" id="Seg_797" s="T16">np.h:A</ta>
            <ta e="T19" id="Seg_798" s="T18">pro.h:R</ta>
            <ta e="T20" id="Seg_799" s="T19">np:Th</ta>
            <ta e="T22" id="Seg_800" s="T21">0.2.h:A</ta>
            <ta e="T25" id="Seg_801" s="T24">pro.h:Th</ta>
            <ta e="T26" id="Seg_802" s="T25">0.1.h:A</ta>
            <ta e="T28" id="Seg_803" s="T27">np:Th</ta>
            <ta e="T30" id="Seg_804" s="T29">0.1.h:A</ta>
            <ta e="T31" id="Seg_805" s="T30">0.1.h:A</ta>
            <ta e="T34" id="Seg_806" s="T33">0.1.h:A</ta>
            <ta e="T36" id="Seg_807" s="T35">np:Th</ta>
            <ta e="T37" id="Seg_808" s="T36">0.1.h:A</ta>
            <ta e="T39" id="Seg_809" s="T38">np:Th</ta>
            <ta e="T45" id="Seg_810" s="T44">0.3.h:A</ta>
            <ta e="T47" id="Seg_811" s="T46">np:Th</ta>
            <ta e="T48" id="Seg_812" s="T47">pro.h:A</ta>
            <ta e="T51" id="Seg_813" s="T50">np.h:P 0.3.h:Poss</ta>
            <ta e="T55" id="Seg_814" s="T54">0.3.h:A</ta>
            <ta e="T56" id="Seg_815" s="T55">pro.h:A</ta>
            <ta e="T61" id="Seg_816" s="T60">np:Th</ta>
            <ta e="T62" id="Seg_817" s="T61">np.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_818" s="T2">pro:O</ta>
            <ta e="T5" id="Seg_819" s="T4">np.h:S</ta>
            <ta e="T6" id="Seg_820" s="T5">v:pred</ta>
            <ta e="T8" id="Seg_821" s="T7">np.h:S</ta>
            <ta e="T9" id="Seg_822" s="T8">v:pred</ta>
            <ta e="T10" id="Seg_823" s="T9">pro.h:S</ta>
            <ta e="T12" id="Seg_824" s="T11">v:pred</ta>
            <ta e="T14" id="Seg_825" s="T13">ptcl:pred</ta>
            <ta e="T15" id="Seg_826" s="T14">v:O</ta>
            <ta e="T17" id="Seg_827" s="T16">np.h:S</ta>
            <ta e="T18" id="Seg_828" s="T17">v:pred</ta>
            <ta e="T20" id="Seg_829" s="T19">np:O</ta>
            <ta e="T22" id="Seg_830" s="T21">0.2.h:S v:pred</ta>
            <ta e="T25" id="Seg_831" s="T24">pro.h:O</ta>
            <ta e="T26" id="Seg_832" s="T25">0.1.h:S v:pred</ta>
            <ta e="T28" id="Seg_833" s="T27">np:O</ta>
            <ta e="T30" id="Seg_834" s="T29">0.1.h:S v:pred</ta>
            <ta e="T31" id="Seg_835" s="T30">0.1.h:S v:pred</ta>
            <ta e="T33" id="Seg_836" s="T32">s:adv</ta>
            <ta e="T34" id="Seg_837" s="T33">0.1.h:S v:pred</ta>
            <ta e="T36" id="Seg_838" s="T35">np:O</ta>
            <ta e="T37" id="Seg_839" s="T36">0.1.h:S v:pred</ta>
            <ta e="T39" id="Seg_840" s="T38">np:O</ta>
            <ta e="T42" id="Seg_841" s="T41">ptcl:pred</ta>
            <ta e="T45" id="Seg_842" s="T44">0.3.h:S v:pred</ta>
            <ta e="T47" id="Seg_843" s="T46">np:O</ta>
            <ta e="T48" id="Seg_844" s="T47">pro.h:S</ta>
            <ta e="T50" id="Seg_845" s="T49">v:pred</ta>
            <ta e="T51" id="Seg_846" s="T50">np.h:S</ta>
            <ta e="T53" id="Seg_847" s="T52">v:pred</ta>
            <ta e="T55" id="Seg_848" s="T54">0.3.h:S v:pred</ta>
            <ta e="T56" id="Seg_849" s="T55">pro.h:S</ta>
            <ta e="T60" id="Seg_850" s="T59">v:pred</ta>
            <ta e="T61" id="Seg_851" s="T60">np:O</ta>
            <ta e="T62" id="Seg_852" s="T61">np.h:S</ta>
            <ta e="T63" id="Seg_853" s="T62">adj:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_854" s="T3">RUS:core</ta>
            <ta e="T8" id="Seg_855" s="T7">RUS:cult</ta>
            <ta e="T14" id="Seg_856" s="T13">RUS:mod</ta>
            <ta e="T15" id="Seg_857" s="T14">RUS:cult</ta>
            <ta e="T16" id="Seg_858" s="T15">RUS:gram</ta>
            <ta e="T23" id="Seg_859" s="T22">RUS:gram</ta>
            <ta e="T26" id="Seg_860" s="T25">RUS:cult</ta>
            <ta e="T41" id="Seg_861" s="T40">RUS:gram</ta>
            <ta e="T42" id="Seg_862" s="T41">RUS:mod</ta>
            <ta e="T43" id="Seg_863" s="T42">RUS:core</ta>
            <ta e="T44" id="Seg_864" s="T43">RUS:gram</ta>
            <ta e="T55" id="Seg_865" s="T54">RUS:cult</ta>
            <ta e="T57" id="Seg_866" s="T56">RUS:gram</ta>
            <ta e="T58" id="Seg_867" s="T57">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_868" s="T1">My mother told me about it.</ta>
            <ta e="T9" id="Seg_869" s="T6">My uncle got married.</ta>
            <ta e="T12" id="Seg_870" s="T9">He went to a priest.</ta>
            <ta e="T15" id="Seg_871" s="T12">One should [go to him to] get married (in church).</ta>
            <ta e="T26" id="Seg_872" s="T15">And the priest said: “Give me a good horse, otherwise I won't marry you.</ta>
            <ta e="T30" id="Seg_873" s="T26">I won't take a bad horse.</ta>
            <ta e="T37" id="Seg_874" s="T30">I'll go and choose a horse, I'll take a good horse.</ta>
            <ta e="T42" id="Seg_875" s="T37">I don't need bad horse.”</ta>
            <ta e="T47" id="Seg_876" s="T42">So [my uncle] gave him a good horse.</ta>
            <ta e="T50" id="Seg_877" s="T47">He was married four times.</ta>
            <ta e="T53" id="Seg_878" s="T50">His wives had been dying.</ta>
            <ta e="T55" id="Seg_879" s="T53">Once he got married in a church.</ta>
            <ta e="T61" id="Seg_880" s="T55">I would have never given a horse.</ta>
            <ta e="T63" id="Seg_881" s="T61">Priests are bad.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_882" s="T1">Meine Mutter erzählte mir davon.</ta>
            <ta e="T9" id="Seg_883" s="T6">Mein Onkel wollte heiraten.</ta>
            <ta e="T12" id="Seg_884" s="T9">Er ging zu einem Priester.</ta>
            <ta e="T15" id="Seg_885" s="T12">Man muss [zu ihm gehen um] in der Kirche zu heiraten.</ta>
            <ta e="T26" id="Seg_886" s="T15">Und der Priester sagte: "Gib mir ein gutes Pferd, sonst werde ich euch nicht trauen.</ta>
            <ta e="T30" id="Seg_887" s="T26">Ich werde kein schlechtes Pferd annehmen.</ta>
            <ta e="T37" id="Seg_888" s="T30">Ich werde gehen und ein Pferd wählen, ich werde ein gutes Pferd nehmen.</ta>
            <ta e="T42" id="Seg_889" s="T37">Ich brauche kein schlechtes Pferd."</ta>
            <ta e="T47" id="Seg_890" s="T42">Also gab [mein Onkel] ihm ein gutes Pferd.</ta>
            <ta e="T50" id="Seg_891" s="T47">Er war viermal verheiratet.</ta>
            <ta e="T53" id="Seg_892" s="T50">Seine Frauen sind (alle) gestorben.</ta>
            <ta e="T55" id="Seg_893" s="T53">Einmal heiratete er in der Kirche.</ta>
            <ta e="T61" id="Seg_894" s="T55">Ich hätte ihm nie ein Pferd gegeben.</ta>
            <ta e="T63" id="Seg_895" s="T61">Priester sind schlecht.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_896" s="T1">Это мне мама рассказывала.</ta>
            <ta e="T9" id="Seg_897" s="T6">Мой дядя женился.</ta>
            <ta e="T12" id="Seg_898" s="T9">Он пошел к попу.</ta>
            <ta e="T15" id="Seg_899" s="T12">К нему надо венчаться.</ta>
            <ta e="T26" id="Seg_900" s="T15">А поп сказал: “Мне хорошего коня давай, а то не буду вас венчать.</ta>
            <ta e="T30" id="Seg_901" s="T26">Плохую лошадь я не возьму.</ta>
            <ta e="T37" id="Seg_902" s="T30">Пойду сам и посмотрю коня, хорошего коня возьму.</ta>
            <ta e="T42" id="Seg_903" s="T37">Плохой лошади мне не надо”.</ta>
            <ta e="T47" id="Seg_904" s="T42">Так и отдал (мой дядя) хорошего коня.</ta>
            <ta e="T50" id="Seg_905" s="T47">Он четыре раза был женатый.</ta>
            <ta e="T53" id="Seg_906" s="T50">Жены умирали.</ta>
            <ta e="T55" id="Seg_907" s="T53">Один раз венчался.</ta>
            <ta e="T61" id="Seg_908" s="T55">Я бы ни за что не отдала бы коня.</ta>
            <ta e="T63" id="Seg_909" s="T61">Попы плохие.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_910" s="T1">это мне мама рассказывала</ta>
            <ta e="T9" id="Seg_911" s="T6">мой дядя женился</ta>
            <ta e="T12" id="Seg_912" s="T9">он пошел к попу</ta>
            <ta e="T15" id="Seg_913" s="T12">к нему надо венчаться</ta>
            <ta e="T26" id="Seg_914" s="T15">а поп сказал мне хорошего коня давай (за венчание) а то не буду венчать</ta>
            <ta e="T30" id="Seg_915" s="T26">плохую лошадь я не возьму</ta>
            <ta e="T37" id="Seg_916" s="T30">пойду сам и посмотрю коня хорошего коня возьму</ta>
            <ta e="T42" id="Seg_917" s="T37">плохой лошади мне не надо</ta>
            <ta e="T47" id="Seg_918" s="T42">так и отдал (мой дядя) хорошего коня</ta>
            <ta e="T50" id="Seg_919" s="T47">он четыре раза был женатый</ta>
            <ta e="T53" id="Seg_920" s="T50">жены умирали</ta>
            <ta e="T55" id="Seg_921" s="T53">один раз венчался</ta>
            <ta e="T61" id="Seg_922" s="T55">я бы ни за что не отдала бы коня</ta>
            <ta e="T63" id="Seg_923" s="T61">попы плохие</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T15" id="Seg_924" s="T12">[KuAI:] Variant: 'tebnä'.</ta>
            <ta e="T26" id="Seg_925" s="T15">[KuAI:] Variant: 'wʼetʼätädʒan'. </ta>
            <ta e="T37" id="Seg_926" s="T30">[BrM:] 'köt də' changed to 'kötdə'. [BrM:] IPFV or INFER?</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T6" id="Seg_927" s="T1">мы говорим ′мамо, а раньше звали а′wаw̹. это давно было</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
