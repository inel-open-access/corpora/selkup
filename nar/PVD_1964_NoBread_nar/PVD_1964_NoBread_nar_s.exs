<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_NoBread_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_NoBread_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">46</ud-information>
            <ud-information attribute-name="# HIAT:w">35</ud-information>
            <ud-information attribute-name="# e">35</ud-information>
            <ud-information attribute-name="# HIAT:u">7</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T36" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Telʼdʼän</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">menan</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">nʼaj</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">tʼäkgus</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Anglʼina</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">Iwanɨwna</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">pačoɣɨm</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">nʼüut</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_32" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">A</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">natʼen</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">ɨlogottə</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">nʼäjla</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">sədə</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_49" n="HIAT:w" s="T14">nʼäja</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_52" n="HIAT:w" s="T15">äpɨmɨt</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_56" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">Täp</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">ɨloɣotdɨ</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_64" n="HIAT:w" s="T18">iːut</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_68" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">Teblam</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_73" n="HIAT:w" s="T20">stol</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">barott</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">pennɨt</ts>
                  <nts id="Seg_80" n="HIAT:ip">,</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_83" n="HIAT:w" s="T23">mekga</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_86" n="HIAT:w" s="T24">tʼärɨn</ts>
                  <nts id="Seg_87" n="HIAT:ip">:</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_89" n="HIAT:ip">“</nts>
                  <ts e="T26" id="Seg_91" n="HIAT:w" s="T25">A</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_94" n="HIAT:w" s="T26">tau</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_97" n="HIAT:w" s="T27">nʼäjla</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip">”</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_102" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">A</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">man</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">jass</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">qodǯirbou</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_117" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">Man</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">tolʼko</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">paroɣotdɨ</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">mannɨpɨzan</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T36" id="Seg_131" n="sc" s="T1">
               <ts e="T2" id="Seg_133" n="e" s="T1">Telʼdʼän </ts>
               <ts e="T3" id="Seg_135" n="e" s="T2">menan </ts>
               <ts e="T4" id="Seg_137" n="e" s="T3">nʼaj </ts>
               <ts e="T5" id="Seg_139" n="e" s="T4">tʼäkgus. </ts>
               <ts e="T6" id="Seg_141" n="e" s="T5">Anglʼina </ts>
               <ts e="T7" id="Seg_143" n="e" s="T6">Iwanɨwna </ts>
               <ts e="T8" id="Seg_145" n="e" s="T7">pačoɣɨm </ts>
               <ts e="T9" id="Seg_147" n="e" s="T8">nʼüut. </ts>
               <ts e="T10" id="Seg_149" n="e" s="T9">A </ts>
               <ts e="T11" id="Seg_151" n="e" s="T10">natʼen </ts>
               <ts e="T12" id="Seg_153" n="e" s="T11">ɨlogottə </ts>
               <ts e="T13" id="Seg_155" n="e" s="T12">nʼäjla </ts>
               <ts e="T14" id="Seg_157" n="e" s="T13">sədə </ts>
               <ts e="T15" id="Seg_159" n="e" s="T14">nʼäja </ts>
               <ts e="T16" id="Seg_161" n="e" s="T15">äpɨmɨt. </ts>
               <ts e="T17" id="Seg_163" n="e" s="T16">Täp </ts>
               <ts e="T18" id="Seg_165" n="e" s="T17">ɨloɣotdɨ </ts>
               <ts e="T19" id="Seg_167" n="e" s="T18">iːut. </ts>
               <ts e="T20" id="Seg_169" n="e" s="T19">Teblam </ts>
               <ts e="T21" id="Seg_171" n="e" s="T20">stol </ts>
               <ts e="T22" id="Seg_173" n="e" s="T21">barott </ts>
               <ts e="T23" id="Seg_175" n="e" s="T22">pennɨt, </ts>
               <ts e="T24" id="Seg_177" n="e" s="T23">mekga </ts>
               <ts e="T25" id="Seg_179" n="e" s="T24">tʼärɨn: </ts>
               <ts e="T26" id="Seg_181" n="e" s="T25">“A </ts>
               <ts e="T27" id="Seg_183" n="e" s="T26">tau </ts>
               <ts e="T28" id="Seg_185" n="e" s="T27">nʼäjla.” </ts>
               <ts e="T29" id="Seg_187" n="e" s="T28">A </ts>
               <ts e="T30" id="Seg_189" n="e" s="T29">man </ts>
               <ts e="T31" id="Seg_191" n="e" s="T30">jass </ts>
               <ts e="T32" id="Seg_193" n="e" s="T31">qodǯirbou. </ts>
               <ts e="T33" id="Seg_195" n="e" s="T32">Man </ts>
               <ts e="T34" id="Seg_197" n="e" s="T33">tolʼko </ts>
               <ts e="T35" id="Seg_199" n="e" s="T34">paroɣotdɨ </ts>
               <ts e="T36" id="Seg_201" n="e" s="T35">mannɨpɨzan. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_202" s="T1">PVD_1964_NoBread_nar.001 (001.001)</ta>
            <ta e="T9" id="Seg_203" s="T5">PVD_1964_NoBread_nar.002 (001.002)</ta>
            <ta e="T16" id="Seg_204" s="T9">PVD_1964_NoBread_nar.003 (001.003)</ta>
            <ta e="T19" id="Seg_205" s="T16">PVD_1964_NoBread_nar.004 (001.004)</ta>
            <ta e="T28" id="Seg_206" s="T19">PVD_1964_NoBread_nar.005 (001.005)</ta>
            <ta e="T32" id="Seg_207" s="T28">PVD_1964_NoBread_nar.006 (001.006)</ta>
            <ta e="T36" id="Seg_208" s="T32">PVD_1964_NoBread_nar.007 (001.007)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_209" s="T1">′телʼдʼӓн ме′нан ′нʼай ′тʼӓкг̂ус.</ta>
            <ta e="T9" id="Seg_210" s="T5">Англʼина Иванывна па′тшоɣым ′нʼӱут.</ta>
            <ta e="T16" id="Seg_211" s="T9">а на′тʼен ′ыlоготтъ ′нʼӓйла съдъ ′нʼӓjа ′ӓпымыт.</ta>
            <ta e="T19" id="Seg_212" s="T16">тӓп ′ыlоɣотды ′ӣут.</ta>
            <ta e="T28" id="Seg_213" s="T19">теб′лам стол ′баротт ′пенныт, ′мекга тʼӓ′рын: а ′тау ′нʼӓйла.</ta>
            <ta e="T32" id="Seg_214" s="T28">а ман jасс ′kодджир‵боу.</ta>
            <ta e="T36" id="Seg_215" s="T32">ман толʼко ′пароɣотды ′манныпы′зан.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_216" s="T1">telʼdʼän menan nʼaj tʼäkĝus.</ta>
            <ta e="T9" id="Seg_217" s="T5">Аnglʼina Iwanɨwna pačoɣɨm nʼüut.</ta>
            <ta e="T16" id="Seg_218" s="T9">a natʼen ɨlogottə nʼäjla sədə nʼäja äpɨmɨt.</ta>
            <ta e="T19" id="Seg_219" s="T16">täp ɨloɣotdɨ iːut.</ta>
            <ta e="T28" id="Seg_220" s="T19">teblam stol barott pennɨt, mekga tʼärɨn: a tau nʼäjla.</ta>
            <ta e="T32" id="Seg_221" s="T28">a man jass qodǯirbou.</ta>
            <ta e="T36" id="Seg_222" s="T32">man tolʼko paroɣotdɨ mannɨpɨzan.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_223" s="T1">Telʼdʼän menan nʼaj tʼäkgus. </ta>
            <ta e="T9" id="Seg_224" s="T5">Anglʼina Iwanɨwna pačoɣɨm nʼüut. </ta>
            <ta e="T16" id="Seg_225" s="T9">A natʼen ɨlogottə nʼäjla sədə nʼäja äpɨmɨt. </ta>
            <ta e="T19" id="Seg_226" s="T16">Täp ɨloɣotdɨ iːut. </ta>
            <ta e="T28" id="Seg_227" s="T19">Teblam stol barott pennɨt, mekga tʼärɨn: “A tau nʼäjla.” </ta>
            <ta e="T32" id="Seg_228" s="T28">A man jass qodǯirbou. </ta>
            <ta e="T36" id="Seg_229" s="T32">Man tolʼko paroɣotdɨ mannɨpɨzan. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_230" s="T1">telʼdʼän</ta>
            <ta e="T3" id="Seg_231" s="T2">me-nan</ta>
            <ta e="T4" id="Seg_232" s="T3">nʼaːj</ta>
            <ta e="T5" id="Seg_233" s="T4">tʼäkgu-s</ta>
            <ta e="T6" id="Seg_234" s="T5">Anglʼina</ta>
            <ta e="T7" id="Seg_235" s="T6">Iwanɨwna</ta>
            <ta e="T8" id="Seg_236" s="T7">pačoɣ-ɨ-m</ta>
            <ta e="T9" id="Seg_237" s="T8">nʼüu-t</ta>
            <ta e="T10" id="Seg_238" s="T9">a</ta>
            <ta e="T11" id="Seg_239" s="T10">natʼe-n</ta>
            <ta e="T12" id="Seg_240" s="T11">ɨl-o-gottə</ta>
            <ta e="T13" id="Seg_241" s="T12">nʼäj-la</ta>
            <ta e="T14" id="Seg_242" s="T13">sədə</ta>
            <ta e="T15" id="Seg_243" s="T14">nʼäja</ta>
            <ta e="T16" id="Seg_244" s="T15">ä-pɨ-mɨ-t</ta>
            <ta e="T17" id="Seg_245" s="T16">täp</ta>
            <ta e="T18" id="Seg_246" s="T17">ɨl-o-ɣotdɨ</ta>
            <ta e="T19" id="Seg_247" s="T18">iː-u-t</ta>
            <ta e="T20" id="Seg_248" s="T19">teb-la-m</ta>
            <ta e="T21" id="Seg_249" s="T20">stol</ta>
            <ta e="T22" id="Seg_250" s="T21">bar-o-tt</ta>
            <ta e="T23" id="Seg_251" s="T22">pen-nɨ-t</ta>
            <ta e="T24" id="Seg_252" s="T23">mekga</ta>
            <ta e="T25" id="Seg_253" s="T24">tʼärɨ-n</ta>
            <ta e="T26" id="Seg_254" s="T25">a</ta>
            <ta e="T27" id="Seg_255" s="T26">tau</ta>
            <ta e="T28" id="Seg_256" s="T27">nʼäj-la</ta>
            <ta e="T29" id="Seg_257" s="T28">a</ta>
            <ta e="T30" id="Seg_258" s="T29">man</ta>
            <ta e="T31" id="Seg_259" s="T30">jass</ta>
            <ta e="T32" id="Seg_260" s="T31">qo-dǯir-bo-u</ta>
            <ta e="T33" id="Seg_261" s="T32">man</ta>
            <ta e="T34" id="Seg_262" s="T33">tolʼko</ta>
            <ta e="T35" id="Seg_263" s="T34">par-o-ɣotdɨ</ta>
            <ta e="T36" id="Seg_264" s="T35">mannɨ-pɨ-za-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_265" s="T1">telʼdʼan</ta>
            <ta e="T3" id="Seg_266" s="T2">me-nan</ta>
            <ta e="T4" id="Seg_267" s="T3">nʼäj</ta>
            <ta e="T5" id="Seg_268" s="T4">tʼäkku-sɨ</ta>
            <ta e="T6" id="Seg_269" s="T5">Angelina</ta>
            <ta e="T7" id="Seg_270" s="T6">Iwanɨwna</ta>
            <ta e="T8" id="Seg_271" s="T7">pačoɣ-ɨ-m</ta>
            <ta e="T9" id="Seg_272" s="T8">nʼuː-t</ta>
            <ta e="T10" id="Seg_273" s="T9">a</ta>
            <ta e="T11" id="Seg_274" s="T10">*natʼe-n</ta>
            <ta e="T12" id="Seg_275" s="T11">ɨl-ɨ-qɨntɨ</ta>
            <ta e="T13" id="Seg_276" s="T12">nʼäj-la</ta>
            <ta e="T14" id="Seg_277" s="T13">sədə</ta>
            <ta e="T15" id="Seg_278" s="T14">nʼäj</ta>
            <ta e="T16" id="Seg_279" s="T15">eː-mbɨ-mbɨ-tɨn</ta>
            <ta e="T17" id="Seg_280" s="T16">täp</ta>
            <ta e="T18" id="Seg_281" s="T17">ɨl-ɨ-qɨntɨ</ta>
            <ta e="T19" id="Seg_282" s="T18">iː-nɨ-t</ta>
            <ta e="T20" id="Seg_283" s="T19">täp-la-m</ta>
            <ta e="T21" id="Seg_284" s="T20">istol</ta>
            <ta e="T22" id="Seg_285" s="T21">par-ɨ-ntə</ta>
            <ta e="T23" id="Seg_286" s="T22">pen-nɨ-t</ta>
            <ta e="T24" id="Seg_287" s="T23">mekka</ta>
            <ta e="T25" id="Seg_288" s="T24">tʼärɨ-n</ta>
            <ta e="T26" id="Seg_289" s="T25">a</ta>
            <ta e="T27" id="Seg_290" s="T26">taw</ta>
            <ta e="T28" id="Seg_291" s="T27">nʼäj-la</ta>
            <ta e="T29" id="Seg_292" s="T28">a</ta>
            <ta e="T30" id="Seg_293" s="T29">man</ta>
            <ta e="T31" id="Seg_294" s="T30">asa</ta>
            <ta e="T32" id="Seg_295" s="T31">qo-nǯir-mbɨ-w</ta>
            <ta e="T33" id="Seg_296" s="T32">man</ta>
            <ta e="T34" id="Seg_297" s="T33">tolʼko</ta>
            <ta e="T35" id="Seg_298" s="T34">par-ɨ-qɨntɨ</ta>
            <ta e="T36" id="Seg_299" s="T35">*mantɨ-mbɨ-sɨ-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_300" s="T1">yesterday</ta>
            <ta e="T3" id="Seg_301" s="T2">we-ADES</ta>
            <ta e="T4" id="Seg_302" s="T3">bread.[NOM]</ta>
            <ta e="T5" id="Seg_303" s="T4">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T6" id="Seg_304" s="T5">Angelina.[NOM]</ta>
            <ta e="T7" id="Seg_305" s="T6">Ivanovna.[NOM]</ta>
            <ta e="T8" id="Seg_306" s="T7">fanny-EP-ACC</ta>
            <ta e="T9" id="Seg_307" s="T8">open-3SG.O</ta>
            <ta e="T10" id="Seg_308" s="T9">and</ta>
            <ta e="T11" id="Seg_309" s="T10">there-ADV.LOC</ta>
            <ta e="T12" id="Seg_310" s="T11">bottom-EP-LOC.3SG</ta>
            <ta e="T13" id="Seg_311" s="T12">bread-PL.[NOM]</ta>
            <ta e="T14" id="Seg_312" s="T13">two</ta>
            <ta e="T15" id="Seg_313" s="T14">bread.[NOM]</ta>
            <ta e="T16" id="Seg_314" s="T15">be-DUR-PST.NAR-3PL</ta>
            <ta e="T17" id="Seg_315" s="T16">(s)he.[NOM]</ta>
            <ta e="T18" id="Seg_316" s="T17">space.under-EP-EL.3SG</ta>
            <ta e="T19" id="Seg_317" s="T18">take-CO-3SG.O</ta>
            <ta e="T20" id="Seg_318" s="T19">(s)he-PL-ACC</ta>
            <ta e="T21" id="Seg_319" s="T20">table.[NOM]</ta>
            <ta e="T22" id="Seg_320" s="T21">top-EP-ILL</ta>
            <ta e="T23" id="Seg_321" s="T22">put-CO-3SG.O</ta>
            <ta e="T24" id="Seg_322" s="T23">I.ALL</ta>
            <ta e="T25" id="Seg_323" s="T24">say-3SG.S</ta>
            <ta e="T26" id="Seg_324" s="T25">and</ta>
            <ta e="T27" id="Seg_325" s="T26">this.[NOM]</ta>
            <ta e="T28" id="Seg_326" s="T27">bread-PL.[NOM]</ta>
            <ta e="T29" id="Seg_327" s="T28">and</ta>
            <ta e="T30" id="Seg_328" s="T29">I.NOM</ta>
            <ta e="T31" id="Seg_329" s="T30">NEG</ta>
            <ta e="T32" id="Seg_330" s="T31">see-DRV-PST.NAR-1SG.O</ta>
            <ta e="T33" id="Seg_331" s="T32">I.NOM</ta>
            <ta e="T34" id="Seg_332" s="T33">only</ta>
            <ta e="T35" id="Seg_333" s="T34">top-EP-LOC.3SG</ta>
            <ta e="T36" id="Seg_334" s="T35">look-DUR-PST-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_335" s="T1">вчера</ta>
            <ta e="T3" id="Seg_336" s="T2">мы-ADES</ta>
            <ta e="T4" id="Seg_337" s="T3">хлеб.[NOM]</ta>
            <ta e="T5" id="Seg_338" s="T4">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T6" id="Seg_339" s="T5">Ангелина.[NOM]</ta>
            <ta e="T7" id="Seg_340" s="T6">Ивановна.[NOM]</ta>
            <ta e="T8" id="Seg_341" s="T7">бачок-EP-ACC</ta>
            <ta e="T9" id="Seg_342" s="T8">открыть-3SG.O</ta>
            <ta e="T10" id="Seg_343" s="T9">а</ta>
            <ta e="T11" id="Seg_344" s="T10">туда-ADV.LOC</ta>
            <ta e="T12" id="Seg_345" s="T11">дно-EP-LOC.3SG</ta>
            <ta e="T13" id="Seg_346" s="T12">хлеб-PL.[NOM]</ta>
            <ta e="T14" id="Seg_347" s="T13">два</ta>
            <ta e="T15" id="Seg_348" s="T14">хлеб.[NOM]</ta>
            <ta e="T16" id="Seg_349" s="T15">быть-DUR-PST.NAR-3PL</ta>
            <ta e="T17" id="Seg_350" s="T16">он(а).[NOM]</ta>
            <ta e="T18" id="Seg_351" s="T17">пространство.под-EP-EL.3SG</ta>
            <ta e="T19" id="Seg_352" s="T18">взять-CO-3SG.O</ta>
            <ta e="T20" id="Seg_353" s="T19">он(а)-PL-ACC</ta>
            <ta e="T21" id="Seg_354" s="T20">стол.[NOM]</ta>
            <ta e="T22" id="Seg_355" s="T21">верхняя.часть-EP-ILL</ta>
            <ta e="T23" id="Seg_356" s="T22">положить-CO-3SG.O</ta>
            <ta e="T24" id="Seg_357" s="T23">я.ALL</ta>
            <ta e="T25" id="Seg_358" s="T24">сказать-3SG.S</ta>
            <ta e="T26" id="Seg_359" s="T25">а</ta>
            <ta e="T27" id="Seg_360" s="T26">этот.[NOM]</ta>
            <ta e="T28" id="Seg_361" s="T27">хлеб-PL.[NOM]</ta>
            <ta e="T29" id="Seg_362" s="T28">а</ta>
            <ta e="T30" id="Seg_363" s="T29">я.NOM</ta>
            <ta e="T31" id="Seg_364" s="T30">NEG</ta>
            <ta e="T32" id="Seg_365" s="T31">увидеть-DRV-PST.NAR-1SG.O</ta>
            <ta e="T33" id="Seg_366" s="T32">я.NOM</ta>
            <ta e="T34" id="Seg_367" s="T33">только</ta>
            <ta e="T35" id="Seg_368" s="T34">верхняя.часть-EP-LOC.3SG</ta>
            <ta e="T36" id="Seg_369" s="T35">посмотреть-DUR-PST-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_370" s="T1">adv</ta>
            <ta e="T3" id="Seg_371" s="T2">pers-n:case</ta>
            <ta e="T4" id="Seg_372" s="T3">n.[n:case]</ta>
            <ta e="T5" id="Seg_373" s="T4">v-v:tense.[v:pn]</ta>
            <ta e="T6" id="Seg_374" s="T5">nprop.[n:case]</ta>
            <ta e="T7" id="Seg_375" s="T6">nprop.[n:case]</ta>
            <ta e="T8" id="Seg_376" s="T7">n-n:ins-n:case</ta>
            <ta e="T9" id="Seg_377" s="T8">v-v:pn</ta>
            <ta e="T10" id="Seg_378" s="T9">conj</ta>
            <ta e="T11" id="Seg_379" s="T10">adv-adv:case</ta>
            <ta e="T12" id="Seg_380" s="T11">n-n:ins-n:case.poss</ta>
            <ta e="T13" id="Seg_381" s="T12">n-n:num.[n:case]</ta>
            <ta e="T14" id="Seg_382" s="T13">num</ta>
            <ta e="T15" id="Seg_383" s="T14">n.[n:case]</ta>
            <ta e="T16" id="Seg_384" s="T15">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_385" s="T16">pers.[n:case]</ta>
            <ta e="T18" id="Seg_386" s="T17">n-n:ins-n:case.poss</ta>
            <ta e="T19" id="Seg_387" s="T18">v-v:ins-v:pn</ta>
            <ta e="T20" id="Seg_388" s="T19">pers-n:num-n:case</ta>
            <ta e="T21" id="Seg_389" s="T20">n.[n:case]</ta>
            <ta e="T22" id="Seg_390" s="T21">n-n:ins-n:case</ta>
            <ta e="T23" id="Seg_391" s="T22">v-v:ins-v:pn</ta>
            <ta e="T24" id="Seg_392" s="T23">pers</ta>
            <ta e="T25" id="Seg_393" s="T24">v-v:pn</ta>
            <ta e="T26" id="Seg_394" s="T25">conj</ta>
            <ta e="T27" id="Seg_395" s="T26">dem.[n:case]</ta>
            <ta e="T28" id="Seg_396" s="T27">n-n:num.[n:case]</ta>
            <ta e="T29" id="Seg_397" s="T28">conj</ta>
            <ta e="T30" id="Seg_398" s="T29">pers</ta>
            <ta e="T31" id="Seg_399" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_400" s="T31">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_401" s="T32">pers</ta>
            <ta e="T34" id="Seg_402" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_403" s="T34">n-n:ins-n:case.poss</ta>
            <ta e="T36" id="Seg_404" s="T35">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_405" s="T1">adv</ta>
            <ta e="T3" id="Seg_406" s="T2">pers</ta>
            <ta e="T4" id="Seg_407" s="T3">n</ta>
            <ta e="T5" id="Seg_408" s="T4">v</ta>
            <ta e="T6" id="Seg_409" s="T5">nprop</ta>
            <ta e="T7" id="Seg_410" s="T6">nprop</ta>
            <ta e="T8" id="Seg_411" s="T7">n</ta>
            <ta e="T9" id="Seg_412" s="T8">v</ta>
            <ta e="T10" id="Seg_413" s="T9">conj</ta>
            <ta e="T11" id="Seg_414" s="T10">adv</ta>
            <ta e="T12" id="Seg_415" s="T11">n</ta>
            <ta e="T13" id="Seg_416" s="T12">n</ta>
            <ta e="T14" id="Seg_417" s="T13">num</ta>
            <ta e="T15" id="Seg_418" s="T14">n</ta>
            <ta e="T16" id="Seg_419" s="T15">v</ta>
            <ta e="T17" id="Seg_420" s="T16">pers</ta>
            <ta e="T18" id="Seg_421" s="T17">n</ta>
            <ta e="T19" id="Seg_422" s="T18">v</ta>
            <ta e="T20" id="Seg_423" s="T19">pers</ta>
            <ta e="T21" id="Seg_424" s="T20">n</ta>
            <ta e="T22" id="Seg_425" s="T21">n</ta>
            <ta e="T23" id="Seg_426" s="T22">v</ta>
            <ta e="T24" id="Seg_427" s="T23">pers</ta>
            <ta e="T25" id="Seg_428" s="T24">v</ta>
            <ta e="T26" id="Seg_429" s="T25">conj</ta>
            <ta e="T27" id="Seg_430" s="T26">dem</ta>
            <ta e="T28" id="Seg_431" s="T27">n</ta>
            <ta e="T29" id="Seg_432" s="T28">conj</ta>
            <ta e="T30" id="Seg_433" s="T29">pers</ta>
            <ta e="T31" id="Seg_434" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_435" s="T31">v</ta>
            <ta e="T33" id="Seg_436" s="T32">pers</ta>
            <ta e="T34" id="Seg_437" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_438" s="T34">n</ta>
            <ta e="T36" id="Seg_439" s="T35">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_440" s="T1">adv:Time</ta>
            <ta e="T3" id="Seg_441" s="T2">pro.h:Poss</ta>
            <ta e="T4" id="Seg_442" s="T3">np:Th</ta>
            <ta e="T6" id="Seg_443" s="T5">np.h:A</ta>
            <ta e="T8" id="Seg_444" s="T7">np:Th</ta>
            <ta e="T12" id="Seg_445" s="T11">np:L</ta>
            <ta e="T13" id="Seg_446" s="T12">np:Th</ta>
            <ta e="T17" id="Seg_447" s="T16">pro.h:A</ta>
            <ta e="T18" id="Seg_448" s="T17">np:So</ta>
            <ta e="T19" id="Seg_449" s="T18">0.3:Th</ta>
            <ta e="T20" id="Seg_450" s="T19">pro:Th</ta>
            <ta e="T22" id="Seg_451" s="T21">np:G</ta>
            <ta e="T23" id="Seg_452" s="T22">0.3.h:A</ta>
            <ta e="T24" id="Seg_453" s="T23">pro.h:R</ta>
            <ta e="T25" id="Seg_454" s="T24">0.3.h:A</ta>
            <ta e="T28" id="Seg_455" s="T27">np:Th</ta>
            <ta e="T30" id="Seg_456" s="T29">pro.h:E</ta>
            <ta e="T32" id="Seg_457" s="T31">0.3:Th</ta>
            <ta e="T33" id="Seg_458" s="T32">pro.h:A</ta>
            <ta e="T35" id="Seg_459" s="T34">np:L</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_460" s="T3">np:S</ta>
            <ta e="T5" id="Seg_461" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_462" s="T5">np.h:S</ta>
            <ta e="T8" id="Seg_463" s="T7">np:O</ta>
            <ta e="T9" id="Seg_464" s="T8">v:pred</ta>
            <ta e="T13" id="Seg_465" s="T12">np:S</ta>
            <ta e="T16" id="Seg_466" s="T15">v:pred</ta>
            <ta e="T17" id="Seg_467" s="T16">pro.h:S</ta>
            <ta e="T19" id="Seg_468" s="T18">v:pred 0.3:O</ta>
            <ta e="T20" id="Seg_469" s="T19">pro:O</ta>
            <ta e="T23" id="Seg_470" s="T22">0.3.h:S v:pred</ta>
            <ta e="T25" id="Seg_471" s="T24">0.3.h:S v:pred</ta>
            <ta e="T28" id="Seg_472" s="T27">np:S</ta>
            <ta e="T30" id="Seg_473" s="T29">pro.h:S</ta>
            <ta e="T32" id="Seg_474" s="T31">v:pred 0.3:O</ta>
            <ta e="T33" id="Seg_475" s="T32">pro.h:S</ta>
            <ta e="T36" id="Seg_476" s="T35">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T8" id="Seg_477" s="T7">RUS:cult</ta>
            <ta e="T10" id="Seg_478" s="T9">RUS:gram</ta>
            <ta e="T21" id="Seg_479" s="T20">RUS:cult</ta>
            <ta e="T22" id="Seg_480" s="T21">WNB Noun or pp</ta>
            <ta e="T26" id="Seg_481" s="T25">RUS:gram</ta>
            <ta e="T29" id="Seg_482" s="T28">RUS:gram</ta>
            <ta e="T34" id="Seg_483" s="T33">RUS:disc</ta>
            <ta e="T35" id="Seg_484" s="T34">WNB Noun or pp</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_485" s="T1">Yesterday we had no bread.</ta>
            <ta e="T9" id="Seg_486" s="T5">Angelina Ivanovna opened the fanny.</ta>
            <ta e="T16" id="Seg_487" s="T9">And there were two pieces of bread on the bottom.</ta>
            <ta e="T19" id="Seg_488" s="T16">She took them from beneath.</ta>
            <ta e="T28" id="Seg_489" s="T19">She put it on the table and said to me: “Here is bread.”</ta>
            <ta e="T32" id="Seg_490" s="T28">And I didn't notice it.</ta>
            <ta e="T36" id="Seg_491" s="T32">I only looked above.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_492" s="T1">Gestern hatten wir kein Brot.</ta>
            <ta e="T9" id="Seg_493" s="T5">Angelina Ivanovna öffnete die Dose.</ta>
            <ta e="T16" id="Seg_494" s="T9">Und da waren zwei Stück (Scheiben?) Brot am Boden.</ta>
            <ta e="T19" id="Seg_495" s="T16">Sie holte sie von unten.</ta>
            <ta e="T28" id="Seg_496" s="T19">Sie legte es auf den Tisch und sagte zu mir: "Hier ist Brot."</ta>
            <ta e="T32" id="Seg_497" s="T28">Und ich sah es nicht.</ta>
            <ta e="T36" id="Seg_498" s="T32">Ich sah nur nach oben.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_499" s="T1">Вчера у нас хлеба не было.</ta>
            <ta e="T9" id="Seg_500" s="T5">Ангелина Ивановна бачок открыла.</ta>
            <ta e="T16" id="Seg_501" s="T9">А там на дне хлеб два хлеба было.</ta>
            <ta e="T19" id="Seg_502" s="T16">Она из-под низа взяла.</ta>
            <ta e="T28" id="Seg_503" s="T19">Его на стол положила, мне говорит: “А вот хлеб”.</ta>
            <ta e="T32" id="Seg_504" s="T28">А я не видела.</ta>
            <ta e="T36" id="Seg_505" s="T32">Я только вверху посмотрела.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_506" s="T1">вчера у нас хлеба не было</ta>
            <ta e="T9" id="Seg_507" s="T5">бачок открыла</ta>
            <ta e="T16" id="Seg_508" s="T9">там на дне хлеб два хлеба было</ta>
            <ta e="T19" id="Seg_509" s="T16">она из-под низа взяла</ta>
            <ta e="T28" id="Seg_510" s="T19">она на стол положила мне говорит вот хлеб</ta>
            <ta e="T32" id="Seg_511" s="T28">я не видела</ta>
            <ta e="T36" id="Seg_512" s="T32">я только вверху посмотрела</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
