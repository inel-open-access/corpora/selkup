<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_NoReindeers_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_NoReindeers_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">14</ud-information>
            <ud-information attribute-name="# HIAT:w">11</ud-information>
            <ud-information attribute-name="# e">11</ud-information>
            <ud-information attribute-name="# HIAT:u">3</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T12" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Qum</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">nawerna</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">sarɨbat</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">aːtʼäm</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Man</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">as</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">qotǯirbou</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">aːtʼalam</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_32" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">Menan</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">tʼäguz</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">atta</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T12" id="Seg_43" n="sc" s="T1">
               <ts e="T2" id="Seg_45" n="e" s="T1">Qum </ts>
               <ts e="T3" id="Seg_47" n="e" s="T2">nawerna </ts>
               <ts e="T4" id="Seg_49" n="e" s="T3">sarɨbat </ts>
               <ts e="T5" id="Seg_51" n="e" s="T4">aːtʼäm. </ts>
               <ts e="T6" id="Seg_53" n="e" s="T5">Man </ts>
               <ts e="T7" id="Seg_55" n="e" s="T6">as </ts>
               <ts e="T8" id="Seg_57" n="e" s="T7">qotǯirbou </ts>
               <ts e="T9" id="Seg_59" n="e" s="T8">aːtʼalam. </ts>
               <ts e="T10" id="Seg_61" n="e" s="T9">Menan </ts>
               <ts e="T11" id="Seg_63" n="e" s="T10">tʼäguz </ts>
               <ts e="T12" id="Seg_65" n="e" s="T11">atta. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_66" s="T1">PVD_1964_NoReindeers_nar.001 (001.001)</ta>
            <ta e="T9" id="Seg_67" s="T5">PVD_1964_NoReindeers_nar.002 (001.002)</ta>
            <ta e="T12" id="Seg_68" s="T9">PVD_1964_NoReindeers_nar.003 (001.003)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_69" s="T1">kум на′вернʌ ′сарыбат ′а̄тʼӓм.</ta>
            <ta e="T9" id="Seg_70" s="T5">ман ас ′kотджирбоу̹ а̄тʼалам.</ta>
            <ta e="T12" id="Seg_71" s="T9">ме′нан тʼӓг̂уз ′атта.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_72" s="T1">qum nawernʌ sarɨbat aːtʼäm.</ta>
            <ta e="T9" id="Seg_73" s="T5">man as qotǯirbou̹ aːtʼalam.</ta>
            <ta e="T12" id="Seg_74" s="T9">menan tʼäĝuz atta.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_75" s="T1">Qum nawerna sarɨbat aːtʼäm. </ta>
            <ta e="T9" id="Seg_76" s="T5">Man as qotǯirbou aːtʼalam. </ta>
            <ta e="T12" id="Seg_77" s="T9">Menan tʼäguz atta. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_78" s="T1">qum</ta>
            <ta e="T3" id="Seg_79" s="T2">nawerna</ta>
            <ta e="T4" id="Seg_80" s="T3">sarɨ-ba-t</ta>
            <ta e="T5" id="Seg_81" s="T4">aːtʼä-m</ta>
            <ta e="T6" id="Seg_82" s="T5">man</ta>
            <ta e="T7" id="Seg_83" s="T6">as</ta>
            <ta e="T8" id="Seg_84" s="T7">qo-tǯir-bo-u</ta>
            <ta e="T9" id="Seg_85" s="T8">aːtʼa-la-m</ta>
            <ta e="T10" id="Seg_86" s="T9">me-nan</ta>
            <ta e="T11" id="Seg_87" s="T10">tʼägu-z</ta>
            <ta e="T12" id="Seg_88" s="T11">atta</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_89" s="T1">qum</ta>
            <ta e="T3" id="Seg_90" s="T2">nawerna</ta>
            <ta e="T4" id="Seg_91" s="T3">saːrə-mbɨ-t</ta>
            <ta e="T5" id="Seg_92" s="T4">aːta-m</ta>
            <ta e="T6" id="Seg_93" s="T5">man</ta>
            <ta e="T7" id="Seg_94" s="T6">asa</ta>
            <ta e="T8" id="Seg_95" s="T7">qo-nǯir-mbɨ-w</ta>
            <ta e="T9" id="Seg_96" s="T8">aːta-la-m</ta>
            <ta e="T10" id="Seg_97" s="T9">me-nan</ta>
            <ta e="T11" id="Seg_98" s="T10">tʼäkku-sɨ</ta>
            <ta e="T12" id="Seg_99" s="T11">aːta</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_100" s="T1">human.being.[NOM]</ta>
            <ta e="T3" id="Seg_101" s="T2">probably</ta>
            <ta e="T4" id="Seg_102" s="T3">bind-PST.NAR-3SG.O</ta>
            <ta e="T5" id="Seg_103" s="T4">reindeer-ACC</ta>
            <ta e="T6" id="Seg_104" s="T5">I.NOM</ta>
            <ta e="T7" id="Seg_105" s="T6">NEG</ta>
            <ta e="T8" id="Seg_106" s="T7">see-DRV-PST.NAR-1SG.O</ta>
            <ta e="T9" id="Seg_107" s="T8">reindeer-PL-ACC</ta>
            <ta e="T10" id="Seg_108" s="T9">we-ADES</ta>
            <ta e="T11" id="Seg_109" s="T10">NEG.EX-PST.[NOM]</ta>
            <ta e="T12" id="Seg_110" s="T11">reindeer.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_111" s="T1">человек.[NOM]</ta>
            <ta e="T3" id="Seg_112" s="T2">наверное</ta>
            <ta e="T4" id="Seg_113" s="T3">привязать-PST.NAR-3SG.O</ta>
            <ta e="T5" id="Seg_114" s="T4">олень-ACC</ta>
            <ta e="T6" id="Seg_115" s="T5">я.NOM</ta>
            <ta e="T7" id="Seg_116" s="T6">NEG</ta>
            <ta e="T8" id="Seg_117" s="T7">увидеть-DRV-PST.NAR-1SG.O</ta>
            <ta e="T9" id="Seg_118" s="T8">олень-PL-ACC</ta>
            <ta e="T10" id="Seg_119" s="T9">мы-ADES</ta>
            <ta e="T11" id="Seg_120" s="T10">NEG.EX-PST.[NOM]</ta>
            <ta e="T12" id="Seg_121" s="T11">олень.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_122" s="T1">n.[n:case]</ta>
            <ta e="T3" id="Seg_123" s="T2">adv</ta>
            <ta e="T4" id="Seg_124" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_125" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_126" s="T5">pers</ta>
            <ta e="T7" id="Seg_127" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_128" s="T7">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_129" s="T8">n-n:num-n:case</ta>
            <ta e="T10" id="Seg_130" s="T9">pers-n:case</ta>
            <ta e="T11" id="Seg_131" s="T10">v-v:tense.[n:case]</ta>
            <ta e="T12" id="Seg_132" s="T11">n.[n:case]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_133" s="T1">n</ta>
            <ta e="T3" id="Seg_134" s="T2">adv</ta>
            <ta e="T4" id="Seg_135" s="T3">v</ta>
            <ta e="T5" id="Seg_136" s="T4">n</ta>
            <ta e="T6" id="Seg_137" s="T5">pers</ta>
            <ta e="T7" id="Seg_138" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_139" s="T7">v</ta>
            <ta e="T9" id="Seg_140" s="T8">n</ta>
            <ta e="T10" id="Seg_141" s="T9">pers</ta>
            <ta e="T11" id="Seg_142" s="T10">v</ta>
            <ta e="T12" id="Seg_143" s="T11">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_144" s="T1">np.h:A</ta>
            <ta e="T5" id="Seg_145" s="T4">np:Th</ta>
            <ta e="T6" id="Seg_146" s="T5">pro.h:E</ta>
            <ta e="T9" id="Seg_147" s="T8">np:Th</ta>
            <ta e="T10" id="Seg_148" s="T9">pro.h:Poss</ta>
            <ta e="T12" id="Seg_149" s="T11">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_150" s="T1">np.h:S</ta>
            <ta e="T4" id="Seg_151" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_152" s="T4">np:O</ta>
            <ta e="T6" id="Seg_153" s="T5">pro.h:S</ta>
            <ta e="T8" id="Seg_154" s="T7">v:pred</ta>
            <ta e="T9" id="Seg_155" s="T8">np:O</ta>
            <ta e="T11" id="Seg_156" s="T10">v:pred</ta>
            <ta e="T12" id="Seg_157" s="T11">np:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_158" s="T2">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_159" s="T1">A man probably tied a reindeer.</ta>
            <ta e="T9" id="Seg_160" s="T5">I never saw reindeer.</ta>
            <ta e="T12" id="Seg_161" s="T9">We had no reindeer.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_162" s="T1">Ein Mann hat vielleicht ein Rentier angebunden.</ta>
            <ta e="T9" id="Seg_163" s="T5">Ich habe nie Rentiere gesehen.</ta>
            <ta e="T12" id="Seg_164" s="T9">Wir hatten keine Rentiere.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_165" s="T1">Человек, наверное, привязал оленя.</ta>
            <ta e="T9" id="Seg_166" s="T5">Я не видала оленей.</ta>
            <ta e="T12" id="Seg_167" s="T9">У нас не было оленей.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_168" s="T1">человек наверно привязал оленя</ta>
            <ta e="T9" id="Seg_169" s="T5">я не видала оленей</ta>
            <ta e="T12" id="Seg_170" s="T9">у нас не было оленей</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
