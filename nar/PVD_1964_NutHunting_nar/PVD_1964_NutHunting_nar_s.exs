<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_NutHunting_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_NutHunting_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">36</ud-information>
            <ud-information attribute-name="# HIAT:w">28</ud-information>
            <ud-information attribute-name="# e">28</ud-information>
            <ud-information attribute-name="# HIAT:u">7</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T29" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">qardʼzʼen</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">madʼödta</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">qwaǯan</ts>
                  <nts id="Seg_14" n="HIAT:ip">,</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_17" n="HIAT:w" s="T5">soŋgolʼe</ts>
                  <nts id="Seg_18" n="HIAT:ip">.</nts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_21" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">Qwaǯan</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">songolʼe</ts>
                  <nts id="Seg_27" n="HIAT:ip">.</nts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_30" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">Soŋa</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">putʼöːm</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">tʼelɨpba</ts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_42" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">Man</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">nagur</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">koʒam</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">meǯau</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">i</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">jedotdə</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">tʼüːtʒan</ts>
                  <nts id="Seg_63" n="HIAT:ip">.</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_66" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">I</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">man</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">merɨnneǯau</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_78" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">Manan</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">qomdeu</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">koːcin</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">jeǯin</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_93" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">Me</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">tätdɨn</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">taweǯaftə</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">soːngam</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T29" id="Seg_107" n="sc" s="T1">
               <ts e="T2" id="Seg_109" n="e" s="T1">Man </ts>
               <ts e="T3" id="Seg_111" n="e" s="T2">qardʼzʼen </ts>
               <ts e="T4" id="Seg_113" n="e" s="T3">madʼödta </ts>
               <ts e="T5" id="Seg_115" n="e" s="T4">qwaǯan, </ts>
               <ts e="T6" id="Seg_117" n="e" s="T5">soŋgolʼe. </ts>
               <ts e="T7" id="Seg_119" n="e" s="T6">Qwaǯan </ts>
               <ts e="T8" id="Seg_121" n="e" s="T7">songolʼe. </ts>
               <ts e="T9" id="Seg_123" n="e" s="T8">Soŋa </ts>
               <ts e="T10" id="Seg_125" n="e" s="T9">putʼöːm </ts>
               <ts e="T11" id="Seg_127" n="e" s="T10">tʼelɨpba. </ts>
               <ts e="T12" id="Seg_129" n="e" s="T11">Man </ts>
               <ts e="T13" id="Seg_131" n="e" s="T12">nagur </ts>
               <ts e="T14" id="Seg_133" n="e" s="T13">koʒam </ts>
               <ts e="T15" id="Seg_135" n="e" s="T14">meǯau </ts>
               <ts e="T16" id="Seg_137" n="e" s="T15">i </ts>
               <ts e="T17" id="Seg_139" n="e" s="T16">jedotdə </ts>
               <ts e="T18" id="Seg_141" n="e" s="T17">tʼüːtʒan. </ts>
               <ts e="T19" id="Seg_143" n="e" s="T18">I </ts>
               <ts e="T20" id="Seg_145" n="e" s="T19">man </ts>
               <ts e="T21" id="Seg_147" n="e" s="T20">merɨnneǯau. </ts>
               <ts e="T22" id="Seg_149" n="e" s="T21">Manan </ts>
               <ts e="T23" id="Seg_151" n="e" s="T22">qomdeu </ts>
               <ts e="T24" id="Seg_153" n="e" s="T23">koːcin </ts>
               <ts e="T25" id="Seg_155" n="e" s="T24">jeǯin. </ts>
               <ts e="T26" id="Seg_157" n="e" s="T25">Me </ts>
               <ts e="T27" id="Seg_159" n="e" s="T26">tätdɨn </ts>
               <ts e="T28" id="Seg_161" n="e" s="T27">taweǯaftə </ts>
               <ts e="T29" id="Seg_163" n="e" s="T28">soːngam. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_164" s="T1">PVD_1964_NutHunting_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_165" s="T6">PVD_1964_NutHunting_nar.002 (001.002)</ta>
            <ta e="T11" id="Seg_166" s="T8">PVD_1964_NutHunting_nar.003 (001.003)</ta>
            <ta e="T18" id="Seg_167" s="T11">PVD_1964_NutHunting_nar.004 (001.004)</ta>
            <ta e="T21" id="Seg_168" s="T18">PVD_1964_NutHunting_nar.005 (001.005)</ta>
            <ta e="T25" id="Seg_169" s="T21">PVD_1964_NutHunting_nar.006 (001.006)</ta>
            <ta e="T29" id="Seg_170" s="T25">PVD_1964_NutHunting_nar.007 (001.007)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_171" s="T1">ман kар′дʼзʼен ма′дʼӧд̂та kwад′жан, ′соңголʼе.</ta>
            <ta e="T8" id="Seg_172" s="T6">kwад′жан ′сонголʼе.</ta>
            <ta e="T11" id="Seg_173" s="T8">′соңа путʼӧ̄м ′тʼ(д̂ʼ)елыпб̂а.</ta>
            <ta e="T18" id="Seg_174" s="T11">ман ′нагур ко′жам ′меджау̹ и ′jедотдъ ′тʼӱ̄тжан.</ta>
            <ta e="T21" id="Seg_175" s="T18">и ман ′мерын′не̨джау̹.</ta>
            <ta e="T25" id="Seg_176" s="T21">ма′нан kом′де̨у̹ ко̄цин jеджин.</ta>
            <ta e="T29" id="Seg_177" s="T25">ме тӓт′дын та′wед̂жафтъ со̄нk(г)ам.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_178" s="T1">man qardʼzʼen madʼöd̂ta qwaǯan, soŋgolʼe.</ta>
            <ta e="T8" id="Seg_179" s="T6">qwaǯan songolʼe.</ta>
            <ta e="T11" id="Seg_180" s="T8">soŋa putʼöːm tʼ(d̂ʼ)elɨpb̂a.</ta>
            <ta e="T18" id="Seg_181" s="T11">man nagur koʒam meǯau̹ i jedotdə tʼüːtʒan.</ta>
            <ta e="T21" id="Seg_182" s="T18">i man merɨnneǯau̹.</ta>
            <ta e="T25" id="Seg_183" s="T21">manan qomdeu̹ koːcin jeǯin.</ta>
            <ta e="T29" id="Seg_184" s="T25">me tätdɨn taweǯaftə soːnq(g)am.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_185" s="T1">Man qardʼzʼen madʼödta qwaǯan, soŋgolʼe. </ta>
            <ta e="T8" id="Seg_186" s="T6">Qwaǯan songolʼe. </ta>
            <ta e="T11" id="Seg_187" s="T8">Soŋa putʼöːm tʼelɨpba. </ta>
            <ta e="T18" id="Seg_188" s="T11">Man nagur koʒam meǯau i jedotdə tʼüːtʒan. </ta>
            <ta e="T21" id="Seg_189" s="T18">I man merɨnneǯau. </ta>
            <ta e="T25" id="Seg_190" s="T21">Manan qomdeu koːcin jeǯin. </ta>
            <ta e="T29" id="Seg_191" s="T25">Me tätdɨn taweǯaftə soːngam. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_192" s="T1">man</ta>
            <ta e="T3" id="Seg_193" s="T2">qardʼzʼe-n</ta>
            <ta e="T4" id="Seg_194" s="T3">madʼ-ö-dta</ta>
            <ta e="T5" id="Seg_195" s="T4">qwa-ǯa-n</ta>
            <ta e="T6" id="Seg_196" s="T5">soŋgo-lʼe</ta>
            <ta e="T7" id="Seg_197" s="T6">qwa-ǯa-n</ta>
            <ta e="T8" id="Seg_198" s="T7">songo-lʼe</ta>
            <ta e="T9" id="Seg_199" s="T8">soŋa</ta>
            <ta e="T10" id="Seg_200" s="T9">putʼöːm</ta>
            <ta e="T11" id="Seg_201" s="T10">tʼelɨp-ba</ta>
            <ta e="T12" id="Seg_202" s="T11">man</ta>
            <ta e="T13" id="Seg_203" s="T12">nagur</ta>
            <ta e="T14" id="Seg_204" s="T13">koʒa-m</ta>
            <ta e="T15" id="Seg_205" s="T14">me-ǯa-u</ta>
            <ta e="T16" id="Seg_206" s="T15">i</ta>
            <ta e="T17" id="Seg_207" s="T16">jedo-tdə</ta>
            <ta e="T18" id="Seg_208" s="T17">tʼüː-tʒa-n</ta>
            <ta e="T19" id="Seg_209" s="T18">i</ta>
            <ta e="T20" id="Seg_210" s="T19">man</ta>
            <ta e="T21" id="Seg_211" s="T20">merɨn-n-eǯa-u</ta>
            <ta e="T22" id="Seg_212" s="T21">ma-nan</ta>
            <ta e="T23" id="Seg_213" s="T22">qomde-u</ta>
            <ta e="T24" id="Seg_214" s="T23">koːci-n</ta>
            <ta e="T25" id="Seg_215" s="T24">je-ǯi-n</ta>
            <ta e="T26" id="Seg_216" s="T25">me</ta>
            <ta e="T27" id="Seg_217" s="T26">tätdɨ-n</ta>
            <ta e="T28" id="Seg_218" s="T27">taw-eǯa-ftə</ta>
            <ta e="T29" id="Seg_219" s="T28">soːnga-m</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_220" s="T1">man</ta>
            <ta e="T3" id="Seg_221" s="T2">qardʼe-n</ta>
            <ta e="T4" id="Seg_222" s="T3">madʼ-ɨ-ntə</ta>
            <ta e="T5" id="Seg_223" s="T4">qwan-enǯɨ-ŋ</ta>
            <ta e="T6" id="Seg_224" s="T5">soːnga-le</ta>
            <ta e="T7" id="Seg_225" s="T6">qwan-enǯɨ-ŋ</ta>
            <ta e="T8" id="Seg_226" s="T7">soːnga-le</ta>
            <ta e="T9" id="Seg_227" s="T8">soːnga</ta>
            <ta e="T10" id="Seg_228" s="T9">putʼom</ta>
            <ta e="T11" id="Seg_229" s="T10">tʼelɨm-mbɨ</ta>
            <ta e="T12" id="Seg_230" s="T11">man</ta>
            <ta e="T13" id="Seg_231" s="T12">nagur</ta>
            <ta e="T14" id="Seg_232" s="T13">koʒa-m</ta>
            <ta e="T15" id="Seg_233" s="T14">meː-enǯɨ-w</ta>
            <ta e="T16" id="Seg_234" s="T15">i</ta>
            <ta e="T17" id="Seg_235" s="T16">eːde-ntə</ta>
            <ta e="T18" id="Seg_236" s="T17">tüː-enǯɨ-ŋ</ta>
            <ta e="T19" id="Seg_237" s="T18">i</ta>
            <ta e="T20" id="Seg_238" s="T19">man</ta>
            <ta e="T21" id="Seg_239" s="T20">merɨŋ-tɨ-enǯɨ-w</ta>
            <ta e="T22" id="Seg_240" s="T21">man-nan</ta>
            <ta e="T23" id="Seg_241" s="T22">qomdɛ-nɨ</ta>
            <ta e="T24" id="Seg_242" s="T23">koːci-ŋ</ta>
            <ta e="T25" id="Seg_243" s="T24">eː-enǯɨ-n</ta>
            <ta e="T26" id="Seg_244" s="T25">me</ta>
            <ta e="T27" id="Seg_245" s="T26">tɨtʼa-n</ta>
            <ta e="T28" id="Seg_246" s="T27">taw-enǯɨ-un</ta>
            <ta e="T29" id="Seg_247" s="T28">soːnga-m</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_248" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_249" s="T2">tomorrow-ADV.LOC</ta>
            <ta e="T4" id="Seg_250" s="T3">taiga-EP-ILL</ta>
            <ta e="T5" id="Seg_251" s="T4">leave-FUT-1SG.S</ta>
            <ta e="T6" id="Seg_252" s="T5">gather.nuts-CVB</ta>
            <ta e="T7" id="Seg_253" s="T6">leave-FUT-1SG.S</ta>
            <ta e="T8" id="Seg_254" s="T7">gather.nuts-CVB</ta>
            <ta e="T9" id="Seg_255" s="T8">nut.[NOM]</ta>
            <ta e="T10" id="Seg_256" s="T9">well</ta>
            <ta e="T11" id="Seg_257" s="T10">give.birth-PST.NAR.[3SG.S]</ta>
            <ta e="T12" id="Seg_258" s="T11">I.NOM</ta>
            <ta e="T13" id="Seg_259" s="T12">three</ta>
            <ta e="T14" id="Seg_260" s="T13">bag-ACC</ta>
            <ta e="T15" id="Seg_261" s="T14">do-FUT-1SG.O</ta>
            <ta e="T16" id="Seg_262" s="T15">and</ta>
            <ta e="T17" id="Seg_263" s="T16">village-ILL</ta>
            <ta e="T18" id="Seg_264" s="T17">come-FUT-1SG.S</ta>
            <ta e="T19" id="Seg_265" s="T18">and</ta>
            <ta e="T20" id="Seg_266" s="T19">I.NOM</ta>
            <ta e="T21" id="Seg_267" s="T20">sell-TR-FUT-1SG.O</ta>
            <ta e="T22" id="Seg_268" s="T21">I-ADES</ta>
            <ta e="T23" id="Seg_269" s="T22">money.[NOM]-1SG</ta>
            <ta e="T24" id="Seg_270" s="T23">much-ADVZ</ta>
            <ta e="T25" id="Seg_271" s="T24">be-FUT-3SG.S</ta>
            <ta e="T26" id="Seg_272" s="T25">we.[NOM]</ta>
            <ta e="T27" id="Seg_273" s="T26">here-ADV.LOC</ta>
            <ta e="T28" id="Seg_274" s="T27">buy-FUT-1PL</ta>
            <ta e="T29" id="Seg_275" s="T28">nut-ACC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_276" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_277" s="T2">завтра-ADV.LOC</ta>
            <ta e="T4" id="Seg_278" s="T3">тайга-EP-ILL</ta>
            <ta e="T5" id="Seg_279" s="T4">отправиться-FUT-1SG.S</ta>
            <ta e="T6" id="Seg_280" s="T5">собирать.орехи-CVB</ta>
            <ta e="T7" id="Seg_281" s="T6">отправиться-FUT-1SG.S</ta>
            <ta e="T8" id="Seg_282" s="T7">собирать.орехи-CVB</ta>
            <ta e="T9" id="Seg_283" s="T8">орех.[NOM]</ta>
            <ta e="T10" id="Seg_284" s="T9">путём</ta>
            <ta e="T11" id="Seg_285" s="T10">родить-PST.NAR.[3SG.S]</ta>
            <ta e="T12" id="Seg_286" s="T11">я.NOM</ta>
            <ta e="T13" id="Seg_287" s="T12">три</ta>
            <ta e="T14" id="Seg_288" s="T13">мешок-ACC</ta>
            <ta e="T15" id="Seg_289" s="T14">сделать-FUT-1SG.O</ta>
            <ta e="T16" id="Seg_290" s="T15">и</ta>
            <ta e="T17" id="Seg_291" s="T16">деревня-ILL</ta>
            <ta e="T18" id="Seg_292" s="T17">приехать-FUT-1SG.S</ta>
            <ta e="T19" id="Seg_293" s="T18">и</ta>
            <ta e="T20" id="Seg_294" s="T19">я.NOM</ta>
            <ta e="T21" id="Seg_295" s="T20">продать-TR-FUT-1SG.O</ta>
            <ta e="T22" id="Seg_296" s="T21">я-ADES</ta>
            <ta e="T23" id="Seg_297" s="T22">деньги.[NOM]-1SG</ta>
            <ta e="T24" id="Seg_298" s="T23">много-ADVZ</ta>
            <ta e="T25" id="Seg_299" s="T24">быть-FUT-3SG.S</ta>
            <ta e="T26" id="Seg_300" s="T25">мы.[NOM]</ta>
            <ta e="T27" id="Seg_301" s="T26">сюда-ADV.LOC</ta>
            <ta e="T28" id="Seg_302" s="T27">купить-FUT-1PL</ta>
            <ta e="T29" id="Seg_303" s="T28">орех-ACC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_304" s="T1">pers</ta>
            <ta e="T3" id="Seg_305" s="T2">adv-adv:case</ta>
            <ta e="T4" id="Seg_306" s="T3">n-n:ins-n:case</ta>
            <ta e="T5" id="Seg_307" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_308" s="T5">v-v&gt;adv</ta>
            <ta e="T7" id="Seg_309" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_310" s="T7">v-v&gt;adv</ta>
            <ta e="T9" id="Seg_311" s="T8">n.[n:case]</ta>
            <ta e="T10" id="Seg_312" s="T9">adv</ta>
            <ta e="T11" id="Seg_313" s="T10">v-v:tense.[v:pn]</ta>
            <ta e="T12" id="Seg_314" s="T11">pers</ta>
            <ta e="T13" id="Seg_315" s="T12">num</ta>
            <ta e="T14" id="Seg_316" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_317" s="T14">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_318" s="T15">conj</ta>
            <ta e="T17" id="Seg_319" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_320" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_321" s="T18">conj</ta>
            <ta e="T20" id="Seg_322" s="T19">pers</ta>
            <ta e="T21" id="Seg_323" s="T20">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_324" s="T21">pers-n:case</ta>
            <ta e="T23" id="Seg_325" s="T22">n.[n:case]-n:poss</ta>
            <ta e="T24" id="Seg_326" s="T23">quant-quant&gt;adv</ta>
            <ta e="T25" id="Seg_327" s="T24">v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_328" s="T25">pers.[n:case]</ta>
            <ta e="T27" id="Seg_329" s="T26">adv-adv:case</ta>
            <ta e="T28" id="Seg_330" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_331" s="T28">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_332" s="T1">pers</ta>
            <ta e="T3" id="Seg_333" s="T2">adv</ta>
            <ta e="T4" id="Seg_334" s="T3">n</ta>
            <ta e="T5" id="Seg_335" s="T4">v</ta>
            <ta e="T6" id="Seg_336" s="T5">adv</ta>
            <ta e="T7" id="Seg_337" s="T6">v</ta>
            <ta e="T8" id="Seg_338" s="T7">adv</ta>
            <ta e="T9" id="Seg_339" s="T8">n</ta>
            <ta e="T10" id="Seg_340" s="T9">adv</ta>
            <ta e="T11" id="Seg_341" s="T10">v</ta>
            <ta e="T12" id="Seg_342" s="T11">pers</ta>
            <ta e="T13" id="Seg_343" s="T12">num</ta>
            <ta e="T14" id="Seg_344" s="T13">n</ta>
            <ta e="T15" id="Seg_345" s="T14">v</ta>
            <ta e="T16" id="Seg_346" s="T15">conj</ta>
            <ta e="T17" id="Seg_347" s="T16">n</ta>
            <ta e="T18" id="Seg_348" s="T17">v</ta>
            <ta e="T19" id="Seg_349" s="T18">conj</ta>
            <ta e="T20" id="Seg_350" s="T19">pers</ta>
            <ta e="T21" id="Seg_351" s="T20">v</ta>
            <ta e="T22" id="Seg_352" s="T21">pers</ta>
            <ta e="T23" id="Seg_353" s="T22">n</ta>
            <ta e="T24" id="Seg_354" s="T23">quant</ta>
            <ta e="T25" id="Seg_355" s="T24">v</ta>
            <ta e="T26" id="Seg_356" s="T25">pers</ta>
            <ta e="T27" id="Seg_357" s="T26">adv</ta>
            <ta e="T28" id="Seg_358" s="T27">v</ta>
            <ta e="T29" id="Seg_359" s="T28">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_360" s="T1">pro.h:A</ta>
            <ta e="T3" id="Seg_361" s="T2">adv:Time</ta>
            <ta e="T4" id="Seg_362" s="T3">np:G</ta>
            <ta e="T7" id="Seg_363" s="T6">0.1.h:A</ta>
            <ta e="T9" id="Seg_364" s="T8">np:P</ta>
            <ta e="T12" id="Seg_365" s="T11">pro.h:A</ta>
            <ta e="T14" id="Seg_366" s="T13">np:Th</ta>
            <ta e="T17" id="Seg_367" s="T16">np:G</ta>
            <ta e="T18" id="Seg_368" s="T17">0.1.h:A</ta>
            <ta e="T20" id="Seg_369" s="T19">pro.h:A</ta>
            <ta e="T21" id="Seg_370" s="T20">0.3:Th</ta>
            <ta e="T22" id="Seg_371" s="T21">pro.h:Poss</ta>
            <ta e="T23" id="Seg_372" s="T22">np:Th</ta>
            <ta e="T26" id="Seg_373" s="T25">pro.h:A</ta>
            <ta e="T27" id="Seg_374" s="T26">adv:L</ta>
            <ta e="T29" id="Seg_375" s="T28">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_376" s="T1">pro.h:S</ta>
            <ta e="T5" id="Seg_377" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_378" s="T5">s:purp</ta>
            <ta e="T7" id="Seg_379" s="T6">0.1.h:S v:pred</ta>
            <ta e="T8" id="Seg_380" s="T7">s:purp</ta>
            <ta e="T9" id="Seg_381" s="T8">np:S</ta>
            <ta e="T11" id="Seg_382" s="T10">v:pred</ta>
            <ta e="T12" id="Seg_383" s="T11">pro.h:S</ta>
            <ta e="T14" id="Seg_384" s="T13">np:O</ta>
            <ta e="T15" id="Seg_385" s="T14">v:pred</ta>
            <ta e="T18" id="Seg_386" s="T17">0.1.h:S v:pred</ta>
            <ta e="T20" id="Seg_387" s="T19">pro.h:S</ta>
            <ta e="T21" id="Seg_388" s="T20">v:pred 0.3:O</ta>
            <ta e="T23" id="Seg_389" s="T22">np:S</ta>
            <ta e="T25" id="Seg_390" s="T24">v:pred</ta>
            <ta e="T26" id="Seg_391" s="T25">pro.h:S</ta>
            <ta e="T28" id="Seg_392" s="T27">v:pred</ta>
            <ta e="T29" id="Seg_393" s="T28">np:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T10" id="Seg_394" s="T9">RUS:core</ta>
            <ta e="T16" id="Seg_395" s="T15">RUS:gram</ta>
            <ta e="T19" id="Seg_396" s="T18">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_397" s="T1">Tomorrow I'll go to taiga to gather nuts.</ta>
            <ta e="T8" id="Seg_398" s="T6">I'll go to gather nuts.</ta>
            <ta e="T11" id="Seg_399" s="T8">There are many nuts (= nuts have been born well).</ta>
            <ta e="T18" id="Seg_400" s="T11">I'll gather three bags and I'll come back home.</ta>
            <ta e="T21" id="Seg_401" s="T18">I'll sell them.</ta>
            <ta e="T25" id="Seg_402" s="T21">I'll get much money.</ta>
            <ta e="T29" id="Seg_403" s="T25">We'll buy nuts here.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_404" s="T1">Ich gehe morgen in die Taiga um Nüsse zu sammeln.</ta>
            <ta e="T8" id="Seg_405" s="T6">Ich werde gehen um Nüsse zu sammeln.</ta>
            <ta e="T11" id="Seg_406" s="T8">Es gibt viele Nüsse (= Nüsse wurden gut geboren).</ta>
            <ta e="T18" id="Seg_407" s="T11">Ich werde drei Taschen sammeln und nach Hause kommen.</ta>
            <ta e="T21" id="Seg_408" s="T18">Ich werde sie verkaufen.</ta>
            <ta e="T25" id="Seg_409" s="T21">Ich werde viel Geld haben.</ta>
            <ta e="T29" id="Seg_410" s="T25">Wir werden hier Nüsse kaufen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_411" s="T1">Я завтра пойду в тайгу за орехами.</ta>
            <ta e="T8" id="Seg_412" s="T6">Я за орехами пойду.</ta>
            <ta e="T11" id="Seg_413" s="T8">Орех путём (хорошо) родился.</ta>
            <ta e="T18" id="Seg_414" s="T11">Я три мешка сделаю и домой приеду.</ta>
            <ta e="T21" id="Seg_415" s="T18">И я их продам.</ta>
            <ta e="T25" id="Seg_416" s="T21">У меня денег много будет.</ta>
            <ta e="T29" id="Seg_417" s="T25">Мы тут купим орехов.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_418" s="T1">я завтра пойду в тайгу</ta>
            <ta e="T8" id="Seg_419" s="T6">за орехами пойду</ta>
            <ta e="T11" id="Seg_420" s="T8">орех путём (хорошо) родился уродился</ta>
            <ta e="T18" id="Seg_421" s="T11">я три мешка сделаю и домой приеду</ta>
            <ta e="T21" id="Seg_422" s="T18">я продам</ta>
            <ta e="T25" id="Seg_423" s="T21">у меня денег много будет</ta>
            <ta e="T29" id="Seg_424" s="T25">мы тут купим орехов</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T11" id="Seg_425" s="T8">[KuAI:] Variant: 'dʼelɨpba'.</ta>
            <ta e="T21" id="Seg_426" s="T18">[BrM:] DRV &gt;&gt; IPFV?</ta>
            <ta e="T29" id="Seg_427" s="T25">[KuAI:] Variant: 'soːnqam'-</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
