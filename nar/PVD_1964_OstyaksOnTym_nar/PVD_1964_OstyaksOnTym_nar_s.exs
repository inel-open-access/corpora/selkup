<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_OstyaksOnTym_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_OstyaksOnTym_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">61</ud-information>
            <ud-information attribute-name="# HIAT:w">48</ud-information>
            <ud-information attribute-name="# e">48</ud-information>
            <ud-information attribute-name="# HIAT:u">12</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T49" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Tɨmɣɨn</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">süsögula</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">kocʼin</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">jewattə</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Natʼen</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">qwɛl</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">kocʼin</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">jen</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_32" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">Man</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">radnʼala</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">natʼen</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">koːcʼin</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">jewattə</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_50" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">Man</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">agau</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">natʼen</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">ilɨt</ts>
                  <nts id="Seg_62" n="HIAT:ip">,</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">Napaskɨn</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_69" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">Nastʼona</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">Kajdalowa</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_77" n="HIAT:w" s="T21">samɨj</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_80" n="HIAT:w" s="T22">karʼennoj</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_83" n="HIAT:w" s="T23">süsöɣən</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_86" n="HIAT:w" s="T24">näjaqum</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_90" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">Tep</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_95" n="HIAT:w" s="T26">tettə</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_98" n="HIAT:w" s="T27">taɣɨn</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_101" n="HIAT:w" s="T28">mannan</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_104" n="HIAT:w" s="T29">warkɨs</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_108" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">A</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">täpär</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_116" n="HIAT:w" s="T32">Tɨmda</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_119" n="HIAT:w" s="T33">qwanba</ts>
                  <nts id="Seg_120" n="HIAT:ip">.</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_123" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">Eragattəse</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">qwassaɣɨ</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_132" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_134" n="HIAT:w" s="T36">Eragat</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_137" n="HIAT:w" s="T37">qupba</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_141" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_143" n="HIAT:w" s="T38">Täp</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_146" n="HIAT:w" s="T39">otdə</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_149" n="HIAT:w" s="T40">qalɨpba</ts>
                  <nts id="Seg_150" n="HIAT:ip">.</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_153" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_155" n="HIAT:w" s="T41">Tebnan</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_158" n="HIAT:w" s="T42">tʼarattə</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_161" n="HIAT:w" s="T43">mattə</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_164" n="HIAT:w" s="T44">soːdʼiga</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_167" n="HIAT:w" s="T45">omdɨlǯibat</ts>
                  <nts id="Seg_168" n="HIAT:ip">.</nts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_171" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_173" n="HIAT:w" s="T46">Natʼen</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_176" n="HIAT:w" s="T47">i</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_179" n="HIAT:w" s="T48">warka</ts>
                  <nts id="Seg_180" n="HIAT:ip">.</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T49" id="Seg_182" n="sc" s="T1">
               <ts e="T2" id="Seg_184" n="e" s="T1">Tɨmɣɨn </ts>
               <ts e="T3" id="Seg_186" n="e" s="T2">süsögula </ts>
               <ts e="T4" id="Seg_188" n="e" s="T3">kocʼin </ts>
               <ts e="T5" id="Seg_190" n="e" s="T4">jewattə. </ts>
               <ts e="T6" id="Seg_192" n="e" s="T5">Natʼen </ts>
               <ts e="T7" id="Seg_194" n="e" s="T6">qwɛl </ts>
               <ts e="T8" id="Seg_196" n="e" s="T7">kocʼin </ts>
               <ts e="T9" id="Seg_198" n="e" s="T8">jen. </ts>
               <ts e="T10" id="Seg_200" n="e" s="T9">Man </ts>
               <ts e="T11" id="Seg_202" n="e" s="T10">radnʼala </ts>
               <ts e="T12" id="Seg_204" n="e" s="T11">natʼen </ts>
               <ts e="T13" id="Seg_206" n="e" s="T12">koːcʼin </ts>
               <ts e="T14" id="Seg_208" n="e" s="T13">jewattə. </ts>
               <ts e="T15" id="Seg_210" n="e" s="T14">Man </ts>
               <ts e="T16" id="Seg_212" n="e" s="T15">agau </ts>
               <ts e="T17" id="Seg_214" n="e" s="T16">natʼen </ts>
               <ts e="T18" id="Seg_216" n="e" s="T17">ilɨt, </ts>
               <ts e="T19" id="Seg_218" n="e" s="T18">Napaskɨn. </ts>
               <ts e="T20" id="Seg_220" n="e" s="T19">Nastʼona </ts>
               <ts e="T21" id="Seg_222" n="e" s="T20">Kajdalowa </ts>
               <ts e="T22" id="Seg_224" n="e" s="T21">samɨj </ts>
               <ts e="T23" id="Seg_226" n="e" s="T22">karʼennoj </ts>
               <ts e="T24" id="Seg_228" n="e" s="T23">süsöɣən </ts>
               <ts e="T25" id="Seg_230" n="e" s="T24">näjaqum. </ts>
               <ts e="T26" id="Seg_232" n="e" s="T25">Tep </ts>
               <ts e="T27" id="Seg_234" n="e" s="T26">tettə </ts>
               <ts e="T28" id="Seg_236" n="e" s="T27">taɣɨn </ts>
               <ts e="T29" id="Seg_238" n="e" s="T28">mannan </ts>
               <ts e="T30" id="Seg_240" n="e" s="T29">warkɨs. </ts>
               <ts e="T31" id="Seg_242" n="e" s="T30">A </ts>
               <ts e="T32" id="Seg_244" n="e" s="T31">täpär </ts>
               <ts e="T33" id="Seg_246" n="e" s="T32">Tɨmda </ts>
               <ts e="T34" id="Seg_248" n="e" s="T33">qwanba. </ts>
               <ts e="T35" id="Seg_250" n="e" s="T34">Eragattəse </ts>
               <ts e="T36" id="Seg_252" n="e" s="T35">qwassaɣɨ. </ts>
               <ts e="T37" id="Seg_254" n="e" s="T36">Eragat </ts>
               <ts e="T38" id="Seg_256" n="e" s="T37">qupba. </ts>
               <ts e="T39" id="Seg_258" n="e" s="T38">Täp </ts>
               <ts e="T40" id="Seg_260" n="e" s="T39">otdə </ts>
               <ts e="T41" id="Seg_262" n="e" s="T40">qalɨpba. </ts>
               <ts e="T42" id="Seg_264" n="e" s="T41">Tebnan </ts>
               <ts e="T43" id="Seg_266" n="e" s="T42">tʼarattə </ts>
               <ts e="T44" id="Seg_268" n="e" s="T43">mattə </ts>
               <ts e="T45" id="Seg_270" n="e" s="T44">soːdʼiga </ts>
               <ts e="T46" id="Seg_272" n="e" s="T45">omdɨlǯibat. </ts>
               <ts e="T47" id="Seg_274" n="e" s="T46">Natʼen </ts>
               <ts e="T48" id="Seg_276" n="e" s="T47">i </ts>
               <ts e="T49" id="Seg_278" n="e" s="T48">warka. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_279" s="T1">PVD_1964_OstyaksOnTym_nar.001 (001.001)</ta>
            <ta e="T9" id="Seg_280" s="T5">PVD_1964_OstyaksOnTym_nar.002 (001.002)</ta>
            <ta e="T14" id="Seg_281" s="T9">PVD_1964_OstyaksOnTym_nar.003 (001.003)</ta>
            <ta e="T19" id="Seg_282" s="T14">PVD_1964_OstyaksOnTym_nar.004 (001.004)</ta>
            <ta e="T25" id="Seg_283" s="T19">PVD_1964_OstyaksOnTym_nar.005 (001.005)</ta>
            <ta e="T30" id="Seg_284" s="T25">PVD_1964_OstyaksOnTym_nar.006 (001.006)</ta>
            <ta e="T34" id="Seg_285" s="T30">PVD_1964_OstyaksOnTym_nar.007 (001.007)</ta>
            <ta e="T36" id="Seg_286" s="T34">PVD_1964_OstyaksOnTym_nar.008 (001.008)</ta>
            <ta e="T38" id="Seg_287" s="T36">PVD_1964_OstyaksOnTym_nar.009 (001.009)</ta>
            <ta e="T41" id="Seg_288" s="T38">PVD_1964_OstyaksOnTym_nar.010 (001.010)</ta>
            <ta e="T46" id="Seg_289" s="T41">PVD_1964_OstyaksOnTym_nar.011 (001.011)</ta>
            <ta e="T49" id="Seg_290" s="T46">PVD_1964_OstyaksOnTym_nar.012 (001.012)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_291" s="T1">′тымɣын сӱ′сӧгула ′коцʼин jеwаттъ.</ta>
            <ta e="T9" id="Seg_292" s="T5">на′тʼен kwɛл ′коцʼин jен.</ta>
            <ta e="T14" id="Seg_293" s="T9">ман рад′нʼала на′тʼен ′ко̄цʼин ′jеwаттъ.</ta>
            <ta e="T19" id="Seg_294" s="T14">ман а′гау на′тʼен ′илыт, На′паскын.</ta>
            <ta e="T25" id="Seg_295" s="T19">Нас′тʼона Кай′далова самый карʼенной сӱ′сӧɣън ′нӓjаkум.</ta>
            <ta e="T30" id="Seg_296" s="T25">теп ′теттъ та′ɣын ман′нан ′варкыс.</ta>
            <ta e="T34" id="Seg_297" s="T30">а тӓ′пӓр ′тымда kwан′ба.</ta>
            <ta e="T36" id="Seg_298" s="T34">′ерагаттъ‵се kwа′ссаɣы.</ta>
            <ta e="T38" id="Seg_299" s="T36">′ерагат ′kупб̂а.</ta>
            <ta e="T41" id="Seg_300" s="T38">тӓп ′отдъ ′kалыпба.</ta>
            <ta e="T46" id="Seg_301" s="T41">теб′нан тʼа′раттъ ′маттъ ′со̄дʼига омдыlджибат.</ta>
            <ta e="T49" id="Seg_302" s="T46">на′тʼен и вар′ка.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_303" s="T1">tɨmɣɨn süsögula kocʼin jewattə.</ta>
            <ta e="T9" id="Seg_304" s="T5">natʼen qwɛl kocʼin jen.</ta>
            <ta e="T14" id="Seg_305" s="T9">man radnʼala natʼen koːcʼin jewattə.</ta>
            <ta e="T19" id="Seg_306" s="T14">man agau natʼen ilɨt, Нapaskɨn.</ta>
            <ta e="T25" id="Seg_307" s="T19">Нastʼona Кajdalowa samɨj karʼennoj süsöɣən näjaqum.</ta>
            <ta e="T30" id="Seg_308" s="T25">tep tettə taɣɨn mannan warkɨs.</ta>
            <ta e="T34" id="Seg_309" s="T30">a täpär tɨmda qwanba.</ta>
            <ta e="T36" id="Seg_310" s="T34">eragattəse qwassaɣɨ.</ta>
            <ta e="T38" id="Seg_311" s="T36">eragat qupb̂a.</ta>
            <ta e="T41" id="Seg_312" s="T38">täp otdə qalɨpba.</ta>
            <ta e="T46" id="Seg_313" s="T41">tebnan tʼarattə mattə soːdʼiga omdɨlǯibat.</ta>
            <ta e="T49" id="Seg_314" s="T46">natʼen i warka.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_315" s="T1">Tɨmɣɨn süsögula kocʼin jewattə. </ta>
            <ta e="T9" id="Seg_316" s="T5">Natʼen qwɛl kocʼin jen. </ta>
            <ta e="T14" id="Seg_317" s="T9">Man radnʼala natʼen koːcʼin jewattə. </ta>
            <ta e="T19" id="Seg_318" s="T14">Man agau natʼen ilɨt, Napaskɨn. </ta>
            <ta e="T25" id="Seg_319" s="T19">Nastʼona Kajdalowa samɨj karʼennoj süsöɣən näjaqum. </ta>
            <ta e="T30" id="Seg_320" s="T25">Tep tettə taɣɨn mannan warkɨs. </ta>
            <ta e="T34" id="Seg_321" s="T30">A täpär Tɨmda qwanba. </ta>
            <ta e="T36" id="Seg_322" s="T34">Eragattəse qwassaɣɨ. </ta>
            <ta e="T38" id="Seg_323" s="T36">Eragat qupba. </ta>
            <ta e="T41" id="Seg_324" s="T38">Täp otdə qalɨpba. </ta>
            <ta e="T46" id="Seg_325" s="T41">Tebnan tʼarattə mattə soːdʼiga omdɨlǯibat. </ta>
            <ta e="T49" id="Seg_326" s="T46">Natʼen i warka. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_327" s="T1">Tɨm-ɣɨn</ta>
            <ta e="T3" id="Seg_328" s="T2">süsögu-la</ta>
            <ta e="T4" id="Seg_329" s="T3">kocʼi-n</ta>
            <ta e="T5" id="Seg_330" s="T4">je-wa-ttə</ta>
            <ta e="T6" id="Seg_331" s="T5">natʼe-n</ta>
            <ta e="T7" id="Seg_332" s="T6">qwɛl</ta>
            <ta e="T8" id="Seg_333" s="T7">kocʼi-n</ta>
            <ta e="T9" id="Seg_334" s="T8">je-n</ta>
            <ta e="T10" id="Seg_335" s="T9">man</ta>
            <ta e="T11" id="Seg_336" s="T10">radnʼa-la</ta>
            <ta e="T12" id="Seg_337" s="T11">natʼe-n</ta>
            <ta e="T13" id="Seg_338" s="T12">koːcʼi-n</ta>
            <ta e="T14" id="Seg_339" s="T13">je-wa-ttə</ta>
            <ta e="T15" id="Seg_340" s="T14">man</ta>
            <ta e="T16" id="Seg_341" s="T15">aga-u</ta>
            <ta e="T17" id="Seg_342" s="T16">natʼe-n</ta>
            <ta e="T18" id="Seg_343" s="T17">ilɨ-t</ta>
            <ta e="T19" id="Seg_344" s="T18">Napas-kɨn</ta>
            <ta e="T20" id="Seg_345" s="T19">Nastʼona</ta>
            <ta e="T21" id="Seg_346" s="T20">Kajdalowa</ta>
            <ta e="T24" id="Seg_347" s="T23">süsöɣən</ta>
            <ta e="T25" id="Seg_348" s="T24">nä-j-a-qum</ta>
            <ta e="T26" id="Seg_349" s="T25">tep</ta>
            <ta e="T27" id="Seg_350" s="T26">tettə</ta>
            <ta e="T28" id="Seg_351" s="T27">taɣ-ɨ-n</ta>
            <ta e="T29" id="Seg_352" s="T28">man-nan</ta>
            <ta e="T30" id="Seg_353" s="T29">warkɨ-s</ta>
            <ta e="T31" id="Seg_354" s="T30">a</ta>
            <ta e="T32" id="Seg_355" s="T31">täpär</ta>
            <ta e="T33" id="Seg_356" s="T32">Tɨm-da</ta>
            <ta e="T34" id="Seg_357" s="T33">qwan-ba</ta>
            <ta e="T35" id="Seg_358" s="T34">era-ga-ttə-se</ta>
            <ta e="T36" id="Seg_359" s="T35">qwas-sa-ɣɨ</ta>
            <ta e="T37" id="Seg_360" s="T36">era-ga-t</ta>
            <ta e="T38" id="Seg_361" s="T37">qu-pba</ta>
            <ta e="T39" id="Seg_362" s="T38">täp</ta>
            <ta e="T40" id="Seg_363" s="T39">otdə</ta>
            <ta e="T41" id="Seg_364" s="T40">qalɨ-pba</ta>
            <ta e="T42" id="Seg_365" s="T41">teb-nan</ta>
            <ta e="T43" id="Seg_366" s="T42">tʼara-ttə</ta>
            <ta e="T44" id="Seg_367" s="T43">mat-tə</ta>
            <ta e="T45" id="Seg_368" s="T44">soːdʼiga</ta>
            <ta e="T46" id="Seg_369" s="T45">omdɨ-lǯi-ba-t</ta>
            <ta e="T47" id="Seg_370" s="T46">natʼe-n</ta>
            <ta e="T48" id="Seg_371" s="T47">i</ta>
            <ta e="T49" id="Seg_372" s="T48">warka</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_373" s="T1">Tɨm-qɨn</ta>
            <ta e="T3" id="Seg_374" s="T2">süsögum-la</ta>
            <ta e="T4" id="Seg_375" s="T3">koːci-ŋ</ta>
            <ta e="T5" id="Seg_376" s="T4">eː-nɨ-tɨn</ta>
            <ta e="T6" id="Seg_377" s="T5">*natʼe-n</ta>
            <ta e="T7" id="Seg_378" s="T6">qwɛl</ta>
            <ta e="T8" id="Seg_379" s="T7">koːci-ŋ</ta>
            <ta e="T9" id="Seg_380" s="T8">eː-n</ta>
            <ta e="T10" id="Seg_381" s="T9">man</ta>
            <ta e="T11" id="Seg_382" s="T10">radnʼa-la</ta>
            <ta e="T12" id="Seg_383" s="T11">*natʼe-n</ta>
            <ta e="T13" id="Seg_384" s="T12">koːci-ŋ</ta>
            <ta e="T14" id="Seg_385" s="T13">eː-nɨ-tɨn</ta>
            <ta e="T15" id="Seg_386" s="T14">man</ta>
            <ta e="T16" id="Seg_387" s="T15">agaː-w</ta>
            <ta e="T17" id="Seg_388" s="T16">*natʼe-n</ta>
            <ta e="T18" id="Seg_389" s="T17">elɨ-ntɨ</ta>
            <ta e="T19" id="Seg_390" s="T18">Napas-qɨn</ta>
            <ta e="T20" id="Seg_391" s="T19">Nastʼona</ta>
            <ta e="T21" id="Seg_392" s="T20">Kajdalowa</ta>
            <ta e="T24" id="Seg_393" s="T23">süsögi</ta>
            <ta e="T25" id="Seg_394" s="T24">ne-lʼ-ɨ-qum</ta>
            <ta e="T26" id="Seg_395" s="T25">täp</ta>
            <ta e="T27" id="Seg_396" s="T26">tettɨ</ta>
            <ta e="T28" id="Seg_397" s="T27">taɣ-ɨ-ŋ</ta>
            <ta e="T29" id="Seg_398" s="T28">man-nan</ta>
            <ta e="T30" id="Seg_399" s="T29">warkɨ-sɨ</ta>
            <ta e="T31" id="Seg_400" s="T30">a</ta>
            <ta e="T32" id="Seg_401" s="T31">teper</ta>
            <ta e="T33" id="Seg_402" s="T32">Tɨm-ntə</ta>
            <ta e="T34" id="Seg_403" s="T33">qwan-mbɨ</ta>
            <ta e="T35" id="Seg_404" s="T34">era-ka-tə-se</ta>
            <ta e="T36" id="Seg_405" s="T35">qwan-sɨ-qij</ta>
            <ta e="T37" id="Seg_406" s="T36">era-ka-tə</ta>
            <ta e="T38" id="Seg_407" s="T37">quː-mbɨ</ta>
            <ta e="T39" id="Seg_408" s="T38">täp</ta>
            <ta e="T40" id="Seg_409" s="T39">ondə</ta>
            <ta e="T41" id="Seg_410" s="T40">qalɨ-mbɨ</ta>
            <ta e="T42" id="Seg_411" s="T41">täp-nan</ta>
            <ta e="T43" id="Seg_412" s="T42">tʼärɨ-tɨn</ta>
            <ta e="T44" id="Seg_413" s="T43">maːt-tə</ta>
            <ta e="T45" id="Seg_414" s="T44">soːdʼiga</ta>
            <ta e="T46" id="Seg_415" s="T45">omdɨ-lǯi-mbɨ-t</ta>
            <ta e="T47" id="Seg_416" s="T46">*natʼe-n</ta>
            <ta e="T48" id="Seg_417" s="T47">i</ta>
            <ta e="T49" id="Seg_418" s="T48">warkɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_419" s="T1">Tym-LOC</ta>
            <ta e="T3" id="Seg_420" s="T2">Selkup-PL.[NOM]</ta>
            <ta e="T4" id="Seg_421" s="T3">much-ADVZ</ta>
            <ta e="T5" id="Seg_422" s="T4">be-CO-3PL</ta>
            <ta e="T6" id="Seg_423" s="T5">there-ADV.LOC</ta>
            <ta e="T7" id="Seg_424" s="T6">fish.[NOM]</ta>
            <ta e="T8" id="Seg_425" s="T7">much-ADVZ</ta>
            <ta e="T9" id="Seg_426" s="T8">be-3SG.S</ta>
            <ta e="T10" id="Seg_427" s="T9">I.GEN</ta>
            <ta e="T11" id="Seg_428" s="T10">relative-PL.[NOM]</ta>
            <ta e="T12" id="Seg_429" s="T11">there-ADV.LOC</ta>
            <ta e="T13" id="Seg_430" s="T12">much-ADVZ</ta>
            <ta e="T14" id="Seg_431" s="T13">be-CO-3PL</ta>
            <ta e="T15" id="Seg_432" s="T14">I.GEN</ta>
            <ta e="T16" id="Seg_433" s="T15">brother.[NOM]-1SG</ta>
            <ta e="T17" id="Seg_434" s="T16">there-ADV.LOC</ta>
            <ta e="T18" id="Seg_435" s="T17">live-INFER.[3SG.S]</ta>
            <ta e="T19" id="Seg_436" s="T18">Napas-LOC</ta>
            <ta e="T20" id="Seg_437" s="T19">Nastyona.[NOM]</ta>
            <ta e="T21" id="Seg_438" s="T20">Kajdalova.[NOM]</ta>
            <ta e="T24" id="Seg_439" s="T23">Selkup</ta>
            <ta e="T25" id="Seg_440" s="T24">woman-ADJZ-EP-human.being.[NOM]</ta>
            <ta e="T26" id="Seg_441" s="T25">(s)he.[NOM]</ta>
            <ta e="T27" id="Seg_442" s="T26">four</ta>
            <ta e="T28" id="Seg_443" s="T27">summer-EP-ADVZ</ta>
            <ta e="T29" id="Seg_444" s="T28">I-ADES</ta>
            <ta e="T30" id="Seg_445" s="T29">live-PST.[3SG.S]</ta>
            <ta e="T31" id="Seg_446" s="T30">and</ta>
            <ta e="T32" id="Seg_447" s="T31">now</ta>
            <ta e="T33" id="Seg_448" s="T32">Tym-ILL</ta>
            <ta e="T34" id="Seg_449" s="T33">leave-PST.NAR.[3SG.S]</ta>
            <ta e="T35" id="Seg_450" s="T34">husband-DIM-3SG-COM</ta>
            <ta e="T36" id="Seg_451" s="T35">leave-PST-3DU.S</ta>
            <ta e="T37" id="Seg_452" s="T36">husband-DIM.[NOM]-3SG</ta>
            <ta e="T38" id="Seg_453" s="T37">die-PST.NAR.[3SG.S]</ta>
            <ta e="T39" id="Seg_454" s="T38">(s)he.[NOM]</ta>
            <ta e="T40" id="Seg_455" s="T39">oneself.3SG</ta>
            <ta e="T41" id="Seg_456" s="T40">stay-PST.NAR.[3SG.S]</ta>
            <ta e="T42" id="Seg_457" s="T41">(s)he-ADES</ta>
            <ta e="T43" id="Seg_458" s="T42">say-3PL</ta>
            <ta e="T44" id="Seg_459" s="T43">tent.[NOM]-3SG</ta>
            <ta e="T45" id="Seg_460" s="T44">good</ta>
            <ta e="T46" id="Seg_461" s="T45">sit.down-TR-PST.NAR-3SG.O</ta>
            <ta e="T47" id="Seg_462" s="T46">there-ADV.LOC</ta>
            <ta e="T48" id="Seg_463" s="T47">and</ta>
            <ta e="T49" id="Seg_464" s="T48">live.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_465" s="T1">Тым-LOC</ta>
            <ta e="T3" id="Seg_466" s="T2">селькуп-PL.[NOM]</ta>
            <ta e="T4" id="Seg_467" s="T3">много-ADVZ</ta>
            <ta e="T5" id="Seg_468" s="T4">быть-CO-3PL</ta>
            <ta e="T6" id="Seg_469" s="T5">туда-ADV.LOC</ta>
            <ta e="T7" id="Seg_470" s="T6">рыба.[NOM]</ta>
            <ta e="T8" id="Seg_471" s="T7">много-ADVZ</ta>
            <ta e="T9" id="Seg_472" s="T8">быть-3SG.S</ta>
            <ta e="T10" id="Seg_473" s="T9">я.GEN</ta>
            <ta e="T11" id="Seg_474" s="T10">родня-PL.[NOM]</ta>
            <ta e="T12" id="Seg_475" s="T11">туда-ADV.LOC</ta>
            <ta e="T13" id="Seg_476" s="T12">много-ADVZ</ta>
            <ta e="T14" id="Seg_477" s="T13">быть-CO-3PL</ta>
            <ta e="T15" id="Seg_478" s="T14">я.GEN</ta>
            <ta e="T16" id="Seg_479" s="T15">брат.[NOM]-1SG</ta>
            <ta e="T17" id="Seg_480" s="T16">туда-ADV.LOC</ta>
            <ta e="T18" id="Seg_481" s="T17">жить-INFER.[3SG.S]</ta>
            <ta e="T19" id="Seg_482" s="T18">Напас-LOC</ta>
            <ta e="T20" id="Seg_483" s="T19">Настёна.[NOM]</ta>
            <ta e="T21" id="Seg_484" s="T20">Кайдалова.[NOM]</ta>
            <ta e="T24" id="Seg_485" s="T23">селькупский</ta>
            <ta e="T25" id="Seg_486" s="T24">женщина-ADJZ-EP-человек.[NOM]</ta>
            <ta e="T26" id="Seg_487" s="T25">он(а).[NOM]</ta>
            <ta e="T27" id="Seg_488" s="T26">четыре</ta>
            <ta e="T28" id="Seg_489" s="T27">лето-EP-ADVZ</ta>
            <ta e="T29" id="Seg_490" s="T28">я-ADES</ta>
            <ta e="T30" id="Seg_491" s="T29">жить-PST.[3SG.S]</ta>
            <ta e="T31" id="Seg_492" s="T30">а</ta>
            <ta e="T32" id="Seg_493" s="T31">теперь</ta>
            <ta e="T33" id="Seg_494" s="T32">Тым-ILL</ta>
            <ta e="T34" id="Seg_495" s="T33">отправиться-PST.NAR.[3SG.S]</ta>
            <ta e="T35" id="Seg_496" s="T34">муж-DIM-3SG-COM</ta>
            <ta e="T36" id="Seg_497" s="T35">отправиться-PST-3DU.S</ta>
            <ta e="T37" id="Seg_498" s="T36">муж-DIM.[NOM]-3SG</ta>
            <ta e="T38" id="Seg_499" s="T37">умереть-PST.NAR.[3SG.S]</ta>
            <ta e="T39" id="Seg_500" s="T38">он(а).[NOM]</ta>
            <ta e="T40" id="Seg_501" s="T39">сам.3SG</ta>
            <ta e="T41" id="Seg_502" s="T40">остаться-PST.NAR.[3SG.S]</ta>
            <ta e="T42" id="Seg_503" s="T41">он(а)-ADES</ta>
            <ta e="T43" id="Seg_504" s="T42">сказать-3PL</ta>
            <ta e="T44" id="Seg_505" s="T43">чум.[NOM]-3SG</ta>
            <ta e="T45" id="Seg_506" s="T44">хороший</ta>
            <ta e="T46" id="Seg_507" s="T45">сесть-TR-PST.NAR-3SG.O</ta>
            <ta e="T47" id="Seg_508" s="T46">туда-ADV.LOC</ta>
            <ta e="T48" id="Seg_509" s="T47">и</ta>
            <ta e="T49" id="Seg_510" s="T48">жить.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_511" s="T1">nprop-n:case</ta>
            <ta e="T3" id="Seg_512" s="T2">n-n:num.[n:case]</ta>
            <ta e="T4" id="Seg_513" s="T3">quant-adj&gt;adv</ta>
            <ta e="T5" id="Seg_514" s="T4">v-v:ins-v:pn</ta>
            <ta e="T6" id="Seg_515" s="T5">adv-adv:case</ta>
            <ta e="T7" id="Seg_516" s="T6">n.[n:case]</ta>
            <ta e="T8" id="Seg_517" s="T7">quant-adj&gt;adv</ta>
            <ta e="T9" id="Seg_518" s="T8">v-v:pn</ta>
            <ta e="T10" id="Seg_519" s="T9">pers</ta>
            <ta e="T11" id="Seg_520" s="T10">n-n:num.[n:case]</ta>
            <ta e="T12" id="Seg_521" s="T11">adv-adv:case</ta>
            <ta e="T13" id="Seg_522" s="T12">quant-quant&gt;adv</ta>
            <ta e="T14" id="Seg_523" s="T13">v-v:ins-v:pn</ta>
            <ta e="T15" id="Seg_524" s="T14">pers</ta>
            <ta e="T16" id="Seg_525" s="T15">n.[n:case]-n:poss</ta>
            <ta e="T17" id="Seg_526" s="T16">adv-adv:case</ta>
            <ta e="T18" id="Seg_527" s="T17">v-v:mood.[v:pn]</ta>
            <ta e="T19" id="Seg_528" s="T18">nprop-n:case</ta>
            <ta e="T20" id="Seg_529" s="T19">nprop.[n:case]</ta>
            <ta e="T21" id="Seg_530" s="T20">nprop.[n:case]</ta>
            <ta e="T24" id="Seg_531" s="T23">adj</ta>
            <ta e="T25" id="Seg_532" s="T24">n-n&gt;adj-n:ins-n.[n:case]</ta>
            <ta e="T26" id="Seg_533" s="T25">pers.[n:case]</ta>
            <ta e="T27" id="Seg_534" s="T26">num</ta>
            <ta e="T28" id="Seg_535" s="T27">n-n:ins-n&gt;adv</ta>
            <ta e="T29" id="Seg_536" s="T28">pers-n:case</ta>
            <ta e="T30" id="Seg_537" s="T29">v-v:tense.[v:pn]</ta>
            <ta e="T31" id="Seg_538" s="T30">conj</ta>
            <ta e="T32" id="Seg_539" s="T31">adv</ta>
            <ta e="T33" id="Seg_540" s="T32">nprop-n:case</ta>
            <ta e="T34" id="Seg_541" s="T33">v-v:tense.[v:pn]</ta>
            <ta e="T35" id="Seg_542" s="T34">n-n&gt;n-n:poss-n:case</ta>
            <ta e="T36" id="Seg_543" s="T35">v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_544" s="T36">n-n&gt;n.[n:case]-n:poss</ta>
            <ta e="T38" id="Seg_545" s="T37">v-v:tense.[v:pn]</ta>
            <ta e="T39" id="Seg_546" s="T38">pers.[n:case]</ta>
            <ta e="T40" id="Seg_547" s="T39">emphpro</ta>
            <ta e="T41" id="Seg_548" s="T40">v-v:tense.[v:pn]</ta>
            <ta e="T42" id="Seg_549" s="T41">pers-n:case</ta>
            <ta e="T43" id="Seg_550" s="T42">v-v:pn</ta>
            <ta e="T44" id="Seg_551" s="T43">n.[n:case]-n:poss</ta>
            <ta e="T45" id="Seg_552" s="T44">adj</ta>
            <ta e="T46" id="Seg_553" s="T45">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_554" s="T46">adv-adv:case</ta>
            <ta e="T48" id="Seg_555" s="T47">conj</ta>
            <ta e="T49" id="Seg_556" s="T48">v.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_557" s="T1">nprop</ta>
            <ta e="T3" id="Seg_558" s="T2">n</ta>
            <ta e="T4" id="Seg_559" s="T3">quant</ta>
            <ta e="T5" id="Seg_560" s="T4">v</ta>
            <ta e="T6" id="Seg_561" s="T5">adv</ta>
            <ta e="T7" id="Seg_562" s="T6">n</ta>
            <ta e="T8" id="Seg_563" s="T7">quant</ta>
            <ta e="T9" id="Seg_564" s="T8">v</ta>
            <ta e="T10" id="Seg_565" s="T9">pers</ta>
            <ta e="T11" id="Seg_566" s="T10">n</ta>
            <ta e="T12" id="Seg_567" s="T11">adv</ta>
            <ta e="T13" id="Seg_568" s="T12">adv</ta>
            <ta e="T14" id="Seg_569" s="T13">v</ta>
            <ta e="T15" id="Seg_570" s="T14">pers</ta>
            <ta e="T16" id="Seg_571" s="T15">n</ta>
            <ta e="T17" id="Seg_572" s="T16">adv</ta>
            <ta e="T18" id="Seg_573" s="T17">v</ta>
            <ta e="T19" id="Seg_574" s="T18">nprop</ta>
            <ta e="T20" id="Seg_575" s="T19">n</ta>
            <ta e="T21" id="Seg_576" s="T20">nprop</ta>
            <ta e="T24" id="Seg_577" s="T23">adj</ta>
            <ta e="T25" id="Seg_578" s="T24">n</ta>
            <ta e="T26" id="Seg_579" s="T25">pers</ta>
            <ta e="T27" id="Seg_580" s="T26">num</ta>
            <ta e="T28" id="Seg_581" s="T27">adv</ta>
            <ta e="T29" id="Seg_582" s="T28">pers</ta>
            <ta e="T30" id="Seg_583" s="T29">v</ta>
            <ta e="T31" id="Seg_584" s="T30">conj</ta>
            <ta e="T32" id="Seg_585" s="T31">adv</ta>
            <ta e="T33" id="Seg_586" s="T32">nprop</ta>
            <ta e="T34" id="Seg_587" s="T33">v</ta>
            <ta e="T35" id="Seg_588" s="T34">n</ta>
            <ta e="T36" id="Seg_589" s="T35">v</ta>
            <ta e="T37" id="Seg_590" s="T36">n</ta>
            <ta e="T38" id="Seg_591" s="T37">v</ta>
            <ta e="T39" id="Seg_592" s="T38">pers</ta>
            <ta e="T40" id="Seg_593" s="T39">emphpro</ta>
            <ta e="T41" id="Seg_594" s="T40">v</ta>
            <ta e="T42" id="Seg_595" s="T41">pers</ta>
            <ta e="T43" id="Seg_596" s="T42">v</ta>
            <ta e="T44" id="Seg_597" s="T43">n</ta>
            <ta e="T45" id="Seg_598" s="T44">adj</ta>
            <ta e="T46" id="Seg_599" s="T45">v</ta>
            <ta e="T47" id="Seg_600" s="T46">adv</ta>
            <ta e="T48" id="Seg_601" s="T47">conj</ta>
            <ta e="T49" id="Seg_602" s="T48">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_603" s="T1">np:L</ta>
            <ta e="T3" id="Seg_604" s="T2">np.h:Th</ta>
            <ta e="T6" id="Seg_605" s="T5">adv:L</ta>
            <ta e="T7" id="Seg_606" s="T6">np:Th</ta>
            <ta e="T10" id="Seg_607" s="T9">pro.h:Poss</ta>
            <ta e="T11" id="Seg_608" s="T10">np.h:Th</ta>
            <ta e="T12" id="Seg_609" s="T11">adv:L</ta>
            <ta e="T15" id="Seg_610" s="T14">pro.h:Poss</ta>
            <ta e="T16" id="Seg_611" s="T15">np.h:Th</ta>
            <ta e="T17" id="Seg_612" s="T16">adv:L</ta>
            <ta e="T19" id="Seg_613" s="T18">np:L</ta>
            <ta e="T20" id="Seg_614" s="T19">np.h:Th</ta>
            <ta e="T26" id="Seg_615" s="T25">pro.h:Th</ta>
            <ta e="T28" id="Seg_616" s="T27">adv:Time</ta>
            <ta e="T29" id="Seg_617" s="T28">pro.h:L</ta>
            <ta e="T32" id="Seg_618" s="T31">adv:Time</ta>
            <ta e="T33" id="Seg_619" s="T32">np:G</ta>
            <ta e="T34" id="Seg_620" s="T33">0.3.h:A</ta>
            <ta e="T35" id="Seg_621" s="T34">np:Com 0.3.h:Poss</ta>
            <ta e="T36" id="Seg_622" s="T35">0.3.h:A</ta>
            <ta e="T37" id="Seg_623" s="T36">np.h:P 0.3.h:Poss</ta>
            <ta e="T39" id="Seg_624" s="T38">pro.h:Th</ta>
            <ta e="T42" id="Seg_625" s="T41">pro.h:B</ta>
            <ta e="T43" id="Seg_626" s="T42">0.3.h:A</ta>
            <ta e="T44" id="Seg_627" s="T43">np:P 0.3.h:Poss</ta>
            <ta e="T46" id="Seg_628" s="T45">0.3.h:A</ta>
            <ta e="T47" id="Seg_629" s="T46">adv:L</ta>
            <ta e="T49" id="Seg_630" s="T48">0.3.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_631" s="T2">np.h:S</ta>
            <ta e="T5" id="Seg_632" s="T4">v:pred</ta>
            <ta e="T7" id="Seg_633" s="T6">np:S</ta>
            <ta e="T9" id="Seg_634" s="T8">v:pred</ta>
            <ta e="T11" id="Seg_635" s="T10">np.h:S</ta>
            <ta e="T14" id="Seg_636" s="T13">v:pred</ta>
            <ta e="T16" id="Seg_637" s="T15">np.h:S</ta>
            <ta e="T18" id="Seg_638" s="T17">v:pred</ta>
            <ta e="T20" id="Seg_639" s="T19">np.h:S</ta>
            <ta e="T25" id="Seg_640" s="T24">n:pred</ta>
            <ta e="T26" id="Seg_641" s="T25">pro.h:S</ta>
            <ta e="T30" id="Seg_642" s="T29">v:pred</ta>
            <ta e="T34" id="Seg_643" s="T33">0.3.h:S v:pred</ta>
            <ta e="T36" id="Seg_644" s="T35">0.3.h:S v:pred</ta>
            <ta e="T37" id="Seg_645" s="T36">np.h:S</ta>
            <ta e="T38" id="Seg_646" s="T37">v:pred</ta>
            <ta e="T39" id="Seg_647" s="T38">pro.h:S</ta>
            <ta e="T41" id="Seg_648" s="T40">v:pred</ta>
            <ta e="T43" id="Seg_649" s="T42">0.3.h:S v:pred</ta>
            <ta e="T44" id="Seg_650" s="T43">np:O</ta>
            <ta e="T46" id="Seg_651" s="T45">0.3.h:S v:pred</ta>
            <ta e="T49" id="Seg_652" s="T48">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T11" id="Seg_653" s="T10">RUS:core</ta>
            <ta e="T31" id="Seg_654" s="T30">RUS:gram</ta>
            <ta e="T32" id="Seg_655" s="T31">RUS:core</ta>
            <ta e="T48" id="Seg_656" s="T47">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_657" s="T1">There are many Selkups on Tym.</ta>
            <ta e="T9" id="Seg_658" s="T5">There is much fish there.</ta>
            <ta e="T14" id="Seg_659" s="T9">Many of my relatives live there.</ta>
            <ta e="T19" id="Seg_660" s="T14">There lives my brother, in Napas.</ta>
            <ta e="T25" id="Seg_661" s="T19">Nastyona Kajdalova is the most native Selkup woman.</ta>
            <ta e="T30" id="Seg_662" s="T25">She spent four summers with me.</ta>
            <ta e="T34" id="Seg_663" s="T30">And now she left to Tym.</ta>
            <ta e="T36" id="Seg_664" s="T34">She left with her husband.</ta>
            <ta e="T38" id="Seg_665" s="T36">Her husband died.</ta>
            <ta e="T41" id="Seg_666" s="T38">She stayed alone.</ta>
            <ta e="T46" id="Seg_667" s="T41">It is said, that [her husband] had built a good house.</ta>
            <ta e="T49" id="Seg_668" s="T46">And she lives there.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_669" s="T1">Am Tym gibt es viele Selkupen.</ta>
            <ta e="T9" id="Seg_670" s="T5">Dort gibt es viel Fisch.</ta>
            <ta e="T14" id="Seg_671" s="T9">Viele meiner Verwandten leben dort.</ta>
            <ta e="T19" id="Seg_672" s="T14">Mein Bruder lebt dort, in Napas.</ta>
            <ta e="T25" id="Seg_673" s="T19">Nastyona Kajdalova ist die ursprünglichste selkupische Frau.</ta>
            <ta e="T30" id="Seg_674" s="T25">Sie lebte vier Sommer bei mir.</ta>
            <ta e="T34" id="Seg_675" s="T30">Und jetzt ist sie zum Tym gegangen.</ta>
            <ta e="T36" id="Seg_676" s="T34">Sie ging mit ihrem Mann.</ta>
            <ta e="T38" id="Seg_677" s="T36">Ihr Mann ist gestorben.</ta>
            <ta e="T41" id="Seg_678" s="T38">Sie blieb allein.</ta>
            <ta e="T46" id="Seg_679" s="T41">Man sagt, dass [ihr Mann] ein gutes Haus gebaut hat.</ta>
            <ta e="T49" id="Seg_680" s="T46">Und dort lebt sie.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_681" s="T1">В Тыму селькупов много.</ta>
            <ta e="T9" id="Seg_682" s="T5">Там рыбы много.</ta>
            <ta e="T14" id="Seg_683" s="T9">Моих родственников там много живет.</ta>
            <ta e="T19" id="Seg_684" s="T14">Там мой брат живет, в Напасе.</ta>
            <ta e="T25" id="Seg_685" s="T19">Настена Кайдалова – самая коренная селькупская женщина.</ta>
            <ta e="T30" id="Seg_686" s="T25">Она четыре лета у меня жила.</ta>
            <ta e="T34" id="Seg_687" s="T30">А теперь она в Тым уехала.</ta>
            <ta e="T36" id="Seg_688" s="T34">С мужем уехали.</ta>
            <ta e="T38" id="Seg_689" s="T36">Муж ее умер.</ta>
            <ta e="T41" id="Seg_690" s="T38">Она одна осталась.</ta>
            <ta e="T46" id="Seg_691" s="T41">У нее, говорят, дом хороший поставил (муж).</ta>
            <ta e="T49" id="Seg_692" s="T46">Там и живет.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_693" s="T1">в Тыму остяков много</ta>
            <ta e="T9" id="Seg_694" s="T5">там говорят рыбы много</ta>
            <ta e="T14" id="Seg_695" s="T9">моих родственников там много живет</ta>
            <ta e="T19" id="Seg_696" s="T14">там мой брат(аник?) живет в Напасе</ta>
            <ta e="T25" id="Seg_697" s="T19">самая коренная остяцка</ta>
            <ta e="T30" id="Seg_698" s="T25">она четыре лета у меня жила</ta>
            <ta e="T34" id="Seg_699" s="T30">а теперь в Тым уехала</ta>
            <ta e="T36" id="Seg_700" s="T34">со стариком уехали</ta>
            <ta e="T38" id="Seg_701" s="T36">старик помер</ta>
            <ta e="T41" id="Seg_702" s="T38">она одна осталась</ta>
            <ta e="T46" id="Seg_703" s="T41">у нее говорят дом хороший дом поставил (старик)</ta>
            <ta e="T49" id="Seg_704" s="T46">там и живет</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto">
            <ta e="T19" id="Seg_705" s="T14">Иван Федорович Кайдаров (60 лет), Люба жена</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
