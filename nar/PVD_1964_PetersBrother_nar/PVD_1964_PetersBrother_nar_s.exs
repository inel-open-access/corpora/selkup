<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_PetersBrother_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_PetersBrother_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">95</ud-information>
            <ud-information attribute-name="# HIAT:w">65</ud-information>
            <ud-information attribute-name="# e">65</ud-information>
            <ud-information attribute-name="# HIAT:u">17</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T66" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Pʼätroj</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">agau</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">mekga</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">sedʼäptan</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Tʼärɨn</ts>
                  <nts id="Seg_20" n="HIAT:ip">:</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_22" n="HIAT:ip">“</nts>
                  <ts e="T7" id="Seg_24" n="HIAT:w" s="T6">Qudə</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_27" n="HIAT:w" s="T7">margʼeːsʼemɨla</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">qwälʼe</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_33" n="HIAT:w" s="T9">sottattə</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_36" n="HIAT:w" s="T10">me</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_39" n="HIAT:w" s="T11">tʼöroɣontu</ts>
                  <nts id="Seg_40" n="HIAT:ip">.</nts>
                  <nts id="Seg_41" n="HIAT:ip">”</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_44" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">Man</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">maːttə</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">kuronnan</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">tʼötǯɨkan</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_59" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">Vanʼänä</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">tʼäran</ts>
                  <nts id="Seg_65" n="HIAT:ip">:</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_67" n="HIAT:ip">“</nts>
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">Qwallaj</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">qwälʼe</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">sottɨgu</ts>
                  <nts id="Seg_76" n="HIAT:ip">.</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_79" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_81" n="HIAT:w" s="T21">Me</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">tʼöroɣontu</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_87" n="HIAT:w" s="T23">qwälʼe</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_90" n="HIAT:w" s="T24">sottattə</ts>
                  <nts id="Seg_91" n="HIAT:ip">.</nts>
                  <nts id="Seg_92" n="HIAT:ip">”</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_95" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_97" n="HIAT:w" s="T25">Kütdɨm</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_100" n="HIAT:w" s="T26">taqtolǯaj</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">i</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">qwannaj</ts>
                  <nts id="Seg_107" n="HIAT:ip">.</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_110" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_112" n="HIAT:w" s="T29">Tʼörot</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">tʼülʼe</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_118" n="HIAT:w" s="T31">medaj</ts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_122" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_124" n="HIAT:w" s="T32">Lɨːban</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">jen</ts>
                  <nts id="Seg_128" n="HIAT:ip">.</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_131" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_133" n="HIAT:w" s="T34">Qaj</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_136" n="HIAT:w" s="T35">qumnäj</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_139" n="HIAT:w" s="T36">tʼäkgu</ts>
                  <nts id="Seg_140" n="HIAT:ip">.</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_143" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_145" n="HIAT:w" s="T37">Aramom</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_148" n="HIAT:w" s="T38">mewaj</ts>
                  <nts id="Seg_149" n="HIAT:ip">.</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_152" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_154" n="HIAT:w" s="T39">I</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_157" n="HIAT:w" s="T40">qaj</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_160" n="HIAT:w" s="T41">qwälnäj</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_163" n="HIAT:w" s="T42">tʼäkgu</ts>
                  <nts id="Seg_164" n="HIAT:ip">.</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_167" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_169" n="HIAT:w" s="T43">Täp</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_172" n="HIAT:w" s="T44">mazɨm</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_175" n="HIAT:w" s="T45">aːlɨpba</ts>
                  <nts id="Seg_176" n="HIAT:ip">.</nts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_179" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_181" n="HIAT:w" s="T46">I</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_184" n="HIAT:w" s="T47">jedot</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_187" n="HIAT:w" s="T48">tʼüwaj</ts>
                  <nts id="Seg_188" n="HIAT:ip">.</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_191" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_193" n="HIAT:w" s="T49">Man</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_196" n="HIAT:w" s="T50">tabnä</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_199" n="HIAT:w" s="T51">tʼaran</ts>
                  <nts id="Seg_200" n="HIAT:ip">:</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_202" n="HIAT:ip">“</nts>
                  <ts e="T53" id="Seg_204" n="HIAT:w" s="T52">Qajno</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_207" n="HIAT:w" s="T53">mazɨm</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_210" n="HIAT:w" s="T54">aːlɨnat</ts>
                  <nts id="Seg_211" n="HIAT:ip">?</nts>
                  <nts id="Seg_212" n="HIAT:ip">”</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_215" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_217" n="HIAT:w" s="T55">A</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_220" n="HIAT:w" s="T56">täp</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_223" n="HIAT:w" s="T57">lagwatpa</ts>
                  <nts id="Seg_224" n="HIAT:ip">.</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_227" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_229" n="HIAT:w" s="T58">Tʼärɨn</ts>
                  <nts id="Seg_230" n="HIAT:ip">:</nts>
                  <nts id="Seg_231" n="HIAT:ip">“</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_233" n="HIAT:ip">“</nts>
                  <ts e="T60" id="Seg_235" n="HIAT:w" s="T59">Tan</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_238" n="HIAT:w" s="T60">šadnan</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_241" n="HIAT:w" s="T61">jeqwat</ts>
                  <nts id="Seg_242" n="HIAT:ip">.</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_245" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_247" n="HIAT:w" s="T62">Man</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_250" n="HIAT:w" s="T63">nano</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_253" n="HIAT:w" s="T64">stə</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_256" n="HIAT:w" s="T65">aːlɨsan</ts>
                  <nts id="Seg_257" n="HIAT:ip">.</nts>
                  <nts id="Seg_258" n="HIAT:ip">”</nts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T66" id="Seg_260" n="sc" s="T1">
               <ts e="T2" id="Seg_262" n="e" s="T1">Pʼätroj </ts>
               <ts e="T3" id="Seg_264" n="e" s="T2">agau </ts>
               <ts e="T4" id="Seg_266" n="e" s="T3">mekga </ts>
               <ts e="T5" id="Seg_268" n="e" s="T4">sedʼäptan. </ts>
               <ts e="T6" id="Seg_270" n="e" s="T5">Tʼärɨn: </ts>
               <ts e="T7" id="Seg_272" n="e" s="T6">“Qudə </ts>
               <ts e="T8" id="Seg_274" n="e" s="T7">margʼeːsʼemɨla </ts>
               <ts e="T9" id="Seg_276" n="e" s="T8">qwälʼe </ts>
               <ts e="T10" id="Seg_278" n="e" s="T9">sottattə </ts>
               <ts e="T11" id="Seg_280" n="e" s="T10">me </ts>
               <ts e="T12" id="Seg_282" n="e" s="T11">tʼöroɣontu.” </ts>
               <ts e="T13" id="Seg_284" n="e" s="T12">Man </ts>
               <ts e="T14" id="Seg_286" n="e" s="T13">maːttə </ts>
               <ts e="T15" id="Seg_288" n="e" s="T14">kuronnan </ts>
               <ts e="T16" id="Seg_290" n="e" s="T15">tʼötǯɨkan. </ts>
               <ts e="T17" id="Seg_292" n="e" s="T16">Vanʼänä </ts>
               <ts e="T18" id="Seg_294" n="e" s="T17">tʼäran: </ts>
               <ts e="T19" id="Seg_296" n="e" s="T18">“Qwallaj </ts>
               <ts e="T20" id="Seg_298" n="e" s="T19">qwälʼe </ts>
               <ts e="T21" id="Seg_300" n="e" s="T20">sottɨgu. </ts>
               <ts e="T22" id="Seg_302" n="e" s="T21">Me </ts>
               <ts e="T23" id="Seg_304" n="e" s="T22">tʼöroɣontu </ts>
               <ts e="T24" id="Seg_306" n="e" s="T23">qwälʼe </ts>
               <ts e="T25" id="Seg_308" n="e" s="T24">sottattə.” </ts>
               <ts e="T26" id="Seg_310" n="e" s="T25">Kütdɨm </ts>
               <ts e="T27" id="Seg_312" n="e" s="T26">taqtolǯaj </ts>
               <ts e="T28" id="Seg_314" n="e" s="T27">i </ts>
               <ts e="T29" id="Seg_316" n="e" s="T28">qwannaj. </ts>
               <ts e="T30" id="Seg_318" n="e" s="T29">Tʼörot </ts>
               <ts e="T31" id="Seg_320" n="e" s="T30">tʼülʼe </ts>
               <ts e="T32" id="Seg_322" n="e" s="T31">medaj. </ts>
               <ts e="T33" id="Seg_324" n="e" s="T32">Lɨːban </ts>
               <ts e="T34" id="Seg_326" n="e" s="T33">jen. </ts>
               <ts e="T35" id="Seg_328" n="e" s="T34">Qaj </ts>
               <ts e="T36" id="Seg_330" n="e" s="T35">qumnäj </ts>
               <ts e="T37" id="Seg_332" n="e" s="T36">tʼäkgu. </ts>
               <ts e="T38" id="Seg_334" n="e" s="T37">Aramom </ts>
               <ts e="T39" id="Seg_336" n="e" s="T38">mewaj. </ts>
               <ts e="T40" id="Seg_338" n="e" s="T39">I </ts>
               <ts e="T41" id="Seg_340" n="e" s="T40">qaj </ts>
               <ts e="T42" id="Seg_342" n="e" s="T41">qwälnäj </ts>
               <ts e="T43" id="Seg_344" n="e" s="T42">tʼäkgu. </ts>
               <ts e="T44" id="Seg_346" n="e" s="T43">Täp </ts>
               <ts e="T45" id="Seg_348" n="e" s="T44">mazɨm </ts>
               <ts e="T46" id="Seg_350" n="e" s="T45">aːlɨpba. </ts>
               <ts e="T47" id="Seg_352" n="e" s="T46">I </ts>
               <ts e="T48" id="Seg_354" n="e" s="T47">jedot </ts>
               <ts e="T49" id="Seg_356" n="e" s="T48">tʼüwaj. </ts>
               <ts e="T50" id="Seg_358" n="e" s="T49">Man </ts>
               <ts e="T51" id="Seg_360" n="e" s="T50">tabnä </ts>
               <ts e="T52" id="Seg_362" n="e" s="T51">tʼaran: </ts>
               <ts e="T53" id="Seg_364" n="e" s="T52">“Qajno </ts>
               <ts e="T54" id="Seg_366" n="e" s="T53">mazɨm </ts>
               <ts e="T55" id="Seg_368" n="e" s="T54">aːlɨnat?” </ts>
               <ts e="T56" id="Seg_370" n="e" s="T55">A </ts>
               <ts e="T57" id="Seg_372" n="e" s="T56">täp </ts>
               <ts e="T58" id="Seg_374" n="e" s="T57">lagwatpa. </ts>
               <ts e="T59" id="Seg_376" n="e" s="T58">Tʼärɨn:“ </ts>
               <ts e="T60" id="Seg_378" n="e" s="T59">“Tan </ts>
               <ts e="T61" id="Seg_380" n="e" s="T60">šadnan </ts>
               <ts e="T62" id="Seg_382" n="e" s="T61">jeqwat. </ts>
               <ts e="T63" id="Seg_384" n="e" s="T62">Man </ts>
               <ts e="T64" id="Seg_386" n="e" s="T63">nano </ts>
               <ts e="T65" id="Seg_388" n="e" s="T64">stə </ts>
               <ts e="T66" id="Seg_390" n="e" s="T65">aːlɨsan.” </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_391" s="T1">PVD_1964_PetersBrother_nar.001 (001.001)</ta>
            <ta e="T12" id="Seg_392" s="T5">PVD_1964_PetersBrother_nar.002 (001.002)</ta>
            <ta e="T16" id="Seg_393" s="T12">PVD_1964_PetersBrother_nar.003 (001.003)</ta>
            <ta e="T21" id="Seg_394" s="T16">PVD_1964_PetersBrother_nar.004 (001.004)</ta>
            <ta e="T25" id="Seg_395" s="T21">PVD_1964_PetersBrother_nar.005 (001.005)</ta>
            <ta e="T29" id="Seg_396" s="T25">PVD_1964_PetersBrother_nar.006 (001.006)</ta>
            <ta e="T32" id="Seg_397" s="T29">PVD_1964_PetersBrother_nar.007 (001.007)</ta>
            <ta e="T34" id="Seg_398" s="T32">PVD_1964_PetersBrother_nar.008 (001.008)</ta>
            <ta e="T37" id="Seg_399" s="T34">PVD_1964_PetersBrother_nar.009 (001.009)</ta>
            <ta e="T39" id="Seg_400" s="T37">PVD_1964_PetersBrother_nar.010 (001.010)</ta>
            <ta e="T43" id="Seg_401" s="T39">PVD_1964_PetersBrother_nar.011 (001.011)</ta>
            <ta e="T46" id="Seg_402" s="T43">PVD_1964_PetersBrother_nar.012 (001.012)</ta>
            <ta e="T49" id="Seg_403" s="T46">PVD_1964_PetersBrother_nar.013 (001.013)</ta>
            <ta e="T55" id="Seg_404" s="T49">PVD_1964_PetersBrother_nar.014 (001.014)</ta>
            <ta e="T58" id="Seg_405" s="T55">PVD_1964_PetersBrother_nar.015 (001.015)</ta>
            <ta e="T62" id="Seg_406" s="T58">PVD_1964_PetersBrother_nar.016 (001.016)</ta>
            <ta e="T66" id="Seg_407" s="T62">PVD_1964_PetersBrother_nar.017 (001.017)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_408" s="T1">Пʼӓт′рой а′гау̹ ′мекга се′дʼӓптан.</ta>
            <ta e="T12" id="Seg_409" s="T5">тʼӓ′рын: ′kудъ мар′гʼе̄сʼемы′ла kwӓлʼе ′соттаттъ ме ′тʼӧроɣонту.</ta>
            <ta e="T16" id="Seg_410" s="T12">ман ′ма̄ттъ курон′нан ′тʼӧтджыкан.</ta>
            <ta e="T21" id="Seg_411" s="T16">Ванʼӓ′нӓ тʼӓ′ран: kwа′ллай ′kwӓлʼе ′соттыгу.</ta>
            <ta e="T25" id="Seg_412" s="T21">ме ′тʼӧроɣонту ′kwӓлʼе ′соттаттъ.</ta>
            <ta e="T29" id="Seg_413" s="T25">кӱт′дым таk′толджай и kwа′ннай.</ta>
            <ta e="T32" id="Seg_414" s="T29">′тʼӧрот ′тʼӱлʼе ме′дай.</ta>
            <ta e="T34" id="Seg_415" s="T32">′лы̄бан jен.</ta>
            <ta e="T37" id="Seg_416" s="T34">kай kум ′нӓй ′тʼӓкгу.</ta>
            <ta e="T39" id="Seg_417" s="T37">ара′мом ′мевай.</ta>
            <ta e="T43" id="Seg_418" s="T39">и ′kай ′kwӓлнӓй ′тʼӓкгу.</ta>
            <ta e="T46" id="Seg_419" s="T43">тӓп ма′зым ′а̄лыпба.</ta>
            <ta e="T49" id="Seg_420" s="T46">и ′jедот ′тʼӱвай.</ta>
            <ta e="T55" id="Seg_421" s="T49">ман таб′нӓ тʼа′ран: kай′но ма′зым ′а̄лынат?</ta>
            <ta e="T58" id="Seg_422" s="T55">а тӓп ′лагватпа.</ta>
            <ta e="T62" id="Seg_423" s="T58">тʼӓ′рын: тан шад̂нан jеkват.</ta>
            <ta e="T66" id="Seg_424" s="T62">ман на′ностъ ′а̄лысан.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_425" s="T1">Пʼätroj agau̹ mekga sedʼäptan.</ta>
            <ta e="T12" id="Seg_426" s="T5">tʼärɨn: qudə margʼeːsʼemɨla qwälʼe sottattə me tʼöroɣontu.</ta>
            <ta e="T16" id="Seg_427" s="T12">man maːttə kuronnan tʼötǯɨkan.</ta>
            <ta e="T21" id="Seg_428" s="T16">Вanʼänä tʼäran: qwallaj qwälʼe sottɨgu.</ta>
            <ta e="T25" id="Seg_429" s="T21">me tʼöroɣontu qwälʼe sottattə.</ta>
            <ta e="T29" id="Seg_430" s="T25">kütdɨm taqtolǯaj i qwannaj.</ta>
            <ta e="T32" id="Seg_431" s="T29">tʼörot tʼülʼe medaj.</ta>
            <ta e="T34" id="Seg_432" s="T32">lɨːban jen.</ta>
            <ta e="T37" id="Seg_433" s="T34">qaj qum näj tʼäkgu.</ta>
            <ta e="T39" id="Seg_434" s="T37">aramom mewaj.</ta>
            <ta e="T43" id="Seg_435" s="T39">i qaj qwälnäj tʼäkgu.</ta>
            <ta e="T46" id="Seg_436" s="T43">täp mazɨm aːlɨpba.</ta>
            <ta e="T49" id="Seg_437" s="T46">i jedot tʼüwaj.</ta>
            <ta e="T55" id="Seg_438" s="T49">man tabnä tʼaran: qajno mazɨm aːlɨnat?</ta>
            <ta e="T58" id="Seg_439" s="T55">a täp lagwatpa.</ta>
            <ta e="T62" id="Seg_440" s="T58">tʼärɨn: tan šad̂nan jeqwat.</ta>
            <ta e="T66" id="Seg_441" s="T62">man nanostə aːlɨsan.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_442" s="T1">Pʼätroj agau mekga sedʼäptan. </ta>
            <ta e="T12" id="Seg_443" s="T5">Tʼärɨn: “Qudə margʼeːsʼemɨla qwälʼe sottattə me tʼöroɣontu.” </ta>
            <ta e="T16" id="Seg_444" s="T12">Man maːttə kuronnan tʼötǯɨkan. </ta>
            <ta e="T21" id="Seg_445" s="T16">Vanʼänä tʼäran: “Qwallaj qwälʼe sottɨgu. </ta>
            <ta e="T25" id="Seg_446" s="T21">Me tʼöroɣontu qwälʼe sottattə.” </ta>
            <ta e="T29" id="Seg_447" s="T25">Kütdɨm taqtolǯaj i qwannaj. </ta>
            <ta e="T32" id="Seg_448" s="T29">Tʼörot tʼülʼe medaj. </ta>
            <ta e="T34" id="Seg_449" s="T32">Lɨːban jen. </ta>
            <ta e="T37" id="Seg_450" s="T34">Qaj qumnäj tʼäkgu. </ta>
            <ta e="T39" id="Seg_451" s="T37">Aramom mewaj. </ta>
            <ta e="T43" id="Seg_452" s="T39">I qaj qwälnäj tʼäkgu. </ta>
            <ta e="T46" id="Seg_453" s="T43">Täp mazɨm aːlɨpba. </ta>
            <ta e="T49" id="Seg_454" s="T46">I jedot tʼüwaj. </ta>
            <ta e="T55" id="Seg_455" s="T49">Man tabnä tʼaran: “Qajno mazɨm aːlɨnat?” </ta>
            <ta e="T58" id="Seg_456" s="T55">A täp lagwatpa. </ta>
            <ta e="T62" id="Seg_457" s="T58">Tʼärɨn:“ “Tan šadnan jeqwat. </ta>
            <ta e="T66" id="Seg_458" s="T62">Man nano stə aːlɨsan.” </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_459" s="T1">Pʼätro-j</ta>
            <ta e="T3" id="Seg_460" s="T2">aga-u</ta>
            <ta e="T4" id="Seg_461" s="T3">mekga</ta>
            <ta e="T5" id="Seg_462" s="T4">sedʼäpta-n</ta>
            <ta e="T6" id="Seg_463" s="T5">tʼärɨ-n</ta>
            <ta e="T7" id="Seg_464" s="T6">qudə</ta>
            <ta e="T8" id="Seg_465" s="T7">margʼeːsʼemɨla</ta>
            <ta e="T9" id="Seg_466" s="T8">qwälʼe</ta>
            <ta e="T10" id="Seg_467" s="T9">sotta-ttə</ta>
            <ta e="T11" id="Seg_468" s="T10">me</ta>
            <ta e="T12" id="Seg_469" s="T11">tʼör-o-ɣontu</ta>
            <ta e="T13" id="Seg_470" s="T12">man</ta>
            <ta e="T14" id="Seg_471" s="T13">maːt-tə</ta>
            <ta e="T15" id="Seg_472" s="T14">kur-on-na-n</ta>
            <ta e="T16" id="Seg_473" s="T15">tʼötǯɨkan</ta>
            <ta e="T17" id="Seg_474" s="T16">Vanʼä-nä</ta>
            <ta e="T18" id="Seg_475" s="T17">tʼära-n</ta>
            <ta e="T19" id="Seg_476" s="T18">qwal-la-j</ta>
            <ta e="T20" id="Seg_477" s="T19">qwälʼe</ta>
            <ta e="T21" id="Seg_478" s="T20">sottɨ-gu</ta>
            <ta e="T22" id="Seg_479" s="T21">me</ta>
            <ta e="T23" id="Seg_480" s="T22">tʼör-o-ɣontu</ta>
            <ta e="T24" id="Seg_481" s="T23">qwälʼe</ta>
            <ta e="T25" id="Seg_482" s="T24">sotta-ttə</ta>
            <ta e="T26" id="Seg_483" s="T25">kütdɨ-m</ta>
            <ta e="T27" id="Seg_484" s="T26">taqtolǯa-j</ta>
            <ta e="T28" id="Seg_485" s="T27">i</ta>
            <ta e="T29" id="Seg_486" s="T28">qwan-na-j</ta>
            <ta e="T30" id="Seg_487" s="T29">tʼör-o-t</ta>
            <ta e="T31" id="Seg_488" s="T30">tʼü-lʼe</ta>
            <ta e="T32" id="Seg_489" s="T31">meda-j</ta>
            <ta e="T33" id="Seg_490" s="T32">lɨːba-n</ta>
            <ta e="T34" id="Seg_491" s="T33">je-n</ta>
            <ta e="T35" id="Seg_492" s="T34">qaj</ta>
            <ta e="T36" id="Seg_493" s="T35">qum-näj</ta>
            <ta e="T37" id="Seg_494" s="T36">tʼäkgu</ta>
            <ta e="T38" id="Seg_495" s="T37">aramo-m</ta>
            <ta e="T39" id="Seg_496" s="T38">me-wa-j</ta>
            <ta e="T40" id="Seg_497" s="T39">i</ta>
            <ta e="T41" id="Seg_498" s="T40">qaj</ta>
            <ta e="T42" id="Seg_499" s="T41">qwäl-näj</ta>
            <ta e="T43" id="Seg_500" s="T42">tʼäkgu</ta>
            <ta e="T44" id="Seg_501" s="T43">täp</ta>
            <ta e="T45" id="Seg_502" s="T44">mazɨm</ta>
            <ta e="T46" id="Seg_503" s="T45">aːlɨ-pba</ta>
            <ta e="T47" id="Seg_504" s="T46">i</ta>
            <ta e="T48" id="Seg_505" s="T47">jedo-t</ta>
            <ta e="T49" id="Seg_506" s="T48">tʼü-wa-j</ta>
            <ta e="T50" id="Seg_507" s="T49">man</ta>
            <ta e="T51" id="Seg_508" s="T50">tab-nä</ta>
            <ta e="T52" id="Seg_509" s="T51">tʼara-n</ta>
            <ta e="T53" id="Seg_510" s="T52">qaj-no</ta>
            <ta e="T54" id="Seg_511" s="T53">mazɨm</ta>
            <ta e="T55" id="Seg_512" s="T54">aːlɨ-na-t</ta>
            <ta e="T56" id="Seg_513" s="T55">a</ta>
            <ta e="T57" id="Seg_514" s="T56">täp</ta>
            <ta e="T58" id="Seg_515" s="T57">lagwat-pa</ta>
            <ta e="T59" id="Seg_516" s="T58">tʼärɨ-n</ta>
            <ta e="T60" id="Seg_517" s="T59">Tan</ta>
            <ta e="T61" id="Seg_518" s="T60">šadna-n</ta>
            <ta e="T62" id="Seg_519" s="T61">je-q-wa-t</ta>
            <ta e="T63" id="Seg_520" s="T62">man</ta>
            <ta e="T64" id="Seg_521" s="T63">nano</ta>
            <ta e="T65" id="Seg_522" s="T64">stə</ta>
            <ta e="T66" id="Seg_523" s="T65">aːlɨ-sa-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_524" s="T1">Petra-lʼ</ta>
            <ta e="T3" id="Seg_525" s="T2">agaː-w</ta>
            <ta e="T4" id="Seg_526" s="T3">mekka</ta>
            <ta e="T5" id="Seg_527" s="T4">sedʼäptə-n</ta>
            <ta e="T6" id="Seg_528" s="T5">tʼärɨ-n</ta>
            <ta e="T7" id="Seg_529" s="T6">kud</ta>
            <ta e="T8" id="Seg_530" s="T7">margʼeːsʼemɨla</ta>
            <ta e="T9" id="Seg_531" s="T8">qwɛl</ta>
            <ta e="T10" id="Seg_532" s="T9">sottɨ-tɨn</ta>
            <ta e="T11" id="Seg_533" s="T10">me</ta>
            <ta e="T12" id="Seg_534" s="T11">tʼör-ɨ-quntu</ta>
            <ta e="T13" id="Seg_535" s="T12">man</ta>
            <ta e="T14" id="Seg_536" s="T13">maːt-ntə</ta>
            <ta e="T15" id="Seg_537" s="T14">kur-ol-nɨ-ŋ</ta>
            <ta e="T16" id="Seg_538" s="T15">tʼötʒɨkaŋ</ta>
            <ta e="T17" id="Seg_539" s="T16">Vanʼä-nä</ta>
            <ta e="T18" id="Seg_540" s="T17">tʼärɨ-ŋ</ta>
            <ta e="T19" id="Seg_541" s="T18">qwan-lä-j</ta>
            <ta e="T20" id="Seg_542" s="T19">qwɛl</ta>
            <ta e="T21" id="Seg_543" s="T20">sottɨ-gu</ta>
            <ta e="T22" id="Seg_544" s="T21">me</ta>
            <ta e="T23" id="Seg_545" s="T22">tʼör-ɨ-quntu</ta>
            <ta e="T24" id="Seg_546" s="T23">qwɛl</ta>
            <ta e="T25" id="Seg_547" s="T24">sottɨ-tɨn</ta>
            <ta e="T26" id="Seg_548" s="T25">kütdə-m</ta>
            <ta e="T27" id="Seg_549" s="T26">taqtolǯu-j</ta>
            <ta e="T28" id="Seg_550" s="T27">i</ta>
            <ta e="T29" id="Seg_551" s="T28">qwan-nɨ-j</ta>
            <ta e="T30" id="Seg_552" s="T29">tʼör-ɨ-ntə</ta>
            <ta e="T31" id="Seg_553" s="T30">tüː-le</ta>
            <ta e="T32" id="Seg_554" s="T31">medə-j</ta>
            <ta e="T33" id="Seg_555" s="T32">lɨba-ŋ</ta>
            <ta e="T34" id="Seg_556" s="T33">eː-n</ta>
            <ta e="T35" id="Seg_557" s="T34">qaj</ta>
            <ta e="T36" id="Seg_558" s="T35">qum-näj</ta>
            <ta e="T37" id="Seg_559" s="T36">tʼäkku</ta>
            <ta e="T38" id="Seg_560" s="T37">aramo-m</ta>
            <ta e="T39" id="Seg_561" s="T38">meː-nɨ-j</ta>
            <ta e="T40" id="Seg_562" s="T39">i</ta>
            <ta e="T41" id="Seg_563" s="T40">qaj</ta>
            <ta e="T42" id="Seg_564" s="T41">qwɛl-näj</ta>
            <ta e="T43" id="Seg_565" s="T42">tʼäkku</ta>
            <ta e="T44" id="Seg_566" s="T43">täp</ta>
            <ta e="T45" id="Seg_567" s="T44">mazɨm</ta>
            <ta e="T46" id="Seg_568" s="T45">aːlu-mbɨ</ta>
            <ta e="T47" id="Seg_569" s="T46">i</ta>
            <ta e="T48" id="Seg_570" s="T47">eːde-ntə</ta>
            <ta e="T49" id="Seg_571" s="T48">tüː-nɨ-j</ta>
            <ta e="T50" id="Seg_572" s="T49">man</ta>
            <ta e="T51" id="Seg_573" s="T50">täp-nä</ta>
            <ta e="T52" id="Seg_574" s="T51">tʼärɨ-ŋ</ta>
            <ta e="T53" id="Seg_575" s="T52">qaj-no</ta>
            <ta e="T54" id="Seg_576" s="T53">mazɨm</ta>
            <ta e="T55" id="Seg_577" s="T54">aːlu-nɨ-ntə</ta>
            <ta e="T56" id="Seg_578" s="T55">a</ta>
            <ta e="T57" id="Seg_579" s="T56">täp</ta>
            <ta e="T58" id="Seg_580" s="T57">laːɣwat-mbɨ</ta>
            <ta e="T59" id="Seg_581" s="T58">tʼärɨ-n</ta>
            <ta e="T60" id="Seg_582" s="T59">tan</ta>
            <ta e="T61" id="Seg_583" s="T60">šadna-ŋ</ta>
            <ta e="T62" id="Seg_584" s="T61">eː-ku-nɨ-ntə</ta>
            <ta e="T63" id="Seg_585" s="T62">man</ta>
            <ta e="T64" id="Seg_586" s="T63">nanoː</ta>
            <ta e="T65" id="Seg_587" s="T64">tastɨ</ta>
            <ta e="T66" id="Seg_588" s="T65">aːlu-sɨ-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_589" s="T1">Pjotr-ADJZ</ta>
            <ta e="T3" id="Seg_590" s="T2">brother.[NOM]-1SG</ta>
            <ta e="T4" id="Seg_591" s="T3">I.ALL</ta>
            <ta e="T5" id="Seg_592" s="T4">tell.a.lie-3SG.S</ta>
            <ta e="T6" id="Seg_593" s="T5">say-3SG.S</ta>
            <ta e="T7" id="Seg_594" s="T6">who.[NOM]</ta>
            <ta e="T8" id="Seg_595" s="T7">((unknown)).[NOM]</ta>
            <ta e="T9" id="Seg_596" s="T8">fish.[NOM]</ta>
            <ta e="T10" id="Seg_597" s="T9">draw.up-3PL</ta>
            <ta e="T11" id="Seg_598" s="T10">we.GEN</ta>
            <ta e="T12" id="Seg_599" s="T11">lake-EP-LOC.1PL</ta>
            <ta e="T13" id="Seg_600" s="T12">I.NOM</ta>
            <ta e="T14" id="Seg_601" s="T13">house-ILL</ta>
            <ta e="T15" id="Seg_602" s="T14">run-MOM-CO-1SG.S</ta>
            <ta e="T16" id="Seg_603" s="T15">slowly</ta>
            <ta e="T17" id="Seg_604" s="T16">Vanya-ALL</ta>
            <ta e="T18" id="Seg_605" s="T17">say-1SG.S</ta>
            <ta e="T19" id="Seg_606" s="T18">leave-OPT-1DU</ta>
            <ta e="T20" id="Seg_607" s="T19">fish.[NOM]</ta>
            <ta e="T21" id="Seg_608" s="T20">draw.up-INF</ta>
            <ta e="T22" id="Seg_609" s="T21">we.GEN</ta>
            <ta e="T23" id="Seg_610" s="T22">lake-EP-LOC.1PL</ta>
            <ta e="T24" id="Seg_611" s="T23">fish.[NOM]</ta>
            <ta e="T25" id="Seg_612" s="T24">draw.up-3PL</ta>
            <ta e="T26" id="Seg_613" s="T25">horse-ACC</ta>
            <ta e="T27" id="Seg_614" s="T26">harness-1DU</ta>
            <ta e="T28" id="Seg_615" s="T27">and</ta>
            <ta e="T29" id="Seg_616" s="T28">leave-CO-1DU</ta>
            <ta e="T30" id="Seg_617" s="T29">lake-EP-ILL</ta>
            <ta e="T31" id="Seg_618" s="T30">come-CVB</ta>
            <ta e="T32" id="Seg_619" s="T31">achieve-1DU</ta>
            <ta e="T33" id="Seg_620" s="T32">dark-ADVZ</ta>
            <ta e="T34" id="Seg_621" s="T33">be-3SG.S</ta>
            <ta e="T35" id="Seg_622" s="T34">what.[NOM]</ta>
            <ta e="T36" id="Seg_623" s="T35">human.being.[NOM]-EMPH</ta>
            <ta e="T37" id="Seg_624" s="T36">NEG.EX.[3SG.S]</ta>
            <ta e="T38" id="Seg_625" s="T37">ice_hole-ACC</ta>
            <ta e="T39" id="Seg_626" s="T38">do-CO-1DU</ta>
            <ta e="T40" id="Seg_627" s="T39">and</ta>
            <ta e="T41" id="Seg_628" s="T40">what.[NOM]</ta>
            <ta e="T42" id="Seg_629" s="T41">fish.[NOM]-EMPH</ta>
            <ta e="T43" id="Seg_630" s="T42">NEG.EX.[3SG.S]</ta>
            <ta e="T44" id="Seg_631" s="T43">(s)he.[NOM]</ta>
            <ta e="T45" id="Seg_632" s="T44">I.ACC</ta>
            <ta e="T46" id="Seg_633" s="T45">cheat-PST.NAR.[3SG.S]</ta>
            <ta e="T47" id="Seg_634" s="T46">and</ta>
            <ta e="T48" id="Seg_635" s="T47">village-ILL</ta>
            <ta e="T49" id="Seg_636" s="T48">come-CO-1DU</ta>
            <ta e="T50" id="Seg_637" s="T49">I.NOM</ta>
            <ta e="T51" id="Seg_638" s="T50">(s)he-ALL</ta>
            <ta e="T52" id="Seg_639" s="T51">say-1SG.S</ta>
            <ta e="T53" id="Seg_640" s="T52">what-TRL</ta>
            <ta e="T54" id="Seg_641" s="T53">I.ACC</ta>
            <ta e="T55" id="Seg_642" s="T54">cheat-CO-2SG.S</ta>
            <ta e="T56" id="Seg_643" s="T55">and</ta>
            <ta e="T57" id="Seg_644" s="T56">(s)he.[NOM]</ta>
            <ta e="T58" id="Seg_645" s="T57">begin.to.laugh-DUR.[3SG.S]</ta>
            <ta e="T59" id="Seg_646" s="T58">say-3SG.S</ta>
            <ta e="T60" id="Seg_647" s="T59">you.SG.NOM</ta>
            <ta e="T61" id="Seg_648" s="T60">greedy-ADVZ</ta>
            <ta e="T62" id="Seg_649" s="T61">be-HAB-CO-2SG.S</ta>
            <ta e="T63" id="Seg_650" s="T62">I.NOM</ta>
            <ta e="T64" id="Seg_651" s="T63">that.is.why</ta>
            <ta e="T65" id="Seg_652" s="T64">you.SG.ACC</ta>
            <ta e="T66" id="Seg_653" s="T65">cheat-PST-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_654" s="T1">Петр-ADJZ</ta>
            <ta e="T3" id="Seg_655" s="T2">брат.[NOM]-1SG</ta>
            <ta e="T4" id="Seg_656" s="T3">я.ALL</ta>
            <ta e="T5" id="Seg_657" s="T4">соврать-3SG.S</ta>
            <ta e="T6" id="Seg_658" s="T5">сказать-3SG.S</ta>
            <ta e="T7" id="Seg_659" s="T6">кто.[NOM]</ta>
            <ta e="T8" id="Seg_660" s="T7">%Тайзаковские?.[NOM]</ta>
            <ta e="T9" id="Seg_661" s="T8">рыба.[NOM]</ta>
            <ta e="T10" id="Seg_662" s="T9">черпать-3PL</ta>
            <ta e="T11" id="Seg_663" s="T10">мы.GEN</ta>
            <ta e="T12" id="Seg_664" s="T11">озеро-EP-LOC.1PL</ta>
            <ta e="T13" id="Seg_665" s="T12">я.NOM</ta>
            <ta e="T14" id="Seg_666" s="T13">дом-ILL</ta>
            <ta e="T15" id="Seg_667" s="T14">бегать-MOM-CO-1SG.S</ta>
            <ta e="T16" id="Seg_668" s="T15">медленно</ta>
            <ta e="T17" id="Seg_669" s="T16">Ваня-ALL</ta>
            <ta e="T18" id="Seg_670" s="T17">сказать-1SG.S</ta>
            <ta e="T19" id="Seg_671" s="T18">отправиться-OPT-1DU</ta>
            <ta e="T20" id="Seg_672" s="T19">рыба.[NOM]</ta>
            <ta e="T21" id="Seg_673" s="T20">черпать-INF</ta>
            <ta e="T22" id="Seg_674" s="T21">мы.GEN</ta>
            <ta e="T23" id="Seg_675" s="T22">озеро-EP-LOC.1PL</ta>
            <ta e="T24" id="Seg_676" s="T23">рыба.[NOM]</ta>
            <ta e="T25" id="Seg_677" s="T24">черпать-3PL</ta>
            <ta e="T26" id="Seg_678" s="T25">лошадь-ACC</ta>
            <ta e="T27" id="Seg_679" s="T26">запрячь-1DU</ta>
            <ta e="T28" id="Seg_680" s="T27">и</ta>
            <ta e="T29" id="Seg_681" s="T28">отправиться-CO-1DU</ta>
            <ta e="T30" id="Seg_682" s="T29">озеро-EP-ILL</ta>
            <ta e="T31" id="Seg_683" s="T30">приехать-CVB</ta>
            <ta e="T32" id="Seg_684" s="T31">достичь-1DU</ta>
            <ta e="T33" id="Seg_685" s="T32">тёмный-ADVZ</ta>
            <ta e="T34" id="Seg_686" s="T33">быть-3SG.S</ta>
            <ta e="T35" id="Seg_687" s="T34">что.[NOM]</ta>
            <ta e="T36" id="Seg_688" s="T35">человек.[NOM]-EMPH</ta>
            <ta e="T37" id="Seg_689" s="T36">NEG.EX.[3SG.S]</ta>
            <ta e="T38" id="Seg_690" s="T37">прорубь-ACC</ta>
            <ta e="T39" id="Seg_691" s="T38">сделать-CO-1DU</ta>
            <ta e="T40" id="Seg_692" s="T39">и</ta>
            <ta e="T41" id="Seg_693" s="T40">что.[NOM]</ta>
            <ta e="T42" id="Seg_694" s="T41">рыба.[NOM]-EMPH</ta>
            <ta e="T43" id="Seg_695" s="T42">NEG.EX.[3SG.S]</ta>
            <ta e="T44" id="Seg_696" s="T43">он(а).[NOM]</ta>
            <ta e="T45" id="Seg_697" s="T44">я.ACC</ta>
            <ta e="T46" id="Seg_698" s="T45">обмануть-PST.NAR.[3SG.S]</ta>
            <ta e="T47" id="Seg_699" s="T46">и</ta>
            <ta e="T48" id="Seg_700" s="T47">деревня-ILL</ta>
            <ta e="T49" id="Seg_701" s="T48">приехать-CO-1DU</ta>
            <ta e="T50" id="Seg_702" s="T49">я.NOM</ta>
            <ta e="T51" id="Seg_703" s="T50">он(а)-ALL</ta>
            <ta e="T52" id="Seg_704" s="T51">сказать-1SG.S</ta>
            <ta e="T53" id="Seg_705" s="T52">что-TRL</ta>
            <ta e="T54" id="Seg_706" s="T53">я.ACC</ta>
            <ta e="T55" id="Seg_707" s="T54">обмануть-CO-2SG.S</ta>
            <ta e="T56" id="Seg_708" s="T55">а</ta>
            <ta e="T57" id="Seg_709" s="T56">он(а).[NOM]</ta>
            <ta e="T58" id="Seg_710" s="T57">засмеяться-DUR.[3SG.S]</ta>
            <ta e="T59" id="Seg_711" s="T58">сказать-3SG.S</ta>
            <ta e="T60" id="Seg_712" s="T59">ты.NOM</ta>
            <ta e="T61" id="Seg_713" s="T60">жадный-ADVZ</ta>
            <ta e="T62" id="Seg_714" s="T61">быть-HAB-CO-2SG.S</ta>
            <ta e="T63" id="Seg_715" s="T62">я.NOM</ta>
            <ta e="T64" id="Seg_716" s="T63">поэтому</ta>
            <ta e="T65" id="Seg_717" s="T64">ты.ACC</ta>
            <ta e="T66" id="Seg_718" s="T65">обмануть-PST-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_719" s="T1">nprop-n&gt;adj</ta>
            <ta e="T3" id="Seg_720" s="T2">n.[n:case]-n:poss</ta>
            <ta e="T4" id="Seg_721" s="T3">pers</ta>
            <ta e="T5" id="Seg_722" s="T4">v-v:pn</ta>
            <ta e="T6" id="Seg_723" s="T5">v-v:pn</ta>
            <ta e="T7" id="Seg_724" s="T6">interrog.[n:case]</ta>
            <ta e="T8" id="Seg_725" s="T7">n.[n:case]</ta>
            <ta e="T9" id="Seg_726" s="T8">n.[n:case]</ta>
            <ta e="T10" id="Seg_727" s="T9">v-v:pn</ta>
            <ta e="T11" id="Seg_728" s="T10">pers</ta>
            <ta e="T12" id="Seg_729" s="T11">n-n:ins-n:case.poss</ta>
            <ta e="T13" id="Seg_730" s="T12">pers</ta>
            <ta e="T14" id="Seg_731" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_732" s="T14">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T16" id="Seg_733" s="T15">adv</ta>
            <ta e="T17" id="Seg_734" s="T16">nprop-n:case</ta>
            <ta e="T18" id="Seg_735" s="T17">v-v:pn</ta>
            <ta e="T19" id="Seg_736" s="T18">v-v:mood-v:pn</ta>
            <ta e="T20" id="Seg_737" s="T19">n.[n:case]</ta>
            <ta e="T21" id="Seg_738" s="T20">v-v:inf</ta>
            <ta e="T22" id="Seg_739" s="T21">pers</ta>
            <ta e="T23" id="Seg_740" s="T22">n-n:ins-n:case.poss</ta>
            <ta e="T24" id="Seg_741" s="T23">n.[n:case]</ta>
            <ta e="T25" id="Seg_742" s="T24">v-v:pn</ta>
            <ta e="T26" id="Seg_743" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_744" s="T26">v-v:pn</ta>
            <ta e="T28" id="Seg_745" s="T27">conj</ta>
            <ta e="T29" id="Seg_746" s="T28">v-v:ins-v:pn</ta>
            <ta e="T30" id="Seg_747" s="T29">n-n:ins-n:case</ta>
            <ta e="T31" id="Seg_748" s="T30">v-v&gt;adv</ta>
            <ta e="T32" id="Seg_749" s="T31">v-v:pn</ta>
            <ta e="T33" id="Seg_750" s="T32">adj-adj&gt;adv</ta>
            <ta e="T34" id="Seg_751" s="T33">v-v:pn</ta>
            <ta e="T35" id="Seg_752" s="T34">interrog.[n:case]</ta>
            <ta e="T36" id="Seg_753" s="T35">n.[n:case]-clit</ta>
            <ta e="T37" id="Seg_754" s="T36">v.[v:pn]</ta>
            <ta e="T38" id="Seg_755" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_756" s="T38">v-v:ins-v:pn</ta>
            <ta e="T40" id="Seg_757" s="T39">conj</ta>
            <ta e="T41" id="Seg_758" s="T40">interrog.[n:case]</ta>
            <ta e="T42" id="Seg_759" s="T41">n.[n:case]-clit</ta>
            <ta e="T43" id="Seg_760" s="T42">v.[v:pn]</ta>
            <ta e="T44" id="Seg_761" s="T43">pers.[n:case]</ta>
            <ta e="T45" id="Seg_762" s="T44">pers</ta>
            <ta e="T46" id="Seg_763" s="T45">v-v:tense.[v:pn]</ta>
            <ta e="T47" id="Seg_764" s="T46">conj</ta>
            <ta e="T48" id="Seg_765" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_766" s="T48">v-v:ins-v:pn</ta>
            <ta e="T50" id="Seg_767" s="T49">pers</ta>
            <ta e="T51" id="Seg_768" s="T50">pers-n:case</ta>
            <ta e="T52" id="Seg_769" s="T51">v-v:pn</ta>
            <ta e="T53" id="Seg_770" s="T52">interrog-n:case</ta>
            <ta e="T54" id="Seg_771" s="T53">pers</ta>
            <ta e="T55" id="Seg_772" s="T54">v-v:ins-v:pn</ta>
            <ta e="T56" id="Seg_773" s="T55">conj</ta>
            <ta e="T57" id="Seg_774" s="T56">pers.[n:case]</ta>
            <ta e="T58" id="Seg_775" s="T57">v-v&gt;v.[v:pn]</ta>
            <ta e="T59" id="Seg_776" s="T58">v-v:pn</ta>
            <ta e="T60" id="Seg_777" s="T59">pers</ta>
            <ta e="T61" id="Seg_778" s="T60">adj-adj&gt;adv</ta>
            <ta e="T62" id="Seg_779" s="T61">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T63" id="Seg_780" s="T62">pers</ta>
            <ta e="T64" id="Seg_781" s="T63">conj</ta>
            <ta e="T65" id="Seg_782" s="T64">pers</ta>
            <ta e="T66" id="Seg_783" s="T65">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_784" s="T1">adj</ta>
            <ta e="T3" id="Seg_785" s="T2">n</ta>
            <ta e="T4" id="Seg_786" s="T3">pers</ta>
            <ta e="T5" id="Seg_787" s="T4">v</ta>
            <ta e="T6" id="Seg_788" s="T5">v</ta>
            <ta e="T7" id="Seg_789" s="T6">interrog</ta>
            <ta e="T8" id="Seg_790" s="T7">n</ta>
            <ta e="T9" id="Seg_791" s="T8">n</ta>
            <ta e="T10" id="Seg_792" s="T9">v</ta>
            <ta e="T11" id="Seg_793" s="T10">pers</ta>
            <ta e="T12" id="Seg_794" s="T11">n</ta>
            <ta e="T13" id="Seg_795" s="T12">pers</ta>
            <ta e="T14" id="Seg_796" s="T13">n</ta>
            <ta e="T15" id="Seg_797" s="T14">v</ta>
            <ta e="T16" id="Seg_798" s="T15">adv</ta>
            <ta e="T17" id="Seg_799" s="T16">nprop</ta>
            <ta e="T18" id="Seg_800" s="T17">v</ta>
            <ta e="T19" id="Seg_801" s="T18">v</ta>
            <ta e="T20" id="Seg_802" s="T19">n</ta>
            <ta e="T21" id="Seg_803" s="T20">v</ta>
            <ta e="T22" id="Seg_804" s="T21">pers</ta>
            <ta e="T23" id="Seg_805" s="T22">n</ta>
            <ta e="T24" id="Seg_806" s="T23">n</ta>
            <ta e="T25" id="Seg_807" s="T24">v</ta>
            <ta e="T26" id="Seg_808" s="T25">n</ta>
            <ta e="T27" id="Seg_809" s="T26">adj</ta>
            <ta e="T28" id="Seg_810" s="T27">conj</ta>
            <ta e="T29" id="Seg_811" s="T28">v</ta>
            <ta e="T30" id="Seg_812" s="T29">n</ta>
            <ta e="T31" id="Seg_813" s="T30">adv</ta>
            <ta e="T32" id="Seg_814" s="T31">v</ta>
            <ta e="T33" id="Seg_815" s="T32">adj</ta>
            <ta e="T34" id="Seg_816" s="T33">v</ta>
            <ta e="T35" id="Seg_817" s="T34">interrog</ta>
            <ta e="T36" id="Seg_818" s="T35">n</ta>
            <ta e="T37" id="Seg_819" s="T36">v</ta>
            <ta e="T38" id="Seg_820" s="T37">n</ta>
            <ta e="T39" id="Seg_821" s="T38">v</ta>
            <ta e="T40" id="Seg_822" s="T39">conj</ta>
            <ta e="T41" id="Seg_823" s="T40">interrog</ta>
            <ta e="T42" id="Seg_824" s="T41">n</ta>
            <ta e="T43" id="Seg_825" s="T42">v</ta>
            <ta e="T44" id="Seg_826" s="T43">pers</ta>
            <ta e="T45" id="Seg_827" s="T44">pers</ta>
            <ta e="T46" id="Seg_828" s="T45">v</ta>
            <ta e="T47" id="Seg_829" s="T46">conj</ta>
            <ta e="T48" id="Seg_830" s="T47">n</ta>
            <ta e="T49" id="Seg_831" s="T48">v</ta>
            <ta e="T50" id="Seg_832" s="T49">pers</ta>
            <ta e="T51" id="Seg_833" s="T50">pers</ta>
            <ta e="T52" id="Seg_834" s="T51">v</ta>
            <ta e="T53" id="Seg_835" s="T52">interrog</ta>
            <ta e="T54" id="Seg_836" s="T53">pers</ta>
            <ta e="T55" id="Seg_837" s="T54">v</ta>
            <ta e="T56" id="Seg_838" s="T55">conj</ta>
            <ta e="T57" id="Seg_839" s="T56">pers</ta>
            <ta e="T58" id="Seg_840" s="T57">v</ta>
            <ta e="T59" id="Seg_841" s="T58">v</ta>
            <ta e="T60" id="Seg_842" s="T59">pers</ta>
            <ta e="T61" id="Seg_843" s="T60">adj</ta>
            <ta e="T62" id="Seg_844" s="T61">v</ta>
            <ta e="T63" id="Seg_845" s="T62">pers</ta>
            <ta e="T64" id="Seg_846" s="T63">adv</ta>
            <ta e="T65" id="Seg_847" s="T64">pers</ta>
            <ta e="T66" id="Seg_848" s="T65">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_849" s="T2">np.h:A</ta>
            <ta e="T4" id="Seg_850" s="T3">pro.h:R</ta>
            <ta e="T6" id="Seg_851" s="T5">0.3.h:A</ta>
            <ta e="T7" id="Seg_852" s="T6">pro.h:A</ta>
            <ta e="T9" id="Seg_853" s="T8">np:P</ta>
            <ta e="T11" id="Seg_854" s="T10">pro.h:Poss</ta>
            <ta e="T12" id="Seg_855" s="T11">np:L</ta>
            <ta e="T13" id="Seg_856" s="T12">pro.h:A</ta>
            <ta e="T14" id="Seg_857" s="T13">np:G</ta>
            <ta e="T17" id="Seg_858" s="T16">np.h:R</ta>
            <ta e="T18" id="Seg_859" s="T17">0.1.h:A</ta>
            <ta e="T19" id="Seg_860" s="T18">0.1.h:A</ta>
            <ta e="T20" id="Seg_861" s="T19">np:P</ta>
            <ta e="T22" id="Seg_862" s="T21">pro.h:Poss</ta>
            <ta e="T23" id="Seg_863" s="T22">np:L</ta>
            <ta e="T24" id="Seg_864" s="T23">np:P</ta>
            <ta e="T25" id="Seg_865" s="T24">0.3.h:A</ta>
            <ta e="T26" id="Seg_866" s="T25">np:Th</ta>
            <ta e="T27" id="Seg_867" s="T26">0.1.h:A</ta>
            <ta e="T29" id="Seg_868" s="T28">0.1.h:A</ta>
            <ta e="T30" id="Seg_869" s="T29">np:G</ta>
            <ta e="T32" id="Seg_870" s="T31">0.1.h:A</ta>
            <ta e="T34" id="Seg_871" s="T33">0.3:Th</ta>
            <ta e="T36" id="Seg_872" s="T35">np.h:Th</ta>
            <ta e="T38" id="Seg_873" s="T37">np:P</ta>
            <ta e="T39" id="Seg_874" s="T38">0.1.h:A</ta>
            <ta e="T42" id="Seg_875" s="T41">np:Th</ta>
            <ta e="T44" id="Seg_876" s="T43">pro.h:A</ta>
            <ta e="T45" id="Seg_877" s="T44">pro.h:Th</ta>
            <ta e="T48" id="Seg_878" s="T47">np:G</ta>
            <ta e="T49" id="Seg_879" s="T48">0.1.h:A</ta>
            <ta e="T50" id="Seg_880" s="T49">pro.h:A</ta>
            <ta e="T51" id="Seg_881" s="T50">pro.h:R</ta>
            <ta e="T54" id="Seg_882" s="T53">pro.h:Th</ta>
            <ta e="T55" id="Seg_883" s="T54">0.2.h:A</ta>
            <ta e="T57" id="Seg_884" s="T56">pro.h:A</ta>
            <ta e="T59" id="Seg_885" s="T58">0.3.h:A</ta>
            <ta e="T60" id="Seg_886" s="T59">pro.h:Th</ta>
            <ta e="T63" id="Seg_887" s="T62">pro.h:A</ta>
            <ta e="T65" id="Seg_888" s="T64">pro.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_889" s="T2">np.h:S</ta>
            <ta e="T5" id="Seg_890" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_891" s="T5">0.3.h:S v:pred</ta>
            <ta e="T7" id="Seg_892" s="T6">pro.h:S</ta>
            <ta e="T9" id="Seg_893" s="T8">np:O</ta>
            <ta e="T10" id="Seg_894" s="T9">v:pred</ta>
            <ta e="T13" id="Seg_895" s="T12">pro.h:S</ta>
            <ta e="T15" id="Seg_896" s="T14">v:pred</ta>
            <ta e="T18" id="Seg_897" s="T17">0.1.h:S v:pred</ta>
            <ta e="T19" id="Seg_898" s="T18">0.1.h:S v:pred</ta>
            <ta e="T21" id="Seg_899" s="T19">s:purp</ta>
            <ta e="T24" id="Seg_900" s="T23">np:O</ta>
            <ta e="T25" id="Seg_901" s="T24">0.3.h:S v:pred</ta>
            <ta e="T26" id="Seg_902" s="T25">np:O</ta>
            <ta e="T27" id="Seg_903" s="T26">0.1.h:S v:pred</ta>
            <ta e="T29" id="Seg_904" s="T28">0.1.h:S v:pred</ta>
            <ta e="T31" id="Seg_905" s="T30">s:temp</ta>
            <ta e="T32" id="Seg_906" s="T31">0.1.h:S v:pred</ta>
            <ta e="T33" id="Seg_907" s="T32">adj:pred</ta>
            <ta e="T34" id="Seg_908" s="T33">0.3:S cop</ta>
            <ta e="T36" id="Seg_909" s="T35">np.h:S</ta>
            <ta e="T37" id="Seg_910" s="T36">v:pred</ta>
            <ta e="T38" id="Seg_911" s="T37">np:O</ta>
            <ta e="T39" id="Seg_912" s="T38">0.1.h:S v:pred</ta>
            <ta e="T42" id="Seg_913" s="T41">np:S</ta>
            <ta e="T43" id="Seg_914" s="T42">v:pred</ta>
            <ta e="T44" id="Seg_915" s="T43">pro.h:S</ta>
            <ta e="T45" id="Seg_916" s="T44">pro.h:O</ta>
            <ta e="T46" id="Seg_917" s="T45">v:pred</ta>
            <ta e="T49" id="Seg_918" s="T48">0.1.h:S v:pred</ta>
            <ta e="T50" id="Seg_919" s="T49">pro.h:S</ta>
            <ta e="T52" id="Seg_920" s="T51">v:pred</ta>
            <ta e="T54" id="Seg_921" s="T53">pro.h:O</ta>
            <ta e="T55" id="Seg_922" s="T54">0.2.h:S v:pred</ta>
            <ta e="T57" id="Seg_923" s="T56">pro.h:S</ta>
            <ta e="T58" id="Seg_924" s="T57">v:pred</ta>
            <ta e="T59" id="Seg_925" s="T58">0.3.h:S v:pred</ta>
            <ta e="T60" id="Seg_926" s="T59">pro.h:S</ta>
            <ta e="T61" id="Seg_927" s="T60">adj:pred</ta>
            <ta e="T62" id="Seg_928" s="T61">cop</ta>
            <ta e="T63" id="Seg_929" s="T62">pro.h:S</ta>
            <ta e="T65" id="Seg_930" s="T64">pro.h:O</ta>
            <ta e="T66" id="Seg_931" s="T65">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_932" s="T1">RUS:cult</ta>
            <ta e="T28" id="Seg_933" s="T27">RUS:gram</ta>
            <ta e="T40" id="Seg_934" s="T39">RUS:gram</ta>
            <ta e="T47" id="Seg_935" s="T46">RUS:gram</ta>
            <ta e="T56" id="Seg_936" s="T55">RUS:gram</ta>
            <ta e="T61" id="Seg_937" s="T60">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_938" s="T1">Pjotr's brother deceived me.</ta>
            <ta e="T12" id="Seg_939" s="T5">He said: “People from Tajzakovo(?) are fishing in our lake.”</ta>
            <ta e="T16" id="Seg_940" s="T12">I ran home.</ta>
            <ta e="T21" id="Seg_941" s="T16">I said to Vanya: “Let's go fishing.</ta>
            <ta e="T25" id="Seg_942" s="T21">[People] are fishing in our lake.”</ta>
            <ta e="T29" id="Seg_943" s="T25">We harnessed a horse and set off.</ta>
            <ta e="T32" id="Seg_944" s="T29">We came to the lake.</ta>
            <ta e="T34" id="Seg_945" s="T32">It was dark.</ta>
            <ta e="T37" id="Seg_946" s="T34">There was noone [there].</ta>
            <ta e="T39" id="Seg_947" s="T37">We made an ice hole.</ta>
            <ta e="T43" id="Seg_948" s="T39">And there was no fish.</ta>
            <ta e="T46" id="Seg_949" s="T43">He deceived me.</ta>
            <ta e="T49" id="Seg_950" s="T46">So we came home.</ta>
            <ta e="T55" id="Seg_951" s="T49">I said to him: “Why did you deceive me?”</ta>
            <ta e="T58" id="Seg_952" s="T55">And he started to laugh.</ta>
            <ta e="T62" id="Seg_953" s="T58">He said: “You were greedy.</ta>
            <ta e="T66" id="Seg_954" s="T62">That's why I deceived you.”</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_955" s="T1">Pjotrs Bruder hat mich reingelegt.</ta>
            <ta e="T12" id="Seg_956" s="T5">Er sagte: "Leute aus Tajzakovo(?) angeln in unserem See."</ta>
            <ta e="T16" id="Seg_957" s="T12">Ich rannte nach Hause.</ta>
            <ta e="T21" id="Seg_958" s="T16">Ich sagte zu Vanya: "Lass uns angeln gehen.</ta>
            <ta e="T25" id="Seg_959" s="T21">[Leute] angeln in unserem See.</ta>
            <ta e="T29" id="Seg_960" s="T25">Wir spannten ein Pferd an und brachen auf.</ta>
            <ta e="T32" id="Seg_961" s="T29">Wir kamen zum See.</ta>
            <ta e="T34" id="Seg_962" s="T32">Es war dunkel.</ta>
            <ta e="T37" id="Seg_963" s="T34">Es war niemand da.</ta>
            <ta e="T39" id="Seg_964" s="T37">Wir machten ein Eisloch.</ta>
            <ta e="T43" id="Seg_965" s="T39">Und es gab keine Fische.</ta>
            <ta e="T46" id="Seg_966" s="T43">Er hat mich reingelegt.</ta>
            <ta e="T49" id="Seg_967" s="T46">So kamen wir nach Hause.</ta>
            <ta e="T55" id="Seg_968" s="T49">Ich sagte zu ihm: "Warum hast du mich reingelegt?"</ta>
            <ta e="T58" id="Seg_969" s="T55">Und er fing an zu lachen.</ta>
            <ta e="T62" id="Seg_970" s="T58">Er sagte: "Du warst gierig.</ta>
            <ta e="T66" id="Seg_971" s="T62">Darum habe ich dich reingelegt."</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_972" s="T1">Брат Петра меня обманул.</ta>
            <ta e="T12" id="Seg_973" s="T5">Говорит: “Тайзаковские рыбу черпают в нашем озере”.</ta>
            <ta e="T16" id="Seg_974" s="T12">Я домой побежала помаленьку.</ta>
            <ta e="T21" id="Seg_975" s="T16">Ване сказала: “Поедем рыбу черпать.</ta>
            <ta e="T25" id="Seg_976" s="T21">В нашем озере рыбу черпают”.</ta>
            <ta e="T29" id="Seg_977" s="T25">Мы лошадь запрягли и поехали.</ta>
            <ta e="T32" id="Seg_978" s="T29">К озеру приехали.</ta>
            <ta e="T34" id="Seg_979" s="T32">Темно было.</ta>
            <ta e="T37" id="Seg_980" s="T34">Никого не было.</ta>
            <ta e="T39" id="Seg_981" s="T37">Мы прорубь сделали.</ta>
            <ta e="T43" id="Seg_982" s="T39">И никакой рыбы не было.</ta>
            <ta e="T46" id="Seg_983" s="T43">Он меня обманул.</ta>
            <ta e="T49" id="Seg_984" s="T46">И домой приехали</ta>
            <ta e="T55" id="Seg_985" s="T49">Я ему сказала: “Почему ты меня обманул?”</ta>
            <ta e="T58" id="Seg_986" s="T55">А он смеется.</ta>
            <ta e="T62" id="Seg_987" s="T58">Говорит: “Ты жадная была.</ta>
            <ta e="T66" id="Seg_988" s="T62">Я поэтому тебя и обманул”.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_989" s="T1">брат Петра меня обманул</ta>
            <ta e="T12" id="Seg_990" s="T5">говорит тайзаковские рыбу черпают в нашем озере</ta>
            <ta e="T16" id="Seg_991" s="T12">я домой побежала помаленьку</ta>
            <ta e="T21" id="Seg_992" s="T16">Ване сказала поедем рыбу черпать</ta>
            <ta e="T25" id="Seg_993" s="T21">в нашем озере рыбу черпают</ta>
            <ta e="T29" id="Seg_994" s="T25">лошадь запрягли и поехали</ta>
            <ta e="T32" id="Seg_995" s="T29">к озеру приехали</ta>
            <ta e="T34" id="Seg_996" s="T32">темно было</ta>
            <ta e="T37" id="Seg_997" s="T34">там никого не было</ta>
            <ta e="T39" id="Seg_998" s="T37">прорубь сделали</ta>
            <ta e="T43" id="Seg_999" s="T39">и никакой рыбы не было</ta>
            <ta e="T46" id="Seg_1000" s="T43">он меня обманул</ta>
            <ta e="T49" id="Seg_1001" s="T46">и домой приехали</ta>
            <ta e="T55" id="Seg_1002" s="T49">я ему сказал(а) почему меня обманул</ta>
            <ta e="T58" id="Seg_1003" s="T55">а он смеется</ta>
            <ta e="T62" id="Seg_1004" s="T58">говорит ты жадная была</ta>
            <ta e="T66" id="Seg_1005" s="T62">я поэтому тебя и обманул</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T37" id="Seg_1006" s="T34">[BrM:] 'qum näj' changed to 'qumnäj'.</ta>
            <ta e="T66" id="Seg_1007" s="T62">[BrM:] 'nanostə' changed to 'nano stə'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
