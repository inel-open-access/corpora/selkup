<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Pigeons_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Pigeons_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">117</ud-information>
            <ud-information attribute-name="# HIAT:w">83</ud-information>
            <ud-information attribute-name="# e">83</ud-information>
            <ud-information attribute-name="# HIAT:u">16</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T84" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Ugon</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">man</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">golublam</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">ass</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">qoǯurguzau</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_20" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">ČʼornajRečkaɣɨn</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">okkɨr</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">näjɣunnan</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">koːcʼin</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">jezattə</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_38" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">Kolubla</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">wes</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">tʼeɣa</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_50" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">Man</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">akoškaon</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">mannɨpaːn</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_62" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">Kurʼicala</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">koːcʼin</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">jewattə</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_74" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">Tebla</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">kak</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">wazezitdattə</ts>
                  <nts id="Seg_83" n="HIAT:ip">,</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">madə</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">barot</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">omdəlattə</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_96" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">Man</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">kɨbanädäkkanä</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">soɣudʼzʼan</ts>
                  <nts id="Seg_105" n="HIAT:ip">:</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_107" n="HIAT:ip">“</nts>
                  <ts e="T30" id="Seg_109" n="HIAT:w" s="T29">A</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_112" n="HIAT:w" s="T30">kuricala</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_115" n="HIAT:w" s="T31">qajno</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_118" n="HIAT:w" s="T32">madə</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_121" n="HIAT:w" s="T33">barot</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_124" n="HIAT:w" s="T34">omdälattə</ts>
                  <nts id="Seg_125" n="HIAT:ip">?</nts>
                  <nts id="Seg_126" n="HIAT:ip">”</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_129" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_131" n="HIAT:w" s="T35">A</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_134" n="HIAT:w" s="T36">qɨbanädän</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_137" n="HIAT:w" s="T37">tʼärɨn</ts>
                  <nts id="Seg_138" n="HIAT:ip">:</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_140" n="HIAT:ip">“</nts>
                  <ts e="T39" id="Seg_142" n="HIAT:w" s="T38">Na</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_145" n="HIAT:w" s="T39">ass</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_148" n="HIAT:w" s="T40">kuricala</ts>
                  <nts id="Seg_149" n="HIAT:ip">,</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_152" n="HIAT:w" s="T41">na</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_155" n="HIAT:w" s="T42">kolubla</ts>
                  <nts id="Seg_156" n="HIAT:ip">.</nts>
                  <nts id="Seg_157" n="HIAT:ip">”</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_160" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_162" n="HIAT:w" s="T43">Man</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_165" n="HIAT:w" s="T44">Valodʼanä</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_168" n="HIAT:w" s="T45">kʼennau</ts>
                  <nts id="Seg_169" n="HIAT:ip">:</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_171" n="HIAT:ip">“</nts>
                  <ts e="T47" id="Seg_173" n="HIAT:w" s="T46">Tänärbɨzan</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_176" n="HIAT:w" s="T47">kuricala</ts>
                  <nts id="Seg_177" n="HIAT:ip">,</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_180" n="HIAT:w" s="T48">kak</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_183" n="HIAT:w" s="T49">täbla</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_186" n="HIAT:w" s="T50">wezezitdattə</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_189" n="HIAT:w" s="T51">maːdə</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_192" n="HIAT:w" s="T52">parotdə</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_195" n="HIAT:w" s="T53">omdattə</ts>
                  <nts id="Seg_196" n="HIAT:ip">.</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_199" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_201" n="HIAT:w" s="T54">Man</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_204" n="HIAT:w" s="T55">kɨbanädäkkanä</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_207" n="HIAT:w" s="T56">soɣudʼzʼän</ts>
                  <nts id="Seg_208" n="HIAT:ip">:</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_210" n="HIAT:ip">“</nts>
                  <ts e="T58" id="Seg_212" n="HIAT:w" s="T57">Kuricala</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_215" n="HIAT:w" s="T58">qajno</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_218" n="HIAT:w" s="T59">wazʼezʼättə</ts>
                  <nts id="Seg_219" n="HIAT:ip">?</nts>
                  <nts id="Seg_220" n="HIAT:ip">”</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_223" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_225" n="HIAT:w" s="T60">Qɨbanädän</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_228" n="HIAT:w" s="T61">tʼarɨn</ts>
                  <nts id="Seg_229" n="HIAT:ip">:</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_231" n="HIAT:ip">“</nts>
                  <ts e="T63" id="Seg_233" n="HIAT:w" s="T62">Na</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_236" n="HIAT:w" s="T63">ass</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_239" n="HIAT:w" s="T64">kuricala</ts>
                  <nts id="Seg_240" n="HIAT:ip">,</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_243" n="HIAT:w" s="T65">na</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_246" n="HIAT:w" s="T66">kolubla</ts>
                  <nts id="Seg_247" n="HIAT:ip">.</nts>
                  <nts id="Seg_248" n="HIAT:ip">”</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_251" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_253" n="HIAT:w" s="T67">A</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_256" n="HIAT:w" s="T68">Valodʼa</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_259" n="HIAT:w" s="T69">datao</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_262" n="HIAT:w" s="T70">lagwatpɨkun</ts>
                  <nts id="Seg_263" n="HIAT:ip">.</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_266" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_268" n="HIAT:w" s="T71">No</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_271" n="HIAT:w" s="T72">man</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_274" n="HIAT:w" s="T73">qaj</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_277" n="HIAT:w" s="T74">tunuzau</ts>
                  <nts id="Seg_278" n="HIAT:ip">?</nts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_281" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_283" n="HIAT:w" s="T75">Man</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_286" n="HIAT:w" s="T76">täblam</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_289" n="HIAT:w" s="T77">ass</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_292" n="HIAT:w" s="T78">qoǯɨrguzau</ts>
                  <nts id="Seg_293" n="HIAT:ip">.</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_296" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_298" n="HIAT:w" s="T79">Qajzʼe</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_301" n="HIAT:w" s="T80">bisqwat</ts>
                  <nts id="Seg_302" n="HIAT:ip">?</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_305" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_307" n="HIAT:w" s="T81">Biskugu</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_310" n="HIAT:w" s="T82">ne</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_313" n="HIAT:w" s="T83">nadə</ts>
                  <nts id="Seg_314" n="HIAT:ip">.</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T84" id="Seg_316" n="sc" s="T1">
               <ts e="T2" id="Seg_318" n="e" s="T1">Ugon </ts>
               <ts e="T3" id="Seg_320" n="e" s="T2">man </ts>
               <ts e="T4" id="Seg_322" n="e" s="T3">golublam </ts>
               <ts e="T5" id="Seg_324" n="e" s="T4">ass </ts>
               <ts e="T6" id="Seg_326" n="e" s="T5">qoǯurguzau. </ts>
               <ts e="T7" id="Seg_328" n="e" s="T6">ČʼornajRečkaɣɨn </ts>
               <ts e="T8" id="Seg_330" n="e" s="T7">okkɨr </ts>
               <ts e="T9" id="Seg_332" n="e" s="T8">näjɣunnan </ts>
               <ts e="T10" id="Seg_334" n="e" s="T9">koːcʼin </ts>
               <ts e="T11" id="Seg_336" n="e" s="T10">jezattə. </ts>
               <ts e="T12" id="Seg_338" n="e" s="T11">Kolubla </ts>
               <ts e="T13" id="Seg_340" n="e" s="T12">wes </ts>
               <ts e="T14" id="Seg_342" n="e" s="T13">tʼeɣa. </ts>
               <ts e="T15" id="Seg_344" n="e" s="T14">Man </ts>
               <ts e="T16" id="Seg_346" n="e" s="T15">akoškaon </ts>
               <ts e="T17" id="Seg_348" n="e" s="T16">mannɨpaːn. </ts>
               <ts e="T18" id="Seg_350" n="e" s="T17">Kurʼicala </ts>
               <ts e="T19" id="Seg_352" n="e" s="T18">koːcʼin </ts>
               <ts e="T20" id="Seg_354" n="e" s="T19">jewattə. </ts>
               <ts e="T21" id="Seg_356" n="e" s="T20">Tebla </ts>
               <ts e="T22" id="Seg_358" n="e" s="T21">kak </ts>
               <ts e="T23" id="Seg_360" n="e" s="T22">wazezitdattə, </ts>
               <ts e="T24" id="Seg_362" n="e" s="T23">madə </ts>
               <ts e="T25" id="Seg_364" n="e" s="T24">barot </ts>
               <ts e="T26" id="Seg_366" n="e" s="T25">omdəlattə. </ts>
               <ts e="T27" id="Seg_368" n="e" s="T26">Man </ts>
               <ts e="T28" id="Seg_370" n="e" s="T27">kɨbanädäkkanä </ts>
               <ts e="T29" id="Seg_372" n="e" s="T28">soɣudʼzʼan: </ts>
               <ts e="T30" id="Seg_374" n="e" s="T29">“A </ts>
               <ts e="T31" id="Seg_376" n="e" s="T30">kuricala </ts>
               <ts e="T32" id="Seg_378" n="e" s="T31">qajno </ts>
               <ts e="T33" id="Seg_380" n="e" s="T32">madə </ts>
               <ts e="T34" id="Seg_382" n="e" s="T33">barot </ts>
               <ts e="T35" id="Seg_384" n="e" s="T34">omdälattə?” </ts>
               <ts e="T36" id="Seg_386" n="e" s="T35">A </ts>
               <ts e="T37" id="Seg_388" n="e" s="T36">qɨbanädän </ts>
               <ts e="T38" id="Seg_390" n="e" s="T37">tʼärɨn: </ts>
               <ts e="T39" id="Seg_392" n="e" s="T38">“Na </ts>
               <ts e="T40" id="Seg_394" n="e" s="T39">ass </ts>
               <ts e="T41" id="Seg_396" n="e" s="T40">kuricala, </ts>
               <ts e="T42" id="Seg_398" n="e" s="T41">na </ts>
               <ts e="T43" id="Seg_400" n="e" s="T42">kolubla.” </ts>
               <ts e="T44" id="Seg_402" n="e" s="T43">Man </ts>
               <ts e="T45" id="Seg_404" n="e" s="T44">Valodʼanä </ts>
               <ts e="T46" id="Seg_406" n="e" s="T45">kʼennau: </ts>
               <ts e="T47" id="Seg_408" n="e" s="T46">“Tänärbɨzan </ts>
               <ts e="T48" id="Seg_410" n="e" s="T47">kuricala, </ts>
               <ts e="T49" id="Seg_412" n="e" s="T48">kak </ts>
               <ts e="T50" id="Seg_414" n="e" s="T49">täbla </ts>
               <ts e="T51" id="Seg_416" n="e" s="T50">wezezitdattə </ts>
               <ts e="T52" id="Seg_418" n="e" s="T51">maːdə </ts>
               <ts e="T53" id="Seg_420" n="e" s="T52">parotdə </ts>
               <ts e="T54" id="Seg_422" n="e" s="T53">omdattə. </ts>
               <ts e="T55" id="Seg_424" n="e" s="T54">Man </ts>
               <ts e="T56" id="Seg_426" n="e" s="T55">kɨbanädäkkanä </ts>
               <ts e="T57" id="Seg_428" n="e" s="T56">soɣudʼzʼän: </ts>
               <ts e="T58" id="Seg_430" n="e" s="T57">“Kuricala </ts>
               <ts e="T59" id="Seg_432" n="e" s="T58">qajno </ts>
               <ts e="T60" id="Seg_434" n="e" s="T59">wazʼezʼättə?” </ts>
               <ts e="T61" id="Seg_436" n="e" s="T60">Qɨbanädän </ts>
               <ts e="T62" id="Seg_438" n="e" s="T61">tʼarɨn: </ts>
               <ts e="T63" id="Seg_440" n="e" s="T62">“Na </ts>
               <ts e="T64" id="Seg_442" n="e" s="T63">ass </ts>
               <ts e="T65" id="Seg_444" n="e" s="T64">kuricala, </ts>
               <ts e="T66" id="Seg_446" n="e" s="T65">na </ts>
               <ts e="T67" id="Seg_448" n="e" s="T66">kolubla.” </ts>
               <ts e="T68" id="Seg_450" n="e" s="T67">A </ts>
               <ts e="T69" id="Seg_452" n="e" s="T68">Valodʼa </ts>
               <ts e="T70" id="Seg_454" n="e" s="T69">datao </ts>
               <ts e="T71" id="Seg_456" n="e" s="T70">lagwatpɨkun. </ts>
               <ts e="T72" id="Seg_458" n="e" s="T71">No </ts>
               <ts e="T73" id="Seg_460" n="e" s="T72">man </ts>
               <ts e="T74" id="Seg_462" n="e" s="T73">qaj </ts>
               <ts e="T75" id="Seg_464" n="e" s="T74">tunuzau? </ts>
               <ts e="T76" id="Seg_466" n="e" s="T75">Man </ts>
               <ts e="T77" id="Seg_468" n="e" s="T76">täblam </ts>
               <ts e="T78" id="Seg_470" n="e" s="T77">ass </ts>
               <ts e="T79" id="Seg_472" n="e" s="T78">qoǯɨrguzau. </ts>
               <ts e="T80" id="Seg_474" n="e" s="T79">Qajzʼe </ts>
               <ts e="T81" id="Seg_476" n="e" s="T80">bisqwat? </ts>
               <ts e="T82" id="Seg_478" n="e" s="T81">Biskugu </ts>
               <ts e="T83" id="Seg_480" n="e" s="T82">ne </ts>
               <ts e="T84" id="Seg_482" n="e" s="T83">nadə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_483" s="T1">PVD_1964_Pigeons_nar.001 (001.001)</ta>
            <ta e="T11" id="Seg_484" s="T6">PVD_1964_Pigeons_nar.002 (001.002)</ta>
            <ta e="T14" id="Seg_485" s="T11">PVD_1964_Pigeons_nar.003 (001.003)</ta>
            <ta e="T17" id="Seg_486" s="T14">PVD_1964_Pigeons_nar.004 (001.004)</ta>
            <ta e="T20" id="Seg_487" s="T17">PVD_1964_Pigeons_nar.005 (001.005)</ta>
            <ta e="T26" id="Seg_488" s="T20">PVD_1964_Pigeons_nar.006 (001.006)</ta>
            <ta e="T35" id="Seg_489" s="T26">PVD_1964_Pigeons_nar.007 (001.007)</ta>
            <ta e="T43" id="Seg_490" s="T35">PVD_1964_Pigeons_nar.008 (001.008)</ta>
            <ta e="T54" id="Seg_491" s="T43">PVD_1964_Pigeons_nar.009 (001.009)</ta>
            <ta e="T60" id="Seg_492" s="T54">PVD_1964_Pigeons_nar.010 (001.010)</ta>
            <ta e="T67" id="Seg_493" s="T60">PVD_1964_Pigeons_nar.011 (001.011)</ta>
            <ta e="T71" id="Seg_494" s="T67">PVD_1964_Pigeons_nar.012 (001.012)</ta>
            <ta e="T75" id="Seg_495" s="T71">PVD_1964_Pigeons_nar.013 (001.013)</ta>
            <ta e="T79" id="Seg_496" s="T75">PVD_1964_Pigeons_nar.014 (001.014)</ta>
            <ta e="T81" id="Seg_497" s="T79">PVD_1964_Pigeons_nar.015 (001.015)</ta>
            <ta e="T84" id="Seg_498" s="T81">PVD_1964_Pigeons_nar.016 (001.016)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_499" s="T1">у′гон ман г̂олублам асс ′kоджургу′зау.</ta>
            <ta e="T11" id="Seg_500" s="T6">Чорнаj речкаɣын ок′кыр ′нӓйɣуннан ′ко̄цʼин ′jезаттъ.</ta>
            <ta e="T14" id="Seg_501" s="T11">колубла вес ′тʼеɣа.</ta>
            <ta e="T17" id="Seg_502" s="T14">ман а′кошкаон ‵манны′па̄н.</ta>
            <ta e="T20" id="Seg_503" s="T17">курʼицала ′ко̄цʼин ′jеwаттъ.</ta>
            <ta e="T26" id="Seg_504" s="T20">теб′ла как ва′зезитдаттъ, ′мадъ ба′рот ′омдъ(е,ӓ)‵латтъ.</ta>
            <ta e="T35" id="Seg_505" s="T26">ман кыбанӓ′дӓкканӓ ′соɣудʼзʼан: а ′курицала kай′но ′мадъ б̂арот ′омдӓ‵латтъ?</ta>
            <ta e="T43" id="Seg_506" s="T35">а ′kыбанӓ‵дӓн тʼӓ′рын: на асс ′курицала, на ′колубла.</ta>
            <ta e="T54" id="Seg_507" s="T43">ман Валодʼа′нӓ кʼен′нау: ′тӓнӓрбызан ′курицала, как тӓб′ла ве′зезит‵даттъ ′ма̄дъ п(б̂)а′рот дъ ′омдаттъ.</ta>
            <ta e="T60" id="Seg_508" s="T54">ман кыбанӓ′дӓ(кка)нӓ соɣу′дʼзʼӓн: ′курицала kай′но вазʼе′зʼ(дʼ)ӓттъ.</ta>
            <ta e="T67" id="Seg_509" s="T60">kы‵банӓ′дӓн тʼа′рын: на асс ′курицала, на ′к(г̂)олубла.</ta>
            <ta e="T71" id="Seg_510" s="T67">а Ва′лодʼа да та′о лаг′ватпыкун.</ta>
            <ta e="T75" id="Seg_511" s="T71">но ман kай ′тунузау?</ta>
            <ta e="T79" id="Seg_512" s="T75">ман тӓб′лам асс ′kоджыргу‵зау.</ta>
            <ta e="T81" id="Seg_513" s="T79">kай′зʼе бис′kwат?</ta>
            <ta e="T84" id="Seg_514" s="T81">′бискугу не надъ.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_515" s="T1">ugon man ĝolublam ass qoǯurguzau.</ta>
            <ta e="T11" id="Seg_516" s="T6">Чornaj rečkaɣɨn okkɨr näjɣunnan koːcʼin jezattə.</ta>
            <ta e="T14" id="Seg_517" s="T11">kolubla wes tʼeɣa.</ta>
            <ta e="T17" id="Seg_518" s="T14">man akoškaon mannɨpaːn.</ta>
            <ta e="T20" id="Seg_519" s="T17">kurʼicala koːcʼin jewattə.</ta>
            <ta e="T26" id="Seg_520" s="T20">tebla kak wazezitdattə, madə barot omdə(e,ä)lattə.</ta>
            <ta e="T35" id="Seg_521" s="T26">man kɨbanädäkkanä soɣudʼzʼan: a kuricala qajno madə b̂arot omdälattə?</ta>
            <ta e="T43" id="Seg_522" s="T35">a qɨbanädän tʼärɨn: na ass kuricala, na kolubla.</ta>
            <ta e="T54" id="Seg_523" s="T43">man Вalodʼanä kʼennau: tänärbɨzan kuricala, kak täbla wezezitdattə maːdə p(b̂)arot də omdattə.</ta>
            <ta e="T60" id="Seg_524" s="T54">man kɨbanädä(kka)nä soɣudʼzʼän: kuricala qajno wazʼezʼ(dʼ)ättə.</ta>
            <ta e="T67" id="Seg_525" s="T60">qɨbanädän tʼarɨn: na ass kuricala, na k(ĝ)olubla.</ta>
            <ta e="T71" id="Seg_526" s="T67">a Вalodʼa da tao lagwatpɨkun.</ta>
            <ta e="T75" id="Seg_527" s="T71">no man qaj tunuzau?</ta>
            <ta e="T79" id="Seg_528" s="T75">man täblam ass qoǯɨrguzau.</ta>
            <ta e="T81" id="Seg_529" s="T79">qajzʼe bisqwat?</ta>
            <ta e="T84" id="Seg_530" s="T81">biskugu ne nadə.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_531" s="T1">Ugon man golublam ass qoǯurguzau. </ta>
            <ta e="T11" id="Seg_532" s="T6">ČʼornajRečkaɣɨn okkɨr näjɣunnan koːcʼin jezattə. </ta>
            <ta e="T14" id="Seg_533" s="T11">Kolubla wes tʼeɣa. </ta>
            <ta e="T17" id="Seg_534" s="T14">Man akoškaon mannɨpaːn. </ta>
            <ta e="T20" id="Seg_535" s="T17">Kurʼicala koːcʼin jewattə. </ta>
            <ta e="T26" id="Seg_536" s="T20">Tebla kak wazezitdattə, madə barot omdəlattə. </ta>
            <ta e="T35" id="Seg_537" s="T26">Man kɨbanädäkkanä soɣudʼzʼan: “A kuricala qajno madə barot omdälattə?” </ta>
            <ta e="T43" id="Seg_538" s="T35">A qɨbanädän tʼärɨn: “Na ass kuricala, na kolubla.” </ta>
            <ta e="T54" id="Seg_539" s="T43">Man Valodʼanä kʼennau: “Tänärbɨzan kuricala, kak täbla wezezitdattə maːdə parotdə omdattə. </ta>
            <ta e="T60" id="Seg_540" s="T54">Man kɨbanädäkkanä soɣudʼzʼän: “Kuricala qajno wazʼezʼättə?” </ta>
            <ta e="T67" id="Seg_541" s="T60">Qɨbanädän tʼarɨn: “Na ass kuricala, na kolubla.” </ta>
            <ta e="T71" id="Seg_542" s="T67">A Valodʼa datao lagwatpɨkun. </ta>
            <ta e="T75" id="Seg_543" s="T71">No man qaj tunuzau? </ta>
            <ta e="T79" id="Seg_544" s="T75">Man täblam ass qoǯɨrguzau. </ta>
            <ta e="T81" id="Seg_545" s="T79">Qajzʼe bisqwat? </ta>
            <ta e="T84" id="Seg_546" s="T81">Biskugu ne nadə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_547" s="T1">ugon</ta>
            <ta e="T3" id="Seg_548" s="T2">man</ta>
            <ta e="T4" id="Seg_549" s="T3">golub-la-m</ta>
            <ta e="T5" id="Seg_550" s="T4">ass</ta>
            <ta e="T6" id="Seg_551" s="T5">qo-ǯur-gu-za-u</ta>
            <ta e="T7" id="Seg_552" s="T6">ČʼornajRečka-ɣɨn</ta>
            <ta e="T8" id="Seg_553" s="T7">okkɨr</ta>
            <ta e="T9" id="Seg_554" s="T8">nä-j-ɣun-nan</ta>
            <ta e="T10" id="Seg_555" s="T9">koːcʼi-n</ta>
            <ta e="T11" id="Seg_556" s="T10">je-za-ttə</ta>
            <ta e="T12" id="Seg_557" s="T11">kolub-la</ta>
            <ta e="T13" id="Seg_558" s="T12">wes</ta>
            <ta e="T14" id="Seg_559" s="T13">tʼeɣa</ta>
            <ta e="T15" id="Seg_560" s="T14">man</ta>
            <ta e="T16" id="Seg_561" s="T15">akoška-on</ta>
            <ta e="T17" id="Seg_562" s="T16">mannɨ-paː-n</ta>
            <ta e="T18" id="Seg_563" s="T17">kurʼica-la</ta>
            <ta e="T19" id="Seg_564" s="T18">koːcʼi-n</ta>
            <ta e="T20" id="Seg_565" s="T19">je-wa-ttə</ta>
            <ta e="T21" id="Seg_566" s="T20">teb-la</ta>
            <ta e="T22" id="Seg_567" s="T21">kak</ta>
            <ta e="T23" id="Seg_568" s="T22">waze-zi-tda-ttə</ta>
            <ta e="T24" id="Seg_569" s="T23">ma-də</ta>
            <ta e="T25" id="Seg_570" s="T24">bar-o-t</ta>
            <ta e="T26" id="Seg_571" s="T25">omdə-la-ttə</ta>
            <ta e="T27" id="Seg_572" s="T26">man</ta>
            <ta e="T28" id="Seg_573" s="T27">kɨba-nädäk-ka-nä</ta>
            <ta e="T29" id="Seg_574" s="T28">soɣudʼzʼa-n</ta>
            <ta e="T30" id="Seg_575" s="T29">a</ta>
            <ta e="T31" id="Seg_576" s="T30">kurica-la</ta>
            <ta e="T32" id="Seg_577" s="T31">qaj-no</ta>
            <ta e="T33" id="Seg_578" s="T32">ma-də</ta>
            <ta e="T34" id="Seg_579" s="T33">bar-o-t</ta>
            <ta e="T35" id="Seg_580" s="T34">omdä-la-ttə</ta>
            <ta e="T36" id="Seg_581" s="T35">a</ta>
            <ta e="T37" id="Seg_582" s="T36">qɨba-nädän</ta>
            <ta e="T38" id="Seg_583" s="T37">tʼärɨ-n</ta>
            <ta e="T39" id="Seg_584" s="T38">na</ta>
            <ta e="T40" id="Seg_585" s="T39">ass</ta>
            <ta e="T41" id="Seg_586" s="T40">kurica-la</ta>
            <ta e="T42" id="Seg_587" s="T41">na</ta>
            <ta e="T43" id="Seg_588" s="T42">kolub-la</ta>
            <ta e="T44" id="Seg_589" s="T43">man</ta>
            <ta e="T45" id="Seg_590" s="T44">Valodʼa-nä</ta>
            <ta e="T46" id="Seg_591" s="T45">kʼen-na-u</ta>
            <ta e="T47" id="Seg_592" s="T46">tänärbɨ-za-n</ta>
            <ta e="T48" id="Seg_593" s="T47">kurica-la</ta>
            <ta e="T49" id="Seg_594" s="T48">kak</ta>
            <ta e="T50" id="Seg_595" s="T49">täb-la</ta>
            <ta e="T51" id="Seg_596" s="T50">weze-zi-tda-ttə</ta>
            <ta e="T52" id="Seg_597" s="T51">maː-də</ta>
            <ta e="T53" id="Seg_598" s="T52">par-o-tdə</ta>
            <ta e="T54" id="Seg_599" s="T53">omda-ttə</ta>
            <ta e="T55" id="Seg_600" s="T54">man</ta>
            <ta e="T56" id="Seg_601" s="T55">kɨba-nädäk-ka-nä</ta>
            <ta e="T57" id="Seg_602" s="T56">soɣudʼzʼä-n</ta>
            <ta e="T58" id="Seg_603" s="T57">kurica-la</ta>
            <ta e="T59" id="Seg_604" s="T58">qaj-no</ta>
            <ta e="T60" id="Seg_605" s="T59">wazʼe-zʼä-ttə</ta>
            <ta e="T61" id="Seg_606" s="T60">qɨba-nädän</ta>
            <ta e="T62" id="Seg_607" s="T61">tʼarɨ-n</ta>
            <ta e="T63" id="Seg_608" s="T62">na</ta>
            <ta e="T64" id="Seg_609" s="T63">ass</ta>
            <ta e="T65" id="Seg_610" s="T64">kurica-la</ta>
            <ta e="T66" id="Seg_611" s="T65">na</ta>
            <ta e="T67" id="Seg_612" s="T66">kolub-la</ta>
            <ta e="T68" id="Seg_613" s="T67">a</ta>
            <ta e="T69" id="Seg_614" s="T68">Valodʼa</ta>
            <ta e="T70" id="Seg_615" s="T69">datao</ta>
            <ta e="T71" id="Seg_616" s="T70">lagwat-pɨ-ku-n</ta>
            <ta e="T72" id="Seg_617" s="T71">no</ta>
            <ta e="T73" id="Seg_618" s="T72">man</ta>
            <ta e="T74" id="Seg_619" s="T73">qaj</ta>
            <ta e="T75" id="Seg_620" s="T74">tunu-za-u</ta>
            <ta e="T76" id="Seg_621" s="T75">man</ta>
            <ta e="T77" id="Seg_622" s="T76">täb-la-m</ta>
            <ta e="T78" id="Seg_623" s="T77">ass</ta>
            <ta e="T79" id="Seg_624" s="T78">qo-ǯɨr-gu-za-u</ta>
            <ta e="T80" id="Seg_625" s="T79">qaj-zʼe</ta>
            <ta e="T81" id="Seg_626" s="T80">bis-q-wa-t</ta>
            <ta e="T82" id="Seg_627" s="T81">bis-ku-gu</ta>
            <ta e="T83" id="Seg_628" s="T82">ne</ta>
            <ta e="T84" id="Seg_629" s="T83">nadə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_630" s="T1">ugon</ta>
            <ta e="T3" id="Seg_631" s="T2">man</ta>
            <ta e="T4" id="Seg_632" s="T3">golub-la-m</ta>
            <ta e="T5" id="Seg_633" s="T4">asa</ta>
            <ta e="T6" id="Seg_634" s="T5">qo-nǯir-ku-sɨ-w</ta>
            <ta e="T7" id="Seg_635" s="T6">Čʼornaja_rečka-qɨn</ta>
            <ta e="T8" id="Seg_636" s="T7">okkɨr</ta>
            <ta e="T9" id="Seg_637" s="T8">ne-lʼ-qum-nan</ta>
            <ta e="T10" id="Seg_638" s="T9">koːci-ŋ</ta>
            <ta e="T11" id="Seg_639" s="T10">eː-sɨ-tɨn</ta>
            <ta e="T12" id="Seg_640" s="T11">golub-la</ta>
            <ta e="T13" id="Seg_641" s="T12">wesʼ</ta>
            <ta e="T14" id="Seg_642" s="T13">tʼeɣə</ta>
            <ta e="T15" id="Seg_643" s="T14">man</ta>
            <ta e="T16" id="Seg_644" s="T15">akoška-un</ta>
            <ta e="T17" id="Seg_645" s="T16">*mantɨ-mbɨ-ŋ</ta>
            <ta e="T18" id="Seg_646" s="T17">kurʼica-la</ta>
            <ta e="T19" id="Seg_647" s="T18">koːci-ŋ</ta>
            <ta e="T20" id="Seg_648" s="T19">eː-nɨ-tɨn</ta>
            <ta e="T21" id="Seg_649" s="T20">täp-la</ta>
            <ta e="T22" id="Seg_650" s="T21">kak</ta>
            <ta e="T23" id="Seg_651" s="T22">wazʼe-zi-ntɨ-tɨn</ta>
            <ta e="T24" id="Seg_652" s="T23">mat-tə</ta>
            <ta e="T25" id="Seg_653" s="T24">par-ɨ-ntə</ta>
            <ta e="T26" id="Seg_654" s="T25">omdɨ-lɨ-tɨn</ta>
            <ta e="T27" id="Seg_655" s="T26">man</ta>
            <ta e="T28" id="Seg_656" s="T27">qɨba-nädek-ka-nä</ta>
            <ta e="T29" id="Seg_657" s="T28">sogandʼe-ŋ</ta>
            <ta e="T30" id="Seg_658" s="T29">a</ta>
            <ta e="T31" id="Seg_659" s="T30">kurʼica-la</ta>
            <ta e="T32" id="Seg_660" s="T31">qaj-no</ta>
            <ta e="T33" id="Seg_661" s="T32">man-tə</ta>
            <ta e="T34" id="Seg_662" s="T33">par-ɨ-ntə</ta>
            <ta e="T35" id="Seg_663" s="T34">omdɨ-lɨ-tɨn</ta>
            <ta e="T36" id="Seg_664" s="T35">a</ta>
            <ta e="T37" id="Seg_665" s="T36">qɨba-nädek</ta>
            <ta e="T38" id="Seg_666" s="T37">tʼärɨ-n</ta>
            <ta e="T39" id="Seg_667" s="T38">na</ta>
            <ta e="T40" id="Seg_668" s="T39">asa</ta>
            <ta e="T41" id="Seg_669" s="T40">kurʼica-la</ta>
            <ta e="T42" id="Seg_670" s="T41">na</ta>
            <ta e="T43" id="Seg_671" s="T42">golub-la</ta>
            <ta e="T44" id="Seg_672" s="T43">man</ta>
            <ta e="T45" id="Seg_673" s="T44">Wolodʼa-nä</ta>
            <ta e="T46" id="Seg_674" s="T45">ket-nɨ-w</ta>
            <ta e="T47" id="Seg_675" s="T46">tärba-sɨ-ŋ</ta>
            <ta e="T48" id="Seg_676" s="T47">kurʼica-la</ta>
            <ta e="T49" id="Seg_677" s="T48">kak</ta>
            <ta e="T50" id="Seg_678" s="T49">täp-la</ta>
            <ta e="T51" id="Seg_679" s="T50">wazʼe-zi-ntɨ-tɨn</ta>
            <ta e="T52" id="Seg_680" s="T51">maːt-tə</ta>
            <ta e="T53" id="Seg_681" s="T52">par-ɨ-ntə</ta>
            <ta e="T54" id="Seg_682" s="T53">omdɨ-tɨn</ta>
            <ta e="T55" id="Seg_683" s="T54">man</ta>
            <ta e="T56" id="Seg_684" s="T55">qɨba-nädek-ka-nä</ta>
            <ta e="T57" id="Seg_685" s="T56">sogandʼe-ŋ</ta>
            <ta e="T58" id="Seg_686" s="T57">kurʼica-la</ta>
            <ta e="T59" id="Seg_687" s="T58">qaj-no</ta>
            <ta e="T60" id="Seg_688" s="T59">wazʼe-zi-tɨn</ta>
            <ta e="T61" id="Seg_689" s="T60">qɨba-nädek</ta>
            <ta e="T62" id="Seg_690" s="T61">tʼärɨ-n</ta>
            <ta e="T63" id="Seg_691" s="T62">na</ta>
            <ta e="T64" id="Seg_692" s="T63">asa</ta>
            <ta e="T65" id="Seg_693" s="T64">kurʼica-la</ta>
            <ta e="T66" id="Seg_694" s="T65">na</ta>
            <ta e="T67" id="Seg_695" s="T66">golub-la</ta>
            <ta e="T68" id="Seg_696" s="T67">a</ta>
            <ta e="T69" id="Seg_697" s="T68">Wolodʼa</ta>
            <ta e="T70" id="Seg_698" s="T69">tatawa</ta>
            <ta e="T71" id="Seg_699" s="T70">laːɣwat-mbɨ-ku-n</ta>
            <ta e="T72" id="Seg_700" s="T71">nu</ta>
            <ta e="T73" id="Seg_701" s="T72">man</ta>
            <ta e="T74" id="Seg_702" s="T73">qaj</ta>
            <ta e="T75" id="Seg_703" s="T74">tonu-sɨ-w</ta>
            <ta e="T76" id="Seg_704" s="T75">man</ta>
            <ta e="T77" id="Seg_705" s="T76">täp-la-m</ta>
            <ta e="T78" id="Seg_706" s="T77">asa</ta>
            <ta e="T79" id="Seg_707" s="T78">qo-nǯir-ku-sɨ-w</ta>
            <ta e="T80" id="Seg_708" s="T79">qaj-se</ta>
            <ta e="T81" id="Seg_709" s="T80">pissɨ-ku-nɨ-ntə</ta>
            <ta e="T82" id="Seg_710" s="T81">pissɨ-ku-gu</ta>
            <ta e="T83" id="Seg_711" s="T82">ne</ta>
            <ta e="T84" id="Seg_712" s="T83">nadə</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_713" s="T1">earlier</ta>
            <ta e="T3" id="Seg_714" s="T2">I.NOM</ta>
            <ta e="T4" id="Seg_715" s="T3">pigeon-PL-ACC</ta>
            <ta e="T5" id="Seg_716" s="T4">NEG</ta>
            <ta e="T6" id="Seg_717" s="T5">see-DRV-HAB-PST-1SG.O</ta>
            <ta e="T7" id="Seg_718" s="T6">Chornaja_rechka-LOC</ta>
            <ta e="T8" id="Seg_719" s="T7">one</ta>
            <ta e="T9" id="Seg_720" s="T8">woman-ADJZ-human.being-ADES</ta>
            <ta e="T10" id="Seg_721" s="T9">much-ADVZ</ta>
            <ta e="T11" id="Seg_722" s="T10">be-PST-3PL</ta>
            <ta e="T12" id="Seg_723" s="T11">pigeon-PL.[NOM]</ta>
            <ta e="T13" id="Seg_724" s="T12">all</ta>
            <ta e="T14" id="Seg_725" s="T13">white</ta>
            <ta e="T15" id="Seg_726" s="T14">I.NOM</ta>
            <ta e="T16" id="Seg_727" s="T15">window-PROL</ta>
            <ta e="T17" id="Seg_728" s="T16">look-DUR-1SG.S</ta>
            <ta e="T18" id="Seg_729" s="T17">hen-PL.[NOM]</ta>
            <ta e="T19" id="Seg_730" s="T18">much-ADVZ</ta>
            <ta e="T20" id="Seg_731" s="T19">be-CO-3PL</ta>
            <ta e="T21" id="Seg_732" s="T20">(s)he-PL.[NOM]</ta>
            <ta e="T22" id="Seg_733" s="T21">suddenly</ta>
            <ta e="T23" id="Seg_734" s="T22">fly-DRV-INFER-3PL</ta>
            <ta e="T24" id="Seg_735" s="T23">house.[NOM]-3SG</ta>
            <ta e="T25" id="Seg_736" s="T24">top-EP-ILL</ta>
            <ta e="T26" id="Seg_737" s="T25">sit.down-RES-3PL</ta>
            <ta e="T27" id="Seg_738" s="T26">I.NOM</ta>
            <ta e="T28" id="Seg_739" s="T27">small-girl-DIM-ALL</ta>
            <ta e="T29" id="Seg_740" s="T28">ask-1SG.S</ta>
            <ta e="T30" id="Seg_741" s="T29">and</ta>
            <ta e="T31" id="Seg_742" s="T30">hen-PL.[NOM]</ta>
            <ta e="T32" id="Seg_743" s="T31">what-TRL</ta>
            <ta e="T33" id="Seg_744" s="T32">house.[NOM]-3SG</ta>
            <ta e="T34" id="Seg_745" s="T33">top-EP-ILL</ta>
            <ta e="T35" id="Seg_746" s="T34">sit.down-RES-3PL</ta>
            <ta e="T36" id="Seg_747" s="T35">and</ta>
            <ta e="T37" id="Seg_748" s="T36">small-girl.[NOM]</ta>
            <ta e="T38" id="Seg_749" s="T37">say-3SG.S</ta>
            <ta e="T39" id="Seg_750" s="T38">this.[NOM]</ta>
            <ta e="T40" id="Seg_751" s="T39">NEG</ta>
            <ta e="T41" id="Seg_752" s="T40">hen-PL.[NOM]</ta>
            <ta e="T42" id="Seg_753" s="T41">this.[NOM]</ta>
            <ta e="T43" id="Seg_754" s="T42">pigeon-PL.[NOM]</ta>
            <ta e="T44" id="Seg_755" s="T43">I.NOM</ta>
            <ta e="T45" id="Seg_756" s="T44">Volodya-ALL</ta>
            <ta e="T46" id="Seg_757" s="T45">say-CO-1SG.O</ta>
            <ta e="T47" id="Seg_758" s="T46">think-PST-1SG.S</ta>
            <ta e="T48" id="Seg_759" s="T47">hen-PL.[NOM]</ta>
            <ta e="T49" id="Seg_760" s="T48">suddenly</ta>
            <ta e="T50" id="Seg_761" s="T49">(s)he-PL.[NOM]</ta>
            <ta e="T51" id="Seg_762" s="T50">fly-DRV-INFER-3PL</ta>
            <ta e="T52" id="Seg_763" s="T51">tent.[NOM]-3SG</ta>
            <ta e="T53" id="Seg_764" s="T52">top-EP-ILL</ta>
            <ta e="T54" id="Seg_765" s="T53">sit.down-3PL</ta>
            <ta e="T55" id="Seg_766" s="T54">I.NOM</ta>
            <ta e="T56" id="Seg_767" s="T55">small-girl-DIM-ALL</ta>
            <ta e="T57" id="Seg_768" s="T56">ask-1SG.S</ta>
            <ta e="T58" id="Seg_769" s="T57">hen-PL.[NOM]</ta>
            <ta e="T59" id="Seg_770" s="T58">what-TRL</ta>
            <ta e="T60" id="Seg_771" s="T59">fly-DRV-3PL</ta>
            <ta e="T61" id="Seg_772" s="T60">small-girl.[NOM]</ta>
            <ta e="T62" id="Seg_773" s="T61">say-3SG.S</ta>
            <ta e="T63" id="Seg_774" s="T62">this.[NOM]</ta>
            <ta e="T64" id="Seg_775" s="T63">NEG</ta>
            <ta e="T65" id="Seg_776" s="T64">hen-PL.[NOM]</ta>
            <ta e="T66" id="Seg_777" s="T65">this.[NOM]</ta>
            <ta e="T67" id="Seg_778" s="T66">pigeon-PL.[NOM]</ta>
            <ta e="T68" id="Seg_779" s="T67">and</ta>
            <ta e="T69" id="Seg_780" s="T68">Volodya.[NOM]</ta>
            <ta e="T70" id="Seg_781" s="T69">to.such.extent</ta>
            <ta e="T71" id="Seg_782" s="T70">begin.to.laugh-DUR-HAB-3SG.S</ta>
            <ta e="T72" id="Seg_783" s="T71">now</ta>
            <ta e="T73" id="Seg_784" s="T72">I.NOM</ta>
            <ta e="T74" id="Seg_785" s="T73">either</ta>
            <ta e="T75" id="Seg_786" s="T74">know-PST-1SG.O</ta>
            <ta e="T76" id="Seg_787" s="T75">I.NOM</ta>
            <ta e="T77" id="Seg_788" s="T76">(s)he-PL-ACC</ta>
            <ta e="T78" id="Seg_789" s="T77">NEG</ta>
            <ta e="T79" id="Seg_790" s="T78">see-DRV-HAB-PST-1SG.O</ta>
            <ta e="T80" id="Seg_791" s="T79">what-INSTR</ta>
            <ta e="T81" id="Seg_792" s="T80">laugh-HAB-CO-2SG.S</ta>
            <ta e="T82" id="Seg_793" s="T81">laugh-HAB-INF</ta>
            <ta e="T83" id="Seg_794" s="T82">NEG</ta>
            <ta e="T84" id="Seg_795" s="T83">one.should</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_796" s="T1">раньше</ta>
            <ta e="T3" id="Seg_797" s="T2">я.NOM</ta>
            <ta e="T4" id="Seg_798" s="T3">голубь-PL-ACC</ta>
            <ta e="T5" id="Seg_799" s="T4">NEG</ta>
            <ta e="T6" id="Seg_800" s="T5">увидеть-DRV-HAB-PST-1SG.O</ta>
            <ta e="T7" id="Seg_801" s="T6">Чёрная_речка-LOC</ta>
            <ta e="T8" id="Seg_802" s="T7">один</ta>
            <ta e="T9" id="Seg_803" s="T8">женщина-ADJZ-человек-ADES</ta>
            <ta e="T10" id="Seg_804" s="T9">много-ADVZ</ta>
            <ta e="T11" id="Seg_805" s="T10">быть-PST-3PL</ta>
            <ta e="T12" id="Seg_806" s="T11">голубь-PL.[NOM]</ta>
            <ta e="T13" id="Seg_807" s="T12">весь</ta>
            <ta e="T14" id="Seg_808" s="T13">белый</ta>
            <ta e="T15" id="Seg_809" s="T14">я.NOM</ta>
            <ta e="T16" id="Seg_810" s="T15">окно-PROL</ta>
            <ta e="T17" id="Seg_811" s="T16">посмотреть-DUR-1SG.S</ta>
            <ta e="T18" id="Seg_812" s="T17">курица-PL.[NOM]</ta>
            <ta e="T19" id="Seg_813" s="T18">много-ADVZ</ta>
            <ta e="T20" id="Seg_814" s="T19">быть-CO-3PL</ta>
            <ta e="T21" id="Seg_815" s="T20">он(а)-PL.[NOM]</ta>
            <ta e="T22" id="Seg_816" s="T21">как</ta>
            <ta e="T23" id="Seg_817" s="T22">полететь-DRV-INFER-3PL</ta>
            <ta e="T24" id="Seg_818" s="T23">дом.[NOM]-3SG</ta>
            <ta e="T25" id="Seg_819" s="T24">верхняя.часть-EP-ILL</ta>
            <ta e="T26" id="Seg_820" s="T25">сесть-RES-3PL</ta>
            <ta e="T27" id="Seg_821" s="T26">я.NOM</ta>
            <ta e="T28" id="Seg_822" s="T27">маленький-девушка-DIM-ALL</ta>
            <ta e="T29" id="Seg_823" s="T28">спросить-1SG.S</ta>
            <ta e="T30" id="Seg_824" s="T29">а</ta>
            <ta e="T31" id="Seg_825" s="T30">курица-PL.[NOM]</ta>
            <ta e="T32" id="Seg_826" s="T31">что-TRL</ta>
            <ta e="T33" id="Seg_827" s="T32">дом.[NOM]-3SG</ta>
            <ta e="T34" id="Seg_828" s="T33">верхняя.часть-EP-ILL</ta>
            <ta e="T35" id="Seg_829" s="T34">сесть-RES-3PL</ta>
            <ta e="T36" id="Seg_830" s="T35">а</ta>
            <ta e="T37" id="Seg_831" s="T36">маленький-девушка.[NOM]</ta>
            <ta e="T38" id="Seg_832" s="T37">сказать-3SG.S</ta>
            <ta e="T39" id="Seg_833" s="T38">этот.[NOM]</ta>
            <ta e="T40" id="Seg_834" s="T39">NEG</ta>
            <ta e="T41" id="Seg_835" s="T40">курица-PL.[NOM]</ta>
            <ta e="T42" id="Seg_836" s="T41">этот.[NOM]</ta>
            <ta e="T43" id="Seg_837" s="T42">голубь-PL.[NOM]</ta>
            <ta e="T44" id="Seg_838" s="T43">я.NOM</ta>
            <ta e="T45" id="Seg_839" s="T44">Володя-ALL</ta>
            <ta e="T46" id="Seg_840" s="T45">сказать-CO-1SG.O</ta>
            <ta e="T47" id="Seg_841" s="T46">думать-PST-1SG.S</ta>
            <ta e="T48" id="Seg_842" s="T47">курица-PL.[NOM]</ta>
            <ta e="T49" id="Seg_843" s="T48">как</ta>
            <ta e="T50" id="Seg_844" s="T49">он(а)-PL.[NOM]</ta>
            <ta e="T51" id="Seg_845" s="T50">полететь-DRV-INFER-3PL</ta>
            <ta e="T52" id="Seg_846" s="T51">чум.[NOM]-3SG</ta>
            <ta e="T53" id="Seg_847" s="T52">верхняя.часть-EP-ILL</ta>
            <ta e="T54" id="Seg_848" s="T53">сесть-3PL</ta>
            <ta e="T55" id="Seg_849" s="T54">я.NOM</ta>
            <ta e="T56" id="Seg_850" s="T55">маленький-девушка-DIM-ALL</ta>
            <ta e="T57" id="Seg_851" s="T56">спросить-1SG.S</ta>
            <ta e="T58" id="Seg_852" s="T57">курица-PL.[NOM]</ta>
            <ta e="T59" id="Seg_853" s="T58">что-TRL</ta>
            <ta e="T60" id="Seg_854" s="T59">полететь-DRV-3PL</ta>
            <ta e="T61" id="Seg_855" s="T60">маленький-девушка.[NOM]</ta>
            <ta e="T62" id="Seg_856" s="T61">сказать-3SG.S</ta>
            <ta e="T63" id="Seg_857" s="T62">этот.[NOM]</ta>
            <ta e="T64" id="Seg_858" s="T63">NEG</ta>
            <ta e="T65" id="Seg_859" s="T64">курица-PL.[NOM]</ta>
            <ta e="T66" id="Seg_860" s="T65">этот.[NOM]</ta>
            <ta e="T67" id="Seg_861" s="T66">голубь-PL.[NOM]</ta>
            <ta e="T68" id="Seg_862" s="T67">а</ta>
            <ta e="T69" id="Seg_863" s="T68">Володя.[NOM]</ta>
            <ta e="T70" id="Seg_864" s="T69">до.того</ta>
            <ta e="T71" id="Seg_865" s="T70">засмеяться-DUR-HAB-3SG.S</ta>
            <ta e="T72" id="Seg_866" s="T71">ну</ta>
            <ta e="T73" id="Seg_867" s="T72">я.NOM</ta>
            <ta e="T74" id="Seg_868" s="T73">ли</ta>
            <ta e="T75" id="Seg_869" s="T74">знать-PST-1SG.O</ta>
            <ta e="T76" id="Seg_870" s="T75">я.NOM</ta>
            <ta e="T77" id="Seg_871" s="T76">он(а)-PL-ACC</ta>
            <ta e="T78" id="Seg_872" s="T77">NEG</ta>
            <ta e="T79" id="Seg_873" s="T78">увидеть-DRV-HAB-PST-1SG.O</ta>
            <ta e="T80" id="Seg_874" s="T79">что-INSTR</ta>
            <ta e="T81" id="Seg_875" s="T80">смеяться-HAB-CO-2SG.S</ta>
            <ta e="T82" id="Seg_876" s="T81">смеяться-HAB-INF</ta>
            <ta e="T83" id="Seg_877" s="T82">NEG</ta>
            <ta e="T84" id="Seg_878" s="T83">надо</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_879" s="T1">adv</ta>
            <ta e="T3" id="Seg_880" s="T2">pers</ta>
            <ta e="T4" id="Seg_881" s="T3">n-n:num-n:case</ta>
            <ta e="T5" id="Seg_882" s="T4">ptcl</ta>
            <ta e="T6" id="Seg_883" s="T5">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_884" s="T6">nprop-n:case</ta>
            <ta e="T8" id="Seg_885" s="T7">num</ta>
            <ta e="T9" id="Seg_886" s="T8">n-n&gt;adj-n-n:case</ta>
            <ta e="T10" id="Seg_887" s="T9">quant-quant&gt;adv</ta>
            <ta e="T11" id="Seg_888" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_889" s="T11">n-n:num.[n:case]</ta>
            <ta e="T13" id="Seg_890" s="T12">quant</ta>
            <ta e="T14" id="Seg_891" s="T13">adj</ta>
            <ta e="T15" id="Seg_892" s="T14">pers</ta>
            <ta e="T16" id="Seg_893" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_894" s="T16">v-v&gt;v-v:pn</ta>
            <ta e="T18" id="Seg_895" s="T17">n-n:num.[n:case]</ta>
            <ta e="T19" id="Seg_896" s="T18">quant-quant&gt;adv</ta>
            <ta e="T20" id="Seg_897" s="T19">v-v:ins-v:pn</ta>
            <ta e="T21" id="Seg_898" s="T20">pers-n:num.[n:case]</ta>
            <ta e="T22" id="Seg_899" s="T21">adv</ta>
            <ta e="T23" id="Seg_900" s="T22">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T24" id="Seg_901" s="T23">n.[n:case]-n:poss</ta>
            <ta e="T25" id="Seg_902" s="T24">n-n:ins-n:case</ta>
            <ta e="T26" id="Seg_903" s="T25">v-v&gt;v-v:pn</ta>
            <ta e="T27" id="Seg_904" s="T26">pers</ta>
            <ta e="T28" id="Seg_905" s="T27">adj-n-n&gt;n-n:case</ta>
            <ta e="T29" id="Seg_906" s="T28">v-v:pn</ta>
            <ta e="T30" id="Seg_907" s="T29">conj</ta>
            <ta e="T31" id="Seg_908" s="T30">n-n:num.[n:case]</ta>
            <ta e="T32" id="Seg_909" s="T31">interrog-n:case</ta>
            <ta e="T33" id="Seg_910" s="T32">n.[n:case]-n:poss</ta>
            <ta e="T34" id="Seg_911" s="T33">n-n:ins-n:case</ta>
            <ta e="T35" id="Seg_912" s="T34">v-v&gt;v-v:pn</ta>
            <ta e="T36" id="Seg_913" s="T35">conj</ta>
            <ta e="T37" id="Seg_914" s="T36">adj-n.[n:case]</ta>
            <ta e="T38" id="Seg_915" s="T37">v-v:pn</ta>
            <ta e="T39" id="Seg_916" s="T38">dem.[n:case]</ta>
            <ta e="T40" id="Seg_917" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_918" s="T40">n-n:num.[n:case]</ta>
            <ta e="T42" id="Seg_919" s="T41">dem.[n:case]</ta>
            <ta e="T43" id="Seg_920" s="T42">n-n:num.[n:case]</ta>
            <ta e="T44" id="Seg_921" s="T43">pers</ta>
            <ta e="T45" id="Seg_922" s="T44">nprop-n:case</ta>
            <ta e="T46" id="Seg_923" s="T45">v-v:ins-v:pn</ta>
            <ta e="T47" id="Seg_924" s="T46">v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_925" s="T47">n-n:num.[n:case]</ta>
            <ta e="T49" id="Seg_926" s="T48">adv</ta>
            <ta e="T50" id="Seg_927" s="T49">pers-n:num.[n:case]</ta>
            <ta e="T51" id="Seg_928" s="T50">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T52" id="Seg_929" s="T51">n.[n:case]-n:poss</ta>
            <ta e="T53" id="Seg_930" s="T52">n-n:ins-n:case</ta>
            <ta e="T54" id="Seg_931" s="T53">v-v:pn</ta>
            <ta e="T55" id="Seg_932" s="T54">pers</ta>
            <ta e="T56" id="Seg_933" s="T55">adj-n-n&gt;n-n:case</ta>
            <ta e="T57" id="Seg_934" s="T56">v-v:pn</ta>
            <ta e="T58" id="Seg_935" s="T57">n-n:num.[n:case]</ta>
            <ta e="T59" id="Seg_936" s="T58">interrog-n:case</ta>
            <ta e="T60" id="Seg_937" s="T59">v-v&gt;v-v:pn</ta>
            <ta e="T61" id="Seg_938" s="T60">adj-n.[n:case]</ta>
            <ta e="T62" id="Seg_939" s="T61">v-v:pn</ta>
            <ta e="T63" id="Seg_940" s="T62">dem.[n:case]</ta>
            <ta e="T64" id="Seg_941" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_942" s="T64">n-n:num.[n:case]</ta>
            <ta e="T66" id="Seg_943" s="T65">dem.[n:case]</ta>
            <ta e="T67" id="Seg_944" s="T66">n-n:num.[n:case]</ta>
            <ta e="T68" id="Seg_945" s="T67">conj</ta>
            <ta e="T69" id="Seg_946" s="T68">nprop.[n:case]</ta>
            <ta e="T70" id="Seg_947" s="T69">adv</ta>
            <ta e="T71" id="Seg_948" s="T70">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T72" id="Seg_949" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_950" s="T72">pers</ta>
            <ta e="T74" id="Seg_951" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_952" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_953" s="T75">pers</ta>
            <ta e="T77" id="Seg_954" s="T76">pers-n:num-n:case</ta>
            <ta e="T78" id="Seg_955" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_956" s="T78">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T80" id="Seg_957" s="T79">interrog-n:case</ta>
            <ta e="T81" id="Seg_958" s="T80">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T82" id="Seg_959" s="T81">v-v&gt;v-v:inf</ta>
            <ta e="T83" id="Seg_960" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_961" s="T83">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_962" s="T1">adv</ta>
            <ta e="T3" id="Seg_963" s="T2">pers</ta>
            <ta e="T4" id="Seg_964" s="T3">n</ta>
            <ta e="T5" id="Seg_965" s="T4">ptcl</ta>
            <ta e="T6" id="Seg_966" s="T5">v</ta>
            <ta e="T7" id="Seg_967" s="T6">nprop</ta>
            <ta e="T8" id="Seg_968" s="T7">num</ta>
            <ta e="T9" id="Seg_969" s="T8">n</ta>
            <ta e="T10" id="Seg_970" s="T9">adv</ta>
            <ta e="T11" id="Seg_971" s="T10">v</ta>
            <ta e="T12" id="Seg_972" s="T11">n</ta>
            <ta e="T13" id="Seg_973" s="T12">quant</ta>
            <ta e="T14" id="Seg_974" s="T13">adj</ta>
            <ta e="T15" id="Seg_975" s="T14">pers</ta>
            <ta e="T16" id="Seg_976" s="T15">n</ta>
            <ta e="T17" id="Seg_977" s="T16">v</ta>
            <ta e="T18" id="Seg_978" s="T17">n</ta>
            <ta e="T19" id="Seg_979" s="T18">adv</ta>
            <ta e="T20" id="Seg_980" s="T19">v</ta>
            <ta e="T21" id="Seg_981" s="T20">pers</ta>
            <ta e="T22" id="Seg_982" s="T21">adv</ta>
            <ta e="T23" id="Seg_983" s="T22">v</ta>
            <ta e="T24" id="Seg_984" s="T23">n</ta>
            <ta e="T25" id="Seg_985" s="T24">n</ta>
            <ta e="T26" id="Seg_986" s="T25">v</ta>
            <ta e="T27" id="Seg_987" s="T26">pers</ta>
            <ta e="T28" id="Seg_988" s="T27">n</ta>
            <ta e="T29" id="Seg_989" s="T28">v</ta>
            <ta e="T30" id="Seg_990" s="T29">conj</ta>
            <ta e="T31" id="Seg_991" s="T30">n</ta>
            <ta e="T32" id="Seg_992" s="T31">interrog</ta>
            <ta e="T33" id="Seg_993" s="T32">n</ta>
            <ta e="T34" id="Seg_994" s="T33">n</ta>
            <ta e="T35" id="Seg_995" s="T34">v</ta>
            <ta e="T36" id="Seg_996" s="T35">conj</ta>
            <ta e="T37" id="Seg_997" s="T36">n</ta>
            <ta e="T38" id="Seg_998" s="T37">v</ta>
            <ta e="T39" id="Seg_999" s="T38">dem</ta>
            <ta e="T40" id="Seg_1000" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_1001" s="T40">n</ta>
            <ta e="T42" id="Seg_1002" s="T41">dem</ta>
            <ta e="T43" id="Seg_1003" s="T42">n</ta>
            <ta e="T44" id="Seg_1004" s="T43">pers</ta>
            <ta e="T45" id="Seg_1005" s="T44">nprop</ta>
            <ta e="T46" id="Seg_1006" s="T45">v</ta>
            <ta e="T47" id="Seg_1007" s="T46">v</ta>
            <ta e="T48" id="Seg_1008" s="T47">n</ta>
            <ta e="T49" id="Seg_1009" s="T48">adv</ta>
            <ta e="T50" id="Seg_1010" s="T49">pers</ta>
            <ta e="T51" id="Seg_1011" s="T50">v</ta>
            <ta e="T52" id="Seg_1012" s="T51">n</ta>
            <ta e="T53" id="Seg_1013" s="T52">n</ta>
            <ta e="T54" id="Seg_1014" s="T53">v</ta>
            <ta e="T55" id="Seg_1015" s="T54">pers</ta>
            <ta e="T56" id="Seg_1016" s="T55">n</ta>
            <ta e="T57" id="Seg_1017" s="T56">v</ta>
            <ta e="T58" id="Seg_1018" s="T57">n</ta>
            <ta e="T59" id="Seg_1019" s="T58">interrog</ta>
            <ta e="T60" id="Seg_1020" s="T59">v</ta>
            <ta e="T61" id="Seg_1021" s="T60">n</ta>
            <ta e="T62" id="Seg_1022" s="T61">v</ta>
            <ta e="T63" id="Seg_1023" s="T62">dem</ta>
            <ta e="T64" id="Seg_1024" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_1025" s="T64">n</ta>
            <ta e="T66" id="Seg_1026" s="T65">dem</ta>
            <ta e="T67" id="Seg_1027" s="T66">n</ta>
            <ta e="T68" id="Seg_1028" s="T67">conj</ta>
            <ta e="T69" id="Seg_1029" s="T68">nprop</ta>
            <ta e="T70" id="Seg_1030" s="T69">adv</ta>
            <ta e="T71" id="Seg_1031" s="T70">v</ta>
            <ta e="T72" id="Seg_1032" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_1033" s="T72">pers</ta>
            <ta e="T74" id="Seg_1034" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_1035" s="T74">v</ta>
            <ta e="T76" id="Seg_1036" s="T75">pers</ta>
            <ta e="T77" id="Seg_1037" s="T76">pers</ta>
            <ta e="T78" id="Seg_1038" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_1039" s="T78">v</ta>
            <ta e="T80" id="Seg_1040" s="T79">interrog</ta>
            <ta e="T81" id="Seg_1041" s="T80">v</ta>
            <ta e="T82" id="Seg_1042" s="T81">v</ta>
            <ta e="T83" id="Seg_1043" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_1044" s="T83">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1045" s="T1">adv:Time</ta>
            <ta e="T3" id="Seg_1046" s="T2">pro.h:E</ta>
            <ta e="T4" id="Seg_1047" s="T3">np:Th</ta>
            <ta e="T7" id="Seg_1048" s="T6">np:L</ta>
            <ta e="T9" id="Seg_1049" s="T8">np.h:Poss</ta>
            <ta e="T11" id="Seg_1050" s="T10">0.3.h:Th</ta>
            <ta e="T12" id="Seg_1051" s="T11">np:Th</ta>
            <ta e="T15" id="Seg_1052" s="T14">pro.h:A</ta>
            <ta e="T16" id="Seg_1053" s="T15">np:Path</ta>
            <ta e="T18" id="Seg_1054" s="T17">np:Th</ta>
            <ta e="T21" id="Seg_1055" s="T20">pro:A</ta>
            <ta e="T25" id="Seg_1056" s="T24">np:G</ta>
            <ta e="T26" id="Seg_1057" s="T25">0.3:A</ta>
            <ta e="T27" id="Seg_1058" s="T26">pro.h:A</ta>
            <ta e="T28" id="Seg_1059" s="T27">np.h:R</ta>
            <ta e="T31" id="Seg_1060" s="T30">np:A</ta>
            <ta e="T34" id="Seg_1061" s="T33">np:G</ta>
            <ta e="T37" id="Seg_1062" s="T36">np.h:A</ta>
            <ta e="T39" id="Seg_1063" s="T38">pro:Th</ta>
            <ta e="T42" id="Seg_1064" s="T41">pro:Th</ta>
            <ta e="T44" id="Seg_1065" s="T43">pro.h:A</ta>
            <ta e="T45" id="Seg_1066" s="T44">np.h:R</ta>
            <ta e="T47" id="Seg_1067" s="T46">0.1.h:E</ta>
            <ta e="T50" id="Seg_1068" s="T49">pro:A</ta>
            <ta e="T53" id="Seg_1069" s="T52">np:G</ta>
            <ta e="T54" id="Seg_1070" s="T53">0.3:A</ta>
            <ta e="T55" id="Seg_1071" s="T54">pro.h:A</ta>
            <ta e="T56" id="Seg_1072" s="T55">np.h:R</ta>
            <ta e="T58" id="Seg_1073" s="T57">np:A</ta>
            <ta e="T61" id="Seg_1074" s="T60">np.h:A</ta>
            <ta e="T63" id="Seg_1075" s="T62">pro:Th</ta>
            <ta e="T66" id="Seg_1076" s="T65">pro:Th</ta>
            <ta e="T69" id="Seg_1077" s="T68">np.h:A</ta>
            <ta e="T73" id="Seg_1078" s="T72">pro.h:E</ta>
            <ta e="T76" id="Seg_1079" s="T75">pro.h:E</ta>
            <ta e="T77" id="Seg_1080" s="T76">pro:Th</ta>
            <ta e="T81" id="Seg_1081" s="T80">0.2.h:A</ta>
            <ta e="T82" id="Seg_1082" s="T81">v:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_1083" s="T2">pro.h:S</ta>
            <ta e="T4" id="Seg_1084" s="T3">np:O</ta>
            <ta e="T6" id="Seg_1085" s="T5">v:pred</ta>
            <ta e="T11" id="Seg_1086" s="T10">0.3.h:S v:pred</ta>
            <ta e="T12" id="Seg_1087" s="T11">np:S</ta>
            <ta e="T14" id="Seg_1088" s="T13">adj:pred</ta>
            <ta e="T15" id="Seg_1089" s="T14">pro.h:S</ta>
            <ta e="T17" id="Seg_1090" s="T16">v:pred</ta>
            <ta e="T18" id="Seg_1091" s="T17">np:S</ta>
            <ta e="T20" id="Seg_1092" s="T19">v:pred</ta>
            <ta e="T21" id="Seg_1093" s="T20">pro:S</ta>
            <ta e="T23" id="Seg_1094" s="T22">v:pred</ta>
            <ta e="T26" id="Seg_1095" s="T25">0.3:S v:pred</ta>
            <ta e="T27" id="Seg_1096" s="T26">pro.h:S</ta>
            <ta e="T29" id="Seg_1097" s="T28">v:pred</ta>
            <ta e="T31" id="Seg_1098" s="T30">np:S</ta>
            <ta e="T35" id="Seg_1099" s="T34">v:pred</ta>
            <ta e="T37" id="Seg_1100" s="T36">np.h:S</ta>
            <ta e="T38" id="Seg_1101" s="T37">v:pred</ta>
            <ta e="T39" id="Seg_1102" s="T38">pro:S</ta>
            <ta e="T41" id="Seg_1103" s="T40">n:pred</ta>
            <ta e="T42" id="Seg_1104" s="T41">pro:S</ta>
            <ta e="T43" id="Seg_1105" s="T42">n:pred</ta>
            <ta e="T44" id="Seg_1106" s="T43">pro.h:S</ta>
            <ta e="T46" id="Seg_1107" s="T45">v:pred</ta>
            <ta e="T47" id="Seg_1108" s="T46">0.1.h:S v:pred</ta>
            <ta e="T50" id="Seg_1109" s="T49">pro:S</ta>
            <ta e="T51" id="Seg_1110" s="T50">v:pred</ta>
            <ta e="T54" id="Seg_1111" s="T53">0.3:S v:pred</ta>
            <ta e="T55" id="Seg_1112" s="T54">pro.h:S</ta>
            <ta e="T57" id="Seg_1113" s="T56">v:pred</ta>
            <ta e="T58" id="Seg_1114" s="T57">np:S</ta>
            <ta e="T60" id="Seg_1115" s="T59">v:pred</ta>
            <ta e="T61" id="Seg_1116" s="T60">np.h:S</ta>
            <ta e="T62" id="Seg_1117" s="T61">v:pred</ta>
            <ta e="T63" id="Seg_1118" s="T62">pro:S</ta>
            <ta e="T65" id="Seg_1119" s="T64">n:pred</ta>
            <ta e="T66" id="Seg_1120" s="T65">pro:S</ta>
            <ta e="T67" id="Seg_1121" s="T66">n:pred</ta>
            <ta e="T69" id="Seg_1122" s="T68">np.h:S</ta>
            <ta e="T71" id="Seg_1123" s="T70">v:pred</ta>
            <ta e="T73" id="Seg_1124" s="T72">pro.h:S</ta>
            <ta e="T75" id="Seg_1125" s="T74">v:pred</ta>
            <ta e="T76" id="Seg_1126" s="T75">pro.h:S</ta>
            <ta e="T77" id="Seg_1127" s="T76">pro:O</ta>
            <ta e="T79" id="Seg_1128" s="T78">v:pred</ta>
            <ta e="T81" id="Seg_1129" s="T80">0.2.h:S v:pred</ta>
            <ta e="T82" id="Seg_1130" s="T81">v:O</ta>
            <ta e="T84" id="Seg_1131" s="T83">ptcl:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_1132" s="T3">RUS:cult</ta>
            <ta e="T12" id="Seg_1133" s="T11">RUS:cult</ta>
            <ta e="T13" id="Seg_1134" s="T12">RUS:core</ta>
            <ta e="T16" id="Seg_1135" s="T15">RUS:cult</ta>
            <ta e="T18" id="Seg_1136" s="T17">RUS:cult</ta>
            <ta e="T22" id="Seg_1137" s="T21">RUS:disc</ta>
            <ta e="T30" id="Seg_1138" s="T29">RUS:gram</ta>
            <ta e="T31" id="Seg_1139" s="T30">RUS:cult</ta>
            <ta e="T34" id="Seg_1140" s="T33">WNB Noun or pp</ta>
            <ta e="T36" id="Seg_1141" s="T35">RUS:gram</ta>
            <ta e="T41" id="Seg_1142" s="T40">RUS:cult</ta>
            <ta e="T43" id="Seg_1143" s="T42">RUS:cult</ta>
            <ta e="T45" id="Seg_1144" s="T44">RUS:cult</ta>
            <ta e="T48" id="Seg_1145" s="T47">RUS:cult</ta>
            <ta e="T49" id="Seg_1146" s="T48">RUS:disc</ta>
            <ta e="T53" id="Seg_1147" s="T52">WNB Noun or pp</ta>
            <ta e="T58" id="Seg_1148" s="T57">RUS:cult</ta>
            <ta e="T65" id="Seg_1149" s="T64">RUS:cult</ta>
            <ta e="T67" id="Seg_1150" s="T66">RUS:cult</ta>
            <ta e="T68" id="Seg_1151" s="T67">RUS:gram</ta>
            <ta e="T69" id="Seg_1152" s="T68">RUS:cult</ta>
            <ta e="T70" id="Seg_1153" s="T69">RUS:core</ta>
            <ta e="T72" id="Seg_1154" s="T71">RUS:disc</ta>
            <ta e="T83" id="Seg_1155" s="T82">RUS:gram</ta>
            <ta e="T84" id="Seg_1156" s="T83">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_1157" s="T1">Earlier I haven't seen pigeons.</ta>
            <ta e="T11" id="Seg_1158" s="T6">One woman in Chornaja Rechka used to have many [of pigeons].</ta>
            <ta e="T14" id="Seg_1159" s="T11">All pigeons were white.</ta>
            <ta e="T17" id="Seg_1160" s="T14">I was looking in the window.</ta>
            <ta e="T20" id="Seg_1161" s="T17">There were many hens.</ta>
            <ta e="T26" id="Seg_1162" s="T20">Suddenly they flew up and landed onto the roof.</ta>
            <ta e="T35" id="Seg_1163" s="T26">I asked a girl: “Why did hens landed onto the roof?”</ta>
            <ta e="T43" id="Seg_1164" s="T35">And the girl said: “These are not hens, but pigeons.”</ta>
            <ta e="T54" id="Seg_1165" s="T43">I said to Volodya: “I thought these were hens, they suddenly flew up and landed onto the roof.</ta>
            <ta e="T60" id="Seg_1166" s="T54">I asked a girl: “Why did hens fly up?”</ta>
            <ta e="T67" id="Seg_1167" s="T60">The girl said: “These are not hens, these are pigeons.”</ta>
            <ta e="T71" id="Seg_1168" s="T67">And Volodya was laughing so much.</ta>
            <ta e="T75" id="Seg_1169" s="T71">How could I know?</ta>
            <ta e="T79" id="Seg_1170" s="T75">I haven't seen them [earlier].</ta>
            <ta e="T81" id="Seg_1171" s="T79">Why do you laugh?</ta>
            <ta e="T84" id="Seg_1172" s="T81">One doesn't have to laugh.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_1173" s="T1">Früher habe ich keine Tauben gesehen.</ta>
            <ta e="T11" id="Seg_1174" s="T6">In Chornaja Rechka hatte eine Frau viele [Tauben].</ta>
            <ta e="T14" id="Seg_1175" s="T11">Alle Tauben waren weiß.</ta>
            <ta e="T17" id="Seg_1176" s="T14">Ich sah durchs Fenster.</ta>
            <ta e="T20" id="Seg_1177" s="T17">Da waren viele Hennen.</ta>
            <ta e="T26" id="Seg_1178" s="T20">Sie flogen plötzlich hoch und landeten auf dem Dach.</ta>
            <ta e="T35" id="Seg_1179" s="T26">Ich fragte ein Mädchen: "Warum sind die Hennen auf dem Dach gelandet?"</ta>
            <ta e="T43" id="Seg_1180" s="T35">Und das Mädchen sagte: "Das sind keine Hennen, das sind Tauben."</ta>
            <ta e="T54" id="Seg_1181" s="T43">Ich sagte zu Volodya: "Ich dachte, es wären Hennen, sie sind plötzlich hochgeflogen und auf dem Dach gelandet.</ta>
            <ta e="T60" id="Seg_1182" s="T54">Ich habe ein Mädchen gefragt: "Warum sind die Hennen hochgeflogen?"</ta>
            <ta e="T67" id="Seg_1183" s="T60">Das Mädchen sagte: "Das sind keine Hennen, das sind Tauben."</ta>
            <ta e="T71" id="Seg_1184" s="T67">Und Volodya lachte so sehr.</ta>
            <ta e="T75" id="Seg_1185" s="T71">Wie hätte ich es wissen können?</ta>
            <ta e="T79" id="Seg_1186" s="T75">Ich hatte [zuvor] keine gesehen.</ta>
            <ta e="T81" id="Seg_1187" s="T79">Warum lachst du?</ta>
            <ta e="T84" id="Seg_1188" s="T81">Man sollte nicht lachen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_1189" s="T1">Раньше я голубей не видела.</ta>
            <ta e="T11" id="Seg_1190" s="T6">В Черной речке у одной женщины много было.</ta>
            <ta e="T14" id="Seg_1191" s="T11">Голуби все белые.</ta>
            <ta e="T17" id="Seg_1192" s="T14">Я в окошко смотрела.</ta>
            <ta e="T20" id="Seg_1193" s="T17">Кур много.</ta>
            <ta e="T26" id="Seg_1194" s="T20">Они как полетели, на крышу сели.</ta>
            <ta e="T35" id="Seg_1195" s="T26">Я у девчонки спросила: “Курицы почему на крышу сели?”</ta>
            <ta e="T43" id="Seg_1196" s="T35">Девочка сказала: “Это не курицы, это голуби”.</ta>
            <ta e="T54" id="Seg_1197" s="T43">Я Володе сказала: “Я думала, курицы, как они полетели, на крышу сели.</ta>
            <ta e="T60" id="Seg_1198" s="T54">Я девочку спросила: “Курицы почему полетели?”</ta>
            <ta e="T67" id="Seg_1199" s="T60">Девочка сказала: “Это не курицы, это голуби”.</ta>
            <ta e="T71" id="Seg_1200" s="T67">А Володя до того смеялся.</ta>
            <ta e="T75" id="Seg_1201" s="T71">Ну я что ли знала?</ta>
            <ta e="T79" id="Seg_1202" s="T75">Я их не видела.</ta>
            <ta e="T81" id="Seg_1203" s="T79">Ты чего смеешься?</ta>
            <ta e="T84" id="Seg_1204" s="T81">Смеяться не надо.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_1205" s="T1">раньше я голубей не видала</ta>
            <ta e="T11" id="Seg_1206" s="T6">в Черной речке у одной женщины много было</ta>
            <ta e="T14" id="Seg_1207" s="T11">голуби все белые</ta>
            <ta e="T17" id="Seg_1208" s="T14">я в окошко смотрела</ta>
            <ta e="T20" id="Seg_1209" s="T17">кур много</ta>
            <ta e="T26" id="Seg_1210" s="T20">они как полетели на крышу сели</ta>
            <ta e="T35" id="Seg_1211" s="T26">я у девчонки спросила курица почему на крышу сели</ta>
            <ta e="T43" id="Seg_1212" s="T35">девочка сказала это не курицы это голуби</ta>
            <ta e="T54" id="Seg_1213" s="T43">я Володе сказала думала курицы как они полетели на крышу сели</ta>
            <ta e="T60" id="Seg_1214" s="T54">я девочку спросила курицы почему полетели</ta>
            <ta e="T67" id="Seg_1215" s="T60">девочка сказала это не курицы это голуби</ta>
            <ta e="T71" id="Seg_1216" s="T67">а Володя до того смеялся</ta>
            <ta e="T75" id="Seg_1217" s="T71">а я чего знала</ta>
            <ta e="T79" id="Seg_1218" s="T75">я их не видала</ta>
            <ta e="T81" id="Seg_1219" s="T79">чего смеешься</ta>
            <ta e="T84" id="Seg_1220" s="T81">смеяться не надо</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T26" id="Seg_1221" s="T20">[KuAI:] Variants: 'omdelattə', 'omdälattə'. [BrM:] Tentative analysis of 'omdəlattə'.</ta>
            <ta e="T35" id="Seg_1222" s="T26">[BrM:] Tentative analysis of 'omdälattə'.</ta>
            <ta e="T54" id="Seg_1223" s="T43">[KuAI:] Variant: 'barot'. [BrM:] 'parot də' changed to 'parotdə'.</ta>
            <ta e="T60" id="Seg_1224" s="T54">[KuAI:] Variants: 'kɨbanädänä', 'wazʼedʼättə'.</ta>
            <ta e="T67" id="Seg_1225" s="T60">[KuAI:] Variant: 'golubla'.</ta>
            <ta e="T71" id="Seg_1226" s="T67">[BrM:] 'da tao' changed to 'datao'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
