<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_PlayingWithCat_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_PlayingWithCat_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">67</ud-information>
            <ud-information attribute-name="# HIAT:w">52</ud-information>
            <ud-information attribute-name="# e">52</ud-information>
            <ud-information attribute-name="# HIAT:u">10</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T53" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Me</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">ugon</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">maːʒukuzawtə</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">skatʼinam</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Tebnan</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">iːkuzawtə</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">puzɨrʼim</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">Na</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">puːɣɨlǯəǯal</ts>
                  <nts id="Seg_35" n="HIAT:ip">,</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">täp</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">iːbɨkan</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">aːzukun</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_48" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">Täbɨn</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">sunʼnʼät</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">penkowtə</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">karoɣɨm</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">alʼi</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">püllagagala</ts>
                  <nts id="Seg_66" n="HIAT:ip">,</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_69" n="HIAT:w" s="T19">nagur</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_72" n="HIAT:w" s="T20">püllagagala</ts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_76" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">Täp</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">rokšɨpbaː</ts>
                  <nts id="Seg_82" n="HIAT:ip">.</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_85" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_87" n="HIAT:w" s="T23">Me</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_90" n="HIAT:w" s="T24">kɨskan</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_93" n="HIAT:w" s="T25">talʼdʼötdə</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_96" n="HIAT:w" s="T26">i</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_99" n="HIAT:w" s="T27">saːrawtə</ts>
                  <nts id="Seg_100" n="HIAT:ip">.</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_103" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_105" n="HIAT:w" s="T28">Kɨska</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_108" n="HIAT:w" s="T29">kak</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_111" n="HIAT:w" s="T30">palʼdʼelʼe</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_114" n="HIAT:w" s="T31">übəratdə</ts>
                  <nts id="Seg_115" n="HIAT:ip">.</nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_118" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_120" n="HIAT:w" s="T32">Istol</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_123" n="HIAT:w" s="T33">baːrot</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_126" n="HIAT:w" s="T34">sɨrətʼen</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_129" n="HIAT:w" s="T35">i</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_132" n="HIAT:w" s="T36">poːrlatdə</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_135" n="HIAT:w" s="T37">i</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_138" n="HIAT:w" s="T38">stenatdə</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_141" n="HIAT:w" s="T39">sɨrətʼin</ts>
                  <nts id="Seg_142" n="HIAT:ip">.</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_145" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_147" n="HIAT:w" s="T40">Me</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_150" n="HIAT:w" s="T41">täbɨm</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_153" n="HIAT:w" s="T42">qaːlʼe</ts>
                  <nts id="Seg_154" n="HIAT:ip">,</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_157" n="HIAT:w" s="T43">qaːlʼe</ts>
                  <nts id="Seg_158" n="HIAT:ip">,</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_161" n="HIAT:w" s="T44">nasel</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_164" n="HIAT:w" s="T45">orannawtə</ts>
                  <nts id="Seg_165" n="HIAT:ip">,</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_168" n="HIAT:w" s="T46">toː</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_171" n="HIAT:w" s="T47">tʼekawtə</ts>
                  <nts id="Seg_172" n="HIAT:ip">.</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_175" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_177" n="HIAT:w" s="T48">A</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_180" n="HIAT:w" s="T49">koška</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_183" n="HIAT:w" s="T50">tʼälʼdʼöɣətdə</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_186" n="HIAT:w" s="T51">kɨdan</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_189" n="HIAT:w" s="T52">mannɨppaː</ts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T53" id="Seg_192" n="sc" s="T1">
               <ts e="T2" id="Seg_194" n="e" s="T1">Me </ts>
               <ts e="T3" id="Seg_196" n="e" s="T2">ugon </ts>
               <ts e="T4" id="Seg_198" n="e" s="T3">maːʒukuzawtə </ts>
               <ts e="T5" id="Seg_200" n="e" s="T4">skatʼinam. </ts>
               <ts e="T6" id="Seg_202" n="e" s="T5">Tebnan </ts>
               <ts e="T7" id="Seg_204" n="e" s="T6">iːkuzawtə </ts>
               <ts e="T8" id="Seg_206" n="e" s="T7">puzɨrʼim. </ts>
               <ts e="T9" id="Seg_208" n="e" s="T8">Na </ts>
               <ts e="T10" id="Seg_210" n="e" s="T9">puːɣɨlǯəǯal, </ts>
               <ts e="T11" id="Seg_212" n="e" s="T10">täp </ts>
               <ts e="T12" id="Seg_214" n="e" s="T11">iːbɨkan </ts>
               <ts e="T13" id="Seg_216" n="e" s="T12">aːzukun. </ts>
               <ts e="T14" id="Seg_218" n="e" s="T13">Täbɨn </ts>
               <ts e="T15" id="Seg_220" n="e" s="T14">sunʼnʼät </ts>
               <ts e="T16" id="Seg_222" n="e" s="T15">penkowtə </ts>
               <ts e="T17" id="Seg_224" n="e" s="T16">karoɣɨm </ts>
               <ts e="T18" id="Seg_226" n="e" s="T17">alʼi </ts>
               <ts e="T19" id="Seg_228" n="e" s="T18">püllagagala, </ts>
               <ts e="T20" id="Seg_230" n="e" s="T19">nagur </ts>
               <ts e="T21" id="Seg_232" n="e" s="T20">püllagagala. </ts>
               <ts e="T22" id="Seg_234" n="e" s="T21">Täp </ts>
               <ts e="T23" id="Seg_236" n="e" s="T22">rokšɨpbaː. </ts>
               <ts e="T24" id="Seg_238" n="e" s="T23">Me </ts>
               <ts e="T25" id="Seg_240" n="e" s="T24">kɨskan </ts>
               <ts e="T26" id="Seg_242" n="e" s="T25">talʼdʼötdə </ts>
               <ts e="T27" id="Seg_244" n="e" s="T26">i </ts>
               <ts e="T28" id="Seg_246" n="e" s="T27">saːrawtə. </ts>
               <ts e="T29" id="Seg_248" n="e" s="T28">Kɨska </ts>
               <ts e="T30" id="Seg_250" n="e" s="T29">kak </ts>
               <ts e="T31" id="Seg_252" n="e" s="T30">palʼdʼelʼe </ts>
               <ts e="T32" id="Seg_254" n="e" s="T31">übəratdə. </ts>
               <ts e="T33" id="Seg_256" n="e" s="T32">Istol </ts>
               <ts e="T34" id="Seg_258" n="e" s="T33">baːrot </ts>
               <ts e="T35" id="Seg_260" n="e" s="T34">sɨrətʼen </ts>
               <ts e="T36" id="Seg_262" n="e" s="T35">i </ts>
               <ts e="T37" id="Seg_264" n="e" s="T36">poːrlatdə </ts>
               <ts e="T38" id="Seg_266" n="e" s="T37">i </ts>
               <ts e="T39" id="Seg_268" n="e" s="T38">stenatdə </ts>
               <ts e="T40" id="Seg_270" n="e" s="T39">sɨrətʼin. </ts>
               <ts e="T41" id="Seg_272" n="e" s="T40">Me </ts>
               <ts e="T42" id="Seg_274" n="e" s="T41">täbɨm </ts>
               <ts e="T43" id="Seg_276" n="e" s="T42">qaːlʼe, </ts>
               <ts e="T44" id="Seg_278" n="e" s="T43">qaːlʼe, </ts>
               <ts e="T45" id="Seg_280" n="e" s="T44">nasel </ts>
               <ts e="T46" id="Seg_282" n="e" s="T45">orannawtə, </ts>
               <ts e="T47" id="Seg_284" n="e" s="T46">toː </ts>
               <ts e="T48" id="Seg_286" n="e" s="T47">tʼekawtə. </ts>
               <ts e="T49" id="Seg_288" n="e" s="T48">A </ts>
               <ts e="T50" id="Seg_290" n="e" s="T49">koška </ts>
               <ts e="T51" id="Seg_292" n="e" s="T50">tʼälʼdʼöɣətdə </ts>
               <ts e="T52" id="Seg_294" n="e" s="T51">kɨdan </ts>
               <ts e="T53" id="Seg_296" n="e" s="T52">mannɨppaː. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_297" s="T1">PVD_1964_PlayingWithCat_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_298" s="T5">PVD_1964_PlayingWithCat_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_299" s="T8">PVD_1964_PlayingWithCat_nar.003 (001.003)</ta>
            <ta e="T21" id="Seg_300" s="T13">PVD_1964_PlayingWithCat_nar.004 (001.004)</ta>
            <ta e="T23" id="Seg_301" s="T21">PVD_1964_PlayingWithCat_nar.005 (001.005)</ta>
            <ta e="T28" id="Seg_302" s="T23">PVD_1964_PlayingWithCat_nar.006 (001.006)</ta>
            <ta e="T32" id="Seg_303" s="T28">PVD_1964_PlayingWithCat_nar.007 (001.007)</ta>
            <ta e="T40" id="Seg_304" s="T32">PVD_1964_PlayingWithCat_nar.008 (001.008)</ta>
            <ta e="T48" id="Seg_305" s="T40">PVD_1964_PlayingWithCat_nar.009 (001.009)</ta>
            <ta e="T53" id="Seg_306" s="T48">PVD_1964_PlayingWithCat_nar.010 (001.010)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_307" s="T1">ме у′гон ′ма̄жукузавтъ ска′тʼинам .</ta>
            <ta e="T8" id="Seg_308" s="T5">теб′нан ′ӣку‵завтъ пу′зырʼим.</ta>
            <ta e="T13" id="Seg_309" s="T8">на ′пӯɣылджъджал, тӓп ′ӣбык(г)ан а̄зу‵кун.</ta>
            <ta e="T21" id="Seg_310" s="T13">′тӓбын су′нʼнʼӓт пен′коwтъ ка′роɣым алʼи ′пӱлlагагала, ′нагур пӱлlагагала.</ta>
            <ta e="T23" id="Seg_311" s="T21">′тӓп ′рокшып‵ба̄.</ta>
            <ta e="T28" id="Seg_312" s="T23">ме ′кыскан талʼ′дʼӧтдъ и ′са̄раwтъ.</ta>
            <ta e="T32" id="Seg_313" s="T28">′кыска как ′палʼдʼелʼе ′ӱбъратдъ.</ta>
            <ta e="T40" id="Seg_314" s="T32">и′стол′б̂а̄рот ′сырътʼен и ′по̄рлатдъ и ′стенатдъ ′сырътʼин.</ta>
            <ta e="T48" id="Seg_315" s="T40">ме тӓ′бым kа̄лʼе, kа̄лʼе, на′сел о′раннаwтъ, ′то̄тʼекавтъ.</ta>
            <ta e="T53" id="Seg_316" s="T48">а ′кошка тʼӓлʼдʼӧɣътдъ ′кыдан ′манныппа̄.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_317" s="T1">me ugon maːʒukuzawtə skatʼinam .</ta>
            <ta e="T8" id="Seg_318" s="T5">tebnan iːkuzawtə puzɨrʼim.</ta>
            <ta e="T13" id="Seg_319" s="T8">na puːɣɨlǯəǯal, täp iːbɨk(g)an aːzukun.</ta>
            <ta e="T21" id="Seg_320" s="T13">täbɨn sunʼnʼät penkowtə karoɣɨm alʼi püllagagala, nagur püllagagala.</ta>
            <ta e="T23" id="Seg_321" s="T21">täp rokšɨpbaː.</ta>
            <ta e="T28" id="Seg_322" s="T23">me kɨskan talʼdʼötdə i saːrawtə.</ta>
            <ta e="T32" id="Seg_323" s="T28">kɨska kak palʼdʼelʼe übəratdə.</ta>
            <ta e="T40" id="Seg_324" s="T32">istolb̂aːrot sɨrətʼen i poːrlatdə i stenatdə sɨrətʼin.</ta>
            <ta e="T48" id="Seg_325" s="T40">me täbɨm qaːlʼe, qaːlʼe, nasel orannawtə, toːtʼekawtə.</ta>
            <ta e="T53" id="Seg_326" s="T48">a koška tʼälʼdʼöɣətdə kɨdan mannɨppaː.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_327" s="T1">Me ugon maːʒukuzawtə skatʼinam. </ta>
            <ta e="T8" id="Seg_328" s="T5">Tebnan iːkuzawtə puzɨrʼim. </ta>
            <ta e="T13" id="Seg_329" s="T8">Na puːɣɨlǯəǯal, täp iːbɨkan aːzukun. </ta>
            <ta e="T21" id="Seg_330" s="T13">Täbɨn sunʼnʼät penkowtə karoɣɨm alʼi püllagagala, nagur püllagagala. </ta>
            <ta e="T23" id="Seg_331" s="T21">Täp rokšɨpbaː. </ta>
            <ta e="T28" id="Seg_332" s="T23">Me kɨskan talʼdʼötdə i saːrawtə. </ta>
            <ta e="T32" id="Seg_333" s="T28">Kɨska kak palʼdʼelʼe übəratdə. </ta>
            <ta e="T40" id="Seg_334" s="T32">Istol baːrot sɨrətʼen i poːrlatdə i stenatdə sɨrətʼin. </ta>
            <ta e="T48" id="Seg_335" s="T40">Me täbɨm qaːlʼe, qaːlʼe, nasel orannawtə, toː tʼekawtə. </ta>
            <ta e="T53" id="Seg_336" s="T48">A koška tʼälʼdʼöɣətdə kɨdan mannɨppaː. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_337" s="T1">me</ta>
            <ta e="T3" id="Seg_338" s="T2">ugon</ta>
            <ta e="T4" id="Seg_339" s="T3">maːʒu-ku-za-wtə</ta>
            <ta e="T5" id="Seg_340" s="T4">skatʼina-m</ta>
            <ta e="T6" id="Seg_341" s="T5">teb-nan</ta>
            <ta e="T7" id="Seg_342" s="T6">iː-ku-za-wtə</ta>
            <ta e="T8" id="Seg_343" s="T7">puzɨrʼ-i-m</ta>
            <ta e="T9" id="Seg_344" s="T8">na</ta>
            <ta e="T10" id="Seg_345" s="T9">puː-ɣɨl-ǯ-əǯa-l</ta>
            <ta e="T11" id="Seg_346" s="T10">täp</ta>
            <ta e="T12" id="Seg_347" s="T11">iːbɨka-n</ta>
            <ta e="T13" id="Seg_348" s="T12">aːzu-ku-n</ta>
            <ta e="T14" id="Seg_349" s="T13">täb-ɨ-n</ta>
            <ta e="T15" id="Seg_350" s="T14">sunʼnʼä-t</ta>
            <ta e="T16" id="Seg_351" s="T15">pen-ko-wtə</ta>
            <ta e="T17" id="Seg_352" s="T16">karoɣ-ɨ-m</ta>
            <ta e="T18" id="Seg_353" s="T17">alʼi</ta>
            <ta e="T19" id="Seg_354" s="T18">pü-llaga-ga-la</ta>
            <ta e="T20" id="Seg_355" s="T19">nagur</ta>
            <ta e="T21" id="Seg_356" s="T20">pü-llaga-ga-la</ta>
            <ta e="T22" id="Seg_357" s="T21">täp</ta>
            <ta e="T23" id="Seg_358" s="T22">rokšɨpbaː</ta>
            <ta e="T24" id="Seg_359" s="T23">me</ta>
            <ta e="T25" id="Seg_360" s="T24">kɨska-n</ta>
            <ta e="T26" id="Seg_361" s="T25">talʼdʼ-ö-tdə</ta>
            <ta e="T27" id="Seg_362" s="T26">i</ta>
            <ta e="T28" id="Seg_363" s="T27">saːra-wtə</ta>
            <ta e="T29" id="Seg_364" s="T28">kɨska</ta>
            <ta e="T30" id="Seg_365" s="T29">kak</ta>
            <ta e="T31" id="Seg_366" s="T30">palʼdʼe-lʼe</ta>
            <ta e="T32" id="Seg_367" s="T31">übə-r-a-tdə</ta>
            <ta e="T33" id="Seg_368" s="T32">istol</ta>
            <ta e="T34" id="Seg_369" s="T33">baːr-o-t</ta>
            <ta e="T35" id="Seg_370" s="T34">sɨrətʼe-n</ta>
            <ta e="T36" id="Seg_371" s="T35">i</ta>
            <ta e="T37" id="Seg_372" s="T36">poːr-la-tdə</ta>
            <ta e="T38" id="Seg_373" s="T37">i</ta>
            <ta e="T39" id="Seg_374" s="T38">stena-tdə</ta>
            <ta e="T40" id="Seg_375" s="T39">sɨrətʼi-n</ta>
            <ta e="T41" id="Seg_376" s="T40">me</ta>
            <ta e="T42" id="Seg_377" s="T41">täb-ɨ-m</ta>
            <ta e="T43" id="Seg_378" s="T42">qaː-lʼe</ta>
            <ta e="T44" id="Seg_379" s="T43">qaː-lʼe</ta>
            <ta e="T45" id="Seg_380" s="T44">nasel</ta>
            <ta e="T46" id="Seg_381" s="T45">oran-na-wtə</ta>
            <ta e="T47" id="Seg_382" s="T46">toː</ta>
            <ta e="T48" id="Seg_383" s="T47">tʼeka-wtə</ta>
            <ta e="T49" id="Seg_384" s="T48">a</ta>
            <ta e="T50" id="Seg_385" s="T49">koška</ta>
            <ta e="T51" id="Seg_386" s="T50">tʼälʼdʼ-ö-ɣətdə</ta>
            <ta e="T52" id="Seg_387" s="T51">kɨdan</ta>
            <ta e="T53" id="Seg_388" s="T52">mannɨ-ppaː</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_389" s="T1">me</ta>
            <ta e="T3" id="Seg_390" s="T2">ugon</ta>
            <ta e="T4" id="Seg_391" s="T3">maǯə-ku-sɨ-un</ta>
            <ta e="T5" id="Seg_392" s="T4">skatʼina-m</ta>
            <ta e="T6" id="Seg_393" s="T5">täp-nan</ta>
            <ta e="T7" id="Seg_394" s="T6">iː-ku-sɨ-un</ta>
            <ta e="T8" id="Seg_395" s="T7">puzɨrʼ-ɨ-m</ta>
            <ta e="T9" id="Seg_396" s="T8">na</ta>
            <ta e="T10" id="Seg_397" s="T9">puː-qɨl-ǯə-enǯɨ-l</ta>
            <ta e="T11" id="Seg_398" s="T10">täp</ta>
            <ta e="T12" id="Seg_399" s="T11">iːbəgaj-ŋ</ta>
            <ta e="T13" id="Seg_400" s="T12">azu-ku-n</ta>
            <ta e="T14" id="Seg_401" s="T13">täp-ɨ-n</ta>
            <ta e="T15" id="Seg_402" s="T14">sʼütdʼe-ntə</ta>
            <ta e="T16" id="Seg_403" s="T15">pen-ku-un</ta>
            <ta e="T17" id="Seg_404" s="T16">karoɣ-ɨ-m</ta>
            <ta e="T18" id="Seg_405" s="T17">alʼi</ta>
            <ta e="T19" id="Seg_406" s="T18">püː-laka-ka-la</ta>
            <ta e="T20" id="Seg_407" s="T19">nagur</ta>
            <ta e="T21" id="Seg_408" s="T20">püː-laka-ka-la</ta>
            <ta e="T22" id="Seg_409" s="T21">täp</ta>
            <ta e="T23" id="Seg_410" s="T22">rokšɨpbu</ta>
            <ta e="T24" id="Seg_411" s="T23">me</ta>
            <ta e="T25" id="Seg_412" s="T24">kɨska-n</ta>
            <ta e="T26" id="Seg_413" s="T25">talʼdʼ-ɨ-ntə</ta>
            <ta e="T27" id="Seg_414" s="T26">i</ta>
            <ta e="T28" id="Seg_415" s="T27">saːrə-un</ta>
            <ta e="T29" id="Seg_416" s="T28">kɨska</ta>
            <ta e="T30" id="Seg_417" s="T29">kak</ta>
            <ta e="T31" id="Seg_418" s="T30">palʼdʼi-le</ta>
            <ta e="T32" id="Seg_419" s="T31">übɨ-r-nɨ-ntɨ</ta>
            <ta e="T33" id="Seg_420" s="T32">istol</ta>
            <ta e="T34" id="Seg_421" s="T33">par-ɨ-ntə</ta>
            <ta e="T35" id="Seg_422" s="T34">sɨrətʼe-n</ta>
            <ta e="T36" id="Seg_423" s="T35">i</ta>
            <ta e="T37" id="Seg_424" s="T36">por-la-ntə</ta>
            <ta e="T38" id="Seg_425" s="T37">i</ta>
            <ta e="T39" id="Seg_426" s="T38">stʼena-ntə</ta>
            <ta e="T40" id="Seg_427" s="T39">sɨrətʼe-n</ta>
            <ta e="T41" id="Seg_428" s="T40">me</ta>
            <ta e="T42" id="Seg_429" s="T41">täp-ɨ-m</ta>
            <ta e="T43" id="Seg_430" s="T42">qa-le</ta>
            <ta e="T44" id="Seg_431" s="T43">qa-le</ta>
            <ta e="T45" id="Seg_432" s="T44">naseːl</ta>
            <ta e="T46" id="Seg_433" s="T45">oral-nɨ-un</ta>
            <ta e="T47" id="Seg_434" s="T46">to</ta>
            <ta e="T48" id="Seg_435" s="T47">tʼekkə-un</ta>
            <ta e="T49" id="Seg_436" s="T48">a</ta>
            <ta e="T50" id="Seg_437" s="T49">koška</ta>
            <ta e="T51" id="Seg_438" s="T50">talʼdʼ-ɨ-qɨntɨ</ta>
            <ta e="T52" id="Seg_439" s="T51">qɨdan</ta>
            <ta e="T53" id="Seg_440" s="T52">*mantɨ-mbɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_441" s="T1">we.[NOM]</ta>
            <ta e="T3" id="Seg_442" s="T2">earlier</ta>
            <ta e="T4" id="Seg_443" s="T3">cut-HAB-PST-1PL</ta>
            <ta e="T5" id="Seg_444" s="T4">cattle-ACC</ta>
            <ta e="T6" id="Seg_445" s="T5">(s)he-ADES</ta>
            <ta e="T7" id="Seg_446" s="T6">take-HAB-PST-1PL</ta>
            <ta e="T8" id="Seg_447" s="T7">bladder-EP-ACC</ta>
            <ta e="T9" id="Seg_448" s="T8">this.[NOM]</ta>
            <ta e="T10" id="Seg_449" s="T9">blow-DRV-DRV-FUT-2SG.O</ta>
            <ta e="T11" id="Seg_450" s="T10">(s)he.[NOM]</ta>
            <ta e="T12" id="Seg_451" s="T11">big-ADVZ</ta>
            <ta e="T13" id="Seg_452" s="T12">become-HAB-3SG.S</ta>
            <ta e="T14" id="Seg_453" s="T13">(s)he-EP-GEN</ta>
            <ta e="T15" id="Seg_454" s="T14">inside-ILL</ta>
            <ta e="T16" id="Seg_455" s="T15">put-HAB-1PL</ta>
            <ta e="T17" id="Seg_456" s="T16">peas-EP-ACC</ta>
            <ta e="T18" id="Seg_457" s="T17">or</ta>
            <ta e="T19" id="Seg_458" s="T18">stone-SNGL-DIM-PL.[NOM]</ta>
            <ta e="T20" id="Seg_459" s="T19">three</ta>
            <ta e="T21" id="Seg_460" s="T20">stone-SNGL-DIM-PL.[NOM]</ta>
            <ta e="T22" id="Seg_461" s="T21">(s)he.[NOM]</ta>
            <ta e="T23" id="Seg_462" s="T22">rattle.[3SG]</ta>
            <ta e="T24" id="Seg_463" s="T23">we.[NOM]</ta>
            <ta e="T25" id="Seg_464" s="T24">cat-GEN</ta>
            <ta e="T26" id="Seg_465" s="T25">tail-EP-ILL</ta>
            <ta e="T27" id="Seg_466" s="T26">and</ta>
            <ta e="T28" id="Seg_467" s="T27">bind-1PL</ta>
            <ta e="T29" id="Seg_468" s="T28">cat.[NOM]</ta>
            <ta e="T30" id="Seg_469" s="T29">suddenly</ta>
            <ta e="T31" id="Seg_470" s="T30">walk-CVB</ta>
            <ta e="T32" id="Seg_471" s="T31">begin-DRV-CO-INFER.[3SG.S]</ta>
            <ta e="T33" id="Seg_472" s="T32">table.[NOM]</ta>
            <ta e="T34" id="Seg_473" s="T33">top-EP-ILL</ta>
            <ta e="T35" id="Seg_474" s="T34">climb-3SG.S</ta>
            <ta e="T36" id="Seg_475" s="T35">and</ta>
            <ta e="T37" id="Seg_476" s="T36">cupboard-PL-ILL</ta>
            <ta e="T38" id="Seg_477" s="T37">and</ta>
            <ta e="T39" id="Seg_478" s="T38">wall-ILL</ta>
            <ta e="T40" id="Seg_479" s="T39">climb-3SG.S</ta>
            <ta e="T41" id="Seg_480" s="T40">we.[NOM]</ta>
            <ta e="T42" id="Seg_481" s="T41">(s)he-EP-ACC</ta>
            <ta e="T43" id="Seg_482" s="T42">pursue-CVB</ta>
            <ta e="T44" id="Seg_483" s="T43">pursue-CVB</ta>
            <ta e="T45" id="Seg_484" s="T44">hardly</ta>
            <ta e="T46" id="Seg_485" s="T45">catch-CO-1PL</ta>
            <ta e="T47" id="Seg_486" s="T46">away</ta>
            <ta e="T48" id="Seg_487" s="T47">untie-1PL</ta>
            <ta e="T49" id="Seg_488" s="T48">and</ta>
            <ta e="T50" id="Seg_489" s="T49">cat.[NOM]</ta>
            <ta e="T51" id="Seg_490" s="T50">tail-EP-ILL.3SG</ta>
            <ta e="T52" id="Seg_491" s="T51">all.the.time</ta>
            <ta e="T53" id="Seg_492" s="T52">look-DUR.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_493" s="T1">мы.[NOM]</ta>
            <ta e="T3" id="Seg_494" s="T2">раньше</ta>
            <ta e="T4" id="Seg_495" s="T3">отрезать-HAB-PST-1PL</ta>
            <ta e="T5" id="Seg_496" s="T4">скотина-ACC</ta>
            <ta e="T6" id="Seg_497" s="T5">он(а)-ADES</ta>
            <ta e="T7" id="Seg_498" s="T6">взять-HAB-PST-1PL</ta>
            <ta e="T8" id="Seg_499" s="T7">пузырь-EP-ACC</ta>
            <ta e="T9" id="Seg_500" s="T8">этот.[NOM]</ta>
            <ta e="T10" id="Seg_501" s="T9">дуть-DRV-DRV-FUT-2SG.O</ta>
            <ta e="T11" id="Seg_502" s="T10">он(а).[NOM]</ta>
            <ta e="T12" id="Seg_503" s="T11">большой-ADVZ</ta>
            <ta e="T13" id="Seg_504" s="T12">стать-HAB-3SG.S</ta>
            <ta e="T14" id="Seg_505" s="T13">он(а)-EP-GEN</ta>
            <ta e="T15" id="Seg_506" s="T14">нутро-ILL</ta>
            <ta e="T16" id="Seg_507" s="T15">положить-HAB-1PL</ta>
            <ta e="T17" id="Seg_508" s="T16">горох-EP-ACC</ta>
            <ta e="T18" id="Seg_509" s="T17">али</ta>
            <ta e="T19" id="Seg_510" s="T18">камень-SNGL-DIM-PL.[NOM]</ta>
            <ta e="T20" id="Seg_511" s="T19">три</ta>
            <ta e="T21" id="Seg_512" s="T20">камень-SNGL-DIM-PL.[NOM]</ta>
            <ta e="T22" id="Seg_513" s="T21">он(а).[NOM]</ta>
            <ta e="T23" id="Seg_514" s="T22">прогреметь.[3SG]</ta>
            <ta e="T24" id="Seg_515" s="T23">мы.[NOM]</ta>
            <ta e="T25" id="Seg_516" s="T24">киска-GEN</ta>
            <ta e="T26" id="Seg_517" s="T25">хвост-EP-ILL</ta>
            <ta e="T27" id="Seg_518" s="T26">и</ta>
            <ta e="T28" id="Seg_519" s="T27">привязать-1PL</ta>
            <ta e="T29" id="Seg_520" s="T28">киска.[NOM]</ta>
            <ta e="T30" id="Seg_521" s="T29">как</ta>
            <ta e="T31" id="Seg_522" s="T30">ходить-CVB</ta>
            <ta e="T32" id="Seg_523" s="T31">начать-DRV-CO-INFER.[3SG.S]</ta>
            <ta e="T33" id="Seg_524" s="T32">стол.[NOM]</ta>
            <ta e="T34" id="Seg_525" s="T33">верхняя.часть-EP-ILL</ta>
            <ta e="T35" id="Seg_526" s="T34">влезть-3SG.S</ta>
            <ta e="T36" id="Seg_527" s="T35">и</ta>
            <ta e="T37" id="Seg_528" s="T36">шкаф-PL-ILL</ta>
            <ta e="T38" id="Seg_529" s="T37">и</ta>
            <ta e="T39" id="Seg_530" s="T38">стена-ILL</ta>
            <ta e="T40" id="Seg_531" s="T39">влезть-3SG.S</ta>
            <ta e="T41" id="Seg_532" s="T40">мы.[NOM]</ta>
            <ta e="T42" id="Seg_533" s="T41">он(а)-EP-ACC</ta>
            <ta e="T43" id="Seg_534" s="T42">преследовать-CVB</ta>
            <ta e="T44" id="Seg_535" s="T43">преследовать-CVB</ta>
            <ta e="T45" id="Seg_536" s="T44">насилу</ta>
            <ta e="T46" id="Seg_537" s="T45">поймать-CO-1PL</ta>
            <ta e="T47" id="Seg_538" s="T46">прочь</ta>
            <ta e="T48" id="Seg_539" s="T47">отвязать-1PL</ta>
            <ta e="T49" id="Seg_540" s="T48">а</ta>
            <ta e="T50" id="Seg_541" s="T49">кошка.[NOM]</ta>
            <ta e="T51" id="Seg_542" s="T50">хвост-EP-ILL.3SG</ta>
            <ta e="T52" id="Seg_543" s="T51">все.время</ta>
            <ta e="T53" id="Seg_544" s="T52">посмотреть-DUR.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_545" s="T1">pers.[n:case]</ta>
            <ta e="T3" id="Seg_546" s="T2">adv</ta>
            <ta e="T4" id="Seg_547" s="T3">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_548" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_549" s="T5">pers-n:case</ta>
            <ta e="T7" id="Seg_550" s="T6">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_551" s="T7">n-n:ins-n:case</ta>
            <ta e="T9" id="Seg_552" s="T8">dem.[n:case]</ta>
            <ta e="T10" id="Seg_553" s="T9">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_554" s="T10">pers.[n:case]</ta>
            <ta e="T12" id="Seg_555" s="T11">adj-adj&gt;adv</ta>
            <ta e="T13" id="Seg_556" s="T12">v-v&gt;v-v:pn</ta>
            <ta e="T14" id="Seg_557" s="T13">pers-n:ins-n:case</ta>
            <ta e="T15" id="Seg_558" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_559" s="T15">v-v&gt;v-v:pn</ta>
            <ta e="T17" id="Seg_560" s="T16">n-n:ins-n:case</ta>
            <ta e="T18" id="Seg_561" s="T17">conj</ta>
            <ta e="T19" id="Seg_562" s="T18">n-n&gt;n-n&gt;n-n:num.[n:case]</ta>
            <ta e="T20" id="Seg_563" s="T19">num</ta>
            <ta e="T21" id="Seg_564" s="T20">n-n&gt;n-n&gt;n-n:num.[n:case]</ta>
            <ta e="T22" id="Seg_565" s="T21">pers.[n:case]</ta>
            <ta e="T23" id="Seg_566" s="T22">v.[v:pn]</ta>
            <ta e="T24" id="Seg_567" s="T23">pers.[n:case]</ta>
            <ta e="T25" id="Seg_568" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_569" s="T25">n-n:ins-n:case</ta>
            <ta e="T27" id="Seg_570" s="T26">conj</ta>
            <ta e="T28" id="Seg_571" s="T27">v-v:pn</ta>
            <ta e="T29" id="Seg_572" s="T28">n.[n:case]</ta>
            <ta e="T30" id="Seg_573" s="T29">adv</ta>
            <ta e="T31" id="Seg_574" s="T30">v-v&gt;adv</ta>
            <ta e="T32" id="Seg_575" s="T31">v-v&gt;v-v:ins-v:mood.[v:pn]</ta>
            <ta e="T33" id="Seg_576" s="T32">n.[n:case]</ta>
            <ta e="T34" id="Seg_577" s="T33">n-n:ins-n:case</ta>
            <ta e="T35" id="Seg_578" s="T34">v-v:pn</ta>
            <ta e="T36" id="Seg_579" s="T35">conj</ta>
            <ta e="T37" id="Seg_580" s="T36">n-n:num-n:case</ta>
            <ta e="T38" id="Seg_581" s="T37">conj</ta>
            <ta e="T39" id="Seg_582" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_583" s="T39">v-v:pn</ta>
            <ta e="T41" id="Seg_584" s="T40">pers.[n:case]</ta>
            <ta e="T42" id="Seg_585" s="T41">pers-n:ins-n:case</ta>
            <ta e="T43" id="Seg_586" s="T42">v-v&gt;adv</ta>
            <ta e="T44" id="Seg_587" s="T43">v-v&gt;adv</ta>
            <ta e="T45" id="Seg_588" s="T44">adv</ta>
            <ta e="T46" id="Seg_589" s="T45">v-v:ins-v:pn</ta>
            <ta e="T47" id="Seg_590" s="T46">preverb</ta>
            <ta e="T48" id="Seg_591" s="T47">v-v:pn</ta>
            <ta e="T49" id="Seg_592" s="T48">conj</ta>
            <ta e="T50" id="Seg_593" s="T49">n.[n:case]</ta>
            <ta e="T51" id="Seg_594" s="T50">n-n:ins-n:case.poss</ta>
            <ta e="T52" id="Seg_595" s="T51">adv</ta>
            <ta e="T53" id="Seg_596" s="T52">v-v&gt;v.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_597" s="T1">pers</ta>
            <ta e="T3" id="Seg_598" s="T2">adv</ta>
            <ta e="T4" id="Seg_599" s="T3">v</ta>
            <ta e="T5" id="Seg_600" s="T4">n</ta>
            <ta e="T6" id="Seg_601" s="T5">pers</ta>
            <ta e="T7" id="Seg_602" s="T6">v</ta>
            <ta e="T8" id="Seg_603" s="T7">n</ta>
            <ta e="T9" id="Seg_604" s="T8">dem</ta>
            <ta e="T10" id="Seg_605" s="T9">v</ta>
            <ta e="T11" id="Seg_606" s="T10">pers</ta>
            <ta e="T12" id="Seg_607" s="T11">adj</ta>
            <ta e="T13" id="Seg_608" s="T12">v</ta>
            <ta e="T14" id="Seg_609" s="T13">pers</ta>
            <ta e="T15" id="Seg_610" s="T14">n</ta>
            <ta e="T16" id="Seg_611" s="T15">v</ta>
            <ta e="T17" id="Seg_612" s="T16">n</ta>
            <ta e="T18" id="Seg_613" s="T17">conj</ta>
            <ta e="T19" id="Seg_614" s="T18">n</ta>
            <ta e="T20" id="Seg_615" s="T19">num</ta>
            <ta e="T21" id="Seg_616" s="T20">n</ta>
            <ta e="T22" id="Seg_617" s="T21">pers</ta>
            <ta e="T23" id="Seg_618" s="T22">v</ta>
            <ta e="T24" id="Seg_619" s="T23">pers</ta>
            <ta e="T25" id="Seg_620" s="T24">n</ta>
            <ta e="T26" id="Seg_621" s="T25">n</ta>
            <ta e="T27" id="Seg_622" s="T26">conj</ta>
            <ta e="T28" id="Seg_623" s="T27">v</ta>
            <ta e="T29" id="Seg_624" s="T28">n</ta>
            <ta e="T30" id="Seg_625" s="T29">adv</ta>
            <ta e="T31" id="Seg_626" s="T30">adv</ta>
            <ta e="T32" id="Seg_627" s="T31">v</ta>
            <ta e="T33" id="Seg_628" s="T32">n</ta>
            <ta e="T34" id="Seg_629" s="T33">n</ta>
            <ta e="T35" id="Seg_630" s="T34">v</ta>
            <ta e="T36" id="Seg_631" s="T35">conj</ta>
            <ta e="T37" id="Seg_632" s="T36">v</ta>
            <ta e="T38" id="Seg_633" s="T37">conj</ta>
            <ta e="T39" id="Seg_634" s="T38">n</ta>
            <ta e="T40" id="Seg_635" s="T39">v</ta>
            <ta e="T41" id="Seg_636" s="T40">pers</ta>
            <ta e="T42" id="Seg_637" s="T41">pers</ta>
            <ta e="T43" id="Seg_638" s="T42">adj</ta>
            <ta e="T44" id="Seg_639" s="T43">adj</ta>
            <ta e="T45" id="Seg_640" s="T44">adv</ta>
            <ta e="T46" id="Seg_641" s="T45">v</ta>
            <ta e="T47" id="Seg_642" s="T46">preverb</ta>
            <ta e="T48" id="Seg_643" s="T47">v</ta>
            <ta e="T49" id="Seg_644" s="T48">conj</ta>
            <ta e="T50" id="Seg_645" s="T49">n</ta>
            <ta e="T51" id="Seg_646" s="T50">n</ta>
            <ta e="T52" id="Seg_647" s="T51">adv</ta>
            <ta e="T53" id="Seg_648" s="T52">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_649" s="T1">pro.h:A</ta>
            <ta e="T3" id="Seg_650" s="T2">adv:Time</ta>
            <ta e="T5" id="Seg_651" s="T4">np:P</ta>
            <ta e="T6" id="Seg_652" s="T5">pro:Poss</ta>
            <ta e="T7" id="Seg_653" s="T6">0.1.h:A</ta>
            <ta e="T8" id="Seg_654" s="T7">np:Th</ta>
            <ta e="T10" id="Seg_655" s="T9">0.2.h:A 0.3:P</ta>
            <ta e="T11" id="Seg_656" s="T10">pro:Th</ta>
            <ta e="T14" id="Seg_657" s="T13">pro:Poss</ta>
            <ta e="T15" id="Seg_658" s="T14">np:G</ta>
            <ta e="T16" id="Seg_659" s="T15">0.1.h:A</ta>
            <ta e="T17" id="Seg_660" s="T16">np:Th</ta>
            <ta e="T19" id="Seg_661" s="T18">np:Th</ta>
            <ta e="T21" id="Seg_662" s="T20">np:Th</ta>
            <ta e="T22" id="Seg_663" s="T21">pro:Th</ta>
            <ta e="T24" id="Seg_664" s="T23">pro.h:A</ta>
            <ta e="T25" id="Seg_665" s="T24">np:Poss</ta>
            <ta e="T26" id="Seg_666" s="T25">np:G</ta>
            <ta e="T28" id="Seg_667" s="T27">0.3:Th</ta>
            <ta e="T29" id="Seg_668" s="T28">np:A</ta>
            <ta e="T34" id="Seg_669" s="T33">np:G</ta>
            <ta e="T35" id="Seg_670" s="T34">0.3:A</ta>
            <ta e="T37" id="Seg_671" s="T36">np:G</ta>
            <ta e="T39" id="Seg_672" s="T38">np:G</ta>
            <ta e="T40" id="Seg_673" s="T39">0.3:A</ta>
            <ta e="T41" id="Seg_674" s="T40">pro.h:A</ta>
            <ta e="T42" id="Seg_675" s="T41">pro:P</ta>
            <ta e="T48" id="Seg_676" s="T47">0.1.h:A 0.3:Th</ta>
            <ta e="T50" id="Seg_677" s="T49">np:A</ta>
            <ta e="T51" id="Seg_678" s="T50">np:G</ta>
            <ta e="T52" id="Seg_679" s="T51">adv:Time</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_680" s="T1">pro.h:S</ta>
            <ta e="T4" id="Seg_681" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_682" s="T4">np:O</ta>
            <ta e="T7" id="Seg_683" s="T6">0.1.h:S v:pred</ta>
            <ta e="T8" id="Seg_684" s="T7">np:O</ta>
            <ta e="T10" id="Seg_685" s="T9">0.2.h:S v:pred 0.3:O</ta>
            <ta e="T11" id="Seg_686" s="T10">pro:S</ta>
            <ta e="T12" id="Seg_687" s="T11">adj:pred</ta>
            <ta e="T13" id="Seg_688" s="T12">v:pred</ta>
            <ta e="T16" id="Seg_689" s="T15">0.1.h:S v:pred</ta>
            <ta e="T17" id="Seg_690" s="T16">np:O</ta>
            <ta e="T19" id="Seg_691" s="T18">np:O</ta>
            <ta e="T21" id="Seg_692" s="T20">np:O</ta>
            <ta e="T22" id="Seg_693" s="T21">pro:S</ta>
            <ta e="T23" id="Seg_694" s="T22">v:pred</ta>
            <ta e="T24" id="Seg_695" s="T23">pro.h:S</ta>
            <ta e="T28" id="Seg_696" s="T27">v:pred 0.3:O</ta>
            <ta e="T29" id="Seg_697" s="T28">np:S</ta>
            <ta e="T32" id="Seg_698" s="T31">v:pred</ta>
            <ta e="T35" id="Seg_699" s="T34">0.3:S v:pred</ta>
            <ta e="T40" id="Seg_700" s="T39">0.3:S v:pred</ta>
            <ta e="T41" id="Seg_701" s="T40">pro.h:S</ta>
            <ta e="T42" id="Seg_702" s="T41">pro:O</ta>
            <ta e="T43" id="Seg_703" s="T42">s:temp</ta>
            <ta e="T44" id="Seg_704" s="T43">s:temp</ta>
            <ta e="T46" id="Seg_705" s="T45">v:pred</ta>
            <ta e="T48" id="Seg_706" s="T47">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T50" id="Seg_707" s="T49">np:S</ta>
            <ta e="T53" id="Seg_708" s="T52">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_709" s="T4">RUS:cult</ta>
            <ta e="T8" id="Seg_710" s="T7">RUS:core</ta>
            <ta e="T17" id="Seg_711" s="T16">RUS:cult</ta>
            <ta e="T18" id="Seg_712" s="T17">RUS:gram</ta>
            <ta e="T25" id="Seg_713" s="T24">RUS:cult</ta>
            <ta e="T27" id="Seg_714" s="T26">RUS:gram</ta>
            <ta e="T29" id="Seg_715" s="T28">RUS:cult</ta>
            <ta e="T30" id="Seg_716" s="T29">RUS:disc</ta>
            <ta e="T33" id="Seg_717" s="T32">RUS:cult</ta>
            <ta e="T34" id="Seg_718" s="T33">WNB Noun or pp</ta>
            <ta e="T36" id="Seg_719" s="T35">RUS:gram</ta>
            <ta e="T38" id="Seg_720" s="T37">RUS:gram</ta>
            <ta e="T39" id="Seg_721" s="T38">RUS:cult</ta>
            <ta e="T45" id="Seg_722" s="T44">RUS:core</ta>
            <ta e="T49" id="Seg_723" s="T48">RUS:gram</ta>
            <ta e="T50" id="Seg_724" s="T49">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_725" s="T1">Earlier we used to slaughter cattle.</ta>
            <ta e="T8" id="Seg_726" s="T5">We took their bladders.</ta>
            <ta e="T13" id="Seg_727" s="T8">If one blows it, it becomes big.</ta>
            <ta e="T21" id="Seg_728" s="T13">We put inside peas or stones, three stones.</ta>
            <ta e="T23" id="Seg_729" s="T21">It rattles.</ta>
            <ta e="T28" id="Seg_730" s="T23">We tied it to the tail of a cat.</ta>
            <ta e="T32" id="Seg_731" s="T28">The cat began running.</ta>
            <ta e="T40" id="Seg_732" s="T32">It climbed onto the table, onto the cupboard and onto the wall.</ta>
            <ta e="T48" id="Seg_733" s="T40">We wew pursuing it until we managed to catch it and to untie [the bladder].</ta>
            <ta e="T53" id="Seg_734" s="T48">And the cat was still looking onto its tail.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_735" s="T1">Früher haben wir Vieh geschlachtet.</ta>
            <ta e="T8" id="Seg_736" s="T5">Wir nahmen ihre Blasen.</ta>
            <ta e="T13" id="Seg_737" s="T8">Wenn man eine aufbläst, wird sie groß.</ta>
            <ta e="T21" id="Seg_738" s="T13">Wir taten Erbsen oder Steine hinein, drei Steine.</ta>
            <ta e="T23" id="Seg_739" s="T21">Sie rasselt.</ta>
            <ta e="T28" id="Seg_740" s="T23">Wir haben sie einer Katze an den Schwanz gebunden.</ta>
            <ta e="T32" id="Seg_741" s="T28">Die Katze begann zu laufen.</ta>
            <ta e="T40" id="Seg_742" s="T32">Sie kletterte auf den Tisch, auf den Schrank und auf die Wand.</ta>
            <ta e="T48" id="Seg_743" s="T40">Wir verfolgten sie, bis wir sie einfingen und [die Blase] losbanden.</ta>
            <ta e="T53" id="Seg_744" s="T48">Und die Katze sah immer wieder auf ihren Schwanz.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_745" s="T1">Мы раньше резали скот.</ta>
            <ta e="T8" id="Seg_746" s="T5">У них брали пузыри.</ta>
            <ta e="T13" id="Seg_747" s="T8">Надуешь его, он большой становится.</ta>
            <ta e="T21" id="Seg_748" s="T13">Внутрь него кладем горох или камешки, три камешка.</ta>
            <ta e="T23" id="Seg_749" s="T21">Он гремит.</ta>
            <ta e="T28" id="Seg_750" s="T23">Мы киске на хвост и привязали.</ta>
            <ta e="T32" id="Seg_751" s="T28">Киска как начала бегать.</ta>
            <ta e="T40" id="Seg_752" s="T32">На стол лезет, и на шкаф, и на стенку лазает.</ta>
            <ta e="T48" id="Seg_753" s="T40">Мы ее гоняли, гоняли, насилу поймали, отвязали.</ta>
            <ta e="T53" id="Seg_754" s="T48">А кошка на хвост все глядит.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_755" s="T1">мы раньше кололи (резали) скот</ta>
            <ta e="T8" id="Seg_756" s="T5">у них брали пузыри</ta>
            <ta e="T13" id="Seg_757" s="T8">надуем этот пузырь он большой станет</ta>
            <ta e="T21" id="Seg_758" s="T13">ему в нутро кладем горох или камешки три камешка</ta>
            <ta e="T23" id="Seg_759" s="T21">он и гремит</ta>
            <ta e="T28" id="Seg_760" s="T23">мы кыске на хвост и привязали</ta>
            <ta e="T32" id="Seg_761" s="T28">кыска как начала бегать</ta>
            <ta e="T40" id="Seg_762" s="T32">на стол лазает (лезет) на шкаф на стенку лазает</ta>
            <ta e="T48" id="Seg_763" s="T40">мы ее гоняли гоняли кое-как поймали прочь развязали</ta>
            <ta e="T53" id="Seg_764" s="T48">а кошка на хвост все (поглядывала) глядит</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T13" id="Seg_765" s="T8">[KuAI:] Variant: 'iːbɨgan'. [BrM:] Tentative analysis of 'puːɣɨlǯəǯal'.</ta>
            <ta e="T40" id="Seg_766" s="T32">[BrM:] 'Istolbaːrot' changed to 'Istol baːrot'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
