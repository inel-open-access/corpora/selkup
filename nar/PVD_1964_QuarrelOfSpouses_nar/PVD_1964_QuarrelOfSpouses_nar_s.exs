<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_QuarrelOfSpouses_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_QuarrelOfSpouses_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">64</ud-information>
            <ud-information attribute-name="# HIAT:w">41</ud-information>
            <ud-information attribute-name="# e">41</ud-information>
            <ud-information attribute-name="# HIAT:u">10</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T42" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Näjɣum</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">matqundo</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">ponä</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">čaǯɨn</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Ärat</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">mannɨmɨtdɨt</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">tʼäːn</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">al</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">ass</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">tütǯɨn</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_38" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">A</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">ärat</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">čaːʒɨn</ts>
                  <nts id="Seg_47" n="HIAT:ip">,</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">ärat</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">qwɛl</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_56" n="HIAT:w" s="T16">taːdɨrɨt</ts>
                  <nts id="Seg_57" n="HIAT:ip">.</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_60" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">Tʼarɨn</ts>
                  <nts id="Seg_63" n="HIAT:ip">:</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_65" n="HIAT:ip">“</nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">Pottə</ts>
                  <nts id="Seg_68" n="HIAT:ip">!</nts>
                  <nts id="Seg_69" n="HIAT:ip">”</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_72" n="HIAT:u" s="T19">
                  <nts id="Seg_73" n="HIAT:ip">“</nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">Ass</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">ättɨrčeǯan</ts>
                  <nts id="Seg_79" n="HIAT:ip">.</nts>
                  <nts id="Seg_80" n="HIAT:ip">”</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_83" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_85" n="HIAT:w" s="T21">Ärat</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_88" n="HIAT:w" s="T22">täbɨm</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_91" n="HIAT:w" s="T23">kak</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">qätɨdɨt</ts>
                  <nts id="Seg_95" n="HIAT:ip">.</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_98" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_100" n="HIAT:w" s="T25">Täp</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_103" n="HIAT:w" s="T26">türlʼe</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_106" n="HIAT:w" s="T27">jübəran</ts>
                  <nts id="Seg_107" n="HIAT:ip">,</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_110" n="HIAT:w" s="T28">tʼarɨn</ts>
                  <nts id="Seg_111" n="HIAT:ip">:</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_113" n="HIAT:ip">“</nts>
                  <ts e="T30" id="Seg_115" n="HIAT:w" s="T29">Man</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_118" n="HIAT:w" s="T30">toː</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_121" n="HIAT:w" s="T31">qwaǯan</ts>
                  <nts id="Seg_122" n="HIAT:ip">.</nts>
                  <nts id="Seg_123" n="HIAT:ip">”</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_126" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_128" n="HIAT:w" s="T32">Ärat</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_131" n="HIAT:w" s="T33">tʼärɨn</ts>
                  <nts id="Seg_132" n="HIAT:ip">:</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_134" n="HIAT:ip">“</nts>
                  <ts e="T35" id="Seg_136" n="HIAT:w" s="T34">Qwanɨq</ts>
                  <nts id="Seg_137" n="HIAT:ip">.</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_140" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_142" n="HIAT:w" s="T35">Man</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_145" n="HIAT:w" s="T36">aːrɨn</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_148" n="HIAT:w" s="T37">nädädǯan</ts>
                  <nts id="Seg_149" n="HIAT:ip">.</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_152" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_154" n="HIAT:w" s="T38">Teper</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_157" n="HIAT:w" s="T39">näjɣula</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_160" n="HIAT:w" s="T40">qocʼin</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_163" n="HIAT:w" s="T41">jewattə</ts>
                  <nts id="Seg_164" n="HIAT:ip">.</nts>
                  <nts id="Seg_165" n="HIAT:ip">”</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T42" id="Seg_167" n="sc" s="T1">
               <ts e="T2" id="Seg_169" n="e" s="T1">Näjɣum </ts>
               <ts e="T3" id="Seg_171" n="e" s="T2">matqundo </ts>
               <ts e="T4" id="Seg_173" n="e" s="T3">ponä </ts>
               <ts e="T5" id="Seg_175" n="e" s="T4">čaǯɨn. </ts>
               <ts e="T6" id="Seg_177" n="e" s="T5">Ärat </ts>
               <ts e="T7" id="Seg_179" n="e" s="T6">mannɨmɨtdɨt </ts>
               <ts e="T8" id="Seg_181" n="e" s="T7">tʼäːn </ts>
               <ts e="T9" id="Seg_183" n="e" s="T8">al </ts>
               <ts e="T10" id="Seg_185" n="e" s="T9">ass </ts>
               <ts e="T11" id="Seg_187" n="e" s="T10">tütǯɨn. </ts>
               <ts e="T12" id="Seg_189" n="e" s="T11">A </ts>
               <ts e="T13" id="Seg_191" n="e" s="T12">ärat </ts>
               <ts e="T14" id="Seg_193" n="e" s="T13">čaːʒɨn, </ts>
               <ts e="T15" id="Seg_195" n="e" s="T14">ärat </ts>
               <ts e="T16" id="Seg_197" n="e" s="T15">qwɛl </ts>
               <ts e="T17" id="Seg_199" n="e" s="T16">taːdɨrɨt. </ts>
               <ts e="T18" id="Seg_201" n="e" s="T17">Tʼarɨn: </ts>
               <ts e="T19" id="Seg_203" n="e" s="T18">“Pottə!” </ts>
               <ts e="T20" id="Seg_205" n="e" s="T19">“Ass </ts>
               <ts e="T21" id="Seg_207" n="e" s="T20">ättɨrčeǯan.” </ts>
               <ts e="T22" id="Seg_209" n="e" s="T21">Ärat </ts>
               <ts e="T23" id="Seg_211" n="e" s="T22">täbɨm </ts>
               <ts e="T24" id="Seg_213" n="e" s="T23">kak </ts>
               <ts e="T25" id="Seg_215" n="e" s="T24">qätɨdɨt. </ts>
               <ts e="T26" id="Seg_217" n="e" s="T25">Täp </ts>
               <ts e="T27" id="Seg_219" n="e" s="T26">türlʼe </ts>
               <ts e="T28" id="Seg_221" n="e" s="T27">jübəran, </ts>
               <ts e="T29" id="Seg_223" n="e" s="T28">tʼarɨn: </ts>
               <ts e="T30" id="Seg_225" n="e" s="T29">“Man </ts>
               <ts e="T31" id="Seg_227" n="e" s="T30">toː </ts>
               <ts e="T32" id="Seg_229" n="e" s="T31">qwaǯan.” </ts>
               <ts e="T33" id="Seg_231" n="e" s="T32">Ärat </ts>
               <ts e="T34" id="Seg_233" n="e" s="T33">tʼärɨn: </ts>
               <ts e="T35" id="Seg_235" n="e" s="T34">“Qwanɨq. </ts>
               <ts e="T36" id="Seg_237" n="e" s="T35">Man </ts>
               <ts e="T37" id="Seg_239" n="e" s="T36">aːrɨn </ts>
               <ts e="T38" id="Seg_241" n="e" s="T37">nädädǯan. </ts>
               <ts e="T39" id="Seg_243" n="e" s="T38">Teper </ts>
               <ts e="T40" id="Seg_245" n="e" s="T39">näjɣula </ts>
               <ts e="T41" id="Seg_247" n="e" s="T40">qocʼin </ts>
               <ts e="T42" id="Seg_249" n="e" s="T41">jewattə.” </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_250" s="T1">PVD_1964_QuarrelOfSpouses_nar.001 (001.001)</ta>
            <ta e="T11" id="Seg_251" s="T5">PVD_1964_QuarrelOfSpouses_nar.002 (001.002)</ta>
            <ta e="T17" id="Seg_252" s="T11">PVD_1964_QuarrelOfSpouses_nar.003 (001.003)</ta>
            <ta e="T19" id="Seg_253" s="T17">PVD_1964_QuarrelOfSpouses_nar.004 (001.004)</ta>
            <ta e="T21" id="Seg_254" s="T19">PVD_1964_QuarrelOfSpouses_nar.005 (001.005)</ta>
            <ta e="T25" id="Seg_255" s="T21">PVD_1964_QuarrelOfSpouses_nar.006 (001.006)</ta>
            <ta e="T32" id="Seg_256" s="T25">PVD_1964_QuarrelOfSpouses_nar.007 (001.007)</ta>
            <ta e="T35" id="Seg_257" s="T32">PVD_1964_QuarrelOfSpouses_nar.008 (001.008)</ta>
            <ta e="T38" id="Seg_258" s="T35">PVD_1964_QuarrelOfSpouses_nar.009 (001.009)</ta>
            <ta e="T42" id="Seg_259" s="T38">PVD_1964_QuarrelOfSpouses_nar.010 (001.010)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_260" s="T1">′нӓйɣум ′матkундо ′понӓ ′тша(т)джын.</ta>
            <ta e="T11" id="Seg_261" s="T5">′ӓрат ′манны‵мытдыт ′тʼӓ̄н аl асс ′тӱтджын.</ta>
            <ta e="T17" id="Seg_262" s="T11">а ӓрат ′тша̄жын, ӓ′рат ′kwɛл ′та̄дырыт.</ta>
            <ta e="T19" id="Seg_263" s="T17">тʼа′рын: ′поттъ!</ta>
            <ta e="T21" id="Seg_264" s="T19">асс ′ӓттыр ′тшеджан.</ta>
            <ta e="T25" id="Seg_265" s="T21">ӓ′рат тӓбым как ′kӓтыдыт.</ta>
            <ta e="T32" id="Seg_266" s="T25">тӓп ′тӱрлʼе ′jӱбъран, тʼа′рын: ман то̄ kwа(т)′джан.</ta>
            <ta e="T35" id="Seg_267" s="T32">ӓ′рат тʼӓ′рын: kwа′ныk.</ta>
            <ta e="T38" id="Seg_268" s="T35">ман ′а̄рын ′нӓдӓдджан.</ta>
            <ta e="T42" id="Seg_269" s="T38">тепер ′нӓйɣу′ла kоцʼин ′jеwаттъ.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_270" s="T1">näjɣum matqundo ponä tša(t)ǯɨn.</ta>
            <ta e="T11" id="Seg_271" s="T5">ärat mannɨmɨtdɨt tʼäːn al ass tütǯɨn.</ta>
            <ta e="T17" id="Seg_272" s="T11">a ärat tšaːʒɨn, ärat qwɛl taːdɨrɨt.</ta>
            <ta e="T19" id="Seg_273" s="T17">tʼarɨn: pottə!</ta>
            <ta e="T21" id="Seg_274" s="T19">ass ättɨr tšeǯan.</ta>
            <ta e="T25" id="Seg_275" s="T21">ärat täbɨm kak qätɨdɨt.</ta>
            <ta e="T32" id="Seg_276" s="T25">täp türlʼe jübəran, tʼarɨn: man toː qwa(t)ǯan.</ta>
            <ta e="T35" id="Seg_277" s="T32">ärat tʼärɨn: qwanɨq.</ta>
            <ta e="T38" id="Seg_278" s="T35">man aːrɨn nädädǯan.</ta>
            <ta e="T42" id="Seg_279" s="T38">teper näjɣula qocʼin jewattə.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_280" s="T1">Näjɣum matqundo ponä čaǯɨn. </ta>
            <ta e="T11" id="Seg_281" s="T5">Ärat mannɨmɨtdɨt tʼäːn al ass tütǯɨn. </ta>
            <ta e="T17" id="Seg_282" s="T11">A ärat čaːʒɨn, ärat qwɛl taːdɨrɨt. </ta>
            <ta e="T19" id="Seg_283" s="T17">Tʼarɨn: “Pottə!” </ta>
            <ta e="T21" id="Seg_284" s="T19">“Ass ättɨrčeǯan.” </ta>
            <ta e="T25" id="Seg_285" s="T21">Ärat täbɨm kak qätɨdɨt. </ta>
            <ta e="T32" id="Seg_286" s="T25">Täp türlʼe jübəran, tʼarɨn: “Man toː qwaǯan.” </ta>
            <ta e="T35" id="Seg_287" s="T32">Ärat tʼärɨn: “Qwanɨq. </ta>
            <ta e="T38" id="Seg_288" s="T35">Man aːrɨn nädädǯan. </ta>
            <ta e="T42" id="Seg_289" s="T38">Teper näjɣula qocʼin jewattə.” </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_290" s="T1">nä-j-ɣum</ta>
            <ta e="T3" id="Seg_291" s="T2">mat-qundo</ta>
            <ta e="T4" id="Seg_292" s="T3">ponä</ta>
            <ta e="T5" id="Seg_293" s="T4">čaǯɨ-n</ta>
            <ta e="T6" id="Seg_294" s="T5">ära-t</ta>
            <ta e="T7" id="Seg_295" s="T6">mannɨ-mɨ-tdɨ-t</ta>
            <ta e="T8" id="Seg_296" s="T7">tʼäːn</ta>
            <ta e="T9" id="Seg_297" s="T8">al</ta>
            <ta e="T10" id="Seg_298" s="T9">ass</ta>
            <ta e="T11" id="Seg_299" s="T10">tü-tǯɨ-n</ta>
            <ta e="T12" id="Seg_300" s="T11">a</ta>
            <ta e="T13" id="Seg_301" s="T12">ära-t</ta>
            <ta e="T14" id="Seg_302" s="T13">čaːʒɨ-n</ta>
            <ta e="T15" id="Seg_303" s="T14">ära-t</ta>
            <ta e="T16" id="Seg_304" s="T15">qwɛl</ta>
            <ta e="T17" id="Seg_305" s="T16">taːd-ɨ-r-ɨ-t</ta>
            <ta e="T18" id="Seg_306" s="T17">tʼarɨ-n</ta>
            <ta e="T19" id="Seg_307" s="T18">pot-tə</ta>
            <ta e="T20" id="Seg_308" s="T19">ass</ta>
            <ta e="T21" id="Seg_309" s="T20">ättɨr-č-eǯa-n</ta>
            <ta e="T22" id="Seg_310" s="T21">ära-t</ta>
            <ta e="T23" id="Seg_311" s="T22">täb-ɨ-m</ta>
            <ta e="T24" id="Seg_312" s="T23">kak</ta>
            <ta e="T25" id="Seg_313" s="T24">qätɨ-dɨ-t</ta>
            <ta e="T26" id="Seg_314" s="T25">täp</ta>
            <ta e="T27" id="Seg_315" s="T26">tür-lʼe</ta>
            <ta e="T28" id="Seg_316" s="T27">jübə-ra-n</ta>
            <ta e="T29" id="Seg_317" s="T28">tʼarɨ-n</ta>
            <ta e="T30" id="Seg_318" s="T29">Man</ta>
            <ta e="T31" id="Seg_319" s="T30">toː</ta>
            <ta e="T32" id="Seg_320" s="T31">qwa-ǯa-n</ta>
            <ta e="T33" id="Seg_321" s="T32">ära-t</ta>
            <ta e="T34" id="Seg_322" s="T33">tʼärɨ-n</ta>
            <ta e="T35" id="Seg_323" s="T34">qwan-ɨ-q</ta>
            <ta e="T36" id="Seg_324" s="T35">man</ta>
            <ta e="T37" id="Seg_325" s="T36">aːr-ɨ-n</ta>
            <ta e="T38" id="Seg_326" s="T37">näd-ädǯa-n</ta>
            <ta e="T39" id="Seg_327" s="T38">teper</ta>
            <ta e="T40" id="Seg_328" s="T39">nä-j-ɣu-la</ta>
            <ta e="T41" id="Seg_329" s="T40">qocʼi-n</ta>
            <ta e="T42" id="Seg_330" s="T41">je-wa-ttə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_331" s="T1">ne-lʼ-qum</ta>
            <ta e="T3" id="Seg_332" s="T2">maːt-qɨntɨ</ta>
            <ta e="T4" id="Seg_333" s="T3">poːne</ta>
            <ta e="T5" id="Seg_334" s="T4">čaǯɨ-n</ta>
            <ta e="T6" id="Seg_335" s="T5">era-tə</ta>
            <ta e="T7" id="Seg_336" s="T6">*mantɨ-mbɨ-ntɨ-t</ta>
            <ta e="T8" id="Seg_337" s="T7">tʼäːŋ</ta>
            <ta e="T9" id="Seg_338" s="T8">alʼi</ta>
            <ta e="T10" id="Seg_339" s="T9">asa</ta>
            <ta e="T11" id="Seg_340" s="T10">tüː-enǯɨ-n</ta>
            <ta e="T12" id="Seg_341" s="T11">a</ta>
            <ta e="T13" id="Seg_342" s="T12">era-tə</ta>
            <ta e="T14" id="Seg_343" s="T13">čaǯɨ-n</ta>
            <ta e="T15" id="Seg_344" s="T14">era-tə</ta>
            <ta e="T16" id="Seg_345" s="T15">qwɛl</ta>
            <ta e="T17" id="Seg_346" s="T16">tat-ɨ-r-ɨ-t</ta>
            <ta e="T18" id="Seg_347" s="T17">tʼärɨ-n</ta>
            <ta e="T19" id="Seg_348" s="T18">poːt-etɨ</ta>
            <ta e="T20" id="Seg_349" s="T19">asa</ta>
            <ta e="T21" id="Seg_350" s="T20">ɛttɨr-ču-enǯɨ-ŋ</ta>
            <ta e="T22" id="Seg_351" s="T21">era-tə</ta>
            <ta e="T23" id="Seg_352" s="T22">täp-ɨ-m</ta>
            <ta e="T24" id="Seg_353" s="T23">kak</ta>
            <ta e="T25" id="Seg_354" s="T24">qättɨ-ntɨ-t</ta>
            <ta e="T26" id="Seg_355" s="T25">täp</ta>
            <ta e="T27" id="Seg_356" s="T26">tʼüru-le</ta>
            <ta e="T28" id="Seg_357" s="T27">übɨ-rɨ-ŋ</ta>
            <ta e="T29" id="Seg_358" s="T28">tʼärɨ-n</ta>
            <ta e="T30" id="Seg_359" s="T29">man</ta>
            <ta e="T31" id="Seg_360" s="T30">to</ta>
            <ta e="T32" id="Seg_361" s="T31">qwan-enǯɨ-ŋ</ta>
            <ta e="T33" id="Seg_362" s="T32">era-tə</ta>
            <ta e="T34" id="Seg_363" s="T33">tʼärɨ-n</ta>
            <ta e="T35" id="Seg_364" s="T34">qwan-ɨ-kɨ</ta>
            <ta e="T36" id="Seg_365" s="T35">man</ta>
            <ta e="T37" id="Seg_366" s="T36">aːr-ɨ-ŋ</ta>
            <ta e="T38" id="Seg_367" s="T37">nedɨ-enǯɨ-ŋ</ta>
            <ta e="T39" id="Seg_368" s="T38">teper</ta>
            <ta e="T40" id="Seg_369" s="T39">ne-lʼ-qum-la</ta>
            <ta e="T41" id="Seg_370" s="T40">koːci-ŋ</ta>
            <ta e="T42" id="Seg_371" s="T41">eː-nɨ-tɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_372" s="T1">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T3" id="Seg_373" s="T2">house-EL.3SG</ta>
            <ta e="T4" id="Seg_374" s="T3">outward(s)</ta>
            <ta e="T5" id="Seg_375" s="T4">go-3SG.S</ta>
            <ta e="T6" id="Seg_376" s="T5">husband.[NOM]-3SG</ta>
            <ta e="T7" id="Seg_377" s="T6">look-DUR-INFER-3SG.O</ta>
            <ta e="T8" id="Seg_378" s="T7">soon</ta>
            <ta e="T9" id="Seg_379" s="T8">or</ta>
            <ta e="T10" id="Seg_380" s="T9">NEG</ta>
            <ta e="T11" id="Seg_381" s="T10">come-FUT-3SG.S</ta>
            <ta e="T12" id="Seg_382" s="T11">and</ta>
            <ta e="T13" id="Seg_383" s="T12">husband.[NOM]-3SG</ta>
            <ta e="T14" id="Seg_384" s="T13">go-3SG.S</ta>
            <ta e="T15" id="Seg_385" s="T14">husband.[NOM]-3SG</ta>
            <ta e="T16" id="Seg_386" s="T15">fish.[NOM]</ta>
            <ta e="T17" id="Seg_387" s="T16">bring-EP-FRQ-EP-3SG.O</ta>
            <ta e="T18" id="Seg_388" s="T17">say-3SG.S</ta>
            <ta e="T19" id="Seg_389" s="T18">cook-IMP.2SG.O</ta>
            <ta e="T20" id="Seg_390" s="T19">NEG</ta>
            <ta e="T21" id="Seg_391" s="T20">cook-TR-FUT-1SG.S</ta>
            <ta e="T22" id="Seg_392" s="T21">husband.[NOM]-3SG</ta>
            <ta e="T23" id="Seg_393" s="T22">(s)he-EP-ACC</ta>
            <ta e="T24" id="Seg_394" s="T23">suddenly</ta>
            <ta e="T25" id="Seg_395" s="T24">beat-INFER-3SG.O</ta>
            <ta e="T26" id="Seg_396" s="T25">(s)he.[NOM]</ta>
            <ta e="T27" id="Seg_397" s="T26">cry-CVB</ta>
            <ta e="T28" id="Seg_398" s="T27">begin-DRV-1SG.S</ta>
            <ta e="T29" id="Seg_399" s="T28">say-3SG.S</ta>
            <ta e="T30" id="Seg_400" s="T29">I.NOM</ta>
            <ta e="T31" id="Seg_401" s="T30">away</ta>
            <ta e="T32" id="Seg_402" s="T31">leave-FUT-1SG.S</ta>
            <ta e="T33" id="Seg_403" s="T32">husband.[NOM]-3SG</ta>
            <ta e="T34" id="Seg_404" s="T33">say-3SG.S</ta>
            <ta e="T35" id="Seg_405" s="T34">leave-EP-IMP.2SG.S</ta>
            <ta e="T36" id="Seg_406" s="T35">I.NOM</ta>
            <ta e="T37" id="Seg_407" s="T36">other-EP-ADVZ</ta>
            <ta e="T38" id="Seg_408" s="T37">get.married-FUT-1SG.S</ta>
            <ta e="T39" id="Seg_409" s="T38">now</ta>
            <ta e="T40" id="Seg_410" s="T39">woman-ADJZ-human.being-PL.[NOM]</ta>
            <ta e="T41" id="Seg_411" s="T40">much-ADVZ</ta>
            <ta e="T42" id="Seg_412" s="T41">be-CO-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_413" s="T1">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T3" id="Seg_414" s="T2">дом-EL.3SG</ta>
            <ta e="T4" id="Seg_415" s="T3">наружу</ta>
            <ta e="T5" id="Seg_416" s="T4">ходить-3SG.S</ta>
            <ta e="T6" id="Seg_417" s="T5">муж.[NOM]-3SG</ta>
            <ta e="T7" id="Seg_418" s="T6">посмотреть-DUR-INFER-3SG.O</ta>
            <ta e="T8" id="Seg_419" s="T7">скоро</ta>
            <ta e="T9" id="Seg_420" s="T8">али</ta>
            <ta e="T10" id="Seg_421" s="T9">NEG</ta>
            <ta e="T11" id="Seg_422" s="T10">прийти-FUT-3SG.S</ta>
            <ta e="T12" id="Seg_423" s="T11">а</ta>
            <ta e="T13" id="Seg_424" s="T12">муж.[NOM]-3SG</ta>
            <ta e="T14" id="Seg_425" s="T13">ходить-3SG.S</ta>
            <ta e="T15" id="Seg_426" s="T14">муж.[NOM]-3SG</ta>
            <ta e="T16" id="Seg_427" s="T15">рыба.[NOM]</ta>
            <ta e="T17" id="Seg_428" s="T16">принести-EP-FRQ-EP-3SG.O</ta>
            <ta e="T18" id="Seg_429" s="T17">сказать-3SG.S</ta>
            <ta e="T19" id="Seg_430" s="T18">сварить-IMP.2SG.O</ta>
            <ta e="T20" id="Seg_431" s="T19">NEG</ta>
            <ta e="T21" id="Seg_432" s="T20">сварить-TR-FUT-1SG.S</ta>
            <ta e="T22" id="Seg_433" s="T21">муж.[NOM]-3SG</ta>
            <ta e="T23" id="Seg_434" s="T22">он(а)-EP-ACC</ta>
            <ta e="T24" id="Seg_435" s="T23">как</ta>
            <ta e="T25" id="Seg_436" s="T24">побить-INFER-3SG.O</ta>
            <ta e="T26" id="Seg_437" s="T25">он(а).[NOM]</ta>
            <ta e="T27" id="Seg_438" s="T26">плакать-CVB</ta>
            <ta e="T28" id="Seg_439" s="T27">начать-DRV-1SG.S</ta>
            <ta e="T29" id="Seg_440" s="T28">сказать-3SG.S</ta>
            <ta e="T30" id="Seg_441" s="T29">я.NOM</ta>
            <ta e="T31" id="Seg_442" s="T30">прочь</ta>
            <ta e="T32" id="Seg_443" s="T31">отправиться-FUT-1SG.S</ta>
            <ta e="T33" id="Seg_444" s="T32">муж.[NOM]-3SG</ta>
            <ta e="T34" id="Seg_445" s="T33">сказать-3SG.S</ta>
            <ta e="T35" id="Seg_446" s="T34">отправиться-EP-IMP.2SG.S</ta>
            <ta e="T36" id="Seg_447" s="T35">я.NOM</ta>
            <ta e="T37" id="Seg_448" s="T36">другой-EP-ADVZ</ta>
            <ta e="T38" id="Seg_449" s="T37">жениться-FUT-1SG.S</ta>
            <ta e="T39" id="Seg_450" s="T38">теперь</ta>
            <ta e="T40" id="Seg_451" s="T39">женщина-ADJZ-человек-PL.[NOM]</ta>
            <ta e="T41" id="Seg_452" s="T40">много-ADVZ</ta>
            <ta e="T42" id="Seg_453" s="T41">быть-CO-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_454" s="T1">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T3" id="Seg_455" s="T2">n-n:case.poss</ta>
            <ta e="T4" id="Seg_456" s="T3">adv</ta>
            <ta e="T5" id="Seg_457" s="T4">v-v:pn</ta>
            <ta e="T6" id="Seg_458" s="T5">n.[n:case]-n:poss</ta>
            <ta e="T7" id="Seg_459" s="T6">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T8" id="Seg_460" s="T7">adv</ta>
            <ta e="T9" id="Seg_461" s="T8">conj</ta>
            <ta e="T10" id="Seg_462" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_463" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_464" s="T11">conj</ta>
            <ta e="T13" id="Seg_465" s="T12">n.[n:case]-n:poss</ta>
            <ta e="T14" id="Seg_466" s="T13">v-v:pn</ta>
            <ta e="T15" id="Seg_467" s="T14">n.[n:case]-n:poss</ta>
            <ta e="T16" id="Seg_468" s="T15">n.[n:case]</ta>
            <ta e="T17" id="Seg_469" s="T16">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T18" id="Seg_470" s="T17">v-v:pn</ta>
            <ta e="T19" id="Seg_471" s="T18">v-v:mood.pn</ta>
            <ta e="T20" id="Seg_472" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_473" s="T20">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_474" s="T21">n.[n:case]-n:poss</ta>
            <ta e="T23" id="Seg_475" s="T22">pers-n:ins-n:case</ta>
            <ta e="T24" id="Seg_476" s="T23">adv</ta>
            <ta e="T25" id="Seg_477" s="T24">v-v:mood-v:pn</ta>
            <ta e="T26" id="Seg_478" s="T25">pers.[n:case]</ta>
            <ta e="T27" id="Seg_479" s="T26">v-v&gt;adv</ta>
            <ta e="T28" id="Seg_480" s="T27">v-v&gt;v-v:pn</ta>
            <ta e="T29" id="Seg_481" s="T28">v-v:pn</ta>
            <ta e="T30" id="Seg_482" s="T29">pers</ta>
            <ta e="T31" id="Seg_483" s="T30">preverb</ta>
            <ta e="T32" id="Seg_484" s="T31">v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_485" s="T32">n.[n:case]-n:poss</ta>
            <ta e="T34" id="Seg_486" s="T33">v-v:pn</ta>
            <ta e="T35" id="Seg_487" s="T34">v-v:ins-v:mood.pn</ta>
            <ta e="T36" id="Seg_488" s="T35">pers</ta>
            <ta e="T37" id="Seg_489" s="T36">adj-n:ins-adj&gt;adv</ta>
            <ta e="T38" id="Seg_490" s="T37">v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_491" s="T38">adv</ta>
            <ta e="T40" id="Seg_492" s="T39">n-n&gt;adj-n-n:num.[n:case]</ta>
            <ta e="T41" id="Seg_493" s="T40">quant-quant&gt;adv</ta>
            <ta e="T42" id="Seg_494" s="T41">v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_495" s="T1">n</ta>
            <ta e="T3" id="Seg_496" s="T2">n</ta>
            <ta e="T4" id="Seg_497" s="T3">adv</ta>
            <ta e="T5" id="Seg_498" s="T4">v</ta>
            <ta e="T6" id="Seg_499" s="T5">n</ta>
            <ta e="T7" id="Seg_500" s="T6">v</ta>
            <ta e="T8" id="Seg_501" s="T7">adv</ta>
            <ta e="T9" id="Seg_502" s="T8">conj</ta>
            <ta e="T10" id="Seg_503" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_504" s="T10">v</ta>
            <ta e="T12" id="Seg_505" s="T11">conj</ta>
            <ta e="T13" id="Seg_506" s="T12">n</ta>
            <ta e="T14" id="Seg_507" s="T13">v</ta>
            <ta e="T15" id="Seg_508" s="T14">n</ta>
            <ta e="T16" id="Seg_509" s="T15">n</ta>
            <ta e="T17" id="Seg_510" s="T16">v</ta>
            <ta e="T18" id="Seg_511" s="T17">v</ta>
            <ta e="T19" id="Seg_512" s="T18">n</ta>
            <ta e="T20" id="Seg_513" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_514" s="T20">v</ta>
            <ta e="T22" id="Seg_515" s="T21">n</ta>
            <ta e="T23" id="Seg_516" s="T22">pers</ta>
            <ta e="T24" id="Seg_517" s="T23">adv</ta>
            <ta e="T25" id="Seg_518" s="T24">v</ta>
            <ta e="T26" id="Seg_519" s="T25">pers</ta>
            <ta e="T27" id="Seg_520" s="T26">adv</ta>
            <ta e="T28" id="Seg_521" s="T27">v</ta>
            <ta e="T29" id="Seg_522" s="T28">v</ta>
            <ta e="T30" id="Seg_523" s="T29">pers</ta>
            <ta e="T31" id="Seg_524" s="T30">preverb</ta>
            <ta e="T32" id="Seg_525" s="T31">v</ta>
            <ta e="T33" id="Seg_526" s="T32">n</ta>
            <ta e="T34" id="Seg_527" s="T33">v</ta>
            <ta e="T35" id="Seg_528" s="T34">v</ta>
            <ta e="T36" id="Seg_529" s="T35">pers</ta>
            <ta e="T37" id="Seg_530" s="T36">adv</ta>
            <ta e="T38" id="Seg_531" s="T37">v</ta>
            <ta e="T39" id="Seg_532" s="T38">adv</ta>
            <ta e="T40" id="Seg_533" s="T39">n</ta>
            <ta e="T41" id="Seg_534" s="T40">adv</ta>
            <ta e="T42" id="Seg_535" s="T41">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_536" s="T1">np.h:A</ta>
            <ta e="T3" id="Seg_537" s="T2">np:So</ta>
            <ta e="T4" id="Seg_538" s="T3">adv:G</ta>
            <ta e="T6" id="Seg_539" s="T5">np.h:Th 0.3.h:Poss</ta>
            <ta e="T7" id="Seg_540" s="T6">0.3.h:A</ta>
            <ta e="T11" id="Seg_541" s="T10">0.3.h:A</ta>
            <ta e="T13" id="Seg_542" s="T12">np.h:A 0.3.h:Poss</ta>
            <ta e="T15" id="Seg_543" s="T14">np.h:A 0.3.h:Poss</ta>
            <ta e="T16" id="Seg_544" s="T15">np:Th</ta>
            <ta e="T18" id="Seg_545" s="T17">0.3.h:A</ta>
            <ta e="T19" id="Seg_546" s="T18">0.2.h:A 0.3:P</ta>
            <ta e="T21" id="Seg_547" s="T20">0.1.h:A</ta>
            <ta e="T22" id="Seg_548" s="T21">np.h:A 0.3.h:Poss</ta>
            <ta e="T23" id="Seg_549" s="T22">pro.h:P</ta>
            <ta e="T26" id="Seg_550" s="T25">pro.h:A</ta>
            <ta e="T29" id="Seg_551" s="T28">0.3.h:A</ta>
            <ta e="T30" id="Seg_552" s="T29">pro.h:A</ta>
            <ta e="T33" id="Seg_553" s="T32">np.h:A 0.3.h:Poss</ta>
            <ta e="T35" id="Seg_554" s="T34">0.2.h:A</ta>
            <ta e="T36" id="Seg_555" s="T35">pro.h:A</ta>
            <ta e="T39" id="Seg_556" s="T38">adv:Time</ta>
            <ta e="T40" id="Seg_557" s="T39">np.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_558" s="T1">np.h:S</ta>
            <ta e="T5" id="Seg_559" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_560" s="T5">np.h:O</ta>
            <ta e="T7" id="Seg_561" s="T6">0.3.h:S v:pred</ta>
            <ta e="T11" id="Seg_562" s="T10">0.3.h:S v:pred</ta>
            <ta e="T13" id="Seg_563" s="T12">np.h:S</ta>
            <ta e="T14" id="Seg_564" s="T13">v:pred</ta>
            <ta e="T15" id="Seg_565" s="T14">np.h:S</ta>
            <ta e="T16" id="Seg_566" s="T15">np:O</ta>
            <ta e="T17" id="Seg_567" s="T16">v:pred</ta>
            <ta e="T18" id="Seg_568" s="T17">0.3.h:S v:pred</ta>
            <ta e="T19" id="Seg_569" s="T18">0.2.h:S v:pred 0.3:O</ta>
            <ta e="T21" id="Seg_570" s="T20">0.1.h:S v:pred</ta>
            <ta e="T22" id="Seg_571" s="T21">np.h:S</ta>
            <ta e="T23" id="Seg_572" s="T22">pro.h:O</ta>
            <ta e="T25" id="Seg_573" s="T24">v:pred</ta>
            <ta e="T26" id="Seg_574" s="T25">pro.h:S</ta>
            <ta e="T28" id="Seg_575" s="T27">v:pred</ta>
            <ta e="T29" id="Seg_576" s="T28">0.3.h:S v:pred</ta>
            <ta e="T30" id="Seg_577" s="T29">pro.h:S</ta>
            <ta e="T32" id="Seg_578" s="T31">v:pred</ta>
            <ta e="T33" id="Seg_579" s="T32">np.h:S</ta>
            <ta e="T34" id="Seg_580" s="T33">v:pred</ta>
            <ta e="T35" id="Seg_581" s="T34">0.2.h:S v:pred</ta>
            <ta e="T36" id="Seg_582" s="T35">pro.h:S</ta>
            <ta e="T38" id="Seg_583" s="T37">v:pred</ta>
            <ta e="T40" id="Seg_584" s="T39">np.h:S</ta>
            <ta e="T42" id="Seg_585" s="T41">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T9" id="Seg_586" s="T8">RUS:gram</ta>
            <ta e="T12" id="Seg_587" s="T11">RUS:gram</ta>
            <ta e="T24" id="Seg_588" s="T23">RUS:disc</ta>
            <ta e="T39" id="Seg_589" s="T38">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_590" s="T1">A woman went outside.</ta>
            <ta e="T11" id="Seg_591" s="T5">She was spotting her husband: whether he is coming or not.</ta>
            <ta e="T17" id="Seg_592" s="T11">And her husband came and brought fish.</ta>
            <ta e="T19" id="Seg_593" s="T17">He said: “Cook it!”</ta>
            <ta e="T21" id="Seg_594" s="T19">“I won't cook.”</ta>
            <ta e="T25" id="Seg_595" s="T21">Her husband hit her.</ta>
            <ta e="T32" id="Seg_596" s="T25">She began crying and said: “I'll go away.”</ta>
            <ta e="T35" id="Seg_597" s="T32">Her husband said: “Go.</ta>
            <ta e="T38" id="Seg_598" s="T35">I'll marry another one.</ta>
            <ta e="T42" id="Seg_599" s="T38">There are many women now.”</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_600" s="T1">Eine Frau ging aus dem Haus nach draußen.</ta>
            <ta e="T11" id="Seg_601" s="T5">Sie sah nach ihrem Mann, ob er kam oder nicht.</ta>
            <ta e="T17" id="Seg_602" s="T11">Und ihr Mann kam und brachte Fisch.</ta>
            <ta e="T19" id="Seg_603" s="T17">Er sagte: "Koch ihn!"</ta>
            <ta e="T21" id="Seg_604" s="T19">"Ich werde nicht kochen."</ta>
            <ta e="T25" id="Seg_605" s="T21">Ihr Mann schlug sie.</ta>
            <ta e="T32" id="Seg_606" s="T25">Sie begann zu weinen und sagte: "Ich werde weggehen."</ta>
            <ta e="T35" id="Seg_607" s="T32">Ihr Mann sagte: "Geh.</ta>
            <ta e="T38" id="Seg_608" s="T35">Ich werde eine andere heiraten.</ta>
            <ta e="T42" id="Seg_609" s="T38">Es gibt jetzt viele Frauen."</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_610" s="T1">Женщина из дома на улицу пошла.</ta>
            <ta e="T11" id="Seg_611" s="T5">Мужа высматривает: скоро или нет придет.</ta>
            <ta e="T17" id="Seg_612" s="T11">А муж идет, муж рыбу несет.</ta>
            <ta e="T19" id="Seg_613" s="T17">Сказал: “Вари!”</ta>
            <ta e="T21" id="Seg_614" s="T19">“Не буду варить.”</ta>
            <ta e="T25" id="Seg_615" s="T21">Муж ее как ударил.</ta>
            <ta e="T32" id="Seg_616" s="T25">Она плакать начала, говорит: “Я уйду”.</ta>
            <ta e="T35" id="Seg_617" s="T32">Муж ее говорит: “Иди.</ta>
            <ta e="T38" id="Seg_618" s="T35">Я на другой женюсь.</ta>
            <ta e="T42" id="Seg_619" s="T38">Теперь женщин много”.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_620" s="T1">женщина из дома на улицу вышла</ta>
            <ta e="T11" id="Seg_621" s="T5">мужика смотрит скоро или нет придет</ta>
            <ta e="T17" id="Seg_622" s="T11">а мужик идет старик рыбы несет</ta>
            <ta e="T19" id="Seg_623" s="T17">сказал вари</ta>
            <ta e="T21" id="Seg_624" s="T19">не буду варить</ta>
            <ta e="T25" id="Seg_625" s="T21">старик ее как ударил</ta>
            <ta e="T32" id="Seg_626" s="T25">она плакать начала говорит я уйду</ta>
            <ta e="T35" id="Seg_627" s="T32">старик говорит иди</ta>
            <ta e="T38" id="Seg_628" s="T35">я на другой женюсь</ta>
            <ta e="T42" id="Seg_629" s="T38">теперь женщин много</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T5" id="Seg_630" s="T1">[KuAI:] Variant: 'čatʒɨn'.</ta>
            <ta e="T21" id="Seg_631" s="T19">[BrM:] 'ättɨr čeǯan' changed to 'ättɨrčeǯan'. [BrM:] TR?</ta>
            <ta e="T25" id="Seg_632" s="T21">[BrM:] INFER?</ta>
            <ta e="T32" id="Seg_633" s="T25">[KuAI:] Variant: 'qwatǯan'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
