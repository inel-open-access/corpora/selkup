<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_RichMan_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_RichMan_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">20</ud-information>
            <ud-information attribute-name="# HIAT:w">16</ud-information>
            <ud-information attribute-name="# e">16</ud-information>
            <ud-information attribute-name="# HIAT:u">4</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T17" id="Seg_0" n="sc" s="T1">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Tau</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">qum</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">koːcʼi</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">qula</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">warɨtɨt</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_19" n="HIAT:w" s="T6">paxajčugu</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_23" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">Tebla</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">tebnan</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">uːdʼattə</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_35" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">Tep</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">teblam</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">mučit</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_47" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">Teblan</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">qomden</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">qɨːban</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">mekut</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T17" id="Seg_61" n="sc" s="T1">
               <ts e="T2" id="Seg_63" n="e" s="T1">Tau </ts>
               <ts e="T3" id="Seg_65" n="e" s="T2">qum </ts>
               <ts e="T4" id="Seg_67" n="e" s="T3">koːcʼi </ts>
               <ts e="T5" id="Seg_69" n="e" s="T4">qula </ts>
               <ts e="T6" id="Seg_71" n="e" s="T5">warɨtɨt </ts>
               <ts e="T7" id="Seg_73" n="e" s="T6">paxajčugu. </ts>
               <ts e="T8" id="Seg_75" n="e" s="T7">Tebla </ts>
               <ts e="T9" id="Seg_77" n="e" s="T8">tebnan </ts>
               <ts e="T10" id="Seg_79" n="e" s="T9">uːdʼattə. </ts>
               <ts e="T11" id="Seg_81" n="e" s="T10">Tep </ts>
               <ts e="T12" id="Seg_83" n="e" s="T11">teblam </ts>
               <ts e="T13" id="Seg_85" n="e" s="T12">mučit. </ts>
               <ts e="T14" id="Seg_87" n="e" s="T13">Teblan </ts>
               <ts e="T15" id="Seg_89" n="e" s="T14">qomden </ts>
               <ts e="T16" id="Seg_91" n="e" s="T15">qɨːban </ts>
               <ts e="T17" id="Seg_93" n="e" s="T16">mekut. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_94" s="T1">PVD_1964_RichMan_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_95" s="T7">PVD_1964_RichMan_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_96" s="T10">PVD_1964_RichMan_nar.003 (001.003)</ta>
            <ta e="T17" id="Seg_97" s="T13">PVD_1964_RichMan_nar.004 (001.004)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T7" id="Seg_98" s="T1">тау kум ко̄цʼи ′kуlа ′wарытыт па′хайч(тш)угу.</ta>
            <ta e="T10" id="Seg_99" s="T7">теб′ла теб′нан ′ӯдʼаттъ.</ta>
            <ta e="T13" id="Seg_100" s="T10">те̨п теблам ′мучит.</ta>
            <ta e="T17" id="Seg_101" s="T13">теб′лан kом′де̨н kы̄бан ме′кут.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T7" id="Seg_102" s="T1">tau qum koːcʼi qula warɨtɨt paxajč(tš)ugu.</ta>
            <ta e="T10" id="Seg_103" s="T7">tebla tebnan uːdʼattə.</ta>
            <ta e="T13" id="Seg_104" s="T10">tep teblam mučit.</ta>
            <ta e="T17" id="Seg_105" s="T13">teblan qomden qɨːban mekut.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_106" s="T1">Tau qum koːcʼi qula warɨtɨt paxajčugu. </ta>
            <ta e="T10" id="Seg_107" s="T7">Tebla tebnan uːdʼattə. </ta>
            <ta e="T13" id="Seg_108" s="T10">Tep teblam mučit. </ta>
            <ta e="T17" id="Seg_109" s="T13">Teblan qomden qɨːban mekut. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_110" s="T1">tau</ta>
            <ta e="T3" id="Seg_111" s="T2">qum</ta>
            <ta e="T4" id="Seg_112" s="T3">koːcʼi</ta>
            <ta e="T5" id="Seg_113" s="T4">qu-la</ta>
            <ta e="T6" id="Seg_114" s="T5">warɨ-tɨ-t</ta>
            <ta e="T7" id="Seg_115" s="T6">paxaj-ču-gu</ta>
            <ta e="T8" id="Seg_116" s="T7">teb-la</ta>
            <ta e="T9" id="Seg_117" s="T8">teb-nan</ta>
            <ta e="T10" id="Seg_118" s="T9">uːdʼa-ttə</ta>
            <ta e="T11" id="Seg_119" s="T10">tep</ta>
            <ta e="T12" id="Seg_120" s="T11">teb-la-m</ta>
            <ta e="T13" id="Seg_121" s="T12">muči-t</ta>
            <ta e="T14" id="Seg_122" s="T13">teb-la-n</ta>
            <ta e="T15" id="Seg_123" s="T14">qomde-n</ta>
            <ta e="T16" id="Seg_124" s="T15">qɨːba-n</ta>
            <ta e="T17" id="Seg_125" s="T16">me-ku-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_126" s="T1">taw</ta>
            <ta e="T3" id="Seg_127" s="T2">qum</ta>
            <ta e="T4" id="Seg_128" s="T3">koːci</ta>
            <ta e="T5" id="Seg_129" s="T4">qum-la</ta>
            <ta e="T6" id="Seg_130" s="T5">warɨ-ntɨ-t</ta>
            <ta e="T7" id="Seg_131" s="T6">paxaj-ču-gu</ta>
            <ta e="T8" id="Seg_132" s="T7">täp-la</ta>
            <ta e="T9" id="Seg_133" s="T8">täp-nan</ta>
            <ta e="T10" id="Seg_134" s="T9">uːdʼi-tɨn</ta>
            <ta e="T11" id="Seg_135" s="T10">täp</ta>
            <ta e="T12" id="Seg_136" s="T11">täp-la-m</ta>
            <ta e="T13" id="Seg_137" s="T12">muči-t</ta>
            <ta e="T14" id="Seg_138" s="T13">täp-la-n</ta>
            <ta e="T15" id="Seg_139" s="T14">qomdɛ-n</ta>
            <ta e="T16" id="Seg_140" s="T15">qɨba-ŋ</ta>
            <ta e="T17" id="Seg_141" s="T16">me-ku-t</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_142" s="T1">this</ta>
            <ta e="T3" id="Seg_143" s="T2">human.being.[NOM]</ta>
            <ta e="T4" id="Seg_144" s="T3">much</ta>
            <ta e="T5" id="Seg_145" s="T4">human.being-PL.[NOM]</ta>
            <ta e="T6" id="Seg_146" s="T5">have-INFER-3SG.O</ta>
            <ta e="T7" id="Seg_147" s="T6">plough-TR-INF</ta>
            <ta e="T8" id="Seg_148" s="T7">(s)he-PL.[NOM]</ta>
            <ta e="T9" id="Seg_149" s="T8">(s)he-ADES</ta>
            <ta e="T10" id="Seg_150" s="T9">work-3PL</ta>
            <ta e="T11" id="Seg_151" s="T10">(s)he.[NOM]</ta>
            <ta e="T12" id="Seg_152" s="T11">(s)he-PL-ACC</ta>
            <ta e="T13" id="Seg_153" s="T12">torture-3SG.O</ta>
            <ta e="T14" id="Seg_154" s="T13">(s)he-PL-GEN</ta>
            <ta e="T15" id="Seg_155" s="T14">money-GEN</ta>
            <ta e="T16" id="Seg_156" s="T15">small-ADVZ</ta>
            <ta e="T17" id="Seg_157" s="T16">give-HAB-3SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_158" s="T1">этот</ta>
            <ta e="T3" id="Seg_159" s="T2">человек.[NOM]</ta>
            <ta e="T4" id="Seg_160" s="T3">много</ta>
            <ta e="T5" id="Seg_161" s="T4">человек-PL.[NOM]</ta>
            <ta e="T6" id="Seg_162" s="T5">иметь-INFER-3SG.O</ta>
            <ta e="T7" id="Seg_163" s="T6">пахать-TR-INF</ta>
            <ta e="T8" id="Seg_164" s="T7">он(а)-PL.[NOM]</ta>
            <ta e="T9" id="Seg_165" s="T8">он(а)-ADES</ta>
            <ta e="T10" id="Seg_166" s="T9">работать-3PL</ta>
            <ta e="T11" id="Seg_167" s="T10">он(а).[NOM]</ta>
            <ta e="T12" id="Seg_168" s="T11">он(а)-PL-ACC</ta>
            <ta e="T13" id="Seg_169" s="T12">мучить-3SG.O</ta>
            <ta e="T14" id="Seg_170" s="T13">он(а)-PL-GEN</ta>
            <ta e="T15" id="Seg_171" s="T14">деньги-GEN</ta>
            <ta e="T16" id="Seg_172" s="T15">маленький-ADVZ</ta>
            <ta e="T17" id="Seg_173" s="T16">дать-HAB-3SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_174" s="T1">dem</ta>
            <ta e="T3" id="Seg_175" s="T2">n.[n:case]</ta>
            <ta e="T4" id="Seg_176" s="T3">quant</ta>
            <ta e="T5" id="Seg_177" s="T4">n-n:num.[n:case]</ta>
            <ta e="T6" id="Seg_178" s="T5">v-v:mood-v:pn</ta>
            <ta e="T7" id="Seg_179" s="T6">v-v&gt;v-v:inf</ta>
            <ta e="T8" id="Seg_180" s="T7">pers-n:num.[n:case]</ta>
            <ta e="T9" id="Seg_181" s="T8">pers-n:case</ta>
            <ta e="T10" id="Seg_182" s="T9">v-v:pn</ta>
            <ta e="T11" id="Seg_183" s="T10">pers.[n:case]</ta>
            <ta e="T12" id="Seg_184" s="T11">pers-n:num-n:case</ta>
            <ta e="T13" id="Seg_185" s="T12">v-v:pn</ta>
            <ta e="T14" id="Seg_186" s="T13">pers-n:num-n:case</ta>
            <ta e="T15" id="Seg_187" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_188" s="T15">adj-adj&gt;adv</ta>
            <ta e="T17" id="Seg_189" s="T16">v-v&gt;v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_190" s="T1">dem</ta>
            <ta e="T3" id="Seg_191" s="T2">n</ta>
            <ta e="T4" id="Seg_192" s="T3">quant</ta>
            <ta e="T5" id="Seg_193" s="T4">n</ta>
            <ta e="T6" id="Seg_194" s="T5">v</ta>
            <ta e="T7" id="Seg_195" s="T6">v</ta>
            <ta e="T8" id="Seg_196" s="T7">pers</ta>
            <ta e="T9" id="Seg_197" s="T8">pers</ta>
            <ta e="T10" id="Seg_198" s="T9">v</ta>
            <ta e="T11" id="Seg_199" s="T10">pers</ta>
            <ta e="T12" id="Seg_200" s="T11">pers</ta>
            <ta e="T13" id="Seg_201" s="T12">v</ta>
            <ta e="T14" id="Seg_202" s="T13">pers</ta>
            <ta e="T15" id="Seg_203" s="T14">n</ta>
            <ta e="T16" id="Seg_204" s="T15">adv</ta>
            <ta e="T17" id="Seg_205" s="T16">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_206" s="T2">np.h:Poss</ta>
            <ta e="T5" id="Seg_207" s="T4">np.h:Th</ta>
            <ta e="T8" id="Seg_208" s="T7">pro.h:A</ta>
            <ta e="T9" id="Seg_209" s="T8">pro.h:L</ta>
            <ta e="T11" id="Seg_210" s="T10">pro.h:A</ta>
            <ta e="T12" id="Seg_211" s="T11">pro.h:P</ta>
            <ta e="T14" id="Seg_212" s="T13">pro.h:Poss</ta>
            <ta e="T15" id="Seg_213" s="T14">np:Th</ta>
            <ta e="T17" id="Seg_214" s="T16">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_215" s="T2">np.h:S</ta>
            <ta e="T5" id="Seg_216" s="T4">np.h:O</ta>
            <ta e="T6" id="Seg_217" s="T5">v:pred</ta>
            <ta e="T7" id="Seg_218" s="T6">s:purp</ta>
            <ta e="T8" id="Seg_219" s="T7">pro.h:S</ta>
            <ta e="T10" id="Seg_220" s="T9">v:pred</ta>
            <ta e="T11" id="Seg_221" s="T10">pro.h:S</ta>
            <ta e="T12" id="Seg_222" s="T11">pro.h:O</ta>
            <ta e="T13" id="Seg_223" s="T12">v:pred</ta>
            <ta e="T15" id="Seg_224" s="T14">np:O</ta>
            <ta e="T17" id="Seg_225" s="T16">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T7" id="Seg_226" s="T6">RUS:cult</ta>
            <ta e="T13" id="Seg_227" s="T12">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_228" s="T1">This man has got many people to plough.</ta>
            <ta e="T10" id="Seg_229" s="T7">They work for him.</ta>
            <ta e="T13" id="Seg_230" s="T10">He tortures them.</ta>
            <ta e="T17" id="Seg_231" s="T13">He gives them little money.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_232" s="T1">Dieser Mann hat viele Leute zum Pflügen.</ta>
            <ta e="T10" id="Seg_233" s="T7">Sie arbeiten bei ihm.</ta>
            <ta e="T13" id="Seg_234" s="T10">Er quält sie.</ta>
            <ta e="T17" id="Seg_235" s="T13">Er gibt ihnen ein wenig Geld.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_236" s="T1">Этот человек много людей держит, чтобы пахать.</ta>
            <ta e="T10" id="Seg_237" s="T7">Они у него работают.</ta>
            <ta e="T13" id="Seg_238" s="T10">Он их мучает.</ta>
            <ta e="T17" id="Seg_239" s="T13">Им он денег мало дает.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_240" s="T1">этот человек много людей держит пахать</ta>
            <ta e="T10" id="Seg_241" s="T7">они у него работают</ta>
            <ta e="T13" id="Seg_242" s="T10">он их мучает</ta>
            <ta e="T17" id="Seg_243" s="T13">он денег мало дает</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T17" id="Seg_244" s="T13">[BrM:] GEN?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
