<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_SelkupDolls_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_SelkupDolls_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">24</ud-information>
            <ud-information attribute-name="# HIAT:w">19</ud-information>
            <ud-information attribute-name="# e">19</ud-information>
            <ud-information attribute-name="# HIAT:u">5</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T20" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Süsögula</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">ugon</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">nekalam</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">mekuzattə</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Tebla</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">nekala</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">qulupbukuzattə</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">i</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">paqtälbukuzattə</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_35" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">Pöjlamdətə</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">panaːlguzattə</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_44" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">Patom</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">aːr</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">pöjlam</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">sətkuzattə</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_59" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">Na</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">nʼekalam</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">ketkuzattə</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">lozuwlʼe</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T20" id="Seg_73" n="sc" s="T1">
               <ts e="T2" id="Seg_75" n="e" s="T1">Süsögula </ts>
               <ts e="T3" id="Seg_77" n="e" s="T2">ugon </ts>
               <ts e="T4" id="Seg_79" n="e" s="T3">nekalam </ts>
               <ts e="T5" id="Seg_81" n="e" s="T4">mekuzattə. </ts>
               <ts e="T6" id="Seg_83" n="e" s="T5">Tebla </ts>
               <ts e="T7" id="Seg_85" n="e" s="T6">nekala </ts>
               <ts e="T8" id="Seg_87" n="e" s="T7">qulupbukuzattə </ts>
               <ts e="T9" id="Seg_89" n="e" s="T8">i </ts>
               <ts e="T10" id="Seg_91" n="e" s="T9">paqtälbukuzattə. </ts>
               <ts e="T11" id="Seg_93" n="e" s="T10">Pöjlamdətə </ts>
               <ts e="T12" id="Seg_95" n="e" s="T11">panaːlguzattə. </ts>
               <ts e="T13" id="Seg_97" n="e" s="T12">Patom </ts>
               <ts e="T14" id="Seg_99" n="e" s="T13">aːr </ts>
               <ts e="T15" id="Seg_101" n="e" s="T14">pöjlam </ts>
               <ts e="T16" id="Seg_103" n="e" s="T15">sətkuzattə. </ts>
               <ts e="T17" id="Seg_105" n="e" s="T16">Na </ts>
               <ts e="T18" id="Seg_107" n="e" s="T17">nʼekalam </ts>
               <ts e="T19" id="Seg_109" n="e" s="T18">ketkuzattə </ts>
               <ts e="T20" id="Seg_111" n="e" s="T19">lozuwlʼe. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_112" s="T1">PVD_1964_SelkupDolls_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_113" s="T5">PVD_1964_SelkupDolls_nar.002 (001.002)</ta>
            <ta e="T12" id="Seg_114" s="T10">PVD_1964_SelkupDolls_nar.003 (001.003)</ta>
            <ta e="T16" id="Seg_115" s="T12">PVD_1964_SelkupDolls_nar.004 (001.004)</ta>
            <ta e="T20" id="Seg_116" s="T16">PVD_1964_SelkupDolls_nar.005 (001.005)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_117" s="T1">сӱ′сӧгула у′гон ′некалам ′мекузаттъ.</ta>
            <ta e="T10" id="Seg_118" s="T5">теб′ла ′некала ′kулупбуку‵заттъ и паk′тӓлбукузаттъ.</ta>
            <ta e="T12" id="Seg_119" s="T10">пӧй′ламдътъ па′на̄лгу‵заттъ.</ta>
            <ta e="T16" id="Seg_120" s="T12">патом ′а̄р пӧй′лам ′съ̊ткузаттъ.</ta>
            <ta e="T20" id="Seg_121" s="T16">на ′нʼекалам кетку′заттъ ′лозуwлʼе.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_122" s="T1">süsögula ugon nekalam mekuzattə.</ta>
            <ta e="T10" id="Seg_123" s="T5">tebla nekala qulupbukuzattə i paqtälbukuzattə.</ta>
            <ta e="T12" id="Seg_124" s="T10">pöjlamdətə panaːlguzattə.</ta>
            <ta e="T16" id="Seg_125" s="T12">patom aːr pöjlam sətkuzattə.</ta>
            <ta e="T20" id="Seg_126" s="T16">na nʼekalam ketkuzattə lozuwlʼe.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_127" s="T1">Süsögula ugon nekalam mekuzattə. </ta>
            <ta e="T10" id="Seg_128" s="T5">Tebla nekala qulupbukuzattə i paqtälbukuzattə. </ta>
            <ta e="T12" id="Seg_129" s="T10">Pöjlamdətə panaːlguzattə. </ta>
            <ta e="T16" id="Seg_130" s="T12">Patom aːr pöjlam sətkuzattə. </ta>
            <ta e="T20" id="Seg_131" s="T16">Na nʼekalam ketkuzattə lozuwlʼe. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_132" s="T1">süsögu-la</ta>
            <ta e="T3" id="Seg_133" s="T2">ugon</ta>
            <ta e="T4" id="Seg_134" s="T3">neka-la-m</ta>
            <ta e="T5" id="Seg_135" s="T4">me-ku-za-ttə</ta>
            <ta e="T6" id="Seg_136" s="T5">teb-la</ta>
            <ta e="T7" id="Seg_137" s="T6">neka-la</ta>
            <ta e="T8" id="Seg_138" s="T7">qulupbu-ku-za-ttə</ta>
            <ta e="T9" id="Seg_139" s="T8">i</ta>
            <ta e="T10" id="Seg_140" s="T9">paqtäl-bu-ku-za-ttə</ta>
            <ta e="T11" id="Seg_141" s="T10">pöj-la-m-dətə</ta>
            <ta e="T12" id="Seg_142" s="T11">panaːl-gu-za-ttə</ta>
            <ta e="T13" id="Seg_143" s="T12">patom</ta>
            <ta e="T14" id="Seg_144" s="T13">aːr</ta>
            <ta e="T15" id="Seg_145" s="T14">pöj-la-m</ta>
            <ta e="T16" id="Seg_146" s="T15">sət-ku-za-ttə</ta>
            <ta e="T17" id="Seg_147" s="T16">na</ta>
            <ta e="T18" id="Seg_148" s="T17">nʼeka-la-m</ta>
            <ta e="T19" id="Seg_149" s="T18">ket-ku-za-ttə</ta>
            <ta e="T20" id="Seg_150" s="T19">lozu-w-lʼe</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_151" s="T1">süsögum-la</ta>
            <ta e="T3" id="Seg_152" s="T2">ugon</ta>
            <ta e="T4" id="Seg_153" s="T3">nekka-la-m</ta>
            <ta e="T5" id="Seg_154" s="T4">meː-ku-sɨ-tɨn</ta>
            <ta e="T6" id="Seg_155" s="T5">täp-la</ta>
            <ta e="T7" id="Seg_156" s="T6">nekka-la</ta>
            <ta e="T8" id="Seg_157" s="T7">kulubu-ku-sɨ-tɨn</ta>
            <ta e="T9" id="Seg_158" s="T8">i</ta>
            <ta e="T10" id="Seg_159" s="T9">paqtɛl-mbɨ-ku-sɨ-tɨn</ta>
            <ta e="T11" id="Seg_160" s="T10">pöw-la-m-tɨn</ta>
            <ta e="T12" id="Seg_161" s="T11">panal-ku-sɨ-tɨn</ta>
            <ta e="T13" id="Seg_162" s="T12">patom</ta>
            <ta e="T14" id="Seg_163" s="T13">aːr</ta>
            <ta e="T15" id="Seg_164" s="T14">pöw-la-m</ta>
            <ta e="T16" id="Seg_165" s="T15">süt-ku-sɨ-tɨn</ta>
            <ta e="T17" id="Seg_166" s="T16">na</ta>
            <ta e="T18" id="Seg_167" s="T17">nekka-la-m</ta>
            <ta e="T19" id="Seg_168" s="T18">ket-ku-sɨ-tɨn</ta>
            <ta e="T20" id="Seg_169" s="T19">loːzɨ-m-le</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_170" s="T1">Selkup-PL.[NOM]</ta>
            <ta e="T3" id="Seg_171" s="T2">earlier</ta>
            <ta e="T4" id="Seg_172" s="T3">doll-PL-ACC</ta>
            <ta e="T5" id="Seg_173" s="T4">do-HAB-PST-3PL</ta>
            <ta e="T6" id="Seg_174" s="T5">(s)he-PL.[NOM]</ta>
            <ta e="T7" id="Seg_175" s="T6">doll-PL.[NOM]</ta>
            <ta e="T8" id="Seg_176" s="T7">speak-HAB-PST-3PL</ta>
            <ta e="T9" id="Seg_177" s="T8">and</ta>
            <ta e="T10" id="Seg_178" s="T9">dance-DUR-HAB-PST-3PL</ta>
            <ta e="T11" id="Seg_179" s="T10">boots-PL-ACC-3PL</ta>
            <ta e="T12" id="Seg_180" s="T11">damage-HAB-PST-3PL</ta>
            <ta e="T13" id="Seg_181" s="T12">then</ta>
            <ta e="T14" id="Seg_182" s="T13">other</ta>
            <ta e="T15" id="Seg_183" s="T14">boots-PL-ACC</ta>
            <ta e="T16" id="Seg_184" s="T15">sew-HAB-PST-3PL</ta>
            <ta e="T17" id="Seg_185" s="T16">this</ta>
            <ta e="T18" id="Seg_186" s="T17">doll-PL-ACC</ta>
            <ta e="T19" id="Seg_187" s="T18">say-HAB-PST-3PL</ta>
            <ta e="T20" id="Seg_188" s="T19">idol-TRL-CVB</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_189" s="T1">селькуп-PL.[NOM]</ta>
            <ta e="T3" id="Seg_190" s="T2">раньше</ta>
            <ta e="T4" id="Seg_191" s="T3">кукла-PL-ACC</ta>
            <ta e="T5" id="Seg_192" s="T4">сделать-HAB-PST-3PL</ta>
            <ta e="T6" id="Seg_193" s="T5">он(а)-PL.[NOM]</ta>
            <ta e="T7" id="Seg_194" s="T6">кукла-PL.[NOM]</ta>
            <ta e="T8" id="Seg_195" s="T7">говорить-HAB-PST-3PL</ta>
            <ta e="T9" id="Seg_196" s="T8">и</ta>
            <ta e="T10" id="Seg_197" s="T9">потанцевать-DUR-HAB-PST-3PL</ta>
            <ta e="T11" id="Seg_198" s="T10">чарки-PL-ACC-3PL</ta>
            <ta e="T12" id="Seg_199" s="T11">испортить-HAB-PST-3PL</ta>
            <ta e="T13" id="Seg_200" s="T12">потом</ta>
            <ta e="T14" id="Seg_201" s="T13">другой</ta>
            <ta e="T15" id="Seg_202" s="T14">чарки-PL-ACC</ta>
            <ta e="T16" id="Seg_203" s="T15">сшить-HAB-PST-3PL</ta>
            <ta e="T17" id="Seg_204" s="T16">этот</ta>
            <ta e="T18" id="Seg_205" s="T17">кукла-PL-ACC</ta>
            <ta e="T19" id="Seg_206" s="T18">сказать-HAB-PST-3PL</ta>
            <ta e="T20" id="Seg_207" s="T19">божок-TRL-CVB</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_208" s="T1">n-n:num.[n:case]</ta>
            <ta e="T3" id="Seg_209" s="T2">adv</ta>
            <ta e="T4" id="Seg_210" s="T3">n-n:num-n:case</ta>
            <ta e="T5" id="Seg_211" s="T4">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_212" s="T5">pers-n:num.[n:case]</ta>
            <ta e="T7" id="Seg_213" s="T6">n-n:num.[n:case]</ta>
            <ta e="T8" id="Seg_214" s="T7">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_215" s="T8">conj</ta>
            <ta e="T10" id="Seg_216" s="T9">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_217" s="T10">n-n:num-n:case-n:poss</ta>
            <ta e="T12" id="Seg_218" s="T11">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_219" s="T12">adv</ta>
            <ta e="T14" id="Seg_220" s="T13">adj</ta>
            <ta e="T15" id="Seg_221" s="T14">n-n:num-n:case</ta>
            <ta e="T16" id="Seg_222" s="T15">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_223" s="T16">dem</ta>
            <ta e="T18" id="Seg_224" s="T17">n-n:num-n:case</ta>
            <ta e="T19" id="Seg_225" s="T18">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_226" s="T19">n-n&gt;v-v&gt;adv</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_227" s="T1">n</ta>
            <ta e="T3" id="Seg_228" s="T2">adv</ta>
            <ta e="T4" id="Seg_229" s="T3">n</ta>
            <ta e="T5" id="Seg_230" s="T4">v</ta>
            <ta e="T6" id="Seg_231" s="T5">pers</ta>
            <ta e="T7" id="Seg_232" s="T6">n</ta>
            <ta e="T8" id="Seg_233" s="T7">v</ta>
            <ta e="T9" id="Seg_234" s="T8">conj</ta>
            <ta e="T10" id="Seg_235" s="T9">v</ta>
            <ta e="T11" id="Seg_236" s="T10">n</ta>
            <ta e="T12" id="Seg_237" s="T11">v</ta>
            <ta e="T13" id="Seg_238" s="T12">adv</ta>
            <ta e="T14" id="Seg_239" s="T13">adj</ta>
            <ta e="T15" id="Seg_240" s="T14">n</ta>
            <ta e="T16" id="Seg_241" s="T15">v</ta>
            <ta e="T17" id="Seg_242" s="T16">dem</ta>
            <ta e="T18" id="Seg_243" s="T17">n</ta>
            <ta e="T19" id="Seg_244" s="T18">v</ta>
            <ta e="T20" id="Seg_245" s="T19">adv</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_246" s="T1">np.h:A</ta>
            <ta e="T3" id="Seg_247" s="T2">adv:Time</ta>
            <ta e="T4" id="Seg_248" s="T3">np.h:P</ta>
            <ta e="T7" id="Seg_249" s="T6">np.h:A</ta>
            <ta e="T10" id="Seg_250" s="T9">0.3.h:A</ta>
            <ta e="T11" id="Seg_251" s="T10">np:P</ta>
            <ta e="T12" id="Seg_252" s="T11">0.3.h:A</ta>
            <ta e="T13" id="Seg_253" s="T12">adv:Time</ta>
            <ta e="T15" id="Seg_254" s="T14">np:P</ta>
            <ta e="T16" id="Seg_255" s="T15">0.3.h:A</ta>
            <ta e="T18" id="Seg_256" s="T17">np.h:Th</ta>
            <ta e="T19" id="Seg_257" s="T18">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_258" s="T1">np.h:S</ta>
            <ta e="T4" id="Seg_259" s="T3">np.h:O</ta>
            <ta e="T5" id="Seg_260" s="T4">v:pred</ta>
            <ta e="T7" id="Seg_261" s="T6">np.h:S</ta>
            <ta e="T8" id="Seg_262" s="T7">v:pred</ta>
            <ta e="T10" id="Seg_263" s="T9">0.3.h:S v:pred</ta>
            <ta e="T11" id="Seg_264" s="T10">np:O</ta>
            <ta e="T12" id="Seg_265" s="T11">0.3.h:S v:pred</ta>
            <ta e="T15" id="Seg_266" s="T14">np:O</ta>
            <ta e="T16" id="Seg_267" s="T15">0.3.h:S v:pred</ta>
            <ta e="T18" id="Seg_268" s="T17">np.h:O</ta>
            <ta e="T19" id="Seg_269" s="T18">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T9" id="Seg_270" s="T8">RUS:gram</ta>
            <ta e="T13" id="Seg_271" s="T12">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_272" s="T1">Earlier Selkups used to make dolls.</ta>
            <ta e="T10" id="Seg_273" s="T5">These dolls used to talk and to dance.</ta>
            <ta e="T12" id="Seg_274" s="T10">They wore their boots out.</ta>
            <ta e="T16" id="Seg_275" s="T12">Then [they] sewed new boots.</ta>
            <ta e="T20" id="Seg_276" s="T16">These dolls were called 'lozy'.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_277" s="T1">Die Selkupen machten früher Puppen.</ta>
            <ta e="T10" id="Seg_278" s="T5">Diese Puppen sprachen und tanzten.</ta>
            <ta e="T12" id="Seg_279" s="T10">Sie liefen ihre Schuhe durch.</ta>
            <ta e="T16" id="Seg_280" s="T12">Dann nähten [sie ihnen] neue Schuhe.</ta>
            <ta e="T20" id="Seg_281" s="T16">Diese Puppen wurden 'lozy' genannt.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_282" s="T1">Селькупы раньше кукол делали.</ta>
            <ta e="T10" id="Seg_283" s="T5">Они, эти куклы, разговаривали и плясали.</ta>
            <ta e="T12" id="Seg_284" s="T10">Чарки свои они снашивали.</ta>
            <ta e="T16" id="Seg_285" s="T12">Потом другие чарки шили.</ta>
            <ta e="T20" id="Seg_286" s="T16">Этих кукол называли лозы.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_287" s="T1">остяки раньше куклу делали</ta>
            <ta e="T10" id="Seg_288" s="T5">они эти куклы разговаривали и плясали</ta>
            <ta e="T12" id="Seg_289" s="T10">чаркы они снашивали</ta>
            <ta e="T16" id="Seg_290" s="T12">потом другие чарки шьют</ta>
            <ta e="T20" id="Seg_291" s="T16">этих кукл называли лозы</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto">
            <ta e="T12" id="Seg_292" s="T10">подошвы все пропадают</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
