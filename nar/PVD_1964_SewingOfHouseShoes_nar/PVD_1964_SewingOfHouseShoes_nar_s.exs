<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_SewingOfHouseShoes_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_SewingOfHouseShoes_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">136</ud-information>
            <ud-information attribute-name="# HIAT:w">100</ud-information>
            <ud-information attribute-name="# e">100</ud-information>
            <ud-information attribute-name="# HIAT:u">20</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T101" id="Seg_0" n="sc" s="T1">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Angelina</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">Iwanowna</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">mekga</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">ügum</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">mistä</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_18" n="HIAT:ip">(</nts>
                  <ts e="T7" id="Seg_20" n="HIAT:w" s="T6">miss</ts>
                  <nts id="Seg_21" n="HIAT:ip">)</nts>
                  <nts id="Seg_22" n="HIAT:ip">.</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_25" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_27" n="HIAT:w" s="T7">A</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">man</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_33" n="HIAT:w" s="T9">kɨgɨzau</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_36" n="HIAT:w" s="T10">megu</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_39" n="HIAT:w" s="T11">onän</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_42" n="HIAT:w" s="T12">kɨbanädäkkanä</ts>
                  <nts id="Seg_43" n="HIAT:ip">.</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_46" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">A</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_51" n="HIAT:w" s="T14">patom</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_54" n="HIAT:w" s="T15">kɨgelau</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_57" n="HIAT:w" s="T16">megu</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_60" n="HIAT:w" s="T17">tapočkam</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_63" n="HIAT:w" s="T18">sutku</ts>
                  <nts id="Seg_64" n="HIAT:ip">.</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_67" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_69" n="HIAT:w" s="T19">Manan</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_72" n="HIAT:w" s="T20">sɨt</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_75" n="HIAT:w" s="T21">kɨba</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_78" n="HIAT:w" s="T22">nädän</ts>
                  <nts id="Seg_79" n="HIAT:ip">.</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_82" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_84" n="HIAT:w" s="T23">Okkɨr</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_87" n="HIAT:w" s="T24">nʼäɣätdə</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_90" n="HIAT:w" s="T25">mettʒal</ts>
                  <nts id="Seg_91" n="HIAT:ip">,</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_94" n="HIAT:w" s="T26">a</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_97" n="HIAT:w" s="T27">au</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_100" n="HIAT:w" s="T28">mana</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_102" n="HIAT:ip">(</nts>
                  <ts e="T30" id="Seg_104" n="HIAT:w" s="T29">au</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_107" n="HIAT:w" s="T30">männan</ts>
                  <nts id="Seg_108" n="HIAT:ip">)</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_111" n="HIAT:w" s="T31">tʼänʼgeǯan</ts>
                  <nts id="Seg_112" n="HIAT:ip">.</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_115" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_117" n="HIAT:w" s="T32">Man</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_120" n="HIAT:w" s="T33">tʼäran</ts>
                  <nts id="Seg_121" n="HIAT:ip">:</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_123" n="HIAT:ip">“</nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">Okkɨrɨm</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">ass</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_131" n="HIAT:w" s="T36">meǯau</ts>
                  <nts id="Seg_132" n="HIAT:ip">.</nts>
                  <nts id="Seg_133" n="HIAT:ip">”</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_136" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_138" n="HIAT:w" s="T37">Okkɨr</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_141" n="HIAT:w" s="T38">näjɣum</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_144" n="HIAT:w" s="T39">mekga</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_147" n="HIAT:w" s="T40">tʼärɨn</ts>
                  <nts id="Seg_148" n="HIAT:ip">:</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_150" n="HIAT:ip">“</nts>
                  <ts e="T42" id="Seg_152" n="HIAT:w" s="T41">Man</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_155" n="HIAT:w" s="T42">tʼekga</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_158" n="HIAT:w" s="T43">sʼutčau</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_161" n="HIAT:w" s="T44">tapočkam</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_164" n="HIAT:w" s="T45">na</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_167" n="HIAT:w" s="T46">ügeɣɨnto</ts>
                  <nts id="Seg_168" n="HIAT:ip">.</nts>
                  <nts id="Seg_169" n="HIAT:ip">”</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_172" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_174" n="HIAT:w" s="T47">A</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_177" n="HIAT:w" s="T48">man</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_180" n="HIAT:w" s="T49">täbnä</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_183" n="HIAT:w" s="T50">tʼäran</ts>
                  <nts id="Seg_184" n="HIAT:ip">:</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_186" n="HIAT:ip">“</nts>
                  <ts e="T52" id="Seg_188" n="HIAT:w" s="T51">Ɨlčɨt</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_191" n="HIAT:w" s="T52">tʼängwɨt</ts>
                  <nts id="Seg_192" n="HIAT:ip">.</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_195" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_197" n="HIAT:w" s="T53">Qardʼen</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_200" n="HIAT:w" s="T54">soɣɨnetǯan</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_203" n="HIAT:w" s="T55">Angelina</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_206" n="HIAT:w" s="T56">Iwanownane</ts>
                  <nts id="Seg_207" n="HIAT:ip">.</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_210" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_212" n="HIAT:w" s="T57">Täbnan</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_215" n="HIAT:w" s="T58">nʼüjbimat</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_218" n="HIAT:w" s="T59">jetda</ts>
                  <nts id="Seg_219" n="HIAT:ip">,</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_222" n="HIAT:w" s="T60">panalɨpbɨdʼi</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_225" n="HIAT:w" s="T61">täp</ts>
                  <nts id="Seg_226" n="HIAT:ip">.</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_229" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_231" n="HIAT:w" s="T62">Moʒətbɨtʼ</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_234" n="HIAT:w" s="T63">metǯɨt</ts>
                  <nts id="Seg_235" n="HIAT:ip">.</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_238" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_240" n="HIAT:w" s="T64">Patom</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_243" n="HIAT:w" s="T65">man</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_246" n="HIAT:w" s="T66">tʼekga</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_249" n="HIAT:w" s="T67">tatčau</ts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip">”</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_254" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_256" n="HIAT:w" s="T68">Ugon</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_259" n="HIAT:w" s="T69">man</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_262" n="HIAT:w" s="T70">passajpajanan</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_265" n="HIAT:w" s="T71">mašinat</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_268" n="HIAT:w" s="T72">jes</ts>
                  <nts id="Seg_269" n="HIAT:ip">.</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_272" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_274" n="HIAT:w" s="T73">Täp</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_277" n="HIAT:w" s="T74">sapoʒniknä</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_280" n="HIAT:w" s="T75">kɨːdan</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_283" n="HIAT:w" s="T76">mekubat</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_286" n="HIAT:w" s="T77">mašinam</ts>
                  <nts id="Seg_287" n="HIAT:ip">.</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_290" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_292" n="HIAT:w" s="T78">Täp</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_295" n="HIAT:w" s="T79">täbɨm</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_298" n="HIAT:w" s="T80">panalbɨt</ts>
                  <nts id="Seg_299" n="HIAT:ip">.</nts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_302" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_304" n="HIAT:w" s="T81">Me</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_307" n="HIAT:w" s="T82">patom</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_310" n="HIAT:w" s="T83">täbɨm</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_313" n="HIAT:w" s="T84">merɨnnaut</ts>
                  <nts id="Seg_314" n="HIAT:ip">,</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_317" n="HIAT:w" s="T85">ton</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_320" n="HIAT:w" s="T86">calkowatdə</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_323" n="HIAT:w" s="T87">merɨnnaft</ts>
                  <nts id="Seg_324" n="HIAT:ip">.</nts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_327" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_329" n="HIAT:w" s="T88">Patom</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_332" n="HIAT:w" s="T89">kɨgɨzaj</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_335" n="HIAT:w" s="T90">arə</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_338" n="HIAT:w" s="T91">mašinam</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_341" n="HIAT:w" s="T92">taugu</ts>
                  <nts id="Seg_342" n="HIAT:ip">.</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_345" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_347" n="HIAT:w" s="T93">Wajna</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_350" n="HIAT:w" s="T94">azutda</ts>
                  <nts id="Seg_351" n="HIAT:ip">.</nts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_354" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_356" n="HIAT:w" s="T95">Äraum</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_359" n="HIAT:w" s="T96">qwannat</ts>
                  <nts id="Seg_360" n="HIAT:ip">.</nts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_363" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_365" n="HIAT:w" s="T97">I</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_368" n="HIAT:w" s="T98">mašinam</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_371" n="HIAT:w" s="T99">ass</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_374" n="HIAT:w" s="T100">taow</ts>
                  <nts id="Seg_375" n="HIAT:ip">.</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T101" id="Seg_377" n="sc" s="T1">
               <ts e="T2" id="Seg_379" n="e" s="T1">Angelina </ts>
               <ts e="T3" id="Seg_381" n="e" s="T2">Iwanowna </ts>
               <ts e="T4" id="Seg_383" n="e" s="T3">mekga </ts>
               <ts e="T5" id="Seg_385" n="e" s="T4">ügum </ts>
               <ts e="T6" id="Seg_387" n="e" s="T5">mistä </ts>
               <ts e="T7" id="Seg_389" n="e" s="T6">(miss). </ts>
               <ts e="T8" id="Seg_391" n="e" s="T7">A </ts>
               <ts e="T9" id="Seg_393" n="e" s="T8">man </ts>
               <ts e="T10" id="Seg_395" n="e" s="T9">kɨgɨzau </ts>
               <ts e="T11" id="Seg_397" n="e" s="T10">megu </ts>
               <ts e="T12" id="Seg_399" n="e" s="T11">onän </ts>
               <ts e="T13" id="Seg_401" n="e" s="T12">kɨbanädäkkanä. </ts>
               <ts e="T14" id="Seg_403" n="e" s="T13">A </ts>
               <ts e="T15" id="Seg_405" n="e" s="T14">patom </ts>
               <ts e="T16" id="Seg_407" n="e" s="T15">kɨgelau </ts>
               <ts e="T17" id="Seg_409" n="e" s="T16">megu </ts>
               <ts e="T18" id="Seg_411" n="e" s="T17">tapočkam </ts>
               <ts e="T19" id="Seg_413" n="e" s="T18">sutku. </ts>
               <ts e="T20" id="Seg_415" n="e" s="T19">Manan </ts>
               <ts e="T21" id="Seg_417" n="e" s="T20">sɨt </ts>
               <ts e="T22" id="Seg_419" n="e" s="T21">kɨba </ts>
               <ts e="T23" id="Seg_421" n="e" s="T22">nädän. </ts>
               <ts e="T24" id="Seg_423" n="e" s="T23">Okkɨr </ts>
               <ts e="T25" id="Seg_425" n="e" s="T24">nʼäɣätdə </ts>
               <ts e="T26" id="Seg_427" n="e" s="T25">mettʒal, </ts>
               <ts e="T27" id="Seg_429" n="e" s="T26">a </ts>
               <ts e="T28" id="Seg_431" n="e" s="T27">au </ts>
               <ts e="T29" id="Seg_433" n="e" s="T28">mana </ts>
               <ts e="T30" id="Seg_435" n="e" s="T29">(au </ts>
               <ts e="T31" id="Seg_437" n="e" s="T30">männan) </ts>
               <ts e="T32" id="Seg_439" n="e" s="T31">tʼänʼgeǯan. </ts>
               <ts e="T33" id="Seg_441" n="e" s="T32">Man </ts>
               <ts e="T34" id="Seg_443" n="e" s="T33">tʼäran: </ts>
               <ts e="T35" id="Seg_445" n="e" s="T34">“Okkɨrɨm </ts>
               <ts e="T36" id="Seg_447" n="e" s="T35">ass </ts>
               <ts e="T37" id="Seg_449" n="e" s="T36">meǯau.” </ts>
               <ts e="T38" id="Seg_451" n="e" s="T37">Okkɨr </ts>
               <ts e="T39" id="Seg_453" n="e" s="T38">näjɣum </ts>
               <ts e="T40" id="Seg_455" n="e" s="T39">mekga </ts>
               <ts e="T41" id="Seg_457" n="e" s="T40">tʼärɨn: </ts>
               <ts e="T42" id="Seg_459" n="e" s="T41">“Man </ts>
               <ts e="T43" id="Seg_461" n="e" s="T42">tʼekga </ts>
               <ts e="T44" id="Seg_463" n="e" s="T43">sʼutčau </ts>
               <ts e="T45" id="Seg_465" n="e" s="T44">tapočkam </ts>
               <ts e="T46" id="Seg_467" n="e" s="T45">na </ts>
               <ts e="T47" id="Seg_469" n="e" s="T46">ügeɣɨnto.” </ts>
               <ts e="T48" id="Seg_471" n="e" s="T47">A </ts>
               <ts e="T49" id="Seg_473" n="e" s="T48">man </ts>
               <ts e="T50" id="Seg_475" n="e" s="T49">täbnä </ts>
               <ts e="T51" id="Seg_477" n="e" s="T50">tʼäran: </ts>
               <ts e="T52" id="Seg_479" n="e" s="T51">“Ɨlčɨt </ts>
               <ts e="T53" id="Seg_481" n="e" s="T52">tʼängwɨt. </ts>
               <ts e="T54" id="Seg_483" n="e" s="T53">Qardʼen </ts>
               <ts e="T55" id="Seg_485" n="e" s="T54">soɣɨnetǯan </ts>
               <ts e="T56" id="Seg_487" n="e" s="T55">Angelina </ts>
               <ts e="T57" id="Seg_489" n="e" s="T56">Iwanownane. </ts>
               <ts e="T58" id="Seg_491" n="e" s="T57">Täbnan </ts>
               <ts e="T59" id="Seg_493" n="e" s="T58">nʼüjbimat </ts>
               <ts e="T60" id="Seg_495" n="e" s="T59">jetda, </ts>
               <ts e="T61" id="Seg_497" n="e" s="T60">panalɨpbɨdʼi </ts>
               <ts e="T62" id="Seg_499" n="e" s="T61">täp. </ts>
               <ts e="T63" id="Seg_501" n="e" s="T62">Moʒətbɨtʼ </ts>
               <ts e="T64" id="Seg_503" n="e" s="T63">metǯɨt. </ts>
               <ts e="T65" id="Seg_505" n="e" s="T64">Patom </ts>
               <ts e="T66" id="Seg_507" n="e" s="T65">man </ts>
               <ts e="T67" id="Seg_509" n="e" s="T66">tʼekga </ts>
               <ts e="T68" id="Seg_511" n="e" s="T67">tatčau.” </ts>
               <ts e="T69" id="Seg_513" n="e" s="T68">Ugon </ts>
               <ts e="T70" id="Seg_515" n="e" s="T69">man </ts>
               <ts e="T71" id="Seg_517" n="e" s="T70">passajpajanan </ts>
               <ts e="T72" id="Seg_519" n="e" s="T71">mašinat </ts>
               <ts e="T73" id="Seg_521" n="e" s="T72">jes. </ts>
               <ts e="T74" id="Seg_523" n="e" s="T73">Täp </ts>
               <ts e="T75" id="Seg_525" n="e" s="T74">sapoʒniknä </ts>
               <ts e="T76" id="Seg_527" n="e" s="T75">kɨːdan </ts>
               <ts e="T77" id="Seg_529" n="e" s="T76">mekubat </ts>
               <ts e="T78" id="Seg_531" n="e" s="T77">mašinam. </ts>
               <ts e="T79" id="Seg_533" n="e" s="T78">Täp </ts>
               <ts e="T80" id="Seg_535" n="e" s="T79">täbɨm </ts>
               <ts e="T81" id="Seg_537" n="e" s="T80">panalbɨt. </ts>
               <ts e="T82" id="Seg_539" n="e" s="T81">Me </ts>
               <ts e="T83" id="Seg_541" n="e" s="T82">patom </ts>
               <ts e="T84" id="Seg_543" n="e" s="T83">täbɨm </ts>
               <ts e="T85" id="Seg_545" n="e" s="T84">merɨnnaut, </ts>
               <ts e="T86" id="Seg_547" n="e" s="T85">ton </ts>
               <ts e="T87" id="Seg_549" n="e" s="T86">calkowatdə </ts>
               <ts e="T88" id="Seg_551" n="e" s="T87">merɨnnaft. </ts>
               <ts e="T89" id="Seg_553" n="e" s="T88">Patom </ts>
               <ts e="T90" id="Seg_555" n="e" s="T89">kɨgɨzaj </ts>
               <ts e="T91" id="Seg_557" n="e" s="T90">arə </ts>
               <ts e="T92" id="Seg_559" n="e" s="T91">mašinam </ts>
               <ts e="T93" id="Seg_561" n="e" s="T92">taugu. </ts>
               <ts e="T94" id="Seg_563" n="e" s="T93">Wajna </ts>
               <ts e="T95" id="Seg_565" n="e" s="T94">azutda. </ts>
               <ts e="T96" id="Seg_567" n="e" s="T95">Äraum </ts>
               <ts e="T97" id="Seg_569" n="e" s="T96">qwannat. </ts>
               <ts e="T98" id="Seg_571" n="e" s="T97">I </ts>
               <ts e="T99" id="Seg_573" n="e" s="T98">mašinam </ts>
               <ts e="T100" id="Seg_575" n="e" s="T99">ass </ts>
               <ts e="T101" id="Seg_577" n="e" s="T100">taow. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_578" s="T1">PVD_1964_SewingOfHouseShoes_nar.001 (001.001)</ta>
            <ta e="T13" id="Seg_579" s="T7">PVD_1964_SewingOfHouseShoes_nar.002 (001.002)</ta>
            <ta e="T19" id="Seg_580" s="T13">PVD_1964_SewingOfHouseShoes_nar.003 (001.003)</ta>
            <ta e="T23" id="Seg_581" s="T19">PVD_1964_SewingOfHouseShoes_nar.004 (001.004)</ta>
            <ta e="T32" id="Seg_582" s="T23">PVD_1964_SewingOfHouseShoes_nar.005 (001.005)</ta>
            <ta e="T37" id="Seg_583" s="T32">PVD_1964_SewingOfHouseShoes_nar.006 (001.006)</ta>
            <ta e="T47" id="Seg_584" s="T37">PVD_1964_SewingOfHouseShoes_nar.007 (001.007)</ta>
            <ta e="T53" id="Seg_585" s="T47">PVD_1964_SewingOfHouseShoes_nar.008 (001.008)</ta>
            <ta e="T57" id="Seg_586" s="T53">PVD_1964_SewingOfHouseShoes_nar.009 (001.009)</ta>
            <ta e="T62" id="Seg_587" s="T57">PVD_1964_SewingOfHouseShoes_nar.010 (001.010)</ta>
            <ta e="T64" id="Seg_588" s="T62">PVD_1964_SewingOfHouseShoes_nar.011 (001.011)</ta>
            <ta e="T68" id="Seg_589" s="T64">PVD_1964_SewingOfHouseShoes_nar.012 (001.012)</ta>
            <ta e="T73" id="Seg_590" s="T68">PVD_1964_SewingOfHouseShoes_nar.013 (001.013)</ta>
            <ta e="T78" id="Seg_591" s="T73">PVD_1964_SewingOfHouseShoes_nar.014 (001.014)</ta>
            <ta e="T81" id="Seg_592" s="T78">PVD_1964_SewingOfHouseShoes_nar.015 (001.015)</ta>
            <ta e="T88" id="Seg_593" s="T81">PVD_1964_SewingOfHouseShoes_nar.016 (001.016)</ta>
            <ta e="T93" id="Seg_594" s="T88">PVD_1964_SewingOfHouseShoes_nar.017 (001.017)</ta>
            <ta e="T95" id="Seg_595" s="T93">PVD_1964_SewingOfHouseShoes_nar.018 (001.018)</ta>
            <ta e="T97" id="Seg_596" s="T95">PVD_1964_SewingOfHouseShoes_nar.019 (001.019)</ta>
            <ta e="T101" id="Seg_597" s="T97">PVD_1964_SewingOfHouseShoes_nar.020 (001.020)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T7" id="Seg_598" s="T1">Ангелина Ивановна ′мекга ′ӱгум ′мистӓ (′мисс).</ta>
            <ta e="T13" id="Seg_599" s="T7">а ман кыгы′зау ме′гу о′нӓн кы′банӓ дӓкканӓ.</ta>
            <ta e="T19" id="Seg_600" s="T13">а па′том кы′гелау̹ ме′гу ′тапочкам сут′ку.</ta>
            <ta e="T23" id="Seg_601" s="T19">ма′нан ′сы(ъ)т кы′ба нӓ‵дӓн.</ta>
            <ta e="T32" id="Seg_602" s="T23">о′ккыр ′нʼӓɣӓтдъ метт′жал, а ау мана (′ау мӓ′ннан) ′тʼӓнʼге‵джан.</ta>
            <ta e="T37" id="Seg_603" s="T32">ман тʼӓ′ран ′оккырым асс ме′джау.</ta>
            <ta e="T47" id="Seg_604" s="T37">о′ккыр нӓйɣум ′мекга тʼӓ′рын: ман ′тʼекга сʼут′тшау ′тапочкам на ӱ′геɣынто.</ta>
            <ta e="T53" id="Seg_605" s="T47">а ман тӓб′нӓ тʼӓ′ран: ыl′тшыт ′тʼӓнгвыт.</ta>
            <ta e="T57" id="Seg_606" s="T53">kар′дʼен соɣы′нетджан Ангелина Ивановнане(ӓ).</ta>
            <ta e="T62" id="Seg_607" s="T57">тӓб′нан ′нʼӱй б̂и′мат ′jетда, па′налыпбыдʼи тӓп.</ta>
            <ta e="T64" id="Seg_608" s="T62">′можът бытʼ ′мет′джыт.</ta>
            <ta e="T68" id="Seg_609" s="T64">па′том ман ′тʼекга тат′тшау.</ta>
            <ta e="T73" id="Seg_610" s="T68">у′гон ман па′ссай паjа′нан ма′шинат ′jес.</ta>
            <ta e="T78" id="Seg_611" s="T73">тӓп са′пожник(г)нӓ ′кы̄дан меку′бат ′машинам.</ta>
            <ta e="T81" id="Seg_612" s="T78">тӓп ′тӓбым па′налбыт.</ta>
            <ta e="T88" id="Seg_613" s="T81">ме па′том ′тӓбым ′мерыннаут, тон цал′коватдъ мерыннафт.</ta>
            <ta e="T93" id="Seg_614" s="T88">па′том кыгы′зай ′аръ ма′шинам ′таугу.</ta>
            <ta e="T95" id="Seg_615" s="T93">вай′на ′азутда.</ta>
            <ta e="T97" id="Seg_616" s="T95">′ӓраум kwа′ннат.</ta>
            <ta e="T101" id="Seg_617" s="T97">и ма′шинам асс та′оw.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T7" id="Seg_618" s="T1">Аngelina Иwanowna mekga ügum mistä (miss).</ta>
            <ta e="T13" id="Seg_619" s="T7">a man kɨgɨzau megu onän kɨbanä däkkanä.</ta>
            <ta e="T19" id="Seg_620" s="T13">a patom kɨgelau̹ megu tapočkam sutku.</ta>
            <ta e="T23" id="Seg_621" s="T19">manan sɨ(ə)t kɨba nädän.</ta>
            <ta e="T32" id="Seg_622" s="T23">okkɨr nʼäɣätdə mettʒal, a au mana (au männan) tʼänʼgeǯan.</ta>
            <ta e="T37" id="Seg_623" s="T32">man tʼäran okkɨrɨm ass meǯau.</ta>
            <ta e="T47" id="Seg_624" s="T37">okkɨr näjɣum mekga tʼärɨn: man tʼekga sʼuttšau tapočkam na ügeɣɨnto.</ta>
            <ta e="T53" id="Seg_625" s="T47">a man täbnä tʼäran: ɨltšɨt tʼängwɨt.</ta>
            <ta e="T57" id="Seg_626" s="T53">qardʼen soɣɨnetǯan Аngelina Иwanownane(ä).</ta>
            <ta e="T62" id="Seg_627" s="T57">täbnan nʼüj b̂imat jetda, panalɨpbɨdʼi täp.</ta>
            <ta e="T64" id="Seg_628" s="T62">moʒət bɨtʼ metǯɨt.</ta>
            <ta e="T68" id="Seg_629" s="T64">patom man tʼekga tattšau.</ta>
            <ta e="T73" id="Seg_630" s="T68">ugon man passaj pajanan mašinat jes.</ta>
            <ta e="T78" id="Seg_631" s="T73">täp sapoʒnik(g)nä kɨːdan mekubat mašinam.</ta>
            <ta e="T81" id="Seg_632" s="T78">täp täbɨm panalbɨt.</ta>
            <ta e="T88" id="Seg_633" s="T81">me patom täbɨm merɨnnaut, ton calkowatdə merɨnnaft.</ta>
            <ta e="T93" id="Seg_634" s="T88">patom kɨgɨzaj arə mašinam taugu.</ta>
            <ta e="T95" id="Seg_635" s="T93">wajna azutda.</ta>
            <ta e="T97" id="Seg_636" s="T95">äraum qwannat.</ta>
            <ta e="T101" id="Seg_637" s="T97">i mašinam ass taow.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_638" s="T1">Angelina Iwanowna mekga ügum mistä (miss). </ta>
            <ta e="T13" id="Seg_639" s="T7">A man kɨgɨzau megu onän kɨbanädäkkanä. </ta>
            <ta e="T19" id="Seg_640" s="T13">A patom kɨgelau megu tapočkam sutku. </ta>
            <ta e="T23" id="Seg_641" s="T19">Manan sɨt kɨba nädän. </ta>
            <ta e="T32" id="Seg_642" s="T23">Okkɨr nʼäɣätdə mettʒal, a au mana (au männan) tʼänʼgeǯan. </ta>
            <ta e="T37" id="Seg_643" s="T32">Man tʼäran: “Okkɨrɨm ass meǯau.” </ta>
            <ta e="T47" id="Seg_644" s="T37">Okkɨr näjɣum mekga tʼärɨn: “Man tʼekga sʼutčau tapočkam na ügeɣɨnto.” </ta>
            <ta e="T53" id="Seg_645" s="T47">A man täbnä tʼäran: “Ɨlčɨt tʼängwɨt. </ta>
            <ta e="T57" id="Seg_646" s="T53">Qardʼen soɣɨnetǯan Angelina Iwanownane. </ta>
            <ta e="T62" id="Seg_647" s="T57">Täbnan nʼüjbimat jetda, panalɨpbɨdʼi täp. </ta>
            <ta e="T64" id="Seg_648" s="T62">Moʒətbɨtʼ metǯɨt. </ta>
            <ta e="T68" id="Seg_649" s="T64">Patom man tʼekga tatčau.” </ta>
            <ta e="T73" id="Seg_650" s="T68">Ugon man passajpajanan mašinat jes. </ta>
            <ta e="T78" id="Seg_651" s="T73">Täp sapoʒniknä kɨːdan mekubat mašinam. </ta>
            <ta e="T81" id="Seg_652" s="T78">Täp täbɨm panalbɨt. </ta>
            <ta e="T88" id="Seg_653" s="T81">Me patom täbɨm merɨnnaut, ton calkowatdə merɨnnaft. </ta>
            <ta e="T93" id="Seg_654" s="T88">Patom kɨgɨzaj arə mašinam taugu. </ta>
            <ta e="T95" id="Seg_655" s="T93">Wajna azutda. </ta>
            <ta e="T97" id="Seg_656" s="T95">Äraum qwannat. </ta>
            <ta e="T101" id="Seg_657" s="T97">I mašinam ass taow. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_658" s="T1">Angelina</ta>
            <ta e="T3" id="Seg_659" s="T2">Iwanowna</ta>
            <ta e="T4" id="Seg_660" s="T3">mekga</ta>
            <ta e="T5" id="Seg_661" s="T4">ügu-m</ta>
            <ta e="T6" id="Seg_662" s="T5">mi-s-tä</ta>
            <ta e="T7" id="Seg_663" s="T6">mi-ss</ta>
            <ta e="T8" id="Seg_664" s="T7">a</ta>
            <ta e="T9" id="Seg_665" s="T8">man</ta>
            <ta e="T10" id="Seg_666" s="T9">kɨgɨ-za-u</ta>
            <ta e="T11" id="Seg_667" s="T10">me-gu</ta>
            <ta e="T12" id="Seg_668" s="T11">onän</ta>
            <ta e="T13" id="Seg_669" s="T12">kɨba-nädäk-ka-nä</ta>
            <ta e="T14" id="Seg_670" s="T13">a</ta>
            <ta e="T15" id="Seg_671" s="T14">patom</ta>
            <ta e="T16" id="Seg_672" s="T15">kɨge-l-a-u</ta>
            <ta e="T17" id="Seg_673" s="T16">me-gu</ta>
            <ta e="T18" id="Seg_674" s="T17">tapočka-m</ta>
            <ta e="T19" id="Seg_675" s="T18">sut-ku</ta>
            <ta e="T20" id="Seg_676" s="T19">ma-nan</ta>
            <ta e="T21" id="Seg_677" s="T20">sɨt</ta>
            <ta e="T22" id="Seg_678" s="T21">kɨba</ta>
            <ta e="T23" id="Seg_679" s="T22">nädän</ta>
            <ta e="T24" id="Seg_680" s="T23">okkɨr</ta>
            <ta e="T25" id="Seg_681" s="T24">nʼä-ɣätdə</ta>
            <ta e="T26" id="Seg_682" s="T25">me-ttʒa-l</ta>
            <ta e="T27" id="Seg_683" s="T26">a</ta>
            <ta e="T28" id="Seg_684" s="T27">au</ta>
            <ta e="T29" id="Seg_685" s="T28">ma-na</ta>
            <ta e="T30" id="Seg_686" s="T29">au</ta>
            <ta e="T31" id="Seg_687" s="T30">mä-nnan</ta>
            <ta e="T32" id="Seg_688" s="T31">tʼänʼg-eǯa-n</ta>
            <ta e="T33" id="Seg_689" s="T32">Man</ta>
            <ta e="T34" id="Seg_690" s="T33">tʼära-n</ta>
            <ta e="T35" id="Seg_691" s="T34">okkɨr-ɨ-m</ta>
            <ta e="T36" id="Seg_692" s="T35">ass</ta>
            <ta e="T37" id="Seg_693" s="T36">me-ǯa-u</ta>
            <ta e="T38" id="Seg_694" s="T37">okkɨr</ta>
            <ta e="T39" id="Seg_695" s="T38">nä-j-ɣum</ta>
            <ta e="T40" id="Seg_696" s="T39">mekga</ta>
            <ta e="T41" id="Seg_697" s="T40">tʼärɨ-n</ta>
            <ta e="T42" id="Seg_698" s="T41">Man</ta>
            <ta e="T43" id="Seg_699" s="T42">tʼekga</ta>
            <ta e="T44" id="Seg_700" s="T43">sʼut-ča-u</ta>
            <ta e="T45" id="Seg_701" s="T44">tapočka-m</ta>
            <ta e="T46" id="Seg_702" s="T45">na</ta>
            <ta e="T47" id="Seg_703" s="T46">üge-ɣɨnto</ta>
            <ta e="T48" id="Seg_704" s="T47">a</ta>
            <ta e="T49" id="Seg_705" s="T48">man</ta>
            <ta e="T50" id="Seg_706" s="T49">täb-nä</ta>
            <ta e="T51" id="Seg_707" s="T50">tʼära-n</ta>
            <ta e="T52" id="Seg_708" s="T51">ɨlčɨt</ta>
            <ta e="T53" id="Seg_709" s="T52">tʼän-gwɨ-t</ta>
            <ta e="T54" id="Seg_710" s="T53">qar-dʼe-n</ta>
            <ta e="T55" id="Seg_711" s="T54">soɣɨn-etǯa-n</ta>
            <ta e="T56" id="Seg_712" s="T55">Angelina</ta>
            <ta e="T57" id="Seg_713" s="T56">Iwanowna-ne</ta>
            <ta e="T58" id="Seg_714" s="T57">täb-nan</ta>
            <ta e="T59" id="Seg_715" s="T58">nʼüjbim-a-t</ta>
            <ta e="T60" id="Seg_716" s="T59">je-tda</ta>
            <ta e="T61" id="Seg_717" s="T60">panal-ɨ-pbɨdʼi</ta>
            <ta e="T62" id="Seg_718" s="T61">täp</ta>
            <ta e="T63" id="Seg_719" s="T62">moʒətbɨtʼ</ta>
            <ta e="T64" id="Seg_720" s="T63">me-tǯɨ-t</ta>
            <ta e="T65" id="Seg_721" s="T64">patom</ta>
            <ta e="T66" id="Seg_722" s="T65">man</ta>
            <ta e="T67" id="Seg_723" s="T66">tʼekga</ta>
            <ta e="T68" id="Seg_724" s="T67">tat-ča-u</ta>
            <ta e="T69" id="Seg_725" s="T68">ugon</ta>
            <ta e="T70" id="Seg_726" s="T69">man</ta>
            <ta e="T71" id="Seg_727" s="T70">passajpaja-nan</ta>
            <ta e="T72" id="Seg_728" s="T71">mašina-t</ta>
            <ta e="T73" id="Seg_729" s="T72">je-s</ta>
            <ta e="T74" id="Seg_730" s="T73">täp</ta>
            <ta e="T75" id="Seg_731" s="T74">sapoʒnik-nä</ta>
            <ta e="T76" id="Seg_732" s="T75">kɨːdan</ta>
            <ta e="T77" id="Seg_733" s="T76">me-ku-ba-t</ta>
            <ta e="T78" id="Seg_734" s="T77">mašina-m</ta>
            <ta e="T79" id="Seg_735" s="T78">täp</ta>
            <ta e="T80" id="Seg_736" s="T79">täb-ɨ-m</ta>
            <ta e="T81" id="Seg_737" s="T80">panal-bɨ-t</ta>
            <ta e="T82" id="Seg_738" s="T81">me</ta>
            <ta e="T83" id="Seg_739" s="T82">patom</ta>
            <ta e="T84" id="Seg_740" s="T83">täb-ɨ-m</ta>
            <ta e="T85" id="Seg_741" s="T84">merɨn-na-ut</ta>
            <ta e="T86" id="Seg_742" s="T85">ton</ta>
            <ta e="T87" id="Seg_743" s="T86">calkowa-tdə</ta>
            <ta e="T88" id="Seg_744" s="T87">merɨn-na-ft</ta>
            <ta e="T89" id="Seg_745" s="T88">patom</ta>
            <ta e="T90" id="Seg_746" s="T89">kɨgɨ-za-j</ta>
            <ta e="T91" id="Seg_747" s="T90">arə</ta>
            <ta e="T92" id="Seg_748" s="T91">mašina-m</ta>
            <ta e="T93" id="Seg_749" s="T92">tau-gu</ta>
            <ta e="T94" id="Seg_750" s="T93">wajna</ta>
            <ta e="T95" id="Seg_751" s="T94">azu-tda</ta>
            <ta e="T96" id="Seg_752" s="T95">ära-u-m</ta>
            <ta e="T97" id="Seg_753" s="T96">qwan-na-t</ta>
            <ta e="T98" id="Seg_754" s="T97">i</ta>
            <ta e="T99" id="Seg_755" s="T98">mašina-m</ta>
            <ta e="T100" id="Seg_756" s="T99">ass</ta>
            <ta e="T101" id="Seg_757" s="T100">ta-ow</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_758" s="T1">Angelina</ta>
            <ta e="T3" id="Seg_759" s="T2">Iwanɨwna</ta>
            <ta e="T4" id="Seg_760" s="T3">mekka</ta>
            <ta e="T5" id="Seg_761" s="T4">ügu-m</ta>
            <ta e="T6" id="Seg_762" s="T5">me-sɨ-t</ta>
            <ta e="T7" id="Seg_763" s="T6">me-sɨ</ta>
            <ta e="T8" id="Seg_764" s="T7">a</ta>
            <ta e="T9" id="Seg_765" s="T8">man</ta>
            <ta e="T10" id="Seg_766" s="T9">kɨgɨ-sɨ-w</ta>
            <ta e="T11" id="Seg_767" s="T10">me-gu</ta>
            <ta e="T12" id="Seg_768" s="T11">oneŋ</ta>
            <ta e="T13" id="Seg_769" s="T12">qɨba-nädek-ka-nä</ta>
            <ta e="T14" id="Seg_770" s="T13">a</ta>
            <ta e="T15" id="Seg_771" s="T14">patom</ta>
            <ta e="T16" id="Seg_772" s="T15">kɨgɨ-l-nɨ-w</ta>
            <ta e="T17" id="Seg_773" s="T16">me-gu</ta>
            <ta e="T18" id="Seg_774" s="T17">tapočka-m</ta>
            <ta e="T19" id="Seg_775" s="T18">süt-gu</ta>
            <ta e="T20" id="Seg_776" s="T19">man-nan</ta>
            <ta e="T21" id="Seg_777" s="T20">sədə</ta>
            <ta e="T22" id="Seg_778" s="T21">qɨba</ta>
            <ta e="T23" id="Seg_779" s="T22">nädek</ta>
            <ta e="T24" id="Seg_780" s="T23">okkɨr</ta>
            <ta e="T25" id="Seg_781" s="T24">ne-qɨntɨ</ta>
            <ta e="T26" id="Seg_782" s="T25">meː-enǯɨ-l</ta>
            <ta e="T27" id="Seg_783" s="T26">a</ta>
            <ta e="T28" id="Seg_784" s="T27">au</ta>
            <ta e="T29" id="Seg_785" s="T28">mɨ-nä</ta>
            <ta e="T30" id="Seg_786" s="T29">au</ta>
            <ta e="T31" id="Seg_787" s="T30">mɨ-nan</ta>
            <ta e="T32" id="Seg_788" s="T31">tʼäkku-enǯɨ-n</ta>
            <ta e="T33" id="Seg_789" s="T32">man</ta>
            <ta e="T34" id="Seg_790" s="T33">tʼärɨ-ŋ</ta>
            <ta e="T35" id="Seg_791" s="T34">okkɨr-ɨ-m</ta>
            <ta e="T36" id="Seg_792" s="T35">asa</ta>
            <ta e="T37" id="Seg_793" s="T36">meː-enǯɨ-w</ta>
            <ta e="T38" id="Seg_794" s="T37">okkɨr</ta>
            <ta e="T39" id="Seg_795" s="T38">ne-lʼ-qum</ta>
            <ta e="T40" id="Seg_796" s="T39">mekka</ta>
            <ta e="T41" id="Seg_797" s="T40">tʼärɨ-n</ta>
            <ta e="T42" id="Seg_798" s="T41">man</ta>
            <ta e="T43" id="Seg_799" s="T42">tekka</ta>
            <ta e="T44" id="Seg_800" s="T43">süt-enǯɨ-w</ta>
            <ta e="T45" id="Seg_801" s="T44">tapočka-m</ta>
            <ta e="T46" id="Seg_802" s="T45">na</ta>
            <ta e="T47" id="Seg_803" s="T46">ügu-qɨntɨ</ta>
            <ta e="T48" id="Seg_804" s="T47">a</ta>
            <ta e="T49" id="Seg_805" s="T48">man</ta>
            <ta e="T50" id="Seg_806" s="T49">täp-nä</ta>
            <ta e="T51" id="Seg_807" s="T50">tʼärɨ-ŋ</ta>
            <ta e="T52" id="Seg_808" s="T51">ɨlčit</ta>
            <ta e="T53" id="Seg_809" s="T52">tʼäkku-ku-ntɨ</ta>
            <ta e="T54" id="Seg_810" s="T53">qare-dʼel-n</ta>
            <ta e="T55" id="Seg_811" s="T54">sogundʼe-enǯɨ-ŋ</ta>
            <ta e="T56" id="Seg_812" s="T55">Angelina</ta>
            <ta e="T57" id="Seg_813" s="T56">Iwanɨwna-nä</ta>
            <ta e="T58" id="Seg_814" s="T57">täp-nan</ta>
            <ta e="T59" id="Seg_815" s="T58">nʼüjbim-ɨ-tə</ta>
            <ta e="T60" id="Seg_816" s="T59">eː-ntɨ</ta>
            <ta e="T61" id="Seg_817" s="T60">panal-ɨ-mbɨdi</ta>
            <ta e="T62" id="Seg_818" s="T61">täp</ta>
            <ta e="T63" id="Seg_819" s="T62">moʒət_bɨtʼ</ta>
            <ta e="T64" id="Seg_820" s="T63">me-enǯɨ-t</ta>
            <ta e="T65" id="Seg_821" s="T64">patom</ta>
            <ta e="T66" id="Seg_822" s="T65">man</ta>
            <ta e="T67" id="Seg_823" s="T66">tekka</ta>
            <ta e="T68" id="Seg_824" s="T67">tat-enǯɨ-w</ta>
            <ta e="T69" id="Seg_825" s="T68">ugon</ta>
            <ta e="T70" id="Seg_826" s="T69">man</ta>
            <ta e="T71" id="Seg_827" s="T70">passajpaja-nan</ta>
            <ta e="T72" id="Seg_828" s="T71">mašina-tə</ta>
            <ta e="T73" id="Seg_829" s="T72">eː-sɨ</ta>
            <ta e="T74" id="Seg_830" s="T73">täp</ta>
            <ta e="T75" id="Seg_831" s="T74">sapoʒnik-nä</ta>
            <ta e="T76" id="Seg_832" s="T75">qɨdan</ta>
            <ta e="T77" id="Seg_833" s="T76">me-ku-mbɨ-t</ta>
            <ta e="T78" id="Seg_834" s="T77">mašina-m</ta>
            <ta e="T79" id="Seg_835" s="T78">täp</ta>
            <ta e="T80" id="Seg_836" s="T79">täp-ɨ-m</ta>
            <ta e="T81" id="Seg_837" s="T80">panal-mbɨ-t</ta>
            <ta e="T82" id="Seg_838" s="T81">me</ta>
            <ta e="T83" id="Seg_839" s="T82">patom</ta>
            <ta e="T84" id="Seg_840" s="T83">täp-ɨ-m</ta>
            <ta e="T85" id="Seg_841" s="T84">merɨŋ-nɨ-un</ta>
            <ta e="T86" id="Seg_842" s="T85">ton</ta>
            <ta e="T87" id="Seg_843" s="T86">calkowa-ntə</ta>
            <ta e="T88" id="Seg_844" s="T87">merɨŋ-nɨ-un</ta>
            <ta e="T89" id="Seg_845" s="T88">patom</ta>
            <ta e="T90" id="Seg_846" s="T89">kɨgɨ-sɨ-j</ta>
            <ta e="T91" id="Seg_847" s="T90">aːr</ta>
            <ta e="T92" id="Seg_848" s="T91">mašina-m</ta>
            <ta e="T93" id="Seg_849" s="T92">taw-gu</ta>
            <ta e="T94" id="Seg_850" s="T93">wajna</ta>
            <ta e="T95" id="Seg_851" s="T94">azu-ntɨ</ta>
            <ta e="T96" id="Seg_852" s="T95">era-w-m</ta>
            <ta e="T97" id="Seg_853" s="T96">qwat-nɨ-tɨn</ta>
            <ta e="T98" id="Seg_854" s="T97">i</ta>
            <ta e="T99" id="Seg_855" s="T98">mašina-m</ta>
            <ta e="T100" id="Seg_856" s="T99">asa</ta>
            <ta e="T101" id="Seg_857" s="T100">taw-un</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_858" s="T1">Angelina.[NOM]</ta>
            <ta e="T3" id="Seg_859" s="T2">Ivanovna.[NOM]</ta>
            <ta e="T4" id="Seg_860" s="T3">I.ALL</ta>
            <ta e="T5" id="Seg_861" s="T4">hat-ACC</ta>
            <ta e="T6" id="Seg_862" s="T5">give-PST-3SG.O</ta>
            <ta e="T7" id="Seg_863" s="T6">give-PST.[3SG.S]</ta>
            <ta e="T8" id="Seg_864" s="T7">and</ta>
            <ta e="T9" id="Seg_865" s="T8">I.NOM</ta>
            <ta e="T10" id="Seg_866" s="T9">want-PST-1SG.O</ta>
            <ta e="T11" id="Seg_867" s="T10">give-INF</ta>
            <ta e="T12" id="Seg_868" s="T11">own.1SG</ta>
            <ta e="T13" id="Seg_869" s="T12">small-girl-DIM-ALL</ta>
            <ta e="T14" id="Seg_870" s="T13">and</ta>
            <ta e="T15" id="Seg_871" s="T14">then</ta>
            <ta e="T16" id="Seg_872" s="T15">want-INCH-CO-1SG.O</ta>
            <ta e="T17" id="Seg_873" s="T16">give-INF</ta>
            <ta e="T18" id="Seg_874" s="T17">slippers-ACC</ta>
            <ta e="T19" id="Seg_875" s="T18">sew-INF</ta>
            <ta e="T20" id="Seg_876" s="T19">I-ADES</ta>
            <ta e="T21" id="Seg_877" s="T20">two</ta>
            <ta e="T22" id="Seg_878" s="T21">small</ta>
            <ta e="T23" id="Seg_879" s="T22">girl.[NOM]</ta>
            <ta e="T24" id="Seg_880" s="T23">one</ta>
            <ta e="T25" id="Seg_881" s="T24">daughter-ALL.3SG</ta>
            <ta e="T26" id="Seg_882" s="T25">do-FUT-2SG.O</ta>
            <ta e="T27" id="Seg_883" s="T26">and</ta>
            <ta e="T28" id="Seg_884" s="T27">other</ta>
            <ta e="T29" id="Seg_885" s="T28">something-ALL</ta>
            <ta e="T30" id="Seg_886" s="T29">other</ta>
            <ta e="T31" id="Seg_887" s="T30">something-ADES</ta>
            <ta e="T32" id="Seg_888" s="T31">NEG.EX-FUT-3SG.S</ta>
            <ta e="T33" id="Seg_889" s="T32">I.NOM</ta>
            <ta e="T34" id="Seg_890" s="T33">say-1SG.S</ta>
            <ta e="T35" id="Seg_891" s="T34">one-EP-ACC</ta>
            <ta e="T36" id="Seg_892" s="T35">NEG</ta>
            <ta e="T37" id="Seg_893" s="T36">do-FUT-1SG.O</ta>
            <ta e="T38" id="Seg_894" s="T37">one</ta>
            <ta e="T39" id="Seg_895" s="T38">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T40" id="Seg_896" s="T39">I.ALL</ta>
            <ta e="T41" id="Seg_897" s="T40">say-3SG.S</ta>
            <ta e="T42" id="Seg_898" s="T41">I.NOM</ta>
            <ta e="T43" id="Seg_899" s="T42">you.ALL</ta>
            <ta e="T44" id="Seg_900" s="T43">sew-FUT-1SG.O</ta>
            <ta e="T45" id="Seg_901" s="T44">slippers-ACC</ta>
            <ta e="T46" id="Seg_902" s="T45">this</ta>
            <ta e="T47" id="Seg_903" s="T46">hat-EL.3SG</ta>
            <ta e="T48" id="Seg_904" s="T47">and</ta>
            <ta e="T49" id="Seg_905" s="T48">I.NOM</ta>
            <ta e="T50" id="Seg_906" s="T49">(s)he-ALL</ta>
            <ta e="T51" id="Seg_907" s="T50">say-1SG.S</ta>
            <ta e="T52" id="Seg_908" s="T51">sole.[NOM]</ta>
            <ta e="T53" id="Seg_909" s="T52">NEG.EX-HAB-INFER.[3SG.S]</ta>
            <ta e="T54" id="Seg_910" s="T53">morning-day-ADV.LOC</ta>
            <ta e="T55" id="Seg_911" s="T54">ask-FUT-1SG.S</ta>
            <ta e="T56" id="Seg_912" s="T55">Angelina.[NOM]</ta>
            <ta e="T57" id="Seg_913" s="T56">Ivanovna-ALL</ta>
            <ta e="T58" id="Seg_914" s="T57">(s)he-ADES</ta>
            <ta e="T59" id="Seg_915" s="T58">%felt.boot-EP.[NOM]-3SG</ta>
            <ta e="T60" id="Seg_916" s="T59">be-INFER.[3SG.S]</ta>
            <ta e="T61" id="Seg_917" s="T60">damage-EP-PTCP.PST</ta>
            <ta e="T62" id="Seg_918" s="T61">(s)he.[NOM]</ta>
            <ta e="T63" id="Seg_919" s="T62">maybe</ta>
            <ta e="T64" id="Seg_920" s="T63">give-FUT-3SG.O</ta>
            <ta e="T65" id="Seg_921" s="T64">then</ta>
            <ta e="T66" id="Seg_922" s="T65">I.NOM</ta>
            <ta e="T67" id="Seg_923" s="T66">you.ALL</ta>
            <ta e="T68" id="Seg_924" s="T67">bring-FUT-1SG.O</ta>
            <ta e="T69" id="Seg_925" s="T68">earlier</ta>
            <ta e="T70" id="Seg_926" s="T69">I.GEN</ta>
            <ta e="T71" id="Seg_927" s="T70">mother_in_law-ADES</ta>
            <ta e="T72" id="Seg_928" s="T71">sewing_machine.[NOM]-3SG</ta>
            <ta e="T73" id="Seg_929" s="T72">be-PST.[3SG.S]</ta>
            <ta e="T74" id="Seg_930" s="T73">(s)he.[NOM]</ta>
            <ta e="T75" id="Seg_931" s="T74">shoemaker-ALL</ta>
            <ta e="T76" id="Seg_932" s="T75">all.the.time</ta>
            <ta e="T77" id="Seg_933" s="T76">give-HAB-PST.NAR-3SG.O</ta>
            <ta e="T78" id="Seg_934" s="T77">sewing_machine-ACC</ta>
            <ta e="T79" id="Seg_935" s="T78">(s)he.[NOM]</ta>
            <ta e="T80" id="Seg_936" s="T79">(s)he-EP-ACC</ta>
            <ta e="T81" id="Seg_937" s="T80">damage-PST.NAR-3SG.O</ta>
            <ta e="T82" id="Seg_938" s="T81">we.[NOM]</ta>
            <ta e="T83" id="Seg_939" s="T82">then</ta>
            <ta e="T84" id="Seg_940" s="T83">(s)he-EP-ACC</ta>
            <ta e="T85" id="Seg_941" s="T84">sell-CO-1PL</ta>
            <ta e="T86" id="Seg_942" s="T85">hundred</ta>
            <ta e="T87" id="Seg_943" s="T86">ruble-ILL</ta>
            <ta e="T88" id="Seg_944" s="T87">sell-CO-1PL</ta>
            <ta e="T89" id="Seg_945" s="T88">then</ta>
            <ta e="T90" id="Seg_946" s="T89">want-PST-1DU</ta>
            <ta e="T91" id="Seg_947" s="T90">other</ta>
            <ta e="T92" id="Seg_948" s="T91">sewing_machine-ACC</ta>
            <ta e="T93" id="Seg_949" s="T92">buy-INF</ta>
            <ta e="T94" id="Seg_950" s="T93">war.[NOM]</ta>
            <ta e="T95" id="Seg_951" s="T94">become-INFER.[3SG.S]</ta>
            <ta e="T96" id="Seg_952" s="T95">husband-1SG-ACC</ta>
            <ta e="T97" id="Seg_953" s="T96">kill-CO-3PL</ta>
            <ta e="T98" id="Seg_954" s="T97">and</ta>
            <ta e="T99" id="Seg_955" s="T98">sewing_machine-ACC</ta>
            <ta e="T100" id="Seg_956" s="T99">NEG</ta>
            <ta e="T101" id="Seg_957" s="T100">buy-1PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_958" s="T1">Ангелина.[NOM]</ta>
            <ta e="T3" id="Seg_959" s="T2">Ивановна.[NOM]</ta>
            <ta e="T4" id="Seg_960" s="T3">я.ALL</ta>
            <ta e="T5" id="Seg_961" s="T4">шляпа-ACC</ta>
            <ta e="T6" id="Seg_962" s="T5">дать-PST-3SG.O</ta>
            <ta e="T7" id="Seg_963" s="T6">дать-PST.[3SG.S]</ta>
            <ta e="T8" id="Seg_964" s="T7">а</ta>
            <ta e="T9" id="Seg_965" s="T8">я.NOM</ta>
            <ta e="T10" id="Seg_966" s="T9">хотеть-PST-1SG.O</ta>
            <ta e="T11" id="Seg_967" s="T10">дать-INF</ta>
            <ta e="T12" id="Seg_968" s="T11">свой.1SG</ta>
            <ta e="T13" id="Seg_969" s="T12">маленький-девушка-DIM-ALL</ta>
            <ta e="T14" id="Seg_970" s="T13">а</ta>
            <ta e="T15" id="Seg_971" s="T14">потом</ta>
            <ta e="T16" id="Seg_972" s="T15">хотеть-INCH-CO-1SG.O</ta>
            <ta e="T17" id="Seg_973" s="T16">дать-INF</ta>
            <ta e="T18" id="Seg_974" s="T17">тапочки-ACC</ta>
            <ta e="T19" id="Seg_975" s="T18">сшить-INF</ta>
            <ta e="T20" id="Seg_976" s="T19">я-ADES</ta>
            <ta e="T21" id="Seg_977" s="T20">два</ta>
            <ta e="T22" id="Seg_978" s="T21">маленький</ta>
            <ta e="T23" id="Seg_979" s="T22">девушка.[NOM]</ta>
            <ta e="T24" id="Seg_980" s="T23">один</ta>
            <ta e="T25" id="Seg_981" s="T24">дочь-ALL.3SG</ta>
            <ta e="T26" id="Seg_982" s="T25">сделать-FUT-2SG.O</ta>
            <ta e="T27" id="Seg_983" s="T26">а</ta>
            <ta e="T28" id="Seg_984" s="T27">другой</ta>
            <ta e="T29" id="Seg_985" s="T28">нечто-ALL</ta>
            <ta e="T30" id="Seg_986" s="T29">другой</ta>
            <ta e="T31" id="Seg_987" s="T30">нечто-ADES</ta>
            <ta e="T32" id="Seg_988" s="T31">NEG.EX-FUT-3SG.S</ta>
            <ta e="T33" id="Seg_989" s="T32">я.NOM</ta>
            <ta e="T34" id="Seg_990" s="T33">сказать-1SG.S</ta>
            <ta e="T35" id="Seg_991" s="T34">один-EP-ACC</ta>
            <ta e="T36" id="Seg_992" s="T35">NEG</ta>
            <ta e="T37" id="Seg_993" s="T36">сделать-FUT-1SG.O</ta>
            <ta e="T38" id="Seg_994" s="T37">один</ta>
            <ta e="T39" id="Seg_995" s="T38">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T40" id="Seg_996" s="T39">я.ALL</ta>
            <ta e="T41" id="Seg_997" s="T40">сказать-3SG.S</ta>
            <ta e="T42" id="Seg_998" s="T41">я.NOM</ta>
            <ta e="T43" id="Seg_999" s="T42">ты.ALL</ta>
            <ta e="T44" id="Seg_1000" s="T43">сшить-FUT-1SG.O</ta>
            <ta e="T45" id="Seg_1001" s="T44">тапочки-ACC</ta>
            <ta e="T46" id="Seg_1002" s="T45">этот</ta>
            <ta e="T47" id="Seg_1003" s="T46">шляпа-EL.3SG</ta>
            <ta e="T48" id="Seg_1004" s="T47">а</ta>
            <ta e="T49" id="Seg_1005" s="T48">я.NOM</ta>
            <ta e="T50" id="Seg_1006" s="T49">он(а)-ALL</ta>
            <ta e="T51" id="Seg_1007" s="T50">сказать-1SG.S</ta>
            <ta e="T52" id="Seg_1008" s="T51">подошва.[NOM]</ta>
            <ta e="T53" id="Seg_1009" s="T52">NEG.EX-HAB-INFER.[3SG.S]</ta>
            <ta e="T54" id="Seg_1010" s="T53">утро-день-ADV.LOC</ta>
            <ta e="T55" id="Seg_1011" s="T54">спросить-FUT-1SG.S</ta>
            <ta e="T56" id="Seg_1012" s="T55">Ангелина.[NOM]</ta>
            <ta e="T57" id="Seg_1013" s="T56">Ивановна-ALL</ta>
            <ta e="T58" id="Seg_1014" s="T57">он(а)-ADES</ta>
            <ta e="T59" id="Seg_1015" s="T58">%валенок-EP.[NOM]-3SG</ta>
            <ta e="T60" id="Seg_1016" s="T59">быть-INFER.[3SG.S]</ta>
            <ta e="T61" id="Seg_1017" s="T60">испортить-EP-PTCP.PST</ta>
            <ta e="T62" id="Seg_1018" s="T61">он(а).[NOM]</ta>
            <ta e="T63" id="Seg_1019" s="T62">может.быть</ta>
            <ta e="T64" id="Seg_1020" s="T63">дать-FUT-3SG.O</ta>
            <ta e="T65" id="Seg_1021" s="T64">потом</ta>
            <ta e="T66" id="Seg_1022" s="T65">я.NOM</ta>
            <ta e="T67" id="Seg_1023" s="T66">ты.ALL</ta>
            <ta e="T68" id="Seg_1024" s="T67">принести-FUT-1SG.O</ta>
            <ta e="T69" id="Seg_1025" s="T68">раньше</ta>
            <ta e="T70" id="Seg_1026" s="T69">я.GEN</ta>
            <ta e="T71" id="Seg_1027" s="T70">свекровь-ADES</ta>
            <ta e="T72" id="Seg_1028" s="T71">швейная.машина.[NOM]-3SG</ta>
            <ta e="T73" id="Seg_1029" s="T72">быть-PST.[3SG.S]</ta>
            <ta e="T74" id="Seg_1030" s="T73">он(а).[NOM]</ta>
            <ta e="T75" id="Seg_1031" s="T74">сапожник-ALL</ta>
            <ta e="T76" id="Seg_1032" s="T75">все.время</ta>
            <ta e="T77" id="Seg_1033" s="T76">дать-HAB-PST.NAR-3SG.O</ta>
            <ta e="T78" id="Seg_1034" s="T77">швейная.машина-ACC</ta>
            <ta e="T79" id="Seg_1035" s="T78">он(а).[NOM]</ta>
            <ta e="T80" id="Seg_1036" s="T79">он(а)-EP-ACC</ta>
            <ta e="T81" id="Seg_1037" s="T80">испортить-PST.NAR-3SG.O</ta>
            <ta e="T82" id="Seg_1038" s="T81">мы.[NOM]</ta>
            <ta e="T83" id="Seg_1039" s="T82">потом</ta>
            <ta e="T84" id="Seg_1040" s="T83">он(а)-EP-ACC</ta>
            <ta e="T85" id="Seg_1041" s="T84">продать-CO-1PL</ta>
            <ta e="T86" id="Seg_1042" s="T85">сто</ta>
            <ta e="T87" id="Seg_1043" s="T86">целковый-ILL</ta>
            <ta e="T88" id="Seg_1044" s="T87">продать-CO-1PL</ta>
            <ta e="T89" id="Seg_1045" s="T88">потом</ta>
            <ta e="T90" id="Seg_1046" s="T89">хотеть-PST-1DU</ta>
            <ta e="T91" id="Seg_1047" s="T90">другой</ta>
            <ta e="T92" id="Seg_1048" s="T91">швейная.машина-ACC</ta>
            <ta e="T93" id="Seg_1049" s="T92">купить-INF</ta>
            <ta e="T94" id="Seg_1050" s="T93">война.[NOM]</ta>
            <ta e="T95" id="Seg_1051" s="T94">стать-INFER.[3SG.S]</ta>
            <ta e="T96" id="Seg_1052" s="T95">муж-1SG-ACC</ta>
            <ta e="T97" id="Seg_1053" s="T96">убить-CO-3PL</ta>
            <ta e="T98" id="Seg_1054" s="T97">и</ta>
            <ta e="T99" id="Seg_1055" s="T98">швейная.машина-ACC</ta>
            <ta e="T100" id="Seg_1056" s="T99">NEG</ta>
            <ta e="T101" id="Seg_1057" s="T100">купить-1PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1058" s="T1">nprop.[n:case]</ta>
            <ta e="T3" id="Seg_1059" s="T2">nprop.[n:case]</ta>
            <ta e="T4" id="Seg_1060" s="T3">pers</ta>
            <ta e="T5" id="Seg_1061" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_1062" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_1063" s="T6">v-v:tense.[v:pn]</ta>
            <ta e="T8" id="Seg_1064" s="T7">conj</ta>
            <ta e="T9" id="Seg_1065" s="T8">pers</ta>
            <ta e="T10" id="Seg_1066" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_1067" s="T10">v-v:inf</ta>
            <ta e="T12" id="Seg_1068" s="T11">emphpro</ta>
            <ta e="T13" id="Seg_1069" s="T12">adj-n-n&gt;n-n:case</ta>
            <ta e="T14" id="Seg_1070" s="T13">conj</ta>
            <ta e="T15" id="Seg_1071" s="T14">adv</ta>
            <ta e="T16" id="Seg_1072" s="T15">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T17" id="Seg_1073" s="T16">v-v:inf</ta>
            <ta e="T18" id="Seg_1074" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_1075" s="T18">v-v:inf</ta>
            <ta e="T20" id="Seg_1076" s="T19">pers-n:case</ta>
            <ta e="T21" id="Seg_1077" s="T20">num</ta>
            <ta e="T22" id="Seg_1078" s="T21">adj</ta>
            <ta e="T23" id="Seg_1079" s="T22">n.[n:case]</ta>
            <ta e="T24" id="Seg_1080" s="T23">num</ta>
            <ta e="T25" id="Seg_1081" s="T24">n-n:case.poss</ta>
            <ta e="T26" id="Seg_1082" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_1083" s="T26">conj</ta>
            <ta e="T28" id="Seg_1084" s="T27">adj</ta>
            <ta e="T29" id="Seg_1085" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_1086" s="T29">adj</ta>
            <ta e="T31" id="Seg_1087" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_1088" s="T31">v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_1089" s="T32">pers</ta>
            <ta e="T34" id="Seg_1090" s="T33">v-v:pn</ta>
            <ta e="T35" id="Seg_1091" s="T34">num-n:ins-n:case</ta>
            <ta e="T36" id="Seg_1092" s="T35">ptcl</ta>
            <ta e="T37" id="Seg_1093" s="T36">v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_1094" s="T37">num</ta>
            <ta e="T39" id="Seg_1095" s="T38">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T40" id="Seg_1096" s="T39">pers</ta>
            <ta e="T41" id="Seg_1097" s="T40">v-v:pn</ta>
            <ta e="T42" id="Seg_1098" s="T41">pers</ta>
            <ta e="T43" id="Seg_1099" s="T42">pers</ta>
            <ta e="T44" id="Seg_1100" s="T43">v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_1101" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_1102" s="T45">dem</ta>
            <ta e="T47" id="Seg_1103" s="T46">n-n:case.poss</ta>
            <ta e="T48" id="Seg_1104" s="T47">conj</ta>
            <ta e="T49" id="Seg_1105" s="T48">pers</ta>
            <ta e="T50" id="Seg_1106" s="T49">pers-n:case</ta>
            <ta e="T51" id="Seg_1107" s="T50">v-v:pn</ta>
            <ta e="T52" id="Seg_1108" s="T51">n.[n:case]</ta>
            <ta e="T53" id="Seg_1109" s="T52">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T54" id="Seg_1110" s="T53">n-n-adv:case</ta>
            <ta e="T55" id="Seg_1111" s="T54">v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_1112" s="T55">nprop.[n:case]</ta>
            <ta e="T57" id="Seg_1113" s="T56">nprop-n:case</ta>
            <ta e="T58" id="Seg_1114" s="T57">pers-n:case</ta>
            <ta e="T59" id="Seg_1115" s="T58">n-n:ins.[n:case]-n:poss</ta>
            <ta e="T60" id="Seg_1116" s="T59">v-v:mood.[v:pn]</ta>
            <ta e="T61" id="Seg_1117" s="T60">v-v:ins-v&gt;ptcp</ta>
            <ta e="T62" id="Seg_1118" s="T61">pers.[n:case]</ta>
            <ta e="T63" id="Seg_1119" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_1120" s="T63">v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_1121" s="T64">adv</ta>
            <ta e="T66" id="Seg_1122" s="T65">pers</ta>
            <ta e="T67" id="Seg_1123" s="T66">pers</ta>
            <ta e="T68" id="Seg_1124" s="T67">v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_1125" s="T68">adv</ta>
            <ta e="T70" id="Seg_1126" s="T69">pers</ta>
            <ta e="T71" id="Seg_1127" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_1128" s="T71">n.[n:case]-n:poss</ta>
            <ta e="T73" id="Seg_1129" s="T72">v-v:tense.[v:pn]</ta>
            <ta e="T74" id="Seg_1130" s="T73">pers.[n:case]</ta>
            <ta e="T75" id="Seg_1131" s="T74">n-n:case</ta>
            <ta e="T76" id="Seg_1132" s="T75">adv</ta>
            <ta e="T77" id="Seg_1133" s="T76">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_1134" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_1135" s="T78">pers.[n:case]</ta>
            <ta e="T80" id="Seg_1136" s="T79">pers-n:ins-n:case</ta>
            <ta e="T81" id="Seg_1137" s="T80">v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_1138" s="T81">pers.[n:case]</ta>
            <ta e="T83" id="Seg_1139" s="T82">adv</ta>
            <ta e="T84" id="Seg_1140" s="T83">pers-n:ins-n:case</ta>
            <ta e="T85" id="Seg_1141" s="T84">v-v:ins-v:pn</ta>
            <ta e="T86" id="Seg_1142" s="T85">num</ta>
            <ta e="T87" id="Seg_1143" s="T86">n-n:case</ta>
            <ta e="T88" id="Seg_1144" s="T87">v-v:ins-v:pn</ta>
            <ta e="T89" id="Seg_1145" s="T88">adv</ta>
            <ta e="T90" id="Seg_1146" s="T89">v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_1147" s="T90">adj</ta>
            <ta e="T92" id="Seg_1148" s="T91">n-n:case</ta>
            <ta e="T93" id="Seg_1149" s="T92">v-v:inf</ta>
            <ta e="T94" id="Seg_1150" s="T93">n.[n:case]</ta>
            <ta e="T95" id="Seg_1151" s="T94">v-v:mood.[v:pn]</ta>
            <ta e="T96" id="Seg_1152" s="T95">n-n:poss-n:case</ta>
            <ta e="T97" id="Seg_1153" s="T96">v-v:ins-v:pn</ta>
            <ta e="T98" id="Seg_1154" s="T97">conj</ta>
            <ta e="T99" id="Seg_1155" s="T98">n-n:case</ta>
            <ta e="T100" id="Seg_1156" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_1157" s="T100">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1158" s="T1">nprop</ta>
            <ta e="T3" id="Seg_1159" s="T2">nprop</ta>
            <ta e="T4" id="Seg_1160" s="T3">pers</ta>
            <ta e="T5" id="Seg_1161" s="T4">n</ta>
            <ta e="T6" id="Seg_1162" s="T5">v</ta>
            <ta e="T7" id="Seg_1163" s="T6">v</ta>
            <ta e="T8" id="Seg_1164" s="T7">conj</ta>
            <ta e="T9" id="Seg_1165" s="T8">pers</ta>
            <ta e="T10" id="Seg_1166" s="T9">v</ta>
            <ta e="T11" id="Seg_1167" s="T10">v</ta>
            <ta e="T12" id="Seg_1168" s="T11">emphpro</ta>
            <ta e="T13" id="Seg_1169" s="T12">n</ta>
            <ta e="T14" id="Seg_1170" s="T13">conj</ta>
            <ta e="T15" id="Seg_1171" s="T14">adv</ta>
            <ta e="T16" id="Seg_1172" s="T15">v</ta>
            <ta e="T17" id="Seg_1173" s="T16">v</ta>
            <ta e="T18" id="Seg_1174" s="T17">n</ta>
            <ta e="T19" id="Seg_1175" s="T18">v</ta>
            <ta e="T20" id="Seg_1176" s="T19">pers</ta>
            <ta e="T21" id="Seg_1177" s="T20">num</ta>
            <ta e="T22" id="Seg_1178" s="T21">adj</ta>
            <ta e="T23" id="Seg_1179" s="T22">n</ta>
            <ta e="T24" id="Seg_1180" s="T23">num</ta>
            <ta e="T25" id="Seg_1181" s="T24">n</ta>
            <ta e="T26" id="Seg_1182" s="T25">v</ta>
            <ta e="T27" id="Seg_1183" s="T26">conj</ta>
            <ta e="T28" id="Seg_1184" s="T27">adj</ta>
            <ta e="T29" id="Seg_1185" s="T28">n</ta>
            <ta e="T30" id="Seg_1186" s="T29">adj</ta>
            <ta e="T31" id="Seg_1187" s="T30">n</ta>
            <ta e="T32" id="Seg_1188" s="T31">v</ta>
            <ta e="T33" id="Seg_1189" s="T32">pers</ta>
            <ta e="T34" id="Seg_1190" s="T33">v</ta>
            <ta e="T35" id="Seg_1191" s="T34">num</ta>
            <ta e="T36" id="Seg_1192" s="T35">ptcl</ta>
            <ta e="T37" id="Seg_1193" s="T36">v</ta>
            <ta e="T38" id="Seg_1194" s="T37">num</ta>
            <ta e="T39" id="Seg_1195" s="T38">n</ta>
            <ta e="T40" id="Seg_1196" s="T39">pers</ta>
            <ta e="T41" id="Seg_1197" s="T40">v</ta>
            <ta e="T42" id="Seg_1198" s="T41">pers</ta>
            <ta e="T43" id="Seg_1199" s="T42">pers</ta>
            <ta e="T44" id="Seg_1200" s="T43">v</ta>
            <ta e="T45" id="Seg_1201" s="T44">n</ta>
            <ta e="T46" id="Seg_1202" s="T45">dem</ta>
            <ta e="T47" id="Seg_1203" s="T46">n</ta>
            <ta e="T48" id="Seg_1204" s="T47">conj</ta>
            <ta e="T49" id="Seg_1205" s="T48">pers</ta>
            <ta e="T50" id="Seg_1206" s="T49">pers</ta>
            <ta e="T51" id="Seg_1207" s="T50">v</ta>
            <ta e="T52" id="Seg_1208" s="T51">n</ta>
            <ta e="T53" id="Seg_1209" s="T52">v</ta>
            <ta e="T54" id="Seg_1210" s="T53">adv</ta>
            <ta e="T55" id="Seg_1211" s="T54">v</ta>
            <ta e="T56" id="Seg_1212" s="T55">nprop</ta>
            <ta e="T57" id="Seg_1213" s="T56">nprop</ta>
            <ta e="T58" id="Seg_1214" s="T57">pers</ta>
            <ta e="T59" id="Seg_1215" s="T58">n</ta>
            <ta e="T60" id="Seg_1216" s="T59">v</ta>
            <ta e="T61" id="Seg_1217" s="T60">ptcp</ta>
            <ta e="T62" id="Seg_1218" s="T61">pers</ta>
            <ta e="T63" id="Seg_1219" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_1220" s="T63">v</ta>
            <ta e="T65" id="Seg_1221" s="T64">adv</ta>
            <ta e="T66" id="Seg_1222" s="T65">pers</ta>
            <ta e="T67" id="Seg_1223" s="T66">pers</ta>
            <ta e="T68" id="Seg_1224" s="T67">v</ta>
            <ta e="T69" id="Seg_1225" s="T68">adv</ta>
            <ta e="T70" id="Seg_1226" s="T69">pers</ta>
            <ta e="T71" id="Seg_1227" s="T70">n</ta>
            <ta e="T72" id="Seg_1228" s="T71">n</ta>
            <ta e="T73" id="Seg_1229" s="T72">v</ta>
            <ta e="T74" id="Seg_1230" s="T73">pers</ta>
            <ta e="T75" id="Seg_1231" s="T74">n</ta>
            <ta e="T76" id="Seg_1232" s="T75">adv</ta>
            <ta e="T77" id="Seg_1233" s="T76">v</ta>
            <ta e="T78" id="Seg_1234" s="T77">n</ta>
            <ta e="T79" id="Seg_1235" s="T78">pers</ta>
            <ta e="T80" id="Seg_1236" s="T79">pers</ta>
            <ta e="T81" id="Seg_1237" s="T80">v</ta>
            <ta e="T82" id="Seg_1238" s="T81">pers</ta>
            <ta e="T83" id="Seg_1239" s="T82">adv</ta>
            <ta e="T84" id="Seg_1240" s="T83">pers</ta>
            <ta e="T85" id="Seg_1241" s="T84">v</ta>
            <ta e="T86" id="Seg_1242" s="T85">num</ta>
            <ta e="T87" id="Seg_1243" s="T86">n</ta>
            <ta e="T88" id="Seg_1244" s="T87">v</ta>
            <ta e="T89" id="Seg_1245" s="T88">adv</ta>
            <ta e="T90" id="Seg_1246" s="T89">v</ta>
            <ta e="T91" id="Seg_1247" s="T90">adj</ta>
            <ta e="T92" id="Seg_1248" s="T91">n</ta>
            <ta e="T93" id="Seg_1249" s="T92">v</ta>
            <ta e="T94" id="Seg_1250" s="T93">n</ta>
            <ta e="T95" id="Seg_1251" s="T94">v</ta>
            <ta e="T96" id="Seg_1252" s="T95">n</ta>
            <ta e="T97" id="Seg_1253" s="T96">v</ta>
            <ta e="T98" id="Seg_1254" s="T97">conj</ta>
            <ta e="T99" id="Seg_1255" s="T98">n</ta>
            <ta e="T100" id="Seg_1256" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_1257" s="T100">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1258" s="T1">np.h:A</ta>
            <ta e="T4" id="Seg_1259" s="T3">pro.h:R</ta>
            <ta e="T5" id="Seg_1260" s="T4">np:Th</ta>
            <ta e="T9" id="Seg_1261" s="T8">pro.h:E</ta>
            <ta e="T10" id="Seg_1262" s="T9">0.3:Th</ta>
            <ta e="T11" id="Seg_1263" s="T10">v:Th</ta>
            <ta e="T13" id="Seg_1264" s="T12">np.h:R</ta>
            <ta e="T15" id="Seg_1265" s="T14">adv:Time</ta>
            <ta e="T16" id="Seg_1266" s="T15">0.1.h:E 0.3:Th</ta>
            <ta e="T17" id="Seg_1267" s="T16">v:Th</ta>
            <ta e="T18" id="Seg_1268" s="T17">np:P</ta>
            <ta e="T20" id="Seg_1269" s="T19">pro.h:Poss</ta>
            <ta e="T23" id="Seg_1270" s="T22">np.h:Th</ta>
            <ta e="T25" id="Seg_1271" s="T24">np.h:R</ta>
            <ta e="T26" id="Seg_1272" s="T25">0.2.h:A 0.3:Th</ta>
            <ta e="T29" id="Seg_1273" s="T28">np.h:R</ta>
            <ta e="T32" id="Seg_1274" s="T31">0.3:Th</ta>
            <ta e="T33" id="Seg_1275" s="T32">pro.h:A</ta>
            <ta e="T35" id="Seg_1276" s="T34">np.h:R</ta>
            <ta e="T37" id="Seg_1277" s="T36">0.1.h:A</ta>
            <ta e="T39" id="Seg_1278" s="T38">np.h:A</ta>
            <ta e="T40" id="Seg_1279" s="T39">pro.h:R</ta>
            <ta e="T42" id="Seg_1280" s="T41">pro.h:A</ta>
            <ta e="T43" id="Seg_1281" s="T42">pro.h:B</ta>
            <ta e="T45" id="Seg_1282" s="T44">np:P</ta>
            <ta e="T49" id="Seg_1283" s="T48">pro.h:A</ta>
            <ta e="T50" id="Seg_1284" s="T49">pro.h:R</ta>
            <ta e="T52" id="Seg_1285" s="T51">np:Th</ta>
            <ta e="T54" id="Seg_1286" s="T53">adv:Time</ta>
            <ta e="T55" id="Seg_1287" s="T54">0.1.h:A</ta>
            <ta e="T57" id="Seg_1288" s="T56">np.h:R</ta>
            <ta e="T58" id="Seg_1289" s="T57">pro.h:Poss</ta>
            <ta e="T59" id="Seg_1290" s="T58">np:Th</ta>
            <ta e="T62" id="Seg_1291" s="T61">pro:Th</ta>
            <ta e="T64" id="Seg_1292" s="T63">0.3.h:A 0.3:Th</ta>
            <ta e="T65" id="Seg_1293" s="T64">adv:Time</ta>
            <ta e="T66" id="Seg_1294" s="T65">pro.h:A</ta>
            <ta e="T67" id="Seg_1295" s="T66">pro.h:R</ta>
            <ta e="T68" id="Seg_1296" s="T67">0.3:Th</ta>
            <ta e="T69" id="Seg_1297" s="T68">adv:Time</ta>
            <ta e="T70" id="Seg_1298" s="T69">pro.h:Poss</ta>
            <ta e="T71" id="Seg_1299" s="T70">np.h:Poss</ta>
            <ta e="T72" id="Seg_1300" s="T71">np:Th</ta>
            <ta e="T74" id="Seg_1301" s="T73">pro.h:A</ta>
            <ta e="T75" id="Seg_1302" s="T74">np.h:R</ta>
            <ta e="T76" id="Seg_1303" s="T75">adv:Time</ta>
            <ta e="T78" id="Seg_1304" s="T77">np:Th</ta>
            <ta e="T79" id="Seg_1305" s="T78">pro.h:A</ta>
            <ta e="T80" id="Seg_1306" s="T79">pro:P</ta>
            <ta e="T82" id="Seg_1307" s="T81">pro.h:A</ta>
            <ta e="T83" id="Seg_1308" s="T82">adv:Time</ta>
            <ta e="T84" id="Seg_1309" s="T83">pro:Th</ta>
            <ta e="T88" id="Seg_1310" s="T87">0.1.h:A 0.3:Th</ta>
            <ta e="T89" id="Seg_1311" s="T88">adv:Time</ta>
            <ta e="T90" id="Seg_1312" s="T89">0.1.h:E</ta>
            <ta e="T92" id="Seg_1313" s="T91">np:Th</ta>
            <ta e="T93" id="Seg_1314" s="T92">v:Th</ta>
            <ta e="T94" id="Seg_1315" s="T93">np:Th</ta>
            <ta e="T96" id="Seg_1316" s="T95">np.h:P</ta>
            <ta e="T97" id="Seg_1317" s="T96">0.3.h:A</ta>
            <ta e="T99" id="Seg_1318" s="T98">np:Th</ta>
            <ta e="T101" id="Seg_1319" s="T100">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1320" s="T1">np.h:S</ta>
            <ta e="T5" id="Seg_1321" s="T4">np:O</ta>
            <ta e="T6" id="Seg_1322" s="T5">v:pred</ta>
            <ta e="T9" id="Seg_1323" s="T8">pro.h:S</ta>
            <ta e="T10" id="Seg_1324" s="T9">v:pred 0.3:O</ta>
            <ta e="T11" id="Seg_1325" s="T10">v:O</ta>
            <ta e="T16" id="Seg_1326" s="T15">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T17" id="Seg_1327" s="T16">v:O</ta>
            <ta e="T19" id="Seg_1328" s="T17">s:purp</ta>
            <ta e="T23" id="Seg_1329" s="T22">np.h:S</ta>
            <ta e="T26" id="Seg_1330" s="T25">0.2.h:S v:pred 0.3:O</ta>
            <ta e="T32" id="Seg_1331" s="T31">0.3:S v:pred</ta>
            <ta e="T33" id="Seg_1332" s="T32">pro.h:S</ta>
            <ta e="T34" id="Seg_1333" s="T33">v:pred</ta>
            <ta e="T35" id="Seg_1334" s="T34">np.h:O</ta>
            <ta e="T37" id="Seg_1335" s="T36">0.1.h:S v:pred</ta>
            <ta e="T39" id="Seg_1336" s="T38">np.h:S</ta>
            <ta e="T41" id="Seg_1337" s="T40">v:pred</ta>
            <ta e="T42" id="Seg_1338" s="T41">pro.h:S</ta>
            <ta e="T44" id="Seg_1339" s="T43">v:pred</ta>
            <ta e="T45" id="Seg_1340" s="T44">np:O</ta>
            <ta e="T49" id="Seg_1341" s="T48">pro.h:S</ta>
            <ta e="T51" id="Seg_1342" s="T50">v:pred</ta>
            <ta e="T52" id="Seg_1343" s="T51">np:S</ta>
            <ta e="T53" id="Seg_1344" s="T52">v:pred</ta>
            <ta e="T55" id="Seg_1345" s="T54">0.1.h:S v:pred</ta>
            <ta e="T59" id="Seg_1346" s="T58">np:S</ta>
            <ta e="T60" id="Seg_1347" s="T59">v:pred</ta>
            <ta e="T61" id="Seg_1348" s="T60">adj:pred</ta>
            <ta e="T62" id="Seg_1349" s="T61">pro:S</ta>
            <ta e="T64" id="Seg_1350" s="T63">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T66" id="Seg_1351" s="T65">pro.h:S</ta>
            <ta e="T68" id="Seg_1352" s="T67">v:pred 0.3:O</ta>
            <ta e="T72" id="Seg_1353" s="T71">np:S</ta>
            <ta e="T73" id="Seg_1354" s="T72">v:pred</ta>
            <ta e="T74" id="Seg_1355" s="T73">pro.h:S</ta>
            <ta e="T77" id="Seg_1356" s="T76">v:pred</ta>
            <ta e="T78" id="Seg_1357" s="T77">np:O</ta>
            <ta e="T79" id="Seg_1358" s="T78">pro.h:S</ta>
            <ta e="T80" id="Seg_1359" s="T79">pro:O</ta>
            <ta e="T81" id="Seg_1360" s="T80">v:pred</ta>
            <ta e="T82" id="Seg_1361" s="T81">pro.h:S</ta>
            <ta e="T84" id="Seg_1362" s="T83">pro:O</ta>
            <ta e="T85" id="Seg_1363" s="T84">v:pred</ta>
            <ta e="T88" id="Seg_1364" s="T87">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T90" id="Seg_1365" s="T89">0.1.h:S v:pred</ta>
            <ta e="T93" id="Seg_1366" s="T92">v:O</ta>
            <ta e="T94" id="Seg_1367" s="T93">np:S</ta>
            <ta e="T95" id="Seg_1368" s="T94">v:pred</ta>
            <ta e="T96" id="Seg_1369" s="T95">np.h:O</ta>
            <ta e="T97" id="Seg_1370" s="T96">0.3.h:S v:pred</ta>
            <ta e="T99" id="Seg_1371" s="T98">np:O</ta>
            <ta e="T101" id="Seg_1372" s="T100">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T8" id="Seg_1373" s="T7">RUS:gram</ta>
            <ta e="T14" id="Seg_1374" s="T13">RUS:gram</ta>
            <ta e="T15" id="Seg_1375" s="T14">RUS:disc</ta>
            <ta e="T18" id="Seg_1376" s="T17">RUS:cult</ta>
            <ta e="T27" id="Seg_1377" s="T26">RUS:gram</ta>
            <ta e="T45" id="Seg_1378" s="T44">RUS:cult</ta>
            <ta e="T48" id="Seg_1379" s="T47">RUS:gram</ta>
            <ta e="T63" id="Seg_1380" s="T62">RUS:mod</ta>
            <ta e="T65" id="Seg_1381" s="T64">RUS:disc</ta>
            <ta e="T72" id="Seg_1382" s="T71">RUS:cult</ta>
            <ta e="T75" id="Seg_1383" s="T74">RUS:cult</ta>
            <ta e="T78" id="Seg_1384" s="T77">RUS:cult</ta>
            <ta e="T83" id="Seg_1385" s="T82">RUS:disc</ta>
            <ta e="T87" id="Seg_1386" s="T86">RUS:cult</ta>
            <ta e="T89" id="Seg_1387" s="T88">RUS:disc</ta>
            <ta e="T92" id="Seg_1388" s="T91">RUS:cult</ta>
            <ta e="T94" id="Seg_1389" s="T93">RUS:cult</ta>
            <ta e="T98" id="Seg_1390" s="T97">RUS:gram</ta>
            <ta e="T99" id="Seg_1391" s="T98">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_1392" s="T1">Angelina Ivanovna gave me a hat.</ta>
            <ta e="T13" id="Seg_1393" s="T7">And I wanted to give it to my daughter.</ta>
            <ta e="T19" id="Seg_1394" s="T13">And then I decided to give it [to someone] to make slippers out of it.</ta>
            <ta e="T23" id="Seg_1395" s="T19">I've got two girls.</ta>
            <ta e="T32" id="Seg_1396" s="T23">If you give [the hat] to one of them, there is nothing left for another one.</ta>
            <ta e="T37" id="Seg_1397" s="T32">I said: “I won't give it to one of them”.</ta>
            <ta e="T47" id="Seg_1398" s="T37">And one woman said to me: “I'll sew you slippers out of this hat”.</ta>
            <ta e="T53" id="Seg_1399" s="T47">And I said to her: “There is no sole.</ta>
            <ta e="T57" id="Seg_1400" s="T53">Tomorrow I'll ask Angelina Ivanovna.</ta>
            <ta e="T62" id="Seg_1401" s="T57">She has got a felt boot(?), it's damaged.</ta>
            <ta e="T64" id="Seg_1402" s="T62">May be she'll give [the sole] to me.</ta>
            <ta e="T68" id="Seg_1403" s="T64">Then I'll bring it to you.”</ta>
            <ta e="T73" id="Seg_1404" s="T68">Earlier my mother-in-law had got a sewing machine.</ta>
            <ta e="T78" id="Seg_1405" s="T73">She used to give it to the shoemaker all the time.</ta>
            <ta e="T81" id="Seg_1406" s="T78">He broke it.</ta>
            <ta e="T88" id="Seg_1407" s="T81">Then we sold it, we sold it for one hundred rubles.</ta>
            <ta e="T93" id="Seg_1408" s="T88">Then we wanted to buy another sewing machine.</ta>
            <ta e="T95" id="Seg_1409" s="T93">The war began.</ta>
            <ta e="T97" id="Seg_1410" s="T95">My husband was killed.</ta>
            <ta e="T101" id="Seg_1411" s="T97">And we didn't buy a sewing machine.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_1412" s="T1">Angelina Ivanovna gab mir einen Hut.</ta>
            <ta e="T13" id="Seg_1413" s="T7">Und ich wollte ihn meiner Tochter geben.</ta>
            <ta e="T19" id="Seg_1414" s="T13">Und dann entschied ich, ihn [jemandem] zu geben um Pantoffeln daraus zu machen.</ta>
            <ta e="T23" id="Seg_1415" s="T19">Ich habe zwei Mädchen.</ta>
            <ta e="T32" id="Seg_1416" s="T23">Wenn du einer [den Hut] gibst, gibt es nichts für die andere.</ta>
            <ta e="T37" id="Seg_1417" s="T32">Ich sagte: "Ich gebe ihn keiner von beiden."</ta>
            <ta e="T47" id="Seg_1418" s="T37">Und eine Frau sagte zu mir: "Ich nähe dir Pantoffeln aus diesem Hut."</ta>
            <ta e="T53" id="Seg_1419" s="T47">Und ich sagte ihr: "Es gibt keine Sohle.</ta>
            <ta e="T57" id="Seg_1420" s="T53">Morgen frage ich Angelina Ivanovna.</ta>
            <ta e="T62" id="Seg_1421" s="T57">Sie hat einen Filzstiefel(?), er ist kaputt.</ta>
            <ta e="T64" id="Seg_1422" s="T62">Vielleicht gibt sie mir [die Sohle].</ta>
            <ta e="T68" id="Seg_1423" s="T64">Dann bringe ich sie dir."</ta>
            <ta e="T73" id="Seg_1424" s="T68">Früher hatte meine Schwiegermutter eine Nähmaschine.</ta>
            <ta e="T78" id="Seg_1425" s="T73">Sie gab sie immer wieder dem Schuhmacher.</ta>
            <ta e="T81" id="Seg_1426" s="T78">Er hat sie kaputt gemacht.</ta>
            <ta e="T88" id="Seg_1427" s="T81">Wir haben sie dann verkauft, wir haben sie für hundert Rubel verkauft.</ta>
            <ta e="T93" id="Seg_1428" s="T88">Dann wollten wir eine andere Nähmaschine kaufen.</ta>
            <ta e="T95" id="Seg_1429" s="T93">Der Krieg begann.</ta>
            <ta e="T97" id="Seg_1430" s="T95">Mein Mann wurde getötet.</ta>
            <ta e="T101" id="Seg_1431" s="T97">Und wir haben keine Nähmaschine gekauft.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_1432" s="T1">Ангелина Ивановна мне дала шляпу.</ta>
            <ta e="T13" id="Seg_1433" s="T7">А я хотела своей дочке дать.</ta>
            <ta e="T19" id="Seg_1434" s="T13">А потом мне вздумалось дать, тапочки сшить.</ta>
            <ta e="T23" id="Seg_1435" s="T19">У меня две девчонки.</ta>
            <ta e="T32" id="Seg_1436" s="T23">Одной дочке дашь, а для второй не будет.</ta>
            <ta e="T37" id="Seg_1437" s="T32">Я говорю: “Я одну (одной) не дам”.</ta>
            <ta e="T47" id="Seg_1438" s="T37">Одна женщина мне говорит: “Я тебе сошью тапочки из этой шляпы”.</ta>
            <ta e="T53" id="Seg_1439" s="T47">А ей сказала: “Подошвы нет.</ta>
            <ta e="T57" id="Seg_1440" s="T53">Завтра спрошу у Ангелины Ивановны.</ta>
            <ta e="T62" id="Seg_1441" s="T57">У нее валенок есть, испорченный он.</ta>
            <ta e="T64" id="Seg_1442" s="T62">Может быть, даст.</ta>
            <ta e="T68" id="Seg_1443" s="T64">Потом я тебе принесу”.</ta>
            <ta e="T73" id="Seg_1444" s="T68">Раньше у моей свекрови швейная машинка была.</ta>
            <ta e="T78" id="Seg_1445" s="T73">Она все время сапожнику давала машинку.</ta>
            <ta e="T81" id="Seg_1446" s="T78">Он ее изломал.</ta>
            <ta e="T88" id="Seg_1447" s="T81">Мы потом ее продали, за сто рублей продали.</ta>
            <ta e="T93" id="Seg_1448" s="T88">Потом хотели другую машинку купить.</ta>
            <ta e="T95" id="Seg_1449" s="T93">Война началась.</ta>
            <ta e="T97" id="Seg_1450" s="T95">Мужа моего убили.</ta>
            <ta e="T101" id="Seg_1451" s="T97">И машинку мы не купили.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_1452" s="T1">Ангелина Ивановна мне дала шляпу</ta>
            <ta e="T13" id="Seg_1453" s="T7">я хотела своей девчонке дать</ta>
            <ta e="T19" id="Seg_1454" s="T13">а потом вздумалось тапочки дать сшить</ta>
            <ta e="T23" id="Seg_1455" s="T19">у меня две девчонки</ta>
            <ta e="T32" id="Seg_1456" s="T23">одной девочке дашь а второй не будет</ta>
            <ta e="T37" id="Seg_1457" s="T32">я говорю я одной (одну) не дам</ta>
            <ta e="T47" id="Seg_1458" s="T37">одна женщина мне говорит я тебе сошью тапочки от (из) этой шляпы</ta>
            <ta e="T53" id="Seg_1459" s="T47">а ей сказала подошвы нет</ta>
            <ta e="T57" id="Seg_1460" s="T53">завтра спрошу у Ангелины Ивановны</ta>
            <ta e="T62" id="Seg_1461" s="T57">у нее валенок есть пропавший (старый)</ta>
            <ta e="T64" id="Seg_1462" s="T62">может быть даст</ta>
            <ta e="T68" id="Seg_1463" s="T64">а я потом тебе принесу</ta>
            <ta e="T73" id="Seg_1464" s="T68">раньше у моей свекрови швейная машина была</ta>
            <ta e="T78" id="Seg_1465" s="T73">она все время сапожнику давала машину</ta>
            <ta e="T81" id="Seg_1466" s="T78">он ее изломал</ta>
            <ta e="T88" id="Seg_1467" s="T81">мы потом ее продали за сто рублей продали</ta>
            <ta e="T93" id="Seg_1468" s="T88">потом хотели другую машину купить</ta>
            <ta e="T95" id="Seg_1469" s="T93">война началась (стала)</ta>
            <ta e="T97" id="Seg_1470" s="T95">мужика убили</ta>
            <ta e="T101" id="Seg_1471" s="T97">и машину не купили</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T13" id="Seg_1472" s="T7">[BrM:] 'kɨbanä däkkanä' changed to 'kɨbanädäkkanä'.</ta>
            <ta e="T23" id="Seg_1473" s="T19">[KuAI:] Variant: 'sət'.</ta>
            <ta e="T32" id="Seg_1474" s="T23">[BrM:] Tentative analysis of 'mana', 'männan'.</ta>
            <ta e="T53" id="Seg_1475" s="T47">[BrM:] Tentative analysis of 'tʼängwɨt'.</ta>
            <ta e="T57" id="Seg_1476" s="T53">[KuAI:] Variant: 'Iwanownanä'.</ta>
            <ta e="T62" id="Seg_1477" s="T57">[BrM:] 'nʼüj bimat' changed to 'nʼüjbimat'.</ta>
            <ta e="T73" id="Seg_1478" s="T68">[BrM:] 'passaj pajanan' changed to 'passajpajanan'.</ta>
            <ta e="T78" id="Seg_1479" s="T73">[KuAI:] Variant: 'sapoʒnignä'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
