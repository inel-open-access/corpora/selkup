<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Sewing_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Sewing_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">21</ud-information>
            <ud-information attribute-name="# HIAT:w">15</ud-information>
            <ud-information attribute-name="# e">15</ud-information>
            <ud-information attribute-name="# HIAT:u">5</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T16" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">tamdʼel</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">südereːǯan</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Wes</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">süčau</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_23" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">A</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">tannan</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">jen</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">süssodəmɨl</ts>
                  <nts id="Seg_35" n="HIAT:ip">?</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_38" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">Tattɨ</ts>
                  <nts id="Seg_41" n="HIAT:ip">,</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">sučau</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_48" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">Tolʼko</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">man</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">awan</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">sütkou</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T16" id="Seg_62" n="sc" s="T1">
               <ts e="T2" id="Seg_64" n="e" s="T1">Man </ts>
               <ts e="T3" id="Seg_66" n="e" s="T2">tamdʼel </ts>
               <ts e="T4" id="Seg_68" n="e" s="T3">südereːǯan. </ts>
               <ts e="T5" id="Seg_70" n="e" s="T4">Wes </ts>
               <ts e="T6" id="Seg_72" n="e" s="T5">süčau. </ts>
               <ts e="T7" id="Seg_74" n="e" s="T6">A </ts>
               <ts e="T8" id="Seg_76" n="e" s="T7">tannan </ts>
               <ts e="T9" id="Seg_78" n="e" s="T8">jen </ts>
               <ts e="T10" id="Seg_80" n="e" s="T9">süssodəmɨl? </ts>
               <ts e="T11" id="Seg_82" n="e" s="T10">Tattɨ, </ts>
               <ts e="T12" id="Seg_84" n="e" s="T11">sučau. </ts>
               <ts e="T13" id="Seg_86" n="e" s="T12">Tolʼko </ts>
               <ts e="T14" id="Seg_88" n="e" s="T13">man </ts>
               <ts e="T15" id="Seg_90" n="e" s="T14">awan </ts>
               <ts e="T16" id="Seg_92" n="e" s="T15">sütkou. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_93" s="T1">PVD_1964_Sewing_nar.001 (001.001)</ta>
            <ta e="T6" id="Seg_94" s="T4">PVD_1964_Sewing_nar.002 (001.002)</ta>
            <ta e="T10" id="Seg_95" s="T6">PVD_1964_Sewing_nar.003 (001.003)</ta>
            <ta e="T12" id="Seg_96" s="T10">PVD_1964_Sewing_nar.004 (001.004)</ta>
            <ta e="T16" id="Seg_97" s="T12">PVD_1964_Sewing_nar.005 (001.005)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_98" s="T1">ман там′дʼел сӱде′ре̄джан.</ta>
            <ta e="T6" id="Seg_99" s="T4">вес сӱтшау.</ta>
            <ta e="T10" id="Seg_100" s="T6">а тан′нан jен сӱ′ссодъмыл?</ta>
            <ta e="T12" id="Seg_101" s="T10">тат′ты, су′тшау.</ta>
            <ta e="T16" id="Seg_102" s="T12">толʼко ман ′аwан сӱт′коу.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_103" s="T1">man tamdʼel südereːǯan.</ta>
            <ta e="T6" id="Seg_104" s="T4">wes sütšau.</ta>
            <ta e="T10" id="Seg_105" s="T6">a tannan jen süssodəmɨl?</ta>
            <ta e="T12" id="Seg_106" s="T10">tattɨ, sutšau.</ta>
            <ta e="T16" id="Seg_107" s="T12">tolʼko man awan sütkou.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_108" s="T1">Man tamdʼel südereːǯan. </ta>
            <ta e="T6" id="Seg_109" s="T4">Wes süčau. </ta>
            <ta e="T10" id="Seg_110" s="T6">A tannan jen süssodəmɨl? </ta>
            <ta e="T12" id="Seg_111" s="T10">Tattɨ, sučau. </ta>
            <ta e="T16" id="Seg_112" s="T12">Tolʼko man awan sütkou. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_113" s="T1">man</ta>
            <ta e="T3" id="Seg_114" s="T2">tam-dʼel</ta>
            <ta e="T4" id="Seg_115" s="T3">süd-e-r-eːǯa-n</ta>
            <ta e="T5" id="Seg_116" s="T4">wes</ta>
            <ta e="T6" id="Seg_117" s="T5">sü-ča-u</ta>
            <ta e="T7" id="Seg_118" s="T6">a</ta>
            <ta e="T8" id="Seg_119" s="T7">tan-nan</ta>
            <ta e="T9" id="Seg_120" s="T8">je-n</ta>
            <ta e="T10" id="Seg_121" s="T9">süs-sodə-mɨ-l</ta>
            <ta e="T11" id="Seg_122" s="T10">tat-tɨ</ta>
            <ta e="T12" id="Seg_123" s="T11">su-ča-u</ta>
            <ta e="T13" id="Seg_124" s="T12">tolʼko</ta>
            <ta e="T14" id="Seg_125" s="T13">man</ta>
            <ta e="T15" id="Seg_126" s="T14">awa-n</ta>
            <ta e="T16" id="Seg_127" s="T15">süt-ko-u</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_128" s="T1">man</ta>
            <ta e="T3" id="Seg_129" s="T2">taw-dʼel</ta>
            <ta e="T4" id="Seg_130" s="T3">süt-ɨ-r-enǯɨ-ŋ</ta>
            <ta e="T5" id="Seg_131" s="T4">wesʼ</ta>
            <ta e="T6" id="Seg_132" s="T5">süt-enǯɨ-w</ta>
            <ta e="T7" id="Seg_133" s="T6">a</ta>
            <ta e="T8" id="Seg_134" s="T7">tan-nan</ta>
            <ta e="T9" id="Seg_135" s="T8">eː-n</ta>
            <ta e="T10" id="Seg_136" s="T9">süt-sodə-mɨ-lʼ</ta>
            <ta e="T11" id="Seg_137" s="T10">tat-etɨ</ta>
            <ta e="T12" id="Seg_138" s="T11">süt-enǯɨ-w</ta>
            <ta e="T13" id="Seg_139" s="T12">tolʼko</ta>
            <ta e="T14" id="Seg_140" s="T13">man</ta>
            <ta e="T15" id="Seg_141" s="T14">awa-ŋ</ta>
            <ta e="T16" id="Seg_142" s="T15">süt-ku-w</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_143" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_144" s="T2">this-day.[NOM]</ta>
            <ta e="T4" id="Seg_145" s="T3">sew-EP-FRQ-FUT-1SG.S</ta>
            <ta e="T5" id="Seg_146" s="T4">all</ta>
            <ta e="T6" id="Seg_147" s="T5">sew-FUT-1SG.O</ta>
            <ta e="T7" id="Seg_148" s="T6">and</ta>
            <ta e="T8" id="Seg_149" s="T7">you.SG-ADES</ta>
            <ta e="T9" id="Seg_150" s="T8">be-3SG.S</ta>
            <ta e="T10" id="Seg_151" s="T9">sew-PTCP.NEC-something-ADJZ</ta>
            <ta e="T11" id="Seg_152" s="T10">bring-IMP.2SG.O</ta>
            <ta e="T12" id="Seg_153" s="T11">sew-FUT-1SG.O</ta>
            <ta e="T13" id="Seg_154" s="T12">only</ta>
            <ta e="T14" id="Seg_155" s="T13">I.NOM</ta>
            <ta e="T15" id="Seg_156" s="T14">bad-ADVZ</ta>
            <ta e="T16" id="Seg_157" s="T15">sew-HAB-1SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_158" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_159" s="T2">этот-день.[NOM]</ta>
            <ta e="T4" id="Seg_160" s="T3">сшить-EP-FRQ-FUT-1SG.S</ta>
            <ta e="T5" id="Seg_161" s="T4">все</ta>
            <ta e="T6" id="Seg_162" s="T5">сшить-FUT-1SG.O</ta>
            <ta e="T7" id="Seg_163" s="T6">а</ta>
            <ta e="T8" id="Seg_164" s="T7">ты-ADES</ta>
            <ta e="T9" id="Seg_165" s="T8">быть-3SG.S</ta>
            <ta e="T10" id="Seg_166" s="T9">сшить-PTCP.NEC-нечто-ADJZ</ta>
            <ta e="T11" id="Seg_167" s="T10">принести-IMP.2SG.O</ta>
            <ta e="T12" id="Seg_168" s="T11">сшить-FUT-1SG.O</ta>
            <ta e="T13" id="Seg_169" s="T12">только</ta>
            <ta e="T14" id="Seg_170" s="T13">я.NOM</ta>
            <ta e="T15" id="Seg_171" s="T14">плохой-ADVZ</ta>
            <ta e="T16" id="Seg_172" s="T15">сшить-HAB-1SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_173" s="T1">pers</ta>
            <ta e="T3" id="Seg_174" s="T2">dem-n.[n:case]</ta>
            <ta e="T4" id="Seg_175" s="T3">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_176" s="T4">quant</ta>
            <ta e="T6" id="Seg_177" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_178" s="T6">conj</ta>
            <ta e="T8" id="Seg_179" s="T7">pers-n:case</ta>
            <ta e="T9" id="Seg_180" s="T8">v-v:pn</ta>
            <ta e="T10" id="Seg_181" s="T9">v-v&gt;ptcp-n-n&gt;adj</ta>
            <ta e="T11" id="Seg_182" s="T10">v-v:mood.pn</ta>
            <ta e="T12" id="Seg_183" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_184" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_185" s="T13">pers</ta>
            <ta e="T15" id="Seg_186" s="T14">adj-adj&gt;adv</ta>
            <ta e="T16" id="Seg_187" s="T15">v-v&gt;v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_188" s="T1">pers</ta>
            <ta e="T3" id="Seg_189" s="T2">adv</ta>
            <ta e="T4" id="Seg_190" s="T3">v</ta>
            <ta e="T5" id="Seg_191" s="T4">quant</ta>
            <ta e="T6" id="Seg_192" s="T5">v</ta>
            <ta e="T7" id="Seg_193" s="T6">conj</ta>
            <ta e="T8" id="Seg_194" s="T7">pers</ta>
            <ta e="T9" id="Seg_195" s="T8">v</ta>
            <ta e="T10" id="Seg_196" s="T9">adj</ta>
            <ta e="T11" id="Seg_197" s="T10">v</ta>
            <ta e="T12" id="Seg_198" s="T11">v</ta>
            <ta e="T13" id="Seg_199" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_200" s="T13">pers</ta>
            <ta e="T15" id="Seg_201" s="T14">adv</ta>
            <ta e="T16" id="Seg_202" s="T15">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_203" s="T1">pro.h:A</ta>
            <ta e="T3" id="Seg_204" s="T2">np:Time</ta>
            <ta e="T5" id="Seg_205" s="T4">pro:P</ta>
            <ta e="T6" id="Seg_206" s="T5">0.1.h:A</ta>
            <ta e="T8" id="Seg_207" s="T7">pro.h:Poss</ta>
            <ta e="T10" id="Seg_208" s="T9">np:Th</ta>
            <ta e="T11" id="Seg_209" s="T10">0.2.h:A 0.3:Th</ta>
            <ta e="T12" id="Seg_210" s="T11">0.1.h:A 0.3:P</ta>
            <ta e="T14" id="Seg_211" s="T13">pro.h:A</ta>
            <ta e="T16" id="Seg_212" s="T15">0.3:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_213" s="T1">pro.h:S</ta>
            <ta e="T4" id="Seg_214" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_215" s="T4">pro:O</ta>
            <ta e="T6" id="Seg_216" s="T5">0.1.h:S v:pred</ta>
            <ta e="T9" id="Seg_217" s="T8">v:pred</ta>
            <ta e="T10" id="Seg_218" s="T9">np:S</ta>
            <ta e="T11" id="Seg_219" s="T10">0.2.h:S v:pred 0.3:O</ta>
            <ta e="T12" id="Seg_220" s="T11">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T14" id="Seg_221" s="T13">pro.h:S</ta>
            <ta e="T16" id="Seg_222" s="T15">v:pred 0.3:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_223" s="T4">RUS:core</ta>
            <ta e="T7" id="Seg_224" s="T6">RUS:gram</ta>
            <ta e="T13" id="Seg_225" s="T12">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_226" s="T1">I'll be sewing today.</ta>
            <ta e="T6" id="Seg_227" s="T4">I'll sew everything.</ta>
            <ta e="T10" id="Seg_228" s="T6">Do you have something to be sewn?</ta>
            <ta e="T12" id="Seg_229" s="T10">Bring it, I'll sew it up.</ta>
            <ta e="T16" id="Seg_230" s="T12">But I sew bad.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_231" s="T1">Ich werde heute nähen.</ta>
            <ta e="T6" id="Seg_232" s="T4">Ich werde alles nähen.</ta>
            <ta e="T10" id="Seg_233" s="T6">Hast du etwas, das genäht werden muss?</ta>
            <ta e="T12" id="Seg_234" s="T10">Bring es, ich nähe es.</ta>
            <ta e="T16" id="Seg_235" s="T12">Aber ich nähe schlecht.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_236" s="T1">Я сегодня шить буду.</ta>
            <ta e="T6" id="Seg_237" s="T4">Все сошью.</ta>
            <ta e="T10" id="Seg_238" s="T6">А у тебя есть, что зашить?</ta>
            <ta e="T12" id="Seg_239" s="T10">Принеси, я зашью.</ta>
            <ta e="T16" id="Seg_240" s="T12">Только я плохо шью.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_241" s="T1">я сегодня шить буду</ta>
            <ta e="T6" id="Seg_242" s="T4">все сошью (что есть)</ta>
            <ta e="T10" id="Seg_243" s="T6">а у тебя есть чего зашить (вещи для зашивания)</ta>
            <ta e="T12" id="Seg_244" s="T10">принеси я зашью</ta>
            <ta e="T16" id="Seg_245" s="T12">только я плохо шью</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
