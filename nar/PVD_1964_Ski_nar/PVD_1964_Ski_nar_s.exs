<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Ski_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Ski_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">19</ud-information>
            <ud-information attribute-name="# HIAT:w">14</ud-information>
            <ud-information attribute-name="# e">14</ud-information>
            <ud-information attribute-name="# HIAT:u">5</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T15" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Täbeɣum</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">tolʼdʼila</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">taːdərətdə</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">A</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">manan</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">tolʼdʼöu</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">tʼakku</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">Nadə</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">taugu</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_38" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">I</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">qomdä</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">tʼakgu</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_50" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">Udʼigu</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">nadə</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T15" id="Seg_58" n="sc" s="T1">
               <ts e="T2" id="Seg_60" n="e" s="T1">Täbeɣum </ts>
               <ts e="T3" id="Seg_62" n="e" s="T2">tolʼdʼila </ts>
               <ts e="T4" id="Seg_64" n="e" s="T3">taːdərətdə. </ts>
               <ts e="T5" id="Seg_66" n="e" s="T4">A </ts>
               <ts e="T6" id="Seg_68" n="e" s="T5">manan </ts>
               <ts e="T7" id="Seg_70" n="e" s="T6">tolʼdʼöu </ts>
               <ts e="T8" id="Seg_72" n="e" s="T7">tʼakku. </ts>
               <ts e="T9" id="Seg_74" n="e" s="T8">Nadə </ts>
               <ts e="T10" id="Seg_76" n="e" s="T9">taugu. </ts>
               <ts e="T11" id="Seg_78" n="e" s="T10">I </ts>
               <ts e="T12" id="Seg_80" n="e" s="T11">qomdä </ts>
               <ts e="T13" id="Seg_82" n="e" s="T12">tʼakgu. </ts>
               <ts e="T14" id="Seg_84" n="e" s="T13">Udʼigu </ts>
               <ts e="T15" id="Seg_86" n="e" s="T14">nadə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_87" s="T1">PVD_1964_Ski_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_88" s="T4">PVD_1964_Ski_nar.002 (001.002)</ta>
            <ta e="T10" id="Seg_89" s="T8">PVD_1964_Ski_nar.003 (001.003)</ta>
            <ta e="T13" id="Seg_90" s="T10">PVD_1964_Ski_nar.004 (001.004)</ta>
            <ta e="T15" id="Seg_91" s="T13">PVD_1964_Ski_nar.005 (001.005)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_92" s="T1">′тӓбеɣум ′толʼдʼила ′та̄дъръ(ы)тд̂ъ.</ta>
            <ta e="T8" id="Seg_93" s="T4">а ма′нан толʼ′дʼӧу ′тʼакку.</ta>
            <ta e="T10" id="Seg_94" s="T8">надъ ′таугу.</ta>
            <ta e="T13" id="Seg_95" s="T10">и ′kомдӓ тʼакг̂у.</ta>
            <ta e="T15" id="Seg_96" s="T13">′удʼигу надъ.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_97" s="T1">täbeɣum tolʼdʼila taːdərə(ɨ)td̂ə.</ta>
            <ta e="T8" id="Seg_98" s="T4">a manan tolʼdʼöu tʼakku.</ta>
            <ta e="T10" id="Seg_99" s="T8">nadə taugu.</ta>
            <ta e="T13" id="Seg_100" s="T10">i qomdä tʼakĝu.</ta>
            <ta e="T15" id="Seg_101" s="T13">udʼigu nadə.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_102" s="T1">Täbeɣum tolʼdʼila taːdərətdə. </ta>
            <ta e="T8" id="Seg_103" s="T4">A manan tolʼdʼöu tʼakku. </ta>
            <ta e="T10" id="Seg_104" s="T8">Nadə taugu. </ta>
            <ta e="T13" id="Seg_105" s="T10">I qomdä tʼakgu. </ta>
            <ta e="T15" id="Seg_106" s="T13">Udʼigu nadə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_107" s="T1">täbe-ɣum</ta>
            <ta e="T3" id="Seg_108" s="T2">tolʼdʼi-la</ta>
            <ta e="T4" id="Seg_109" s="T3">taːd-ə-r-ə-tdə</ta>
            <ta e="T5" id="Seg_110" s="T4">a</ta>
            <ta e="T6" id="Seg_111" s="T5">ma-nan</ta>
            <ta e="T7" id="Seg_112" s="T6">tolʼdʼö-u</ta>
            <ta e="T8" id="Seg_113" s="T7">tʼakku</ta>
            <ta e="T9" id="Seg_114" s="T8">nadə</ta>
            <ta e="T10" id="Seg_115" s="T9">tau-gu</ta>
            <ta e="T11" id="Seg_116" s="T10">i</ta>
            <ta e="T12" id="Seg_117" s="T11">qomdä</ta>
            <ta e="T13" id="Seg_118" s="T12">tʼakgu</ta>
            <ta e="T14" id="Seg_119" s="T13">udʼi-gu</ta>
            <ta e="T15" id="Seg_120" s="T14">nadə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_121" s="T1">täbe-qum</ta>
            <ta e="T3" id="Seg_122" s="T2">tolʼdʼ-la</ta>
            <ta e="T4" id="Seg_123" s="T3">tat-ɨ-r-ɨ-t</ta>
            <ta e="T5" id="Seg_124" s="T4">a</ta>
            <ta e="T6" id="Seg_125" s="T5">man-nan</ta>
            <ta e="T7" id="Seg_126" s="T6">tolʼdʼ-w</ta>
            <ta e="T8" id="Seg_127" s="T7">tʼäkku</ta>
            <ta e="T9" id="Seg_128" s="T8">nadə</ta>
            <ta e="T10" id="Seg_129" s="T9">taw-gu</ta>
            <ta e="T11" id="Seg_130" s="T10">i</ta>
            <ta e="T12" id="Seg_131" s="T11">qomdɛ</ta>
            <ta e="T13" id="Seg_132" s="T12">tʼäkku</ta>
            <ta e="T14" id="Seg_133" s="T13">uːdʼi-gu</ta>
            <ta e="T15" id="Seg_134" s="T14">nadə</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_135" s="T1">man-human.being.[NOM]</ta>
            <ta e="T3" id="Seg_136" s="T2">ski-PL.[NOM]</ta>
            <ta e="T4" id="Seg_137" s="T3">bring-EP-DRV-EP-3SG.O</ta>
            <ta e="T5" id="Seg_138" s="T4">and</ta>
            <ta e="T6" id="Seg_139" s="T5">I-ADES</ta>
            <ta e="T7" id="Seg_140" s="T6">ski.[NOM]-1SG</ta>
            <ta e="T8" id="Seg_141" s="T7">NEG.EX.[3SG.S]</ta>
            <ta e="T9" id="Seg_142" s="T8">one.should</ta>
            <ta e="T10" id="Seg_143" s="T9">buy-INF</ta>
            <ta e="T11" id="Seg_144" s="T10">and</ta>
            <ta e="T12" id="Seg_145" s="T11">money.[NOM]</ta>
            <ta e="T13" id="Seg_146" s="T12">NEG.EX.[3SG.S]</ta>
            <ta e="T14" id="Seg_147" s="T13">work-INF</ta>
            <ta e="T15" id="Seg_148" s="T14">one.should</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_149" s="T1">мужчина-человек.[NOM]</ta>
            <ta e="T3" id="Seg_150" s="T2">лыжи-PL.[NOM]</ta>
            <ta e="T4" id="Seg_151" s="T3">принести-EP-DRV-EP-3SG.O</ta>
            <ta e="T5" id="Seg_152" s="T4">а</ta>
            <ta e="T6" id="Seg_153" s="T5">я-ADES</ta>
            <ta e="T7" id="Seg_154" s="T6">лыжи.[NOM]-1SG</ta>
            <ta e="T8" id="Seg_155" s="T7">NEG.EX.[3SG.S]</ta>
            <ta e="T9" id="Seg_156" s="T8">надо</ta>
            <ta e="T10" id="Seg_157" s="T9">купить-INF</ta>
            <ta e="T11" id="Seg_158" s="T10">и</ta>
            <ta e="T12" id="Seg_159" s="T11">деньги.[NOM]</ta>
            <ta e="T13" id="Seg_160" s="T12">NEG.EX.[3SG.S]</ta>
            <ta e="T14" id="Seg_161" s="T13">работать-INF</ta>
            <ta e="T15" id="Seg_162" s="T14">надо</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_163" s="T1">n-n.[n:case]</ta>
            <ta e="T3" id="Seg_164" s="T2">n-n:num.[n:case]</ta>
            <ta e="T4" id="Seg_165" s="T3">v-v:ins-v&gt;v-n:ins-v:pn</ta>
            <ta e="T5" id="Seg_166" s="T4">conj</ta>
            <ta e="T6" id="Seg_167" s="T5">pers-n:case</ta>
            <ta e="T7" id="Seg_168" s="T6">n.[n:case]-n:poss</ta>
            <ta e="T8" id="Seg_169" s="T7">v.[v:pn]</ta>
            <ta e="T9" id="Seg_170" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_171" s="T9">v-v:inf</ta>
            <ta e="T11" id="Seg_172" s="T10">conj</ta>
            <ta e="T12" id="Seg_173" s="T11">n.[n:case]</ta>
            <ta e="T13" id="Seg_174" s="T12">v.[v:pn]</ta>
            <ta e="T14" id="Seg_175" s="T13">v-v:inf</ta>
            <ta e="T15" id="Seg_176" s="T14">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_177" s="T1">n</ta>
            <ta e="T3" id="Seg_178" s="T2">n</ta>
            <ta e="T4" id="Seg_179" s="T3">v</ta>
            <ta e="T5" id="Seg_180" s="T4">conj</ta>
            <ta e="T6" id="Seg_181" s="T5">pers</ta>
            <ta e="T7" id="Seg_182" s="T6">n</ta>
            <ta e="T8" id="Seg_183" s="T7">v</ta>
            <ta e="T9" id="Seg_184" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_185" s="T9">v</ta>
            <ta e="T11" id="Seg_186" s="T10">conj</ta>
            <ta e="T12" id="Seg_187" s="T11">n</ta>
            <ta e="T13" id="Seg_188" s="T12">v</ta>
            <ta e="T14" id="Seg_189" s="T13">v</ta>
            <ta e="T15" id="Seg_190" s="T14">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_191" s="T1">np.h:A</ta>
            <ta e="T3" id="Seg_192" s="T2">np:Th</ta>
            <ta e="T6" id="Seg_193" s="T5">pro.h:Poss</ta>
            <ta e="T7" id="Seg_194" s="T6">np:Th</ta>
            <ta e="T10" id="Seg_195" s="T9">v:Th</ta>
            <ta e="T12" id="Seg_196" s="T11">np:Th</ta>
            <ta e="T14" id="Seg_197" s="T13">v:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_198" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_199" s="T2">np:O</ta>
            <ta e="T4" id="Seg_200" s="T3">v:pred</ta>
            <ta e="T7" id="Seg_201" s="T6">np:S</ta>
            <ta e="T8" id="Seg_202" s="T7">v:pred</ta>
            <ta e="T9" id="Seg_203" s="T8">ptcl:pred</ta>
            <ta e="T10" id="Seg_204" s="T9">v:O</ta>
            <ta e="T12" id="Seg_205" s="T11">np:S</ta>
            <ta e="T13" id="Seg_206" s="T12">v:pred</ta>
            <ta e="T14" id="Seg_207" s="T13">v:O</ta>
            <ta e="T15" id="Seg_208" s="T14">ptcl:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_209" s="T4">RUS:gram</ta>
            <ta e="T9" id="Seg_210" s="T8">RUS:mod</ta>
            <ta e="T11" id="Seg_211" s="T10">RUS:gram</ta>
            <ta e="T15" id="Seg_212" s="T14">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_213" s="T1">A man carries skies.</ta>
            <ta e="T8" id="Seg_214" s="T4">And I have no skies.</ta>
            <ta e="T10" id="Seg_215" s="T8">[I] have to buy [it].</ta>
            <ta e="T13" id="Seg_216" s="T10">And I have no money.</ta>
            <ta e="T15" id="Seg_217" s="T13">[I] have to work.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_218" s="T1">Ein Mann trägt Skier.</ta>
            <ta e="T8" id="Seg_219" s="T4">Und ich habe keine Skier.</ta>
            <ta e="T10" id="Seg_220" s="T8">[Ich] muss [sie] kaufen.</ta>
            <ta e="T13" id="Seg_221" s="T10">Und ich habe kein Geld.</ta>
            <ta e="T15" id="Seg_222" s="T13">[Ich] muss arbeiten.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_223" s="T1">Мужчина лыжи несет.</ta>
            <ta e="T8" id="Seg_224" s="T4">А у меня лыж нет.</ta>
            <ta e="T10" id="Seg_225" s="T8">Надо купить.</ta>
            <ta e="T13" id="Seg_226" s="T10">И денег нет.</ta>
            <ta e="T15" id="Seg_227" s="T13">Надо работать.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_228" s="T1">мужчина лыжи несет</ta>
            <ta e="T8" id="Seg_229" s="T4">а у меня лыж нет</ta>
            <ta e="T10" id="Seg_230" s="T8">надо купить</ta>
            <ta e="T13" id="Seg_231" s="T10">денег нет</ta>
            <ta e="T15" id="Seg_232" s="T13">надо работать</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
