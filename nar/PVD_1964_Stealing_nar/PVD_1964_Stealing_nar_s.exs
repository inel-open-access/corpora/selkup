<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Stealing_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Stealing_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">64</ud-information>
            <ud-information attribute-name="# HIAT:w">51</ud-information>
            <ud-information attribute-name="# e">51</ud-information>
            <ud-information attribute-name="# HIAT:u">11</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T52" id="Seg_0" n="sc" s="T1">
               <ts e="T9" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">A</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">tore</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">qulam</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">qwatqwattə</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">tebla</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_19" n="HIAT:w" s="T6">štobɨ</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_22" n="HIAT:w" s="T7">igə</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_25" n="HIAT:w" s="T8">toːrnijämtə</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_29" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">Manan</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">näjɣum</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_37" n="HIAT:w" s="T11">sekɨs</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_41" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">Mazɨm</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">toːläsa</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_50" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">Teblam</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">senku</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">asʼ</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">üdəkeǯau</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_65" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">Okɨrɨn</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">man</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_73" n="HIAT:w" s="T20">nejɣum</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">seːŋku</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">üːdau</ts>
                  <nts id="Seg_80" n="HIAT:ip">.</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_83" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">Tep</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_88" n="HIAT:w" s="T24">mazɨm</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_91" n="HIAT:w" s="T25">tolʼenɨn</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_95" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">Lʼupkau</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_100" n="HIAT:w" s="T27">tolɨdɨt</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_103" n="HIAT:w" s="T28">i</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_106" n="HIAT:w" s="T29">koptau</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_109" n="HIAT:w" s="T30">i</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_112" n="HIAT:w" s="T31">pladmu</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_115" n="HIAT:w" s="T32">tolɨdɨt</ts>
                  <nts id="Seg_116" n="HIAT:ip">.</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_119" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_121" n="HIAT:w" s="T33">Me</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_124" n="HIAT:w" s="T34">tebɨm</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_127" n="HIAT:w" s="T35">qalʼe</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_130" n="HIAT:w" s="T36">qwatdaj</ts>
                  <nts id="Seg_131" n="HIAT:ip">,</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_134" n="HIAT:w" s="T37">as</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_137" n="HIAT:w" s="T38">medəraj</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_141" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_143" n="HIAT:w" s="T39">Tep</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_146" n="HIAT:w" s="T40">man</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_149" n="HIAT:w" s="T41">adʼöʒau</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_152" n="HIAT:w" s="T42">Plʼešiwkaɣən</ts>
                  <nts id="Seg_153" n="HIAT:ip">,</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_156" n="HIAT:w" s="T43">wes</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_159" n="HIAT:w" s="T44">merɨbatt</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_163" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_165" n="HIAT:w" s="T45">Man</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_168" n="HIAT:w" s="T46">bɨ</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_171" n="HIAT:w" s="T47">šičas</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_174" n="HIAT:w" s="T48">bɨ</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_177" n="HIAT:w" s="T49">waːrnäu</ts>
                  <nts id="Seg_178" n="HIAT:ip">.</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_181" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_183" n="HIAT:w" s="T50">Man</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_186" n="HIAT:w" s="T51">tʼüːrzan</ts>
                  <nts id="Seg_187" n="HIAT:ip">.</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T52" id="Seg_189" n="sc" s="T1">
               <ts e="T2" id="Seg_191" n="e" s="T1">A </ts>
               <ts e="T3" id="Seg_193" n="e" s="T2">tore </ts>
               <ts e="T4" id="Seg_195" n="e" s="T3">qulam </ts>
               <ts e="T5" id="Seg_197" n="e" s="T4">qwatqwattə </ts>
               <ts e="T6" id="Seg_199" n="e" s="T5">tebla </ts>
               <ts e="T7" id="Seg_201" n="e" s="T6">štobɨ </ts>
               <ts e="T8" id="Seg_203" n="e" s="T7">igə </ts>
               <ts e="T9" id="Seg_205" n="e" s="T8">toːrnijämtə. </ts>
               <ts e="T10" id="Seg_207" n="e" s="T9">Manan </ts>
               <ts e="T11" id="Seg_209" n="e" s="T10">näjɣum </ts>
               <ts e="T12" id="Seg_211" n="e" s="T11">sekɨs. </ts>
               <ts e="T13" id="Seg_213" n="e" s="T12">Mazɨm </ts>
               <ts e="T14" id="Seg_215" n="e" s="T13">toːläsa. </ts>
               <ts e="T15" id="Seg_217" n="e" s="T14">Teblam </ts>
               <ts e="T16" id="Seg_219" n="e" s="T15">senku </ts>
               <ts e="T17" id="Seg_221" n="e" s="T16">asʼ </ts>
               <ts e="T18" id="Seg_223" n="e" s="T17">üdəkeǯau. </ts>
               <ts e="T19" id="Seg_225" n="e" s="T18">Okɨrɨn </ts>
               <ts e="T20" id="Seg_227" n="e" s="T19">man </ts>
               <ts e="T21" id="Seg_229" n="e" s="T20">nejɣum </ts>
               <ts e="T22" id="Seg_231" n="e" s="T21">seːŋku </ts>
               <ts e="T23" id="Seg_233" n="e" s="T22">üːdau. </ts>
               <ts e="T24" id="Seg_235" n="e" s="T23">Tep </ts>
               <ts e="T25" id="Seg_237" n="e" s="T24">mazɨm </ts>
               <ts e="T26" id="Seg_239" n="e" s="T25">tolʼenɨn. </ts>
               <ts e="T27" id="Seg_241" n="e" s="T26">Lʼupkau </ts>
               <ts e="T28" id="Seg_243" n="e" s="T27">tolɨdɨt </ts>
               <ts e="T29" id="Seg_245" n="e" s="T28">i </ts>
               <ts e="T30" id="Seg_247" n="e" s="T29">koptau </ts>
               <ts e="T31" id="Seg_249" n="e" s="T30">i </ts>
               <ts e="T32" id="Seg_251" n="e" s="T31">pladmu </ts>
               <ts e="T33" id="Seg_253" n="e" s="T32">tolɨdɨt. </ts>
               <ts e="T34" id="Seg_255" n="e" s="T33">Me </ts>
               <ts e="T35" id="Seg_257" n="e" s="T34">tebɨm </ts>
               <ts e="T36" id="Seg_259" n="e" s="T35">qalʼe </ts>
               <ts e="T37" id="Seg_261" n="e" s="T36">qwatdaj, </ts>
               <ts e="T38" id="Seg_263" n="e" s="T37">as </ts>
               <ts e="T39" id="Seg_265" n="e" s="T38">medəraj. </ts>
               <ts e="T40" id="Seg_267" n="e" s="T39">Tep </ts>
               <ts e="T41" id="Seg_269" n="e" s="T40">man </ts>
               <ts e="T42" id="Seg_271" n="e" s="T41">adʼöʒau </ts>
               <ts e="T43" id="Seg_273" n="e" s="T42">Plʼešiwkaɣən, </ts>
               <ts e="T44" id="Seg_275" n="e" s="T43">wes </ts>
               <ts e="T45" id="Seg_277" n="e" s="T44">merɨbatt. </ts>
               <ts e="T46" id="Seg_279" n="e" s="T45">Man </ts>
               <ts e="T47" id="Seg_281" n="e" s="T46">bɨ </ts>
               <ts e="T48" id="Seg_283" n="e" s="T47">šičas </ts>
               <ts e="T49" id="Seg_285" n="e" s="T48">bɨ </ts>
               <ts e="T50" id="Seg_287" n="e" s="T49">waːrnäu. </ts>
               <ts e="T51" id="Seg_289" n="e" s="T50">Man </ts>
               <ts e="T52" id="Seg_291" n="e" s="T51">tʼüːrzan. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T9" id="Seg_292" s="T1">PVD_1964_Stealing_nar.001 (001.001)</ta>
            <ta e="T12" id="Seg_293" s="T9">PVD_1964_Stealing_nar.002 (001.002)</ta>
            <ta e="T14" id="Seg_294" s="T12">PVD_1964_Stealing_nar.003 (001.003)</ta>
            <ta e="T18" id="Seg_295" s="T14">PVD_1964_Stealing_nar.004 (001.004)</ta>
            <ta e="T23" id="Seg_296" s="T18">PVD_1964_Stealing_nar.005 (001.005)</ta>
            <ta e="T26" id="Seg_297" s="T23">PVD_1964_Stealing_nar.006 (001.006)</ta>
            <ta e="T33" id="Seg_298" s="T26">PVD_1964_Stealing_nar.007 (001.007)</ta>
            <ta e="T39" id="Seg_299" s="T33">PVD_1964_Stealing_nar.008 (001.008)</ta>
            <ta e="T45" id="Seg_300" s="T39">PVD_1964_Stealing_nar.009 (001.009)</ta>
            <ta e="T50" id="Seg_301" s="T45">PVD_1964_Stealing_nar.010 (001.010)</ta>
            <ta e="T52" id="Seg_302" s="T50">PVD_1964_Stealing_nar.011 (001.011)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T9" id="Seg_303" s="T1">а ′торе ′kуlам kwат′kwаттъ теб′ла штобы ′игъ ′то̄рниjӓмтъ.</ta>
            <ta e="T12" id="Seg_304" s="T9">ма′нан нӓй′ɣум ′секыс.</ta>
            <ta e="T14" id="Seg_305" s="T12">ма′зым ′то̄lӓса.</ta>
            <ta e="T18" id="Seg_306" s="T14">теб′лам ′сен̃ку ′асʼ ′ӱдъкеджау̹.</ta>
            <ta e="T23" id="Seg_307" s="T18">′окыр ын ман ней′ɣум ′се̄ŋ‵ку ′ӱ̄дау̹.</ta>
            <ta e="T26" id="Seg_308" s="T23">теп ма′зым ′толʼенын.</ta>
            <ta e="T33" id="Seg_309" s="T26">лʼуп′кау̹ ′толыдыт и ′коптау̹ и ′пладму ′толыдыт.</ta>
            <ta e="T39" id="Seg_310" s="T33">ме те′бым ′kалʼе kwат′дай, ас ‵медъ′рай.</ta>
            <ta e="T45" id="Seg_311" s="T39">те̨п ман а′дʼӧжау̹ плʼе′шив‵каɣъ(ы)н, вес ′мерыбатт.</ta>
            <ta e="T50" id="Seg_312" s="T45">ман бы ши′час бы wа̄р′нӓу̹.</ta>
            <ta e="T52" id="Seg_313" s="T50">ман ′тʼӱ̄рзан.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T9" id="Seg_314" s="T1">a tore qulam qwatqwattə tebla štobɨ igə toːrnijämtə.</ta>
            <ta e="T12" id="Seg_315" s="T9">manan näjɣum sekɨs.</ta>
            <ta e="T14" id="Seg_316" s="T12">mazɨm toːläsa.</ta>
            <ta e="T18" id="Seg_317" s="T14">teblam senku asʼ üdəkeǯau̹.</ta>
            <ta e="T23" id="Seg_318" s="T18">okɨr ɨn man nejɣum seːŋku üːdau̹.</ta>
            <ta e="T26" id="Seg_319" s="T23">tep mazɨm tolʼenɨn.</ta>
            <ta e="T33" id="Seg_320" s="T26">lʼupkau̹ tolɨdɨt i koptau̹ i pladmu tolɨdɨt.</ta>
            <ta e="T39" id="Seg_321" s="T33">me tebɨm qalʼe qwatdaj, as medəraj.</ta>
            <ta e="T45" id="Seg_322" s="T39">tep man adʼöʒau̹ plʼešiwkaɣə(ɨ)n, wes merɨbatt.</ta>
            <ta e="T50" id="Seg_323" s="T45">man bɨ šičas bɨ waːrnäu̹.</ta>
            <ta e="T52" id="Seg_324" s="T50">man tʼüːrzan.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T9" id="Seg_325" s="T1">A tore qulam qwatqwattə tebla štobɨ igə toːrnijämtə. </ta>
            <ta e="T12" id="Seg_326" s="T9">Manan näjɣum sekɨs. </ta>
            <ta e="T14" id="Seg_327" s="T12">Mazɨm toːläsa. </ta>
            <ta e="T18" id="Seg_328" s="T14">Teblam senku asʼ üdəkeǯau. </ta>
            <ta e="T23" id="Seg_329" s="T18">Okɨrɨn man nejɣum seːŋku üːdau. </ta>
            <ta e="T26" id="Seg_330" s="T23">Tep mazɨm tolʼenɨn. </ta>
            <ta e="T33" id="Seg_331" s="T26">Lʼupkau tolɨdɨt i koptau i pladmu tolɨdɨt. </ta>
            <ta e="T39" id="Seg_332" s="T33">Me tebɨm qalʼe qwatdaj, as medəraj. </ta>
            <ta e="T45" id="Seg_333" s="T39">Tep man adʼöʒau Plʼešiwkaɣən, wes merɨbatt. </ta>
            <ta e="T50" id="Seg_334" s="T45">Man bɨ šičas bɨ waːrnäu. </ta>
            <ta e="T52" id="Seg_335" s="T50">Man tʼüːrzan. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_336" s="T1">a</ta>
            <ta e="T3" id="Seg_337" s="T2">tor-e</ta>
            <ta e="T4" id="Seg_338" s="T3">qu-la-m</ta>
            <ta e="T5" id="Seg_339" s="T4">qwat-q-wa-ttə</ta>
            <ta e="T6" id="Seg_340" s="T5">teb-la</ta>
            <ta e="T7" id="Seg_341" s="T6">štobɨ</ta>
            <ta e="T8" id="Seg_342" s="T7">igə</ta>
            <ta e="T9" id="Seg_343" s="T8">toːr-nijä-mtə</ta>
            <ta e="T10" id="Seg_344" s="T9">ma-nan</ta>
            <ta e="T11" id="Seg_345" s="T10">nä-j-ɣum</ta>
            <ta e="T12" id="Seg_346" s="T11">sekɨ-s</ta>
            <ta e="T13" id="Seg_347" s="T12">mazɨm</ta>
            <ta e="T14" id="Seg_348" s="T13">toːlä-sa</ta>
            <ta e="T15" id="Seg_349" s="T14">teb-la-m</ta>
            <ta e="T16" id="Seg_350" s="T15">sen-ku</ta>
            <ta e="T17" id="Seg_351" s="T16">asʼ</ta>
            <ta e="T18" id="Seg_352" s="T17">üdə-k-eǯa-u</ta>
            <ta e="T19" id="Seg_353" s="T18">okɨr-ɨ-n</ta>
            <ta e="T20" id="Seg_354" s="T19">man</ta>
            <ta e="T21" id="Seg_355" s="T20">ne-j-ɣum</ta>
            <ta e="T22" id="Seg_356" s="T21">seːŋ-ku</ta>
            <ta e="T23" id="Seg_357" s="T22">üːda-u</ta>
            <ta e="T24" id="Seg_358" s="T23">tep</ta>
            <ta e="T25" id="Seg_359" s="T24">mazɨm</ta>
            <ta e="T26" id="Seg_360" s="T25">tolʼe-nɨ-n</ta>
            <ta e="T27" id="Seg_361" s="T26">lʼupka-u</ta>
            <ta e="T28" id="Seg_362" s="T27">tolɨ-dɨ-t</ta>
            <ta e="T29" id="Seg_363" s="T28">i</ta>
            <ta e="T30" id="Seg_364" s="T29">kopta-u</ta>
            <ta e="T31" id="Seg_365" s="T30">i</ta>
            <ta e="T32" id="Seg_366" s="T31">plad-mu</ta>
            <ta e="T33" id="Seg_367" s="T32">tolɨ-dɨ-t</ta>
            <ta e="T34" id="Seg_368" s="T33">me</ta>
            <ta e="T35" id="Seg_369" s="T34">teb-ɨ-m</ta>
            <ta e="T36" id="Seg_370" s="T35">qa-lʼe</ta>
            <ta e="T37" id="Seg_371" s="T36">qwat-da-j</ta>
            <ta e="T38" id="Seg_372" s="T37">as</ta>
            <ta e="T39" id="Seg_373" s="T38">medə-ra-j</ta>
            <ta e="T40" id="Seg_374" s="T39">tep</ta>
            <ta e="T41" id="Seg_375" s="T40">man</ta>
            <ta e="T42" id="Seg_376" s="T41">adʼöʒa-u</ta>
            <ta e="T43" id="Seg_377" s="T42">Plʼešiwka-ɣən</ta>
            <ta e="T44" id="Seg_378" s="T43">wes</ta>
            <ta e="T45" id="Seg_379" s="T44">merɨ-ba-tt</ta>
            <ta e="T46" id="Seg_380" s="T45">man</ta>
            <ta e="T47" id="Seg_381" s="T46">bɨ</ta>
            <ta e="T48" id="Seg_382" s="T47">šičas</ta>
            <ta e="T49" id="Seg_383" s="T48">bɨ</ta>
            <ta e="T50" id="Seg_384" s="T49">waːr-nä-u</ta>
            <ta e="T51" id="Seg_385" s="T50">man</ta>
            <ta e="T52" id="Seg_386" s="T51">tʼüːr-za-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_387" s="T1">a</ta>
            <ta e="T3" id="Seg_388" s="T2">tor-e</ta>
            <ta e="T4" id="Seg_389" s="T3">qum-la-m</ta>
            <ta e="T5" id="Seg_390" s="T4">qwat-ku-nɨ-tɨn</ta>
            <ta e="T6" id="Seg_391" s="T5">täp-la</ta>
            <ta e="T7" id="Seg_392" s="T6">štobɨ</ta>
            <ta e="T8" id="Seg_393" s="T7">igə</ta>
            <ta e="T9" id="Seg_394" s="T8">tor-nijä-mtə</ta>
            <ta e="T10" id="Seg_395" s="T9">man-nan</ta>
            <ta e="T11" id="Seg_396" s="T10">ne-lʼ-qum</ta>
            <ta e="T12" id="Seg_397" s="T11">seqqɨ-sɨ</ta>
            <ta e="T13" id="Seg_398" s="T12">mazɨm</ta>
            <ta e="T14" id="Seg_399" s="T13">toːlä-sɨ</ta>
            <ta e="T15" id="Seg_400" s="T14">täp-la-m</ta>
            <ta e="T16" id="Seg_401" s="T15">seːŋ-gu</ta>
            <ta e="T17" id="Seg_402" s="T16">asa</ta>
            <ta e="T18" id="Seg_403" s="T17">üdə-ku-enǯɨ-w</ta>
            <ta e="T19" id="Seg_404" s="T18">okkɨr-ɨ-ŋ</ta>
            <ta e="T20" id="Seg_405" s="T19">man</ta>
            <ta e="T21" id="Seg_406" s="T20">ne-lʼ-qum</ta>
            <ta e="T22" id="Seg_407" s="T21">seːŋ-gu</ta>
            <ta e="T23" id="Seg_408" s="T22">üdə-w</ta>
            <ta e="T24" id="Seg_409" s="T23">täp</ta>
            <ta e="T25" id="Seg_410" s="T24">mazɨm</ta>
            <ta e="T26" id="Seg_411" s="T25">toːlä-nɨ-n</ta>
            <ta e="T27" id="Seg_412" s="T26">lʼübka-w</ta>
            <ta e="T28" id="Seg_413" s="T27">tolɨ-ntɨ-t</ta>
            <ta e="T29" id="Seg_414" s="T28">i</ta>
            <ta e="T30" id="Seg_415" s="T29">kopta-w</ta>
            <ta e="T31" id="Seg_416" s="T30">i</ta>
            <ta e="T32" id="Seg_417" s="T31">plad-w</ta>
            <ta e="T33" id="Seg_418" s="T32">tolɨ-ntɨ-t</ta>
            <ta e="T34" id="Seg_419" s="T33">me</ta>
            <ta e="T35" id="Seg_420" s="T34">täp-ɨ-m</ta>
            <ta e="T36" id="Seg_421" s="T35">qa-le</ta>
            <ta e="T37" id="Seg_422" s="T36">qwan-ntɨ-j</ta>
            <ta e="T38" id="Seg_423" s="T37">asa</ta>
            <ta e="T39" id="Seg_424" s="T38">medə-rɨ-j</ta>
            <ta e="T40" id="Seg_425" s="T39">täp</ta>
            <ta e="T41" id="Seg_426" s="T40">man</ta>
            <ta e="T42" id="Seg_427" s="T41">adʼöʒa-w</ta>
            <ta e="T43" id="Seg_428" s="T42">Plʼešiwka-qɨn</ta>
            <ta e="T44" id="Seg_429" s="T43">wesʼ</ta>
            <ta e="T45" id="Seg_430" s="T44">merɨŋ-mbɨ-t</ta>
            <ta e="T46" id="Seg_431" s="T45">man</ta>
            <ta e="T47" id="Seg_432" s="T46">bɨ</ta>
            <ta e="T48" id="Seg_433" s="T47">sičas</ta>
            <ta e="T49" id="Seg_434" s="T48">bɨ</ta>
            <ta e="T50" id="Seg_435" s="T49">warɨ-ne-w</ta>
            <ta e="T51" id="Seg_436" s="T50">man</ta>
            <ta e="T52" id="Seg_437" s="T51">tʼüru-sɨ-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_438" s="T1">and</ta>
            <ta e="T3" id="Seg_439" s="T2">steal-PTCP.PRS2</ta>
            <ta e="T4" id="Seg_440" s="T3">human.being-PL-ACC</ta>
            <ta e="T5" id="Seg_441" s="T4">kill-HAB-CO-3PL</ta>
            <ta e="T6" id="Seg_442" s="T5">(s)he-PL.[NOM]</ta>
            <ta e="T7" id="Seg_443" s="T6">so.that</ta>
            <ta e="T8" id="Seg_444" s="T7">NEG.IMP</ta>
            <ta e="T9" id="Seg_445" s="T8">steal-IMP.3-IMP.3SG/PL.O</ta>
            <ta e="T10" id="Seg_446" s="T9">I-ADES</ta>
            <ta e="T11" id="Seg_447" s="T10">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T12" id="Seg_448" s="T11">overnight-PST.[3SG.S]</ta>
            <ta e="T13" id="Seg_449" s="T12">I.ACC</ta>
            <ta e="T14" id="Seg_450" s="T13">%rob-PST.[3SG.S]</ta>
            <ta e="T15" id="Seg_451" s="T14">(s)he-PL-ACC</ta>
            <ta e="T16" id="Seg_452" s="T15">overnight-INF</ta>
            <ta e="T17" id="Seg_453" s="T16">NEG</ta>
            <ta e="T18" id="Seg_454" s="T17">let.go-HAB-FUT-1SG.O</ta>
            <ta e="T19" id="Seg_455" s="T18">one-EP-ADVZ</ta>
            <ta e="T20" id="Seg_456" s="T19">I.NOM</ta>
            <ta e="T21" id="Seg_457" s="T20">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T22" id="Seg_458" s="T21">overnight-INF</ta>
            <ta e="T23" id="Seg_459" s="T22">let.go-1SG.O</ta>
            <ta e="T24" id="Seg_460" s="T23">(s)he.[NOM]</ta>
            <ta e="T25" id="Seg_461" s="T24">I.ACC</ta>
            <ta e="T26" id="Seg_462" s="T25">%rob-CO-3SG.S</ta>
            <ta e="T27" id="Seg_463" s="T26">skirt.[NOM]-1SG</ta>
            <ta e="T28" id="Seg_464" s="T27">%steal-INFER-3SG.O</ta>
            <ta e="T29" id="Seg_465" s="T28">and</ta>
            <ta e="T30" id="Seg_466" s="T29">jersey.[NOM]-1SG</ta>
            <ta e="T31" id="Seg_467" s="T30">and</ta>
            <ta e="T32" id="Seg_468" s="T31">%shawl.[NOM]-1SG</ta>
            <ta e="T33" id="Seg_469" s="T32">%steal-INFER-3SG.O</ta>
            <ta e="T34" id="Seg_470" s="T33">we.[NOM]</ta>
            <ta e="T35" id="Seg_471" s="T34">(s)he-EP-ACC</ta>
            <ta e="T36" id="Seg_472" s="T35">pursue-CVB</ta>
            <ta e="T37" id="Seg_473" s="T36">leave-INFER-1DU</ta>
            <ta e="T38" id="Seg_474" s="T37">NEG</ta>
            <ta e="T39" id="Seg_475" s="T38">achieve-DRV-1DU</ta>
            <ta e="T40" id="Seg_476" s="T39">(s)he.[NOM]</ta>
            <ta e="T41" id="Seg_477" s="T40">I.GEN</ta>
            <ta e="T42" id="Seg_478" s="T41">clothes.[NOM]-1SG</ta>
            <ta e="T43" id="Seg_479" s="T42">Pleshivka-LOC</ta>
            <ta e="T44" id="Seg_480" s="T43">all</ta>
            <ta e="T45" id="Seg_481" s="T44">sell-PST.NAR-3SG.O</ta>
            <ta e="T46" id="Seg_482" s="T45">I.NOM</ta>
            <ta e="T47" id="Seg_483" s="T46">IRREAL</ta>
            <ta e="T48" id="Seg_484" s="T47">now</ta>
            <ta e="T49" id="Seg_485" s="T48">IRREAL</ta>
            <ta e="T50" id="Seg_486" s="T49">wear-CONJ-1SG.O</ta>
            <ta e="T51" id="Seg_487" s="T50">I.NOM</ta>
            <ta e="T52" id="Seg_488" s="T51">cry-PST-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_489" s="T1">а</ta>
            <ta e="T3" id="Seg_490" s="T2">красть-PTCP.PRS2</ta>
            <ta e="T4" id="Seg_491" s="T3">человек-PL-ACC</ta>
            <ta e="T5" id="Seg_492" s="T4">убить-HAB-CO-3PL</ta>
            <ta e="T6" id="Seg_493" s="T5">он(а)-PL.[NOM]</ta>
            <ta e="T7" id="Seg_494" s="T6">чтобы</ta>
            <ta e="T8" id="Seg_495" s="T7">NEG.IMP</ta>
            <ta e="T9" id="Seg_496" s="T8">красть-IMP.3-IMP.3SG/PL.O</ta>
            <ta e="T10" id="Seg_497" s="T9">я-ADES</ta>
            <ta e="T11" id="Seg_498" s="T10">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T12" id="Seg_499" s="T11">ночевать-PST.[3SG.S]</ta>
            <ta e="T13" id="Seg_500" s="T12">я.ACC</ta>
            <ta e="T14" id="Seg_501" s="T13">%обокрасть-PST.[3SG.S]</ta>
            <ta e="T15" id="Seg_502" s="T14">он(а)-PL-ACC</ta>
            <ta e="T16" id="Seg_503" s="T15">ночевать-INF</ta>
            <ta e="T17" id="Seg_504" s="T16">NEG</ta>
            <ta e="T18" id="Seg_505" s="T17">пустить-HAB-FUT-1SG.O</ta>
            <ta e="T19" id="Seg_506" s="T18">один-EP-ADVZ</ta>
            <ta e="T20" id="Seg_507" s="T19">я.NOM</ta>
            <ta e="T21" id="Seg_508" s="T20">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T22" id="Seg_509" s="T21">ночевать-INF</ta>
            <ta e="T23" id="Seg_510" s="T22">пустить-1SG.O</ta>
            <ta e="T24" id="Seg_511" s="T23">он(а).[NOM]</ta>
            <ta e="T25" id="Seg_512" s="T24">я.ACC</ta>
            <ta e="T26" id="Seg_513" s="T25">%обокрасть-CO-3SG.S</ta>
            <ta e="T27" id="Seg_514" s="T26">юбка.[NOM]-1SG</ta>
            <ta e="T28" id="Seg_515" s="T27">%украсть-INFER-3SG.O</ta>
            <ta e="T29" id="Seg_516" s="T28">и</ta>
            <ta e="T30" id="Seg_517" s="T29">кофта.[NOM]-1SG</ta>
            <ta e="T31" id="Seg_518" s="T30">и</ta>
            <ta e="T32" id="Seg_519" s="T31">%полушалок.[NOM]-1SG</ta>
            <ta e="T33" id="Seg_520" s="T32">%украсть-INFER-3SG.O</ta>
            <ta e="T34" id="Seg_521" s="T33">мы.[NOM]</ta>
            <ta e="T35" id="Seg_522" s="T34">он(а)-EP-ACC</ta>
            <ta e="T36" id="Seg_523" s="T35">преследовать-CVB</ta>
            <ta e="T37" id="Seg_524" s="T36">отправиться-INFER-1DU</ta>
            <ta e="T38" id="Seg_525" s="T37">NEG</ta>
            <ta e="T39" id="Seg_526" s="T38">достичь-DRV-1DU</ta>
            <ta e="T40" id="Seg_527" s="T39">он(а).[NOM]</ta>
            <ta e="T41" id="Seg_528" s="T40">я.GEN</ta>
            <ta e="T42" id="Seg_529" s="T41">одежда.[NOM]-1SG</ta>
            <ta e="T43" id="Seg_530" s="T42">Плешивка-LOC</ta>
            <ta e="T44" id="Seg_531" s="T43">весь</ta>
            <ta e="T45" id="Seg_532" s="T44">продать-PST.NAR-3SG.O</ta>
            <ta e="T46" id="Seg_533" s="T45">я.NOM</ta>
            <ta e="T47" id="Seg_534" s="T46">IRREAL</ta>
            <ta e="T48" id="Seg_535" s="T47">сейчас</ta>
            <ta e="T49" id="Seg_536" s="T48">IRREAL</ta>
            <ta e="T50" id="Seg_537" s="T49">носить-CONJ-1SG.O</ta>
            <ta e="T51" id="Seg_538" s="T50">я.NOM</ta>
            <ta e="T52" id="Seg_539" s="T51">плакать-PST-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_540" s="T1">conj</ta>
            <ta e="T3" id="Seg_541" s="T2">v-v&gt;ptcp</ta>
            <ta e="T4" id="Seg_542" s="T3">n-n:num-n:case</ta>
            <ta e="T5" id="Seg_543" s="T4">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T6" id="Seg_544" s="T5">pers-n:num.[n:case]</ta>
            <ta e="T7" id="Seg_545" s="T6">conj</ta>
            <ta e="T8" id="Seg_546" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_547" s="T8">v-v:mood.pn-v:mood.pn</ta>
            <ta e="T10" id="Seg_548" s="T9">pers-n:case</ta>
            <ta e="T11" id="Seg_549" s="T10">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T12" id="Seg_550" s="T11">v-v:tense.[v:pn]</ta>
            <ta e="T13" id="Seg_551" s="T12">pers</ta>
            <ta e="T14" id="Seg_552" s="T13">v-v:tense.[v:pn]</ta>
            <ta e="T15" id="Seg_553" s="T14">pers-n:num-n:case</ta>
            <ta e="T16" id="Seg_554" s="T15">v-v:inf</ta>
            <ta e="T17" id="Seg_555" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_556" s="T17">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_557" s="T18">num-n:ins-quant&gt;adv</ta>
            <ta e="T20" id="Seg_558" s="T19">pers</ta>
            <ta e="T21" id="Seg_559" s="T20">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T22" id="Seg_560" s="T21">v-v:inf</ta>
            <ta e="T23" id="Seg_561" s="T22">v-v:pn</ta>
            <ta e="T24" id="Seg_562" s="T23">pers.[n:case]</ta>
            <ta e="T25" id="Seg_563" s="T24">pers</ta>
            <ta e="T26" id="Seg_564" s="T25">v-v:ins-v:pn</ta>
            <ta e="T27" id="Seg_565" s="T26">n.[n:case]-n:poss</ta>
            <ta e="T28" id="Seg_566" s="T27">v-v:mood-v:pn</ta>
            <ta e="T29" id="Seg_567" s="T28">conj</ta>
            <ta e="T30" id="Seg_568" s="T29">n.[n:case]-n:poss</ta>
            <ta e="T31" id="Seg_569" s="T30">conj</ta>
            <ta e="T32" id="Seg_570" s="T31">n.[n:case]-n:poss</ta>
            <ta e="T33" id="Seg_571" s="T32">v-v:mood-v:pn</ta>
            <ta e="T34" id="Seg_572" s="T33">pers.[n:case]</ta>
            <ta e="T35" id="Seg_573" s="T34">pers-n:ins-n:case</ta>
            <ta e="T36" id="Seg_574" s="T35">v-v&gt;adv</ta>
            <ta e="T37" id="Seg_575" s="T36">v-v:mood-v:pn</ta>
            <ta e="T38" id="Seg_576" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_577" s="T38">v-v&gt;v-v:pn</ta>
            <ta e="T40" id="Seg_578" s="T39">pers.[n:case]</ta>
            <ta e="T41" id="Seg_579" s="T40">pers</ta>
            <ta e="T42" id="Seg_580" s="T41">n.[n:case]-n:poss</ta>
            <ta e="T43" id="Seg_581" s="T42">nprop-n:case</ta>
            <ta e="T44" id="Seg_582" s="T43">quant</ta>
            <ta e="T45" id="Seg_583" s="T44">v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_584" s="T45">pers</ta>
            <ta e="T47" id="Seg_585" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_586" s="T47">adv</ta>
            <ta e="T49" id="Seg_587" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_588" s="T49">v-v:mood-v:pn</ta>
            <ta e="T51" id="Seg_589" s="T50">pers</ta>
            <ta e="T52" id="Seg_590" s="T51">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_591" s="T1">conj</ta>
            <ta e="T3" id="Seg_592" s="T2">v</ta>
            <ta e="T4" id="Seg_593" s="T3">n</ta>
            <ta e="T5" id="Seg_594" s="T4">v</ta>
            <ta e="T6" id="Seg_595" s="T5">pers</ta>
            <ta e="T7" id="Seg_596" s="T6">conj</ta>
            <ta e="T8" id="Seg_597" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_598" s="T8">v</ta>
            <ta e="T10" id="Seg_599" s="T9">pers</ta>
            <ta e="T11" id="Seg_600" s="T10">n</ta>
            <ta e="T12" id="Seg_601" s="T11">v</ta>
            <ta e="T13" id="Seg_602" s="T12">pers</ta>
            <ta e="T14" id="Seg_603" s="T13">v</ta>
            <ta e="T15" id="Seg_604" s="T14">pers</ta>
            <ta e="T16" id="Seg_605" s="T15">v</ta>
            <ta e="T17" id="Seg_606" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_607" s="T17">v</ta>
            <ta e="T19" id="Seg_608" s="T18">adv</ta>
            <ta e="T20" id="Seg_609" s="T19">pers</ta>
            <ta e="T21" id="Seg_610" s="T20">n</ta>
            <ta e="T22" id="Seg_611" s="T21">v</ta>
            <ta e="T23" id="Seg_612" s="T22">v</ta>
            <ta e="T24" id="Seg_613" s="T23">pers</ta>
            <ta e="T25" id="Seg_614" s="T24">pers</ta>
            <ta e="T26" id="Seg_615" s="T25">v</ta>
            <ta e="T27" id="Seg_616" s="T26">n</ta>
            <ta e="T28" id="Seg_617" s="T27">v</ta>
            <ta e="T29" id="Seg_618" s="T28">conj</ta>
            <ta e="T30" id="Seg_619" s="T29">n</ta>
            <ta e="T31" id="Seg_620" s="T30">conj</ta>
            <ta e="T32" id="Seg_621" s="T31">n</ta>
            <ta e="T33" id="Seg_622" s="T32">v</ta>
            <ta e="T34" id="Seg_623" s="T33">pers</ta>
            <ta e="T35" id="Seg_624" s="T34">pers</ta>
            <ta e="T36" id="Seg_625" s="T35">adv</ta>
            <ta e="T37" id="Seg_626" s="T36">v</ta>
            <ta e="T38" id="Seg_627" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_628" s="T38">v</ta>
            <ta e="T40" id="Seg_629" s="T39">pers</ta>
            <ta e="T41" id="Seg_630" s="T40">pers</ta>
            <ta e="T42" id="Seg_631" s="T41">n</ta>
            <ta e="T43" id="Seg_632" s="T42">nprop</ta>
            <ta e="T44" id="Seg_633" s="T43">quant</ta>
            <ta e="T45" id="Seg_634" s="T44">v</ta>
            <ta e="T46" id="Seg_635" s="T45">pers</ta>
            <ta e="T47" id="Seg_636" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_637" s="T47">adv</ta>
            <ta e="T49" id="Seg_638" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_639" s="T49">v</ta>
            <ta e="T51" id="Seg_640" s="T50">pers</ta>
            <ta e="T52" id="Seg_641" s="T51">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T4" id="Seg_642" s="T3">np.h:P</ta>
            <ta e="T5" id="Seg_643" s="T4">0.3.h:A</ta>
            <ta e="T6" id="Seg_644" s="T5">pro.h:A</ta>
            <ta e="T10" id="Seg_645" s="T9">pro.h:L</ta>
            <ta e="T11" id="Seg_646" s="T10">np.h:Th</ta>
            <ta e="T13" id="Seg_647" s="T12">pro.h:Th</ta>
            <ta e="T14" id="Seg_648" s="T13">0.3.h:A</ta>
            <ta e="T15" id="Seg_649" s="T14">pro.h:Th</ta>
            <ta e="T18" id="Seg_650" s="T17">0.1.h:A</ta>
            <ta e="T19" id="Seg_651" s="T18">adv:Time</ta>
            <ta e="T20" id="Seg_652" s="T19">pro.h:A</ta>
            <ta e="T21" id="Seg_653" s="T20">np.h:Th</ta>
            <ta e="T24" id="Seg_654" s="T23">pro.h:A</ta>
            <ta e="T25" id="Seg_655" s="T24">pro.h:Th</ta>
            <ta e="T27" id="Seg_656" s="T26">np:Th 0.1.h:Poss</ta>
            <ta e="T28" id="Seg_657" s="T27">0.3.h:A</ta>
            <ta e="T30" id="Seg_658" s="T29">np:Th 0.1.h:Poss</ta>
            <ta e="T32" id="Seg_659" s="T31">np:Th 0.1.h:Poss</ta>
            <ta e="T33" id="Seg_660" s="T32">0.3.h:A</ta>
            <ta e="T34" id="Seg_661" s="T33">pro.h:A</ta>
            <ta e="T35" id="Seg_662" s="T34">pro.h:P</ta>
            <ta e="T39" id="Seg_663" s="T38">0.1.h:A</ta>
            <ta e="T40" id="Seg_664" s="T39">pro.h:A</ta>
            <ta e="T41" id="Seg_665" s="T40">pro.h:Poss</ta>
            <ta e="T42" id="Seg_666" s="T41">np:Th</ta>
            <ta e="T43" id="Seg_667" s="T42">np:L</ta>
            <ta e="T46" id="Seg_668" s="T45">pro.h:A</ta>
            <ta e="T48" id="Seg_669" s="T47">adv:Time</ta>
            <ta e="T50" id="Seg_670" s="T49">0.3:Th</ta>
            <ta e="T51" id="Seg_671" s="T50">pro.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_672" s="T3">np.h:O</ta>
            <ta e="T5" id="Seg_673" s="T4">0.3.h:S v:pred</ta>
            <ta e="T9" id="Seg_674" s="T5">s:purp</ta>
            <ta e="T11" id="Seg_675" s="T10">np.h:S</ta>
            <ta e="T12" id="Seg_676" s="T11">v:pred</ta>
            <ta e="T13" id="Seg_677" s="T12">pro.h:O</ta>
            <ta e="T14" id="Seg_678" s="T13">0.3.h:S v:pred</ta>
            <ta e="T15" id="Seg_679" s="T14">pro.h:O</ta>
            <ta e="T16" id="Seg_680" s="T15">s:purp</ta>
            <ta e="T18" id="Seg_681" s="T17">0.1.h:S v:pred</ta>
            <ta e="T20" id="Seg_682" s="T19">pro.h:S</ta>
            <ta e="T21" id="Seg_683" s="T20">np.h:O</ta>
            <ta e="T22" id="Seg_684" s="T21">s:purp</ta>
            <ta e="T23" id="Seg_685" s="T22">v:pred</ta>
            <ta e="T24" id="Seg_686" s="T23">pro.h:S</ta>
            <ta e="T25" id="Seg_687" s="T24">pro.h:O</ta>
            <ta e="T26" id="Seg_688" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_689" s="T26">np:O</ta>
            <ta e="T28" id="Seg_690" s="T27">0.3.h:S v:pred</ta>
            <ta e="T30" id="Seg_691" s="T29">np:O</ta>
            <ta e="T32" id="Seg_692" s="T31">np:O</ta>
            <ta e="T33" id="Seg_693" s="T32">0.3.h:S v:pred</ta>
            <ta e="T34" id="Seg_694" s="T33">pro.h:S</ta>
            <ta e="T36" id="Seg_695" s="T34">s:purp</ta>
            <ta e="T37" id="Seg_696" s="T36">v:pred</ta>
            <ta e="T39" id="Seg_697" s="T38">0.1.h:S v:pred</ta>
            <ta e="T40" id="Seg_698" s="T39">pro.h:S</ta>
            <ta e="T42" id="Seg_699" s="T41">np:O</ta>
            <ta e="T45" id="Seg_700" s="T44">v:pred</ta>
            <ta e="T46" id="Seg_701" s="T45">pro.h:S</ta>
            <ta e="T50" id="Seg_702" s="T49">v:pred 0.3:O</ta>
            <ta e="T51" id="Seg_703" s="T50">pro.h:S</ta>
            <ta e="T52" id="Seg_704" s="T51">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_705" s="T1">RUS:gram</ta>
            <ta e="T7" id="Seg_706" s="T6">RUS:gram</ta>
            <ta e="T27" id="Seg_707" s="T26">RUS:cult</ta>
            <ta e="T29" id="Seg_708" s="T28">RUS:gram</ta>
            <ta e="T30" id="Seg_709" s="T29">RUS:cult</ta>
            <ta e="T31" id="Seg_710" s="T30">RUS:gram</ta>
            <ta e="T32" id="Seg_711" s="T31">%RUS:cult</ta>
            <ta e="T42" id="Seg_712" s="T41">RUS:cult</ta>
            <ta e="T44" id="Seg_713" s="T43">RUS:core</ta>
            <ta e="T47" id="Seg_714" s="T46">RUS:gram</ta>
            <ta e="T48" id="Seg_715" s="T47">RUS:core</ta>
            <ta e="T49" id="Seg_716" s="T48">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T9" id="Seg_717" s="T1">And those people, who steal, they are beaten, so that they don't steal.</ta>
            <ta e="T12" id="Seg_718" s="T9">One woman spent a night in my house.</ta>
            <ta e="T14" id="Seg_719" s="T12">She robbed me.</ta>
            <ta e="T18" id="Seg_720" s="T14">I won't let them in to overnight.</ta>
            <ta e="T23" id="Seg_721" s="T18">Once I let a woman to overnight in my house.</ta>
            <ta e="T26" id="Seg_722" s="T23">She robbed me.</ta>
            <ta e="T33" id="Seg_723" s="T26">She stole a skirt, a jersey and a shawl(?).</ta>
            <ta e="T39" id="Seg_724" s="T33">We pursued her, but didn't manage to catch her.</ta>
            <ta e="T45" id="Seg_725" s="T39">She sold all of my clothes in Pleshivka.</ta>
            <ta e="T50" id="Seg_726" s="T45">I could wear it now.</ta>
            <ta e="T52" id="Seg_727" s="T50">I cried.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T9" id="Seg_728" s="T1">Und diese Leute, die stehlen, sie werden geschlagen, damit sie nicht stehlen.</ta>
            <ta e="T12" id="Seg_729" s="T9">Eine Frau übernachtete bei mir.</ta>
            <ta e="T14" id="Seg_730" s="T12">Sie hat mich ausgeraubt.</ta>
            <ta e="T18" id="Seg_731" s="T14">Ich lasse sie nicht mehr hinein zum Übernachten.</ta>
            <ta e="T23" id="Seg_732" s="T18">Einmal ließ ich eine Frau bei mir übernachten.</ta>
            <ta e="T26" id="Seg_733" s="T23">Sie hat mich ausgeraubt.</ta>
            <ta e="T33" id="Seg_734" s="T26">Sie hat einen Rock, eine Jacke und einen Schal(?) gestohlen.</ta>
            <ta e="T39" id="Seg_735" s="T33">Wir verfolgten sie, aber wir schnappten sie nicht.</ta>
            <ta e="T45" id="Seg_736" s="T39">Sie hat all meine Kleider in Pleshivka verkauft.</ta>
            <ta e="T50" id="Seg_737" s="T45">Ich könnte sie jetzt tragen.</ta>
            <ta e="T52" id="Seg_738" s="T50">Ich habe geweint.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T9" id="Seg_739" s="T1">А людей, который воруют, тех бьют, чтобы они не воровали.</ta>
            <ta e="T12" id="Seg_740" s="T9">У меня женщина ночевала.</ta>
            <ta e="T14" id="Seg_741" s="T12">Меня обокрала.</ta>
            <ta e="T18" id="Seg_742" s="T14">Их теперь ночевать не буду пускать.</ta>
            <ta e="T23" id="Seg_743" s="T18">Один раз я женщину ночевать пустила.</ta>
            <ta e="T26" id="Seg_744" s="T23">Она меня обокрала.</ta>
            <ta e="T33" id="Seg_745" s="T26">Юбку украла, и кофту, и полушалок украла.</ta>
            <ta e="T39" id="Seg_746" s="T33">Мы погнались за ней, не догнали.</ta>
            <ta e="T45" id="Seg_747" s="T39">Она мою одежду в Плешивке всю продала.</ta>
            <ta e="T50" id="Seg_748" s="T45">Я бы сейчас бы носила.</ta>
            <ta e="T52" id="Seg_749" s="T50">Я плакала.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T9" id="Seg_750" s="T1">кто воруют тех бьют чтобы они не воровали</ta>
            <ta e="T12" id="Seg_751" s="T9">у меня женщина ночевала</ta>
            <ta e="T14" id="Seg_752" s="T12">меня обокрала</ta>
            <ta e="T18" id="Seg_753" s="T14">их теперь ночевать не буду пускать</ta>
            <ta e="T23" id="Seg_754" s="T18">один раз я женщину ночевать пустила</ta>
            <ta e="T26" id="Seg_755" s="T23">она меня обокрала</ta>
            <ta e="T33" id="Seg_756" s="T26">юбку украла и кофту и полушалок украла</ta>
            <ta e="T39" id="Seg_757" s="T33">мы ее погнали (погнались за ней) не догнали</ta>
            <ta e="T45" id="Seg_758" s="T39">она мою одежду в Плешивке всю продала</ta>
            <ta e="T50" id="Seg_759" s="T45">я бы сейчас бы носила</ta>
            <ta e="T52" id="Seg_760" s="T50">я плакала</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T23" id="Seg_761" s="T18">[BrM:] 'Okɨr ɨn' changed to 'Okɨrɨn'</ta>
            <ta e="T39" id="Seg_762" s="T33">[BrM:] Tentative analysis of 'qwatdaj'.</ta>
            <ta e="T45" id="Seg_763" s="T39">[KuAI:] Variant: 'Plʼešiwkaɣɨn'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
