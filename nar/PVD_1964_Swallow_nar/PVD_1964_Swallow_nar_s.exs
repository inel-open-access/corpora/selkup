<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Swallow_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Swallow_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">21</ud-information>
            <ud-information attribute-name="# HIAT:w">16</ud-information>
            <ud-information attribute-name="# e">16</ud-information>
            <ud-information attribute-name="# HIAT:u">3</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T17" id="Seg_0" n="sc" s="T1">
               <ts e="T8" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Köɣajka</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">jeʒlʼi</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">mattə</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">taɣɨdʼan</ts>
                  <nts id="Seg_14" n="HIAT:ip">,</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_17" n="HIAT:w" s="T5">to</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_20" n="HIAT:w" s="T6">qaga</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_23" n="HIAT:w" s="T7">jenɨš</ts>
                  <nts id="Seg_24" n="HIAT:ip">.</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_27" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">Me</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">amdɨzautə</ts>
                  <nts id="Seg_33" n="HIAT:ip">,</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_36" n="HIAT:w" s="T10">a</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_39" n="HIAT:w" s="T11">koɣajka</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_42" n="HIAT:w" s="T12">mattɨ</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_45" n="HIAT:w" s="T13">taɣɨdʼan</ts>
                  <nts id="Seg_46" n="HIAT:ip">.</nts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_49" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_51" n="HIAT:w" s="T14">I</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_54" n="HIAT:w" s="T15">Rekajkin</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_57" n="HIAT:w" s="T16">quːa</ts>
                  <nts id="Seg_58" n="HIAT:ip">.</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T17" id="Seg_60" n="sc" s="T1">
               <ts e="T2" id="Seg_62" n="e" s="T1">Köɣajka </ts>
               <ts e="T3" id="Seg_64" n="e" s="T2">jeʒlʼi </ts>
               <ts e="T4" id="Seg_66" n="e" s="T3">mattə </ts>
               <ts e="T5" id="Seg_68" n="e" s="T4">taɣɨdʼan, </ts>
               <ts e="T6" id="Seg_70" n="e" s="T5">to </ts>
               <ts e="T7" id="Seg_72" n="e" s="T6">qaga </ts>
               <ts e="T8" id="Seg_74" n="e" s="T7">jenɨš. </ts>
               <ts e="T9" id="Seg_76" n="e" s="T8">Me </ts>
               <ts e="T10" id="Seg_78" n="e" s="T9">amdɨzautə, </ts>
               <ts e="T11" id="Seg_80" n="e" s="T10">a </ts>
               <ts e="T12" id="Seg_82" n="e" s="T11">koɣajka </ts>
               <ts e="T13" id="Seg_84" n="e" s="T12">mattɨ </ts>
               <ts e="T14" id="Seg_86" n="e" s="T13">taɣɨdʼan. </ts>
               <ts e="T15" id="Seg_88" n="e" s="T14">I </ts>
               <ts e="T16" id="Seg_90" n="e" s="T15">Rekajkin </ts>
               <ts e="T17" id="Seg_92" n="e" s="T16">quːa. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T8" id="Seg_93" s="T1">PVD_1964_Swallow_nar.001 (001.001)</ta>
            <ta e="T14" id="Seg_94" s="T8">PVD_1964_Swallow_nar.002 (001.002)</ta>
            <ta e="T17" id="Seg_95" s="T14">PVD_1964_Swallow_nar.003 (001.003)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T8" id="Seg_96" s="T1">′кӧɣайка ′jежлʼи ′маттъ ′таɣыдʼан, то ′kага ′jеныш.</ta>
            <ta e="T14" id="Seg_97" s="T8">ме ′амдызау(ф)тъ, а ко′ɣайка ′матты ′таɣыдʼан.</ta>
            <ta e="T17" id="Seg_98" s="T14">и Ре′кайкин ′kӯ͜а.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T8" id="Seg_99" s="T1">köɣajka jeʒlʼi mattə taɣɨdʼan, to qaga jenɨš.</ta>
            <ta e="T14" id="Seg_100" s="T8">me amdɨzau(f)tə, a koɣajka mattɨ taɣɨdʼan.</ta>
            <ta e="T17" id="Seg_101" s="T14">i Рekajkin quː͜a.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T8" id="Seg_102" s="T1">Köɣajka jeʒlʼi mattə taɣɨdʼan, to qaga jenɨš. </ta>
            <ta e="T14" id="Seg_103" s="T8">Me amdɨzautə, a koɣajka mattɨ taɣɨdʼan. </ta>
            <ta e="T17" id="Seg_104" s="T14">I Rekajkin quːa. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_105" s="T1">köɣajka</ta>
            <ta e="T3" id="Seg_106" s="T2">jeʒlʼi</ta>
            <ta e="T4" id="Seg_107" s="T3">mat-tə</ta>
            <ta e="T5" id="Seg_108" s="T4">taɣɨdʼa-n</ta>
            <ta e="T6" id="Seg_109" s="T5">to</ta>
            <ta e="T7" id="Seg_110" s="T6">qaga</ta>
            <ta e="T8" id="Seg_111" s="T7">je-nɨš</ta>
            <ta e="T9" id="Seg_112" s="T8">me</ta>
            <ta e="T10" id="Seg_113" s="T9">amdɨ-za-utə</ta>
            <ta e="T11" id="Seg_114" s="T10">a</ta>
            <ta e="T12" id="Seg_115" s="T11">koɣajka</ta>
            <ta e="T13" id="Seg_116" s="T12">mat-tɨ</ta>
            <ta e="T14" id="Seg_117" s="T13">taɣɨdʼa-n</ta>
            <ta e="T15" id="Seg_118" s="T14">i</ta>
            <ta e="T16" id="Seg_119" s="T15">Rekajkin</ta>
            <ta e="T17" id="Seg_120" s="T16">quː-a</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_121" s="T1">koɣajka</ta>
            <ta e="T3" id="Seg_122" s="T2">jeʒlʼe</ta>
            <ta e="T4" id="Seg_123" s="T3">maːt-ntə</ta>
            <ta e="T5" id="Seg_124" s="T4">taɣətʼe-n</ta>
            <ta e="T6" id="Seg_125" s="T5">to</ta>
            <ta e="T7" id="Seg_126" s="T6">qaga</ta>
            <ta e="T8" id="Seg_127" s="T7">eː-nɨš</ta>
            <ta e="T9" id="Seg_128" s="T8">me</ta>
            <ta e="T10" id="Seg_129" s="T9">amdɨ-sɨ-un</ta>
            <ta e="T11" id="Seg_130" s="T10">a</ta>
            <ta e="T12" id="Seg_131" s="T11">koɣajka</ta>
            <ta e="T13" id="Seg_132" s="T12">maːt-ntə</ta>
            <ta e="T14" id="Seg_133" s="T13">taɣətʼe-n</ta>
            <ta e="T15" id="Seg_134" s="T14">i</ta>
            <ta e="T16" id="Seg_135" s="T15">Rekajkin</ta>
            <ta e="T17" id="Seg_136" s="T16">quː-ɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_137" s="T1">swallow.[NOM]</ta>
            <ta e="T3" id="Seg_138" s="T2">if</ta>
            <ta e="T4" id="Seg_139" s="T3">house-ILL</ta>
            <ta e="T5" id="Seg_140" s="T4">fly.into-3SG.S</ta>
            <ta e="T6" id="Seg_141" s="T5">then</ta>
            <ta e="T7" id="Seg_142" s="T6">the.deceased.[NOM]</ta>
            <ta e="T8" id="Seg_143" s="T7">be-POT.FUT.3SG</ta>
            <ta e="T9" id="Seg_144" s="T8">we.[NOM]</ta>
            <ta e="T10" id="Seg_145" s="T9">sit-PST-1PL</ta>
            <ta e="T11" id="Seg_146" s="T10">and</ta>
            <ta e="T12" id="Seg_147" s="T11">swallow.[NOM]</ta>
            <ta e="T13" id="Seg_148" s="T12">house-ILL</ta>
            <ta e="T14" id="Seg_149" s="T13">fly.into-3SG.S</ta>
            <ta e="T15" id="Seg_150" s="T14">and</ta>
            <ta e="T16" id="Seg_151" s="T15">Rekajkin.[NOM]</ta>
            <ta e="T17" id="Seg_152" s="T16">die-EP.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_153" s="T1">ласточка.[NOM]</ta>
            <ta e="T3" id="Seg_154" s="T2">ежели</ta>
            <ta e="T4" id="Seg_155" s="T3">дом-ILL</ta>
            <ta e="T5" id="Seg_156" s="T4">влететь-3SG.S</ta>
            <ta e="T6" id="Seg_157" s="T5">то</ta>
            <ta e="T7" id="Seg_158" s="T6">покойник.[NOM]</ta>
            <ta e="T8" id="Seg_159" s="T7">быть-POT.FUT.3SG</ta>
            <ta e="T9" id="Seg_160" s="T8">мы.[NOM]</ta>
            <ta e="T10" id="Seg_161" s="T9">сидеть-PST-1PL</ta>
            <ta e="T11" id="Seg_162" s="T10">а</ta>
            <ta e="T12" id="Seg_163" s="T11">ласточка.[NOM]</ta>
            <ta e="T13" id="Seg_164" s="T12">дом-ILL</ta>
            <ta e="T14" id="Seg_165" s="T13">влететь-3SG.S</ta>
            <ta e="T15" id="Seg_166" s="T14">и</ta>
            <ta e="T16" id="Seg_167" s="T15">Рекайкин.[NOM]</ta>
            <ta e="T17" id="Seg_168" s="T16">умереть-EP.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_169" s="T1">n.[n:case]</ta>
            <ta e="T3" id="Seg_170" s="T2">conj</ta>
            <ta e="T4" id="Seg_171" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_172" s="T4">v-v:pn</ta>
            <ta e="T6" id="Seg_173" s="T5">conj</ta>
            <ta e="T7" id="Seg_174" s="T6">n.[n:case]</ta>
            <ta e="T8" id="Seg_175" s="T7">v-v:tense</ta>
            <ta e="T9" id="Seg_176" s="T8">pers.[n:case]</ta>
            <ta e="T10" id="Seg_177" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_178" s="T10">conj</ta>
            <ta e="T12" id="Seg_179" s="T11">n.[n:case]</ta>
            <ta e="T13" id="Seg_180" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_181" s="T13">v-v:pn</ta>
            <ta e="T15" id="Seg_182" s="T14">conj</ta>
            <ta e="T16" id="Seg_183" s="T15">nprop.[n:case]</ta>
            <ta e="T17" id="Seg_184" s="T16">v-n:ins.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_185" s="T1">n</ta>
            <ta e="T3" id="Seg_186" s="T2">conj</ta>
            <ta e="T4" id="Seg_187" s="T3">n</ta>
            <ta e="T5" id="Seg_188" s="T4">v</ta>
            <ta e="T6" id="Seg_189" s="T5">conj</ta>
            <ta e="T7" id="Seg_190" s="T6">n</ta>
            <ta e="T8" id="Seg_191" s="T7">v</ta>
            <ta e="T9" id="Seg_192" s="T8">pers</ta>
            <ta e="T10" id="Seg_193" s="T9">v</ta>
            <ta e="T11" id="Seg_194" s="T10">conj</ta>
            <ta e="T12" id="Seg_195" s="T11">n</ta>
            <ta e="T13" id="Seg_196" s="T12">n</ta>
            <ta e="T14" id="Seg_197" s="T13">v</ta>
            <ta e="T15" id="Seg_198" s="T14">conj</ta>
            <ta e="T16" id="Seg_199" s="T15">n</ta>
            <ta e="T17" id="Seg_200" s="T16">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_201" s="T1">np:A</ta>
            <ta e="T4" id="Seg_202" s="T3">np:G</ta>
            <ta e="T7" id="Seg_203" s="T6">np.h:Th</ta>
            <ta e="T9" id="Seg_204" s="T8">pro.h:Th</ta>
            <ta e="T12" id="Seg_205" s="T11">np:A</ta>
            <ta e="T13" id="Seg_206" s="T12">np:G</ta>
            <ta e="T16" id="Seg_207" s="T15">np.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T5" id="Seg_208" s="T1">s:cond</ta>
            <ta e="T7" id="Seg_209" s="T6">np.h:S</ta>
            <ta e="T8" id="Seg_210" s="T7">v:pred</ta>
            <ta e="T9" id="Seg_211" s="T8">pro.h:S</ta>
            <ta e="T10" id="Seg_212" s="T9">v:pred</ta>
            <ta e="T12" id="Seg_213" s="T11">np:S</ta>
            <ta e="T14" id="Seg_214" s="T13">v:pred</ta>
            <ta e="T16" id="Seg_215" s="T15">np.h:S</ta>
            <ta e="T17" id="Seg_216" s="T16">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_217" s="T2">RUS:gram</ta>
            <ta e="T6" id="Seg_218" s="T5">RUS:gram</ta>
            <ta e="T11" id="Seg_219" s="T10">RUS:gram</ta>
            <ta e="T15" id="Seg_220" s="T14">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T8" id="Seg_221" s="T1">If a swallow flies into a house, there will be a deceased.</ta>
            <ta e="T14" id="Seg_222" s="T8">We were sitting, and a swallow flew into the house.</ta>
            <ta e="T17" id="Seg_223" s="T14">And Rekajkin died.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T8" id="Seg_224" s="T1">Wenn eine Schwalbe ins Haus fliegt, wird es Tote geben.</ta>
            <ta e="T14" id="Seg_225" s="T8">Wir saßen und eine Schwalbe flog ins Haus.</ta>
            <ta e="T17" id="Seg_226" s="T14">Und Rekajkin starb.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T8" id="Seg_227" s="T1">Если ласточка в дом залетела, то покойник будет.</ta>
            <ta e="T14" id="Seg_228" s="T8">Мы сидели, а ласточка в дом залетела.</ta>
            <ta e="T17" id="Seg_229" s="T14">И Рекайкин умер.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T8" id="Seg_230" s="T1">ласточка покойник</ta>
            <ta e="T14" id="Seg_231" s="T8">мы сидели а ласточка в дом залетела</ta>
            <ta e="T17" id="Seg_232" s="T14">и Рекайкин умер</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T14" id="Seg_233" s="T8">[KuAI:] Variant: 'amdɨzaftə'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
