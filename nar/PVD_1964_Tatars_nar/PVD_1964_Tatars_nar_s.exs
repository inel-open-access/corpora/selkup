<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Tatars_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Tatars_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">42</ud-information>
            <ud-information attribute-name="# HIAT:w">28</ud-information>
            <ud-information attribute-name="# e">28</ud-information>
            <ud-information attribute-name="# HIAT:u">7</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T29" id="Seg_0" n="sc" s="T1">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Tebɨstaɣə</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">qwannaɣə</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_11" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">A</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Vanja</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">maːdam</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">nʼüut</ts>
                  <nts id="Seg_23" n="HIAT:ip">,</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">mekga</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">tʼarɨn</ts>
                  <nts id="Seg_30" n="HIAT:ip">:</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_32" n="HIAT:ip">“</nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">Qaj</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">tɨ</ts>
                  <nts id="Seg_38" n="HIAT:ip">?</nts>
                  <nts id="Seg_39" n="HIAT:ip">”</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_42" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">A</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">man</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">na</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">qaːrolʼdʼän</ts>
                  <nts id="Seg_54" n="HIAT:ip">:</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_56" n="HIAT:ip">“</nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">Tatarin</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip">”</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_63" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">Tɨ</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">üsedit</ts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_72" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_74" n="HIAT:w" s="T18">Mekga</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_77" n="HIAT:w" s="T19">aːwan</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">jes</ts>
                  <nts id="Seg_81" n="HIAT:ip">.</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_84" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_86" n="HIAT:w" s="T21">Täp</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_89" n="HIAT:w" s="T22">qajno</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_92" n="HIAT:w" s="T23">tɨtdɨn</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_95" n="HIAT:w" s="T24">soːgudʼzʼelʼe</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">jübɨrɨn</ts>
                  <nts id="Seg_99" n="HIAT:ip">.</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_102" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_104" n="HIAT:w" s="T26">Tülʼe</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_107" n="HIAT:w" s="T27">bɨ</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_110" n="HIAT:w" s="T28">sogudʼzʼenän</ts>
                  <nts id="Seg_111" n="HIAT:ip">.</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T29" id="Seg_113" n="sc" s="T1">
               <ts e="T2" id="Seg_115" n="e" s="T1">Tebɨstaɣə </ts>
               <ts e="T3" id="Seg_117" n="e" s="T2">qwannaɣə. </ts>
               <ts e="T4" id="Seg_119" n="e" s="T3">A </ts>
               <ts e="T5" id="Seg_121" n="e" s="T4">Vanja </ts>
               <ts e="T6" id="Seg_123" n="e" s="T5">maːdam </ts>
               <ts e="T7" id="Seg_125" n="e" s="T6">nʼüut, </ts>
               <ts e="T8" id="Seg_127" n="e" s="T7">mekga </ts>
               <ts e="T9" id="Seg_129" n="e" s="T8">tʼarɨn: </ts>
               <ts e="T10" id="Seg_131" n="e" s="T9">“Qaj </ts>
               <ts e="T11" id="Seg_133" n="e" s="T10">tɨ?” </ts>
               <ts e="T12" id="Seg_135" n="e" s="T11">A </ts>
               <ts e="T13" id="Seg_137" n="e" s="T12">man </ts>
               <ts e="T14" id="Seg_139" n="e" s="T13">na </ts>
               <ts e="T15" id="Seg_141" n="e" s="T14">qaːrolʼdʼän: </ts>
               <ts e="T16" id="Seg_143" n="e" s="T15">“Tatarin.” </ts>
               <ts e="T17" id="Seg_145" n="e" s="T16">Tɨ </ts>
               <ts e="T18" id="Seg_147" n="e" s="T17">üsedit. </ts>
               <ts e="T19" id="Seg_149" n="e" s="T18">Mekga </ts>
               <ts e="T20" id="Seg_151" n="e" s="T19">aːwan </ts>
               <ts e="T21" id="Seg_153" n="e" s="T20">jes. </ts>
               <ts e="T22" id="Seg_155" n="e" s="T21">Täp </ts>
               <ts e="T23" id="Seg_157" n="e" s="T22">qajno </ts>
               <ts e="T24" id="Seg_159" n="e" s="T23">tɨtdɨn </ts>
               <ts e="T25" id="Seg_161" n="e" s="T24">soːgudʼzʼelʼe </ts>
               <ts e="T26" id="Seg_163" n="e" s="T25">jübɨrɨn. </ts>
               <ts e="T27" id="Seg_165" n="e" s="T26">Tülʼe </ts>
               <ts e="T28" id="Seg_167" n="e" s="T27">bɨ </ts>
               <ts e="T29" id="Seg_169" n="e" s="T28">sogudʼzʼenän. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_170" s="T1">PVD_1964_Tatars_nar.001 (001.001)</ta>
            <ta e="T11" id="Seg_171" s="T3">PVD_1964_Tatars_nar.002 (001.002)</ta>
            <ta e="T16" id="Seg_172" s="T11">PVD_1964_Tatars_nar.003 (001.003)</ta>
            <ta e="T18" id="Seg_173" s="T16">PVD_1964_Tatars_nar.004 (001.004)</ta>
            <ta e="T21" id="Seg_174" s="T18">PVD_1964_Tatars_nar.005 (001.005)</ta>
            <ta e="T26" id="Seg_175" s="T21">PVD_1964_Tatars_nar.006 (001.006)</ta>
            <ta e="T29" id="Seg_176" s="T26">PVD_1964_Tatars_nar.007 (001.007)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_177" s="T1">тебыс′таɣъ kwа′ннаɣъ.</ta>
            <ta e="T11" id="Seg_178" s="T3">а ′Ванjа ′ма̄дам нʼӱут, ′мекга тʼа′рын: kай ты?</ta>
            <ta e="T16" id="Seg_179" s="T11">а ман на ′kа̄ролʼдʼӓн: та′тарин.</ta>
            <ta e="T18" id="Seg_180" s="T16">ты ӱ′се(ӓ)дит.</ta>
            <ta e="T21" id="Seg_181" s="T18">′мекг(х)а ′а̄wан jес.</ta>
            <ta e="T26" id="Seg_182" s="T21">тӓп kай′но тыт′дын ′со̄гу′дʼзʼелʼе ′jӱбырын.</ta>
            <ta e="T29" id="Seg_183" s="T26">′тӱлʼе бы согудʼзʼе′нӓн.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_184" s="T1">tebɨstaɣə qwannaɣə.</ta>
            <ta e="T11" id="Seg_185" s="T3">a Вanja maːdam nʼüut, mekga tʼarɨn: qaj tɨ?</ta>
            <ta e="T16" id="Seg_186" s="T11">a man na qaːrolʼdʼän: tatarin.</ta>
            <ta e="T18" id="Seg_187" s="T16">tɨ üse(ä)dit.</ta>
            <ta e="T21" id="Seg_188" s="T18">mekg(x)a aːwan jes.</ta>
            <ta e="T26" id="Seg_189" s="T21">täp qajno tɨtdɨn soːgudʼzʼelʼe jübɨrɨn.</ta>
            <ta e="T29" id="Seg_190" s="T26">tülʼe bɨ sogudʼzʼenän.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_191" s="T1">Tebɨstaɣə qwannaɣə. </ta>
            <ta e="T11" id="Seg_192" s="T3">A Vanja maːdam nʼüut, mekga tʼarɨn: “Qaj tɨ?” </ta>
            <ta e="T16" id="Seg_193" s="T11">A man na qaːrolʼdʼän: “Tatarin.” </ta>
            <ta e="T18" id="Seg_194" s="T16">Tɨ üsedit. </ta>
            <ta e="T21" id="Seg_195" s="T18">Mekga aːwan jes. </ta>
            <ta e="T26" id="Seg_196" s="T21">Täp qajno tɨtdɨn soːgudʼzʼelʼe jübɨrɨn. </ta>
            <ta e="T29" id="Seg_197" s="T26">Tülʼe bɨ sogudʼzʼenän. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_198" s="T1">teb-ɨ-staɣə</ta>
            <ta e="T3" id="Seg_199" s="T2">qwan-na-ɣə</ta>
            <ta e="T4" id="Seg_200" s="T3">a</ta>
            <ta e="T5" id="Seg_201" s="T4">Vanja</ta>
            <ta e="T6" id="Seg_202" s="T5">maːda-m</ta>
            <ta e="T7" id="Seg_203" s="T6">nʼüu-t</ta>
            <ta e="T8" id="Seg_204" s="T7">mekga</ta>
            <ta e="T9" id="Seg_205" s="T8">tʼarɨ-n</ta>
            <ta e="T10" id="Seg_206" s="T9">Qaj</ta>
            <ta e="T11" id="Seg_207" s="T10">tɨ</ta>
            <ta e="T12" id="Seg_208" s="T11">a</ta>
            <ta e="T13" id="Seg_209" s="T12">man</ta>
            <ta e="T14" id="Seg_210" s="T13">na</ta>
            <ta e="T15" id="Seg_211" s="T14">qaːr-olʼ-dʼä-n</ta>
            <ta e="T17" id="Seg_212" s="T16">tɨ</ta>
            <ta e="T18" id="Seg_213" s="T17">üse-di-t</ta>
            <ta e="T19" id="Seg_214" s="T18">mekga</ta>
            <ta e="T20" id="Seg_215" s="T19">aːwa-n</ta>
            <ta e="T21" id="Seg_216" s="T20">je-s</ta>
            <ta e="T22" id="Seg_217" s="T21">täp</ta>
            <ta e="T23" id="Seg_218" s="T22">qaj-no</ta>
            <ta e="T24" id="Seg_219" s="T23">tɨtdɨ-n</ta>
            <ta e="T25" id="Seg_220" s="T24">soːgudʼzʼe-lʼe</ta>
            <ta e="T26" id="Seg_221" s="T25">jübɨ-r-ɨ-n</ta>
            <ta e="T27" id="Seg_222" s="T26">tü-lʼe</ta>
            <ta e="T28" id="Seg_223" s="T27">bɨ</ta>
            <ta e="T29" id="Seg_224" s="T28">sogudʼzʼe-nä-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_225" s="T1">täp-ɨ-staɣɨ</ta>
            <ta e="T3" id="Seg_226" s="T2">qwan-nɨ-qij</ta>
            <ta e="T4" id="Seg_227" s="T3">a</ta>
            <ta e="T5" id="Seg_228" s="T4">Vanʼä</ta>
            <ta e="T6" id="Seg_229" s="T5">maːda-m</ta>
            <ta e="T7" id="Seg_230" s="T6">nʼuː-t</ta>
            <ta e="T8" id="Seg_231" s="T7">mekka</ta>
            <ta e="T9" id="Seg_232" s="T8">tʼärɨ-n</ta>
            <ta e="T10" id="Seg_233" s="T9">qaj</ta>
            <ta e="T11" id="Seg_234" s="T10">tɨ</ta>
            <ta e="T12" id="Seg_235" s="T11">a</ta>
            <ta e="T13" id="Seg_236" s="T12">man</ta>
            <ta e="T14" id="Seg_237" s="T13">na</ta>
            <ta e="T15" id="Seg_238" s="T14">qarɨ-ol-dʼi-ŋ</ta>
            <ta e="T17" id="Seg_239" s="T16">tɨ</ta>
            <ta e="T18" id="Seg_240" s="T17">üsʼe-dʼi-t</ta>
            <ta e="T19" id="Seg_241" s="T18">mekka</ta>
            <ta e="T20" id="Seg_242" s="T19">awa-ŋ</ta>
            <ta e="T21" id="Seg_243" s="T20">eː-sɨ</ta>
            <ta e="T22" id="Seg_244" s="T21">täp</ta>
            <ta e="T23" id="Seg_245" s="T22">qaj-no</ta>
            <ta e="T24" id="Seg_246" s="T23">tɨtʼa-n</ta>
            <ta e="T25" id="Seg_247" s="T24">sogundʼe-le</ta>
            <ta e="T26" id="Seg_248" s="T25">übɨ-r-ɨ-n</ta>
            <ta e="T27" id="Seg_249" s="T26">tüː-le</ta>
            <ta e="T28" id="Seg_250" s="T27">bɨ</ta>
            <ta e="T29" id="Seg_251" s="T28">sogundʼe-ne-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_252" s="T1">(s)he-EP-DU.[NOM]</ta>
            <ta e="T3" id="Seg_253" s="T2">leave-CO-3DU.S</ta>
            <ta e="T4" id="Seg_254" s="T3">and</ta>
            <ta e="T5" id="Seg_255" s="T4">Vanya.[NOM]</ta>
            <ta e="T6" id="Seg_256" s="T5">door-ACC</ta>
            <ta e="T7" id="Seg_257" s="T6">open-3SG.O</ta>
            <ta e="T8" id="Seg_258" s="T7">I.ALL</ta>
            <ta e="T9" id="Seg_259" s="T8">say-3SG.S</ta>
            <ta e="T10" id="Seg_260" s="T9">either</ta>
            <ta e="T11" id="Seg_261" s="T10">Tatar.[NOM]</ta>
            <ta e="T12" id="Seg_262" s="T11">and</ta>
            <ta e="T13" id="Seg_263" s="T12">I.NOM</ta>
            <ta e="T14" id="Seg_264" s="T13">here</ta>
            <ta e="T15" id="Seg_265" s="T14">shout-MOM-DRV-1SG.S</ta>
            <ta e="T17" id="Seg_266" s="T16">Tatar.[NOM]</ta>
            <ta e="T18" id="Seg_267" s="T17">hear-DRV-3SG.O</ta>
            <ta e="T19" id="Seg_268" s="T18">I.ALL</ta>
            <ta e="T20" id="Seg_269" s="T19">bad-ADVZ</ta>
            <ta e="T21" id="Seg_270" s="T20">be-PST.[3SG.S]</ta>
            <ta e="T22" id="Seg_271" s="T21">(s)he.[NOM]</ta>
            <ta e="T23" id="Seg_272" s="T22">what-TRL</ta>
            <ta e="T24" id="Seg_273" s="T23">here-ADV.LOC</ta>
            <ta e="T25" id="Seg_274" s="T24">ask-CVB</ta>
            <ta e="T26" id="Seg_275" s="T25">begin-DRV-EP-3SG.S</ta>
            <ta e="T27" id="Seg_276" s="T26">come-CVB</ta>
            <ta e="T28" id="Seg_277" s="T27">IRREAL</ta>
            <ta e="T29" id="Seg_278" s="T28">ask-CONJ-3SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_279" s="T1">он(а)-EP-DU.[NOM]</ta>
            <ta e="T3" id="Seg_280" s="T2">отправиться-CO-3DU.S</ta>
            <ta e="T4" id="Seg_281" s="T3">а</ta>
            <ta e="T5" id="Seg_282" s="T4">Ваня.[NOM]</ta>
            <ta e="T6" id="Seg_283" s="T5">дверь-ACC</ta>
            <ta e="T7" id="Seg_284" s="T6">открыть-3SG.O</ta>
            <ta e="T8" id="Seg_285" s="T7">я.ALL</ta>
            <ta e="T9" id="Seg_286" s="T8">сказать-3SG.S</ta>
            <ta e="T10" id="Seg_287" s="T9">ли</ta>
            <ta e="T11" id="Seg_288" s="T10">татарин.[NOM]</ta>
            <ta e="T12" id="Seg_289" s="T11">а</ta>
            <ta e="T13" id="Seg_290" s="T12">я.NOM</ta>
            <ta e="T14" id="Seg_291" s="T13">ну</ta>
            <ta e="T15" id="Seg_292" s="T14">кричать-MOM-DRV-1SG.S</ta>
            <ta e="T17" id="Seg_293" s="T16">татарин.[NOM]</ta>
            <ta e="T18" id="Seg_294" s="T17">услышать-DRV-3SG.O</ta>
            <ta e="T19" id="Seg_295" s="T18">я.ALL</ta>
            <ta e="T20" id="Seg_296" s="T19">плохой-ADVZ</ta>
            <ta e="T21" id="Seg_297" s="T20">быть-PST.[3SG.S]</ta>
            <ta e="T22" id="Seg_298" s="T21">он(а).[NOM]</ta>
            <ta e="T23" id="Seg_299" s="T22">что-TRL</ta>
            <ta e="T24" id="Seg_300" s="T23">сюда-ADV.LOC</ta>
            <ta e="T25" id="Seg_301" s="T24">спросить-CVB</ta>
            <ta e="T26" id="Seg_302" s="T25">начать-DRV-EP-3SG.S</ta>
            <ta e="T27" id="Seg_303" s="T26">прийти-CVB</ta>
            <ta e="T28" id="Seg_304" s="T27">IRREAL</ta>
            <ta e="T29" id="Seg_305" s="T28">спросить-CONJ-3SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_306" s="T1">pers-n:ins-n:num.[n:case]</ta>
            <ta e="T3" id="Seg_307" s="T2">v-v:ins-v:pn</ta>
            <ta e="T4" id="Seg_308" s="T3">conj</ta>
            <ta e="T5" id="Seg_309" s="T4">nprop.[n:case]</ta>
            <ta e="T6" id="Seg_310" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_311" s="T6">v-v:pn</ta>
            <ta e="T8" id="Seg_312" s="T7">pers</ta>
            <ta e="T9" id="Seg_313" s="T8">v-v:pn</ta>
            <ta e="T10" id="Seg_314" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_315" s="T10">n.[n:case]</ta>
            <ta e="T12" id="Seg_316" s="T11">conj</ta>
            <ta e="T13" id="Seg_317" s="T12">pers</ta>
            <ta e="T14" id="Seg_318" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_319" s="T14">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T17" id="Seg_320" s="T16">n.[n:case]</ta>
            <ta e="T18" id="Seg_321" s="T17">v-v&gt;v-v:pn</ta>
            <ta e="T19" id="Seg_322" s="T18">pers</ta>
            <ta e="T20" id="Seg_323" s="T19">adj-adj&gt;adv</ta>
            <ta e="T21" id="Seg_324" s="T20">v-v:tense.[v:pn]</ta>
            <ta e="T22" id="Seg_325" s="T21">pers.[n:case]</ta>
            <ta e="T23" id="Seg_326" s="T22">interrog-n:case</ta>
            <ta e="T24" id="Seg_327" s="T23">adv-adv:case</ta>
            <ta e="T25" id="Seg_328" s="T24">v-v&gt;adv</ta>
            <ta e="T26" id="Seg_329" s="T25">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T27" id="Seg_330" s="T26">v-v&gt;adv</ta>
            <ta e="T28" id="Seg_331" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_332" s="T28">v-v:mood-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_333" s="T1">pers</ta>
            <ta e="T3" id="Seg_334" s="T2">v</ta>
            <ta e="T4" id="Seg_335" s="T3">conj</ta>
            <ta e="T5" id="Seg_336" s="T4">nprop</ta>
            <ta e="T6" id="Seg_337" s="T5">n</ta>
            <ta e="T7" id="Seg_338" s="T6">v</ta>
            <ta e="T8" id="Seg_339" s="T7">pers</ta>
            <ta e="T9" id="Seg_340" s="T8">v</ta>
            <ta e="T10" id="Seg_341" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_342" s="T10">n</ta>
            <ta e="T12" id="Seg_343" s="T11">conj</ta>
            <ta e="T13" id="Seg_344" s="T12">pers</ta>
            <ta e="T14" id="Seg_345" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_346" s="T14">v</ta>
            <ta e="T17" id="Seg_347" s="T16">n</ta>
            <ta e="T18" id="Seg_348" s="T17">v</ta>
            <ta e="T19" id="Seg_349" s="T18">pers</ta>
            <ta e="T20" id="Seg_350" s="T19">adj</ta>
            <ta e="T21" id="Seg_351" s="T20">v</ta>
            <ta e="T22" id="Seg_352" s="T21">pers</ta>
            <ta e="T23" id="Seg_353" s="T22">interrog</ta>
            <ta e="T24" id="Seg_354" s="T23">adv</ta>
            <ta e="T25" id="Seg_355" s="T24">adv</ta>
            <ta e="T26" id="Seg_356" s="T25">v</ta>
            <ta e="T27" id="Seg_357" s="T26">adv</ta>
            <ta e="T28" id="Seg_358" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_359" s="T28">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_360" s="T1">pro.h:A</ta>
            <ta e="T5" id="Seg_361" s="T4">np.h:A</ta>
            <ta e="T6" id="Seg_362" s="T5">np:Th</ta>
            <ta e="T8" id="Seg_363" s="T7">pro.h:R</ta>
            <ta e="T9" id="Seg_364" s="T8">0.3.h:A</ta>
            <ta e="T11" id="Seg_365" s="T10">0.3.h:Th</ta>
            <ta e="T13" id="Seg_366" s="T12">pro.h:A</ta>
            <ta e="T17" id="Seg_367" s="T16">np.h:E</ta>
            <ta e="T19" id="Seg_368" s="T18">pro.h:E</ta>
            <ta e="T21" id="Seg_369" s="T20">0.3:Th</ta>
            <ta e="T22" id="Seg_370" s="T21">pro.h:A</ta>
            <ta e="T24" id="Seg_371" s="T23">adv:L</ta>
            <ta e="T29" id="Seg_372" s="T28">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_373" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_374" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_375" s="T4">np.h:S</ta>
            <ta e="T6" id="Seg_376" s="T5">np:O</ta>
            <ta e="T7" id="Seg_377" s="T6">v:pred</ta>
            <ta e="T9" id="Seg_378" s="T8">0.3.h:S v:pred</ta>
            <ta e="T11" id="Seg_379" s="T10">0.3.h:S n:pred</ta>
            <ta e="T13" id="Seg_380" s="T12">pro.h:S</ta>
            <ta e="T15" id="Seg_381" s="T14">v:pred</ta>
            <ta e="T17" id="Seg_382" s="T16">np.h:S</ta>
            <ta e="T18" id="Seg_383" s="T17">v:pred</ta>
            <ta e="T21" id="Seg_384" s="T20">0.3:S v:pred</ta>
            <ta e="T22" id="Seg_385" s="T21">pro.h:S</ta>
            <ta e="T26" id="Seg_386" s="T25">v:pred</ta>
            <ta e="T29" id="Seg_387" s="T28">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_388" s="T3">RUS:gram</ta>
            <ta e="T12" id="Seg_389" s="T11">RUS:gram</ta>
            <ta e="T28" id="Seg_390" s="T27">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_391" s="T1">The two of them came.</ta>
            <ta e="T11" id="Seg_392" s="T3">And Vanya opened a door and said to me: “Is he a Tatar?”</ta>
            <ta e="T16" id="Seg_393" s="T11">And I cried: “Tatar.” (in Russian)</ta>
            <ta e="T18" id="Seg_394" s="T16">The Tatar heard it.</ta>
            <ta e="T21" id="Seg_395" s="T18">I was confused.</ta>
            <ta e="T26" id="Seg_396" s="T21">Why did he start asking me here?</ta>
            <ta e="T29" id="Seg_397" s="T26">He could come and ask.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_398" s="T1">Die beiden kamen.</ta>
            <ta e="T11" id="Seg_399" s="T3">Und Vanya öffnete die Tür und sagte zu mir: "Ist er ein Tatar?"</ta>
            <ta e="T16" id="Seg_400" s="T11">Und ich rief: "Tatar." (auf Russisch)</ta>
            <ta e="T18" id="Seg_401" s="T16">Der Tatar hörte es.</ta>
            <ta e="T21" id="Seg_402" s="T18">Ich war verwirrt.</ta>
            <ta e="T26" id="Seg_403" s="T21">Warum fing er an mich hier zu fragen?</ta>
            <ta e="T29" id="Seg_404" s="T26">Er könnte kommen und fragen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_405" s="T1">Они вдвоем пошли.</ta>
            <ta e="T11" id="Seg_406" s="T3">А Ваня дверь открыл, мне говорит: “Татарин что ли?”</ta>
            <ta e="T16" id="Seg_407" s="T11">А я закричала: “Татарин”.</ta>
            <ta e="T18" id="Seg_408" s="T16">Татарин услыхал.</ta>
            <ta e="T21" id="Seg_409" s="T18">Мне было неудобно.</ta>
            <ta e="T26" id="Seg_410" s="T21">Он почему здесь начал спрашивать.</ta>
            <ta e="T29" id="Seg_411" s="T26">Пришел бы и спросил.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_412" s="T1">они вдвоем пошли</ta>
            <ta e="T11" id="Seg_413" s="T3">Ваня домой дверь открыл мне говорит какой ты</ta>
            <ta e="T16" id="Seg_414" s="T11">а я закричала татарин</ta>
            <ta e="T18" id="Seg_415" s="T16">татарин услыхал</ta>
            <ta e="T21" id="Seg_416" s="T18">мне было неудобно</ta>
            <ta e="T26" id="Seg_417" s="T21">он почему здесь начал спрашивать</ta>
            <ta e="T29" id="Seg_418" s="T26">пришел бы и спросил</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T18" id="Seg_419" s="T16">[KuAI:] Variant: 'üsädit'.</ta>
            <ta e="T21" id="Seg_420" s="T18">[KuAI:] Variant: 'mekxa'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T11" id="Seg_421" s="T3">ты, тыла - татарин</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
