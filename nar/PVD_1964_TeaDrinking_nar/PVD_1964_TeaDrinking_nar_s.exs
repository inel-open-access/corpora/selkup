<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_TeaDrinking_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_TeaDrinking_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">63</ud-information>
            <ud-information attribute-name="# HIAT:w">46</ud-information>
            <ud-information attribute-name="# e">46</ud-information>
            <ud-information attribute-name="# HIAT:u">16</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T47" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Čaj</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">pöčeǯan</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">kakawaze</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T5" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Nʼüːjeǯɨn</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_20" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">Man</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">nɨkolǯɨlʼe</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">taːdərau</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">soːlansʼe</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_35" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">Saxarɨm</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">nɨkolǯau</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_44" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">Sičas</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">kamneǯau</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">čajotdə</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_56" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_58" n="HIAT:w" s="T14">Tep</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">perčoɣɨlǯəǯɨn</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_65" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_67" n="HIAT:w" s="T16">Čʼaj</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_70" n="HIAT:w" s="T17">pödədʼin</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_74" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_76" n="HIAT:w" s="T18">Čʼaj</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_79" n="HIAT:w" s="T19">qamǯugu</ts>
                  <nts id="Seg_80" n="HIAT:ip">?</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_83" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_85" n="HIAT:w" s="T20">Qamǯet</ts>
                  <nts id="Seg_86" n="HIAT:ip">.</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_89" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_91" n="HIAT:w" s="T21">Tütdä</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_94" n="HIAT:w" s="T22">bɨ</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_97" n="HIAT:w" s="T23">daiča</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_100" n="HIAT:w" s="T24">tʼarnɨt</ts>
                  <nts id="Seg_101" n="HIAT:ip">.</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_104" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_106" n="HIAT:w" s="T25">Man</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_109" n="HIAT:w" s="T26">kabɨʒan</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_112" n="HIAT:w" s="T27">selʼödkazʼe</ts>
                  <nts id="Seg_113" n="HIAT:ip">.</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_116" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_118" n="HIAT:w" s="T28">Qwadčizʼe</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_121" n="HIAT:w" s="T29">näiɣum</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_124" n="HIAT:w" s="T30">tüǯɨn</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_127" n="HIAT:w" s="T31">al</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_130" n="HIAT:w" s="T32">as</ts>
                  <nts id="Seg_131" n="HIAT:ip">?</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_134" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_136" n="HIAT:w" s="T33">Tüǯɨn</ts>
                  <nts id="Seg_137" n="HIAT:ip">.</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_140" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_142" n="HIAT:w" s="T34">Čaj</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_145" n="HIAT:w" s="T35">qatdʼediba</ts>
                  <nts id="Seg_146" n="HIAT:ip">.</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_149" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_151" n="HIAT:w" s="T36">A</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_154" n="HIAT:w" s="T37">man</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_157" n="HIAT:w" s="T38">perčilʼe</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_160" n="HIAT:w" s="T39">üsau</ts>
                  <nts id="Seg_161" n="HIAT:ip">.</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_164" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_166" n="HIAT:w" s="T40">Jeʒəlʼi</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_169" n="HIAT:w" s="T41">naǯə</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_172" n="HIAT:w" s="T42">pasedʼin</ts>
                  <nts id="Seg_173" n="HIAT:ip">,</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_176" n="HIAT:w" s="T43">qaborɣɨn</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_179" n="HIAT:w" s="T44">puʒoɣɨn</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_182" n="HIAT:w" s="T45">az</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_185" n="HIAT:w" s="T46">adun</ts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T47" id="Seg_188" n="sc" s="T1">
               <ts e="T2" id="Seg_190" n="e" s="T1">Čaj </ts>
               <ts e="T3" id="Seg_192" n="e" s="T2">pöčeǯan </ts>
               <ts e="T4" id="Seg_194" n="e" s="T3">kakawaze. </ts>
               <ts e="T5" id="Seg_196" n="e" s="T4">Nʼüːjeǯɨn. </ts>
               <ts e="T6" id="Seg_198" n="e" s="T5">Man </ts>
               <ts e="T7" id="Seg_200" n="e" s="T6">nɨkolǯɨlʼe </ts>
               <ts e="T8" id="Seg_202" n="e" s="T7">taːdərau </ts>
               <ts e="T9" id="Seg_204" n="e" s="T8">soːlansʼe. </ts>
               <ts e="T10" id="Seg_206" n="e" s="T9">Saxarɨm </ts>
               <ts e="T11" id="Seg_208" n="e" s="T10">nɨkolǯau. </ts>
               <ts e="T12" id="Seg_210" n="e" s="T11">Sičas </ts>
               <ts e="T13" id="Seg_212" n="e" s="T12">kamneǯau </ts>
               <ts e="T14" id="Seg_214" n="e" s="T13">čajotdə. </ts>
               <ts e="T15" id="Seg_216" n="e" s="T14">Tep </ts>
               <ts e="T16" id="Seg_218" n="e" s="T15">perčoɣɨlǯəǯɨn. </ts>
               <ts e="T17" id="Seg_220" n="e" s="T16">Čʼaj </ts>
               <ts e="T18" id="Seg_222" n="e" s="T17">pödədʼin. </ts>
               <ts e="T19" id="Seg_224" n="e" s="T18">Čʼaj </ts>
               <ts e="T20" id="Seg_226" n="e" s="T19">qamǯugu? </ts>
               <ts e="T21" id="Seg_228" n="e" s="T20">Qamǯet. </ts>
               <ts e="T22" id="Seg_230" n="e" s="T21">Tütdä </ts>
               <ts e="T23" id="Seg_232" n="e" s="T22">bɨ </ts>
               <ts e="T24" id="Seg_234" n="e" s="T23">daiča </ts>
               <ts e="T25" id="Seg_236" n="e" s="T24">tʼarnɨt. </ts>
               <ts e="T26" id="Seg_238" n="e" s="T25">Man </ts>
               <ts e="T27" id="Seg_240" n="e" s="T26">kabɨʒan </ts>
               <ts e="T28" id="Seg_242" n="e" s="T27">selʼödkazʼe. </ts>
               <ts e="T29" id="Seg_244" n="e" s="T28">Qwadčizʼe </ts>
               <ts e="T30" id="Seg_246" n="e" s="T29">näiɣum </ts>
               <ts e="T31" id="Seg_248" n="e" s="T30">tüǯɨn </ts>
               <ts e="T32" id="Seg_250" n="e" s="T31">al </ts>
               <ts e="T33" id="Seg_252" n="e" s="T32">as? </ts>
               <ts e="T34" id="Seg_254" n="e" s="T33">Tüǯɨn. </ts>
               <ts e="T35" id="Seg_256" n="e" s="T34">Čaj </ts>
               <ts e="T36" id="Seg_258" n="e" s="T35">qatdʼediba. </ts>
               <ts e="T37" id="Seg_260" n="e" s="T36">A </ts>
               <ts e="T38" id="Seg_262" n="e" s="T37">man </ts>
               <ts e="T39" id="Seg_264" n="e" s="T38">perčilʼe </ts>
               <ts e="T40" id="Seg_266" n="e" s="T39">üsau. </ts>
               <ts e="T41" id="Seg_268" n="e" s="T40">Jeʒəlʼi </ts>
               <ts e="T42" id="Seg_270" n="e" s="T41">naǯə </ts>
               <ts e="T43" id="Seg_272" n="e" s="T42">pasedʼin, </ts>
               <ts e="T44" id="Seg_274" n="e" s="T43">qaborɣɨn </ts>
               <ts e="T45" id="Seg_276" n="e" s="T44">puʒoɣɨn </ts>
               <ts e="T46" id="Seg_278" n="e" s="T45">az </ts>
               <ts e="T47" id="Seg_280" n="e" s="T46">adun. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_281" s="T1">PVD_1964_TeaDrinking_nar.001 (001.001)</ta>
            <ta e="T5" id="Seg_282" s="T4">PVD_1964_TeaDrinking_nar.002 (001.002)</ta>
            <ta e="T9" id="Seg_283" s="T5">PVD_1964_TeaDrinking_nar.003 (001.003)</ta>
            <ta e="T11" id="Seg_284" s="T9">PVD_1964_TeaDrinking_nar.004 (001.004)</ta>
            <ta e="T14" id="Seg_285" s="T11">PVD_1964_TeaDrinking_nar.005 (001.005)</ta>
            <ta e="T16" id="Seg_286" s="T14">PVD_1964_TeaDrinking_nar.006 (001.006)</ta>
            <ta e="T18" id="Seg_287" s="T16">PVD_1964_TeaDrinking_nar.007 (001.007)</ta>
            <ta e="T20" id="Seg_288" s="T18">PVD_1964_TeaDrinking_nar.008 (001.008)</ta>
            <ta e="T21" id="Seg_289" s="T20">PVD_1964_TeaDrinking_nar.009 (001.009)</ta>
            <ta e="T25" id="Seg_290" s="T21">PVD_1964_TeaDrinking_nar.010 (001.010)</ta>
            <ta e="T28" id="Seg_291" s="T25">PVD_1964_TeaDrinking_nar.011 (001.011)</ta>
            <ta e="T33" id="Seg_292" s="T28">PVD_1964_TeaDrinking_nar.012 (001.012)</ta>
            <ta e="T34" id="Seg_293" s="T33">PVD_1964_TeaDrinking_nar.013 (001.013)</ta>
            <ta e="T36" id="Seg_294" s="T34">PVD_1964_TeaDrinking_nar.014 (001.014)</ta>
            <ta e="T40" id="Seg_295" s="T36">PVD_1964_TeaDrinking_nar.015 (001.015)</ta>
            <ta e="T47" id="Seg_296" s="T40">PVD_1964_TeaDrinking_nar.016 (001.016)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_297" s="T1">′тшай ′пӧтшеджан ка′каwазе.</ta>
            <ta e="T5" id="Seg_298" s="T4">нʼӱ̄ ′jеджын.</ta>
            <ta e="T9" id="Seg_299" s="T5">ман ны′коlджылʼе ′та̄дърау со̄′лансʼе.</ta>
            <ta e="T11" id="Seg_300" s="T9">′сахарым ныкоl′джау.</ta>
            <ta e="T14" id="Seg_301" s="T11">си′час кам′неджау ′тшайотдъ.</ta>
            <ta e="T16" id="Seg_302" s="T14">теп пер′тшоɣыlджъджын.</ta>
            <ta e="T18" id="Seg_303" s="T16">чʼай ′пӧдъдʼин.</ta>
            <ta e="T20" id="Seg_304" s="T18">чʼай kамджугу?</ta>
            <ta e="T21" id="Seg_305" s="T20">kам′джет.</ta>
            <ta e="T25" id="Seg_306" s="T21">тӱтдӓ бы ′даича тʼар′ныт.</ta>
            <ta e="T28" id="Seg_307" s="T25">ман кабыжан се′лʼӧдказʼе.</ta>
            <ta e="T33" id="Seg_308" s="T28">′kwад̂тшизʼе нӓиɣум ′тӱджын аl ас?</ta>
            <ta e="T34" id="Seg_309" s="T33">′тӱджын.</ta>
            <ta e="T36" id="Seg_310" s="T34">чай kат′д̂ʼед̂иб̂а.</ta>
            <ta e="T40" id="Seg_311" s="T36">а ман пертши′лʼе ӱ′сау.</ta>
            <ta e="T47" id="Seg_312" s="T40">jежълʼи ′наджъ па′седʼин, kа′борɣын пу′жоɣын аз а′дун.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_313" s="T1">tšaj pötšeǯan kakawaze.</ta>
            <ta e="T5" id="Seg_314" s="T4">nʼüː jeǯɨn.</ta>
            <ta e="T9" id="Seg_315" s="T5">man nɨkolǯɨlʼe taːdərau soːlansʼe.</ta>
            <ta e="T11" id="Seg_316" s="T9">saxarɨm nɨkolǯau.</ta>
            <ta e="T14" id="Seg_317" s="T11">sičas kamneǯau tšajotdə.</ta>
            <ta e="T16" id="Seg_318" s="T14">tep pertšoɣɨlǯəǯɨn.</ta>
            <ta e="T18" id="Seg_319" s="T16">čʼaj pödədʼin.</ta>
            <ta e="T20" id="Seg_320" s="T18">čʼaj qamǯugu?</ta>
            <ta e="T21" id="Seg_321" s="T20">qamǯet.</ta>
            <ta e="T25" id="Seg_322" s="T21">tütdä bɨ daiča tʼarnɨt.</ta>
            <ta e="T28" id="Seg_323" s="T25">man kabɨʒan selʼödkazʼe.</ta>
            <ta e="T33" id="Seg_324" s="T28">qwad̂tšizʼe näiɣum tüǯɨn al as?</ta>
            <ta e="T34" id="Seg_325" s="T33">tüǯɨn.</ta>
            <ta e="T36" id="Seg_326" s="T34">čaj qatd̂ʼed̂ib̂a.</ta>
            <ta e="T40" id="Seg_327" s="T36">a man pertšilʼe üsau.</ta>
            <ta e="T47" id="Seg_328" s="T40">jeʒəlʼi naǯə pasedʼin, qaborɣɨn puʒoɣɨn az adun.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_329" s="T1">Čaj pöčeǯan kakawaze. </ta>
            <ta e="T5" id="Seg_330" s="T4">Nʼüːjeǯɨn. </ta>
            <ta e="T9" id="Seg_331" s="T5">Man nɨkolǯɨlʼe taːdərau soːlansʼe. </ta>
            <ta e="T11" id="Seg_332" s="T9">Saxarɨm nɨkolǯau. </ta>
            <ta e="T14" id="Seg_333" s="T11">Sičas kamneǯau čajotdə. </ta>
            <ta e="T16" id="Seg_334" s="T14">Tep perčoɣɨlǯəǯɨn. </ta>
            <ta e="T18" id="Seg_335" s="T16">Čʼaj pödədʼin. </ta>
            <ta e="T20" id="Seg_336" s="T18">Čʼaj qamǯugu? </ta>
            <ta e="T21" id="Seg_337" s="T20">Qamǯet. </ta>
            <ta e="T25" id="Seg_338" s="T21">Tütdä bɨ daiča tʼarnɨt. </ta>
            <ta e="T28" id="Seg_339" s="T25">Man kabɨʒan selʼödkazʼe. </ta>
            <ta e="T33" id="Seg_340" s="T28">Qwadčizʼe näiɣum tüǯɨn al as? </ta>
            <ta e="T34" id="Seg_341" s="T33">Tüǯɨn. </ta>
            <ta e="T36" id="Seg_342" s="T34">Čaj qatdʼediba. </ta>
            <ta e="T40" id="Seg_343" s="T36">A man perčilʼe üsau. </ta>
            <ta e="T47" id="Seg_344" s="T40">Jeʒəlʼi naǯə pasedʼin, qaborɣɨn puʒoɣɨn az adun. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_345" s="T1">čaj</ta>
            <ta e="T3" id="Seg_346" s="T2">pöč-eǯa-n</ta>
            <ta e="T4" id="Seg_347" s="T3">kakawa-ze</ta>
            <ta e="T5" id="Seg_348" s="T4">nʼüːj-eǯɨ-n</ta>
            <ta e="T6" id="Seg_349" s="T5">man</ta>
            <ta e="T7" id="Seg_350" s="T6">nɨkolǯɨ-lʼe</ta>
            <ta e="T8" id="Seg_351" s="T7">taːd-ə-r-a-u</ta>
            <ta e="T9" id="Seg_352" s="T8">soːlan-sʼe</ta>
            <ta e="T10" id="Seg_353" s="T9">saxar-ɨ-m</ta>
            <ta e="T11" id="Seg_354" s="T10">nɨkolǯa-u</ta>
            <ta e="T12" id="Seg_355" s="T11">sičas</ta>
            <ta e="T13" id="Seg_356" s="T12">kamn-eǯa-u</ta>
            <ta e="T14" id="Seg_357" s="T13">čaj-o-tdə</ta>
            <ta e="T15" id="Seg_358" s="T14">tep</ta>
            <ta e="T16" id="Seg_359" s="T15">perčo-ɣɨ-lǯ-əǯɨ-n</ta>
            <ta e="T17" id="Seg_360" s="T16">čʼaj</ta>
            <ta e="T18" id="Seg_361" s="T17">pödədʼi-n</ta>
            <ta e="T19" id="Seg_362" s="T18">čʼaj</ta>
            <ta e="T20" id="Seg_363" s="T19">qamǯu-gu</ta>
            <ta e="T21" id="Seg_364" s="T20">qamǯ-et</ta>
            <ta e="T22" id="Seg_365" s="T21">tütdä</ta>
            <ta e="T23" id="Seg_366" s="T22">bɨ</ta>
            <ta e="T24" id="Seg_367" s="T23">daiča</ta>
            <ta e="T25" id="Seg_368" s="T24">tʼar-nɨ-t</ta>
            <ta e="T26" id="Seg_369" s="T25">man</ta>
            <ta e="T27" id="Seg_370" s="T26">kabɨʒa-n</ta>
            <ta e="T28" id="Seg_371" s="T27">selʼödka-zʼe</ta>
            <ta e="T29" id="Seg_372" s="T28">qwadči-zʼe</ta>
            <ta e="T30" id="Seg_373" s="T29">nä-i-ɣum</ta>
            <ta e="T31" id="Seg_374" s="T30">tü-ǯɨ-n</ta>
            <ta e="T32" id="Seg_375" s="T31">al</ta>
            <ta e="T33" id="Seg_376" s="T32">as</ta>
            <ta e="T34" id="Seg_377" s="T33">tü-ǯɨ-n</ta>
            <ta e="T35" id="Seg_378" s="T34">čaj</ta>
            <ta e="T36" id="Seg_379" s="T35">qatdʼe-di-ba</ta>
            <ta e="T37" id="Seg_380" s="T36">a</ta>
            <ta e="T38" id="Seg_381" s="T37">man</ta>
            <ta e="T39" id="Seg_382" s="T38">perči-lʼe</ta>
            <ta e="T40" id="Seg_383" s="T39">ü-sa-u</ta>
            <ta e="T41" id="Seg_384" s="T40">jeʒəlʼi</ta>
            <ta e="T42" id="Seg_385" s="T41">naǯə</ta>
            <ta e="T43" id="Seg_386" s="T42">pasedʼi-n</ta>
            <ta e="T44" id="Seg_387" s="T43">qaborɣ-ɨ-n</ta>
            <ta e="T45" id="Seg_388" s="T44">puʒo-ɣɨn</ta>
            <ta e="T46" id="Seg_389" s="T45">az</ta>
            <ta e="T47" id="Seg_390" s="T46">adu-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_391" s="T1">čʼai</ta>
            <ta e="T3" id="Seg_392" s="T2">pöču-enǯɨ-ŋ</ta>
            <ta e="T4" id="Seg_393" s="T3">kakawa-se</ta>
            <ta e="T5" id="Seg_394" s="T4">nʼuːi-enǯɨ-n</ta>
            <ta e="T6" id="Seg_395" s="T5">man</ta>
            <ta e="T7" id="Seg_396" s="T6">nɨkolǯɨ-le</ta>
            <ta e="T8" id="Seg_397" s="T7">tat-ɨ-r-nɨ-w</ta>
            <ta e="T9" id="Seg_398" s="T8">solaŋ-se</ta>
            <ta e="T10" id="Seg_399" s="T9">saxar-ɨ-m</ta>
            <ta e="T11" id="Seg_400" s="T10">nɨkolǯɨ-w</ta>
            <ta e="T12" id="Seg_401" s="T11">sičas</ta>
            <ta e="T13" id="Seg_402" s="T12">qamdə-enǯɨ-w</ta>
            <ta e="T14" id="Seg_403" s="T13">čʼai-ɨ-ntə</ta>
            <ta e="T15" id="Seg_404" s="T14">täp</ta>
            <ta e="T16" id="Seg_405" s="T15">pärču-kɨ-lǯi-enǯɨ-n</ta>
            <ta e="T17" id="Seg_406" s="T16">čʼai</ta>
            <ta e="T18" id="Seg_407" s="T17">pödədʼi-n</ta>
            <ta e="T19" id="Seg_408" s="T18">čʼai</ta>
            <ta e="T20" id="Seg_409" s="T19">qamǯu-gu</ta>
            <ta e="T21" id="Seg_410" s="T20">qamǯu-etɨ</ta>
            <ta e="T22" id="Seg_411" s="T21">tütdä</ta>
            <ta e="T23" id="Seg_412" s="T22">bɨ</ta>
            <ta e="T24" id="Seg_413" s="T23">daiča</ta>
            <ta e="T25" id="Seg_414" s="T24">tʼärɨ-nɨ-t</ta>
            <ta e="T26" id="Seg_415" s="T25">man</ta>
            <ta e="T27" id="Seg_416" s="T26">qabuʒu-ŋ</ta>
            <ta e="T28" id="Seg_417" s="T27">selʼödka-se</ta>
            <ta e="T29" id="Seg_418" s="T28">qwačə-se</ta>
            <ta e="T30" id="Seg_419" s="T29">ne-lʼ-qum</ta>
            <ta e="T31" id="Seg_420" s="T30">tüː-enǯɨ-n</ta>
            <ta e="T32" id="Seg_421" s="T31">alʼi</ta>
            <ta e="T33" id="Seg_422" s="T32">asa</ta>
            <ta e="T34" id="Seg_423" s="T33">tüː-enǯɨ-n</ta>
            <ta e="T35" id="Seg_424" s="T34">čʼai</ta>
            <ta e="T36" id="Seg_425" s="T35">qandɨ-dʼi-mbɨ</ta>
            <ta e="T37" id="Seg_426" s="T36">a</ta>
            <ta e="T38" id="Seg_427" s="T37">man</ta>
            <ta e="T39" id="Seg_428" s="T38">pärču-le</ta>
            <ta e="T40" id="Seg_429" s="T39">üt-sɨ-w</ta>
            <ta e="T41" id="Seg_430" s="T40">jesʼli</ta>
            <ta e="T42" id="Seg_431" s="T41">naːǯə</ta>
            <ta e="T43" id="Seg_432" s="T42">pasedʼi-n</ta>
            <ta e="T44" id="Seg_433" s="T43">qaborɣ-ɨ-n</ta>
            <ta e="T45" id="Seg_434" s="T44">puːǯə-qɨn</ta>
            <ta e="T46" id="Seg_435" s="T45">asa</ta>
            <ta e="T47" id="Seg_436" s="T46">aːdu-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_437" s="T1">tea.[NOM]</ta>
            <ta e="T3" id="Seg_438" s="T2">heat.up-FUT-1SG.S</ta>
            <ta e="T4" id="Seg_439" s="T3">cocoa-COM</ta>
            <ta e="T5" id="Seg_440" s="T4">be.tasty-FUT-3SG.S</ta>
            <ta e="T6" id="Seg_441" s="T5">I.NOM</ta>
            <ta e="T7" id="Seg_442" s="T6">stir-CVB</ta>
            <ta e="T8" id="Seg_443" s="T7">bring-EP-FRQ-CO-1SG.O</ta>
            <ta e="T9" id="Seg_444" s="T8">spoon-INSTR</ta>
            <ta e="T10" id="Seg_445" s="T9">sugar-EP-ACC</ta>
            <ta e="T11" id="Seg_446" s="T10">stir-1SG.O</ta>
            <ta e="T12" id="Seg_447" s="T11">now</ta>
            <ta e="T13" id="Seg_448" s="T12">turn.around-FUT-1SG.O</ta>
            <ta e="T14" id="Seg_449" s="T13">tea-EP-ILL</ta>
            <ta e="T15" id="Seg_450" s="T14">(s)he.[NOM]</ta>
            <ta e="T16" id="Seg_451" s="T15">boil-RFL-PFV-FUT-GEN</ta>
            <ta e="T17" id="Seg_452" s="T16">tea.[NOM]</ta>
            <ta e="T18" id="Seg_453" s="T17">get.warm-3SG.S</ta>
            <ta e="T19" id="Seg_454" s="T18">tea.[NOM]</ta>
            <ta e="T20" id="Seg_455" s="T19">pour-INF</ta>
            <ta e="T21" id="Seg_456" s="T20">pour-IMP.2SG.O</ta>
            <ta e="T22" id="Seg_457" s="T21">%earlier</ta>
            <ta e="T23" id="Seg_458" s="T22">IRREAL</ta>
            <ta e="T24" id="Seg_459" s="T23">recently</ta>
            <ta e="T25" id="Seg_460" s="T24">say-CO-3SG.O</ta>
            <ta e="T26" id="Seg_461" s="T25">I.NOM</ta>
            <ta e="T27" id="Seg_462" s="T26">eat.one's.fill-1SG.S</ta>
            <ta e="T28" id="Seg_463" s="T27">herring-INSTR</ta>
            <ta e="T29" id="Seg_464" s="T28">city-ADJZ</ta>
            <ta e="T30" id="Seg_465" s="T29">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T31" id="Seg_466" s="T30">come-FUT-3SG.S</ta>
            <ta e="T32" id="Seg_467" s="T31">or</ta>
            <ta e="T33" id="Seg_468" s="T32">NEG</ta>
            <ta e="T34" id="Seg_469" s="T33">come-FUT-3SG.S</ta>
            <ta e="T35" id="Seg_470" s="T34">tea.[NOM]</ta>
            <ta e="T36" id="Seg_471" s="T35">freeze-DRV-PST.NAR.[3SG.S]</ta>
            <ta e="T37" id="Seg_472" s="T36">and</ta>
            <ta e="T38" id="Seg_473" s="T37">I.NOM</ta>
            <ta e="T39" id="Seg_474" s="T38">boil-CVB</ta>
            <ta e="T40" id="Seg_475" s="T39">drink-PST-1SG.O</ta>
            <ta e="T41" id="Seg_476" s="T40">if</ta>
            <ta e="T42" id="Seg_477" s="T41">paunch.[NOM]</ta>
            <ta e="T43" id="Seg_478" s="T42">split-3SG.S</ta>
            <ta e="T44" id="Seg_479" s="T43">clothes-EP-GEN</ta>
            <ta e="T45" id="Seg_480" s="T44">inside-LOC</ta>
            <ta e="T46" id="Seg_481" s="T45">NEG</ta>
            <ta e="T47" id="Seg_482" s="T46">be.visible-3SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_483" s="T1">чай.[NOM]</ta>
            <ta e="T3" id="Seg_484" s="T2">разогреть-FUT-1SG.S</ta>
            <ta e="T4" id="Seg_485" s="T3">какао-COM</ta>
            <ta e="T5" id="Seg_486" s="T4">быть.вкусным-FUT-3SG.S</ta>
            <ta e="T6" id="Seg_487" s="T5">я.NOM</ta>
            <ta e="T7" id="Seg_488" s="T6">%помешать-CVB</ta>
            <ta e="T8" id="Seg_489" s="T7">принести-EP-FRQ-CO-1SG.O</ta>
            <ta e="T9" id="Seg_490" s="T8">ложка-INSTR</ta>
            <ta e="T10" id="Seg_491" s="T9">сахар-EP-ACC</ta>
            <ta e="T11" id="Seg_492" s="T10">%помешать-1SG.O</ta>
            <ta e="T12" id="Seg_493" s="T11">сейчас</ta>
            <ta e="T13" id="Seg_494" s="T12">перевернуть-FUT-1SG.O</ta>
            <ta e="T14" id="Seg_495" s="T13">чай-EP-ILL</ta>
            <ta e="T15" id="Seg_496" s="T14">он(а).[NOM]</ta>
            <ta e="T16" id="Seg_497" s="T15">кипеть-RFL-PFV-FUT-GEN</ta>
            <ta e="T17" id="Seg_498" s="T16">чай.[NOM]</ta>
            <ta e="T18" id="Seg_499" s="T17">согреться-3SG.S</ta>
            <ta e="T19" id="Seg_500" s="T18">чай.[NOM]</ta>
            <ta e="T20" id="Seg_501" s="T19">налить-INF</ta>
            <ta e="T21" id="Seg_502" s="T20">налить-IMP.2SG.O</ta>
            <ta e="T22" id="Seg_503" s="T21">%раньше</ta>
            <ta e="T23" id="Seg_504" s="T22">IRREAL</ta>
            <ta e="T24" id="Seg_505" s="T23">давеча</ta>
            <ta e="T25" id="Seg_506" s="T24">сказать-CO-3SG.O</ta>
            <ta e="T26" id="Seg_507" s="T25">я.NOM</ta>
            <ta e="T27" id="Seg_508" s="T26">насытиться-1SG.S</ta>
            <ta e="T28" id="Seg_509" s="T27">селедка-INSTR</ta>
            <ta e="T29" id="Seg_510" s="T28">город-ADJZ</ta>
            <ta e="T30" id="Seg_511" s="T29">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T31" id="Seg_512" s="T30">приехать-FUT-3SG.S</ta>
            <ta e="T32" id="Seg_513" s="T31">али</ta>
            <ta e="T33" id="Seg_514" s="T32">NEG</ta>
            <ta e="T34" id="Seg_515" s="T33">приехать-FUT-3SG.S</ta>
            <ta e="T35" id="Seg_516" s="T34">чай.[NOM]</ta>
            <ta e="T36" id="Seg_517" s="T35">замерзнуть-DRV-PST.NAR.[3SG.S]</ta>
            <ta e="T37" id="Seg_518" s="T36">а</ta>
            <ta e="T38" id="Seg_519" s="T37">я.NOM</ta>
            <ta e="T39" id="Seg_520" s="T38">кипеть-CVB</ta>
            <ta e="T40" id="Seg_521" s="T39">пить-PST-1SG.O</ta>
            <ta e="T41" id="Seg_522" s="T40">если</ta>
            <ta e="T42" id="Seg_523" s="T41">живот.[NOM]</ta>
            <ta e="T43" id="Seg_524" s="T42">расколоться-3SG.S</ta>
            <ta e="T44" id="Seg_525" s="T43">одежда-EP-GEN</ta>
            <ta e="T45" id="Seg_526" s="T44">нутро-LOC</ta>
            <ta e="T46" id="Seg_527" s="T45">NEG</ta>
            <ta e="T47" id="Seg_528" s="T46">виднеться-3SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_529" s="T1">n.[n:case]</ta>
            <ta e="T3" id="Seg_530" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_531" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_532" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_533" s="T5">pers</ta>
            <ta e="T7" id="Seg_534" s="T6">v-v&gt;adv</ta>
            <ta e="T8" id="Seg_535" s="T7">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T9" id="Seg_536" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_537" s="T9">n-n:ins-n:case</ta>
            <ta e="T11" id="Seg_538" s="T10">v-v:pn</ta>
            <ta e="T12" id="Seg_539" s="T11">adv</ta>
            <ta e="T13" id="Seg_540" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_541" s="T13">n-n:ins-n:case</ta>
            <ta e="T15" id="Seg_542" s="T14">pers.[n:case]</ta>
            <ta e="T16" id="Seg_543" s="T15">v-v&gt;v-v&gt;v-v:tense-n:case</ta>
            <ta e="T17" id="Seg_544" s="T16">n.[n:case]</ta>
            <ta e="T18" id="Seg_545" s="T17">v-v:pn</ta>
            <ta e="T19" id="Seg_546" s="T18">n.[n:case]</ta>
            <ta e="T20" id="Seg_547" s="T19">v-v:inf</ta>
            <ta e="T21" id="Seg_548" s="T20">v-v:mood.pn</ta>
            <ta e="T22" id="Seg_549" s="T21">adv</ta>
            <ta e="T23" id="Seg_550" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_551" s="T23">adv</ta>
            <ta e="T25" id="Seg_552" s="T24">v-v:ins-v:pn</ta>
            <ta e="T26" id="Seg_553" s="T25">pers</ta>
            <ta e="T27" id="Seg_554" s="T26">v-v:pn</ta>
            <ta e="T28" id="Seg_555" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_556" s="T28">n-n&gt;adj</ta>
            <ta e="T30" id="Seg_557" s="T29">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T31" id="Seg_558" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_559" s="T31">conj</ta>
            <ta e="T33" id="Seg_560" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_561" s="T33">v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_562" s="T34">n.[n:case]</ta>
            <ta e="T36" id="Seg_563" s="T35">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T37" id="Seg_564" s="T36">conj</ta>
            <ta e="T38" id="Seg_565" s="T37">pers</ta>
            <ta e="T39" id="Seg_566" s="T38">v-v&gt;adv</ta>
            <ta e="T40" id="Seg_567" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_568" s="T40">conj</ta>
            <ta e="T42" id="Seg_569" s="T41">n.[n:case]</ta>
            <ta e="T43" id="Seg_570" s="T42">v-v:pn</ta>
            <ta e="T44" id="Seg_571" s="T43">n-n:ins-n:case</ta>
            <ta e="T45" id="Seg_572" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_573" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_574" s="T46">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_575" s="T1">n</ta>
            <ta e="T3" id="Seg_576" s="T2">v</ta>
            <ta e="T4" id="Seg_577" s="T3">n</ta>
            <ta e="T5" id="Seg_578" s="T4">v</ta>
            <ta e="T6" id="Seg_579" s="T5">pers</ta>
            <ta e="T7" id="Seg_580" s="T6">adv</ta>
            <ta e="T8" id="Seg_581" s="T7">v</ta>
            <ta e="T9" id="Seg_582" s="T8">n</ta>
            <ta e="T10" id="Seg_583" s="T9">n</ta>
            <ta e="T11" id="Seg_584" s="T10">v</ta>
            <ta e="T12" id="Seg_585" s="T11">adv</ta>
            <ta e="T13" id="Seg_586" s="T12">v</ta>
            <ta e="T14" id="Seg_587" s="T13">n</ta>
            <ta e="T15" id="Seg_588" s="T14">pers</ta>
            <ta e="T16" id="Seg_589" s="T15">v</ta>
            <ta e="T17" id="Seg_590" s="T16">n</ta>
            <ta e="T18" id="Seg_591" s="T17">v</ta>
            <ta e="T19" id="Seg_592" s="T18">n</ta>
            <ta e="T20" id="Seg_593" s="T19">v</ta>
            <ta e="T21" id="Seg_594" s="T20">v</ta>
            <ta e="T22" id="Seg_595" s="T21">adv</ta>
            <ta e="T23" id="Seg_596" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_597" s="T23">adv</ta>
            <ta e="T25" id="Seg_598" s="T24">v</ta>
            <ta e="T26" id="Seg_599" s="T25">pers</ta>
            <ta e="T27" id="Seg_600" s="T26">v</ta>
            <ta e="T28" id="Seg_601" s="T27">n</ta>
            <ta e="T29" id="Seg_602" s="T28">adj</ta>
            <ta e="T30" id="Seg_603" s="T29">n</ta>
            <ta e="T31" id="Seg_604" s="T30">v</ta>
            <ta e="T32" id="Seg_605" s="T31">conj</ta>
            <ta e="T33" id="Seg_606" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_607" s="T33">v</ta>
            <ta e="T35" id="Seg_608" s="T34">n</ta>
            <ta e="T36" id="Seg_609" s="T35">v</ta>
            <ta e="T37" id="Seg_610" s="T36">conj</ta>
            <ta e="T38" id="Seg_611" s="T37">pers</ta>
            <ta e="T39" id="Seg_612" s="T38">adv</ta>
            <ta e="T40" id="Seg_613" s="T39">v</ta>
            <ta e="T41" id="Seg_614" s="T40">conj</ta>
            <ta e="T42" id="Seg_615" s="T41">n</ta>
            <ta e="T43" id="Seg_616" s="T42">v</ta>
            <ta e="T44" id="Seg_617" s="T43">n</ta>
            <ta e="T45" id="Seg_618" s="T44">n</ta>
            <ta e="T46" id="Seg_619" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_620" s="T46">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_621" s="T1">np:P</ta>
            <ta e="T3" id="Seg_622" s="T2">0.1.h:A</ta>
            <ta e="T4" id="Seg_623" s="T3">np:Com</ta>
            <ta e="T5" id="Seg_624" s="T4">0.3:Th</ta>
            <ta e="T6" id="Seg_625" s="T5">pro.h:A</ta>
            <ta e="T8" id="Seg_626" s="T7">0.3:P</ta>
            <ta e="T9" id="Seg_627" s="T8">np:Ins</ta>
            <ta e="T10" id="Seg_628" s="T9">np:P</ta>
            <ta e="T11" id="Seg_629" s="T10">0.1.h:A</ta>
            <ta e="T12" id="Seg_630" s="T11">adv:Time</ta>
            <ta e="T13" id="Seg_631" s="T12">0.1.h:A 0.3:Th</ta>
            <ta e="T14" id="Seg_632" s="T13">np:G</ta>
            <ta e="T15" id="Seg_633" s="T14">pro:P</ta>
            <ta e="T17" id="Seg_634" s="T16">np:P</ta>
            <ta e="T21" id="Seg_635" s="T20">0.2.h:A 0.3:Th</ta>
            <ta e="T22" id="Seg_636" s="T21">adv:Time</ta>
            <ta e="T25" id="Seg_637" s="T24">0.3.h:A</ta>
            <ta e="T26" id="Seg_638" s="T25">pro.h:A</ta>
            <ta e="T28" id="Seg_639" s="T27">np:Ins</ta>
            <ta e="T30" id="Seg_640" s="T29">np.h:A</ta>
            <ta e="T34" id="Seg_641" s="T33">0.3.h:A</ta>
            <ta e="T35" id="Seg_642" s="T34">np:P</ta>
            <ta e="T38" id="Seg_643" s="T37">pro.h:A</ta>
            <ta e="T40" id="Seg_644" s="T39">0.3:P</ta>
            <ta e="T42" id="Seg_645" s="T41">np:P</ta>
            <ta e="T44" id="Seg_646" s="T43">np:Poss</ta>
            <ta e="T45" id="Seg_647" s="T44">np:L</ta>
            <ta e="T47" id="Seg_648" s="T46">0.3:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_649" s="T1">np:O</ta>
            <ta e="T3" id="Seg_650" s="T2">0.1.h:S v:pred</ta>
            <ta e="T5" id="Seg_651" s="T4">0.3:S v:pred</ta>
            <ta e="T6" id="Seg_652" s="T5">pro.h:S</ta>
            <ta e="T7" id="Seg_653" s="T6">s:adv</ta>
            <ta e="T8" id="Seg_654" s="T7">v:pred 0.3:O</ta>
            <ta e="T10" id="Seg_655" s="T9">np:O</ta>
            <ta e="T11" id="Seg_656" s="T10">0.1.h:S v:pred</ta>
            <ta e="T13" id="Seg_657" s="T12">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T15" id="Seg_658" s="T14">pro:S</ta>
            <ta e="T16" id="Seg_659" s="T15">v:pred</ta>
            <ta e="T17" id="Seg_660" s="T16">np:S</ta>
            <ta e="T18" id="Seg_661" s="T17">v:pred</ta>
            <ta e="T21" id="Seg_662" s="T20">0.2.h:S v:pred 0.3:O</ta>
            <ta e="T25" id="Seg_663" s="T24">0.3.h:S v:pred</ta>
            <ta e="T26" id="Seg_664" s="T25">pro.h:S</ta>
            <ta e="T27" id="Seg_665" s="T26">v:pred</ta>
            <ta e="T30" id="Seg_666" s="T29">np.h:S</ta>
            <ta e="T31" id="Seg_667" s="T30">v:pred</ta>
            <ta e="T34" id="Seg_668" s="T33">0.3.h:S v:pred</ta>
            <ta e="T35" id="Seg_669" s="T34">np:S</ta>
            <ta e="T36" id="Seg_670" s="T35">v:pred</ta>
            <ta e="T38" id="Seg_671" s="T37">pro.h:S</ta>
            <ta e="T39" id="Seg_672" s="T38">s:adv</ta>
            <ta e="T40" id="Seg_673" s="T39">v:pred 0.3:O</ta>
            <ta e="T43" id="Seg_674" s="T40">s:cond</ta>
            <ta e="T47" id="Seg_675" s="T46">0.3:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_676" s="T1">RUS:cult</ta>
            <ta e="T4" id="Seg_677" s="T3">RUS:cult</ta>
            <ta e="T10" id="Seg_678" s="T9">RUS:cult</ta>
            <ta e="T12" id="Seg_679" s="T11">RUS:core</ta>
            <ta e="T14" id="Seg_680" s="T13">RUS:cult</ta>
            <ta e="T17" id="Seg_681" s="T16">RUS:cult</ta>
            <ta e="T19" id="Seg_682" s="T18">RUS:cult</ta>
            <ta e="T23" id="Seg_683" s="T22">RUS:gram</ta>
            <ta e="T24" id="Seg_684" s="T23">RUS:disc</ta>
            <ta e="T28" id="Seg_685" s="T27">RUS:cult</ta>
            <ta e="T32" id="Seg_686" s="T31">RUS:gram</ta>
            <ta e="T35" id="Seg_687" s="T34">RUS:cult</ta>
            <ta e="T37" id="Seg_688" s="T36">RUS:gram</ta>
            <ta e="T41" id="Seg_689" s="T40">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_690" s="T1">I'll make tea with cocoa.</ta>
            <ta e="T5" id="Seg_691" s="T4">It'll be tasty.</ta>
            <ta e="T9" id="Seg_692" s="T5">I stir it with a spoon.</ta>
            <ta e="T11" id="Seg_693" s="T9">I stirred sugar.</ta>
            <ta e="T14" id="Seg_694" s="T11">Now I'll pour it into tea.</ta>
            <ta e="T16" id="Seg_695" s="T14">It'll boil.</ta>
            <ta e="T18" id="Seg_696" s="T16">Tea is ready.</ta>
            <ta e="T20" id="Seg_697" s="T18">Should I pour you tea?</ta>
            <ta e="T21" id="Seg_698" s="T20">Pour it.</ta>
            <ta e="T25" id="Seg_699" s="T21">You could have said it earlier.</ta>
            <ta e="T28" id="Seg_700" s="T25">I've already eaten lots of herring .</ta>
            <ta e="T33" id="Seg_701" s="T28">Will the urban woman come or not?</ta>
            <ta e="T34" id="Seg_702" s="T33">She will come.</ta>
            <ta e="T36" id="Seg_703" s="T34">[Your] tea got cold.</ta>
            <ta e="T40" id="Seg_704" s="T36">And I drank it hot.</ta>
            <ta e="T47" id="Seg_705" s="T40">If the paunch blows, it won't be visible under the shirt. </ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_706" s="T1">Ich mache Tee mit Kakao.</ta>
            <ta e="T5" id="Seg_707" s="T4">Er wird lecker sein.</ta>
            <ta e="T9" id="Seg_708" s="T5">Ich rühre ihn mit einem Löffel um.</ta>
            <ta e="T11" id="Seg_709" s="T9">Ich verrühre Zucker.</ta>
            <ta e="T14" id="Seg_710" s="T11">Jetzt gieße ich ihn in den Tee.</ta>
            <ta e="T16" id="Seg_711" s="T14">Er wird kochen.</ta>
            <ta e="T18" id="Seg_712" s="T16">Der Tee ist fertig.</ta>
            <ta e="T20" id="Seg_713" s="T18">Soll ich dir Tee einschenken?</ta>
            <ta e="T21" id="Seg_714" s="T20">Gieß ihn ein.</ta>
            <ta e="T25" id="Seg_715" s="T21">Du hättest früher was sagen können.</ta>
            <ta e="T28" id="Seg_716" s="T25">Ich habe schon viel Hering gegessen.</ta>
            <ta e="T33" id="Seg_717" s="T28">Wird die Frau aus der Stadt kommen oder nicht?</ta>
            <ta e="T34" id="Seg_718" s="T33">Sie wird kommen.</ta>
            <ta e="T36" id="Seg_719" s="T34">[Dein] Tee ist kalt geworden.</ta>
            <ta e="T40" id="Seg_720" s="T36">Und ich habe meinen heiß getrunken.</ta>
            <ta e="T47" id="Seg_721" s="T40">Wenn sich der Bauch aufbläht, ist es unter den Kleidern nicht zu sehen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_722" s="T1">Чай согрею с какао.</ta>
            <ta e="T5" id="Seg_723" s="T4">Вкусный будет.</ta>
            <ta e="T9" id="Seg_724" s="T5">Я мешаю ложкой.</ta>
            <ta e="T11" id="Seg_725" s="T9">Сахар помешала.</ta>
            <ta e="T14" id="Seg_726" s="T11">Сейчас вылью в чай.</ta>
            <ta e="T16" id="Seg_727" s="T14">Он вскипит.</ta>
            <ta e="T18" id="Seg_728" s="T16">Чай согрелся.</ta>
            <ta e="T20" id="Seg_729" s="T18">Чай налить?</ta>
            <ta e="T21" id="Seg_730" s="T20">Наливай.</ta>
            <ta e="T25" id="Seg_731" s="T21">Ты бы раньше сказала.</ta>
            <ta e="T28" id="Seg_732" s="T25">Я наелась уже селедкой.</ta>
            <ta e="T33" id="Seg_733" s="T28">Городская женщина приедет или нет?</ta>
            <ta e="T34" id="Seg_734" s="T33">Приедет.</ta>
            <ta e="T36" id="Seg_735" s="T34">Чай застыл.</ta>
            <ta e="T40" id="Seg_736" s="T36">А я пила горячий.</ta>
            <ta e="T47" id="Seg_737" s="T40">Если живот лопнет, под рубашкой не видать.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_738" s="T1">чай согрею с какао</ta>
            <ta e="T5" id="Seg_739" s="T4">вкусный будет</ta>
            <ta e="T9" id="Seg_740" s="T5">я мешаю ложкой</ta>
            <ta e="T11" id="Seg_741" s="T9">сахар смешала</ta>
            <ta e="T14" id="Seg_742" s="T11">сейчас вылью в чай</ta>
            <ta e="T16" id="Seg_743" s="T14">он вскипит</ta>
            <ta e="T18" id="Seg_744" s="T16">чай согрелся</ta>
            <ta e="T20" id="Seg_745" s="T18">чай налить</ta>
            <ta e="T21" id="Seg_746" s="T20">наливай</ta>
            <ta e="T25" id="Seg_747" s="T21">ты бы (раньше) сказала</ta>
            <ta e="T28" id="Seg_748" s="T25">я наелась уже селедкой</ta>
            <ta e="T33" id="Seg_749" s="T28">городская женщина приедет или нет</ta>
            <ta e="T34" id="Seg_750" s="T33">приедет</ta>
            <ta e="T36" id="Seg_751" s="T34">чай застыл</ta>
            <ta e="T40" id="Seg_752" s="T36">а я пила горячий</ta>
            <ta e="T47" id="Seg_753" s="T40">если живот лопнет под рубашкой не видать</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T5" id="Seg_754" s="T4">[BrM:] 'Nʼüː jeǯɨn' changed to 'Nʼüːjeǯɨn'.</ta>
            <ta e="T14" id="Seg_755" s="T11">[BrM:] Tentative analysis of 'kamneǯau'.</ta>
            <ta e="T16" id="Seg_756" s="T14">[BrM:] Tentative analysis of 'pertšoɣɨlǯəǯɨn'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T36" id="Seg_757" s="T34">остыл</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
