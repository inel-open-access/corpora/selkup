<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_TheFirstOfMay_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_TheFirstOfMay_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">137</ud-information>
            <ud-information attribute-name="# HIAT:w">91</ud-information>
            <ud-information attribute-name="# e">91</ud-information>
            <ud-information attribute-name="# HIAT:u">23</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T92" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Perwoje</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">majɣɨn</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">man</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">közizan</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">praznikdə</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_20" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">Datao</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">soːdʼigan</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">jes</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_32" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">Flagla</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">tiːrbɨzattə</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">wsʼäkij</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">raznɨj</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_47" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">Saldatla</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">koːcin</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">jezattə</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_59" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">Perwo</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">saldatla</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">mʼänɨdattə</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_69" n="HIAT:ip">(</nts>
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">metdɨzattə</ts>
                  <nts id="Seg_72" n="HIAT:ip">)</nts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_76" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">Patom</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_81" n="HIAT:w" s="T21">školala</ts>
                  <nts id="Seg_82" n="HIAT:ip">.</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_85" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_87" n="HIAT:w" s="T22">Qula</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_90" n="HIAT:w" s="T23">wes</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_93" n="HIAT:w" s="T24">toɣulǯəlʼe</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_96" n="HIAT:w" s="T25">tadəratdə</ts>
                  <nts id="Seg_97" n="HIAT:ip">.</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_100" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_102" n="HIAT:w" s="T26">Tʼärattə</ts>
                  <nts id="Seg_103" n="HIAT:ip">:</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_105" n="HIAT:ip">“</nts>
                  <ts e="T28" id="Seg_107" n="HIAT:w" s="T27">Uniwersitʼet</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_110" n="HIAT:w" s="T28">čaːʒətda</ts>
                  <nts id="Seg_111" n="HIAT:ip">.</nts>
                  <nts id="Seg_112" n="HIAT:ip">”</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_115" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_117" n="HIAT:w" s="T29">Man</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_120" n="HIAT:w" s="T30">čaʒekan</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_123" n="HIAT:w" s="T31">nɨlʼedʼän</ts>
                  <nts id="Seg_124" n="HIAT:ip">.</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_127" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_129" n="HIAT:w" s="T32">Mannɨmɨtdau</ts>
                  <nts id="Seg_130" n="HIAT:ip">,</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_133" n="HIAT:w" s="T33">Angelina</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_136" n="HIAT:w" s="T34">Iwanowna</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_139" n="HIAT:w" s="T35">čaʒɨn</ts>
                  <nts id="Seg_140" n="HIAT:ip">.</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_143" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_145" n="HIAT:w" s="T36">Man</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_148" n="HIAT:w" s="T37">tʼaran</ts>
                  <nts id="Seg_149" n="HIAT:ip">:</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_151" n="HIAT:ip">“</nts>
                  <ts e="T39" id="Seg_153" n="HIAT:w" s="T38">Tʼolom</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_156" n="HIAT:w" s="T39">Angelina</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_159" n="HIAT:w" s="T40">Iwanowna</ts>
                  <nts id="Seg_160" n="HIAT:ip">,</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_163" n="HIAT:w" s="T41">prazdnikse</ts>
                  <nts id="Seg_164" n="HIAT:ip">.</nts>
                  <nts id="Seg_165" n="HIAT:ip">”</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_168" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_170" n="HIAT:w" s="T42">A</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_173" n="HIAT:w" s="T43">täp</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_176" n="HIAT:w" s="T44">jassʼ</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_179" n="HIAT:w" s="T45">ütdädit</ts>
                  <nts id="Seg_180" n="HIAT:ip">.</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_183" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_185" n="HIAT:w" s="T46">Qwannɨn</ts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_189" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_191" n="HIAT:w" s="T47">Man</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_194" n="HIAT:w" s="T48">pajaganä</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_197" n="HIAT:w" s="T49">tʼaran</ts>
                  <nts id="Seg_198" n="HIAT:ip">:</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_200" n="HIAT:ip">“</nts>
                  <ts e="T51" id="Seg_202" n="HIAT:w" s="T50">Tan</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_205" n="HIAT:w" s="T51">iːl</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_208" n="HIAT:w" s="T52">menäǯa</ts>
                  <nts id="Seg_209" n="HIAT:ip">.</nts>
                  <nts id="Seg_210" n="HIAT:ip">”</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_213" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_215" n="HIAT:w" s="T53">Sečas</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_218" n="HIAT:w" s="T54">mannɨpan</ts>
                  <nts id="Seg_219" n="HIAT:ip">,</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_222" n="HIAT:w" s="T55">Valʼka</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_225" n="HIAT:w" s="T56">čaːʒan</ts>
                  <nts id="Seg_226" n="HIAT:ip">.</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_229" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_231" n="HIAT:w" s="T57">Aj</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_234" n="HIAT:w" s="T58">toɣulǯattə</ts>
                  <nts id="Seg_235" n="HIAT:ip">,</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_238" n="HIAT:w" s="T59">tʼärattə</ts>
                  <nts id="Seg_239" n="HIAT:ip">:</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_241" n="HIAT:ip">“</nts>
                  <ts e="T61" id="Seg_243" n="HIAT:w" s="T60">Palʼnica</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_246" n="HIAT:w" s="T61">čaʒət</ts>
                  <nts id="Seg_247" n="HIAT:ip">.</nts>
                  <nts id="Seg_248" n="HIAT:ip">”</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_251" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_253" n="HIAT:w" s="T62">Man</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_256" n="HIAT:w" s="T63">mannɨpan</ts>
                  <nts id="Seg_257" n="HIAT:ip">,</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_260" n="HIAT:w" s="T64">täjärbɨzan</ts>
                  <nts id="Seg_261" n="HIAT:ip">:</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_264" n="HIAT:w" s="T65">Aːda</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_267" n="HIAT:w" s="T66">meneǯa</ts>
                  <nts id="Seg_268" n="HIAT:ip">,</nts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_271" n="HIAT:w" s="T67">Nataša</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_274" n="HIAT:w" s="T68">meneǯa</ts>
                  <nts id="Seg_275" n="HIAT:ip">?</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_278" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_280" n="HIAT:w" s="T69">A</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_283" n="HIAT:w" s="T70">täbɨstaq</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_286" n="HIAT:w" s="T71">ass</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_289" n="HIAT:w" s="T72">közəbbaɣə</ts>
                  <nts id="Seg_290" n="HIAT:ip">.</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_293" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_295" n="HIAT:w" s="T73">Man</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_298" n="HIAT:w" s="T74">mannɨpaːn</ts>
                  <nts id="Seg_299" n="HIAT:ip">,</nts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_302" n="HIAT:w" s="T75">mannɨpan</ts>
                  <nts id="Seg_303" n="HIAT:ip">,</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_306" n="HIAT:w" s="T76">qajɣɨnä</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_309" n="HIAT:w" s="T77">tʼägwaɣə</ts>
                  <nts id="Seg_310" n="HIAT:ip">.</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_313" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_315" n="HIAT:w" s="T78">A</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_318" n="HIAT:w" s="T79">täbɨstaɣə</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_321" n="HIAT:w" s="T80">jess</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_324" n="HIAT:w" s="T81">közipbaɣə</ts>
                  <nts id="Seg_325" n="HIAT:ip">.</nts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_328" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_330" n="HIAT:w" s="T82">A</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_333" n="HIAT:w" s="T83">bolʼše</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_336" n="HIAT:w" s="T84">kudämnäss</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_339" n="HIAT:w" s="T85">tunou</ts>
                  <nts id="Seg_340" n="HIAT:ip">.</nts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_343" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_345" n="HIAT:w" s="T86">No</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_348" n="HIAT:w" s="T87">qula</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_351" n="HIAT:w" s="T88">koːcʼin</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_354" n="HIAT:w" s="T89">jezattə</ts>
                  <nts id="Seg_355" n="HIAT:ip">.</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_358" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_360" n="HIAT:w" s="T90">Soːdʼigan</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_363" n="HIAT:w" s="T91">tʼipbɨppɨzattə</ts>
                  <nts id="Seg_364" n="HIAT:ip">.</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T92" id="Seg_366" n="sc" s="T1">
               <ts e="T2" id="Seg_368" n="e" s="T1">Perwoje </ts>
               <ts e="T3" id="Seg_370" n="e" s="T2">majɣɨn </ts>
               <ts e="T4" id="Seg_372" n="e" s="T3">man </ts>
               <ts e="T5" id="Seg_374" n="e" s="T4">közizan </ts>
               <ts e="T6" id="Seg_376" n="e" s="T5">praznikdə. </ts>
               <ts e="T7" id="Seg_378" n="e" s="T6">Datao </ts>
               <ts e="T8" id="Seg_380" n="e" s="T7">soːdʼigan </ts>
               <ts e="T9" id="Seg_382" n="e" s="T8">jes. </ts>
               <ts e="T10" id="Seg_384" n="e" s="T9">Flagla </ts>
               <ts e="T11" id="Seg_386" n="e" s="T10">tiːrbɨzattə </ts>
               <ts e="T12" id="Seg_388" n="e" s="T11">wsʼäkij </ts>
               <ts e="T13" id="Seg_390" n="e" s="T12">raznɨj. </ts>
               <ts e="T14" id="Seg_392" n="e" s="T13">Saldatla </ts>
               <ts e="T15" id="Seg_394" n="e" s="T14">koːcin </ts>
               <ts e="T16" id="Seg_396" n="e" s="T15">jezattə. </ts>
               <ts e="T17" id="Seg_398" n="e" s="T16">Perwo </ts>
               <ts e="T18" id="Seg_400" n="e" s="T17">saldatla </ts>
               <ts e="T19" id="Seg_402" n="e" s="T18">mʼänɨdattə </ts>
               <ts e="T20" id="Seg_404" n="e" s="T19">(metdɨzattə). </ts>
               <ts e="T21" id="Seg_406" n="e" s="T20">Patom </ts>
               <ts e="T22" id="Seg_408" n="e" s="T21">školala. </ts>
               <ts e="T23" id="Seg_410" n="e" s="T22">Qula </ts>
               <ts e="T24" id="Seg_412" n="e" s="T23">wes </ts>
               <ts e="T25" id="Seg_414" n="e" s="T24">toɣulǯəlʼe </ts>
               <ts e="T26" id="Seg_416" n="e" s="T25">tadəratdə. </ts>
               <ts e="T27" id="Seg_418" n="e" s="T26">Tʼärattə: </ts>
               <ts e="T28" id="Seg_420" n="e" s="T27">“Uniwersitʼet </ts>
               <ts e="T29" id="Seg_422" n="e" s="T28">čaːʒətda.” </ts>
               <ts e="T30" id="Seg_424" n="e" s="T29">Man </ts>
               <ts e="T31" id="Seg_426" n="e" s="T30">čaʒekan </ts>
               <ts e="T32" id="Seg_428" n="e" s="T31">nɨlʼedʼän. </ts>
               <ts e="T33" id="Seg_430" n="e" s="T32">Mannɨmɨtdau, </ts>
               <ts e="T34" id="Seg_432" n="e" s="T33">Angelina </ts>
               <ts e="T35" id="Seg_434" n="e" s="T34">Iwanowna </ts>
               <ts e="T36" id="Seg_436" n="e" s="T35">čaʒɨn. </ts>
               <ts e="T37" id="Seg_438" n="e" s="T36">Man </ts>
               <ts e="T38" id="Seg_440" n="e" s="T37">tʼaran: </ts>
               <ts e="T39" id="Seg_442" n="e" s="T38">“Tʼolom </ts>
               <ts e="T40" id="Seg_444" n="e" s="T39">Angelina </ts>
               <ts e="T41" id="Seg_446" n="e" s="T40">Iwanowna, </ts>
               <ts e="T42" id="Seg_448" n="e" s="T41">prazdnikse.” </ts>
               <ts e="T43" id="Seg_450" n="e" s="T42">A </ts>
               <ts e="T44" id="Seg_452" n="e" s="T43">täp </ts>
               <ts e="T45" id="Seg_454" n="e" s="T44">jassʼ </ts>
               <ts e="T46" id="Seg_456" n="e" s="T45">ütdädit. </ts>
               <ts e="T47" id="Seg_458" n="e" s="T46">Qwannɨn. </ts>
               <ts e="T48" id="Seg_460" n="e" s="T47">Man </ts>
               <ts e="T49" id="Seg_462" n="e" s="T48">pajaganä </ts>
               <ts e="T50" id="Seg_464" n="e" s="T49">tʼaran: </ts>
               <ts e="T51" id="Seg_466" n="e" s="T50">“Tan </ts>
               <ts e="T52" id="Seg_468" n="e" s="T51">iːl </ts>
               <ts e="T53" id="Seg_470" n="e" s="T52">menäǯa.” </ts>
               <ts e="T54" id="Seg_472" n="e" s="T53">Sečas </ts>
               <ts e="T55" id="Seg_474" n="e" s="T54">mannɨpan, </ts>
               <ts e="T56" id="Seg_476" n="e" s="T55">Valʼka </ts>
               <ts e="T57" id="Seg_478" n="e" s="T56">čaːʒan. </ts>
               <ts e="T58" id="Seg_480" n="e" s="T57">Aj </ts>
               <ts e="T59" id="Seg_482" n="e" s="T58">toɣulǯattə, </ts>
               <ts e="T60" id="Seg_484" n="e" s="T59">tʼärattə: </ts>
               <ts e="T61" id="Seg_486" n="e" s="T60">“Palʼnica </ts>
               <ts e="T62" id="Seg_488" n="e" s="T61">čaʒət.” </ts>
               <ts e="T63" id="Seg_490" n="e" s="T62">Man </ts>
               <ts e="T64" id="Seg_492" n="e" s="T63">mannɨpan, </ts>
               <ts e="T65" id="Seg_494" n="e" s="T64">täjärbɨzan: </ts>
               <ts e="T66" id="Seg_496" n="e" s="T65">Aːda </ts>
               <ts e="T67" id="Seg_498" n="e" s="T66">meneǯa, </ts>
               <ts e="T68" id="Seg_500" n="e" s="T67">Nataša </ts>
               <ts e="T69" id="Seg_502" n="e" s="T68">meneǯa? </ts>
               <ts e="T70" id="Seg_504" n="e" s="T69">A </ts>
               <ts e="T71" id="Seg_506" n="e" s="T70">täbɨstaq </ts>
               <ts e="T72" id="Seg_508" n="e" s="T71">ass </ts>
               <ts e="T73" id="Seg_510" n="e" s="T72">közəbbaɣə. </ts>
               <ts e="T74" id="Seg_512" n="e" s="T73">Man </ts>
               <ts e="T75" id="Seg_514" n="e" s="T74">mannɨpaːn, </ts>
               <ts e="T76" id="Seg_516" n="e" s="T75">mannɨpan, </ts>
               <ts e="T77" id="Seg_518" n="e" s="T76">qajɣɨnä </ts>
               <ts e="T78" id="Seg_520" n="e" s="T77">tʼägwaɣə. </ts>
               <ts e="T79" id="Seg_522" n="e" s="T78">A </ts>
               <ts e="T80" id="Seg_524" n="e" s="T79">täbɨstaɣə </ts>
               <ts e="T81" id="Seg_526" n="e" s="T80">jess </ts>
               <ts e="T82" id="Seg_528" n="e" s="T81">közipbaɣə. </ts>
               <ts e="T83" id="Seg_530" n="e" s="T82">A </ts>
               <ts e="T84" id="Seg_532" n="e" s="T83">bolʼše </ts>
               <ts e="T85" id="Seg_534" n="e" s="T84">kudämnäss </ts>
               <ts e="T86" id="Seg_536" n="e" s="T85">tunou. </ts>
               <ts e="T87" id="Seg_538" n="e" s="T86">No </ts>
               <ts e="T88" id="Seg_540" n="e" s="T87">qula </ts>
               <ts e="T89" id="Seg_542" n="e" s="T88">koːcʼin </ts>
               <ts e="T90" id="Seg_544" n="e" s="T89">jezattə. </ts>
               <ts e="T91" id="Seg_546" n="e" s="T90">Soːdʼigan </ts>
               <ts e="T92" id="Seg_548" n="e" s="T91">tʼipbɨppɨzattə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_549" s="T1">PVD_1964_TheFirstOfMay_nar.001 (001.001)</ta>
            <ta e="T9" id="Seg_550" s="T6">PVD_1964_TheFirstOfMay_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_551" s="T9">PVD_1964_TheFirstOfMay_nar.003 (001.003)</ta>
            <ta e="T16" id="Seg_552" s="T13">PVD_1964_TheFirstOfMay_nar.004 (001.004)</ta>
            <ta e="T20" id="Seg_553" s="T16">PVD_1964_TheFirstOfMay_nar.005 (001.005)</ta>
            <ta e="T22" id="Seg_554" s="T20">PVD_1964_TheFirstOfMay_nar.006 (001.006)</ta>
            <ta e="T26" id="Seg_555" s="T22">PVD_1964_TheFirstOfMay_nar.007 (001.007)</ta>
            <ta e="T29" id="Seg_556" s="T26">PVD_1964_TheFirstOfMay_nar.008 (001.008)</ta>
            <ta e="T32" id="Seg_557" s="T29">PVD_1964_TheFirstOfMay_nar.009 (001.009)</ta>
            <ta e="T36" id="Seg_558" s="T32">PVD_1964_TheFirstOfMay_nar.010 (001.010)</ta>
            <ta e="T42" id="Seg_559" s="T36">PVD_1964_TheFirstOfMay_nar.011 (001.011)</ta>
            <ta e="T46" id="Seg_560" s="T42">PVD_1964_TheFirstOfMay_nar.012 (001.012)</ta>
            <ta e="T47" id="Seg_561" s="T46">PVD_1964_TheFirstOfMay_nar.013 (001.013)</ta>
            <ta e="T53" id="Seg_562" s="T47">PVD_1964_TheFirstOfMay_nar.014 (001.014)</ta>
            <ta e="T57" id="Seg_563" s="T53">PVD_1964_TheFirstOfMay_nar.015 (001.015)</ta>
            <ta e="T62" id="Seg_564" s="T57">PVD_1964_TheFirstOfMay_nar.016 (001.016)</ta>
            <ta e="T69" id="Seg_565" s="T62">PVD_1964_TheFirstOfMay_nar.017 (001.017)</ta>
            <ta e="T73" id="Seg_566" s="T69">PVD_1964_TheFirstOfMay_nar.018 (001.018)</ta>
            <ta e="T78" id="Seg_567" s="T73">PVD_1964_TheFirstOfMay_nar.019 (001.019)</ta>
            <ta e="T82" id="Seg_568" s="T78">PVD_1964_TheFirstOfMay_nar.020 (001.020)</ta>
            <ta e="T86" id="Seg_569" s="T82">PVD_1964_TheFirstOfMay_nar.021 (001.021)</ta>
            <ta e="T90" id="Seg_570" s="T86">PVD_1964_TheFirstOfMay_nar.022 (001.022)</ta>
            <ta e="T92" id="Seg_571" s="T90">PVD_1964_TheFirstOfMay_nar.023 (001.023)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_572" s="T1">′первоjе ′майɣын ман ′кӧзизан ′празникдъ.</ta>
            <ta e="T9" id="Seg_573" s="T6">д̂а та′о ′со̄дʼиган jес.</ta>
            <ta e="T13" id="Seg_574" s="T9">′флагла ′тӣрбы′заттъ всʼӓкий ′разный.</ta>
            <ta e="T16" id="Seg_575" s="T13">сал′датла ′ко̄цин ′jезаттъ.</ta>
            <ta e="T20" id="Seg_576" s="T16">перво сал′датла мʼӓны′даттъ (′метдызаттъ).</ta>
            <ta e="T22" id="Seg_577" s="T20">па′том ′школала.</ta>
            <ta e="T26" id="Seg_578" s="T22">kу′lа вес ′тоɣуlджълʼе ′тадъратдъ.</ta>
            <ta e="T29" id="Seg_579" s="T26">тʼӓраттъ: универси′тʼет ′тша̄жътда.</ta>
            <ta e="T32" id="Seg_580" s="T29">ман тша′жекан ′нылʼедʼӓн.</ta>
            <ta e="T36" id="Seg_581" s="T32">′маннымыт′дау, Ангелина Ивановна тшажын.</ta>
            <ta e="T42" id="Seg_582" s="T36">ман тʼа′ран: тʼо′лом Ангелина Ивановна, праздниксе.</ta>
            <ta e="T46" id="Seg_583" s="T42">а тӓп jассʼ ӱт′дӓдит.</ta>
            <ta e="T47" id="Seg_584" s="T46">′kwаннын.</ta>
            <ta e="T53" id="Seg_585" s="T47">ман па′jаганӓ тʼа′ран: тан ′ӣл ме′нӓ(е)джа.</ta>
            <ta e="T57" id="Seg_586" s="T53">се′час манны′пан, Валʼка ′тша̄жан.</ta>
            <ta e="T62" id="Seg_587" s="T57">ай ′тоɣуlджаттъ, тʼӓ′раттъ: па′лʼница тшажът.</ta>
            <ta e="T69" id="Seg_588" s="T62">ман маннып(б̂)ан, ′тӓйӓрбызан: А̄да ме′неджа, Наташа ме′неджа.</ta>
            <ta e="T73" id="Seg_589" s="T69">а тӓбыстаk асс ′кӧзъб̂б̂аɣъ.</ta>
            <ta e="T78" id="Seg_590" s="T73">ман ′манныпа̄н, ′манныпан, kайɣы′нӓ тʼӓ′гваɣъ.</ta>
            <ta e="T82" id="Seg_591" s="T78">а тӓбыс′таɣъ ′jесс ′кӧзипб̂аɣъ.</ta>
            <ta e="T86" id="Seg_592" s="T82">а ′болʼше ′кудӓм нӓсс ту′ноу.</ta>
            <ta e="T90" id="Seg_593" s="T86">но kу′lа ′ко̄цʼин jе′заттъ.</ta>
            <ta e="T92" id="Seg_594" s="T90">со̄дʼиган ′тʼипбыппы′заттъ.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_595" s="T1">perwoje majɣɨn man közizan praznikdə.</ta>
            <ta e="T9" id="Seg_596" s="T6">d̂a tao soːdʼigan jes.</ta>
            <ta e="T13" id="Seg_597" s="T9">flagla tiːrbɨzattə wsʼäkij raznɨj.</ta>
            <ta e="T16" id="Seg_598" s="T13">saldatla koːcin jezattə.</ta>
            <ta e="T20" id="Seg_599" s="T16">perwo saldatla mʼänɨdattə (metdɨzattə).</ta>
            <ta e="T22" id="Seg_600" s="T20">patom školala.</ta>
            <ta e="T26" id="Seg_601" s="T22">qula wes toɣulǯəlʼe tadəratdə.</ta>
            <ta e="T29" id="Seg_602" s="T26">tʼärattə: uniwersitʼet tšaːʒətda.</ta>
            <ta e="T32" id="Seg_603" s="T29">man tšaʒekan nɨlʼedʼän.</ta>
            <ta e="T36" id="Seg_604" s="T32">mannɨmɨtdau, Аngelina Иwanowna tšaʒɨn.</ta>
            <ta e="T42" id="Seg_605" s="T36">man tʼaran: tʼolom Аngelina Иwanowna, prazdnikse.</ta>
            <ta e="T46" id="Seg_606" s="T42">a täp jassʼ ütdädit.</ta>
            <ta e="T47" id="Seg_607" s="T46">qwannɨn.</ta>
            <ta e="T53" id="Seg_608" s="T47">man pajaganä tʼaran: tan iːl menä(e)ǯa.</ta>
            <ta e="T57" id="Seg_609" s="T53">sečas mannɨpan, Вalʼka tšaːʒan.</ta>
            <ta e="T62" id="Seg_610" s="T57">aj toɣulǯattə, tʼärattə: palʼnica tšaʒət.</ta>
            <ta e="T69" id="Seg_611" s="T62">man mannɨp(b̂)an, täjärbɨzan: Аːda meneǯa, Нataša meneǯa.</ta>
            <ta e="T73" id="Seg_612" s="T69">a täbɨstaq ass közəb̂b̂aɣə.</ta>
            <ta e="T78" id="Seg_613" s="T73">man mannɨpaːn, mannɨpan, qajɣɨnä tʼägwaɣə.</ta>
            <ta e="T82" id="Seg_614" s="T78">a täbɨstaɣə jess közipb̂aɣə.</ta>
            <ta e="T86" id="Seg_615" s="T82">a bolʼše kudäm näss tunou.</ta>
            <ta e="T90" id="Seg_616" s="T86">no qula koːcʼin jezattə.</ta>
            <ta e="T92" id="Seg_617" s="T90">soːdʼigan tʼipbɨppɨzattə.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_618" s="T1">Perwoje majɣɨn man közizan praznikdə. </ta>
            <ta e="T9" id="Seg_619" s="T6">Datao soːdʼigan jes. </ta>
            <ta e="T13" id="Seg_620" s="T9">Flagla tiːrbɨzattə wsʼäkij raznɨj. </ta>
            <ta e="T16" id="Seg_621" s="T13">Saldatla koːcin jezattə. </ta>
            <ta e="T20" id="Seg_622" s="T16">Perwo saldatla mʼänɨdattə (metdɨzattə). </ta>
            <ta e="T22" id="Seg_623" s="T20">Patom školala. </ta>
            <ta e="T26" id="Seg_624" s="T22">Qula wes toɣulǯəlʼe tadəratdə. </ta>
            <ta e="T29" id="Seg_625" s="T26">Tʼärattə: “Uniwersitʼet čaːʒətda.” </ta>
            <ta e="T32" id="Seg_626" s="T29">Man čaʒekan nɨlʼedʼän. </ta>
            <ta e="T36" id="Seg_627" s="T32">Mannɨmɨtdau, Angelina Iwanowna čaʒɨn. </ta>
            <ta e="T42" id="Seg_628" s="T36">Man tʼaran: “Tʼolom Angelina Iwanowna, prazdnikse.” </ta>
            <ta e="T46" id="Seg_629" s="T42">A täp jassʼ ütdädit. </ta>
            <ta e="T47" id="Seg_630" s="T46">Qwannɨn. </ta>
            <ta e="T53" id="Seg_631" s="T47">Man pajaganä tʼaran: “Tan iːl menäǯa.” </ta>
            <ta e="T57" id="Seg_632" s="T53">Sečas mannɨpan, Valʼka čaːʒan. </ta>
            <ta e="T62" id="Seg_633" s="T57">Aj toɣulǯattə, tʼärattə: “Palʼnica čaʒət.” </ta>
            <ta e="T69" id="Seg_634" s="T62">Man mannɨpan, täjärbɨzan: Aːda meneǯa, Nataša meneǯa? </ta>
            <ta e="T73" id="Seg_635" s="T69">A täbɨstaq ass közəbbaɣə. </ta>
            <ta e="T78" id="Seg_636" s="T73">Man mannɨpaːn, mannɨpan, qajɣɨnä tʼägwaɣə. </ta>
            <ta e="T82" id="Seg_637" s="T78">A täbɨstaɣə jess közipbaɣə. </ta>
            <ta e="T86" id="Seg_638" s="T82">A bolʼše kudämnäss tunou. </ta>
            <ta e="T90" id="Seg_639" s="T86">No qula koːcʼin jezattə. </ta>
            <ta e="T92" id="Seg_640" s="T90">Soːdʼigan tʼipbɨppɨzattə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_641" s="T1">perwoje</ta>
            <ta e="T3" id="Seg_642" s="T2">maj-ɣɨn</ta>
            <ta e="T4" id="Seg_643" s="T3">man</ta>
            <ta e="T5" id="Seg_644" s="T4">közi-za-n</ta>
            <ta e="T6" id="Seg_645" s="T5">praznik-də</ta>
            <ta e="T7" id="Seg_646" s="T6">datao</ta>
            <ta e="T8" id="Seg_647" s="T7">soːdʼiga-n</ta>
            <ta e="T9" id="Seg_648" s="T8">je-s</ta>
            <ta e="T10" id="Seg_649" s="T9">flag-la</ta>
            <ta e="T11" id="Seg_650" s="T10">tiːrbɨ-za-ttə</ta>
            <ta e="T14" id="Seg_651" s="T13">saldat-la</ta>
            <ta e="T15" id="Seg_652" s="T14">koːci-n</ta>
            <ta e="T16" id="Seg_653" s="T15">je-za-ttə</ta>
            <ta e="T17" id="Seg_654" s="T16">perwo</ta>
            <ta e="T18" id="Seg_655" s="T17">saldat-la</ta>
            <ta e="T19" id="Seg_656" s="T18">mʼänɨda-ttə</ta>
            <ta e="T20" id="Seg_657" s="T19">metdɨ-za-ttə</ta>
            <ta e="T21" id="Seg_658" s="T20">patom</ta>
            <ta e="T22" id="Seg_659" s="T21">škola-la</ta>
            <ta e="T23" id="Seg_660" s="T22">qu-la</ta>
            <ta e="T24" id="Seg_661" s="T23">wes</ta>
            <ta e="T25" id="Seg_662" s="T24">toɣu-lǯə-lʼe</ta>
            <ta e="T26" id="Seg_663" s="T25">tad-ə-r-a-tdə</ta>
            <ta e="T27" id="Seg_664" s="T26">tʼära-ttə</ta>
            <ta e="T29" id="Seg_665" s="T28">čaːʒə-tda</ta>
            <ta e="T30" id="Seg_666" s="T29">man</ta>
            <ta e="T31" id="Seg_667" s="T30">čaʒek-a-n</ta>
            <ta e="T32" id="Seg_668" s="T31">nɨ-lʼ-edʼä-n</ta>
            <ta e="T33" id="Seg_669" s="T32">mannɨ-mɨ-tda-u</ta>
            <ta e="T34" id="Seg_670" s="T33">Angelina</ta>
            <ta e="T35" id="Seg_671" s="T34">Iwanowna</ta>
            <ta e="T36" id="Seg_672" s="T35">čaʒɨ-n</ta>
            <ta e="T37" id="Seg_673" s="T36">man</ta>
            <ta e="T38" id="Seg_674" s="T37">tʼara-n</ta>
            <ta e="T39" id="Seg_675" s="T38">tʼolom</ta>
            <ta e="T40" id="Seg_676" s="T39">Angelina</ta>
            <ta e="T41" id="Seg_677" s="T40">Iwanowna</ta>
            <ta e="T42" id="Seg_678" s="T41">prazdnik-se</ta>
            <ta e="T43" id="Seg_679" s="T42">a</ta>
            <ta e="T44" id="Seg_680" s="T43">täp</ta>
            <ta e="T45" id="Seg_681" s="T44">jassʼ</ta>
            <ta e="T46" id="Seg_682" s="T45">ütdä-di-t</ta>
            <ta e="T47" id="Seg_683" s="T46">qwan-nɨ-n</ta>
            <ta e="T48" id="Seg_684" s="T47">man</ta>
            <ta e="T49" id="Seg_685" s="T48">paja-ga-nä</ta>
            <ta e="T50" id="Seg_686" s="T49">tʼara-n</ta>
            <ta e="T51" id="Seg_687" s="T50">Tan</ta>
            <ta e="T52" id="Seg_688" s="T51">iː-l</ta>
            <ta e="T53" id="Seg_689" s="T52">men-äǯa</ta>
            <ta e="T54" id="Seg_690" s="T53">sečas</ta>
            <ta e="T55" id="Seg_691" s="T54">mannɨ-pa-n</ta>
            <ta e="T56" id="Seg_692" s="T55">Valʼka</ta>
            <ta e="T57" id="Seg_693" s="T56">čaːʒa-n</ta>
            <ta e="T58" id="Seg_694" s="T57">aj</ta>
            <ta e="T59" id="Seg_695" s="T58">toɣu-lǯa-ttə</ta>
            <ta e="T60" id="Seg_696" s="T59">tʼära-ttə</ta>
            <ta e="T61" id="Seg_697" s="T60">palʼnica</ta>
            <ta e="T62" id="Seg_698" s="T61">čaʒə-t</ta>
            <ta e="T63" id="Seg_699" s="T62">man</ta>
            <ta e="T64" id="Seg_700" s="T63">mannɨ-pa-n</ta>
            <ta e="T65" id="Seg_701" s="T64">täjärbɨ-za-n</ta>
            <ta e="T66" id="Seg_702" s="T65">Aːda</ta>
            <ta e="T67" id="Seg_703" s="T66">men-eǯa</ta>
            <ta e="T68" id="Seg_704" s="T67">Nataša</ta>
            <ta e="T69" id="Seg_705" s="T68">men-eǯa</ta>
            <ta e="T70" id="Seg_706" s="T69">a</ta>
            <ta e="T71" id="Seg_707" s="T70">täb-ɨ-staq</ta>
            <ta e="T72" id="Seg_708" s="T71">ass</ta>
            <ta e="T73" id="Seg_709" s="T72">közə-bba-ɣə</ta>
            <ta e="T74" id="Seg_710" s="T73">man</ta>
            <ta e="T75" id="Seg_711" s="T74">mannɨ-paː-n</ta>
            <ta e="T76" id="Seg_712" s="T75">mannɨ-pa-n</ta>
            <ta e="T77" id="Seg_713" s="T76">qaj-ɣɨ-nä</ta>
            <ta e="T78" id="Seg_714" s="T77">tʼäg-wa-ɣə</ta>
            <ta e="T79" id="Seg_715" s="T78">a</ta>
            <ta e="T80" id="Seg_716" s="T79">täb-ɨ-staɣə</ta>
            <ta e="T81" id="Seg_717" s="T80">jess</ta>
            <ta e="T82" id="Seg_718" s="T81">közi-pba-ɣə</ta>
            <ta e="T83" id="Seg_719" s="T82">a</ta>
            <ta e="T84" id="Seg_720" s="T83">bolʼše</ta>
            <ta e="T85" id="Seg_721" s="T84">kud-ä-m-nä-ss</ta>
            <ta e="T86" id="Seg_722" s="T85">tuno-u</ta>
            <ta e="T87" id="Seg_723" s="T86">no</ta>
            <ta e="T88" id="Seg_724" s="T87">qu-la</ta>
            <ta e="T89" id="Seg_725" s="T88">koːcʼi-n</ta>
            <ta e="T90" id="Seg_726" s="T89">je-za-ttə</ta>
            <ta e="T91" id="Seg_727" s="T90">soːdʼiga-n</ta>
            <ta e="T92" id="Seg_728" s="T91">tʼipbɨ-ppɨ-za-ttə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_729" s="T1">perwɨj</ta>
            <ta e="T3" id="Seg_730" s="T2">maj-qɨn</ta>
            <ta e="T4" id="Seg_731" s="T3">man</ta>
            <ta e="T5" id="Seg_732" s="T4">közi-sɨ-ŋ</ta>
            <ta e="T6" id="Seg_733" s="T5">praznik-ntə</ta>
            <ta e="T7" id="Seg_734" s="T6">tatawa</ta>
            <ta e="T8" id="Seg_735" s="T7">soːdʼiga-ŋ</ta>
            <ta e="T9" id="Seg_736" s="T8">eː-sɨ</ta>
            <ta e="T10" id="Seg_737" s="T9">flag-la</ta>
            <ta e="T11" id="Seg_738" s="T10">tirɨmpɨ-sɨ-tɨn</ta>
            <ta e="T14" id="Seg_739" s="T13">soldat-la</ta>
            <ta e="T15" id="Seg_740" s="T14">koːci-ŋ</ta>
            <ta e="T16" id="Seg_741" s="T15">eː-sɨ-tɨn</ta>
            <ta e="T17" id="Seg_742" s="T16">perwa</ta>
            <ta e="T18" id="Seg_743" s="T17">soldat-la</ta>
            <ta e="T19" id="Seg_744" s="T18">mendɨ-tɨn</ta>
            <ta e="T20" id="Seg_745" s="T19">mendɨ-sɨ-tɨn</ta>
            <ta e="T21" id="Seg_746" s="T20">patom</ta>
            <ta e="T22" id="Seg_747" s="T21">škola-la</ta>
            <ta e="T23" id="Seg_748" s="T22">qum-la</ta>
            <ta e="T24" id="Seg_749" s="T23">wesʼ</ta>
            <ta e="T25" id="Seg_750" s="T24">*toːqo-lǯi-le</ta>
            <ta e="T26" id="Seg_751" s="T25">tat-ɨ-r-nɨ-tɨn</ta>
            <ta e="T27" id="Seg_752" s="T26">tʼärɨ-tɨn</ta>
            <ta e="T29" id="Seg_753" s="T28">čaǯɨ-ntɨ</ta>
            <ta e="T30" id="Seg_754" s="T29">man</ta>
            <ta e="T31" id="Seg_755" s="T30">čaček-ɨ-n</ta>
            <ta e="T32" id="Seg_756" s="T31">nɨ-l-lǯi-ŋ</ta>
            <ta e="T33" id="Seg_757" s="T32">*mantɨ-mbɨ-ntɨ-w</ta>
            <ta e="T34" id="Seg_758" s="T33">Angelina</ta>
            <ta e="T35" id="Seg_759" s="T34">Iwanɨwna</ta>
            <ta e="T36" id="Seg_760" s="T35">čaǯɨ-n</ta>
            <ta e="T37" id="Seg_761" s="T36">man</ta>
            <ta e="T38" id="Seg_762" s="T37">tʼärɨ-ŋ</ta>
            <ta e="T39" id="Seg_763" s="T38">tʼolom</ta>
            <ta e="T40" id="Seg_764" s="T39">Angelina</ta>
            <ta e="T41" id="Seg_765" s="T40">Iwanɨwna</ta>
            <ta e="T42" id="Seg_766" s="T41">praznik-se</ta>
            <ta e="T43" id="Seg_767" s="T42">a</ta>
            <ta e="T44" id="Seg_768" s="T43">täp</ta>
            <ta e="T45" id="Seg_769" s="T44">asa</ta>
            <ta e="T46" id="Seg_770" s="T45">ündɨ-dʼi-t</ta>
            <ta e="T47" id="Seg_771" s="T46">qwan-nɨ-ŋ</ta>
            <ta e="T48" id="Seg_772" s="T47">man</ta>
            <ta e="T49" id="Seg_773" s="T48">paja-ka-nä</ta>
            <ta e="T50" id="Seg_774" s="T49">tʼärɨ-ŋ</ta>
            <ta e="T51" id="Seg_775" s="T50">tan</ta>
            <ta e="T52" id="Seg_776" s="T51">iː-l</ta>
            <ta e="T53" id="Seg_777" s="T52">mendɨ-enǯɨ</ta>
            <ta e="T54" id="Seg_778" s="T53">sičas</ta>
            <ta e="T55" id="Seg_779" s="T54">*mantɨ-mbɨ-ŋ</ta>
            <ta e="T56" id="Seg_780" s="T55">Valʼka</ta>
            <ta e="T57" id="Seg_781" s="T56">čaǯɨ-n</ta>
            <ta e="T58" id="Seg_782" s="T57">aj</ta>
            <ta e="T59" id="Seg_783" s="T58">*toːqo-lǯi-tɨn</ta>
            <ta e="T60" id="Seg_784" s="T59">tʼärɨ-tɨn</ta>
            <ta e="T61" id="Seg_785" s="T60">palʼnica</ta>
            <ta e="T62" id="Seg_786" s="T61">čaǯɨ-ntɨ</ta>
            <ta e="T63" id="Seg_787" s="T62">man</ta>
            <ta e="T64" id="Seg_788" s="T63">*mantɨ-mbɨ-ŋ</ta>
            <ta e="T65" id="Seg_789" s="T64">tärba-sɨ-ŋ</ta>
            <ta e="T66" id="Seg_790" s="T65">Aːda</ta>
            <ta e="T67" id="Seg_791" s="T66">mendɨ-enǯɨ</ta>
            <ta e="T68" id="Seg_792" s="T67">Nataša</ta>
            <ta e="T69" id="Seg_793" s="T68">mendɨ-enǯɨ</ta>
            <ta e="T70" id="Seg_794" s="T69">a</ta>
            <ta e="T71" id="Seg_795" s="T70">täp-ɨ-staɣɨ</ta>
            <ta e="T72" id="Seg_796" s="T71">asa</ta>
            <ta e="T73" id="Seg_797" s="T72">közi-mbɨ-qij</ta>
            <ta e="T74" id="Seg_798" s="T73">man</ta>
            <ta e="T75" id="Seg_799" s="T74">*mantɨ-mbɨ-ŋ</ta>
            <ta e="T76" id="Seg_800" s="T75">*mantɨ-mbɨ-ŋ</ta>
            <ta e="T77" id="Seg_801" s="T76">qaj-qi-näj</ta>
            <ta e="T78" id="Seg_802" s="T77">tʼäkku-nɨ-qij</ta>
            <ta e="T79" id="Seg_803" s="T78">a</ta>
            <ta e="T80" id="Seg_804" s="T79">täp-ɨ-staɣɨ</ta>
            <ta e="T81" id="Seg_805" s="T80">asa</ta>
            <ta e="T82" id="Seg_806" s="T81">közi-mbɨ-qij</ta>
            <ta e="T83" id="Seg_807" s="T82">a</ta>
            <ta e="T84" id="Seg_808" s="T83">bolʼše</ta>
            <ta e="T85" id="Seg_809" s="T84">kud-ɨ-m-näj-s</ta>
            <ta e="T86" id="Seg_810" s="T85">tonu-w</ta>
            <ta e="T87" id="Seg_811" s="T86">nu</ta>
            <ta e="T88" id="Seg_812" s="T87">qum-la</ta>
            <ta e="T89" id="Seg_813" s="T88">koːci-ŋ</ta>
            <ta e="T90" id="Seg_814" s="T89">eː-sɨ-tɨn</ta>
            <ta e="T91" id="Seg_815" s="T90">soːdʼiga-ŋ</ta>
            <ta e="T92" id="Seg_816" s="T91">tʼipbɨ-mbɨ-sɨ-tɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_817" s="T1">first</ta>
            <ta e="T3" id="Seg_818" s="T2">May-LOC</ta>
            <ta e="T4" id="Seg_819" s="T3">I.NOM</ta>
            <ta e="T5" id="Seg_820" s="T4">go-PST-1SG.S</ta>
            <ta e="T6" id="Seg_821" s="T5">holiday-ILL</ta>
            <ta e="T7" id="Seg_822" s="T6">to.such.extent</ta>
            <ta e="T8" id="Seg_823" s="T7">good-ADVZ</ta>
            <ta e="T9" id="Seg_824" s="T8">be-PST.[3SG.S]</ta>
            <ta e="T10" id="Seg_825" s="T9">flag-PL.[NOM]</ta>
            <ta e="T11" id="Seg_826" s="T10">fill-PST-3PL</ta>
            <ta e="T14" id="Seg_827" s="T13">soldier-PL.[NOM]</ta>
            <ta e="T15" id="Seg_828" s="T14">much-ADVZ</ta>
            <ta e="T16" id="Seg_829" s="T15">be-PST-3PL</ta>
            <ta e="T17" id="Seg_830" s="T16">first</ta>
            <ta e="T18" id="Seg_831" s="T17">soldier-PL.[NOM]</ta>
            <ta e="T19" id="Seg_832" s="T18">pass-3PL</ta>
            <ta e="T20" id="Seg_833" s="T19">pass-PST-3PL</ta>
            <ta e="T21" id="Seg_834" s="T20">then</ta>
            <ta e="T22" id="Seg_835" s="T21">school-PL.[NOM]</ta>
            <ta e="T23" id="Seg_836" s="T22">human.being-PL.[NOM]</ta>
            <ta e="T24" id="Seg_837" s="T23">all</ta>
            <ta e="T25" id="Seg_838" s="T24">read-PFV-CVB</ta>
            <ta e="T26" id="Seg_839" s="T25">bring-EP-FRQ-CO-3PL</ta>
            <ta e="T27" id="Seg_840" s="T26">say-3PL</ta>
            <ta e="T29" id="Seg_841" s="T28">go-INFER.[3SG.S]</ta>
            <ta e="T30" id="Seg_842" s="T29">I.NOM</ta>
            <ta e="T31" id="Seg_843" s="T30">near-EP-ADV.LOC</ta>
            <ta e="T32" id="Seg_844" s="T31">stand-INCH-PFV-1SG.S</ta>
            <ta e="T33" id="Seg_845" s="T32">look-DUR-INFER-1SG.O</ta>
            <ta e="T34" id="Seg_846" s="T33">Angelina.[NOM]</ta>
            <ta e="T35" id="Seg_847" s="T34">Ivanovna.[NOM]</ta>
            <ta e="T36" id="Seg_848" s="T35">go-3SG.S</ta>
            <ta e="T37" id="Seg_849" s="T36">I.NOM</ta>
            <ta e="T38" id="Seg_850" s="T37">say-1SG.S</ta>
            <ta e="T39" id="Seg_851" s="T38">hello</ta>
            <ta e="T40" id="Seg_852" s="T39">Angelina.[NOM]</ta>
            <ta e="T41" id="Seg_853" s="T40">Ivanovna.[NOM]</ta>
            <ta e="T42" id="Seg_854" s="T41">holiday-COM</ta>
            <ta e="T43" id="Seg_855" s="T42">and</ta>
            <ta e="T44" id="Seg_856" s="T43">(s)he.[NOM]</ta>
            <ta e="T45" id="Seg_857" s="T44">NEG</ta>
            <ta e="T46" id="Seg_858" s="T45">hear-DRV-3SG.O</ta>
            <ta e="T47" id="Seg_859" s="T46">leave-CO-1SG.S</ta>
            <ta e="T48" id="Seg_860" s="T47">I.NOM</ta>
            <ta e="T49" id="Seg_861" s="T48">old.woman-DIM-ALL</ta>
            <ta e="T50" id="Seg_862" s="T49">say-1SG.S</ta>
            <ta e="T51" id="Seg_863" s="T50">you.SG.GEN</ta>
            <ta e="T52" id="Seg_864" s="T51">son.[NOM]-2SG</ta>
            <ta e="T53" id="Seg_865" s="T52">pass-FUT.[3SG.S]</ta>
            <ta e="T54" id="Seg_866" s="T53">now</ta>
            <ta e="T55" id="Seg_867" s="T54">look-DUR-1SG.S</ta>
            <ta e="T56" id="Seg_868" s="T55">Valka.[NOM]</ta>
            <ta e="T57" id="Seg_869" s="T56">go-3SG.S</ta>
            <ta e="T58" id="Seg_870" s="T57">again</ta>
            <ta e="T59" id="Seg_871" s="T58">read-PFV-3PL</ta>
            <ta e="T60" id="Seg_872" s="T59">say-3PL</ta>
            <ta e="T61" id="Seg_873" s="T60">hospital.[NOM]</ta>
            <ta e="T62" id="Seg_874" s="T61">go-INFER.[3SG.S]</ta>
            <ta e="T63" id="Seg_875" s="T62">I.NOM</ta>
            <ta e="T64" id="Seg_876" s="T63">look-DUR-1SG.S</ta>
            <ta e="T65" id="Seg_877" s="T64">think-PST-1SG.S</ta>
            <ta e="T66" id="Seg_878" s="T65">Ada.[NOM]</ta>
            <ta e="T67" id="Seg_879" s="T66">pass-FUT.[3SG.S]</ta>
            <ta e="T68" id="Seg_880" s="T67">Natasha.[NOM]</ta>
            <ta e="T69" id="Seg_881" s="T68">pass-FUT.[3SG.S]</ta>
            <ta e="T70" id="Seg_882" s="T69">and</ta>
            <ta e="T71" id="Seg_883" s="T70">(s)he-EP-DU.[NOM]</ta>
            <ta e="T72" id="Seg_884" s="T71">NEG</ta>
            <ta e="T73" id="Seg_885" s="T72">go-PST.NAR-3DU.S</ta>
            <ta e="T74" id="Seg_886" s="T73">I.NOM</ta>
            <ta e="T75" id="Seg_887" s="T74">look-DUR-1SG.S</ta>
            <ta e="T76" id="Seg_888" s="T75">look-DUR-1SG.S</ta>
            <ta e="T77" id="Seg_889" s="T76">what-DU.[NOM]-EMPH</ta>
            <ta e="T78" id="Seg_890" s="T77">NEG.EX-CO-3DU.S</ta>
            <ta e="T79" id="Seg_891" s="T78">and</ta>
            <ta e="T80" id="Seg_892" s="T79">(s)he-EP-DU.[NOM]</ta>
            <ta e="T81" id="Seg_893" s="T80">NEG</ta>
            <ta e="T82" id="Seg_894" s="T81">go-PST.NAR-3DU.S</ta>
            <ta e="T83" id="Seg_895" s="T82">and</ta>
            <ta e="T84" id="Seg_896" s="T83">more</ta>
            <ta e="T85" id="Seg_897" s="T84">who-EP-ACC-EMPH-NEG</ta>
            <ta e="T86" id="Seg_898" s="T85">know-1SG.O</ta>
            <ta e="T87" id="Seg_899" s="T86">now</ta>
            <ta e="T88" id="Seg_900" s="T87">human.being-PL.[NOM]</ta>
            <ta e="T89" id="Seg_901" s="T88">much-ADVZ</ta>
            <ta e="T90" id="Seg_902" s="T89">be-PST-3PL</ta>
            <ta e="T91" id="Seg_903" s="T90">good-ADVZ</ta>
            <ta e="T92" id="Seg_904" s="T91">dress-RES-PST-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_905" s="T1">первый</ta>
            <ta e="T3" id="Seg_906" s="T2">май-LOC</ta>
            <ta e="T4" id="Seg_907" s="T3">я.NOM</ta>
            <ta e="T5" id="Seg_908" s="T4">идти-PST-1SG.S</ta>
            <ta e="T6" id="Seg_909" s="T5">праздник-ILL</ta>
            <ta e="T7" id="Seg_910" s="T6">до.того</ta>
            <ta e="T8" id="Seg_911" s="T7">хороший-ADVZ</ta>
            <ta e="T9" id="Seg_912" s="T8">быть-PST.[3SG.S]</ta>
            <ta e="T10" id="Seg_913" s="T9">флаг-PL.[NOM]</ta>
            <ta e="T11" id="Seg_914" s="T10">наполниться-PST-3PL</ta>
            <ta e="T14" id="Seg_915" s="T13">солдат-PL.[NOM]</ta>
            <ta e="T15" id="Seg_916" s="T14">много-ADVZ</ta>
            <ta e="T16" id="Seg_917" s="T15">быть-PST-3PL</ta>
            <ta e="T17" id="Seg_918" s="T16">сперва</ta>
            <ta e="T18" id="Seg_919" s="T17">солдат-PL.[NOM]</ta>
            <ta e="T19" id="Seg_920" s="T18">пройти-3PL</ta>
            <ta e="T20" id="Seg_921" s="T19">пройти-PST-3PL</ta>
            <ta e="T21" id="Seg_922" s="T20">потом</ta>
            <ta e="T22" id="Seg_923" s="T21">школа-PL.[NOM]</ta>
            <ta e="T23" id="Seg_924" s="T22">человек-PL.[NOM]</ta>
            <ta e="T24" id="Seg_925" s="T23">все</ta>
            <ta e="T25" id="Seg_926" s="T24">читать-PFV-CVB</ta>
            <ta e="T26" id="Seg_927" s="T25">принести-EP-FRQ-CO-3PL</ta>
            <ta e="T27" id="Seg_928" s="T26">сказать-3PL</ta>
            <ta e="T29" id="Seg_929" s="T28">ходить-INFER.[3SG.S]</ta>
            <ta e="T30" id="Seg_930" s="T29">я.NOM</ta>
            <ta e="T31" id="Seg_931" s="T30">близко-EP-ADV.LOC</ta>
            <ta e="T32" id="Seg_932" s="T31">стоять-INCH-PFV-1SG.S</ta>
            <ta e="T33" id="Seg_933" s="T32">посмотреть-DUR-INFER-1SG.O</ta>
            <ta e="T34" id="Seg_934" s="T33">Ангелина.[NOM]</ta>
            <ta e="T35" id="Seg_935" s="T34">Ивановна.[NOM]</ta>
            <ta e="T36" id="Seg_936" s="T35">ходить-3SG.S</ta>
            <ta e="T37" id="Seg_937" s="T36">я.NOM</ta>
            <ta e="T38" id="Seg_938" s="T37">сказать-1SG.S</ta>
            <ta e="T39" id="Seg_939" s="T38">здравствуй</ta>
            <ta e="T40" id="Seg_940" s="T39">Ангелина.[NOM]</ta>
            <ta e="T41" id="Seg_941" s="T40">Ивановна.[NOM]</ta>
            <ta e="T42" id="Seg_942" s="T41">праздник-COM</ta>
            <ta e="T43" id="Seg_943" s="T42">а</ta>
            <ta e="T44" id="Seg_944" s="T43">он(а).[NOM]</ta>
            <ta e="T45" id="Seg_945" s="T44">NEG</ta>
            <ta e="T46" id="Seg_946" s="T45">услышать-DRV-3SG.O</ta>
            <ta e="T47" id="Seg_947" s="T46">отправиться-CO-1SG.S</ta>
            <ta e="T48" id="Seg_948" s="T47">я.NOM</ta>
            <ta e="T49" id="Seg_949" s="T48">старуха-DIM-ALL</ta>
            <ta e="T50" id="Seg_950" s="T49">сказать-1SG.S</ta>
            <ta e="T51" id="Seg_951" s="T50">ты.GEN</ta>
            <ta e="T52" id="Seg_952" s="T51">сын.[NOM]-2SG</ta>
            <ta e="T53" id="Seg_953" s="T52">пройти-FUT.[3SG.S]</ta>
            <ta e="T54" id="Seg_954" s="T53">сейчас</ta>
            <ta e="T55" id="Seg_955" s="T54">посмотреть-DUR-1SG.S</ta>
            <ta e="T56" id="Seg_956" s="T55">Валька.[NOM]</ta>
            <ta e="T57" id="Seg_957" s="T56">ходить-3SG.S</ta>
            <ta e="T58" id="Seg_958" s="T57">опять</ta>
            <ta e="T59" id="Seg_959" s="T58">читать-PFV-3PL</ta>
            <ta e="T60" id="Seg_960" s="T59">сказать-3PL</ta>
            <ta e="T61" id="Seg_961" s="T60">больница.[NOM]</ta>
            <ta e="T62" id="Seg_962" s="T61">ходить-INFER.[3SG.S]</ta>
            <ta e="T63" id="Seg_963" s="T62">я.NOM</ta>
            <ta e="T64" id="Seg_964" s="T63">посмотреть-DUR-1SG.S</ta>
            <ta e="T65" id="Seg_965" s="T64">думать-PST-1SG.S</ta>
            <ta e="T66" id="Seg_966" s="T65">Ада.[NOM]</ta>
            <ta e="T67" id="Seg_967" s="T66">пройти-FUT.[3SG.S]</ta>
            <ta e="T68" id="Seg_968" s="T67">Наташа.[NOM]</ta>
            <ta e="T69" id="Seg_969" s="T68">пройти-FUT.[3SG.S]</ta>
            <ta e="T70" id="Seg_970" s="T69">а</ta>
            <ta e="T71" id="Seg_971" s="T70">он(а)-EP-DU.[NOM]</ta>
            <ta e="T72" id="Seg_972" s="T71">NEG</ta>
            <ta e="T73" id="Seg_973" s="T72">идти-PST.NAR-3DU.S</ta>
            <ta e="T74" id="Seg_974" s="T73">я.NOM</ta>
            <ta e="T75" id="Seg_975" s="T74">посмотреть-DUR-1SG.S</ta>
            <ta e="T76" id="Seg_976" s="T75">посмотреть-DUR-1SG.S</ta>
            <ta e="T77" id="Seg_977" s="T76">что-DU.[NOM]-EMPH</ta>
            <ta e="T78" id="Seg_978" s="T77">NEG.EX-CO-3DU.S</ta>
            <ta e="T79" id="Seg_979" s="T78">а</ta>
            <ta e="T80" id="Seg_980" s="T79">он(а)-EP-DU.[NOM]</ta>
            <ta e="T81" id="Seg_981" s="T80">NEG</ta>
            <ta e="T82" id="Seg_982" s="T81">идти-PST.NAR-3DU.S</ta>
            <ta e="T83" id="Seg_983" s="T82">а</ta>
            <ta e="T84" id="Seg_984" s="T83">больше</ta>
            <ta e="T85" id="Seg_985" s="T84">кто-EP-ACC-EMPH-NEG</ta>
            <ta e="T86" id="Seg_986" s="T85">знать-1SG.O</ta>
            <ta e="T87" id="Seg_987" s="T86">ну</ta>
            <ta e="T88" id="Seg_988" s="T87">человек-PL.[NOM]</ta>
            <ta e="T89" id="Seg_989" s="T88">много-ADVZ</ta>
            <ta e="T90" id="Seg_990" s="T89">быть-PST-3PL</ta>
            <ta e="T91" id="Seg_991" s="T90">хороший-ADVZ</ta>
            <ta e="T92" id="Seg_992" s="T91">одеться-RES-PST-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_993" s="T1">adj</ta>
            <ta e="T3" id="Seg_994" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_995" s="T3">pers</ta>
            <ta e="T5" id="Seg_996" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_997" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_998" s="T6">adv</ta>
            <ta e="T8" id="Seg_999" s="T7">adj-adj&gt;adv</ta>
            <ta e="T9" id="Seg_1000" s="T8">v-v:tense.[v:pn]</ta>
            <ta e="T10" id="Seg_1001" s="T9">n-n:num.[n:case]</ta>
            <ta e="T11" id="Seg_1002" s="T10">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_1003" s="T13">n-n:num.[n:case]</ta>
            <ta e="T15" id="Seg_1004" s="T14">quant-quant&gt;adv</ta>
            <ta e="T16" id="Seg_1005" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_1006" s="T16">adv</ta>
            <ta e="T18" id="Seg_1007" s="T17">n-n:num.[n:case]</ta>
            <ta e="T19" id="Seg_1008" s="T18">v-v:pn</ta>
            <ta e="T20" id="Seg_1009" s="T19">v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_1010" s="T20">adv</ta>
            <ta e="T22" id="Seg_1011" s="T21">n-n:num.[n:case]</ta>
            <ta e="T23" id="Seg_1012" s="T22">n-n:num.[n:case]</ta>
            <ta e="T24" id="Seg_1013" s="T23">quant</ta>
            <ta e="T25" id="Seg_1014" s="T24">v-v&gt;v-v&gt;adv</ta>
            <ta e="T26" id="Seg_1015" s="T25">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T27" id="Seg_1016" s="T26">v-v:pn</ta>
            <ta e="T29" id="Seg_1017" s="T28">v-v:mood.[v:pn]</ta>
            <ta e="T30" id="Seg_1018" s="T29">pers</ta>
            <ta e="T31" id="Seg_1019" s="T30">adv-n:ins-adv:case</ta>
            <ta e="T32" id="Seg_1020" s="T31">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T33" id="Seg_1021" s="T32">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T34" id="Seg_1022" s="T33">nprop.[n:case]</ta>
            <ta e="T35" id="Seg_1023" s="T34">nprop.[n:case]</ta>
            <ta e="T36" id="Seg_1024" s="T35">v-v:pn</ta>
            <ta e="T37" id="Seg_1025" s="T36">pers</ta>
            <ta e="T38" id="Seg_1026" s="T37">v-v:pn</ta>
            <ta e="T39" id="Seg_1027" s="T38">interj</ta>
            <ta e="T40" id="Seg_1028" s="T39">nprop.[n:case]</ta>
            <ta e="T41" id="Seg_1029" s="T40">nprop.[n:case]</ta>
            <ta e="T42" id="Seg_1030" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_1031" s="T42">conj</ta>
            <ta e="T44" id="Seg_1032" s="T43">pers.[n:case]</ta>
            <ta e="T45" id="Seg_1033" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_1034" s="T45">v-v&gt;v-v:pn</ta>
            <ta e="T47" id="Seg_1035" s="T46">v-v:ins-v:pn</ta>
            <ta e="T48" id="Seg_1036" s="T47">pers</ta>
            <ta e="T49" id="Seg_1037" s="T48">n-n&gt;n-n:case</ta>
            <ta e="T50" id="Seg_1038" s="T49">v-v:pn</ta>
            <ta e="T51" id="Seg_1039" s="T50">pers</ta>
            <ta e="T52" id="Seg_1040" s="T51">n.[n:case]-n:poss</ta>
            <ta e="T53" id="Seg_1041" s="T52">v-v:tense.[v:pn]</ta>
            <ta e="T54" id="Seg_1042" s="T53">adv</ta>
            <ta e="T55" id="Seg_1043" s="T54">v-v&gt;v-v:pn</ta>
            <ta e="T56" id="Seg_1044" s="T55">nprop.[n:case]</ta>
            <ta e="T57" id="Seg_1045" s="T56">v-v:pn</ta>
            <ta e="T58" id="Seg_1046" s="T57">adv</ta>
            <ta e="T59" id="Seg_1047" s="T58">v-v&gt;v-v:pn</ta>
            <ta e="T60" id="Seg_1048" s="T59">v-v:pn</ta>
            <ta e="T61" id="Seg_1049" s="T60">n.[n:case]</ta>
            <ta e="T62" id="Seg_1050" s="T61">v-v:mood.[v:pn]</ta>
            <ta e="T63" id="Seg_1051" s="T62">pers</ta>
            <ta e="T64" id="Seg_1052" s="T63">v-v&gt;v-v:pn</ta>
            <ta e="T65" id="Seg_1053" s="T64">v-v:tense-v:pn</ta>
            <ta e="T66" id="Seg_1054" s="T65">nprop.[n:case]</ta>
            <ta e="T67" id="Seg_1055" s="T66">v-v:tense.[v:pn]</ta>
            <ta e="T68" id="Seg_1056" s="T67">nprop.[n:case]</ta>
            <ta e="T69" id="Seg_1057" s="T68">v-v:tense.[v:pn]</ta>
            <ta e="T70" id="Seg_1058" s="T69">conj</ta>
            <ta e="T71" id="Seg_1059" s="T70">pers-n:ins-n:num.[n:case]</ta>
            <ta e="T72" id="Seg_1060" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_1061" s="T72">v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_1062" s="T73">pers</ta>
            <ta e="T75" id="Seg_1063" s="T74">v-v&gt;v-v:pn</ta>
            <ta e="T76" id="Seg_1064" s="T75">v-v&gt;v-v:pn</ta>
            <ta e="T77" id="Seg_1065" s="T76">interrog-n:num.[n:case]-clit</ta>
            <ta e="T78" id="Seg_1066" s="T77">v-v:ins-v:pn</ta>
            <ta e="T79" id="Seg_1067" s="T78">conj</ta>
            <ta e="T80" id="Seg_1068" s="T79">pers-n:ins-n:num.[n:case]</ta>
            <ta e="T81" id="Seg_1069" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_1070" s="T81">v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_1071" s="T82">conj</ta>
            <ta e="T84" id="Seg_1072" s="T83">adv</ta>
            <ta e="T85" id="Seg_1073" s="T84">interrog-n:ins-n:case-clit-clit</ta>
            <ta e="T86" id="Seg_1074" s="T85">v-v:pn</ta>
            <ta e="T87" id="Seg_1075" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_1076" s="T87">n-n:num.[n:case]</ta>
            <ta e="T89" id="Seg_1077" s="T88">quant-quant&gt;adv</ta>
            <ta e="T90" id="Seg_1078" s="T89">v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_1079" s="T90">adj-adj&gt;adv</ta>
            <ta e="T92" id="Seg_1080" s="T91">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1081" s="T1">adj</ta>
            <ta e="T3" id="Seg_1082" s="T2">n</ta>
            <ta e="T4" id="Seg_1083" s="T3">pers</ta>
            <ta e="T5" id="Seg_1084" s="T4">v</ta>
            <ta e="T6" id="Seg_1085" s="T5">n</ta>
            <ta e="T7" id="Seg_1086" s="T6">adv</ta>
            <ta e="T8" id="Seg_1087" s="T7">adv</ta>
            <ta e="T9" id="Seg_1088" s="T8">v</ta>
            <ta e="T10" id="Seg_1089" s="T9">n</ta>
            <ta e="T11" id="Seg_1090" s="T10">v</ta>
            <ta e="T14" id="Seg_1091" s="T13">n</ta>
            <ta e="T15" id="Seg_1092" s="T14">quant</ta>
            <ta e="T16" id="Seg_1093" s="T15">v</ta>
            <ta e="T17" id="Seg_1094" s="T16">adv</ta>
            <ta e="T18" id="Seg_1095" s="T17">n</ta>
            <ta e="T19" id="Seg_1096" s="T18">v</ta>
            <ta e="T20" id="Seg_1097" s="T19">v</ta>
            <ta e="T21" id="Seg_1098" s="T20">adv</ta>
            <ta e="T22" id="Seg_1099" s="T21">n</ta>
            <ta e="T23" id="Seg_1100" s="T22">n</ta>
            <ta e="T24" id="Seg_1101" s="T23">quant</ta>
            <ta e="T25" id="Seg_1102" s="T24">adv</ta>
            <ta e="T26" id="Seg_1103" s="T25">v</ta>
            <ta e="T27" id="Seg_1104" s="T26">v</ta>
            <ta e="T29" id="Seg_1105" s="T28">v</ta>
            <ta e="T30" id="Seg_1106" s="T29">pers</ta>
            <ta e="T31" id="Seg_1107" s="T30">adv</ta>
            <ta e="T32" id="Seg_1108" s="T31">v</ta>
            <ta e="T33" id="Seg_1109" s="T32">v</ta>
            <ta e="T34" id="Seg_1110" s="T33">nprop</ta>
            <ta e="T35" id="Seg_1111" s="T34">nprop</ta>
            <ta e="T36" id="Seg_1112" s="T35">v</ta>
            <ta e="T37" id="Seg_1113" s="T36">pers</ta>
            <ta e="T38" id="Seg_1114" s="T37">v</ta>
            <ta e="T39" id="Seg_1115" s="T38">interj</ta>
            <ta e="T40" id="Seg_1116" s="T39">nprop</ta>
            <ta e="T41" id="Seg_1117" s="T40">nprop</ta>
            <ta e="T42" id="Seg_1118" s="T41">n</ta>
            <ta e="T43" id="Seg_1119" s="T42">conj</ta>
            <ta e="T44" id="Seg_1120" s="T43">pers</ta>
            <ta e="T45" id="Seg_1121" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_1122" s="T45">v</ta>
            <ta e="T47" id="Seg_1123" s="T46">v</ta>
            <ta e="T48" id="Seg_1124" s="T47">pers</ta>
            <ta e="T49" id="Seg_1125" s="T48">n</ta>
            <ta e="T50" id="Seg_1126" s="T49">v</ta>
            <ta e="T51" id="Seg_1127" s="T50">pers</ta>
            <ta e="T52" id="Seg_1128" s="T51">n</ta>
            <ta e="T53" id="Seg_1129" s="T52">v</ta>
            <ta e="T54" id="Seg_1130" s="T53">adv</ta>
            <ta e="T55" id="Seg_1131" s="T54">v</ta>
            <ta e="T56" id="Seg_1132" s="T55">nprop</ta>
            <ta e="T57" id="Seg_1133" s="T56">v</ta>
            <ta e="T58" id="Seg_1134" s="T57">adv</ta>
            <ta e="T59" id="Seg_1135" s="T58">v</ta>
            <ta e="T60" id="Seg_1136" s="T59">v</ta>
            <ta e="T61" id="Seg_1137" s="T60">n</ta>
            <ta e="T62" id="Seg_1138" s="T61">v</ta>
            <ta e="T63" id="Seg_1139" s="T62">pers</ta>
            <ta e="T64" id="Seg_1140" s="T63">v</ta>
            <ta e="T65" id="Seg_1141" s="T64">v</ta>
            <ta e="T66" id="Seg_1142" s="T65">nprop</ta>
            <ta e="T67" id="Seg_1143" s="T66">v</ta>
            <ta e="T68" id="Seg_1144" s="T67">nprop</ta>
            <ta e="T69" id="Seg_1145" s="T68">v</ta>
            <ta e="T70" id="Seg_1146" s="T69">conj</ta>
            <ta e="T71" id="Seg_1147" s="T70">pers</ta>
            <ta e="T72" id="Seg_1148" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_1149" s="T72">v</ta>
            <ta e="T74" id="Seg_1150" s="T73">pers</ta>
            <ta e="T75" id="Seg_1151" s="T74">v</ta>
            <ta e="T76" id="Seg_1152" s="T75">v</ta>
            <ta e="T77" id="Seg_1153" s="T76">interrog</ta>
            <ta e="T78" id="Seg_1154" s="T77">v</ta>
            <ta e="T79" id="Seg_1155" s="T78">conj</ta>
            <ta e="T80" id="Seg_1156" s="T79">pers</ta>
            <ta e="T81" id="Seg_1157" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_1158" s="T81">v</ta>
            <ta e="T83" id="Seg_1159" s="T82">conj</ta>
            <ta e="T84" id="Seg_1160" s="T83">adv</ta>
            <ta e="T85" id="Seg_1161" s="T84">pro</ta>
            <ta e="T86" id="Seg_1162" s="T85">v</ta>
            <ta e="T87" id="Seg_1163" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_1164" s="T87">n</ta>
            <ta e="T89" id="Seg_1165" s="T88">adv</ta>
            <ta e="T90" id="Seg_1166" s="T89">v</ta>
            <ta e="T91" id="Seg_1167" s="T90">adv</ta>
            <ta e="T92" id="Seg_1168" s="T91">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_1169" s="T2">np:Time</ta>
            <ta e="T4" id="Seg_1170" s="T3">pro.h:A</ta>
            <ta e="T6" id="Seg_1171" s="T5">np:G</ta>
            <ta e="T9" id="Seg_1172" s="T8">0.3:Th</ta>
            <ta e="T10" id="Seg_1173" s="T9">np:Th</ta>
            <ta e="T14" id="Seg_1174" s="T13">np.h:Th</ta>
            <ta e="T17" id="Seg_1175" s="T16">adv:Time</ta>
            <ta e="T18" id="Seg_1176" s="T17">np.h:Th</ta>
            <ta e="T21" id="Seg_1177" s="T20">adv:Time</ta>
            <ta e="T23" id="Seg_1178" s="T22">np.h:A</ta>
            <ta e="T27" id="Seg_1179" s="T26">0.3.h:A</ta>
            <ta e="T28" id="Seg_1180" s="T27">np.h:A</ta>
            <ta e="T30" id="Seg_1181" s="T29">pro.h:A</ta>
            <ta e="T31" id="Seg_1182" s="T30">adv:L</ta>
            <ta e="T33" id="Seg_1183" s="T32">0.1.h:A</ta>
            <ta e="T34" id="Seg_1184" s="T33">np.h:A</ta>
            <ta e="T37" id="Seg_1185" s="T36">pro.h:A</ta>
            <ta e="T44" id="Seg_1186" s="T43">pro.h:E</ta>
            <ta e="T47" id="Seg_1187" s="T46">0.3.h:A</ta>
            <ta e="T48" id="Seg_1188" s="T47">pro.h:A</ta>
            <ta e="T49" id="Seg_1189" s="T48">np.h:R</ta>
            <ta e="T51" id="Seg_1190" s="T50">pro.h:Poss</ta>
            <ta e="T52" id="Seg_1191" s="T51">np.h:A</ta>
            <ta e="T54" id="Seg_1192" s="T53">adv:Time</ta>
            <ta e="T55" id="Seg_1193" s="T54">0.1.h:A</ta>
            <ta e="T56" id="Seg_1194" s="T55">np.h:A</ta>
            <ta e="T59" id="Seg_1195" s="T58">0.3.h:A</ta>
            <ta e="T60" id="Seg_1196" s="T59">0.3.h:A</ta>
            <ta e="T61" id="Seg_1197" s="T60">np.h:A</ta>
            <ta e="T63" id="Seg_1198" s="T62">pro.h:A</ta>
            <ta e="T65" id="Seg_1199" s="T64">0.1.h:E</ta>
            <ta e="T66" id="Seg_1200" s="T65">np.h:A</ta>
            <ta e="T68" id="Seg_1201" s="T67">np.h:A</ta>
            <ta e="T71" id="Seg_1202" s="T70">pro.h:A</ta>
            <ta e="T74" id="Seg_1203" s="T73">pro.h:A</ta>
            <ta e="T76" id="Seg_1204" s="T75">0.1.h:A</ta>
            <ta e="T77" id="Seg_1205" s="T76">pro.h:Th</ta>
            <ta e="T80" id="Seg_1206" s="T79">pro.h:A</ta>
            <ta e="T85" id="Seg_1207" s="T84">pro.h:Th</ta>
            <ta e="T86" id="Seg_1208" s="T85">0.1.h:E</ta>
            <ta e="T88" id="Seg_1209" s="T87">np.h:Th</ta>
            <ta e="T92" id="Seg_1210" s="T91">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_1211" s="T3">pro.h:S</ta>
            <ta e="T5" id="Seg_1212" s="T4">v:pred</ta>
            <ta e="T9" id="Seg_1213" s="T8">0.3:S v:pred</ta>
            <ta e="T10" id="Seg_1214" s="T9">np:S</ta>
            <ta e="T11" id="Seg_1215" s="T10">v:pred</ta>
            <ta e="T14" id="Seg_1216" s="T13">np.h:S</ta>
            <ta e="T16" id="Seg_1217" s="T15">v:pred</ta>
            <ta e="T18" id="Seg_1218" s="T17">np.h:S</ta>
            <ta e="T19" id="Seg_1219" s="T18">v:pred</ta>
            <ta e="T20" id="Seg_1220" s="T19">v:pred</ta>
            <ta e="T23" id="Seg_1221" s="T22">np.h:S</ta>
            <ta e="T25" id="Seg_1222" s="T24">s:temp</ta>
            <ta e="T26" id="Seg_1223" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_1224" s="T26">0.3.h:S v:pred</ta>
            <ta e="T28" id="Seg_1225" s="T27">np.h:S</ta>
            <ta e="T29" id="Seg_1226" s="T28">v:pred</ta>
            <ta e="T30" id="Seg_1227" s="T29">pro.h:S</ta>
            <ta e="T32" id="Seg_1228" s="T31">v:pred</ta>
            <ta e="T33" id="Seg_1229" s="T32">0.1.h:S v:pred</ta>
            <ta e="T34" id="Seg_1230" s="T33">np.h:S</ta>
            <ta e="T36" id="Seg_1231" s="T35">v:pred</ta>
            <ta e="T37" id="Seg_1232" s="T36">pro.h:S</ta>
            <ta e="T38" id="Seg_1233" s="T37">v:pred</ta>
            <ta e="T44" id="Seg_1234" s="T43">pro.h:S</ta>
            <ta e="T46" id="Seg_1235" s="T45">v:pred</ta>
            <ta e="T47" id="Seg_1236" s="T46">0.3.h:S v:pred</ta>
            <ta e="T48" id="Seg_1237" s="T47">pro.h:S</ta>
            <ta e="T50" id="Seg_1238" s="T49">v:pred</ta>
            <ta e="T52" id="Seg_1239" s="T51">np.h:S</ta>
            <ta e="T53" id="Seg_1240" s="T52">v:pred</ta>
            <ta e="T55" id="Seg_1241" s="T54">0.1.h:S v:pred</ta>
            <ta e="T56" id="Seg_1242" s="T55">np.h:S</ta>
            <ta e="T57" id="Seg_1243" s="T56">v:pred</ta>
            <ta e="T59" id="Seg_1244" s="T58">0.3.h:S v:pred</ta>
            <ta e="T60" id="Seg_1245" s="T59">0.3.h:S v:pred</ta>
            <ta e="T61" id="Seg_1246" s="T60">np.h:S</ta>
            <ta e="T62" id="Seg_1247" s="T61">v:pred</ta>
            <ta e="T63" id="Seg_1248" s="T62">pro.h:S</ta>
            <ta e="T64" id="Seg_1249" s="T63">v:pred</ta>
            <ta e="T65" id="Seg_1250" s="T64">0.1.h:S v:pred</ta>
            <ta e="T66" id="Seg_1251" s="T65">np.h:S</ta>
            <ta e="T67" id="Seg_1252" s="T66">v:pred</ta>
            <ta e="T68" id="Seg_1253" s="T67">np.h:S</ta>
            <ta e="T69" id="Seg_1254" s="T68">v:pred</ta>
            <ta e="T71" id="Seg_1255" s="T70">pro.h:S</ta>
            <ta e="T73" id="Seg_1256" s="T72">v:pred</ta>
            <ta e="T74" id="Seg_1257" s="T73">pro.h:S</ta>
            <ta e="T75" id="Seg_1258" s="T74">v:pred</ta>
            <ta e="T76" id="Seg_1259" s="T75">0.1.h:S v:pred</ta>
            <ta e="T77" id="Seg_1260" s="T76">pro.h:S</ta>
            <ta e="T78" id="Seg_1261" s="T77">v:pred</ta>
            <ta e="T80" id="Seg_1262" s="T79">pro.h:S</ta>
            <ta e="T82" id="Seg_1263" s="T81">v:pred</ta>
            <ta e="T85" id="Seg_1264" s="T84">pro.h:O</ta>
            <ta e="T86" id="Seg_1265" s="T85">0.1.h:S v:pred</ta>
            <ta e="T88" id="Seg_1266" s="T87">np.h:S</ta>
            <ta e="T90" id="Seg_1267" s="T89">v:pred</ta>
            <ta e="T92" id="Seg_1268" s="T91">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_1269" s="T1">RUS:cult</ta>
            <ta e="T3" id="Seg_1270" s="T2">RUS:cult</ta>
            <ta e="T6" id="Seg_1271" s="T5">RUS:cult</ta>
            <ta e="T7" id="Seg_1272" s="T6">RUS:core</ta>
            <ta e="T10" id="Seg_1273" s="T9">RUS:cult</ta>
            <ta e="T14" id="Seg_1274" s="T13">RUS:cult</ta>
            <ta e="T17" id="Seg_1275" s="T16">RUS:core</ta>
            <ta e="T18" id="Seg_1276" s="T17">RUS:cult</ta>
            <ta e="T21" id="Seg_1277" s="T20">RUS:disc</ta>
            <ta e="T22" id="Seg_1278" s="T21">RUS:cult</ta>
            <ta e="T24" id="Seg_1279" s="T23">RUS:core</ta>
            <ta e="T42" id="Seg_1280" s="T41">RUS:cult</ta>
            <ta e="T43" id="Seg_1281" s="T42">RUS:gram</ta>
            <ta e="T54" id="Seg_1282" s="T53">RUS:core</ta>
            <ta e="T61" id="Seg_1283" s="T60">RUS:cult</ta>
            <ta e="T70" id="Seg_1284" s="T69">RUS:gram</ta>
            <ta e="T79" id="Seg_1285" s="T78">RUS:gram</ta>
            <ta e="T83" id="Seg_1286" s="T82">RUS:gram</ta>
            <ta e="T84" id="Seg_1287" s="T83">RUS:core</ta>
            <ta e="T87" id="Seg_1288" s="T86">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_1289" s="T1">On the first of May I went to a festival.</ta>
            <ta e="T9" id="Seg_1290" s="T6">It was so good.</ta>
            <ta e="T13" id="Seg_1291" s="T9">There were many different flags.</ta>
            <ta e="T16" id="Seg_1292" s="T13">There were many soldiers.</ta>
            <ta e="T20" id="Seg_1293" s="T16">First the soldiers passed.</ta>
            <ta e="T22" id="Seg_1294" s="T20">Then the schools.</ta>
            <ta e="T26" id="Seg_1295" s="T22">People were reading everything (written on the posters).</ta>
            <ta e="T29" id="Seg_1296" s="T26">They said: “The university is passing.”</ta>
            <ta e="T32" id="Seg_1297" s="T29">I stopped nearby.</ta>
            <ta e="T36" id="Seg_1298" s="T32">I looked, Angelina Ivanovna was passing by.</ta>
            <ta e="T42" id="Seg_1299" s="T36">I said: “Hello, Angelina Ivanovna, have a nice holiday.”</ta>
            <ta e="T46" id="Seg_1300" s="T42">And she didn't hear.</ta>
            <ta e="T47" id="Seg_1301" s="T46">She passed by.</ta>
            <ta e="T53" id="Seg_1302" s="T47">I said to an old woman: “Now your son will pass by.”</ta>
            <ta e="T57" id="Seg_1303" s="T53">Then I looked, Valjka was coming.</ta>
            <ta e="T62" id="Seg_1304" s="T57">They were reading again: “The hospital is passing by.”</ta>
            <ta e="T69" id="Seg_1305" s="T62">I looked at it and thought: Will Ada pass by, will Natasha pass by?</ta>
            <ta e="T73" id="Seg_1306" s="T69">And they didn't go (to the demonstration).</ta>
            <ta e="T78" id="Seg_1307" s="T73">And I was looking and looking [for them], [I couldn't see them] anywhere.</ta>
            <ta e="T82" id="Seg_1308" s="T78">And they didn't go (to the demonstration).</ta>
            <ta e="T86" id="Seg_1309" s="T82">And I didn't know anyone else.</ta>
            <ta e="T90" id="Seg_1310" s="T86">There were many people.</ta>
            <ta e="T92" id="Seg_1311" s="T90">They were dressed well.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_1312" s="T1">Am ersten Mai ging ich auf ein Fest.</ta>
            <ta e="T9" id="Seg_1313" s="T6">Es war so schön.</ta>
            <ta e="T13" id="Seg_1314" s="T9">Es gab dort viele verschiedene Flaggen.</ta>
            <ta e="T16" id="Seg_1315" s="T13">Viele Soldaten waren dort.</ta>
            <ta e="T20" id="Seg_1316" s="T16">Zuerst gingen die Soldaten vorbei.</ta>
            <ta e="T22" id="Seg_1317" s="T20">Dann die Schulen.</ta>
            <ta e="T26" id="Seg_1318" s="T22">Die Leute lasen alles (was auf Postern geschrieben war).</ta>
            <ta e="T29" id="Seg_1319" s="T26">Sie sagten: "Die Universität kommt vorbei."</ta>
            <ta e="T32" id="Seg_1320" s="T29">Ich hielt in der Nähe an.</ta>
            <ta e="T36" id="Seg_1321" s="T32">Ich guckte, Angelina Ivanovna ging vorbei.</ta>
            <ta e="T42" id="Seg_1322" s="T36">Ich sagte: "Hallo, Angelina Ivanovna, einen schönen Feiertag dir."</ta>
            <ta e="T46" id="Seg_1323" s="T42">Und sie hörte (mich) nicht.</ta>
            <ta e="T47" id="Seg_1324" s="T46">Sie ging vorbei.</ta>
            <ta e="T53" id="Seg_1325" s="T47">Ich sagte zu einer alten Frau: "Jetzt kommt dein Sohn vorbei."</ta>
            <ta e="T57" id="Seg_1326" s="T53">Dann schaute ich, Valka kam.</ta>
            <ta e="T62" id="Seg_1327" s="T57">Sie lasen wieder: "Das Krankenhaus kommt vorbei."</ta>
            <ta e="T69" id="Seg_1328" s="T62">Ich sah hin und dachte: Kommt Ada vorbei, kommt Natascha vorbei?</ta>
            <ta e="T73" id="Seg_1329" s="T69">Und sie kamen nicht (zur Demonstration).</ta>
            <ta e="T78" id="Seg_1330" s="T73">Ich schaute und schaute, sie waren nirgends [zu sehen].</ta>
            <ta e="T82" id="Seg_1331" s="T78">Und sie gingen nicht (zur Demonstration).</ta>
            <ta e="T86" id="Seg_1332" s="T82">Und ich kannte keinen anderen.</ta>
            <ta e="T90" id="Seg_1333" s="T86">Da waren viele Menschen.</ta>
            <ta e="T92" id="Seg_1334" s="T90">Sie waren gut gekleidet.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_1335" s="T1">Первого мая я ходила на праздник.</ta>
            <ta e="T9" id="Seg_1336" s="T6">До того хорошо было.</ta>
            <ta e="T13" id="Seg_1337" s="T9">Флагов было много всяких разных.</ta>
            <ta e="T16" id="Seg_1338" s="T13">Солдат много было.</ta>
            <ta e="T20" id="Seg_1339" s="T16">Сначала солдаты прошли.</ta>
            <ta e="T22" id="Seg_1340" s="T20">Потом школы.</ta>
            <ta e="T26" id="Seg_1341" s="T22">Люди всё читают.</ta>
            <ta e="T29" id="Seg_1342" s="T26">Говорят: “Университет идет”.</ta>
            <ta e="T32" id="Seg_1343" s="T29">Я близко встала.</ta>
            <ta e="T36" id="Seg_1344" s="T32">Гляжу, Ангелина Ивановна идет.</ta>
            <ta e="T42" id="Seg_1345" s="T36">Я говорю: “Здравствуй, Ангелина Ивановна, с праздником”.</ta>
            <ta e="T46" id="Seg_1346" s="T42">А она не слышит.</ta>
            <ta e="T47" id="Seg_1347" s="T46">Пошла.</ta>
            <ta e="T53" id="Seg_1348" s="T47">Я старухе сказала: “Твой сын сейчас пройдет”.</ta>
            <ta e="T57" id="Seg_1349" s="T53">Сейчас смотрю, Валька идет.</ta>
            <ta e="T62" id="Seg_1350" s="T57">Опять читают, говорят: “Больница идет”.</ta>
            <ta e="T69" id="Seg_1351" s="T62">Я гляжу, подумала: Ада пройдет, Наташа пройдет?</ta>
            <ta e="T73" id="Seg_1352" s="T69">А они не ходили.</ta>
            <ta e="T78" id="Seg_1353" s="T73">Я гляжу, гляжу, нигде нет.</ta>
            <ta e="T82" id="Seg_1354" s="T78">А они не ходили.</ta>
            <ta e="T86" id="Seg_1355" s="T82">Я больше никого не знаю.</ta>
            <ta e="T90" id="Seg_1356" s="T86">Ну людей много было.</ta>
            <ta e="T92" id="Seg_1357" s="T90">Хорошо одеты.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_1358" s="T1">первого мая я ходила на праздник</ta>
            <ta e="T9" id="Seg_1359" s="T6">до того хорошо было</ta>
            <ta e="T13" id="Seg_1360" s="T9">флагов много много разных</ta>
            <ta e="T16" id="Seg_1361" s="T13">солдатов много было</ta>
            <ta e="T20" id="Seg_1362" s="T16">сначала солдаты прошли</ta>
            <ta e="T26" id="Seg_1363" s="T22">люди все читают</ta>
            <ta e="T29" id="Seg_1364" s="T26">говорят университет идет</ta>
            <ta e="T32" id="Seg_1365" s="T29">я близко встала</ta>
            <ta e="T36" id="Seg_1366" s="T32">гляжу идет</ta>
            <ta e="T42" id="Seg_1367" s="T36">я говорю здравствуй с праздником</ta>
            <ta e="T46" id="Seg_1368" s="T42">а она не слышит</ta>
            <ta e="T47" id="Seg_1369" s="T46">и пошла</ta>
            <ta e="T53" id="Seg_1370" s="T47">я старухе сказала твой сын сейчас пройдет</ta>
            <ta e="T57" id="Seg_1371" s="T53">смотрю Валька идет</ta>
            <ta e="T62" id="Seg_1372" s="T57">опять читают говорят больница идет</ta>
            <ta e="T69" id="Seg_1373" s="T62">я гляжу думаю Ада пройдет Наташа пройдет</ta>
            <ta e="T73" id="Seg_1374" s="T69">а они не ходили</ta>
            <ta e="T78" id="Seg_1375" s="T73">я гляжу гляжу нигде нет</ta>
            <ta e="T82" id="Seg_1376" s="T78">а они не ходили</ta>
            <ta e="T86" id="Seg_1377" s="T82">а больше никого не знаю</ta>
            <ta e="T90" id="Seg_1378" s="T86">людей много было</ta>
            <ta e="T92" id="Seg_1379" s="T90">хорошо одеты</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T9" id="Seg_1380" s="T6">[BrM:] 'Da tao' changed to 'Datao'.</ta>
            <ta e="T20" id="Seg_1381" s="T16">[BrM:] Tentative analysis of 'mʼänɨdattə'.</ta>
            <ta e="T26" id="Seg_1382" s="T22">[BrM:] Tentative analysis of 'toɣulǯəlʼe'.</ta>
            <ta e="T32" id="Seg_1383" s="T29">[BrM:] Tentative analysis of 'nɨlʼedʼän'.</ta>
            <ta e="T46" id="Seg_1384" s="T42">[BrM:] IPFV or INFER?</ta>
            <ta e="T47" id="Seg_1385" s="T46">[BrM:] 1SG.S or 3SG.S?</ta>
            <ta e="T53" id="Seg_1386" s="T47">[KuAI:] Variant: 'meneǯa'.</ta>
            <ta e="T62" id="Seg_1387" s="T57">[BrM:] INFER? Tentative analysis of 'toɣulǯattə'.</ta>
            <ta e="T69" id="Seg_1388" s="T62">[KuAI:] Variant: 'mannɨban'.</ta>
            <ta e="T86" id="Seg_1389" s="T82">[BrM:] 'kudäm näss' changed to 'kudämnäss'. [BrM:] The last part of 'kudämnäss' may be negative particle 'asa'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T62" id="Seg_1390" s="T57">на транспорантах стоящие люди читают</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
