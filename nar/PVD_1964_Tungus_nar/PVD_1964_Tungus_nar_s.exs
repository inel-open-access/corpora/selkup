<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Tungus_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Tungus_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">70</ud-information>
            <ud-information attribute-name="# HIAT:w">49</ud-information>
            <ud-information attribute-name="# e">49</ud-information>
            <ud-information attribute-name="# HIAT:u">11</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T50" id="Seg_0" n="sc" s="T1">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Perwɨj</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">ras</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">man</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">Кalpašɨkɨn</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">qoǯirsau</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_19" n="HIAT:w" s="T6">kəlɨllam</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_23" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">Datawa</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">maːttɨ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">taːɣɨdʼan</ts>
                  <nts id="Seg_32" n="HIAT:ip">,</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_35" n="HIAT:w" s="T10">tʼaran</ts>
                  <nts id="Seg_36" n="HIAT:ip">:</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_38" n="HIAT:ip">“</nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">Čaːʒaltə</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">ponə</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_47" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">Qaj</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">nildʼzʼi</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">qula</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip">”</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_60" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">A</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">mekga</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">tʼärattə</ts>
                  <nts id="Seg_69" n="HIAT:ip">:</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_71" n="HIAT:ip">“</nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">No</ts>
                  <nts id="Seg_74" n="HIAT:ip">,</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">kəlolla</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip">”</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_82" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_84" n="HIAT:w" s="T21">Tebla</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_87" n="HIAT:w" s="T22">šeːɣan</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_90" n="HIAT:w" s="T23">jewattə</ts>
                  <nts id="Seg_91" n="HIAT:ip">.</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_94" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_96" n="HIAT:w" s="T24">Sejlat</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_99" n="HIAT:w" s="T25">töːska</ts>
                  <nts id="Seg_100" n="HIAT:ip">,</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_103" n="HIAT:w" s="T26">a</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_106" n="HIAT:w" s="T27">optɨlattə</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_109" n="HIAT:w" s="T28">tüːpba</ts>
                  <nts id="Seg_110" n="HIAT:ip">.</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_113" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_115" n="HIAT:w" s="T29">Tebla</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_118" n="HIAT:w" s="T30">atdokan</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_121" n="HIAT:w" s="T31">paːroɣɨn</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_124" n="HIAT:w" s="T32">aːmdɨzattə</ts>
                  <nts id="Seg_125" n="HIAT:ip">.</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_128" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_130" n="HIAT:w" s="T33">Sobla</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_133" n="HIAT:w" s="T34">qum</ts>
                  <nts id="Seg_134" n="HIAT:ip">,</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_137" n="HIAT:w" s="T35">wes</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_140" n="HIAT:w" s="T36">täbequla</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_143" n="HIAT:w" s="T37">jezatt</ts>
                  <nts id="Seg_144" n="HIAT:ip">.</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_147" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_149" n="HIAT:w" s="T38">A</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_152" n="HIAT:w" s="T39">näjɣulam</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_155" n="HIAT:w" s="T40">ass</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_158" n="HIAT:w" s="T41">qoǯirsau</ts>
                  <nts id="Seg_159" n="HIAT:ip">.</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_162" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_164" n="HIAT:w" s="T42">A</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_167" n="HIAT:w" s="T43">sɨgalla</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_170" n="HIAT:w" s="T44">toʒə</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_173" n="HIAT:w" s="T45">šeːɣan</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_176" n="HIAT:w" s="T46">jewattə</ts>
                  <nts id="Seg_177" n="HIAT:ip">.</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_180" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_182" n="HIAT:w" s="T47">No</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_185" n="HIAT:w" s="T48">sejlattə</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_188" n="HIAT:w" s="T49">iːbɨgaj</ts>
                  <nts id="Seg_189" n="HIAT:ip">.</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T50" id="Seg_191" n="sc" s="T1">
               <ts e="T2" id="Seg_193" n="e" s="T1">Perwɨj </ts>
               <ts e="T3" id="Seg_195" n="e" s="T2">ras </ts>
               <ts e="T4" id="Seg_197" n="e" s="T3">man </ts>
               <ts e="T5" id="Seg_199" n="e" s="T4">Кalpašɨkɨn </ts>
               <ts e="T6" id="Seg_201" n="e" s="T5">qoǯirsau </ts>
               <ts e="T7" id="Seg_203" n="e" s="T6">kəlɨllam. </ts>
               <ts e="T8" id="Seg_205" n="e" s="T7">Datawa </ts>
               <ts e="T9" id="Seg_207" n="e" s="T8">maːttɨ </ts>
               <ts e="T10" id="Seg_209" n="e" s="T9">taːɣɨdʼan, </ts>
               <ts e="T11" id="Seg_211" n="e" s="T10">tʼaran: </ts>
               <ts e="T12" id="Seg_213" n="e" s="T11">“Čaːʒaltə </ts>
               <ts e="T13" id="Seg_215" n="e" s="T12">ponə. </ts>
               <ts e="T14" id="Seg_217" n="e" s="T13">Qaj </ts>
               <ts e="T15" id="Seg_219" n="e" s="T14">nildʼzʼi </ts>
               <ts e="T16" id="Seg_221" n="e" s="T15">qula.” </ts>
               <ts e="T17" id="Seg_223" n="e" s="T16">A </ts>
               <ts e="T18" id="Seg_225" n="e" s="T17">mekga </ts>
               <ts e="T19" id="Seg_227" n="e" s="T18">tʼärattə: </ts>
               <ts e="T20" id="Seg_229" n="e" s="T19">“No, </ts>
               <ts e="T21" id="Seg_231" n="e" s="T20">kəlolla.” </ts>
               <ts e="T22" id="Seg_233" n="e" s="T21">Tebla </ts>
               <ts e="T23" id="Seg_235" n="e" s="T22">šeːɣan </ts>
               <ts e="T24" id="Seg_237" n="e" s="T23">jewattə. </ts>
               <ts e="T25" id="Seg_239" n="e" s="T24">Sejlat </ts>
               <ts e="T26" id="Seg_241" n="e" s="T25">töːska, </ts>
               <ts e="T27" id="Seg_243" n="e" s="T26">a </ts>
               <ts e="T28" id="Seg_245" n="e" s="T27">optɨlattə </ts>
               <ts e="T29" id="Seg_247" n="e" s="T28">tüːpba. </ts>
               <ts e="T30" id="Seg_249" n="e" s="T29">Tebla </ts>
               <ts e="T31" id="Seg_251" n="e" s="T30">atdokan </ts>
               <ts e="T32" id="Seg_253" n="e" s="T31">paːroɣɨn </ts>
               <ts e="T33" id="Seg_255" n="e" s="T32">aːmdɨzattə. </ts>
               <ts e="T34" id="Seg_257" n="e" s="T33">Sobla </ts>
               <ts e="T35" id="Seg_259" n="e" s="T34">qum, </ts>
               <ts e="T36" id="Seg_261" n="e" s="T35">wes </ts>
               <ts e="T37" id="Seg_263" n="e" s="T36">täbequla </ts>
               <ts e="T38" id="Seg_265" n="e" s="T37">jezatt. </ts>
               <ts e="T39" id="Seg_267" n="e" s="T38">A </ts>
               <ts e="T40" id="Seg_269" n="e" s="T39">näjɣulam </ts>
               <ts e="T41" id="Seg_271" n="e" s="T40">ass </ts>
               <ts e="T42" id="Seg_273" n="e" s="T41">qoǯirsau. </ts>
               <ts e="T43" id="Seg_275" n="e" s="T42">A </ts>
               <ts e="T44" id="Seg_277" n="e" s="T43">sɨgalla </ts>
               <ts e="T45" id="Seg_279" n="e" s="T44">toʒə </ts>
               <ts e="T46" id="Seg_281" n="e" s="T45">šeːɣan </ts>
               <ts e="T47" id="Seg_283" n="e" s="T46">jewattə. </ts>
               <ts e="T48" id="Seg_285" n="e" s="T47">No </ts>
               <ts e="T49" id="Seg_287" n="e" s="T48">sejlattə </ts>
               <ts e="T50" id="Seg_289" n="e" s="T49">iːbɨgaj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_290" s="T1">PVD_1964_Tungus_nar.001 (001.001)</ta>
            <ta e="T13" id="Seg_291" s="T7">PVD_1964_Tungus_nar.002 (001.002)</ta>
            <ta e="T16" id="Seg_292" s="T13">PVD_1964_Tungus_nar.003 (001.003)</ta>
            <ta e="T21" id="Seg_293" s="T16">PVD_1964_Tungus_nar.004 (001.004)</ta>
            <ta e="T24" id="Seg_294" s="T21">PVD_1964_Tungus_nar.005 (001.005)</ta>
            <ta e="T29" id="Seg_295" s="T24">PVD_1964_Tungus_nar.006 (001.006)</ta>
            <ta e="T33" id="Seg_296" s="T29">PVD_1964_Tungus_nar.007 (001.007)</ta>
            <ta e="T38" id="Seg_297" s="T33">PVD_1964_Tungus_nar.008 (001.008)</ta>
            <ta e="T42" id="Seg_298" s="T38">PVD_1964_Tungus_nar.009 (001.009)</ta>
            <ta e="T47" id="Seg_299" s="T42">PVD_1964_Tungus_nar.010 (001.010)</ta>
            <ta e="T50" id="Seg_300" s="T47">PVD_1964_Tungus_nar.011 (001.011)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T7" id="Seg_301" s="T1">первый рас ман Кал′пашыкын ′kоджирсау̹ ′къ̊lыlлам.</ta>
            <ta e="T13" id="Seg_302" s="T7">да ′тава ′ма̄тты ′та̄ɣыдʼан, тʼа′ран: ′тша̄жалтъ ′понъ.</ta>
            <ta e="T16" id="Seg_303" s="T13">kай ниl′дʼзʼи kу′lа.</ta>
            <ta e="T21" id="Seg_304" s="T16">а ′мекга тʼӓ′раттъ: но, къ′lо(ы)lла.</ta>
            <ta e="T24" id="Seg_305" s="T21">теб′ла ше̄ɣан ′jе̨wаттъ.</ta>
            <ta e="T29" id="Seg_306" s="T24">сей′лат ′тӧ̄ска, а опты′латтъ ′тӱ̄пб̂а.</ta>
            <ta e="T33" id="Seg_307" s="T29">теб′ла ат′докан ′па̄роɣын ′а̄мдызаттъ.</ta>
            <ta e="T38" id="Seg_308" s="T33">′собла k(ɣ)ум, вес ′тӓбеkуlа ′jе̨затт.</ta>
            <ta e="T42" id="Seg_309" s="T38">а нӓйɣу′лам асс ′kоджирсау̹.</ta>
            <ta e="T47" id="Seg_310" s="T42">а сы′галла тожъ ′ше̄ɣан ′jеwаттъ.</ta>
            <ta e="T50" id="Seg_311" s="T47">но сейлаттъ ′ӣбыгай.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T7" id="Seg_312" s="T1">perwɨj ras man Кalpašɨkɨn qoǯirsau̹ kəlɨllam.</ta>
            <ta e="T13" id="Seg_313" s="T7">da tawa maːttɨ taːɣɨdʼan, tʼaran: tšaːʒaltə ponə.</ta>
            <ta e="T16" id="Seg_314" s="T13">qaj nildʼzʼi qula.</ta>
            <ta e="T21" id="Seg_315" s="T16">a mekga tʼärattə: no, kəlo(ɨ)lla.</ta>
            <ta e="T24" id="Seg_316" s="T21">tebla šeːɣan jewattə.</ta>
            <ta e="T29" id="Seg_317" s="T24">sejlat töːska, a optɨlattə tüːpb̂a.</ta>
            <ta e="T33" id="Seg_318" s="T29">tebla atdokan paːroɣɨn aːmdɨzattə.</ta>
            <ta e="T38" id="Seg_319" s="T33">sobla q(ɣ)um, wes täbequla jezatt.</ta>
            <ta e="T42" id="Seg_320" s="T38">a näjɣulam ass qoǯirsau̹.</ta>
            <ta e="T47" id="Seg_321" s="T42">a sɨgalla toʒə šeːɣan jewattə.</ta>
            <ta e="T50" id="Seg_322" s="T47">no sejlattə iːbɨgaj.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_323" s="T1">Perwɨj ras man Кalpašɨkɨn qoǯirsau kəlɨllam. </ta>
            <ta e="T13" id="Seg_324" s="T7">Datawa maːttɨ taːɣɨdʼan, tʼaran: “Čaːʒaltə ponə. </ta>
            <ta e="T16" id="Seg_325" s="T13">Qaj nildʼzʼi qula.” </ta>
            <ta e="T21" id="Seg_326" s="T16">A mekga tʼärattə: “No, kəlolla.” </ta>
            <ta e="T24" id="Seg_327" s="T21">Tebla šeːɣan jewattə. </ta>
            <ta e="T29" id="Seg_328" s="T24">Sejlat töːska, a optɨlattə tüːpba. </ta>
            <ta e="T33" id="Seg_329" s="T29">Tebla atdokan paːroɣɨn aːmdɨzattə. </ta>
            <ta e="T38" id="Seg_330" s="T33">Sobla qum, wes täbequla jezatt. </ta>
            <ta e="T42" id="Seg_331" s="T38">A näjɣulam ass qoǯirsau. </ta>
            <ta e="T47" id="Seg_332" s="T42">A sɨgalla toʒə šeːɣan jewattə. </ta>
            <ta e="T50" id="Seg_333" s="T47">No sejlattə iːbɨgaj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_334" s="T1">perwɨj</ta>
            <ta e="T3" id="Seg_335" s="T2">ras</ta>
            <ta e="T4" id="Seg_336" s="T3">man</ta>
            <ta e="T5" id="Seg_337" s="T4">Кalpašɨ-kɨn</ta>
            <ta e="T6" id="Seg_338" s="T5">qo-ǯir-sa-u</ta>
            <ta e="T7" id="Seg_339" s="T6">kəlɨl-la-m</ta>
            <ta e="T8" id="Seg_340" s="T7">datawa</ta>
            <ta e="T9" id="Seg_341" s="T8">maːt-tɨ</ta>
            <ta e="T10" id="Seg_342" s="T9">taːɣɨdʼa-n</ta>
            <ta e="T11" id="Seg_343" s="T10">tʼara-n</ta>
            <ta e="T12" id="Seg_344" s="T11">čaːʒa-ltə</ta>
            <ta e="T13" id="Seg_345" s="T12">ponə</ta>
            <ta e="T14" id="Seg_346" s="T13">qaj</ta>
            <ta e="T15" id="Seg_347" s="T14">nildʼzʼi</ta>
            <ta e="T16" id="Seg_348" s="T15">qu-la</ta>
            <ta e="T17" id="Seg_349" s="T16">a</ta>
            <ta e="T18" id="Seg_350" s="T17">mekga</ta>
            <ta e="T19" id="Seg_351" s="T18">tʼära-ttə</ta>
            <ta e="T20" id="Seg_352" s="T19">no</ta>
            <ta e="T21" id="Seg_353" s="T20">kəlol-la</ta>
            <ta e="T22" id="Seg_354" s="T21">teb-la</ta>
            <ta e="T23" id="Seg_355" s="T22">šeːɣa-n</ta>
            <ta e="T24" id="Seg_356" s="T23">je-wa-ttə</ta>
            <ta e="T25" id="Seg_357" s="T24">sej-la-t</ta>
            <ta e="T26" id="Seg_358" s="T25">töːska</ta>
            <ta e="T27" id="Seg_359" s="T26">a</ta>
            <ta e="T28" id="Seg_360" s="T27">optɨ-la-ttə</ta>
            <ta e="T29" id="Seg_361" s="T28">tüːpba</ta>
            <ta e="T30" id="Seg_362" s="T29">teb-la</ta>
            <ta e="T31" id="Seg_363" s="T30">atdo-ka-n</ta>
            <ta e="T32" id="Seg_364" s="T31">paːr-o-ɣɨn</ta>
            <ta e="T33" id="Seg_365" s="T32">aːmdɨ-za-ttə</ta>
            <ta e="T34" id="Seg_366" s="T33">sobla</ta>
            <ta e="T35" id="Seg_367" s="T34">qum</ta>
            <ta e="T36" id="Seg_368" s="T35">wes</ta>
            <ta e="T37" id="Seg_369" s="T36">täbe-qu-la</ta>
            <ta e="T38" id="Seg_370" s="T37">je-za-tt</ta>
            <ta e="T39" id="Seg_371" s="T38">a</ta>
            <ta e="T40" id="Seg_372" s="T39">nä-j-ɣu-la-m</ta>
            <ta e="T41" id="Seg_373" s="T40">ass</ta>
            <ta e="T42" id="Seg_374" s="T41">qo-ǯir-sa-u</ta>
            <ta e="T43" id="Seg_375" s="T42">a</ta>
            <ta e="T44" id="Seg_376" s="T43">sɨgal-la</ta>
            <ta e="T45" id="Seg_377" s="T44">toʒə</ta>
            <ta e="T46" id="Seg_378" s="T45">šeːɣa-n</ta>
            <ta e="T47" id="Seg_379" s="T46">je-wa-ttə</ta>
            <ta e="T48" id="Seg_380" s="T47">no</ta>
            <ta e="T49" id="Seg_381" s="T48">sej-la-ttə</ta>
            <ta e="T50" id="Seg_382" s="T49">iːbɨgaj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_383" s="T1">perwɨj</ta>
            <ta e="T3" id="Seg_384" s="T2">ras</ta>
            <ta e="T4" id="Seg_385" s="T3">man</ta>
            <ta e="T5" id="Seg_386" s="T4">Kolpaše-qɨn</ta>
            <ta e="T6" id="Seg_387" s="T5">qo-nǯir-sɨ-w</ta>
            <ta e="T7" id="Seg_388" s="T6">kəlɨl-la-m</ta>
            <ta e="T8" id="Seg_389" s="T7">tatawa</ta>
            <ta e="T9" id="Seg_390" s="T8">maːt-ntə</ta>
            <ta e="T10" id="Seg_391" s="T9">taɣətʼe-ŋ</ta>
            <ta e="T11" id="Seg_392" s="T10">tʼärɨ-ŋ</ta>
            <ta e="T12" id="Seg_393" s="T11">čaǯɨ-naltə</ta>
            <ta e="T13" id="Seg_394" s="T12">poːne</ta>
            <ta e="T14" id="Seg_395" s="T13">qaj</ta>
            <ta e="T15" id="Seg_396" s="T14">nʼilʼdʼi</ta>
            <ta e="T16" id="Seg_397" s="T15">qum-la</ta>
            <ta e="T17" id="Seg_398" s="T16">a</ta>
            <ta e="T18" id="Seg_399" s="T17">mekka</ta>
            <ta e="T19" id="Seg_400" s="T18">tʼärɨ-tɨn</ta>
            <ta e="T20" id="Seg_401" s="T19">nu</ta>
            <ta e="T21" id="Seg_402" s="T20">kəlɨl-la</ta>
            <ta e="T22" id="Seg_403" s="T21">täp-la</ta>
            <ta e="T23" id="Seg_404" s="T22">šeːɣa-ŋ</ta>
            <ta e="T24" id="Seg_405" s="T23">eː-nɨ-tɨn</ta>
            <ta e="T25" id="Seg_406" s="T24">sej-la-tɨn</ta>
            <ta e="T26" id="Seg_407" s="T25">tʼöska</ta>
            <ta e="T27" id="Seg_408" s="T26">a</ta>
            <ta e="T28" id="Seg_409" s="T27">optə-la-tɨn</ta>
            <ta e="T29" id="Seg_410" s="T28">tʼümbɨ</ta>
            <ta e="T30" id="Seg_411" s="T29">täp-la</ta>
            <ta e="T31" id="Seg_412" s="T30">andǝ-ka-n</ta>
            <ta e="T32" id="Seg_413" s="T31">par-ɨ-qɨn</ta>
            <ta e="T33" id="Seg_414" s="T32">amdɨ-sɨ-tɨn</ta>
            <ta e="T34" id="Seg_415" s="T33">sobla</ta>
            <ta e="T35" id="Seg_416" s="T34">qum</ta>
            <ta e="T36" id="Seg_417" s="T35">wesʼ</ta>
            <ta e="T37" id="Seg_418" s="T36">täbe-qum-la</ta>
            <ta e="T38" id="Seg_419" s="T37">eː-sɨ-tɨn</ta>
            <ta e="T39" id="Seg_420" s="T38">a</ta>
            <ta e="T40" id="Seg_421" s="T39">ne-lʼ-qum-la-m</ta>
            <ta e="T41" id="Seg_422" s="T40">asa</ta>
            <ta e="T42" id="Seg_423" s="T41">qo-nǯir-sɨ-w</ta>
            <ta e="T43" id="Seg_424" s="T42">a</ta>
            <ta e="T44" id="Seg_425" s="T43">sɨgan-la</ta>
            <ta e="T45" id="Seg_426" s="T44">toʒa</ta>
            <ta e="T46" id="Seg_427" s="T45">šeːɣa-ŋ</ta>
            <ta e="T47" id="Seg_428" s="T46">eː-nɨ-tɨn</ta>
            <ta e="T48" id="Seg_429" s="T47">nu</ta>
            <ta e="T49" id="Seg_430" s="T48">sej-la-tɨn</ta>
            <ta e="T50" id="Seg_431" s="T49">iːbəgaj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_432" s="T1">first</ta>
            <ta e="T3" id="Seg_433" s="T2">time.[NOM]</ta>
            <ta e="T4" id="Seg_434" s="T3">I.NOM</ta>
            <ta e="T5" id="Seg_435" s="T4">Kolpashevo-LOC</ta>
            <ta e="T6" id="Seg_436" s="T5">see-DRV-PST-1SG.O</ta>
            <ta e="T7" id="Seg_437" s="T6">Tungus-PL-ACC</ta>
            <ta e="T8" id="Seg_438" s="T7">to.such.extent</ta>
            <ta e="T9" id="Seg_439" s="T8">house-ILL</ta>
            <ta e="T10" id="Seg_440" s="T9">fly.into-1SG.S</ta>
            <ta e="T11" id="Seg_441" s="T10">say-1SG.S</ta>
            <ta e="T12" id="Seg_442" s="T11">run-IMP.2PL.S/O</ta>
            <ta e="T13" id="Seg_443" s="T12">outward(s)</ta>
            <ta e="T14" id="Seg_444" s="T13">what</ta>
            <ta e="T15" id="Seg_445" s="T14">such</ta>
            <ta e="T16" id="Seg_446" s="T15">human.being-PL.[NOM]</ta>
            <ta e="T17" id="Seg_447" s="T16">and</ta>
            <ta e="T18" id="Seg_448" s="T17">I.ALL</ta>
            <ta e="T19" id="Seg_449" s="T18">say-3PL</ta>
            <ta e="T20" id="Seg_450" s="T19">now</ta>
            <ta e="T21" id="Seg_451" s="T20">Tungus-PL.[NOM]</ta>
            <ta e="T22" id="Seg_452" s="T21">(s)he-PL.[NOM]</ta>
            <ta e="T23" id="Seg_453" s="T22">black-ADVZ</ta>
            <ta e="T24" id="Seg_454" s="T23">be-CO-3PL</ta>
            <ta e="T25" id="Seg_455" s="T24">eye-PL.[NOM]-3PL</ta>
            <ta e="T26" id="Seg_456" s="T25">narrow</ta>
            <ta e="T27" id="Seg_457" s="T26">and</ta>
            <ta e="T28" id="Seg_458" s="T27">hair-PL.[NOM]-3PL</ta>
            <ta e="T29" id="Seg_459" s="T28">long</ta>
            <ta e="T30" id="Seg_460" s="T29">(s)he-PL.[NOM]</ta>
            <ta e="T31" id="Seg_461" s="T30">boat-DIM-GEN</ta>
            <ta e="T32" id="Seg_462" s="T31">top-EP-LOC</ta>
            <ta e="T33" id="Seg_463" s="T32">sit-PST-3PL</ta>
            <ta e="T34" id="Seg_464" s="T33">five</ta>
            <ta e="T35" id="Seg_465" s="T34">human.being.[NOM]</ta>
            <ta e="T36" id="Seg_466" s="T35">all</ta>
            <ta e="T37" id="Seg_467" s="T36">man-human.being-PL.[NOM]</ta>
            <ta e="T38" id="Seg_468" s="T37">be-PST-3PL</ta>
            <ta e="T39" id="Seg_469" s="T38">and</ta>
            <ta e="T40" id="Seg_470" s="T39">woman-ADJZ-human.being-PL-ACC</ta>
            <ta e="T41" id="Seg_471" s="T40">NEG</ta>
            <ta e="T42" id="Seg_472" s="T41">see-DRV-PST-1SG.O</ta>
            <ta e="T43" id="Seg_473" s="T42">and</ta>
            <ta e="T44" id="Seg_474" s="T43">Gipsy-PL.[NOM]</ta>
            <ta e="T45" id="Seg_475" s="T44">also</ta>
            <ta e="T46" id="Seg_476" s="T45">black-ADVZ</ta>
            <ta e="T47" id="Seg_477" s="T46">be-CO-3PL</ta>
            <ta e="T48" id="Seg_478" s="T47">now</ta>
            <ta e="T49" id="Seg_479" s="T48">eye-PL.[NOM]-3PL</ta>
            <ta e="T50" id="Seg_480" s="T49">big</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_481" s="T1">первый</ta>
            <ta e="T3" id="Seg_482" s="T2">раз.[NOM]</ta>
            <ta e="T4" id="Seg_483" s="T3">я.NOM</ta>
            <ta e="T5" id="Seg_484" s="T4">Колпашево-LOC</ta>
            <ta e="T6" id="Seg_485" s="T5">увидеть-DRV-PST-1SG.O</ta>
            <ta e="T7" id="Seg_486" s="T6">тунгус-PL-ACC</ta>
            <ta e="T8" id="Seg_487" s="T7">до.того</ta>
            <ta e="T9" id="Seg_488" s="T8">дом-ILL</ta>
            <ta e="T10" id="Seg_489" s="T9">влететь-1SG.S</ta>
            <ta e="T11" id="Seg_490" s="T10">сказать-1SG.S</ta>
            <ta e="T12" id="Seg_491" s="T11">бегать-IMP.2PL.S/O</ta>
            <ta e="T13" id="Seg_492" s="T12">наружу</ta>
            <ta e="T14" id="Seg_493" s="T13">что</ta>
            <ta e="T15" id="Seg_494" s="T14">такой</ta>
            <ta e="T16" id="Seg_495" s="T15">человек-PL.[NOM]</ta>
            <ta e="T17" id="Seg_496" s="T16">а</ta>
            <ta e="T18" id="Seg_497" s="T17">я.ALL</ta>
            <ta e="T19" id="Seg_498" s="T18">сказать-3PL</ta>
            <ta e="T20" id="Seg_499" s="T19">ну</ta>
            <ta e="T21" id="Seg_500" s="T20">тунгус-PL.[NOM]</ta>
            <ta e="T22" id="Seg_501" s="T21">он(а)-PL.[NOM]</ta>
            <ta e="T23" id="Seg_502" s="T22">чёрный-ADVZ</ta>
            <ta e="T24" id="Seg_503" s="T23">быть-CO-3PL</ta>
            <ta e="T25" id="Seg_504" s="T24">глаз-PL.[NOM]-3PL</ta>
            <ta e="T26" id="Seg_505" s="T25">узкий</ta>
            <ta e="T27" id="Seg_506" s="T26">а</ta>
            <ta e="T28" id="Seg_507" s="T27">волос-PL.[NOM]-3PL</ta>
            <ta e="T29" id="Seg_508" s="T28">длинный</ta>
            <ta e="T30" id="Seg_509" s="T29">он(а)-PL.[NOM]</ta>
            <ta e="T31" id="Seg_510" s="T30">обласок-DIM-GEN</ta>
            <ta e="T32" id="Seg_511" s="T31">верхняя.часть-EP-LOC</ta>
            <ta e="T33" id="Seg_512" s="T32">сидеть-PST-3PL</ta>
            <ta e="T34" id="Seg_513" s="T33">пять</ta>
            <ta e="T35" id="Seg_514" s="T34">человек.[NOM]</ta>
            <ta e="T36" id="Seg_515" s="T35">все</ta>
            <ta e="T37" id="Seg_516" s="T36">мужчина-человек-PL.[NOM]</ta>
            <ta e="T38" id="Seg_517" s="T37">быть-PST-3PL</ta>
            <ta e="T39" id="Seg_518" s="T38">а</ta>
            <ta e="T40" id="Seg_519" s="T39">женщина-ADJZ-человек-PL-ACC</ta>
            <ta e="T41" id="Seg_520" s="T40">NEG</ta>
            <ta e="T42" id="Seg_521" s="T41">увидеть-DRV-PST-1SG.O</ta>
            <ta e="T43" id="Seg_522" s="T42">а</ta>
            <ta e="T44" id="Seg_523" s="T43">цыган-PL.[NOM]</ta>
            <ta e="T45" id="Seg_524" s="T44">тоже</ta>
            <ta e="T46" id="Seg_525" s="T45">чёрный-ADVZ</ta>
            <ta e="T47" id="Seg_526" s="T46">быть-CO-3PL</ta>
            <ta e="T48" id="Seg_527" s="T47">ну</ta>
            <ta e="T49" id="Seg_528" s="T48">глаз-PL.[NOM]-3PL</ta>
            <ta e="T50" id="Seg_529" s="T49">большой</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_530" s="T1">adj</ta>
            <ta e="T3" id="Seg_531" s="T2">n.[n:case]</ta>
            <ta e="T4" id="Seg_532" s="T3">pers</ta>
            <ta e="T5" id="Seg_533" s="T4">nprop-n:case</ta>
            <ta e="T6" id="Seg_534" s="T5">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_535" s="T6">n-n:num-n:case</ta>
            <ta e="T8" id="Seg_536" s="T7">adv</ta>
            <ta e="T9" id="Seg_537" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_538" s="T9">v-v:pn</ta>
            <ta e="T11" id="Seg_539" s="T10">v-v:pn</ta>
            <ta e="T12" id="Seg_540" s="T11">v-v:mood.pn</ta>
            <ta e="T13" id="Seg_541" s="T12">adv</ta>
            <ta e="T14" id="Seg_542" s="T13">interrog</ta>
            <ta e="T15" id="Seg_543" s="T14">adj</ta>
            <ta e="T16" id="Seg_544" s="T15">n-n:num.[n:case]</ta>
            <ta e="T17" id="Seg_545" s="T16">conj</ta>
            <ta e="T18" id="Seg_546" s="T17">pers</ta>
            <ta e="T19" id="Seg_547" s="T18">v-v:pn</ta>
            <ta e="T20" id="Seg_548" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_549" s="T20">n-n:num.[n:case]</ta>
            <ta e="T22" id="Seg_550" s="T21">pers-n:num.[n:case]</ta>
            <ta e="T23" id="Seg_551" s="T22">adj-adj&gt;adv</ta>
            <ta e="T24" id="Seg_552" s="T23">v-v:ins-v:pn</ta>
            <ta e="T25" id="Seg_553" s="T24">n-n:num.[n:case]-n:poss</ta>
            <ta e="T26" id="Seg_554" s="T25">adj</ta>
            <ta e="T27" id="Seg_555" s="T26">conj</ta>
            <ta e="T28" id="Seg_556" s="T27">n-n:num.[n:case]-n:poss</ta>
            <ta e="T29" id="Seg_557" s="T28">adj</ta>
            <ta e="T30" id="Seg_558" s="T29">pers-n:num.[n:case]</ta>
            <ta e="T31" id="Seg_559" s="T30">n-n&gt;n-n:case</ta>
            <ta e="T32" id="Seg_560" s="T31">n-n:ins-n:case</ta>
            <ta e="T33" id="Seg_561" s="T32">v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_562" s="T33">num</ta>
            <ta e="T35" id="Seg_563" s="T34">n.[n:case]</ta>
            <ta e="T36" id="Seg_564" s="T35">quant</ta>
            <ta e="T37" id="Seg_565" s="T36">n-n-n:num.[n:case]</ta>
            <ta e="T38" id="Seg_566" s="T37">v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_567" s="T38">conj</ta>
            <ta e="T40" id="Seg_568" s="T39">n-n&gt;adj-n-n:num-n:case</ta>
            <ta e="T41" id="Seg_569" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_570" s="T41">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_571" s="T42">conj</ta>
            <ta e="T44" id="Seg_572" s="T43">n-n:num.[n:case]</ta>
            <ta e="T45" id="Seg_573" s="T44">adv</ta>
            <ta e="T46" id="Seg_574" s="T45">adj-adj&gt;adv</ta>
            <ta e="T47" id="Seg_575" s="T46">v-v:ins-v:pn</ta>
            <ta e="T48" id="Seg_576" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_577" s="T48">n-n:num.[n:case]-n:poss</ta>
            <ta e="T50" id="Seg_578" s="T49">adj</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_579" s="T1">adj</ta>
            <ta e="T3" id="Seg_580" s="T2">n</ta>
            <ta e="T4" id="Seg_581" s="T3">pers</ta>
            <ta e="T5" id="Seg_582" s="T4">nprop</ta>
            <ta e="T6" id="Seg_583" s="T5">v</ta>
            <ta e="T7" id="Seg_584" s="T6">n</ta>
            <ta e="T8" id="Seg_585" s="T7">adv</ta>
            <ta e="T9" id="Seg_586" s="T8">n</ta>
            <ta e="T10" id="Seg_587" s="T9">v</ta>
            <ta e="T11" id="Seg_588" s="T10">v</ta>
            <ta e="T12" id="Seg_589" s="T11">v</ta>
            <ta e="T13" id="Seg_590" s="T12">adv</ta>
            <ta e="T14" id="Seg_591" s="T13">interrog</ta>
            <ta e="T15" id="Seg_592" s="T14">adj</ta>
            <ta e="T16" id="Seg_593" s="T15">n</ta>
            <ta e="T17" id="Seg_594" s="T16">conj</ta>
            <ta e="T18" id="Seg_595" s="T17">pers</ta>
            <ta e="T19" id="Seg_596" s="T18">v</ta>
            <ta e="T20" id="Seg_597" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_598" s="T20">n</ta>
            <ta e="T22" id="Seg_599" s="T21">pers</ta>
            <ta e="T23" id="Seg_600" s="T22">adv</ta>
            <ta e="T24" id="Seg_601" s="T23">v</ta>
            <ta e="T25" id="Seg_602" s="T24">n</ta>
            <ta e="T26" id="Seg_603" s="T25">adj</ta>
            <ta e="T27" id="Seg_604" s="T26">conj</ta>
            <ta e="T28" id="Seg_605" s="T27">n</ta>
            <ta e="T29" id="Seg_606" s="T28">adj</ta>
            <ta e="T30" id="Seg_607" s="T29">pers</ta>
            <ta e="T31" id="Seg_608" s="T30">n</ta>
            <ta e="T32" id="Seg_609" s="T31">n</ta>
            <ta e="T33" id="Seg_610" s="T32">v</ta>
            <ta e="T34" id="Seg_611" s="T33">num</ta>
            <ta e="T35" id="Seg_612" s="T34">n</ta>
            <ta e="T36" id="Seg_613" s="T35">quant</ta>
            <ta e="T37" id="Seg_614" s="T36">n</ta>
            <ta e="T38" id="Seg_615" s="T37">v</ta>
            <ta e="T39" id="Seg_616" s="T38">conj</ta>
            <ta e="T40" id="Seg_617" s="T39">n</ta>
            <ta e="T41" id="Seg_618" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_619" s="T41">v</ta>
            <ta e="T43" id="Seg_620" s="T42">conj</ta>
            <ta e="T44" id="Seg_621" s="T43">n</ta>
            <ta e="T45" id="Seg_622" s="T44">adv</ta>
            <ta e="T46" id="Seg_623" s="T45">adv</ta>
            <ta e="T47" id="Seg_624" s="T46">v</ta>
            <ta e="T48" id="Seg_625" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_626" s="T48">n</ta>
            <ta e="T50" id="Seg_627" s="T49">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_628" s="T2">np:Time</ta>
            <ta e="T4" id="Seg_629" s="T3">pro.h:E</ta>
            <ta e="T5" id="Seg_630" s="T4">np:L</ta>
            <ta e="T7" id="Seg_631" s="T6">np.h:Th</ta>
            <ta e="T9" id="Seg_632" s="T8">np:G</ta>
            <ta e="T10" id="Seg_633" s="T9">0.1.h:A</ta>
            <ta e="T11" id="Seg_634" s="T10">0.1.h:A</ta>
            <ta e="T12" id="Seg_635" s="T11">0.2.h:A</ta>
            <ta e="T13" id="Seg_636" s="T12">adv:G</ta>
            <ta e="T18" id="Seg_637" s="T17">pro.h:R</ta>
            <ta e="T19" id="Seg_638" s="T18">0.3.h:A</ta>
            <ta e="T22" id="Seg_639" s="T21">pro.h:Th</ta>
            <ta e="T25" id="Seg_640" s="T24">np:Th 0.3.h:Poss</ta>
            <ta e="T28" id="Seg_641" s="T27">np:Th 0.3.h:Poss</ta>
            <ta e="T30" id="Seg_642" s="T29">pro.h:Th</ta>
            <ta e="T31" id="Seg_643" s="T30">np:Poss</ta>
            <ta e="T32" id="Seg_644" s="T31">np:L</ta>
            <ta e="T36" id="Seg_645" s="T35">pro.h:Th</ta>
            <ta e="T40" id="Seg_646" s="T39">np.h:Th</ta>
            <ta e="T42" id="Seg_647" s="T41">0.1.h:E</ta>
            <ta e="T44" id="Seg_648" s="T43">np.h:Th</ta>
            <ta e="T49" id="Seg_649" s="T48">np:Th 0.3.h:Poss</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_650" s="T3">pro.h:S</ta>
            <ta e="T6" id="Seg_651" s="T5">v:pred</ta>
            <ta e="T7" id="Seg_652" s="T6">np.h:O</ta>
            <ta e="T10" id="Seg_653" s="T9">0.1.h:S v:pred</ta>
            <ta e="T11" id="Seg_654" s="T10">0.1.h:S v:pred</ta>
            <ta e="T12" id="Seg_655" s="T11">0.2.h:S v:pred</ta>
            <ta e="T19" id="Seg_656" s="T18">0.3.h:S v:pred</ta>
            <ta e="T22" id="Seg_657" s="T21">pro.h:S</ta>
            <ta e="T24" id="Seg_658" s="T23">v:pred</ta>
            <ta e="T25" id="Seg_659" s="T24">np:S</ta>
            <ta e="T26" id="Seg_660" s="T25">adj:pred</ta>
            <ta e="T28" id="Seg_661" s="T27">np:S</ta>
            <ta e="T29" id="Seg_662" s="T28">adj:pred</ta>
            <ta e="T30" id="Seg_663" s="T29">pro.h:S</ta>
            <ta e="T33" id="Seg_664" s="T32">v:pred</ta>
            <ta e="T36" id="Seg_665" s="T35">pro.h:S</ta>
            <ta e="T37" id="Seg_666" s="T36">n:pred</ta>
            <ta e="T38" id="Seg_667" s="T37">cop</ta>
            <ta e="T40" id="Seg_668" s="T39">np.h:O</ta>
            <ta e="T42" id="Seg_669" s="T41">0.1.h:S v:pred</ta>
            <ta e="T44" id="Seg_670" s="T43">np.h:S</ta>
            <ta e="T47" id="Seg_671" s="T46">v:pred</ta>
            <ta e="T49" id="Seg_672" s="T48">np:S</ta>
            <ta e="T50" id="Seg_673" s="T49">adj:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_674" s="T1">RUS:cult</ta>
            <ta e="T3" id="Seg_675" s="T2">RUS:core</ta>
            <ta e="T5" id="Seg_676" s="T4">RUS:cult</ta>
            <ta e="T8" id="Seg_677" s="T7">RUS:core</ta>
            <ta e="T17" id="Seg_678" s="T16">RUS:gram</ta>
            <ta e="T20" id="Seg_679" s="T19">RUS:disc</ta>
            <ta e="T27" id="Seg_680" s="T26">RUS:gram</ta>
            <ta e="T32" id="Seg_681" s="T31">WNB Noun or pp</ta>
            <ta e="T36" id="Seg_682" s="T35">RUS:core</ta>
            <ta e="T39" id="Seg_683" s="T38">RUS:gram</ta>
            <ta e="T43" id="Seg_684" s="T42">RUS:gram</ta>
            <ta e="T44" id="Seg_685" s="T43">RUS:cult</ta>
            <ta e="T45" id="Seg_686" s="T44">RUS:core</ta>
            <ta e="T48" id="Seg_687" s="T47">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_688" s="T1">In Kolpashevo I saw Tunguses for the first time.</ta>
            <ta e="T13" id="Seg_689" s="T7">I ran into the house very quickly and said: “Run into the street.</ta>
            <ta e="T16" id="Seg_690" s="T13">What for [strange] people.”</ta>
            <ta e="T21" id="Seg_691" s="T16">And they said to me: “Well, Tunguses.”</ta>
            <ta e="T24" id="Seg_692" s="T21">They were black.</ta>
            <ta e="T29" id="Seg_693" s="T24">They had narrow eyes and long hair.</ta>
            <ta e="T33" id="Seg_694" s="T29">They were sitting in a boat.</ta>
            <ta e="T38" id="Seg_695" s="T33">Five persons, all of them were men.</ta>
            <ta e="T42" id="Seg_696" s="T38">I didn't see women.</ta>
            <ta e="T47" id="Seg_697" s="T42">And Gypsies are also black.</ta>
            <ta e="T50" id="Seg_698" s="T47">But they have big eyes.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_699" s="T1">In Kolpashevo sah ich zum ersten Mal Tungusier.</ta>
            <ta e="T13" id="Seg_700" s="T7">Ich rannte schnell ins Haus und sagte: "Kommt raus auf die Straße.</ta>
            <ta e="T16" id="Seg_701" s="T13">Was für [seltsame] Leute."</ta>
            <ta e="T21" id="Seg_702" s="T16">Und sie sagten mir: "Nun ja, Tungusen."</ta>
            <ta e="T24" id="Seg_703" s="T21">Sie waren schwarz.</ta>
            <ta e="T29" id="Seg_704" s="T24">Sie hatten schmale Augen und langes Haar.</ta>
            <ta e="T33" id="Seg_705" s="T29">Sie saßen in einem Boot.</ta>
            <ta e="T38" id="Seg_706" s="T33">Fünf Personen, alle von ihnen waren Männer.</ta>
            <ta e="T42" id="Seg_707" s="T38">Ich sah keine Frauen.</ta>
            <ta e="T47" id="Seg_708" s="T42">Und Zigeuner sind auch schwarz.</ta>
            <ta e="T50" id="Seg_709" s="T47">Aber ihre Augen sind groß.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_710" s="T1">Первый раз я видела в Колпашево тунгусов.</ta>
            <ta e="T13" id="Seg_711" s="T7">Так быстро я в избу забежала, сказала: “Бегите на улицу.</ta>
            <ta e="T16" id="Seg_712" s="T13">Что за такие люди”.</ta>
            <ta e="T21" id="Seg_713" s="T16">А мне сказали: “Ну, тунгусы”.</ta>
            <ta e="T24" id="Seg_714" s="T21">Они черные были.</ta>
            <ta e="T29" id="Seg_715" s="T24">Глаза узенькие, а волосы длинные.</ta>
            <ta e="T33" id="Seg_716" s="T29">Они на обласке сидели.</ta>
            <ta e="T38" id="Seg_717" s="T33">Пять человек все мужики были.</ta>
            <ta e="T42" id="Seg_718" s="T38">А женщин я не видела.</ta>
            <ta e="T47" id="Seg_719" s="T42">А цыгане тоже черные.</ta>
            <ta e="T50" id="Seg_720" s="T47">Но у них глаза большие.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_721" s="T1">первый раз я видала в Колпашево тунгусов</ta>
            <ta e="T13" id="Seg_722" s="T7">я в избу забежала сказала идите на улицу (скорее)</ta>
            <ta e="T16" id="Seg_723" s="T13">какие люди</ta>
            <ta e="T21" id="Seg_724" s="T16">а мне сказали «тунгусы»</ta>
            <ta e="T24" id="Seg_725" s="T21">они черные были</ta>
            <ta e="T29" id="Seg_726" s="T24">глаза узенькие а волосы длинные</ta>
            <ta e="T33" id="Seg_727" s="T29">они на обласке сидели</ta>
            <ta e="T38" id="Seg_728" s="T33">пять человек все мужики были</ta>
            <ta e="T42" id="Seg_729" s="T38">а женщин я не видела</ta>
            <ta e="T47" id="Seg_730" s="T42">цыгане тоже черные</ta>
            <ta e="T50" id="Seg_731" s="T47">но у них глаза большие</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T13" id="Seg_732" s="T7">[BrM:] 'Da tawa' changed to 'Datawa'.</ta>
            <ta e="T21" id="Seg_733" s="T16">[KuAI:] Variant: 'kəlɨlla'.</ta>
            <ta e="T38" id="Seg_734" s="T33">[KuAI:] Variant: 'ɣum'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
