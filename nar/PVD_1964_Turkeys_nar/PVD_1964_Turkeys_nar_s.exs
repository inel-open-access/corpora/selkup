<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Turkeys_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Turkeys_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">75</ud-information>
            <ud-information attribute-name="# HIAT:w">52</ud-information>
            <ud-information attribute-name="# e">52</ud-information>
            <ud-information attribute-name="# HIAT:u">12</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T53" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Me</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">tʼüːwaj</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">Taxtamɨšowat</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Natʼen</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">me</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">ilɨzaj</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_24" n="HIAT:ip">(</nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">warkɨzaj</ts>
                  <nts id="Seg_27" n="HIAT:ip">)</nts>
                  <nts id="Seg_28" n="HIAT:ip">.</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_31" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">Täp</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">üdə</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_39" n="HIAT:w" s="T10">tutdɨkkus</ts>
                  <nts id="Seg_40" n="HIAT:ip">.</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_43" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_45" n="HIAT:w" s="T11">A</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">man</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">tʼäran</ts>
                  <nts id="Seg_52" n="HIAT:ip">:</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_54" n="HIAT:ip">“</nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">Tamdʼel</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">üt</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">tuɣulǯəǯan</ts>
                  <nts id="Seg_63" n="HIAT:ip">.</nts>
                  <nts id="Seg_64" n="HIAT:ip">”</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_67" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_69" n="HIAT:w" s="T17">A</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_72" n="HIAT:w" s="T18">täp</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">mekga</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">tʼärɨn</ts>
                  <nts id="Seg_79" n="HIAT:ip">:</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_81" n="HIAT:ip">“</nts>
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">Üdno</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">na</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_89" n="HIAT:w" s="T23">qwannaš</ts>
                  <nts id="Seg_90" n="HIAT:ip">,</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_93" n="HIAT:w" s="T24">igə</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_96" n="HIAT:w" s="T25">kocʼiwatkə</ts>
                  <nts id="Seg_97" n="HIAT:ip">.</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_100" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_102" n="HIAT:w" s="T26">Natʼen</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_105" n="HIAT:w" s="T27">indʼügla</ts>
                  <nts id="Seg_106" n="HIAT:ip">.</nts>
                  <nts id="Seg_107" n="HIAT:ip">”</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_110" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_112" n="HIAT:w" s="T28">Man</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_115" n="HIAT:w" s="T29">na</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_118" n="HIAT:w" s="T30">qwatdan</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_121" n="HIAT:w" s="T31">üdno</ts>
                  <nts id="Seg_122" n="HIAT:ip">.</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_125" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_127" n="HIAT:w" s="T32">Tebla</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_130" n="HIAT:w" s="T33">natʼen</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_133" n="HIAT:w" s="T34">paldʼät</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_137" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_139" n="HIAT:w" s="T35">Šäɣan</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_142" n="HIAT:w" s="T36">jewattə</ts>
                  <nts id="Seg_143" n="HIAT:ip">,</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_146" n="HIAT:w" s="T37">iːbɨɣan</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_149" n="HIAT:w" s="T38">jewattə</ts>
                  <nts id="Seg_150" n="HIAT:ip">.</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_153" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_155" n="HIAT:w" s="T39">Man</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_158" n="HIAT:w" s="T40">bɨ</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_161" n="HIAT:w" s="T41">kəcʼiwannän</ts>
                  <nts id="Seg_162" n="HIAT:ip">,</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_165" n="HIAT:w" s="T42">jeʒlʼe</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_168" n="HIAT:w" s="T43">Valodʼä</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_171" n="HIAT:w" s="T44">mekga</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_174" n="HIAT:w" s="T45">ass</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_177" n="HIAT:w" s="T46">kʼennät</ts>
                  <nts id="Seg_178" n="HIAT:ip">.</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_181" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_183" n="HIAT:w" s="T47">Kəcʼewatku</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_186" n="HIAT:w" s="T48">ne</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_189" n="HIAT:w" s="T49">nadə</ts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_193" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_195" n="HIAT:w" s="T50">Nadə</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_198" n="HIAT:w" s="T51">üdə</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_201" n="HIAT:w" s="T52">tutdɨgu</ts>
                  <nts id="Seg_202" n="HIAT:ip">.</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T53" id="Seg_204" n="sc" s="T1">
               <ts e="T2" id="Seg_206" n="e" s="T1">Me </ts>
               <ts e="T3" id="Seg_208" n="e" s="T2">tʼüːwaj </ts>
               <ts e="T4" id="Seg_210" n="e" s="T3">Taxtamɨšowat. </ts>
               <ts e="T5" id="Seg_212" n="e" s="T4">Natʼen </ts>
               <ts e="T6" id="Seg_214" n="e" s="T5">me </ts>
               <ts e="T7" id="Seg_216" n="e" s="T6">ilɨzaj </ts>
               <ts e="T8" id="Seg_218" n="e" s="T7">(warkɨzaj). </ts>
               <ts e="T9" id="Seg_220" n="e" s="T8">Täp </ts>
               <ts e="T10" id="Seg_222" n="e" s="T9">üdə </ts>
               <ts e="T11" id="Seg_224" n="e" s="T10">tutdɨkkus. </ts>
               <ts e="T12" id="Seg_226" n="e" s="T11">A </ts>
               <ts e="T13" id="Seg_228" n="e" s="T12">man </ts>
               <ts e="T14" id="Seg_230" n="e" s="T13">tʼäran: </ts>
               <ts e="T15" id="Seg_232" n="e" s="T14">“Tamdʼel </ts>
               <ts e="T16" id="Seg_234" n="e" s="T15">üt </ts>
               <ts e="T17" id="Seg_236" n="e" s="T16">tuɣulǯəǯan.” </ts>
               <ts e="T18" id="Seg_238" n="e" s="T17">A </ts>
               <ts e="T19" id="Seg_240" n="e" s="T18">täp </ts>
               <ts e="T20" id="Seg_242" n="e" s="T19">mekga </ts>
               <ts e="T21" id="Seg_244" n="e" s="T20">tʼärɨn: </ts>
               <ts e="T22" id="Seg_246" n="e" s="T21">“Üdno </ts>
               <ts e="T23" id="Seg_248" n="e" s="T22">na </ts>
               <ts e="T24" id="Seg_250" n="e" s="T23">qwannaš, </ts>
               <ts e="T25" id="Seg_252" n="e" s="T24">igə </ts>
               <ts e="T26" id="Seg_254" n="e" s="T25">kocʼiwatkə. </ts>
               <ts e="T27" id="Seg_256" n="e" s="T26">Natʼen </ts>
               <ts e="T28" id="Seg_258" n="e" s="T27">indʼügla.” </ts>
               <ts e="T29" id="Seg_260" n="e" s="T28">Man </ts>
               <ts e="T30" id="Seg_262" n="e" s="T29">na </ts>
               <ts e="T31" id="Seg_264" n="e" s="T30">qwatdan </ts>
               <ts e="T32" id="Seg_266" n="e" s="T31">üdno. </ts>
               <ts e="T33" id="Seg_268" n="e" s="T32">Tebla </ts>
               <ts e="T34" id="Seg_270" n="e" s="T33">natʼen </ts>
               <ts e="T35" id="Seg_272" n="e" s="T34">paldʼät. </ts>
               <ts e="T36" id="Seg_274" n="e" s="T35">Šäɣan </ts>
               <ts e="T37" id="Seg_276" n="e" s="T36">jewattə, </ts>
               <ts e="T38" id="Seg_278" n="e" s="T37">iːbɨɣan </ts>
               <ts e="T39" id="Seg_280" n="e" s="T38">jewattə. </ts>
               <ts e="T40" id="Seg_282" n="e" s="T39">Man </ts>
               <ts e="T41" id="Seg_284" n="e" s="T40">bɨ </ts>
               <ts e="T42" id="Seg_286" n="e" s="T41">kəcʼiwannän, </ts>
               <ts e="T43" id="Seg_288" n="e" s="T42">jeʒlʼe </ts>
               <ts e="T44" id="Seg_290" n="e" s="T43">Valodʼä </ts>
               <ts e="T45" id="Seg_292" n="e" s="T44">mekga </ts>
               <ts e="T46" id="Seg_294" n="e" s="T45">ass </ts>
               <ts e="T47" id="Seg_296" n="e" s="T46">kʼennät. </ts>
               <ts e="T48" id="Seg_298" n="e" s="T47">Kəcʼewatku </ts>
               <ts e="T49" id="Seg_300" n="e" s="T48">ne </ts>
               <ts e="T50" id="Seg_302" n="e" s="T49">nadə. </ts>
               <ts e="T51" id="Seg_304" n="e" s="T50">Nadə </ts>
               <ts e="T52" id="Seg_306" n="e" s="T51">üdə </ts>
               <ts e="T53" id="Seg_308" n="e" s="T52">tutdɨgu. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_309" s="T1">PVD_1964_Turkeys_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_310" s="T4">PVD_1964_Turkeys_nar.002 (001.002)</ta>
            <ta e="T11" id="Seg_311" s="T8">PVD_1964_Turkeys_nar.003 (001.003)</ta>
            <ta e="T17" id="Seg_312" s="T11">PVD_1964_Turkeys_nar.004 (001.004)</ta>
            <ta e="T26" id="Seg_313" s="T17">PVD_1964_Turkeys_nar.005 (001.005)</ta>
            <ta e="T28" id="Seg_314" s="T26">PVD_1964_Turkeys_nar.006 (001.006)</ta>
            <ta e="T32" id="Seg_315" s="T28">PVD_1964_Turkeys_nar.007 (001.007)</ta>
            <ta e="T35" id="Seg_316" s="T32">PVD_1964_Turkeys_nar.008 (001.008)</ta>
            <ta e="T39" id="Seg_317" s="T35">PVD_1964_Turkeys_nar.009 (001.009)</ta>
            <ta e="T47" id="Seg_318" s="T39">PVD_1964_Turkeys_nar.010 (001.010)</ta>
            <ta e="T50" id="Seg_319" s="T47">PVD_1964_Turkeys_nar.011 (001.011)</ta>
            <ta e="T53" id="Seg_320" s="T50">PVD_1964_Turkeys_nar.012 (001.012)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_321" s="T1">ме ′тʼӱ̄wай Тахта′мышоват.</ta>
            <ta e="T8" id="Seg_322" s="T4">на′тʼен ме ′илызай (′варкызай).</ta>
            <ta e="T11" id="Seg_323" s="T8">тӓп ′ӱдъ ′т(д̂)утдыккус.</ta>
            <ta e="T17" id="Seg_324" s="T11">а ман тʼӓ′ран: там′дʼел ′ӱт ′туɣуlджъджан.</ta>
            <ta e="T26" id="Seg_325" s="T17">а тӓп ′мекг̂а тʼӓ′рын: ӱд′но на kwан′наш, игъ ко(ӓ)цʼи′ваткъ.</ta>
            <ta e="T28" id="Seg_326" s="T26">на′тʼен ин′дʼӱгла.</ta>
            <ta e="T32" id="Seg_327" s="T28">ман на kwат′дан ӱд′но.</ta>
            <ta e="T35" id="Seg_328" s="T32">теб′ла на′тʼен паl′дʼӓт.</ta>
            <ta e="T39" id="Seg_329" s="T35">′шӓɣан jеwаттъ, ′ӣбыɣан ′jеwаттъ.</ta>
            <ta e="T47" id="Seg_330" s="T39">ман бы къ(ӓ)цʼи′ваннӓн, ′jежлʼе Ва′лодʼӓ ′мекга асс кʼе′ннӓт.</ta>
            <ta e="T50" id="Seg_331" s="T47">къ(ӓ)цʼе′ватку не ′надъ.</ta>
            <ta e="T53" id="Seg_332" s="T50">надъ ӱдъ ′тутдыгу.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_333" s="T1">me tʼüːwaj Тaxtamɨšowat.</ta>
            <ta e="T8" id="Seg_334" s="T4">natʼen me ilɨzaj (warkɨzaj).</ta>
            <ta e="T11" id="Seg_335" s="T8">täp üdə t(d̂)utdɨkkus.</ta>
            <ta e="T17" id="Seg_336" s="T11">a man tʼäran: tamdʼel üt tuɣulǯəǯan.</ta>
            <ta e="T26" id="Seg_337" s="T17">a täp mekĝa tʼärɨn: üdno na qwannaš, igə ko(ä)cʼiwatkə.</ta>
            <ta e="T28" id="Seg_338" s="T26">natʼen indʼügla.</ta>
            <ta e="T32" id="Seg_339" s="T28">man na qwatdan üdno.</ta>
            <ta e="T35" id="Seg_340" s="T32">tebla natʼen paldʼät.</ta>
            <ta e="T39" id="Seg_341" s="T35">šäɣan jewattə, iːbɨɣan jewattə.</ta>
            <ta e="T47" id="Seg_342" s="T39">man bɨ kə(ä)cʼiwannän, jeʒlʼe Вalodʼä mekga ass kʼennät.</ta>
            <ta e="T50" id="Seg_343" s="T47">kə(ä)cʼewatku ne nadə.</ta>
            <ta e="T53" id="Seg_344" s="T50">nadə üdə tutdɨgu.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_345" s="T1">Me tʼüːwaj Taxtamɨšowat. </ta>
            <ta e="T8" id="Seg_346" s="T4">Natʼen me ilɨzaj (warkɨzaj). </ta>
            <ta e="T11" id="Seg_347" s="T8">Täp üdə tutdɨkkus. </ta>
            <ta e="T17" id="Seg_348" s="T11">A man tʼäran: “Tamdʼel üt tuɣulǯəǯan.” </ta>
            <ta e="T26" id="Seg_349" s="T17">A täp mekga tʼärɨn: “Üdno na qwannaš, igə kocʼiwatkə. </ta>
            <ta e="T28" id="Seg_350" s="T26">Natʼen indʼügla.” </ta>
            <ta e="T32" id="Seg_351" s="T28">Man na qwatdan üdno. </ta>
            <ta e="T35" id="Seg_352" s="T32">Tebla natʼen paldʼät. </ta>
            <ta e="T39" id="Seg_353" s="T35">Šäɣan jewattə, iːbɨɣan jewattə. </ta>
            <ta e="T47" id="Seg_354" s="T39">Man bɨ kəcʼiwannän, jeʒlʼe Valodʼä mekga ass kʼennät. </ta>
            <ta e="T50" id="Seg_355" s="T47">Kəcʼewatku ne nadə. </ta>
            <ta e="T53" id="Seg_356" s="T50">Nadə üdə tutdɨgu. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_357" s="T1">me</ta>
            <ta e="T3" id="Seg_358" s="T2">tʼüː-wa-j</ta>
            <ta e="T4" id="Seg_359" s="T3">Taxtamɨšowa-t</ta>
            <ta e="T5" id="Seg_360" s="T4">natʼe-n</ta>
            <ta e="T6" id="Seg_361" s="T5">me</ta>
            <ta e="T7" id="Seg_362" s="T6">ilɨ-za-j</ta>
            <ta e="T8" id="Seg_363" s="T7">warkɨ-za-j</ta>
            <ta e="T9" id="Seg_364" s="T8">täp</ta>
            <ta e="T10" id="Seg_365" s="T9">üdə</ta>
            <ta e="T11" id="Seg_366" s="T10">tutdɨ-kku-s</ta>
            <ta e="T12" id="Seg_367" s="T11">a</ta>
            <ta e="T13" id="Seg_368" s="T12">man</ta>
            <ta e="T14" id="Seg_369" s="T13">tʼära-n</ta>
            <ta e="T15" id="Seg_370" s="T14">tam-dʼel</ta>
            <ta e="T16" id="Seg_371" s="T15">üt</ta>
            <ta e="T17" id="Seg_372" s="T16">tuɣul-ǯ-əǯa-n</ta>
            <ta e="T18" id="Seg_373" s="T17">a</ta>
            <ta e="T19" id="Seg_374" s="T18">täp</ta>
            <ta e="T20" id="Seg_375" s="T19">mekga</ta>
            <ta e="T21" id="Seg_376" s="T20">tʼärɨ-n</ta>
            <ta e="T22" id="Seg_377" s="T21">üd-no</ta>
            <ta e="T23" id="Seg_378" s="T22">na</ta>
            <ta e="T24" id="Seg_379" s="T23">qwan-naš</ta>
            <ta e="T25" id="Seg_380" s="T24">igə</ta>
            <ta e="T26" id="Seg_381" s="T25">kocʼiwat-kə</ta>
            <ta e="T27" id="Seg_382" s="T26">natʼe-n</ta>
            <ta e="T28" id="Seg_383" s="T27">indʼüg-la</ta>
            <ta e="T29" id="Seg_384" s="T28">man</ta>
            <ta e="T30" id="Seg_385" s="T29">na</ta>
            <ta e="T31" id="Seg_386" s="T30">qwa-tda-n</ta>
            <ta e="T32" id="Seg_387" s="T31">üd-no</ta>
            <ta e="T33" id="Seg_388" s="T32">teb-la</ta>
            <ta e="T34" id="Seg_389" s="T33">natʼe-n</ta>
            <ta e="T35" id="Seg_390" s="T34">paldʼä-t</ta>
            <ta e="T36" id="Seg_391" s="T35">šäɣa-n</ta>
            <ta e="T37" id="Seg_392" s="T36">je-wa-ttə</ta>
            <ta e="T38" id="Seg_393" s="T37">iːbɨɣa-n</ta>
            <ta e="T39" id="Seg_394" s="T38">je-wa-ttə</ta>
            <ta e="T40" id="Seg_395" s="T39">man</ta>
            <ta e="T41" id="Seg_396" s="T40">bɨ</ta>
            <ta e="T42" id="Seg_397" s="T41">kəcʼiwan-nä-n</ta>
            <ta e="T43" id="Seg_398" s="T42">jeʒlʼe</ta>
            <ta e="T44" id="Seg_399" s="T43">Valodʼä</ta>
            <ta e="T45" id="Seg_400" s="T44">mekga</ta>
            <ta e="T46" id="Seg_401" s="T45">ass</ta>
            <ta e="T47" id="Seg_402" s="T46">kʼen-nä-t</ta>
            <ta e="T48" id="Seg_403" s="T47">kəcʼewat-ku</ta>
            <ta e="T49" id="Seg_404" s="T48">ne</ta>
            <ta e="T50" id="Seg_405" s="T49">nadə</ta>
            <ta e="T51" id="Seg_406" s="T50">nadə</ta>
            <ta e="T52" id="Seg_407" s="T51">üdə</ta>
            <ta e="T53" id="Seg_408" s="T52">tutdɨ-gu</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_409" s="T1">me</ta>
            <ta e="T3" id="Seg_410" s="T2">tüː-nɨ-j</ta>
            <ta e="T4" id="Seg_411" s="T3">Taxtamɨšowa-ntə</ta>
            <ta e="T5" id="Seg_412" s="T4">*natʼe-n</ta>
            <ta e="T6" id="Seg_413" s="T5">me</ta>
            <ta e="T7" id="Seg_414" s="T6">elɨ-sɨ-j</ta>
            <ta e="T8" id="Seg_415" s="T7">warkɨ-sɨ-j</ta>
            <ta e="T9" id="Seg_416" s="T8">täp</ta>
            <ta e="T10" id="Seg_417" s="T9">üt</ta>
            <ta e="T11" id="Seg_418" s="T10">tundɨ-ku-sɨ</ta>
            <ta e="T12" id="Seg_419" s="T11">a</ta>
            <ta e="T13" id="Seg_420" s="T12">man</ta>
            <ta e="T14" id="Seg_421" s="T13">tʼärɨ-ŋ</ta>
            <ta e="T15" id="Seg_422" s="T14">taw-dʼel</ta>
            <ta e="T16" id="Seg_423" s="T15">üt</ta>
            <ta e="T17" id="Seg_424" s="T16">tugul-ǯə-enǯɨ-ŋ</ta>
            <ta e="T18" id="Seg_425" s="T17">a</ta>
            <ta e="T19" id="Seg_426" s="T18">täp</ta>
            <ta e="T20" id="Seg_427" s="T19">mekka</ta>
            <ta e="T21" id="Seg_428" s="T20">tʼärɨ-n</ta>
            <ta e="T22" id="Seg_429" s="T21">üt-no</ta>
            <ta e="T23" id="Seg_430" s="T22">na</ta>
            <ta e="T24" id="Seg_431" s="T23">qwan-naš</ta>
            <ta e="T25" id="Seg_432" s="T24">igə</ta>
            <ta e="T26" id="Seg_433" s="T25">kɨcʼwat-kɨ</ta>
            <ta e="T27" id="Seg_434" s="T26">*natʼe-n</ta>
            <ta e="T28" id="Seg_435" s="T27">indʼük-la</ta>
            <ta e="T29" id="Seg_436" s="T28">man</ta>
            <ta e="T30" id="Seg_437" s="T29">na</ta>
            <ta e="T31" id="Seg_438" s="T30">qwan-ntɨ-ŋ</ta>
            <ta e="T32" id="Seg_439" s="T31">üt-no</ta>
            <ta e="T33" id="Seg_440" s="T32">täp-la</ta>
            <ta e="T34" id="Seg_441" s="T33">*natʼe-n</ta>
            <ta e="T35" id="Seg_442" s="T34">palʼdʼi-tɨn</ta>
            <ta e="T36" id="Seg_443" s="T35">šeːɣa-ŋ</ta>
            <ta e="T37" id="Seg_444" s="T36">eː-nɨ-tɨn</ta>
            <ta e="T38" id="Seg_445" s="T37">iːbəgaj-ŋ</ta>
            <ta e="T39" id="Seg_446" s="T38">eː-nɨ-tɨn</ta>
            <ta e="T40" id="Seg_447" s="T39">man</ta>
            <ta e="T41" id="Seg_448" s="T40">bɨ</ta>
            <ta e="T42" id="Seg_449" s="T41">kɨcʼwat-ne-ŋ</ta>
            <ta e="T43" id="Seg_450" s="T42">jeʒlʼe</ta>
            <ta e="T44" id="Seg_451" s="T43">Wolodʼa</ta>
            <ta e="T45" id="Seg_452" s="T44">mekka</ta>
            <ta e="T46" id="Seg_453" s="T45">asa</ta>
            <ta e="T47" id="Seg_454" s="T46">ket-ne-t</ta>
            <ta e="T48" id="Seg_455" s="T47">kɨcʼwat-gu</ta>
            <ta e="T49" id="Seg_456" s="T48">ne</ta>
            <ta e="T50" id="Seg_457" s="T49">nadə</ta>
            <ta e="T51" id="Seg_458" s="T50">nadə</ta>
            <ta e="T52" id="Seg_459" s="T51">üt</ta>
            <ta e="T53" id="Seg_460" s="T52">tundɨ-gu</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_461" s="T1">we.[NOM]</ta>
            <ta e="T3" id="Seg_462" s="T2">come-CO-1DU</ta>
            <ta e="T4" id="Seg_463" s="T3">Taxtamyshevo-ILL</ta>
            <ta e="T5" id="Seg_464" s="T4">there-ADV.LOC</ta>
            <ta e="T6" id="Seg_465" s="T5">we.[NOM]</ta>
            <ta e="T7" id="Seg_466" s="T6">live-PST-1DU</ta>
            <ta e="T8" id="Seg_467" s="T7">live-PST-1DU</ta>
            <ta e="T9" id="Seg_468" s="T8">(s)he.[NOM]</ta>
            <ta e="T10" id="Seg_469" s="T9">water.[NOM]</ta>
            <ta e="T11" id="Seg_470" s="T10">carry-HAB-PST.[3SG.S]</ta>
            <ta e="T12" id="Seg_471" s="T11">and</ta>
            <ta e="T13" id="Seg_472" s="T12">I.NOM</ta>
            <ta e="T14" id="Seg_473" s="T13">say-1SG.S</ta>
            <ta e="T15" id="Seg_474" s="T14">this-day.[NOM]</ta>
            <ta e="T16" id="Seg_475" s="T15">water.[NOM]</ta>
            <ta e="T17" id="Seg_476" s="T16">pull.out-DRV-FUT-1SG.S</ta>
            <ta e="T18" id="Seg_477" s="T17">and</ta>
            <ta e="T19" id="Seg_478" s="T18">(s)he.[NOM]</ta>
            <ta e="T20" id="Seg_479" s="T19">I.ALL</ta>
            <ta e="T21" id="Seg_480" s="T20">say-3SG.S</ta>
            <ta e="T22" id="Seg_481" s="T21">water-TRL</ta>
            <ta e="T23" id="Seg_482" s="T22">here</ta>
            <ta e="T24" id="Seg_483" s="T23">leave-POT.FUT.2SG</ta>
            <ta e="T25" id="Seg_484" s="T24">NEG.IMP</ta>
            <ta e="T26" id="Seg_485" s="T25">get.scared-IMP.2SG.S</ta>
            <ta e="T27" id="Seg_486" s="T26">there-ADV.LOC</ta>
            <ta e="T28" id="Seg_487" s="T27">turkey.cock-PL.[NOM]</ta>
            <ta e="T29" id="Seg_488" s="T28">I.NOM</ta>
            <ta e="T30" id="Seg_489" s="T29">here</ta>
            <ta e="T31" id="Seg_490" s="T30">leave-INFER-1SG.S</ta>
            <ta e="T32" id="Seg_491" s="T31">water-TRL</ta>
            <ta e="T33" id="Seg_492" s="T32">(s)he-PL.[NOM]</ta>
            <ta e="T34" id="Seg_493" s="T33">there-ADV.LOC</ta>
            <ta e="T35" id="Seg_494" s="T34">walk-3PL</ta>
            <ta e="T36" id="Seg_495" s="T35">black-ADVZ</ta>
            <ta e="T37" id="Seg_496" s="T36">be-CO-3PL</ta>
            <ta e="T38" id="Seg_497" s="T37">big-ADVZ</ta>
            <ta e="T39" id="Seg_498" s="T38">be-CO-3PL</ta>
            <ta e="T40" id="Seg_499" s="T39">I.NOM</ta>
            <ta e="T41" id="Seg_500" s="T40">IRREAL</ta>
            <ta e="T42" id="Seg_501" s="T41">get.scared-CONJ-1SG.S</ta>
            <ta e="T43" id="Seg_502" s="T42">if</ta>
            <ta e="T44" id="Seg_503" s="T43">Volodya.[NOM]</ta>
            <ta e="T45" id="Seg_504" s="T44">I.ALL</ta>
            <ta e="T46" id="Seg_505" s="T45">NEG</ta>
            <ta e="T47" id="Seg_506" s="T46">say-CONJ-3SG.O</ta>
            <ta e="T48" id="Seg_507" s="T47">get.scared-INF</ta>
            <ta e="T49" id="Seg_508" s="T48">NEG</ta>
            <ta e="T50" id="Seg_509" s="T49">one.should</ta>
            <ta e="T51" id="Seg_510" s="T50">one.should</ta>
            <ta e="T52" id="Seg_511" s="T51">water.[NOM]</ta>
            <ta e="T53" id="Seg_512" s="T52">carry-INF</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_513" s="T1">мы.[NOM]</ta>
            <ta e="T3" id="Seg_514" s="T2">приехать-CO-1DU</ta>
            <ta e="T4" id="Seg_515" s="T3">Тахтамышево-ILL</ta>
            <ta e="T5" id="Seg_516" s="T4">туда-ADV.LOC</ta>
            <ta e="T6" id="Seg_517" s="T5">мы.[NOM]</ta>
            <ta e="T7" id="Seg_518" s="T6">жить-PST-1DU</ta>
            <ta e="T8" id="Seg_519" s="T7">жить-PST-1DU</ta>
            <ta e="T9" id="Seg_520" s="T8">он(а).[NOM]</ta>
            <ta e="T10" id="Seg_521" s="T9">вода.[NOM]</ta>
            <ta e="T11" id="Seg_522" s="T10">таскать-HAB-PST.[3SG.S]</ta>
            <ta e="T12" id="Seg_523" s="T11">а</ta>
            <ta e="T13" id="Seg_524" s="T12">я.NOM</ta>
            <ta e="T14" id="Seg_525" s="T13">сказать-1SG.S</ta>
            <ta e="T15" id="Seg_526" s="T14">этот-день.[NOM]</ta>
            <ta e="T16" id="Seg_527" s="T15">вода.[NOM]</ta>
            <ta e="T17" id="Seg_528" s="T16">вытащить-DRV-FUT-1SG.S</ta>
            <ta e="T18" id="Seg_529" s="T17">а</ta>
            <ta e="T19" id="Seg_530" s="T18">он(а).[NOM]</ta>
            <ta e="T20" id="Seg_531" s="T19">я.ALL</ta>
            <ta e="T21" id="Seg_532" s="T20">сказать-3SG.S</ta>
            <ta e="T22" id="Seg_533" s="T21">вода-TRL</ta>
            <ta e="T23" id="Seg_534" s="T22">ну</ta>
            <ta e="T24" id="Seg_535" s="T23">отправиться-POT.FUT.2SG</ta>
            <ta e="T25" id="Seg_536" s="T24">NEG.IMP</ta>
            <ta e="T26" id="Seg_537" s="T25">испугаться-IMP.2SG.S</ta>
            <ta e="T27" id="Seg_538" s="T26">туда-ADV.LOC</ta>
            <ta e="T28" id="Seg_539" s="T27">индюк-PL.[NOM]</ta>
            <ta e="T29" id="Seg_540" s="T28">я.NOM</ta>
            <ta e="T30" id="Seg_541" s="T29">ну</ta>
            <ta e="T31" id="Seg_542" s="T30">отправиться-INFER-1SG.S</ta>
            <ta e="T32" id="Seg_543" s="T31">вода-TRL</ta>
            <ta e="T33" id="Seg_544" s="T32">он(а)-PL.[NOM]</ta>
            <ta e="T34" id="Seg_545" s="T33">туда-ADV.LOC</ta>
            <ta e="T35" id="Seg_546" s="T34">ходить-3PL</ta>
            <ta e="T36" id="Seg_547" s="T35">чёрный-ADVZ</ta>
            <ta e="T37" id="Seg_548" s="T36">быть-CO-3PL</ta>
            <ta e="T38" id="Seg_549" s="T37">большой-ADVZ</ta>
            <ta e="T39" id="Seg_550" s="T38">быть-CO-3PL</ta>
            <ta e="T40" id="Seg_551" s="T39">я.NOM</ta>
            <ta e="T41" id="Seg_552" s="T40">IRREAL</ta>
            <ta e="T42" id="Seg_553" s="T41">испугаться-CONJ-1SG.S</ta>
            <ta e="T43" id="Seg_554" s="T42">ежели</ta>
            <ta e="T44" id="Seg_555" s="T43">Володя.[NOM]</ta>
            <ta e="T45" id="Seg_556" s="T44">я.ALL</ta>
            <ta e="T46" id="Seg_557" s="T45">NEG</ta>
            <ta e="T47" id="Seg_558" s="T46">сказать-CONJ-3SG.O</ta>
            <ta e="T48" id="Seg_559" s="T47">испугаться-INF</ta>
            <ta e="T49" id="Seg_560" s="T48">NEG</ta>
            <ta e="T50" id="Seg_561" s="T49">надо</ta>
            <ta e="T51" id="Seg_562" s="T50">надо</ta>
            <ta e="T52" id="Seg_563" s="T51">вода.[NOM]</ta>
            <ta e="T53" id="Seg_564" s="T52">таскать-INF</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_565" s="T1">pers.[n:case]</ta>
            <ta e="T3" id="Seg_566" s="T2">v-v:ins-v:pn</ta>
            <ta e="T4" id="Seg_567" s="T3">nprop-n:case</ta>
            <ta e="T5" id="Seg_568" s="T4">adv-adv:case</ta>
            <ta e="T6" id="Seg_569" s="T5">pers.[n:case]</ta>
            <ta e="T7" id="Seg_570" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_571" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_572" s="T8">pers.[n:case]</ta>
            <ta e="T10" id="Seg_573" s="T9">n.[n:case]</ta>
            <ta e="T11" id="Seg_574" s="T10">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T12" id="Seg_575" s="T11">conj</ta>
            <ta e="T13" id="Seg_576" s="T12">pers</ta>
            <ta e="T14" id="Seg_577" s="T13">v-v:pn</ta>
            <ta e="T15" id="Seg_578" s="T14">dem-n.[n:case]</ta>
            <ta e="T16" id="Seg_579" s="T15">n.[n:case]</ta>
            <ta e="T17" id="Seg_580" s="T16">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_581" s="T17">conj</ta>
            <ta e="T19" id="Seg_582" s="T18">pers.[n:case]</ta>
            <ta e="T20" id="Seg_583" s="T19">pers</ta>
            <ta e="T21" id="Seg_584" s="T20">v-v:pn</ta>
            <ta e="T22" id="Seg_585" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_586" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_587" s="T23">v-v:tense</ta>
            <ta e="T25" id="Seg_588" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_589" s="T25">v-v:mood.pn</ta>
            <ta e="T27" id="Seg_590" s="T26">adv-adv:case</ta>
            <ta e="T28" id="Seg_591" s="T27">n-n:num.[n:case]</ta>
            <ta e="T29" id="Seg_592" s="T28">pers</ta>
            <ta e="T30" id="Seg_593" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_594" s="T30">v-v:mood-v:pn</ta>
            <ta e="T32" id="Seg_595" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_596" s="T32">pers-n:num.[n:case]</ta>
            <ta e="T34" id="Seg_597" s="T33">adv-adv:case</ta>
            <ta e="T35" id="Seg_598" s="T34">v-v:pn</ta>
            <ta e="T36" id="Seg_599" s="T35">adj-adj&gt;adv</ta>
            <ta e="T37" id="Seg_600" s="T36">v-v:ins-v:pn</ta>
            <ta e="T38" id="Seg_601" s="T37">adj-adj&gt;adv</ta>
            <ta e="T39" id="Seg_602" s="T38">v-v:ins-v:pn</ta>
            <ta e="T40" id="Seg_603" s="T39">pers</ta>
            <ta e="T41" id="Seg_604" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_605" s="T41">v-v:mood-v:pn</ta>
            <ta e="T43" id="Seg_606" s="T42">conj</ta>
            <ta e="T44" id="Seg_607" s="T43">nprop.[n:case]</ta>
            <ta e="T45" id="Seg_608" s="T44">pers</ta>
            <ta e="T46" id="Seg_609" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_610" s="T46">v-v:mood-v:pn</ta>
            <ta e="T48" id="Seg_611" s="T47">v-v:inf</ta>
            <ta e="T49" id="Seg_612" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_613" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_614" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_615" s="T51">n.[n:case]</ta>
            <ta e="T53" id="Seg_616" s="T52">v-v:inf</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_617" s="T1">pers</ta>
            <ta e="T3" id="Seg_618" s="T2">v</ta>
            <ta e="T4" id="Seg_619" s="T3">nprop</ta>
            <ta e="T5" id="Seg_620" s="T4">adv</ta>
            <ta e="T6" id="Seg_621" s="T5">pers</ta>
            <ta e="T7" id="Seg_622" s="T6">v</ta>
            <ta e="T8" id="Seg_623" s="T7">v</ta>
            <ta e="T9" id="Seg_624" s="T8">pers</ta>
            <ta e="T10" id="Seg_625" s="T9">n</ta>
            <ta e="T11" id="Seg_626" s="T10">v</ta>
            <ta e="T12" id="Seg_627" s="T11">conj</ta>
            <ta e="T13" id="Seg_628" s="T12">pers</ta>
            <ta e="T14" id="Seg_629" s="T13">v</ta>
            <ta e="T15" id="Seg_630" s="T14">adv</ta>
            <ta e="T16" id="Seg_631" s="T15">n</ta>
            <ta e="T17" id="Seg_632" s="T16">v</ta>
            <ta e="T18" id="Seg_633" s="T17">conj</ta>
            <ta e="T19" id="Seg_634" s="T18">pers</ta>
            <ta e="T20" id="Seg_635" s="T19">pers</ta>
            <ta e="T21" id="Seg_636" s="T20">v</ta>
            <ta e="T22" id="Seg_637" s="T21">n</ta>
            <ta e="T23" id="Seg_638" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_639" s="T23">v</ta>
            <ta e="T25" id="Seg_640" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_641" s="T25">n</ta>
            <ta e="T27" id="Seg_642" s="T26">adv</ta>
            <ta e="T28" id="Seg_643" s="T27">n</ta>
            <ta e="T29" id="Seg_644" s="T28">pers</ta>
            <ta e="T30" id="Seg_645" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_646" s="T30">v</ta>
            <ta e="T32" id="Seg_647" s="T31">n</ta>
            <ta e="T33" id="Seg_648" s="T32">pers</ta>
            <ta e="T34" id="Seg_649" s="T33">adv</ta>
            <ta e="T35" id="Seg_650" s="T34">v</ta>
            <ta e="T36" id="Seg_651" s="T35">adv</ta>
            <ta e="T37" id="Seg_652" s="T36">v</ta>
            <ta e="T38" id="Seg_653" s="T37">adv</ta>
            <ta e="T39" id="Seg_654" s="T38">v</ta>
            <ta e="T40" id="Seg_655" s="T39">pers</ta>
            <ta e="T41" id="Seg_656" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_657" s="T41">v</ta>
            <ta e="T43" id="Seg_658" s="T42">conj</ta>
            <ta e="T44" id="Seg_659" s="T43">nprop</ta>
            <ta e="T45" id="Seg_660" s="T44">pers</ta>
            <ta e="T46" id="Seg_661" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_662" s="T46">v</ta>
            <ta e="T48" id="Seg_663" s="T47">v</ta>
            <ta e="T49" id="Seg_664" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_665" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_666" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_667" s="T51">n</ta>
            <ta e="T53" id="Seg_668" s="T52">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_669" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_670" s="T2">v:pred</ta>
            <ta e="T6" id="Seg_671" s="T5">pro.h:S</ta>
            <ta e="T7" id="Seg_672" s="T6">v:pred</ta>
            <ta e="T9" id="Seg_673" s="T8">pro.h:S</ta>
            <ta e="T10" id="Seg_674" s="T9">np:O</ta>
            <ta e="T11" id="Seg_675" s="T10">v:pred</ta>
            <ta e="T13" id="Seg_676" s="T12">pro.h:S</ta>
            <ta e="T14" id="Seg_677" s="T13">v:pred</ta>
            <ta e="T16" id="Seg_678" s="T15">np:O</ta>
            <ta e="T17" id="Seg_679" s="T16">0.1.h:S v:pred</ta>
            <ta e="T19" id="Seg_680" s="T18">pro.h:S</ta>
            <ta e="T21" id="Seg_681" s="T20">v:pred</ta>
            <ta e="T24" id="Seg_682" s="T23">0.2.h:S v:pred</ta>
            <ta e="T26" id="Seg_683" s="T25">0.2.h:S v:pred</ta>
            <ta e="T28" id="Seg_684" s="T27">np:S</ta>
            <ta e="T29" id="Seg_685" s="T28">pro.h:S</ta>
            <ta e="T31" id="Seg_686" s="T30">v:pred</ta>
            <ta e="T33" id="Seg_687" s="T32">pro:S</ta>
            <ta e="T35" id="Seg_688" s="T34">v:pred</ta>
            <ta e="T37" id="Seg_689" s="T36">0.3:S v:pred</ta>
            <ta e="T39" id="Seg_690" s="T38">0.3:S v:pred</ta>
            <ta e="T40" id="Seg_691" s="T39">pro.h:S</ta>
            <ta e="T42" id="Seg_692" s="T41">v:pred</ta>
            <ta e="T47" id="Seg_693" s="T42">s:cond</ta>
            <ta e="T48" id="Seg_694" s="T47">v:O</ta>
            <ta e="T50" id="Seg_695" s="T49">ptcl:pred</ta>
            <ta e="T51" id="Seg_696" s="T50">ptcl:pred</ta>
            <ta e="T53" id="Seg_697" s="T52">v:O</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_698" s="T1">pro.h:A</ta>
            <ta e="T4" id="Seg_699" s="T3">np:G</ta>
            <ta e="T5" id="Seg_700" s="T4">adv:L</ta>
            <ta e="T6" id="Seg_701" s="T5">pro.h:Th</ta>
            <ta e="T9" id="Seg_702" s="T8">pro.h:A</ta>
            <ta e="T10" id="Seg_703" s="T9">np:Th</ta>
            <ta e="T13" id="Seg_704" s="T12">pro.h:A</ta>
            <ta e="T15" id="Seg_705" s="T14">adv:Time</ta>
            <ta e="T16" id="Seg_706" s="T15">np:Th</ta>
            <ta e="T17" id="Seg_707" s="T16">0.1.h:A</ta>
            <ta e="T19" id="Seg_708" s="T18">pro.h:A</ta>
            <ta e="T20" id="Seg_709" s="T19">pro.h:R</ta>
            <ta e="T24" id="Seg_710" s="T23">0.2.h:A</ta>
            <ta e="T26" id="Seg_711" s="T25">0.2.h:E</ta>
            <ta e="T27" id="Seg_712" s="T26">adv:L</ta>
            <ta e="T28" id="Seg_713" s="T27">np:Th</ta>
            <ta e="T29" id="Seg_714" s="T28">pro.h:A</ta>
            <ta e="T33" id="Seg_715" s="T32">pro:A</ta>
            <ta e="T34" id="Seg_716" s="T33">adv:L</ta>
            <ta e="T37" id="Seg_717" s="T36">0.3:Th</ta>
            <ta e="T39" id="Seg_718" s="T38">0.3:Th</ta>
            <ta e="T40" id="Seg_719" s="T39">pro.h:E</ta>
            <ta e="T44" id="Seg_720" s="T43">np.h:A</ta>
            <ta e="T45" id="Seg_721" s="T44">pro.h:R</ta>
            <ta e="T48" id="Seg_722" s="T47">v:Th</ta>
            <ta e="T52" id="Seg_723" s="T51">np:Th</ta>
            <ta e="T53" id="Seg_724" s="T52">v:Th</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T12" id="Seg_725" s="T11">RUS:gram</ta>
            <ta e="T18" id="Seg_726" s="T17">RUS:gram</ta>
            <ta e="T41" id="Seg_727" s="T40">RUS:gram</ta>
            <ta e="T43" id="Seg_728" s="T42">RUS:gram</ta>
            <ta e="T44" id="Seg_729" s="T43">RUS:cult</ta>
            <ta e="T49" id="Seg_730" s="T48">RUS:gram</ta>
            <ta e="T50" id="Seg_731" s="T49">RUS:mod</ta>
            <ta e="T51" id="Seg_732" s="T50">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T43" id="Seg_733" s="T42">Csub</ta>
            <ta e="T50" id="Seg_734" s="T49">Vsub</ta>
            <ta e="T51" id="Seg_735" s="T50">Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_736" s="T1">We came to Taxtamyshevo.</ta>
            <ta e="T8" id="Seg_737" s="T4">There we lived.</ta>
            <ta e="T11" id="Seg_738" s="T8">He used to bring water.</ta>
            <ta e="T17" id="Seg_739" s="T11">I said: “Today I will bring water.”</ta>
            <ta e="T26" id="Seg_740" s="T17">And he said to me: “When you go to bring water, don't get afraid.</ta>
            <ta e="T28" id="Seg_741" s="T26">There are turkeys [there].”</ta>
            <ta e="T32" id="Seg_742" s="T28">I went to bring water.</ta>
            <ta e="T35" id="Seg_743" s="T32">They were walking there.</ta>
            <ta e="T39" id="Seg_744" s="T35">They were big and black.</ta>
            <ta e="T47" id="Seg_745" s="T39">I would have got frightened, if Volodya hadn't warned me.</ta>
            <ta e="T50" id="Seg_746" s="T47">One shouldn't be afraid.</ta>
            <ta e="T53" id="Seg_747" s="T50">One should bring water.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_748" s="T1">Wir kamen nach Taxtamyshevo.</ta>
            <ta e="T8" id="Seg_749" s="T4">Dort lebten wir.</ta>
            <ta e="T11" id="Seg_750" s="T8">Er brachte für gewöhnlich Wasser.</ta>
            <ta e="T17" id="Seg_751" s="T11">Ich sagte: "Heute will ich Wasser holen."</ta>
            <ta e="T26" id="Seg_752" s="T17">Und er sagte zu mir: "Wenn du zum Wasserholen gehst, hab keine Angst.</ta>
            <ta e="T28" id="Seg_753" s="T26">Es gibt [dort] Truthähne."</ta>
            <ta e="T32" id="Seg_754" s="T28">Ich ging um Wasser zu holen.</ta>
            <ta e="T35" id="Seg_755" s="T32">Sie liefen dort.</ta>
            <ta e="T39" id="Seg_756" s="T35">Sie waren groß und schwarz.</ta>
            <ta e="T47" id="Seg_757" s="T39">Ich hätte Angst gehabt, wenn Volodya mich nicht gewarnt hätte.</ta>
            <ta e="T50" id="Seg_758" s="T47">Man sollte keine Angst haben.</ta>
            <ta e="T53" id="Seg_759" s="T50">Man sollte Wasser holen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_760" s="T1">Мы приехали в Тахтомышево.</ta>
            <ta e="T8" id="Seg_761" s="T4">Там мы и жили.</ta>
            <ta e="T11" id="Seg_762" s="T8">Он воды таскал.</ta>
            <ta e="T17" id="Seg_763" s="T11">Я сказала: “Сегодня я воды натаскаю”.</ta>
            <ta e="T26" id="Seg_764" s="T17">А он мне говорит: “По воду пойдешь, не испугайся.</ta>
            <ta e="T28" id="Seg_765" s="T26">Там индюки”.</ta>
            <ta e="T32" id="Seg_766" s="T28">Я пошла за водой.</ta>
            <ta e="T35" id="Seg_767" s="T32">Они там ходят.</ta>
            <ta e="T39" id="Seg_768" s="T35">Черные, большие.</ta>
            <ta e="T47" id="Seg_769" s="T39">Я бы испугалась, если бы Володя мне не сказал.</ta>
            <ta e="T50" id="Seg_770" s="T47">Бояться не надо.</ta>
            <ta e="T53" id="Seg_771" s="T50">Надо воды таскать.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_772" s="T1">мы приехали в Тахтомышево</ta>
            <ta e="T8" id="Seg_773" s="T4">там мы и жили</ta>
            <ta e="T11" id="Seg_774" s="T8">он воды таскал</ta>
            <ta e="T17" id="Seg_775" s="T11">я сказала сегодня я воды натаскаю</ta>
            <ta e="T26" id="Seg_776" s="T17">а он мне говорит по воду пойдешь не испугайся</ta>
            <ta e="T28" id="Seg_777" s="T26">там индюки</ta>
            <ta e="T32" id="Seg_778" s="T28">я пошла за водой</ta>
            <ta e="T35" id="Seg_779" s="T32">они там ходят</ta>
            <ta e="T39" id="Seg_780" s="T35">черные да большие</ta>
            <ta e="T47" id="Seg_781" s="T39">я бы испугалась если бы Володя мне не сказал</ta>
            <ta e="T50" id="Seg_782" s="T47">бояться не надо</ta>
            <ta e="T53" id="Seg_783" s="T50">надо воды таскать</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T11" id="Seg_784" s="T8">[KuAI:] Variant: 'dutdɨkkus'.</ta>
            <ta e="T26" id="Seg_785" s="T17">[KuAI:] Variant: 'käcʼiwatkə'.</ta>
            <ta e="T47" id="Seg_786" s="T39">[KuAI:] Variant: 'käcʼiwannän'.</ta>
            <ta e="T50" id="Seg_787" s="T47">[KuAI:] Variant: 'Käcʼewatku'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
