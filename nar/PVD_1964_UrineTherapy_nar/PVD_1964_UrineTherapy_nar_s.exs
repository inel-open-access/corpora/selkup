<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_UrineTherapy_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_UrineTherapy_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">83</ud-information>
            <ud-information attribute-name="# HIAT:w">63</ud-information>
            <ud-information attribute-name="# e">63</ud-information>
            <ud-information attribute-name="# HIAT:u">15</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T64" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Mannan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">čuškakalau</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">mʼässalan</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">aːzuzattə</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Qoblattə</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">közɨzattə</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">Wetʼenar</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">mekga</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">qajda</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">mazʼem</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">tatkus</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_44" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">Man</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">nazʼe</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">natkɨlǯau</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_56" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">Täbɨstaɣə</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">ešo</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">xuʒə</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">aːwan</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">aːzun</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_74" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">Patom</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">man</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">kuzse</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">müzulǯəlʼe</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_88" n="HIAT:w" s="T24">übərau</ts>
                  <nts id="Seg_89" n="HIAT:ip">.</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_92" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">Sədən</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">müzulǯau</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_101" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">I</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">täbɨstaɣɨnan</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_108" n="HIAT:ip">(</nts>
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">mannan</ts>
                  <nts id="Seg_111" n="HIAT:ip">)</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_114" n="HIAT:w" s="T30">qobladə</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_117" n="HIAT:w" s="T31">somnattə</ts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_121" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_123" n="HIAT:w" s="T32">Oj</ts>
                  <nts id="Seg_124" n="HIAT:ip">,</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">səte</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">čuškakau</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">quːnäɣə</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_136" n="HIAT:w" s="T36">ba</ts>
                  <nts id="Seg_137" n="HIAT:ip">.</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_140" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_142" n="HIAT:w" s="T37">Küsse</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_145" n="HIAT:w" s="T38">ass</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_148" n="HIAT:w" s="T39">müzulǯunäu</ts>
                  <nts id="Seg_149" n="HIAT:ip">,</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_152" n="HIAT:w" s="T40">quːnäɣə</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_155" n="HIAT:w" s="T41">bə</ts>
                  <nts id="Seg_156" n="HIAT:ip">.</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_159" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_161" n="HIAT:w" s="T42">Mekga</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_164" n="HIAT:w" s="T43">tʼärattə</ts>
                  <nts id="Seg_165" n="HIAT:ip">:</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_168" n="HIAT:w" s="T44">toblal</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_171" n="HIAT:w" s="T45">küsse</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_174" n="HIAT:w" s="T46">lekčitdə</ts>
                  <nts id="Seg_175" n="HIAT:ip">.</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_178" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_180" n="HIAT:w" s="T47">Trʼäpkam</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_183" n="HIAT:w" s="T48">mačiget</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_186" n="HIAT:w" s="T49">i</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_189" n="HIAT:w" s="T50">toboɣətdə</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_192" n="HIAT:w" s="T51">penket</ts>
                  <nts id="Seg_193" n="HIAT:ip">.</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_196" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_198" n="HIAT:w" s="T52">Man</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_201" n="HIAT:w" s="T53">tapbon</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_204" n="HIAT:w" s="T54">nildʼzʼin</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_207" n="HIAT:w" s="T55">meǯau</ts>
                  <nts id="Seg_208" n="HIAT:ip">.</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_211" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_213" n="HIAT:w" s="T56">Mekga</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_216" n="HIAT:w" s="T57">okkɨr</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_219" n="HIAT:w" s="T58">pajaga</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_222" n="HIAT:w" s="T59">tʼarɨs</ts>
                  <nts id="Seg_223" n="HIAT:ip">.</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_226" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_228" n="HIAT:w" s="T60">Man</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_231" n="HIAT:w" s="T61">nɨldʼzʼin</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_234" n="HIAT:w" s="T62">i</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_237" n="HIAT:w" s="T63">metǯau</ts>
                  <nts id="Seg_238" n="HIAT:ip">.</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T64" id="Seg_240" n="sc" s="T1">
               <ts e="T2" id="Seg_242" n="e" s="T1">Mannan </ts>
               <ts e="T3" id="Seg_244" n="e" s="T2">čuškakalau </ts>
               <ts e="T4" id="Seg_246" n="e" s="T3">mʼässalan </ts>
               <ts e="T5" id="Seg_248" n="e" s="T4">aːzuzattə. </ts>
               <ts e="T6" id="Seg_250" n="e" s="T5">Qoblattə </ts>
               <ts e="T7" id="Seg_252" n="e" s="T6">közɨzattə. </ts>
               <ts e="T8" id="Seg_254" n="e" s="T7">Wetʼenar </ts>
               <ts e="T9" id="Seg_256" n="e" s="T8">mekga </ts>
               <ts e="T10" id="Seg_258" n="e" s="T9">qajda </ts>
               <ts e="T11" id="Seg_260" n="e" s="T10">mazʼem </ts>
               <ts e="T12" id="Seg_262" n="e" s="T11">tatkus. </ts>
               <ts e="T13" id="Seg_264" n="e" s="T12">Man </ts>
               <ts e="T14" id="Seg_266" n="e" s="T13">nazʼe </ts>
               <ts e="T15" id="Seg_268" n="e" s="T14">natkɨlǯau. </ts>
               <ts e="T16" id="Seg_270" n="e" s="T15">Täbɨstaɣə </ts>
               <ts e="T17" id="Seg_272" n="e" s="T16">ešo </ts>
               <ts e="T18" id="Seg_274" n="e" s="T17">xuʒə </ts>
               <ts e="T19" id="Seg_276" n="e" s="T18">aːwan </ts>
               <ts e="T20" id="Seg_278" n="e" s="T19">aːzun. </ts>
               <ts e="T21" id="Seg_280" n="e" s="T20">Patom </ts>
               <ts e="T22" id="Seg_282" n="e" s="T21">man </ts>
               <ts e="T23" id="Seg_284" n="e" s="T22">kuzse </ts>
               <ts e="T24" id="Seg_286" n="e" s="T23">müzulǯəlʼe </ts>
               <ts e="T25" id="Seg_288" n="e" s="T24">übərau. </ts>
               <ts e="T26" id="Seg_290" n="e" s="T25">Sədən </ts>
               <ts e="T27" id="Seg_292" n="e" s="T26">müzulǯau. </ts>
               <ts e="T28" id="Seg_294" n="e" s="T27">I </ts>
               <ts e="T29" id="Seg_296" n="e" s="T28">täbɨstaɣɨnan </ts>
               <ts e="T30" id="Seg_298" n="e" s="T29">(mannan) </ts>
               <ts e="T31" id="Seg_300" n="e" s="T30">qobladə </ts>
               <ts e="T32" id="Seg_302" n="e" s="T31">somnattə. </ts>
               <ts e="T33" id="Seg_304" n="e" s="T32">Oj, </ts>
               <ts e="T34" id="Seg_306" n="e" s="T33">səte </ts>
               <ts e="T35" id="Seg_308" n="e" s="T34">čuškakau </ts>
               <ts e="T36" id="Seg_310" n="e" s="T35">quːnäɣə </ts>
               <ts e="T37" id="Seg_312" n="e" s="T36">ba. </ts>
               <ts e="T38" id="Seg_314" n="e" s="T37">Küsse </ts>
               <ts e="T39" id="Seg_316" n="e" s="T38">ass </ts>
               <ts e="T40" id="Seg_318" n="e" s="T39">müzulǯunäu, </ts>
               <ts e="T41" id="Seg_320" n="e" s="T40">quːnäɣə </ts>
               <ts e="T42" id="Seg_322" n="e" s="T41">bə. </ts>
               <ts e="T43" id="Seg_324" n="e" s="T42">Mekga </ts>
               <ts e="T44" id="Seg_326" n="e" s="T43">tʼärattə: </ts>
               <ts e="T45" id="Seg_328" n="e" s="T44">toblal </ts>
               <ts e="T46" id="Seg_330" n="e" s="T45">küsse </ts>
               <ts e="T47" id="Seg_332" n="e" s="T46">lekčitdə. </ts>
               <ts e="T48" id="Seg_334" n="e" s="T47">Trʼäpkam </ts>
               <ts e="T49" id="Seg_336" n="e" s="T48">mačiget </ts>
               <ts e="T50" id="Seg_338" n="e" s="T49">i </ts>
               <ts e="T51" id="Seg_340" n="e" s="T50">toboɣətdə </ts>
               <ts e="T52" id="Seg_342" n="e" s="T51">penket. </ts>
               <ts e="T53" id="Seg_344" n="e" s="T52">Man </ts>
               <ts e="T54" id="Seg_346" n="e" s="T53">tapbon </ts>
               <ts e="T55" id="Seg_348" n="e" s="T54">nildʼzʼin </ts>
               <ts e="T56" id="Seg_350" n="e" s="T55">meǯau. </ts>
               <ts e="T57" id="Seg_352" n="e" s="T56">Mekga </ts>
               <ts e="T58" id="Seg_354" n="e" s="T57">okkɨr </ts>
               <ts e="T59" id="Seg_356" n="e" s="T58">pajaga </ts>
               <ts e="T60" id="Seg_358" n="e" s="T59">tʼarɨs. </ts>
               <ts e="T61" id="Seg_360" n="e" s="T60">Man </ts>
               <ts e="T62" id="Seg_362" n="e" s="T61">nɨldʼzʼin </ts>
               <ts e="T63" id="Seg_364" n="e" s="T62">i </ts>
               <ts e="T64" id="Seg_366" n="e" s="T63">metǯau. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_367" s="T1">PVD_1964_UrineTherapy_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_368" s="T5">PVD_1964_UrineTherapy_nar.002 (001.002)</ta>
            <ta e="T12" id="Seg_369" s="T7">PVD_1964_UrineTherapy_nar.003 (001.003)</ta>
            <ta e="T15" id="Seg_370" s="T12">PVD_1964_UrineTherapy_nar.004 (001.004)</ta>
            <ta e="T20" id="Seg_371" s="T15">PVD_1964_UrineTherapy_nar.005 (001.005)</ta>
            <ta e="T25" id="Seg_372" s="T20">PVD_1964_UrineTherapy_nar.006 (001.006)</ta>
            <ta e="T27" id="Seg_373" s="T25">PVD_1964_UrineTherapy_nar.007 (001.007)</ta>
            <ta e="T32" id="Seg_374" s="T27">PVD_1964_UrineTherapy_nar.008 (001.008)</ta>
            <ta e="T37" id="Seg_375" s="T32">PVD_1964_UrineTherapy_nar.009 (001.009)</ta>
            <ta e="T42" id="Seg_376" s="T37">PVD_1964_UrineTherapy_nar.010 (001.010)</ta>
            <ta e="T47" id="Seg_377" s="T42">PVD_1964_UrineTherapy_nar.011 (001.011)</ta>
            <ta e="T52" id="Seg_378" s="T47">PVD_1964_UrineTherapy_nar.012 (001.012)</ta>
            <ta e="T56" id="Seg_379" s="T52">PVD_1964_UrineTherapy_nar.013 (001.013)</ta>
            <ta e="T60" id="Seg_380" s="T56">PVD_1964_UrineTherapy_nar.014 (001.014)</ta>
            <ta e="T64" id="Seg_381" s="T60">PVD_1964_UrineTherapy_nar.015 (001.015)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_382" s="T1">ман′нан ′тшушкака′лау мʼӓ′ссалан ′а̄зузаттъ.</ta>
            <ta e="T7" id="Seg_383" s="T5">kоб′латтъ кӧзы′заттъ.</ta>
            <ta e="T12" id="Seg_384" s="T7">ветʼе′нар ′мекга ′kайда ′мазʼем ′таткус.</ta>
            <ta e="T15" id="Seg_385" s="T12">ман на′зʼе ‵наткыl′джау̹.</ta>
            <ta e="T20" id="Seg_386" s="T15">‵тӓбыс′таɣъ е′шо ′хужъ ′а̄wан а̄′зун.</ta>
            <ta e="T25" id="Seg_387" s="T20">па′том ман куз̂′се мӱ′зуlджълʼе ′ӱбърау̹.</ta>
            <ta e="T27" id="Seg_388" s="T25">′съдън мӱзуl′джау̹.</ta>
            <ta e="T32" id="Seg_389" s="T27">и тӓбыстаɣы′нан (ма′ннан) ′kобладъ ′сомнаттъ.</ta>
            <ta e="T37" id="Seg_390" s="T32">ой, съте ′тшушкакау ′kӯнӓɣъ ба.</ta>
            <ta e="T42" id="Seg_391" s="T37">кӱс′се асс мӱзуlджу′нӓу̹, kӯ ′нӓɣъ бъ.</ta>
            <ta e="T47" id="Seg_392" s="T42">′мекга тʼӓ′раттъ: тоб′лал кӱс′се лек′тшитдъ.</ta>
            <ta e="T52" id="Seg_393" s="T47">′трʼӓпкам матши′гет и тобо′ɣътдъ пен′кет.</ta>
            <ta e="T56" id="Seg_394" s="T52">ман тап′б̂он ниl′дʼзʼин ′меджау̹.</ta>
            <ta e="T60" id="Seg_395" s="T56">′мекга о′ккыр па′jага тʼа′рыс.</ta>
            <ta e="T64" id="Seg_396" s="T60">ман ныl′дʼзʼин и ′метджау.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_397" s="T1">mannan tšuškakalau mʼässalan aːzuzattə.</ta>
            <ta e="T7" id="Seg_398" s="T5">qoblattə közɨzattə.</ta>
            <ta e="T12" id="Seg_399" s="T7">wetʼenar mekga qajda mazʼem tatkus.</ta>
            <ta e="T15" id="Seg_400" s="T12">man nazʼe natkɨlǯau̹.</ta>
            <ta e="T20" id="Seg_401" s="T15">täbɨstaɣə ešo xuʒə aːwan aːzun.</ta>
            <ta e="T25" id="Seg_402" s="T20">patom man kuẑse müzulǯəlʼe übərau̹.</ta>
            <ta e="T27" id="Seg_403" s="T25">sədən müzulǯau̹.</ta>
            <ta e="T32" id="Seg_404" s="T27">i täbɨstaɣɨnan (mannan) qobladə somnattə.</ta>
            <ta e="T37" id="Seg_405" s="T32">oj, səte tšuškakau quːnäɣə ba.</ta>
            <ta e="T42" id="Seg_406" s="T37">küsse ass müzulǯunäu̹, quː näɣə bə.</ta>
            <ta e="T47" id="Seg_407" s="T42">mekga tʼärattə: toblal küsse lektšitdə.</ta>
            <ta e="T52" id="Seg_408" s="T47">trʼäpkam matšiget i toboɣətdə penket.</ta>
            <ta e="T56" id="Seg_409" s="T52">man tapb̂on nildʼzʼin meǯau̹.</ta>
            <ta e="T60" id="Seg_410" s="T56">mekga okkɨr pajaga tʼarɨs.</ta>
            <ta e="T64" id="Seg_411" s="T60">man nɨldʼzʼin i metǯau.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_412" s="T1">Mannan čuškakalau mʼässalan aːzuzattə. </ta>
            <ta e="T7" id="Seg_413" s="T5">Qoblattə közɨzattə. </ta>
            <ta e="T12" id="Seg_414" s="T7">Wetʼenar mekga qajda mazʼem tatkus. </ta>
            <ta e="T15" id="Seg_415" s="T12">Man nazʼe natkɨlǯau. </ta>
            <ta e="T20" id="Seg_416" s="T15">Täbɨstaɣə ešo xuʒə aːwan aːzun. </ta>
            <ta e="T25" id="Seg_417" s="T20">Patom man kuzse müzulǯəlʼe übərau. </ta>
            <ta e="T27" id="Seg_418" s="T25">Sədən müzulǯau. </ta>
            <ta e="T32" id="Seg_419" s="T27">I täbɨstaɣɨnan (mannan) qobladə somnattə. </ta>
            <ta e="T37" id="Seg_420" s="T32">Oj, səte čuškakau quːnäɣə ba. </ta>
            <ta e="T42" id="Seg_421" s="T37">Küsse ass müzulǯunäu, quːnäɣə bə. </ta>
            <ta e="T47" id="Seg_422" s="T42">Mekga tʼärattə: toblal küsse lekčitdə. </ta>
            <ta e="T52" id="Seg_423" s="T47">Trʼäpkam mačiget i toboɣətdə penket. </ta>
            <ta e="T56" id="Seg_424" s="T52">Man tapbon nildʼzʼin meǯau. </ta>
            <ta e="T60" id="Seg_425" s="T56">Mekga okkɨr pajaga tʼarɨs. </ta>
            <ta e="T64" id="Seg_426" s="T60">Man nɨldʼzʼin i metǯau. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_427" s="T1">man-nan</ta>
            <ta e="T3" id="Seg_428" s="T2">čuška-ka-la-u</ta>
            <ta e="T4" id="Seg_429" s="T3">mʼässala-n</ta>
            <ta e="T5" id="Seg_430" s="T4">aːzu-za-ttə</ta>
            <ta e="T6" id="Seg_431" s="T5">qob-la-ttə</ta>
            <ta e="T7" id="Seg_432" s="T6">közɨ-za-ttə</ta>
            <ta e="T8" id="Seg_433" s="T7">wetʼenar</ta>
            <ta e="T9" id="Seg_434" s="T8">mekga</ta>
            <ta e="T10" id="Seg_435" s="T9">qaj-da</ta>
            <ta e="T11" id="Seg_436" s="T10">mazʼ-e-m</ta>
            <ta e="T12" id="Seg_437" s="T11">tat-ku-s</ta>
            <ta e="T13" id="Seg_438" s="T12">man</ta>
            <ta e="T14" id="Seg_439" s="T13">na-zʼe</ta>
            <ta e="T15" id="Seg_440" s="T14">natkɨl-ǯa-u</ta>
            <ta e="T16" id="Seg_441" s="T15">täb-ɨ-staɣə</ta>
            <ta e="T19" id="Seg_442" s="T18">aːwa-n</ta>
            <ta e="T20" id="Seg_443" s="T19">aːzu-n</ta>
            <ta e="T21" id="Seg_444" s="T20">patom</ta>
            <ta e="T22" id="Seg_445" s="T21">man</ta>
            <ta e="T23" id="Seg_446" s="T22">kuz-se</ta>
            <ta e="T24" id="Seg_447" s="T23">müzulǯə-lʼe</ta>
            <ta e="T25" id="Seg_448" s="T24">übə-r-a-u</ta>
            <ta e="T26" id="Seg_449" s="T25">sədə-n</ta>
            <ta e="T27" id="Seg_450" s="T26">müzulǯa-u</ta>
            <ta e="T28" id="Seg_451" s="T27">i</ta>
            <ta e="T29" id="Seg_452" s="T28">täb-ɨ-staɣɨ-nan</ta>
            <ta e="T30" id="Seg_453" s="T29">man-nan</ta>
            <ta e="T31" id="Seg_454" s="T30">qob-la-də</ta>
            <ta e="T32" id="Seg_455" s="T31">so-m-na-ttə</ta>
            <ta e="T33" id="Seg_456" s="T32">oj</ta>
            <ta e="T34" id="Seg_457" s="T33">səte</ta>
            <ta e="T35" id="Seg_458" s="T34">čuška-ka-u</ta>
            <ta e="T36" id="Seg_459" s="T35">quː-nä-ɣə</ta>
            <ta e="T37" id="Seg_460" s="T36">ba</ta>
            <ta e="T38" id="Seg_461" s="T37">küs-se</ta>
            <ta e="T39" id="Seg_462" s="T38">ass</ta>
            <ta e="T40" id="Seg_463" s="T39">müzulǯu-nä-u</ta>
            <ta e="T41" id="Seg_464" s="T40">quː-nä-ɣə</ta>
            <ta e="T42" id="Seg_465" s="T41">bə</ta>
            <ta e="T43" id="Seg_466" s="T42">mekga</ta>
            <ta e="T44" id="Seg_467" s="T43">tʼära-ttə</ta>
            <ta e="T45" id="Seg_468" s="T44">tob-la-l</ta>
            <ta e="T46" id="Seg_469" s="T45">küs-se</ta>
            <ta e="T47" id="Seg_470" s="T46">lekči-tdə</ta>
            <ta e="T48" id="Seg_471" s="T47">trʼäpka-m</ta>
            <ta e="T49" id="Seg_472" s="T48">mači-g-et</ta>
            <ta e="T50" id="Seg_473" s="T49">i</ta>
            <ta e="T51" id="Seg_474" s="T50">tob-o-ɣətdə</ta>
            <ta e="T52" id="Seg_475" s="T51">pen-k-et</ta>
            <ta e="T53" id="Seg_476" s="T52">man</ta>
            <ta e="T54" id="Seg_477" s="T53">tap-bo-n</ta>
            <ta e="T55" id="Seg_478" s="T54">nildʼzʼi-n</ta>
            <ta e="T56" id="Seg_479" s="T55">me-ǯa-u</ta>
            <ta e="T57" id="Seg_480" s="T56">mekga</ta>
            <ta e="T58" id="Seg_481" s="T57">okkɨr</ta>
            <ta e="T59" id="Seg_482" s="T58">paja-ga</ta>
            <ta e="T60" id="Seg_483" s="T59">tʼarɨ-s</ta>
            <ta e="T61" id="Seg_484" s="T60">man</ta>
            <ta e="T62" id="Seg_485" s="T61">nɨldʼzʼi-n</ta>
            <ta e="T63" id="Seg_486" s="T62">i</ta>
            <ta e="T64" id="Seg_487" s="T63">me-tǯa-u</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_488" s="T1">man-nan</ta>
            <ta e="T3" id="Seg_489" s="T2">čuška-ka-la-w</ta>
            <ta e="T4" id="Seg_490" s="T3">məsalaj-ŋ</ta>
            <ta e="T5" id="Seg_491" s="T4">azu-sɨ-tɨn</ta>
            <ta e="T6" id="Seg_492" s="T5">qob-la-tɨn</ta>
            <ta e="T7" id="Seg_493" s="T6">qüzu-sɨ-tɨn</ta>
            <ta e="T8" id="Seg_494" s="T7">wetʼenar</ta>
            <ta e="T9" id="Seg_495" s="T8">mekka</ta>
            <ta e="T10" id="Seg_496" s="T9">qaj-ta</ta>
            <ta e="T11" id="Seg_497" s="T10">mazʼ-ɨ-m</ta>
            <ta e="T12" id="Seg_498" s="T11">tat-ku-sɨ</ta>
            <ta e="T13" id="Seg_499" s="T12">man</ta>
            <ta e="T14" id="Seg_500" s="T13">na-se</ta>
            <ta e="T15" id="Seg_501" s="T14">natqɨl-lǯɨ-w</ta>
            <ta e="T16" id="Seg_502" s="T15">täp-ɨ-staɣɨ</ta>
            <ta e="T19" id="Seg_503" s="T18">awa-ŋ</ta>
            <ta e="T20" id="Seg_504" s="T19">azu-n</ta>
            <ta e="T21" id="Seg_505" s="T20">patom</ta>
            <ta e="T22" id="Seg_506" s="T21">man</ta>
            <ta e="T23" id="Seg_507" s="T22">qözu-se</ta>
            <ta e="T24" id="Seg_508" s="T23">müzulǯu-le</ta>
            <ta e="T25" id="Seg_509" s="T24">übɨ-r-nɨ-w</ta>
            <ta e="T26" id="Seg_510" s="T25">sədə-ŋ</ta>
            <ta e="T27" id="Seg_511" s="T26">müzulǯu-w</ta>
            <ta e="T28" id="Seg_512" s="T27">i</ta>
            <ta e="T29" id="Seg_513" s="T28">täp-ɨ-staɣɨ-nan</ta>
            <ta e="T30" id="Seg_514" s="T29">man-nan</ta>
            <ta e="T31" id="Seg_515" s="T30">qob-la-tə</ta>
            <ta e="T32" id="Seg_516" s="T31">so-m-nɨ-tɨn</ta>
            <ta e="T33" id="Seg_517" s="T32">oj</ta>
            <ta e="T34" id="Seg_518" s="T33">sədə</ta>
            <ta e="T35" id="Seg_519" s="T34">čuška-ka-w</ta>
            <ta e="T36" id="Seg_520" s="T35">quː-ne-qij</ta>
            <ta e="T37" id="Seg_521" s="T36">bɨ</ta>
            <ta e="T38" id="Seg_522" s="T37">qözu-se</ta>
            <ta e="T39" id="Seg_523" s="T38">asa</ta>
            <ta e="T40" id="Seg_524" s="T39">müzulǯu-ne-w</ta>
            <ta e="T41" id="Seg_525" s="T40">quː-ne-qij</ta>
            <ta e="T42" id="Seg_526" s="T41">bɨ</ta>
            <ta e="T43" id="Seg_527" s="T42">mekka</ta>
            <ta e="T44" id="Seg_528" s="T43">tʼärɨ-tɨn</ta>
            <ta e="T45" id="Seg_529" s="T44">tob-la-l</ta>
            <ta e="T46" id="Seg_530" s="T45">qözu-se</ta>
            <ta e="T47" id="Seg_531" s="T46">lekči-etɨ</ta>
            <ta e="T48" id="Seg_532" s="T47">trʼäpka-m</ta>
            <ta e="T49" id="Seg_533" s="T48">mači-ku-etɨ</ta>
            <ta e="T50" id="Seg_534" s="T49">i</ta>
            <ta e="T51" id="Seg_535" s="T50">tob-ɨ-qɨntɨ</ta>
            <ta e="T52" id="Seg_536" s="T51">pen-ku-etɨ</ta>
            <ta e="T53" id="Seg_537" s="T52">man</ta>
            <ta e="T54" id="Seg_538" s="T53">taw-po-n</ta>
            <ta e="T55" id="Seg_539" s="T54">nʼilʼdʼi-ŋ</ta>
            <ta e="T56" id="Seg_540" s="T55">meː-enǯɨ-w</ta>
            <ta e="T57" id="Seg_541" s="T56">mekka</ta>
            <ta e="T58" id="Seg_542" s="T57">okkɨr</ta>
            <ta e="T59" id="Seg_543" s="T58">paja-ka</ta>
            <ta e="T60" id="Seg_544" s="T59">tʼärɨ-sɨ</ta>
            <ta e="T61" id="Seg_545" s="T60">man</ta>
            <ta e="T62" id="Seg_546" s="T61">nʼilʼdʼi-ŋ</ta>
            <ta e="T63" id="Seg_547" s="T62">i</ta>
            <ta e="T64" id="Seg_548" s="T63">meː-enǯɨ-w</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_549" s="T1">I-ADES</ta>
            <ta e="T3" id="Seg_550" s="T2">pig-DIM-PL.[NOM]-1SG</ta>
            <ta e="T4" id="Seg_551" s="T3">sick-ADVZ</ta>
            <ta e="T5" id="Seg_552" s="T4">become-PST-3PL</ta>
            <ta e="T6" id="Seg_553" s="T5">skin-PL.[NOM]-3PL</ta>
            <ta e="T7" id="Seg_554" s="T6">ache-PST-3PL</ta>
            <ta e="T8" id="Seg_555" s="T7">veterinarian.[NOM]</ta>
            <ta e="T9" id="Seg_556" s="T8">I.ALL</ta>
            <ta e="T10" id="Seg_557" s="T9">what-INDEF.[NOM]</ta>
            <ta e="T11" id="Seg_558" s="T10">ointment-EP-ACC</ta>
            <ta e="T12" id="Seg_559" s="T11">give-HAB-PST.[3SG.S]</ta>
            <ta e="T13" id="Seg_560" s="T12">I.NOM</ta>
            <ta e="T14" id="Seg_561" s="T13">this-INSTR</ta>
            <ta e="T15" id="Seg_562" s="T14">rub-TR-1SG.O</ta>
            <ta e="T16" id="Seg_563" s="T15">(s)he-EP-DU.[NOM]</ta>
            <ta e="T19" id="Seg_564" s="T18">bad-ADVZ</ta>
            <ta e="T20" id="Seg_565" s="T19">become-3SG.S</ta>
            <ta e="T21" id="Seg_566" s="T20">then</ta>
            <ta e="T22" id="Seg_567" s="T21">I.NOM</ta>
            <ta e="T23" id="Seg_568" s="T22">urine-INSTR</ta>
            <ta e="T24" id="Seg_569" s="T23">wash-CVB</ta>
            <ta e="T25" id="Seg_570" s="T24">begin-DRV-CO-1SG.O</ta>
            <ta e="T26" id="Seg_571" s="T25">two-ADVZ</ta>
            <ta e="T27" id="Seg_572" s="T26">wash-1SG.O</ta>
            <ta e="T28" id="Seg_573" s="T27">and</ta>
            <ta e="T29" id="Seg_574" s="T28">(s)he-EP-DU-ADES</ta>
            <ta e="T30" id="Seg_575" s="T29">I-ADES</ta>
            <ta e="T31" id="Seg_576" s="T30">skin-PL.[NOM]-3SG</ta>
            <ta e="T32" id="Seg_577" s="T31">good-TRL-CO-3PL</ta>
            <ta e="T33" id="Seg_578" s="T32">oh</ta>
            <ta e="T34" id="Seg_579" s="T33">two</ta>
            <ta e="T35" id="Seg_580" s="T34">pig-DIM.[NOM]-1SG</ta>
            <ta e="T36" id="Seg_581" s="T35">die-CONJ-3DU.S</ta>
            <ta e="T37" id="Seg_582" s="T36">IRREAL</ta>
            <ta e="T38" id="Seg_583" s="T37">urine-INSTR</ta>
            <ta e="T39" id="Seg_584" s="T38">NEG</ta>
            <ta e="T40" id="Seg_585" s="T39">wash-CONJ-1SG.O</ta>
            <ta e="T41" id="Seg_586" s="T40">die-CONJ-3DU.S</ta>
            <ta e="T42" id="Seg_587" s="T41">IRREAL</ta>
            <ta e="T43" id="Seg_588" s="T42">I.ALL</ta>
            <ta e="T44" id="Seg_589" s="T43">say-3PL</ta>
            <ta e="T45" id="Seg_590" s="T44">leg-PL.[NOM]-2SG</ta>
            <ta e="T46" id="Seg_591" s="T45">urine-INSTR</ta>
            <ta e="T47" id="Seg_592" s="T46">treat-IMP.2SG.O</ta>
            <ta e="T48" id="Seg_593" s="T47">rag-ACC</ta>
            <ta e="T49" id="Seg_594" s="T48">moisten-HAB-IMP.2SG.O</ta>
            <ta e="T50" id="Seg_595" s="T49">and</ta>
            <ta e="T51" id="Seg_596" s="T50">leg-EP-ILL.3SG</ta>
            <ta e="T52" id="Seg_597" s="T51">put-HAB-IMP.2SG.O</ta>
            <ta e="T53" id="Seg_598" s="T52">I.NOM</ta>
            <ta e="T54" id="Seg_599" s="T53">this-year-ADV.LOC</ta>
            <ta e="T55" id="Seg_600" s="T54">so-ADVZ</ta>
            <ta e="T56" id="Seg_601" s="T55">do-FUT-1SG.O</ta>
            <ta e="T57" id="Seg_602" s="T56">I.ALL</ta>
            <ta e="T58" id="Seg_603" s="T57">one</ta>
            <ta e="T59" id="Seg_604" s="T58">old.woman-DIM.[NOM]</ta>
            <ta e="T60" id="Seg_605" s="T59">say-PST.[3SG.S]</ta>
            <ta e="T61" id="Seg_606" s="T60">I.NOM</ta>
            <ta e="T62" id="Seg_607" s="T61">such-ADVZ</ta>
            <ta e="T63" id="Seg_608" s="T62">and</ta>
            <ta e="T64" id="Seg_609" s="T63">do-FUT-1SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_610" s="T1">я-ADES</ta>
            <ta e="T3" id="Seg_611" s="T2">свинья-DIM-PL.[NOM]-1SG</ta>
            <ta e="T4" id="Seg_612" s="T3">больной-ADVZ</ta>
            <ta e="T5" id="Seg_613" s="T4">стать-PST-3PL</ta>
            <ta e="T6" id="Seg_614" s="T5">кожа-PL.[NOM]-3PL</ta>
            <ta e="T7" id="Seg_615" s="T6">болеть-PST-3PL</ta>
            <ta e="T8" id="Seg_616" s="T7">ветеринар.[NOM]</ta>
            <ta e="T9" id="Seg_617" s="T8">я.ALL</ta>
            <ta e="T10" id="Seg_618" s="T9">что-INDEF.[NOM]</ta>
            <ta e="T11" id="Seg_619" s="T10">мазь-EP-ACC</ta>
            <ta e="T12" id="Seg_620" s="T11">подать-HAB-PST.[3SG.S]</ta>
            <ta e="T13" id="Seg_621" s="T12">я.NOM</ta>
            <ta e="T14" id="Seg_622" s="T13">этот-INSTR</ta>
            <ta e="T15" id="Seg_623" s="T14">натереть-TR-1SG.O</ta>
            <ta e="T16" id="Seg_624" s="T15">он(а)-EP-DU.[NOM]</ta>
            <ta e="T19" id="Seg_625" s="T18">плохой-ADVZ</ta>
            <ta e="T20" id="Seg_626" s="T19">стать-3SG.S</ta>
            <ta e="T21" id="Seg_627" s="T20">потом</ta>
            <ta e="T22" id="Seg_628" s="T21">я.NOM</ta>
            <ta e="T23" id="Seg_629" s="T22">моча-INSTR</ta>
            <ta e="T24" id="Seg_630" s="T23">вымыть-CVB</ta>
            <ta e="T25" id="Seg_631" s="T24">начать-DRV-CO-1SG.O</ta>
            <ta e="T26" id="Seg_632" s="T25">два-ADVZ</ta>
            <ta e="T27" id="Seg_633" s="T26">вымыть-1SG.O</ta>
            <ta e="T28" id="Seg_634" s="T27">и</ta>
            <ta e="T29" id="Seg_635" s="T28">он(а)-EP-DU-ADES</ta>
            <ta e="T30" id="Seg_636" s="T29">я-ADES</ta>
            <ta e="T31" id="Seg_637" s="T30">кожа-PL.[NOM]-3SG</ta>
            <ta e="T32" id="Seg_638" s="T31">хороший-TRL-CO-3PL</ta>
            <ta e="T33" id="Seg_639" s="T32">ой</ta>
            <ta e="T34" id="Seg_640" s="T33">два</ta>
            <ta e="T35" id="Seg_641" s="T34">свинья-DIM.[NOM]-1SG</ta>
            <ta e="T36" id="Seg_642" s="T35">умереть-CONJ-3DU.S</ta>
            <ta e="T37" id="Seg_643" s="T36">IRREAL</ta>
            <ta e="T38" id="Seg_644" s="T37">моча-INSTR</ta>
            <ta e="T39" id="Seg_645" s="T38">NEG</ta>
            <ta e="T40" id="Seg_646" s="T39">вымыть-CONJ-1SG.O</ta>
            <ta e="T41" id="Seg_647" s="T40">умереть-CONJ-3DU.S</ta>
            <ta e="T42" id="Seg_648" s="T41">IRREAL</ta>
            <ta e="T43" id="Seg_649" s="T42">я.ALL</ta>
            <ta e="T44" id="Seg_650" s="T43">сказать-3PL</ta>
            <ta e="T45" id="Seg_651" s="T44">нога-PL.[NOM]-2SG</ta>
            <ta e="T46" id="Seg_652" s="T45">моча-INSTR</ta>
            <ta e="T47" id="Seg_653" s="T46">лечить-IMP.2SG.O</ta>
            <ta e="T48" id="Seg_654" s="T47">тряпка-ACC</ta>
            <ta e="T49" id="Seg_655" s="T48">намочить-HAB-IMP.2SG.O</ta>
            <ta e="T50" id="Seg_656" s="T49">и</ta>
            <ta e="T51" id="Seg_657" s="T50">нога-EP-ILL.3SG</ta>
            <ta e="T52" id="Seg_658" s="T51">положить-HAB-IMP.2SG.O</ta>
            <ta e="T53" id="Seg_659" s="T52">я.NOM</ta>
            <ta e="T54" id="Seg_660" s="T53">этот-год-ADV.LOC</ta>
            <ta e="T55" id="Seg_661" s="T54">так-ADVZ</ta>
            <ta e="T56" id="Seg_662" s="T55">сделать-FUT-1SG.O</ta>
            <ta e="T57" id="Seg_663" s="T56">я.ALL</ta>
            <ta e="T58" id="Seg_664" s="T57">один</ta>
            <ta e="T59" id="Seg_665" s="T58">старуха-DIM.[NOM]</ta>
            <ta e="T60" id="Seg_666" s="T59">сказать-PST.[3SG.S]</ta>
            <ta e="T61" id="Seg_667" s="T60">я.NOM</ta>
            <ta e="T62" id="Seg_668" s="T61">такой-ADVZ</ta>
            <ta e="T63" id="Seg_669" s="T62">и</ta>
            <ta e="T64" id="Seg_670" s="T63">сделать-FUT-1SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_671" s="T1">pers-n:case</ta>
            <ta e="T3" id="Seg_672" s="T2">n-n&gt;n-n:num.[n:case]-n:poss</ta>
            <ta e="T4" id="Seg_673" s="T3">adj-adj&gt;adv</ta>
            <ta e="T5" id="Seg_674" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_675" s="T5">n-n:num.[n:case]-n:poss</ta>
            <ta e="T7" id="Seg_676" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_677" s="T7">n.[n:case]</ta>
            <ta e="T9" id="Seg_678" s="T8">pers</ta>
            <ta e="T10" id="Seg_679" s="T9">interrog-clit.[n:case]</ta>
            <ta e="T11" id="Seg_680" s="T10">n-n:ins-n:case</ta>
            <ta e="T12" id="Seg_681" s="T11">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T13" id="Seg_682" s="T12">pers</ta>
            <ta e="T14" id="Seg_683" s="T13">dem-n:case</ta>
            <ta e="T15" id="Seg_684" s="T14">v-v&gt;v-v:pn</ta>
            <ta e="T16" id="Seg_685" s="T15">pers-n:ins-n:num.[n:case]</ta>
            <ta e="T19" id="Seg_686" s="T18">adj-adj&gt;adv</ta>
            <ta e="T20" id="Seg_687" s="T19">v-v:pn</ta>
            <ta e="T21" id="Seg_688" s="T20">adv</ta>
            <ta e="T22" id="Seg_689" s="T21">pers</ta>
            <ta e="T23" id="Seg_690" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_691" s="T23">v-v&gt;adv</ta>
            <ta e="T25" id="Seg_692" s="T24">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T26" id="Seg_693" s="T25">num-quant&gt;adv</ta>
            <ta e="T27" id="Seg_694" s="T26">v-v:pn</ta>
            <ta e="T28" id="Seg_695" s="T27">conj</ta>
            <ta e="T29" id="Seg_696" s="T28">pers-n:ins-n:num-n:case</ta>
            <ta e="T30" id="Seg_697" s="T29">pers-n:case</ta>
            <ta e="T31" id="Seg_698" s="T30">n-n:num.[n:case]-n:poss</ta>
            <ta e="T32" id="Seg_699" s="T31">adj-n&gt;v-v:ins-v:pn</ta>
            <ta e="T33" id="Seg_700" s="T32">interj</ta>
            <ta e="T34" id="Seg_701" s="T33">num</ta>
            <ta e="T35" id="Seg_702" s="T34">n-n&gt;n.[n:case]-n:poss</ta>
            <ta e="T36" id="Seg_703" s="T35">v-v:mood-v:pn</ta>
            <ta e="T37" id="Seg_704" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_705" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_706" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_707" s="T39">v-v:mood-v:pn</ta>
            <ta e="T41" id="Seg_708" s="T40">v-v:mood-v:pn</ta>
            <ta e="T42" id="Seg_709" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_710" s="T42">pers</ta>
            <ta e="T44" id="Seg_711" s="T43">v-v:pn</ta>
            <ta e="T45" id="Seg_712" s="T44">n-n:num.[n:case]-n:poss</ta>
            <ta e="T46" id="Seg_713" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_714" s="T46">v-v:mood.pn</ta>
            <ta e="T48" id="Seg_715" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_716" s="T48">v-v&gt;v-v:mood.pn</ta>
            <ta e="T50" id="Seg_717" s="T49">conj</ta>
            <ta e="T51" id="Seg_718" s="T50">n-n:ins-n:case.poss</ta>
            <ta e="T52" id="Seg_719" s="T51">v-v&gt;v-v:mood.pn</ta>
            <ta e="T53" id="Seg_720" s="T52">pers</ta>
            <ta e="T54" id="Seg_721" s="T53">dem-n-n&gt;adv</ta>
            <ta e="T55" id="Seg_722" s="T54">adv-adj&gt;adv</ta>
            <ta e="T56" id="Seg_723" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_724" s="T56">pers</ta>
            <ta e="T58" id="Seg_725" s="T57">num</ta>
            <ta e="T59" id="Seg_726" s="T58">n-n&gt;n.[n:case]</ta>
            <ta e="T60" id="Seg_727" s="T59">v-v:tense.[v:pn]</ta>
            <ta e="T61" id="Seg_728" s="T60">pers</ta>
            <ta e="T62" id="Seg_729" s="T61">adj-adj&gt;adv</ta>
            <ta e="T63" id="Seg_730" s="T62">conj</ta>
            <ta e="T64" id="Seg_731" s="T63">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_732" s="T1">pers</ta>
            <ta e="T3" id="Seg_733" s="T2">n</ta>
            <ta e="T4" id="Seg_734" s="T3">adv</ta>
            <ta e="T5" id="Seg_735" s="T4">v</ta>
            <ta e="T6" id="Seg_736" s="T5">n</ta>
            <ta e="T7" id="Seg_737" s="T6">v</ta>
            <ta e="T8" id="Seg_738" s="T7">n</ta>
            <ta e="T9" id="Seg_739" s="T8">pers</ta>
            <ta e="T10" id="Seg_740" s="T9">pro</ta>
            <ta e="T11" id="Seg_741" s="T10">n</ta>
            <ta e="T12" id="Seg_742" s="T11">v</ta>
            <ta e="T13" id="Seg_743" s="T12">pers</ta>
            <ta e="T14" id="Seg_744" s="T13">dem</ta>
            <ta e="T15" id="Seg_745" s="T14">v</ta>
            <ta e="T16" id="Seg_746" s="T15">pers</ta>
            <ta e="T19" id="Seg_747" s="T18">adj</ta>
            <ta e="T20" id="Seg_748" s="T19">v</ta>
            <ta e="T21" id="Seg_749" s="T20">adv</ta>
            <ta e="T22" id="Seg_750" s="T21">pers</ta>
            <ta e="T23" id="Seg_751" s="T22">n</ta>
            <ta e="T24" id="Seg_752" s="T23">adv</ta>
            <ta e="T25" id="Seg_753" s="T24">v</ta>
            <ta e="T26" id="Seg_754" s="T25">adv</ta>
            <ta e="T27" id="Seg_755" s="T26">v</ta>
            <ta e="T28" id="Seg_756" s="T27">conj</ta>
            <ta e="T29" id="Seg_757" s="T28">pers</ta>
            <ta e="T30" id="Seg_758" s="T29">pers</ta>
            <ta e="T31" id="Seg_759" s="T30">n</ta>
            <ta e="T32" id="Seg_760" s="T31">v</ta>
            <ta e="T33" id="Seg_761" s="T32">interj</ta>
            <ta e="T34" id="Seg_762" s="T33">num</ta>
            <ta e="T35" id="Seg_763" s="T34">n</ta>
            <ta e="T36" id="Seg_764" s="T35">v</ta>
            <ta e="T37" id="Seg_765" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_766" s="T37">n</ta>
            <ta e="T39" id="Seg_767" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_768" s="T39">v</ta>
            <ta e="T41" id="Seg_769" s="T40">v</ta>
            <ta e="T42" id="Seg_770" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_771" s="T42">pers</ta>
            <ta e="T44" id="Seg_772" s="T43">v</ta>
            <ta e="T45" id="Seg_773" s="T44">n</ta>
            <ta e="T46" id="Seg_774" s="T45">n</ta>
            <ta e="T47" id="Seg_775" s="T46">v</ta>
            <ta e="T48" id="Seg_776" s="T47">n</ta>
            <ta e="T49" id="Seg_777" s="T48">v</ta>
            <ta e="T50" id="Seg_778" s="T49">conj</ta>
            <ta e="T51" id="Seg_779" s="T50">n</ta>
            <ta e="T52" id="Seg_780" s="T51">v</ta>
            <ta e="T53" id="Seg_781" s="T52">pers</ta>
            <ta e="T54" id="Seg_782" s="T53">adv</ta>
            <ta e="T55" id="Seg_783" s="T54">adv</ta>
            <ta e="T56" id="Seg_784" s="T55">v</ta>
            <ta e="T57" id="Seg_785" s="T56">pers</ta>
            <ta e="T58" id="Seg_786" s="T57">num</ta>
            <ta e="T59" id="Seg_787" s="T58">n</ta>
            <ta e="T60" id="Seg_788" s="T59">v</ta>
            <ta e="T61" id="Seg_789" s="T60">pers</ta>
            <ta e="T62" id="Seg_790" s="T61">adj</ta>
            <ta e="T63" id="Seg_791" s="T62">conj</ta>
            <ta e="T64" id="Seg_792" s="T63">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_793" s="T2">np:S</ta>
            <ta e="T5" id="Seg_794" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_795" s="T5">np:S</ta>
            <ta e="T7" id="Seg_796" s="T6">v:pred</ta>
            <ta e="T8" id="Seg_797" s="T7">np.h:S</ta>
            <ta e="T11" id="Seg_798" s="T10">np:O</ta>
            <ta e="T12" id="Seg_799" s="T11">v:pred</ta>
            <ta e="T13" id="Seg_800" s="T12">pro.h:S</ta>
            <ta e="T15" id="Seg_801" s="T14">v:pred 0.3:O</ta>
            <ta e="T16" id="Seg_802" s="T15">pro:S</ta>
            <ta e="T20" id="Seg_803" s="T19">v:pred</ta>
            <ta e="T22" id="Seg_804" s="T21">pro.h:S</ta>
            <ta e="T25" id="Seg_805" s="T24">v:pred</ta>
            <ta e="T27" id="Seg_806" s="T26">0.1.h:S 0.3:O v:pred</ta>
            <ta e="T31" id="Seg_807" s="T30">np:S</ta>
            <ta e="T32" id="Seg_808" s="T31">v:pred</ta>
            <ta e="T35" id="Seg_809" s="T34">np:S</ta>
            <ta e="T36" id="Seg_810" s="T35">v:pred</ta>
            <ta e="T40" id="Seg_811" s="T39">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T41" id="Seg_812" s="T40">0.3:S v:pred</ta>
            <ta e="T44" id="Seg_813" s="T43">0.3.h:S v:pred</ta>
            <ta e="T45" id="Seg_814" s="T44">np:O</ta>
            <ta e="T47" id="Seg_815" s="T46">0.2.h:S v:pred</ta>
            <ta e="T48" id="Seg_816" s="T47">np:O</ta>
            <ta e="T49" id="Seg_817" s="T48">0.2.h:S v:pred</ta>
            <ta e="T52" id="Seg_818" s="T51">0.2.h:S v:pred 0.3:O</ta>
            <ta e="T53" id="Seg_819" s="T52">pro.h:S</ta>
            <ta e="T56" id="Seg_820" s="T55">v:pred</ta>
            <ta e="T59" id="Seg_821" s="T58">np.h:S</ta>
            <ta e="T60" id="Seg_822" s="T59">v:pred</ta>
            <ta e="T61" id="Seg_823" s="T60">pro.h:S</ta>
            <ta e="T64" id="Seg_824" s="T63">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_825" s="T1">pro.h:Poss</ta>
            <ta e="T3" id="Seg_826" s="T2">np:P</ta>
            <ta e="T6" id="Seg_827" s="T5">np:Th 0.3:Poss</ta>
            <ta e="T8" id="Seg_828" s="T7">np.h:A</ta>
            <ta e="T9" id="Seg_829" s="T8">pro.h:R</ta>
            <ta e="T11" id="Seg_830" s="T10">np:Th</ta>
            <ta e="T13" id="Seg_831" s="T12">pro.h:A</ta>
            <ta e="T14" id="Seg_832" s="T13">pro:Ins</ta>
            <ta e="T15" id="Seg_833" s="T14">0.3:Th</ta>
            <ta e="T16" id="Seg_834" s="T15">pro:P</ta>
            <ta e="T21" id="Seg_835" s="T20">adv:Time</ta>
            <ta e="T22" id="Seg_836" s="T21">pro.h:A</ta>
            <ta e="T23" id="Seg_837" s="T22">np:Ins</ta>
            <ta e="T27" id="Seg_838" s="T26">0.1.h:A 0.3:Th</ta>
            <ta e="T29" id="Seg_839" s="T28">pro.h:Poss</ta>
            <ta e="T31" id="Seg_840" s="T30">np:Th</ta>
            <ta e="T35" id="Seg_841" s="T34">np:P 0.1.h:Poss</ta>
            <ta e="T38" id="Seg_842" s="T37">np:Ins</ta>
            <ta e="T40" id="Seg_843" s="T39">0.1.h:A 0.3:Th</ta>
            <ta e="T41" id="Seg_844" s="T40">0.3:P</ta>
            <ta e="T43" id="Seg_845" s="T42">pro.h:R</ta>
            <ta e="T44" id="Seg_846" s="T43">0.3.h:A</ta>
            <ta e="T45" id="Seg_847" s="T44">0.2.h:Poss np:Th</ta>
            <ta e="T46" id="Seg_848" s="T45">np:Ins</ta>
            <ta e="T47" id="Seg_849" s="T46">0.2.h:A</ta>
            <ta e="T48" id="Seg_850" s="T47">np:P</ta>
            <ta e="T49" id="Seg_851" s="T48">0.2.h:A</ta>
            <ta e="T51" id="Seg_852" s="T50">np:G</ta>
            <ta e="T52" id="Seg_853" s="T51">0.2.h:A 0.3:Th</ta>
            <ta e="T53" id="Seg_854" s="T52">pro.h:A</ta>
            <ta e="T54" id="Seg_855" s="T53">adv:Time</ta>
            <ta e="T57" id="Seg_856" s="T56">0.1.h:R</ta>
            <ta e="T59" id="Seg_857" s="T58">np.h:A</ta>
            <ta e="T61" id="Seg_858" s="T60">pro.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_859" s="T2">RUS:cult</ta>
            <ta e="T8" id="Seg_860" s="T7">RUS:cult</ta>
            <ta e="T10" id="Seg_861" s="T9">RUS:gram</ta>
            <ta e="T11" id="Seg_862" s="T10">RUS:cult</ta>
            <ta e="T21" id="Seg_863" s="T20">RUS:disc</ta>
            <ta e="T28" id="Seg_864" s="T27">RUS:gram</ta>
            <ta e="T33" id="Seg_865" s="T32">RUS:disc</ta>
            <ta e="T35" id="Seg_866" s="T34">RUS:cult</ta>
            <ta e="T37" id="Seg_867" s="T36">RUS:gram</ta>
            <ta e="T42" id="Seg_868" s="T41">RUS:gram</ta>
            <ta e="T47" id="Seg_869" s="T46">RUS:cult</ta>
            <ta e="T48" id="Seg_870" s="T47">RUS:cult</ta>
            <ta e="T49" id="Seg_871" s="T48">RUS:core</ta>
            <ta e="T50" id="Seg_872" s="T49">RUS:gram</ta>
            <ta e="T63" id="Seg_873" s="T62">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T8" id="Seg_874" s="T7">medCdel medVdel</ta>
            <ta e="T47" id="Seg_875" s="T46">medCins</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T8" id="Seg_876" s="T7">dir:bare</ta>
            <ta e="T11" id="Seg_877" s="T10">dir:infl</ta>
            <ta e="T21" id="Seg_878" s="T20">dir:bare</ta>
            <ta e="T47" id="Seg_879" s="T46">parad:infl</ta>
            <ta e="T48" id="Seg_880" s="T47">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T18" id="Seg_881" s="T16">RUS:int.ins</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_882" s="T1">My piglings got sick.</ta>
            <ta e="T7" id="Seg_883" s="T5">Their skin hurt.</ta>
            <ta e="T12" id="Seg_884" s="T7">The veterinarian gave me some ointment.</ta>
            <ta e="T15" id="Seg_885" s="T12">I rubbed them with it.</ta>
            <ta e="T20" id="Seg_886" s="T15">It made them even worse.</ta>
            <ta e="T25" id="Seg_887" s="T20">Then I began washing them with urine.</ta>
            <ta e="T27" id="Seg_888" s="T25">I washed them two times.</ta>
            <ta e="T32" id="Seg_889" s="T27">And their skin became healthy.</ta>
            <ta e="T37" id="Seg_890" s="T32">Oh, they could have died, the two piglings.</ta>
            <ta e="T42" id="Seg_891" s="T37">They would have died, if I hadn't washed them with urine.</ta>
            <ta e="T47" id="Seg_892" s="T42">I was said: cure your legs with urine.</ta>
            <ta e="T52" id="Seg_893" s="T47">Moisten a rag and put it onto your legs.</ta>
            <ta e="T56" id="Seg_894" s="T52">Now I'll do so.</ta>
            <ta e="T60" id="Seg_895" s="T56">One old woman told me [to do so].</ta>
            <ta e="T64" id="Seg_896" s="T60">I'll do so.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_897" s="T1">Meine Ferkel wurden krank.</ta>
            <ta e="T7" id="Seg_898" s="T5">Ihre Haut schmerzte.</ta>
            <ta e="T12" id="Seg_899" s="T7">Der Tierarzt gab mir eine Salbe.</ta>
            <ta e="T15" id="Seg_900" s="T12">Ich rieb sie damit ein.</ta>
            <ta e="T20" id="Seg_901" s="T15">Das machte es nur noch schlimmer.</ta>
            <ta e="T25" id="Seg_902" s="T20">Dann begann ich sie mit Urin zu waschen.</ta>
            <ta e="T27" id="Seg_903" s="T25">Ich wusch sie zweimal.</ta>
            <ta e="T32" id="Seg_904" s="T27">Und ihre Haut wurde wieder gesund.</ta>
            <ta e="T37" id="Seg_905" s="T32">Oh, sie hätten sterben können, die beiden Ferkel.</ta>
            <ta e="T42" id="Seg_906" s="T37">Wenn ich sie nicht mit Urin gewaschen hätte, wären sie gestorben.</ta>
            <ta e="T47" id="Seg_907" s="T42">Mir wurde gesagt: Behandle deine Beine mit Urin.</ta>
            <ta e="T52" id="Seg_908" s="T47">Befeuchte einen Lappen und leg ihn auf deine Beine.</ta>
            <ta e="T56" id="Seg_909" s="T52">Das mache ich jetzt so.</ta>
            <ta e="T60" id="Seg_910" s="T56">Eine alte Frau sagte mir [das so zu machen].</ta>
            <ta e="T64" id="Seg_911" s="T60">Ich mache es so.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_912" s="T1">У меня поросята захворали.</ta>
            <ta e="T7" id="Seg_913" s="T5">У них кожа болела.</ta>
            <ta e="T12" id="Seg_914" s="T7">Ветеринар мне какую-то мазь давал.</ta>
            <ta e="T15" id="Seg_915" s="T12">Я ей намазала.</ta>
            <ta e="T20" id="Seg_916" s="T15">Им (двоим) еще хуже стало.</ta>
            <ta e="T25" id="Seg_917" s="T20">Потом я мочой мыть стала.</ta>
            <ta e="T27" id="Seg_918" s="T25">Два раза вымыла.</ta>
            <ta e="T32" id="Seg_919" s="T27">И у них (у меня) кожа зажила.</ta>
            <ta e="T37" id="Seg_920" s="T32">Ой, умерли бы два поросенка.</ta>
            <ta e="T42" id="Seg_921" s="T37">Мочой если бы не вымыла бы, они бы умерли.</ta>
            <ta e="T47" id="Seg_922" s="T42">Мне говорили: ноги мочой лечи.</ta>
            <ta e="T52" id="Seg_923" s="T47">Тряпку мочи и на ноги клади.</ta>
            <ta e="T56" id="Seg_924" s="T52">Я нынче так сделаю.</ta>
            <ta e="T60" id="Seg_925" s="T56">Мне одна старуха сказала.</ta>
            <ta e="T64" id="Seg_926" s="T60">Я так и сделаю.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_927" s="T1">у меня поросята захворали</ta>
            <ta e="T7" id="Seg_928" s="T5">у них кожа болела</ta>
            <ta e="T12" id="Seg_929" s="T7">ветеринар мне какой-то мази давал</ta>
            <ta e="T15" id="Seg_930" s="T12">я этим (этой мазью) намазала</ta>
            <ta e="T20" id="Seg_931" s="T15">им (двоим) еще хуже стало</ta>
            <ta e="T25" id="Seg_932" s="T20">потом я ссакой (мочой) мыть стала</ta>
            <ta e="T27" id="Seg_933" s="T25">два раза вымыла</ta>
            <ta e="T32" id="Seg_934" s="T27">у них кожа зажила</ta>
            <ta e="T37" id="Seg_935" s="T32">ой а то пропали бы (умерли бы) две чушки</ta>
            <ta e="T42" id="Seg_936" s="T37">ссакой если бы не вымыла они бы пропали</ta>
            <ta e="T47" id="Seg_937" s="T42">мне говорили ноги мочой лечи</ta>
            <ta e="T52" id="Seg_938" s="T47">тряпку мочи и на ноги клади</ta>
            <ta e="T56" id="Seg_939" s="T52">я нынче так сделаю</ta>
            <ta e="T60" id="Seg_940" s="T56">мне одна старуха сказала</ta>
            <ta e="T64" id="Seg_941" s="T60">я так и сделаю</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T15" id="Seg_942" s="T12">[BrM:] TR?</ta>
            <ta e="T42" id="Seg_943" s="T37">[BrM:] 'quː näɣə' changed to 'quːnäɣə'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
