<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_VisitingAuntMarina_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_VisitingAuntMarina_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">101</ud-information>
            <ud-information attribute-name="# HIAT:w">76</ud-information>
            <ud-information attribute-name="# e">76</ud-information>
            <ud-information attribute-name="# HIAT:u">16</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T77" id="Seg_0" n="sc" s="T1">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">kosti</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">közizan</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">taɣɨn</ts>
                  <nts id="Seg_14" n="HIAT:ip">,</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_17" n="HIAT:w" s="T5">Satdijedotdə</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_20" n="HIAT:w" s="T6">közizan</ts>
                  <nts id="Seg_21" n="HIAT:ip">.</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_24" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">Man</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">čaːʒan</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">magazinqɨndɨ</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_35" n="HIAT:w" s="T10">Vanʼän</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_38" n="HIAT:w" s="T11">mattɨ</ts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_42" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">I</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">Marajazʼe</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">qoɣɨdʼaj</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">Marinan</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_56" n="HIAT:w" s="T16">maːdɨn</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_59" n="HIAT:w" s="T17">köɣɨn</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_63" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">Marʼina</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">tʼarɨn</ts>
                  <nts id="Seg_69" n="HIAT:ip">:</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_71" n="HIAT:ip">“</nts>
                  <ts e="T21" id="Seg_73" n="HIAT:w" s="T20">Sernalʼe</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">mekga</ts>
                  <nts id="Seg_77" n="HIAT:ip">.</nts>
                  <nts id="Seg_78" n="HIAT:ip">”</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_81" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">Me</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">sernaj</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_90" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_92" n="HIAT:w" s="T24">Täp</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">qɨbanädekam</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">üdut</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">araqano</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_105" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_107" n="HIAT:w" s="T28">Täp</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">tannɨt</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">araqam</ts>
                  <nts id="Seg_114" n="HIAT:ip">,</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_117" n="HIAT:w" s="T31">nʼärɣa</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_120" n="HIAT:w" s="T32">araqɨm</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_123" n="HIAT:w" s="T33">tannɨt</ts>
                  <nts id="Seg_124" n="HIAT:ip">.</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_127" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_129" n="HIAT:w" s="T34">Täp</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_132" n="HIAT:w" s="T35">mezɨwe</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_135" n="HIAT:w" s="T36">ärčin</ts>
                  <nts id="Seg_136" n="HIAT:ip">.</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_139" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_141" n="HIAT:w" s="T37">Tʼärɨn</ts>
                  <nts id="Seg_142" n="HIAT:ip">:</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_144" n="HIAT:ip">“</nts>
                  <ts e="T39" id="Seg_146" n="HIAT:w" s="T38">Tau</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_149" n="HIAT:w" s="T39">Vera</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_152" n="HIAT:w" s="T40">taut</ts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip">”</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_157" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_159" n="HIAT:w" s="T41">Araqam</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_162" n="HIAT:w" s="T42">mazɨm</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_165" n="HIAT:w" s="T43">ärčin</ts>
                  <nts id="Seg_166" n="HIAT:ip">.</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_169" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_171" n="HIAT:w" s="T44">Tʼotka</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_174" n="HIAT:w" s="T45">Marʼinanä</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_177" n="HIAT:w" s="T46">nagɨr</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_180" n="HIAT:w" s="T47">nagɨrčeǯan</ts>
                  <nts id="Seg_181" n="HIAT:ip">.</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_184" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_186" n="HIAT:w" s="T48">Puskaj</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_189" n="HIAT:w" s="T49">togulǯimdə</ts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_193" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_195" n="HIAT:w" s="T50">Man</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_198" n="HIAT:w" s="T51">üdɨn</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_201" n="HIAT:w" s="T52">Satdijedot</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_204" n="HIAT:w" s="T53">tütǯan</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_207" n="HIAT:w" s="T54">qwɛlɨsku</ts>
                  <nts id="Seg_208" n="HIAT:ip">,</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_211" n="HIAT:w" s="T55">čobərɨm</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_214" n="HIAT:w" s="T56">qwadəgu</ts>
                  <nts id="Seg_215" n="HIAT:ip">.</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_218" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_220" n="HIAT:w" s="T57">Man</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_223" n="HIAT:w" s="T58">elan</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_226" n="HIAT:w" s="T59">näjɣunnan</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_229" n="HIAT:w" s="T60">mäɣuntə</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_232" n="HIAT:w" s="T61">na</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_235" n="HIAT:w" s="T62">tükkutda</ts>
                  <nts id="Seg_236" n="HIAT:ip">.</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_239" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_241" n="HIAT:w" s="T63">Süsöga</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_244" n="HIAT:w" s="T64">selam</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_247" n="HIAT:w" s="T65">nagɨrlʼe</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_250" n="HIAT:w" s="T66">tadərɨst</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_253" n="HIAT:w" s="T67">tekga</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_256" n="HIAT:w" s="T68">balʼdʼukus</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_259" n="HIAT:w" s="T69">tazʼe</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_262" n="HIAT:w" s="T70">qɨːdan</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_265" n="HIAT:w" s="T71">nagɨrčukus</ts>
                  <nts id="Seg_266" n="HIAT:ip">.</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_269" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_271" n="HIAT:w" s="T72">Man</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_274" n="HIAT:w" s="T73">na</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_277" n="HIAT:w" s="T74">näjɣunnan</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_280" n="HIAT:w" s="T75">warkan</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_283" n="HIAT:w" s="T76">Nowosibirskan</ts>
                  <nts id="Seg_284" n="HIAT:ip">.</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T77" id="Seg_286" n="sc" s="T1">
               <ts e="T2" id="Seg_288" n="e" s="T1">Man </ts>
               <ts e="T3" id="Seg_290" n="e" s="T2">kosti </ts>
               <ts e="T4" id="Seg_292" n="e" s="T3">közizan </ts>
               <ts e="T5" id="Seg_294" n="e" s="T4">taɣɨn, </ts>
               <ts e="T6" id="Seg_296" n="e" s="T5">Satdijedotdə </ts>
               <ts e="T7" id="Seg_298" n="e" s="T6">közizan. </ts>
               <ts e="T8" id="Seg_300" n="e" s="T7">Man </ts>
               <ts e="T9" id="Seg_302" n="e" s="T8">čaːʒan </ts>
               <ts e="T10" id="Seg_304" n="e" s="T9">magazinqɨndɨ </ts>
               <ts e="T11" id="Seg_306" n="e" s="T10">Vanʼän </ts>
               <ts e="T12" id="Seg_308" n="e" s="T11">mattɨ. </ts>
               <ts e="T13" id="Seg_310" n="e" s="T12">I </ts>
               <ts e="T14" id="Seg_312" n="e" s="T13">Marajazʼe </ts>
               <ts e="T15" id="Seg_314" n="e" s="T14">qoɣɨdʼaj </ts>
               <ts e="T16" id="Seg_316" n="e" s="T15">Marinan </ts>
               <ts e="T17" id="Seg_318" n="e" s="T16">maːdɨn </ts>
               <ts e="T18" id="Seg_320" n="e" s="T17">köɣɨn. </ts>
               <ts e="T19" id="Seg_322" n="e" s="T18">Marʼina </ts>
               <ts e="T20" id="Seg_324" n="e" s="T19">tʼarɨn: </ts>
               <ts e="T21" id="Seg_326" n="e" s="T20">“Sernalʼe </ts>
               <ts e="T22" id="Seg_328" n="e" s="T21">mekga.” </ts>
               <ts e="T23" id="Seg_330" n="e" s="T22">Me </ts>
               <ts e="T24" id="Seg_332" n="e" s="T23">sernaj. </ts>
               <ts e="T25" id="Seg_334" n="e" s="T24">Täp </ts>
               <ts e="T26" id="Seg_336" n="e" s="T25">qɨbanädekam </ts>
               <ts e="T27" id="Seg_338" n="e" s="T26">üdut </ts>
               <ts e="T28" id="Seg_340" n="e" s="T27">araqano. </ts>
               <ts e="T29" id="Seg_342" n="e" s="T28">Täp </ts>
               <ts e="T30" id="Seg_344" n="e" s="T29">tannɨt </ts>
               <ts e="T31" id="Seg_346" n="e" s="T30">araqam, </ts>
               <ts e="T32" id="Seg_348" n="e" s="T31">nʼärɣa </ts>
               <ts e="T33" id="Seg_350" n="e" s="T32">araqɨm </ts>
               <ts e="T34" id="Seg_352" n="e" s="T33">tannɨt. </ts>
               <ts e="T35" id="Seg_354" n="e" s="T34">Täp </ts>
               <ts e="T36" id="Seg_356" n="e" s="T35">mezɨwe </ts>
               <ts e="T37" id="Seg_358" n="e" s="T36">ärčin. </ts>
               <ts e="T38" id="Seg_360" n="e" s="T37">Tʼärɨn: </ts>
               <ts e="T39" id="Seg_362" n="e" s="T38">“Tau </ts>
               <ts e="T40" id="Seg_364" n="e" s="T39">Vera </ts>
               <ts e="T41" id="Seg_366" n="e" s="T40">taut.” </ts>
               <ts e="T42" id="Seg_368" n="e" s="T41">Araqam </ts>
               <ts e="T43" id="Seg_370" n="e" s="T42">mazɨm </ts>
               <ts e="T44" id="Seg_372" n="e" s="T43">ärčin. </ts>
               <ts e="T45" id="Seg_374" n="e" s="T44">Tʼotka </ts>
               <ts e="T46" id="Seg_376" n="e" s="T45">Marʼinanä </ts>
               <ts e="T47" id="Seg_378" n="e" s="T46">nagɨr </ts>
               <ts e="T48" id="Seg_380" n="e" s="T47">nagɨrčeǯan. </ts>
               <ts e="T49" id="Seg_382" n="e" s="T48">Puskaj </ts>
               <ts e="T50" id="Seg_384" n="e" s="T49">togulǯimdə. </ts>
               <ts e="T51" id="Seg_386" n="e" s="T50">Man </ts>
               <ts e="T52" id="Seg_388" n="e" s="T51">üdɨn </ts>
               <ts e="T53" id="Seg_390" n="e" s="T52">Satdijedot </ts>
               <ts e="T54" id="Seg_392" n="e" s="T53">tütǯan </ts>
               <ts e="T55" id="Seg_394" n="e" s="T54">qwɛlɨsku, </ts>
               <ts e="T56" id="Seg_396" n="e" s="T55">čobərɨm </ts>
               <ts e="T57" id="Seg_398" n="e" s="T56">qwadəgu. </ts>
               <ts e="T58" id="Seg_400" n="e" s="T57">Man </ts>
               <ts e="T59" id="Seg_402" n="e" s="T58">elan </ts>
               <ts e="T60" id="Seg_404" n="e" s="T59">näjɣunnan </ts>
               <ts e="T61" id="Seg_406" n="e" s="T60">mäɣuntə </ts>
               <ts e="T62" id="Seg_408" n="e" s="T61">na </ts>
               <ts e="T63" id="Seg_410" n="e" s="T62">tükkutda. </ts>
               <ts e="T64" id="Seg_412" n="e" s="T63">Süsöga </ts>
               <ts e="T65" id="Seg_414" n="e" s="T64">selam </ts>
               <ts e="T66" id="Seg_416" n="e" s="T65">nagɨrlʼe </ts>
               <ts e="T67" id="Seg_418" n="e" s="T66">tadərɨst </ts>
               <ts e="T68" id="Seg_420" n="e" s="T67">tekga </ts>
               <ts e="T69" id="Seg_422" n="e" s="T68">balʼdʼukus </ts>
               <ts e="T70" id="Seg_424" n="e" s="T69">tazʼe </ts>
               <ts e="T71" id="Seg_426" n="e" s="T70">qɨːdan </ts>
               <ts e="T72" id="Seg_428" n="e" s="T71">nagɨrčukus. </ts>
               <ts e="T73" id="Seg_430" n="e" s="T72">Man </ts>
               <ts e="T74" id="Seg_432" n="e" s="T73">na </ts>
               <ts e="T75" id="Seg_434" n="e" s="T74">näjɣunnan </ts>
               <ts e="T76" id="Seg_436" n="e" s="T75">warkan </ts>
               <ts e="T77" id="Seg_438" n="e" s="T76">Nowosibirskan. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_439" s="T1">PVD_1964_VisitingAuntMarina_nar.001 (001.001)</ta>
            <ta e="T12" id="Seg_440" s="T7">PVD_1964_VisitingAuntMarina_nar.002 (001.002)</ta>
            <ta e="T18" id="Seg_441" s="T12">PVD_1964_VisitingAuntMarina_nar.003 (001.003)</ta>
            <ta e="T22" id="Seg_442" s="T18">PVD_1964_VisitingAuntMarina_nar.004 (001.004)</ta>
            <ta e="T24" id="Seg_443" s="T22">PVD_1964_VisitingAuntMarina_nar.005 (001.005)</ta>
            <ta e="T28" id="Seg_444" s="T24">PVD_1964_VisitingAuntMarina_nar.006 (001.006)</ta>
            <ta e="T34" id="Seg_445" s="T28">PVD_1964_VisitingAuntMarina_nar.007 (001.007)</ta>
            <ta e="T37" id="Seg_446" s="T34">PVD_1964_VisitingAuntMarina_nar.008 (001.008)</ta>
            <ta e="T41" id="Seg_447" s="T37">PVD_1964_VisitingAuntMarina_nar.009 (001.009)</ta>
            <ta e="T44" id="Seg_448" s="T41">PVD_1964_VisitingAuntMarina_nar.010 (001.010)</ta>
            <ta e="T48" id="Seg_449" s="T44">PVD_1964_VisitingAuntMarina_nar.011 (001.011)</ta>
            <ta e="T50" id="Seg_450" s="T48">PVD_1964_VisitingAuntMarina_nar.012 (001.012)</ta>
            <ta e="T57" id="Seg_451" s="T50">PVD_1964_VisitingAuntMarina_nar.013 (001.013)</ta>
            <ta e="T63" id="Seg_452" s="T57">PVD_1964_VisitingAuntMarina_nar.014 (001.014)</ta>
            <ta e="T72" id="Seg_453" s="T63">PVD_1964_VisitingAuntMarina_nar.015 (001.015)</ta>
            <ta e="T77" id="Seg_454" s="T72">PVD_1964_VisitingAuntMarina_nar.016 (001.016)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T7" id="Seg_455" s="T1">ман кости ′кӧзи‵зан та′ɣын, ′сатд̂иjедотдъ ′кӧзизан.</ta>
            <ta e="T12" id="Seg_456" s="T7">ман ′тша̄жан магазинkын‵ды ′Ванʼӓн ′матты.</ta>
            <ta e="T18" id="Seg_457" s="T12">и Ма′раjазʼе kоɣы′дʼай Ма′ринан ′ма̄дын ′кӧɣын.</ta>
            <ta e="T22" id="Seg_458" s="T18">Марʼина тʼа′рын: ′серналʼе ′мекг̂а.</ta>
            <ta e="T24" id="Seg_459" s="T22">ме ′сернай.</ta>
            <ta e="T28" id="Seg_460" s="T24">тӓп kыбанӓ′де̨кам ′ӱдут а′раkано.</ta>
            <ta e="T34" id="Seg_461" s="T28">тӓп та′нныт а′раkам, ′нʼӓрɣа а′раkым танныт.</ta>
            <ta e="T37" id="Seg_462" s="T34">тӓп мезы′ве ′ӓртшин.</ta>
            <ta e="T41" id="Seg_463" s="T37">тʼӓ′рын ′тау ′Вера ′таут.</ta>
            <ta e="T44" id="Seg_464" s="T41">′араkам ма′зым ′ӓртшин.</ta>
            <ta e="T48" id="Seg_465" s="T44">тʼотка Марʼинанӓ ′нагыр ′нагыртше‵джан.</ta>
            <ta e="T50" id="Seg_466" s="T48">пускай ′тогулджимдъ.</ta>
            <ta e="T57" id="Seg_467" s="T50">ман ′ӱдын ‵сатди′jедот ′тӱтджан ′kwɛлыску, ′тшобърым ′kwадъгу.</ta>
            <ta e="T63" id="Seg_468" s="T57">ман е′лан нӓйɣун′нан мӓ′ɣунтъ на ′тӱккутда.</ta>
            <ta e="T72" id="Seg_469" s="T63">сӱ′сӧга ′селам нагыр′лʼе тадърыст ′текга ′б̂алʼдʼукус тазʼе kы̄дан ′нагыртшукус.</ta>
            <ta e="T77" id="Seg_470" s="T72">ман на нӓйɣуннан вар′кан Новоси′бирскан.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T7" id="Seg_471" s="T1">man kosti közizan taɣɨn, satd̂ijedotdə közizan.</ta>
            <ta e="T12" id="Seg_472" s="T7">man tšaːʒan magazinqɨndɨ Вanʼän mattɨ.</ta>
            <ta e="T18" id="Seg_473" s="T12">i Мarajazʼe qoɣɨdʼaj Мarinan maːdɨn köɣɨn.</ta>
            <ta e="T22" id="Seg_474" s="T18">Мarʼina tʼarɨn: sernalʼe mekĝa.</ta>
            <ta e="T24" id="Seg_475" s="T22">me sernaj.</ta>
            <ta e="T28" id="Seg_476" s="T24">täp qɨbanädekam üdut araqano.</ta>
            <ta e="T34" id="Seg_477" s="T28">täp tannɨt araqam, nʼärɣa araqɨm tannɨt.</ta>
            <ta e="T37" id="Seg_478" s="T34">täp mezɨwe ärtšin.</ta>
            <ta e="T41" id="Seg_479" s="T37">tʼärɨn tau Вera taut.</ta>
            <ta e="T44" id="Seg_480" s="T41">araqam mazɨm ärtšin.</ta>
            <ta e="T48" id="Seg_481" s="T44">tʼotka Мarʼinanä nagɨr nagɨrtšeǯan.</ta>
            <ta e="T50" id="Seg_482" s="T48">puskaj togulǯimdə.</ta>
            <ta e="T57" id="Seg_483" s="T50">man üdɨn satdijedot tütǯan qwɛlɨsku, tšobərɨm qwadəgu.</ta>
            <ta e="T63" id="Seg_484" s="T57">man elan näjɣunnan mäɣuntə na tükkutda.</ta>
            <ta e="T72" id="Seg_485" s="T63">süsöga selam nagɨrlʼe tadərɨst tekga b̂alʼdʼukus tazʼe qɨːdan nagɨrtšukus.</ta>
            <ta e="T77" id="Seg_486" s="T72">man na näjɣunnan warkan Нowosibirskan.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_487" s="T1">Man kosti közizan taɣɨn, Satdijedotdə közizan. </ta>
            <ta e="T12" id="Seg_488" s="T7">Man čaːʒan magazinqɨndɨ Vanʼän mattɨ. </ta>
            <ta e="T18" id="Seg_489" s="T12">I Marajazʼe qoɣɨdʼaj Marinan maːdɨn köɣɨn. </ta>
            <ta e="T22" id="Seg_490" s="T18">Marʼina tʼarɨn: “Sernalʼe mekga.” </ta>
            <ta e="T24" id="Seg_491" s="T22">Me sernaj. </ta>
            <ta e="T28" id="Seg_492" s="T24">Täp qɨbanädekam üdut araqano. </ta>
            <ta e="T34" id="Seg_493" s="T28">Täp tannɨt araqam, nʼärɣa araqɨm tannɨt. </ta>
            <ta e="T37" id="Seg_494" s="T34">Täp mezɨwe ärčin. </ta>
            <ta e="T41" id="Seg_495" s="T37">Tʼärɨn: “Tau Vera taut.” </ta>
            <ta e="T44" id="Seg_496" s="T41">Araqam mazɨm ärčin. </ta>
            <ta e="T48" id="Seg_497" s="T44">Tʼotka Marʼinanä nagɨr nagɨrčeǯan. </ta>
            <ta e="T50" id="Seg_498" s="T48">Puskaj togulǯimdə. </ta>
            <ta e="T57" id="Seg_499" s="T50">Man üdɨn Satdijedot tütǯan qwɛlɨsku, čobərɨm qwadəgu. </ta>
            <ta e="T63" id="Seg_500" s="T57">Man elan näjɣunnan mäɣuntə na tükkutda. </ta>
            <ta e="T72" id="Seg_501" s="T63">Süsöga selam nagɨrlʼe tadərɨst tekga balʼdʼukus tazʼe qɨːdan nagɨrčukus. </ta>
            <ta e="T77" id="Seg_502" s="T72">Man na näjɣunnan warkan Nowosibirskan. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_503" s="T1">man</ta>
            <ta e="T3" id="Seg_504" s="T2">kosti</ta>
            <ta e="T4" id="Seg_505" s="T3">közi-za-n</ta>
            <ta e="T5" id="Seg_506" s="T4">taɣ-ɨ-n</ta>
            <ta e="T6" id="Seg_507" s="T5">Satdijedo-tdə</ta>
            <ta e="T7" id="Seg_508" s="T6">közi-za-n</ta>
            <ta e="T8" id="Seg_509" s="T7">man</ta>
            <ta e="T9" id="Seg_510" s="T8">čaːʒa-n</ta>
            <ta e="T10" id="Seg_511" s="T9">magazin-qɨndɨ</ta>
            <ta e="T11" id="Seg_512" s="T10">Vanʼä-n</ta>
            <ta e="T12" id="Seg_513" s="T11">mat-tɨ</ta>
            <ta e="T13" id="Seg_514" s="T12">i</ta>
            <ta e="T14" id="Seg_515" s="T13">Maraja-zʼe</ta>
            <ta e="T15" id="Seg_516" s="T14">qo-ɣɨ-dʼa-j</ta>
            <ta e="T16" id="Seg_517" s="T15">Marina-n</ta>
            <ta e="T17" id="Seg_518" s="T16">maːd-ɨ-n</ta>
            <ta e="T18" id="Seg_519" s="T17">kö-ɣɨn</ta>
            <ta e="T19" id="Seg_520" s="T18">Marʼina</ta>
            <ta e="T20" id="Seg_521" s="T19">tʼarɨ-n</ta>
            <ta e="T21" id="Seg_522" s="T20">ser-nalʼe</ta>
            <ta e="T22" id="Seg_523" s="T21">mekga</ta>
            <ta e="T23" id="Seg_524" s="T22">me</ta>
            <ta e="T24" id="Seg_525" s="T23">ser-na-j</ta>
            <ta e="T25" id="Seg_526" s="T24">täp</ta>
            <ta e="T26" id="Seg_527" s="T25">qɨba-nädek-a-m</ta>
            <ta e="T27" id="Seg_528" s="T26">üdu-t</ta>
            <ta e="T28" id="Seg_529" s="T27">araqa-no</ta>
            <ta e="T29" id="Seg_530" s="T28">täp</ta>
            <ta e="T30" id="Seg_531" s="T29">tan-nɨ-t</ta>
            <ta e="T31" id="Seg_532" s="T30">araqa-m</ta>
            <ta e="T32" id="Seg_533" s="T31">nʼärɣa</ta>
            <ta e="T33" id="Seg_534" s="T32">araqɨ-m</ta>
            <ta e="T34" id="Seg_535" s="T33">tan-nɨ-t</ta>
            <ta e="T35" id="Seg_536" s="T34">täp</ta>
            <ta e="T36" id="Seg_537" s="T35">mezɨwe</ta>
            <ta e="T37" id="Seg_538" s="T36">är-či-n</ta>
            <ta e="T38" id="Seg_539" s="T37">tʼärɨ-n</ta>
            <ta e="T39" id="Seg_540" s="T38">tau</ta>
            <ta e="T40" id="Seg_541" s="T39">Vera</ta>
            <ta e="T41" id="Seg_542" s="T40">tau-t</ta>
            <ta e="T42" id="Seg_543" s="T41">araqa-m</ta>
            <ta e="T43" id="Seg_544" s="T42">mazɨm</ta>
            <ta e="T44" id="Seg_545" s="T43">är-či-n</ta>
            <ta e="T45" id="Seg_546" s="T44">tʼotka</ta>
            <ta e="T46" id="Seg_547" s="T45">Marʼina-nä</ta>
            <ta e="T47" id="Seg_548" s="T46">nagɨr</ta>
            <ta e="T48" id="Seg_549" s="T47">nagɨr-č-eǯa-n</ta>
            <ta e="T49" id="Seg_550" s="T48">puskaj</ta>
            <ta e="T50" id="Seg_551" s="T49">togu-lǯi-mdə</ta>
            <ta e="T51" id="Seg_552" s="T50">man</ta>
            <ta e="T52" id="Seg_553" s="T51">üdɨn</ta>
            <ta e="T53" id="Seg_554" s="T52">Satdijedo-t</ta>
            <ta e="T54" id="Seg_555" s="T53">tü-tǯa-n</ta>
            <ta e="T55" id="Seg_556" s="T54">qwɛl-ɨ-s-ku</ta>
            <ta e="T56" id="Seg_557" s="T55">čobər-ɨ-m</ta>
            <ta e="T57" id="Seg_558" s="T56">qwad-ə-gu</ta>
            <ta e="T58" id="Seg_559" s="T57">man</ta>
            <ta e="T59" id="Seg_560" s="T58">ela-n</ta>
            <ta e="T60" id="Seg_561" s="T59">nä-j-ɣun-nan</ta>
            <ta e="T61" id="Seg_562" s="T60">mäɣuntə</ta>
            <ta e="T62" id="Seg_563" s="T61">na</ta>
            <ta e="T63" id="Seg_564" s="T62">tü-kku-tda</ta>
            <ta e="T64" id="Seg_565" s="T63">süsöga</ta>
            <ta e="T65" id="Seg_566" s="T64">se-la-m</ta>
            <ta e="T66" id="Seg_567" s="T65">nagɨr-lʼe</ta>
            <ta e="T67" id="Seg_568" s="T66">tad-ə-r-ɨ-s-t</ta>
            <ta e="T68" id="Seg_569" s="T67">tekga</ta>
            <ta e="T69" id="Seg_570" s="T68">balʼdʼu-ku-s</ta>
            <ta e="T70" id="Seg_571" s="T69">ta-zʼe</ta>
            <ta e="T71" id="Seg_572" s="T70">qɨːdan</ta>
            <ta e="T72" id="Seg_573" s="T71">nagɨr-ču-ku-s</ta>
            <ta e="T73" id="Seg_574" s="T72">man</ta>
            <ta e="T74" id="Seg_575" s="T73">na</ta>
            <ta e="T75" id="Seg_576" s="T74">nä-j-ɣun-nan</ta>
            <ta e="T76" id="Seg_577" s="T75">warka-n</ta>
            <ta e="T77" id="Seg_578" s="T76">Nowosibirs-kan</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_579" s="T1">man</ta>
            <ta e="T3" id="Seg_580" s="T2">kosti</ta>
            <ta e="T4" id="Seg_581" s="T3">közi-sɨ-ŋ</ta>
            <ta e="T5" id="Seg_582" s="T4">taɣ-ɨ-ŋ</ta>
            <ta e="T6" id="Seg_583" s="T5">Satdijedo-ntə</ta>
            <ta e="T7" id="Seg_584" s="T6">közi-sɨ-ŋ</ta>
            <ta e="T8" id="Seg_585" s="T7">man</ta>
            <ta e="T9" id="Seg_586" s="T8">čaǯɨ-ŋ</ta>
            <ta e="T10" id="Seg_587" s="T9">magazin-qɨntɨ</ta>
            <ta e="T11" id="Seg_588" s="T10">Vanʼä-n</ta>
            <ta e="T12" id="Seg_589" s="T11">maːt-ntə</ta>
            <ta e="T13" id="Seg_590" s="T12">i</ta>
            <ta e="T14" id="Seg_591" s="T13">Мarʼija-se</ta>
            <ta e="T15" id="Seg_592" s="T14">qo-kɨ-dʼi-j</ta>
            <ta e="T16" id="Seg_593" s="T15">Marʼina-n</ta>
            <ta e="T17" id="Seg_594" s="T16">maːt-ɨ-n</ta>
            <ta e="T18" id="Seg_595" s="T17">kö-qɨn</ta>
            <ta e="T19" id="Seg_596" s="T18">Marʼina</ta>
            <ta e="T20" id="Seg_597" s="T19">tʼärɨ-n</ta>
            <ta e="T21" id="Seg_598" s="T20">ser-naltə</ta>
            <ta e="T22" id="Seg_599" s="T21">mekka</ta>
            <ta e="T23" id="Seg_600" s="T22">me</ta>
            <ta e="T24" id="Seg_601" s="T23">ser-nɨ-j</ta>
            <ta e="T25" id="Seg_602" s="T24">täp</ta>
            <ta e="T26" id="Seg_603" s="T25">qɨba-nädek-ɨ-m</ta>
            <ta e="T27" id="Seg_604" s="T26">üdə-t</ta>
            <ta e="T28" id="Seg_605" s="T27">araŋka-no</ta>
            <ta e="T29" id="Seg_606" s="T28">täp</ta>
            <ta e="T30" id="Seg_607" s="T29">tat-nɨ-t</ta>
            <ta e="T31" id="Seg_608" s="T30">araŋka-m</ta>
            <ta e="T32" id="Seg_609" s="T31">nʼarɣə</ta>
            <ta e="T33" id="Seg_610" s="T32">araŋka-m</ta>
            <ta e="T34" id="Seg_611" s="T33">tat-nɨ-t</ta>
            <ta e="T35" id="Seg_612" s="T34">täp</ta>
            <ta e="T36" id="Seg_613" s="T35">mezuwi</ta>
            <ta e="T37" id="Seg_614" s="T36">öru-ču-n</ta>
            <ta e="T38" id="Seg_615" s="T37">tʼärɨ-n</ta>
            <ta e="T39" id="Seg_616" s="T38">taw</ta>
            <ta e="T40" id="Seg_617" s="T39">Vera</ta>
            <ta e="T41" id="Seg_618" s="T40">taw-t</ta>
            <ta e="T42" id="Seg_619" s="T41">araŋka-w</ta>
            <ta e="T43" id="Seg_620" s="T42">mazɨm</ta>
            <ta e="T44" id="Seg_621" s="T43">öru-ču-n</ta>
            <ta e="T45" id="Seg_622" s="T44">tʼötka</ta>
            <ta e="T46" id="Seg_623" s="T45">Marʼina-nä</ta>
            <ta e="T47" id="Seg_624" s="T46">nagər</ta>
            <ta e="T48" id="Seg_625" s="T47">nagər-ču-enǯɨ-ŋ</ta>
            <ta e="T49" id="Seg_626" s="T48">puskaj</ta>
            <ta e="T50" id="Seg_627" s="T49">*toːqo-lǯi-mtə</ta>
            <ta e="T51" id="Seg_628" s="T50">man</ta>
            <ta e="T52" id="Seg_629" s="T51">üdɨn</ta>
            <ta e="T53" id="Seg_630" s="T52">Satdijedo-ntə</ta>
            <ta e="T54" id="Seg_631" s="T53">tüː-enǯɨ-ŋ</ta>
            <ta e="T55" id="Seg_632" s="T54">qwɛl-ɨ-s-gu</ta>
            <ta e="T56" id="Seg_633" s="T55">čobər-ɨ-m</ta>
            <ta e="T57" id="Seg_634" s="T56">qwat-ɨ-gu</ta>
            <ta e="T58" id="Seg_635" s="T57">man</ta>
            <ta e="T59" id="Seg_636" s="T58">elɨ-ŋ</ta>
            <ta e="T60" id="Seg_637" s="T59">ne-lʼ-qum-nan</ta>
            <ta e="T61" id="Seg_638" s="T60">mäɣuntə</ta>
            <ta e="T62" id="Seg_639" s="T61">na</ta>
            <ta e="T63" id="Seg_640" s="T62">tüː-ku-ntɨ</ta>
            <ta e="T64" id="Seg_641" s="T63">süsögi</ta>
            <ta e="T65" id="Seg_642" s="T64">se-la-m</ta>
            <ta e="T66" id="Seg_643" s="T65">nagər-le</ta>
            <ta e="T67" id="Seg_644" s="T66">tat-ɨ-r-ɨ-sɨ-t</ta>
            <ta e="T68" id="Seg_645" s="T67">tekka</ta>
            <ta e="T69" id="Seg_646" s="T68">palʼdʼi-ku-sɨ</ta>
            <ta e="T70" id="Seg_647" s="T69">tan-se</ta>
            <ta e="T71" id="Seg_648" s="T70">qɨdan</ta>
            <ta e="T72" id="Seg_649" s="T71">nagər-ču-ku-sɨ</ta>
            <ta e="T73" id="Seg_650" s="T72">man</ta>
            <ta e="T74" id="Seg_651" s="T73">na</ta>
            <ta e="T75" id="Seg_652" s="T74">ne-lʼ-qum-nan</ta>
            <ta e="T76" id="Seg_653" s="T75">warkɨ-ŋ</ta>
            <ta e="T77" id="Seg_654" s="T76">Nowosibirsk-qɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_655" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_656" s="T2">visiting</ta>
            <ta e="T4" id="Seg_657" s="T3">go.by-PST-1SG.S</ta>
            <ta e="T5" id="Seg_658" s="T4">summer-EP-ADVZ</ta>
            <ta e="T6" id="Seg_659" s="T5">Starosondorovo-ILL</ta>
            <ta e="T7" id="Seg_660" s="T6">go.by-PST-1SG.S</ta>
            <ta e="T8" id="Seg_661" s="T7">I.NOM</ta>
            <ta e="T9" id="Seg_662" s="T8">go-1SG.S</ta>
            <ta e="T10" id="Seg_663" s="T9">shop-EL.3SG</ta>
            <ta e="T11" id="Seg_664" s="T10">Vanya-GEN</ta>
            <ta e="T12" id="Seg_665" s="T11">house-ILL</ta>
            <ta e="T13" id="Seg_666" s="T12">and</ta>
            <ta e="T14" id="Seg_667" s="T13">Maria-COM</ta>
            <ta e="T15" id="Seg_668" s="T14">see-RFL-DRV-1DU</ta>
            <ta e="T16" id="Seg_669" s="T15">Marina-GEN</ta>
            <ta e="T17" id="Seg_670" s="T16">house-EP-GEN</ta>
            <ta e="T18" id="Seg_671" s="T17">side-LOC</ta>
            <ta e="T19" id="Seg_672" s="T18">Marina.[NOM]</ta>
            <ta e="T20" id="Seg_673" s="T19">say-3SG.S</ta>
            <ta e="T21" id="Seg_674" s="T20">come.in-IMP.2PL.S/O</ta>
            <ta e="T22" id="Seg_675" s="T21">I.ALL</ta>
            <ta e="T23" id="Seg_676" s="T22">we.[NOM]</ta>
            <ta e="T24" id="Seg_677" s="T23">come.in-CO-1DU</ta>
            <ta e="T25" id="Seg_678" s="T24">(s)he.[NOM]</ta>
            <ta e="T26" id="Seg_679" s="T25">small-girl-EP-ACC</ta>
            <ta e="T27" id="Seg_680" s="T26">send-3SG.O</ta>
            <ta e="T28" id="Seg_681" s="T27">wine-TRL</ta>
            <ta e="T29" id="Seg_682" s="T28">(s)he.[NOM]</ta>
            <ta e="T30" id="Seg_683" s="T29">bring-CO-3SG.O</ta>
            <ta e="T31" id="Seg_684" s="T30">wine-ACC</ta>
            <ta e="T32" id="Seg_685" s="T31">red</ta>
            <ta e="T33" id="Seg_686" s="T32">wine-ACC</ta>
            <ta e="T34" id="Seg_687" s="T33">bring-CO-3SG.O</ta>
            <ta e="T35" id="Seg_688" s="T34">(s)he.[NOM]</ta>
            <ta e="T36" id="Seg_689" s="T35">we.ACC</ta>
            <ta e="T37" id="Seg_690" s="T36">drink-TR-3SG.S</ta>
            <ta e="T38" id="Seg_691" s="T37">say-3SG.S</ta>
            <ta e="T39" id="Seg_692" s="T38">this</ta>
            <ta e="T40" id="Seg_693" s="T39">Vera.[NOM]</ta>
            <ta e="T41" id="Seg_694" s="T40">buy-3SG.O</ta>
            <ta e="T42" id="Seg_695" s="T41">wine.[NOM]-1SG</ta>
            <ta e="T43" id="Seg_696" s="T42">I.ACC</ta>
            <ta e="T44" id="Seg_697" s="T43">drink-TR-3SG.S</ta>
            <ta e="T45" id="Seg_698" s="T44">aunt.[NOM]</ta>
            <ta e="T46" id="Seg_699" s="T45">Marina-ALL</ta>
            <ta e="T47" id="Seg_700" s="T46">letter.[NOM]</ta>
            <ta e="T48" id="Seg_701" s="T47">letter-VBLZ-FUT-1SG.S</ta>
            <ta e="T49" id="Seg_702" s="T48">JUSS</ta>
            <ta e="T50" id="Seg_703" s="T49">read-PFV-IMP.3SG/PL.O</ta>
            <ta e="T51" id="Seg_704" s="T50">I.NOM</ta>
            <ta e="T52" id="Seg_705" s="T51">in.spring</ta>
            <ta e="T53" id="Seg_706" s="T52">Starosondorovo-ILL</ta>
            <ta e="T54" id="Seg_707" s="T53">come-FUT-1SG.S</ta>
            <ta e="T55" id="Seg_708" s="T54">fish-EP-CAP-INF</ta>
            <ta e="T56" id="Seg_709" s="T55">berry-EP-ACC</ta>
            <ta e="T57" id="Seg_710" s="T56">manage.to.get-EP-INF</ta>
            <ta e="T58" id="Seg_711" s="T57">I.NOM</ta>
            <ta e="T59" id="Seg_712" s="T58">live-1SG.S</ta>
            <ta e="T60" id="Seg_713" s="T59">woman-ADJZ-human.being-ADES</ta>
            <ta e="T61" id="Seg_714" s="T60">we.PL.ALL</ta>
            <ta e="T62" id="Seg_715" s="T61">here</ta>
            <ta e="T63" id="Seg_716" s="T62">come-HAB-INFER.[3SG.S]</ta>
            <ta e="T64" id="Seg_717" s="T63">Selkup</ta>
            <ta e="T65" id="Seg_718" s="T64">language-PL-ACC</ta>
            <ta e="T66" id="Seg_719" s="T65">write-CVB</ta>
            <ta e="T67" id="Seg_720" s="T66">bring-EP-FRQ-EP-PST-3SG.O</ta>
            <ta e="T68" id="Seg_721" s="T67">you.ALL</ta>
            <ta e="T69" id="Seg_722" s="T68">walk-HAB-PST.[3SG.S]</ta>
            <ta e="T70" id="Seg_723" s="T69">you.SG-COM</ta>
            <ta e="T71" id="Seg_724" s="T70">all.the.time</ta>
            <ta e="T72" id="Seg_725" s="T71">letter-VBLZ-HAB-PST.[3SG.S]</ta>
            <ta e="T73" id="Seg_726" s="T72">I.NOM</ta>
            <ta e="T74" id="Seg_727" s="T73">this</ta>
            <ta e="T75" id="Seg_728" s="T74">woman-ADJZ-human.being-ADES</ta>
            <ta e="T76" id="Seg_729" s="T75">live-1SG.S</ta>
            <ta e="T77" id="Seg_730" s="T76">Novosibirsk-LOC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_731" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_732" s="T2">в.гости</ta>
            <ta e="T4" id="Seg_733" s="T3">ездить-PST-1SG.S</ta>
            <ta e="T5" id="Seg_734" s="T4">лето-EP-ADVZ</ta>
            <ta e="T6" id="Seg_735" s="T5">Старосондорово-ILL</ta>
            <ta e="T7" id="Seg_736" s="T6">ездить-PST-1SG.S</ta>
            <ta e="T8" id="Seg_737" s="T7">я.NOM</ta>
            <ta e="T9" id="Seg_738" s="T8">ходить-1SG.S</ta>
            <ta e="T10" id="Seg_739" s="T9">магазин-EL.3SG</ta>
            <ta e="T11" id="Seg_740" s="T10">Ваня-GEN</ta>
            <ta e="T12" id="Seg_741" s="T11">дом-ILL</ta>
            <ta e="T13" id="Seg_742" s="T12">и</ta>
            <ta e="T14" id="Seg_743" s="T13">Мария-COM</ta>
            <ta e="T15" id="Seg_744" s="T14">увидеть-RFL-DRV-1DU</ta>
            <ta e="T16" id="Seg_745" s="T15">Марина-GEN</ta>
            <ta e="T17" id="Seg_746" s="T16">дом-EP-GEN</ta>
            <ta e="T18" id="Seg_747" s="T17">бок-LOC</ta>
            <ta e="T19" id="Seg_748" s="T18">Марина.[NOM]</ta>
            <ta e="T20" id="Seg_749" s="T19">сказать-3SG.S</ta>
            <ta e="T21" id="Seg_750" s="T20">зайти-IMP.2PL.S/O</ta>
            <ta e="T22" id="Seg_751" s="T21">я.ALL</ta>
            <ta e="T23" id="Seg_752" s="T22">мы.[NOM]</ta>
            <ta e="T24" id="Seg_753" s="T23">зайти-CO-1DU</ta>
            <ta e="T25" id="Seg_754" s="T24">он(а).[NOM]</ta>
            <ta e="T26" id="Seg_755" s="T25">маленький-девушка-EP-ACC</ta>
            <ta e="T27" id="Seg_756" s="T26">посылать-3SG.O</ta>
            <ta e="T28" id="Seg_757" s="T27">вино-TRL</ta>
            <ta e="T29" id="Seg_758" s="T28">он(а).[NOM]</ta>
            <ta e="T30" id="Seg_759" s="T29">принести-CO-3SG.O</ta>
            <ta e="T31" id="Seg_760" s="T30">вино-ACC</ta>
            <ta e="T32" id="Seg_761" s="T31">красный</ta>
            <ta e="T33" id="Seg_762" s="T32">вино-ACC</ta>
            <ta e="T34" id="Seg_763" s="T33">принести-CO-3SG.O</ta>
            <ta e="T35" id="Seg_764" s="T34">он(а).[NOM]</ta>
            <ta e="T36" id="Seg_765" s="T35">мы.ACC</ta>
            <ta e="T37" id="Seg_766" s="T36">пить-TR-3SG.S</ta>
            <ta e="T38" id="Seg_767" s="T37">сказать-3SG.S</ta>
            <ta e="T39" id="Seg_768" s="T38">этот</ta>
            <ta e="T40" id="Seg_769" s="T39">Вера.[NOM]</ta>
            <ta e="T41" id="Seg_770" s="T40">купить-3SG.O</ta>
            <ta e="T42" id="Seg_771" s="T41">вино.[NOM]-1SG</ta>
            <ta e="T43" id="Seg_772" s="T42">я.ACC</ta>
            <ta e="T44" id="Seg_773" s="T43">пить-TR-3SG.S</ta>
            <ta e="T45" id="Seg_774" s="T44">тетка.[NOM]</ta>
            <ta e="T46" id="Seg_775" s="T45">Марина-ALL</ta>
            <ta e="T47" id="Seg_776" s="T46">письмо.[NOM]</ta>
            <ta e="T48" id="Seg_777" s="T47">письмо-VBLZ-FUT-1SG.S</ta>
            <ta e="T49" id="Seg_778" s="T48">JUSS</ta>
            <ta e="T50" id="Seg_779" s="T49">читать-PFV-IMP.3SG/PL.O</ta>
            <ta e="T51" id="Seg_780" s="T50">я.NOM</ta>
            <ta e="T52" id="Seg_781" s="T51">весной</ta>
            <ta e="T53" id="Seg_782" s="T52">Старосондорово-ILL</ta>
            <ta e="T54" id="Seg_783" s="T53">приехать-FUT-1SG.S</ta>
            <ta e="T55" id="Seg_784" s="T54">рыба-EP-CAP-INF</ta>
            <ta e="T56" id="Seg_785" s="T55">ягода-EP-ACC</ta>
            <ta e="T57" id="Seg_786" s="T56">добыть-EP-INF</ta>
            <ta e="T58" id="Seg_787" s="T57">я.NOM</ta>
            <ta e="T59" id="Seg_788" s="T58">жить-1SG.S</ta>
            <ta e="T60" id="Seg_789" s="T59">женщина-ADJZ-человек-ADES</ta>
            <ta e="T61" id="Seg_790" s="T60">мы.PL.ALL</ta>
            <ta e="T62" id="Seg_791" s="T61">ну</ta>
            <ta e="T63" id="Seg_792" s="T62">приехать-HAB-INFER.[3SG.S]</ta>
            <ta e="T64" id="Seg_793" s="T63">селькупский</ta>
            <ta e="T65" id="Seg_794" s="T64">язык-PL-ACC</ta>
            <ta e="T66" id="Seg_795" s="T65">написать-CVB</ta>
            <ta e="T67" id="Seg_796" s="T66">принести-EP-FRQ-EP-PST-3SG.O</ta>
            <ta e="T68" id="Seg_797" s="T67">ты.ALL</ta>
            <ta e="T69" id="Seg_798" s="T68">ходить-HAB-PST.[3SG.S]</ta>
            <ta e="T70" id="Seg_799" s="T69">ты-COM</ta>
            <ta e="T71" id="Seg_800" s="T70">все.время</ta>
            <ta e="T72" id="Seg_801" s="T71">письмо-VBLZ-HAB-PST.[3SG.S]</ta>
            <ta e="T73" id="Seg_802" s="T72">я.NOM</ta>
            <ta e="T74" id="Seg_803" s="T73">этот</ta>
            <ta e="T75" id="Seg_804" s="T74">женщина-ADJZ-человек-ADES</ta>
            <ta e="T76" id="Seg_805" s="T75">жить-1SG.S</ta>
            <ta e="T77" id="Seg_806" s="T76">Новосибирск-LOC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_807" s="T1">pers</ta>
            <ta e="T3" id="Seg_808" s="T2">adv</ta>
            <ta e="T4" id="Seg_809" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_810" s="T4">n-n:ins-n&gt;adv</ta>
            <ta e="T6" id="Seg_811" s="T5">nprop-n:case</ta>
            <ta e="T7" id="Seg_812" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_813" s="T7">pers</ta>
            <ta e="T9" id="Seg_814" s="T8">v-v:pn</ta>
            <ta e="T10" id="Seg_815" s="T9">n-n:case.poss</ta>
            <ta e="T11" id="Seg_816" s="T10">nprop-n:case</ta>
            <ta e="T12" id="Seg_817" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_818" s="T12">conj</ta>
            <ta e="T14" id="Seg_819" s="T13">nprop-n:case</ta>
            <ta e="T15" id="Seg_820" s="T14">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T16" id="Seg_821" s="T15">nprop-n:case</ta>
            <ta e="T17" id="Seg_822" s="T16">n-n:ins-n:case</ta>
            <ta e="T18" id="Seg_823" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_824" s="T18">nprop.[n:case]</ta>
            <ta e="T20" id="Seg_825" s="T19">v-v:pn</ta>
            <ta e="T21" id="Seg_826" s="T20">v-v:mood.pn</ta>
            <ta e="T22" id="Seg_827" s="T21">pers</ta>
            <ta e="T23" id="Seg_828" s="T22">pers.[n:case]</ta>
            <ta e="T24" id="Seg_829" s="T23">v-v:ins-v:pn</ta>
            <ta e="T25" id="Seg_830" s="T24">pers.[n:case]</ta>
            <ta e="T26" id="Seg_831" s="T25">adj-n-n:ins-n:case</ta>
            <ta e="T27" id="Seg_832" s="T26">v-v:pn</ta>
            <ta e="T28" id="Seg_833" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_834" s="T28">pers.[n:case]</ta>
            <ta e="T30" id="Seg_835" s="T29">v-v:ins-v:pn</ta>
            <ta e="T31" id="Seg_836" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_837" s="T31">adj</ta>
            <ta e="T33" id="Seg_838" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_839" s="T33">v-v:ins-v:pn</ta>
            <ta e="T35" id="Seg_840" s="T34">pers.[n:case]</ta>
            <ta e="T36" id="Seg_841" s="T35">pers</ta>
            <ta e="T37" id="Seg_842" s="T36">v-v&gt;v-v:pn</ta>
            <ta e="T38" id="Seg_843" s="T37">v-v:pn</ta>
            <ta e="T39" id="Seg_844" s="T38">dem</ta>
            <ta e="T40" id="Seg_845" s="T39">nprop.[n:case]</ta>
            <ta e="T41" id="Seg_846" s="T40">v-v:pn</ta>
            <ta e="T42" id="Seg_847" s="T41">n.[n:case]-n:poss</ta>
            <ta e="T43" id="Seg_848" s="T42">pers</ta>
            <ta e="T44" id="Seg_849" s="T43">v-v&gt;v-v:pn</ta>
            <ta e="T45" id="Seg_850" s="T44">n.[n:case]</ta>
            <ta e="T46" id="Seg_851" s="T45">nprop-n:case</ta>
            <ta e="T47" id="Seg_852" s="T46">n.[n:case]</ta>
            <ta e="T48" id="Seg_853" s="T47">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_854" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_855" s="T49">v-v&gt;v-v:mood.pn</ta>
            <ta e="T51" id="Seg_856" s="T50">pers</ta>
            <ta e="T52" id="Seg_857" s="T51">adv</ta>
            <ta e="T53" id="Seg_858" s="T52">nprop-n:case</ta>
            <ta e="T54" id="Seg_859" s="T53">v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_860" s="T54">n-n:ins-n&gt;v-v:inf</ta>
            <ta e="T56" id="Seg_861" s="T55">n-n:ins-n:case</ta>
            <ta e="T57" id="Seg_862" s="T56">v-n:ins-v:inf</ta>
            <ta e="T58" id="Seg_863" s="T57">pers</ta>
            <ta e="T59" id="Seg_864" s="T58">v-v:pn</ta>
            <ta e="T60" id="Seg_865" s="T59">n-n&gt;adj-n-n:case</ta>
            <ta e="T61" id="Seg_866" s="T60">pers</ta>
            <ta e="T62" id="Seg_867" s="T61">ptcl</ta>
            <ta e="T63" id="Seg_868" s="T62">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T64" id="Seg_869" s="T63">adj</ta>
            <ta e="T65" id="Seg_870" s="T64">n-n:num-n:case</ta>
            <ta e="T66" id="Seg_871" s="T65">v-v&gt;adv</ta>
            <ta e="T67" id="Seg_872" s="T66">v-v:ins-v&gt;v-v:ins-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_873" s="T67">pers</ta>
            <ta e="T69" id="Seg_874" s="T68">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T70" id="Seg_875" s="T69">pers-n:case</ta>
            <ta e="T71" id="Seg_876" s="T70">adv</ta>
            <ta e="T72" id="Seg_877" s="T71">n-n&gt;v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T73" id="Seg_878" s="T72">pers</ta>
            <ta e="T74" id="Seg_879" s="T73">dem</ta>
            <ta e="T75" id="Seg_880" s="T74">n-n&gt;adj-n-n:case</ta>
            <ta e="T76" id="Seg_881" s="T75">v-v:pn</ta>
            <ta e="T77" id="Seg_882" s="T76">nprop-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_883" s="T1">pers</ta>
            <ta e="T3" id="Seg_884" s="T2">adv</ta>
            <ta e="T4" id="Seg_885" s="T3">v</ta>
            <ta e="T5" id="Seg_886" s="T4">adv</ta>
            <ta e="T6" id="Seg_887" s="T5">nprop</ta>
            <ta e="T7" id="Seg_888" s="T6">v</ta>
            <ta e="T8" id="Seg_889" s="T7">pers</ta>
            <ta e="T9" id="Seg_890" s="T8">v</ta>
            <ta e="T10" id="Seg_891" s="T9">n</ta>
            <ta e="T11" id="Seg_892" s="T10">nprop</ta>
            <ta e="T12" id="Seg_893" s="T11">n</ta>
            <ta e="T13" id="Seg_894" s="T12">conj</ta>
            <ta e="T14" id="Seg_895" s="T13">nprop</ta>
            <ta e="T15" id="Seg_896" s="T14">v</ta>
            <ta e="T16" id="Seg_897" s="T15">nprop</ta>
            <ta e="T17" id="Seg_898" s="T16">n</ta>
            <ta e="T18" id="Seg_899" s="T17">n</ta>
            <ta e="T19" id="Seg_900" s="T18">nprop</ta>
            <ta e="T20" id="Seg_901" s="T19">v</ta>
            <ta e="T21" id="Seg_902" s="T20">v</ta>
            <ta e="T22" id="Seg_903" s="T21">pers</ta>
            <ta e="T23" id="Seg_904" s="T22">pers</ta>
            <ta e="T24" id="Seg_905" s="T23">v</ta>
            <ta e="T25" id="Seg_906" s="T24">pers</ta>
            <ta e="T26" id="Seg_907" s="T25">n</ta>
            <ta e="T27" id="Seg_908" s="T26">v</ta>
            <ta e="T28" id="Seg_909" s="T27">n</ta>
            <ta e="T29" id="Seg_910" s="T28">pers</ta>
            <ta e="T30" id="Seg_911" s="T29">v</ta>
            <ta e="T31" id="Seg_912" s="T30">n</ta>
            <ta e="T32" id="Seg_913" s="T31">adj</ta>
            <ta e="T33" id="Seg_914" s="T32">n</ta>
            <ta e="T34" id="Seg_915" s="T33">v</ta>
            <ta e="T35" id="Seg_916" s="T34">pers</ta>
            <ta e="T36" id="Seg_917" s="T35">pers</ta>
            <ta e="T37" id="Seg_918" s="T36">v</ta>
            <ta e="T38" id="Seg_919" s="T37">v</ta>
            <ta e="T39" id="Seg_920" s="T38">dem</ta>
            <ta e="T40" id="Seg_921" s="T39">nprop</ta>
            <ta e="T41" id="Seg_922" s="T40">v</ta>
            <ta e="T42" id="Seg_923" s="T41">n</ta>
            <ta e="T43" id="Seg_924" s="T42">pers</ta>
            <ta e="T44" id="Seg_925" s="T43">v</ta>
            <ta e="T45" id="Seg_926" s="T44">n</ta>
            <ta e="T46" id="Seg_927" s="T45">nprop</ta>
            <ta e="T47" id="Seg_928" s="T46">n</ta>
            <ta e="T48" id="Seg_929" s="T47">v</ta>
            <ta e="T49" id="Seg_930" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_931" s="T49">v</ta>
            <ta e="T51" id="Seg_932" s="T50">pers</ta>
            <ta e="T52" id="Seg_933" s="T51">adv</ta>
            <ta e="T53" id="Seg_934" s="T52">nprop</ta>
            <ta e="T54" id="Seg_935" s="T53">v</ta>
            <ta e="T55" id="Seg_936" s="T54">v</ta>
            <ta e="T56" id="Seg_937" s="T55">n</ta>
            <ta e="T57" id="Seg_938" s="T56">v</ta>
            <ta e="T58" id="Seg_939" s="T57">pers</ta>
            <ta e="T59" id="Seg_940" s="T58">v</ta>
            <ta e="T60" id="Seg_941" s="T59">n</ta>
            <ta e="T61" id="Seg_942" s="T60">pers</ta>
            <ta e="T62" id="Seg_943" s="T61">ptcl</ta>
            <ta e="T63" id="Seg_944" s="T62">v</ta>
            <ta e="T64" id="Seg_945" s="T63">adj</ta>
            <ta e="T65" id="Seg_946" s="T64">n</ta>
            <ta e="T66" id="Seg_947" s="T65">adv</ta>
            <ta e="T67" id="Seg_948" s="T66">v</ta>
            <ta e="T68" id="Seg_949" s="T67">pers</ta>
            <ta e="T69" id="Seg_950" s="T68">v</ta>
            <ta e="T70" id="Seg_951" s="T69">pers</ta>
            <ta e="T71" id="Seg_952" s="T70">adv</ta>
            <ta e="T72" id="Seg_953" s="T71">v</ta>
            <ta e="T73" id="Seg_954" s="T72">pers</ta>
            <ta e="T74" id="Seg_955" s="T73">dem</ta>
            <ta e="T75" id="Seg_956" s="T74">n</ta>
            <ta e="T76" id="Seg_957" s="T75">v</ta>
            <ta e="T77" id="Seg_958" s="T76">nprop</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_959" s="T1">pro.h:A</ta>
            <ta e="T5" id="Seg_960" s="T4">adv:Time</ta>
            <ta e="T6" id="Seg_961" s="T5">np:G</ta>
            <ta e="T7" id="Seg_962" s="T6">0.1.h:A</ta>
            <ta e="T8" id="Seg_963" s="T7">pro.h:A</ta>
            <ta e="T10" id="Seg_964" s="T9">np:So</ta>
            <ta e="T11" id="Seg_965" s="T10">np.h:Poss</ta>
            <ta e="T12" id="Seg_966" s="T11">np:G</ta>
            <ta e="T14" id="Seg_967" s="T13">np:Com</ta>
            <ta e="T15" id="Seg_968" s="T14">0.1.h:E</ta>
            <ta e="T16" id="Seg_969" s="T15">np.h:Poss</ta>
            <ta e="T17" id="Seg_970" s="T16">np:Poss</ta>
            <ta e="T18" id="Seg_971" s="T17">np:L</ta>
            <ta e="T19" id="Seg_972" s="T18">np.h:A</ta>
            <ta e="T21" id="Seg_973" s="T20">0.2.h:A</ta>
            <ta e="T22" id="Seg_974" s="T21">pro.h:G</ta>
            <ta e="T23" id="Seg_975" s="T22">pro.h:A</ta>
            <ta e="T25" id="Seg_976" s="T24">pro.h:A</ta>
            <ta e="T26" id="Seg_977" s="T25">np.h:Th</ta>
            <ta e="T29" id="Seg_978" s="T28">pro.h:A</ta>
            <ta e="T31" id="Seg_979" s="T30">np:Th</ta>
            <ta e="T33" id="Seg_980" s="T32">np:Th</ta>
            <ta e="T34" id="Seg_981" s="T33">0.3.h:A</ta>
            <ta e="T35" id="Seg_982" s="T34">pro.h:A</ta>
            <ta e="T36" id="Seg_983" s="T35">pro.h:B</ta>
            <ta e="T38" id="Seg_984" s="T37">0.3.h:A</ta>
            <ta e="T40" id="Seg_985" s="T39">np.h:A</ta>
            <ta e="T41" id="Seg_986" s="T40">0.3:Th</ta>
            <ta e="T42" id="Seg_987" s="T41">np:Cau 0.1.h:Poss</ta>
            <ta e="T43" id="Seg_988" s="T42">pro.h:P</ta>
            <ta e="T46" id="Seg_989" s="T45">np.h:R</ta>
            <ta e="T47" id="Seg_990" s="T46">np:Th</ta>
            <ta e="T48" id="Seg_991" s="T47">0.1.h:A</ta>
            <ta e="T50" id="Seg_992" s="T49">0.3.h:A</ta>
            <ta e="T51" id="Seg_993" s="T50">pro.h:A</ta>
            <ta e="T52" id="Seg_994" s="T51">adv:Time</ta>
            <ta e="T53" id="Seg_995" s="T52">np:G</ta>
            <ta e="T56" id="Seg_996" s="T55">np:Th</ta>
            <ta e="T58" id="Seg_997" s="T57">pro.h:Th</ta>
            <ta e="T60" id="Seg_998" s="T59">np.h:L</ta>
            <ta e="T61" id="Seg_999" s="T60">pro.h:G</ta>
            <ta e="T63" id="Seg_1000" s="T62">0.3.h:A</ta>
            <ta e="T65" id="Seg_1001" s="T64">np:Th</ta>
            <ta e="T67" id="Seg_1002" s="T66">0.3.h:A</ta>
            <ta e="T68" id="Seg_1003" s="T67">pro.h:G</ta>
            <ta e="T69" id="Seg_1004" s="T68">0.3.h:A</ta>
            <ta e="T70" id="Seg_1005" s="T69">pro:Com</ta>
            <ta e="T71" id="Seg_1006" s="T70">adv:Time</ta>
            <ta e="T72" id="Seg_1007" s="T71">0.3.h:A</ta>
            <ta e="T73" id="Seg_1008" s="T72">pro.h:Th</ta>
            <ta e="T75" id="Seg_1009" s="T74">np.h:L</ta>
            <ta e="T77" id="Seg_1010" s="T76">np:L</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1011" s="T1">pro.h:S</ta>
            <ta e="T4" id="Seg_1012" s="T3">v:pred</ta>
            <ta e="T7" id="Seg_1013" s="T6">0.1.h:S v:pred</ta>
            <ta e="T8" id="Seg_1014" s="T7">pro.h:S</ta>
            <ta e="T9" id="Seg_1015" s="T8">v:pred</ta>
            <ta e="T15" id="Seg_1016" s="T14">0.1.h:S v:pred</ta>
            <ta e="T19" id="Seg_1017" s="T18">np.h:S</ta>
            <ta e="T20" id="Seg_1018" s="T19">v:pred</ta>
            <ta e="T21" id="Seg_1019" s="T20">0.2.h:S v:pred</ta>
            <ta e="T23" id="Seg_1020" s="T22">pro.h:S</ta>
            <ta e="T24" id="Seg_1021" s="T23">v:pred</ta>
            <ta e="T25" id="Seg_1022" s="T24">pro.h:S</ta>
            <ta e="T26" id="Seg_1023" s="T25">np.h:O</ta>
            <ta e="T27" id="Seg_1024" s="T26">v:pred</ta>
            <ta e="T29" id="Seg_1025" s="T28">pro.h:S</ta>
            <ta e="T30" id="Seg_1026" s="T29">v:pred</ta>
            <ta e="T31" id="Seg_1027" s="T30">np:O</ta>
            <ta e="T33" id="Seg_1028" s="T32">np:O</ta>
            <ta e="T34" id="Seg_1029" s="T33">0.3.h:S v:pred</ta>
            <ta e="T35" id="Seg_1030" s="T34">pro.h:S</ta>
            <ta e="T36" id="Seg_1031" s="T35">pro.h:O</ta>
            <ta e="T37" id="Seg_1032" s="T36">v:pred</ta>
            <ta e="T38" id="Seg_1033" s="T37">0.3.h:S v:pred</ta>
            <ta e="T40" id="Seg_1034" s="T39">np.h:S</ta>
            <ta e="T41" id="Seg_1035" s="T40">v:pred 0.3:O</ta>
            <ta e="T42" id="Seg_1036" s="T41">np:S</ta>
            <ta e="T43" id="Seg_1037" s="T42">pro.h:O</ta>
            <ta e="T44" id="Seg_1038" s="T43">v:pred</ta>
            <ta e="T47" id="Seg_1039" s="T46">np:O</ta>
            <ta e="T48" id="Seg_1040" s="T47">0.1.h:S v:pred</ta>
            <ta e="T50" id="Seg_1041" s="T49">0.3.h:S v:pred</ta>
            <ta e="T51" id="Seg_1042" s="T50">pro.h:S</ta>
            <ta e="T54" id="Seg_1043" s="T53">v:pred</ta>
            <ta e="T55" id="Seg_1044" s="T54">s:purp</ta>
            <ta e="T57" id="Seg_1045" s="T55">s:purp</ta>
            <ta e="T58" id="Seg_1046" s="T57">pro.h:S</ta>
            <ta e="T59" id="Seg_1047" s="T58">v:pred</ta>
            <ta e="T63" id="Seg_1048" s="T62">0.3.h:S v:pred</ta>
            <ta e="T66" id="Seg_1049" s="T63">s:temp</ta>
            <ta e="T67" id="Seg_1050" s="T66">0.3.h:S v:pred</ta>
            <ta e="T69" id="Seg_1051" s="T68">0.3.h:S v:pred</ta>
            <ta e="T72" id="Seg_1052" s="T71">0.3.h:S v:pred</ta>
            <ta e="T73" id="Seg_1053" s="T72">pro.h:S</ta>
            <ta e="T76" id="Seg_1054" s="T75">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_1055" s="T2">RUS:cult</ta>
            <ta e="T10" id="Seg_1056" s="T9">RUS:cult</ta>
            <ta e="T13" id="Seg_1057" s="T12">RUS:gram</ta>
            <ta e="T28" id="Seg_1058" s="T27">TURK:cult</ta>
            <ta e="T31" id="Seg_1059" s="T30">TURK:cult</ta>
            <ta e="T33" id="Seg_1060" s="T32">TURK:cult</ta>
            <ta e="T42" id="Seg_1061" s="T41">TURK:cult</ta>
            <ta e="T45" id="Seg_1062" s="T44">RUS:cult</ta>
            <ta e="T49" id="Seg_1063" s="T48">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_1064" s="T1">In summer I went to Novosondorovo to visit [friends].</ta>
            <ta e="T12" id="Seg_1065" s="T7">I was going to Vanya from the shop.</ta>
            <ta e="T18" id="Seg_1066" s="T12">And near Marina's house I met Maria.</ta>
            <ta e="T22" id="Seg_1067" s="T18">Marina says: “Come into my house.”</ta>
            <ta e="T24" id="Seg_1068" s="T22">We came in.</ta>
            <ta e="T28" id="Seg_1069" s="T24">She sent a girl to bring drinks.</ta>
            <ta e="T34" id="Seg_1070" s="T28">She brought drinks, she brought red wine.</ta>
            <ta e="T37" id="Seg_1071" s="T34">She wined us.</ta>
            <ta e="T41" id="Seg_1072" s="T37">She said: “Vera bought it.”</ta>
            <ta e="T44" id="Seg_1073" s="T41">The wine made me drunk.</ta>
            <ta e="T48" id="Seg_1074" s="T44">I'll write a letter to aunt Marina.</ta>
            <ta e="T50" id="Seg_1075" s="T48">Let her read.</ta>
            <ta e="T57" id="Seg_1076" s="T50">In spring I'll come to fish and to pick berries.</ta>
            <ta e="T63" id="Seg_1077" s="T57">I live with a woman, who had come to us.</ta>
            <ta e="T72" id="Seg_1078" s="T63">She was writing the Selkup language, she used to come to you and to write.</ta>
            <ta e="T77" id="Seg_1079" s="T72">I live with this woman in Novosibirsk.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_1080" s="T1">Im Sommer ging ich nach Novosondorovo um [Freunde] zu besuchen.</ta>
            <ta e="T12" id="Seg_1081" s="T7">Ich ging vom Laden aus zu Vanya.</ta>
            <ta e="T18" id="Seg_1082" s="T12">Und nah bei Marinas Haus traf ich Maria.</ta>
            <ta e="T22" id="Seg_1083" s="T18">Marina sagt: "Kommt herein."</ta>
            <ta e="T24" id="Seg_1084" s="T22">Wir gingen hinein.</ta>
            <ta e="T28" id="Seg_1085" s="T24">Sie schickte ein Mädchen um Getränke zu holen.</ta>
            <ta e="T34" id="Seg_1086" s="T28">Sie brachte Wein, sie brachte Rotwein.</ta>
            <ta e="T37" id="Seg_1087" s="T34">Sie ließ uns trinken.</ta>
            <ta e="T41" id="Seg_1088" s="T37">Sie sagte: "Vera hat ihn gekauft."</ta>
            <ta e="T44" id="Seg_1089" s="T41">Der Wein machte mich betrunken.</ta>
            <ta e="T48" id="Seg_1090" s="T44">Ich werde Tante Marina einen Brief schreiben.</ta>
            <ta e="T50" id="Seg_1091" s="T48">Lass sie lesen.</ta>
            <ta e="T57" id="Seg_1092" s="T50">Im Frühling komme ich um zu fischen und Beeren zu sammeln.</ta>
            <ta e="T63" id="Seg_1093" s="T57">Ich lebe bei einer Frau, die zu uns gekommen ist.</ta>
            <ta e="T72" id="Seg_1094" s="T63">Sie schrieb die selkupische Sprache auf, sie kam für gewöhnlich zu dir um zu schreiben.</ta>
            <ta e="T77" id="Seg_1095" s="T72">Ich wohne bei dieser Frau in Novosibirsk.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_1096" s="T1">Я в гости ездила летом, в Новосондорово ездила.</ta>
            <ta e="T12" id="Seg_1097" s="T7">Я шла из магазина к Ване домой.</ta>
            <ta e="T18" id="Seg_1098" s="T12">И с Марией встретились около Марининой избы.</ta>
            <ta e="T22" id="Seg_1099" s="T18">Марина говорит: “Зайдите ко мне”.</ta>
            <ta e="T24" id="Seg_1100" s="T22">Мы зашли.</ta>
            <ta e="T28" id="Seg_1101" s="T24">Она девочку послала за вином.</ta>
            <ta e="T34" id="Seg_1102" s="T28">Она принесла вино, красное вино принесла.</ta>
            <ta e="T37" id="Seg_1103" s="T34">Она нас напоила.</ta>
            <ta e="T41" id="Seg_1104" s="T37">Сказала: “Это Вера купила”.</ta>
            <ta e="T44" id="Seg_1105" s="T41">Вино меня напоило.</ta>
            <ta e="T48" id="Seg_1106" s="T44">Тетке Марине письмо напишу.</ta>
            <ta e="T50" id="Seg_1107" s="T48">Пускай прочитает.</ta>
            <ta e="T57" id="Seg_1108" s="T50">Я весной приеду рыбачить, ягоду брать.</ta>
            <ta e="T63" id="Seg_1109" s="T57">Я живу у женщины, которая к нам приезжала.</ta>
            <ta e="T72" id="Seg_1110" s="T63">Селькупский язык писала, с тобой ходила к тебе все время, писала.</ta>
            <ta e="T77" id="Seg_1111" s="T72">Я у этой женщины живу в Новосибирске.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_1112" s="T1">я в гости ездила летом в Новосондорово ездила</ta>
            <ta e="T12" id="Seg_1113" s="T7">я шла с магазина к Ване в дом</ta>
            <ta e="T18" id="Seg_1114" s="T12">с Марией встретились около Марининой избы</ta>
            <ta e="T22" id="Seg_1115" s="T18">Марина говорит зайдите ко мне</ta>
            <ta e="T24" id="Seg_1116" s="T22">мы зашли</ta>
            <ta e="T28" id="Seg_1117" s="T24">она девчонка послала за вином</ta>
            <ta e="T34" id="Seg_1118" s="T28">она принесла вино красное вино принесла</ta>
            <ta e="T37" id="Seg_1119" s="T34">она нас напоила</ta>
            <ta e="T41" id="Seg_1120" s="T37">сказала это Вера купила</ta>
            <ta e="T44" id="Seg_1121" s="T41">вино меня напоило</ta>
            <ta e="T48" id="Seg_1122" s="T44">тетке Марине письмо напишу</ta>
            <ta e="T50" id="Seg_1123" s="T48">пускай прочитает</ta>
            <ta e="T57" id="Seg_1124" s="T50">я весной приеду рыбачить ягоду брать</ta>
            <ta e="T63" id="Seg_1125" s="T57">я живу у женщины которая к нам приезжала</ta>
            <ta e="T72" id="Seg_1126" s="T63">по-остяцки (остяцкий язык) писала с тобой к тебе ходила все писала</ta>
            <ta e="T77" id="Seg_1127" s="T72">я у этой женщины живу в Новосибирске</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T18" id="Seg_1128" s="T12">[BrM:] Tentative analysis of 'qoɣɨdʼaj'.</ta>
            <ta e="T22" id="Seg_1129" s="T18">[BrM:] Tentative analysis of 'sernalʼe'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
