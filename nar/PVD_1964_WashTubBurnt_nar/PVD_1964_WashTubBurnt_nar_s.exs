<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_WashTubBurnt_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_WashTubBurnt_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">96</ud-information>
            <ud-information attribute-name="# HIAT:w">69</ud-information>
            <ud-information attribute-name="# e">69</ud-information>
            <ud-information attribute-name="# HIAT:u">14</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T70" id="Seg_0" n="sc" s="T1">
               <ts e="T9" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Tʼätʼau</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6" n="HIAT:ip">(</nts>
                  <ts e="T3" id="Seg_8" n="HIAT:w" s="T2">aːdʼau</ts>
                  <nts id="Seg_9" n="HIAT:ip">)</nts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_12" n="HIAT:w" s="T3">mekga</ts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_15" n="HIAT:w" s="T4">čaɣənɨm</ts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_18" n="HIAT:w" s="T5">mekkus</ts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_21" n="HIAT:w" s="T6">koːrə</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_24" n="HIAT:w" s="T7">sʼütʼdʼi</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_27" n="HIAT:w" s="T8">čaɣɨnä</ts>
                  <nts id="Seg_28" n="HIAT:ip">.</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_31" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_33" n="HIAT:w" s="T9">A</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_36" n="HIAT:w" s="T10">Nʼüra</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_39" n="HIAT:w" s="T11">na</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_42" n="HIAT:w" s="T12">čaɣnäze</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_45" n="HIAT:w" s="T13">qwäzʼe</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_48" n="HIAT:w" s="T14">pʼötʼom</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_51" n="HIAT:w" s="T15">qaːibbat</ts>
                  <nts id="Seg_52" n="HIAT:ip">.</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_55" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_57" n="HIAT:w" s="T16">Ponän</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_60" n="HIAT:w" s="T17">jes</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_63" n="HIAT:w" s="T18">qwäzi</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_66" n="HIAT:w" s="T19">pʼötʼtʼi</ts>
                  <nts id="Seg_67" n="HIAT:ip">.</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_70" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_72" n="HIAT:w" s="T20">Soros</ts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_76" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">Täp</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_80" n="HIAT:ip">(</nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">čaɣnä</ts>
                  <nts id="Seg_83" n="HIAT:ip">)</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">pʼöːdətʼippa</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">i</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">poːrɨlʼe</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_95" n="HIAT:w" s="T26">übərɨpba</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_99" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">A</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">Čʼigatkin</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">Nʼüra</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">tʼärɨn</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">mekga</ts>
                  <nts id="Seg_114" n="HIAT:ip">:</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_116" n="HIAT:ip">“</nts>
                  <ts e="T33" id="Seg_118" n="HIAT:w" s="T32">Tänan</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_121" n="HIAT:w" s="T33">čaɣɨnä</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_124" n="HIAT:w" s="T34">porɨmɨt</ts>
                  <nts id="Seg_125" n="HIAT:ip">.</nts>
                  <nts id="Seg_126" n="HIAT:ip">”</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_129" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_131" n="HIAT:w" s="T35">A</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_134" n="HIAT:w" s="T36">man</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_137" n="HIAT:w" s="T37">tʼäran</ts>
                  <nts id="Seg_138" n="HIAT:ip">:</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_140" n="HIAT:ip">“</nts>
                  <ts e="T39" id="Seg_142" n="HIAT:w" s="T38">Qaj</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_145" n="HIAT:w" s="T39">čaɣənä</ts>
                  <nts id="Seg_146" n="HIAT:ip">?</nts>
                  <nts id="Seg_147" n="HIAT:ip">”</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_150" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_152" n="HIAT:w" s="T40">A</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_155" n="HIAT:w" s="T41">Nʼüra</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_158" n="HIAT:w" s="T42">tʼärɨn</ts>
                  <nts id="Seg_159" n="HIAT:ip">:</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_161" n="HIAT:ip">“</nts>
                  <ts e="T44" id="Seg_163" n="HIAT:w" s="T43">Man</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_166" n="HIAT:w" s="T44">qaːjzau</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_169" n="HIAT:w" s="T45">qwäzi</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_172" n="HIAT:w" s="T46">pʼötʼim</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_175" n="HIAT:w" s="T47">tan</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_178" n="HIAT:w" s="T48">čagnätdɨze</ts>
                  <nts id="Seg_179" n="HIAT:ip">.</nts>
                  <nts id="Seg_180" n="HIAT:ip">”</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_183" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_185" n="HIAT:w" s="T49">Man</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_188" n="HIAT:w" s="T50">kuronnan</ts>
                  <nts id="Seg_189" n="HIAT:ip">.</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_192" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_194" n="HIAT:w" s="T51">A</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_197" n="HIAT:w" s="T52">čaɣənä</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_200" n="HIAT:w" s="T53">porɨpba</ts>
                  <nts id="Seg_201" n="HIAT:ip">.</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_204" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_206" n="HIAT:w" s="T54">A</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_209" n="HIAT:w" s="T55">man</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_212" n="HIAT:w" s="T56">tʼän</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_215" n="HIAT:w" s="T57">ütse</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_218" n="HIAT:w" s="T58">qamǯənnau</ts>
                  <nts id="Seg_219" n="HIAT:ip">.</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_222" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_224" n="HIAT:w" s="T59">A</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_227" n="HIAT:w" s="T60">täp</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_230" n="HIAT:w" s="T61">kɨcʼiwatpa</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_233" n="HIAT:w" s="T62">i</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_236" n="HIAT:w" s="T63">nɨkga</ts>
                  <nts id="Seg_237" n="HIAT:ip">.</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_240" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_242" n="HIAT:w" s="T64">Sʼütʼdʼit</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_245" n="HIAT:w" s="T65">šeːɣan</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_248" n="HIAT:w" s="T66">jes</ts>
                  <nts id="Seg_249" n="HIAT:ip">.</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_252" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_254" n="HIAT:w" s="T67">Mekga</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_257" n="HIAT:w" s="T68">ʒalkan</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_260" n="HIAT:w" s="T69">jes</ts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T70" id="Seg_263" n="sc" s="T1">
               <ts e="T2" id="Seg_265" n="e" s="T1">Tʼätʼau </ts>
               <ts e="T3" id="Seg_267" n="e" s="T2">(aːdʼau) </ts>
               <ts e="T4" id="Seg_269" n="e" s="T3">mekga </ts>
               <ts e="T5" id="Seg_271" n="e" s="T4">čaɣənɨm </ts>
               <ts e="T6" id="Seg_273" n="e" s="T5">mekkus </ts>
               <ts e="T7" id="Seg_275" n="e" s="T6">koːrə </ts>
               <ts e="T8" id="Seg_277" n="e" s="T7">sʼütʼdʼi </ts>
               <ts e="T9" id="Seg_279" n="e" s="T8">čaɣɨnä. </ts>
               <ts e="T10" id="Seg_281" n="e" s="T9">A </ts>
               <ts e="T11" id="Seg_283" n="e" s="T10">Nʼüra </ts>
               <ts e="T12" id="Seg_285" n="e" s="T11">na </ts>
               <ts e="T13" id="Seg_287" n="e" s="T12">čaɣnäze </ts>
               <ts e="T14" id="Seg_289" n="e" s="T13">qwäzʼe </ts>
               <ts e="T15" id="Seg_291" n="e" s="T14">pʼötʼom </ts>
               <ts e="T16" id="Seg_293" n="e" s="T15">qaːibbat. </ts>
               <ts e="T17" id="Seg_295" n="e" s="T16">Ponän </ts>
               <ts e="T18" id="Seg_297" n="e" s="T17">jes </ts>
               <ts e="T19" id="Seg_299" n="e" s="T18">qwäzi </ts>
               <ts e="T20" id="Seg_301" n="e" s="T19">pʼötʼtʼi. </ts>
               <ts e="T21" id="Seg_303" n="e" s="T20">Soros. </ts>
               <ts e="T22" id="Seg_305" n="e" s="T21">Täp </ts>
               <ts e="T23" id="Seg_307" n="e" s="T22">(čaɣnä) </ts>
               <ts e="T24" id="Seg_309" n="e" s="T23">pʼöːdətʼippa </ts>
               <ts e="T25" id="Seg_311" n="e" s="T24">i </ts>
               <ts e="T26" id="Seg_313" n="e" s="T25">poːrɨlʼe </ts>
               <ts e="T27" id="Seg_315" n="e" s="T26">übərɨpba. </ts>
               <ts e="T28" id="Seg_317" n="e" s="T27">A </ts>
               <ts e="T29" id="Seg_319" n="e" s="T28">Čʼigatkin </ts>
               <ts e="T30" id="Seg_321" n="e" s="T29">Nʼüra </ts>
               <ts e="T31" id="Seg_323" n="e" s="T30">tʼärɨn </ts>
               <ts e="T32" id="Seg_325" n="e" s="T31">mekga: </ts>
               <ts e="T33" id="Seg_327" n="e" s="T32">“Tänan </ts>
               <ts e="T34" id="Seg_329" n="e" s="T33">čaɣɨnä </ts>
               <ts e="T35" id="Seg_331" n="e" s="T34">porɨmɨt.” </ts>
               <ts e="T36" id="Seg_333" n="e" s="T35">A </ts>
               <ts e="T37" id="Seg_335" n="e" s="T36">man </ts>
               <ts e="T38" id="Seg_337" n="e" s="T37">tʼäran: </ts>
               <ts e="T39" id="Seg_339" n="e" s="T38">“Qaj </ts>
               <ts e="T40" id="Seg_341" n="e" s="T39">čaɣənä?” </ts>
               <ts e="T41" id="Seg_343" n="e" s="T40">A </ts>
               <ts e="T42" id="Seg_345" n="e" s="T41">Nʼüra </ts>
               <ts e="T43" id="Seg_347" n="e" s="T42">tʼärɨn: </ts>
               <ts e="T44" id="Seg_349" n="e" s="T43">“Man </ts>
               <ts e="T45" id="Seg_351" n="e" s="T44">qaːjzau </ts>
               <ts e="T46" id="Seg_353" n="e" s="T45">qwäzi </ts>
               <ts e="T47" id="Seg_355" n="e" s="T46">pʼötʼim </ts>
               <ts e="T48" id="Seg_357" n="e" s="T47">tan </ts>
               <ts e="T49" id="Seg_359" n="e" s="T48">čagnätdɨze.” </ts>
               <ts e="T50" id="Seg_361" n="e" s="T49">Man </ts>
               <ts e="T51" id="Seg_363" n="e" s="T50">kuronnan. </ts>
               <ts e="T52" id="Seg_365" n="e" s="T51">A </ts>
               <ts e="T53" id="Seg_367" n="e" s="T52">čaɣənä </ts>
               <ts e="T54" id="Seg_369" n="e" s="T53">porɨpba. </ts>
               <ts e="T55" id="Seg_371" n="e" s="T54">A </ts>
               <ts e="T56" id="Seg_373" n="e" s="T55">man </ts>
               <ts e="T57" id="Seg_375" n="e" s="T56">tʼän </ts>
               <ts e="T58" id="Seg_377" n="e" s="T57">ütse </ts>
               <ts e="T59" id="Seg_379" n="e" s="T58">qamǯənnau. </ts>
               <ts e="T60" id="Seg_381" n="e" s="T59">A </ts>
               <ts e="T61" id="Seg_383" n="e" s="T60">täp </ts>
               <ts e="T62" id="Seg_385" n="e" s="T61">kɨcʼiwatpa </ts>
               <ts e="T63" id="Seg_387" n="e" s="T62">i </ts>
               <ts e="T64" id="Seg_389" n="e" s="T63">nɨkga. </ts>
               <ts e="T65" id="Seg_391" n="e" s="T64">Sʼütʼdʼit </ts>
               <ts e="T66" id="Seg_393" n="e" s="T65">šeːɣan </ts>
               <ts e="T67" id="Seg_395" n="e" s="T66">jes. </ts>
               <ts e="T68" id="Seg_397" n="e" s="T67">Mekga </ts>
               <ts e="T69" id="Seg_399" n="e" s="T68">ʒalkan </ts>
               <ts e="T70" id="Seg_401" n="e" s="T69">jes. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T9" id="Seg_402" s="T1">PVD_1964_WashTubBurnt_nar.001 (001.001)</ta>
            <ta e="T16" id="Seg_403" s="T9">PVD_1964_WashTubBurnt_nar.002 (001.002)</ta>
            <ta e="T20" id="Seg_404" s="T16">PVD_1964_WashTubBurnt_nar.003 (001.003)</ta>
            <ta e="T21" id="Seg_405" s="T20">PVD_1964_WashTubBurnt_nar.004 (001.004)</ta>
            <ta e="T27" id="Seg_406" s="T21">PVD_1964_WashTubBurnt_nar.005 (001.005)</ta>
            <ta e="T35" id="Seg_407" s="T27">PVD_1964_WashTubBurnt_nar.006 (001.006)</ta>
            <ta e="T40" id="Seg_408" s="T35">PVD_1964_WashTubBurnt_nar.007 (001.007)</ta>
            <ta e="T49" id="Seg_409" s="T40">PVD_1964_WashTubBurnt_nar.008 (001.008)</ta>
            <ta e="T51" id="Seg_410" s="T49">PVD_1964_WashTubBurnt_nar.009 (001.009)</ta>
            <ta e="T54" id="Seg_411" s="T51">PVD_1964_WashTubBurnt_nar.010 (001.010)</ta>
            <ta e="T59" id="Seg_412" s="T54">PVD_1964_WashTubBurnt_nar.011 (001.011)</ta>
            <ta e="T64" id="Seg_413" s="T59">PVD_1964_WashTubBurnt_nar.012 (001.012)</ta>
            <ta e="T67" id="Seg_414" s="T64">PVD_1964_WashTubBurnt_nar.013 (001.013)</ta>
            <ta e="T70" id="Seg_415" s="T67">PVD_1964_WashTubBurnt_nar.014 (001.014)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T9" id="Seg_416" s="T1">′тʼӓтʼау (′а̄дʼау) мекга тшаɣъным ′меккус ′ко̄ръ ′сʼӱтʼдʼи тшаɣы̹′нӓ.</ta>
            <ta e="T16" id="Seg_417" s="T9">а Нʼӱра на тшаɣ′нӓзе ′kwӓзʼе ′пʼӧтʼом ′kа̄иб̂бат.</ta>
            <ta e="T20" id="Seg_418" s="T16">′понӓн jес ′kwӓзи пʼӧтʼтʼи.</ta>
            <ta e="T21" id="Seg_419" s="T20">со′рос.</ta>
            <ta e="T27" id="Seg_420" s="T21">тӓп (тшаɣ′нӓ) ′пʼӧ̄дътʼиппа и ′по̄рылʼе ′ӱбърыпба.</ta>
            <ta e="T35" id="Seg_421" s="T27">а Чи′гаткин ′Нʼӱра тʼӓ′рын ′мекга: тӓ′нан тшаɣы′нӓ ′порымыт.</ta>
            <ta e="T40" id="Seg_422" s="T35">а ман тʼӓ′ран: ′kай тшаɣъ′нӓ.</ta>
            <ta e="T49" id="Seg_423" s="T40">а ′Нʼӱра тʼӓ′рын: ман ′kа̄йзау̹ kwӓзи ′пʼӧтʼим тан тшаг′нӓтдызе.</ta>
            <ta e="T51" id="Seg_424" s="T49">ман ку′роннан.</ta>
            <ta e="T54" id="Seg_425" s="T51">а тшаɣъ′нӓ ′порыпба.</ta>
            <ta e="T59" id="Seg_426" s="T54">а ман тʼӓн ӱт′се kамджъ′ннау̹.</ta>
            <ta e="T64" id="Seg_427" s="T59">а тӓп кыцʼи′ватпа и нык′га.</ta>
            <ta e="T67" id="Seg_428" s="T64">′сʼӱтʼдʼит ′ше̄ɣан jес.</ta>
            <ta e="T70" id="Seg_429" s="T67">мекга ′ж̂алкан jес.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T9" id="Seg_430" s="T1">tʼätʼau (aːdʼau) mekga tšaɣənɨm mekkus koːrə sʼütʼdʼi tšaɣɨ̹nä.</ta>
            <ta e="T16" id="Seg_431" s="T9">a Нʼüra na tšaɣnäze qwäzʼe pʼötʼom qaːib̂bat.</ta>
            <ta e="T20" id="Seg_432" s="T16">ponän jes qwäzi pʼötʼtʼi.</ta>
            <ta e="T21" id="Seg_433" s="T20">soros.</ta>
            <ta e="T27" id="Seg_434" s="T21">täp (tšaɣnä) pʼöːdətʼippa i poːrɨlʼe übərɨpba.</ta>
            <ta e="T35" id="Seg_435" s="T27">a Чigatkin Нʼüra tʼärɨn mekga: tänan tšaɣɨnä porɨmɨt.</ta>
            <ta e="T40" id="Seg_436" s="T35">a man tʼäran: qaj tšaɣənä.</ta>
            <ta e="T49" id="Seg_437" s="T40">a Нʼüra tʼärɨn: man qaːjzau̹ qwäzi pʼötʼim tan tšagnätdɨze.</ta>
            <ta e="T51" id="Seg_438" s="T49">man kuronnan.</ta>
            <ta e="T54" id="Seg_439" s="T51">a tšaɣənä porɨpba.</ta>
            <ta e="T59" id="Seg_440" s="T54">a man tʼän ütse qamǯənnau̹.</ta>
            <ta e="T64" id="Seg_441" s="T59">a täp kɨcʼiwatpa i nɨkga.</ta>
            <ta e="T67" id="Seg_442" s="T64">sʼütʼdʼit šeːɣan jes.</ta>
            <ta e="T70" id="Seg_443" s="T67">mekga ʒ̂alkan jes.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T9" id="Seg_444" s="T1">Tʼätʼau (aːdʼau) mekga čaɣənɨm mekkus koːrə sʼütʼdʼi čaɣɨnä. </ta>
            <ta e="T16" id="Seg_445" s="T9">A Nʼüra na čaɣnäze qwäzʼe pʼötʼom qaːibbat. </ta>
            <ta e="T20" id="Seg_446" s="T16">Ponän jes qwäzi pʼötʼtʼi. </ta>
            <ta e="T21" id="Seg_447" s="T20">Soros. </ta>
            <ta e="T27" id="Seg_448" s="T21">Täp (čaɣnä) pʼöːdətʼippa i poːrɨlʼe übərɨpba. </ta>
            <ta e="T35" id="Seg_449" s="T27">A Čʼigatkin Nʼüra tʼärɨn mekga: “Tänan čaɣɨnä porɨmɨt.” </ta>
            <ta e="T40" id="Seg_450" s="T35">A man tʼäran: “Qaj čaɣənä?” </ta>
            <ta e="T49" id="Seg_451" s="T40">A Nʼüra tʼärɨn: “Man qaːjzau qwäzi pʼötʼim tan čagnätdɨze.” </ta>
            <ta e="T51" id="Seg_452" s="T49">Man kuronnan. </ta>
            <ta e="T54" id="Seg_453" s="T51">A čaɣənä porɨpba. </ta>
            <ta e="T59" id="Seg_454" s="T54">A man tʼän ütse qamǯənnau. </ta>
            <ta e="T64" id="Seg_455" s="T59">A täp kɨcʼiwatpa i nɨkga. </ta>
            <ta e="T67" id="Seg_456" s="T64">Sʼütʼdʼit šeːɣan jes. </ta>
            <ta e="T70" id="Seg_457" s="T67">Mekga ʒalkan jes. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_458" s="T1">tʼätʼa-u</ta>
            <ta e="T3" id="Seg_459" s="T2">aːdʼa-u</ta>
            <ta e="T4" id="Seg_460" s="T3">mekga</ta>
            <ta e="T5" id="Seg_461" s="T4">čaɣənɨ-m</ta>
            <ta e="T6" id="Seg_462" s="T5">me-kku-s</ta>
            <ta e="T7" id="Seg_463" s="T6">koːrə</ta>
            <ta e="T8" id="Seg_464" s="T7">sʼütʼdʼi</ta>
            <ta e="T9" id="Seg_465" s="T8">čaɣɨnä</ta>
            <ta e="T10" id="Seg_466" s="T9">a</ta>
            <ta e="T11" id="Seg_467" s="T10">Nʼüra</ta>
            <ta e="T12" id="Seg_468" s="T11">na</ta>
            <ta e="T13" id="Seg_469" s="T12">čaɣnä-ze</ta>
            <ta e="T14" id="Seg_470" s="T13">qwäzʼe</ta>
            <ta e="T15" id="Seg_471" s="T14">pʼötʼo-m</ta>
            <ta e="T16" id="Seg_472" s="T15">qaːi-bba-t</ta>
            <ta e="T17" id="Seg_473" s="T16">ponä-n</ta>
            <ta e="T18" id="Seg_474" s="T17">je-s</ta>
            <ta e="T19" id="Seg_475" s="T18">qwäzi</ta>
            <ta e="T20" id="Seg_476" s="T19">pʼötʼtʼi</ta>
            <ta e="T21" id="Seg_477" s="T20">soro-s</ta>
            <ta e="T22" id="Seg_478" s="T21">täp</ta>
            <ta e="T23" id="Seg_479" s="T22">čaɣnä</ta>
            <ta e="T24" id="Seg_480" s="T23">pʼöːdətʼi-ppa</ta>
            <ta e="T25" id="Seg_481" s="T24">i</ta>
            <ta e="T26" id="Seg_482" s="T25">poːr-ɨ-lʼe</ta>
            <ta e="T27" id="Seg_483" s="T26">übə-r-ɨ-pba</ta>
            <ta e="T28" id="Seg_484" s="T27">a</ta>
            <ta e="T29" id="Seg_485" s="T28">Čʼigatkin</ta>
            <ta e="T30" id="Seg_486" s="T29">Nʼüra</ta>
            <ta e="T31" id="Seg_487" s="T30">tʼärɨ-n</ta>
            <ta e="T32" id="Seg_488" s="T31">mekga</ta>
            <ta e="T33" id="Seg_489" s="T32">tä-nan</ta>
            <ta e="T34" id="Seg_490" s="T33">čaɣɨnä</ta>
            <ta e="T35" id="Seg_491" s="T34">por-ɨ-mɨ-t</ta>
            <ta e="T36" id="Seg_492" s="T35">a</ta>
            <ta e="T37" id="Seg_493" s="T36">man</ta>
            <ta e="T38" id="Seg_494" s="T37">tʼära-n</ta>
            <ta e="T39" id="Seg_495" s="T38">Qaj</ta>
            <ta e="T40" id="Seg_496" s="T39">čaɣənä</ta>
            <ta e="T41" id="Seg_497" s="T40">a</ta>
            <ta e="T42" id="Seg_498" s="T41">Nʼüra</ta>
            <ta e="T43" id="Seg_499" s="T42">tʼärɨ-n</ta>
            <ta e="T44" id="Seg_500" s="T43">Man</ta>
            <ta e="T45" id="Seg_501" s="T44">qaːj-za-u</ta>
            <ta e="T46" id="Seg_502" s="T45">qwäzi</ta>
            <ta e="T47" id="Seg_503" s="T46">pʼötʼi-m</ta>
            <ta e="T48" id="Seg_504" s="T47">tat</ta>
            <ta e="T49" id="Seg_505" s="T48">čagnä-tdɨ-ze</ta>
            <ta e="T50" id="Seg_506" s="T49">man</ta>
            <ta e="T51" id="Seg_507" s="T50">kur-on-na-n</ta>
            <ta e="T52" id="Seg_508" s="T51">a</ta>
            <ta e="T53" id="Seg_509" s="T52">čaɣənä</ta>
            <ta e="T54" id="Seg_510" s="T53">por-ɨ-pba</ta>
            <ta e="T55" id="Seg_511" s="T54">a</ta>
            <ta e="T56" id="Seg_512" s="T55">man</ta>
            <ta e="T57" id="Seg_513" s="T56">tʼän</ta>
            <ta e="T58" id="Seg_514" s="T57">üt-se</ta>
            <ta e="T59" id="Seg_515" s="T58">qamǯə-nna-u</ta>
            <ta e="T60" id="Seg_516" s="T59">a</ta>
            <ta e="T61" id="Seg_517" s="T60">täp</ta>
            <ta e="T62" id="Seg_518" s="T61">kɨcʼiwat-pa</ta>
            <ta e="T63" id="Seg_519" s="T62">i</ta>
            <ta e="T64" id="Seg_520" s="T63">nɨ-kga</ta>
            <ta e="T65" id="Seg_521" s="T64">sʼütʼdʼi-t</ta>
            <ta e="T66" id="Seg_522" s="T65">šeːɣa-n</ta>
            <ta e="T67" id="Seg_523" s="T66">je-s</ta>
            <ta e="T68" id="Seg_524" s="T67">mekga</ta>
            <ta e="T69" id="Seg_525" s="T68">ʒalka-n</ta>
            <ta e="T70" id="Seg_526" s="T69">je-s</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_527" s="T1">tʼätʼa-w</ta>
            <ta e="T3" id="Seg_528" s="T2">aːdʼa-w</ta>
            <ta e="T4" id="Seg_529" s="T3">mekka</ta>
            <ta e="T5" id="Seg_530" s="T4">čaɣɨnä-m</ta>
            <ta e="T6" id="Seg_531" s="T5">meː-ku-sɨ</ta>
            <ta e="T7" id="Seg_532" s="T6">kor</ta>
            <ta e="T8" id="Seg_533" s="T7">sʼütdʼe</ta>
            <ta e="T9" id="Seg_534" s="T8">čaɣɨnä</ta>
            <ta e="T10" id="Seg_535" s="T9">a</ta>
            <ta e="T11" id="Seg_536" s="T10">Nʼüra</ta>
            <ta e="T12" id="Seg_537" s="T11">na</ta>
            <ta e="T13" id="Seg_538" s="T12">čaɣɨnä-se</ta>
            <ta e="T14" id="Seg_539" s="T13">qwəzi</ta>
            <ta e="T15" id="Seg_540" s="T14">pötte-m</ta>
            <ta e="T16" id="Seg_541" s="T15">qaːj-mbɨ-t</ta>
            <ta e="T17" id="Seg_542" s="T16">poːne-n</ta>
            <ta e="T18" id="Seg_543" s="T17">eː-sɨ</ta>
            <ta e="T19" id="Seg_544" s="T18">qwəzi</ta>
            <ta e="T20" id="Seg_545" s="T19">pötte</ta>
            <ta e="T21" id="Seg_546" s="T20">soro-sɨ</ta>
            <ta e="T22" id="Seg_547" s="T21">täp</ta>
            <ta e="T23" id="Seg_548" s="T22">čaɣɨnä</ta>
            <ta e="T24" id="Seg_549" s="T23">pödədʼi-mbɨ</ta>
            <ta e="T25" id="Seg_550" s="T24">i</ta>
            <ta e="T26" id="Seg_551" s="T25">por-ɨ-le</ta>
            <ta e="T27" id="Seg_552" s="T26">übɨ-r-ɨ-mbɨ</ta>
            <ta e="T28" id="Seg_553" s="T27">a</ta>
            <ta e="T29" id="Seg_554" s="T28">Čʼigatkin</ta>
            <ta e="T30" id="Seg_555" s="T29">Nʼüra</ta>
            <ta e="T31" id="Seg_556" s="T30">tʼärɨ-n</ta>
            <ta e="T32" id="Seg_557" s="T31">mekka</ta>
            <ta e="T33" id="Seg_558" s="T32">te-nan</ta>
            <ta e="T34" id="Seg_559" s="T33">čaɣɨnä</ta>
            <ta e="T35" id="Seg_560" s="T34">por-ɨ-mbɨ-ntɨ</ta>
            <ta e="T36" id="Seg_561" s="T35">a</ta>
            <ta e="T37" id="Seg_562" s="T36">man</ta>
            <ta e="T38" id="Seg_563" s="T37">tʼärɨ-ŋ</ta>
            <ta e="T39" id="Seg_564" s="T38">qaj</ta>
            <ta e="T40" id="Seg_565" s="T39">čaɣɨnä</ta>
            <ta e="T41" id="Seg_566" s="T40">a</ta>
            <ta e="T42" id="Seg_567" s="T41">Nʼüra</ta>
            <ta e="T43" id="Seg_568" s="T42">tʼärɨ-n</ta>
            <ta e="T44" id="Seg_569" s="T43">man</ta>
            <ta e="T45" id="Seg_570" s="T44">qaːj-sɨ-w</ta>
            <ta e="T46" id="Seg_571" s="T45">qwəzi</ta>
            <ta e="T47" id="Seg_572" s="T46">pötte-m</ta>
            <ta e="T48" id="Seg_573" s="T47">tan</ta>
            <ta e="T49" id="Seg_574" s="T48">čaɣɨnä-tdɨ-se</ta>
            <ta e="T50" id="Seg_575" s="T49">man</ta>
            <ta e="T51" id="Seg_576" s="T50">kur-ol-nɨ-ŋ</ta>
            <ta e="T52" id="Seg_577" s="T51">a</ta>
            <ta e="T53" id="Seg_578" s="T52">čaɣɨnä</ta>
            <ta e="T54" id="Seg_579" s="T53">por-ɨ-mbɨ</ta>
            <ta e="T55" id="Seg_580" s="T54">a</ta>
            <ta e="T56" id="Seg_581" s="T55">man</ta>
            <ta e="T57" id="Seg_582" s="T56">tʼaŋ</ta>
            <ta e="T58" id="Seg_583" s="T57">üt-se</ta>
            <ta e="T59" id="Seg_584" s="T58">qamǯu-nɨ-w</ta>
            <ta e="T60" id="Seg_585" s="T59">a</ta>
            <ta e="T61" id="Seg_586" s="T60">täp</ta>
            <ta e="T62" id="Seg_587" s="T61">kɨcʼwat-mbɨ</ta>
            <ta e="T63" id="Seg_588" s="T62">i</ta>
            <ta e="T64" id="Seg_589" s="T63">nɨ-ku</ta>
            <ta e="T65" id="Seg_590" s="T64">sʼütdʼe-tə</ta>
            <ta e="T66" id="Seg_591" s="T65">šeːɣa-ŋ</ta>
            <ta e="T67" id="Seg_592" s="T66">eː-sɨ</ta>
            <ta e="T68" id="Seg_593" s="T67">mekka</ta>
            <ta e="T69" id="Seg_594" s="T68">ʒalka-ŋ</ta>
            <ta e="T70" id="Seg_595" s="T69">eː-sɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_596" s="T1">father.[NOM]-1SG</ta>
            <ta e="T3" id="Seg_597" s="T2">father.[NOM]-1SG</ta>
            <ta e="T4" id="Seg_598" s="T3">I.ALL</ta>
            <ta e="T5" id="Seg_599" s="T4">wash_tub-ACC</ta>
            <ta e="T6" id="Seg_600" s="T5">do-HAB-PST.[3SG.S]</ta>
            <ta e="T7" id="Seg_601" s="T6">deep</ta>
            <ta e="T8" id="Seg_602" s="T7">inside.[NOM]</ta>
            <ta e="T9" id="Seg_603" s="T8">wash_tub.[NOM]</ta>
            <ta e="T10" id="Seg_604" s="T9">and</ta>
            <ta e="T11" id="Seg_605" s="T10">Njura.[NOM]</ta>
            <ta e="T12" id="Seg_606" s="T11">this</ta>
            <ta e="T13" id="Seg_607" s="T12">wash_tub-INSTR</ta>
            <ta e="T14" id="Seg_608" s="T13">iron</ta>
            <ta e="T15" id="Seg_609" s="T14">stove-ACC</ta>
            <ta e="T16" id="Seg_610" s="T15">cover-PST.NAR-3SG.O</ta>
            <ta e="T17" id="Seg_611" s="T16">outward(s)-ADV.LOC</ta>
            <ta e="T18" id="Seg_612" s="T17">be-PST.[3SG.S]</ta>
            <ta e="T19" id="Seg_613" s="T18">iron</ta>
            <ta e="T20" id="Seg_614" s="T19">stove.[NOM]</ta>
            <ta e="T21" id="Seg_615" s="T20">rain-PST.[3SG.S]</ta>
            <ta e="T22" id="Seg_616" s="T21">(s)he.[NOM]</ta>
            <ta e="T23" id="Seg_617" s="T22">wash_tub.[NOM]</ta>
            <ta e="T24" id="Seg_618" s="T23">warm.up-PST.NAR.[3SG.S]</ta>
            <ta e="T25" id="Seg_619" s="T24">and</ta>
            <ta e="T26" id="Seg_620" s="T25">burn-EP-CVB</ta>
            <ta e="T27" id="Seg_621" s="T26">begin-DRV-EP-PST.NAR.[3SG.S]</ta>
            <ta e="T28" id="Seg_622" s="T27">and</ta>
            <ta e="T29" id="Seg_623" s="T28">Chigatkin.[NOM]</ta>
            <ta e="T30" id="Seg_624" s="T29">Njura.[NOM]</ta>
            <ta e="T31" id="Seg_625" s="T30">say-3SG.S</ta>
            <ta e="T32" id="Seg_626" s="T31">I.ALL</ta>
            <ta e="T33" id="Seg_627" s="T32">you.PL.NOM-ADES</ta>
            <ta e="T34" id="Seg_628" s="T33">wash_tub.[NOM]</ta>
            <ta e="T35" id="Seg_629" s="T34">burn-EP-DUR-INFER.[3SG.S]</ta>
            <ta e="T36" id="Seg_630" s="T35">and</ta>
            <ta e="T37" id="Seg_631" s="T36">I.NOM</ta>
            <ta e="T38" id="Seg_632" s="T37">say-1SG.S</ta>
            <ta e="T39" id="Seg_633" s="T38">what.[NOM]</ta>
            <ta e="T40" id="Seg_634" s="T39">wash_tub.[NOM]</ta>
            <ta e="T41" id="Seg_635" s="T40">and</ta>
            <ta e="T42" id="Seg_636" s="T41">Njura.[NOM]</ta>
            <ta e="T43" id="Seg_637" s="T42">say-3SG.S</ta>
            <ta e="T44" id="Seg_638" s="T43">I.NOM</ta>
            <ta e="T45" id="Seg_639" s="T44">cover-PST-1SG.O</ta>
            <ta e="T46" id="Seg_640" s="T45">iron</ta>
            <ta e="T47" id="Seg_641" s="T46">stove-ACC</ta>
            <ta e="T48" id="Seg_642" s="T47">you.SG.GEN</ta>
            <ta e="T49" id="Seg_643" s="T48">wash_tub-OBL.2SG-INSTR</ta>
            <ta e="T50" id="Seg_644" s="T49">I.NOM</ta>
            <ta e="T51" id="Seg_645" s="T50">go-MOM-CO-1SG.S</ta>
            <ta e="T52" id="Seg_646" s="T51">and</ta>
            <ta e="T53" id="Seg_647" s="T52">wash_tub.[NOM]</ta>
            <ta e="T54" id="Seg_648" s="T53">burn-EP-DUR.[3SG.S]</ta>
            <ta e="T55" id="Seg_649" s="T54">and</ta>
            <ta e="T56" id="Seg_650" s="T55">I.NOM</ta>
            <ta e="T57" id="Seg_651" s="T56">fast</ta>
            <ta e="T58" id="Seg_652" s="T57">water-INSTR</ta>
            <ta e="T59" id="Seg_653" s="T58">pour-CO-1SG.O</ta>
            <ta e="T60" id="Seg_654" s="T59">and</ta>
            <ta e="T61" id="Seg_655" s="T60">(s)he.[NOM]</ta>
            <ta e="T62" id="Seg_656" s="T61">get.scared-PST.NAR.[3SG.S]</ta>
            <ta e="T63" id="Seg_657" s="T62">and</ta>
            <ta e="T64" id="Seg_658" s="T63">stand-HAB.[3SG.S]</ta>
            <ta e="T65" id="Seg_659" s="T64">inside.[NOM]-3SG</ta>
            <ta e="T66" id="Seg_660" s="T65">black-ADVZ</ta>
            <ta e="T67" id="Seg_661" s="T66">be-PST.[3SG.S]</ta>
            <ta e="T68" id="Seg_662" s="T67">I.ALL</ta>
            <ta e="T69" id="Seg_663" s="T68">feel.sorry-ADVZ</ta>
            <ta e="T70" id="Seg_664" s="T69">be-PST.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_665" s="T1">тятя.[NOM]-1SG</ta>
            <ta e="T3" id="Seg_666" s="T2">отец.[NOM]-1SG</ta>
            <ta e="T4" id="Seg_667" s="T3">я.ALL</ta>
            <ta e="T5" id="Seg_668" s="T4">корыто-ACC</ta>
            <ta e="T6" id="Seg_669" s="T5">сделать-HAB-PST.[3SG.S]</ta>
            <ta e="T7" id="Seg_670" s="T6">глубокий</ta>
            <ta e="T8" id="Seg_671" s="T7">нутро.[NOM]</ta>
            <ta e="T9" id="Seg_672" s="T8">корыто.[NOM]</ta>
            <ta e="T10" id="Seg_673" s="T9">а</ta>
            <ta e="T11" id="Seg_674" s="T10">Нюра.[NOM]</ta>
            <ta e="T12" id="Seg_675" s="T11">этот</ta>
            <ta e="T13" id="Seg_676" s="T12">корыто-INSTR</ta>
            <ta e="T14" id="Seg_677" s="T13">железный</ta>
            <ta e="T15" id="Seg_678" s="T14">печь-ACC</ta>
            <ta e="T16" id="Seg_679" s="T15">накрыть-PST.NAR-3SG.O</ta>
            <ta e="T17" id="Seg_680" s="T16">наружу-ADV.LOC</ta>
            <ta e="T18" id="Seg_681" s="T17">быть-PST.[3SG.S]</ta>
            <ta e="T19" id="Seg_682" s="T18">железный</ta>
            <ta e="T20" id="Seg_683" s="T19">печь.[NOM]</ta>
            <ta e="T21" id="Seg_684" s="T20">идти(про_дождь)-PST.[3SG.S]</ta>
            <ta e="T22" id="Seg_685" s="T21">он(а).[NOM]</ta>
            <ta e="T23" id="Seg_686" s="T22">корыто.[NOM]</ta>
            <ta e="T24" id="Seg_687" s="T23">нагреться-PST.NAR.[3SG.S]</ta>
            <ta e="T25" id="Seg_688" s="T24">и</ta>
            <ta e="T26" id="Seg_689" s="T25">сгореть-EP-CVB</ta>
            <ta e="T27" id="Seg_690" s="T26">начать-DRV-EP-PST.NAR.[3SG.S]</ta>
            <ta e="T28" id="Seg_691" s="T27">а</ta>
            <ta e="T29" id="Seg_692" s="T28">Чигаткин.[NOM]</ta>
            <ta e="T30" id="Seg_693" s="T29">Нюра.[NOM]</ta>
            <ta e="T31" id="Seg_694" s="T30">сказать-3SG.S</ta>
            <ta e="T32" id="Seg_695" s="T31">я.ALL</ta>
            <ta e="T33" id="Seg_696" s="T32">вы.PL.NOM-ADES</ta>
            <ta e="T34" id="Seg_697" s="T33">корыто.[NOM]</ta>
            <ta e="T35" id="Seg_698" s="T34">сгореть-EP-DUR-INFER.[3SG.S]</ta>
            <ta e="T36" id="Seg_699" s="T35">а</ta>
            <ta e="T37" id="Seg_700" s="T36">я.NOM</ta>
            <ta e="T38" id="Seg_701" s="T37">сказать-1SG.S</ta>
            <ta e="T39" id="Seg_702" s="T38">что.[NOM]</ta>
            <ta e="T40" id="Seg_703" s="T39">корыто.[NOM]</ta>
            <ta e="T41" id="Seg_704" s="T40">а</ta>
            <ta e="T42" id="Seg_705" s="T41">Нюра.[NOM]</ta>
            <ta e="T43" id="Seg_706" s="T42">сказать-3SG.S</ta>
            <ta e="T44" id="Seg_707" s="T43">я.NOM</ta>
            <ta e="T45" id="Seg_708" s="T44">накрыть-PST-1SG.O</ta>
            <ta e="T46" id="Seg_709" s="T45">железный</ta>
            <ta e="T47" id="Seg_710" s="T46">печь-ACC</ta>
            <ta e="T48" id="Seg_711" s="T47">ты.GEN</ta>
            <ta e="T49" id="Seg_712" s="T48">корыто-OBL.2SG-INSTR</ta>
            <ta e="T50" id="Seg_713" s="T49">я.NOM</ta>
            <ta e="T51" id="Seg_714" s="T50">ходить-MOM-CO-1SG.S</ta>
            <ta e="T52" id="Seg_715" s="T51">а</ta>
            <ta e="T53" id="Seg_716" s="T52">корыто.[NOM]</ta>
            <ta e="T54" id="Seg_717" s="T53">сгореть-EP-DUR.[3SG.S]</ta>
            <ta e="T55" id="Seg_718" s="T54">а</ta>
            <ta e="T56" id="Seg_719" s="T55">я.NOM</ta>
            <ta e="T57" id="Seg_720" s="T56">быстро</ta>
            <ta e="T58" id="Seg_721" s="T57">вода-INSTR</ta>
            <ta e="T59" id="Seg_722" s="T58">налить-CO-1SG.O</ta>
            <ta e="T60" id="Seg_723" s="T59">а</ta>
            <ta e="T61" id="Seg_724" s="T60">он(а).[NOM]</ta>
            <ta e="T62" id="Seg_725" s="T61">испугаться-PST.NAR.[3SG.S]</ta>
            <ta e="T63" id="Seg_726" s="T62">и</ta>
            <ta e="T64" id="Seg_727" s="T63">стоять-HAB.[3SG.S]</ta>
            <ta e="T65" id="Seg_728" s="T64">нутро.[NOM]-3SG</ta>
            <ta e="T66" id="Seg_729" s="T65">чёрный-ADVZ</ta>
            <ta e="T67" id="Seg_730" s="T66">быть-PST.[3SG.S]</ta>
            <ta e="T68" id="Seg_731" s="T67">я.ALL</ta>
            <ta e="T69" id="Seg_732" s="T68">жалко-ADVZ</ta>
            <ta e="T70" id="Seg_733" s="T69">быть-PST.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_734" s="T1">n.[n:case]-n:poss</ta>
            <ta e="T3" id="Seg_735" s="T2">n.[n:case]-n:poss</ta>
            <ta e="T4" id="Seg_736" s="T3">pers</ta>
            <ta e="T5" id="Seg_737" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_738" s="T5">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T7" id="Seg_739" s="T6">adj</ta>
            <ta e="T8" id="Seg_740" s="T7">n.[n:case]</ta>
            <ta e="T9" id="Seg_741" s="T8">n.[n:case]</ta>
            <ta e="T10" id="Seg_742" s="T9">conj</ta>
            <ta e="T11" id="Seg_743" s="T10">nprop.[n:case]</ta>
            <ta e="T12" id="Seg_744" s="T11">dem</ta>
            <ta e="T13" id="Seg_745" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_746" s="T13">adj</ta>
            <ta e="T15" id="Seg_747" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_748" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_749" s="T16">adv-adv:case</ta>
            <ta e="T18" id="Seg_750" s="T17">v-v:tense.[v:pn]</ta>
            <ta e="T19" id="Seg_751" s="T18">adj</ta>
            <ta e="T20" id="Seg_752" s="T19">n.[n:case]</ta>
            <ta e="T21" id="Seg_753" s="T20">v-v:tense.[v:pn]</ta>
            <ta e="T22" id="Seg_754" s="T21">pers.[n:case]</ta>
            <ta e="T23" id="Seg_755" s="T22">n.[n:case]</ta>
            <ta e="T24" id="Seg_756" s="T23">v-v:tense.[v:pn]</ta>
            <ta e="T25" id="Seg_757" s="T24">conj</ta>
            <ta e="T26" id="Seg_758" s="T25">v-v:ins-v&gt;adv</ta>
            <ta e="T27" id="Seg_759" s="T26">v-v&gt;v-v:ins-v:tense.[v:pn]</ta>
            <ta e="T28" id="Seg_760" s="T27">conj</ta>
            <ta e="T29" id="Seg_761" s="T28">nprop.[n:case]</ta>
            <ta e="T30" id="Seg_762" s="T29">nprop.[n:case]</ta>
            <ta e="T31" id="Seg_763" s="T30">v-v:pn</ta>
            <ta e="T32" id="Seg_764" s="T31">pers</ta>
            <ta e="T33" id="Seg_765" s="T32">pers-n:case</ta>
            <ta e="T34" id="Seg_766" s="T33">n.[n:case]</ta>
            <ta e="T35" id="Seg_767" s="T34">v-v:ins-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T36" id="Seg_768" s="T35">conj</ta>
            <ta e="T37" id="Seg_769" s="T36">pers</ta>
            <ta e="T38" id="Seg_770" s="T37">v-v:pn</ta>
            <ta e="T39" id="Seg_771" s="T38">interrog.[n:case]</ta>
            <ta e="T40" id="Seg_772" s="T39">n.[n:case]</ta>
            <ta e="T41" id="Seg_773" s="T40">conj</ta>
            <ta e="T42" id="Seg_774" s="T41">nprop.[n:case]</ta>
            <ta e="T43" id="Seg_775" s="T42">v-v:pn</ta>
            <ta e="T44" id="Seg_776" s="T43">pers</ta>
            <ta e="T45" id="Seg_777" s="T44">v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_778" s="T45">adj</ta>
            <ta e="T47" id="Seg_779" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_780" s="T47">pers</ta>
            <ta e="T49" id="Seg_781" s="T48">n-n:obl.poss-n:case</ta>
            <ta e="T50" id="Seg_782" s="T49">pers</ta>
            <ta e="T51" id="Seg_783" s="T50">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T52" id="Seg_784" s="T51">conj</ta>
            <ta e="T53" id="Seg_785" s="T52">n.[n:case]</ta>
            <ta e="T54" id="Seg_786" s="T53">v-v:ins-v&gt;v.[v:pn]</ta>
            <ta e="T55" id="Seg_787" s="T54">conj</ta>
            <ta e="T56" id="Seg_788" s="T55">pers</ta>
            <ta e="T57" id="Seg_789" s="T56">adv</ta>
            <ta e="T58" id="Seg_790" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_791" s="T58">v-v:ins-v:pn</ta>
            <ta e="T60" id="Seg_792" s="T59">conj</ta>
            <ta e="T61" id="Seg_793" s="T60">pers.[n:case]</ta>
            <ta e="T62" id="Seg_794" s="T61">v-v:tense.[v:pn]</ta>
            <ta e="T63" id="Seg_795" s="T62">conj</ta>
            <ta e="T64" id="Seg_796" s="T63">v-v&gt;v.[v:pn]</ta>
            <ta e="T65" id="Seg_797" s="T64">n.[n:case]-n:poss</ta>
            <ta e="T66" id="Seg_798" s="T65">adj-adj&gt;adv</ta>
            <ta e="T67" id="Seg_799" s="T66">v-v:tense.[v:pn]</ta>
            <ta e="T68" id="Seg_800" s="T67">pers</ta>
            <ta e="T69" id="Seg_801" s="T68">adv-adj&gt;adv</ta>
            <ta e="T70" id="Seg_802" s="T69">v-v:tense.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_803" s="T1">n</ta>
            <ta e="T3" id="Seg_804" s="T2">n</ta>
            <ta e="T4" id="Seg_805" s="T3">pers</ta>
            <ta e="T5" id="Seg_806" s="T4">n</ta>
            <ta e="T6" id="Seg_807" s="T5">v</ta>
            <ta e="T7" id="Seg_808" s="T6">adj</ta>
            <ta e="T8" id="Seg_809" s="T7">n</ta>
            <ta e="T9" id="Seg_810" s="T8">n</ta>
            <ta e="T10" id="Seg_811" s="T9">conj</ta>
            <ta e="T11" id="Seg_812" s="T10">nprop</ta>
            <ta e="T12" id="Seg_813" s="T11">dem</ta>
            <ta e="T13" id="Seg_814" s="T12">n</ta>
            <ta e="T14" id="Seg_815" s="T13">adj</ta>
            <ta e="T15" id="Seg_816" s="T14">n</ta>
            <ta e="T16" id="Seg_817" s="T15">v</ta>
            <ta e="T17" id="Seg_818" s="T16">adv</ta>
            <ta e="T18" id="Seg_819" s="T17">v</ta>
            <ta e="T19" id="Seg_820" s="T18">adj</ta>
            <ta e="T20" id="Seg_821" s="T19">n</ta>
            <ta e="T21" id="Seg_822" s="T20">v</ta>
            <ta e="T22" id="Seg_823" s="T21">pers</ta>
            <ta e="T23" id="Seg_824" s="T22">n</ta>
            <ta e="T24" id="Seg_825" s="T23">v</ta>
            <ta e="T25" id="Seg_826" s="T24">conj</ta>
            <ta e="T26" id="Seg_827" s="T25">adv</ta>
            <ta e="T27" id="Seg_828" s="T26">v</ta>
            <ta e="T28" id="Seg_829" s="T27">conj</ta>
            <ta e="T29" id="Seg_830" s="T28">nprop</ta>
            <ta e="T30" id="Seg_831" s="T29">nprop</ta>
            <ta e="T31" id="Seg_832" s="T30">v</ta>
            <ta e="T32" id="Seg_833" s="T31">pers</ta>
            <ta e="T33" id="Seg_834" s="T32">pers</ta>
            <ta e="T34" id="Seg_835" s="T33">n</ta>
            <ta e="T35" id="Seg_836" s="T34">v</ta>
            <ta e="T36" id="Seg_837" s="T35">conj</ta>
            <ta e="T37" id="Seg_838" s="T36">pers</ta>
            <ta e="T38" id="Seg_839" s="T37">v</ta>
            <ta e="T39" id="Seg_840" s="T38">interrog</ta>
            <ta e="T40" id="Seg_841" s="T39">n</ta>
            <ta e="T41" id="Seg_842" s="T40">conj</ta>
            <ta e="T42" id="Seg_843" s="T41">nprop</ta>
            <ta e="T43" id="Seg_844" s="T42">v</ta>
            <ta e="T44" id="Seg_845" s="T43">pers</ta>
            <ta e="T45" id="Seg_846" s="T44">v</ta>
            <ta e="T46" id="Seg_847" s="T45">adj</ta>
            <ta e="T47" id="Seg_848" s="T46">n</ta>
            <ta e="T48" id="Seg_849" s="T47">pers</ta>
            <ta e="T49" id="Seg_850" s="T48">n</ta>
            <ta e="T50" id="Seg_851" s="T49">pers</ta>
            <ta e="T51" id="Seg_852" s="T50">v</ta>
            <ta e="T52" id="Seg_853" s="T51">conj</ta>
            <ta e="T53" id="Seg_854" s="T52">n</ta>
            <ta e="T54" id="Seg_855" s="T53">v</ta>
            <ta e="T55" id="Seg_856" s="T54">conj</ta>
            <ta e="T56" id="Seg_857" s="T55">pers</ta>
            <ta e="T57" id="Seg_858" s="T56">adv</ta>
            <ta e="T58" id="Seg_859" s="T57">n</ta>
            <ta e="T59" id="Seg_860" s="T58">v</ta>
            <ta e="T60" id="Seg_861" s="T59">conj</ta>
            <ta e="T61" id="Seg_862" s="T60">pers</ta>
            <ta e="T62" id="Seg_863" s="T61">v</ta>
            <ta e="T63" id="Seg_864" s="T62">conj</ta>
            <ta e="T64" id="Seg_865" s="T63">v</ta>
            <ta e="T65" id="Seg_866" s="T64">n</ta>
            <ta e="T66" id="Seg_867" s="T65">adv</ta>
            <ta e="T67" id="Seg_868" s="T66">v</ta>
            <ta e="T68" id="Seg_869" s="T67">pers</ta>
            <ta e="T69" id="Seg_870" s="T68">adv</ta>
            <ta e="T70" id="Seg_871" s="T69">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_872" s="T1">np.h:A 0.1.h:Poss</ta>
            <ta e="T4" id="Seg_873" s="T3">pro.h:B</ta>
            <ta e="T5" id="Seg_874" s="T4">np:P</ta>
            <ta e="T11" id="Seg_875" s="T10">np.h:A</ta>
            <ta e="T13" id="Seg_876" s="T12">np:Ins</ta>
            <ta e="T15" id="Seg_877" s="T14">np:Th</ta>
            <ta e="T17" id="Seg_878" s="T16">adv:L</ta>
            <ta e="T20" id="Seg_879" s="T19">np:Th</ta>
            <ta e="T21" id="Seg_880" s="T20">0.3:Th</ta>
            <ta e="T22" id="Seg_881" s="T21">pro:P</ta>
            <ta e="T27" id="Seg_882" s="T26">0.3:P</ta>
            <ta e="T30" id="Seg_883" s="T29">np.h:A</ta>
            <ta e="T32" id="Seg_884" s="T31">pro.h:R</ta>
            <ta e="T33" id="Seg_885" s="T32">pro.h:Poss</ta>
            <ta e="T34" id="Seg_886" s="T33">np:P</ta>
            <ta e="T37" id="Seg_887" s="T36">pro.h:A</ta>
            <ta e="T42" id="Seg_888" s="T41">np.h:A</ta>
            <ta e="T44" id="Seg_889" s="T43">pro.h:A</ta>
            <ta e="T47" id="Seg_890" s="T46">np:Th</ta>
            <ta e="T48" id="Seg_891" s="T47">pro.h:Poss</ta>
            <ta e="T49" id="Seg_892" s="T48">np:Ins</ta>
            <ta e="T50" id="Seg_893" s="T49">pro.h:A</ta>
            <ta e="T53" id="Seg_894" s="T52">np:P</ta>
            <ta e="T56" id="Seg_895" s="T55">pro.h:A</ta>
            <ta e="T58" id="Seg_896" s="T57">np:Ins</ta>
            <ta e="T59" id="Seg_897" s="T58">0.3:Th</ta>
            <ta e="T61" id="Seg_898" s="T60">pro.h:E</ta>
            <ta e="T64" id="Seg_899" s="T63">0.3.h:Th</ta>
            <ta e="T65" id="Seg_900" s="T64">np:Th 0.3.h:Poss</ta>
            <ta e="T68" id="Seg_901" s="T67">pro.h:E</ta>
            <ta e="T70" id="Seg_902" s="T69">0.3:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_903" s="T1">np.h:S</ta>
            <ta e="T5" id="Seg_904" s="T4">np:O</ta>
            <ta e="T6" id="Seg_905" s="T5">v:pred</ta>
            <ta e="T11" id="Seg_906" s="T10">np.h:S</ta>
            <ta e="T15" id="Seg_907" s="T14">np:O</ta>
            <ta e="T16" id="Seg_908" s="T15">v:pred</ta>
            <ta e="T18" id="Seg_909" s="T17">v:pred</ta>
            <ta e="T20" id="Seg_910" s="T19">np:S</ta>
            <ta e="T21" id="Seg_911" s="T20">0.3:S v:pred</ta>
            <ta e="T22" id="Seg_912" s="T21">pro:S</ta>
            <ta e="T24" id="Seg_913" s="T23">v:pred</ta>
            <ta e="T27" id="Seg_914" s="T26">0.3:S v:pred</ta>
            <ta e="T30" id="Seg_915" s="T29">np.h:S</ta>
            <ta e="T31" id="Seg_916" s="T30">v:pred</ta>
            <ta e="T34" id="Seg_917" s="T33">np:S</ta>
            <ta e="T35" id="Seg_918" s="T34">v:pred</ta>
            <ta e="T37" id="Seg_919" s="T36">pro.h:S</ta>
            <ta e="T38" id="Seg_920" s="T37">v:pred</ta>
            <ta e="T42" id="Seg_921" s="T41">np.h:S</ta>
            <ta e="T43" id="Seg_922" s="T42">v:pred</ta>
            <ta e="T44" id="Seg_923" s="T43">pro.h:S</ta>
            <ta e="T45" id="Seg_924" s="T44">v:pred</ta>
            <ta e="T47" id="Seg_925" s="T46">np:O</ta>
            <ta e="T50" id="Seg_926" s="T49">pro.h:S</ta>
            <ta e="T51" id="Seg_927" s="T50">v:pred</ta>
            <ta e="T53" id="Seg_928" s="T52">np:S</ta>
            <ta e="T54" id="Seg_929" s="T53">v:pred</ta>
            <ta e="T56" id="Seg_930" s="T55">pro.h:S</ta>
            <ta e="T59" id="Seg_931" s="T58">v:pred 0.3:O</ta>
            <ta e="T61" id="Seg_932" s="T60">pro.h:S</ta>
            <ta e="T62" id="Seg_933" s="T61">v:pred</ta>
            <ta e="T64" id="Seg_934" s="T63">0.3.h:S v:pred</ta>
            <ta e="T65" id="Seg_935" s="T64">np:S</ta>
            <ta e="T67" id="Seg_936" s="T66">v:pred</ta>
            <ta e="T70" id="Seg_937" s="T69">0.3:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_938" s="T1">RUS:core</ta>
            <ta e="T10" id="Seg_939" s="T9">RUS:gram</ta>
            <ta e="T25" id="Seg_940" s="T24">RUS:gram</ta>
            <ta e="T28" id="Seg_941" s="T27">RUS:gram</ta>
            <ta e="T36" id="Seg_942" s="T35">RUS:gram</ta>
            <ta e="T41" id="Seg_943" s="T40">RUS:gram</ta>
            <ta e="T52" id="Seg_944" s="T51">RUS:gram</ta>
            <ta e="T55" id="Seg_945" s="T54">RUS:gram</ta>
            <ta e="T60" id="Seg_946" s="T59">RUS:gram</ta>
            <ta e="T63" id="Seg_947" s="T62">RUS:gram</ta>
            <ta e="T69" id="Seg_948" s="T68">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T9" id="Seg_949" s="T1">My father made a deep wash-tub for me.</ta>
            <ta e="T16" id="Seg_950" s="T9">And Njura covered an oven with this wash-tub.</ta>
            <ta e="T20" id="Seg_951" s="T16">There was an iron oven outside.</ta>
            <ta e="T21" id="Seg_952" s="T20">It was raining.</ta>
            <ta e="T27" id="Seg_953" s="T21">It warmed up and started to burn.</ta>
            <ta e="T35" id="Seg_954" s="T27">And Njura Chigatkina said to me: “Your wash-tub is burning.”</ta>
            <ta e="T40" id="Seg_955" s="T35">And I said: “What wash-tub?”</ta>
            <ta e="T49" id="Seg_956" s="T40">And Njura said: “I covered the iron oven with your wash-tub.”</ta>
            <ta e="T51" id="Seg_957" s="T49">I ran.</ta>
            <ta e="T54" id="Seg_958" s="T51">And the wash-tub was burning.</ta>
            <ta e="T59" id="Seg_959" s="T54">And I poured water over it.</ta>
            <ta e="T64" id="Seg_960" s="T59">And she got frightened and stood [aside].</ta>
            <ta e="T67" id="Seg_961" s="T64">It was black inside.</ta>
            <ta e="T70" id="Seg_962" s="T67">I regretted.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T9" id="Seg_963" s="T1">Mein Vater machte einen tiefen Waschzuber für mich.</ta>
            <ta e="T16" id="Seg_964" s="T9">Und Njura deckte den Ofen mit diesem Waschzuber ab.</ta>
            <ta e="T20" id="Seg_965" s="T16">Draußen war ein Eisenofen.</ta>
            <ta e="T21" id="Seg_966" s="T20">Es regnete.</ta>
            <ta e="T27" id="Seg_967" s="T21">Der Waschzuber heizte sich auf und begann zu brennen.</ta>
            <ta e="T35" id="Seg_968" s="T27">Und Njura Chigatkina sagte zu mir: "Dein Waschzuber brennt."</ta>
            <ta e="T40" id="Seg_969" s="T35">Und ich sagte: "Welcher Waschzuber?"</ta>
            <ta e="T49" id="Seg_970" s="T40">Und Njura sagte: "Ich habe den Eisenofen mit deinem Waschzuber abgedeckt."</ta>
            <ta e="T51" id="Seg_971" s="T49">Ich rannte.</ta>
            <ta e="T54" id="Seg_972" s="T51">Und der Waschzuber brannte.</ta>
            <ta e="T59" id="Seg_973" s="T54">Und ich goss Wasser über ihn.</ta>
            <ta e="T64" id="Seg_974" s="T59">Und sie bekam Angst und stand [abseits].</ta>
            <ta e="T67" id="Seg_975" s="T64">Er war innen schwarz.</ta>
            <ta e="T70" id="Seg_976" s="T67">Ich bereute es.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T9" id="Seg_977" s="T1">Мой отец мне сделал корыто глубокое.</ta>
            <ta e="T16" id="Seg_978" s="T9">А Нюра эти корытом железную печку закрыла.</ta>
            <ta e="T20" id="Seg_979" s="T16">На улице была железная печь.</ta>
            <ta e="T21" id="Seg_980" s="T20">Шел дождь.</ta>
            <ta e="T27" id="Seg_981" s="T21">Оно (корыто) нагрелось и гореть начало.</ta>
            <ta e="T35" id="Seg_982" s="T27">А Чигаткина Нюра сказала мне: “У вас корыто горит”.</ta>
            <ta e="T40" id="Seg_983" s="T35">А я говорю: “Какое корыто?”</ta>
            <ta e="T49" id="Seg_984" s="T40">А Нюра говорит: “Я накрыла железную печку твоим корытом”.</ta>
            <ta e="T51" id="Seg_985" s="T49">Я побежала.</ta>
            <ta e="T54" id="Seg_986" s="T51">А корыто горит.</ta>
            <ta e="T59" id="Seg_987" s="T54">А я быстро водой залила.</ta>
            <ta e="T64" id="Seg_988" s="T59">А она испугалась и стоит.</ta>
            <ta e="T67" id="Seg_989" s="T64">Внутри черно было.</ta>
            <ta e="T70" id="Seg_990" s="T67">Мне жалко было.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T9" id="Seg_991" s="T1">отец мне сделал корыто глубокое</ta>
            <ta e="T16" id="Seg_992" s="T9">а Нюра это корыто закрыла железную печку закрыла</ta>
            <ta e="T20" id="Seg_993" s="T16">на улице была железная печь</ta>
            <ta e="T21" id="Seg_994" s="T20">шел сильный дождь</ta>
            <ta e="T27" id="Seg_995" s="T21">оно согрелось гореть начинает</ta>
            <ta e="T35" id="Seg_996" s="T27">Чигаткина Нюра сказала мне у вас корыто горит</ta>
            <ta e="T40" id="Seg_997" s="T35">и говорю какое корыто</ta>
            <ta e="T49" id="Seg_998" s="T40">я накрыла железную печку твоим корытом</ta>
            <ta e="T51" id="Seg_999" s="T49">я побежала</ta>
            <ta e="T54" id="Seg_1000" s="T51">корыто горит</ta>
            <ta e="T59" id="Seg_1001" s="T54">я быстро водой залила</ta>
            <ta e="T64" id="Seg_1002" s="T59">она испугалась и стоит или кицʼи′ватпа (?)</ta>
            <ta e="T67" id="Seg_1003" s="T64">внутри черное было</ta>
            <ta e="T70" id="Seg_1004" s="T67">мне жалко было</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T35" id="Seg_1005" s="T27">[BrM:] INFER?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
