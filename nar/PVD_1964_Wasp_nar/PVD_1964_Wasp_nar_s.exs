<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Wasp_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Wasp_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">25</ud-information>
            <ud-information attribute-name="# HIAT:w">19</ud-information>
            <ud-information attribute-name="# e">19</ud-information>
            <ud-information attribute-name="# HIAT:u">6</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T20" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Loɣə</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">mattə</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">taɣədʼetda</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Mazɨm</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">pulʼdʼatdə</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_23" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">Pulʼdʼzʼelgu</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">ne</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">nadə</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_35" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">Tep</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">sɨm</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">pulʼdʼelle</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">dadərɨm</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_50" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">Man</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">waddou</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">weš</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">tčaggudin</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_65" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">Tepɛr</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">kuːnen</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">küzetǯin</ts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T20" id="Seg_76" n="sc" s="T1">
               <ts e="T2" id="Seg_78" n="e" s="T1">Loɣə </ts>
               <ts e="T3" id="Seg_80" n="e" s="T2">mattə </ts>
               <ts e="T4" id="Seg_82" n="e" s="T3">taɣədʼetda. </ts>
               <ts e="T5" id="Seg_84" n="e" s="T4">Mazɨm </ts>
               <ts e="T6" id="Seg_86" n="e" s="T5">pulʼdʼatdə. </ts>
               <ts e="T7" id="Seg_88" n="e" s="T6">Pulʼdʼzʼelgu </ts>
               <ts e="T8" id="Seg_90" n="e" s="T7">ne </ts>
               <ts e="T9" id="Seg_92" n="e" s="T8">nadə. </ts>
               <ts e="T10" id="Seg_94" n="e" s="T9">Tep </ts>
               <ts e="T11" id="Seg_96" n="e" s="T10">sɨm </ts>
               <ts e="T12" id="Seg_98" n="e" s="T11">pulʼdʼelle </ts>
               <ts e="T13" id="Seg_100" n="e" s="T12">dadərɨm. </ts>
               <ts e="T14" id="Seg_102" n="e" s="T13">Man </ts>
               <ts e="T15" id="Seg_104" n="e" s="T14">waddou </ts>
               <ts e="T16" id="Seg_106" n="e" s="T15">weš </ts>
               <ts e="T17" id="Seg_108" n="e" s="T16">tčaggudin. </ts>
               <ts e="T18" id="Seg_110" n="e" s="T17">Tepɛr </ts>
               <ts e="T19" id="Seg_112" n="e" s="T18">kuːnen </ts>
               <ts e="T20" id="Seg_114" n="e" s="T19">küzetǯin. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_115" s="T1">PVD_1964_Wasp_nar.001 (001.001)</ta>
            <ta e="T6" id="Seg_116" s="T4">PVD_1964_Wasp_nar.002 (001.002)</ta>
            <ta e="T9" id="Seg_117" s="T6">PVD_1964_Wasp_nar.003 (001.003)</ta>
            <ta e="T13" id="Seg_118" s="T9">PVD_1964_Wasp_nar.004 (001.004)</ta>
            <ta e="T17" id="Seg_119" s="T13">PVD_1964_Wasp_nar.005 (001.005)</ta>
            <ta e="T20" id="Seg_120" s="T17">PVD_1964_Wasp_nar.006 (001.006)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_121" s="T1">лоɣъ ′маттъ ′таɣъдʼетд̂а.</ta>
            <ta e="T6" id="Seg_122" s="T4">мазым ′пулʼдʼатдъ.</ta>
            <ta e="T9" id="Seg_123" s="T6">′пулʼдʼзʼ(дʼ)елгу не надъ.</ta>
            <ta e="T13" id="Seg_124" s="T9">теп сым ′пулʼдʼеllе ′д̂адърым.</ta>
            <ta e="T17" id="Seg_125" s="T13">ман вад̂′доу̹ ′вештчаг̂гудин.</ta>
            <ta e="T20" id="Seg_126" s="T17">те′пɛр ′кӯне(ъ)н кӱ′зетджин.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_127" s="T1">loɣə mattə taɣədʼetd̂a.</ta>
            <ta e="T6" id="Seg_128" s="T4">mazɨm pulʼdʼatdə.</ta>
            <ta e="T9" id="Seg_129" s="T6">pulʼdʼzʼ(dʼ)elgu ne nadə.</ta>
            <ta e="T13" id="Seg_130" s="T9">tep sɨm pulʼdʼelle d̂adərɨm.</ta>
            <ta e="T17" id="Seg_131" s="T13">man wad̂dou̹ weštčaĝgudin.</ta>
            <ta e="T20" id="Seg_132" s="T17">tepɛr kuːne(ə)n küzetǯin.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_133" s="T1">Loɣə mattə taɣədʼetda. </ta>
            <ta e="T6" id="Seg_134" s="T4">Mazɨm pulʼdʼatdə. </ta>
            <ta e="T9" id="Seg_135" s="T6">Pulʼdʼzʼelgu ne nadə. </ta>
            <ta e="T13" id="Seg_136" s="T9">Tep sɨm pulʼdʼelle dadərɨm. </ta>
            <ta e="T17" id="Seg_137" s="T13">Man waddou weš tčaggudin. </ta>
            <ta e="T20" id="Seg_138" s="T17">Tepɛr kuːnen küzetǯin. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_139" s="T1">loɣə</ta>
            <ta e="T3" id="Seg_140" s="T2">mat-tə</ta>
            <ta e="T4" id="Seg_141" s="T3">taɣədʼe-tda</ta>
            <ta e="T5" id="Seg_142" s="T4">mazɨm</ta>
            <ta e="T6" id="Seg_143" s="T5">pulʼdʼa-tdə</ta>
            <ta e="T7" id="Seg_144" s="T6">pulʼdʼzʼe-l-gu</ta>
            <ta e="T8" id="Seg_145" s="T7">ne</ta>
            <ta e="T9" id="Seg_146" s="T8">nadə</ta>
            <ta e="T10" id="Seg_147" s="T9">tep</ta>
            <ta e="T11" id="Seg_148" s="T10">sɨm</ta>
            <ta e="T12" id="Seg_149" s="T11">pulʼdʼe-l-le</ta>
            <ta e="T13" id="Seg_150" s="T12">dad-ə-r-ɨ-m</ta>
            <ta e="T14" id="Seg_151" s="T13">man</ta>
            <ta e="T15" id="Seg_152" s="T14">waddo-u</ta>
            <ta e="T16" id="Seg_153" s="T15">weš</ta>
            <ta e="T17" id="Seg_154" s="T16">tčaggu-di-n</ta>
            <ta e="T18" id="Seg_155" s="T17">tepɛr</ta>
            <ta e="T19" id="Seg_156" s="T18">kuːne-n</ta>
            <ta e="T20" id="Seg_157" s="T19">küz-etǯi-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_158" s="T1">loɣə</ta>
            <ta e="T3" id="Seg_159" s="T2">maːt-ntə</ta>
            <ta e="T4" id="Seg_160" s="T3">taɣətʼe-ntɨ</ta>
            <ta e="T5" id="Seg_161" s="T4">mazɨm</ta>
            <ta e="T6" id="Seg_162" s="T5">pulʼdzʼi-tɨn</ta>
            <ta e="T7" id="Seg_163" s="T6">pulʼdzʼi-l-gu</ta>
            <ta e="T8" id="Seg_164" s="T7">ne</ta>
            <ta e="T9" id="Seg_165" s="T8">nadə</ta>
            <ta e="T10" id="Seg_166" s="T9">täp</ta>
            <ta e="T11" id="Seg_167" s="T10">mazɨm</ta>
            <ta e="T12" id="Seg_168" s="T11">pulʼdzʼi-l-le</ta>
            <ta e="T13" id="Seg_169" s="T12">tat-ɨ-r-ɨ-w</ta>
            <ta e="T14" id="Seg_170" s="T13">man</ta>
            <ta e="T15" id="Seg_171" s="T14">wandǝ-w</ta>
            <ta e="T16" id="Seg_172" s="T15">wesʼ</ta>
            <ta e="T17" id="Seg_173" s="T16">čaŋgu-dʼi-n</ta>
            <ta e="T18" id="Seg_174" s="T17">teper</ta>
            <ta e="T19" id="Seg_175" s="T18">kundɨ-ŋ</ta>
            <ta e="T20" id="Seg_176" s="T19">küzɨ-enǯɨ-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_177" s="T1">wasp.[NOM]</ta>
            <ta e="T3" id="Seg_178" s="T2">house-ILL</ta>
            <ta e="T4" id="Seg_179" s="T3">fly.into-INFER.[3SG.S]</ta>
            <ta e="T5" id="Seg_180" s="T4">I.ACC</ta>
            <ta e="T6" id="Seg_181" s="T5">sting-3PL</ta>
            <ta e="T7" id="Seg_182" s="T6">sting-INCH-INF</ta>
            <ta e="T8" id="Seg_183" s="T7">NEG</ta>
            <ta e="T9" id="Seg_184" s="T8">one.should</ta>
            <ta e="T10" id="Seg_185" s="T9">(s)he.[NOM]</ta>
            <ta e="T11" id="Seg_186" s="T10">I.ACC</ta>
            <ta e="T12" id="Seg_187" s="T11">sting-INCH-CVB</ta>
            <ta e="T13" id="Seg_188" s="T12">bring-EP-FRQ-EP-1SG.O</ta>
            <ta e="T14" id="Seg_189" s="T13">I.GEN</ta>
            <ta e="T15" id="Seg_190" s="T14">face.[NOM]-1SG</ta>
            <ta e="T16" id="Seg_191" s="T15">all</ta>
            <ta e="T17" id="Seg_192" s="T16">swell.up-RFL-3SG.S</ta>
            <ta e="T18" id="Seg_193" s="T17">now</ta>
            <ta e="T19" id="Seg_194" s="T18">long-ADVZ</ta>
            <ta e="T20" id="Seg_195" s="T19">hurt-FUT-3SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_196" s="T1">оса.[NOM]</ta>
            <ta e="T3" id="Seg_197" s="T2">дом-ILL</ta>
            <ta e="T4" id="Seg_198" s="T3">влететь-INFER.[3SG.S]</ta>
            <ta e="T5" id="Seg_199" s="T4">я.ACC</ta>
            <ta e="T6" id="Seg_200" s="T5">ужалить-3PL</ta>
            <ta e="T7" id="Seg_201" s="T6">ужалить-INCH-INF</ta>
            <ta e="T8" id="Seg_202" s="T7">NEG</ta>
            <ta e="T9" id="Seg_203" s="T8">надо</ta>
            <ta e="T10" id="Seg_204" s="T9">он(а).[NOM]</ta>
            <ta e="T11" id="Seg_205" s="T10">я.ACC</ta>
            <ta e="T12" id="Seg_206" s="T11">ужалить-INCH-CVB</ta>
            <ta e="T13" id="Seg_207" s="T12">принести-EP-FRQ-EP-1SG.O</ta>
            <ta e="T14" id="Seg_208" s="T13">я.GEN</ta>
            <ta e="T15" id="Seg_209" s="T14">лицо.[NOM]-1SG</ta>
            <ta e="T16" id="Seg_210" s="T15">весь</ta>
            <ta e="T17" id="Seg_211" s="T16">распухать-RFL-3SG.S</ta>
            <ta e="T18" id="Seg_212" s="T17">теперь</ta>
            <ta e="T19" id="Seg_213" s="T18">долго-ADVZ</ta>
            <ta e="T20" id="Seg_214" s="T19">болеть-FUT-3SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_215" s="T1">n.[n:case]</ta>
            <ta e="T3" id="Seg_216" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_217" s="T3">v-v:mood.[v:pn]</ta>
            <ta e="T5" id="Seg_218" s="T4">pers</ta>
            <ta e="T6" id="Seg_219" s="T5">v-v:pn</ta>
            <ta e="T7" id="Seg_220" s="T6">v-v&gt;v-v:inf</ta>
            <ta e="T8" id="Seg_221" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_222" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_223" s="T9">pers.[n:case]</ta>
            <ta e="T11" id="Seg_224" s="T10">pers</ta>
            <ta e="T12" id="Seg_225" s="T11">v-v&gt;v-v&gt;adv</ta>
            <ta e="T13" id="Seg_226" s="T12">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T14" id="Seg_227" s="T13">pers</ta>
            <ta e="T15" id="Seg_228" s="T14">n.[n:case]-n:poss</ta>
            <ta e="T16" id="Seg_229" s="T15">quant</ta>
            <ta e="T17" id="Seg_230" s="T16">v-v&gt;v-v:pn</ta>
            <ta e="T18" id="Seg_231" s="T17">adv</ta>
            <ta e="T19" id="Seg_232" s="T18">adv-adj&gt;adv</ta>
            <ta e="T20" id="Seg_233" s="T19">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_234" s="T1">n</ta>
            <ta e="T3" id="Seg_235" s="T2">n</ta>
            <ta e="T4" id="Seg_236" s="T3">v</ta>
            <ta e="T5" id="Seg_237" s="T4">pers</ta>
            <ta e="T6" id="Seg_238" s="T5">v</ta>
            <ta e="T7" id="Seg_239" s="T6">v</ta>
            <ta e="T8" id="Seg_240" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_241" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_242" s="T9">pers</ta>
            <ta e="T11" id="Seg_243" s="T10">pers</ta>
            <ta e="T12" id="Seg_244" s="T11">adv</ta>
            <ta e="T13" id="Seg_245" s="T12">v</ta>
            <ta e="T14" id="Seg_246" s="T13">pers</ta>
            <ta e="T15" id="Seg_247" s="T14">n</ta>
            <ta e="T16" id="Seg_248" s="T15">quant</ta>
            <ta e="T17" id="Seg_249" s="T16">v</ta>
            <ta e="T18" id="Seg_250" s="T17">adv</ta>
            <ta e="T19" id="Seg_251" s="T18">adv</ta>
            <ta e="T20" id="Seg_252" s="T19">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_253" s="T1">np:A</ta>
            <ta e="T3" id="Seg_254" s="T2">np:G</ta>
            <ta e="T5" id="Seg_255" s="T4">pro.h:P</ta>
            <ta e="T6" id="Seg_256" s="T5">0.3:A</ta>
            <ta e="T7" id="Seg_257" s="T6">v:Th</ta>
            <ta e="T10" id="Seg_258" s="T9">pro:A</ta>
            <ta e="T11" id="Seg_259" s="T10">pro.h:P</ta>
            <ta e="T14" id="Seg_260" s="T13">pro.h:Poss</ta>
            <ta e="T15" id="Seg_261" s="T14">np:P</ta>
            <ta e="T18" id="Seg_262" s="T17">adv:Time</ta>
            <ta e="T20" id="Seg_263" s="T19">0.3:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_264" s="T1">np:S</ta>
            <ta e="T4" id="Seg_265" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_266" s="T4">pro.h:O</ta>
            <ta e="T6" id="Seg_267" s="T5">0.3.h:S v:pred</ta>
            <ta e="T7" id="Seg_268" s="T6">v:O</ta>
            <ta e="T9" id="Seg_269" s="T8">ptcl:pred</ta>
            <ta e="T10" id="Seg_270" s="T9">pro:S</ta>
            <ta e="T11" id="Seg_271" s="T10">pro.h:O</ta>
            <ta e="T12" id="Seg_272" s="T11">s:temp</ta>
            <ta e="T13" id="Seg_273" s="T12">v:pred</ta>
            <ta e="T15" id="Seg_274" s="T14">np:S</ta>
            <ta e="T17" id="Seg_275" s="T16">v:pred</ta>
            <ta e="T20" id="Seg_276" s="T19">0.3:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T8" id="Seg_277" s="T7">RUS:gram</ta>
            <ta e="T9" id="Seg_278" s="T8">RUS:mod</ta>
            <ta e="T16" id="Seg_279" s="T15">RUS:core</ta>
            <ta e="T18" id="Seg_280" s="T17">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_281" s="T1">A wasp flew into the house.</ta>
            <ta e="T6" id="Seg_282" s="T4">I was stung.</ta>
            <ta e="T9" id="Seg_283" s="T6">One shouldn't sting.</ta>
            <ta e="T13" id="Seg_284" s="T9">It stings me.</ta>
            <ta e="T17" id="Seg_285" s="T13">My face swelled up.</ta>
            <ta e="T20" id="Seg_286" s="T17">Now it will be hurting for a long time.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_287" s="T1">Eine Wespe flog ins Haus.</ta>
            <ta e="T6" id="Seg_288" s="T4">Ich wurde gestochen.</ta>
            <ta e="T9" id="Seg_289" s="T6">Man sollte nicht stechen.</ta>
            <ta e="T13" id="Seg_290" s="T9">Sie hat mich gestochen.</ta>
            <ta e="T17" id="Seg_291" s="T13">Mein Gesicht ist angeschwollen.</ta>
            <ta e="T20" id="Seg_292" s="T17">Jetzt wird es lange wehtun.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_293" s="T1">Оса залетела в избу.</ta>
            <ta e="T6" id="Seg_294" s="T4">Меня ужалили.</ta>
            <ta e="T9" id="Seg_295" s="T6">Жалить не надо.</ta>
            <ta e="T13" id="Seg_296" s="T9">Она меня жалит.</ta>
            <ta e="T17" id="Seg_297" s="T13">У меня лицо все распухло.</ta>
            <ta e="T20" id="Seg_298" s="T17">Теперь долго будет болеть.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_299" s="T1">оса залетела в избу</ta>
            <ta e="T6" id="Seg_300" s="T4">меня жалили</ta>
            <ta e="T9" id="Seg_301" s="T6">жалить не надо</ta>
            <ta e="T13" id="Seg_302" s="T9">а она меня жалит</ta>
            <ta e="T17" id="Seg_303" s="T13">у меня лицо все распухло</ta>
            <ta e="T20" id="Seg_304" s="T17">теперь долго будет болеть</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T13" id="Seg_305" s="T9">[BrM:] 1SG.O? 3SgG would be expected instead.</ta>
            <ta e="T17" id="Seg_306" s="T13">[BrM:] 'weštčaggudin' changed to 'weš tčaggudin'.</ta>
            <ta e="T20" id="Seg_307" s="T17">[KuAI:] Variant: 'kuːnən'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
