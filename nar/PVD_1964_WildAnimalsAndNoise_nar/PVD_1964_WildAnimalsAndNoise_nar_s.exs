<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_WildAnimalsAndNoise_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_WildAnimalsAndNoise_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">39</ud-information>
            <ud-information attribute-name="# HIAT:w">29</ud-information>
            <ud-information attribute-name="# e">29</ud-information>
            <ud-information attribute-name="# HIAT:u">7</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T30" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Tʼarattə</ts>
                  <nts id="Seg_5" n="HIAT:ip">,</nts>
                  <nts id="Seg_6" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_8" n="HIAT:w" s="T2">surɨmla</ts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_11" n="HIAT:w" s="T3">šumnä</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_14" n="HIAT:w" s="T4">larɨppattə</ts>
                  <nts id="Seg_15" n="HIAT:ip">.</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_18" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">A</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">tebla</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">ass</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">larɨpattə</ts>
                  <nts id="Seg_30" n="HIAT:ip">.</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_33" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">Näjɣumɨm</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">abbattə</ts>
                  <nts id="Seg_39" n="HIAT:ip">,</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">qwezʼi</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">wattoɣɨn</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">abbattə</ts>
                  <nts id="Seg_49" n="HIAT:ip">.</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_52" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">Mašina</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">udotʼepaː</ts>
                  <nts id="Seg_58" n="HIAT:ip">,</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">täp</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">uʒ</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">qwannɨpaː</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_71" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">Menan</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">madʼoɣɨn</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">qwɛldʼakam</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">dʼäːdʼeptəqwattə</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_86" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">Što</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">surəm</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">ik</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">tʼüə</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_101" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">Teb</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">ass</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_109" n="HIAT:w" s="T29">larɨpbat</ts>
                  <nts id="Seg_110" n="HIAT:ip">.</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T30" id="Seg_112" n="sc" s="T1">
               <ts e="T2" id="Seg_114" n="e" s="T1">Tʼarattə, </ts>
               <ts e="T3" id="Seg_116" n="e" s="T2">surɨmla </ts>
               <ts e="T4" id="Seg_118" n="e" s="T3">šumnä </ts>
               <ts e="T5" id="Seg_120" n="e" s="T4">larɨppattə. </ts>
               <ts e="T6" id="Seg_122" n="e" s="T5">A </ts>
               <ts e="T7" id="Seg_124" n="e" s="T6">tebla </ts>
               <ts e="T8" id="Seg_126" n="e" s="T7">ass </ts>
               <ts e="T9" id="Seg_128" n="e" s="T8">larɨpattə. </ts>
               <ts e="T10" id="Seg_130" n="e" s="T9">Näjɣumɨm </ts>
               <ts e="T11" id="Seg_132" n="e" s="T10">abbattə, </ts>
               <ts e="T12" id="Seg_134" n="e" s="T11">qwezʼi </ts>
               <ts e="T13" id="Seg_136" n="e" s="T12">wattoɣɨn </ts>
               <ts e="T14" id="Seg_138" n="e" s="T13">abbattə. </ts>
               <ts e="T15" id="Seg_140" n="e" s="T14">Mašina </ts>
               <ts e="T16" id="Seg_142" n="e" s="T15">udotʼepaː, </ts>
               <ts e="T17" id="Seg_144" n="e" s="T16">täp </ts>
               <ts e="T18" id="Seg_146" n="e" s="T17">uʒ </ts>
               <ts e="T19" id="Seg_148" n="e" s="T18">qwannɨpaː. </ts>
               <ts e="T20" id="Seg_150" n="e" s="T19">Menan </ts>
               <ts e="T21" id="Seg_152" n="e" s="T20">madʼoɣɨn </ts>
               <ts e="T22" id="Seg_154" n="e" s="T21">qwɛldʼakam </ts>
               <ts e="T23" id="Seg_156" n="e" s="T22">dʼäːdʼeptəqwattə. </ts>
               <ts e="T24" id="Seg_158" n="e" s="T23">Što </ts>
               <ts e="T25" id="Seg_160" n="e" s="T24">surəm </ts>
               <ts e="T26" id="Seg_162" n="e" s="T25">ik </ts>
               <ts e="T27" id="Seg_164" n="e" s="T26">tʼüə. </ts>
               <ts e="T28" id="Seg_166" n="e" s="T27">Teb </ts>
               <ts e="T29" id="Seg_168" n="e" s="T28">ass </ts>
               <ts e="T30" id="Seg_170" n="e" s="T29">larɨpbat. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_171" s="T1">PVD_1964_WildAnimalsAndNoise_nar.001 (001.001)</ta>
            <ta e="T9" id="Seg_172" s="T5">PVD_1964_WildAnimalsAndNoise_nar.002 (001.002)</ta>
            <ta e="T14" id="Seg_173" s="T9">PVD_1964_WildAnimalsAndNoise_nar.003 (001.003)</ta>
            <ta e="T19" id="Seg_174" s="T14">PVD_1964_WildAnimalsAndNoise_nar.004 (001.004)</ta>
            <ta e="T23" id="Seg_175" s="T19">PVD_1964_WildAnimalsAndNoise_nar.005 (001.005)</ta>
            <ta e="T27" id="Seg_176" s="T23">PVD_1964_WildAnimalsAndNoise_nar.006 (001.006)</ta>
            <ta e="T30" id="Seg_177" s="T27">PVD_1964_WildAnimalsAndNoise_nar.007 (001.007)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_178" s="T1">тʼа′раттъ, ′сурымла ′шумнӓ ′ларыппаттъ.</ta>
            <ta e="T9" id="Seg_179" s="T5">а теб′ла асс лары′паттъ.</ta>
            <ta e="T14" id="Seg_180" s="T9">′нӓйɣумым ′аббаттъ, kwезʼи wаттоɣын ′аб̂баттъ.</ta>
            <ta e="T19" id="Seg_181" s="T14">машина ′удотʼе′па̄, тӓп ′уж ′kwанныпа̄.</ta>
            <ta e="T23" id="Seg_182" s="T19">ме′нан ма′дʼоɣын ′kwɛlдʼакам ′д̂ʼӓ̄дʼептъ′kwаттъ.</ta>
            <ta e="T27" id="Seg_183" s="T23">што ′суръм ′ик ′тʼӱъ.</ta>
            <ta e="T30" id="Seg_184" s="T27">теб асс ′ларыпбат.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_185" s="T1">tʼarattə, surɨmla šumnä larɨppattə.</ta>
            <ta e="T9" id="Seg_186" s="T5">a tebla ass larɨpattə.</ta>
            <ta e="T14" id="Seg_187" s="T9">näjɣumɨm abbattə, qwezʼi wattoɣɨn ab̂battə.</ta>
            <ta e="T19" id="Seg_188" s="T14">mašina udotʼepaː, täp uʒ qwannɨpaː.</ta>
            <ta e="T23" id="Seg_189" s="T19">menan madʼoɣɨn qwɛldʼakam d̂ʼäːdʼeptəqwattə.</ta>
            <ta e="T27" id="Seg_190" s="T23">što surəm ik tʼüə.</ta>
            <ta e="T30" id="Seg_191" s="T27">teb ass larɨpbat.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_192" s="T1">Tʼarattə, surɨmla šumnä larɨppattə. </ta>
            <ta e="T9" id="Seg_193" s="T5">A tebla ass larɨpattə. </ta>
            <ta e="T14" id="Seg_194" s="T9">Näjɣumɨm abbattə, qwezʼi wattoɣɨn abbattə. </ta>
            <ta e="T19" id="Seg_195" s="T14">Mašina udotʼepaː, täp uʒ qwannɨpaː. </ta>
            <ta e="T23" id="Seg_196" s="T19">Menan madʼoɣɨn qwɛldʼakam dʼäːdʼeptəqwattə. </ta>
            <ta e="T27" id="Seg_197" s="T23">Što surəm ik tʼüə. </ta>
            <ta e="T30" id="Seg_198" s="T27">Teb ass larɨpbat. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_199" s="T1">tʼara-ttə</ta>
            <ta e="T3" id="Seg_200" s="T2">surɨm-la</ta>
            <ta e="T4" id="Seg_201" s="T3">šum-nä</ta>
            <ta e="T5" id="Seg_202" s="T4">larɨ-ppa-ttə</ta>
            <ta e="T6" id="Seg_203" s="T5">a</ta>
            <ta e="T7" id="Seg_204" s="T6">teb-la</ta>
            <ta e="T8" id="Seg_205" s="T7">ass</ta>
            <ta e="T9" id="Seg_206" s="T8">larɨ-pa-ttə</ta>
            <ta e="T10" id="Seg_207" s="T9">nä-j-ɣum-ɨ-m</ta>
            <ta e="T11" id="Seg_208" s="T10">ab-ba-ttə</ta>
            <ta e="T12" id="Seg_209" s="T11">qwezʼi</ta>
            <ta e="T13" id="Seg_210" s="T12">watt-o-ɣɨn</ta>
            <ta e="T14" id="Seg_211" s="T13">ab-ba-ttə</ta>
            <ta e="T15" id="Seg_212" s="T14">mašina</ta>
            <ta e="T16" id="Seg_213" s="T15">udo-tʼe-paː</ta>
            <ta e="T17" id="Seg_214" s="T16">täp</ta>
            <ta e="T18" id="Seg_215" s="T17">uʒ</ta>
            <ta e="T19" id="Seg_216" s="T18">qwan-nɨ-paː</ta>
            <ta e="T20" id="Seg_217" s="T19">me-nan</ta>
            <ta e="T21" id="Seg_218" s="T20">madʼ-o-ɣɨn</ta>
            <ta e="T22" id="Seg_219" s="T21">qwɛldʼa-ka-m</ta>
            <ta e="T23" id="Seg_220" s="T22">dʼäːdʼe-ptə-q-wa-ttə</ta>
            <ta e="T24" id="Seg_221" s="T23">što</ta>
            <ta e="T25" id="Seg_222" s="T24">surəm</ta>
            <ta e="T26" id="Seg_223" s="T25">ik</ta>
            <ta e="T27" id="Seg_224" s="T26">tʼü-ə</ta>
            <ta e="T28" id="Seg_225" s="T27">teb</ta>
            <ta e="T29" id="Seg_226" s="T28">ass</ta>
            <ta e="T30" id="Seg_227" s="T29">larɨ-pba-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_228" s="T1">tʼärɨ-tɨn</ta>
            <ta e="T3" id="Seg_229" s="T2">suːrum-la</ta>
            <ta e="T4" id="Seg_230" s="T3">šum-nä</ta>
            <ta e="T5" id="Seg_231" s="T4">*larɨ-mbɨ-tɨn</ta>
            <ta e="T6" id="Seg_232" s="T5">a</ta>
            <ta e="T7" id="Seg_233" s="T6">täp-la</ta>
            <ta e="T8" id="Seg_234" s="T7">asa</ta>
            <ta e="T9" id="Seg_235" s="T8">*larɨ-mbɨ-tɨn</ta>
            <ta e="T10" id="Seg_236" s="T9">ne-lʼ-qum-ɨ-m</ta>
            <ta e="T11" id="Seg_237" s="T10">am-mbɨ-tɨn</ta>
            <ta e="T12" id="Seg_238" s="T11">qwəzi</ta>
            <ta e="T13" id="Seg_239" s="T12">watt-ɨ-qɨn</ta>
            <ta e="T14" id="Seg_240" s="T13">am-mbɨ-tɨn</ta>
            <ta e="T15" id="Seg_241" s="T14">mašina</ta>
            <ta e="T16" id="Seg_242" s="T15">udɨr-ntɨ-mbɨ</ta>
            <ta e="T17" id="Seg_243" s="T16">täp</ta>
            <ta e="T18" id="Seg_244" s="T17">uʒ</ta>
            <ta e="T19" id="Seg_245" s="T18">qwan-ntɨ-mbɨ</ta>
            <ta e="T20" id="Seg_246" s="T19">me-nan</ta>
            <ta e="T21" id="Seg_247" s="T20">madʼ-ɨ-qɨn</ta>
            <ta e="T22" id="Seg_248" s="T21">qwelʼdʼi-ka-m</ta>
            <ta e="T23" id="Seg_249" s="T22">dʼäːdʼe-ptɨ-ku-nɨ-tɨn</ta>
            <ta e="T24" id="Seg_250" s="T23">što</ta>
            <ta e="T25" id="Seg_251" s="T24">suːrum</ta>
            <ta e="T26" id="Seg_252" s="T25">igə</ta>
            <ta e="T27" id="Seg_253" s="T26">tüː-nɨ</ta>
            <ta e="T28" id="Seg_254" s="T27">täp</ta>
            <ta e="T29" id="Seg_255" s="T28">asa</ta>
            <ta e="T30" id="Seg_256" s="T29">*larɨ-mbɨ-t</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_257" s="T1">say-3PL</ta>
            <ta e="T3" id="Seg_258" s="T2">wild.animal-PL.[NOM]</ta>
            <ta e="T4" id="Seg_259" s="T3">noise-ALL</ta>
            <ta e="T5" id="Seg_260" s="T4">get.afraid-DUR-3PL</ta>
            <ta e="T6" id="Seg_261" s="T5">and</ta>
            <ta e="T7" id="Seg_262" s="T6">(s)he-PL.[NOM]</ta>
            <ta e="T8" id="Seg_263" s="T7">NEG</ta>
            <ta e="T9" id="Seg_264" s="T8">get.afraid-DUR-3PL</ta>
            <ta e="T10" id="Seg_265" s="T9">woman-ADJZ-human.being-EP-ACC</ta>
            <ta e="T11" id="Seg_266" s="T10">eat-PST.NAR-3PL</ta>
            <ta e="T12" id="Seg_267" s="T11">iron</ta>
            <ta e="T13" id="Seg_268" s="T12">road-EP-LOC</ta>
            <ta e="T14" id="Seg_269" s="T13">eat-PST.NAR-3PL</ta>
            <ta e="T15" id="Seg_270" s="T14">car.[NOM]</ta>
            <ta e="T16" id="Seg_271" s="T15">stop-IPFV-PST.NAR.[3SG.S]</ta>
            <ta e="T17" id="Seg_272" s="T16">(s)he.[NOM]</ta>
            <ta e="T18" id="Seg_273" s="T17">already</ta>
            <ta e="T19" id="Seg_274" s="T18">run.away-INFER-PST.NAR.[NOM]</ta>
            <ta e="T20" id="Seg_275" s="T19">we-ADES</ta>
            <ta e="T21" id="Seg_276" s="T20">taiga-EP-LOC</ta>
            <ta e="T22" id="Seg_277" s="T21">pot-DIM-ACC</ta>
            <ta e="T23" id="Seg_278" s="T22">%clank-CAUS-HAB-CO-3PL</ta>
            <ta e="T24" id="Seg_279" s="T23">that</ta>
            <ta e="T25" id="Seg_280" s="T24">wild.animal.[NOM]</ta>
            <ta e="T26" id="Seg_281" s="T25">NEG.IMP</ta>
            <ta e="T27" id="Seg_282" s="T26">come-CO.[3SG.S]</ta>
            <ta e="T28" id="Seg_283" s="T27">(s)he.[NOM]</ta>
            <ta e="T29" id="Seg_284" s="T28">NEG</ta>
            <ta e="T30" id="Seg_285" s="T29">get.afraid-DUR-3SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_286" s="T1">сказать-3PL</ta>
            <ta e="T3" id="Seg_287" s="T2">зверь-PL.[NOM]</ta>
            <ta e="T4" id="Seg_288" s="T3">шум-ALL</ta>
            <ta e="T5" id="Seg_289" s="T4">испугаться-DUR-3PL</ta>
            <ta e="T6" id="Seg_290" s="T5">а</ta>
            <ta e="T7" id="Seg_291" s="T6">он(а)-PL.[NOM]</ta>
            <ta e="T8" id="Seg_292" s="T7">NEG</ta>
            <ta e="T9" id="Seg_293" s="T8">испугаться-DUR-3PL</ta>
            <ta e="T10" id="Seg_294" s="T9">женщина-ADJZ-человек-EP-ACC</ta>
            <ta e="T11" id="Seg_295" s="T10">съесть-PST.NAR-3PL</ta>
            <ta e="T12" id="Seg_296" s="T11">железный</ta>
            <ta e="T13" id="Seg_297" s="T12">дорога-EP-LOC</ta>
            <ta e="T14" id="Seg_298" s="T13">съесть-PST.NAR-3PL</ta>
            <ta e="T15" id="Seg_299" s="T14">машина.[NOM]</ta>
            <ta e="T16" id="Seg_300" s="T15">остановить-IPFV-PST.NAR.[3SG.S]</ta>
            <ta e="T17" id="Seg_301" s="T16">он(а).[NOM]</ta>
            <ta e="T18" id="Seg_302" s="T17">уже</ta>
            <ta e="T19" id="Seg_303" s="T18">убежать-INFER-PST.NAR.[NOM]</ta>
            <ta e="T20" id="Seg_304" s="T19">мы-ADES</ta>
            <ta e="T21" id="Seg_305" s="T20">тайга-EP-LOC</ta>
            <ta e="T22" id="Seg_306" s="T21">котёл-DIM-ACC</ta>
            <ta e="T23" id="Seg_307" s="T22">%греметь-CAUS-HAB-CO-3PL</ta>
            <ta e="T24" id="Seg_308" s="T23">что</ta>
            <ta e="T25" id="Seg_309" s="T24">зверь.[NOM]</ta>
            <ta e="T26" id="Seg_310" s="T25">NEG.IMP</ta>
            <ta e="T27" id="Seg_311" s="T26">прийти-CO.[3SG.S]</ta>
            <ta e="T28" id="Seg_312" s="T27">он(а).[NOM]</ta>
            <ta e="T29" id="Seg_313" s="T28">NEG</ta>
            <ta e="T30" id="Seg_314" s="T29">испугаться-DUR-3SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_315" s="T1">v-v:pn</ta>
            <ta e="T3" id="Seg_316" s="T2">n-n:num.[n:case]</ta>
            <ta e="T4" id="Seg_317" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_318" s="T4">v-v&gt;v-v:pn</ta>
            <ta e="T6" id="Seg_319" s="T5">conj</ta>
            <ta e="T7" id="Seg_320" s="T6">pers-n:num.[n:case]</ta>
            <ta e="T8" id="Seg_321" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_322" s="T8">v-v&gt;v-v:pn</ta>
            <ta e="T10" id="Seg_323" s="T9">n-n&gt;adj-n-n:ins-n:case</ta>
            <ta e="T11" id="Seg_324" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_325" s="T11">adj</ta>
            <ta e="T13" id="Seg_326" s="T12">n-n:ins-n:case</ta>
            <ta e="T14" id="Seg_327" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_328" s="T14">n.[n:case]</ta>
            <ta e="T16" id="Seg_329" s="T15">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T17" id="Seg_330" s="T16">pers.[n:case]</ta>
            <ta e="T18" id="Seg_331" s="T17">adv</ta>
            <ta e="T19" id="Seg_332" s="T18">v-v:mood-v:tense.[n:case]</ta>
            <ta e="T20" id="Seg_333" s="T19">pers-n:case</ta>
            <ta e="T21" id="Seg_334" s="T20">n-n:ins-n:case</ta>
            <ta e="T22" id="Seg_335" s="T21">n-n&gt;n-n:case</ta>
            <ta e="T23" id="Seg_336" s="T22">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T24" id="Seg_337" s="T23">conj</ta>
            <ta e="T25" id="Seg_338" s="T24">n.[n:case]</ta>
            <ta e="T26" id="Seg_339" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_340" s="T26">v-v:ins.[v:pn]</ta>
            <ta e="T28" id="Seg_341" s="T27">pers.[n:case]</ta>
            <ta e="T29" id="Seg_342" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_343" s="T29">v-v&gt;v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_344" s="T1">v</ta>
            <ta e="T3" id="Seg_345" s="T2">n</ta>
            <ta e="T4" id="Seg_346" s="T3">n</ta>
            <ta e="T5" id="Seg_347" s="T4">v</ta>
            <ta e="T6" id="Seg_348" s="T5">conj</ta>
            <ta e="T7" id="Seg_349" s="T6">pers</ta>
            <ta e="T8" id="Seg_350" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_351" s="T8">v</ta>
            <ta e="T10" id="Seg_352" s="T9">n</ta>
            <ta e="T11" id="Seg_353" s="T10">v</ta>
            <ta e="T12" id="Seg_354" s="T11">adj</ta>
            <ta e="T13" id="Seg_355" s="T12">n</ta>
            <ta e="T14" id="Seg_356" s="T13">v</ta>
            <ta e="T15" id="Seg_357" s="T14">n</ta>
            <ta e="T16" id="Seg_358" s="T15">v</ta>
            <ta e="T17" id="Seg_359" s="T16">pers</ta>
            <ta e="T18" id="Seg_360" s="T17">adv</ta>
            <ta e="T19" id="Seg_361" s="T18">v</ta>
            <ta e="T20" id="Seg_362" s="T19">pers</ta>
            <ta e="T21" id="Seg_363" s="T20">n</ta>
            <ta e="T22" id="Seg_364" s="T21">n</ta>
            <ta e="T23" id="Seg_365" s="T22">v</ta>
            <ta e="T24" id="Seg_366" s="T23">conj</ta>
            <ta e="T25" id="Seg_367" s="T24">n</ta>
            <ta e="T26" id="Seg_368" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_369" s="T26">v</ta>
            <ta e="T28" id="Seg_370" s="T27">pers</ta>
            <ta e="T29" id="Seg_371" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_372" s="T29">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_373" s="T1">0.3.h:A</ta>
            <ta e="T3" id="Seg_374" s="T2">np:E</ta>
            <ta e="T4" id="Seg_375" s="T3">np:Cau</ta>
            <ta e="T7" id="Seg_376" s="T6">pro:E</ta>
            <ta e="T10" id="Seg_377" s="T9">np.h:P</ta>
            <ta e="T11" id="Seg_378" s="T10">0.3:A</ta>
            <ta e="T13" id="Seg_379" s="T12">np:L</ta>
            <ta e="T14" id="Seg_380" s="T13">0.3:A 0.3.h:P</ta>
            <ta e="T15" id="Seg_381" s="T14">np:Th</ta>
            <ta e="T16" id="Seg_382" s="T15">0.3.h:A</ta>
            <ta e="T17" id="Seg_383" s="T16">pro:A</ta>
            <ta e="T21" id="Seg_384" s="T20">np:L</ta>
            <ta e="T22" id="Seg_385" s="T21">np:Th</ta>
            <ta e="T23" id="Seg_386" s="T22">0.3.h:A</ta>
            <ta e="T25" id="Seg_387" s="T24">np:A</ta>
            <ta e="T28" id="Seg_388" s="T27">pro:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_389" s="T1">0.3.h:S v:pred</ta>
            <ta e="T3" id="Seg_390" s="T2">np:S</ta>
            <ta e="T5" id="Seg_391" s="T4">v:pred</ta>
            <ta e="T7" id="Seg_392" s="T6">pro:S</ta>
            <ta e="T9" id="Seg_393" s="T8">v:pred</ta>
            <ta e="T10" id="Seg_394" s="T9">np.h:O</ta>
            <ta e="T11" id="Seg_395" s="T10">0.3:S v:pred</ta>
            <ta e="T14" id="Seg_396" s="T13">0.3:S v:pred 0.3.h:O</ta>
            <ta e="T15" id="Seg_397" s="T14">np:O</ta>
            <ta e="T16" id="Seg_398" s="T15">0.3.h:S v:pred</ta>
            <ta e="T17" id="Seg_399" s="T16">pro:S</ta>
            <ta e="T19" id="Seg_400" s="T18">v:pred</ta>
            <ta e="T22" id="Seg_401" s="T21">np:O</ta>
            <ta e="T23" id="Seg_402" s="T22">0.3.h:S v:pred</ta>
            <ta e="T25" id="Seg_403" s="T24">np:S</ta>
            <ta e="T27" id="Seg_404" s="T26">v:pred</ta>
            <ta e="T28" id="Seg_405" s="T27">pro:S</ta>
            <ta e="T30" id="Seg_406" s="T29">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_407" s="T3">RUS:core</ta>
            <ta e="T6" id="Seg_408" s="T5">RUS:gram</ta>
            <ta e="T15" id="Seg_409" s="T14">RUS:cult</ta>
            <ta e="T18" id="Seg_410" s="T17">RUS:disc</ta>
            <ta e="T24" id="Seg_411" s="T23">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_412" s="T1">Wild animals are said to be afraid of noise.</ta>
            <ta e="T9" id="Seg_413" s="T5">But they are not afraid.</ta>
            <ta e="T14" id="Seg_414" s="T9">They ate one woman, they ate her near the railroad.</ta>
            <ta e="T19" id="Seg_415" s="T14">While they were stopping the car, it was already gone.</ta>
            <ta e="T23" id="Seg_416" s="T19">In the taiga we use to clang with a pot.</ta>
            <ta e="T27" id="Seg_417" s="T23">So that a wild animal wouldn't come.</ta>
            <ta e="T30" id="Seg_418" s="T27">It is not afraid.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_419" s="T1">Man sagt, wilde Tiere fürchten sich vor Lärm.</ta>
            <ta e="T9" id="Seg_420" s="T5">Aber sie fürchten sich nicht.</ta>
            <ta e="T14" id="Seg_421" s="T9">Sie haben eine Frau gefressen, sie haben sie bei den Bahngleisen gefressen.</ta>
            <ta e="T19" id="Seg_422" s="T14">Während sie den Waggon anhielten, rannten sie schon weg.</ta>
            <ta e="T23" id="Seg_423" s="T19">In der Taiga schlugen wir für gewöhnlich auf einen Topf.</ta>
            <ta e="T27" id="Seg_424" s="T23">Damit kein wildes Tier kommt.</ta>
            <ta e="T30" id="Seg_425" s="T27">Es hat keine Angst.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_426" s="T1">Говорят, звери шума боятся.</ta>
            <ta e="T9" id="Seg_427" s="T5">А они не боятся.</ta>
            <ta e="T14" id="Seg_428" s="T9">Они женщину съели, на железной дороге съели.</ta>
            <ta e="T19" id="Seg_429" s="T14">Машину пока(?) останавливали, он уже убежал.</ta>
            <ta e="T23" id="Seg_430" s="T19">У нас в тайге котелком гремят.</ta>
            <ta e="T27" id="Seg_431" s="T23">Чтобы зверь не подходил.</ta>
            <ta e="T30" id="Seg_432" s="T27">Он не боится.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_433" s="T1">говорят звери шума боятся</ta>
            <ta e="T9" id="Seg_434" s="T5">а они не боятся</ta>
            <ta e="T14" id="Seg_435" s="T9">женщину съели на железной дороге съели</ta>
            <ta e="T19" id="Seg_436" s="T14">машину пока останавливали он уже убежал</ta>
            <ta e="T23" id="Seg_437" s="T19">у нас в тайге котелком гремят</ta>
            <ta e="T27" id="Seg_438" s="T23">чтобы зверь не подходил</ta>
            <ta e="T30" id="Seg_439" s="T27">он не боится</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T19" id="Seg_440" s="T14">[BrM:] Tentative analysis of 'udotʼepaː', unclear translation of the first clause.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
