<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Wine_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Wine_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">52</ud-information>
            <ud-information attribute-name="# HIAT:w">38</ud-information>
            <ud-information attribute-name="# e">38</ud-information>
            <ud-information attribute-name="# HIAT:u">11</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T39" id="Seg_0" n="sc" s="T1">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Täbəɣum</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">čaʒɨn</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_11" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">A</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">man</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">tebne</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">tʼaran</ts>
                  <nts id="Seg_23" n="HIAT:ip">:</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_25" n="HIAT:ip">“</nts>
                  <ts e="T8" id="Seg_27" n="HIAT:w" s="T7">Serga</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">mattə</ts>
                  <nts id="Seg_31" n="HIAT:ip">.</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_34" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">Serga</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_39" n="HIAT:w" s="T10">tattə</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">sütdinäj</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">mattə</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">serga</ts>
                  <nts id="Seg_49" n="HIAT:ip">.</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_52" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">Natʼen</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">näjɣum</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">amnɨta</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_64" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">Täp</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">aːraqačin</ts>
                  <nts id="Seg_70" n="HIAT:ip">.</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_73" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">A</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">tan</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_81" n="HIAT:w" s="T21">törǯin</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">serga</ts>
                  <nts id="Seg_85" n="HIAT:ip">.</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_88" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_90" n="HIAT:w" s="T23">Tep</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_93" n="HIAT:w" s="T24">kocʼiwaːčɨn</ts>
                  <nts id="Seg_94" n="HIAT:ip">.</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_97" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_99" n="HIAT:w" s="T25">Tastɨ</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_102" n="HIAT:w" s="T26">arakazʼe</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_105" n="HIAT:w" s="T27">äːrčibɨlʼe</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_108" n="HIAT:w" s="T28">übəreǯan</ts>
                  <nts id="Seg_109" n="HIAT:ip">.</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_112" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_114" n="HIAT:w" s="T29">Tan</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_117" n="HIAT:w" s="T30">uš</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_120" n="HIAT:w" s="T31">qajnä</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_123" n="HIAT:w" s="T32">ketket</ts>
                  <nts id="Seg_124" n="HIAT:ip">.</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_127" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_129" n="HIAT:w" s="T33">Ütčal</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_132" n="HIAT:w" s="T34">i</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_135" n="HIAT:w" s="T35">qwannɨq</ts>
                  <nts id="Seg_136" n="HIAT:ip">.</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_139" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_141" n="HIAT:w" s="T36">Tep</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_144" n="HIAT:w" s="T37">naraqa</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_147" n="HIAT:w" s="T38">döšowan</ts>
                  <nts id="Seg_148" n="HIAT:ip">.</nts>
                  <nts id="Seg_149" n="HIAT:ip">”</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T39" id="Seg_151" n="sc" s="T1">
               <ts e="T2" id="Seg_153" n="e" s="T1">Täbəɣum </ts>
               <ts e="T3" id="Seg_155" n="e" s="T2">čaʒɨn. </ts>
               <ts e="T4" id="Seg_157" n="e" s="T3">A </ts>
               <ts e="T5" id="Seg_159" n="e" s="T4">man </ts>
               <ts e="T6" id="Seg_161" n="e" s="T5">tebne </ts>
               <ts e="T7" id="Seg_163" n="e" s="T6">tʼaran: </ts>
               <ts e="T8" id="Seg_165" n="e" s="T7">“Serga </ts>
               <ts e="T9" id="Seg_167" n="e" s="T8">mattə. </ts>
               <ts e="T10" id="Seg_169" n="e" s="T9">Serga </ts>
               <ts e="T11" id="Seg_171" n="e" s="T10">tattə </ts>
               <ts e="T12" id="Seg_173" n="e" s="T11">sütdinäj </ts>
               <ts e="T13" id="Seg_175" n="e" s="T12">mattə </ts>
               <ts e="T14" id="Seg_177" n="e" s="T13">serga. </ts>
               <ts e="T15" id="Seg_179" n="e" s="T14">Natʼen </ts>
               <ts e="T16" id="Seg_181" n="e" s="T15">näjɣum </ts>
               <ts e="T17" id="Seg_183" n="e" s="T16">amnɨta. </ts>
               <ts e="T18" id="Seg_185" n="e" s="T17">Täp </ts>
               <ts e="T19" id="Seg_187" n="e" s="T18">aːraqačin. </ts>
               <ts e="T20" id="Seg_189" n="e" s="T19">A </ts>
               <ts e="T21" id="Seg_191" n="e" s="T20">tan </ts>
               <ts e="T22" id="Seg_193" n="e" s="T21">törǯin </ts>
               <ts e="T23" id="Seg_195" n="e" s="T22">serga. </ts>
               <ts e="T24" id="Seg_197" n="e" s="T23">Tep </ts>
               <ts e="T25" id="Seg_199" n="e" s="T24">kocʼiwaːčɨn. </ts>
               <ts e="T26" id="Seg_201" n="e" s="T25">Tastɨ </ts>
               <ts e="T27" id="Seg_203" n="e" s="T26">arakazʼe </ts>
               <ts e="T28" id="Seg_205" n="e" s="T27">äːrčibɨlʼe </ts>
               <ts e="T29" id="Seg_207" n="e" s="T28">übəreǯan. </ts>
               <ts e="T30" id="Seg_209" n="e" s="T29">Tan </ts>
               <ts e="T31" id="Seg_211" n="e" s="T30">uš </ts>
               <ts e="T32" id="Seg_213" n="e" s="T31">qajnä </ts>
               <ts e="T33" id="Seg_215" n="e" s="T32">ketket. </ts>
               <ts e="T34" id="Seg_217" n="e" s="T33">Ütčal </ts>
               <ts e="T35" id="Seg_219" n="e" s="T34">i </ts>
               <ts e="T36" id="Seg_221" n="e" s="T35">qwannɨq. </ts>
               <ts e="T37" id="Seg_223" n="e" s="T36">Tep </ts>
               <ts e="T38" id="Seg_225" n="e" s="T37">naraqa </ts>
               <ts e="T39" id="Seg_227" n="e" s="T38">döšowan.” </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_228" s="T1">PVD_1964_Wine_nar.001 (001.001)</ta>
            <ta e="T9" id="Seg_229" s="T3">PVD_1964_Wine_nar.002 (001.002)</ta>
            <ta e="T14" id="Seg_230" s="T9">PVD_1964_Wine_nar.003 (001.003)</ta>
            <ta e="T17" id="Seg_231" s="T14">PVD_1964_Wine_nar.004 (001.004)</ta>
            <ta e="T19" id="Seg_232" s="T17">PVD_1964_Wine_nar.005 (001.005)</ta>
            <ta e="T23" id="Seg_233" s="T19">PVD_1964_Wine_nar.006 (001.006)</ta>
            <ta e="T25" id="Seg_234" s="T23">PVD_1964_Wine_nar.007 (001.007)</ta>
            <ta e="T29" id="Seg_235" s="T25">PVD_1964_Wine_nar.008 (001.008)</ta>
            <ta e="T33" id="Seg_236" s="T29">PVD_1964_Wine_nar.009 (001.009)</ta>
            <ta e="T36" id="Seg_237" s="T33">PVD_1964_Wine_nar.010 (001.010)</ta>
            <ta e="T39" id="Seg_238" s="T36">PVD_1964_Wine_nar.011 (001.011)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_239" s="T1">тӓбъ′ɣум ′тшажын.</ta>
            <ta e="T9" id="Seg_240" s="T3">а ман тебне тʼа′ран: ′серга маттъ.</ta>
            <ta e="T14" id="Seg_241" s="T9">серга таттъ сӱтди′нӓй ′маттъ ′серга.</ta>
            <ta e="T17" id="Seg_242" s="T14">на′тʼен ′нӓйɣум ′амныта.</ta>
            <ta e="T19" id="Seg_243" s="T17">тӓп ′а̄раkатшин.</ta>
            <ta e="T23" id="Seg_244" s="T19">а тан ′тӧрджин ′серга.</ta>
            <ta e="T25" id="Seg_245" s="T23">теп коцʼи′ва̄тшын.</ta>
            <ta e="T29" id="Seg_246" s="T25">тас′ты а′раказʼе ′ӓ̄ртшибылʼе ′ӱбъреджан.</ta>
            <ta e="T33" id="Seg_247" s="T29">тан уш kай′нӓ ′кеткет.</ta>
            <ta e="T36" id="Seg_248" s="T33">ӱт′тшал и ′kwанныk.</ta>
            <ta e="T39" id="Seg_249" s="T36">теп ′нараkа ′д̂ӧщоwан.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_250" s="T1">täbəɣum tšaʒɨn.</ta>
            <ta e="T9" id="Seg_251" s="T3">a man tebne tʼaran: serga mattə.</ta>
            <ta e="T14" id="Seg_252" s="T9">serga tattə sütdinäj mattə serga.</ta>
            <ta e="T17" id="Seg_253" s="T14">natʼen näjɣum amnɨta.</ta>
            <ta e="T19" id="Seg_254" s="T17">täp aːraqatšin.</ta>
            <ta e="T23" id="Seg_255" s="T19">a tan törǯin serga.</ta>
            <ta e="T25" id="Seg_256" s="T23">tep kocʼiwaːtšɨn.</ta>
            <ta e="T29" id="Seg_257" s="T25">tastɨ arakazʼe äːrtšibɨlʼe übəreǯan.</ta>
            <ta e="T33" id="Seg_258" s="T29">tan uš qajnä ketket.</ta>
            <ta e="T36" id="Seg_259" s="T33">üttšal i qwannɨq.</ta>
            <ta e="T39" id="Seg_260" s="T36">tep naraqa d̂öšowan.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_261" s="T1">Täbəɣum čaʒɨn. </ta>
            <ta e="T9" id="Seg_262" s="T3">A man tebne tʼaran: “Serga mattə. </ta>
            <ta e="T14" id="Seg_263" s="T9">Serga tattə sütdinäj mattə serga. </ta>
            <ta e="T17" id="Seg_264" s="T14">Natʼen näjɣum amnɨta. </ta>
            <ta e="T19" id="Seg_265" s="T17">Täp aːraqačin. </ta>
            <ta e="T23" id="Seg_266" s="T19">A tan törǯin serga. </ta>
            <ta e="T25" id="Seg_267" s="T23">Tep kocʼiwaːčɨn. </ta>
            <ta e="T29" id="Seg_268" s="T25">Tastɨ arakazʼe äːrčibɨlʼe übəreǯan. </ta>
            <ta e="T33" id="Seg_269" s="T29">Tan uš qajnä ketket. </ta>
            <ta e="T36" id="Seg_270" s="T33">Ütčal i qwannɨq. </ta>
            <ta e="T39" id="Seg_271" s="T36">Tep naraqa döšowan.” </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_272" s="T1">täbə-ɣum</ta>
            <ta e="T3" id="Seg_273" s="T2">čaʒɨ-n</ta>
            <ta e="T4" id="Seg_274" s="T3">a</ta>
            <ta e="T5" id="Seg_275" s="T4">man</ta>
            <ta e="T6" id="Seg_276" s="T5">teb-ne</ta>
            <ta e="T7" id="Seg_277" s="T6">tʼara-n</ta>
            <ta e="T8" id="Seg_278" s="T7">ser-ga</ta>
            <ta e="T9" id="Seg_279" s="T8">mat-tə</ta>
            <ta e="T10" id="Seg_280" s="T9">ser-ga</ta>
            <ta e="T11" id="Seg_281" s="T10">tattə</ta>
            <ta e="T12" id="Seg_282" s="T11">sütdi-näj</ta>
            <ta e="T13" id="Seg_283" s="T12">mat-tə</ta>
            <ta e="T14" id="Seg_284" s="T13">ser-ga</ta>
            <ta e="T15" id="Seg_285" s="T14">natʼe-n</ta>
            <ta e="T16" id="Seg_286" s="T15">nä-j-ɣum</ta>
            <ta e="T17" id="Seg_287" s="T16">amnɨ-ta</ta>
            <ta e="T18" id="Seg_288" s="T17">täp</ta>
            <ta e="T19" id="Seg_289" s="T18">aːraqa-či-n</ta>
            <ta e="T20" id="Seg_290" s="T19">a</ta>
            <ta e="T21" id="Seg_291" s="T20">tat</ta>
            <ta e="T22" id="Seg_292" s="T21">törǯin</ta>
            <ta e="T23" id="Seg_293" s="T22">ser-ga</ta>
            <ta e="T24" id="Seg_294" s="T23">tep</ta>
            <ta e="T25" id="Seg_295" s="T24">kocʼiwaː-čɨ-n</ta>
            <ta e="T26" id="Seg_296" s="T25">tastɨ</ta>
            <ta e="T27" id="Seg_297" s="T26">araka-zʼe</ta>
            <ta e="T28" id="Seg_298" s="T27">äːr-či-bɨ-lʼe</ta>
            <ta e="T29" id="Seg_299" s="T28">übə-r-eǯa-n</ta>
            <ta e="T30" id="Seg_300" s="T29">tat</ta>
            <ta e="T31" id="Seg_301" s="T30">uš</ta>
            <ta e="T32" id="Seg_302" s="T31">qaj-nä</ta>
            <ta e="T33" id="Seg_303" s="T32">ket-k-et</ta>
            <ta e="T34" id="Seg_304" s="T33">üt-ča-l</ta>
            <ta e="T35" id="Seg_305" s="T34">i</ta>
            <ta e="T36" id="Seg_306" s="T35">qwan-nɨ-q</ta>
            <ta e="T37" id="Seg_307" s="T36">tep</ta>
            <ta e="T38" id="Seg_308" s="T37">naraqa</ta>
            <ta e="T39" id="Seg_309" s="T38">döšowa-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_310" s="T1">täbe-qum</ta>
            <ta e="T3" id="Seg_311" s="T2">čaǯɨ-n</ta>
            <ta e="T4" id="Seg_312" s="T3">a</ta>
            <ta e="T5" id="Seg_313" s="T4">man</ta>
            <ta e="T6" id="Seg_314" s="T5">täp-nä</ta>
            <ta e="T7" id="Seg_315" s="T6">tʼärɨ-ŋ</ta>
            <ta e="T8" id="Seg_316" s="T7">ser-kɨ</ta>
            <ta e="T9" id="Seg_317" s="T8">maːt-ntə</ta>
            <ta e="T10" id="Seg_318" s="T9">ser-kɨ</ta>
            <ta e="T11" id="Seg_319" s="T10">tattə</ta>
            <ta e="T12" id="Seg_320" s="T11">sʼütdʼe-näj</ta>
            <ta e="T13" id="Seg_321" s="T12">maːt-ntə</ta>
            <ta e="T14" id="Seg_322" s="T13">ser-kɨ</ta>
            <ta e="T15" id="Seg_323" s="T14">*natʼe-n</ta>
            <ta e="T16" id="Seg_324" s="T15">ne-lʼ-qum</ta>
            <ta e="T17" id="Seg_325" s="T16">amdɨ-ntɨ</ta>
            <ta e="T18" id="Seg_326" s="T17">täp</ta>
            <ta e="T19" id="Seg_327" s="T18">araŋka-ču-n</ta>
            <ta e="T20" id="Seg_328" s="T19">a</ta>
            <ta e="T21" id="Seg_329" s="T20">tan</ta>
            <ta e="T22" id="Seg_330" s="T21">törǯin</ta>
            <ta e="T23" id="Seg_331" s="T22">ser-kɨ</ta>
            <ta e="T24" id="Seg_332" s="T23">täp</ta>
            <ta e="T25" id="Seg_333" s="T24">kɨcʼwat-či-n</ta>
            <ta e="T26" id="Seg_334" s="T25">tastɨ</ta>
            <ta e="T27" id="Seg_335" s="T26">araŋka-se</ta>
            <ta e="T28" id="Seg_336" s="T27">öru-ču-mbɨ-le</ta>
            <ta e="T29" id="Seg_337" s="T28">übɨ-r-enǯɨ-n</ta>
            <ta e="T30" id="Seg_338" s="T29">tan</ta>
            <ta e="T31" id="Seg_339" s="T30">uʒ</ta>
            <ta e="T32" id="Seg_340" s="T31">qaj-näj</ta>
            <ta e="T33" id="Seg_341" s="T32">ket-ku-etɨ</ta>
            <ta e="T34" id="Seg_342" s="T33">üt-enǯɨ-l</ta>
            <ta e="T35" id="Seg_343" s="T34">i</ta>
            <ta e="T36" id="Seg_344" s="T35">qwan-nɨ-kɨ</ta>
            <ta e="T37" id="Seg_345" s="T36">täp</ta>
            <ta e="T38" id="Seg_346" s="T37">naraqa</ta>
            <ta e="T39" id="Seg_347" s="T38">döšowaj-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_348" s="T1">man-human.being.[NOM]</ta>
            <ta e="T3" id="Seg_349" s="T2">go-3SG.S</ta>
            <ta e="T4" id="Seg_350" s="T3">and</ta>
            <ta e="T5" id="Seg_351" s="T4">I.NOM</ta>
            <ta e="T6" id="Seg_352" s="T5">(s)he-ALL</ta>
            <ta e="T7" id="Seg_353" s="T6">say-1SG.S</ta>
            <ta e="T8" id="Seg_354" s="T7">come.in-IMP.2SG.S</ta>
            <ta e="T9" id="Seg_355" s="T8">house-ILL</ta>
            <ta e="T10" id="Seg_356" s="T9">come.in-IMP.2SG.S</ta>
            <ta e="T11" id="Seg_357" s="T10">%%</ta>
            <ta e="T12" id="Seg_358" s="T11">room-EMPH</ta>
            <ta e="T13" id="Seg_359" s="T12">house-ILL</ta>
            <ta e="T14" id="Seg_360" s="T13">come.in-IMP.2SG.S</ta>
            <ta e="T15" id="Seg_361" s="T14">there-ADV.LOC</ta>
            <ta e="T16" id="Seg_362" s="T15">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T17" id="Seg_363" s="T16">sit-INFER.[NOM]</ta>
            <ta e="T18" id="Seg_364" s="T17">(s)he.[NOM]</ta>
            <ta e="T19" id="Seg_365" s="T18">wine-VBLZ-3SG.S</ta>
            <ta e="T20" id="Seg_366" s="T19">and</ta>
            <ta e="T21" id="Seg_367" s="T20">you.SG.NOM</ta>
            <ta e="T22" id="Seg_368" s="T21">%slowly</ta>
            <ta e="T23" id="Seg_369" s="T22">come.in-IMP.2SG.S</ta>
            <ta e="T24" id="Seg_370" s="T23">(s)he.[NOM]</ta>
            <ta e="T25" id="Seg_371" s="T24">get.scared-RFL-3SG.S</ta>
            <ta e="T26" id="Seg_372" s="T25">you.SG.ACC</ta>
            <ta e="T27" id="Seg_373" s="T26">wine-COM</ta>
            <ta e="T28" id="Seg_374" s="T27">drink-TR-DUR-CVB</ta>
            <ta e="T29" id="Seg_375" s="T28">begin-DRV-FUT-3SG.S</ta>
            <ta e="T30" id="Seg_376" s="T29">you.SG.NOM</ta>
            <ta e="T31" id="Seg_377" s="T30">already</ta>
            <ta e="T32" id="Seg_378" s="T31">what.[NOM]-EMPH</ta>
            <ta e="T33" id="Seg_379" s="T32">say-HAB-IMP.2SG.O</ta>
            <ta e="T34" id="Seg_380" s="T33">drink-FUT-2SG.O</ta>
            <ta e="T35" id="Seg_381" s="T34">and</ta>
            <ta e="T36" id="Seg_382" s="T35">leave-CO-IMP.2SG.S</ta>
            <ta e="T37" id="Seg_383" s="T36">(s)he.[NOM]</ta>
            <ta e="T38" id="Seg_384" s="T37">%%</ta>
            <ta e="T39" id="Seg_385" s="T38">cheap-ADVZ</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_386" s="T1">мужчина-человек.[NOM]</ta>
            <ta e="T3" id="Seg_387" s="T2">ходить-3SG.S</ta>
            <ta e="T4" id="Seg_388" s="T3">а</ta>
            <ta e="T5" id="Seg_389" s="T4">я.NOM</ta>
            <ta e="T6" id="Seg_390" s="T5">он(а)-ALL</ta>
            <ta e="T7" id="Seg_391" s="T6">сказать-1SG.S</ta>
            <ta e="T8" id="Seg_392" s="T7">зайти-IMP.2SG.S</ta>
            <ta e="T9" id="Seg_393" s="T8">дом-ILL</ta>
            <ta e="T10" id="Seg_394" s="T9">зайти-IMP.2SG.S</ta>
            <ta e="T11" id="Seg_395" s="T10">%%</ta>
            <ta e="T12" id="Seg_396" s="T11">комната-EMPH</ta>
            <ta e="T13" id="Seg_397" s="T12">дом-ILL</ta>
            <ta e="T14" id="Seg_398" s="T13">зайти-IMP.2SG.S</ta>
            <ta e="T15" id="Seg_399" s="T14">туда-ADV.LOC</ta>
            <ta e="T16" id="Seg_400" s="T15">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T17" id="Seg_401" s="T16">сидеть-INFER.[NOM]</ta>
            <ta e="T18" id="Seg_402" s="T17">он(а).[NOM]</ta>
            <ta e="T19" id="Seg_403" s="T18">вино-VBLZ-3SG.S</ta>
            <ta e="T20" id="Seg_404" s="T19">а</ta>
            <ta e="T21" id="Seg_405" s="T20">ты.NOM</ta>
            <ta e="T22" id="Seg_406" s="T21">%медленно</ta>
            <ta e="T23" id="Seg_407" s="T22">зайти-IMP.2SG.S</ta>
            <ta e="T24" id="Seg_408" s="T23">он(а).[NOM]</ta>
            <ta e="T25" id="Seg_409" s="T24">испугаться-RFL-3SG.S</ta>
            <ta e="T26" id="Seg_410" s="T25">ты.ACC</ta>
            <ta e="T27" id="Seg_411" s="T26">вино-COM</ta>
            <ta e="T28" id="Seg_412" s="T27">пить-TR-DUR-CVB</ta>
            <ta e="T29" id="Seg_413" s="T28">начать-DRV-FUT-3SG.S</ta>
            <ta e="T30" id="Seg_414" s="T29">ты.NOM</ta>
            <ta e="T31" id="Seg_415" s="T30">уже</ta>
            <ta e="T32" id="Seg_416" s="T31">что.[NOM]-EMPH</ta>
            <ta e="T33" id="Seg_417" s="T32">сказать-HAB-IMP.2SG.O</ta>
            <ta e="T34" id="Seg_418" s="T33">пить-FUT-2SG.O</ta>
            <ta e="T35" id="Seg_419" s="T34">и</ta>
            <ta e="T36" id="Seg_420" s="T35">отправиться-CO-IMP.2SG.S</ta>
            <ta e="T37" id="Seg_421" s="T36">он(а).[NOM]</ta>
            <ta e="T38" id="Seg_422" s="T37">%%</ta>
            <ta e="T39" id="Seg_423" s="T38">дешевый-ADVZ</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_424" s="T1">n-n.[n:case]</ta>
            <ta e="T3" id="Seg_425" s="T2">v-v:pn</ta>
            <ta e="T4" id="Seg_426" s="T3">conj</ta>
            <ta e="T5" id="Seg_427" s="T4">pers</ta>
            <ta e="T6" id="Seg_428" s="T5">pers-n:case</ta>
            <ta e="T7" id="Seg_429" s="T6">v-v:pn</ta>
            <ta e="T8" id="Seg_430" s="T7">v-v:mood.pn</ta>
            <ta e="T9" id="Seg_431" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_432" s="T9">v-v:mood.pn</ta>
            <ta e="T11" id="Seg_433" s="T10">%%</ta>
            <ta e="T12" id="Seg_434" s="T11">n-clit</ta>
            <ta e="T13" id="Seg_435" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_436" s="T13">v-v:mood.pn</ta>
            <ta e="T15" id="Seg_437" s="T14">adv-adv:case</ta>
            <ta e="T16" id="Seg_438" s="T15">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T17" id="Seg_439" s="T16">v-v:mood.[n:case]</ta>
            <ta e="T18" id="Seg_440" s="T17">pers.[n:case]</ta>
            <ta e="T19" id="Seg_441" s="T18">n-n&gt;v-v:pn</ta>
            <ta e="T20" id="Seg_442" s="T19">conj</ta>
            <ta e="T21" id="Seg_443" s="T20">pers</ta>
            <ta e="T22" id="Seg_444" s="T21">adv</ta>
            <ta e="T23" id="Seg_445" s="T22">v-v:mood.pn</ta>
            <ta e="T24" id="Seg_446" s="T23">pers.[n:case]</ta>
            <ta e="T25" id="Seg_447" s="T24">v-v&gt;v-v:pn</ta>
            <ta e="T26" id="Seg_448" s="T25">pers</ta>
            <ta e="T27" id="Seg_449" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_450" s="T27">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T29" id="Seg_451" s="T28">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_452" s="T29">pers</ta>
            <ta e="T31" id="Seg_453" s="T30">adv</ta>
            <ta e="T32" id="Seg_454" s="T31">interrog.[n:case]-clit</ta>
            <ta e="T33" id="Seg_455" s="T32">v-v&gt;v-v:mood.pn</ta>
            <ta e="T34" id="Seg_456" s="T33">v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_457" s="T34">conj</ta>
            <ta e="T36" id="Seg_458" s="T35">v-v:ins-v:mood.pn</ta>
            <ta e="T37" id="Seg_459" s="T36">pers.[n:case]</ta>
            <ta e="T38" id="Seg_460" s="T37">%%</ta>
            <ta e="T39" id="Seg_461" s="T38">adj-adj&gt;adv</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_462" s="T1">n</ta>
            <ta e="T3" id="Seg_463" s="T2">v</ta>
            <ta e="T4" id="Seg_464" s="T3">conj</ta>
            <ta e="T5" id="Seg_465" s="T4">pers</ta>
            <ta e="T6" id="Seg_466" s="T5">pers</ta>
            <ta e="T7" id="Seg_467" s="T6">v</ta>
            <ta e="T8" id="Seg_468" s="T7">v</ta>
            <ta e="T9" id="Seg_469" s="T8">n</ta>
            <ta e="T10" id="Seg_470" s="T9">v</ta>
            <ta e="T12" id="Seg_471" s="T11">n</ta>
            <ta e="T13" id="Seg_472" s="T12">n</ta>
            <ta e="T14" id="Seg_473" s="T13">v</ta>
            <ta e="T15" id="Seg_474" s="T14">adv</ta>
            <ta e="T16" id="Seg_475" s="T15">n</ta>
            <ta e="T17" id="Seg_476" s="T16">v</ta>
            <ta e="T18" id="Seg_477" s="T17">pers</ta>
            <ta e="T19" id="Seg_478" s="T18">v</ta>
            <ta e="T20" id="Seg_479" s="T19">conj</ta>
            <ta e="T21" id="Seg_480" s="T20">pers</ta>
            <ta e="T22" id="Seg_481" s="T21">adv</ta>
            <ta e="T23" id="Seg_482" s="T22">v</ta>
            <ta e="T24" id="Seg_483" s="T23">pers</ta>
            <ta e="T25" id="Seg_484" s="T24">v</ta>
            <ta e="T26" id="Seg_485" s="T25">pers</ta>
            <ta e="T27" id="Seg_486" s="T26">n</ta>
            <ta e="T28" id="Seg_487" s="T27">adv</ta>
            <ta e="T29" id="Seg_488" s="T28">v</ta>
            <ta e="T30" id="Seg_489" s="T29">pers</ta>
            <ta e="T31" id="Seg_490" s="T30">adv</ta>
            <ta e="T32" id="Seg_491" s="T31">pro</ta>
            <ta e="T33" id="Seg_492" s="T32">v</ta>
            <ta e="T34" id="Seg_493" s="T33">v</ta>
            <ta e="T35" id="Seg_494" s="T34">conj</ta>
            <ta e="T36" id="Seg_495" s="T35">v</ta>
            <ta e="T37" id="Seg_496" s="T36">pers</ta>
            <ta e="T39" id="Seg_497" s="T38">adv</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_498" s="T1">np.h:A</ta>
            <ta e="T5" id="Seg_499" s="T4">pro.h:A</ta>
            <ta e="T6" id="Seg_500" s="T5">pro.h:R</ta>
            <ta e="T8" id="Seg_501" s="T7">0.2.h:A</ta>
            <ta e="T9" id="Seg_502" s="T8">np:G</ta>
            <ta e="T10" id="Seg_503" s="T9">0.2.h:A</ta>
            <ta e="T13" id="Seg_504" s="T12">np:G</ta>
            <ta e="T14" id="Seg_505" s="T13">0.2.h:A</ta>
            <ta e="T15" id="Seg_506" s="T14">adv:L</ta>
            <ta e="T16" id="Seg_507" s="T15">np.h:Th</ta>
            <ta e="T18" id="Seg_508" s="T17">pro.h:A</ta>
            <ta e="T21" id="Seg_509" s="T20">pro.h:A</ta>
            <ta e="T24" id="Seg_510" s="T23">pro.h:E</ta>
            <ta e="T26" id="Seg_511" s="T25">pro.h:B</ta>
            <ta e="T27" id="Seg_512" s="T26">np:Th</ta>
            <ta e="T29" id="Seg_513" s="T28">0.3.h:A</ta>
            <ta e="T30" id="Seg_514" s="T29">pro.h:A</ta>
            <ta e="T32" id="Seg_515" s="T31">pro:Th</ta>
            <ta e="T34" id="Seg_516" s="T33">0.2.h:A</ta>
            <ta e="T36" id="Seg_517" s="T35">0.2.h:A</ta>
            <ta e="T37" id="Seg_518" s="T36">pro:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_519" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_520" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_521" s="T4">pro.h:S</ta>
            <ta e="T7" id="Seg_522" s="T6">v:pred</ta>
            <ta e="T8" id="Seg_523" s="T7">0.2.h:S v:pred</ta>
            <ta e="T10" id="Seg_524" s="T9">0.2.h:S v:pred</ta>
            <ta e="T14" id="Seg_525" s="T13">0.2.h:S v:pred</ta>
            <ta e="T16" id="Seg_526" s="T15">np.h:S</ta>
            <ta e="T17" id="Seg_527" s="T16">v:pred</ta>
            <ta e="T18" id="Seg_528" s="T17">pro.h:S</ta>
            <ta e="T19" id="Seg_529" s="T18">v:pred</ta>
            <ta e="T21" id="Seg_530" s="T20">pro.h:S</ta>
            <ta e="T23" id="Seg_531" s="T22">v:pred</ta>
            <ta e="T24" id="Seg_532" s="T23">pro.h:S</ta>
            <ta e="T25" id="Seg_533" s="T24">v:pred</ta>
            <ta e="T26" id="Seg_534" s="T25">pro.h:O</ta>
            <ta e="T29" id="Seg_535" s="T28">0.3.h:S v:pred</ta>
            <ta e="T30" id="Seg_536" s="T29">pro.h:S</ta>
            <ta e="T32" id="Seg_537" s="T31">pro:O</ta>
            <ta e="T33" id="Seg_538" s="T32">v:pred</ta>
            <ta e="T34" id="Seg_539" s="T33">0.2.h:S v:pred</ta>
            <ta e="T36" id="Seg_540" s="T35">0.2.h:S v:pred</ta>
            <ta e="T37" id="Seg_541" s="T36">pro:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_542" s="T3">RUS:gram</ta>
            <ta e="T19" id="Seg_543" s="T18">TURK:cult</ta>
            <ta e="T20" id="Seg_544" s="T19">RUS:gram</ta>
            <ta e="T27" id="Seg_545" s="T26">TURK:cult</ta>
            <ta e="T31" id="Seg_546" s="T30">RUS:disc</ta>
            <ta e="T35" id="Seg_547" s="T34">RUS:gram</ta>
            <ta e="T39" id="Seg_548" s="T38">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_549" s="T1">A man came.</ta>
            <ta e="T9" id="Seg_550" s="T3">I said to him: “Come in.</ta>
            <ta e="T14" id="Seg_551" s="T9">Come in, come into the room.</ta>
            <ta e="T17" id="Seg_552" s="T14">There sits a woman.</ta>
            <ta e="T19" id="Seg_553" s="T17">She is making moonshine.</ta>
            <ta e="T23" id="Seg_554" s="T19">Enter slowly.</ta>
            <ta e="T25" id="Seg_555" s="T23">She'll get afraid.</ta>
            <ta e="T29" id="Seg_556" s="T25">She'll give you moonshine to drink.</ta>
            <ta e="T33" id="Seg_557" s="T29">Don't say anything.</ta>
            <ta e="T36" id="Seg_558" s="T33">Drink and leave.</ta>
            <ta e="T39" id="Seg_559" s="T36">It (moonshine) is (?) cheap.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_560" s="T1">Ein Mann kam.</ta>
            <ta e="T9" id="Seg_561" s="T3">Ich sagte ihm: "Komm herein.</ta>
            <ta e="T14" id="Seg_562" s="T9">Komm herein, komm ins Zimmer.</ta>
            <ta e="T17" id="Seg_563" s="T14">Dort sitzt eine Frau.</ta>
            <ta e="T19" id="Seg_564" s="T17">Sie macht Wein.</ta>
            <ta e="T23" id="Seg_565" s="T19">Tritt langsam ein.</ta>
            <ta e="T25" id="Seg_566" s="T23">Sie kriegt Angst.</ta>
            <ta e="T29" id="Seg_567" s="T25">Sie wird dir Wein zu trinken geben.</ta>
            <ta e="T33" id="Seg_568" s="T29">Sag nichts.</ta>
            <ta e="T36" id="Seg_569" s="T33">Trink und geh.</ta>
            <ta e="T39" id="Seg_570" s="T36">Er (der Wein) ist billig.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_571" s="T1">Мужчина пришел.</ta>
            <ta e="T9" id="Seg_572" s="T3">А я ему сказала: “Иди в дом.</ta>
            <ta e="T14" id="Seg_573" s="T9">Заходи, заходи внутрь комнаты(?).</ta>
            <ta e="T17" id="Seg_574" s="T14">А там женщина сидит.</ta>
            <ta e="T19" id="Seg_575" s="T17">Она вино гонит.</ta>
            <ta e="T23" id="Seg_576" s="T19">А ты помаленьку заходи.</ta>
            <ta e="T25" id="Seg_577" s="T23">Она испугается.</ta>
            <ta e="T29" id="Seg_578" s="T25">Тебя она вином поить начнет.</ta>
            <ta e="T33" id="Seg_579" s="T29">Ты уж ничего не говори.</ta>
            <ta e="T36" id="Seg_580" s="T33">Выпьешь и иди.</ta>
            <ta e="T39" id="Seg_581" s="T36">Оно (вино) (обходится)? дешево”.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_582" s="T1">мужчина пришел</ta>
            <ta e="T9" id="Seg_583" s="T3">а я ему сказала иди в комнату</ta>
            <ta e="T14" id="Seg_584" s="T9">заходи, заходи внутрь комнаты</ta>
            <ta e="T17" id="Seg_585" s="T14">а там женщина сидит</ta>
            <ta e="T19" id="Seg_586" s="T17">она вино гонит ′а̄раkаттшугу - гнать вино</ta>
            <ta e="T23" id="Seg_587" s="T19">ты помаленьку заходи</ta>
            <ta e="T25" id="Seg_588" s="T23">она испугается</ta>
            <ta e="T29" id="Seg_589" s="T25">тебя она тогда поить начнет</ta>
            <ta e="T33" id="Seg_590" s="T29">ты уж ничего не говори (не доказывай)</ta>
            <ta e="T36" id="Seg_591" s="T33">выпьешь и иди</ta>
            <ta e="T39" id="Seg_592" s="T36">оно (вино) обходится дешево</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T14" id="Seg_593" s="T9">[BrM:] Unclear construction: 'tattə sütdinäj mattə'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
