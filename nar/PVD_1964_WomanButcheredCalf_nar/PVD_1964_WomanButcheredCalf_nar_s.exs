<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_WomanButcheredCalf_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_WomanButcheredCalf_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">82</ud-information>
            <ud-information attribute-name="# HIAT:w">59</ud-information>
            <ud-information attribute-name="# e">59</ud-information>
            <ud-information attribute-name="# HIAT:u">12</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T60" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Oqqɨr</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">näjɣum</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">Kalpašaqɨn</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">maʒɨppat</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">sɨrgamdə</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_20" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">Täp</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">sədəmal</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">maʒəpat</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">wadʼimdə</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">i</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_37" n="HIAT:w" s="T11">nagɨrbat</ts>
                  <nts id="Seg_38" n="HIAT:ip">:</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_41" n="HIAT:w" s="T12">moɣunäːj</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_44" n="HIAT:w" s="T13">častə</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_47" n="HIAT:w" s="T14">mersej</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_50" n="HIAT:w" s="T15">jen</ts>
                  <nts id="Seg_51" n="HIAT:ip">,</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_54" n="HIAT:w" s="T16">a</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_57" n="HIAT:w" s="T17">perednʼaj</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_60" n="HIAT:w" s="T18">dʼešewlʼe</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_63" n="HIAT:w" s="T19">jen</ts>
                  <nts id="Seg_64" n="HIAT:ip">.</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_67" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_69" n="HIAT:w" s="T20">I</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_72" n="HIAT:w" s="T21">stuldə</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_75" n="HIAT:w" s="T22">penbat</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_78" n="HIAT:w" s="T23">na</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_81" n="HIAT:w" s="T24">nagɨrɨm</ts>
                  <nts id="Seg_82" n="HIAT:ip">.</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_85" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_87" n="HIAT:w" s="T25">A</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_90" n="HIAT:w" s="T26">padrugat</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_93" n="HIAT:w" s="T27">košädʼin</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_96" n="HIAT:w" s="T28">täbnä</ts>
                  <nts id="Seg_97" n="HIAT:ip">.</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_100" n="HIAT:u" s="T29">
                  <nts id="Seg_101" n="HIAT:ip">“</nts>
                  <ts e="T30" id="Seg_103" n="HIAT:w" s="T29">Qwallaj</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_106" n="HIAT:w" s="T30">tʼän</ts>
                  <nts id="Seg_107" n="HIAT:ip">.</nts>
                  <nts id="Seg_108" n="HIAT:ip">”</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_111" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">Täp</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_116" n="HIAT:w" s="T32">omdädʼippa</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_119" n="HIAT:w" s="T33">stultə</ts>
                  <nts id="Seg_120" n="HIAT:ip">.</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_123" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">Na</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">nagɨr</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_131" n="HIAT:w" s="T36">täbnä</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_134" n="HIAT:w" s="T37">paːrɨdʼippa</ts>
                  <nts id="Seg_135" n="HIAT:ip">.</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_138" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_140" n="HIAT:w" s="T38">Wadʼilam</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_143" n="HIAT:w" s="T39">tʼäbodə</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_146" n="HIAT:w" s="T40">i</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_149" n="HIAT:w" s="T41">kuronnaɣə</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_152" n="HIAT:w" s="T42">bazartə</ts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_156" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_158" n="HIAT:w" s="T43">Milʼicʼianer</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_161" n="HIAT:w" s="T44">täbɨstaɣɨm</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_164" n="HIAT:w" s="T45">udurɨt</ts>
                  <nts id="Seg_165" n="HIAT:ip">.</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_168" n="HIAT:u" s="T46">
                  <nts id="Seg_169" n="HIAT:ip">“</nts>
                  <ts e="T47" id="Seg_171" n="HIAT:w" s="T46">Udurukok</ts>
                  <nts id="Seg_172" n="HIAT:ip">!</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_175" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_177" n="HIAT:w" s="T47">Qaj</ts>
                  <nts id="Seg_178" n="HIAT:ip">,</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_181" n="HIAT:w" s="T48">moɣunäj</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_184" n="HIAT:w" s="T49">čazla</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_187" n="HIAT:w" s="T50">merseː</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_190" n="HIAT:w" s="T51">jen</ts>
                  <nts id="Seg_191" n="HIAT:ip">,</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_194" n="HIAT:w" s="T52">a</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_197" n="HIAT:w" s="T53">perednʼäja</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_200" n="HIAT:w" s="T54">čast</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_203" n="HIAT:w" s="T55">dʼöšoan</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_206" n="HIAT:w" s="T56">jen</ts>
                  <nts id="Seg_207" n="HIAT:ip">?</nts>
                  <nts id="Seg_208" n="HIAT:ip">”</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_211" n="HIAT:u" s="T57">
                  <nts id="Seg_212" n="HIAT:ip">“</nts>
                  <ts e="T58" id="Seg_214" n="HIAT:w" s="T57">Qaj</ts>
                  <nts id="Seg_215" n="HIAT:ip">,</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_218" n="HIAT:w" s="T58">obəjawlenie</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_221" n="HIAT:w" s="T59">ädämɨtdau</ts>
                  <nts id="Seg_222" n="HIAT:ip">.</nts>
                  <nts id="Seg_223" n="HIAT:ip">”</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T60" id="Seg_225" n="sc" s="T1">
               <ts e="T2" id="Seg_227" n="e" s="T1">Oqqɨr </ts>
               <ts e="T3" id="Seg_229" n="e" s="T2">näjɣum </ts>
               <ts e="T4" id="Seg_231" n="e" s="T3">Kalpašaqɨn </ts>
               <ts e="T5" id="Seg_233" n="e" s="T4">maʒɨppat </ts>
               <ts e="T6" id="Seg_235" n="e" s="T5">sɨrgamdə. </ts>
               <ts e="T7" id="Seg_237" n="e" s="T6">Täp </ts>
               <ts e="T8" id="Seg_239" n="e" s="T7">sədəmal </ts>
               <ts e="T9" id="Seg_241" n="e" s="T8">maʒəpat </ts>
               <ts e="T10" id="Seg_243" n="e" s="T9">wadʼimdə </ts>
               <ts e="T11" id="Seg_245" n="e" s="T10">i </ts>
               <ts e="T12" id="Seg_247" n="e" s="T11">nagɨrbat: </ts>
               <ts e="T13" id="Seg_249" n="e" s="T12">moɣunäːj </ts>
               <ts e="T14" id="Seg_251" n="e" s="T13">častə </ts>
               <ts e="T15" id="Seg_253" n="e" s="T14">mersej </ts>
               <ts e="T16" id="Seg_255" n="e" s="T15">jen, </ts>
               <ts e="T17" id="Seg_257" n="e" s="T16">a </ts>
               <ts e="T18" id="Seg_259" n="e" s="T17">perednʼaj </ts>
               <ts e="T19" id="Seg_261" n="e" s="T18">dʼešewlʼe </ts>
               <ts e="T20" id="Seg_263" n="e" s="T19">jen. </ts>
               <ts e="T21" id="Seg_265" n="e" s="T20">I </ts>
               <ts e="T22" id="Seg_267" n="e" s="T21">stuldə </ts>
               <ts e="T23" id="Seg_269" n="e" s="T22">penbat </ts>
               <ts e="T24" id="Seg_271" n="e" s="T23">na </ts>
               <ts e="T25" id="Seg_273" n="e" s="T24">nagɨrɨm. </ts>
               <ts e="T26" id="Seg_275" n="e" s="T25">A </ts>
               <ts e="T27" id="Seg_277" n="e" s="T26">padrugat </ts>
               <ts e="T28" id="Seg_279" n="e" s="T27">košädʼin </ts>
               <ts e="T29" id="Seg_281" n="e" s="T28">täbnä. </ts>
               <ts e="T30" id="Seg_283" n="e" s="T29">“Qwallaj </ts>
               <ts e="T31" id="Seg_285" n="e" s="T30">tʼän.” </ts>
               <ts e="T32" id="Seg_287" n="e" s="T31">Täp </ts>
               <ts e="T33" id="Seg_289" n="e" s="T32">omdädʼippa </ts>
               <ts e="T34" id="Seg_291" n="e" s="T33">stultə. </ts>
               <ts e="T35" id="Seg_293" n="e" s="T34">Na </ts>
               <ts e="T36" id="Seg_295" n="e" s="T35">nagɨr </ts>
               <ts e="T37" id="Seg_297" n="e" s="T36">täbnä </ts>
               <ts e="T38" id="Seg_299" n="e" s="T37">paːrɨdʼippa. </ts>
               <ts e="T39" id="Seg_301" n="e" s="T38">Wadʼilam </ts>
               <ts e="T40" id="Seg_303" n="e" s="T39">tʼäbodə </ts>
               <ts e="T41" id="Seg_305" n="e" s="T40">i </ts>
               <ts e="T42" id="Seg_307" n="e" s="T41">kuronnaɣə </ts>
               <ts e="T43" id="Seg_309" n="e" s="T42">bazartə. </ts>
               <ts e="T44" id="Seg_311" n="e" s="T43">Milʼicʼianer </ts>
               <ts e="T45" id="Seg_313" n="e" s="T44">täbɨstaɣɨm </ts>
               <ts e="T46" id="Seg_315" n="e" s="T45">udurɨt. </ts>
               <ts e="T47" id="Seg_317" n="e" s="T46">“Udurukok! </ts>
               <ts e="T48" id="Seg_319" n="e" s="T47">Qaj, </ts>
               <ts e="T49" id="Seg_321" n="e" s="T48">moɣunäj </ts>
               <ts e="T50" id="Seg_323" n="e" s="T49">čazla </ts>
               <ts e="T51" id="Seg_325" n="e" s="T50">merseː </ts>
               <ts e="T52" id="Seg_327" n="e" s="T51">jen, </ts>
               <ts e="T53" id="Seg_329" n="e" s="T52">a </ts>
               <ts e="T54" id="Seg_331" n="e" s="T53">perednʼäja </ts>
               <ts e="T55" id="Seg_333" n="e" s="T54">čast </ts>
               <ts e="T56" id="Seg_335" n="e" s="T55">dʼöšoan </ts>
               <ts e="T57" id="Seg_337" n="e" s="T56">jen?” </ts>
               <ts e="T58" id="Seg_339" n="e" s="T57">“Qaj, </ts>
               <ts e="T59" id="Seg_341" n="e" s="T58">obəjawlenie </ts>
               <ts e="T60" id="Seg_343" n="e" s="T59">ädämɨtdau.” </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_344" s="T1">PVD_1964_WomanButcheredCalf_nar.001 (001.001)</ta>
            <ta e="T20" id="Seg_345" s="T6">PVD_1964_WomanButcheredCalf_nar.002 (001.002)</ta>
            <ta e="T25" id="Seg_346" s="T20">PVD_1964_WomanButcheredCalf_nar.003 (001.003)</ta>
            <ta e="T29" id="Seg_347" s="T25">PVD_1964_WomanButcheredCalf_nar.004 (001.004)</ta>
            <ta e="T31" id="Seg_348" s="T29">PVD_1964_WomanButcheredCalf_nar.005 (001.005)</ta>
            <ta e="T34" id="Seg_349" s="T31">PVD_1964_WomanButcheredCalf_nar.006 (001.006)</ta>
            <ta e="T38" id="Seg_350" s="T34">PVD_1964_WomanButcheredCalf_nar.007 (001.007)</ta>
            <ta e="T43" id="Seg_351" s="T38">PVD_1964_WomanButcheredCalf_nar.008 (001.008)</ta>
            <ta e="T46" id="Seg_352" s="T43">PVD_1964_WomanButcheredCalf_nar.009 (001.009)</ta>
            <ta e="T47" id="Seg_353" s="T46">PVD_1964_WomanButcheredCalf_nar.010 (001.010)</ta>
            <ta e="T57" id="Seg_354" s="T47">PVD_1964_WomanButcheredCalf_nar.011 (001.011)</ta>
            <ta e="T60" id="Seg_355" s="T57">PVD_1964_WomanButcheredCalf_nar.012 (001.012)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_356" s="T1">оkkыр нӓй′ɣум Кал′пашаkын ′мажыппат ′сыргамдъ.</ta>
            <ta e="T20" id="Seg_357" s="T6">тӓп съдъ′мал мажъ′пат ва′дʼимдъ и нагыр′бат: моɣу′нӓ̄й ′частъ мер′сей jен, а пе′реднʼай д̂ʼешевлʼе jен.</ta>
            <ta e="T25" id="Seg_358" s="T20">и ′стулдъ пен′бат на ′нагырым.</ta>
            <ta e="T29" id="Seg_359" s="T25">а пад′ругат ко′шӓдʼин тӓб′нӓ.</ta>
            <ta e="T31" id="Seg_360" s="T29">kwа′ллай тʼӓн.</ta>
            <ta e="T34" id="Seg_361" s="T31">тӓп ом′дӓдʼиппа стултъ.</ta>
            <ta e="T38" id="Seg_362" s="T34">на на′гыр тӓб′нӓ ′па̄рыдʼиппа.</ta>
            <ta e="T43" id="Seg_363" s="T38">вадʼи′лам ′тʼӓбодъ и куро′ннаɣъ ба′зартъ.</ta>
            <ta e="T46" id="Seg_364" s="T43">милʼицʼиа′нер тӓбыс′таɣым ′удурыт.</ta>
            <ta e="T47" id="Seg_365" s="T46">удуру′кок. </ta>
            <ta e="T57" id="Seg_366" s="T47">kай, моɣу′нӓй ′тшазла мер′се̄ jен, а пе′реднʼӓjа част дʼӧ′шоан jен.</ta>
            <ta e="T60" id="Seg_367" s="T57">kай, объявление ′ӓдӓмытдау̹.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_368" s="T1">oqqɨr näjɣum Кalpašaqɨn maʒɨppat sɨrgamdə.</ta>
            <ta e="T20" id="Seg_369" s="T6">täp sədəmal maʒəpat wadʼimdə i nagɨrbat: moɣunäːj častə mersej jen, a perednʼaj d̂ʼešewlʼe jen.</ta>
            <ta e="T25" id="Seg_370" s="T20">i stuldə penbat na nagɨrɨm.</ta>
            <ta e="T29" id="Seg_371" s="T25">a padrugat košädʼin täbnä.</ta>
            <ta e="T31" id="Seg_372" s="T29">qwallaj tʼän.</ta>
            <ta e="T34" id="Seg_373" s="T31">täp omdädʼippa stultə.</ta>
            <ta e="T38" id="Seg_374" s="T34">na nagɨr täbnä paːrɨdʼippa.</ta>
            <ta e="T43" id="Seg_375" s="T38">wadʼilam tʼäbodə i kuronnaɣə bazartə.</ta>
            <ta e="T46" id="Seg_376" s="T43">milʼicʼianer täbɨstaɣɨm udurɨt.</ta>
            <ta e="T47" id="Seg_377" s="T46">udurukok. </ta>
            <ta e="T57" id="Seg_378" s="T47">qaj, moɣunäj tšazla merseː jen, a perednʼäja čast dʼöšoan jen.</ta>
            <ta e="T60" id="Seg_379" s="T57">qaj, obəjawlenie ädämɨtdau̹.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_380" s="T1">Oqqɨr näjɣum Kalpašaqɨn maʒɨppat sɨrgamdə. </ta>
            <ta e="T20" id="Seg_381" s="T6">Täp sədəmal maʒəpat wadʼimdə i nagɨrbat: moɣunäːj častə mersej jen, a perednʼaj dʼešewlʼe jen. </ta>
            <ta e="T25" id="Seg_382" s="T20">I stuldə penbat na nagɨrɨm. </ta>
            <ta e="T29" id="Seg_383" s="T25">A padrugat košädʼin täbnä. </ta>
            <ta e="T31" id="Seg_384" s="T29">“Qwallaj tʼän.” </ta>
            <ta e="T34" id="Seg_385" s="T31">Täp omdädʼippa stultə. </ta>
            <ta e="T38" id="Seg_386" s="T34">Na nagɨr täbnä paːrɨdʼippa. </ta>
            <ta e="T43" id="Seg_387" s="T38">Wadʼilam tʼäbodə i kuronnaɣə bazartə. </ta>
            <ta e="T46" id="Seg_388" s="T43">Milʼicʼianer täbɨstaɣɨm udurɨt. </ta>
            <ta e="T47" id="Seg_389" s="T46">“Udurukok! </ta>
            <ta e="T57" id="Seg_390" s="T47">Qaj, moɣunäj čazla merseː jen, a perednʼäja čast dʼöšoan jen?” </ta>
            <ta e="T60" id="Seg_391" s="T57">“Qaj, obəjawlenie ädämɨtdau.” </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_392" s="T1">oqqɨr</ta>
            <ta e="T3" id="Seg_393" s="T2">nä-j-ɣum</ta>
            <ta e="T4" id="Seg_394" s="T3">Kalpaša-qɨn</ta>
            <ta e="T5" id="Seg_395" s="T4">maʒɨ-ppa-t</ta>
            <ta e="T6" id="Seg_396" s="T5">sɨr-ga-m-də</ta>
            <ta e="T7" id="Seg_397" s="T6">täp</ta>
            <ta e="T8" id="Seg_398" s="T7">sədə-mal</ta>
            <ta e="T9" id="Seg_399" s="T8">maʒə-pa-t</ta>
            <ta e="T10" id="Seg_400" s="T9">wadʼi-m-də</ta>
            <ta e="T11" id="Seg_401" s="T10">i</ta>
            <ta e="T12" id="Seg_402" s="T11">nagɨr-ba-t</ta>
            <ta e="T13" id="Seg_403" s="T12">moɣunäː-j</ta>
            <ta e="T14" id="Seg_404" s="T13">častə</ta>
            <ta e="T15" id="Seg_405" s="T14">mer-se-j</ta>
            <ta e="T16" id="Seg_406" s="T15">je-n</ta>
            <ta e="T17" id="Seg_407" s="T16">a</ta>
            <ta e="T20" id="Seg_408" s="T19">je-n</ta>
            <ta e="T21" id="Seg_409" s="T20">i</ta>
            <ta e="T22" id="Seg_410" s="T21">stul-də</ta>
            <ta e="T23" id="Seg_411" s="T22">pen-ba-t</ta>
            <ta e="T24" id="Seg_412" s="T23">na</ta>
            <ta e="T25" id="Seg_413" s="T24">nagɨr-ɨ-m</ta>
            <ta e="T26" id="Seg_414" s="T25">a</ta>
            <ta e="T27" id="Seg_415" s="T26">padruga-t</ta>
            <ta e="T28" id="Seg_416" s="T27">košädʼi-n</ta>
            <ta e="T29" id="Seg_417" s="T28">täb-nä</ta>
            <ta e="T30" id="Seg_418" s="T29">qwal-la-j</ta>
            <ta e="T31" id="Seg_419" s="T30">tʼän</ta>
            <ta e="T32" id="Seg_420" s="T31">täp</ta>
            <ta e="T33" id="Seg_421" s="T32">omdä-dʼi-ppa</ta>
            <ta e="T34" id="Seg_422" s="T33">stul-tə</ta>
            <ta e="T35" id="Seg_423" s="T34">na</ta>
            <ta e="T36" id="Seg_424" s="T35">nagɨr</ta>
            <ta e="T37" id="Seg_425" s="T36">täb-nä</ta>
            <ta e="T38" id="Seg_426" s="T37">paːrɨdʼi-ppa</ta>
            <ta e="T39" id="Seg_427" s="T38">wadʼi-la-m</ta>
            <ta e="T40" id="Seg_428" s="T39">tʼäbo-də</ta>
            <ta e="T41" id="Seg_429" s="T40">i</ta>
            <ta e="T42" id="Seg_430" s="T41">kur-on-na-ɣə</ta>
            <ta e="T43" id="Seg_431" s="T42">bazar-tə</ta>
            <ta e="T44" id="Seg_432" s="T43">milʼicʼianer</ta>
            <ta e="T45" id="Seg_433" s="T44">täb-ɨ-staɣɨ-m</ta>
            <ta e="T46" id="Seg_434" s="T45">udur-ɨ-t</ta>
            <ta e="T47" id="Seg_435" s="T46">udur-u-kok</ta>
            <ta e="T48" id="Seg_436" s="T47">qaj</ta>
            <ta e="T49" id="Seg_437" s="T48">moɣunä-j</ta>
            <ta e="T50" id="Seg_438" s="T49">čaz-la</ta>
            <ta e="T51" id="Seg_439" s="T50">mer-seː</ta>
            <ta e="T52" id="Seg_440" s="T51">je-n</ta>
            <ta e="T53" id="Seg_441" s="T52">a</ta>
            <ta e="T56" id="Seg_442" s="T55">dʼöšoa-n</ta>
            <ta e="T57" id="Seg_443" s="T56">je-n</ta>
            <ta e="T58" id="Seg_444" s="T57">qaj</ta>
            <ta e="T59" id="Seg_445" s="T58">obəjawlenie</ta>
            <ta e="T60" id="Seg_446" s="T59">ädä-mɨ-tda-u</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_447" s="T1">okkɨr</ta>
            <ta e="T3" id="Seg_448" s="T2">ne-lʼ-qum</ta>
            <ta e="T4" id="Seg_449" s="T3">Kolpaše-qɨn</ta>
            <ta e="T5" id="Seg_450" s="T4">maǯə-mbɨ-t</ta>
            <ta e="T6" id="Seg_451" s="T5">sɨr-ka-m-tə</ta>
            <ta e="T7" id="Seg_452" s="T6">täp</ta>
            <ta e="T8" id="Seg_453" s="T7">sədə-mal</ta>
            <ta e="T9" id="Seg_454" s="T8">maǯə-mbɨ-t</ta>
            <ta e="T10" id="Seg_455" s="T9">wadʼi-m-tə</ta>
            <ta e="T11" id="Seg_456" s="T10">i</ta>
            <ta e="T12" id="Seg_457" s="T11">nagər-mbɨ-t</ta>
            <ta e="T13" id="Seg_458" s="T12">moqɨnä-lʼ</ta>
            <ta e="T14" id="Seg_459" s="T13">častə</ta>
            <ta e="T15" id="Seg_460" s="T14">mer-se-lʼ</ta>
            <ta e="T16" id="Seg_461" s="T15">eː-n</ta>
            <ta e="T17" id="Seg_462" s="T16">a</ta>
            <ta e="T20" id="Seg_463" s="T19">eː-n</ta>
            <ta e="T21" id="Seg_464" s="T20">i</ta>
            <ta e="T22" id="Seg_465" s="T21">stul-ntə</ta>
            <ta e="T23" id="Seg_466" s="T22">pen-mbɨ-t</ta>
            <ta e="T24" id="Seg_467" s="T23">na</ta>
            <ta e="T25" id="Seg_468" s="T24">nagər-ɨ-m</ta>
            <ta e="T26" id="Seg_469" s="T25">a</ta>
            <ta e="T27" id="Seg_470" s="T26">padruga-tə</ta>
            <ta e="T28" id="Seg_471" s="T27">košädʼi-n</ta>
            <ta e="T29" id="Seg_472" s="T28">täp-nä</ta>
            <ta e="T30" id="Seg_473" s="T29">qwan-lä-j</ta>
            <ta e="T31" id="Seg_474" s="T30">tʼaŋ</ta>
            <ta e="T32" id="Seg_475" s="T31">täp</ta>
            <ta e="T33" id="Seg_476" s="T32">omdɨ-dʼi-mbɨ</ta>
            <ta e="T34" id="Seg_477" s="T33">stul-ntə</ta>
            <ta e="T35" id="Seg_478" s="T34">na</ta>
            <ta e="T36" id="Seg_479" s="T35">nagər</ta>
            <ta e="T37" id="Seg_480" s="T36">täp-nä</ta>
            <ta e="T38" id="Seg_481" s="T37">parɨdi-mbɨ</ta>
            <ta e="T39" id="Seg_482" s="T38">wadʼi-la-m</ta>
            <ta e="T40" id="Seg_483" s="T39">tʼabɨ-di</ta>
            <ta e="T41" id="Seg_484" s="T40">i</ta>
            <ta e="T42" id="Seg_485" s="T41">kur-ol-nɨ-qij</ta>
            <ta e="T43" id="Seg_486" s="T42">pazar-ntə</ta>
            <ta e="T44" id="Seg_487" s="T43">milʼicʼianer</ta>
            <ta e="T45" id="Seg_488" s="T44">täp-ɨ-staɣɨ-m</ta>
            <ta e="T46" id="Seg_489" s="T45">udɨr-ɨ-t</ta>
            <ta e="T47" id="Seg_490" s="T46">udɨr-ɨ-kok</ta>
            <ta e="T48" id="Seg_491" s="T47">qaj</ta>
            <ta e="T49" id="Seg_492" s="T48">moqɨnä-lʼ</ta>
            <ta e="T50" id="Seg_493" s="T49">častə-la</ta>
            <ta e="T51" id="Seg_494" s="T50">mer-se</ta>
            <ta e="T52" id="Seg_495" s="T51">eː-n</ta>
            <ta e="T53" id="Seg_496" s="T52">a</ta>
            <ta e="T56" id="Seg_497" s="T55">döšowaj-ŋ</ta>
            <ta e="T57" id="Seg_498" s="T56">eː-n</ta>
            <ta e="T58" id="Seg_499" s="T57">qaj</ta>
            <ta e="T59" id="Seg_500" s="T58">obəjawlenie</ta>
            <ta e="T60" id="Seg_501" s="T59">ödə-mbɨ-ntɨ-w</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_502" s="T1">one</ta>
            <ta e="T3" id="Seg_503" s="T2">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T4" id="Seg_504" s="T3">Kolpashevo-LOC</ta>
            <ta e="T5" id="Seg_505" s="T4">stab-PST.NAR-3SG.O</ta>
            <ta e="T6" id="Seg_506" s="T5">cow-DIM-ACC-3SG</ta>
            <ta e="T7" id="Seg_507" s="T6">(s)he.[NOM]</ta>
            <ta e="T8" id="Seg_508" s="T7">two-separately</ta>
            <ta e="T9" id="Seg_509" s="T8">cut-PST.NAR-3SG.O</ta>
            <ta e="T10" id="Seg_510" s="T9">meat-ACC-3SG</ta>
            <ta e="T11" id="Seg_511" s="T10">and</ta>
            <ta e="T12" id="Seg_512" s="T11">write-PST.NAR-3SG.O</ta>
            <ta e="T13" id="Seg_513" s="T12">back-ADJZ</ta>
            <ta e="T14" id="Seg_514" s="T13">part.[NOM]</ta>
            <ta e="T15" id="Seg_515" s="T14">price-ADJZ-DRV</ta>
            <ta e="T16" id="Seg_516" s="T15">be-3SG.S</ta>
            <ta e="T17" id="Seg_517" s="T16">and</ta>
            <ta e="T20" id="Seg_518" s="T19">be-3SG.S</ta>
            <ta e="T21" id="Seg_519" s="T20">and</ta>
            <ta e="T22" id="Seg_520" s="T21">chair-ILL</ta>
            <ta e="T23" id="Seg_521" s="T22">put-PST.NAR-3SG.O</ta>
            <ta e="T24" id="Seg_522" s="T23">this</ta>
            <ta e="T25" id="Seg_523" s="T24">writing-EP-ACC</ta>
            <ta e="T26" id="Seg_524" s="T25">and</ta>
            <ta e="T27" id="Seg_525" s="T26">female.friend.[NOM]-3SG</ta>
            <ta e="T28" id="Seg_526" s="T27">%come.running-3SG.S</ta>
            <ta e="T29" id="Seg_527" s="T28">(s)he-ALL</ta>
            <ta e="T30" id="Seg_528" s="T29">leave-OPT-1DU</ta>
            <ta e="T31" id="Seg_529" s="T30">fast</ta>
            <ta e="T32" id="Seg_530" s="T31">(s)he.[NOM]</ta>
            <ta e="T33" id="Seg_531" s="T32">sit.down-DRV-PST.NAR.[3SG.S]</ta>
            <ta e="T34" id="Seg_532" s="T33">chair-ILL</ta>
            <ta e="T35" id="Seg_533" s="T34">this</ta>
            <ta e="T36" id="Seg_534" s="T35">writing.[NOM]</ta>
            <ta e="T37" id="Seg_535" s="T36">(s)he-ALL</ta>
            <ta e="T38" id="Seg_536" s="T37">stick-PST.NAR.[3SG.S]</ta>
            <ta e="T39" id="Seg_537" s="T38">meat-PL-ACC</ta>
            <ta e="T40" id="Seg_538" s="T39">seize-3DU.O</ta>
            <ta e="T41" id="Seg_539" s="T40">and</ta>
            <ta e="T42" id="Seg_540" s="T41">run-MOM-CO-3DU.S</ta>
            <ta e="T43" id="Seg_541" s="T42">market-ILL</ta>
            <ta e="T44" id="Seg_542" s="T43">policeman.[NOM]</ta>
            <ta e="T45" id="Seg_543" s="T44">(s)he-EP-DU-ACC</ta>
            <ta e="T46" id="Seg_544" s="T45">stop-EP-3SG.O</ta>
            <ta e="T47" id="Seg_545" s="T46">stop-EP-%IMP.2DU</ta>
            <ta e="T48" id="Seg_546" s="T47">what</ta>
            <ta e="T49" id="Seg_547" s="T48">back-ADJZ</ta>
            <ta e="T50" id="Seg_548" s="T49">part-PL.[NOM]</ta>
            <ta e="T51" id="Seg_549" s="T50">price-ADJZ</ta>
            <ta e="T52" id="Seg_550" s="T51">be-3SG.S</ta>
            <ta e="T53" id="Seg_551" s="T52">and</ta>
            <ta e="T56" id="Seg_552" s="T55">cheap-ADVZ</ta>
            <ta e="T57" id="Seg_553" s="T56">be-3SG.S</ta>
            <ta e="T58" id="Seg_554" s="T57">what</ta>
            <ta e="T59" id="Seg_555" s="T58">notice.[NOM]</ta>
            <ta e="T60" id="Seg_556" s="T59">hang-PST.NAR-INFER-1SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_557" s="T1">один</ta>
            <ta e="T3" id="Seg_558" s="T2">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T4" id="Seg_559" s="T3">Колпашево-LOC</ta>
            <ta e="T5" id="Seg_560" s="T4">зарезать-PST.NAR-3SG.O</ta>
            <ta e="T6" id="Seg_561" s="T5">корова-DIM-ACC-3SG</ta>
            <ta e="T7" id="Seg_562" s="T6">он(а).[NOM]</ta>
            <ta e="T8" id="Seg_563" s="T7">два-раздельно</ta>
            <ta e="T9" id="Seg_564" s="T8">отрезать-PST.NAR-3SG.O</ta>
            <ta e="T10" id="Seg_565" s="T9">мясо-ACC-3SG</ta>
            <ta e="T11" id="Seg_566" s="T10">и</ta>
            <ta e="T12" id="Seg_567" s="T11">написать-PST.NAR-3SG.O</ta>
            <ta e="T13" id="Seg_568" s="T12">назад-ADJZ</ta>
            <ta e="T14" id="Seg_569" s="T13">часть.[NOM]</ta>
            <ta e="T15" id="Seg_570" s="T14">цена-ADJZ-DRV</ta>
            <ta e="T16" id="Seg_571" s="T15">быть-3SG.S</ta>
            <ta e="T17" id="Seg_572" s="T16">а</ta>
            <ta e="T20" id="Seg_573" s="T19">быть-3SG.S</ta>
            <ta e="T21" id="Seg_574" s="T20">и</ta>
            <ta e="T22" id="Seg_575" s="T21">стул-ILL</ta>
            <ta e="T23" id="Seg_576" s="T22">положить-PST.NAR-3SG.O</ta>
            <ta e="T24" id="Seg_577" s="T23">этот</ta>
            <ta e="T25" id="Seg_578" s="T24">написанное-EP-ACC</ta>
            <ta e="T26" id="Seg_579" s="T25">а</ta>
            <ta e="T27" id="Seg_580" s="T26">подруга.[NOM]-3SG</ta>
            <ta e="T28" id="Seg_581" s="T27">%прибежать-3SG.S</ta>
            <ta e="T29" id="Seg_582" s="T28">он(а)-ALL</ta>
            <ta e="T30" id="Seg_583" s="T29">отправиться-OPT-1DU</ta>
            <ta e="T31" id="Seg_584" s="T30">быстро</ta>
            <ta e="T32" id="Seg_585" s="T31">он(а).[NOM]</ta>
            <ta e="T33" id="Seg_586" s="T32">сесть-DRV-PST.NAR.[3SG.S]</ta>
            <ta e="T34" id="Seg_587" s="T33">стул-ILL</ta>
            <ta e="T35" id="Seg_588" s="T34">этот</ta>
            <ta e="T36" id="Seg_589" s="T35">написанное.[NOM]</ta>
            <ta e="T37" id="Seg_590" s="T36">он(а)-ALL</ta>
            <ta e="T38" id="Seg_591" s="T37">прилипнуть-PST.NAR.[3SG.S]</ta>
            <ta e="T39" id="Seg_592" s="T38">мясо-PL-ACC</ta>
            <ta e="T40" id="Seg_593" s="T39">схватить-3DU.O</ta>
            <ta e="T41" id="Seg_594" s="T40">и</ta>
            <ta e="T42" id="Seg_595" s="T41">бегать-MOM-CO-3DU.S</ta>
            <ta e="T43" id="Seg_596" s="T42">базар-ILL</ta>
            <ta e="T44" id="Seg_597" s="T43">милиционер.[NOM]</ta>
            <ta e="T45" id="Seg_598" s="T44">он(а)-EP-DU-ACC</ta>
            <ta e="T46" id="Seg_599" s="T45">остановить-EP-3SG.O</ta>
            <ta e="T47" id="Seg_600" s="T46">остановиться-EP-%IMP.2DU</ta>
            <ta e="T48" id="Seg_601" s="T47">что</ta>
            <ta e="T49" id="Seg_602" s="T48">назад-ADJZ</ta>
            <ta e="T50" id="Seg_603" s="T49">часть-PL.[NOM]</ta>
            <ta e="T51" id="Seg_604" s="T50">цена-ADJZ</ta>
            <ta e="T52" id="Seg_605" s="T51">быть-3SG.S</ta>
            <ta e="T53" id="Seg_606" s="T52">а</ta>
            <ta e="T56" id="Seg_607" s="T55">дешевый-ADVZ</ta>
            <ta e="T57" id="Seg_608" s="T56">быть-3SG.S</ta>
            <ta e="T58" id="Seg_609" s="T57">что</ta>
            <ta e="T59" id="Seg_610" s="T58">объявление.[NOM]</ta>
            <ta e="T60" id="Seg_611" s="T59">повесить-PST.NAR-INFER-1SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_612" s="T1">num</ta>
            <ta e="T3" id="Seg_613" s="T2">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T4" id="Seg_614" s="T3">nprop-n:case</ta>
            <ta e="T5" id="Seg_615" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_616" s="T5">n-n&gt;n-n:case-n:poss</ta>
            <ta e="T7" id="Seg_617" s="T6">pers.[n:case]</ta>
            <ta e="T8" id="Seg_618" s="T7">num-preverb</ta>
            <ta e="T9" id="Seg_619" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_620" s="T9">n-n:case-n:poss</ta>
            <ta e="T11" id="Seg_621" s="T10">conj</ta>
            <ta e="T12" id="Seg_622" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_623" s="T12">adv-adv&gt;adj</ta>
            <ta e="T14" id="Seg_624" s="T13">n.[n:case]</ta>
            <ta e="T15" id="Seg_625" s="T14">n-n&gt;adj-adj&gt;adj</ta>
            <ta e="T16" id="Seg_626" s="T15">v-v:pn</ta>
            <ta e="T17" id="Seg_627" s="T16">conj</ta>
            <ta e="T20" id="Seg_628" s="T19">v-v:pn</ta>
            <ta e="T21" id="Seg_629" s="T20">conj</ta>
            <ta e="T22" id="Seg_630" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_631" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_632" s="T23">dem</ta>
            <ta e="T25" id="Seg_633" s="T24">n-n:ins-n:case</ta>
            <ta e="T26" id="Seg_634" s="T25">conj</ta>
            <ta e="T27" id="Seg_635" s="T26">n.[n:case]-n:poss</ta>
            <ta e="T28" id="Seg_636" s="T27">v-v:pn</ta>
            <ta e="T29" id="Seg_637" s="T28">pers-n:case</ta>
            <ta e="T30" id="Seg_638" s="T29">v-v:mood-v:pn</ta>
            <ta e="T31" id="Seg_639" s="T30">adv</ta>
            <ta e="T32" id="Seg_640" s="T31">pers.[n:case]</ta>
            <ta e="T33" id="Seg_641" s="T32">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T34" id="Seg_642" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_643" s="T34">dem</ta>
            <ta e="T36" id="Seg_644" s="T35">n.[n:case]</ta>
            <ta e="T37" id="Seg_645" s="T36">pers-n:case</ta>
            <ta e="T38" id="Seg_646" s="T37">v-v:tense.[v:pn]</ta>
            <ta e="T39" id="Seg_647" s="T38">n-n:num-n:case</ta>
            <ta e="T40" id="Seg_648" s="T39">v-v:pn</ta>
            <ta e="T41" id="Seg_649" s="T40">conj</ta>
            <ta e="T42" id="Seg_650" s="T41">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T43" id="Seg_651" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_652" s="T43">n.[n:case]</ta>
            <ta e="T45" id="Seg_653" s="T44">pers-n:ins-n:num-n:case</ta>
            <ta e="T46" id="Seg_654" s="T45">v-v:ins-v:pn</ta>
            <ta e="T47" id="Seg_655" s="T46">v-v:ins-v:mood.pn</ta>
            <ta e="T48" id="Seg_656" s="T47">interrog</ta>
            <ta e="T49" id="Seg_657" s="T48">adv-adv&gt;adj</ta>
            <ta e="T50" id="Seg_658" s="T49">n-n:num.[n:case]</ta>
            <ta e="T51" id="Seg_659" s="T50">n-n&gt;adj</ta>
            <ta e="T52" id="Seg_660" s="T51">v-v:pn</ta>
            <ta e="T53" id="Seg_661" s="T52">conj</ta>
            <ta e="T56" id="Seg_662" s="T55">adj-adj&gt;adv</ta>
            <ta e="T57" id="Seg_663" s="T56">v-v:pn</ta>
            <ta e="T58" id="Seg_664" s="T57">interrog</ta>
            <ta e="T59" id="Seg_665" s="T58">n.[n:case]</ta>
            <ta e="T60" id="Seg_666" s="T59">v-v:tense-v:mood-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_667" s="T1">num</ta>
            <ta e="T3" id="Seg_668" s="T2">n</ta>
            <ta e="T4" id="Seg_669" s="T3">nprop</ta>
            <ta e="T5" id="Seg_670" s="T4">v</ta>
            <ta e="T6" id="Seg_671" s="T5">n</ta>
            <ta e="T7" id="Seg_672" s="T6">pers</ta>
            <ta e="T8" id="Seg_673" s="T7">adv</ta>
            <ta e="T9" id="Seg_674" s="T8">v</ta>
            <ta e="T10" id="Seg_675" s="T9">n</ta>
            <ta e="T11" id="Seg_676" s="T10">conj</ta>
            <ta e="T12" id="Seg_677" s="T11">v</ta>
            <ta e="T13" id="Seg_678" s="T12">adj</ta>
            <ta e="T14" id="Seg_679" s="T13">n</ta>
            <ta e="T15" id="Seg_680" s="T14">adj</ta>
            <ta e="T16" id="Seg_681" s="T15">v</ta>
            <ta e="T17" id="Seg_682" s="T16">conj</ta>
            <ta e="T20" id="Seg_683" s="T19">v</ta>
            <ta e="T21" id="Seg_684" s="T20">conj</ta>
            <ta e="T22" id="Seg_685" s="T21">n</ta>
            <ta e="T23" id="Seg_686" s="T22">v</ta>
            <ta e="T24" id="Seg_687" s="T23">dem</ta>
            <ta e="T25" id="Seg_688" s="T24">n</ta>
            <ta e="T26" id="Seg_689" s="T25">conj</ta>
            <ta e="T27" id="Seg_690" s="T26">n</ta>
            <ta e="T28" id="Seg_691" s="T27">v</ta>
            <ta e="T29" id="Seg_692" s="T28">pers</ta>
            <ta e="T30" id="Seg_693" s="T29">v</ta>
            <ta e="T31" id="Seg_694" s="T30">adv</ta>
            <ta e="T32" id="Seg_695" s="T31">pers</ta>
            <ta e="T33" id="Seg_696" s="T32">v</ta>
            <ta e="T34" id="Seg_697" s="T33">n</ta>
            <ta e="T35" id="Seg_698" s="T34">dem</ta>
            <ta e="T36" id="Seg_699" s="T35">n</ta>
            <ta e="T37" id="Seg_700" s="T36">pers</ta>
            <ta e="T38" id="Seg_701" s="T37">v</ta>
            <ta e="T39" id="Seg_702" s="T38">n</ta>
            <ta e="T40" id="Seg_703" s="T39">v</ta>
            <ta e="T41" id="Seg_704" s="T40">conj</ta>
            <ta e="T42" id="Seg_705" s="T41">v</ta>
            <ta e="T43" id="Seg_706" s="T42">n</ta>
            <ta e="T44" id="Seg_707" s="T43">n</ta>
            <ta e="T45" id="Seg_708" s="T44">pers</ta>
            <ta e="T46" id="Seg_709" s="T45">v</ta>
            <ta e="T47" id="Seg_710" s="T46">v</ta>
            <ta e="T48" id="Seg_711" s="T47">interrog</ta>
            <ta e="T49" id="Seg_712" s="T48">adj</ta>
            <ta e="T50" id="Seg_713" s="T49">n</ta>
            <ta e="T51" id="Seg_714" s="T50">n</ta>
            <ta e="T52" id="Seg_715" s="T51">v</ta>
            <ta e="T53" id="Seg_716" s="T52">conj</ta>
            <ta e="T56" id="Seg_717" s="T55">adv</ta>
            <ta e="T57" id="Seg_718" s="T56">v</ta>
            <ta e="T58" id="Seg_719" s="T57">interrog</ta>
            <ta e="T59" id="Seg_720" s="T58">n</ta>
            <ta e="T60" id="Seg_721" s="T59">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_722" s="T2">np.h:A</ta>
            <ta e="T4" id="Seg_723" s="T3">np:L</ta>
            <ta e="T6" id="Seg_724" s="T5">np:P</ta>
            <ta e="T7" id="Seg_725" s="T6">pro.h:A</ta>
            <ta e="T10" id="Seg_726" s="T9">np:P 0.3:Poss</ta>
            <ta e="T12" id="Seg_727" s="T11">0.3.h:A</ta>
            <ta e="T14" id="Seg_728" s="T13">np:Th</ta>
            <ta e="T22" id="Seg_729" s="T21">np:G</ta>
            <ta e="T23" id="Seg_730" s="T22">0.3.h:A</ta>
            <ta e="T25" id="Seg_731" s="T24">np:Th</ta>
            <ta e="T27" id="Seg_732" s="T26">np.h:A 0.3.h:Poss</ta>
            <ta e="T29" id="Seg_733" s="T28">pro.h:G</ta>
            <ta e="T30" id="Seg_734" s="T29">0.1.h:A</ta>
            <ta e="T32" id="Seg_735" s="T31">pro.h:A</ta>
            <ta e="T34" id="Seg_736" s="T33">np:G</ta>
            <ta e="T36" id="Seg_737" s="T35">np:Th</ta>
            <ta e="T37" id="Seg_738" s="T36">pro.h:G</ta>
            <ta e="T39" id="Seg_739" s="T38">np:Th</ta>
            <ta e="T40" id="Seg_740" s="T39">0.3.h:A</ta>
            <ta e="T42" id="Seg_741" s="T41">0.3.h:A</ta>
            <ta e="T43" id="Seg_742" s="T42">np:G</ta>
            <ta e="T44" id="Seg_743" s="T43">np.h:A</ta>
            <ta e="T45" id="Seg_744" s="T44">pro.h:Th</ta>
            <ta e="T47" id="Seg_745" s="T46">0.2.h:A</ta>
            <ta e="T50" id="Seg_746" s="T49">np:Th</ta>
            <ta e="T59" id="Seg_747" s="T58">np:Th</ta>
            <ta e="T60" id="Seg_748" s="T59">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_749" s="T2">np.h:S</ta>
            <ta e="T5" id="Seg_750" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_751" s="T5">np:O</ta>
            <ta e="T7" id="Seg_752" s="T6">pro.h:S</ta>
            <ta e="T9" id="Seg_753" s="T8">v:pred</ta>
            <ta e="T10" id="Seg_754" s="T9">np:O</ta>
            <ta e="T12" id="Seg_755" s="T11">0.3.h:S v:pred</ta>
            <ta e="T14" id="Seg_756" s="T13">np:S</ta>
            <ta e="T15" id="Seg_757" s="T14">adj:pred</ta>
            <ta e="T16" id="Seg_758" s="T15">cop</ta>
            <ta e="T23" id="Seg_759" s="T22">0.3.h:S v:pred</ta>
            <ta e="T25" id="Seg_760" s="T24">np:O</ta>
            <ta e="T27" id="Seg_761" s="T26">np.h:S</ta>
            <ta e="T28" id="Seg_762" s="T27">v:pred</ta>
            <ta e="T30" id="Seg_763" s="T29">0.1.h:S v:pred</ta>
            <ta e="T32" id="Seg_764" s="T31">pro.h:S</ta>
            <ta e="T33" id="Seg_765" s="T32">v:pred</ta>
            <ta e="T36" id="Seg_766" s="T35">np:S</ta>
            <ta e="T38" id="Seg_767" s="T37">v:pred</ta>
            <ta e="T39" id="Seg_768" s="T38">np:O</ta>
            <ta e="T40" id="Seg_769" s="T39">0.3.h:S v:pred</ta>
            <ta e="T42" id="Seg_770" s="T41">0.3.h:S v:pred</ta>
            <ta e="T44" id="Seg_771" s="T43">np.h:S</ta>
            <ta e="T45" id="Seg_772" s="T44">pro.h:O</ta>
            <ta e="T46" id="Seg_773" s="T45">v:pred</ta>
            <ta e="T47" id="Seg_774" s="T46">0.2.h:S v:pred</ta>
            <ta e="T50" id="Seg_775" s="T49">np:S</ta>
            <ta e="T51" id="Seg_776" s="T50">adj:pred</ta>
            <ta e="T52" id="Seg_777" s="T51">cop</ta>
            <ta e="T59" id="Seg_778" s="T58">np:O</ta>
            <ta e="T60" id="Seg_779" s="T59">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_780" s="T3">RUS:cult</ta>
            <ta e="T11" id="Seg_781" s="T10">RUS:gram</ta>
            <ta e="T14" id="Seg_782" s="T13">RUS:core</ta>
            <ta e="T17" id="Seg_783" s="T16">RUS:gram</ta>
            <ta e="T21" id="Seg_784" s="T20">RUS:gram</ta>
            <ta e="T22" id="Seg_785" s="T21">RUS:cult</ta>
            <ta e="T26" id="Seg_786" s="T25">RUS:gram</ta>
            <ta e="T27" id="Seg_787" s="T26">RUS:core</ta>
            <ta e="T34" id="Seg_788" s="T33">RUS:cult</ta>
            <ta e="T41" id="Seg_789" s="T40">RUS:gram</ta>
            <ta e="T43" id="Seg_790" s="T42">RUS:cult</ta>
            <ta e="T44" id="Seg_791" s="T43">RUS:cult</ta>
            <ta e="T50" id="Seg_792" s="T49">RUS:core</ta>
            <ta e="T53" id="Seg_793" s="T52">RUS:gram</ta>
            <ta e="T56" id="Seg_794" s="T55">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_795" s="T1">One woman stabbed a calf in Kolpashevo.</ta>
            <ta e="T20" id="Seg_796" s="T6">She cut its meat [into two parts] and wrote: the rear part costs more, the front part costs less.</ta>
            <ta e="T25" id="Seg_797" s="T20">And she put her writing onto the chair.</ta>
            <ta e="T29" id="Seg_798" s="T25">And a friend of her came to her running.</ta>
            <ta e="T31" id="Seg_799" s="T29">“Let's go fast.”</ta>
            <ta e="T34" id="Seg_800" s="T31">She sat down onto the chair.</ta>
            <ta e="T38" id="Seg_801" s="T34">This writing stuck to her.</ta>
            <ta e="T43" id="Seg_802" s="T38">They took the meat and ran to the market.</ta>
            <ta e="T46" id="Seg_803" s="T43">A policeman stopped them.</ta>
            <ta e="T47" id="Seg_804" s="T46">“Stop!</ta>
            <ta e="T57" id="Seg_805" s="T47">What is it: the rear part costs more, the front part costs less?”</ta>
            <ta e="T60" id="Seg_806" s="T57">“What is it, I hung a notice.”</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_807" s="T1">Eine Frau erstach in Kolpashevo ein Kalb.</ta>
            <ta e="T20" id="Seg_808" s="T6">Sie schnitt sein Fleisch in zwei Teile und schrieb: Der hintere Teil kostet mehr, der vordere Teil kostet weniger.</ta>
            <ta e="T25" id="Seg_809" s="T20">Und sie legte das Schreiben auf den Stuhl.</ta>
            <ta e="T29" id="Seg_810" s="T25">Und eine Freundin kam zu ihr angerannt.</ta>
            <ta e="T31" id="Seg_811" s="T29">"Lass uns schnell gehen."</ta>
            <ta e="T34" id="Seg_812" s="T31">Sie setzte sich auf den Stuhl.</ta>
            <ta e="T38" id="Seg_813" s="T34">Dieses Schreiben blieb an ihr kleben.</ta>
            <ta e="T43" id="Seg_814" s="T38">Sie nahmen das Fleisch und rannten zum Markt.</ta>
            <ta e="T46" id="Seg_815" s="T43">Ein Polizist hielt sie an.</ta>
            <ta e="T47" id="Seg_816" s="T46">"Halt!</ta>
            <ta e="T57" id="Seg_817" s="T47">Was ist das: der hintere Teil kostet mehr, der vordere Teil kostet weniger?"</ta>
            <ta e="T60" id="Seg_818" s="T57">"Was es ist, ich hing eine Notiz (auf)."</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_819" s="T1">Одна женщина в Колпашево зарезала теленка.</ta>
            <ta e="T20" id="Seg_820" s="T6">Она пополам разрезала мясо и написала: задняя часть дороже, а передняя – дешевле.</ta>
            <ta e="T25" id="Seg_821" s="T20">И на стул положила то, что написала.</ta>
            <ta e="T29" id="Seg_822" s="T25">А подруга прибежала к ней.</ta>
            <ta e="T31" id="Seg_823" s="T29">“Пойдем скорее”.</ta>
            <ta e="T34" id="Seg_824" s="T31">Она села на стул.</ta>
            <ta e="T38" id="Seg_825" s="T34">Это написанное к ней прилипло.</ta>
            <ta e="T43" id="Seg_826" s="T38">Мясо подхватили и побежали на базар.</ta>
            <ta e="T46" id="Seg_827" s="T43">Милиционер их остановил.</ta>
            <ta e="T47" id="Seg_828" s="T46">“Остановитесь! </ta>
            <ta e="T57" id="Seg_829" s="T47">Что, задняя часть дороже, а передняя часть дешевле?”</ta>
            <ta e="T60" id="Seg_830" s="T57">“Что, я объявление повесила”.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_831" s="T1">одна женщина в Колпашево зарезала теленка</ta>
            <ta e="T20" id="Seg_832" s="T6">она пополам разрезала (отрезала) мясо написала задняя часть дороже а передняя дешевле</ta>
            <ta e="T25" id="Seg_833" s="T20">на стул положила то что написала</ta>
            <ta e="T29" id="Seg_834" s="T25">а подруга прибежала к ней</ta>
            <ta e="T31" id="Seg_835" s="T29">пойдем скорее</ta>
            <ta e="T34" id="Seg_836" s="T31">она села на стул</ta>
            <ta e="T38" id="Seg_837" s="T34">это написанное к ней прильнуло (прилепилось)</ta>
            <ta e="T43" id="Seg_838" s="T38">мясо подхватили и побежали на базар</ta>
            <ta e="T46" id="Seg_839" s="T43">милиционер их остановил</ta>
            <ta e="T47" id="Seg_840" s="T46">остановитесь</ta>
            <ta e="T57" id="Seg_841" s="T47">чего задняя часть дороже а передняя часть дешевле</ta>
            <ta e="T60" id="Seg_842" s="T57">чего объявление повесила</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
