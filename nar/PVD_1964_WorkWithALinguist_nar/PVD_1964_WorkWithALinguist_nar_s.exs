<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_WorkWithALinguist_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_WorkWithALinguist_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">195</ud-information>
            <ud-information attribute-name="# HIAT:w">148</ud-information>
            <ud-information attribute-name="# e">148</ud-information>
            <ud-information attribute-name="# HIAT:u">35</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T149" id="Seg_0" n="sc" s="T1">
               <ts e="T10" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">A</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">tan</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">kutdär</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">kostal</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">što</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_19" n="HIAT:w" s="T6">man</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_22" n="HIAT:w" s="T7">süsögulʼ</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_25" n="HIAT:w" s="T8">der</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_28" n="HIAT:w" s="T9">koluppan</ts>
                  <nts id="Seg_29" n="HIAT:ip">?</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_32" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">Tan</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_37" n="HIAT:w" s="T11">man</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_40" n="HIAT:w" s="T12">seu</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_43" n="HIAT:w" s="T13">wes</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_46" n="HIAT:w" s="T14">tunal</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_49" n="HIAT:w" s="T15">i</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_52" n="HIAT:w" s="T16">kolomaːt</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_55" n="HIAT:w" s="T17">soːdʼigan</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_59" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_61" n="HIAT:w" s="T18">A</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_64" n="HIAT:w" s="T19">Nina</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_67" n="HIAT:w" s="T20">ass</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_70" n="HIAT:w" s="T21">tunun</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_73" n="HIAT:w" s="T22">kulupbugu</ts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_77" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_79" n="HIAT:w" s="T23">Nu</ts>
                  <nts id="Seg_80" n="HIAT:ip">,</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_83" n="HIAT:w" s="T24">qwallaj</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_86" n="HIAT:w" s="T25">aːmnetǯaj</ts>
                  <nts id="Seg_87" n="HIAT:ip">,</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_90" n="HIAT:w" s="T26">natʼen</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_93" n="HIAT:w" s="T27">i</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_96" n="HIAT:w" s="T28">kulumeǯaj</ts>
                  <nts id="Seg_97" n="HIAT:ip">.</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_100" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_102" n="HIAT:w" s="T29">Wes</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_105" n="HIAT:w" s="T30">äʒlam</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_108" n="HIAT:w" s="T31">ketčaj</ts>
                  <nts id="Seg_109" n="HIAT:ip">.</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_112" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_114" n="HIAT:w" s="T32">Man</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_117" n="HIAT:w" s="T33">ass</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_120" n="HIAT:w" s="T34">tunnou</ts>
                  <nts id="Seg_121" n="HIAT:ip">,</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_124" n="HIAT:w" s="T35">qusakɨn</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_127" n="HIAT:w" s="T36">malčɨdʼetʒan</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_130" n="HIAT:w" s="T37">alʼi</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_133" n="HIAT:w" s="T38">ass</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_137" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_139" n="HIAT:w" s="T39">Majɣən</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_142" n="HIAT:w" s="T40">wes</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_145" n="HIAT:w" s="T41">malčeǯaj</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_148" n="HIAT:w" s="T42">äʒlam</ts>
                  <nts id="Seg_149" n="HIAT:ip">.</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_152" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_154" n="HIAT:w" s="T43">Man</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_157" n="HIAT:w" s="T44">patom</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_160" n="HIAT:w" s="T45">jedot</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_163" n="HIAT:w" s="T46">qwaǯan</ts>
                  <nts id="Seg_164" n="HIAT:ip">.</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_167" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_169" n="HIAT:w" s="T47">I</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_172" n="HIAT:w" s="T48">tannä</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_175" n="HIAT:w" s="T49">qwannaš</ts>
                  <nts id="Seg_176" n="HIAT:ip">.</nts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_179" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_181" n="HIAT:w" s="T50">Köse</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_184" n="HIAT:w" s="T51">čaʒəlʼe</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_187" n="HIAT:w" s="T52">mʼekga</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_190" n="HIAT:w" s="T53">udurukok</ts>
                  <nts id="Seg_191" n="HIAT:ip">.</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_194" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_196" n="HIAT:w" s="T54">Man</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_199" n="HIAT:w" s="T55">qwɛlla</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_202" n="HIAT:w" s="T56">qwatčedan</ts>
                  <nts id="Seg_203" n="HIAT:ip">.</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_206" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_208" n="HIAT:w" s="T57">A</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_211" n="HIAT:w" s="T58">tan</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_214" n="HIAT:w" s="T59">udurukok</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_217" n="HIAT:w" s="T60">qwɛlɨm</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_220" n="HIAT:w" s="T61">amgu</ts>
                  <nts id="Seg_221" n="HIAT:ip">.</nts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_224" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_226" n="HIAT:w" s="T62">Qajze</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_229" n="HIAT:w" s="T63">qazaxsen</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_232" n="HIAT:w" s="T64">qulummat</ts>
                  <nts id="Seg_233" n="HIAT:ip">?</nts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_236" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_238" n="HIAT:w" s="T65">Mat</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_241" n="HIAT:w" s="T66">darʼe</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_244" n="HIAT:w" s="T67">nadə</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_247" n="HIAT:w" s="T68">quluppugu</ts>
                  <nts id="Seg_248" n="HIAT:ip">.</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_251" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_253" n="HIAT:w" s="T69">A</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_256" n="HIAT:w" s="T70">tan</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_259" n="HIAT:w" s="T71">qulumatdə</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_262" n="HIAT:w" s="T72">süsögusen</ts>
                  <nts id="Seg_263" n="HIAT:ip">?</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_266" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_268" n="HIAT:w" s="T73">Oqqɨr</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_271" n="HIAT:w" s="T74">täbequm</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_274" n="HIAT:w" s="T75">süsögusen</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_277" n="HIAT:w" s="T76">soːdʼigan</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_280" n="HIAT:w" s="T77">qulupbas</ts>
                  <nts id="Seg_281" n="HIAT:ip">.</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_284" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_286" n="HIAT:w" s="T78">A</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_289" n="HIAT:w" s="T79">sičʼas</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_292" n="HIAT:w" s="T80">qurolǯeǯan</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_295" n="HIAT:w" s="T81">udʼigu</ts>
                  <nts id="Seg_296" n="HIAT:ip">.</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_299" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_301" n="HIAT:w" s="T82">Udʼigu</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_304" n="HIAT:w" s="T83">nadə</ts>
                  <nts id="Seg_305" n="HIAT:ip">.</nts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_308" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_310" n="HIAT:w" s="T84">As</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_313" n="HIAT:w" s="T85">udʼanaš</ts>
                  <nts id="Seg_314" n="HIAT:ip">,</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_317" n="HIAT:w" s="T86">a</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_320" n="HIAT:w" s="T87">qaim</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_323" n="HIAT:w" s="T88">amgu</ts>
                  <nts id="Seg_324" n="HIAT:ip">?</nts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_327" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_329" n="HIAT:w" s="T89">Me</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_332" n="HIAT:w" s="T90">witʼ</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_335" n="HIAT:w" s="T91">nagurtaftə</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_338" n="HIAT:w" s="T92">jewoftə</ts>
                  <nts id="Seg_339" n="HIAT:ip">.</nts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_342" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_344" n="HIAT:w" s="T93">Nagur</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_347" n="HIAT:w" s="T94">qumdə</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_350" n="HIAT:w" s="T95">koːcʼ</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_353" n="HIAT:w" s="T96">qomdə</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_356" n="HIAT:w" s="T97">nadə</ts>
                  <nts id="Seg_357" n="HIAT:ip">.</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_360" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_362" n="HIAT:w" s="T98">Tamdʼel</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_365" n="HIAT:w" s="T99">pen</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_368" n="HIAT:w" s="T100">tastɨ</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_371" n="HIAT:w" s="T101">oɣɨlǯoʒiku</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_374" n="HIAT:w" s="T102">kɨgɨzan</ts>
                  <nts id="Seg_375" n="HIAT:ip">.</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_378" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_380" n="HIAT:w" s="T103">A</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_383" n="HIAT:w" s="T104">tan</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_386" n="HIAT:w" s="T105">sɨdɨdʼat</ts>
                  <nts id="Seg_387" n="HIAT:ip">.</nts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_390" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_392" n="HIAT:w" s="T106">Man</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_395" n="HIAT:w" s="T107">tʼöːtǯikan</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_398" n="HIAT:w" s="T108">tüwan</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_401" n="HIAT:w" s="T109">tekga</ts>
                  <nts id="Seg_402" n="HIAT:ip">.</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_405" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_407" n="HIAT:w" s="T110">A</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_410" n="HIAT:w" s="T111">tan</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_413" n="HIAT:w" s="T112">ütdədäːl</ts>
                  <nts id="Seg_414" n="HIAT:ip">.</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_417" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_419" n="HIAT:w" s="T113">A</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_422" n="HIAT:w" s="T114">qutder</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_425" n="HIAT:w" s="T115">tastɨ</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_428" n="HIAT:w" s="T116">oɣulǯoʒigu</ts>
                  <nts id="Seg_429" n="HIAT:ip">?</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_432" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_434" n="HIAT:w" s="T117">Onetdə</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_437" n="HIAT:w" s="T118">tʼarkwat</ts>
                  <nts id="Seg_438" n="HIAT:ip">:</nts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_440" n="HIAT:ip">“</nts>
                  <ts e="T120" id="Seg_442" n="HIAT:w" s="T119">Mazɨm</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_445" n="HIAT:w" s="T120">oɣulǯoʒak</ts>
                  <nts id="Seg_446" n="HIAT:ip">,</nts>
                  <nts id="Seg_447" n="HIAT:ip">”</nts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_449" n="HIAT:ip">–</nts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_452" n="HIAT:w" s="T121">onetdə</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_455" n="HIAT:w" s="T122">sədɨtʼekwattə</ts>
                  <nts id="Seg_456" n="HIAT:ip">.</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_459" n="HIAT:u" s="T123">
                  <nts id="Seg_460" n="HIAT:ip">“</nts>
                  <ts e="T124" id="Seg_462" n="HIAT:w" s="T123">Man</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_465" n="HIAT:w" s="T124">as</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_468" n="HIAT:w" s="T125">sɨdɨtʼukkəʒan</ts>
                  <nts id="Seg_469" n="HIAT:ip">.</nts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_472" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_474" n="HIAT:w" s="T126">Man</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_477" n="HIAT:w" s="T127">krʼepkan</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_480" n="HIAT:w" s="T128">qonneǯan</ts>
                  <nts id="Seg_481" n="HIAT:ip">.</nts>
                  <nts id="Seg_482" n="HIAT:ip">”</nts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_485" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_487" n="HIAT:w" s="T129">Sirawno</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_490" n="HIAT:w" s="T130">sɨdɨtʼannaš</ts>
                  <nts id="Seg_491" n="HIAT:ip">.</nts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_494" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_496" n="HIAT:w" s="T131">Man</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_499" n="HIAT:w" s="T132">tabben</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_502" n="HIAT:w" s="T133">tekga</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_505" n="HIAT:w" s="T134">aj</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_508" n="HIAT:w" s="T135">müdəkeǯan</ts>
                  <nts id="Seg_509" n="HIAT:ip">.</nts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_512" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_514" n="HIAT:w" s="T136">Man</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_517" n="HIAT:w" s="T137">tʼekka</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_520" n="HIAT:w" s="T138">sɨsari</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_523" n="HIAT:w" s="T139">äːš</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_526" n="HIAT:w" s="T140">ketčedan</ts>
                  <nts id="Seg_527" n="HIAT:ip">.</nts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_530" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_532" n="HIAT:w" s="T141">Oɣulǯoʒak</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_535" n="HIAT:w" s="T142">soːdʼigan</ts>
                  <nts id="Seg_536" n="HIAT:ip">.</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_539" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_541" n="HIAT:w" s="T143">Ato</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_544" n="HIAT:w" s="T144">menan</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_547" n="HIAT:w" s="T145">biskweǯattə</ts>
                  <nts id="Seg_548" n="HIAT:ip">.</nts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_551" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_553" n="HIAT:w" s="T146">Qaj</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_556" n="HIAT:w" s="T147">nüpla</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_559" n="HIAT:w" s="T148">jes</ts>
                  <nts id="Seg_560" n="HIAT:ip">.</nts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T149" id="Seg_562" n="sc" s="T1">
               <ts e="T2" id="Seg_564" n="e" s="T1">A </ts>
               <ts e="T3" id="Seg_566" n="e" s="T2">tan </ts>
               <ts e="T4" id="Seg_568" n="e" s="T3">kutdär </ts>
               <ts e="T5" id="Seg_570" n="e" s="T4">kostal </ts>
               <ts e="T6" id="Seg_572" n="e" s="T5">što </ts>
               <ts e="T7" id="Seg_574" n="e" s="T6">man </ts>
               <ts e="T8" id="Seg_576" n="e" s="T7">süsögulʼ </ts>
               <ts e="T9" id="Seg_578" n="e" s="T8">der </ts>
               <ts e="T10" id="Seg_580" n="e" s="T9">koluppan? </ts>
               <ts e="T11" id="Seg_582" n="e" s="T10">Tan </ts>
               <ts e="T12" id="Seg_584" n="e" s="T11">man </ts>
               <ts e="T13" id="Seg_586" n="e" s="T12">seu </ts>
               <ts e="T14" id="Seg_588" n="e" s="T13">wes </ts>
               <ts e="T15" id="Seg_590" n="e" s="T14">tunal </ts>
               <ts e="T16" id="Seg_592" n="e" s="T15">i </ts>
               <ts e="T17" id="Seg_594" n="e" s="T16">kolomaːt </ts>
               <ts e="T18" id="Seg_596" n="e" s="T17">soːdʼigan. </ts>
               <ts e="T19" id="Seg_598" n="e" s="T18">A </ts>
               <ts e="T20" id="Seg_600" n="e" s="T19">Nina </ts>
               <ts e="T21" id="Seg_602" n="e" s="T20">ass </ts>
               <ts e="T22" id="Seg_604" n="e" s="T21">tunun </ts>
               <ts e="T23" id="Seg_606" n="e" s="T22">kulupbugu. </ts>
               <ts e="T24" id="Seg_608" n="e" s="T23">Nu, </ts>
               <ts e="T25" id="Seg_610" n="e" s="T24">qwallaj </ts>
               <ts e="T26" id="Seg_612" n="e" s="T25">aːmnetǯaj, </ts>
               <ts e="T27" id="Seg_614" n="e" s="T26">natʼen </ts>
               <ts e="T28" id="Seg_616" n="e" s="T27">i </ts>
               <ts e="T29" id="Seg_618" n="e" s="T28">kulumeǯaj. </ts>
               <ts e="T30" id="Seg_620" n="e" s="T29">Wes </ts>
               <ts e="T31" id="Seg_622" n="e" s="T30">äʒlam </ts>
               <ts e="T32" id="Seg_624" n="e" s="T31">ketčaj. </ts>
               <ts e="T33" id="Seg_626" n="e" s="T32">Man </ts>
               <ts e="T34" id="Seg_628" n="e" s="T33">ass </ts>
               <ts e="T35" id="Seg_630" n="e" s="T34">tunnou, </ts>
               <ts e="T36" id="Seg_632" n="e" s="T35">qusakɨn </ts>
               <ts e="T37" id="Seg_634" n="e" s="T36">malčɨdʼetʒan </ts>
               <ts e="T38" id="Seg_636" n="e" s="T37">alʼi </ts>
               <ts e="T39" id="Seg_638" n="e" s="T38">ass. </ts>
               <ts e="T40" id="Seg_640" n="e" s="T39">Majɣən </ts>
               <ts e="T41" id="Seg_642" n="e" s="T40">wes </ts>
               <ts e="T42" id="Seg_644" n="e" s="T41">malčeǯaj </ts>
               <ts e="T43" id="Seg_646" n="e" s="T42">äʒlam. </ts>
               <ts e="T44" id="Seg_648" n="e" s="T43">Man </ts>
               <ts e="T45" id="Seg_650" n="e" s="T44">patom </ts>
               <ts e="T46" id="Seg_652" n="e" s="T45">jedot </ts>
               <ts e="T47" id="Seg_654" n="e" s="T46">qwaǯan. </ts>
               <ts e="T48" id="Seg_656" n="e" s="T47">I </ts>
               <ts e="T49" id="Seg_658" n="e" s="T48">tannä </ts>
               <ts e="T50" id="Seg_660" n="e" s="T49">qwannaš. </ts>
               <ts e="T51" id="Seg_662" n="e" s="T50">Köse </ts>
               <ts e="T52" id="Seg_664" n="e" s="T51">čaʒəlʼe </ts>
               <ts e="T53" id="Seg_666" n="e" s="T52">mʼekga </ts>
               <ts e="T54" id="Seg_668" n="e" s="T53">udurukok. </ts>
               <ts e="T55" id="Seg_670" n="e" s="T54">Man </ts>
               <ts e="T56" id="Seg_672" n="e" s="T55">qwɛlla </ts>
               <ts e="T57" id="Seg_674" n="e" s="T56">qwatčedan. </ts>
               <ts e="T58" id="Seg_676" n="e" s="T57">A </ts>
               <ts e="T59" id="Seg_678" n="e" s="T58">tan </ts>
               <ts e="T60" id="Seg_680" n="e" s="T59">udurukok </ts>
               <ts e="T61" id="Seg_682" n="e" s="T60">qwɛlɨm </ts>
               <ts e="T62" id="Seg_684" n="e" s="T61">amgu. </ts>
               <ts e="T63" id="Seg_686" n="e" s="T62">Qajze </ts>
               <ts e="T64" id="Seg_688" n="e" s="T63">qazaxsen </ts>
               <ts e="T65" id="Seg_690" n="e" s="T64">qulummat? </ts>
               <ts e="T66" id="Seg_692" n="e" s="T65">Mat </ts>
               <ts e="T67" id="Seg_694" n="e" s="T66">darʼe </ts>
               <ts e="T68" id="Seg_696" n="e" s="T67">nadə </ts>
               <ts e="T69" id="Seg_698" n="e" s="T68">quluppugu. </ts>
               <ts e="T70" id="Seg_700" n="e" s="T69">A </ts>
               <ts e="T71" id="Seg_702" n="e" s="T70">tan </ts>
               <ts e="T72" id="Seg_704" n="e" s="T71">qulumatdə </ts>
               <ts e="T73" id="Seg_706" n="e" s="T72">süsögusen? </ts>
               <ts e="T74" id="Seg_708" n="e" s="T73">Oqqɨr </ts>
               <ts e="T75" id="Seg_710" n="e" s="T74">täbequm </ts>
               <ts e="T76" id="Seg_712" n="e" s="T75">süsögusen </ts>
               <ts e="T77" id="Seg_714" n="e" s="T76">soːdʼigan </ts>
               <ts e="T78" id="Seg_716" n="e" s="T77">qulupbas. </ts>
               <ts e="T79" id="Seg_718" n="e" s="T78">A </ts>
               <ts e="T80" id="Seg_720" n="e" s="T79">sičʼas </ts>
               <ts e="T81" id="Seg_722" n="e" s="T80">qurolǯeǯan </ts>
               <ts e="T82" id="Seg_724" n="e" s="T81">udʼigu. </ts>
               <ts e="T83" id="Seg_726" n="e" s="T82">Udʼigu </ts>
               <ts e="T84" id="Seg_728" n="e" s="T83">nadə. </ts>
               <ts e="T85" id="Seg_730" n="e" s="T84">As </ts>
               <ts e="T86" id="Seg_732" n="e" s="T85">udʼanaš, </ts>
               <ts e="T87" id="Seg_734" n="e" s="T86">a </ts>
               <ts e="T88" id="Seg_736" n="e" s="T87">qaim </ts>
               <ts e="T89" id="Seg_738" n="e" s="T88">amgu? </ts>
               <ts e="T90" id="Seg_740" n="e" s="T89">Me </ts>
               <ts e="T91" id="Seg_742" n="e" s="T90">witʼ </ts>
               <ts e="T92" id="Seg_744" n="e" s="T91">nagurtaftə </ts>
               <ts e="T93" id="Seg_746" n="e" s="T92">jewoftə. </ts>
               <ts e="T94" id="Seg_748" n="e" s="T93">Nagur </ts>
               <ts e="T95" id="Seg_750" n="e" s="T94">qumdə </ts>
               <ts e="T96" id="Seg_752" n="e" s="T95">koːcʼ </ts>
               <ts e="T97" id="Seg_754" n="e" s="T96">qomdə </ts>
               <ts e="T98" id="Seg_756" n="e" s="T97">nadə. </ts>
               <ts e="T99" id="Seg_758" n="e" s="T98">Tamdʼel </ts>
               <ts e="T100" id="Seg_760" n="e" s="T99">pen </ts>
               <ts e="T101" id="Seg_762" n="e" s="T100">tastɨ </ts>
               <ts e="T102" id="Seg_764" n="e" s="T101">oɣɨlǯoʒiku </ts>
               <ts e="T103" id="Seg_766" n="e" s="T102">kɨgɨzan. </ts>
               <ts e="T104" id="Seg_768" n="e" s="T103">A </ts>
               <ts e="T105" id="Seg_770" n="e" s="T104">tan </ts>
               <ts e="T106" id="Seg_772" n="e" s="T105">sɨdɨdʼat. </ts>
               <ts e="T107" id="Seg_774" n="e" s="T106">Man </ts>
               <ts e="T108" id="Seg_776" n="e" s="T107">tʼöːtǯikan </ts>
               <ts e="T109" id="Seg_778" n="e" s="T108">tüwan </ts>
               <ts e="T110" id="Seg_780" n="e" s="T109">tekga. </ts>
               <ts e="T111" id="Seg_782" n="e" s="T110">A </ts>
               <ts e="T112" id="Seg_784" n="e" s="T111">tan </ts>
               <ts e="T113" id="Seg_786" n="e" s="T112">ütdədäːl. </ts>
               <ts e="T114" id="Seg_788" n="e" s="T113">A </ts>
               <ts e="T115" id="Seg_790" n="e" s="T114">qutder </ts>
               <ts e="T116" id="Seg_792" n="e" s="T115">tastɨ </ts>
               <ts e="T117" id="Seg_794" n="e" s="T116">oɣulǯoʒigu? </ts>
               <ts e="T118" id="Seg_796" n="e" s="T117">Onetdə </ts>
               <ts e="T119" id="Seg_798" n="e" s="T118">tʼarkwat: </ts>
               <ts e="T120" id="Seg_800" n="e" s="T119">“Mazɨm </ts>
               <ts e="T121" id="Seg_802" n="e" s="T120">oɣulǯoʒak,” – </ts>
               <ts e="T122" id="Seg_804" n="e" s="T121">onetdə </ts>
               <ts e="T123" id="Seg_806" n="e" s="T122">sədɨtʼekwattə. </ts>
               <ts e="T124" id="Seg_808" n="e" s="T123">“Man </ts>
               <ts e="T125" id="Seg_810" n="e" s="T124">as </ts>
               <ts e="T126" id="Seg_812" n="e" s="T125">sɨdɨtʼukkəʒan. </ts>
               <ts e="T127" id="Seg_814" n="e" s="T126">Man </ts>
               <ts e="T128" id="Seg_816" n="e" s="T127">krʼepkan </ts>
               <ts e="T129" id="Seg_818" n="e" s="T128">qonneǯan.” </ts>
               <ts e="T130" id="Seg_820" n="e" s="T129">Sirawno </ts>
               <ts e="T131" id="Seg_822" n="e" s="T130">sɨdɨtʼannaš. </ts>
               <ts e="T132" id="Seg_824" n="e" s="T131">Man </ts>
               <ts e="T133" id="Seg_826" n="e" s="T132">tabben </ts>
               <ts e="T134" id="Seg_828" n="e" s="T133">tekga </ts>
               <ts e="T135" id="Seg_830" n="e" s="T134">aj </ts>
               <ts e="T136" id="Seg_832" n="e" s="T135">müdəkeǯan. </ts>
               <ts e="T137" id="Seg_834" n="e" s="T136">Man </ts>
               <ts e="T138" id="Seg_836" n="e" s="T137">tʼekka </ts>
               <ts e="T139" id="Seg_838" n="e" s="T138">sɨsari </ts>
               <ts e="T140" id="Seg_840" n="e" s="T139">äːš </ts>
               <ts e="T141" id="Seg_842" n="e" s="T140">ketčedan. </ts>
               <ts e="T142" id="Seg_844" n="e" s="T141">Oɣulǯoʒak </ts>
               <ts e="T143" id="Seg_846" n="e" s="T142">soːdʼigan. </ts>
               <ts e="T144" id="Seg_848" n="e" s="T143">Ato </ts>
               <ts e="T145" id="Seg_850" n="e" s="T144">menan </ts>
               <ts e="T146" id="Seg_852" n="e" s="T145">biskweǯattə. </ts>
               <ts e="T147" id="Seg_854" n="e" s="T146">Qaj </ts>
               <ts e="T148" id="Seg_856" n="e" s="T147">nüpla </ts>
               <ts e="T149" id="Seg_858" n="e" s="T148">jes. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T10" id="Seg_859" s="T1">PVD_1964_WorkWithALinguist_nar.001 (001.001)</ta>
            <ta e="T18" id="Seg_860" s="T10">PVD_1964_WorkWithALinguist_nar.002 (001.002)</ta>
            <ta e="T23" id="Seg_861" s="T18">PVD_1964_WorkWithALinguist_nar.003 (001.003)</ta>
            <ta e="T29" id="Seg_862" s="T23">PVD_1964_WorkWithALinguist_nar.004 (001.004)</ta>
            <ta e="T32" id="Seg_863" s="T29">PVD_1964_WorkWithALinguist_nar.005 (001.005)</ta>
            <ta e="T39" id="Seg_864" s="T32">PVD_1964_WorkWithALinguist_nar.006 (001.006)</ta>
            <ta e="T43" id="Seg_865" s="T39">PVD_1964_WorkWithALinguist_nar.007 (001.007)</ta>
            <ta e="T47" id="Seg_866" s="T43">PVD_1964_WorkWithALinguist_nar.008 (001.008)</ta>
            <ta e="T50" id="Seg_867" s="T47">PVD_1964_WorkWithALinguist_nar.009 (001.009)</ta>
            <ta e="T54" id="Seg_868" s="T50">PVD_1964_WorkWithALinguist_nar.010 (001.010)</ta>
            <ta e="T57" id="Seg_869" s="T54">PVD_1964_WorkWithALinguist_nar.011 (001.011)</ta>
            <ta e="T62" id="Seg_870" s="T57">PVD_1964_WorkWithALinguist_nar.012 (001.012)</ta>
            <ta e="T65" id="Seg_871" s="T62">PVD_1964_WorkWithALinguist_nar.013 (001.013)</ta>
            <ta e="T69" id="Seg_872" s="T65">PVD_1964_WorkWithALinguist_nar.014 (001.014)</ta>
            <ta e="T73" id="Seg_873" s="T69">PVD_1964_WorkWithALinguist_nar.015 (001.015)</ta>
            <ta e="T78" id="Seg_874" s="T73">PVD_1964_WorkWithALinguist_nar.016 (001.016)</ta>
            <ta e="T82" id="Seg_875" s="T78">PVD_1964_WorkWithALinguist_nar.017 (001.017)</ta>
            <ta e="T84" id="Seg_876" s="T82">PVD_1964_WorkWithALinguist_nar.018 (001.018)</ta>
            <ta e="T89" id="Seg_877" s="T84">PVD_1964_WorkWithALinguist_nar.019 (001.019)</ta>
            <ta e="T93" id="Seg_878" s="T89">PVD_1964_WorkWithALinguist_nar.020 (001.020)</ta>
            <ta e="T98" id="Seg_879" s="T93">PVD_1964_WorkWithALinguist_nar.021 (001.021)</ta>
            <ta e="T103" id="Seg_880" s="T98">PVD_1964_WorkWithALinguist_nar.022 (001.022)</ta>
            <ta e="T106" id="Seg_881" s="T103">PVD_1964_WorkWithALinguist_nar.023 (001.023)</ta>
            <ta e="T110" id="Seg_882" s="T106">PVD_1964_WorkWithALinguist_nar.024 (001.024)</ta>
            <ta e="T113" id="Seg_883" s="T110">PVD_1964_WorkWithALinguist_nar.025 (001.025)</ta>
            <ta e="T117" id="Seg_884" s="T113">PVD_1964_WorkWithALinguist_nar.026 (001.026)</ta>
            <ta e="T123" id="Seg_885" s="T117">PVD_1964_WorkWithALinguist_nar.027 (001.027)</ta>
            <ta e="T126" id="Seg_886" s="T123">PVD_1964_WorkWithALinguist_nar.028 (001.028)</ta>
            <ta e="T129" id="Seg_887" s="T126">PVD_1964_WorkWithALinguist_nar.029 (001.029)</ta>
            <ta e="T131" id="Seg_888" s="T129">PVD_1964_WorkWithALinguist_nar.030 (001.030)</ta>
            <ta e="T136" id="Seg_889" s="T131">PVD_1964_WorkWithALinguist_nar.031 (001.031)</ta>
            <ta e="T141" id="Seg_890" s="T136">PVD_1964_WorkWithALinguist_nar.032 (001.032)</ta>
            <ta e="T143" id="Seg_891" s="T141">PVD_1964_WorkWithALinguist_nar.033 (001.033)</ta>
            <ta e="T146" id="Seg_892" s="T143">PVD_1964_WorkWithALinguist_nar.034 (001.034)</ta>
            <ta e="T149" id="Seg_893" s="T146">PVD_1964_WorkWithALinguist_nar.035 (001.035)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T10" id="Seg_894" s="T1">а тан кут′дӓр кос′тал што ман сӱ′сӧгулʼдер коlуппан?</ta>
            <ta e="T18" id="Seg_895" s="T10">тан ман ′сеу вес ту′нал и коlома̄т со̄дʼиган.</ta>
            <ta e="T23" id="Seg_896" s="T18">а ′Нина асс ту′нун ку′lупбугу.</ta>
            <ta e="T29" id="Seg_897" s="T23">ну, kwа′ллай ′а̄мнетджай, на′тʼен и ′куlумеджай.</ta>
            <ta e="T32" id="Seg_898" s="T29">вес ′ӓжлам кет′тшай.</ta>
            <ta e="T39" id="Seg_899" s="T32">ман асс ту′нноу, kу′сакын маlтшы′д̂ʼетжан алʼи асс.</ta>
            <ta e="T43" id="Seg_900" s="T39">′майɣън вес маl′тшеджай ′ӓжлам.</ta>
            <ta e="T47" id="Seg_901" s="T43">ман па′том ′jедот kwа′джан.</ta>
            <ta e="T50" id="Seg_902" s="T47">и та′ннӓ ′kwаннаш.</ta>
            <ta e="T54" id="Seg_903" s="T50">′кӧсе тшажълʼе мʼекга удуру′кок.</ta>
            <ta e="T57" id="Seg_904" s="T54">ман ′kwɛлла ′kwаттшед̂ан.</ta>
            <ta e="T62" id="Seg_905" s="T57">а тан удуру′кок ′kwɛлым ′амгу.</ta>
            <ta e="T65" id="Seg_906" s="T62">kай′зе kа′захсен kуlуммат?</ta>
            <ta e="T69" id="Seg_907" s="T65">мат′дарʼе надъ kу′lуппугу.</ta>
            <ta e="T73" id="Seg_908" s="T69">а тан kулу′матд̂ъ сӱ′сӧгусен?</ta>
            <ta e="T78" id="Seg_909" s="T73">о′kkыр ′тӓбеkум сӱ′сӧгусен со̄дʼиган kу′лупб̂ас.</ta>
            <ta e="T82" id="Seg_910" s="T78">а си′чʼас kу′роlдже‵джан ′удʼигу.</ta>
            <ta e="T84" id="Seg_911" s="T82">′удʼигу ′надъ.</ta>
            <ta e="T89" id="Seg_912" s="T84">ас ′удʼанаш, а kаим ′амгу?</ta>
            <ta e="T93" id="Seg_913" s="T89">ме витʼ ′нагуртаф(у)тъ ′jеwофтъ.</ta>
            <ta e="T98" id="Seg_914" s="T93">′нагур ′kумдъ ко̄цʼ kомдъ надъ.</ta>
            <ta e="T103" id="Seg_915" s="T98">там′дʼел пен ′тасты ′оɣыl′джожику ′кыгы‵зан.</ta>
            <ta e="T106" id="Seg_916" s="T103">а тан ′сыды′дʼат.</ta>
            <ta e="T110" id="Seg_917" s="T106">ман ′тʼӧ̄тджикан ′тӱw(у)ан ′текга.</ta>
            <ta e="T113" id="Seg_918" s="T110">а тан ′ӱтд̂ъд̂ӓ̄л.</ta>
            <ta e="T117" id="Seg_919" s="T113">а kут′де̨р ′тасты ′оɣуlд̂ж̂ожигу?</ta>
            <ta e="T123" id="Seg_920" s="T117">о′не̨тдъ тʼар′кwат(дъ): ′мазым ′оɣуlджо‵жак, о′нетдъ ′съ(ӓ)дытʼекwат(д̂)тъ.</ta>
            <ta e="T126" id="Seg_921" s="T123">ман ас ′сы(ъ)ды(ъ)тʼуккъж̂ан.</ta>
            <ta e="T129" id="Seg_922" s="T126">ман крʼепкан ′kоннеджан.</ta>
            <ta e="T131" id="Seg_923" s="T129">сира′вно ′сыды′тʼаннаш.</ta>
            <ta e="T136" id="Seg_924" s="T131">ман таб′бе̨н ′текг̂а ай ′мӱдъ‵кеджан.</ta>
            <ta e="T141" id="Seg_925" s="T136">ман ′тʼекка сы′сари ӓ̄ш ′кеттше′дан.</ta>
            <ta e="T143" id="Seg_926" s="T141">′оɣуlджожак со̄дʼиган.</ta>
            <ta e="T146" id="Seg_927" s="T143">а то ме′нан б̂ис′кwе̨джаттъ.</ta>
            <ta e="T149" id="Seg_928" s="T146">kай нӱпла jес.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T10" id="Seg_929" s="T1">a tan kutdär kostal što man süsögulʼder koluppan?</ta>
            <ta e="T18" id="Seg_930" s="T10">tan man seu wes tunal i kolomaːt soːdʼigan.</ta>
            <ta e="T23" id="Seg_931" s="T18">a Нina ass tunun kulupbugu.</ta>
            <ta e="T29" id="Seg_932" s="T23">nu, qwallaj aːmnetǯaj, natʼen i kulumeǯaj.</ta>
            <ta e="T32" id="Seg_933" s="T29">wes äʒlam kettšaj.</ta>
            <ta e="T39" id="Seg_934" s="T32">man ass tunnou, qusakɨn maltšɨd̂ʼetʒan alʼi ass.</ta>
            <ta e="T43" id="Seg_935" s="T39">majɣən wes maltšeǯaj äʒlam.</ta>
            <ta e="T47" id="Seg_936" s="T43">man patom jedot qwaǯan.</ta>
            <ta e="T50" id="Seg_937" s="T47">i tannä qwannaš.</ta>
            <ta e="T54" id="Seg_938" s="T50">köse tšaʒəlʼe mʼekga udurukok.</ta>
            <ta e="T57" id="Seg_939" s="T54">man qwɛlla qwattšed̂an.</ta>
            <ta e="T62" id="Seg_940" s="T57">a tan udurukok qwɛlɨm amgu.</ta>
            <ta e="T65" id="Seg_941" s="T62">qajze qazaxsen qulummat?</ta>
            <ta e="T69" id="Seg_942" s="T65">matdarʼe nadə quluppugu.</ta>
            <ta e="T73" id="Seg_943" s="T69">a tan qulumatd̂ə süsögusen?</ta>
            <ta e="T78" id="Seg_944" s="T73">oqqɨr täbequm süsögusen soːdʼigan qulupb̂as.</ta>
            <ta e="T82" id="Seg_945" s="T78">a sičʼas qurolǯeǯan udʼigu.</ta>
            <ta e="T84" id="Seg_946" s="T82">udʼigu nadə.</ta>
            <ta e="T89" id="Seg_947" s="T84">as udʼanaš, a qaim amgu?</ta>
            <ta e="T93" id="Seg_948" s="T89">me witʼ nagurtaf(u)tə jewoftə.</ta>
            <ta e="T98" id="Seg_949" s="T93">nagur qumdə koːcʼ qomdə nadə.</ta>
            <ta e="T103" id="Seg_950" s="T98">tamdʼel pen tastɨ oɣɨlǯoʒiku kɨgɨzan.</ta>
            <ta e="T106" id="Seg_951" s="T103">a tan sɨdɨdʼat.</ta>
            <ta e="T110" id="Seg_952" s="T106">man tʼöːtǯikan tüw(u)an tekga.</ta>
            <ta e="T113" id="Seg_953" s="T110">a tan ütd̂əd̂äːl.</ta>
            <ta e="T117" id="Seg_954" s="T113">a qutder tastɨ oɣuld̂ʒ̂oʒigu?</ta>
            <ta e="T123" id="Seg_955" s="T117">onetdə tʼarkwat(də): mazɨm oɣulǯoʒak, onetdə sə(ä)dɨtʼekwat(d̂)tə.</ta>
            <ta e="T126" id="Seg_956" s="T123">man as sɨ(ə)dɨ(ə)tʼukkəʒ̂an.</ta>
            <ta e="T129" id="Seg_957" s="T126">man krʼepkan qonneǯan.</ta>
            <ta e="T131" id="Seg_958" s="T129">sirawno sɨdɨtʼannaš.</ta>
            <ta e="T136" id="Seg_959" s="T131">man tabben tekĝa aj müdəkeǯan.</ta>
            <ta e="T141" id="Seg_960" s="T136">man tʼekka sɨsari äːš kettšedan.</ta>
            <ta e="T143" id="Seg_961" s="T141">oɣulǯoʒak soːdʼigan.</ta>
            <ta e="T146" id="Seg_962" s="T143">a to menan b̂iskweǯattə.</ta>
            <ta e="T149" id="Seg_963" s="T146">qaj nüpla jes.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T10" id="Seg_964" s="T1">A tan kutdär kostal što man süsögulʼ der koluppan? </ta>
            <ta e="T18" id="Seg_965" s="T10">Tan man seu wes tunal i kolomaːt soːdʼigan. </ta>
            <ta e="T23" id="Seg_966" s="T18">A Nina ass tunun kulupbugu. </ta>
            <ta e="T29" id="Seg_967" s="T23">Nu, qwallaj aːmnetǯaj, natʼen i kulumeǯaj. </ta>
            <ta e="T32" id="Seg_968" s="T29">Wes äʒlam ketčaj. </ta>
            <ta e="T39" id="Seg_969" s="T32">Man ass tunnou, qusakɨn malčɨdʼetʒan alʼi ass. </ta>
            <ta e="T43" id="Seg_970" s="T39">Majɣən wes malčeǯaj äʒlam. </ta>
            <ta e="T47" id="Seg_971" s="T43">Man patom jedot qwaǯan. </ta>
            <ta e="T50" id="Seg_972" s="T47">I tannä qwannaš. </ta>
            <ta e="T54" id="Seg_973" s="T50">Köse čaʒəlʼe mʼekga udurukok. </ta>
            <ta e="T57" id="Seg_974" s="T54">Man qwɛlla qwatčedan. </ta>
            <ta e="T62" id="Seg_975" s="T57">A tan udurukok qwɛlɨm amgu. </ta>
            <ta e="T65" id="Seg_976" s="T62">Qajze qazaxsen qulummat? </ta>
            <ta e="T69" id="Seg_977" s="T65">Mat darʼe nadə quluppugu. </ta>
            <ta e="T73" id="Seg_978" s="T69">A tan qulumatdə süsögusen? </ta>
            <ta e="T78" id="Seg_979" s="T73">Oqqɨr täbequm süsögusen soːdʼigan qulupbas. </ta>
            <ta e="T82" id="Seg_980" s="T78">A sičʼas qurolǯeǯan udʼigu. </ta>
            <ta e="T84" id="Seg_981" s="T82">Udʼigu nadə. </ta>
            <ta e="T89" id="Seg_982" s="T84">As udʼanaš, a qaim amgu? </ta>
            <ta e="T93" id="Seg_983" s="T89">Me witʼ nagurtaftə jewoftə. </ta>
            <ta e="T98" id="Seg_984" s="T93">Nagur qumdə koːcʼ qomdə nadə. </ta>
            <ta e="T103" id="Seg_985" s="T98">Tamdʼel pen tastɨ oɣɨlǯoʒiku kɨgɨzan. </ta>
            <ta e="T106" id="Seg_986" s="T103">A tan sɨdɨdʼat. </ta>
            <ta e="T110" id="Seg_987" s="T106">Man tʼöːtǯikan tüwan tekga. </ta>
            <ta e="T113" id="Seg_988" s="T110">A tan ütdədäːl. </ta>
            <ta e="T117" id="Seg_989" s="T113">A qutder tastɨ oɣulǯoʒigu? </ta>
            <ta e="T123" id="Seg_990" s="T117">Onetdə tʼarkwat: “Mazɨm oɣulǯoʒak,” – onetdə sədɨtʼekwattə. </ta>
            <ta e="T126" id="Seg_991" s="T123">“Man as sɨdɨtʼukkəʒan. </ta>
            <ta e="T129" id="Seg_992" s="T126">Man krʼepkan qonneǯan.” </ta>
            <ta e="T131" id="Seg_993" s="T129">Sirawno sɨdɨtʼannaš. </ta>
            <ta e="T136" id="Seg_994" s="T131">Man tabben tekga aj müdəkeǯan. </ta>
            <ta e="T141" id="Seg_995" s="T136">Man tʼekka sɨsari äːš ketčedan. </ta>
            <ta e="T143" id="Seg_996" s="T141">Oɣulǯoʒak soːdʼigan. </ta>
            <ta e="T146" id="Seg_997" s="T143">Ato menan biskweǯattə. </ta>
            <ta e="T149" id="Seg_998" s="T146">Qaj nüpla jes. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_999" s="T1">a</ta>
            <ta e="T3" id="Seg_1000" s="T2">tat</ta>
            <ta e="T4" id="Seg_1001" s="T3">kutdär</ta>
            <ta e="T5" id="Seg_1002" s="T4">kosta-l</ta>
            <ta e="T6" id="Seg_1003" s="T5">što</ta>
            <ta e="T7" id="Seg_1004" s="T6">man</ta>
            <ta e="T8" id="Seg_1005" s="T7">süsögu-lʼ</ta>
            <ta e="T9" id="Seg_1006" s="T8">der</ta>
            <ta e="T10" id="Seg_1007" s="T9">koluppa-n</ta>
            <ta e="T11" id="Seg_1008" s="T10">tat</ta>
            <ta e="T12" id="Seg_1009" s="T11">man</ta>
            <ta e="T13" id="Seg_1010" s="T12">se-u</ta>
            <ta e="T14" id="Seg_1011" s="T13">wes</ta>
            <ta e="T15" id="Seg_1012" s="T14">tuna-l</ta>
            <ta e="T16" id="Seg_1013" s="T15">i</ta>
            <ta e="T17" id="Seg_1014" s="T16">kolomaː-t</ta>
            <ta e="T18" id="Seg_1015" s="T17">soːdʼiga-n</ta>
            <ta e="T19" id="Seg_1016" s="T18">a</ta>
            <ta e="T20" id="Seg_1017" s="T19">Nina</ta>
            <ta e="T21" id="Seg_1018" s="T20">ass</ta>
            <ta e="T22" id="Seg_1019" s="T21">tunu-n</ta>
            <ta e="T23" id="Seg_1020" s="T22">kulupbu-gu</ta>
            <ta e="T24" id="Seg_1021" s="T23">nu</ta>
            <ta e="T25" id="Seg_1022" s="T24">qwal-la-j</ta>
            <ta e="T26" id="Seg_1023" s="T25">aːmn-etǯa-j</ta>
            <ta e="T27" id="Seg_1024" s="T26">natʼe-n</ta>
            <ta e="T28" id="Seg_1025" s="T27">i</ta>
            <ta e="T29" id="Seg_1026" s="T28">kulum-eǯa-j</ta>
            <ta e="T30" id="Seg_1027" s="T29">wes</ta>
            <ta e="T31" id="Seg_1028" s="T30">äʒ-la-m</ta>
            <ta e="T32" id="Seg_1029" s="T31">ket-ča-j</ta>
            <ta e="T33" id="Seg_1030" s="T32">man</ta>
            <ta e="T34" id="Seg_1031" s="T33">ass</ta>
            <ta e="T35" id="Seg_1032" s="T34">tunno-u</ta>
            <ta e="T36" id="Seg_1033" s="T35">qusakɨn</ta>
            <ta e="T37" id="Seg_1034" s="T36">malčɨ-dʼ-etʒa-n</ta>
            <ta e="T38" id="Seg_1035" s="T37">alʼi</ta>
            <ta e="T39" id="Seg_1036" s="T38">ass</ta>
            <ta e="T40" id="Seg_1037" s="T39">maj-ɣən</ta>
            <ta e="T41" id="Seg_1038" s="T40">wes</ta>
            <ta e="T42" id="Seg_1039" s="T41">malč-eǯa-j</ta>
            <ta e="T43" id="Seg_1040" s="T42">äʒ-la-m</ta>
            <ta e="T44" id="Seg_1041" s="T43">man</ta>
            <ta e="T45" id="Seg_1042" s="T44">patom</ta>
            <ta e="T46" id="Seg_1043" s="T45">jedo-t</ta>
            <ta e="T47" id="Seg_1044" s="T46">qwa-ǯa-n</ta>
            <ta e="T48" id="Seg_1045" s="T47">i</ta>
            <ta e="T49" id="Seg_1046" s="T48">tan-nä</ta>
            <ta e="T50" id="Seg_1047" s="T49">qwan-naš</ta>
            <ta e="T51" id="Seg_1048" s="T50">köse</ta>
            <ta e="T52" id="Seg_1049" s="T51">čaʒə-lʼe</ta>
            <ta e="T53" id="Seg_1050" s="T52">mʼekga</ta>
            <ta e="T54" id="Seg_1051" s="T53">udur-u-ko-k</ta>
            <ta e="T55" id="Seg_1052" s="T54">man</ta>
            <ta e="T56" id="Seg_1053" s="T55">qwɛl-la</ta>
            <ta e="T57" id="Seg_1054" s="T56">qwat-č-eda-n</ta>
            <ta e="T58" id="Seg_1055" s="T57">a</ta>
            <ta e="T59" id="Seg_1056" s="T58">tat</ta>
            <ta e="T60" id="Seg_1057" s="T59">udur-u-ko-k</ta>
            <ta e="T61" id="Seg_1058" s="T60">qwɛl-ɨ-m</ta>
            <ta e="T62" id="Seg_1059" s="T61">am-gu</ta>
            <ta e="T63" id="Seg_1060" s="T62">qaj-ze</ta>
            <ta e="T64" id="Seg_1061" s="T63">qazax-se-n</ta>
            <ta e="T65" id="Seg_1062" s="T64">qulumma-t</ta>
            <ta e="T66" id="Seg_1063" s="T65">mat</ta>
            <ta e="T67" id="Seg_1064" s="T66">darʼe</ta>
            <ta e="T68" id="Seg_1065" s="T67">nadə</ta>
            <ta e="T69" id="Seg_1066" s="T68">quluppu-gu</ta>
            <ta e="T70" id="Seg_1067" s="T69">a</ta>
            <ta e="T71" id="Seg_1068" s="T70">tat</ta>
            <ta e="T72" id="Seg_1069" s="T71">quluma-tdə</ta>
            <ta e="T73" id="Seg_1070" s="T72">süsögu-se-n</ta>
            <ta e="T74" id="Seg_1071" s="T73">oqqɨr</ta>
            <ta e="T75" id="Seg_1072" s="T74">täbe-qum</ta>
            <ta e="T76" id="Seg_1073" s="T75">süsögu-se-n</ta>
            <ta e="T77" id="Seg_1074" s="T76">soːdʼiga-n</ta>
            <ta e="T78" id="Seg_1075" s="T77">qulupba-s</ta>
            <ta e="T79" id="Seg_1076" s="T78">a</ta>
            <ta e="T80" id="Seg_1077" s="T79">sičʼas</ta>
            <ta e="T81" id="Seg_1078" s="T80">qur-ol-ǯ-eǯa-n</ta>
            <ta e="T82" id="Seg_1079" s="T81">udʼi-gu</ta>
            <ta e="T83" id="Seg_1080" s="T82">udʼi-gu</ta>
            <ta e="T84" id="Seg_1081" s="T83">nadə</ta>
            <ta e="T85" id="Seg_1082" s="T84">as</ta>
            <ta e="T86" id="Seg_1083" s="T85">udʼa-naš</ta>
            <ta e="T87" id="Seg_1084" s="T86">a</ta>
            <ta e="T88" id="Seg_1085" s="T87">qai-m</ta>
            <ta e="T89" id="Seg_1086" s="T88">am-gu</ta>
            <ta e="T90" id="Seg_1087" s="T89">me</ta>
            <ta e="T91" id="Seg_1088" s="T90">witʼ</ta>
            <ta e="T92" id="Seg_1089" s="T91">nagur-ta-ftə</ta>
            <ta e="T93" id="Seg_1090" s="T92">je-wo-ftə</ta>
            <ta e="T94" id="Seg_1091" s="T93">nagur</ta>
            <ta e="T95" id="Seg_1092" s="T94">qum-də</ta>
            <ta e="T96" id="Seg_1093" s="T95">koːcʼ</ta>
            <ta e="T97" id="Seg_1094" s="T96">qomdə</ta>
            <ta e="T98" id="Seg_1095" s="T97">nadə</ta>
            <ta e="T99" id="Seg_1096" s="T98">tam-dʼel</ta>
            <ta e="T100" id="Seg_1097" s="T99">pe-n</ta>
            <ta e="T101" id="Seg_1098" s="T100">tastɨ</ta>
            <ta e="T102" id="Seg_1099" s="T101">oɣɨlǯo-ʒi-ku</ta>
            <ta e="T103" id="Seg_1100" s="T102">kɨgɨ-za-n</ta>
            <ta e="T104" id="Seg_1101" s="T103">a</ta>
            <ta e="T105" id="Seg_1102" s="T104">tat</ta>
            <ta e="T106" id="Seg_1103" s="T105">sɨdɨ-dʼa-t</ta>
            <ta e="T107" id="Seg_1104" s="T106">man</ta>
            <ta e="T108" id="Seg_1105" s="T107">tʼöːtǯikan</ta>
            <ta e="T109" id="Seg_1106" s="T108">tü-wa-n</ta>
            <ta e="T110" id="Seg_1107" s="T109">tekga</ta>
            <ta e="T111" id="Seg_1108" s="T110">a</ta>
            <ta e="T112" id="Seg_1109" s="T111">tat</ta>
            <ta e="T113" id="Seg_1110" s="T112">ütdə-däː-l</ta>
            <ta e="T114" id="Seg_1111" s="T113">a</ta>
            <ta e="T115" id="Seg_1112" s="T114">qutder</ta>
            <ta e="T116" id="Seg_1113" s="T115">tastɨ</ta>
            <ta e="T117" id="Seg_1114" s="T116">oɣulǯo-ʒi-gu</ta>
            <ta e="T118" id="Seg_1115" s="T117">onetdə</ta>
            <ta e="T119" id="Seg_1116" s="T118">tʼar-k-wa-t</ta>
            <ta e="T120" id="Seg_1117" s="T119">mazɨm</ta>
            <ta e="T121" id="Seg_1118" s="T120">oɣulǯo-ʒ-ak</ta>
            <ta e="T122" id="Seg_1119" s="T121">onetdə</ta>
            <ta e="T123" id="Seg_1120" s="T122">sədɨ-tʼe-k-wa-ttə</ta>
            <ta e="T124" id="Seg_1121" s="T123">man</ta>
            <ta e="T125" id="Seg_1122" s="T124">as</ta>
            <ta e="T126" id="Seg_1123" s="T125">sɨdɨ-tʼu-kk-əʒa-n</ta>
            <ta e="T127" id="Seg_1124" s="T126">man</ta>
            <ta e="T128" id="Seg_1125" s="T127">krʼepka-n</ta>
            <ta e="T129" id="Seg_1126" s="T128">qonn-eǯa-n</ta>
            <ta e="T130" id="Seg_1127" s="T129">sirawno</ta>
            <ta e="T131" id="Seg_1128" s="T130">sɨdɨ-tʼa-nnaš</ta>
            <ta e="T132" id="Seg_1129" s="T131">man</ta>
            <ta e="T133" id="Seg_1130" s="T132">tab-be-n</ta>
            <ta e="T134" id="Seg_1131" s="T133">tekga</ta>
            <ta e="T135" id="Seg_1132" s="T134">aj</ta>
            <ta e="T136" id="Seg_1133" s="T135">müdək-eǯa-n</ta>
            <ta e="T137" id="Seg_1134" s="T136">man</ta>
            <ta e="T138" id="Seg_1135" s="T137">tʼekka</ta>
            <ta e="T139" id="Seg_1136" s="T138">sɨsari</ta>
            <ta e="T140" id="Seg_1137" s="T139">äːš</ta>
            <ta e="T141" id="Seg_1138" s="T140">ket-č-eda-n</ta>
            <ta e="T142" id="Seg_1139" s="T141">oɣulǯo-ʒ-ak</ta>
            <ta e="T143" id="Seg_1140" s="T142">soːdʼiga-n</ta>
            <ta e="T144" id="Seg_1141" s="T143">ato</ta>
            <ta e="T145" id="Seg_1142" s="T144">me-nan</ta>
            <ta e="T146" id="Seg_1143" s="T145">bis-kw-eǯa-ttə</ta>
            <ta e="T147" id="Seg_1144" s="T146">qaj</ta>
            <ta e="T148" id="Seg_1145" s="T147">nüpla</ta>
            <ta e="T149" id="Seg_1146" s="T148">je-s</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1147" s="T1">a</ta>
            <ta e="T3" id="Seg_1148" s="T2">tan</ta>
            <ta e="T4" id="Seg_1149" s="T3">qundar</ta>
            <ta e="T5" id="Seg_1150" s="T4">kostɨ-l</ta>
            <ta e="T6" id="Seg_1151" s="T5">što</ta>
            <ta e="T7" id="Seg_1152" s="T6">man</ta>
            <ta e="T8" id="Seg_1153" s="T7">süsögum-lʼ</ta>
            <ta e="T9" id="Seg_1154" s="T8">dʼar</ta>
            <ta e="T10" id="Seg_1155" s="T9">kulubu-ŋ</ta>
            <ta e="T11" id="Seg_1156" s="T10">tan</ta>
            <ta e="T12" id="Seg_1157" s="T11">man</ta>
            <ta e="T13" id="Seg_1158" s="T12">se-w</ta>
            <ta e="T14" id="Seg_1159" s="T13">wesʼ</ta>
            <ta e="T15" id="Seg_1160" s="T14">tonu-l</ta>
            <ta e="T16" id="Seg_1161" s="T15">i</ta>
            <ta e="T17" id="Seg_1162" s="T16">kulubu-ntə</ta>
            <ta e="T18" id="Seg_1163" s="T17">soːdʼiga-ŋ</ta>
            <ta e="T19" id="Seg_1164" s="T18">a</ta>
            <ta e="T20" id="Seg_1165" s="T19">Nina</ta>
            <ta e="T21" id="Seg_1166" s="T20">asa</ta>
            <ta e="T22" id="Seg_1167" s="T21">tonu-n</ta>
            <ta e="T23" id="Seg_1168" s="T22">kulubu-gu</ta>
            <ta e="T24" id="Seg_1169" s="T23">nu</ta>
            <ta e="T25" id="Seg_1170" s="T24">qwan-lä-j</ta>
            <ta e="T26" id="Seg_1171" s="T25">amdɨ-enǯɨ-j</ta>
            <ta e="T27" id="Seg_1172" s="T26">*natʼe-n</ta>
            <ta e="T28" id="Seg_1173" s="T27">i</ta>
            <ta e="T29" id="Seg_1174" s="T28">kulubu-enǯɨ-j</ta>
            <ta e="T30" id="Seg_1175" s="T29">wesʼ</ta>
            <ta e="T31" id="Seg_1176" s="T30">əǯə-la-m</ta>
            <ta e="T32" id="Seg_1177" s="T31">ket-enǯɨ-j</ta>
            <ta e="T33" id="Seg_1178" s="T32">man</ta>
            <ta e="T34" id="Seg_1179" s="T33">asa</ta>
            <ta e="T35" id="Seg_1180" s="T34">tonu-w</ta>
            <ta e="T36" id="Seg_1181" s="T35">qussaqɨn</ta>
            <ta e="T37" id="Seg_1182" s="T36">malčə-dʼi-enǯɨ-n</ta>
            <ta e="T38" id="Seg_1183" s="T37">alʼi</ta>
            <ta e="T39" id="Seg_1184" s="T38">asa</ta>
            <ta e="T40" id="Seg_1185" s="T39">maj-qɨn</ta>
            <ta e="T41" id="Seg_1186" s="T40">wesʼ</ta>
            <ta e="T42" id="Seg_1187" s="T41">malčə-enǯɨ-j</ta>
            <ta e="T43" id="Seg_1188" s="T42">əǯə-la-m</ta>
            <ta e="T44" id="Seg_1189" s="T43">man</ta>
            <ta e="T45" id="Seg_1190" s="T44">patom</ta>
            <ta e="T46" id="Seg_1191" s="T45">eːde-ntə</ta>
            <ta e="T47" id="Seg_1192" s="T46">qwan-enǯɨ-ŋ</ta>
            <ta e="T48" id="Seg_1193" s="T47">i</ta>
            <ta e="T49" id="Seg_1194" s="T48">tan-näj</ta>
            <ta e="T50" id="Seg_1195" s="T49">qwan-naš</ta>
            <ta e="T51" id="Seg_1196" s="T50">kössə</ta>
            <ta e="T52" id="Seg_1197" s="T51">čaǯɨ-le</ta>
            <ta e="T53" id="Seg_1198" s="T52">mekka</ta>
            <ta e="T54" id="Seg_1199" s="T53">udɨr-ɨ-ku-kɨ</ta>
            <ta e="T55" id="Seg_1200" s="T54">man</ta>
            <ta e="T56" id="Seg_1201" s="T55">qwɛl-la</ta>
            <ta e="T57" id="Seg_1202" s="T56">qwat-ču-enǯɨ-ŋ</ta>
            <ta e="T58" id="Seg_1203" s="T57">a</ta>
            <ta e="T59" id="Seg_1204" s="T58">tan</ta>
            <ta e="T60" id="Seg_1205" s="T59">udɨr-ɨ-ku-kɨ</ta>
            <ta e="T61" id="Seg_1206" s="T60">qwɛl-ɨ-m</ta>
            <ta e="T62" id="Seg_1207" s="T61">am-gu</ta>
            <ta e="T63" id="Seg_1208" s="T62">qaj-se</ta>
            <ta e="T64" id="Seg_1209" s="T63">qazaq-se-ŋ</ta>
            <ta e="T65" id="Seg_1210" s="T64">kulubu-ntə</ta>
            <ta e="T66" id="Seg_1211" s="T65">man</ta>
            <ta e="T67" id="Seg_1212" s="T66">dari</ta>
            <ta e="T68" id="Seg_1213" s="T67">nadə</ta>
            <ta e="T69" id="Seg_1214" s="T68">kulubu-gu</ta>
            <ta e="T70" id="Seg_1215" s="T69">a</ta>
            <ta e="T71" id="Seg_1216" s="T70">tan</ta>
            <ta e="T72" id="Seg_1217" s="T71">kulubu-ntə</ta>
            <ta e="T73" id="Seg_1218" s="T72">süsögum-se-ŋ</ta>
            <ta e="T74" id="Seg_1219" s="T73">okkɨr</ta>
            <ta e="T75" id="Seg_1220" s="T74">täbe-qum</ta>
            <ta e="T76" id="Seg_1221" s="T75">süsögum-se-ŋ</ta>
            <ta e="T77" id="Seg_1222" s="T76">soːdʼiga-ŋ</ta>
            <ta e="T78" id="Seg_1223" s="T77">kulubu-sɨ</ta>
            <ta e="T79" id="Seg_1224" s="T78">a</ta>
            <ta e="T80" id="Seg_1225" s="T79">sičas</ta>
            <ta e="T81" id="Seg_1226" s="T80">kur-ol-ǯə-enǯɨ-ŋ</ta>
            <ta e="T82" id="Seg_1227" s="T81">uːdʼi-gu</ta>
            <ta e="T83" id="Seg_1228" s="T82">uːdʼi-gu</ta>
            <ta e="T84" id="Seg_1229" s="T83">nadə</ta>
            <ta e="T85" id="Seg_1230" s="T84">asa</ta>
            <ta e="T86" id="Seg_1231" s="T85">uːdʼi-naš</ta>
            <ta e="T87" id="Seg_1232" s="T86">a</ta>
            <ta e="T88" id="Seg_1233" s="T87">qaj-m</ta>
            <ta e="T89" id="Seg_1234" s="T88">am-gu</ta>
            <ta e="T90" id="Seg_1235" s="T89">me</ta>
            <ta e="T91" id="Seg_1236" s="T90">wetʼ</ta>
            <ta e="T92" id="Seg_1237" s="T91">nagur-ta-un</ta>
            <ta e="T93" id="Seg_1238" s="T92">eː-nɨ-un</ta>
            <ta e="T94" id="Seg_1239" s="T93">nagur</ta>
            <ta e="T95" id="Seg_1240" s="T94">qum-ntə</ta>
            <ta e="T96" id="Seg_1241" s="T95">koːci</ta>
            <ta e="T97" id="Seg_1242" s="T96">qomdɛ</ta>
            <ta e="T98" id="Seg_1243" s="T97">nadə</ta>
            <ta e="T99" id="Seg_1244" s="T98">taw-dʼel</ta>
            <ta e="T100" id="Seg_1245" s="T99">pi-n</ta>
            <ta e="T101" id="Seg_1246" s="T100">tastɨ</ta>
            <ta e="T102" id="Seg_1247" s="T101">oɣulǯɨ-ʒu-gu</ta>
            <ta e="T103" id="Seg_1248" s="T102">kɨgɨ-sɨ-ŋ</ta>
            <ta e="T104" id="Seg_1249" s="T103">a</ta>
            <ta e="T105" id="Seg_1250" s="T104">tan</ta>
            <ta e="T106" id="Seg_1251" s="T105">söde-či-ntə</ta>
            <ta e="T107" id="Seg_1252" s="T106">man</ta>
            <ta e="T108" id="Seg_1253" s="T107">tʼötʒɨkaŋ</ta>
            <ta e="T109" id="Seg_1254" s="T108">tüː-nɨ-ŋ</ta>
            <ta e="T110" id="Seg_1255" s="T109">tekka</ta>
            <ta e="T111" id="Seg_1256" s="T110">a</ta>
            <ta e="T112" id="Seg_1257" s="T111">tan</ta>
            <ta e="T113" id="Seg_1258" s="T112">ündɨ-dʼi-l</ta>
            <ta e="T114" id="Seg_1259" s="T113">a</ta>
            <ta e="T115" id="Seg_1260" s="T114">qundar</ta>
            <ta e="T116" id="Seg_1261" s="T115">tastɨ</ta>
            <ta e="T117" id="Seg_1262" s="T116">oɣulǯɨ-ʒu-gu</ta>
            <ta e="T118" id="Seg_1263" s="T117">onendǝ</ta>
            <ta e="T119" id="Seg_1264" s="T118">tʼärɨ-ku-nɨ-ntə</ta>
            <ta e="T120" id="Seg_1265" s="T119">mazɨm</ta>
            <ta e="T121" id="Seg_1266" s="T120">oɣulǯɨ-ʒu-kɨ</ta>
            <ta e="T122" id="Seg_1267" s="T121">onendǝ</ta>
            <ta e="T123" id="Seg_1268" s="T122">söde-či-ku-nɨ-ntə</ta>
            <ta e="T124" id="Seg_1269" s="T123">man</ta>
            <ta e="T125" id="Seg_1270" s="T124">asa</ta>
            <ta e="T126" id="Seg_1271" s="T125">söde-či-ku-enǯɨ-ŋ</ta>
            <ta e="T127" id="Seg_1272" s="T126">man</ta>
            <ta e="T128" id="Seg_1273" s="T127">krepka-ŋ</ta>
            <ta e="T129" id="Seg_1274" s="T128">qondu-enǯɨ-ŋ</ta>
            <ta e="T130" id="Seg_1275" s="T129">sirawno</ta>
            <ta e="T131" id="Seg_1276" s="T130">söde-či-naš</ta>
            <ta e="T132" id="Seg_1277" s="T131">man</ta>
            <ta e="T133" id="Seg_1278" s="T132">taw-pi-ŋ</ta>
            <ta e="T134" id="Seg_1279" s="T133">tekka</ta>
            <ta e="T135" id="Seg_1280" s="T134">aj</ta>
            <ta e="T136" id="Seg_1281" s="T135">müdək-enǯɨ-ŋ</ta>
            <ta e="T137" id="Seg_1282" s="T136">man</ta>
            <ta e="T138" id="Seg_1283" s="T137">tekka</ta>
            <ta e="T139" id="Seg_1284" s="T138">sɨsari</ta>
            <ta e="T140" id="Seg_1285" s="T139">əǯə</ta>
            <ta e="T141" id="Seg_1286" s="T140">ket-ču-enǯɨ-ŋ</ta>
            <ta e="T142" id="Seg_1287" s="T141">oɣulǯɨ-ʒu-kɨ</ta>
            <ta e="T143" id="Seg_1288" s="T142">soːdʼiga-ŋ</ta>
            <ta e="T144" id="Seg_1289" s="T143">ato</ta>
            <ta e="T145" id="Seg_1290" s="T144">me-nan</ta>
            <ta e="T146" id="Seg_1291" s="T145">pissɨ-ku-enǯɨ-tɨn</ta>
            <ta e="T147" id="Seg_1292" s="T146">qaj</ta>
            <ta e="T148" id="Seg_1293" s="T147">nʼöpla</ta>
            <ta e="T149" id="Seg_1294" s="T148">eː-sɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1295" s="T1">and</ta>
            <ta e="T3" id="Seg_1296" s="T2">you.SG.NOM</ta>
            <ta e="T4" id="Seg_1297" s="T3">how</ta>
            <ta e="T5" id="Seg_1298" s="T4">get.to.know-2SG.O</ta>
            <ta e="T6" id="Seg_1299" s="T5">that</ta>
            <ta e="T7" id="Seg_1300" s="T6">I.NOM</ta>
            <ta e="T8" id="Seg_1301" s="T7">Selkup-ADJZ</ta>
            <ta e="T9" id="Seg_1302" s="T8">(on)</ta>
            <ta e="T10" id="Seg_1303" s="T9">speak-1SG.S</ta>
            <ta e="T11" id="Seg_1304" s="T10">you.SG.NOM</ta>
            <ta e="T12" id="Seg_1305" s="T11">I.GEN</ta>
            <ta e="T13" id="Seg_1306" s="T12">language.[NOM]-1SG</ta>
            <ta e="T14" id="Seg_1307" s="T13">all</ta>
            <ta e="T15" id="Seg_1308" s="T14">know-2SG.O</ta>
            <ta e="T16" id="Seg_1309" s="T15">and</ta>
            <ta e="T17" id="Seg_1310" s="T16">speak-2SG.S</ta>
            <ta e="T18" id="Seg_1311" s="T17">good-ADVZ</ta>
            <ta e="T19" id="Seg_1312" s="T18">and</ta>
            <ta e="T20" id="Seg_1313" s="T19">Nina.[NOM]</ta>
            <ta e="T21" id="Seg_1314" s="T20">NEG</ta>
            <ta e="T22" id="Seg_1315" s="T21">know-3SG.S</ta>
            <ta e="T23" id="Seg_1316" s="T22">speak-INF</ta>
            <ta e="T24" id="Seg_1317" s="T23">now</ta>
            <ta e="T25" id="Seg_1318" s="T24">leave-OPT-1DU</ta>
            <ta e="T26" id="Seg_1319" s="T25">sit-FUT-1DU</ta>
            <ta e="T27" id="Seg_1320" s="T26">there-ADV.LOC</ta>
            <ta e="T28" id="Seg_1321" s="T27">and</ta>
            <ta e="T29" id="Seg_1322" s="T28">speak-FUT-1DU</ta>
            <ta e="T30" id="Seg_1323" s="T29">all</ta>
            <ta e="T31" id="Seg_1324" s="T30">word-PL-ACC</ta>
            <ta e="T32" id="Seg_1325" s="T31">say-FUT-1DU</ta>
            <ta e="T33" id="Seg_1326" s="T32">I.NOM</ta>
            <ta e="T34" id="Seg_1327" s="T33">NEG</ta>
            <ta e="T35" id="Seg_1328" s="T34">know-1SG.O</ta>
            <ta e="T36" id="Seg_1329" s="T35">when</ta>
            <ta e="T37" id="Seg_1330" s="T36">stop-RFL-FUT-3SG.S</ta>
            <ta e="T38" id="Seg_1331" s="T37">or</ta>
            <ta e="T39" id="Seg_1332" s="T38">NEG</ta>
            <ta e="T40" id="Seg_1333" s="T39">May-LOC</ta>
            <ta e="T41" id="Seg_1334" s="T40">all</ta>
            <ta e="T42" id="Seg_1335" s="T41">stop-FUT-1DU</ta>
            <ta e="T43" id="Seg_1336" s="T42">word-PL-ACC</ta>
            <ta e="T44" id="Seg_1337" s="T43">I.NOM</ta>
            <ta e="T45" id="Seg_1338" s="T44">then</ta>
            <ta e="T46" id="Seg_1339" s="T45">village-ILL</ta>
            <ta e="T47" id="Seg_1340" s="T46">leave-FUT-1SG.S</ta>
            <ta e="T48" id="Seg_1341" s="T47">and</ta>
            <ta e="T49" id="Seg_1342" s="T48">you.SG.NOM-EMPH</ta>
            <ta e="T50" id="Seg_1343" s="T49">leave-POT.FUT.2SG</ta>
            <ta e="T51" id="Seg_1344" s="T50">backward</ta>
            <ta e="T52" id="Seg_1345" s="T51">go-CVB</ta>
            <ta e="T53" id="Seg_1346" s="T52">I.ALL</ta>
            <ta e="T54" id="Seg_1347" s="T53">stop.in-EP-HAB-IMP.2SG.S</ta>
            <ta e="T55" id="Seg_1348" s="T54">I.NOM</ta>
            <ta e="T56" id="Seg_1349" s="T55">fish-PL.[NOM]</ta>
            <ta e="T57" id="Seg_1350" s="T56">catch-TR-FUT-1SG.S</ta>
            <ta e="T58" id="Seg_1351" s="T57">and</ta>
            <ta e="T59" id="Seg_1352" s="T58">you.SG.NOM</ta>
            <ta e="T60" id="Seg_1353" s="T59">stop.in-EP-HAB-IMP.2SG.S</ta>
            <ta e="T61" id="Seg_1354" s="T60">fish-EP-ACC</ta>
            <ta e="T62" id="Seg_1355" s="T61">eat-INF</ta>
            <ta e="T63" id="Seg_1356" s="T62">what-INSTR</ta>
            <ta e="T64" id="Seg_1357" s="T63">Russian-language-ADVZ</ta>
            <ta e="T65" id="Seg_1358" s="T64">speak-2SG.S</ta>
            <ta e="T66" id="Seg_1359" s="T65">I.GEN</ta>
            <ta e="T67" id="Seg_1360" s="T66">like</ta>
            <ta e="T68" id="Seg_1361" s="T67">one.should</ta>
            <ta e="T69" id="Seg_1362" s="T68">speak-INF</ta>
            <ta e="T70" id="Seg_1363" s="T69">and</ta>
            <ta e="T71" id="Seg_1364" s="T70">you.SG.NOM</ta>
            <ta e="T72" id="Seg_1365" s="T71">speak-2SG.S</ta>
            <ta e="T73" id="Seg_1366" s="T72">Selkup-language-ADVZ</ta>
            <ta e="T74" id="Seg_1367" s="T73">one</ta>
            <ta e="T75" id="Seg_1368" s="T74">man-human.being.[NOM]</ta>
            <ta e="T76" id="Seg_1369" s="T75">Selkup-language-ADVZ</ta>
            <ta e="T77" id="Seg_1370" s="T76">good-ADVZ</ta>
            <ta e="T78" id="Seg_1371" s="T77">speak-PST.[3SG.S]</ta>
            <ta e="T79" id="Seg_1372" s="T78">and</ta>
            <ta e="T80" id="Seg_1373" s="T79">now</ta>
            <ta e="T81" id="Seg_1374" s="T80">run-MOM-DRV-FUT-1SG.S</ta>
            <ta e="T82" id="Seg_1375" s="T81">work-INF</ta>
            <ta e="T83" id="Seg_1376" s="T82">work-INF</ta>
            <ta e="T84" id="Seg_1377" s="T83">one.should</ta>
            <ta e="T85" id="Seg_1378" s="T84">NEG</ta>
            <ta e="T86" id="Seg_1379" s="T85">work-POT.FUT.2SG</ta>
            <ta e="T87" id="Seg_1380" s="T86">and</ta>
            <ta e="T88" id="Seg_1381" s="T87">what-ACC</ta>
            <ta e="T89" id="Seg_1382" s="T88">eat-INF</ta>
            <ta e="T90" id="Seg_1383" s="T89">we.[NOM]</ta>
            <ta e="T91" id="Seg_1384" s="T90">still</ta>
            <ta e="T92" id="Seg_1385" s="T91">three-%%-1PL</ta>
            <ta e="T93" id="Seg_1386" s="T92">be-CO-1PL</ta>
            <ta e="T94" id="Seg_1387" s="T93">three</ta>
            <ta e="T95" id="Seg_1388" s="T94">human.being-ILL</ta>
            <ta e="T96" id="Seg_1389" s="T95">much</ta>
            <ta e="T97" id="Seg_1390" s="T96">money.[NOM]</ta>
            <ta e="T98" id="Seg_1391" s="T97">one.should</ta>
            <ta e="T99" id="Seg_1392" s="T98">this-day.[NOM]</ta>
            <ta e="T100" id="Seg_1393" s="T99">night-ADV.LOC</ta>
            <ta e="T101" id="Seg_1394" s="T100">you.SG.ACC</ta>
            <ta e="T102" id="Seg_1395" s="T101">teach-DRV-INF</ta>
            <ta e="T103" id="Seg_1396" s="T102">want-PST-1SG.S</ta>
            <ta e="T104" id="Seg_1397" s="T103">and</ta>
            <ta e="T105" id="Seg_1398" s="T104">you.SG.NOM</ta>
            <ta e="T106" id="Seg_1399" s="T105">awake-RFL-2SG.S</ta>
            <ta e="T107" id="Seg_1400" s="T106">I.NOM</ta>
            <ta e="T108" id="Seg_1401" s="T107">slowly</ta>
            <ta e="T109" id="Seg_1402" s="T108">come-CO-1SG.S</ta>
            <ta e="T110" id="Seg_1403" s="T109">you.ALL</ta>
            <ta e="T111" id="Seg_1404" s="T110">and</ta>
            <ta e="T112" id="Seg_1405" s="T111">you.SG.NOM</ta>
            <ta e="T113" id="Seg_1406" s="T112">hear-DRV-2SG.O</ta>
            <ta e="T114" id="Seg_1407" s="T113">and</ta>
            <ta e="T115" id="Seg_1408" s="T114">how</ta>
            <ta e="T116" id="Seg_1409" s="T115">you.SG.ACC</ta>
            <ta e="T117" id="Seg_1410" s="T116">teach-DRV-INF</ta>
            <ta e="T118" id="Seg_1411" s="T117">oneself.2SG</ta>
            <ta e="T119" id="Seg_1412" s="T118">say-HAB-CO-2SG.S</ta>
            <ta e="T120" id="Seg_1413" s="T119">I.ACC</ta>
            <ta e="T121" id="Seg_1414" s="T120">teach-DRV-IMP.2SG.S</ta>
            <ta e="T122" id="Seg_1415" s="T121">oneself.2SG</ta>
            <ta e="T123" id="Seg_1416" s="T122">awake-RFL-HAB-CO-2SG.S</ta>
            <ta e="T124" id="Seg_1417" s="T123">I.NOM</ta>
            <ta e="T125" id="Seg_1418" s="T124">NEG</ta>
            <ta e="T126" id="Seg_1419" s="T125">awake-RFL-HAB-FUT-1SG.S</ta>
            <ta e="T127" id="Seg_1420" s="T126">I.NOM</ta>
            <ta e="T128" id="Seg_1421" s="T127">strong-ADVZ</ta>
            <ta e="T129" id="Seg_1422" s="T128">sleep-FUT-1SG.S</ta>
            <ta e="T130" id="Seg_1423" s="T129">anyway</ta>
            <ta e="T131" id="Seg_1424" s="T130">awake-RFL-POT.FUT.2SG</ta>
            <ta e="T132" id="Seg_1425" s="T131">I.NOM</ta>
            <ta e="T133" id="Seg_1426" s="T132">this-night-ADVZ</ta>
            <ta e="T134" id="Seg_1427" s="T133">you.ALL</ta>
            <ta e="T135" id="Seg_1428" s="T134">again</ta>
            <ta e="T136" id="Seg_1429" s="T135">%%-FUT-1SG.S</ta>
            <ta e="T137" id="Seg_1430" s="T136">I.NOM</ta>
            <ta e="T138" id="Seg_1431" s="T137">you.ALL</ta>
            <ta e="T139" id="Seg_1432" s="T138">twenty</ta>
            <ta e="T140" id="Seg_1433" s="T139">word.[NOM]</ta>
            <ta e="T141" id="Seg_1434" s="T140">say-TR-FUT-1SG.S</ta>
            <ta e="T142" id="Seg_1435" s="T141">teach-DRV-IMP.2SG.S</ta>
            <ta e="T143" id="Seg_1436" s="T142">good-ADVZ</ta>
            <ta e="T144" id="Seg_1437" s="T143">otherwise</ta>
            <ta e="T145" id="Seg_1438" s="T144">we-ADES</ta>
            <ta e="T146" id="Seg_1439" s="T145">laugh-HAB-FUT-3PL</ta>
            <ta e="T147" id="Seg_1440" s="T146">what</ta>
            <ta e="T148" id="Seg_1441" s="T147">%very.bad</ta>
            <ta e="T149" id="Seg_1442" s="T148">be-PST.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1443" s="T1">а</ta>
            <ta e="T3" id="Seg_1444" s="T2">ты.NOM</ta>
            <ta e="T4" id="Seg_1445" s="T3">как</ta>
            <ta e="T5" id="Seg_1446" s="T4">узнать-2SG.O</ta>
            <ta e="T6" id="Seg_1447" s="T5">что</ta>
            <ta e="T7" id="Seg_1448" s="T6">я.NOM</ta>
            <ta e="T8" id="Seg_1449" s="T7">селькуп-ADJZ</ta>
            <ta e="T9" id="Seg_1450" s="T8">по</ta>
            <ta e="T10" id="Seg_1451" s="T9">говорить-1SG.S</ta>
            <ta e="T11" id="Seg_1452" s="T10">ты.NOM</ta>
            <ta e="T12" id="Seg_1453" s="T11">я.GEN</ta>
            <ta e="T13" id="Seg_1454" s="T12">язык.[NOM]-1SG</ta>
            <ta e="T14" id="Seg_1455" s="T13">весь</ta>
            <ta e="T15" id="Seg_1456" s="T14">знать-2SG.O</ta>
            <ta e="T16" id="Seg_1457" s="T15">и</ta>
            <ta e="T17" id="Seg_1458" s="T16">говорить-2SG.S</ta>
            <ta e="T18" id="Seg_1459" s="T17">хороший-ADVZ</ta>
            <ta e="T19" id="Seg_1460" s="T18">а</ta>
            <ta e="T20" id="Seg_1461" s="T19">Нина.[NOM]</ta>
            <ta e="T21" id="Seg_1462" s="T20">NEG</ta>
            <ta e="T22" id="Seg_1463" s="T21">знать-3SG.S</ta>
            <ta e="T23" id="Seg_1464" s="T22">говорить-INF</ta>
            <ta e="T24" id="Seg_1465" s="T23">ну</ta>
            <ta e="T25" id="Seg_1466" s="T24">отправиться-OPT-1DU</ta>
            <ta e="T26" id="Seg_1467" s="T25">сидеть-FUT-1DU</ta>
            <ta e="T27" id="Seg_1468" s="T26">туда-ADV.LOC</ta>
            <ta e="T28" id="Seg_1469" s="T27">и</ta>
            <ta e="T29" id="Seg_1470" s="T28">говорить-FUT-1DU</ta>
            <ta e="T30" id="Seg_1471" s="T29">все</ta>
            <ta e="T31" id="Seg_1472" s="T30">слово-PL-ACC</ta>
            <ta e="T32" id="Seg_1473" s="T31">сказать-FUT-1DU</ta>
            <ta e="T33" id="Seg_1474" s="T32">я.NOM</ta>
            <ta e="T34" id="Seg_1475" s="T33">NEG</ta>
            <ta e="T35" id="Seg_1476" s="T34">знать-1SG.O</ta>
            <ta e="T36" id="Seg_1477" s="T35">когда</ta>
            <ta e="T37" id="Seg_1478" s="T36">закончить-RFL-FUT-3SG.S</ta>
            <ta e="T38" id="Seg_1479" s="T37">али</ta>
            <ta e="T39" id="Seg_1480" s="T38">NEG</ta>
            <ta e="T40" id="Seg_1481" s="T39">май-LOC</ta>
            <ta e="T41" id="Seg_1482" s="T40">все</ta>
            <ta e="T42" id="Seg_1483" s="T41">закончить-FUT-1DU</ta>
            <ta e="T43" id="Seg_1484" s="T42">слово-PL-ACC</ta>
            <ta e="T44" id="Seg_1485" s="T43">я.NOM</ta>
            <ta e="T45" id="Seg_1486" s="T44">потом</ta>
            <ta e="T46" id="Seg_1487" s="T45">деревня-ILL</ta>
            <ta e="T47" id="Seg_1488" s="T46">отправиться-FUT-1SG.S</ta>
            <ta e="T48" id="Seg_1489" s="T47">и</ta>
            <ta e="T49" id="Seg_1490" s="T48">ты.NOM-EMPH</ta>
            <ta e="T50" id="Seg_1491" s="T49">отправиться-POT.FUT.2SG</ta>
            <ta e="T51" id="Seg_1492" s="T50">назад</ta>
            <ta e="T52" id="Seg_1493" s="T51">ходить-CVB</ta>
            <ta e="T53" id="Seg_1494" s="T52">я.ALL</ta>
            <ta e="T54" id="Seg_1495" s="T53">заехать-EP-HAB-IMP.2SG.S</ta>
            <ta e="T55" id="Seg_1496" s="T54">я.NOM</ta>
            <ta e="T56" id="Seg_1497" s="T55">рыба-PL.[NOM]</ta>
            <ta e="T57" id="Seg_1498" s="T56">поймать-TR-FUT-1SG.S</ta>
            <ta e="T58" id="Seg_1499" s="T57">а</ta>
            <ta e="T59" id="Seg_1500" s="T58">ты.NOM</ta>
            <ta e="T60" id="Seg_1501" s="T59">заехать-EP-HAB-IMP.2SG.S</ta>
            <ta e="T61" id="Seg_1502" s="T60">рыба-EP-ACC</ta>
            <ta e="T62" id="Seg_1503" s="T61">съесть-INF</ta>
            <ta e="T63" id="Seg_1504" s="T62">что-INSTR</ta>
            <ta e="T64" id="Seg_1505" s="T63">русский-язык-ADVZ</ta>
            <ta e="T65" id="Seg_1506" s="T64">говорить-2SG.S</ta>
            <ta e="T66" id="Seg_1507" s="T65">я.GEN</ta>
            <ta e="T67" id="Seg_1508" s="T66">как</ta>
            <ta e="T68" id="Seg_1509" s="T67">надо</ta>
            <ta e="T69" id="Seg_1510" s="T68">говорить-INF</ta>
            <ta e="T70" id="Seg_1511" s="T69">а</ta>
            <ta e="T71" id="Seg_1512" s="T70">ты.NOM</ta>
            <ta e="T72" id="Seg_1513" s="T71">говорить-2SG.S</ta>
            <ta e="T73" id="Seg_1514" s="T72">селькуп-язык-ADVZ</ta>
            <ta e="T74" id="Seg_1515" s="T73">один</ta>
            <ta e="T75" id="Seg_1516" s="T74">мужчина-человек.[NOM]</ta>
            <ta e="T76" id="Seg_1517" s="T75">селькуп-язык-ADVZ</ta>
            <ta e="T77" id="Seg_1518" s="T76">хороший-ADVZ</ta>
            <ta e="T78" id="Seg_1519" s="T77">говорить-PST.[3SG.S]</ta>
            <ta e="T79" id="Seg_1520" s="T78">а</ta>
            <ta e="T80" id="Seg_1521" s="T79">сейчас</ta>
            <ta e="T81" id="Seg_1522" s="T80">бегать-MOM-DRV-FUT-1SG.S</ta>
            <ta e="T82" id="Seg_1523" s="T81">работать-INF</ta>
            <ta e="T83" id="Seg_1524" s="T82">работать-INF</ta>
            <ta e="T84" id="Seg_1525" s="T83">надо</ta>
            <ta e="T85" id="Seg_1526" s="T84">NEG</ta>
            <ta e="T86" id="Seg_1527" s="T85">работать-POT.FUT.2SG</ta>
            <ta e="T87" id="Seg_1528" s="T86">а</ta>
            <ta e="T88" id="Seg_1529" s="T87">что-ACC</ta>
            <ta e="T89" id="Seg_1530" s="T88">съесть-INF</ta>
            <ta e="T90" id="Seg_1531" s="T89">мы.[NOM]</ta>
            <ta e="T91" id="Seg_1532" s="T90">ведь</ta>
            <ta e="T92" id="Seg_1533" s="T91">три-%%-1PL</ta>
            <ta e="T93" id="Seg_1534" s="T92">быть-CO-1PL</ta>
            <ta e="T94" id="Seg_1535" s="T93">три</ta>
            <ta e="T95" id="Seg_1536" s="T94">человек-ILL</ta>
            <ta e="T96" id="Seg_1537" s="T95">много</ta>
            <ta e="T97" id="Seg_1538" s="T96">деньги.[NOM]</ta>
            <ta e="T98" id="Seg_1539" s="T97">надо</ta>
            <ta e="T99" id="Seg_1540" s="T98">этот-день.[NOM]</ta>
            <ta e="T100" id="Seg_1541" s="T99">ночь-ADV.LOC</ta>
            <ta e="T101" id="Seg_1542" s="T100">ты.ACC</ta>
            <ta e="T102" id="Seg_1543" s="T101">научить-DRV-INF</ta>
            <ta e="T103" id="Seg_1544" s="T102">хотеть-PST-1SG.S</ta>
            <ta e="T104" id="Seg_1545" s="T103">а</ta>
            <ta e="T105" id="Seg_1546" s="T104">ты.NOM</ta>
            <ta e="T106" id="Seg_1547" s="T105">разбудить-RFL-2SG.S</ta>
            <ta e="T107" id="Seg_1548" s="T106">я.NOM</ta>
            <ta e="T108" id="Seg_1549" s="T107">медленно</ta>
            <ta e="T109" id="Seg_1550" s="T108">прийти-CO-1SG.S</ta>
            <ta e="T110" id="Seg_1551" s="T109">ты.ALL</ta>
            <ta e="T111" id="Seg_1552" s="T110">а</ta>
            <ta e="T112" id="Seg_1553" s="T111">ты.NOM</ta>
            <ta e="T113" id="Seg_1554" s="T112">услышать-DRV-2SG.O</ta>
            <ta e="T114" id="Seg_1555" s="T113">а</ta>
            <ta e="T115" id="Seg_1556" s="T114">как</ta>
            <ta e="T116" id="Seg_1557" s="T115">ты.ACC</ta>
            <ta e="T117" id="Seg_1558" s="T116">научить-DRV-INF</ta>
            <ta e="T118" id="Seg_1559" s="T117">сам.2SG</ta>
            <ta e="T119" id="Seg_1560" s="T118">сказать-HAB-CO-2SG.S</ta>
            <ta e="T120" id="Seg_1561" s="T119">я.ACC</ta>
            <ta e="T121" id="Seg_1562" s="T120">научить-DRV-IMP.2SG.S</ta>
            <ta e="T122" id="Seg_1563" s="T121">сам.2SG</ta>
            <ta e="T123" id="Seg_1564" s="T122">разбудить-RFL-HAB-CO-2SG.S</ta>
            <ta e="T124" id="Seg_1565" s="T123">я.NOM</ta>
            <ta e="T125" id="Seg_1566" s="T124">NEG</ta>
            <ta e="T126" id="Seg_1567" s="T125">разбудить-RFL-HAB-FUT-1SG.S</ta>
            <ta e="T127" id="Seg_1568" s="T126">я.NOM</ta>
            <ta e="T128" id="Seg_1569" s="T127">крепкий-ADVZ</ta>
            <ta e="T129" id="Seg_1570" s="T128">спать-FUT-1SG.S</ta>
            <ta e="T130" id="Seg_1571" s="T129">всё_равно</ta>
            <ta e="T131" id="Seg_1572" s="T130">разбудить-RFL-POT.FUT.2SG</ta>
            <ta e="T132" id="Seg_1573" s="T131">я.NOM</ta>
            <ta e="T133" id="Seg_1574" s="T132">этот-ночь-ADVZ</ta>
            <ta e="T134" id="Seg_1575" s="T133">ты.ALL</ta>
            <ta e="T135" id="Seg_1576" s="T134">опять</ta>
            <ta e="T136" id="Seg_1577" s="T135">%%-FUT-1SG.S</ta>
            <ta e="T137" id="Seg_1578" s="T136">я.NOM</ta>
            <ta e="T138" id="Seg_1579" s="T137">ты.ALL</ta>
            <ta e="T139" id="Seg_1580" s="T138">двадцать</ta>
            <ta e="T140" id="Seg_1581" s="T139">слово.[NOM]</ta>
            <ta e="T141" id="Seg_1582" s="T140">сказать-TR-FUT-1SG.S</ta>
            <ta e="T142" id="Seg_1583" s="T141">научить-DRV-IMP.2SG.S</ta>
            <ta e="T143" id="Seg_1584" s="T142">хороший-ADVZ</ta>
            <ta e="T144" id="Seg_1585" s="T143">а.то</ta>
            <ta e="T145" id="Seg_1586" s="T144">мы-ADES</ta>
            <ta e="T146" id="Seg_1587" s="T145">смеяться-HAB-FUT-3PL</ta>
            <ta e="T147" id="Seg_1588" s="T146">что</ta>
            <ta e="T148" id="Seg_1589" s="T147">%очень.плохой</ta>
            <ta e="T149" id="Seg_1590" s="T148">быть-PST.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1591" s="T1">conj</ta>
            <ta e="T3" id="Seg_1592" s="T2">pers</ta>
            <ta e="T4" id="Seg_1593" s="T3">interrog</ta>
            <ta e="T5" id="Seg_1594" s="T4">v-v:pn</ta>
            <ta e="T6" id="Seg_1595" s="T5">conj</ta>
            <ta e="T7" id="Seg_1596" s="T6">pers</ta>
            <ta e="T8" id="Seg_1597" s="T7">n-n&gt;adj</ta>
            <ta e="T9" id="Seg_1598" s="T8">pp</ta>
            <ta e="T10" id="Seg_1599" s="T9">v-v:pn</ta>
            <ta e="T11" id="Seg_1600" s="T10">pers</ta>
            <ta e="T12" id="Seg_1601" s="T11">pers</ta>
            <ta e="T13" id="Seg_1602" s="T12">n.[n:case]-n:poss</ta>
            <ta e="T14" id="Seg_1603" s="T13">quant</ta>
            <ta e="T15" id="Seg_1604" s="T14">v-v:pn</ta>
            <ta e="T16" id="Seg_1605" s="T15">conj</ta>
            <ta e="T17" id="Seg_1606" s="T16">v-v:pn</ta>
            <ta e="T18" id="Seg_1607" s="T17">adj-adj&gt;adv</ta>
            <ta e="T19" id="Seg_1608" s="T18">conj</ta>
            <ta e="T20" id="Seg_1609" s="T19">nprop.[n:case]</ta>
            <ta e="T21" id="Seg_1610" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_1611" s="T21">v-v:pn</ta>
            <ta e="T23" id="Seg_1612" s="T22">v-v:inf</ta>
            <ta e="T24" id="Seg_1613" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_1614" s="T24">v-v:mood-v:pn</ta>
            <ta e="T26" id="Seg_1615" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_1616" s="T26">adv-adv:case</ta>
            <ta e="T28" id="Seg_1617" s="T27">conj</ta>
            <ta e="T29" id="Seg_1618" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_1619" s="T29">quant</ta>
            <ta e="T31" id="Seg_1620" s="T30">n-n:num-n:case</ta>
            <ta e="T32" id="Seg_1621" s="T31">v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_1622" s="T32">pers</ta>
            <ta e="T34" id="Seg_1623" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_1624" s="T34">v-v:pn</ta>
            <ta e="T36" id="Seg_1625" s="T35">conj</ta>
            <ta e="T37" id="Seg_1626" s="T36">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_1627" s="T37">conj</ta>
            <ta e="T39" id="Seg_1628" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_1629" s="T39">n-n:case</ta>
            <ta e="T41" id="Seg_1630" s="T40">quant</ta>
            <ta e="T42" id="Seg_1631" s="T41">v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_1632" s="T42">n-n:num-n:case</ta>
            <ta e="T44" id="Seg_1633" s="T43">pers</ta>
            <ta e="T45" id="Seg_1634" s="T44">adv</ta>
            <ta e="T46" id="Seg_1635" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_1636" s="T46">v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_1637" s="T47">conj</ta>
            <ta e="T49" id="Seg_1638" s="T48">pers-clit</ta>
            <ta e="T50" id="Seg_1639" s="T49">v-v:tense</ta>
            <ta e="T51" id="Seg_1640" s="T50">adv</ta>
            <ta e="T52" id="Seg_1641" s="T51">v-v&gt;adv</ta>
            <ta e="T53" id="Seg_1642" s="T52">pers</ta>
            <ta e="T54" id="Seg_1643" s="T53">v-v:ins-v&gt;v-v:mood.pn</ta>
            <ta e="T55" id="Seg_1644" s="T54">pers</ta>
            <ta e="T56" id="Seg_1645" s="T55">n-n:num.[n:case]</ta>
            <ta e="T57" id="Seg_1646" s="T56">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T58" id="Seg_1647" s="T57">conj</ta>
            <ta e="T59" id="Seg_1648" s="T58">pers</ta>
            <ta e="T60" id="Seg_1649" s="T59">v-v:ins-v&gt;v-v:mood.pn</ta>
            <ta e="T61" id="Seg_1650" s="T60">n-n:ins-n:case</ta>
            <ta e="T62" id="Seg_1651" s="T61">v-v:inf</ta>
            <ta e="T63" id="Seg_1652" s="T62">interrog-n:case</ta>
            <ta e="T64" id="Seg_1653" s="T63">adj-n-n&gt;adv</ta>
            <ta e="T65" id="Seg_1654" s="T64">v-v:pn</ta>
            <ta e="T66" id="Seg_1655" s="T65">pers</ta>
            <ta e="T67" id="Seg_1656" s="T66">pp</ta>
            <ta e="T68" id="Seg_1657" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_1658" s="T68">v-v:inf</ta>
            <ta e="T70" id="Seg_1659" s="T69">conj</ta>
            <ta e="T71" id="Seg_1660" s="T70">pers</ta>
            <ta e="T72" id="Seg_1661" s="T71">v-v:pn</ta>
            <ta e="T73" id="Seg_1662" s="T72">n-n-n&gt;adv</ta>
            <ta e="T74" id="Seg_1663" s="T73">num</ta>
            <ta e="T75" id="Seg_1664" s="T74">n-n.[n:case]</ta>
            <ta e="T76" id="Seg_1665" s="T75">n-n-n&gt;adv</ta>
            <ta e="T77" id="Seg_1666" s="T76">adj-adj&gt;adv</ta>
            <ta e="T78" id="Seg_1667" s="T77">v-v:tense.[v:pn]</ta>
            <ta e="T79" id="Seg_1668" s="T78">conj</ta>
            <ta e="T80" id="Seg_1669" s="T79">adv</ta>
            <ta e="T81" id="Seg_1670" s="T80">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_1671" s="T81">v-v:inf</ta>
            <ta e="T83" id="Seg_1672" s="T82">v-v:inf</ta>
            <ta e="T84" id="Seg_1673" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_1674" s="T84">ptcl</ta>
            <ta e="T86" id="Seg_1675" s="T85">v-v:tense</ta>
            <ta e="T87" id="Seg_1676" s="T86">conj</ta>
            <ta e="T88" id="Seg_1677" s="T87">interrog-n:case</ta>
            <ta e="T89" id="Seg_1678" s="T88">v-v:inf</ta>
            <ta e="T90" id="Seg_1679" s="T89">pers.[n:case]</ta>
            <ta e="T91" id="Seg_1680" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_1681" s="T91">num-%%-n:poss</ta>
            <ta e="T93" id="Seg_1682" s="T92">v-v:ins-v:pn</ta>
            <ta e="T94" id="Seg_1683" s="T93">num</ta>
            <ta e="T95" id="Seg_1684" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_1685" s="T95">quant</ta>
            <ta e="T97" id="Seg_1686" s="T96">n.[n:case]</ta>
            <ta e="T98" id="Seg_1687" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_1688" s="T98">dem-n.[n:case]</ta>
            <ta e="T100" id="Seg_1689" s="T99">n-adv:case</ta>
            <ta e="T101" id="Seg_1690" s="T100">pers</ta>
            <ta e="T102" id="Seg_1691" s="T101">v-v&gt;v-v:inf</ta>
            <ta e="T103" id="Seg_1692" s="T102">v-v:tense-v:pn</ta>
            <ta e="T104" id="Seg_1693" s="T103">conj</ta>
            <ta e="T105" id="Seg_1694" s="T104">pers</ta>
            <ta e="T106" id="Seg_1695" s="T105">v-v&gt;v-v:pn</ta>
            <ta e="T107" id="Seg_1696" s="T106">pers</ta>
            <ta e="T108" id="Seg_1697" s="T107">adv</ta>
            <ta e="T109" id="Seg_1698" s="T108">v-v:ins-v:pn</ta>
            <ta e="T110" id="Seg_1699" s="T109">pers</ta>
            <ta e="T111" id="Seg_1700" s="T110">conj</ta>
            <ta e="T112" id="Seg_1701" s="T111">pers</ta>
            <ta e="T113" id="Seg_1702" s="T112">v-v&gt;v-v:pn</ta>
            <ta e="T114" id="Seg_1703" s="T113">conj</ta>
            <ta e="T115" id="Seg_1704" s="T114">interrog</ta>
            <ta e="T116" id="Seg_1705" s="T115">pers</ta>
            <ta e="T117" id="Seg_1706" s="T116">v-v&gt;v-v:inf</ta>
            <ta e="T118" id="Seg_1707" s="T117">emphpro</ta>
            <ta e="T119" id="Seg_1708" s="T118">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T120" id="Seg_1709" s="T119">pers</ta>
            <ta e="T121" id="Seg_1710" s="T120">v-v&gt;v-v:mood.pn</ta>
            <ta e="T122" id="Seg_1711" s="T121">emphpro</ta>
            <ta e="T123" id="Seg_1712" s="T122">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T124" id="Seg_1713" s="T123">pers</ta>
            <ta e="T125" id="Seg_1714" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_1715" s="T125">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T127" id="Seg_1716" s="T126">pers</ta>
            <ta e="T128" id="Seg_1717" s="T127">adj-adj&gt;adv</ta>
            <ta e="T129" id="Seg_1718" s="T128">v-v:tense-v:pn</ta>
            <ta e="T130" id="Seg_1719" s="T129">adv</ta>
            <ta e="T131" id="Seg_1720" s="T130">v-v&gt;v-v:tense</ta>
            <ta e="T132" id="Seg_1721" s="T131">pers</ta>
            <ta e="T133" id="Seg_1722" s="T132">dem-n-n&gt;adv</ta>
            <ta e="T134" id="Seg_1723" s="T133">pers</ta>
            <ta e="T135" id="Seg_1724" s="T134">adv</ta>
            <ta e="T136" id="Seg_1725" s="T135">v-v:tense-v:pn</ta>
            <ta e="T137" id="Seg_1726" s="T136">pers</ta>
            <ta e="T138" id="Seg_1727" s="T137">pers</ta>
            <ta e="T139" id="Seg_1728" s="T138">num</ta>
            <ta e="T140" id="Seg_1729" s="T139">n.[n:case]</ta>
            <ta e="T141" id="Seg_1730" s="T140">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T142" id="Seg_1731" s="T141">v-v&gt;v-v:mood.pn</ta>
            <ta e="T143" id="Seg_1732" s="T142">adj-adj&gt;adv</ta>
            <ta e="T144" id="Seg_1733" s="T143">conj</ta>
            <ta e="T145" id="Seg_1734" s="T144">pers-n:case</ta>
            <ta e="T146" id="Seg_1735" s="T145">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T147" id="Seg_1736" s="T146">interrog</ta>
            <ta e="T149" id="Seg_1737" s="T148">v-v:tense.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1738" s="T1">conj</ta>
            <ta e="T3" id="Seg_1739" s="T2">pers</ta>
            <ta e="T4" id="Seg_1740" s="T3">interrog</ta>
            <ta e="T5" id="Seg_1741" s="T4">v</ta>
            <ta e="T6" id="Seg_1742" s="T5">conj</ta>
            <ta e="T7" id="Seg_1743" s="T6">pers</ta>
            <ta e="T8" id="Seg_1744" s="T7">adj</ta>
            <ta e="T9" id="Seg_1745" s="T8">pp</ta>
            <ta e="T10" id="Seg_1746" s="T9">v</ta>
            <ta e="T11" id="Seg_1747" s="T10">pers</ta>
            <ta e="T12" id="Seg_1748" s="T11">pers</ta>
            <ta e="T13" id="Seg_1749" s="T12">n</ta>
            <ta e="T14" id="Seg_1750" s="T13">quant</ta>
            <ta e="T15" id="Seg_1751" s="T14">v</ta>
            <ta e="T16" id="Seg_1752" s="T15">conj</ta>
            <ta e="T17" id="Seg_1753" s="T16">v</ta>
            <ta e="T18" id="Seg_1754" s="T17">adv</ta>
            <ta e="T19" id="Seg_1755" s="T18">conj</ta>
            <ta e="T20" id="Seg_1756" s="T19">nprop</ta>
            <ta e="T21" id="Seg_1757" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_1758" s="T21">v</ta>
            <ta e="T23" id="Seg_1759" s="T22">v</ta>
            <ta e="T24" id="Seg_1760" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_1761" s="T24">v</ta>
            <ta e="T26" id="Seg_1762" s="T25">v</ta>
            <ta e="T27" id="Seg_1763" s="T26">adv</ta>
            <ta e="T28" id="Seg_1764" s="T27">conj</ta>
            <ta e="T29" id="Seg_1765" s="T28">v</ta>
            <ta e="T30" id="Seg_1766" s="T29">quant</ta>
            <ta e="T31" id="Seg_1767" s="T30">n</ta>
            <ta e="T32" id="Seg_1768" s="T31">v</ta>
            <ta e="T33" id="Seg_1769" s="T32">pers</ta>
            <ta e="T34" id="Seg_1770" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_1771" s="T34">v</ta>
            <ta e="T36" id="Seg_1772" s="T35">conj</ta>
            <ta e="T37" id="Seg_1773" s="T36">v</ta>
            <ta e="T38" id="Seg_1774" s="T37">conj</ta>
            <ta e="T39" id="Seg_1775" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_1776" s="T39">n</ta>
            <ta e="T41" id="Seg_1777" s="T40">quant</ta>
            <ta e="T42" id="Seg_1778" s="T41">v</ta>
            <ta e="T43" id="Seg_1779" s="T42">n</ta>
            <ta e="T44" id="Seg_1780" s="T43">pers</ta>
            <ta e="T45" id="Seg_1781" s="T44">adv</ta>
            <ta e="T46" id="Seg_1782" s="T45">n</ta>
            <ta e="T47" id="Seg_1783" s="T46">v</ta>
            <ta e="T48" id="Seg_1784" s="T47">conj</ta>
            <ta e="T49" id="Seg_1785" s="T48">pro</ta>
            <ta e="T50" id="Seg_1786" s="T49">v</ta>
            <ta e="T51" id="Seg_1787" s="T50">adv</ta>
            <ta e="T52" id="Seg_1788" s="T51">adv</ta>
            <ta e="T53" id="Seg_1789" s="T52">pers</ta>
            <ta e="T54" id="Seg_1790" s="T53">v</ta>
            <ta e="T55" id="Seg_1791" s="T54">pers</ta>
            <ta e="T56" id="Seg_1792" s="T55">n</ta>
            <ta e="T57" id="Seg_1793" s="T56">v</ta>
            <ta e="T58" id="Seg_1794" s="T57">conj</ta>
            <ta e="T59" id="Seg_1795" s="T58">pers</ta>
            <ta e="T60" id="Seg_1796" s="T59">v</ta>
            <ta e="T61" id="Seg_1797" s="T60">n</ta>
            <ta e="T62" id="Seg_1798" s="T61">v</ta>
            <ta e="T63" id="Seg_1799" s="T62">interrog</ta>
            <ta e="T64" id="Seg_1800" s="T63">adv</ta>
            <ta e="T65" id="Seg_1801" s="T64">v</ta>
            <ta e="T66" id="Seg_1802" s="T65">pers</ta>
            <ta e="T67" id="Seg_1803" s="T66">pp</ta>
            <ta e="T68" id="Seg_1804" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_1805" s="T68">v</ta>
            <ta e="T70" id="Seg_1806" s="T69">conj</ta>
            <ta e="T71" id="Seg_1807" s="T70">pers</ta>
            <ta e="T72" id="Seg_1808" s="T71">v</ta>
            <ta e="T73" id="Seg_1809" s="T72">adv</ta>
            <ta e="T74" id="Seg_1810" s="T73">num</ta>
            <ta e="T75" id="Seg_1811" s="T74">n</ta>
            <ta e="T76" id="Seg_1812" s="T75">adv</ta>
            <ta e="T77" id="Seg_1813" s="T76">adv</ta>
            <ta e="T78" id="Seg_1814" s="T77">v</ta>
            <ta e="T79" id="Seg_1815" s="T78">conj</ta>
            <ta e="T80" id="Seg_1816" s="T79">adv</ta>
            <ta e="T81" id="Seg_1817" s="T80">v</ta>
            <ta e="T82" id="Seg_1818" s="T81">v</ta>
            <ta e="T83" id="Seg_1819" s="T82">v</ta>
            <ta e="T84" id="Seg_1820" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_1821" s="T84">ptcl</ta>
            <ta e="T86" id="Seg_1822" s="T85">v</ta>
            <ta e="T87" id="Seg_1823" s="T86">conj</ta>
            <ta e="T88" id="Seg_1824" s="T87">interrog</ta>
            <ta e="T89" id="Seg_1825" s="T88">v</ta>
            <ta e="T90" id="Seg_1826" s="T89">pers</ta>
            <ta e="T91" id="Seg_1827" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_1828" s="T91">num</ta>
            <ta e="T93" id="Seg_1829" s="T92">v</ta>
            <ta e="T94" id="Seg_1830" s="T93">num</ta>
            <ta e="T95" id="Seg_1831" s="T94">n</ta>
            <ta e="T96" id="Seg_1832" s="T95">quant</ta>
            <ta e="T97" id="Seg_1833" s="T96">n</ta>
            <ta e="T98" id="Seg_1834" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_1835" s="T98">adv</ta>
            <ta e="T100" id="Seg_1836" s="T99">v</ta>
            <ta e="T101" id="Seg_1837" s="T100">pers</ta>
            <ta e="T102" id="Seg_1838" s="T101">v</ta>
            <ta e="T103" id="Seg_1839" s="T102">v</ta>
            <ta e="T104" id="Seg_1840" s="T103">conj</ta>
            <ta e="T105" id="Seg_1841" s="T104">pers</ta>
            <ta e="T106" id="Seg_1842" s="T105">v</ta>
            <ta e="T107" id="Seg_1843" s="T106">pers</ta>
            <ta e="T108" id="Seg_1844" s="T107">adv</ta>
            <ta e="T109" id="Seg_1845" s="T108">v</ta>
            <ta e="T110" id="Seg_1846" s="T109">pers</ta>
            <ta e="T111" id="Seg_1847" s="T110">conj</ta>
            <ta e="T112" id="Seg_1848" s="T111">pers</ta>
            <ta e="T113" id="Seg_1849" s="T112">v</ta>
            <ta e="T114" id="Seg_1850" s="T113">conj</ta>
            <ta e="T115" id="Seg_1851" s="T114">interrog</ta>
            <ta e="T116" id="Seg_1852" s="T115">pers</ta>
            <ta e="T117" id="Seg_1853" s="T116">v</ta>
            <ta e="T118" id="Seg_1854" s="T117">emphpro</ta>
            <ta e="T119" id="Seg_1855" s="T118">v</ta>
            <ta e="T120" id="Seg_1856" s="T119">pers</ta>
            <ta e="T121" id="Seg_1857" s="T120">v</ta>
            <ta e="T122" id="Seg_1858" s="T121">emphpro</ta>
            <ta e="T123" id="Seg_1859" s="T122">v</ta>
            <ta e="T124" id="Seg_1860" s="T123">pers</ta>
            <ta e="T125" id="Seg_1861" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_1862" s="T125">v</ta>
            <ta e="T127" id="Seg_1863" s="T126">pers</ta>
            <ta e="T128" id="Seg_1864" s="T127">adj</ta>
            <ta e="T129" id="Seg_1865" s="T128">v</ta>
            <ta e="T130" id="Seg_1866" s="T129">adv</ta>
            <ta e="T131" id="Seg_1867" s="T130">v</ta>
            <ta e="T132" id="Seg_1868" s="T131">pers</ta>
            <ta e="T133" id="Seg_1869" s="T132">adv</ta>
            <ta e="T134" id="Seg_1870" s="T133">pers</ta>
            <ta e="T135" id="Seg_1871" s="T134">adv</ta>
            <ta e="T136" id="Seg_1872" s="T135">v</ta>
            <ta e="T137" id="Seg_1873" s="T136">pers</ta>
            <ta e="T138" id="Seg_1874" s="T137">pers</ta>
            <ta e="T139" id="Seg_1875" s="T138">num</ta>
            <ta e="T140" id="Seg_1876" s="T139">n</ta>
            <ta e="T141" id="Seg_1877" s="T140">v</ta>
            <ta e="T142" id="Seg_1878" s="T141">v</ta>
            <ta e="T143" id="Seg_1879" s="T142">adv</ta>
            <ta e="T144" id="Seg_1880" s="T143">conj</ta>
            <ta e="T145" id="Seg_1881" s="T144">pers</ta>
            <ta e="T146" id="Seg_1882" s="T145">v</ta>
            <ta e="T147" id="Seg_1883" s="T146">interrog</ta>
            <ta e="T149" id="Seg_1884" s="T148">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_1885" s="T2">pro.h:E</ta>
            <ta e="T7" id="Seg_1886" s="T6">pro.h:A</ta>
            <ta e="T11" id="Seg_1887" s="T10">pro.h:E</ta>
            <ta e="T12" id="Seg_1888" s="T11">pro.h:Poss</ta>
            <ta e="T13" id="Seg_1889" s="T12">np:Th</ta>
            <ta e="T17" id="Seg_1890" s="T16">0.2.h:A</ta>
            <ta e="T20" id="Seg_1891" s="T19">np.h:E</ta>
            <ta e="T23" id="Seg_1892" s="T22">v:Th</ta>
            <ta e="T25" id="Seg_1893" s="T24">0.1.h:A</ta>
            <ta e="T26" id="Seg_1894" s="T25">0.1.h:Th</ta>
            <ta e="T27" id="Seg_1895" s="T26">adv:L</ta>
            <ta e="T29" id="Seg_1896" s="T28">0.1.h:A</ta>
            <ta e="T31" id="Seg_1897" s="T30">np:Th</ta>
            <ta e="T32" id="Seg_1898" s="T31">0.1.h:A</ta>
            <ta e="T33" id="Seg_1899" s="T32">pro.h:E</ta>
            <ta e="T37" id="Seg_1900" s="T36">0.3:Th</ta>
            <ta e="T40" id="Seg_1901" s="T39">np:Time</ta>
            <ta e="T42" id="Seg_1902" s="T41">0.1.h:A</ta>
            <ta e="T43" id="Seg_1903" s="T42">np:Th</ta>
            <ta e="T44" id="Seg_1904" s="T43">pro.h:A</ta>
            <ta e="T45" id="Seg_1905" s="T44">adv:Time</ta>
            <ta e="T46" id="Seg_1906" s="T45">np:G</ta>
            <ta e="T49" id="Seg_1907" s="T48">pro.h:A</ta>
            <ta e="T51" id="Seg_1908" s="T50">adv:G</ta>
            <ta e="T53" id="Seg_1909" s="T52">pro.h:G</ta>
            <ta e="T54" id="Seg_1910" s="T53">0.2.h:A</ta>
            <ta e="T55" id="Seg_1911" s="T54">pro.h:A</ta>
            <ta e="T56" id="Seg_1912" s="T55">np:P</ta>
            <ta e="T59" id="Seg_1913" s="T58">pro.h:A</ta>
            <ta e="T61" id="Seg_1914" s="T60">np:P</ta>
            <ta e="T65" id="Seg_1915" s="T64">0.2.h:A</ta>
            <ta e="T69" id="Seg_1916" s="T68">v:Th</ta>
            <ta e="T71" id="Seg_1917" s="T70">pro.h:A</ta>
            <ta e="T75" id="Seg_1918" s="T74">np.h:A</ta>
            <ta e="T80" id="Seg_1919" s="T79">adv:Time</ta>
            <ta e="T81" id="Seg_1920" s="T80">0.1.h:A</ta>
            <ta e="T83" id="Seg_1921" s="T82">v:Th</ta>
            <ta e="T86" id="Seg_1922" s="T85">0.2.h:A</ta>
            <ta e="T90" id="Seg_1923" s="T89">pro.h:Th</ta>
            <ta e="T97" id="Seg_1924" s="T96">np:Th</ta>
            <ta e="T100" id="Seg_1925" s="T99">adv:Time</ta>
            <ta e="T101" id="Seg_1926" s="T100">pro.h:Th</ta>
            <ta e="T102" id="Seg_1927" s="T101">v:Th</ta>
            <ta e="T103" id="Seg_1928" s="T102">0.1.h:E</ta>
            <ta e="T105" id="Seg_1929" s="T104">pro.h:P</ta>
            <ta e="T107" id="Seg_1930" s="T106">pro.h:A</ta>
            <ta e="T110" id="Seg_1931" s="T109">pro.h:G</ta>
            <ta e="T112" id="Seg_1932" s="T111">pro.h:E</ta>
            <ta e="T119" id="Seg_1933" s="T118">0.2.h:A</ta>
            <ta e="T120" id="Seg_1934" s="T119">pro.h:Th</ta>
            <ta e="T121" id="Seg_1935" s="T120">0.2.h:A</ta>
            <ta e="T123" id="Seg_1936" s="T122">0.2.h:P</ta>
            <ta e="T124" id="Seg_1937" s="T123">pro.h:P</ta>
            <ta e="T127" id="Seg_1938" s="T126">pro.h:Th</ta>
            <ta e="T131" id="Seg_1939" s="T130">0.2.h:P</ta>
            <ta e="T132" id="Seg_1940" s="T131">pro.h:A</ta>
            <ta e="T133" id="Seg_1941" s="T132">adv:Time</ta>
            <ta e="T134" id="Seg_1942" s="T133">pro.h:G</ta>
            <ta e="T137" id="Seg_1943" s="T136">pro.h:A</ta>
            <ta e="T138" id="Seg_1944" s="T137">pro.h:R</ta>
            <ta e="T140" id="Seg_1945" s="T139">np:Th</ta>
            <ta e="T142" id="Seg_1946" s="T141">0.2.h:A</ta>
            <ta e="T146" id="Seg_1947" s="T145">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_1948" s="T2">pro.h:S</ta>
            <ta e="T5" id="Seg_1949" s="T4">v:pred</ta>
            <ta e="T10" id="Seg_1950" s="T5">s:compl</ta>
            <ta e="T11" id="Seg_1951" s="T10">pro.h:S</ta>
            <ta e="T13" id="Seg_1952" s="T12">np:O</ta>
            <ta e="T15" id="Seg_1953" s="T14">v:pred</ta>
            <ta e="T17" id="Seg_1954" s="T16">0.2.h:S v:pred</ta>
            <ta e="T20" id="Seg_1955" s="T19">np.h:S</ta>
            <ta e="T22" id="Seg_1956" s="T21">v:pred</ta>
            <ta e="T23" id="Seg_1957" s="T22">v:O</ta>
            <ta e="T25" id="Seg_1958" s="T24">0.1.h:S v:pred</ta>
            <ta e="T26" id="Seg_1959" s="T25">0.1.h:S v:pred</ta>
            <ta e="T29" id="Seg_1960" s="T28">0.1.h:S v:pred</ta>
            <ta e="T31" id="Seg_1961" s="T30">np:O</ta>
            <ta e="T32" id="Seg_1962" s="T31">0.1.h:S v:pred</ta>
            <ta e="T33" id="Seg_1963" s="T32">pro.h:S</ta>
            <ta e="T35" id="Seg_1964" s="T34">v:pred</ta>
            <ta e="T39" id="Seg_1965" s="T35">s:compl</ta>
            <ta e="T42" id="Seg_1966" s="T41">0.1.h:S v:pred</ta>
            <ta e="T43" id="Seg_1967" s="T42">np:O</ta>
            <ta e="T44" id="Seg_1968" s="T43">pro.h:S</ta>
            <ta e="T47" id="Seg_1969" s="T46">v:pred</ta>
            <ta e="T49" id="Seg_1970" s="T48">pro.h:S</ta>
            <ta e="T50" id="Seg_1971" s="T49">v:pred</ta>
            <ta e="T52" id="Seg_1972" s="T50">s:temp</ta>
            <ta e="T54" id="Seg_1973" s="T53">0.2.h:S v:pred</ta>
            <ta e="T55" id="Seg_1974" s="T54">pro.h:S</ta>
            <ta e="T56" id="Seg_1975" s="T55">np:O</ta>
            <ta e="T57" id="Seg_1976" s="T56">v:pred</ta>
            <ta e="T59" id="Seg_1977" s="T58">pro.h:S</ta>
            <ta e="T60" id="Seg_1978" s="T59">v:pred</ta>
            <ta e="T62" id="Seg_1979" s="T60">s:purp</ta>
            <ta e="T65" id="Seg_1980" s="T64">0.2.h:S v:pred</ta>
            <ta e="T68" id="Seg_1981" s="T67">ptcl:pred</ta>
            <ta e="T69" id="Seg_1982" s="T68">v:O</ta>
            <ta e="T71" id="Seg_1983" s="T70">pro.h:S</ta>
            <ta e="T72" id="Seg_1984" s="T71">v:pred</ta>
            <ta e="T75" id="Seg_1985" s="T74">np.h:S</ta>
            <ta e="T78" id="Seg_1986" s="T77">v:pred</ta>
            <ta e="T81" id="Seg_1987" s="T80">0.1.h:S v:pred</ta>
            <ta e="T82" id="Seg_1988" s="T81">s:purp</ta>
            <ta e="T83" id="Seg_1989" s="T82">v:O</ta>
            <ta e="T84" id="Seg_1990" s="T83">ptcl:pred</ta>
            <ta e="T86" id="Seg_1991" s="T85">0.2.h:S v:pred</ta>
            <ta e="T90" id="Seg_1992" s="T89">pro.h:S</ta>
            <ta e="T92" id="Seg_1993" s="T91">n:pred</ta>
            <ta e="T93" id="Seg_1994" s="T92">cop</ta>
            <ta e="T97" id="Seg_1995" s="T96">np:O</ta>
            <ta e="T98" id="Seg_1996" s="T97">ptcl:pred</ta>
            <ta e="T102" id="Seg_1997" s="T101">v:O</ta>
            <ta e="T103" id="Seg_1998" s="T102">0.1.h:S v:pred</ta>
            <ta e="T105" id="Seg_1999" s="T104">pro.h:S</ta>
            <ta e="T106" id="Seg_2000" s="T105">v:pred</ta>
            <ta e="T107" id="Seg_2001" s="T106">pro.h:S</ta>
            <ta e="T109" id="Seg_2002" s="T108">v:pred</ta>
            <ta e="T112" id="Seg_2003" s="T111">pro.h:S</ta>
            <ta e="T113" id="Seg_2004" s="T112">v:pred</ta>
            <ta e="T119" id="Seg_2005" s="T118">0.2.h:S v:pred</ta>
            <ta e="T120" id="Seg_2006" s="T119">pro.h:O</ta>
            <ta e="T121" id="Seg_2007" s="T120">0.2.h:S v:pred</ta>
            <ta e="T123" id="Seg_2008" s="T122">0.2.h:S v:pred</ta>
            <ta e="T124" id="Seg_2009" s="T123">pro.h:S</ta>
            <ta e="T126" id="Seg_2010" s="T125">v:pred</ta>
            <ta e="T127" id="Seg_2011" s="T126">pro.h:S</ta>
            <ta e="T129" id="Seg_2012" s="T128">v:pred</ta>
            <ta e="T131" id="Seg_2013" s="T130">0.2.h:S v:pred</ta>
            <ta e="T132" id="Seg_2014" s="T131">pro.h:S</ta>
            <ta e="T136" id="Seg_2015" s="T135">v:pred</ta>
            <ta e="T137" id="Seg_2016" s="T136">pro.h:S</ta>
            <ta e="T140" id="Seg_2017" s="T139">np:O</ta>
            <ta e="T141" id="Seg_2018" s="T140">v:pred</ta>
            <ta e="T142" id="Seg_2019" s="T141">0.2.h:S v:pred</ta>
            <ta e="T146" id="Seg_2020" s="T145">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_2021" s="T1">RUS:gram</ta>
            <ta e="T6" id="Seg_2022" s="T5">RUS:gram</ta>
            <ta e="T14" id="Seg_2023" s="T13">RUS:core</ta>
            <ta e="T16" id="Seg_2024" s="T15">RUS:gram</ta>
            <ta e="T19" id="Seg_2025" s="T18">RUS:gram</ta>
            <ta e="T24" id="Seg_2026" s="T23">RUS:disc</ta>
            <ta e="T28" id="Seg_2027" s="T27">RUS:gram</ta>
            <ta e="T30" id="Seg_2028" s="T29">RUS:core</ta>
            <ta e="T38" id="Seg_2029" s="T37">RUS:gram</ta>
            <ta e="T40" id="Seg_2030" s="T39">RUS:cult</ta>
            <ta e="T41" id="Seg_2031" s="T40">RUS:core</ta>
            <ta e="T45" id="Seg_2032" s="T44">RUS:disc</ta>
            <ta e="T48" id="Seg_2033" s="T47">RUS:gram</ta>
            <ta e="T58" id="Seg_2034" s="T57">RUS:gram</ta>
            <ta e="T68" id="Seg_2035" s="T67">RUS:mod</ta>
            <ta e="T70" id="Seg_2036" s="T69">RUS:gram</ta>
            <ta e="T79" id="Seg_2037" s="T78">RUS:gram</ta>
            <ta e="T80" id="Seg_2038" s="T79">RUS:core</ta>
            <ta e="T84" id="Seg_2039" s="T83">RUS:mod</ta>
            <ta e="T87" id="Seg_2040" s="T86">RUS:gram</ta>
            <ta e="T91" id="Seg_2041" s="T90">RUS:disc</ta>
            <ta e="T98" id="Seg_2042" s="T97">RUS:mod</ta>
            <ta e="T104" id="Seg_2043" s="T103">RUS:gram</ta>
            <ta e="T111" id="Seg_2044" s="T110">RUS:gram</ta>
            <ta e="T114" id="Seg_2045" s="T113">RUS:gram</ta>
            <ta e="T128" id="Seg_2046" s="T127">RUS:cult</ta>
            <ta e="T130" id="Seg_2047" s="T129">RUS:disc</ta>
            <ta e="T144" id="Seg_2048" s="T143">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T10" id="Seg_2049" s="T1">And how did you get to know, that I speak Selkup?</ta>
            <ta e="T18" id="Seg_2050" s="T10">You know my language, you speak well.</ta>
            <ta e="T23" id="Seg_2051" s="T18">And Nina can't speak [Selkup].</ta>
            <ta e="T29" id="Seg_2052" s="T23">Well, let's go and sit down, we'll speak there.</ta>
            <ta e="T32" id="Seg_2053" s="T29">We'll tell all words.</ta>
            <ta e="T39" id="Seg_2054" s="T32">I don't know, whether it'll come to an end or not.</ta>
            <ta e="T43" id="Seg_2055" s="T39">In May we'll be done with all the words.</ta>
            <ta e="T47" id="Seg_2056" s="T43">I'll go home then.</ta>
            <ta e="T50" id="Seg_2057" s="T47">And you'll go.</ta>
            <ta e="T54" id="Seg_2058" s="T50">When you travel back, come to visit me.</ta>
            <ta e="T57" id="Seg_2059" s="T54">I'll catch fish.</ta>
            <ta e="T62" id="Seg_2060" s="T57">And you come [to me] to eat fish.</ta>
            <ta e="T65" id="Seg_2061" s="T62">Why do you speak Russian?</ta>
            <ta e="T69" id="Seg_2062" s="T65">[You] should speak my language.</ta>
            <ta e="T73" id="Seg_2063" s="T69">And do you speak Selkup?</ta>
            <ta e="T78" id="Seg_2064" s="T73">One man spoke Selkup very well.</ta>
            <ta e="T82" id="Seg_2065" s="T78">And now I'll run to my work.</ta>
            <ta e="T84" id="Seg_2066" s="T82">One should work.</ta>
            <ta e="T89" id="Seg_2067" s="T84">If you don't work, what shall you eat?</ta>
            <ta e="T93" id="Seg_2068" s="T89">There are three of us.</ta>
            <ta e="T98" id="Seg_2069" s="T93">One needs much money for three [persons].</ta>
            <ta e="T103" id="Seg_2070" s="T98">Yesterday night I wanted to teach you.</ta>
            <ta e="T106" id="Seg_2071" s="T103">And you woke up.</ta>
            <ta e="T110" id="Seg_2072" s="T106">I went silently towards you.</ta>
            <ta e="T113" id="Seg_2073" s="T110">And you heard [me coming].</ta>
            <ta e="T117" id="Seg_2074" s="T113">And how shall I teach you?</ta>
            <ta e="T123" id="Seg_2075" s="T117">You say yourself: “Teach me”, – and you wake up.</ta>
            <ta e="T126" id="Seg_2076" s="T123">“I won't wake up.</ta>
            <ta e="T129" id="Seg_2077" s="T126">I'll be deep asleep.”</ta>
            <ta e="T131" id="Seg_2078" s="T129">You'll wake up anyway.</ta>
            <ta e="T136" id="Seg_2079" s="T131">Tomorrow night I'll come to you again.</ta>
            <ta e="T141" id="Seg_2080" s="T136">I'll tell you twenty words.</ta>
            <ta e="T143" id="Seg_2081" s="T141">Learn well.</ta>
            <ta e="T146" id="Seg_2082" s="T143">Otherwise you'll be laughed at.</ta>
            <ta e="T149" id="Seg_2083" s="T146">(What a bad man.)?</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T10" id="Seg_2084" s="T1">Und wie hast du erfahren, dass ich Selkupisch spreche?</ta>
            <ta e="T18" id="Seg_2085" s="T10">Du kennst meine Sprache, du sprichst gut.</ta>
            <ta e="T23" id="Seg_2086" s="T18">Und Nina kann kein [Selkupisch] sprechen.</ta>
            <ta e="T29" id="Seg_2087" s="T23">Nunja, lass uns gehen und uns setzen, wir sprechen dort.</ta>
            <ta e="T32" id="Seg_2088" s="T29">Wir nennen alle Wörter.</ta>
            <ta e="T39" id="Seg_2089" s="T32">Ich weiß nicht, ob es zu einem Ende kommt oder nicht.</ta>
            <ta e="T43" id="Seg_2090" s="T39">Im Mai werden wir mit allen Wörtern fertig sein.</ta>
            <ta e="T47" id="Seg_2091" s="T43">Ich gehe dann nach Hause.</ta>
            <ta e="T50" id="Seg_2092" s="T47">Und du gehst.</ta>
            <ta e="T54" id="Seg_2093" s="T50">Wenn du zurückkehrst, komm mich besuchen.</ta>
            <ta e="T57" id="Seg_2094" s="T54">Ich werde Fisch fangen.</ta>
            <ta e="T62" id="Seg_2095" s="T57">Und du kommst [zu mir] um Fisch zu essen.</ta>
            <ta e="T65" id="Seg_2096" s="T62">Warum sprichst du Russisch?</ta>
            <ta e="T69" id="Seg_2097" s="T65">[Du] solltest meine Sprache sprechen.</ta>
            <ta e="T73" id="Seg_2098" s="T69">Und sprichst du Selkupisch?</ta>
            <ta e="T78" id="Seg_2099" s="T73">Ein Mann sprach Selkupisch sehr gut.</ta>
            <ta e="T82" id="Seg_2100" s="T78">Und jetzt laufe ich zur Arbeit.</ta>
            <ta e="T84" id="Seg_2101" s="T82">Man muss arbeiten.</ta>
            <ta e="T89" id="Seg_2102" s="T84">Wenn du nicht arbeitest, was sollst du essen?</ta>
            <ta e="T93" id="Seg_2103" s="T89">Wir sind zu dritt.</ta>
            <ta e="T98" id="Seg_2104" s="T93">Man braucht viel Geld für drei Leute.</ta>
            <ta e="T103" id="Seg_2105" s="T98">Gestern Nacht wollte ich dich unterrichten.</ta>
            <ta e="T106" id="Seg_2106" s="T103">Und du bist aufgewacht.</ta>
            <ta e="T110" id="Seg_2107" s="T106">Ich ging leise zu dir.</ta>
            <ta e="T113" id="Seg_2108" s="T110">Und du hast [mich kommen] hören.</ta>
            <ta e="T117" id="Seg_2109" s="T113">Und wie soll ich dich unterrichten?</ta>
            <ta e="T123" id="Seg_2110" s="T117">Du sagst selbst: "Unterrichte mich", -- und du wachst auf.</ta>
            <ta e="T126" id="Seg_2111" s="T123">"Ich werde nicht aufwachen.</ta>
            <ta e="T129" id="Seg_2112" s="T126">Ich werde tief schlafen."</ta>
            <ta e="T131" id="Seg_2113" s="T129">Du wachst sowieso auf.</ta>
            <ta e="T136" id="Seg_2114" s="T131">Morgen Nacht komme ich wieder zu dir.</ta>
            <ta e="T141" id="Seg_2115" s="T136">Ich werde dir zwanzig Wörter sagen.</ta>
            <ta e="T143" id="Seg_2116" s="T141">Lerne gut.</ta>
            <ta e="T146" id="Seg_2117" s="T143">Sonst lacht man dich aus.</ta>
            <ta e="T149" id="Seg_2118" s="T146">(Was für ein schlechter Mann)?</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T10" id="Seg_2119" s="T1">А ты откуда узнала, что я по-селькупски говорю?</ta>
            <ta e="T18" id="Seg_2120" s="T10">Ты мой язык весь знаешь, разговариваешь хорошо.</ta>
            <ta e="T23" id="Seg_2121" s="T18">А Нина не умеет говорить.</ta>
            <ta e="T29" id="Seg_2122" s="T23">Ну, пойдем там посидим, там и поговорим.</ta>
            <ta e="T32" id="Seg_2123" s="T29">Все слова скажем.</ta>
            <ta e="T39" id="Seg_2124" s="T32">Я не знаю, когда-нибудь кончится или нет.</ta>
            <ta e="T43" id="Seg_2125" s="T39">В мае все закончим слова.</ta>
            <ta e="T47" id="Seg_2126" s="T43">Я потом домой поеду.</ta>
            <ta e="T50" id="Seg_2127" s="T47">И ты поедешь.</ta>
            <ta e="T54" id="Seg_2128" s="T50">Обратно когда поедешь, заезжай ко мне.</ta>
            <ta e="T57" id="Seg_2129" s="T54">Я рыбу добуду.</ta>
            <ta e="T62" id="Seg_2130" s="T57">А ты заезжай рыбу поесть.</ta>
            <ta e="T65" id="Seg_2131" s="T62">Почему по-русски разговариваешь?</ta>
            <ta e="T69" id="Seg_2132" s="T65">По-моему надо разговаривать.</ta>
            <ta e="T73" id="Seg_2133" s="T69">А ты разговариваешь по-селькупски?</ta>
            <ta e="T78" id="Seg_2134" s="T73">Один мужик по-селькупски хорошо говорил.</ta>
            <ta e="T82" id="Seg_2135" s="T78">А сейчас я побегу работать.</ta>
            <ta e="T84" id="Seg_2136" s="T82">Работать надо.</ta>
            <ta e="T89" id="Seg_2137" s="T84">Если работать не будешь, а что есть?</ta>
            <ta e="T93" id="Seg_2138" s="T89">Нас ведь трое.</ta>
            <ta e="T98" id="Seg_2139" s="T93">На троих много денег надо.</ta>
            <ta e="T103" id="Seg_2140" s="T98">Сегодня ночью я тебя учить хотела.</ta>
            <ta e="T106" id="Seg_2141" s="T103">А ты проснулась.</ta>
            <ta e="T110" id="Seg_2142" s="T106">Я потихоньку подошла к тебе.</ta>
            <ta e="T113" id="Seg_2143" s="T110">А ты слышишь.</ta>
            <ta e="T117" id="Seg_2144" s="T113">А как тебя научить?</ta>
            <ta e="T123" id="Seg_2145" s="T117">Ты сама говоришь: “Меня учи”, – а сама просыпаешься.</ta>
            <ta e="T126" id="Seg_2146" s="T123">“Я не буду просыпаться.</ta>
            <ta e="T129" id="Seg_2147" s="T126">Я крепко буду спать”.</ta>
            <ta e="T131" id="Seg_2148" s="T129">Все равно проснешься.</ta>
            <ta e="T136" id="Seg_2149" s="T131">Я сегодня ночью к тебе опять подойду.</ta>
            <ta e="T141" id="Seg_2150" s="T136">Я тебе сегодня двадцать слов скажу.</ta>
            <ta e="T143" id="Seg_2151" s="T141">Учись хорошо.</ta>
            <ta e="T146" id="Seg_2152" s="T143">А то у нас будут смеяться.</ta>
            <ta e="T149" id="Seg_2153" s="T146">(What a bad man.)?</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T10" id="Seg_2154" s="T1">ты откуда знаешь (узнала) что я по-остяцки говорю</ta>
            <ta e="T18" id="Seg_2155" s="T10">ты мой язык весь знаешь разговариваешь хорошо</ta>
            <ta e="T23" id="Seg_2156" s="T18">а Нина не умеет говорить</ta>
            <ta e="T29" id="Seg_2157" s="T23">ну пойдем там посидим и будем разговаривать</ta>
            <ta e="T32" id="Seg_2158" s="T29">все слова скажем</ta>
            <ta e="T39" id="Seg_2159" s="T32">я не знаю когда-нибудь слова кончатся или нет</ta>
            <ta e="T43" id="Seg_2160" s="T39">в мае все слова наверно кончаем (кончим)</ta>
            <ta e="T47" id="Seg_2161" s="T43">я потом поеду</ta>
            <ta e="T50" id="Seg_2162" s="T47">и ты поедешь</ta>
            <ta e="T54" id="Seg_2163" s="T50">обратно когда поедешь заезжай ко мне</ta>
            <ta e="T57" id="Seg_2164" s="T54">я рыбу добуду</ta>
            <ta e="T62" id="Seg_2165" s="T57">ты заедешь рыбу покушать</ta>
            <ta e="T65" id="Seg_2166" s="T62">почему по-русски разговариваешь</ta>
            <ta e="T69" id="Seg_2167" s="T65">по-моему надо разговаривать</ta>
            <ta e="T73" id="Seg_2168" s="T69">а ты разговариваешь по-остяцки</ta>
            <ta e="T78" id="Seg_2169" s="T73">один мужик по-селькупски хорошо говорил</ta>
            <ta e="T82" id="Seg_2170" s="T78">а сейчас я побегу работать</ta>
            <ta e="T84" id="Seg_2171" s="T82">работать надо</ta>
            <ta e="T89" id="Seg_2172" s="T84">если работать не будешь а что есть будешь</ta>
            <ta e="T93" id="Seg_2173" s="T89">нас ведь трое</ta>
            <ta e="T98" id="Seg_2174" s="T93">на троих много денег надо</ta>
            <ta e="T103" id="Seg_2175" s="T98">сегодня ночью тебя учить хотела</ta>
            <ta e="T106" id="Seg_2176" s="T103">а ты проснулась</ta>
            <ta e="T110" id="Seg_2177" s="T106">я помаленьку подошла к тебе</ta>
            <ta e="T113" id="Seg_2178" s="T110">а ты слышишь</ta>
            <ta e="T117" id="Seg_2179" s="T113">а как тебя научить</ta>
            <ta e="T123" id="Seg_2180" s="T117">сама говоришь меня учи а сама разбужаешься</ta>
            <ta e="T126" id="Seg_2181" s="T123">я не буду разбужаться</ta>
            <ta e="T129" id="Seg_2182" s="T126">я крепко буду спать</ta>
            <ta e="T136" id="Seg_2183" s="T131">я сегодня ночь(ю) к тебе опять подойду</ta>
            <ta e="T141" id="Seg_2184" s="T136">я тебе сегодня двадцать слов скажу</ta>
            <ta e="T143" id="Seg_2185" s="T141">учись хорошо</ta>
            <ta e="T146" id="Seg_2186" s="T143">а у нас будут смеяться</ta>
            <ta e="T149" id="Seg_2187" s="T146">(какой человек плохо говорит)</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T10" id="Seg_2188" s="T1">[BrM:] 'süsögulʼder' changed to 'süsögulʼ der'.</ta>
            <ta e="T54" id="Seg_2189" s="T50">[BrM:] Tentative analysis of 'udurukok'.</ta>
            <ta e="T57" id="Seg_2190" s="T54">[BrM:] TR?</ta>
            <ta e="T62" id="Seg_2191" s="T57">[BrM:] Tentative analysis of 'udurukok'.</ta>
            <ta e="T69" id="Seg_2192" s="T65">[BrM:] 'Matdarʼe' changed to 'Mat darʼe'.</ta>
            <ta e="T93" id="Seg_2193" s="T89">[KuAI:] Variant: 'nagurtautə'.</ta>
            <ta e="T110" id="Seg_2194" s="T106">[KuAI:] Variant: 'tüuan'.</ta>
            <ta e="T123" id="Seg_2195" s="T117">[KuAI:] Variants: 'tʼarkwatdə', 'sädɨtʼekwatdtə'.</ta>
            <ta e="T126" id="Seg_2196" s="T123">[KuAI:] Variant: 'sədətʼukkəʒan'.</ta>
            <ta e="T141" id="Seg_2197" s="T136">[BrM:] TR?</ta>
            <ta e="T146" id="Seg_2198" s="T143">[BrM:] 'A to' changed to 'Ato'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
