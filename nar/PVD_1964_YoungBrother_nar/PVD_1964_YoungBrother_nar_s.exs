<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_YoungBrother_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_YoungBrother_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">126</ud-information>
            <ud-information attribute-name="# HIAT:w">89</ud-information>
            <ud-information attribute-name="# e">89</ud-information>
            <ud-information attribute-name="# HIAT:u">20</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T90" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">pelgattə</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">senku</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">larɨbɨkuzan</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Qwɛrau</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">Irinam</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">senku</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">Mʼäɣunä</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">tʼüa</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">man</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">tɨmnʼäu</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_44" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">Aːmdɨdʼiːn</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">menan</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">i</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">qwannɨn</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_59" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">Me</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">täjerbɨzaj</ts>
                  <nts id="Seg_65" n="HIAT:ip">,</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">täp</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">qwenba</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">maːtqətda</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_78" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">A</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">täp</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">akoškan</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">ɨlotdə</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">nɨlʼezippa</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_96" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">A</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">me</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">jass</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">tunuzaj</ts>
                  <nts id="Seg_108" n="HIAT:ip">.</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_111" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">Man</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_116" n="HIAT:w" s="T31">Irinanä</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">tʼäran</ts>
                  <nts id="Seg_120" n="HIAT:ip">:</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_122" n="HIAT:ip">“</nts>
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">Qonsat</ts>
                  <nts id="Seg_125" n="HIAT:ip">?</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_128" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">Tan</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">pasʼ</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_136" n="HIAT:w" s="T36">kɨgat</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_139" n="HIAT:w" s="T37">qotdugu</ts>
                  <nts id="Seg_140" n="HIAT:ip">?</nts>
                  <nts id="Seg_141" n="HIAT:ip">”</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_144" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_146" n="HIAT:w" s="T38">A</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_149" n="HIAT:w" s="T39">Irina</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_152" n="HIAT:w" s="T40">tʼärɨn</ts>
                  <nts id="Seg_153" n="HIAT:ip">:</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_155" n="HIAT:ip">“</nts>
                  <ts e="T42" id="Seg_157" n="HIAT:w" s="T41">Ass</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_160" n="HIAT:w" s="T42">qonsan</ts>
                  <nts id="Seg_161" n="HIAT:ip">.</nts>
                  <nts id="Seg_162" n="HIAT:ip">”</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_165" n="HIAT:u" s="T43">
                  <nts id="Seg_166" n="HIAT:ip">“</nts>
                  <ts e="T44" id="Seg_168" n="HIAT:w" s="T43">Man</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_171" n="HIAT:w" s="T44">tekga</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_174" n="HIAT:w" s="T45">qajämɨlam</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_177" n="HIAT:w" s="T46">kʼelʼlʼeu</ts>
                  <nts id="Seg_178" n="HIAT:ip">.</nts>
                  <nts id="Seg_179" n="HIAT:ip">”</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_182" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_184" n="HIAT:w" s="T47">A</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_187" n="HIAT:w" s="T48">Irina</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_190" n="HIAT:w" s="T49">tʼärɨn</ts>
                  <nts id="Seg_191" n="HIAT:ip">:</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_193" n="HIAT:ip">“</nts>
                  <ts e="T51" id="Seg_195" n="HIAT:w" s="T50">Ketɨ</ts>
                  <nts id="Seg_196" n="HIAT:ip">.</nts>
                  <nts id="Seg_197" n="HIAT:ip">”</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_200" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_202" n="HIAT:w" s="T51">I</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_205" n="HIAT:w" s="T52">man</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_208" n="HIAT:w" s="T53">kʼelʼlʼe</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_211" n="HIAT:w" s="T54">jübərau</ts>
                  <nts id="Seg_212" n="HIAT:ip">.</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_215" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_217" n="HIAT:w" s="T55">A</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_220" n="HIAT:w" s="T56">Kriša</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_223" n="HIAT:w" s="T57">üsedʼepat</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_226" n="HIAT:w" s="T58">i</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_229" n="HIAT:w" s="T59">qomdä</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_232" n="HIAT:w" s="T60">mašqoːulǯebat</ts>
                  <nts id="Seg_233" n="HIAT:ip">,</nts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_236" n="HIAT:w" s="T61">ügulǯupbaː</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_239" n="HIAT:w" s="T62">qajla</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_242" n="HIAT:w" s="T63">kʼetčɨdɨt</ts>
                  <nts id="Seg_243" n="HIAT:ip">.</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_246" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_248" n="HIAT:w" s="T64">A</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_251" n="HIAT:w" s="T65">man</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_254" n="HIAT:w" s="T66">qulupbaːn</ts>
                  <nts id="Seg_255" n="HIAT:ip">.</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_258" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_260" n="HIAT:w" s="T67">Man</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_263" n="HIAT:w" s="T68">kʼelʼlʼe</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_266" n="HIAT:w" s="T69">mʼalčao</ts>
                  <nts id="Seg_267" n="HIAT:ip">.</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_270" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_272" n="HIAT:w" s="T70">Tɨmnʼäu</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_275" n="HIAT:w" s="T71">kak</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_278" n="HIAT:w" s="T72">qaːrolʼdä</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_281" n="HIAT:w" s="T73">akoškan</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_284" n="HIAT:w" s="T74">ɨloɣɨn</ts>
                  <nts id="Seg_285" n="HIAT:ip">.</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_288" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_290" n="HIAT:w" s="T75">Tʼärɨn</ts>
                  <nts id="Seg_291" n="HIAT:ip">:</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_293" n="HIAT:ip">“</nts>
                  <ts e="T77" id="Seg_295" n="HIAT:w" s="T76">Soːlaga</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_298" n="HIAT:w" s="T77">näjɣum</ts>
                  <nts id="Seg_299" n="HIAT:ip">.</nts>
                  <nts id="Seg_300" n="HIAT:ip">”</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_303" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_305" n="HIAT:w" s="T78">Me</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_308" n="HIAT:w" s="T79">datao</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_311" n="HIAT:w" s="T80">kocʼiwannaj</ts>
                  <nts id="Seg_312" n="HIAT:ip">,</nts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_315" n="HIAT:w" s="T81">qaːronʼaj</ts>
                  <nts id="Seg_316" n="HIAT:ip">.</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_319" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_321" n="HIAT:w" s="T82">Me</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_324" n="HIAT:w" s="T83">jass</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_327" n="HIAT:w" s="T84">tunuzaj</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_330" n="HIAT:w" s="T85">što</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_333" n="HIAT:w" s="T86">täp</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_336" n="HIAT:w" s="T87">ügulǯupba</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_339" n="HIAT:w" s="T88">akoškan</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_342" n="HIAT:w" s="T89">iloɣɨn</ts>
                  <nts id="Seg_343" n="HIAT:ip">.</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T90" id="Seg_345" n="sc" s="T1">
               <ts e="T2" id="Seg_347" n="e" s="T1">Man </ts>
               <ts e="T3" id="Seg_349" n="e" s="T2">pelgattə </ts>
               <ts e="T4" id="Seg_351" n="e" s="T3">senku </ts>
               <ts e="T5" id="Seg_353" n="e" s="T4">larɨbɨkuzan. </ts>
               <ts e="T6" id="Seg_355" n="e" s="T5">Qwɛrau </ts>
               <ts e="T7" id="Seg_357" n="e" s="T6">Irinam </ts>
               <ts e="T8" id="Seg_359" n="e" s="T7">senku. </ts>
               <ts e="T9" id="Seg_361" n="e" s="T8">Mʼäɣunä </ts>
               <ts e="T10" id="Seg_363" n="e" s="T9">tʼüa </ts>
               <ts e="T11" id="Seg_365" n="e" s="T10">man </ts>
               <ts e="T12" id="Seg_367" n="e" s="T11">tɨmnʼäu. </ts>
               <ts e="T13" id="Seg_369" n="e" s="T12">Aːmdɨdʼiːn </ts>
               <ts e="T14" id="Seg_371" n="e" s="T13">menan </ts>
               <ts e="T15" id="Seg_373" n="e" s="T14">i </ts>
               <ts e="T16" id="Seg_375" n="e" s="T15">qwannɨn. </ts>
               <ts e="T17" id="Seg_377" n="e" s="T16">Me </ts>
               <ts e="T18" id="Seg_379" n="e" s="T17">täjerbɨzaj, </ts>
               <ts e="T19" id="Seg_381" n="e" s="T18">täp </ts>
               <ts e="T20" id="Seg_383" n="e" s="T19">qwenba </ts>
               <ts e="T21" id="Seg_385" n="e" s="T20">maːtqətda. </ts>
               <ts e="T22" id="Seg_387" n="e" s="T21">A </ts>
               <ts e="T23" id="Seg_389" n="e" s="T22">täp </ts>
               <ts e="T24" id="Seg_391" n="e" s="T23">akoškan </ts>
               <ts e="T25" id="Seg_393" n="e" s="T24">ɨlotdə </ts>
               <ts e="T26" id="Seg_395" n="e" s="T25">nɨlʼezippa. </ts>
               <ts e="T27" id="Seg_397" n="e" s="T26">A </ts>
               <ts e="T28" id="Seg_399" n="e" s="T27">me </ts>
               <ts e="T29" id="Seg_401" n="e" s="T28">jass </ts>
               <ts e="T30" id="Seg_403" n="e" s="T29">tunuzaj. </ts>
               <ts e="T31" id="Seg_405" n="e" s="T30">Man </ts>
               <ts e="T32" id="Seg_407" n="e" s="T31">Irinanä </ts>
               <ts e="T33" id="Seg_409" n="e" s="T32">tʼäran: </ts>
               <ts e="T34" id="Seg_411" n="e" s="T33">“Qonsat? </ts>
               <ts e="T35" id="Seg_413" n="e" s="T34">Tan </ts>
               <ts e="T36" id="Seg_415" n="e" s="T35">pasʼ </ts>
               <ts e="T37" id="Seg_417" n="e" s="T36">kɨgat </ts>
               <ts e="T38" id="Seg_419" n="e" s="T37">qotdugu?” </ts>
               <ts e="T39" id="Seg_421" n="e" s="T38">A </ts>
               <ts e="T40" id="Seg_423" n="e" s="T39">Irina </ts>
               <ts e="T41" id="Seg_425" n="e" s="T40">tʼärɨn: </ts>
               <ts e="T42" id="Seg_427" n="e" s="T41">“Ass </ts>
               <ts e="T43" id="Seg_429" n="e" s="T42">qonsan.” </ts>
               <ts e="T44" id="Seg_431" n="e" s="T43">“Man </ts>
               <ts e="T45" id="Seg_433" n="e" s="T44">tekga </ts>
               <ts e="T46" id="Seg_435" n="e" s="T45">qajämɨlam </ts>
               <ts e="T47" id="Seg_437" n="e" s="T46">kʼelʼlʼeu.” </ts>
               <ts e="T48" id="Seg_439" n="e" s="T47">A </ts>
               <ts e="T49" id="Seg_441" n="e" s="T48">Irina </ts>
               <ts e="T50" id="Seg_443" n="e" s="T49">tʼärɨn: </ts>
               <ts e="T51" id="Seg_445" n="e" s="T50">“Ketɨ.” </ts>
               <ts e="T52" id="Seg_447" n="e" s="T51">I </ts>
               <ts e="T53" id="Seg_449" n="e" s="T52">man </ts>
               <ts e="T54" id="Seg_451" n="e" s="T53">kʼelʼlʼe </ts>
               <ts e="T55" id="Seg_453" n="e" s="T54">jübərau. </ts>
               <ts e="T56" id="Seg_455" n="e" s="T55">A </ts>
               <ts e="T57" id="Seg_457" n="e" s="T56">Kriša </ts>
               <ts e="T58" id="Seg_459" n="e" s="T57">üsedʼepat </ts>
               <ts e="T59" id="Seg_461" n="e" s="T58">i </ts>
               <ts e="T60" id="Seg_463" n="e" s="T59">qomdä </ts>
               <ts e="T61" id="Seg_465" n="e" s="T60">mašqoːulǯebat, </ts>
               <ts e="T62" id="Seg_467" n="e" s="T61">ügulǯupbaː </ts>
               <ts e="T63" id="Seg_469" n="e" s="T62">qajla </ts>
               <ts e="T64" id="Seg_471" n="e" s="T63">kʼetčɨdɨt. </ts>
               <ts e="T65" id="Seg_473" n="e" s="T64">A </ts>
               <ts e="T66" id="Seg_475" n="e" s="T65">man </ts>
               <ts e="T67" id="Seg_477" n="e" s="T66">qulupbaːn. </ts>
               <ts e="T68" id="Seg_479" n="e" s="T67">Man </ts>
               <ts e="T69" id="Seg_481" n="e" s="T68">kʼelʼlʼe </ts>
               <ts e="T70" id="Seg_483" n="e" s="T69">mʼalčao. </ts>
               <ts e="T71" id="Seg_485" n="e" s="T70">Tɨmnʼäu </ts>
               <ts e="T72" id="Seg_487" n="e" s="T71">kak </ts>
               <ts e="T73" id="Seg_489" n="e" s="T72">qaːrolʼdä </ts>
               <ts e="T74" id="Seg_491" n="e" s="T73">akoškan </ts>
               <ts e="T75" id="Seg_493" n="e" s="T74">ɨloɣɨn. </ts>
               <ts e="T76" id="Seg_495" n="e" s="T75">Tʼärɨn: </ts>
               <ts e="T77" id="Seg_497" n="e" s="T76">“Soːlaga </ts>
               <ts e="T78" id="Seg_499" n="e" s="T77">näjɣum.” </ts>
               <ts e="T79" id="Seg_501" n="e" s="T78">Me </ts>
               <ts e="T80" id="Seg_503" n="e" s="T79">datao </ts>
               <ts e="T81" id="Seg_505" n="e" s="T80">kocʼiwannaj, </ts>
               <ts e="T82" id="Seg_507" n="e" s="T81">qaːronʼaj. </ts>
               <ts e="T83" id="Seg_509" n="e" s="T82">Me </ts>
               <ts e="T84" id="Seg_511" n="e" s="T83">jass </ts>
               <ts e="T85" id="Seg_513" n="e" s="T84">tunuzaj </ts>
               <ts e="T86" id="Seg_515" n="e" s="T85">što </ts>
               <ts e="T87" id="Seg_517" n="e" s="T86">täp </ts>
               <ts e="T88" id="Seg_519" n="e" s="T87">ügulǯupba </ts>
               <ts e="T89" id="Seg_521" n="e" s="T88">akoškan </ts>
               <ts e="T90" id="Seg_523" n="e" s="T89">iloɣɨn. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_524" s="T1">PVD_1964_YoungBrother_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_525" s="T5">PVD_1964_YoungBrother_nar.002 (001.002)</ta>
            <ta e="T12" id="Seg_526" s="T8">PVD_1964_YoungBrother_nar.003 (001.003)</ta>
            <ta e="T16" id="Seg_527" s="T12">PVD_1964_YoungBrother_nar.004 (001.004)</ta>
            <ta e="T21" id="Seg_528" s="T16">PVD_1964_YoungBrother_nar.005 (001.005)</ta>
            <ta e="T26" id="Seg_529" s="T21">PVD_1964_YoungBrother_nar.006 (001.006)</ta>
            <ta e="T30" id="Seg_530" s="T26">PVD_1964_YoungBrother_nar.007 (001.007)</ta>
            <ta e="T34" id="Seg_531" s="T30">PVD_1964_YoungBrother_nar.008 (001.008)</ta>
            <ta e="T38" id="Seg_532" s="T34">PVD_1964_YoungBrother_nar.009 (001.009)</ta>
            <ta e="T43" id="Seg_533" s="T38">PVD_1964_YoungBrother_nar.010 (001.010)</ta>
            <ta e="T47" id="Seg_534" s="T43">PVD_1964_YoungBrother_nar.011 (001.011)</ta>
            <ta e="T51" id="Seg_535" s="T47">PVD_1964_YoungBrother_nar.012 (001.012)</ta>
            <ta e="T55" id="Seg_536" s="T51">PVD_1964_YoungBrother_nar.013 (001.013)</ta>
            <ta e="T64" id="Seg_537" s="T55">PVD_1964_YoungBrother_nar.014 (001.014)</ta>
            <ta e="T67" id="Seg_538" s="T64">PVD_1964_YoungBrother_nar.015 (001.015)</ta>
            <ta e="T70" id="Seg_539" s="T67">PVD_1964_YoungBrother_nar.016 (001.016)</ta>
            <ta e="T75" id="Seg_540" s="T70">PVD_1964_YoungBrother_nar.017 (001.017)</ta>
            <ta e="T78" id="Seg_541" s="T75">PVD_1964_YoungBrother_nar.018 (001.018)</ta>
            <ta e="T82" id="Seg_542" s="T78">PVD_1964_YoungBrother_nar.019 (001.019)</ta>
            <ta e="T90" id="Seg_543" s="T82">PVD_1964_YoungBrother_nar.020 (001.020)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_544" s="T1">ман пел′гаттъ ′сен̃ку ′ларыбыку′зан.</ta>
            <ta e="T8" id="Seg_545" s="T5">′kwɛрау̹ И′ринам ′сен̃ку.</ta>
            <ta e="T12" id="Seg_546" s="T8">мʼӓɣунӓ ′тʼӱа ман тым′нʼӓу.</ta>
            <ta e="T16" id="Seg_547" s="T12">′а̄мдыдʼӣн ме′нан и kwан′нын.</ta>
            <ta e="T21" id="Seg_548" s="T16">ме ′тӓйербызай, тӓп kwен′ба ′ма̄тkътда.</ta>
            <ta e="T26" id="Seg_549" s="T21">а тӓп а′кошкан ′ылотдъ ′нылʼез(дʼзʼ)иппа.</ta>
            <ta e="T30" id="Seg_550" s="T26">а ме jасс туну′зай.</ta>
            <ta e="T34" id="Seg_551" s="T30">ман И′ринанӓ тʼӓ′ран: kон′сат?</ta>
            <ta e="T38" id="Seg_552" s="T34">(тан пасʼ кы′гат ′kотдугу?)</ta>
            <ta e="T43" id="Seg_553" s="T38">а И′рина тʼӓ′рын: асс kон′сан.</ta>
            <ta e="T47" id="Seg_554" s="T43">ман ′текга kа′jӓмылам кʼе′лʼлʼеу.</ta>
            <ta e="T51" id="Seg_555" s="T47">а И′рина тʼӓ′рын: ке′ты.</ta>
            <ta e="T55" id="Seg_556" s="T51">и ман ′кʼелʼлʼе ′jӱбърау.</ta>
            <ta e="T64" id="Seg_557" s="T55">а Криша ӱ′седʼепат и ′kомдӓ маш′kо̄уlджеб̂ат, ′ӱгуlджупб̂а̄ kай′ла ′кʼеттшыдыт.</ta>
            <ta e="T67" id="Seg_558" s="T64">а ман kуlуп′ба̄н.</ta>
            <ta e="T70" id="Seg_559" s="T67">ман ′кʼелʼлʼе мʼаl′тшао.</ta>
            <ta e="T75" id="Seg_560" s="T70">тым′нʼӓу как ′kа̄ролʼдӓ а′кошкан ы′лоɣын.</ta>
            <ta e="T78" id="Seg_561" s="T75">тʼӓ′рын: ′со̄лага нӓйɣум.</ta>
            <ta e="T82" id="Seg_562" s="T78">ме д̂а тао коцʼиваннай, ′kа̄ро′нʼай.</ta>
            <ta e="T90" id="Seg_563" s="T82">ме jасс туну′зай што тӓп ӱ′гуlджупб̂а а′кошкан и′лоɣын.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_564" s="T1">man pelgattə senku larɨbɨkuzan.</ta>
            <ta e="T8" id="Seg_565" s="T5">qwɛrau̹ Иrinam senku.</ta>
            <ta e="T12" id="Seg_566" s="T8">mʼäɣunä tʼüa man tɨmnʼäu.</ta>
            <ta e="T16" id="Seg_567" s="T12">aːmdɨdʼiːn menan i qwannɨn.</ta>
            <ta e="T21" id="Seg_568" s="T16">me täjerbɨzaj, täp qwenba maːtqətda.</ta>
            <ta e="T26" id="Seg_569" s="T21">a täp akoškan ɨlotdə nɨlʼez(dʼzʼ)ippa.</ta>
            <ta e="T30" id="Seg_570" s="T26">a me jass tunuzaj.</ta>
            <ta e="T34" id="Seg_571" s="T30">man Иrinanä tʼäran: qonsat?</ta>
            <ta e="T38" id="Seg_572" s="T34">(tan pasʼ kɨgat qotdugu?)</ta>
            <ta e="T43" id="Seg_573" s="T38">a Иrina tʼärɨn: ass qonsan.</ta>
            <ta e="T47" id="Seg_574" s="T43">man tekga qajämɨlam kʼelʼlʼeu.</ta>
            <ta e="T51" id="Seg_575" s="T47">a Иrina tʼärɨn: ketɨ.</ta>
            <ta e="T55" id="Seg_576" s="T51">i man kʼelʼlʼe jübərau.</ta>
            <ta e="T64" id="Seg_577" s="T55">a Кriša üsedʼepat i qomdä mašqoːulǯeb̂at, ügulǯupb̂aː qajla kʼettšɨdɨt.</ta>
            <ta e="T67" id="Seg_578" s="T64">a man qulupbaːn.</ta>
            <ta e="T70" id="Seg_579" s="T67">man kʼelʼlʼe mʼaltšao.</ta>
            <ta e="T75" id="Seg_580" s="T70">tɨmnʼäu kak qaːrolʼdä akoškan ɨloɣɨn.</ta>
            <ta e="T78" id="Seg_581" s="T75">tʼärɨn: soːlaga näjɣum.</ta>
            <ta e="T82" id="Seg_582" s="T78">me d̂a tao kocʼiwannaj, qaːronʼaj.</ta>
            <ta e="T90" id="Seg_583" s="T82">me jass tunuzaj što täp ügulǯupb̂a akoškan iloɣɨn.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_584" s="T1">Man pelgattə senku larɨbɨkuzan. </ta>
            <ta e="T8" id="Seg_585" s="T5">Qwɛrau Irinam senku. </ta>
            <ta e="T12" id="Seg_586" s="T8">Mʼäɣunä tʼüa man tɨmnʼäu. </ta>
            <ta e="T16" id="Seg_587" s="T12">Aːmdɨdʼiːn menan i qwannɨn. </ta>
            <ta e="T21" id="Seg_588" s="T16">Me täjerbɨzaj, täp qwenba maːtqətda. </ta>
            <ta e="T26" id="Seg_589" s="T21">A täp akoškan ɨlotdə nɨlʼezippa. </ta>
            <ta e="T30" id="Seg_590" s="T26">A me jass tunuzaj. </ta>
            <ta e="T34" id="Seg_591" s="T30">Man Irinanä tʼäran: “Qonsat? </ta>
            <ta e="T38" id="Seg_592" s="T34">Tan pasʼ kɨgat qotdugu?” </ta>
            <ta e="T43" id="Seg_593" s="T38">A Irina tʼärɨn: “Ass qonsan.” </ta>
            <ta e="T47" id="Seg_594" s="T43">“Man tekga qajämɨlam kʼelʼlʼeu.” </ta>
            <ta e="T51" id="Seg_595" s="T47">A Irina tʼärɨn: “Ketɨ.” </ta>
            <ta e="T55" id="Seg_596" s="T51">I man kʼelʼlʼe jübərau. </ta>
            <ta e="T64" id="Seg_597" s="T55">A Kriša üsedʼepat i qomdä mašqoːulǯebat, ügulǯupbaː qajla kʼetčɨdɨt. </ta>
            <ta e="T67" id="Seg_598" s="T64">A man qulupbaːn. </ta>
            <ta e="T70" id="Seg_599" s="T67">Man kʼelʼlʼe mʼalčao. </ta>
            <ta e="T75" id="Seg_600" s="T70">Tɨmnʼäu kak qaːrolʼdä akoškan ɨloɣɨn. </ta>
            <ta e="T78" id="Seg_601" s="T75">Tʼärɨn: “Soːlaga näjɣum.” </ta>
            <ta e="T82" id="Seg_602" s="T78">Me datao kocʼiwannaj, qaːronʼaj. </ta>
            <ta e="T90" id="Seg_603" s="T82">Me jass tunuzaj što täp ügulǯupba akoškan iloɣɨn. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_604" s="T1">man</ta>
            <ta e="T3" id="Seg_605" s="T2">pel-gattə</ta>
            <ta e="T4" id="Seg_606" s="T3">sen-ku</ta>
            <ta e="T5" id="Seg_607" s="T4">larɨ-bɨ-ku-za-n</ta>
            <ta e="T6" id="Seg_608" s="T5">qwɛra-u</ta>
            <ta e="T7" id="Seg_609" s="T6">Irina-m</ta>
            <ta e="T8" id="Seg_610" s="T7">sen-ku</ta>
            <ta e="T9" id="Seg_611" s="T8">mʼäɣunä</ta>
            <ta e="T10" id="Seg_612" s="T9">tʼü-a</ta>
            <ta e="T11" id="Seg_613" s="T10">man</ta>
            <ta e="T12" id="Seg_614" s="T11">tɨmnʼä-u</ta>
            <ta e="T13" id="Seg_615" s="T12">aːmdɨ-dʼiː-n</ta>
            <ta e="T14" id="Seg_616" s="T13">me-nan</ta>
            <ta e="T15" id="Seg_617" s="T14">i</ta>
            <ta e="T16" id="Seg_618" s="T15">qwan-nɨ-n</ta>
            <ta e="T17" id="Seg_619" s="T16">me</ta>
            <ta e="T18" id="Seg_620" s="T17">täjerbɨ-za-j</ta>
            <ta e="T19" id="Seg_621" s="T18">täp</ta>
            <ta e="T20" id="Seg_622" s="T19">qwen-ba</ta>
            <ta e="T21" id="Seg_623" s="T20">maːt-qətda</ta>
            <ta e="T22" id="Seg_624" s="T21">a</ta>
            <ta e="T23" id="Seg_625" s="T22">täp</ta>
            <ta e="T24" id="Seg_626" s="T23">akoška-n</ta>
            <ta e="T25" id="Seg_627" s="T24">ɨl-o-tdə</ta>
            <ta e="T26" id="Seg_628" s="T25">nɨ-lʼ-ezi-ppa</ta>
            <ta e="T27" id="Seg_629" s="T26">a</ta>
            <ta e="T28" id="Seg_630" s="T27">me</ta>
            <ta e="T29" id="Seg_631" s="T28">jass</ta>
            <ta e="T30" id="Seg_632" s="T29">tunu-za-j</ta>
            <ta e="T31" id="Seg_633" s="T30">man</ta>
            <ta e="T32" id="Seg_634" s="T31">Irina-nä</ta>
            <ta e="T33" id="Seg_635" s="T32">tʼära-n</ta>
            <ta e="T34" id="Seg_636" s="T33">qon-sa-t</ta>
            <ta e="T35" id="Seg_637" s="T34">tat</ta>
            <ta e="T36" id="Seg_638" s="T35">pasʼ</ta>
            <ta e="T37" id="Seg_639" s="T36">kɨga-t</ta>
            <ta e="T38" id="Seg_640" s="T37">qotdu-gu</ta>
            <ta e="T39" id="Seg_641" s="T38">a</ta>
            <ta e="T40" id="Seg_642" s="T39">Irina</ta>
            <ta e="T41" id="Seg_643" s="T40">tʼärɨ-n</ta>
            <ta e="T42" id="Seg_644" s="T41">ass</ta>
            <ta e="T43" id="Seg_645" s="T42">qon-sa-n</ta>
            <ta e="T44" id="Seg_646" s="T43">Man</ta>
            <ta e="T45" id="Seg_647" s="T44">tekga</ta>
            <ta e="T46" id="Seg_648" s="T45">qaj-ä-mɨ-la-m</ta>
            <ta e="T47" id="Seg_649" s="T46">kʼelʼ-lʼe-u</ta>
            <ta e="T48" id="Seg_650" s="T47">a</ta>
            <ta e="T49" id="Seg_651" s="T48">Irina</ta>
            <ta e="T50" id="Seg_652" s="T49">tʼärɨ-n</ta>
            <ta e="T51" id="Seg_653" s="T50">ke-tɨ</ta>
            <ta e="T52" id="Seg_654" s="T51">i</ta>
            <ta e="T53" id="Seg_655" s="T52">man</ta>
            <ta e="T54" id="Seg_656" s="T53">kʼelʼ-lʼe</ta>
            <ta e="T55" id="Seg_657" s="T54">jübə-ra-u</ta>
            <ta e="T56" id="Seg_658" s="T55">a</ta>
            <ta e="T57" id="Seg_659" s="T56">Kriša</ta>
            <ta e="T58" id="Seg_660" s="T57">üse-dʼe-pa-t</ta>
            <ta e="T59" id="Seg_661" s="T58">i</ta>
            <ta e="T60" id="Seg_662" s="T59">qo-m-dä</ta>
            <ta e="T61" id="Seg_663" s="T60">mašqoːu-lǯe-ba-t</ta>
            <ta e="T62" id="Seg_664" s="T61">ügulǯu-pbaː</ta>
            <ta e="T63" id="Seg_665" s="T62">qaj-la</ta>
            <ta e="T64" id="Seg_666" s="T63">kʼet-čɨ-dɨt</ta>
            <ta e="T65" id="Seg_667" s="T64">a</ta>
            <ta e="T66" id="Seg_668" s="T65">man</ta>
            <ta e="T67" id="Seg_669" s="T66">qulupbaː-n</ta>
            <ta e="T68" id="Seg_670" s="T67">man</ta>
            <ta e="T69" id="Seg_671" s="T68">kʼelʼ-lʼe</ta>
            <ta e="T70" id="Seg_672" s="T69">mʼalča-o</ta>
            <ta e="T71" id="Seg_673" s="T70">tɨmnʼä-u</ta>
            <ta e="T72" id="Seg_674" s="T71">kak</ta>
            <ta e="T73" id="Seg_675" s="T72">qaːr-olʼ-dä</ta>
            <ta e="T74" id="Seg_676" s="T73">akoška-n</ta>
            <ta e="T75" id="Seg_677" s="T74">ɨl-o-ɣɨn</ta>
            <ta e="T76" id="Seg_678" s="T75">tʼärɨ-n</ta>
            <ta e="T77" id="Seg_679" s="T76">soː-laga</ta>
            <ta e="T78" id="Seg_680" s="T77">nä-j-ɣum</ta>
            <ta e="T79" id="Seg_681" s="T78">me</ta>
            <ta e="T80" id="Seg_682" s="T79">datao</ta>
            <ta e="T81" id="Seg_683" s="T80">kocʼiwan-na-j</ta>
            <ta e="T82" id="Seg_684" s="T81">qaːr-onʼ-a-j</ta>
            <ta e="T83" id="Seg_685" s="T82">me</ta>
            <ta e="T84" id="Seg_686" s="T83">jass</ta>
            <ta e="T85" id="Seg_687" s="T84">tunu-za-j</ta>
            <ta e="T86" id="Seg_688" s="T85">što</ta>
            <ta e="T87" id="Seg_689" s="T86">täp</ta>
            <ta e="T88" id="Seg_690" s="T87">ügulǯu-pba</ta>
            <ta e="T89" id="Seg_691" s="T88">akoška-n</ta>
            <ta e="T90" id="Seg_692" s="T89">il-o-ɣɨn</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_693" s="T1">man</ta>
            <ta e="T3" id="Seg_694" s="T2">*pel-kattə</ta>
            <ta e="T4" id="Seg_695" s="T3">seːŋ-gu</ta>
            <ta e="T5" id="Seg_696" s="T4">*larɨ-mbɨ-ku-sɨ-ŋ</ta>
            <ta e="T6" id="Seg_697" s="T5">qwɨrɨ-w</ta>
            <ta e="T7" id="Seg_698" s="T6">Irina-m</ta>
            <ta e="T8" id="Seg_699" s="T7">seːŋ-gu</ta>
            <ta e="T9" id="Seg_700" s="T8">meguni</ta>
            <ta e="T10" id="Seg_701" s="T9">tüː-nɨ</ta>
            <ta e="T11" id="Seg_702" s="T10">man</ta>
            <ta e="T12" id="Seg_703" s="T11">tɨmnʼa-w</ta>
            <ta e="T13" id="Seg_704" s="T12">amdɨ-dʼi-n</ta>
            <ta e="T14" id="Seg_705" s="T13">me-nan</ta>
            <ta e="T15" id="Seg_706" s="T14">i</ta>
            <ta e="T16" id="Seg_707" s="T15">qwan-nɨ-n</ta>
            <ta e="T17" id="Seg_708" s="T16">me</ta>
            <ta e="T18" id="Seg_709" s="T17">tärba-sɨ-j</ta>
            <ta e="T19" id="Seg_710" s="T18">täp</ta>
            <ta e="T20" id="Seg_711" s="T19">qwan-mbɨ</ta>
            <ta e="T21" id="Seg_712" s="T20">maːt-qɨntɨ</ta>
            <ta e="T22" id="Seg_713" s="T21">a</ta>
            <ta e="T23" id="Seg_714" s="T22">täp</ta>
            <ta e="T24" id="Seg_715" s="T23">akoška-n</ta>
            <ta e="T25" id="Seg_716" s="T24">ɨl-ɨ-ntə</ta>
            <ta e="T26" id="Seg_717" s="T25">nɨ-l-lǯi-mbɨ</ta>
            <ta e="T27" id="Seg_718" s="T26">a</ta>
            <ta e="T28" id="Seg_719" s="T27">me</ta>
            <ta e="T29" id="Seg_720" s="T28">asa</ta>
            <ta e="T30" id="Seg_721" s="T29">tonu-sɨ-j</ta>
            <ta e="T31" id="Seg_722" s="T30">man</ta>
            <ta e="T32" id="Seg_723" s="T31">Irina-nä</ta>
            <ta e="T33" id="Seg_724" s="T32">tʼärɨ-ŋ</ta>
            <ta e="T34" id="Seg_725" s="T33">qondu-psot-ntə</ta>
            <ta e="T35" id="Seg_726" s="T34">tan</ta>
            <ta e="T36" id="Seg_727" s="T35">pasʼ</ta>
            <ta e="T37" id="Seg_728" s="T36">kɨgɨ-ntə</ta>
            <ta e="T38" id="Seg_729" s="T37">qondu-gu</ta>
            <ta e="T39" id="Seg_730" s="T38">a</ta>
            <ta e="T40" id="Seg_731" s="T39">Irina</ta>
            <ta e="T41" id="Seg_732" s="T40">tʼärɨ-n</ta>
            <ta e="T42" id="Seg_733" s="T41">asa</ta>
            <ta e="T43" id="Seg_734" s="T42">qondu-psot-ŋ</ta>
            <ta e="T44" id="Seg_735" s="T43">man</ta>
            <ta e="T45" id="Seg_736" s="T44">tekka</ta>
            <ta e="T46" id="Seg_737" s="T45">qaj-ɨ-mɨ-la-m</ta>
            <ta e="T47" id="Seg_738" s="T46">ket-lä-w</ta>
            <ta e="T48" id="Seg_739" s="T47">a</ta>
            <ta e="T49" id="Seg_740" s="T48">Irina</ta>
            <ta e="T50" id="Seg_741" s="T49">tʼärɨ-n</ta>
            <ta e="T51" id="Seg_742" s="T50">ket-etɨ</ta>
            <ta e="T52" id="Seg_743" s="T51">i</ta>
            <ta e="T53" id="Seg_744" s="T52">man</ta>
            <ta e="T54" id="Seg_745" s="T53">ket-le</ta>
            <ta e="T55" id="Seg_746" s="T54">übɨ-rɨ-w</ta>
            <ta e="T56" id="Seg_747" s="T55">a</ta>
            <ta e="T57" id="Seg_748" s="T56">Kriša</ta>
            <ta e="T58" id="Seg_749" s="T57">üsʼe-dʼi-mbɨ-t</ta>
            <ta e="T59" id="Seg_750" s="T58">i</ta>
            <ta e="T60" id="Seg_751" s="T59">qo-m-tə</ta>
            <ta e="T61" id="Seg_752" s="T60">mašqoːu-lǯi-mbɨ-t</ta>
            <ta e="T62" id="Seg_753" s="T61">üŋgulǯu-mbɨ</ta>
            <ta e="T63" id="Seg_754" s="T62">qaj-la</ta>
            <ta e="T64" id="Seg_755" s="T63">ket-enǯɨ-tɨn</ta>
            <ta e="T65" id="Seg_756" s="T64">a</ta>
            <ta e="T66" id="Seg_757" s="T65">man</ta>
            <ta e="T67" id="Seg_758" s="T66">kulubu-ŋ</ta>
            <ta e="T68" id="Seg_759" s="T67">man</ta>
            <ta e="T69" id="Seg_760" s="T68">ket-le</ta>
            <ta e="T70" id="Seg_761" s="T69">malčə-w</ta>
            <ta e="T71" id="Seg_762" s="T70">tɨmnʼa-w</ta>
            <ta e="T72" id="Seg_763" s="T71">kak</ta>
            <ta e="T73" id="Seg_764" s="T72">qarɨ-ol-t</ta>
            <ta e="T74" id="Seg_765" s="T73">akoška-n</ta>
            <ta e="T75" id="Seg_766" s="T74">ɨl-ɨ-qɨn</ta>
            <ta e="T76" id="Seg_767" s="T75">tʼärɨ-n</ta>
            <ta e="T77" id="Seg_768" s="T76">soː-laq</ta>
            <ta e="T78" id="Seg_769" s="T77">ne-lʼ-qum</ta>
            <ta e="T79" id="Seg_770" s="T78">me</ta>
            <ta e="T80" id="Seg_771" s="T79">tatawa</ta>
            <ta e="T81" id="Seg_772" s="T80">kɨcʼwat-nɨ-j</ta>
            <ta e="T82" id="Seg_773" s="T81">qarɨ-ol-nɨ-j</ta>
            <ta e="T83" id="Seg_774" s="T82">me</ta>
            <ta e="T84" id="Seg_775" s="T83">asa</ta>
            <ta e="T85" id="Seg_776" s="T84">tonu-sɨ-j</ta>
            <ta e="T86" id="Seg_777" s="T85">što</ta>
            <ta e="T87" id="Seg_778" s="T86">täp</ta>
            <ta e="T88" id="Seg_779" s="T87">üŋgulǯu-mbɨ</ta>
            <ta e="T89" id="Seg_780" s="T88">akoška-n</ta>
            <ta e="T90" id="Seg_781" s="T89">ɨl-ɨ-qɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_782" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_783" s="T2">friend-CAR</ta>
            <ta e="T4" id="Seg_784" s="T3">overnight-INF</ta>
            <ta e="T5" id="Seg_785" s="T4">get.afraid-DUR-HAB-PST-1SG.S</ta>
            <ta e="T6" id="Seg_786" s="T5">call-1SG.O</ta>
            <ta e="T7" id="Seg_787" s="T6">Irina-ACC</ta>
            <ta e="T8" id="Seg_788" s="T7">overnight-INF</ta>
            <ta e="T9" id="Seg_789" s="T8">we.DU.ALL</ta>
            <ta e="T10" id="Seg_790" s="T9">come-CO.[3SG.S]</ta>
            <ta e="T11" id="Seg_791" s="T10">I.GEN</ta>
            <ta e="T12" id="Seg_792" s="T11">brother.[NOM]-1SG</ta>
            <ta e="T13" id="Seg_793" s="T12">sit-DRV-3SG.S</ta>
            <ta e="T14" id="Seg_794" s="T13">we-ADES</ta>
            <ta e="T15" id="Seg_795" s="T14">and</ta>
            <ta e="T16" id="Seg_796" s="T15">leave-CO-3SG.S</ta>
            <ta e="T17" id="Seg_797" s="T16">we.[NOM]</ta>
            <ta e="T18" id="Seg_798" s="T17">think-PST-1DU</ta>
            <ta e="T19" id="Seg_799" s="T18">(s)he.[NOM]</ta>
            <ta e="T20" id="Seg_800" s="T19">leave-PST.NAR.[3SG.S]</ta>
            <ta e="T21" id="Seg_801" s="T20">house-ILL.3SG</ta>
            <ta e="T22" id="Seg_802" s="T21">and</ta>
            <ta e="T23" id="Seg_803" s="T22">(s)he.[NOM]</ta>
            <ta e="T24" id="Seg_804" s="T23">window-GEN</ta>
            <ta e="T25" id="Seg_805" s="T24">space.under-EP-ILL</ta>
            <ta e="T26" id="Seg_806" s="T25">stand-INCH-PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T27" id="Seg_807" s="T26">and</ta>
            <ta e="T28" id="Seg_808" s="T27">we.[NOM]</ta>
            <ta e="T29" id="Seg_809" s="T28">NEG</ta>
            <ta e="T30" id="Seg_810" s="T29">know-PST-1DU</ta>
            <ta e="T31" id="Seg_811" s="T30">I.NOM</ta>
            <ta e="T32" id="Seg_812" s="T31">Irina-ALL</ta>
            <ta e="T33" id="Seg_813" s="T32">say-1SG.S</ta>
            <ta e="T34" id="Seg_814" s="T33">sleep-DEB-2SG.S</ta>
            <ta e="T35" id="Seg_815" s="T34">you.SG.NOM</ta>
            <ta e="T36" id="Seg_816" s="T35">%%</ta>
            <ta e="T37" id="Seg_817" s="T36">want-2SG.S</ta>
            <ta e="T38" id="Seg_818" s="T37">sleep-INF</ta>
            <ta e="T39" id="Seg_819" s="T38">and</ta>
            <ta e="T40" id="Seg_820" s="T39">Irina.[NOM]</ta>
            <ta e="T41" id="Seg_821" s="T40">say-3SG.S</ta>
            <ta e="T42" id="Seg_822" s="T41">NEG</ta>
            <ta e="T43" id="Seg_823" s="T42">sleep-DEB-1SG.S</ta>
            <ta e="T44" id="Seg_824" s="T43">I.NOM</ta>
            <ta e="T45" id="Seg_825" s="T44">you.ALL</ta>
            <ta e="T46" id="Seg_826" s="T45">what-EP-something-PL-ACC</ta>
            <ta e="T47" id="Seg_827" s="T46">say-OPT-1SG.O</ta>
            <ta e="T48" id="Seg_828" s="T47">and</ta>
            <ta e="T49" id="Seg_829" s="T48">Irina.[NOM]</ta>
            <ta e="T50" id="Seg_830" s="T49">say-3SG.S</ta>
            <ta e="T51" id="Seg_831" s="T50">say-IMP.2SG.O</ta>
            <ta e="T52" id="Seg_832" s="T51">and</ta>
            <ta e="T53" id="Seg_833" s="T52">I.NOM</ta>
            <ta e="T54" id="Seg_834" s="T53">say-CVB</ta>
            <ta e="T55" id="Seg_835" s="T54">begin-DRV-1SG.O</ta>
            <ta e="T56" id="Seg_836" s="T55">and</ta>
            <ta e="T57" id="Seg_837" s="T56">Grisha.[NOM]</ta>
            <ta e="T58" id="Seg_838" s="T57">hear-DRV-PST.NAR-3SG.O</ta>
            <ta e="T59" id="Seg_839" s="T58">and</ta>
            <ta e="T60" id="Seg_840" s="T59">ear-ACC-3SG</ta>
            <ta e="T61" id="Seg_841" s="T60">%%-TR-PST.NAR-3SG.O</ta>
            <ta e="T62" id="Seg_842" s="T61">listen-DUR.[3SG.S]</ta>
            <ta e="T63" id="Seg_843" s="T62">what-PL.[NOM]</ta>
            <ta e="T64" id="Seg_844" s="T63">say-FUT-3PL</ta>
            <ta e="T65" id="Seg_845" s="T64">and</ta>
            <ta e="T66" id="Seg_846" s="T65">I.NOM</ta>
            <ta e="T67" id="Seg_847" s="T66">speak-1SG.S</ta>
            <ta e="T68" id="Seg_848" s="T67">I.NOM</ta>
            <ta e="T69" id="Seg_849" s="T68">say-CVB</ta>
            <ta e="T70" id="Seg_850" s="T69">stop-1SG.O</ta>
            <ta e="T71" id="Seg_851" s="T70">brother.[NOM]-1SG</ta>
            <ta e="T72" id="Seg_852" s="T71">suddenly</ta>
            <ta e="T73" id="Seg_853" s="T72">shout-MOM-3SG.O</ta>
            <ta e="T74" id="Seg_854" s="T73">window-GEN</ta>
            <ta e="T75" id="Seg_855" s="T74">space.under-EP-LOC</ta>
            <ta e="T76" id="Seg_856" s="T75">say-3SG.S</ta>
            <ta e="T77" id="Seg_857" s="T76">good-ATTEN</ta>
            <ta e="T78" id="Seg_858" s="T77">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T79" id="Seg_859" s="T78">we.[NOM]</ta>
            <ta e="T80" id="Seg_860" s="T79">to.such.extent</ta>
            <ta e="T81" id="Seg_861" s="T80">get.scared-CO-1DU</ta>
            <ta e="T82" id="Seg_862" s="T81">shout-MOM-CO-1DU</ta>
            <ta e="T83" id="Seg_863" s="T82">we.[NOM]</ta>
            <ta e="T84" id="Seg_864" s="T83">NEG</ta>
            <ta e="T85" id="Seg_865" s="T84">know-PST-1DU</ta>
            <ta e="T86" id="Seg_866" s="T85">that</ta>
            <ta e="T87" id="Seg_867" s="T86">(s)he.[NOM]</ta>
            <ta e="T88" id="Seg_868" s="T87">listen-DUR.[3SG.S]</ta>
            <ta e="T89" id="Seg_869" s="T88">window-GEN</ta>
            <ta e="T90" id="Seg_870" s="T89">space.under-EP-LOC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_871" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_872" s="T2">друг-CAR</ta>
            <ta e="T4" id="Seg_873" s="T3">ночевать-INF</ta>
            <ta e="T5" id="Seg_874" s="T4">испугаться-DUR-HAB-PST-1SG.S</ta>
            <ta e="T6" id="Seg_875" s="T5">позвать-1SG.O</ta>
            <ta e="T7" id="Seg_876" s="T6">Ирина-ACC</ta>
            <ta e="T8" id="Seg_877" s="T7">ночевать-INF</ta>
            <ta e="T9" id="Seg_878" s="T8">мы.DU.ALL</ta>
            <ta e="T10" id="Seg_879" s="T9">прийти-CO.[3SG.S]</ta>
            <ta e="T11" id="Seg_880" s="T10">я.GEN</ta>
            <ta e="T12" id="Seg_881" s="T11">брат.[NOM]-1SG</ta>
            <ta e="T13" id="Seg_882" s="T12">сидеть-DRV-3SG.S</ta>
            <ta e="T14" id="Seg_883" s="T13">мы-ADES</ta>
            <ta e="T15" id="Seg_884" s="T14">и</ta>
            <ta e="T16" id="Seg_885" s="T15">отправиться-CO-3SG.S</ta>
            <ta e="T17" id="Seg_886" s="T16">мы.[NOM]</ta>
            <ta e="T18" id="Seg_887" s="T17">думать-PST-1DU</ta>
            <ta e="T19" id="Seg_888" s="T18">он(а).[NOM]</ta>
            <ta e="T20" id="Seg_889" s="T19">отправиться-PST.NAR.[3SG.S]</ta>
            <ta e="T21" id="Seg_890" s="T20">дом-ILL.3SG</ta>
            <ta e="T22" id="Seg_891" s="T21">а</ta>
            <ta e="T23" id="Seg_892" s="T22">он(а).[NOM]</ta>
            <ta e="T24" id="Seg_893" s="T23">окно-GEN</ta>
            <ta e="T25" id="Seg_894" s="T24">пространство.под-EP-ILL</ta>
            <ta e="T26" id="Seg_895" s="T25">стоять-INCH-PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T27" id="Seg_896" s="T26">а</ta>
            <ta e="T28" id="Seg_897" s="T27">мы.[NOM]</ta>
            <ta e="T29" id="Seg_898" s="T28">NEG</ta>
            <ta e="T30" id="Seg_899" s="T29">знать-PST-1DU</ta>
            <ta e="T31" id="Seg_900" s="T30">я.NOM</ta>
            <ta e="T32" id="Seg_901" s="T31">Ирина-ALL</ta>
            <ta e="T33" id="Seg_902" s="T32">сказать-1SG.S</ta>
            <ta e="T34" id="Seg_903" s="T33">спать-DEB-2SG.S</ta>
            <ta e="T35" id="Seg_904" s="T34">ты.NOM</ta>
            <ta e="T36" id="Seg_905" s="T35">%%</ta>
            <ta e="T37" id="Seg_906" s="T36">хотеть-2SG.S</ta>
            <ta e="T38" id="Seg_907" s="T37">спать-INF</ta>
            <ta e="T39" id="Seg_908" s="T38">а</ta>
            <ta e="T40" id="Seg_909" s="T39">Ирина.[NOM]</ta>
            <ta e="T41" id="Seg_910" s="T40">сказать-3SG.S</ta>
            <ta e="T42" id="Seg_911" s="T41">NEG</ta>
            <ta e="T43" id="Seg_912" s="T42">спать-DEB-1SG.S</ta>
            <ta e="T44" id="Seg_913" s="T43">я.NOM</ta>
            <ta e="T45" id="Seg_914" s="T44">ты.ALL</ta>
            <ta e="T46" id="Seg_915" s="T45">что-EP-нечто-PL-ACC</ta>
            <ta e="T47" id="Seg_916" s="T46">сказать-OPT-1SG.O</ta>
            <ta e="T48" id="Seg_917" s="T47">а</ta>
            <ta e="T49" id="Seg_918" s="T48">Ирина.[NOM]</ta>
            <ta e="T50" id="Seg_919" s="T49">сказать-3SG.S</ta>
            <ta e="T51" id="Seg_920" s="T50">сказать-IMP.2SG.O</ta>
            <ta e="T52" id="Seg_921" s="T51">и</ta>
            <ta e="T53" id="Seg_922" s="T52">я.NOM</ta>
            <ta e="T54" id="Seg_923" s="T53">сказать-CVB</ta>
            <ta e="T55" id="Seg_924" s="T54">начать-DRV-1SG.O</ta>
            <ta e="T56" id="Seg_925" s="T55">а</ta>
            <ta e="T57" id="Seg_926" s="T56">Гриша.[NOM]</ta>
            <ta e="T58" id="Seg_927" s="T57">услышать-DRV-PST.NAR-3SG.O</ta>
            <ta e="T59" id="Seg_928" s="T58">и</ta>
            <ta e="T60" id="Seg_929" s="T59">ухо-ACC-3SG</ta>
            <ta e="T61" id="Seg_930" s="T60">%%-TR-PST.NAR-3SG.O</ta>
            <ta e="T62" id="Seg_931" s="T61">послушать-DUR.[3SG.S]</ta>
            <ta e="T63" id="Seg_932" s="T62">что-PL.[NOM]</ta>
            <ta e="T64" id="Seg_933" s="T63">сказать-FUT-3PL</ta>
            <ta e="T65" id="Seg_934" s="T64">а</ta>
            <ta e="T66" id="Seg_935" s="T65">я.NOM</ta>
            <ta e="T67" id="Seg_936" s="T66">говорить-1SG.S</ta>
            <ta e="T68" id="Seg_937" s="T67">я.NOM</ta>
            <ta e="T69" id="Seg_938" s="T68">сказать-CVB</ta>
            <ta e="T70" id="Seg_939" s="T69">закончить-1SG.O</ta>
            <ta e="T71" id="Seg_940" s="T70">брат.[NOM]-1SG</ta>
            <ta e="T72" id="Seg_941" s="T71">как</ta>
            <ta e="T73" id="Seg_942" s="T72">кричать-MOM-3SG.O</ta>
            <ta e="T74" id="Seg_943" s="T73">окно-GEN</ta>
            <ta e="T75" id="Seg_944" s="T74">пространство.под-EP-LOC</ta>
            <ta e="T76" id="Seg_945" s="T75">сказать-3SG.S</ta>
            <ta e="T77" id="Seg_946" s="T76">хороший-ATTEN</ta>
            <ta e="T78" id="Seg_947" s="T77">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T79" id="Seg_948" s="T78">мы.[NOM]</ta>
            <ta e="T80" id="Seg_949" s="T79">до.того</ta>
            <ta e="T81" id="Seg_950" s="T80">испугаться-CO-1DU</ta>
            <ta e="T82" id="Seg_951" s="T81">кричать-MOM-CO-1DU</ta>
            <ta e="T83" id="Seg_952" s="T82">мы.[NOM]</ta>
            <ta e="T84" id="Seg_953" s="T83">NEG</ta>
            <ta e="T85" id="Seg_954" s="T84">знать-PST-1DU</ta>
            <ta e="T86" id="Seg_955" s="T85">что</ta>
            <ta e="T87" id="Seg_956" s="T86">он(а).[NOM]</ta>
            <ta e="T88" id="Seg_957" s="T87">послушать-DUR.[3SG.S]</ta>
            <ta e="T89" id="Seg_958" s="T88">окно-GEN</ta>
            <ta e="T90" id="Seg_959" s="T89">пространство.под-EP-LOC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_960" s="T1">pers</ta>
            <ta e="T3" id="Seg_961" s="T2">n-n&gt;n</ta>
            <ta e="T4" id="Seg_962" s="T3">v-v:inf</ta>
            <ta e="T5" id="Seg_963" s="T4">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_964" s="T5">v-v:pn</ta>
            <ta e="T7" id="Seg_965" s="T6">nprop-n:case</ta>
            <ta e="T8" id="Seg_966" s="T7">v-v:inf</ta>
            <ta e="T9" id="Seg_967" s="T8">pers</ta>
            <ta e="T10" id="Seg_968" s="T9">v-v:ins.[v:pn]</ta>
            <ta e="T11" id="Seg_969" s="T10">pers</ta>
            <ta e="T12" id="Seg_970" s="T11">n.[n:case]-n:poss</ta>
            <ta e="T13" id="Seg_971" s="T12">v-v&gt;v-v:pn</ta>
            <ta e="T14" id="Seg_972" s="T13">pers-n:case</ta>
            <ta e="T15" id="Seg_973" s="T14">conj</ta>
            <ta e="T16" id="Seg_974" s="T15">v-v:ins-v:pn</ta>
            <ta e="T17" id="Seg_975" s="T16">pers.[n:case]</ta>
            <ta e="T18" id="Seg_976" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_977" s="T18">pers.[n:case]</ta>
            <ta e="T20" id="Seg_978" s="T19">v-v:tense.[v:pn]</ta>
            <ta e="T21" id="Seg_979" s="T20">n-n:case.poss</ta>
            <ta e="T22" id="Seg_980" s="T21">conj</ta>
            <ta e="T23" id="Seg_981" s="T22">pers.[n:case]</ta>
            <ta e="T24" id="Seg_982" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_983" s="T24">n-n:ins-n:case</ta>
            <ta e="T26" id="Seg_984" s="T25">v-v&gt;v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T27" id="Seg_985" s="T26">conj</ta>
            <ta e="T28" id="Seg_986" s="T27">pers.[n:case]</ta>
            <ta e="T29" id="Seg_987" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_988" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_989" s="T30">pers</ta>
            <ta e="T32" id="Seg_990" s="T31">nprop-n:case</ta>
            <ta e="T33" id="Seg_991" s="T32">v-v:pn</ta>
            <ta e="T34" id="Seg_992" s="T33">v-v&gt;v-v:pn</ta>
            <ta e="T35" id="Seg_993" s="T34">pers</ta>
            <ta e="T36" id="Seg_994" s="T35">%%</ta>
            <ta e="T37" id="Seg_995" s="T36">v-v:pn</ta>
            <ta e="T38" id="Seg_996" s="T37">v-v:inf</ta>
            <ta e="T39" id="Seg_997" s="T38">conj</ta>
            <ta e="T40" id="Seg_998" s="T39">nprop.[n:case]</ta>
            <ta e="T41" id="Seg_999" s="T40">v-v:pn</ta>
            <ta e="T42" id="Seg_1000" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_1001" s="T42">v-v&gt;v-v:pn</ta>
            <ta e="T44" id="Seg_1002" s="T43">pers</ta>
            <ta e="T45" id="Seg_1003" s="T44">pers</ta>
            <ta e="T46" id="Seg_1004" s="T45">interrog-n:ins-n-n:num-n:case</ta>
            <ta e="T47" id="Seg_1005" s="T46">v-v:mood-v:pn</ta>
            <ta e="T48" id="Seg_1006" s="T47">conj</ta>
            <ta e="T49" id="Seg_1007" s="T48">nprop.[n:case]</ta>
            <ta e="T50" id="Seg_1008" s="T49">v-v:pn</ta>
            <ta e="T51" id="Seg_1009" s="T50">v-v:mood.pn</ta>
            <ta e="T52" id="Seg_1010" s="T51">conj</ta>
            <ta e="T53" id="Seg_1011" s="T52">pers</ta>
            <ta e="T54" id="Seg_1012" s="T53">v-v&gt;adv</ta>
            <ta e="T55" id="Seg_1013" s="T54">v-v&gt;v-v:pn</ta>
            <ta e="T56" id="Seg_1014" s="T55">conj</ta>
            <ta e="T57" id="Seg_1015" s="T56">nprop.[n:case]</ta>
            <ta e="T58" id="Seg_1016" s="T57">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_1017" s="T58">conj</ta>
            <ta e="T60" id="Seg_1018" s="T59">n-n:case-n:poss</ta>
            <ta e="T61" id="Seg_1019" s="T60">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_1020" s="T61">v-v&gt;v.[v:pn]</ta>
            <ta e="T63" id="Seg_1021" s="T62">interrog-n:num.[n:case]</ta>
            <ta e="T64" id="Seg_1022" s="T63">v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_1023" s="T64">conj</ta>
            <ta e="T66" id="Seg_1024" s="T65">pers</ta>
            <ta e="T67" id="Seg_1025" s="T66">v-v:pn</ta>
            <ta e="T68" id="Seg_1026" s="T67">pers</ta>
            <ta e="T69" id="Seg_1027" s="T68">v-v&gt;adv</ta>
            <ta e="T70" id="Seg_1028" s="T69">v-v:pn</ta>
            <ta e="T71" id="Seg_1029" s="T70">n.[n:case]-n:poss</ta>
            <ta e="T72" id="Seg_1030" s="T71">adv</ta>
            <ta e="T73" id="Seg_1031" s="T72">v-v&gt;v-v:pn</ta>
            <ta e="T74" id="Seg_1032" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_1033" s="T74">n-n:ins-n:case</ta>
            <ta e="T76" id="Seg_1034" s="T75">v-v:pn</ta>
            <ta e="T77" id="Seg_1035" s="T76">adj-adj&gt;adj</ta>
            <ta e="T78" id="Seg_1036" s="T77">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T79" id="Seg_1037" s="T78">pers.[n:case]</ta>
            <ta e="T80" id="Seg_1038" s="T79">adv</ta>
            <ta e="T81" id="Seg_1039" s="T80">v-v:ins-v:pn</ta>
            <ta e="T82" id="Seg_1040" s="T81">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T83" id="Seg_1041" s="T82">pers.[n:case]</ta>
            <ta e="T84" id="Seg_1042" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_1043" s="T84">v-v:tense-v:pn</ta>
            <ta e="T86" id="Seg_1044" s="T85">conj</ta>
            <ta e="T87" id="Seg_1045" s="T86">pers.[n:case]</ta>
            <ta e="T88" id="Seg_1046" s="T87">v-v&gt;v.[v:pn]</ta>
            <ta e="T89" id="Seg_1047" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_1048" s="T89">n-n:ins-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1049" s="T1">pers</ta>
            <ta e="T3" id="Seg_1050" s="T2">n</ta>
            <ta e="T4" id="Seg_1051" s="T3">v</ta>
            <ta e="T5" id="Seg_1052" s="T4">v</ta>
            <ta e="T6" id="Seg_1053" s="T5">v</ta>
            <ta e="T7" id="Seg_1054" s="T6">nprop</ta>
            <ta e="T8" id="Seg_1055" s="T7">v</ta>
            <ta e="T9" id="Seg_1056" s="T8">pers</ta>
            <ta e="T10" id="Seg_1057" s="T9">v</ta>
            <ta e="T11" id="Seg_1058" s="T10">pers</ta>
            <ta e="T12" id="Seg_1059" s="T11">n</ta>
            <ta e="T13" id="Seg_1060" s="T12">v</ta>
            <ta e="T14" id="Seg_1061" s="T13">pers</ta>
            <ta e="T15" id="Seg_1062" s="T14">conj</ta>
            <ta e="T16" id="Seg_1063" s="T15">v</ta>
            <ta e="T17" id="Seg_1064" s="T16">pers</ta>
            <ta e="T18" id="Seg_1065" s="T17">v</ta>
            <ta e="T19" id="Seg_1066" s="T18">pers</ta>
            <ta e="T20" id="Seg_1067" s="T19">v</ta>
            <ta e="T21" id="Seg_1068" s="T20">n</ta>
            <ta e="T22" id="Seg_1069" s="T21">conj</ta>
            <ta e="T23" id="Seg_1070" s="T22">pers</ta>
            <ta e="T24" id="Seg_1071" s="T23">n</ta>
            <ta e="T25" id="Seg_1072" s="T24">pp</ta>
            <ta e="T26" id="Seg_1073" s="T25">v</ta>
            <ta e="T27" id="Seg_1074" s="T26">conj</ta>
            <ta e="T28" id="Seg_1075" s="T27">pers</ta>
            <ta e="T29" id="Seg_1076" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_1077" s="T29">v</ta>
            <ta e="T31" id="Seg_1078" s="T30">pers</ta>
            <ta e="T32" id="Seg_1079" s="T31">n</ta>
            <ta e="T33" id="Seg_1080" s="T32">v</ta>
            <ta e="T34" id="Seg_1081" s="T33">v</ta>
            <ta e="T35" id="Seg_1082" s="T34">pers</ta>
            <ta e="T37" id="Seg_1083" s="T36">v</ta>
            <ta e="T38" id="Seg_1084" s="T37">v</ta>
            <ta e="T39" id="Seg_1085" s="T38">conj</ta>
            <ta e="T40" id="Seg_1086" s="T39">nprop</ta>
            <ta e="T41" id="Seg_1087" s="T40">v</ta>
            <ta e="T42" id="Seg_1088" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_1089" s="T42">v</ta>
            <ta e="T44" id="Seg_1090" s="T43">pers</ta>
            <ta e="T45" id="Seg_1091" s="T44">pers</ta>
            <ta e="T46" id="Seg_1092" s="T45">pro</ta>
            <ta e="T47" id="Seg_1093" s="T46">v</ta>
            <ta e="T48" id="Seg_1094" s="T47">conj</ta>
            <ta e="T49" id="Seg_1095" s="T48">nprop</ta>
            <ta e="T50" id="Seg_1096" s="T49">v</ta>
            <ta e="T51" id="Seg_1097" s="T50">v</ta>
            <ta e="T52" id="Seg_1098" s="T51">conj</ta>
            <ta e="T53" id="Seg_1099" s="T52">pers</ta>
            <ta e="T54" id="Seg_1100" s="T53">v</ta>
            <ta e="T55" id="Seg_1101" s="T54">v</ta>
            <ta e="T56" id="Seg_1102" s="T55">conj</ta>
            <ta e="T57" id="Seg_1103" s="T56">nprop</ta>
            <ta e="T58" id="Seg_1104" s="T57">v</ta>
            <ta e="T59" id="Seg_1105" s="T58">conj</ta>
            <ta e="T60" id="Seg_1106" s="T59">n</ta>
            <ta e="T61" id="Seg_1107" s="T60">v</ta>
            <ta e="T62" id="Seg_1108" s="T61">v</ta>
            <ta e="T63" id="Seg_1109" s="T62">interrog</ta>
            <ta e="T64" id="Seg_1110" s="T63">v</ta>
            <ta e="T65" id="Seg_1111" s="T64">conj</ta>
            <ta e="T66" id="Seg_1112" s="T65">pers</ta>
            <ta e="T67" id="Seg_1113" s="T66">v</ta>
            <ta e="T68" id="Seg_1114" s="T67">pers</ta>
            <ta e="T69" id="Seg_1115" s="T68">v</ta>
            <ta e="T70" id="Seg_1116" s="T69">v</ta>
            <ta e="T71" id="Seg_1117" s="T70">n</ta>
            <ta e="T72" id="Seg_1118" s="T71">adv</ta>
            <ta e="T73" id="Seg_1119" s="T72">v</ta>
            <ta e="T74" id="Seg_1120" s="T73">n</ta>
            <ta e="T75" id="Seg_1121" s="T74">n</ta>
            <ta e="T76" id="Seg_1122" s="T75">v</ta>
            <ta e="T77" id="Seg_1123" s="T76">adj</ta>
            <ta e="T78" id="Seg_1124" s="T77">n</ta>
            <ta e="T79" id="Seg_1125" s="T78">pers</ta>
            <ta e="T80" id="Seg_1126" s="T79">adv</ta>
            <ta e="T81" id="Seg_1127" s="T80">v</ta>
            <ta e="T82" id="Seg_1128" s="T81">v</ta>
            <ta e="T83" id="Seg_1129" s="T82">pers</ta>
            <ta e="T84" id="Seg_1130" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_1131" s="T84">v</ta>
            <ta e="T86" id="Seg_1132" s="T85">conj</ta>
            <ta e="T87" id="Seg_1133" s="T86">pers</ta>
            <ta e="T88" id="Seg_1134" s="T87">v</ta>
            <ta e="T89" id="Seg_1135" s="T88">n</ta>
            <ta e="T90" id="Seg_1136" s="T89">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1137" s="T1">pro.h:E</ta>
            <ta e="T4" id="Seg_1138" s="T3">v:Th</ta>
            <ta e="T6" id="Seg_1139" s="T5">0.1.h:A</ta>
            <ta e="T7" id="Seg_1140" s="T6">np.h:Th</ta>
            <ta e="T9" id="Seg_1141" s="T8">pro.h:G</ta>
            <ta e="T11" id="Seg_1142" s="T10">pro.h:Poss</ta>
            <ta e="T12" id="Seg_1143" s="T11">np.h:A</ta>
            <ta e="T13" id="Seg_1144" s="T12">0.3.h:Th</ta>
            <ta e="T14" id="Seg_1145" s="T13">pro.h:L</ta>
            <ta e="T16" id="Seg_1146" s="T15">0.3.h:A</ta>
            <ta e="T17" id="Seg_1147" s="T16">pro.h:E</ta>
            <ta e="T19" id="Seg_1148" s="T18">pro.h:A</ta>
            <ta e="T21" id="Seg_1149" s="T20">np:G</ta>
            <ta e="T23" id="Seg_1150" s="T22">pro.h:Th</ta>
            <ta e="T24" id="Seg_1151" s="T23">pp:L</ta>
            <ta e="T28" id="Seg_1152" s="T27">pro.h:E</ta>
            <ta e="T31" id="Seg_1153" s="T30">pro.h:A</ta>
            <ta e="T32" id="Seg_1154" s="T31">np.h:R</ta>
            <ta e="T34" id="Seg_1155" s="T33">0.2.h:E</ta>
            <ta e="T35" id="Seg_1156" s="T34">pro.h:E</ta>
            <ta e="T38" id="Seg_1157" s="T37">v:Th</ta>
            <ta e="T40" id="Seg_1158" s="T39">np.h:A</ta>
            <ta e="T43" id="Seg_1159" s="T42">0.1.h:E</ta>
            <ta e="T44" id="Seg_1160" s="T43">pro.h:A</ta>
            <ta e="T45" id="Seg_1161" s="T44">pro.h:R</ta>
            <ta e="T46" id="Seg_1162" s="T45">np:Th</ta>
            <ta e="T49" id="Seg_1163" s="T48">np.h:A</ta>
            <ta e="T51" id="Seg_1164" s="T50">0.2.h:A</ta>
            <ta e="T53" id="Seg_1165" s="T52">pro.h:A</ta>
            <ta e="T57" id="Seg_1166" s="T56">np.h:E</ta>
            <ta e="T60" id="Seg_1167" s="T59">np:Th 0.3.h:Poss</ta>
            <ta e="T61" id="Seg_1168" s="T60">0.3.h:A</ta>
            <ta e="T62" id="Seg_1169" s="T61">0.3.h:A</ta>
            <ta e="T63" id="Seg_1170" s="T62">pro:Th</ta>
            <ta e="T64" id="Seg_1171" s="T63">0.3.h:A</ta>
            <ta e="T66" id="Seg_1172" s="T65">pro.h:A</ta>
            <ta e="T68" id="Seg_1173" s="T67">pro.h:A</ta>
            <ta e="T71" id="Seg_1174" s="T70">np.h:A 0.1.h:Poss</ta>
            <ta e="T74" id="Seg_1175" s="T73">np:Poss</ta>
            <ta e="T75" id="Seg_1176" s="T74">np:L</ta>
            <ta e="T76" id="Seg_1177" s="T75">0.3.h:A</ta>
            <ta e="T79" id="Seg_1178" s="T78">pro.h:E</ta>
            <ta e="T82" id="Seg_1179" s="T81">0.1.h:A</ta>
            <ta e="T83" id="Seg_1180" s="T82">pro.h:E</ta>
            <ta e="T87" id="Seg_1181" s="T86">pro.h:A</ta>
            <ta e="T89" id="Seg_1182" s="T88">np:Poss</ta>
            <ta e="T90" id="Seg_1183" s="T89">np:L</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1184" s="T1">pro.h:S</ta>
            <ta e="T4" id="Seg_1185" s="T3">v:O</ta>
            <ta e="T5" id="Seg_1186" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_1187" s="T5">0.1.h:S v:pred</ta>
            <ta e="T8" id="Seg_1188" s="T7">s:purp</ta>
            <ta e="T10" id="Seg_1189" s="T9">v:pred</ta>
            <ta e="T12" id="Seg_1190" s="T11">np.h:S</ta>
            <ta e="T13" id="Seg_1191" s="T12">0.3.h:S v:pred</ta>
            <ta e="T16" id="Seg_1192" s="T15">0.3.h:S v:pred</ta>
            <ta e="T17" id="Seg_1193" s="T16">pro.h:S</ta>
            <ta e="T18" id="Seg_1194" s="T17">v:pred</ta>
            <ta e="T21" id="Seg_1195" s="T18">s:compl</ta>
            <ta e="T23" id="Seg_1196" s="T22">pro.h:S</ta>
            <ta e="T26" id="Seg_1197" s="T25">v:pred</ta>
            <ta e="T28" id="Seg_1198" s="T27">pro.h:S</ta>
            <ta e="T30" id="Seg_1199" s="T29">v:pred</ta>
            <ta e="T31" id="Seg_1200" s="T30">pro.h:S</ta>
            <ta e="T33" id="Seg_1201" s="T32">v:pred</ta>
            <ta e="T34" id="Seg_1202" s="T33">0.2.h:S v:pred</ta>
            <ta e="T35" id="Seg_1203" s="T34">pro.h:S</ta>
            <ta e="T37" id="Seg_1204" s="T36">v:pred</ta>
            <ta e="T38" id="Seg_1205" s="T37">v:O</ta>
            <ta e="T40" id="Seg_1206" s="T39">np.h:S</ta>
            <ta e="T41" id="Seg_1207" s="T40">v:pred</ta>
            <ta e="T43" id="Seg_1208" s="T42">0.1.h:S v:pred</ta>
            <ta e="T44" id="Seg_1209" s="T43">pro.h:S</ta>
            <ta e="T46" id="Seg_1210" s="T45">np:O</ta>
            <ta e="T47" id="Seg_1211" s="T46">v:pred</ta>
            <ta e="T49" id="Seg_1212" s="T48">np.h:S</ta>
            <ta e="T50" id="Seg_1213" s="T49">v:pred</ta>
            <ta e="T51" id="Seg_1214" s="T50">0.2.h:S v:pred</ta>
            <ta e="T53" id="Seg_1215" s="T52">pro.h:S</ta>
            <ta e="T55" id="Seg_1216" s="T54">v:pred</ta>
            <ta e="T57" id="Seg_1217" s="T56">np.h:S</ta>
            <ta e="T58" id="Seg_1218" s="T57">v:pred</ta>
            <ta e="T60" id="Seg_1219" s="T59">np:O</ta>
            <ta e="T61" id="Seg_1220" s="T60">0.3.h:S v:pred</ta>
            <ta e="T62" id="Seg_1221" s="T61">0.3.h:S v:pred</ta>
            <ta e="T64" id="Seg_1222" s="T62">s:compl</ta>
            <ta e="T66" id="Seg_1223" s="T65">pro.h:S</ta>
            <ta e="T67" id="Seg_1224" s="T66">v:pred</ta>
            <ta e="T68" id="Seg_1225" s="T67">pro.h:S</ta>
            <ta e="T70" id="Seg_1226" s="T69">v:pred</ta>
            <ta e="T71" id="Seg_1227" s="T70">np.h:S</ta>
            <ta e="T73" id="Seg_1228" s="T72">v:pred</ta>
            <ta e="T76" id="Seg_1229" s="T75">0.3.h:S v:pred</ta>
            <ta e="T79" id="Seg_1230" s="T78">pro.h:S</ta>
            <ta e="T81" id="Seg_1231" s="T80">v:pred</ta>
            <ta e="T82" id="Seg_1232" s="T81">0.1.h:S v:pred</ta>
            <ta e="T83" id="Seg_1233" s="T82">pro.h:S</ta>
            <ta e="T85" id="Seg_1234" s="T84">v:pred</ta>
            <ta e="T90" id="Seg_1235" s="T85">s:compl</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T15" id="Seg_1236" s="T14">RUS:gram</ta>
            <ta e="T22" id="Seg_1237" s="T21">RUS:gram</ta>
            <ta e="T24" id="Seg_1238" s="T23">RUS:cult</ta>
            <ta e="T27" id="Seg_1239" s="T26">RUS:gram</ta>
            <ta e="T39" id="Seg_1240" s="T38">RUS:gram</ta>
            <ta e="T48" id="Seg_1241" s="T47">RUS:gram</ta>
            <ta e="T52" id="Seg_1242" s="T51">RUS:gram</ta>
            <ta e="T56" id="Seg_1243" s="T55">RUS:gram</ta>
            <ta e="T59" id="Seg_1244" s="T58">RUS:gram</ta>
            <ta e="T65" id="Seg_1245" s="T64">RUS:gram</ta>
            <ta e="T72" id="Seg_1246" s="T71">RUS:disc</ta>
            <ta e="T74" id="Seg_1247" s="T73">RUS:cult</ta>
            <ta e="T80" id="Seg_1248" s="T79">RUS:core</ta>
            <ta e="T86" id="Seg_1249" s="T85">RUS:gram</ta>
            <ta e="T89" id="Seg_1250" s="T88">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_1251" s="T1">I was afraid to spend a night alone.</ta>
            <ta e="T8" id="Seg_1252" s="T5">I called Irina to spend a night with me.</ta>
            <ta e="T12" id="Seg_1253" s="T8">My younger brother came to us.</ta>
            <ta e="T16" id="Seg_1254" s="T12">He sat with us some time and [then] he left.</ta>
            <ta e="T21" id="Seg_1255" s="T16">We thought he had gone home.</ta>
            <ta e="T26" id="Seg_1256" s="T21">And he was standing under the window.</ta>
            <ta e="T30" id="Seg_1257" s="T26">And we didn't know that.</ta>
            <ta e="T34" id="Seg_1258" s="T30">I said to Irina: “Do you want to sleep?</ta>
            <ta e="T38" id="Seg_1259" s="T34">You want to sleep, don't you?”</ta>
            <ta e="T43" id="Seg_1260" s="T38">And Irina said: “No, I don't”.</ta>
            <ta e="T47" id="Seg_1261" s="T43">“I'll tell you something.”</ta>
            <ta e="T51" id="Seg_1262" s="T47">And Irina said: “Tell me.”</ta>
            <ta e="T55" id="Seg_1263" s="T51">And I began to tell.</ta>
            <ta e="T64" id="Seg_1264" s="T55">And Grisha heard it and put(?) his ears, started listening, what they would be telling.</ta>
            <ta e="T67" id="Seg_1265" s="T64">And I was telling.</ta>
            <ta e="T70" id="Seg_1266" s="T67">I finished telling.</ta>
            <ta e="T75" id="Seg_1267" s="T70">Suddenly my younger brother burst out with a cry from under the window.</ta>
            <ta e="T78" id="Seg_1268" s="T75">He said: “A good woman”.</ta>
            <ta e="T82" id="Seg_1269" s="T78">We got frightened so much, and we began crying.</ta>
            <ta e="T90" id="Seg_1270" s="T82">We didn't know he had been listening to us from under the window.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_1271" s="T1">Ich hatte Angst alleine zu schlafen.</ta>
            <ta e="T8" id="Seg_1272" s="T5">Ich rief Irina, damit sie die Nacht über bei mir bleibt.</ta>
            <ta e="T12" id="Seg_1273" s="T8">Mein kleiner Bruder kam zu uns.</ta>
            <ta e="T16" id="Seg_1274" s="T12">Er saß mit uns einige Zeit und [dann] ging er.</ta>
            <ta e="T21" id="Seg_1275" s="T16">Wir dachten, er wäre nach Hause gegangen.</ta>
            <ta e="T26" id="Seg_1276" s="T21">Und er stand unter dem Fenster.</ta>
            <ta e="T30" id="Seg_1277" s="T26">Und wir wussten das nicht.</ta>
            <ta e="T34" id="Seg_1278" s="T30">Ich sagte zu Irina: "Willst du schlafen?</ta>
            <ta e="T38" id="Seg_1279" s="T34">Du willst schlafen, oder?"</ta>
            <ta e="T43" id="Seg_1280" s="T38">Und Irina sagte: "Nein, ich will nicht schlafen."</ta>
            <ta e="T47" id="Seg_1281" s="T43">"Ich werde dir etwas erzählen."</ta>
            <ta e="T51" id="Seg_1282" s="T47">Und Irina sagte: "Erzähl."</ta>
            <ta e="T55" id="Seg_1283" s="T51">Und ich begann zu erzählen.</ta>
            <ta e="T64" id="Seg_1284" s="T55">Und Grisha hörte es und spitzte die Ohren, begann zu lauschen, was sie erzählen würden.</ta>
            <ta e="T67" id="Seg_1285" s="T64">Und ich erzählte.</ta>
            <ta e="T70" id="Seg_1286" s="T67">Ich hörte auf zu erzählen.</ta>
            <ta e="T75" id="Seg_1287" s="T70">Plötzlich schrie mein kleiner Bruder von unter dem Fenster.</ta>
            <ta e="T78" id="Seg_1288" s="T75">Er sagte: "Eine gute Frau."</ta>
            <ta e="T82" id="Seg_1289" s="T78">Wir bekamen solche Angst und begannen zu schreien.</ta>
            <ta e="T90" id="Seg_1290" s="T82">Wir wussten nicht, dass er von unter dem Fenster aus gelauscht hatte.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_1291" s="T1">Я одна ночевать боялась.</ta>
            <ta e="T8" id="Seg_1292" s="T5">Я позвала Ирину ночевать.</ta>
            <ta e="T12" id="Seg_1293" s="T8">К нам пришел мой младший брат.</ta>
            <ta e="T16" id="Seg_1294" s="T12">Посидел у нас и ушел.</ta>
            <ta e="T21" id="Seg_1295" s="T16">Мы думали, что он ушел домой.</ta>
            <ta e="T26" id="Seg_1296" s="T21">А он под окошком стоял.</ta>
            <ta e="T30" id="Seg_1297" s="T26">А мы не знали.</ta>
            <ta e="T34" id="Seg_1298" s="T30">Я Ирине сказала: “Спать хочешь?</ta>
            <ta e="T38" id="Seg_1299" s="T34">Ты, наверное, спать хочешь?”</ta>
            <ta e="T43" id="Seg_1300" s="T38">А Ирина сказала: “Нет, не хочу”.</ta>
            <ta e="T47" id="Seg_1301" s="T43">“Я тебе чего-нибудь расскажу”.</ta>
            <ta e="T51" id="Seg_1302" s="T47">А Ирина говорит: “Рассказывай”.</ta>
            <ta e="T55" id="Seg_1303" s="T51">И я стала рассказывать.</ta>
            <ta e="T64" id="Seg_1304" s="T55">А Гриша услышал и уши поставил(?), слушает, что будут рассказывать.</ta>
            <ta e="T67" id="Seg_1305" s="T64">А я рассказываю.</ta>
            <ta e="T70" id="Seg_1306" s="T67">Я рассказывать закончила.</ta>
            <ta e="T75" id="Seg_1307" s="T70">Младший брат как закричал под окном.</ta>
            <ta e="T78" id="Seg_1308" s="T75">Говорит: “Хорошая женщина”.</ta>
            <ta e="T82" id="Seg_1309" s="T78">Мы так испугались, закричали.</ta>
            <ta e="T90" id="Seg_1310" s="T82">Мы же не знали, что он слушает под окошком.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_1311" s="T1">я одна ночевать боялась</ta>
            <ta e="T8" id="Seg_1312" s="T5">позвала Ирину ночевать</ta>
            <ta e="T12" id="Seg_1313" s="T8">к нам пришел мой младший брат</ta>
            <ta e="T16" id="Seg_1314" s="T12">посидел у нас и ушел</ta>
            <ta e="T21" id="Seg_1315" s="T16">мы думали он ушел домой</ta>
            <ta e="T26" id="Seg_1316" s="T21">а он под окошком стоял</ta>
            <ta e="T30" id="Seg_1317" s="T26">а мы не знали</ta>
            <ta e="T34" id="Seg_1318" s="T30">я Ирине сказала спать хочешь</ta>
            <ta e="T38" id="Seg_1319" s="T34">ты наверное спать хочешь</ta>
            <ta e="T43" id="Seg_1320" s="T38">а Ирина сказала нет не хочу</ta>
            <ta e="T47" id="Seg_1321" s="T43">я тебе чего-нибудь расскажу (рассказывать буду)</ta>
            <ta e="T51" id="Seg_1322" s="T47">а Ирина говорит рассказывай</ta>
            <ta e="T55" id="Seg_1323" s="T51">я стала рассказывать</ta>
            <ta e="T64" id="Seg_1324" s="T55">а Гриша услыхал уши поставил и слушает (кого) что буду рассказывать</ta>
            <ta e="T67" id="Seg_1325" s="T64">я рассказываю</ta>
            <ta e="T70" id="Seg_1326" s="T67">я все рассказала</ta>
            <ta e="T75" id="Seg_1327" s="T70">младший брат как заревел под окном</ta>
            <ta e="T78" id="Seg_1328" s="T75">говорит хорошая женщина</ta>
            <ta e="T82" id="Seg_1329" s="T78">мы так испугались заревели</ta>
            <ta e="T90" id="Seg_1330" s="T82">мы же не знали что он под окошком слушает</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T64" id="Seg_1331" s="T55">[BrM:] Tentative analysis of 'kʼetčɨdɨt'.</ta>
            <ta e="T82" id="Seg_1332" s="T78">[BrM:] 'da tao' changed to 'datao'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
