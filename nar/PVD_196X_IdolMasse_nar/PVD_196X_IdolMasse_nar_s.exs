<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_196X_IdolMasse_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_196X_IdolMasse_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">90</ud-information>
            <ud-information attribute-name="# HIAT:w">67</ud-information>
            <ud-information attribute-name="# e">67</ud-information>
            <ud-information attribute-name="# HIAT:u">15</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T68" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Dʼedʼaon</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">ästä</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">madʼöɣən</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">qopbat</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">nomɨm</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_20" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">Tannɨt</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">täbɨn</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">maːttə</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_32" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">Qula</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">tʼärattə</ts>
                  <nts id="Seg_38" n="HIAT:ip">:</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_40" n="HIAT:ip">“</nts>
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">Tau</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">ass</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">nom</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_50" n="HIAT:ip">–</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">Massä</ts>
                  <nts id="Seg_54" n="HIAT:ip">.</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_57" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">Täbnä</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">nadə</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">oːmtugu</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_69" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">Täp</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">teːkga</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">wes</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">miketǯit</ts>
                  <nts id="Seg_81" n="HIAT:ip">,</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">suːrumla</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_87" n="HIAT:w" s="T23">miketǯit</ts>
                  <nts id="Seg_88" n="HIAT:ip">.</nts>
                  <nts id="Seg_89" n="HIAT:ip">”</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_92" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">Dʼeduškaw</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_97" n="HIAT:w" s="T25">suːrumlʼe</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_100" n="HIAT:w" s="T26">üːbəran</ts>
                  <nts id="Seg_101" n="HIAT:ip">.</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_104" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_106" n="HIAT:w" s="T27">I</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_109" n="HIAT:w" s="T28">sədə</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_112" n="HIAT:w" s="T29">nʼäja</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">ladaɣɨn</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_118" n="HIAT:w" s="T31">jen</ts>
                  <nts id="Seg_119" n="HIAT:ip">,</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_122" n="HIAT:w" s="T32">a</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_125" n="HIAT:w" s="T33">nagurumdettə</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_128" n="HIAT:w" s="T34">nʼäja</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_131" n="HIAT:w" s="T35">ladan</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_134" n="HIAT:w" s="T36">köɣɨn</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_137" n="HIAT:w" s="T37">ippa</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_141" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_143" n="HIAT:w" s="T38">Dʼeduškan</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_146" n="HIAT:w" s="T39">ästə</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_149" n="HIAT:w" s="T40">oːmtɨkus</ts>
                  <nts id="Seg_150" n="HIAT:ip">.</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_153" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_155" n="HIAT:w" s="T41">Man</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_158" n="HIAT:w" s="T42">ezeɣɨn</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_161" n="HIAT:w" s="T43">tärɨkus</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_164" n="HIAT:w" s="T44">oːmtɨku</ts>
                  <nts id="Seg_165" n="HIAT:ip">.</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_168" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_170" n="HIAT:w" s="T45">Man</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_173" n="HIAT:w" s="T46">ezew</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_176" n="HIAT:w" s="T47">oːmtɨkus</ts>
                  <nts id="Seg_177" n="HIAT:ip">.</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_180" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_182" n="HIAT:w" s="T48">Patom</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_185" n="HIAT:w" s="T49">qua</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_188" n="HIAT:w" s="T50">täp</ts>
                  <nts id="Seg_189" n="HIAT:ip">.</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_192" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_194" n="HIAT:w" s="T51">A</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_197" n="HIAT:w" s="T52">mamou</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_200" n="HIAT:w" s="T53">na</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_203" n="HIAT:w" s="T54">Massɨm</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_206" n="HIAT:w" s="T55">tüːtdə</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_209" n="HIAT:w" s="T56">tʼädɨt</ts>
                  <nts id="Seg_210" n="HIAT:ip">.</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_213" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_215" n="HIAT:w" s="T57">Datao</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_218" n="HIAT:w" s="T58">porɨpbɨs</ts>
                  <nts id="Seg_219" n="HIAT:ip">,</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_222" n="HIAT:w" s="T59">mamou</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_225" n="HIAT:w" s="T60">zaslonkam</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_228" n="HIAT:w" s="T61">zaslanitdɨt</ts>
                  <nts id="Seg_229" n="HIAT:ip">.</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_232" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_234" n="HIAT:w" s="T62">Me</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_237" n="HIAT:w" s="T63">larɨpbɨzawtə</ts>
                  <nts id="Seg_238" n="HIAT:ip">.</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_241" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_243" n="HIAT:w" s="T64">Massänan</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_246" n="HIAT:w" s="T65">seldi</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_249" n="HIAT:w" s="T66">iːt</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_252" n="HIAT:w" s="T67">aːmdattə</ts>
                  <nts id="Seg_253" n="HIAT:ip">.</nts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T68" id="Seg_255" n="sc" s="T1">
               <ts e="T2" id="Seg_257" n="e" s="T1">Dʼedʼaon </ts>
               <ts e="T3" id="Seg_259" n="e" s="T2">ästä </ts>
               <ts e="T4" id="Seg_261" n="e" s="T3">madʼöɣən </ts>
               <ts e="T5" id="Seg_263" n="e" s="T4">qopbat </ts>
               <ts e="T6" id="Seg_265" n="e" s="T5">nomɨm. </ts>
               <ts e="T7" id="Seg_267" n="e" s="T6">Tannɨt </ts>
               <ts e="T8" id="Seg_269" n="e" s="T7">täbɨn </ts>
               <ts e="T9" id="Seg_271" n="e" s="T8">maːttə. </ts>
               <ts e="T10" id="Seg_273" n="e" s="T9">Qula </ts>
               <ts e="T11" id="Seg_275" n="e" s="T10">tʼärattə: </ts>
               <ts e="T12" id="Seg_277" n="e" s="T11">“Tau </ts>
               <ts e="T13" id="Seg_279" n="e" s="T12">ass </ts>
               <ts e="T14" id="Seg_281" n="e" s="T13">nom – </ts>
               <ts e="T15" id="Seg_283" n="e" s="T14">Massä. </ts>
               <ts e="T16" id="Seg_285" n="e" s="T15">Täbnä </ts>
               <ts e="T17" id="Seg_287" n="e" s="T16">nadə </ts>
               <ts e="T18" id="Seg_289" n="e" s="T17">oːmtugu. </ts>
               <ts e="T19" id="Seg_291" n="e" s="T18">Täp </ts>
               <ts e="T20" id="Seg_293" n="e" s="T19">teːkga </ts>
               <ts e="T21" id="Seg_295" n="e" s="T20">wes </ts>
               <ts e="T22" id="Seg_297" n="e" s="T21">miketǯit, </ts>
               <ts e="T23" id="Seg_299" n="e" s="T22">suːrumla </ts>
               <ts e="T24" id="Seg_301" n="e" s="T23">miketǯit.” </ts>
               <ts e="T25" id="Seg_303" n="e" s="T24">Dʼeduškaw </ts>
               <ts e="T26" id="Seg_305" n="e" s="T25">suːrumlʼe </ts>
               <ts e="T27" id="Seg_307" n="e" s="T26">üːbəran. </ts>
               <ts e="T28" id="Seg_309" n="e" s="T27">I </ts>
               <ts e="T29" id="Seg_311" n="e" s="T28">sədə </ts>
               <ts e="T30" id="Seg_313" n="e" s="T29">nʼäja </ts>
               <ts e="T31" id="Seg_315" n="e" s="T30">ladaɣɨn </ts>
               <ts e="T32" id="Seg_317" n="e" s="T31">jen, </ts>
               <ts e="T33" id="Seg_319" n="e" s="T32">a </ts>
               <ts e="T34" id="Seg_321" n="e" s="T33">nagurumdettə </ts>
               <ts e="T35" id="Seg_323" n="e" s="T34">nʼäja </ts>
               <ts e="T36" id="Seg_325" n="e" s="T35">ladan </ts>
               <ts e="T37" id="Seg_327" n="e" s="T36">köɣɨn </ts>
               <ts e="T38" id="Seg_329" n="e" s="T37">ippa. </ts>
               <ts e="T39" id="Seg_331" n="e" s="T38">Dʼeduškan </ts>
               <ts e="T40" id="Seg_333" n="e" s="T39">ästə </ts>
               <ts e="T41" id="Seg_335" n="e" s="T40">oːmtɨkus. </ts>
               <ts e="T42" id="Seg_337" n="e" s="T41">Man </ts>
               <ts e="T43" id="Seg_339" n="e" s="T42">ezeɣɨn </ts>
               <ts e="T44" id="Seg_341" n="e" s="T43">tärɨkus </ts>
               <ts e="T45" id="Seg_343" n="e" s="T44">oːmtɨku. </ts>
               <ts e="T46" id="Seg_345" n="e" s="T45">Man </ts>
               <ts e="T47" id="Seg_347" n="e" s="T46">ezew </ts>
               <ts e="T48" id="Seg_349" n="e" s="T47">oːmtɨkus. </ts>
               <ts e="T49" id="Seg_351" n="e" s="T48">Patom </ts>
               <ts e="T50" id="Seg_353" n="e" s="T49">qua </ts>
               <ts e="T51" id="Seg_355" n="e" s="T50">täp. </ts>
               <ts e="T52" id="Seg_357" n="e" s="T51">A </ts>
               <ts e="T53" id="Seg_359" n="e" s="T52">mamou </ts>
               <ts e="T54" id="Seg_361" n="e" s="T53">na </ts>
               <ts e="T55" id="Seg_363" n="e" s="T54">Massɨm </ts>
               <ts e="T56" id="Seg_365" n="e" s="T55">tüːtdə </ts>
               <ts e="T57" id="Seg_367" n="e" s="T56">tʼädɨt. </ts>
               <ts e="T58" id="Seg_369" n="e" s="T57">Datao </ts>
               <ts e="T59" id="Seg_371" n="e" s="T58">porɨpbɨs, </ts>
               <ts e="T60" id="Seg_373" n="e" s="T59">mamou </ts>
               <ts e="T61" id="Seg_375" n="e" s="T60">zaslonkam </ts>
               <ts e="T62" id="Seg_377" n="e" s="T61">zaslanitdɨt. </ts>
               <ts e="T63" id="Seg_379" n="e" s="T62">Me </ts>
               <ts e="T64" id="Seg_381" n="e" s="T63">larɨpbɨzawtə. </ts>
               <ts e="T65" id="Seg_383" n="e" s="T64">Massänan </ts>
               <ts e="T66" id="Seg_385" n="e" s="T65">seldi </ts>
               <ts e="T67" id="Seg_387" n="e" s="T66">iːt </ts>
               <ts e="T68" id="Seg_389" n="e" s="T67">aːmdattə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_390" s="T1">PVD_196X_IdolMasse_nar.001 (001.001)</ta>
            <ta e="T9" id="Seg_391" s="T6">PVD_196X_IdolMasse_nar.002 (001.002)</ta>
            <ta e="T15" id="Seg_392" s="T9">PVD_196X_IdolMasse_nar.003 (001.003)</ta>
            <ta e="T18" id="Seg_393" s="T15">PVD_196X_IdolMasse_nar.004 (001.004)</ta>
            <ta e="T24" id="Seg_394" s="T18">PVD_196X_IdolMasse_nar.005 (001.005)</ta>
            <ta e="T27" id="Seg_395" s="T24">PVD_196X_IdolMasse_nar.006 (001.006)</ta>
            <ta e="T38" id="Seg_396" s="T27">PVD_196X_IdolMasse_nar.007 (001.007)</ta>
            <ta e="T41" id="Seg_397" s="T38">PVD_196X_IdolMasse_nar.008 (001.008)</ta>
            <ta e="T45" id="Seg_398" s="T41">PVD_196X_IdolMasse_nar.009 (001.009)</ta>
            <ta e="T48" id="Seg_399" s="T45">PVD_196X_IdolMasse_nar.010 (001.010)</ta>
            <ta e="T51" id="Seg_400" s="T48">PVD_196X_IdolMasse_nar.011 (001.011)</ta>
            <ta e="T57" id="Seg_401" s="T51">PVD_196X_IdolMasse_nar.012 (001.012)</ta>
            <ta e="T62" id="Seg_402" s="T57">PVD_196X_IdolMasse_nar.013 (001.013)</ta>
            <ta e="T64" id="Seg_403" s="T62">PVD_196X_IdolMasse_nar.014 (001.014)</ta>
            <ta e="T68" id="Seg_404" s="T64">PVD_196X_IdolMasse_nar.015 (001.015)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_405" s="T1">′дʼедʼаон ӓс′тӓ ма′дʼӧɣън ′kопбат ′номым.</ta>
            <ta e="T9" id="Seg_406" s="T6">та′нныт ′тӓбын ′ма̄ттъ.</ta>
            <ta e="T15" id="Seg_407" s="T9">ма′ссӓ. kу′lа тʼӓ′раттъ: тау̹ асс ном – ма′ссӓ.</ta>
            <ta e="T18" id="Seg_408" s="T15">тӓб′нӓ надъ о̄мтугу.</ta>
            <ta e="T24" id="Seg_409" s="T18">тӓп те̄кга вес ми′кетджит, ′сӯрумла ми′кетджит.</ta>
            <ta e="T27" id="Seg_410" s="T24">′дʼедушкаw ′сӯрумлʼе ′ӱ̄бъран.</ta>
            <ta e="T38" id="Seg_411" s="T27">и ′съдъ ′нʼӓjа ла′даɣын jен, а ′нагурум′деттъ ′нʼӓjа ла′дан ′кӧɣын и′ппа.</ta>
            <ta e="T41" id="Seg_412" s="T38">′дʼедушкан ′ӓстъ ′о̄мтыкус.</ta>
            <ta e="T45" id="Seg_413" s="T41">ман е′зеɣын тӓры′кус ′о̄мты′ку.</ta>
            <ta e="T48" id="Seg_414" s="T45">ман е′зеw о̄мтыкус.</ta>
            <ta e="T51" id="Seg_415" s="T48">па′том ′kуа тӓп.</ta>
            <ta e="T57" id="Seg_416" s="T51">а ′мамоу на ′массым ′тӱ̄тдъ ′тʼӓдыт.</ta>
            <ta e="T62" id="Seg_417" s="T57">да та′о ′порыпбыс, ′мамоу за′слонкам засла′нитдыт.</ta>
            <ta e="T64" id="Seg_418" s="T62">ме ларыпбы′заwтъ.</ta>
            <ta e="T68" id="Seg_419" s="T64">ма′ссӓнан ′сеlди ӣт ′а̄мдаттъ.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_420" s="T1">dʼedʼaon ästä madʼöɣən qopbat nomɨm.</ta>
            <ta e="T9" id="Seg_421" s="T6">tannɨt täbɨn maːttə.</ta>
            <ta e="T15" id="Seg_422" s="T9">qula tʼärattə: tau̹ ass nom – massä.</ta>
            <ta e="T18" id="Seg_423" s="T15">täbnä nadə oːmtugu.</ta>
            <ta e="T24" id="Seg_424" s="T18">täp teːkga wes miketǯit, suːrumla miketǯit.</ta>
            <ta e="T27" id="Seg_425" s="T24">dʼeduškaw suːrumlʼe üːbəran.</ta>
            <ta e="T38" id="Seg_426" s="T27">i sədə nʼäja ladaɣɨn jen, a nagurumdettə nʼäja ladan köɣɨn ippa.</ta>
            <ta e="T41" id="Seg_427" s="T38">dʼeduškan ästə oːmtɨkus.</ta>
            <ta e="T45" id="Seg_428" s="T41">man ezeɣɨn tärɨkus oːmtɨku.</ta>
            <ta e="T48" id="Seg_429" s="T45">man ezew oːmtɨkus.</ta>
            <ta e="T51" id="Seg_430" s="T48">patom qua täp.</ta>
            <ta e="T57" id="Seg_431" s="T51">a mamou na massɨm tüːtdə tʼädɨt.</ta>
            <ta e="T62" id="Seg_432" s="T57">da tao porɨpbɨs, mamou zaslonkam zaslanitdɨt.</ta>
            <ta e="T64" id="Seg_433" s="T62">me larɨpbɨzawtə.</ta>
            <ta e="T68" id="Seg_434" s="T64">massänan seldi iːt aːmdattə.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_435" s="T1">Dʼedʼaon ästä madʼöɣən qopbat nomɨm. </ta>
            <ta e="T9" id="Seg_436" s="T6">Tannɨt täbɨn maːttə. </ta>
            <ta e="T15" id="Seg_437" s="T9">Qula tʼärattə: “Tau ass nom – Massä. </ta>
            <ta e="T18" id="Seg_438" s="T15">Täbnä nadə oːmtugu. </ta>
            <ta e="T24" id="Seg_439" s="T18">Täp teːkga wes miketǯit, suːrumla miketǯit.” </ta>
            <ta e="T27" id="Seg_440" s="T24">Dʼeduškaw suːrumlʼe üːbəran. </ta>
            <ta e="T38" id="Seg_441" s="T27">I sədə nʼäja ladaɣɨn jen, a nagurumdettə nʼäja ladan köɣɨn ippa. </ta>
            <ta e="T41" id="Seg_442" s="T38">Dʼeduškan ästə oːmtɨkus. </ta>
            <ta e="T45" id="Seg_443" s="T41">Man ezeɣɨn tärɨkus oːmtɨku. </ta>
            <ta e="T48" id="Seg_444" s="T45">Man ezew oːmtɨkus. </ta>
            <ta e="T51" id="Seg_445" s="T48">Patom qua täp. </ta>
            <ta e="T57" id="Seg_446" s="T51">A mamou na Massɨm tüːtdə tʼädɨt. </ta>
            <ta e="T62" id="Seg_447" s="T57">Datao porɨpbɨs, mamou zaslonkam zaslanitdɨt. </ta>
            <ta e="T64" id="Seg_448" s="T62">Me larɨpbɨzawtə. </ta>
            <ta e="T68" id="Seg_449" s="T64">Massänan seldi iːt aːmdattə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_450" s="T1">dʼedʼa-o-n</ta>
            <ta e="T3" id="Seg_451" s="T2">äs-tä</ta>
            <ta e="T4" id="Seg_452" s="T3">madʼ-ö-ɣən</ta>
            <ta e="T5" id="Seg_453" s="T4">qo-pba-t</ta>
            <ta e="T6" id="Seg_454" s="T5">nom-ɨ-m</ta>
            <ta e="T7" id="Seg_455" s="T6">tan-nɨ-t</ta>
            <ta e="T8" id="Seg_456" s="T7">täb-ɨ-n</ta>
            <ta e="T9" id="Seg_457" s="T8">maːt-tə</ta>
            <ta e="T10" id="Seg_458" s="T9">qu-la</ta>
            <ta e="T11" id="Seg_459" s="T10">tʼära-ttə</ta>
            <ta e="T12" id="Seg_460" s="T11">tau</ta>
            <ta e="T13" id="Seg_461" s="T12">ass</ta>
            <ta e="T14" id="Seg_462" s="T13">nom</ta>
            <ta e="T15" id="Seg_463" s="T14">Massä</ta>
            <ta e="T16" id="Seg_464" s="T15">täb-nä</ta>
            <ta e="T17" id="Seg_465" s="T16">nadə</ta>
            <ta e="T18" id="Seg_466" s="T17">oːmtu-gu</ta>
            <ta e="T19" id="Seg_467" s="T18">täp</ta>
            <ta e="T20" id="Seg_468" s="T19">teːkga</ta>
            <ta e="T21" id="Seg_469" s="T20">wes</ta>
            <ta e="T22" id="Seg_470" s="T21">mi-k-etǯi-t</ta>
            <ta e="T23" id="Seg_471" s="T22">suːrum-la</ta>
            <ta e="T24" id="Seg_472" s="T23">mi-k-etǯi-t</ta>
            <ta e="T25" id="Seg_473" s="T24">dʼeduška-w</ta>
            <ta e="T26" id="Seg_474" s="T25">suːrum-lʼe</ta>
            <ta e="T27" id="Seg_475" s="T26">üːbə-r-a-n</ta>
            <ta e="T28" id="Seg_476" s="T27">i</ta>
            <ta e="T29" id="Seg_477" s="T28">sədə</ta>
            <ta e="T30" id="Seg_478" s="T29">nʼäja</ta>
            <ta e="T31" id="Seg_479" s="T30">lada-ɣɨn</ta>
            <ta e="T32" id="Seg_480" s="T31">je-n</ta>
            <ta e="T33" id="Seg_481" s="T32">a</ta>
            <ta e="T34" id="Seg_482" s="T33">naguru-mdettə</ta>
            <ta e="T35" id="Seg_483" s="T34">nʼäja</ta>
            <ta e="T36" id="Seg_484" s="T35">lada-n</ta>
            <ta e="T37" id="Seg_485" s="T36">kö-ɣɨn</ta>
            <ta e="T38" id="Seg_486" s="T37">ippa</ta>
            <ta e="T39" id="Seg_487" s="T38">dʼeduška-n</ta>
            <ta e="T40" id="Seg_488" s="T39">äs-tə</ta>
            <ta e="T41" id="Seg_489" s="T40">oːmtɨ-ku-s</ta>
            <ta e="T42" id="Seg_490" s="T41">man</ta>
            <ta e="T43" id="Seg_491" s="T42">eze-ɣɨn</ta>
            <ta e="T44" id="Seg_492" s="T43">tärɨ-ku-s</ta>
            <ta e="T45" id="Seg_493" s="T44">oːmtɨ-ku</ta>
            <ta e="T46" id="Seg_494" s="T45">man</ta>
            <ta e="T47" id="Seg_495" s="T46">eze-w</ta>
            <ta e="T48" id="Seg_496" s="T47">oːmtɨ-ku-s</ta>
            <ta e="T49" id="Seg_497" s="T48">patom</ta>
            <ta e="T50" id="Seg_498" s="T49">qu-a</ta>
            <ta e="T51" id="Seg_499" s="T50">täp</ta>
            <ta e="T52" id="Seg_500" s="T51">a</ta>
            <ta e="T53" id="Seg_501" s="T52">mamo-u</ta>
            <ta e="T54" id="Seg_502" s="T53">na</ta>
            <ta e="T55" id="Seg_503" s="T54">Massɨ-m</ta>
            <ta e="T56" id="Seg_504" s="T55">tüː-tdə</ta>
            <ta e="T57" id="Seg_505" s="T56">tʼädɨ-t</ta>
            <ta e="T58" id="Seg_506" s="T57">datao</ta>
            <ta e="T59" id="Seg_507" s="T58">porɨpbɨ-s</ta>
            <ta e="T60" id="Seg_508" s="T59">mamo-u</ta>
            <ta e="T61" id="Seg_509" s="T60">zaslonka-m</ta>
            <ta e="T62" id="Seg_510" s="T61">zaslani-tdɨ-t</ta>
            <ta e="T63" id="Seg_511" s="T62">me</ta>
            <ta e="T64" id="Seg_512" s="T63">larɨ-pbɨ-za-wtə</ta>
            <ta e="T65" id="Seg_513" s="T64">Massä-nan</ta>
            <ta e="T66" id="Seg_514" s="T65">seldi</ta>
            <ta e="T67" id="Seg_515" s="T66">iː-t</ta>
            <ta e="T68" id="Seg_516" s="T67">aːmda-ttə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_517" s="T1">dʼedʼa-ɨ-n</ta>
            <ta e="T3" id="Seg_518" s="T2">aze-tə</ta>
            <ta e="T4" id="Seg_519" s="T3">madʼ-ɨ-qɨn</ta>
            <ta e="T5" id="Seg_520" s="T4">qo-mbɨ-t</ta>
            <ta e="T6" id="Seg_521" s="T5">nom-ɨ-m</ta>
            <ta e="T7" id="Seg_522" s="T6">tat-nɨ-t</ta>
            <ta e="T8" id="Seg_523" s="T7">täp-ɨ-n</ta>
            <ta e="T9" id="Seg_524" s="T8">maːt-ntə</ta>
            <ta e="T10" id="Seg_525" s="T9">qum-la</ta>
            <ta e="T11" id="Seg_526" s="T10">tʼärɨ-tɨn</ta>
            <ta e="T12" id="Seg_527" s="T11">taw</ta>
            <ta e="T13" id="Seg_528" s="T12">asa</ta>
            <ta e="T14" id="Seg_529" s="T13">nom</ta>
            <ta e="T15" id="Seg_530" s="T14">Massä</ta>
            <ta e="T16" id="Seg_531" s="T15">täp-nä</ta>
            <ta e="T17" id="Seg_532" s="T16">nadə</ta>
            <ta e="T18" id="Seg_533" s="T17">omtu-gu</ta>
            <ta e="T19" id="Seg_534" s="T18">täp</ta>
            <ta e="T20" id="Seg_535" s="T19">tekka</ta>
            <ta e="T21" id="Seg_536" s="T20">wesʼ</ta>
            <ta e="T22" id="Seg_537" s="T21">me-ku-enǯɨ-t</ta>
            <ta e="T23" id="Seg_538" s="T22">suːrum-la</ta>
            <ta e="T24" id="Seg_539" s="T23">me-ku-enǯɨ-t</ta>
            <ta e="T25" id="Seg_540" s="T24">dʼeduška-w</ta>
            <ta e="T26" id="Seg_541" s="T25">suːrum-le</ta>
            <ta e="T27" id="Seg_542" s="T26">übɨ-r-nɨ-n</ta>
            <ta e="T28" id="Seg_543" s="T27">i</ta>
            <ta e="T29" id="Seg_544" s="T28">sədə</ta>
            <ta e="T30" id="Seg_545" s="T29">nʼaja</ta>
            <ta e="T31" id="Seg_546" s="T30">lada-qɨn</ta>
            <ta e="T32" id="Seg_547" s="T31">eː-n</ta>
            <ta e="T33" id="Seg_548" s="T32">a</ta>
            <ta e="T34" id="Seg_549" s="T33">nagur-mǯʼeːli</ta>
            <ta e="T35" id="Seg_550" s="T34">nʼaja</ta>
            <ta e="T36" id="Seg_551" s="T35">lada-n</ta>
            <ta e="T37" id="Seg_552" s="T36">kö-qɨn</ta>
            <ta e="T38" id="Seg_553" s="T37">ippɨ</ta>
            <ta e="T39" id="Seg_554" s="T38">dʼeduška-n</ta>
            <ta e="T40" id="Seg_555" s="T39">aze-tə</ta>
            <ta e="T41" id="Seg_556" s="T40">omtu-ku-sɨ</ta>
            <ta e="T42" id="Seg_557" s="T41">man</ta>
            <ta e="T43" id="Seg_558" s="T42">aze-qɨn</ta>
            <ta e="T44" id="Seg_559" s="T43">tʼärɨ-ku-sɨ</ta>
            <ta e="T45" id="Seg_560" s="T44">omtu-gu</ta>
            <ta e="T46" id="Seg_561" s="T45">man</ta>
            <ta e="T47" id="Seg_562" s="T46">aze-w</ta>
            <ta e="T48" id="Seg_563" s="T47">omtu-ku-sɨ</ta>
            <ta e="T49" id="Seg_564" s="T48">patom</ta>
            <ta e="T50" id="Seg_565" s="T49">quː-nɨ</ta>
            <ta e="T51" id="Seg_566" s="T50">täp</ta>
            <ta e="T52" id="Seg_567" s="T51">a</ta>
            <ta e="T53" id="Seg_568" s="T52">mamo-w</ta>
            <ta e="T54" id="Seg_569" s="T53">na</ta>
            <ta e="T55" id="Seg_570" s="T54">Massä-m</ta>
            <ta e="T56" id="Seg_571" s="T55">tü-ntə</ta>
            <ta e="T57" id="Seg_572" s="T56">čʼadɨ-t</ta>
            <ta e="T58" id="Seg_573" s="T57">tatawa</ta>
            <ta e="T59" id="Seg_574" s="T58">porəmbɨ-sɨ</ta>
            <ta e="T60" id="Seg_575" s="T59">mamo-w</ta>
            <ta e="T61" id="Seg_576" s="T60">zaslonka-m</ta>
            <ta e="T62" id="Seg_577" s="T61">zaslani-ntɨ-t</ta>
            <ta e="T63" id="Seg_578" s="T62">me</ta>
            <ta e="T64" id="Seg_579" s="T63">*larɨ-mbɨ-sɨ-un</ta>
            <ta e="T65" id="Seg_580" s="T64">Massä-nan</ta>
            <ta e="T66" id="Seg_581" s="T65">selʼdʼi</ta>
            <ta e="T67" id="Seg_582" s="T66">iː-tə</ta>
            <ta e="T68" id="Seg_583" s="T67">amdɨ-tɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_584" s="T1">grandfather-EP-GEN</ta>
            <ta e="T3" id="Seg_585" s="T2">father.[NOM]-3SG</ta>
            <ta e="T4" id="Seg_586" s="T3">taiga-EP-LOC</ta>
            <ta e="T5" id="Seg_587" s="T4">find-PST.NAR-3SG.O</ta>
            <ta e="T6" id="Seg_588" s="T5">icon-EP-ACC</ta>
            <ta e="T7" id="Seg_589" s="T6">bring-CO-3SG.O</ta>
            <ta e="T8" id="Seg_590" s="T7">(s)he-EP-GEN</ta>
            <ta e="T9" id="Seg_591" s="T8">house-ILL</ta>
            <ta e="T10" id="Seg_592" s="T9">human.being-PL.[NOM]</ta>
            <ta e="T11" id="Seg_593" s="T10">say-3PL</ta>
            <ta e="T12" id="Seg_594" s="T11">this</ta>
            <ta e="T13" id="Seg_595" s="T12">NEG</ta>
            <ta e="T14" id="Seg_596" s="T13">god.[3SG.S]</ta>
            <ta e="T15" id="Seg_597" s="T14">Masse.[3SG.S]</ta>
            <ta e="T16" id="Seg_598" s="T15">(s)he-ALL</ta>
            <ta e="T17" id="Seg_599" s="T16">one.should</ta>
            <ta e="T18" id="Seg_600" s="T17">pray-INF</ta>
            <ta e="T19" id="Seg_601" s="T18">(s)he.[NOM]</ta>
            <ta e="T20" id="Seg_602" s="T19">you.ALL</ta>
            <ta e="T21" id="Seg_603" s="T20">all.[NOM]</ta>
            <ta e="T22" id="Seg_604" s="T21">give-HAB-FUT-3SG.O</ta>
            <ta e="T23" id="Seg_605" s="T22">wild.animal-PL.[NOM]</ta>
            <ta e="T24" id="Seg_606" s="T23">give-HAB-FUT-3SG.O</ta>
            <ta e="T25" id="Seg_607" s="T24">grandfather.[NOM]-1SG</ta>
            <ta e="T26" id="Seg_608" s="T25">hunt-CVB</ta>
            <ta e="T27" id="Seg_609" s="T26">begin-FRQ-CO-3SG.S</ta>
            <ta e="T28" id="Seg_610" s="T27">and</ta>
            <ta e="T29" id="Seg_611" s="T28">two</ta>
            <ta e="T30" id="Seg_612" s="T29">squirrel.[NOM]</ta>
            <ta e="T31" id="Seg_613" s="T30">trap-LOC</ta>
            <ta e="T32" id="Seg_614" s="T31">be-3SG.S</ta>
            <ta e="T33" id="Seg_615" s="T32">and</ta>
            <ta e="T34" id="Seg_616" s="T33">three-ORD</ta>
            <ta e="T35" id="Seg_617" s="T34">squirrel.[NOM]</ta>
            <ta e="T36" id="Seg_618" s="T35">trap-GEN</ta>
            <ta e="T37" id="Seg_619" s="T36">side-LOC</ta>
            <ta e="T38" id="Seg_620" s="T37">lie.[3SG.S]</ta>
            <ta e="T39" id="Seg_621" s="T38">grandfather-GEN</ta>
            <ta e="T40" id="Seg_622" s="T39">father.[NOM]-3SG</ta>
            <ta e="T41" id="Seg_623" s="T40">pray-HAB-PST.[NOM]</ta>
            <ta e="T42" id="Seg_624" s="T41">I.GEN</ta>
            <ta e="T43" id="Seg_625" s="T42">father-LOC</ta>
            <ta e="T44" id="Seg_626" s="T43">say-HAB-PST.[3SG.S]</ta>
            <ta e="T45" id="Seg_627" s="T44">pray-INF</ta>
            <ta e="T46" id="Seg_628" s="T45">I.GEN</ta>
            <ta e="T47" id="Seg_629" s="T46">father.[NOM]-1SG</ta>
            <ta e="T48" id="Seg_630" s="T47">pray-HAB-PST.[NOM]</ta>
            <ta e="T49" id="Seg_631" s="T48">then</ta>
            <ta e="T50" id="Seg_632" s="T49">die-CO.[3SG.S]</ta>
            <ta e="T51" id="Seg_633" s="T50">(s)he.[NOM]</ta>
            <ta e="T52" id="Seg_634" s="T51">and</ta>
            <ta e="T53" id="Seg_635" s="T52">mum.[NOM]-1SG</ta>
            <ta e="T54" id="Seg_636" s="T53">this</ta>
            <ta e="T55" id="Seg_637" s="T54">Masse-ACC</ta>
            <ta e="T56" id="Seg_638" s="T55">fire-ILL</ta>
            <ta e="T57" id="Seg_639" s="T56">burn.down-3SG.O</ta>
            <ta e="T58" id="Seg_640" s="T57">to.such.extent</ta>
            <ta e="T59" id="Seg_641" s="T58">burn-PST.[3SG.S]</ta>
            <ta e="T60" id="Seg_642" s="T59">mum.[NOM]-1SG</ta>
            <ta e="T61" id="Seg_643" s="T60">oven.door-ACC</ta>
            <ta e="T62" id="Seg_644" s="T61">close.the.oven.door-INFER-3SG.O</ta>
            <ta e="T63" id="Seg_645" s="T62">we.[NOM]</ta>
            <ta e="T64" id="Seg_646" s="T63">get.afraid-DUR-PST-1PL</ta>
            <ta e="T65" id="Seg_647" s="T64">Masse-ADES</ta>
            <ta e="T66" id="Seg_648" s="T65">seven</ta>
            <ta e="T67" id="Seg_649" s="T66">son.[NOM]-3SG</ta>
            <ta e="T68" id="Seg_650" s="T67">sit-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_651" s="T1">дедушка-EP-GEN</ta>
            <ta e="T3" id="Seg_652" s="T2">отец.[NOM]-3SG</ta>
            <ta e="T4" id="Seg_653" s="T3">тайга-EP-LOC</ta>
            <ta e="T5" id="Seg_654" s="T4">найти-PST.NAR-3SG.O</ta>
            <ta e="T6" id="Seg_655" s="T5">икона-EP-ACC</ta>
            <ta e="T7" id="Seg_656" s="T6">принести-CO-3SG.O</ta>
            <ta e="T8" id="Seg_657" s="T7">он(а)-EP-GEN</ta>
            <ta e="T9" id="Seg_658" s="T8">дом-ILL</ta>
            <ta e="T10" id="Seg_659" s="T9">человек-PL.[NOM]</ta>
            <ta e="T11" id="Seg_660" s="T10">сказать-3PL</ta>
            <ta e="T12" id="Seg_661" s="T11">этот</ta>
            <ta e="T13" id="Seg_662" s="T12">NEG</ta>
            <ta e="T14" id="Seg_663" s="T13">бог.[3SG.S]</ta>
            <ta e="T15" id="Seg_664" s="T14">Массе.[3SG.S]</ta>
            <ta e="T16" id="Seg_665" s="T15">он(а)-ALL</ta>
            <ta e="T17" id="Seg_666" s="T16">надо</ta>
            <ta e="T18" id="Seg_667" s="T17">молиться-INF</ta>
            <ta e="T19" id="Seg_668" s="T18">он(а).[NOM]</ta>
            <ta e="T20" id="Seg_669" s="T19">ты.ALL</ta>
            <ta e="T21" id="Seg_670" s="T20">весь.[NOM]</ta>
            <ta e="T22" id="Seg_671" s="T21">дать-HAB-FUT-3SG.O</ta>
            <ta e="T23" id="Seg_672" s="T22">зверь-PL.[NOM]</ta>
            <ta e="T24" id="Seg_673" s="T23">дать-HAB-FUT-3SG.O</ta>
            <ta e="T25" id="Seg_674" s="T24">дедушка.[NOM]-1SG</ta>
            <ta e="T26" id="Seg_675" s="T25">охотиться-CVB</ta>
            <ta e="T27" id="Seg_676" s="T26">начать-FRQ-CO-3SG.S</ta>
            <ta e="T28" id="Seg_677" s="T27">и</ta>
            <ta e="T29" id="Seg_678" s="T28">два</ta>
            <ta e="T30" id="Seg_679" s="T29">белка.[NOM]</ta>
            <ta e="T31" id="Seg_680" s="T30">черкан-LOC</ta>
            <ta e="T32" id="Seg_681" s="T31">быть-3SG.S</ta>
            <ta e="T33" id="Seg_682" s="T32">а</ta>
            <ta e="T34" id="Seg_683" s="T33">три-ORD</ta>
            <ta e="T35" id="Seg_684" s="T34">белка.[NOM]</ta>
            <ta e="T36" id="Seg_685" s="T35">черкан-GEN</ta>
            <ta e="T37" id="Seg_686" s="T36">бок-LOC</ta>
            <ta e="T38" id="Seg_687" s="T37">лежать.[3SG.S]</ta>
            <ta e="T39" id="Seg_688" s="T38">дедушка-GEN</ta>
            <ta e="T40" id="Seg_689" s="T39">отец.[NOM]-3SG</ta>
            <ta e="T41" id="Seg_690" s="T40">молиться-HAB-PST.[NOM]</ta>
            <ta e="T42" id="Seg_691" s="T41">я.GEN</ta>
            <ta e="T43" id="Seg_692" s="T42">отец-LOC</ta>
            <ta e="T44" id="Seg_693" s="T43">сказать-HAB-PST.[3SG.S]</ta>
            <ta e="T45" id="Seg_694" s="T44">молиться-INF</ta>
            <ta e="T46" id="Seg_695" s="T45">я.GEN</ta>
            <ta e="T47" id="Seg_696" s="T46">отец.[NOM]-1SG</ta>
            <ta e="T48" id="Seg_697" s="T47">молиться-HAB-PST.[NOM]</ta>
            <ta e="T49" id="Seg_698" s="T48">потом</ta>
            <ta e="T50" id="Seg_699" s="T49">умереть-CO.[3SG.S]</ta>
            <ta e="T51" id="Seg_700" s="T50">он(а).[NOM]</ta>
            <ta e="T52" id="Seg_701" s="T51">а</ta>
            <ta e="T53" id="Seg_702" s="T52">мама.[NOM]-1SG</ta>
            <ta e="T54" id="Seg_703" s="T53">этот</ta>
            <ta e="T55" id="Seg_704" s="T54">Массе-ACC</ta>
            <ta e="T56" id="Seg_705" s="T55">огонь-ILL</ta>
            <ta e="T57" id="Seg_706" s="T56">сжечь-3SG.O</ta>
            <ta e="T58" id="Seg_707" s="T57">до.того</ta>
            <ta e="T59" id="Seg_708" s="T58">гореть-PST.[3SG.S]</ta>
            <ta e="T60" id="Seg_709" s="T59">мама.[NOM]-1SG</ta>
            <ta e="T61" id="Seg_710" s="T60">заслонка-ACC</ta>
            <ta e="T62" id="Seg_711" s="T61">заслонить-INFER-3SG.O</ta>
            <ta e="T63" id="Seg_712" s="T62">мы.[NOM]</ta>
            <ta e="T64" id="Seg_713" s="T63">испугаться-DUR-PST-1PL</ta>
            <ta e="T65" id="Seg_714" s="T64">Массе-ADES</ta>
            <ta e="T66" id="Seg_715" s="T65">семь</ta>
            <ta e="T67" id="Seg_716" s="T66">сын.[NOM]-3SG</ta>
            <ta e="T68" id="Seg_717" s="T67">сидеть-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_718" s="T1">n-n:ins-n:case</ta>
            <ta e="T3" id="Seg_719" s="T2">n.[n:case]-n:poss</ta>
            <ta e="T4" id="Seg_720" s="T3">n-n:ins-n:case</ta>
            <ta e="T5" id="Seg_721" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_722" s="T5">n-n:ins-n:case</ta>
            <ta e="T7" id="Seg_723" s="T6">v-v:ins-v:pn</ta>
            <ta e="T8" id="Seg_724" s="T7">pers-n:ins-n:case</ta>
            <ta e="T9" id="Seg_725" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_726" s="T9">n-n:num.[n:case]</ta>
            <ta e="T11" id="Seg_727" s="T10">v-v:pn</ta>
            <ta e="T12" id="Seg_728" s="T11">dem</ta>
            <ta e="T13" id="Seg_729" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_730" s="T13">n.[v:pn]</ta>
            <ta e="T15" id="Seg_731" s="T14">nprop.[v:pn]</ta>
            <ta e="T16" id="Seg_732" s="T15">pers-n:case</ta>
            <ta e="T17" id="Seg_733" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_734" s="T17">v-v:inf</ta>
            <ta e="T19" id="Seg_735" s="T18">pers.[n:case]</ta>
            <ta e="T20" id="Seg_736" s="T19">pers</ta>
            <ta e="T21" id="Seg_737" s="T20">quant.[n:case]</ta>
            <ta e="T22" id="Seg_738" s="T21">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_739" s="T22">n-n:num.[n:case]</ta>
            <ta e="T24" id="Seg_740" s="T23">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_741" s="T24">n.[n:case]-n:poss</ta>
            <ta e="T26" id="Seg_742" s="T25">v-v&gt;adv</ta>
            <ta e="T27" id="Seg_743" s="T26">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T28" id="Seg_744" s="T27">conj</ta>
            <ta e="T29" id="Seg_745" s="T28">num</ta>
            <ta e="T30" id="Seg_746" s="T29">n.[n:case]</ta>
            <ta e="T31" id="Seg_747" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_748" s="T31">v-v:pn</ta>
            <ta e="T33" id="Seg_749" s="T32">conj</ta>
            <ta e="T34" id="Seg_750" s="T33">num-num&gt;adj</ta>
            <ta e="T35" id="Seg_751" s="T34">n.[n:case]</ta>
            <ta e="T36" id="Seg_752" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_753" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_754" s="T37">v.[v:pn]</ta>
            <ta e="T39" id="Seg_755" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_756" s="T39">n.[n:case]-n:poss</ta>
            <ta e="T41" id="Seg_757" s="T40">v-v&gt;v-v:tense.[n:case]</ta>
            <ta e="T42" id="Seg_758" s="T41">pers</ta>
            <ta e="T43" id="Seg_759" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_760" s="T43">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T45" id="Seg_761" s="T44">v-v:inf</ta>
            <ta e="T46" id="Seg_762" s="T45">pers</ta>
            <ta e="T47" id="Seg_763" s="T46">n.[n:case]-n:poss</ta>
            <ta e="T48" id="Seg_764" s="T47">v-v&gt;v-v:tense.[n:case]</ta>
            <ta e="T49" id="Seg_765" s="T48">adv</ta>
            <ta e="T50" id="Seg_766" s="T49">v-v:ins.[v:pn]</ta>
            <ta e="T51" id="Seg_767" s="T50">pers.[n:case]</ta>
            <ta e="T52" id="Seg_768" s="T51">conj</ta>
            <ta e="T53" id="Seg_769" s="T52">n.[n:case]-n:poss</ta>
            <ta e="T54" id="Seg_770" s="T53">dem</ta>
            <ta e="T55" id="Seg_771" s="T54">nprop-n:case</ta>
            <ta e="T56" id="Seg_772" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_773" s="T56">v-v:pn</ta>
            <ta e="T58" id="Seg_774" s="T57">adv</ta>
            <ta e="T59" id="Seg_775" s="T58">v-v:tense.[v:pn]</ta>
            <ta e="T60" id="Seg_776" s="T59">n.[n:case]-n:poss</ta>
            <ta e="T61" id="Seg_777" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_778" s="T61">v-v:mood-v:pn</ta>
            <ta e="T63" id="Seg_779" s="T62">pers.[n:case]</ta>
            <ta e="T64" id="Seg_780" s="T63">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_781" s="T64">nprop-n:case</ta>
            <ta e="T66" id="Seg_782" s="T65">num</ta>
            <ta e="T67" id="Seg_783" s="T66">n.[n:case]-n:poss</ta>
            <ta e="T68" id="Seg_784" s="T67">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_785" s="T1">n</ta>
            <ta e="T3" id="Seg_786" s="T2">n</ta>
            <ta e="T4" id="Seg_787" s="T3">n</ta>
            <ta e="T5" id="Seg_788" s="T4">v</ta>
            <ta e="T6" id="Seg_789" s="T5">n</ta>
            <ta e="T7" id="Seg_790" s="T6">v</ta>
            <ta e="T8" id="Seg_791" s="T7">pers</ta>
            <ta e="T9" id="Seg_792" s="T8">n</ta>
            <ta e="T10" id="Seg_793" s="T9">n</ta>
            <ta e="T11" id="Seg_794" s="T10">v</ta>
            <ta e="T12" id="Seg_795" s="T11">dem</ta>
            <ta e="T13" id="Seg_796" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_797" s="T13">n</ta>
            <ta e="T15" id="Seg_798" s="T14">nprop</ta>
            <ta e="T16" id="Seg_799" s="T15">pers</ta>
            <ta e="T17" id="Seg_800" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_801" s="T17">v</ta>
            <ta e="T19" id="Seg_802" s="T18">pers</ta>
            <ta e="T20" id="Seg_803" s="T19">pers</ta>
            <ta e="T21" id="Seg_804" s="T20">quant</ta>
            <ta e="T22" id="Seg_805" s="T21">v</ta>
            <ta e="T23" id="Seg_806" s="T22">n</ta>
            <ta e="T24" id="Seg_807" s="T23">v</ta>
            <ta e="T25" id="Seg_808" s="T24">n</ta>
            <ta e="T26" id="Seg_809" s="T25">adv</ta>
            <ta e="T27" id="Seg_810" s="T26">v</ta>
            <ta e="T28" id="Seg_811" s="T27">conj</ta>
            <ta e="T29" id="Seg_812" s="T28">num</ta>
            <ta e="T30" id="Seg_813" s="T29">n</ta>
            <ta e="T31" id="Seg_814" s="T30">n</ta>
            <ta e="T32" id="Seg_815" s="T31">v</ta>
            <ta e="T33" id="Seg_816" s="T32">conj</ta>
            <ta e="T34" id="Seg_817" s="T33">adj</ta>
            <ta e="T35" id="Seg_818" s="T34">n</ta>
            <ta e="T36" id="Seg_819" s="T35">n</ta>
            <ta e="T37" id="Seg_820" s="T36">n</ta>
            <ta e="T38" id="Seg_821" s="T37">v</ta>
            <ta e="T39" id="Seg_822" s="T38">n</ta>
            <ta e="T40" id="Seg_823" s="T39">n</ta>
            <ta e="T41" id="Seg_824" s="T40">v</ta>
            <ta e="T42" id="Seg_825" s="T41">pers</ta>
            <ta e="T43" id="Seg_826" s="T42">n</ta>
            <ta e="T44" id="Seg_827" s="T43">v</ta>
            <ta e="T45" id="Seg_828" s="T44">v</ta>
            <ta e="T46" id="Seg_829" s="T45">pers</ta>
            <ta e="T47" id="Seg_830" s="T46">n</ta>
            <ta e="T48" id="Seg_831" s="T47">v</ta>
            <ta e="T49" id="Seg_832" s="T48">adv</ta>
            <ta e="T50" id="Seg_833" s="T49">v</ta>
            <ta e="T51" id="Seg_834" s="T50">pers</ta>
            <ta e="T52" id="Seg_835" s="T51">conj</ta>
            <ta e="T53" id="Seg_836" s="T52">n</ta>
            <ta e="T54" id="Seg_837" s="T53">dem</ta>
            <ta e="T55" id="Seg_838" s="T54">nprop</ta>
            <ta e="T56" id="Seg_839" s="T55">n</ta>
            <ta e="T57" id="Seg_840" s="T56">v</ta>
            <ta e="T58" id="Seg_841" s="T57">adv</ta>
            <ta e="T59" id="Seg_842" s="T58">v</ta>
            <ta e="T60" id="Seg_843" s="T59">n</ta>
            <ta e="T61" id="Seg_844" s="T60">n</ta>
            <ta e="T62" id="Seg_845" s="T61">v</ta>
            <ta e="T63" id="Seg_846" s="T62">pers</ta>
            <ta e="T64" id="Seg_847" s="T63">v</ta>
            <ta e="T65" id="Seg_848" s="T64">nprop</ta>
            <ta e="T66" id="Seg_849" s="T65">num</ta>
            <ta e="T67" id="Seg_850" s="T66">n</ta>
            <ta e="T68" id="Seg_851" s="T67">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_852" s="T1">np.h:Poss</ta>
            <ta e="T3" id="Seg_853" s="T2">np.h:B</ta>
            <ta e="T4" id="Seg_854" s="T3">np:L</ta>
            <ta e="T6" id="Seg_855" s="T5">np:Th</ta>
            <ta e="T7" id="Seg_856" s="T6">0.3.h:A 0.3:Th</ta>
            <ta e="T8" id="Seg_857" s="T7">pro.h:Poss</ta>
            <ta e="T9" id="Seg_858" s="T8">np:G</ta>
            <ta e="T10" id="Seg_859" s="T9">np.h:A</ta>
            <ta e="T12" id="Seg_860" s="T11">pro:Th</ta>
            <ta e="T16" id="Seg_861" s="T15">pro.h:R</ta>
            <ta e="T18" id="Seg_862" s="T17">v:Th</ta>
            <ta e="T19" id="Seg_863" s="T18">pro.h:A</ta>
            <ta e="T20" id="Seg_864" s="T19">pro.h:R</ta>
            <ta e="T21" id="Seg_865" s="T20">pro:Th</ta>
            <ta e="T23" id="Seg_866" s="T22">np:Th</ta>
            <ta e="T24" id="Seg_867" s="T23">0.3.h:A</ta>
            <ta e="T25" id="Seg_868" s="T24">np.h:A 0.1.h:Poss</ta>
            <ta e="T30" id="Seg_869" s="T29">np:Th</ta>
            <ta e="T31" id="Seg_870" s="T30">np:L</ta>
            <ta e="T35" id="Seg_871" s="T34">np:Th</ta>
            <ta e="T36" id="Seg_872" s="T35">np:Poss</ta>
            <ta e="T37" id="Seg_873" s="T36">np:L</ta>
            <ta e="T39" id="Seg_874" s="T38">np.h:Poss</ta>
            <ta e="T40" id="Seg_875" s="T39">np.h:A</ta>
            <ta e="T42" id="Seg_876" s="T41">pro.h:Poss</ta>
            <ta e="T43" id="Seg_877" s="T42">np.h:R</ta>
            <ta e="T44" id="Seg_878" s="T43">0.3.h:A</ta>
            <ta e="T46" id="Seg_879" s="T45">pro.h:Poss</ta>
            <ta e="T47" id="Seg_880" s="T46">np.h:A</ta>
            <ta e="T49" id="Seg_881" s="T48">adv:Time</ta>
            <ta e="T51" id="Seg_882" s="T50">pro.h:P</ta>
            <ta e="T53" id="Seg_883" s="T52">np.h:A 0.1.h:Poss</ta>
            <ta e="T55" id="Seg_884" s="T54">np.h:P</ta>
            <ta e="T56" id="Seg_885" s="T55">np:L</ta>
            <ta e="T59" id="Seg_886" s="T58">0.3.h:P</ta>
            <ta e="T60" id="Seg_887" s="T59">np.h:A 0.1.h:Poss</ta>
            <ta e="T61" id="Seg_888" s="T60">np:Th</ta>
            <ta e="T63" id="Seg_889" s="T62">pro.h:E</ta>
            <ta e="T65" id="Seg_890" s="T64">np.h:L</ta>
            <ta e="T67" id="Seg_891" s="T66">np.h:Th 0.3.h:Poss</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_892" s="T2">np.h:S</ta>
            <ta e="T5" id="Seg_893" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_894" s="T5">np:O</ta>
            <ta e="T7" id="Seg_895" s="T6">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T10" id="Seg_896" s="T9">np.h:S</ta>
            <ta e="T11" id="Seg_897" s="T10">v:pred</ta>
            <ta e="T12" id="Seg_898" s="T11">pro:S</ta>
            <ta e="T14" id="Seg_899" s="T13">n:pred</ta>
            <ta e="T17" id="Seg_900" s="T16">ptcl:pred</ta>
            <ta e="T18" id="Seg_901" s="T17">v:O</ta>
            <ta e="T19" id="Seg_902" s="T18">pro.h:S</ta>
            <ta e="T21" id="Seg_903" s="T20">pro:O</ta>
            <ta e="T22" id="Seg_904" s="T21">v:pred</ta>
            <ta e="T23" id="Seg_905" s="T22">np:O</ta>
            <ta e="T24" id="Seg_906" s="T23">0.3.h:S v:pred</ta>
            <ta e="T25" id="Seg_907" s="T24">np.h:S</ta>
            <ta e="T27" id="Seg_908" s="T26">v:pred</ta>
            <ta e="T30" id="Seg_909" s="T29">np:S</ta>
            <ta e="T32" id="Seg_910" s="T31">v:pred</ta>
            <ta e="T35" id="Seg_911" s="T34">np:S</ta>
            <ta e="T38" id="Seg_912" s="T37">v:pred</ta>
            <ta e="T40" id="Seg_913" s="T39">np.h:S</ta>
            <ta e="T41" id="Seg_914" s="T40">v:pred</ta>
            <ta e="T44" id="Seg_915" s="T43">0.3.h:S v:pred</ta>
            <ta e="T47" id="Seg_916" s="T46">np.h:S</ta>
            <ta e="T48" id="Seg_917" s="T47">v:pred</ta>
            <ta e="T50" id="Seg_918" s="T49">v:pred</ta>
            <ta e="T51" id="Seg_919" s="T50">pro.h:S</ta>
            <ta e="T53" id="Seg_920" s="T52">np.h:S</ta>
            <ta e="T55" id="Seg_921" s="T54">np.h:O</ta>
            <ta e="T57" id="Seg_922" s="T56">v:pred</ta>
            <ta e="T59" id="Seg_923" s="T58">0.3.h:S v:pred</ta>
            <ta e="T60" id="Seg_924" s="T59">np.h:S</ta>
            <ta e="T61" id="Seg_925" s="T60">np:O</ta>
            <ta e="T62" id="Seg_926" s="T61">v:pred</ta>
            <ta e="T63" id="Seg_927" s="T62">pro.h:S</ta>
            <ta e="T64" id="Seg_928" s="T63">v:pred</ta>
            <ta e="T67" id="Seg_929" s="T66">np.h:S</ta>
            <ta e="T68" id="Seg_930" s="T67">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_931" s="T1">RUS:core</ta>
            <ta e="T17" id="Seg_932" s="T16">RUS:mod</ta>
            <ta e="T21" id="Seg_933" s="T20">RUS:core</ta>
            <ta e="T25" id="Seg_934" s="T24">RUS:core</ta>
            <ta e="T28" id="Seg_935" s="T27">RUS:gram</ta>
            <ta e="T33" id="Seg_936" s="T32">RUS:gram</ta>
            <ta e="T39" id="Seg_937" s="T38">RUS:core</ta>
            <ta e="T49" id="Seg_938" s="T48">RUS:disc</ta>
            <ta e="T52" id="Seg_939" s="T51">RUS:gram</ta>
            <ta e="T53" id="Seg_940" s="T52">RUS:core</ta>
            <ta e="T58" id="Seg_941" s="T57">RUS:core</ta>
            <ta e="T60" id="Seg_942" s="T59">RUS:core</ta>
            <ta e="T61" id="Seg_943" s="T60">RUS:cult</ta>
            <ta e="T62" id="Seg_944" s="T61">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_945" s="T1">The father of the grandfather found an icon in the taiga.</ta>
            <ta e="T9" id="Seg_946" s="T6">He brought it home.</ta>
            <ta e="T15" id="Seg_947" s="T9">People say: “It's not a god – Masse.</ta>
            <ta e="T18" id="Seg_948" s="T15">You should pray to him.</ta>
            <ta e="T24" id="Seg_949" s="T18">He'll give you everything, he'll give you wild animals.</ta>
            <ta e="T27" id="Seg_950" s="T24">My grandfather started hunting.</ta>
            <ta e="T38" id="Seg_951" s="T27">And there were two squirrels in traps, and the third squirrel lay near a trap.</ta>
            <ta e="T41" id="Seg_952" s="T38">The father of the grandfather was praying.</ta>
            <ta e="T45" id="Seg_953" s="T41">And he told my father to pray.</ta>
            <ta e="T48" id="Seg_954" s="T45">My father was praying.</ta>
            <ta e="T51" id="Seg_955" s="T48">Then he died.</ta>
            <ta e="T57" id="Seg_956" s="T51">And my mother burnt this Masse in fire.</ta>
            <ta e="T62" id="Seg_957" s="T57">It was burning so, that my mother closed the oven door.</ta>
            <ta e="T64" id="Seg_958" s="T62">We were afraid.</ta>
            <ta e="T68" id="Seg_959" s="T64">Masse has seven sons sitting [by him].</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_960" s="T1">Großvaters Vater fand in der Taiga eine Götze.</ta>
            <ta e="T9" id="Seg_961" s="T6">Er brachte sie nach Hause.</ta>
            <ta e="T15" id="Seg_962" s="T9">Die Leute sagen: "Es ist kein Gott -- Masse.</ta>
            <ta e="T18" id="Seg_963" s="T15">Man muss zu ihm beten.</ta>
            <ta e="T24" id="Seg_964" s="T18">Er gibt dir alles, er gibt dir wilde Tiere.</ta>
            <ta e="T27" id="Seg_965" s="T24">Mein Großvater begann zu jagen.</ta>
            <ta e="T38" id="Seg_966" s="T27">Und es waren zwei tote Eichhörnchen in den Fallen, ein drittes Eichhörnchen lag neben der Falle.</ta>
            <ta e="T41" id="Seg_967" s="T38">Großvaters Vater betete.</ta>
            <ta e="T45" id="Seg_968" s="T41">Und er trug meinem Vater auf zu beten.</ta>
            <ta e="T48" id="Seg_969" s="T45">Mein Vater betete.</ta>
            <ta e="T51" id="Seg_970" s="T48">Dann starb er.</ta>
            <ta e="T57" id="Seg_971" s="T51">Und meine Mutter verbrannte diesen Masse im Feuer.</ta>
            <ta e="T62" id="Seg_972" s="T57">Er brannte so sehr, dass meine Mutter die Ofentür schloss.</ta>
            <ta e="T64" id="Seg_973" s="T62">Wir hatten Angst.</ta>
            <ta e="T68" id="Seg_974" s="T64">Bei Masse sitzen seine sieben Söhne.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_975" s="T1">Дедушкин отец в тайге нашел икону.</ta>
            <ta e="T9" id="Seg_976" s="T6">Принес ее в дом.</ta>
            <ta e="T15" id="Seg_977" s="T9">Люди говорят: “Это не бог – Массе.</ta>
            <ta e="T18" id="Seg_978" s="T15">Ему надо молиться.</ta>
            <ta e="T24" id="Seg_979" s="T18">Он тебе все давать будет, зверей даст”.</ta>
            <ta e="T27" id="Seg_980" s="T24">Дедушка мой стал промышлять.</ta>
            <ta e="T38" id="Seg_981" s="T27">И две белки в капканах (чарканах), а третья белка около капкана лежит.</ta>
            <ta e="T41" id="Seg_982" s="T38">Дедушкин отец молился (на икону).</ta>
            <ta e="T45" id="Seg_983" s="T41">Моему отцу говорил (велел) молиться.</ta>
            <ta e="T48" id="Seg_984" s="T45">Мой отец молился.</ta>
            <ta e="T51" id="Seg_985" s="T48">Потом умер он.</ta>
            <ta e="T57" id="Seg_986" s="T51">А мама моя этого Массе в огне сожгла.</ta>
            <ta e="T62" id="Seg_987" s="T57">Так сильно горел, мать моя заслонку заслонила.</ta>
            <ta e="T64" id="Seg_988" s="T62">Мы боялись.</ta>
            <ta e="T68" id="Seg_989" s="T64">У Массе (бога) семь сыновей сидят.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_990" s="T1">дедушкин отец в тайге нашел икону</ta>
            <ta e="T9" id="Seg_991" s="T6">принес ее в дом</ta>
            <ta e="T15" id="Seg_992" s="T9">люди говорят это не бог а остяцкий хозяин</ta>
            <ta e="T18" id="Seg_993" s="T15">ему надо молиться</ta>
            <ta e="T24" id="Seg_994" s="T18">он тебе все давать будет зверей дает</ta>
            <ta e="T27" id="Seg_995" s="T24">дедушка стал промышлять</ta>
            <ta e="T38" id="Seg_996" s="T27">две белки в чарканах третья белка чарка около лежит</ta>
            <ta e="T41" id="Seg_997" s="T38">дедушки отец молился</ta>
            <ta e="T45" id="Seg_998" s="T41">моему отцу говорил (велел) молиться</ta>
            <ta e="T48" id="Seg_999" s="T45">мой отец молился</ta>
            <ta e="T51" id="Seg_1000" s="T48">умер он</ta>
            <ta e="T57" id="Seg_1001" s="T51">а мама (моя) эту икоу в огне сожгла</ta>
            <ta e="T62" id="Seg_1002" s="T57">так горела мать заслонку заслонила</ta>
            <ta e="T64" id="Seg_1003" s="T62">мы боялись</ta>
            <ta e="T68" id="Seg_1004" s="T64">у масена (бога) семь сыновей сидят</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T6" id="Seg_1005" s="T1">[BrM:] Unexpected EP-vowel in 'dʼedʼaon'.</ta>
            <ta e="T9" id="Seg_1006" s="T6">[BrM:] Accusative form 'täb-ɨ-m' would be expected instead of the genitive one.</ta>
            <ta e="T15" id="Seg_1007" s="T9">[KuAI:] Masse is a Selkup master, a god of all animals.</ta>
            <ta e="T45" id="Seg_1008" s="T41">[BrM:] LOC?</ta>
            <ta e="T62" id="Seg_1009" s="T57">[BrM:] 'da tao' changed to 'datao'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T15" id="Seg_1010" s="T9">бог всем зверькам</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
