<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>SG_196X_Cow_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">SG_196X_Cow_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">21</ud-information>
            <ud-information attribute-name="# HIAT:w">16</ud-information>
            <ud-information attribute-name="# e">16</ud-information>
            <ud-information attribute-name="# HIAT:u">5</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SG">
            <abbreviation>SG</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SG"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T16" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">tapčel</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">Kosta</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">Čičigin</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">tümba</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">tadɨmbat</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">sɨːrəp</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">alagoze</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">üdəmbad</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">konä</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">qätparond</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_41" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">taper</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">aulusʼ</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">molokap</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_53" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_55" n="HIAT:w" s="T13">mat</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_58" n="HIAT:w" s="T14">awurgu</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">kɨkag</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T16" id="Seg_64" n="sc" s="T0">
               <ts e="T1" id="Seg_66" n="e" s="T0">tapčel </ts>
               <ts e="T2" id="Seg_68" n="e" s="T1">Kosta </ts>
               <ts e="T3" id="Seg_70" n="e" s="T2">Čičigin </ts>
               <ts e="T4" id="Seg_72" n="e" s="T3">tümba. </ts>
               <ts e="T5" id="Seg_74" n="e" s="T4">tadɨmbat </ts>
               <ts e="T6" id="Seg_76" n="e" s="T5">sɨːrəp </ts>
               <ts e="T7" id="Seg_78" n="e" s="T6">alagoze. </ts>
               <ts e="T8" id="Seg_80" n="e" s="T7">üdəmbad </ts>
               <ts e="T9" id="Seg_82" n="e" s="T8">konä </ts>
               <ts e="T10" id="Seg_84" n="e" s="T9">qätparond. </ts>
               <ts e="T11" id="Seg_86" n="e" s="T10">taper </ts>
               <ts e="T12" id="Seg_88" n="e" s="T11">aulusʼ </ts>
               <ts e="T13" id="Seg_90" n="e" s="T12">molokap. </ts>
               <ts e="T14" id="Seg_92" n="e" s="T13">mat </ts>
               <ts e="T15" id="Seg_94" n="e" s="T14">awurgu </ts>
               <ts e="T16" id="Seg_96" n="e" s="T15">kɨkag. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_97" s="T0">SG_196X_Cow_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_98" s="T4">SG_196X_Cow_nar.002 (001.002)</ta>
            <ta e="T10" id="Seg_99" s="T7">SG_196X_Cow_nar.003 (001.003)</ta>
            <ta e="T13" id="Seg_100" s="T10">SG_196X_Cow_nar.004 (001.004)</ta>
            <ta e="T16" id="Seg_101" s="T13">SG_196X_Cow_nar.005 (001.005)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_102" s="T0">тап′чел коста чичигин ′тӱмба.</ta>
            <ta e="T7" id="Seg_103" s="T4">′тадымбат сы̄ръп ала′гозе.</ta>
            <ta e="T10" id="Seg_104" s="T7">′ӱдъмбад ко′нӓ ′kӓт‵паронд.</ta>
            <ta e="T13" id="Seg_105" s="T10">та′пер ау′lусʼ мо′lокап.</ta>
            <ta e="T16" id="Seg_106" s="T13">мат авур′гу кы′каг.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_107" s="T0">tapčel kosta čičigin tümba.</ta>
            <ta e="T7" id="Seg_108" s="T4">tadɨmbat sɨːrəp alagoze.</ta>
            <ta e="T10" id="Seg_109" s="T7">üdəmbad konä qätparond.</ta>
            <ta e="T13" id="Seg_110" s="T10">taper aulusʼ molokap.</ta>
            <ta e="T16" id="Seg_111" s="T13">mat avurgu kɨkag.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_112" s="T0">tapčel Kosta Čičigin tümba. </ta>
            <ta e="T7" id="Seg_113" s="T4">tadɨmbat sɨːrəp alagoze. </ta>
            <ta e="T10" id="Seg_114" s="T7">üdəmbad konä qätparond. </ta>
            <ta e="T13" id="Seg_115" s="T10">taper aulusʼ molokap. </ta>
            <ta e="T16" id="Seg_116" s="T13">mat awurgu kɨkag. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_117" s="T0">tap-čel</ta>
            <ta e="T2" id="Seg_118" s="T1">Kosta</ta>
            <ta e="T3" id="Seg_119" s="T2">Čičigin</ta>
            <ta e="T4" id="Seg_120" s="T3">tü-mba</ta>
            <ta e="T5" id="Seg_121" s="T4">tade-mba-t</ta>
            <ta e="T6" id="Seg_122" s="T5">sɨːr-ə-p</ta>
            <ta e="T7" id="Seg_123" s="T6">alago-ze</ta>
            <ta e="T8" id="Seg_124" s="T7">üdə-mba-d</ta>
            <ta e="T9" id="Seg_125" s="T8">konä</ta>
            <ta e="T10" id="Seg_126" s="T9">qä-t-par-o-nd</ta>
            <ta e="T11" id="Seg_127" s="T10">taper</ta>
            <ta e="T12" id="Seg_128" s="T11">au-lu-sʼ</ta>
            <ta e="T13" id="Seg_129" s="T12">moloka-p</ta>
            <ta e="T14" id="Seg_130" s="T13">mat</ta>
            <ta e="T15" id="Seg_131" s="T14">aw-u-r-gu</ta>
            <ta e="T16" id="Seg_132" s="T15">kɨka-g</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_133" s="T0">taw-čeːl</ta>
            <ta e="T2" id="Seg_134" s="T1">Kosta</ta>
            <ta e="T3" id="Seg_135" s="T2">Čičigin</ta>
            <ta e="T4" id="Seg_136" s="T3">töː-mbɨ</ta>
            <ta e="T5" id="Seg_137" s="T4">tade-mbɨ-tɨ</ta>
            <ta e="T6" id="Seg_138" s="T5">sɨr-ɨ-p</ta>
            <ta e="T7" id="Seg_139" s="T6">alago-se</ta>
            <ta e="T8" id="Seg_140" s="T7">üdɨ-mbɨ-tɨ</ta>
            <ta e="T9" id="Seg_141" s="T8">konne</ta>
            <ta e="T10" id="Seg_142" s="T9">kɨ-n-par-ɨ-nde</ta>
            <ta e="T11" id="Seg_143" s="T10">taper</ta>
            <ta e="T12" id="Seg_144" s="T11">am-j-se</ta>
            <ta e="T13" id="Seg_145" s="T12">moloka-p</ta>
            <ta e="T14" id="Seg_146" s="T13">man</ta>
            <ta e="T15" id="Seg_147" s="T14">am-ɨ-r-gu</ta>
            <ta e="T16" id="Seg_148" s="T15">kɨge-k</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_149" s="T0">this-day.[NOM]</ta>
            <ta e="T2" id="Seg_150" s="T1">Kostja.[NOM]</ta>
            <ta e="T3" id="Seg_151" s="T2">Chichigin.[NOM]</ta>
            <ta e="T4" id="Seg_152" s="T3">come-PST.NAR.[3SG.S]</ta>
            <ta e="T5" id="Seg_153" s="T4">bring-PST.NAR-3SG.O</ta>
            <ta e="T6" id="Seg_154" s="T5">cow-EP-ACC</ta>
            <ta e="T7" id="Seg_155" s="T6">covered.boat-INSTR</ta>
            <ta e="T8" id="Seg_156" s="T7">send-PST.NAR-3SG.O</ta>
            <ta e="T9" id="Seg_157" s="T8">upwards</ta>
            <ta e="T10" id="Seg_158" s="T9">river-GEN-top-EP-ILL</ta>
            <ta e="T11" id="Seg_159" s="T10">now</ta>
            <ta e="T12" id="Seg_160" s="T11">eat-1DU-OPT</ta>
            <ta e="T13" id="Seg_161" s="T12">milk-ACC</ta>
            <ta e="T14" id="Seg_162" s="T13">I.NOM</ta>
            <ta e="T15" id="Seg_163" s="T14">eat-EP-FRQ-INF</ta>
            <ta e="T16" id="Seg_164" s="T15">want-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_165" s="T0">этот-день.[NOM]</ta>
            <ta e="T2" id="Seg_166" s="T1">Костя.[NOM]</ta>
            <ta e="T3" id="Seg_167" s="T2">Чичигин.[NOM]</ta>
            <ta e="T4" id="Seg_168" s="T3">прийти-PST.NAR.[3SG.S]</ta>
            <ta e="T5" id="Seg_169" s="T4">принести-PST.NAR-3SG.O</ta>
            <ta e="T6" id="Seg_170" s="T5">корова-EP-ACC</ta>
            <ta e="T7" id="Seg_171" s="T6">крытая.лодка-INSTR</ta>
            <ta e="T8" id="Seg_172" s="T7">посылать-PST.NAR-3SG.O</ta>
            <ta e="T9" id="Seg_173" s="T8">вверх</ta>
            <ta e="T10" id="Seg_174" s="T9">река-GEN-верхняя.часть-EP-ILL</ta>
            <ta e="T11" id="Seg_175" s="T10">теперь</ta>
            <ta e="T12" id="Seg_176" s="T11">есть-1DU-OPT</ta>
            <ta e="T13" id="Seg_177" s="T12">молоко-ACC</ta>
            <ta e="T14" id="Seg_178" s="T13">я.NOM</ta>
            <ta e="T15" id="Seg_179" s="T14">есть-EP-FRQ-INF</ta>
            <ta e="T16" id="Seg_180" s="T15">хотеть-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_181" s="T0">dem-n-n:case</ta>
            <ta e="T2" id="Seg_182" s="T1">nprop-n:case</ta>
            <ta e="T3" id="Seg_183" s="T2">nprop-n:case</ta>
            <ta e="T4" id="Seg_184" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_185" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_186" s="T5">n-n:ins-n:case</ta>
            <ta e="T7" id="Seg_187" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_188" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_189" s="T8">adv</ta>
            <ta e="T10" id="Seg_190" s="T9">n-n:case-n&gt;n-n:ins-n:case</ta>
            <ta e="T11" id="Seg_191" s="T10">adv</ta>
            <ta e="T12" id="Seg_192" s="T11">v-v:pn-clit</ta>
            <ta e="T13" id="Seg_193" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_194" s="T13">pers</ta>
            <ta e="T15" id="Seg_195" s="T14">v-n:ins-v&gt;v-v:inf</ta>
            <ta e="T16" id="Seg_196" s="T15">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_197" s="T0">n</ta>
            <ta e="T2" id="Seg_198" s="T1">nprop</ta>
            <ta e="T3" id="Seg_199" s="T2">nprop</ta>
            <ta e="T4" id="Seg_200" s="T3">v</ta>
            <ta e="T5" id="Seg_201" s="T4">v</ta>
            <ta e="T6" id="Seg_202" s="T5">n</ta>
            <ta e="T7" id="Seg_203" s="T6">n</ta>
            <ta e="T8" id="Seg_204" s="T7">v</ta>
            <ta e="T9" id="Seg_205" s="T8">adv</ta>
            <ta e="T10" id="Seg_206" s="T9">n</ta>
            <ta e="T11" id="Seg_207" s="T10">adv</ta>
            <ta e="T12" id="Seg_208" s="T11">v</ta>
            <ta e="T13" id="Seg_209" s="T12">n</ta>
            <ta e="T14" id="Seg_210" s="T13">pers</ta>
            <ta e="T15" id="Seg_211" s="T14">v</ta>
            <ta e="T16" id="Seg_212" s="T15">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_213" s="T2">np.h:S</ta>
            <ta e="T4" id="Seg_214" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_215" s="T4">0.3.h:S v:pred</ta>
            <ta e="T6" id="Seg_216" s="T5">np:O</ta>
            <ta e="T8" id="Seg_217" s="T7">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T12" id="Seg_218" s="T11">0.1.h:S v:pred</ta>
            <ta e="T13" id="Seg_219" s="T12">np:O</ta>
            <ta e="T14" id="Seg_220" s="T13">pro.h:S</ta>
            <ta e="T15" id="Seg_221" s="T14">v:O</ta>
            <ta e="T16" id="Seg_222" s="T15">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_223" s="T0">np:Time</ta>
            <ta e="T3" id="Seg_224" s="T2">np.h:A</ta>
            <ta e="T5" id="Seg_225" s="T4">0.3.h:A</ta>
            <ta e="T6" id="Seg_226" s="T5">np:Th</ta>
            <ta e="T7" id="Seg_227" s="T6">np:Ins</ta>
            <ta e="T8" id="Seg_228" s="T7">0.3.h:A 0.3:Th</ta>
            <ta e="T10" id="Seg_229" s="T9">np:G</ta>
            <ta e="T11" id="Seg_230" s="T10">adv:Time</ta>
            <ta e="T12" id="Seg_231" s="T11">0.1.h:A</ta>
            <ta e="T13" id="Seg_232" s="T12">np:P</ta>
            <ta e="T14" id="Seg_233" s="T13">pro.h:E</ta>
            <ta e="T15" id="Seg_234" s="T14">v:Th</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_235" s="T1">RUS:cult</ta>
            <ta e="T3" id="Seg_236" s="T2">RUS:cult</ta>
            <ta e="T11" id="Seg_237" s="T10">RUS:core</ta>
            <ta e="T13" id="Seg_238" s="T12">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_239" s="T0">Сегодня приехал Костя Чичигин.</ta>
            <ta e="T7" id="Seg_240" s="T4">Привез корову на лодке.</ta>
            <ta e="T10" id="Seg_241" s="T7">Отпустил (ее) на берег.</ta>
            <ta e="T13" id="Seg_242" s="T10">Теперь поедим молоко.</ta>
            <ta e="T16" id="Seg_243" s="T13">Я хочу есть.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_244" s="T0">Today Kostya Chichigin has come.</ta>
            <ta e="T7" id="Seg_245" s="T4">He brought a cow with a boat.</ta>
            <ta e="T10" id="Seg_246" s="T7">He let it go to the shore.</ta>
            <ta e="T13" id="Seg_247" s="T10">Now we will drink milk [eat].</ta>
            <ta e="T16" id="Seg_248" s="T13">I want to eat.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_249" s="T0">Heute ist Kostja Tschitschigin gekommen.</ta>
            <ta e="T7" id="Seg_250" s="T4">Er brachte auf dem Boot eine Kuh mit.</ta>
            <ta e="T10" id="Seg_251" s="T7">Er ließ sie an das Ufer gehen.</ta>
            <ta e="T13" id="Seg_252" s="T10">Jetzt werden wir Milch trinken [essen].</ta>
            <ta e="T16" id="Seg_253" s="T13">Ich will essen.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_254" s="T0">сегодня приехал</ta>
            <ta e="T7" id="Seg_255" s="T4">привез корову на лодке</ta>
            <ta e="T10" id="Seg_256" s="T7">отпустил (ее) на берег</ta>
            <ta e="T13" id="Seg_257" s="T10">теперь поедим молоко [есть будем]</ta>
            <ta e="T16" id="Seg_258" s="T13">я хочу есть</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
