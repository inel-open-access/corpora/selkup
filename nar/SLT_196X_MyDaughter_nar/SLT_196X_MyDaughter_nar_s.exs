<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name />
         <referenced-file url="SLT_196X_MyDaughter_nar.wav" />
         <referenced-file url="SLT_196X_MyDaughter_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">SLT_196X_MyDaughter_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">58</ud-information>
            <ud-information attribute-name="# HIAT:w">32</ud-information>
            <ud-information attribute-name="# e">35</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">3</ud-information>
            <ud-information attribute-name="# HIAT:u">3</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SPK">
            <abbreviation>SLT</abbreviation>
            <sex value="f" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="5.12" type="appl" />
         <tli id="T1" time="5.615" type="appl" />
         <tli id="T2" time="6.11" type="appl" />
         <tli id="T3" time="6.605" type="appl" />
         <tli id="T4" time="7.1" type="appl" />
         <tli id="T5" time="7.595" type="appl" />
         <tli id="T6" time="8.09" type="appl" />
         <tli id="T7" time="8.585" type="appl" />
         <tli id="T8" time="9.08" type="appl" />
         <tli id="T9" time="9.575" type="appl" />
         <tli id="T33" time="9.74" type="intp" />
         <tli id="T10" time="10.07" type="appl" />
         <tli id="T11" time="10.571" type="appl" />
         <tli id="T12" time="11.071" type="appl" />
         <tli id="T32" time="11.27975" type="intp" />
         <tli id="T13" time="11.572" type="appl" />
         <tli id="T34" time="11.8225" type="intp" />
         <tli id="T14" time="12.073" type="appl" />
         <tli id="T15" time="12.574" type="appl" />
         <tli id="T35" time="12.824" type="intp" />
         <tli id="T16" time="13.074" type="appl" />
         <tli id="T17" time="13.575" type="appl" />
         <tli id="T18" time="14.076" type="appl" />
         <tli id="T19" time="14.576" type="appl" />
         <tli id="T20" time="15.077" type="appl" />
         <tli id="T21" time="15.578" type="appl" />
         <tli id="T22" time="16.079" type="appl" />
         <tli id="T23" time="16.579" type="appl" />
         <tli id="T24" time="17.08" type="appl" />
         <tli id="T25" time="17.788" type="appl" />
         <tli id="T36" time="18.002499999999998" type="intp" />
         <tli id="T26" time="18.217" type="appl" />
         <tli id="T27" time="18.645" type="appl" />
         <tli id="T28" time="19.073" type="appl" />
         <tli id="T29" time="19.502" type="appl" />
         <tli id="T30" time="19.93" type="appl" />
         <tli id="T31" time="21.751" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SPK"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T30" id="Seg_0" n="sc" s="T0">
               <ts e="T10" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">nem</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">ogolalǯešpa</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">Topqet</ts>
                  <nts id="Seg_14" n="HIAT:ip">,</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">i</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">našak</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">kund</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">čʼwesse</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">aː</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_32" n="HIAT:w" s="T9">dönǯa</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_34" n="HIAT:ip">(</nts>
                  <nts id="Seg_35" n="HIAT:ip">(</nts>
                  <ats e="T10" id="Seg_36" n="HIAT:non-pho" s="T33">DMG</ats>
                  <nts id="Seg_37" n="HIAT:ip">)</nts>
                  <nts id="Seg_38" n="HIAT:ip">)</nts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_42" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_44" n="HIAT:w" s="T10">Mat</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_47" n="HIAT:w" s="T11">tabem</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_50" n="HIAT:w" s="T12">adam</ts>
                  <nts id="Seg_51" n="HIAT:ip">,</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_55" n="HIAT:w" s="T32">adam</ts>
                  <nts id="Seg_56" n="HIAT:ip">,</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_59" n="HIAT:w" s="T13">i</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_61" n="HIAT:ip">(</nts>
                  <ts e="T14" id="Seg_63" n="HIAT:w" s="T34">sʼ-</ts>
                  <nts id="Seg_64" n="HIAT:ip">)</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_67" n="HIAT:w" s="T14">kaʒna</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_70" n="HIAT:w" s="T15">tʼel</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_72" n="HIAT:ip">(</nts>
                  <nts id="Seg_73" n="HIAT:ip">(</nts>
                  <ats e="T16" id="Seg_74" n="HIAT:non-pho" s="T35">…</ats>
                  <nts id="Seg_75" n="HIAT:ip">)</nts>
                  <nts id="Seg_76" n="HIAT:ip">)</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_79" n="HIAT:w" s="T16">erodromundə</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_82" n="HIAT:w" s="T17">kojahak</ts>
                  <nts id="Seg_83" n="HIAT:ip">,</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_86" n="HIAT:w" s="T18">a</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_89" n="HIAT:w" s="T19">tab</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_92" n="HIAT:w" s="T20">wsjo</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_95" n="HIAT:w" s="T21">aː</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_98" n="HIAT:w" s="T22">dönǯa</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_101" n="HIAT:w" s="T23">čʼwesse</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_105" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_107" n="HIAT:w" s="T24">I</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_110" n="HIAT:w" s="T25">kučʼan</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_113" n="HIAT:w" s="T36">eːk</ts>
                  <nts id="Seg_114" n="HIAT:ip">,</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_116" n="HIAT:ip">(</nts>
                  <nts id="Seg_117" n="HIAT:ip">(</nts>
                  <ats e="T27" id="Seg_118" n="HIAT:non-pho" s="T26">…</ats>
                  <nts id="Seg_119" n="HIAT:ip">)</nts>
                  <nts id="Seg_120" n="HIAT:ip">)</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_123" n="HIAT:w" s="T27">eːk</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_126" n="HIAT:w" s="T28">aː</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_129" n="HIAT:w" s="T29">tänwam</ts>
                  <nts id="Seg_130" n="HIAT:ip">.</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T30" id="Seg_132" n="sc" s="T0">
               <ts e="T1" id="Seg_134" n="e" s="T0">Man </ts>
               <ts e="T2" id="Seg_136" n="e" s="T1">nem </ts>
               <ts e="T3" id="Seg_138" n="e" s="T2">ogolalǯešpa </ts>
               <ts e="T4" id="Seg_140" n="e" s="T3">Topqet, </ts>
               <ts e="T5" id="Seg_142" n="e" s="T4">i </ts>
               <ts e="T6" id="Seg_144" n="e" s="T5">našak </ts>
               <ts e="T7" id="Seg_146" n="e" s="T6">kund </ts>
               <ts e="T8" id="Seg_148" n="e" s="T7">čʼwesse </ts>
               <ts e="T9" id="Seg_150" n="e" s="T8">aː </ts>
               <ts e="T33" id="Seg_152" n="e" s="T9">dönǯa </ts>
               <ts e="T10" id="Seg_154" n="e" s="T33">((DMG)). </ts>
               <ts e="T11" id="Seg_156" n="e" s="T10">Mat </ts>
               <ts e="T12" id="Seg_158" n="e" s="T11">tabem </ts>
               <ts e="T32" id="Seg_160" n="e" s="T12">adam, </ts>
               <ts e="T13" id="Seg_162" n="e" s="T32"> adam, </ts>
               <ts e="T34" id="Seg_164" n="e" s="T13">i </ts>
               <ts e="T14" id="Seg_166" n="e" s="T34">(sʼ-) </ts>
               <ts e="T15" id="Seg_168" n="e" s="T14">kaʒna </ts>
               <ts e="T35" id="Seg_170" n="e" s="T15">tʼel </ts>
               <ts e="T16" id="Seg_172" n="e" s="T35">((…)) </ts>
               <ts e="T17" id="Seg_174" n="e" s="T16">erodromundə </ts>
               <ts e="T18" id="Seg_176" n="e" s="T17">kojahak, </ts>
               <ts e="T19" id="Seg_178" n="e" s="T18">a </ts>
               <ts e="T20" id="Seg_180" n="e" s="T19">tab </ts>
               <ts e="T21" id="Seg_182" n="e" s="T20">wsjo </ts>
               <ts e="T22" id="Seg_184" n="e" s="T21">aː </ts>
               <ts e="T23" id="Seg_186" n="e" s="T22">dönǯa </ts>
               <ts e="T24" id="Seg_188" n="e" s="T23">čʼwesse. </ts>
               <ts e="T25" id="Seg_190" n="e" s="T24">I </ts>
               <ts e="T36" id="Seg_192" n="e" s="T25">kučʼan </ts>
               <ts e="T26" id="Seg_194" n="e" s="T36">eːk, </ts>
               <ts e="T27" id="Seg_196" n="e" s="T26">((…)) </ts>
               <ts e="T28" id="Seg_198" n="e" s="T27">eːk </ts>
               <ts e="T29" id="Seg_200" n="e" s="T28">aː </ts>
               <ts e="T30" id="Seg_202" n="e" s="T29">tänwam. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T10" id="Seg_203" s="T0">SLT_196X_MyDaughter_nar.001 (001)</ta>
            <ta e="T24" id="Seg_204" s="T10">SLT_196X_MyDaughter_nar.002 (002)</ta>
            <ta e="T30" id="Seg_205" s="T24">SLT_196X_MyDaughter_nar.003 (003)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T10" id="Seg_206" s="T0">Ман нэм о́голалджэшпа То́пӄэт, и наша́к кунд чвэ́ссе а̄ тӧнджа.</ta>
            <ta e="T24" id="Seg_207" s="T10">Мат та́бэп ада́м-ада́м, и ка́жна чел аэродро́мунд коя́ҳак, а таб всё а̄ тӧнджа чвэ́ссе.</ta>
            <ta e="T30" id="Seg_208" s="T24">И куча́н таб э̄к — а̄ дӓнвам.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T10" id="Seg_209" s="T0">Man nem ogolalǯešpa Topqet, i našak kund čʼvesse aː tönǯa.</ta>
            <ta e="T24" id="Seg_210" s="T10">Mat tabep adam-adam, i kaʒna čel aerodromund kojaxak, a tab vsjo aː tönǯa čʼvesse.</ta>
            <ta e="T30" id="Seg_211" s="T24">I kučan tab eːk — aː dänvam.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T10" id="Seg_212" s="T0">Man nem ogolalǯešpa Topqet, i našak kund čʼwesse aː dönǯa ((DMG)). </ta>
            <ta e="T24" id="Seg_213" s="T10">Mat tabem adam, adam, i (sʼ-) kaʒna tʼel ((…)) erodromundə kojahak, a tab wsjo aː dönǯa čʼwesse. </ta>
            <ta e="T30" id="Seg_214" s="T24">I kučʼan eːk, ((…)) eːk aː tänwam. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_215" s="T0">man</ta>
            <ta e="T2" id="Seg_216" s="T1">ne-m</ta>
            <ta e="T3" id="Seg_217" s="T2">ogol-a-lǯe-špa</ta>
            <ta e="T4" id="Seg_218" s="T3">Top-qet</ta>
            <ta e="T5" id="Seg_219" s="T4">i</ta>
            <ta e="T6" id="Seg_220" s="T5">naša-k</ta>
            <ta e="T7" id="Seg_221" s="T6">kund</ta>
            <ta e="T8" id="Seg_222" s="T7">čʼwesse</ta>
            <ta e="T9" id="Seg_223" s="T8">aː</ta>
            <ta e="T33" id="Seg_224" s="T9">dö-nǯa</ta>
            <ta e="T11" id="Seg_225" s="T10">mat</ta>
            <ta e="T12" id="Seg_226" s="T11">tabe-m</ta>
            <ta e="T32" id="Seg_227" s="T12">ada-m</ta>
            <ta e="T13" id="Seg_228" s="T32">ada-m</ta>
            <ta e="T34" id="Seg_229" s="T13">i</ta>
            <ta e="T15" id="Seg_230" s="T14">kaʒna</ta>
            <ta e="T35" id="Seg_231" s="T15">tʼel</ta>
            <ta e="T17" id="Seg_232" s="T16">erodrom-u-ndə</ta>
            <ta e="T18" id="Seg_233" s="T17">koja-ha-k</ta>
            <ta e="T19" id="Seg_234" s="T18">a</ta>
            <ta e="T20" id="Seg_235" s="T19">tab</ta>
            <ta e="T21" id="Seg_236" s="T20">wsjo</ta>
            <ta e="T22" id="Seg_237" s="T21">aː</ta>
            <ta e="T23" id="Seg_238" s="T22">dö-nǯa</ta>
            <ta e="T24" id="Seg_239" s="T23">čʼwesse</ta>
            <ta e="T25" id="Seg_240" s="T24">i</ta>
            <ta e="T36" id="Seg_241" s="T25">kučʼa-n</ta>
            <ta e="T26" id="Seg_242" s="T36">eː-k</ta>
            <ta e="T28" id="Seg_243" s="T27">eː-k</ta>
            <ta e="T29" id="Seg_244" s="T28">aː</ta>
            <ta e="T30" id="Seg_245" s="T29">tän-wa-m</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_246" s="T0">man</ta>
            <ta e="T2" id="Seg_247" s="T1">neː-mɨ</ta>
            <ta e="T3" id="Seg_248" s="T2">oqol-ɨ-lʼčǝ-špɨ</ta>
            <ta e="T4" id="Seg_249" s="T3">Tom-qɨn</ta>
            <ta e="T5" id="Seg_250" s="T4">i</ta>
            <ta e="T6" id="Seg_251" s="T5">nača-k</ta>
            <ta e="T7" id="Seg_252" s="T6">kundɨ</ta>
            <ta e="T8" id="Seg_253" s="T7">čwesse</ta>
            <ta e="T9" id="Seg_254" s="T8">aː</ta>
            <ta e="T33" id="Seg_255" s="T9">töː-nǯe</ta>
            <ta e="T11" id="Seg_256" s="T10">man</ta>
            <ta e="T12" id="Seg_257" s="T11">tab-m</ta>
            <ta e="T32" id="Seg_258" s="T12">aːdɨ-m</ta>
            <ta e="T13" id="Seg_259" s="T32">aːdɨ-m</ta>
            <ta e="T34" id="Seg_260" s="T13">i</ta>
            <ta e="T15" id="Seg_261" s="T14">kaʒnɨj</ta>
            <ta e="T35" id="Seg_262" s="T15">čeːl</ta>
            <ta e="T17" id="Seg_263" s="T16">aerodrom-ɨ-nde</ta>
            <ta e="T18" id="Seg_264" s="T17">koja-sɨ-k</ta>
            <ta e="T19" id="Seg_265" s="T18">a</ta>
            <ta e="T20" id="Seg_266" s="T19">tab</ta>
            <ta e="T21" id="Seg_267" s="T20">wsjo</ta>
            <ta e="T22" id="Seg_268" s="T21">aː</ta>
            <ta e="T23" id="Seg_269" s="T22">töː-nǯe</ta>
            <ta e="T24" id="Seg_270" s="T23">čwesse</ta>
            <ta e="T25" id="Seg_271" s="T24">i</ta>
            <ta e="T36" id="Seg_272" s="T25">kuča-n</ta>
            <ta e="T26" id="Seg_273" s="T36">e-k</ta>
            <ta e="T28" id="Seg_274" s="T27">e-k</ta>
            <ta e="T29" id="Seg_275" s="T28">aː</ta>
            <ta e="T30" id="Seg_276" s="T29">tanu-wa-m</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_277" s="T0">I.GEN</ta>
            <ta e="T2" id="Seg_278" s="T1">daughter-1SG</ta>
            <ta e="T3" id="Seg_279" s="T2">learn-EP-PFV-IPFV2.[3SG.S]</ta>
            <ta e="T4" id="Seg_280" s="T3">Tomsk-LOC</ta>
            <ta e="T5" id="Seg_281" s="T4">and</ta>
            <ta e="T6" id="Seg_282" s="T5">there-ADVZ</ta>
            <ta e="T7" id="Seg_283" s="T6">long</ta>
            <ta e="T8" id="Seg_284" s="T7">backward</ta>
            <ta e="T9" id="Seg_285" s="T8">NEG</ta>
            <ta e="T33" id="Seg_286" s="T9">come-IPFV3.[3SG.S]</ta>
            <ta e="T11" id="Seg_287" s="T10">I.NOM</ta>
            <ta e="T12" id="Seg_288" s="T11">(s)he-ACC</ta>
            <ta e="T32" id="Seg_289" s="T12">wait-1SG.O</ta>
            <ta e="T13" id="Seg_290" s="T32">wait-1SG.O</ta>
            <ta e="T34" id="Seg_291" s="T13">and</ta>
            <ta e="T15" id="Seg_292" s="T14">every</ta>
            <ta e="T35" id="Seg_293" s="T15">day.[NOM]</ta>
            <ta e="T17" id="Seg_294" s="T16">airport-EP-ILL</ta>
            <ta e="T18" id="Seg_295" s="T17">go-PST-1SG.S</ta>
            <ta e="T19" id="Seg_296" s="T18">but</ta>
            <ta e="T20" id="Seg_297" s="T19">(s)he.[NOM]</ta>
            <ta e="T21" id="Seg_298" s="T20">still</ta>
            <ta e="T22" id="Seg_299" s="T21">NEG</ta>
            <ta e="T23" id="Seg_300" s="T22">come-IPFV3.[3SG.S]</ta>
            <ta e="T24" id="Seg_301" s="T23">backward</ta>
            <ta e="T25" id="Seg_302" s="T24">and</ta>
            <ta e="T36" id="Seg_303" s="T25">where-ADV.LOC</ta>
            <ta e="T26" id="Seg_304" s="T36">be-3SG.S</ta>
            <ta e="T28" id="Seg_305" s="T27">be-3SG.S</ta>
            <ta e="T29" id="Seg_306" s="T28">NEG</ta>
            <ta e="T30" id="Seg_307" s="T29">think-CO-1SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_308" s="T0">я.GEN</ta>
            <ta e="T2" id="Seg_309" s="T1">дочь-1SG</ta>
            <ta e="T3" id="Seg_310" s="T2">учиться-EP-PFV-IPFV2.[3SG.S]</ta>
            <ta e="T4" id="Seg_311" s="T3">Томск-LOC</ta>
            <ta e="T5" id="Seg_312" s="T4">и</ta>
            <ta e="T6" id="Seg_313" s="T5">туда-ADVZ</ta>
            <ta e="T7" id="Seg_314" s="T6">длинный</ta>
            <ta e="T8" id="Seg_315" s="T7">назад</ta>
            <ta e="T9" id="Seg_316" s="T8">NEG</ta>
            <ta e="T33" id="Seg_317" s="T9">прийти-IPFV3.[3SG.S]</ta>
            <ta e="T11" id="Seg_318" s="T10">я.NOM</ta>
            <ta e="T12" id="Seg_319" s="T11">он(а)-ACC</ta>
            <ta e="T32" id="Seg_320" s="T12">ждать-1SG.O</ta>
            <ta e="T13" id="Seg_321" s="T32">ждать-1SG.O</ta>
            <ta e="T34" id="Seg_322" s="T13">и</ta>
            <ta e="T15" id="Seg_323" s="T14">каждый</ta>
            <ta e="T35" id="Seg_324" s="T15">день.[NOM]</ta>
            <ta e="T17" id="Seg_325" s="T16">аэродром-EP-ILL</ta>
            <ta e="T18" id="Seg_326" s="T17">ходить-PST-1SG.S</ta>
            <ta e="T19" id="Seg_327" s="T18">а</ta>
            <ta e="T20" id="Seg_328" s="T19">он(а).[NOM]</ta>
            <ta e="T21" id="Seg_329" s="T20">всё.ещё</ta>
            <ta e="T22" id="Seg_330" s="T21">NEG</ta>
            <ta e="T23" id="Seg_331" s="T22">прийти-IPFV3.[3SG.S]</ta>
            <ta e="T24" id="Seg_332" s="T23">назад</ta>
            <ta e="T25" id="Seg_333" s="T24">и</ta>
            <ta e="T36" id="Seg_334" s="T25">куда-ADV.LOC</ta>
            <ta e="T26" id="Seg_335" s="T36">быть-3SG.S</ta>
            <ta e="T28" id="Seg_336" s="T27">быть-3SG.S</ta>
            <ta e="T29" id="Seg_337" s="T28">NEG</ta>
            <ta e="T30" id="Seg_338" s="T29">думать-CO-1SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_339" s="T0">pers</ta>
            <ta e="T2" id="Seg_340" s="T1">n-n:poss</ta>
            <ta e="T3" id="Seg_341" s="T2">v-n:ins-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T4" id="Seg_342" s="T3">nprop-n:case</ta>
            <ta e="T5" id="Seg_343" s="T4">conj</ta>
            <ta e="T6" id="Seg_344" s="T5">adv-adj&gt;adv</ta>
            <ta e="T7" id="Seg_345" s="T6">adv</ta>
            <ta e="T8" id="Seg_346" s="T7">adv</ta>
            <ta e="T9" id="Seg_347" s="T8">ptcl</ta>
            <ta e="T33" id="Seg_348" s="T9">v-v&gt;v-v:pn</ta>
            <ta e="T11" id="Seg_349" s="T10">pers</ta>
            <ta e="T12" id="Seg_350" s="T11">pers-n:case</ta>
            <ta e="T32" id="Seg_351" s="T12">v-v:pn</ta>
            <ta e="T13" id="Seg_352" s="T32">v-v:pn</ta>
            <ta e="T34" id="Seg_353" s="T13">conj</ta>
            <ta e="T15" id="Seg_354" s="T14">adj</ta>
            <ta e="T35" id="Seg_355" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_356" s="T16">n-n:ins-n:case</ta>
            <ta e="T18" id="Seg_357" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_358" s="T18">conj</ta>
            <ta e="T20" id="Seg_359" s="T19">pers-n:case</ta>
            <ta e="T21" id="Seg_360" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_361" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_362" s="T22">v-v&gt;v-v:pn</ta>
            <ta e="T24" id="Seg_363" s="T23">adv</ta>
            <ta e="T25" id="Seg_364" s="T24">conj</ta>
            <ta e="T36" id="Seg_365" s="T25">interrog-adv:case</ta>
            <ta e="T26" id="Seg_366" s="T36">v-v:pn</ta>
            <ta e="T28" id="Seg_367" s="T27">v-v:pn</ta>
            <ta e="T29" id="Seg_368" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_369" s="T29">v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_370" s="T0">pers</ta>
            <ta e="T2" id="Seg_371" s="T1">n</ta>
            <ta e="T3" id="Seg_372" s="T2">v</ta>
            <ta e="T4" id="Seg_373" s="T3">nprop</ta>
            <ta e="T5" id="Seg_374" s="T4">conj</ta>
            <ta e="T6" id="Seg_375" s="T5">adv</ta>
            <ta e="T7" id="Seg_376" s="T6">adj</ta>
            <ta e="T8" id="Seg_377" s="T7">adv</ta>
            <ta e="T9" id="Seg_378" s="T8">ptcl</ta>
            <ta e="T33" id="Seg_379" s="T9">v</ta>
            <ta e="T11" id="Seg_380" s="T10">pers</ta>
            <ta e="T12" id="Seg_381" s="T11">pers</ta>
            <ta e="T32" id="Seg_382" s="T12">v</ta>
            <ta e="T13" id="Seg_383" s="T32">v</ta>
            <ta e="T34" id="Seg_384" s="T13">conj</ta>
            <ta e="T15" id="Seg_385" s="T14">adj</ta>
            <ta e="T35" id="Seg_386" s="T15">n</ta>
            <ta e="T17" id="Seg_387" s="T16">n</ta>
            <ta e="T18" id="Seg_388" s="T17">v</ta>
            <ta e="T19" id="Seg_389" s="T18">conj</ta>
            <ta e="T20" id="Seg_390" s="T19">pers</ta>
            <ta e="T21" id="Seg_391" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_392" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_393" s="T22">v</ta>
            <ta e="T24" id="Seg_394" s="T23">adv</ta>
            <ta e="T25" id="Seg_395" s="T24">conj</ta>
            <ta e="T36" id="Seg_396" s="T25">interrog</ta>
            <ta e="T26" id="Seg_397" s="T36">v</ta>
            <ta e="T28" id="Seg_398" s="T27">v</ta>
            <ta e="T29" id="Seg_399" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_400" s="T29">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_401" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_402" s="T2">v:pred</ta>
            <ta e="T33" id="Seg_403" s="T9">0.3.h:S v:pred</ta>
            <ta e="T11" id="Seg_404" s="T10">pro.h:S</ta>
            <ta e="T12" id="Seg_405" s="T11">pro.h:O</ta>
            <ta e="T32" id="Seg_406" s="T12">v:pred</ta>
            <ta e="T13" id="Seg_407" s="T32">v:pred</ta>
            <ta e="T18" id="Seg_408" s="T17">0.1.h:S v:pred</ta>
            <ta e="T20" id="Seg_409" s="T19">pro.h:S</ta>
            <ta e="T23" id="Seg_410" s="T22">v:pred</ta>
            <ta e="T26" id="Seg_411" s="T36">0.3.h:S v:pred</ta>
            <ta e="T28" id="Seg_412" s="T27">v:pred</ta>
            <ta e="T30" id="Seg_413" s="T29">0.1.h:S 0.3:O v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_414" s="T0">pro.h:Poss</ta>
            <ta e="T2" id="Seg_415" s="T1">np.h:A</ta>
            <ta e="T4" id="Seg_416" s="T3">np:L</ta>
            <ta e="T8" id="Seg_417" s="T7">adv:G</ta>
            <ta e="T33" id="Seg_418" s="T9">0.3.h:A</ta>
            <ta e="T11" id="Seg_419" s="T10">pro.h:A</ta>
            <ta e="T12" id="Seg_420" s="T11">pro.h:Th</ta>
            <ta e="T35" id="Seg_421" s="T15">np:Time</ta>
            <ta e="T17" id="Seg_422" s="T16">np:G</ta>
            <ta e="T18" id="Seg_423" s="T17">0.1.h:A</ta>
            <ta e="T20" id="Seg_424" s="T19">pro.h:A</ta>
            <ta e="T24" id="Seg_425" s="T23">adv:G</ta>
            <ta e="T36" id="Seg_426" s="T25">adv:L</ta>
            <ta e="T30" id="Seg_427" s="T29">0.1.h:E 0.3:Th</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_428" s="T4">RUS:gram</ta>
            <ta e="T34" id="Seg_429" s="T13">RUS:gram</ta>
            <ta e="T15" id="Seg_430" s="T14">RUS:core</ta>
            <ta e="T17" id="Seg_431" s="T16">RUS:cult</ta>
            <ta e="T19" id="Seg_432" s="T18">RUS:gram</ta>
            <ta e="T21" id="Seg_433" s="T20">RUS:core</ta>
            <ta e="T25" id="Seg_434" s="T24">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T10" id="Seg_435" s="T0">Моя дочь учится в Томске, и так долго назад не приезжает.</ta>
            <ta e="T24" id="Seg_436" s="T10">Я её жду-жду, и каждый день на аэродром ходила, а она всё не едет обратно.</ta>
            <ta e="T30" id="Seg_437" s="T24">И где она есть — не знаю.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T10" id="Seg_438" s="T0">My daughter goes to school in town, and so she doesn’t come back for long periods of time.</ta>
            <ta e="T24" id="Seg_439" s="T10">I waited and waited for her, I went to the airport every day, but she hasn’t returned yet.</ta>
            <ta e="T30" id="Seg_440" s="T24">Where she is – I don’t know.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T10" id="Seg_441" s="T0">Meine Tochter geht in der Stadt zur Schule, und deswegen kommt sie über langen Zeitabschnitten nicht nach Hause.</ta>
            <ta e="T24" id="Seg_442" s="T10">Ich wartete und wartete auf sie, ich ging jeden Tag zum Flughafen, aber sie ist noch nicht zurückgekommen.</ta>
            <ta e="T30" id="Seg_443" s="T24">Wo sie bleibt – ich weiß es nicht.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T10" id="Seg_444" s="T0">Моя дочь учится в городе, и так долго назад не приезжает.</ta>
            <ta e="T24" id="Seg_445" s="T10">Я её жду-жду, и каждый день на аэродром ходила, а она всё не едет обратно.</ta>
            <ta e="T30" id="Seg_446" s="T24">И где она есть — не знаю.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T10" id="Seg_447" s="T0">[AAV:] aː dönǯal ?</ta>
            <ta e="T24" id="Seg_448" s="T10">[AAV:] kaʒna tʼel wəl ?; aː dönǯak ? (cf. eːk [be-3SG.S] in sent. 003).</ta>
            <ta e="T30" id="Seg_449" s="T24">[AAV:] a- jon eːk ?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T33" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T32" />
            <conversion-tli id="T13" />
            <conversion-tli id="T34" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T35" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T36" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
