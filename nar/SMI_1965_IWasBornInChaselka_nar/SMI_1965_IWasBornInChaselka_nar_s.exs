<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>SMI_1965_IWasBornInChaselka_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">SMI_1965_IWasBornInChaselka_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">236</ud-information>
            <ud-information attribute-name="# HIAT:w">179</ud-information>
            <ud-information attribute-name="# e">179</ud-information>
            <ud-information attribute-name="# HIAT:u">40</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SMI">
            <abbreviation>SMI</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="SPK0">
            <abbreviation>SMI</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SMI"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T179" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Mat</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">čʼeːlaksa</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">Čʼosalkɨqɨt</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Nɨmtä</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">ilesak</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">meːltɨ</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_26" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">Nɨmtɨ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">qäːlɨsʼsʼak</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_35" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_37" n="HIAT:w" s="T8">Aj</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_40" n="HIAT:w" s="T9">surɨsʼkɔːmɨn</ts>
                  <nts id="Seg_41" n="HIAT:ip">,</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_44" n="HIAT:w" s="T10">korakkɔːmɨn</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_47" n="HIAT:w" s="T11">suːrɨlʼlʼä</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_51" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_53" n="HIAT:w" s="T12">Korakkɔːmɨn</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_56" n="HIAT:w" s="T13">täpälʼlʼä</ts>
                  <nts id="Seg_57" n="HIAT:ip">.</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_60" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_62" n="HIAT:w" s="T14">Täpak</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_65" n="HIAT:w" s="T15">qɨtkɔːmɨn</ts>
                  <nts id="Seg_66" n="HIAT:ip">,</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_69" n="HIAT:w" s="T16">loqar</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_72" n="HIAT:w" s="T17">qätkɔːmen</ts>
                  <nts id="Seg_73" n="HIAT:ip">,</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_76" n="HIAT:w" s="T18">mačʼin</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_79" n="HIAT:w" s="T19">ɔːtap</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_82" n="HIAT:w" s="T20">qätkɔːmen</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_86" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_88" n="HIAT:w" s="T21">Mačʼoːqɨt</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_91" n="HIAT:w" s="T22">ɔːta</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_94" n="HIAT:w" s="T23">kočʼ</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_97" n="HIAT:w" s="T24">ɛːja</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_101" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_103" n="HIAT:w" s="T25">Nʼaroːqɨt</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_106" n="HIAT:w" s="T26">ɔːtä</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_109" n="HIAT:w" s="T27">nɨmt</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_112" n="HIAT:w" s="T28">ɛːjan</ts>
                  <nts id="Seg_113" n="HIAT:ip">,</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_116" n="HIAT:w" s="T29">mačʼit</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_119" n="HIAT:w" s="T30">ɔːtä</ts>
                  <nts id="Seg_120" n="HIAT:ip">.</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_123" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_125" n="HIAT:w" s="T31">Nʼuːtam</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_128" n="HIAT:w" s="T32">pačʼälkɔːmɨn</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_131" n="HIAT:w" s="T33">taŋɨt</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_134" n="HIAT:w" s="T34">sɨːrɨ</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_137" n="HIAT:w" s="T35">čʼɔːtə</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_141" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_143" n="HIAT:w" s="T36">Sɨːrɨmɨt</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_146" n="HIAT:w" s="T37">kočʼ</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_149" n="HIAT:w" s="T38">ɛːja</ts>
                  <nts id="Seg_150" n="HIAT:ip">.</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_153" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_155" n="HIAT:w" s="T39">Čʼuntɨmɨn</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_158" n="HIAT:w" s="T40">aj</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_161" n="HIAT:w" s="T41">eŋa</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_164" n="HIAT:w" s="T42">kočʼek</ts>
                  <nts id="Seg_165" n="HIAT:ip">.</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_168" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_170" n="HIAT:w" s="T43">Nʼuːtop</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_173" n="HIAT:w" s="T44">pačʼälsɔːn</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_176" n="HIAT:w" s="T45">tɛːttɨ</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_179" n="HIAT:w" s="T46">ton</ts>
                  <nts id="Seg_180" n="HIAT:ip">.</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_183" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_185" n="HIAT:w" s="T47">Nʼuːtə</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_188" n="HIAT:w" s="T48">qɔːn</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_191" n="HIAT:w" s="T49">ɛːja</ts>
                  <nts id="Seg_192" n="HIAT:ip">,</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_195" n="HIAT:w" s="T50">kət</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_198" n="HIAT:w" s="T51">aša</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_201" n="HIAT:w" s="T52">tuːrantɨkkɨtɨ</ts>
                  <nts id="Seg_202" n="HIAT:ip">.</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_205" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_207" n="HIAT:w" s="T53">Meː</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_210" n="HIAT:w" s="T54">poːnʼorsɨman</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_213" n="HIAT:w" s="T55">rɨpkop</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_216" n="HIAT:w" s="T56">čʼɔːtɨ</ts>
                  <nts id="Seg_217" n="HIAT:ip">.</nts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_220" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_222" n="HIAT:w" s="T57">Nassari</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_225" n="HIAT:w" s="T58">kupametra</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_228" n="HIAT:w" s="T59">pačʼalsɨmɨn</ts>
                  <nts id="Seg_229" n="HIAT:ip">.</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_232" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_234" n="HIAT:w" s="T60">Našak</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_237" n="HIAT:w" s="T61">qɔːn</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_240" n="HIAT:w" s="T62">ɛːja</ts>
                  <nts id="Seg_241" n="HIAT:ip">,</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_244" n="HIAT:w" s="T63">kətə</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_247" n="HIAT:w" s="T64">čʼump</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_250" n="HIAT:w" s="T65">ɛːja</ts>
                  <nts id="Seg_251" n="HIAT:ip">.</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_254" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_256" n="HIAT:w" s="T66">Aša</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_259" n="HIAT:w" s="T67">tuːrantɨkkɨtɨ</ts>
                  <nts id="Seg_260" n="HIAT:ip">.</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_263" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_265" n="HIAT:w" s="T68">Qəllamat</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_268" n="HIAT:w" s="T69">mɨnɨlʼ</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_271" n="HIAT:w" s="T70">tətantɨ</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_274" n="HIAT:w" s="T71">poːnʼarla</ts>
                  <nts id="Seg_275" n="HIAT:ip">.</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_278" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_280" n="HIAT:w" s="T72">Tɨmtä</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_283" n="HIAT:w" s="T73">poːtə</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_286" n="HIAT:w" s="T74">čʼäːnka</ts>
                  <nts id="Seg_287" n="HIAT:ip">.</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_290" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_292" n="HIAT:w" s="T75">Me</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_295" n="HIAT:w" s="T76">qəntɔːmɨn</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_298" n="HIAT:w" s="T77">qəːlalʼlʼä</ts>
                  <nts id="Seg_299" n="HIAT:ip">.</nts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_302" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_304" n="HIAT:w" s="T78">Poqomɨn</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_307" n="HIAT:w" s="T79">iːtɔːmɨn</ts>
                  <nts id="Seg_308" n="HIAT:ip">.</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_311" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_313" n="HIAT:w" s="T80">Poqqə</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_316" n="HIAT:w" s="T81">sompɨlʼa</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_319" n="HIAT:w" s="T82">köt</ts>
                  <nts id="Seg_320" n="HIAT:ip">.</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_323" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_325" n="HIAT:w" s="T83">Nɨːnä</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_328" n="HIAT:w" s="T84">kərtantɔːman</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_331" n="HIAT:w" s="T85">šöttə</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_334" n="HIAT:w" s="T86">qəːlalʼlʼä</ts>
                  <nts id="Seg_335" n="HIAT:ip">.</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_338" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_340" n="HIAT:w" s="T87">Alakomɨn</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_343" n="HIAT:w" s="T88">tɛltantɔːmɨn</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_346" n="HIAT:w" s="T89">ompa</ts>
                  <nts id="Seg_347" n="HIAT:ip">,</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_350" n="HIAT:w" s="T90">tat</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_353" n="HIAT:w" s="T91">mačʼa</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_356" n="HIAT:w" s="T92">ɨkə</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_359" n="HIAT:w" s="T93">qɨnasʼik</ts>
                  <nts id="Seg_360" n="HIAT:ip">.</nts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_363" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_365" n="HIAT:w" s="T94">Iːjalʼ</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_368" n="HIAT:w" s="T95">käralʼ</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_371" n="HIAT:w" s="T96">tɛlʼtät</ts>
                  <nts id="Seg_372" n="HIAT:ip">.</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_375" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_377" n="HIAT:w" s="T97">Nɛnaqa</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_380" n="HIAT:w" s="T98">namɨnmantə</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_383" n="HIAT:w" s="T99">kočʼ</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_386" n="HIAT:w" s="T100">ɛːja</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_389" n="HIAT:w" s="T101">mačʼoːqɨt</ts>
                  <nts id="Seg_390" n="HIAT:ip">.</nts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_393" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_395" n="HIAT:w" s="T102">Iːjap</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_398" n="HIAT:w" s="T103">nɛnaqa</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_401" n="HIAT:w" s="T104">koptəkɔːlɨk</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_404" n="HIAT:w" s="T105">amnɨtɨ</ts>
                  <nts id="Seg_405" n="HIAT:ip">.</nts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_408" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_410" n="HIAT:w" s="T106">Mej</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_413" n="HIAT:w" s="T107">tiː</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_416" n="HIAT:w" s="T108">qäntak</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_419" n="HIAT:w" s="T109">sɨːrap</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_422" n="HIAT:w" s="T110">apstɨqa</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_425" n="HIAT:w" s="T111">tajeknɛnta</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_428" n="HIAT:w" s="T112">ɛj</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_431" n="HIAT:w" s="T113">tüttä</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_434" n="HIAT:w" s="T114">qataltɛntap</ts>
                  <nts id="Seg_435" n="HIAT:ip">.</nts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_438" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_440" n="HIAT:w" s="T115">Nɨːnäj</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_443" n="HIAT:w" s="T116">nʼuːcʼe</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_446" n="HIAT:w" s="T117">mintap</ts>
                  <nts id="Seg_447" n="HIAT:ip">.</nts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_450" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_452" n="HIAT:w" s="T118">Nɨːnäj</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_455" n="HIAT:w" s="T119">ɛj</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_458" n="HIAT:w" s="T120">molokomɨ</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_461" n="HIAT:w" s="T121">qɨnɛntap</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_464" n="HIAT:w" s="T122">stalovantɨ</ts>
                  <nts id="Seg_465" n="HIAT:ip">.</nts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_468" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_470" n="HIAT:w" s="T123">Čʼunta</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_473" n="HIAT:w" s="T124">sʼarältɨ</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_476" n="HIAT:w" s="T125">qɨntoqa</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_479" n="HIAT:w" s="T126">moloko</ts>
                  <nts id="Seg_480" n="HIAT:ip">.</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_483" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_485" n="HIAT:w" s="T127">Ɛj</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_488" n="HIAT:w" s="T128">nʼuːtɨ</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_491" n="HIAT:w" s="T129">taːtatə</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_494" n="HIAT:w" s="T130">sɨːrɨm</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_497" n="HIAT:w" s="T131">apstɨqɨ</ts>
                  <nts id="Seg_498" n="HIAT:ip">,</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_501" n="HIAT:w" s="T132">nʼuːtə</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_504" n="HIAT:w" s="T133">čʼäːnka</ts>
                  <nts id="Seg_505" n="HIAT:ip">.</nts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_508" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_510" n="HIAT:w" s="T134">Kunä</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_513" n="HIAT:w" s="T135">taːtanta</ts>
                  <nts id="Seg_514" n="HIAT:ip">?</nts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_517" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_519" n="HIAT:w" s="T136">Tuːnna</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_522" n="HIAT:w" s="T137">nʼuːtə</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_525" n="HIAT:w" s="T138">akarotqɨt</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_528" n="HIAT:w" s="T139">näj</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_531" n="HIAT:w" s="T140">ɛːntɨ</ts>
                  <nts id="Seg_532" n="HIAT:ip">.</nts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_535" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_537" n="HIAT:w" s="T141">Nɨnɨ</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_540" n="HIAT:w" s="T142">taːtatə</ts>
                  <nts id="Seg_541" n="HIAT:ip">,</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_544" n="HIAT:w" s="T143">sɨːrɨt</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_547" n="HIAT:w" s="T144">amnat</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_550" n="HIAT:w" s="T145">nɨŋɔːtat</ts>
                  <nts id="Seg_551" n="HIAT:ip">,</nts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_554" n="HIAT:w" s="T146">nʼimatɨt</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_557" n="HIAT:w" s="T147">čʼäːnkɛnta</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_560" n="HIAT:w" s="T148">üːtat</ts>
                  <nts id="Seg_561" n="HIAT:ip">.</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_564" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_566" n="HIAT:w" s="T149">Ɛj</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_569" n="HIAT:w" s="T150">ütäp</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_572" n="HIAT:w" s="T151">nata</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_575" n="HIAT:w" s="T152">taːtaqa</ts>
                  <nts id="Seg_576" n="HIAT:ip">.</nts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_579" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_581" n="HIAT:w" s="T153">Üːtat</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_584" n="HIAT:w" s="T154">sɨːratap</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_587" n="HIAT:w" s="T155">ütaltɛːpsɔːtɨj</ts>
                  <nts id="Seg_588" n="HIAT:ip">,</nts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_591" n="HIAT:w" s="T156">čʼäːnka</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_594" n="HIAT:w" s="T157">üt</ts>
                  <nts id="Seg_595" n="HIAT:ip">.</nts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_598" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_600" n="HIAT:w" s="T158">Mat</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_603" n="HIAT:w" s="T159">tiː</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_606" n="HIAT:w" s="T160">qəntak</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_609" n="HIAT:w" s="T161">topɨrɨlʼlʼä</ts>
                  <nts id="Seg_610" n="HIAT:ip">,</nts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_613" n="HIAT:w" s="T162">a</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_616" n="HIAT:w" s="T163">topɨr</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_619" n="HIAT:w" s="T164">mačʼoːqɨt</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_622" n="HIAT:w" s="T165">namɨnmɔːnt</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_625" n="HIAT:w" s="T166">ɛːja</ts>
                  <nts id="Seg_626" n="HIAT:ip">.</nts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_629" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_631" n="HIAT:w" s="T167">Qailʼ</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_634" n="HIAT:w" s="T168">topɨr</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_637" n="HIAT:w" s="T169">ɛjsa</ts>
                  <nts id="Seg_638" n="HIAT:ip">?</nts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_641" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_643" n="HIAT:w" s="T170">Mačʼoːqɨt</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_646" n="HIAT:w" s="T171">nʼär</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_649" n="HIAT:w" s="T172">topɨr</ts>
                  <nts id="Seg_650" n="HIAT:ip">,</nts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_653" n="HIAT:w" s="T173">kərɨntä</ts>
                  <nts id="Seg_654" n="HIAT:ip">,</nts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_657" n="HIAT:w" s="T174">kotɨlʼ</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_660" n="HIAT:w" s="T175">topɨr</ts>
                  <nts id="Seg_661" n="HIAT:ip">,</nts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_664" n="HIAT:w" s="T176">palqak</ts>
                  <nts id="Seg_665" n="HIAT:ip">,</nts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_668" n="HIAT:w" s="T177">kəptä</ts>
                  <nts id="Seg_669" n="HIAT:ip">,</nts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_672" n="HIAT:w" s="T178">turraj</ts>
                  <nts id="Seg_673" n="HIAT:ip">.</nts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T179" id="Seg_675" n="sc" s="T0">
               <ts e="T1" id="Seg_677" n="e" s="T0">Mat </ts>
               <ts e="T2" id="Seg_679" n="e" s="T1">čʼeːlaksa </ts>
               <ts e="T3" id="Seg_681" n="e" s="T2">Čʼosalkɨqɨt. </ts>
               <ts e="T4" id="Seg_683" n="e" s="T3">Nɨmtä </ts>
               <ts e="T5" id="Seg_685" n="e" s="T4">ilesak </ts>
               <ts e="T6" id="Seg_687" n="e" s="T5">meːltɨ. </ts>
               <ts e="T7" id="Seg_689" n="e" s="T6">Nɨmtɨ </ts>
               <ts e="T8" id="Seg_691" n="e" s="T7">qäːlɨsʼsʼak. </ts>
               <ts e="T9" id="Seg_693" n="e" s="T8">Aj </ts>
               <ts e="T10" id="Seg_695" n="e" s="T9">surɨsʼkɔːmɨn, </ts>
               <ts e="T11" id="Seg_697" n="e" s="T10">korakkɔːmɨn </ts>
               <ts e="T12" id="Seg_699" n="e" s="T11">suːrɨlʼlʼä. </ts>
               <ts e="T13" id="Seg_701" n="e" s="T12">Korakkɔːmɨn </ts>
               <ts e="T14" id="Seg_703" n="e" s="T13">täpälʼlʼä. </ts>
               <ts e="T15" id="Seg_705" n="e" s="T14">Täpak </ts>
               <ts e="T16" id="Seg_707" n="e" s="T15">qɨtkɔːmɨn, </ts>
               <ts e="T17" id="Seg_709" n="e" s="T16">loqar </ts>
               <ts e="T18" id="Seg_711" n="e" s="T17">qätkɔːmen, </ts>
               <ts e="T19" id="Seg_713" n="e" s="T18">mačʼin </ts>
               <ts e="T20" id="Seg_715" n="e" s="T19">ɔːtap </ts>
               <ts e="T21" id="Seg_717" n="e" s="T20">qätkɔːmen. </ts>
               <ts e="T22" id="Seg_719" n="e" s="T21">Mačʼoːqɨt </ts>
               <ts e="T23" id="Seg_721" n="e" s="T22">ɔːta </ts>
               <ts e="T24" id="Seg_723" n="e" s="T23">kočʼ </ts>
               <ts e="T25" id="Seg_725" n="e" s="T24">ɛːja. </ts>
               <ts e="T26" id="Seg_727" n="e" s="T25">Nʼaroːqɨt </ts>
               <ts e="T27" id="Seg_729" n="e" s="T26">ɔːtä </ts>
               <ts e="T28" id="Seg_731" n="e" s="T27">nɨmt </ts>
               <ts e="T29" id="Seg_733" n="e" s="T28">ɛːjan, </ts>
               <ts e="T30" id="Seg_735" n="e" s="T29">mačʼit </ts>
               <ts e="T31" id="Seg_737" n="e" s="T30">ɔːtä. </ts>
               <ts e="T32" id="Seg_739" n="e" s="T31">Nʼuːtam </ts>
               <ts e="T33" id="Seg_741" n="e" s="T32">pačʼälkɔːmɨn </ts>
               <ts e="T34" id="Seg_743" n="e" s="T33">taŋɨt </ts>
               <ts e="T35" id="Seg_745" n="e" s="T34">sɨːrɨ </ts>
               <ts e="T36" id="Seg_747" n="e" s="T35">čʼɔːtə. </ts>
               <ts e="T37" id="Seg_749" n="e" s="T36">Sɨːrɨmɨt </ts>
               <ts e="T38" id="Seg_751" n="e" s="T37">kočʼ </ts>
               <ts e="T39" id="Seg_753" n="e" s="T38">ɛːja. </ts>
               <ts e="T40" id="Seg_755" n="e" s="T39">Čʼuntɨmɨn </ts>
               <ts e="T41" id="Seg_757" n="e" s="T40">aj </ts>
               <ts e="T42" id="Seg_759" n="e" s="T41">eŋa </ts>
               <ts e="T43" id="Seg_761" n="e" s="T42">kočʼek. </ts>
               <ts e="T44" id="Seg_763" n="e" s="T43">Nʼuːtop </ts>
               <ts e="T45" id="Seg_765" n="e" s="T44">pačʼälsɔːn </ts>
               <ts e="T46" id="Seg_767" n="e" s="T45">tɛːttɨ </ts>
               <ts e="T47" id="Seg_769" n="e" s="T46">ton. </ts>
               <ts e="T48" id="Seg_771" n="e" s="T47">Nʼuːtə </ts>
               <ts e="T49" id="Seg_773" n="e" s="T48">qɔːn </ts>
               <ts e="T50" id="Seg_775" n="e" s="T49">ɛːja, </ts>
               <ts e="T51" id="Seg_777" n="e" s="T50">kət </ts>
               <ts e="T52" id="Seg_779" n="e" s="T51">aša </ts>
               <ts e="T53" id="Seg_781" n="e" s="T52">tuːrantɨkkɨtɨ. </ts>
               <ts e="T54" id="Seg_783" n="e" s="T53">Meː </ts>
               <ts e="T55" id="Seg_785" n="e" s="T54">poːnʼorsɨman </ts>
               <ts e="T56" id="Seg_787" n="e" s="T55">rɨpkop </ts>
               <ts e="T57" id="Seg_789" n="e" s="T56">čʼɔːtɨ. </ts>
               <ts e="T58" id="Seg_791" n="e" s="T57">Nassari </ts>
               <ts e="T59" id="Seg_793" n="e" s="T58">kupametra </ts>
               <ts e="T60" id="Seg_795" n="e" s="T59">pačʼalsɨmɨn. </ts>
               <ts e="T61" id="Seg_797" n="e" s="T60">Našak </ts>
               <ts e="T62" id="Seg_799" n="e" s="T61">qɔːn </ts>
               <ts e="T63" id="Seg_801" n="e" s="T62">ɛːja, </ts>
               <ts e="T64" id="Seg_803" n="e" s="T63">kətə </ts>
               <ts e="T65" id="Seg_805" n="e" s="T64">čʼump </ts>
               <ts e="T66" id="Seg_807" n="e" s="T65">ɛːja. </ts>
               <ts e="T67" id="Seg_809" n="e" s="T66">Aša </ts>
               <ts e="T68" id="Seg_811" n="e" s="T67">tuːrantɨkkɨtɨ. </ts>
               <ts e="T69" id="Seg_813" n="e" s="T68">Qəllamat </ts>
               <ts e="T70" id="Seg_815" n="e" s="T69">mɨnɨlʼ </ts>
               <ts e="T71" id="Seg_817" n="e" s="T70">tətantɨ </ts>
               <ts e="T72" id="Seg_819" n="e" s="T71">poːnʼarla. </ts>
               <ts e="T73" id="Seg_821" n="e" s="T72">Tɨmtä </ts>
               <ts e="T74" id="Seg_823" n="e" s="T73">poːtə </ts>
               <ts e="T75" id="Seg_825" n="e" s="T74">čʼäːnka. </ts>
               <ts e="T76" id="Seg_827" n="e" s="T75">Me </ts>
               <ts e="T77" id="Seg_829" n="e" s="T76">qəntɔːmɨn </ts>
               <ts e="T78" id="Seg_831" n="e" s="T77">qəːlalʼlʼä. </ts>
               <ts e="T79" id="Seg_833" n="e" s="T78">Poqomɨn </ts>
               <ts e="T80" id="Seg_835" n="e" s="T79">iːtɔːmɨn. </ts>
               <ts e="T81" id="Seg_837" n="e" s="T80">Poqqə </ts>
               <ts e="T82" id="Seg_839" n="e" s="T81">sompɨlʼa </ts>
               <ts e="T83" id="Seg_841" n="e" s="T82">köt. </ts>
               <ts e="T84" id="Seg_843" n="e" s="T83">Nɨːnä </ts>
               <ts e="T85" id="Seg_845" n="e" s="T84">kərtantɔːman </ts>
               <ts e="T86" id="Seg_847" n="e" s="T85">šöttə </ts>
               <ts e="T87" id="Seg_849" n="e" s="T86">qəːlalʼlʼä. </ts>
               <ts e="T88" id="Seg_851" n="e" s="T87">Alakomɨn </ts>
               <ts e="T89" id="Seg_853" n="e" s="T88">tɛltantɔːmɨn </ts>
               <ts e="T90" id="Seg_855" n="e" s="T89">ompa, </ts>
               <ts e="T91" id="Seg_857" n="e" s="T90">tat </ts>
               <ts e="T92" id="Seg_859" n="e" s="T91">mačʼa </ts>
               <ts e="T93" id="Seg_861" n="e" s="T92">ɨkə </ts>
               <ts e="T94" id="Seg_863" n="e" s="T93">qɨnasʼik. </ts>
               <ts e="T95" id="Seg_865" n="e" s="T94">Iːjalʼ </ts>
               <ts e="T96" id="Seg_867" n="e" s="T95">käralʼ </ts>
               <ts e="T97" id="Seg_869" n="e" s="T96">tɛlʼtät. </ts>
               <ts e="T98" id="Seg_871" n="e" s="T97">Nɛnaqa </ts>
               <ts e="T99" id="Seg_873" n="e" s="T98">namɨnmantə </ts>
               <ts e="T100" id="Seg_875" n="e" s="T99">kočʼ </ts>
               <ts e="T101" id="Seg_877" n="e" s="T100">ɛːja </ts>
               <ts e="T102" id="Seg_879" n="e" s="T101">mačʼoːqɨt. </ts>
               <ts e="T103" id="Seg_881" n="e" s="T102">Iːjap </ts>
               <ts e="T104" id="Seg_883" n="e" s="T103">nɛnaqa </ts>
               <ts e="T105" id="Seg_885" n="e" s="T104">koptəkɔːlɨk </ts>
               <ts e="T106" id="Seg_887" n="e" s="T105">amnɨtɨ. </ts>
               <ts e="T107" id="Seg_889" n="e" s="T106">Mej </ts>
               <ts e="T108" id="Seg_891" n="e" s="T107">tiː </ts>
               <ts e="T109" id="Seg_893" n="e" s="T108">qäntak </ts>
               <ts e="T110" id="Seg_895" n="e" s="T109">sɨːrap </ts>
               <ts e="T111" id="Seg_897" n="e" s="T110">apstɨqa </ts>
               <ts e="T112" id="Seg_899" n="e" s="T111">tajeknɛnta </ts>
               <ts e="T113" id="Seg_901" n="e" s="T112">ɛj </ts>
               <ts e="T114" id="Seg_903" n="e" s="T113">tüttä </ts>
               <ts e="T115" id="Seg_905" n="e" s="T114">qataltɛntap. </ts>
               <ts e="T116" id="Seg_907" n="e" s="T115">Nɨːnäj </ts>
               <ts e="T117" id="Seg_909" n="e" s="T116">nʼuːcʼe </ts>
               <ts e="T118" id="Seg_911" n="e" s="T117">mintap. </ts>
               <ts e="T119" id="Seg_913" n="e" s="T118">Nɨːnäj </ts>
               <ts e="T120" id="Seg_915" n="e" s="T119">ɛj </ts>
               <ts e="T121" id="Seg_917" n="e" s="T120">molokomɨ </ts>
               <ts e="T122" id="Seg_919" n="e" s="T121">qɨnɛntap </ts>
               <ts e="T123" id="Seg_921" n="e" s="T122">stalovantɨ. </ts>
               <ts e="T124" id="Seg_923" n="e" s="T123">Čʼunta </ts>
               <ts e="T125" id="Seg_925" n="e" s="T124">sʼarältɨ </ts>
               <ts e="T126" id="Seg_927" n="e" s="T125">qɨntoqa </ts>
               <ts e="T127" id="Seg_929" n="e" s="T126">moloko. </ts>
               <ts e="T128" id="Seg_931" n="e" s="T127">Ɛj </ts>
               <ts e="T129" id="Seg_933" n="e" s="T128">nʼuːtɨ </ts>
               <ts e="T130" id="Seg_935" n="e" s="T129">taːtatə </ts>
               <ts e="T131" id="Seg_937" n="e" s="T130">sɨːrɨm </ts>
               <ts e="T132" id="Seg_939" n="e" s="T131">apstɨqɨ, </ts>
               <ts e="T133" id="Seg_941" n="e" s="T132">nʼuːtə </ts>
               <ts e="T134" id="Seg_943" n="e" s="T133">čʼäːnka. </ts>
               <ts e="T135" id="Seg_945" n="e" s="T134">Kunä </ts>
               <ts e="T136" id="Seg_947" n="e" s="T135">taːtanta? </ts>
               <ts e="T137" id="Seg_949" n="e" s="T136">Tuːnna </ts>
               <ts e="T138" id="Seg_951" n="e" s="T137">nʼuːtə </ts>
               <ts e="T139" id="Seg_953" n="e" s="T138">akarotqɨt </ts>
               <ts e="T140" id="Seg_955" n="e" s="T139">näj </ts>
               <ts e="T141" id="Seg_957" n="e" s="T140">ɛːntɨ. </ts>
               <ts e="T142" id="Seg_959" n="e" s="T141">Nɨnɨ </ts>
               <ts e="T143" id="Seg_961" n="e" s="T142">taːtatə, </ts>
               <ts e="T144" id="Seg_963" n="e" s="T143">sɨːrɨt </ts>
               <ts e="T145" id="Seg_965" n="e" s="T144">amnat </ts>
               <ts e="T146" id="Seg_967" n="e" s="T145">nɨŋɔːtat, </ts>
               <ts e="T147" id="Seg_969" n="e" s="T146">nʼimatɨt </ts>
               <ts e="T148" id="Seg_971" n="e" s="T147">čʼäːnkɛnta </ts>
               <ts e="T149" id="Seg_973" n="e" s="T148">üːtat. </ts>
               <ts e="T150" id="Seg_975" n="e" s="T149">Ɛj </ts>
               <ts e="T151" id="Seg_977" n="e" s="T150">ütäp </ts>
               <ts e="T152" id="Seg_979" n="e" s="T151">nata </ts>
               <ts e="T153" id="Seg_981" n="e" s="T152">taːtaqa. </ts>
               <ts e="T154" id="Seg_983" n="e" s="T153">Üːtat </ts>
               <ts e="T155" id="Seg_985" n="e" s="T154">sɨːratap </ts>
               <ts e="T156" id="Seg_987" n="e" s="T155">ütaltɛːpsɔːtɨj, </ts>
               <ts e="T157" id="Seg_989" n="e" s="T156">čʼäːnka </ts>
               <ts e="T158" id="Seg_991" n="e" s="T157">üt. </ts>
               <ts e="T159" id="Seg_993" n="e" s="T158">Mat </ts>
               <ts e="T160" id="Seg_995" n="e" s="T159">tiː </ts>
               <ts e="T161" id="Seg_997" n="e" s="T160">qəntak </ts>
               <ts e="T162" id="Seg_999" n="e" s="T161">topɨrɨlʼlʼä, </ts>
               <ts e="T163" id="Seg_1001" n="e" s="T162">a </ts>
               <ts e="T164" id="Seg_1003" n="e" s="T163">topɨr </ts>
               <ts e="T165" id="Seg_1005" n="e" s="T164">mačʼoːqɨt </ts>
               <ts e="T166" id="Seg_1007" n="e" s="T165">namɨnmɔːnt </ts>
               <ts e="T167" id="Seg_1009" n="e" s="T166">ɛːja. </ts>
               <ts e="T168" id="Seg_1011" n="e" s="T167">Qailʼ </ts>
               <ts e="T169" id="Seg_1013" n="e" s="T168">topɨr </ts>
               <ts e="T170" id="Seg_1015" n="e" s="T169">ɛjsa? </ts>
               <ts e="T171" id="Seg_1017" n="e" s="T170">Mačʼoːqɨt </ts>
               <ts e="T172" id="Seg_1019" n="e" s="T171">nʼär </ts>
               <ts e="T173" id="Seg_1021" n="e" s="T172">topɨr, </ts>
               <ts e="T174" id="Seg_1023" n="e" s="T173">kərɨntä, </ts>
               <ts e="T175" id="Seg_1025" n="e" s="T174">kotɨlʼ </ts>
               <ts e="T176" id="Seg_1027" n="e" s="T175">topɨr, </ts>
               <ts e="T177" id="Seg_1029" n="e" s="T176">palqak, </ts>
               <ts e="T178" id="Seg_1031" n="e" s="T177">kəptä, </ts>
               <ts e="T179" id="Seg_1033" n="e" s="T178">turraj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_1034" s="T0">SMI_1965_IWasBornInChaselka_nar.001 (001.001)</ta>
            <ta e="T6" id="Seg_1035" s="T3">SMI_1965_IWasBornInChaselka_nar.002 (001.002)</ta>
            <ta e="T8" id="Seg_1036" s="T6">SMI_1965_IWasBornInChaselka_nar.003 (001.003)</ta>
            <ta e="T12" id="Seg_1037" s="T8">SMI_1965_IWasBornInChaselka_nar.004 (001.004)</ta>
            <ta e="T14" id="Seg_1038" s="T12">SMI_1965_IWasBornInChaselka_nar.005 (001.005)</ta>
            <ta e="T21" id="Seg_1039" s="T14">SMI_1965_IWasBornInChaselka_nar.006 (001.006)</ta>
            <ta e="T25" id="Seg_1040" s="T21">SMI_1965_IWasBornInChaselka_nar.007 (001.007)</ta>
            <ta e="T31" id="Seg_1041" s="T25">SMI_1965_IWasBornInChaselka_nar.008 (001.008)</ta>
            <ta e="T36" id="Seg_1042" s="T31">SMI_1965_IWasBornInChaselka_nar.009 (001.009)</ta>
            <ta e="T39" id="Seg_1043" s="T36">SMI_1965_IWasBornInChaselka_nar.010 (001.010)</ta>
            <ta e="T43" id="Seg_1044" s="T39">SMI_1965_IWasBornInChaselka_nar.011 (001.011)</ta>
            <ta e="T47" id="Seg_1045" s="T43">SMI_1965_IWasBornInChaselka_nar.012 (001.012)</ta>
            <ta e="T53" id="Seg_1046" s="T47">SMI_1965_IWasBornInChaselka_nar.013 (001.013)</ta>
            <ta e="T57" id="Seg_1047" s="T53">SMI_1965_IWasBornInChaselka_nar.014 (001.014)</ta>
            <ta e="T60" id="Seg_1048" s="T57">SMI_1965_IWasBornInChaselka_nar.015 (001.015)</ta>
            <ta e="T66" id="Seg_1049" s="T60">SMI_1965_IWasBornInChaselka_nar.016 (001.016)</ta>
            <ta e="T68" id="Seg_1050" s="T66">SMI_1965_IWasBornInChaselka_nar.017 (001.017)</ta>
            <ta e="T72" id="Seg_1051" s="T68">SMI_1965_IWasBornInChaselka_nar.018 (001.018)</ta>
            <ta e="T75" id="Seg_1052" s="T72">SMI_1965_IWasBornInChaselka_nar.019 (001.019)</ta>
            <ta e="T78" id="Seg_1053" s="T75">SMI_1965_IWasBornInChaselka_nar.020 (001.020)</ta>
            <ta e="T80" id="Seg_1054" s="T78">SMI_1965_IWasBornInChaselka_nar.021 (001.021)</ta>
            <ta e="T83" id="Seg_1055" s="T80">SMI_1965_IWasBornInChaselka_nar.022 (001.022)</ta>
            <ta e="T87" id="Seg_1056" s="T83">SMI_1965_IWasBornInChaselka_nar.023 (001.023)</ta>
            <ta e="T94" id="Seg_1057" s="T87">SMI_1965_IWasBornInChaselka_nar.024 (001.024)</ta>
            <ta e="T97" id="Seg_1058" s="T94">SMI_1965_IWasBornInChaselka_nar.025 (001.025)</ta>
            <ta e="T102" id="Seg_1059" s="T97">SMI_1965_IWasBornInChaselka_nar.026 (001.026)</ta>
            <ta e="T106" id="Seg_1060" s="T102">SMI_1965_IWasBornInChaselka_nar.027 (001.027)</ta>
            <ta e="T115" id="Seg_1061" s="T106">SMI_1965_IWasBornInChaselka_nar.028 (001.028)</ta>
            <ta e="T118" id="Seg_1062" s="T115">SMI_1965_IWasBornInChaselka_nar.029 (001.029)</ta>
            <ta e="T123" id="Seg_1063" s="T118">SMI_1965_IWasBornInChaselka_nar.030 (001.030)</ta>
            <ta e="T127" id="Seg_1064" s="T123">SMI_1965_IWasBornInChaselka_nar.031 (001.031)</ta>
            <ta e="T134" id="Seg_1065" s="T127">SMI_1965_IWasBornInChaselka_nar.032 (001.032)</ta>
            <ta e="T136" id="Seg_1066" s="T134">SMI_1965_IWasBornInChaselka_nar.033 (001.033)</ta>
            <ta e="T141" id="Seg_1067" s="T136">SMI_1965_IWasBornInChaselka_nar.034 (001.034)</ta>
            <ta e="T149" id="Seg_1068" s="T141">SMI_1965_IWasBornInChaselka_nar.035 (001.035)</ta>
            <ta e="T153" id="Seg_1069" s="T149">SMI_1965_IWasBornInChaselka_nar.036 (001.036)</ta>
            <ta e="T158" id="Seg_1070" s="T153">SMI_1965_IWasBornInChaselka_nar.037 (001.037)</ta>
            <ta e="T167" id="Seg_1071" s="T158">SMI_1965_IWasBornInChaselka_nar.038 (001.038)</ta>
            <ta e="T170" id="Seg_1072" s="T167">SMI_1965_IWasBornInChaselka_nar.039 (001.039)</ta>
            <ta e="T179" id="Seg_1073" s="T170">SMI_1965_IWasBornInChaselka_nar.040 (001.040)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_1074" s="T0">Мат ′тʼе̄lакса ′чо̄саlkыkыт.</ta>
            <ta e="T6" id="Seg_1075" s="T3">′нымтӓ ′иlесак ′меlды.</ta>
            <ta e="T8" id="Seg_1076" s="T6">′нымтӓ ′kӓlысʼак.</ta>
            <ta e="T12" id="Seg_1077" s="T8">ай ′сурысʼкомын, ′kора ′комын ′сӯрылʼе.</ta>
            <ta e="T14" id="Seg_1078" s="T12">′kора′комын ′тӓпӓлʼе.</ta>
            <ta e="T21" id="Seg_1079" s="T14">′тӓпак ‵kыт′комын, ′lоkар kӓткомен, ′мачин ′о̨тап kӓт′коме̨н.</ta>
            <ta e="T25" id="Seg_1080" s="T21">′ма̄чоkыт ′о̨̄та ′ко̄′тʼе̄jа.</ta>
            <ta e="T31" id="Seg_1081" s="T25">′нʼароɣыт ′о̨̄тӓ ным′дӓjан ′мачʼит ′о̨тӓ.</ta>
            <ta e="T36" id="Seg_1082" s="T31">′нʼӯтам ′па̄тʼӓлкомын ′таңыт ′сырычо̄тъ.</ta>
            <ta e="T39" id="Seg_1083" s="T36">′сы̄рымыт ко̄′чʼеjа.</ta>
            <ta e="T43" id="Seg_1084" s="T39">′чунтымын ай е′ңа ′кочʼек.</ta>
            <ta e="T47" id="Seg_1085" s="T43">′нӱ̄топ ′па̄тʼӓлʼ[l]сон ′тӓтты тон.</ta>
            <ta e="T53" id="Seg_1086" s="T47">′нӱтъ kо̄′не̨jа ′къ̊̄тъ ′а̄шʼа ′тӯрандыɣытты.</ta>
            <ta e="T57" id="Seg_1087" s="T53">ме̄ ′по̄нʼорсыман рып′коп′чо̄ты.</ta>
            <ta e="T60" id="Seg_1088" s="T57">′нассари ‵купа′метра ′па̄тʼаlсымын.</ta>
            <ta e="T66" id="Seg_1089" s="T60">′на̄шʼа kо̄̊не̨jа ′къ̊тъ ′чумб̂еjа.</ta>
            <ta e="T68" id="Seg_1090" s="T66">′ашʼа ′тӯрандыɣыты.</ta>
            <ta e="T72" id="Seg_1091" s="T68">′kы̄[ъ̊]лlамат ′мы̄нылʼ ′тӓ̄танты ′по̄н′арла.</ta>
            <ta e="T75" id="Seg_1092" s="T72">′тымтӓ ′по̄тъ ′тʼе̨нка.</ta>
            <ta e="T78" id="Seg_1093" s="T75">ме ′къ̊н‵томын ′къ̊̄lалʼе.</ta>
            <ta e="T80" id="Seg_1094" s="T78">′поkома[ы]н ӣ′томын.</ta>
            <ta e="T83" id="Seg_1095" s="T80">′поkkъ ′сомбылʼа кӧт.</ta>
            <ta e="T87" id="Seg_1096" s="T83">′нынӓ ′къ̊ртанд̂оман. ′шʼӧттъ. ′k[ɣ]ъ̊lалʼлʼе.</ta>
            <ta e="T94" id="Seg_1097" s="T87">′а̄lакомын ‵теlтан′томын ′омпа, та̄т ′ма̄тʼа ′ыкъ ′kы̄насʼе[и]к.</ta>
            <ta e="T97" id="Seg_1098" s="T94">′ӣjал[l] kӓрɣа ′те̨лʼтӓт.</ta>
            <ta e="T102" id="Seg_1099" s="T97">′нӓнаɣа ′намынмантъ ′ко̄чʼ′еjа ма̄чоkыт.</ta>
            <ta e="T106" id="Seg_1100" s="T102">′ӣjап ′нӓнаɣа ′kоптъ‵коlыk ′амныты.</ta>
            <ta e="T115" id="Seg_1101" s="T106">′мей т̇ӣ ′хӓнтак ′сырап ′апстыkа ′таjегнӓнта ′ей ′тӱттӓ ха′таlтӓнтап[м].</ta>
            <ta e="T118" id="Seg_1102" s="T115">′нынӓй ′нӱцʼе минтап.</ta>
            <ta e="T123" id="Seg_1103" s="T118">′нынӓй е̨й моlокомы ′kынӓнтап ста′lованты.</ta>
            <ta e="T127" id="Seg_1104" s="T123">′чунта ′сʼа̄реlты ′хынтоɣа моlо′ко.</ta>
            <ta e="T134" id="Seg_1105" s="T127">ей ′нӱты ′тататъ ′сырым ′апстыɣы, ′нӱтъ ′чʼенка.</ta>
            <ta e="T136" id="Seg_1106" s="T134">′кунӓ ′татанта. </ta>
            <ta e="T141" id="Seg_1107" s="T136">′тунна ′нӱтъ ака′ротkыт ‵нӓ̄′jенты.</ta>
            <ta e="T149" id="Seg_1108" s="T141">ныны ′тататъ ′сы̄рыт ′амнат нын′котат, ни′матыт чʼ[е]нкӓнта ′ӱ̄тат.</ta>
            <ta e="T153" id="Seg_1109" s="T149">′ей ′ӱ̄тӓп ′ната ′татаɣа.</ta>
            <ta e="T158" id="Seg_1110" s="T153">′ӱ̄тат ′сы̄ратап ′ӱтаlтӓп′сотый ′тʼенка ′ӱт.</ta>
            <ta e="T167" id="Seg_1111" s="T158">мат ′ти kъ̊нтак ′топырылʼе а ′топыр ′мачоɣыт ′намынмонт′еjа.</ta>
            <ta e="T170" id="Seg_1112" s="T167">′kаиl ′топыр е̨йса?</ta>
            <ta e="T179" id="Seg_1113" s="T170">′ма̄′чоkыт ′нʼӓр топыр, ′къ̊̄рынтӓ ′котыl ′топыр ′паlkак, ′къ̊[ӧ]птӓ, ′туррай.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_1114" s="T0">Mat tʼeːlʼaksa čʼoːsalʼqɨqɨt.</ta>
            <ta e="T6" id="Seg_1115" s="T3">nɨmtä ilʼesak melʼdɨ.</ta>
            <ta e="T8" id="Seg_1116" s="T6">nɨmtä qälʼɨsʼak.</ta>
            <ta e="T12" id="Seg_1117" s="T8">aj surɨsʼkomɨn, qora komɨn suːrɨlʼe.</ta>
            <ta e="T14" id="Seg_1118" s="T12">qorakomɨn täpälʼe.</ta>
            <ta e="T21" id="Seg_1119" s="T14">täpak qɨtkomɨn, lʼoqar qätkomen, mačʼin otap qätkomen.</ta>
            <ta e="T25" id="Seg_1120" s="T21">maːčʼoqɨt oːta koːtʼeːja.</ta>
            <ta e="T31" id="Seg_1121" s="T25">nʼaroqɨt oːtä nɨmdäjan mačʼʼit otä.</ta>
            <ta e="T36" id="Seg_1122" s="T31">nʼuːtam paːtʼälkomɨn taŋɨt sɨrɨčʼoːtə.</ta>
            <ta e="T39" id="Seg_1123" s="T36">sɨːrɨmɨt koːčʼeja.</ta>
            <ta e="T43" id="Seg_1124" s="T39">čʼuntɨmɨn aj eŋa kočʼek.</ta>
            <ta e="T47" id="Seg_1125" s="T43">nüːtop paːtʼälʼ[lʼ]son tättɨ ton.</ta>
            <ta e="T53" id="Seg_1126" s="T47">nütə qoːneja kəːtə aːšʼa tuːrandɨqɨttɨ.</ta>
            <ta e="T57" id="Seg_1127" s="T53">meː poːnʼorsɨman rɨpkopčʼoːtɨ.</ta>
            <ta e="T60" id="Seg_1128" s="T57">nassari kupametra paːtʼalʼsɨmɨn.</ta>
            <ta e="T66" id="Seg_1129" s="T60">naːšʼa qoːnɛja kətə čʼump̂eja.</ta>
            <ta e="T68" id="Seg_1130" s="T66">ašʼa tuːrandɨqɨtɨ.</ta>
            <ta e="T72" id="Seg_1131" s="T68">qɨː[ə]llʼamat mɨːnɨlʼ täːtantɨ poːnʼarla.</ta>
            <ta e="T75" id="Seg_1132" s="T72">tɨmtä poːtə tʼenka.</ta>
            <ta e="T78" id="Seg_1133" s="T75">me kəntomɨn kəːlʼalʼe.</ta>
            <ta e="T80" id="Seg_1134" s="T78">poqoma[ɨ]n iːtomɨn.</ta>
            <ta e="T83" id="Seg_1135" s="T80">poqqə sompɨlʼa köt.</ta>
            <ta e="T87" id="Seg_1136" s="T83">nɨnä kərtand̂oman. šʼöttə. q[q]əlʼalʼlʼe.</ta>
            <ta e="T94" id="Seg_1137" s="T87">aːlʼakomɨn telʼtantomɨn ompa, taːt maːtʼa ɨkə qɨːnasʼe[i]k.</ta>
            <ta e="T97" id="Seg_1138" s="T94">iːjal[lʼ] qärqa telʼtät.</ta>
            <ta e="T102" id="Seg_1139" s="T97">nänaqa namɨnmantə koːčʼʼeja maːčʼoqɨt.</ta>
            <ta e="T106" id="Seg_1140" s="T102">iːjap nänaqa qoptəkolʼɨq amnɨtɨ.</ta>
            <ta e="T115" id="Seg_1141" s="T106">mej ṫiː häntak sɨrap apstɨqa tajegnänta ej tüttä hatalʼtäntap[m].</ta>
            <ta e="T118" id="Seg_1142" s="T115">nɨnäj nücʼe mintap.</ta>
            <ta e="T123" id="Seg_1143" s="T118">nɨnäj ej molʼokomɨ qɨnäntap stalʼovantɨ.</ta>
            <ta e="T127" id="Seg_1144" s="T123">čʼunta sʼaːrelʼtɨ hɨntoqa molʼoko.</ta>
            <ta e="T134" id="Seg_1145" s="T127">ej nütɨ tatatə sɨrɨm apstɨqɨ, nütə čʼenka.</ta>
            <ta e="T136" id="Seg_1146" s="T134">kunä tatanta. </ta>
            <ta e="T141" id="Seg_1147" s="T136">tunna nütə akarotqɨt näːjentɨ.</ta>
            <ta e="T149" id="Seg_1148" s="T141">nɨnɨ tatatə sɨːrɨt amnat nɨnkotat, nimatɨt čʼʼ[e]nkänta üːtat.</ta>
            <ta e="T153" id="Seg_1149" s="T149">ej üːtäp nata tataqa.</ta>
            <ta e="T158" id="Seg_1150" s="T153">üːtat sɨːratap ütalʼtäpsotɨj tʼenka üt.</ta>
            <ta e="T167" id="Seg_1151" s="T158">mat ti qəntak topɨrɨlʼe a topɨr mačʼoqɨt namɨnmonteja.</ta>
            <ta e="T170" id="Seg_1152" s="T167">qailʼ topɨr ejsa?</ta>
            <ta e="T179" id="Seg_1153" s="T170">maːčʼoqɨt nʼär topɨr, kəːrɨntä kotɨlʼ topɨr palʼqak, kə[ö]ptä, turraj.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_1154" s="T0">Mat čʼeːlaksa Čʼosalkɨqɨt. </ta>
            <ta e="T6" id="Seg_1155" s="T3">Nɨmtä ilesak meːltɨ. </ta>
            <ta e="T8" id="Seg_1156" s="T6">Nɨmtɨ qäːlɨsʼsʼak. </ta>
            <ta e="T12" id="Seg_1157" s="T8">Aj surɨsʼkɔːmɨn, korakkɔːmɨn suːrɨlʼlʼä. </ta>
            <ta e="T14" id="Seg_1158" s="T12">Korakkɔːmɨn täpälʼlʼä. </ta>
            <ta e="T21" id="Seg_1159" s="T14">Täpak qɨtkɔːmɨn, loqar qätkɔːmen, mačʼin ɔːtap qätkɔːmen. </ta>
            <ta e="T25" id="Seg_1160" s="T21">Mačʼoːqɨt ɔːta kočʼ ɛːja. </ta>
            <ta e="T31" id="Seg_1161" s="T25">Nʼaroːqɨt ɔːtä nɨmt ɛːjan, mačʼit ɔːtä. </ta>
            <ta e="T36" id="Seg_1162" s="T31">Nʼuːtam pačʼälkɔːmɨn taŋɨt sɨːrɨ čʼɔːtə. </ta>
            <ta e="T39" id="Seg_1163" s="T36">Sɨːrɨmɨt kočʼ ɛːja. </ta>
            <ta e="T43" id="Seg_1164" s="T39">Čʼuntɨmɨn aj eŋa kočʼek. </ta>
            <ta e="T47" id="Seg_1165" s="T43">Nʼuːtop pačʼälsɔːn tɛːttɨ ton. </ta>
            <ta e="T53" id="Seg_1166" s="T47">Nʼuːtə qɔːn ɛːja, kət aša tuːrantɨkkɨtɨ. </ta>
            <ta e="T57" id="Seg_1167" s="T53">Meː poːnʼorsɨman rɨpkop čʼɔːtɨ. </ta>
            <ta e="T60" id="Seg_1168" s="T57">Nassari kupametra pačʼalsɨmɨn. </ta>
            <ta e="T66" id="Seg_1169" s="T60">Našak qɔːn ɛːja, kətə čʼump ɛːja. </ta>
            <ta e="T68" id="Seg_1170" s="T66">Aša tuːrantɨkkɨtɨ. </ta>
            <ta e="T72" id="Seg_1171" s="T68">Qəllamat mɨnɨlʼ tətantɨ poːnʼarla. </ta>
            <ta e="T75" id="Seg_1172" s="T72">Tɨmtä poːtə čʼäːnka. </ta>
            <ta e="T78" id="Seg_1173" s="T75">Me qəntɔːmɨn qəːlalʼlʼä. </ta>
            <ta e="T80" id="Seg_1174" s="T78">Poqomɨn iːtɔːmɨn. </ta>
            <ta e="T83" id="Seg_1175" s="T80">Poqqə sompɨlʼa köt. </ta>
            <ta e="T87" id="Seg_1176" s="T83">Nɨːnä kərtantɔːman šöttə qəːlalʼlʼä. </ta>
            <ta e="T94" id="Seg_1177" s="T87">Alakomɨn tɛltantɔːmɨn ompa, tat mačʼa ɨkə qɨnasʼik. </ta>
            <ta e="T97" id="Seg_1178" s="T94">Iːjalʼ käralʼ tɛlʼtät. </ta>
            <ta e="T102" id="Seg_1179" s="T97">Nɛnaqa namɨnmantə kočʼ ɛːja mačʼoːqɨt. </ta>
            <ta e="T106" id="Seg_1180" s="T102">Iːjap nɛnaqa koptəkɔːlɨk amnɨtɨ. </ta>
            <ta e="T115" id="Seg_1181" s="T106">Mej tiː qäntak sɨːrap apstɨqa, tajeknɛnta ɛj tüttä qataltɛntap. </ta>
            <ta e="T118" id="Seg_1182" s="T115">Nɨːnäj nʼuːcʼe mintap. </ta>
            <ta e="T123" id="Seg_1183" s="T118">Nɨːnäj ɛj molokomɨ qɨnɛntap stalovantɨ. </ta>
            <ta e="T127" id="Seg_1184" s="T123">Čʼunta sʼarältɨ qɨntoqa moloko. </ta>
            <ta e="T134" id="Seg_1185" s="T127">Ɛj nʼuːtɨ taːtatə sɨːrɨm apstɨqɨ, nʼuːtə čʼäːnka. </ta>
            <ta e="T136" id="Seg_1186" s="T134">Kunä taːtanta? </ta>
            <ta e="T141" id="Seg_1187" s="T136">Tuːnna nʼuːtə akarotqɨt näj ɛːntɨ. </ta>
            <ta e="T149" id="Seg_1188" s="T141">Nɨnɨ taːtatə, sɨːrɨt amnat nɨŋɔːtat, nʼimatɨt čʼäːnkɛnta üːtat. </ta>
            <ta e="T153" id="Seg_1189" s="T149">Ɛj ütäp nata taːtaqa. </ta>
            <ta e="T158" id="Seg_1190" s="T153">Üːtat sɨːratap ütaltɛːpsɔːtɨj, čʼäːnka üt. </ta>
            <ta e="T167" id="Seg_1191" s="T158">Mat tiː qəntak topɨrɨlʼlʼä, a topɨr mačʼoːqɨt namɨnmɔːnt ɛːja. </ta>
            <ta e="T170" id="Seg_1192" s="T167">Qailʼ topɨr ɛjsa? </ta>
            <ta e="T179" id="Seg_1193" s="T170">Mačʼoːqɨt nʼär topɨr, kərɨntä, kotɨlʼ topɨr, palqak, kəptä, turraj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1194" s="T0">mat</ta>
            <ta e="T2" id="Seg_1195" s="T1">čʼeːlak-sa</ta>
            <ta e="T3" id="Seg_1196" s="T2">Čʼosalkɨ-qɨt</ta>
            <ta e="T4" id="Seg_1197" s="T3">nɨmtä</ta>
            <ta e="T5" id="Seg_1198" s="T4">ile-sa-k</ta>
            <ta e="T6" id="Seg_1199" s="T5">meːltɨ</ta>
            <ta e="T7" id="Seg_1200" s="T6">nɨmtɨ</ta>
            <ta e="T8" id="Seg_1201" s="T7">qäːlɨ-sʼ-sʼa-k</ta>
            <ta e="T9" id="Seg_1202" s="T8">aj</ta>
            <ta e="T10" id="Seg_1203" s="T9">surɨ-sʼ-kɔː-mɨn</ta>
            <ta e="T11" id="Seg_1204" s="T10">kora-kkɔː-mɨn</ta>
            <ta e="T12" id="Seg_1205" s="T11">suːrɨ-lʼ-lʼä</ta>
            <ta e="T13" id="Seg_1206" s="T12">kora-kkɔː-mɨn</ta>
            <ta e="T14" id="Seg_1207" s="T13">täpä-lʼ-lʼä</ta>
            <ta e="T15" id="Seg_1208" s="T14">täpak</ta>
            <ta e="T16" id="Seg_1209" s="T15">qɨt-kɔː-mɨn</ta>
            <ta e="T17" id="Seg_1210" s="T16">loqar</ta>
            <ta e="T18" id="Seg_1211" s="T17">qät-kɔː-men</ta>
            <ta e="T19" id="Seg_1212" s="T18">mačʼi-n</ta>
            <ta e="T20" id="Seg_1213" s="T19">ɔːta-p</ta>
            <ta e="T21" id="Seg_1214" s="T20">qät-kɔː-men</ta>
            <ta e="T22" id="Seg_1215" s="T21">mačʼoː-qɨt</ta>
            <ta e="T23" id="Seg_1216" s="T22">ɔːta</ta>
            <ta e="T24" id="Seg_1217" s="T23">kočʼ</ta>
            <ta e="T25" id="Seg_1218" s="T24">ɛː-ja</ta>
            <ta e="T26" id="Seg_1219" s="T25">nʼaroː-qɨt</ta>
            <ta e="T27" id="Seg_1220" s="T26">ɔːtä</ta>
            <ta e="T28" id="Seg_1221" s="T27">nɨmt</ta>
            <ta e="T29" id="Seg_1222" s="T28">ɛː-ja-n</ta>
            <ta e="T30" id="Seg_1223" s="T29">mačʼi-t</ta>
            <ta e="T31" id="Seg_1224" s="T30">ɔːtä</ta>
            <ta e="T32" id="Seg_1225" s="T31">nʼuːta-m</ta>
            <ta e="T33" id="Seg_1226" s="T32">pačʼ-äl-kɔː-mɨn</ta>
            <ta e="T34" id="Seg_1227" s="T33">taŋɨ-t</ta>
            <ta e="T35" id="Seg_1228" s="T34">sɨːrɨ</ta>
            <ta e="T36" id="Seg_1229" s="T35">čʼɔːtə</ta>
            <ta e="T37" id="Seg_1230" s="T36">sɨːrɨ-mɨt</ta>
            <ta e="T38" id="Seg_1231" s="T37">kočʼ</ta>
            <ta e="T39" id="Seg_1232" s="T38">ɛː-ja</ta>
            <ta e="T40" id="Seg_1233" s="T39">čʼuntɨ-mɨn</ta>
            <ta e="T41" id="Seg_1234" s="T40">aj</ta>
            <ta e="T42" id="Seg_1235" s="T41">e-ŋa</ta>
            <ta e="T43" id="Seg_1236" s="T42">kočʼe-k</ta>
            <ta e="T44" id="Seg_1237" s="T43">nʼuːto-p</ta>
            <ta e="T45" id="Seg_1238" s="T44">pačʼ-äl-sɔː-n</ta>
            <ta e="T46" id="Seg_1239" s="T45">tɛːttɨ</ta>
            <ta e="T47" id="Seg_1240" s="T46">ton</ta>
            <ta e="T48" id="Seg_1241" s="T47">nʼuːtə</ta>
            <ta e="T49" id="Seg_1242" s="T48">qɔːn</ta>
            <ta e="T50" id="Seg_1243" s="T49">ɛː-ja</ta>
            <ta e="T51" id="Seg_1244" s="T50">kə-t</ta>
            <ta e="T52" id="Seg_1245" s="T51">aša</ta>
            <ta e="T53" id="Seg_1246" s="T52">tuːra-ntɨ-kkɨ-tɨ</ta>
            <ta e="T54" id="Seg_1247" s="T53">meː</ta>
            <ta e="T55" id="Seg_1248" s="T54">poː-nʼor-sɨ-man</ta>
            <ta e="T56" id="Seg_1249" s="T55">rɨpkop</ta>
            <ta e="T57" id="Seg_1250" s="T56">čʼɔːtɨ</ta>
            <ta e="T58" id="Seg_1251" s="T57">nas-sari</ta>
            <ta e="T59" id="Seg_1252" s="T58">kupametra</ta>
            <ta e="T60" id="Seg_1253" s="T59">pačʼ-al-sɨ-mɨn</ta>
            <ta e="T61" id="Seg_1254" s="T60">našak</ta>
            <ta e="T62" id="Seg_1255" s="T61">qɔːn</ta>
            <ta e="T63" id="Seg_1256" s="T62">ɛː-ja</ta>
            <ta e="T64" id="Seg_1257" s="T63">kə-tə</ta>
            <ta e="T65" id="Seg_1258" s="T64">čʼump</ta>
            <ta e="T66" id="Seg_1259" s="T65">ɛː-ja</ta>
            <ta e="T67" id="Seg_1260" s="T66">aša</ta>
            <ta e="T68" id="Seg_1261" s="T67">tuːra-ntɨ-kkɨ-tɨ</ta>
            <ta e="T69" id="Seg_1262" s="T68">qəl-la-mat</ta>
            <ta e="T70" id="Seg_1263" s="T69">mɨnɨlʼ</ta>
            <ta e="T71" id="Seg_1264" s="T70">təta-ntɨ</ta>
            <ta e="T72" id="Seg_1265" s="T71">poː-nʼar-la</ta>
            <ta e="T73" id="Seg_1266" s="T72">tɨmtä</ta>
            <ta e="T74" id="Seg_1267" s="T73">poː-tə</ta>
            <ta e="T75" id="Seg_1268" s="T74">čʼäːnka</ta>
            <ta e="T76" id="Seg_1269" s="T75">me</ta>
            <ta e="T77" id="Seg_1270" s="T76">qən-tɔː-mɨn</ta>
            <ta e="T78" id="Seg_1271" s="T77">qəːla-lʼ-lʼä</ta>
            <ta e="T79" id="Seg_1272" s="T78">poqo-mɨn</ta>
            <ta e="T80" id="Seg_1273" s="T79">iː-tɔː-mɨn</ta>
            <ta e="T81" id="Seg_1274" s="T80">poqqə</ta>
            <ta e="T82" id="Seg_1275" s="T81">sompɨlʼa</ta>
            <ta e="T83" id="Seg_1276" s="T82">köt</ta>
            <ta e="T84" id="Seg_1277" s="T83">nɨːnä</ta>
            <ta e="T85" id="Seg_1278" s="T84">kər-ta-ntɔː-man</ta>
            <ta e="T86" id="Seg_1279" s="T85">šöt-tə</ta>
            <ta e="T87" id="Seg_1280" s="T86">qəːla-lʼ-lʼä</ta>
            <ta e="T88" id="Seg_1281" s="T87">alako-mɨn</ta>
            <ta e="T89" id="Seg_1282" s="T88">tɛlta-ntɔː-mɨn</ta>
            <ta e="T90" id="Seg_1283" s="T89">ompa</ta>
            <ta e="T91" id="Seg_1284" s="T90">tan</ta>
            <ta e="T92" id="Seg_1285" s="T91">mačʼa</ta>
            <ta e="T93" id="Seg_1286" s="T92">ɨkə</ta>
            <ta e="T94" id="Seg_1287" s="T93">qɨn-asʼik</ta>
            <ta e="T95" id="Seg_1288" s="T94">iːja-lʼ</ta>
            <ta e="T96" id="Seg_1289" s="T95">kära-lʼ</ta>
            <ta e="T97" id="Seg_1290" s="T96">tɛlʼt-ät</ta>
            <ta e="T98" id="Seg_1291" s="T97">nɛnaqa</ta>
            <ta e="T99" id="Seg_1292" s="T98">namɨnmantə</ta>
            <ta e="T100" id="Seg_1293" s="T99">kočʼ</ta>
            <ta e="T101" id="Seg_1294" s="T100">ɛː-ja</ta>
            <ta e="T102" id="Seg_1295" s="T101">mačʼoː-qɨt</ta>
            <ta e="T103" id="Seg_1296" s="T102">iːja-p</ta>
            <ta e="T104" id="Seg_1297" s="T103">nɛnaqa</ta>
            <ta e="T105" id="Seg_1298" s="T104">koptə-kɔːlɨ-k</ta>
            <ta e="T106" id="Seg_1299" s="T105">am-nɨ-tɨ</ta>
            <ta e="T107" id="Seg_1300" s="T106">mej</ta>
            <ta e="T108" id="Seg_1301" s="T107">tiː</ta>
            <ta e="T109" id="Seg_1302" s="T108">qän-ta-k</ta>
            <ta e="T110" id="Seg_1303" s="T109">sɨːra-p</ta>
            <ta e="T111" id="Seg_1304" s="T110">aps-tɨ-qa</ta>
            <ta e="T112" id="Seg_1305" s="T111">tajekn-ɛnta</ta>
            <ta e="T113" id="Seg_1306" s="T112">ɛj</ta>
            <ta e="T114" id="Seg_1307" s="T113">tüt-tä</ta>
            <ta e="T115" id="Seg_1308" s="T114">qatal-tɛnta-p</ta>
            <ta e="T116" id="Seg_1309" s="T115">nɨːnäj</ta>
            <ta e="T117" id="Seg_1310" s="T116">nʼuːcʼe</ta>
            <ta e="T118" id="Seg_1311" s="T117">mi-nta-p</ta>
            <ta e="T119" id="Seg_1312" s="T118">nɨːnäj</ta>
            <ta e="T120" id="Seg_1313" s="T119">ɛj</ta>
            <ta e="T121" id="Seg_1314" s="T120">moloko-mɨ</ta>
            <ta e="T122" id="Seg_1315" s="T121">qɨn-ɛnta-p</ta>
            <ta e="T123" id="Seg_1316" s="T122">stalova-ntɨ</ta>
            <ta e="T124" id="Seg_1317" s="T123">čʼunta</ta>
            <ta e="T125" id="Seg_1318" s="T124">sʼar-äl-tɨ</ta>
            <ta e="T126" id="Seg_1319" s="T125">qɨn-to-qa</ta>
            <ta e="T127" id="Seg_1320" s="T126">moloko</ta>
            <ta e="T128" id="Seg_1321" s="T127">ɛj</ta>
            <ta e="T129" id="Seg_1322" s="T128">nʼuːtɨ</ta>
            <ta e="T130" id="Seg_1323" s="T129">taːt-atə</ta>
            <ta e="T131" id="Seg_1324" s="T130">sɨːrɨ-m</ta>
            <ta e="T132" id="Seg_1325" s="T131">aps-tɨ-qɨ</ta>
            <ta e="T133" id="Seg_1326" s="T132">nʼuːtə</ta>
            <ta e="T134" id="Seg_1327" s="T133">čʼäːnka</ta>
            <ta e="T135" id="Seg_1328" s="T134">kunä</ta>
            <ta e="T136" id="Seg_1329" s="T135">taːt-anta</ta>
            <ta e="T137" id="Seg_1330" s="T136">tuːnna</ta>
            <ta e="T138" id="Seg_1331" s="T137">nʼuːtə</ta>
            <ta e="T139" id="Seg_1332" s="T138">akarot-qɨt</ta>
            <ta e="T140" id="Seg_1333" s="T139">näj</ta>
            <ta e="T141" id="Seg_1334" s="T140">ɛː-ntɨ</ta>
            <ta e="T142" id="Seg_1335" s="T141">nɨnɨ</ta>
            <ta e="T143" id="Seg_1336" s="T142">taːt-atə</ta>
            <ta e="T144" id="Seg_1337" s="T143">sɨːrɨ-t</ta>
            <ta e="T145" id="Seg_1338" s="T144">amna-t</ta>
            <ta e="T146" id="Seg_1339" s="T145">nɨŋɔː-tat</ta>
            <ta e="T147" id="Seg_1340" s="T146">nʼima-tɨt</ta>
            <ta e="T148" id="Seg_1341" s="T147">čʼäːnk-ɛnta</ta>
            <ta e="T149" id="Seg_1342" s="T148">üːta-t</ta>
            <ta e="T150" id="Seg_1343" s="T149">ɛj</ta>
            <ta e="T151" id="Seg_1344" s="T150">üt-ä-p</ta>
            <ta e="T152" id="Seg_1345" s="T151">nata</ta>
            <ta e="T153" id="Seg_1346" s="T152">taːta-qa</ta>
            <ta e="T154" id="Seg_1347" s="T153">üːta-t</ta>
            <ta e="T155" id="Seg_1348" s="T154">sɨːra-t-a-p</ta>
            <ta e="T156" id="Seg_1349" s="T155">üt-alt-ɛː-psɔːtɨ-j</ta>
            <ta e="T157" id="Seg_1350" s="T156">čʼäːnka</ta>
            <ta e="T158" id="Seg_1351" s="T157">üt</ta>
            <ta e="T159" id="Seg_1352" s="T158">mat</ta>
            <ta e="T160" id="Seg_1353" s="T159">tiː</ta>
            <ta e="T161" id="Seg_1354" s="T160">qən-ta-k</ta>
            <ta e="T162" id="Seg_1355" s="T161">topɨr-ɨ-lʼ-lʼä</ta>
            <ta e="T163" id="Seg_1356" s="T162">a</ta>
            <ta e="T164" id="Seg_1357" s="T163">topɨr</ta>
            <ta e="T165" id="Seg_1358" s="T164">mačʼoː-qɨt</ta>
            <ta e="T166" id="Seg_1359" s="T165">namɨnmɔːnt</ta>
            <ta e="T167" id="Seg_1360" s="T166">ɛː-ja</ta>
            <ta e="T168" id="Seg_1361" s="T167">qai-lʼ</ta>
            <ta e="T169" id="Seg_1362" s="T168">topɨr</ta>
            <ta e="T170" id="Seg_1363" s="T169">ɛjsa</ta>
            <ta e="T171" id="Seg_1364" s="T170">mačʼoː-qɨt</ta>
            <ta e="T172" id="Seg_1365" s="T171">nʼär</ta>
            <ta e="T173" id="Seg_1366" s="T172">topɨr</ta>
            <ta e="T174" id="Seg_1367" s="T173">kərɨntä</ta>
            <ta e="T175" id="Seg_1368" s="T174">kotɨlʼ</ta>
            <ta e="T176" id="Seg_1369" s="T175">topɨr</ta>
            <ta e="T177" id="Seg_1370" s="T176">palqak</ta>
            <ta e="T178" id="Seg_1371" s="T177">kəptä</ta>
            <ta e="T179" id="Seg_1372" s="T178">turraj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1373" s="T0">man</ta>
            <ta e="T2" id="Seg_1374" s="T1">čʼeːlɨŋ-sɨ</ta>
            <ta e="T3" id="Seg_1375" s="T2">Čʼosalkɨ-qɨn</ta>
            <ta e="T4" id="Seg_1376" s="T3">nɨmtɨ</ta>
            <ta e="T5" id="Seg_1377" s="T4">ilɨ-sɨ-k</ta>
            <ta e="T6" id="Seg_1378" s="T5">meːltɨ</ta>
            <ta e="T7" id="Seg_1379" s="T6">nɨmtɨ</ta>
            <ta e="T8" id="Seg_1380" s="T7">qəːlɨ-š-sɨ-k</ta>
            <ta e="T9" id="Seg_1381" s="T8">aj</ta>
            <ta e="T10" id="Seg_1382" s="T9">suːrɨm-š-kkɨ-mɨt</ta>
            <ta e="T11" id="Seg_1383" s="T10">kora-kkɨ-mɨt</ta>
            <ta e="T12" id="Seg_1384" s="T11">suːrɨm-š-lä</ta>
            <ta e="T13" id="Seg_1385" s="T12">kora-kkɨ-mɨt</ta>
            <ta e="T14" id="Seg_1386" s="T13">täpäŋ-š-lä</ta>
            <ta e="T15" id="Seg_1387" s="T14">täpäŋ</ta>
            <ta e="T16" id="Seg_1388" s="T15">qət-kkɨ-mɨt</ta>
            <ta e="T17" id="Seg_1389" s="T16">loqa</ta>
            <ta e="T18" id="Seg_1390" s="T17">qət-kkɨ-mɨt</ta>
            <ta e="T19" id="Seg_1391" s="T18">mačʼɨ-n</ta>
            <ta e="T20" id="Seg_1392" s="T19">ɔːtä-m</ta>
            <ta e="T21" id="Seg_1393" s="T20">qət-kkɨ-mɨt</ta>
            <ta e="T22" id="Seg_1394" s="T21">mačʼɨ-qɨn</ta>
            <ta e="T23" id="Seg_1395" s="T22">ɔːtä</ta>
            <ta e="T24" id="Seg_1396" s="T23">kočʼčʼɨ</ta>
            <ta e="T25" id="Seg_1397" s="T24">ɛː-ŋɨ</ta>
            <ta e="T26" id="Seg_1398" s="T25">nʼarɨ-qɨn</ta>
            <ta e="T27" id="Seg_1399" s="T26">ɔːtä</ta>
            <ta e="T28" id="Seg_1400" s="T27">nɨmtɨ</ta>
            <ta e="T29" id="Seg_1401" s="T28">ɛː-ŋɨ-n</ta>
            <ta e="T30" id="Seg_1402" s="T29">mačʼɨ-n</ta>
            <ta e="T31" id="Seg_1403" s="T30">ɔːtä</ta>
            <ta e="T32" id="Seg_1404" s="T31">nʼuːtɨ-m</ta>
            <ta e="T33" id="Seg_1405" s="T32">pačʼčʼɨ-äl-kkɨ-mɨt</ta>
            <ta e="T34" id="Seg_1406" s="T33">taŋɨ-n</ta>
            <ta e="T35" id="Seg_1407" s="T34">sɨːrɨ</ta>
            <ta e="T36" id="Seg_1408" s="T35">čʼɔːtɨ</ta>
            <ta e="T37" id="Seg_1409" s="T36">sɨːrɨ-mɨt</ta>
            <ta e="T38" id="Seg_1410" s="T37">kočʼčʼɨ</ta>
            <ta e="T39" id="Seg_1411" s="T38">ɛː-ŋɨ</ta>
            <ta e="T40" id="Seg_1412" s="T39">čʼuntɨ-mɨt</ta>
            <ta e="T41" id="Seg_1413" s="T40">aj</ta>
            <ta e="T42" id="Seg_1414" s="T41">ɛː-ŋɨ</ta>
            <ta e="T43" id="Seg_1415" s="T42">kočʼčʼɨ-k</ta>
            <ta e="T44" id="Seg_1416" s="T43">nʼuːtɨ-m</ta>
            <ta e="T45" id="Seg_1417" s="T44">pačʼčʼɨ-äl-sɨ-n</ta>
            <ta e="T46" id="Seg_1418" s="T45">tɛːttɨ</ta>
            <ta e="T47" id="Seg_1419" s="T46">ton</ta>
            <ta e="T48" id="Seg_1420" s="T47">nʼuːtɨ</ta>
            <ta e="T49" id="Seg_1421" s="T48">qɔːna</ta>
            <ta e="T50" id="Seg_1422" s="T49">ɛː-ŋɨ</ta>
            <ta e="T51" id="Seg_1423" s="T50">kə-n</ta>
            <ta e="T52" id="Seg_1424" s="T51">ašša</ta>
            <ta e="T53" id="Seg_1425" s="T52">tuːrɨ-ntɨ-kkɨ-tɨ</ta>
            <ta e="T54" id="Seg_1426" s="T53">meː</ta>
            <ta e="T55" id="Seg_1427" s="T54">poː-nʼɨr-sɨ-mɨt</ta>
            <ta e="T56" id="Seg_1428" s="T55">rɨpkop</ta>
            <ta e="T57" id="Seg_1429" s="T56">čʼɔːtɨ</ta>
            <ta e="T58" id="Seg_1430" s="T57">nɔːkɨr-sar</ta>
            <ta e="T59" id="Seg_1431" s="T58">kupametra</ta>
            <ta e="T60" id="Seg_1432" s="T59">pačʼčʼɨ-ätɔːl-sɨ-mɨt</ta>
            <ta e="T61" id="Seg_1433" s="T60">naššak</ta>
            <ta e="T62" id="Seg_1434" s="T61">qɔːna</ta>
            <ta e="T63" id="Seg_1435" s="T62">ɛː-ŋɨ</ta>
            <ta e="T64" id="Seg_1436" s="T63">kə-tɨ</ta>
            <ta e="T65" id="Seg_1437" s="T64">čʼumpɨ</ta>
            <ta e="T66" id="Seg_1438" s="T65">ɛː-ŋɨ</ta>
            <ta e="T67" id="Seg_1439" s="T66">ašša</ta>
            <ta e="T68" id="Seg_1440" s="T67">tuːrɨ-ntɨ-kkɨ-tɨ</ta>
            <ta e="T69" id="Seg_1441" s="T68">qən-lä-mɨt</ta>
            <ta e="T70" id="Seg_1442" s="T69">mənɨlʼ</ta>
            <ta e="T71" id="Seg_1443" s="T70">təttɨ-ntɨ</ta>
            <ta e="T72" id="Seg_1444" s="T71">poː-nʼɨr-lä</ta>
            <ta e="T73" id="Seg_1445" s="T72">tɨmtɨ</ta>
            <ta e="T74" id="Seg_1446" s="T73">poː-tɨ</ta>
            <ta e="T75" id="Seg_1447" s="T74">čʼäːŋkɨ</ta>
            <ta e="T76" id="Seg_1448" s="T75">meː</ta>
            <ta e="T77" id="Seg_1449" s="T76">qən-ɛntɨ-mɨt</ta>
            <ta e="T78" id="Seg_1450" s="T77">qəːlɨ-š-lä</ta>
            <ta e="T79" id="Seg_1451" s="T78">poqqɨ-mɨt</ta>
            <ta e="T80" id="Seg_1452" s="T79">iː-ɛntɨ-mɨt</ta>
            <ta e="T81" id="Seg_1453" s="T80">poqqɨ</ta>
            <ta e="T82" id="Seg_1454" s="T81">sompɨla</ta>
            <ta e="T83" id="Seg_1455" s="T82">köt</ta>
            <ta e="T84" id="Seg_1456" s="T83">nɨːnɨ</ta>
            <ta e="T85" id="Seg_1457" s="T84">kərɨ-tɨ-ɛntɨ-mɨt</ta>
            <ta e="T86" id="Seg_1458" s="T85">šöt-ntɨ</ta>
            <ta e="T87" id="Seg_1459" s="T86">qəːlɨ-š-lä</ta>
            <ta e="T88" id="Seg_1460" s="T87">alako-mɨt</ta>
            <ta e="T89" id="Seg_1461" s="T88">tɛltɨ-ɛntɨ-mɨt</ta>
            <ta e="T90" id="Seg_1462" s="T89">ompä</ta>
            <ta e="T91" id="Seg_1463" s="T90">tan</ta>
            <ta e="T92" id="Seg_1464" s="T91">mačʼä</ta>
            <ta e="T93" id="Seg_1465" s="T92">ɨkɨ</ta>
            <ta e="T94" id="Seg_1466" s="T93">qən-äšɨk</ta>
            <ta e="T95" id="Seg_1467" s="T94">iːja-lʼ</ta>
            <ta e="T96" id="Seg_1468" s="T95">kärɨ-lʼ</ta>
            <ta e="T97" id="Seg_1469" s="T96">tɛltɨ-ätɨ</ta>
            <ta e="T98" id="Seg_1470" s="T97">nɛnɨqa</ta>
            <ta e="T99" id="Seg_1471" s="T98">namantɨ</ta>
            <ta e="T100" id="Seg_1472" s="T99">kočʼčʼɨ</ta>
            <ta e="T101" id="Seg_1473" s="T100">ɛː-ŋɨ</ta>
            <ta e="T102" id="Seg_1474" s="T101">mačʼɨ-qɨn</ta>
            <ta e="T103" id="Seg_1475" s="T102">iːja-m</ta>
            <ta e="T104" id="Seg_1476" s="T103">nɛnɨqa</ta>
            <ta e="T105" id="Seg_1477" s="T104">koptɨ-kɔːlɨ-k</ta>
            <ta e="T106" id="Seg_1478" s="T105">am-ŋɨ-tɨ</ta>
            <ta e="T107" id="Seg_1479" s="T106">meː</ta>
            <ta e="T108" id="Seg_1480" s="T107">tiː</ta>
            <ta e="T109" id="Seg_1481" s="T108">qən-ɛntɨ-k</ta>
            <ta e="T110" id="Seg_1482" s="T109">sɨːrɨ-m</ta>
            <ta e="T111" id="Seg_1483" s="T110">apsɨ-tɨ-qo</ta>
            <ta e="T112" id="Seg_1484" s="T111">tajekn-ɛnta</ta>
            <ta e="T113" id="Seg_1485" s="T112">aj</ta>
            <ta e="T114" id="Seg_1486" s="T113">tüt-tɨ</ta>
            <ta e="T115" id="Seg_1487" s="T114">qatal-ɛntɨ-m</ta>
            <ta e="T116" id="Seg_1488" s="T115">nɨːnɨ</ta>
            <ta e="T117" id="Seg_1489" s="T116">nʼuːtɨ</ta>
            <ta e="T118" id="Seg_1490" s="T117">mi-ɛntɨ-m</ta>
            <ta e="T119" id="Seg_1491" s="T118">nɨːnɨ</ta>
            <ta e="T120" id="Seg_1492" s="T119">aj</ta>
            <ta e="T121" id="Seg_1493" s="T120">moloko-mɨ</ta>
            <ta e="T122" id="Seg_1494" s="T121">qən-ɛntɨ-m</ta>
            <ta e="T123" id="Seg_1495" s="T122">stalova-ntɨ</ta>
            <ta e="T124" id="Seg_1496" s="T123">čʼuntɨ</ta>
            <ta e="T125" id="Seg_1497" s="T124">*sar-äl-ätɨ</ta>
            <ta e="T126" id="Seg_1498" s="T125">qən-tɨ-qo</ta>
            <ta e="T127" id="Seg_1499" s="T126">moloko</ta>
            <ta e="T128" id="Seg_1500" s="T127">aj</ta>
            <ta e="T129" id="Seg_1501" s="T128">nʼuːtɨ</ta>
            <ta e="T130" id="Seg_1502" s="T129">taːtɨ-ätɨ</ta>
            <ta e="T131" id="Seg_1503" s="T130">sɨːrɨ-m</ta>
            <ta e="T132" id="Seg_1504" s="T131">apsɨ-tɨ-qo</ta>
            <ta e="T133" id="Seg_1505" s="T132">nʼuːtɨ</ta>
            <ta e="T134" id="Seg_1506" s="T133">čʼäːŋkɨ</ta>
            <ta e="T135" id="Seg_1507" s="T134">kunɨ</ta>
            <ta e="T136" id="Seg_1508" s="T135">taːtɨ-ɛntɨ</ta>
            <ta e="T137" id="Seg_1509" s="T136">toːnna</ta>
            <ta e="T138" id="Seg_1510" s="T137">nʼuːtɨ</ta>
            <ta e="T139" id="Seg_1511" s="T138">akarot-qɨn</ta>
            <ta e="T140" id="Seg_1512" s="T139">naj</ta>
            <ta e="T141" id="Seg_1513" s="T140">ɛː-ntɨ</ta>
            <ta e="T142" id="Seg_1514" s="T141">nɨːnɨ</ta>
            <ta e="T143" id="Seg_1515" s="T142">taːtɨ-ätɨ</ta>
            <ta e="T144" id="Seg_1516" s="T143">sɨːrɨ-t</ta>
            <ta e="T145" id="Seg_1517" s="T144">amna-n</ta>
            <ta e="T146" id="Seg_1518" s="T145">nɨŋ-tɨt</ta>
            <ta e="T147" id="Seg_1519" s="T146">nʼima-tɨt</ta>
            <ta e="T148" id="Seg_1520" s="T147">čʼäːŋkɨ-ɛntɨ</ta>
            <ta e="T149" id="Seg_1521" s="T148">üːtɨ-k</ta>
            <ta e="T150" id="Seg_1522" s="T149">aj</ta>
            <ta e="T151" id="Seg_1523" s="T150">üt-ɨ-m</ta>
            <ta e="T152" id="Seg_1524" s="T151">naːda</ta>
            <ta e="T153" id="Seg_1525" s="T152">taːtɨ-qo</ta>
            <ta e="T154" id="Seg_1526" s="T153">üːtɨ-k</ta>
            <ta e="T155" id="Seg_1527" s="T154">sɨːrɨ-t-ɨ-m</ta>
            <ta e="T156" id="Seg_1528" s="T155">ütɨ-altɨ-ɛː-psɔːt-j</ta>
            <ta e="T157" id="Seg_1529" s="T156">čʼäːŋkɨ</ta>
            <ta e="T158" id="Seg_1530" s="T157">üt</ta>
            <ta e="T159" id="Seg_1531" s="T158">man</ta>
            <ta e="T160" id="Seg_1532" s="T159">tiː</ta>
            <ta e="T161" id="Seg_1533" s="T160">qən-ɛntɨ-k</ta>
            <ta e="T162" id="Seg_1534" s="T161">topɨr-ɨ-š-lä</ta>
            <ta e="T163" id="Seg_1535" s="T162">a</ta>
            <ta e="T164" id="Seg_1536" s="T163">topɨr</ta>
            <ta e="T165" id="Seg_1537" s="T164">mačʼɨ-qɨn</ta>
            <ta e="T166" id="Seg_1538" s="T165">namantɨ</ta>
            <ta e="T167" id="Seg_1539" s="T166">ɛː-ŋɨ</ta>
            <ta e="T168" id="Seg_1540" s="T167">qaj-lʼ</ta>
            <ta e="T169" id="Seg_1541" s="T168">topɨr</ta>
            <ta e="T170" id="Seg_1542" s="T169">ɛsɨ</ta>
            <ta e="T171" id="Seg_1543" s="T170">mačʼɨ-qɨn</ta>
            <ta e="T172" id="Seg_1544" s="T171">nʼarqɨ</ta>
            <ta e="T173" id="Seg_1545" s="T172">topɨr</ta>
            <ta e="T174" id="Seg_1546" s="T173">kərɨntä</ta>
            <ta e="T175" id="Seg_1547" s="T174">kotɨlʼ</ta>
            <ta e="T176" id="Seg_1548" s="T175">topɨr</ta>
            <ta e="T177" id="Seg_1549" s="T176">palqaŋ</ta>
            <ta e="T178" id="Seg_1550" s="T177">kəptä</ta>
            <ta e="T179" id="Seg_1551" s="T178">turanʼ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1552" s="T0">I.NOM</ta>
            <ta e="T2" id="Seg_1553" s="T1">be.born-PST.[3SG.S]</ta>
            <ta e="T3" id="Seg_1554" s="T2">Chaselka-LOC</ta>
            <ta e="T4" id="Seg_1555" s="T3">here</ta>
            <ta e="T5" id="Seg_1556" s="T4">live-PST-1SG.S</ta>
            <ta e="T6" id="Seg_1557" s="T5">all.the.time</ta>
            <ta e="T7" id="Seg_1558" s="T6">there</ta>
            <ta e="T8" id="Seg_1559" s="T7">fish-VBLZ-PST-1SG.S</ta>
            <ta e="T9" id="Seg_1560" s="T8">also</ta>
            <ta e="T10" id="Seg_1561" s="T9">wild.animal-VBLZ-HAB-1PL</ta>
            <ta e="T11" id="Seg_1562" s="T10">go.hunting-HAB-1PL</ta>
            <ta e="T12" id="Seg_1563" s="T11">wild.animal-VBLZ-CVB</ta>
            <ta e="T13" id="Seg_1564" s="T12">go.hunting-HAB-1PL</ta>
            <ta e="T14" id="Seg_1565" s="T13">squirrel-CAP-CVB</ta>
            <ta e="T15" id="Seg_1566" s="T14">squirrel.[NOM]</ta>
            <ta e="T16" id="Seg_1567" s="T15">kill-HAB-1PL</ta>
            <ta e="T17" id="Seg_1568" s="T16">fox.[NOM]</ta>
            <ta e="T18" id="Seg_1569" s="T17">kill-HAB-1PL</ta>
            <ta e="T19" id="Seg_1570" s="T18">forest-GEN</ta>
            <ta e="T20" id="Seg_1571" s="T19">reindeer-ACC</ta>
            <ta e="T21" id="Seg_1572" s="T20">kill-HAB-1PL</ta>
            <ta e="T22" id="Seg_1573" s="T21">forest-LOC</ta>
            <ta e="T23" id="Seg_1574" s="T22">reindeer.[NOM]</ta>
            <ta e="T24" id="Seg_1575" s="T23">much</ta>
            <ta e="T25" id="Seg_1576" s="T24">be-CO.[3SG.S]</ta>
            <ta e="T26" id="Seg_1577" s="T25">tundra-LOC</ta>
            <ta e="T27" id="Seg_1578" s="T26">reindeer.[NOM]</ta>
            <ta e="T28" id="Seg_1579" s="T27">here</ta>
            <ta e="T29" id="Seg_1580" s="T28">be-CO-3SG.S</ta>
            <ta e="T30" id="Seg_1581" s="T29">forest-GEN</ta>
            <ta e="T31" id="Seg_1582" s="T30">reindeer.[NOM]</ta>
            <ta e="T32" id="Seg_1583" s="T31">grass-ACC</ta>
            <ta e="T33" id="Seg_1584" s="T32">chop-MULO-HAB-1PL</ta>
            <ta e="T34" id="Seg_1585" s="T33">summer-ADV.LOC</ta>
            <ta e="T35" id="Seg_1586" s="T34">cow.[NOM]</ta>
            <ta e="T36" id="Seg_1587" s="T35">for</ta>
            <ta e="T37" id="Seg_1588" s="T36">cow.[NOM]-1PL</ta>
            <ta e="T38" id="Seg_1589" s="T37">much</ta>
            <ta e="T39" id="Seg_1590" s="T38">be-CO.[3SG.S]</ta>
            <ta e="T40" id="Seg_1591" s="T39">horse.[NOM]-1PL</ta>
            <ta e="T41" id="Seg_1592" s="T40">also</ta>
            <ta e="T42" id="Seg_1593" s="T41">be-CO.[3SG.S]</ta>
            <ta e="T43" id="Seg_1594" s="T42">much-ADVZ</ta>
            <ta e="T44" id="Seg_1595" s="T43">grass-ACC</ta>
            <ta e="T45" id="Seg_1596" s="T44">chop-MULO-PST-3SG.S</ta>
            <ta e="T46" id="Seg_1597" s="T45">four</ta>
            <ta e="T47" id="Seg_1598" s="T46">ton.[NOM]</ta>
            <ta e="T48" id="Seg_1599" s="T47">hay.[NOM]</ta>
            <ta e="T49" id="Seg_1600" s="T48">few</ta>
            <ta e="T50" id="Seg_1601" s="T49">be-CO.[3SG.S]</ta>
            <ta e="T51" id="Seg_1602" s="T50">winter-ADV.LOC</ta>
            <ta e="T52" id="Seg_1603" s="T51">NEG</ta>
            <ta e="T53" id="Seg_1604" s="T52">suffice-IPFV-HAB-3SG.O</ta>
            <ta e="T54" id="Seg_1605" s="T53">we.PL.NOM</ta>
            <ta e="T55" id="Seg_1606" s="T54">firewood-VBLZ-PST-1PL</ta>
            <ta e="T56" id="Seg_1607" s="T55">fish.cooperative.[NOM]</ta>
            <ta e="T57" id="Seg_1608" s="T56">for</ta>
            <ta e="T58" id="Seg_1609" s="T57">three-ten</ta>
            <ta e="T59" id="Seg_1610" s="T58">cubic.meter.[NOM]</ta>
            <ta e="T60" id="Seg_1611" s="T59">chop-MOM-PST-1PL</ta>
            <ta e="T61" id="Seg_1612" s="T60">so.much</ta>
            <ta e="T62" id="Seg_1613" s="T61">few</ta>
            <ta e="T63" id="Seg_1614" s="T62">be-CO.[3SG.S]</ta>
            <ta e="T64" id="Seg_1615" s="T63">winter.[NOM]-3SG</ta>
            <ta e="T65" id="Seg_1616" s="T64">long</ta>
            <ta e="T66" id="Seg_1617" s="T65">be-CO.[3SG.S]</ta>
            <ta e="T67" id="Seg_1618" s="T66">NEG</ta>
            <ta e="T68" id="Seg_1619" s="T67">suffice-IPFV-HAB-3SG.O</ta>
            <ta e="T69" id="Seg_1620" s="T68">leave-OPT-1PL</ta>
            <ta e="T70" id="Seg_1621" s="T69">new</ta>
            <ta e="T71" id="Seg_1622" s="T70">earth-ILL</ta>
            <ta e="T72" id="Seg_1623" s="T71">firewood-VBLZ-CVB</ta>
            <ta e="T73" id="Seg_1624" s="T72">here</ta>
            <ta e="T74" id="Seg_1625" s="T73">firewood.[NOM]-3SG</ta>
            <ta e="T75" id="Seg_1626" s="T74">NEG.EX.[3SG.S]</ta>
            <ta e="T76" id="Seg_1627" s="T75">we.PL.NOM</ta>
            <ta e="T77" id="Seg_1628" s="T76">go.away-FUT-1PL</ta>
            <ta e="T78" id="Seg_1629" s="T77">fish-VBLZ-CVB</ta>
            <ta e="T79" id="Seg_1630" s="T78">seine.[NOM]-1PL</ta>
            <ta e="T80" id="Seg_1631" s="T79">take-FUT-1PL</ta>
            <ta e="T81" id="Seg_1632" s="T80">seine.[NOM]</ta>
            <ta e="T82" id="Seg_1633" s="T81">five</ta>
            <ta e="T83" id="Seg_1634" s="T82">ten</ta>
            <ta e="T84" id="Seg_1635" s="T83">then</ta>
            <ta e="T85" id="Seg_1636" s="T84">deer.train-TR-FUT-1PL</ta>
            <ta e="T86" id="Seg_1637" s="T85">forest-ILL</ta>
            <ta e="T87" id="Seg_1638" s="T86">fish-VBLZ-CVB</ta>
            <ta e="T88" id="Seg_1639" s="T87">boat.[NOM]-1PL</ta>
            <ta e="T89" id="Seg_1640" s="T88">load-FUT-1PL</ta>
            <ta e="T90" id="Seg_1641" s="T89">soon</ta>
            <ta e="T91" id="Seg_1642" s="T90">you.SG.NOM</ta>
            <ta e="T92" id="Seg_1643" s="T91">away</ta>
            <ta e="T93" id="Seg_1644" s="T92">NEG.IMP</ta>
            <ta e="T94" id="Seg_1645" s="T93">go.away-IMP.2SG.S</ta>
            <ta e="T95" id="Seg_1646" s="T94">son-ADJZ</ta>
            <ta e="T96" id="Seg_1647" s="T95">flock-ADJZ</ta>
            <ta e="T97" id="Seg_1648" s="T96">load-IMP.2SG.O</ta>
            <ta e="T98" id="Seg_1649" s="T97">mosquito.[NOM]</ta>
            <ta e="T99" id="Seg_1650" s="T98">so</ta>
            <ta e="T100" id="Seg_1651" s="T99">much</ta>
            <ta e="T101" id="Seg_1652" s="T100">be-CO.[3SG.S]</ta>
            <ta e="T102" id="Seg_1653" s="T101">forest-LOC</ta>
            <ta e="T103" id="Seg_1654" s="T102">son-ACC</ta>
            <ta e="T104" id="Seg_1655" s="T103">mosquito.[NOM]</ta>
            <ta e="T105" id="Seg_1656" s="T104">place-CAR-ADVZ</ta>
            <ta e="T106" id="Seg_1657" s="T105">eat-CO-3SG.O</ta>
            <ta e="T107" id="Seg_1658" s="T106">we.PL.NOM</ta>
            <ta e="T108" id="Seg_1659" s="T107">now</ta>
            <ta e="T109" id="Seg_1660" s="T108">leave-FUT-1SG.S</ta>
            <ta e="T110" id="Seg_1661" s="T109">cow-ACC</ta>
            <ta e="T111" id="Seg_1662" s="T110">food-TR-INF</ta>
            <ta e="T112" id="Seg_1663" s="T111">milk-FUT.[3SG.S]</ta>
            <ta e="T113" id="Seg_1664" s="T112">and</ta>
            <ta e="T114" id="Seg_1665" s="T113">excrements.[NOM]-3SG</ta>
            <ta e="T115" id="Seg_1666" s="T114">sweep-FUT-1SG.O</ta>
            <ta e="T116" id="Seg_1667" s="T115">then</ta>
            <ta e="T117" id="Seg_1668" s="T116">hay.[NOM]</ta>
            <ta e="T118" id="Seg_1669" s="T117">give-FUT-1SG.O</ta>
            <ta e="T119" id="Seg_1670" s="T118">then</ta>
            <ta e="T120" id="Seg_1671" s="T119">also</ta>
            <ta e="T121" id="Seg_1672" s="T120">milk.[NOM]-1SG</ta>
            <ta e="T122" id="Seg_1673" s="T121">leave-FUT-1SG.O</ta>
            <ta e="T123" id="Seg_1674" s="T122">canteen-ILL</ta>
            <ta e="T124" id="Seg_1675" s="T123">horse.[NOM]</ta>
            <ta e="T125" id="Seg_1676" s="T124">harness-MULO-IMP.2SG.O</ta>
            <ta e="T126" id="Seg_1677" s="T125">leave-TR-INF</ta>
            <ta e="T127" id="Seg_1678" s="T126">milk.[NOM]</ta>
            <ta e="T128" id="Seg_1679" s="T127">also</ta>
            <ta e="T129" id="Seg_1680" s="T128">hay.[NOM]</ta>
            <ta e="T130" id="Seg_1681" s="T129">bring-IMP.2SG.O</ta>
            <ta e="T131" id="Seg_1682" s="T130">cow-ACC</ta>
            <ta e="T132" id="Seg_1683" s="T131">food-TR-INF</ta>
            <ta e="T133" id="Seg_1684" s="T132">hay.[NOM]</ta>
            <ta e="T134" id="Seg_1685" s="T133">NEG.EX.[3SG.S]</ta>
            <ta e="T135" id="Seg_1686" s="T134">where.from</ta>
            <ta e="T136" id="Seg_1687" s="T135">bring-FUT.[3SG.S]</ta>
            <ta e="T137" id="Seg_1688" s="T136">that</ta>
            <ta e="T138" id="Seg_1689" s="T137">hay.[NOM]</ta>
            <ta e="T139" id="Seg_1690" s="T138">vegetable.garden-LOC</ta>
            <ta e="T140" id="Seg_1691" s="T139">EMPH</ta>
            <ta e="T141" id="Seg_1692" s="T140">be-IPFV.[3SG.S]</ta>
            <ta e="T142" id="Seg_1693" s="T141">from.here</ta>
            <ta e="T143" id="Seg_1694" s="T142">bring-IMP.2SG.O</ta>
            <ta e="T144" id="Seg_1695" s="T143">cow-PL.[NOM]</ta>
            <ta e="T145" id="Seg_1696" s="T144">hunger-GEN</ta>
            <ta e="T146" id="Seg_1697" s="T145">stand-3PL</ta>
            <ta e="T147" id="Seg_1698" s="T146">milk.[NOM]-3PL</ta>
            <ta e="T148" id="Seg_1699" s="T147">NEG.EX-FUT.[3SG.S]</ta>
            <ta e="T149" id="Seg_1700" s="T148">evening-ADVZ</ta>
            <ta e="T150" id="Seg_1701" s="T149">also</ta>
            <ta e="T151" id="Seg_1702" s="T150">water-EP-ACC</ta>
            <ta e="T152" id="Seg_1703" s="T151">one.should</ta>
            <ta e="T153" id="Seg_1704" s="T152">bring-INF</ta>
            <ta e="T154" id="Seg_1705" s="T153">evening-ADVZ</ta>
            <ta e="T155" id="Seg_1706" s="T154">cow-PL-EP-ACC</ta>
            <ta e="T156" id="Seg_1707" s="T155">drink-TR-PFV-DEB-1DU</ta>
            <ta e="T157" id="Seg_1708" s="T156">NEG.EX.[3SG.S]</ta>
            <ta e="T158" id="Seg_1709" s="T157">water.[NOM]</ta>
            <ta e="T159" id="Seg_1710" s="T158">I.NOM</ta>
            <ta e="T160" id="Seg_1711" s="T159">now</ta>
            <ta e="T161" id="Seg_1712" s="T160">leave-FUT-1SG.S</ta>
            <ta e="T162" id="Seg_1713" s="T161">berry-EP-VBLZ-CVB</ta>
            <ta e="T163" id="Seg_1714" s="T162">but</ta>
            <ta e="T164" id="Seg_1715" s="T163">berry.[NOM]</ta>
            <ta e="T165" id="Seg_1716" s="T164">forest-LOC</ta>
            <ta e="T166" id="Seg_1717" s="T165">so</ta>
            <ta e="T167" id="Seg_1718" s="T166">be-CO.[3SG.S]</ta>
            <ta e="T168" id="Seg_1719" s="T167">what-ADJZ</ta>
            <ta e="T169" id="Seg_1720" s="T168">berry.[NOM]</ta>
            <ta e="T170" id="Seg_1721" s="T169">become.[3SG.S]</ta>
            <ta e="T171" id="Seg_1722" s="T170">forest-LOC</ta>
            <ta e="T172" id="Seg_1723" s="T171">red</ta>
            <ta e="T173" id="Seg_1724" s="T172">berry.[NOM]</ta>
            <ta e="T174" id="Seg_1725" s="T173">bilberry.[NOM]</ta>
            <ta e="T175" id="Seg_1726" s="T174">leather</ta>
            <ta e="T176" id="Seg_1727" s="T175">berry.[NOM]</ta>
            <ta e="T177" id="Seg_1728" s="T176">cloudberry.[NOM]</ta>
            <ta e="T178" id="Seg_1729" s="T177">blackberry.[NOM]</ta>
            <ta e="T179" id="Seg_1730" s="T178">red.currant.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1731" s="T0">я.NOM</ta>
            <ta e="T2" id="Seg_1732" s="T1">родиться-PST.[3SG.S]</ta>
            <ta e="T3" id="Seg_1733" s="T2">Часелька-LOC</ta>
            <ta e="T4" id="Seg_1734" s="T3">здесь</ta>
            <ta e="T5" id="Seg_1735" s="T4">жить-PST-1SG.S</ta>
            <ta e="T6" id="Seg_1736" s="T5">всё.время</ta>
            <ta e="T7" id="Seg_1737" s="T6">там</ta>
            <ta e="T8" id="Seg_1738" s="T7">рыба-VBLZ-PST-1SG.S</ta>
            <ta e="T9" id="Seg_1739" s="T8">тоже</ta>
            <ta e="T10" id="Seg_1740" s="T9">зверь-VBLZ-HAB-1PL</ta>
            <ta e="T11" id="Seg_1741" s="T10">отправиться.на.охоту-HAB-1PL</ta>
            <ta e="T12" id="Seg_1742" s="T11">зверь-VBLZ-CVB</ta>
            <ta e="T13" id="Seg_1743" s="T12">отправиться.на.охоту-HAB-1PL</ta>
            <ta e="T14" id="Seg_1744" s="T13">белка-CAP-CVB</ta>
            <ta e="T15" id="Seg_1745" s="T14">белка.[NOM]</ta>
            <ta e="T16" id="Seg_1746" s="T15">убить-HAB-1PL</ta>
            <ta e="T17" id="Seg_1747" s="T16">лисица.[NOM]</ta>
            <ta e="T18" id="Seg_1748" s="T17">убить-HAB-1PL</ta>
            <ta e="T19" id="Seg_1749" s="T18">лес-GEN</ta>
            <ta e="T20" id="Seg_1750" s="T19">олень-ACC</ta>
            <ta e="T21" id="Seg_1751" s="T20">убить-HAB-1PL</ta>
            <ta e="T22" id="Seg_1752" s="T21">лес-LOC</ta>
            <ta e="T23" id="Seg_1753" s="T22">олень.[NOM]</ta>
            <ta e="T24" id="Seg_1754" s="T23">много</ta>
            <ta e="T25" id="Seg_1755" s="T24">быть-CO.[3SG.S]</ta>
            <ta e="T26" id="Seg_1756" s="T25">тундра-LOC</ta>
            <ta e="T27" id="Seg_1757" s="T26">олень.[NOM]</ta>
            <ta e="T28" id="Seg_1758" s="T27">здесь</ta>
            <ta e="T29" id="Seg_1759" s="T28">быть-CO-3SG.S</ta>
            <ta e="T30" id="Seg_1760" s="T29">лес-GEN</ta>
            <ta e="T31" id="Seg_1761" s="T30">олень.[NOM]</ta>
            <ta e="T32" id="Seg_1762" s="T31">трава-ACC</ta>
            <ta e="T33" id="Seg_1763" s="T32">рубить-MULO-HAB-1PL</ta>
            <ta e="T34" id="Seg_1764" s="T33">лето-ADV.LOC</ta>
            <ta e="T35" id="Seg_1765" s="T34">корова.[NOM]</ta>
            <ta e="T36" id="Seg_1766" s="T35">для</ta>
            <ta e="T37" id="Seg_1767" s="T36">корова.[NOM]-1PL</ta>
            <ta e="T38" id="Seg_1768" s="T37">много</ta>
            <ta e="T39" id="Seg_1769" s="T38">быть-CO.[3SG.S]</ta>
            <ta e="T40" id="Seg_1770" s="T39">лошадь.[NOM]-1PL</ta>
            <ta e="T41" id="Seg_1771" s="T40">тоже</ta>
            <ta e="T42" id="Seg_1772" s="T41">быть-CO.[3SG.S]</ta>
            <ta e="T43" id="Seg_1773" s="T42">много-ADVZ</ta>
            <ta e="T44" id="Seg_1774" s="T43">трава-ACC</ta>
            <ta e="T45" id="Seg_1775" s="T44">рубить-MULO-PST-3SG.S</ta>
            <ta e="T46" id="Seg_1776" s="T45">четыре</ta>
            <ta e="T47" id="Seg_1777" s="T46">тонна.[NOM]</ta>
            <ta e="T48" id="Seg_1778" s="T47">сено.[NOM]</ta>
            <ta e="T49" id="Seg_1779" s="T48">мало</ta>
            <ta e="T50" id="Seg_1780" s="T49">быть-CO.[3SG.S]</ta>
            <ta e="T51" id="Seg_1781" s="T50">зима-ADV.LOC</ta>
            <ta e="T52" id="Seg_1782" s="T51">NEG</ta>
            <ta e="T53" id="Seg_1783" s="T52">хватить-IPFV-HAB-3SG.O</ta>
            <ta e="T54" id="Seg_1784" s="T53">мы.PL.NOM</ta>
            <ta e="T55" id="Seg_1785" s="T54">дрова-VBLZ-PST-1PL</ta>
            <ta e="T56" id="Seg_1786" s="T55">рыбкооп.[NOM]</ta>
            <ta e="T57" id="Seg_1787" s="T56">для</ta>
            <ta e="T58" id="Seg_1788" s="T57">три-десять</ta>
            <ta e="T59" id="Seg_1789" s="T58">кубометр.[NOM]</ta>
            <ta e="T60" id="Seg_1790" s="T59">рубить-MOM-PST-1PL</ta>
            <ta e="T61" id="Seg_1791" s="T60">столько</ta>
            <ta e="T62" id="Seg_1792" s="T61">мало</ta>
            <ta e="T63" id="Seg_1793" s="T62">быть-CO.[3SG.S]</ta>
            <ta e="T64" id="Seg_1794" s="T63">зима.[NOM]-3SG</ta>
            <ta e="T65" id="Seg_1795" s="T64">длинный</ta>
            <ta e="T66" id="Seg_1796" s="T65">быть-CO.[3SG.S]</ta>
            <ta e="T67" id="Seg_1797" s="T66">NEG</ta>
            <ta e="T68" id="Seg_1798" s="T67">хватить-IPFV-HAB-3SG.O</ta>
            <ta e="T69" id="Seg_1799" s="T68">отправиться-OPT-1PL</ta>
            <ta e="T70" id="Seg_1800" s="T69">новый</ta>
            <ta e="T71" id="Seg_1801" s="T70">земля-ILL</ta>
            <ta e="T72" id="Seg_1802" s="T71">дрова-VBLZ-CVB</ta>
            <ta e="T73" id="Seg_1803" s="T72">здесь</ta>
            <ta e="T74" id="Seg_1804" s="T73">дрова.[NOM]-3SG</ta>
            <ta e="T75" id="Seg_1805" s="T74">NEG.EX.[3SG.S]</ta>
            <ta e="T76" id="Seg_1806" s="T75">мы.PL.NOM</ta>
            <ta e="T77" id="Seg_1807" s="T76">уйти-FUT-1PL</ta>
            <ta e="T78" id="Seg_1808" s="T77">рыба-VBLZ-CVB</ta>
            <ta e="T79" id="Seg_1809" s="T78">невод.[NOM]-1PL</ta>
            <ta e="T80" id="Seg_1810" s="T79">взять-FUT-1PL</ta>
            <ta e="T81" id="Seg_1811" s="T80">невод.[NOM]</ta>
            <ta e="T82" id="Seg_1812" s="T81">пять</ta>
            <ta e="T83" id="Seg_1813" s="T82">десять</ta>
            <ta e="T84" id="Seg_1814" s="T83">потом</ta>
            <ta e="T85" id="Seg_1815" s="T84">олений.обоз-TR-FUT-1PL</ta>
            <ta e="T86" id="Seg_1816" s="T85">лес-ILL</ta>
            <ta e="T87" id="Seg_1817" s="T86">рыба-VBLZ-CVB</ta>
            <ta e="T88" id="Seg_1818" s="T87">лодка.[NOM]-1PL</ta>
            <ta e="T89" id="Seg_1819" s="T88">нагрузить-FUT-1PL</ta>
            <ta e="T90" id="Seg_1820" s="T89">скоро</ta>
            <ta e="T91" id="Seg_1821" s="T90">ты.NOM</ta>
            <ta e="T92" id="Seg_1822" s="T91">прочь</ta>
            <ta e="T93" id="Seg_1823" s="T92">NEG.IMP</ta>
            <ta e="T94" id="Seg_1824" s="T93">уйти-IMP.2SG.S</ta>
            <ta e="T95" id="Seg_1825" s="T94">сын-ADJZ</ta>
            <ta e="T96" id="Seg_1826" s="T95">стая-ADJZ</ta>
            <ta e="T97" id="Seg_1827" s="T96">нагрузить-IMP.2SG.O</ta>
            <ta e="T98" id="Seg_1828" s="T97">комар.[NOM]</ta>
            <ta e="T99" id="Seg_1829" s="T98">так</ta>
            <ta e="T100" id="Seg_1830" s="T99">много</ta>
            <ta e="T101" id="Seg_1831" s="T100">быть-CO.[3SG.S]</ta>
            <ta e="T102" id="Seg_1832" s="T101">лес-LOC</ta>
            <ta e="T103" id="Seg_1833" s="T102">сын-ACC</ta>
            <ta e="T104" id="Seg_1834" s="T103">комар.[NOM]</ta>
            <ta e="T105" id="Seg_1835" s="T104">место-CAR-ADVZ</ta>
            <ta e="T106" id="Seg_1836" s="T105">съесть-CO-3SG.O</ta>
            <ta e="T107" id="Seg_1837" s="T106">мы.PL.NOM</ta>
            <ta e="T108" id="Seg_1838" s="T107">теперь</ta>
            <ta e="T109" id="Seg_1839" s="T108">отправиться-FUT-1SG.S</ta>
            <ta e="T110" id="Seg_1840" s="T109">корова-ACC</ta>
            <ta e="T111" id="Seg_1841" s="T110">еда-TR-INF</ta>
            <ta e="T112" id="Seg_1842" s="T111">доить-FUT.[3SG.S]</ta>
            <ta e="T113" id="Seg_1843" s="T112">и</ta>
            <ta e="T114" id="Seg_1844" s="T113">кал.[NOM]-3SG</ta>
            <ta e="T115" id="Seg_1845" s="T114">подмести-FUT-1SG.O</ta>
            <ta e="T116" id="Seg_1846" s="T115">потом</ta>
            <ta e="T117" id="Seg_1847" s="T116">сено.[NOM]</ta>
            <ta e="T118" id="Seg_1848" s="T117">дать-FUT-1SG.O</ta>
            <ta e="T119" id="Seg_1849" s="T118">потом</ta>
            <ta e="T120" id="Seg_1850" s="T119">тоже</ta>
            <ta e="T121" id="Seg_1851" s="T120">молоко.[NOM]-1SG</ta>
            <ta e="T122" id="Seg_1852" s="T121">отправиться-FUT-1SG.O</ta>
            <ta e="T123" id="Seg_1853" s="T122">столовая-ILL</ta>
            <ta e="T124" id="Seg_1854" s="T123">лошадь.[NOM]</ta>
            <ta e="T125" id="Seg_1855" s="T124">запрячь-MULO-IMP.2SG.O</ta>
            <ta e="T126" id="Seg_1856" s="T125">отправиться-TR-INF</ta>
            <ta e="T127" id="Seg_1857" s="T126">молоко.[NOM]</ta>
            <ta e="T128" id="Seg_1858" s="T127">тоже</ta>
            <ta e="T129" id="Seg_1859" s="T128">сено.[NOM]</ta>
            <ta e="T130" id="Seg_1860" s="T129">принести-IMP.2SG.O</ta>
            <ta e="T131" id="Seg_1861" s="T130">корова-ACC</ta>
            <ta e="T132" id="Seg_1862" s="T131">еда-TR-INF</ta>
            <ta e="T133" id="Seg_1863" s="T132">сено.[NOM]</ta>
            <ta e="T134" id="Seg_1864" s="T133">NEG.EX.[3SG.S]</ta>
            <ta e="T135" id="Seg_1865" s="T134">откуда</ta>
            <ta e="T136" id="Seg_1866" s="T135">принести-FUT.[3SG.S]</ta>
            <ta e="T137" id="Seg_1867" s="T136">тот</ta>
            <ta e="T138" id="Seg_1868" s="T137">сено.[NOM]</ta>
            <ta e="T139" id="Seg_1869" s="T138">огород-LOC</ta>
            <ta e="T140" id="Seg_1870" s="T139">EMPH</ta>
            <ta e="T141" id="Seg_1871" s="T140">быть-IPFV.[3SG.S]</ta>
            <ta e="T142" id="Seg_1872" s="T141">отсюда</ta>
            <ta e="T143" id="Seg_1873" s="T142">принести-IMP.2SG.O</ta>
            <ta e="T144" id="Seg_1874" s="T143">корова-PL.[NOM]</ta>
            <ta e="T145" id="Seg_1875" s="T144">голод-GEN</ta>
            <ta e="T146" id="Seg_1876" s="T145">стоять-3PL</ta>
            <ta e="T147" id="Seg_1877" s="T146">молоко.[NOM]-3PL</ta>
            <ta e="T148" id="Seg_1878" s="T147">NEG.EX-FUT.[3SG.S]</ta>
            <ta e="T149" id="Seg_1879" s="T148">вечер-ADVZ</ta>
            <ta e="T150" id="Seg_1880" s="T149">тоже</ta>
            <ta e="T151" id="Seg_1881" s="T150">вода-EP-ACC</ta>
            <ta e="T152" id="Seg_1882" s="T151">надо</ta>
            <ta e="T153" id="Seg_1883" s="T152">принести-INF</ta>
            <ta e="T154" id="Seg_1884" s="T153">вечер-ADVZ</ta>
            <ta e="T155" id="Seg_1885" s="T154">корова-PL-EP-ACC</ta>
            <ta e="T156" id="Seg_1886" s="T155">пить-TR-PFV-DEB-1DU</ta>
            <ta e="T157" id="Seg_1887" s="T156">NEG.EX.[3SG.S]</ta>
            <ta e="T158" id="Seg_1888" s="T157">вода.[NOM]</ta>
            <ta e="T159" id="Seg_1889" s="T158">я.NOM</ta>
            <ta e="T160" id="Seg_1890" s="T159">теперь</ta>
            <ta e="T161" id="Seg_1891" s="T160">отправиться-FUT-1SG.S</ta>
            <ta e="T162" id="Seg_1892" s="T161">ягода-EP-VBLZ-CVB</ta>
            <ta e="T163" id="Seg_1893" s="T162">а</ta>
            <ta e="T164" id="Seg_1894" s="T163">ягода.[NOM]</ta>
            <ta e="T165" id="Seg_1895" s="T164">лес-LOC</ta>
            <ta e="T166" id="Seg_1896" s="T165">так</ta>
            <ta e="T167" id="Seg_1897" s="T166">быть-CO.[3SG.S]</ta>
            <ta e="T168" id="Seg_1898" s="T167">что-ADJZ</ta>
            <ta e="T169" id="Seg_1899" s="T168">ягода.[NOM]</ta>
            <ta e="T170" id="Seg_1900" s="T169">стать.[3SG.S]</ta>
            <ta e="T171" id="Seg_1901" s="T170">лес-LOC</ta>
            <ta e="T172" id="Seg_1902" s="T171">красный</ta>
            <ta e="T173" id="Seg_1903" s="T172">ягода.[NOM]</ta>
            <ta e="T174" id="Seg_1904" s="T173">черника.[NOM]</ta>
            <ta e="T175" id="Seg_1905" s="T174">кожаный</ta>
            <ta e="T176" id="Seg_1906" s="T175">ягода.[NOM]</ta>
            <ta e="T177" id="Seg_1907" s="T176">морошка.[NOM]</ta>
            <ta e="T178" id="Seg_1908" s="T177">чёрная.смородина.[NOM]</ta>
            <ta e="T179" id="Seg_1909" s="T178">красная.смородина.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1910" s="T0">pers</ta>
            <ta e="T2" id="Seg_1911" s="T1">v-v:tense-v:pn</ta>
            <ta e="T3" id="Seg_1912" s="T2">nprop-n:case</ta>
            <ta e="T4" id="Seg_1913" s="T3">adv</ta>
            <ta e="T5" id="Seg_1914" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_1915" s="T5">adv</ta>
            <ta e="T7" id="Seg_1916" s="T6">adv</ta>
            <ta e="T8" id="Seg_1917" s="T7">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_1918" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_1919" s="T9">n-n&gt;v-v&gt;v-v:pn</ta>
            <ta e="T11" id="Seg_1920" s="T10">v-v&gt;v-v:pn</ta>
            <ta e="T12" id="Seg_1921" s="T11">n-n&gt;v-v&gt;adv</ta>
            <ta e="T13" id="Seg_1922" s="T12">v-v&gt;v-v:pn</ta>
            <ta e="T14" id="Seg_1923" s="T13">n-n&gt;v-v&gt;adv</ta>
            <ta e="T15" id="Seg_1924" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_1925" s="T15">v-v&gt;v-v:pn</ta>
            <ta e="T17" id="Seg_1926" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_1927" s="T17">v-v&gt;v-v:pn</ta>
            <ta e="T19" id="Seg_1928" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_1929" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_1930" s="T20">v-v&gt;v-v:pn</ta>
            <ta e="T22" id="Seg_1931" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_1932" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_1933" s="T23">quant</ta>
            <ta e="T25" id="Seg_1934" s="T24">v-v:ins-v:pn</ta>
            <ta e="T26" id="Seg_1935" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_1936" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_1937" s="T27">adv</ta>
            <ta e="T29" id="Seg_1938" s="T28">v-v:ins-v:pn</ta>
            <ta e="T30" id="Seg_1939" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_1940" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_1941" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_1942" s="T32">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T34" id="Seg_1943" s="T33">n-n&gt;adv</ta>
            <ta e="T35" id="Seg_1944" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_1945" s="T35">pp</ta>
            <ta e="T37" id="Seg_1946" s="T36">n-n:case-n:poss</ta>
            <ta e="T38" id="Seg_1947" s="T37">quant</ta>
            <ta e="T39" id="Seg_1948" s="T38">v-v:ins-v:pn</ta>
            <ta e="T40" id="Seg_1949" s="T39">n-n:case-n:poss</ta>
            <ta e="T41" id="Seg_1950" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_1951" s="T41">v-v:ins-v:pn</ta>
            <ta e="T43" id="Seg_1952" s="T42">quant-adj&gt;adv</ta>
            <ta e="T44" id="Seg_1953" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_1954" s="T44">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_1955" s="T45">num</ta>
            <ta e="T47" id="Seg_1956" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_1957" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_1958" s="T48">quant</ta>
            <ta e="T50" id="Seg_1959" s="T49">v-v:ins-v:pn</ta>
            <ta e="T51" id="Seg_1960" s="T50">n-n&gt;adv</ta>
            <ta e="T52" id="Seg_1961" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_1962" s="T52">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T54" id="Seg_1963" s="T53">pers</ta>
            <ta e="T55" id="Seg_1964" s="T54">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_1965" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_1966" s="T56">pp</ta>
            <ta e="T58" id="Seg_1967" s="T57">num-num&gt;num</ta>
            <ta e="T59" id="Seg_1968" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_1969" s="T59">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_1970" s="T60">quant</ta>
            <ta e="T62" id="Seg_1971" s="T61">quant</ta>
            <ta e="T63" id="Seg_1972" s="T62">v-v:ins-v:pn</ta>
            <ta e="T64" id="Seg_1973" s="T63">n-n:case-n:poss</ta>
            <ta e="T65" id="Seg_1974" s="T64">adj</ta>
            <ta e="T66" id="Seg_1975" s="T65">v-v:ins-v:pn</ta>
            <ta e="T67" id="Seg_1976" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_1977" s="T67">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T69" id="Seg_1978" s="T68">v-v:mood-v:pn</ta>
            <ta e="T70" id="Seg_1979" s="T69">adj</ta>
            <ta e="T71" id="Seg_1980" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_1981" s="T71">n-n&gt;v-v&gt;adv</ta>
            <ta e="T73" id="Seg_1982" s="T72">adv</ta>
            <ta e="T74" id="Seg_1983" s="T73">n-n:case-n:poss</ta>
            <ta e="T75" id="Seg_1984" s="T74">v-v:pn</ta>
            <ta e="T76" id="Seg_1985" s="T75">pers</ta>
            <ta e="T77" id="Seg_1986" s="T76">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_1987" s="T77">n-n&gt;v-v&gt;adv</ta>
            <ta e="T79" id="Seg_1988" s="T78">n-n:case-n:poss</ta>
            <ta e="T80" id="Seg_1989" s="T79">v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_1990" s="T80">n-n:case</ta>
            <ta e="T82" id="Seg_1991" s="T81">num</ta>
            <ta e="T83" id="Seg_1992" s="T82">num</ta>
            <ta e="T84" id="Seg_1993" s="T83">adv</ta>
            <ta e="T85" id="Seg_1994" s="T84">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T86" id="Seg_1995" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_1996" s="T86">n-n&gt;v-v&gt;adv</ta>
            <ta e="T88" id="Seg_1997" s="T87">n-n:case-n:poss</ta>
            <ta e="T89" id="Seg_1998" s="T88">v-v:tense-v:pn</ta>
            <ta e="T90" id="Seg_1999" s="T89">adv</ta>
            <ta e="T91" id="Seg_2000" s="T90">pers</ta>
            <ta e="T92" id="Seg_2001" s="T91">preverb</ta>
            <ta e="T93" id="Seg_2002" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_2003" s="T93">v-v:mood.pn</ta>
            <ta e="T95" id="Seg_2004" s="T94">n-n&gt;adj</ta>
            <ta e="T96" id="Seg_2005" s="T95">n-n&gt;adj</ta>
            <ta e="T97" id="Seg_2006" s="T96">v-v:mood.pn</ta>
            <ta e="T98" id="Seg_2007" s="T97">n-n:case</ta>
            <ta e="T99" id="Seg_2008" s="T98">adv</ta>
            <ta e="T100" id="Seg_2009" s="T99">quant</ta>
            <ta e="T101" id="Seg_2010" s="T100">v-v:ins-v:pn</ta>
            <ta e="T102" id="Seg_2011" s="T101">n-n:case</ta>
            <ta e="T103" id="Seg_2012" s="T102">n-n:case</ta>
            <ta e="T104" id="Seg_2013" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_2014" s="T104">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T106" id="Seg_2015" s="T105">v-v:ins-v:pn</ta>
            <ta e="T107" id="Seg_2016" s="T106">pers</ta>
            <ta e="T108" id="Seg_2017" s="T107">adv</ta>
            <ta e="T109" id="Seg_2018" s="T108">v-v:tense-v:pn</ta>
            <ta e="T110" id="Seg_2019" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_2020" s="T110">n-n&gt;v-v:inf</ta>
            <ta e="T112" id="Seg_2021" s="T111">v-v:tense-v:pn</ta>
            <ta e="T113" id="Seg_2022" s="T112">conj</ta>
            <ta e="T114" id="Seg_2023" s="T113">n-n:case-n:poss</ta>
            <ta e="T115" id="Seg_2024" s="T114">v-v:tense-v:pn</ta>
            <ta e="T116" id="Seg_2025" s="T115">adv</ta>
            <ta e="T117" id="Seg_2026" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_2027" s="T117">v-v:tense-v:pn</ta>
            <ta e="T119" id="Seg_2028" s="T118">adv</ta>
            <ta e="T120" id="Seg_2029" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_2030" s="T120">n-n:case-n:poss</ta>
            <ta e="T122" id="Seg_2031" s="T121">v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_2032" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_2033" s="T123">n-n:case</ta>
            <ta e="T125" id="Seg_2034" s="T124">v-v&gt;v-v:mood.pn</ta>
            <ta e="T126" id="Seg_2035" s="T125">v-v&gt;v-v:inf</ta>
            <ta e="T127" id="Seg_2036" s="T126">n-n:case</ta>
            <ta e="T128" id="Seg_2037" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_2038" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_2039" s="T129">v-v:mood.pn</ta>
            <ta e="T131" id="Seg_2040" s="T130">n-n:case</ta>
            <ta e="T132" id="Seg_2041" s="T131">n-n&gt;v-v:inf</ta>
            <ta e="T133" id="Seg_2042" s="T132">n-n:case</ta>
            <ta e="T134" id="Seg_2043" s="T133">v-v:pn</ta>
            <ta e="T135" id="Seg_2044" s="T134">interrog</ta>
            <ta e="T136" id="Seg_2045" s="T135">v-v:tense-v:pn</ta>
            <ta e="T137" id="Seg_2046" s="T136">dem</ta>
            <ta e="T138" id="Seg_2047" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_2048" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_2049" s="T139">clit</ta>
            <ta e="T141" id="Seg_2050" s="T140">v-v&gt;v-v:pn</ta>
            <ta e="T142" id="Seg_2051" s="T141">adv</ta>
            <ta e="T143" id="Seg_2052" s="T142">v-v:mood.pn</ta>
            <ta e="T144" id="Seg_2053" s="T143">n-n:num-n:case</ta>
            <ta e="T145" id="Seg_2054" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_2055" s="T145">v-v:pn</ta>
            <ta e="T147" id="Seg_2056" s="T146">n-n:case-n:poss</ta>
            <ta e="T148" id="Seg_2057" s="T147">v-v:tense-v:pn</ta>
            <ta e="T149" id="Seg_2058" s="T148">n-n&gt;adv</ta>
            <ta e="T150" id="Seg_2059" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_2060" s="T150">n-n:ins-n:case</ta>
            <ta e="T152" id="Seg_2061" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_2062" s="T152">v-v:inf</ta>
            <ta e="T154" id="Seg_2063" s="T153">n-n&gt;adv</ta>
            <ta e="T155" id="Seg_2064" s="T154">n-n:num-n:ins-n:case</ta>
            <ta e="T156" id="Seg_2065" s="T155">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T157" id="Seg_2066" s="T156">v-v:pn</ta>
            <ta e="T158" id="Seg_2067" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_2068" s="T158">pers</ta>
            <ta e="T160" id="Seg_2069" s="T159">adv</ta>
            <ta e="T161" id="Seg_2070" s="T160">v-v:tense-v:pn</ta>
            <ta e="T162" id="Seg_2071" s="T161">n-n:ins-n&gt;v-v&gt;adv</ta>
            <ta e="T163" id="Seg_2072" s="T162">conj</ta>
            <ta e="T164" id="Seg_2073" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_2074" s="T164">n-n:case</ta>
            <ta e="T166" id="Seg_2075" s="T165">adv</ta>
            <ta e="T167" id="Seg_2076" s="T166">v-v:ins-v:pn</ta>
            <ta e="T168" id="Seg_2077" s="T167">interrog-n&gt;adj</ta>
            <ta e="T169" id="Seg_2078" s="T168">n-n:case</ta>
            <ta e="T170" id="Seg_2079" s="T169">v-v:pn</ta>
            <ta e="T171" id="Seg_2080" s="T170">n-n:case</ta>
            <ta e="T172" id="Seg_2081" s="T171">adj</ta>
            <ta e="T173" id="Seg_2082" s="T172">n-n:case</ta>
            <ta e="T174" id="Seg_2083" s="T173">n-n:case</ta>
            <ta e="T175" id="Seg_2084" s="T174">adj</ta>
            <ta e="T176" id="Seg_2085" s="T175">n-n:case</ta>
            <ta e="T177" id="Seg_2086" s="T176">n-n:case</ta>
            <ta e="T178" id="Seg_2087" s="T177">n-n:case</ta>
            <ta e="T179" id="Seg_2088" s="T178">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2089" s="T0">pers</ta>
            <ta e="T2" id="Seg_2090" s="T1">v</ta>
            <ta e="T3" id="Seg_2091" s="T2">nprop</ta>
            <ta e="T4" id="Seg_2092" s="T3">adv</ta>
            <ta e="T5" id="Seg_2093" s="T4">v</ta>
            <ta e="T6" id="Seg_2094" s="T5">adv</ta>
            <ta e="T7" id="Seg_2095" s="T6">adv</ta>
            <ta e="T8" id="Seg_2096" s="T7">v</ta>
            <ta e="T9" id="Seg_2097" s="T8">conj</ta>
            <ta e="T10" id="Seg_2098" s="T9">v</ta>
            <ta e="T11" id="Seg_2099" s="T10">v</ta>
            <ta e="T12" id="Seg_2100" s="T11">adv</ta>
            <ta e="T13" id="Seg_2101" s="T12">v</ta>
            <ta e="T14" id="Seg_2102" s="T13">adv</ta>
            <ta e="T15" id="Seg_2103" s="T14">n</ta>
            <ta e="T16" id="Seg_2104" s="T15">v</ta>
            <ta e="T17" id="Seg_2105" s="T16">n</ta>
            <ta e="T18" id="Seg_2106" s="T17">v</ta>
            <ta e="T19" id="Seg_2107" s="T18">n</ta>
            <ta e="T20" id="Seg_2108" s="T19">n</ta>
            <ta e="T21" id="Seg_2109" s="T20">v</ta>
            <ta e="T22" id="Seg_2110" s="T21">n</ta>
            <ta e="T23" id="Seg_2111" s="T22">n</ta>
            <ta e="T24" id="Seg_2112" s="T23">quant</ta>
            <ta e="T25" id="Seg_2113" s="T24">v</ta>
            <ta e="T26" id="Seg_2114" s="T25">n</ta>
            <ta e="T27" id="Seg_2115" s="T26">n</ta>
            <ta e="T28" id="Seg_2116" s="T27">adv</ta>
            <ta e="T29" id="Seg_2117" s="T28">v</ta>
            <ta e="T30" id="Seg_2118" s="T29">n</ta>
            <ta e="T31" id="Seg_2119" s="T30">n</ta>
            <ta e="T32" id="Seg_2120" s="T31">n</ta>
            <ta e="T33" id="Seg_2121" s="T32">v</ta>
            <ta e="T34" id="Seg_2122" s="T33">adv</ta>
            <ta e="T35" id="Seg_2123" s="T34">n</ta>
            <ta e="T36" id="Seg_2124" s="T35">pp</ta>
            <ta e="T37" id="Seg_2125" s="T36">n</ta>
            <ta e="T38" id="Seg_2126" s="T37">quant</ta>
            <ta e="T39" id="Seg_2127" s="T38">v</ta>
            <ta e="T40" id="Seg_2128" s="T39">n</ta>
            <ta e="T41" id="Seg_2129" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_2130" s="T41">v</ta>
            <ta e="T43" id="Seg_2131" s="T42">quant</ta>
            <ta e="T44" id="Seg_2132" s="T43">n</ta>
            <ta e="T45" id="Seg_2133" s="T44">v</ta>
            <ta e="T46" id="Seg_2134" s="T45">num</ta>
            <ta e="T47" id="Seg_2135" s="T46">n</ta>
            <ta e="T48" id="Seg_2136" s="T47">n</ta>
            <ta e="T49" id="Seg_2137" s="T48">quant</ta>
            <ta e="T50" id="Seg_2138" s="T49">v</ta>
            <ta e="T51" id="Seg_2139" s="T50">adv</ta>
            <ta e="T52" id="Seg_2140" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_2141" s="T52">v</ta>
            <ta e="T54" id="Seg_2142" s="T53">pers</ta>
            <ta e="T55" id="Seg_2143" s="T54">v</ta>
            <ta e="T56" id="Seg_2144" s="T55">n</ta>
            <ta e="T57" id="Seg_2145" s="T56">pp</ta>
            <ta e="T58" id="Seg_2146" s="T57">num</ta>
            <ta e="T59" id="Seg_2147" s="T58">n</ta>
            <ta e="T60" id="Seg_2148" s="T59">v</ta>
            <ta e="T61" id="Seg_2149" s="T60">quant</ta>
            <ta e="T62" id="Seg_2150" s="T61">quant</ta>
            <ta e="T63" id="Seg_2151" s="T62">v</ta>
            <ta e="T64" id="Seg_2152" s="T63">n</ta>
            <ta e="T65" id="Seg_2153" s="T64">adj</ta>
            <ta e="T66" id="Seg_2154" s="T65">v</ta>
            <ta e="T67" id="Seg_2155" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_2156" s="T67">v</ta>
            <ta e="T69" id="Seg_2157" s="T68">v</ta>
            <ta e="T70" id="Seg_2158" s="T69">adj</ta>
            <ta e="T71" id="Seg_2159" s="T70">n</ta>
            <ta e="T72" id="Seg_2160" s="T71">adv</ta>
            <ta e="T73" id="Seg_2161" s="T72">adv</ta>
            <ta e="T74" id="Seg_2162" s="T73">n</ta>
            <ta e="T75" id="Seg_2163" s="T74">v</ta>
            <ta e="T76" id="Seg_2164" s="T75">pers</ta>
            <ta e="T77" id="Seg_2165" s="T76">v</ta>
            <ta e="T78" id="Seg_2166" s="T77">adv</ta>
            <ta e="T79" id="Seg_2167" s="T78">n</ta>
            <ta e="T80" id="Seg_2168" s="T79">v</ta>
            <ta e="T81" id="Seg_2169" s="T80">n</ta>
            <ta e="T82" id="Seg_2170" s="T81">num</ta>
            <ta e="T83" id="Seg_2171" s="T82">num</ta>
            <ta e="T84" id="Seg_2172" s="T83">adv</ta>
            <ta e="T85" id="Seg_2173" s="T84">v</ta>
            <ta e="T86" id="Seg_2174" s="T85">n</ta>
            <ta e="T87" id="Seg_2175" s="T86">adv</ta>
            <ta e="T88" id="Seg_2176" s="T87">n</ta>
            <ta e="T89" id="Seg_2177" s="T88">v</ta>
            <ta e="T90" id="Seg_2178" s="T89">adv</ta>
            <ta e="T91" id="Seg_2179" s="T90">pers</ta>
            <ta e="T92" id="Seg_2180" s="T91">preverb</ta>
            <ta e="T93" id="Seg_2181" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_2182" s="T93">v</ta>
            <ta e="T95" id="Seg_2183" s="T94">adj</ta>
            <ta e="T96" id="Seg_2184" s="T95">adj</ta>
            <ta e="T97" id="Seg_2185" s="T96">v</ta>
            <ta e="T98" id="Seg_2186" s="T97">n</ta>
            <ta e="T99" id="Seg_2187" s="T98">adv</ta>
            <ta e="T100" id="Seg_2188" s="T99">quant</ta>
            <ta e="T101" id="Seg_2189" s="T100">v</ta>
            <ta e="T102" id="Seg_2190" s="T101">n</ta>
            <ta e="T103" id="Seg_2191" s="T102">n</ta>
            <ta e="T104" id="Seg_2192" s="T103">n</ta>
            <ta e="T105" id="Seg_2193" s="T104">adv</ta>
            <ta e="T106" id="Seg_2194" s="T105">v</ta>
            <ta e="T107" id="Seg_2195" s="T106">pers</ta>
            <ta e="T108" id="Seg_2196" s="T107">adv</ta>
            <ta e="T109" id="Seg_2197" s="T108">v</ta>
            <ta e="T110" id="Seg_2198" s="T109">n</ta>
            <ta e="T111" id="Seg_2199" s="T110">v</ta>
            <ta e="T112" id="Seg_2200" s="T111">v</ta>
            <ta e="T113" id="Seg_2201" s="T112">conj</ta>
            <ta e="T114" id="Seg_2202" s="T113">n</ta>
            <ta e="T115" id="Seg_2203" s="T114">v</ta>
            <ta e="T116" id="Seg_2204" s="T115">adv</ta>
            <ta e="T117" id="Seg_2205" s="T116">n</ta>
            <ta e="T118" id="Seg_2206" s="T117">v</ta>
            <ta e="T119" id="Seg_2207" s="T118">adv</ta>
            <ta e="T120" id="Seg_2208" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_2209" s="T120">n</ta>
            <ta e="T122" id="Seg_2210" s="T121">v</ta>
            <ta e="T123" id="Seg_2211" s="T122">n</ta>
            <ta e="T124" id="Seg_2212" s="T123">n</ta>
            <ta e="T125" id="Seg_2213" s="T124">v</ta>
            <ta e="T126" id="Seg_2214" s="T125">v</ta>
            <ta e="T127" id="Seg_2215" s="T126">n</ta>
            <ta e="T128" id="Seg_2216" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_2217" s="T128">n</ta>
            <ta e="T130" id="Seg_2218" s="T129">v</ta>
            <ta e="T131" id="Seg_2219" s="T130">n</ta>
            <ta e="T132" id="Seg_2220" s="T131">n</ta>
            <ta e="T133" id="Seg_2221" s="T132">n</ta>
            <ta e="T134" id="Seg_2222" s="T133">v</ta>
            <ta e="T135" id="Seg_2223" s="T134">interrog</ta>
            <ta e="T136" id="Seg_2224" s="T135">v</ta>
            <ta e="T137" id="Seg_2225" s="T136">dem</ta>
            <ta e="T138" id="Seg_2226" s="T137">n</ta>
            <ta e="T139" id="Seg_2227" s="T138">n</ta>
            <ta e="T140" id="Seg_2228" s="T139">clit</ta>
            <ta e="T141" id="Seg_2229" s="T140">v</ta>
            <ta e="T142" id="Seg_2230" s="T141">adv</ta>
            <ta e="T143" id="Seg_2231" s="T142">v</ta>
            <ta e="T144" id="Seg_2232" s="T143">n</ta>
            <ta e="T145" id="Seg_2233" s="T144">n</ta>
            <ta e="T146" id="Seg_2234" s="T145">v</ta>
            <ta e="T147" id="Seg_2235" s="T146">n</ta>
            <ta e="T148" id="Seg_2236" s="T147">v</ta>
            <ta e="T149" id="Seg_2237" s="T148">adv</ta>
            <ta e="T150" id="Seg_2238" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_2239" s="T150">n</ta>
            <ta e="T152" id="Seg_2240" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_2241" s="T152">v</ta>
            <ta e="T154" id="Seg_2242" s="T153">adv</ta>
            <ta e="T155" id="Seg_2243" s="T154">n</ta>
            <ta e="T156" id="Seg_2244" s="T155">v</ta>
            <ta e="T157" id="Seg_2245" s="T156">v</ta>
            <ta e="T158" id="Seg_2246" s="T157">n</ta>
            <ta e="T159" id="Seg_2247" s="T158">pers</ta>
            <ta e="T160" id="Seg_2248" s="T159">adv</ta>
            <ta e="T161" id="Seg_2249" s="T160">v</ta>
            <ta e="T162" id="Seg_2250" s="T161">adv</ta>
            <ta e="T163" id="Seg_2251" s="T162">conj</ta>
            <ta e="T164" id="Seg_2252" s="T163">n</ta>
            <ta e="T165" id="Seg_2253" s="T164">n</ta>
            <ta e="T166" id="Seg_2254" s="T165">adv</ta>
            <ta e="T167" id="Seg_2255" s="T166">v</ta>
            <ta e="T168" id="Seg_2256" s="T167">adj</ta>
            <ta e="T169" id="Seg_2257" s="T168">n</ta>
            <ta e="T170" id="Seg_2258" s="T169">v</ta>
            <ta e="T171" id="Seg_2259" s="T170">n</ta>
            <ta e="T172" id="Seg_2260" s="T171">adj</ta>
            <ta e="T173" id="Seg_2261" s="T172">n</ta>
            <ta e="T174" id="Seg_2262" s="T173">n</ta>
            <ta e="T175" id="Seg_2263" s="T174">adj</ta>
            <ta e="T176" id="Seg_2264" s="T175">n</ta>
            <ta e="T177" id="Seg_2265" s="T176">n</ta>
            <ta e="T178" id="Seg_2266" s="T177">n</ta>
            <ta e="T179" id="Seg_2267" s="T178">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_2268" s="T0">pro.h:P</ta>
            <ta e="T3" id="Seg_2269" s="T2">np:L</ta>
            <ta e="T4" id="Seg_2270" s="T3">adv:L</ta>
            <ta e="T5" id="Seg_2271" s="T4">0.1.h:Th</ta>
            <ta e="T6" id="Seg_2272" s="T5">adv:Time</ta>
            <ta e="T7" id="Seg_2273" s="T6">adv:L</ta>
            <ta e="T8" id="Seg_2274" s="T7">0.1.h:A</ta>
            <ta e="T10" id="Seg_2275" s="T9">0.1.h:A</ta>
            <ta e="T11" id="Seg_2276" s="T10">0.1.h:A</ta>
            <ta e="T13" id="Seg_2277" s="T12">0.1.h:A</ta>
            <ta e="T15" id="Seg_2278" s="T14">np:P</ta>
            <ta e="T16" id="Seg_2279" s="T15">0.1.h:A</ta>
            <ta e="T17" id="Seg_2280" s="T16">np:P</ta>
            <ta e="T18" id="Seg_2281" s="T17">0.1.h:A</ta>
            <ta e="T19" id="Seg_2282" s="T18">np:Poss</ta>
            <ta e="T20" id="Seg_2283" s="T19">np:P</ta>
            <ta e="T21" id="Seg_2284" s="T20">0.1.h:A</ta>
            <ta e="T22" id="Seg_2285" s="T21">np:L</ta>
            <ta e="T23" id="Seg_2286" s="T22">np:Th</ta>
            <ta e="T26" id="Seg_2287" s="T25">np:L</ta>
            <ta e="T27" id="Seg_2288" s="T26">np:Th</ta>
            <ta e="T28" id="Seg_2289" s="T27">adv:L</ta>
            <ta e="T30" id="Seg_2290" s="T29">np:Poss</ta>
            <ta e="T31" id="Seg_2291" s="T30">np:Th</ta>
            <ta e="T32" id="Seg_2292" s="T31">np:P</ta>
            <ta e="T33" id="Seg_2293" s="T32">0.1.h:A</ta>
            <ta e="T34" id="Seg_2294" s="T33">adv:Time</ta>
            <ta e="T35" id="Seg_2295" s="T34">pp:B</ta>
            <ta e="T37" id="Seg_2296" s="T36">0.1.h:Poss np:Th</ta>
            <ta e="T40" id="Seg_2297" s="T39">0.1.h:Poss np:Th</ta>
            <ta e="T44" id="Seg_2298" s="T43">np:P</ta>
            <ta e="T45" id="Seg_2299" s="T44">0.3.h:A</ta>
            <ta e="T48" id="Seg_2300" s="T47">np:Th</ta>
            <ta e="T51" id="Seg_2301" s="T50">adv:Time</ta>
            <ta e="T53" id="Seg_2302" s="T52">0.3:Th</ta>
            <ta e="T54" id="Seg_2303" s="T53">pro.h:A</ta>
            <ta e="T56" id="Seg_2304" s="T55">pp:B</ta>
            <ta e="T59" id="Seg_2305" s="T58">np:P</ta>
            <ta e="T60" id="Seg_2306" s="T59">0.1.h:A</ta>
            <ta e="T63" id="Seg_2307" s="T62">0.3:Th</ta>
            <ta e="T64" id="Seg_2308" s="T63">np:Th</ta>
            <ta e="T68" id="Seg_2309" s="T67">0.3:Th</ta>
            <ta e="T69" id="Seg_2310" s="T68">0.1.h:A</ta>
            <ta e="T71" id="Seg_2311" s="T70">np:G</ta>
            <ta e="T73" id="Seg_2312" s="T72">adv:L</ta>
            <ta e="T74" id="Seg_2313" s="T73">np:Th</ta>
            <ta e="T76" id="Seg_2314" s="T75">pro.h:A</ta>
            <ta e="T79" id="Seg_2315" s="T78">np:Th</ta>
            <ta e="T80" id="Seg_2316" s="T79">0.1.h:A</ta>
            <ta e="T84" id="Seg_2317" s="T83">adv:Time</ta>
            <ta e="T85" id="Seg_2318" s="T84">0.1.h:A</ta>
            <ta e="T86" id="Seg_2319" s="T85">np:G</ta>
            <ta e="T88" id="Seg_2320" s="T87">np:Th 0.1.h:Poss</ta>
            <ta e="T89" id="Seg_2321" s="T88">0.1.h:A</ta>
            <ta e="T90" id="Seg_2322" s="T89">adv:Time</ta>
            <ta e="T91" id="Seg_2323" s="T90">pro.h:A</ta>
            <ta e="T95" id="Seg_2324" s="T94">np.h:Th</ta>
            <ta e="T97" id="Seg_2325" s="T96">0.2.h:A</ta>
            <ta e="T98" id="Seg_2326" s="T97">np:Th</ta>
            <ta e="T102" id="Seg_2327" s="T101">np:L</ta>
            <ta e="T103" id="Seg_2328" s="T102">np.h:P</ta>
            <ta e="T104" id="Seg_2329" s="T103">np:A</ta>
            <ta e="T107" id="Seg_2330" s="T106">pro.h:A</ta>
            <ta e="T108" id="Seg_2331" s="T107">adv:Time</ta>
            <ta e="T110" id="Seg_2332" s="T109">np:B</ta>
            <ta e="T112" id="Seg_2333" s="T111">0.3.h:A</ta>
            <ta e="T114" id="Seg_2334" s="T113">np:Th</ta>
            <ta e="T115" id="Seg_2335" s="T114">0.1.h:A</ta>
            <ta e="T116" id="Seg_2336" s="T115">adv:Time</ta>
            <ta e="T117" id="Seg_2337" s="T116">np:Th</ta>
            <ta e="T118" id="Seg_2338" s="T117">0.1.h:A</ta>
            <ta e="T119" id="Seg_2339" s="T118">adv:Time</ta>
            <ta e="T121" id="Seg_2340" s="T120">np:Th</ta>
            <ta e="T122" id="Seg_2341" s="T121">0.1.h:A</ta>
            <ta e="T123" id="Seg_2342" s="T122">np:G</ta>
            <ta e="T124" id="Seg_2343" s="T123">np:Th</ta>
            <ta e="T125" id="Seg_2344" s="T124">0.2.h:A</ta>
            <ta e="T127" id="Seg_2345" s="T126">np:Th</ta>
            <ta e="T129" id="Seg_2346" s="T128">np:Th</ta>
            <ta e="T130" id="Seg_2347" s="T129">0.2.h:A</ta>
            <ta e="T131" id="Seg_2348" s="T130">np:B</ta>
            <ta e="T133" id="Seg_2349" s="T132">np:Th</ta>
            <ta e="T135" id="Seg_2350" s="T134">pro:So</ta>
            <ta e="T136" id="Seg_2351" s="T135">0.3.h:A</ta>
            <ta e="T138" id="Seg_2352" s="T137">np:Th</ta>
            <ta e="T142" id="Seg_2353" s="T141">adv:So</ta>
            <ta e="T143" id="Seg_2354" s="T142">0.2.h:A 0.3:Th</ta>
            <ta e="T144" id="Seg_2355" s="T143">np:Th</ta>
            <ta e="T147" id="Seg_2356" s="T146">0.3:Poss np:Th</ta>
            <ta e="T149" id="Seg_2357" s="T148">adv:Time</ta>
            <ta e="T151" id="Seg_2358" s="T150">np:Th</ta>
            <ta e="T153" id="Seg_2359" s="T152">v:Th</ta>
            <ta e="T154" id="Seg_2360" s="T153">adv:Time</ta>
            <ta e="T155" id="Seg_2361" s="T154">np:B</ta>
            <ta e="T156" id="Seg_2362" s="T155">0.1.h:A</ta>
            <ta e="T158" id="Seg_2363" s="T157">np:Th</ta>
            <ta e="T159" id="Seg_2364" s="T158">pro.h:A</ta>
            <ta e="T160" id="Seg_2365" s="T159">adv:Time</ta>
            <ta e="T164" id="Seg_2366" s="T163">np:Th</ta>
            <ta e="T165" id="Seg_2367" s="T164">np:L</ta>
            <ta e="T169" id="Seg_2368" s="T168">np:Th</ta>
            <ta e="T171" id="Seg_2369" s="T170">np:L</ta>
            <ta e="T173" id="Seg_2370" s="T172">np:Th</ta>
            <ta e="T174" id="Seg_2371" s="T173">np:Th</ta>
            <ta e="T176" id="Seg_2372" s="T175">np:Th</ta>
            <ta e="T177" id="Seg_2373" s="T176">np:Th</ta>
            <ta e="T178" id="Seg_2374" s="T177">np:Th</ta>
            <ta e="T179" id="Seg_2375" s="T178">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_2376" s="T0">pro.h:S</ta>
            <ta e="T2" id="Seg_2377" s="T1">v:pred</ta>
            <ta e="T5" id="Seg_2378" s="T4">0.1.h:S v:pred</ta>
            <ta e="T8" id="Seg_2379" s="T7">0.1.h:S v:pred</ta>
            <ta e="T10" id="Seg_2380" s="T9">0.1.h:S v:pred</ta>
            <ta e="T11" id="Seg_2381" s="T10">0.1.h:S v:pred</ta>
            <ta e="T12" id="Seg_2382" s="T11">s:adv</ta>
            <ta e="T13" id="Seg_2383" s="T12">0.1.h:S v:pred</ta>
            <ta e="T14" id="Seg_2384" s="T13">s:adv</ta>
            <ta e="T15" id="Seg_2385" s="T14">np:O</ta>
            <ta e="T16" id="Seg_2386" s="T15">0.1.h:S v:pred</ta>
            <ta e="T17" id="Seg_2387" s="T16">np:O</ta>
            <ta e="T18" id="Seg_2388" s="T17">0.1.h:S v:pred</ta>
            <ta e="T20" id="Seg_2389" s="T19">np:O</ta>
            <ta e="T21" id="Seg_2390" s="T20">0.1.h:S v:pred</ta>
            <ta e="T23" id="Seg_2391" s="T22">np:S</ta>
            <ta e="T24" id="Seg_2392" s="T23">n:pred</ta>
            <ta e="T25" id="Seg_2393" s="T24">cop</ta>
            <ta e="T27" id="Seg_2394" s="T26">np:S</ta>
            <ta e="T29" id="Seg_2395" s="T28">v:pred</ta>
            <ta e="T31" id="Seg_2396" s="T30">np:S</ta>
            <ta e="T32" id="Seg_2397" s="T31">np:O</ta>
            <ta e="T33" id="Seg_2398" s="T32">0.1.h:S v:pred</ta>
            <ta e="T37" id="Seg_2399" s="T36">np:S</ta>
            <ta e="T38" id="Seg_2400" s="T37">n:pred</ta>
            <ta e="T39" id="Seg_2401" s="T38">cop</ta>
            <ta e="T40" id="Seg_2402" s="T39">np:S</ta>
            <ta e="T42" id="Seg_2403" s="T41">v:pred</ta>
            <ta e="T44" id="Seg_2404" s="T43">np:O</ta>
            <ta e="T45" id="Seg_2405" s="T44">0.3.h:S v:pred</ta>
            <ta e="T48" id="Seg_2406" s="T47">np:S</ta>
            <ta e="T49" id="Seg_2407" s="T48">n:pred</ta>
            <ta e="T50" id="Seg_2408" s="T49">cop</ta>
            <ta e="T53" id="Seg_2409" s="T52">0.3:S v:pred</ta>
            <ta e="T54" id="Seg_2410" s="T53">pro.h:S</ta>
            <ta e="T55" id="Seg_2411" s="T54">v:pred</ta>
            <ta e="T59" id="Seg_2412" s="T58">np:O</ta>
            <ta e="T60" id="Seg_2413" s="T59">0.1.h:S v:pred</ta>
            <ta e="T62" id="Seg_2414" s="T61">n:pred</ta>
            <ta e="T63" id="Seg_2415" s="T62">0.3:S cop</ta>
            <ta e="T64" id="Seg_2416" s="T63">np:S</ta>
            <ta e="T65" id="Seg_2417" s="T64">adj:pred</ta>
            <ta e="T66" id="Seg_2418" s="T65">cop</ta>
            <ta e="T68" id="Seg_2419" s="T67">0.3:S v:pred</ta>
            <ta e="T69" id="Seg_2420" s="T68">0.1.h:S v:pred</ta>
            <ta e="T72" id="Seg_2421" s="T71">s:purp</ta>
            <ta e="T74" id="Seg_2422" s="T73">np:S</ta>
            <ta e="T75" id="Seg_2423" s="T74">v:pred</ta>
            <ta e="T76" id="Seg_2424" s="T75">pro.h:S</ta>
            <ta e="T77" id="Seg_2425" s="T76">v:pred</ta>
            <ta e="T78" id="Seg_2426" s="T77">s:purp</ta>
            <ta e="T79" id="Seg_2427" s="T78">np:O</ta>
            <ta e="T80" id="Seg_2428" s="T79">0.1.h:S v:pred</ta>
            <ta e="T85" id="Seg_2429" s="T84">0.1.h:S v:pred</ta>
            <ta e="T87" id="Seg_2430" s="T86">s:purp</ta>
            <ta e="T88" id="Seg_2431" s="T87">np:O</ta>
            <ta e="T89" id="Seg_2432" s="T88">0.1.h:S v:pred</ta>
            <ta e="T91" id="Seg_2433" s="T90">pro.h:S</ta>
            <ta e="T94" id="Seg_2434" s="T93">v:pred</ta>
            <ta e="T95" id="Seg_2435" s="T94">np.h:O</ta>
            <ta e="T97" id="Seg_2436" s="T96">0.2.h:S v:pred</ta>
            <ta e="T98" id="Seg_2437" s="T97">np:S</ta>
            <ta e="T100" id="Seg_2438" s="T99">n:pred</ta>
            <ta e="T101" id="Seg_2439" s="T100">cop</ta>
            <ta e="T103" id="Seg_2440" s="T102">np.h:O</ta>
            <ta e="T104" id="Seg_2441" s="T103">np:S</ta>
            <ta e="T106" id="Seg_2442" s="T105">v:pred</ta>
            <ta e="T107" id="Seg_2443" s="T106">pro.h:S</ta>
            <ta e="T109" id="Seg_2444" s="T108">v:pred</ta>
            <ta e="T111" id="Seg_2445" s="T109">s:purp</ta>
            <ta e="T112" id="Seg_2446" s="T111">0.3.h:S v:pred</ta>
            <ta e="T114" id="Seg_2447" s="T113">np:O</ta>
            <ta e="T115" id="Seg_2448" s="T114">0.1.h:S v:pred</ta>
            <ta e="T117" id="Seg_2449" s="T116">np:O</ta>
            <ta e="T118" id="Seg_2450" s="T117">0.1.h:S v:pred</ta>
            <ta e="T121" id="Seg_2451" s="T120">np:O</ta>
            <ta e="T122" id="Seg_2452" s="T121">0.1.h:S v:pred</ta>
            <ta e="T124" id="Seg_2453" s="T123">np:O</ta>
            <ta e="T125" id="Seg_2454" s="T124">0.2.h:S v:pred</ta>
            <ta e="T127" id="Seg_2455" s="T125">s:purp</ta>
            <ta e="T129" id="Seg_2456" s="T128">np:O</ta>
            <ta e="T130" id="Seg_2457" s="T129">0.2.h:S v:pred</ta>
            <ta e="T132" id="Seg_2458" s="T130">s:purp</ta>
            <ta e="T133" id="Seg_2459" s="T132">np:S</ta>
            <ta e="T134" id="Seg_2460" s="T133">v:pred</ta>
            <ta e="T136" id="Seg_2461" s="T135">0.3.h:S v:pred</ta>
            <ta e="T138" id="Seg_2462" s="T137">np:S</ta>
            <ta e="T139" id="Seg_2463" s="T138">n:pred</ta>
            <ta e="T141" id="Seg_2464" s="T140">cop</ta>
            <ta e="T143" id="Seg_2465" s="T142">0.2.h:S 0.3:O v:pred</ta>
            <ta e="T144" id="Seg_2466" s="T143">np:S</ta>
            <ta e="T146" id="Seg_2467" s="T145">v:pred</ta>
            <ta e="T147" id="Seg_2468" s="T146">np:S</ta>
            <ta e="T148" id="Seg_2469" s="T147">v:pred</ta>
            <ta e="T152" id="Seg_2470" s="T151">ptcl:pred</ta>
            <ta e="T153" id="Seg_2471" s="T152">v:O</ta>
            <ta e="T155" id="Seg_2472" s="T154">np:O</ta>
            <ta e="T156" id="Seg_2473" s="T155">0.1.h:S v:pred</ta>
            <ta e="T157" id="Seg_2474" s="T156">v:pred</ta>
            <ta e="T158" id="Seg_2475" s="T157">np:S</ta>
            <ta e="T159" id="Seg_2476" s="T158">pro.h:S</ta>
            <ta e="T161" id="Seg_2477" s="T160">v:pred</ta>
            <ta e="T162" id="Seg_2478" s="T161">s:purp</ta>
            <ta e="T164" id="Seg_2479" s="T163">np:S</ta>
            <ta e="T167" id="Seg_2480" s="T166">v:pred</ta>
            <ta e="T169" id="Seg_2481" s="T168">np:S</ta>
            <ta e="T170" id="Seg_2482" s="T169">v:pred</ta>
            <ta e="T173" id="Seg_2483" s="T172">np:S</ta>
            <ta e="T174" id="Seg_2484" s="T173">np:S</ta>
            <ta e="T176" id="Seg_2485" s="T175">np:S</ta>
            <ta e="T177" id="Seg_2486" s="T176">np:S</ta>
            <ta e="T178" id="Seg_2487" s="T177">np:S</ta>
            <ta e="T179" id="Seg_2488" s="T178">np:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T47" id="Seg_2489" s="T46">RUS:cult</ta>
            <ta e="T112" id="Seg_2490" s="T111">RUS:cult</ta>
            <ta e="T152" id="Seg_2491" s="T151">RUS:mod</ta>
            <ta e="T163" id="Seg_2492" s="T162">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_2493" s="T0">Я родилась в Часельке.</ta>
            <ta e="T6" id="Seg_2494" s="T3">Там и жила всё время.</ta>
            <ta e="T8" id="Seg_2495" s="T6">Там и рыбачила.</ta>
            <ta e="T12" id="Seg_2496" s="T8">Ещё мы охотимся, ездим охотиться.</ta>
            <ta e="T14" id="Seg_2497" s="T12">Ездим белковать.</ta>
            <ta e="T21" id="Seg_2498" s="T14">Белок убиваем, лисиц убиваем, лесных оленей убиваем.</ta>
            <ta e="T25" id="Seg_2499" s="T21">В лесу оленей много.</ta>
            <ta e="T31" id="Seg_2500" s="T25">В тундре есть олени, лесные олени.</ta>
            <ta e="T36" id="Seg_2501" s="T31">Летом сено косим для коров.</ta>
            <ta e="T39" id="Seg_2502" s="T36">Коров у нас много.</ta>
            <ta e="T43" id="Seg_2503" s="T39">Лошадей у нас тоже много.</ta>
            <ta e="T47" id="Seg_2504" s="T43">Сена он накосил [?] 4 тонны.</ta>
            <ta e="T53" id="Seg_2505" s="T47">Сена мало, зимой не хватает.</ta>
            <ta e="T57" id="Seg_2506" s="T53">Мы дрова заготовили для рыбкопа.</ta>
            <ta e="T60" id="Seg_2507" s="T57">Мы 30 кубометров нарубили.</ta>
            <ta e="T66" id="Seg_2508" s="T60">Это так мало, зима долгая.</ta>
            <ta e="T68" id="Seg_2509" s="T66">[Этого] не хватает.</ta>
            <ta e="T72" id="Seg_2510" s="T68">Мы поедем в другое место заготовлять дрова.</ta>
            <ta e="T75" id="Seg_2511" s="T72">Здесь дров нет.</ta>
            <ta e="T78" id="Seg_2512" s="T75">Мы поедем рыбачить.</ta>
            <ta e="T80" id="Seg_2513" s="T78">Сети возьмём.</ta>
            <ta e="T83" id="Seg_2514" s="T80">Сетей 15 [штук].</ta>
            <ta e="T87" id="Seg_2515" s="T83">Потом перекочуем в лес рыбачить.</ta>
            <ta e="T94" id="Seg_2516" s="T87">Лодки скоро нагрузим, ты не уходи.</ta>
            <ta e="T97" id="Seg_2517" s="T94">Детей посади [в лодку].</ta>
            <ta e="T102" id="Seg_2518" s="T97">Комаров так много в лесу.</ta>
            <ta e="T106" id="Seg_2519" s="T102">Сына комары совсем заедают.</ta>
            <ta e="T115" id="Seg_2520" s="T106">[Мы/я?] сейчас пойду корову кормить, подою и навоз уберу.</ta>
            <ta e="T118" id="Seg_2521" s="T115">Потом сено раздам.</ta>
            <ta e="T123" id="Seg_2522" s="T118">Потом ещё молоко отвезу в столовую.</ta>
            <ta e="T127" id="Seg_2523" s="T123">Лошадь запрягай, чтобы отвезти молоко.</ta>
            <ta e="T134" id="Seg_2524" s="T127">Ещё сена привези, чтобы коров кормить, сена нет.</ta>
            <ta e="T136" id="Seg_2525" s="T134">Откуда привезёт? </ta>
            <ta e="T141" id="Seg_2526" s="T136">Вон там есть сено на огороде.</ta>
            <ta e="T149" id="Seg_2527" s="T141">Оттуда принеси, коровы стоят голодные, у них молока не будет вечером.</ta>
            <ta e="T153" id="Seg_2528" s="T149">Ещё воды надо привезти.</ta>
            <ta e="T158" id="Seg_2529" s="T153">Вечером нам надо коров поить, [а] воды нет.</ta>
            <ta e="T167" id="Seg_2530" s="T158">Я сейчас пойду по ягоды, а ягод в лесу так [много].</ta>
            <ta e="T170" id="Seg_2531" s="T167">Какие ягоды [есть]?</ta>
            <ta e="T179" id="Seg_2532" s="T170">В лесу [есть] брусника, черника, голубика, морошка, чёрная смородина, красная смородина.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_2533" s="T0">I was born in Chaselka.</ta>
            <ta e="T6" id="Seg_2534" s="T3">I lived here all the time.</ta>
            <ta e="T8" id="Seg_2535" s="T6">I fished here.</ta>
            <ta e="T12" id="Seg_2536" s="T8">We also go hunting.</ta>
            <ta e="T14" id="Seg_2537" s="T12">We hunt squirrels too.</ta>
            <ta e="T21" id="Seg_2538" s="T14">We kill squirrels, we kill foxes, we kill forest reindeer.</ta>
            <ta e="T25" id="Seg_2539" s="T21">There are many reindeer in the forest.</ta>
            <ta e="T31" id="Seg_2540" s="T25">There are reindeer in Tundra, forest reindeer.</ta>
            <ta e="T36" id="Seg_2541" s="T31">In summer we chop grass for cows.</ta>
            <ta e="T39" id="Seg_2542" s="T36">We've got many cows.</ta>
            <ta e="T43" id="Seg_2543" s="T39">We've also got a lot of horses.</ta>
            <ta e="T47" id="Seg_2544" s="T43">He [?] chopped 4 tonnes of grass.</ta>
            <ta e="T53" id="Seg_2545" s="T47">There is few hay, it won't be enough for the winter.</ta>
            <ta e="T57" id="Seg_2546" s="T53">We prepared firewood for the fish cooperative.</ta>
            <ta e="T60" id="Seg_2547" s="T57">We've chopped 30 cubic meter.</ta>
            <ta e="T66" id="Seg_2548" s="T60">It is very few, the winter is long.</ta>
            <ta e="T68" id="Seg_2549" s="T66">It's not enough.</ta>
            <ta e="T72" id="Seg_2550" s="T68">We will go to another place to get firewood.</ta>
            <ta e="T75" id="Seg_2551" s="T72">There is no firewood here.</ta>
            <ta e="T78" id="Seg_2552" s="T75">We will go fishing.</ta>
            <ta e="T80" id="Seg_2553" s="T78">We will take sweep-nets.</ta>
            <ta e="T83" id="Seg_2554" s="T80">15 fischnets.</ta>
            <ta e="T87" id="Seg_2555" s="T83">Then we will nomadize to the forest to fish.</ta>
            <ta e="T94" id="Seg_2556" s="T87">We will load our boats soon, don't go away.</ta>
            <ta e="T97" id="Seg_2557" s="T94">Bring the children [into the boat].</ta>
            <ta e="T102" id="Seg_2558" s="T97">There are so many mosquitoes in the forest.</ta>
            <ta e="T106" id="Seg_2559" s="T102">Mosquitoes stung our son all over.</ta>
            <ta e="T115" id="Seg_2560" s="T106">[We/I?] will go now to feed the cow, will milk [it] and shovel the manure out.</ta>
            <ta e="T118" id="Seg_2561" s="T115">Then I'll distribute the hay.</ta>
            <ta e="T123" id="Seg_2562" s="T118">Then I will also bring milk to the canteen.</ta>
            <ta e="T127" id="Seg_2563" s="T123">Harness the horse to take off the milk.</ta>
            <ta e="T134" id="Seg_2564" s="T127">Bring also more hay to feed the cows, there is no any.</ta>
            <ta e="T136" id="Seg_2565" s="T134">Where from?</ta>
            <ta e="T141" id="Seg_2566" s="T136">Over there in the vegetable garden there is hay.</ta>
            <ta e="T149" id="Seg_2567" s="T141">Bring from there, the cows are hungry, there won't be milk in the evening.</ta>
            <ta e="T153" id="Seg_2568" s="T149">We should also bring water.</ta>
            <ta e="T158" id="Seg_2569" s="T153">In the evening I have to water the cows, there is no water.</ta>
            <ta e="T167" id="Seg_2570" s="T158">I will go now to pick berries, there are many berries in the forest.</ta>
            <ta e="T170" id="Seg_2571" s="T167">What kind of berries?</ta>
            <ta e="T179" id="Seg_2572" s="T170">There is cranberry, bilberry, blueberry, cloudberry, blackcurrant, redcurrant in the forest.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_2573" s="T0">Ich wurde in Chaselka geboren.</ta>
            <ta e="T6" id="Seg_2574" s="T3">Ich lebte schon immer hier.</ta>
            <ta e="T8" id="Seg_2575" s="T6">Ich fischte hier.</ta>
            <ta e="T12" id="Seg_2576" s="T8">Wir gehen auch jagen, wir jagen wilde Tiere.</ta>
            <ta e="T14" id="Seg_2577" s="T12">Wir gehen Eichhörnchen jagen.</ta>
            <ta e="T21" id="Seg_2578" s="T14">Wir töten Eichhörnchen, wir töten Füchse, wir töten Waldrentiere.</ta>
            <ta e="T25" id="Seg_2579" s="T21">Im Wald gibt es viele Rentiere.</ta>
            <ta e="T31" id="Seg_2580" s="T25">In der Tundra gibt es Rentiere, Waldrentiere.</ta>
            <ta e="T36" id="Seg_2581" s="T31">Im Sommer mähen wir Gras für die Kühe.</ta>
            <ta e="T39" id="Seg_2582" s="T36">Wir haben viele Kühe.</ta>
            <ta e="T43" id="Seg_2583" s="T39">Wir haben auch viele Pferde.</ta>
            <ta e="T47" id="Seg_2584" s="T43">Er [?] hat vier Tonnen Gras gemäht.</ta>
            <ta e="T53" id="Seg_2585" s="T47">Es gibt wenig Heu, das reicht nicht für den Winter.</ta>
            <ta e="T57" id="Seg_2586" s="T53">Wir haben Brennholz für die Fischereigenossenschaft gemacht.</ta>
            <ta e="T60" id="Seg_2587" s="T57">Wir haben 30 Kubikmeter gehackt.</ta>
            <ta e="T66" id="Seg_2588" s="T60">Das ist sehr wenig, der Winter ist lang.</ta>
            <ta e="T68" id="Seg_2589" s="T66">Es reicht nicht.</ta>
            <ta e="T72" id="Seg_2590" s="T68">Wir fahren woanders hin, um Brennholz zu machen.</ta>
            <ta e="T75" id="Seg_2591" s="T72">Hier gibt es kein Brennholz.</ta>
            <ta e="T78" id="Seg_2592" s="T75">Wir gehen fischen. </ta>
            <ta e="T80" id="Seg_2593" s="T78">Wir nehmen Ringwaden mit.</ta>
            <ta e="T83" id="Seg_2594" s="T80">15 Ringwaden.</ta>
            <ta e="T87" id="Seg_2595" s="T83">Dann ziehen wir zum Fischen in den Wald.</ta>
            <ta e="T94" id="Seg_2596" s="T87">Wir werden bald die Boote beladen, geh nicht weg.</ta>
            <ta e="T97" id="Seg_2597" s="T94">Setz die Kinder [ins Boot].</ta>
            <ta e="T102" id="Seg_2598" s="T97">Es gibt so viele Mücken im Wald.</ta>
            <ta e="T106" id="Seg_2599" s="T102">Die Mücken haben den Sohn überall gestochen.</ta>
            <ta e="T115" id="Seg_2600" s="T106">[Wir/ich] gehe[-n] jetzt die Kuh füttern, melke [sie] und miste aus.</ta>
            <ta e="T118" id="Seg_2601" s="T115">Dann verteile ich das Heu.</ta>
            <ta e="T123" id="Seg_2602" s="T118">Dann bringe ich noch meine Milch in die Kantine.</ta>
            <ta e="T127" id="Seg_2603" s="T123">Spann das Pferd an, um die Milch wegzubringen.</ta>
            <ta e="T134" id="Seg_2604" s="T127">Bring auch noch mehr Heu, um die Kühe zu füttern, es ist kein Heu da.</ta>
            <ta e="T136" id="Seg_2605" s="T134">Woher bringt er/sie es?</ta>
            <ta e="T141" id="Seg_2606" s="T136">Das Heu ist im Gemüsegarten.</ta>
            <ta e="T149" id="Seg_2607" s="T141">Bring es von dort, die Kühe sind hungrig, sie werden am Abend keine Milch geben.</ta>
            <ta e="T153" id="Seg_2608" s="T149">Wir müssen auch noch Wasser holen.</ta>
            <ta e="T158" id="Seg_2609" s="T153">Am Abend müssen wir beide die Kühe tränken, [aber] es ist kein Wasser da.</ta>
            <ta e="T167" id="Seg_2610" s="T158">Ich gehe jetzt Beeren pflücken, und es gibt viele Beeren im Wald.</ta>
            <ta e="T170" id="Seg_2611" s="T167">Was für Beeren?</ta>
            <ta e="T179" id="Seg_2612" s="T170">Im Wald gibt es Preiselbeeren, Heidelbeeren, Blaubeeren, Moltebeeren, schwarze und rote Jahannisbeeren.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_2613" s="T0">я родилась в Чосальке.</ta>
            <ta e="T6" id="Seg_2614" s="T3">там и жила все время.</ta>
            <ta e="T8" id="Seg_2615" s="T6">там и рыбачила</ta>
            <ta e="T12" id="Seg_2616" s="T8">и охотились. ездили охотиться.</ta>
            <ta e="T14" id="Seg_2617" s="T12">ездили белковать.</ta>
            <ta e="T21" id="Seg_2618" s="T14">белки мы убиваем лисицу убиваем. дикий (тихий) оленя убиваем.</ta>
            <ta e="T25" id="Seg_2619" s="T21">в лесу оленей много (полно)</ta>
            <ta e="T31" id="Seg_2620" s="T25">в тундрах олени там есть. дикий олень.</ta>
            <ta e="T36" id="Seg_2621" s="T31">сено косили лето для коровы.</ta>
            <ta e="T39" id="Seg_2622" s="T36">коров много</ta>
            <ta e="T43" id="Seg_2623" s="T39">лошадей тоже еть много</ta>
            <ta e="T47" id="Seg_2624" s="T43">сена косили 4 тонны.</ta>
            <ta e="T53" id="Seg_2625" s="T47">сена все равно мало, на зиму не хватит.</ta>
            <ta e="T57" id="Seg_2626" s="T53">мы дрова готовили для рыбкопа.</ta>
            <ta e="T60" id="Seg_2627" s="T57">30 кубаметров готовили (рубили) (мы).</ta>
            <ta e="T66" id="Seg_2628" s="T60">это мало зима длинная.</ta>
            <ta e="T68" id="Seg_2629" s="T66">не хватит.</ta>
            <ta e="T72" id="Seg_2630" s="T68">поеедем в другой место дрова готовить.</ta>
            <ta e="T75" id="Seg_2631" s="T72">тут дров нет.</ta>
            <ta e="T78" id="Seg_2632" s="T75">мы поедем рыбачить.</ta>
            <ta e="T80" id="Seg_2633" s="T78">сети получим.</ta>
            <ta e="T83" id="Seg_2634" s="T80">сетей 15 штук.</ta>
            <ta e="T87" id="Seg_2635" s="T83">потом аргишим. в лет. рыбачить.</ta>
            <ta e="T94" id="Seg_2636" s="T87">лодки сейчас погрузим скоро ты куда-нибудь не иди.</ta>
            <ta e="T97" id="Seg_2637" s="T94">сына посади в лодку.</ta>
            <ta e="T102" id="Seg_2638" s="T97">комары так [сильно] много в лесу.</ta>
            <ta e="T106" id="Seg_2639" s="T102">сына комары всего съели.</ta>
            <ta e="T115" id="Seg_2640" s="T106">мы сейчас пойду коров кормить доить сейчас навоз чистить.</ta>
            <ta e="T118" id="Seg_2641" s="T115">потом сено буду раздавать.</ta>
            <ta e="T123" id="Seg_2642" s="T118">потом молоко повезу в столовую.</ta>
            <ta e="T127" id="Seg_2643" s="T123">лошадь запрягай свезем молоко.</ta>
            <ta e="T134" id="Seg_2644" s="T127">еще сена привези коров кормить сена нет.</ta>
            <ta e="T136" id="Seg_2645" s="T134">откуда привезу. </ta>
            <ta e="T141" id="Seg_2646" s="T136">вот сено на огороде.</ta>
            <ta e="T149" id="Seg_2647" s="T141">оттуда привези коровам кормить стоят голодом молока не будет вечером.</ta>
            <ta e="T153" id="Seg_2648" s="T149">и воды надо привезти.</ta>
            <ta e="T158" id="Seg_2649" s="T153">вечером коров поить нет воды.</ta>
            <ta e="T167" id="Seg_2650" s="T158">я сейчас пойду за ягодой, а ягод в лесу так много.</ta>
            <ta e="T170" id="Seg_2651" s="T167">какие ягоды есть.</ta>
            <ta e="T179" id="Seg_2652" s="T170"> ‎‎в лесу брусника черника голубица, морошка смородина красная смородина.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T47" id="Seg_2653" s="T43">[OSV:] The verbal form "pačʼ-äl-sɔː-n" (cut-MULO-PST-3SG.S) is used, though according to the previous context the 1st Person Plural is supposed to be used.</ta>
            <ta e="T53" id="Seg_2654" s="T47">[OSV:] The form "kətə" has been edited into "kə-t" (winter-ADV.LOC).</ta>
            <ta e="T66" id="Seg_2655" s="T60">[OSV:] "naːšʼa" has been edited into "našʼak"</ta>
            <ta e="T97" id="Seg_2656" s="T94">[OSV:] The word "qärqa" has been edited into "käralʼ".</ta>
            <ta e="T106" id="Seg_2657" s="T102">[OSV:] "koptɨkɔːlɨk" - "at all". </ta>
            <ta e="T115" id="Seg_2658" s="T106">[OSV:] The verbal forms "qän-ta-k" (leave-FUT-1SG.S), "tajekn-ɛnta" (milk-FUT.3SG.S) and "qatal-tɛnta-p" (sweep-FUT-1SG.O) do not agree with the person and number of the subject "mej" (we.PL.NOM).</ta>
            <ta e="T123" id="Seg_2659" s="T118">[OSV:] The form "qən-t-ɛnta-p" (leave-TR-FUT-1SG.O) would be more correct here. </ta>
            <ta e="T136" id="Seg_2660" s="T134">[OSV:] The form "taːt-ɛnta-m" (bring-FUT-1SG.O) would be more correct here.</ta>
            <ta e="T170" id="Seg_2661" s="T167">[OSV:] Possible interpretation of the verbal form is "ɛj-sa-^0" (be-PST-3SG.S). </ta>
            <ta e="T179" id="Seg_2662" s="T170">[OSV:] "nʼar(qɨ) topɨr" - "cranberry", "kotɨlʼ topɨr" - "blueberry".</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
