<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>SMV_196X_Verse_nar</transcription-name>
         <referenced-file url="SMV_196X_Verse_nar.wav" />
         <referenced-file url="SMV_196X_Verse_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">SMV_196X_Verse_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">22</ud-information>
            <ud-information attribute-name="# HIAT:w">15</ud-information>
            <ud-information attribute-name="# e">15</ud-information>
            <ud-information attribute-name="# HIAT:u">4</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SMV">
            <abbreviation>SMV</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.04" type="appl" />
         <tli id="T1" time="1.027" type="appl" />
         <tli id="T2" time="2.013" type="appl" />
         <tli id="T3" time="3.0" type="appl" />
         <tli id="T4" time="3.665" type="appl" />
         <tli id="T5" time="4.296" type="appl" />
         <tli id="T6" time="4.928" type="appl" />
         <tli id="T7" time="5.56" type="appl" />
         <tli id="T8" time="6.657" type="appl" />
         <tli id="T9" time="7.534" type="appl" />
         <tli id="T10" time="8.412" type="appl" />
         <tli id="T11" time="8.905" type="appl" />
         <tli id="T12" time="9.319" type="appl" />
         <tli id="T13" time="9.732" type="appl" />
         <tli id="T14" time="10.146" type="appl" />
         <tli id="T15" time="10.559" type="appl" />
         <tli id="T16" time="10.65" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SMV"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T15" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Saraj</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">təpɨt</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">qälimpɔːtɨt</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Kun</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">aj</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">znamja</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">wəčʼimpɔːtɨt</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">İjat</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">qəmpɨla</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">šʼolqumɨt</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_41" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">Školantɨ</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_45" n="HIAT:ip">(</nts>
                  <ts e="T12" id="Seg_47" n="HIAT:w" s="T11">nɔːt</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">na</ts>
                  <nts id="Seg_51" n="HIAT:ip">/</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_54" n="HIAT:w" s="T13">nɔːtna</ts>
                  <nts id="Seg_55" n="HIAT:ip">)</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_58" n="HIAT:w" s="T14">meːkɨnʼi</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T15" id="Seg_61" n="sc" s="T0">
               <ts e="T1" id="Seg_63" n="e" s="T0">Saraj </ts>
               <ts e="T2" id="Seg_65" n="e" s="T1">təpɨt </ts>
               <ts e="T3" id="Seg_67" n="e" s="T2">qälimpɔːtɨt. </ts>
               <ts e="T4" id="Seg_69" n="e" s="T3">Kun </ts>
               <ts e="T5" id="Seg_71" n="e" s="T4">aj </ts>
               <ts e="T6" id="Seg_73" n="e" s="T5">znamja </ts>
               <ts e="T7" id="Seg_75" n="e" s="T6">wəčʼimpɔːtɨt. </ts>
               <ts e="T8" id="Seg_77" n="e" s="T7">İjat </ts>
               <ts e="T9" id="Seg_79" n="e" s="T8">qəmpɨla </ts>
               <ts e="T10" id="Seg_81" n="e" s="T9">šʼolqumɨt. </ts>
               <ts e="T11" id="Seg_83" n="e" s="T10">Školantɨ </ts>
               <ts e="T12" id="Seg_85" n="e" s="T11">(nɔːt </ts>
               <ts e="T13" id="Seg_87" n="e" s="T12">na/ </ts>
               <ts e="T14" id="Seg_89" n="e" s="T13">nɔːtna) </ts>
               <ts e="T15" id="Seg_91" n="e" s="T14">meːkɨnʼi. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_92" s="T0">SMV_196X_Verse_nar.001 (001)</ta>
            <ta e="T7" id="Seg_93" s="T3">SMV_196X_Verse_nar.002 (002)</ta>
            <ta e="T10" id="Seg_94" s="T7">SMV_196X_Verse_nar.003 (003)</ta>
            <ta e="T15" id="Seg_95" s="T10">SMV_196X_Verse_nar.004 (004)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_96" s="T0">šaraj təpɨt qälimpɔːtɨn</ta>
            <ta e="T7" id="Seg_97" s="T3">kunaj znamja wəčʼimpɔːtɨt</ta>
            <ta e="T10" id="Seg_98" s="T7">ijat qəmpɨla šʼol qumɨt</ta>
            <ta e="T15" id="Seg_99" s="T10">školantɨ nɔːt na meːkɨnʼi</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_100" s="T0">Saraj təpɨt qälimpɔːtɨt. </ta>
            <ta e="T7" id="Seg_101" s="T3">Kun aj znamja wəčʼimpɔːtɨt. </ta>
            <ta e="T10" id="Seg_102" s="T7">İjat qəmpɨla šʼolqumɨt. </ta>
            <ta e="T15" id="Seg_103" s="T10">Školantɨ (nɔːt na/ nɔːtna) meːkɨnʼi. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_104" s="T0">sara-j</ta>
            <ta e="T2" id="Seg_105" s="T1">təp-ɨ-t</ta>
            <ta e="T3" id="Seg_106" s="T2">qäli-mpɔː-tɨt</ta>
            <ta e="T4" id="Seg_107" s="T3">kun</ta>
            <ta e="T5" id="Seg_108" s="T4">aj</ta>
            <ta e="T6" id="Seg_109" s="T5">znamja</ta>
            <ta e="T7" id="Seg_110" s="T6">wəčʼi-mpɔː-tɨt</ta>
            <ta e="T8" id="Seg_111" s="T7">ija-t</ta>
            <ta e="T9" id="Seg_112" s="T8">qə-mpɨ-la</ta>
            <ta e="T10" id="Seg_113" s="T9">šʼolqum-ɨ-t</ta>
            <ta e="T11" id="Seg_114" s="T10">škola-ntɨ</ta>
            <ta e="T12" id="Seg_115" s="T11">nɔːt</ta>
            <ta e="T13" id="Seg_116" s="T12">na</ta>
            <ta e="T14" id="Seg_117" s="T13">nɔːtna</ta>
            <ta e="T15" id="Seg_118" s="T14">meː-kɨnʼi</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_119" s="T0">šarɨ-lʼ</ta>
            <ta e="T2" id="Seg_120" s="T1">təp-ɨ-t</ta>
            <ta e="T3" id="Seg_121" s="T2">qäl-mpɨ-tɨt</ta>
            <ta e="T4" id="Seg_122" s="T3">kun</ta>
            <ta e="T5" id="Seg_123" s="T4">aj</ta>
            <ta e="T6" id="Seg_124" s="T5">znamja</ta>
            <ta e="T7" id="Seg_125" s="T6">wəčʼčʼɨ-mpɨ-tɨt</ta>
            <ta e="T8" id="Seg_126" s="T7">iːja-t</ta>
            <ta e="T9" id="Seg_127" s="T8">qən-mpɨ-lä</ta>
            <ta e="T10" id="Seg_128" s="T9">šölʼqum-ɨ-t</ta>
            <ta e="T11" id="Seg_129" s="T10">škola-ntɨ</ta>
            <ta e="T12" id="Seg_130" s="T11">nɔːtɨ</ta>
            <ta e="T13" id="Seg_131" s="T12">na</ta>
            <ta e="T14" id="Seg_132" s="T13">nɔːtna</ta>
            <ta e="T15" id="Seg_133" s="T14">meː-nkinı</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_134" s="T0">unanimous-ADJZ</ta>
            <ta e="T2" id="Seg_135" s="T1">(s)he-EP-PL.[NOM]</ta>
            <ta e="T3" id="Seg_136" s="T2">go-DUR-3PL</ta>
            <ta e="T4" id="Seg_137" s="T3">where</ta>
            <ta e="T5" id="Seg_138" s="T4">and</ta>
            <ta e="T6" id="Seg_139" s="T5">flag.[NOM]</ta>
            <ta e="T7" id="Seg_140" s="T6">lift-DUR-3PL</ta>
            <ta e="T8" id="Seg_141" s="T7">child-PL.[NOM]</ta>
            <ta e="T9" id="Seg_142" s="T8">leave-DUR-CVB</ta>
            <ta e="T10" id="Seg_143" s="T9">Selkup-EP-PL.[NOM]</ta>
            <ta e="T11" id="Seg_144" s="T10">school-ILL</ta>
            <ta e="T12" id="Seg_145" s="T11">then</ta>
            <ta e="T13" id="Seg_146" s="T12">here</ta>
            <ta e="T14" id="Seg_147" s="T13">one.needs</ta>
            <ta e="T15" id="Seg_148" s="T14">we.PL-ALL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_149" s="T0">дружный-ADJZ</ta>
            <ta e="T2" id="Seg_150" s="T1">он(а)-EP-PL.[NOM]</ta>
            <ta e="T3" id="Seg_151" s="T2">ходить-DUR-3PL</ta>
            <ta e="T4" id="Seg_152" s="T3">где</ta>
            <ta e="T5" id="Seg_153" s="T4">и</ta>
            <ta e="T6" id="Seg_154" s="T5">знамя.[NOM]</ta>
            <ta e="T7" id="Seg_155" s="T6">поднять-DUR-3PL</ta>
            <ta e="T8" id="Seg_156" s="T7">ребенок-PL.[NOM]</ta>
            <ta e="T9" id="Seg_157" s="T8">отправиться-DUR-CVB</ta>
            <ta e="T10" id="Seg_158" s="T9">селькуп-EP-PL.[NOM]</ta>
            <ta e="T11" id="Seg_159" s="T10">школа-ILL</ta>
            <ta e="T12" id="Seg_160" s="T11">затем</ta>
            <ta e="T13" id="Seg_161" s="T12">вот</ta>
            <ta e="T14" id="Seg_162" s="T13">нужно</ta>
            <ta e="T15" id="Seg_163" s="T14">мы.PL-ALL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_164" s="T0">adj-n&gt;adj</ta>
            <ta e="T2" id="Seg_165" s="T1">pers-n:ins-n:num-n:case</ta>
            <ta e="T3" id="Seg_166" s="T2">v-v&gt;v-v:pn</ta>
            <ta e="T4" id="Seg_167" s="T3">interrog</ta>
            <ta e="T5" id="Seg_168" s="T4">conj</ta>
            <ta e="T6" id="Seg_169" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_170" s="T6">v-v&gt;v-v:pn</ta>
            <ta e="T8" id="Seg_171" s="T7">n-n:num-n:case</ta>
            <ta e="T9" id="Seg_172" s="T8">v-v&gt;v-v&gt;adv</ta>
            <ta e="T10" id="Seg_173" s="T9">n-n:ins-n:num-n:case</ta>
            <ta e="T11" id="Seg_174" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_175" s="T11">adv</ta>
            <ta e="T13" id="Seg_176" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_177" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_178" s="T14">pers-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_179" s="T0">adj</ta>
            <ta e="T2" id="Seg_180" s="T1">pers</ta>
            <ta e="T3" id="Seg_181" s="T2">v</ta>
            <ta e="T4" id="Seg_182" s="T3">interrog</ta>
            <ta e="T5" id="Seg_183" s="T4">conj</ta>
            <ta e="T6" id="Seg_184" s="T5">n</ta>
            <ta e="T7" id="Seg_185" s="T6">v</ta>
            <ta e="T8" id="Seg_186" s="T7">n</ta>
            <ta e="T9" id="Seg_187" s="T8">adv</ta>
            <ta e="T10" id="Seg_188" s="T9">n</ta>
            <ta e="T11" id="Seg_189" s="T10">n</ta>
            <ta e="T12" id="Seg_190" s="T11">adv</ta>
            <ta e="T13" id="Seg_191" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_192" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_193" s="T14">pers</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_194" s="T1">pro.h:A</ta>
            <ta e="T4" id="Seg_195" s="T3">pro:L</ta>
            <ta e="T6" id="Seg_196" s="T5">np:Th</ta>
            <ta e="T7" id="Seg_197" s="T6">0.3.h:A</ta>
            <ta e="T8" id="Seg_198" s="T7">np.h:A</ta>
            <ta e="T11" id="Seg_199" s="T10">np:G</ta>
            <ta e="T12" id="Seg_200" s="T11">adv:Time</ta>
            <ta e="T15" id="Seg_201" s="T14">pro.h:G</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_202" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_203" s="T2">v:pred</ta>
            <ta e="T6" id="Seg_204" s="T5">np:O</ta>
            <ta e="T7" id="Seg_205" s="T6">0.3.h:S v:pred</ta>
            <ta e="T9" id="Seg_206" s="T7">s:adv</ta>
            <ta e="T14" id="Seg_207" s="T13">ptcl:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T6" id="Seg_208" s="T5">RUS:cult</ta>
            <ta e="T11" id="Seg_209" s="T10">RUS:cult</ta>
            <ta e="T14" id="Seg_210" s="T13">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_211" s="T0">Дружно они шагают.</ta>
            <ta e="T7" id="Seg_212" s="T3">Где знамя поднимают.</ta>
            <ta e="T10" id="Seg_213" s="T7">Ребята идя, селькупы.</ta>
            <ta e="T15" id="Seg_214" s="T10">В школу нужно/потом вот(?) к нам.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_215" s="T0">They are marching all together.</ta>
            <ta e="T7" id="Seg_216" s="T3">Where they raise the flag.</ta>
            <ta e="T10" id="Seg_217" s="T7">The children [are] walking, the Selkups.</ta>
            <ta e="T15" id="Seg_218" s="T10">Then they [go]/They need [to go](?) to school, to us.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_219" s="T0">Sie marschieren alle zusammen</ta>
            <ta e="T7" id="Seg_220" s="T3">Wo sie die Fahne hissen.</ta>
            <ta e="T10" id="Seg_221" s="T7">The Kinder gehen, die Selkupen.</ta>
            <ta e="T15" id="Seg_222" s="T10">Dann [gehen] sie/Sie müssen zur Schule, zu uns [gehen].</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_223" s="T0"> ‎‎дружно они шагают</ta>
            <ta e="T7" id="Seg_224" s="T3">где знамя поднимают</ta>
            <ta e="T10" id="Seg_225" s="T7">ребята идут селькупы</ta>
            <ta e="T15" id="Seg_226" s="T10">в школу к нам</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T10" id="Seg_227" s="T7">[BrM:] NB converb instead of expected finite form.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
