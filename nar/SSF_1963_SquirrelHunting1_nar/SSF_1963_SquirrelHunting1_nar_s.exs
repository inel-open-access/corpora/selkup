<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>SSF_196X_SquirrelHunting1_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">SSF_1963_SquirrelHunting1_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">167</ud-information>
            <ud-information attribute-name="# HIAT:w">130</ud-information>
            <ud-information attribute-name="# e">130</ud-information>
            <ud-information attribute-name="# HIAT:u">36</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SSF">
            <abbreviation>SSF</abbreviation>
            <sex value="m" />
            <languages-used>
               <language lang="sel" />
            </languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SSF"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T131" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">qwässaŋ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">qɨndabɨɣɨnnɨ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">maːdʼiŋbɨlʼlʼe</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">i</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">maɣɨlʼǯembesaŋ</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">wättoɣɨn</ts>
                  <nts id="Seg_26" n="HIAT:ip">:</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">qaina</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">qum</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_35" n="HIAT:w" s="T10">tʼäŋgus</ts>
                  <nts id="Seg_36" n="HIAT:ip">.</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_39" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">man</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">okonäŋ</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">äːsan</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_51" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">qainä</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">tʼäŋus</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">massä</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_63" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">mazɨm</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">nʼünʼikasɨŋ</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">üt</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">serbazɨt</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_78" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">man</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">wattoɣänä</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">qonǯersaw</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">surɨm</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">nagur</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_96" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">man</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">kɨkɨzaŋ</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">täpɨlannan</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">kunɨgu</ts>
                  <nts id="Seg_108" n="HIAT:ip">.</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_111" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">no</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_116" n="HIAT:w" s="T31">meŋa</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">prišlosʼ</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">tʼačegu</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">täpɨlam</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">tʼülʼdisäsä</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_132" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_134" n="HIAT:w" s="T36">no</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_137" n="HIAT:w" s="T37">man</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_140" n="HIAT:w" s="T38">qaimnas</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_143" n="HIAT:w" s="T39">qwäsaŋ</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_146" n="HIAT:w" s="T40">täpɨlam</ts>
                  <nts id="Seg_147" n="HIAT:ip">.</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_150" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_152" n="HIAT:w" s="T41">no</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_155" n="HIAT:w" s="T42">surɨla</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_158" n="HIAT:w" s="T43">wes</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_161" n="HIAT:w" s="T44">qwäŋɨlusadɨt</ts>
                  <nts id="Seg_162" n="HIAT:ip">.</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_165" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_167" n="HIAT:w" s="T45">man</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_170" n="HIAT:w" s="T46">okonä</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_173" n="HIAT:w" s="T47">qalɨzaŋ</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_176" n="HIAT:w" s="T48">tün</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_179" n="HIAT:w" s="T49">doqqän</ts>
                  <nts id="Seg_180" n="HIAT:ip">.</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_183" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_185" n="HIAT:w" s="T50">man</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_188" n="HIAT:w" s="T51">oqqoronä</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_191" n="HIAT:w" s="T52">qaːlɨzan</ts>
                  <nts id="Seg_192" n="HIAT:ip">.</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_195" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_197" n="HIAT:w" s="T53">kutäna</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_200" n="HIAT:w" s="T54">qajna</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_203" n="HIAT:w" s="T55">tʼäŋus</ts>
                  <nts id="Seg_204" n="HIAT:ip">.</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_207" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_209" n="HIAT:w" s="T56">man</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_212" n="HIAT:w" s="T57">qwässaŋ</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_215" n="HIAT:w" s="T58">nʼärroun</ts>
                  <nts id="Seg_216" n="HIAT:ip">.</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_219" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_221" n="HIAT:w" s="T59">man</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_224" n="HIAT:w" s="T60">maɣəlǯɨsaŋ</ts>
                  <nts id="Seg_225" n="HIAT:ip">.</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_228" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_230" n="HIAT:w" s="T61">qoqsaŋ</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_233" n="HIAT:w" s="T62">kɨkendä</ts>
                  <nts id="Seg_234" n="HIAT:ip">.</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_237" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_239" n="HIAT:w" s="T63">man</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_242" n="HIAT:w" s="T64">wesʼ</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_245" n="HIAT:w" s="T65">ütumbɨzaŋ</ts>
                  <nts id="Seg_246" n="HIAT:ip">.</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_249" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_251" n="HIAT:w" s="T66">spički</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_254" n="HIAT:w" s="T67">tʼäŋus</ts>
                  <nts id="Seg_255" n="HIAT:ip">.</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_258" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_260" n="HIAT:w" s="T68">nʼäjmɨ</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_263" n="HIAT:w" s="T69">tʼäŋus</ts>
                  <nts id="Seg_264" n="HIAT:ip">.</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_267" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_269" n="HIAT:w" s="T70">pawä</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_272" n="HIAT:w" s="T71">tʼäŋus</ts>
                  <nts id="Seg_273" n="HIAT:ip">.</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_276" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_278" n="HIAT:w" s="T72">tolʼdelaw</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_281" n="HIAT:w" s="T73">esadät</ts>
                  <nts id="Seg_282" n="HIAT:ip">.</nts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_285" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_287" n="HIAT:w" s="T74">nak</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_290" n="HIAT:w" s="T75">tämbadɨt</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_293" n="HIAT:w" s="T76">tidam</ts>
                  <nts id="Seg_294" n="HIAT:ip">.</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_297" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_299" n="HIAT:w" s="T77">toppulam</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_302" n="HIAT:w" s="T78">küsugumnadɨt</ts>
                  <nts id="Seg_303" n="HIAT:ip">.</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_306" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_308" n="HIAT:w" s="T79">paqtɨrsaw</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_311" n="HIAT:w" s="T80">šɨtə</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_314" n="HIAT:w" s="T81">topow</ts>
                  <nts id="Seg_315" n="HIAT:ip">.</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_318" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_320" n="HIAT:w" s="T82">man</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_323" n="HIAT:w" s="T83">čaǯɨzaŋ</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_326" n="HIAT:w" s="T84">naːr</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_329" n="HIAT:w" s="T85">tʼeːlɨt</ts>
                  <nts id="Seg_330" n="HIAT:ip">.</nts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_333" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_335" n="HIAT:w" s="T86">tättɨmdʼelǯä</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_338" n="HIAT:w" s="T87">tʼelɨt</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_341" n="HIAT:w" s="T88">man</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_344" n="HIAT:w" s="T89">qättə</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_347" n="HIAT:w" s="T90">čaːǯəzaŋ</ts>
                  <nts id="Seg_348" n="HIAT:ip">.</nts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_351" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_353" n="HIAT:w" s="T91">man</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_356" n="HIAT:w" s="T92">mittäzaŋ</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_359" n="HIAT:w" s="T93">ködɨn</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_362" n="HIAT:w" s="T94">krajɣə</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_365" n="HIAT:w" s="T95">maːdɨmgaːdɨ</ts>
                  <nts id="Seg_366" n="HIAT:ip">.</nts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_369" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_371" n="HIAT:w" s="T96">maːdumgandə</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_374" n="HIAT:w" s="T97">man</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_377" n="HIAT:w" s="T98">tüssaŋ</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_380" n="HIAT:w" s="T99">säŋɨgu</ts>
                  <nts id="Seg_381" n="HIAT:ip">.</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_384" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_386" n="HIAT:w" s="T100">nemda</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_389" n="HIAT:w" s="T101">man</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_392" n="HIAT:w" s="T102">säŋɨzaŋ</ts>
                  <nts id="Seg_393" n="HIAT:ip">.</nts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_396" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_398" n="HIAT:w" s="T103">toppɨlaw</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_401" n="HIAT:w" s="T104">meŋanan</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_404" n="HIAT:w" s="T105">čangumbizadɨt</ts>
                  <nts id="Seg_405" n="HIAT:ip">.</nts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_408" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_410" n="HIAT:w" s="T106">man</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_413" n="HIAT:w" s="T107">ne</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_416" n="HIAT:w" s="T108">smok</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_419" n="HIAT:w" s="T109">maːttə</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_422" n="HIAT:w" s="T110">mittɨgu</ts>
                  <nts id="Seg_423" n="HIAT:ip">.</nts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_426" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_428" n="HIAT:w" s="T111">mazɨm</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_431" n="HIAT:w" s="T112">qula</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_434" n="HIAT:w" s="T113">tɨtʼä</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_437" n="HIAT:w" s="T114">tatpɨzadɨt</ts>
                  <nts id="Seg_438" n="HIAT:ip">.</nts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_441" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_443" n="HIAT:w" s="T115">nʼäjam</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_446" n="HIAT:w" s="T116">man</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_449" n="HIAT:w" s="T117">qaimna</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_452" n="HIAT:w" s="T118">assä</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_455" n="HIAT:w" s="T119">qwässaw</ts>
                  <nts id="Seg_456" n="HIAT:ip">.</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_459" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_461" n="HIAT:w" s="T120">man</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_464" n="HIAT:w" s="T121">qwässaw</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_467" n="HIAT:w" s="T122">üŋunʼǯum</ts>
                  <nts id="Seg_468" n="HIAT:ip">.</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_471" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_473" n="HIAT:w" s="T123">man</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_476" n="HIAT:w" s="T124">täpɨlani</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_479" n="HIAT:w" s="T125">missam</ts>
                  <nts id="Seg_480" n="HIAT:ip">.</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_483" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_485" n="HIAT:w" s="T126">täp</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_488" n="HIAT:w" s="T127">meŋanan</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_491" n="HIAT:w" s="T128">iːzat</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_494" n="HIAT:w" s="T129">muqtut</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_497" n="HIAT:w" s="T130">rublʼej</ts>
                  <nts id="Seg_498" n="HIAT:ip">.</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T131" id="Seg_500" n="sc" s="T1">
               <ts e="T2" id="Seg_502" n="e" s="T1">man </ts>
               <ts e="T3" id="Seg_504" n="e" s="T2">qwässaŋ </ts>
               <ts e="T4" id="Seg_506" n="e" s="T3">qɨndabɨɣɨnnɨ </ts>
               <ts e="T5" id="Seg_508" n="e" s="T4">maːdʼiŋbɨlʼlʼe. </ts>
               <ts e="T6" id="Seg_510" n="e" s="T5">i </ts>
               <ts e="T7" id="Seg_512" n="e" s="T6">maɣɨlʼǯembesaŋ </ts>
               <ts e="T8" id="Seg_514" n="e" s="T7">wättoɣɨn: </ts>
               <ts e="T9" id="Seg_516" n="e" s="T8">qaina </ts>
               <ts e="T10" id="Seg_518" n="e" s="T9">qum </ts>
               <ts e="T11" id="Seg_520" n="e" s="T10">tʼäŋgus. </ts>
               <ts e="T12" id="Seg_522" n="e" s="T11">man </ts>
               <ts e="T13" id="Seg_524" n="e" s="T12">okonäŋ </ts>
               <ts e="T14" id="Seg_526" n="e" s="T13">äːsan. </ts>
               <ts e="T15" id="Seg_528" n="e" s="T14">qainä </ts>
               <ts e="T16" id="Seg_530" n="e" s="T15">tʼäŋus </ts>
               <ts e="T17" id="Seg_532" n="e" s="T16">massä. </ts>
               <ts e="T18" id="Seg_534" n="e" s="T17">mazɨm </ts>
               <ts e="T19" id="Seg_536" n="e" s="T18">nʼünʼikasɨŋ </ts>
               <ts e="T20" id="Seg_538" n="e" s="T19">üt </ts>
               <ts e="T21" id="Seg_540" n="e" s="T20">serbazɨt. </ts>
               <ts e="T22" id="Seg_542" n="e" s="T21">man </ts>
               <ts e="T23" id="Seg_544" n="e" s="T22">wattoɣänä </ts>
               <ts e="T24" id="Seg_546" n="e" s="T23">qonǯersaw </ts>
               <ts e="T25" id="Seg_548" n="e" s="T24">surɨm </ts>
               <ts e="T26" id="Seg_550" n="e" s="T25">nagur. </ts>
               <ts e="T27" id="Seg_552" n="e" s="T26">man </ts>
               <ts e="T28" id="Seg_554" n="e" s="T27">kɨkɨzaŋ </ts>
               <ts e="T29" id="Seg_556" n="e" s="T28">täpɨlannan </ts>
               <ts e="T30" id="Seg_558" n="e" s="T29">kunɨgu. </ts>
               <ts e="T31" id="Seg_560" n="e" s="T30">no </ts>
               <ts e="T32" id="Seg_562" n="e" s="T31">meŋa </ts>
               <ts e="T33" id="Seg_564" n="e" s="T32">prišlosʼ </ts>
               <ts e="T34" id="Seg_566" n="e" s="T33">tʼačegu </ts>
               <ts e="T35" id="Seg_568" n="e" s="T34">täpɨlam </ts>
               <ts e="T36" id="Seg_570" n="e" s="T35">tʼülʼdisäsä. </ts>
               <ts e="T37" id="Seg_572" n="e" s="T36">no </ts>
               <ts e="T38" id="Seg_574" n="e" s="T37">man </ts>
               <ts e="T39" id="Seg_576" n="e" s="T38">qaimnas </ts>
               <ts e="T40" id="Seg_578" n="e" s="T39">qwäsaŋ </ts>
               <ts e="T41" id="Seg_580" n="e" s="T40">täpɨlam. </ts>
               <ts e="T42" id="Seg_582" n="e" s="T41">no </ts>
               <ts e="T43" id="Seg_584" n="e" s="T42">surɨla </ts>
               <ts e="T44" id="Seg_586" n="e" s="T43">wes </ts>
               <ts e="T45" id="Seg_588" n="e" s="T44">qwäŋɨlusadɨt. </ts>
               <ts e="T46" id="Seg_590" n="e" s="T45">man </ts>
               <ts e="T47" id="Seg_592" n="e" s="T46">okonä </ts>
               <ts e="T48" id="Seg_594" n="e" s="T47">qalɨzaŋ </ts>
               <ts e="T49" id="Seg_596" n="e" s="T48">tün </ts>
               <ts e="T50" id="Seg_598" n="e" s="T49">doqqän. </ts>
               <ts e="T51" id="Seg_600" n="e" s="T50">man </ts>
               <ts e="T52" id="Seg_602" n="e" s="T51">oqqoronä </ts>
               <ts e="T53" id="Seg_604" n="e" s="T52">qaːlɨzan. </ts>
               <ts e="T54" id="Seg_606" n="e" s="T53">kutäna </ts>
               <ts e="T55" id="Seg_608" n="e" s="T54">qajna </ts>
               <ts e="T56" id="Seg_610" n="e" s="T55">tʼäŋus. </ts>
               <ts e="T57" id="Seg_612" n="e" s="T56">man </ts>
               <ts e="T58" id="Seg_614" n="e" s="T57">qwässaŋ </ts>
               <ts e="T59" id="Seg_616" n="e" s="T58">nʼärroun. </ts>
               <ts e="T60" id="Seg_618" n="e" s="T59">man </ts>
               <ts e="T61" id="Seg_620" n="e" s="T60">maɣəlǯɨsaŋ. </ts>
               <ts e="T62" id="Seg_622" n="e" s="T61">qoqsaŋ </ts>
               <ts e="T63" id="Seg_624" n="e" s="T62">kɨkendä. </ts>
               <ts e="T64" id="Seg_626" n="e" s="T63">man </ts>
               <ts e="T65" id="Seg_628" n="e" s="T64">wesʼ </ts>
               <ts e="T66" id="Seg_630" n="e" s="T65">ütumbɨzaŋ. </ts>
               <ts e="T67" id="Seg_632" n="e" s="T66">spički </ts>
               <ts e="T68" id="Seg_634" n="e" s="T67">tʼäŋus. </ts>
               <ts e="T69" id="Seg_636" n="e" s="T68">nʼäjmɨ </ts>
               <ts e="T70" id="Seg_638" n="e" s="T69">tʼäŋus. </ts>
               <ts e="T71" id="Seg_640" n="e" s="T70">pawä </ts>
               <ts e="T72" id="Seg_642" n="e" s="T71">tʼäŋus. </ts>
               <ts e="T73" id="Seg_644" n="e" s="T72">tolʼdelaw </ts>
               <ts e="T74" id="Seg_646" n="e" s="T73">esadät. </ts>
               <ts e="T75" id="Seg_648" n="e" s="T74">nak </ts>
               <ts e="T76" id="Seg_650" n="e" s="T75">tämbadɨt </ts>
               <ts e="T77" id="Seg_652" n="e" s="T76">tidam. </ts>
               <ts e="T78" id="Seg_654" n="e" s="T77">toppulam </ts>
               <ts e="T79" id="Seg_656" n="e" s="T78">küsugumnadɨt. </ts>
               <ts e="T80" id="Seg_658" n="e" s="T79">paqtɨrsaw </ts>
               <ts e="T81" id="Seg_660" n="e" s="T80">šɨtə </ts>
               <ts e="T82" id="Seg_662" n="e" s="T81">topow. </ts>
               <ts e="T83" id="Seg_664" n="e" s="T82">man </ts>
               <ts e="T84" id="Seg_666" n="e" s="T83">čaǯɨzaŋ </ts>
               <ts e="T85" id="Seg_668" n="e" s="T84">naːr </ts>
               <ts e="T86" id="Seg_670" n="e" s="T85">tʼeːlɨt. </ts>
               <ts e="T87" id="Seg_672" n="e" s="T86">tättɨmdʼelǯä </ts>
               <ts e="T88" id="Seg_674" n="e" s="T87">tʼelɨt </ts>
               <ts e="T89" id="Seg_676" n="e" s="T88">man </ts>
               <ts e="T90" id="Seg_678" n="e" s="T89">qättə </ts>
               <ts e="T91" id="Seg_680" n="e" s="T90">čaːǯəzaŋ. </ts>
               <ts e="T92" id="Seg_682" n="e" s="T91">man </ts>
               <ts e="T93" id="Seg_684" n="e" s="T92">mittäzaŋ </ts>
               <ts e="T94" id="Seg_686" n="e" s="T93">ködɨn </ts>
               <ts e="T95" id="Seg_688" n="e" s="T94">krajɣə </ts>
               <ts e="T96" id="Seg_690" n="e" s="T95">maːdɨmgaːdɨ. </ts>
               <ts e="T97" id="Seg_692" n="e" s="T96">maːdumgandə </ts>
               <ts e="T98" id="Seg_694" n="e" s="T97">man </ts>
               <ts e="T99" id="Seg_696" n="e" s="T98">tüssaŋ </ts>
               <ts e="T100" id="Seg_698" n="e" s="T99">säŋɨgu. </ts>
               <ts e="T101" id="Seg_700" n="e" s="T100">nemda </ts>
               <ts e="T102" id="Seg_702" n="e" s="T101">man </ts>
               <ts e="T103" id="Seg_704" n="e" s="T102">säŋɨzaŋ. </ts>
               <ts e="T104" id="Seg_706" n="e" s="T103">toppɨlaw </ts>
               <ts e="T105" id="Seg_708" n="e" s="T104">meŋanan </ts>
               <ts e="T106" id="Seg_710" n="e" s="T105">čangumbizadɨt. </ts>
               <ts e="T107" id="Seg_712" n="e" s="T106">man </ts>
               <ts e="T108" id="Seg_714" n="e" s="T107">ne </ts>
               <ts e="T109" id="Seg_716" n="e" s="T108">smok </ts>
               <ts e="T110" id="Seg_718" n="e" s="T109">maːttə </ts>
               <ts e="T111" id="Seg_720" n="e" s="T110">mittɨgu. </ts>
               <ts e="T112" id="Seg_722" n="e" s="T111">mazɨm </ts>
               <ts e="T113" id="Seg_724" n="e" s="T112">qula </ts>
               <ts e="T114" id="Seg_726" n="e" s="T113">tɨtʼä </ts>
               <ts e="T115" id="Seg_728" n="e" s="T114">tatpɨzadɨt. </ts>
               <ts e="T116" id="Seg_730" n="e" s="T115">nʼäjam </ts>
               <ts e="T117" id="Seg_732" n="e" s="T116">man </ts>
               <ts e="T118" id="Seg_734" n="e" s="T117">qaimna </ts>
               <ts e="T119" id="Seg_736" n="e" s="T118">assä </ts>
               <ts e="T120" id="Seg_738" n="e" s="T119">qwässaw. </ts>
               <ts e="T121" id="Seg_740" n="e" s="T120">man </ts>
               <ts e="T122" id="Seg_742" n="e" s="T121">qwässaw </ts>
               <ts e="T123" id="Seg_744" n="e" s="T122">üŋunʼǯum. </ts>
               <ts e="T124" id="Seg_746" n="e" s="T123">man </ts>
               <ts e="T125" id="Seg_748" n="e" s="T124">täpɨlani </ts>
               <ts e="T126" id="Seg_750" n="e" s="T125">missam. </ts>
               <ts e="T127" id="Seg_752" n="e" s="T126">täp </ts>
               <ts e="T128" id="Seg_754" n="e" s="T127">meŋanan </ts>
               <ts e="T129" id="Seg_756" n="e" s="T128">iːzat </ts>
               <ts e="T130" id="Seg_758" n="e" s="T129">muqtut </ts>
               <ts e="T131" id="Seg_760" n="e" s="T130">rublʼej. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_761" s="T1">SSF_1963_SquirrelHunting1_nar.001 (001.001)</ta>
            <ta e="T11" id="Seg_762" s="T5">SSF_1963_SquirrelHunting1_nar.002 (001.002)</ta>
            <ta e="T14" id="Seg_763" s="T11">SSF_1963_SquirrelHunting1_nar.003 (001.003)</ta>
            <ta e="T17" id="Seg_764" s="T14">SSF_1963_SquirrelHunting1_nar.004 (001.004)</ta>
            <ta e="T21" id="Seg_765" s="T17">SSF_1963_SquirrelHunting1_nar.005 (001.005)</ta>
            <ta e="T26" id="Seg_766" s="T21">SSF_1963_SquirrelHunting1_nar.006 (001.006)</ta>
            <ta e="T30" id="Seg_767" s="T26">SSF_1963_SquirrelHunting1_nar.007 (001.007)</ta>
            <ta e="T36" id="Seg_768" s="T30">SSF_1963_SquirrelHunting1_nar.008 (001.008)</ta>
            <ta e="T41" id="Seg_769" s="T36">SSF_1963_SquirrelHunting1_nar.009 (001.009)</ta>
            <ta e="T45" id="Seg_770" s="T41">SSF_1963_SquirrelHunting1_nar.010 (001.010)</ta>
            <ta e="T50" id="Seg_771" s="T45">SSF_1963_SquirrelHunting1_nar.011 (001.011)</ta>
            <ta e="T53" id="Seg_772" s="T50">SSF_1963_SquirrelHunting1_nar.012 (001.012)</ta>
            <ta e="T56" id="Seg_773" s="T53">SSF_1963_SquirrelHunting1_nar.013 (001.013)</ta>
            <ta e="T59" id="Seg_774" s="T56">SSF_1963_SquirrelHunting1_nar.014 (001.014)</ta>
            <ta e="T61" id="Seg_775" s="T59">SSF_1963_SquirrelHunting1_nar.015 (001.015)</ta>
            <ta e="T63" id="Seg_776" s="T61">SSF_1963_SquirrelHunting1_nar.016 (001.016)</ta>
            <ta e="T66" id="Seg_777" s="T63">SSF_1963_SquirrelHunting1_nar.017 (001.017)</ta>
            <ta e="T68" id="Seg_778" s="T66">SSF_1963_SquirrelHunting1_nar.018 (001.018)</ta>
            <ta e="T70" id="Seg_779" s="T68">SSF_1963_SquirrelHunting1_nar.019 (001.019)</ta>
            <ta e="T72" id="Seg_780" s="T70">SSF_1963_SquirrelHunting1_nar.020 (001.020)</ta>
            <ta e="T74" id="Seg_781" s="T72">SSF_1963_SquirrelHunting1_nar.021 (001.021)</ta>
            <ta e="T77" id="Seg_782" s="T74">SSF_1963_SquirrelHunting1_nar.022 (001.022)</ta>
            <ta e="T79" id="Seg_783" s="T77">SSF_1963_SquirrelHunting1_nar.023 (001.023)</ta>
            <ta e="T82" id="Seg_784" s="T79">SSF_1963_SquirrelHunting1_nar.024 (001.024)</ta>
            <ta e="T86" id="Seg_785" s="T82">SSF_1963_SquirrelHunting1_nar.025 (001.025)</ta>
            <ta e="T91" id="Seg_786" s="T86">SSF_1963_SquirrelHunting1_nar.026 (001.026)</ta>
            <ta e="T96" id="Seg_787" s="T91">SSF_1963_SquirrelHunting1_nar.027 (001.027)</ta>
            <ta e="T100" id="Seg_788" s="T96">SSF_1963_SquirrelHunting1_nar.028 (001.028)</ta>
            <ta e="T103" id="Seg_789" s="T100">SSF_1963_SquirrelHunting1_nar.029 (001.029)</ta>
            <ta e="T106" id="Seg_790" s="T103">SSF_1963_SquirrelHunting1_nar.030 (001.030)</ta>
            <ta e="T111" id="Seg_791" s="T106">SSF_1963_SquirrelHunting1_nar.031 (001.031)</ta>
            <ta e="T115" id="Seg_792" s="T111">SSF_1963_SquirrelHunting1_nar.032 (001.032)</ta>
            <ta e="T120" id="Seg_793" s="T115">SSF_1963_SquirrelHunting1_nar.033 (001.033)</ta>
            <ta e="T123" id="Seg_794" s="T120">SSF_1963_SquirrelHunting1_nar.034 (001.034)</ta>
            <ta e="T126" id="Seg_795" s="T123">SSF_1963_SquirrelHunting1_nar.035 (001.035)</ta>
            <ta e="T131" id="Seg_796" s="T126">SSF_1963_SquirrelHunting1_nar.036 (001.036)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_797" s="T1">ман kwӓ′ссаң kын′дабыɣы‵нны ′ма̄дʼиңбылʼлʼе.</ta>
            <ta e="T11" id="Seg_798" s="T5">и ′маɣылʼджембесаң вӓ′ттоɣын: kаи′на kум тʼӓңгус.</ta>
            <ta e="T14" id="Seg_799" s="T11">ман о′ко′нӓң ′ӓ̄сан.</ta>
            <ta e="T17" id="Seg_800" s="T14">kаи′нӓ ′тʼӓңус ма′ссӓ.</ta>
            <ta e="T21" id="Seg_801" s="T17">′мазым нʼӱнʼикасың ӱт ′се(ӓ)рбазыт.</ta>
            <ta e="T26" id="Seg_802" s="T21">ман ва′ттоɣӓнӓ kонджер′саw ′сурым ′нагур.</ta>
            <ta e="T30" id="Seg_803" s="T26">ман кык(к)ы′заң тӓпы′ланнан ′куныгу.</ta>
            <ta e="T36" id="Seg_804" s="T30">но ′меңа при′шлосʼ тʼатше′гу ′тӓпылам ′тʼӱлʼдисӓсӓ.</ta>
            <ta e="T41" id="Seg_805" s="T36">но ман kаим′нас kwӓ′саң тӓпы′лам.</ta>
            <ta e="T45" id="Seg_806" s="T41">но ′сурыла вес ′kwӓңылу′садыт.</ta>
            <ta e="T50" id="Seg_807" s="T45">ман око′нӓ kалы′заң тӱн ′доkkӓн.</ta>
            <ta e="T53" id="Seg_808" s="T50">ман оkkоро′нӓ kа̄лызан.</ta>
            <ta e="T56" id="Seg_809" s="T53">кутӓ′на kай′на ′тʼӓңус.</ta>
            <ta e="T59" id="Seg_810" s="T56">ман kwӓ′ссаң нʼӓ′рроу(w)н.</ta>
            <ta e="T61" id="Seg_811" s="T59">ман ‵маɣълджысаң.</ta>
            <ta e="T63" id="Seg_812" s="T61">kоk′саң кы′кендӓ.</ta>
            <ta e="T66" id="Seg_813" s="T63">ман весʼ ӱтумбызаң.</ta>
            <ta e="T68" id="Seg_814" s="T66">′спички ′тʼӓңус.</ta>
            <ta e="T70" id="Seg_815" s="T68">′нʼӓймы ′тʼӓңус.</ta>
            <ta e="T72" id="Seg_816" s="T70">′павӓ ′тʼӓңус.</ta>
            <ta e="T74" id="Seg_817" s="T72">толʼде′лаw ′е(ӓ)садӓт.</ta>
            <ta e="T77" id="Seg_818" s="T74">′нак ′тӓмбадыт ти′дам.</ta>
            <ta e="T79" id="Seg_819" s="T77">топпу′лам кӱсугум′надыт.</ta>
            <ta e="T82" id="Seg_820" s="T79">паkтыр′саw ′шытъ то′поw.</ta>
            <ta e="T86" id="Seg_821" s="T82">ман тшаджы′заң на̄р ′тʼе̄лыт.</ta>
            <ta e="T91" id="Seg_822" s="T86">′тӓттым′дʼелджӓ ′тʼелыт ман ′kӓттъ ′тша̄джъзаң.</ta>
            <ta e="T96" id="Seg_823" s="T91">ман миттӓ′заң ′кӧдын ′крайɣъ ′ма̄дым′га̄ды.</ta>
            <ta e="T100" id="Seg_824" s="T96">′ма̄дум ′гандъ ман ′тӱссаң ′сӓңыгу.</ta>
            <ta e="T103" id="Seg_825" s="T100">′немда ман ′сӓңызаң.</ta>
            <ta e="T106" id="Seg_826" s="T103">топпы′лаw ′меңанан тшангумби′задыт.</ta>
            <ta e="T111" id="Seg_827" s="T106">ман не(и) ′смок ′ма̄ттъ мит(т)ы′гу.</ta>
            <ta e="T115" id="Seg_828" s="T111">′мазым ′kула ты′тʼӓ татпы′задыт.</ta>
            <ta e="T120" id="Seg_829" s="T115">′нʼӓjам ман kаим′на ассӓ kwӓ′ссаw(м).</ta>
            <ta e="T123" id="Seg_830" s="T120">ман kwӓ′ссаw(м) ӱңунʼджум.</ta>
            <ta e="T126" id="Seg_831" s="T123">ман тӓпы′лани ми′ссам.</ta>
            <ta e="T131" id="Seg_832" s="T126">тӓп ′меңанан ′ӣза(ы)т ′муkтут рублʼей.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_833" s="T1">man qwässaŋ qɨndabɨɣɨnnɨ maːdʼiŋbɨlʼlʼe.</ta>
            <ta e="T11" id="Seg_834" s="T5">i maɣɨlʼǯembesaŋ vättoɣɨn: qaina qum tʼäŋgus.</ta>
            <ta e="T14" id="Seg_835" s="T11">man okonäŋ äːsan.</ta>
            <ta e="T17" id="Seg_836" s="T14">qainä tʼäŋus massä.</ta>
            <ta e="T21" id="Seg_837" s="T17">mazɨm nʼünʼikasɨŋ üt se(ä)rbazɨt.</ta>
            <ta e="T26" id="Seg_838" s="T21">man vattoɣänä qonǯersaw surɨm nagur.</ta>
            <ta e="T30" id="Seg_839" s="T26">man kɨk(k)ɨzaŋ täpɨlannan kunɨgu.</ta>
            <ta e="T36" id="Seg_840" s="T30">no meŋa prišlosʼ tʼatšegu täpɨlam tʼülʼdisäsä.</ta>
            <ta e="T41" id="Seg_841" s="T36">no man qaimnas qwäsaŋ täpɨlam.</ta>
            <ta e="T45" id="Seg_842" s="T41">no surɨla ves qwäŋɨlusadɨt.</ta>
            <ta e="T50" id="Seg_843" s="T45">man okonä qalɨzaŋ tün doqqän.</ta>
            <ta e="T53" id="Seg_844" s="T50">man oqqoronä qaːlɨzan.</ta>
            <ta e="T56" id="Seg_845" s="T53">kutäna qajna tʼäŋus.</ta>
            <ta e="T59" id="Seg_846" s="T56">man qwässaŋ nʼärrou(w)n.</ta>
            <ta e="T61" id="Seg_847" s="T59">man maɣəlǯɨsaŋ.</ta>
            <ta e="T63" id="Seg_848" s="T61">qoqsaŋ kɨkendä.</ta>
            <ta e="T66" id="Seg_849" s="T63">man vesʼ ütumbɨzaŋ.</ta>
            <ta e="T68" id="Seg_850" s="T66">spički tʼäŋus.</ta>
            <ta e="T70" id="Seg_851" s="T68">nʼäjmɨ tʼäŋus.</ta>
            <ta e="T72" id="Seg_852" s="T70">pavä tʼäŋus.</ta>
            <ta e="T74" id="Seg_853" s="T72">tolʼdelaw e(ä)sadät.</ta>
            <ta e="T77" id="Seg_854" s="T74">nak tämbadɨt tidam.</ta>
            <ta e="T79" id="Seg_855" s="T77">toppulam küsugumnadɨt.</ta>
            <ta e="T82" id="Seg_856" s="T79">paqtɨrsaw šɨtə topow.</ta>
            <ta e="T86" id="Seg_857" s="T82">man tšaǯɨzaŋ naːr tʼeːlɨt.</ta>
            <ta e="T91" id="Seg_858" s="T86">tättɨmdʼelǯä tʼelɨt man qättə tšaːǯəzaŋ.</ta>
            <ta e="T96" id="Seg_859" s="T91">man mittäzaŋ ködɨn krajɣə maːdɨmgaːdɨ.</ta>
            <ta e="T100" id="Seg_860" s="T96">maːdum gandə man tüssaŋ säŋɨgu.</ta>
            <ta e="T103" id="Seg_861" s="T100">nemda man säŋɨzaŋ.</ta>
            <ta e="T106" id="Seg_862" s="T103">toppɨlaw meŋanan tšangumbizadɨt.</ta>
            <ta e="T111" id="Seg_863" s="T106">man ne(i) smok maːttə mit(t)ɨgu.</ta>
            <ta e="T115" id="Seg_864" s="T111">mazɨm qula tɨtʼä tatpɨzadɨt.</ta>
            <ta e="T120" id="Seg_865" s="T115">nʼäjam man qaimna assä qwässaw(m).</ta>
            <ta e="T123" id="Seg_866" s="T120">man qwässaw(m) üŋunʼǯum.</ta>
            <ta e="T126" id="Seg_867" s="T123">man täpɨlani missam. </ta>
            <ta e="T131" id="Seg_868" s="T126">täp meŋanan iːza(ɨ)t muqtut rublʼej.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_869" s="T1">man qwässaŋ qɨndabɨɣɨnnɨ maːdʼiŋbɨlʼlʼe. </ta>
            <ta e="T11" id="Seg_870" s="T5">i maɣɨlʼǯembesaŋ wättoɣɨn: qaina qum tʼäŋgus. </ta>
            <ta e="T14" id="Seg_871" s="T11">man okonäŋ äːsan. </ta>
            <ta e="T17" id="Seg_872" s="T14">qainä tʼäŋus massä. </ta>
            <ta e="T21" id="Seg_873" s="T17">mazɨm nʼünʼikasɨŋ üt serbazɨt. </ta>
            <ta e="T26" id="Seg_874" s="T21">man wattoɣänä qonǯersaw surɨm nagur. </ta>
            <ta e="T30" id="Seg_875" s="T26">man kɨkɨzaŋ täpɨlannan kunɨgu. </ta>
            <ta e="T36" id="Seg_876" s="T30">no meŋa prišlosʼ tʼačegu täpɨlam tʼülʼdisäsä. </ta>
            <ta e="T41" id="Seg_877" s="T36">no man qaimnas qwäsaŋ täpɨlam. </ta>
            <ta e="T45" id="Seg_878" s="T41">no surɨla wes qwäŋɨlusadɨt. </ta>
            <ta e="T50" id="Seg_879" s="T45">man okonä qalɨzaŋ tün doqqän. </ta>
            <ta e="T53" id="Seg_880" s="T50">man oqqoronä qaːlɨzan. </ta>
            <ta e="T56" id="Seg_881" s="T53">kutäna qajna tʼäŋus. </ta>
            <ta e="T59" id="Seg_882" s="T56">man qwässaŋ nʼärroun. </ta>
            <ta e="T61" id="Seg_883" s="T59">man maɣəlǯɨsaŋ. </ta>
            <ta e="T63" id="Seg_884" s="T61">qoqsaŋ kɨkendä. </ta>
            <ta e="T66" id="Seg_885" s="T63">man wesʼ ütumbɨzaŋ. </ta>
            <ta e="T68" id="Seg_886" s="T66">spički tʼäŋus. </ta>
            <ta e="T70" id="Seg_887" s="T68">nʼäjmɨ tʼäŋus. </ta>
            <ta e="T72" id="Seg_888" s="T70">pawä tʼäŋus. </ta>
            <ta e="T74" id="Seg_889" s="T72">tolʼdelaw esadät. </ta>
            <ta e="T77" id="Seg_890" s="T74">nak tämbadɨt tidam. </ta>
            <ta e="T79" id="Seg_891" s="T77">toppulam küsugumnadɨt. </ta>
            <ta e="T82" id="Seg_892" s="T79">paqtɨrsaw šɨtə topow. </ta>
            <ta e="T86" id="Seg_893" s="T82">man čaǯɨzaŋ naːr tʼeːlɨt. </ta>
            <ta e="T91" id="Seg_894" s="T86">tättɨmdʼelǯä tʼelɨt man qättə čaːǯəzaŋ. </ta>
            <ta e="T96" id="Seg_895" s="T91">man mittäzaŋ ködɨn krajɣə maːdɨmgaːdɨ. </ta>
            <ta e="T100" id="Seg_896" s="T96">maːdumgandə man tüssaŋ säŋɨgu. </ta>
            <ta e="T103" id="Seg_897" s="T100">nemda man säŋɨzaŋ. </ta>
            <ta e="T106" id="Seg_898" s="T103">toppɨlaw meŋanan čangumbizadɨt. </ta>
            <ta e="T111" id="Seg_899" s="T106">man ne smok maːttə mittɨgu. </ta>
            <ta e="T115" id="Seg_900" s="T111">mazɨm qula tɨtʼä tatpɨzadɨt. </ta>
            <ta e="T120" id="Seg_901" s="T115">nʼäjam man qaimna assä qwässaw. </ta>
            <ta e="T123" id="Seg_902" s="T120">man qwässaw üŋunʼǯum. </ta>
            <ta e="T126" id="Seg_903" s="T123">man täpɨlani missam. </ta>
            <ta e="T131" id="Seg_904" s="T126">täp meŋanan iːzat muqtut rublʼej. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_905" s="T1">man</ta>
            <ta e="T3" id="Seg_906" s="T2">qwäs-sa-ŋ</ta>
            <ta e="T4" id="Seg_907" s="T3">qɨndabɨ-ɣɨnnɨ</ta>
            <ta e="T5" id="Seg_908" s="T4">maːdʼi-ŋ-bɨ-lʼ-lʼe</ta>
            <ta e="T6" id="Seg_909" s="T5">i</ta>
            <ta e="T7" id="Seg_910" s="T6">maɣɨ-lʼǯe-mbe-sa-ŋ</ta>
            <ta e="T8" id="Seg_911" s="T7">wätto-ɣɨn</ta>
            <ta e="T9" id="Seg_912" s="T8">qai-na</ta>
            <ta e="T10" id="Seg_913" s="T9">qum</ta>
            <ta e="T11" id="Seg_914" s="T10">tʼäŋgu-s</ta>
            <ta e="T12" id="Seg_915" s="T11">man</ta>
            <ta e="T13" id="Seg_916" s="T12">oko-nä-ŋ</ta>
            <ta e="T14" id="Seg_917" s="T13">äː-sa-ŋ</ta>
            <ta e="T15" id="Seg_918" s="T14">qai-nä</ta>
            <ta e="T16" id="Seg_919" s="T15">tʼäŋu-s</ta>
            <ta e="T17" id="Seg_920" s="T16">mas-sä</ta>
            <ta e="T18" id="Seg_921" s="T17">mazɨm</ta>
            <ta e="T19" id="Seg_922" s="T18">nʼünʼi-ka-sɨ-ŋ</ta>
            <ta e="T20" id="Seg_923" s="T19">üt</ta>
            <ta e="T21" id="Seg_924" s="T20">ser-ba-zɨ-t</ta>
            <ta e="T22" id="Seg_925" s="T21">man</ta>
            <ta e="T23" id="Seg_926" s="T22">watto-ɣänä</ta>
            <ta e="T24" id="Seg_927" s="T23">qo-nǯe-r-sa-w</ta>
            <ta e="T25" id="Seg_928" s="T24">surɨm</ta>
            <ta e="T26" id="Seg_929" s="T25">nakkɨr</ta>
            <ta e="T27" id="Seg_930" s="T26">man</ta>
            <ta e="T28" id="Seg_931" s="T27">kɨkɨ-za-ŋ</ta>
            <ta e="T29" id="Seg_932" s="T28">täp-ɨ-la-n-nan</ta>
            <ta e="T30" id="Seg_933" s="T29">kunɨ-gu</ta>
            <ta e="T31" id="Seg_934" s="T30">no</ta>
            <ta e="T32" id="Seg_935" s="T31">meŋa</ta>
            <ta e="T34" id="Seg_936" s="T33">tʼače-gu</ta>
            <ta e="T35" id="Seg_937" s="T34">täp-ɨ-la-m</ta>
            <ta e="T36" id="Seg_938" s="T35">tʼülʼdisä-sä</ta>
            <ta e="T37" id="Seg_939" s="T36">no</ta>
            <ta e="T38" id="Seg_940" s="T37">man</ta>
            <ta e="T39" id="Seg_941" s="T38">qai-m-nas</ta>
            <ta e="T40" id="Seg_942" s="T39">qwä-sa-ŋ</ta>
            <ta e="T41" id="Seg_943" s="T40">täp-ɨ-la-m</ta>
            <ta e="T42" id="Seg_944" s="T41">no</ta>
            <ta e="T43" id="Seg_945" s="T42">surɨ-la</ta>
            <ta e="T44" id="Seg_946" s="T43">wes</ta>
            <ta e="T45" id="Seg_947" s="T44">qwäŋ-ɨ-lu-sa-dɨt</ta>
            <ta e="T46" id="Seg_948" s="T45">man</ta>
            <ta e="T47" id="Seg_949" s="T46">oko-nä</ta>
            <ta e="T48" id="Seg_950" s="T47">qalɨ-za-ŋ</ta>
            <ta e="T49" id="Seg_951" s="T48">tü-n</ta>
            <ta e="T50" id="Seg_952" s="T49">doqqän</ta>
            <ta e="T51" id="Seg_953" s="T50">man</ta>
            <ta e="T52" id="Seg_954" s="T51">oqqor-o-nä</ta>
            <ta e="T53" id="Seg_955" s="T52">qaːlɨ-za-n</ta>
            <ta e="T54" id="Seg_956" s="T53">kutä-na</ta>
            <ta e="T55" id="Seg_957" s="T54">qaj-na</ta>
            <ta e="T56" id="Seg_958" s="T55">tʼäŋu-s</ta>
            <ta e="T57" id="Seg_959" s="T56">man</ta>
            <ta e="T58" id="Seg_960" s="T57">qwäs-sa-ŋ</ta>
            <ta e="T59" id="Seg_961" s="T58">nʼärro-un</ta>
            <ta e="T60" id="Seg_962" s="T59">man</ta>
            <ta e="T61" id="Seg_963" s="T60">*maɣə-lǯɨ-sa-ŋ</ta>
            <ta e="T62" id="Seg_964" s="T61">qoq-sa-ŋ</ta>
            <ta e="T63" id="Seg_965" s="T62">kɨke-ndä</ta>
            <ta e="T64" id="Seg_966" s="T63">man</ta>
            <ta e="T65" id="Seg_967" s="T64">wesʼ</ta>
            <ta e="T66" id="Seg_968" s="T65">üt-u-m-bɨ-za-ŋ</ta>
            <ta e="T67" id="Seg_969" s="T66">spički</ta>
            <ta e="T68" id="Seg_970" s="T67">tʼäŋu-s</ta>
            <ta e="T69" id="Seg_971" s="T68">nʼäj-mɨ</ta>
            <ta e="T70" id="Seg_972" s="T69">tʼäŋu-s</ta>
            <ta e="T71" id="Seg_973" s="T70">pa-wä</ta>
            <ta e="T72" id="Seg_974" s="T71">tʼäŋu-s</ta>
            <ta e="T73" id="Seg_975" s="T72">tolʼde-la-w</ta>
            <ta e="T74" id="Seg_976" s="T73">e-sa-dät</ta>
            <ta e="T75" id="Seg_977" s="T74">nak</ta>
            <ta e="T76" id="Seg_978" s="T75">tä-mba-dɨt</ta>
            <ta e="T77" id="Seg_979" s="T76">tidam</ta>
            <ta e="T78" id="Seg_980" s="T77">toppu-la-m</ta>
            <ta e="T79" id="Seg_981" s="T78">küs-u-gu-m-na-dɨt</ta>
            <ta e="T80" id="Seg_982" s="T79">paqtɨ-r-sa-w</ta>
            <ta e="T81" id="Seg_983" s="T80">šɨtə</ta>
            <ta e="T82" id="Seg_984" s="T81">topo-w</ta>
            <ta e="T83" id="Seg_985" s="T82">man</ta>
            <ta e="T84" id="Seg_986" s="T83">čaǯɨ-za-ŋ</ta>
            <ta e="T85" id="Seg_987" s="T84">naːr</ta>
            <ta e="T86" id="Seg_988" s="T85">tʼeːlɨ-tɨ</ta>
            <ta e="T87" id="Seg_989" s="T86">tättɨ-mdʼelǯä</ta>
            <ta e="T88" id="Seg_990" s="T87">tʼelɨ-tɨ</ta>
            <ta e="T89" id="Seg_991" s="T88">man</ta>
            <ta e="T90" id="Seg_992" s="T89">qättə</ta>
            <ta e="T91" id="Seg_993" s="T90">čaːǯə-za-ŋ</ta>
            <ta e="T92" id="Seg_994" s="T91">man</ta>
            <ta e="T93" id="Seg_995" s="T92">mittä-za-ŋ</ta>
            <ta e="T94" id="Seg_996" s="T93">köd-ɨ-n</ta>
            <ta e="T95" id="Seg_997" s="T94">kraj-ɣə</ta>
            <ta e="T96" id="Seg_998" s="T95">maːdɨmgaː-dɨ</ta>
            <ta e="T97" id="Seg_999" s="T96">maːdumga-ndə</ta>
            <ta e="T98" id="Seg_1000" s="T97">man</ta>
            <ta e="T99" id="Seg_1001" s="T98">tü-ssa-ŋ</ta>
            <ta e="T100" id="Seg_1002" s="T99">säŋɨ-gu</ta>
            <ta e="T101" id="Seg_1003" s="T100">nemda</ta>
            <ta e="T102" id="Seg_1004" s="T101">man</ta>
            <ta e="T103" id="Seg_1005" s="T102">säŋɨ-za-ŋ</ta>
            <ta e="T104" id="Seg_1006" s="T103">toppɨ-la-w</ta>
            <ta e="T105" id="Seg_1007" s="T104">meŋa-nan</ta>
            <ta e="T106" id="Seg_1008" s="T105">čangu-mbi-za-dɨt</ta>
            <ta e="T107" id="Seg_1009" s="T106">man</ta>
            <ta e="T108" id="Seg_1010" s="T107">ne</ta>
            <ta e="T109" id="Seg_1011" s="T108">smok</ta>
            <ta e="T110" id="Seg_1012" s="T109">maːt-tə</ta>
            <ta e="T111" id="Seg_1013" s="T110">mittɨ-gu</ta>
            <ta e="T112" id="Seg_1014" s="T111">mazɨm</ta>
            <ta e="T113" id="Seg_1015" s="T112">qu-la</ta>
            <ta e="T114" id="Seg_1016" s="T113">tɨtʼä</ta>
            <ta e="T115" id="Seg_1017" s="T114">tat-pɨ-za-dɨt</ta>
            <ta e="T116" id="Seg_1018" s="T115">nʼäja-m</ta>
            <ta e="T117" id="Seg_1019" s="T116">man</ta>
            <ta e="T118" id="Seg_1020" s="T117">qai-m-na</ta>
            <ta e="T119" id="Seg_1021" s="T118">assä</ta>
            <ta e="T120" id="Seg_1022" s="T119">qwäs-sa-w</ta>
            <ta e="T121" id="Seg_1023" s="T120">man</ta>
            <ta e="T122" id="Seg_1024" s="T121">qwäs-sa-w</ta>
            <ta e="T123" id="Seg_1025" s="T122">üŋunʼǯu-m</ta>
            <ta e="T124" id="Seg_1026" s="T123">man</ta>
            <ta e="T125" id="Seg_1027" s="T124">täp-ɨ-la-ni</ta>
            <ta e="T126" id="Seg_1028" s="T125">mi-ssa-m</ta>
            <ta e="T127" id="Seg_1029" s="T126">täp</ta>
            <ta e="T128" id="Seg_1030" s="T127">meŋa-nan</ta>
            <ta e="T129" id="Seg_1031" s="T128">iː-za-t</ta>
            <ta e="T130" id="Seg_1032" s="T129">muqtut</ta>
            <ta e="T131" id="Seg_1033" s="T130">rublʼej</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1034" s="T1">man</ta>
            <ta e="T3" id="Seg_1035" s="T2">qwən-sɨ-ŋ</ta>
            <ta e="T4" id="Seg_1036" s="T3">qɨndabɨ-qəntɨ</ta>
            <ta e="T5" id="Seg_1037" s="T4">matʼtʼi-ŋ-mbɨ-lɨ-le</ta>
            <ta e="T6" id="Seg_1038" s="T5">i</ta>
            <ta e="T7" id="Seg_1039" s="T6">maɣɨ-lǯi-mbɨ-sɨ-n</ta>
            <ta e="T8" id="Seg_1040" s="T7">wattə-qən</ta>
            <ta e="T9" id="Seg_1041" s="T8">qaj-naj</ta>
            <ta e="T10" id="Seg_1042" s="T9">qum</ta>
            <ta e="T11" id="Seg_1043" s="T10">tʼäŋu-sɨ</ta>
            <ta e="T12" id="Seg_1044" s="T11">man</ta>
            <ta e="T13" id="Seg_1045" s="T12">okkɨr-nɨ-k</ta>
            <ta e="T14" id="Seg_1046" s="T13">eː-sɨ-ŋ</ta>
            <ta e="T15" id="Seg_1047" s="T14">qaj-naj</ta>
            <ta e="T16" id="Seg_1048" s="T15">tʼäŋu-sɨ</ta>
            <ta e="T17" id="Seg_1049" s="T16">man-se</ta>
            <ta e="T18" id="Seg_1050" s="T17">mašim</ta>
            <ta e="T19" id="Seg_1051" s="T18">nʼünʼü-ka-sɨ-k</ta>
            <ta e="T20" id="Seg_1052" s="T19">üt</ta>
            <ta e="T21" id="Seg_1053" s="T20">šeːr-mbɨ-sɨ-tɨ</ta>
            <ta e="T22" id="Seg_1054" s="T21">man</ta>
            <ta e="T23" id="Seg_1055" s="T22">wattə-qɨniŋ</ta>
            <ta e="T24" id="Seg_1056" s="T23">qo-nče-r-sɨ-m</ta>
            <ta e="T25" id="Seg_1057" s="T24">suːrǝm</ta>
            <ta e="T26" id="Seg_1058" s="T25">nakkɨr</ta>
            <ta e="T27" id="Seg_1059" s="T26">man</ta>
            <ta e="T28" id="Seg_1060" s="T27">kɨkkɨ-sɨ-ŋ</ta>
            <ta e="T29" id="Seg_1061" s="T28">tap-ɨ-la-n-nan</ta>
            <ta e="T30" id="Seg_1062" s="T29">kuːnɨ-gu</ta>
            <ta e="T31" id="Seg_1063" s="T30">nu</ta>
            <ta e="T32" id="Seg_1064" s="T31">mäkkä</ta>
            <ta e="T34" id="Seg_1065" s="T33">tʼaččɨ-gu</ta>
            <ta e="T35" id="Seg_1066" s="T34">tap-ɨ-la-m</ta>
            <ta e="T36" id="Seg_1067" s="T35">tʼülʼdisä-se</ta>
            <ta e="T37" id="Seg_1068" s="T36">nu</ta>
            <ta e="T38" id="Seg_1069" s="T37">man</ta>
            <ta e="T39" id="Seg_1070" s="T38">qaj-m-naj</ta>
            <ta e="T40" id="Seg_1071" s="T39">qwat-sɨ-ŋ</ta>
            <ta e="T41" id="Seg_1072" s="T40">tap-ɨ-la-m</ta>
            <ta e="T42" id="Seg_1073" s="T41">nu</ta>
            <ta e="T43" id="Seg_1074" s="T42">suːrǝm-la</ta>
            <ta e="T44" id="Seg_1075" s="T43">wesʼ</ta>
            <ta e="T45" id="Seg_1076" s="T44">qwən-ɨ-lɨ-sɨ-tɨt</ta>
            <ta e="T46" id="Seg_1077" s="T45">man</ta>
            <ta e="T47" id="Seg_1078" s="T46">okkɨr-nɨ</ta>
            <ta e="T48" id="Seg_1079" s="T47">qalɨ-sɨ-ŋ</ta>
            <ta e="T49" id="Seg_1080" s="T48">tüː-n</ta>
            <ta e="T50" id="Seg_1081" s="T49">toqqän</ta>
            <ta e="T51" id="Seg_1082" s="T50">man</ta>
            <ta e="T52" id="Seg_1083" s="T51">okkɨr-ɨ-nɨ</ta>
            <ta e="T53" id="Seg_1084" s="T52">qalɨ-sɨ-ŋ</ta>
            <ta e="T54" id="Seg_1085" s="T53">kuːča-naj</ta>
            <ta e="T55" id="Seg_1086" s="T54">qaj-naj</ta>
            <ta e="T56" id="Seg_1087" s="T55">tʼäŋu-sɨ</ta>
            <ta e="T57" id="Seg_1088" s="T56">man</ta>
            <ta e="T58" id="Seg_1089" s="T57">qwən-sɨ-ŋ</ta>
            <ta e="T59" id="Seg_1090" s="T58">nʼarrɨ-mɨn</ta>
            <ta e="T60" id="Seg_1091" s="T59">man</ta>
            <ta e="T61" id="Seg_1092" s="T60">*maɣə-lʼčǝ-sɨ-ŋ</ta>
            <ta e="T62" id="Seg_1093" s="T61">qoŋ-sɨ-ŋ</ta>
            <ta e="T63" id="Seg_1094" s="T62">kɨke-ndɨ</ta>
            <ta e="T64" id="Seg_1095" s="T63">man</ta>
            <ta e="T65" id="Seg_1096" s="T64">wesʼ</ta>
            <ta e="T66" id="Seg_1097" s="T65">üt-ɨ-m-mbɨ-sɨ-ŋ</ta>
            <ta e="T67" id="Seg_1098" s="T66">spički</ta>
            <ta e="T68" id="Seg_1099" s="T67">tʼäŋu-sɨ</ta>
            <ta e="T69" id="Seg_1100" s="T68">nʼaj-mɨ</ta>
            <ta e="T70" id="Seg_1101" s="T69">tʼäŋu-sɨ</ta>
            <ta e="T71" id="Seg_1102" s="T70">paː-mɨ</ta>
            <ta e="T72" id="Seg_1103" s="T71">tʼäŋu-sɨ</ta>
            <ta e="T73" id="Seg_1104" s="T72">tolʼdʼi-la-mɨ</ta>
            <ta e="T74" id="Seg_1105" s="T73">eː-sɨ-tɨt</ta>
            <ta e="T75" id="Seg_1106" s="T74">naj</ta>
            <ta e="T76" id="Seg_1107" s="T75">te-mbɨ-tɨt</ta>
            <ta e="T77" id="Seg_1108" s="T76">tiːtam</ta>
            <ta e="T78" id="Seg_1109" s="T77">topǝ-la-mɨ</ta>
            <ta e="T79" id="Seg_1110" s="T78">küs-ŋɨ-ku-m-ŋɨ-tɨt</ta>
            <ta e="T80" id="Seg_1111" s="T79">paktǝ-r-sɨ-m</ta>
            <ta e="T81" id="Seg_1112" s="T80">šittə</ta>
            <ta e="T82" id="Seg_1113" s="T81">topǝ-mɨ</ta>
            <ta e="T83" id="Seg_1114" s="T82">man</ta>
            <ta e="T84" id="Seg_1115" s="T83">čaːǯɨ-sɨ-n</ta>
            <ta e="T85" id="Seg_1116" s="T84">nakkɨr</ta>
            <ta e="T86" id="Seg_1117" s="T85">tʼeːlɨ-tɨ</ta>
            <ta e="T87" id="Seg_1118" s="T86">tättɨ-mtelǯij</ta>
            <ta e="T88" id="Seg_1119" s="T87">tʼeːlɨ-tɨ</ta>
            <ta e="T89" id="Seg_1120" s="T88">man</ta>
            <ta e="T90" id="Seg_1121" s="T89">qotä</ta>
            <ta e="T91" id="Seg_1122" s="T90">čaːǯɨ-sɨ-ŋ</ta>
            <ta e="T92" id="Seg_1123" s="T91">man</ta>
            <ta e="T93" id="Seg_1124" s="T92">mittɨ-sɨ-ŋ</ta>
            <ta e="T94" id="Seg_1125" s="T93">Qät-ɨ-n</ta>
            <ta e="T95" id="Seg_1126" s="T94">kraj-%%</ta>
            <ta e="T96" id="Seg_1127" s="T95">maːduŋga-ndɨ</ta>
            <ta e="T97" id="Seg_1128" s="T96">maːduŋga-ndɨ</ta>
            <ta e="T98" id="Seg_1129" s="T97">man</ta>
            <ta e="T99" id="Seg_1130" s="T98">tüː-sɨ-ŋ</ta>
            <ta e="T100" id="Seg_1131" s="T99">šäqqɨ-gu</ta>
            <ta e="T101" id="Seg_1132" s="T100">nɨmtɨ</ta>
            <ta e="T102" id="Seg_1133" s="T101">man</ta>
            <ta e="T103" id="Seg_1134" s="T102">šäqqɨ-sɨ-ŋ</ta>
            <ta e="T104" id="Seg_1135" s="T103">topǝ-la-mɨ</ta>
            <ta e="T105" id="Seg_1136" s="T104">mäkkä-nan</ta>
            <ta e="T106" id="Seg_1137" s="T105">čangu-mbɨ-sɨ-tɨt</ta>
            <ta e="T107" id="Seg_1138" s="T106">man</ta>
            <ta e="T108" id="Seg_1139" s="T107">ne</ta>
            <ta e="T109" id="Seg_1140" s="T108">smok</ta>
            <ta e="T110" id="Seg_1141" s="T109">maːt-ndɨ</ta>
            <ta e="T111" id="Seg_1142" s="T110">mittɨ-gu</ta>
            <ta e="T112" id="Seg_1143" s="T111">mašim</ta>
            <ta e="T113" id="Seg_1144" s="T112">qum-la</ta>
            <ta e="T114" id="Seg_1145" s="T113">təndə</ta>
            <ta e="T115" id="Seg_1146" s="T114">tadɨ-mbɨ-sɨ-tɨt</ta>
            <ta e="T116" id="Seg_1147" s="T115">nʼaja-m</ta>
            <ta e="T117" id="Seg_1148" s="T116">man</ta>
            <ta e="T118" id="Seg_1149" s="T117">qaj-m-naj</ta>
            <ta e="T119" id="Seg_1150" s="T118">assɨ</ta>
            <ta e="T120" id="Seg_1151" s="T119">qwat-sɨ-m</ta>
            <ta e="T121" id="Seg_1152" s="T120">man</ta>
            <ta e="T122" id="Seg_1153" s="T121">qwat-sɨ-m</ta>
            <ta e="T123" id="Seg_1154" s="T122">üŋgɨnǯu-m</ta>
            <ta e="T124" id="Seg_1155" s="T123">man</ta>
            <ta e="T125" id="Seg_1156" s="T124">tap-ɨ-la-nɨ</ta>
            <ta e="T126" id="Seg_1157" s="T125">mi-sɨ-m</ta>
            <ta e="T127" id="Seg_1158" s="T126">tap</ta>
            <ta e="T128" id="Seg_1159" s="T127">mäkkä-nan</ta>
            <ta e="T129" id="Seg_1160" s="T128">iː-sɨ-tɨ</ta>
            <ta e="T130" id="Seg_1161" s="T129">muktut</ta>
            <ta e="T131" id="Seg_1162" s="T130">rublʼej</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1163" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_1164" s="T2">go.away-PST-1SG.S</ta>
            <ta e="T4" id="Seg_1165" s="T3">%%-ILL.3SG</ta>
            <ta e="T5" id="Seg_1166" s="T4">taiga-VBLZ-DUR-RES-CVB</ta>
            <ta e="T6" id="Seg_1167" s="T5">and</ta>
            <ta e="T7" id="Seg_1168" s="T6">examine-DRV-DUR-PST-3SG.S</ta>
            <ta e="T8" id="Seg_1169" s="T7">road-LOC</ta>
            <ta e="T9" id="Seg_1170" s="T8">what-EMPH</ta>
            <ta e="T10" id="Seg_1171" s="T9">human.being.[NOM]</ta>
            <ta e="T11" id="Seg_1172" s="T10">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T12" id="Seg_1173" s="T11">I.NOM</ta>
            <ta e="T13" id="Seg_1174" s="T12">one-OBL.1SG-ADVZ</ta>
            <ta e="T14" id="Seg_1175" s="T13">be-PST-1SG.S</ta>
            <ta e="T15" id="Seg_1176" s="T14">what-EMPH</ta>
            <ta e="T16" id="Seg_1177" s="T15">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T17" id="Seg_1178" s="T16">I.NOM-COM</ta>
            <ta e="T18" id="Seg_1179" s="T17">I.ACC</ta>
            <ta e="T19" id="Seg_1180" s="T18">small-DIM-DRV-ADVZ</ta>
            <ta e="T20" id="Seg_1181" s="T19">water.[NOM]</ta>
            <ta e="T21" id="Seg_1182" s="T20">come.in-PST.NAR-PST-3SG.O</ta>
            <ta e="T22" id="Seg_1183" s="T21">I.NOM</ta>
            <ta e="T23" id="Seg_1184" s="T22">road-ILL.1SG</ta>
            <ta e="T24" id="Seg_1185" s="T23">see-IPFV3-FRQ-PST-1SG.O</ta>
            <ta e="T25" id="Seg_1186" s="T24">wild.animal.[NOM]</ta>
            <ta e="T26" id="Seg_1187" s="T25">three</ta>
            <ta e="T27" id="Seg_1188" s="T26">I.NOM</ta>
            <ta e="T28" id="Seg_1189" s="T27">want-PST-1SG.S</ta>
            <ta e="T29" id="Seg_1190" s="T28">(s)he-EP-PL-GEN-ADES</ta>
            <ta e="T30" id="Seg_1191" s="T29">run.away-INF</ta>
            <ta e="T31" id="Seg_1192" s="T30">now</ta>
            <ta e="T32" id="Seg_1193" s="T31">I.ALL</ta>
            <ta e="T34" id="Seg_1194" s="T33">shoot-INF</ta>
            <ta e="T35" id="Seg_1195" s="T34">(s)he-EP-PL-ACC</ta>
            <ta e="T36" id="Seg_1196" s="T35">weapon-INSTR</ta>
            <ta e="T37" id="Seg_1197" s="T36">now</ta>
            <ta e="T38" id="Seg_1198" s="T37">I.NOM</ta>
            <ta e="T39" id="Seg_1199" s="T38">what-ACC-EMPH</ta>
            <ta e="T40" id="Seg_1200" s="T39">kill-PST-1SG.S</ta>
            <ta e="T41" id="Seg_1201" s="T40">(s)he-EP-PL-ACC</ta>
            <ta e="T42" id="Seg_1202" s="T41">now</ta>
            <ta e="T43" id="Seg_1203" s="T42">wild.animal-PL.[NOM]</ta>
            <ta e="T44" id="Seg_1204" s="T43">all</ta>
            <ta e="T45" id="Seg_1205" s="T44">go.away-EP-RES-PST-3PL</ta>
            <ta e="T46" id="Seg_1206" s="T45">I.NOM</ta>
            <ta e="T47" id="Seg_1207" s="T46">one-OBL.1SG</ta>
            <ta e="T48" id="Seg_1208" s="T47">stay-PST-1SG.S</ta>
            <ta e="T49" id="Seg_1209" s="T48">fire-GEN</ta>
            <ta e="T50" id="Seg_1210" s="T49">around</ta>
            <ta e="T51" id="Seg_1211" s="T50">I.NOM</ta>
            <ta e="T52" id="Seg_1212" s="T51">one-EP-OBL.1SG</ta>
            <ta e="T53" id="Seg_1213" s="T52">stay-PST-1SG.S</ta>
            <ta e="T54" id="Seg_1214" s="T53">where-EMPH</ta>
            <ta e="T55" id="Seg_1215" s="T54">what-EMPH</ta>
            <ta e="T56" id="Seg_1216" s="T55">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T57" id="Seg_1217" s="T56">I.NOM</ta>
            <ta e="T58" id="Seg_1218" s="T57">go.away-PST-1SG.S</ta>
            <ta e="T59" id="Seg_1219" s="T58">swamp-PROL</ta>
            <ta e="T60" id="Seg_1220" s="T59">I.NOM</ta>
            <ta e="T61" id="Seg_1221" s="T60">lose-PFV-PST-1SG.S</ta>
            <ta e="T62" id="Seg_1222" s="T61">drown-PST-1SG.S</ta>
            <ta e="T63" id="Seg_1223" s="T62">river-ILL</ta>
            <ta e="T64" id="Seg_1224" s="T63">I.NOM</ta>
            <ta e="T65" id="Seg_1225" s="T64">all</ta>
            <ta e="T66" id="Seg_1226" s="T65">water-EP-TRL-DUR-PST-1SG.S</ta>
            <ta e="T67" id="Seg_1227" s="T66">matchstick.PL.[NOM]</ta>
            <ta e="T68" id="Seg_1228" s="T67">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T69" id="Seg_1229" s="T68">bread.[NOM]-1SG</ta>
            <ta e="T70" id="Seg_1230" s="T69">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T71" id="Seg_1231" s="T70">knife.[NOM]-1SG</ta>
            <ta e="T72" id="Seg_1232" s="T71">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T73" id="Seg_1233" s="T72">ski-PL.[NOM]-1SG</ta>
            <ta e="T74" id="Seg_1234" s="T73">be-PST-3PL</ta>
            <ta e="T75" id="Seg_1235" s="T74">also</ta>
            <ta e="T76" id="Seg_1236" s="T75">go.bad-PST.NAR-3PL</ta>
            <ta e="T77" id="Seg_1237" s="T76">now</ta>
            <ta e="T78" id="Seg_1238" s="T77">leg-PL.[NOM]-1SG</ta>
            <ta e="T79" id="Seg_1239" s="T78">hurt-CO-HAB-TRL-CO-3PL</ta>
            <ta e="T80" id="Seg_1240" s="T79">freeze-FRQ-PST-1SG.O</ta>
            <ta e="T81" id="Seg_1241" s="T80">two</ta>
            <ta e="T82" id="Seg_1242" s="T81">leg.[NOM]-1SG</ta>
            <ta e="T83" id="Seg_1243" s="T82">I.NOM</ta>
            <ta e="T84" id="Seg_1244" s="T83">travel-PST-3SG.S</ta>
            <ta e="T85" id="Seg_1245" s="T84">three</ta>
            <ta e="T86" id="Seg_1246" s="T85">sun.[NOM]-3SG</ta>
            <ta e="T87" id="Seg_1247" s="T86">four-ORD</ta>
            <ta e="T88" id="Seg_1248" s="T87">day.[NOM]-3SG</ta>
            <ta e="T89" id="Seg_1249" s="T88">I.NOM</ta>
            <ta e="T90" id="Seg_1250" s="T89">back</ta>
            <ta e="T91" id="Seg_1251" s="T90">travel-PST-1SG.S</ta>
            <ta e="T92" id="Seg_1252" s="T91">I.NOM</ta>
            <ta e="T93" id="Seg_1253" s="T92">reach-PST-1SG.S</ta>
            <ta e="T94" id="Seg_1254" s="T93">Ket-EP-GEN</ta>
            <ta e="T95" id="Seg_1255" s="T94">border-%%</ta>
            <ta e="T96" id="Seg_1256" s="T95">house-ILL</ta>
            <ta e="T97" id="Seg_1257" s="T96">house-ILL</ta>
            <ta e="T98" id="Seg_1258" s="T97">I.NOM</ta>
            <ta e="T99" id="Seg_1259" s="T98">come-PST-1SG.S</ta>
            <ta e="T100" id="Seg_1260" s="T99">spend.night-INF</ta>
            <ta e="T101" id="Seg_1261" s="T100">here</ta>
            <ta e="T102" id="Seg_1262" s="T101">I.NOM</ta>
            <ta e="T103" id="Seg_1263" s="T102">spend.night-PST-1SG.S</ta>
            <ta e="T104" id="Seg_1264" s="T103">leg-PL.[NOM]-1SG</ta>
            <ta e="T105" id="Seg_1265" s="T104">I.ALL-ADES</ta>
            <ta e="T106" id="Seg_1266" s="T105">swollen-DUR-PST-3PL</ta>
            <ta e="T107" id="Seg_1267" s="T106">I.NOM</ta>
            <ta e="T108" id="Seg_1268" s="T107">NEG</ta>
            <ta e="T109" id="Seg_1269" s="T108">can.PST.3SG</ta>
            <ta e="T110" id="Seg_1270" s="T109">house-ILL</ta>
            <ta e="T111" id="Seg_1271" s="T110">reach-INF</ta>
            <ta e="T112" id="Seg_1272" s="T111">I.ACC</ta>
            <ta e="T113" id="Seg_1273" s="T112">human.being-PL.[NOM]</ta>
            <ta e="T114" id="Seg_1274" s="T113">here</ta>
            <ta e="T115" id="Seg_1275" s="T114">bring-DUR-PST-3PL</ta>
            <ta e="T116" id="Seg_1276" s="T115">squirrel-ACC</ta>
            <ta e="T117" id="Seg_1277" s="T116">I.NOM</ta>
            <ta e="T118" id="Seg_1278" s="T117">what-ACC-EMPH</ta>
            <ta e="T119" id="Seg_1279" s="T118">NEG</ta>
            <ta e="T120" id="Seg_1280" s="T119">kill-PST-1SG.O</ta>
            <ta e="T121" id="Seg_1281" s="T120">I.NOM</ta>
            <ta e="T122" id="Seg_1282" s="T121">kill-PST-1SG.O</ta>
            <ta e="T123" id="Seg_1283" s="T122">wolverine-ACC</ta>
            <ta e="T124" id="Seg_1284" s="T123">I.NOM</ta>
            <ta e="T125" id="Seg_1285" s="T124">(s)he-EP-PL-ALL</ta>
            <ta e="T126" id="Seg_1286" s="T125">give-PST-1SG.O</ta>
            <ta e="T127" id="Seg_1287" s="T126">(s)he.[NOM]</ta>
            <ta e="T128" id="Seg_1288" s="T127">I.ALL-ADES</ta>
            <ta e="T129" id="Seg_1289" s="T128">take-PST-3SG.O</ta>
            <ta e="T130" id="Seg_1290" s="T129">six</ta>
            <ta e="T131" id="Seg_1291" s="T130">ruble.PL.GEN</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1292" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_1293" s="T2">уйти-PST-1SG.S</ta>
            <ta e="T4" id="Seg_1294" s="T3">%%-ILL.3SG</ta>
            <ta e="T5" id="Seg_1295" s="T4">тайга-VBLZ-DUR-RES-CVB</ta>
            <ta e="T6" id="Seg_1296" s="T5">и</ta>
            <ta e="T7" id="Seg_1297" s="T6">оглядеть--DUR-PST-3SG.S</ta>
            <ta e="T8" id="Seg_1298" s="T7">дорога-LOC</ta>
            <ta e="T9" id="Seg_1299" s="T8">что-EMPH</ta>
            <ta e="T10" id="Seg_1300" s="T9">человек.[NOM]</ta>
            <ta e="T11" id="Seg_1301" s="T10">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T12" id="Seg_1302" s="T11">я.NOM</ta>
            <ta e="T13" id="Seg_1303" s="T12">один-OBL.1SG-ADVZ</ta>
            <ta e="T14" id="Seg_1304" s="T13">быть-PST-1SG.S</ta>
            <ta e="T15" id="Seg_1305" s="T14">что-EMPH</ta>
            <ta e="T16" id="Seg_1306" s="T15">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T17" id="Seg_1307" s="T16">я.NOM-COM</ta>
            <ta e="T18" id="Seg_1308" s="T17">я.ACC</ta>
            <ta e="T19" id="Seg_1309" s="T18">маленький-DIM-DRV-ADVZ</ta>
            <ta e="T20" id="Seg_1310" s="T19">вода.[NOM]</ta>
            <ta e="T21" id="Seg_1311" s="T20">войти-PST.NAR-PST-3SG.O</ta>
            <ta e="T22" id="Seg_1312" s="T21">я.NOM</ta>
            <ta e="T23" id="Seg_1313" s="T22">дорога-ILL.1SG</ta>
            <ta e="T24" id="Seg_1314" s="T23">увидеть-IPFV3-FRQ-PST-1SG.O</ta>
            <ta e="T25" id="Seg_1315" s="T24">зверь.[NOM]</ta>
            <ta e="T26" id="Seg_1316" s="T25">три</ta>
            <ta e="T27" id="Seg_1317" s="T26">я.NOM</ta>
            <ta e="T28" id="Seg_1318" s="T27">хотеть-PST-1SG.S</ta>
            <ta e="T29" id="Seg_1319" s="T28">он(а)-EP-PL-GEN-ADES</ta>
            <ta e="T30" id="Seg_1320" s="T29">убежать-INF</ta>
            <ta e="T31" id="Seg_1321" s="T30">ну</ta>
            <ta e="T32" id="Seg_1322" s="T31">я.ALL</ta>
            <ta e="T34" id="Seg_1323" s="T33">стрелять-INF</ta>
            <ta e="T35" id="Seg_1324" s="T34">он(а)-EP-PL-ACC</ta>
            <ta e="T36" id="Seg_1325" s="T35">ружье-INSTR</ta>
            <ta e="T37" id="Seg_1326" s="T36">ну</ta>
            <ta e="T38" id="Seg_1327" s="T37">я.NOM</ta>
            <ta e="T39" id="Seg_1328" s="T38">что-ACC-EMPH</ta>
            <ta e="T40" id="Seg_1329" s="T39">убить-PST-1SG.S</ta>
            <ta e="T41" id="Seg_1330" s="T40">он(а)-EP-PL-ACC</ta>
            <ta e="T42" id="Seg_1331" s="T41">ну</ta>
            <ta e="T43" id="Seg_1332" s="T42">зверь-PL.[NOM]</ta>
            <ta e="T44" id="Seg_1333" s="T43">весь</ta>
            <ta e="T45" id="Seg_1334" s="T44">уйти-EP-RES-PST-3PL</ta>
            <ta e="T46" id="Seg_1335" s="T45">я.NOM</ta>
            <ta e="T47" id="Seg_1336" s="T46">один-OBL.1SG</ta>
            <ta e="T48" id="Seg_1337" s="T47">остаться-PST-1SG.S</ta>
            <ta e="T49" id="Seg_1338" s="T48">огонь-GEN</ta>
            <ta e="T50" id="Seg_1339" s="T49">около</ta>
            <ta e="T51" id="Seg_1340" s="T50">я.NOM</ta>
            <ta e="T52" id="Seg_1341" s="T51">один-EP-OBL.1SG</ta>
            <ta e="T53" id="Seg_1342" s="T52">остаться-PST-1SG.S</ta>
            <ta e="T54" id="Seg_1343" s="T53">куда-EMPH</ta>
            <ta e="T55" id="Seg_1344" s="T54">что-EMPH</ta>
            <ta e="T56" id="Seg_1345" s="T55">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T57" id="Seg_1346" s="T56">я.NOM</ta>
            <ta e="T58" id="Seg_1347" s="T57">уйти-PST-1SG.S</ta>
            <ta e="T59" id="Seg_1348" s="T58">болото-PROL</ta>
            <ta e="T60" id="Seg_1349" s="T59">я.NOM</ta>
            <ta e="T61" id="Seg_1350" s="T60">заблудиться-PFV-PST-1SG.S</ta>
            <ta e="T62" id="Seg_1351" s="T61">утонуть-PST-1SG.S</ta>
            <ta e="T63" id="Seg_1352" s="T62">река-ILL</ta>
            <ta e="T64" id="Seg_1353" s="T63">я.NOM</ta>
            <ta e="T65" id="Seg_1354" s="T64">весь</ta>
            <ta e="T66" id="Seg_1355" s="T65">вода-EP-TRL-DUR-PST-1SG.S</ta>
            <ta e="T67" id="Seg_1356" s="T66">спичка.PL.[NOM]</ta>
            <ta e="T68" id="Seg_1357" s="T67">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T69" id="Seg_1358" s="T68">хлеб.[NOM]-1SG</ta>
            <ta e="T70" id="Seg_1359" s="T69">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T71" id="Seg_1360" s="T70">нож.[NOM]-1SG</ta>
            <ta e="T72" id="Seg_1361" s="T71">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T73" id="Seg_1362" s="T72">лыжа-PL.[NOM]-1SG</ta>
            <ta e="T74" id="Seg_1363" s="T73">быть-PST-3PL</ta>
            <ta e="T75" id="Seg_1364" s="T74">тоже</ta>
            <ta e="T76" id="Seg_1365" s="T75">сгнить-PST.NAR-3PL</ta>
            <ta e="T77" id="Seg_1366" s="T76">сейчас</ta>
            <ta e="T78" id="Seg_1367" s="T77">нога-PL.[NOM]-1SG</ta>
            <ta e="T79" id="Seg_1368" s="T78">болеть-CO-HAB-TRL-CO-3PL</ta>
            <ta e="T80" id="Seg_1369" s="T79">обморозить-FRQ-PST-1SG.O</ta>
            <ta e="T81" id="Seg_1370" s="T80">два</ta>
            <ta e="T82" id="Seg_1371" s="T81">нога.[NOM]-1SG</ta>
            <ta e="T83" id="Seg_1372" s="T82">я.NOM</ta>
            <ta e="T84" id="Seg_1373" s="T83">ехать-PST-3SG.S</ta>
            <ta e="T85" id="Seg_1374" s="T84">три</ta>
            <ta e="T86" id="Seg_1375" s="T85">солнце.[NOM]-3SG</ta>
            <ta e="T87" id="Seg_1376" s="T86">четыре-ORD</ta>
            <ta e="T88" id="Seg_1377" s="T87">день.[NOM]-3SG</ta>
            <ta e="T89" id="Seg_1378" s="T88">я.NOM</ta>
            <ta e="T90" id="Seg_1379" s="T89">назад</ta>
            <ta e="T91" id="Seg_1380" s="T90">ехать-PST-1SG.S</ta>
            <ta e="T92" id="Seg_1381" s="T91">я.NOM</ta>
            <ta e="T93" id="Seg_1382" s="T92">дойти-PST-1SG.S</ta>
            <ta e="T94" id="Seg_1383" s="T93">Кеть-EP-GEN</ta>
            <ta e="T95" id="Seg_1384" s="T94">край-%%</ta>
            <ta e="T96" id="Seg_1385" s="T95">избушка-ILL</ta>
            <ta e="T97" id="Seg_1386" s="T96">избушка-ILL</ta>
            <ta e="T98" id="Seg_1387" s="T97">я.NOM</ta>
            <ta e="T99" id="Seg_1388" s="T98">прийти-PST-1SG.S</ta>
            <ta e="T100" id="Seg_1389" s="T99">ночевать-INF</ta>
            <ta e="T101" id="Seg_1390" s="T100">здесь</ta>
            <ta e="T102" id="Seg_1391" s="T101">я.NOM</ta>
            <ta e="T103" id="Seg_1392" s="T102">ночевать-PST-1SG.S</ta>
            <ta e="T104" id="Seg_1393" s="T103">нога-PL.[NOM]-1SG</ta>
            <ta e="T105" id="Seg_1394" s="T104">я.ALL-ADES</ta>
            <ta e="T106" id="Seg_1395" s="T105">опухнуть-DUR-PST-3PL</ta>
            <ta e="T107" id="Seg_1396" s="T106">я.NOM</ta>
            <ta e="T108" id="Seg_1397" s="T107">NEG</ta>
            <ta e="T109" id="Seg_1398" s="T108">мочь.PST.3SG</ta>
            <ta e="T110" id="Seg_1399" s="T109">дом-ILL</ta>
            <ta e="T111" id="Seg_1400" s="T110">дойти-INF</ta>
            <ta e="T112" id="Seg_1401" s="T111">я.ACC</ta>
            <ta e="T113" id="Seg_1402" s="T112">человек-PL.[NOM]</ta>
            <ta e="T114" id="Seg_1403" s="T113">здесь</ta>
            <ta e="T115" id="Seg_1404" s="T114">принести-DUR-PST-3PL</ta>
            <ta e="T116" id="Seg_1405" s="T115">белка-ACC</ta>
            <ta e="T117" id="Seg_1406" s="T116">я.NOM</ta>
            <ta e="T118" id="Seg_1407" s="T117">что-ACC-EMPH</ta>
            <ta e="T119" id="Seg_1408" s="T118">NEG</ta>
            <ta e="T120" id="Seg_1409" s="T119">убить-PST-1SG.O</ta>
            <ta e="T121" id="Seg_1410" s="T120">я.NOM</ta>
            <ta e="T122" id="Seg_1411" s="T121">убить-PST-1SG.O</ta>
            <ta e="T123" id="Seg_1412" s="T122">росомаха-ACC</ta>
            <ta e="T124" id="Seg_1413" s="T123">я.NOM</ta>
            <ta e="T125" id="Seg_1414" s="T124">он(а)-EP-PL-ALL</ta>
            <ta e="T126" id="Seg_1415" s="T125">дать-PST-1SG.O</ta>
            <ta e="T127" id="Seg_1416" s="T126">он(а).[NOM]</ta>
            <ta e="T128" id="Seg_1417" s="T127">я.ALL-ADES</ta>
            <ta e="T129" id="Seg_1418" s="T128">взять-PST-3SG.O</ta>
            <ta e="T130" id="Seg_1419" s="T129">шесть</ta>
            <ta e="T131" id="Seg_1420" s="T130">рубль.PL.GEN</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1421" s="T1">pers</ta>
            <ta e="T3" id="Seg_1422" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_1423" s="T3">n-n:case.poss</ta>
            <ta e="T5" id="Seg_1424" s="T4">n-n&gt;v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T6" id="Seg_1425" s="T5">conj</ta>
            <ta e="T7" id="Seg_1426" s="T6">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_1427" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_1428" s="T8">interrog-clit</ta>
            <ta e="T10" id="Seg_1429" s="T9">n.[n:case]</ta>
            <ta e="T11" id="Seg_1430" s="T10">v-v:tense.[v:pn]</ta>
            <ta e="T12" id="Seg_1431" s="T11">pers</ta>
            <ta e="T13" id="Seg_1432" s="T12">num-n:obl.poss-num&gt;adv</ta>
            <ta e="T14" id="Seg_1433" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_1434" s="T14">interrog-clit</ta>
            <ta e="T16" id="Seg_1435" s="T15">v-v:tense.[v:pn]</ta>
            <ta e="T17" id="Seg_1436" s="T16">pers-n:case</ta>
            <ta e="T18" id="Seg_1437" s="T17">pers</ta>
            <ta e="T19" id="Seg_1438" s="T18">adj-n&gt;n-adj&gt;adj-adj&gt;adv</ta>
            <ta e="T20" id="Seg_1439" s="T19">n.[n:case]</ta>
            <ta e="T21" id="Seg_1440" s="T20">v-v:tense-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_1441" s="T21">pers</ta>
            <ta e="T23" id="Seg_1442" s="T22">n-n:case.poss</ta>
            <ta e="T24" id="Seg_1443" s="T23">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_1444" s="T24">n.[n:case]</ta>
            <ta e="T26" id="Seg_1445" s="T25">num</ta>
            <ta e="T27" id="Seg_1446" s="T26">pers</ta>
            <ta e="T28" id="Seg_1447" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_1448" s="T28">pers-n:ins-n:num-n:case-n:case</ta>
            <ta e="T30" id="Seg_1449" s="T29">v-v:inf</ta>
            <ta e="T31" id="Seg_1450" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_1451" s="T31">pers</ta>
            <ta e="T34" id="Seg_1452" s="T33">v-v:inf</ta>
            <ta e="T35" id="Seg_1453" s="T34">pers-n:ins-n:num-n:case</ta>
            <ta e="T36" id="Seg_1454" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_1455" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_1456" s="T37">pers</ta>
            <ta e="T39" id="Seg_1457" s="T38">interrog-n:case-clit</ta>
            <ta e="T40" id="Seg_1458" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_1459" s="T40">pers-n:ins-n:num-n:case</ta>
            <ta e="T42" id="Seg_1460" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_1461" s="T42">n-n:num.[n:case]</ta>
            <ta e="T44" id="Seg_1462" s="T43">quant</ta>
            <ta e="T45" id="Seg_1463" s="T44">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_1464" s="T45">pers</ta>
            <ta e="T47" id="Seg_1465" s="T46">num-n:obl.poss</ta>
            <ta e="T48" id="Seg_1466" s="T47">v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_1467" s="T48">n-n:case</ta>
            <ta e="T50" id="Seg_1468" s="T49">pp</ta>
            <ta e="T51" id="Seg_1469" s="T50">pers</ta>
            <ta e="T52" id="Seg_1470" s="T51">num-n:ins-n:obl.poss</ta>
            <ta e="T53" id="Seg_1471" s="T52">v-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_1472" s="T53">interrog-clit</ta>
            <ta e="T55" id="Seg_1473" s="T54">interrog-clit</ta>
            <ta e="T56" id="Seg_1474" s="T55">v-v:tense.[v:pn]</ta>
            <ta e="T57" id="Seg_1475" s="T56">pers</ta>
            <ta e="T58" id="Seg_1476" s="T57">v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_1477" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_1478" s="T59">pers</ta>
            <ta e="T61" id="Seg_1479" s="T60">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_1480" s="T61">v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_1481" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_1482" s="T63">pers</ta>
            <ta e="T65" id="Seg_1483" s="T64">quant</ta>
            <ta e="T66" id="Seg_1484" s="T65">n-n:ins-n&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_1485" s="T66">n.[n:case]</ta>
            <ta e="T68" id="Seg_1486" s="T67">v-v:tense.[v:pn]</ta>
            <ta e="T69" id="Seg_1487" s="T68">n.[n:case]-n:poss</ta>
            <ta e="T70" id="Seg_1488" s="T69">v-v:tense.[v:pn]</ta>
            <ta e="T71" id="Seg_1489" s="T70">n.[n:case]-n:poss</ta>
            <ta e="T72" id="Seg_1490" s="T71">v-v:tense.[v:pn]</ta>
            <ta e="T73" id="Seg_1491" s="T72">n-n:num.[n:case]-n:poss</ta>
            <ta e="T74" id="Seg_1492" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_1493" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_1494" s="T75">v-v:tense-v:pn</ta>
            <ta e="T77" id="Seg_1495" s="T76">adv</ta>
            <ta e="T78" id="Seg_1496" s="T77">n-n:num.[n:case]-n:poss</ta>
            <ta e="T79" id="Seg_1497" s="T78">v-v:ins-v&gt;v-n&gt;v-v:ins-v:pn</ta>
            <ta e="T80" id="Seg_1498" s="T79">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_1499" s="T80">num</ta>
            <ta e="T82" id="Seg_1500" s="T81">n.[n:case]-n:poss</ta>
            <ta e="T83" id="Seg_1501" s="T82">pers</ta>
            <ta e="T84" id="Seg_1502" s="T83">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_1503" s="T84">num</ta>
            <ta e="T86" id="Seg_1504" s="T85">n.[n:case]-n:poss</ta>
            <ta e="T87" id="Seg_1505" s="T86">num-num&gt;adj</ta>
            <ta e="T88" id="Seg_1506" s="T87">n.[n:case]-n:poss</ta>
            <ta e="T89" id="Seg_1507" s="T88">pers</ta>
            <ta e="T90" id="Seg_1508" s="T89">adv</ta>
            <ta e="T91" id="Seg_1509" s="T90">v-v:tense-v:pn</ta>
            <ta e="T92" id="Seg_1510" s="T91">pers</ta>
            <ta e="T93" id="Seg_1511" s="T92">v-v:tense-v:pn</ta>
            <ta e="T94" id="Seg_1512" s="T93">nprop-n:ins-n:case</ta>
            <ta e="T95" id="Seg_1513" s="T94">n</ta>
            <ta e="T96" id="Seg_1514" s="T95">n-n:case</ta>
            <ta e="T97" id="Seg_1515" s="T96">n-n:case</ta>
            <ta e="T98" id="Seg_1516" s="T97">pers</ta>
            <ta e="T99" id="Seg_1517" s="T98">v-v:tense-v:pn</ta>
            <ta e="T100" id="Seg_1518" s="T99">v-v:inf</ta>
            <ta e="T101" id="Seg_1519" s="T100">adv</ta>
            <ta e="T102" id="Seg_1520" s="T101">pers</ta>
            <ta e="T103" id="Seg_1521" s="T102">v-v:tense-v:pn</ta>
            <ta e="T104" id="Seg_1522" s="T103">n-n:num.[n:case]-n:poss</ta>
            <ta e="T105" id="Seg_1523" s="T104">pers-n:case</ta>
            <ta e="T106" id="Seg_1524" s="T105">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T107" id="Seg_1525" s="T106">pers</ta>
            <ta e="T108" id="Seg_1526" s="T107">ptcl</ta>
            <ta e="T109" id="Seg_1527" s="T108">v</ta>
            <ta e="T110" id="Seg_1528" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_1529" s="T110">v-v:inf</ta>
            <ta e="T112" id="Seg_1530" s="T111">pers</ta>
            <ta e="T113" id="Seg_1531" s="T112">n-n:num.[n:case]</ta>
            <ta e="T114" id="Seg_1532" s="T113">adv</ta>
            <ta e="T115" id="Seg_1533" s="T114">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T116" id="Seg_1534" s="T115">n-n:case</ta>
            <ta e="T117" id="Seg_1535" s="T116">pers</ta>
            <ta e="T118" id="Seg_1536" s="T117">interrog-n:case-clit</ta>
            <ta e="T119" id="Seg_1537" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_1538" s="T119">v-v:tense-v:pn</ta>
            <ta e="T121" id="Seg_1539" s="T120">pers</ta>
            <ta e="T122" id="Seg_1540" s="T121">v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_1541" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_1542" s="T123">pers</ta>
            <ta e="T125" id="Seg_1543" s="T124">pers-n:ins-n:num-n:case</ta>
            <ta e="T126" id="Seg_1544" s="T125">v-v:tense-v:pn</ta>
            <ta e="T127" id="Seg_1545" s="T126">pers.[n:case]</ta>
            <ta e="T128" id="Seg_1546" s="T127">pers-n:case</ta>
            <ta e="T129" id="Seg_1547" s="T128">v-v:tense-v:pn</ta>
            <ta e="T130" id="Seg_1548" s="T129">num</ta>
            <ta e="T131" id="Seg_1549" s="T130">n</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1550" s="T1">pers</ta>
            <ta e="T3" id="Seg_1551" s="T2">v</ta>
            <ta e="T4" id="Seg_1552" s="T3">n</ta>
            <ta e="T5" id="Seg_1553" s="T4">v</ta>
            <ta e="T6" id="Seg_1554" s="T5">conj</ta>
            <ta e="T7" id="Seg_1555" s="T6">v</ta>
            <ta e="T8" id="Seg_1556" s="T7">n</ta>
            <ta e="T9" id="Seg_1557" s="T8">pro</ta>
            <ta e="T10" id="Seg_1558" s="T9">n</ta>
            <ta e="T11" id="Seg_1559" s="T10">v</ta>
            <ta e="T12" id="Seg_1560" s="T11">pers</ta>
            <ta e="T13" id="Seg_1561" s="T12">pro</ta>
            <ta e="T14" id="Seg_1562" s="T13">v</ta>
            <ta e="T15" id="Seg_1563" s="T14">pro</ta>
            <ta e="T16" id="Seg_1564" s="T15">v</ta>
            <ta e="T17" id="Seg_1565" s="T16">pers</ta>
            <ta e="T18" id="Seg_1566" s="T17">pers</ta>
            <ta e="T19" id="Seg_1567" s="T18">adj</ta>
            <ta e="T20" id="Seg_1568" s="T19">n</ta>
            <ta e="T21" id="Seg_1569" s="T20">v</ta>
            <ta e="T22" id="Seg_1570" s="T21">pers</ta>
            <ta e="T23" id="Seg_1571" s="T22">n</ta>
            <ta e="T24" id="Seg_1572" s="T23">v</ta>
            <ta e="T25" id="Seg_1573" s="T24">n</ta>
            <ta e="T26" id="Seg_1574" s="T25">num</ta>
            <ta e="T27" id="Seg_1575" s="T26">pers</ta>
            <ta e="T28" id="Seg_1576" s="T27">v</ta>
            <ta e="T29" id="Seg_1577" s="T28">pers</ta>
            <ta e="T30" id="Seg_1578" s="T29">v</ta>
            <ta e="T31" id="Seg_1579" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_1580" s="T31">pers</ta>
            <ta e="T34" id="Seg_1581" s="T33">v</ta>
            <ta e="T35" id="Seg_1582" s="T34">pers</ta>
            <ta e="T36" id="Seg_1583" s="T35">n</ta>
            <ta e="T37" id="Seg_1584" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_1585" s="T37">pers</ta>
            <ta e="T39" id="Seg_1586" s="T38">pro</ta>
            <ta e="T40" id="Seg_1587" s="T39">v</ta>
            <ta e="T41" id="Seg_1588" s="T40">pers</ta>
            <ta e="T42" id="Seg_1589" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_1590" s="T42">n</ta>
            <ta e="T44" id="Seg_1591" s="T43">quant</ta>
            <ta e="T45" id="Seg_1592" s="T44">v</ta>
            <ta e="T46" id="Seg_1593" s="T45">pers</ta>
            <ta e="T47" id="Seg_1594" s="T46">pro</ta>
            <ta e="T48" id="Seg_1595" s="T47">v</ta>
            <ta e="T49" id="Seg_1596" s="T48">n</ta>
            <ta e="T50" id="Seg_1597" s="T49">pp</ta>
            <ta e="T51" id="Seg_1598" s="T50">pers</ta>
            <ta e="T52" id="Seg_1599" s="T51">num</ta>
            <ta e="T53" id="Seg_1600" s="T52">v</ta>
            <ta e="T54" id="Seg_1601" s="T53">pro</ta>
            <ta e="T55" id="Seg_1602" s="T54">pro</ta>
            <ta e="T56" id="Seg_1603" s="T55">v</ta>
            <ta e="T57" id="Seg_1604" s="T56">pers</ta>
            <ta e="T58" id="Seg_1605" s="T57">v</ta>
            <ta e="T59" id="Seg_1606" s="T58">n</ta>
            <ta e="T60" id="Seg_1607" s="T59">pers</ta>
            <ta e="T61" id="Seg_1608" s="T60">v</ta>
            <ta e="T62" id="Seg_1609" s="T61">v</ta>
            <ta e="T63" id="Seg_1610" s="T62">n</ta>
            <ta e="T64" id="Seg_1611" s="T63">pers</ta>
            <ta e="T65" id="Seg_1612" s="T64">quant</ta>
            <ta e="T66" id="Seg_1613" s="T65">v</ta>
            <ta e="T67" id="Seg_1614" s="T66">n</ta>
            <ta e="T68" id="Seg_1615" s="T67">v</ta>
            <ta e="T69" id="Seg_1616" s="T68">n</ta>
            <ta e="T70" id="Seg_1617" s="T69">v</ta>
            <ta e="T71" id="Seg_1618" s="T70">n</ta>
            <ta e="T72" id="Seg_1619" s="T71">v</ta>
            <ta e="T73" id="Seg_1620" s="T72">n</ta>
            <ta e="T74" id="Seg_1621" s="T73">v</ta>
            <ta e="T75" id="Seg_1622" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_1623" s="T75">v</ta>
            <ta e="T77" id="Seg_1624" s="T76">adv</ta>
            <ta e="T78" id="Seg_1625" s="T77">n</ta>
            <ta e="T79" id="Seg_1626" s="T78">v</ta>
            <ta e="T80" id="Seg_1627" s="T79">v</ta>
            <ta e="T81" id="Seg_1628" s="T80">num</ta>
            <ta e="T82" id="Seg_1629" s="T81">n</ta>
            <ta e="T83" id="Seg_1630" s="T82">pers</ta>
            <ta e="T84" id="Seg_1631" s="T83">v</ta>
            <ta e="T85" id="Seg_1632" s="T84">num</ta>
            <ta e="T86" id="Seg_1633" s="T85">n</ta>
            <ta e="T87" id="Seg_1634" s="T86">adj</ta>
            <ta e="T88" id="Seg_1635" s="T87">n</ta>
            <ta e="T89" id="Seg_1636" s="T88">pers</ta>
            <ta e="T90" id="Seg_1637" s="T89">adv</ta>
            <ta e="T91" id="Seg_1638" s="T90">v</ta>
            <ta e="T92" id="Seg_1639" s="T91">pers</ta>
            <ta e="T93" id="Seg_1640" s="T92">v</ta>
            <ta e="T94" id="Seg_1641" s="T93">nprop</ta>
            <ta e="T96" id="Seg_1642" s="T95">n</ta>
            <ta e="T97" id="Seg_1643" s="T96">n</ta>
            <ta e="T98" id="Seg_1644" s="T97">pers</ta>
            <ta e="T99" id="Seg_1645" s="T98">v</ta>
            <ta e="T100" id="Seg_1646" s="T99">v</ta>
            <ta e="T101" id="Seg_1647" s="T100">adv</ta>
            <ta e="T102" id="Seg_1648" s="T101">pers</ta>
            <ta e="T103" id="Seg_1649" s="T102">v</ta>
            <ta e="T104" id="Seg_1650" s="T103">n</ta>
            <ta e="T105" id="Seg_1651" s="T104">pers</ta>
            <ta e="T106" id="Seg_1652" s="T105">v</ta>
            <ta e="T107" id="Seg_1653" s="T106">pers</ta>
            <ta e="T108" id="Seg_1654" s="T107">ptcl</ta>
            <ta e="T109" id="Seg_1655" s="T108">v</ta>
            <ta e="T110" id="Seg_1656" s="T109">n</ta>
            <ta e="T111" id="Seg_1657" s="T110">v</ta>
            <ta e="T112" id="Seg_1658" s="T111">pers</ta>
            <ta e="T113" id="Seg_1659" s="T112">n</ta>
            <ta e="T114" id="Seg_1660" s="T113">adv</ta>
            <ta e="T115" id="Seg_1661" s="T114">v</ta>
            <ta e="T116" id="Seg_1662" s="T115">n</ta>
            <ta e="T117" id="Seg_1663" s="T116">pers</ta>
            <ta e="T118" id="Seg_1664" s="T117">interrog</ta>
            <ta e="T119" id="Seg_1665" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_1666" s="T119">v</ta>
            <ta e="T121" id="Seg_1667" s="T120">pers</ta>
            <ta e="T122" id="Seg_1668" s="T121">v</ta>
            <ta e="T123" id="Seg_1669" s="T122">n</ta>
            <ta e="T124" id="Seg_1670" s="T123">pers</ta>
            <ta e="T125" id="Seg_1671" s="T124">pers</ta>
            <ta e="T126" id="Seg_1672" s="T125">v</ta>
            <ta e="T127" id="Seg_1673" s="T126">pers</ta>
            <ta e="T128" id="Seg_1674" s="T127">pers</ta>
            <ta e="T129" id="Seg_1675" s="T128">v</ta>
            <ta e="T130" id="Seg_1676" s="T129">num</ta>
            <ta e="T131" id="Seg_1677" s="T130">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1678" s="T1">pro.h:A</ta>
            <ta e="T4" id="Seg_1679" s="T3">np:G</ta>
            <ta e="T7" id="Seg_1680" s="T6">0.3.h:A</ta>
            <ta e="T8" id="Seg_1681" s="T7">np:L</ta>
            <ta e="T10" id="Seg_1682" s="T9">np.h:Th</ta>
            <ta e="T12" id="Seg_1683" s="T11">pro.h:Th</ta>
            <ta e="T15" id="Seg_1684" s="T14">pro.h:Th</ta>
            <ta e="T17" id="Seg_1685" s="T16">pro:Com</ta>
            <ta e="T18" id="Seg_1686" s="T17">pro.h:E</ta>
            <ta e="T20" id="Seg_1687" s="T19">np:Cau</ta>
            <ta e="T22" id="Seg_1688" s="T21">pro.h:E</ta>
            <ta e="T23" id="Seg_1689" s="T22">np:L</ta>
            <ta e="T25" id="Seg_1690" s="T24">np:Th</ta>
            <ta e="T27" id="Seg_1691" s="T26">pro.h:E</ta>
            <ta e="T29" id="Seg_1692" s="T28">pro:So</ta>
            <ta e="T30" id="Seg_1693" s="T29">v:Th</ta>
            <ta e="T35" id="Seg_1694" s="T34">pro:P</ta>
            <ta e="T36" id="Seg_1695" s="T35">np:Ins</ta>
            <ta e="T38" id="Seg_1696" s="T37">pro.h:A</ta>
            <ta e="T41" id="Seg_1697" s="T40">pro:P</ta>
            <ta e="T43" id="Seg_1698" s="T42">np:A</ta>
            <ta e="T46" id="Seg_1699" s="T45">pro.h:Th</ta>
            <ta e="T49" id="Seg_1700" s="T48">pp:L</ta>
            <ta e="T51" id="Seg_1701" s="T50">pro.h:Th</ta>
            <ta e="T54" id="Seg_1702" s="T53">pro:L</ta>
            <ta e="T55" id="Seg_1703" s="T54">pro.h:Th</ta>
            <ta e="T57" id="Seg_1704" s="T56">pro.h:A</ta>
            <ta e="T59" id="Seg_1705" s="T58">np:Path</ta>
            <ta e="T60" id="Seg_1706" s="T59">pro.h:P</ta>
            <ta e="T62" id="Seg_1707" s="T61">0.1.h:P</ta>
            <ta e="T63" id="Seg_1708" s="T62">np:G</ta>
            <ta e="T64" id="Seg_1709" s="T63">pro.h:Th</ta>
            <ta e="T67" id="Seg_1710" s="T66">np:Th</ta>
            <ta e="T69" id="Seg_1711" s="T68">np:Th 0.1.h:Poss</ta>
            <ta e="T71" id="Seg_1712" s="T70">np:Th 0.1.h:Poss</ta>
            <ta e="T73" id="Seg_1713" s="T72">np:Th 0.1.h:Poss</ta>
            <ta e="T76" id="Seg_1714" s="T75">0.3:P</ta>
            <ta e="T77" id="Seg_1715" s="T76">adv:Time</ta>
            <ta e="T78" id="Seg_1716" s="T77">np:P 0.1.h:Poss</ta>
            <ta e="T82" id="Seg_1717" s="T81">np:P 0.1.h:Poss</ta>
            <ta e="T83" id="Seg_1718" s="T82">pro.h:A</ta>
            <ta e="T86" id="Seg_1719" s="T85">np:Time</ta>
            <ta e="T88" id="Seg_1720" s="T87">np:Time</ta>
            <ta e="T89" id="Seg_1721" s="T88">pro.h:A</ta>
            <ta e="T90" id="Seg_1722" s="T89">adv:G</ta>
            <ta e="T92" id="Seg_1723" s="T91">pro.h:A</ta>
            <ta e="T94" id="Seg_1724" s="T93">np:Poss</ta>
            <ta e="T96" id="Seg_1725" s="T95">np:G</ta>
            <ta e="T97" id="Seg_1726" s="T96">np:G</ta>
            <ta e="T98" id="Seg_1727" s="T97">pro.h:A</ta>
            <ta e="T101" id="Seg_1728" s="T100">adv:L</ta>
            <ta e="T102" id="Seg_1729" s="T101">pro.h:Th</ta>
            <ta e="T104" id="Seg_1730" s="T103">np:P 0.1.h:Poss</ta>
            <ta e="T105" id="Seg_1731" s="T104">pro.h:B</ta>
            <ta e="T107" id="Seg_1732" s="T106">pro.h:A</ta>
            <ta e="T110" id="Seg_1733" s="T109">np:G</ta>
            <ta e="T111" id="Seg_1734" s="T110">v:Th</ta>
            <ta e="T112" id="Seg_1735" s="T111">pro.h:Th</ta>
            <ta e="T113" id="Seg_1736" s="T112">np.h:A</ta>
            <ta e="T114" id="Seg_1737" s="T113">adv:G</ta>
            <ta e="T116" id="Seg_1738" s="T115">np:P</ta>
            <ta e="T117" id="Seg_1739" s="T116">pro.h:A</ta>
            <ta e="T121" id="Seg_1740" s="T120">pro.h:A</ta>
            <ta e="T123" id="Seg_1741" s="T122">np:P</ta>
            <ta e="T124" id="Seg_1742" s="T123">pro.h:A</ta>
            <ta e="T125" id="Seg_1743" s="T124">pro.h:R</ta>
            <ta e="T126" id="Seg_1744" s="T125">0.3:Th</ta>
            <ta e="T127" id="Seg_1745" s="T126">pro.h:A</ta>
            <ta e="T128" id="Seg_1746" s="T127">pro.h:R</ta>
            <ta e="T131" id="Seg_1747" s="T130">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1748" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_1749" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_1750" s="T4">s:purp</ta>
            <ta e="T7" id="Seg_1751" s="T6">0.3.h:S v:pred</ta>
            <ta e="T10" id="Seg_1752" s="T9">np.h:S</ta>
            <ta e="T11" id="Seg_1753" s="T10">v:pred</ta>
            <ta e="T12" id="Seg_1754" s="T11">pro.h:S</ta>
            <ta e="T14" id="Seg_1755" s="T13">v:pred</ta>
            <ta e="T15" id="Seg_1756" s="T14">pro.h:S</ta>
            <ta e="T16" id="Seg_1757" s="T15">v:pred</ta>
            <ta e="T18" id="Seg_1758" s="T17">pro.h:O</ta>
            <ta e="T20" id="Seg_1759" s="T19">np:S</ta>
            <ta e="T21" id="Seg_1760" s="T20">v:pred</ta>
            <ta e="T22" id="Seg_1761" s="T21">pro.h:S</ta>
            <ta e="T24" id="Seg_1762" s="T23">v:pred</ta>
            <ta e="T25" id="Seg_1763" s="T24">np:O</ta>
            <ta e="T27" id="Seg_1764" s="T26">pro.h:S</ta>
            <ta e="T28" id="Seg_1765" s="T27">v:pred</ta>
            <ta e="T30" id="Seg_1766" s="T29">v:O</ta>
            <ta e="T38" id="Seg_1767" s="T37">pro.h:S</ta>
            <ta e="T40" id="Seg_1768" s="T39">v:pred</ta>
            <ta e="T41" id="Seg_1769" s="T40">pro:O</ta>
            <ta e="T43" id="Seg_1770" s="T42">np:S</ta>
            <ta e="T45" id="Seg_1771" s="T44">v:pred</ta>
            <ta e="T46" id="Seg_1772" s="T45">pro.h:S</ta>
            <ta e="T48" id="Seg_1773" s="T47">v:pred</ta>
            <ta e="T51" id="Seg_1774" s="T50">pro.h:S</ta>
            <ta e="T53" id="Seg_1775" s="T52">v:pred</ta>
            <ta e="T55" id="Seg_1776" s="T54">pro.h:S</ta>
            <ta e="T56" id="Seg_1777" s="T55">v:pred</ta>
            <ta e="T57" id="Seg_1778" s="T56">pro.h:S</ta>
            <ta e="T58" id="Seg_1779" s="T57">v:pred</ta>
            <ta e="T60" id="Seg_1780" s="T59">pro.h:S</ta>
            <ta e="T61" id="Seg_1781" s="T60">v:pred</ta>
            <ta e="T62" id="Seg_1782" s="T61">0.1.h:S v:pred</ta>
            <ta e="T64" id="Seg_1783" s="T63">pro.h:S</ta>
            <ta e="T66" id="Seg_1784" s="T65">v:pred</ta>
            <ta e="T67" id="Seg_1785" s="T66">np:S</ta>
            <ta e="T68" id="Seg_1786" s="T67">v:pred</ta>
            <ta e="T69" id="Seg_1787" s="T68">np:S</ta>
            <ta e="T70" id="Seg_1788" s="T69">v:pred</ta>
            <ta e="T71" id="Seg_1789" s="T70">np:S</ta>
            <ta e="T72" id="Seg_1790" s="T71">v:pred</ta>
            <ta e="T73" id="Seg_1791" s="T72">np:S</ta>
            <ta e="T74" id="Seg_1792" s="T73">v:pred</ta>
            <ta e="T76" id="Seg_1793" s="T75">0.3:S v:pred</ta>
            <ta e="T78" id="Seg_1794" s="T77">np:S</ta>
            <ta e="T79" id="Seg_1795" s="T78">v:pred</ta>
            <ta e="T80" id="Seg_1796" s="T79">v:pred</ta>
            <ta e="T82" id="Seg_1797" s="T81">np:S</ta>
            <ta e="T83" id="Seg_1798" s="T82">pro.h:S</ta>
            <ta e="T84" id="Seg_1799" s="T83">v:pred</ta>
            <ta e="T89" id="Seg_1800" s="T88">pro.h:S</ta>
            <ta e="T91" id="Seg_1801" s="T90">v:pred</ta>
            <ta e="T92" id="Seg_1802" s="T91">pro.h:S</ta>
            <ta e="T93" id="Seg_1803" s="T92">v:pred</ta>
            <ta e="T98" id="Seg_1804" s="T97">pro.h:S</ta>
            <ta e="T99" id="Seg_1805" s="T98">v:pred</ta>
            <ta e="T100" id="Seg_1806" s="T99">s:purp</ta>
            <ta e="T102" id="Seg_1807" s="T101">pro.h:S</ta>
            <ta e="T103" id="Seg_1808" s="T102">v:pred</ta>
            <ta e="T104" id="Seg_1809" s="T103">np:S</ta>
            <ta e="T106" id="Seg_1810" s="T105">v:pred</ta>
            <ta e="T107" id="Seg_1811" s="T106">pro.h:S</ta>
            <ta e="T109" id="Seg_1812" s="T108">v:pred</ta>
            <ta e="T111" id="Seg_1813" s="T110">v:O</ta>
            <ta e="T112" id="Seg_1814" s="T111">pro.h:O</ta>
            <ta e="T113" id="Seg_1815" s="T112">np.h:S</ta>
            <ta e="T115" id="Seg_1816" s="T114">v:pred</ta>
            <ta e="T116" id="Seg_1817" s="T115">np:O</ta>
            <ta e="T117" id="Seg_1818" s="T116">pro.h:S</ta>
            <ta e="T120" id="Seg_1819" s="T119">v:pred</ta>
            <ta e="T121" id="Seg_1820" s="T120">pro.h:S</ta>
            <ta e="T122" id="Seg_1821" s="T121">v:pred</ta>
            <ta e="T123" id="Seg_1822" s="T122">np:O</ta>
            <ta e="T124" id="Seg_1823" s="T123">pro.h:S</ta>
            <ta e="T126" id="Seg_1824" s="T125">v:pred 0.3:O</ta>
            <ta e="T127" id="Seg_1825" s="T126">pro.h:S</ta>
            <ta e="T129" id="Seg_1826" s="T128">v:pred</ta>
            <ta e="T131" id="Seg_1827" s="T130">np:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T6" id="Seg_1828" s="T5">RUS:gram</ta>
            <ta e="T31" id="Seg_1829" s="T30">RUS:disc</ta>
            <ta e="T33" id="Seg_1830" s="T32">RUS:gram</ta>
            <ta e="T37" id="Seg_1831" s="T36">RUS:disc</ta>
            <ta e="T42" id="Seg_1832" s="T41">RUS:disc</ta>
            <ta e="T44" id="Seg_1833" s="T43">RUS:core</ta>
            <ta e="T65" id="Seg_1834" s="T64">RUS:core</ta>
            <ta e="T67" id="Seg_1835" s="T66">RUS:cult</ta>
            <ta e="T95" id="Seg_1836" s="T94">RUS:cult</ta>
            <ta e="T108" id="Seg_1837" s="T107">RUS:gram</ta>
            <ta e="T109" id="Seg_1838" s="T108">RUS:core</ta>
            <ta e="T131" id="Seg_1839" s="T130">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T44" id="Seg_1840" s="T43">Csub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T33" id="Seg_1841" s="T32">parad:bare</ta>
            <ta e="T44" id="Seg_1842" s="T43">dir:bare</ta>
            <ta e="T65" id="Seg_1843" s="T64">dir:bare</ta>
            <ta e="T67" id="Seg_1844" s="T66">parad:bare</ta>
            <ta e="T95" id="Seg_1845" s="T94">dir:infl</ta>
            <ta e="T109" id="Seg_1846" s="T108">parad:bare</ta>
            <ta e="T131" id="Seg_1847" s="T130">parad:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T33" id="Seg_1848" s="T32">RUS:int.ins</ta>
            <ta e="T109" id="Seg_1849" s="T107">RUS:int</ta>
            <ta e="T131" id="Seg_1850" s="T130">RUS:int.alt</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_1851" s="T1">I went to the taiga to hunt.</ta>
            <ta e="T11" id="Seg_1852" s="T5">I looked at the road, there was noone there.</ta>
            <ta e="T14" id="Seg_1853" s="T11">I was alone.</ta>
            <ta e="T17" id="Seg_1854" s="T14">There was nobody with me.</ta>
            <ta e="T21" id="Seg_1855" s="T17">I was a bit drunk.</ta>
            <ta e="T26" id="Seg_1856" s="T21">I saw three animals (=bears) on the road.</ta>
            <ta e="T30" id="Seg_1857" s="T26">I wanted to run away from them.</ta>
            <ta e="T36" id="Seg_1858" s="T30">But I had to shoot at them from the gun.</ta>
            <ta e="T41" id="Seg_1859" s="T36">But I didn't kill anyone of them.</ta>
            <ta e="T45" id="Seg_1860" s="T41">Well, all the bears ran away.</ta>
            <ta e="T50" id="Seg_1861" s="T45">I stayed alone near the fire.</ta>
            <ta e="T53" id="Seg_1862" s="T50">I stayed alone.</ta>
            <ta e="T56" id="Seg_1863" s="T53">There was nobody nowhere.</ta>
            <ta e="T59" id="Seg_1864" s="T56">I went through the swamp.</ta>
            <ta e="T61" id="Seg_1865" s="T59">I got lost.</ta>
            <ta e="T63" id="Seg_1866" s="T61">I fell into a river.</ta>
            <ta e="T66" id="Seg_1867" s="T63">I was all wet.</ta>
            <ta e="T68" id="Seg_1868" s="T66">There were no matches.</ta>
            <ta e="T70" id="Seg_1869" s="T68">There was no bread.</ta>
            <ta e="T72" id="Seg_1870" s="T70">There was no knife.</ta>
            <ta e="T74" id="Seg_1871" s="T72">There were skis.</ta>
            <ta e="T77" id="Seg_1872" s="T74">Now they rotted.</ta>
            <ta e="T79" id="Seg_1873" s="T77">My legs hurted.</ta>
            <ta e="T82" id="Seg_1874" s="T79">I got my legs frostbitten.</ta>
            <ta e="T86" id="Seg_1875" s="T82">I walked for three days.</ta>
            <ta e="T91" id="Seg_1876" s="T86">On the fourth day I came out.</ta>
            <ta e="T96" id="Seg_1877" s="T91">I came to the Ket river, to a hut.</ta>
            <ta e="T100" id="Seg_1878" s="T96">I came into the hut to spend a night there.</ta>
            <ta e="T103" id="Seg_1879" s="T100">I spent a night there.</ta>
            <ta e="T106" id="Seg_1880" s="T103">My legs swelled.</ta>
            <ta e="T111" id="Seg_1881" s="T106">I couldn't go home.</ta>
            <ta e="T115" id="Seg_1882" s="T111">People brought me here.</ta>
            <ta e="T120" id="Seg_1883" s="T115">I didn't kill any squirrels.</ta>
            <ta e="T123" id="Seg_1884" s="T120">I killed a wolverine.</ta>
            <ta e="T126" id="Seg_1885" s="T123">I handed it over.</ta>
            <ta e="T131" id="Seg_1886" s="T126">He gave me six rubles.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_1887" s="T1">Ich ging in die Taiga um zu jagen.</ta>
            <ta e="T11" id="Seg_1888" s="T5">Ich betrachtete die Straße, es war niemand dort.</ta>
            <ta e="T14" id="Seg_1889" s="T11">Ich war allein.</ta>
            <ta e="T17" id="Seg_1890" s="T14">Es war keiner bei mir.</ta>
            <ta e="T21" id="Seg_1891" s="T17">Ich war ein wenig betrunken.</ta>
            <ta e="T26" id="Seg_1892" s="T21">Ich sah drei Tiere (=Bären) auf der Straße.</ta>
            <ta e="T30" id="Seg_1893" s="T26">Ich wollte vor ihnen davonlaufen.</ta>
            <ta e="T36" id="Seg_1894" s="T30">Aber ich musste nach ihnen schießen mit der Waffe.</ta>
            <ta e="T41" id="Seg_1895" s="T36">Aber ich habe keinen von ihnen getötet.</ta>
            <ta e="T45" id="Seg_1896" s="T41">Nun ja, alle Bären liefen davon.</ta>
            <ta e="T50" id="Seg_1897" s="T45">Ich blieb allein am Feuer.</ta>
            <ta e="T53" id="Seg_1898" s="T50">Ich blieb allein.</ta>
            <ta e="T56" id="Seg_1899" s="T53">Es gab niemanden nirgendwo.</ta>
            <ta e="T59" id="Seg_1900" s="T56">Ich ging durch den Sumpf.</ta>
            <ta e="T61" id="Seg_1901" s="T59">Ich habe mich verlaufen.</ta>
            <ta e="T63" id="Seg_1902" s="T61">Ich fiel in einen Fluss.</ta>
            <ta e="T66" id="Seg_1903" s="T63">Ich war vollkommen nass.</ta>
            <ta e="T68" id="Seg_1904" s="T66">Es gab keine Streichhölzer.</ta>
            <ta e="T70" id="Seg_1905" s="T68">Es gab kein Brot.</ta>
            <ta e="T72" id="Seg_1906" s="T70">Es gab kein Messer.</ta>
            <ta e="T74" id="Seg_1907" s="T72">Es gab Skier.</ta>
            <ta e="T77" id="Seg_1908" s="T74">Sie sind jetzt verfault.</ta>
            <ta e="T79" id="Seg_1909" s="T77">Meine Beine taten weh.</ta>
            <ta e="T82" id="Seg_1910" s="T79">Meine Beine waren erfroren.</ta>
            <ta e="T86" id="Seg_1911" s="T82">Ich lief drei Tage lang.</ta>
            <ta e="T91" id="Seg_1912" s="T86">Am vierten Tag fand ich zurück.</ta>
            <ta e="T96" id="Seg_1913" s="T91">Ich kam zum Fluss Ket, zu einer Hütte.</ta>
            <ta e="T100" id="Seg_1914" s="T96">Ich kam in die Hütte und übernachtete dort.</ta>
            <ta e="T103" id="Seg_1915" s="T100">Ich übernachtete dort.</ta>
            <ta e="T106" id="Seg_1916" s="T103">Meine Beine schwollen an.</ta>
            <ta e="T111" id="Seg_1917" s="T106">Ich konnte nicht nach Hause gehen.</ta>
            <ta e="T115" id="Seg_1918" s="T111">Leute brachten mich hierher.</ta>
            <ta e="T120" id="Seg_1919" s="T115">Ich tötete keine Eichhörnchen.</ta>
            <ta e="T123" id="Seg_1920" s="T120">Ich tötete ein Vielfraß.</ta>
            <ta e="T126" id="Seg_1921" s="T123">Ich übergab ihn.</ta>
            <ta e="T131" id="Seg_1922" s="T126">Er gab mir sechs Rubel.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_1923" s="T1">Я пошел в тайгу на промысел.</ta>
            <ta e="T11" id="Seg_1924" s="T5">Смотрел на дороге, никого не было.</ta>
            <ta e="T14" id="Seg_1925" s="T11">Я один был.</ta>
            <ta e="T17" id="Seg_1926" s="T14">Никого не было со мной.</ta>
            <ta e="T21" id="Seg_1927" s="T17">Я был немного выпивши.</ta>
            <ta e="T26" id="Seg_1928" s="T21">На дороге я увидел трех зверей.</ta>
            <ta e="T30" id="Seg_1929" s="T26">Я хотел от них сбежать.</ta>
            <ta e="T36" id="Seg_1930" s="T30">Но мне пришлось стрелять в них из ружья.</ta>
            <ta e="T41" id="Seg_1931" s="T36">Но я никого из них не убил.</ta>
            <ta e="T45" id="Seg_1932" s="T41">Ну, медведи все убежали.</ta>
            <ta e="T50" id="Seg_1933" s="T45">Я один остался около костра.</ta>
            <ta e="T53" id="Seg_1934" s="T50">Я один остался.</ta>
            <ta e="T56" id="Seg_1935" s="T53">Нигде никого не было.</ta>
            <ta e="T59" id="Seg_1936" s="T56">Я пошел болотом.</ta>
            <ta e="T61" id="Seg_1937" s="T59">Я заблудился.</ta>
            <ta e="T63" id="Seg_1938" s="T61">Я провалился в речку.</ta>
            <ta e="T66" id="Seg_1939" s="T63">Я был весь мокрый.</ta>
            <ta e="T68" id="Seg_1940" s="T66">Спичек не было.</ta>
            <ta e="T70" id="Seg_1941" s="T68">Хлеба на было.</ta>
            <ta e="T72" id="Seg_1942" s="T70">Ножа не было.</ta>
            <ta e="T74" id="Seg_1943" s="T72">Лыжи были.</ta>
            <ta e="T77" id="Seg_1944" s="T74">Теперь они сгнили.</ta>
            <ta e="T79" id="Seg_1945" s="T77">У меня болели ноги.</ta>
            <ta e="T82" id="Seg_1946" s="T79">Я обморозил обе ноги.</ta>
            <ta e="T86" id="Seg_1947" s="T82">Я шел три дня.</ta>
            <ta e="T91" id="Seg_1948" s="T86">На четвертый день я вышел.</ta>
            <ta e="T96" id="Seg_1949" s="T91">Я вышел на край Кети, к избушке.</ta>
            <ta e="T100" id="Seg_1950" s="T96">В избушку я вошел, чтобы переночевать.</ta>
            <ta e="T103" id="Seg_1951" s="T100">Там я ночевал.</ta>
            <ta e="T106" id="Seg_1952" s="T103">Ноги у меня опухли.</ta>
            <ta e="T111" id="Seg_1953" s="T106">Я не смог дойти до дома.</ta>
            <ta e="T115" id="Seg_1954" s="T111">Меня люди сюда привезли.</ta>
            <ta e="T120" id="Seg_1955" s="T115">Белок я ни одной не добыл.</ta>
            <ta e="T123" id="Seg_1956" s="T120">Я добыл россомаху.</ta>
            <ta e="T126" id="Seg_1957" s="T123">Я ее сдал.</ta>
            <ta e="T131" id="Seg_1958" s="T126">Он мне дал шесть рублей.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_1959" s="T1">я пошел в тайгу на промысел</ta>
            <ta e="T11" id="Seg_1960" s="T5">смотрел на дороге никого не было</ta>
            <ta e="T14" id="Seg_1961" s="T11">я один был</ta>
            <ta e="T17" id="Seg_1962" s="T14">никого не было меня</ta>
            <ta e="T21" id="Seg_1963" s="T17">я немного был выпивши</ta>
            <ta e="T26" id="Seg_1964" s="T21">мне дорогой увидел зверей трех</ta>
            <ta e="T30" id="Seg_1965" s="T26">я хотел от них сбежать</ta>
            <ta e="T36" id="Seg_1966" s="T30">мне пришлось стрелять в них с ружья</ta>
            <ta e="T41" id="Seg_1967" s="T36">но я никого не добыл из них</ta>
            <ta e="T45" id="Seg_1968" s="T41">медведи все убежали</ta>
            <ta e="T50" id="Seg_1969" s="T45">я один остался около костра</ta>
            <ta e="T53" id="Seg_1970" s="T50">я один остался</ta>
            <ta e="T56" id="Seg_1971" s="T53">игде никого не было</ta>
            <ta e="T59" id="Seg_1972" s="T56">я пошел болотом</ta>
            <ta e="T61" id="Seg_1973" s="T59">я заблудился</ta>
            <ta e="T63" id="Seg_1974" s="T61">утонул в речку</ta>
            <ta e="T66" id="Seg_1975" s="T63">я весь мокрый был</ta>
            <ta e="T68" id="Seg_1976" s="T66">спичек не было</ta>
            <ta e="T70" id="Seg_1977" s="T68">хлеба</ta>
            <ta e="T72" id="Seg_1978" s="T70">ножа не было</ta>
            <ta e="T74" id="Seg_1979" s="T72">лыжи были</ta>
            <ta e="T77" id="Seg_1980" s="T74">сгнили теперь (сейчас) они</ta>
            <ta e="T79" id="Seg_1981" s="T77">ноги болят</ta>
            <ta e="T82" id="Seg_1982" s="T79">обзнобил (обморозил) обе ноги</ta>
            <ta e="T86" id="Seg_1983" s="T82">я шел три дня</ta>
            <ta e="T91" id="Seg_1984" s="T86">(на) четвертый день я вышел</ta>
            <ta e="T96" id="Seg_1985" s="T91">я вышел на Кеть на край избушки</ta>
            <ta e="T100" id="Seg_1986" s="T96">в избушку я вошел ночевать</ta>
            <ta e="T103" id="Seg_1987" s="T100">здесь я ночевал</ta>
            <ta e="T106" id="Seg_1988" s="T103">ноги у меня опухли</ta>
            <ta e="T111" id="Seg_1989" s="T106">я не смог до дома дойти</ta>
            <ta e="T115" id="Seg_1990" s="T111">меня люди сюда привезли</ta>
            <ta e="T120" id="Seg_1991" s="T115">белок я ни одной не добыл</ta>
            <ta e="T123" id="Seg_1992" s="T120">я добыл россомаху</ta>
            <ta e="T126" id="Seg_1993" s="T123">я ее сдал</ta>
            <ta e="T131" id="Seg_1994" s="T126">он мне дал шесть рублей</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
