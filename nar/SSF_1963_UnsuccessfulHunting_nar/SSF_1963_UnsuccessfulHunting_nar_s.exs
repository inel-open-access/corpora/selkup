<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>SSF_1963_UnsuccessfulHunting_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">SSF_1963_UnsuccessfulHunting_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">39</ud-information>
            <ud-information attribute-name="# HIAT:w">30</ud-information>
            <ud-information attribute-name="# e">30</ud-information>
            <ud-information attribute-name="# HIAT:u">7</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SSF">
            <abbreviation>SSF</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SSF"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T31" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <ts e="T2" id="Seg_5" n="HIAT:w" s="T1">peŋa</ts>
                  <nts id="Seg_6" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_8" n="HIAT:w" s="T2">kɨgɨŋ</ts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_11" n="HIAT:w" s="T3">ton</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_14" n="HIAT:w" s="T4">taj</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_17" n="HIAT:w" s="T5">pugu</ts>
                  <nts id="Seg_18" n="HIAT:ip">.</nts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_21" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">qwärɣä</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">kɨgɨŋ</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">täbɨm</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">mittägu</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_35" n="HIAT:w" s="T10">amgu</ts>
                  <nts id="Seg_36" n="HIAT:ip">)</nts>
                  <nts id="Seg_37" n="HIAT:ip">.</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_40" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">man</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">tam</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">bon</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_51" n="HIAT:w" s="T14">kɨkkɨzaŋ</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_54" n="HIAT:w" s="T15">qwatku</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_57" n="HIAT:w" s="T16">täbɨm</ts>
                  <nts id="Seg_58" n="HIAT:ip">.</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_61" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_63" n="HIAT:w" s="T17">meŋanan</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_66" n="HIAT:w" s="T18">täpsä</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_69" n="HIAT:w" s="T19">qaimna</ts>
                  <nts id="Seg_70" n="HIAT:ip">.</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_73" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">ne</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">smog</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">täbɨm</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_84" n="HIAT:w" s="T23">qwätku</ts>
                  <nts id="Seg_85" n="HIAT:ip">.</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_88" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_90" n="HIAT:w" s="T24">meŋanan</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_93" n="HIAT:w" s="T25">pulʼej</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_96" n="HIAT:w" s="T26">tʼäŋuzan</ts>
                  <nts id="Seg_97" n="HIAT:ip">.</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_100" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_102" n="HIAT:w" s="T27">no</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_105" n="HIAT:w" s="T28">surum</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_108" n="HIAT:w" s="T29">qwässan</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_111" n="HIAT:w" s="T30">meŋanan</ts>
                  <nts id="Seg_112" n="HIAT:ip">.</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T31" id="Seg_114" n="sc" s="T1">
               <ts e="T2" id="Seg_116" n="e" s="T1">(peŋa </ts>
               <ts e="T3" id="Seg_118" n="e" s="T2">kɨgɨŋ </ts>
               <ts e="T4" id="Seg_120" n="e" s="T3">ton </ts>
               <ts e="T5" id="Seg_122" n="e" s="T4">taj </ts>
               <ts e="T6" id="Seg_124" n="e" s="T5">pugu. </ts>
               <ts e="T7" id="Seg_126" n="e" s="T6">qwärɣä </ts>
               <ts e="T8" id="Seg_128" n="e" s="T7">kɨgɨŋ </ts>
               <ts e="T9" id="Seg_130" n="e" s="T8">täbɨm </ts>
               <ts e="T10" id="Seg_132" n="e" s="T9">mittägu </ts>
               <ts e="T11" id="Seg_134" n="e" s="T10">amgu). </ts>
               <ts e="T12" id="Seg_136" n="e" s="T11">man </ts>
               <ts e="T13" id="Seg_138" n="e" s="T12">tam </ts>
               <ts e="T14" id="Seg_140" n="e" s="T13">bon </ts>
               <ts e="T15" id="Seg_142" n="e" s="T14">kɨkkɨzaŋ </ts>
               <ts e="T16" id="Seg_144" n="e" s="T15">qwatku </ts>
               <ts e="T17" id="Seg_146" n="e" s="T16">täbɨm. </ts>
               <ts e="T18" id="Seg_148" n="e" s="T17">meŋanan </ts>
               <ts e="T19" id="Seg_150" n="e" s="T18">täpsä </ts>
               <ts e="T20" id="Seg_152" n="e" s="T19">qaimna. </ts>
               <ts e="T21" id="Seg_154" n="e" s="T20">ne </ts>
               <ts e="T22" id="Seg_156" n="e" s="T21">smog </ts>
               <ts e="T23" id="Seg_158" n="e" s="T22">täbɨm </ts>
               <ts e="T24" id="Seg_160" n="e" s="T23">qwätku. </ts>
               <ts e="T25" id="Seg_162" n="e" s="T24">meŋanan </ts>
               <ts e="T26" id="Seg_164" n="e" s="T25">pulʼej </ts>
               <ts e="T27" id="Seg_166" n="e" s="T26">tʼäŋuzan. </ts>
               <ts e="T28" id="Seg_168" n="e" s="T27">no </ts>
               <ts e="T29" id="Seg_170" n="e" s="T28">surum </ts>
               <ts e="T30" id="Seg_172" n="e" s="T29">qwässan </ts>
               <ts e="T31" id="Seg_174" n="e" s="T30">meŋanan. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_175" s="T1">SSF_1963_UnsuccessfulHunting_nar.001 (001.001)</ta>
            <ta e="T11" id="Seg_176" s="T6">SSF_1963_UnsuccessfulHunting_nar.002 (001.002)</ta>
            <ta e="T17" id="Seg_177" s="T11">SSF_1963_UnsuccessfulHunting_nar.003 (001.003)</ta>
            <ta e="T20" id="Seg_178" s="T17">SSF_1963_UnsuccessfulHunting_nar.004 (001.004)</ta>
            <ta e="T24" id="Seg_179" s="T20">SSF_1963_UnsuccessfulHunting_nar.005 (001.005)</ta>
            <ta e="T27" id="Seg_180" s="T24">SSF_1963_UnsuccessfulHunting_nar.006 (001.006)</ta>
            <ta e="T31" id="Seg_181" s="T27">SSF_1963_UnsuccessfulHunting_nar.007 (001.007)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_182" s="T1">(′пеңа кы′гың ′тон тай ′пугу.</ta>
            <ta e="T11" id="Seg_183" s="T6">′kwӓрɣӓ кы′гың ′тӓбым миттӓ′гу ам′гу.)</ta>
            <ta e="T17" id="Seg_184" s="T11">ман там′бон кыккы′заң kwат′ку ′тӓбым.</ta>
            <ta e="T20" id="Seg_185" s="T17">′меңанан тӓп′сӓ kаим′на.</ta>
            <ta e="T24" id="Seg_186" s="T20">не смог ′тӓбым kwӓт′ку.</ta>
            <ta e="T27" id="Seg_187" s="T24">′меңанан ′пулʼей ′тʼӓңузан.</ta>
            <ta e="T31" id="Seg_188" s="T27">но ′сурум kwӓ′ссан ′меңанан.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_189" s="T1">(peŋa kɨgɨŋ ton taj pugu.</ta>
            <ta e="T11" id="Seg_190" s="T6">qwärɣä kɨgɨŋ täbɨm mittägu amgu.)</ta>
            <ta e="T17" id="Seg_191" s="T11">man tambon kɨkkɨzaŋ qwatku täbɨm.</ta>
            <ta e="T20" id="Seg_192" s="T17">meŋanan täpsä qaimna.</ta>
            <ta e="T24" id="Seg_193" s="T20">ne smog täbɨm qwätku.</ta>
            <ta e="T27" id="Seg_194" s="T24">meŋanan pulʼej tʼäŋuzan.</ta>
            <ta e="T31" id="Seg_195" s="T27">no surum qwässan meŋanan.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_196" s="T1">(peŋa kɨgɨŋ ton taj pugu. </ta>
            <ta e="T11" id="Seg_197" s="T6">qwärɣä kɨgɨŋ täbɨm mittägu amgu.) </ta>
            <ta e="T17" id="Seg_198" s="T11">man tam bon kɨkkɨzaŋ qwatku täbɨm. </ta>
            <ta e="T20" id="Seg_199" s="T17">meŋanan täpsä qaimna. </ta>
            <ta e="T24" id="Seg_200" s="T20">ne smog täbɨm qwätku. </ta>
            <ta e="T27" id="Seg_201" s="T24">meŋanan pulʼej tʼäŋuzan. </ta>
            <ta e="T31" id="Seg_202" s="T27">no surum qwässan meŋanan. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_203" s="T1">peŋa</ta>
            <ta e="T3" id="Seg_204" s="T2">kɨgɨ-n</ta>
            <ta e="T4" id="Seg_205" s="T3">to-n</ta>
            <ta e="T5" id="Seg_206" s="T4">taj</ta>
            <ta e="T6" id="Seg_207" s="T5">pu-gu</ta>
            <ta e="T7" id="Seg_208" s="T6">qwärɣä</ta>
            <ta e="T8" id="Seg_209" s="T7">kɨgɨ-n</ta>
            <ta e="T9" id="Seg_210" s="T8">täb-ɨ-m</ta>
            <ta e="T10" id="Seg_211" s="T9">mittä-gu</ta>
            <ta e="T11" id="Seg_212" s="T10">am-ɨ-gu</ta>
            <ta e="T12" id="Seg_213" s="T11">man</ta>
            <ta e="T13" id="Seg_214" s="T12">tam</ta>
            <ta e="T14" id="Seg_215" s="T13">bo-n</ta>
            <ta e="T15" id="Seg_216" s="T14">kɨkkɨ-za-ŋ</ta>
            <ta e="T16" id="Seg_217" s="T15">qwat-ku</ta>
            <ta e="T17" id="Seg_218" s="T16">täb-ɨ-m</ta>
            <ta e="T18" id="Seg_219" s="T17">meŋa-nan</ta>
            <ta e="T19" id="Seg_220" s="T18">täp-se</ta>
            <ta e="T20" id="Seg_221" s="T19">qai-m-na</ta>
            <ta e="T21" id="Seg_222" s="T20">ne</ta>
            <ta e="T22" id="Seg_223" s="T21">smog</ta>
            <ta e="T23" id="Seg_224" s="T22">täb-ɨ-m</ta>
            <ta e="T24" id="Seg_225" s="T23">qwät-ku</ta>
            <ta e="T25" id="Seg_226" s="T24">meŋa-nan</ta>
            <ta e="T27" id="Seg_227" s="T26">tʼäŋu-za-n</ta>
            <ta e="T28" id="Seg_228" s="T27">no</ta>
            <ta e="T29" id="Seg_229" s="T28">surum</ta>
            <ta e="T30" id="Seg_230" s="T29">qwäs-sa-ŋ</ta>
            <ta e="T31" id="Seg_231" s="T30">meŋa-nan</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_232" s="T1">päŋqa</ta>
            <ta e="T3" id="Seg_233" s="T2">kɨkkɨ-n</ta>
            <ta e="T4" id="Seg_234" s="T3">toː-n</ta>
            <ta e="T5" id="Seg_235" s="T4">taːj</ta>
            <ta e="T6" id="Seg_236" s="T5">puː-gu</ta>
            <ta e="T7" id="Seg_237" s="T6">qwärqa</ta>
            <ta e="T8" id="Seg_238" s="T7">kɨkkɨ-n</ta>
            <ta e="T9" id="Seg_239" s="T8">tap-ɨ-m</ta>
            <ta e="T10" id="Seg_240" s="T9">mittɨ-gu</ta>
            <ta e="T11" id="Seg_241" s="T10">am-ɨ-gu</ta>
            <ta e="T12" id="Seg_242" s="T11">man</ta>
            <ta e="T13" id="Seg_243" s="T12">taw</ta>
            <ta e="T14" id="Seg_244" s="T13">po-n</ta>
            <ta e="T15" id="Seg_245" s="T14">kɨkkɨ-sɨ-ŋ</ta>
            <ta e="T16" id="Seg_246" s="T15">qwat-gu</ta>
            <ta e="T17" id="Seg_247" s="T16">tap-ɨ-m</ta>
            <ta e="T18" id="Seg_248" s="T17">mäkkä-nan</ta>
            <ta e="T19" id="Seg_249" s="T18">tap-se</ta>
            <ta e="T20" id="Seg_250" s="T19">qaj-m-naj</ta>
            <ta e="T21" id="Seg_251" s="T20">ne</ta>
            <ta e="T22" id="Seg_252" s="T21">smog</ta>
            <ta e="T23" id="Seg_253" s="T22">tap-ɨ-m</ta>
            <ta e="T24" id="Seg_254" s="T23">qwat-gu</ta>
            <ta e="T25" id="Seg_255" s="T24">mäkkä-nan</ta>
            <ta e="T27" id="Seg_256" s="T26">tʼäŋu-sɨ-n</ta>
            <ta e="T28" id="Seg_257" s="T27">nu</ta>
            <ta e="T29" id="Seg_258" s="T28">suːrǝm</ta>
            <ta e="T30" id="Seg_259" s="T29">qwən-sɨ-n</ta>
            <ta e="T31" id="Seg_260" s="T30">mäkkä-nan</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_261" s="T1">elk.[NOM]</ta>
            <ta e="T3" id="Seg_262" s="T2">want-3SG.S</ta>
            <ta e="T4" id="Seg_263" s="T3">lake-GEN</ta>
            <ta e="T5" id="Seg_264" s="T4">across</ta>
            <ta e="T6" id="Seg_265" s="T5">cross-INF</ta>
            <ta e="T7" id="Seg_266" s="T6">bear.[NOM]</ta>
            <ta e="T8" id="Seg_267" s="T7">want-3SG.S</ta>
            <ta e="T9" id="Seg_268" s="T8">(s)he-EP-ACC</ta>
            <ta e="T10" id="Seg_269" s="T9">reach-INF</ta>
            <ta e="T11" id="Seg_270" s="T10">eat-EP-INF</ta>
            <ta e="T12" id="Seg_271" s="T11">I.NOM</ta>
            <ta e="T13" id="Seg_272" s="T12">this</ta>
            <ta e="T14" id="Seg_273" s="T13">year-ADV.LOC</ta>
            <ta e="T15" id="Seg_274" s="T14">want-PST-1SG.S</ta>
            <ta e="T16" id="Seg_275" s="T15">catch-INF</ta>
            <ta e="T17" id="Seg_276" s="T16">(s)he-EP-ACC</ta>
            <ta e="T18" id="Seg_277" s="T17">I.ALL-ADES</ta>
            <ta e="T19" id="Seg_278" s="T18">(s)he-INSTR</ta>
            <ta e="T20" id="Seg_279" s="T19">what-ACC-EMPH</ta>
            <ta e="T21" id="Seg_280" s="T20">NEG</ta>
            <ta e="T22" id="Seg_281" s="T21">can.PST.3SG</ta>
            <ta e="T23" id="Seg_282" s="T22">(s)he-EP-ACC</ta>
            <ta e="T24" id="Seg_283" s="T23">kill-INF</ta>
            <ta e="T25" id="Seg_284" s="T24">I.ALL-ADES</ta>
            <ta e="T27" id="Seg_285" s="T26">NEG.EX-PST-3SG.S</ta>
            <ta e="T28" id="Seg_286" s="T27">now</ta>
            <ta e="T29" id="Seg_287" s="T28">wild.animal.[NOM]</ta>
            <ta e="T30" id="Seg_288" s="T29">go.away-PST-3SG.S</ta>
            <ta e="T31" id="Seg_289" s="T30">I.ALL-ADES</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_290" s="T1">лось.[NOM]</ta>
            <ta e="T3" id="Seg_291" s="T2">хотеть-3SG.S</ta>
            <ta e="T4" id="Seg_292" s="T3">озеро-GEN</ta>
            <ta e="T5" id="Seg_293" s="T4">через</ta>
            <ta e="T6" id="Seg_294" s="T5">перейти-INF</ta>
            <ta e="T7" id="Seg_295" s="T6">медведь.[NOM]</ta>
            <ta e="T8" id="Seg_296" s="T7">хотеть-3SG.S</ta>
            <ta e="T9" id="Seg_297" s="T8">он(а)-EP-ACC</ta>
            <ta e="T10" id="Seg_298" s="T9">дойти-INF</ta>
            <ta e="T11" id="Seg_299" s="T10">съесть-EP-INF</ta>
            <ta e="T12" id="Seg_300" s="T11">я.NOM</ta>
            <ta e="T13" id="Seg_301" s="T12">этот</ta>
            <ta e="T14" id="Seg_302" s="T13">год-ADV.LOC</ta>
            <ta e="T15" id="Seg_303" s="T14">хотеть-PST-1SG.S</ta>
            <ta e="T16" id="Seg_304" s="T15">поймать-INF</ta>
            <ta e="T17" id="Seg_305" s="T16">он(а)-EP-ACC</ta>
            <ta e="T18" id="Seg_306" s="T17">я.ALL-ADES</ta>
            <ta e="T19" id="Seg_307" s="T18">он(а)-INSTR</ta>
            <ta e="T20" id="Seg_308" s="T19">что-ACC-EMPH</ta>
            <ta e="T21" id="Seg_309" s="T20">NEG</ta>
            <ta e="T22" id="Seg_310" s="T21">мочь.PST.3SG</ta>
            <ta e="T23" id="Seg_311" s="T22">он(а)-EP-ACC</ta>
            <ta e="T24" id="Seg_312" s="T23">убить-INF</ta>
            <ta e="T25" id="Seg_313" s="T24">я.ALL-ADES</ta>
            <ta e="T27" id="Seg_314" s="T26">NEG.EX-PST-3SG.S</ta>
            <ta e="T28" id="Seg_315" s="T27">ну</ta>
            <ta e="T29" id="Seg_316" s="T28">зверь.[NOM]</ta>
            <ta e="T30" id="Seg_317" s="T29">уйти-PST-3SG.S</ta>
            <ta e="T31" id="Seg_318" s="T30">я.ALL-ADES</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_319" s="T1">n.[n:case]</ta>
            <ta e="T3" id="Seg_320" s="T2">v-v:pn</ta>
            <ta e="T4" id="Seg_321" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_322" s="T4">pp</ta>
            <ta e="T6" id="Seg_323" s="T5">v-v:inf</ta>
            <ta e="T7" id="Seg_324" s="T6">n.[n:case]</ta>
            <ta e="T8" id="Seg_325" s="T7">v-v:pn</ta>
            <ta e="T9" id="Seg_326" s="T8">pers-n:ins-n:case</ta>
            <ta e="T10" id="Seg_327" s="T9">v-v:inf</ta>
            <ta e="T11" id="Seg_328" s="T10">v-n:ins-v:inf</ta>
            <ta e="T12" id="Seg_329" s="T11">pers</ta>
            <ta e="T13" id="Seg_330" s="T12">dem</ta>
            <ta e="T14" id="Seg_331" s="T13">n-adv:case</ta>
            <ta e="T15" id="Seg_332" s="T14">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_333" s="T15">v-v:inf</ta>
            <ta e="T17" id="Seg_334" s="T16">pers-n:ins-n:case</ta>
            <ta e="T18" id="Seg_335" s="T17">pers-n:case</ta>
            <ta e="T19" id="Seg_336" s="T18">pers-n:case</ta>
            <ta e="T20" id="Seg_337" s="T19">interrog-n:case-clit</ta>
            <ta e="T21" id="Seg_338" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_339" s="T21">v</ta>
            <ta e="T23" id="Seg_340" s="T22">pers-n:ins-n:case</ta>
            <ta e="T24" id="Seg_341" s="T23">v-v:inf</ta>
            <ta e="T25" id="Seg_342" s="T24">pers-n:case</ta>
            <ta e="T27" id="Seg_343" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_344" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_345" s="T28">n.[n:case]</ta>
            <ta e="T30" id="Seg_346" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_347" s="T30">pers-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_348" s="T1">n</ta>
            <ta e="T3" id="Seg_349" s="T2">v</ta>
            <ta e="T4" id="Seg_350" s="T3">n</ta>
            <ta e="T5" id="Seg_351" s="T4">pp</ta>
            <ta e="T6" id="Seg_352" s="T5">v</ta>
            <ta e="T7" id="Seg_353" s="T6">n</ta>
            <ta e="T8" id="Seg_354" s="T7">v</ta>
            <ta e="T9" id="Seg_355" s="T8">pers</ta>
            <ta e="T10" id="Seg_356" s="T9">v</ta>
            <ta e="T11" id="Seg_357" s="T10">v</ta>
            <ta e="T12" id="Seg_358" s="T11">pers</ta>
            <ta e="T13" id="Seg_359" s="T12">dem</ta>
            <ta e="T14" id="Seg_360" s="T13">n</ta>
            <ta e="T15" id="Seg_361" s="T14">v</ta>
            <ta e="T16" id="Seg_362" s="T15">v</ta>
            <ta e="T17" id="Seg_363" s="T16">pers</ta>
            <ta e="T18" id="Seg_364" s="T17">pers</ta>
            <ta e="T19" id="Seg_365" s="T18">pers</ta>
            <ta e="T20" id="Seg_366" s="T19">interrog</ta>
            <ta e="T21" id="Seg_367" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_368" s="T21">v</ta>
            <ta e="T23" id="Seg_369" s="T22">pers</ta>
            <ta e="T24" id="Seg_370" s="T23">v</ta>
            <ta e="T25" id="Seg_371" s="T24">pers</ta>
            <ta e="T27" id="Seg_372" s="T26">v</ta>
            <ta e="T28" id="Seg_373" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_374" s="T28">n</ta>
            <ta e="T30" id="Seg_375" s="T29">v</ta>
            <ta e="T31" id="Seg_376" s="T30">pers</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_377" s="T1">np:S</ta>
            <ta e="T3" id="Seg_378" s="T2">v:pred</ta>
            <ta e="T6" id="Seg_379" s="T5">v:O</ta>
            <ta e="T7" id="Seg_380" s="T6">np:S</ta>
            <ta e="T8" id="Seg_381" s="T7">v:pred</ta>
            <ta e="T10" id="Seg_382" s="T9">v:O</ta>
            <ta e="T11" id="Seg_383" s="T10">v:O</ta>
            <ta e="T12" id="Seg_384" s="T11">pro.h:S</ta>
            <ta e="T15" id="Seg_385" s="T14">v:pred</ta>
            <ta e="T16" id="Seg_386" s="T15">v:O</ta>
            <ta e="T22" id="Seg_387" s="T21">0.1.h:S v:pred</ta>
            <ta e="T24" id="Seg_388" s="T23">v:O</ta>
            <ta e="T26" id="Seg_389" s="T25">np:S</ta>
            <ta e="T27" id="Seg_390" s="T26">v:pred</ta>
            <ta e="T29" id="Seg_391" s="T28">np:S</ta>
            <ta e="T30" id="Seg_392" s="T29">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_393" s="T1">np:E</ta>
            <ta e="T5" id="Seg_394" s="T3">pp:Path</ta>
            <ta e="T6" id="Seg_395" s="T5">v:Th</ta>
            <ta e="T7" id="Seg_396" s="T6">np:E</ta>
            <ta e="T9" id="Seg_397" s="T8">pro:Th</ta>
            <ta e="T10" id="Seg_398" s="T9">v:Th</ta>
            <ta e="T11" id="Seg_399" s="T10">v:Th</ta>
            <ta e="T12" id="Seg_400" s="T11">pro.h:E</ta>
            <ta e="T14" id="Seg_401" s="T13">np:Time</ta>
            <ta e="T16" id="Seg_402" s="T15">v:Th</ta>
            <ta e="T17" id="Seg_403" s="T16">np:P</ta>
            <ta e="T22" id="Seg_404" s="T21">0.1.h:E</ta>
            <ta e="T23" id="Seg_405" s="T22">pro.h:P</ta>
            <ta e="T24" id="Seg_406" s="T23">v:Th</ta>
            <ta e="T25" id="Seg_407" s="T24">pro.h:Poss</ta>
            <ta e="T26" id="Seg_408" s="T25">np:Th</ta>
            <ta e="T29" id="Seg_409" s="T28">np:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T20" id="Seg_410" s="T19">-</ta>
            <ta e="T21" id="Seg_411" s="T20">RUS:gram</ta>
            <ta e="T22" id="Seg_412" s="T21">RUS</ta>
            <ta e="T26" id="Seg_413" s="T25">RUS</ta>
            <ta e="T28" id="Seg_414" s="T27">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T22" id="Seg_415" s="T20">RUS:int.ins</ta>
            <ta e="T26" id="Seg_416" s="T25">RUS:int.alt</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_417" s="T1">The elk wants to swim on the other side of the lake.</ta>
            <ta e="T11" id="Seg_418" s="T6">The bear wants to catch it and eat it.</ta>
            <ta e="T17" id="Seg_419" s="T11">I wanted to kill him this year.</ta>
            <ta e="T20" id="Seg_420" s="T17">I have noone with him (?).</ta>
            <ta e="T24" id="Seg_421" s="T20">I couldn't kill him.</ta>
            <ta e="T27" id="Seg_422" s="T24">I didn't have bullets.</ta>
            <ta e="T31" id="Seg_423" s="T27">Now, the animal ran away from me.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_424" s="T1">Der Elch wollte auf die andere Seite des Sees schwimmen.</ta>
            <ta e="T11" id="Seg_425" s="T6">Der Bär will ihn fangen und fressen.</ta>
            <ta e="T17" id="Seg_426" s="T11">Ich wollte ihn dieses Jahr töten.</ta>
            <ta e="T20" id="Seg_427" s="T17">Ich habe niemanden bei ihm (?).</ta>
            <ta e="T24" id="Seg_428" s="T20">Ich konnte ihn nicht töten.</ta>
            <ta e="T27" id="Seg_429" s="T24">Ich hatte keine Patronen.</ta>
            <ta e="T31" id="Seg_430" s="T27">Nun, das Tier lief vor mir davon.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_431" s="T1">Лось хочет переплыть через озеро.</ta>
            <ta e="T11" id="Seg_432" s="T6">Медведь хочет его поймать и съесть.</ta>
            <ta e="T17" id="Seg_433" s="T11">Я в этом году хотел убить его.</ta>
            <ta e="T20" id="Seg_434" s="T17">У меня с ним никого.</ta>
            <ta e="T24" id="Seg_435" s="T20">Я не смог его убить.</ta>
            <ta e="T27" id="Seg_436" s="T24">У меня не было пуль.</ta>
            <ta e="T31" id="Seg_437" s="T27">Ну, зверь убежал от меня.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_438" s="T1">лось хочет на ту сторону плыть</ta>
            <ta e="T11" id="Seg_439" s="T6">медведь хочет его поймать и съесть</ta>
            <ta e="T17" id="Seg_440" s="T11">я в этом году хотел убить его</ta>
            <ta e="T20" id="Seg_441" s="T17">у меня с ним никого</ta>
            <ta e="T24" id="Seg_442" s="T20">его убить</ta>
            <ta e="T27" id="Seg_443" s="T24">у меня пулей не было</ta>
            <ta e="T31" id="Seg_444" s="T27">но зверь убежал от меня</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T20" id="Seg_445" s="T17">[BrM:] Unclear sense.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
