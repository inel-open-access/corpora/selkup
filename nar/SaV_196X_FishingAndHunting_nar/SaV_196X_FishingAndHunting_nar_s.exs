<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>SaV_196X_FishingAndHunting_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">SaV_196X_FishingAndHunting_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">124</ud-information>
            <ud-information attribute-name="# HIAT:w">90</ud-information>
            <ud-information attribute-name="# e">90</ud-information>
            <ud-information attribute-name="# HIAT:u">16</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SaV">
            <abbreviation>SaV</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SaV"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T90" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">ödət</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">kɨget</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">čalwadišpat</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">qwäl</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">qoččik</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">kɨge</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">šünǯʼugun</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">čaːǯa</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_32" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">poqqɨ</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">kɨnčet</ts>
                  <nts id="Seg_38" n="HIAT:ip">,</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">koči</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">qwälp</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">qwalla</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_51" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">mat</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">qwälčukuhak</ts>
                  <nts id="Seg_57" n="HIAT:ip">.</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_60" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_62" n="HIAT:w" s="T15">šaːɣetqwap</ts>
                  <nts id="Seg_63" n="HIAT:ip">,</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_66" n="HIAT:w" s="T16">čačerqɨhap</ts>
                  <nts id="Seg_67" n="HIAT:ip">,</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_70" n="HIAT:w" s="T17">enäɨtqwap</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_74" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_76" n="HIAT:w" s="T18">kalʼǯomp</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_79" n="HIAT:w" s="T19">awešpɨkwap</ts>
                  <nts id="Seg_80" n="HIAT:ip">.</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_83" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_85" n="HIAT:w" s="T20">tawaršam</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_88" n="HIAT:w" s="T21">aː</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_91" n="HIAT:w" s="T22">kɨgak</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_94" n="HIAT:w" s="T23">qwälčugu</ts>
                  <nts id="Seg_95" n="HIAT:ip">,</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_98" n="HIAT:w" s="T24">čeːlʼ</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_101" n="HIAT:w" s="T25">mol</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_104" n="HIAT:w" s="T26">qonta</ts>
                  <nts id="Seg_105" n="HIAT:ip">,</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_108" n="HIAT:w" s="T27">lär</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_111" n="HIAT:w" s="T28">qwätɨmɨndɨt</ts>
                  <nts id="Seg_112" n="HIAT:ip">.</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_115" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_117" n="HIAT:w" s="T29">qanna</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_120" n="HIAT:w" s="T30">meːdila</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_123" n="HIAT:w" s="T31">meːka</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_126" n="HIAT:w" s="T32">tölčila</ts>
                  <nts id="Seg_127" n="HIAT:ip">,</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_130" n="HIAT:w" s="T33">meka</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_133" n="HIAT:w" s="T34">qwäl</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_136" n="HIAT:w" s="T35">meːeš</ts>
                  <nts id="Seg_137" n="HIAT:ip">,</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_140" n="HIAT:w" s="T36">qwäːšak</ts>
                  <nts id="Seg_141" n="HIAT:ip">.</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_144" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_146" n="HIAT:w" s="T37">tabetčihak</ts>
                  <nts id="Seg_147" n="HIAT:ip">,</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_150" n="HIAT:w" s="T38">čweːga</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_153" n="HIAT:w" s="T39">nʼartomut</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_156" n="HIAT:w" s="T40">čaǯihap</ts>
                  <nts id="Seg_157" n="HIAT:ip">.</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_160" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_162" n="HIAT:w" s="T41">tabek</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_165" n="HIAT:w" s="T42">kuralbant</ts>
                  <nts id="Seg_166" n="HIAT:ip">,</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_169" n="HIAT:w" s="T43">mat</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_172" n="HIAT:w" s="T44">kanaŋm</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_175" n="HIAT:w" s="T45">nʼoːle</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_178" n="HIAT:w" s="T46">qwändɨt</ts>
                  <nts id="Seg_179" n="HIAT:ip">.</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_182" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_184" n="HIAT:w" s="T47">a</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_187" n="HIAT:w" s="T48">mat</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_190" n="HIAT:w" s="T49">kanap</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_193" n="HIAT:w" s="T50">nʼole</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_196" n="HIAT:w" s="T51">qwännɨndak</ts>
                  <nts id="Seg_197" n="HIAT:ip">.</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_200" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_202" n="HIAT:w" s="T52">kanaŋɨm</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_205" n="HIAT:w" s="T53">muːtela</ts>
                  <nts id="Seg_206" n="HIAT:ip">,</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_209" n="HIAT:w" s="T54">arɨk</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_212" n="HIAT:w" s="T55">höuhe</ts>
                  <nts id="Seg_213" n="HIAT:ip">,</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_216" n="HIAT:w" s="T56">hoŋmut</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_219" n="HIAT:w" s="T57">čaːrak</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_222" n="HIAT:w" s="T58">peqq</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_225" n="HIAT:w" s="T59">nɨŋnɨnda</ts>
                  <nts id="Seg_226" n="HIAT:ip">.</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_229" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_231" n="HIAT:w" s="T60">mat</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_234" n="HIAT:w" s="T61">tüldem</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_237" n="HIAT:w" s="T62">tɨ</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_240" n="HIAT:w" s="T63">mišannap</ts>
                  <nts id="Seg_241" n="HIAT:ip">,</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_244" n="HIAT:w" s="T64">olɨl</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_247" n="HIAT:w" s="T65">patron</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_250" n="HIAT:w" s="T66">panneǯʼindak</ts>
                  <nts id="Seg_251" n="HIAT:ip">,</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_254" n="HIAT:w" s="T67">čatčap</ts>
                  <nts id="Seg_255" n="HIAT:ip">,</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_258" n="HIAT:w" s="T68">peq</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_261" n="HIAT:w" s="T69">ille</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_264" n="HIAT:w" s="T70">aːlča</ts>
                  <nts id="Seg_265" n="HIAT:ip">.</nts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_268" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_270" n="HIAT:w" s="T71">patom</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_273" n="HIAT:w" s="T72">qobɨm</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_276" n="HIAT:w" s="T73">täkargu</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_279" n="HIAT:w" s="T74">andɨrčak</ts>
                  <nts id="Seg_280" n="HIAT:ip">.</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_283" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_285" n="HIAT:w" s="T75">kərap</ts>
                  <nts id="Seg_286" n="HIAT:ip">,</nts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_289" n="HIAT:w" s="T76">waǯʼində</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_292" n="HIAT:w" s="T77">okkɨrmɨndɨ</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_295" n="HIAT:w" s="T78">pannap</ts>
                  <nts id="Seg_296" n="HIAT:ip">.</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_299" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_301" n="HIAT:w" s="T79">tüp</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_304" n="HIAT:w" s="T80">čaːdap</ts>
                  <nts id="Seg_305" n="HIAT:ip">,</nts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_308" n="HIAT:w" s="T81">čabɨhe</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_311" n="HIAT:w" s="T82">qareː</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_314" n="HIAT:w" s="T83">waǯʼe</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_317" n="HIAT:w" s="T84">maškelǯindak</ts>
                  <nts id="Seg_318" n="HIAT:ip">,</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_321" n="HIAT:w" s="T85">waǯʼom</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_324" n="HIAT:w" s="T86">perčaa</ts>
                  <nts id="Seg_325" n="HIAT:ip">,</nts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_328" n="HIAT:w" s="T87">čaip</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_331" n="HIAT:w" s="T88">ödegu</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_334" n="HIAT:w" s="T89">omdak</ts>
                  <nts id="Seg_335" n="HIAT:ip">.</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T90" id="Seg_337" n="sc" s="T0">
               <ts e="T1" id="Seg_339" n="e" s="T0">ödət </ts>
               <ts e="T2" id="Seg_341" n="e" s="T1">kɨget </ts>
               <ts e="T3" id="Seg_343" n="e" s="T2">čalwadišpat. </ts>
               <ts e="T4" id="Seg_345" n="e" s="T3">qwäl </ts>
               <ts e="T5" id="Seg_347" n="e" s="T4">qoččik </ts>
               <ts e="T6" id="Seg_349" n="e" s="T5">kɨge </ts>
               <ts e="T7" id="Seg_351" n="e" s="T6">šünǯʼugun </ts>
               <ts e="T8" id="Seg_353" n="e" s="T7">čaːǯa. </ts>
               <ts e="T9" id="Seg_355" n="e" s="T8">poqqɨ </ts>
               <ts e="T10" id="Seg_357" n="e" s="T9">kɨnčet, </ts>
               <ts e="T11" id="Seg_359" n="e" s="T10">koči </ts>
               <ts e="T12" id="Seg_361" n="e" s="T11">qwälp </ts>
               <ts e="T13" id="Seg_363" n="e" s="T12">qwalla. </ts>
               <ts e="T14" id="Seg_365" n="e" s="T13">mat </ts>
               <ts e="T15" id="Seg_367" n="e" s="T14">qwälčukuhak. </ts>
               <ts e="T16" id="Seg_369" n="e" s="T15">šaːɣetqwap, </ts>
               <ts e="T17" id="Seg_371" n="e" s="T16">čačerqɨhap, </ts>
               <ts e="T18" id="Seg_373" n="e" s="T17">enäɨtqwap. </ts>
               <ts e="T19" id="Seg_375" n="e" s="T18">kalʼǯomp </ts>
               <ts e="T20" id="Seg_377" n="e" s="T19">awešpɨkwap. </ts>
               <ts e="T21" id="Seg_379" n="e" s="T20">tawaršam </ts>
               <ts e="T22" id="Seg_381" n="e" s="T21">aː </ts>
               <ts e="T23" id="Seg_383" n="e" s="T22">kɨgak </ts>
               <ts e="T24" id="Seg_385" n="e" s="T23">qwälčugu, </ts>
               <ts e="T25" id="Seg_387" n="e" s="T24">čeːlʼ </ts>
               <ts e="T26" id="Seg_389" n="e" s="T25">mol </ts>
               <ts e="T27" id="Seg_391" n="e" s="T26">qonta, </ts>
               <ts e="T28" id="Seg_393" n="e" s="T27">lär </ts>
               <ts e="T29" id="Seg_395" n="e" s="T28">qwätɨmɨndɨt. </ts>
               <ts e="T30" id="Seg_397" n="e" s="T29">qanna </ts>
               <ts e="T31" id="Seg_399" n="e" s="T30">meːdila </ts>
               <ts e="T32" id="Seg_401" n="e" s="T31">meːka </ts>
               <ts e="T33" id="Seg_403" n="e" s="T32">tölčila, </ts>
               <ts e="T34" id="Seg_405" n="e" s="T33">meka </ts>
               <ts e="T35" id="Seg_407" n="e" s="T34">qwäl </ts>
               <ts e="T36" id="Seg_409" n="e" s="T35">meːeš, </ts>
               <ts e="T37" id="Seg_411" n="e" s="T36">qwäːšak. </ts>
               <ts e="T38" id="Seg_413" n="e" s="T37">tabetčihak, </ts>
               <ts e="T39" id="Seg_415" n="e" s="T38">čweːga </ts>
               <ts e="T40" id="Seg_417" n="e" s="T39">nʼartomut </ts>
               <ts e="T41" id="Seg_419" n="e" s="T40">čaǯihap. </ts>
               <ts e="T42" id="Seg_421" n="e" s="T41">tabek </ts>
               <ts e="T43" id="Seg_423" n="e" s="T42">kuralbant, </ts>
               <ts e="T44" id="Seg_425" n="e" s="T43">mat </ts>
               <ts e="T45" id="Seg_427" n="e" s="T44">kanaŋm </ts>
               <ts e="T46" id="Seg_429" n="e" s="T45">nʼoːle </ts>
               <ts e="T47" id="Seg_431" n="e" s="T46">qwändɨt. </ts>
               <ts e="T48" id="Seg_433" n="e" s="T47">a </ts>
               <ts e="T49" id="Seg_435" n="e" s="T48">mat </ts>
               <ts e="T50" id="Seg_437" n="e" s="T49">kanap </ts>
               <ts e="T51" id="Seg_439" n="e" s="T50">nʼole </ts>
               <ts e="T52" id="Seg_441" n="e" s="T51">qwännɨndak. </ts>
               <ts e="T53" id="Seg_443" n="e" s="T52">kanaŋɨm </ts>
               <ts e="T54" id="Seg_445" n="e" s="T53">muːtela, </ts>
               <ts e="T55" id="Seg_447" n="e" s="T54">arɨk </ts>
               <ts e="T56" id="Seg_449" n="e" s="T55">höuhe, </ts>
               <ts e="T57" id="Seg_451" n="e" s="T56">hoŋmut </ts>
               <ts e="T58" id="Seg_453" n="e" s="T57">čaːrak </ts>
               <ts e="T59" id="Seg_455" n="e" s="T58">peqq </ts>
               <ts e="T60" id="Seg_457" n="e" s="T59">nɨŋnɨnda. </ts>
               <ts e="T61" id="Seg_459" n="e" s="T60">mat </ts>
               <ts e="T62" id="Seg_461" n="e" s="T61">tüldem </ts>
               <ts e="T63" id="Seg_463" n="e" s="T62">tɨ </ts>
               <ts e="T64" id="Seg_465" n="e" s="T63">mišannap, </ts>
               <ts e="T65" id="Seg_467" n="e" s="T64">olɨl </ts>
               <ts e="T66" id="Seg_469" n="e" s="T65">patron </ts>
               <ts e="T67" id="Seg_471" n="e" s="T66">panneǯʼindak, </ts>
               <ts e="T68" id="Seg_473" n="e" s="T67">čatčap, </ts>
               <ts e="T69" id="Seg_475" n="e" s="T68">peq </ts>
               <ts e="T70" id="Seg_477" n="e" s="T69">ille </ts>
               <ts e="T71" id="Seg_479" n="e" s="T70">aːlča. </ts>
               <ts e="T72" id="Seg_481" n="e" s="T71">patom </ts>
               <ts e="T73" id="Seg_483" n="e" s="T72">qobɨm </ts>
               <ts e="T74" id="Seg_485" n="e" s="T73">täkargu </ts>
               <ts e="T75" id="Seg_487" n="e" s="T74">andɨrčak. </ts>
               <ts e="T76" id="Seg_489" n="e" s="T75">kərap, </ts>
               <ts e="T77" id="Seg_491" n="e" s="T76">waǯʼində </ts>
               <ts e="T78" id="Seg_493" n="e" s="T77">okkɨrmɨndɨ </ts>
               <ts e="T79" id="Seg_495" n="e" s="T78">pannap. </ts>
               <ts e="T80" id="Seg_497" n="e" s="T79">tüp </ts>
               <ts e="T81" id="Seg_499" n="e" s="T80">čaːdap, </ts>
               <ts e="T82" id="Seg_501" n="e" s="T81">čabɨhe </ts>
               <ts e="T83" id="Seg_503" n="e" s="T82">qareː </ts>
               <ts e="T84" id="Seg_505" n="e" s="T83">waǯʼe </ts>
               <ts e="T85" id="Seg_507" n="e" s="T84">maškelǯindak, </ts>
               <ts e="T86" id="Seg_509" n="e" s="T85">waǯʼom </ts>
               <ts e="T87" id="Seg_511" n="e" s="T86">perčaa, </ts>
               <ts e="T88" id="Seg_513" n="e" s="T87">čaip </ts>
               <ts e="T89" id="Seg_515" n="e" s="T88">ödegu </ts>
               <ts e="T90" id="Seg_517" n="e" s="T89">omdak. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_518" s="T0">SaV_196X_FishingAndHunting_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_519" s="T3">SaV_196X_FishingAndHunting_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_520" s="T8">SaV_196X_FishingAndHunting_nar.003 (001.003)</ta>
            <ta e="T15" id="Seg_521" s="T13">SaV_196X_FishingAndHunting_nar.004 (001.004)</ta>
            <ta e="T18" id="Seg_522" s="T15">SaV_196X_FishingAndHunting_nar.005 (001.005)</ta>
            <ta e="T20" id="Seg_523" s="T18">SaV_196X_FishingAndHunting_nar.006 (001.006)</ta>
            <ta e="T29" id="Seg_524" s="T20">SaV_196X_FishingAndHunting_nar.007 (001.007)</ta>
            <ta e="T37" id="Seg_525" s="T29">SaV_196X_FishingAndHunting_nar.008 (001.008)</ta>
            <ta e="T41" id="Seg_526" s="T37">SaV_196X_FishingAndHunting_nar.009 (001.009)</ta>
            <ta e="T47" id="Seg_527" s="T41">SaV_196X_FishingAndHunting_nar.010 (001.010)</ta>
            <ta e="T52" id="Seg_528" s="T47">SaV_196X_FishingAndHunting_nar.011 (001.011)</ta>
            <ta e="T60" id="Seg_529" s="T52">SaV_196X_FishingAndHunting_nar.012 (001.012)</ta>
            <ta e="T71" id="Seg_530" s="T60">SaV_196X_FishingAndHunting_nar.013 (001.013)</ta>
            <ta e="T75" id="Seg_531" s="T71">SaV_196X_FishingAndHunting_nar.014 (001.014)</ta>
            <ta e="T79" id="Seg_532" s="T75">SaV_196X_FishingAndHunting_nar.015 (001.015)</ta>
            <ta e="T90" id="Seg_533" s="T79">SaV_196X_FishingAndHunting_nar.016 (001.016)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_534" s="T0">ӧдът кы′гет чал′вадишпат.</ta>
            <ta e="T8" id="Seg_535" s="T3">kwӓл kоччик кы′ге ′шʼӱндʼжʼугун ча̄джа.</ta>
            <ta e="T13" id="Seg_536" s="T8">поkkы кын′чет, кочи kwӓlп kwаllа.</ta>
            <ta e="T15" id="Seg_537" s="T13">мат ′kwӓlчукуhак.</ta>
            <ta e="T18" id="Seg_538" s="T15">′ша̄ɣетkwап, чачерkыhап, е′нӓытkwап.</ta>
            <ta e="T20" id="Seg_539" s="T18">калʼ′джомп а′вешпыквап.</ta>
            <ta e="T29" id="Seg_540" s="T20">та′варщам а̄ кы‵гак ′kwӓlчугу, ′че̄лʼ моl kонта, lӓр ′kwӓтымындыт.</ta>
            <ta e="T37" id="Seg_541" s="T29">′kанна ′ме̄ди′ла ме̄ка ′тӧlчиlа, мека kwӓl ме̄еш, kwӓ̄ш[ж]ак.</ta>
            <ta e="T41" id="Seg_542" s="T37">та′бетчиhак, чве̄га нʼартомут ′чаджиhап.</ta>
            <ta e="T47" id="Seg_543" s="T41">та′б̂ек ку′раlбант, мат ка′наңм нʼо̄lе ′kwӓндыт.</ta>
            <ta e="T52" id="Seg_544" s="T47">а мат ка′нап нʼоlе kwӓннындак.</ta>
            <ta e="T60" id="Seg_545" s="T52">ка′наңым мӯ′теlа, ′арык ′hе[ӧ]уhе, hоңмут ′ча̄рак ′пеkk ′ныңнында.</ta>
            <ta e="T71" id="Seg_546" s="T60">мат тӱlдем ты ми′шаннап, оlыl пат′рон па′ннедʼзʼиндак, ч[тч]артч[тч]ап, пеk и′llе а̄lча.</ta>
            <ta e="T75" id="Seg_547" s="T71">па′том kобым ′тӓкаргу ′андырчак.</ta>
            <ta e="T79" id="Seg_548" s="T75">къ′рап, вадʼжʼиндъ оккырмын′ды па′ннап.</ta>
            <ta e="T90" id="Seg_549" s="T79">тӱп ′ча̄дап, ′чабыhе kа′ре̄ вадʼжʼе маш′кеlджиндак, вадʼжʼом пер[ъ]′ча̄, чаип ӧдегу ′омдак.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_550" s="T0">ödət kɨget čalvadišpat.</ta>
            <ta e="T8" id="Seg_551" s="T3">qwäl qoččik kɨge šünǯʼugun čaːǯa.</ta>
            <ta e="T13" id="Seg_552" s="T8">poqqɨ kɨnčet, koči qwälp qwalla.</ta>
            <ta e="T15" id="Seg_553" s="T13">mat qwälčukuhak.</ta>
            <ta e="T18" id="Seg_554" s="T15">šaːɣetqwap, čačerqɨhap, enäɨtqwap.</ta>
            <ta e="T20" id="Seg_555" s="T18">kalʼǯomp avešpɨkvap.</ta>
            <ta e="T29" id="Seg_556" s="T20">tavaršam aː kɨgak qwälčugu, čeːlʼ mol qonta, lär qwätɨmɨndɨt.</ta>
            <ta e="T37" id="Seg_557" s="T29">qanna meːdila meːka tölčila, meka qwäl meːeš, qwäːš[ʒ]ak.</ta>
            <ta e="T41" id="Seg_558" s="T37">tabetčihak, čveːga nʼartomut čaǯihap.</ta>
            <ta e="T47" id="Seg_559" s="T41">tab̂ek kuralbant, mat kanaŋm nʼoːle qwändɨt.</ta>
            <ta e="T52" id="Seg_560" s="T47">a mat kanap nʼole qwännɨndak.</ta>
            <ta e="T60" id="Seg_561" s="T52">kanaŋɨm muːtela, arɨk he[ö]uhe, hoŋmut čaːrak peqq nɨŋnɨnda.</ta>
            <ta e="T71" id="Seg_562" s="T60">mat tüldem tɨ mišannap, olɨl patron pannedʼzʼindak, č[tč]artč[tč]ap, peq ille aːlča.</ta>
            <ta e="T75" id="Seg_563" s="T71">patom qobɨm täkargu andɨrčak.</ta>
            <ta e="T79" id="Seg_564" s="T75">kərap, vadʼʒʼində okkɨrmɨndɨ pannap.</ta>
            <ta e="T90" id="Seg_565" s="T79">tüp čaːdap, čabɨhe qareː vadʼʒʼe maškelǯindak, vadʼʒʼom per[ə]čaː, čaip ödegu omdak.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_566" s="T0">ödət kɨget čalwadišpat. </ta>
            <ta e="T8" id="Seg_567" s="T3">qwäl qoččik kɨge šünǯʼugun čaːǯa. </ta>
            <ta e="T13" id="Seg_568" s="T8">poqqɨ kɨnčet, koči qwälp qwalla. </ta>
            <ta e="T15" id="Seg_569" s="T13">mat qwälčukuhak. </ta>
            <ta e="T18" id="Seg_570" s="T15">šaːɣetqwap, čačerqɨhap, enäɨtqwap. </ta>
            <ta e="T20" id="Seg_571" s="T18">kalʼǯomp awešpɨkwap. </ta>
            <ta e="T29" id="Seg_572" s="T20">tawaršam aː kɨgak qwälčugu, čeːlʼ mol qonta, lär qwätɨmɨndɨt. </ta>
            <ta e="T37" id="Seg_573" s="T29">qanna meːdila meːka tölčila, meka qwäl meːeš, qwäːšak. </ta>
            <ta e="T41" id="Seg_574" s="T37">tabetčihak, čweːga nʼartomut čaǯihap. </ta>
            <ta e="T47" id="Seg_575" s="T41">tabek kuralbant, mat kanaŋm nʼoːle qwändɨt. </ta>
            <ta e="T52" id="Seg_576" s="T47">a mat kanap nʼole qwännɨndak. </ta>
            <ta e="T60" id="Seg_577" s="T52">kanaŋɨm muːtela, arɨk höuhe, hoŋmut čaːrak peqq nɨŋnɨnda. </ta>
            <ta e="T71" id="Seg_578" s="T60">mat tüldem tɨ mišannap, olɨl patron panneǯʼindak, čatčap, peq ille aːlča. </ta>
            <ta e="T75" id="Seg_579" s="T71">patom qobɨm täkargu andɨrčak. </ta>
            <ta e="T79" id="Seg_580" s="T75">kərap, waǯʼində okkɨrmɨndɨ pannap. </ta>
            <ta e="T90" id="Seg_581" s="T79">tüp čaːdap, čabɨhe qareː waǯʼe maškelǯindak, waǯʼom perčaa, čaip ödegu omdak. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_582" s="T0">ödə-t</ta>
            <ta e="T2" id="Seg_583" s="T1">kɨge-t</ta>
            <ta e="T3" id="Seg_584" s="T2">čalwa-di-špa-t</ta>
            <ta e="T4" id="Seg_585" s="T3">qwäl</ta>
            <ta e="T5" id="Seg_586" s="T4">qoččik</ta>
            <ta e="T6" id="Seg_587" s="T5">kɨgǝ</ta>
            <ta e="T7" id="Seg_588" s="T6">šünǯʼu-gun</ta>
            <ta e="T8" id="Seg_589" s="T7">čaːǯa</ta>
            <ta e="T9" id="Seg_590" s="T8">poqqɨ</ta>
            <ta e="T10" id="Seg_591" s="T9">kɨnče-t</ta>
            <ta e="T11" id="Seg_592" s="T10">koči</ta>
            <ta e="T12" id="Seg_593" s="T11">qwäl-p</ta>
            <ta e="T13" id="Seg_594" s="T12">qwal-la</ta>
            <ta e="T14" id="Seg_595" s="T13">mat</ta>
            <ta e="T15" id="Seg_596" s="T14">qwäl-ču-ku-ha-k</ta>
            <ta e="T16" id="Seg_597" s="T15">šaːɣ-e-t-q-wa-p</ta>
            <ta e="T17" id="Seg_598" s="T16">čačer-qɨ-ha-p</ta>
            <ta e="T18" id="Seg_599" s="T17">enäɨt-q-wa-p</ta>
            <ta e="T19" id="Seg_600" s="T18">ken-lʼ-ǯomp</ta>
            <ta e="T20" id="Seg_601" s="T19">aw-e-špɨ-k-wa-p</ta>
            <ta e="T21" id="Seg_602" s="T20">tawarš-a-m</ta>
            <ta e="T22" id="Seg_603" s="T21">aː</ta>
            <ta e="T23" id="Seg_604" s="T22">kɨga-k</ta>
            <ta e="T24" id="Seg_605" s="T23">qwäl-ču-gu</ta>
            <ta e="T25" id="Seg_606" s="T24">čeːlʼ</ta>
            <ta e="T26" id="Seg_607" s="T25">mol</ta>
            <ta e="T27" id="Seg_608" s="T26">qonta</ta>
            <ta e="T28" id="Seg_609" s="T27">lär</ta>
            <ta e="T29" id="Seg_610" s="T28">qwätɨm-ɨ-ndɨ-t</ta>
            <ta e="T30" id="Seg_611" s="T29">qanna</ta>
            <ta e="T31" id="Seg_612" s="T30">meːdi-la</ta>
            <ta e="T32" id="Seg_613" s="T31">meːka</ta>
            <ta e="T33" id="Seg_614" s="T32">tö-lči-la</ta>
            <ta e="T34" id="Seg_615" s="T33">meka</ta>
            <ta e="T35" id="Seg_616" s="T34">qwäl</ta>
            <ta e="T36" id="Seg_617" s="T35">meː-eš</ta>
            <ta e="T37" id="Seg_618" s="T36">qwäːša-k</ta>
            <ta e="T38" id="Seg_619" s="T37">tabe-tči-ha-k</ta>
            <ta e="T39" id="Seg_620" s="T38">čweːga</ta>
            <ta e="T40" id="Seg_621" s="T39">nʼar-to-mut</ta>
            <ta e="T41" id="Seg_622" s="T40">čaǯi-ha-p</ta>
            <ta e="T42" id="Seg_623" s="T41">tabek</ta>
            <ta e="T43" id="Seg_624" s="T42">kur-a-l-ba-nt</ta>
            <ta e="T44" id="Seg_625" s="T43">mat</ta>
            <ta e="T45" id="Seg_626" s="T44">kanaŋ-m</ta>
            <ta e="T46" id="Seg_627" s="T45">nʼoː-le</ta>
            <ta e="T47" id="Seg_628" s="T46">qwän-dɨ-tɨ</ta>
            <ta e="T48" id="Seg_629" s="T47">a</ta>
            <ta e="T49" id="Seg_630" s="T48">mat</ta>
            <ta e="T50" id="Seg_631" s="T49">kana-p</ta>
            <ta e="T51" id="Seg_632" s="T50">nʼo-le</ta>
            <ta e="T52" id="Seg_633" s="T51">qwän-nɨ-nda-k</ta>
            <ta e="T53" id="Seg_634" s="T52">kanaŋ-ɨ-m</ta>
            <ta e="T54" id="Seg_635" s="T53">muːt-e-la</ta>
            <ta e="T55" id="Seg_636" s="T54">arɨk</ta>
            <ta e="T56" id="Seg_637" s="T55">höu-he</ta>
            <ta e="T57" id="Seg_638" s="T56">hoŋ-mut</ta>
            <ta e="T58" id="Seg_639" s="T57">čaːr-a-k</ta>
            <ta e="T59" id="Seg_640" s="T58">peqq</ta>
            <ta e="T60" id="Seg_641" s="T59">nɨŋ-nɨ-nda</ta>
            <ta e="T61" id="Seg_642" s="T60">mat</ta>
            <ta e="T62" id="Seg_643" s="T61">tülde-m</ta>
            <ta e="T63" id="Seg_644" s="T62">tɨ</ta>
            <ta e="T64" id="Seg_645" s="T63">mišan-na-p</ta>
            <ta e="T65" id="Seg_646" s="T64">olɨ-l</ta>
            <ta e="T66" id="Seg_647" s="T65">patron</ta>
            <ta e="T67" id="Seg_648" s="T66">panne-ǯʼi-nda-k</ta>
            <ta e="T68" id="Seg_649" s="T67">čatča-p</ta>
            <ta e="T69" id="Seg_650" s="T68">peq</ta>
            <ta e="T70" id="Seg_651" s="T69">ille</ta>
            <ta e="T71" id="Seg_652" s="T70">aːlča</ta>
            <ta e="T72" id="Seg_653" s="T71">patom</ta>
            <ta e="T73" id="Seg_654" s="T72">qobɨ-m</ta>
            <ta e="T74" id="Seg_655" s="T73">täkar-gu</ta>
            <ta e="T75" id="Seg_656" s="T74">andɨr-ča-k</ta>
            <ta e="T76" id="Seg_657" s="T75">kəra-p</ta>
            <ta e="T77" id="Seg_658" s="T76">waǯʼi-ndə</ta>
            <ta e="T78" id="Seg_659" s="T77">okkɨr-mɨn-dɨ</ta>
            <ta e="T79" id="Seg_660" s="T78">pan-na-p</ta>
            <ta e="T80" id="Seg_661" s="T79">tü-p</ta>
            <ta e="T81" id="Seg_662" s="T80">čaːda-p</ta>
            <ta e="T82" id="Seg_663" s="T81">čabɨ-he</ta>
            <ta e="T83" id="Seg_664" s="T82">qareː</ta>
            <ta e="T84" id="Seg_665" s="T83">waǯʼe</ta>
            <ta e="T85" id="Seg_666" s="T84">maške-lǯi-nda-k</ta>
            <ta e="T86" id="Seg_667" s="T85">waǯʼo-m</ta>
            <ta e="T87" id="Seg_668" s="T86">per-ča-a</ta>
            <ta e="T88" id="Seg_669" s="T87">čai-p</ta>
            <ta e="T89" id="Seg_670" s="T88">öde-gu</ta>
            <ta e="T90" id="Seg_671" s="T89">omda-k</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_672" s="T0">öːdɨ-t</ta>
            <ta e="T2" id="Seg_673" s="T1">qɨgä-t</ta>
            <ta e="T3" id="Seg_674" s="T2">čalwa-dʼi-špɨ-dət</ta>
            <ta e="T4" id="Seg_675" s="T3">qwel</ta>
            <ta e="T5" id="Seg_676" s="T4">koček</ta>
            <ta e="T6" id="Seg_677" s="T5">qɨgä</ta>
            <ta e="T7" id="Seg_678" s="T6">šünde-qɨn</ta>
            <ta e="T8" id="Seg_679" s="T7">čaːǯɨ</ta>
            <ta e="T9" id="Seg_680" s="T8">poq</ta>
            <ta e="T10" id="Seg_681" s="T9">kɨnčɨ-ätɨ</ta>
            <ta e="T11" id="Seg_682" s="T10">koček</ta>
            <ta e="T12" id="Seg_683" s="T11">qwel-p</ta>
            <ta e="T13" id="Seg_684" s="T12">kwat-la</ta>
            <ta e="T14" id="Seg_685" s="T13">man</ta>
            <ta e="T15" id="Seg_686" s="T14">qwel-ču-ku-sɨ-k</ta>
            <ta e="T16" id="Seg_687" s="T15">šaɣ-ɨ-t-ku-wa-m</ta>
            <ta e="T17" id="Seg_688" s="T16">čagar-ku-sɨ-m</ta>
            <ta e="T18" id="Seg_689" s="T17">enäɨt-ku-wa-m</ta>
            <ta e="T19" id="Seg_690" s="T18">ken-lʼ-čomb</ta>
            <ta e="T20" id="Seg_691" s="T19">am-ɨ-špɨ-ku-wa-m</ta>
            <ta e="T21" id="Seg_692" s="T20">tovariš-ɨ-mɨ</ta>
            <ta e="T22" id="Seg_693" s="T21">aː</ta>
            <ta e="T23" id="Seg_694" s="T22">kɨge-k</ta>
            <ta e="T24" id="Seg_695" s="T23">qwel-ču-gu</ta>
            <ta e="T25" id="Seg_696" s="T24">čeːl</ta>
            <ta e="T26" id="Seg_697" s="T25">mol</ta>
            <ta e="T27" id="Seg_698" s="T26">qontɨ</ta>
            <ta e="T28" id="Seg_699" s="T27">lärɨ</ta>
            <ta e="T29" id="Seg_700" s="T28">qwätɨm-ɨ-ndɨ-tɨ</ta>
            <ta e="T30" id="Seg_701" s="T29">ken</ta>
            <ta e="T31" id="Seg_702" s="T30">meːdi-la</ta>
            <ta e="T32" id="Seg_703" s="T31">mäkkä</ta>
            <ta e="T33" id="Seg_704" s="T32">töː-lʼčǝ-la</ta>
            <ta e="T34" id="Seg_705" s="T33">mäkkä</ta>
            <ta e="T35" id="Seg_706" s="T34">qwel</ta>
            <ta e="T36" id="Seg_707" s="T35">me-äšɨk</ta>
            <ta e="T37" id="Seg_708" s="T36">qwäːša-k</ta>
            <ta e="T38" id="Seg_709" s="T37">tabek-ču-ŋɨ-k</ta>
            <ta e="T39" id="Seg_710" s="T38">čwɛːka</ta>
            <ta e="T40" id="Seg_711" s="T39">nʼar-to-mun</ta>
            <ta e="T41" id="Seg_712" s="T40">čaːǯɨ-ŋɨ-m</ta>
            <ta e="T42" id="Seg_713" s="T41">tabek</ta>
            <ta e="T43" id="Seg_714" s="T42">kur-ɨ-lɨ-mbɨ-ndɨ</ta>
            <ta e="T44" id="Seg_715" s="T43">man</ta>
            <ta e="T45" id="Seg_716" s="T44">kanak-mɨ</ta>
            <ta e="T46" id="Seg_717" s="T45">nʼeː-le</ta>
            <ta e="T47" id="Seg_718" s="T46">qwän-ntɨ-tɨ</ta>
            <ta e="T48" id="Seg_719" s="T47">a</ta>
            <ta e="T49" id="Seg_720" s="T48">man</ta>
            <ta e="T50" id="Seg_721" s="T49">kanak-m</ta>
            <ta e="T51" id="Seg_722" s="T50">nʼeː-le</ta>
            <ta e="T52" id="Seg_723" s="T51">qwän-ntɨ-ndɨ-k</ta>
            <ta e="T53" id="Seg_724" s="T52">kanak-ɨ-mɨ</ta>
            <ta e="T54" id="Seg_725" s="T53">muːd-ɨ-lɨ</ta>
            <ta e="T55" id="Seg_726" s="T54">aːrq</ta>
            <ta e="T56" id="Seg_727" s="T55">höw-se</ta>
            <ta e="T57" id="Seg_728" s="T56">hoŋ-mun</ta>
            <ta e="T58" id="Seg_729" s="T57">čar-ɨ-k</ta>
            <ta e="T59" id="Seg_730" s="T58">peqqa</ta>
            <ta e="T60" id="Seg_731" s="T59">nɨŋ-ntɨ-ndɨ</ta>
            <ta e="T61" id="Seg_732" s="T60">man</ta>
            <ta e="T62" id="Seg_733" s="T61">tüːlʼde-m</ta>
            <ta e="T63" id="Seg_734" s="T62">tɨ</ta>
            <ta e="T64" id="Seg_735" s="T63">meššal-ŋɨ-m</ta>
            <ta e="T65" id="Seg_736" s="T64">olo-lʼ</ta>
            <ta e="T66" id="Seg_737" s="T65">patron</ta>
            <ta e="T67" id="Seg_738" s="T66">panne-lʼčǝ-ndɨ-k</ta>
            <ta e="T68" id="Seg_739" s="T67">čačɨ-m</ta>
            <ta e="T69" id="Seg_740" s="T68">peqqa</ta>
            <ta e="T70" id="Seg_741" s="T69">illä</ta>
            <ta e="T71" id="Seg_742" s="T70">alʼči</ta>
            <ta e="T72" id="Seg_743" s="T71">patom</ta>
            <ta e="T73" id="Seg_744" s="T72">kobɨ-m</ta>
            <ta e="T74" id="Seg_745" s="T73">taqqer-gu</ta>
            <ta e="T75" id="Seg_746" s="T74">andɨr-če-k</ta>
            <ta e="T76" id="Seg_747" s="T75">kɨrɨ-m</ta>
            <ta e="T77" id="Seg_748" s="T76">waǯʼe-nde</ta>
            <ta e="T78" id="Seg_749" s="T77">okkər-mun-tɨ</ta>
            <ta e="T79" id="Seg_750" s="T78">pan-ŋɨ-m</ta>
            <ta e="T80" id="Seg_751" s="T79">tüː-m</ta>
            <ta e="T81" id="Seg_752" s="T80">čʼadɨ-m</ta>
            <ta e="T82" id="Seg_753" s="T81">čabɨ-se</ta>
            <ta e="T83" id="Seg_754" s="T82">qareː</ta>
            <ta e="T84" id="Seg_755" s="T83">waǯʼe</ta>
            <ta e="T85" id="Seg_756" s="T84">maške-lʼčǝ-ndɨ-k</ta>
            <ta e="T86" id="Seg_757" s="T85">waǯʼe-m</ta>
            <ta e="T87" id="Seg_758" s="T86">per-če-ŋɨ</ta>
            <ta e="T88" id="Seg_759" s="T87">čaːj-p</ta>
            <ta e="T89" id="Seg_760" s="T88">üdɨ-gu</ta>
            <ta e="T90" id="Seg_761" s="T89">omde-k</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_762" s="T0">spring-ADV.LOC</ta>
            <ta e="T2" id="Seg_763" s="T1">river-PL.[NOM]</ta>
            <ta e="T3" id="Seg_764" s="T2">%%-DRV-IPFV2-3PL</ta>
            <ta e="T4" id="Seg_765" s="T3">fish.[NOM]</ta>
            <ta e="T5" id="Seg_766" s="T4">much</ta>
            <ta e="T6" id="Seg_767" s="T5">river.[NOM]</ta>
            <ta e="T7" id="Seg_768" s="T6">inside-LOC</ta>
            <ta e="T8" id="Seg_769" s="T7">go.[3SG.S]</ta>
            <ta e="T9" id="Seg_770" s="T8">net.[NOM]</ta>
            <ta e="T10" id="Seg_771" s="T9">set-IMP.2SG.O</ta>
            <ta e="T11" id="Seg_772" s="T10">much</ta>
            <ta e="T12" id="Seg_773" s="T11">fish-ACC</ta>
            <ta e="T13" id="Seg_774" s="T12">catch-FUT.[3SG.S]</ta>
            <ta e="T14" id="Seg_775" s="T13">I.NOM</ta>
            <ta e="T15" id="Seg_776" s="T14">fish-CAP-HAB-PST-1SG.S</ta>
            <ta e="T16" id="Seg_777" s="T15">salt-EP-TR-HAB-CO-1SG.O</ta>
            <ta e="T17" id="Seg_778" s="T16">dry-HAB-PST-1SG.O</ta>
            <ta e="T18" id="Seg_779" s="T17">%%-HAB-CO-1SG.O</ta>
            <ta e="T19" id="Seg_780" s="T18">winter-ADJZ-length.[NOM]</ta>
            <ta e="T20" id="Seg_781" s="T19">eat-EP-IPFV2-HAB-CO-1SG.O</ta>
            <ta e="T21" id="Seg_782" s="T20">companion-EP-1SG</ta>
            <ta e="T22" id="Seg_783" s="T21">NEG</ta>
            <ta e="T23" id="Seg_784" s="T22">want-3SG.S</ta>
            <ta e="T24" id="Seg_785" s="T23">fish-CAP-INF</ta>
            <ta e="T25" id="Seg_786" s="T24">day.[NOM]</ta>
            <ta e="T26" id="Seg_787" s="T25">supposedly</ta>
            <ta e="T27" id="Seg_788" s="T26">sleep.[3SG.S]</ta>
            <ta e="T28" id="Seg_789" s="T27">song.[NOM]</ta>
            <ta e="T29" id="Seg_790" s="T28">sing-EP-INFER-3SG.O</ta>
            <ta e="T30" id="Seg_791" s="T29">winter.[NOM]</ta>
            <ta e="T31" id="Seg_792" s="T30">come.up-FUT.[3SG.S]</ta>
            <ta e="T32" id="Seg_793" s="T31">I.ALL</ta>
            <ta e="T33" id="Seg_794" s="T32">come-PFV-FUT.[3SG.S]</ta>
            <ta e="T34" id="Seg_795" s="T33">I.ALL</ta>
            <ta e="T35" id="Seg_796" s="T34">fish.[NOM]</ta>
            <ta e="T36" id="Seg_797" s="T35">do-IMP.2SG.S</ta>
            <ta e="T37" id="Seg_798" s="T36">get.hungry-1SG.S</ta>
            <ta e="T38" id="Seg_799" s="T37">squirrel-TR-CO-1SG.S</ta>
            <ta e="T39" id="Seg_800" s="T38">narrow</ta>
            <ta e="T40" id="Seg_801" s="T39">swamp-lake-PROL</ta>
            <ta e="T41" id="Seg_802" s="T40">run-CO-1SG.O</ta>
            <ta e="T42" id="Seg_803" s="T41">squirrel.[NOM]</ta>
            <ta e="T43" id="Seg_804" s="T42">run-EP-RES-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T44" id="Seg_805" s="T43">I.GEN</ta>
            <ta e="T45" id="Seg_806" s="T44">dog-1SG</ta>
            <ta e="T46" id="Seg_807" s="T45">hunt-CVB</ta>
            <ta e="T47" id="Seg_808" s="T46">go.away-IPFV-3SG.O</ta>
            <ta e="T48" id="Seg_809" s="T47">but</ta>
            <ta e="T49" id="Seg_810" s="T48">I.NOM</ta>
            <ta e="T50" id="Seg_811" s="T49">dog-ACC</ta>
            <ta e="T51" id="Seg_812" s="T50">hunt-CVB</ta>
            <ta e="T52" id="Seg_813" s="T51">go.away-IPFV-INFER-1SG.S</ta>
            <ta e="T53" id="Seg_814" s="T52">dog-EP-1SG</ta>
            <ta e="T54" id="Seg_815" s="T53">bark-EP-RES.[3SG.S]</ta>
            <ta e="T55" id="Seg_816" s="T54">other</ta>
            <ta e="T56" id="Seg_817" s="T55">voice-INSTR</ta>
            <ta e="T57" id="Seg_818" s="T56">promontory-PROL</ta>
            <ta e="T58" id="Seg_819" s="T57">jump.out-EP-3SG.S</ta>
            <ta e="T59" id="Seg_820" s="T58">elk.[NOM]</ta>
            <ta e="T60" id="Seg_821" s="T59">stand-IPFV-INFER.[3SG.S]</ta>
            <ta e="T61" id="Seg_822" s="T60">I.NOM</ta>
            <ta e="T62" id="Seg_823" s="T61">rifle-ACC</ta>
            <ta e="T63" id="Seg_824" s="T62">this</ta>
            <ta e="T64" id="Seg_825" s="T63">pull.out-CO-1SG.O</ta>
            <ta e="T65" id="Seg_826" s="T64">bullet-ADJZ</ta>
            <ta e="T66" id="Seg_827" s="T65">shell.[NOM]</ta>
            <ta e="T67" id="Seg_828" s="T66">%%-PFV-INFER-1SG.S</ta>
            <ta e="T68" id="Seg_829" s="T67">shoot-1SG.O</ta>
            <ta e="T69" id="Seg_830" s="T68">elk.[NOM]</ta>
            <ta e="T70" id="Seg_831" s="T69">down</ta>
            <ta e="T71" id="Seg_832" s="T70">fall.[3SG.S]</ta>
            <ta e="T72" id="Seg_833" s="T71">then</ta>
            <ta e="T73" id="Seg_834" s="T72">skin-ACC</ta>
            <ta e="T74" id="Seg_835" s="T73">pull.off-INF</ta>
            <ta e="T75" id="Seg_836" s="T74">%%-DRV-1SG.S</ta>
            <ta e="T76" id="Seg_837" s="T75">pull.off-1SG.O</ta>
            <ta e="T77" id="Seg_838" s="T76">meat-ILL</ta>
            <ta e="T78" id="Seg_839" s="T77">one-PROL-3SG</ta>
            <ta e="T79" id="Seg_840" s="T78">put-CO-1SG.O</ta>
            <ta e="T80" id="Seg_841" s="T79">fire-ACC</ta>
            <ta e="T81" id="Seg_842" s="T80">burn-1SG.O</ta>
            <ta e="T82" id="Seg_843" s="T81">stick-INSTR</ta>
            <ta e="T83" id="Seg_844" s="T82">%%</ta>
            <ta e="T84" id="Seg_845" s="T83">meat.[NOM]</ta>
            <ta e="T85" id="Seg_846" s="T84">put-PFV-INFER-1SG.S</ta>
            <ta e="T86" id="Seg_847" s="T85">meat-ACC</ta>
            <ta e="T87" id="Seg_848" s="T86">fry-DRV-CO.[3SG.S]</ta>
            <ta e="T88" id="Seg_849" s="T87">tea-ACC</ta>
            <ta e="T89" id="Seg_850" s="T88">drink-INF</ta>
            <ta e="T90" id="Seg_851" s="T89">sit.down-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_852" s="T0">весна-ADV.LOC</ta>
            <ta e="T2" id="Seg_853" s="T1">река-PL.[NOM]</ta>
            <ta e="T3" id="Seg_854" s="T2">%%-DRV-IPFV2-3PL</ta>
            <ta e="T4" id="Seg_855" s="T3">рыба.[NOM]</ta>
            <ta e="T5" id="Seg_856" s="T4">много</ta>
            <ta e="T6" id="Seg_857" s="T5">река.[NOM]</ta>
            <ta e="T7" id="Seg_858" s="T6">нутро-LOC</ta>
            <ta e="T8" id="Seg_859" s="T7">идти.[3SG.S]</ta>
            <ta e="T9" id="Seg_860" s="T8">сеть.[NOM]</ta>
            <ta e="T10" id="Seg_861" s="T9">поставить-IMP.2SG.O</ta>
            <ta e="T11" id="Seg_862" s="T10">много</ta>
            <ta e="T12" id="Seg_863" s="T11">рыба-ACC</ta>
            <ta e="T13" id="Seg_864" s="T12">поймать-FUT.[3SG.S]</ta>
            <ta e="T14" id="Seg_865" s="T13">я.NOM</ta>
            <ta e="T15" id="Seg_866" s="T14">рыба-CAP-HAB-PST-1SG.S</ta>
            <ta e="T16" id="Seg_867" s="T15">соль-EP-TR-HAB-CO-1SG.O</ta>
            <ta e="T17" id="Seg_868" s="T16">засушить-HAB-PST-1SG.O</ta>
            <ta e="T18" id="Seg_869" s="T17">%%-HAB-CO-1SG.O</ta>
            <ta e="T19" id="Seg_870" s="T18">зима-ADJZ-длина.[NOM]</ta>
            <ta e="T20" id="Seg_871" s="T19">есть-EP-IPFV2-HAB-CO-1SG.O</ta>
            <ta e="T21" id="Seg_872" s="T20">товарищ-EP-1SG</ta>
            <ta e="T22" id="Seg_873" s="T21">NEG</ta>
            <ta e="T23" id="Seg_874" s="T22">хотеть-3SG.S</ta>
            <ta e="T24" id="Seg_875" s="T23">рыба-CAP-INF</ta>
            <ta e="T25" id="Seg_876" s="T24">день.[NOM]</ta>
            <ta e="T26" id="Seg_877" s="T25">мол</ta>
            <ta e="T27" id="Seg_878" s="T26">спять.[3SG.S]</ta>
            <ta e="T28" id="Seg_879" s="T27">песня.[NOM]</ta>
            <ta e="T29" id="Seg_880" s="T28">спеть-EP-INFER-3SG.O</ta>
            <ta e="T30" id="Seg_881" s="T29">зима.[NOM]</ta>
            <ta e="T31" id="Seg_882" s="T30">подойти-FUT.[3SG.S]</ta>
            <ta e="T32" id="Seg_883" s="T31">я.ALL</ta>
            <ta e="T33" id="Seg_884" s="T32">прийти-PFV-FUT.[3SG.S]</ta>
            <ta e="T34" id="Seg_885" s="T33">я.ALL</ta>
            <ta e="T35" id="Seg_886" s="T34">рыба.[NOM]</ta>
            <ta e="T36" id="Seg_887" s="T35">делать-IMP.2SG.S</ta>
            <ta e="T37" id="Seg_888" s="T36">проголодаться-1SG.S</ta>
            <ta e="T38" id="Seg_889" s="T37">белка-TR-CO-1SG.S</ta>
            <ta e="T39" id="Seg_890" s="T38">узкий</ta>
            <ta e="T40" id="Seg_891" s="T39">болото-озеро-PROL</ta>
            <ta e="T41" id="Seg_892" s="T40">бегать-CO-1SG.O</ta>
            <ta e="T42" id="Seg_893" s="T41">белка.[NOM]</ta>
            <ta e="T43" id="Seg_894" s="T42">бегать-EP-RES-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T44" id="Seg_895" s="T43">я.GEN</ta>
            <ta e="T45" id="Seg_896" s="T44">собака-1SG</ta>
            <ta e="T46" id="Seg_897" s="T45">гнать-CVB</ta>
            <ta e="T47" id="Seg_898" s="T46">пойти-IPFV-3SG.O</ta>
            <ta e="T48" id="Seg_899" s="T47">а</ta>
            <ta e="T49" id="Seg_900" s="T48">я.NOM</ta>
            <ta e="T50" id="Seg_901" s="T49">собака-ACC</ta>
            <ta e="T51" id="Seg_902" s="T50">гнать-CVB</ta>
            <ta e="T52" id="Seg_903" s="T51">пойти-IPFV-INFER-1SG.S</ta>
            <ta e="T53" id="Seg_904" s="T52">собака-EP-1SG</ta>
            <ta e="T54" id="Seg_905" s="T53">лаять-EP-RES.[3SG.S]</ta>
            <ta e="T55" id="Seg_906" s="T54">другой</ta>
            <ta e="T56" id="Seg_907" s="T55">голос-INSTR</ta>
            <ta e="T57" id="Seg_908" s="T56">мыс-PROL</ta>
            <ta e="T58" id="Seg_909" s="T57">выскочить-EP-3SG.S</ta>
            <ta e="T59" id="Seg_910" s="T58">лось.[NOM]</ta>
            <ta e="T60" id="Seg_911" s="T59">стоять-IPFV-INFER.[3SG.S]</ta>
            <ta e="T61" id="Seg_912" s="T60">я.NOM</ta>
            <ta e="T62" id="Seg_913" s="T61">ружье-ACC</ta>
            <ta e="T63" id="Seg_914" s="T62">этот</ta>
            <ta e="T64" id="Seg_915" s="T63">выдернуть-CO-1SG.O</ta>
            <ta e="T65" id="Seg_916" s="T64">пуля-ADJZ</ta>
            <ta e="T66" id="Seg_917" s="T65">патрон.[NOM]</ta>
            <ta e="T67" id="Seg_918" s="T66">%%-PFV-INFER-1SG.S</ta>
            <ta e="T68" id="Seg_919" s="T67">стрелять-1SG.O</ta>
            <ta e="T69" id="Seg_920" s="T68">лось.[NOM]</ta>
            <ta e="T70" id="Seg_921" s="T69">вниз</ta>
            <ta e="T71" id="Seg_922" s="T70">упасть.[3SG.S]</ta>
            <ta e="T72" id="Seg_923" s="T71">потом</ta>
            <ta e="T73" id="Seg_924" s="T72">шкура-ACC</ta>
            <ta e="T74" id="Seg_925" s="T73">ободрать-INF</ta>
            <ta e="T75" id="Seg_926" s="T74">%%-DRV-1SG.S</ta>
            <ta e="T76" id="Seg_927" s="T75">ободрать-1SG.O</ta>
            <ta e="T77" id="Seg_928" s="T76">мясо-ILL</ta>
            <ta e="T78" id="Seg_929" s="T77">один-PROL-3SG</ta>
            <ta e="T79" id="Seg_930" s="T78">положить-CO-1SG.O</ta>
            <ta e="T80" id="Seg_931" s="T79">огонь-ACC</ta>
            <ta e="T81" id="Seg_932" s="T80">гореть-1SG.O</ta>
            <ta e="T82" id="Seg_933" s="T81">палочка-INSTR</ta>
            <ta e="T83" id="Seg_934" s="T82">%%</ta>
            <ta e="T84" id="Seg_935" s="T83">мясо.[NOM]</ta>
            <ta e="T85" id="Seg_936" s="T84">класть-PFV-INFER-1SG.S</ta>
            <ta e="T86" id="Seg_937" s="T85">мясо-ACC</ta>
            <ta e="T87" id="Seg_938" s="T86">поджарить-DRV-CO.[3SG.S]</ta>
            <ta e="T88" id="Seg_939" s="T87">чай-ACC</ta>
            <ta e="T89" id="Seg_940" s="T88">пить-INF</ta>
            <ta e="T90" id="Seg_941" s="T89">сесть-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_942" s="T0">n-adv:case</ta>
            <ta e="T2" id="Seg_943" s="T1">n-n:num-n:case</ta>
            <ta e="T3" id="Seg_944" s="T2">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T4" id="Seg_945" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_946" s="T4">quant</ta>
            <ta e="T6" id="Seg_947" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_948" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_949" s="T7">v-v:pn</ta>
            <ta e="T9" id="Seg_950" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_951" s="T9">v-v:mood.pn</ta>
            <ta e="T11" id="Seg_952" s="T10">quant</ta>
            <ta e="T12" id="Seg_953" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_954" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_955" s="T13">pers</ta>
            <ta e="T15" id="Seg_956" s="T14">n-n&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T16" id="Seg_957" s="T15">n-n:ins-n&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T17" id="Seg_958" s="T16">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T18" id="Seg_959" s="T17">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T19" id="Seg_960" s="T18">n-n&gt;adj-n-n:case</ta>
            <ta e="T20" id="Seg_961" s="T19">v-n:ins-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T21" id="Seg_962" s="T20">n-n:ins-n:poss</ta>
            <ta e="T22" id="Seg_963" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_964" s="T22">v-v:pn</ta>
            <ta e="T24" id="Seg_965" s="T23">n-n&gt;v-v:inf</ta>
            <ta e="T25" id="Seg_966" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_967" s="T25">adv</ta>
            <ta e="T27" id="Seg_968" s="T26">v-v:pn</ta>
            <ta e="T28" id="Seg_969" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_970" s="T28">v-n:ins-v:mood-v:pn</ta>
            <ta e="T30" id="Seg_971" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_972" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_973" s="T31">pers</ta>
            <ta e="T33" id="Seg_974" s="T32">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_975" s="T33">pers</ta>
            <ta e="T35" id="Seg_976" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_977" s="T35">v-v:mood.pn</ta>
            <ta e="T37" id="Seg_978" s="T36">v-v:pn</ta>
            <ta e="T38" id="Seg_979" s="T37">n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T39" id="Seg_980" s="T38">adj</ta>
            <ta e="T40" id="Seg_981" s="T39">n-n-n:case</ta>
            <ta e="T41" id="Seg_982" s="T40">v-v:ins-v:pn</ta>
            <ta e="T42" id="Seg_983" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_984" s="T42">v-n:ins-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T44" id="Seg_985" s="T43">pers</ta>
            <ta e="T45" id="Seg_986" s="T44">n-n:poss</ta>
            <ta e="T46" id="Seg_987" s="T45">v-v&gt;adv</ta>
            <ta e="T47" id="Seg_988" s="T46">v-v&gt;v-v:pn</ta>
            <ta e="T48" id="Seg_989" s="T47">conj</ta>
            <ta e="T49" id="Seg_990" s="T48">pers</ta>
            <ta e="T50" id="Seg_991" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_992" s="T50">v-v&gt;adv</ta>
            <ta e="T52" id="Seg_993" s="T51">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T53" id="Seg_994" s="T52">n-n:ins-n:poss</ta>
            <ta e="T54" id="Seg_995" s="T53">v-n:ins-v&gt;v-v:pn</ta>
            <ta e="T55" id="Seg_996" s="T54">adj</ta>
            <ta e="T56" id="Seg_997" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_998" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_999" s="T57">v-n:ins-v:pn</ta>
            <ta e="T59" id="Seg_1000" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_1001" s="T59">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T61" id="Seg_1002" s="T60">pers</ta>
            <ta e="T62" id="Seg_1003" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_1004" s="T62">dem</ta>
            <ta e="T64" id="Seg_1005" s="T63">v-v:ins-v:pn</ta>
            <ta e="T65" id="Seg_1006" s="T64">n-n&gt;adj</ta>
            <ta e="T66" id="Seg_1007" s="T65">n-n:case</ta>
            <ta e="T67" id="Seg_1008" s="T66">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T68" id="Seg_1009" s="T67">v-v:pn</ta>
            <ta e="T69" id="Seg_1010" s="T68">n-n:case</ta>
            <ta e="T70" id="Seg_1011" s="T69">preverb</ta>
            <ta e="T71" id="Seg_1012" s="T70">v-v:pn</ta>
            <ta e="T72" id="Seg_1013" s="T71">adv</ta>
            <ta e="T73" id="Seg_1014" s="T72">n-n:case</ta>
            <ta e="T74" id="Seg_1015" s="T73">v-v:inf</ta>
            <ta e="T75" id="Seg_1016" s="T74">v-v&gt;v-v:pn</ta>
            <ta e="T76" id="Seg_1017" s="T75">v-v:pn</ta>
            <ta e="T77" id="Seg_1018" s="T76">n-n:case</ta>
            <ta e="T78" id="Seg_1019" s="T77">num-n:case-n:poss</ta>
            <ta e="T79" id="Seg_1020" s="T78">v-v:ins-v:pn</ta>
            <ta e="T80" id="Seg_1021" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_1022" s="T80">v-v:pn</ta>
            <ta e="T82" id="Seg_1023" s="T81">n-n:case</ta>
            <ta e="T84" id="Seg_1024" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_1025" s="T84">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T86" id="Seg_1026" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_1027" s="T86">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T88" id="Seg_1028" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_1029" s="T88">v-v:inf</ta>
            <ta e="T90" id="Seg_1030" s="T89">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1031" s="T0">n</ta>
            <ta e="T2" id="Seg_1032" s="T1">n</ta>
            <ta e="T3" id="Seg_1033" s="T2">v</ta>
            <ta e="T4" id="Seg_1034" s="T3">n</ta>
            <ta e="T5" id="Seg_1035" s="T4">quant</ta>
            <ta e="T6" id="Seg_1036" s="T5">n</ta>
            <ta e="T7" id="Seg_1037" s="T6">n</ta>
            <ta e="T8" id="Seg_1038" s="T7">v</ta>
            <ta e="T9" id="Seg_1039" s="T8">n</ta>
            <ta e="T10" id="Seg_1040" s="T9">v</ta>
            <ta e="T11" id="Seg_1041" s="T10">quant</ta>
            <ta e="T12" id="Seg_1042" s="T11">n</ta>
            <ta e="T13" id="Seg_1043" s="T12">v</ta>
            <ta e="T14" id="Seg_1044" s="T13">pers</ta>
            <ta e="T15" id="Seg_1045" s="T14">v</ta>
            <ta e="T16" id="Seg_1046" s="T15">v</ta>
            <ta e="T17" id="Seg_1047" s="T16">v</ta>
            <ta e="T18" id="Seg_1048" s="T17">v</ta>
            <ta e="T19" id="Seg_1049" s="T18">adv</ta>
            <ta e="T20" id="Seg_1050" s="T19">v</ta>
            <ta e="T21" id="Seg_1051" s="T20">n</ta>
            <ta e="T22" id="Seg_1052" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_1053" s="T22">v</ta>
            <ta e="T24" id="Seg_1054" s="T23">v</ta>
            <ta e="T25" id="Seg_1055" s="T24">n</ta>
            <ta e="T26" id="Seg_1056" s="T25">adv</ta>
            <ta e="T27" id="Seg_1057" s="T26">v</ta>
            <ta e="T28" id="Seg_1058" s="T27">n</ta>
            <ta e="T29" id="Seg_1059" s="T28">v</ta>
            <ta e="T30" id="Seg_1060" s="T29">n</ta>
            <ta e="T31" id="Seg_1061" s="T30">v</ta>
            <ta e="T32" id="Seg_1062" s="T31">pers</ta>
            <ta e="T33" id="Seg_1063" s="T32">v</ta>
            <ta e="T34" id="Seg_1064" s="T33">pers</ta>
            <ta e="T35" id="Seg_1065" s="T34">n</ta>
            <ta e="T36" id="Seg_1066" s="T35">v</ta>
            <ta e="T37" id="Seg_1067" s="T36">v</ta>
            <ta e="T38" id="Seg_1068" s="T37">v</ta>
            <ta e="T39" id="Seg_1069" s="T38">adj</ta>
            <ta e="T40" id="Seg_1070" s="T39">n</ta>
            <ta e="T41" id="Seg_1071" s="T40">v</ta>
            <ta e="T42" id="Seg_1072" s="T41">n</ta>
            <ta e="T43" id="Seg_1073" s="T42">v</ta>
            <ta e="T44" id="Seg_1074" s="T43">pers</ta>
            <ta e="T45" id="Seg_1075" s="T44">n</ta>
            <ta e="T46" id="Seg_1076" s="T45">adv</ta>
            <ta e="T47" id="Seg_1077" s="T46">v</ta>
            <ta e="T48" id="Seg_1078" s="T47">conj</ta>
            <ta e="T49" id="Seg_1079" s="T48">pers</ta>
            <ta e="T50" id="Seg_1080" s="T49">n</ta>
            <ta e="T51" id="Seg_1081" s="T50">adv</ta>
            <ta e="T52" id="Seg_1082" s="T51">v</ta>
            <ta e="T53" id="Seg_1083" s="T52">n</ta>
            <ta e="T54" id="Seg_1084" s="T53">v</ta>
            <ta e="T55" id="Seg_1085" s="T54">adj</ta>
            <ta e="T56" id="Seg_1086" s="T55">n</ta>
            <ta e="T57" id="Seg_1087" s="T56">n</ta>
            <ta e="T58" id="Seg_1088" s="T57">v</ta>
            <ta e="T59" id="Seg_1089" s="T58">n</ta>
            <ta e="T60" id="Seg_1090" s="T59">v</ta>
            <ta e="T61" id="Seg_1091" s="T60">pers</ta>
            <ta e="T62" id="Seg_1092" s="T61">n</ta>
            <ta e="T63" id="Seg_1093" s="T62">dem</ta>
            <ta e="T64" id="Seg_1094" s="T63">v</ta>
            <ta e="T65" id="Seg_1095" s="T64">adj</ta>
            <ta e="T66" id="Seg_1096" s="T65">n</ta>
            <ta e="T67" id="Seg_1097" s="T66">v</ta>
            <ta e="T68" id="Seg_1098" s="T67">v</ta>
            <ta e="T69" id="Seg_1099" s="T68">n</ta>
            <ta e="T70" id="Seg_1100" s="T69">preverb</ta>
            <ta e="T71" id="Seg_1101" s="T70">v</ta>
            <ta e="T72" id="Seg_1102" s="T71">adv</ta>
            <ta e="T73" id="Seg_1103" s="T72">n</ta>
            <ta e="T74" id="Seg_1104" s="T73">v</ta>
            <ta e="T75" id="Seg_1105" s="T74">v</ta>
            <ta e="T76" id="Seg_1106" s="T75">v</ta>
            <ta e="T77" id="Seg_1107" s="T76">n</ta>
            <ta e="T78" id="Seg_1108" s="T77">num</ta>
            <ta e="T79" id="Seg_1109" s="T78">v</ta>
            <ta e="T80" id="Seg_1110" s="T79">n</ta>
            <ta e="T81" id="Seg_1111" s="T80">v</ta>
            <ta e="T82" id="Seg_1112" s="T81">n</ta>
            <ta e="T84" id="Seg_1113" s="T83">n</ta>
            <ta e="T85" id="Seg_1114" s="T84">v</ta>
            <ta e="T86" id="Seg_1115" s="T85">n</ta>
            <ta e="T87" id="Seg_1116" s="T86">v</ta>
            <ta e="T88" id="Seg_1117" s="T87">n</ta>
            <ta e="T89" id="Seg_1118" s="T88">v</ta>
            <ta e="T90" id="Seg_1119" s="T89">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1120" s="T1">np:S</ta>
            <ta e="T3" id="Seg_1121" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_1122" s="T3">np:S</ta>
            <ta e="T8" id="Seg_1123" s="T7">v:pred</ta>
            <ta e="T9" id="Seg_1124" s="T8">np:O</ta>
            <ta e="T10" id="Seg_1125" s="T9">0.2.h:S v:pred</ta>
            <ta e="T12" id="Seg_1126" s="T11">np:O</ta>
            <ta e="T13" id="Seg_1127" s="T12">0.3:S v:pred</ta>
            <ta e="T14" id="Seg_1128" s="T13">pro.h:S</ta>
            <ta e="T15" id="Seg_1129" s="T14">v:pred</ta>
            <ta e="T16" id="Seg_1130" s="T15">0.1.h:S 0.3:O v:pred</ta>
            <ta e="T17" id="Seg_1131" s="T16">0.1.h:S 0.3:O v:pred</ta>
            <ta e="T18" id="Seg_1132" s="T17">0.1.h:S 0.3:O v:pred</ta>
            <ta e="T20" id="Seg_1133" s="T19">0.1.h:S 0.3:O v:pred</ta>
            <ta e="T21" id="Seg_1134" s="T20">np.h:S</ta>
            <ta e="T23" id="Seg_1135" s="T22">v:pred</ta>
            <ta e="T24" id="Seg_1136" s="T23">v:O</ta>
            <ta e="T27" id="Seg_1137" s="T26">0.3.h:S v:pred</ta>
            <ta e="T28" id="Seg_1138" s="T27">np:O</ta>
            <ta e="T29" id="Seg_1139" s="T28">0.3.h:S v:pred</ta>
            <ta e="T30" id="Seg_1140" s="T29">np:S</ta>
            <ta e="T31" id="Seg_1141" s="T30">v:pred</ta>
            <ta e="T33" id="Seg_1142" s="T32">0.3.h:S v:pred</ta>
            <ta e="T35" id="Seg_1143" s="T34">np:O</ta>
            <ta e="T36" id="Seg_1144" s="T35">0.2.h:S v:pred</ta>
            <ta e="T37" id="Seg_1145" s="T36">0.1.h:S v:pred</ta>
            <ta e="T38" id="Seg_1146" s="T37">0.1.h:S v:pred</ta>
            <ta e="T41" id="Seg_1147" s="T40">0.1.h:S v:pred</ta>
            <ta e="T42" id="Seg_1148" s="T41">np:S</ta>
            <ta e="T43" id="Seg_1149" s="T42">v:pred</ta>
            <ta e="T45" id="Seg_1150" s="T44">np:S</ta>
            <ta e="T47" id="Seg_1151" s="T46">v:pred</ta>
            <ta e="T49" id="Seg_1152" s="T48">pro.h:S</ta>
            <ta e="T50" id="Seg_1153" s="T49">np:O</ta>
            <ta e="T52" id="Seg_1154" s="T51">v:pred</ta>
            <ta e="T53" id="Seg_1155" s="T52">np:S</ta>
            <ta e="T54" id="Seg_1156" s="T53">v:pred</ta>
            <ta e="T58" id="Seg_1157" s="T57">v:pred</ta>
            <ta e="T59" id="Seg_1158" s="T58">np:S</ta>
            <ta e="T60" id="Seg_1159" s="T59">0.3:S v:pred</ta>
            <ta e="T61" id="Seg_1160" s="T60">pro.h:S</ta>
            <ta e="T62" id="Seg_1161" s="T61">np:O</ta>
            <ta e="T64" id="Seg_1162" s="T63">v:pred</ta>
            <ta e="T66" id="Seg_1163" s="T65">np:O</ta>
            <ta e="T67" id="Seg_1164" s="T66">0.1.h:S v:pred</ta>
            <ta e="T68" id="Seg_1165" s="T67">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T69" id="Seg_1166" s="T68">np:S</ta>
            <ta e="T71" id="Seg_1167" s="T70">v:pred</ta>
            <ta e="T73" id="Seg_1168" s="T72">np:O</ta>
            <ta e="T74" id="Seg_1169" s="T73">v:O</ta>
            <ta e="T75" id="Seg_1170" s="T74">0.1.h:S v:pred</ta>
            <ta e="T76" id="Seg_1171" s="T75">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T79" id="Seg_1172" s="T78">0.1.h:S 0.3:O v:pred</ta>
            <ta e="T80" id="Seg_1173" s="T79">np:O</ta>
            <ta e="T81" id="Seg_1174" s="T80">0.1.h:S v:pred</ta>
            <ta e="T84" id="Seg_1175" s="T83">np:O</ta>
            <ta e="T85" id="Seg_1176" s="T84">0.1.h:S v:pred</ta>
            <ta e="T86" id="Seg_1177" s="T85">np:O</ta>
            <ta e="T87" id="Seg_1178" s="T86">0.1.h:S v:pred</ta>
            <ta e="T89" id="Seg_1179" s="T87">s:purp</ta>
            <ta e="T90" id="Seg_1180" s="T89">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_1181" s="T0">np:Time</ta>
            <ta e="T2" id="Seg_1182" s="T1">np:Th</ta>
            <ta e="T4" id="Seg_1183" s="T3">np:A</ta>
            <ta e="T6" id="Seg_1184" s="T5">np:Poss</ta>
            <ta e="T7" id="Seg_1185" s="T6">np:L</ta>
            <ta e="T9" id="Seg_1186" s="T8">np:Th</ta>
            <ta e="T10" id="Seg_1187" s="T9">0.2.h:A</ta>
            <ta e="T12" id="Seg_1188" s="T11">np:Th</ta>
            <ta e="T13" id="Seg_1189" s="T12">0.3:A</ta>
            <ta e="T14" id="Seg_1190" s="T13">pro.h:A</ta>
            <ta e="T16" id="Seg_1191" s="T15">0.1.h:A 0.3:P</ta>
            <ta e="T17" id="Seg_1192" s="T16">0.1.h:A 0.3:P</ta>
            <ta e="T18" id="Seg_1193" s="T17">0.1.h:A 0.3:Th</ta>
            <ta e="T19" id="Seg_1194" s="T18">np:Time</ta>
            <ta e="T20" id="Seg_1195" s="T19">0.1.h:A 0.3:P</ta>
            <ta e="T21" id="Seg_1196" s="T20">0.1.h:Poss np.h:E</ta>
            <ta e="T24" id="Seg_1197" s="T23">v:Th</ta>
            <ta e="T25" id="Seg_1198" s="T24">np:Time</ta>
            <ta e="T27" id="Seg_1199" s="T26">0.3.h:Th</ta>
            <ta e="T28" id="Seg_1200" s="T27">np:Th</ta>
            <ta e="T29" id="Seg_1201" s="T28">0.3.h:A</ta>
            <ta e="T30" id="Seg_1202" s="T29">np:Th</ta>
            <ta e="T32" id="Seg_1203" s="T31">np.h:G</ta>
            <ta e="T33" id="Seg_1204" s="T32">0.3.h:A</ta>
            <ta e="T34" id="Seg_1205" s="T33">pro.h:R</ta>
            <ta e="T35" id="Seg_1206" s="T34">np:Th</ta>
            <ta e="T36" id="Seg_1207" s="T35">0.2.h:A</ta>
            <ta e="T37" id="Seg_1208" s="T36">0.1.h:E</ta>
            <ta e="T38" id="Seg_1209" s="T37">0.1.h:A</ta>
            <ta e="T40" id="Seg_1210" s="T39">np:Path</ta>
            <ta e="T41" id="Seg_1211" s="T40">0.1.h:A</ta>
            <ta e="T42" id="Seg_1212" s="T41">np:A</ta>
            <ta e="T44" id="Seg_1213" s="T43">pro.h:Poss</ta>
            <ta e="T45" id="Seg_1214" s="T44">np:A</ta>
            <ta e="T49" id="Seg_1215" s="T48">pro.h:A</ta>
            <ta e="T50" id="Seg_1216" s="T49">np:Th</ta>
            <ta e="T53" id="Seg_1217" s="T52">np:A 0.1.h:Poss</ta>
            <ta e="T56" id="Seg_1218" s="T55">np:Ins</ta>
            <ta e="T57" id="Seg_1219" s="T56">np:Path</ta>
            <ta e="T59" id="Seg_1220" s="T58">np:A</ta>
            <ta e="T60" id="Seg_1221" s="T59">0.3:Th</ta>
            <ta e="T61" id="Seg_1222" s="T60">pro.h:A</ta>
            <ta e="T62" id="Seg_1223" s="T61">np:Th</ta>
            <ta e="T66" id="Seg_1224" s="T65">np:Th</ta>
            <ta e="T67" id="Seg_1225" s="T66">0.1.h:A</ta>
            <ta e="T68" id="Seg_1226" s="T67">0.1.h:A 0.3:P</ta>
            <ta e="T69" id="Seg_1227" s="T68">np:P</ta>
            <ta e="T73" id="Seg_1228" s="T72">np:P</ta>
            <ta e="T74" id="Seg_1229" s="T73">v:Th</ta>
            <ta e="T75" id="Seg_1230" s="T74">0.1.h:A</ta>
            <ta e="T76" id="Seg_1231" s="T75">0.1.h:A 0.3:P</ta>
            <ta e="T79" id="Seg_1232" s="T78">0.1.h:A 0.3:Th</ta>
            <ta e="T80" id="Seg_1233" s="T79">np:P</ta>
            <ta e="T81" id="Seg_1234" s="T80">0.1.h:A</ta>
            <ta e="T82" id="Seg_1235" s="T81">np:Path</ta>
            <ta e="T84" id="Seg_1236" s="T83">np:Th</ta>
            <ta e="T85" id="Seg_1237" s="T84">0.1.h:A</ta>
            <ta e="T86" id="Seg_1238" s="T85">np:P</ta>
            <ta e="T87" id="Seg_1239" s="T86">0.1.h:A</ta>
            <ta e="T88" id="Seg_1240" s="T87">np:P</ta>
            <ta e="T90" id="Seg_1241" s="T89">0.1.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T21" id="Seg_1242" s="T20">RUS:cult</ta>
            <ta e="T26" id="Seg_1243" s="T25">RUS:core</ta>
            <ta e="T48" id="Seg_1244" s="T47">RUS:gram</ta>
            <ta e="T66" id="Seg_1245" s="T65">RUS:cult</ta>
            <ta e="T72" id="Seg_1246" s="T71">RUS:core</ta>
            <ta e="T88" id="Seg_1247" s="T87">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_1248" s="T0">Весной реки разливаются (?).</ta>
            <ta e="T8" id="Seg_1249" s="T3">По речке идет много рыб.</ta>
            <ta e="T13" id="Seg_1250" s="T8">Сеть поставь и много рыб добудешь.</ta>
            <ta e="T15" id="Seg_1251" s="T13">Я рыбачила.</ta>
            <ta e="T18" id="Seg_1252" s="T15">Посолила, высушила, повесила(?).</ta>
            <ta e="T20" id="Seg_1253" s="T18">Всю зиму ела.</ta>
            <ta e="T29" id="Seg_1254" s="T20">Мой товарищ не хочет рыбачить, он весь день спит и песни поет.</ta>
            <ta e="T37" id="Seg_1255" s="T29">Зима придет, ко мне придет: “Мне рыбу дай, я проголодалась”.</ta>
            <ta e="T41" id="Seg_1256" s="T37">Я белковала, по краю узкого болота шла.</ta>
            <ta e="T47" id="Seg_1257" s="T41">Белка бежала, моя собака за белкой побежала.</ta>
            <ta e="T52" id="Seg_1258" s="T47">А я за собакой побежала.</ta>
            <ta e="T60" id="Seg_1259" s="T52">Собака залаяла другим голосом, из-за мыса выскочил лось и стоит.</ta>
            <ta e="T71" id="Seg_1260" s="T60">Я ружье вздернул, пулевой патрон зарядил, выстрелил, лось упал.</ta>
            <ta e="T75" id="Seg_1261" s="T71">Потом начинаю(?) шкуру обдирать.</ta>
            <ta e="T79" id="Seg_1262" s="T75">Ободрал мясо, вместе положил.</ta>
            <ta e="T90" id="Seg_1263" s="T79">Огонь зажег, на чабсе мясо жарить поставил, мясо изжарилось, чай пить сел.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_1264" s="T0">In spring rivers overflow (?).</ta>
            <ta e="T8" id="Seg_1265" s="T3">In the river lots of fish is coming.</ta>
            <ta e="T13" id="Seg_1266" s="T8">Pit a net and you will catch lots of fish.</ta>
            <ta e="T15" id="Seg_1267" s="T13">I used to fish.</ta>
            <ta e="T18" id="Seg_1268" s="T15">I salted it, dries it out and hung it(?).</ta>
            <ta e="T20" id="Seg_1269" s="T18">I was eating it through the whole winter.</ta>
            <ta e="T29" id="Seg_1270" s="T20">My companion doesn't want to fish, the whole day he sleeps and sings songs.</ta>
            <ta e="T37" id="Seg_1271" s="T29">As the winter comes, she comes to me: “Give me some fish, I got hungry.”</ta>
            <ta e="T41" id="Seg_1272" s="T37">I was hunting squirrels, I went along a narrow swamp.</ta>
            <ta e="T47" id="Seg_1273" s="T41">A squirrel ran out, my dog ran after it.</ta>
            <ta e="T52" id="Seg_1274" s="T47">And I ran after my dog.</ta>
            <ta e="T60" id="Seg_1275" s="T52">My dog started barking with another voice, an elk jumped from behind a promontary and stopped.</ta>
            <ta e="T71" id="Seg_1276" s="T60">I pulled out my rifle, charged the bullets, shooted, the elk fell down.</ta>
            <ta e="T75" id="Seg_1277" s="T71">Then I started(?) to pull of its skin.</ta>
            <ta e="T79" id="Seg_1278" s="T75">I cut the meat, put it aside.</ta>
            <ta e="T90" id="Seg_1279" s="T79">I made fire, put the meat onto a stick, the meat roasted, I sat down to drink tea.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_1280" s="T0">Im Frühjahr treten die Flüsse über die Ufer.</ta>
            <ta e="T8" id="Seg_1281" s="T3">Viele Fische kommen im Fluss.</ta>
            <ta e="T13" id="Seg_1282" s="T8">Leg ein Netz aus und du fängst viel Fisch.</ta>
            <ta e="T15" id="Seg_1283" s="T13">Ich fischte.</ta>
            <ta e="T18" id="Seg_1284" s="T15">Ich salzte sie, trocknete sie, hängte sie auf (?)</ta>
            <ta e="T20" id="Seg_1285" s="T18">Ich aß sie den ganzen Winter lang.</ta>
            <ta e="T29" id="Seg_1286" s="T20">Mein Gefährte will nicht fischen, er schläft den ganzen Tag und singt Lieder.</ta>
            <ta e="T37" id="Seg_1287" s="T29">Der Winter kommt, sie kommt zu mir: "Gib mir Fisch, ich bin hungrig."</ta>
            <ta e="T41" id="Seg_1288" s="T37">Ich jagte Eichhörnchen, ich lief durch den schmalen Sumpf.</ta>
            <ta e="T47" id="Seg_1289" s="T41">Ein Eichhörnchen rannte davon, mein Hund jagte ihm nach.</ta>
            <ta e="T52" id="Seg_1290" s="T47">Und ich jagte dem Hund nach.</ta>
            <ta e="T60" id="Seg_1291" s="T52">Mein Hund begann mit einer anderen Stimme zu bellen, ein Elch sprang hinter einem Felsvorsprung hervor und hielt an.</ta>
            <ta e="T71" id="Seg_1292" s="T60">Ich holte mein Gewehr hervor, lud die Patronen, schoss, der Elch fiel.</ta>
            <ta e="T75" id="Seg_1293" s="T71">Dann begann ich, ihm die Haut abzuziehen.</ta>
            <ta e="T79" id="Seg_1294" s="T75">Ich schnitt das Fleisch, legte es zur Seite.</ta>
            <ta e="T90" id="Seg_1295" s="T79">Ich machte Feuer, schob das Fleisch auf einen Stock, briet das Fleisch, ich setzte mich um Tee zu trinken.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_1296" s="T0">весной реки разливаются</ta>
            <ta e="T8" id="Seg_1297" s="T3">рыбы много по речке идет</ta>
            <ta e="T13" id="Seg_1298" s="T8">сеть поставь и много рыбы добудешь</ta>
            <ta e="T15" id="Seg_1299" s="T13">я рыбачила</ta>
            <ta e="T18" id="Seg_1300" s="T15">посолила, высушила, повесила</ta>
            <ta e="T20" id="Seg_1301" s="T18">всю зиму ела</ta>
            <ta e="T29" id="Seg_1302" s="T20">товарищ не хочет рыбачить весь день спит песни (поет)</ta>
            <ta e="T37" id="Seg_1303" s="T29">зима придет ко мне придет мне рыбу дай голодная [есть хочу]</ta>
            <ta e="T41" id="Seg_1304" s="T37">белковал узкого болота по краю шла</ta>
            <ta e="T47" id="Seg_1305" s="T41">белка бежала моя собака за белкой побежала</ta>
            <ta e="T52" id="Seg_1306" s="T47">а я за собакой побежала</ta>
            <ta e="T60" id="Seg_1307" s="T52">собака залаяла другим тоном (голосом) из-за мыса выскочил лось стоит</ta>
            <ta e="T71" id="Seg_1308" s="T60">я ружье вздернул пулевой патрон зарядил стрелял лось упал</ta>
            <ta e="T75" id="Seg_1309" s="T71">потом шкуру обдирать начинаю</ta>
            <ta e="T79" id="Seg_1310" s="T75">ободрала мясо вместе положил</ta>
            <ta e="T90" id="Seg_1311" s="T79">огонь зажег [затопил] на чабсе мясо жарить поставил мясо изжарилось чай пить села</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T15" id="Seg_1312" s="T13">[BrM:] The speaker is male, but there is always a mess in the original translation between male and female forms of verbs, which are kept in the tier of russian translation.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
