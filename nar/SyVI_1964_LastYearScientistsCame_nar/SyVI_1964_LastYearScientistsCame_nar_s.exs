<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>_SyVI_1964_LastYearScientistsCame_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">SyVI_1964_LastYearScientistsCame_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">16</ud-information>
            <ud-information attribute-name="# HIAT:w">13</ud-information>
            <ud-information attribute-name="# e">13</ud-information>
            <ud-information attribute-name="# HIAT:u">3</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SPK">
            <abbreviation>SyVI</abbreviation>
            <sex value="f" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SPK"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T13" id="Seg_0" n="sc" s="T0">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">meːɣanɨt</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">pondan</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">tüːɣanɨt</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">quːla</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">nägɨrgu</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">süsügə</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">äǯi</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">täpla</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">ästə</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">kotʼättə</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_38" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">täp</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">tʼäraŋ</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">qonɨŋ</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T13" id="Seg_49" n="sc" s="T0">
               <ts e="T1" id="Seg_51" n="e" s="T0">meːɣanɨt </ts>
               <ts e="T2" id="Seg_53" n="e" s="T1">pondan </ts>
               <ts e="T3" id="Seg_55" n="e" s="T2">tüːɣanɨt </ts>
               <ts e="T4" id="Seg_57" n="e" s="T3">quːla </ts>
               <ts e="T5" id="Seg_59" n="e" s="T4">nägɨrgu </ts>
               <ts e="T6" id="Seg_61" n="e" s="T5">süsügə </ts>
               <ts e="T7" id="Seg_63" n="e" s="T6">äǯi. </ts>
               <ts e="T8" id="Seg_65" n="e" s="T7">täpla </ts>
               <ts e="T9" id="Seg_67" n="e" s="T8">ästə </ts>
               <ts e="T10" id="Seg_69" n="e" s="T9">kotʼättə. </ts>
               <ts e="T11" id="Seg_71" n="e" s="T10">täp </ts>
               <ts e="T12" id="Seg_73" n="e" s="T11">tʼäraŋ </ts>
               <ts e="T13" id="Seg_75" n="e" s="T12">qonɨŋ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_76" s="T0">SyVI_1964_LastYearScientistsCame_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_77" s="T7">SyVI_1964_LastYearScientistsCame_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_78" s="T10">SyVI_1964_LastYearScientistsCame_nar.003 (001.003)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T7" id="Seg_79" s="T0">′ме̄ɣаныт ′пондан ′тӱ̄ɣаныт ′kӯла нӓгыр′гу сӱ′сӱгъ ′ӓджи.</ta>
            <ta e="T10" id="Seg_80" s="T7">тӓп′ла ′ӓстъ ко′тʼӓттъ.</ta>
            <ta e="T13" id="Seg_81" s="T10">тӓп тʼӓ′раң ′kоның.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T7" id="Seg_82" s="T0">meːɣanɨt pondan tüːɣanɨt quːla nägɨrgu süsügə äǯi.</ta>
            <ta e="T10" id="Seg_83" s="T7">täpla ästə kotʼättə.</ta>
            <ta e="T13" id="Seg_84" s="T10">täp tʼäraŋ qonɨŋ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_85" s="T0">meːɣanɨt pondan tüːɣanɨt quːla nägɨrgu süsügə äǯi. </ta>
            <ta e="T10" id="Seg_86" s="T7">täpla ästə kotʼättə. </ta>
            <ta e="T13" id="Seg_87" s="T10">täp tʼäraŋ qonɨŋ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_88" s="T0">meːɣanɨt</ta>
            <ta e="T2" id="Seg_89" s="T1">po-ndan</ta>
            <ta e="T3" id="Seg_90" s="T2">tüː-ɣa-nɨ-t</ta>
            <ta e="T4" id="Seg_91" s="T3">quː-la</ta>
            <ta e="T5" id="Seg_92" s="T4">nägɨ-r-gu</ta>
            <ta e="T6" id="Seg_93" s="T5">süsügə</ta>
            <ta e="T7" id="Seg_94" s="T6">äǯi</ta>
            <ta e="T8" id="Seg_95" s="T7">täp-la</ta>
            <ta e="T9" id="Seg_96" s="T8">ä-s-tə</ta>
            <ta e="T10" id="Seg_97" s="T9">kotʼä-ttə</ta>
            <ta e="T11" id="Seg_98" s="T10">täp</ta>
            <ta e="T12" id="Seg_99" s="T11">tʼära-n</ta>
            <ta e="T13" id="Seg_100" s="T12">qonɨŋ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_101" s="T0">miɣnut</ta>
            <ta e="T2" id="Seg_102" s="T1">po-ndan</ta>
            <ta e="T3" id="Seg_103" s="T2">tüː-kku-ŋɨ-tɨt</ta>
            <ta e="T4" id="Seg_104" s="T3">qum-la</ta>
            <ta e="T5" id="Seg_105" s="T4">nagǝ-r-gu</ta>
            <ta e="T6" id="Seg_106" s="T5">süsügu</ta>
            <ta e="T7" id="Seg_107" s="T6">ɛːǯa</ta>
            <ta e="T8" id="Seg_108" s="T7">tap-la</ta>
            <ta e="T9" id="Seg_109" s="T8">eː-sɨ-tɨt</ta>
            <ta e="T10" id="Seg_110" s="T9">koči-tɨt</ta>
            <ta e="T11" id="Seg_111" s="T10">tap</ta>
            <ta e="T12" id="Seg_112" s="T11">tʼarɨ-n</ta>
            <ta e="T13" id="Seg_113" s="T12">qoːnɨŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_114" s="T0">we.ALL</ta>
            <ta e="T2" id="Seg_115" s="T1">year-%%</ta>
            <ta e="T3" id="Seg_116" s="T2">come-HAB-CO-3PL</ta>
            <ta e="T4" id="Seg_117" s="T3">human.being-PL.[NOM]</ta>
            <ta e="T5" id="Seg_118" s="T4">write-FRQ-INF</ta>
            <ta e="T6" id="Seg_119" s="T5">Selkup</ta>
            <ta e="T7" id="Seg_120" s="T6">word.[NOM]</ta>
            <ta e="T8" id="Seg_121" s="T7">(s)he-PL.[NOM]</ta>
            <ta e="T9" id="Seg_122" s="T8">be-PST-3PL</ta>
            <ta e="T10" id="Seg_123" s="T9">much-3PL</ta>
            <ta e="T11" id="Seg_124" s="T10">(s)he.[NOM]</ta>
            <ta e="T12" id="Seg_125" s="T11">say-3SG.S</ta>
            <ta e="T13" id="Seg_126" s="T12">many</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_127" s="T0">мы.ALL</ta>
            <ta e="T2" id="Seg_128" s="T1">год-%%</ta>
            <ta e="T3" id="Seg_129" s="T2">прийти-HAB-CO-3PL</ta>
            <ta e="T4" id="Seg_130" s="T3">человек-PL.[NOM]</ta>
            <ta e="T5" id="Seg_131" s="T4">писать-FRQ-INF</ta>
            <ta e="T6" id="Seg_132" s="T5">селькуп</ta>
            <ta e="T7" id="Seg_133" s="T6">слово.[NOM]</ta>
            <ta e="T8" id="Seg_134" s="T7">он(а)-PL.[NOM]</ta>
            <ta e="T9" id="Seg_135" s="T8">быть-PST-3PL</ta>
            <ta e="T10" id="Seg_136" s="T9">много-3PL</ta>
            <ta e="T11" id="Seg_137" s="T10">он(а).[NOM]</ta>
            <ta e="T12" id="Seg_138" s="T11">сказать-3SG.S</ta>
            <ta e="T13" id="Seg_139" s="T12">много</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_140" s="T0">pers</ta>
            <ta e="T2" id="Seg_141" s="T1">n</ta>
            <ta e="T3" id="Seg_142" s="T2">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T4" id="Seg_143" s="T3">n-n:num-n:case</ta>
            <ta e="T5" id="Seg_144" s="T4">v-v&gt;v-v:inf</ta>
            <ta e="T6" id="Seg_145" s="T5">n</ta>
            <ta e="T7" id="Seg_146" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_147" s="T7">pers-n:num-n:case</ta>
            <ta e="T9" id="Seg_148" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_149" s="T9">quant-v:pn</ta>
            <ta e="T11" id="Seg_150" s="T10">pers-n:case</ta>
            <ta e="T12" id="Seg_151" s="T11">v-v:pn</ta>
            <ta e="T13" id="Seg_152" s="T12">quant</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_153" s="T0">pro</ta>
            <ta e="T2" id="Seg_154" s="T1">n</ta>
            <ta e="T3" id="Seg_155" s="T2">v</ta>
            <ta e="T4" id="Seg_156" s="T3">n</ta>
            <ta e="T5" id="Seg_157" s="T4">v</ta>
            <ta e="T6" id="Seg_158" s="T5">n</ta>
            <ta e="T7" id="Seg_159" s="T6">n</ta>
            <ta e="T8" id="Seg_160" s="T7">pers</ta>
            <ta e="T9" id="Seg_161" s="T8">v</ta>
            <ta e="T10" id="Seg_162" s="T9">quant</ta>
            <ta e="T11" id="Seg_163" s="T10">pers</ta>
            <ta e="T12" id="Seg_164" s="T11">v</ta>
            <ta e="T13" id="Seg_165" s="T12">quant</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_166" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_167" s="T3">np.h:S</ta>
            <ta e="T7" id="Seg_168" s="T4">s:purp</ta>
            <ta e="T8" id="Seg_169" s="T7">pro.h:S</ta>
            <ta e="T9" id="Seg_170" s="T8">v:pred</ta>
            <ta e="T11" id="Seg_171" s="T10">pro.h:S</ta>
            <ta e="T12" id="Seg_172" s="T11">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_173" s="T0">np:G</ta>
            <ta e="T2" id="Seg_174" s="T1">np:Time</ta>
            <ta e="T4" id="Seg_175" s="T3">np.h:A</ta>
            <ta e="T7" id="Seg_176" s="T6">np:Th</ta>
            <ta e="T8" id="Seg_177" s="T7">pro.h:Th</ta>
            <ta e="T11" id="Seg_178" s="T10">pro.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_179" s="T0">К нам в прошлом году приходили люди писать селькупские слова.</ta>
            <ta e="T10" id="Seg_180" s="T7">Их было много.</ta>
            <ta e="T13" id="Seg_181" s="T10">Она сказала много.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_182" s="T0">[Last] year people came to us to write down Selkup words.</ta>
            <ta e="T10" id="Seg_183" s="T7">They were many.</ta>
            <ta e="T13" id="Seg_184" s="T10">She said a lot.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_185" s="T0">[Letztes] Jahr kamen Leute zu uns, um selkupische Wörter aufzuschreiben.</ta>
            <ta e="T10" id="Seg_186" s="T7">Sie waren viele.</ta>
            <ta e="T13" id="Seg_187" s="T10">Sie sagte viel.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_188" s="T0">к нам в прошлом году приходили люди писать остяцкие слова</ta>
            <ta e="T10" id="Seg_189" s="T7">их было много</ta>
            <ta e="T13" id="Seg_190" s="T10">она сказала много</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
