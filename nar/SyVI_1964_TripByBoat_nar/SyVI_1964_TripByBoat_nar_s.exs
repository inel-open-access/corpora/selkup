<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>_SyVI_1964_TripByBoat_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">SyVI_1964_TripByBoat_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">35</ud-information>
            <ud-information attribute-name="# HIAT:w">28</ud-information>
            <ud-information attribute-name="# e">28</ud-information>
            <ud-information attribute-name="# HIAT:u">5</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SPK">
            <abbreviation>SyVI</abbreviation>
            <sex value="f" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SPK"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T28" id="Seg_0" n="sc" s="T0">
               <ts e="T8" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">me</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">andun</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">kwässo</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">Kɨndawɨndo</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">nängo</ts>
                  <nts id="Seg_17" n="HIAT:ip">,</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">täbɨm</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">tatku</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">maːttə</ts>
                  <nts id="Seg_27" n="HIAT:ip">.</nts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_30" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">tamdʼe</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">qwanǯat</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">maːttə</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">andun</ts>
                  <nts id="Seg_42" n="HIAT:ip">.</nts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_45" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">qoze</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">kuldʼiŋ</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">qwangu</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">mattə</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">särroŋ</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_63" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">me</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">poːrɣɨla</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">assə</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">iːmbat</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_77" n="HIAT:w" s="T21">maːtqanɨ</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_81" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">kutɨ</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">labum</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">assə</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">tunǯiŋ</ts>
                  <nts id="Seg_93" n="HIAT:ip">,</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_96" n="HIAT:w" s="T26">täp</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_99" n="HIAT:w" s="T27">qandetʼenǯiŋ</ts>
                  <nts id="Seg_100" n="HIAT:ip">.</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T28" id="Seg_102" n="sc" s="T0">
               <ts e="T1" id="Seg_104" n="e" s="T0">me </ts>
               <ts e="T2" id="Seg_106" n="e" s="T1">andun </ts>
               <ts e="T3" id="Seg_108" n="e" s="T2">kwässo </ts>
               <ts e="T4" id="Seg_110" n="e" s="T3">Kɨndawɨndo </ts>
               <ts e="T5" id="Seg_112" n="e" s="T4">nängo, </ts>
               <ts e="T6" id="Seg_114" n="e" s="T5">täbɨm </ts>
               <ts e="T7" id="Seg_116" n="e" s="T6">tatku </ts>
               <ts e="T8" id="Seg_118" n="e" s="T7">maːttə. </ts>
               <ts e="T9" id="Seg_120" n="e" s="T8">tamdʼe </ts>
               <ts e="T10" id="Seg_122" n="e" s="T9">qwanǯat </ts>
               <ts e="T11" id="Seg_124" n="e" s="T10">maːttə </ts>
               <ts e="T12" id="Seg_126" n="e" s="T11">andun. </ts>
               <ts e="T13" id="Seg_128" n="e" s="T12">qoze </ts>
               <ts e="T14" id="Seg_130" n="e" s="T13">kuldʼiŋ </ts>
               <ts e="T15" id="Seg_132" n="e" s="T14">qwangu </ts>
               <ts e="T16" id="Seg_134" n="e" s="T15">mattə </ts>
               <ts e="T17" id="Seg_136" n="e" s="T16">särroŋ. </ts>
               <ts e="T18" id="Seg_138" n="e" s="T17">me </ts>
               <ts e="T19" id="Seg_140" n="e" s="T18">poːrɣɨla </ts>
               <ts e="T20" id="Seg_142" n="e" s="T19">assə </ts>
               <ts e="T21" id="Seg_144" n="e" s="T20">iːmbat </ts>
               <ts e="T22" id="Seg_146" n="e" s="T21">maːtqanɨ. </ts>
               <ts e="T23" id="Seg_148" n="e" s="T22">kutɨ </ts>
               <ts e="T24" id="Seg_150" n="e" s="T23">labum </ts>
               <ts e="T25" id="Seg_152" n="e" s="T24">assə </ts>
               <ts e="T26" id="Seg_154" n="e" s="T25">tunǯiŋ, </ts>
               <ts e="T27" id="Seg_156" n="e" s="T26">täp </ts>
               <ts e="T28" id="Seg_158" n="e" s="T27">qandetʼenǯiŋ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T8" id="Seg_159" s="T0">SyVI_1964_TripByBoat_nar.001 (001.001)</ta>
            <ta e="T12" id="Seg_160" s="T8">SyVI_1964_TripByBoat_nar.002 (001.002)</ta>
            <ta e="T17" id="Seg_161" s="T12">SyVI_1964_TripByBoat_nar.003 (001.003)</ta>
            <ta e="T22" id="Seg_162" s="T17">SyVI_1964_TripByBoat_nar.004 (001.004)</ta>
            <ta e="T28" id="Seg_163" s="T22">SyVI_1964_TripByBoat_nar.005 (001.005)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T8" id="Seg_164" s="T0">ме ан′дун квӓ′ссо кын′давындо нӓнго тӓ′бым тат′ку ма̄ттъ.</ta>
            <ta e="T12" id="Seg_165" s="T8">там′дʼе kwан′джат ма̄ттъ андун.</ta>
            <ta e="T17" id="Seg_166" s="T12">kо′зе куl′дʼиң kwан′гу ′маттъ сӓ′рроң.</ta>
            <ta e="T22" id="Seg_167" s="T17">ме по̄рɣы′ла ′ассъ ′ӣмбат ′ма̄тkаны.</ta>
            <ta e="T28" id="Seg_168" s="T22">ку′ты ла′бум ассъ тун′джиң, тӓп kанде′тʼенджиң.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T8" id="Seg_169" s="T0">me andun kvässo kɨndavɨndo nängo täbɨm tatku maːttə.</ta>
            <ta e="T12" id="Seg_170" s="T8">tamdʼe qwanǯat maːttə andun.</ta>
            <ta e="T17" id="Seg_171" s="T12">qoze kuldʼiŋ qwangu mattə särroŋ.</ta>
            <ta e="T22" id="Seg_172" s="T17">me poːrɣɨla assə iːmbat maːtqanɨ.</ta>
            <ta e="T28" id="Seg_173" s="T22">kutɨ labum assə tunǯiŋ, täp qandetʼenǯiŋ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T8" id="Seg_174" s="T0">me andun kwässo Kɨndawɨndo nängo, täbɨm tatku maːttə. </ta>
            <ta e="T12" id="Seg_175" s="T8">tamdʼe qwanǯat maːttə andun. </ta>
            <ta e="T17" id="Seg_176" s="T12">qoze kuldʼiŋ qwangu mattə särroŋ. </ta>
            <ta e="T22" id="Seg_177" s="T17">me poːrɣɨla assə iːmbat maːtqanɨ. </ta>
            <ta e="T28" id="Seg_178" s="T22">kutɨ labum assə tunǯiŋ, täp qandetʼenǯiŋ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_179" s="T0">me</ta>
            <ta e="T2" id="Seg_180" s="T1">andu-n</ta>
            <ta e="T3" id="Seg_181" s="T2">kwäs-s-o</ta>
            <ta e="T4" id="Seg_182" s="T3">Kɨndawɨ-ndo</ta>
            <ta e="T5" id="Seg_183" s="T4">nä-ngo</ta>
            <ta e="T6" id="Seg_184" s="T5">täb-ɨ-m</ta>
            <ta e="T7" id="Seg_185" s="T6">tat-ku</ta>
            <ta e="T8" id="Seg_186" s="T7">maːt-tə</ta>
            <ta e="T9" id="Seg_187" s="T8">tamdʼe</ta>
            <ta e="T10" id="Seg_188" s="T9">qwa-nǯ-at</ta>
            <ta e="T11" id="Seg_189" s="T10">maːt-tə</ta>
            <ta e="T12" id="Seg_190" s="T11">andu-n</ta>
            <ta e="T13" id="Seg_191" s="T12">qoze</ta>
            <ta e="T14" id="Seg_192" s="T13">kuldʼi-ŋ</ta>
            <ta e="T15" id="Seg_193" s="T14">qwan-gu</ta>
            <ta e="T16" id="Seg_194" s="T15">mat-tə</ta>
            <ta e="T17" id="Seg_195" s="T16">särro-ŋ</ta>
            <ta e="T18" id="Seg_196" s="T17">me</ta>
            <ta e="T19" id="Seg_197" s="T18">poːrɣɨ-la</ta>
            <ta e="T20" id="Seg_198" s="T19">assə</ta>
            <ta e="T21" id="Seg_199" s="T20">iː-mb-at</ta>
            <ta e="T22" id="Seg_200" s="T21">maːt-qanɨ</ta>
            <ta e="T23" id="Seg_201" s="T22">kutɨ</ta>
            <ta e="T24" id="Seg_202" s="T23">labu-m</ta>
            <ta e="T25" id="Seg_203" s="T24">assə</ta>
            <ta e="T26" id="Seg_204" s="T25">tu-nǯi-ŋ</ta>
            <ta e="T27" id="Seg_205" s="T26">täp</ta>
            <ta e="T28" id="Seg_206" s="T27">qande-tʼe-nǯi-ŋ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_207" s="T0">meː</ta>
            <ta e="T2" id="Seg_208" s="T1">andu-n</ta>
            <ta e="T3" id="Seg_209" s="T2">qwən-sɨ-ut</ta>
            <ta e="T4" id="Seg_210" s="T3">Kɨndabu-ndɨ</ta>
            <ta e="T5" id="Seg_211" s="T4">neː-nqo</ta>
            <ta e="T6" id="Seg_212" s="T5">tap-ɨ-m</ta>
            <ta e="T7" id="Seg_213" s="T6">tadɨ-gu</ta>
            <ta e="T8" id="Seg_214" s="T7">maːt-ndɨ</ta>
            <ta e="T9" id="Seg_215" s="T8">tawtʼeːlɨ</ta>
            <ta e="T10" id="Seg_216" s="T9">qwən-nǯɨ-ut</ta>
            <ta e="T11" id="Seg_217" s="T10">maːt-ndɨ</ta>
            <ta e="T12" id="Seg_218" s="T11">andu-n</ta>
            <ta e="T13" id="Seg_219" s="T12">qoze</ta>
            <ta e="T14" id="Seg_220" s="T13">kulʼdi-k</ta>
            <ta e="T15" id="Seg_221" s="T14">qwən-gu</ta>
            <ta e="T16" id="Seg_222" s="T15">maːt-ndɨ</ta>
            <ta e="T17" id="Seg_223" s="T16">särro-n</ta>
            <ta e="T18" id="Seg_224" s="T17">meː</ta>
            <ta e="T19" id="Seg_225" s="T18">porqɨ-la</ta>
            <ta e="T20" id="Seg_226" s="T19">assɨ</ta>
            <ta e="T21" id="Seg_227" s="T20">iː-mbɨ-ut</ta>
            <ta e="T22" id="Seg_228" s="T21">maːt-qɨnnɨ</ta>
            <ta e="T23" id="Seg_229" s="T22">kutɨ</ta>
            <ta e="T24" id="Seg_230" s="T23">lappu-m</ta>
            <ta e="T25" id="Seg_231" s="T24">assɨ</ta>
            <ta e="T26" id="Seg_232" s="T25">tuː-nǯɨ-n</ta>
            <ta e="T27" id="Seg_233" s="T26">tap</ta>
            <ta e="T28" id="Seg_234" s="T27">qantǝ-ntɨ-nǯɨ-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_235" s="T0">we.NOM</ta>
            <ta e="T2" id="Seg_236" s="T1">boat-INSTR2</ta>
            <ta e="T3" id="Seg_237" s="T2">go.away-PST-1PL</ta>
            <ta e="T4" id="Seg_238" s="T3">Ust_Ozyornoe-ILL</ta>
            <ta e="T5" id="Seg_239" s="T4">daughter-TRL</ta>
            <ta e="T6" id="Seg_240" s="T5">(s)he-EP-ACC</ta>
            <ta e="T7" id="Seg_241" s="T6">bring-INF</ta>
            <ta e="T8" id="Seg_242" s="T7">house-ILL</ta>
            <ta e="T9" id="Seg_243" s="T8">this.day.[NOM]</ta>
            <ta e="T10" id="Seg_244" s="T9">go.away-FUT-1PL</ta>
            <ta e="T11" id="Seg_245" s="T10">house-ILL</ta>
            <ta e="T12" id="Seg_246" s="T11">boat-INSTR2</ta>
            <ta e="T13" id="Seg_247" s="T12">I.dont.know</ta>
            <ta e="T14" id="Seg_248" s="T13">which-ADVZ</ta>
            <ta e="T15" id="Seg_249" s="T14">go.away-INF</ta>
            <ta e="T16" id="Seg_250" s="T15">house-ILL</ta>
            <ta e="T17" id="Seg_251" s="T16">rain-3SG.S</ta>
            <ta e="T18" id="Seg_252" s="T17">we.NOM</ta>
            <ta e="T19" id="Seg_253" s="T18">clothing-PL.[NOM]</ta>
            <ta e="T20" id="Seg_254" s="T19">NEG</ta>
            <ta e="T21" id="Seg_255" s="T20">take-DUR-1PL</ta>
            <ta e="T22" id="Seg_256" s="T21">house-EL</ta>
            <ta e="T23" id="Seg_257" s="T22">who.[NOM]</ta>
            <ta e="T24" id="Seg_258" s="T23">oar-ACC</ta>
            <ta e="T25" id="Seg_259" s="T24">NEG</ta>
            <ta e="T26" id="Seg_260" s="T25">row-FUT-3SG.S</ta>
            <ta e="T27" id="Seg_261" s="T26">(s)he.[NOM]</ta>
            <ta e="T28" id="Seg_262" s="T27">freeze-IPFV-FUT-3SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_263" s="T0">мы.NOM</ta>
            <ta e="T2" id="Seg_264" s="T1">лодка-INSTR2</ta>
            <ta e="T3" id="Seg_265" s="T2">уйти-PST-1PL</ta>
            <ta e="T4" id="Seg_266" s="T3">Усть_Озёрное-ILL</ta>
            <ta e="T5" id="Seg_267" s="T4">дочь-TRL</ta>
            <ta e="T6" id="Seg_268" s="T5">он(а)-EP-ACC</ta>
            <ta e="T7" id="Seg_269" s="T6">принести-INF</ta>
            <ta e="T8" id="Seg_270" s="T7">дом-ILL</ta>
            <ta e="T9" id="Seg_271" s="T8">этот.день.[NOM]</ta>
            <ta e="T10" id="Seg_272" s="T9">уйти-FUT-1PL</ta>
            <ta e="T11" id="Seg_273" s="T10">дом-ILL</ta>
            <ta e="T12" id="Seg_274" s="T11">лодка-INSTR2</ta>
            <ta e="T13" id="Seg_275" s="T12">не.знаю</ta>
            <ta e="T14" id="Seg_276" s="T13">какой-ADVZ</ta>
            <ta e="T15" id="Seg_277" s="T14">уйти-INF</ta>
            <ta e="T16" id="Seg_278" s="T15">дом-ILL</ta>
            <ta e="T17" id="Seg_279" s="T16">дождь-3SG.S</ta>
            <ta e="T18" id="Seg_280" s="T17">мы.NOM</ta>
            <ta e="T19" id="Seg_281" s="T18">одежда-PL.[NOM]</ta>
            <ta e="T20" id="Seg_282" s="T19">NEG</ta>
            <ta e="T21" id="Seg_283" s="T20">взять-DUR-1PL</ta>
            <ta e="T22" id="Seg_284" s="T21">дом-EL</ta>
            <ta e="T23" id="Seg_285" s="T22">кто.[NOM]</ta>
            <ta e="T24" id="Seg_286" s="T23">весло-ACC</ta>
            <ta e="T25" id="Seg_287" s="T24">NEG</ta>
            <ta e="T26" id="Seg_288" s="T25">грести-FUT-3SG.S</ta>
            <ta e="T27" id="Seg_289" s="T26">он(а).[NOM]</ta>
            <ta e="T28" id="Seg_290" s="T27">замерзнуть-IPFV-FUT-3SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_291" s="T0">pers</ta>
            <ta e="T2" id="Seg_292" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_293" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_294" s="T3">nprop-n:case</ta>
            <ta e="T5" id="Seg_295" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_296" s="T5">pers-n:ins-n:case</ta>
            <ta e="T7" id="Seg_297" s="T6">v-v:inf</ta>
            <ta e="T8" id="Seg_298" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_299" s="T8">dem-n-n:case</ta>
            <ta e="T10" id="Seg_300" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_301" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_302" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_303" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_304" s="T13">interrog-adj&gt;adv</ta>
            <ta e="T15" id="Seg_305" s="T14">v-v:inf</ta>
            <ta e="T16" id="Seg_306" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_307" s="T16">n-v:pn</ta>
            <ta e="T18" id="Seg_308" s="T17">pers</ta>
            <ta e="T19" id="Seg_309" s="T18">n-n:num-n:case</ta>
            <ta e="T20" id="Seg_310" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_311" s="T20">v-v&gt;v-v:pn</ta>
            <ta e="T22" id="Seg_312" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_313" s="T22">interrog-n:case</ta>
            <ta e="T24" id="Seg_314" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_315" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_316" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_317" s="T26">pers-n:case</ta>
            <ta e="T28" id="Seg_318" s="T27">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_319" s="T0">pers</ta>
            <ta e="T2" id="Seg_320" s="T1">n</ta>
            <ta e="T3" id="Seg_321" s="T2">n</ta>
            <ta e="T4" id="Seg_322" s="T3">nprop</ta>
            <ta e="T5" id="Seg_323" s="T4">n</ta>
            <ta e="T6" id="Seg_324" s="T5">pers</ta>
            <ta e="T7" id="Seg_325" s="T6">v</ta>
            <ta e="T8" id="Seg_326" s="T7">n</ta>
            <ta e="T9" id="Seg_327" s="T8">n</ta>
            <ta e="T10" id="Seg_328" s="T9">v</ta>
            <ta e="T11" id="Seg_329" s="T10">n</ta>
            <ta e="T12" id="Seg_330" s="T11">n</ta>
            <ta e="T13" id="Seg_331" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_332" s="T13">adv</ta>
            <ta e="T15" id="Seg_333" s="T14">v</ta>
            <ta e="T16" id="Seg_334" s="T15">n</ta>
            <ta e="T17" id="Seg_335" s="T16">n</ta>
            <ta e="T18" id="Seg_336" s="T17">pers</ta>
            <ta e="T19" id="Seg_337" s="T18">n</ta>
            <ta e="T20" id="Seg_338" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_339" s="T20">v</ta>
            <ta e="T22" id="Seg_340" s="T21">n</ta>
            <ta e="T23" id="Seg_341" s="T22">interrog</ta>
            <ta e="T24" id="Seg_342" s="T23">n</ta>
            <ta e="T25" id="Seg_343" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_344" s="T25">v</ta>
            <ta e="T27" id="Seg_345" s="T26">pers</ta>
            <ta e="T28" id="Seg_346" s="T27">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_347" s="T0">pro.h:A</ta>
            <ta e="T2" id="Seg_348" s="T1">np:Ins</ta>
            <ta e="T4" id="Seg_349" s="T3">np:G</ta>
            <ta e="T6" id="Seg_350" s="T5">pro.h:Th</ta>
            <ta e="T7" id="Seg_351" s="T6">0.1.h:A</ta>
            <ta e="T8" id="Seg_352" s="T7">np:G</ta>
            <ta e="T9" id="Seg_353" s="T8">adv:Time</ta>
            <ta e="T10" id="Seg_354" s="T9">0.1.h:A</ta>
            <ta e="T11" id="Seg_355" s="T10">np:G</ta>
            <ta e="T12" id="Seg_356" s="T11">np:Ins</ta>
            <ta e="T15" id="Seg_357" s="T14">0.1.h:A</ta>
            <ta e="T16" id="Seg_358" s="T15">np:G</ta>
            <ta e="T17" id="Seg_359" s="T16">0.3:Th</ta>
            <ta e="T18" id="Seg_360" s="T17">pro.h:A</ta>
            <ta e="T19" id="Seg_361" s="T18">np:Th</ta>
            <ta e="T22" id="Seg_362" s="T21">np:So</ta>
            <ta e="T23" id="Seg_363" s="T22">pro.h:A</ta>
            <ta e="T24" id="Seg_364" s="T23">np:Th</ta>
            <ta e="T27" id="Seg_365" s="T26">pro.h:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_366" s="T0">pro.h:S</ta>
            <ta e="T3" id="Seg_367" s="T2">v:pred</ta>
            <ta e="T8" id="Seg_368" s="T5">s:purp</ta>
            <ta e="T10" id="Seg_369" s="T9">0.1.h:S v:pred</ta>
            <ta e="T13" id="Seg_370" s="T12">ptcl:pred</ta>
            <ta e="T16" id="Seg_371" s="T13">s:compl</ta>
            <ta e="T17" id="Seg_372" s="T16">0.3:S v:pred</ta>
            <ta e="T18" id="Seg_373" s="T17">pro.h:S</ta>
            <ta e="T19" id="Seg_374" s="T18">np:O</ta>
            <ta e="T21" id="Seg_375" s="T20">v:pred</ta>
            <ta e="T26" id="Seg_376" s="T22">s:rel</ta>
            <ta e="T27" id="Seg_377" s="T26">pro.h:S</ta>
            <ta e="T28" id="Seg_378" s="T27">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T8" id="Seg_379" s="T0">Мы в обласке поехали в Усть-Озёрное за дочерью, ее привезти домой.</ta>
            <ta e="T12" id="Seg_380" s="T8">Сегодня поедем домой на обласке.</ta>
            <ta e="T17" id="Seg_381" s="T12">Не знаю как ехать домой, идет дождь.</ta>
            <ta e="T22" id="Seg_382" s="T17">Мы одежду не взяли из дома.</ta>
            <ta e="T28" id="Seg_383" s="T22">Кто веслом грести не будет, тот замерзнет.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T8" id="Seg_384" s="T0">We went by boat to Ust'-Ozyornoe for our daughter, to bring her home.</ta>
            <ta e="T12" id="Seg_385" s="T8">Today we will go home by boat.</ta>
            <ta e="T17" id="Seg_386" s="T12">I don’t know how to go home, it rains.</ta>
            <ta e="T22" id="Seg_387" s="T17">We haven’t taken clothes from home.</ta>
            <ta e="T28" id="Seg_388" s="T22">Who will not row with an oar, will freeze.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T8" id="Seg_389" s="T0">Wir fuhren mit dem Boot nach Ust'-Ozernoe für unserer Tochter, um sie nach Hause zu bringen.</ta>
            <ta e="T12" id="Seg_390" s="T8">Heute werden wir mit dem Boot nach Hause fahren.</ta>
            <ta e="T17" id="Seg_391" s="T12">Ich weiß nicht, wie [wir] nach Hause fahren, es regnet.</ta>
            <ta e="T22" id="Seg_392" s="T17">Wir haben keine Kleidung von zuhause mitgenommen.</ta>
            <ta e="T28" id="Seg_393" s="T22">Wer nicht nicht mit dem Ruder rudert, wird frieren.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T8" id="Seg_394" s="T0">мы в обласке поехали на остье за дочерью ее привезти домой</ta>
            <ta e="T12" id="Seg_395" s="T8">сегодня поедем домой на обласке</ta>
            <ta e="T17" id="Seg_396" s="T12">не знаю как ехать домой дождь</ta>
            <ta e="T22" id="Seg_397" s="T17">мы одежду не взяли из дома</ta>
            <ta e="T28" id="Seg_398" s="T22">кто веслом грести не будет тот замерзнет</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
