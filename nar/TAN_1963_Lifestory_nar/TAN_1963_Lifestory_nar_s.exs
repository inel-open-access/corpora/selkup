<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>TAN_1963_Lifestory_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">TAN_1963_Lifestory_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">109</ud-information>
            <ud-information attribute-name="# HIAT:w">77</ud-information>
            <ud-information attribute-name="# e">77</ud-information>
            <ud-information attribute-name="# HIAT:u">21</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="TAN">
            <abbreviation>TAN</abbreviation>
            <sex value="f" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="TAN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T78" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">tändɨ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">tʼelɨzaŋ</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">man</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">tändɨ</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">orɨsɨjaŋ</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">poŋursaŋ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">qwälɨzaŋ</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_35" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">sɨrəlam</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">pargɨlsaw</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_44" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">pom</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">tatkuzaw</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_53" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_55" n="HIAT:w" s="T13">menan</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_58" n="HIAT:w" s="T14">ean</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">nar</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">üčeːm</ts>
                  <nts id="Seg_65" n="HIAT:ip">:</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">oqqɨr</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">täbeɣum</ts>
                  <nts id="Seg_72" n="HIAT:ip">,</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">šɨtäq</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">näjɣum</ts>
                  <nts id="Seg_79" n="HIAT:ip">.</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_82" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_84" n="HIAT:w" s="T21">iram</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_87" n="HIAT:w" s="T22">qwändɨmba</ts>
                  <nts id="Seg_88" n="HIAT:ip">,</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_91" n="HIAT:w" s="T23">qüːdɨŋ</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_95" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_97" n="HIAT:w" s="T24">maːdɨka</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_100" n="HIAT:w" s="T25">nʼünʼöka</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_103" n="HIAT:w" s="T26">eɣan</ts>
                  <nts id="Seg_104" n="HIAT:ip">.</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_107" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_109" n="HIAT:w" s="T27">oqqɨr</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_112" n="HIAT:w" s="T28">üče</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_115" n="HIAT:w" s="T29">qumba</ts>
                  <nts id="Seg_116" n="HIAT:ip">.</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_119" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_121" n="HIAT:w" s="T30">man</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_124" n="HIAT:w" s="T31">qunnä</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_127" n="HIAT:w" s="T32">qwännaŋ</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_130" n="HIAT:w" s="T33">qundoqɨn</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_134" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_136" n="HIAT:w" s="T34">man</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_139" n="HIAT:w" s="T35">iraw</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_142" n="HIAT:w" s="T36">soː</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_145" n="HIAT:w" s="T37">qum</ts>
                  <nts id="Seg_146" n="HIAT:ip">:</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_149" n="HIAT:w" s="T38">assä</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_152" n="HIAT:w" s="T39">qwädɨbiquŋ</ts>
                  <nts id="Seg_153" n="HIAT:ip">,</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_156" n="HIAT:w" s="T40">assä</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_159" n="HIAT:w" s="T41">qättälgut</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_163" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_165" n="HIAT:w" s="T42">täp</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_168" n="HIAT:w" s="T43">qwälɨnʼi</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_170" n="HIAT:ip">(</nts>
                  <ts e="T45" id="Seg_172" n="HIAT:w" s="T44">qwälɨnʼja</ts>
                  <nts id="Seg_173" n="HIAT:ip">)</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_176" n="HIAT:w" s="T45">qoːtʼiŋ</ts>
                  <nts id="Seg_177" n="HIAT:ip">,</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_180" n="HIAT:w" s="T46">qwälɨm</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_183" n="HIAT:w" s="T47">qwätkut</ts>
                  <nts id="Seg_184" n="HIAT:ip">.</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_187" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_189" n="HIAT:w" s="T48">man</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_192" n="HIAT:w" s="T49">nʼekčaŋ</ts>
                  <nts id="Seg_193" n="HIAT:ip">,</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_196" n="HIAT:w" s="T50">iraw</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_199" n="HIAT:w" s="T51">nʼekčiŋ</ts>
                  <nts id="Seg_200" n="HIAT:ip">.</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_203" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_205" n="HIAT:w" s="T52">me</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_208" n="HIAT:w" s="T53">üdɨm</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_211" n="HIAT:w" s="T54">ɨrɨkɨo</ts>
                  <nts id="Seg_212" n="HIAT:ip">.</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_215" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_217" n="HIAT:w" s="T55">man</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_220" n="HIAT:w" s="T56">tʼärakuaŋ</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_223" n="HIAT:w" s="T57">täbɨnɨ</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_226" n="HIAT:w" s="T58">iːgu</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_229" n="HIAT:w" s="T59">čorgɨm</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_232" n="HIAT:w" s="T60">ütku</ts>
                  <nts id="Seg_233" n="HIAT:ip">.</nts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_236" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_238" n="HIAT:w" s="T61">täp</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_241" n="HIAT:w" s="T62">qätnɨt</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_244" n="HIAT:w" s="T63">meːŋa</ts>
                  <nts id="Seg_245" n="HIAT:ip">:</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_248" n="HIAT:w" s="T64">qödik</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_251" n="HIAT:w" s="T65">iːdä</ts>
                  <nts id="Seg_252" n="HIAT:ip">.</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_255" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_257" n="HIAT:w" s="T66">man</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_260" n="HIAT:w" s="T67">qwäsaŋ</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_263" n="HIAT:w" s="T68">iːsaw</ts>
                  <nts id="Seg_264" n="HIAT:ip">.</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_267" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_269" n="HIAT:w" s="T69">qojmɨčeso</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_272" n="HIAT:w" s="T70">täpsä</ts>
                  <nts id="Seg_273" n="HIAT:ip">,</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_276" n="HIAT:w" s="T71">paqtɨmbɨso</ts>
                  <nts id="Seg_277" n="HIAT:ip">.</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_280" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_282" n="HIAT:w" s="T72">nännɨ</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_285" n="HIAT:w" s="T73">kučanno</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_288" n="HIAT:w" s="T74">qondɨsoː</ts>
                  <nts id="Seg_289" n="HIAT:ip">.</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_292" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_294" n="HIAT:w" s="T75">wɨssaŋ</ts>
                  <nts id="Seg_295" n="HIAT:ip">.</nts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_298" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_300" n="HIAT:w" s="T76">olow</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_303" n="HIAT:w" s="T77">kaškɨmba</ts>
                  <nts id="Seg_304" n="HIAT:ip">.</nts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T78" id="Seg_306" n="sc" s="T1">
               <ts e="T2" id="Seg_308" n="e" s="T1">man </ts>
               <ts e="T3" id="Seg_310" n="e" s="T2">tändɨ </ts>
               <ts e="T4" id="Seg_312" n="e" s="T3">tʼelɨzaŋ. </ts>
               <ts e="T5" id="Seg_314" n="e" s="T4">man </ts>
               <ts e="T6" id="Seg_316" n="e" s="T5">tändɨ </ts>
               <ts e="T7" id="Seg_318" n="e" s="T6">orɨsɨjaŋ. </ts>
               <ts e="T8" id="Seg_320" n="e" s="T7">poŋursaŋ </ts>
               <ts e="T9" id="Seg_322" n="e" s="T8">qwälɨzaŋ. </ts>
               <ts e="T10" id="Seg_324" n="e" s="T9">sɨrəlam </ts>
               <ts e="T11" id="Seg_326" n="e" s="T10">pargɨlsaw. </ts>
               <ts e="T12" id="Seg_328" n="e" s="T11">pom </ts>
               <ts e="T13" id="Seg_330" n="e" s="T12">tatkuzaw. </ts>
               <ts e="T14" id="Seg_332" n="e" s="T13">menan </ts>
               <ts e="T15" id="Seg_334" n="e" s="T14">ean </ts>
               <ts e="T16" id="Seg_336" n="e" s="T15">nar </ts>
               <ts e="T17" id="Seg_338" n="e" s="T16">üčeːm: </ts>
               <ts e="T18" id="Seg_340" n="e" s="T17">oqqɨr </ts>
               <ts e="T19" id="Seg_342" n="e" s="T18">täbeɣum, </ts>
               <ts e="T20" id="Seg_344" n="e" s="T19">šɨtäq </ts>
               <ts e="T21" id="Seg_346" n="e" s="T20">näjɣum. </ts>
               <ts e="T22" id="Seg_348" n="e" s="T21">iram </ts>
               <ts e="T23" id="Seg_350" n="e" s="T22">qwändɨmba, </ts>
               <ts e="T24" id="Seg_352" n="e" s="T23">qüːdɨŋ. </ts>
               <ts e="T25" id="Seg_354" n="e" s="T24">maːdɨka </ts>
               <ts e="T26" id="Seg_356" n="e" s="T25">nʼünʼöka </ts>
               <ts e="T27" id="Seg_358" n="e" s="T26">eɣan. </ts>
               <ts e="T28" id="Seg_360" n="e" s="T27">oqqɨr </ts>
               <ts e="T29" id="Seg_362" n="e" s="T28">üče </ts>
               <ts e="T30" id="Seg_364" n="e" s="T29">qumba. </ts>
               <ts e="T31" id="Seg_366" n="e" s="T30">man </ts>
               <ts e="T32" id="Seg_368" n="e" s="T31">qunnä </ts>
               <ts e="T33" id="Seg_370" n="e" s="T32">qwännaŋ </ts>
               <ts e="T34" id="Seg_372" n="e" s="T33">qundoqɨn. </ts>
               <ts e="T35" id="Seg_374" n="e" s="T34">man </ts>
               <ts e="T36" id="Seg_376" n="e" s="T35">iraw </ts>
               <ts e="T37" id="Seg_378" n="e" s="T36">soː </ts>
               <ts e="T38" id="Seg_380" n="e" s="T37">qum: </ts>
               <ts e="T39" id="Seg_382" n="e" s="T38">assä </ts>
               <ts e="T40" id="Seg_384" n="e" s="T39">qwädɨbiquŋ, </ts>
               <ts e="T41" id="Seg_386" n="e" s="T40">assä </ts>
               <ts e="T42" id="Seg_388" n="e" s="T41">qättälgut. </ts>
               <ts e="T43" id="Seg_390" n="e" s="T42">täp </ts>
               <ts e="T44" id="Seg_392" n="e" s="T43">qwälɨnʼi </ts>
               <ts e="T45" id="Seg_394" n="e" s="T44">(qwälɨnʼja) </ts>
               <ts e="T46" id="Seg_396" n="e" s="T45">qoːtʼiŋ, </ts>
               <ts e="T47" id="Seg_398" n="e" s="T46">qwälɨm </ts>
               <ts e="T48" id="Seg_400" n="e" s="T47">qwätkut. </ts>
               <ts e="T49" id="Seg_402" n="e" s="T48">man </ts>
               <ts e="T50" id="Seg_404" n="e" s="T49">nʼekčaŋ, </ts>
               <ts e="T51" id="Seg_406" n="e" s="T50">iraw </ts>
               <ts e="T52" id="Seg_408" n="e" s="T51">nʼekčiŋ. </ts>
               <ts e="T53" id="Seg_410" n="e" s="T52">me </ts>
               <ts e="T54" id="Seg_412" n="e" s="T53">üdɨm </ts>
               <ts e="T55" id="Seg_414" n="e" s="T54">ɨrɨkɨo. </ts>
               <ts e="T56" id="Seg_416" n="e" s="T55">man </ts>
               <ts e="T57" id="Seg_418" n="e" s="T56">tʼärakuaŋ </ts>
               <ts e="T58" id="Seg_420" n="e" s="T57">täbɨnɨ </ts>
               <ts e="T59" id="Seg_422" n="e" s="T58">iːgu </ts>
               <ts e="T60" id="Seg_424" n="e" s="T59">čorgɨm </ts>
               <ts e="T61" id="Seg_426" n="e" s="T60">ütku. </ts>
               <ts e="T62" id="Seg_428" n="e" s="T61">täp </ts>
               <ts e="T63" id="Seg_430" n="e" s="T62">qätnɨt </ts>
               <ts e="T64" id="Seg_432" n="e" s="T63">meːŋa: </ts>
               <ts e="T65" id="Seg_434" n="e" s="T64">qödik </ts>
               <ts e="T66" id="Seg_436" n="e" s="T65">iːdä. </ts>
               <ts e="T67" id="Seg_438" n="e" s="T66">man </ts>
               <ts e="T68" id="Seg_440" n="e" s="T67">qwäsaŋ </ts>
               <ts e="T69" id="Seg_442" n="e" s="T68">iːsaw. </ts>
               <ts e="T70" id="Seg_444" n="e" s="T69">qojmɨčeso </ts>
               <ts e="T71" id="Seg_446" n="e" s="T70">täpsä, </ts>
               <ts e="T72" id="Seg_448" n="e" s="T71">paqtɨmbɨso. </ts>
               <ts e="T73" id="Seg_450" n="e" s="T72">nännɨ </ts>
               <ts e="T74" id="Seg_452" n="e" s="T73">kučanno </ts>
               <ts e="T75" id="Seg_454" n="e" s="T74">qondɨsoː. </ts>
               <ts e="T76" id="Seg_456" n="e" s="T75">wɨssaŋ. </ts>
               <ts e="T77" id="Seg_458" n="e" s="T76">olow </ts>
               <ts e="T78" id="Seg_460" n="e" s="T77">kaškɨmba. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_461" s="T1">TAN_1963_Lifestory_nar.001 (001.001)</ta>
            <ta e="T7" id="Seg_462" s="T4">TAN_1963_Lifestory_nar.002 (001.002)</ta>
            <ta e="T9" id="Seg_463" s="T7">TAN_1963_Lifestory_nar.003 (001.003)</ta>
            <ta e="T11" id="Seg_464" s="T9">TAN_1963_Lifestory_nar.004 (001.004)</ta>
            <ta e="T13" id="Seg_465" s="T11">TAN_1963_Lifestory_nar.005 (001.005)</ta>
            <ta e="T21" id="Seg_466" s="T13">TAN_1963_Lifestory_nar.006 (001.006)</ta>
            <ta e="T24" id="Seg_467" s="T21">TAN_1963_Lifestory_nar.007 (001.007)</ta>
            <ta e="T27" id="Seg_468" s="T24">TAN_1963_Lifestory_nar.008 (001.008)</ta>
            <ta e="T30" id="Seg_469" s="T27">TAN_1963_Lifestory_nar.009 (001.009)</ta>
            <ta e="T34" id="Seg_470" s="T30">TAN_1963_Lifestory_nar.010 (001.010)</ta>
            <ta e="T42" id="Seg_471" s="T34">TAN_1963_Lifestory_nar.011 (001.011)</ta>
            <ta e="T48" id="Seg_472" s="T42">TAN_1963_Lifestory_nar.012 (001.012)</ta>
            <ta e="T52" id="Seg_473" s="T48">TAN_1963_Lifestory_nar.013 (001.013)</ta>
            <ta e="T55" id="Seg_474" s="T52">TAN_1963_Lifestory_nar.014 (001.014)</ta>
            <ta e="T61" id="Seg_475" s="T55">TAN_1963_Lifestory_nar.015 (001.015)</ta>
            <ta e="T66" id="Seg_476" s="T61">TAN_1963_Lifestory_nar.016 (001.016)</ta>
            <ta e="T69" id="Seg_477" s="T66">TAN_1963_Lifestory_nar.017 (001.017)</ta>
            <ta e="T72" id="Seg_478" s="T69">TAN_1963_Lifestory_nar.018 (001.018)</ta>
            <ta e="T75" id="Seg_479" s="T72">TAN_1963_Lifestory_nar.019 (001.019)</ta>
            <ta e="T76" id="Seg_480" s="T75">TAN_1963_Lifestory_nar.020 (001.020)</ta>
            <ta e="T78" id="Seg_481" s="T76">TAN_1963_Lifestory_nar.021 (001.021)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_482" s="T1">ман тӓн′ды ′тʼелы‵заң.</ta>
            <ta e="T7" id="Seg_483" s="T4">ман тӓн′ды орысы′jаң.</ta>
            <ta e="T9" id="Seg_484" s="T7">′поңурсаң ′kwӓлызаң.</ta>
            <ta e="T11" id="Seg_485" s="T9">′сырълам паргыл′саw.</ta>
            <ta e="T13" id="Seg_486" s="T11">′пом татку′заw.</ta>
            <ta e="T21" id="Seg_487" s="T13">менан ′еан нар ӱ′че̄м: ′оkkыр ′тӓ(ы)беɣум, шы′тӓk ′нӓйɣум.</ta>
            <ta e="T24" id="Seg_488" s="T21">и′рам ′kwӓндымба, ′kӱ̄дың.</ta>
            <ta e="T27" id="Seg_489" s="T24">′ма̄дыка ′нʼӱнʼӧка ′е(ɣ)ан.</ta>
            <ta e="T30" id="Seg_490" s="T27">′оkkыр ӱ′че ′kумба.</ta>
            <ta e="T34" id="Seg_491" s="T30">ман ′kуннӓ kwӓ′ннаң kун′доkын.</ta>
            <ta e="T42" id="Seg_492" s="T34">ман и′раw со̄ ′kум: ′ассӓ ′kwӓдыбиkуң, ′ассӓ kӓ′ттӓлгут.</ta>
            <ta e="T48" id="Seg_493" s="T42">тӓп ′kwӓлынʼи (′kwӓлынʼjа) kо̄′тʼиң, ′kwӓлым kwӓт′кут.</ta>
            <ta e="T52" id="Seg_494" s="T48">ман нʼек′тшаң, и′раw нʼек′тшиң.</ta>
            <ta e="T55" id="Seg_495" s="T52">ме ′ӱдым ′ырыкы(w)о.</ta>
            <ta e="T61" id="Seg_496" s="T55">ман тʼӓраку′аң тӓбы′ны ′ӣгу ′тшоргым ӱт′ку.</ta>
            <ta e="T66" id="Seg_497" s="T61">тӓп kӓт′ныт ′ме̄ңа: ′kӧдик ′ӣдӓ.</ta>
            <ta e="T69" id="Seg_498" s="T66">ман kwӓ′саң ′ӣсаw.</ta>
            <ta e="T72" id="Seg_499" s="T69">′kоймычесо тӓп′сӓ, паkтымбысо.</ta>
            <ta e="T75" id="Seg_500" s="T72">′нӓнны(ъ) ку′тшанно ′kондысо̄.</ta>
            <ta e="T76" id="Seg_501" s="T75">вы′ссаң.</ta>
            <ta e="T78" id="Seg_502" s="T76">о′лоw(м) ′кашкымба.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_503" s="T1">man tändɨ tʼelɨzaŋ.</ta>
            <ta e="T7" id="Seg_504" s="T4">man tändɨ orɨsɨjaŋ.</ta>
            <ta e="T9" id="Seg_505" s="T7">poŋursaŋ qwälɨzaŋ.</ta>
            <ta e="T11" id="Seg_506" s="T9">sɨrəlam pargɨlsaw.</ta>
            <ta e="T13" id="Seg_507" s="T11">pom tatkuzaw.</ta>
            <ta e="T21" id="Seg_508" s="T13">menan ean nar üčeːm: oqqɨr tä(ɨ)beɣum, šɨtäq näjɣum.</ta>
            <ta e="T24" id="Seg_509" s="T21">iram qwändɨmba, qüːdɨŋ.</ta>
            <ta e="T27" id="Seg_510" s="T24">maːdɨka nʼünʼöka e(ɣ)an.</ta>
            <ta e="T30" id="Seg_511" s="T27">oqqɨr üče qumba.</ta>
            <ta e="T34" id="Seg_512" s="T30">man qunnä qwännaŋ qundoqɨn.</ta>
            <ta e="T42" id="Seg_513" s="T34">man iraw soː qum: assä qwädɨbiquŋ, assä qättälgut.</ta>
            <ta e="T48" id="Seg_514" s="T42">täp qwälɨnʼi (qwälɨnʼja) qoːtʼiŋ, qwälɨm qwätkut.</ta>
            <ta e="T52" id="Seg_515" s="T48">man nʼektšaŋ, iraw nʼektšiŋ.</ta>
            <ta e="T55" id="Seg_516" s="T52">me üdɨm ɨrɨkɨ(w)o.</ta>
            <ta e="T61" id="Seg_517" s="T55">man tʼärakuaŋ täbɨnɨ iːgu tšorgɨm ütku.</ta>
            <ta e="T66" id="Seg_518" s="T61">täp qätnɨt meːŋa: qödik iːdä.</ta>
            <ta e="T69" id="Seg_519" s="T66">man qwäsaŋ iːsaw.</ta>
            <ta e="T72" id="Seg_520" s="T69">qojmɨčeso täpsä, paqtɨmbɨso.</ta>
            <ta e="T75" id="Seg_521" s="T72">nännɨ(ə) kutšanno qondɨsoː.</ta>
            <ta e="T76" id="Seg_522" s="T75">vɨssaŋ.</ta>
            <ta e="T78" id="Seg_523" s="T76">olow(m) kaškɨmba.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_524" s="T1">man tändɨ tʼelɨzaŋ. </ta>
            <ta e="T7" id="Seg_525" s="T4">man tändɨ orɨsɨjaŋ. </ta>
            <ta e="T9" id="Seg_526" s="T7">poŋursaŋ qwälɨzaŋ. </ta>
            <ta e="T11" id="Seg_527" s="T9">sɨrəlam pargɨlsaw. </ta>
            <ta e="T13" id="Seg_528" s="T11">pom tatkuzaw. </ta>
            <ta e="T21" id="Seg_529" s="T13">menan ean nar üčeːm: oqqɨr täbeɣum, šɨtäq näjɣum. </ta>
            <ta e="T24" id="Seg_530" s="T21">iram qwändɨmba, qüːdɨŋ. </ta>
            <ta e="T27" id="Seg_531" s="T24">maːdɨka nʼünʼöka eɣan. </ta>
            <ta e="T30" id="Seg_532" s="T27">oqqɨr üče qumba. </ta>
            <ta e="T34" id="Seg_533" s="T30">man qunnä qwännaŋ qundoqɨn. </ta>
            <ta e="T42" id="Seg_534" s="T34">man iraw soː qum: assä qwädɨbiquŋ, assä qättälgut. </ta>
            <ta e="T48" id="Seg_535" s="T42">täp qwälɨnʼi (qwälɨnʼja) qoːtʼiŋ, qwälɨm qwätkut. </ta>
            <ta e="T52" id="Seg_536" s="T48">man nʼekčaŋ, iraw nʼekčiŋ. </ta>
            <ta e="T55" id="Seg_537" s="T52">me üdɨm ɨrɨkɨo. </ta>
            <ta e="T61" id="Seg_538" s="T55">man tʼärakuaŋ täbɨnɨ iːgu čorgɨm ütku. </ta>
            <ta e="T66" id="Seg_539" s="T61">täp qätnɨt meːŋa: qödik iːdä. </ta>
            <ta e="T69" id="Seg_540" s="T66">man qwäsaŋ iːsaw. </ta>
            <ta e="T72" id="Seg_541" s="T69">qojmɨčeso täpsä, paqtɨmbɨso. </ta>
            <ta e="T75" id="Seg_542" s="T72">nännɨ kučanno qondɨsoː. </ta>
            <ta e="T76" id="Seg_543" s="T75">wɨssaŋ. </ta>
            <ta e="T78" id="Seg_544" s="T76">olow kaškɨmba. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_545" s="T1">man</ta>
            <ta e="T3" id="Seg_546" s="T2">tändɨ</ta>
            <ta e="T4" id="Seg_547" s="T3">tʼelɨ-za-ŋ</ta>
            <ta e="T5" id="Seg_548" s="T4">man</ta>
            <ta e="T6" id="Seg_549" s="T5">tändɨ</ta>
            <ta e="T7" id="Seg_550" s="T6">or-ɨ-sɨ-ja-ŋ</ta>
            <ta e="T8" id="Seg_551" s="T7">poŋur-sa-ŋ</ta>
            <ta e="T9" id="Seg_552" s="T8">qwälɨ-za-ŋ</ta>
            <ta e="T10" id="Seg_553" s="T9">sɨr-ə-la-m</ta>
            <ta e="T11" id="Seg_554" s="T10">pargɨl-sa-w</ta>
            <ta e="T12" id="Seg_555" s="T11">po-m</ta>
            <ta e="T13" id="Seg_556" s="T12">tat-ku-za-w</ta>
            <ta e="T14" id="Seg_557" s="T13">me-nan</ta>
            <ta e="T15" id="Seg_558" s="T14">e-a-ŋ</ta>
            <ta e="T16" id="Seg_559" s="T15">nar</ta>
            <ta e="T17" id="Seg_560" s="T16">üčče-m</ta>
            <ta e="T18" id="Seg_561" s="T17">oqqɨr</ta>
            <ta e="T19" id="Seg_562" s="T18">täbe-ɣum</ta>
            <ta e="T20" id="Seg_563" s="T19">šɨtä-q</ta>
            <ta e="T21" id="Seg_564" s="T20">näj-ɣum</ta>
            <ta e="T22" id="Seg_565" s="T21">ira-m</ta>
            <ta e="T23" id="Seg_566" s="T22">qwänd-ɨ-mba</ta>
            <ta e="T24" id="Seg_567" s="T23">qüːdɨ-ŋ</ta>
            <ta e="T25" id="Seg_568" s="T24">maːd-ɨ-ka</ta>
            <ta e="T26" id="Seg_569" s="T25">nʼünʼö-ka</ta>
            <ta e="T27" id="Seg_570" s="T26">e-ɣa-n</ta>
            <ta e="T28" id="Seg_571" s="T27">oqqɨr</ta>
            <ta e="T29" id="Seg_572" s="T28">üče</ta>
            <ta e="T30" id="Seg_573" s="T29">qu-mba</ta>
            <ta e="T31" id="Seg_574" s="T30">man</ta>
            <ta e="T32" id="Seg_575" s="T31">qun-nä</ta>
            <ta e="T33" id="Seg_576" s="T32">qwän-na-ŋ</ta>
            <ta e="T34" id="Seg_577" s="T33">qundo-qɨn</ta>
            <ta e="T35" id="Seg_578" s="T34">man</ta>
            <ta e="T36" id="Seg_579" s="T35">ira-w</ta>
            <ta e="T37" id="Seg_580" s="T36">soː</ta>
            <ta e="T38" id="Seg_581" s="T37">qum</ta>
            <ta e="T39" id="Seg_582" s="T38">assä</ta>
            <ta e="T40" id="Seg_583" s="T39">qwädɨ-bi-qu-ŋ</ta>
            <ta e="T41" id="Seg_584" s="T40">assä</ta>
            <ta e="T42" id="Seg_585" s="T41">qättä-l-gu-t</ta>
            <ta e="T43" id="Seg_586" s="T42">täp</ta>
            <ta e="T44" id="Seg_587" s="T43">qwälɨ-nʼi</ta>
            <ta e="T45" id="Seg_588" s="T44">qwälɨ-nʼja</ta>
            <ta e="T46" id="Seg_589" s="T45">qoːtʼiŋ</ta>
            <ta e="T47" id="Seg_590" s="T46">qwälɨ-m</ta>
            <ta e="T48" id="Seg_591" s="T47">qwät-ku-t</ta>
            <ta e="T49" id="Seg_592" s="T48">man</ta>
            <ta e="T50" id="Seg_593" s="T49">nʼekča-ŋ</ta>
            <ta e="T51" id="Seg_594" s="T50">ira-w</ta>
            <ta e="T52" id="Seg_595" s="T51">nʼekči-ŋ</ta>
            <ta e="T53" id="Seg_596" s="T52">me</ta>
            <ta e="T54" id="Seg_597" s="T53">üd-ɨ-m</ta>
            <ta e="T55" id="Seg_598" s="T54">ɨrɨ-kɨ-o</ta>
            <ta e="T56" id="Seg_599" s="T55">man</ta>
            <ta e="T57" id="Seg_600" s="T56">tʼära-ku-a-ŋ</ta>
            <ta e="T58" id="Seg_601" s="T57">täb-ɨ-nɨ</ta>
            <ta e="T59" id="Seg_602" s="T58">iː-gu</ta>
            <ta e="T60" id="Seg_603" s="T59">čorgɨ-m</ta>
            <ta e="T61" id="Seg_604" s="T60">üt-ku</ta>
            <ta e="T62" id="Seg_605" s="T61">täp</ta>
            <ta e="T63" id="Seg_606" s="T62">qät-nɨ-t</ta>
            <ta e="T64" id="Seg_607" s="T63">meːŋa</ta>
            <ta e="T65" id="Seg_608" s="T64">qödi-k</ta>
            <ta e="T66" id="Seg_609" s="T65">iː-dä</ta>
            <ta e="T67" id="Seg_610" s="T66">man</ta>
            <ta e="T68" id="Seg_611" s="T67">qwä-sa-ŋ</ta>
            <ta e="T69" id="Seg_612" s="T68">iː-sa-w</ta>
            <ta e="T70" id="Seg_613" s="T69">qojmɨče-s-o</ta>
            <ta e="T71" id="Seg_614" s="T70">täp-se</ta>
            <ta e="T72" id="Seg_615" s="T71">paqtɨ-mbɨ-s-o</ta>
            <ta e="T73" id="Seg_616" s="T72">nännɨ</ta>
            <ta e="T74" id="Seg_617" s="T73">kučan-n-o</ta>
            <ta e="T75" id="Seg_618" s="T74">qondɨ-s-oː</ta>
            <ta e="T76" id="Seg_619" s="T75">wɨs-sa-ŋ</ta>
            <ta e="T77" id="Seg_620" s="T76">olo-w</ta>
            <ta e="T78" id="Seg_621" s="T77">kaš-kɨ-mba</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_622" s="T1">man</ta>
            <ta e="T3" id="Seg_623" s="T2">tämdɨ</ta>
            <ta e="T4" id="Seg_624" s="T3">tʼeːlɨ-sɨ-ŋ</ta>
            <ta e="T5" id="Seg_625" s="T4">man</ta>
            <ta e="T6" id="Seg_626" s="T5">tämdɨ</ta>
            <ta e="T7" id="Seg_627" s="T6">or-ɨ-sɨ-ŋɨ-ŋ</ta>
            <ta e="T8" id="Seg_628" s="T7">poŋur-sɨ-ŋ</ta>
            <ta e="T9" id="Seg_629" s="T8">qwǝlɨ-sɨ-ŋ</ta>
            <ta e="T10" id="Seg_630" s="T9">sɨr-ɨ-la-m</ta>
            <ta e="T11" id="Seg_631" s="T10">pargɨl-sɨ-m</ta>
            <ta e="T12" id="Seg_632" s="T11">po-m</ta>
            <ta e="T13" id="Seg_633" s="T12">tadɨ-ku-sɨ-m</ta>
            <ta e="T14" id="Seg_634" s="T13">man-nan</ta>
            <ta e="T15" id="Seg_635" s="T14">eː-ŋɨ-n</ta>
            <ta e="T16" id="Seg_636" s="T15">nakkɨr</ta>
            <ta e="T17" id="Seg_637" s="T16">üčče-mɨ</ta>
            <ta e="T18" id="Seg_638" s="T17">okkɨr</ta>
            <ta e="T19" id="Seg_639" s="T18">tebe-qum</ta>
            <ta e="T20" id="Seg_640" s="T19">šittə-qi</ta>
            <ta e="T21" id="Seg_641" s="T20">naj-qum</ta>
            <ta e="T22" id="Seg_642" s="T21">ira-mɨ</ta>
            <ta e="T23" id="Seg_643" s="T22">qwänd-ɨ-mbɨ</ta>
            <ta e="T24" id="Seg_644" s="T23">qüːtǝ-n</ta>
            <ta e="T25" id="Seg_645" s="T24">maːt-ɨ-ka</ta>
            <ta e="T26" id="Seg_646" s="T25">nʼünʼü-ka</ta>
            <ta e="T27" id="Seg_647" s="T26">eː-ŋɨ-n</ta>
            <ta e="T28" id="Seg_648" s="T27">okkɨr</ta>
            <ta e="T29" id="Seg_649" s="T28">üččə</ta>
            <ta e="T30" id="Seg_650" s="T29">quː-mbɨ</ta>
            <ta e="T31" id="Seg_651" s="T30">man</ta>
            <ta e="T32" id="Seg_652" s="T31">qum-nɨ</ta>
            <ta e="T33" id="Seg_653" s="T32">qwən-ŋɨ-ŋ</ta>
            <ta e="T34" id="Seg_654" s="T33">kundɨ-qɨn</ta>
            <ta e="T35" id="Seg_655" s="T34">man</ta>
            <ta e="T36" id="Seg_656" s="T35">ira-mɨ</ta>
            <ta e="T37" id="Seg_657" s="T36">soː</ta>
            <ta e="T38" id="Seg_658" s="T37">qum</ta>
            <ta e="T39" id="Seg_659" s="T38">assɨ</ta>
            <ta e="T40" id="Seg_660" s="T39">qwäːdə-mbɨ-ku-n</ta>
            <ta e="T41" id="Seg_661" s="T40">assɨ</ta>
            <ta e="T42" id="Seg_662" s="T41">qätə-lɨ-ku-tɨ</ta>
            <ta e="T43" id="Seg_663" s="T42">tap</ta>
            <ta e="T44" id="Seg_664" s="T43">qwǝlɨ-ŋɨ</ta>
            <ta e="T45" id="Seg_665" s="T44">qwǝlɨ-ŋɨ</ta>
            <ta e="T46" id="Seg_666" s="T45">qoːnɨŋ</ta>
            <ta e="T47" id="Seg_667" s="T46">qwǝlɨ-m</ta>
            <ta e="T48" id="Seg_668" s="T47">qwat-ku-tɨ</ta>
            <ta e="T49" id="Seg_669" s="T48">man</ta>
            <ta e="T50" id="Seg_670" s="T49">negəčču-ŋ</ta>
            <ta e="T51" id="Seg_671" s="T50">ira-mɨ</ta>
            <ta e="T52" id="Seg_672" s="T51">negəčču-n</ta>
            <ta e="T53" id="Seg_673" s="T52">meː</ta>
            <ta e="T54" id="Seg_674" s="T53">üt-ɨ-m</ta>
            <ta e="T55" id="Seg_675" s="T54">ɨrɨ-ku-ut</ta>
            <ta e="T56" id="Seg_676" s="T55">man</ta>
            <ta e="T57" id="Seg_677" s="T56">tʼarɨ-ku-ɨ-ŋ</ta>
            <ta e="T58" id="Seg_678" s="T57">tap-ɨ-nɨ</ta>
            <ta e="T59" id="Seg_679" s="T58">iː-gu</ta>
            <ta e="T60" id="Seg_680" s="T59">čorgɨ-m</ta>
            <ta e="T61" id="Seg_681" s="T60">ütɨ-gu</ta>
            <ta e="T62" id="Seg_682" s="T61">tap</ta>
            <ta e="T63" id="Seg_683" s="T62">kät-ŋɨ-tɨ</ta>
            <ta e="T64" id="Seg_684" s="T63">mäkkä</ta>
            <ta e="T65" id="Seg_685" s="T64">köːdi-k</ta>
            <ta e="T66" id="Seg_686" s="T65">iː-ätɨ</ta>
            <ta e="T67" id="Seg_687" s="T66">man</ta>
            <ta e="T68" id="Seg_688" s="T67">qwən-sɨ-ŋ</ta>
            <ta e="T69" id="Seg_689" s="T68">iː-sɨ-m</ta>
            <ta e="T70" id="Seg_690" s="T69">qojmɨčɨ-sɨ-ut</ta>
            <ta e="T71" id="Seg_691" s="T70">tap-se</ta>
            <ta e="T72" id="Seg_692" s="T71">paktǝ-mbɨ-sɨ-ut</ta>
            <ta e="T73" id="Seg_693" s="T72">nɨːnɨ</ta>
            <ta e="T74" id="Seg_694" s="T73">quːčal-ŋɨ-ut</ta>
            <ta e="T75" id="Seg_695" s="T74">qontə-sɨ-ut</ta>
            <ta e="T76" id="Seg_696" s="T75">wessɨ-sɨ-ŋ</ta>
            <ta e="T77" id="Seg_697" s="T76">olɨ-mɨ</ta>
            <ta e="T78" id="Seg_698" s="T77">küs-ku-mbɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_699" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_700" s="T2">here</ta>
            <ta e="T4" id="Seg_701" s="T3">be.born-PST-1SG.S</ta>
            <ta e="T5" id="Seg_702" s="T4">I.NOM</ta>
            <ta e="T6" id="Seg_703" s="T5">here</ta>
            <ta e="T7" id="Seg_704" s="T6">force-EP-VBLZ-CO-1SG.S</ta>
            <ta e="T8" id="Seg_705" s="T7">fish.with.net-PST-1SG.S</ta>
            <ta e="T9" id="Seg_706" s="T8">fish-PST-1SG.S</ta>
            <ta e="T10" id="Seg_707" s="T9">cow-EP-PL-ACC</ta>
            <ta e="T11" id="Seg_708" s="T10">milk-PST-1SG.O</ta>
            <ta e="T12" id="Seg_709" s="T11">tree-ACC</ta>
            <ta e="T13" id="Seg_710" s="T12">carry-HAB-PST-1SG.O</ta>
            <ta e="T14" id="Seg_711" s="T13">I.GEN-ADES</ta>
            <ta e="T15" id="Seg_712" s="T14">be-CO-3SG.S</ta>
            <ta e="T16" id="Seg_713" s="T15">three</ta>
            <ta e="T17" id="Seg_714" s="T16">child.[NOM]-1SG</ta>
            <ta e="T18" id="Seg_715" s="T17">one</ta>
            <ta e="T19" id="Seg_716" s="T18">man-human.being.[NOM]</ta>
            <ta e="T20" id="Seg_717" s="T19">two-DU</ta>
            <ta e="T21" id="Seg_718" s="T20">also-human.being.[NOM]</ta>
            <ta e="T22" id="Seg_719" s="T21">husband.[NOM]-1SG</ta>
            <ta e="T23" id="Seg_720" s="T22">become.old-EP-PST.NAR.[3SG.S]</ta>
            <ta e="T24" id="Seg_721" s="T23">be.ill-3SG.S</ta>
            <ta e="T25" id="Seg_722" s="T24">house-EP-DIM.[NOM]</ta>
            <ta e="T26" id="Seg_723" s="T25">small-DIM.[NOM]</ta>
            <ta e="T27" id="Seg_724" s="T26">be-CO-3SG.S</ta>
            <ta e="T28" id="Seg_725" s="T27">one</ta>
            <ta e="T29" id="Seg_726" s="T28">child.[NOM]</ta>
            <ta e="T30" id="Seg_727" s="T29">die-PST.NAR.[3SG.S]</ta>
            <ta e="T31" id="Seg_728" s="T30">I.NOM</ta>
            <ta e="T32" id="Seg_729" s="T31">human.being-ALL</ta>
            <ta e="T33" id="Seg_730" s="T32">go.away-CO-1SG.S</ta>
            <ta e="T34" id="Seg_731" s="T33">long-ADV.LOC</ta>
            <ta e="T35" id="Seg_732" s="T34">I.GEN</ta>
            <ta e="T36" id="Seg_733" s="T35">husband.[NOM]-1SG</ta>
            <ta e="T37" id="Seg_734" s="T36">good</ta>
            <ta e="T38" id="Seg_735" s="T37">human.being.[NOM]</ta>
            <ta e="T39" id="Seg_736" s="T38">NEG</ta>
            <ta e="T40" id="Seg_737" s="T39">swear-DUR-HAB-3SG.S</ta>
            <ta e="T41" id="Seg_738" s="T40">NEG</ta>
            <ta e="T42" id="Seg_739" s="T41">beat-RES-HAB-3SG.O</ta>
            <ta e="T43" id="Seg_740" s="T42">(s)he</ta>
            <ta e="T44" id="Seg_741" s="T43">fish-CO.[3SG.S]</ta>
            <ta e="T45" id="Seg_742" s="T44">fish-CO.[3SG.S]</ta>
            <ta e="T46" id="Seg_743" s="T45">many</ta>
            <ta e="T47" id="Seg_744" s="T46">fish-ACC</ta>
            <ta e="T48" id="Seg_745" s="T47">kill-HAB-3SG.O</ta>
            <ta e="T49" id="Seg_746" s="T48">I.NOM</ta>
            <ta e="T50" id="Seg_747" s="T49">smoke-1SG.S</ta>
            <ta e="T51" id="Seg_748" s="T50">husband.[NOM]-1SG</ta>
            <ta e="T52" id="Seg_749" s="T51">smoke-3SG.S</ta>
            <ta e="T53" id="Seg_750" s="T52">we.NOM</ta>
            <ta e="T54" id="Seg_751" s="T53">vodka-EP-ACC</ta>
            <ta e="T55" id="Seg_752" s="T54">drink-HAB-1PL</ta>
            <ta e="T56" id="Seg_753" s="T55">I.NOM</ta>
            <ta e="T57" id="Seg_754" s="T56">say-HAB-EP-1SG.S</ta>
            <ta e="T58" id="Seg_755" s="T57">(s)he-EP-ALL</ta>
            <ta e="T59" id="Seg_756" s="T58">take-INF</ta>
            <ta e="T60" id="Seg_757" s="T59">bottle-ACC</ta>
            <ta e="T61" id="Seg_758" s="T60">drink-INF</ta>
            <ta e="T62" id="Seg_759" s="T61">(s)he</ta>
            <ta e="T63" id="Seg_760" s="T62">say-CO-3SG.O</ta>
            <ta e="T64" id="Seg_761" s="T63">I.ALL</ta>
            <ta e="T65" id="Seg_762" s="T64">go-IMP.2SG.S</ta>
            <ta e="T66" id="Seg_763" s="T65">take-IMP.2SG.O</ta>
            <ta e="T67" id="Seg_764" s="T66">I.NOM</ta>
            <ta e="T68" id="Seg_765" s="T67">go.away-PST-1SG.S</ta>
            <ta e="T69" id="Seg_766" s="T68">take-PST-1SG.O</ta>
            <ta e="T70" id="Seg_767" s="T69">sing-PST-1PL</ta>
            <ta e="T71" id="Seg_768" s="T70">(s)he-INSTR</ta>
            <ta e="T72" id="Seg_769" s="T71">dance-DUR-PST-1PL</ta>
            <ta e="T73" id="Seg_770" s="T72">then</ta>
            <ta e="T74" id="Seg_771" s="T73">lie.down-CO-1PL</ta>
            <ta e="T75" id="Seg_772" s="T74">sleep-PST-1PL</ta>
            <ta e="T76" id="Seg_773" s="T75">get.up-PST-1SG.S</ta>
            <ta e="T77" id="Seg_774" s="T76">head.[NOM]-1SG</ta>
            <ta e="T78" id="Seg_775" s="T77">hurt-HAB-PST.NAR.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_776" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_777" s="T2">здесь</ta>
            <ta e="T4" id="Seg_778" s="T3">родиться-PST-1SG.S</ta>
            <ta e="T5" id="Seg_779" s="T4">я.NOM</ta>
            <ta e="T6" id="Seg_780" s="T5">здесь</ta>
            <ta e="T7" id="Seg_781" s="T6">сила-EP-VBLZ-CO-1SG.S</ta>
            <ta e="T8" id="Seg_782" s="T7">рыбачить.сетью-PST-1SG.S</ta>
            <ta e="T9" id="Seg_783" s="T8">рыбачить-PST-1SG.S</ta>
            <ta e="T10" id="Seg_784" s="T9">корова-EP-PL-ACC</ta>
            <ta e="T11" id="Seg_785" s="T10">подоить-PST-1SG.O</ta>
            <ta e="T12" id="Seg_786" s="T11">дерево-ACC</ta>
            <ta e="T13" id="Seg_787" s="T12">таскать-HAB-PST-1SG.O</ta>
            <ta e="T14" id="Seg_788" s="T13">я.GEN-ADES</ta>
            <ta e="T15" id="Seg_789" s="T14">быть-CO-3SG.S</ta>
            <ta e="T16" id="Seg_790" s="T15">три</ta>
            <ta e="T17" id="Seg_791" s="T16">ребёнок.[NOM]-1SG</ta>
            <ta e="T18" id="Seg_792" s="T17">один</ta>
            <ta e="T19" id="Seg_793" s="T18">мужчина-человек.[NOM]</ta>
            <ta e="T20" id="Seg_794" s="T19">два-DU</ta>
            <ta e="T21" id="Seg_795" s="T20">тоже-человек.[NOM]</ta>
            <ta e="T22" id="Seg_796" s="T21">муж.[NOM]-1SG</ta>
            <ta e="T23" id="Seg_797" s="T22">стареть-EP-PST.NAR.[3SG.S]</ta>
            <ta e="T24" id="Seg_798" s="T23">быть.больным-3SG.S</ta>
            <ta e="T25" id="Seg_799" s="T24">дом-EP-DIM.[NOM]</ta>
            <ta e="T26" id="Seg_800" s="T25">маленький-DIM.[NOM]</ta>
            <ta e="T27" id="Seg_801" s="T26">быть-CO-3SG.S</ta>
            <ta e="T28" id="Seg_802" s="T27">один</ta>
            <ta e="T29" id="Seg_803" s="T28">ребёнок.[NOM]</ta>
            <ta e="T30" id="Seg_804" s="T29">умереть-PST.NAR.[3SG.S]</ta>
            <ta e="T31" id="Seg_805" s="T30">я.NOM</ta>
            <ta e="T32" id="Seg_806" s="T31">человек-ALL</ta>
            <ta e="T33" id="Seg_807" s="T32">уйти-CO-1SG.S</ta>
            <ta e="T34" id="Seg_808" s="T33">долго-ADV.LOC</ta>
            <ta e="T35" id="Seg_809" s="T34">я.GEN</ta>
            <ta e="T36" id="Seg_810" s="T35">муж.[NOM]-1SG</ta>
            <ta e="T37" id="Seg_811" s="T36">хороший</ta>
            <ta e="T38" id="Seg_812" s="T37">человек.[NOM]</ta>
            <ta e="T39" id="Seg_813" s="T38">NEG</ta>
            <ta e="T40" id="Seg_814" s="T39">ругаться-DUR-HAB-3SG.S</ta>
            <ta e="T41" id="Seg_815" s="T40">NEG</ta>
            <ta e="T42" id="Seg_816" s="T41">бить-RES-HAB-3SG.O</ta>
            <ta e="T43" id="Seg_817" s="T42">он(а)</ta>
            <ta e="T44" id="Seg_818" s="T43">рыбачить-CO.[3SG.S]</ta>
            <ta e="T45" id="Seg_819" s="T44">рыбачить-CO.[3SG.S]</ta>
            <ta e="T46" id="Seg_820" s="T45">много</ta>
            <ta e="T47" id="Seg_821" s="T46">рыба-ACC</ta>
            <ta e="T48" id="Seg_822" s="T47">убить-HAB-3SG.O</ta>
            <ta e="T49" id="Seg_823" s="T48">я.NOM</ta>
            <ta e="T50" id="Seg_824" s="T49">курить-1SG.S</ta>
            <ta e="T51" id="Seg_825" s="T50">муж.[NOM]-1SG</ta>
            <ta e="T52" id="Seg_826" s="T51">курить-3SG.S</ta>
            <ta e="T53" id="Seg_827" s="T52">мы.NOM</ta>
            <ta e="T54" id="Seg_828" s="T53">водка-EP-ACC</ta>
            <ta e="T55" id="Seg_829" s="T54">пить-HAB-1PL</ta>
            <ta e="T56" id="Seg_830" s="T55">я.NOM</ta>
            <ta e="T57" id="Seg_831" s="T56">сказать-HAB-EP-1SG.S</ta>
            <ta e="T58" id="Seg_832" s="T57">он(а)-EP-ALL</ta>
            <ta e="T59" id="Seg_833" s="T58">взять-INF</ta>
            <ta e="T60" id="Seg_834" s="T59">бутылка-ACC</ta>
            <ta e="T61" id="Seg_835" s="T60">пить-INF</ta>
            <ta e="T62" id="Seg_836" s="T61">он(а)</ta>
            <ta e="T63" id="Seg_837" s="T62">сказать-CO-3SG.O</ta>
            <ta e="T64" id="Seg_838" s="T63">я.ALL</ta>
            <ta e="T65" id="Seg_839" s="T64">ходить-IMP.2SG.S</ta>
            <ta e="T66" id="Seg_840" s="T65">взять-IMP.2SG.O</ta>
            <ta e="T67" id="Seg_841" s="T66">я.NOM</ta>
            <ta e="T68" id="Seg_842" s="T67">уйти-PST-1SG.S</ta>
            <ta e="T69" id="Seg_843" s="T68">взять-PST-1SG.O</ta>
            <ta e="T70" id="Seg_844" s="T69">петь-PST-1PL</ta>
            <ta e="T71" id="Seg_845" s="T70">он(а)-INSTR</ta>
            <ta e="T72" id="Seg_846" s="T71">танцевать-DUR-PST-1PL</ta>
            <ta e="T73" id="Seg_847" s="T72">потом</ta>
            <ta e="T74" id="Seg_848" s="T73">лечь-CO-1PL</ta>
            <ta e="T75" id="Seg_849" s="T74">спать-PST-1PL</ta>
            <ta e="T76" id="Seg_850" s="T75">встать-PST-1SG.S</ta>
            <ta e="T77" id="Seg_851" s="T76">голова.[NOM]-1SG</ta>
            <ta e="T78" id="Seg_852" s="T77">болеть-HAB-PST.NAR.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_853" s="T1">pers</ta>
            <ta e="T3" id="Seg_854" s="T2">adv</ta>
            <ta e="T4" id="Seg_855" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_856" s="T4">pers</ta>
            <ta e="T6" id="Seg_857" s="T5">adv</ta>
            <ta e="T7" id="Seg_858" s="T6">n-n:ins-adj&gt;v-v:ins-v:pn</ta>
            <ta e="T8" id="Seg_859" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_860" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_861" s="T9">n-n:ins-n:num-n:case</ta>
            <ta e="T11" id="Seg_862" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_863" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_864" s="T12">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_865" s="T13">pers-n:case</ta>
            <ta e="T15" id="Seg_866" s="T14">v-v:ins-v:pn</ta>
            <ta e="T16" id="Seg_867" s="T15">num</ta>
            <ta e="T17" id="Seg_868" s="T16">n.[n:case]-n:poss</ta>
            <ta e="T18" id="Seg_869" s="T17">num</ta>
            <ta e="T19" id="Seg_870" s="T18">n-n.[n:case]</ta>
            <ta e="T20" id="Seg_871" s="T19">num-n:num</ta>
            <ta e="T21" id="Seg_872" s="T20">ptcl-n.[n:case]</ta>
            <ta e="T22" id="Seg_873" s="T21">n.[n:case]-n:poss</ta>
            <ta e="T23" id="Seg_874" s="T22">v-v:ins-v:tense.[v:pn]</ta>
            <ta e="T24" id="Seg_875" s="T23">v-v:pn</ta>
            <ta e="T25" id="Seg_876" s="T24">n-n:ins-n&gt;n.[n:case]</ta>
            <ta e="T26" id="Seg_877" s="T25">adj-n&gt;n.[n:case]</ta>
            <ta e="T27" id="Seg_878" s="T26">v-v:ins-v:pn</ta>
            <ta e="T28" id="Seg_879" s="T27">num</ta>
            <ta e="T29" id="Seg_880" s="T28">n.[n:case]</ta>
            <ta e="T30" id="Seg_881" s="T29">v-v:tense.[v:pn]</ta>
            <ta e="T31" id="Seg_882" s="T30">pers</ta>
            <ta e="T32" id="Seg_883" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_884" s="T32">v-v:ins-v:pn</ta>
            <ta e="T34" id="Seg_885" s="T33">adv-adv:case</ta>
            <ta e="T35" id="Seg_886" s="T34">pers</ta>
            <ta e="T36" id="Seg_887" s="T35">n.[n:case]-n:poss</ta>
            <ta e="T37" id="Seg_888" s="T36">adj</ta>
            <ta e="T38" id="Seg_889" s="T37">n.[n:case]</ta>
            <ta e="T39" id="Seg_890" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_891" s="T39">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T41" id="Seg_892" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_893" s="T41">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T43" id="Seg_894" s="T42">pers</ta>
            <ta e="T44" id="Seg_895" s="T43">v-v:ins.[v:pn]</ta>
            <ta e="T45" id="Seg_896" s="T44">v-v:ins.[v:pn]</ta>
            <ta e="T46" id="Seg_897" s="T45">quant</ta>
            <ta e="T47" id="Seg_898" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_899" s="T47">v-v&gt;v-v:pn</ta>
            <ta e="T49" id="Seg_900" s="T48">pers</ta>
            <ta e="T50" id="Seg_901" s="T49">v-v:pn</ta>
            <ta e="T51" id="Seg_902" s="T50">n.[n:case]-n:poss</ta>
            <ta e="T52" id="Seg_903" s="T51">v-v:pn</ta>
            <ta e="T53" id="Seg_904" s="T52">pers</ta>
            <ta e="T54" id="Seg_905" s="T53">n-n:ins-n:case</ta>
            <ta e="T55" id="Seg_906" s="T54">v-v&gt;v-v:pn</ta>
            <ta e="T56" id="Seg_907" s="T55">pers</ta>
            <ta e="T57" id="Seg_908" s="T56">v-v&gt;v-n:ins-v:pn</ta>
            <ta e="T58" id="Seg_909" s="T57">pers-n:ins-n:case</ta>
            <ta e="T59" id="Seg_910" s="T58">v-v:inf</ta>
            <ta e="T60" id="Seg_911" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_912" s="T60">v-v:inf</ta>
            <ta e="T62" id="Seg_913" s="T61">pers</ta>
            <ta e="T63" id="Seg_914" s="T62">v-v:ins-v:pn</ta>
            <ta e="T64" id="Seg_915" s="T63">pers</ta>
            <ta e="T65" id="Seg_916" s="T64">v-v:mood.pn</ta>
            <ta e="T66" id="Seg_917" s="T65">v-v:mood.pn</ta>
            <ta e="T67" id="Seg_918" s="T66">pers</ta>
            <ta e="T68" id="Seg_919" s="T67">v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_920" s="T68">v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_921" s="T69">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_922" s="T70">pers-n:case</ta>
            <ta e="T72" id="Seg_923" s="T71">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T73" id="Seg_924" s="T72">adv</ta>
            <ta e="T74" id="Seg_925" s="T73">v-v:ins-v:pn</ta>
            <ta e="T75" id="Seg_926" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_927" s="T75">v-v:tense-v:pn</ta>
            <ta e="T77" id="Seg_928" s="T76">n.[n:case]-n:poss</ta>
            <ta e="T78" id="Seg_929" s="T77">v-v&gt;v-v:tense.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_930" s="T1">pers</ta>
            <ta e="T3" id="Seg_931" s="T2">adv</ta>
            <ta e="T4" id="Seg_932" s="T3">v</ta>
            <ta e="T5" id="Seg_933" s="T4">pers</ta>
            <ta e="T6" id="Seg_934" s="T5">adv</ta>
            <ta e="T7" id="Seg_935" s="T6">v</ta>
            <ta e="T8" id="Seg_936" s="T7">v</ta>
            <ta e="T9" id="Seg_937" s="T8">v</ta>
            <ta e="T10" id="Seg_938" s="T9">n</ta>
            <ta e="T11" id="Seg_939" s="T10">v</ta>
            <ta e="T12" id="Seg_940" s="T11">n</ta>
            <ta e="T13" id="Seg_941" s="T12">v</ta>
            <ta e="T14" id="Seg_942" s="T13">pers</ta>
            <ta e="T15" id="Seg_943" s="T14">v</ta>
            <ta e="T16" id="Seg_944" s="T15">num</ta>
            <ta e="T17" id="Seg_945" s="T16">n</ta>
            <ta e="T18" id="Seg_946" s="T17">num</ta>
            <ta e="T19" id="Seg_947" s="T18">n</ta>
            <ta e="T20" id="Seg_948" s="T19">adv</ta>
            <ta e="T21" id="Seg_949" s="T20">n</ta>
            <ta e="T22" id="Seg_950" s="T21">n</ta>
            <ta e="T23" id="Seg_951" s="T22">v</ta>
            <ta e="T24" id="Seg_952" s="T23">v</ta>
            <ta e="T25" id="Seg_953" s="T24">n</ta>
            <ta e="T26" id="Seg_954" s="T25">n</ta>
            <ta e="T27" id="Seg_955" s="T26">v</ta>
            <ta e="T28" id="Seg_956" s="T27">num</ta>
            <ta e="T29" id="Seg_957" s="T28">n</ta>
            <ta e="T30" id="Seg_958" s="T29">v</ta>
            <ta e="T31" id="Seg_959" s="T30">pers</ta>
            <ta e="T32" id="Seg_960" s="T31">v</ta>
            <ta e="T33" id="Seg_961" s="T32">v</ta>
            <ta e="T34" id="Seg_962" s="T33">adv</ta>
            <ta e="T35" id="Seg_963" s="T34">pers</ta>
            <ta e="T36" id="Seg_964" s="T35">n</ta>
            <ta e="T37" id="Seg_965" s="T36">adj</ta>
            <ta e="T38" id="Seg_966" s="T37">n</ta>
            <ta e="T39" id="Seg_967" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_968" s="T39">v</ta>
            <ta e="T41" id="Seg_969" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_970" s="T41">v</ta>
            <ta e="T43" id="Seg_971" s="T42">pers</ta>
            <ta e="T44" id="Seg_972" s="T43">v</ta>
            <ta e="T45" id="Seg_973" s="T44">v</ta>
            <ta e="T46" id="Seg_974" s="T45">quant</ta>
            <ta e="T47" id="Seg_975" s="T46">n</ta>
            <ta e="T48" id="Seg_976" s="T47">v</ta>
            <ta e="T49" id="Seg_977" s="T48">pers</ta>
            <ta e="T50" id="Seg_978" s="T49">v</ta>
            <ta e="T51" id="Seg_979" s="T50">n</ta>
            <ta e="T52" id="Seg_980" s="T51">v</ta>
            <ta e="T53" id="Seg_981" s="T52">pers</ta>
            <ta e="T54" id="Seg_982" s="T53">n</ta>
            <ta e="T55" id="Seg_983" s="T54">v</ta>
            <ta e="T56" id="Seg_984" s="T55">pers</ta>
            <ta e="T57" id="Seg_985" s="T56">v</ta>
            <ta e="T58" id="Seg_986" s="T57">pers</ta>
            <ta e="T59" id="Seg_987" s="T58">v</ta>
            <ta e="T60" id="Seg_988" s="T59">n</ta>
            <ta e="T61" id="Seg_989" s="T60">v</ta>
            <ta e="T62" id="Seg_990" s="T61">pers</ta>
            <ta e="T63" id="Seg_991" s="T62">v</ta>
            <ta e="T64" id="Seg_992" s="T63">pers</ta>
            <ta e="T65" id="Seg_993" s="T64">v</ta>
            <ta e="T66" id="Seg_994" s="T65">v</ta>
            <ta e="T67" id="Seg_995" s="T66">pers</ta>
            <ta e="T68" id="Seg_996" s="T67">v</ta>
            <ta e="T69" id="Seg_997" s="T68">v</ta>
            <ta e="T70" id="Seg_998" s="T69">v</ta>
            <ta e="T71" id="Seg_999" s="T70">pers</ta>
            <ta e="T72" id="Seg_1000" s="T71">v</ta>
            <ta e="T73" id="Seg_1001" s="T72">adv</ta>
            <ta e="T74" id="Seg_1002" s="T73">v</ta>
            <ta e="T75" id="Seg_1003" s="T74">v</ta>
            <ta e="T76" id="Seg_1004" s="T75">v</ta>
            <ta e="T77" id="Seg_1005" s="T76">n</ta>
            <ta e="T78" id="Seg_1006" s="T77">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1007" s="T1">pro.h:S</ta>
            <ta e="T4" id="Seg_1008" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_1009" s="T4">pro.h:S</ta>
            <ta e="T7" id="Seg_1010" s="T6">v:pred</ta>
            <ta e="T8" id="Seg_1011" s="T7">0.1.h:S v:pred</ta>
            <ta e="T9" id="Seg_1012" s="T8">0.1.h:S v:pred</ta>
            <ta e="T10" id="Seg_1013" s="T9">np:O</ta>
            <ta e="T11" id="Seg_1014" s="T10">0.1.h:S v:pred</ta>
            <ta e="T12" id="Seg_1015" s="T11">np:O</ta>
            <ta e="T13" id="Seg_1016" s="T12">0.1.h:S v:pred</ta>
            <ta e="T15" id="Seg_1017" s="T14">v:pred</ta>
            <ta e="T17" id="Seg_1018" s="T16">np.h:S</ta>
            <ta e="T19" id="Seg_1019" s="T18">np.h:S</ta>
            <ta e="T21" id="Seg_1020" s="T20">np.h:S</ta>
            <ta e="T22" id="Seg_1021" s="T21">np.h:S</ta>
            <ta e="T23" id="Seg_1022" s="T22">v:pred</ta>
            <ta e="T24" id="Seg_1023" s="T23">0.3.h:S v:pred</ta>
            <ta e="T25" id="Seg_1024" s="T24">np:S</ta>
            <ta e="T26" id="Seg_1025" s="T25">adj:pred</ta>
            <ta e="T27" id="Seg_1026" s="T26">cop</ta>
            <ta e="T29" id="Seg_1027" s="T28">np.h:S</ta>
            <ta e="T30" id="Seg_1028" s="T29">v:pred</ta>
            <ta e="T31" id="Seg_1029" s="T30">pro.h:S</ta>
            <ta e="T33" id="Seg_1030" s="T32">v:pred</ta>
            <ta e="T36" id="Seg_1031" s="T35">np.h:S</ta>
            <ta e="T38" id="Seg_1032" s="T37">n:pred</ta>
            <ta e="T40" id="Seg_1033" s="T39">0.3.h:S v:pred</ta>
            <ta e="T42" id="Seg_1034" s="T41">0.3.h:S v:pred</ta>
            <ta e="T43" id="Seg_1035" s="T42">pro.h:S</ta>
            <ta e="T44" id="Seg_1036" s="T43">v:pred</ta>
            <ta e="T47" id="Seg_1037" s="T46">np:O</ta>
            <ta e="T48" id="Seg_1038" s="T47">0.3.h:S v:pred</ta>
            <ta e="T49" id="Seg_1039" s="T48">pro.h:S</ta>
            <ta e="T50" id="Seg_1040" s="T49">pro.h:S</ta>
            <ta e="T51" id="Seg_1041" s="T50">np.h:S</ta>
            <ta e="T52" id="Seg_1042" s="T51">v:pred</ta>
            <ta e="T53" id="Seg_1043" s="T52">pro.h:S</ta>
            <ta e="T54" id="Seg_1044" s="T53">np:O</ta>
            <ta e="T55" id="Seg_1045" s="T54">v:pred</ta>
            <ta e="T56" id="Seg_1046" s="T55">pro.h:S</ta>
            <ta e="T57" id="Seg_1047" s="T56">v:pred</ta>
            <ta e="T62" id="Seg_1048" s="T61">pro.h:S</ta>
            <ta e="T63" id="Seg_1049" s="T62">v:pred</ta>
            <ta e="T65" id="Seg_1050" s="T64">0.2.h:S v:pred</ta>
            <ta e="T66" id="Seg_1051" s="T65">0.2.h:S v:pred</ta>
            <ta e="T67" id="Seg_1052" s="T66">pro.h:S</ta>
            <ta e="T68" id="Seg_1053" s="T67">v:pred</ta>
            <ta e="T69" id="Seg_1054" s="T68">0.1.h:S v:pred</ta>
            <ta e="T70" id="Seg_1055" s="T69">0.1.h:S v:pred</ta>
            <ta e="T72" id="Seg_1056" s="T71">0.1.h:S v:pred</ta>
            <ta e="T74" id="Seg_1057" s="T73">0.1.h:S v:pred</ta>
            <ta e="T75" id="Seg_1058" s="T74">0.1.h:S v:pred</ta>
            <ta e="T76" id="Seg_1059" s="T75">0.1.h:S v:pred</ta>
            <ta e="T77" id="Seg_1060" s="T76">np:S</ta>
            <ta e="T78" id="Seg_1061" s="T77">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1062" s="T1">pro.h:P</ta>
            <ta e="T3" id="Seg_1063" s="T2">adv:L</ta>
            <ta e="T5" id="Seg_1064" s="T4">pro.h:P</ta>
            <ta e="T6" id="Seg_1065" s="T5">adv:L</ta>
            <ta e="T8" id="Seg_1066" s="T7">0.1.h:A</ta>
            <ta e="T9" id="Seg_1067" s="T8">0.1.h:A</ta>
            <ta e="T10" id="Seg_1068" s="T9">np:P</ta>
            <ta e="T11" id="Seg_1069" s="T10">0.1.h:A</ta>
            <ta e="T12" id="Seg_1070" s="T11">np:Th</ta>
            <ta e="T13" id="Seg_1071" s="T12">0.1.h:A</ta>
            <ta e="T14" id="Seg_1072" s="T13">pro.h:Poss</ta>
            <ta e="T17" id="Seg_1073" s="T16">np.h:Th</ta>
            <ta e="T19" id="Seg_1074" s="T18">np.h:Th</ta>
            <ta e="T21" id="Seg_1075" s="T20">np.h:Th</ta>
            <ta e="T22" id="Seg_1076" s="T21">np.h:P 0.1.h:Poss</ta>
            <ta e="T24" id="Seg_1077" s="T23">0.3.h:P</ta>
            <ta e="T25" id="Seg_1078" s="T24">np:Th</ta>
            <ta e="T29" id="Seg_1079" s="T28">np.h:P</ta>
            <ta e="T31" id="Seg_1080" s="T30">pro.h:A</ta>
            <ta e="T32" id="Seg_1081" s="T31">np.h:G</ta>
            <ta e="T34" id="Seg_1082" s="T33">adv:Time</ta>
            <ta e="T35" id="Seg_1083" s="T34">pro.h:Poss</ta>
            <ta e="T36" id="Seg_1084" s="T35">np.h:Th 0.1.h:Poss</ta>
            <ta e="T40" id="Seg_1085" s="T39">0.3.h:A</ta>
            <ta e="T42" id="Seg_1086" s="T41">0.3.h:A</ta>
            <ta e="T43" id="Seg_1087" s="T42">pro.h:A</ta>
            <ta e="T47" id="Seg_1088" s="T46">np:P</ta>
            <ta e="T48" id="Seg_1089" s="T47">0.3.h:A</ta>
            <ta e="T49" id="Seg_1090" s="T48">pro.h:A</ta>
            <ta e="T51" id="Seg_1091" s="T50">np.h:A 0.1.h:Poss</ta>
            <ta e="T53" id="Seg_1092" s="T52">pro.h:A</ta>
            <ta e="T54" id="Seg_1093" s="T53">np:P</ta>
            <ta e="T56" id="Seg_1094" s="T55">pro.h:A</ta>
            <ta e="T58" id="Seg_1095" s="T57">np.h:R</ta>
            <ta e="T60" id="Seg_1096" s="T59">np:Th</ta>
            <ta e="T62" id="Seg_1097" s="T61">pro.h:A</ta>
            <ta e="T64" id="Seg_1098" s="T63">pro.h:R</ta>
            <ta e="T65" id="Seg_1099" s="T64">0.2.h:A</ta>
            <ta e="T66" id="Seg_1100" s="T65">0.2.h:A</ta>
            <ta e="T67" id="Seg_1101" s="T66">pro.h:A</ta>
            <ta e="T69" id="Seg_1102" s="T68">0.1.h:A</ta>
            <ta e="T70" id="Seg_1103" s="T69">0.1.h:A</ta>
            <ta e="T71" id="Seg_1104" s="T70">pro:Com</ta>
            <ta e="T72" id="Seg_1105" s="T71">0.1.h:A</ta>
            <ta e="T73" id="Seg_1106" s="T72">adv:Time</ta>
            <ta e="T74" id="Seg_1107" s="T73">0.1.h:A</ta>
            <ta e="T75" id="Seg_1108" s="T74">0.1.h:Th</ta>
            <ta e="T76" id="Seg_1109" s="T75">0.1.h:A</ta>
            <ta e="T77" id="Seg_1110" s="T76">np:P 0.1.h:Poss</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_1111" s="T1">I was born here.</ta>
            <ta e="T7" id="Seg_1112" s="T4">I grew up here.</ta>
            <ta e="T9" id="Seg_1113" s="T7">I went fishing, I fished with nets.</ta>
            <ta e="T11" id="Seg_1114" s="T9">I milked cows.</ta>
            <ta e="T13" id="Seg_1115" s="T11">I used to carry firewood.</ta>
            <ta e="T21" id="Seg_1116" s="T13">I've got three children: one boy, two girls.</ta>
            <ta e="T24" id="Seg_1117" s="T21">My husband grew old, he is ill.</ta>
            <ta e="T27" id="Seg_1118" s="T24">The house is small.</ta>
            <ta e="T30" id="Seg_1119" s="T27">One child died.</ta>
            <ta e="T34" id="Seg_1120" s="T30">I got married long ago.</ta>
            <ta e="T42" id="Seg_1121" s="T34">My husband is a good man, he doesn't scold and doesn't beat me.</ta>
            <ta e="T48" id="Seg_1122" s="T42">He goes fishing, he catches lots of fish.</ta>
            <ta e="T52" id="Seg_1123" s="T48">I smoke, my husband smokes.</ta>
            <ta e="T55" id="Seg_1124" s="T52">We use to drink vodka.</ta>
            <ta e="T61" id="Seg_1125" s="T55">I told him to buy a bottle for us to drink.</ta>
            <ta e="T66" id="Seg_1126" s="T61">He told me: “You go and buy.”</ta>
            <ta e="T69" id="Seg_1127" s="T66">I went and bought it.</ta>
            <ta e="T72" id="Seg_1128" s="T69">We sang and danced with him.</ta>
            <ta e="T75" id="Seg_1129" s="T72">Then we went to sleep.</ta>
            <ta e="T76" id="Seg_1130" s="T75">I got up.</ta>
            <ta e="T78" id="Seg_1131" s="T76">I had headache.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_1132" s="T1">Ich wurde hier geboren.</ta>
            <ta e="T7" id="Seg_1133" s="T4">Ich bin hier aufgewachsen.</ta>
            <ta e="T9" id="Seg_1134" s="T7">Ich ging fischen, ich fischte mit Netzen.</ta>
            <ta e="T11" id="Seg_1135" s="T9">Ich habe Kühe gemolken.</ta>
            <ta e="T13" id="Seg_1136" s="T11">Ich pflegte, Feuerholz zu tragen.</ta>
            <ta e="T21" id="Seg_1137" s="T13">Ich habe drei Kinder: einen Jungen und zwei Mädchen.</ta>
            <ta e="T24" id="Seg_1138" s="T21">Mein Mann wurde alt, er ist krank.</ta>
            <ta e="T27" id="Seg_1139" s="T24">Das Haus ist klein.</ta>
            <ta e="T30" id="Seg_1140" s="T27">Ein Kind ist gestorben.</ta>
            <ta e="T34" id="Seg_1141" s="T30">Ich habe vor langer Zeit geheiratet.</ta>
            <ta e="T42" id="Seg_1142" s="T34">Mein Mann ist ein guter Mann, er schimpft nicht und er schlägt mich nicht.</ta>
            <ta e="T48" id="Seg_1143" s="T42">Er geht fischen, er fängt viel Fisch.</ta>
            <ta e="T52" id="Seg_1144" s="T48">Ich rauche, mein Mann raucht.</ta>
            <ta e="T55" id="Seg_1145" s="T52">Wir trinken Wodka.</ta>
            <ta e="T61" id="Seg_1146" s="T55">Ich bat ihn, eine Flasche für uns zum Trinken zu kaufen.</ta>
            <ta e="T66" id="Seg_1147" s="T61">Er sagte mir: "Geh du und kaufe."</ta>
            <ta e="T69" id="Seg_1148" s="T66">Ich ging und kaufte ihn.</ta>
            <ta e="T72" id="Seg_1149" s="T69">Wir haben mit ihm gesungen und getanzt.</ta>
            <ta e="T75" id="Seg_1150" s="T72">Dann gingen wir schlafen.</ta>
            <ta e="T76" id="Seg_1151" s="T75">Ich stand auf.</ta>
            <ta e="T78" id="Seg_1152" s="T76">Ich hatte Kopfschmerzen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_1153" s="T1">Я здесь родилась.</ta>
            <ta e="T7" id="Seg_1154" s="T4">Я здесь выросла.</ta>
            <ta e="T9" id="Seg_1155" s="T7">Сети ставила, рыбачила.</ta>
            <ta e="T11" id="Seg_1156" s="T9">Коров доила.</ta>
            <ta e="T13" id="Seg_1157" s="T11">Дрова возила.</ta>
            <ta e="T21" id="Seg_1158" s="T13">У меня три ребенка: один мальчик, две девочки.</ta>
            <ta e="T24" id="Seg_1159" s="T21">Старик состарился, болеет. </ta>
            <ta e="T27" id="Seg_1160" s="T24">Дом маленький.</ta>
            <ta e="T30" id="Seg_1161" s="T27">Один ребенок умер.</ta>
            <ta e="T34" id="Seg_1162" s="T30">Я замуж вышла давно.</ta>
            <ta e="T42" id="Seg_1163" s="T34">Мой муж хороший человек, не ругается, меня не бьет.</ta>
            <ta e="T48" id="Seg_1164" s="T42">Он рыбачит, много рыбы добывает.</ta>
            <ta e="T52" id="Seg_1165" s="T48">Я курю, старик курит.</ta>
            <ta e="T55" id="Seg_1166" s="T52">Мы водку пьем.</ta>
            <ta e="T61" id="Seg_1167" s="T55">Я сказала ему взять бутылку выпить.</ta>
            <ta e="T66" id="Seg_1168" s="T61">Он сказал мне: “Пойди возьми”.</ta>
            <ta e="T69" id="Seg_1169" s="T66">Я пошла, взяла.</ta>
            <ta e="T72" id="Seg_1170" s="T69">Пели с ним, плясали.</ta>
            <ta e="T75" id="Seg_1171" s="T72">Потом легли спать.</ta>
            <ta e="T76" id="Seg_1172" s="T75">Я встала.</ta>
            <ta e="T78" id="Seg_1173" s="T76">У меня болела голова.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_1174" s="T1">я здесь родилась</ta>
            <ta e="T7" id="Seg_1175" s="T4">я здесь выросла</ta>
            <ta e="T9" id="Seg_1176" s="T7">сети ставила рыбачила</ta>
            <ta e="T11" id="Seg_1177" s="T9">коров доила</ta>
            <ta e="T13" id="Seg_1178" s="T11">дрова возила</ta>
            <ta e="T21" id="Seg_1179" s="T13">у меня три ребенка один мальчик две девочки</ta>
            <ta e="T24" id="Seg_1180" s="T21">старик старый стал (состарился) хворает</ta>
            <ta e="T27" id="Seg_1181" s="T24">дом маленький</ta>
            <ta e="T30" id="Seg_1182" s="T27">один ребенок умер</ta>
            <ta e="T34" id="Seg_1183" s="T30">я замуж вышла давно</ta>
            <ta e="T42" id="Seg_1184" s="T34">мой муж хороший человек не ругается не бьет</ta>
            <ta e="T48" id="Seg_1185" s="T42">он рыбачит много рыбы добывает</ta>
            <ta e="T52" id="Seg_1186" s="T48">я курю старик курит</ta>
            <ta e="T55" id="Seg_1187" s="T52">мы водку пьем</ta>
            <ta e="T61" id="Seg_1188" s="T55">я сказала ему взять поллитра выпить</ta>
            <ta e="T66" id="Seg_1189" s="T61">он сказал мне пойди возьми</ta>
            <ta e="T69" id="Seg_1190" s="T66">я пошла взяла</ta>
            <ta e="T72" id="Seg_1191" s="T69">пели с ним плясали</ta>
            <ta e="T75" id="Seg_1192" s="T72">потом легли спать</ta>
            <ta e="T76" id="Seg_1193" s="T75">встали</ta>
            <ta e="T78" id="Seg_1194" s="T76">голова болит</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
