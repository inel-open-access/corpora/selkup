<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>TAN_1963_MyDay_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">TAN_1963_MyDay_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">20</ud-information>
            <ud-information attribute-name="# HIAT:w">13</ud-information>
            <ud-information attribute-name="# e">13</ud-information>
            <ud-information attribute-name="# HIAT:u">4</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="TAN">
            <abbreviation>TAN</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="TAN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T14" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">qarʼimɨɣɨn</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">wäsaaŋ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">awɨrgu</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">qwɨnnaŋ</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_18" n="HIAT:ip">(</nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">lafqwɨndə</ts>
                  <nts id="Seg_21" n="HIAT:ip">)</nts>
                  <nts id="Seg_22" n="HIAT:ip">,</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">lawkoɣɨn</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">iːmbaw</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">nʼäjem</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_35" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">qwɨsaŋ</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">banʼäm</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">tʼättɨgu</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_47" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">man</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">aːwɨrsaŋ</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T14" id="Seg_55" n="sc" s="T1">
               <ts e="T2" id="Seg_57" n="e" s="T1">qarʼimɨɣɨn </ts>
               <ts e="T3" id="Seg_59" n="e" s="T2">wäsaaŋ </ts>
               <ts e="T4" id="Seg_61" n="e" s="T3">awɨrgu. </ts>
               <ts e="T5" id="Seg_63" n="e" s="T4">qwɨnnaŋ </ts>
               <ts e="T6" id="Seg_65" n="e" s="T5">(lafqwɨndə), </ts>
               <ts e="T7" id="Seg_67" n="e" s="T6">lawkoɣɨn </ts>
               <ts e="T8" id="Seg_69" n="e" s="T7">iːmbaw </ts>
               <ts e="T9" id="Seg_71" n="e" s="T8">nʼäjem. </ts>
               <ts e="T10" id="Seg_73" n="e" s="T9">qwɨsaŋ </ts>
               <ts e="T11" id="Seg_75" n="e" s="T10">banʼäm </ts>
               <ts e="T12" id="Seg_77" n="e" s="T11">tʼättɨgu. </ts>
               <ts e="T13" id="Seg_79" n="e" s="T12">man </ts>
               <ts e="T14" id="Seg_81" n="e" s="T13">aːwɨrsaŋ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_82" s="T1">TAN_1963_MyDay_nar.001 (001.001)</ta>
            <ta e="T9" id="Seg_83" s="T4">TAN_1963_MyDay_nar.002 (001.002)</ta>
            <ta e="T12" id="Seg_84" s="T9">TAN_1963_MyDay_nar.003 (001.003)</ta>
            <ta e="T14" id="Seg_85" s="T12">TAN_1963_MyDay_nar.004 (001.004)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_86" s="T1">kа′рʼе(и)мыɣын вӓ′са̄ң ′авыргу.</ta>
            <ta e="T9" id="Seg_87" s="T4">kwы′ннаң (′лафkwындъ) ′лавкоɣын, ′ӣмбав ′нʼӓjем.</ta>
            <ta e="T12" id="Seg_88" s="T9">kwы′саң ′банʼӓм ′тʼӓтты(а)гу.</ta>
            <ta e="T14" id="Seg_89" s="T12">ман а̄выр′саң.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_90" s="T1">qarʼe(i)mɨɣɨn väsaːŋ avɨrgu.</ta>
            <ta e="T9" id="Seg_91" s="T4">qwɨnnaŋ (lafqwɨndə) lavkoɣɨn, iːmbav nʼäjem.</ta>
            <ta e="T12" id="Seg_92" s="T9">qwɨsaŋ banʼäm tʼättɨ(a)gu.</ta>
            <ta e="T14" id="Seg_93" s="T12">man aːvɨrsaŋ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_94" s="T1">qarʼimɨɣɨn wäsaaŋ awɨrgu. </ta>
            <ta e="T9" id="Seg_95" s="T4">qwɨnnaŋ (lafqwɨndə), lawkoɣɨn iːmbaw nʼäjem. </ta>
            <ta e="T12" id="Seg_96" s="T9">qwɨsaŋ banʼäm tʼättɨgu. </ta>
            <ta e="T14" id="Seg_97" s="T12">man aːwɨrsaŋ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_98" s="T1">qarʼi-mɨ-ɣɨn</ta>
            <ta e="T3" id="Seg_99" s="T2">wäsa-a-ŋ</ta>
            <ta e="T4" id="Seg_100" s="T3">aw-ɨ-r-gu</ta>
            <ta e="T5" id="Seg_101" s="T4">qwɨn-na-ŋ</ta>
            <ta e="T6" id="Seg_102" s="T5">lafqwɨ-ndə</ta>
            <ta e="T7" id="Seg_103" s="T6">lawko-ɣɨn</ta>
            <ta e="T8" id="Seg_104" s="T7">iː-mba-w</ta>
            <ta e="T9" id="Seg_105" s="T8">nʼäj-e-m</ta>
            <ta e="T10" id="Seg_106" s="T9">qwɨ-sa-ŋ</ta>
            <ta e="T11" id="Seg_107" s="T10">banʼä-m</ta>
            <ta e="T12" id="Seg_108" s="T11">tʼättɨ-gu</ta>
            <ta e="T13" id="Seg_109" s="T12">man</ta>
            <ta e="T14" id="Seg_110" s="T13">aːw-ɨ-r-sa-ŋ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_111" s="T1">qarɨ-mɨ-qən</ta>
            <ta e="T3" id="Seg_112" s="T2">wessɨ-ɨ-ŋ</ta>
            <ta e="T4" id="Seg_113" s="T3">am-ɨ-r-gu</ta>
            <ta e="T5" id="Seg_114" s="T4">qwən-ŋɨ-ŋ</ta>
            <ta e="T6" id="Seg_115" s="T5">lafqwɨ-ndɨ</ta>
            <ta e="T7" id="Seg_116" s="T6">lafqwɨ-qən</ta>
            <ta e="T8" id="Seg_117" s="T7">iː-mbɨ-m</ta>
            <ta e="T9" id="Seg_118" s="T8">nʼaj-ɨ-m</ta>
            <ta e="T10" id="Seg_119" s="T9">qwən-sɨ-ŋ</ta>
            <ta e="T11" id="Seg_120" s="T10">banʼä-m</ta>
            <ta e="T12" id="Seg_121" s="T11">tʼadɨ-gu</ta>
            <ta e="T13" id="Seg_122" s="T12">man</ta>
            <ta e="T14" id="Seg_123" s="T13">am-ɨ-r-sɨ-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_124" s="T1">morning-something-LOC</ta>
            <ta e="T3" id="Seg_125" s="T2">get.up-EP-1SG.S</ta>
            <ta e="T4" id="Seg_126" s="T3">eat-EP-FRQ-INF</ta>
            <ta e="T5" id="Seg_127" s="T4">go.away-CO-1SG.S</ta>
            <ta e="T6" id="Seg_128" s="T5">shop-ILL</ta>
            <ta e="T7" id="Seg_129" s="T6">shop-LOC</ta>
            <ta e="T8" id="Seg_130" s="T7">take-PST.NAR-1SG.O</ta>
            <ta e="T9" id="Seg_131" s="T8">bread-EP-ACC</ta>
            <ta e="T10" id="Seg_132" s="T9">go.away-PST-1SG.S</ta>
            <ta e="T11" id="Seg_133" s="T10">bathhouse-ACC</ta>
            <ta e="T12" id="Seg_134" s="T11">burn-INF</ta>
            <ta e="T13" id="Seg_135" s="T12">I.NOM</ta>
            <ta e="T14" id="Seg_136" s="T13">eat-EP-FRQ-PST-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_137" s="T1">утро-нечто-LOC</ta>
            <ta e="T3" id="Seg_138" s="T2">встать-EP-1SG.S</ta>
            <ta e="T4" id="Seg_139" s="T3">съесть-EP-FRQ-INF</ta>
            <ta e="T5" id="Seg_140" s="T4">уйти-CO-1SG.S</ta>
            <ta e="T6" id="Seg_141" s="T5">лавка-ILL</ta>
            <ta e="T7" id="Seg_142" s="T6">лавка-LOC</ta>
            <ta e="T8" id="Seg_143" s="T7">взять-PST.NAR-1SG.O</ta>
            <ta e="T9" id="Seg_144" s="T8">хлеб-EP-ACC</ta>
            <ta e="T10" id="Seg_145" s="T9">уйти-PST-1SG.S</ta>
            <ta e="T11" id="Seg_146" s="T10">баня-ACC</ta>
            <ta e="T12" id="Seg_147" s="T11">гореть-INF</ta>
            <ta e="T13" id="Seg_148" s="T12">я.NOM</ta>
            <ta e="T14" id="Seg_149" s="T13">съесть-EP-FRQ-PST-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_150" s="T1">n-n-n:case</ta>
            <ta e="T3" id="Seg_151" s="T2">v-v:ins-v:pn</ta>
            <ta e="T4" id="Seg_152" s="T3">v-v:ins-v&gt;v-v:inf</ta>
            <ta e="T5" id="Seg_153" s="T4">v-v:ins-v:pn</ta>
            <ta e="T6" id="Seg_154" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_155" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_156" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_157" s="T8">n-n:ins-n:case</ta>
            <ta e="T10" id="Seg_158" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_159" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_160" s="T11">v-v:inf</ta>
            <ta e="T13" id="Seg_161" s="T12">pers</ta>
            <ta e="T14" id="Seg_162" s="T13">v-v:ins-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_163" s="T1">n</ta>
            <ta e="T3" id="Seg_164" s="T2">v</ta>
            <ta e="T4" id="Seg_165" s="T3">v</ta>
            <ta e="T5" id="Seg_166" s="T4">v</ta>
            <ta e="T6" id="Seg_167" s="T5">n</ta>
            <ta e="T7" id="Seg_168" s="T6">n</ta>
            <ta e="T8" id="Seg_169" s="T7">v</ta>
            <ta e="T9" id="Seg_170" s="T8">n</ta>
            <ta e="T10" id="Seg_171" s="T9">v</ta>
            <ta e="T11" id="Seg_172" s="T10">n</ta>
            <ta e="T12" id="Seg_173" s="T11">v</ta>
            <ta e="T13" id="Seg_174" s="T12">pers</ta>
            <ta e="T14" id="Seg_175" s="T13">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_176" s="T2">0.1.h:S v:pred</ta>
            <ta e="T4" id="Seg_177" s="T3">s:purp</ta>
            <ta e="T5" id="Seg_178" s="T4">0.1.h:S v:pred</ta>
            <ta e="T8" id="Seg_179" s="T7">0.1.h:S v:pred</ta>
            <ta e="T9" id="Seg_180" s="T8">np:O</ta>
            <ta e="T10" id="Seg_181" s="T9">0.1.h:S v:pred</ta>
            <ta e="T12" id="Seg_182" s="T10">s:purp</ta>
            <ta e="T13" id="Seg_183" s="T12">pro.h:S</ta>
            <ta e="T14" id="Seg_184" s="T13">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_185" s="T1">np:Time</ta>
            <ta e="T3" id="Seg_186" s="T2">0.1.h:A</ta>
            <ta e="T5" id="Seg_187" s="T4">0.1.h:A</ta>
            <ta e="T6" id="Seg_188" s="T5">np:G</ta>
            <ta e="T7" id="Seg_189" s="T6">np:L</ta>
            <ta e="T8" id="Seg_190" s="T7">0.1.h:A</ta>
            <ta e="T9" id="Seg_191" s="T8">np:Th</ta>
            <ta e="T10" id="Seg_192" s="T9">0.1.h:A</ta>
            <ta e="T11" id="Seg_193" s="T10">np:Th</ta>
            <ta e="T13" id="Seg_194" s="T12">pro.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T6" id="Seg_195" s="T5">RUS:cult</ta>
            <ta e="T7" id="Seg_196" s="T6">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T6" id="Seg_197" s="T5">finVins</ta>
            <ta e="T7" id="Seg_198" s="T6">finVins</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T6" id="Seg_199" s="T5">dir:infl</ta>
            <ta e="T7" id="Seg_200" s="T6">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_201" s="T1">I got up for breakfast this morning.</ta>
            <ta e="T9" id="Seg_202" s="T4">I went to the shop, bought bread in the shop.</ta>
            <ta e="T12" id="Seg_203" s="T9">I went to heat up the bathhouse.</ta>
            <ta e="T14" id="Seg_204" s="T12">I have eaten.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_205" s="T1">Ich bin heute Morgen zum Frühstück aufgestanden.</ta>
            <ta e="T9" id="Seg_206" s="T4">Ich ging in den Laden, kaufte Brot im Laden.</ta>
            <ta e="T12" id="Seg_207" s="T9">Ich ging, um das Badehaus anzuheizen. </ta>
            <ta e="T14" id="Seg_208" s="T12">Ich habe gegessen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_209" s="T1">Утром встала поесть.</ta>
            <ta e="T9" id="Seg_210" s="T4">Сходила в магазин, в магазине купила хлеб.</ta>
            <ta e="T12" id="Seg_211" s="T9">Пошла баню (за)топить.</ta>
            <ta e="T14" id="Seg_212" s="T12">Я поела.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_213" s="T1">утром встала завтракать (ела)</ta>
            <ta e="T9" id="Seg_214" s="T4">сходила в магазин взяла хлеб</ta>
            <ta e="T12" id="Seg_215" s="T9">пошла баню (за)топить</ta>
            <ta e="T14" id="Seg_216" s="T12">я поела</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
