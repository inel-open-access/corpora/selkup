<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>TAN_1963_MyWorkWithLinguists_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">TAN_1963_MyWorkWithLinguists_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">48</ud-information>
            <ud-information attribute-name="# HIAT:w">39</ud-information>
            <ud-information attribute-name="# e">39</ud-information>
            <ud-information attribute-name="# HIAT:u">8</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="TAN">
            <abbreviation>TAN</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="TAN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T40" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">iraw</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">qwännɨ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">oqqɨr</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">irettə</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_20" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">man</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">onnäŋ</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">qalaŋ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">üčezä</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_35" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">meŋa</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">tümbattɨ</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">šɨtə</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">qum</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_49" n="HIAT:w" s="T14">qasaɣə</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_52" n="HIAT:w" s="T15">pajala</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_55" n="HIAT:w" s="T16">nägɨrguwattə</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_58" n="HIAT:w" s="T17">süsüɣulan</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_61" n="HIAT:w" s="T18">äːǯʼilam</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_65" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_67" n="HIAT:w" s="T19">meŋa</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_70" n="HIAT:w" s="T20">qondɨgu</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_73" n="HIAT:w" s="T21">täpɨla</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_76" n="HIAT:w" s="T22">tüukuwattə</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_79" n="HIAT:w" s="T23">assä</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_82" n="HIAT:w" s="T24">mikowattə</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_86" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_88" n="HIAT:w" s="T25">säwa</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_91" n="HIAT:w" s="T26">nunuŋ</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_94" n="HIAT:w" s="T27">čenačugu</ts>
                  <nts id="Seg_95" n="HIAT:ip">.</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_98" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_100" n="HIAT:w" s="T28">täpɨla</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_103" n="HIAT:w" s="T29">qätnattə</ts>
                  <nts id="Seg_104" n="HIAT:ip">:</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_107" n="HIAT:w" s="T30">čenučʼik</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_110" n="HIAT:w" s="T31">süsöguldʼel</ts>
                  <nts id="Seg_111" n="HIAT:ip">.</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_114" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_116" n="HIAT:w" s="T32">man</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_119" n="HIAT:w" s="T33">assä</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_122" n="HIAT:w" s="T34">kɨkkaŋ</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_125" n="HIAT:w" s="T35">čenučugu</ts>
                  <nts id="Seg_126" n="HIAT:ip">.</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_129" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_131" n="HIAT:w" s="T36">meŋa</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_134" n="HIAT:w" s="T37">südärugu</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_137" n="HIAT:w" s="T38">assä</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_140" n="HIAT:w" s="T39">mikuwattə</ts>
                  <nts id="Seg_141" n="HIAT:ip">.</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T40" id="Seg_143" n="sc" s="T1">
               <ts e="T2" id="Seg_145" n="e" s="T1">man </ts>
               <ts e="T3" id="Seg_147" n="e" s="T2">iraw </ts>
               <ts e="T4" id="Seg_149" n="e" s="T3">qwännɨ </ts>
               <ts e="T5" id="Seg_151" n="e" s="T4">oqqɨr </ts>
               <ts e="T6" id="Seg_153" n="e" s="T5">irettə. </ts>
               <ts e="T7" id="Seg_155" n="e" s="T6">man </ts>
               <ts e="T8" id="Seg_157" n="e" s="T7">onnäŋ </ts>
               <ts e="T9" id="Seg_159" n="e" s="T8">qalaŋ </ts>
               <ts e="T10" id="Seg_161" n="e" s="T9">üčezä. </ts>
               <ts e="T11" id="Seg_163" n="e" s="T10">meŋa </ts>
               <ts e="T12" id="Seg_165" n="e" s="T11">tümbattɨ </ts>
               <ts e="T13" id="Seg_167" n="e" s="T12">šɨtə </ts>
               <ts e="T14" id="Seg_169" n="e" s="T13">qum </ts>
               <ts e="T15" id="Seg_171" n="e" s="T14">qasaɣə </ts>
               <ts e="T16" id="Seg_173" n="e" s="T15">pajala </ts>
               <ts e="T17" id="Seg_175" n="e" s="T16">nägɨrguwattə </ts>
               <ts e="T18" id="Seg_177" n="e" s="T17">süsüɣulan </ts>
               <ts e="T19" id="Seg_179" n="e" s="T18">äːǯʼilam. </ts>
               <ts e="T20" id="Seg_181" n="e" s="T19">meŋa </ts>
               <ts e="T21" id="Seg_183" n="e" s="T20">qondɨgu </ts>
               <ts e="T22" id="Seg_185" n="e" s="T21">täpɨla </ts>
               <ts e="T23" id="Seg_187" n="e" s="T22">tüukuwattə </ts>
               <ts e="T24" id="Seg_189" n="e" s="T23">assä </ts>
               <ts e="T25" id="Seg_191" n="e" s="T24">mikowattə. </ts>
               <ts e="T26" id="Seg_193" n="e" s="T25">säwa </ts>
               <ts e="T27" id="Seg_195" n="e" s="T26">nunuŋ </ts>
               <ts e="T28" id="Seg_197" n="e" s="T27">čenačugu. </ts>
               <ts e="T29" id="Seg_199" n="e" s="T28">täpɨla </ts>
               <ts e="T30" id="Seg_201" n="e" s="T29">qätnattə: </ts>
               <ts e="T31" id="Seg_203" n="e" s="T30">čenučʼik </ts>
               <ts e="T32" id="Seg_205" n="e" s="T31">süsöguldʼel. </ts>
               <ts e="T33" id="Seg_207" n="e" s="T32">man </ts>
               <ts e="T34" id="Seg_209" n="e" s="T33">assä </ts>
               <ts e="T35" id="Seg_211" n="e" s="T34">kɨkkaŋ </ts>
               <ts e="T36" id="Seg_213" n="e" s="T35">čenučugu. </ts>
               <ts e="T37" id="Seg_215" n="e" s="T36">meŋa </ts>
               <ts e="T38" id="Seg_217" n="e" s="T37">südärugu </ts>
               <ts e="T39" id="Seg_219" n="e" s="T38">assä </ts>
               <ts e="T40" id="Seg_221" n="e" s="T39">mikuwattə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_222" s="T1">TAN_1963_MyWorkWithLinguists_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_223" s="T6">TAN_1963_MyWorkWithLinguists_nar.002 (001.002)</ta>
            <ta e="T19" id="Seg_224" s="T10">TAN_1963_MyWorkWithLinguists_nar.003 (001.003)</ta>
            <ta e="T25" id="Seg_225" s="T19">TAN_1963_MyWorkWithLinguists_nar.004 (001.004)</ta>
            <ta e="T28" id="Seg_226" s="T25">TAN_1963_MyWorkWithLinguists_nar.005 (001.005)</ta>
            <ta e="T32" id="Seg_227" s="T28">TAN_1963_MyWorkWithLinguists_nar.006 (001.006)</ta>
            <ta e="T36" id="Seg_228" s="T32">TAN_1963_MyWorkWithLinguists_nar.007 (001.007)</ta>
            <ta e="T40" id="Seg_229" s="T36">TAN_1963_MyWorkWithLinguists_nar.008 (001.008)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_230" s="T1">ман и′раw kwӓ′нны ′оkkыр и′ре(ӓ)ттъ.</ta>
            <ta e="T10" id="Seg_231" s="T6">ман о′ннӓң kа′лаң ӱ′чезӓ.</ta>
            <ta e="T19" id="Seg_232" s="T10">′меңа ′тӱмбатты ′шытъ kум kа′саɣъ па′jала нӓгыргу′ваттъ ‵сӱсӱ′ɣ′(k)улан ′ӓ̄дʼжʼилам.</ta>
            <ta e="T25" id="Seg_233" s="T19">′меңа kонды(у)′гу тӓпы′ла ′тӱуку‵wаттъ а′ссӓ мико′ваттъ.</ta>
            <ta e="T28" id="Seg_234" s="T25">′сӓwа ′нунуң ′ченачугу.</ta>
            <ta e="T32" id="Seg_235" s="T28">тӓпы′ла kӓт′наттъ: ченучʼик ′сӱсӧгулдʼел.</ta>
            <ta e="T36" id="Seg_236" s="T32">ман а′ссӓ кы′ккаң ′ченучугу.</ta>
            <ta e="T40" id="Seg_237" s="T36">′меңа ′сӱдӓругу а′ссӓ мику′wаттъ.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_238" s="T1">man iraw qwännɨ oqqɨr ire(ä)ttə.</ta>
            <ta e="T10" id="Seg_239" s="T6">man onnäŋ qalaŋ üčezä.</ta>
            <ta e="T19" id="Seg_240" s="T10">meŋa tümbattɨ šɨtə qum qasaɣə pajala nägɨrguvattə süsüɣ(q)ulan äːǯʼilam.</ta>
            <ta e="T25" id="Seg_241" s="T19">meŋa qondɨ(u)gu täpɨla tüukuwattə assä mikovattə.</ta>
            <ta e="T28" id="Seg_242" s="T25">säwa nunuŋ čenačugu.</ta>
            <ta e="T32" id="Seg_243" s="T28">täpɨla qätnattə: čenučʼik süsöguldʼel.</ta>
            <ta e="T36" id="Seg_244" s="T32">man assä kɨkkaŋ čenučugu.</ta>
            <ta e="T40" id="Seg_245" s="T36">meŋa südärugu assä mikuwattə.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_246" s="T1">man iraw qwännɨ oqqɨr irettə. </ta>
            <ta e="T10" id="Seg_247" s="T6">man onnäŋ qalaŋ üčezä. </ta>
            <ta e="T19" id="Seg_248" s="T10">meŋa tümbattɨ šɨtə qum qasaɣə pajala nägɨrguwattə süsüɣulan äːǯʼilam. </ta>
            <ta e="T25" id="Seg_249" s="T19">meŋa qondɨgu täpɨla tüukuwattə assä mikowattə. </ta>
            <ta e="T28" id="Seg_250" s="T25">säwa nunuŋ čenačugu. </ta>
            <ta e="T32" id="Seg_251" s="T28">täpɨla qätnattə: čenučʼik süsöguldʼel. </ta>
            <ta e="T36" id="Seg_252" s="T32">man assä kɨkkaŋ čenučugu. </ta>
            <ta e="T40" id="Seg_253" s="T36">meŋa südärugu assä mikuwattə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_254" s="T1">man</ta>
            <ta e="T3" id="Seg_255" s="T2">ira-w</ta>
            <ta e="T4" id="Seg_256" s="T3">qwän-nɨ</ta>
            <ta e="T5" id="Seg_257" s="T4">oqqɨr</ta>
            <ta e="T6" id="Seg_258" s="T5">ire-ttə</ta>
            <ta e="T7" id="Seg_259" s="T6">man</ta>
            <ta e="T8" id="Seg_260" s="T7">onnäŋ</ta>
            <ta e="T9" id="Seg_261" s="T8">qala-ŋ</ta>
            <ta e="T10" id="Seg_262" s="T9">üče-zä</ta>
            <ta e="T11" id="Seg_263" s="T10">meŋa</ta>
            <ta e="T12" id="Seg_264" s="T11">tü-mba-ttɨ</ta>
            <ta e="T13" id="Seg_265" s="T12">šɨtə</ta>
            <ta e="T14" id="Seg_266" s="T13">qum</ta>
            <ta e="T15" id="Seg_267" s="T14">qasaɣə</ta>
            <ta e="T16" id="Seg_268" s="T15">paja-la</ta>
            <ta e="T17" id="Seg_269" s="T16">nägɨ-r-gu-wa-ttə</ta>
            <ta e="T18" id="Seg_270" s="T17">süsüɣu-la-n</ta>
            <ta e="T19" id="Seg_271" s="T18">äːǯʼi-la-m</ta>
            <ta e="T20" id="Seg_272" s="T19">meŋa</ta>
            <ta e="T21" id="Seg_273" s="T20">qondɨ-gu</ta>
            <ta e="T22" id="Seg_274" s="T21">täp-ɨ-la</ta>
            <ta e="T23" id="Seg_275" s="T22">tüu-ku-wa-ttə</ta>
            <ta e="T24" id="Seg_276" s="T23">assä</ta>
            <ta e="T25" id="Seg_277" s="T24">mi-ko-wa-ttə</ta>
            <ta e="T26" id="Seg_278" s="T25">sä-wa</ta>
            <ta e="T27" id="Seg_279" s="T26">nunu-ŋ</ta>
            <ta e="T28" id="Seg_280" s="T27">čenaču-gu</ta>
            <ta e="T29" id="Seg_281" s="T28">täp-ɨ-la</ta>
            <ta e="T30" id="Seg_282" s="T29">qät-na-ttə</ta>
            <ta e="T31" id="Seg_283" s="T30">čenučʼi-k</ta>
            <ta e="T32" id="Seg_284" s="T31">süsögu-l-dʼel</ta>
            <ta e="T33" id="Seg_285" s="T32">man</ta>
            <ta e="T34" id="Seg_286" s="T33">assä</ta>
            <ta e="T35" id="Seg_287" s="T34">kɨkka-n</ta>
            <ta e="T36" id="Seg_288" s="T35">čenuču-gu</ta>
            <ta e="T37" id="Seg_289" s="T36">meŋa</ta>
            <ta e="T38" id="Seg_290" s="T37">süd-ä-r-u-gu</ta>
            <ta e="T39" id="Seg_291" s="T38">assä</ta>
            <ta e="T40" id="Seg_292" s="T39">mi-ku-wa-ttə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_293" s="T1">man</ta>
            <ta e="T3" id="Seg_294" s="T2">ira-mɨ</ta>
            <ta e="T4" id="Seg_295" s="T3">qwən-ŋɨ</ta>
            <ta e="T5" id="Seg_296" s="T4">okkɨr</ta>
            <ta e="T6" id="Seg_297" s="T5">irä-ndɨ</ta>
            <ta e="T7" id="Seg_298" s="T6">man</ta>
            <ta e="T8" id="Seg_299" s="T7">onäk</ta>
            <ta e="T9" id="Seg_300" s="T8">qalɨ-ŋ</ta>
            <ta e="T10" id="Seg_301" s="T9">üččə-se</ta>
            <ta e="T11" id="Seg_302" s="T10">mäkkä</ta>
            <ta e="T12" id="Seg_303" s="T11">tüː-mbɨ-tɨt</ta>
            <ta e="T13" id="Seg_304" s="T12">šittə</ta>
            <ta e="T14" id="Seg_305" s="T13">qum</ta>
            <ta e="T15" id="Seg_306" s="T14">qassaːq</ta>
            <ta e="T16" id="Seg_307" s="T15">paja-la</ta>
            <ta e="T17" id="Seg_308" s="T16">nägɨ-r-ku-ŋɨ-tɨt</ta>
            <ta e="T18" id="Seg_309" s="T17">süsügu-la-n</ta>
            <ta e="T19" id="Seg_310" s="T18">ɛːǯa-la-m</ta>
            <ta e="T20" id="Seg_311" s="T19">mäkkä</ta>
            <ta e="T21" id="Seg_312" s="T20">qontə-gu</ta>
            <ta e="T22" id="Seg_313" s="T21">tap-ɨ-la</ta>
            <ta e="T23" id="Seg_314" s="T22">tüː-ku-ŋɨ-tɨt</ta>
            <ta e="T24" id="Seg_315" s="T23">assɨ</ta>
            <ta e="T25" id="Seg_316" s="T24">mi-ku-ŋɨ-tɨt</ta>
            <ta e="T26" id="Seg_317" s="T25">seː-mɨ</ta>
            <ta e="T27" id="Seg_318" s="T26">nuːnə-n</ta>
            <ta e="T28" id="Seg_319" s="T27">čenču-gu</ta>
            <ta e="T29" id="Seg_320" s="T28">tap-ɨ-la</ta>
            <ta e="T30" id="Seg_321" s="T29">kät-ŋɨ-tɨt</ta>
            <ta e="T31" id="Seg_322" s="T30">čenču-äšɨk</ta>
            <ta e="T32" id="Seg_323" s="T31">süsügu-lʼ-dʼel</ta>
            <ta e="T33" id="Seg_324" s="T32">man</ta>
            <ta e="T34" id="Seg_325" s="T33">assɨ</ta>
            <ta e="T35" id="Seg_326" s="T34">kɨkkɨ-n</ta>
            <ta e="T36" id="Seg_327" s="T35">čenču-gu</ta>
            <ta e="T37" id="Seg_328" s="T36">mäkkä</ta>
            <ta e="T38" id="Seg_329" s="T37">šüt-ɨ-r-ɨ-gu</ta>
            <ta e="T39" id="Seg_330" s="T38">assɨ</ta>
            <ta e="T40" id="Seg_331" s="T39">mi-ku-ŋɨ-tɨt</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_332" s="T1">I.GEN</ta>
            <ta e="T3" id="Seg_333" s="T2">husband.[NOM]-1SG</ta>
            <ta e="T4" id="Seg_334" s="T3">go.away-CO.[3SG.S]</ta>
            <ta e="T5" id="Seg_335" s="T4">one</ta>
            <ta e="T6" id="Seg_336" s="T5">month-ILL</ta>
            <ta e="T7" id="Seg_337" s="T6">I.NOM</ta>
            <ta e="T8" id="Seg_338" s="T7">oneself.1SG</ta>
            <ta e="T9" id="Seg_339" s="T8">stay-1SG.S</ta>
            <ta e="T10" id="Seg_340" s="T9">child-COM</ta>
            <ta e="T11" id="Seg_341" s="T10">I.ALL</ta>
            <ta e="T12" id="Seg_342" s="T11">come-PST.NAR-3PL</ta>
            <ta e="T13" id="Seg_343" s="T12">two</ta>
            <ta e="T14" id="Seg_344" s="T13">human.being.[NOM]</ta>
            <ta e="T15" id="Seg_345" s="T14">Russian</ta>
            <ta e="T16" id="Seg_346" s="T15">old.woman-PL.[NOM]</ta>
            <ta e="T17" id="Seg_347" s="T16">write-FRQ-HAB-CO-3PL</ta>
            <ta e="T18" id="Seg_348" s="T17">Selkup-PL-GEN</ta>
            <ta e="T19" id="Seg_349" s="T18">word-PL-ACC</ta>
            <ta e="T20" id="Seg_350" s="T19">I.ALL</ta>
            <ta e="T21" id="Seg_351" s="T20">sleep-INF</ta>
            <ta e="T22" id="Seg_352" s="T21">(s)he-EP-PL.[NOM]</ta>
            <ta e="T23" id="Seg_353" s="T22">come-HAB-CO-3PL</ta>
            <ta e="T24" id="Seg_354" s="T23">NEG</ta>
            <ta e="T25" id="Seg_355" s="T24">give-HAB-CO-3PL</ta>
            <ta e="T26" id="Seg_356" s="T25">tongue.[NOM]-1SG</ta>
            <ta e="T27" id="Seg_357" s="T26">get.tired-3SG.S</ta>
            <ta e="T28" id="Seg_358" s="T27">speak-INF</ta>
            <ta e="T29" id="Seg_359" s="T28">(s)he-EP-PL.[NOM]</ta>
            <ta e="T30" id="Seg_360" s="T29">say-CO-3PL</ta>
            <ta e="T31" id="Seg_361" s="T30">speak-IMP.2SG.S</ta>
            <ta e="T32" id="Seg_362" s="T31">Selkup-ADJZ-like</ta>
            <ta e="T33" id="Seg_363" s="T32">I.NOM</ta>
            <ta e="T34" id="Seg_364" s="T33">NEG</ta>
            <ta e="T35" id="Seg_365" s="T34">want-3SG.S</ta>
            <ta e="T36" id="Seg_366" s="T35">speak-INF</ta>
            <ta e="T37" id="Seg_367" s="T36">I.ALL</ta>
            <ta e="T38" id="Seg_368" s="T37">sew-EP-FRQ-EP-INF</ta>
            <ta e="T39" id="Seg_369" s="T38">NEG</ta>
            <ta e="T40" id="Seg_370" s="T39">give-HAB-CO-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_371" s="T1">я.GEN</ta>
            <ta e="T3" id="Seg_372" s="T2">муж.[NOM]-1SG</ta>
            <ta e="T4" id="Seg_373" s="T3">уйти-CO.[3SG.S]</ta>
            <ta e="T5" id="Seg_374" s="T4">один</ta>
            <ta e="T6" id="Seg_375" s="T5">месяц-ILL</ta>
            <ta e="T7" id="Seg_376" s="T6">я.NOM</ta>
            <ta e="T8" id="Seg_377" s="T7">сам.1SG</ta>
            <ta e="T9" id="Seg_378" s="T8">остаться-1SG.S</ta>
            <ta e="T10" id="Seg_379" s="T9">ребёнок-COM</ta>
            <ta e="T11" id="Seg_380" s="T10">я.ALL</ta>
            <ta e="T12" id="Seg_381" s="T11">прийти-PST.NAR-3PL</ta>
            <ta e="T13" id="Seg_382" s="T12">два</ta>
            <ta e="T14" id="Seg_383" s="T13">человек.[NOM]</ta>
            <ta e="T15" id="Seg_384" s="T14">русский</ta>
            <ta e="T16" id="Seg_385" s="T15">старуха-PL.[NOM]</ta>
            <ta e="T17" id="Seg_386" s="T16">писать-FRQ-HAB-CO-3PL</ta>
            <ta e="T18" id="Seg_387" s="T17">селькуп-PL-GEN</ta>
            <ta e="T19" id="Seg_388" s="T18">слово-PL-ACC</ta>
            <ta e="T20" id="Seg_389" s="T19">я.ALL</ta>
            <ta e="T21" id="Seg_390" s="T20">спать-INF</ta>
            <ta e="T22" id="Seg_391" s="T21">он(а)-EP-PL.[NOM]</ta>
            <ta e="T23" id="Seg_392" s="T22">прийти-HAB-CO-3PL</ta>
            <ta e="T24" id="Seg_393" s="T23">NEG</ta>
            <ta e="T25" id="Seg_394" s="T24">дать-HAB-CO-3PL</ta>
            <ta e="T26" id="Seg_395" s="T25">язык.[NOM]-1SG</ta>
            <ta e="T27" id="Seg_396" s="T26">устать-3SG.S</ta>
            <ta e="T28" id="Seg_397" s="T27">говорить-INF</ta>
            <ta e="T29" id="Seg_398" s="T28">он(а)-EP-PL.[NOM]</ta>
            <ta e="T30" id="Seg_399" s="T29">сказать-CO-3PL</ta>
            <ta e="T31" id="Seg_400" s="T30">говорить-IMP.2SG.S</ta>
            <ta e="T32" id="Seg_401" s="T31">селькуп-ADJZ-вроде</ta>
            <ta e="T33" id="Seg_402" s="T32">я.NOM</ta>
            <ta e="T34" id="Seg_403" s="T33">NEG</ta>
            <ta e="T35" id="Seg_404" s="T34">хотеть-3SG.S</ta>
            <ta e="T36" id="Seg_405" s="T35">говорить-INF</ta>
            <ta e="T37" id="Seg_406" s="T36">я.ALL</ta>
            <ta e="T38" id="Seg_407" s="T37">сшить-EP-FRQ-EP-INF</ta>
            <ta e="T39" id="Seg_408" s="T38">NEG</ta>
            <ta e="T40" id="Seg_409" s="T39">дать-HAB-CO-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_410" s="T1">pers</ta>
            <ta e="T3" id="Seg_411" s="T2">n.[n:case]-n:poss</ta>
            <ta e="T4" id="Seg_412" s="T3">v-v:ins.[v:pn]</ta>
            <ta e="T5" id="Seg_413" s="T4">num</ta>
            <ta e="T6" id="Seg_414" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_415" s="T6">pers</ta>
            <ta e="T8" id="Seg_416" s="T7">emphpro</ta>
            <ta e="T9" id="Seg_417" s="T8">v-v:pn</ta>
            <ta e="T10" id="Seg_418" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_419" s="T10">pers</ta>
            <ta e="T12" id="Seg_420" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_421" s="T12">num</ta>
            <ta e="T14" id="Seg_422" s="T13">n.[n:case]</ta>
            <ta e="T15" id="Seg_423" s="T14">n</ta>
            <ta e="T16" id="Seg_424" s="T15">n-n:num.[n:case]</ta>
            <ta e="T17" id="Seg_425" s="T16">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T18" id="Seg_426" s="T17">n-n:num-n:case</ta>
            <ta e="T19" id="Seg_427" s="T18">n-n:num-n:case</ta>
            <ta e="T20" id="Seg_428" s="T19">pers</ta>
            <ta e="T21" id="Seg_429" s="T20">v-v:inf</ta>
            <ta e="T22" id="Seg_430" s="T21">pers-n:ins-n:num.[n:case]</ta>
            <ta e="T23" id="Seg_431" s="T22">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T24" id="Seg_432" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_433" s="T24">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T26" id="Seg_434" s="T25">n.[n:case]-n:poss</ta>
            <ta e="T27" id="Seg_435" s="T26">v-v:pn</ta>
            <ta e="T28" id="Seg_436" s="T27">v-v:inf</ta>
            <ta e="T29" id="Seg_437" s="T28">pers-n:ins-n:num.[n:case]</ta>
            <ta e="T30" id="Seg_438" s="T29">v-v:ins-v:pn</ta>
            <ta e="T31" id="Seg_439" s="T30">v-v:mood.pn</ta>
            <ta e="T32" id="Seg_440" s="T31">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T33" id="Seg_441" s="T32">pers</ta>
            <ta e="T34" id="Seg_442" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_443" s="T34">v-v:pn</ta>
            <ta e="T36" id="Seg_444" s="T35">v-v:inf</ta>
            <ta e="T37" id="Seg_445" s="T36">pers</ta>
            <ta e="T38" id="Seg_446" s="T37">v-n:ins-v&gt;v-n:ins-v:inf</ta>
            <ta e="T39" id="Seg_447" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_448" s="T39">v-v&gt;v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_449" s="T1">pers</ta>
            <ta e="T3" id="Seg_450" s="T2">n</ta>
            <ta e="T4" id="Seg_451" s="T3">v</ta>
            <ta e="T5" id="Seg_452" s="T4">num</ta>
            <ta e="T6" id="Seg_453" s="T5">n</ta>
            <ta e="T7" id="Seg_454" s="T6">pers</ta>
            <ta e="T8" id="Seg_455" s="T7">emphpro</ta>
            <ta e="T9" id="Seg_456" s="T8">v</ta>
            <ta e="T10" id="Seg_457" s="T9">n</ta>
            <ta e="T11" id="Seg_458" s="T10">pers</ta>
            <ta e="T12" id="Seg_459" s="T11">v</ta>
            <ta e="T13" id="Seg_460" s="T12">num</ta>
            <ta e="T14" id="Seg_461" s="T13">n</ta>
            <ta e="T15" id="Seg_462" s="T14">n</ta>
            <ta e="T16" id="Seg_463" s="T15">n</ta>
            <ta e="T17" id="Seg_464" s="T16">v</ta>
            <ta e="T18" id="Seg_465" s="T17">n</ta>
            <ta e="T19" id="Seg_466" s="T18">v</ta>
            <ta e="T20" id="Seg_467" s="T19">pers</ta>
            <ta e="T21" id="Seg_468" s="T20">v</ta>
            <ta e="T22" id="Seg_469" s="T21">pers</ta>
            <ta e="T23" id="Seg_470" s="T22">v</ta>
            <ta e="T24" id="Seg_471" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_472" s="T24">v</ta>
            <ta e="T26" id="Seg_473" s="T25">n</ta>
            <ta e="T27" id="Seg_474" s="T26">v</ta>
            <ta e="T28" id="Seg_475" s="T27">v</ta>
            <ta e="T29" id="Seg_476" s="T28">pers</ta>
            <ta e="T30" id="Seg_477" s="T29">v</ta>
            <ta e="T31" id="Seg_478" s="T30">v</ta>
            <ta e="T32" id="Seg_479" s="T31">adv</ta>
            <ta e="T33" id="Seg_480" s="T32">pers</ta>
            <ta e="T34" id="Seg_481" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_482" s="T34">v</ta>
            <ta e="T36" id="Seg_483" s="T35">v</ta>
            <ta e="T37" id="Seg_484" s="T36">pers</ta>
            <ta e="T38" id="Seg_485" s="T37">v</ta>
            <ta e="T39" id="Seg_486" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_487" s="T39">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_488" s="T2">np.h:S</ta>
            <ta e="T4" id="Seg_489" s="T3">v:pred</ta>
            <ta e="T7" id="Seg_490" s="T6">pro.h:S</ta>
            <ta e="T9" id="Seg_491" s="T8">v:pred</ta>
            <ta e="T12" id="Seg_492" s="T11">v:pred</ta>
            <ta e="T14" id="Seg_493" s="T13">np.h:S</ta>
            <ta e="T16" id="Seg_494" s="T15">np.h:S</ta>
            <ta e="T17" id="Seg_495" s="T16">v:pred</ta>
            <ta e="T19" id="Seg_496" s="T18">np:O</ta>
            <ta e="T22" id="Seg_497" s="T21">pro.h:S</ta>
            <ta e="T23" id="Seg_498" s="T22">v:pred</ta>
            <ta e="T26" id="Seg_499" s="T25">np:S</ta>
            <ta e="T27" id="Seg_500" s="T26">v:pred</ta>
            <ta e="T29" id="Seg_501" s="T28">pro.h:S</ta>
            <ta e="T30" id="Seg_502" s="T29">v:pred</ta>
            <ta e="T31" id="Seg_503" s="T30">0.2.h:S v:pred</ta>
            <ta e="T33" id="Seg_504" s="T32">pro.h:S</ta>
            <ta e="T35" id="Seg_505" s="T34">v:pred</ta>
            <ta e="T36" id="Seg_506" s="T35">s:compl</ta>
            <ta e="T40" id="Seg_507" s="T39">0.3.h:S</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_508" s="T1">pro.h:Poss</ta>
            <ta e="T3" id="Seg_509" s="T2">np.h:A 0.1.h:Poss</ta>
            <ta e="T6" id="Seg_510" s="T5">np:Time</ta>
            <ta e="T7" id="Seg_511" s="T6">pro.h:Th</ta>
            <ta e="T10" id="Seg_512" s="T9">np:Com</ta>
            <ta e="T11" id="Seg_513" s="T10">pro.h:G</ta>
            <ta e="T14" id="Seg_514" s="T13">np.h:A</ta>
            <ta e="T16" id="Seg_515" s="T15">np.h:A</ta>
            <ta e="T18" id="Seg_516" s="T17">np.h:Poss</ta>
            <ta e="T19" id="Seg_517" s="T18">np:Th</ta>
            <ta e="T20" id="Seg_518" s="T19">pro.h:R</ta>
            <ta e="T22" id="Seg_519" s="T21">pro.h:A</ta>
            <ta e="T25" id="Seg_520" s="T24">0.3.h:A</ta>
            <ta e="T26" id="Seg_521" s="T25">np:E 0.1.h:Poss</ta>
            <ta e="T29" id="Seg_522" s="T28">pro.h:A</ta>
            <ta e="T31" id="Seg_523" s="T30">0.2.h:A</ta>
            <ta e="T33" id="Seg_524" s="T32">pro.h:E</ta>
            <ta e="T37" id="Seg_525" s="T36">pro.h:R</ta>
            <ta e="T40" id="Seg_526" s="T39">0.3.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_527" s="T1">My husband is gone for one month.</ta>
            <ta e="T10" id="Seg_528" s="T6">I stayed with the child (at home).</ta>
            <ta e="T19" id="Seg_529" s="T10">Two people came to me, Russian women wrote Selkup words.</ta>
            <ta e="T25" id="Seg_530" s="T19">They come and don't let me sleep.</ta>
            <ta e="T28" id="Seg_531" s="T25">My tongue is tired of talking.</ta>
            <ta e="T32" id="Seg_532" s="T28">They say: Speak in Selkup!</ta>
            <ta e="T36" id="Seg_533" s="T32">I do not want to speak.</ta>
            <ta e="T40" id="Seg_534" s="T36">They won't let me sew.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_535" s="T1">Mein Mann ist für einen Monat weggegangen.</ta>
            <ta e="T10" id="Seg_536" s="T6">Ich blieb mit dem Kind (zu Hause).</ta>
            <ta e="T19" id="Seg_537" s="T10">Zwei Leute kamen zu mir, russische Frauen schrieben selkupische Wörter auf.</ta>
            <ta e="T25" id="Seg_538" s="T19">Sie kommen und lassen mich nicht schlafen.</ta>
            <ta e="T28" id="Seg_539" s="T25">Meine Zunge ist müde zu sprechen.</ta>
            <ta e="T32" id="Seg_540" s="T28">Sie sagen: Sprich auf Selkupisch!</ta>
            <ta e="T36" id="Seg_541" s="T32">Ich will nicht sprechen.</ta>
            <ta e="T40" id="Seg_542" s="T36">Sie lassen mich nicht nähen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_543" s="T1">Mой муж ушел на один месяц.</ta>
            <ta e="T10" id="Seg_544" s="T6">Я одна осталась с ребенком.</ta>
            <ta e="T19" id="Seg_545" s="T10">Ко мне пришли два человека, русские женщины, записывали остяцкие слова.</ta>
            <ta e="T25" id="Seg_546" s="T19">Они приходят, мне спать не дают.</ta>
            <ta e="T28" id="Seg_547" s="T25">Язык у меня устал разговаривать.</ta>
            <ta e="T32" id="Seg_548" s="T28">Они говорят: Разговаривай по-селькупски!</ta>
            <ta e="T36" id="Seg_549" s="T32">Я не хочу разговаривать.</ta>
            <ta e="T40" id="Seg_550" s="T36">Они мне шить не дают.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_551" s="T1">мой мужик ушел на один месяц</ta>
            <ta e="T10" id="Seg_552" s="T6">я одна осталась с ребенком</ta>
            <ta e="T19" id="Seg_553" s="T10">ко мне пришли две человека русские женщины записывать остяцкие слова</ta>
            <ta e="T25" id="Seg_554" s="T19">мне спать они приходят не дают</ta>
            <ta e="T28" id="Seg_555" s="T25">язык у меня устал разговаривать</ta>
            <ta e="T32" id="Seg_556" s="T28">они говорят разговаривай по-остяцки</ta>
            <ta e="T36" id="Seg_557" s="T32">я не хочу разговаривать</ta>
            <ta e="T40" id="Seg_558" s="T36">мне шить не дают</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
